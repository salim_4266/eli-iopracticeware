<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReviewRecordsControl
    Inherits Soaf.Presentation.Controls.WindowsForms.UserControl

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.RecordsGrid = New System.Windows.Forms.DataGridView()
        Me.ReviewLabel = New System.Windows.Forms.Label()
        Me.ConfirmButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.CountLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
        CType(Me.RecordsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RecordsGrid
        '
        Me.RecordsGrid.AllowUserToAddRows = False
        Me.RecordsGrid.AllowUserToDeleteRows = False
        Me.RecordsGrid.AllowUserToResizeRows = False
        Me.RecordsGrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RecordsGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.RecordsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.RecordsGrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.RecordsGrid.Location = New System.Drawing.Point(11, 41)
        Me.RecordsGrid.MultiSelect = False
        Me.RecordsGrid.Name = "RecordsGrid"
        Me.RecordsGrid.ReadOnly = True
        Me.RecordsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.RecordsGrid.Size = New System.Drawing.Size(507, 176)
        Me.RecordsGrid.TabIndex = 10
        '
        'ReviewLabel
        '
        Me.ReviewLabel.AutoSize = True
        Me.ReviewLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReviewLabel.Location = New System.Drawing.Point(9, 9)
        Me.ReviewLabel.Name = "ReviewLabel"
        Me.ReviewLabel.Size = New System.Drawing.Size(0, 20)
        Me.ReviewLabel.TabIndex = 12
        '
        'ConfirmButton
        '
        Me.ConfirmButton.Location = New System.Drawing.Point(410, 236)
        Me.ConfirmButton.Name = "ConfirmButton"
        Me.ConfirmButton.Size = New System.Drawing.Size(105, 55)
        Me.ConfirmButton.TabIndex = 9
        Me.ConfirmButton.Text = "Confirm"
        Me.ConfirmButton.Tooltip = Nothing
        '
        'CountLabel
        '
        Me.CountLabel.Location = New System.Drawing.Point(11, 223)
        Me.CountLabel.Name = "CountLabel"
        Me.CountLabel.Size = New System.Drawing.Size(78, 36)
        Me.CountLabel.TabIndex = 11
        Me.CountLabel.Text = "Count: "
        '
        'ReviewRecordsControl
        '
        Me.Controls.Add(Me.RecordsGrid)
        Me.Controls.Add(Me.ReviewLabel)
        Me.Controls.Add(Me.ConfirmButton)
        Me.Controls.Add(Me.CountLabel)
        Me.Name = "ReviewRecordsControl"
        Me.Size = New System.Drawing.Size(527, 300)
        CType(Me.RecordsGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RecordsGrid As System.Windows.Forms.DataGridView
    Friend WithEvents ReviewLabel As System.Windows.Forms.Label
    Friend WithEvents ConfirmButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents CountLabel As Soaf.Presentation.Controls.WindowsForms.Label

End Class
