Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports IO.Practiceware.Data
Imports Soaf.Presentation
Imports Soaf.Logging
Imports LogCategory = IO.Practiceware.Logging.LogCategory

Namespace SPEXUPCImport
    Public Class MainControl
        Private _logger As ILogger
        Private Property logManager() As ILogManager

        Private Sub SPEXUPCImportUtilityMainControl_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            PathTextBox.Text = "C:\"
            For Each Drive As DriveInfo In My.Computer.FileSystem.Drives
                If Drive.DriveType = DriveType.CDRom AndAlso Drive.IsReady Then
                    PathTextBox.Text = Drive.Name + "SpexUPC"
                    Exit For
                End If
            Next
        End Sub

        Private Sub BrowseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BrowseButton.Click
            FolderBrowserDialog.SelectedPath = PathTextBox.Text
            FolderBrowserDialog.ShowDialog()
            PathTextBox.Text = FolderBrowserDialog.SelectedPath
        End Sub

        Private Sub ImportButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportButton.Click
            Dim IsValidLocation As Boolean = True
            For Each Drive As DriveInfo In My.Computer.FileSystem.Drives
                If PathTextBox.Text.Length >= 2 AndAlso Drive.Name.Substring(0, 2) = PathTextBox.Text.Substring(0, 2) AndAlso Drive.DriveType = DriveType.Network Then
                    IsValidLocation = False
                End If
            Next
            If Not Directory.Exists(PathTextBox.Text) Then
                IsValidLocation = False
            End If
            Try
                If IsValidLocation Then
                    logManager = IO.Practiceware.Bootstrapper.Run(Of ILogManager)()
                    _logger = logManager.GetCurrentTypeLogger()
                    LogTextBox.Text = String.Empty
                    While BackgroundWorker.IsBusy()
                    End While
                    ImportButton.Enabled = False
                    DropAndCreatePCInventorySPEXUPCTable()
                    CancelImportButton.Enabled = True
                    BackgroundWorker.RunWorkerAsync(PathTextBox.Text)
                Else
                    InteractionManager.Current.Alert("Invalid directory. Please make sure you are using My Network Places to choose the location if you have the SPEX UPC contents at a network location.")
                End If
            Catch ex As Exception
                Trace.TraceError(ex.ToString())
            End Try
        End Sub

        Private Sub BackgroundWorker_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker.DoWork
            Try
                
                Dim UtilityDirectoryPath As String = "\\" + New SqlConnectionStringBuilder(Configuration.ConfigurationManager.PracticeRepositoryConnectionString).DataSource + "\IOP\Pinpoint\Utility\SPEXUPC\"

                If Directory.Exists(UtilityDirectoryPath) Then
                    My.Computer.FileSystem.MoveDirectory(UtilityDirectoryPath, UtilityDirectoryPath.Substring(0, UtilityDirectoryPath.Length - 1) + "." + Date.Now().ToString("MM.dd.yyyy.HH.mm"), True)
                End If
                My.Computer.FileSystem.CreateDirectory(UtilityDirectoryPath)
                SearchDirectory(e.Argument.ToString)
                If Not BackgroundWorker.CancellationPending Then
                    TrimPCInventorySPEXUPCTable()
                    UpdatePCInventoryTable()
                End If
            Catch ex As Exception
                Trace.TraceError(ex.ToString())
            End Try
        End Sub

        Private Sub BackgroundWorker_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker.ProgressChanged
            LogTextBox.AppendText(String.Format("\n{0}", e.UserState))

        End Sub

        Private Sub BackgroundWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker.RunWorkerCompleted
            LogTextBox.Text += Environment.NewLine + "Done!"
            CancelImportButton.Enabled = False
            ImportButton.Enabled = True
            _logger.Log("Imported UPC Data", categories:={LogCategory.Utilities})
        End Sub

        Private Sub CancelImportButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelImportButton.Click
            BackgroundWorker.ReportProgress(0, "Cancelled!")
            BackgroundWorker.CancelAsync()
            CancelImportButton.Enabled = False
            ImportButton.Enabled = True
        End Sub

        Private Sub DropAndCreatePCInventorySPEXUPCTable()
            Try
                Dim dbConnection = DbConnectionFactory.PracticeRepository()
                Dim dbCommand = dbConnection.CreateCommand()
                dbCommand.CommandText = My.Resources.DropAndCreatePCInventorySPEXUPC
                DbConnection.Open()
                If DbConnection.State = ConnectionState.Open Then
                    DbCommand.ExecuteNonQuery()
                    DbConnection.Close()
                End If
            Catch ex As Exception
                Trace.TraceError(ex.ToString())
            End Try
        End Sub

        Private Sub TrimPCInventorySPEXUPCTable()
            Try
                BackgroundWorker.ReportProgress(0, "Trimming PCInventorySPEXUPC table...")
                Dim dbConnection = DbConnectionFactory.PracticeRepository()
                Dim dbCommand = dbConnection.CreateCommand()
                dbCommand.CommandText = My.Resources.TrimPCInventorySPEXUPC
                DbConnection.Open()
                If DbConnection.State = ConnectionState.Open Then
                    DbCommand.CommandTimeout = 1200
                    DbCommand.ExecuteNonQuery()
                    DbConnection.Close()
                End If
            Catch ex As Exception
                Trace.TraceError(ex.ToString())
            End Try
        End Sub

        Private Sub UpdatePCInventoryTable()
            Try
                BackgroundWorker.ReportProgress(0, "Updating PCInventory table...")

                Dim dbConnection = DbConnectionFactory.PracticeRepository()
                Dim dbCommand = dbConnection.CreateCommand()
                dbCommand.CommandText = My.Resources.UpdatePCInventory
                DbConnection.Open()
                If DbConnection.State = ConnectionState.Open Then
                    DbCommand.CommandTimeout = 1200
                    DbCommand.ExecuteNonQuery()
                    DbConnection.Close()
                End If
            Catch ex As Exception
                Trace.TraceError(ex.ToString())
            End Try
        End Sub

        Private Sub SearchDirectory(ByVal Path As String)
            Dim FileCount As Integer = 1
            BackgroundWorker.ReportProgress(0, "Processing directory " + Path + "...")
            For Each FilePath As String In My.Computer.FileSystem.GetFiles(Path, FileIO.SearchOption.SearchTopLevelOnly, "b*.upc")
                If BackgroundWorker.CancellationPending Then
                    Exit For
                End If
                Dim FileName As String = My.Computer.FileSystem.GetName(FilePath)
                BackgroundWorker.ReportProgress(0, "Processing " + FileName + ", file " + FileCount.ToString() + " of " + Directory.GetFiles(Path, "b*.upc", SearchOption.AllDirectories).Length.ToString() + "...")
                ImportFile(FilePath)
                FileCount += 1
            Next
        End Sub

        Private Sub ImportFile(ByVal Path As String)
            Try
                Dim UtilityDirectoryPath As String = "\\" + New SqlConnectionStringBuilder(Configuration.ConfigurationManager.PracticeRepositoryConnectionString).DataSource + "\IOP\Pinpoint\Utility\SPEXUPC\"

                My.Computer.FileSystem.WriteAllBytes(UtilityDirectoryPath + "PCInventorySPEXUPC.fmt", My.Resources.PCInventorySPEXUPC, False)

                Dim FileName As String = My.Computer.FileSystem.GetName(Path)
                My.Computer.FileSystem.CopyFile(Path, UtilityDirectoryPath + FileName, True)
                My.Computer.FileSystem.GetFileInfo(UtilityDirectoryPath + FileName).Attributes = FileAttributes.Normal And Not FileAttributes.ReadOnly
                Trace.TraceInformation("Getting file info...")
                Dim ImportCommandText As String = My.Resources.BulkImport
                ImportCommandText = ImportCommandText.Replace("%IMPORTTABLE%", DbConnectionFactory.PracticeRepository.Database + ".dbo.PCInventorySPEXUPC").Replace("%IMPORTFILE%", UtilityDirectoryPath + FileName).Replace("%FORMATFILE%", UtilityDirectoryPath + "PCInventorySPEXUPC.fmt")

                Trace.TraceInformation("Creating DB connection.")
                Dim dbConnection = DbConnectionFactory.PracticeRepository()
                Dim dbCommand = dbConnection.CreateCommand()
                dbCommand.CommandText = My.Resources.Executexp_cmdshell.Replace("%COMMAND%", ImportCommandText)
                DbConnection.Open()
                If DbConnection.State = ConnectionState.Open Then
                    Try
                        Trace.TraceInformation("Running import.")
                        Dim Result As Object = DbCommand.ExecuteScalar()
                        If Not TypeOf Result Is DBNull Then
                            BackgroundWorker.ReportProgress(0, "Error encountered performing import. Cancelling.")
                            BackgroundWorker.CancelAsync()
                        End If
                    Catch ex As Exception
                    End Try
                    DbConnection.Close()
                End If
                Trace.TraceInformation("Done importing.")
            Catch ex As Exception
                Trace.TraceError(ex.ToString())
            End Try

        End Sub

        Private Sub LogTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LogTextBox.TextChanged
            LogTextBox.SelectionStart = LogTextBox.Text.Length
            LogTextBox.ScrollToCaret()
        End Sub
    End Class
End Namespace