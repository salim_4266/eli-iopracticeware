Namespace SPEXUPCImport
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MainControl
        Inherits Soaf.Presentation.Controls.WindowsForms.UserControl

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.StatusLabel = New System.Windows.Forms.Label()
            Me.CancelImportButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.FolderBrowserDialog = New System.Windows.Forms.FolderBrowserDialog()
            Me.ImportButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.LogTextBox = New System.Windows.Forms.TextBox()
            Me.BrowseButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.BackgroundWorker = New System.ComponentModel.BackgroundWorker()
            Me.PathTextBox = New System.Windows.Forms.TextBox()
            Me.SpecifyDirectoryLabel = New System.Windows.Forms.Label()
            Me.SuspendLayout()
            '
            'StatusLabel
            '
            Me.StatusLabel.AutoSize = True
            Me.StatusLabel.Location = New System.Drawing.Point(18, 93)
            Me.StatusLabel.Name = "StatusLabel"
            Me.StatusLabel.Size = New System.Drawing.Size(40, 13)
            Me.StatusLabel.TabIndex = 13
            Me.StatusLabel.Text = "Status:"
            '
            'CancelImportButton
            '
            Me.CancelImportButton.Enabled = False
            Me.CancelImportButton.Location = New System.Drawing.Point(474, 187)
            Me.CancelImportButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.CancelImportButton.Name = "CancelImportButton"
            Me.CancelImportButton.Size = New System.Drawing.Size(75, 36)
            Me.CancelImportButton.TabIndex = 12
            Me.CancelImportButton.Text = "Cancel"
            Me.CancelImportButton.Tooltip = Nothing
            '
            'FolderBrowserDialog
            '
            Me.FolderBrowserDialog.ShowNewFolderButton = False
            '
            'ImportButton
            '
            Me.ImportButton.Location = New System.Drawing.Point(474, 155)
            Me.ImportButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.ImportButton.Name = "ImportButton"
            Me.ImportButton.Size = New System.Drawing.Size(75, 28)
            Me.ImportButton.TabIndex = 10
            Me.ImportButton.Text = "Import"
            Me.ImportButton.Tooltip = Nothing
            '
            'LogTextBox
            '
            Me.LogTextBox.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LogTextBox.Location = New System.Drawing.Point(20, 114)
            Me.LogTextBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LogTextBox.Multiline = True
            Me.LogTextBox.Name = "LogTextBox"
            Me.LogTextBox.ReadOnly = True
            Me.LogTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.LogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me.LogTextBox.Size = New System.Drawing.Size(421, 110)
            Me.LogTextBox.TabIndex = 11
            Me.LogTextBox.WordWrap = False
            '
            'BrowseButton
            '
            Me.BrowseButton.Location = New System.Drawing.Point(382, 29)
            Me.BrowseButton.Margin = New System.Windows.Forms.Padding(2)
            Me.BrowseButton.Name = "BrowseButton"
            Me.BrowseButton.Size = New System.Drawing.Size(58, 24)
            Me.BrowseButton.TabIndex = 9
            Me.BrowseButton.Text = "Browse"
            Me.BrowseButton.Tooltip = Nothing
            '
            'BackgroundWorker
            '
            Me.BackgroundWorker.WorkerReportsProgress = True
            Me.BackgroundWorker.WorkerSupportsCancellation = True
            '
            'PathTextBox
            '
            Me.PathTextBox.Location = New System.Drawing.Point(20, 32)
            Me.PathTextBox.Margin = New System.Windows.Forms.Padding(2)
            Me.PathTextBox.Name = "PathTextBox"
            Me.PathTextBox.Size = New System.Drawing.Size(345, 20)
            Me.PathTextBox.TabIndex = 8
            '
            'SpecifyDirectoryLabel
            '
            Me.SpecifyDirectoryLabel.AutoSize = True
            Me.SpecifyDirectoryLabel.Location = New System.Drawing.Point(18, 14)
            Me.SpecifyDirectoryLabel.Name = "SpecifyDirectoryLabel"
            Me.SpecifyDirectoryLabel.Size = New System.Drawing.Size(344, 13)
            Me.SpecifyDirectoryLabel.TabIndex = 7
            Me.SpecifyDirectoryLabel.Text = "Specify the location of the SPEXUPC directory to import UPC data from:"
            '
            'MainControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)

            Me.Controls.Add(Me.StatusLabel)
            Me.Controls.Add(Me.CancelImportButton)
            Me.Controls.Add(Me.ImportButton)
            Me.Controls.Add(Me.LogTextBox)
            Me.Controls.Add(Me.BrowseButton)
            Me.Controls.Add(Me.PathTextBox)
            Me.Controls.Add(Me.SpecifyDirectoryLabel)
            Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.Name = "MainControl"
            Me.Size = New System.Drawing.Size(567, 239)
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents StatusLabel As System.Windows.Forms.Label
        Friend WithEvents CancelImportButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents FolderBrowserDialog As System.Windows.Forms.FolderBrowserDialog
        Friend WithEvents ImportButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents LogTextBox As System.Windows.Forms.TextBox
        Friend WithEvents BrowseButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents BackgroundWorker As System.ComponentModel.BackgroundWorker
        Friend WithEvents PathTextBox As System.Windows.Forms.TextBox
        Friend WithEvents SpecifyDirectoryLabel As System.Windows.Forms.Label

    End Class
End Namespace