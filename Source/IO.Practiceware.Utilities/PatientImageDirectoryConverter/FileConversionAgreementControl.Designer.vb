﻿
Namespace PatientImageDirectoryConverter
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class FileConversionAgreementControl
        Inherits Soaf.Presentation.Controls.WindowsForms.UserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()

            Me.AgreementText = New Soaf.Presentation.Controls.WindowsForms.TextBox()
            Me.HeaderLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.ConditionLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.NameLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.NameText = New Soaf.Presentation.Controls.WindowsForms.TextBox()
            Me.NextButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.Termscheckbox = New System.Windows.Forms.CheckBox()
            Me.SuspendLayout()
            '
            'AgreementText
            '
            Me.AgreementText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.AgreementText.Location = New System.Drawing.Point(13, 33)
            Me.AgreementText.Multiline = True
            Me.AgreementText.Name = "AgreementText"
            Me.AgreementText.Size = New System.Drawing.Size(352, 386)
            Me.AgreementText.TabIndex = 11
            '
            'HeaderLabel
            '
            Me.HeaderLabel.Font = New System.Drawing.Font("Segoe UI Semibold", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.HeaderLabel.Location = New System.Drawing.Point(21, -3)
            Me.HeaderLabel.Name = "HeaderLabel"
            Me.HeaderLabel.Size = New System.Drawing.Size(379, 25)
            Me.HeaderLabel.TabIndex = 12
            Me.HeaderLabel.Text = "IO Practiceware File Conversion"
            Me.HeaderLabel.TextAlignment = System.Windows.TextAlignment.Center
            '
            'ConditionLabel
            '
            Me.ConditionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.ConditionLabel.Location = New System.Drawing.Point(15, 442)
            Me.ConditionLabel.Name = "ConditionLabel"
            Me.ConditionLabel.Size = New System.Drawing.Size(303, 17)
            Me.ConditionLabel.TabIndex = 13
            Me.ConditionLabel.Text = "By Checking this box you agree terms and Conditions above"
            '
            'NameLabel
            '
            Me.NameLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.NameLabel.Location = New System.Drawing.Point(15, 500)
            Me.NameLabel.Name = "NameLabel"
            Me.NameLabel.Size = New System.Drawing.Size(38, 19)
            Me.NameLabel.TabIndex = 15
            Me.NameLabel.Text = "Name"
            '
            'NameText
            '
            Me.NameText.Location = New System.Drawing.Point(55, 495)
            Me.NameText.Name = "NameText"
            Me.NameText.Size = New System.Drawing.Size(231, 27)
            Me.NameText.TabIndex = 16
            '
            'NextButton
            '
            Me.NextButton.Location = New System.Drawing.Point(303, 485)
            Me.NextButton.Name = "NextButton"
            Me.NextButton.Size = New System.Drawing.Size(62, 48)
            Me.NextButton.TabIndex = 17
            Me.NextButton.Text = "Next"
            '
            'Termscheckbox
            '
            Me.Termscheckbox.FlatStyle = System.Windows.Forms.FlatStyle.System
            Me.Termscheckbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Termscheckbox.Location = New System.Drawing.Point(343, 434)
            Me.Termscheckbox.Name = "Termscheckbox"
            Me.Termscheckbox.Size = New System.Drawing.Size(37, 32)
            Me.Termscheckbox.TabIndex = 20
            Me.Termscheckbox.UseVisualStyleBackColor = True
            '
            'FileConversionAgreementControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.Termscheckbox)
            Me.Controls.Add(Me.NextButton)
            Me.Controls.Add(Me.NameText)
            Me.Controls.Add(Me.NameLabel)
            Me.Controls.Add(Me.ConditionLabel)
            Me.Controls.Add(Me.HeaderLabel)
            Me.Controls.Add(Me.AgreementText)

            Me.Name = "FileConversionAgreementControl"
            Me.Size = New System.Drawing.Size(463, 640)
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents AgreementText As Soaf.Presentation.Controls.WindowsForms.TextBox
        Friend WithEvents HeaderLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents ConditionLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents NameLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents NameText As Soaf.Presentation.Controls.WindowsForms.TextBox
        Friend WithEvents NextButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents Termscheckbox As System.Windows.Forms.CheckBox

    End Class
End Namespace



