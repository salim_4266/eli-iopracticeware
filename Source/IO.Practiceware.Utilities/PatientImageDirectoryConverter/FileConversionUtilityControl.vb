﻿Imports IO.Practiceware.Configuration
Imports Soaf
Imports Soaf.ComponentModel
Imports System.IO
Imports System.Text.RegularExpressions
Imports Soaf.Presentation
Imports IO.Practiceware.Storage

Namespace PatientImageDirectoryConverter

    Public Class FileConversionUtilityControl

        Public Sub New()
            InitializeComponent()
        End Sub

#Region " Loading Presenter "

        Protected Overrides Sub OnLoad(ByVal e As EventArgs)
            MyBase.OnLoad(e)

            Presenter = New FileConversionUtilityControlPresenter
            AddHandler Presenter.ProgressChanged, AddressOf OnBackgroundWorkerProgressChanged
            AddHandler Presenter.RunWorkerCompleted, AddressOf OnBackgroundWorkerRunWorkerCompleted

        End Sub

#End Region

#Region "Events"


        Private Sub ConfigurationFilePathBrowseButtonClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles ConfigurationFilePathBrowseButton.Click
            FolderBrowserDialog.SelectedPath = ConfigurationManager.ServerDataPath + "\MyScan"
            FolderBrowserDialog.ShowDialog()
            FileLocationPathText.Text = FolderBrowserDialog.SelectedPath
        End Sub
        Private Sub StartButtonClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles StartButton.Click


            If DateFilterCheckbox.Checked = True Then
                If StartDatePicker.SelectedValue Is Nothing Or EndDatePicker.SelectedValue Is Nothing Then
                    InteractionManager.Current.Confirm("Please select date range.")
                    Exit Sub
                Else
                    Presenter.Arguments.StartDate = CDate(StartDatePicker.SelectedValue)
                    Presenter.Arguments.EndDate = CDate(EndDatePicker.SelectedValue)
                    Presenter.Arguments.Filterdate = True
                End If
            End If

            If FileFilterCheckbox.Checked = True Then
                If FileFilterText.Text = "" Then
                    InteractionManager.Current.Confirm("Please enter filter text")
                    Exit Sub
                Else
                    Presenter.Arguments.Filtertext = FileFilterText.Text
                    Presenter.Arguments.Filterfile = True
                End If

            End If

            If RunHoursCheckbox.Checked = True Then
                If RunHoursTextbox.Text = "" Then
                    InteractionManager.Current.Confirm("Please enter number of hours to run.")
                    Exit Sub
                End If
                If Not IsNumeric(RunHoursTextbox.Text) Then
                    InteractionManager.Current.Confirm("Run hours must be numeric.")
                    Exit Sub
                End If

                Presenter.Arguments.Runminutes = CInt(RunHoursTextbox.Text)
                Presenter.Arguments.Processendtime = DateTime.Now.AddMinutes(Presenter.Arguments.Runminutes)
            Else
                Presenter.Arguments.Runminutes = 0
            End If

            If MoveFilesCheckbox.Checked = True Then
                If MoveFilesTextbox.Text = "" Then
                    InteractionManager.Current.Confirm("Please enter number of files to be moved.")
                    Exit Sub
                End If
                If Not IsNumeric(MoveFilesTextbox.Text) Then
                    InteractionManager.Current.Confirm("Number of files to move must be numeric.")
                    Exit Sub
                End If
                Presenter.Arguments.MoveFilesCount = CInt(MoveFilesTextbox.Text)
            Else
                Presenter.Arguments.MoveFilesCount = 0
            End If

            StartButton.Enabled = False
            StopButton.Enabled = True
            LogTextBox.Text = String.Empty
            Presenter.Arguments.Locationpath = FileLocationPathText.Text

            Presenter.Dowork()

            End Sub
        Private Sub DateFilterCheckboxClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles DateFilterCheckbox.Click

            If DateFilterCheckbox.Checked = True Then
                StartDatePicker.Enabled = True
                EndDatePicker.Enabled = True
            Else
                StartDatePicker.Enabled = False
                EndDatePicker.Enabled = False
                StartDatePicker.SelectedValue = Nothing
                EndDatePicker.SelectedValue = Nothing
            End If

        End Sub
        Private Sub FileFilterCheckboxClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles FileFilterCheckbox.Click

            If FileFilterCheckbox.Checked = True Then

                FileFilterText.Enabled = True
            Else
                FileFilterText.Enabled = False
                FileFilterText.Text = String.Empty
            End If

        End Sub
        Private Sub RunHoursCheckboxClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles RunHoursCheckbox.Click

            If RunHoursCheckbox.Checked = True Then
                RunHoursTextbox.Enabled = True
            Else
                RunHoursTextbox.Enabled = False
                RunHoursTextbox.Text = String.Empty
            End If

        End Sub
        Private Sub MoveFilesCheckboxClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles MoveFilesCheckbox.Click

            If MoveFilesCheckbox.Checked = True Then
                MoveFilesTextbox.Enabled = True
            Else
                MoveFilesTextbox.Enabled = False
                MoveFilesTextbox.Text = String.Empty
            End If
        End Sub
        Private Sub StopButtonClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles StopButton.Click

            Presenter.StopBackgroundworker()
          
        End Sub
        Private Sub OnBackgroundWorkerProgressChanged(ByVal sender As Object, ByVal e As EventArgs(Of String))
            LogTextBox.Text += Environment.NewLine
            LogTextBox.AppendText(String.Format("{0}", e.Value))
        End Sub
        Private Sub OnBackgroundWorkerRunWorkerCompleted(ByVal sender As Object, ByVal e As EventArgs(Of Exception))

            If e.Value IsNot Nothing Then
                LogTextBox.SelectionColor = Drawing.Color.Red
                LogTextBox.AppendText(Environment.NewLine + e.Value.Message)
            Else
                LogTextBox.AppendText(Environment.NewLine + "Done!")

            End If

            StartButton.Enabled = True
            StopButton.Enabled = False
        End Sub


#End Region

    End Class
    Public Class FileConversionUtilityControlPresenter
        Implements ILoadable

        Private ReadOnly _backgroundWorker As New BackgroundWorker
        Private ReadOnly _arguments As New FileConversionUtilityControlArguments
        Public Event ProgressChanged As EventHandler(Of EventArgs(Of String))
        Public Event RunWorkerCompleted As EventHandler(Of EventArgs(Of Exception))

        Public ReadOnly Property LoadActions() As IEnumerable(Of Action) Implements ILoadable.LoadActions
            Get
                Dim actions As Action()
                actions = {}
                Return actions
            End Get
        End Property
        Public ReadOnly Property Arguments As FileConversionUtilityControlArguments
            Get
                Return _arguments
            End Get
        End Property
        Public Sub New()

            _backgroundWorker.WorkerReportsProgress = True
            _backgroundWorker.WorkerSupportsCancellation = True
            AddHandler _backgroundWorker.DoWork, Sub() Processfiles()
            AddHandler _backgroundWorker.ProgressChanged, Sub(sender, e) RaiseEvent ProgressChanged(Me, New EventArgs(Of String)(DirectCast(e.UserState, String)))
            AddHandler _backgroundWorker.RunWorkerCompleted, Sub(sender, e) RaiseEvent RunWorkerCompleted(Me, New EventArgs(Of Exception)(e.Error))


        End Sub
        Public Sub Processfiles()

            Dim fileList() As String = FileManager.Instance.ListFiles(Arguments.Locationpath)
            Dim actualFileList = FileManager.Instance.GetFileInfos(fileList).ToList()
            Arguments.FilesProcessedCount = 0

            If Arguments.FilterDate Then
                actualFileList = actualFileList.Where(Function(t) DateTime.Compare(t.CreationTimeUtc.ToLocalTime(), CDate(Arguments.StartDate)) > 0 And DateTime.Compare(t.CreationTimeUtc.ToLocalTime(), CDate(Arguments.EndDate)) < 0).ToList()
            End If

            If Arguments.FilterFile Then
                actualFileList = actualFileList.Where(Function(t) Path.GetFileName(t.FullName).ToLower.StartsWith(Arguments.FilterText.ToLower())).ToList
            End If

            If actualFileList.Count > 0 Then

                For Each fileObj In actualFileList

                    If _backgroundWorker.CancellationPending Then
                        Exit For
                    End If

                    If Arguments.RunMinutes > 0 Then
                        If DateTime.Now > Arguments.ProcessEndTime Then
                            Exit For
                        End If
                    End If

                    If Arguments.MoveFilesCount > 0 Then
                        If Arguments.MoveFilesCount = Arguments.FilesProcessedCount Then
                            Exit For
                        End If
                    End If


                    If Patientfiles(Arguments.LocationPath, fileObj, actualFileList.Count) Then
                        Arguments.FilesProcessedCount += 1
                    End If

                    If PatientAppointmentfiles(Arguments.LocationPath, fileObj, actualFileList.Count) Then
                        Arguments.FilesProcessedCount += 1
                    End If

                Next

            End If

        End Sub
        Public Sub Dowork()

            _backgroundWorker.RunWorkerAsync()

        End Sub
        Private Function Patientfiles(ByVal directorylocation As String, ByVal fileObj As FileInfoRecord, ByVal actualFilescount As Integer) As Boolean
            Try
                Dim regexPatient As New Regex("__(?<PatientId>\d+?)-")
                Dim matchpatient As Match = regexPatient.Match(fileObj.Name)

                If matchpatient.Groups("PatientId").Value <> "" Then

                    Dim patientid = matchpatient.Groups("PatientId").Value
                    If IsNumeric(patientid) = True Then
                        Dim patientfolder = directorylocation & "\" & patientid
                        Dim sourcefilename = fileObj.FullName
                        Dim destinationfilename As String = patientfolder & "\" & fileObj.Name
                        Movefiles(patientfolder, sourcefilename, destinationfilename, actualFilescount)
                        Return True

                    End If

                End If
            Catch ex As Exception
                Trace.TraceError(ex.ToString())
            End Try
            Return False

        End Function
        Private Function PatientAppointmentfiles(ByVal directorylocation As String, ByVal fileObj As FileInfoRecord, ByVal actualFilescount As Integer) As Boolean
            Try
                Dim regexAppoitntment As New Regex("-_(?<AppointmentId>\d+?)_(?<PatientId>\d+?)-")
                Dim matchpatientandappointment As Match = regexAppoitntment.Match(fileObj.Name)


                If matchpatientandappointment.Groups("AppointmentId").Value <> "" And matchpatientandappointment.Groups("PatientId").Value <> "" Then
                    Dim patientid = matchpatientandappointment.Groups("PatientId").Value
                    Dim appointmentid = matchpatientandappointment.Groups("AppointmentId").Value

                    If IsNumeric(patientid) = True And IsNumeric(appointmentid) = True Then
                        Dim patientfolder = directorylocation & "\" & patientid
                        Dim sourcefilename = fileObj.FullName
                        Dim destinationfilename As String = patientfolder & "\" & fileObj.Name
                        Movefiles(patientfolder, sourcefilename, destinationfilename, actualFilescount)
                        Return True
                    End If
                End If


            Catch ex As Exception
                Trace.TraceError(ex.ToString())
            End Try
            Return False
        End Function
        Private Sub Movefiles(ByVal patientfolder As String, ByVal sourceFileName As String, ByVal destinationfilename As String, ByVal actualFilescount As Integer)
            Try
                If FileManager.Instance.FileExists(destinationfilename) Then
                    FileManager.Instance.Delete(destinationfilename)
                End If
                If FileManager.Instance.DirectoryExists(patientfolder) Then
                    FileManager.Instance.Move(sourceFileName, destinationfilename)
                    _backgroundWorker.ReportProgress(0, "Processed  " & destinationfilename & ", file " & Arguments.FilesProcessedCount + 1 & " of " & actualFilescount & "...")

                Else
                    FileManager.Instance.CreateDirectory(patientfolder)
                    FileManager.Instance.Move(sourceFileName, destinationfilename)

                    _backgroundWorker.ReportProgress(0, "Processed  " & destinationfilename + ", file " & Arguments.FilesProcessedCount + 1 & " of " & actualFilescount & "...")

                End If

            Catch ex As Exception
                Trace.TraceError(ex.ToString())
                Throw
            End Try
        End Sub
        Public Sub StopBackgroundworker()

            _backgroundWorker.CancelAsync()

        End Sub

    End Class
    Public Class FileConversionUtilityControlArguments

        Public Runminutes As Integer
        Public Movefilescount As Integer
        Public Locationpath As String
        Public FilesProcessedCount As Integer
        Public FilterDate As Boolean
        Public FilterFile As Boolean
        Public Processendtime As DateTime
        Public StartDate As DateTime
        Public EndDate As DateTime
        Public FilterText As String

    End Class

End Namespace

