﻿Namespace PatientImageDirectoryConverter
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class FileConversionUtilityControl
        Inherits FileConversionUtilityControlBase


        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()

            Me.HeaderLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.FileCriteriaLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.Panel1 = New Soaf.Presentation.Controls.WindowsForms.Panel()
            Me.FileFilterCheckbox = New System.Windows.Forms.CheckBox()
            Me.DateFilterCheckbox = New System.Windows.Forms.CheckBox()
            Me.ConfigurationFilePathBrowseButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.FilePtahLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.FileFilterText = New Soaf.Presentation.Controls.WindowsForms.TextBox()
            Me.FileFilterLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.AndLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.EndDatePicker = New Soaf.Presentation.Controls.WindowsForms.DatePicker()
            Me.StartDatePicker = New Soaf.Presentation.Controls.WindowsForms.DatePicker()
            Me.DateConditionLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.FileLocationPathText = New Soaf.Presentation.Controls.WindowsForms.TextBox()
            Me.LocationLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.Panel2 = New Soaf.Presentation.Controls.WindowsForms.Panel()
            Me.MoveFilesCheckbox = New System.Windows.Forms.CheckBox()
            Me.RunHoursCheckbox = New System.Windows.Forms.CheckBox()
            Me.FilesLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.HoursLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.MoveFilesTextbox = New Soaf.Presentation.Controls.WindowsForms.TextBox()
            Me.MoveFileLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.RunHoursTextbox = New Soaf.Presentation.Controls.WindowsForms.TextBox()
            Me.RunForLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.TimeCreteriaLabel = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.StartButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.StopButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.FolderBrowserDialog = New System.Windows.Forms.FolderBrowserDialog()
            Me.Label1 = New Soaf.Presentation.Controls.WindowsForms.Label()
            Me.LogTextBox = New System.Windows.Forms.RichTextBox()
            Me.Panel1.SuspendLayout()
            Me.Panel2.SuspendLayout()
            Me.SuspendLayout()
            '
            'HeaderLabel
            '
            Me.HeaderLabel.Font = New System.Drawing.Font("Segoe UI Semibold", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.HeaderLabel.Location = New System.Drawing.Point(79, 0)
            Me.HeaderLabel.Name = "HeaderLabel"
            Me.HeaderLabel.Size = New System.Drawing.Size(379, 25)
            Me.HeaderLabel.TabIndex = 13
            Me.HeaderLabel.Text = "IO Practiceware File Conversion Utility"
            Me.HeaderLabel.TextAlignment = System.Windows.TextAlignment.Center
            '
            'FileCriteriaLabel
            '
            Me.FileCriteriaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FileCriteriaLabel.Location = New System.Drawing.Point(7, 41)
            Me.FileCriteriaLabel.Name = "FileCriteriaLabel"
            Me.FileCriteriaLabel.Size = New System.Drawing.Size(100, 17)
            Me.FileCriteriaLabel.TabIndex = 14
            Me.FileCriteriaLabel.Text = "Specify file criteria"
            '
            'Panel1
            '
            Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.Panel1.Controls.Add(Me.FileFilterCheckbox)
            Me.Panel1.Controls.Add(Me.DateFilterCheckbox)
            Me.Panel1.Controls.Add(Me.ConfigurationFilePathBrowseButton)
            Me.Panel1.Controls.Add(Me.FilePtahLabel)
            Me.Panel1.Controls.Add(Me.FileFilterText)
            Me.Panel1.Controls.Add(Me.FileFilterLabel)
            Me.Panel1.Controls.Add(Me.AndLabel)
            Me.Panel1.Controls.Add(Me.EndDatePicker)
            Me.Panel1.Controls.Add(Me.StartDatePicker)
            Me.Panel1.Controls.Add(Me.DateConditionLabel)
            Me.Panel1.Controls.Add(Me.FileLocationPathText)
            Me.Panel1.Controls.Add(Me.LocationLabel)
            Me.Panel1.Location = New System.Drawing.Point(3, 64)
            Me.Panel1.Name = "Panel1"
            Me.Panel1.Size = New System.Drawing.Size(559, 193)
            Me.Panel1.TabIndex = 15
            '
            'FileFilterCheckbox
            '
            Me.FileFilterCheckbox.AutoSize = True
            Me.FileFilterCheckbox.Location = New System.Drawing.Point(19, 160)
            Me.FileFilterCheckbox.Name = "FileFilterCheckbox"
            Me.FileFilterCheckbox.Size = New System.Drawing.Size(15, 14)
            Me.FileFilterCheckbox.TabIndex = 31
            Me.FileFilterCheckbox.UseVisualStyleBackColor = True
            '
            'DateFilterCheckbox
            '
            Me.DateFilterCheckbox.AutoSize = True
            Me.DateFilterCheckbox.Location = New System.Drawing.Point(19, 70)
            Me.DateFilterCheckbox.Name = "DateFilterCheckbox"
            Me.DateFilterCheckbox.Size = New System.Drawing.Size(15, 14)
            Me.DateFilterCheckbox.TabIndex = 30
            Me.DateFilterCheckbox.UseVisualStyleBackColor = True
            '
            'ConfigurationFilePathBrowseButton
            '
            Me.ConfigurationFilePathBrowseButton.Location = New System.Drawing.Point(484, 10)
            Me.ConfigurationFilePathBrowseButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.ConfigurationFilePathBrowseButton.Name = "ConfigurationFilePathBrowseButton"
            Me.ConfigurationFilePathBrowseButton.Size = New System.Drawing.Size(64, 26)
            Me.ConfigurationFilePathBrowseButton.TabIndex = 29
            Me.ConfigurationFilePathBrowseButton.Text = "Browse"
            '
            'FilePtahLabel
            '
            Me.FilePtahLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FilePtahLabel.Location = New System.Drawing.Point(173, 41)
            Me.FilePtahLabel.Name = "FilePtahLabel"
            Me.FilePtahLabel.Size = New System.Drawing.Size(279, 14)
            Me.FilePtahLabel.TabIndex = 27
            Me.FilePtahLabel.Text = "ie: D:\IOP\Pinpoint\MyScan"
            '
            'FileFilterText
            '
            Me.FileFilterText.Enabled = False
            Me.FileFilterText.Location = New System.Drawing.Point(175, 155)
            Me.FileFilterText.Name = "FileFilterText"
            Me.FileFilterText.Size = New System.Drawing.Size(56, 27)
            Me.FileFilterText.TabIndex = 26
            '
            'FileFilterLabel
            '
            Me.FileFilterLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FileFilterLabel.Location = New System.Drawing.Point(41, 161)
            Me.FileFilterLabel.Name = "FileFilterLabel"
            Me.FileFilterLabel.Size = New System.Drawing.Size(130, 19)
            Me.FileFilterLabel.TabIndex = 25
            Me.FileFilterLabel.Text = "Only files beginning with :"
            '
            'AndLabel
            '
            Me.AndLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.AndLabel.Location = New System.Drawing.Point(258, 114)
            Me.AndLabel.Name = "AndLabel"
            Me.AndLabel.Size = New System.Drawing.Size(23, 18)
            Me.AndLabel.TabIndex = 23
            Me.AndLabel.Text = "and"
            '
            'EndDatePicker
            '
            Me.EndDatePicker.Enabled = False
            Me.EndDatePicker.Location = New System.Drawing.Point(296, 108)
            Me.EndDatePicker.Name = "EndDatePicker"
            Me.EndDatePicker.Size = New System.Drawing.Size(166, 26)
            Me.EndDatePicker.TabIndex = 22
            '
            'StartDatePicker
            '
            Me.StartDatePicker.Enabled = False
            Me.StartDatePicker.Location = New System.Drawing.Point(66, 107)
            Me.StartDatePicker.Name = "StartDatePicker"
            Me.StartDatePicker.Size = New System.Drawing.Size(166, 26)
            Me.StartDatePicker.TabIndex = 21
            '
            'DateConditionLabel
            '
            Me.DateConditionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.DateConditionLabel.Location = New System.Drawing.Point(41, 71)
            Me.DateConditionLabel.Name = "DateConditionLabel"
            Me.DateConditionLabel.Size = New System.Drawing.Size(396, 19)
            Me.DateConditionLabel.TabIndex = 20
            Me.DateConditionLabel.Text = "Only Move files with a date modified  between : (Leave unchecked for all dates)"
            '
            'FileLocationPathText
            '
            Me.FileLocationPathText.Location = New System.Drawing.Point(172, 9)
            Me.FileLocationPathText.Name = "FileLocationPathText"
            Me.FileLocationPathText.Size = New System.Drawing.Size(299, 27)
            Me.FileLocationPathText.TabIndex = 17
            '
            'LocationLabel
            '
            Me.LocationLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LocationLabel.Location = New System.Drawing.Point(9, 15)
            Me.LocationLabel.Name = "LocationLabel"
            Me.LocationLabel.Size = New System.Drawing.Size(162, 17)
            Me.LocationLabel.TabIndex = 15
            Me.LocationLabel.Text = "Pick to location of scanned files:"
            '
            'Panel2
            '
            Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.Panel2.Controls.Add(Me.MoveFilesCheckbox)
            Me.Panel2.Controls.Add(Me.RunHoursCheckbox)
            Me.Panel2.Controls.Add(Me.FilesLabel)
            Me.Panel2.Controls.Add(Me.HoursLabel)
            Me.Panel2.Controls.Add(Me.MoveFilesTextbox)
            Me.Panel2.Controls.Add(Me.MoveFileLabel)
            Me.Panel2.Controls.Add(Me.RunHoursTextbox)
            Me.Panel2.Controls.Add(Me.RunForLabel)
            Me.Panel2.Location = New System.Drawing.Point(2, 292)
            Me.Panel2.Name = "Panel2"
            Me.Panel2.Size = New System.Drawing.Size(560, 96)
            Me.Panel2.TabIndex = 17
            '
            'MoveFilesCheckbox
            '
            Me.MoveFilesCheckbox.AutoSize = True
            Me.MoveFilesCheckbox.Location = New System.Drawing.Point(20, 64)
            Me.MoveFilesCheckbox.Name = "MoveFilesCheckbox"
            Me.MoveFilesCheckbox.Size = New System.Drawing.Size(15, 14)
            Me.MoveFilesCheckbox.TabIndex = 37
            Me.MoveFilesCheckbox.UseVisualStyleBackColor = True
            '
            'RunHoursCheckbox
            '
            Me.RunHoursCheckbox.AutoSize = True
            Me.RunHoursCheckbox.Location = New System.Drawing.Point(20, 18)
            Me.RunHoursCheckbox.Name = "RunHoursCheckbox"
            Me.RunHoursCheckbox.Size = New System.Drawing.Size(15, 14)
            Me.RunHoursCheckbox.TabIndex = 36
            Me.RunHoursCheckbox.UseVisualStyleBackColor = True
            '
            'FilesLabel
            '
            Me.FilesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FilesLabel.Location = New System.Drawing.Point(216, 67)
            Me.FilesLabel.Name = "FilesLabel"
            Me.FilesLabel.Size = New System.Drawing.Size(51, 19)
            Me.FilesLabel.TabIndex = 33
            Me.FilesLabel.Text = "files"
            '
            'HoursLabel
            '
            Me.HoursLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.HoursLabel.Location = New System.Drawing.Point(216, 20)
            Me.HoursLabel.Name = "HoursLabel"
            Me.HoursLabel.Size = New System.Drawing.Size(51, 19)
            Me.HoursLabel.TabIndex = 32
            Me.HoursLabel.Text = "Minutes"
            '
            'MoveFilesTextbox
            '
            Me.MoveFilesTextbox.Enabled = False
            Me.MoveFilesTextbox.Location = New System.Drawing.Point(99, 58)
            Me.MoveFilesTextbox.Name = "MoveFilesTextbox"
            Me.MoveFilesTextbox.Size = New System.Drawing.Size(106, 27)
            Me.MoveFilesTextbox.TabIndex = 31
            '
            'MoveFileLabel
            '
            Me.MoveFileLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.MoveFileLabel.Location = New System.Drawing.Point(42, 65)
            Me.MoveFileLabel.Name = "MoveFileLabel"
            Me.MoveFileLabel.Size = New System.Drawing.Size(42, 19)
            Me.MoveFileLabel.TabIndex = 29
            Me.MoveFileLabel.Text = "Move"
            '
            'RunHoursTextbox
            '
            Me.RunHoursTextbox.Enabled = False
            Me.RunHoursTextbox.Location = New System.Drawing.Point(99, 12)
            Me.RunHoursTextbox.Name = "RunHoursTextbox"
            Me.RunHoursTextbox.Size = New System.Drawing.Size(106, 27)
            Me.RunHoursTextbox.TabIndex = 30
            '
            'RunForLabel
            '
            Me.RunForLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.RunForLabel.Location = New System.Drawing.Point(42, 20)
            Me.RunForLabel.Name = "RunForLabel"
            Me.RunForLabel.Size = New System.Drawing.Size(51, 19)
            Me.RunForLabel.TabIndex = 27
            Me.RunForLabel.Text = "Run for:"
            '
            'TimeCreteriaLabel
            '
            Me.TimeCreteriaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.TimeCreteriaLabel.Location = New System.Drawing.Point(3, 267)
            Me.TimeCreteriaLabel.Name = "TimeCreteriaLabel"
            Me.TimeCreteriaLabel.Size = New System.Drawing.Size(152, 19)
            Me.TimeCreteriaLabel.TabIndex = 16
            Me.TimeCreteriaLabel.Text = "Specify run time  criteria"
            '
            'StartButton
            '
            Me.StartButton.Location = New System.Drawing.Point(191, 536)
            Me.StartButton.Name = "StartButton"
            Me.StartButton.Size = New System.Drawing.Size(62, 48)
            Me.StartButton.TabIndex = 18
            Me.StartButton.Text = "Start"
            '
            'StopButton
            '
            Me.StopButton.Enabled = False
            Me.StopButton.Location = New System.Drawing.Point(281, 536)
            Me.StopButton.Name = "StopButton"
            Me.StopButton.Size = New System.Drawing.Size(62, 48)
            Me.StopButton.TabIndex = 19
            Me.StopButton.Text = "Stop"
            '
            'Label1
            '
            Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Location = New System.Drawing.Point(6, 400)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(152, 17)
            Me.Label1.TabIndex = 22
            Me.Label1.Text = "Status"
            '
            'LogTextBox
            '
            Me.LogTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.LogTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!)
            Me.LogTextBox.Location = New System.Drawing.Point(2, 419)
            Me.LogTextBox.Name = "LogTextBox"
            Me.LogTextBox.Size = New System.Drawing.Size(558, 108)
            Me.LogTextBox.TabIndex = 23
            Me.LogTextBox.Text = ""
            '
            'FileConversionUtilityControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LogTextBox)
            Me.Controls.Add(Me.Label1)
            Me.Controls.Add(Me.StopButton)
            Me.Controls.Add(Me.StartButton)
            Me.Controls.Add(Me.Panel2)
            Me.Controls.Add(Me.TimeCreteriaLabel)
            Me.Controls.Add(Me.Panel1)
            Me.Controls.Add(Me.FileCriteriaLabel)
            Me.Controls.Add(Me.HeaderLabel)
           
            Me.Name = "FileConversionUtilityControl"
            Me.Size = New System.Drawing.Size(670, 700)
            Me.Panel1.ResumeLayout(False)
            Me.Panel1.PerformLayout()
            Me.Panel2.ResumeLayout(False)
            Me.Panel2.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents HeaderLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents FileCriteriaLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents Panel1 As Soaf.Presentation.Controls.WindowsForms.Panel
        Friend WithEvents Panel2 As Soaf.Presentation.Controls.WindowsForms.Panel
        Friend WithEvents TimeCreteriaLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents StartButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents StopButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents LocationLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents FileLocationPathText As Soaf.Presentation.Controls.WindowsForms.TextBox
        Friend WithEvents DateConditionLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents StartDatePicker As Soaf.Presentation.Controls.WindowsForms.DatePicker
        Friend WithEvents FileFilterText As Soaf.Presentation.Controls.WindowsForms.TextBox
        Friend WithEvents FileFilterLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents AndLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents EndDatePicker As Soaf.Presentation.Controls.WindowsForms.DatePicker
        Friend WithEvents FilesLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents HoursLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents MoveFilesTextbox As Soaf.Presentation.Controls.WindowsForms.TextBox
        Friend WithEvents RunHoursTextbox As Soaf.Presentation.Controls.WindowsForms.TextBox
        Friend WithEvents MoveFileLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents RunForLabel As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents FilePtahLabel As Soaf.Presentation.Controls.WindowsForms.Label
        public WithEvents ConfigurationFilePathBrowseButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents FolderBrowserDialog As System.Windows.Forms.FolderBrowserDialog
        Friend WithEvents Label1 As Soaf.Presentation.Controls.WindowsForms.Label
        Friend WithEvents FileFilterCheckbox As System.Windows.Forms.CheckBox
        Friend WithEvents DateFilterCheckbox As System.Windows.Forms.CheckBox
        Friend WithEvents RunHoursCheckbox As System.Windows.Forms.CheckBox
        Friend WithEvents MoveFilesCheckbox As System.Windows.Forms.CheckBox
        Friend WithEvents LogTextBox As System.Windows.Forms.RichTextBox

    End Class

    Public Class FileConversionUtilityControlBase
        Inherits Soaf.Presentation.Controls.WindowsForms.PresentationControl(Of FileConversionUtilityControlPresenter)
    End Class
End Namespace

