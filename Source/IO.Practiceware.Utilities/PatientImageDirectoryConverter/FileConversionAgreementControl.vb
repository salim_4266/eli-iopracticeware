﻿

Option Strict Off
Imports Soaf.Presentation




Namespace PatientImageDirectoryConverter

    Public Class FileConversionAgreementControl

        Public Sub New()


            ' This call is required by the designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call.

        End Sub
        Private Sub NextButtonClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles NextButton.Click

            If Termscheckbox.Checked = False Then
                InteractionManager.Current.Confirm("Please check Terms and Conditions")
                Exit Sub
            End If

            If NameText.Text = String.Empty Then
                InteractionManager.Current.Confirm("Please enter Name")
                Exit Sub
            End If

            InteractionContext.Complete(True)
            InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = New FileConversionUtilityControl})
         

        End Sub
        Public Shared Sub ShowAgreementControl()
            InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = New FileConversionAgreementControl})
        End Sub
        Private Sub FileConversionAgreementControlLoad(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            AgreementText.Text = My.Resources.FileConversionAgreement
            End Sub


    End Class
End Namespace



