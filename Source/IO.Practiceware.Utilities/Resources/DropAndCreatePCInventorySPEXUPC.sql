if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PCInventorySPEXUPC]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PCInventorySPEXUPC];

CREATE TABLE [dbo].[PCInventorySPEXUPC] (
	[InventoryId] [int] IDENTITY (1, 1) NOT NULL ,
	[FPC] [nvarchar] (14) NULL ,
	[UPC] [nvarchar] (14) NULL ,
	[StyleId] [nvarchar] (8) NULL ,
	[StyleName] [nvarchar] (50) NULL ,
	[ColorDescription] [nvarchar] (50) NULL ,
	[ColorCode] [nvarchar] (20) NULL ,
	[LensColor] [nvarchar] (50) NULL ,
	[LensColorCode] [nvarchar] (20) NULL ,
	[Eye] [nvarchar] (3) NULL ,
	[Bridge] [nvarchar] (10) NULL ,
	[Temple] [nvarchar] (10) NULL ,
	[DBL] [nvarchar] (10) NULL ,
	[A] [nvarchar] (5) NULL ,
	[B] [nvarchar] (5) NULL ,
	[ED] [nvarchar] (5) NULL ,
	[Circumference] [nvarchar] (6) NULL ,
	[EDAngle] [nvarchar] (6) NULL ,
	[FrontPrice] [nvarchar] (6) NULL ,
	[HalfTemplesPrice] [nvarchar] (6) NULL ,
	[TemplesPrice] [nvarchar] (6) NULL ,
	[CompletePrice] [nvarchar] (7) NULL ,
	[ManufacturerName] [nvarchar] (50) NULL ,
	[BrandName] [nvarchar] (50) NULL ,
	[CollectionName] [nvarchar] (50) NULL ,
	[GenderType] [nvarchar] (10) NULL ,
	[AgeGroup] [nvarchar] (15) NULL ,
	[ActiveStatus] [nvarchar] (1) NULL ,
	[ProductGroupName] [nvarchar] (15) NULL ,
	[RimType] [nvarchar] (25) NULL ,
	[Material] [nvarchar] (50) NULL ,
	[FrameShape] [nvarchar] (15) NULL ,
	[StyleNew] [nvarchar] (1) NULL ,
	[ChangedPrice] [nvarchar] (1) NULL ,
	[County] [nvarchar] (30) NULL ,
	[SKU] [nvarchar] (30) NULL ,
	[YearIntroduced] [nvarchar] (4) NULL 
) ON [PRIMARY];