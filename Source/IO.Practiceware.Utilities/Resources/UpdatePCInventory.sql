IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
WHERE CONSTRAINT_SCHEMA='dbo' AND CONSTRAINT_NAME='CK_PCInventory' AND TABLE_NAME='PCInventory')
BEGIN
ALTER TABLE PCInventory
DROP CONSTRAINT [CK_PCInventory] 
ALTER TABLE PCInventory
ADD CONSTRAINT [CK_PCInventory] CHECK  NOT FOR REPLICATION ([InventoryId] >= 1 and [InventoryId] <= 50000000)
END;

INSERT INTO dbo.PCInventory (SKU, Type_, Manufacturer, Model, Color, PCSize, LensCoating, LensTint, PDD, PDN, CEGHeight, Qty, Cost, WCost)
SELECT UPC, CollectionName, ManufacturerName, StyleName, ColorDescription, 0,  '', LensColor, '', '', '', 0, CompletePrice, CompletePrice FROM
PCInventorySPEXUPC
WHERE PCInventorySPEXUPC.UPC NOT IN (SELECT SKU FROM PCInventory);
