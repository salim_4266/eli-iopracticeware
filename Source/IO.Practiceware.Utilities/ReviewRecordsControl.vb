Imports System.Collections.Generic
Imports System.Data
Imports System.Windows.Forms
Imports System.Transactions
Imports Soaf.Data
Imports IO.Practiceware.Data
Imports Soaf.Logging
Imports LogCategory = IO.Practiceware.Logging.LogCategory


Public Class ReviewRecordsControl
    ReadOnly _selectQuery As String = ""
    ReadOnly _updateQueries As List(Of String)
    Private _logger As ILogger
    Private Property LogManager() As ILogManager


    Public Sub New(ByVal reviewMessage As String, ByVal selectQuery As String, ByVal updateQueries As List(Of String))

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        ReviewLabel.Text = reviewMessage
        _selectQuery = selectQuery
        _updateQueries = updateQueries
    End Sub

    Private Sub ReviewRecordsAffectedForm_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        RecordsGrid.Rows.Clear()

        Dim dt = DbConnectionFactory.PracticeRepository.ExecuteAndDispose(Of DataTable)(_selectQuery)
        RecordsGrid.DataSource = dt
        If (RecordsGrid.Rows.Count > 0) Then
            RecordsGrid.Rows(0).Selected = True
        End If

        CountLabel.Text = String.Format("Count: {0}", RecordsGrid.RowCount)
    End Sub

    Private Sub ConfirmButton_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ConfirmButton.Click
        If MessageBox.Show("Commit these changes?", "", MessageBoxButtons.YesNo) = DialogResult.Yes Then
            Try
                LogManager = Bootstrapper.Run(Of ILogManager)()
                _logger = LogManager.GetCurrentTypeLogger()
                Using ts = New TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(10))

                    Using dbConnection = DbConnectionFactory.PracticeRepository()
                        dbConnection.Open()
                        For Each updateQuery As String In _updateQueries
                            Dim dbCommand = dbConnection.CreateCommand()
                            dbCommand.CommandText = updateQuery
                            dbCommand.ExecuteNonQuery()
                        Next
                    End Using
                    ts.Complete()
                    If ReviewLabel.Text.ToUpper = "Confirm the recalls to be requeued:".ToUpper Then
                        _logger.Log("Requeued Recalls", categories:={LogCategory.Utilities})
                    Else
                        _logger.Log("Requeued Claims", categories:={LogCategory.Utilities})
                    End If
                    MessageBox.Show("Done.")
                End Using
            Catch exception As Exception
                MessageBox.Show(exception.Message)
            End Try
        End If
        InteractionContext.Complete(True)
    End Sub
End Class