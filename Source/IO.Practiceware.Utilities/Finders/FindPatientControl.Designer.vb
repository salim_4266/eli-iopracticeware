Namespace Finders
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class FindPatientControl
        Inherits Soaf.Presentation.Controls.WindowsForms.UserControl

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LastNameLabel = New System.Windows.Forms.Label()
            Me.FirstNameLabel = New System.Windows.Forms.Label()
            Me.LastNameTextBox = New System.Windows.Forms.TextBox()
            Me.FirstNameTextBox = New System.Windows.Forms.TextBox()
            Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
            Me.PatientsGrid = New System.Windows.Forms.DataGridView()
            Me.IdColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.LastNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.FirstNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.FindButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.TableLayoutPanel1.SuspendLayout()
            CType(Me.PatientsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LastNameLabel
            '
            Me.LastNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Right
            Me.LastNameLabel.AutoSize = True
            Me.LastNameLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LastNameLabel.Location = New System.Drawing.Point(30, 12)
            Me.LastNameLabel.Name = "LastNameLabel"
            Me.LastNameLabel.Size = New System.Drawing.Size(59, 13)
            Me.LastNameLabel.TabIndex = 0
            Me.LastNameLabel.Text = "Last Name"
            '
            'FirstNameLabel
            '
            Me.FirstNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Right
            Me.FirstNameLabel.AutoSize = True
            Me.FirstNameLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FirstNameLabel.Location = New System.Drawing.Point(212, 12)
            Me.FirstNameLabel.Name = "FirstNameLabel"
            Me.FirstNameLabel.Size = New System.Drawing.Size(61, 13)
            Me.FirstNameLabel.TabIndex = 1
            Me.FirstNameLabel.Text = "First Name"
            '
            'LastNameTextBox
            '
            Me.LastNameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Right
            Me.LastNameTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LastNameTextBox.Location = New System.Drawing.Point(95, 7)
            Me.LastNameTextBox.Name = "LastNameTextBox"
            Me.LastNameTextBox.Size = New System.Drawing.Size(86, 22)
            Me.LastNameTextBox.TabIndex = 2
            '
            'FirstNameTextBox
            '
            Me.FirstNameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Right
            Me.FirstNameTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FirstNameTextBox.Location = New System.Drawing.Point(279, 7)
            Me.FirstNameTextBox.Name = "FirstNameTextBox"
            Me.FirstNameTextBox.Size = New System.Drawing.Size(86, 22)
            Me.FirstNameTextBox.TabIndex = 3
            '
            'TableLayoutPanel1
            '
            Me.TableLayoutPanel1.ColumnCount = 4
            Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
            Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
            Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
            Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
            Me.TableLayoutPanel1.Controls.Add(Me.LastNameLabel, 0, 0)
            Me.TableLayoutPanel1.Controls.Add(Me.FirstNameLabel, 2, 0)
            Me.TableLayoutPanel1.Controls.Add(Me.FirstNameTextBox, 3, 0)
            Me.TableLayoutPanel1.Controls.Add(Me.LastNameTextBox, 1, 0)
            Me.TableLayoutPanel1.Location = New System.Drawing.Point(12, 12)
            Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
            Me.TableLayoutPanel1.RowCount = 1
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
            Me.TableLayoutPanel1.Size = New System.Drawing.Size(368, 37)
            Me.TableLayoutPanel1.TabIndex = 4
            '
            'PatientsGrid
            '
            Me.PatientsGrid.AllowUserToAddRows = False
            Me.PatientsGrid.AllowUserToDeleteRows = False
            Me.PatientsGrid.AllowUserToResizeColumns = False
            Me.PatientsGrid.AllowUserToResizeRows = False
            Me.PatientsGrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
            Me.PatientsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
            Me.PatientsGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdColumn, Me.LastNameColumn, Me.FirstNameColumn})
            Me.PatientsGrid.Location = New System.Drawing.Point(12, 78)
            Me.PatientsGrid.MultiSelect = False
            Me.PatientsGrid.Name = "PatientsGrid"
            Me.PatientsGrid.ReadOnly = True
            Me.PatientsGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.PatientsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
            Me.PatientsGrid.Size = New System.Drawing.Size(381, 150)
            Me.PatientsGrid.TabIndex = 5
            '
            'IdColumn
            '
            Me.IdColumn.HeaderText = "Id"
            Me.IdColumn.Name = "IdColumn"
            Me.IdColumn.ReadOnly = True
            Me.IdColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            '
            'LastNameColumn
            '
            Me.LastNameColumn.HeaderText = "Last Name"
            Me.LastNameColumn.Name = "LastNameColumn"
            Me.LastNameColumn.ReadOnly = True
            Me.LastNameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.LastNameColumn.Width = 150
            '
            'FirstNameColumn
            '
            Me.FirstNameColumn.HeaderText = "First Name"
            Me.FirstNameColumn.Name = "FirstNameColumn"
            Me.FirstNameColumn.ReadOnly = True
            Me.FirstNameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.FirstNameColumn.Width = 150
            '
            'FindButton
            '
            Me.FindButton.Location = New System.Drawing.Point(403, 12)
            Me.FindButton.Name = "FindButton"
            Me.FindButton.Size = New System.Drawing.Size(75, 38)
            Me.FindButton.TabIndex = 6
            Me.FindButton.Text = "Find"
            '
            'FindPatientControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)

            Me.ClientSize = New System.Drawing.Size(505, 259)
            Me.Controls.Add(Me.FindButton)
            Me.Controls.Add(Me.PatientsGrid)
            Me.Controls.Add(Me.TableLayoutPanel1)
            Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me.Name = "FindPatientForm"
            Me.Text = "Find Patient"
            Me.Controls.SetChildIndex(Me.TableLayoutPanel1, 0)
            Me.Controls.SetChildIndex(Me.PatientsGrid, 0)
            Me.Controls.SetChildIndex(Me.FindButton, 0)
            Me.TableLayoutPanel1.ResumeLayout(False)
            Me.TableLayoutPanel1.PerformLayout()
            CType(Me.PatientsGrid, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LastNameLabel As System.Windows.Forms.Label
        Friend WithEvents FirstNameLabel As System.Windows.Forms.Label
        Friend WithEvents LastNameTextBox As System.Windows.Forms.TextBox
        Friend WithEvents FirstNameTextBox As System.Windows.Forms.TextBox
        Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
        Friend WithEvents PatientsGrid As System.Windows.Forms.DataGridView
        Friend WithEvents FindButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents IdColumn As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents LastNameColumn As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents FirstNameColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    End Class
End Namespace