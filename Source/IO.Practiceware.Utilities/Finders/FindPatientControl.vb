Imports System.Windows.Forms
Imports IO.Practiceware.Data
Imports Soaf.Presentation

Namespace Finders
    Public Class FindPatientControl
        Public PatientId As Integer

        Private Sub FindButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles FindButton.Click
            PatientsGrid.Rows.Clear()
            If LastNameTextBox.Text <> "" Or FirstNameTextBox.Text <> "" Then
                Dim dbConnection = DbConnectionFactory.PracticeRepository()
                Dim dbCommand = dbConnection.CreateCommand()
                dbCommand.CommandText = "SELECT * FROM PatientDemographics"
                Dim criteriaString As String = " WHERE"
                Dim hasCriteria As Boolean = False
                If LastNameTextBox.Text <> "" Then
                    If hasCriteria Then
                        criteriaString += " AND"
                    End If
                    criteriaString += " LastName LIKE '" + LastNameTextBox.Text + "%'"
                    hasCriteria = True
                End If
                If FirstNameTextBox.Text <> "" Then
                    If hasCriteria Then
                        criteriaString += " AND"
                    End If
                    criteriaString += " FirstName LIKE '" + FirstNameTextBox.Text + "%'"
                    hasCriteria = True
                End If
                If hasCriteria Then
                    dbCommand.CommandText += criteriaString
                End If
                dbCommand.CommandText += " ORDER BY LastName"
                dbConnection.Open()
                If dbConnection.State = ConnectionState.Open Then
                    Dim reader = CType(dbCommand.ExecuteReader(), DataTableReader)
                    If Reader.HasRows Then
                        While (Reader.Read)
                            PatientsGrid.Rows.Add(Reader("PatientId"), Reader("LastName"), Reader("FirstName"))
                        End While
                        PatientsGrid.Rows(0).Selected = True
                    End If
                    Reader.Close()
                    dbConnection.Close()
                End If
            Else
                InteractionManager.Current.Alert("Invalid Entry.")
            End If
        End Sub

        Private Sub PatientsGridCellDoubleClick(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) _
            Handles PatientsGrid.CellDoubleClick
            If e.RowIndex >= 0 Then
                Integer.TryParse(PatientsGrid.Rows(e.RowIndex).Cells("IdColumn").Value.ToString(), PatientId)
                InteractionContext.Complete(True)
            End If
        End Sub

        Private Sub PatientsGridCellPainting(ByVal sender As Object, ByVal e As DataGridViewCellPaintingEventArgs) _
            Handles PatientsGrid.CellPainting
            If PatientsGrid.SelectedCells.Count > 0 Then
                PatientsGrid.Rows(PatientsGrid.SelectedCells(0).RowIndex).Selected = True
            End If
        End Sub
    End Class
End Namespace