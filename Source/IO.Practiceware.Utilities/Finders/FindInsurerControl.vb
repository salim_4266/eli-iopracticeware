Imports System.Windows.Forms
Imports IO.Practiceware.Data

Namespace Finders
    Public Class FindInsurerControl
        Public InsurerId As Integer
        Public InsurerName As String
        Public InsurerPlanName As String
        Public InsurerAddress As String
        Public InsurerCity As String
        Public InsurerState As String
        Public InsurerZip As String

        Private Sub FindButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles FindButton.Click
            InsurersGrid.Rows.Clear()
            Dim dbConnection = DbConnectionFactory.PracticeRepository()
            Dim dbCommand = dbConnection.CreateCommand()
            dbCommand.CommandText = "SELECT * FROM PracticeInsurers"
            Dim criteriaString As String = " WHERE"
            Dim hasCriteria As Boolean = False
            If NameTextBox.Text <> "" Then
                If hasCriteria Then
                    criteriaString += " AND"
                End If
                criteriaString += " InsurerName LIKE '" + NameTextBox.Text + "%'"
                hasCriteria = True
            End If
            If hasCriteria Then
                dbCommand.CommandText += criteriaString
            End If
            dbCommand.CommandText += " ORDER BY InsurerName"
            dbConnection.Open()
            If dbConnection.State = ConnectionState.Open Then
                Dim reader = CType(dbCommand.ExecuteReader(), DataTableReader)
                If reader.HasRows Then
                    While (reader.Read)
                        InsurersGrid.Rows.Add(reader("InsurerId"), reader("InsurerName"), reader("InsurerPlanName"),
                                              reader("InsurerAddress"), reader("InsurerCity"), reader("InsurerState"),
                                              reader("InsurerZip"))
                    End While
                    InsurersGrid.Rows(0).Selected = True
                End If
                reader.Close()
                dbConnection.Close()
            End If
        End Sub

        Private Sub InsurersGridCellDoubleClick(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) _
            Handles InsurersGrid.CellDoubleClick
            If e.RowIndex >= 0 Then
                Integer.TryParse(InsurersGrid.Rows(e.RowIndex).Cells("IdColumn").Value.ToString(), InsurerId)
                InsurerName = InsurersGrid.Rows(e.RowIndex).Cells("NameColumn").Value.ToString()
                InsurerPlanName = InsurersGrid.Rows(e.RowIndex).Cells("PlanColumn").Value.ToString()
                InsurerAddress = InsurersGrid.Rows(e.RowIndex).Cells("AddressColumn").Value.ToString()
                InsurerCity = InsurersGrid.Rows(e.RowIndex).Cells("CityColumn").Value.ToString()
                InsurerState = InsurersGrid.Rows(e.RowIndex).Cells("StateColumn").Value.ToString()
                InsurerZip = InsurersGrid.Rows(e.RowIndex).Cells("ZipColumn").Value.ToString()
                InteractionContext.Complete(True)
            End If
        End Sub

        Private Sub InsurersGridCellPainting(ByVal sender As Object, ByVal e As DataGridViewCellPaintingEventArgs) _
            Handles InsurersGrid.CellPainting
            If InsurersGrid.SelectedCells.Count > 0 Then
                InsurersGrid.Rows(InsurersGrid.SelectedCells(0).RowIndex).Selected = True
            End If
        End Sub
    End Class
End Namespace