Namespace Finders
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class FindInsurerControl
        Inherits Soaf.Presentation.Controls.WindowsForms.UserControl

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.NameLabel = New System.Windows.Forms.Label()
            Me.NameTextBox = New System.Windows.Forms.TextBox()
            Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
            Me.InsurersGrid = New System.Windows.Forms.DataGridView()
            Me.IdColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.NameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.PlanColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.AddressColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.CityColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.StateColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.ZipColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.FindButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.TableLayoutPanel1.SuspendLayout()
            CType(Me.InsurersGrid, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'NameLabel
            '
            Me.NameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.NameLabel.AutoSize = True
            Me.NameLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.NameLabel.Location = New System.Drawing.Point(3, 12)
            Me.NameLabel.Name = "NameLabel"
            Me.NameLabel.Size = New System.Drawing.Size(36, 13)
            Me.NameLabel.TabIndex = 0
            Me.NameLabel.Text = "Name"
            '
            'NameTextBox
            '
            Me.NameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Right
            Me.NameTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.NameTextBox.Location = New System.Drawing.Point(70, 7)
            Me.NameTextBox.Name = "NameTextBox"
            Me.NameTextBox.Size = New System.Drawing.Size(161, 22)
            Me.NameTextBox.TabIndex = 2
            '
            'TableLayoutPanel1
            '
            Me.TableLayoutPanel1.ColumnCount = 2
            Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.04762!))
            Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.95238!))
            Me.TableLayoutPanel1.Controls.Add(Me.NameLabel, 0, 0)
            Me.TableLayoutPanel1.Controls.Add(Me.NameTextBox, 1, 0)
            Me.TableLayoutPanel1.Location = New System.Drawing.Point(12, 12)
            Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
            Me.TableLayoutPanel1.RowCount = 1
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
            Me.TableLayoutPanel1.Size = New System.Drawing.Size(234, 37)
            Me.TableLayoutPanel1.TabIndex = 4
            '
            'InsurersGrid
            '
            Me.InsurersGrid.AllowUserToAddRows = False
            Me.InsurersGrid.AllowUserToDeleteRows = False
            Me.InsurersGrid.AllowUserToResizeColumns = False
            Me.InsurersGrid.AllowUserToResizeRows = False
            Me.InsurersGrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
            Me.InsurersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
            Me.InsurersGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdColumn, Me.NameColumn, Me.PlanColumn, Me.AddressColumn, Me.CityColumn, Me.StateColumn, Me.ZipColumn})
            Me.InsurersGrid.Location = New System.Drawing.Point(12, 78)
            Me.InsurersGrid.MultiSelect = False
            Me.InsurersGrid.Name = "InsurersGrid"
            Me.InsurersGrid.ReadOnly = True
            Me.InsurersGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.InsurersGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
            Me.InsurersGrid.Size = New System.Drawing.Size(638, 150)
            Me.InsurersGrid.TabIndex = 5
            '
            'IdColumn
            '
            Me.IdColumn.HeaderText = "Id"
            Me.IdColumn.Name = "IdColumn"
            Me.IdColumn.ReadOnly = True
            Me.IdColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.IdColumn.Visible = False
            Me.IdColumn.Width = 5
            '
            'NameColumn
            '
            Me.NameColumn.HeaderText = "Name"
            Me.NameColumn.Name = "NameColumn"
            Me.NameColumn.ReadOnly = True
            Me.NameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.NameColumn.Width = 150
            '
            'PlanColumn
            '
            Me.PlanColumn.HeaderText = "Plan"
            Me.PlanColumn.Name = "PlanColumn"
            Me.PlanColumn.ReadOnly = True
            Me.PlanColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            '
            'AddressColumn
            '
            Me.AddressColumn.HeaderText = "Address"
            Me.AddressColumn.Name = "AddressColumn"
            Me.AddressColumn.ReadOnly = True
            Me.AddressColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.AddressColumn.Width = 150
            '
            'CityColumn
            '
            Me.CityColumn.HeaderText = "City"
            Me.CityColumn.Name = "CityColumn"
            Me.CityColumn.ReadOnly = True
            Me.CityColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            '
            'StateColumn
            '
            Me.StateColumn.HeaderText = "State"
            Me.StateColumn.Name = "StateColumn"
            Me.StateColumn.ReadOnly = True
            Me.StateColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            '
            'ZipColumn
            '
            Me.ZipColumn.HeaderText = "Zip"
            Me.ZipColumn.Name = "ZipColumn"
            Me.ZipColumn.ReadOnly = True
            Me.ZipColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            '
            'FindButton
            '
            Me.FindButton.Location = New System.Drawing.Point(575, 10)
            Me.FindButton.Name = "FindButton"
            Me.FindButton.Size = New System.Drawing.Size(75, 38)
            Me.FindButton.TabIndex = 6
            Me.FindButton.Text = "Find"
            '
            'FindInsurerControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)

            Me.ClientSize = New System.Drawing.Size(663, 259)
            Me.Controls.Add(Me.FindButton)
            Me.Controls.Add(Me.InsurersGrid)
            Me.Controls.Add(Me.TableLayoutPanel1)
            Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me.Name = "FindInsurerForm"
            Me.Text = "Find Insurer"
            Me.Controls.SetChildIndex(Me.TableLayoutPanel1, 0)
            Me.Controls.SetChildIndex(Me.InsurersGrid, 0)
            Me.Controls.SetChildIndex(Me.FindButton, 0)
            Me.TableLayoutPanel1.ResumeLayout(False)
            Me.TableLayoutPanel1.PerformLayout()
            CType(Me.InsurersGrid, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents NameLabel As System.Windows.Forms.Label
        Friend WithEvents NameTextBox As System.Windows.Forms.TextBox
        Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
        Friend WithEvents InsurersGrid As System.Windows.Forms.DataGridView
        Friend WithEvents FindButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents IdColumn As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents NameColumn As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents PlanColumn As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents AddressColumn As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents CityColumn As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents StateColumn As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents ZipColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    End Class
End Namespace