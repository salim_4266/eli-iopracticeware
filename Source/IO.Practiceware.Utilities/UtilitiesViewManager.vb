﻿Imports IO.Practiceware.HashingComparer.HashingComparer
Imports IO.Practiceware.Encryption_Decryption.Encryption_Decryption
Imports ClinicalDecisions.ClinicalDecisions
Imports ClinicalIntegration
Imports IO.Practiceware.Application
Imports IO.Practiceware.Utilities.PatientImageDirectoryConverter
Imports IO.Practiceware.Configuration
Imports IO.Practiceware.Utilities.RequeueClaims
Imports IO.Practiceware.Model
Imports Soaf.Presentation
Imports Soaf.Collections
Imports Soaf.ComponentModel
Imports System.Windows


Public Class UtilitiesViewManager
    Public Shared Sub OpenRequeueClaims()
        Dim control As New MainControl
        InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = control, .ResizeMode = ResizeMode.NoResize})
    End Sub

    Public Shared Sub OpenRequeueRecalls()
        Dim control As New RequeueRecalls.MainControl
        InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = control, .ResizeMode = ResizeMode.NoResize})
    End Sub


    Public Shared Sub OpenMergePatients()
        If Not (PermissionId.MergePatients.EnsurePermission()) Then Exit Sub

        Dim control As New PatientMerge.MainControl
        InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = control, .ResizeMode = ResizeMode.NoResize})
    End Sub

    Public Shared Sub OpenUpdoxConfiguration()
        Dim frm As New frmUpdoxConfiguration
        frm.DBObject = Data.DbConnectionFactory.PracticeRepository
        frm.ShowDialog()
    End Sub
    Public Shared Sub OpenDrFirstConfiguration()
        Dim frm As New frmDrFirstConfiguration
        frm.DBObject = Data.DbConnectionFactory.PracticeRepository
        frm.ShowDialog()
    End Sub
    Public Shared Sub OpenRCPConfiguration()
        Dim frm As New frmACIConfiguration
        frm.DBObject = Data.DbConnectionFactory.PracticeRepository
        frm.ShowDialog()
    End Sub

    Public Shared Sub OpenCredentialConfirmation(PatientId As String, PracticeToken As String, UserName As String,
                                                 Password As String, PatientName As String, UserType As String, IsEmailAddress As Boolean)
        Dim frm As New frmERPPatientConfirmation
        frm.DBObject = Data.DbConnectionFactory.PracticeRepository
        frm._externalId = PatientId
        frm._practiceToken = PracticeToken
        frm._userName = UserName
        frm._password = Password
        frm._patientName = PatientName
        frm._userType = UserType
        frm._isemailaddress = IsEmailAddress
        frm.ShowDialog()
    End Sub

    Public Shared Sub OpenMigratePatientChart()
        Dim control As New MigratePatientChart.MainControl
        InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = control, .ResizeMode = ResizeMode.NoResize})
    End Sub

    Public Shared Sub OpenClinDesc()
        If Not (PermissionId.ClinicalDecisions.EnsurePermission()) Then Exit Sub

        Dim frm As New frmInferenceEngine
        Dim dbHelper As New ClinicalDecisions.ClinicalDecisions.DbHelper()
        dbHelper.DbObject = Data.DbConnectionFactory.PracticeRepository
        frm.MDBFilePath = ConfigurationManager.ApplicationDataPath
        frm.OLEDbConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="
        If Not UserContext.Current.UserDetails Is Nothing Then
            frm.UserId = UserContext.Current.UserDetails.Id
        End If
        frm.ShowDialog()
        frm.Dispose()
    End Sub

    Public Shared Sub OpenIntegrity()
        Dim frm As New ClinicalIntegration.frmDataIntegration
        frm.ProviderId = Convert.ToString(UserContext.Current.UserDetails.Id)
        frm.DBObject = Data.DbConnectionFactory.PracticeRepository
        frm.ShowDialog()
        frm.Dispose()
    End Sub



    Public Shared Sub OpenErxNotification(SSOContext As String)
        Dim frm As New ClinicalIntegration.frmEPrescriptionStatus

        'frm.userId = Convert.ToString(UserContext.Current.UserDetails.Id)
        frm.IDBCon = Data.DbConnectionFactory.PracticeRepository
        frm.SSOContext = SSOContext
        frm.ShowDialog()
        frm.Dispose()
    End Sub
    Public Shared Sub OpenEncryption()
        Dim form As New FrmEncryptDecrypt
        form.ShowDialog()
        form.Dispose()
    End Sub

    Public Shared Sub OpenCms()
        Dim frm As New PQRI.PQRI.frmCMSReport
        Dim dbHelper As New PQRI.PQRI.DbHelper()
        dbHelper.DbObject = Data.DbConnectionFactory.PracticeRepository
        frm.RepType = "CMS"
        frm.CSVFilePath = ConfigurationManager.ServerDataPath
        frm.ShowDialog()
        frm.Dispose()
    End Sub

    Public Shared Sub OpenPqrs()
        Dim frm As New PQRI.PQRI.frmCMSReport
        Dim dbHelper As New PQRI.PQRI.DbHelper()
        dbHelper.DbObject = Data.DbConnectionFactory.PracticeRepository
        frm.RepType = "PQRI"
        If (ConfigurationManager.ClientApplicationConfiguration.RdsWithSharedAccountMode) Then
            frm.CSVFilePath = "\\tsclient\C"
        ElseIf (ConfigurationManager.ApplicationServerClientConfiguration.RemoteFileService) Then
            frm.CSVFilePath = "C:"
        Else
            frm.CSVFilePath = ConfigurationManager.ServerDataPath
        End If
        frm.ShowDialog()
        frm.Dispose()
    End Sub

    Public Shared Sub OpenFileconversion()
        Dim control As New FileConversionAgreementControl
        InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = control, .ResizeMode = ResizeMode.NoResize})
    End Sub

    Public Shared Sub OpenSpexupcImport()
        Dim control As New SPEXUPCImport.MainControl
        InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = control, .ResizeMode = ResizeMode.NoResize})
    End Sub

End Class


Public Module PermissionExtensions

    ''' <summary>
    ''' Ensures that user has specified permission.
    ''' Will show a UI alert if user doesn't have the permission
    ''' </summary>
    ''' <param name="permission">The permission.</param>
    ''' <param name="alertNoPermission">if set to <c>true</c> displays UI alert of user permission missing.</param>
    ''' <returns></returns>
    <System.Runtime.CompilerServices.Extension>
    Public Function EnsurePermission(permission As PermissionId, Optional alertNoPermission As Boolean = True) As Boolean
        Dim errorMessages = permission.Validate()
        If errorMessages.Any() Then
            If alertNoPermission Then
                Dim alertMessage = errorMessages.Join(Function(x) x.Value, Environment.NewLine)
                InteractionManager.Current.Alert(alertMessage)
            End If
            Return False
        End If

        Return True
    End Function
End Module