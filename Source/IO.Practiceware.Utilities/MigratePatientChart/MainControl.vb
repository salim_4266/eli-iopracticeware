Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text.RegularExpressions
Imports IO.Practiceware.Configuration
Imports IO.Practiceware.Data
Imports Soaf.Presentation
Imports Soaf.Logging
Imports IO.Practiceware.Storage
Imports LogCategory = IO.Practiceware.Logging.LogCategory

Namespace MigratePatientChart
    Public Class MainControl

        Public MigrateFromPatientAndAppointment As Integer() = Nothing
        Public MigrateToPatientAndAppointment As Integer() = Nothing
        Private _logger As ILogger
        Private Property logManager() As ILogManager

        Private Sub MigratePatientChartMainControl_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            MigrateFromPatientDetailsBox.Visible = False
            MigrateToPatientDetailsBox.Visible = False
            MigrateFromAppointmentLabel.Visible = False
            MigrateFromAppointmentsGrid.Visible = False
            MigrateToAppointmentLabel.Visible = False
            MigrateToAppointmentsGrid.Visible = False
        End Sub

        Private Sub MigrateFromSelectButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MigrateFromSelectButton.Click
            MigrateFromPatientAndAppointment = Nothing
            MigrateFromPatientDetailsBox.Visible = False
            MigrateFromAppointmentLabel.Visible = False
            MigrateFromAppointmentsGrid.Visible = False
            Dim PatientId As Integer
            If Integer.TryParse(MigrateFromPatientIdInputTextBox.Text, PatientId) Then
                Try
                    Dim dbConnection = DbConnectionFactory.PracticeRepository()
                    DbConnection.Open()
                    If DbConnection.State = ConnectionState.Open Then
                        Dim dbCommand = dbConnection.CreateCommand()
                        dbCommand.CommandText = "SELECT * FROM PatientDemographics WHERE PatientId = " + PatientId.ToString()
                        Dim reader = CType(dbCommand.ExecuteReader(), DataTableReader)
                        If Reader.HasRows Then
                            Reader.Read()
                            MigrateFromPatientDetailsBox.Visible = True
                            MigrateFromPatientIdValueLabel.Text = Reader("PatientId").ToString()
                            MigrateFromNameValueLabel.Text = Reader("FirstName").ToString() + " " + Reader("LastName").ToString()
                            MigrateFromSSNValueLabel.Text = Reader("SocialSecurity").ToString()
                            MigrateFromAddressValueLabel.Text = Reader("Address").ToString() + " " + Reader("Suite").ToString() + Environment.NewLine + Reader("City").ToString() + ", " + Reader("State").ToString() + " " + Reader("Zip").ToString()
                            MigrateFromPatientAndAppointment = New Integer() {PatientId, 0}
                        Else
                            InteractionManager.Current.Alert("Patient not found.")
                        End If
                        Reader.Close()
                        If Not MigrateFromPatientAndAppointment Is Nothing Then
                            MigrateFromAppointmentLabel.Visible = True
                            MigrateFromAppointmentsGrid.Visible = True
                            DbCommand.CommandText = "SELECT * FROM Appointments INNER JOIN AppointmentType ON Appointments.AppTypeId = AppointmentType.AppTypeId WHERE PatientId = " + PatientId.ToString()
                            reader = CType(dbCommand.ExecuteReader(), DataTableReader)
                            While (Reader.Read)
                                MigrateFromAppointmentsGrid.Rows.Add(Reader("AppointmentId"), Reader("AppDate"), Reader("AppTime"), Reader("AppointmentType"), Reader("ScheduleStatus"))
                            End While
                        End If
                        Reader.Close()
                        DbConnection.Close()
                    End If
                Catch ex As Exception
                    Trace.TraceError(ex.ToString())
                End Try
            Else
                InteractionManager.Current.Alert("Invalid Entry.")
            End If
        End Sub

        Private Sub MigrateToSelectButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MigrateToSelectButton.Click
            MigrateToPatientAndAppointment = Nothing
            MigrateToPatientDetailsBox.Visible = False
            Dim PatientId As Integer
            If Integer.TryParse(MigrateToPatientIdInputTextBox.Text, PatientId) Then
                Try
                    Dim dbConnection = DbConnectionFactory.PracticeRepository()
                    DbConnection.Open()
                    If DbConnection.State = ConnectionState.Open Then
                        Dim dbCommand = dbConnection.CreateCommand()
                        dbCommand.CommandText = "SELECT * FROM PatientDemographics WHERE PatientId = " + PatientId.ToString()
                        Dim reader = CType(dbCommand.ExecuteReader(), DataTableReader)
                        If Reader.HasRows Then
                            Reader.Read()
                            MigrateToPatientDetailsBox.Visible = True
                            MigrateToPatientIdValueLabel.Text = Reader("PatientId").ToString()
                            MigrateToNameValueLabel.Text = Reader("FirstName").ToString() + " " + Reader("LastName").ToString()
                            MigrateToSSNValueLabel.Text = Reader("SocialSecurity").ToString()
                            MigrateToAddressValueLabel.Text = Reader("Address").ToString() + " " + Reader("Suite").ToString() + Environment.NewLine + Reader("City").ToString() + ", " + Reader("State").ToString() + " " + Reader("Zip").ToString()
                            MigrateToPatientAndAppointment = New Integer() {PatientId, 0}
                        Else
                            InteractionManager.Current.Alert("Patient not found.")
                        End If
                        Reader.Close()
                        If Not MigrateToPatientAndAppointment Is Nothing Then
                            MigrateToAppointmentLabel.Visible = True
                            MigrateToAppointmentsGrid.Visible = True
                            DbCommand.CommandText = "SELECT * FROM Appointments INNER JOIN AppointmentType ON Appointments.AppTypeId = AppointmentType.AppTypeId WHERE PatientId = " + PatientId.ToString()
                            reader = CType(dbCommand.ExecuteReader(), DataTableReader)
                            While (Reader.Read)
                                MigrateToAppointmentsGrid.Rows.Add(Reader("AppointmentId"), Reader("AppDate"), Reader("AppTime"), Reader("AppointmentType"), Reader("ScheduleStatus"))
                            End While
                        End If
                        Reader.Close()
                        DbConnection.Close()
                    End If
                Catch ex As Exception
                    Trace.TraceError(ex.ToString())
                End Try
            Else
                InteractionManager.Current.Alert("Invalid Entry.")
            End If
        End Sub

        Private Sub MigrateFromFindButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MigrateFromFindButton.Click
            Dim control As New Finders.FindPatientControl
            InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = control, .ResizeMode = Windows.ResizeMode.NoResize})
            If control.PatientId > 0 Then
                MigrateFromPatientIdInputTextBox.Text = control.PatientId.ToString()
                MigrateFromSelectButton_Click(sender, e)
            End If
        End Sub

        Private Sub MigrateToFindButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MigrateToFindButton.Click
            Dim control As New Finders.FindPatientControl
            InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = control, .ResizeMode = Windows.ResizeMode.NoResize})
            If control.PatientId > 0 Then
                MigrateToPatientIdInputTextBox.Text = control.PatientId.ToString()
                MigrateToSelectButton_Click(sender, e)
            End If
        End Sub

        Private Sub MigrateFromAppointmentsGrid_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles MigrateFromAppointmentsGrid.CellPainting
            If MigrateFromAppointmentsGrid.Rows.Count > 0 Then
                If MigrateFromAppointmentsGrid.SelectedCells.Count > 0 Then
                    MigrateFromAppointmentsGrid.Rows(MigrateFromAppointmentsGrid.SelectedCells(0).RowIndex).Selected = True
                Else
                    MigrateFromAppointmentsGrid.Rows(0).Selected = True
                End If
                MigrateFromPatientAndAppointment(1) = CType(MigrateFromAppointmentsGrid.Rows(MigrateFromAppointmentsGrid.SelectedCells(0).RowIndex).Cells("MigrateFromAppointmentId").Value, Integer)
            End If
        End Sub

        Private Sub MigrateToAppointmentsGrid_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles MigrateToAppointmentsGrid.CellPainting
            If MigrateToAppointmentsGrid.Rows.Count > 0 Then
                If MigrateToAppointmentsGrid.SelectedCells.Count > 0 Then
                    MigrateToAppointmentsGrid.Rows(MigrateToAppointmentsGrid.SelectedCells(0).RowIndex).Selected = True
                Else
                    MigrateToAppointmentsGrid.Rows(0).Selected = True
                End If
                MigrateToPatientAndAppointment(1) = CType(MigrateToAppointmentsGrid.Rows(MigrateToAppointmentsGrid.SelectedCells(0).RowIndex).Cells("MigrateToAppointmentId").Value, Integer)
            End If
        End Sub

        Private Sub MigrateButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MigrateButton.Click
            If Not MigrateFromPatientAndAppointment Is Nothing And Not MigrateToPatientAndAppointment Is Nothing Then
                If InteractionManager.Current.Confirm("Are you sure you want to migrate this patient chart?") = True Then
                    'Perform database operations
                    Try
                        logManager = Bootstrapper.Run(Of ILogManager)()
                        _logger = logManager.GetCurrentTypeLogger()
                        Dim dbConnection = DbConnectionFactory.PracticeRepository()
                        DbConnection.Open()
                        If DbConnection.State = ConnectionState.Open Then
                            Dim dbCommand = dbConnection.CreateCommand()
                            dbCommand.CommandText = "UPDATE PatientClinical SET PatientId = " + MigrateToPatientAndAppointment(0).ToString() + ", AppointmentId = " + MigrateToPatientAndAppointment(1).ToString() + " WHERE PatientId = " + MigrateFromPatientAndAppointment(0).ToString() + " AND AppointmentId = " + MigrateFromPatientAndAppointment(1).ToString
                            DbCommand.ExecuteNonQuery()
                            DbCommand.CommandText = "UPDATE PatientNotes SET PatientId = " + MigrateToPatientAndAppointment(0).ToString() + ", AppointmentId = " + MigrateToPatientAndAppointment(1).ToString() + " WHERE PatientId = " + MigrateFromPatientAndAppointment(0).ToString() + " AND AppointmentId = " + MigrateFromPatientAndAppointment(1).ToString
                            DbCommand.ExecuteNonQuery()
                            DbCommand.CommandText = "UPDATE PatientReceivables SET PatientId = " + MigrateToPatientAndAppointment(0).ToString() + ", AppointmentId = " + MigrateToPatientAndAppointment(1).ToString() + ", Invoice = '" + MigrateToPatientAndAppointment(0).ToString() + "-" + MigrateToPatientAndAppointment(1).ToString().PadLeft(5, "0"c) + "' WHERE PatientId = " + MigrateFromPatientAndAppointment(0).ToString() + " AND AppointmentId = " + MigrateFromPatientAndAppointment(1).ToString() + " AND Invoice = '" + MigrateToPatientAndAppointment(0).ToString() + "-" + MigrateToPatientAndAppointment(1).ToString.PadLeft(5, "0"c) + "'"
                            DbCommand.ExecuteNonQuery()
                            DbCommand.CommandText = "UPDATE PracticeActivity SET PatientId = " + MigrateToPatientAndAppointment(0).ToString() + ", AppointmentId = " + MigrateToPatientAndAppointment(1).ToString() + " WHERE PatientId = " + MigrateFromPatientAndAppointment(0).ToString() + " AND AppointmentId = " + MigrateFromPatientAndAppointment(1).ToString
                            DbCommand.ExecuteNonQuery()
                            DbConnection.Close()
                            For Each FileName As String In Directory.GetFiles(ConfigurationManager.ServerDataPath + "\MyScan")
                                Dim expression As New Regex("(.+?-_" + MigrateFromPatientAndAppointment(1).ToString() + "_" + MigrateFromPatientAndAppointment(0).ToString() + "-.+?-.+?\..+?)")
                                If expression.IsMatch(FileName) Then
                                    If FileManager.Instance.FileExists(FileName) Then
                                        FileManager.Instance.Move(FileName, FileName.Replace("-_" + MigrateFromPatientAndAppointment(1).ToString() + "_" + MigrateFromPatientAndAppointment(0).ToString() + "-", "-_" + MigrateToPatientAndAppointment(1).ToString() + "_" + MigrateToPatientAndAppointment(0).ToString() + "-"))
                                    End If
                                End If
                            Next
                            For Each FileName As String In Directory.GetFiles(ConfigurationManager.ServerDataPath + "\MyImages")
                                Dim expression As New Regex("(.+?-_" + MigrateFromPatientAndAppointment(1).ToString() + "_" + MigrateFromPatientAndAppointment(0).ToString() + "-.+?-.+?\..+?)")
                                If expression.IsMatch(FileName) Then
                                    If FileManager.Instance.FileExists(FileName) Then
                                        FileManager.Instance.Move(FileName, FileName.Replace("-_" + MigrateFromPatientAndAppointment(1).ToString() + "_" + MigrateFromPatientAndAppointment(0).ToString() + "-", "-_" + MigrateToPatientAndAppointment(1).ToString() + "_" + MigrateToPatientAndAppointment(0).ToString() + "-"))
                                    End If
                                End If
                            Next
                            _logger.Log("Migrated Patient Records", categories:={LogCategory.Utilities})
                            InteractionManager.Current.Alert("Done migrating.")
                        End If
                    Catch ex As Exception
                        Trace.TraceError(ex.ToString())
                    End Try
                End If
            Else
                InteractionManager.Current.Alert("Please complete the form.")
            End If
        End Sub
    End Class
End Namespace