Namespace MigratePatientChart
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MainControl
        Inherits Soaf.Presentation.Controls.WindowsForms.UserControl

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.MigrateButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.Button4 = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.Button3 = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.GroupBox1 = New System.Windows.Forms.GroupBox()
            Me.GroupBox4 = New System.Windows.Forms.GroupBox()
            Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
            Me.MigrateFromPatientIdInputLabel = New System.Windows.Forms.Label()
            Me.MigrateFromPatientIdInputTextBox = New System.Windows.Forms.TextBox()
            Me.MigrateFromAppointmentLabel = New System.Windows.Forms.Label()
            Me.MigrateFromAppointmentsGrid = New System.Windows.Forms.DataGridView()
            Me.MigrateFromAppointmentStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.MigrateFromAppointmentType = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.MigrateFromAppointmentTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.MigrateFromAppointmentDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.MigrateFromAppointmentId = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.MigrateFromBox = New System.Windows.Forms.GroupBox()
            Me.Label2 = New System.Windows.Forms.Label()
            Me.MigrateFromPatientDetailsBox = New System.Windows.Forms.GroupBox()
            Me.Label1 = New System.Windows.Forms.Label()
            Me.MigrateFromSSNValueLabel = New System.Windows.Forms.Label()
            Me.MigrateFromNameValueLabel = New System.Windows.Forms.Label()
            Me.MigrateFromNameLabel = New System.Windows.Forms.Label()
            Me.MigrateFromSSNLabel = New System.Windows.Forms.Label()
            Me.MigrateFromPatientDetailsPanel = New System.Windows.Forms.TableLayoutPanel()
            Me.MigrateFromPatientIdLabel = New System.Windows.Forms.Label()
            Me.MigrateFromPatientIdValueLabel = New System.Windows.Forms.Label()
            Me.MigrateFromAddressLabel = New System.Windows.Forms.Label()
            Me.MigrateToPatientIdInputLabel = New System.Windows.Forms.Label()
            Me.MigrateToPatientIdInputTextBox = New System.Windows.Forms.TextBox()
            Me.MigrateToAppointmentLabel = New System.Windows.Forms.Label()
            Me.MigrateToPatientDetailsBox = New System.Windows.Forms.GroupBox()
            Me.MigrateToPatientDetailsPanel = New System.Windows.Forms.TableLayoutPanel()
            Me.MigrateToSSNValueLabel = New System.Windows.Forms.Label()
            Me.MigrateToNameValueLabel = New System.Windows.Forms.Label()
            Me.MigrateToNameLabel = New System.Windows.Forms.Label()
            Me.MigrateToSSNLabel = New System.Windows.Forms.Label()
            Me.MigrateToPatientIdLabel = New System.Windows.Forms.Label()
            Me.MigrateToPatientIdValueLabel = New System.Windows.Forms.Label()
            Me.MigrateToAddressLabel = New System.Windows.Forms.Label()
            Me.MigrateToAddressValueLabel = New System.Windows.Forms.Label()
            Me.MigrateToSelectButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.MigrateToFindButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.MigrateToAppointmentsGrid = New System.Windows.Forms.DataGridView()
            Me.MigrateToAppointmentStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.MigrateToAppointmentType = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.MigrateToAppointmentTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.MigrateToAppointmentDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.MigrateToAppointmentId = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.MigrateToBox = New System.Windows.Forms.GroupBox()
            Me.MigrateFromFindButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.MigrateFromAddressValueLabel = New System.Windows.Forms.Label()
            Me.MigrateFromSelectButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.GroupBox1.SuspendLayout()
            Me.TableLayoutPanel2.SuspendLayout()
            CType(Me.MigrateFromAppointmentsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.MigrateFromBox.SuspendLayout()
            Me.MigrateFromPatientDetailsBox.SuspendLayout()
            Me.MigrateFromPatientDetailsPanel.SuspendLayout()
            Me.MigrateToPatientDetailsBox.SuspendLayout()
            Me.MigrateToPatientDetailsPanel.SuspendLayout()
            CType(Me.MigrateToAppointmentsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.MigrateToBox.SuspendLayout()
            Me.SuspendLayout()
            '
            'MigrateButton
            '
            Me.MigrateButton.Location = New System.Drawing.Point(770, 426)
            Me.MigrateButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateButton.Name = "MigrateButton"
            Me.MigrateButton.Size = New System.Drawing.Size(87, 44)
            Me.MigrateButton.TabIndex = 7
            Me.MigrateButton.Text = "Migrate"
            Me.MigrateButton.Tooltip = Nothing
            '
            'Button4
            '
            Me.Button4.Location = New System.Drawing.Point(251, 43)
            Me.Button4.Name = "Button4"
            Me.Button4.Size = New System.Drawing.Size(85, 31)
            Me.Button4.TabIndex = 5
            Me.Button4.Text = "Select"
            Me.Button4.Tooltip = Nothing
            '
            'Button3
            '
            Me.Button3.Location = New System.Drawing.Point(342, 43)
            Me.Button3.Name = "Button3"
            Me.Button3.Size = New System.Drawing.Size(85, 31)
            Me.Button3.TabIndex = 6
            Me.Button3.Text = "Find"
            Me.Button3.Tooltip = Nothing
            '
            'GroupBox1
            '
            Me.GroupBox1.Controls.Add(Me.Button3)
            Me.GroupBox1.Controls.Add(Me.Button4)
            Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(200, 100)
            Me.GroupBox1.TabIndex = 0
            Me.GroupBox1.TabStop = False
            '
            'GroupBox4
            '
            Me.GroupBox4.Location = New System.Drawing.Point(0, 0)
            Me.GroupBox4.Name = "GroupBox4"
            Me.GroupBox4.Size = New System.Drawing.Size(200, 100)
            Me.GroupBox4.TabIndex = 0
            Me.GroupBox4.TabStop = False
            '
            'TableLayoutPanel2
            '
            Me.TableLayoutPanel2.ColumnCount = 4
            Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.85185!))
            Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.14815!))
            Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 121.0!))
            Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 108.0!))
            Me.TableLayoutPanel2.Controls.Add(Me.Label1, 3, 0)
            Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
            Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
            Me.TableLayoutPanel2.RowCount = 1
            Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
            Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
            Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
            Me.TableLayoutPanel2.Size = New System.Drawing.Size(200, 100)
            Me.TableLayoutPanel2.TabIndex = 0
            '
            'MigrateFromPatientIdInputLabel
            '
            Me.MigrateFromPatientIdInputLabel.AutoSize = True
            Me.MigrateFromPatientIdInputLabel.Location = New System.Drawing.Point(15, 37)
            Me.MigrateFromPatientIdInputLabel.Name = "MigrateFromPatientIdInputLabel"
            Me.MigrateFromPatientIdInputLabel.Size = New System.Drawing.Size(52, 13)
            Me.MigrateFromPatientIdInputLabel.TabIndex = 0
            Me.MigrateFromPatientIdInputLabel.Text = "Patient Id"
            '
            'MigrateFromPatientIdInputTextBox
            '
            Me.MigrateFromPatientIdInputTextBox.Location = New System.Drawing.Point(85, 34)
            Me.MigrateFromPatientIdInputTextBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromPatientIdInputTextBox.Name = "MigrateFromPatientIdInputTextBox"
            Me.MigrateFromPatientIdInputTextBox.Size = New System.Drawing.Size(97, 20)
            Me.MigrateFromPatientIdInputTextBox.TabIndex = 1
            '
            'MigrateFromAppointmentLabel
            '
            Me.MigrateFromAppointmentLabel.AutoSize = True
            Me.MigrateFromAppointmentLabel.Location = New System.Drawing.Point(385, 37)
            Me.MigrateFromAppointmentLabel.Name = "MigrateFromAppointmentLabel"
            Me.MigrateFromAppointmentLabel.Size = New System.Drawing.Size(66, 13)
            Me.MigrateFromAppointmentLabel.TabIndex = 3
            Me.MigrateFromAppointmentLabel.Text = "Appointment"
            '
            'MigrateFromAppointmentsGrid
            '
            Me.MigrateFromAppointmentsGrid.AllowUserToAddRows = False
            Me.MigrateFromAppointmentsGrid.AllowUserToDeleteRows = False
            Me.MigrateFromAppointmentsGrid.AllowUserToResizeColumns = False
            Me.MigrateFromAppointmentsGrid.AllowUserToResizeRows = False
            Me.MigrateFromAppointmentsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
            Me.MigrateFromAppointmentsGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MigrateFromAppointmentId, Me.MigrateFromAppointmentDate, Me.MigrateFromAppointmentTime, Me.MigrateFromAppointmentType, Me.MigrateFromAppointmentStatus})
            Me.MigrateFromAppointmentsGrid.Location = New System.Drawing.Point(477, 33)
            Me.MigrateFromAppointmentsGrid.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromAppointmentsGrid.MultiSelect = False
            Me.MigrateFromAppointmentsGrid.Name = "MigrateFromAppointmentsGrid"
            Me.MigrateFromAppointmentsGrid.ReadOnly = True
            Me.MigrateFromAppointmentsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
            Me.MigrateFromAppointmentsGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.MigrateFromAppointmentsGrid.Size = New System.Drawing.Size(365, 139)
            Me.MigrateFromAppointmentsGrid.TabIndex = 2
            '
            'MigrateFromAppointmentStatus
            '
            Me.MigrateFromAppointmentStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
            Me.MigrateFromAppointmentStatus.HeaderText = "Status"
            Me.MigrateFromAppointmentStatus.MinimumWidth = 75
            Me.MigrateFromAppointmentStatus.Name = "MigrateFromAppointmentStatus"
            Me.MigrateFromAppointmentStatus.ReadOnly = True
            Me.MigrateFromAppointmentStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            '
            'MigrateFromAppointmentType
            '
            Me.MigrateFromAppointmentType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
            Me.MigrateFromAppointmentType.HeaderText = "Type"
            Me.MigrateFromAppointmentType.MinimumWidth = 75
            Me.MigrateFromAppointmentType.Name = "MigrateFromAppointmentType"
            Me.MigrateFromAppointmentType.ReadOnly = True
            Me.MigrateFromAppointmentType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.MigrateFromAppointmentType.Width = 75
            '
            'MigrateFromAppointmentTime
            '
            Me.MigrateFromAppointmentTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
            Me.MigrateFromAppointmentTime.HeaderText = "Time"
            Me.MigrateFromAppointmentTime.MinimumWidth = 75
            Me.MigrateFromAppointmentTime.Name = "MigrateFromAppointmentTime"
            Me.MigrateFromAppointmentTime.ReadOnly = True
            Me.MigrateFromAppointmentTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.MigrateFromAppointmentTime.Width = 75
            '
            'MigrateFromAppointmentDate
            '
            Me.MigrateFromAppointmentDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
            Me.MigrateFromAppointmentDate.HeaderText = "Date"
            Me.MigrateFromAppointmentDate.MinimumWidth = 75
            Me.MigrateFromAppointmentDate.Name = "MigrateFromAppointmentDate"
            Me.MigrateFromAppointmentDate.ReadOnly = True
            Me.MigrateFromAppointmentDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.MigrateFromAppointmentDate.Width = 75
            '
            'MigrateFromAppointmentId
            '
            Me.MigrateFromAppointmentId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
            Me.MigrateFromAppointmentId.HeaderText = "Id"
            Me.MigrateFromAppointmentId.Name = "MigrateFromAppointmentId"
            Me.MigrateFromAppointmentId.ReadOnly = True
            Me.MigrateFromAppointmentId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.MigrateFromAppointmentId.Visible = False
            '
            'MigrateFromBox
            '
            Me.MigrateFromBox.Controls.Add(Me.MigrateFromFindButton)
            Me.MigrateFromBox.Controls.Add(Me.MigrateFromSelectButton)
            Me.MigrateFromBox.Controls.Add(Me.MigrateFromPatientDetailsBox)
            Me.MigrateFromBox.Controls.Add(Me.MigrateFromAppointmentsGrid)
            Me.MigrateFromBox.Controls.Add(Me.MigrateFromAppointmentLabel)
            Me.MigrateFromBox.Controls.Add(Me.MigrateFromPatientIdInputTextBox)
            Me.MigrateFromBox.Controls.Add(Me.MigrateFromPatientIdInputLabel)
            Me.MigrateFromBox.Location = New System.Drawing.Point(15, 12)
            Me.MigrateFromBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromBox.Name = "MigrateFromBox"
            Me.MigrateFromBox.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromBox.Size = New System.Drawing.Size(847, 195)
            Me.MigrateFromBox.TabIndex = 7
            Me.MigrateFromBox.TabStop = False
            Me.MigrateFromBox.Text = "Migrate From..."
            '
            'Label2
            '
            Me.Label2.AutoSize = True
            Me.Label2.Location = New System.Drawing.Point(273, 0)
            Me.Label2.Name = "Label2"
            Me.Label2.Padding = New System.Windows.Forms.Padding(3)
            Me.Label2.Size = New System.Drawing.Size(75, 27)
            Me.Label2.TabIndex = 6
            Me.Label2.Text = "Address:"
            '
            'MigrateFromPatientDetailsBox
            '
            Me.MigrateFromPatientDetailsBox.Controls.Add(Me.MigrateFromPatientDetailsPanel)
            Me.MigrateFromPatientDetailsBox.Location = New System.Drawing.Point(19, 78)
            Me.MigrateFromPatientDetailsBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromPatientDetailsBox.Name = "MigrateFromPatientDetailsBox"
            Me.MigrateFromPatientDetailsBox.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromPatientDetailsBox.Size = New System.Drawing.Size(440, 102)
            Me.MigrateFromPatientDetailsBox.TabIndex = 4
            Me.MigrateFromPatientDetailsBox.TabStop = False
            Me.MigrateFromPatientDetailsBox.Text = "Patient Details"
            '
            'Label1
            '
            Me.Label1.AutoSize = True
            Me.Label1.Location = New System.Drawing.Point(96, 0)
            Me.Label1.Name = "Label1"
            Me.Label1.Padding = New System.Windows.Forms.Padding(3)
            Me.TableLayoutPanel2.SetRowSpan(Me.Label1, 3)
            Me.Label1.Size = New System.Drawing.Size(54, 19)
            Me.Label1.TabIndex = 7
            Me.Label1.Text = "Address:"
            '
            'MigrateFromSSNValueLabel
            '
            Me.MigrateFromSSNValueLabel.AutoSize = True
            Me.MigrateFromSSNValueLabel.Location = New System.Drawing.Point(74, 34)
            Me.MigrateFromSSNValueLabel.Name = "MigrateFromSSNValueLabel"
            Me.MigrateFromSSNValueLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromSSNValueLabel.Size = New System.Drawing.Size(6, 17)
            Me.MigrateFromSSNValueLabel.TabIndex = 5
            '
            'MigrateFromNameValueLabel
            '
            Me.MigrateFromNameValueLabel.AutoSize = True
            Me.MigrateFromNameValueLabel.Location = New System.Drawing.Point(74, 17)
            Me.MigrateFromNameValueLabel.Name = "MigrateFromNameValueLabel"
            Me.MigrateFromNameValueLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromNameValueLabel.Size = New System.Drawing.Size(6, 17)
            Me.MigrateFromNameValueLabel.TabIndex = 4
            '
            'MigrateFromNameLabel
            '
            Me.MigrateFromNameLabel.AutoSize = True
            Me.MigrateFromNameLabel.Location = New System.Drawing.Point(3, 17)
            Me.MigrateFromNameLabel.Name = "MigrateFromNameLabel"
            Me.MigrateFromNameLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromNameLabel.Size = New System.Drawing.Size(44, 17)
            Me.MigrateFromNameLabel.TabIndex = 0
            Me.MigrateFromNameLabel.Text = "Name:"
            '
            'MigrateFromSSNLabel
            '
            Me.MigrateFromSSNLabel.AutoSize = True
            Me.MigrateFromSSNLabel.Location = New System.Drawing.Point(3, 34)
            Me.MigrateFromSSNLabel.Name = "MigrateFromSSNLabel"
            Me.MigrateFromSSNLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromSSNLabel.Size = New System.Drawing.Size(38, 17)
            Me.MigrateFromSSNLabel.TabIndex = 2
            Me.MigrateFromSSNLabel.Text = "SSN:"
            '
            'MigrateFromPatientDetailsPanel
            '
            Me.MigrateFromPatientDetailsPanel.ColumnCount = 4
            Me.MigrateFromPatientDetailsPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.21053!))
            Me.MigrateFromPatientDetailsPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.78947!))
            Me.MigrateFromPatientDetailsPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
            Me.MigrateFromPatientDetailsPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 151.0!))
            Me.MigrateFromPatientDetailsPanel.Controls.Add(Me.MigrateFromAddressValueLabel, 3, 0)
            Me.MigrateFromPatientDetailsPanel.Controls.Add(Me.MigrateFromAddressLabel, 2, 0)
            Me.MigrateFromPatientDetailsPanel.Controls.Add(Me.MigrateFromPatientIdValueLabel, 1, 0)
            Me.MigrateFromPatientDetailsPanel.Controls.Add(Me.MigrateFromPatientIdLabel, 0, 0)
            Me.MigrateFromPatientDetailsPanel.Controls.Add(Me.MigrateFromSSNLabel, 0, 2)
            Me.MigrateFromPatientDetailsPanel.Controls.Add(Me.MigrateFromNameLabel, 0, 1)
            Me.MigrateFromPatientDetailsPanel.Controls.Add(Me.MigrateFromNameValueLabel, 1, 1)
            Me.MigrateFromPatientDetailsPanel.Controls.Add(Me.MigrateFromSSNValueLabel, 1, 2)
            Me.MigrateFromPatientDetailsPanel.Location = New System.Drawing.Point(6, 21)
            Me.MigrateFromPatientDetailsPanel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromPatientDetailsPanel.Name = "MigrateFromPatientDetailsPanel"
            Me.MigrateFromPatientDetailsPanel.RowCount = 3
            Me.MigrateFromPatientDetailsPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
            Me.MigrateFromPatientDetailsPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
            Me.MigrateFromPatientDetailsPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
            Me.MigrateFromPatientDetailsPanel.Size = New System.Drawing.Size(429, 76)
            Me.MigrateFromPatientDetailsPanel.TabIndex = 3
            '
            'MigrateFromPatientIdLabel
            '
            Me.MigrateFromPatientIdLabel.AutoSize = True
            Me.MigrateFromPatientIdLabel.Location = New System.Drawing.Point(3, 0)
            Me.MigrateFromPatientIdLabel.Name = "MigrateFromPatientIdLabel"
            Me.MigrateFromPatientIdLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromPatientIdLabel.Size = New System.Drawing.Size(61, 17)
            Me.MigrateFromPatientIdLabel.TabIndex = 1
            Me.MigrateFromPatientIdLabel.Text = "Patient Id:"
            '
            'MigrateFromPatientIdValueLabel
            '
            Me.MigrateFromPatientIdValueLabel.AutoSize = True
            Me.MigrateFromPatientIdValueLabel.Location = New System.Drawing.Point(74, 0)
            Me.MigrateFromPatientIdValueLabel.Name = "MigrateFromPatientIdValueLabel"
            Me.MigrateFromPatientIdValueLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromPatientIdValueLabel.Size = New System.Drawing.Size(6, 17)
            Me.MigrateFromPatientIdValueLabel.TabIndex = 3
            '
            'MigrateFromAddressLabel
            '
            Me.MigrateFromAddressLabel.AutoSize = True
            Me.MigrateFromAddressLabel.Location = New System.Drawing.Point(211, 0)
            Me.MigrateFromAddressLabel.Name = "MigrateFromAddressLabel"
            Me.MigrateFromAddressLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromAddressLabel.Size = New System.Drawing.Size(54, 17)
            Me.MigrateFromAddressLabel.TabIndex = 6
            Me.MigrateFromAddressLabel.Text = "Address:"
            '
            'MigrateToPatientIdInputLabel
            '
            Me.MigrateToPatientIdInputLabel.AutoSize = True
            Me.MigrateToPatientIdInputLabel.Location = New System.Drawing.Point(15, 37)
            Me.MigrateToPatientIdInputLabel.Name = "MigrateToPatientIdInputLabel"
            Me.MigrateToPatientIdInputLabel.Size = New System.Drawing.Size(52, 13)
            Me.MigrateToPatientIdInputLabel.TabIndex = 0
            Me.MigrateToPatientIdInputLabel.Text = "Patient Id"
            '
            'MigrateToPatientIdInputTextBox
            '
            Me.MigrateToPatientIdInputTextBox.Location = New System.Drawing.Point(85, 34)
            Me.MigrateToPatientIdInputTextBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToPatientIdInputTextBox.Name = "MigrateToPatientIdInputTextBox"
            Me.MigrateToPatientIdInputTextBox.Size = New System.Drawing.Size(97, 20)
            Me.MigrateToPatientIdInputTextBox.TabIndex = 20
            '
            'MigrateToAppointmentLabel
            '
            Me.MigrateToAppointmentLabel.AutoSize = True
            Me.MigrateToAppointmentLabel.Location = New System.Drawing.Point(385, 37)
            Me.MigrateToAppointmentLabel.Name = "MigrateToAppointmentLabel"
            Me.MigrateToAppointmentLabel.Size = New System.Drawing.Size(66, 13)
            Me.MigrateToAppointmentLabel.TabIndex = 3
            Me.MigrateToAppointmentLabel.Text = "Appointment"
            '
            'MigrateToPatientDetailsBox
            '
            Me.MigrateToPatientDetailsBox.Controls.Add(Me.MigrateToPatientDetailsPanel)
            Me.MigrateToPatientDetailsBox.Location = New System.Drawing.Point(19, 78)
            Me.MigrateToPatientDetailsBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToPatientDetailsBox.Name = "MigrateToPatientDetailsBox"
            Me.MigrateToPatientDetailsBox.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToPatientDetailsBox.Size = New System.Drawing.Size(440, 102)
            Me.MigrateToPatientDetailsBox.TabIndex = 4
            Me.MigrateToPatientDetailsBox.TabStop = False
            Me.MigrateToPatientDetailsBox.Text = "Patient Details"
            '
            'MigrateToPatientDetailsPanel
            '
            Me.MigrateToPatientDetailsPanel.ColumnCount = 4
            Me.MigrateToPatientDetailsPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.21053!))
            Me.MigrateToPatientDetailsPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.78947!))
            Me.MigrateToPatientDetailsPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 71.0!))
            Me.MigrateToPatientDetailsPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 142.0!))
            Me.MigrateToPatientDetailsPanel.Controls.Add(Me.MigrateToAddressValueLabel, 3, 0)
            Me.MigrateToPatientDetailsPanel.Controls.Add(Me.MigrateToAddressLabel, 2, 0)
            Me.MigrateToPatientDetailsPanel.Controls.Add(Me.MigrateToPatientIdValueLabel, 1, 0)
            Me.MigrateToPatientDetailsPanel.Controls.Add(Me.MigrateToPatientIdLabel, 0, 0)
            Me.MigrateToPatientDetailsPanel.Controls.Add(Me.MigrateToSSNLabel, 0, 2)
            Me.MigrateToPatientDetailsPanel.Controls.Add(Me.MigrateToNameLabel, 0, 1)
            Me.MigrateToPatientDetailsPanel.Controls.Add(Me.MigrateToNameValueLabel, 1, 1)
            Me.MigrateToPatientDetailsPanel.Controls.Add(Me.MigrateToSSNValueLabel, 1, 2)
            Me.MigrateToPatientDetailsPanel.Location = New System.Drawing.Point(6, 21)
            Me.MigrateToPatientDetailsPanel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToPatientDetailsPanel.Name = "MigrateToPatientDetailsPanel"
            Me.MigrateToPatientDetailsPanel.RowCount = 3
            Me.MigrateToPatientDetailsPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
            Me.MigrateToPatientDetailsPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
            Me.MigrateToPatientDetailsPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
            Me.MigrateToPatientDetailsPanel.Size = New System.Drawing.Size(429, 76)
            Me.MigrateToPatientDetailsPanel.TabIndex = 3
            '
            'MigrateToSSNValueLabel
            '
            Me.MigrateToSSNValueLabel.AutoSize = True
            Me.MigrateToSSNValueLabel.Location = New System.Drawing.Point(76, 34)
            Me.MigrateToSSNValueLabel.Name = "MigrateToSSNValueLabel"
            Me.MigrateToSSNValueLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToSSNValueLabel.Size = New System.Drawing.Size(6, 17)
            Me.MigrateToSSNValueLabel.TabIndex = 5
            '
            'MigrateToNameValueLabel
            '
            Me.MigrateToNameValueLabel.AutoSize = True
            Me.MigrateToNameValueLabel.Location = New System.Drawing.Point(76, 17)
            Me.MigrateToNameValueLabel.Name = "MigrateToNameValueLabel"
            Me.MigrateToNameValueLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToNameValueLabel.Size = New System.Drawing.Size(6, 17)
            Me.MigrateToNameValueLabel.TabIndex = 4
            '
            'MigrateToNameLabel
            '
            Me.MigrateToNameLabel.AutoSize = True
            Me.MigrateToNameLabel.Location = New System.Drawing.Point(3, 17)
            Me.MigrateToNameLabel.Name = "MigrateToNameLabel"
            Me.MigrateToNameLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToNameLabel.Size = New System.Drawing.Size(44, 17)
            Me.MigrateToNameLabel.TabIndex = 0
            Me.MigrateToNameLabel.Text = "Name:"
            '
            'MigrateToSSNLabel
            '
            Me.MigrateToSSNLabel.AutoSize = True
            Me.MigrateToSSNLabel.Location = New System.Drawing.Point(3, 34)
            Me.MigrateToSSNLabel.Name = "MigrateToSSNLabel"
            Me.MigrateToSSNLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToSSNLabel.Size = New System.Drawing.Size(38, 17)
            Me.MigrateToSSNLabel.TabIndex = 2
            Me.MigrateToSSNLabel.Text = "SSN:"
            '
            'MigrateToPatientIdLabel
            '
            Me.MigrateToPatientIdLabel.AutoSize = True
            Me.MigrateToPatientIdLabel.Location = New System.Drawing.Point(3, 0)
            Me.MigrateToPatientIdLabel.Name = "MigrateToPatientIdLabel"
            Me.MigrateToPatientIdLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToPatientIdLabel.Size = New System.Drawing.Size(61, 17)
            Me.MigrateToPatientIdLabel.TabIndex = 1
            Me.MigrateToPatientIdLabel.Text = "Patient Id:"
            '
            'MigrateToPatientIdValueLabel
            '
            Me.MigrateToPatientIdValueLabel.AutoSize = True
            Me.MigrateToPatientIdValueLabel.Location = New System.Drawing.Point(76, 0)
            Me.MigrateToPatientIdValueLabel.Name = "MigrateToPatientIdValueLabel"
            Me.MigrateToPatientIdValueLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToPatientIdValueLabel.Size = New System.Drawing.Size(6, 17)
            Me.MigrateToPatientIdValueLabel.TabIndex = 3
            '
            'MigrateToAddressLabel
            '
            Me.MigrateToAddressLabel.AutoSize = True
            Me.MigrateToAddressLabel.Location = New System.Drawing.Point(218, 0)
            Me.MigrateToAddressLabel.Name = "MigrateToAddressLabel"
            Me.MigrateToAddressLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToAddressLabel.Size = New System.Drawing.Size(54, 17)
            Me.MigrateToAddressLabel.TabIndex = 6
            Me.MigrateToAddressLabel.Text = "Address:"
            '
            'MigrateToAddressValueLabel
            '
            Me.MigrateToAddressValueLabel.AutoSize = True
            Me.MigrateToAddressValueLabel.Location = New System.Drawing.Point(289, 0)
            Me.MigrateToAddressValueLabel.Name = "MigrateToAddressValueLabel"
            Me.MigrateToAddressValueLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToPatientDetailsPanel.SetRowSpan(Me.MigrateToAddressValueLabel, 3)
            Me.MigrateToAddressValueLabel.Size = New System.Drawing.Size(6, 17)
            Me.MigrateToAddressValueLabel.TabIndex = 7
            '
            'MigrateToSelectButton
            '
            Me.MigrateToSelectButton.Location = New System.Drawing.Point(200, 33)
            Me.MigrateToSelectButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToSelectButton.Name = "MigrateToSelectButton"
            Me.MigrateToSelectButton.Size = New System.Drawing.Size(73, 24)
            Me.MigrateToSelectButton.TabIndex = 5
            Me.MigrateToSelectButton.Text = "Select"
            Me.MigrateToSelectButton.Tooltip = Nothing
            '
            'MigrateToFindButton
            '
            Me.MigrateToFindButton.Location = New System.Drawing.Point(278, 33)
            Me.MigrateToFindButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToFindButton.Name = "MigrateToFindButton"
            Me.MigrateToFindButton.Size = New System.Drawing.Size(73, 24)
            Me.MigrateToFindButton.TabIndex = 6
            Me.MigrateToFindButton.Text = "Find"
            Me.MigrateToFindButton.Tooltip = Nothing
            '
            'MigrateToAppointmentsGrid
            '
            Me.MigrateToAppointmentsGrid.AllowUserToAddRows = False
            Me.MigrateToAppointmentsGrid.AllowUserToDeleteRows = False
            Me.MigrateToAppointmentsGrid.AllowUserToResizeColumns = False
            Me.MigrateToAppointmentsGrid.AllowUserToResizeRows = False
            Me.MigrateToAppointmentsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
            Me.MigrateToAppointmentsGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MigrateToAppointmentId, Me.MigrateToAppointmentDate, Me.MigrateToAppointmentTime, Me.MigrateToAppointmentType, Me.MigrateToAppointmentStatus})
            Me.MigrateToAppointmentsGrid.Location = New System.Drawing.Point(477, 33)
            Me.MigrateToAppointmentsGrid.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToAppointmentsGrid.MultiSelect = False
            Me.MigrateToAppointmentsGrid.Name = "MigrateToAppointmentsGrid"
            Me.MigrateToAppointmentsGrid.ReadOnly = True
            Me.MigrateToAppointmentsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
            Me.MigrateToAppointmentsGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.MigrateToAppointmentsGrid.Size = New System.Drawing.Size(365, 139)
            Me.MigrateToAppointmentsGrid.TabIndex = 7
            '
            'MigrateToAppointmentStatus
            '
            Me.MigrateToAppointmentStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
            Me.MigrateToAppointmentStatus.HeaderText = "Status"
            Me.MigrateToAppointmentStatus.MinimumWidth = 75
            Me.MigrateToAppointmentStatus.Name = "MigrateToAppointmentStatus"
            Me.MigrateToAppointmentStatus.ReadOnly = True
            Me.MigrateToAppointmentStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            '
            'MigrateToAppointmentType
            '
            Me.MigrateToAppointmentType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
            Me.MigrateToAppointmentType.HeaderText = "Type"
            Me.MigrateToAppointmentType.MinimumWidth = 75
            Me.MigrateToAppointmentType.Name = "MigrateToAppointmentType"
            Me.MigrateToAppointmentType.ReadOnly = True
            Me.MigrateToAppointmentType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.MigrateToAppointmentType.Width = 75
            '
            'MigrateToAppointmentTime
            '
            Me.MigrateToAppointmentTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
            Me.MigrateToAppointmentTime.HeaderText = "Time"
            Me.MigrateToAppointmentTime.MinimumWidth = 75
            Me.MigrateToAppointmentTime.Name = "MigrateToAppointmentTime"
            Me.MigrateToAppointmentTime.ReadOnly = True
            Me.MigrateToAppointmentTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.MigrateToAppointmentTime.Width = 75
            '
            'MigrateToAppointmentDate
            '
            Me.MigrateToAppointmentDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
            Me.MigrateToAppointmentDate.HeaderText = "Date"
            Me.MigrateToAppointmentDate.MinimumWidth = 75
            Me.MigrateToAppointmentDate.Name = "MigrateToAppointmentDate"
            Me.MigrateToAppointmentDate.ReadOnly = True
            Me.MigrateToAppointmentDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.MigrateToAppointmentDate.Width = 75
            '
            'MigrateToAppointmentId
            '
            Me.MigrateToAppointmentId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
            Me.MigrateToAppointmentId.HeaderText = "Id"
            Me.MigrateToAppointmentId.Name = "MigrateToAppointmentId"
            Me.MigrateToAppointmentId.ReadOnly = True
            Me.MigrateToAppointmentId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Me.MigrateToAppointmentId.Visible = False
            '
            'MigrateToBox
            '
            Me.MigrateToBox.Controls.Add(Me.MigrateToAppointmentsGrid)
            Me.MigrateToBox.Controls.Add(Me.MigrateToFindButton)
            Me.MigrateToBox.Controls.Add(Me.MigrateToSelectButton)
            Me.MigrateToBox.Controls.Add(Me.MigrateToPatientDetailsBox)
            Me.MigrateToBox.Controls.Add(Me.MigrateToAppointmentLabel)
            Me.MigrateToBox.Controls.Add(Me.MigrateToPatientIdInputTextBox)
            Me.MigrateToBox.Controls.Add(Me.MigrateToPatientIdInputLabel)
            Me.MigrateToBox.Location = New System.Drawing.Point(15, 212)
            Me.MigrateToBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToBox.Name = "MigrateToBox"
            Me.MigrateToBox.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateToBox.Size = New System.Drawing.Size(847, 195)
            Me.MigrateToBox.TabIndex = 5
            Me.MigrateToBox.TabStop = False
            Me.MigrateToBox.Text = "To..."
            '
            'MigrateFromFindButton
            '
            Me.MigrateFromFindButton.Location = New System.Drawing.Point(278, 33)
            Me.MigrateFromFindButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromFindButton.Name = "MigrateFromFindButton"
            Me.MigrateFromFindButton.Size = New System.Drawing.Size(73, 24)
            Me.MigrateFromFindButton.TabIndex = 6
            Me.MigrateFromFindButton.Text = "Find"
            Me.MigrateFromFindButton.Tooltip = Nothing
            '
            'MigrateFromAddressValueLabel
            '
            Me.MigrateFromAddressValueLabel.AutoSize = True
            Me.MigrateFromAddressValueLabel.Location = New System.Drawing.Point(280, 0)
            Me.MigrateFromAddressValueLabel.Name = "MigrateFromAddressValueLabel"
            Me.MigrateFromAddressValueLabel.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromPatientDetailsPanel.SetRowSpan(Me.MigrateFromAddressValueLabel, 3)
            Me.MigrateFromAddressValueLabel.Size = New System.Drawing.Size(6, 17)
            Me.MigrateFromAddressValueLabel.TabIndex = 7
            '
            'MigrateFromSelectButton
            '
            Me.MigrateFromSelectButton.Location = New System.Drawing.Point(200, 33)
            Me.MigrateFromSelectButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MigrateFromSelectButton.Name = "MigrateFromSelectButton"
            Me.MigrateFromSelectButton.Size = New System.Drawing.Size(73, 24)
            Me.MigrateFromSelectButton.TabIndex = 5
            Me.MigrateFromSelectButton.Text = "Select"
            Me.MigrateFromSelectButton.Tooltip = Nothing
            '
            'MainControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)

            Me.Controls.Add(Me.MigrateButton)
            Me.Controls.Add(Me.MigrateFromBox)
            Me.Controls.Add(Me.MigrateToBox)
            Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.Name = "MainControl"
            Me.Size = New System.Drawing.Size(873, 479)
            Me.GroupBox1.ResumeLayout(False)
            Me.TableLayoutPanel2.ResumeLayout(False)
            Me.TableLayoutPanel2.PerformLayout()
            CType(Me.MigrateFromAppointmentsGrid, System.ComponentModel.ISupportInitialize).EndInit()
            Me.MigrateFromBox.ResumeLayout(False)
            Me.MigrateFromBox.PerformLayout()
            Me.MigrateFromPatientDetailsBox.ResumeLayout(False)
            Me.MigrateFromPatientDetailsPanel.ResumeLayout(False)
            Me.MigrateFromPatientDetailsPanel.PerformLayout()
            Me.MigrateToPatientDetailsBox.ResumeLayout(False)
            Me.MigrateToPatientDetailsPanel.ResumeLayout(False)
            Me.MigrateToPatientDetailsPanel.PerformLayout()
            CType(Me.MigrateToAppointmentsGrid, System.ComponentModel.ISupportInitialize).EndInit()
            Me.MigrateToBox.ResumeLayout(False)
            Me.MigrateToBox.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents MigrateButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents Button4 As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents Button3 As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
        Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
        Friend WithEvents Label1 As System.Windows.Forms.Label
        Friend WithEvents MigrateFromPatientIdInputLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateFromPatientIdInputTextBox As System.Windows.Forms.TextBox
        Friend WithEvents MigrateFromAppointmentLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateFromAppointmentsGrid As System.Windows.Forms.DataGridView
        Friend WithEvents MigrateFromAppointmentId As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents MigrateFromAppointmentDate As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents MigrateFromAppointmentTime As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents MigrateFromAppointmentType As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents MigrateFromAppointmentStatus As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents MigrateFromBox As System.Windows.Forms.GroupBox
        Friend WithEvents MigrateFromFindButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents MigrateFromSelectButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents MigrateFromPatientDetailsBox As System.Windows.Forms.GroupBox
        Friend WithEvents MigrateFromPatientDetailsPanel As System.Windows.Forms.TableLayoutPanel
        Friend WithEvents MigrateFromAddressValueLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateFromAddressLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateFromPatientIdValueLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateFromPatientIdLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateFromSSNLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateFromNameLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateFromNameValueLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateFromSSNValueLabel As System.Windows.Forms.Label
        Friend WithEvents Label2 As System.Windows.Forms.Label
        Friend WithEvents MigrateToPatientIdInputLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateToPatientIdInputTextBox As System.Windows.Forms.TextBox
        Friend WithEvents MigrateToAppointmentLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateToPatientDetailsBox As System.Windows.Forms.GroupBox
        Friend WithEvents MigrateToPatientDetailsPanel As System.Windows.Forms.TableLayoutPanel
        Friend WithEvents MigrateToAddressValueLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateToAddressLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateToPatientIdValueLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateToPatientIdLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateToSSNLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateToNameLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateToNameValueLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateToSSNValueLabel As System.Windows.Forms.Label
        Friend WithEvents MigrateToSelectButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents MigrateToFindButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents MigrateToAppointmentsGrid As System.Windows.Forms.DataGridView
        Friend WithEvents MigrateToAppointmentId As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents MigrateToAppointmentDate As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents MigrateToAppointmentTime As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents MigrateToAppointmentType As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents MigrateToAppointmentStatus As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents MigrateToBox As System.Windows.Forms.GroupBox

    End Class
End Namespace
