Namespace RequeueClaims
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MainControl
        Inherits Soaf.Presentation.Controls.WindowsForms.UserControl

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.StatusesPanel = New System.Windows.Forms.TableLayoutPanel()
            Me.CrossoverStatusCheckBox = New System.Windows.Forms.CheckBox()
            Me.BilledStatusCheckBox = New System.Windows.Forms.CheckBox()
            Me.WaitingStatusCheckBox = New System.Windows.Forms.CheckBox()
            Me.BatchesList = New System.Windows.Forms.ComboBox()
            Me.Label2 = New System.Windows.Forms.Label()
            Me.TypesList = New System.Windows.Forms.ComboBox()
            Me.BatchCheckBox = New System.Windows.Forms.CheckBox()
            Me.DateCheckBox = New System.Windows.Forms.CheckBox()
            Me.TypeCheckBox = New System.Windows.Forms.CheckBox()
            Me.CriteriaPanel = New System.Windows.Forms.TableLayoutPanel()
            Me.IncludeAllInsurerPlansCheckBox = New System.Windows.Forms.CheckBox()
            Me.InsurerCheckBox = New System.Windows.Forms.CheckBox()
            Me.InsurerDetailsPanel = New System.Windows.Forms.TableLayoutPanel()
            Me.InsurerNameValueLabel = New System.Windows.Forms.Label()
            Me.InsurerNameLabel = New System.Windows.Forms.Label()
            Me.InsurerPlanNameValueLabel = New System.Windows.Forms.Label()
            Me.InsurerAddressValueLabel = New System.Windows.Forms.Label()
            Me.InsurerAddressLabel = New System.Windows.Forms.Label()
            Me.InsurerPlanNameLabel = New System.Windows.Forms.Label()
            Me.FindInsurerButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.DatePanel = New System.Windows.Forms.TableLayoutPanel()
            Me.DatePicker = New System.Windows.Forms.DateTimePicker()
            Me.DateComparisonsList = New System.Windows.Forms.ComboBox()
            Me.Label1 = New System.Windows.Forms.Label()
            Me.RequeueClaimsButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.StatusesPanel.SuspendLayout()
            Me.CriteriaPanel.SuspendLayout()
            Me.InsurerDetailsPanel.SuspendLayout()
            Me.DatePanel.SuspendLayout()
            Me.SuspendLayout()
            '
            'StatusesPanel
            '
            Me.StatusesPanel.ColumnCount = 3
            Me.StatusesPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
            Me.StatusesPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
            Me.StatusesPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
            Me.StatusesPanel.Controls.Add(Me.CrossoverStatusCheckBox, 2, 0)
            Me.StatusesPanel.Controls.Add(Me.BilledStatusCheckBox, 0, 0)
            Me.StatusesPanel.Controls.Add(Me.WaitingStatusCheckBox, 1, 0)
            Me.StatusesPanel.Location = New System.Drawing.Point(109, 132)
            Me.StatusesPanel.Name = "StatusesPanel"
            Me.StatusesPanel.RowCount = 1
            Me.StatusesPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
            Me.StatusesPanel.Size = New System.Drawing.Size(293, 37)
            Me.StatusesPanel.TabIndex = 19
            '
            'CrossoverStatusCheckBox
            '
            Me.CrossoverStatusCheckBox.AutoSize = True
            Me.CrossoverStatusCheckBox.Location = New System.Drawing.Point(197, 3)
            Me.CrossoverStatusCheckBox.Name = "CrossoverStatusCheckBox"
            Me.CrossoverStatusCheckBox.Size = New System.Drawing.Size(73, 17)
            Me.CrossoverStatusCheckBox.TabIndex = 20
            Me.CrossoverStatusCheckBox.Text = "Crossover"
            Me.CrossoverStatusCheckBox.UseVisualStyleBackColor = True
            '
            'BilledStatusCheckBox
            '
            Me.BilledStatusCheckBox.AutoSize = True
            Me.BilledStatusCheckBox.Location = New System.Drawing.Point(3, 3)
            Me.BilledStatusCheckBox.Name = "BilledStatusCheckBox"
            Me.BilledStatusCheckBox.Size = New System.Drawing.Size(51, 17)
            Me.BilledStatusCheckBox.TabIndex = 19
            Me.BilledStatusCheckBox.Text = "Billed"
            Me.BilledStatusCheckBox.UseVisualStyleBackColor = True
            '
            'WaitingStatusCheckBox
            '
            Me.WaitingStatusCheckBox.AutoSize = True
            Me.WaitingStatusCheckBox.Location = New System.Drawing.Point(100, 3)
            Me.WaitingStatusCheckBox.Name = "WaitingStatusCheckBox"
            Me.WaitingStatusCheckBox.Size = New System.Drawing.Size(62, 17)
            Me.WaitingStatusCheckBox.TabIndex = 18
            Me.WaitingStatusCheckBox.Text = "Waiting"
            Me.WaitingStatusCheckBox.UseVisualStyleBackColor = True
            '
            'BatchesList
            '
            Me.BatchesList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.BatchesList.FormattingEnabled = True
            Me.BatchesList.Location = New System.Drawing.Point(109, 89)
            Me.BatchesList.Name = "BatchesList"
            Me.BatchesList.Size = New System.Drawing.Size(132, 21)
            Me.BatchesList.TabIndex = 15
            '
            'Label2
            '
            Me.Label2.AutoSize = True
            Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label2.Location = New System.Drawing.Point(7, 10)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(158, 18)
            Me.Label2.TabIndex = 7
            Me.Label2.Text = "Specifiy claim criteria..."
            '
            'TypesList
            '
            Me.TypesList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.TypesList.FormattingEnabled = True
            Me.TypesList.Location = New System.Drawing.Point(109, 3)
            Me.TypesList.Name = "TypesList"
            Me.TypesList.Size = New System.Drawing.Size(132, 21)
            Me.TypesList.TabIndex = 13
            '
            'BatchCheckBox
            '
            Me.BatchCheckBox.AutoSize = True
            Me.BatchCheckBox.Location = New System.Drawing.Point(3, 89)
            Me.BatchCheckBox.Name = "BatchCheckBox"
            Me.BatchCheckBox.Size = New System.Drawing.Size(54, 17)
            Me.BatchCheckBox.TabIndex = 8
            Me.BatchCheckBox.Text = "Batch"
            '
            'DateCheckBox
            '
            Me.DateCheckBox.AutoSize = True
            Me.DateCheckBox.Location = New System.Drawing.Point(3, 46)
            Me.DateCheckBox.Name = "DateCheckBox"
            Me.DateCheckBox.Size = New System.Drawing.Size(49, 17)
            Me.DateCheckBox.TabIndex = 6
            Me.DateCheckBox.Text = "Date"
            '
            'TypeCheckBox
            '
            Me.TypeCheckBox.AutoSize = True
            Me.TypeCheckBox.Location = New System.Drawing.Point(3, 3)
            Me.TypeCheckBox.Name = "TypeCheckBox"
            Me.TypeCheckBox.Size = New System.Drawing.Size(50, 17)
            Me.TypeCheckBox.TabIndex = 4
            Me.TypeCheckBox.Text = "Type"
            '
            'CriteriaPanel
            '
            Me.CriteriaPanel.ColumnCount = 2
            Me.CriteriaPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.78478!))
            Me.CriteriaPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.21522!))
            Me.CriteriaPanel.Controls.Add(Me.StatusesPanel, 1, 3)
            Me.CriteriaPanel.Controls.Add(Me.BatchesList, 1, 2)
            Me.CriteriaPanel.Controls.Add(Me.TypesList, 1, 0)
            Me.CriteriaPanel.Controls.Add(Me.BatchCheckBox, 0, 2)
            Me.CriteriaPanel.Controls.Add(Me.DateCheckBox, 0, 1)
            Me.CriteriaPanel.Controls.Add(Me.TypeCheckBox, 0, 0)
            Me.CriteriaPanel.Controls.Add(Me.IncludeAllInsurerPlansCheckBox, 0, 5)
            Me.CriteriaPanel.Controls.Add(Me.InsurerCheckBox, 0, 4)
            Me.CriteriaPanel.Controls.Add(Me.InsurerDetailsPanel, 1, 4)
            Me.CriteriaPanel.Controls.Add(Me.DatePanel, 1, 1)
            Me.CriteriaPanel.Controls.Add(Me.Label1, 0, 3)
            Me.CriteriaPanel.Location = New System.Drawing.Point(27, 41)
            Me.CriteriaPanel.Name = "CriteriaPanel"
            Me.CriteriaPanel.RowCount = 7
            Me.CriteriaPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
            Me.CriteriaPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
            Me.CriteriaPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
            Me.CriteriaPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
            Me.CriteriaPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 141.0!))
            Me.CriteriaPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
            Me.CriteriaPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
            Me.CriteriaPanel.Size = New System.Drawing.Size(487, 397)
            Me.CriteriaPanel.TabIndex = 8
            '
            'IncludeAllInsurerPlansCheckBox
            '
            Me.IncludeAllInsurerPlansCheckBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.IncludeAllInsurerPlansCheckBox.AutoSize = True
            Me.CriteriaPanel.SetColumnSpan(Me.IncludeAllInsurerPlansCheckBox, 2)
            Me.IncludeAllInsurerPlansCheckBox.Location = New System.Drawing.Point(3, 325)
            Me.IncludeAllInsurerPlansCheckBox.Name = "IncludeAllInsurerPlansCheckBox"
            Me.IncludeAllInsurerPlansCheckBox.Size = New System.Drawing.Size(176, 17)
            Me.IncludeAllInsurerPlansCheckBox.TabIndex = 11
            Me.IncludeAllInsurerPlansCheckBox.Text = "Include all plans for this insurer?"
            '
            'InsurerCheckBox
            '
            Me.InsurerCheckBox.AutoSize = True
            Me.InsurerCheckBox.Location = New System.Drawing.Point(3, 182)
            Me.InsurerCheckBox.Name = "InsurerCheckBox"
            Me.InsurerCheckBox.Size = New System.Drawing.Size(58, 17)
            Me.InsurerCheckBox.TabIndex = 10
            Me.InsurerCheckBox.Text = "Insurer"
            '
            'InsurerDetailsPanel
            '
            Me.InsurerDetailsPanel.ColumnCount = 4
            Me.InsurerDetailsPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
            Me.InsurerDetailsPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 87.0!))
            Me.InsurerDetailsPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
            Me.InsurerDetailsPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 158.0!))
            Me.InsurerDetailsPanel.Controls.Add(Me.InsurerNameValueLabel, 1, 0)
            Me.InsurerDetailsPanel.Controls.Add(Me.InsurerNameLabel, 0, 0)
            Me.InsurerDetailsPanel.Controls.Add(Me.InsurerPlanNameValueLabel, 1, 1)
            Me.InsurerDetailsPanel.Controls.Add(Me.InsurerAddressValueLabel, 3, 1)
            Me.InsurerDetailsPanel.Controls.Add(Me.InsurerAddressLabel, 2, 1)
            Me.InsurerDetailsPanel.Controls.Add(Me.InsurerPlanNameLabel, 0, 1)
            Me.InsurerDetailsPanel.Controls.Add(Me.FindInsurerButton, 3, 0)
            Me.InsurerDetailsPanel.Location = New System.Drawing.Point(109, 182)
            Me.InsurerDetailsPanel.Name = "InsurerDetailsPanel"
            Me.InsurerDetailsPanel.RowCount = 2
            Me.InsurerDetailsPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.19048!))
            Me.InsurerDetailsPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.80952!))
            Me.InsurerDetailsPanel.Size = New System.Drawing.Size(375, 93)
            Me.InsurerDetailsPanel.TabIndex = 16
            '
            'InsurerNameValueLabel
            '
            Me.InsurerNameValueLabel.AutoSize = True
            Me.InsurerNameValueLabel.Location = New System.Drawing.Point(68, 0)
            Me.InsurerNameValueLabel.Name = "InsurerNameValueLabel"
            Me.InsurerNameValueLabel.Size = New System.Drawing.Size(0, 13)
            Me.InsurerNameValueLabel.TabIndex = 4
            '
            'InsurerNameLabel
            '
            Me.InsurerNameLabel.AutoSize = True
            Me.InsurerNameLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.InsurerNameLabel.Location = New System.Drawing.Point(3, 0)
            Me.InsurerNameLabel.Name = "InsurerNameLabel"
            Me.InsurerNameLabel.Size = New System.Drawing.Size(49, 15)
            Me.InsurerNameLabel.TabIndex = 1
            Me.InsurerNameLabel.Text = "Name:"
            '
            'InsurerPlanNameValueLabel
            '
            Me.InsurerPlanNameValueLabel.AutoSize = True
            Me.InsurerPlanNameValueLabel.Location = New System.Drawing.Point(68, 47)
            Me.InsurerPlanNameValueLabel.Name = "InsurerPlanNameValueLabel"
            Me.InsurerPlanNameValueLabel.Size = New System.Drawing.Size(0, 13)
            Me.InsurerPlanNameValueLabel.TabIndex = 7
            '
            'InsurerAddressValueLabel
            '
            Me.InsurerAddressValueLabel.AutoSize = True
            Me.InsurerAddressValueLabel.Location = New System.Drawing.Point(221, 47)
            Me.InsurerAddressValueLabel.Name = "InsurerAddressValueLabel"
            Me.InsurerAddressValueLabel.Size = New System.Drawing.Size(0, 13)
            Me.InsurerAddressValueLabel.TabIndex = 5
            '
            'InsurerAddressLabel
            '
            Me.InsurerAddressLabel.AutoSize = True
            Me.InsurerAddressLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.InsurerAddressLabel.Location = New System.Drawing.Point(155, 47)
            Me.InsurerAddressLabel.Name = "InsurerAddressLabel"
            Me.InsurerAddressLabel.Size = New System.Drawing.Size(58, 30)
            Me.InsurerAddressLabel.TabIndex = 2
            Me.InsurerAddressLabel.Text = "Address:"
            '
            'InsurerPlanNameLabel
            '
            Me.InsurerPlanNameLabel.AutoSize = True
            Me.InsurerPlanNameLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.InsurerPlanNameLabel.Location = New System.Drawing.Point(3, 47)
            Me.InsurerPlanNameLabel.Name = "InsurerPlanNameLabel"
            Me.InsurerPlanNameLabel.Size = New System.Drawing.Size(49, 30)
            Me.InsurerPlanNameLabel.TabIndex = 3
            Me.InsurerPlanNameLabel.Text = "Plan Name:"
            '
            'FindInsurerButton
            '
            Me.FindInsurerButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.FindInsurerButton.Location = New System.Drawing.Point(309, 3)
            Me.FindInsurerButton.Name = "FindInsurerButton"
            Me.FindInsurerButton.Size = New System.Drawing.Size(64, 30)
            Me.FindInsurerButton.TabIndex = 0
            Me.FindInsurerButton.Text = "Find"
            '
            'DatePanel
            '
            Me.DatePanel.ColumnCount = 2
            Me.DatePanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.95349!))
            Me.DatePanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.04651!))
            Me.DatePanel.Controls.Add(Me.DatePicker, 1, 0)
            Me.DatePanel.Controls.Add(Me.DateComparisonsList, 0, 0)
            Me.DatePanel.Location = New System.Drawing.Point(109, 46)
            Me.DatePanel.Name = "DatePanel"
            Me.DatePanel.RowCount = 2
            Me.DatePanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
            Me.DatePanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
            Me.DatePanel.Size = New System.Drawing.Size(246, 37)
            Me.DatePanel.TabIndex = 17
            '
            'DatePicker
            '
            Me.DatePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
            Me.DatePicker.Location = New System.Drawing.Point(160, 3)
            Me.DatePicker.Name = "DatePicker"
            Me.DatePicker.Size = New System.Drawing.Size(83, 20)
            Me.DatePicker.TabIndex = 17
            '
            'DateComparisonsList
            '
            Me.DateComparisonsList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.DateComparisonsList.FormattingEnabled = True
            Me.DateComparisonsList.Location = New System.Drawing.Point(3, 3)
            Me.DateComparisonsList.Name = "DateComparisonsList"
            Me.DateComparisonsList.Size = New System.Drawing.Size(129, 21)
            Me.DateComparisonsList.TabIndex = 18
            '
            'Label1
            '
            Me.Label1.AutoSize = True
            Me.Label1.Location = New System.Drawing.Point(3, 135)
            Me.Label1.Margin = New System.Windows.Forms.Padding(3, 6, 3, 0)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(37, 13)
            Me.Label1.TabIndex = 19
            Me.Label1.Text = "Status"
            '
            'RequeueClaimsButton
            '
            Me.RequeueClaimsButton.Location = New System.Drawing.Point(401, 485)
            Me.RequeueClaimsButton.Name = "RequeueClaimsButton"
            Me.RequeueClaimsButton.Size = New System.Drawing.Size(118, 49)
            Me.RequeueClaimsButton.TabIndex = 6
            Me.RequeueClaimsButton.Text = "Requeue Claims"
            '
            'MainControl
            '
            Me.Controls.Add(Me.Label2)
            Me.Controls.Add(Me.CriteriaPanel)
            Me.Controls.Add(Me.RequeueClaimsButton)
            Me.Name = "MainControl"
            Me.Size = New System.Drawing.Size(527, 544)
            Me.StatusesPanel.ResumeLayout(False)
            Me.StatusesPanel.PerformLayout()
            Me.CriteriaPanel.ResumeLayout(False)
            Me.CriteriaPanel.PerformLayout()
            Me.InsurerDetailsPanel.ResumeLayout(False)
            Me.InsurerDetailsPanel.PerformLayout()
            Me.DatePanel.ResumeLayout(False)
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents StatusesPanel As System.Windows.Forms.TableLayoutPanel
        Friend WithEvents CrossoverStatusCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents BilledStatusCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents WaitingStatusCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents BatchesList As System.Windows.Forms.ComboBox
        Friend WithEvents Label2 As System.Windows.Forms.Label
        Friend WithEvents TypesList As System.Windows.Forms.ComboBox
        Friend WithEvents BatchCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents DateCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents TypeCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents CriteriaPanel As System.Windows.Forms.TableLayoutPanel
        Friend WithEvents IncludeAllInsurerPlansCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents InsurerCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents InsurerDetailsPanel As System.Windows.Forms.TableLayoutPanel
        Friend WithEvents InsurerNameValueLabel As System.Windows.Forms.Label
        Friend WithEvents InsurerNameLabel As System.Windows.Forms.Label
        Friend WithEvents InsurerPlanNameValueLabel As System.Windows.Forms.Label
        Friend WithEvents InsurerAddressValueLabel As System.Windows.Forms.Label
        Friend WithEvents InsurerAddressLabel As System.Windows.Forms.Label
        Friend WithEvents InsurerPlanNameLabel As System.Windows.Forms.Label
        Friend WithEvents FindInsurerButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents DatePanel As System.Windows.Forms.TableLayoutPanel
        Friend WithEvents DatePicker As System.Windows.Forms.DateTimePicker
        Friend WithEvents DateComparisonsList As System.Windows.Forms.ComboBox
        Friend WithEvents Label1 As System.Windows.Forms.Label
        Friend WithEvents RequeueClaimsButton As Soaf.Presentation.Controls.WindowsForms.Button

    End Class
End Namespace
