Imports System.Collections.Generic
Imports System.Data
Imports System.Windows.Forms
Imports IO.Practiceware.Data
Imports Soaf.Presentation


Namespace RequeueClaims
    Public Class MainControl

        Friend InsurerId As Integer
        Friend Types As New Dictionary(Of String, String)
        Friend DateComparisons As New Dictionary(Of String, String)

        Private Sub RequeueClaimsMainControl_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load

            Types.Add("Electronic", "17")
            Types.Add("Paper", "26")
            Dim typesBindingSource As New BindingSource()
            typesBindingSource.DataSource = Types.Keys
            TypesList.DataSource = typesBindingSource

            DateComparisons.Add("Greater Than", ">")
            DateComparisons.Add("Equal To", "=")
            DateComparisons.Add("Less Than", "<")
            Dim dateComparisonsBindingSource As New BindingSource()
            dateComparisonsBindingSource.DataSource = DateComparisons.Keys
            DateComparisonsList.DataSource = dateComparisonsBindingSource
            DatePicker.Value = Now()

            Dim dbConnection2 = DbConnectionFactory.PracticeRepository()
            Dim dbCommand2 = dbConnection2.CreateCommand()
            dbCommand2.CommandText = "SELECT DISTINCT TOP 100 esm.CreatedDateTime FROM model.externalsystemmessagetypes esmt INNER JOIN model.ExternalSystemExternalSystemMessageTypes esesmt ON esmt.id = esesmt.ExternalSystemMessageTypeId INNER JOIN model.externalsystems es ON es.id = esesmt.ExternalSystemId INNER JOIN model.ExternalSystemMessages esm ON esm.ExternalSystemExternalSystemMessageTypeId = esesmt.Id WHERE isoutbound = 1 AND es.id = 4 ORDER BY esm.CreatedDateTime desc"
            Try
                dbConnection2.Open()
                If dbConnection2.State = ConnectionState.Open Then
                    Dim reader2 = CType(dbCommand2.ExecuteReader(), DataTableReader)
                    If reader2.HasRows Then
                        While (reader2.Read)
                            Dim Name = reader2("CreatedDateTime").ToString()
                            If Not BatchesList.Items.Contains(Name) Then
                                BatchesList.Items.Add(Name)
                            End If
                        End While
                        If BatchesList.Items.Count > 0 Then
                            BatchesList.SelectedIndex = 0
                        End If
                    End If
                    reader2.Close()
                    dbConnection2.Close()
                End If
            Catch ex As Exception
                Trace.TraceError(ex.ToString())
            End Try

            TypeCheckBox_CheckedChanged(Nothing, Nothing)
            DateCheckBox_CheckedChanged(Nothing, Nothing)
            BatchCheckBox_CheckedChanged(Nothing, Nothing)
            InsurerCheckBox_CheckedChanged(Nothing, Nothing)
        End Sub

        Private Sub TypeCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles TypeCheckBox.CheckedChanged
            If TypeCheckBox.Checked Then
                TypesList.Enabled = True
            Else
                TypesList.Enabled = False
            End If
        End Sub

        Private Sub DateCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles DateCheckBox.CheckedChanged
            If DateCheckBox.Checked Then
                DatePanel.Enabled = True
            Else
                DatePanel.Enabled = False
            End If
        End Sub


        Private Sub BatchCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles BatchCheckBox.CheckedChanged
            If BatchCheckBox.Checked Then
                BatchesList.Enabled = True
            Else
                BatchesList.Enabled = False
            End If
        End Sub

        Private Sub InsurerCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles InsurerCheckBox.CheckedChanged
            If InsurerCheckBox.Checked Then
                InsurerDetailsPanel.Enabled = True
                IncludeAllInsurerPlansCheckBox.Enabled = True
            Else
                InsurerDetailsPanel.Enabled = False
                IncludeAllInsurerPlansCheckBox.Checked = False
                IncludeAllInsurerPlansCheckBox.Enabled = False
            End If
        End Sub

        Private Sub FindInsurerButton_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles FindInsurerButton.Click
            Dim control As New Finders.FindInsurerControl
            InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = control, .ResizeMode = Windows.ResizeMode.NoResize})
            If control.InsurerId > 0 Then
                InsurerId = control.InsurerId
                InsurerNameValueLabel.Text = control.InsurerName
                InsurerPlanNameValueLabel.Text = control.InsurerPlanName
                InsurerAddressValueLabel.Text = control.InsurerAddress + Environment.NewLine + control.InsurerCity + ", " + control.InsurerState + "  " + control.InsurerZip
            End If
        End Sub


        Private Sub RequeueClaimsButton_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles RequeueClaimsButton.Click
            If InsurerCheckBox.Checked And InsurerId = 0 Then
                MessageBox.Show("Please complete the form.")
            Else
                Dim selectQuery As String = _
"SELECT CASE " + _
    "WHEN esmt.Id = '26' " + _
    "	THEN 'Paper Claim' " + _
    "WHEN esmt.Id = '17' " + _
    "	THEN 'Electronic claim of type 837' " + _
    "END AS ClaimType " + _
    ",dbo.LocalToUTCDate(esm.CreatedDateTime, DATEDIFF(hh, GETDATE(), GETUTCDATE())) AS TransactionDate " + _
    ",ins.Name AS InsurerName" + _
    ",e.StartDateTime AS EncounterDate " + _
        ",e.PatientId AS PatienptId " + _
    ",p.LastName AS LastName " + _
"FROM model.ExternalSystemMessages esm " + _
"INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpr ON esmpr.PracticeRepositoryEntityId = 10 " + _
"AND esmpr.ExternalSystemMessageId = esm.Id "+ _
"INNER JOIN model.BillingServiceTransactions bst ON CONVERT(UNIQUEIDENTIFIER,esmpr.PracticeRepositoryEntityKey) = bst.Id " + _
"INNER JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId " + _
"INNER JOIN model.PatientInsurances pins ON pins.Id = ir.PatientInsuranceId " + _
"INNER JOIN model.InsurancePolicies ip ON pins.InsurancePolicyId = ip.Id " + _
"INNER JOIN model.Insurers ins ON ins.Id = ip.InsurerId " + _
"INNER JOIN model.Invoices inv ON ir.InvoiceId = inv.Id " + _
"INNER JOIN model.Encounters e ON e.Id = inv.EncounterId " + _
"INNER JOIN model.Patients p ON p.Id = e.PatientId " + _
"INNER JOIN model.ExternalSystemExternalSystemMessageTypes esesmt ON esm.ExternalSystemExternalSystemMessageTypeId = esesmt.Id " + _
"INNER JOIN model.ExternalSystemMessageTypes esmt ON esmt.Id = esesmt.ExternalSystemMessageTypeId " + _
"INNER JOIN model.ExternalSystems es ON es.id = esesmt.ExternalSystemId " + _
"WHERE es.Id = 4 AND bst.BillingServiceTransactionStatusId IN (2, 3, 4) "

                Dim criteriaString As String = ""
                If TypeCheckBox.Checked Then
                    criteriaString += " AND esmt.Id = '" + Types(TypesList.SelectedItem.ToString()) + "'"
                End If
                If DateCheckBox.Checked Then
                    criteriaString += " AND CONVERT(date, esm.CreatedDateTime) " + DateComparisons(DateComparisonsList.SelectedItem.ToString()) + " '" + DatePicker.Value.ToString("yyyy-MM-dd") + "'" 
                End If
                If BatchCheckBox.Checked Then
                    criteriaString += " AND  CONVERT(VARCHAR(20),esm.CreatedDateTime,120) = CONVERT(VARCHAR(20),CONVERT(datetime, '" + BatchesList.SelectedItem.ToString() + "'),120)"
                End If
                If InsurerCheckBox.Checked And Not IncludeAllInsurerPlansCheckBox.Checked Then
                    criteriaString += " AND ip.insurerid = " + InsurerId.ToString()
                End If
                If InsurerCheckBox.Checked And IncludeAllInsurerPlansCheckBox.Checked Then
                    criteriaString += " AND ins.Name LIKE '%" + InsurerNameValueLabel.Text + "%'"
                End If

                Dim transportCriteria = New List(Of String)()

                If BilledStatusCheckBox.Checked Then
                    transportCriteria.Add("(bst.BillingServiceTransactionStatusId = 2 or bst.BillingServiceTransactionStatusId IS NULL)")
                End If
                If WaitingStatusCheckBox.Checked Then
                    transportCriteria.Add("(bst.BillingServiceTransactionStatusId = 3)")
                End If
                If CrossoverStatusCheckBox.Checked Then
                    transportCriteria.Add("(bst.BillingServiceTransactionStatusId = 4)")
                End If

                selectQuery += criteriaString
                Dim updateQueries As New List(Of String)
                updateQueries.Add("INSERT INTO model.BillingServiceTransactions " + _
    "(BillingServiceId, Datetime, AmountSent, BillingServiceTransactionStatusId, MethodSentId, InvoiceReceivableId, ClaimFileReceiverId, ClaimFileNumber) " + _
    "SELECT bst.BillingServiceId, GETDATE(), bst.AmountSent, 1 AS BillingServiceTransactionStatusId, bst.MethodSentId, bst.InvoiceReceivableId, bst.ClaimFileReceiverId, bst.ClaimFileNumber " + _
    "FROM model.BillingServiceTransactions bst " + _
    "INNER join model.InvoiceReceivables ir ON bst.InvoiceReceivableId = ir.Id " + _
    "INNER join model.PatientInsurances pins ON ir.PatientInsuranceId = pins.Id " + _
    "INNER join model.InsurancePolicies ipols ON pins.InsurancePolicyId = ipols.Id " + _
    "WHERE bst.BillingServiceTransactionStatusId IN (2, 3, 4) AND bst.Id IN " + _
        "(SELECT CONVERT(UNIQUEIDENTIFIER,esmpr.PracticeRepositoryEntityKey) " + _
        "FROM model.ExternalSystemMessages esm " + _
        "INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpr ON esmpr.PracticeRepositoryEntityId = 10 " + _
            "AND esmpr.ExternalSystemMessageId = esm.Id "+ _
        "INNER JOIN model.BillingServiceTransactions bst ON CONVERT(UNIQUEIDENTIFIER,esmpr.PracticeRepositoryEntityKey) = bst.Id " + _
        "INNER JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId " + _
        "INNER JOIN model.PatientInsurances pins ON pins.Id = ir.PatientInsuranceId " + _
        "INNER JOIN model.InsurancePolicies ip ON pins.InsurancePolicyId = ip.Id " + _
        "INNER JOIN model.Insurers ins ON ins.Id = ip.InsurerId " + _
        "INNER JOIN model.Invoices inv ON ir.InvoiceId = inv.Id " + _
        "INNER JOIN model.Encounters e ON e.Id = inv.EncounterId " + _
        "INNER JOIN model.Patients p ON p.Id = e.PatientId " + _
        "INNER JOIN model.ExternalSystemExternalSystemMessageTypes esesmt ON esm.ExternalSystemExternalSystemMessageTypeId = esesmt.Id " + _
        "INNER JOIN model.ExternalSystemMessageTypes esmt ON esmt.Id = esesmt.ExternalSystemMessageTypeId " + _
        "INNER JOIN model.ExternalSystems es ON es.id = esesmt.ExternalSystemId " + _
        "WHERE es.Id = 4" + _
    criteriaString + ")"  _
                )

                updateQueries.Add( _
"UPDATE bst " + _
    "SET bst.BillingServiceTransactionStatusId = CASE bst.BillingServiceTransactionStatusId " + _
        "WHEN 2 THEN 6 " + _
        "WHEN 3 THEN 5 " + _
        "WHEN 4 THEN 7 " + _
        "ELSE 6 END " + _
    "FROM model.billingservicetransactions bst " + _
    "WHERE bst.Id IN " + _
        "(SELECT CONVERT(UNIQUEIDENTIFIER,esmpr.PracticeRepositoryEntityKey) " + _
        "FROM model.ExternalSystemMessages esm " + _
        "INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpr ON esmpr.PracticeRepositoryEntityId = 10 " + _
            "AND esmpr.ExternalSystemMessageId = esm.Id "+ _
        "INNER JOIN model.BillingServiceTransactions bst ON CONVERT(UNIQUEIDENTIFIER,esmpr.PracticeRepositoryEntityKey) = bst.Id " + _
        "INNER JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId " + _
        "INNER JOIN model.PatientInsurances pins ON pins.Id = ir.PatientInsuranceId " + _
        "INNER JOIN model.InsurancePolicies ip ON pins.InsurancePolicyId = ip.Id " + _
        "INNER JOIN model.Insurers ins ON ins.Id = ip.InsurerId " + _
        "INNER JOIN model.Invoices inv ON ir.InvoiceId = inv.Id " + _
        "INNER JOIN model.Encounters e ON e.Id = inv.EncounterId " + _
        "INNER JOIN model.Patients p ON p.Id = e.PatientId " + _
        "INNER JOIN model.ExternalSystemExternalSystemMessageTypes esesmt ON esm.ExternalSystemExternalSystemMessageTypeId = esesmt.Id " + _
        "INNER JOIN model.ExternalSystemMessageTypes esmt ON esmt.Id = esesmt.ExternalSystemMessageTypeId " + _
        "INNER JOIN model.ExternalSystems es ON es.id = esesmt.ExternalSystemId " + _
        "WHERE es.Id = 4" + _
    criteriaString + ") " _
                )

                Dim control As New ReviewRecordsControl("Confirm the claims to be requeued:", selectQuery, updateQueries)
                InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = control, .ResizeMode = Windows.ResizeMode.NoResize})
            End If
        End Sub

    End Class
End Namespace