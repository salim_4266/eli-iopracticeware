Namespace RequeueRecalls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MainControl
        Inherits Soaf.Presentation.Controls.WindowsForms.UserControl

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.RequeueRecallsButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.DateLabel = New System.Windows.Forms.Label()
            Me.DatePicker = New System.Windows.Forms.DateTimePicker()
            Me.SuspendLayout()
            '
            'RequeueRecallsButton
            '
            Me.RequeueRecallsButton.Location = New System.Drawing.Point(343, 11)
            Me.RequeueRecallsButton.Name = "RequeueRecallsButton"
            Me.RequeueRecallsButton.Size = New System.Drawing.Size(118, 59)
            Me.RequeueRecallsButton.TabIndex = 20
            Me.RequeueRecallsButton.Text = "Requeue Recalls"
            Me.RequeueRecallsButton.Tooltip = Nothing
            '
            'DateLabel
            '
            Me.DateLabel.AutoSize = True
            Me.DateLabel.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.DateLabel.Location = New System.Drawing.Point(10, 29)
            Me.DateLabel.Name = "DateLabel"
            Me.DateLabel.Size = New System.Drawing.Size(173, 20)
            Me.DateLabel.TabIndex = 19
            Me.DateLabel.Text = "Requeue recalls for date:"
            '
            'DatePicker
            '
            Me.DatePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
            Me.DatePicker.Location = New System.Drawing.Point(189, 28)
            Me.DatePicker.Name = "DatePicker"
            Me.DatePicker.Size = New System.Drawing.Size(91, 20)
            Me.DatePicker.TabIndex = 21
            '
            'MainControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)

            Me.Controls.Add(Me.RequeueRecallsButton)
            Me.Controls.Add(Me.DateLabel)
            Me.Controls.Add(Me.DatePicker)
            Me.Name = "MainControl"
            Me.Size = New System.Drawing.Size(471, 81)
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents RequeueRecallsButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents DateLabel As System.Windows.Forms.Label
        Friend WithEvents DatePicker As System.Windows.Forms.DateTimePicker

    End Class
End Namespace