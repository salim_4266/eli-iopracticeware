Imports System.Collections.Generic
Imports Soaf.Presentation

Namespace RequeueRecalls
    Public Class MainControl

        Private Sub RequeueRecallsMainControl_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            DatePicker.Value = Now()
        End Sub

        Private Sub RequeueRecallsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RequeueRecallsButton.Click
            Dim SelectQuery As String = "select * from practicetransactionjournal where transactiontype = 'L' and transactionstatus = 'S' and transactiondate = '" + DatePicker.Value.ToString("yyyyMMdd") + "'"
            Dim UpdateQueries As New List(Of String)
            UpdateQueries.Add("update practicetransactionjournal set transactionstatus = 'P', transactiondate = replace(replace(transactionremark, '[', ''), ']', ''), transactionremark = '' where transactiontype = 'L' and transactionstatus = 'S' and transactiondate = '" + DatePicker.Value.ToString("yyyyMMdd") + "'")
            Dim control As New ReviewRecordsControl("Confirm the recalls to be requeued:", SelectQuery, UpdateQueries)
            InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = control, .ResizeMode = Windows.ResizeMode.NoResize})
        End Sub
    End Class
End Namespace