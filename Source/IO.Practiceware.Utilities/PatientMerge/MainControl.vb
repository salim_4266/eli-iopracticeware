Imports System.IO
Imports System.Windows.Forms
Imports IO.Practiceware.Utilities.Finders
Imports IO.Practiceware.Configuration
Imports System.Text.RegularExpressions
Imports Soaf.Data
Imports IO.Practiceware.Data
Imports Soaf.Presentation
Imports Soaf.Logging
Imports System.Windows
Imports IO.Practiceware.Storage
Imports System.Text
Imports Microsoft.WindowsAPICodePack.Dialogs
Imports LogCategory = IO.Practiceware.Logging.LogCategory

Namespace PatientMerge
    Public Class MainControl
        Public PatientToUseId As Integer = 0, PatientToOverwriteId As Integer = 0, ValuesDictionary As Dictionary(Of Integer, Integer)
        Private _logger As ILogger
        Private Property LogManager() As ILogManager

        Private Sub PatientMergeMainControl_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        End Sub

        Private Sub SelectPatientToUseClick(ByVal sender As Object, ByVal e As EventArgs) _
            Handles SelectPatientToUse.Click
            PatientToUseId = 0
            PatientToUseIdTextBox.Text = ""
            PatientToUseNameTextBox.Text = ""
            PatientToUseSSNTextBox.Text = ""
            PatientToUseAddressTextBox.Text = ""

            Dim patientId As Integer
            If Integer.TryParse(PatientToUseTextBox.Text, patientId) Then
                Dim dbConnection = DbConnectionFactory.PracticeRepository()
                Dim dbCommand = dbConnection.CreateCommand()
                dbCommand.CommandText = "SELECT Id As PatientId,ISNULL(FirstName,'') As FirstName,ISNULL(LastName,'') As LastName,ISNULL(SocialSecurityNumber,'') As  SocialSecurity " +
                                                ",(SELECT TOP 1 ISNULL(Line1,'') FROM model.PatientAddresses pa WHERE pa.PatientId = p.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS [Address] " +
                                                ",(SELECT TOP 1 ISNULL(Line2,'') FROM model.PatientAddresses pa WHERE pa.PatientId = p.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS Suite " +
                                                ",(SELECT TOP 1 ISNULL(City,'') FROM model.PatientAddresses pa WHERE pa.PatientId = p.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS City " +
                                                ",(SELECT TOP 1 ISNULL(Abbreviation,'') FROM model.StateOrProvinces sp WHERE sp.Id = (SELECT TOP 1 StateOrProvinceId FROM model.PatientAddresses pa WHERE pa.PatientId = p.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1)) AS [State] " +
                                                ",(SELECT TOP 1 ISNULL(PostalCode,'') FROM model.PatientAddresses pa WHERE pa.PatientId = p.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS Zip " +
                                                " from model.Patients p  WHERE Id = " + patientId.ToString()
                Try
                    dbConnection.Open()
                    If dbConnection.State = ConnectionState.Open Then
                        Dim reader = CType(dbCommand.ExecuteReader(), DataTableReader)
                        If reader.HasRows Then
                            reader.Read()
                            PatientToUseIdTextBox.Text = "Patient Id: " + Environment.NewLine +
                                                         reader("PatientId").ToString()
                            PatientToUseNameTextBox.Text = "Name: " + Environment.NewLine +
                                                           reader("FirstName").ToString() + " " +
                                                           reader("LastName").ToString()
                            PatientToUseSSNTextBox.Text = "SSN: " + Environment.NewLine +
                                                          reader("SocialSecurity").ToString()
                            PatientToUseAddressTextBox.Text = "Address: " + Environment.NewLine +
                                                              reader("Address").ToString() + " " +
                                                              reader("Suite").ToString() + Environment.NewLine +
                                                              reader("City").ToString() + ", " +
                                                              reader("State").ToString() + " " +
                                                              reader("Zip").ToString()
                            PatientToUseId = CType(reader("PatientId"), Integer)
                        Else
                            InteractionManager.Current.Alert("Patient not found.")
                        End If
                        reader.Close()
                        dbConnection.Close()
                    End If
                Catch ex As Exception
                    Trace.TraceError(ex.ToString())
                End Try
            Else
                InteractionManager.Current.Alert("Invalid Entry.")
            End If
        End Sub

        Private Sub SelectPatientToOverwriteClick(ByVal sender As Object, ByVal e As EventArgs) _
            Handles SelectPatientToOverwrite.Click
            PatientToOverwriteId = 0
            PatientToOverwriteIdTextBox.Text = ""
            PatientToOverwriteNameTextBox.Text = ""
            PatientToOverwriteSSNTextBox.Text = ""
            PatientToOverwriteAddressTextBox.Text = ""
            Dim patientId As Integer
            If Integer.TryParse(PatientToOverwriteTextBox.Text, patientId) Then
                Dim dbConnection = DbConnectionFactory.PracticeRepository()
                Dim dbCommand = dbConnection.CreateCommand()
                dbCommand.CommandText = "SELECT Id As PatientId,ISNULL(FirstName,'') As FirstName,ISNULL(LastName,'') As LastName,ISNULL(SocialSecurityNumber,'') As  SocialSecurity " +
                                                ",(SELECT TOP 1 ISNULL(Line1,'') FROM model.PatientAddresses pa WHERE pa.PatientId = p.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS [Address] " +
                                                ",(SELECT TOP 1 ISNULL(Line2,'') FROM model.PatientAddresses pa WHERE pa.PatientId = p.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS Suite " +
                                                ",(SELECT TOP 1 ISNULL(City,'') FROM model.PatientAddresses pa WHERE pa.PatientId = p.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS City " +
                                                ",(SELECT TOP 1 ISNULL(Abbreviation,'') FROM model.StateOrProvinces sp WHERE sp.Id = (SELECT TOP 1 StateOrProvinceId FROM model.PatientAddresses pa WHERE pa.PatientId = p.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1)) AS [State] " +
                                                ",(SELECT TOP 1 ISNULL(PostalCode,'') FROM model.PatientAddresses pa WHERE pa.PatientId = p.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS Zip " +
                                                " from model.Patients p  WHERE Id = " + patientId.ToString()
                Try
                    dbConnection.Open()
                    If dbConnection.State = ConnectionState.Open Then
                        Dim reader = CType(dbCommand.ExecuteReader(), DataTableReader)
                        If reader.HasRows Then
                            reader.Read()
                            PatientToOverwriteIdTextBox.Text = "Patient Id: " + Environment.NewLine +
                                                               reader("PatientId").ToString()
                            PatientToOverwriteNameTextBox.Text = "Name: " + Environment.NewLine +
                                                                 reader("FirstName").ToString() + " " +
                                                                 reader("LastName").ToString()
                            PatientToOverwriteSSNTextBox.Text = "SSN: " + Environment.NewLine +
                                                                reader("SocialSecurity").ToString()
                            PatientToOverwriteAddressTextBox.Text = "Address: " + Environment.NewLine +
                                                                    reader("Address").ToString() + " " +
                                                                    reader("Suite").ToString() + Environment.NewLine +
                                                                    reader("City").ToString() + ", " +
                                                                    reader("State").ToString() + " " +
                                                                    reader("Zip").ToString()
                            PatientToOverwriteId = CType(reader("PatientId"), Integer)
                        Else
                            InteractionManager.Current.Alert("Patient not found.")
                        End If
                        reader.Close()
                        dbConnection.Close()
                    End If
                Catch ex As Exception
                    Trace.TraceError(ex.ToString())
                End Try
            Else
                InteractionManager.Current.Alert("Invalid Entry.")
            End If
        End Sub

        Private Sub MergeButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles MergeButton.Click
            Try
                If Not IsNothing(ValuesDictionary) Then
                    If InteractionManager.Current.Confirm("Are you sure you want to merge these patients?") = True Then
                        Dim errorBuilder As New StringBuilder
                        Dim listPatients As String = String.Join(",", ValuesDictionary.Keys.ToArray)
                        listPatients += "," + String.Join(",", ValuesDictionary.Values.ToArray)
                        Dim listPatientNotExists As String() = GetPatientsNotExists(listPatients).Split(CChar(",")).ToArray
                        For Each pair As KeyValuePair(Of Integer, Integer) In ValuesDictionary
                            If listPatientNotExists.Contains(pair.Key.ToString) Or listPatientNotExists.Contains(pair.Value.ToString) Then
                                errorBuilder.AppendLine()
                                errorBuilder.Append(pair.Key.ToString + "," + pair.Value.ToString())
                            Else
                                MergePatients(pair.Key, pair.Value)
                            End If
                        Next
                        If errorBuilder.Length > 0 Then
                            errorBuilder.Insert(0, "The following patients doesn't exists and cannot be merged:")
                            Dim saveFileDialog As New SaveFileDialog
                            saveFileDialog.Filter = "csv files (*.csv)|*.csv"
                            saveFileDialog.FileName = "ErrorFile"
                            If saveFileDialog.ShowDialog() = DialogResult.OK Then
                                File.WriteAllText(saveFileDialog.FileName, errorBuilder.ToString())
                            End If
                        End If
                        InteractionManager.Current.Alert("Done merging.")
                    End If
                ElseIf PatientToUseId > 0 And PatientToOverwriteId > 0 Then
                    If InteractionManager.Current.Confirm("Are you sure you want to merge these patients?") = True Then
                        MergePatients(PatientToUseId, PatientToOverwriteId)
                        InteractionManager.Current.Alert("Done merging.")
                    End If
                Else
                    InteractionManager.Current.Alert("Please either upload the file or complete the form.")
                End If
            Catch ex As Exception
                Trace.TraceError(ex.ToString())
            End Try
        End Sub

        Private Sub FindPatientToUseButtonClick(ByVal sender As Object, ByVal e As EventArgs) _
            Handles FindPatientToUseButton.Click
            Dim control As New FindPatientControl
            InteractionManager.Current.ShowModal(New WindowInteractionArguments _
                                                    With {.Content = control, .ResizeMode = ResizeMode.NoResize})
            If control.PatientId > 0 Then
                PatientToUseTextBox.Text = control.PatientId.ToString()
                SelectPatientToUseClick(sender, e)
            End If
        End Sub

        Private Sub FindPatientToOverwriteButtonClick(ByVal sender As Object, ByVal e As EventArgs) _
            Handles FindPatientToOverwriteButton.Click
            Dim control As New FindPatientControl
            InteractionManager.Current.ShowModal(New WindowInteractionArguments _
                                                    With {.Content = control, .ResizeMode = ResizeMode.NoResize})
            If control.PatientId > 0 Then
                PatientToOverwriteTextBox.Text = control.PatientId.ToString()
                SelectPatientToOverwriteClick(sender, e)
            End If
        End Sub

        Private Sub UploadFileButton_Click(sender As Object, e As EventArgs) Handles UploadFileButton.Click
            ValuesDictionary = New Dictionary(Of Integer, Integer)
            Dim fileDialog As New CommonOpenFileDialog
            fileDialog.Title = "Please select a CSV file with two columns PatientId to keep & PatientId to be removed."
            fileDialog.AddToMostRecentlyUsedList = False
            fileDialog.AllowNonFileSystemItems = False
            fileDialog.EnsureFileExists = True
            fileDialog.EnsurePathExists = True
            fileDialog.EnsureReadOnly = False
            fileDialog.EnsureValidNames = True
            fileDialog.Multiselect = False
            fileDialog.ShowPlacesList = True
            fileDialog.Filters.Add(New CommonFileDialogFilter("csv files", "*.csv"))
            If fileDialog.ShowDialog() = DialogResult.OK Then
                Try
                    Dim isFirstrow As Boolean = True
                    Dim fileName As String = fileDialog.FileName
                    Dim sr As StreamReader = New StreamReader(fileName)
                    Do While sr.Peek() <> -1
                        Dim values As String() = sr.ReadLine().Split(CChar(","))
                        If isFirstrow Then
                            Dim firstColName As String = values(0)
                            Dim secondColName As String = values(1)
                            If firstColName <> "PatientId to keep" Or secondColName <> "PatientId to be removed" Then
                                InteractionManager.Current.Alert("Invalid column headers.")
                                Exit Sub
                            End If
                            isFirstrow = False
                        Else
                            Dim patientIdToKeep As Integer, patientIdToOverwrite As Integer
                            If Integer.TryParse(values(0), patientIdToKeep) And Integer.TryParse(values(1), patientIdToOverwrite) Then
                                ValuesDictionary.Add(patientIdToKeep, patientIdToOverwrite)
                            Else
                                InteractionManager.Current.Alert("Invalid patientIds in the file.")
                                Exit Sub
                            End If
                        End If
                    Loop
                    InteractionManager.Current.Alert("File has been uploaded successfully.")
                Catch ex As Exception
                    InteractionManager.Current.Alert("There is an error occurred while uploading the file.")
                End Try
            End If

        End Sub

        Private Sub MergePatients(patientToUseId As Integer, patientToOverwriteId As Integer)
            LogManager = Bootstrapper.Run(Of ILogManager)()
            _logger = LogManager.GetCurrentTypeLogger()
            Dim dbConnection = DbConnectionFactory.PracticeRepository()
            dbConnection.Open()
            If dbConnection.State = ConnectionState.Open Then
                dbConnection.Execute("SELECT 1 AS Value INTO #NoCheck")
                Dim dbCommand = dbConnection.CreateCommand()
                dbCommand.CommandText = "UPDATE dbo.Appointments SET PatientId = " + patientToUseId.ToString() +
                                        " WHERE PatientId = " + patientToOverwriteId.ToString()
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE model.Encounters SET PatientId = " + patientToUseId.ToString() +
                                        " WHERE PatientId = " + patientToOverwriteId.ToString()
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE PracticeActivity SET PatientId = " +
                                        patientToUseId.ToString() + " WHERE PatientId = " +
                                        patientToOverwriteId.ToString()
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE PatientClinical SET PatientId = " +
                                        patientToUseId.ToString() + " WHERE PatientId = " +
                                        patientToOverwriteId.ToString()
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE PatientNotes SET PatientId = " + patientToUseId.ToString() +
                                        " WHERE PatientId = " + patientToOverwriteId.ToString()
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE pir " +
                                        "SET PatientId = " + patientToUseId.ToString() +
                                        " FROM model.PatientInsuranceReferrals pir" +
                                        " WHERE PatientId = " + patientToOverwriteId.ToString()
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE ad" +
                                        " SET PatientId = " + patientToUseId.ToString() +
                                        " FROM model.Adjustments ad" +
                                        " WHERE PatientId = " + patientToOverwriteId.ToString() +
                                        " AND FinancialSourceTypeId = 2"
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE fi" +
                                        " SET PatientId = " + patientToUseId.ToString() +
                                        " FROM model.FinancialInformations fi" +
                                        " WHERE PatientId = " + patientToOverwriteId.ToString() +
                                        " AND FinancialSourceTypeId = 2"
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE fb" +
                                        " SET PatientId = " + patientToUseId.ToString() +
                                        " FROM model.FinancialBatches fb" +
                                        " WHERE PatientId = " + patientToOverwriteId.ToString() +
                                        " AND FinancialSourceTypeId = 2"
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE model.Invoices " +
                                        " SET LegacyPatientReceivablesPatientId = " + patientToUseId.ToString() +
                                        " WHERE LegacyPatientReceivablesPatientId = " + patientToOverwriteId.ToString()
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE model.Invoices SET LegacyPatientReceivablesInvoice = REPLACE(LegacyPatientReceivablesInvoice, '" +
                                       patientToOverwriteId.ToString().PadLeft(5, "0"c) + "', '" +
                                       patientToUseId.ToString().PadLeft(5, "0"c) +
                                       "') WHERE LegacyPatientReceivablesInvoice LIKE '" +
                                       patientToOverwriteId.ToString().PadLeft(5, "0"c) + "-%'"
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE PracticeTransactionJournal SET TransactionTypeId = " +
                                        patientToUseId.ToString() + " WHERE TransactionTypeId = " +
                                        patientToOverwriteId.ToString() + " AND TransactionType = 'L'"
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE PracticeTransactionJournal SET TransactionRcvrId = " +
                                        patientToUseId.ToString() + " WHERE TransactionRcvrId = " +
                                        patientToOverwriteId.ToString() +
                                        " AND TransactionType = 'R' AND TransactionRef = 'G'"
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE ip " +
                                       "SET PolicyHolderPatientId = " + patientToUseId.ToString() +
                                       " FROM model.InsurancePolicies ip" +
                                       " WHERE PolicyHolderPatientId = " + patientToOverwriteId.ToString()
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE pi " +
                                        "SET InsuredPatientId = " + patientToUseId.ToString() +
                                        " FROM model.PatientInsurances pi" +
                                        " WHERE InsuredPatientId = " + patientToOverwriteId.ToString()
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE model.PatientDiagnosisComments SET PatientId=" + patientToUseId.ToString() +
                                      " WHERE PatientId=" + patientToOverwriteId.ToString() + ""
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE model.PatientAllergenComments SET PatientId=" + patientToUseId.ToString() +
                                      " WHERE PatientId=" + patientToOverwriteId.ToString() + ""
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE model.PatientMedicalReviews SET PatientId=" + patientToUseId.ToString() +
                                      " WHERE PatientId=" + patientToOverwriteId.ToString() + ""
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE model.CommunicationTransactions SET PatientId=" + patientToUseId.ToString() +
                                      " WHERE PatientId=" + patientToOverwriteId.ToString() + ""
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE model.PatientFamilyHistoryEntries SET PatientId=" + patientToUseId.ToString() +
                                      " WHERE PatientId=" + patientToOverwriteId.ToString() + ""
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE model.PatientFinancialComments SET PatientId=" + patientToUseId.ToString() +
                                      " WHERE PatientId=" + patientToOverwriteId.ToString() + ""
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE model.Tasks SET PatientId=" + patientToUseId.ToString() +
                                      " WHERE PatientId=" + patientToOverwriteId.ToString() + ""
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE model.X12EligibilityPatients SET PatientId=" + patientToUseId.ToString() +
                                      " WHERE PatientId=" + patientToOverwriteId.ToString() + ""
                dbCommand.ExecuteNonQuery()
                'Update MU Tables
                dbCommand.CommandText = "UPDATE dbo.SurveillanceMessageType SET PatientId = '" + patientToUseId.ToString() + "' " +
                                        " WHERE PatientID = '" + patientToOverwriteId.ToString() + "' "
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE dbo.User_Patients_Restrict SET PatientId = '" + patientToUseId.ToString() + "' " +
                                        " WHERE PatientID = '" + patientToOverwriteId.ToString() + "' "
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE dbo.Patient_MedicationsXMl SET PatientId = '" + patientToUseId.ToString() + "' " +
                                        " WHERE PatientID = '" + patientToOverwriteId.ToString() + "' "
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE dbo.Patient_Immunizations SET PatientId = '" + patientToUseId.ToString() + "' " +
                                        " WHERE PatientID = '" + patientToOverwriteId.ToString() + "' "
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE dbo.Patient_Images SET PatientId = '" + patientToUseId.ToString() + "' " +
                                        " WHERE PatientID = '" + patientToOverwriteId.ToString() + "' "
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE dbo.Patient_EPrescription_RenewalRequest SET PatientId = '" + patientToUseId.ToString() + "' " +
                                        " WHERE PatientID = '" + patientToOverwriteId.ToString() + "' "
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE dbo.Patient_Contacts SET PatientId = '" + patientToUseId.ToString() + "' " +
                                        " WHERE PatientID = '" + patientToOverwriteId.ToString() + "' "
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE dbo.IE_Rules SET PatientId = '" + patientToUseId.ToString() + "' " +
                                        " WHERE PatientID = '" + patientToOverwriteId.ToString() + "' "
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE dbo.CLOrders SET PatientId = '" + patientToUseId.ToString() + "' " +
                                        " WHERE PatientID = '" + patientToOverwriteId.ToString() + "' "
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE dbo.HL7_LabInterface SET PatientId = '" + patientToUseId.ToString() + "' " +
                                        " WHERE PatientID = '" + patientToOverwriteId.ToString() + "' "
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText = "UPDATE dbo.HL7_LabReports SET PatientId = '" + patientToUseId.ToString() + "' " +
                                        " WHERE PatientID = '" + patientToOverwriteId.ToString() + "' "
                dbCommand.ExecuteNonQuery()
                'Update Patient Details at the end
                dbCommand.CommandText = "UPDATE model.PatientComments SET PatientCommentTypeId = 2,Value = 'DO NOT USE THIS A/C. USE A/C " +
                    patientToUseId.ToString + "' + ISNULL(Value,'')  WHERE PatientId = " + patientToOverwriteId.ToString()
                dbCommand.CommandText =
                    "UPDATE model.Patients SET LastName = 'ZZZ' + LastName, PatientStatusId = 2 WHERE Id = " + patientToOverwriteId.ToString()
                dbCommand.ExecuteNonQuery()

                dbCommand.CommandText =
                    "UPDATE dbo.PatientClinicalIcd10 SET PatientId = " + patientToUseId.ToString() + " WHERE PatientId = " + patientToOverwriteId.ToString()
                dbCommand.ExecuteNonQuery()

                dbCommand.CommandText =
                    "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ExternalSystemMapping]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) update ESM set ESM. PracticeRepositoryEntityKey = '" +
                    patientToUseId.ToString() +
                    "' from model.ExternalSystemMapping ESM inner join model.PracticeRepositoryEntity PRT on PRT.PracticeRepositoryEntityId = ESM.PracticeRepositoryEntityId where PRT.Name = 'PatientDemographic' and ESM.PracticeRepositoryEntityKey = " +
                    patientToOverwriteId.ToString()
                dbCommand.ExecuteNonQuery()
                dbCommand.CommandText =
                    "IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[model].[ExternalSystemEntityMappings]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) UPDATE m SET m.PracticeRepositoryEntityKey = '" +
                    patientToUseId.ToString() +
                    "' FROM model.ExternalSystemEntityMappings m INNER JOIN model.ExternalSystemEntities e ON e.Id = m.ExternalSystemEntityId WHERE e.Name = 'Patient' AND m.PracticeRepositoryEntityKey = '" +
                    patientToOverwriteId.ToString() + "'"
                dbCommand.ExecuteNonQuery()
                dbConnection.Close()
                Dim files() As String = FileManager.Instance.ListFiles(ConfigurationManager.ServerDataPath + "\MyScan")
                For Each fileName As String In files
                    Dim expression As New Regex(".*?(_" + patientToOverwriteId.ToString() + "-).*")
                    If expression.IsMatch(fileName) Then
                        If FileManager.Instance.FileExists(fileName) Then
                            FileManager.Instance.Move(fileName,
                                      fileName.Replace("_" + patientToOverwriteId.ToString() + "-",
                                                       "_" + patientToUseId.ToString + "-"))
                        End If
                    End If
                Next

                Dim sourceDirPath As String = ConfigurationManager.ServerDataPath + "\MyScan\" + patientToOverwriteId.ToString()
                Dim destinationDirPath As String = ConfigurationManager.ServerDataPath + "\MyScan\" + patientToUseId.ToString()
                Dim destinationFileName As String
                Dim sourceFileName As String

                If (FileManager.Instance.DirectoryExists(sourceDirPath)) Then
                    If Not (FileManager.Instance.DirectoryExists(destinationDirPath)) Then
                        FileManager.Instance.CreateDirectory(destinationDirPath)
                    End If
                    For Each sourceFilePath In FileManager.Instance.ListFiles(sourceDirPath, True)
                        sourceFileName = sourceDirPath + "\" + Path.GetFileName(sourceFilePath)
                        destinationFileName = destinationDirPath + "\" + Path.GetFileName(sourceFilePath).Replace("_" + patientToOverwriteId.ToString() + "-", "_" + patientToUseId.ToString + "-")
                        FileManager.Instance.Move(sourceFileName, destinationFileName)
                    Next
                End If

                _logger.Log("Merged Patient Records", categories:={LogCategory.Utilities})
            End If

        End Sub

        Function GetPatientsNotExists(listPatient As String) As String
            Dim listPatientsNotExists As String = String.Empty
            Dim dbConnection = DbConnectionFactory.PracticeRepository()
            Dim dbCommand = dbConnection.CreateCommand()
            dbCommand.CommandText = " DECLARE @ListPatientsNotExists VARCHAR(Max) " +
                                    "SELECT @ListPatientsNotExists = COALESCE(@ListPatientsNotExists+',','') + list.patientid FROM " +
                                    "(SELECT CONVERT(NVARCHAR,nstr) AS patientId FROM CharTable('" + listPatient + "',',')) list " +
                                    "WHERE list.patientid not in (SELECT Id FROM model.Patients WHERE ID in (" + listPatient + ") AND patientStatusId <>2) " +
                                    "SELECT @ListPatientsNotExists AS 'ListPatientsNotExists' "
            Try
                dbConnection.Open()
                If dbConnection.State = ConnectionState.Open Then
                    listPatientsNotExists = CStr(dbCommand.ExecuteScalar())
                    dbConnection.Close()
                End If
            Catch ex As Exception
                Trace.TraceError(ex.ToString())
            End Try
            Return listPatientsNotExists
        End Function
    End Class
End Namespace