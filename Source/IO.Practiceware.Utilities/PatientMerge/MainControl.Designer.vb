Namespace PatientMerge
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MainControl
        Inherits Soaf.Presentation.Controls.WindowsForms.UserControl

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.PatientToOverwriteAddressTextBox = New System.Windows.Forms.Label()
            Me.FindPatientToOverwriteButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.PatientToOverwriteDetails = New System.Windows.Forms.GroupBox()
            Me.PatientToOverwriteSSNTextBox = New System.Windows.Forms.Label()
            Me.PatientToOverwriteIdTextBox = New System.Windows.Forms.Label()
            Me.PatientToOverwriteNameTextBox = New System.Windows.Forms.Label()
            Me.FindPatientToUseButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.PatientToOverwriteLabel = New System.Windows.Forms.Label()
            Me.MergeButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.SelectPatientToOverwrite = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.PatientToUseAddressTextBox = New System.Windows.Forms.Label()
            Me.PatientToOverwriteTextBox = New System.Windows.Forms.TextBox()
            Me.PatientToUseDetails = New System.Windows.Forms.GroupBox()
            Me.PatientToUseSSNTextBox = New System.Windows.Forms.Label()
            Me.PatientToUseNameTextBox = New System.Windows.Forms.Label()
            Me.PatientToUseIdTextBox = New System.Windows.Forms.Label()
            Me.SelectPatientToUse = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.PatientToUseLabel = New System.Windows.Forms.Label()
            Me.PatientToUseTextBox = New System.Windows.Forms.TextBox()
            Me.UploadFileButton = New Soaf.Presentation.Controls.WindowsForms.Button()
            Me.PatientToOverwriteDetails.SuspendLayout()
            Me.PatientToUseDetails.SuspendLayout()
            Me.SuspendLayout()
            '
            'PatientToOverwriteAddressTextBox
            '
            Me.PatientToOverwriteAddressTextBox.AutoSize = True
            Me.PatientToOverwriteAddressTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToOverwriteAddressTextBox.Location = New System.Drawing.Point(131, 57)
            Me.PatientToOverwriteAddressTextBox.Name = "PatientToOverwriteAddressTextBox"
            Me.PatientToOverwriteAddressTextBox.Size = New System.Drawing.Size(0, 13)
            Me.PatientToOverwriteAddressTextBox.TabIndex = 4
            '
            'FindPatientToOverwriteButton
            '
            Me.FindPatientToOverwriteButton.Location = New System.Drawing.Point(366, 201)
            Me.FindPatientToOverwriteButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.FindPatientToOverwriteButton.Name = "FindPatientToOverwriteButton"
            Me.FindPatientToOverwriteButton.Size = New System.Drawing.Size(55, 24)
            Me.FindPatientToOverwriteButton.TabIndex = 20
            Me.FindPatientToOverwriteButton.Text = "Find"
            Me.FindPatientToOverwriteButton.Tooltip = Nothing
            '
            'PatientToOverwriteDetails
            '
            Me.PatientToOverwriteDetails.Controls.Add(Me.PatientToOverwriteAddressTextBox)
            Me.PatientToOverwriteDetails.Controls.Add(Me.PatientToOverwriteSSNTextBox)
            Me.PatientToOverwriteDetails.Controls.Add(Me.PatientToOverwriteIdTextBox)
            Me.PatientToOverwriteDetails.Controls.Add(Me.PatientToOverwriteNameTextBox)
            Me.PatientToOverwriteDetails.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToOverwriteDetails.Location = New System.Drawing.Point(24, 232)
            Me.PatientToOverwriteDetails.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.PatientToOverwriteDetails.Name = "PatientToOverwriteDetails"
            Me.PatientToOverwriteDetails.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.PatientToOverwriteDetails.Size = New System.Drawing.Size(295, 140)
            Me.PatientToOverwriteDetails.TabIndex = 13
            Me.PatientToOverwriteDetails.TabStop = False
            Me.PatientToOverwriteDetails.Text = "Patient Details"
            '
            'PatientToOverwriteSSNTextBox
            '
            Me.PatientToOverwriteSSNTextBox.AutoSize = True
            Me.PatientToOverwriteSSNTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToOverwriteSSNTextBox.Location = New System.Drawing.Point(131, 21)
            Me.PatientToOverwriteSSNTextBox.Name = "PatientToOverwriteSSNTextBox"
            Me.PatientToOverwriteSSNTextBox.Size = New System.Drawing.Size(0, 13)
            Me.PatientToOverwriteSSNTextBox.TabIndex = 5
            '
            'PatientToOverwriteIdTextBox
            '
            Me.PatientToOverwriteIdTextBox.AutoSize = True
            Me.PatientToOverwriteIdTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToOverwriteIdTextBox.Location = New System.Drawing.Point(15, 21)
            Me.PatientToOverwriteIdTextBox.Name = "PatientToOverwriteIdTextBox"
            Me.PatientToOverwriteIdTextBox.Size = New System.Drawing.Size(0, 13)
            Me.PatientToOverwriteIdTextBox.TabIndex = 3
            '
            'PatientToOverwriteNameTextBox
            '
            Me.PatientToOverwriteNameTextBox.AutoSize = True
            Me.PatientToOverwriteNameTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToOverwriteNameTextBox.Location = New System.Drawing.Point(15, 57)
            Me.PatientToOverwriteNameTextBox.Name = "PatientToOverwriteNameTextBox"
            Me.PatientToOverwriteNameTextBox.Size = New System.Drawing.Size(0, 13)
            Me.PatientToOverwriteNameTextBox.TabIndex = 4
            '
            'FindPatientToUseButton
            '
            Me.FindPatientToUseButton.Location = New System.Drawing.Point(366, 15)
            Me.FindPatientToUseButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.FindPatientToUseButton.Name = "FindPatientToUseButton"
            Me.FindPatientToUseButton.Size = New System.Drawing.Size(55, 24)
            Me.FindPatientToUseButton.TabIndex = 19
            Me.FindPatientToUseButton.Text = "Find"
            Me.FindPatientToUseButton.Tooltip = Nothing
            '
            'PatientToOverwriteLabel
            '
            Me.PatientToOverwriteLabel.AutoSize = True
            Me.PatientToOverwriteLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToOverwriteLabel.Location = New System.Drawing.Point(21, 206)
            Me.PatientToOverwriteLabel.Name = "PatientToOverwriteLabel"
            Me.PatientToOverwriteLabel.Size = New System.Drawing.Size(124, 13)
            Me.PatientToOverwriteLabel.TabIndex = 17
            Me.PatientToOverwriteLabel.Text = "Patient Id To Overwrite"
            '
            'MergeButton
            '
            Me.MergeButton.Location = New System.Drawing.Point(316, 401)
            Me.MergeButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.MergeButton.Name = "MergeButton"
            Me.MergeButton.Size = New System.Drawing.Size(105, 60)
            Me.MergeButton.TabIndex = 18
            Me.MergeButton.Text = "Merge"
            Me.MergeButton.Tooltip = Nothing
            '
            'SelectPatientToOverwrite
            '
            Me.SelectPatientToOverwrite.Location = New System.Drawing.Point(263, 201)
            Me.SelectPatientToOverwrite.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.SelectPatientToOverwrite.Name = "SelectPatientToOverwrite"
            Me.SelectPatientToOverwrite.Size = New System.Drawing.Size(88, 24)
            Me.SelectPatientToOverwrite.TabIndex = 16
            Me.SelectPatientToOverwrite.Text = "Select Patient"
            Me.SelectPatientToOverwrite.Tooltip = Nothing
            '
            'PatientToUseAddressTextBox
            '
            Me.PatientToUseAddressTextBox.AutoSize = True
            Me.PatientToUseAddressTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToUseAddressTextBox.Location = New System.Drawing.Point(131, 55)
            Me.PatientToUseAddressTextBox.Name = "PatientToUseAddressTextBox"
            Me.PatientToUseAddressTextBox.Size = New System.Drawing.Size(0, 13)
            Me.PatientToUseAddressTextBox.TabIndex = 3
            '
            'PatientToOverwriteTextBox
            '
            Me.PatientToOverwriteTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToOverwriteTextBox.Location = New System.Drawing.Point(161, 203)
            Me.PatientToOverwriteTextBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.PatientToOverwriteTextBox.Name = "PatientToOverwriteTextBox"
            Me.PatientToOverwriteTextBox.Size = New System.Drawing.Size(86, 22)
            Me.PatientToOverwriteTextBox.TabIndex = 15
            '
            'PatientToUseDetails
            '
            Me.PatientToUseDetails.Controls.Add(Me.PatientToUseAddressTextBox)
            Me.PatientToUseDetails.Controls.Add(Me.PatientToUseSSNTextBox)
            Me.PatientToUseDetails.Controls.Add(Me.PatientToUseNameTextBox)
            Me.PatientToUseDetails.Controls.Add(Me.PatientToUseIdTextBox)
            Me.PatientToUseDetails.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToUseDetails.Location = New System.Drawing.Point(24, 47)
            Me.PatientToUseDetails.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.PatientToUseDetails.Name = "PatientToUseDetails"
            Me.PatientToUseDetails.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.PatientToUseDetails.Size = New System.Drawing.Size(295, 140)
            Me.PatientToUseDetails.TabIndex = 11
            Me.PatientToUseDetails.TabStop = False
            Me.PatientToUseDetails.Text = "Patient Details"
            '
            'PatientToUseSSNTextBox
            '
            Me.PatientToUseSSNTextBox.AutoSize = True
            Me.PatientToUseSSNTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToUseSSNTextBox.Location = New System.Drawing.Point(131, 19)
            Me.PatientToUseSSNTextBox.Name = "PatientToUseSSNTextBox"
            Me.PatientToUseSSNTextBox.Size = New System.Drawing.Size(0, 13)
            Me.PatientToUseSSNTextBox.TabIndex = 2
            '
            'PatientToUseNameTextBox
            '
            Me.PatientToUseNameTextBox.AutoSize = True
            Me.PatientToUseNameTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToUseNameTextBox.Location = New System.Drawing.Point(15, 55)
            Me.PatientToUseNameTextBox.Name = "PatientToUseNameTextBox"
            Me.PatientToUseNameTextBox.Size = New System.Drawing.Size(0, 13)
            Me.PatientToUseNameTextBox.TabIndex = 1
            '
            'PatientToUseIdTextBox
            '
            Me.PatientToUseIdTextBox.AutoSize = True
            Me.PatientToUseIdTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToUseIdTextBox.Location = New System.Drawing.Point(15, 19)
            Me.PatientToUseIdTextBox.Name = "PatientToUseIdTextBox"
            Me.PatientToUseIdTextBox.Size = New System.Drawing.Size(0, 13)
            Me.PatientToUseIdTextBox.TabIndex = 0
            '
            'SelectPatientToUse
            '
            Me.SelectPatientToUse.Location = New System.Drawing.Point(263, 15)
            Me.SelectPatientToUse.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.SelectPatientToUse.Name = "SelectPatientToUse"
            Me.SelectPatientToUse.Size = New System.Drawing.Size(88, 24)
            Me.SelectPatientToUse.TabIndex = 12
            Me.SelectPatientToUse.Text = "Select Patient"
            Me.SelectPatientToUse.Tooltip = Nothing
            '
            'PatientToUseLabel
            '
            Me.PatientToUseLabel.AutoSize = True
            Me.PatientToUseLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToUseLabel.Location = New System.Drawing.Point(21, 19)
            Me.PatientToUseLabel.Name = "PatientToUseLabel"
            Me.PatientToUseLabel.Size = New System.Drawing.Size(93, 13)
            Me.PatientToUseLabel.TabIndex = 14
            Me.PatientToUseLabel.Text = "Patient Id To Use"
            '
            'PatientToUseTextBox
            '
            Me.PatientToUseTextBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PatientToUseTextBox.Location = New System.Drawing.Point(161, 17)
            Me.PatientToUseTextBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.PatientToUseTextBox.Name = "PatientToUseTextBox"
            Me.PatientToUseTextBox.Size = New System.Drawing.Size(86, 22)
            Me.PatientToUseTextBox.TabIndex = 10
            '
            'UploadFileButton
            '
            Me.UploadFileButton.Location = New System.Drawing.Point(219, 401)
            Me.UploadFileButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.UploadFileButton.Name = "UploadFileButton"
            Me.UploadFileButton.Size = New System.Drawing.Size(82, 60)
            Me.UploadFileButton.TabIndex = 21
            Me.UploadFileButton.Text = "Upload File"
            Me.UploadFileButton.Tooltip = Nothing
            '
            'MainControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)

            Me.Controls.Add(Me.UploadFileButton)
            Me.Controls.Add(Me.FindPatientToOverwriteButton)
            Me.Controls.Add(Me.PatientToOverwriteDetails)
            Me.Controls.Add(Me.FindPatientToUseButton)
            Me.Controls.Add(Me.PatientToOverwriteLabel)
            Me.Controls.Add(Me.MergeButton)
            Me.Controls.Add(Me.SelectPatientToOverwrite)
            Me.Controls.Add(Me.PatientToOverwriteTextBox)
            Me.Controls.Add(Me.PatientToUseDetails)
            Me.Controls.Add(Me.SelectPatientToUse)
            Me.Controls.Add(Me.PatientToUseLabel)
            Me.Controls.Add(Me.PatientToUseTextBox)
            Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.Name = "MainControl"
            Me.Size = New System.Drawing.Size(443, 476)
            Me.PatientToOverwriteDetails.ResumeLayout(False)
            Me.PatientToOverwriteDetails.PerformLayout()
            Me.PatientToUseDetails.ResumeLayout(False)
            Me.PatientToUseDetails.PerformLayout()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents PatientToOverwriteAddressTextBox As System.Windows.Forms.Label
        Friend WithEvents FindPatientToOverwriteButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents PatientToOverwriteDetails As System.Windows.Forms.GroupBox
        Friend WithEvents PatientToOverwriteSSNTextBox As System.Windows.Forms.Label
        Friend WithEvents PatientToOverwriteIdTextBox As System.Windows.Forms.Label
        Friend WithEvents PatientToOverwriteNameTextBox As System.Windows.Forms.Label
        Friend WithEvents FindPatientToUseButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents PatientToOverwriteLabel As System.Windows.Forms.Label
        Friend WithEvents MergeButton As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents SelectPatientToOverwrite As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents PatientToUseAddressTextBox As System.Windows.Forms.Label
        Friend WithEvents PatientToOverwriteTextBox As System.Windows.Forms.TextBox
        Friend WithEvents PatientToUseDetails As System.Windows.Forms.GroupBox
        Friend WithEvents PatientToUseSSNTextBox As System.Windows.Forms.Label
        Friend WithEvents PatientToUseNameTextBox As System.Windows.Forms.Label
        Friend WithEvents PatientToUseIdTextBox As System.Windows.Forms.Label
        Friend WithEvents SelectPatientToUse As Soaf.Presentation.Controls.WindowsForms.Button
        Friend WithEvents PatientToUseLabel As System.Windows.Forms.Label
        Friend WithEvents PatientToUseTextBox As System.Windows.Forms.TextBox
        Friend WithEvents UploadFileButton As Soaf.Presentation.Controls.WindowsForms.Button

    End Class
End Namespace