﻿IF OBJECT_ID('ArchiveTable') IS NOT NULL
	DROP PROCEDURE ArchiveTable

GO

CREATE PROCEDURE ArchiveTable(@tableName nvarchar(max), @deleteFilter nvarchar(max))
AS
BEGIN

--DECLARE @tableName nvarchar(max)
--DECLARE @deleteFilter nvarchar(max)

--SET @tableName = 'dbo.LogEntryProperties'
--SET @deleteFilter = 'LogEntryId IN (SELECT Id FROM dbo.LogEntries WHERE DateTime <= DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, -30, GETDATE()))))'

--SET @tableName = 'dbo.LogEntryLogCategory'
--SET @deleteFilter = 'LogEntryLogCategory_LogCategory_Id IN (SELECT Id FROM dbo.LogEntries WHERE DateTime <= DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, -30, GETDATE()))))'

--SET @tableName = 'dbo.LogEntries'
--SET @deleteFilter = 'DateTime <= DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, -30, GETDATE())))'

--SET @tableName = 'dbo.AuditEntryChanges'
--SET @deleteFilter = 'AuditEntryId IN (SELECT Id FROM dbo.AuditEntries WHERE AuditDateTime <= DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, -30, GETDATE()))))'

--SET @tableName = 'dbo.AuditEntryKeyValues'
--SET @deleteFilter = 'AuditEntryId IN (SELECT Id FROM dbo.AuditEntries WHERE AuditDateTime <= DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, -30, GETDATE()))))'

--SET @tableName = 'dbo.AuditEntries'
--SET @deleteFilter = 'AuditDateTime <= DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, -30, GETDATE())))'

SET NOCOUNT ON

DECLARE @disableConstraintsSql nvarchar(max)
SET @disableConstraintsSql = ''

;WITH x AS 
(
  SELECT DISTINCT obj = 
      QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id)) + '.' 
    + QUOTENAME(OBJECT_NAME(parent_object_id)) 
  FROM sys.foreign_keys
)
SELECT @disableConstraintsSql = @disableConstraintsSql + N'ALTER TABLE ' + obj + ' NOCHECK CONSTRAINT ALL;
' FROM x;

EXEC sp_executesql @disableConstraintsSql;


DECLARE @archiveDbSql nvarchar(max)

DECLARE @archiveDbName nvarchar(max)
SET @archiveDbName = DB_NAME() + '_Archive'


-- Rename existing db
IF EXISTS(SELECT * FROM sys.databases WHERE name = 'AuditEntryArchiveRepository_' + DB_NAME())
BEGIN
	SET @archiveDbSql = 'ALTER DATABASE [AuditEntryArchiveRepository_' + DB_NAME() + '] MODIFY NAME = ' + QUOTENAME(@archiveDbName)
	EXEC sp_executesql @statement = @archiveDbSql
END

-- Create db
IF NOT EXISTS(SELECT * FROM sys.databases d WHERE d.name = @archiveDbName)
BEGIN
	SET @archiveDbSql = 'CREATE DATABASE ' + QUOTENAME(@archiveDbName) + '; ALTER DATABASE ' + QUOTENAME(@archiveDbName) + ' SET RECOVERY SIMPLE;'
	EXEC sp_executesql @statement = @archiveDbSql
END


-- Create function to get table definitions
IF OBJECT_ID('dbo.GetTableDefinition') IS NOT NULL
	DROP FUNCTION dbo.GetTableDefinition

IF OBJECT_ID(QUOTENAME(@archiveDbName) + '.dbo.GetTableDefinition') IS NOT NULL
BEGIN
	DECLARE @dropArchiveDbGetTableDefinitionSql nvarchar(max)
	SET @dropArchiveDbGetTableDefinitionSql  = 'USE ' + QUOTENAME(@archiveDbName) + '; DROP FUNCTION dbo.GetTableDefinition'
	EXEC(@dropArchiveDbGetTableDefinitionSql)
END

DECLARE @createGetTableDefinitionSql nvarchar(max)
SET @createGetTableDefinitionSql = 'CREATE FUNCTION dbo.GetTableDefinition(@tableName nvarchar(max), @targetDbName nvarchar(max))
RETURNS nvarchar(max)
AS
BEGIN
DECLARE @columnsDefinition nvarchar(max)
SET @columnsDefinition = ''(''

DECLARE @columnDefinitions table(Definition nvarchar(max))

INSERT INTO @columnDefinitions(Definition)
SELECT 	
	c.COLUMN_NAME + '' ''
	+ c.DATA_TYPE 
	+ (
	CASE 
		WHEN c.CHARACTER_MAXIMUM_LENGTH IS NULL THEN '' ''
		WHEN c.CHARACTER_MAXIMUM_LENGTH IS NOT NULL AND c.CHARACTER_MAXIMUM_LENGTH = -1 THEN ''(max) ''
		ELSE ''('' + CONVERT(nvarchar(max), c.CHARACTER_MAXIMUM_LENGTH) + '') ''
	END
	)
	+ ''NULL, '' AS Definition
FROM INFORMATION_SCHEMA.COLUMNS c
WHERE OBJECT_ID(QUOTENAME(c.TABLE_SCHEMA) + ''.'' + QUOTENAME(c.TABLE_NAME)) = OBJECT_ID(@tableName) 
ORDER BY c.ORDINAL_POSITION

SELECT @columnsDefinition = @columnsDefinition + Definition
FROM @columnDefinitions

SET @columnsDefinition = SUBSTRING(@columnsDefinition, 1, LEN(@columnsDefinition) - 1) + '')''

DECLARE @createTableSql nvarchar(max)
SELECT @createTableSql  = ''CREATE TABLE '' + COALESCE(QUOTENAME(@targetDbName), DB_NAME()) + ''.'' + QUOTENAME(t.TABLE_SCHEMA) + ''.'' + QUOTENAME(t.TABLE_NAME) + @columnsDefinition
FROM INFORMATION_SCHEMA.TABLES t
WHERE OBJECT_ID(QUOTENAME(t.TABLE_SCHEMA) + ''.'' + QUOTENAME(t.TABLE_NAME)) = OBJECT_ID(@tableName)

RETURN @createTableSql
END
'

EXEC(@createGetTableDefinitionSql)
DECLARE @createArchiveDbGetTableDefinitionSql nvarchar(max)
SET @createArchiveDbGetTableDefinitionSql  = 'USE ' + QUOTENAME(@archiveDbName) + '; EXEC sp_executesql @statement = N''' + REPLACE(@createGetTableDefinitionSql, '''', '''''') + ''''
EXEC(@createArchiveDbGetTableDefinitionSql)

-- Get SQL to create the archive table
DECLARE @createTableSql nvarchar(max)
SET @createTableSql = dbo.GetTableDefinition(@tableName, @archiveDbName)

-- in case table definition has changed
DECLARE @currentNameCount int
SET @currentNameCount = 1

DECLARE @auditDbTableDefinition nvarchar(max)
DECLARE @getAuditDbTableDefinition nvarchar(max)
SET @getAuditDbTableDefinition  = 'USE ' + QUOTENAME(@archiveDbName) + ';SELECT @auditDbTableDefinition = dbo.GetTableDefinition(''' + @tableName + ''', ''' + @archiveDbName + ''')'
EXEC sp_executesql @getAuditDbTableDefinition, N'@auditDbTableDefinition nvarchar(max) OUTPUT', @auditDbTableDefinition = @auditDbTableDefinition OUTPUT

-- We need to rename the preivous table because the definition has changed
IF OBJECT_ID(QUOTENAME(@archiveDbName) + '.' + @tableName) IS NOT NULL AND dbo.GetTableDefinition(@tableName, @archiveDbName) <> @auditDbTableDefinition
BEGIN
	DECLARE @hasConflictingName bit
	DECLARE @hasConflictingNameSql nvarchar(max)
	SET @hasConflictingNameSql = 'USE ' + QUOTENAME(@archiveDbName) + '; SELECT @hasConflictingName = CASE WHEN OBJECT_ID(''' + @tableName + '_' + CONVERT(nvarchar(max), @currentNameCount) + ''') IS NULL THEN 0 ELSE 1 END'
	EXEC sp_executesql @hasConflictingNameSql, N'@hasConflictingName bit OUTPUT', @hasConflictingName = @hasConflictingName OUTPUT
	
	WHILE @hasConflictingName = 1
	BEGIN
		SET @currentNameCount = @currentNameCount + 1
		SET @hasConflictingNameSql = 'USE ' + QUOTENAME(@archiveDbName) + '; SELECT @hasConflictingName = CASE WHEN OBJECT_ID(''' + @tableName + '_' + CONVERT(nvarchar(max), @currentNameCount) + ''') IS NULL THEN 0 ELSE 1 END'
		EXEC sp_executesql @hasConflictingNameSql, N'@hasConflictingName bit OUTPUT', @hasConflictingName = @hasConflictingName OUTPUT
		
	END
	
	DECLARE @newName nvarchar(max)
	SET @newName = OBJECT_NAME(OBJECT_ID(@tableName))+ '_' + CONVERT(nvarchar(max), @currentNameCount)
	
	DECLARE @renameSql nvarchar(max)
	SET @renameSql =  'USE ' + QUOTENAME(@archiveDbName) + '; EXEC sp_rename ''' + @tableName + ''', ''' + @newName + ''''

	EXEC sp_executesql @statement = @renameSql
END

-- Create the table in the archive db
IF OBJECT_ID(QUOTENAME(@archiveDbName) + '.' + @tableName) IS NULL
	EXEC sp_executesql @statement = @createTableSql
	
IF OBJECt_ID('tempdb..#NoCheck') IS NOT NULL
	DROP TABLE #NoCheck

-- Insert into archive and delete from main
SELECT 1 AS Value INTO #NoCheck

-- Get sql to delete and output deleted into archive db
DECLARE @outputDeletedSql nvarchar(max)
SET @outputDeletedSql = 'OUTPUT '

SELECT @outputDeletedSql = @outputDeletedSql + 'deleted.' + QUOTENAME(c.COLUMN_NAME) + ', '
FROM INFORMATION_SCHEMA.COLUMNS c
WHERE OBJECT_ID(QUOTENAME(c.TABLE_SCHEMA) + '.' + QUOTENAME(c.TABLE_NAME)) = OBJECT_ID(@tableName) 
ORDER BY c.ORDINAL_POSITION

SET @outputDeletedSql = SUBSTRING(@outputDeletedSql, 1, LEN(@outputDeletedSql) - 1)
SELECT @outputDeletedSql = @outputDeletedSql + ' INTO ' + QUOTENAME(@archiveDbName) + '.' + QUOTENAME(t.TABLE_SCHEMA) + '.' + QUOTENAME(t.TABLE_NAME)
FROM INFORMATION_SCHEMA.TABLES t
WHERE OBJECT_ID(QUOTENAME(t.TABLE_SCHEMA) + '.' + QUOTENAME(t.TABLE_NAME)) = OBJECT_ID(@tableName)

DECLARE @deleteSql nvarchar(max)

SELECT @deleteSql = 'DELETE FROM ' + QUOTENAME(t.TABLE_SCHEMA) + '.' + QUOTENAME(t.TABLE_NAME) +'
' + @outputDeletedSql + '
WHERE ' + @deleteFilter
FROM INFORMATION_SCHEMA.TABLES t
WHERE OBJECT_ID(QUOTENAME(t.TABLE_SCHEMA) + '.' + QUOTENAME(t.TABLE_NAME)) = OBJECT_ID(@tableName)

-- Do delete
EXEC sp_executesql @statment = @deleteSql

DROP TABLE #NoCheck

DECLARE @enableConstraintsSql nvarchar(max)
SET @enableConstraintsSql = ''

;WITH x AS 
(
  SELECT DISTINCT obj = 
      QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id)) + '.' 
    + QUOTENAME(OBJECT_NAME(parent_object_id)) 
  FROM sys.foreign_keys
)
SELECT @enableConstraintsSql = @enableConstraintsSql + N'ALTER TABLE ' + obj + ' WITH NOCHECK CHECK CONSTRAINT ALL;
' FROM x;

EXEC sp_executesql @enableConstraintsSql;

END