﻿using Quartz;

namespace IO.Practiceware.Monitoring.Service.Common
{
    /// <summary>
    /// Basic job listener that records information about executed jobs.
    /// </summary>
    internal class JobListener : IJobListener
    {
        public int JobsExecutedCount { get; private set; }

        public void JobToBeExecuted(IJobExecutionContext context)
        {
        }

        public void JobExecutionVetoed(IJobExecutionContext context)
        {
        }

        public void JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException)
        {
            JobsExecutedCount++;
        }

        public string Name
        {
            get { return "IO Job Listener"; }
        }
    }
}