﻿using System;
using System.Collections.Concurrent;
using Soaf.Collections;

namespace IO.Practiceware.Monitoring.Service.Common
{
    public class TenantExecutionLog
    {
        private readonly ConcurrentDictionary<string, ConcurrentDictionary<DateTime, TimeSpan>> _executions = 
            new ConcurrentDictionary<string, ConcurrentDictionary<DateTime, TimeSpan>>();

        /// <summary>
        /// Get total time spent in recent executions of tenant.
        /// </summary>
        /// <value>
        /// The recent executions total time.
        /// </value>
        public TimeSpan GetRecentExecutionsTotalTime(string jobName)
        {
            var jobExecutions = _executions.GetValue(jobName, () => new ConcurrentDictionary<DateTime, TimeSpan>());

            var today = DateTime.UtcNow.Date;
            var yesterday = today.AddDays(-1);

            var recentTotal = jobExecutions.GetValue(today, () => TimeSpan.Zero)
                + jobExecutions.GetValue(yesterday, () => TimeSpan.Zero);
            return recentTotal;
        }

        /// <summary>
        /// Logs tenant execution
        /// </summary>
        /// <param name="jobName">Executed job name</param>
        /// <param name="timeExecuting">The time executing.</param>
        public void LogExecution(string jobName, TimeSpan timeExecuting)
        {
            var jobExecutions = _executions.GetValue(jobName, () => new ConcurrentDictionary<DateTime, TimeSpan>());
            var today = DateTime.UtcNow.Date;
            lock (jobExecutions)
            {
                var currentTotal = jobExecutions.GetValue(today, () => TimeSpan.Zero);
                jobExecutions[today] = currentTotal + timeExecuting; 
            }
        }
    }
}