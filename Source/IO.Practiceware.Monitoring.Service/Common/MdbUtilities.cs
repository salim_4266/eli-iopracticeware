﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using IO.Practiceware.Configuration;
using Soaf;

namespace IO.Practiceware.Monitoring.Service.Common
{
    public class MdbUtilities
    {
        public static void Import(string fileName, string schemaName)
        {
            //if the file is not found, break out of this method.
            if (!File.Exists(fileName)) return;

            var sqlConnectionString = ConfigurationManager.PracticeRepositoryConnectionString;
            var odbcConnectionString = string.Format("Driver={{Microsoft Access Driver (*.mdb)}};DBQ={0};", fileName);

            List<DbTable> listOfDatabaseTables;

            using (var odbcConnection = new OdbcConnection(odbcConnectionString))
            using (var sqlConnection = new SqlConnection(sqlConnectionString))
            {
                odbcConnection.Open();
                sqlConnection.Open();
                //query the mdf file for a list of table names, store names in a datatable
                var tableNames = odbcConnection.GetSchema("Tables");
                //using the table just created, obtain a list of database tables
                listOfDatabaseTables = GetTableData(tableNames).ToList();
                //query the mdf file for a list of table column names and types, store them in a datatable
                var columnTypesAndNames = odbcConnection.GetSchema("Columns");
                //using the columnTable just created, obtain a list of database columns and type from the list of database tables.
                GetColumnData(columnTypesAndNames, listOfDatabaseTables);
                //check if schema exists, if not create it.
                CreateSchema(sqlConnection, schemaName);
            }
            Parallel.ForEach(listOfDatabaseTables.Where(IsTableNotExcluded), dbTable =>
            {
                using (var odbcConnection = new OdbcConnection(odbcConnectionString))
                using (var sqlConnection = new SqlConnection(sqlConnectionString))
                {
                    odbcConnection.Open();
                    sqlConnection.Open();
                    //if the sql table does not exist, create it. Otherwise, check existance of all columns in sql table.
                    CreateTable(dbTable, sqlConnection, schemaName);
                    //transfer data from the mdb table to the sql table.
                    TransferTableData(dbTable, odbcConnection, sqlConnection, schemaName);
                }
            });
        }

        public static string[] ExcludedDbTableNames { get; set; }

        public static bool IsTableNotExcluded(DbTable dbTable)
        {
            return ExcludedDbTableNames.All(c => c != dbTable.TableName);
        }

        /// <summary>
        /// Creates a schema.
        /// </summary>
        /// <param name="sqlConnection">The SQL connection.</param>
        /// <param name="schemaPrefix">The schema prefix.</param>
        public static void CreateSchema(SqlConnection sqlConnection, String schemaPrefix)
        {
            var createSchemaString = String.Format(@"IF NOT EXISTS (
                            SELECT  schema_name
                            FROM    information_schema.schemata
                            WHERE   schema_name = '{0}' ) 

                            BEGIN
                            EXEC sp_executesql N'CREATE SCHEMA {0}'
                            END", schemaPrefix);
            using (var comm = new SqlCommand(createSchemaString, sqlConnection))
            {
                comm.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Creates the sql table, from DbTable.
        /// </summary>
        /// <param name="dbTable">The database table.</param>
        /// <param name="sqlConnection">The SQL connection.</param>
        /// <param name="schemaPrefix">The schema prefix.</param>
        public static void CreateTable(DbTable dbTable, SqlConnection sqlConnection, String schemaPrefix)
        {
            var createTableSql = String.Format(@"IF OBJECT_ID('{0}.{1}') IS NULL
                            BEGIN
                            ", schemaPrefix, dbTable.TableName);
            createTableSql += string.Format("CREATE TABLE [{0}].[{1}](", schemaPrefix, dbTable.TableName);
            foreach (var dbColumn in dbTable.Columns)
            {
                createTableSql += string.Format("[{0}] {1} NULL", dbColumn.ColumnName, FormatColumnType(dbColumn.ColumnDataType));

                if (dbColumn != dbTable.Columns[dbTable.Columns.Count - 1])
                {
                    createTableSql += ",";
                }
                else
                {
                    createTableSql += @")
                                END";
                }
            }
            using (var cmd = new SqlCommand(createTableSql, sqlConnection))
            {
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Formats the type of the column.
        /// </summary>
        /// <param name="columnType">Type of the column.</param>
        /// <returns></returns>
        public static string FormatColumnType(string columnType)
        {
            switch (columnType.ToUpper())
            {
                case "LONGCHAR": columnType = "[nvarchar](max)";
                    break;
                case "VARCHAR": columnType = "[nvarchar](max)";
                    break;
                case "DATE/TIME": columnType = "[datetime]";
                    break;
                case "BYTE": columnType = "[bit]";
                    break;
                case "GUID": columnType = "[uniqueidentifier]";
                    break;
                case "DECIMAL": columnType = "[int]";
                    break;
                case "COUNTER": columnType = "[int]";
                    break;
                case "INTEGER": columnType = "[int]";
                    break;
                case "DOUBLE": columnType = "[int]";
                    break;
                default: columnType = String.Format("[{0}]", columnType);
                    break;
            }
            return columnType;
        }

        /// <summary>
        /// Gets the mdf table data.
        /// </summary>
        /// <param name="table">The table.</param>
        /// <returns></returns>
        public static IEnumerable<DbTable> GetTableData(DataTable table)
        {
            return
                (from DataRow row in table.Rows
                 where row[3].ToString() == "TABLE"
                 select new DbTable { TableName = row[2].ToString() }).ToList();
        }

        /// <summary>
        /// Gets the mdf column data from a list of tables.
        /// </summary>
        /// <param name="columnTable">The column table.</param>
        /// <param name="listOfTables">The list of tables.</param>
        public static void GetColumnData(DataTable columnTable, List<DbTable> listOfTables)
        {
            foreach (DataRow row in columnTable.Rows)
            {
                var currentTable = listOfTables.FirstOrDefault(table => table.TableName == row[2].ToString());

                if (currentTable == null) continue;
                var currentColumn =
                    currentTable.Columns.FirstOrDefault(column => column.ColumnName == row[3].ToString());

                if (currentColumn != null) continue;
                currentColumn = new DbColumn { ColumnName = row[3].ToString(), ColumnDataType = row[5].ToString() };
                currentTable.Columns.Add(currentColumn);
            }
        }

        /// <summary>
        /// Deletes the contents of a table in order to prep it for new data.
        /// </summary>
        /// <param name="dbTable">The database table.</param>
        /// <param name="sqlConnection">The SQL connection.</param>
        /// <param name="schemaPrefix">The schema prefix.</param>
        public static void ClearTable(DbTable dbTable, SqlConnection sqlConnection, String schemaPrefix)
        {
            var clearTableSql = String.Format(@"IF OBJECT_ID('{0}.{1}') IS NOT NULL
                            BEGIN
                            DELETE FROM {0}.{1}", schemaPrefix, dbTable.TableName);

            clearTableSql = dbTable.Columns.Aggregate(clearTableSql,
                                                      (current, dbColumn) =>
                                                      current +
                                                      String.Format(@"
                                                        IF NOT EXISTS (
		                                                        SELECT *
		                                                        FROM sys.columns
		                                                        WHERE NAME = N'{0}'
			                                                        AND object_id = OBJECT_ID(N'{3}.{1}')
		                                                        )
	                                                        ALTER TABLE {3}.{1} ADD {0} {2}",
                                                                    dbColumn.ColumnName, dbTable.TableName,
                                                                    FormatColumnType(dbColumn.ColumnDataType), schemaPrefix));

            clearTableSql += @"
                        END";


            using (var cmd = new SqlCommand(clearTableSql, sqlConnection))
            {
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Bulk copy the data from dbTable to the sql server.
        /// </summary>
        /// <param name="dbTable">The database table.</param>
        /// <param name="odbcConnection">The ODBC connection.</param>
        /// <param name="sqlConnection">The SQL connection.</param>
        /// <param name="schemaName">The schema prefix.</param>
        public static void TransferTableData(DbTable dbTable, OdbcConnection odbcConnection, SqlConnection sqlConnection, String schemaName)
        {
            using (var cmd = new OdbcCommand(String.Format("SELECT * FROM [{0}]", dbTable.TableName), odbcConnection))
            {
                using (var dt = new DataTable(dbTable.TableName))
                {
                    dt.Load(cmd.ExecuteReader());
                    using (var transactionScope = new TransactionScope())
                    {
                        MonitoringService.VerboseLog.TraceInformation("Transferred table {0}.{1} from {2} to {3}".FormatWith(schemaName, dbTable.TableName, odbcConnection.ConnectionString, sqlConnection.ConnectionString));

                        //if the sql table exists, delete it's contents. 
                        ClearTable(dbTable, sqlConnection, schemaName);

                        var bulkCopy = new SqlBulkCopy(sqlConnection) { DestinationTableName = string.Format("{0}.{1}", schemaName, dbTable.TableName), BulkCopyTimeout = 180 };
                        bulkCopy.WriteToServer(dt);
                        transactionScope.Complete();
                    }
                }
            }
        }

    }
    public class DbTable
    {
        public DbTable()
        {
            Columns = new List<DbColumn>();
        }

        public string TableName { get; set; }
        public List<DbColumn> Columns { get; set; }
    }

    public class DbColumn
    {
        public string ColumnName { get; set; }
        public String ColumnDataType { get; set; }
    }
}