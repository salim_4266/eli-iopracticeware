﻿using System;
using System.Net;
using System.Runtime.Serialization;
using IO.Practiceware.Services.ApplicationSettings;
using Quartz;
using Soaf.Net;

namespace IO.Practiceware.Monitoring.Service.Common
{
    /// <summary>
    /// Acquires locks to enable running jobs per tenant
    /// </summary>
    [DisallowConcurrentExecution]
    public class TenantJobsActivationMonitor : TenantJobWithState<TenantJobsActivationMonitor.ActivationInfo>
    {
        public const string ActiveInstanceSettingKey = "ActiveJobsProcessingInstance";

        private readonly IApplicationSettingsService _settingsService;
        private static readonly Lazy<string> LocalIpAddress = new Lazy<string>(GetIpAddress);

        public TenantJobsActivationMonitor(IApplicationSettingsService settingsService)
        {
            _settingsService = settingsService;

            // Run it on all instances
            SupportsConcurrency = true;
        }

        protected override void Execute(IJobExecutionContext context, ActivationInfo stateObject)
        {
            // Retrieve current servicing instance information
            var activeInstance = _settingsService.GetOrCreateSetting<JobsProcessingInstance>(ActiveInstanceSettingKey);

            // Is there another activate instance?
            var currentTenantKey = TenantRegistry.CurrentTenantKey;
            var lockInfo = activeInstance.Value ?? new JobsProcessingInstance();

            var isProcessedByAnotherInstance = (lockInfo.InstanceId != MonitoringService.Id
                    || lockInfo.TenantKey != currentTenantKey)
                    && lockInfo.LastConfirmedActivityUtc >= DateTime.UtcNow.AddMinutes(-2);
            var isEnoughForCurrent = TenantRegistry.GetActiveTenantsCount(true) >= TenantRegistry.ConcurrencyLimit;

            if (isProcessedByAnotherInstance
                && (!lockInfo.ReadyToShare || (lockInfo.ReadyToShare && isEnoughForCurrent)))
            {
                stateObject.ReadyToShare = null; // Reset activation information

                // Ensure it's not in list of active ones
                TenantRegistry.SetTenantActivationLockState(currentTenantKey, TenantActivationLockState.NotActive);
                // Till next scheduler activation attempt
                return;
            }

            // Record lock type as it was acquired
            var isInitialAcquire = stateObject.ReadyToShare == null;
            if (isInitialAcquire)
            {
                stateObject.ReadyToShare = isEnoughForCurrent;
            }

            // Refresh current instance active state
            activeInstance.Value = new JobsProcessingInstance
            {
                InstanceId = MonitoringService.Id,
                TenantKey = currentTenantKey,
                LastConfirmedActivityUtc = DateTime.UtcNow,
                InstanceIpAddress = LocalIpAddress.Value,
                ReadyToShare = stateObject.ReadyToShare.Value
            };
            _settingsService.SetSetting(activeInstance);

            // Start processing tenants only after 2nd pass to avoid processing simultaneously by multiple instances
            TenantRegistry.SetTenantActivationLockState(currentTenantKey, isInitialAcquire 
                ? TenantActivationLockState.Pending
                : TenantActivationLockState.Active);
        }

        protected override ActivationInfo Initialize()
        {
            return new ActivationInfo {ReadyToShare = null};
        }

        private static string GetIpAddress()
        {
            // Get instance IP address
            var ipAddress = "Unknown IP";
            try
            {
                IPAddress internetIpAddress = Net.GetLocalInternetIpAddress();
                if (internetIpAddress != null)
                    ipAddress = internetIpAddress.ToString();
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch
            { }

            return ipAddress;
        }

        /// <summary>
        /// Details of the activated scheduler instance
        /// </summary>
        [DataContract]
        public class JobsProcessingInstance
        {
            [DataMember]
            public string InstanceId { get; set; }

            [DataMember]
            public string TenantKey { get; set; }

            [DataMember]
            public DateTime LastConfirmedActivityUtc { get; set; }

            [DataMember]
            public string InstanceIpAddress { get; set; }

            /// <summary>
            /// Flag which indicates that instance is willing to transfer lock to a less occupied instance
            /// </summary>
            /// <value>
            ///   <c>true</c> if [ready to share]; otherwise, <c>false</c>.
            /// </value>
            [DataMember]
            public bool ReadyToShare { get; set; }
        }

        public class ActivationInfo : IDisposable
        {
            public bool? ReadyToShare { get;set; }

            public void Dispose()
            {
                ReadyToShare = null;
            }
        }
    }
}