﻿using System;
using Quartz;
using Quartz.Spi;
using Soaf;

namespace IO.Practiceware.Monitoring.Service.Common
{
    /// <summary>
    /// Uses IServiceProvider for job construction.
    /// </summary>
    public class JobFactory : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public JobFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            try
            {
                var jobDetail = bundle.JobDetail;
                Type jobType = jobDetail.JobType;

                // Return job that is registrated in container
                return (IJob)_serviceProvider.GetService(jobType);
            }
            catch (Exception ex)
            {
                var se = new SchedulerException("Problem instantiating type {0}.".FormatWith(bundle.JobDetail.JobType), ex);
                throw se;
            }
        }

        public void ReturnJob(IJob job)
        {
        }
    }
}