﻿using Quartz;
using Soaf.Configuration;

namespace IO.Practiceware.Monitoring.Service.Common
{
    public static class JobExtensions
    {
        /// <summary>
        /// Retrieves current job specific configuration
        /// </summary>
        /// <typeparam name="TConfiguration"></typeparam>
        /// <param name="context"></param>
        /// <returns></returns>
        public static TConfiguration GetConfiguration<TConfiguration>(this IJobExecutionContext context) 
            where TConfiguration : class
        {
            var sectionName = context.JobDetail.Key.Name + (context.JobDetail.Key.Name.EndsWith("Configuration") ? "" : "Configuration");
            var configuration = ConfigurationSection<TConfiguration>.Named(sectionName) ?? ConfigurationSection<TConfiguration>.Named(char.ToLowerInvariant(sectionName[0]) + sectionName.Substring(1));
            return configuration;
        }
    }
}
