﻿using System;

namespace IO.Practiceware.Monitoring.Service.Common
{
    public interface ITenantStoredValue : IDisposable
    {
        /// <summary>
        /// Gets a value indicating whether this instance is disposed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is disposed; otherwise, <c>false</c>.
        /// </value>
        bool IsDisposed { get; }

        /// <summary>
        /// Specifies when state object should be cleaned up
        /// </summary>
        /// <value>
        /// The type of the disposal.
        /// </value>
        TenantStoredValueDisposalType DisposalType { get; }
    }

    public enum TenantStoredValueDisposalType
    {
        UponDeactivate,
        UponCleanup
    }
}