﻿using System;
using System.Diagnostics;
using Quartz;
using Soaf;

namespace IO.Practiceware.Monitoring.Service.Common
{
    /// <summary>
    /// Job that runs for each application service client. 
    /// Allows to keep state per-client.
    /// </summary>
    public abstract class TenantJobWithState<TState>: TenantJob
        where TState : IDisposable
    {
        protected override void Execute(IJobExecutionContext context)
        {
            var stateStorageKey = string.Format("{0}_State", context.JobDetail.Key.Name);
            ITenantStoredValue state;
            var storage = TenantRegistry.GetTenantInMemoryStorage(TenantRegistry.CurrentTenantKey);
            if (!storage.TryGetValue(stateStorageKey, out state))
            {
                MonitoringService.VerboseLog.TraceInformation("Initializing state for job: {0}. Tenant: {1}", GetType().Name, TenantRegistry.CurrentTenantKey);
                try
                {
                    var disposalType = SupportsConcurrency 
                        ? TenantStoredValueDisposalType.UponCleanup
                        : TenantStoredValueDisposalType.UponDeactivate;
                    
                    state = new TenantState<TState>(Initialize(), disposalType);
                }
                catch (Exception ex)
                {
                    Trace.TraceError("Failed to initialize state for job: {0}. Tenant: {1}. Will retry. Error: {2}", 
                        GetType().Name, TenantRegistry.CurrentTenantKey, ex);
                    // Will attempt to re-initialize on next job activation
                    return;
                }

                storage.AddOrUpdate(stateStorageKey, state, (k, v) => { v.Dispose(); return state; });
            }

            Execute(context, state.CastTo<TenantState<TState>>().State);
        }

        protected abstract void Execute(IJobExecutionContext context, TState stateObject);
        protected abstract TState Initialize();
    }

    public class TenantState<TState> : ITenantStoredValue
        where TState : IDisposable
    {
        public TenantState(TState state, TenantStoredValueDisposalType disposalType = TenantStoredValueDisposalType.UponDeactivate)
        {
            State = state.EnsureNotDefault("State must be provided");
            DisposalType = disposalType;
        }

        public void Dispose()
        {
            State.Dispose();
            IsDisposed = true;
        }

        public TState State { get; private set; }
        public bool IsDisposed { get; private set; }
        public TenantStoredValueDisposalType DisposalType { get; private set; }
    }
}