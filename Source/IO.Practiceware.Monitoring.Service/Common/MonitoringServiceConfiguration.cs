using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using IO.Practiceware.Configuration;
using Quartz;
using Soaf;

namespace IO.Practiceware.Monitoring.Service.Common
{
    [XmlRoot("monitoringServiceConfiguration")]
    public class MonitoringServiceConfiguration
    {
        [XmlAttribute("maximumMemoryInKb")]
        public int MaximumMemoryInKb { get; set; }

        [XmlAttribute("tenantsConcurrencyLimit")]
        public int TenantsConcurrencyLimit { get; set; }

        [XmlArray("schedules")]
        [XmlArrayItem("add")]
        public List<MonitoringServiceSchedule> Schedules { get; set; }

        public string AlertsDestinationEmailAddresses
        {
            get
            {
                const string defaultAlertsEmailAddress = "alerts@iopracticeware.com";
                var result = ConfigurationManager.CustomAppSettings["AlertsDestinationEmailAddresses"];
                if (!result.Contains(defaultAlertsEmailAddress)) result = defaultAlertsEmailAddress + ";" + result;
                return result;
            }
        }

        public int ArchiveDataMonitorAgeThresholdInDays
        {
            get
            {
                int result;
                bool isValid = int.TryParse(ConfigurationManager.SystemAppSettings["ArchiveDataMonitorAgeThresholdInDays"], out result);
                return isValid ? result : 450;
            }
        }
    }

    public class MonitoringServiceSchedule
    {
        private string _typeName;

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("typeName")]
        public string TypeName
        {
            get { return _typeName ?? Name; }
            set { _typeName = value; }
        }

        /// <summary>
        /// Execution interval in minutes
        /// </summary>
        [XmlAttribute("minutes")]
        public string MinutesString { get; set; }

        /// <summary>
        /// Start time (time span format). Optional
        /// </summary>
        [XmlAttribute("time")]
        public string TimeString { get; set; }

        /// <summary>
        /// End time (time span format). Optional specifies if job should halt execution at certain time of day
        /// </summary>
        [XmlAttribute("endTime")]
        public string EndTimeString { get; set; }

        public TimeSpan? Interval
        {
            get
            {
                double minutes;
                var isValid = double.TryParse(MinutesString, out minutes);
                var interval = isValid ? minutes : new double?();
                return interval == null ? (TimeSpan?)null : TimeSpan.FromMinutes(interval.Value);
            }
        }

        public TimeOfDay StartTimeOfDay
        {
            get { return ToTimeOfDay(TimeString); }
        }

        public TimeOfDay EndTimeOfDay
        {
            get { return ToTimeOfDay(EndTimeString); }
        }

        private static TimeOfDay ToTimeOfDay(string time)
        {
            TimeSpan result;
            var isValid = TimeSpan.TryParse(time, out result);

            if (!isValid) return null;

            TimeOfDay tod;
            try
            {
                tod = TimeOfDay.HourAndMinuteOfDay(result.Hours, result.Minutes);
            }
            catch
            {
                return null;
            }

            return tod;
        }

        [XmlAttribute("daysOfWeek")]
        public string DaysOfWeekString { get; set; }

        public DayOfWeek[] DaysOfWeek
        {
            get { return DaysOfWeekString == null ? null : DaysOfWeekString.Split(',').Select(w => w.ToEnum<DayOfWeek>()).ToArray(); }
        }

        public new string ToString()
        {
            return "{{ Name: {0}, TypeName: {1}, MinutesString: {2}, TimeString: {3}, EndTime: {4} }}".FormatWith(Name, TypeName, MinutesString, TimeString, EndTimeString);
        }
    }
}