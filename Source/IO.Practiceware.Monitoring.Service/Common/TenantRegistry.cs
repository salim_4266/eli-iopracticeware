﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using IO.Practiceware.Configuration;
using IO.Practiceware.Monitoring.Service.Common;
using Soaf;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Data;

[assembly:Component(typeof(TenantRegistry), typeof(ITenantRegistry))]

namespace IO.Practiceware.Monitoring.Service.Common
{
    public interface ITenantRegistry
    {
        /// <summary>
        /// Max number of concurrently processing clients.
        /// </summary>
        int ConcurrencyLimit { get; }

        /// <summary>
        /// A list of configured tenants jobs should execute for
        /// </summary>
        IDictionary<string, string> ConfiguredTenants { get; }

        /// <summary>
        /// Returns key of a tenant which is configured to use "PracticeRepository" connection string
        /// </summary>
        string PrimaryTenantKey { get; }

        /// <summary>
        /// Returns currently active tenant key
        /// </summary>
        string CurrentTenantKey { get; }

        /// <summary>
        /// Updates tenant activation lock
        /// </summary>
        /// <param name="tenantKey"></param>
        /// <param name="lockState"></param>
        void SetTenantActivationLockState(string tenantKey, TenantActivationLockState lockState);

        /// <summary>
        /// Returns whether specified tenant has been activated for non-concurrent jobs processing
        /// </summary>
        /// <param name="tenantKey"></param>
        /// <returns></returns>
        bool IsTenantActive(string tenantKey);

        /// <summary>
        /// Gets the active tenants count.
        /// </summary>
        /// <param name="includePendingActivation"></param>
        /// <returns></returns>
        int GetActiveTenantsCount(bool includePendingActivation = false);

        /// <summary>
        /// Checks whether tenant is currently accessible
        /// </summary>
        /// <param name="tenantKey"></param>
        /// <returns></returns>
        bool IsTenantAccessible(string tenantKey);

        /// <summary>
        /// Gets the tenant execution log
        /// </summary>
        /// <param name="tenantKey">The tenant key.</param>
        /// <returns></returns>
        TenantExecutionLog GetTenantExecutionLog(string tenantKey);

        /// <summary>
        /// Allows to listen to the changes of tenant activation state
        /// </summary>
        event EventHandler<TenantActiveStateChangedArgs> TenantActiveStateChanged;

        /// <summary>
        /// Gets in-memory storage of disposable objects for tenant
        /// </summary>
        /// <param name="tenantKey">The tenant key.</param>
        /// <returns></returns>
        ConcurrentDictionary<string, ITenantStoredValue> GetTenantInMemoryStorage(string tenantKey);

        /// <summary>
        /// Resets registry
        /// </summary>
        void Reset();
    }

    [Singleton]
    public class TenantRegistry : ITenantRegistry
    {
        private readonly IDictionary<string, TenantExecutionLog> _tenantTotalExecutionTime = new Dictionary<string, TenantExecutionLog>();
        private readonly IDictionary<string, ConcurrentDictionary<string, ITenantStoredValue>> _tenantStorage = new Dictionary<string, ConcurrentDictionary<string, ITenantStoredValue>>();
        private readonly IDictionary<string, TenantActivationLockState> _activatedTenants = new Dictionary<string, TenantActivationLockState>();

        public event EventHandler<TenantActiveStateChangedArgs> TenantActiveStateChanged;

        public string PrimaryTenantKey { get; private set; }
        public int ConcurrencyLimit { get; private set; }
        public IDictionary<string, string> ConfiguredTenants { get; private set; }

        public string CurrentTenantKey
        {
            get { return ConfigurationManager.AuthenticationToken; }
        }

        public TenantRegistry()
        {
            Init();
        }

        private void Init()
        {
            ConfiguredTenants = new ConcurrentDictionary<string, string>();
            PrimaryTenantKey = "Unknown";
            ConcurrencyLimit = ConfigurationSection<MonitoringServiceConfiguration>.Current.TenantsConcurrencyLimit;

            // Read list of all tenants
            if (ConfigurationSection<ApplicationServerConfiguration>.Current != null
                && ConfigurationSection<ApplicationServerConfiguration>.Current.Clients != null)
            {
                PrimaryTenantKey = ConfigurationManager.DefaultClientConfiguration.IfNotNull(c => c.AuthenticationToken);

                foreach (var client in ConfigurationSection<ApplicationServerConfiguration>.Current.Clients)
                {
                    ConfiguredTenants[client.AuthenticationToken] = client.Name;
                }
            }

            // Warn if no tenants found
            if (ConfiguredTenants.Count == 0)
            {
                Trace.TraceWarning(new ApplicationException("No clients configured for monitoring service. Check configuration").ToString());
            }
        }

        public void SetTenantActivationLockState(string tenantKey, TenantActivationLockState lockState)
        {
            lock (_activatedTenants)
            {
                var oldLockState = _activatedTenants.GetValue(tenantKey, () => TenantActivationLockState.NotActive);
                if (oldLockState == lockState) return;

                _activatedTenants[tenantKey] = lockState;
            }

            if (lockState == TenantActivationLockState.NotActive)
            {
                ConcurrentDictionary<string, ITenantStoredValue> storage;
                if (_tenantStorage.TryGetValue(tenantKey, out storage))
                {
                    lock (storage)
                    {
                        // When tenant deactivates -> cleanup it's storage
                        foreach (var item in storage
                            .Where(kv => kv.Value.DisposalType == TenantStoredValueDisposalType.UponDeactivate)
                            .ToList())
                        {
                            // Remove and dispose item values
                            ITenantStoredValue itemValue;
                            if (storage.TryRemove(item.Key, out itemValue))
                            {
                                itemValue.Dispose();
                            }
                        }
                    }
                }
            }

            OnTenantActiveStateChanged(tenantKey, lockState);
        }

        public bool IsTenantActive(string tenantKey)
        {
            lock (_activatedTenants)
            {
                return _activatedTenants.GetValue(tenantKey, 
                    () => TenantActivationLockState.NotActive) == TenantActivationLockState.Active;   
            }
        }

        public int GetActiveTenantsCount(bool includePendingActivation = false)
        {
            lock (_activatedTenants)
            {
                return _activatedTenants.Count(t => t.Value == TenantActivationLockState.Active
                    || (includePendingActivation && t.Value == TenantActivationLockState.Pending));
            }
        }

        public bool IsTenantAccessible(string tenantKey)
        {
            var isAccessible = this.ExecuteCached(p => IsTenantAccessibleInternal(tenantKey), CacheHoldInterval.Short);
            if (!isAccessible)
            {
                // Ensure deactivated while not accessible
                SetTenantActivationLockState(tenantKey, TenantActivationLockState.NotActive);
            }
            return isAccessible;
        }

        public TenantExecutionLog GetTenantExecutionLog(string tenantKey)
        {
            return _tenantTotalExecutionTime.GetValue(tenantKey, () => new TenantExecutionLog());
        }

        public ConcurrentDictionary<string, ITenantStoredValue> GetTenantInMemoryStorage(string tenantKey)
        {
            return _tenantStorage.GetValue(tenantKey, () => new ConcurrentDictionary<string, ITenantStoredValue>());
        }

        public void Reset()
        {
            // De-activate all tenants
            lock (_activatedTenants)
            {
                _activatedTenants.ToList().ForEach(t => SetTenantActivationLockState(t.Key, TenantActivationLockState.NotActive));
                _activatedTenants.Clear();
            }

            lock (_tenantStorage)
            {
                // Dispose all states
                _tenantStorage.ForEach(t => t.Value.ForEach(k => k.Value.Dispose()));
                _tenantStorage.Clear();
                _tenantTotalExecutionTime.Clear();
            }

            // Re-init
            Init();
        }

        protected virtual void OnTenantActiveStateChanged(string tenantKey, TenantActivationLockState lockState)
        {
            var handler = TenantActiveStateChanged;
            if (handler != null) handler(this, new TenantActiveStateChangedArgs
            {
                TenantKey = tenantKey,
                LockState = lockState
            });
        }

        /// <summary>
        /// Implementation of Sql server and database connection check
        /// </summary>
        /// <param name="tenantKey"></param>
        /// <returns></returns>
        private static bool IsTenantAccessibleInternal(string tenantKey)
        {
            try
            {
                // Locate client and ensure connection string is defined
                var clientConfiguration = ConfigurationSection<ApplicationServerConfiguration>.Current.Clients.First(c => c.AuthenticationToken == tenantKey);
                var connectionString = ConfigurationManager.ConnectionStrings[clientConfiguration.ConnectionStringName].ConnectionString;
                var connectionBuilder = DbConnections.TryGetConnectionStringBuilder<SqlConnectionStringBuilder>(connectionString);

                // Max 15 seconds to connect (we have default values of 1200 seconds which is way too long)
                connectionBuilder.ConnectTimeout = 15;
                var database = connectionBuilder.InitialCatalog;
                connectionBuilder.InitialCatalog = "master";

                using (var connection = new SqlConnection(connectionBuilder.ToString()))
                {
                    connection.Open();
                    // Verify by checking that specified database exists and ApplicationSettings table
                    var command = new SqlCommand(@"SELECT OBJECT_ID('[' + name + '].[model].[ApplicationSettings]') FROM sys.databases WHERE name = '{0}'".FormatWith(database), connection);
                    var result = command.ExecuteScalar();
                    var isAccessible = result != null && result != DBNull.Value;
                    connection.Close();

                    // Log that tenant found to be not accessible
                    if (!isAccessible)
                    {
                        Trace.TraceWarning("Tenant \"{0}\" is not accessible. Pausing job execution until available", tenantKey);
                    }

                    return isAccessible;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceWarning("Tenant \"{0}\" accessibility check failed. Error: {1}", tenantKey, ex);
                return false;
            }
        }
    }

    public enum TenantActivationLockState
    {
        NotActive,
        Pending,
        Active
    }

    public class TenantActiveStateChangedArgs : EventArgs
    {
        /// <summary>
        /// Tenant key
        /// </summary>
        public string TenantKey { get; set; }

        /// <summary>
        /// Whether tenant has been activated or deactivated
        /// </summary>
        public TenantActivationLockState LockState { get; set; }
    }
}