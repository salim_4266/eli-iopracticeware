﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using IO.Practiceware.Application;
using Quartz;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;

namespace IO.Practiceware.Monitoring.Service.Common
{
    /// <summary>
    ///     A multi-client job that runs for each application service client.
    /// </summary>
    [DisallowConcurrentExecution]
    public abstract class TenantJob : IJob
    {
        [Dependency]
        public ITenantRegistry TenantRegistry { get; set; }

        /// <summary>
        /// Specifies whether lock is required to execute job on a tenant
        /// </summary>
        public bool SupportsConcurrency { get; protected set; }

        void IJob.Execute(IJobExecutionContext context)
        {
            PerformActionPerEveryClient(context, "Execute", () => Execute(context));
        }

        protected void PerformActionPerEveryClient(IJobExecutionContext context, string actionName, Action action)
        {
            try
            {
                var jobName = context.JobDetail.Key.Name;
                var exceptions = new ConcurrentQueue<Exception>();

                var tenants = TenantRegistry.ConfiguredTenants
                    .Select(t => new Tuple<string, string>(t.Key, t.Value))
                    // Distribute jobs processing time fairly
                    .OrderByDescending(t => t.Item1 == TenantRegistry.PrimaryTenantKey)
                    .ThenBy(t => TenantRegistry
                        .GetTenantExecutionLog(t.Item1)
                        .GetRecentExecutionsTotalTime(jobName))
                    .ToList();

                // Process tenants in parallel according to desired order
                var queue = new ConcurrentQueue<Tuple<string, string>>(tenants);
                new int[Math.Min(TenantRegistry.ConcurrencyLimit, tenants.Count)].ForAllInParallel(dummy =>
                {
                    while (true)
                    {
                        Tuple<string, string> tenant;
                        // Another tenant to process?
                        if (!queue.TryDequeue(out tenant))
                        {
                            break;
                        }

                        try
                        {
                            if (!TenantRegistry.IsTenantAccessible(tenant.Item1))
                            {
                                MonitoringService.VerboseLog.TraceInformation("Skipping action: {0}. Job: {1} for client: {2} because tenant is inaccessible", actionName, jobName, tenant.Item2);
                                // Skip tenants which are not accessible at the moment
                                continue;
                            }

                            if (!SupportsConcurrency && !TenantRegistry.IsTenantActive(tenant.Item1))
                            {
                                MonitoringService.VerboseLog.TraceInformation("Skipping action: {0}. Job: {1} for client: {2} because tenant is not active", actionName, jobName, tenant.Item2);
                                // Lock for tenant hasn't been acquired by activation monitor -> skipping
                                continue;
                            }

                            MonitoringService.VerboseLog.TraceInformation("Performing action: {0}. Job: {1} for client: {2}", actionName, jobName, tenant.Item2);

                            using (new SystemUserContext(tenant.Item1))
                            using (new DisposableScope<DateTime>(DateTime.UtcNow, null,
                                start => TenantRegistry.GetTenantExecutionLog(tenant.Item1)
                                    .LogExecution(jobName, DateTime.UtcNow - start)))
                            {
                                action();
                            }
                        }
                        catch (Exception ex)
                        {
                            exceptions.Enqueue(new ApplicationException(string.Format(
                                "Failed to execute job {0} for client {1}", jobName, tenant.Item2), ex));
                        }
                    }
                }, true, TenantRegistry.ConcurrencyLimit);
                
                if (exceptions.Any())
                {
                    throw new AggregateException(exceptions);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                throw new JobExecutionException(ex);
            }
        }

        protected abstract void Execute(IJobExecutionContext context);

        private static readonly TimeSpan DefaultJobCooldown = TimeSpan.FromSeconds(15);

        /// <summary>
        /// Allows to evaluate whether this long running job should continue running.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="isDisposed">if set to <c>true</c> disposed.</param>
        /// <param name="cooldown">The cooldown (minimum idle period before next activation). Defaults to 15 seconds.</param>
        /// <returns></returns>
        protected virtual bool CanContinueRunning(IJobExecutionContext context, bool isDisposed = false, TimeSpan? cooldown = null)
        {
            var canContinueRunning = !context.Scheduler.InStandbyMode
                && !context.Scheduler.IsShutdown
                && !isDisposed;

            // Last execution? -> run while not stopped or in standby
            if (!canContinueRunning || context.NextFireTimeUtc == null)
            {
                return canContinueRunning;
            }

            cooldown = cooldown ?? DefaultJobCooldown;
            var now = DateTime.UtcNow;
            var stopProcessingTime = now + cooldown;

            // Stop running shortly before next scheduled execution
            if (context.NextFireTimeUtc <= stopProcessingTime)
            {
                return false;
            }

            // Stop processing by end time of day specified in configuration
            var dailyTrigger = context.Trigger.As<IDailyTimeIntervalTrigger>();
            var endTimePerConfiguration = dailyTrigger
                .IfNotNull(dt => dt.EndTimeOfDay
                    .IfNotNull(etod => etod.GetTimeOfDayForDate(context.ScheduledFireTimeUtc)));
            if (endTimePerConfiguration != null
                && endTimePerConfiguration <= stopProcessingTime)
            {
                return false;
            }

            return true;
        }
    }
}