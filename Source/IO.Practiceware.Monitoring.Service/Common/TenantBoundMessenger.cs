﻿using System;
using IO.Practiceware.Application;
using Soaf;

namespace IO.Practiceware.Monitoring.Service.Common
{
    public class TenantBoundMessenger : IMessenger
    {
        private readonly IMessenger _messenger;

        public TenantBoundMessenger(IMessenger messenger)
        {
            _messenger = messenger;
        }

        public void Dispose()
        {
            _messenger.Dispose();
        }

        public void Publish(object message)
        {
            _messenger.Publish(message);
        }

        public object Subscribe(Type messageType, MessageHandler handler, bool useWeakReference = false)
        {
            var tenant = ApplicationContext.Current.ClientKey;
            var redefinedHandler = new MessageHandler((m, t, d) =>
            {
                // Handle messages posted only within specified tenant
                if (ApplicationContext.Current.ClientKey != tenant)
                {
                    return;
                }

                handler(m, t, d);
            });
            return _messenger.Subscribe(messageType, redefinedHandler, useWeakReference);
        }

        public void Unsubscribe(object token)
        {
            _messenger.Unsubscribe(token);
        }

        public void Unsubscribe(Type messageType)
        {
            _messenger.Unsubscribe(messageType);
        }
    }
}
