﻿using System.Data.SqlClient;
using IO.Practiceware.Configuration;
using IO.Practiceware.Monitoring.Service.Common;
using Quartz;
using Soaf;
using Soaf.Data;

namespace IO.Practiceware.Monitoring.Service.ArchiveDataMonitor
{
    public class ArchiveDataMonitor : TenantJob
    {
        protected override void Execute(IJobExecutionContext context)
        {
            Execute();
        }

        public static void Execute()
        {
            using (var auditEntryCheckConnection = new SqlConnection(ConfigurationManager.PracticeRepositoryConnectionString))
            {
                ExecuteForConnection(auditEntryCheckConnection);
            }
        }

        public static void ExecuteForConnection(SqlConnection connection)
        {
            connection.RunScript(Resources.ArchiveTable);

            var olderThanDays = ConfigurationManager.SystemAppSettings["ArchiveDataMonitorAgeThresholdInDays"].ToInt() ?? 450;

            var tableName = "dbo.LogEntryProperties";
            var deleteFilter = "LogEntryId IN (SELECT Id FROM dbo.LogEntries WHERE DateTime <= DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, -{0}, GETDATE()))))".FormatWith(olderThanDays);
            ExecuteForConnection(connection, tableName, deleteFilter);

            tableName = "dbo.LogEntryLogCategory";
            deleteFilter = "LogEntryLogCategory_LogCategory_Id IN (SELECT Id FROM dbo.LogEntries WHERE DateTime <= DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, -{0}, GETDATE()))))".FormatWith(olderThanDays);
            ExecuteForConnection(connection, tableName, deleteFilter);

            tableName = "dbo.LogEntries";
            deleteFilter = "DateTime <= DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, -{0}, GETDATE())))".FormatWith(olderThanDays);
            ExecuteForConnection(connection, tableName, deleteFilter);

            tableName = "dbo.AuditEntryKeyValues";
            deleteFilter = "AuditEntryId IN (SELECT Id FROM dbo.AuditEntries WHERE AuditDateTime <= DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, -{0}, GETDATE()))))".FormatWith(olderThanDays);
            ExecuteForConnection(connection, tableName, deleteFilter);

            tableName = "dbo.AuditEntryChanges";
            deleteFilter = "AuditEntryId IN (SELECT Id FROM dbo.AuditEntries WHERE AuditDateTime <= DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, -{0}, GETDATE()))))".FormatWith(olderThanDays);
            ExecuteForConnection(connection, tableName, deleteFilter);

            tableName = "dbo.AuditEntries";
            deleteFilter = "AuditDateTime <= DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, -{0}, GETDATE())))".FormatWith(olderThanDays);
            ExecuteForConnection(connection, tableName, deleteFilter);

            var tableExists = connection.Execute<bool>("IF OBJECT_ID(N'[dbo].[AuditEntryChangesTemp]', 'U') IS NOT NULL SELECT CAST(1 AS BIT) ELSE SELECT CAST(0 AS BIT)");
            if (tableExists)
            {
                tableName = "dbo.AuditEntryChangesTemp";
                deleteFilter = "AuditEntryId IN (SELECT AuditEntryId FROM dbo.AuditEntryChangesTemp)";
                ExecuteForConnection(connection, tableName, deleteFilter);
            }

            const string commandText = " IF OBJECT_ID(N'[dbo].[AuditEntryChangesTemp]', 'U') IS NOT NULL DROP TABLE dbo.AuditEntryChangesTemp ";
            connection.Execute(commandText);
        }

        public static void ExecuteForConnection(SqlConnection connection, string tableName, string deleteFilter)
        {
            connection.Execute("EXEC ArchiveTable N'{0}', N'{1}'".FormatWith(tableName, deleteFilter), null, false, () => new SqlCommand { CommandTimeout = 10800 });
        }

    }
}