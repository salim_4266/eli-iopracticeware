using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using Soaf.Collections;
using Soaf.ComponentModel;

namespace IO.Practiceware.Monitoring.Service.UpdateMonitor
{
    public class UpdateMonitorConfiguration
    {
        [XmlArray("updateClients")]
        [XmlArrayItem("updateClient")]
        public List<UpdateClientConfiguration> UpdateClients { get; set; }
    }

    public class UpdateClientConfiguration
    {
        private string _localPath;

        public UpdateClientConfiguration()
        {
            UploadLogs = true;
        }

        [XmlAttribute("userName")]
        public string UserName { get; set; }

        [XmlAttribute("password")]
        public string Password { get; set; }

        [XmlAttribute("ftpAddress")]
        public string FtpAddressString { get; set; }

        [XmlIgnore]
        public Uri FtpAddress
        {
            get
            {
                try
                {
                    if (FtpAddressString == null) return null;

                    var uri = new UriBuilder(FtpAddressString);
                    if (UserName.IsNotNullOrEmpty())
                    {
                        uri.UserName = Uri.EscapeDataString(UserName);
                        uri.Password = Uri.EscapeDataString(Password ?? string.Empty);
                    }
                    return uri.Uri;
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Could not parse ftp address for using the following client: {0}.", this), ex);
                }
            }
            set { FtpAddressString = value != null ? value.ToString() : null; }
        }

        [XmlAttribute("localPath")]
        public string LocalPath
        {
            get { return _localPath; }
            set { _localPath = Path.GetFullPath(Environment.ExpandEnvironmentVariables(value)); }
        }

        [XmlAttribute("uploadLogs")]
        public bool UploadLogs { get; set; }

        [XmlAttribute("enableAutomaticServerUpdates")]
        public bool EnableAutomaticServerUpdates { get; set; }

        public override string ToString()
        {
            try
            {
                return this.ToXml();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Serializing the configuration failed.  UserName={0}, Password={1}, FtpAddress={2}, LocalPath={3}",
                    UserName, Password, FtpAddress, LocalPath), ex);
            }
        }
    }
}