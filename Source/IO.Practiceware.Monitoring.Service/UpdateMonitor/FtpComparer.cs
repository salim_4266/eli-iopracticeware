﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Monitoring.Service.UpdateMonitor
{
    /// <summary>
    /// Compares a collection of FtpManifestItems to a given local directory path.
    /// Produces collections of files to download and delete that represent the changes
    /// necessary to sync the given local directory to the ftp manifest items.
    /// </summary>
    public class FtpComparer
    {

        public FtpComparer(IEnumerable<FtpManifestItem> source, IEnumerable<FtpManifestItem> destination)
        {
            ItemsInSourceNotDestination = new List<FtpManifestItem>();
            ItemsInDestinationNotSource = new List<FtpManifestItem>();
            ItemsDifferent = new List<Tuple<FtpManifestItem, FtpManifestItem>>();
            CompareTo(source.ToArray(), destination.ToArray());
        }

        private void CompareTo(FtpManifestItem[] source, FtpManifestItem[] destination)
        {
            #region helper func - join (left joins on FtpItem Name, parentDirectoryName, IsDirectory)
            Func<IEnumerable<FtpManifestItem>, IEnumerable<FtpManifestItem>, IEnumerable<Tuple<FtpManifestItem, FtpManifestItem>>> join = (x, y) =>
                from item1 in x
                join item2 in y on item1.RelativePath equals item2.RelativePath
                    into server2Group
                from server2Item in server2Group.DefaultIfEmpty()
                select Tuple.Create(item1, server2Item);
            #endregion

            var merged = join(source, destination).Union(join(destination, source).Select(i => Tuple.Create(i.Item2, i.Item1)));

            foreach (var pair in merged)
            {
                var sourceItem = pair.Item1;
                var destinationItem = pair.Item2;
                if (destinationItem != null && sourceItem == null)
                {
                    // Exists locally, but not on ftp - delete
                    ItemsInDestinationNotSource.Add(destinationItem);
                }
                else if (destinationItem == null && sourceItem != null)
                {
                    // Exists on ftp, but not locally - add
                    ItemsInSourceNotDestination.Add(sourceItem);
                }
                else if (destinationItem != null && !destinationItem.Equals(sourceItem))
                {
                    ItemsDifferent.Add(Tuple.Create(sourceItem, destinationItem));
                }
            }
        }

        /// <summary>
        /// Gets a flattened list of local items to delete.
        /// </summary>
        public List<FtpManifestItem> ItemsInDestinationNotSource { get; private set; }

        /// <summary>
        /// Gets a flattened list of ftp items to download.
        /// </summary>
        public List<FtpManifestItem> ItemsInSourceNotDestination { get; private set; }

        /// <summary>
        /// Gets a flattened list of ftp items to download.
        /// </summary>
        public List<Tuple<FtpManifestItem, FtpManifestItem>> ItemsDifferent { get; private set; }


        /// <summary>
        /// Gets a value indicating whether this instance has items that require action (local to delete or ftp items to download).
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has items; otherwise, <c>false</c>.
        /// </value>
        public bool HasItems
        {
            get { return ItemsInDestinationNotSource.Count > 0 || ItemsInSourceNotDestination.Count > 0 || ItemsDifferent.Count > 0; }
        }
    }

}
