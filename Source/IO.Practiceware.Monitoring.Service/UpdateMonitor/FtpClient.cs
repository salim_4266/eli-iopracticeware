﻿
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.FtpClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace IO.Practiceware.Monitoring.Service.UpdateMonitor
{
    public class FtpClient
    {
        private const int BufferSize = 10240;
        private static readonly Func<Exception, bool> ExceptionsToRetryOn = ex => new[]
                                                                                      {
                                                                                          "The process cannot access the file because it is being used by another process.", 
                                                                                          "The system cannot find the file specified.",
                                                                                          "Cannot create a file when that file already exists."
                                                                                      }.Contains(ex.Message);

        private static System.Net.FtpClient.FtpClient CreateFtpClient(Uri uri)
        {
            var client = new System.Net.FtpClient.FtpClient();
            client.ValidateCertificate += (sender, e) => e.Accept = true;
            client.Host = uri.Host;
            client.Port = uri.Port > 0 ? uri.Port : 21;
            client.EncryptionMode = "ftps".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase) ? FtpEncryptionMode.Implicit : FtpEncryptionMode.None;
            string[] userNamePassword = uri.UserInfo.Split(':');

            if (userNamePassword.Length > 2)
            {
                var password = uri.UserInfo.Substring(uri.UserInfo.IndexOf(':') + 1);
                if (!string.IsNullOrEmpty(password))
                    client.Credentials = new NetworkCredential(Uri.UnescapeDataString(userNamePassword[0]), Uri.UnescapeDataString(password));
            }

            else if (userNamePassword.Length == 2)
            {
                client.Credentials = new NetworkCredential(Uri.UnescapeDataString(userNamePassword[0]), Uri.UnescapeDataString(userNamePassword[1]));
            }
            else if (uri.UserInfo.Length > 0)
            {
                client.Credentials = new NetworkCredential(Uri.UnescapeDataString(uri.UserInfo), "");
            }
            else
            {
                client.Credentials = new NetworkCredential("ftp", "ftp");
            }

            client.EnableThreadSafeDataConnections = false;
            RetryUtility.ExecuteWithRetry(() => client.Connect(), 10, TimeSpan.FromSeconds(2), ex => ex is TimeoutException);

            if (uri.PathAndQuery.EndsWith("/"))
                client.SetWorkingDirectory(Uri.UnescapeDataString(uri.PathAndQuery));

            return client;
        }

        private static IEnumerable<string> GetDirectoryParts(Uri uri)
        {
            string[] source = uri.PathAndQuery.Split('/', '\\').Where(s => s != string.Empty).ToArray();

            IEnumerator it = source.GetEnumerator();
            bool hasRemainingItems;
            bool isFirst = true;
            string item = null;

            do
            {
                hasRemainingItems = it.MoveNext();
                if (hasRemainingItems)
                {
                    if (!isFirst) yield return item;
                    item = it.Current as string;
                    isFirst = false;
                }
            } while (hasRemainingItems);
        }

        private static void CreateDirectories(System.Net.FtpClient.FtpClient client, Uri fileFtpAddress)
        {
            var current = new StringBuilder();
            foreach (string part in GetDirectoryParts(fileFtpAddress))
            {
                current.Append(part + '/');
                if (!client.DirectoryExists(current.ToString()))
                {
                    client.CreateDirectory(current.ToString());
                }
            }
        }

        /// <summary>
        ///     Uploads a file to the destination ftp address.
        ///     Creates the directories to the destination ftp address if they don't exist.
        ///     Overwrites the file if the same filename exists at the destination ftp address.
        /// </summary>
        /// <param name="localFilePath"> </param>
        /// <param name="destinationFtpAddress"> </param>
        public void UploadFile(string localFilePath, Uri destinationFtpAddress)
        {
            RetryUtility.ExecuteWithRetry(() => UploadFileInternal(localFilePath, destinationFtpAddress), 10, TimeSpan.FromSeconds(2), ExceptionsToRetryOn);
        }

        private static void UploadFileInternal(string localFilePath, Uri destinationFtpAddress)
        {
            var pathSegments = destinationFtpAddress.PathAndQuery.Split('/');
            pathSegments[pathSegments.Length - 1] = "~" + Path.GetFileName(FileManager.Instance.GetTempPathName());

            var tempUploadPath = string.Join("/", pathSegments);

            using (System.Net.FtpClient.FtpClient client = CreateFtpClient(destinationFtpAddress))
            {
                CreateDirectories(client, destinationFtpAddress);

                var buffer = new byte[BufferSize];

                using (Stream ftpStream = client.OpenWrite(tempUploadPath, FtpDataType.Binary))
                using (var fileStream = new FileStream(localFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    int bytesRead = fileStream.Read(buffer, 0, buffer.Length);
                    while (bytesRead > 0)
                    {
                        ftpStream.Write(buffer, 0, bytesRead);
                        bytesRead = fileStream.Read(buffer, 0, buffer.Length);
                    }
                }

                if (!TryWaitUntilExists(client, tempUploadPath))
                {
                    throw new InvalidOperationException("Could not find uploaded file {0} on server.".FormatWith(tempUploadPath));
                }

                DateTime modifiedTime = client.GetModifiedTime(tempUploadPath);

                new FileInfo(localFilePath).LastWriteTime = modifiedTime;

                RetryUtility.ExecuteWithRetry(() =>
                    {
                        if (client.GetNameListing(Path.GetDirectoryName(destinationFtpAddress.PathAndQuery)).Any(l => l.Equals(destinationFtpAddress.PathAndQuery, StringComparison.OrdinalIgnoreCase)))
                            // file exists check is case sensitive, so not safe to use
                            client.DeleteFile(destinationFtpAddress.PathAndQuery);

                        client.Rename(tempUploadPath, destinationFtpAddress.PathAndQuery);
                    }, 10, TimeSpan.FromSeconds(2), ExceptionsToRetryOn);
            }
        }

        private static bool TryWaitUntilExists(System.Net.FtpClient.FtpClient client, string path)
        {
            int checkCount = 1;
            while (checkCount < 10 && !client.FileExists(path, FtpListOption.AllFiles))
            {
                Thread.Sleep(2000);
                checkCount++;
            }
            return checkCount != 10;
        }

        public void DownloadItem(FtpManifestItem ftpManifestItem, string destinationPath)
        {
            if (ftpManifestItem.IsDirectory)
            {
                // Ensure directory exists
                var directory = new DirectoryInfo(Path.Combine(destinationPath, ftpManifestItem.Name));
                if (!directory.Exists)
                {
                    directory.Create();
                }

                // Update last write time to make it match one on Ftp
                directory.LastWriteTime = ftpManifestItem.LastWriteTime;
            }
            else
            {
                DownloadFile(ftpManifestItem, destinationPath);
            }
        }

        private static void DownloadFile(FtpManifestItem ftpManifestItem, string destinationPath)
        {
            string tempDestination = FileManager.Instance.GetTempPathName();
            try
            {
                var buffer = new byte[BufferSize];

                var uri = new Uri(ftpManifestItem.AbsolutePath);
                using (System.Net.FtpClient.FtpClient client = CreateFtpClient(uri))
                using (Stream ftpStream = client.OpenRead(Uri.UnescapeDataString(uri.PathAndQuery), FtpDataType.Binary))
                using (var fileStream = new FileStream(tempDestination, FileMode.Create))
                {
                    int bytesRead = ftpStream.Read(buffer, 0, buffer.Length);
                    while (bytesRead > 0)
                    {
                        fileStream.Write(buffer, 0, bytesRead);
                        bytesRead = ftpStream.Read(buffer, 0, buffer.Length);
                    }
                }

                var downloadedFile = new FileInfo(tempDestination);
                // Additionally, we sync up the write times as well
                downloadedFile.LastWriteTime = ftpManifestItem.LastWriteTime;

                FileSystem.TryMoveFile(tempDestination, destinationPath);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not download file at {0} to {1} ({2}).".FormatWith(ftpManifestItem.RelativePath.Value, destinationPath, tempDestination), ex);
            }
            finally
            {
                if (File.Exists(tempDestination)) FileSystem.TryDeleteFile(tempDestination);
            }
        }

        public void DeleteFile(Uri ftpAddress)
        {
            using (System.Net.FtpClient.FtpClient client = CreateFtpClient(ftpAddress))
            {
                RetryUtility.ExecuteWithRetry(() => client.DeleteFile(ftpAddress.PathAndQuery), 10, TimeSpan.FromSeconds(2), ExceptionsToRetryOn);
            }
        }

        /// <summary>
        ///     Returns a collection of ftp directories and files at the specified URI.
        /// </summary>
        /// <param name="ftpAddress"> </param>
        /// <param name="getChildDirectoryItems"> </param>
        /// <returns> </returns>
        public IEnumerable<FtpManifestItem> GetFtpDirectoryAndFileManifest(Uri ftpAddress, bool getChildDirectoryItems = true)
        {
            return GetFtpDirectoryAndFileManifest(ftpAddress, getChildDirectoryItems, ftpAddress.ToString());
        }

        public IEnumerable<FtpManifestItem> GetFtpDirectoryAndFileManifest(Uri ftpAddress, bool getChildDirectoryItems, string root)
        {
            using (System.Net.FtpClient.FtpClient client = CreateFtpClient(ftpAddress))
            {
                string currentDirectory = ftpAddress.LocalPath;

                FtpListItem[] listItems = client.GetListing(currentDirectory, FtpListOption.AllFiles);

                var results = new List<FtpManifestItem>();

                foreach (FtpListItem listItem in listItems.OrderBy(i => i.Type == FtpFileSystemObjectType.Directory).ThenBy(i => i.Name))
                {
                    Uri absolutePath = ftpAddress.Combine(listItem.FullName);

                    var manifestItem = new FtpManifestItem(absolutePath.ToString(), listItem.Type == FtpFileSystemObjectType.Directory, listItem.Size, GetLastWriteTime(listItem), root);

                    results.Add(manifestItem);

                    if (manifestItem.IsDirectory && getChildDirectoryItems)
                    {
                        results.AddRange(GetFtpDirectoryAndFileManifest(new Uri(manifestItem.AbsolutePath), true, root));
                    }
                }

                return results;
            }
        }

        private static DateTime GetLastWriteTime(FtpListItem listItem)
        {
            if (listItem.Modified != DateTime.MinValue) return listItem.Modified;

            var regex = new Regex(@"^((?<DIR>([dD]{1}))|)(?<ATTRIBS>(.*))\s(?<SIZE>([0-9]{1,}))\s(?<DATE>((?<MONTHDAY>((?<MONTH>(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\s(?<DAY>([0-9\s]{2}))))\s*((?<YEAR>([0-9]{4}))|(?<TIME>([0-9]{1,2}\:[0-9]{2})))))\s(?<NAME>(.{1,}))$");
            Match match = regex.Match(listItem.Input);

            #region helper functions

            Func<string, string> parseMatch =
                matchName =>
                {
                    if (match.Groups[matchName] != null && !string.IsNullOrEmpty(match.Groups[matchName].Value))
                    {
                        return match.Groups[matchName].Value.Trim(new[] { ' ', '\'', '\"' });
                    }
                    return string.Empty;
                };

            #endregion

            DateTime lastWriteTime;
            if (string.IsNullOrEmpty(parseMatch("TIME")))
                DateTime.TryParseExact(parseMatch("DATE").Replace("  ", " "), "MMM d yyyy", null, DateTimeStyles.None, out lastWriteTime);
            else
            {
                DateTime.TryParseExact(parseMatch("DATE").Replace("  ", " "), "MMM d H:mm", null, DateTimeStyles.None, out lastWriteTime);
            }
            return lastWriteTime;
        }

        public List<FileSystemInfo> DownloadRemoteItems(FtpManifestItem[] remoteItemsToDownload, string root)
        {
            var results = new List<FileSystemInfo>();
            foreach (var fileToDownload in remoteItemsToDownload)
            {
                var destinationPath = Path.Combine(root, fileToDownload.RelativePath.Value);
                DownloadItem(fileToDownload, destinationPath);
                results.Add(new FileInfo(destinationPath));
            }

            if (remoteItemsToDownload.Any())
            {
                Trace.TraceInformation("Downloaded the following items from the FTP server:\r\n{0}",
                    remoteItemsToDownload.Select(ri => ri.AbsolutePath).Join(Environment.NewLine));
            }

            return results;
        }
    }

    public class FtpManifestItem
    {
        public FtpManifestItem(string absolutePath, bool isDirectory, long fileLength, DateTime lastWriteTime, string root)
        {
            AbsolutePath = absolutePath;
            IsDirectory = isDirectory;
            FileLength = fileLength;
            LastWriteTime = lastWriteTime;
            SetRelativePath(root);
        }

        public string Name
        {
            get { return Path.GetFileName(RelativePath.Value); }
        }

        public DateTime LastWriteTime { get; private set; }

        public bool IsDirectory { get; private set; }

        public long FileLength { get; private set; }

        public string AbsolutePath { get; private set; }

        public RelativePath RelativePath { get; private set; }

        private void SetRelativePath(string root)
        {
            root = root.TrimEnd('\\');
            if (!root.EndsWith("/")) root += "/";
            var uri = new Uri(root);
            RelativePath = new RelativePath(Uri.UnescapeDataString(uri.MakeRelativeUri(new Uri(AbsolutePath)).ToString()));
        }

        public override string ToString()
        {
            const string format = "LastWriteTime = {0} " +
                                  "\nIsDirectory = {1} " +
                                  "\nName = {2} " +
                                  "\nFileLength = {3} " +
                                  "\nRelativePath = {4} " +
                                  "\nAbsolutePath = {5} ";

            return string.Format(format, new object[] { LastWriteTime.ToString(), IsDirectory, Name, FileLength.ToString(), RelativePath, AbsolutePath });
        }

        public override bool Equals(object o)
        {
            var other = o as FtpManifestItem;
            if (other == null) return false;

            return RelativePath.Equals(other.RelativePath) && IsDirectory == other.IsDirectory && FileLength == other.FileLength && LastWriteTime.Equals(other.LastWriteTime);
        }

        public override int GetHashCode()
        {
            return IsDirectory.GetHashCode() ^ FileLength.GetHashCode() ^ LastWriteTime.GetHashCode();
        }

        public static IEnumerable<FtpManifestItem> GetLocalDirectoryManifestItems(string directoryPath, SearchOption searchOption = SearchOption.AllDirectories)
        {
            return new DirectoryInfo(directoryPath)
                .EnumerateFileSystemInfos("*", searchOption)
                .Select(i => new FtpManifestItem(
                    i.FullName,
                    i is DirectoryInfo,
                    i is DirectoryInfo ? 0 : ((FileInfo)i).Length,
                    i.LastWriteTime,
                    directoryPath));
        }

        public void Delete()
        {
            if (File.Exists(AbsolutePath))
            {
                File.Delete(AbsolutePath);
            }
            else if (Directory.Exists(AbsolutePath))
            {
                Directory.Delete(AbsolutePath, true);
            }
        }
    }

    /// <summary>
    ///     Defines a path relative to a root.
    /// </summary>
    public class RelativePath
    {
        private string _value;

        public RelativePath(string value)
        {
            Value = value;
        }

        public string Value
        {
            get { return _value; }
            private set
            {
                if (value == null) throw new ArgumentNullException("value");
                value = value.Replace(@"\", "/").TrimEnd('/');
                _value = value;
            }
        }

        public override int GetHashCode()
        {
            return 1;
        }

        public override bool Equals(object obj)
        {
            var other = obj as RelativePath;
            return other != null && other.Value.Equals(Value, StringComparison.OrdinalIgnoreCase);
        }

        public override string ToString()
        {
            return Value;
        }
    }
}
