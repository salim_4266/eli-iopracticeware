﻿using System;
using System.Data.SqlClient;
using IO.Practiceware.Configuration;

namespace IO.Practiceware.Monitoring.Service.UpdateMonitor
{
    internal class RxNormMapper
    {
        public static void Map()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.PracticeRepositoryConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(String.Format(@"DELETE FROM model.ExternalSystemEntityMappings
WHERE ExternalSystemEntityId IN 
	(SELECT ese.Id FROM model.ExternalSystemEntities ese JOIN model.ExternalSystems es ON ese.ExternalSystemId = es.Id WHERE es.Name = 'RxNorm')

DECLARE @MedicationId INT
SELECT @MedicationId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'Medication'

INSERT INTO model.ExternalSystemEntityMappings(PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
  SELECT 
	@MedicationId, esm.PracticeRepositoryEntityKey, eseRxNorm.Id, MappedID
  FROM fdb_rxnorm_mapping.tblDrugMappingFDB map
  JOIN model.ExternalSystemEntityMappings esm ON esm.ExternalSystemEntityKey = map.DrugID
  JOIN model.ExternalSystemEntities ese ON esm.ExternalSystemEntityId = ese.Id
  JOIN model.ExternalSystems es ON ese.ExternalSystemId = es.Id AND es.Name = 'FDB'
  JOIN model.ExternalSystems esRxNorm ON esRxNorm.Name = 'RxNorm'
  JOIN model.ExternalSystemEntities eseRxNorm ON eseRxNorm.Name = 'Medication' AND eseRxNorm.ExternalSystemId = esRxNorm.Id"), sqlConnection))
                sqlCommand.ExecuteNonQuery();
            }
        }
    }
}