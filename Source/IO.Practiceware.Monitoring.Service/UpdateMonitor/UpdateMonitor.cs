﻿using ICSharpCode.SharpZipLib.Zip;
using IO.Practiceware.Monitoring.Service.Common;
using IO.Practiceware.Storage;
using Quartz;
using Soaf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using ConfigurationManager = IO.Practiceware.Configuration.ConfigurationManager;

namespace IO.Practiceware.Monitoring.Service.UpdateMonitor
{
    [DisallowConcurrentExecution]
    public class UpdateMonitor : TenantJob
    {
        internal static readonly Object SyncRoot = new object();

        public static string ServerInstallSetupName = "IO Practiceware Server Setup.msi";

        /// <summary>
        ///   List of items to exclude in download from the ftp server
        /// </summary>
        internal static readonly ReadOnlyCollection<string> ItemsToExcludeInDownload = new List<string>
        {
            "thumbs.db"
        }.AsReadOnly();

        private readonly ITenantRegistry _tenantRegistry;
        private readonly FtpClient _ftpClient;

        public UpdateMonitor(ITenantRegistry tenantRegistry, FtpClient ftpClient)
        {
            _tenantRegistry = tenantRegistry;
            _ftpClient = ftpClient;
        }

        protected override void Execute(IJobExecutionContext context)
        {
            lock (SyncRoot)
            {
                var updateMonitorName = context.JobDetail.Key.Name;

                // Read current update monitor configuration
                var configuration = context.GetConfiguration<UpdateMonitorConfiguration>();

                // Multiple tenants? -> sync to server data path location instead of specified LocalPath
                var syncToServerDataPath = _tenantRegistry.ConfiguredTenants.Count > 1;

                // Perform update
                var downloadServerInstaller = _tenantRegistry.CurrentTenantKey == _tenantRegistry.PrimaryTenantKey;
                foreach (var c in configuration.UpdateClients)
                {
                    UpdateClient(c, updateMonitorName, syncToServerDataPath, downloadServerInstaller);
                }
            }
        }

        private static string GetServerInstallPath()
        {
            try
            {
                var executableDirectoryPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                if (executableDirectoryPath == null)
                {
                    throw new Exception("Could not get the path to where the running update service executable is located.");
                }
                return Path.GetFullPath(Path.Combine(executableDirectoryPath, ".."));
            }
            catch (Exception ex)
            {
                throw new Exception("Could not get the path to the install log and migration log", ex);
            }
        }

        public void UpdateClient(UpdateClientConfiguration client, string updateMonitorName, bool syncToServerDataPath, bool downloadServerInstaller)
        {
            if (syncToServerDataPath || string.IsNullOrEmpty(client.LocalPath))
            {
                // Generate Id for this client configuration
                var updateClientKey = BitConverter.ToString(string
                    .Concat(client.FtpAddressString, client.UserName, client.Password)
                    .ComputeMd5Hash())
                    .Replace("-", string.Empty)
                    .ToLower();

                var tenantLocalPath = Path.Combine(ConfigurationManager.ServerDataPath,
                    string.Format(@"{0}\{1}", updateMonitorName, updateClientKey));

                // Copy client configuration and set new evaluated LocalPath
                client = new UpdateClientConfiguration
                {
                    LocalPath = tenantLocalPath,
                    FtpAddressString = client.FtpAddressString,
                    UserName = client.UserName,
                    Password = client.Password,
                    UploadLogs = client.UploadLogs,
                    EnableAutomaticServerUpdates = client.EnableAutomaticServerUpdates,
                };
            }


            MonitoringService.VerboseLog.TraceInformation("Update check started for {0}. Tenant: {1}. Using configuration:\r\n{2}.".FormatWith(
                updateMonitorName, 
                _tenantRegistry.CurrentTenantKey, 
                client));


            // Verify location exists
            FileSystem.EnsureDirectory(client.LocalPath);

            var ftpItems = _ftpClient.GetFtpDirectoryAndFileManifest(client.FtpAddress).ToList();
            SynchronizeLogFiles(client, ftpItems);
            SynchronizeLocalPath(client, ftpItems, downloadServerInstaller);
        }

        private void SynchronizeLogFiles(UpdateClientConfiguration client, IEnumerable<FtpManifestItem> ftpItems)
        {
            if (!client.UploadLogs) return;

            var localItems = FtpManifestItem.GetLocalDirectoryManifestItems(GetServerInstallPath())
                .Where(i => i.RelativePath.Value.StartsWith("~Logs/"));

            var comparer = new FtpComparer(localItems, ftpItems);
            var uploadUri = client.FtpAddress.Combine("~Logs");
            var localLogFilesToUpload = comparer.ItemsInSourceNotDestination.Concat(comparer.ItemsDifferent.Select(i => i.Item1)).ToArray();
            UploadLocalFiles(localLogFilesToUpload, uploadUri);
        }

        private void SynchronizeLocalPath(UpdateClientConfiguration client, IEnumerable<FtpManifestItem> ftpItems, bool downloadServerInstaller)
        {
            Func<FtpManifestItem, bool> isNotEscapedPath = i => !i.RelativePath.Value.Contains("~");

            var localItems = FtpManifestItem.GetLocalDirectoryManifestItems(client.LocalPath);

            var ftpComparer = new FtpComparer(ftpItems, localItems);

            var itemsToDelete = ftpComparer.ItemsInDestinationNotSource.Where(isNotEscapedPath);
            DeleteLocalItems(itemsToDelete, client.LocalPath);

            var itemsToDownload = ftpComparer
                .ItemsInSourceNotDestination
                .Concat(ftpComparer.ItemsDifferent
                    .Select(i => i.Item1)
                    .Where(i => !i.IsDirectory)) // It's not important to sync timestamp differences on folders
                .Where(isNotEscapedPath)
                .ToArray();

            if (!downloadServerInstaller)
            {
                // don't download server installer if not main client configuration
                itemsToDownload = itemsToDownload.Where(i => !i.Name.Equals(ServerInstallSetupName, StringComparison.OrdinalIgnoreCase)).ToArray();
            }

            itemsToDownload = FilterItemsByName(itemsToDownload, ItemsToExcludeInDownload);

            var downloadedItems = _ftpClient.DownloadRemoteItems(itemsToDownload.ToArray(), client.LocalPath);

            if (itemsToDownload.Any(i => i.Name == ServerInstallSetupName) && client.EnableAutomaticServerUpdates)
            {
                var serverInstallMsiPath = Directory.EnumerateFiles(client.LocalPath, ServerInstallSetupName, SearchOption.TopDirectoryOnly).First();
                var updater = new ServerUpdater();
                updater.UpdateServer(serverInstallMsiPath);
            }
            foreach (var itemToDownload in itemsToDownload.Where(i => ".sql".Equals(Path.GetExtension(i.Name), StringComparison.OrdinalIgnoreCase)))
            {
                RunSqlFile(Path.Combine(client.LocalPath, itemToDownload.RelativePath.Value));
            }

            // Extract any zip files and detect whether mdb files exist
            var mdbFiles = new List<String>();
            var fz = new FastZip();
            foreach (var zipFile in downloadedItems.Where(i => i.Extension.Equals(".zip", StringComparison.OrdinalIgnoreCase)))
            {
                fz.ExtractZip(zipFile.FullName, zipFile.FullName + "_extracted", null);
                mdbFiles.AddRange(Directory.GetFiles(zipFile.FullName + "_extracted", "*.mdb", SearchOption.AllDirectories));
            }

            SynchronizeMdbFiles(mdbFiles);
        }

        private static void SynchronizeMdbFiles(List<string> mdbFiles)
        {
            MdbSynchronizer.SynchronizeMdbFiles(mdbFiles);
            if (mdbFiles.Any(f => Path.GetFullPath(f).Contains("fdb_rxnorm_mapping.zip")))
            {
                RxNormMapper.Map();
            }
        }

        private static FtpManifestItem[] FilterItemsByName(IEnumerable<FtpManifestItem> itemsToFilter, IEnumerable<string> namedItemsToExclude)
        {
            return itemsToFilter.Where(x => !namedItemsToExclude.Any(y => x.Name.Equals(y, StringComparison.OrdinalIgnoreCase))).ToArray();
        }

        private void UploadLocalFiles(IEnumerable<FtpManifestItem> filesToUpload, Uri uploadDirectory)
        {
            foreach (var fileToUpload in filesToUpload.Where(i => !i.IsDirectory))
            {
                var uploadUri = uploadDirectory.Combine(fileToUpload.Name);
                _ftpClient.UploadFile(fileToUpload.AbsolutePath, uploadUri);
            }
        }

        private static void DeleteLocalItems(IEnumerable<FtpManifestItem> filesAndDirectoriesToDelete, string root)
        {
            var deleteDirectory = Path.Combine(root, "~Deleted");
            if (!Directory.Exists(deleteDirectory))
            {
                Directory.CreateDirectory(deleteDirectory);
            }

            foreach (var itemToDelete in filesAndDirectoriesToDelete.Where(i => i.Name != "IO Practiceware Client Setup.exe").OrderBy(item => !item.IsDirectory))
            {
                var deleteItemPath = Path.Combine(deleteDirectory, itemToDelete.RelativePath.Value);

                if (Directory.Exists(itemToDelete.AbsolutePath) || File.Exists(itemToDelete.AbsolutePath))
                {
                    DeleteLocalItem(itemToDelete, deleteItemPath);
                }
            }
        }

        private static void DeleteLocalItem(FtpManifestItem itemToDelete, string deleteItemPath)
        {
            try
            {
                if (Directory.Exists(deleteItemPath))
                {
                    var dirInfo = new DirectoryInfo(deleteItemPath);
                    dirInfo.Delete(true);
                }
                if (File.Exists(deleteItemPath))
                {
                    File.Delete(deleteItemPath);
                }
            }
            catch (Exception ex)
            {
                var exception = new InvalidOperationException(string.Format("Could not delete item {0}", deleteItemPath), ex);
                Trace.TraceError(exception.ToString());
            }

            try
            {
                var parentDeletedItemDirectory = Path.GetDirectoryName(deleteItemPath);
                if (parentDeletedItemDirectory != null && !Directory.Exists(parentDeletedItemDirectory)) Directory.CreateDirectory(parentDeletedItemDirectory);

                if (itemToDelete.IsDirectory)
                {
                    Directory.Move(itemToDelete.AbsolutePath, deleteItemPath);
                }
                else
                {
                    File.Move(itemToDelete.AbsolutePath, deleteItemPath);
                }
            }
            catch (Exception ex)
            {
                var exception = new InvalidOperationException(string.Format("Could not move item {0} to {1}", itemToDelete.AbsolutePath, deleteItemPath), ex);
                Trace.TraceError(exception.ToString());
            }
        }


        private static void RunSqlFile(string path)
        {
            try
            {
                var sql = File.ReadAllText(path);

                using (var connection = new SqlConnection(ConfigurationManager.PracticeRepositoryConnectionString))
                using (var command = connection.CreateCommand())
                {
                    connection.Open();

                    var result = new StringBuilder();

                    connection.FireInfoMessageEventOnUserErrors = true;
                    connection.InfoMessage += (sender, e) => result.AppendLine(e.Message.ToString());

                    command.StatementCompleted += (sender, e) => result.AppendLine(e.RecordCount.ToString() + " rows affected");

                    RunScript(connection, command, sql);

                    WriteSqlFileOutputToLog(GetServerInstallPath(), Path.GetFileName(path), result.ToString());
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                WriteSqlFileOutputToLog(GetServerInstallPath(), Path.GetFileName(path), ex.ToString());
            }
        }

        private static void RunScript(IDbConnection connection, IDbCommand command, string sql)
        {
            using (var transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                try
                {
                    command.Transaction = transaction;
                    command.CommandTimeout = 0;

                    foreach (var batch in sql.Split(new[] { Environment.NewLine + "GO" + Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        command.CommandText = batch;

                        if (command.CommandText.EndsWith(Environment.NewLine + "GO"))
                            command.CommandText = command.CommandText.Substring(0, command.CommandText.Length - (Environment.NewLine + "GO").Length);

                        command.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        private static void WriteSqlFileOutputToLog(string root, string fileName, string value)
        {
            var directory = Path.Combine(root, "~Logs");
            var destinationPath = Path.Combine(directory, string.Format("{0}.log", fileName));
            if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);
            File.WriteAllText(destinationPath, value);
        }
    }
}