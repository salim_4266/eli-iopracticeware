﻿using IO.Practiceware.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using IO.Practiceware.Monitoring.Service.Common;

namespace IO.Practiceware.Monitoring.Service.UpdateMonitor
{
    public static class MdbSynchronizer
    {
        public static void SynchronizeMdbFiles(IEnumerable<string> mdbFileNames)
        {
            var localMdbFileNames = mdbFileNames.ToArray();
            foreach (var file in localMdbFileNames)
            {
                var schemaName = ((Path.GetDirectoryName(file) ?? file).Split('\\').Last()).Split('.').First();

                var sqlConnectionString = ConfigurationManager.PracticeRepositoryConnectionString;
                var odbcConnectionString = string.Format("Driver={{Microsoft Access Driver (*.mdb)}};DBQ={0};", file);
                using (var odbcConnection = new OdbcConnection(odbcConnectionString))
                using (var sqlConnection = new SqlConnection(sqlConnectionString))
                {
                    odbcConnection.Open();
                    sqlConnection.Open();
                    var table = odbcConnection.GetSchema("Tables");
                    var listOfTables = MdbUtilities.GetTableData(table).ToList();

                    var columnTable = odbcConnection.GetSchema("Columns");

                    MdbUtilities.GetColumnData(columnTable, listOfTables);

                    MdbUtilities.CreateSchema(sqlConnection, schemaName);

                    foreach (var dbTable in listOfTables)
                    {
                        MdbUtilities.ClearTable(dbTable, sqlConnection, schemaName);

                        MdbUtilities.CreateTable(dbTable, sqlConnection, schemaName);

                        MdbUtilities.TransferTableData(dbTable, odbcConnection, sqlConnection, schemaName);
                    }

                    CreatePrimaryKeys(sqlConnection);

                }

            }

        }

        private static void CreatePrimaryKeys(SqlConnection sqlConnection)
        {
            var makeNonNullable = String.Format(@"ALTER TABLE fdb.HealthplanDetail
                ALTER COLUMN [HealthplanDetailID] INTEGER NOT NULL

                ALTER TABLE [fdb].[HealthplanSummary]
                ALTER COLUMN [HealthplanSummaryID] INTEGER NOT NULL

                ALTER TABLE [fdb].[RFMLINM0]
                ALTER COLUMN [ICD9CM] NVARCHAR(10) NOT NULL

                ALTER TABLE [fdb].[tblCompositeAllergy]
                ALTER COLUMN [CompositeAllergyID] INTEGER NOT NULL

                ALTER TABLE [fdb].[tblCompositeDrug]
                ALTER COLUMN [MEDID] INTEGER NOT NULL

                ALTER TABLE [fdb].[tblPharmacy]
                ALTER COLUMN [PharmacyGuid] UNIQUEIDENTIFIER NOT NULL

                ALTER TABLE [fdb].[viewBlackBoxFDB]
                ALTER COLUMN [GCN_SEQNO] INTEGER NOT NULL

                ALTER TABLE [fdb].[viewWSDrug]
                ALTER COLUMN [DrugID] INTEGER NOT NULL
                ALTER TABLE [fdb].[viewWSDrug]
                ALTER COLUMN [DrugName] NVARCHAR(30) NOT NULL");

            using (var cmd = new SqlCommand(makeNonNullable, sqlConnection))
            {
                cmd.ExecuteNonQuery();
            }


            var createTablePrimaryKey = String.Format(@"
                SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

                BEGIN TRANSACTION;

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__HealthplanDetail%') 
                ALTER TABLE fdb.HealthplanDetail
                ADD CONSTRAINT PK__HealthplanDetail PRIMARY KEY ([HealthplanDetailID])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__HealthplanSummar%') 
                ALTER TABLE [fdb].[HealthplanSummary]
                ADD CONSTRAINT PK__HealthplanSummary PRIMARY KEY ([HealthplanSummaryID])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__RFMLINM0%') 
                ALTER TABLE [fdb].[RFMLINM0]
                ADD CONSTRAINT PK__RFMLINM0 PRIMARY KEY ([ICD9CM])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__tblCompositeAlle%')
                ALTER TABLE [fdb].[tblCompositeAllergy]
                ADD CONSTRAINT PK__tblCompositeAllergy PRIMARY KEY ([CompositeAllergyID])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__tblCompositeDrug%') 
                ALTER TABLE [fdb].[tblCompositeDrug]
                ADD CONSTRAINT PK__tblCompositeDrug PRIMARY KEY ([MEDID])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__tblPharmacy%') 
                ALTER TABLE [fdb].[tblPharmacy]
                ADD CONSTRAINT PK__tblPharmacy PRIMARY KEY ([PharmacyGuid])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'%viewBlackBoxFDB%') 
                ALTER TABLE [fdb].[viewBlackBoxFDB]
                ADD CONSTRAINT PK__viewBlackBoxFDB PRIMARY KEY ([GCN_SEQNO])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__viewWSDrug%') 
                ALTER TABLE [fdb].[viewWSDrug]
                ADD CONSTRAINT PK__viewWSDrug PRIMARY KEY ([DrugID], [DrugName])

                COMMIT TRANSACTION;");

            using (var cmd = new SqlCommand(createTablePrimaryKey, sqlConnection))
            {
                cmd.ExecuteNonQuery();
            }
        }

    }
}