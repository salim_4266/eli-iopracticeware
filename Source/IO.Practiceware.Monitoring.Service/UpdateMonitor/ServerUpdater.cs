﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace IO.Practiceware.Monitoring.Service.UpdateMonitor
{

    public class ServerMsiProperties
    {
        public static string IntegrationServiceName = "IO Practiceware Integration Service";
        public static string InstallDirectoryPropertyName = "INSTALLDIR";


        private static ServerMsiProperties _instance;

        private ServerMsiProperties()
        {
            string executableDirectoryPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (string.IsNullOrEmpty(executableDirectoryPath))
            {
                throw new Exception("Could not find current update service executable directory.");
            }
            InstallDirectory = Path.GetFullPath(Path.Combine(executableDirectoryPath, ".."));
        }


        public static ServerMsiProperties Instance
        {
            get { return _instance ?? (_instance = new ServerMsiProperties()); }
        }

        public string InstallDirectory { get; set; }


    }

    public class ServerUpdater
        {
            // Can't use the update interval to timeout the msiexec because it is user-configurable. 
            // (If they choose too quick of an interval, the update will always fail.)
            // Instead use a default timeout of 300 minutes.
        private static readonly TimeSpan DefaultUpdateTimeout = new TimeSpan(hours: 0, minutes: 600, seconds: 0);
        private readonly ServerMsiProperties _msiProperties;

            public ServerUpdater()
            {
                _msiProperties = ServerMsiProperties.Instance;
            }

            public void UpdateServer(string serverMsiPath)
            {
                if (!File.Exists(serverMsiPath))
                {
                    throw new Exception(string.Format("MSI server file does not exist at {0} so no update was performed.", serverMsiPath));
                }

                ProcessStartInfo msiExec = GetMsiExecStartInfo(serverMsiPath);

                var process = Process.Start(msiExec);
                bool installTimedOut = !process.WaitForExit((int)DefaultUpdateTimeout.TotalMilliseconds);

                process.Close();
                process.Dispose();

                if (installTimedOut)
                {
                    throw new Exception(string.Format("Installing the msi at {0} with the arguments {1} resulted in a timeout of {2} minutes.  Please delete the msi, and re-run the update service or manually install the msi.",
                                                         serverMsiPath, msiExec.Arguments, DefaultUpdateTimeout.TotalMinutes));
                }
            }


            private ProcessStartInfo GetMsiExecStartInfo(string serverMsiPath)
            {
                var process = new Process();
                var startInfo = new ProcessStartInfo();
                startInfo.CreateNoWindow = true;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/c net stop \"" + MonitoringService.Name + "\"";
                startInfo.LoadUserProfile = false;
                startInfo.UseShellExecute = false;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo = startInfo;
                process.Start();
                process.WaitForExit();

                Trace.TraceInformation("Reinstalling Server Setup....");

                var logsDirectory = string.Format("{0}\\~Logs", _msiProperties.InstallDirectory);
                if (!Directory.Exists(logsDirectory)) Directory.CreateDirectory(logsDirectory);

                string msiLogPath = string.Format("{0}\\serverInstall_{1}.log", logsDirectory, DateTime.Now.ToString("yyyy.MM.dd_HH.mm.ss"));

                string commandLineString = string.Format("/i \"{0}\" /qn /norestart /L* \"{1}\"", serverMsiPath, msiLogPath);

                var processStartInfo = new ProcessStartInfo("msiexec.exe", commandLineString);
                processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                processStartInfo.UseShellExecute = false;
                return processStartInfo;
            }
        }


    }

