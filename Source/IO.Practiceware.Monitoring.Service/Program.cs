﻿using System.Diagnostics;
using IO.Practiceware.Configuration;
using System;
using System.Linq;
using System.ServiceProcess;

namespace IO.Practiceware.Monitoring.Service
{
    internal class Program
    {
        public static bool StartedAsWindowsService { get; private set; }

        // ReSharper disable UnusedParameter.Local
        // Suppressing the warning and keeping the function signature.
        private static void Main(string[] args)
        // ReSharper restore UnusedParameter.Local
        {
            // If UserInteractive, then this is not running as a service.
            // It is possible that this may be set to true for services under the following condition:
            // Windows XP with "Allow this service to interact with the desktop" (unlikely this will ever happen)

            ConfigurationManager.Load();
            ConfigurationManager.ConfigurationChanged += delegate
            {
                MonitoringService.Restart();
            };
            Bootstrapper.Run<IServiceProvider>();

            // Setup default logging for monitoring service's verbose log
            Trace.Listeners.OfType<TraceListener>().ToList().ForEach(i => MonitoringService.VerboseLog.Listeners.Add(i));
            if (MonitoringService.VerboseLog.Switch == null || MonitoringService.VerboseLog.Switch.Level == SourceLevels.Off)
                // only explicitly set the error level switch if it currently has a .NET provided default value (of Off)
                MonitoringService.VerboseLog.Switch = new SourceSwitch("MonitoringServiceTraceSwitch") { Level = SourceLevels.Error };

            if (args.Any(a => a.Equals("-service", StringComparison.OrdinalIgnoreCase)))
            {
                StartedAsWindowsService = true;
                var servicesToRun = new ServiceBase[] { new MonitoringService() };
                ServiceBase.Run(servicesToRun);
                return;
            }

            var shouldRePause = false;
            var shouldReStart = false;

            var monitoringServiceController = ServiceController.GetServices().FirstOrDefault(i => i.ServiceName == MonitoringService.Name);
            if (monitoringServiceController != null)
            {
                monitoringServiceController.WaitForCompleteOperation();

                if (monitoringServiceController.Status == ServiceControllerStatus.Paused)
                {
                    shouldRePause = true;
                }
                else if (monitoringServiceController.Status == ServiceControllerStatus.Running)
                {
                    shouldReStart = true;
                }

                if (monitoringServiceController.Status != ServiceControllerStatus.StopPending && monitoringServiceController.Status != ServiceControllerStatus.Stopped && monitoringServiceController.CanStop)
                {
                    monitoringServiceController.Stop();
                    monitoringServiceController.WaitForCompleteOperation();
                }
            }


            AppDomain.CurrentDomain.ProcessExit += delegate { if (MonitoringService.IsServiceRunning) MonitoringService.Stop(); };

            if (args.Any(a => a.Equals("-keepRunning", StringComparison.OrdinalIgnoreCase)))
            {
                MonitoringService.Start();
                Console.WriteLine("Press any key to stop");
                Console.ReadKey(true);
                MonitoringService.Stop();
            }
            else
            {
                MonitoringService.RunOnce();
            }

            if (monitoringServiceController != null)
            {
                monitoringServiceController.Refresh();
                if (shouldReStart && (monitoringServiceController.Status != ServiceControllerStatus.StartPending || monitoringServiceController.Status != ServiceControllerStatus.Running))
                {
                    monitoringServiceController.Start();
                    monitoringServiceController.WaitForCompleteOperation();
                }
                if (shouldRePause && (monitoringServiceController.Status != ServiceControllerStatus.PausePending || monitoringServiceController.Status != ServiceControllerStatus.Paused))
                {
                    monitoringServiceController.Pause();
                    monitoringServiceController.WaitForCompleteOperation();
                }
            }
        }
    }

    public static class UriExtensions
    {
        /// <summary>
        /// Appends a trailing slash to the Uri if not present.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns></returns>
        public static Uri AsDirectory(this Uri uri)
        {
            if (!uri.ToString().EndsWith("/")) return new Uri(uri + "/");
            return uri;
        }

        public static Uri Combine(this Uri uri, string value)
        {
            return new Uri(uri.AsDirectory(), value);
        }
    }

}