using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using IO.Practiceware.Monitoring.Service.Common;
using Quartz;
using Soaf.Configuration;
using Soaf.EntityFramework;

namespace IO.Practiceware.Monitoring.Service.Maintenance
{
    [DisallowConcurrentExecution]
    public class MemoryMonitor : IJob
    {
        private readonly IEntityFrameworkRepositoryProviderFactory _efProviderFactory;

        public MemoryMonitor(IEntityFrameworkRepositoryProviderFactory efProviderFactory)
        {
            _efProviderFactory = efProviderFactory;
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                if (!IsMaximumMemoryReached()) return;

                // Pause all current jobs in attempt to reclaim memory
                context.Scheduler.Standby();

                // Wait maximum 10 minutes until all jobs are idle
                var waitStart = DateTime.Now;
                while (DateTime.Now - waitStart < TimeSpan.FromMinutes(10))
                {
                    var pending = context.Scheduler.GetCurrentlyExecutingJobs()
                        .Any(jc => jc != context);
                    if (!pending)
                    {
                        break;
                    }
                    Thread.Sleep(1500);
                }

                // Cleanup EF cache
                _efProviderFactory.ClearEntityFrameworkCache();

                // Cleanup memory
                GC.Collect();
                GC.WaitForPendingFinalizers();

                // Good now? -> resume
                if (!IsMaximumMemoryReached())
                {
                    context.Scheduler.Start();
                    return;
                }

                // Restarting otherwise
                MonitoringService.Restart();
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                throw new JobExecutionException(ex);
            }
        }

        private bool IsMaximumMemoryReached()
        {
            var currentProcess = Process.GetCurrentProcess();
            var memorySizeInKb = currentProcess.PrivateMemorySize64 / 1024; //Converting PrivateMemorySize64 Bytes into KB
            var result = memorySizeInKb > ConfigurationSection<MonitoringServiceConfiguration>.Current.MaximumMemoryInKb;

            if (result)
            {
                Trace.TraceWarning("Monitoring service memory usage reached {0}Kb at {1}", memorySizeInKb, DateTime.Now);
            }

            return result;
        }
    }
}