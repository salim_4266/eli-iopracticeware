﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using IO.Practiceware.Application;
using IO.Practiceware.Monitoring.Service.Common;
using Quartz;

namespace IO.Practiceware.Monitoring.Service.Maintenance
{
    [DisallowConcurrentExecution]
    public class StartProcessMonitor : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var currentConfiguration = context.GetConfiguration<StartProcessMonitorConfiguration>();
                var psi = new ProcessStartInfo
                {
                    FileName = currentConfiguration.FileName,
                    Arguments = (currentConfiguration.Arguments ?? string.Empty)
                        .Replace("?ioEnvName?", ApplicationContext.Current.EnvironmentName),
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    WorkingDirectory = currentConfiguration.WorkingDirectory,
                    UseShellExecute = false
                };

                // Not waiting for exit -> start now
                var waitForExit = string.IsNullOrWhiteSpace(currentConfiguration.WaitForExit) 
                    || Convert.ToBoolean(currentConfiguration.WaitForExit);

                if (!waitForExit)
                {
                    Process.Start(psi);
                    return;
                }

                psi.RedirectStandardError = true;
                psi.RedirectStandardOutput = true;

                var p = new Process { StartInfo = psi };
                var waitTimeout = !string.IsNullOrWhiteSpace(currentConfiguration.WaitForExitTimeout)
                    ? Convert.ToInt32(currentConfiguration.WaitForExitTimeout)
                    : Convert.ToInt32(TimeSpan.FromMinutes(5).TotalMilliseconds);

                using (StringWriter outputWriter = new StringWriter(), errorWriter = new StringWriter())
                {
                    var countdownEvent = new CountdownEvent(2);

                    p.OutputDataReceived += (sender, args) =>
                    {
                        // WaitForExit doesn't wait for last input: http://stackoverflow.com/a/16496089
                        if (args.Data == null)
                        {
                            countdownEvent.AddCount(1);
                            return;
                        }

                        outputWriter.WriteLine(args.Data);
                    };
                    p.ErrorDataReceived += (sender, args) =>
                    {
                        if (args.Data == null)
                        {
                            countdownEvent.AddCount(1);
                            return;
                        }

                        errorWriter.WriteLine(args.Data);
                    };

                    var sw = new Stopwatch();
                    sw.Start();

                    // Start process and receiving output
                    p.Start();
                    p.BeginOutputReadLine();
                    p.BeginErrorReadLine();

                    // Wait or kill
                    var killed = false;
                    if (!p.WaitForExit(waitTimeout))
                    {
                        try
                        {
                            p.Kill();
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceError("Failed to kill process {0}. Error: {1}", context.JobDetail.Key.Name, ex);
                        }
                        killed = true;
                    }

                    sw.Stop();

                    // Wait to get all output
                    if (!countdownEvent.Wait(10*1000)) // 10 seconds
                    {
                        p.CancelErrorRead();
                        p.CancelOutputRead();
                    }

                    // Read exit code before disposing process resources (otherwise -> exception)
                    var exitCode = p.ExitCode;

                    // Free process resources
                    p.Close();
                    p.Dispose();

                    // Log result
                    var output = outputWriter.GetStringBuilder().ToString().Trim();
                    var error = errorWriter.GetStringBuilder().ToString().Trim();

                    var explain = killed
                        ? string.Format("timed out and was killed after waiting for {0}ms", waitTimeout)
                        : string.Format("completed in {0} with exit code {1}", sw.ElapsedMilliseconds, exitCode);
                    var logMessage = string.Format("{0} {1}. Output: {2}. Error: {3}",
                        context.JobDetail.Key.Name, explain, output, error);
                    if (exitCode == 0)
                    {
                        MonitoringService.VerboseLog.TraceEvent(TraceEventType.Verbose, 0, logMessage);
                    }
                    else
                    {
                        Trace.TraceError(logMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                throw new JobExecutionException(ex);
            }
        }
    }
}
