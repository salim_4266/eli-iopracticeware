﻿using System.Xml.Serialization;

namespace IO.Practiceware.Monitoring.Service.Maintenance
{
    public class StartProcessMonitorConfiguration
    {
        [XmlAttribute("fileName")]
        public string FileName { get; set; }

        [XmlAttribute("workingDirectory")]
        public string WorkingDirectory { get; set; }

        [XmlAttribute("arguments")]
        public string Arguments { get; set; }

        [XmlAttribute("waitForExit")]
        public string WaitForExit { get; set; }

        [XmlAttribute("waitForExitTimeout")]
        public string WaitForExitTimeout { get; set; }
    }
}
