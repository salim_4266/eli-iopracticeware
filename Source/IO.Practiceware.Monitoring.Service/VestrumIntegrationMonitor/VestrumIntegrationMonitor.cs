﻿using System;
using System.Data.SqlClient;
using IO.Practiceware.Data;
using IO.Practiceware.Monitoring.Service.Common;
using IO.Practiceware.Services.ApplicationSettings;
using Quartz;
using System.Net.Mail;
using System.IO;
using IO.Practiceware.Monitoring.Service.UpdateMonitor;
using IO.Practiceware.Monitoring.Service.DatabaseBackupMonitor;

namespace IO.Practiceware.Monitoring.Service.VestrumIntegrationMonitor
{
    [DisallowConcurrentExecution]
    internal class VestrumIntegrationMonitor : TenantJob
    {
        private const string VestrumIntegrationMonitorConfigurationSettingKey = "VestrumIntegrationMonitorConfiguration";
        private readonly IApplicationSettingsService _settingsService;
        private const string VestrumString = @"_Vestrum";


        public VestrumIntegrationMonitor(IApplicationSettingsService settingsService)
        {
            _settingsService = settingsService;
        }


        protected override void Execute(IJobExecutionContext context)
        {
            try
            {
                //Read current Vestrum DBOperations monitor configuration
                var configuration = _settingsService.GetSetting<VestrumIntegrationMonitorConfiguration>(VestrumIntegrationMonitorConfigurationSettingKey);
                if (configuration == null || configuration.Value == null || String.IsNullOrEmpty(configuration.Value.FtpAddress)) return;

                string versionNumber = GetSqlVersion();

                bool isCompressed = true;

                if (versionNumber == "Unsupported SQL Server Version")
                    throw new Exception("Unsupported SQL Server Version");

                if (versionNumber == "SQL Server 2000" || versionNumber == "SQL Server 2005")
                    isCompressed = false;

                string backupDirectory = GetDefaultBackupDirectory();

                if (String.IsNullOrEmpty(backupDirectory))
                    throw new Exception("Backup directory does not exist.");

                string backupNameWithPath = CreateBackup(backupDirectory, string.Empty, isCompressed);
                if (String.IsNullOrEmpty(backupNameWithPath))
                    throw new Exception("Backup file is not created.");

                string newDbName = RestoreDatabase(backupNameWithPath);

                ExecuteSqlScripts(newDbName);


                string backupNewDbNameWithPath = CreateBackup(backupDirectory, VestrumString, isCompressed);

                DropDatabase(newDbName);

                CopyFileToServer(backupNewDbNameWithPath, configuration.Value.FtpAddress, configuration.Value.FtpUserName, configuration.Value.FtpPassword);

                string[] fileNames = { backupNameWithPath, backupNewDbNameWithPath };
                DeleteBackupFiles(fileNames);
            }
            catch (Exception ex)
            {
                using (var notificationEmail = new MailMessage())
                {
                    notificationEmail.Subject = string.Format("ATTENTION: Error in Vestrum Integration Monitor Job on {0}.", DateTime.Now);
                    notificationEmail.Body = ex.Message;
                    VestrumIntegrationUtilities.SendEmail(notificationEmail);
                    throw;
                }
            }
        }

        /// <summary>
        /// Calling DatabaseBackupUtilities.GetSqlServerVersion() to get the Sql Server Version.
        /// </summary>
        /// <returns></returns>
        private static string GetSqlVersion()
        {
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                string version = DatabaseBackupUtilities.GetSqlServerVersion(connection);
                return version;
            }
        }

        /// <summary>
        /// Calling DatabaseBackupUtilities.GetDefaultBackUpDirectory() to get the Default backup directory
        /// </summary>
        /// <returns></returns>
        private static string GetDefaultBackupDirectory()
        {
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                string backupDirectory = DatabaseBackupUtilities.GetDefaultBackUpDirectory(connection);
                return backupDirectory;
            }
        }


        /// <summary>
        /// Creates the backup.
        /// </summary>
        /// <param name="backupDirectory">The backup directory.</param>
        /// <param name="dbSuffix">The database suffix.</param>
        /// <param name="isCompressed">if set to <c>true</c> [is compressed].</param>
        /// <returns></returns>
        private static string CreateBackup(string backupDirectory, string dbSuffix, bool isCompressed)
        {
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                string dbName = connection.Database + dbSuffix + @"_" + DateTime.Now.ToString("yyyy.MM.dd_HH.mm.ss") + @".bak";

                DatabaseBackupUtilities.BackupDatabase(connection, backupDirectory, dbName, isCompressed);

                string backupNameWithPath = Path.Combine(backupDirectory, dbName);
                return backupNameWithPath;
            }
        }

        /// <summary>
        /// Calling DatabaseBackupUtilities.RestoreDatabase() to restore the database
        /// </summary>
        /// <param name="backupNameWithPath">The backup name with path.</param>
        /// <returns></returns>
        private static string RestoreDatabase(string backupNameWithPath)
        {
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                string newDbName = connection.Database + VestrumString;
                newDbName = DatabaseBackupUtilities.RestoreDatabase(connection, backupNameWithPath, newDbName, VestrumString);
                return newDbName;
            }
        }

        /// <summary>
        /// Calling VestrumIntegrationUtilities.ExecuteVestrumScripts() to execute the required scripts
        /// </summary>
        /// <param name="dbName">Name of the database.</param>
        private static void ExecuteSqlScripts(string dbName)
        {
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                VestrumIntegrationUtilities.ExecuteVestrumScripts(connection, dbName);
            }
        }


        /// <summary>
        /// Calling DatabaseBackupUtilities.DropDatabase() to drop the Database
        /// </summary>
        /// <param name="dbName">Name of the database.</param>
        private static void DropDatabase(string dbName)
        {
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                DatabaseBackupUtilities.DropDatabase(connection, dbName);
            }
        }


        /// <summary>
        /// Copies the back up file to FTP server.
        /// </summary>
        /// <param name="backupPath">Path of backup.</param>
        /// <param name="address">The address.</param>
        /// <param name="userName">The user identifier.</param>
        /// <param name="password">The password.</param>
        private void CopyFileToServer(string backupPath, string address, string userName, string password)
        {
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                string sqlserverPath = @"\\" + (new SqlConnectionStringBuilder(connection.ConnectionString).DataSource).Split('\\')[0] + @"\" + backupPath.Replace(':', '$');
                string destinationFileName = Path.GetFileName(backupPath);
                var uriBuilder = new UriBuilder(address);
                uriBuilder.UserName = Uri.EscapeDataString(userName);
                uriBuilder.Password = Uri.EscapeDataString(password);
                uriBuilder.Uri.Combine(destinationFileName);
                FtpClient ftpClient = new FtpClient();
                ftpClient.UploadFile(sqlserverPath, uriBuilder.Uri.Combine(destinationFileName));

                MonitoringService.VerboseLog.TraceInformation("Uploaded {0}.", destinationFileName);
            }
        }


        /// <summary>
        /// Calling DatabaseBackupUtilities.DeleteBackupFiles() to delete the backup files.
        /// </summary>
        /// <param name="fileNames">The file names.</param>
        private static void DeleteBackupFiles(string[] fileNames)
        {
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                for (int i = 0; i < fileNames.Length; i++)
                {
                    DatabaseBackupUtilities.DeleteBackupFiles(connection, fileNames[0]);
                }
            }
        }
    }
}
