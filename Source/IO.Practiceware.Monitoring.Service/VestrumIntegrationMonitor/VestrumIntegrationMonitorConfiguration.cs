﻿using System.Runtime.Serialization;


namespace IO.Practiceware.Monitoring.Service.VestrumIntegrationMonitor
{
    [DataContract]
    public class VestrumIntegrationMonitorConfiguration
    {
        [DataMember]
        public string FtpAddress { get; set; }

        [DataMember]
        public string FtpUserName { get; set; }

        [DataMember]
        public string FtpPassword { get; set; }

    }
}
