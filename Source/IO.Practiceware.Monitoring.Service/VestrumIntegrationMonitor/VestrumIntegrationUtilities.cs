﻿using System.Data;
using Soaf.Data;
using System.Net.Mail;
using Soaf.Configuration;
using IO.Practiceware.Monitoring.Service.Common;

namespace IO.Practiceware.Monitoring.Service.VestrumIntegrationMonitor
{
    internal static class VestrumIntegrationUtilities
    {

        /// <summary>
        /// Executes the vestrum scripts.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="dbName">Name of the database.</param>
        public static void ExecuteVestrumScripts(IDbConnection connection, string dbName)
        {
            connection.ChangeDatabase(dbName);
            //This query will be updated with the vestrum scripts
            var sqlScripts = @"SELECT TOP 1 * FROM model.Patients WHERE 1 = 2 ";
            MonitoringService.VerboseLog.TraceInformation(@"Executing Vestrum scripts against connection string {0}", connection.ConnectionString);
            connection.Execute(sqlScripts);

        }


        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="notificationEmail">The notification email.</param>
        public static void SendEmail(MailMessage notificationEmail)
        {
            var emailAddresses = ConfigurationSection<MonitoringServiceConfiguration>.Current.AlertsDestinationEmailAddresses.Split(';');
            foreach (var emailAddress in emailAddresses)
            {
                notificationEmail.To.Add(emailAddress);
            }

            var smtpClient = new SmtpClient();

            smtpClient.Send(notificationEmail);
            MonitoringService.VerboseLog.TraceInformation("Email sent successfully to {" + notificationEmail.To + "}.");
        }



    }
}
