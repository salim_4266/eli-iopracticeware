﻿using System;
using System.Diagnostics;
using System.Linq;
using IO.Practiceware.Integration.Cda;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Monitoring.Service.Common;
using Quartz;
using Soaf;
using Soaf.Linq;

namespace IO.Practiceware.Monitoring.Service.Integration
{
    /// <summary>
    /// Job to process outbound Ccda messages.
    /// </summary>
    [DisallowConcurrentExecution]
    internal class CcdaMessageProcessorMonitor : TenantJobWithState<CcdaMessageProcessorMonitor.State>
    {
        private readonly Func<OptimizedCcdaMessageProcessor> _getProcessor;

        public CcdaMessageProcessorMonitor(Func<OptimizedCcdaMessageProcessor> getProcessor)
        {
            _getProcessor = getProcessor;
        }

        protected override State Initialize()
        {
            return new State { IsDisposed = false };
        }

        protected override void Execute(IJobExecutionContext context, State stateObject)
        {
            var processor = _getProcessor();
            while (CanContinueRunning(context, stateObject.IsDisposed))
            {
                // Process while allowed and there are messages to process
                if (!processor.ProcessNextBatchOfMessages(() => CanContinueRunning(context, stateObject.IsDisposed)))
                {
                    break;
                }
            }
        }

        internal class OptimizedCcdaMessageProcessor : MessageProcessor
        {
            // Ccda message types
            private static readonly int[] CcdaMessageTypes =
            {
                (int)ExternalSystemMessageTypeId.PatientTransitionOfCareCCDA_Outbound,
                (int)ExternalSystemMessageTypeId.EncounterClinicalSummaryCCDA_Outbound
            };

            // Supported states of messages to fetch
            private static readonly int[] CcdaSupportedProcessingStates =
            {
                (int)ExternalSystemMessageProcessingStateId.Unprocessed,
                (int)ExternalSystemMessageProcessingStateId.Processing
            };

            private readonly Func<CdaMessageProducer> _getCcdaMessageProducer;

            public OptimizedCcdaMessageProcessor(IPracticeRepository practiceRepository, IMessenger messenger, Func<CdaMessageProducer> getCcdaMessageProducer) 
                : base(practiceRepository, messenger)
            {
                _getCcdaMessageProducer = getCcdaMessageProducer;
            }

            /// <summary>
            /// Processes the next batch of messages.
            /// </summary>
            /// <param name="shouldContinueProcessing"></param>
            /// <param name="batchSize">Size of the batch.</param>
            public bool ProcessNextBatchOfMessages(Func<bool> shouldContinueProcessing = null, int batchSize = 10)
            {
                var haveMore = true;

                ProcessMessages((practiceRepository, processNextMessage) =>
                {
                    // Load messages
                    var messagesQuery = practiceRepository.ExternalSystemMessages
                        .Where(i => i.ProcessingAttemptCount < MaxAttemptCount
                                    && !i.ExternalSystemExternalSystemMessageType.IsDisabled
                                    && CcdaMessageTypes.Contains(i.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId)
                                    && CcdaSupportedProcessingStates.Contains(i.ExternalSystemMessageProcessingStateId));

                    var messagesBatch = messagesQuery
                        .OrderBy(i => i.UpdatedDateTime)
                        .Take(batchSize)
                        .ToList();

                    // No messages to process -> quit
                    if (!messagesBatch.Any())
                    {
                        haveMore = false;
                        return;
                    }

                    // Load all the fields for these messages
                    var loadFactory = practiceRepository.AsQueryableFactory();
                    loadFactory.Load(messagesBatch, i => i.ExternalSystemExternalSystemMessageType.ExternalSystem,
                            i => i.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType,
                            i => i.ExternalSystemMessagePracticeRepositoryEntities.Select(j => j.PracticeRepositoryEntity));

                    var messageIds = messagesBatch.Select(m => m.Id).ToList();
                    // Find duplicates
                    var messageDuplicates = practiceRepository.ExternalSystemMessages
                        .Where(m => messageIds.Contains(m.Id) 
                            // We support searching for duplicates only when there is 1 mentioned entity of given type
                            // Note: CCda outbound types only mention 1 entity, so safe to not use filter below
                            //&& m.ExternalSystemMessagePracticeRepositoryEntities.Count == 1
                            )
                        .SelectMany(m => m.ExternalSystemMessagePracticeRepositoryEntities)
                        .Select(m => new
                        {
                            Id = m.ExternalSystemMessageId,
                            Duplicates = messagesQuery
                                .Where(mdup => mdup.Id != m.ExternalSystemMessageId 
                                    && mdup.ExternalSystemMessagePracticeRepositoryEntities
                                    .All(me => me.PracticeRepositoryEntityKey == m.PracticeRepositoryEntityKey && me.PracticeRepositoryEntityId == m.PracticeRepositoryEntityId))
                                .Select(mdup => mdup.Id)
                                .Distinct()
                                .ToList()
                        })
                        .ToList();

                    // Filter out duplicates from current batch
                    var duplicatesToProcess = messageDuplicates.ToList();
                    while (duplicatesToProcess.Count > 0)
                    {
                        var duplicateToProcess = duplicatesToProcess.First();
                        duplicatesToProcess.Remove(duplicateToProcess);

                        // Remove duplicates from messages batch
                        var toRemove = messagesBatch
                            .Where(m => duplicateToProcess.Duplicates.Contains(m.Id))
                            .ToList();
                        toRemove.ForEach(m => messagesBatch.Remove(m));

                        // Remove from duplicates to process as well
                        duplicatesToProcess.RemoveAll(md => toRemove.Any(m => m.Id == md.Id));
                    }

                    // Produce ccda messages in batch
                    var producer = _getCcdaMessageProducer();
                    var produceRequests = messagesBatch
                        // Only for messages that can go out
                        .Where(m => !m.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType.IsOutbound
                            || CanMessageGoOutbound(m))
                        .Select(m => new ProduceMessageRequest
                        {
                            ExternalSystemMessage = m
                        }).ToList();

                    var batchProduceFailed = false;
                    try
                    {
                        producer.ProduceMessages(produceRequests);

                        // Save produced message on external system message before processing (so that message is reused)
                        produceRequests.ForEach(r => r.ExternalSystemMessage.Value = r.ProducedMessage.ToString());
                    }
                    catch (Exception ex)
                    {
                        batchProduceFailed = true;
                        Trace.TraceWarning("Producing a batch of ccda messages failed. Will fall back to one by one generation. Error: {0}", ex);
                    }

                    // Process messages
                    foreach (var message in messagesBatch)
                    {
                        // If batch produce didn't fail -> we must complete processing of all messages in a batch. Otherwise -> continue with check
                        if (batchProduceFailed && shouldContinueProcessing != null && !shouldContinueProcessing()) return;

                        if (!message.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType.IsOutbound
                            || CanMessageGoOutbound(message))
                        {
                            processNextMessage(message);
                        }
                        else
                        {
                            message.Error = string.Format("No outbound endpoints configured for destination '{0}' and message type '{1}'", 
                                message.ExternalSystemExternalSystemMessageType.ExternalSystem.Name, 
                                message.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType.Name);
                            UpdateMessageStateAndSave(message, ProcessingState.Processed);

                            Trace.TraceWarning("Message {0} had no handlers that supported sending the message. Error: {1}", message.Id, message.Error);
                        }

                        // Find duplicates info
                        var duplicatesInfo = messageDuplicates.FirstOrDefault(md => md.Id == message.Id);
                        
                        // Skip if no duplicates
                        if (duplicatesInfo == null || duplicatesInfo.Duplicates.Count <= 0) continue;

                        // Fetch actual messages
                        var duplicates = practiceRepository.ExternalSystemMessages
                            .Where(esm => duplicatesInfo.Duplicates.Contains(esm.Id))
                            .ToList();

                        // Update outcome
                        foreach (var duplicate in duplicates)
                        {
                            duplicate.Error = message.Error;
                            duplicate.CorrelationId = message.CorrelationId;
                            duplicate.ExternalSystemMessageProcessingStateId = message.ExternalSystemMessageProcessingStateId;
                            duplicate.ProcessingAttemptCount = message.ProcessingAttemptCount;
                            duplicate.UpdatedDateTime = message.UpdatedDateTime;
                            duplicate.Value = message.Value;
                        }

                        // Save
                        practiceRepository.Save(duplicates);
                    }
                });

                return haveMore;
            }

            /// <summary>
            /// Determines whether this message can go outbound through any of the endpoints.
            /// </summary>
            private static bool CanMessageGoOutbound(ExternalSystemMessage message)
            {
                var messageType = message.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType.Name;
                var destination = message.ExternalSystemExternalSystemMessageType.ExternalSystem.Name;

                var canGoOut = MessagingConfiguration.Current.WebServiceMessageConnectorConfiguration.OutboundEndpoints.For(destination, messageType) != null
                    || MessagingConfiguration.Current.SocketsMessageConnectorConfiguration.OutboundEndpoints.For(destination, messageType) != null
                    || MessagingConfiguration.Current.FileMessageConnectorConfiguration.OutboundEndpoints.For(destination, messageType) != null;
                return canGoOut;
            }
        }

        internal class State : IDisposable
        {
            public bool IsDisposed { get; set; }

            public void Dispose()
            {
                IsDisposed = true;
            }
        }
    }
}