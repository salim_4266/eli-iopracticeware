﻿using IO.Practiceware.Integration.HL7.Connections;
using IO.Practiceware.Monitoring.Service.Common;
using Quartz;
using Soaf;
using System;

namespace IO.Practiceware.Monitoring.Service.Integration
{
    /// <summary>
    /// Wraps the WebServiceMessageConnector for initialization and disposal via monitoring service.
    /// </summary>
    [DisallowConcurrentExecution]
    public class WebServiceMessageConnectorMonitor : TenantJobWithState<WebServiceMessageConnector>
    {
        private readonly TenantBoundMessenger _messenger;
        private readonly Func<IMessenger, WebServiceMessageConnector> _connectorConstruct;

        public WebServiceMessageConnectorMonitor(
            TenantBoundMessenger messenger,
            Func<IMessenger, WebServiceMessageConnector> connectorConstruct)
        {
            _messenger = messenger;
            _connectorConstruct = connectorConstruct;
        }

        protected override void Execute(IJobExecutionContext context, WebServiceMessageConnector stateObject)
        { }

        protected override WebServiceMessageConnector Initialize()
        {
            var connector = _connectorConstruct(_messenger);
            connector.Start();
            return connector;
        }
    }
}