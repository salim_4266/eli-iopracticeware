﻿using IO.Practiceware.Integration.HL7.Connections;
using IO.Practiceware.Monitoring.Service.Common;
using Quartz;
using Soaf;
using System;

namespace IO.Practiceware.Monitoring.Service.Integration
{
    /// <summary>
    /// Wraps the SocketsMessageConnector for initialization and disposal via monitoring service.
    /// </summary>
    [DisallowConcurrentExecution]
    public class SocketsMessageConnectorMonitor : TenantJobWithState<SocketsMessageConnector>
    {
        private readonly TenantBoundMessenger _messenger;
        private readonly Func<IMessenger, SocketsMessageConnector> _connectorConstruct;

        public SocketsMessageConnectorMonitor(
            TenantBoundMessenger messenger, 
            Func<IMessenger, SocketsMessageConnector> connectorConstruct)
        {
            _messenger = messenger;
            _connectorConstruct = connectorConstruct;
            
            // We need to listen sockets on every machine (especially in load balanced environment)
            SupportsConcurrency = true;
        }

        protected override void Execute(IJobExecutionContext context, SocketsMessageConnector stateObject)
        {}

        protected override SocketsMessageConnector Initialize()
        {
            var connector = _connectorConstruct(_messenger);
            connector.Start();
            return connector;
        }
    }
}