﻿using IO.Practiceware.Integration.HL7.Connections;
using IO.Practiceware.Monitoring.Service.Common;
using Quartz;
using Soaf;
using System;

namespace IO.Practiceware.Monitoring.Service.Integration
{
    /// <summary>
    /// Wraps the FileMessageConnector using a scheduled job.
    /// </summary>
    [DisallowConcurrentExecution]
    public class FileMessageConnectorMonitor : TenantJobWithState<FileMessageConnector>
    {
        private readonly TenantBoundMessenger _messenger;
        private readonly Func<IMessenger, FileMessageConnector> _connectorConstruct;

        public FileMessageConnectorMonitor(
            TenantBoundMessenger messenger,
            Func<IMessenger, FileMessageConnector> connectorConstruct)
        {
            _messenger = messenger;
            _connectorConstruct = connectorConstruct;
        }

        protected override void Execute(IJobExecutionContext context, FileMessageConnector stateObject)
        {
            stateObject.ProcessFiles();
        }

        protected override FileMessageConnector Initialize()
        {
            var connector = _connectorConstruct(_messenger);
            connector.Start();
            return connector;
        }
    }
}