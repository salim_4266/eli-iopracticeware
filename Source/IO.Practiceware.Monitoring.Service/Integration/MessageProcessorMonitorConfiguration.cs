using System.Xml.Serialization;
using IO.Practiceware.Model;

namespace IO.Practiceware.Monitoring.Service.Integration
{
    public class MessageProcessorMonitorConfiguration
    {
        public MessageProcessorMonitorConfiguration()
        {
            Includes = new ExternalSystemMessageTypeId[0];
            Excludes = new ExternalSystemMessageTypeId[0];
        }

        [XmlArray("includeMessageTypes")]
        [XmlArrayItem("messageType")]
        public ExternalSystemMessageTypeId[] Includes { get; set; }

        [XmlArray("excludeMessageTypes")]
        [XmlArrayItem("messageType")]
        public ExternalSystemMessageTypeId[] Excludes { get; set; }
    }
}