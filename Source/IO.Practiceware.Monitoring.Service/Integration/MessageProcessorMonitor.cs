﻿using System;
using System.Linq;
using System.Linq.Expressions;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Monitoring.Service.Common;
using Quartz;

namespace IO.Practiceware.Monitoring.Service.Integration
{
    /// <summary>
    ///     Wraps the MessageProcessor in a scheduled job.
    /// </summary>
    [DisallowConcurrentExecution]
    internal class MessageProcessorMonitor : TenantJobWithState<MessageProcessorMonitor.State>
    {
        private readonly Func<MessageProcessor> _getProcessor;

        public MessageProcessorMonitor(Func<MessageProcessor> getProcessor)
        {
            _getProcessor = getProcessor;
        }

        protected override State Initialize()
        {
            return new State { HandledAbandonedTasks = false, IsDisposed = false };
        }

        protected override void Execute(IJobExecutionContext context, State stateObject)
        {
            var processor = _getProcessor();
            var currentConfiguration = context.GetConfiguration<MessageProcessorMonitorConfiguration>();
            Expression<Func<ExternalSystemMessage, bool>> filter = null;

            if (currentConfiguration != null)
            {
                var includeIds = currentConfiguration.Includes.Select(i => (int)i).ToArray();
                var excludeIds = currentConfiguration.Excludes.Select(i => (int)i).ToArray();

                // Create filter expression
                filter = esm =>
                    (includeIds.Length == 0 || includeIds.Contains(esm.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId))
                    && (excludeIds.Length == 0 || !excludeIds.Contains(esm.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId));
            }

            // Force processing of messages left in "Processing" state by previous execution
            var statesFilter = new[] { ExternalSystemMessageProcessingStateId.Unprocessed };

            if (!stateObject.HandledAbandonedTasks)
            {
                statesFilter = new[] { ExternalSystemMessageProcessingStateId.Processing }.Concat(statesFilter).ToArray();
                stateObject.HandledAbandonedTasks = true;
            }

            processor.ProcessMessages(filter, () => CanContinueRunning(context, stateObject.IsDisposed), statesFilter);
        }

        internal class State : IDisposable
        {
            public bool HandledAbandonedTasks { get; set; }
            public bool IsDisposed { get; set; }

            public void Dispose()
            {
                IsDisposed = true;
            }
        }
    }
}