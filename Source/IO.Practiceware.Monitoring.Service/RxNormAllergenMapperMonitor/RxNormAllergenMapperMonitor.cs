﻿using System;
using System.Data.SqlClient;
using IO.Practiceware.Configuration;
using IO.Practiceware.Monitoring.Service.Common;
using Quartz;

namespace IO.Practiceware.Monitoring.Service.RxNormAllergenMapperMonitor
{
    /// <summary>
    /// This monitoring service maps allergens to RxNorm
    /// It can be expected to run anywhere from 20 minutes to taking a few hours depending on the user's internet connection.
    /// This is at least partially due to going through a million rows of allergens, as well as comparing the existing rows  to find what's missing
    /// so that the database can be up to date
    /// </summary>
    [DisallowConcurrentExecution]
    internal class RxNormAllergenMapperMonitor : TenantJob
    {
        protected override void Execute(IJobExecutionContext context)
        {
            Map();
        }
        public static void Map()
        {
            var sb = new SqlConnectionStringBuilder(ConfigurationManager.PracticeRepositoryConnectionString);
            sb.ConnectTimeout = 10800;
            using (var sqlConnection = new SqlConnection(sb.ConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(String.Format(@"
                    SET NOCOUNT ON;
                    DECLARE @RxNormExternalSystemId INT
                    SELECT @RxNormExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'RxNorm'

                    DECLARE @AllergenId INT
                    SELECT @AllergenId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'Allergen'

                    DECLARE @RxNormAllergen INT
                    SELECT @RxNormAllergen = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @RxNormExternalSystemId
	                    AND Name = 'Allergen'

                    CREATE TABLE #AllergenMappingToRxNormQueries (
	                    Id INT IDENTITY,
	                    Query nvarchar(MAX)
                    )

                    SELECT RXCUI
		                    ,[STR]
		                    ,SUPPRESS 
                    INTO #RxNorm 
                    FROM rxNorm.RXNCONSO

                    SELECT Id, Name
                    INTO #Allergens
                    FROM model.Allergens

                    ;WITH CTE AS (
	                    SELECT 
	                    ROW_NUMBER() OVER (PARTITION BY RXCUI,[STR],Suppressor,Name,Id
		                    ORDER BY RXCUI,[STR],Suppressor,Name,Id
		                    ) AS rn
	                    ,RXCUI 
	                    ,[STR]
	                    ,Suppressor
	                    ,Name
	                    ,Id
	                    FROM (
		                    SELECT 
		                    MAX(RXCUI) AS RXCUI
		                    ,[STR]
		                    ,MIN(CASE SUPPRESS
			                    WHEN 'N'
				                    THEN 1
			                    WHEN 'E'
				                    THEN 2
			                    WHEN 'Y'
				                    THEN 3
			                    WHEN 'O'
				                    THEN 4
		                    END) AS Suppressor
		                    ,a.Name
		                    ,a.Id
		                    FROM #RxNorm b
		                    INNER JOIN #Allergens a ON a.Name = b.[STR]
		                    GROUP BY [STR]
		                    ,a.Name
		                    ,a.Id
	                    )v
                    )
                    SELECT * INTO #AllResults FROM(
	                    SELECT @AllergenId AS PracticeRepositoryEntityId, 
	                    CONVERT(NVARCHAR(MAX),c.Id) AS PracticeRepositoryEntityKey, 
	                    @RxNormAllergen AS ExternalSystemEntityId, 
	                    RXCUI AS ExternalSystemEntityKey
	                    FROM CTE c
	                    INNER JOIN model.Allergens a ON a.Id = c.Id
	                    WHERE c.rn = 1
	                    GROUP BY c.Id, RXCUI

	                    EXCEPT

	                    SELECT PracticeRepositoryEntityId AS PracticeRepositoryEntityId, 
	                    PracticeRepositoryEntityKey AS PracticeRepositoryEntityKey, 
	                    ExternalSystemEntityId AS ExternalSystemEntityId, 
	                    ExternalSystemEntityKey  AS ExternalSystemEntityKey
	                    FROM model.ExternalSystemEntityMappings
	                    WHERE ExternalSystemEntityId = @RxNormAllergen
	                    AND PracticeRepositoryEntityId =@AllergenId
                    ) ar

                    INSERT INTO #AllergenMappingToRxNormQueries (Query)
                    SELECT 
                    'SELECT @AllergenId, '+ar.PracticeRepositoryEntityKey+', @RxNormAllergen, '+ar.ExternalSystemEntityKey AS Query
                    FROM #AllResults ar

                    DECLARE @Sql NVARCHAR(MAX)
                    SET @Sql = 'INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey) '
                    DECLARE @FullSql NVARCHAR(MAX)

                    DECLARE @MaxId INT
                    SET @MaxId = (SELECT MAX(Id) FROM #AllergenMappingToRxNormQueries)

                    DECLARE @Id INT
                    SET @Id = 1

                    WHILE (@Id <@MaxId)
	                    BEGIN
		                    SET @FullSql = @SQL + (SELECT Query FROM #AllergenMappingToRxNormQueries WHERE @Id = Id)
		                    EXEC sp_executesql @FullSql, N'@AllergenId INT, @RxNormAllergen INT', @AllergenId, @RxNormAllergen
		                    SET @Id = @Id + 1
	                    END
	
                    DROP TABLE #AllergenMappingToRxNormQueries
                    DROP TABLE #RxNorm
                    DROP TABLE #Allergens
                    DROP TABLE #AllResults
                    SET NOCOUNT OFF"), sqlConnection))
                {
                    sqlCommand.CommandTimeout = sqlConnection.ConnectionTimeout;
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }
    }
}
