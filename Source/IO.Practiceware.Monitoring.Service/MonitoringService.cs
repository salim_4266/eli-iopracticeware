﻿using IO.Practiceware.Monitoring.Service.Common;
using Quartz;
using Quartz.Impl;
using Soaf;
using Soaf.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;

namespace IO.Practiceware.Monitoring.Service
{
    public class MonitoringService : ServiceBase
    {
        public static string Id { get; private set; }
        public static string Name = "IO Practiceware Monitoring Service";

        /// <summary>
        /// Specialized trace source for verbose logging within Monitoring Service
        /// </summary>
        public static readonly TraceSource VerboseLog = new TraceSource(typeof(MonitoringService).Name);

        private static readonly IServiceProvider ServiceProvider;
        private static readonly IScheduler Scheduler;
        private static readonly JobListener JobListener = new JobListener();
        private static readonly TimeSpan ExecutingJobsStopWaitTimeout = TimeSpan.FromSeconds(30);

        static MonitoringService()
        {
            Id = string.Join("_", Name, Environment.MachineName, Guid.NewGuid());
            ServiceProvider = Bootstrapper.Run<IServiceProvider>();
            Scheduler = StdSchedulerFactory.GetDefaultScheduler();
            Scheduler.ListenerManager.AddSchedulerListener(new SchedulerListener(Scheduler));
            Scheduler.ListenerManager.AddJobListener(JobListener);
            Scheduler.JobFactory = new JobFactory(ServiceProvider);
        }

        public static bool IsServiceRunning { get; private set; }

        public static void Start()
        {
            var instance = new MonitoringService();
            instance.OnStart(new string[0]);
        }

        public new static void Stop()
        {
            Scheduler.Shutdown();

            // Wait maximum 30 seconds until all jobs are idle
            var waitStart = DateTime.Now;
            while (DateTime.Now - waitStart < ExecutingJobsStopWaitTimeout)
            {
                var pending = Scheduler.GetCurrentlyExecutingJobs().Any();
                if (!pending)
                {
                    break;
                }
                Thread.Sleep(1500);
            }

            // Reset registry to clean it up
            var registry = ServiceProvider.GetService<ITenantRegistry>();
            registry.Reset();

            IsServiceRunning = false;
        }

        public static void Restart()
        {
            if (Program.StartedAsWindowsService)
            {
                var process = new Process();
                var startInfo = new ProcessStartInfo
                {
                    CreateNoWindow = true,
                    FileName = "cmd.exe",
                    Arguments = string.Format("/c \"net stop \"{0}\" & net start \"{0}\"\"", Name),
                    LoadUserProfile = false,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden
                };
                process.StartInfo = startInfo;
                process.Start();

                Trace.TraceInformation("Restart of Monitoring service triggered.");
            }
            else
            {
                // Do soft restart
                Stop();
                Start();

                Trace.TraceInformation("Soft restart of Monitoring service performed.");
            }
        }

        protected override void OnStart(string[] args)
        {
            var candidateTypes = Assembly.GetExecutingAssembly().GetTypes();
            Init();

            Scheduler.Start();
            RunAllConfiguredJobs(candidateTypes);
            IsServiceRunning = true;

            base.OnStart(args);
        }

        protected override void OnStop()
        {
            // Allow enough time for service to shutdown
            var maxAdditionalTime = ExecutingJobsStopWaitTimeout.TotalMilliseconds + TimeSpan.FromSeconds(20).TotalMilliseconds;
            RequestAdditionalTime(Convert.ToInt32(maxAdditionalTime));

            Stop();
            base.OnStop();
        }

        public static void RunOnce()
        {
            var candidateTypes = Assembly.GetExecutingAssembly().GetTypes();
            Init(true);

            Scheduler.Start();
            var scheduledJobsCount = RunAllJobsNowAndOnce(candidateTypes);
            IsServiceRunning = true;

            while ((JobListener.JobsExecutedCount < scheduledJobsCount)
                && !Scheduler.IsShutdown)
            {
                Thread.Sleep(2000);
            }

            Stop();
        }

        private static void Init(bool runOnce = false)
        {
            var registry = ServiceProvider.GetService<ITenantRegistry>();
            if (GetSchedules(typeof (TenantJobsActivationMonitor)).Any() && !runOnce) return;

            VerboseLog.TraceInformation("Forcing activation of all tenants: running once or TenantJobsActivationMonitor schedule is not present");
            // Activate all tenants
            registry.ConfiguredTenants.Select(t => t.Key).ToList().ForEach(tenantKey => registry.SetTenantActivationLockState(tenantKey, TenantActivationLockState.Active));
        }

// ReSharper disable once UnusedMethodReturnValue.Local
        private static int RunAllConfiguredJobs(IEnumerable<Type> candidateTypes)
        {
            var jobCount = 0;
            foreach (var jt in candidateTypes.Where(t => t.Is<IJob>() && t.IsClass && !t.IsAbstract))
            {
                var jobType = jt;
                var schedules = GetSchedules(jobType);

                foreach (var schedule in schedules)
                {

                    var localSchedule = schedule;
                    var job = JobBuilder.Create(jobType).WithIdentity(new JobKey(localSchedule.Name)).Build();

                    VerboseLog.TraceInformation("Scheduling job {0}", schedule.ToString());

                    var triggerBuilder = TriggerBuilder.Create();
                    ITrigger trigger = null;

                    var doScheduleJob = new Action(() =>
                    {
// ReSharper disable AccessToModifiedClosure
                        Scheduler.ScheduleJob(job, trigger);
                        jobCount++;
// ReSharper restore AccessToModifiedClosure
                    });

                    // Run by daily schedule
                    if (localSchedule.StartTimeOfDay != null)
                    {
                        trigger = triggerBuilder.WithDailyTimeIntervalSchedule(s =>
                        {
                            // Treat time of day as Utc
                            s.InTimeZone(TimeZoneInfo.Utc);

                            // Every day at start time
                            s.StartingDailyAt(localSchedule.StartTimeOfDay).OnEveryDay();

                            // Execute either 1 per 24 hours or per specified interval
                            if (localSchedule.Interval != null)
                            {
                                s.WithIntervalInSeconds(
                                    Convert.ToInt32(localSchedule.Interval.Value.TotalSeconds));
                            }
                            else
                            {
                                s.WithIntervalInHours(24);
                            }
                            
                            if (localSchedule.DaysOfWeek != null)
                            {
                                s.OnDaysOfTheWeek(localSchedule.DaysOfWeek);
                            }

                            // Set when to end execution if provided
                            if (localSchedule.EndTimeOfDay != null)
                            {
                                s.EndingDailyAt(localSchedule.EndTimeOfDay);
                            }
                        }).Build();

                        doScheduleJob();
                    }
                    // Run all time by interval
                    else if (localSchedule.Interval != null)
                    {
                        trigger = triggerBuilder
                            .WithSimpleSchedule(x => x
                                .WithInterval(localSchedule.Interval.Value)
                                .RepeatForever()
                                .WithMisfireHandlingInstructionIgnoreMisfires())
                            .Build();

                        doScheduleJob();
                    }
                }
            }

            return jobCount;
        }

        private static int RunAllJobsNowAndOnce(IEnumerable<Type> candidateTypes)
        {
            int jobCount = 0;
            foreach (var jt in candidateTypes.Where(t => t.Is<IJob>() && t.IsClass && !t.IsAbstract))
            {
                var jobType = jt;

                var schedules = ConfigurationSection<MonitoringServiceConfiguration>.Current.Schedules.Where(s => s.TypeName.Equals(jobType.Name, StringComparison.InvariantCultureIgnoreCase));
                foreach (var schedule in schedules)
                {
                    var localSchedule = schedule;
                    var job = JobBuilder.Create(jobType).WithIdentity(new JobKey(localSchedule.Name)).Build();
                    var triggerBuilder = TriggerBuilder.Create();
                    var trigger = triggerBuilder
                        .WithSimpleSchedule(x => x
                            .WithIntervalInMinutes(0)
                            .WithRepeatCount(0)
                            .WithMisfireHandlingInstructionIgnoreMisfires())
                        .Build();


                    jobCount++;
                    Scheduler.ScheduleJob(job, trigger);
                }
            }
            return jobCount;
        }

        private static IEnumerable<MonitoringServiceSchedule> GetSchedules(Type t)
        {
            return ConfigurationSection<MonitoringServiceConfiguration>.Current.Schedules.Where(s => s.TypeName.Equals(t.Name, StringComparison.InvariantCultureIgnoreCase));
        }
    }

    [RunInstaller(true)]
    public class MonitoringServiceInstaller : Installer
    {
        public MonitoringServiceInstaller()
        {
            var serviceProcessInstaller = new ServiceProcessInstaller { Account = ServiceAccount.LocalSystem };

            var serviceAdmin = new ServiceInstaller { StartType = ServiceStartMode.Automatic, ServiceName = MonitoringService.Name, DisplayName = MonitoringService.Name };

            Installers.Add(serviceProcessInstaller);
            Installers.Add(serviceAdmin);
        }
    }
}