﻿using System.Xml.Serialization;

namespace IO.Practiceware.Monitoring.Service.NewCropMonitor
{
    [XmlRoot("newCropMonitorConfiguration")]
    public class NewCropMonitorConfiguration
    {
        [XmlAttribute("endpointAddress")]
        public string EndpointAddress { get; set; }

        [XmlAttribute("partnerName")]
        public string PartnerName { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("password")]
        public string Password { get; set; }

        [XmlAttribute("accountId")]
        public string AccountId { get; set; }

        [XmlAttribute("siteId")]
        public string SiteId { get; set; }

        [XmlAttribute("ftpPath")]
        public string FtpPath { get; set; }

        [XmlAttribute("ftpUserName")]
        public string FtpUserName { get; set; }

        [XmlAttribute("ftpPassword")]
        public string FtpPassword { get; set; }
    }
}