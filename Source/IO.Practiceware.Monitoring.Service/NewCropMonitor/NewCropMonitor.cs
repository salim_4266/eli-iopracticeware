﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.ServiceModel;
using IO.Practiceware.Monitoring.Service.Common;
using IO.Practiceware.Monitoring.Service.NewCropUpdate1Service;
using IO.Practiceware.Monitoring.Service.UpdateMonitor;
using IO.Practiceware.Storage;
using Quartz;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;

namespace IO.Practiceware.Monitoring.Service.NewCropMonitor
{
    /// <summary>
    /// Downloads latest mappings from NewCrop and uploads to Ftp for later processing by <see cref="UpdateMonitor"/>
    /// </summary>
    [DisallowConcurrentExecution]
    internal class NewCropMonitor : IJob
    {
        private readonly ITenantRegistry _tenantRegistry;
        private readonly FtpClient _ftpClient;

        public NewCropMonitor(ITenantRegistry tenantRegistry, FtpClient ftpClient)
        {
            _tenantRegistry = tenantRegistry;
            _ftpClient = ftpClient;
        }

        public void Execute(IJobExecutionContext context)
        {
            lock (UpdateMonitor.UpdateMonitor.SyncRoot)
            {
                try
                {
                    // New crop monitor runs only on instance which processes primary tenant
                    if (!_tenantRegistry.IsTenantActive(_tenantRegistry.PrimaryTenantKey)) return;

                    var configuration = context.GetConfiguration<NewCropMonitorConfiguration>();

                    new[] {new
                    {
                        Id = 1, FileName = "fdb.zip"
                    }, new
                    {
                        Id = 13, FileName = "fdb_rxnorm_mapping.zip"
                    }}
                    .TryForEach(x => Run(configuration, x.Id, x.FileName));
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.ToString());
                    throw new JobExecutionException(ex);
                }
            }
        }

        private void Run(NewCropMonitorConfiguration configuration, int desiredData, string fileName)
        {
            string zipFileName;
            if (DownloadFromNewCrop(configuration, desiredData, out zipFileName))
            {
                UploadToFtp(configuration, zipFileName, fileName);
            }
        }

        private static bool DownloadFromNewCrop(NewCropMonitorConfiguration configuration, int desiredData, out string filePath)
        {
            filePath = null;

            var soapClient = new Update1SoapClient(new BasicHttpBinding(new Uri(configuration.EndpointAddress).Scheme == Uri.UriSchemeHttps ? BasicHttpSecurityMode.Transport : BasicHttpSecurityMode.None), new EndpointAddress(configuration.EndpointAddress));

            var accountCredentials = new Credentials { PartnerName = configuration.PartnerName, Name = configuration.Name, Password = configuration.Password };
            var accountRequest = new AccountRequest { AccountId = configuration.AccountId, SiteId = configuration.SiteId };

            var url = soapClient.GetMostRecentDownloadUrl(accountCredentials, accountRequest, desiredData);

            if (url.downloadDetail == null || url.downloadDetail.LatestDownloadUrl.IsNullOrEmpty())
            {
                Trace.TraceWarning("Could not get download url for NewCrop data {0}:\r\n{1}", desiredData, url.ToXml());
                return false;
            }

            filePath = Path.Combine(FileSystem.GetTempPath(), "NewCrop", desiredData.ToString(), Path.GetFileName(url.downloadDetail.LatestDownloadUrl) ?? string.Empty);
            FileSystem.EnsureFilePath(filePath);

            int size;
            if (File.Exists(filePath) && int.TryParse(url.downloadDetail.LatestDownloadSize.Replace(",", string.Empty), out size)
                && size == new FileInfo(filePath).Length)
            {
                Trace.TraceInformation("No new updates to NewCrop data {0}. Zip file already exists at {1}.", desiredData, filePath);
                return false;
            }

            Trace.TraceInformation("Downloading new NewCrop data for {0} from url {1}.", desiredData, url.downloadDetail.LatestDownloadUrl);

            if (File.Exists(filePath))
            {
                FileSystem.TryDeleteFile(filePath);
            }

            var webClient = new WebClient();
            webClient.DownloadFile(url.downloadDetail.LatestDownloadUrl, filePath);
            return true;
        }

        private void UploadToFtp(NewCropMonitorConfiguration configuration, string zipFileName, string destinationFileName)
        {
            var uriBuilder = new UriBuilder(configuration.FtpPath);
            uriBuilder.UserName = Uri.EscapeDataString(configuration.FtpUserName);
            uriBuilder.Password = Uri.EscapeDataString(configuration.FtpPassword);

            _ftpClient.UploadFile(zipFileName, uriBuilder.Uri.Combine(destinationFileName));

            Trace.TraceInformation("Uploaded {0}.".FormatWith(destinationFileName));
        }
    }
}