﻿using System;
using System.Runtime.Serialization;
using IO.Practiceware.Application;

namespace IO.Practiceware.Monitoring.Service.DatabaseBackupMonitor
{
    [DataContract]
    public class DatabaseBackupMonitorConfiguration
    {
        private string _backupDirectory;
        [DataMember]
        public string BackupDirectory
        {
            get { return _backupDirectory; }
            set { _backupDirectory = value != null ? value.ToLower().Replace(@"%clientname%", ApplicationContext.Current.ClientName) : null; }
        }

        private string _backupFileName;
        [DataMember]
        public string BackupFileName
        {
            get { return _backupFileName; }
            set
            {
                _backupFileName = value;
                if (String.IsNullOrEmpty(value))
                {
                    return;
                }
                _backupFileName = _backupFileName.ToLower().Replace(@"%clientname%", ApplicationContext.Current.ClientName);
                _backupFileName = !_backupFileName.EndsWith(".bak") && _backupFileName != string.Empty ? _backupFileName + @".bak" : _backupFileName;
            }
        }

        [DataMember]
        public bool CompressDatabase { get; set; }
    }
}