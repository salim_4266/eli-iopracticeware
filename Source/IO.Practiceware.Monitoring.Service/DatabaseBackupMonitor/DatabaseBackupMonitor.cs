﻿using System;
using System.Data.SqlClient;
using IO.Practiceware.Data;
using IO.Practiceware.Monitoring.Service.Common;
using IO.Practiceware.Services.ApplicationSettings;
using Quartz;

namespace IO.Practiceware.Monitoring.Service.DatabaseBackupMonitor
{
    [DisallowConcurrentExecution]
    internal class DatabaseBackupMonitor : TenantJob
    {
        private const string DatabaseBackupMonitorConfigurationSettingKey = "DatabaseBackupMonitorConfiguration";

        private readonly IApplicationSettingsService _settingsService;

        public DatabaseBackupMonitor(IApplicationSettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        protected override void Execute(IJobExecutionContext context)
        {
            var configuration = _settingsService.GetOrCreateSetting<DatabaseBackupMonitorConfiguration>(DatabaseBackupMonitorConfigurationSettingKey);

            if (configuration == null || configuration.Value == null || String.IsNullOrEmpty(configuration.Value.BackupDirectory)) return;

            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                MonitoringService.VerboseLog.TraceInformation(@"Starting Database Backup Monitor on {0} backing up: {1} to: {2}"
                    , new SqlConnectionStringBuilder(connection.ConnectionString).DataSource
                    , connection.Database
                    , configuration.Value.BackupDirectory);
                DatabaseBackupUtilities.BackupDatabase(connection, configuration.Value.BackupDirectory, configuration.Value.BackupFileName,
                    configuration.Value.CompressDatabase);
            }
        }
        //--INSERT INTO model.ApplicationSettings (Name, ApplicationSettingTypeId, Value)
        //--VALUES ('DatabaseBackupMonitorConfiguration', 11,
        //--'<z:anyType xmlns:i="http://www.w3.org/2001/XMLSchema-instance" z:Id="1" xmlns:d1p1="http://schemas.datacontract.org/2004/07/IO.Practiceware.Monitoring.Service.DatabaseBackupMonitor" i:type="d1p1:DatabaseBackupMonitorConfiguration" xmlns:z="http://schemas.microsoft.com/2003/10/Serialization/">
        //--  <d1p1:BackupDirectory>...</d1p1:BackupDirectory>
        //--  <d1p1:BackupFileName>..</d1p1:BackupFileName>
        //--  <d1p1:CompressDatabase>true</d1p1:CompressDatabase> 
        //--</z:anyType>')
    }
}
