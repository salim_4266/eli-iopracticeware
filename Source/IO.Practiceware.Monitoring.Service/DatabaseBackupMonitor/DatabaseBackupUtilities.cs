﻿using System;
using System.Data;
using System.IO;
using Soaf;
using Soaf.Data;
using System.Text;

namespace IO.Practiceware.Monitoring.Service.DatabaseBackupMonitor
{

    internal class DatabaseBackupUtilities
    {
        /// <summary>
        /// Backups the database.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="backupDirectory">The backup directory.</param>
        /// <param name="backupFileName">Name of the backup file.</param>
        /// <param name="compressDatabase">if set to <c>true</c> [compress database].</param>
        public static void BackupDatabase(IDbConnection connection, string backupDirectory, string backupFileName, bool compressDatabase)
        {
            if (!Directory.Exists(backupDirectory)) Directory.CreateDirectory(backupDirectory);
            var autoNameBackup = string.IsNullOrEmpty(backupFileName);
            if (autoNameBackup) backupFileName = connection.Database + @"_" + DateTime.Now.ToString("yyyy.MM.dd_HH.mm.ss") + @".bak";

            var backupDatabaseCommand = String.Format("BACKUP DATABASE [{0}] TO DISK ='{1}' WITH INIT", connection.Database, Path.Combine(backupDirectory, backupFileName));
            if (compressDatabase) backupDatabaseCommand = backupDatabaseCommand + @", COMPRESSION;";

            MonitoringService.VerboseLog.TraceInformation("Running command {0} against connection string {1}.".FormatWith(backupDatabaseCommand, connection.ConnectionString));
            connection.Execute(backupDatabaseCommand);
        }



        /// <summary>
        /// Gets the Sql server version.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        public static string GetSqlServerVersion(IDbConnection connection)
        {
            const string sqlVersionQuery = @"DECLARE @ver nvarchar(128)
                                    SET @ver = CAST(serverproperty('ProductVersion') AS nvarchar)
                                    SET @ver = SUBSTRING(@ver, 1, CHARINDEX('.', @ver) - 1)
                                    IF ( @ver = '7' )
                                       SELECT 'SQL Server 7'
                                    ELSE IF ( @ver = '8' )
                                       SELECT 'SQL Server 2000'
                                    ELSE IF ( @ver = '9' )
                                       SELECT 'SQL Server 2005'
                                    ELSE IF ( @ver = '10' )
                                       SELECT 'SQL Server 2008/2008 R2'
                                    ELSE IF ( @ver = '11' )
                                       SELECT 'SQL Server 2012'
                                    ELSE
                                       SELECT 'Unsupported SQL Server Version'";

            MonitoringService.VerboseLog.TraceInformation("Running command {0} against connection string {1}.".FormatWith(sqlVersionQuery, connection.ConnectionString));
            string version = connection.Execute<string>(sqlVersionQuery);
            return version;
        }


        /// <summary>
        /// Gets the default back up directory of Sql Server.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        public static string GetDefaultBackUpDirectory(IDbConnection connection)
        {
            const string backupDirQuery = @"DECLARE @BackupDirectory NVARCHAR(1000)   
                                     EXEC master..xp_instance_regread @rootkey = 'HKEY_LOCAL_MACHINE',  
                                    @key = 'Software\Microsoft\MSSQLServer\MSSQLServer',  
                                    @value_name = 'BackupDirectory', @BackupDirectory = @BackupDirectory OUTPUT ;  

                                   SELECT @BackupDirectory AS [SQLServerDefaultBackupPath]";

            MonitoringService.VerboseLog.TraceInformation("Running command {0} against connection string {1}.".FormatWith(backupDirQuery, connection.ConnectionString));
            string backUpDir = connection.Execute<string>(backupDirQuery);
            return backUpDir;
        }



        /// <summary>
        /// Restores the database.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="backupNameWithPath">The backup name with path.</param>
        /// <param name="newDbName">New name of the database.</param>
        /// <param name="newMoveToFilesPrefix">The new move to files prefix.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Not enough space to restore the database</exception>
        public static string RestoreDatabase(IDbConnection connection, string backupNameWithPath, string newDbName, string newMoveToFilesPrefix)
        {
            StringBuilder moveToText = new StringBuilder();

            const string diskSizeCommand = @"SELECT DISTINCT 
                                            CONVERT(INT,dovs.available_bytes/1048576.0) AS FreeSpaceInMB
                                            FROM sys.master_files mf
                                            CROSS APPLY sys.dm_os_volume_stats(mf.database_id, mf.FILE_ID) dovs
                                            ORDER BY FreeSpaceInMB ASC";

            MonitoringService.VerboseLog.TraceInformation("Running command {0} against connection string {1}.".FormatWith(diskSizeCommand, connection.ConnectionString));
            decimal disksize = connection.Execute<decimal>(diskSizeCommand);

            const string backupSizeCommand = @" SELECT SUM(CAST(size/128.0 AS DECIMAL(10,2))) AS [Size in MB] FROM sysfiles";
            MonitoringService.VerboseLog.TraceInformation("Running command {0} against connection string {1}.".FormatWith(backupSizeCommand, connection.ConnectionString));
            decimal backupsize = connection.Execute<decimal>(backupSizeCommand);

            if (backupsize > disksize)
                throw new Exception("Not enough space to restore the database");


            var moveToCommand = String.Format(@"SELECT name,physical_name FROM sys.[master_files] WHERE [database_id] IN (DB_ID('{0}')) 
                                                    ORDER BY [type], DB_NAME([database_id])", connection.Database);

            MonitoringService.VerboseLog.TraceInformation("Running command {0} against connection string {1}.".FormatWith(moveToCommand, connection.ConnectionString));
            DataTable dt = connection.Execute<DataTable>(moveToCommand);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    moveToText.Append(@" ,MOVE '" + Convert.ToString(dr["name"]) + "' TO '" + Convert.ToString(dr["physical_name"]).Insert(Convert.ToString(dr["physical_name"]).LastIndexOf('.'), newMoveToFilesPrefix) + "'");
                }
            }

            var restoreDatabaseCommand = String.Format(@"RESTORE DATABASE {0} FROM DISK = '{1}' WITH FILE = 1 "
                                                         + moveToText + ", REPLACE, RECOVERY;"
                                         , newDbName, backupNameWithPath);

            MonitoringService.VerboseLog.TraceInformation("Running command {0} against connection string {1}.".FormatWith(restoreDatabaseCommand, connection.ConnectionString));
            connection.Execute(restoreDatabaseCommand);
            return newDbName;

        }


        /// <summary>
        /// Drops the database.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="dbName">Name of the database.</param>
        public static void DropDatabase(IDbConnection connection, string dbName)
        {
            var dropScript = @"DROP DATABASE [{0}]".FormatWith(dbName);
            MonitoringService.VerboseLog.TraceInformation("Running command {0} against connection string {1}.".FormatWith(dropScript, connection.ConnectionString));
            connection.Execute(dropScript);
        }

        /// <summary>
        /// Deletes the backup file.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="fileName">Name of the file.</param>
        public static void DeleteBackupFiles(IDbConnection connection, string fileName)
        {
            var deleteFileScript = @"Execute master.dbo.xp_delete_file 0, N'{0}', N'bak', getdate(), 0".FormatWith(fileName);
            MonitoringService.VerboseLog.TraceInformation("Running command {0} against connection string {1}.".FormatWith(deleteFileScript, connection.ConnectionString));
            connection.Execute(deleteFileScript);
        }
    }


}