using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Monitoring.Service.Common;
using Quartz;
using Soaf;
using Soaf.Configuration;
using Soaf.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Transactions;

namespace IO.Practiceware.Monitoring.Service.ReplicationMonitor
{
    [DisallowConcurrentExecution]
    public class ReplicationMonitor : TenantJob
    {
        #region "Properties"

        //<summary>
        //Get max percentage for seed range email subject
        //</summary>

        public static string SeedPercentage
        {
            get { return ConfigurationManager.CustomAppSettings["ReplicationMonitorSeedPercentageWarningThreshold"].EnsureNotDefault("The seed percentage threshold is not set"); }
        }

        #endregion

        protected override void Execute(IJobExecutionContext context)
        {
            ExecuteInternal();
        }

        private void ExecuteInternal()
        {
            var distributionCheckConnectionString = new SqlConnectionStringBuilder
                {
                    ConnectionString = ConfigurationManager.PracticeRepositoryConnectionString
                };

            const string distributionDatabaseExistenceCheckQuery = @"SELECT COUNT(*) 
FROM master.dbo.sysdatabases
WHERE NAME = 'distribution'";

            bool distributionDatabaseExists;

            using (var distributionCheckConnection = new SqlConnection(distributionCheckConnectionString.ToString()))
            {
                distributionCheckConnection.Open();

                using (var sqlCmd = new SqlCommand(distributionDatabaseExistenceCheckQuery, distributionCheckConnection))
                {
                    var result = (int)sqlCmd.ExecuteScalar();

                    distributionDatabaseExists = result > 0;
                }

                distributionCheckConnection.Close();
            }


            if (!distributionDatabaseExists) return;
            using (var ts = new TransactionScope(TransactionScopeOption.Suppress))
            using (var connection = new SqlConnection(ConfigurationManager.PracticeRepositoryConnectionString))
            using (var notificationEmail = new MailMessage())
            {
                try
                {
                    connection.Open();
                    var replicationCheck = new DataSet();
                    var dataAdapter = new SqlDataAdapter("exec sp_replmonitorhelppublisher", connection);
                    dataAdapter.Fill(replicationCheck);

                    foreach (DataRow row in replicationCheck.Tables[0].Rows)
                    {
                        Check(row, connection, notificationEmail);
                    }

                    Trace.TraceInformation("Replication check completed for {" + ApplicationContext.Current.ClientName + "} on {" + DateTime.Now + "}");
                }

                catch (Exception ex)
                {
                    notificationEmail.Subject = "ATTENTION: Cannot connect to SQL server at {" + ApplicationContext.Current.ClientName + "} on {" + DateTime.Now + "}";
                    notificationEmail.Body = ex.Message;
                    Trace.TraceError(ex.ToString());
                }
                finally
                {
                    if (notificationEmail.Body != string.Empty) SendEmail(notificationEmail);
                }

                ts.Complete();
            }
        }

        private void Check(DataRow row, IDbConnection connection, MailMessage notificationEmail)
        {
            Trace.TraceInformation("Replication check started at {" + ApplicationContext.Current.ClientName + "} on {" + DateTime.Now + "} for server " + row["publisher"]);

            string subject = null, body = null;

            try
            {
            var publisher = row["publisher"].ToString();

            var hasError = CheckForErrorLogs(connection, publisher, out subject, out body);

            if (hasError)
            {
                notificationEmail.Subject = subject;
                notificationEmail.Body = body;
                return;
            }

            hasError = CheckSeedRange(connection, out subject, out body);
            notificationEmail.Body += Environment.NewLine + body;
            if (hasError)
            {
                notificationEmail.Subject = subject;
                return;
            }

            CheckStatus(row, out subject, out body);

            notificationEmail.Subject = subject;
            notificationEmail.Body += Environment.NewLine + Environment.NewLine + body;

            Trace.TraceInformation("The server: {" + publisher + "} completed a replication check on {" + DateTime.Now + "}");
        }
            catch (Exception ex)
            {
                throw new Exception("Error sending email with subject {0} and body {1}".FormatWith(subject, body), ex);
            }
        }

        private void CheckStatus(DataRow row, out string subject, out string body)
        {
            var status = row["status"].ToString();
            var warning = row["warning"].ToString();
            var publisher = row["publisher"].ToString();

            if (((status == "1") || (status == "3") || (status == "4")) && warning == "0")
            {
                if (warning == "0")
                {
                    subject = "Replication and seed range verified at: " + ApplicationContext.Current.ClientName + " on " + DateTime.Now + " ";

                    body = "The server: {" + publisher + "} is replicating without warnings with the status of { " + StatusDescription(status) + "} ";

                    return;
                }
                subject = "ATTENTION: Check replication at {" + ApplicationContext.Current.ClientName + "}";

                body = "The server: {" + publisher + "} is replicating with warnings with the status of { " + StatusDescription(status) + "} and warnings of {" + WarningDescription(warning) + "}";

                return;
            }

            subject = "ATTENTION: Check replication at {" + ApplicationContext.Current.ClientName + "}";

            if (warning != "0")
            {
                body = "The server: {" + publisher + "} " + Environment.NewLine + "Has errors of:{" + WarningDescription(warning) + "}";
            }
            else
            {
                body = "The server: {" + publisher + "} " + Environment.NewLine + "Has errors of:{" + StatusDescription(status) + "}";
            }
        }

        // <summary>
        // SeedRange Calculation 
        // </summary>
        // <param name="connection"></param>
        // <param name="SqlCommand"></param>
        public bool CheckSeedRange(IDbConnection connection, out string subject, out string body)
        {
            subject = null;
            body = null;

            DataTable seedCheck;
            try
            {
                const string mainQuery = @"SELECT ('[' + [TableList].[TABLE_SCHEMA] + ']' + '.' + '[' +  [TableList].[TABLE_NAME] + ']') AS TableName,[PrimaryKey].[COLUMN_NAME] AS PrimaryKey 
FROM INFORMATION_SCHEMA.TABLES TableList
INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE PrimaryKey ON [TableList].[TABLE_NAME] = [PrimaryKey].[TABLE_NAME] AND [PrimaryKey].[TABLE_SCHEMA] = [TableList].[TABLE_SCHEMA]
WHERE OBJECTPROPERTY(OBJECT_ID('[' + [PrimaryKey].[TABLE_SCHEMA] + ']' + '.' + '[' + [PrimaryKey].[TABLE_NAME] + ']'), 'TableHasIdentity') = 1 
	AND TABLE_TYPE ='BASE TABLE' AND IDENT_CURRENT('[' + [PrimaryKey].[TABLE_SCHEMA] + ']' + '.' + '[' + [PrimaryKey].[TABLE_NAME] + ']') > 1
	AND CONSTRAINT_NAME like 'PK%'";

                seedCheck = connection.Execute<DataTable>(mainQuery);
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                return true;
            }

            subject = "ATTENTION: check seed range at {" + ApplicationContext.Current.ClientName + "}" + Environment.NewLine;

            for (var i = 0; i <= seedCheck.Rows.Count - 1; i++)
            {
                var tableName = seedCheck.Rows[i]["TableName"].ToString();
                var primaryKey = seedCheck.Rows[i]["PrimaryKey"].ToString();

                if (tableName == string.Empty || primaryKey == string.Empty) continue;

                    var subquery = " SELECT TOP 1 " + primaryKey + " AS SeedMax, IDENT_CURRENT('" + tableName + "') AS CurrentIdentity ";
                    subquery += " FROM " + tableName + " WHERE " + primaryKey + " > IDENT_CURRENT('" + tableName + "') ORDER BY " + primaryKey + " ";

                    var seedCheckRange = new DataSet();
                    var subqueryDataAdapter = new SqlDataAdapter(subquery, connection as SqlConnection);
                    subqueryDataAdapter.Fill(seedCheckRange);

                    if (seedCheckRange.Tables[0].Rows.Count > 0)
                    {
                    Trace.TraceInformation("Replication seedrange check process started on {" + tableName + "} ");

                        var seedMax = Convert.ToDouble(seedCheckRange.Tables[0].Rows[0]["SeedMax"]);
                        var currentIdentity = Convert.ToDouble(seedCheckRange.Tables[0].Rows[0]["CurrentIdentity"]);
                        var seedPercentageInt = Convert.ToInt32(SeedPercentage);
                        if (Convert.ToInt32(seedMax) == 0)
                        {
                            seedMax = int.MaxValue;
                        }

                        var percentage = (currentIdentity) / (seedMax);

                        percentage = Math.Round(percentage * 100, 2);

                    Trace.TraceInformation("Replication seedrange check completed for:     " + tableName + " ");

                        if (percentage > seedPercentageInt)
                        {
                        subject += tableName;
                            body += "TABLE:     " + tableName.PadRight(60) + "CURRENT IDENTITY:     " + currentIdentity.ToString().PadRight(20) + " PERCENT: " + percentage.ToString().PadRight(20) + "%**********" + Environment.NewLine;
                            return true;
                        }
                        if (percentage < seedPercentageInt)
                        {
                            body += " Table:     " + tableName.PadRight(60) + "Current identity:     " + currentIdentity.ToString().PadRight(20) + "Percent: " + percentage.ToString().PadRight(20) + " %" + Environment.NewLine;
                        }
                    }

            }
            return false;
        }

        // <summary>
        // Assigns status Description 
        // </summary>
        // <param name="status"></param>
        private static string StatusDescription(string status)
        {
            var description = string.Empty;

            switch (status)
            {
                case "1":
                    description = "Started";
                    break;
                case "2":
                    description = "SQL Server Agent is Stopped";
                    break;
                case "3":
                    description = "In progress";
                    break;
                case "4":
                    description = "Idle";
                    break;
                case "5":
                    description = "Retrying";
                    break;
                case "6":
                    description = "Failed";
                    break;
            }
            return description;
        }

        // <summary>
        // Assigns Warning Description 
        // </summary>
        // <param name="status"></param>
        private static string WarningDescription(string warning)
        {
            var description = string.Empty;

            switch (warning)
            {
                case "1":
                    description =
                        "expiration � a subscription to a transactional publication has not been synchronized within the retention period threshold";
                    break;
                case "2":
                    description =
                        "latency - the time taken to replicate data from a transactional Publisher to the Subscriber exceeds the threshold, in seconds.";
                    break;
                case "4":
                    description =
                        "mergeexpiration - a subscription to a merge publication has not been synchronized within the retention period threshold.";
                    break;
                case "8":
                    description =
                        "mergefastrunduration - the time taken to complete synchronization of a merge subscription exceeds the threshold, in seconds, over a fast network connection.";
                    break;
                case "16":
                    description =
                        "mergeslowrunduration - the time taken to complete synchronization of a merge subscription exceeds the threshold, in seconds, over a slow or dial-up network connection.";
                    break;
                case "32":
                    description =
                        "mergefastrunspeed � the delivery rate for rows during synchronization of a merge subscription has failed to maintain the threshold rate, in rows per second, over a fast network connection.";
                    break;
                case "34":
                    description =
                        "mergeslowrunspeed � the delivery rate for rows during synchronization of a merge subscription has failed to maintain the threshold rate, in rows per second, over a slow or dial-up network connection.";
                    break;
            }
            return description;
        }

        // <summary>
        // Assigns Email subject and Body
        // </summary>
        // <param name="subject"></param>
        // <param name="message"></param>
        public static void SendEmail(MailMessage notificationEmail)
        {
            var emailAddresses = ConfigurationSection<MonitoringServiceConfiguration>.Current.AlertsDestinationEmailAddresses.Split(';');

            foreach (var emailAddress in emailAddresses)
            {
                notificationEmail.To.Add(emailAddress);
            }


            Send(notificationEmail);
        }

        // <summary>
        // Send a Message
        // </summary>
        // <param name="MailMessage"></param>
        public static void Send(MailMessage mailMessage)
        {
            try
            {
                var smtpClient = new SmtpClient();

                smtpClient.Send(mailMessage);
                Trace.TraceInformation("Email sent successfully to {" + mailMessage.To + "} ");
            }

            catch (Exception ex)
            {
                Trace.TraceError("Email failed to send to {" + mailMessage.To + "} " + ex);
            }
        }

        private bool CheckForErrorLogs(IDbConnection connection, string publisher, out string subject, out string body)
        {
            body = null;
            subject = null;
            try
            {
                var scheduleMinutes = ConfigurationSection<MonitoringServiceConfiguration>.Current.Schedules
                    .Where(s => s.Name == "ReplicationMonitor")
                    .Select(i => i.Interval)
                    .FirstOrDefault();

                if (scheduleMinutes == null || scheduleMinutes.Value.TotalMinutes < 1)
                {
                    scheduleMinutes = TimeSpan.FromMinutes(1);
                }

                var errorMessage = string.Empty;

                const string checkErrorLogQuery = @"DECLARE @publisher AS NVARCHAR (MAX);
                                                    SET @publisher = '{0}'; 
                                                    SELECT message,a.run_date as Date,RIGHT('00000' + CONVERT (VARCHAR, a.run_time), 6) AS Time 
                                                    FROM msdb..sysjobhistory AS a INNER JOIN msdb..sysjobs AS b ON a.job_id = b.job_id   
                                                    WHERE b.name LIKE @publisher + '%' AND run_status <> 1 AND message LIKE '%error%'  
                                                    AND CONVERT(DATETIME, CONVERT(CHAR(8), run_date, 112) + ' '+ 
                                                    STUFF(STUFF(RIGHT('000000' + CONVERT(VARCHAR(8), run_time), 6), 5, 0, ':'), 3, 0, ':'), 121) 
                                                    >DATEADD(mi, -{1}, GETDATE())";

                var errorLogs = connection.Execute<DataTable>(string.Format(checkErrorLogQuery, publisher, scheduleMinutes.Value.TotalMinutes));

                for (var i = 0; i <= errorLogs.Rows.Count - 1; i++)
                {
                    errorMessage += "Publisher:     " + publisher + " " + Environment.NewLine +
                                    "Date:   " + errorLogs.Rows[i]["Date"] + Environment.NewLine +
                                    "Time:   " + errorLogs.Rows[i]["Time"] + Environment.NewLine +
                                    "Error Message:    " + errorLogs.Rows[i]["message"] + Environment.NewLine;
                }

                if (errorMessage != string.Empty)
                {
                    subject = "ATTENTION: check for replication errors at {" + ApplicationContext.Current.ClientName + "}";
                    body = "Error Message(s):        " + Environment.NewLine + errorMessage;
                    return true;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                return true;
            }
            return false;
        }
    }
}