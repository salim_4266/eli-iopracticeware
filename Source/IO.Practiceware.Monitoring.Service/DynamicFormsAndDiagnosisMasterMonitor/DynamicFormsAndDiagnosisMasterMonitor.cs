﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using IO.Practiceware.Configuration;
using IO.Practiceware.Monitoring.Service.Common;
using Quartz;

namespace IO.Practiceware.Monitoring.Service.DynamicFormsAndDiagnosisMasterMonitor
{
    [DisallowConcurrentExecution]
    internal class DynamicFormsAndDiagnosisMasterMonitor : TenantJob
    {
        protected override void Execute(IJobExecutionContext context)
        {
            var dynamicFormsAndDiagnosisMasterFolder = String.Format(@"{0}\SoftwareUpdates\Client\Pinpoint", ConfigurationManager.ServerDataPath);

            // Skip when NewVersion directory doesn't exist
            if (!Directory.Exists(dynamicFormsAndDiagnosisMasterFolder))
            {
                return;
            }

            MdbUtilities.ExcludedDbTableNames = new[] { "Paste Errors" };
            //look in the server data path NewVersion folder for a file with Name "DynamicForms.mdb".
            var dynamicForms = Directory.GetFiles(dynamicFormsAndDiagnosisMasterFolder, "DynamicForms.mdb", SearchOption.AllDirectories).FirstOrDefault();
            if (dynamicForms != null)
                MdbUtilities.Import(dynamicForms, "df");
            //look in the server data path NewVersion folder for a file with Name "DiagnosisMaster.mdb".
            var diagnosisMaster = Directory.GetFiles(dynamicFormsAndDiagnosisMasterFolder, "DiagnosisMaster.mdb", SearchOption.AllDirectories).FirstOrDefault();
            if (diagnosisMaster != null)
                MdbUtilities.Import(diagnosisMaster, "dm");

            MapDiagnosisMasterToSnomedViaExternalSystemEntityMappings(ConfigurationManager.PracticeRepositoryConnectionString);
            MapDiagnosisMasterToIcd9ViaExternalSystemEntityMappings(ConfigurationManager.PracticeRepositoryConnectionString);
        }

        public static void MapDiagnosisMasterToIcd9ViaExternalSystemEntityMappings(string practiceRepositoryConnectionString)
        {
            using (var c = new SqlConnection(practiceRepositoryConnectionString.Replace("MultipleActiveResultSets=True", string.Empty)))
            {
                c.Open();
                using (var sqlCommand = new SqlCommand(String.Format(@"
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId,PracticeRepositoryEntityKey,ExternalSystemEntityId,ExternalSystemEntityKey) 
SELECT
(SELECT MAX(Id) FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalCondition') AS PracticeRepositoryEntityId
,CONVERT(NVARCHAR(MAX),c.DrillId) AS PracticeRepositoryEntityKey
,(SELECT MAX(es.Id) 
FROM model.ExternalSystemEntities es
JOIN model.ExternalSystems e on e.Id = es.ExternalSystemId
WHERE es.name = 'ClinicalCondition'
	AND e.Name = 'Icd9') AS ExternalSystemEntityId
,CONVERT(NVARCHAR(MAX),i.DiagnosisCode) AS ExternalSystemEntityKey
FROM dm.PrimaryDiagnosisTable p
JOIN (
	SELECT 
	d.PrimaryDrillId AS DrillId
	FROM dm.PrimaryDiagnosisTable d
	LEFT JOIN model.OrganStructures o ON d.MedicalSystem = 
		CASE
			WHEN o.Name = 'External' THEN 'A'
			WHEN o.Name = 'Pupils' THEN 'B'
			WHEN o.Name = 'Extraocular Motility' THEN 'C'
			WHEN o.Name = 'Visual Field' THEN 'D'
			WHEN o.Name = 'Lids/Lacrimal' THEN 'E'
			WHEN o.Name = 'Conjunctiva/Sclera' THEN 'F'
			WHEN o.Name = 'Cornea' THEN 'G'
			WHEN o.Name = 'Anterior Chamber' THEN 'H'
			WHEN o.Name = 'Iris' THEN 'I'
			WHEN o.Name = 'Lens' THEN 'J'
			WHEN o.Name = 'Vitreous' THEN 'K'
			WHEN o.Name = 'Optic Nerve' THEN 'L'
			WHEN o.Name = 'Blood Vessels' THEN 'M'
			WHEN o.Name = 'Macula' THEN 'N'
			WHEN o.Name = 'Retina/Choroid' THEN 'O'
			WHEN o.Name = 'Peripheral Retina' THEN 'P'
			WHEN o.Name = 'Visual Defects' THEN 'R'
			END
	WHERE dbo.IsNullOrEmpty(MedicalSystem) = 0
	
	UNION ALL
	
	SELECT 
	MAX(d.PrimaryDrillId) AS DrillId
	FROM dm.PrimaryDiagnosisTable d
	GROUP BY d.DiagnosisNextLevel
		,d.DiagnosisRank) c on c.DrillId = p.PrimaryDrillID
JOIN [IOPUBLICDATA].[ExternalData].[icd9].[ICD9] i on i.DiagnosisCode = p.DiagnosisNextLevel 
WHERE p.DiagnosisNextLevel <> '' 
	AND p.DiagnosisNextLevel IS NOT NULL
GROUP BY c.DrillId
	,i.DiagnosisCode

EXCEPT

SELECT 
PracticeRepositoryEntityId
,CONVERT(NVARCHAR(MAX),PracticeRepositoryEntityKey)
,ExternalSystemEntityId
,CONVERT(NVARCHAR(MAX),ExternalSystemEntityKey) 
FROM model.ExternalSystemEntityMappings
"), c))
                {
                    sqlCommand.CommandTimeout = c.ConnectionTimeout;
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Maps the diagnosis master to snomed via external system entity mappings.
        /// </summary>
        /// <param name="practiceRepositoryConnectionString">The practice repository connection string.</param>
        public static void MapDiagnosisMasterToSnomedViaExternalSystemEntityMappings(string practiceRepositoryConnectionString)
        {
            using (var c = new SqlConnection(practiceRepositoryConnectionString.Replace("MultipleActiveResultSets=True", string.Empty)))
            {
                c.Open();
                using (var sqlCommand = new SqlCommand(String.Format(@"
SELECT * INTO #DiagnosisMaster
FROM dm.DMDFMapping

;WITH CTE AS (
SELECT 
(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalCondition') AS PracticeRepositoryEntityId
,CONVERT(NVARCHAR(MAX),d.PrimaryDrillId) AS PracticeRepositoryEntityKey
,(SELECT Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'SnomedCt') AND Name = 'ClinicalCondition') AS ExternalSystemEntityId
,CONVERT(NVARCHAR(MAX),a.SNOMED_CID) AS ExternalSystemEntityKey
FROM dm.PrimaryDiagnosisTable d
LEFT JOIN #DiagnosisMaster a ON a.PrimaryDrillId = CONVERT(NVARCHAR(MAX),d.PrimaryDrillId)
WHERE a.SNOMED_CID <> 'NULL' 
	AND a.SNOMED_CID <> '' 
	AND a.SNOMED_CID IS NOT NULL
	AND d.MedicalSystem <> 'Q'
	AND a.GregMatch = 1
)
,CTE2 AS (
SELECT 
*
,ROW_NUMBER() OVER (PARTITION BY PracticeRepositoryEntityKey ORDER BY PracticeRepositoryEntityKey) AS Number
FROM CTE 
)
,CTE3 AS (
SELECT PracticeRepositoryEntityId,PracticeRepositoryEntityKey,ExternalSystemEntityId,ExternalSystemEntityKey
FROM CTE2 WHERE Number = 1
EXCEPT
SELECT PracticeRepositoryEntityId,PracticeRepositoryEntityKey,ExternalSystemEntityId,ExternalSystemEntityKey FROM model.ExternalSystemEntityMappings
)
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId,PracticeRepositoryEntityKey,ExternalSystemEntityId,ExternalSystemEntityKey) 
SELECT 
PracticeRepositoryEntityId
,PracticeRepositoryEntityKey
,ExternalSystemEntityId
,ExternalSystemEntityKey 
FROM CTE3
EXCEPT
SELECT 
PracticeRepositoryEntityId
,PracticeRepositoryEntityKey
,ExternalSystemEntityId
,ExternalSystemEntityKey 
FROM model.ExternalSystemEntityMappings

DROP TABLE #DiagnosisMaster"), c))
                {
                    sqlCommand.CommandTimeout = c.ConnectionTimeout;
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }
    }
}
