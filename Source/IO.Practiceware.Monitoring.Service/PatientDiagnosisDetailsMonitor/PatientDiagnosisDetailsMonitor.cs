﻿using IO.Practiceware.Integration.Cda;
using IO.Practiceware.Monitoring.Service.Common;
using Quartz;

namespace IO.Practiceware.Monitoring.Service.PatientDiagnosisDetailsMonitor
{
    [DisallowConcurrentExecution]
    internal class PatientDiagnosisDetailsMonitor : TenantJob
    {
        protected override void Execute(IJobExecutionContext context)
        {
            CdaMessageProducerUtilities.SynchronizePatientDiagnosisDetails(null,null);
        }
    }
  
}
