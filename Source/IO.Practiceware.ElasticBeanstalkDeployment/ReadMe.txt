To deploy the default parameters production, staging or dev using the command line:
- Edit references in 02run-deploy-customcommands 02-Setup: to point to the name of the environment you are using (eg. CloudStaging)
- Install AWS Tools if not installed
- Optionally delete the application for the environment you are deploying to from Elastic Beanstalk in the AWS Management Console
- Publish the project using the default profile (right click the project > Publish > Publish)
- Run Deploy_[Environment].bat (only works on a 64 bit computer) from the Deploy directory under the project directory

To deploy the Elastic Beanstalk Application and Environment from Visual Studio (DO NOT DO THIS UNLESS YOU UNDERSTAND WHAT YOU'RE DOING):
- Edit references in 02run-deploy-customcommands 02-Setup: to point to the name of the environment you are using (eg. CloudStaging)
- Install AWS Tools if not installed (See this page for the Access Key and Secret Acces Key http://www.cloudberrylab.com/blog/how-to-find-your-aws-access-key-id-and-secret-access-key-and-register-with-cloudberry-s3-explorer/)
- Delete application for the environment you are deploying to from Elastic Beanstalk in the AWS Management Console
- Right click IO.Practiceware.ElasticBeanstalkDeployment
- Publish to AWS
- Deploy new application with template
- Select Virginia as the region
- AWS Elastic Beanstalk
- Next
- Name (eg. Staging Application Server)
- Next
- Name (eg. StagingAppServer)
- Environment URL (eg. iopracticeware-staging)
- Next
- Instance type (m1.small for Dev/Staging, c1.medium for Production)
- Launch into VPC
- Next
- Security Group ApplicationServer
- Next
- Application health check URL /IOPracticeware/Services/AuthenticationService
- Deploy


NOTE: upon deployment command execution can timeout. It's normal, but it should get Green in 1/2 hour. Otherwise, log in onto the box and troubleshoot
