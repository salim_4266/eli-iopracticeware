@echo off
set retry=0

:step1
set step=1  
echo (%date%%time%) Step 1 : caching credentials for System
C:\PsExec.exe -s /accepteula cmd /c net use \\10.1.0.211\ftp\iopracticeware !0practiceware /user:iopracticeware\IOP
if %ERRORLEVEL% NEQ 0 goto retry
C:\PsExec.exe -s /accepteula cmd /c IF exist \\10.1.0.211\ftp\iopracticeware ( echo Pinged share under System and found it ) ELSE ( exit /b 101 )
if %ERRORLEVEL% NEQ 0 goto retry
set retry=0

:step2
set step=2
echo (%date%%time%) Step 2 : caching credentials for Network Service
C:\PsExec.exe -u "nt authority\network service" /accepteula cmd /c net use \\10.1.0.211\ftp\iopracticeware !0practiceware /user:iopracticeware\IOP
if %ERRORLEVEL% NEQ 0 goto retry
C:\PsExec.exe -u "nt authority\network service" /accepteula cmd /c IF exist \\10.1.0.211\ftp\iopracticeware ( echo Pinged share under Network Service and found it ) ELSE ( exit /b 101 )
if %ERRORLEVEL% NEQ 0 goto retry

set retry=0
if %ERRORLEVEL% EQU 0 goto eof

:retry
set /a retry=%retry%+1
echo (%date% %time%) There was an error at STEP%step%. Doing retry %retry% in 5 seconds
timeout 5 /nobreak
if %retry% LSS 50 (goto :step%step%)  
if %retry% EQU 50 (goto :err)

:err  
echo (%date% %time%) Failed after 50 retries
   
:eof