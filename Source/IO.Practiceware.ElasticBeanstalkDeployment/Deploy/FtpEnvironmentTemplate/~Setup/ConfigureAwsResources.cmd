@echo off

@rem Download AWS .NET SDK
powershell -Command "Invoke-WebRequest http://sdk-for-net.amazonwebservices.com/latest/AWSToolsAndSDKForNet.msi -OutFile C:\AWSToolsAndSDKForNet.msi"
start /wait msiexec /qn /i "C:\AWSToolsAndSDKForNet.msi"
echo Installed Amazon SDK

@rem Download latest 64bit AWS CLI
powershell -Command "Invoke-WebRequest https://s3.amazonaws.com/aws-cli/AWSCLI64.msi -OutFile C:\AWSCLI64.msi"
@rem Can't set Idle timeout via Elastic Beanstalk configuration: https://forums.aws.amazon.com/message.jspa?messageID=560395
start /wait msiexec /qn /i "C:\AWSCLI64.msi"
echo Installed Amazon CLI

@rem Configure credentials (TODO: provide access to modifying Load Balancer via IAM role?)
set AWS_ACCESS_KEY_ID=AKIAJAJOTHLHBMP7VWBA
set AWS_SECRET_ACCESS_KEY=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
set AWS_DEFAULT_REGION=us-east-1

@rem Set connection idle timeout to 20 minutes
"C:\Program Files\Amazon\AWSCLI\aws.exe" elb modify-load-balancer-attributes --load-balancer-name %1 --load-balancer-attributes "{\"ConnectionSettings\":{\"IdleTimeout\":1200}}"

@rem Enabling back directory caching for SMB2
reg add HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\Lanmanworkstation\Parameters /v DirectoryCacheLifetime /t REG_DWORD /d 10 /f
@rem Still keeping FileNotFound disabled
reg add HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\Lanmanworkstation\Parameters /v FileNotFoundCacheLifetime /t REG_DWORD /d 0 /f

@rem Security policy to enable SSLv3 and thus support Windows XP users (but become vulnerable to POODLE)
"C:\Program Files\Amazon\AWSCLI\aws.exe" elb create-load-balancer-policy --load-balancer-name %1 --policy-name %1SSLNegotiationPolicy  --policy-type-name SSLNegotiationPolicyType --policy-attributes AttributeName=Reference-Security-Policy,AttributeValue=ELBSecurityPolicy-2014-01

@rem Set policies
"C:\Program Files\Amazon\AWSCLI\aws.exe" elb set-load-balancer-policies-of-listener  --load-balancer-name %1 --load-balancer-port 443 --policy-names %1SSLNegotiationPolicy

@rem Self destruct
start /b "" cmd /c del "%~f0"&exit /b