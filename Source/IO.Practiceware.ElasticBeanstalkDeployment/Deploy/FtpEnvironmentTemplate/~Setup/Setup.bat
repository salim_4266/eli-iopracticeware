@echo off

echo ------- %date%_%time% Installing IO Server on Cloud Machine (ENV %1) -------

xcopy "G:\*.*" "C:\*.*" /y /d
echo Copied all setup scripts to local drive

diskpart /s "C:\ExtendCDrive.diskpart.txt
echo Extended C drive space to include all extra unallocated space

rem Comment out rewrite rules provided by ELB, but keep web.config, so that we can inherit environment settings
start C:\PsExec.exe -s /accepteula cmd /c C:\replace.exe -srcdir "C:\inetpub\wwwroot" -destdir "C:\inetpub\wwwroot" -fname "web.config" -find "<system.webServer>" -replace "<!--<system.webServer>" & C:\replace.exe -srcdir "C:\inetpub\wwwroot" -destdir "C:\inetpub\wwwroot" -fname "web.config" -find "</system.webServer>" -replace "</system.webServer>-->"
echo Fixed ELB provided parent web.config

rem Allow IO ELB Web App to access network storage once credentials are cached (for health check)
%windir%\system32\inetsrv\appcmd set config /section:applicationPools /[name='DefaultAppPool'].processModel.identityType:NetworkService

md "C:\Program Files (x86)\IO Practiceware Server\"
echo Location Created.

copy /Y "\\10.1.0.211\ftp\iopracticeware\%1\~Program Files\*.config" "C:\Program Files (x86)\IO Practiceware Server\*.*"
echo Copied config files

copy /Y "\\10.1.0.211\ftp\iopracticeware\%1\IO Practiceware Server Setup.msi" C:\
echo Copied server setup.

start /wait msiexec /i "C:\IO Practiceware Server Setup.msi" /l*v "C:\IO Practiceware Server Setup.log" /qn UPDATEDB=0 INSTALLDIR="C:\Program Files (x86)\IO Practiceware Server\" DBSERVER="server: cloudserver, database: PracticeRepository" ALERTSDESTINATIONEMAILADDRESS="dev@iopracticeware.com" AUTOSTART_MONITORINGSERVICE=0
echo Installed Server Setup

copy /Y "\\10.1.0.211\ftp\iopracticeware\%1\~Program Files\*.config" "C:\Program Files (x86)\IO Practiceware Server\*.*"
echo Copied config files

rem Hangs during execution if executed as part of this script, so using 'start'
start cmd /c "C:\CacheNetworkCredentials.bat >> C:\CacheNetworkCredentials.log 2>&1"
echo Cached network credentials

wevtutil sl "IO Practiceware" /rt:false /ab:false
wevtutil sl "IO Practiceware" /ms:419430400
echo Set event log max size to 400Mb and enabled overwrite of older entries

net start "IO Practiceware Monitoring Service"
echo Started monitoring service

iisreset
echo Reset IIS

del "C:\IO Practiceware Server Setup.msi"
echo Removed msi

start cmd /c "C:\ConfigureAwsResources.cmd %1LoadBalancer >> C:\ConfigureLoadBalancer.log 2>&1"
echo Configured AWS resources