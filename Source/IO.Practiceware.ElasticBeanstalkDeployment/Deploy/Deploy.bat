@echo off

call DeploySetupAndMigrate.bat %1

if not errorlevel 0 goto OnError

echo Deploying to Amazon

echo[

"C:\Program Files (x86)\AWS Tools\Deployment Tool\AWSDeploy.exe" /w /r %2

if errorlevel 3 goto FreshDeployment

echo[

echo Deployment to AWS complete

goto End

:FreshDeployment

echo[

echo No pre-existing deployment was found. Deploying application from scratch.

echo[

"C:\Program Files (x86)\AWS Tools\Deployment Tool\AWSDeploy.exe" /w %2

echo Deployment to AWS complete

goto End

:OnError
echo[
echo An error occurred. Check logs.

:End

echo[
pause
