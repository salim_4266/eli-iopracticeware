@echo off

set /p BuildPath=Enter build relative path (\\192.168.25.180\TFS-Process\IOP\Builds\)
net use L: "\\192.168.25.180\TFS-Process\IOP\Builds\%BuildPath%" Access$#$@123 /User:Eli\IOP-TFSAdmin
if errorlevel 1 goto CleanupAndExitOnError

net use Y: \\10.1.0.211\ftp\iopracticeware\%1 !0practiceware /user:IOP
if errorlevel 1 goto CleanupAndExitOnError

echo Securing connection to SQL server
net use \\10.1.1.162 !0practiceware /user:sql1\Admin
if errorlevel 1 goto CleanupAndExitOnError

echo Copying cloud custom config to migrator directory
move /y "L:\Migrator\IO.Practiceware.Custom.config" "L:\Migrator\IO.Practiceware.Custom.config.bak"
if errorlevel 1 goto CleanupAndExitOnError

copy /y "Y:\~Program Files\IO.Practiceware.Custom.config" "L:\Migrator\IO.Practiceware.Custom.config"
if errorlevel 1 goto CleanupAndExitOnError

echo Updating custom config file for migration
replace.exe -srcdir "L:\Migrator" -destdir "L:\Migrator" -fname "IO.Practiceware.Custom.config" -find "User Id=ioclouduser;Password=!0pware" -replace "User Id=admin;Password=!0practiceware"
if errorlevel 1 goto CleanupAndExitOnError

L:\Migrator\IO.Practiceware.DbMigration.exe
if errorlevel 10000 goto OnMigrationError
if not errorlevel 0 goto OnMigrationError

echo Restoring original custom config file in migrator directory
move /y "L:\Migrator\IO.Practiceware.Custom.config" "L:\Migrator\IO.Practiceware.Custom.Cloud.config"
move /y "L:\Migrator\IO.Practiceware.Custom.config.bak" "L:\Migrator\IO.Practiceware.Custom.config"

echo Copying server setup to FTP
xcopy /y "L:\IO Practiceware Server Setup.msi" "Y:\IO Practiceware Server Setup.msi"

echo Updating version check file
echo #DO NOT DELETE THIS FILE. USED BY CLOUD > Y:\!version.txt
GetAssemblyName.exe L:\Client\IO.Practiceware.Core.dll >> Y:\!version.txt

echo Copying client setup to client NewVersion directories
UpdateCloudClientSetup.exe "Y:\~Program Files\IO.Practiceware.Custom.config" "L:\IO Practiceware Client Setup.exe"

echo Updating deployment scripts and dependencies
robocopy FtpEnvironmentTemplate Y:\ * /E

net use Y: /delete /y
net use L: /delete /y

echo[ 
echo Migration and setup deployment complete
echo[

pause

echo[

exit /b 0

:OnMigrationError

echo Migration failed with code %errorlevel%. Cleaning up.
move /y "L:\Migrator\IO.Practiceware.Custom.config" "L:\Migrator\IO.Practiceware.Custom.Cloud.config"
move /y "L:\Migrator\IO.Practiceware.Custom.config.bak" "L:\Migrator\IO.Practiceware.Custom.config"

echo[ 
echo A migration error occurred. Check logs.
echo[

pause 
echo[

:CleanupAndExitOnError
net use Y: /delete /y
net use L: /delete /y

exit /b -1