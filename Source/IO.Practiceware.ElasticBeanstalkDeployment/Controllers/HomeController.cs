﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using IO.Practiceware.ElasticBeanstalkDeployment.WebServerApi;

namespace IO.Practiceware.ElasticBeanstalkDeployment.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ViewResult HealthCheck()
        {
            const string checkWebAppErrorFormat = "IO Web is not responding properly. Error: {0}";
            var failures = new List<string>();

            // Doing a full check or short check?
            var versionSource = ConfigurationManager.AppSettings["CloudInstalledVersionSource"];
            if (string.IsNullOrEmpty(versionSource))
            {
                // Ping authentication service of web app to see if server is up
                var healthCheckRequest = (HttpWebRequest) WebRequest.Create(@"http://localhost/IOPracticeware/Services/AuthenticationService");
                try
                {
                    healthCheckRequest.Timeout = (int) TimeSpan.FromSeconds(5).TotalMilliseconds;
                    var response = (HttpWebResponse) healthCheckRequest.GetResponse();
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        failures.Add(string.Format(checkWebAppErrorFormat, "status code is " + response.StatusCode));
                    }
                }
                catch (WebException ex)
                {
                    failures.Add(string.Format(checkWebAppErrorFormat, ex));
                }
            }
            else
            {
                // Check whether right version is currently installed and also verifies connection with Ftp
                try
                {
                    var versionLine = System.IO.File.ReadAllLines(versionSource).LastOrDefault(l => !string.IsNullOrEmpty(l));
                    if (versionLine == null)
                    {
                        failures.Add("No version is specified in Version source file. Please, check");
                    }
                    else
                    {
                        var webServerClient = new AuthenticationServiceClient();
                        if (!webServerClient.ValidateAssemblyNameAndVersion(versionLine))
                        {
                            failures.Add(string.Format("IO Web version mismatch"));
                        }
                    }
                }
                catch (Exception ex)
                {
                    failures.Add("Version check failed. Error: " + ex);
                }
            }

            // Set status code in case of failure
            if (failures.Any())
            {
                Response.StatusCode = 500;
            }

            return View((object)string.Join(Environment.NewLine, failures));
        }

        public ActionResult Index()
        {
            return View();
        }
        //
    }
}
