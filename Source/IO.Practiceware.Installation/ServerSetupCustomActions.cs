﻿using System.Net;
using System.IO;
using System.Net.FtpClient;
using System.Numerics;
using System.Security.Principal;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Deployment.WindowsInstaller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Microsoft.Web.Administration;
using Microsoft.Win32;
using FileAttributes = System.IO.FileAttributes;

namespace IO.Practiceware.Installation
{
    public static class ServerSetupCustomActions
    {
        private const string CustomActionException = "CustomActionException: ";
        private const string NetworkServiceAccount = @"NT AUTHORITY\NETWORK SERVICE";
        private const string LocalSystemAccount = @"NT AUTHORITY\SYSTEM";
        private const string InstallDirectory = "INSTALLDIR";
        private const string IoFileSharePath = "IOFILESHAREDIR";
        private const string MonitoringServiceName = "IO Practiceware Monitoring Service";
        private const string CustomConfigFile = @"IO.Practiceware.Custom.config";
        private const string AutoStartService = "AUTOSTART_MONITORINGSERVICE";
        private const string ConnectionStringsInput = "DBSERVER";
        private const string AlertsDestinationEmailAddresses = "ALERTSDESTINATIONEMAILADDRESS";
        private const string ConnectionStringsValidInput = "CONNECTION_STRINGS_VALID";
        private const string ReplicationTypeInput = "REPLICATIONTYPE";

        private const string SnapshotReplicationType = null;
        private const string NonSnapshotReplicationType = "1";
        /// <summary>
        /// Checkbox allows switching between 1 and nothing
        /// </summary>
        private const string SkipConfiguringReplicationType = "0";

        private const string PracticeRepositoryConnectionStringNamePrefix = "PracticeRepository";

        private static readonly string CurrentMachineAccount = GetMachineName();

        [DllImport("kernel32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern bool Wow64DisableWow64FsRedirection(ref IntPtr ptr);

        [DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern bool Wow64RevertWow64FsRedirection(ref IntPtr ptr);

        [DllImport("netapi32.dll", CharSet = CharSet.Auto)]
        private static extern int NetWkstaGetInfo(string server,
                                                  int level,
                                                  out IntPtr info);

        [DllImport("netapi32.dll")]
        private static extern int NetApiBufferFree(IntPtr pBuf);

        private static string GetMachineNetBiosDomain()
        {
            IntPtr pBuffer;

            int retval = NetWkstaGetInfo(null, 100, out pBuffer);
            if (retval != 0)
                throw new Win32Exception(retval);

            var info = (WKSTA_INFO_100)Marshal.PtrToStructure(pBuffer, typeof(WKSTA_INFO_100));
            string domainName = info.wki100_langroup;
            NetApiBufferFree(pBuffer);
            return domainName;
        }

        private static string GetMachineName()
        {
            return string.Format(@"{0}\{1}$", GetMachineNetBiosDomain(), Environment.MachineName);
        }

        private static ServiceController GetServiceController(string serviceName)
        {
            ServiceController serviceController = ServiceController.GetServices().FirstOrDefault(i => i.ServiceName == serviceName);
            if (serviceController == null)
            {
                var exception = new Exception(string.Format("Could not find {0}", serviceName));
                Trace.TraceError(exception.ToString());
                throw exception;
            }
            return serviceController;
        }

        /// <summary>
        ///   Updates custom config file based on installation provided data
        /// </summary>
        /// <param name="session"> </param>
        /// <remarks>! DEFERRED ACTION ! Context here is System account (!)</remarks>
        /// <returns> </returns>
        [CustomAction]
        public static ActionResult UpdateCustomConfigFile(Session session)
        {
            var result = ActionResult.Success;

            try
            {
                if (session == null)
                {
                    throw new ArgumentNullException("session");
                }

                string customConfigFilePath = Path.Combine(session.CustomActionData[InstallDirectory], CustomConfigFile);
                session.Log("Custom config file path: " + customConfigFilePath);

                var xDoc = new XmlDocument();
                xDoc.Load(customConfigFilePath);

                UpdateConnectionStrings(session, xDoc);

                UpdateDbMigrationAttributes(xDoc, session.CustomActionData[ReplicationTypeInput]);

                UpdateAppSettings(session, xDoc);

                UpdateApplicationServer(xDoc);

                UpdateServerDataDrive(session, xDoc);

                UpdateUpdateMonitorConfigurationElements(xDoc, session);

                RemoveObsoleteCloudSettings(xDoc, false);

                xDoc.Save(customConfigFilePath); // save the config file  
            }
            catch (Exception ex)
            {
                result = ActionResult.Failure;
                if (session != null)
                {
                    session.Log(CustomActionException + ex);
                }
            }

            return result;
        }

        private static void RemoveObsoleteCloudSettings(XmlDocument xDoc, Boolean isClient)
        {
            XmlNode appSettingsNode = xDoc.SelectSingleNode("//configuration/appSettings");

            if (appSettingsNode != null)
            {
                var servicesBaseUriNode = (XmlElement)xDoc.SelectSingleNode("//configuration/appSettings/add[@key='ServicesBaseUri']");

                if (servicesBaseUriNode != null) appSettingsNode.RemoveChild(servicesBaseUriNode);

                if (isClient)
                {
                    var authenticationTokenNode = (XmlElement)xDoc.SelectSingleNode("//configuration/appSettings/add[@key='AuthenticationToken']");

                    if (authenticationTokenNode != null) appSettingsNode.RemoveChild(authenticationTokenNode);
                }

                var generateEndpointsNode = (XmlElement)xDoc.SelectSingleNode("//configuration/appSettings/add[@key='GenerateEndpoints']");

                if (generateEndpointsNode != null) appSettingsNode.RemoveChild(generateEndpointsNode);
            }

            XmlNode configNode = xDoc.SelectSingleNode("//configuration");
            if (configNode != null)
            {
                //Remove End Points in Service Model Node
                XmlNode serviceModelNode = xDoc.SelectSingleNode("//configuration/system.serviceModel");

                if (serviceModelNode != null) serviceModelNode.RemoveAll();

                //Remove applicationServerClient node
                var applicationServerClientNode = (XmlElement)xDoc.SelectSingleNode("//configuration/applicationServerClient");
                if (applicationServerClientNode != null)
                {
                    string servicesUriFormat = applicationServerClientNode.GetAttribute("servicesUriFormat");
                    if (servicesUriFormat == "https://localhost/IOPracticeware/Services/{0}/Protobuf") applicationServerClientNode.SetAttribute("isEnabled", "false");
                }
            }
        }

        private static void UpdateAppSettings(Session session, XmlDocument xDoc)
        {
            XmlNode appSettingsNode = xDoc.SelectSingleNode("//configuration/appSettings");
            if (appSettingsNode == null)
            {
                appSettingsNode = xDoc.CreateNode(XmlNodeType.Element, "appSettings", null);
                if (xDoc.DocumentElement != null) xDoc.DocumentElement.AppendChild(appSettingsNode);
            }

            UpdateReplicationMonitorSeedPercentage(xDoc, appSettingsNode);

            UpdateAlertsDestinationEmailAddress(session, xDoc, appSettingsNode);
        }

        private static void UpdateApplicationServer(XmlDocument xDoc)
        {
            var applicationServer = xDoc.SelectSingleNode("//configuration/applicationServer");
            if (applicationServer == null)
            {
                var config = xDoc.SelectSingleNode("//configuration");
                if (config != null)
                {
                    XmlNode applicationServerNode = xDoc.CreateElement("applicationServer");
                    applicationServer = config.AppendChild(applicationServerNode);

                }
            }

            // ReSharper disable once PossibleNullReferenceException
            var clients = applicationServer.SelectSingleNode("clients");

            if (clients == null)
            {
                clients = xDoc.CreateNode(XmlNodeType.Element, "clients", "");
                applicationServer.AppendChild(clients);
            }

            if (clients.ChildNodes.Count == 0)
            {
                XmlElement client = xDoc.CreateElement("client");
                client.SetAttribute("name", "Default");
                client.SetAttribute("connectionStringName", PracticeRepositoryConnectionStringNamePrefix);
                client.SetAttribute("authenticationToken", "PracticeRepository");
                clients.AppendChild(client);
            }
        }

        private static void UpdateServerDataDrive(Session session, XmlDocument xDoc)
        {
            // We support updating ServerDataPath when application server is configured for 1 client
            // ReSharper disable once AssignNullToNotNullAttribute
            var clientNodes = xDoc.SelectNodes("//configuration/applicationServer/clients/client")
                .Cast<XmlElement>().ToList();
            if (clientNodes.Count != 1) return;

            // Evaluate file share path
            string ioFileSharePath = session.CustomActionData[IoFileSharePath];
            var pinpointPath = Path.Combine(ioFileSharePath, "Pinpoint");

            // Set it
            var node = clientNodes.First();
            node.SetAttribute("serverDataPath", pinpointPath);
        }

        private static void UpdateAlertsDestinationEmailAddress(Session session, XmlDocument xDoc, XmlNode appSettingsNode)
        {
            string alertsDestinationEmailAddress = session.CustomActionData[AlertsDestinationEmailAddresses];

            if (alertsDestinationEmailAddress != null)
            {
                var add = xDoc.SelectSingleNode("//configuration/appSettings/add[@key='AlertsDestinationEmailAddresses']") as XmlElement;

                if (add == null)
                {
                    add = xDoc.CreateElement("add");
                    add.SetAttribute("key", "AlertsDestinationEmailAddresses");
                    appSettingsNode.AppendChild(add);
                }

                add.SetAttribute("value", alertsDestinationEmailAddress);
            }
        }

        private static void UpdateReplicationMonitorSeedPercentage(XmlDocument xDoc, XmlNode appSettingsNode)
        {
            var replicationMonitorSeedNode = (XmlElement)xDoc.SelectSingleNode("//configuration/appSettings/add[@key='ReplicationMonitorSeedPercentageWarningThreshold']");
            if (replicationMonitorSeedNode == null)
            {
                replicationMonitorSeedNode = xDoc.CreateElement("add");
                replicationMonitorSeedNode.SetAttribute("key", "ReplicationMonitorSeedPercentageWarningThreshold");
                replicationMonitorSeedNode.SetAttribute("value", "85");
                appSettingsNode.AppendChild(replicationMonitorSeedNode);
            }
        }

        private static void UpdateUpdateMonitorConfigurationElements(XmlDocument xDoc, Session session)
        {
            XmlNode deprecatedUpdateServiceConfigurationNode = xDoc.SelectSingleNode("//configuration/updateServiceConfiguration");
            XmlNode updateMonitorConfigurationNode = xDoc.SelectSingleNode("//configuration/updateMonitorConfiguration");

            if (updateMonitorConfigurationNode == null)
            {
                updateMonitorConfigurationNode = xDoc.CreateNode(XmlNodeType.Element, "updateMonitorConfiguration", null);
                if (xDoc.DocumentElement != null)
                {
                    // warning for xDoc.DocumentElement possibly being null...but there is a check above.
                    if (deprecatedUpdateServiceConfigurationNode != null)
                    {
                        xDoc.DocumentElement.InsertAfter(updateMonitorConfigurationNode, deprecatedUpdateServiceConfigurationNode);
                    }
                    else
                    {
                        xDoc.DocumentElement.AppendChild(updateMonitorConfigurationNode);
                    }
                }
            }

            if (deprecatedUpdateServiceConfigurationNode != null && xDoc.DocumentElement != null)
            {
                xDoc.DocumentElement.RemoveChild(deprecatedUpdateServiceConfigurationNode);
            }

            XmlNode updateClientsNode = xDoc.SelectSingleNode("//configuration/updateMonitorConfiguration/updateClients");
            if (updateClientsNode == null)
            {
                updateClientsNode = xDoc.CreateNode(XmlNodeType.Element, "updateClients", null);
                updateMonitorConfigurationNode.AppendChild(updateClientsNode);
            }

            var updateClientElement = (XmlElement)xDoc.SelectSingleNode("//configuration/updateMonitorConfiguration/updateClients/updateClient");

            if (updateClientElement == null)
            {
                if (string.IsNullOrEmpty(session.CustomActionData["FTPUSERNAME"]) || string.IsNullOrEmpty(session.CustomActionData["FTPPASSWORD"]))
                {
                    session.Log("No updateClient element present. No credentials provided. Not adding.");
                    return;
                }
                session.Log("No updateClient element present. Adding.");
                updateClientElement = (XmlElement)xDoc.CreateNode(XmlNodeType.Element, "updateClient", null);
                updateClientsNode.AppendChild(updateClientElement);
            }

            RemoveLegacyUpdateClientAttributes(updateClientElement);

            if (!string.IsNullOrEmpty(session.CustomActionData["FTPUSERNAME"]) && !string.IsNullOrEmpty(session.CustomActionData["FTPPASSWORD"]))
            {
                updateClientElement.SetAttribute("userName", session.CustomActionData["FTPUSERNAME"]);
                updateClientElement.SetAttribute("password", session.CustomActionData["FTPPASSWORD"]);
            }

            const string ftpAddress = "ftps://ftp.iopracticeware.com:990/SoftwareUpdates/";
            updateClientElement.SetAttribute("ftpAddress", ftpAddress);

            string ioFileSharePath = session.CustomActionData[IoFileSharePath];

            if (string.IsNullOrEmpty(updateClientElement.GetAttribute("localPath")) ||
                updateClientElement.GetAttribute("localPath").EndsWith(@"\Pinpoint\NewVersion\"))
            {
                session.Log(String.Format(@"The localPath is currently set to {0}, this indicates a migration is needed to update the folder structure.", updateClientElement.GetAttribute("localPath")));
                UpdateSoftwareUpdatesFolder(session, ioFileSharePath, updateClientElement);
            }
            else
            {
                session.Log(String.Format(@"The local path is currently empty or does not end with \Pinpoint\NewVersion\, setting the localPath to {0}. No SoftwareUpdates folder migration performed.", Path.Combine(ioFileSharePath, "Pinpoint", "SoftwareUpdates") + @"\"));
                updateClientElement.SetAttribute("localPath", Path.Combine(ioFileSharePath, "Pinpoint", "SoftwareUpdates") + @"\");
            }
        }

        private static void UpdateSoftwareUpdatesFolder(Session session, string ioFileSharePath, XmlElement updateClientElement)
        {
            // Store old directory.
            var oldLocalDirectory = updateClientElement.GetAttribute("localPath");
            if (string.IsNullOrEmpty(oldLocalDirectory))
            {
                oldLocalDirectory = Path.Combine(ioFileSharePath, "Pinpoint", "NewVersion");
            }

            // Update to new directory.
            updateClientElement.SetAttribute("localPath", Path.Combine(ioFileSharePath, "Pinpoint", "SoftwareUpdates") + @"\");
            var newLocalDirectory = updateClientElement.GetAttribute("localPath");

            // Create the new directory if it doesn't exist.
            if (!Directory.Exists(newLocalDirectory))
            {
                Directory.CreateDirectory(newLocalDirectory);
                session.Log("Created directory: {0}", newLocalDirectory);
            }

            var localSoftwareUpdatesClientPinpointPath = Path.Combine(newLocalDirectory, "Client", "Pinpoint");
            if (!Directory.Exists(localSoftwareUpdatesClientPinpointPath))
            {
                Directory.CreateDirectory(localSoftwareUpdatesClientPinpointPath);
                session.Log("Created directory: {0}", localSoftwareUpdatesClientPinpointPath);
            }

            session.Log(String.Format(@"Starting local file move of {0} to {1}", oldLocalDirectory, localSoftwareUpdatesClientPinpointPath));
            if (Directory.Exists(oldLocalDirectory))
            {
                CopyAll(session, new DirectoryInfo(oldLocalDirectory), new DirectoryInfo(localSoftwareUpdatesClientPinpointPath));
            }

            const string softwareUpdatesClientPinpointUri = @"SoftwareUpdates/Client/Pinpoint/";
            session.Log(@"Starting move of FTP files from root to " + softwareUpdatesClientPinpointUri);

            var userName = updateClientElement.GetAttribute("userName");
            var password = updateClientElement.GetAttribute("password");
            var uri = new Uri(updateClientElement.GetAttribute("ftpAddress"));

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(uri.ToString()))
            {
                session.Log(String.Format("FTP configuration is not complete. Parameters used are: username: {0}, password: {1}, uri: {2}.", userName, password, uri.OriginalString));
                return;
            }

            // In the case this is a brand new install, the oldLocalDirectory does not exist. So only move if the directory exists.
            if (Directory.Exists(oldLocalDirectory))
            {
                DeleteDirectory(session, oldLocalDirectory, true);
            }

            using (var client = new FtpClient
            {
                Host = uri.Host,
                Credentials = new NetworkCredential(userName, password),
                Port = uri.Port > 0 ? uri.Port : 21,
                EncryptionMode = "ftps".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase) ? FtpEncryptionMode.Implicit : FtpEncryptionMode.None,
                EnableThreadSafeDataConnections = false
            })
            {
                client.ValidateCertificate += (sender, e) => e.Accept = true;
                try
                {
                    client.Connect();
                    session.Log(String.Format("Connected to FTP server."));
                }
                catch (Exception ex)
                {
                    session.Log(String.Format("Could not connect to FTP server. Parameters used are: username: {0}, password: {1}, uri: {2}. Exception: {3}", userName, password, uri.OriginalString, ex));
                    throw;
                }

                if (!client.DirectoryExists(softwareUpdatesClientPinpointUri))
                {
                    var toMove = client.GetListing("/")
                        .Where(l => !l.Name.StartsWith(@"_")
                                    && !l.Name.StartsWith(@"~")
                                    && !l.FullName.EndsWith(@".bak") && !l.FullName.EndsWith("IO Practiceware Server Setup.msi"))
                        .ToArray();

                    client.CreateDirectory(softwareUpdatesClientPinpointUri);

                    foreach (var ftpListItem in toMove)
                    {
                        try
                        {
                            session.Log(String.Format("Trying to move FTP item {0} to {1}", ftpListItem.FullName, softwareUpdatesClientPinpointUri + (ftpListItem.FullName.StartsWith(@"/") ? ftpListItem.FullName.Substring(1) : ftpListItem.FullName)));
                            client.Rename(ftpListItem.FullName, softwareUpdatesClientPinpointUri + (ftpListItem.FullName.StartsWith(@"/") ? ftpListItem.FullName.Substring(1) : ftpListItem.FullName));
                        }
                        catch (Exception ex)
                        {
                            session.Log(String.Format("Could not move FTP item {0} to {1}. Exception {2}", ftpListItem.FullName, softwareUpdatesClientPinpointUri + ftpListItem.FullName, ex));
                            throw;
                        }
                    }
                }
                else
                {
                    session.Log(String.Format("The {0} directory already exists, no action(s) performed.", softwareUpdatesClientPinpointUri));
                }
            }

            session.Log("Migrated NewVersion to SoftwareUpdates directory.");
        }

        /// <summary>
        /// Deletes the directory. Deletes directories and files recursively. Handles readonly files. Deletes one at a time
        /// because of known limitation in Directory.DeleteDirectory(path, true). See: [insert reference].
        /// Calls the deletionScope resolver before each file and directory to be deleted. Disposes the IDisposable upon
        /// completion of deleting that file/directory.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="directory">The directory.</param>
        /// <param name="silent">if set to <c>true</c> runs in silent mode and doesn't raise exceptions.</param>
        /// <exception cref="System.ApplicationException">Could not delete directory that contains the following content</exception>
        public static void DeleteDirectory(Session session, string directory, bool silent = false)
        {
            var directoryInfo = new DirectoryInfo(directory);
            if (!directoryInfo.Exists) return;

            var files = new FileInfo[0];
            try
            {
                files = directoryInfo.GetFiles();
            }
            catch (Exception ex)
            {
                if (!silent) throw;
                session.Log("Silent clean of directory partially failed. Error: {0}", ex);
            }

            var dirs = new DirectoryInfo[0];
            try
            {
                dirs = directoryInfo.GetDirectories();
            }
            catch (Exception ex)
            {
                if (!silent) throw;
                session.Log("Silent clean of directory partially failed. Error: {0}", ex);
            }


            // Delete all files in a directory
            foreach (var file in files)
            {
                try
                {
                    file.Attributes = FileAttributes.Normal;
                    File.Delete(file.FullName);
                }
                catch (Exception ex)
                {
                    if (!silent) throw;
                    session.Log("Silent clean of directory partially failed. Error: {0}", ex);
                }

            }

            // Delete all subdirectories recursively
            foreach (var dir in dirs)
            {
                DeleteDirectory(session, dir.FullName, silent);
            }


            // Can't delete non-empty folders, so check beforehand
            var currentDirectoryContent = directoryInfo.EnumerateFileSystemInfos().ToArray();
            if (currentDirectoryContent.Any())
            {
                if (!silent)
                {
                    throw new ApplicationException(String.Format("Could not delete directory {0} that contains the following content: {1}.", directoryInfo.FullName, String.Join(",", currentDirectoryContent.Select(s => s.FullName))));
                }
            }
            else
            {
                try
                {
                    // Try delete folder
                    directoryInfo.Attributes = directoryInfo.Attributes & ~FileAttributes.ReadOnly;
                    directoryInfo.Delete(false);
                }
                catch (Exception ex)
                {
                    if (!silent) throw;
                    session.Log("Silent clean of directory partially failed. Error: {0}", ex);
                }
            }
        }

        private static void CopyAll(Session session, DirectoryInfo source, DirectoryInfo target)
        {
            // Check if the target directory exists, if not, create it.
            if (!Directory.Exists(target.FullName))
            {
                try
                {
                    Directory.CreateDirectory(target.FullName);
                    session.Log(String.Format("Created directory: {0}", target.FullName));
                }
                catch (Exception ex)
                {
                    session.Log("Failed to create directory {0}.\r\n{1}", target.FullName,ex);
                }
            }

            // Copy each file into it's new directory.
            foreach (var fi in source.GetFiles())
            {
                var targetFile = Path.Combine(target.ToString(), fi.Name);
                if (!File.Exists(targetFile))
                {
                    try
                    {
                        fi.CopyTo(targetFile);
                        session.Log(String.Format("Copied file {0} to {1}", fi.Name, targetFile));
                    }
                    catch (Exception ex)
                    {
                        session.Log("Failed to copy file {0} to {1}.\r\n{2}", fi.FullName, targetFile, ex);
                    }
                }
            }

            // Copy each subdirectory using recursion.
            foreach (var sourceSubDirectory in source.GetDirectories())
            {
                var targetSubDirectory = target.CreateSubdirectory(sourceSubDirectory.Name);
                CopyAll(session, sourceSubDirectory, targetSubDirectory);
            }
        }

        private static void RemoveLegacyUpdateClientAttributes(XmlElement updateClientElement)
        {
            if (updateClientElement != null && updateClientElement.GetAttribute("ftpNewVersionPath") != string.Empty)
            {
                updateClientElement.RemoveAttribute("ftpNewVersionPath");
            }

            if (updateClientElement != null && updateClientElement.GetAttribute("localNewVersionPath") != string.Empty)
            {
                updateClientElement.RemoveAttribute("localNewVersionPath");
            }

            if (updateClientElement != null && updateClientElement.GetAttribute("username") != string.Empty)
            {
                updateClientElement.RemoveAttribute("username");
            }
        }

        private static void UpdateDbMigrationAttributes(XmlDocument xDoc, string replicationType)
        {
            var dbMigrations = xDoc.SelectNodes("//configuration/dbMigrations/dbMigration");
            if (dbMigrations == null 
                || dbMigrations.Count != 1 // Only if 1 Db migration (non-cloud)
                || replicationType == SkipConfiguringReplicationType)
            {
                return;
            }

            // Update replication settings
            foreach (var dbMigration in dbMigrations.OfType<XmlElement>())
            {
                dbMigration.SetAttribute("useSnapshotForReplication",
                    replicationType == NonSnapshotReplicationType ? "false" : "true");
                dbMigration.SetAttribute("useDbSyncForReplication",
                    replicationType == NonSnapshotReplicationType ? "true" : "false");
                dbMigration.SetAttribute("executeDbSyncScriptAutomatically",
                    replicationType == NonSnapshotReplicationType ? "true" : "false");
            }
        }

        private static void UpdateConnectionStrings(Session session, XmlDocument xDoc)
        {
            string databaseServerInputEntry = session.CustomActionData[ConnectionStringsInput];
            session.Log("Database Server List: " + databaseServerInputEntry);

            List<InputDatabaseServer> inputDatabaseServerList = ParseDatabaseServerInputEntry(databaseServerInputEntry);

            if (inputDatabaseServerList != null && inputDatabaseServerList.Count != 0)
            {
                AlterCustomConfigFileWithInputList(xDoc, inputDatabaseServerList);
            }
            else
            {
                XmlNode connectionStrings = xDoc.SelectSingleNode("//configuration/connectionStrings");

                var dbMigrations = xDoc.SelectNodes("//configuration/dbMigrations/dbMigration");

                // don't redefine connection strings if there is more than 1 dbmigration group (could be cloud envrionment)
                if (connectionStrings == null || dbMigrations == null || dbMigrations.Count != 1) return;

                connectionStrings.ChildNodes
                    .OfType<XmlElement>()
                    .Where(e => e.GetAttribute("name")
                        .StartsWith(PracticeRepositoryConnectionStringNamePrefix, StringComparison.OrdinalIgnoreCase))
                    .ToList()
                    .ForEach(e => connectionStrings.RemoveChild(e));

                var migrationConnectionStrings = dbMigrations[0]["migrationConnectionStrings"];
                if (migrationConnectionStrings == null)
                {
                    migrationConnectionStrings = xDoc.CreateElement("migrationConnectionStrings");
                    dbMigrations[0].AppendChild(migrationConnectionStrings);
                }

                migrationConnectionStrings.RemoveAll();

                int count = 1;

                foreach (var inputConnectionString in databaseServerInputEntry.Split('\r', '\n').Where(l => !string.IsNullOrWhiteSpace(l)).ToArray())
                {
                    var name = PracticeRepositoryConnectionStringNamePrefix;
                    if (count > 1) name += count.ToString();

                    XmlElement addConnectionString = xDoc.CreateElement("add");
                    addConnectionString.SetAttribute("name", name);
                    addConnectionString.SetAttribute("connectionString", inputConnectionString.Trim());
                    addConnectionString.SetAttribute("providerName", "AdoServiceProvider");
                    connectionStrings.AppendChild(addConnectionString);

                    XmlElement addMigration = xDoc.CreateElement("add");
                    addMigration.InnerText = name;
                    migrationConnectionStrings.AppendChild(addMigration);

                    count++;
                }
            }

        }

        /// <summary>
        /// Applies known system configuration settings for best application experience
        /// </summary>
        /// <param name="session"></param>
        /// <remarks>! DEFERRED ACTION ! Context here is System account (!)</remarks>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult ConfigureServerProperties(Session session)
        {
            try
            {
                // Reset a couple of SMB2 Client Redirector Caches we used to configure in attempt to ensure Cloud Storage works properly
                var registryKey = Registry.LocalMachine.OpenSubKey(@"System\CurrentControlSet\Services\Lanmanworkstation\Parameters", true);
                if (registryKey != null)
                {
                    if (registryKey.GetValue("DirectoryCacheLifetime") != null)
                    {
                        registryKey.DeleteValue("DirectoryCacheLifetime");
                    }
                    if (registryKey.GetValue("FileNotFoundCacheLifetime") != null)
                    {
                        registryKey.DeleteValue("FileNotFoundCacheLifetime");
                    }
                }
            }
            catch (Exception ex)
            {
                session.Log("{0} {1}", CustomActionException, ex);
            }

            return ActionResult.Success;
        }

        /// <summary>
        ///   Provides the Network Service Account and the Local System Account with full access to the pinpoint directory and the server installation path.
        /// </summary>
        /// <param name="session"> </param>
        /// <remarks>! DEFERRED ACTION ! Context here is System account (!)</remarks>
        /// <returns> </returns>
        [CustomAction]
        public static ActionResult ConfigureLocalDirectoryPermissions(Session session)
        {
            try
            {
                session.Log("Executing ConfigureLocalDirectoryPermissions with custom arguments: {0}", string.Join(", ", session.CustomActionData.Select(kv => string.Format("{0} - {1}", kv.Key, kv.Value)).ToArray()));

                // Prepare list of directories to grant permissions
                var directoryAccounts =
                    from directory in new[]
                    {
                        session.CustomActionData[InstallDirectory], 
                        session.CustomActionData[IoFileSharePath]
                    }
                    from account in IsInDomain()
                        ? new[]
                            {
                                @"NT AUTHORITY\NETWORK SERVICE", 
                                @"NT AUTHORITY\SYSTEM", 
                                CurrentMachineAccount
                            }
                        : new[] { "NETWORK SERVICE", "SYSTEM" }
                    select new { directory, account };

                // Grant permissions
                foreach (var i in directoryAccounts)
                {
                    try
                    {
                        session.Log(string.Format("Granting full access to {0} on {1}.", i.account, i.directory));
                        GrantFullAccess(i.directory, i.account);
                    }
                    catch (Exception ex)
                    {
                        session.Log(ex.ToString());
                        return ActionResult.Failure;
                    }
                }
            }
            catch (Exception ex)
            {
                if (session != null)
                {
                    session.Log(CustomActionException + ex);
                }
                return ActionResult.Failure;
            }

            return ActionResult.Success;
        }

        private static bool IsInDomain()
        {
            // ReSharper disable RedundantAssignment
            var status = Win32.NetJoinStatus.NetSetupUnknownStatus;
            // ReSharper restore RedundantAssignment
            IntPtr pDomain;
            int result = Win32.NetGetJoinInformation(null, out pDomain, out status);
            if (pDomain != IntPtr.Zero)
            {
                NetApiBufferFree(pDomain);
            }
            if (result == Win32.ErrorSuccess)
            {
                return status == Win32.NetJoinStatus.NetSetupDomainName;
            }
            throw new Exception("Domain Info Get Failed");
        }

        internal static void GrantFullAccess(string directory, string account)
        {
            try
            {
                DirectorySecurity directorySecurity = Directory.GetAccessControl(directory);

                if (!directorySecurity.GetAccessRules(true, true, typeof(NTAccount)).OfType<FileSystemAccessRule>().Any(
                    r => r.IdentityReference.Value.Equals(account, StringComparison.OrdinalIgnoreCase)
                    && r.FileSystemRights == FileSystemRights.FullControl && r.AccessControlType == AccessControlType.Allow))
                {
                    var rule = new FileSystemAccessRule(account, FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow);
                    directorySecurity.AddAccessRule(rule);
                    directorySecurity.SetAccessRule(rule);
                    Directory.SetAccessControl(directory, directorySecurity);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Could not grant full access to the directory {0} for the account {1}", directory, account), ex);
            }
        }

        private static IDictionary<string, string> GetConfigFileAuthenticationTokens(string installDirectory)
        {
            var results = new Dictionary<string, string>();

            string customConfigFilePath = Path.Combine(installDirectory, CustomConfigFile);
            var xDoc = new XmlDocument();
            xDoc.Load(customConfigFilePath);

            XmlNode clients = xDoc.SelectSingleNode("//configuration/applicationServer/clients");
            if (clients != null)
            {
                foreach (XmlNode client in clients.ChildNodes.OfType<XmlNode>().ToArray())
                {
                    if (client.Attributes == null) continue;

                    XmlAttribute name = client.Attributes["connectionStringName"];

                    if (name == null) continue;

                    XmlAttribute authenticationToken = client.Attributes["authenticationToken"];

                    if (authenticationToken == null) continue;

                    results.Add(name.Value, authenticationToken.Value);
                }
            }
            return results;
        }

        private static IDictionary<string, string> GetConfigFileConnectionStrings(Session session)
        {
            var results = new Dictionary<string, string>();

            string customConfigFilePath = Path.Combine(session.CustomActionData[InstallDirectory], CustomConfigFile);

            try
            {
                var xDoc = new XmlDocument();
                xDoc.Load(customConfigFilePath);

                XmlNode connectionStrings = xDoc.SelectSingleNode("//configuration/connectionStrings");
                if (connectionStrings != null)
                {
                    foreach (XmlNode connectionString in connectionStrings.ChildNodes.OfType<XmlNode>().ToArray())
                    {
                        if (connectionString.Attributes == null) continue;

                        XmlAttribute name = connectionString.Attributes["name"];
                        if (name == null || !name.Value.StartsWith(PracticeRepositoryConnectionStringNamePrefix, StringComparison.OrdinalIgnoreCase)) continue;

                        XmlAttribute connectionStringAttribute = connectionString.Attributes["connectionString"];
                        if (connectionStringAttribute == null) continue;

                        results.Add(name.Value, connectionStringAttribute.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                session.Log(CustomActionException + string.Format("Could not get the connection string at the file {0}, whose key is {1} due to the following exception: {2}", customConfigFilePath, PracticeRepositoryConnectionStringNamePrefix, ex));
            }

            if (results.Count == 0)
                session.Log(string.Format("Could not get the connection string at the file {0}, whose key is {1}", customConfigFilePath, PracticeRepositoryConnectionStringNamePrefix));
            return results;
        }

        /// <summary>
        ///   Verifies the connection strings in the session. Tries to open connections to each value specified.
        /// </summary>
        /// <param name="session"> </param>
        /// <returns> </returns>
        [CustomAction]
        public static ActionResult VerifyConnectionStrings(Session session)
        {
            // Default to not valid
            session[ConnectionStringsValidInput] = "0";

            // Read current value
            string databaseServerInputEntry = session[ConnectionStringsInput];
            if (string.IsNullOrEmpty(databaseServerInputEntry))
            {
                MessageBox.Show("Connection string must be specified.");
                return ActionResult.Success;
            }

            // Check if the user has write and delete privileges for the Installation directory
            HasRequiredPermissionOnDirectory(session[InstallDirectory], session);
            // Check if the user has write and delete privileges for the IO FileShare directory
            HasRequiredPermissionOnDirectory(session[IoFileSharePath], session);

            var servers = ParseDatabaseServerInputEntry(databaseServerInputEntry);

            // Update replication type selection
            RefreshReplicationTypeSelection(session, servers.Count, NonSnapshotReplicationType);

            foreach (var server in servers.Take(1)) // we now only verify the first connection string, because the database may automatically be created on satellite servers
            {
                try
                {
                    SqlConnectionStringBuilder connectionString = BuildConnectionString(server);
                    connectionString.ConnectTimeout = 5;
                    using (var connection = new SqlConnection(connectionString.ToString()))
                    {
                        connection.Open();
                    }
                }
                catch (Exception)
                {
                    string message = string.Format("Could not verify the connection string: {0}{1}.", 
                        Environment.NewLine, server);
                    MessageBox.Show(message);

                    return ActionResult.Success;
                }
            }

            MessageBox.Show("Connection strings verified.");
            session[ConnectionStringsValidInput] = "1";

            return ActionResult.Success;
        }

        /// <summary>
        ///   Verifies the username and password in the session against the FTP server. Bypasses the check if both are null.
        /// </summary>
        /// <param name="session"> </param>
        /// <returns> </returns>
        [CustomAction]
        public static ActionResult VerifyFtpInformation(Session session)
        {
            var userName = session["FTPUSERNAME"];
            var password = session["FTPPASSWORD"];
            var uri = new Uri("ftps://ftp.iopracticeware.com:990/");
            try
            {
                if (!(userName.ToLower().Equals("test") && password.ToLower().Equals("test")))
                {
                    using (var client = new FtpClient())
                    {
                        client.Host = uri.Host;
                        client.Credentials = new NetworkCredential(userName, password);
                        client.Port = uri.Port > 0 ? uri.Port : 21;
                        client.EncryptionMode = "ftps".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase) ? FtpEncryptionMode.Implicit : FtpEncryptionMode.None;
                        client.EnableThreadSafeDataConnections = false;
                        client.ValidateCertificate += (sender, e) => e.Accept = true;

                        client.Connect();
                    }
                }
            }
            catch (Exception)
            {
                string message = string.Format(
                    @"Could not verify the user name and password:
{0} {1}.", userName, password);

                MessageBox.Show(message);

                session["FTP_INFORMATION_VALID"] = "0";

                return ActionResult.Success;
            }

            var uiLevel = Convert.ToInt32(session["UILevel"]);
            if (uiLevel > 2)
                MessageBox.Show("User Name and Password verified.");

            session["FTP_INFORMATION_VALID"] = "1";

            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult ConfigureDatabasePermissions(Session session)
        {
            foreach (string connectionString in GetConfigFileConnectionStrings(session).Values)
            {
                if (string.IsNullOrEmpty(connectionString))
                {
                    session.Log(CustomActionException + string.Format("Could not find connection string named {0}.", PracticeRepositoryConnectionStringNamePrefix));
                    return ActionResult.Failure;
                }

                // The following will create the login user for the NetworkServiceAccount, LocalSystemAccount, and machine name if it doesnt exist.
                // It will then add the users to the "db_owner" users group if it isn't part of it.
                const string addDbOwnerRoleFormatString =
                    @"IF NOT EXISTS (SELECT name FROM master.sys.syslogins WHERE name = '{0}') BEGIN CREATE LOGIN [{0}] FROM WINDOWS END 
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = '{0}') BEGIN CREATE USER [{0}] FOR LOGIN [{0}] END 
exec sp_addrolemember 'db_owner', '{0}' ";

                string addDbOwnerRoleNetworkServiceAccountQuery = string.Format(addDbOwnerRoleFormatString, NetworkServiceAccount);
                string addDbOwnerRoleLocalSystemAccountQuery = string.Format(addDbOwnerRoleFormatString, LocalSystemAccount);
                string addDbOwnerRoleMachineAccountQuery = string.Format(addDbOwnerRoleFormatString, CurrentMachineAccount);

                // The following will create the login user for the LocalSystemAccount, if it doesnt exist.
                // It will then add the user as a "sysadmin" if it's not in that role.
                const string addSysAdminRoleFormatString =
                    @"IF NOT EXISTS (SELECT name FROM master.sys.syslogins WHERE name = '{0}') BEGIN CREATE LOGIN [{0}] FROM WINDOWS END 
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = '{0}') BEGIN CREATE USER [{0}] FOR LOGIN [{0}] END 
exec sp_addsrvrolemember '{0}', 'sysadmin' ";
                string addSysAdminRoleLocalSystemAccountQuery = string.Format(addSysAdminRoleFormatString, LocalSystemAccount);
                string addSysAdminRoleMachineAccountQuery = string.Format(addDbOwnerRoleFormatString, CurrentMachineAccount);

                var connectionBuilder = new SqlConnectionStringBuilder(connectionString);
                connectionBuilder.ConnectTimeout = 15;
                using (var connection = new SqlConnection(connectionBuilder.ConnectionString))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.Connection = connection;
                        try
                        {
                            connection.Open();
                        }
                        catch (Exception ex)
                        {
                            var exception = new Exception(string.Format("Invalid connection string \"{0}\"", connectionBuilder.ConnectionString), ex);
                            session.Log(CustomActionException + exception);
                            return ActionResult.Failure;
                        }

                        var queries = new List<string>(new[] { addSysAdminRoleLocalSystemAccountQuery, addDbOwnerRoleLocalSystemAccountQuery, addDbOwnerRoleNetworkServiceAccountQuery });
                        if (IsInDomain())
                        {
                            queries.AddRange(new[] { addDbOwnerRoleMachineAccountQuery, addSysAdminRoleMachineAccountQuery });
                        }

                        foreach (string query in queries)
                        {
                            command.CommandText = query;
                            try
                            {
                                command.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                var exception = new Exception(string.Format("Could not execute query \"{0}\"", query), ex);
                                session.Log(CustomActionException + exception);
                                return ActionResult.NotExecuted;
                            }
                        }
                    }
                }
            }

            return ActionResult.Success;
        }

        /// <summary>
        /// Adds the client information (id and name) to application settings table.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult AddClientInfoApplicationSettings(Session session)
        {
            const string sqlFormatString = @"
IF NOT EXISTS(SELECT * FROM model.ApplicationSettings WHERE Name = 'ClientName')
	INSERT INTO model.ApplicationSettings(Name, Value)
	VALUES('ClientName', '{0}')

IF NOT EXISTS(SELECT * FROM model.ApplicationSettings WHERE Name = 'ClientId')
	INSERT INTO model.ApplicationSettings(Name, Value)
	VALUES('ClientId', '{1}')";

            var clientName = session.CustomActionData["FTPUSERNAME"];

            var tokens = GetConfigFileAuthenticationTokens(session.CustomActionData[InstallDirectory]);

            if (tokens.Count == 1 && !string.IsNullOrWhiteSpace(clientName))
            {
                // this only works for not multi-tenant application servers
                var cs = GetConfigFileConnectionStrings(session)
                    .Where(i => i.Key == PracticeRepositoryConnectionStringNamePrefix)
                    .Select(i => i.Value).FirstOrDefault();

                if (!string.IsNullOrWhiteSpace(cs))
                {
                    var connectionBuilder = new SqlConnectionStringBuilder(cs);
                    connectionBuilder.ConnectTimeout = 15;

                    using (var connection = new SqlConnection(connectionBuilder.ConnectionString))
                    {
                        // use ascii to keep as small as possible and generate a unique client id
                        var clientId = new BigInteger(Encoding.ASCII.GetBytes(clientName));
                        var sql = string.Format(sqlFormatString, clientName, clientId);

                        connection.Open();

                        using (var command = connection.CreateCommand())
                        {
                            command.CommandText = sql;
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult AddApplicationServerApplicationSettings(Session session)
        {
            const string applicationServerUrlSqlFormatString = @"
DELETE FROM model.ApplicationSettings WHERE Name = 'ApplicationServerUrl'
INSERT INTO model.ApplicationSettings(Name, Value)
VALUES
('ApplicationServerUrl', '{0}')
";

            const string authenticationTokenSqlFormatString = @"
DELETE FROM model.ApplicationSettings WHERE Name = 'AuthenticationToken'

IF NOT EXISTS(SELECT * FROM model.ApplicationSettings WHERE Name = 'AuthenticationToken')
INSERT INTO model.ApplicationSettings(Name, Value)
VALUES
('AuthenticationToken', '{0}')
";

            var applicationServerUrlSql = string.Format(applicationServerUrlSqlFormatString, GetApplicationServerUrl());

            var tokens = GetConfigFileAuthenticationTokens(session.CustomActionData[InstallDirectory]);

            foreach (var connectionString in GetConfigFileConnectionStrings(session))
            {
                if (string.IsNullOrEmpty(connectionString.Value))
                {
                    session.Log(CustomActionException + string.Format("Could not find connection string named {0}.", PracticeRepositoryConnectionStringNamePrefix));
                    return ActionResult.Failure;
                }
                var connectionBuilder = new SqlConnectionStringBuilder(connectionString.Value);
                connectionBuilder.ConnectTimeout = 15;

                using (var connection = new SqlConnection(connectionBuilder.ConnectionString))
                {
                    connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = applicationServerUrlSql;
                        command.ExecuteNonQuery();

                        string authenticationToken;
                        if (tokens.TryGetValue(connectionString.Key, out authenticationToken))
                        {
                            command.CommandText = string.Format(authenticationTokenSqlFormatString, authenticationToken);
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            return ActionResult.Success;
        }

        private static string GetApplicationServerUrl()
        {
            var serverManager = new ServerManager();

            // Granting permissions to the Network Service for the IIS path
            var site = serverManager.Sites.First(s => s.GetCollection().Any(a => a["applicationPool"] as string == "IOPracticeware"));

            var app = site.Applications["/IOPracticeware"];

            if (app != null)
            {
                var binding = site.Bindings.FirstOrDefault(b => b.Protocol == "https" && b.BindingInformation == "*:443:") ?? site.Bindings.First();

                string hostAndPort = string.IsNullOrWhiteSpace(binding.Host) ? Environment.MachineName : binding.Host;

                string[] parts = binding.BindingInformation.Split(':');
                if (parts.Length == 3)
                {
                    hostAndPort += ":" + parts[1];
                }

                return string.Format("{0}://{1}/IOPracticeware", binding.Protocol, hostAndPort);
            }

            return string.Format("https://{0}/IOPracticeware", Environment.MachineName);
        }

        /// <summary>
        /// Starts monitoring service and awaits until it is started
        /// </summary>
        /// <remarks>! DEFERRED ACTION !</remarks>
        /// <param name="session"></param>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult StartMonitoringService(Session session)
        {
            var result = StartService(session, MonitoringServiceName);

            var uiLevel = Convert.ToInt32(session.CustomActionData["UILevel"]);
            if (result == ActionResult.Failure && uiLevel > 2) // INSTALLUILEVEL_NONE = 2
            {
                MessageBox.Show("Starting Monitoring Service failed. Please check Event Log and Services.", "IO Practiceware", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return result;
        }

        private static ActionResult StartService(Session session, string serviceName)
        {
            ServiceController serviceController = GetServiceController(serviceName);
            serviceController.Refresh();

            session.Log(string.Format("Service {0} state is {1}", serviceName, serviceController.Status));

            // Return if it's already running
            if (serviceController.Status == ServiceControllerStatus.Running)
            {
                return ActionResult.Success;
            }

            // Start the service if it's not already starting
            session.Log(string.Format("Starting service {0}.", serviceName));
            if (serviceController.Status != ServiceControllerStatus.StartPending)
            {
                serviceController.Start();
            }

            // Wait for a maximum of 1.5 minutes for a service to start
            serviceController.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(90));

            // Verify it's now in running state
            serviceController.Refresh();
            return serviceController.Status == ServiceControllerStatus.Running
                ? ActionResult.Success
                : ActionResult.Failure;
        }

        /// <summary>
        /// Gets the installation parameters (mostly from the client config file. Does not access the system config). Gets the installation directory parameter 
        /// by first checking if the monitoring service is installed (existing installation) and using that path as the Install Path.
        /// Stores other information in the session such as Connection String info 
        /// and appSettings from the config files.
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult GetInstallParamsFromCustomConfigFile(Session session)
        {
            try
            {
                string installDir;

                // Query monitoring service location
                string filter = String.Format("SELECT * FROM Win32_Service WHERE Name = '{0}'", MonitoringServiceName);
                var query = new ManagementObjectSearcher(filter);
                var service = query.Get().OfType<ManagementBaseObject>().FirstOrDefault();

                if (service != null)
                {
                    if (string.IsNullOrEmpty(session[AutoStartService]))
                    {
                        // Read auto start parameter
                        session[AutoStartService] = service.GetPropertyValue("StartMode").ToString() == "Auto" ? "1" : "0";
                    }

                    // Read directory where Monitoring Service is located
                    var invalidChars = new Regex(string.Format(CultureInfo.InvariantCulture, "[{0}]", Regex.Escape(new string(Path.GetInvalidPathChars()))));
                    var monitoringServiceDir = Path.GetDirectoryName(invalidChars
                        .Replace(service.GetPropertyValue("PathName").ToString(), string.Empty));

                    // Set install dir
                    installDir = Path.GetDirectoryName(monitoringServiceDir);
                    session.Log("Read current INSTALLDIR path: {0}.", installDir);
                    SafeSetTargetPath(session, InstallDirectory, installDir);
                }
                else
                {
                    // Locate program files folder
                    string installDirRoot = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
                    if (string.IsNullOrEmpty(installDirRoot) || !Directory.Exists(installDirRoot))
                    {
                        installDirRoot = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
                    }

                    // Set install dir
                    installDir = Path.Combine(installDirRoot, "IO Practiceware Server");
                    session.Log("New evaluated INSTALLDIR is {0}.", installDir);
                    SafeSetTargetPath(session, InstallDirectory, installDir);
                }

                // Locate custom config
                // ReSharper disable once AssignNullToNotNullAttribute
                string customConfigFilePath = Path.Combine(installDir, CustomConfigFile);
                session.Log("Custom config file is expected at {0}.", customConfigFilePath);

                if (!File.Exists(customConfigFilePath))
                {
                    if (string.IsNullOrEmpty(session[ConnectionStringsInput]))
                    {
                        session[ConnectionStringsInput] = "server:localhost,database:PracticeRepository";
                    }

                    session.Log(string.Format("Custom config file not found at: {0}.", customConfigFilePath));
                    return ActionResult.NotExecuted;
                }

                var xDoc = new XmlDocument();
                xDoc.Load(customConfigFilePath);

                
                string serverDataPath = null;

                // Read server data path from application server client only when single client is configured
                var clientNodes = xDoc.SelectNodes("//configuration/applicationServer/clients/client")
                    .Cast<XmlElement>()
                    .ToList();
                if (clientNodes.Count == 1)
                {
                    serverDataPath = clientNodes.First().GetAttribute("serverDataPath");
                }

                // Alternatively try to read server data path from app settings
                if (string.IsNullOrEmpty(serverDataPath))
                {
                    var shareDirNode = xDoc.SelectSingleNode("//configuration/appSettings/add[@key='ServerDataPath']") as XmlElement;
                    if (shareDirNode != null)
                    {
                        serverDataPath = shareDirNode.GetAttribute("value");
                    }
                }

                // If set -> use it for IOShareDir
                if (!string.IsNullOrEmpty(serverDataPath))
                {
                    SafeSetTargetPath(session, IoFileSharePath, Path.GetDirectoryName(serverDataPath));
                }

                //Get back connection string
                XmlNode connectionStringsNode = xDoc.SelectSingleNode("//configuration/connectionStrings");
                if (connectionStringsNode != null)
                {
                    // Build a list of connection strings specified in config
                    var practiceRepostioryConnectionStrings = connectionStringsNode.ChildNodes.OfType<XmlElement>().ToArray()
                        .Where(i => i.Attributes["name"] != null &&
                                    i.Attributes["name"].Value.StartsWith(PracticeRepositoryConnectionStringNamePrefix, StringComparison.OrdinalIgnoreCase))
                        .Select(i => i.Attributes["connectionString"])
                        .Where(a => a != null)
                        .Select(a => a.Value)
                        .ToArray();

                    // Read currently used replication type
                    string configuredReplicationType = NonSnapshotReplicationType;

                    var dbMigrations = xDoc.SelectNodes("//configuration/dbMigrations/dbMigration");
                    if (dbMigrations != null && dbMigrations.Count == 1)
                    {
                        bool isSnapshotReplication;
                        var snapshotReplication = dbMigrations.OfType<XmlElement>().First().GetAttribute("useSnapshotForReplication");
                        if (!string.IsNullOrEmpty(snapshotReplication)
                            && bool.TryParse(snapshotReplication, out isSnapshotReplication))
                        {
                            configuredReplicationType = isSnapshotReplication 
                                ? SnapshotReplicationType 
                                : NonSnapshotReplicationType;
                        }
                    }

                    // Refresh whether replication is possible
                    RefreshReplicationTypeSelection(session, practiceRepostioryConnectionStrings.Length, configuredReplicationType);

                    bool useRawConnectionStrings = false;
                    string connectionStrings = string.Empty;

                    foreach (string practiceRepostioryConnectionString in practiceRepostioryConnectionStrings)
                    {
                        var connection = new SqlConnectionStringBuilder(practiceRepostioryConnectionString);

                        // Connection string has UserId or Password specified -> short syntax not supported. Edit full connection string
                        if (!string.IsNullOrEmpty(connection.UserID) || !string.IsNullOrEmpty(connection.Password))
                        {
                            useRawConnectionStrings = true;
                            break;
                        }

                        connectionStrings += string.Format("server:{0},database:{1}{2}", connection.DataSource, connection.InitialCatalog, Environment.NewLine);
                    }

                    if (useRawConnectionStrings)
                    {
                        connectionStrings = string.Join(Environment.NewLine, practiceRepostioryConnectionStrings);
                    }

                    if (string.IsNullOrEmpty(session[ConnectionStringsInput]))
                    {
                        session[ConnectionStringsInput] = connectionStrings;

                        session.Log("GetInstallParamsFromCustomConfigFile: Recording existing connection strings " + connectionStrings);
                    }
                }

                //Get back email address
                var emailAddressNode = xDoc.SelectSingleNode("//configuration/appSettings/add[@key='AlertsDestinationEmailAddresses']") as XmlElement;

                if (emailAddressNode != null && string.IsNullOrEmpty(session["AlertsDestinationEmailAddresses"]))
                {
                    session[AlertsDestinationEmailAddresses] = emailAddressNode.GetAttribute("value");
                }


                var updateClientElement = (XmlElement)xDoc.SelectSingleNode("//configuration/updateMonitorConfiguration/updateClients/updateClient");
                session.Log("GetInstallParamsFromCustomConfigFile: The client element is " + (updateClientElement != null ? updateClientElement.OuterXml : ""));
                if (updateClientElement != null && (string.IsNullOrEmpty(session["FTPUSERNAME"]) || string.IsNullOrEmpty(session["FTPPASSWORD"])))
                {
                    if (!GetInstallParamsFromLegacyUpdateClientConfiguration(session, updateClientElement))
                    {
                        session["FTPUSERNAME"] = updateClientElement.GetAttribute("userName");
                        session["FTPPASSWORD"] = updateClientElement.GetAttribute("password");
                    }

                    session.Log("GetInstallParamsFromCustomConfigFile: The FTP username is " + session["FTPUSERNAME"] + " and the password is " + session["FTPPASSWORD"]);
                }
                return ActionResult.Success;
            }
            catch (Exception ex)
            {
                session.Log(ex.ToString());
                return ActionResult.NotExecuted;
            }
        }

        private static bool GetInstallParamsFromLegacyUpdateClientConfiguration(Session session, XmlElement updateClientElement)
        {
            session.Log("Existing updateClient element is " + (updateClientElement != null ? updateClientElement.OuterXml : ""));
            // Migrate legacy ftp infoTfs

            const string legacyNewVersionPrefix = "ftp://www.iopsupport.com/";
            if (updateClientElement != null && updateClientElement.GetAttribute("username") == "IOUpdate")
            {
                var ftpNewVersionPath = updateClientElement.GetAttribute("ftpNewVersionPath");
                if (ftpNewVersionPath.StartsWith(legacyNewVersionPrefix) && ftpNewVersionPath != legacyNewVersionPrefix)
                {
                    var clientName = ftpNewVersionPath.Split('/').Last(i => i != string.Empty);
                    session.Log("Migrating legacy updateClient for " + clientName);
                    var password = Resources.FtpAccounts.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).Where(l => l.StartsWith(clientName + ",", StringComparison.OrdinalIgnoreCase)).Select(l => l.Substring(clientName.Length + 1)).FirstOrDefault();
                    session.Log("Legacy updateClient password " + password);
                    if (!string.IsNullOrWhiteSpace(password))
                    {
                        session["FTPUSERNAME"] = clientName;
                        session["FTPPASSWORD"] = password;
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Configures MSDTC
        /// </summary>
        /// <param name="session"></param>
        /// <remarks>! DEFERRED ACTION !</remarks>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult ConfigureMsdtc(Session session)
        {
            var oldWowRedirect = new IntPtr(0);

            if (Environment.Is64BitOperatingSystem)
            {
                Wow64DisableWow64FsRedirection(ref oldWowRedirect);
                session.Log(String.Format("Disabled 64 bit file system redirection. Old Redirect: {0}.", oldWowRedirect));
            }

            try
            {
                // Enable DTC Network Access
                var msdtcScriptPath = Path.GetFullPath("MSDTC_Security.reg");
                File.WriteAllText(msdtcScriptPath, Resources.MSDTC_Security);
                session.Log("Extracted MSDTC_Security.reg to {0}", msdtcScriptPath);

                var startInfo = new ProcessStartInfo
                {
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    FileName = Environment.Is64BitOperatingSystem ? Path.GetDirectoryName(Environment.SystemDirectory) + "\\regedit.exe" : Environment.GetFolderPath(Environment.SpecialFolder.Windows) + "\\regedit.exe",
                    Arguments = string.Format(@"/s ""{0}""", msdtcScriptPath)
                };

                session.Log(String.Format("Running regedit from {0}", startInfo.FileName));
                var regeditProcess = new Process { StartInfo = startInfo };
                regeditProcess.Start();
                regeditProcess.WaitForExit();

                session.Log(string.Format("regedit for MSDTC_Security.reg returned {0}.", regeditProcess.ExitCode));
            }
            finally
            {
                if (Environment.Is64BitOperatingSystem)
                    Wow64RevertWow64FsRedirection(ref oldWowRedirect);
            }
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult RunDbMigration(Session session)
        {
            try
            {
                var migratorPath = Path.Combine(session.CustomActionData["DBMIGRATIONDIR"], "IO.Practiceware.DbMigration.exe");

                session.Log("Running migrator at {0}.", migratorPath);

                var process = new Process();
                var startInfo = new ProcessStartInfo();
                startInfo.FileName = migratorPath;
                startInfo.Arguments = "ALLOWSQLRESTART";
                startInfo.LoadUserProfile = false;
                startInfo.UseShellExecute = true;
                startInfo.Verb = "runas";
                startInfo.WindowStyle = ProcessWindowStyle.Normal;
                process.StartInfo = startInfo;
                process.Start();
                process.WaitForExit();

                // Succeeded?
                if (process.ExitCode == 0)
                {
                    return ActionResult.Success;
                }

                session.Log("Db Migration exited with error code {0}. Please review migration logs.", process.ExitCode);

                // Partially succeeded?
                var partiallySucceeded = process.ExitCode == 10000;

                var uiLevel = Convert.ToInt32(session.CustomActionData["UILevel"]);
                if (partiallySucceeded && uiLevel > 2) // INSTALLUILEVEL_NONE = 2
                {
                    MessageBox.Show("Db Migration only succeeded partially. Please review migration logs.", "IO Practiceware", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                return partiallySucceeded
                    ? ActionResult.Success
                    : ActionResult.Failure;
            }
            catch (Exception ex)
            {
                session.Log(ex.Message);
                return ActionResult.Failure;
            }
        }


        /// <summary>
        /// Updates the Tfs work item for installing the server
        /// with the Product version 
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult UpdateTfsBuildVersionNumber(Session session)
        {
            var alias = session["FTPUSERNAME"];
            var succeeded = false;
            session.Log(String.Format("Alias: {0}", alias ?? string.Empty));
            var version = session["ProductVersion"];
            session.Log(String.Format("Version: {0}", version ?? string.Empty));
            var installationDirectory = session[InstallDirectory];
            var logDirectory = Path.Combine(installationDirectory, "~Logs");

            session.Log("Looking for logs in: " + logDirectory);
            try
            {
                var logFile = !Directory.Exists(logDirectory) ? null :
                    new DirectoryInfo(logDirectory)
                    .GetFiles()
                    .Where(a => a.Name.StartsWith("migration", StringComparison.OrdinalIgnoreCase) && a.Extension.Equals(".log", StringComparison.OrdinalIgnoreCase))
                    .OrderByDescending(f => f.Name)
                    .FirstOrDefault();
                FileInfo zippedFile = null;
                if (logFile != null)
                {
                    succeeded = File.ReadAllText(logFile.FullName).Contains("Migration succeeded");
                    session.Log(String.Format("Succeeded: {0}", succeeded));
                    session.Log(new StringBuilder("Got log file: ").Append(Path.Combine(Path.GetTempPath(), logFile.Name)).Append(Environment.NewLine).ToString());
                    new FastZip().CreateZip(String.Format("{0}{1}", Path.Combine(Path.GetTempPath(), logFile.Name), ".zip"), logFile.DirectoryName, false, logFile.Name);
                    session.Log("Zipped File: " + logFile.FullName);
                    zippedFile = new DirectoryInfo(Path.GetTempPath()).GetFiles(String.Format("{0}{1}", logFile.Name, ".zip")).OrderBy(a => a.LastWriteTime).FirstOrDefault();
                }
                using (var uploader = new WebClient())
                {
                    if (zippedFile != null)
                    {
                        uploader.UploadFile(new Uri(
                            String.Format(
                                "http://releaserequest.iopracticeware.com/ReleaseRequest/TfsCompletion/UpdateTfsWithVersion/?alias={0}&version={1}&succeeded={2}",
                                alias, version, succeeded)), zippedFile.FullName);
                    }
                    else
                    {
                        if (logFile != null) session.Log(String.Format("Zipped File Not Found {0} {1}", Path.GetTempPath(), logFile.FullName));
                    }
                }
                if (zippedFile != null && File.Exists(zippedFile.FullName))
                {
                    File.Delete(zippedFile.FullName);
                    session.Log(new StringBuilder("Deleted file: ").Append(zippedFile.FullName).Append(Environment.NewLine).ToString());
                }
                session.Log(new StringBuilder("Successfully Changed TFS Ticket, and uploaded log file").Append(Environment.NewLine).ToString());

            }
            catch (Exception ex)
            {
                session.Log(String.Format("Could not update the Build Version Number: {3} on TFS for user: {0}{1}{2}", alias ?? string.Empty, Environment.NewLine, ex, version ?? string.Empty));
            }
            return ActionResult.Success;
        }

        private static List<InputDatabaseServer> ParseDatabaseServerInputEntry(string inputEntry)
        {
            var inputEntryItems = new List<InputDatabaseServer>();

            // server: localhost, database: PracticeRepository \n server: localhost2, database: PracticeRepository2
            string[] input = inputEntry.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string piece in input)
            {
                var inputDatabaseServer = new InputDatabaseServer();

                // server: localhost, database: PracticeRepository
                string[] values = piece.Split(',');
                foreach (string value in values)
                {
                    // server: localhost
                    if (value.ToLower().Contains("server"))
                    {
                        string[] entry = value.Split(new[] { ':' });
                        inputDatabaseServer.Server = entry[1].Trim();
                    }

                    // database: PracticeRepository
                    if (value.ToLower().Contains("database"))
                    {
                        string[] entry = value.Split(new[] { ':' });
                        inputDatabaseServer.Database = entry[1].Trim();
                    }
                }

                if (!string.IsNullOrWhiteSpace(inputDatabaseServer.Database) && !string.IsNullOrWhiteSpace(inputDatabaseServer.Server))
                    inputEntryItems.Add(inputDatabaseServer);
            }

            return inputEntryItems;
        }

        /// <summary>
        /// Refreshes whether replication type can be selected and provides default value.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <param name="serversCount">The servers count.</param>
        /// <param name="defaultValue">Default value to be used when allowing replication type configuration</param>
        private static void RefreshReplicationTypeSelection(Session session, int serversCount, string defaultValue)
        {
            session[ReplicationTypeInput] = serversCount > 1
                ? session[ReplicationTypeInput] == SkipConfiguringReplicationType
                    ? defaultValue
                    : session[ReplicationTypeInput]
                : SkipConfiguringReplicationType;
        }

        private static void AlterCustomConfigFileWithInputList(XmlDocument xDoc, List<InputDatabaseServer> inputDatabaseServerList)
        {
            XmlNode connectionStrings = xDoc.SelectSingleNode("//configuration/connectionStrings");

            var dbMigrations = xDoc.SelectNodes("//configuration/dbMigrations/dbMigration");

            // don't redefine connection strings if there is more than 1 dbmigration group (could be cloud envrionment)
            if (connectionStrings == null || dbMigrations == null || dbMigrations.Count != 1) return;

            foreach (XmlNode connectionString in connectionStrings.ChildNodes.OfType<XmlNode>().ToArray())
            {
                if (connectionString.Attributes != null)
                {
                    XmlAttribute name = connectionString.Attributes["name"];
                    if (name != null && name.Value.StartsWith(PracticeRepositoryConnectionStringNamePrefix, StringComparison.OrdinalIgnoreCase)) connectionStrings.RemoveChild(connectionString);
                }
            }

            var nameCount = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

            foreach (InputDatabaseServer x in inputDatabaseServerList)
            {
                string connString = BuildConnectionString(x).ToString();

                XmlElement add = xDoc.CreateElement("add");

                string name = nameCount.Count == 0 
                    ? PracticeRepositoryConnectionStringNamePrefix 
                    : x.Database.StartsWith(PracticeRepositoryConnectionStringNamePrefix) 
                        ? x.Database
                        : PracticeRepositoryConnectionStringNamePrefix + x.Database;
                int count;
                nameCount.TryGetValue(name, out count);
                nameCount[name] = ++count;

                if (count > 1) name = name + count;

                add.SetAttribute("name", name);
                add.SetAttribute("connectionString", connString);
                add.SetAttribute("providerName", "AdoServiceProvider");
                connectionStrings.AppendChild(add);
            }

            XmlNode migrationConnectionStrings = xDoc.SelectSingleNode("//configuration/dbMigrations/dbMigration/migrationConnectionStrings");
            if (migrationConnectionStrings != null)
            {
                migrationConnectionStrings.RemoveAll();

                nameCount = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

                foreach (InputDatabaseServer x in inputDatabaseServerList)
                {
                    XmlElement add = xDoc.CreateElement("add");

                    string name = nameCount.Count == 0
                        ? PracticeRepositoryConnectionStringNamePrefix
                        : x.Database.StartsWith(PracticeRepositoryConnectionStringNamePrefix)
                            ? x.Database
                            : PracticeRepositoryConnectionStringNamePrefix + x.Database;
                    int count;
                    nameCount.TryGetValue(name, out count);
                    nameCount[name] = ++count;

                    if (count > 1) name = name + count;


                    add.InnerText = name;
                    migrationConnectionStrings.AppendChild(add);
                }
            }
        }

        /// <summary>
        ///   Builds a connection string for an input database server.
        /// </summary>
        /// <param name="inputDatabaseServer"> The input database server. </param>
        /// <returns> </returns>
        private static SqlConnectionStringBuilder BuildConnectionString(InputDatabaseServer inputDatabaseServer)
        {
            return new SqlConnectionStringBuilder(@"Data Source=" + inputDatabaseServer.Server + ";Initial Catalog=" + inputDatabaseServer.Database + ";Integrated Security=True;Connection Timeout=1200;MultipleActiveResultSets=True");
        }

        private static void SafeSetTargetPath(Session session, string directory, string value)
        {
            var currentValue = "<failed to retrieve>";
            try
            {
                // Ensure directory path ends with "\"
                if (!value.EndsWith(@"\"))
                {
                    value += @"\";
                }

                // Get current value
                currentValue = session.GetTargetPath(directory);

                if (string.Equals(currentValue, value, StringComparison.OrdinalIgnoreCase))
                {
                    session.Log("Target path {0} is already set to value {1}", directory, currentValue);
                    // Nothing to update
                    return;
                }

                // Cannot modify target path upon uninstall
                if (session.EvaluateCondition("REMOVE=\"ALL\"", false))
                {
                    session.Log("Target path {0} cannot be modified upon uninstall. Current value: {1}. Suggested value: {2}", directory, currentValue, value);
                    return;
                }

                // Ensure it's not a file path
                if (File.Exists(value))
                {
                    session.Log("Target path {0} cannot represent a file path. Current value: {1}. Suggested value: {2}", directory, currentValue, value);
                    return;
                }

                // Directory must exist before setting it as target path
                if (!Directory.Exists(value))
                {
                    Directory.CreateDirectory(value);
                }

                // Update target path
                session.SetTargetPath(directory, value);
            }
            catch (Exception ex)
            {
                session.Log("Setting target path {0} failed. Current value: {1}. Suggested value: {2}. Error: {3}", directory, currentValue, value, ex);
            }
        }

        /// <summary>
        /// Used to check if the user has write or delete privileges in the folder specified
        /// </summary>
        /// <param name="currentValue"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        private static void HasRequiredPermissionOnDirectory(string currentValue, Session session)
        {
            try
            {
                if (Directory.Exists(currentValue))
                {
                    Directory.GetAccessControl(currentValue);
                }
            }
            catch (UnauthorizedAccessException uae)
            {
                // let the user know that they cannot write to that folder, and put it in the log
                if ((session.CustomActionData != null) && !string.IsNullOrEmpty(session.CustomActionData["UILevel"]))
                {
                    if (Convert.ToInt32(session.CustomActionData["UILevel"]) <= 2)
                    {
                        MessageBox.Show(String.Format("User does not have full access to path {0}", currentValue));
                    }
                }
                session.Log(String.Format("User does not have full access to path {0}. Full Error: {1}", currentValue, uae));
            }
            catch (Exception e)
            {
                // let the user know that an exception was caught
                if ((session.CustomActionData != null) && !string.IsNullOrEmpty(session.CustomActionData["UILevel"]))
                {
                    if (Convert.ToInt32(session.CustomActionData["UILevel"]) <= 2)
                    {
                        MessageBox.Show(String.Format("Could not verfiy permissions on {0}. Error: {1}", currentValue, e));
                    }
                }
                session.Log(String.Format("Could not verfiy permissions on {0}. Error: {1}", currentValue, e));
            }
        }

        #region Nested type: InputDatabaseServer

        private class InputDatabaseServer
        {
            public string Server { get; set; }
            public string Database { get; set; }

            public override string ToString()
            {
                return string.Format("Server: {0}. Database: {1}", Server, Database);
            }
        }

        #endregion

        #region Nested type: WKSTA_INFO_100

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        // ReSharper disable InconsistentNaming
        private class WKSTA_INFO_100
        // ReSharper restore InconsistentNaming
        {
#pragma warning disable 169
#pragma warning disable 649
            public int wki100_platform_id;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string wki100_computername;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string wki100_langroup;
            public int wki100_ver_major;
            public int wki100_ver_minor;
#pragma warning restore 169
#pragma warning restore 649
        }

        #endregion

        #region Nested type: Win32

        internal class Win32
        {
            #region NetJoinStatus enum

            public enum NetJoinStatus
            {
                NetSetupUnknownStatus = 0,
                NetSetupUnjoined,
                NetSetupWorkgroupName,
                NetSetupDomainName
            }

            #endregion

            public const int ErrorSuccess = 0;

            [DllImport("Netapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            public static extern int NetGetJoinInformation(string server, out IntPtr domain, out NetJoinStatus status);
        }

        #endregion
    }
}