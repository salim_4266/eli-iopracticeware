﻿using System.Data.SqlClient;
using Microsoft.Deployment.WindowsInstaller;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Xml;
using Microsoft.Win32;

namespace IO.Practiceware.Installation
{
    public static class ClientSetupCustomActions
    {
        private const string CustomActionException = "CustomActionException: ";
        private const string AdminShortcutName = "Admin.lnk";
        private const string ExamShortcutName = "Exam.lnk";
        private const string PatientShortcutName = "Patient.lnk";
        private const string PinpointLog = "Pinpoint.log";
        private const string EmulationSettingsRegistryKey = "SOFTWARE\\Microsoft\\Internet Explorer\\MAIN\\FeatureControl\\FEATURE_BROWSER_EMULATION";
        private const string TestClientExe = "IO.Practiceware.TestClient.exe";
        private const string AdministratorExe = "Administrator.exe";
        private const string DoctorInterfaceExe = "DoctorInterface.exe";

        private const string CustomConfigRelativePath = @"IO Practiceware\IO.Practiceware.Custom.config";

        [DllImport("kernel32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern bool Wow64DisableWow64FsRedirection(ref IntPtr ptr);

        [DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern bool Wow64RevertWow64FsRedirection(ref IntPtr ptr);

        [CustomAction]
        public static ActionResult ConfigurePlatformIndependentRegistrySettings(Session session)
        {
            var oldWowRedirect = new IntPtr(0);

            if (Environment.Is64BitOperatingSystem)
            {
                Wow64DisableWow64FsRedirection(ref oldWowRedirect);
                session.Log(String.Format("Disabled 64 bit file system redirection. Old Redirect: {0}.", oldWowRedirect));
            }


            try
            {
                var startInfo = new ProcessStartInfo
                                    {
                                        FileName = Environment.GetFolderPath(Environment.SpecialFolder.Windows) + "\\regedit.exe",
                                        CreateNoWindow = true,
                                        UseShellExecute = false
                                    };

                session.Log(String.Format("Using regedit from {0}", Environment.GetFolderPath(Environment.SpecialFolder.Windows) + "\\regedit.exe"));
                // Enable DTC Network Access
                File.WriteAllText("MSDTC_Security.reg", Resources.MSDTC_Security);

                session.Log("Extracted MSDTC_Security.reg to {0}", Path.GetFullPath("MSDTC_Security.reg"));

                startInfo.Arguments = "/s \"" + Path.GetFullPath("MSDTC_Security.reg") + "\"";

                var regeditProcess = new Process { StartInfo = startInfo };
                regeditProcess.Start();
                regeditProcess.WaitForExit();

                session.Log(String.Format("regedit for MSDTC_Security.reg returned {0}.", regeditProcess.ExitCode));
            }
            finally
            {
                if (Environment.Is64BitOperatingSystem)
                    Wow64RevertWow64FsRedirection(ref oldWowRedirect);
            }
            return ActionResult.Success;
        }

        /// <summary>
        ///   Gets the paths to the desktop shortcuts from the older installer.
        ///   The older installer added the desktop shortcuts to user specific desktops, rather then "All Users".
        /// </summary>
        /// <param name="session"> </param>
        /// <returns> </returns>
        [CustomAction]
        public static ActionResult DeleteLegacyShortcuts(Session session)
        {
            string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            try
            {
                string shortcutPath = Path.Combine(desktopDirectory, AdminShortcutName);
                if (File.Exists(shortcutPath))
                {
                    File.Delete(shortcutPath);
                }

                shortcutPath = Path.Combine(desktopDirectory, ExamShortcutName);
                if (File.Exists(shortcutPath))
                {
                    File.Delete(shortcutPath);
                }

                shortcutPath = Path.Combine(desktopDirectory, PatientShortcutName);
                if (File.Exists(shortcutPath))
                {
                    File.Delete(shortcutPath);
                }
            }
            catch (Exception ex)
            {
                session.Log(String.Format("{0}Could not delete the legacy shortcuts at the directory {1} due to the following exception, {2}", CustomActionException, desktopDirectory, ex));
                return ActionResult.Failure;
            }

            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult BoostrapCustomConfig(Session session)
        {
            var result = ActionResult.NotExecuted;

            // Locate primary config
            var primaryConfigLocation = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                CustomConfigRelativePath);
            var backupConfigLocation = primaryConfigLocation + ".bak";

            // Locate shared config
            var checkConfigInFolder = new Func<string, string>(configPath => File.Exists(configPath) ? configPath : null);

            var bootstrapConfigLocation =
                checkConfigInFolder(backupConfigLocation) // v8.1 backup location
                ?? checkConfigInFolder(Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), // v8.0 location
                    CustomConfigRelativePath))
                ?? checkConfigInFolder(Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), // v7.31 location
                    CustomConfigRelativePath))
                ?? checkConfigInFolder(Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), // 64bit v7.31 location
                    CustomConfigRelativePath));

            var primaryExists = File.Exists(primaryConfigLocation);
            if (!primaryExists)
            {
                session.Log("Configuration not found at: {0}", primaryConfigLocation);
            }
            else
            {
                // Create backup to restore configuration file if it gets removed by installation
                try
                {
                    File.Copy(primaryConfigLocation, backupConfigLocation, true);
                }
                catch (Exception ex)
                {
                    session.Log("Failed to create configuration file backup at: {0}. Error: {1}", backupConfigLocation, ex);
                }
            }

            // If bootstrap config exists -> copy it to local location, so it is used from there
            if (!primaryExists && !String.IsNullOrEmpty(bootstrapConfigLocation))
            {
                // This CustomAction executes very early when IO folder doesn't yet exist
                var ioFolderInAppData = Path.GetDirectoryName(primaryConfigLocation);
                if (!String.IsNullOrEmpty(ioFolderInAppData) && !Directory.Exists(ioFolderInAppData))
                {
                    Directory.CreateDirectory(ioFolderInAppData);
                }

                session.Log("Bootstrapping configuration from {0}", bootstrapConfigLocation);
                File.Copy(bootstrapConfigLocation, primaryConfigLocation);
                result = ActionResult.Success;
            }

            return result;
        }

        [CustomAction]
        public static ActionResult UpdateClientConfigFile(Session session)
        {
            var result = ActionResult.Success;

            try
            {
                if (session == null)
                {
                    throw new ArgumentNullException("session");
                }

                var customConfigFilePath = Path.Combine(Environment.GetFolderPath(
                    Environment.SpecialFolder.LocalApplicationData),
                    CustomConfigRelativePath);

                session.Log("CA: Custom Config File Path: " + customConfigFilePath);
                if (!File.Exists(customConfigFilePath)) return ActionResult.NotExecuted;

                var xDoc = new XmlDocument();
                xDoc.Load(customConfigFilePath);

                SetTimeoutInConnectionString(xDoc);

                SetProviderInConnectionString(xDoc);

                AddClientCloudSettings(session, xDoc);

                RemoveObsoleteCloudSettings(xDoc, true);

                RemoveMessagingConfigurationNode(xDoc, session);

                xDoc.Save(customConfigFilePath);

                RemovePinpointLog(session);

            }
            catch (Exception ex)
            {
                result = ActionResult.Failure;
                if (session != null)
                {
                    session.Log(CustomActionException + ex);
                }
            }

            return result;
        }


        [CustomAction]
        public static ActionResult CreateRegistryKeyForInternetExplorerEmulation(Session session)
        {
            var result = ActionResult.Success;

            var registryKey = Registry.LocalMachine.OpenSubKey(EmulationSettingsRegistryKey, true);
            if (registryKey != null)
            {
                var testClientValue = registryKey.GetValue(TestClientExe);
                if (testClientValue == null)
                {
                    registryKey.SetValue(TestClientExe, 9999, RegistryValueKind.DWord);
                }
                var administratorValue = registryKey.GetValue(AdministratorExe);
                if (administratorValue == null)
                {
                    registryKey.SetValue(AdministratorExe, 9999, RegistryValueKind.DWord);
                }
                var doctorInterfaceValue = registryKey.GetValue(DoctorInterfaceExe);
                if (doctorInterfaceValue == null)
                {
                    registryKey.SetValue(DoctorInterfaceExe, 9999, RegistryValueKind.DWord);
                }

            }
            return result;
        }
        private static void SetProviderInConnectionString(XmlDocument xDoc)
        {
            XmlNode connectionStrings = xDoc.SelectSingleNode("//configuration/connectionStrings");
            if (connectionStrings != null)
            {
                foreach (XmlElement connectionString in connectionStrings.ChildNodes.OfType<XmlElement>().Where(e => e.GetAttribute("name").StartsWith("PracticeRepository")).ToArray())
                {
                    connectionString.SetAttribute("providerName", "AdoServiceProvider");
                }
            }
        }

        private static void RemovePinpointLog(Session session)
        {
            try
            {
                var pinpointLogPath = Path.Combine(session["PinpointDirectory"], PinpointLog);
                if (File.Exists(pinpointLogPath))
                {
                    File.Delete(pinpointLogPath);
                }
            }
            catch (Exception ex)
            {
                session.Log(ex.Message);
            }
        }

        /// <summary>
        /// Sets the default timeout in connection string if supported and one is not already present.
        /// </summary>
        /// <param name="xDoc">The x document.</param>
        private static void SetTimeoutInConnectionString(XmlDocument xDoc)
        {
            XmlNode connectionStrings = xDoc.SelectSingleNode("//configuration/connectionStrings");
            if (connectionStrings != null)
            {
                foreach (XmlNode connectionString in connectionStrings.ChildNodes.OfType<XmlNode>().ToArray())
                {
                    if (connectionString.Attributes != null)
                    {
                        XmlAttribute connectString = connectionString.Attributes["connectionString"];

                        const string connectTimeout = "Connect Timeout=";

                        SqlConnectionStringBuilder csb = TryGetSqlConnectionStringBuilder(connectionString.Value);

                        if (connectString != null && csb != null && !String.IsNullOrEmpty(csb.DataSource) && !String.IsNullOrEmpty(csb.InitialCatalog) && !csb.ShouldSerialize("Connection Timeout"))
                        {
                            if (!connectString.Value.Contains(connectTimeout))
                            {
                                connectString.Value = connectString.Value.TrimEnd(new[] { ';' }) + ";" + connectTimeout + "1200";
                            }
                            else
                            {
                                connectString.Value = Regex.Replace(connectString.Value, "(Connect Timeout\\s*=\\s*\\d*)", connectTimeout + "1200");
                            }
                        }
                    }
                }
            }
        }

        [DebuggerNonUserCode]
        private static SqlConnectionStringBuilder TryGetSqlConnectionStringBuilder(string value)
        {
            try
            {
                return new SqlConnectionStringBuilder(value);
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// Removes the Messaging configuration from the Custom Config file for the server installer
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="session"></param>
        private static void RemoveMessagingConfigurationNode(XmlDocument xDoc, Session session)
        {
            try
            {
                XmlNode configNode = xDoc.SelectSingleNode("//configuration");
                if (configNode != null)
                {
                    //Remove End Points in Service Model Node
                    XmlNode messagingNode = xDoc.SelectSingleNode("//configuration/messagingConfiguration");
                    session.Log("Configuration Node Found In Client Custom Config File");

                    if (messagingNode != null)
                    {
                        configNode.RemoveChild(messagingNode);
                        session.Log("Messaging Node Removed from Client Custom Config File");
                    }
                    else
                    {
                        session.Log("Messaging Node Not found in Client Custom Config File");
                    }
                }
                else
                {
                    session.Log("Configuration Node Not Found In Client Custom Config File");
                }
            }
            catch (Exception ex)
            {
                session.Log(ex.Message);
            }
        }


        private static void AddClientCloudSettings(Session session, XmlDocument xDoc)
        {
            XmlNode appSettingsNode = xDoc.SelectSingleNode("//configuration/appSettings");
            if (appSettingsNode != null)
            {
                //ServiceLocationShortName
                var siteShortNameNode = (XmlElement)xDoc.SelectSingleNode("//configuration/appSettings/add[@key='SiteShortName']");
                if (siteShortNameNode != null && siteShortNameNode.ParentNode != null)
                {
                    siteShortNameNode.ParentNode.RemoveChild(siteShortNameNode);
                }

                var serviceLocationShortNameNode = (XmlElement)xDoc.SelectSingleNode("//configuration/appSettings/add[@key='ServiceLocationShortName']");
                if (serviceLocationShortNameNode != null && serviceLocationShortNameNode.ParentNode != null)
                {
                    serviceLocationShortNameNode.ParentNode.RemoveChild(serviceLocationShortNameNode);
                }


                var pinpointLogPath = Path.Combine(session["PinpointDirectory"], PinpointLog);
                if (File.Exists(pinpointLogPath))
                {
                    var lines = File.ReadAllLines(pinpointLogPath);
                    if (lines.Length >= 5)
                    {
                        var serviceLocationShortName = lines[4].Trim();
                        if (!serviceLocationShortName.Trim().Equals(serviceLocationShortNameNode.GetAttribute("value"), StringComparison.OrdinalIgnoreCase))
                        {
                            serviceLocationShortNameNode.SetAttribute("value", serviceLocationShortName);
                        }
                    }
                }
            }

            var configNode = xDoc.SelectSingleNode("//configuration");
            if (configNode != null)
            {
                //clientApplicationConfiguration (add it)
                var clientApplicationConfigurationNode = (XmlElement)xDoc.SelectSingleNode("//configuration/clientApplicationConfiguration");
                if (clientApplicationConfigurationNode == null)
                {
                    clientApplicationConfigurationNode = xDoc.CreateElement("clientApplicationConfiguration");
                    configNode.AppendChild(clientApplicationConfigurationNode);
                }

                //applicationServerClient
                var applicationServerClientNode = (XmlElement)xDoc.SelectSingleNode("//configuration/applicationServerClient");
                if (applicationServerClientNode == null)
                {
                    applicationServerClientNode = xDoc.CreateElement("applicationServerClient");
                    applicationServerClientNode.SetAttribute("bindingType", "basicHttpBinding");
                    applicationServerClientNode.SetAttribute("bindingConfiguration", "HttpsBinding");
                    applicationServerClientNode.SetAttribute("behaviorConfiguration", "ProtoEndpointBehavior");
                    applicationServerClientNode.SetAttribute("servicesUriFormat", "https://localhost/IOPracticeware/Services/{0}/Protobuf");
                    applicationServerClientNode.SetAttribute("authenticationToken", "");
                    applicationServerClientNode.SetAttribute("remoteFileService", "false");
                    applicationServerClientNode.SetAttribute("isEnabled", "false");
                    configNode.AppendChild(applicationServerClientNode);
                }

            }
        }

        private static void RemoveObsoleteCloudSettings(XmlDocument xDoc, Boolean isClient)
        {
            XmlNode appSettingsNode = xDoc.SelectSingleNode("//configuration/appSettings");

            if (appSettingsNode != null)
            {
                var servicesBaseUriNode = (XmlElement)xDoc.SelectSingleNode("//configuration/appSettings/add[@key='ServicesBaseUri']");

                if (servicesBaseUriNode != null) appSettingsNode.RemoveChild(servicesBaseUriNode);

                if (isClient)
                {
                    var authenticationTokenNode = (XmlElement)xDoc.SelectSingleNode("//configuration/appSettings/add[@key='AuthenticationToken']");

                    if (authenticationTokenNode != null) appSettingsNode.RemoveChild(authenticationTokenNode);
                }

                var generateEndpointsNode = (XmlElement)xDoc.SelectSingleNode("//configuration/appSettings/add[@key='GenerateEndpoints']");

                if (generateEndpointsNode != null) appSettingsNode.RemoveChild(generateEndpointsNode);
            }

            XmlNode configNode = xDoc.SelectSingleNode("//configuration");
            if (configNode != null)
            {
                //Remove End Points in Service Model Node
                XmlNode serviceModelNode = xDoc.SelectSingleNode("//configuration/system.serviceModel");

                if (serviceModelNode != null) serviceModelNode.RemoveAll();

                //Remove applicationServerClient node
                var applicationServerClientNode = (XmlElement)xDoc.SelectSingleNode("//configuration/applicationServerClient");
                if (applicationServerClientNode != null)
                {
                    string servicesUriFormat = applicationServerClientNode.GetAttribute("servicesUriFormat");
                    if (servicesUriFormat == "https://localhost/IOPracticeware/Services/{0}/Protobuf") applicationServerClientNode.SetAttribute("isEnabled", "false");
                }
            }
        }
    }
}
