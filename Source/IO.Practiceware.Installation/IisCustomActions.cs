﻿namespace IO.Practiceware.Installation
{
    using Microsoft.Deployment.WindowsInstaller;
    using Microsoft.Web.Administration;
    using Microsoft.Win32;
    using System;
    using System.Diagnostics;
    using System.Collections.Generic;
    using System.DirectoryServices;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Security.Cryptography.X509Certificates;

    public static class IisCustomActions
    {
        internal const string IisEntry = "IIS://localhost/W3SVC";
        private const string ServerComment = "ServerComment";
        private const string CustomActionException = "CustomActionException: ";
        private const string IisRegKey = @"Software\Microsoft\InetStp";
        private const string IisRegKeyMajorVersion = "MajorVersion";
        private const string IisRegKeyMinorVersion = "MinorVersion";
        private const string IisWebServer = "iiswebserver";
        private static readonly Version WindowsServer2012Version = new Version(6, 2);

        /// <summary>
        /// Gets all registered IIS websites on the computer and either selected first one or one with IO Practiceware application.
        /// Selection is performed by setting WEBSITE_ID, WEBSITE_PATH and WEBSITE_DESCRIPTION properties (see Setup.wxs and Files.wxs)
        /// </summary>
        /// <param name="session"></param>
        /// <remarks>IMPORTANT: SKIPPED FOR NOW. On Windows 8.1 access to IIS configuration requires elevated context.
        /// This means action must run deferred, but deferred means we have no access to Session object
        /// </remarks>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult SelectDefaultInstallationWebsite(Session session)
        {
            ActionResult result;

            try
            {
                if (session == null)
                {
                    throw new ArgumentNullException("session");
                }

                // Get list of websites
                var websites = GetIisMajorVersion() > 7
                    ? GetWebSitesViaWebAdministration(session)
                    : GetWebSitesViaMetabase(session);

                // Select default site or site which hosts IO app
                var defaultOrIOSite = websites.FirstOrDefault(ws => ws.HostsIOSite)
                                      ?? websites.First();

                session.Log("Selected web site {0} {1} {2} {3}", defaultOrIOSite.Id,
                    defaultOrIOSite.Description, defaultOrIOSite.Path, defaultOrIOSite.HostsIOSite);

                // Init website search variables
                session["WEBSITE_ID"] = defaultOrIOSite.Id;
                session["WEBSITE_DESCRIPTION"] = defaultOrIOSite.Description;
                session["WEBSITE_PATH"] = defaultOrIOSite.Path;

                result = ActionResult.Success;
            }
            catch (Exception ex)
            {
                if (session != null)
                {
                    session.Log(CustomActionException + ex);
                }
                throw;
            }

            return result;
        }

        /// <summary>
        /// Ensures IIS is installed on current machine
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult InstallIis(Session session)
        {
            bool success = true;

            Func<string, string, bool> runIisInstall = (processName, arguments) =>
            {
                var startInfo = new ProcessStartInfo();
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = true;
                startInfo.Verb = "runas";
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = string.Format("/c {0} {1}", processName, arguments);

                session.Log(string.Format("Running {0} {1}", processName, arguments));
                try
                {
                    using (var process = Process.Start(startInfo))
                    {
                        process.WaitForExit();
                        process.Close();
                        process.Dispose();
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    var exception = new Exception(string.Format("Could not start process {0}", processName), ex);
                    session.Log(exception.ToString());
                    return false;
                }
            };

            try
            {
                if (session == null) { throw new ArgumentNullException("session"); }

                if (IsIIs6Required()) // TODO: Add check to determine if all required components are already installed (Can't just check on version)
                {
                    const string installScript =
                                                @"[Components]
                                                aspnet = ON
                                                complusnetwork = ON
                                                dtcnetwork = ON
                                                bitsserverextensionsisapi = ON
                                                bitsserverextensionsmanager = ON
                                                iis_common = ON
                                                iis_ftp = ON
                                                fp_extensions = ON
                                                iis_inetmer = ON
                                                iis_nntp = ON
                                                iis_smtp = ON
                                                iis_asp = ON
                                                iis_internetdataconnector = ON
                                                sakit_web = ON
                                                tswebclient = ON
                                                iis_serversideincludes = ON
                                                iis_webdav = ON
                                                iis_www = ON
                                                inetprint = ON";

                    // Prepare install script
                    var iis6InstallScriptPath = Path.GetFullPath("Iis6InstallScript.txt");
                    File.WriteAllText(iis6InstallScriptPath, installScript);

                    // Run install
                    string commandLineArgs = string.Format(@"/i:sysoc.inf /u:""{0}""", iis6InstallScriptPath);
                    var exePath = Environment.ExpandEnvironmentVariables(@"%WINDIR%\SysNative\Sysocmgr.exe");
                    if (!File.Exists(exePath)) exePath = "Sysocmgr.exe";
                    success = runIisInstall(exePath, commandLineArgs);

                    // Cleanup file
                    File.Delete(iis6InstallScriptPath);
                }
                else if (IsIIs7Required()) // TODO: Add check to determine if all required components are already installed (Can't just check on version)
                {
                    #region helper functions - tryInstallWithDism and tryInstallWithServerManagerCmd
                    Func<bool> tryInstallWithDism = () =>
                                                    {
                                                        session.Log("Attempting Install of IIS >=7 using DISM");
                                                        string exePath = string.Empty;
                                                        string winDir = Environment.GetEnvironmentVariable("windir");
                                                        if (string.IsNullOrEmpty(winDir))
                                                        {
                                                            var ex = new NullReferenceException("Could not find the environmental variable, 'windir'.");
                                                            session.Log(CustomActionException + ex);
                                                        }
                                                        else
                                                        {
                                                            exePath = Environment.ExpandEnvironmentVariables(@"%WINDIR%\SysNative\dism.exe");
                                                        }
                                                        if (!File.Exists(exePath)) exePath = "dism.exe";

                                                        OsVersionInfo os = GetOsVersionInfo();

                                                        //Only Windows Server 2012 and up support '/all' switch
                                                        string commandLineArgs = new Version(os.dwMajorVersion, os.dwMinorVersion) >= WindowsServer2012Version ? @"/Online /NoRestart /Enable-Feature /all /FeatureName:IIS-ApplicationDevelopment /FeatureName:IIS-ASP /FeatureName:IIS-ASPNET /all /FeatureName:IIS-ASPNET45 /all /FeatureName:IIS-BasicAuthentication /FeatureName:IIS-CGI /FeatureName:IIS-ClientCertificateMappingAuthentication /FeatureName:IIS-CommonHttpFeatures /FeatureName:IIS-CustomLogging /FeatureName:IIS-DefaultDocument /FeatureName:IIS-DigestAuthentication /FeatureName:IIS-DirectoryBrowsing /FeatureName:IIS-FTPExtensibility /FeatureName:IIS-FTPServer /FeatureName:IIS-FTPSvc /FeatureName:IIS-HealthAndDiagnostics /FeatureName:IIS-HostableWebCore /FeatureName:IIS-HttpCompressionDynamic /FeatureName:IIS-HttpCompressionStatic /FeatureName:IIS-HttpErrors /FeatureName:IIS-HttpLogging /FeatureName:IIS-HttpRedirect /FeatureName:IIS-HttpTracing /FeatureName:IIS-IIS6ManagementCompatibility /FeatureName:IIS-IISCertificateMappingAuthentication /FeatureName:IIS-IPSecurity /FeatureName:IIS-ISAPIExtensions /FeatureName:IIS-ISAPIFilter /FeatureName:IIS-LegacyScripts /FeatureName:IIS-LegacySnapIn /FeatureName:IIS-LoggingLibraries /FeatureName:IIS-ManagementConsole /FeatureName:IIS-ManagementScriptingTools /FeatureName:IIS-ManagementService /FeatureName:IIS-Metabase /FeatureName:IIS-NetFxExtensibility /FeatureName:IIS-ODBCLogging /FeatureName:IIS-Performance /FeatureName:IIS-RequestFiltering /FeatureName:IIS-RequestMonitor /FeatureName:IIS-Security /FeatureName:IIS-ServerSideIncludes /FeatureName:IIS-StaticContent /FeatureName:IIS-URLAuthorization /FeatureName:IIS-WebDAV /FeatureName:IIS-WebServer /FeatureName:IIS-WebServerManagementTools /FeatureName:IIS-WebServerRole /FeatureName:IIS-WindowsAuthentication /FeatureName:IIS-WMICompatibility /FeatureName:WAS-ConfigurationAPI /FeatureName:WAS-NetFxEnvironment /FeatureName:WAS-ProcessModel /FeatureName:WAS-WindowsActivationService /FeatureName:IIS-ApplicationInit"
                                                                                    : @"/Online /NoRestart /Enable-Feature /FeatureName:IIS-ApplicationDevelopment /FeatureName:IIS-ASP /FeatureName:IIS-ASPNET /FeatureName:IIS-BasicAuthentication /FeatureName:IIS-CGI /FeatureName:IIS-ClientCertificateMappingAuthentication /FeatureName:IIS-CommonHttpFeatures /FeatureName:IIS-CustomLogging /FeatureName:IIS-DefaultDocument /FeatureName:IIS-DigestAuthentication /FeatureName:IIS-DirectoryBrowsing /FeatureName:IIS-FTPExtensibility /FeatureName:IIS-FTPServer /FeatureName:IIS-FTPSvc /FeatureName:IIS-HealthAndDiagnostics /FeatureName:IIS-HostableWebCore /FeatureName:IIS-HttpCompressionDynamic /FeatureName:IIS-HttpCompressionStatic /FeatureName:IIS-HttpErrors /FeatureName:IIS-HttpLogging /FeatureName:IIS-HttpRedirect /FeatureName:IIS-HttpTracing /FeatureName:IIS-IIS6ManagementCompatibility /FeatureName:IIS-IISCertificateMappingAuthentication /FeatureName:IIS-IPSecurity /FeatureName:IIS-ISAPIExtensions /FeatureName:IIS-ISAPIFilter /FeatureName:IIS-LegacyScripts /FeatureName:IIS-LegacySnapIn /FeatureName:IIS-LoggingLibraries /FeatureName:IIS-ManagementConsole /FeatureName:IIS-ManagementScriptingTools /FeatureName:IIS-ManagementService /FeatureName:IIS-Metabase /FeatureName:IIS-NetFxExtensibility /FeatureName:IIS-ODBCLogging /FeatureName:IIS-Performance /FeatureName:IIS-RequestFiltering /FeatureName:IIS-RequestMonitor /FeatureName:IIS-Security /FeatureName:IIS-ServerSideIncludes /FeatureName:IIS-StaticContent /FeatureName:IIS-URLAuthorization /FeatureName:IIS-WebDAV /FeatureName:IIS-WebServer /FeatureName:IIS-WebServerManagementTools /FeatureName:IIS-WebServerRole /FeatureName:IIS-WindowsAuthentication /FeatureName:IIS-WMICompatibility /FeatureName:WAS-ConfigurationAPI /FeatureName:WAS-NetFxEnvironment /FeatureName:WAS-ProcessModel /FeatureName:WAS-WindowsActivationService ";
                                                        return runIisInstall(exePath, commandLineArgs);
                                                    };

                    Func<bool> tryInstallWithServerManagerCmd = () =>
                    {
                        session.Log("Attempting Install of IIS >=7 using ServerManagerCMD");
                        string exePath = string.Empty;
                        string winDir = Environment.GetEnvironmentVariable("windir");
                        if (string.IsNullOrEmpty(winDir))
                        {
                            var ex = new NullReferenceException("Could not find the environmental variable, 'windir'.");
                            session.Log(CustomActionException + ex);
                        }
                        else
                        {
                            exePath = Environment.ExpandEnvironmentVariables(@"%WINDIR%\SysNative\ServerManagerCMD.exe");
                        }
                        if (!File.Exists(exePath)) exePath = "ServerManagerCMD.exe";

                        const string commandLineArgs = @"-I Web-Server -allsubfeatures";
                        return runIisInstall(exePath, commandLineArgs);
                    };

                    #endregion

                    if (!tryInstallWithDism() && !tryInstallWithServerManagerCmd())
                    {
                        session.Log("Install of IIS >=7 failed.");
                        success = false;
                    }
                    session.Log("Successfully installed IIS >=7.");
                }

                // Ensure installer's IIS variables are properly initialized, since AppSearch occurs before CostInitialize
                if (success && string.IsNullOrEmpty(session["IISMAJORVERSION"]))
                {
                    session["IISMAJORVERSION"] = GetIisMajorVersion().ToString();
                    session["IISMINORVERSION"] = GetIisMinorVersion().ToString();
                }
            }
            catch (Exception ex)
            {
                if (session != null) { session.Log(CustomActionException + ex); }
                return ActionResult.Failure;
            }

            return success ? ActionResult.Success : ActionResult.Failure;
        }

        private static IList<RegisteredIisWebsite> GetWebSitesViaWebAdministration(Session session)
        {
            var result = new List<RegisteredIisWebsite>();

            using (var iisManager = new ServerManager())
            {
                foreach (Site webSite in iisManager.Sites)
                {
                    var website = new RegisteredIisWebsite
                    {
                        Id = webSite.Id.ToString(CultureInfo.InvariantCulture),
                        Description = webSite.Name,
                        Path = webSite.PhysicalPath()
                    };

                    if (webSite.Applications.Any(a => a.Path == "/IOPracticeware"))
                    {
                        session.Log("Found existing IOPracticeware application on website: " + website.Id);
                        website.HostsIOSite = true;
                    }

                    result.Add(website);
                }
            }

            return result;
        }

        private static IList<RegisteredIisWebsite> GetWebSitesViaMetabase(Session session)
        {
            var result = new List<RegisteredIisWebsite>();

            using (var iisRoot = new DirectoryEntry(IisEntry))
            {
                foreach (DirectoryEntry webSite in iisRoot.Children)
                {
                    if (webSite.SchemaClassName.Equals(IisWebServer, StringComparison.InvariantCultureIgnoreCase))
                    {
                        var website = new RegisteredIisWebsite
                        {
                            Id = webSite.Name,
                            Description = webSite.Properties[ServerComment].Value.ToString(),
                            Path = webSite.PhysicalPath()
                        };

                        using (var de = new DirectoryEntry(string.Join("/", new[] { IisEntry, webSite.Name, Extensions.Root })))
                        {
                            if (de.Children.OfType<DirectoryEntry>().Any(i => i.Name == "IOPracticeware"))
                            {
                                session.Log("Found existing IOPracticeware application on website: " + webSite.Name);
                                website.HostsIOSite = true;
                            }
                        }

                        result.Add(website);
                    }
                }
            }

            return result;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct OsVersionInfo
        {
            public int dwOSVersionInfoSize;
            public int dwMajorVersion;
            public int dwMinorVersion;
            public int dwBuildNumber;
            public int dwPlatformId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szCSDVersion;
        }
        [DllImport("kernel32.Dll")]
        public static extern short GetVersionEx(ref OsVersionInfo o);

        private static OsVersionInfo GetOsVersionInfo()
        {
            var os = new OsVersionInfo();
            os.dwOSVersionInfoSize = Marshal.SizeOf(typeof(OsVersionInfo));
            GetVersionEx(ref os);
            return os;
        }

        public static bool IsIIs6Required()
        {
            OsVersionInfo os = GetOsVersionInfo();
            // Os version of at least 5.0 definies WindowsXp or Server2003
            return os.dwMajorVersion == 5;
        }

        public static bool IsIIs7Required()
        {
            OsVersionInfo os = GetOsVersionInfo();
            // Os version of at least 6.0 defines Vista, Server2008 and Windows7
            return os.dwMajorVersion == 6;
        }

        /// <summary>
        /// Installs IO SSL certificate and configures Https endpoint
        /// </summary>
        /// <param name="session"></param>
        /// <remarks>! DEFERRED ACTION !</remarks>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult SetSslCertificate(Session session)
        {
            var iisMajorVersionInt = GetIisMajorVersion();
            if (iisMajorVersionInt < 7) return ActionResult.NotExecuted;

            var thumbPrintBytes = InstallSslCertificate();

            using (var serverManager = new ServerManager())
            {
                // Locate IO application
                var ioApp = GetIOApp(serverManager);
                if (ioApp == null)
                {
                    return ActionResult.NotExecuted;
                }

                // Granting permissions to the Network Service for the IIS path
                var iisPath = ioApp.VirtualDirectories[0].PhysicalPath;
                if (!string.IsNullOrEmpty(iisPath))
                {
                    const string account = "NETWORK SERVICE";
                    ServerSetupCustomActions.GrantFullAccess(iisPath, account);
                }

                // Ensure https binding is set
                var site = serverManager.Sites.First(s => s.Applications.Contains(ioApp));
                var httpsBinding = site.Bindings
                    .FirstOrDefault(b => b.Protocol == "https" && b.BindingInformation == "*:443:");

                // https already installed/configured?
                if (httpsBinding == null)
                {
                    site.Bindings.Add("*:443:", thumbPrintBytes, "My");
                    serverManager.CommitChanges();
                }
                else
                {
                    httpsBinding.CertificateHash = thumbPrintBytes;
                    serverManager.CommitChanges();
                }
            }

            return ActionResult.Success;
        }

        /// <summary>
        /// Configures IIS settings
        /// </summary>
        /// <param name="session"></param>
        /// <remarks>! DEFERRED ACTION !</remarks>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult IisWarmUp(Session session)
        {
            var iisMajorVersionInt = GetIisMajorVersion();
            if (iisMajorVersionInt < 7) return ActionResult.NotExecuted;

            using (var serverManager = new ServerManager())
            {
                var ioApp = GetIOApp(serverManager);
                if (ioApp == null)
                {
                    return ActionResult.NotExecuted;
                }

                // Locate app pool
                var ioAppPool = serverManager.ApplicationPools
                    .First(a => a.Name == ioApp.ApplicationPoolName);

                // Adding/Changing an application pool with startMode (alwaysRunning) to warm up IIS
                ioAppPool.AutoStart = true;
                ioAppPool["startMode"] = "AlwaysRunning";
                session.Log("Configured auto start for app pool {0}. Running IIS {1} and IO website {2}", ioAppPool.Name, iisMajorVersionInt, ioApp.Path);

                // Additionally force usage of 64bit processes
                ioAppPool.Enable32BitAppOnWin64 = false;
                session.Log("Configured app pool for 64 bit");

                // Ensure dynamic compression is enabled for all content types
                var config = serverManager.GetApplicationHostConfiguration();
                var httpCompressionSection = config.GetSection("system.webServer/httpCompression");
                var dynamicTypesCollection = httpCompressionSection.GetCollection("dynamicTypes");
                var element = dynamicTypesCollection.FirstOrDefault(i => i["mimeType"] as string == "*/*") ?? dynamicTypesCollection.CreateElement("add");
                element["mimeType"] = @"*/*";
                element["enabled"] = true;
                session.Log("Configured dynamic compression.");

                // Changing the IOPracticeware property preloadEnabled = True (available on IIS 8)
                if (iisMajorVersionInt >= 8)
                {
                    // Enable preload
                    ioApp["preloadEnabled"] = "true";

                    // Configure application initialization
                    var webConfig = ioApp.GetWebConfiguration();
                    if (webConfig != null)
                    {
                        var applicationInitializationSection = webConfig.GetSection("system.webServer/applicationInitialization");
                        applicationInitializationSection.SetAttributeValue("skipManagedModules", "true");
                        var applicationInitializationElements = applicationInitializationSection.GetCollection();
                        if (applicationInitializationElements.All(e => e["initializationPage"] as string != "Services/AuthenticationService"))
                        {
                            var initializationPageElement = applicationInitializationElements.CreateElement("add");
                            initializationPageElement["initializationPage"] = @"Services/AuthenticationService";
                            applicationInitializationElements.Add(initializationPageElement);
                        }
                    }
                }

                // Save all changes
                serverManager.CommitChanges();
            }
            return ActionResult.Success;
        }

        private static Application GetIOApp(ServerManager serverManager)
        {
            return serverManager.Sites
                .SelectMany(a => a.Applications)
                .FirstOrDefault(a => string.Equals(a.Path, "/IOPracticeware", StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Installs IO Ssl certificate into Computer's LocalMachine store
        /// </summary>
        /// <returns></returns>
        private static byte[] InstallSslCertificate()
        {
            var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadWrite | OpenFlags.MaxAllowed);

            // Extract certificate
            var certificatePath = Path.GetFullPath("IO Practiceware.pfx");
            File.WriteAllBytes(certificatePath, Resources.IOPracticewarePfx);

            // Install it
            var cert = new X509Certificate2(certificatePath, "iopracticeware", X509KeyStorageFlags.Exportable | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.MachineKeySet);
            var thumbPrintBytes = cert.GetCertHash();
            store.Add(cert);
            store.Close();

            // Cleanup
            File.Delete(certificatePath);

            return thumbPrintBytes;
        }

        /// <summary>
        /// Identifies and returns IIS major version
        /// </summary>
        /// <returns></returns>
        private static int GetIisMajorVersion()
        {
            int iisVersion;

            using (RegistryKey iisKey = Registry.LocalMachine.OpenSubKey(IisRegKey))
            {
                if (iisKey == null) throw new InvalidOperationException("Could not determine IIS major version.");
                iisVersion = (int)iisKey.GetValue(IisRegKeyMajorVersion);
            }

            return iisVersion;
        }

        /// <summary>
        /// Identifies and returns IIS minor version
        /// </summary>
        /// <returns></returns>
        private static int GetIisMinorVersion()
        {
            int iisVersion;

            using (RegistryKey iisKey = Registry.LocalMachine.OpenSubKey(IisRegKey))
            {
                if (iisKey == null) throw new InvalidOperationException("Could not determine IIS minor version.");
                iisVersion = (int)iisKey.GetValue(IisRegKeyMinorVersion);
            }

            return iisVersion;
        }

        private class RegisteredIisWebsite
        {
            public string Id { get; set; }
            public string Description { get; set; }
            public string Path { get; set; }
            public bool HostsIOSite { get; set; }
        }
    }

    internal static class Extensions
    {
        internal const string Root = "ROOT";
        internal const string Path = "Path";

        public static string PhysicalPath(this Site site)
        {
            if (site == null)
            {
                throw new ArgumentNullException("site");
            }

            Application root = site.Applications.Single(a => a.Path == "/");
            VirtualDirectory vRoot = root.VirtualDirectories.Single(v => v.Path == "/");

            // Can get environment variables, so need to expand them
            return Environment.ExpandEnvironmentVariables(vRoot.PhysicalPath);
        }

        public static string PhysicalPath(this DirectoryEntry site)
        {
            if (site == null)
            {
                throw new ArgumentNullException("site");
            }

            string path;

            using (var de = new DirectoryEntry(string.Join("/", new[] { IisCustomActions.IisEntry, site.Name, Root })))
            {
                path = de.Properties[Path].Value.ToString();
            }

            return path;
        }
    }
}