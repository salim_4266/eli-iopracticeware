<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://schemas.microsoft.com/wix/2006/wi"
    xmlns:wix="http://schemas.microsoft.com/wix/2006/wi">

  <xsl:template match="wix:Component">
    <xsl:copy>
      <xsl:apply-templates select="@*" />
      <xsl:attribute name="Permanent">
        <xsl:text>yes</xsl:text>
      </xsl:attribute>
      <xsl:if test="not(wix:File[contains(@Source, '.msi')]) and not(wix:File[contains(@Source, '.exe')])">
        <xsl:attribute name="NeverOverwrite">
          <xsl:text>yes</xsl:text>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="*" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="wix:File">
    <xsl:copy>
      <xsl:attribute name="DiskId">
        <xsl:text>2</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates select="*" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="*">
    <xsl:copy>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates select="* | text()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="@* | text()">
    <xsl:copy />
  </xsl:template>

  <xsl:template match="/">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="wix:File">
    <xsl:element name="File" namespace="http://schemas.microsoft.com/wix/2006/wi">


      <xsl:choose>
        <xsl:when test="not(contains(@Source,'IO.') or contains(@Source,'Soaf') or contains(@Source,'.cshtml') or contains(@Source,'.config'))">
          <xsl:attribute name="Id">
            <xsl:value-of select="concat('Sharepoint_',translate(translate(translate(translate(translate(translate(translate(translate(translate(translate(translate(substring-after(@Source,'\'),'\',''),'-','Dash'),' ','Space'),',','Comma'),'+','Plus'),'(','LeftParen'),')','RightParen'),'!','Exclamation'),'$','Dollar'),';','Semicolon'),'`','Tilde'))"/>
          </xsl:attribute>
          <xsl:attribute name="DiskId">
            <xsl:text>2</xsl:text>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="Id">
            <xsl:value-of select="@Id"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:attribute name ="Source">
        <xsl:value-of select="@Source"/>
      </xsl:attribute>

      <xsl:attribute name="KeyPath">yes</xsl:attribute>
      <xsl:apply-templates select="*" />
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>