<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:wix="http://schemas.microsoft.com/wix/2006/wi" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>

  <xsl:key name="IOPracticewareSystemConfig" match="wix:Component[contains(wix:File/@Source, 'IO.Practiceware.System.config')]" use="@Id" />
  <xsl:key name="IOPracticewareCustomConfig" match="wix:Component[contains(wix:File/@Source, 'IO.Practiceware.Custom.config')]" use="@Id" />
  <xsl:key name="Pdb" match="wix:Component[contains(wix:File/@Source, '.pdb')]" use="@Id" />
  <xsl:key name="WebDll" match="wix:Component[contains(wix:File/@Source, 'IO.Practiceware.Web.dll')]" use="@Id" />

  <xsl:template match="wix:Component[key('IOPracticewareSystemConfig', @Id)]" />
  <xsl:template match="wix:ComponentRef[key('IOPracticewareSystemConfig', @Id)]" />

  <xsl:template match="wix:Component[key('IOPracticewareCustomConfig', @Id)]" />
  <xsl:template match="wix:ComponentRef[key('IOPracticewareCustomConfig', @Id)]" />

  <xsl:template match="wix:Component[key('Pdb', @Id)]" />
  <xsl:template match="wix:ComponentRef[key('Pdb', @Id)]" />

  <xsl:template match="wix:Component[key('WebDll', @Id)]" />
  <xsl:template match="wix:ComponentRef[key('WebDll', @Id)]" />
  <xsl:template match="wix:File">

    <xsl:element name="File" namespace="http://schemas.microsoft.com/wix/2006/wi">

      <xsl:choose>
        <xsl:when test="not(contains(@Source,'IO.') or contains(@Source,'Soaf') or contains(@Source,'.cshtml') or contains(@Source,'.config'))">
          <xsl:attribute name="Id">
            <xsl:value-of select="concat('Web_',translate(translate(translate(translate(translate(translate(translate(translate(translate(translate(translate(substring-after(@Source,'\'),'\',''),'-','Dash'),' ','Space'),',','Comma'),'+','Plus'),'(','LeftParen'),')','RightParen'),'!','Exclamation'),'$','Dollar'),';','Semicolon'),'`','Tilde'))"/>
          </xsl:attribute>
          <xsl:attribute name="DiskId">
            <xsl:text>2</xsl:text>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="Id">
            <xsl:value-of select="@Id"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:attribute name ="Source">
        <xsl:value-of select="@Source"/>
      </xsl:attribute>

      <xsl:attribute name="KeyPath">yes</xsl:attribute>
      <xsl:apply-templates select="*" />
    </xsl:element>

  </xsl:template>
  
</xsl:stylesheet>
