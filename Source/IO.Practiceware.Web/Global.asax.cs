﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using IO.Practiceware.Presentation;
using IO.Practiceware.Web;
using IO.Practiceware.Web.Areas.Integration;
using Soaf.ComponentModel;
using Soaf.Web;
using Soaf.Web.Services;

[assembly: Component(typeof(RepositoryServicesConfiguration), typeof(IRepositoryServicesConfiguration))]

namespace IO.Practiceware.Web
{
    public class RepositoryServicesConfiguration : IRepositoryServicesConfiguration
    {
        public bool ServicesEnabled
        {
            get { return true; }
        }
    }

    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class Application : HttpApplication
    {
        private static WebComponents _webComponents;

        public override void Init()
        {
            base.Init();
            if (_webComponents != null) _webComponents.UserPrincipalBootstrapper.Initialize();
        }

        protected void Application_Start()
        {
            if (_webComponents == null)
            {
                _webComponents = Bootstrapper.Run<WebComponents>(Server.MapPath("~/"));
                _webComponents.MvcBootstrapper.RegisterAllAreas = false;
                _webComponents.InitializeAll();
                RegisterArea<IntegrationAreaRegistration>(RouteTable.Routes, null);

                PresentationBootstrapper.AddViewModelKnownTypes();
            }
        }

        public static void RegisterArea<T>(RouteCollection routes, object state) where T : AreaRegistration
        {
            var registration = (AreaRegistration)Activator.CreateInstance(typeof(T));
            var context = new AreaRegistrationContext(registration.AreaName, routes, state);
            string tNamespace = registration.GetType().Namespace;
            if (tNamespace != null)
            {
                context.Namespaces.Add(tNamespace + ".*");
            }
            registration.RegisterArea(context);
        }
    }
}