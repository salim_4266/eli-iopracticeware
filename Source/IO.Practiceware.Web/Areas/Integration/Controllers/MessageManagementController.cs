﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewServices.IntegrationMessageManagement;
using IO.Practiceware.Web.HtmlHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IO.Practiceware.Web.Areas.Integration.Controllers
{
    [Authorize]
    public class MessageManagementController : Controller
    {
        private const int PageSize = 10;
        private readonly IIntegrationMesageManagementViewService _hl7Manager;
        private readonly IPracticeRepository _practiceRepository;

        public MessageManagementController(IPracticeRepository practiceRepository, IIntegrationMesageManagementViewService hl7Manager)
        {
            _practiceRepository = practiceRepository;
            _hl7Manager = hl7Manager;
        }

        public ActionResult Index()
        {
            var model = new MessageSearchModel();
            model.ExternalSystems = GetExternalSystems();
            return View("MessageManagement", model);
        }

        private IDictionary<int, string> GetExternalSystems()
        {
            IDictionary<int, string> items = _practiceRepository.ExternalSystems.ToDictionary(x => x.Id, x => x.Name);
            return items;
        }

        [ValidateInput(false)]
        public ActionResult SearchMessages(MessageSearchModel model, int externalSystemsId)
        {
            model.ExternalSystemId = externalSystemsId;
            model.PageSize = PageSize;
            GetMessages(model, model.SelectedPage);
            return View("MessageManagement", model);
        }

        public ActionResult ValidateMessage(string id)
        {
            string message = _hl7Manager.GetMessage(id);
            List<string> returnVal = _hl7Manager.HL7MessageValidation(message);
            ViewBag.ValidationMessage = returnVal;
            return PartialView("MessageValidation");
        }

        [HttpPost]
        [ActionName("Post")]
        [AcceptSubmitButton(Name = "SearchButton", Value = "Search")]
        public ActionResult Search(MessageSearchModel model, int page = 1)
        {
            if (ModelState.IsValid)
            {
                GetMessages(model, page);
            }
            return View("MessageManagement", model);
        }

        [HttpPost]
        [ActionName("Post")]
        [ValidateInput(false)]
        [AcceptSubmitButton(Name = "ReprocessButton", Value = "Reprocess")]
        public ActionResult Reprocess(MessageSearchModel model)
        {
            var hl7Message = new List<MessagesModel>();
            if (model.MessageModelList.Any())
            {
                foreach (MessagesModel item in model.MessageModelList)
                {
                    if (Equals(item.IsChecked, true))
                    {
                        var hl7Model = new MessagesModel();
                        hl7Model.Id = item.Id;
                        hl7Model.ProcessingAttemptCount = 0;
                        hl7Message.Add(hl7Model);
                    }
                }
                _hl7Manager.Save(hl7Message);
            }
            GetMessages(model);
            return View("MessageManagement", model);
        }

        [HttpPost]
        [ActionName("Post")]
        [ValidateInput(false)]
        [AcceptSubmitButton(Name = "GoButton", Value = "Go")]
        public ActionResult SearchMessages(MessageSearchModel model)
        {
            if (ModelState.IsValid)
            {
                GetMessages(model, model.SelectedPage);
            }
            return View("MessageManagement", model);
        }

        private void GetMessages(MessageSearchModel model, int page = 1)
        {
            model.PageSize = PageSize;
            model.CurrentPage = page;
            if (model.ExternalSystemMessageProcessingStateId != 0)
            {
                if (model.ExternalSystemMessageProcessingState != null) model.ExternalSystemMessageProcessingStateId = (int)model.ExternalSystemMessageProcessingState;
            }
            model.MessageModelList = _hl7Manager.SearchMessages(model).ToList();

            model.ExternalSystems = GetExternalSystems();
            var pagelist = new List<string>();
            for (int i = 1; i <= model.TotalPages; i++)
            {
                pagelist.Add(i.ToString());
            }
            model.PageList = pagelist;
        }
    }
}