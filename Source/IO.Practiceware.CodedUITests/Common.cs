﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;

namespace IO.Practiceware.CodedUITests
{
    [TestClass]
#if DISABLE_UI_TESTS
    [Ignore]
#endif
    public class Common
    {
#if DEBUG
        internal const string PracticeRepositoryConnectionString = "Data Source=dev-sql01;Initial Catalog=PracticeRepositoryUITests;Connection Timeout=1200;Integrated Security=True;MultipleActiveResultSets=True";
#else
        internal const string PracticeRepositoryConnectionString = "Data Source=localhost;Initial Catalog=PracticeRepository;Connection Timeout=1200;Integrated Security=True;MultipleActiveResultSets=True";
#endif

        /// <summary>
        /// Returns an open connection to the PracticeRepository.
        /// </summary>
        public static IDbConnection PracticeRepository
        {
            get
            {
                var connection = new SqlConnection(PracticeRepositoryConnectionString);
                connection.Open();
                return connection;
            }
        }

        /// <summary>
        /// Runs an arbitrary scalar SQL script and returns the result.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static object RunSqlScript(string sql)
        {
            using (var connection = PracticeRepository)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = sql;
                return command.ExecuteScalar();
            }
        }

        [AssemblyInitialize]
        public static void OnAssemblyInitialize(TestContext testContext)
        {
            Playback.PlaybackSettings.ShouldSearchFailFast = false;
            Playback.PlaybackSettings.SearchTimeout = 10000;
            Playback.PlaybackSettings.WaitForReadyTimeout = 10000;
            Playback.PlaybackSettings.DelayBetweenActions = 2000;

            RunSqlSetupScripts();
        }

        /// <summary>
        /// Runs any setup scripts for data driven tests.
        /// </summary>
        private static void RunSqlSetupScripts()
        {
            RunSqlScript(Properties.Resources.UITestPatients);
            RunSqlScript(Properties.Resources.UITest___Delete_AdminSetUp_Elements);
        }

        /// <summary>
        /// Performs installation and configuration steps required to run UI tests. Any test plans should add this test as the first to run.
        /// </summary>
        [TestMethod]
        [Timeout(TestTimeout.Infinite)]
        [TestCategory("Common")]
        public void SetUpInstallation()
        {
            Trace.Listeners.Add(new TextWriterTraceListener("C:\\CodedUITests.log") { TraceOutputOptions = TraceOptions.DateTime });
            try
            {
                MapDrives();

                InstallServerAndClientSetup();
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                throw;
            }
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            // warm up web application
            try { new WebClient().DownloadString(new Uri("https://localhost/IOPracticeware/Services/AuthenticationService/WsHttp/Protobuf")); }
            catch (Exception ex)
            {
                Trace.TraceWarning(ex.ToString());
            }
        }

        private void CopyMdbs()
        {
            const string newVersionPath = @"C:\IOP\Pinpoint\SoftwareUpdates\Client\Pinpoint\";

            File.Copy(@"T:\DiagnosisMaster.mdb", Path.Combine(newVersionPath, "DiagnosisMaster.mdb"), true);
            File.Copy(@"T:\DynamicForms.mdb", Path.Combine(newVersionPath, "DynamicForms.mdb"), true);
            Trace.TraceInformation("Copied DiagnosisMaster and DynamicForms");
        }

        public void MapDrives()
        {
            if (DriveInfo.GetDrives().All(i => i.Name != "P:\\"))
            {
                var process = new Process();
                process.StartInfo.Arguments =
                    @"USE P: \\localhost\C$\IOP reT&*325 /USER:IOPRACTICEWARE\Administrator /PERSISTENT:YES";
                process.StartInfo.FileName = "net.exe";
                process.Start();
                process.WaitForExit();
                process.Close();
                process.Dispose();
            }
            if (DriveInfo.GetDrives().All(i => i.Name != "P:\\"))
            {
                throw new Exception("P drive not mapped correctly.");
            }

            if (DriveInfo.GetDrives().All(i => i.Name != "G:\\"))
            {
                var process = new Process();
                process.StartInfo.Arguments =
                    @"USE G: \\iop-filesvr\IOP reT&*325 /USER:IOPRACTICEWARE\Administrator /PERSISTENT:YES";
                process.StartInfo.FileName = "net.exe";
                process.Start();
                process.WaitForExit();
                process.Close();
                process.Dispose();
            }
            if (DriveInfo.GetDrives().All(i => i.Name != "G:\\"))
            {
                throw new Exception("G drive not mapped correctly.");
            }

            if (DriveInfo.GetDrives().All(i => i.Name != "T:\\"))
            {
                var process = new Process();
                process.StartInfo.Arguments =
                    @"USE T: ""\\TFS\DM DF"" reT&*325 /USER:IOPRACTICEWARE\Administrator /PERSISTENT:YES";
                process.StartInfo.FileName = "net.exe";
                process.Start();
                process.WaitForExit();
                process.Close();
                process.Dispose();
            }
            if (DriveInfo.GetDrives().All(i => i.Name != "T:\\"))
            {
                throw new Exception("T drive not mapped correctly.");
            }
        }

        public void InstallServerAndClientSetup()
        {
            // get first three parts of the assembly version (eg 7.31.2000.0 is 7.31.2000)
            var version = string.Join(".", Assembly.GetExecutingAssembly().GetName().Version.ToString().Split('.').Take(3));

            // find the first build with a matching directory name
            var folder = Directory.GetDirectories("G:\\Builds", "*", SearchOption.AllDirectories).Select(d => new DirectoryInfo(d)).Where(d => d.Name.Equals(version)).Select(d => d.FullName).FirstOrDefault();

            // if no matching build found, go for the newest build from Main
            if (folder == null) folder = Directory.GetDirectories("G:\\Builds\\Main").OrderByDescending(Directory.GetLastWriteTime).FirstOrDefault();

            if (folder == null) throw new Exception("No suitable build folder found.");

            Trace.TraceInformation("Installing server msi from {0}.", folder);

            if (Directory.GetFiles(folder).Any(i => i.Contains("IO Practiceware Server Setup.msi")))
            {
                var path = Path.Combine(folder, "IO Practiceware Server Setup.msi");
                var programFiles = Environment.GetEnvironmentVariable("PROGRAMFILES");

                var args = String.Format("/i \"{0}\" /l*v C:\\ServerSetup.log /qn TARGETDIR=C:\\ FTPUSERNAME=\"UITests\" FTPPASSWORD=\"%IRO7v7]\" INSTALLDIR=\"{1}\\IO Practiceware Server\\\" DBSERVER=\"{2}\" WEBSITE=1 ALERTSDESTINATIONEMAILADDRESS=\"dev@iopracticeware.com\"", path, programFiles, PracticeRepositoryConnectionString.Replace(";", ";;"));

                Trace.TraceInformation("Running msiexec with args {0}.", args);

                var process = new Process { StartInfo = { Arguments = args, FileName = "msiexec.exe" } };
                process.Start();
                process.WaitForExit();

                CopyMdbs();

                Trace.TraceInformation("msiexec returned exit code {0}.", process.ExitCode);

                process.Close();
                process.Dispose();
                var logPath = programFiles + "\\IO Practiceware Server\\~Logs";
                var latestLogFile = Directory.GetFiles(logPath).OrderByDescending(Directory.GetLastWriteTime).FirstOrDefault();

                if (latestLogFile == null)
                {
                    throw new Exception("Migration log file not found. Installation probably failed.");
                }

                var reader = File.ReadAllText(latestLogFile);
                if (!reader.Contains("Migration succeeded"))
                {
                    throw new Exception("Migration failed.");
                }

                if (Directory.GetFiles(folder).Any(i => i.Contains("IO Practiceware Client Setup.exe")))
                {
                    path = Path.Combine(folder, "IO Practiceware Client Setup.exe");
                    File.Copy(path, @"C:\IO Practiceware Client Setup.exe", true);
                    if (!InstallClientExe(@"C:\IO Practiceware Client Setup.exe"))
                    {
                        throw new Exception("Could not install client exe.");
                    }
                }

            }
        }

        private static bool InstallClientExe(string installerPath)
        {
            // ReSharper disable AssignNullToNotNullAttribute
            var logLocation = Path.Combine(Path.GetDirectoryName(installerPath), Path.GetFileNameWithoutExtension(installerPath)) + ".log";
            var arguments = string.Format("-passive -log \"{0}\"", logLocation);
            var exitCode = ExecuteAndWait(installerPath, arguments);
            return exitCode == 0; // Success
        }

        private static int ExecuteAndWait(string command, string arguments = null)
        {
            var process = Process.Start(new ProcessStartInfo
            {
                FileName = command,
                Arguments = arguments,
                WindowStyle = ProcessWindowStyle.Hidden
            });
            process.WaitForExit();
            return process.ExitCode;
        }

    }
}
