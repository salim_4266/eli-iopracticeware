﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;

namespace IO.Practiceware.CodedUITests
{


    public partial class UIMap
    {

        public void EnterInput(WinEdit uiItemEdit, string value)
        {
            uiItemEdit.Text = value;
        }

        public void Enter(WpfText uiItemEdit, string value)
        {
            Mouse.Click(uiItemEdit);
            Keyboard.SendKeys(uiItemEdit, value);
        }

       /* internal void Enter(WpfEdit wpfEdit, string p)
        {
            throw new System.NotImplementedException();
        }

        internal void Enter(UIRadComboBoxComboBox uIRadComboBoxComboBox)
        {
            throw new System.NotImplementedException();
        }

        internal void Enter(UIRadComboBoxComboBox uIRadComboBoxComboBox, string p)
        {
            throw new System.NotImplementedException();
        }

        internal void Enter(WpfComboBox wpfComboBox)
        {
            throw new System.NotImplementedException();
        }

        internal void Enter(WpfEdit wpfEdit)
        {
            throw new System.NotImplementedException();
        }

        internal void Enter(WpfComboBox wpfComboBox, string p)
        {
            throw new System.NotImplementedException();
        }*/
    }
}


