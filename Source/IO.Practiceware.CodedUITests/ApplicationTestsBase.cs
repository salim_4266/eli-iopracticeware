﻿using System;
using CodedUITests.LoginUIMapClasses;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace IO.Practiceware.CodedUITests
{
    public abstract class ApplicationTestsBase
    {
        static ApplicationTestsBase()
        {
            Playback.PlaybackSettings.ShouldSearchFailFast = false;
            Playback.PlaybackSettings.SearchTimeout = 10000;
            Playback.PlaybackSettings.WaitForReadyTimeout = 10000;
            Playback.PlaybackSettings.DelayBetweenActions = 5000;
        }

        public TestContext TestContext { get; set; }

        protected abstract string ExePath { get; }

        protected static ApplicationUnderTest Application { get; private set; }

        /// <summary>
        /// This should log in to the software. Assumes the log in screen is already up for whatever the current TestApplication is.
        /// </summary>
        public static ApplicationUnderTest LogIn(string exePath, bool retryIfProcessExited = true)
        {
            try
            {
                Trace.TraceInformation("Launching application at {0}.", exePath);
                var testApplication = ApplicationUnderTest.Launch(exePath, exePath);
                testApplication.CloseOnPlaybackCleanup = false;
                Playback.Wait(60000);

                Trace.TraceInformation("Logging in.");
                var map = new LoginUIMap();

                map.Login();

                Playback.Wait(60000);

                // software may have updated
                if (testApplication.Process.HasExited && retryIfProcessExited)
                {
                    Trace.TraceInformation("Original process was terminated.");

                    WaitForUpdateComplete();

                    testApplication.Close();

                    Trace.TraceInformation("Restarting application and logging in after update.");

                    return LogIn(exePath, false);
                }

                Trace.TraceInformation("Logged in successfully.");

                return testApplication;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                throw;
            }
        }

        private static void WaitForUpdateComplete()
        {
            var updater = Process.GetProcessesByName("IOPUpdater.tmp").FirstOrDefault();

            if (updater == null)
            {
                Trace.TraceInformation("Updater not running.");
                return;
            }

            Trace.TraceInformation("Updater is running. Waiting for exit.");

            updater.WaitForExit();

            Trace.TraceInformation("Software was updated.");
        }

        [TestInitialize]
        public void OnTestInitialize()
        {
            if (!Process.GetProcessesByName(Path.GetFileNameWithoutExtension(Environment.ExpandEnvironmentVariables(ExePath))).Any())
                LogIn(Environment.ExpandEnvironmentVariables(ExePath));

            Playback.Wait(10000);
        }

        [ClassCleanup]
        public static void OnClassCleanup()
        {
            if (Application != null)
            {
                Application.Close();
            }
        }

        [TestCleanup]
        public void OnTestCleanup()
        {
            if (TestContext.CurrentTestOutcome != UnitTestOutcome.Passed && Application != null)
            {
                Application.Close();
            }
        }
    }
}