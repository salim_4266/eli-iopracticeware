﻿namespace IO.Practiceware.CodedUITests.Exam.TestEditPreviousVisitClasses
{
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [CodedUITest]
#if DISABLE_UI_TESTS
    [Ignore]
#endif
    public partial class TestEditPreviousVisit
    {
        private TestEditPreviousVisit _epv = null;

        [TestMethod]
        public void DeleteAllergiesFromEPV()
        {
            //This method will delete the allergies from EPV
            _epv = new TestEditPreviousVisit();
            _epv.DeleteAllergiesFromEPV();

        }

        [TestMethod]
        public void AddPatientHistoryFromEpv()
        {
            //This method will delete the allergies from EPV
            _epv = new TestEditPreviousVisit();
            _epv.AddpatienthistoryfromEPV();

        }
    }
}
