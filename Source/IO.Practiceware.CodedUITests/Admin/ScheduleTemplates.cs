﻿namespace IO.Practiceware.CodedUITests.Admin.ScheduleTemplatesClasses
{
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;


#if DISABLE_UI_TESTS
    [Ignore]
#endif
    [CodedUITest]

    public partial class ScheduleTemplates : ApplicationTestsBase
    {
        private ScheduleTemplates _st = null;

        [TestMethod]
        public void CreateScheduleTemplates()
        {
            //This method will create schedule template from Admin Setup Screen.
            _st = new ScheduleTemplates();
            _st.CreateTemplate();
        }

        protected override string ExePath
        {
            get { return @"%LocalAppData%\IO Practiceware\Administrator.exe"; }
        }

    }
}
