﻿namespace IO.Practiceware.CodedUITests.Admin.AdminSetUpUITestClasses
{
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [CodedUITest]
#if DISABLE_UI_TESTS
    [Ignore]
#endif
    public partial class AdminSetUpUITest : ApplicationTestsBase
    {

        private AdminSetUpUITest _ui = null;
        

        [TestMethod]
        public void CreateProvider()
        {
           // This Method will Create New Resources 
            _ui = new AdminSetUpUITest();

            _ui.CreateNewStaffDavid();
            
            _ui.CreateNewStaffPaul();
            
            //_ui.AddNPIToResources();


        }

        [TestMethod]
        public void CreateAppointmentTypes()
        {
            //This method will create appointment type.
            _ui = new AdminSetUpUITest();
            _ui.CreateNewApptTypes();

        }

       

        [TestMethod]
        public void CreateAppointmentCategories()
        {
            //This Method will Create Two New Appointment Categories.
            _ui = new AdminSetUpUITest();
            _ui.CreateNewApptCategories();
        }

       

        [TestMethod]
        public void CreateNewInsurance()
        {
            // This Method will create New Insurance Plan
            _ui = new AdminSetUpUITest();
            
           // _ui.CreateNewInsurancePlan();
            _ui.CreateNewInsuranceICICI();
           // _ui.CreateNewInsuranceAflac();

        }

        [TestMethod]
        public void TestMoreOptionsForInsurancePlan()
        {
            //This Method will search the insurance plan and make modifications under more options tab
            _ui = new AdminSetUpUITest();
            _ui.OpenSetUpScreen();
            _ui.Testmoreopetionsforinsuranceplan();
        }

        [TestMethod]
        public void TestAddCliamCrossOversForInsurancePlan()
        {
            //This Method will search the insurance plan and add the claim cross overs for the insurance plan.
            _ui = new AdminSetUpUITest();
            _ui.OpenSetUpScreen();
            _ui.Testclaimcrossoverforinsuranceplan();
        }

        [TestMethod]
        public void TestAddClaimFormSetupForInsurancePlan()
        {
            //This Method will search the insurance plan and add the Claim Association for the Plan
            _ui = new AdminSetUpUITest();
            _ui.OpenSetUpScreen();
            _ui.Testclaimformsetupforinsuraceplan();
        }

        [TestMethod]
        public void TestArchivingInsurancePlan()
        {
            //This Method will Archived the insuracence Plan
            _ui = new AdminSetUpUITest();
            _ui.OpenSetUpScreen();
            _ui.Testarchivinginsuraceplan();
        }
        [TestMethod]
        public void TestReactivatingInsurancePlan()
        {
            // This Method will Reactivate the insurance policy.
            _ui = new AdminSetUpUITest();
            _ui.OpenSetUpScreen();
            _ui.Testreactivatinginsuracepolicy();

        }

        [TestMethod]
        public void TestDocumentPrinterConfigeration()
        {
            _ui = new AdminSetUpUITest();
            _ui.OpenSetUpScreen();
            _ui.PrinterConfiguration();
            
        }

      
        [TestMethod]
        public void CreateNewScheduleTemplate()
        {
            _ui = new AdminSetUpUITest();
            _ui.OpenSetUpScreen();
            _ui.CreateNewTemplate();
        }

        [TestMethod]
        public void ApplyScheduleTemplateToProviders()
        {
            _ui = new AdminSetUpUITest();
            _ui.ApplyScheduleTempToDavid();
            Playback.Wait(4000);
            _ui.ApplyScheduleTempToPaul();

        }

         protected override string ExePath
         {
             get { return @"%LocalAppData%\IO Practiceware\Administrator.exe"; }
         }
    }
}
