﻿using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;

namespace IO.Practiceware.CodedUITests.Admin.TestPatientsClasses
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Input;
    using System.CodeDom.Compiler;
    using System.Text.RegularExpressions;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;

    [CodedUITest]
#if DISABLE_UI_TESTS
    [Ignore]
#endif
    public partial class TestPatients 
    {
        public TestPatients()
            {
        }
        [DataSource("System.Data.SqlClient", Common.PracticeRepositoryConnectionString, "CodedUITestPatients", DataAccessMethod.Sequential), TestMethod]
        public void CreateUITestPatients()
        {
            this.OpenPatientSearchScreen();
            this.ClickCreateNewPatient();
            Playback.Wait(4000);
            this.Enter(this.UIHomeWindow.UIPatientInfoViewNameCustom.UIItemTabList.UIXceedWpfAvalonDockLaTabPage.UIPdViewControlCustom.UITopLeftScrollViewerPane.UIFirstNameText, TestContext.DataRow["FirstName"].ToString());
            this.Enter(this.UIHomeWindow.UIPatientInfoViewNameCustom.UIItemTabList.UIXceedWpfAvalonDockLaTabPage.UIPdViewControlCustom.UITopLeftScrollViewerPane.UILastNameText, TestContext.DataRow["LastName"].ToString());
            this.Enter(this.UIHomeWindow.UIPatientInfoViewNameCustom.UIItemTabList.UIXceedWpfAvalonDockLaTabPage.UIPdViewControlCustom.UITopLeftScrollViewerPane.UIAddressLine1Text, TestContext.DataRow["Address1"].ToString());
            this.SelectAddType();
            this.Enter(this.UIHomeWindow.UIPatientInfoViewNameCustom.UIItemTabList.UIXceedWpfAvalonDockLaTabPage.UIPdViewControlCustom.UITopLeftScrollViewerPane.UIPostalCodeEdit.UIPostalCodeText, TestContext.DataRow["Zip"].ToString());
            this.Enter(this.UIHomeWindow.UIPatientInfoViewNameCustom.UIItemTabList.UIXceedWpfAvalonDockLaTabPage.UIPdViewControlCustom.UITopLeftScrollViewerPane.UIRootUserControlCustom.UIAreaText, TestContext.DataRow["AreaCode"].ToString());
            this.Enter(this.UIHomeWindow.UIPatientInfoViewNameCustom.UIItemTabList.UIXceedWpfAvalonDockLaTabPage.UIPdViewControlCustom.UITopLeftScrollViewerPane.UIRootUserControlCustom.UIPhoneNumberText, TestContext.DataRow["Phone"].ToString());
            this.SelectPhoneType();
            this.Enter(this.UIHomeWindow.UIPatientInfoViewNameCustom.UIItemTabList.UIXceedWpfAvalonDockLaTabPage.UIPdViewControlCustom.UITopLeftScrollViewerPane.UIEmailText, TestContext.DataRow["Email"].ToString());
            this.SelectEmailType();
            this.Enter(this.UIHomeWindow.UIPatientInfoViewNameCustom.UIItemTabList.UIXceedWpfAvalonDockLaTabPage.UIPdViewControlCustom.UITopRightScrollViewerPane.UIDatetimepickerCustom.UIPART_DateTimeInputEdit.UIBirthdateText, TestContext.DataRow["DateOfBirth"].ToString());
            this.SelectGender();
            //this.SelectRace();
            this.SelectEthnicity();
            this.SelectLanguage();
            this.SelectPhysicians();
            this.SelectBillingOrganizations();
            this.SelectPatientTags();
            this.EnterComments();
            this.SelectStatus();
            this.RemovePCPAndOtherComboBox();
            this.RemoveReferringComboBox();
            this.ClickSave();
            Playback.Wait(5000);
            this.ClosePatientInfo();

        }

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public void Enter(WpfText uiItemEdit, string value)
        {
            Mouse.Click(uiItemEdit);
            Keyboard.SendKeys(uiItemEdit, value);
        }



    }

   
}
