﻿namespace IO.Practiceware.CodedUITests.Admin.AppointmentsUITestClasses
{

    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [CodedUITest]
#if DISABLE_UI_TESTS
    [Ignore]
#endif
    public partial class AppointmentsUITest : ApplicationTestsBase
    {
        private AppointmentsUITest _ui = null;


        [TestMethod]
        public void CreateAppointments()
        {
            _ui = new AppointmentsUITest();
            _ui.AppointmentForSmith();
            _ui.AppointmentForviola();
            _ui.AppointmentForSofia();
            
        }

        [TestMethod]
        public void CreateNewAppointmentFromCalendar()
        {
            _ui = new AppointmentsUITest();
            _ui.CreateApptForSamFromScheduler();

        }

        [TestMethod]
        public void RescheduleAppointment()
        {
            //this method will create an appointment and reschedule that appointment for kevin
            _ui = new AppointmentsUITest();
            _ui.RescheduleApptForViola();

        }

        [TestMethod]
        public void CancelAppontments()
        {
           //this method will create an appointment and Cancel that appointment for Mark 
            _ui = new AppointmentsUITest();
            _ui.CancelAppointmentForSofia();

        }

      

        protected override string ExePath
        {
            get { return @"%LocalAppData%\IO Practiceware\Administrator.exe"; }
        }
         
    }
}
