﻿namespace IO.Practiceware.CodedUITests.Admin.ConfigureHomeScreenClasses
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Input;
    using System.CodeDom.Compiler;
    using System.Text.RegularExpressions;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;


    [CodedUITest]
#if DISABLE_UI_TESTS
    [Ignore]
#endif

    public partial class ConfigureHomeScreen : ApplicationTestsBase
    {

        private ConfigureHomeScreen _ch = null;

        [TestMethod]
        public void ConfigureAdminHomeScreen()
        {
            _ch = new ConfigureHomeScreen();
            _ch.SetUpHomeScreen();

        }

        protected override string ExePath
        {
            get { return @"%LocalAppData%\IO Practiceware\Administrator.exe"; }
        }
    }
}
