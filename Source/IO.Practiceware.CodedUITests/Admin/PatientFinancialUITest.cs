﻿namespace IO.Practiceware.CodedUITests.Admin.PatientFinancialUITestClasses
{
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [CodedUITest]
#if DISABLE_UI_TESTS
    [Ignore]
#endif
    public partial class PatientFinancialUITest : ApplicationTestsBase
    {
        private PatientFinancialUITest _pf = null;

        [TestMethod]
        public void TestCreateManualClaims()
        {
            //This Method will create a manual claims.

            _pf = new PatientFinancialUITest();
           _pf.ManualClaimForThomas();
            //Assert.AreEqual(Common.RunSqlScript(Properties.Resources.CheckThomasPatientHasOnlyOneInvoice), 1);
        }

        [TestMethod]
        public void TestClaimBillingFromPatientFinancial()
        {
            //Test bill the claim with Patient
            _pf = new PatientFinancialUITest();
            _pf.ClaimBilledWithPatient();

        }

        [TestMethod]
        public void TestPostPaymentsFromPatientFinancial()
        {
            _pf = new PatientFinancialUITest();
            _pf.PostPaymentsFromPatient();
        }

        [TestMethod]
        public void AddManualClaimForBrandy()
        {
            _pf = new PatientFinancialUITest();
            _pf.CreateManualClaimforBrandy();
        }


        protected override string ExePath
        {
            get { return @"%LocalAppData%\IO Practiceware\Administrator.exe"; }
        }
    }
}
