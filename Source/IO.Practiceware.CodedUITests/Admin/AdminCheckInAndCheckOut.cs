﻿namespace IO.Practiceware.CodedUITests.Admin.AdminCheckInAndCheckOutClasses
{
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [CodedUITest]
#if DISABLE_UI_TESTS
    [Ignore]
#endif
    public partial class AdminCheckInAndCheckOut : ApplicationTestsBase
    {

        private AdminCheckInAndCheckOut _acio = null;

        [TestMethod]
        public void TestCheckInAppointment()
        {
            // This method will create an appointment and check-in that appointment.
            _acio = new AdminCheckInAndCheckOut();
            _acio.CheckInEvansAppt();
            //this.UIMap.CheckinTest();

        }
        
        protected override string ExePath
        {
            get { return @"%LocalAppData%\IO Practiceware\Administrator.exe"; }
        }

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }

}
