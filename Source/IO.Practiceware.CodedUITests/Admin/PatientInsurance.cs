﻿namespace IO.Practiceware.CodedUITests.Admin.PatientInsuranceClasses
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Input;
    using System.CodeDom.Compiler;
    using System.Text.RegularExpressions;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;

      [CodedUITest]
#if DISABLE_UI_TESTS
    [Ignore]
#endif
    public partial class PatientInsurance
    {
          private PatientInsurance _pi = null;

          [TestMethod]
          public void AddInsurancePolicyToBrandy()
          {
              _pi = new PatientInsurance();
              _pi.AddPolicyForBrandy();
          }
    }
}
