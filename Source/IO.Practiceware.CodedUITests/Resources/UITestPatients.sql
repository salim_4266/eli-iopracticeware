IF EXISTS(SELECT * FROM sysobjects WHERE name = 'CodedUITestPatients')
BEGIN
DROP TABLE CodedUITestPatients
END 

EXEC('CREATE TABLE CodedUITestPatients(
	FirstName nvarchar(max) NULL,
	LastName nvarchar(max) NOT NULL,
	MiddleName nvarchar(max) NULL,
	Suffix nvarchar(max) NULL,
	NickName nvarchar(max) NULL,
	EmployerName nvarchar(max) NULL,
	Address1 nvarchar(max) NULL,
	City nvarchar(max) NULL,
	State nvarchar(max) NULL,
	Zip nvarchar(max) NULL,
	AreaCode nvarchar(max) NULL,
	Phone nvarchar(max) NULL,
	CellPhone nvarchar(max) NULL,
	Prefix nvarchar(max) NULL,
	Email nvarchar(max) NULL,
	Honorific nvarchar(max) NULL,
	Salutation nvarchar(max) NULL,
	DateOfBirth nvarchar(255) NULL,
	Gender nvarchar(1) NULL,
	SocialSecurityNumber nvarchar(max) NULL,
	Occupation nvarchar(max) NULL,
	PriorLastName nvarchar(max) NULL,
	PriorFirstName nvarchar(max) NULL,
	BillingOrganizationId int NULL,
	DateOfDeath datetime NULL,
	
	
	) ')

EXEC('
INSERT INTO CodedUITestPatients (LastName, FirstName, Gender, DateOfBirth, Address1, City, State, Zip, AreaCode, Phone, Email)
VALUES (''ABAD'', ''JULIE'', ''M'',  ''12/20/1956'', ''RiverRoad'', ''Brooklyn'', ''NY'', ''11217'', ''205'',''8896633'', ''Abad@iop.com''),
(''Patient'', ''Smith'', ''M'', ''03/15/86'', ''Street4'', ''Brooklyn'', ''NY'', ''11218'', ''205'',''6685533'', ''smith@iop.com''),
(''Patient'', ''kevin'', ''M'', ''03/12/1984'', ''Street5'',  ''Brooklyn'', ''NY'', ''11218'', ''205'',''6685533'', ''kevin@iop.com''),
(''patient'', ''mark'', ''M'', ''03/15/86'', ''Street4'', ''Brooklyn'', ''NY'', ''11217'', ''207'',''7785533'', ''mark@iop.com''),
(''Patient'', ''clark'', ''M'', ''12/20/1956'', ''RiverRoad'', ''Brooklyn'', ''NY'', ''11217'', ''205'',''8896633'', ''clark@iop.com''),
(''ABAD'', ''IRIS'', ''M'', ''03/12/1984'', ''Street5'',  ''Brooklyn'', ''NY'', ''11218'', ''205'',''6685533'', ''iris@iop.com''),
(''Patient'', ''Henkel'', ''M'', ''03/12/1984'', ''Street5'',  ''Brooklyn'', ''NY'', ''11218'', ''205'',''6685533'', ''henkel@iop.com''),
(''Moore'', ''Patient'', ''M'', ''03/12/1984'', ''Street5'',  ''Brooklyn'', ''NY'', ''11218'', ''205'',''6115533'', ''moore@iop.com''),
(''Thomas'', ''Patient'', ''M'', ''03/12/1984'', ''Street5'',  ''Brooklyn'', ''NY'', ''11218'', ''205'',''2285533'', ''thomas@iop.com''),
(''Patient'', ''Sofia'', ''M'', ''03/12/1984'', ''Bank Street 2'',  ''Brooklyn'', ''NY'', ''10004'', ''205'',''2285533'', ''sofia@iop.com''),
(''Patient'', ''VIOLA'', ''M'', ''07/08/1974'', ''Bank Street'',  ''Brooklyn'', ''NY'', ''11216'', ''205'',''4485534'', ''vpatient@iop.com''),
(''Patient'', ''Brandy'', ''M'', ''10/21/1974'', ''Bakars Street'',  ''Brooklyn'', ''NY'', ''40009'', ''205'',''7755411'', ''bpatient@iop.com'')

')