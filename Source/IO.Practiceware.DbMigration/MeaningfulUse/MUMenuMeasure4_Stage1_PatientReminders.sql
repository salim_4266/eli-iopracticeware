/****** Object:  StoredProcedure [model].[USP_CMS_Stage1_PatientReminders]     Script Date: 12/17/2013 4:27:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--exec [model].[USP_CMS_Stage1_PatientReminders] '12/15/2013', '03/15/2014', 192
DECLARE @sql NVARCHAR(MAX)

SET @sql = '
CREATE PROCEDURE [model].[USP_CMS_Stage1_PatientReminders] 
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   

BEGIN    
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

--Denominator
select DISTINCT e.PatientId, e.Id, e.ConfirmationStatusId
into #Denominator
from model.encounters e
inner join model.Patients p on p.id = e.PatientId
inner join model.appointments a on a.EncounterId = e.id and a.UserId = @ResourceIds
inner join patientclinical pc on pc.appointmentid = e.id and clinicaltype = ''Q''
where DATEADD(yy,65,p.DateOfBirth) <= @StartDate OR DATEADD(yy,5,p.DateOfBirth) >= @StartDate
and e.EncounterStatusId = 7 and a.DateTime <= DATEADD(mi,1440,@EndDate)

--Numerator
select d.PatientId, ptj.TransactionID as ReminderId, CONVERT(datetime, TransactionDate) as ReminderSentDate
into #Numerator
from #Denominator d
inner join PracticeTransactionJournal PTJ on ptj.TransactionTypeId = d.PatientId
inner join Resources re on re.ResourceName = REVERSE(SUBSTRING(REVERSE(PTJ.TransactionRef),0,CHARINDEX(''/'',REVERSE(PTJ.TransactionRef),0)))
where PTJ.TransactionType = ''L'' and PTJ.TransactionStatus = ''S''
and ptj.transactiontypeid = d.PatientId
and re.ResourceId = @ResourceIds
and CONVERT(datetime, TransactionDate) between @StartDate and DATEADD(mi,1440,@EndDate)--DATEADD so that EndDate includes the whole day of encounters

UNION ALL

select d.PatientId, d.Id AS ReminderId, NULL AS ReminderSentDate
from #Denominator d
where d.ConfirmationStatusId is not null


SET @TotParm = (SELECT COUNT (DISTINCT PatientID) from #Denominator)
SET @CountParm = (SELECT COUNT (DISTINCT PatientID) from #Numerator)

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId

select d.PatientId, p.FirstName, p.LastName, d.ReminderSentDate
from #Numerator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.ReminderSentDate

DROP TABLE #Denominator
DROP TABLE #Numerator

END 

'

IF OBJECT_ID('model.USP_CMS_Stage1_PatientReminders') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)