---------------MU CQM #55 DM with Dilated Exam----------------

DECLARE @sql nvarchar(max)
SET @sql = N'

CREATE PROCEDURE [dbo].[USP_NQM_55_DM_with_DilatedExam]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;
	SET @TotParm=0;
	SET @CountParm=0;

	CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
	IF @ResourceIds=''''
	BEGIN
	
		INSERT INTO #TMPResources 
		SELECT Resources.ResourceId 
		FROM Resources 
		WHERE ResourceType IN (''D'', ''Q'')
	END

	ELSE
	BEGIN
	
		DECLARE @TmpResourceIds VARCHAR(100)
		DECLARE @ResId VARCHAR(10)
	
		SELECT @TmpResourceIds =@ResourceIds
		WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
			BEGIN
				SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
				SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    
				INSERT INTO #TMPResources values(@ResId)
			END
			INSERT INTO #TMPResources values(@TmpResourceIds)
	END


		-----------CQM #55 DM Denominator
		
		----Age 18-75 by reporting end date
		SELECT pd.PatientId 
		INTO #Age
		FROM PatientDemographics pd
		WHERE DATEDIFF (yyyy, CONVERT(datetime, pd.BirthDate), CONVERT(datetime, @EndDate)) BETWEEN 18 AND 75


		----two non-acute office visits ever WITH OUR DOCTOR
		SELECT a.PatientId
		INTO #TwoNonAcuteVisits
		FROM #Age a
		INNER JOIN PatientReceivables pr ON pr.PatientId = a.PatientId
		INNER JOIN PatientReceivableServices prs ON prs.invoice = pr.invoice 
			AND prs.Service IN (''99201'', ''99202'', ''99203'', ''99204'', ''99205'', ''99211'', ''99212'', ''99213'', ''99214'', ''99215'', ''99217'', ''99218'', ''99219'', ''99220''
				, ''99241'', ''99242'', ''99243'', ''99244'', ''99245''
				, ''99341'', ''99342'', ''99343'', ''99344'', ''99345'', ''99347'', ''99348'', ''99349'', ''99350''
				, ''99384'', ''99385'', ''99386'', ''99387''
				, ''99394'', ''99395'', ''99396'', ''99397'', ''99401'', ''99402'', ''99403'', ''99404'', ''99411'', ''99412'', ''99420'', ''99429'', ''99455'', ''99456''
				, ''92002'', ''92003'', ''92004'', ''92005'', ''92006'', ''92007'', ''92008'', ''92009'', ''92010'', ''92011'', ''92012'', ''92013'', ''92014''
				, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310'', ''99315'', ''99316'', ''99318''
				, ''99324'', ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337'')
			AND prs.Status=''A''
		INNER JOIN Resources re on re.ResourceId = pr.BillToDr 
			AND re.ResourceId IN (SELECT #TMPResources.ResourceId FROM #TMPResources)
		GROUP BY a.PatientId, pr.AppointmentId
		HAVING COUNT(*) > 1

		----one acute visit ever WITH OUR DOCTOR
		SELECT a.PatientId
		INTO #OneAcuteVisit
		FROM #Age a
		INNER JOIN PatientReceivables pr ON pr.PatientId = a.PatientId
		INNER JOIN PatientReceivableServices prs ON prs.invoice = pr.invoice 
			AND prs.Service IN (''99221'', ''99222'', ''99223'', ''99231'', ''99232'', ''99233'', ''99238'', ''99239''
			, ''99251'', ''99252'', ''99253'', ''99254'', ''99255'', ''99291''
			, ''99281'', ''99282'', ''99283'', ''99284'', ''99285'')
			AND prs.Status=''A''
		INNER JOIN Resources re on re.ResourceId = pr.BillToDr 
			AND re.ResourceId IN (SELECT #TMPResources.ResourceId FROM #TMPResources)
		

		---Candidates - had visits, correct age
		SELECT *
		INTO #Visits
		FROM #TwoNonAcuteVisits

		UNION

		SELECT *
		FROM #OneAcuteVisit



		-----Diagnosis of Diabetes within two years of Reporting End Date
		SELECT v.PatientId 
		INTO #DM
		FROM #Visits v
		INNER JOIN Appointments ap2Y on ap2Y.patientid = v.patientid
			AND DATEDIFF (mm, CONVERT(datetime, @EndDate), CONVERT(datetime, ap2Y.appdate)) BETWEEN -24 AND 0
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap2Y.AppointmentId
			AND pc.ClinicalType = ''Q'' 
			AND pc.Status=''A''
			AND CASE 
				WHEN LEN(FindingDetail) > 6
					THEN SUBSTRING(FindingDetail, 1, LEN(FindingDetail)-CHARINDEX(''.'',REVERSE(findingdetail)))
				ELSE FindingDetail
				END IN (''250'', ''250.0'', ''250.00'', ''250.01'', ''250.02'', ''250.03''
				, ''250.10'', ''250.11'', ''250.12'', ''250.13''
				, ''250.20'', ''250.21'', ''250.22'', ''250.23''
				, ''250.30'', ''250.31'', ''250.32'', ''250.33''
				, ''250.4'', ''250.40'', ''250.41'', ''250.42'', ''250.43''
				, ''250.50'', ''250.51'', ''250.52'', ''250.53''
				, ''250.60'', ''250.61'', ''250.62'', ''250.63''
				, ''250.7'', ''250.70'', ''250.71'', ''250.72'', ''250.73''
				, ''250.8'', ''250.80'', ''250.81'', ''250.82'', ''250.83''
				, ''250.9'', ''250.90'', ''250.91'', ''250.92'', ''250.93''
				, ''357.2''
				, ''362.0'', ''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'', ''362.07''
				, ''366.41''
				, ''648.0'', ''648.00'', ''648.01'', ''648.02'', ''648.03'', ''648.04'')
		
		
		
		-----Taking diabetic medications within two years of Reporting End Date (NO OV REQUIREMENT; have to look at all visits for active meds)
		SELECT a.PatientId 
		INTO #Meds
		FROM #Age a
		INNER JOIN PatientClinical pc ON pc.PatientId = a.PatientId
			AND pc.ClinicalType = ''A'' 
			AND pc.Status=''A''
			AND SUBSTRING(pc.FindingDetail, 1, 3) = ''RX-''
			AND SUBSTRING(ImageDescriptor, 1, 1) IN (''!'', ''K'', ''R'', '''')
			AND SUBSTRING(pc.FindingDetail, 6, CHARINDEX(''-2/'', pc.FindingDetail)-6) IN
				(SELECT MED_ROUTED_DF_MED_ID_DESC  
					FROM FDB.tblCompositeDrug 
					WHERE 
						etc LIKE ''%Amylin Analog%'' OR
						etc LIKE ''%Biguanid%'' OR
						etc LIKE ''%diabet%'' OR
						etc LIKE ''%glucosidas%'' OR
						etc LIKE ''%insulin%'' OR
						etc LIKE ''%Meglitinid%'' OR
						etc LIKE ''%Sulfonylure%'' OR
						etc LIKE ''%Thiazolidinedi%'' 
				)
		INNER JOIN PatientReceivables pr ON pr.AppointmentId = pc.AppointmentId
		INNER JOIN Resources re on re.ResourceId = pr.BillToDr 
		INNER JOIN #TMPResources tmp ON tmp.ResourceId = re.ResourceId
		
		---Denominator before exclusions
		SELECT *
		INTO #DenominatorBeforeExclusions
		FROM #DM

		UNION

		SELECT *
		FROM #Meds

		----exclusions - polycystic ovaries, gestational diabetes, steroid induced diabetes
		SELECT dbe.PatientId
		INTO #Exclusions
		FROM #DenominatorBeforeExclusions dbe
		INNER JOIN Appointments ap2Y on ap2Y.patientid = dbe.patientid
			AND DATEDIFF (mm, CONVERT(datetime, @EndDate), CONVERT(datetime, ap2Y.appdate)) BETWEEN -24 AND 0
		INNER JOIN PatientClinical pc ON ap2Y.AppointmentId = pc.AppointmentId
			AND pc.ClinicalType = ''Q''
			AND pc.Status = ''A''
			AND CASE 
				WHEN LEN(FindingDetail) > 6
					THEN SUBSTRING(FindingDetail, 1, LEN(FindingDetail)-CHARINDEX(''.'',REVERSE(findingdetail)))
				ELSE FindingDetail
				END IN (''256.4''
					, ''648.8'', ''648.80'', ''648.81'', ''648.82'', ''648.83'', ''648.84''
					, ''249'', ''249.0'', ''249.00'', ''249.01'', ''249.1'', ''249.10'', ''249.11''
					, ''249.2'', ''249.20'', ''249.21'', ''249.3'', ''249.30'', ''249.31''
					, ''249.4'', ''249.40'', ''249.41'', ''249.5'', ''249.50'', ''249.51''
					, ''249.6'', ''249.60'', ''249.61'', ''249.7'', ''249.70'', ''249.71''
					, ''249.8'', ''249.80'', ''249.81'', ''249.9'', ''249.90'', ''249.91''
					, ''251.8'', ''962.0'')
		GROUP BY dbe.PatientId

		--CQM #55 Denominator
		SELECT dbe.PatientId
		INTO #Denominator
		FROM #DenominatorBeforeExclusions dbe
		LEFT JOIN #Exclusions e on e.PatientId = dbe.PatientId
		WHERE e.PatientId IS NULL


		--Eye Exam had during reporting period 
		----Dilation and Findings at back of eye 
		SELECT d.PatientId
		INTO #Numerator
		FROM #Denominator d
			INNER JOIN Appointments ap on ap.PatientId = d.patientid 
				AND ap.AppDate BETWEEN @StartDate AND @EndDate
			--Dilated
			INNER JOIN PatientClinical pcDilation on pcDilation.AppointmentId = ap.AppointmentId 
				AND (
						(pcDilation.ClinicalType = ''F'' 
							AND pcDilation.Symptom IN 
								(''/DILATION''
								, ''/DILATED IOP PROVOCATIVE''
								, ''/DILATED REFRACTION''
								, ''/FA''
								, ''/FA AND FP''
								, ''/FA AND FP TECH''
								, ''/FLUORESCEIN ANGIO''
								, ''/FLUORESCEIN ANGIO TECH''
								, ''/FLUORESCEIN ANGIOGRAPHY''
								, ''/ICG ANGIOGRAPHY''
								, ''/ICG TECH''
								, ''/INFRARED''
								, ''/INFRARED TECH''
								, ''/OPHTHALMOSCOPY INITIAL''
								, ''/OPHTHALMOSCOPY SUBSEQ''
								, ''/OPHTHALMOSCOPY, DETECTION''
								, ''/OPHTHALMOSCOPY, MANAGE''
								, ''/PQRI (ARMD DILATED MACULAR EXAM)''
								, ''/PQRI (DM RETINOPATHY DOCUMENT)''
								, ''/PQRI (DM RETINOPATHY INFO)''
								, ''/PQRI (DM1 DILATED EXAM IN DM)''
								)
						)
						OR 
						(pcDilation.ClinicalType = ''Q'' 
							AND pcDilation.Symptom LIKE ''%&%'')
					)
				AND pcDilation.Status = ''A''
			--Any Back of the Eye Findings
			INNER JOIN PatientClinical pcFindings ON pcFindings.AppointmentId = pcDilation.AppointmentId
				AND pcFindings.ClinicalType = ''Q''
				AND pcFindings.ImageDescriptor IN (''M'', ''N'', ''O'', ''P'')
				AND pcFindings.Status = ''A''
			GROUP BY d.PatientId
	
			UNION

			----Imaging of back of eye is also a retinal exam
			SELECT d.PatientId
			FROM #Denominator d
			INNER JOIN Appointments ap on ap.PatientId = d.patientid 
				AND ap.AppDate BETWEEN @StartDate AND @EndDate	
			INNER JOIN PatientClinical pcImaging on pcImaging.AppointmentId = ap.AppointmentId 
				AND pcImaging.ClinicalType = ''F'' 
				AND pcImaging.Status = ''A''
				AND pcImaging.Symptom IN 
					(''/AUTOFLUORESCENCE''
					, ''/FA''
					, ''/FA AND FP''
					, ''/FLUORESCEIN ANGIO''
					, ''/FLUORESCEIN ANGIOGRAPHY''
					, ''/FUNDUS PHOTO''
					, ''/FUNDUS PHOTO (FP)''
					, ''/FUNDUS PHOTO (RETINA)''
					, ''/GDX''
					, ''/HRT''
					, ''/HRT, MACULA''
					, ''/ICG ANGIOGRAPHY''
					, ''/INFRARED''
					, ''/OCT''
					, ''/OCT, CENTRAL MACULAR THICKNESS''
					, ''/OCT H''
					, ''/OCT, MACULA''
					, ''/RED FREE''
					, ''/SCANNING LASER POLARIMETRY''
					, ''/STEREO  PHOTO'')

			UNION

			--or Low Risk -- no DR finding in year before reporting period
			SELECT d.PatientId
			FROM #Denominator d
			INNER JOIN Appointments App1Year ON App1Year.PatientId = d.PatientId
				AND DATEDIFF (yyyy, CONVERT(datetime, App1Year.AppDate), CONVERT(datetime, @StartDate)) = 1
				AND App1Year.ScheduleStatus = ''D''
			INNER JOIN PatientClinical pcLowRisk ON pcLowRisk.AppointmentId = App1Year.AppointmentId
				AND pcLowRisk.Status = ''A''
				AND pcLowRisk.ClinicalType IN (''B'', ''K'')
				AND SUBSTRING(pcLowRisk.FindingDetail, 1, 6) NOT IN (''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
		
		SELECT @TotParm=COUNT(*)  
		FROM #Denominator

		SELECT @CountParm=COUNT(*)
		FROM #Numerator

		DROP TABLE #TMPResources
		DROP TABLE #Age
		DROP TABLE #TwoNonAcuteVisits
		DROP TABLE #OneAcuteVisit
		DROP TABLE #Visits
		DROP TABLE #DM
		DROP TABLE #Meds
		DROP TABLE #Exclusions
		DROP TABLE #DenominatorBeforeExclusions
		DROP TABLE #Denominator
		DROP TABLE #Numerator

		SELECT @TotParm as Denominator,@CountParm as Numerator

END

'

IF OBJECT_ID('USP_NQM_55_DM_with_DilatedExam') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)