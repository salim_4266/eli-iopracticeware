/****** Object:  StoredProcedure [qrda].[QRDA_HighBloodPressure_CMS165v3]    Script Date: 09/03/2014 16:34:13 ******/
/***FOR DROPPING (IF EXIST) *******/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_HighBloodPressure_CMS165v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [qrda].[QRDA_HighBloodPressure_CMS165v3]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_HighBloodPressure_CMS165v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [qrda].[QRDA_HighBloodPressure_CMS165v3]
(
@StartDate datetime,
@EndDate datetime,
@UserId int
)
AS
BEGIN
	
		---Denominator - Patients 18-84 with 1 office visit in the reporting period and dx of hypertension
		SELECT DISTINCT pd.Id
			,pd.FirstName + pd.LastName AS Name
			,0 as IsInNumerator
		INTO #Denominator
		FROM model.Patients pd
		INNER JOIN dbo.Appointments apOV ON apOV.PatientId = pd.Id
			AND @UserId = apOV.ResourceId1
			AND apOV.AppTime > 0
		INNER JOIN PatientReceivableServices prs ON prs.AppointmentId = apOV.AppointmentId
			AND prs.Service in (
				--
				''92002'', ''92004'', ''92012'', ''92014''
				--office visit
				, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''
				, ''99212'', ''99213'', ''99214'', ''99215''
				--
				, ''99241'', ''99242'', ''99243'', ''99244'', ''99245''
				, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310''
				, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''
				, ''99334'', ''99335'', ''99336'', ''99337'')  
			AND prs.Status=''A''
			AND CONVERT(datetime, prs.ServiceDate) BETWEEN @StartDate AND @EndDate
		INNER JOIN dbo.Appointments apHyper ON apHyper.PatientId = pd.Id
		--TODO - when measurement period is one year, must calculate "within the first 6 months of the measurement period"
			AND CONVERT(datetime, apHyper.AppDate) <= @EndDate
		INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = apHyper.AppointmentId
			AND pc.ClinicalType IN (''B'', ''U'', ''Q'')
			AND pc.Status = ''A''
			AND pc.FindingDetail IN (''401.0'', ''401.1'', ''401.9'', ''402.00'', ''402.01'', ''402.10'', ''402.11'', ''402.90'', ''402.91'', ''403.00'', ''403.01'', ''403.10'', ''403.11'', ''403.90'', ''403.91'', ''404.00'', ''404.01'', ''404.02'', ''404.03'', ''404.10'', ''404.11'', ''404.12'', ''404.13'', ''404.90'', ''404.91'', ''404.92'', ''404.93'')
		WHERE DATEDIFF (yyyy, pd.DateOfBirth, @StartDate) BETWEEN 18 and 84
			AND pd.LastName <> ''TEST''

		SELECT DISTINCT d.Id
			, d.Name
			, 1 AS IsInNumerator
		INTO #Numerator
		FROM #Denominator d
		INNER JOIN dbo.Appointments a ON d.Id = a.PatientId
			AND a.AppDate BETWEEN @StartDate AND @EndDate
		INNER JOIN model.PatientBloodPressures bp ON a.EncounterId = bp.EncounterId
			AND bp.DiastolicPressure < 90
			AND bp.SystolicPressure < 140

		--Output
			SELECT DISTINCT
			 d.Id
			,d.Name
			,CONVERT(bit, d.IsInNumerator + COALESCE(n.IsInNumerator, 0)) AS IsInNumerator
		FROM #Denominator d
		LEFT JOIN #Numerator n ON n.id = d.id

	END
'
END
GO