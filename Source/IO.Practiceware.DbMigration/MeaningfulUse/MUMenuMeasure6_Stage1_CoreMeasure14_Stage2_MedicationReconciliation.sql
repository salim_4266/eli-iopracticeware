/****** Object:  StoredProcedure [model].[USP_CMS_Stage2_MedicationReconciliation]    Script Date: 9/17/2014 8:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec model.USP_CMS_Stage2_MedicationReconciliation '2013-12-15', '2014-03-15', 192

DECLARE @sql NVARCHAR(max)

SET @sql = 
	N'
ALTER PROCEDURE [model].[USP_CMS_Stage2_MedicationReconciliation] (
	@StartDate NVARCHAR(200)
	,@EndDate NVARCHAR(200)
	,@ResourceIds NVARCHAR(1000)
	)
AS
BEGIN
	---#15-Medication Reconclication--------------------------------------------------
	DECLARE @TotParm INT
	DECLARE @CountParm INT

	SET @TotParm = 0
	SET @CountParm = 0

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT ipp.*
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
	INNER JOIN PatientClinical pc ON pc.AppointmentId = ipp.EncounterId
	WHERE Symptom = ''/MEDS RECONCILIATION''
		AND FindingDetail IN (
			''*SUMMARYOFCARENONELECTRONIC-MEDREC=T23294 <SUMMARYOFCARENONELECTRONIC-MEDREC>''
			,''*SUMMARYOFCAREELECTRONIC-MEDREC=T23293 <SUMMARYOFCAREELECTRONIC-MEDREC>''
			,''*TRANSITIONOFCARE-MEDREC=T14911 <TRANSITIONOFCARE-MEDREC>''
			,''*MEDRECPERFORMED-MEDREC=T14913 <MEDRECPERFORMED-MEDREC>''
			)

	--Numerator
	SELECT d.EncounterId
		,pc.PatientId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientClinical pc ON pc.AppointmentId = d.EncounterId
	WHERE Symptom = ''/MEDS RECONCILIATION''
		AND FindingDetail = ''*MEDRECPERFORMED-MEDREC=T14913 <MEDRECPERFORMED-MEDREC>''

	SET @TotParm = (
			SELECT COUNT(DISTINCT EncounterId)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT EncounterId)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.EncounterId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
'

IF OBJECT_ID('model.USP_CMS_Stage2_MedicationReconciliation') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)
