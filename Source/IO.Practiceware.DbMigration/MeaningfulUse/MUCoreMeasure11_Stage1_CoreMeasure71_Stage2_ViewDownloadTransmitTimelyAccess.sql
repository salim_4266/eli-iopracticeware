/****** Object:  StoredProcedure [model].[USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess]    Script Date: 12/17/2013 2:37:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
			
--exec [model].[USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess] '2014-01-01', '2014-09-15', 205
DECLARE @sql NVARCHAR(MAX)
SET @sql = '
CREATE PROCEDURE [model].[USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess]    
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   

BEGIN    

--SET @StartDate = CONVERT(DATETIME, CONVERT(NVARCHAR(10),@StartDate,101))
--SET @EndDate = CONVERT(DATETIME, CONVERT(NVARCHAR(10),@EndDate,101))
		
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
   

--Numerator -- either messages generated or practice providing access

select d.PatientId, EncounterId, EncounterDate
into #Numerator
from #Denominator d
inner join model.ExternalSystemMessagePracticeRepositoryEntities esmpre on esmpre.PracticeRepositoryEntityKey = d.PatientId and esmpre.PracticeRepositoryEntityId in (select Id from model.PracticeRepositoryEntities where Name = ''Patient'')
inner join model.PracticeRepositoryEntities pre on pre.id = esmpre.PracticeRepositoryEntityId
inner join model.ExternalSystemMessages esm on esm.id = esmpre.ExternalSystemMessageId and Description like ''%CCDA%_Outbound'' and error is null
where DATEDIFF (dd,d.EncounterDate,[IO.Practiceware.SqlClr].ConvertToClientDateTime([IO.Practiceware.SqlClr].ConvertToLocalDateTime(esm.CreatedDateTime))) <= 4


SET @TotParm = (SELECT COUNT (DISTINCT PatientID) from #Denominator)
SET @CountParm = (SELECT COUNT (DISTINCT PatientID) from #Numerator)

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.EncounterDate
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.EncounterDate, d.EncounterId

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.EncounterDate
from #Numerator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.EncounterDate, d.EncounterId

DROP TABLE #Denominator
DROP TABLE #Numerator

END 
'

IF OBJECT_ID('model.USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)
