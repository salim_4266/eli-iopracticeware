-------------------------Measure #19 DR Communication DR  PQRS ----------------------

DECLARE @sql nvarchar(max)
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_PQRI_GetDiabetic_Retinopathy_Communication_Physician]
(
@StartDate nvarchar(100),
@EndDate nvarchar(100),
@ResourceIds nvarchar(1000)
)
As

BEGIN



	CREATE TABLE #TMPResources(ResourceId nvarchar(1000))

	IF @ResourceIds=''''

		BEGIN

			INSERT INTO #TMPResources 

			SELECT Resources.ResourceId 

			FROM Resources 

			WHERE ResourceType IN(''D'',''Q'')

		END

	ELSE

		BEGIN

			DECLARE @TmpResourceIds varchar(100)

			DECLARE @ResId varchar(10)

			SELECT @TmpResourceIds =@ResourceIds

			WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0

				BEGIN

					SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)

					SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    

					INSERT INTO #TMPResources values(@ResId)

				END

				INSERT INTO #TMPResources values(@TmpResourceIds)

		END





	--#19 DR Denominator

	SELECT p.PatientId AS patient_os_id

		,ap.AppointmentId AS OVAppointmentId

		,MIN(pcDx.Symptom) AS DxRank

		,p.FirstName AS tq_firstname

		,p.LastName AS tq_lastname

		,ap.AppDate AS tq_encounterdt

		,re.NPI AS tq_physiciannpi

		,p.BirthDate AS tq_dob

		,CASE p.Gender

			WHEN ''M'' THEN ''M''

			WHEN ''F'' THEN ''F''

			ELSE ''U''

			END AS tq_gender

		,''1'' AS tq_medicare2

		,ISNULL(MAX(SUBSTRING(icd10.FindingDetailIcd10, 1, 6)),MAX(SUBSTRING(pcDx.FindingDetail, 1, 6))) AS tq_icd  

		,prs.Service AS tq_em

	INTO #DRDenominator

	FROM PatientDemographics p

	INNER JOIN Appointments ap ON p.PatientId = ap.PatientId

	INNER JOIN PatientClinical pcDx ON ap.AppointmentId = pcDx.AppointmentId

		AND pcDx.ClinicalType IN (''B'', ''K'')

		AND pcDx.Status = ''A''

	INNER JOIN PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId

	INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId

		AND pri.InsurerReferenceCode IN (''N'', ''M'', ''['')

	INNER JOIN Resources re ON re.ResourceId = pr.BillToDr

		AND re.ResourceId IN (SELECT #TMPResources.ResourceId from #TMPResources)

	INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice

		AND prs.Status = ''A''

	 LEFT JOIN (SELECT AppointmentId,FindingDetailIcd10 FROM PatientClinicalIcd10 
				WHERE ClinicalType IN (''B'', ''K'')
				AND Status = ''A''
				AND  FindingDetailIcd10 IN (''E08.311'',''E08.319'',''E08.321'',''E08.329'',''E08.331'',''E08.339'',''E08.341'',''E08.349'',''E08.351'',
										''E08.359'',''E09.311'',''E09.319'',''E09.321'',''E09.329'',''E09.331'',''E09.339'',''E09.341'',''E09.349'',''E09.351'',''E09.359'',''E10.311'',''E10.319'',''E10.321'',
										''E10.329'',''E10.331'',''E10.339'',''E10.341'',''E10.349'',''E10.351'',''E10.359'',''E11.311'',''E11.319'',''E11.321'',''E11.329'',''E11.331'',''E11.339'',''E11.341'',
										''E11.349'',''E11.351'',''E11.359'',''E13.311'',''E13.319'',''E13.321'',''E13.329'',''E13.331'',''E13.339'',''E13.341'',''E13.349'',''E13.351'',''E13.359''
										)
				) AS icd10 ON icd10.AppointmentId=ap.AppointmentId

	WHERE 

		--appointment during reporting period

		ap.AppDate BETWEEN @StartDate AND @EndDate

		--over 18 years

		AND DATEDIFF (yyyy, CONVERT(datetime, BirthDate), CONVERT(datetime, ap.AppDate)) >= 18

		--diagnosis of Diabetic Retinopathy

		AND SUBSTRING(pcDx.FindingDetail, 1, 6) IN (''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
			
		--with office visit

		AND prs.Service IN (''92002'', ''92004'', ''92012'', ''92014''

			, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''

			, ''99212'', ''99213'', ''99214'', ''99215''

			, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310''

			, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''

			, ''99334'', ''99335'', ''99336'', ''99337'') 

		AND p.LastName <> ''TEST''

	 GROUP BY  ap.AppointmentId

		,p.PatientId 

		,p.FirstName 

		,p.LastName 

		,ap.AppDate

		,re.NPI 

		,p.BirthDate

		,p.Gender

		,prs.Service 





	--#19 DR Numerator

	---Dilation, Mac Edema

	SELECT p.patient_os_id

		, 1 AS tq_fundus

	INTO #DRNumeratorDilate

	FROM #DRDenominator p

	INNER JOIN Appointments App1Year ON App1Year.PatientId = patient_os_id

		AND DATEDIFF (mm, CONVERT(datetime, App1Year.AppDate), CONVERT(datetime, p.tq_encounterdt)) BETWEEN 0 and 12

		AND App1Year.AppDate BETWEEN @StartDate AND @EndDate

	--Dilation

	INNER JOIN PatientClinical pcDilation ON pcDilation.AppointmentId = App1Year.AppointmentId

		AND pcDilation.Status = ''A''

		AND (

				(pcDilation.ClinicalType = ''F'' AND pcDilation.Symptom IN (''/DILATION'', ''/PQRI (DM RETINOPATHY DOCUMENT)''))

				OR 

				(pcDilation.ClinicalType = ''Q'' AND pcDilation.Symptom LIKE ''%&%'')

			)

	--Severity

	INNER JOIN PatientClinical pcSeverity ON App1Year.AppointmentId = pcSeverity.AppointmentId

		AND pcSeverity.ClinicalType = ''Q''

		AND pcSeverity.Status = ''A''

		AND pcSeverity.FindingDetail IN (''362.02'', ''362.04'', ''362.05'', ''362.06'')

	--Macular edema

	INNER JOIN PatientClinical pcMacEdema ON App1Year.AppointmentId = pcMacEdema.AppointmentId

		AND pcMacEdema.Status = ''A''

		AND pcMacEdema.ClinicalType = ''Q''

		AND pcMacEdema.FindingDetail IN (

			''362.07'',

			''362.07.01'',

			''362.07.02'',

			''362.07.03'',

			''362.07.04'',

			''362.07.05'',

			''362.16.11'',

			''362.53'',

			''362.53.1'',

			''362.83'',

			''362.83.2'',

			''362.83.23'',

			''362.83.4'',

			''362.83.5'',

			''362.83.77'',

			''366.16.44'',

			''377.0'',

			''377.00'',

			''377.00.11'',

			''377.00.12'',

			''377.00.13'',

			''377.00.14'',

			''377.01'',

			''377.02'',

			''377.03'',

			''377.24'',

			''377.49.01'',

			''P158'',

			''P405'',

			''P427 '',

			''P978'',

			''P979'',

			''Q009'',

			''Q027'',

			''Q029'',

			''Q100'',

			''Q151'',

			''Q173'',

			''Q909'',

			''S612'',

			''S743''

			)

	GROUP BY p.patient_os_id



	---Communication

	SELECT p.patient_os_id

		, 1 AS tq_funduscom

	INTO #DRNumeratorComm

	FROM #DRDenominator p

	INNER JOIN Appointments App1Year ON App1Year.PatientId = patient_os_id

		AND DATEDIFF (mm, CONVERT(datetime, App1Year.AppDate), CONVERT(datetime, p.tq_encounterdt)) BETWEEN 0 and 12

		AND App1Year.AppDate BETWEEN @StartDate AND @EndDate

	INNER JOIN PatientClinical pcCommunicate ON pcCommunicate.AppointmentId = App1Year.AppointmentId

		AND pcCommunicate.Status = ''A''

		AND pcCommunicate.ClinicalType = ''A'' 

		AND SUBSTRING(pcCommunicate.FindingDetail, 1, 26) = ''SEND A CONSULTATION LETTER''

	GROUP BY p.patient_os_id





	--#19 DR output

	SELECT d.patient_os_id AS PatientId

		,d.tq_firstname AS FirstName

		,d.tq_lastname AS LastName

		,CONVERT(nvarchar(10), CONVERT(datetime, d.tq_encounterdt),101) AS EncounterDate

		,d.tq_physiciannpi AS PhysicianNPI

		,CONVERT(nvarchar(10), CONVERT(datetime, d.tq_dob),101) AS PatientDateOfBirth

		,d.tq_gender AS PatientGender

		,d.tq_medicare2 AS IsMedicare

		,d.tq_icd AS ICDCode

		,d.tq_em AS EM

		,CASE WHEN dn.tq_fundus IS NULL THEN ''G8398''

			WHEN dn.tq_fundus IS NOT NULL AND c.tq_FundusCom is NULL THEN ''5010F 8P G8397''

			WHEN dn.tq_fundus IS NOT NULL AND c.tq_FundusCom IS NOT NULL THEN ''5010F''

			END AS Numerator

	FROM #DRDenominator d

	LEFT JOIN #DRNumeratorDilate dn ON dn.patient_os_id = d.patient_os_id

	LEFT JOIN #DRNumeratorComm c ON c.patient_os_id = d.patient_os_id

	GROUP BY d.patient_os_id

		,d.tq_firstname

		,d.tq_lastname

		,tq_encounterdt

		,d.tq_physiciannpi

		,d.tq_dob

		,d.tq_gender

		,d.tq_medicare2

		,d.tq_icd   

		,d.tq_em

		,tq_fundus

		,tq_funduscom

	ORDER BY d.patient_os_id, tq_encounterdt



	DROP TABLE #TMPResources

	DROP TABLE #DRDenominator

	DROP TABLE #DRNumeratorDilate

	DROP TABLE #DRNumeratorComm



END
'
IF OBJECT_ID('USP_PQRI_GetDiabetic_Retinopathy_Communication_Physician') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)