/****** Object:  StoredProcedure [model].[USP_CMS_Stage1_VitalSigns2013]    Script Date: 12/17/2013 8:14:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [model].[USP_CMS_Stage1_VitalSigns2013] '12/15/2013', '03/15/2014', 192
DECLARE @sql NVARCHAR(MAX)
SET @sql = '
CREATE PROCEDURE [model].[USP_CMS_Stage1_VitalSigns2013]    
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   

BEGIN    
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

--Denominator
select PatientId, e.Id as EncounterId, a.DateTime as AppointmentDate
into #Denominator
from model.encounters e
inner join model.appointments a on a.EncounterId = e.id
inner join model.patients p on p.id = e.PatientId
where a.DateTime between @StartDate and DATEADD(mi,1440,@EndDate) --DATEADD so that EndDate includes the whole day of encounters
and e.EncounterStatusId = 7
and a.UserId = @ResourceIds
and DATEADD(yy,2,p.DateOfBirth) <= a.DateTime
   
--Numerator    
select distinct d.*
into #Numerator
from #Denominator d
inner join model.PatientHeightAndWeights hw on hw.PatientId = d.PatientId
inner join model.PatientBloodPressures bp on bp.PatientId = d.PatientId
where HeightUnit is not null and WeightUnit is not null and SystolicPressure is not null and DiastolicPressure is not null

SET @TotParm = (SELECT COUNT (DISTINCT PatientID) from #Denominator)
SET @CountParm = (SELECT COUNT (DISTINCT PatientID) from #Numerator)

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.AppointmentDate
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

select d.PatientId, p.FirstName, p.LastName
from #Numerator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

DROP TABLE #Denominator
DROP TABLE #Numerator

END
'

IF OBJECT_ID('model.USP_CMS_Stage1_VitalSigns2013') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)