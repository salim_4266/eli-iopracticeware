/****** Object:  StoredProcedure [qrda].[QRDA_CurrentMeds_CMS068v3]    Script Date: 9/4/2014 11:22:24 AM ******/
/***FOR DROPPING (IF EXIST) *******/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_CurrentMeds_CMS068v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [qrda].[QRDA_CurrentMeds_CMS068v3]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_CurrentMeds_CMS068v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [qrda].[QRDA_CurrentMeds_CMS068v3]
(
@StartDate datetime,
@EndDate datetime,
@UserId int
)
AS
BEGIN

		--Initial Patient Population
		SELECT p.Id
			,p.FirstName +'' ''+ p.LastName AS Name
			,apOV.appointmentId as EncounterId
			,0 AS IsInNumerator
			,ResourceId1
		INTO #Denominator
		FROM model.Patients p
		INNER JOIN Appointments apOV ON apOV.PatientId = p.id 
			AND @UserId = apOV.ResourceId1
			AND apOV.AppTime > 0
		INNER JOIN PatientReceivableServices prs on prs.AppointmentId = apOV.AppointmentId
			AND Service IN (
			--Medications Encounter Code Set
			''90791'', ''90792'', ''90832'', ''90834'', ''90837'', ''90839'', ''90957'', ''90958'', ''90959'', ''90960'', ''90962'', ''90965'', ''90966'', ''92002'', ''92004'', ''92012'', ''92014''
			, ''92507'', ''92508'', ''92526'', ''92541'', ''92542'', ''92543'', ''92544'', ''92545'', ''92547'', ''92548'', ''92557'', ''92567'', ''92568'', ''92570'', ''92585'', ''92588'', ''92626''
			, ''96116'', ''96150'', ''96151'', ''96152'', ''97001'', ''97002'', ''97003'', ''97004'', ''97110'', ''97140'', ''97532'', ''97802'', ''97803'', ''97804'', ''98960'', ''98961'', ''98962''
			, ''99201'', ''99202'', ''99203'', ''99204'', ''99205'', ''99212'', ''99213'', ''99214'', ''99215'', ''99324'', ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337''
			, ''99341'', ''99342'', ''99343'', ''99344'', ''99345'', ''99347'', ''99348'', ''99349'', ''99350'', ''99495'', ''99496''''90791'', ''90792'', ''90832'', ''90834'', ''90837'', ''90839''
			, ''90957'', ''90958'', ''90959'', ''90960'', ''90962'', ''90965'', ''90966'', ''92002'', ''92004'', ''92012'', ''92014'', ''92507'', ''92508'', ''92526'', ''92541'', ''92542'', ''92543'', ''92544'', ''92545'', ''92547'', ''92548''
			, ''92557'', ''92567'', ''92568'', ''92570'', ''92585'', ''92588'', ''92626'', ''96116'', ''96150'', ''96151'', ''96152'', ''97001'', ''97002'', ''97003'', ''97004'', ''97110'', ''97140'', ''97532'', ''97802'', ''97803'', ''97804''
			, ''98960'', ''98961'', ''98962'', ''99201'', ''99202'', ''99203'', ''99204'', ''99205'', ''99212'', ''99213'', ''99214'', ''99215'', ''99324'', ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337''
			, ''99341'', ''99342'', ''99343'', ''99344'', ''99345'', ''99347'', ''99348'', ''99349'', ''99350'', ''99495'', ''99496'')
			AND prs.Status = ''A''
			AND CONVERT(datetime, prs.ServiceDate) BETWEEN @StartDate AND @EndDate
		WHERE DATEDIFF (yyyy, DateOfBirth, @StartDate) >= 18	
			AND p.LastName <> ''TEST''

		--Numerator
		SELECT Id
			,Name
			,1 AS IsInNumerator
		INTO #Numerator
		FROM #Denominator d 
		INNER JOIN PatientClinical pc on pc.AppointmentId = d.EncounterId
			AND ClinicalType = ''F''
			AND Symptom = ''/PQRS (MEDS, DOCUMENTATION)''
			AND Status = ''A''
		
		UNION

		SELECT Id
			,Name
			,1 AS IsInNumerator
		FROM #Denominator d 
		INNER JOIN PatientClinical pc on pc.AppointmentId = d.EncounterId
			AND ClinicalType = ''F''
			AND Symptom = ''/CQM EXCEPTIONS''
			and FindingDetail = ''*MEDSDOCUMENTATION-CQM=T23076 <; >''
			AND Status = ''A''

		--#192 Output
		SELECT DISTINCT
			 d.Id
			,d.Name
			,CONVERT(bit, d.IsInNumerator + COALESCE(n.IsInNumerator, 0)) AS IsInNumerator
		FROM #Denominator d
		LEFT JOIN #Numerator n ON n.id = d.id

		drop table #Denominator
		drop table #Numerator
		
	END
'
END
GO