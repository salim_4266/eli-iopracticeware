/****** Object:  StoredProcedure [model].[USP_CMS_Stage2_CPOERadiology]    Script Date: 12/17/2013 9:56:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [model].[USP_CMS_Stage2_CPOERadiology] '12/15/2013', '03/15/2014', 192

DECLARE @sql NVARCHAR(max)

SET @sql = N'
CREATE PROCEDURE [model].[USP_CMS_Stage2_CPOERadiology]  
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   

BEGIN    
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

--Denominator
select e.PatientId, e.id as EncounterId, edto.Id as DiagnosticTestOrderId, a.datetime as OrderDate
into #Denominator
from model.EncounterDiagnosticTestOrders edto
inner join model.encounters e on e.id = edto.EncounterId
inner join model.Appointments a on a.EncounterId = e.Id
inner join model.DiagnosticTestOrders dto on dto.id = edto.DiagnosticTestOrderId
inner join dbo.PracticeCodeTable pct on pct.Id = dto.Id
inner join dbo.PracticeCodeTable pct2 on pct.TestOrder = pct2.AlternateCode and pct2.ReferenceType = ''SCANTYPE''
where a.DateTime between @StartDate and DATEADD(mi,1440,@EndDate) --DATEADD so that EndDate includes the whole day of encounters
and e.EncounterStatusId = 7
and a.UserId = @ResourceIds
  
--numerator  
select d.*
into #Numerator
from #Denominator d
inner join model.EncounterDiagnosticTestOrders edto on edto.id = d.DiagnosticTestOrderId

SET @TotParm = (SELECT COUNT (DISTINCT DiagnosticTestOrderId) from #Denominator)
SET @CountParm = (SELECT COUNT (DISTINCT DiagnosticTestOrderId) from #Numerator)

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.OrderDate, d.DiagnosticTestOrderId
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.EncounterId, d.OrderDate, d.DiagnosticTestOrderId

select n.PatientId, p.FirstName, p.LastName, n.EncounterId, n.OrderDate, n.DiagnosticTestOrderId
from #Numerator n
inner join model.patients p on p.id = n.PatientId
order by p.LastName, p.FirstName, n.PatientId, n.EncounterId, n.OrderDate, n.DiagnosticTestOrderId

DROP TABLE #Denominator
DROP TABLE #Numerator

END 
'

IF OBJECT_ID('model.USP_CMS_Stage2_CPOERadiology') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)
