DECLARE @sql NVARCHAR(MAX)

SET @sql = 
	'
CREATE PROCEDURE [model].[USP_CMS_Stage1_ClinicalLabTestResults] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Denominator
	SELECT DISTINCT hl7.ReportID AS LaboratoryTestOrderId
		,hl7.PatientID AS PatientId
		,hl7.ReportDate AS OrderDate
	INTO #Denominator
	FROM HL7_LabReports hl7
	INNER JOIN model.patients p ON p.id = hl7.PatientId
	WHERE hl7.ReportDate BETWEEN @StartDate
			AND @EndDate
		AND hl7.ProviderID = @ResourceIds
		AND p.LastName <>''TEST''
	--numerator  
	SELECT d.PatientId
		,d.LaboratoryTestOrderId
		,d.OrderDate
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN HL7_Observation orderdetails ON orderdetails.ReportID = d.LaboratoryTestOrderId
	INNER JOIN HL7_Observation_Details results ON results.OBRID = orderdetails.OBRID
	INNER JOIN model.patients p ON p.id = d.PatientId
	WHERE  p.LastName <>''TEST''

	SET @TotParm = (
			SELECT COUNT(DISTINCT LaboratoryTestOrderId)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT LaboratoryTestOrderId)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.OrderDate
		,d.LaboratoryTestOrderId
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	WHERE p.LastName<>''TEST''
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.OrderDate
		,d.LaboratoryTestOrderId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.OrderDate
		,n.LaboratoryTestOrderId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	WHERE p.LastName<>''TEST''
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.OrderDate
		,n.LaboratoryTestOrderId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
'

IF OBJECT_ID('model.USP_CMS_Stage1_ClinicalLabTestResults') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)