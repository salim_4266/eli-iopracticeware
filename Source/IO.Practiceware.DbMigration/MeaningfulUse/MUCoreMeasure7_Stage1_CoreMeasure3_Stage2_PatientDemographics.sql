/****** Object:  StoredProcedure [model].[USP_CMS_Stage1_Demographics]    Script Date: 12/17/2013 2:37:02 PM ******/
/****** Stage 1: C7, Stage 2: C3 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--exec [model].[USP_CMS_Stage1_Demographics] '12/15/2013', '03/15/2013', 192
DECLARE @sql NVARCHAR(MAX)
SET @sql = '

CREATE PROCEDURE [model].[USP_CMS_Stage1_Demographics] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Denominator
	SELECT PatientId
		,e.Id AS EncounterId
		,a.DATETIME AS AppointmentDate
	INTO #Denominator
	FROM model.encounters e
	INNER JOIN model.appointments a ON a.EncounterId = e.id
	INNER JOIN model.Patients p ON p.Id = e.patientId
	WHERE a.DATETIME BETWEEN @StartDate
			AND DATEADD(mi, 1440, @EndDate) --DATEADD so that EndDate includes the whole day of encounters
		AND e.EncounterStatusId = 7
		AND a.UserId = @ResourceIds
		AND p.LastName<>''TEST''

	--Numerator    
	SELECT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.Patients p ON p.Id = d.PatientId
	INNER JOIN model.PatientRace ppr ON ppr.Patients_Id = p.Id
	WHERE p.DateOfBirth IS NOT NULL
		AND p.genderid IS NOT NULL
		AND p.EthnicityId IS NOT NULL
		AND p.LanguageId IS NOT NULL
		AND p.LastName<>''TEST''

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.AppointmentDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	WHERE p.LastName<>''TEST''
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.AppointmentDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.AppointmentDate
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	WHERE p.LastName<>''TEST''
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.AppointmentDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
'
IF OBJECT_ID('model.USP_CMS_Stage1_Demographics') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)