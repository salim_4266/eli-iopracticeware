---------CQM #89 DR - Communication---------------
DECLARE @sql nvarchar(max)
SET @sql = N'

CREATE PROCEDURE [dbo].[USP_NQM_89_DR_Communication_Physician]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS

BEGIN

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;
	SET @TotParm=0;
	SET @CountParm=0;

	CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
	IF @ResourceIds=''''
	BEGIN
		INSERT INTO #TMPResources 
		SELECT Resources.ResourceId 
		FROM Resources 
		WHERE ResourceType IN (''D'',''Q'')
	END
	ELSE
		BEGIN
			DECLARE @TmpResourceIds VARCHAR(100)
			DECLARE @ResId VARCHAR(10)
			
			SELECT @TmpResourceIds =@ResourceIds
			WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
				BEGIN
					SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
					SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    
					INSERT INTO #TMPResources values(@ResId)
				END
		
		INSERT INTO #TMPResources values(@TmpResourceIds)
		
		END

		--CQM #89 Patients 18 and over with DR and office visits (ever)
		SELECT pd.PatientId, pr.AppointmentId
		INTO #InitialPopulation
		FROM PatientDemographics pd
		-- DR ever
		INNER JOIN PatientClinical pc ON pc.PatientId = pd.PatientId
			AND pc.ClinicalType = ''Q'' 
			AND pc.Status=''A''
			AND SUBSTRING(pc.FindingDetail,1,6) IN (''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
		INNER JOIN PatientReceivables pr ON pr.PatientId = pd.PatientId
		--office visits ever
		INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice
			AND prs.Service in(''92002'', ''92004'', ''92012'', ''92014''
				, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''
				, ''99212'', ''99213'', ''99214'', ''99215''
				, ''99241'', ''99242'', ''99243'', ''99244'', ''99245''
				, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310''
				, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''
				, ''99334'', ''99335'', ''99336'', ''99337'')  
			AND prs.Status = ''A''
		--Exam in last 12 months
		INNER JOIN Appointments ap ON ap.PatientId = pd.Patientid 
			AND DATEDIFF (mm, CONVERT(datetime, @EndDate), CONVERT(datetime, ap.appdate)) BETWEEN -12 AND 0
		--Dilated exam
		INNER JOIN PatientClinical pcDilation on pcDilation.AppointmentId = ap.AppointmentId 
				AND (
						(pcDilation.ClinicalType = ''F'' 
							AND pcDilation.Symptom IN 
								(''/DILATION''
								, ''/DILATED IOP PROVOCATIVE''
								, ''/DILATED REFRACTION''
								, ''/FA''
								, ''/FA AND FP''
								, ''/FA AND FP TECH''
								, ''/FLUORESCEIN ANGIO''
								, ''/FLUORESCEIN ANGIO TECH''
								, ''/FLUORESCEIN ANGIOGRAPHY''
								, ''/ICG ANGIOGRAPHY''
								, ''/ICG TECH''
								, ''/INFRARED''
								, ''/INFRARED TECH''
								, ''/OPHTHALMOSCOPY INITIAL''
								, ''/OPHTHALMOSCOPY SUBSEQ''
								, ''/OPHTHALMOSCOPY, DETECTION''
								, ''/OPHTHALMOSCOPY, MANAGE''
								, ''/PQRI (ARMD DILATED MACULAR EXAM)''
								, ''/PQRI (DM RETINOPATHY DOCUMENT)''
								, ''/PQRI (DM RETINOPATHY INFO)''
								, ''/PQRI (DM1 DILATED EXAM IN DM)''
								)
						)
						OR 
						(pcDilation.ClinicalType = ''Q'' 
							AND pcDilation.Symptom LIKE ''%&%'')
					)
				AND pcDilation.Status = ''A''
		INNER JOIN Resources re on re.ResourceId = pr.BillToDr 
			AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
		WHERE DATEDIFF (yyyy, CONVERT(datetime, pd.BirthDate), CONVERT(datetime, @EndDate)) >= 18
		GROUP BY pd.patientid, pr.AppointmentId


		--CQM #89 Denominator - more than one office visit ever
		SELECT ip.PatientId
		INTO #Denominator
		FROM #InitialPopulation ip
		GROUP BY ip.PatientId
		HAVING COUNT(*) > 1


		--CQM #89 Level severity/macular edema
		SELECT d.PatientId
		INTO #Numerator
		FROM #Denominator d
		--Exam in last 12 months
		INNER JOIN Appointments ap ON ap.PatientId = d.Patientid 
			AND DATEDIFF (mm, CONVERT(datetime, @EndDate), CONVERT(datetime, ap.appdate)) BETWEEN -12 AND 0
		--Dilated exam
		INNER JOIN PatientClinical pcDilation on pcDilation.AppointmentId = ap.AppointmentId 
			AND (
					(pcDilation.ClinicalType = ''F'' AND pcDilation.Symptom IN (''/DILATION'', ''/PQRI (DM RETINOPATHY DOCUMENT)''))
					OR 
					(pcDilation.ClinicalType = ''Q'' AND pcDilation.Symptom LIKE ''%&%'')
				)
			AND pcDilation.Status = ''A''
		INNER JOIN PatientClinical pcSeverity ON ap.AppointmentId = pcSeverity.AppointmentId
			AND pcSeverity.ClinicalType = ''Q''
			AND pcSeverity.Status = ''A''
			AND pcSeverity.FindingDetail IN (''362.02'', ''362.04'', ''362.05'', ''362.06'')
		INNER JOIN PatientClinical pcMacEdema ON ap.AppointmentId = pcMacEdema.AppointmentId
			AND pcMacEdema.Status = ''A''
			AND pcMacEdema.ClinicalType = ''Q''
			AND pcMacEdema.FindingDetail IN (
				''362.07'',
				''362.07.01'',
				''362.07.02'',
				''362.07.03'',
				''362.07.04'',
				''362.07.05'',
				''362.16.11'',
				''362.53'',
				''362.53.1'',
				''362.83'',
				''362.83.2'',
				''362.83.23'',
				''362.83.4'',
				''362.83.5'',
				''362.83.77'',
				''366.16.44'',
				''377.0'',
				''377.00'',
				''377.00.11'',
				''377.00.12'',
				''377.00.13'',
				''377.00.14'',
				''377.01'',
				''377.02'',
				''377.03'',
				''377.24'',
				''377.49.01'',
				''P158'',
				''P405'',
				''P427 '',
				''P978'',
				''P979'',
				''Q009'',
				''Q027'',
				''Q029'',
				''Q100'',
				''Q151'',
				''Q173'',
				''Q909'',
				''S612'',
				''S743''
				)
		INNER JOIN PatientClinical pcCommunicate ON pcCommunicate.AppointmentId = ap.AppointmentId
			AND pcCommunicate.Status = ''A''
			AND pcCommunicate.ClinicalType = ''A'' 
			AND SUBSTRING(pcCommunicate.FindingDetail, 1, 26) = ''SEND A CONSULTATION LETTER''
		GROUP BY d.PatientId

	SELECT @TotParm = COUNT(*)
	FROM #Denominator

	SELECT @CountParm = COUNT(*) 
	FROM #Numerator

		--DROP TABLE #TMPResources
		DROP TABLE #InitialPopulation
		DROP TABLE #Denominator
		DROP TABLE #Numerator

		SELECT @TotParm as Denominator,@CountParm as Numerator

END
'

IF OBJECT_ID('USP_NQM_89_DR_Communication_Physician') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)