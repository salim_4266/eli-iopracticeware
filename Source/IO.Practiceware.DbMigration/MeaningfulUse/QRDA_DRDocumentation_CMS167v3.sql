/****** Object:  StoredProcedure [qrda].[QRDA_DRDocumentation_CMS167v3]    Script Date: 9/4/2014 12:57:47 PM ******/
/***FOR DROPPING (IF EXIST) *******/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_DRDocumentation_CMS167v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [qrda].[QRDA_DRDocumentation_CMS167v3]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_DRDocumentation_CMS167v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [qrda].[QRDA_DRDocumentation_CMS167v3]
(
@StartDate datetime,
@EndDate datetime,
@UserId int
)
AS

BEGIN

		--Patients 18 and over with DR before end of Measurement Period and OV during MP
		SELECT pd.Id
			, pd.LastName as Name
			, apOv.AppointmentId
			, 0 AS IsInNumerator
		INTO #Denominator
		FROM model.Patients pd
		--Exam in the reporting period
		INNER JOIN Appointments apOV ON apOV.PatientId = pd.Id
			AND apOV.ResourceId1 = @UserId
			AND apOV.AppTime > 0
		INNER JOIN PatientReceivableServices prs ON prs.AppointmentId = apOV.AppointmentId
			AND prs.Service IN
					(''92002'', ''92004'', ''92012'', ''92014''
					, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''
					, ''99212'', ''99213'', ''99214'', ''99215''
					, ''99241'', ''99242'', ''99243'', ''99244'', ''99245''
					, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310''
					, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''
					, ''99334'', ''99335'', ''99336'', ''99337'') 
			AND prs.Status = ''A''
			AND CONVERT(datetime, prs.ServiceDate) BETWEEN @StartDate AND @EndDate
		-- DR starting before or during OV
		INNER JOIN Appointments apDx ON apDx.PatientId = pd.Id 
			AND apDx.AppDate <= ServiceDate
		INNER JOIN PatientClinical pc ON pc.AppointmentId = apDx.AppointmentId
			AND pc.ClinicalType = ''Q'' 
			AND pc.Status=''A''
			AND SUBSTRING(pc.FindingDetail,1,6) IN (''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
		WHERE DATEDIFF (yyyy, pd.DateOfBirth, @StartDate) >= 18	
			AND pd.LastName <> ''TEST''
		
		--Numerator - Macular Exam during the OV
		SELECT d.Id as Id
			, d.Name
			, d.AppointmentId
			, 0 AS IsInNumerator
		INTO #DilatedExam
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.AppointmentId = d.AppointmentId
		--Dilated exam
		INNER JOIN PatientClinical pcDilation on pcDilation.AppointmentId = ap.AppointmentId 
				AND (
						(pcDilation.ClinicalType = ''F'' 
							AND pcDilation.Symptom IN 
								(''/DILATION''
								, ''/DILATED IOP PROVOCATIVE''
								, ''/DILATED REFRACTION''
								, ''/FA''
								, ''/FA AND FP''
								, ''/FA AND FP TECH''
								, ''/FLUORESCEIN ANGIO''
								, ''/FLUORESCEIN ANGIO TECH''
								, ''/FLUORESCEIN ANGIOGRAPHY''
								, ''/ICG ANGIOGRAPHY''
								, ''/ICG TECH''
								, ''/INFRARED''
								, ''/INFRARED TECH''
								, ''/OPHTHALMOSCOPY INITIAL''
								, ''/OPHTHALMOSCOPY SUBSEQ''
								, ''/OPHTHALMOSCOPY, DETECTION''
								, ''/OPHTHALMOSCOPY, MANAGE''
								, ''/PQRI (ARMD DILATED MACULAR EXAM)''
								, ''/PQRI (DM RETINOPATHY DOCUMENT)''
								, ''/PQRI (DM RETINOPATHY INFO)''
								, ''/PQRI (DM1 DILATED EXAM IN DM)''
								, ''/PQRI (DM2 VF IN DM PATIENT)''
								, ''/PQRI (DM3 PHOTOS IN DM PATIENT)''
								, ''/PQRI (DM4 LOW RISK DM PATIENT)''
								)
						)
						OR 
						(pcDilation.ClinicalType = ''Q'' 
							AND pcDilation.Symptom LIKE ''%&%'')
					)
				AND pcDilation.Status = ''A''

		--Level severity/macular edema
		SELECT d.Id
			, Name
			, 1 as IsInNumerator
		INTO #Numerator 
		FROM #DilatedExam d
		INNER JOIN PatientClinical pcSeverity ON d.AppointmentId = pcSeverity.AppointmentId
			AND pcSeverity.ClinicalType = ''Q''
			AND pcSeverity.Status = ''A''
			AND pcSeverity.FindingDetail IN (''362.02'', ''362.04'', ''362.05'', ''362.06'')
		INNER JOIN PatientClinical pcMacEdema ON d.AppointmentId = pcMacEdema.AppointmentId
			AND pcMacEdema.Status = ''A''
			AND pcMacEdema.ClinicalType = ''Q''
			AND pcMacEdema.FindingDetail IN (
				''362.07'',
				''362.07.01'',
				''362.07.02'',
				''362.07.03'',
				''362.07.04'',
				''362.07.05'',
				''362.16.11'',
				''362.53'',
				''362.53.1'',
				''362.83'',
				''362.83.2'',
				''362.83.23'',
				''362.83.4'',
				''362.83.5'',
				''362.83.77'',
				''366.16.44'',
				''377.0'',
				''377.00'',
				''377.00.11'',
				''377.00.12'',
				''377.00.13'',
				''377.00.14'',
				''377.01'',
				''377.02'',
				''377.03'',
				''377.24'',
				''377.49.01'',
				''P158'',
				''P405'',
				''P427 '',
				''P978'',
				''P979'',
				''Q009'',
				''Q027'',
				''Q029'',
				''Q100'',
				''Q151'',
				''Q173'',
				''Q909'',
				''S612'',
				''S743''
				)

		--Output
			SELECT DISTINCT
			 d.Id
			,d.Name
			,CONVERT(bit, d.IsInNumerator + COALESCE(n.IsInNumerator, 0)) AS IsInNumerator
		FROM #Denominator d
		LEFT JOIN #Numerator n ON n.Id = d.Id

		DROP TABLE #DilatedExam
		DROP TABLE #Denominator
		DROP TABLE #Numerator

END
'
END
GO