/****** Object:  StoredProcedure [model].[USP_CMS_Stage2_VitalSignsNoBP]    Script Date: 12/17/2013 8:14:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [model].[USP_CMS_Stage2_VitalSignsNoBP] '12/15/2013', '03/15/2014', 192
DECLARE @sql NVARCHAR(MAX)
SET @sql = '
CREATE PROCEDURE [model].[USP_CMS_Stage2_VitalSignsNoBP] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	--Numerator    
	SELECT DISTINCT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.PatientHeightAndWeights hw ON hw.PatientId = d.PatientId
	WHERE (
			HeightUnit IS NOT NULL
			AND WeightUnit IS NOT NULL
			)

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
'

IF OBJECT_ID('model.USP_CMS_Stage2_VitalSignsNoBP') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)