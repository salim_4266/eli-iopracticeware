/***FOR DROPPING (IF EXIST) *******/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[USP_CMS_GetElectronicCopyStatus]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
       DROP PROCEDURE USP_CMS_GetElectronicCopyStatus
END
GO
/***C4*** Object:  StoredProcedure [dbo].[USP_CMS_GetElectronicCopyStatus]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[USP_CMS_GetElectronicCopyStatus]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[USP_CMS_GetElectronicCopyStatus]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN

DECLARE @TotParm INT;
DECLARE @CountParm INT;
DECLARE @TempEndDate NVARCHAR(200);
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN (''D'',''Q'', ''Z'', ''X'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--denominator
SELECT @TotParm=COUNT(DISTINCT PTJ.TransactionId)
from PracticeTransactionJournal PTJ
INNER JOIN Appointments ap on ap.AppointmentId=PTJ.TransactionTypeId
inner join Resources re on re.ResourceId =ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where PTJ.TransactionType = ''P''
and PTJ.TransactionDate between @StartDate and @EndDate

--numerator
SELECT @TempEndDate=CONVERT(NVARCHAR,DATEADD(D,(CASE WHEN DATEPART(DW, CAST(SUBSTRING(@EndDate,0,5)+ ''/'' + SUBSTRING(@EndDate,5,2) + ''/'' + SUBSTRING(@EndDate,7,2) 
AS DATETIME))=4 THEN -1
WHEN DATEPART(DW, CAST(SUBSTRING(@EndDate,0,5)+ ''/'' + SUBSTRING(@EndDate,5,2) + ''/'' + SUBSTRING(@EndDate,7,2) 
AS DATETIME))=5 THEN -2
WHEN DATEPART(DW, CAST(SUBSTRING(@EndDate,0,5)+ ''/'' + SUBSTRING(@EndDate,5,2) + ''/'' + SUBSTRING(@EndDate,7,2) 
AS DATETIME))=6 THEN -3
WHEN DATEPART(DW, CAST(SUBSTRING(@EndDate,0,5)+ ''/'' + SUBSTRING(@EndDate,5,2) + ''/'' + SUBSTRING(@EndDate,7,2) 
AS DATETIME))=7 THEN -4
 ELSE 0 END),
CAST(SUBSTRING(@EndDate,0,5)+ ''/'' + SUBSTRING(@EndDate,5,2) + ''/'' + SUBSTRING(@EndDate,7,2) 
AS DATETIME)),112)


SELECT @CountParm=COUNT(DISTINCT PTJ.TransactionId)
from PracticeTransactionJournal PTJ
INNER JOIN Appointments ap on ap.AppointmentId=PTJ.TransactionTypeId
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where PTJ.TransactionType = ''P'' and PTJ.TransactionStatus = ''S''
and PTJ.TransactionDate between @StartDate and @TempEndDate
and PTJ.TransactionRemark <= 
CONVERT(NVARCHAR,DATEADD(D,3+(CASE WHEN DATEPART(DW, CAST(SUBSTRING(PTJ.TransactionDate,0,5)+ ''/'' + SUBSTRING(PTJ.TransactionDate,5,2) + ''/'' + SUBSTRING(PTJ.TransactionDate,7,2) 
AS DATETIME))>3 THEN 2 ELSE 0 END),
CAST(SUBSTRING(PTJ.TransactionDate,0,5)+ ''/'' + SUBSTRING(PTJ.TransactionDate,5,2) + ''/'' + SUBSTRING(PTJ.TransactionDate,7,2) 
AS DATETIME)),112)


DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator

END
'
END
GO