/****** Object:  StoredProcedure [model].[USP_CMS_Stage1_ProblemList]    Script Date: 12/17/2013 6:10:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [model].[USP_CMS_Stage1_ProblemList] '12/15/2013', '03/15/2014', 192
DECLARE @sql NVARCHAR(MAX)
SET @sql = '
CREATE PROCEDURE [model].[USP_CMS_Stage1_ProblemList] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation ipp

	--Numerator    
	SELECT DISTINCT pc.PatientId
	INTO #Numerator
	FROM #Denominator d
	inner join dbo.PatientClinical pc on pc.PatientId = d.PatientId
	where ClinicalType = ''U'' and Status = ''A''
	--INNER JOIN model.PatientDiagnoses pd ON pd.PatientId = d.PatientId
	--INNER JOIN model.PatientDiagnosisDetails pdd ON pd.id = pdd.PatientDiagnosisId
	--	AND IsOnProblemList = 1

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		
	SELECT d.PatientId
	FROM #Denominator d
	LEFT JOIN #Numerator n on n.PatientId = d.PatientId
	WHERE n.PatientId is NULL

	DROP TABLE #InitialPatientPopulation

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
'

IF OBJECT_ID('model.USP_CMS_Stage1_ProblemList') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)