/****** Object:  StoredProcedure [qrda].[QRDA_CataractsComplications_CMS132v3]    Script Date: 9/4/2014 11:31:07 AM ******/
/***FOR DROPPING (IF EXIST) *******/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_CataractsComplications_CMS132v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [qrda].[QRDA_CataractsComplications_CMS132v3]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_CataractsComplications_CMS132v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [qrda].[QRDA_CataractsComplications_CMS132v3]
(
@StartDate datetime,
@EndDate datetime,
@UserId int
)
As
BEGIN

		--#192 Cataract Complications Denominator
		SELECT p.Id 
			,apOV.AppointmentId AS OVAppointmentId
			,p.FirstName +'' '' + p.LastName AS Name
			,apOv.AppDate 
			,prs.Service 
			,CASE
				WHEN prs.Modifier LIKE ''%LT%'' THEN ''LT'' 
				WHEN prs.Modifier LIKE ''%RT%'' THEN ''RT'' 
				END AS Modifier
		INTO #AllCataracts
		FROM model.Patients p
		INNER JOIN Appointments apOV ON p.Id = apOv.PatientId
			AND @UserId = apOv.ResourceId1
			AND apOV.AppTime > 0
		INNER JOIN PatientReceivableServices prs ON prs.AppointmentId = apOv.AppointmentId
			AND prs.Service IN (''66840'', ''66850'', ''66852'', ''66920'', ''66930'', ''66940'', ''66982'', ''66983'', ''66984'')  
			AND (prs.Modifier LIKE ''%LT%'' OR prs.Modifier LIKE ''%RT%'')
			AND prs.Status = ''A''
			AND prs.Modifier NOT LIKE ''%56%''
			AND CONVERT(datetime, prs.ServiceDate) BETWEEN @StartDate AND @EndDate
		WHERE DATEDIFF (yyyy, DateOfBirth, CONVERT(datetime, apOv.AppDate)) >= 18		
			AND p.LastName <> ''TEST''
		 GROUP BY  apOv.AppointmentId
			,p.Id 
			,p.FirstName +'' '' + p.LastName 
			,apOv.AppDate
			,prs.Service 
			,prs.Modifier 

		--Comorbid patients
		SELECT DISTINCT pcComorbid.PatientId AS SickPatientId
		INTO #ComorbidPatients
		FROM PatientClinical pcComorbid
		INNER JOIN Appointments apComorbid ON pcComorbid.AppointmentId = apComorbid.AppointmentId
		INNER JOIN PatientReceivables pr ON pr.AppointmentId = apComorbid.AppointmentId
		INNER JOIN PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		INNER JOIN #AllCataracts AllCats ON AllCats.Id = apComorbid.PatientId
			AND apComorbid.AppDate < AllCats.AppDate
			AND pcComorbid.ClinicalType IN (''Q'', ''B'', ''K'')
			AND pcComorbid.Status = ''A''
			AND (SUBSTRING(pcComorbid.FindingDetail,1,6) IN (
				''364.00'', ''364.01'', ''364.02'', ''364.03'', ''364.04'', ''364.05'',
				''364.70'', ''364.71'', ''364.72'', ''364.73'', ''364.74'', ''364.75'',''364.76'',''364.77'',''364.81'',''364.82'',''364.89'',
				''379.42'',
				''379.32'', ''379.33'',	''379.34'',
				''366.32'', ''366.33'', 
				''743.30'',
				''743.31'',
				''364.21'', ''364.22'', ''364.23'', ''364.24'', 
				''364.10'', ''364.11'', 
				''371.01'', ''371.02'', ''371.03'', ''371.04'',
				''371.00'', --''371.03'',''371.04'',
				''371.20'', ''371.21'', ''371.22'', ''371.23'', ''371.43'', ''371.44'',
				''364.60'', ''364.61'', ''364.62'', ''364.63'', ''364.64'',
				''376.50'', ''376.51'', ''376.52'',
				''365.10'', ''365.11'', ''365.12'', ''365.13'', ''365.14'', ''365.15'',
				''365.20'', ''365.21'', ''365.22'', ''365.23'', ''365.24'', ''365.31'', 
				''365.32'', ''365.51'', ''365.52'', ''365.59'', ''365.60'', ''365.61'',
				''365.62'', ''365.63'', ''365.64'', ''365.65'', ''365.81'', ''365.82'', 
				''365.83'', ''365.89'', 
				''371.50'', ''371.51'', ''371.52'', ''371.53'', ''371.54'', ''371.55'', ''371.56'', ''371.57'', ''371.58'',
				''360.21'', 
				''360.30'', ''360.31'', ''360.32'', ''360.33'', ''360.34'',
				''370.03'',
				''360.20'', ''360.21'', 
				''743.36'',
				''365.52'',
				''362.21'', 
				''366.11'',
				''366.21'', ''366.22'', ''366.23'', ''366.20'', 
				''360.11'', ''360.12'', 
				''364.42'')
			OR SUBSTRING(pcComorbid.FindingDetail, 1, 5) IN (
				''940.0'', ''940.1'', ''940.2'', ''940.3'', ''940.4'', ''940.5'', ''940.9'',
				''366.9'',
				''364.3'', 
				''367.0'',
				''950.0'', ''950.1'', ''950.2'', ''950.3'', ''950.9'',
				''871.0'', ''871.1'', ''871.2'', ''871.3'', ''871.4'', ''871.5'', ''871.6'',''871.7'',''871.9'',''921.3'')
			)

		--more comorbid - PPV history
		SELECT DISTINCT apPPV.PatientId  AS PPVPatientId
		INTO #ComorbidPPVPatients
		FROM #AllCataracts AllCats
		INNER JOIN Appointments apPPV ON apPPV.AppDate < AllCats.AppDate 
			AND AllCats.Id = apPPV.PatientId
		INNER JOIN PatientReceivables prPPV ON prPPV.AppointmentId = apPPV.AppointmentId
		INNER JOIN PatientReceivableServices prsPPV ON prsPPV.Invoice = prPPV.Invoice
			AND prsPPV.Status = ''A''
			AND prsPPV.Service IN (''67036'', ''67039'', ''67040'', ''67041'', ''67042'', ''67043'')

		--#192 Flomax Patients
		SELECT ap.PatientId AS FlomaxPatientId
		INTO #FlomaxPatients
		FROM #AllCataracts AllCats
		INNER JOIN Appointments ap ON ap.PatientId = AllCats.Id
		INNER JOIN PatientClinical pc ON ap.AppointmentId = pc.AppointmentId
			AND pc.ClinicalType = ''A''
			AND SUBSTRING(pc.FindingDetail, 1, 3) = ''RX-''
			AND (pc.FindingDetail LIKE ''%Flomax%''
				or pc.FindingDetail LIKE ''%Tamsulosin%''
				or pc.FindingDetail LIKE ''%Flomaxtra %''
				or pc.FindingDetail LIKE ''%Contiflo XL%''
				or pc.FindingDetail LIKE ''%Urimax%''
				or pc.FindingDetail LIKE ''%Pradif%''
				or pc.FindingDetail LIKE ''%Secotex%''
				or pc.FindingDetail LIKE ''%Harnal D%''
				or pc.FindingDetail LIKE ''%Omnic %'')
			AND pc.Status = ''A''

		--#192 Healthy Cataract Patients
		SELECT Id
			, Name
			, AppDate
			, 0 AS InInNumerator
			, CASE 
				WHEN FlomaxPatientId IS NULL THEN 2
				ELSE 1
				END AS tq_tamsulosin
			, Modifier
		INTO #CataractComplicationsDenominator
		FROM #AllCataracts AllCats
		LEFT JOIN #ComorbidPatients c ON c.SickPatientId = AllCats.Id
		LEFT JOIN #ComorbidPPVPatients p ON p.PPVPatientId = AllCats.Id
		LEFT JOIN #FlomaxPatients f ON f.FlomaxPatientId = AllCats.Id
		WHERE c.SickPatientId IS NULL
			AND p.PPVPatientId IS NULL
			AND f.FlomaxPatientId IS NULL
		GROUP BY Id, OVAppointmentId, Name, AppDate,  Modifier, FlomaxPatientId


		--#192 Cataract Complications Numerator
		SELECT Id
			, 1 AS IsInNumerator
		INTO #CataractComplicationsNumerator
		FROM #CataractComplicationsDenominator d
		INNER JOIN Appointments ap30Days ON ap30Days.PatientId = d.Id
			AND DATEDIFF(dd, d.AppDate, ap30Days.AppDate) BETWEEN 0 AND 30
			AND ap30Days.ScheduleStatus = ''D''
		INNER JOIN PatientReceivables pr ON pr.AppointmentId = ap30Days.AppointmentId
		INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice
			AND (prs.Modifier LIKE ''%RT%'' OR prs.Modifier LIKE ''%LT%'')
			AND prs.Service IN
				(''65235'', ''65800'', ''65810'', ''65815'', ''65860'', ''65880'', ''65900'',	''65920'', ''65930'', ''66030'', 
				''66250'', ''66820'', ''66825'', ''66830'', ''66852'', ''66986'', ''67005'', ''67010'', ''67015'', ''67025'', ''67028'', ''67030'', 
				''67031'', ''67036'', ''67039'', ''67041'', ''67042'', ''67043'', ''67101'', ''67105'', ''67107'', ''67108'', ''67110'', ''67112'', 
				''67141'', ''67145'', ''67250'', ''67255'')
			AND prs.Status = ''A''
		WHERE 	d.Modifier = CASE
					WHEN prs.Modifier LIKE ''%RT%'' THEN ''RT''
					WHEN prs.Modifier LIKE ''%LT%'' THEN ''LT''
					END 

		--#192 Output
		SELECT DISTINCT
			 d.Id as Id
			,d.Name
			,CONVERT(bit, d.InInNumerator + COALESCE(n.IsInNumerator, 0)) AS IsInNumerator
		FROM #CataractComplicationsDenominator d
		LEFT JOIN #CataractComplicationsNumerator n ON n.Id = d.Id


		drop table #CataractComplicationsDenominator 
		drop table #CataractComplicationsNumerator
		drop table #AllCataracts
		drop table #ComorbidPatients
		drop table #ComorbidPPVPatients
		drop table #FlomaxPatients	

END
'
END
GO