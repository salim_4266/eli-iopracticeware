
---------#141 POAG Plan PQRS ---------------

DECLARE @sql nvarchar(max)
SET @sql = N'


CREATE PROCEDURE [dbo].[USP_PQRI_GetPOAG_ReducePressure]
(
@StartDate nvarchar(100),
@EndDate nvarchar(100),
@ResourceIds nvarchar(1000)
)
AS

BEGIN



	CREATE TABLE #TMPResources(ResourceId nvarchar(1000))

	IF @ResourceIds=''''

		BEGIN

			INSERT INTO #TMPResources 

			SELECT Resources.ResourceId 

			FROM Resources 

			WHERE ResourceType IN(''D'',''Q'')

		END

	ELSE

		BEGIN

			DECLARE @TmpResourceIds varchar(100)

			DECLARE @ResId varchar(10)

			SELECT @TmpResourceIds =@ResourceIds

			WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0

				BEGIN

					SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)

					SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    

					INSERT INTO #TMPResources values(@ResId)

				END

				INSERT INTO #TMPResources values(@TmpResourceIds)

		END





		--#141 POAG Reduce Pressure Denominator  

		SELECT p.PatientId AS patient_os_id

			,ap.AppointmentId AS OVAppointmentId

			,MIN(pcDx.Symptom) AS DxRank

			,p.FirstName AS tq_firstname

			,p.LastName AS tq_lastname

			,ap.AppDate AS tq_encounterdt

			,re.NPI AS tq_physiciannpi

			,p.BirthDate AS tq_dob

			,CASE p.Gender

				WHEN ''M'' THEN ''M''

				WHEN ''F'' THEN ''F''

				ELSE ''U''

				END AS tq_gender

			,''1'' AS tq_medicare2

			,ISNULL(MAX(SUBSTRING(icd10.FindingDetailIcd10, 1, 6)),MAX(SUBSTRING(pcDx.FindingDetail, 1, 6))) AS tq_icd   

			,prs.Service AS tq_em

			,3 AS tq_iopmeasure

			,2 AS tq_glaucplan2

			,pcDx.EyeContext

		INTO #POAGIOPDenominator

		FROM PatientDemographics p

		INNER JOIN Appointments ap ON p.PatientId = ap.PatientId

		INNER JOIN PatientClinical pcDx ON ap.AppointmentId = pcDx.AppointmentId

			AND pcDx.ClinicalType = ''U''

			AND pcDx.Status = ''A''

		INNER JOIN PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId

		INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId

			AND pri.InsurerReferenceCode IN (''N'', ''M'', ''['')

		INNER JOIN Resources re ON re.ResourceId = pr.BillToDr

			AND re.ResourceId IN (SELECT #TMPResources.ResourceId from #TMPResources)

		INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice

			AND prs.Status = ''A''

		LEFT JOIN (SELECT AppointmentId,FindingDetailIcd10 FROM PatientClinicalIcd10   
					WHERE ClinicalType IN (''B'', ''K'')
					AND Status = ''A''
					AND FindingDetailIcd10 IN (''H40.10X0'',''H40.10X1'',''H40.10X2'',''H40.10X3'',''H40.10X4'',''H40.11X0'',''H40.11X1'',''H40.11X2'',''H40.11X3'',''H40.11X4'',''H40.1210'',
											''H40.1211'',''H40.1212'',''H40.1213'',''H40.1214'',''H40.1220'',''H40.1221'',''H40.1222'',''H40.1223'',''H40.1224'',''H40.1230'',''H40.1231'',''H40.1232'',''H40.1233'',''H40.1234'',
											''H40.1290'',''H40.1291'',''H40.1292'',''H40.1293'',''H40.1294'',''H40.151'',''H40.152'',''H40.153'',''H40.159''
											)
					) AS icd10 ON icd10.AppointmentId=ap.AppointmentId


		WHERE 

			--appointment during 2012

			ap.AppDate BETWEEN @StartDate AND @EndDate

			--over 18 years

			AND DATEDIFF (yyyy, CONVERT(datetime, BirthDate), CONVERT(datetime, ap.AppDate)) >= 18

			--diagnosis of POAG

			AND SUBSTRING(pcDx.FindingDetail, 1, 6) IN (''365.10'', ''365.11'', ''365.12'', ''365.15'')

			--with office visit

			AND prs.Service IN (''92002'', ''92004'', ''92012'', ''92014''

				, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''

				, ''99212'', ''99213'', ''99214'', ''99215''

				, ''99307'', ''99308'', ''99309'', ''99310''

				, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''

				, ''99334'', ''99335'', ''99336'', ''99337'') 

			AND p.LastName <> ''TEST''

		 GROUP BY  ap.AppointmentId

			,p.PatientId 

			,p.FirstName 

			,p.LastName 

			,ap.AppDate

			,re.NPI 

			,p.BirthDate

			,p.Gender

			,prs.Service

			,pcDx.EyeContext 

 

 

		--#141 All IOPs within the last year for patients in the denominator, including reported visit

		SELECT App1Year.PatientId

			, App1Year.AppDate

			, App1Year.AppointmentId

			, SUBSTRING(pcIOPEye.FindingDetail, 2, 2) AS IOPEye

			, pcIOPEye.ClinicalId AS EyeId

			, pcIOPValue.ClinicalId AS IOPId

			, SUBSTRING(pcIOPValue.FindingDetail, CHARINDEX(''<'', pcIOPValue.FindingDetail)+1, CHARINDEX(''>'', pcIOPValue.FindingDetail)-CHARINDEX(''<'', pcIOPValue.FindingDetail)-1) AS IOP

			,-2 AS tq_iopmeasure

		INTO #POAGAllIOPs

		FROM #POAGIOPDenominator d

		--All appointments within the reporting period

		INNER JOIN Appointments App1Year ON App1Year.AppDate <= d.tq_encounterdt

			AND DATEDIFF (mm, CONVERT(datetime, App1Year.AppDate), CONVERT(datetime, d.tq_encounterdt)) BETWEEN 0 and 12

			AND App1Year.AppDate BETWEEN @StartDate AND @EndDate

			AND App1Year.ScheduleStatus = ''D''

		--Find Eye matching reported eye with glaucoma

		INNER JOIN PatientClinical pcIOPEye ON pcIOPEye.AppointmentId = App1Year.AppointmentId

			AND pcIOPEye.ClinicalType = ''F''

			AND pcIOPEye.Symptom = ''/IOP''

			AND pcIOPEye.Status = ''A''

			AND SUBSTRING(pcIOPEye.FindingDetail, 2, 2) = d.EyeContext

		--Find IOP for glaucoma eye

		INNER JOIN PatientClinical pcIOPValue ON pcIOPValue.AppointmentId = pcIOPEye.AppointmentId

			AND pcIOPValue.Symptom = ''/IOP''

			AND pcIOPValue.ClinicalId = pcIOPEye.ClinicalId + 1

			AND SUBSTRING(pcIOPValue.FindingDetail, 1, 12) = ''*IOP=T1342 <''

			AND ISNUMERIC(SUBSTRING(pcIOPValue.FindingDetail, CHARINDEX(''<'', pcIOPValue.FindingDetail)+1, CHARINDEX(''>'', pcIOPValue.FindingDetail)-CHARINDEX(''<'', pcIOPValue.FindingDetail)-1)) = 1

		GROUP BY App1Year.PatientId, App1Year.AppDate, App1Year.AppointmentId, d.EyeContext, pcIOPEye.FindingDetail, pcIOPEye.ClinicalId, pcIOPValue.ClinicalId, pcIOPValue.FindingDetail

		ORDER BY App1Year.PatientId, App1Year.AppDate, App1Year.AppointmentId, d.EyeContext, pcIOPEye.FindingDetail, pcIOPValue.FindingDetail



		--Find visit with IOPs prior to reported visit

		SELECT d.patient_os_id

			, d.tq_encounterdt AS CurrentAppDate

			, d.OVAppointmentId AS CurrentAppointmentId

			, MAX(i.AppDate) AS PriorAppDate

			, MAX(i.AppointmentId) AS PriorAppointmentId

		INTO #PriorAppDate

		FROM #POAGIOPDenominator d

		INNER JOIN #POAGAllIOPs i ON d.patient_os_id = i.PatientId

			AND i.AppDate < d.tq_encounterdt

		GROUP BY d.patient_os_id

			, d.tq_encounterdt

			, d.OvAppointmentId

		ORDER BY d.patient_os_id



		---Compare IOP of each visit in reporting period to PriorAppDate''s IOP -- GUYS WITH UNSUCCESSFUL IOP REDUCTION

		SELECT d.Patient_os_id

			, d.EyeContext

			, d.tq_encounterdt

			, d.OVAppointmentId

			, CurrentIOP.IOPId AS CurrentIOPId

			, CurrentIOP.IOPEye AS CurrentEye

			, CurrentIOP.IOP AS CurrentIOP

			, p.PriorAppDate AS PriorAppDate

			, PriorIOP.IOPId AS PriorIOPId

			, PriorIOP.IOPEye AS PriorEye

			, PriorIOP.IOP AS PriorIOP

			,1 AS tq_iopmeasure

			,0 AS tq_glaucplan2

		INTO #Unsuccessful

		FROM #POAGIOPDenominator d

		INNER JOIN #POAGAllIOPs CurrentIOP ON d.OVAppointmentId = CurrentIOP.AppointmentId

			AND d.EyeContext = CurrentIOP.IOPEye

		INNER JOIN #PriorAppDate p ON d.patient_os_id = p.patient_os_id

			AND p.CurrentAppointmentId = d.OVAppointmentId

		INNER JOIN #POAGAllIOPs PriorIOP ON PriorIOP.AppointmentId = p.PriorAppointmentId

			AND PriorIOP.IOPEye = CurrentIOP.IOPEye

		WHERE CONVERT(decimal (18,2),PriorIOP.IOP)-CONVERT(decimal (18,2), CurrentIOP.IOP) <  CONVERT(decimal (18,2),PriorIOP.IOP) *.15

		ORDER BY d.patient_os_id, d.tq_Encounterdt,  d.EyeContext





		SELECT d.patient_os_id

			,d.OVAppointmentId

			,0 AS tq_iopmeasure

			,-1 AS tq_glaucplan2

		INTO #Plan

		FROM #POAGIOPDenominator d

		INNER JOIN PatientClinical pcPlan ON pcPlan.AppointmentId = d.OVAppointmentId

			AND pcPlan.ClinicalType = ''A''

			AND pcPlan.Status = ''A''

		GROUP BY d.patient_os_id

			,d.OVAppointmentId



		--#141 POAG Treatment Plan Output

		SELECT d.patient_os_id AS PatientId

			,d.tq_firstname AS FirstName

			,d.tq_lastname AS LastName

			,CONVERT(nvarchar(10), CONVERT(datetime, d.tq_encounterdt),101) AS EncounterDate

			,d.tq_physiciannpi AS PhysicianNPI

			,CONVERT(nvarchar(10), CONVERT(datetime, d.tq_dob),101) AS PatientDateOfBirth

			,d.tq_gender AS PatientGender

			,d.tq_medicare2 AS IsMedicare

			,d.tq_icd AS ICDCode

			,d.tq_em AS EM

			,d.tq_iopmeasure + COALESCE(a.tq_iopmeasure, 0) + COALESCE(u.tq_iopmeasure, 0) + COALESCE(p.tq_iopmeasure, 0) AS tq_iopmeasure 

			,d.tq_glaucplan2 + COALESCE(u.tq_glaucplan2, 0) + COALESCE(p.tq_glaucplan2, 0) AS tq_glaucplan2

		INTO #Output

		FROM #POAGIOPDenominator d

		LEFT JOIN #POAGAllIOPs a ON a.AppointmentId = d.OVAppointmentId

		LEFT JOIN #Unsuccessful u ON u.OVAppointmentId = d.OVAppointmentId

		LEFT JOIN #Plan p ON p.OVAppointmentId = d.OVAppointmentId

		GROUP BY d.patient_os_id

			,d.tq_firstname

			,d.tq_lastname

			,d.tq_encounterdt

			,d.tq_physiciannpi

			,d.tq_dob

			,d.tq_gender

			,d.tq_medicare2

			,d.tq_icd   

			,d.tq_em

			,d.tq_iopmeasure,a.tq_iopmeasure,u.tq_iopmeasure,p.tq_iopmeasure

			,d.tq_glaucplan2,u.tq_glaucplan2,p.tq_glaucplan2

		ORDER BY d.patient_os_id, d.tq_encounterdt



		SELECT PatientId

			,FirstName

			,LastName

			,EncounterDate

			,PhysicianNPI

			,PatientDateOfBirth

			,PatientGender

			,IsMedicare

			,ICDCode

			,EM

			,CASE 

				WHEN tq_iopmeasure = 1 THEN ''3284F''

				WHEN tq_iopmeasure = 2 AND tq_glaucplan2 = 1 THEN ''0517F 3285F''

				WHEN tq_iopmeasure = 2 AND tq_glaucplan2 = 2 THEN ''0517 8P 3285F''

				WHEN tq_iopmeasure = 3 THEN ''3284F 8P''

				END AS Numerator

		FROM #Output d



		DROP TABLE #TMPResources 

		DROP TABLE #POAGIOPDenominator

		drop table #POAGAllIOPs

		DROP TABLE #PriorAppDate

		DROP TABLE #Unsuccessful

		DROP TABLE #Plan

		DROP TABLE #Output



	END
'
IF OBJECT_ID('USP_PQRI_GetPOAG_ReducePressure') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)