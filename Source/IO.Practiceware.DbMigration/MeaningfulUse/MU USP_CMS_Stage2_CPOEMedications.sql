/****** Object:  StoredProcedure [model].[USP_CMS_Stage1_CPOEMedications]    Script Date: 12/17/2013 9:51:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--exec [model].[USP_CMS_Stage1_CPOEMedications] '12/15/2013', '03/15/2014', 192
DECLARE @sql NVARCHAR(MAX)
SET @sql = '

CREATE PROCEDURE [model].[USP_CMS_Stage2_CPOEMedications]  
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   

BEGIN    
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

		--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT ipp.EncounterId ,ipp.EncounterDate AS AppointmentDate,ipp.PatientId,ipp.DOB,ipp.Age,MedicationId AS PatientMedicationID,ClinicalDataSourceTypeId
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
	INNER JOIN PatientMedications pm ON pm.PatientId = ipp.PatientId

	--numerator  
	SELECT DISTINCT d.PatientId
		,pm.Id AS PatientMedicationId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientMedications pm ON pm.PatientId = d.PatientId

SET @TotParm = (SELECT COUNT (DISTINCT PatientId) from #Denominator)
SET @CountParm = (SELECT COUNT (DISTINCT PatientId) from #Numerator)

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.AppointmentDate, PatientMedicationId
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

select n.PatientId, p.FirstName, p.LastName, n.PatientMedicationId
from #Numerator n
inner join model.patients p on p.id = n.PatientId
order by p.LastName, p.FirstName, n.PatientId, n.PatientMedicationId

DROP TABLE #Denominator
DROP TABLE #Numerator
DROP TABLE #InitialPatientPopulation

END 
'

IF OBJECT_ID('model.USP_CMS_Stage2_CPOEMedications') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)