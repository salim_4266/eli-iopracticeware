/****** Object:  StoredProcedure [model].[USP_CMS_Stage2_Imaging]    Script Date: 12/17/2013 2:37:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- exec [model].[USP_CMS_Stage2_Imaging] '12/15/2013', '03/15/2014', 192

DECLARE @sql NVARCHAR(MAX)
SET @sql = '  
CREATE PROCEDURE [model].[USP_CMS_Stage2_Imaging]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN


--patient diagnostic tests performed
	SELECT MAX(ClinicalId) AS Id
		,1 AS ClinicalDataSourceTypeId
		,cp.Id AS ClinicalProcedureId
		,ap.EncounterId AS EncounterId
		,pc.PatientId AS PatientId
		,DATEADD(mi, AppTime, (CONVERT(DATETIME, AppDate))) AS PerformedDateTime
		,NULL AS TimeQualifierId
	INTO #PatientDiagnosticTestPerformed
	FROM dbo.PatientClinical pc
	INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
	INNER JOIN model.ClinicalProcedures cp ON ''/'' + cp.NAME = pc.Symptom
	INNER JOIN Resources re ON re.ResourceId = ap.ResourceId1
		AND re.ResourceId = @ResourceIds
	WHERE pc.ClinicalType = ''F''
		AND pc.[Status] = ''A''
		AND ClinicalProcedureCategoryId = 2
	GROUP BY cp.Id
		,ap.EncounterId
		,pc.PatientId
		,cp.NAME
		,DATEADD(mi, AppTime, (CONVERT(DATETIME, AppDate)))

	--Denominator
	SELECT p.PatientId
		,ePerformed.Id AS PerformedEncounterId
		,o.Id OrderId
		,p.ClinicalProcedureId
		,p.Id AS PerformedId
		,eOrdered.StartDateTime AS OrderedStartDateTime
		,ePerformed.StartDateTime AS PerformedStartDateTime
	INTO #Denominator
	FROM model.EncounterDiagnosticTestOrders ed
	INNER JOIN model.DiagnosticTestOrders o ON o.Id = ed.DiagnosticTestOrderId
	INNER JOIN model.Encounters eOrdered ON eOrdered.Id = ed.EncounterId
		AND eOrdered.StartDateTime BETWEEN @StartDate
			AND @EndDate
	INNER JOIN #PatientDiagnosticTestPerformed p ON p.PatientId = eOrdered.PatientId
		AND p.ClinicalProcedureId = o.ClinicalProcedureId
	INNER JOIN model.Encounters ePerformed ON p.EncounterId = ePerformed.Id
		AND ePerformed.StartDateTime >= eOrdered.StartDateTime

	--Numerator - 
	SELECT d.PatientId
		,d.PerformedEncounterId
		,d.OrderId
		,d.ClinicalProcedureId
		,d.PerformedId
		,OrderedStartDateTime
		,PerformedStartDateTime
		,pct.Code AS ScanType
	INTO #GetFilesHelp
	FROM #Denominator d
	INNER JOIN model.ClinicalProcedures c ON c.Id = d.ClinicalProcedureId
	INNER JOIN df.Questions q ON c.NAME = q.Question
		AND q.QuestionParty <> ''P''
	INNER JOIN PracticeCodeTable pct ON pct.AlternateCode = q.QuestionOrder
	WHERE ReferenceType = ''SCANTYPE''

	/*File named in this format:  ScanType-_EncounterId_PatientId-YYYYMMDD-##.@@@
	If EncounterId = ePerformed.Id then it''s a match
	If no EncounterId and PatientId = d.PatientId and YYYYMMDD = ePerformed.StartDateTime, then it''s a match
	*/

	DECLARE @PatientId NVARCHAR(20)
	DECLARE @EPerformedId NVARCHAR(20)
	DECLARE @Scantype NVARCHAR(50)
	DECLARE @CountParm INT
	DECLARE @TotParm INT

	DECLARE @path NVARCHAR(max)

	IF EXISTS (	SELECT * FROM model.ApplicationSettings	WHERE NAME = ''ServerDataPath'')
		SELECT @Path = Value
		FROM model.ApplicationSettings
		WHERE NAME = ''ServerDataPath''
	ELSE
		SET @Path = @@SERVERNAME + ''\IOP\Pinpoint\MyScan''

	CREATE TABLE #Numerator ( 
		PatientId INT
		,OrderId INT
		,ClinicalProcedureId INT
		,PerformedId INT
		,OrderedStartDateTime DATETIME
		,PerformedStartDateTime DATETIME
		,DATE DATETIME
		,Path NVARCHAR(MAX)
		)

	DECLARE numCursor CURSOR FOR SELECT PatientId FROM #GetFilesHelp
	OPEN numCursor

	FETCH NEXT FROM numCursor INTO @PatientId

	SELECT @Scantype = Scantype FROM #GetFilesHelp WHERE PatientId = @PatientId

	WHILE @@FETCH_STATUS = 0
	BEGIN	
			
			SELECT ROW_NUMBER() OVER (
					ORDER BY path
					) AS Row
				,CASE WHEN dbo.[IsNullOrEmpty]([IO.Practiceware.SqlClr].[SearchRegexPattern](path,''[0-9]+_'')) = 0
						THEN LEFT([IO.Practiceware.SqlClr].[SearchRegexPattern](path,''[0-9]+_''),LEN([IO.Practiceware.SqlClr].[SearchRegexPattern](path,''[0-9]+_''))-1) 
						ELSE '''' END AS EncounterID
				,CASE WHEN dbo.[IsNullOrEmpty]([IO.Practiceware.SqlClr].[SearchRegexPattern](path,''[0-9]+-'')) = 0
						THEN LEFT([IO.Practiceware.SqlClr].[SearchRegexPattern](path,''[0-9]+-''),LEN([IO.Practiceware.SqlClr].[SearchRegexPattern](path,''[0-9]+-''))-1) 
						ELSE '''' END AS PatientId
				,CASE WHEN dbo.[IsNullOrEmpty]([IO.Practiceware.SqlClr].[SearchRegexPattern](path,''[0-9]+-'')) = 0
						THEN RIGHT([IO.Practiceware.SqlClr].[SearchRegexPattern](path,''-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]''),8) 
						ELSE '''' END AS DATE
				,path
			INTO #paths
			FROM [IO.Practiceware.SqlClr].GetFiles(@Path + CONVERT(NVARCHAR(10), @PatientId), ''*.*'')
			WHERE path LIKE ''%'' + @Scantype + ''%''

			INSERT INTO #Numerator
			SELECT d.PatientId
				,d.OrderId
				,d.ClinicalProcedureId
				,d.PerformedId
				,d.OrderedStartDateTime
				,d.PerformedStartDateTime
				,p.DATE
				,p.Path
			FROM #Denominator d
			INNER JOIN #paths p ON d.PatientId = CONVERT(INT,p.PatientId)
				AND CONVERT(INT,p.EncounterId) = d.PerformedEncounterId
			WHERE p.EncounterId <> ''''
				OR p.DATE = CONVERT(NVARCHAR(8), d.PerformedStartDateTime, 112)

			DROP TABLE #paths

		FETCH NEXT FROM numCursor INTO @PatientId
	END

	CLOSE numCursor
	DEALLOCATE numCursor
	

	SELECT @CountParm = COUNT(DISTINCT Path)
	FROM #Numerator

	SELECT @TotParm = COUNT(*)
	FROM #Denominator

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	select d.PatientId AS DENOMINATOR, p.FirstName, p.LastName, d.OrderId, d.OrderedStartDateTime
	from #Denominator d
	inner join model.patients p on p.id = d.PatientId
	order by p.LastName, p.FirstName, d.PatientId, d.OrderId, d.OrderedStartDateTime

	select DISTINCT d.PatientId AS NUMERATOR, p.FirstName, p.LastName, d.Path
	from #Numerator d
	inner join model.patients p on p.id = d.PatientId
	order by p.LastName, p.FirstName, d.PatientId

	-- drop table #paths
	DROP TABLE #Denominator
	DROP TABLE #GetFilesHelp
	DROP TABLE #PatientDiagnosticTestPerformed
	DROP TABLE #Numerator


END
'
IF OBJECT_ID('model.USP_CMS_Stage2_Imaging') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)
