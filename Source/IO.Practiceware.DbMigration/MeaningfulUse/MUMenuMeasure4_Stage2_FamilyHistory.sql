/****** Object:  StoredProcedure [model].[USP_CMS_Stage2_FamilyHistory]    Script Date: 12/17/2013 2:37:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--exec [model].[USP_CMS_Stage2_FamilyHistory] '12/15/2013', '03/15/2014', 192
DECLARE @sql NVARCHAR(MAX)

SET @sql = 
	'
CREATE PROCEDURE [model].[USP_CMS_Stage2_FamilyHistory]    
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   

BEGIN    

IF @ResourceIds LIKE ''%,%''
			RAISERROR (''Please ONLY send one doctor id at a TIME.'',16	,2)
			
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

--Denominator
select PatientId, e.Id as EncounterId, a.DateTime as AppointmentDate
into #Denominator
from model.encounters e
inner join model.appointments a on a.EncounterId = e.id
where a.DateTime between @StartDate and DATEADD(mi,1440,@EndDate) --DATEADD so that EndDate includes the whole day of encounters
and e.EncounterStatusId = 7
and a.UserId = @ResourceIds

-- numerator
SELECT v.patientid,v.EncounterId,v.AppointmentDate,FamilyRelationships_Id
	into #numerator
	FROM (
		SELECT
		 ap.patientid,ap.EncounterId,ap.AppTime as AppointmentDate		
		 ,CASE 
			WHEN frHWoutS.Id IS NULL
				THEN
					CASE SUBSTRING(pc.FindingDetail,0,2)
			WHEN ''*''
				THEN frC.Id
			ELSE frH.Id 
						END 
			ELSE frHWoutS.Id
			END AS FamilyRelationships_Id
	FROM PatientClinical pc 
	INNER JOIN dbo.PatientClinical pc2 ON pc.PatientId = pc2.PatientId
		AND (dbo.ExtractTextValue(''-'', pc.FindingDetail, ''='') = dbo.ExtractTextValue(''('', ''('' + pc2.FindingDetail, ''-FAM'')
			OR (dbo.ExtractTextValue(''-'', pc.FindingDetail, ''='') = ''HD''
				AND dbo.ExtractTextValue(''('', ''('' + pc2.FindingDetail, ''-FAM'') = ''HEART-DISEASE'')
			OR (dbo.ExtractTextValue(''-'', pc.FindingDetail, ''='') = ''EYE-DISORDER''
				AND dbo.ExtractTextValue(''('', ''('' + pc2.FindingDetail, ''-FAM'') = ''OTHER-EYE-DISORDER'')
			OR (dbo.ExtractTextValue(''-'', pc.FindingDetail, ''='') = dbo.ExtractTextValue(''*'', pc2.FindingDetail, ''-FAM''))
			OR (dbo.ExtractTextValue(''-'', pc.FindingDetail, ''='') = ''HD''
				AND dbo.ExtractTextValue(''*'', pc2.FindingDetail, ''-FAM'') = ''HEART-DISEASE'')
			OR (dbo.ExtractTextValue(''-'', pc.FindingDetail, ''='') = ''EYE-DISORDER''
				AND dbo.ExtractTextValue(''*'', pc2.FindingDetail, ''-FAM'') = ''OTHER-EYE-DISORDER'')
			OR (dbo.ExtractTextValue(''-'', pc.FindingDetail, ''='') = ''UNKNOWN-FHX''
				AND dbo.ExtractTextValue(''('', ''('' + pc2.FindingDetail, ''-FAM'') = ''UNKNOWN''))
		AND (pc2.Symptom = ''/FAMILY MEMBER - - - - CATARACT,  RETINAL DISEASE,  GLAUCOMA,  CANCER OF THE EYE,  BLINDNESS,  OTHER''
			OR pc2.Symptom = ''/FAMILY MEMBER - - - - DIABETES,  CANCER,  BLOOD PRESSURE,  SICKLE CELL,  MIGRAINE'')
	INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
	LEFT JOIN model.FamilyRelationships frH ON frH.NAME = dbo.ExtractTextValue(''('', ''('' + pc.FindingDetail, ''-'')
	LEFT JOIN model.FamilyRelationships frHWoutS ON frHWoutS.NAME = dbo.ExtractTextValue(''('', ''('' + pc.FindingDetail, ''s-'') --This is because Patient Clincal sometimes saves plurl, but enum values are singular.
	LEFT JOIN model.FamilyRelationships frC ON frC.NAME = dbo.ExtractTextValue(''*'', pc.FindingDetail, ''-'')
	WHERE (pc.Symptom = ''?WHICH FAMILY''
			OR pc.Symptom = ''?TYPE OF FAMILY MEDICAL HISTORY'')
		AND pc.STATUS = ''A'' AND pc2.STATUS = ''A''
	GROUP BY ap.patientid,ap.EncounterId,ap.AppTime,pc.FindingDetail
		,pc2.FindingDetail
		,frH.Id
		,frC.Id
		,pc.PatientId
		,frHWoutS.Id
	) v
INNER join #Denominator d on v.patientid=d.patientid and FamilyRelationships_Id in (2,3,5,11,15,16,17,19,20)
GROUP BY  v.patientid, v.EncounterId, v.AppointmentDate,FamilyRelationships_Id


SET @TotParm = (SELECT COUNT (DISTINCT PatientID) from #Denominator)
SET @CountParm = (SELECT COUNT (DISTINCT PatientID) from #Numerator)

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.AppointmentDate
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

select d.PatientId, p.FirstName, p.LastName
from #Numerator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

DROP TABLE #Denominator
DROP TABLE #Numerator

END 
'

IF OBJECT_ID('model.USP_CMS_Stage2_FamilyHistory') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)