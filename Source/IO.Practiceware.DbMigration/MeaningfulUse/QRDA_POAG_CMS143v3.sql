/****** Object:  StoredProcedure [qrda].[QRDA_POAG_CMS143v3]    Script Date: 9/4/2014 2:57:32 PM ******/
/***FOR DROPPING (IF EXIST) *******/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_POAG_CMS143v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [qrda].[QRDA_POAG_CMS143v3]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_POAG_CMS143v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [qrda].[QRDA_POAG_CMS143v3]
(
@StartDate datetime,
@EndDate datetime,
@UserId int
)
AS
BEGIN

		-- aged 18 and over, OV during MP, POAG diagnosis <= OV
		SELECT pd.Id
			, apOV.AppointmentId
			, FirstName + '' '' + LastName AS Name
		INTO #InitialPopulation
		FROM model.Patients pd
		INNER JOIN dbo.Appointments apOV ON apOV.PatientId = pd.Id
			AND apOV.ResourceId1 = @UserId
			AND apOV.AppTime > 0
		INNER JOIN PatientReceivableServices prs ON apOv.AppointmentId = prs.AppointmentId
			AND prs.Service IN
					(''92002'', ''92004'', ''92012'', ''92014''
					, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''
					, ''99212'', ''99213'', ''99214'', ''99215''
					, ''99241'', ''99242'', ''99243'', ''99244'', ''99245''
					, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310''
					, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''
					, ''99334'', ''99335'', ''99336'', ''99337'') 
			AND CONVERT(datetime, prs.ServiceDate) BETWEEN @StartDate AND @EndDate
			AND prs.Status=''A''
		INNER JOIN dbo.Appointments apDx ON apDx.PatientId = pd.Id
			AND apDx.AppDate <= prs.ServiceDate
		INNER JOIN PatientClinical pc ON pc.AppointmentId = apDx.AppointmentId
			AND pc.ClinicalType = ''Q'' 
			AND pc.Status=''A''
			AND SUBSTRING(pc.FindingDetail,1,6)  IN (''365.10'', ''365.11'', ''365.12'', ''365.15'')
		WHERE DATEDIFF (yyyy, pd.DateOfBirth, @StartDate) >= 18
			AND pd.LastName <> ''TEST''

		--Denominator
		SELECT ip.Id
			, Name
			, 0 AS IsInNumerator
		INTO #Denominator
		FROM #InitialPopulation ip

		--Numerator - optic nerve head evaluation
		CREATE TABLE #Numerator(PatientId INT, IsInNumerator BIT)

		INSERT INTO #Numerator

		SELECT d.Id, 1
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.Id
			AND DATEDIFF (mm, CONVERT(datetime, @EndDate), CONVERT(datetime, ap.appdate)) BETWEEN -12 AND 0
		INNER JOIN PatientClinical pcF On pcF.AppointmentId=ap.AppointmentId
			AND pcF.ClinicalType=''F'' 
			AND pcF.Status=''A''
			AND pcF.Symptom IN (''/CUP/DISC''
				, ''/PQRI (POAG, OPTIC NERVE EVAL)''
				, ''/OCT''
				, ''/OCT, OPTIC NERVE''
				, ''/OCT, RNFL'')
		GROUP BY d.Id
		
		UNION
		
		SELECT d.Id, 1
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.Id
			AND DATEDIFF (mm, CONVERT(datetime, @EndDate), CONVERT(datetime, ap.appdate)) BETWEEN -12 AND 0
		INNER JOIN PatientClinical pcQ ON pcQ.AppointmentId=ap.AppointmentId 
			AND pcQ.ClinicalType=''Q'' 
			AND pcQ.Status=''A''
			AND pcQ.ImageDescriptor =''L'' 
			AND pcQ.FindingDetail NOT IN (''LN'', ''LX'', ''365.10'', ''365.11'', ''365.12'', ''365.15'')
		INNER JOIN PatientClinical pcU ON pcU.AppointmentId=ap.AppointmentId 
			AND pcU.ClinicalType = ''U''
			AND pcU.Status = ''A''
		GROUP BY d.Id

		SELECT DISTINCT
			 d.Id 
			,d.Name
			,CONVERT(bit, d.IsInNumerator + COALESCE(n.IsInNumerator, 0)) AS IsInNumerator
		FROM #Denominator d
		LEFT JOIN #Numerator n ON n.PatientId = d.Id

		DROP TABLE #InitialPopulation
		DROP TABLE #Denominator
		DROP TABLE #Numerator

END
'
END 
GO