DECLARE @sql NVARCHAR(MAX)

SET @sql = 
	'
CREATE PROCEDURE  [model].[USP_CMS_Stage1_ClinicalSummary] 
(    
	@StartDate DateTime,    
	@EndDate DateTime,    
	@ResourceIds int   
)    
AS   

BEGIN    


DECLARE @TotParm INT;    
DECLARE @CountParm INT;
DECLARE @Status NVARCHAR (1)   
SET @TotParm=0;    
SET @CountParm=0;    

--#Numerator

	CREATE TABLE #Numerator(
	 PatientId INT, 
	 EncounterId INT, 
	 EncounterDate DATETIME,
	 ClinicalSummaryId NVARCHAR(100)
	 )

--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation ipp

--Numerator 
	--1.  Is autoprinting on?  If so, 100%
	SELECT @Status=FieldValue 
	FROM PracticeInterfaceConfiguration 
	WHERE FieldReference = ''PRINT''



	SELECT @TotParm = COUNT(DISTINCT EncounterId)
	FROM #Denominator

	IF @Status=''T''  
		BEGIN		  
			SELECT @CountParm = @TotParm
		END 
	ELSE 
		BEGIN
			-- Patient Declines
			INSERT INTO #Numerator
			SELECT  d.PatientId, d.EncounterId, d.EncounterDate, CONVERT(nvarchar(100),ClinicalId) as ClinicalSummaryId
			FROM #Denominator d
			INNER JOIN PatientClinical pc on pc.AppointmentId = d.EncounterId
			WHERE ClinicalType = ''A'' and FindingDetail like ''CLIN SUMMARY, PATIENT DECLINES%''
			
			UNION ALL

			--Practice posts availability of access
			select d.PatientId, d.EncounterId, d.EncounterDate, ''none'' as ClinicalSummaryId
			from #Denominator d
			INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''ACCESSADS'' 
				AND SUBSTRING (Code, 6, 14) = ''Patient Portal''
				AND pct.AlternateCode <> ''''
			WHERE d.EncounterDate >= CONVERT(datetime, pct.AlternateCode)

			UNION ALL

			-- Clinical Data Files
			-- portal
			select d.PatientId, d.EncounterId, d.EncounterDate, CONVERT(nvarchar(100),esm.Id) as ClinicalSummaryId
			from #Denominator d
			inner join model.ExternalSystemMessagePracticeRepositoryEntities esmpre on esmpre.PracticeRepositoryEntityKey = Convert(varchar(100),d.EncounterId)
			inner join model.ExternalSystemMessages esm on esm.id = esmpre.ExternalSystemMessageId
			inner join model.ExternalSystemExternalSystemMessageTypes esesmt on esesmt.id = esm.ExternalSystemExternalSystemMessageTypeId 
			inner join model.ExternalSystemMessageTypes esmt on esmt.Id = esesmt.ExternalSystemMessageTypeId and esmt.Name = ''EncounterClinicalSummaryCCDA'' and IsOutbound = 1
			inner join model.Appointments ap on ap.Id = esmpre.PracticeRepositoryEntityKey
			and DATEDIFF (dd, ap.datetime, 
				[IO.Practiceware.SqlClr].ConvertToLocalDateTime(esm.CreatedDateTime)) <= 4

			SET @CountParm = (SELECT COUNT (DISTINCT EncounterId) from #Numerator)
		END

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.EncounterDate
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.EncounterDate, d.EncounterId


IF OBJECT_ID(''#Numerator'') is not null
BEGIN
	select n.PatientId, p.FirstName, p.LastName, n.EncounterId, n.EncounterDate, n.ClinicalSummaryId
	from #Numerator n
	inner join model.patients p on p.id = n.PatientId
	order by p.LastName, p.FirstName, n.PatientId, n.EncounterDate, n.EncounterId
	DROP TABLE #Numerator
END 

DROP TABLE #Denominator

DROP TABLE #InitialPatientPopulation
END 
'

IF OBJECT_ID('model.USP_CMS_Stage1_ClinicalSummary') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)