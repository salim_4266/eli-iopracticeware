/****** Object:  StoredProcedure [qrda].[QRDA_ReferralLoop_CMS050v3]    Script Date: 9/4/2014 11:22:24 AM ******/
/***FOR DROPPING (IF EXIST) *******/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_ReferralLoop_CMS050v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [qrda].[QRDA_ReferralLoop_CMS050v3]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_ReferralLoop_CMS050v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [qrda].[QRDA_ReferralLoop_CMS050v3]
(
@StartDate datetime,
@EndDate datetime,
@UserId int
)
As
BEGIN

		--Initial Patient Population
		SELECT p.Id
			,FirstName +'' '' +p.LastName AS Name
			,SentDateTime
			,ResponseReceivedDateTime
			,0 AS IsInNumerator
			,ResourceId1
		INTO #Denominator
		FROM model.Patients p
		INNER JOIN Appointments apOV on apOV.PatientId = p.Id 
			AND ResourceId1 = @UserId
			AND apOV.AppTime > 0
		INNER JOIN PatientReceivableServices prs ON apOv.AppointmentId = prs.AppointmentId
			AND Service IN (
				--Office Visit
				''99201'', ''99202'', ''99203'', ''99204'', ''99205'', ''99212'', ''99213'', ''99214'', ''99215''
				--Preventive Care Services - Established Office Visit, 18 and Up
				, ''99395'', ''99396'', ''99397''
				--Preventive Care Services-Initial Office Visit, 18 and Up
				, ''99385'', ''99386'', ''99387''
				--Preventive Care- Initial Office Visit, 0 to 17
				, ''99381'', ''99382'', ''99383'', ''99384''
				)
			AND prs.Status = ''A''
			AND CONVERT(datetime,prs.ServiceDate) BETWEEN @StartDate AND @EndDate
		--Referral
		INNER JOIN model.EncounterTransitionOfCareOrders etoco ON etoco.EncounterId = apOV.EncounterId
		WHERE p.LastName <> ''TEST''

		--Numerator
		SELECT Id
			,Name
			,SentDateTime
			,ResponseReceivedDateTime
			,1 AS IsInNumerator
		INTO #Numerator
		FROM #Denominator
		--Consultant Report
		WHERE ResponseReceivedDateTime IS NOT NULL

		--Output
		SELECT DISTINCT
			 d.Id
			,d.Name
			,CONVERT(bit, d.IsInNumerator + COALESCE(n.IsInNumerator, 0)) AS IsInNumerator
		FROM #Denominator d
		LEFT JOIN #Numerator n ON n.id = d.id

		drop table #Denominator
		drop table #Numerator
		
	END
'
END
GO