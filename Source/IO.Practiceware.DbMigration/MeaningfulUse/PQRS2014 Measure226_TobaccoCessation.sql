DECLARE @sql nvarchar(max)
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_PQRI_GetTobaccoCessation]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN

	CREATE TABLE #TMPResources(ResourceId nvarchar(1000))
	IF @ResourceIds=''''
		BEGIN
			INSERT INTO #TMPResources 
			SELECT Resources.ResourceId 
			FROM Resources 
			WHERE ResourceType IN(''D'',''Q'')
		END
	ELSE
		BEGIN
			DECLARE @TmpResourceIds varchar(100)
			DECLARE @ResId varchar(10)
			SELECT @TmpResourceIds =@ResourceIds
			WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
				BEGIN
					SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
					SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    
					INSERT INTO #TMPResources values(@ResId)
				END
				INSERT INTO #TMPResources values(@TmpResourceIds)
		END
	
		---Patients 18 and over with office visits in the reporting period
	SELECT pd.Id AS PatientId
			,pd.FirstName
			,pd.LastName
			,apov.Appdate
			,re.NPI
			,pd.DateOfBirth
			,SUBSTRING(g.name,1,1) AS PatientGender
			,apOV.appointmentId as EncounterId
			,Service
		INTO #Visits
		FROM model.Patients pd
		INNER JOIN model.Gender_Enumeration g on g.id = pd.genderid
		INNER JOIN dbo.Appointments apOV ON apOV.PatientId = pd.Id
			AND apOV.AppTime > 0
		INNER JOIN PatientReceivables pr ON pr.appointmentid = apov.appointmentid
		INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
			AND pri.InsurerReferenceCode IN (''N'', ''M'', ''['')
		INNER JOIN Resources re ON re.ResourceId = pr.BillToDr
			AND re.ResourceId IN (SELECT #TMPResources.ResourceId from #TMPResources)
		INNER JOIN PatientReceivableServices prs ON apOv.AppointmentId = prs.AppointmentId
			AND prs.Service in (
				''90791'', ''90792'', ''90832'', ''90834'', ''90837'', ''90839'', ''90845'', ''92002'', ''92004'', ''92012'', ''92014'', ''96150'', ''96151'', ''96152'', ''97003'', ''97004'', ''99201'', ''99202'', ''99203'', ''99204'', ''99205'', ''99212'', ''99213'', ''99214'', ''99215'', ''99406'', ''99407'', ''G0438'', ''G0439'')  
			AND prs.Status=''A''
			AND CONVERT(datetime, prs.ServiceDate) BETWEEN @StartDate AND @EndDate
		WHERE DATEDIFF (yyyy, pd.DateOfBirth,  @StartDate) >= 18
			AND pd.LastName <> ''TEST''

		--Denominator
		SELECT v.*
			, 0 AS Numerator
		INTO #Denominator 
		FROM #Visits v

		--Numerator
		SELECT d.PatientId
			, 1 AS Numerator
		INTO #Numerator
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.PatientId
			AND DATEDIFF (month, CONVERT(datetime, ap.AppDate), CONVERT(datetime, @EndDate)) <= 24 
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
			AND pc.ClinicalType = ''F'' 
			AND pc.Status=''A''
			AND pc.Symptom = ''/SMOKING'' AND pc.FindingDetail in (''*NEVER=T14857 <NEVER>'',''*FORMER=T14856 <FORMER>'')

		UNION
		
		SELECT d.PatientId
			, 1 AS Numerator
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.PatientId
			AND DATEDIFF (month, CONVERT(datetime, ap.appDate), CONVERT(datetime, @EndDate)) <= 24
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
			AND pc.ClinicalType = ''F'' 
			AND pc.Status=''A''
			AND pc.Symptom = ''/PQRS (TOBACCO NON-USER)''

		UNION

		SELECT d.PatientId
			, 2 AS Numerator
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.PatientId
			AND DATEDIFF (month, CONVERT(datetime, ap.appDate), CONVERT(datetime, @EndDate)) <= 24
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
			AND pc.ClinicalType = ''F'' 
			AND pc.Status=''A''
			AND pc.Symptom = ''/SMOKING'' AND pc.FindingDetail = ''*CESSATION=T22892 <CESSATION>''
		
		UNION
		
		SELECT d.PatientId
			, 2 AS Numerator
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.PatientId
			AND DATEDIFF (month, CONVERT(datetime, ap.appDate), CONVERT(datetime, @EndDate)) <= 24
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
			AND pc.ClinicalType = ''F'' 
			AND pc.Status=''A''
			AND pc.Symptom = ''/PQRS (TOBACCO USER)''

		SELECT DISTINCT
			 d.PatientId
			 ,FirstName
			 ,LastName
			 ,CONVERT(nvarchar(10), CONVERT(datetime, appdate),101) AS EncounterDate
			 ,NPI AS PhysicianNPI
			 ,DateOfBirth AS PatientDateOfBirth
			 ,PatientGender
			 ,1 AS IsMedicare
			 ,Service AS EM
			,CASE n.Numerator
				WHEN 1 THEN ''1036F''
				WHEN 2 THEN ''4004F''
				ELSE ''4004F 8P''
				END AS Numerator
		FROM #Denominator d
		LEFT JOIN #Numerator n ON n.PatientId = d.PatientId

		DROP TABLE #Visits
		DROP TABLE #Denominator
		DROP TABLE #Numerator

	END
'
IF OBJECT_ID('USP_PQRI_GetTobaccoCessation') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)