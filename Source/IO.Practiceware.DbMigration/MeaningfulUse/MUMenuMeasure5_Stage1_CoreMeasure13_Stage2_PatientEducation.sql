/**M6**** Object:  StoredProcedure [dbo].[USP_CMS_PatientEducation]    Script Date: 12/17/2013 9:26:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--exec [model].[USP_CMS_Stage1_PatientEducation] '12/15/2013', '03/15/2014', 192
DECLARE @sql NVARCHAR(MAX)

SET @sql = '
CREATE PROCEDURE [model].[USP_CMS_Stage1_PatientEducation]    
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   
BEGIN    
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

--Denominator
SELECT EncounterId
	,EncounterDate
	,PatientId
INTO #Denominator
FROM #InitialPatientPopulation ipp
   
--Numerator    
select DISTINCT d.*
into #Numerator
from #Denominator d
inner join model.patients p on p.id = d.PatientId
inner join model.Gender_Enumeration g on g.Id = p.GenderId
inner join [IO.Practiceware.SqlClr].GetNumeratorDetailsForPatientEducation (@StartDate,@EndDate,@ResourceIds,''OMEDIX'') pe 
on pe.PatientDob = p.DateOfBirth and pe.patientFirstName = p.FirstName and pe.patientLastName = p.LastName and pe.patientGender = SUBSTRING(g.Name,1,1)

SELECT @TotParm = COUNT(DISTINCT PatientId) FROM #Denominator
SELECT @CountParm = COUNT (DISTINCT PatientID) from #Numerator

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.EncounterDate
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.EncounterDate, d.EncounterId

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.EncounterDate
from #Numerator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.EncounterDate, d.EncounterId

DROP TABLE #Denominator
DROP TABLE #Numerator

END
'

IF OBJECT_ID('model.USP_CMS_Stage1_PatientEducation') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)