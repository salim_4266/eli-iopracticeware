/****** Object:  StoredProcedure [qrda].[QRDA_CataractsVA_CMS133v3]    Script Date: 9/4/2014 11:20:59 AM ******/
/***FOR DROPPING (IF EXIST) *******/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_CataractsVA_CMS133v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [qrda].[QRDA_CataractsVA_CMS133v3]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_CataractsVA_CMS133v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [qrda].[QRDA_CataractsVA_CMS133v3]
(
@StartDate datetime,
@EndDate datetime,
@UserId int
)
AS
BEGIN

		--#191 All Cataract Surgeries in reporting period
		SELECT p.Id 
			,p.FirstName +'' '' + p.LastName AS Name
			,apOV.AppDate AS AppDate
			,prs.Service 
			,0 AS VisualAcuity
			,CASE
				WHEN prs.Modifier LIKE ''%LT%'' THEN ''OS'' 
				WHEN prs.Modifier LIKE ''%RT%'' THEN ''OD'' 
				END AS Modifier
		INTO #AllCataracts
		FROM model.Patients p
		INNER JOIN Appointments apOV ON p.Id = apOV.PatientId
			AND @UserId = apOV.ResourceId1
			AND apOV.AppTime > 0
		INNER JOIN PatientReceivableServices prs ON prs.AppointmentId = apOV.AppointmentId
			AND prs.Service IN (''66840'', ''66850'', ''66852'', ''66920'', ''66930'', ''66940'', ''66982'', ''66983'', ''66984'')  
			AND prs.Modifier NOT LIKE ''%56%''
			AND (prs.Modifier LIKE ''%LT%'' OR prs.Modifier LIKE ''%RT%'')
			AND prs.Status = ''A''
			AND CONVERT(datetime, prs.ServiceDate) BETWEEN @StartDate AND DATEADD (mm, -3, @EndDate)
		WHERE DATEDIFF (yyyy, CONVERT(datetime, DateOfBirth), CONVERT(datetime, apOV.AppDate)) >= 18
			AND p.LastName <> ''TEST''
		 GROUP BY  apOV.AppointmentId
			,p.Id 
			,p.FirstName +'' '' + p.LastName 
			,apOV.AppDate
			,prs.Service 
			,prs.Modifier


		--comorbid patients
		SELECT apComorbid.PatientId 
		INTO #ComorbidPatients
		FROM #AllCataracts a
		INNER JOIN Appointments apComorbid ON a.Id = apComorbid.PatientId
			AND apComorbid.AppDate <= a.AppDate
		INNER JOIN PatientClinical pcComorbid ON apComorbid.AppointmentId = pcComorbid.AppointmentId
			AND pcComorbid.ClinicalType IN (''Q'', ''B'', ''K'')
			AND pcComorbid.Status = ''A''
			AND (SUBSTRING(pcComorbid.FindingDetail,1,6) IN (
					''364.00'', ''364.01'', ''364.02'', ''364.03'', ''364.04'', ''364.05'',
					''368.01'', ''368.02'', ''368.03'', 
					''366.32'', ''366.33'', 
					''364.21'', ''364.22'', ''364.23'', ''364.24'', 
					''363.43'',
					''363.72'', 
					''363.61'', ''363.62'', ''363.63'', 
					''363.30'', ''363.31'', ''363.32'', ''363.33'',''363.35'',
					''364.10'', ''364.11'', 
					''371.01'', ''371.02'', ''371.03'', ''371.04'',
					''371.00'', --''371.03'',''371.04'',
					''371.20'', ''371.21'', ''371.22'', ''371.23'', ''371.43'', ''371.44'',
					''362.50'', ''362.51'', ''362.52'', ''362.53'', ''362.54'', ''362.55'', ''362.56'', ''362.57'',
					''360.20'', ''360.21'', ''360.23'', ''360.24'', ''360.29'', 	
					''362.07'',
					''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'', 
					''377.51'', ''377.52'', ''377.53'', ''377.54'', 
					''377.75'', 
					''363.10'', ''363.11'', ''363.12'', ''363.13'', ''363.14'', ''363.15'', 
					''363.00'', ''363.01'', ''363.03'', ''363.04'', ''363.05'', ''363.06'', ''363.07'', ''363.08'', 
					''365.10'', ''365.11'', ''365.12'', ''365.13'', ''365.14'', ''365.15'',
					''365.20'', ''365.21'', ''365.22'', ''365.23'', ''365.24'', ''365.31'', 
					''365.32'', ''365.51'', ''365.52'', ''365.59'', ''365.60'', ''365.61'',
					''365.62'', ''365.63'', ''365.64'', ''365.65'', ''365.81'', ''365.82'', 
					''365.83'', ''365.89'', 
					''365.41'', ''365.42'', ''365.43'', ''365.44'', ''365.60'', ''365.61'', 
					''365.62'', ''365.63'', ''365.64'', ''365.65'', ''365.81'', ''365.82'', 
					''365.83'', ''365.89'', 
					''371.50'', ''371.51'', ''371.52'', ''371.53'', ''371.54'', ''371.55'', ''371.56'', ''371.57'', ''371.58'',
  					''363.50'', ''363.51'', ''363.52'', ''363.53'', ''363.54'', ''363.55'', ''363.56'', ''363.57'', 
					''362.70'', ''362.71'', ''362.72'', ''362.73'', ''362.74'', ''362.75'', ''362.76'', 
					--''360.20'',''360.21'', 
					''370.03'',
					''369.10'', ''369.11'', ''369.12'', ''369.13'', ''369.14'', ''369.15'', ''369.16'', ''369.17'', ''369.18'',
					''379.51'',
					''377.10'', ''377.11'', ''377.12'', ''377.13'', ''377.14'', ''377.15'', ''377.16'', 
					''377.30'', ''377.31'', ''377.32'', ''377.33'', ''377.34'', ''377.39'',
					''362.12'', ''362.16'', ''362.18'', 
					''371.70'', ''371.71'', ''371.72'', ''371.73'', 	
					''377.41'',
					''379.11'', ''379.12'',
					''360.11'', ''360.12'', ''360.13'', ''360.14'', ''360.19'', 
					''362.81'', ''362.82'', ''362.83'', ''362.84'', ''362.85'', ''362.89'', 
					''363.20'', ''363.21'', ''363.22'', 
					''371.60'', ''371.61'', ''371.62'', 
					''369.00'', ''369.01'', ''369.02'', ''369.03'', ''369.04'', ''369.05'', 
					''369.06'', ''369.07'', ''369.08'', 	
					''360.00'', ''360.01'', ''360.02'', ''360.03'', ''360.04'',
					''361.00'', ''361.01'', ''361.02'', ''361.03'', ''361.04'', ''361.05'', ''361.06'', ''361.07'', 
					''362.31'', ''362.32'', ''362.35'', ''362.36'', 
					''362.20'', ''362.21'', ''362.22'', ''362.23'', ''362.24'', ''362.25'', ''362.26'', ''362.27'',	
					''379.04'', ''379.05'', ''379.06'', ''379.07'', ''379.09'', 
					''362.41'', ''362.42'', ''362.43'',  
					--''360.11'', ''360.12'', 
					''368.41'' )
				OR SUBSTRING(pcComorbid.FindingDetail, 1, 5) IN (
					''364.3'', 
					''365.9'',
					''871.0'', ''871.1'', ''871.2'', ''871.3'', ''871.4'', ''871.5'', ''871.6'',''871.7'',''871.9'',''921.3'',
					''940.0'', ''940.1'', ''940.2'', ''940.3'', ''940.4'', ''940.5'', ''940.9'',
					''950.0'', ''950.1'', ''950.2'', ''950.3'', ''950.9'')
			)

		 --#191 Cataract VA Denominator
		 SELECT * 
		 INTO #CataractVADenominator
		 FROM #AllCataracts a
		 LEFT JOIN #ComorbidPatients c ON a.Id = c.PatientId
		 WHERE c.PatientId IS NULL

 

		--#191 Cataract VA Numerator
		SELECT d.AppDate
			, pcVisualAcuity.ClinicalId
			, pcVisualAcuity.Appointmentid
			, pcVisualAcuity.PatientId
			, pcVisualAcuity.Symptom
			, pcVisualAcuity.FindingDetail
			, d.Modifier
		--which denominator patients have a good vision measurement?
		INTO #CataractGoodVision
		FROM #CataractVADenominator d
		INNER JOIN Appointments ap90Days ON ap90Days.PatientId = d.Id
			AND DATEDIFF(dd, d.AppDate, ap90Days.AppDate) BETWEEN 0 AND 90
			AND ap90Days.ScheduleStatus = ''D''
		INNER JOIN PatientClinical pcVisualAcuity ON pcVisualAcuity.AppointmentId = ap90Days.AppointmentId
			AND pcVisualAcuity.ClinicalType = ''F''
			AND pcVisualAcuity.Status = ''A''
			AND CHARINDEX('' <'',FindingDetail) > 0
			--vision equal to or better than 20/40; from DynamicForms..FormsControl table
			AND SUBSTRING(FindingDetail, CHARINDEX(''=T'',FindingDetail)+2, CHARINDEX('' <'',FindingDetail)-CHARINDEX(''=T'',FindingDetail)-2) IN (
				''1051'', ''1052'', ''1053'', ''1054'', ''1055'', ''1080'', ''1081'', ''1082'', ''1083'', ''1084'', ''1109'', ''1110'', ''1111'', ''1112'', ''1113'', 
				''1221'', ''1222'', ''1223'', ''1224'', ''1225'', ''1644'', ''1645'', ''1646'', ''1647'', ''1648'', ''1721'', ''1722'', ''1723'', ''1724'', ''1725'', 
				''1744'', ''1745'', ''1746'', ''1747'', ''1748'', ''4021'', ''4022'', ''4023'', ''4024'', ''4025'', ''4311'', ''4312'', ''4313'', ''4314'', ''4315'', 
				''4366'', ''4367'', ''4368'', ''4369'', ''4370'', ''4395'', ''4396'', ''4397'', ''4398'', ''4399'', ''4426'', ''4427'', ''4428'', ''4429'', ''4430'', 
				''4453'', ''4454'', ''4455'', ''4456'', ''4457'', ''4570'', ''4571'', ''4572'', ''4573'', ''4574'', ''4599'', ''4600'', ''4601'', ''4602'', ''4603'', 
				''5171'', ''5173'', ''5175'', ''5176'', ''5177'', ''8040'', ''8041'', ''8042'', ''8043'', ''8044'', ''8108'', ''8109'', ''8110'', ''8111'', ''8112'', 
				''8234'', ''8235'', ''8236'', ''8237'', ''8238'', ''8623'', ''8624'', ''8625'', ''8626'', ''8627'', ''9688'', ''9689'', ''9690'', ''9691'', ''9692'', 
				''9705'', ''9706'', ''9707'', ''9708'', ''9709'', ''9727'', ''9728'', ''9729'', ''9730'', ''9731'', ''9755'', ''9756'', ''9757'', ''9758'', ''9759'', 
				''9773'', ''9774'', ''9775'', ''9776'', ''9777'', ''9893'', ''9894'', ''9895'', ''9896'', ''9897'', ''9907'', ''9908'', ''9909'', ''9910'', ''9911'', 
				''9922'', ''9923'', ''9924'', ''9925'', ''9926'', ''10142'', ''10143'', ''10144'', ''10145'', ''10146'', ''10167'', ''10168'', ''10169'', ''10170'', 
				''10171'', ''10180'', ''10181'', ''10182'', ''10183'', ''10184'', ''10236'', ''10237'', ''10238'', ''10239'', ''10240'', ''10254'', ''10255'', 
				''10256'', ''10257'', ''10258'', ''10435'', ''10436'', ''10437'', ''10438'', ''10439'', ''11185'', ''11186'', ''11187'', ''11188'', ''11189'', 
				''11307'', ''11308'', ''11309'', ''11310'', ''11311'', ''11611'', ''11612'', ''11613'', ''11614'', ''11615'', ''12141'', ''12142'', ''12143'', 
				''12144'', ''12145'', ''12146'', ''12147'', ''12148'', ''12149'', ''12150'', ''12151'', ''12152'', ''12153'', ''12154'', ''12155'', ''12476'', 
				''12477'', ''12478'', ''12479'', ''12505'', ''12513'', ''12514'', ''12515'', ''12544'', ''12545'', ''12546'', ''12547'', ''12833'', ''12835'', 
				''12836'', ''12837'', ''12897'', ''12898'', ''12899'', ''12900'', ''14989'', ''14990'', ''14991'', ''14992'', ''15131'', ''15132'', ''15133'', 
				''15134'', ''15896'', ''15897'', ''15904'', ''15912'', ''16977'', ''16978'', ''16981'', ''16982'', ''16988'', ''16989'', ''16990'', ''16991'', 
				''17024'', ''17025'', ''17026'', ''17027'', ''20501'', ''20502'', ''20503'', ''20504'', ''20538'', ''20539'', ''20540'', ''20541'')
		order by pcVisualAcuity.appointmentid, pcVisualAcuity.ClinicalId

		--#191 Numerator
		--Is good vision on operative eye?  This assumes that the Eye is one clinical id less than the visual acuity.
		SELECT cv.PatientId AS Id,
			cv.AppDate, 
			1 AS VisualAcuity
		INTO #CataractVANumerator
		FROM #CataractGoodVision cv
		INNER JOIN PatientClinical pcEye ON cv.AppointmentId = pcEye.AppointmentId
			AND pcEye.ClinicalType = ''F''
			AND cv.Symptom = pcEye.Symptom
			AND cv.Modifier = SUBSTRING(pcEye.FindingDetail, 2, 2)
		GROUP BY cv.AppDate, cv.patientid


		--#191 Output
		SELECT  DISTINCT d.Id as Id
			,d.Name as Name
			,CONVERT(bit, d.VisualAcuity + COALESCE(n.VisualAcuity, 0)) AS IsInNumerator
		FROM #CataractVADenominator d
		LEFT JOIN #CataractVANumerator n ON d.Id = n.Id
			AND d.AppDate = n.AppDate


		drop table #AllCataracts
		drop table #ComorbidPatients
		drop table #CataractVADenominator
		drop table #CataractGoodVision
		drop table #CataractVANumerator

END
'
END
GO