---------------USP_CMS_GetEPrescStatus----------------

DECLARE @sql nvarchar(max)
SET @sql = N'

CREATE PROCEDURE [dbo].[USP_CMS_GetEPrescStatus] (
	@StartDate NVARCHAR(200)
	,@EndDate NVARCHAR(200)
	,@ResourceIds NVARCHAR(1000)
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	CREATE TABLE #TMPResources (ResourceId NVARCHAR(1000))

	IF @ResourceIds = ''''
	BEGIN
		INSERT INTO #TMPResources
		SELECT Resources.ResourceId
		FROM Resources
		WHERE ResourceType IN (
				''D''
				,''Q''
				)
	END
	ELSE
	BEGIN
		DECLARE @TmpResourceIds VARCHAR(100)
		DECLARE @ResId VARCHAR(10)

		SELECT @TmpResourceIds = @ResourceIds

		WHILE PATINDEX(''%,%'', @TmpResourceIds) <> 0
		BEGIN
			SELECT @ResId = LEFT(@TmpResourceIds, PATINDEX(''%,%'', @TmpResourceIds) - 1)

			SELECT @TmpResourceIds = SUBSTRING(@TmpResourceIds, PATINDEX(''%,%'', @TmpResourceIds) + 1, LEN(@TmpResourceIds))

			INSERT INTO #TMPResources
			VALUES (@ResId)
		END

		INSERT INTO #TMPResources
		VALUES (@TmpResourceIds)
	END

	--denominator before exclusions
	SELECT AppDate
		,pc.PatientId
		,FindingDetail
		,ImageDescriptor
		,ImageInstructions
		,pc.ClinicalId
	INTO #rx
	FROM Appointments ap
	INNER JOIN PatientDemographics pd ON pd.PatientId = ap.PatientId
		AND pd.LastName <> ''TEST''
	INNER JOIN Resources re ON re.ResourceId = ap.ResourceId1
		AND re.ResourceId IN (
			SELECT #TMPResources.ResourceId
			FROM #TMPResources
			)
	INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
	WHERE AppDate BETWEEN @StartDate
			AND @EndDate
		AND pc.ClinicalType = ''A''
		AND pc.STATUS = ''A''
		AND pc.FindingDetail LIKE ''RX-%/P-7/%''

	--exclude changed rx''s
	---changed
	SELECT r.clinicalid
	INTO #exclusions
	FROM #rx r
	INNER JOIN Appointments apChange ON apChange.PatientId = r.PatientId
		AND apChange.AppDate < r.AppDate
	INNER JOIN PatientClinical pcChange ON pcChange.AppointmentId = apChange.AppointmentId
		AND pcChange.ClinicalType = ''A''
		AND SUBSTRING(pcchange.FindingDetail, 6, CASE WHEN CHARINDEX(''-2/'', pcchange.FindingDetail) > 0 THEN CHARINDEX(''-2/'', pcchange.FindingDetail)-6 END) = SUBSTRING(r.FindingDetail, 6,CASE WHEN CHARINDEX(''-2/'', r.FindingDetail) > 0 THEN CHARINDEX(''-2/'', r.FindingDetail) - 6 END)
		AND SUBSTRING(pcchange.Imagedescriptor, 1, 1) = ''C''
	GROUP BY r.ClinicalId
	
	UNION
	
	--otc
	SELECT r.ClinicalId
	FROM #rx r
	INNER JOIN fdb.tblcompositedrug t ON t.MED_ROUTED_DF_MED_ID_DESC = SUBSTRING(r.FindingDetail, 6, CHARINDEX(''-2/'', FindingDetail) - 6)
	WHERE MED_REF_FED_LEGEND_IND_DESC = ''OTC''
	GROUP BY r.ClinicalId

	--Denominator
	SELECT r.ClinicalId
		,r.ImageInstructions
	INTO #denominator
	FROM #rx r
	LEFT JOIN #exclusions e ON e.Clinicalid = r.Clinicalid
	WHERE e.ClinicalId IS NULL

	--numerator
	SELECT d.ClinicalId
	INTO #Numerator
	FROM #denominator d
	WHERE len(d.ImageInstructions) > 10

	SELECT @TotParm = COUNT(*)
	FROM #Denominator

	SELECT @CountParm = COUNT(*)
	FROM #Numerator

	DROP TABLE #TMPResources

	DROP TABLE #RX

	DROP TABLE #Exclusions

	DROP TABLE #Denominator

	DROP TABLE #Numerator

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator
END

'

IF OBJECT_ID('USP_CMS_GetEPrescStatus') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)