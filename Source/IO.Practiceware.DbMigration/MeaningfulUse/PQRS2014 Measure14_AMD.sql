
--------------------------------------MEASURE #14 AMD  PQRS--------------------

DECLARE @sql nvarchar(max)
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_PQRI_GetAMD_Dilated_Macular_Examination]
(
@StartDate nvarchar(100),
@EndDate nvarchar(100),
@ResourceIds nvarchar(1000)
)
AS 

	BEGIN



		CREATE TABLE #TMPResources(ResourceId nvarchar(1000))

		IF @ResourceIds=''''

			BEGIN

				INSERT INTO #TMPResources 

				SELECT Resources.ResourceId 

				FROM Resources 

				WHERE ResourceType IN (''D'',''Q'')

			END

		ELSE

			BEGIN

			DECLARE @TmpResourceIds varchar(100)

			DECLARE @ResId varchar(10)

			SELECT @TmpResourceIds =@ResourceIds

			WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0

				BEGIN

					SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)

					SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    

					INSERT INTO #TMPResources values(@ResId)

				END

				INSERT INTO #TMPResources values(@TmpResourceIds)

			END



		Select p.PatientId AS patient_os_id

			,ap.AppointmentId AS OVAppointmentId

			,MIN(pcDx.Symptom) AS DxRank

			,p.FirstName AS tq_firstname

			,p.LastName AS tq_lastname

			,ap.AppDate AS tq_encounterdt

			,re.NPI AS tq_physiciannpi

			,p.BirthDate AS tq_dob

			,CASE p.Gender

				WHEN ''M'' THEN ''M''

				WHEN ''F'' THEN ''F''

				ELSE ''U''

				END AS tq_gender

			,''1'' AS tq_medicare2

			,ISNULL(MAX(SUBSTRING(icd10.FindingDetailIcd10, 1, 6)),MAX(SUBSTRING(pcDx.FindingDetail, 1, 6))) AS tq_icd   

			,prs.Service AS tq_em

		INTO #AMDDenominator

		FROM PatientDemographics p

		INNER JOIN Appointments ap ON p.PatientId = ap.PatientId

		INNER JOIN PatientClinical pcDx ON ap.AppointmentId = pcDx.AppointmentId

			AND pcDx.ClinicalType IN (''B'', ''K'')

			AND pcDx.Status = ''A''

		INNER JOIN PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId

		INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId

			AND pri.InsurerReferenceCode IN (''N'', ''M'', ''['')

		INNER JOIN Resources re ON re.ResourceId = pr.BillToDr

				AND re.ResourceId IN (SELECT #TMPResources.ResourceId from #TMPResources)

		INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice

			AND prs.Status = ''A''

		LEFT JOIN (SELECT AppointmentId,FindingDetailIcd10 FROM PatientClinicalIcd10  
					WHERE ClinicalType IN (''B'', ''K'')
					AND Status = ''A''
					AND FindingDetailIcd10 IN (''H35.30'',''H35.31'',''H35.32'')
					) AS icd10 ON icd10.AppointmentId=ap.AppointmentId

		WHERE 

			--appointment during reporting period

			ap.AppDate BETWEEN @StartDate AND @EndDate

			--over 50 years

			AND DATEDIFF (yyyy, CONVERT(datetime, BirthDate), CONVERT(datetime, ap.AppDate)) >= 50

			--diagnosis of AMD

			AND	SUBSTRING(pcDx.FindingDetail, 1, 6) IN (''362.50'', ''362.51'', ''362.52'')


			--with office visit

			AND prs.Service IN (''92002'', ''92004'', ''92012'', ''92014''

				, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''

				, ''99212'', ''99213'', ''99214'', ''99215''

				, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310''

				, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''

				, ''99334'', ''99335'', ''99336'', ''99337'') 

			AND p.LastName <> ''TEST''

		 GROUP BY  ap.AppointmentId

			,p.PatientId 

			,p.FirstName 

			,p.LastName 

			,ap.AppDate

			,re.NPI 

			,p.BirthDate

			,p.Gender

			,prs.Service 

	

		--#14 AMD NUMERATOR 

		---Dilation, Severity, Macular Thickening

		SELECT p.patient_os_id

			, 1 AS tq_dilatedmac3

		INTO #AMDNumerator

		FROM #AMDDenominator p

		INNER JOIN Appointments App1Year ON App1Year.PatientId = patient_os_id

			AND DATEDIFF (mm, CONVERT(datetime, App1Year.AppDate), CONVERT(datetime, p.tq_encounterdt)) BETWEEN 0 and 12

			AND App1Year.AppDate BETWEEN @StartDate AND @EndDate

		INNER JOIN PatientClinical pcDilation ON pcDilation.AppointmentId = App1Year.AppointmentId

			AND pcDilation.Status = ''A''

			AND (

					(pcDilation.ClinicalType = ''F'' AND pcDilation.Symptom IN (''/DILATION'', ''/PQRI (ARMD DILATED MACULAR EXAM)''))

					OR 

					(pcDilation.ClinicalType = ''Q'' AND pcDilation.Symptom LIKE ''%&%'')

				)

		INNER JOIN PatientClinical pcAMDSeverity ON App1Year.AppointmentId = pcAMDSeverity.AppointmentId

			AND pcAMDSeverity.Status = ''A''

			AND pcAMDSeverity.ClinicalType = ''Q''

			AND pcAMDSeverity.FindingDetail IN (''362.51.01'', ''362.51.02'', ''362.51.03'', ''362.51.11'', ''362.51.12'', ''362.51.13'', ''362.52'')

		INNER JOIN PatientClinical pcMacThick ON App1Year.AppointmentId = pcMacThick.AppointmentId

			AND pcMacThick.Status = ''A''

			AND pcMacThick.ClinicalType = ''Q''

			AND pcMacThick.FindingDetail IN (

				''361.2'',

				''362.42'',

				''362.43'',

				''362.43'',

				''362.53'',

				''362.64'',

				''362.81'',

				''362.83'',

				''362.16.11'',

				''362.41.44'',

				''362.42.1'',

				''362.42.11'',

				''362.42.12'',

				''362.42.13'',

				''362.42.2'',

				''362.42.21'',

				''362.42.57'',

				''362.42.58'',

				''362.42.59'',

				''362.50.08'',

				''362.50.09'',

				''362.50.1'',

				''362.50.11'',

				''362.50.12'',

				''362.50.13'',

				''362.50.14'',

				''362.50.15'',

				''362.50.17'',

				''362.50.18'',

				''362.50.19'',

				''362.50.2'',

				''362.50.20'',

				''362.50.21'',

				''362.50.22'',

				''362.50.22'',

				''362.50.23'',

				''362.50.23'',

				''362.50.28'',

				''362.50.3'',

				''362.50.31'',

				''362.50.33'',

				''362.50.34'',

				''362.50.4'',

				''362.50.4'',

				''362.50.5'',

				''362.50.5'',

				''362.50.63'',

				''362.50.63'',

				''362.53.1'',

				''362.81.1'',

				''362.81.2'',

				''362.81.23'',

				''362.81.24'',

				''362.81.25'',

				''362.81.25'',

				''362.81.31'',

				''362.81.4'',

				''362.81.4'',

				''362.81.44'',

				''362.81.5'',

				''362.81.5'',

				''362.81.5'',

				''362.81.50'',

				''362.81.51'',

				''362.81.54'',

				''362.81.55'',

				''362.81.56'',

				''362.81.6'',

				''362.81.7'',

				''362.81.77'',

				''362.81.8'',

				''362.83.2'',

				''362.83.23'',

				''362.83.4'',

				''362.83.5'',

				''362.83.77'',

				''P062'',

				''P237'',

				''P450'',

				''P978'',

				''P979'',

				''P980'',

				''Q008'',

				''Q009 '',

				''Q026'',

				''Q027'',

				''Q028'',

				''Q029'',

				''Q064'',

				''Q100'',

				''Q101'',

				''Q114'',

				''Q151'',

				''Q170'',

				''Q213'',

				''Q226'',

				''Q227'',

				''Q271'',

				''Q272'',

				''Q909'',

				''S743''

				)

		GROUP BY p.patient_os_id



		--14 AMD output

SELECT d.patient_os_id AS PatientId

		,d.tq_firstname AS FirstName

		,d.tq_lastname AS LastName

		,CONVERT(nvarchar(10), CONVERT(datetime, tq_encounterdt),101) AS EncounterDate

		,d.tq_physiciannpi AS PhysicianNPI

		,CONVERT(nvarchar(10), CONVERT(datetime, d.tq_dob),101) AS PatientDateOfBirth

		,d.tq_gender AS PatientGender

		,d.tq_medicare2 AS IsMedicare

		,d.tq_icd AS ICDCode

		,d.tq_em AS EM

		,CASE WHEN n.tq_dilatedmac3 IS NULL THEN ''2019F 8P''

				ELSE ''2019F''

				END AS Numerator

		FROM #AMDDenominator d

		LEFT JOIN #AMDNumerator n ON n.patient_os_id = d.patient_os_id

		GROUP BY d.patient_os_id

			,d.tq_firstname

			,d.tq_lastname

			,tq_encounterdt

			,d.tq_physiciannpi

			,d.tq_dob

			,d.tq_gender

			,d.tq_medicare2

			,d.tq_icd   

			,d.tq_em

			,tq_dilatedmac3

		ORDER BY d.patient_os_id, tq_encounterdt



	DROP TABLE #TMPResources

	DROP TABLE #AMDDenominator

	DROP TABLE #AMDNumerator



END
'

IF OBJECT_ID('USP_PQRI_GetAMD_Dilated_Macular_Examination') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)

