/***FOR DROPPING (IF EXIST) *******/

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[USP_CMS_HL7LabOrdersStats]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
       DROP PROCEDURE USP_CMS_HL7LabOrdersStats
END
GO
/**C10**** Object:  StoredProcedure [dbo].[USP_CMS_HL7LabOrdersStats]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[USP_CMS_HL7LabOrdersStats]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'


CREATE PROCEDURE [dbo].[USP_CMS_HL7LabOrdersStats]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
---#11--Clinical lab results entered as structured data > 40%--------------------------
DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN (''D'',''Q'', ''Z'', ''X'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--Denominator
select @TotParm=COUNT(DISTINCT lr.ReportId)
from HL7_LabReports LR 
inner join Resources re on re.ResourceId = lr.providerid
inner join model.Patients p on p.id=LR.PatientID
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where CONVERT(NVARCHAR,LR.ReportDate,112) between @StartDate and @EndDate 
AND p.lastName<>''TEST''

--Numerator
select @CountParm=COUNT(DISTINCT lr.ReportId)
from HL7_LabReports LR 
inner join HL7_Observation OBR on LR.ReportId = OBR.ReportId
inner join HL7_Observation_Details DET on OBR.OBRID = DET.OBRID 
inner join Resources re on re.ResourceId = lr.ProviderId
inner join model.Patients p on p.id=LR.PatientID
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where CONVERT(NVARCHAR,LR.ReportDate,112) between @StartDate and @EndDate 
AND p.lastName<>''TEST''

DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END

' 
END
GO