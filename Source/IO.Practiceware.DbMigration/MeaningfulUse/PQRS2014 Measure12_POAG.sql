--PQRS #12 POAG  [p30 of Outcome spec for formatting] distinct appointments
DECLARE @sql nvarchar(max)
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_PQRI_GetPOAGDetails]
(
@StartDate nvarchar(100),
@EndDate nvarchar(100),
@ResourceIds nvarchar(1000)
)
AS
BEGIN



	CREATE TABLE #TMPResources(ResourceId nvarchar(1000))

	IF @ResourceIds=''''

		BEGIN

	

		INSERT INTO #TMPResources 

		SELECT Resources.ResourceId 

		FROM Resources 

		WHERE ResourceType IN (''D'',''Q'')

		END

	ELSE

		BEGIN



		DECLARE @TmpResourceIds varchar(100)

		DECLARE @ResId varchar(10)

		SELECT @TmpResourceIds =@ResourceIds

		WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0

			BEGIN

				SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)

				SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1,LEN(@TmpResourceIds))    

				INSERT INTO #TMPResources values(@ResId)

			END

			INSERT INTO #TMPResources values(@TmpResourceIds)

		END



	SELECT p.PatientId AS patient_os_id

		,ap.AppointmentId AS OVAppointmentId

		,MIN(pcDx.Symptom) AS DxRank

		,p.FirstName AS tq_firstname

		,p.LastName AS tq_lastname

		,ap.AppDate AS tq_encounterdt

		,re.NPI AS tq_physiciannpi

		,p.BirthDate AS tq_dob

		,p.Gender AS tq_gender

		,''1'' AS tq_medicare2

		,ISNULL(MAX(SUBSTRING(icd10.FindingDetailIcd10, 1, 6)),MAX(SUBSTRING(pcDx.FindingDetail, 1, 6))) AS tq_icd    

		,prs.Service AS tq_em

	INTO #POAGDenominator

	FROM PatientDemographics p

	INNER JOIN Appointments ap ON p.PatientId = ap.PatientId

	INNER JOIN PatientClinical pcDx ON ap.AppointmentId = pcDx.AppointmentId

		AND pcDx.ClinicalType IN (''B'', ''K'')

		AND pcDx.Status = ''A''

	INNER JOIN PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId

	INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId

		AND pri.InsurerReferenceCode IN (''N'', ''M'', ''['')

	INNER JOIN Resources re ON re.ResourceId = pr.BillToDr

		And re.ResourceId IN (SELECT #TMPResources.ResourceId from #TMPResources)

	INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice

		AND prs.Status = ''A''
	LEFT JOIN (SELECT AppointmentId,FindingDetailIcd10 FROM PatientClinicalIcd10
				WHERE ClinicalType IN (''B'', ''K'')
				AND Status = ''A''
				AND FindingDetailIcd10 IN (''H40.10X0'',''H40.10X1'',''H40.10X2'',''H40.10X3'',''H40.10X4'',''H40.11X0'',''H40.11X1'',
											''H40.11X2'',''H40.11X3'',''H40.11X4'',''H40.1210'',''H40.1211'',''H40.1212'',''H40.1213'',''H40.1214'',''H40.1220'',''H40.1221'',''H40.1222'',
											''H40.1223'',''H40.1224'',''H40.1230'',''H40.1231'',''H40.1232'',''H40.1233'',''H40.1234'',''H40.1290'',''H40.1291'',''H40.1292'',''H40.1293'',
											''H40.1294'',''H40.151'',''H40.152'',''H40.153'',''H40.159''
											)
				)AS icd10 ON icd10.AppointmentId=ap.AppointmentId

	WHERE 

		--appointment during 2012

		ap.AppDate BETWEEN @StartDate AND @EndDate

		--over 18 years

		AND DATEDIFF (yyyy, CONVERT(datetime, BirthDate), CONVERT(datetime, ap.AppDate)) >= 18

		--diagnosis of POAG

		AND SUBSTRING(pcDx.FindingDetail, 1, 6) IN (''365.10'', ''365.11'', ''365.12'', ''365.15'')


		--with office visit

		AND prs.Service IN (''92002'', ''92004'', ''92012'', ''92014''

			, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''

			, ''99212'', ''99213'', ''99214'', ''99215''

			, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310''

			, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''

			, ''99334'', ''99335'', ''99336'', ''99337'') 

		AND p.LastName <> ''TEST''

	 GROUP BY  ap.AppointmentId

		,p.PatientId 

		,p.FirstName 

		,p.LastName 

		,ap.AppDate

		,re.NPI 

		,p.BirthDate

		,p.Gender

		,prs.Service 



	--12 POAG NUMERATOR optic nerve head evaluation

	---exam elements

	SELECT p.patient_os_id

		, 1 AS tq_opticnerve3

	INTO #POAGNumerator

	FROM #POAGDenominator p

	INNER JOIN Appointments ap ON ap.PatientId = patient_os_id

	INNER JOIN PatientClinical pcEval ON pcEval.AppointmentId = ap.AppointmentId

		AND pcEval.ClinicalType =''F''

		AND pcEval.SYMPTOM IN (''/CUP/DISC'', ''/PQRI (POAG, OPTIC NERVE EVAL)'', ''/OCT'', ''/OCT, OPTIC NERVE'', ''/OCT, RNFL'')

		AND pcEval.Status = ''A''

	WHERE DATEDIFF (mm, CONVERT(datetime, ap.AppDate), CONVERT(datetime, p.tq_encounterdt)) BETWEEN 0 and 12

		AND ap.AppDate BETWEEN @StartDate AND @EndDate

	GROUP BY p.patient_os_id



	UNION ALL



	--optic nerve findings besides Normal and Glaucoma

	SELECT p.patient_os_id

		, 1 AS tq_opticnerve3

	FROM #POAGDenominator p

	INNER JOIN Appointments ap ON ap.PatientId = patient_os_id

	INNER JOIN PatientClinical pcEval ON pcEval.AppointmentId = p.OVAppointmentId

		AND pcEval.ClinicalType = ''Q''

		AND pcEval.ImageDescriptor = ''L''

		AND pcEval.FindingDetail NOT IN (''LN'',''LX'',''YN'',''YX'', ''365.10'', ''365.11'', ''365.12'', ''365.15'', ''365.70'', ''365.71'', ''365.72'', ''365.73'', ''365.74'')

	WHERE DATEDIFF (mm, CONVERT(datetime, ap.AppDate), CONVERT(datetime, p.tq_encounterdt)) BETWEEN 0 and 12

		AND ap.AppDate BETWEEN @StartDate AND @EndDate

	GROUP BY p.patient_os_id



	--12 POAG output

	SELECT d.patient_os_id AS PatientId

		,d.tq_firstname AS FirstName

		,d.tq_lastname AS LastName

		,CONVERT(nvarchar(10), CONVERT(datetime, tq_encounterdt),101) AS EncounterDate

		,d.tq_physiciannpi AS PhysicianNPI

		,CONVERT(nvarchar(10), CONVERT(datetime, d.tq_dob),101) AS PatientDateOfBirth

		,d.tq_gender AS PatientGender

		,d.tq_medicare2 AS IsMedicare

		,d.tq_icd AS ICD_Code

		,d.tq_em AS EM

		,CASE WHEN n.tq_opticnerve3 IS NULL THEN ''2027F 8P''

			ELSE ''2027F''

			END AS Numerator

	FROM #POAGDenominator d

	LEFT JOIN #POAGNumerator n ON n.patient_os_id = d.patient_os_id

	GROUP BY d.patient_os_id

		,d.tq_firstname

		,d.tq_lastname

		,d.tq_encounterdt

		,d.tq_physiciannpi

		,d.tq_dob

		,d.tq_gender

		,d.tq_medicare2

		,d.tq_icd    

		,d.tq_em

		,n.tq_opticnerve3 

	ORDER BY d.patient_os_id, tq_encounterdt



	drop table #TMPResources

	drop table #POAGDenominator

	drop table #POAGNumerator



	END
'

IF OBJECT_ID('USP_PQRI_GetPOAGDetails') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)