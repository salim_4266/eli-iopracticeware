
--------------------------------------MEASURE #140 AMD Antioxidant Counseling  PQRS--------------------
DECLARE @sql nvarchar(max)
SET @sql = N'

CREATE PROCEDURE [dbo].[USP_PQRI_GetAMD_AREDS_Counseling]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS 

BEGIN



	CREATE TABLE #TMPResources(ResourceId nvarchar(1000))

	IF @ResourceIds=''''

		BEGIN

			INSERT INTO #TMPResources 

			SELECT Resources.ResourceId 

			FROM Resources 

			WHERE ResourceType IN(''D'',''Q'')

		END

	ELSE

		BEGIN

			DECLARE @TmpResourceIds varchar(100)

			DECLARE @ResId varchar(10)

			SELECT @TmpResourceIds =@ResourceIds

			WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0

				BEGIN

					SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)

					SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    

					INSERT INTO #TMPResources values(@ResId)

				END

				INSERT INTO #TMPResources values(@TmpResourceIds)

		END



		-----#140 AMD AREDS Counseling Denominator

		Select p.PatientId AS patient_os_id

			,ap.AppointmentId AS OVAppointmentId

			,MIN(pcDx.Symptom) AS DxRank

			,p.FirstName AS tq_firstname

			,p.LastName AS tq_lastname

			,ap.AppDate AS tq_encounterdt

			,re.NPI AS tq_physiciannpi

			,p.BirthDate AS tq_dob

			,CASE p.Gender

				WHEN ''M'' THEN ''M''

				WHEN ''F'' THEN ''F''

				ELSE ''U''

				END AS tq_gender

			,''1'' AS tq_medicare2

			,ISNULL(MAX(SUBSTRING(icd10.FindingDetailIcd10, 1, 6)),MAX(SUBSTRING(pcDx.FindingDetail, 1, 6))) AS tq_icd  

			,prs.Service AS tq_em

			,2 AS tq_areds2

		INTO #AMDAREDSDenominator

		FROM PatientDemographics p

		INNER JOIN Appointments ap ON p.PatientId = ap.PatientId

		INNER JOIN PatientClinical pcDx ON ap.AppointmentId = pcDx.AppointmentId

			AND pcDx.ClinicalType IN (''B'', ''K'')

			AND pcDx.Status = ''A''

		INNER JOIN PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId

		INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId

			AND pri.InsurerReferenceCode IN (''N'', ''M'', ''['')

		INNER JOIN Resources re ON re.ResourceId = pr.BillToDr

			AND re.ResourceId IN (SELECT #TMPResources.ResourceId from #TMPResources)

		INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice

			AND prs.Status = ''A''

		LEFT JOIN (SELECT AppointmentId,FindingDetailIcd10 FROM PatientClinicalIcd10 
				WHERE ClinicalType IN (''B'', ''K'')
				AND  Status = ''A''
				AND  FindingDetailIcd10 IN (''H35.30'',''H35.31'',''H35.32'')
			) AS icd10 ON icd10.AppointmentId=ap.AppointmentId

		WHERE 

			--appointment during 2012

			ap.AppDate BETWEEN @StartDate AND @EndDate

			--over 50 years

			AND DATEDIFF (yyyy, CONVERT(datetime, BirthDate), CONVERT(datetime, ap.AppDate)) >= 50

			--diagnosis of AMD

			AND SUBSTRING(pcDx.FindingDetail, 1, 6) IN (''362.50'', ''362.51'', ''362.52'')

			--with office visit

			AND prs.Service IN (''92002'', ''92004'', ''92012'', ''92014''

				, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''

				, ''99212'', ''99213'', ''99214'', ''99215''

				, ''99307'', ''99308'', ''99309'', ''99310''

				, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''

				, ''99334'', ''99335'', ''99336'', ''99337'') 

			AND p.LastName <> ''TEST''

		 GROUP BY  ap.AppointmentId

			,p.PatientId 

			,p.FirstName 

			,p.LastName 

			,ap.AppDate

			,re.NPI 

			,p.BirthDate

			,p.Gender

			,prs.Service 

	

		--#140 AMD AREDS NUMERATOR 

		---taking AREDS vitamins

		SELECT p.patient_os_id

			, -1 AS tq_areds2

		INTO #AMDAREDSMeds

		FROM #AMDAREDSDenominator p

		INNER JOIN Appointments apAREDS ON apAREDS.PatientId = patient_os_id

		INNER JOIN PatientClinical pcAREDSMeds ON pcAREDSMeds.AppointmentId = apAREDS.AppointmentId

		WHERE pcAREDSMeds.ClinicalType = ''A''

			AND SUBSTRING(pcAREDSMeds.FindingDetail, 1, 3) = ''RX-''

			AND pcAREDSMeds.Status = ''A''

			AND (pcAREDSMeds.FindingDetail LIKE ''%AREDS%''

				OR pcAREDSMeds.FindingDetail LIKE ''%eye vision support%''

				OR pcAREDSMeds.FindingDetail LIKE ''%EyePromise%''

				OR pcAREDSMeds.FindingDetail LIKE ''%ICaps%''

				OR pcAREDSMeds.FindingDetail LIKE ''%Lipotriad%''

				OR pcAREDSMeds.FindingDetail LIKE ''%Oculair%''

				OR pcAREDSMeds.FindingDetail LIKE ''%Ocutabs%''

				OR pcAREDSMeds.FindingDetail LIKE ''%Ocuvite%''

				OR pcAREDSMeds.FindingDetail LIKE ''%Optim-Eyes%''

				OR pcAREDSMeds.FindingDetail LIKE ''%OptiVision Forte%''

				OR pcAREDSMeds.FindingDetail LIKE ''%Macular Health Formula%''

				OR pcAREDSMeds.FindingDetail LIKE ''%MacularProtect%''

				OR pcAREDSMeds.FindingDetail LIKE ''%NutriVision%''

				OR pcAREDSMeds.FindingDetail LIKE ''%PreserVision%''

				OR pcAREDSMeds.FindingDetail LIKE ''%Savision%''

				OR pcAREDSMeds.FindingDetail LIKE ''%Stabileyes%''

				OR pcAREDSMeds.FindingDetail LIKE ''%Sure Sight%''

				OR pcAREDSMeds.FindingDetail LIKE ''%visivite%''

				OR pcAREDSMeds.FindingDetail LIKE ''%viteyes%''

				--Did not include:

				--OR pcAREDSMeds.FindingDetail LIKE ''%anti-oxidant%''

				--OR pcAREDSMeds.FindingDetail LIKE ''%carotene%''

				--OR pcAREDSMeds.FindingDetail LIKE ''%lutein%''

				--OR pcAREDSMeds.FindingDetail LIKE ''%lycopene%''

				--OR pcAREDSMeds.FindingDetail LIKE ''%zeaxanthin%''

				)

		GROUP BY p.patient_os_id



		--#140 AREDS counseling - Notes

		SELECT p.patient_os_id

			, -1 AS tq_areds2

		INTO #AMDAREDSNote

		FROM #AMDAREDSDenominator p

		INNER JOIN PatientNotes pn ON pn.PatientId = p.patient_os_id

			AND NoteType IN (''R'', ''C'')

		WHERE Note1 LIKE ''%AREDS%''

			OR Note2 LIKE ''%AREDS%''

			OR Note3 LIKE ''%AREDS%''

			OR Note4 LIKE ''%AREDS%''



		--#140 AREDS counseling - PQRI ARMD Vitamins Exam Element

		SELECT p.patient_os_id

			, -1 AS tq_areds2

		INTO #AMDAREDSExamElement

		FROM #AMDAREDSDenominator p

		INNER JOIN PatientClinical pc ON pc.PatientId = p.patient_os_id

			AND pc.ClinicalType = ''F''

			AND pc.Status = ''A''

			AND pc.Symptom = ''/PQRI (ARMD VITAMINS)''



		--#140 Numerator

		SELECT * 

		INTO #AMDAREDSNumerator

		FROM #AMDAREDSMeds

		UNION

		SELECT * FROM #AMDAREDSNote

		UNION

		SELECT * FROM #AMDAREDSExamElement



		--#140 AMD output

		SELECT d.patient_os_id AS PatientId

			,d.tq_firstname AS FirstName

			,d.tq_lastname AS LastName

			,CONVERT(nvarchar(10), CONVERT(datetime, d.tq_encounterdt),101) AS EncounterDate

			,d.tq_physiciannpi AS PhysicianNPI

			,CONVERT(nvarchar(10), CONVERT(datetime, d.tq_dob),101) AS PatientDateOfBirth

			,d.tq_gender AS PatientGender

			,d.tq_medicare2 AS IsMedicare

			,d.tq_icd AS ICDCode

			,d.tq_em AS EM

			,CASE 

				WHEN MAX(d.tq_areds2) + MAX(COALESCE(n.tq_areds2,0)) = 2 THEN ''4177F''

				ELSE ''4177 8P''

				END AS Numerator

		FROM #AMDAREDSDenominator d

		LEFT JOIN #AMDAREDSNumerator n ON n.patient_os_id = d.patient_os_id

		GROUP BY d.patient_os_id

			,d.tq_firstname

			,d.tq_lastname

			,tq_encounterdt

			,d.tq_physiciannpi

			,d.tq_dob

			,d.tq_gender

			,d.tq_medicare2

			,d.tq_icd   

			,d.tq_em

		ORDER BY d.patient_os_id, tq_encounterdt



		drop table #TMPResources

		drop table #AMDAREDSDenominator

		drop table #AMDAREDSMeds 

		drop table #AMDAREDSNote

		drop table #AMDAREDSExamElement

		drop table #AMDAREDSNumerator



	END
'

IF OBJECT_ID('USP_PQRI_GetAMD_AREDS_Counseling') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)