DECLARE @sql nvarchar(max)
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_PQRI_GetCurrentMeds]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN

	CREATE TABLE #TMPResources(ResourceId nvarchar(1000))
	IF @ResourceIds=''''
		BEGIN
			INSERT INTO #TMPResources 
			SELECT Resources.ResourceId 
			FROM Resources 
			WHERE ResourceType IN(''D'',''Q'')
		END
	ELSE
		BEGIN
			DECLARE @TmpResourceIds varchar(100)
			DECLARE @ResId varchar(10)
			SELECT @TmpResourceIds =@ResourceIds
			WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
				BEGIN
					SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
					SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    
					INSERT INTO #TMPResources values(@ResId)
				END
				INSERT INTO #TMPResources values(@TmpResourceIds)
		END

		--Initial Patient Population
		SELECT p.Id AS PatientId
			,p.FirstName
			,p.LastName
			,apov.Appdate
			,re.NPI
			,p.DateOfBirth
			,SUBSTRING(g.name,1,1) AS PatientGender
			,apOV.appointmentId as EncounterId
			,Service
		INTO #Denominator
		FROM model.Patients p
		INNER JOIN model.Gender_Enumeration g on g.id = p.genderid
		INNER JOIN Appointments apOV ON apOV.PatientId = p.id 
			AND apOV.AppTime > 0
		INNER JOIN PatientReceivables pr ON pr.appointmentid = apov.appointmentid
		INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
			AND pri.InsurerReferenceCode IN (''N'', ''M'', ''['')
		INNER JOIN Resources re ON re.ResourceId = pr.BillToDr
			AND re.ResourceId IN (SELECT #TMPResources.ResourceId from #TMPResources)
		INNER JOIN PatientReceivableServices prs on prs.AppointmentId = apOV.AppointmentId
			AND Service IN (
			--Medications Encounter Code Set
			''90791'', ''90792'', ''90832'', ''90834'', ''90837'', ''90839'', ''90957'', ''90958'', ''90959'', ''90960'', ''90962'', ''90965'', ''90966'', ''92002'', ''92004'', ''92012'', ''92014'', ''92507'', ''92508'', ''92526'', ''92541'', ''92542'', ''92543'', ''92544'', ''92545'', ''92547'', ''92548'', ''92557'', ''92567'', ''92568'', ''92570'', ''92585'', ''92588'', ''92626'', ''96116'', ''96150'', ''96152'', ''97001'', ''97002'', ''97003'', ''97004'', ''97110'', ''97140'', ''97532'', ''97802'', ''97803'', ''97804'', ''98960'', ''98961'', ''98962'', 
			''99201'', ''99202'', ''99203'', ''99204'', ''99205'', 
			''99212'', ''99213'', ''99214'', ''99215'', 
			''99221'', ''99222'', ''99223'', ''99324'', ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337'', ''99341'', ''99342'', ''99343'', ''99344'', ''99345'', ''99347'', ''99348'', ''99349'', ''99350'', ''99495'', ''99496'', ''G0101'', ''G0108'', ''G0270'', ''G0402'', ''G0438'', ''G0439'')
			AND prs.Status = ''A''
			AND CONVERT(datetime, prs.ServiceDate) BETWEEN @StartDate AND @EndDate
		WHERE DATEDIFF (yyyy, DateOfBirth, @StartDate) >= 18	
			AND p.LastName <> ''TEST''

		--Numerator
		SELECT d.PatientId
			,1 AS IsInNumerator
		INTO #Numerator
		FROM #Denominator d 
		INNER JOIN PatientClinical pc on pc.AppointmentId = d.EncounterId
			AND ClinicalType = ''F''
			AND Symptom = ''/PQRS (MEDS, DOCUMENTATION)''
			AND Status = ''A''
		
		UNION

		SELECT d.PatientId
			,1 AS IsInNumerator
		FROM #Denominator d 
		INNER JOIN PatientClinical pc on pc.AppointmentId = d.EncounterId
			AND ClinicalType = ''F''
			AND Symptom = ''/CQM EXCEPTIONS''
			and FindingDetail = ''*MEDSDOCUMENTATION-CQM=T23076 <; >''
			AND Status = ''A''

		--#192 Output
		SELECT DISTINCT
			 d.PatientId AS PatientID
			 ,FirstName
			 ,LastName
			 ,CONVERT(nvarchar(10), CONVERT(datetime, appdate),101) AS EncounterDate
			 ,NPI AS PhysicianNPI
			 ,DateofBirth AS PatientDateOfBirth
			 ,PatientGender
			 ,1 AS IsMedicare
			 ,Service as EM
			,CASE n.IsInNumerator
				WHEN 1 THEN ''G8427''
				ELSE ''G8428''
				END AS Numerator
		FROM #Denominator d
		LEFT JOIN #Numerator n ON n.Patientid = d.Patientid

		drop table #Denominator
		drop table #Numerator
		
	END
'
IF OBJECT_ID('USP_PQRI_GetCurrentMeds') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)