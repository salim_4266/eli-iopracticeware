/****** Object:  StoredProcedure [model].[USP_CMS_Stage1_SummaryOfCare]    Script Date: 9/17/2014 9:59:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [model].[USP_CMS_Stage1_SummaryOfCare] '01/01/2013', '12/31/2013', 192
DECLARE @sql NVARCHAR(max)

SET @sql = 
	N'
ALTER PROCEDURE [model].[USP_CMS_Stage1_SummaryOfCare] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN    
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

DECLARE @userId NVARCHAR(20)
DECLARE @userType NVARCHAR(20)
DECLARE @transactionid NVARCHAR(max)
DECLARE @SummaryOfCareNumerator INT

SET @userId = ''demo''
SET @userType = ''D''

--Denominator
select PatientId, e.Id as EncounterId, a.DateTime as AppointmentDate, etoco.Id as TransitionOfCareOrderId
into #Denominator
from model.EncounterTransitionOfCareOrders etoco
inner join model.encounters e on e.id = etoco.EncounterId
inner join model.Appointments a on a.EncounterId = e.id
where a.DateTime between @StartDate and DATEADD(mi,1440,@EndDate) --DATEADD so that EndDate includes the whole day of encounters
and e.EncounterStatusId = 7
and a.UserId = @ResourceIds
   
--Numerator
--Letters printed
select d.*, SentDateTime as SendDate
into #Numerator
from #Denominator d
inner join model.EncounterTransitionOfCareOrders etoco on etoco.id = d.TransitionOfCareOrderId and SentDateTime is not null

UNION ALL

--C-CDAs Printed
select d.*, CreatedDateTime as SendDate
from #Denominator d
inner join model.ExternalSystemMessagePracticeRepositoryEntities esmpre on esmpre.PracticeRepositoryEntityKey = d.PatientId and PracticeRepositoryEntityId = 3
inner join model.ExternalSystemMessages esm on esm.id = esmpre.ExternalSystemMessageId
where ExternalSystemMessageId in (select id from model.ExternalSystemMessages where ExternalSystemExternalSystemMessageTypeId in (10))

--NEWCROP C-CDAs via Direct
--SELECT TOP 1 @transactionid = transactionid
--FROM model.NewCropUtilizationReports
--WHERE userid = @userId
--AND UserType = @userType
--AND LicensedPrescriberId = @ResourceIds
--ORDER BY TIMESTAMP DESC

--exec [IO.Practiceware.SqlClr].GetMeaningfulUseUtilizationReport @transactionid,@userId, @userType
--															,NULL
--															,NULL
--															,NULL
--															,NULL
--															,NULL
--															,NULL
--															,NULL
--															,NULL
--															,NULL
--															,NULL
--															,NULL
--															,NULL
--															,NULL
--															,NULL
--															,NULL
--															,@SummaryOfCareNumerator OUTPUT

SELECT @TotParm = COUNT (DISTINCT TransitionOfCareOrderId) from #Denominator
SELECT @CountParm = COUNT(DISTINCT TransitionOfCareOrderId) FROM #Numerator

SET @CountParm = @CountParm-- + @SummaryOfCareNumerator

IF @CountParm > @TotParm
	SET @CountParm = @TotParm

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.AppointmentDate, d.TransitionOfCareOrderId
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.AppointmentDate, d.TransitionOfCareOrderId, d.SendDate
from #Numerator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

DROP TABLE #Denominator
DROP TABLE #Numerator
END
'

IF OBJECT_ID('model.USP_CMS_Stage1_SummaryOfCare') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)
