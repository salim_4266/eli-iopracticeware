IF OBJECT_ID('model.USP_CMS_Stage2_SummaryOfCareB') IS NOT NULL
	DROP PROCEDURE model.USP_CMS_Stage2_SummaryOfCareB

GO

--exec [model].[USP_CMS_Stage2_SummaryOfCareB] '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage2_SummaryOfCareB] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	DECLARE @userId NVARCHAR(20)
	DECLARE @userType NVARCHAR(20)
	DECLARE @transactionid NVARCHAR(max)
	DECLARE @SummaryOfCareNumerator INT

	SET @userId = 'demo'
	SET @userType = 'D'

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT ipp.*
		,etoco.Id AS TransitionOfCareOrderId
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
	INNER JOIN model.EncounterTransitionOfCareOrders etoco ON etoco.EncounterId = ipp.EncounterId

	--Numerator
	--C-CDAs Saved
	SELECT d.*
		,CreatedDateTime AS SendDate
		,'C-CDA Saved' AS Type
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpre ON esmpre.PracticeRepositoryEntityKey = d.PatientId and PracticeRepositoryEntityId = 3
	INNER JOIN model.ExternalSystemMessages esm ON esm.id = esmpre.ExternalSystemMessageId
	WHERE ExternalSystemExternalSystemMessageTypeId IN (
			SELECT id
			FROM model.ExternalSystemExternalSystemMessageTypes
			WHERE ExternalSystemId IN (
					SELECT Id
					FROM model.ExternalSystems
					WHERE NAME = 'Saved'
					)
				AND ExternalSystemMessageTypeId IN (
					SELECT id
					FROM model.ExternalSystemMessageTypes
					WHERE NAME = 'PatientTransitionOfCareCCDA'
						AND IsOutbound = 1
					)
			)

	SELECT @TotParm = COUNT(DISTINCT TransitionOfCareOrderId)
	FROM #Denominator

	SELECT @CountParm = COUNT(DISTINCT TransitionOfCareOrderId)
	FROM #Numerator

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
		,d.TransitionOfCareOrderId
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
		,d.TransitionOfCareOrderId
		,d.SendDate
		,Type
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END