-----------------------------MEASURE #192 Cataract Surgery Complications PQRS ---------------------------
DECLARE @sql nvarchar(max)
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_PQRI_GetCataracts_Complications]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
As
BEGIN

	CREATE TABLE #TMPResources(ResourceId nvarchar(1000))
	IF @ResourceIds=''''
		BEGIN
			INSERT INTO #TMPResources 
			SELECT Resources.ResourceId 
			FROM Resources 
			WHERE ResourceType IN(''D'',''Q'')
		END
	ELSE
		BEGIN
			DECLARE @TmpResourceIds varchar(100)
			DECLARE @ResId varchar(10)
			SELECT @TmpResourceIds =@ResourceIds
			WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
				BEGIN
					SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
					SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    
					INSERT INTO #TMPResources values(@ResId)
				END
				INSERT INTO #TMPResources values(@TmpResourceIds)
		END


		--#192 Cataract Complications Denominator
		SELECT p.PatientId AS patient_os_id
			,ap.AppointmentId AS OVAppointmentId
			,MIN(pcDx.Symptom) AS DxRank
			,p.FirstName AS tq_firstname
			,p.LastName AS tq_lastname
			,ap.AppDate AS tq_encounterdt
			,re.NPI AS tq_physiciannpi
			,p.BirthDate AS tq_dob
			,CASE p.Gender
				WHEN ''M'' THEN ''M''
				WHEN ''F'' THEN ''F''
				ELSE ''U''
				END AS tq_gender
			,''1'' AS tq_medicare2
			,ISNULL(MAX(SUBSTRING(icd10.FindingDetailIcd10, 1, 6)),MAX(SUBSTRING(pcDx.FindingDetail, 1, 6))) AS tq_icd     
			,prs.Service AS tq_cpt
			,CASE
				WHEN prs.Modifier LIKE ''%LT%'' THEN ''LT'' 
				WHEN prs.Modifier LIKE ''%RT%'' THEN ''RT'' 
				END AS Modifier
		INTO #AllCataracts
		FROM PatientDemographics p
		INNER JOIN Appointments ap ON p.PatientId = ap.PatientId
		INNER JOIN PatientClinical pcDx ON ap.AppointmentId = pcDx.AppointmentId
			AND pcDx.ClinicalType IN (''B'', ''K'')
			AND pcDx.Status = ''A''
		INNER JOIN PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId
		INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
			AND pri.InsurerReferenceCode IN (''N'', ''M'', ''['')
		INNER JOIN Resources re ON re.ResourceId = pr.BillToDr
			AND re.ResourceId IN (SELECT #TMPResources.ResourceId from #TMPResources)
		INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice
			AND prs.Status = ''A''
			AND (prs.Modifier LIKE ''%LT%'' OR prs.Modifier LIKE ''%RT%'')
		LEFT JOIN (SELECT AppointmentId,FindingDetailIcd10 FROM PatientClinicalIcd10 
					WHERE ClinicalType IN (''B'',''K'') AND Status = ''A''
				) AS icd10 ON icd10.AppointmentId=ap.AppointmentId
		WHERE 
			--surgery during the reporting year
			ap.AppDate BETWEEN @StartDate AND @EndDate
			--over 17 years
			AND DATEDIFF (yyyy, CONVERT(datetime, BirthDate), CONVERT(datetime, ap.AppDate)) >= 18
			--with surgery
			AND prs.Service IN (''66840'', ''66850'', ''66852'', ''66920'', ''66930'', ''66940'', ''66982'', ''66983'', ''66984'')  
				AND prs.Status=''A''
				AND prs.Modifier NOT LIKE ''%56%''
				AND prs.Modifier NOT LIKE ''%55%''
			AND p.LastName <> ''TEST''
		 GROUP BY  ap.AppointmentId
			,p.PatientId 
			,p.FirstName 
			,p.LastName 
			,ap.AppDate
			,re.NPI 
			,p.BirthDate
			,p.Gender
			,prs.Service 
			,prs.Modifier 

		--Comorbid patients
		SELECT DISTINCT pcComorbid.PatientId AS SickPatientId
		INTO #ComorbidPatients
		FROM PatientClinical pcComorbid
		INNER JOIN Appointments apComorbid ON pcComorbid.AppointmentId = apComorbid.AppointmentId
		INNER JOIN PatientReceivables pr ON pr.AppointmentId = apComorbid.AppointmentId
		INNER JOIN PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		INNER JOIN #AllCataracts AllCats ON AllCats.Patient_os_id = apComorbid.PatientId
			AND apComorbid.AppDate < AllCats.tq_encounterdt
			AND pcComorbid.ClinicalType IN (''Q'', ''B'', ''K'')
			AND pcComorbid.Status = ''A''
			AND (SUBSTRING(pcComorbid.FindingDetail,1,6) IN (
				''364.00'', ''364.01'', ''364.02'', ''364.03'', ''364.04'', ''364.05'',
				''364.70'', ''364.71'', ''364.72'', ''364.73'', ''364.74'', ''364.75'',''364.76'',''364.77'',''364.81'',''364.82'',''364.89'',
				''379.42'',
				''379.32'', ''379.33'',	''379.34'',
				''366.32'', ''366.33'', 
				''743.30'',
				''743.31'',
				''364.21'', ''364.22'', ''364.23'', ''364.24'', 
				''364.10'', ''364.11'', 
				''371.01'', ''371.02'', ''371.03'', ''371.04'',
				''371.00'', --''371.03'',''371.04'',
				''371.20'', ''371.21'', ''371.22'', ''371.23'', ''371.43'', ''371.44'',
				''364.60'', ''364.61'', ''364.62'', ''364.63'', ''364.64'',
				''376.50'', ''376.51'', ''376.52'',
				''365.10'', ''365.11'', ''365.12'', ''365.13'', ''365.14'', ''365.15'',
				''365.20'', ''365.21'', ''365.22'', ''365.23'', ''365.24'', ''365.31'', 
				''365.32'', ''365.51'', ''365.52'', ''365.59'', ''365.60'', ''365.61'',
				''365.62'', ''365.63'', ''365.64'', ''365.65'', ''365.81'', ''365.82'', 
				''365.83'', ''365.89'', 
				''371.50'', ''371.51'', ''371.52'', ''371.53'', ''371.54'', ''371.55'', ''371.56'', ''371.57'', ''371.58'',
				''360.21'', 
				''360.30'', ''360.31'', ''360.32'', ''360.33'', ''360.34'',
				''370.03'',
				''360.20'', ''360.21'', 
				''743.36'',
				''365.52'',
				''362.21'', 
				''366.11'',
				''366.21'', ''366.22'', ''366.23'', ''366.20'', 
				''360.11'', ''360.12'', 
				''364.42'')
			OR SUBSTRING(pcComorbid.FindingDetail, 1, 5) IN (
				''940.0'', ''940.1'', ''940.2'', ''940.3'', ''940.4'', ''940.5'', ''940.9'',
				''366.9'',
				''364.3'', 
				''367.0'',
				''950.0'', ''950.1'', ''950.2'', ''950.3'', ''950.9'',
				''871.0'', ''871.1'', ''871.2'', ''871.3'', ''871.4'', ''871.5'', ''871.6'',''871.7'',''871.9'',''921.3'')
			)
		UNION
		--Comorbid patients for ICD-10
		SELECT DISTINCT pcComorbid.PatientId AS SickPatientId
		FROM PatientClinicalIcd10 pcComorbid
		INNER JOIN Appointments apComorbid ON pcComorbid.AppointmentId = apComorbid.AppointmentId
		INNER JOIN PatientReceivables pr ON pr.AppointmentId = apComorbid.AppointmentId
		INNER JOIN PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		INNER JOIN #AllCataracts AllCats ON AllCats.Patient_os_id = apComorbid.PatientId
			AND apComorbid.AppDate < AllCats.tq_encounterdt
			AND pcComorbid.ClinicalType IN (''B'', ''K'')
			AND pcComorbid.Status = ''A''
			AND pcComorbid.FindingDetailIcd10  IN (
			''H20.00'',''H20.011'',''H20.012'',''H20.013'',''H20.019'',''H20.021'',''H20.022'',''H20.023'',''H20.029'',''H20.031'',''H20.032'',''H20.033'',''H20.039'',''H20.041'',''H20.042'',''H20.043'',''H20.049'',''H20.051'',''H20.052'',''H20.053'',''H20.059''
			,''H21.40'',''H21.41'',''H21.42'',''H21.43'',''H21.501'',''H21.502'',''H21.503'',''H21.509'',''H21.511'',''H21.512'',''H21.513'',''H21.519'',''H21.521'',''H21.522'',''H21.523'',''H21.529'',''H21.531'',''H21.532'',''H21.533'',''H21.539'',''H21.541'',''H21.542'',''H21.543'',''H21.549'',''H21.551''
			,''H21.552'',''H21.553'',''H21.559'',''H21.561'',''H21.562'',''H21.563'',''H21.569'',''H21.81'',''H21.82'',''H21.89''
			,''H57.03''
			,''H27.10'',''H27.111'',''H27.112'',''H27.113'',''H27.119'',''H27.121'',''H27.122'',''H27.123'',''H27.129'',''H27.131'',''H27.132'',''H27.133'',''H27.139''
			,''T26.00XA'',''T26.01XA'',''T26.02XA'',''T26.10XA'',''T26.11XA'',''T26.12XA'',''T26.20XA'',''T26.21XA'',''T26.22XA'',''T26.30XA'',''T26.31XA'',''T26.32XA'',''T26.40XA'',''T26.41XA'',''T26.42XA'',''T26.50XA'',''T26.51XA'',''T26.52XA'',''T26.60XA'',''T26.61XA'',''T26.62XA'',''T26.70XA'',''T26.71XA''
			,''T26.72XA'',''T26.80XA'',''T26.81XA'',''T26.82XA'',''T26.90XA'',''T26.91XA'',''T26.92XA''
			,''H26.211'',''H26.212'',''H26.213'',''H26.219'',''H26.221'',''H26.222'',''H26.223'',''H26.229''
			,''Q12.0''
			,''H26.9''
			,''Q12.0''
			,''H16.011'', ''H16.012'', ''H16.013'', ''H16.019''
			,''H20.20'',''H20.21'',''H20.22'',''H20.23'',''H20.811'',''H20.812'',''H20.813'',''H20.819'',''H20.821'',''H20.822'',''H20.823'',''H20.829'',''H20.9'',''H40.40X0''
			,''A18.54'',''H20.10'',''H20.11'',''H20.12'',''H20.13'',''H20.9''
			,''H17.00'',''H17.01'',''H17.02'',''H17.03'',''H17.10'',''H17.11'',''H17.12'',''H17.13'',''H17.811'',''H17.812'',''H17.813'',''H17.819'',''H17.821'',''H17.822'',''H17.823'',''H17.829''
			--,''H17.00'',''H17.01'',''H17.02'',''H17.03'',''H17.10'',''H17.11'',''H17.12'',''H17.13''
			,''H17.89'',''H17.9''
			,''H18.10'',''H18.11'',''H18.12'',''H18.13'',''H18.20'',''H18.221'',''H18.222'',''H18.223'',''H18.229'',''H18.231'',''H18.232'',''H18.233'',''H18.239'',''H18.421'',''H18.422'',''H18.423'',''H18.429'',''H18.43''
			,''H21.301'',''H21.302'',''H21.303'',''H21.309'',''H21.311'',''H21.312'',''H21.313'',''H21.319'',''H21.321'',''H21.322'',''H21.323'',''H21.329'',''H21.341'',''H21.342'',''H21.343'',''H21.349'',''H21.351'',''H21.352'',''H21.353'',''H21.359''
			,''H05.401'',''H05.402'',''H05.403'',''H05.409'',''H05.411'',''H05.412'',''H05.413'',''H05.419'',''H05.421'',''H05.422'',''H05.423'',''H05.429''
			,''H40.10X0'',''H40.10X1'',''H40.10X2'',''H40.10X3'',''H40.10X4'',''H40.11X0'',''H40.11X1'',''H40.11X2'',''H40.11X3'',''H40.11X4'',''H40.1210'',''H40.1211'',''H40.1212'',''H40.1213'',''H40.1214'',''H40.1220'',''H40.1221'',''H40.1222'',''H40.1223'',''H40.1224'',''H40.1230'',''H40.1231''
			,''H40.1232'',''H40.1233'',''H40.1234'',''H40.1290'',''H40.1291'',''H40.1292'',''H40.1293'',''H40.1294'',''H40.1310'',''H40.1311'',''H40.1312'',''H40.1313'',''H40.1314'',''H40.1320'',''H40.1321'',''H40.1322'',''H40.1323'',''H40.1324'',''H40.1330'',''H40.1331'',''H40.1332'',''H40.1333''
			,''H40.1334'',''H40.1390'',''H40.1391'',''H40.1392'',''H40.1393'',''H40.1394'',''H40.1410'',''H40.1411'',''H40.1412'',''H40.1413'',''H40.1414'',''H40.1420'',''H40.1421'',''H40.1422'',''H40.1423'',''H40.1424'',''H40.1430'',''H40.1431'',''H40.1432'',''H40.1433'',''H40.1434'',''H40.1490''
			,''H40.1491'',''H40.1492'',''H40.1493'',''H40.1494'',''H40.151'',''H40.152'',''H40.153'',''H40.159'',''H40.20X0'',''H40.20X1'',''H40.20X2'',''H40.20X3'',''H40.20X4'',''H40.211'',''H40.212'',''H40.213'',''H40.219'',''H40.2210'',''H40.2211'',''H40.2212'',''H40.2213'',''H40.2214'',''H40.2220''
			,''H40.2221'',''H40.2222'',''H40.2223'',''H40.2224'',''H40.2230'',''H40.2231'',''H40.2232'',''H40.2233'',''H40.2234'',''H40.2290'',''H40.2291'',''H40.2292'',''H40.2293'',''H40.2294'',''H40.231'',''H40.232'',''H40.233'',''H40.239'',''H40.241'',''H40.242'',''H40.243'',''H40.249'',''H40.30X0''
			,''H40.30X1'',''H40.30X2'',''H40.30X3'',''H40.30X4'',''H40.31X0'',''H40.31X1'',''H40.31X2'',''H40.31X3'',''H40.31X4'',''H40.32X0'',''H40.32X1'',''H40.32X2'',''H40.32X3'',''H40.32X4'',''H40.33X0'',''H40.33X1'',''H40.33X2'',''H40.33X3'',''H40.33X4'',''H40.40X0'',''H40.40X1'',''H40.40X2''
			,''H40.40X3'',''H40.40X4'',''H40.41X0'',''H40.41X1'',''H40.41X2'',''H40.41X3'',''H40.41X4'',''H40.42X0'',''H40.42X1'',''H40.42X2'',''H40.42X3'',''H40.42X4'',''H40.43X0'',''H40.43X1'',''H40.43X2'',''H40.43X3'',''H40.43X4'',''H40.50X0'',''H40.50X1'',''H40.50X2'',''H40.50X3'',''H40.50X4''
			,''H40.51X0'',''H40.51X1'',''H40.51X2'',''H40.51X3'',''H40.51X4'',''H40.52X0'',''H40.52X1'',''H40.52X2'',''H40.52X3'',''H40.52X4'',''H40.53X0'',''H40.53X1'',''H40.53X2'',''H40.53X3'',''H40.53X4'',''H40.60X0'',''H40.60X1'',''H40.60X2'',''H40.60X3'',''H40.60X4'',''H40.61X0'',''H40.61X1''
			,''H40.61X2'',''H40.61X3'',''H40.61X4'',''H40.62X0'',''H40.62X1'',''H40.62X2'',''H40.62X3'',''H40.62X4'',''H40.63X0'',''H40.63X1'',''H40.63X2'',''H40.63X3'',''H40.63X4'',''H40.811'',''H40.812'',''H40.813'',''H40.819'',''H40.821'',''H40.822'',''H40.823'',''H40.829'',''H40.831'',''H40.832''
			,''H40.833'',''H40.839'',''H40.89'',''Q15.0''
			,''H18.50'',''H18.51'',''H18.52'',''H18.53'',''H18.54'',''H18.55'',''H18.59''
			,''H52.00'',''H52.01'',''H52.02'',''H52.03''
			,''H44.40'',''H44.411'',''H44.412'',''H44.413'',''H44.419'',''H44.421'',''H44.422'',''H44.423'',''H44.429'',''H44.431'',''H44.432'',''H44.433'',''H44.439'',''H44.441'',''H44.442'',''H44.443'',''H44.449''
			,''S04.011A'',''S04.012A'',''S04.019A'',''S04.02XA'',''S04.031A'',''S04.032A'',''S04.039A'',''S04.041A'',''S04.042A'',''S04.049A''
			,''S05.10XA'',''S05.11XA'',''S05.12XA'',''S05.20XA'',''S05.21XA'',''S05.22XA'',''S05.30XA'',''S05.31XA'',''S05.32XA'',''S05.50XA'',''S05.51XA'',''S05.52XA'',''S05.60XA'',''S05.61XA'',''S05.62XA'',''S05.70XA'',''S05.71XA'',''S05.72XA'',''S05.8X1A'',''S05.8X2A'',''S05.8X9A'',''S05.90XA'',''S05.91XA'',''S05.92XA''
			,''H44.20'',''H44.21'',''H44.22'',''H44.23'',''H44.30''
			,''Q12.2'', ''Q12.4'', ''Q12.8''
			,''H40.1410'',''H40.1411'',''H40.1412'',''H40.1413'',''H40.1414'',''H40.1420'',''H40.1421'',''H40.1422'',''H40.1423'',''H40.1424'',''H40.1430'',''H40.1431'',''H40.1432'',''H40.1433'',''H40.1434'',''H40.1490'',''H40.1491'',''H40.1492'',''H40.1493'',''H40.1494''
			,''H35.171'', ''H35.172'', ''H35.173'', ''H35.179''
			,''H25.89''
			,''H26.101'',''H26.102'',''H26.103'',''H26.109'',''H26.111'',''H26.112'',''H26.113'',''H26.119'',''H26.121'',''H26.122'',''H26.123'',''H26.129'',''H26.131'',''H26.132'',''H26.133'',''H26.139''
			,''H44.111'',''H44.112'',''H44.113'',''H44.119'',''H44.131'',''H44.132'',''H44.133'',''H44.139''
			,''H21.1X1'',''H21.1X2'',''H21.1X3'',''H21.1X9''
			)

		--more comorbid - PPV history
		SELECT DISTINCT apPPV.PatientId  AS PPVPatientId
		INTO #ComorbidPPVPatients
		FROM #AllCataracts AllCats
		INNER JOIN Appointments apPPV ON apPPV.AppDate < AllCats.tq_encounterdt  
		INNER JOIN PatientReceivables prPPV ON prPPV.AppointmentId = apPPV.AppointmentId
		INNER JOIN PatientReceivableServices prsPPV ON prsPPV.Invoice = prPPV.Invoice
			AND prsPPV.Status = ''A''
			AND prsPPV.Service IN (''67036'', ''67039'', ''67040'', ''67041'', ''67042'', ''67043'')

		--#192 Flomax Patients
		SELECT ap.PatientId AS FlomaxPatientId
		INTO #FlomaxPatients
		FROM #AllCataracts AllCats
		INNER JOIN Appointments ap ON ap.PatientId = AllCats.patient_os_id
		INNER JOIN PatientClinical pc ON ap.AppointmentId = pc.AppointmentId
			AND pc.ClinicalType = ''A''
			AND SUBSTRING(pc.FindingDetail, 1, 3) = ''RX-''
			AND (pc.FindingDetail LIKE ''%Flomax%''
				or pc.FindingDetail LIKE ''%Tamsulosin%''
				or pc.FindingDetail LIKE ''%Flomaxtra %''
				or pc.FindingDetail LIKE ''%Contiflo XL%''
				or pc.FindingDetail LIKE ''%Urimax%''
				or pc.FindingDetail LIKE ''%Pradif%''
				or pc.FindingDetail LIKE ''%Secotex%''
				or pc.FindingDetail LIKE ''%Harnal D%''
				or pc.FindingDetail LIKE ''%Omnic %'')
			AND pc.Status = ''A''

		--#192 Healthy Cataract Patients
		SELECT patient_os_id
			, tq_firstname
			, tq_lastname
			, tq_encounterdt
			, tq_physiciannpi
			, tq_dob
			, tq_gender
			, tq_medicare2
			, tq_icd
			, tq_cpt
			, 2 AS tq_surgcomplications
			, CASE 
				WHEN FlomaxPatientId IS NULL THEN 2
				ELSE 1
				END AS tq_tamsulosin
			, Modifier
		INTO #CataractComplicationsDenominator
		FROM #AllCataracts AllCats
		LEFT JOIN #ComorbidPatients c ON c.SickPatientId = AllCats.patient_os_id
		LEFT JOIN #ComorbidPPVPatients p ON p.PPVPatientId = AllCats.patient_os_id
		LEFT JOIN #FlomaxPatients f ON f.FlomaxPatientId = AllCats.patient_os_id
		WHERE c.SickPatientId IS NULL
			AND p.PPVPatientId IS NULL
			AND f.FlomaxPatientId IS NULL
		GROUP BY patient_os_id, OVAppointmentId, DxRank, tq_firstname, tq_lastname, tq_encounterdt, tq_physiciannpi, tq_dob, tq_gender, tq_medicare2, tq_icd, tq_cpt, Modifier, FlomaxPatientId


		--#192 Cataract Complications Numerator
		SELECT patient_os_id
			, -1 AS tq_surgcomplications
		INTO #CataractComplicationsNumerator
		FROM #CataractComplicationsDenominator d
		INNER JOIN Appointments ap30Days ON ap30Days.PatientId = d.patient_os_id
			AND DATEDIFF(dd, d.tq_encounterdt, ap30Days.AppDate) BETWEEN 0 AND 30
			AND ap30Days.ScheduleStatus = ''D''
		INNER JOIN PatientReceivables pr ON pr.AppointmentId = ap30Days.AppointmentId
		INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice
			AND (prs.Modifier LIKE ''%RT%'' OR prs.Modifier LIKE ''%LT%'')
			AND prs.Service IN
				(''65235'', ''65800'', ''65810'', ''65815'', ''65860'', ''65880'', ''65900'',	''65920'', ''65930'', ''66030'', 
				''66250'', ''66820'', ''66825'', ''66830'', ''66852'', ''66986'', ''67005'', ''67010'', ''67015'', ''67025'', ''67028'', ''67030'', 
				''67031'', ''67036'', ''67039'', ''67041'', ''67042'', ''67043'', ''67101'', ''67105'', ''67107'', ''67108'', ''67110'', ''67112'', 
				''67141'', ''67145'', ''67250'', ''67255'')
			AND prs.Status = ''A''
		WHERE 	d.Modifier = CASE
					WHEN prs.Modifier LIKE ''%RT%'' THEN ''RT''
					WHEN prs.Modifier LIKE ''%LT%'' THEN ''LT''
					END 

		--#192 Output
		SELECT 
			d.patient_os_id AS PatientId
			, tq_firstname AS FirstName
			, tq_lastname AS LastName
			, CONVERT(nvarchar(10), CONVERT(datetime, d.tq_encounterdt),101) AS EncounterDate
			, d.tq_physiciannpi AS PhysicianNPI
			, CONVERT(nvarchar(10), CONVERT(datetime, d.tq_dob),101) AS PatientDateOfBirth
			, tq_gender AS PatientGender
			, tq_medicare2 AS IsMedicare
			, tq_icd AS ICDCode
			, tq_cpt AS CPT
			, d.tq_surgcomplications + COALESCE(n.tq_surgcomplications, 0) AS tq_surgcomplications
		INTO #Output
		FROM #CataractComplicationsDenominator d
		LEFT JOIN #CataractComplicationsNumerator n ON n.patient_os_id = d.patient_os_id

		SELECT PatientId
			, FirstName
			, LastName
			, EncounterDate
			, PhysicianNPI
			, PatientDateOfBirth
			, PatientGender
			, IsMedicare
			, ICDCode
			, CPT
			, CASE tq_surgcomplications
				WHEN 1 THEN ''G8627''
				WHEN 2 THEN ''G8628''
				END AS Numerator
		FROM #Output

		drop table #CataractComplicationsDenominator 
		drop table #CataractComplicationsNumerator
		drop table #AllCataracts
		drop table #ComorbidPatients
		drop table #ComorbidPPVPatients
		drop table #FlomaxPatients

	END
'
IF OBJECT_ID('USP_PQRI_GetCataracts_Complications') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)