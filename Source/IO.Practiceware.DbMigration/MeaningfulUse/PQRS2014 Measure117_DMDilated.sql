--------------MEASURE #117 DM DILATED EYE EXAM   PQRS ----------------------------------

DECLARE @sql nvarchar(max)
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_PQRI_Diabetes_GetMellitus_Dilated_Eye]
(
@StartDate nvarchar(100),
@EndDate nvarchar(100),
@ResourceIds nvarchar(1000)
)
AS

BEGIN



	--Filter by Doctor

	CREATE TABLE #TMPResources(ResourceId nvarchar(1000))

	IF @ResourceIds=''''

		BEGIN

			INSERT INTO #TMPResources 

			SELECT Resources.ResourceId 

			FROM Resources 

			WHERE ResourceType IN(''D'',''Q'')

		END

	ELSE

		BEGIN

			DECLARE @TmpResourceIds varchar(100)

			DECLARE @ResId varchar(10)

			SELECT @TmpResourceIds =@ResourceIds

			WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0

				BEGIN

					SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)

					SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    

					INSERT INTO #TMPResources values(@ResId)

				END

				INSERT INTO #TMPResources values(@TmpResourceIds)

		END





	--#117 DM Denominator

	SELECT p.PatientId AS patient_os_id

		,ap.AppointmentId AS OVAppointmentId

		,MIN(pcDx.Symptom) AS DxRank

		,p.FirstName AS tq_firstname

		,p.LastName AS tq_lastname

		,ap.AppDate AS tq_encounterdt

		,re.NPI AS tq_physiciannpi

		,p.BirthDate AS tq_dob

		,CASE p.Gender

			WHEN ''M'' THEN ''M''

			WHEN ''F'' THEN ''F''

			ELSE ''U''

			END AS tq_gender

		,NULL AS tq_medicare2

		,ISNULL(MAX(SUBSTRING(icd10.FindingDetailIcd10, 1, 6)),MAX(SUBSTRING(pcDx.FindingDetail, 1, 6))) AS tq_icd  

		,prs.Service AS tq_em

		,0 AS tq_dilate2

	INTO #DMDenominator

	FROM PatientDemographics p

	INNER JOIN Appointments ap ON p.PatientId = ap.PatientId

	INNER JOIN PatientClinical pcDx ON ap.AppointmentId = pcDx.AppointmentId

		AND pcDx.ClinicalType IN (''B'', ''K'')

		AND pcDx.Status = ''A''

	INNER JOIN PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId

	INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId

		AND pri.InsurerReferenceCode IN (''N'', ''M'', ''['')

	INNER JOIN Resources re ON re.ResourceId = pr.BillToDr

		AND re.ResourceId IN (SELECT #TMPResources.ResourceId from #TMPResources)

	INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice

		AND prs.Status = ''A''

	LEFT JOIN (SELECT AppointmentId,FindingDetailIcd10 FROM PatientClinicalIcd10  
				WHERE ClinicalType IN (''B'', ''K'')
				AND Status = ''A''
				AND FindingDetailIcd10 IN (''E10.10'',''E10.11'',''E10.21'',''E10.22'',''E10.29'',''E10.311'',''E10.319'',
			''E10.321'',''E10.329'',''E10.331'',''E10.339'',''E10.341'',''E10.349'',''E10.351'',''E10.359'',''E10.36'',''E10.39'',''E10.40'',''E10.41'',''E10.42'',''E10.43'',''E10.44'',''E10.49'',
			''E10.51'',''E10.52'',''E10.59'',''E10.610'',''E10.618'',''E10.620'',''E10.621'',''E10.622'',''E10.628'',''E10.630'',''E10.638'',''E10.641'',''E10.649'',''E10.65'',''E10.69'',''E10.8'',
			''E10.9'',''E11.00'',''E11.01'',''E11.21'',''E11.22'',''E11.29'',''E11.311'',''E11.319'',''E11.321'',''E11.329'',''E11.331'',''E11.339'',''E11.341'',''E11.349'',''E11.351'',''E11.359'',
			''E11.36'',''E11.39'',''E11.40'',''E11.41'',''E11.42'',''E11.43'',''E11.44'',''E11.49'',''E11.51'',''E11.52'',''E11.59'',''E11.610'',''E11.618'',''E11.620'',''E11.621'',''E11.622'',''E11.628'',
			''E11.630'',''E11.638'',''E11.641'',''E11.649'',''E11.65'',''E11.69'',''E11.8'',''E11.9'',''O24.011'',''O24.012'',''O24.013'',''O24.019'',''O24.02'',''O24.03'',''O24.111'',''O24.112'',
			''O24.113'',''O24.119'',''O24.12'',''O24.13'')
			) AS icd10 ON icd10.AppointmentId=ap.AppointmentId

	WHERE 

		--appointment during 2012

		ap.AppDate BETWEEN @StartDate AND @EndDate

		--aged 18-75 years

		AND DATEDIFF (yyyy, CONVERT(datetime, BirthDate), CONVERT(datetime, ap.AppDate)) BETWEEN 18 AND 75

		--diagnosis of Diabetes

		AND SUBSTRING(pcDx.FindingDetail, 1, 6) IN (''250.00'', ''250.01'', ''250.02'', ''250.03'', ''250.10'', ''250.11'', 

			''250.12'', ''250.13'', ''250.20'', ''250.21'', ''250.22'', ''250.23'', ''250.30'', ''250.31'', ''250.32'', ''250.33'',

			 ''250.40'', ''250.41'', ''250.42'', ''250.43'', ''250.50'', ''250.51'', ''250.52'', ''250.53'', ''250.60'', ''250.61'',

			  ''250.62'', ''250.63'', ''250.70'', ''250.71'', ''250.72'', ''250.73'', ''250.80'', ''250.81'', ''250.82'', ''250.83'',

			   ''250.90'', ''250.91'', ''250.92'', ''250.93'', ''357.2'', ''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'',

				''362.06'', ''362.07'', ''366.41'', ''648.00'', ''648.01'', ''648.02'', ''648.03'', ''648.04'')

		--with office visit

		AND prs.Service IN (''92002'', ''92004'', ''92012'', ''92014'', 

		''99201'', ''99202'', ''99203'', ''99204'', ''99205'', 

		''99211'', ''99212'', ''99213'', ''99214'', ''99215'', 

		''99217'', ''99218'', ''99219'', ''99220'', ''99221'', ''99222'', ''99223'', ''99231'', ''99232'', ''99233'', ''99238'', ''99239'', ''99281'', ''99282'', ''99283'', ''99284'', ''99285'', ''99291'', ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310'', ''99315'', ''99316'', ''99318'', 
		''99324'', ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337'', ''99341'', ''99342'', ''99343'', ''99344'', ''99345'', ''99347'', ''99348'', ''99349'', ''99350'', ''99455'', ''99456'', ''G0402'', ''G0438'', ''G0439'') 

		AND p.LastName <> ''TEST''

	 GROUP BY  ap.AppointmentId

		,p.PatientId 

		,p.FirstName 

		,p.LastName 

		,ap.AppDate

		,re.NPI 

		,p.BirthDate

		,p.Gender

		,prs.Service 





	--#117 DM Numerator

	---Dilated exam

	SELECT p.patient_os_id AS patient_os_id

		, 10000 AS tq_dilate2

	INTO #DMNumeratorDilate

	FROM #DMDenominator p

	INNER JOIN Appointments App1Year ON App1Year.PatientId = p.patient_os_id

		AND DATEDIFF (mm, CONVERT(datetime, App1Year.AppDate), CONVERT(datetime, p.tq_encounterdt)) BETWEEN 0 and 12

		AND App1Year.AppDate BETWEEN @StartDate AND @EndDate

	--Dilated Exam

	INNER JOIN PatientClinical pcDilation ON pcDilation.AppointmentId = App1Year.AppointmentId

		AND pcDilation.Status = ''A''

		AND (

				(pcDilation.ClinicalType = ''F'' AND pcDilation.Symptom IN (''/DILATION'', ''/PQRI (DM RETINOPATHY DOCUMENT)''))

				OR 

				(pcDilation.ClinicalType = ''Q'' AND pcDilation.Symptom LIKE ''%&%'')

			)

	--Exam documented by adding findings

	INNER JOIN PatientClinical pcFindings ON pcFindings.AppointmentId = pcDilation.AppointmentId

		AND pcFindings.ClinicalType = ''Q''

		AND pcFindings.ImageDescriptor IN (''M'', ''N'', ''O'', ''P'')

		AND pcFindings.Status = ''A''

	--Exam interpreted by adding impressions

	INNER JOIN PatientClinical pcImpression ON pcImpression.AppointmentId = pcDilation.AppointmentId

		AND pcImpression.ClinicalType = ''U''

		AND pcImpression.Status = ''A''

	GROUP BY patient_os_id





	---Fundus Photos Performed

	SELECT p.patient_os_id AS patient_os_id

		, 1000 AS tq_dilate2

	INTO #DMNumeratorFundusPhotos

	FROM #DMDenominator p

	INNER JOIN Appointments App1Year ON App1Year.PatientId = p.patient_os_id

		AND DATEDIFF (mm, CONVERT(datetime, App1Year.AppDate), CONVERT(datetime, p.tq_encounterdt)) BETWEEN 0 and 12

		AND App1Year.AppDate BETWEEN @StartDate AND @EndDate

	--Fundus photos documented

	INNER JOIN PatientClinical pcFundusPhotos ON pcFundusPhotos.AppointmentId = App1Year.AppointmentId

		AND pcFundusPhotos.ClinicalType = ''F''

		AND pcFundusPhotos.Status = ''A''

		AND pcFundusPhotos.Symptom IN (''/AUTOFLUORESCENCE'', ''/FUNDUS PHOTO'', ''/FUNDUS PHOTO (FP)'', ''/FUNDUS PHOTO (RETINA)'', ''/INFRARED'', ''/RED FREE'', ''/STEREO  PHOTO'')

	GROUP BY patient_os_id

	

	---Other Imaging Performed

	SELECT p.patient_os_id

		, 100 AS tq_dilate2

	INTO #DMNumeratorOtherImaging

	FROM #DMDenominator p

	INNER JOIN Appointments App1Year ON App1Year.PatientId = p.patient_os_id

		AND DATEDIFF (mm, CONVERT(datetime, App1Year.AppDate), CONVERT(datetime, p.tq_encounterdt)) BETWEEN 0 and 12

		AND App1Year.AppDate BETWEEN @StartDate AND @EndDate

	--OtherImaging documented

	INNER JOIN PatientClinical pcOtherImaging ON pcOtherImaging.AppointmentId = App1Year.AppointmentId

		AND pcOtherImaging.ClinicalType = ''F''

		AND pcOtherImaging.Status = ''A''

		AND pcOtherImaging.Symptom IN 

			(''/FA'', ''/FA AND FP'', ''/FLUORESCEIN ANGIO'',  ''/FLUORESCEIN ANGIOGRAPHY'',

			''/GDX'', ''/HRT'', ''/HRT, MACULA'', ''/ICG ANGIOGRAPHY'', 

			''/OCT'', ''/OCT, CENTRAL MACULAR THICKNESS'', ''/OCT H'', ''/OCT, MACULA'', 

			''/SCANNING LASER POLARIMETRY'')	

	GROUP BY patient_os_id


	
	---#117 Low Risk.  If DR present but no dilated exam, FP or other imaging, then Not Performed

	SELECT p.patient_os_id

		, 4 AS tq_dilate2

	INTO #DMNumeratorLowRisk

	FROM #DMDenominator p

	INNER JOIN Appointments App1Year ON App1Year.PatientId = p.patient_os_id

	----Exam in the year before the reporting period

		AND DATEDIFF (yyyy, CONVERT(datetime, App1Year.AppDate), CONVERT(datetime, p.tq_encounterdt)) = 1

	----Low Risk

	LEFT JOIN PatientClinical pcLowRisk ON pcLowRisk.AppointmentId = App1Year.AppointmentId

		AND pcLowRisk.Status = ''A''

		AND pcLowRisk.ClinicalType IN (''B'', ''K'')

		AND SUBSTRING(pcLowRisk.FindingDetail, 1, 6) IN (''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')

	LEFT JOIN #DMNumeratorDilate d ON d.patient_os_id = p.patient_os_id

	LEFT JOIN #DMNumeratorFundusPhotos f ON f.patient_os_id = p.patient_os_id

	LEFT JOIN #DMNumeratorOtherImaging i ON i.patient_os_id = p.patient_os_id

	WHERE d.patient_os_id IS NULL

		AND f.patient_os_id IS NULL

		AND i.patient_os_id IS NULL

		AND pcLowRisk.ClinicalId IS NULL

	GROUP BY  p.patient_os_id



		----#117 DM Output --285

	SELECT d.patient_os_id AS PatientId

		,tq_firstname AS FirstName

		,tq_lastname AS LastName

		,CONVERT(nvarchar(10), CONVERT(datetime, d.tq_encounterdt),101) AS EncounterDate

		,tq_physiciannpi AS PhysicianNPI

		,CONVERT(nvarchar(10), CONVERT(datetime, tq_dob),101) AS PatientDateOfBirth

		,tq_gender AS PatientGender

		,1 AS IsMedicare

		,tq_icd AS ICDCode

		,tq_em AS EM

		,CASE 

			WHEN COALESCE(d.tq_dilate2, 0)+COALESCE(f.tq_dilate2, 0)+COALESCE(i.tq_dilate2, 0)+COALESCE(l.tq_dilate2, 0) >= 10000

				THEN ''2022F''

			WHEN COALESCE(d.tq_dilate2, 0)+COALESCE(f.tq_dilate2, 0)+COALESCE(i.tq_dilate2, 0)+COALESCE(l.tq_dilate2, 0) BETWEEN 1000 AND 9999

				THEN ''2024F''

			WHEN COALESCE(d.tq_dilate2, 0)+COALESCE(f.tq_dilate2, 0)+COALESCE(i.tq_dilate2, 0)+COALESCE(l.tq_dilate2, 0) BETWEEN 100 AND 999

				THEN ''2026F''

			WHEN COALESCE(d.tq_dilate2, 0)+COALESCE(f.tq_dilate2, 0)+COALESCE(i.tq_dilate2, 0)+COALESCE(l.tq_dilate2, 0) = 4

				THEN ''3072F''

			ELSE ''2022F 8P''

			END AS Numerator

	FROM #DMDenominator d

	LEFT JOIN #DMNumeratorDilate dn ON dn.patient_os_id = d.patient_os_id

	LEFT JOIN #DMNumeratorFundusPhotos f ON f.patient_os_id = d.patient_os_id

	LEFT JOIN #DMNumeratorOtherImaging i ON i.patient_os_id = d.patient_os_id

	LEFT JOIN #DMNumeratorLowRisk l ON l.patient_os_id = d.patient_os_id

	ORDER BY d.patient_os_id, tq_encounterdt



	DROP TABLE #TMPResources

	DROP TABLE #DMDenominator

	DROP TABLE #DMNumeratorDilate

	DROP TABLE #DMNumeratorFundusPhotos

	DROP TABLE #DMNumeratorOtherImaging

	DROP TABLE #DMNumeratorLowRisk



END
'
IF OBJECT_ID('USP_PQRI_Diabetes_GetMellitus_Dilated_Eye') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)