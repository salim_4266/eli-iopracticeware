﻿--This file has ITON/CCHIT SPs and eRx SPs

--All MU and PQRS calculators moved to separate files

-----------------SPs FOR ITON/CCHIT---------------------

/****** Object:  StoredProcedure [dbo].[USP_BMI_GROWTHCHARTDETAILS]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'

CREATE PROCEDURE [dbo].[USP_BMI_GROWTHCHARTDETAILS]    
@MODE AS VARCHAR(20),    
@ID AS INTEGER,
@PatientID as INTEGER    
AS    
 BEGIN    
  IF (@MODE = ''GCDETAIL'')    
   BEGIN     
    SELECT GCD.GRAPHID,GCD.GROWTHCHARTID,ISNULL(GCD.XAXIS,''0.0'') AS XAXIS,ISNULL(GCD.Y1AXIS,''0.0'')     
    AS Y1AXIS,ISNULL(GCD.Y2AXIS,''0.0'') AS Y2AXIS,ISNULL(GCD.HCBMI,''0.0'') AS HCBMI    
    FROM TBLGROWTHCHARTDETAIL GCD WHERE GCD.GROWTHCHARTID = @ID ORDER BY GCD.XAXIS    
   END    
  ELSE IF (@MODE = ''GCMASTER'')    
   BEGIN     
    SELECT GROWTHCHARTID,CHARTID,CHARTNAME,DEVIATION FROM TBLGROWTHCHARTMASTER    
    WHERE CHARTID = @ID    
   END    
  ELSE IF (@MODE = ''PATDETAILS'')    
   BEGIN   
	select pc.PatientId,ap.AppDate,pd.birthdate,ap.AppointmentId,
	ROUND(CAST((DATEDIFF(MM,(SUBSTRING(pd.BirthDate,1,4)+''-'' + SUBSTRING(pd.BirthDate,5,2) + ''-'' + SUBSTRING(pd.BirthDate,7,2)),(SUBSTRING(ap.AppDate,1,4)+''-'' + SUBSTRING(ap.AppDate,5,2) + ''-'' + SUBSTRING(ap.AppDate,7,2)))) AS DECIMAL)/12,1) AS AGE,
	CASE WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%BMI%'' 
	THEN (REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))
	ELSE ''0'' END AS BMI,
	CASE WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%HEIGHT%'' 
	THEN 
		CASE WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%FEET%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*12)) 
		WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%METERS%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*39.370113))
		WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%CM%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*0.393700787))
		WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%INCHES%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*1))
		ELSE 0 END 
	ELSE 0 END As HEIGHT,
	CASE WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%WEIGHT%'' 
	THEN 
	CASE WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%OUNCES%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*0.0625))
		WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%POUNDS%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*1))
		WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%KGS%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*2.20462262))
		WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%GRAMS%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*0.00220462262))
		ELSE 0 END 
	ELSE 0 END AS WEIGHT,
	SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) as TEXT,
	REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'','''') as VALUE,
	CASE WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%TIME%'' THEN 0
	ELSE 1 END AS TimeCol
	,pc.FindingDetail
	from PatientClinical pc
	inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
	INNER JOIN PatientDemographics pd on pd.PatientId=pc.PatientId
	where ClinicalType in (''F'') 
	and Symptom = ''/HEIGHT AND WEIGHT'' 
	and pc.Status = ''A'' ANd ap.ScheduleStatus in (''D'', ''A'')
	AND pc.PatientId=@PatientID AND ap.AppDate <> ''0'' ORDER BY AGE   
   END    
 END
' 
IF OBJECT_ID('USP_BMI_GROWTHCHARTDETAILS') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)

/****** Object:  StoredProcedure [dbo].[USP_BMI_GetPatientDetails]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_BMI_GetPatientDetails]     
   @PATIENT_ID INTEGER=NULL,       
   @RACE VARCHAR(100)=NULL,    
   @Ethnicity VARCHAR(100)=NULL,    
   @MODE VARCHAR(50)=NULL,    
   @STATE VARCHAR(20)=NULL    
AS        
BEGIN        
 IF(@MODE=''PATIENTDETAILS'' )    
    SELECT ISNULL(FIRSTNAME,'''') AS FIRSTNAME,ISNULL(MiddleInitial,'''') AS MI,ISNULL(LASTNAME,'''') AS LASTNAME, ISNULL(BirthDate,'''') AS DOB,      
    ISNULL(GENDER,'''') AS GENDER,ISNULL(ADDRESS,'''') AS ADDRESS,ISNULL(CITY,'''') AS CITY,ISNULL(STATE,'''') AS STATE,      
    ISNULL(ZIP,'''') AS ZIP,ISNULL(HomePhone,'''') AS HOMEPH,ISNULL(BusinessPhone,'''') AS WORKPH,ISNULL('''','''') AS EXTERNALID,      
    ISNULL(Marital,'''') AS MARITALSTATUS,ISNULL('''','''') AS RACE,ISNULL(FirstName+ ''  '' + LastName,'''') AS PATIENTNAME,      
    ISNULL('''','''') AS ETHNICITY,ISNULL('''','''') AS NATIONALITY,ISNULL('''','''') AS RELIGION      
  ,''GENDERVALUE''= ISNULL(CASE GENDER WHEN ''M'' THEN ''MALE'' ELSE ''FEMALE'' END,'''')    
  ,''AGEYEARS''= ISNULL(CONVERT(VARCHAR(4),DATEDIFF(YEAR,BirthDate,CONVERT(NVARCHAR,GETDATE(),112))),'''')    
  ,CONVERT(VARCHAR(20),ISNULL(BirthDate,''''),101) AS PATIENTDOB    
    FROM PatientDemoGraphics WHERE PATIENTID=@PATIENT_ID      
 ELSE IF(@MODE=''AGE'')    
   SELECT ISNULL(VALUESETCODE,'''') AS VALUESETCODE,ISNULL(CODESYSTEMOID,'''') AS CODESYSTEMOID    
   FROM TBLVALUESETS where VALUESETTYPE=''age''and VALUESETNAME=''year''    
 ELSE IF(@MODE=''RACE'')    
   SELECT ISNULL(VALUESETCODE,'''') AS VALUESETCODE,ISNULL(CODESYSTEMOID,'''') AS CODESYSTEMOID    
   FROM TBLVALUESETS where VALUESETNAME like ''''+ @RACE+ ''%''    
 ELSE IF(@MODE=''ETHNICITY'')    
   SELECT ISNULL(VALUESETCODE,'''') AS VALUESETCODE,ISNULL(CODESYSTEMOID,'''') AS CODESYSTEMOID    
   FROM TBLVALUESETS where VALUESETNAME LIKE ''''+@Ethnicity+''%''    
 ELSE IF(@MODE=''STATE'')    
   SELECT ISNULL(VALUESETNAME,'''') AS VALUESETNAME,ISNULL(CODESYSTEMOID,'''') AS CODESYSTEMOID    
   FROM TBLVALUESETS where VALUESETTYPE=''State'' and VALUESETCODE=''+@State+''    
END 
' 
IF OBJECT_ID('USP_BMI_GetPatientDetails') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/****** Object:  StoredProcedure [dbo].[USP_Lab_GetScheduledProvId]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_Lab_GetScheduledProvId] (@PatientId int)    
AS    
Declare    
 @ResourceId1 int    
BEGIN    
 select top 1 @ResourceId1= isnull(ResourceId1,-1) from Appointments where Patientid = @Patientid and ResourceId1 in     
 (select ResourceId from Resources)    
 --and encounterid in (select encounterid from encounter)     
 --order by appointmentdate desc    
    
 If @ResourceId1 is NULL    
 set @ResourceId1 = -1    
    
 select @ResourceId1   
END 
'
IF OBJECT_ID('USP_Lab_GetScheduledProvId') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/****** Object:  StoredProcedure [dbo].[USP_Lab_GetProviderIds]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_Lab_GetProviderIds] (@UPIN Varchar(20))    
as     
Declare    
 @ResourceId int,    
 @Count int    
BEGIN    
    select @Count = count(ResourceId) ,@ResourceId = ResourceId from Resources where ResourceUPIN = @UPIN     
 group by ResourceId   
    If @Count =0    
  Set @ResourceId = -1    
    
    Select @ResourceId   
END
' 
IF OBJECT_ID('USP_Lab_GetProviderIds') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/****** Object:  StoredProcedure [dbo].[USp_Lab_GetPatientIds]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[USp_Lab_GetPatientIds](@ExternalID Varchar(15))    
as    
Declare     
 @Cnt int,    
 @PatId int    
BEGIN    
 select @Cnt = Count(PatientID), @PatId = PatientID from PatientDemographics where PatientId = @ExternalID     
 group by PatientId    
       
 If @cnt >1     
  Set @PatId = -2    
    If @Cnt <1    
  Set @PatId = -1      
    
   Select @PatId    
      
END
' 
IF OBJECT_ID('USp_Lab_GetPatientIds') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)



/****** Object:  StoredProcedure [dbo].[USP_Del_Immunizations]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
Create procedure [dbo].[USP_Del_Immunizations](  
	@Patientid    varchar(50),  
	@Encounterid varchar(50),  
	@Immid   varchar(50))  
as  
begin   
 delete from patient_immunizations where patientid=@Patientid and Encounterid=@Encounterid and Immid=@Immid  
end

' 
IF OBJECT_ID('USP_Del_Immunizations') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/****** Object:  StoredProcedure [dbo].[USP_Insert_Immunizations]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE procedure [dbo].[USP_Insert_Immunizations](      
 @Patientid   varchar(20),      
 @Encounterid  varchar(20),      
 @immid   varchar(10),      
 @Dategiven   Datetime,      
 @Expdate     datetime,      
 @Site        varchar(100),      
 @dosage   varchar(30),      
 @Reaction    varchar(1000),      
 @Initials    Varchar(100),      
 @ManuName    varchar(100),      
 @Dos_Units   varchar(100),      
 @Vis         varchar(50),      
 @LotNo       varchar(100),      
 @ImmName  varchar(100),  
 @Manufact_Code  varchar(10))      
as       
begin      
 if  not exists(select patientid from patient_immunizations where patientid=@Patientid and EncounterID=@Encounterid and ImmID=@immid )       
  begin      
   insert into patient_immunizations(PatientID,EncounterID,ImmID,DateGiven,LotExpDate,Site,      
   Reaction,ManuName,Dosage,Dos_Units,Vis,Initials,Lotid,ImmName,Manufacturer_Code)     
   values( @Patientid , @Encounterid , @immid , @Dategiven ,      
   @Expdate, @Site , @Reaction , @ManuName , @dosage ,@Dos_Units, @Vis,@Initials,@LotNo,@ImmName,@Manufact_Code)      
  end      
 else      
  begin      
   update Patient_immunizations set DateGiven=@Dategiven ,LotExpDate= @Expdate,Site=@Site ,      
   Reaction= @Reaction ,ManuName=@ManuName,Dosage=@dosage,Dos_Units=@Dos_Units,Vis=@Vis,Initials=@Initials,Lotid=@LotNo,ImmName=@ImmName,Manufacturer_Code=@Manufact_Code      
   where patientid=@Patientid and EncounterID=@Encounterid and ImmID=@immid       
  end      
 end   
  
' 
IF OBJECT_ID('USP_Insert_Immunizations') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/****** Object:  StoredProcedure [dbo].[USP_RulesEngine_GetRules]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_RulesEngine_GetRules]              
AS              
BEGIN       
Select IE.* from IE_Rules IE
LEFT OUTER  JOIN encounter_ie_overrides IER 
ON IE.RULEID=IER.RULEID 
WHERE CONVERT(NVARCHAR,IER.Comments)='''' 
OR CONVERT(NVARCHAR,IER.Comments) IS NULL
END  
' 
IF OBJECT_ID('USP_RulesEngine_GetRules') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/****** Object:  StoredProcedure [dbo].[USP_RulesEngine_GetRulePhrases]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_RulesEngine_GetRulePhrases]      
 -- Add the parameters for the stored procedure here      
AS      
BEGIN   
Select RuleId,Slno,FieldArea,FieldName,Operation,FieldValue,AndOrFlag from IE_RulesPhrases Order By 1,2       
end 
' 
IF OBJECT_ID('USP_RulesEngine_GetRulePhrases') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)

/****** Object:  StoredProcedure [dbo].[USP_Get_Pat_Immunizations]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_Get_Pat_Immunizations](        
@Encounterid  varchar(50))        
as        
declare        
@Sqlstr  varchar(1000)        
  begin        
 set @Sqlstr=''select dategiven Dategiven,Immid,I.IMMNAME,initials,site,dosage,        
 manuname,reaction,lotid,         
 IsNull(Convert(Varchar(10),lotexpdate,101),'''''''') ExpDate,vis,isnull(Dos_units,'''''''') Dos_Units,Manufacturer_Code,ImmCode from patient_immunizations PI,        
 IMMUNIZATIONS I where PI.EncounterID =''+ @Encounterid +''  and dategiven is not null AND IMMNO=IMMID''           
    
  exec (@Sqlstr)          
end   
' 
IF OBJECT_ID('USP_Get_Pat_Immunizations') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)

/****** Object:  StoredProcedure [dbo].[USP_PatientImmunizations]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE procedure [dbo].[USP_PatientImmunizations](    
@Patient_Id Varchar(20))    
as    
declare    
@Sqlstr varchar(2000)       
begin    
 set @Sqlstr=''select isnull(I.ImmName,'''''''') ImmName,isnull(LotId,'''''''') LotId,isnull(DateGiven,'''''''') DateGiven,isnull(Dosage,'''''''') Dosage, isnull(Dos_Units,'''''''') Dos_Units, isnull(ManuName,'''''''') ManuName,   
isnull(Manufacturer_Code,'''''''') ManuCode, isnull(Immcode,'''''''') Immcode     
from patient_immunizations p,Immunizations i where p.immid=i.immno and patientid='''''' + @Patient_Id +''''''''      
  
exec (@Sqlstr)    
end 

' 
IF OBJECT_ID('USP_PatientImmunizations') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)

/****** Object:  StoredProcedure [dbo].[USP_PatientDemographics]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'

CREATE procedure [dbo].[USP_PatientDemographics](      
@Patient_Id varchar(20))      
as      
declare      
@Sqlstr varchar(2000)         
begin      
 set @Sqlstr=''Select isnull(Patientid,'''''''') Patientid,isnull(LastName,'''''''') LastName,     
 isnull(FirstName,'''''''') FirstName, isnull(MiddleInitial,'''''''') MI,     
 isnull(BirthDate,'''''''') DOB, Gender as Gender,isnull(Address,'''''''') Address,isnull(Race,'''''''') Race,    
 isnull(City,'''''''') City,isnull(State,'''''''') State,isnull(Ethnicity,'''''''') Ethnicity,    
 isnull(zip,'''''''') zip,isnull(HomePhone,'''''''') HomePh,    
 isnull(LastName,'''''''') + '''', '''' +     
 isnull(FirstName,'''''''') + '''' ''''+ isnull(MiddleInitial,'''''''') PatientName      
 from patientdemographics where patientid ='' + @Patient_Id +''''      
 exec (@Sqlstr)      
end 
' 
IF OBJECT_ID('USP_PatientDemographics') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)

/****** Object:  StoredProcedure [dbo].[USP_RulesEngine_GetFieldParams]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_RulesEngine_GetFieldParams]      
 -- Add the parameters for the stored procedure here      
AS      
BEGIN   
Select FldArea,FldName,FldType from IE_FieldParams   
END
' 
IF OBJECT_ID('USP_RulesEngine_GetFieldParams') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/****** Object:  StoredProcedure [dbo].[usp_UpdateImgInstructions]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_UpdateImgInstructions]
(
@PrescriptionGuId uniqueidentifier,
@ClinicalId NVARCHAR(200)
)
AS
BEGIN

UPDATE PatientClinical SET ImageInstructions=@PrescriptionGuId
WHERE ClinicalId=@ClinicalId
END
' 
IF OBJECT_ID('usp_UpdateImgInstructions') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)

----------MEANINGFUL USE CALCULATORS START HERE -----------------------

--------------END MEANINGFUL USE CALCULATORS.  BEGIN E-PRESCRIBING SPS----------------


/****** Object:  StoredProcedure [dbo].[usp_EPInsUpd_PatEPrescription_RenewalRequest]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE Procedure [dbo].[usp_EPInsUpd_PatEPrescription_RenewalRequest]    
(
@PatientID as varchar(10),    
@RenewalRequestGuid varchar(100),    
@ReceivedTimestamp varchar(100),    
@LocationName varchar(100),    
@DoctorFullName varchar(100),    
@PharmacyInfo varchar(100),    
@PharmacyFullInfo varchar(100),    
@PharmacyStoreName varchar(100),    
@PatientFirstName varchar(100),    
@PatientMiddleName varchar(100),    
@PatientLastName varchar(100),    
@PatientDOB varchar(100),    
@PatientGender varchar(100),    
@DrugInfo varchar(100),    
@NumberOfRefills varchar(100),    
@ExternalLocationId varchar(100),    
@ExternalDoctorId varchar(100),    
@ExternalPatientId varchar(100),    
@ExternalPrescriptionId varchar(100),    
@Quantity varchar(100),    
@Sig varchar(100),    
@NcpdpId varchar(100),    
@Spare1 varchar(100)    
)    
As 
BEGIN   
if not exists (Select * from Patient_EPrescription_RenewalRequest Where renewalRequestId=@RenewalRequestGuid)    
BEGIN
     INSERT INTO Patient_EPrescription_RenewalRequest(renewalRequestId, PatientID,DrugName,PharmacyName,Location,DoctorName,PatientName,CreatedDate)     
     Values(@RenewalRequestGuid, @PatientID,@DrugInfo,@PharmacyStoreName,@LocationName,@DoctorFullName,@PatientLastName + '' '' + @PatientFirstName,GETDATE())      
     END
--ELSE    
--     Update Patient_EPrescription_RenewalRequest set PatientID=@PatientID,  
END     
     
' 
IF OBJECT_ID('usp_EPInsUpd_PatEPrescription_RenewalRequest') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)

/****** Object:  StoredProcedure [dbo].[usp_EPgetRenewalRequest]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
Create Procedure [dbo].[usp_EPgetRenewalRequest]  
as   
BEGIN
select renewalRequestId  from Patient_EPrescription_RenewalRequest where ProcessStatus is null or ProcessStatus<>''OK''
END
     
' 
IF OBJECT_ID('usp_EPgetRenewalRequest') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


--***************************ERX SPs***************
--18 SPs for eRx


/**1**** Object:  StoredProcedure [dbo].[usp_EPrescription_Patient_RenewalRequest]    Script Date: 12/15/2010 18:14:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE Procedure [dbo].[usp_EPrescription_Patient_RenewalRequest]  
(
@IsRenewalReq bit
)
  As  
  Declare @Str Varchar(1000)
  begin 
  SET @Str=''Select renewalRequestId,PatientID,PatientName,DrugName,PharmacyName,Location,DoctorName,Isnull(AcceptDenyStatus,'''''''') as AcceptDenyStatus,ISNULL(ProcessStatus,'''''''') as ProcessStatus from Patient_EPrescription_RenewalRequest ''
  if @IsRenewalReq <> 0 
  SET @Str= @Str + ''where ProcessStatus = ''''OK''''''
  else
  SET @Str= @Str + ''where ProcessStatus <> ''''OK'''' OR ProcessStatus is null''
 -- Print @Str 
  Exec (@Str)
    end 
'
IF OBJECT_ID('usp_EPrescription_Patient_RenewalRequest') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)



/******2 Object:  StoredProcedure [dbo].[usp_EPUpdateRenewalRequest]    Script Date: 12/01/2010 18:53:14 ******/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 DECLARE @sql NVARCHAR(MAX) 
 SET @sql = N'    
CREATE Procedure [dbo].[usp_EPUpdateRenewalRequest]    
(
@renewalRequestId varchar(100),    
@PharmacyNCPDP varchar(100),    
@ResponseCode varchar(100),    
@ResponseCodeDetail varchar(100),    
@ResponseFreeText varchar(100),    
@ResponseTimestamp varchar(100),    
@ResponseNumberOfRefills varchar(100),    
@ResponseDrugId varchar(100),    
@ResponseDrugTypeId varchar(100),    
@SentTimestamp varchar(100),    
@SentStatus varchar(100),    
@SentStatusMessage varchar(100),    
@PrescriptionGuid varchar(100)    
)    
As    
Declare @Str Varchar(1000)  
Begin     
    
 set @Str= ''Update Patient_EPrescription_RenewalRequest Set ProcessStatus='''''' + @SentStatus + '''''',''   
  if Upper(@ResponseCode) = ''D''    
  set @Str = @Str + ''AcceptDenyStatus = ''''Deny'''',''  
  else if Upper(@ResponseCode)= ''A''    
  set @Str = @Str + ''AcceptDenyStatus =''''Accept'''',''  
  else if Upper(@ResponseCode)= ''N''    
  set @Str = @Str + ''AcceptDenyStatus =''''New Rx'''',''  
      
 if @ResponseCodeDetail<>''''    
  set @Str = @Str + ''Comments='''''' + @ResponseCodeDetail + ''''  
  else     
  set @Str = @Str + ''Comments='''''' + @ResponseFreeText + ''''  
      
  set @Str = @Str + '' where renewalRequestId='' + @renewalRequestId    
End     
'
 
 IF OBJECT_ID('usp_EPUpdateRenewalRequest') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')
  
EXEC (@sql)
  



/*3***** Object:  StoredProcedure [dbo].[usp_GetEPAccountInfo]    Script Date: 12/15/2010 18:15:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_GetEPAccountInfo]
AS
BEGIN

select AccountID  as AccountID, rtrim(ltrim(replace(PracticeName,''&'',''''))) as AccountName,1 as AccountSiteID, 
rtrim(ltrim(replace(PracticeAddress,''&'',''''))) as Address1, rtrim(ltrim(replace(PracticeSuite,''&'',''''))) as Address2,
UPPER(PracticeState) as State, rtrim(ltrim(PracticeCity)) as City,replace(substring(PracticeZip,1,5),''&'','''') as Zip,
replace(PracticePhone,''&'','''') as ''Phone'',replace(PracticeFax,''&'','''') as ''Fax'',''US'' as Country 
from PracticeName 
cross join EPrescription_Credentials
where PracticeType = ''P'' and LocationReference = ''''

--select 1 as AccountID, rtrim(ltrim(PracticeName)) as AccountName,1 as AccountSiteID, 
--rtrim(ltrim(PracticeAddress)) as Address1, rtrim(ltrim(PracticeSuite)) as Address2,
--UPPER(PracticeState) as State, rtrim(ltrim(PracticeCity)) as City,substring(PracticeZip,1,5) as Zip,
--PracticePhone as ''Phone'',PracticeFax as ''Fax'',''US'' as Country from PracticeName where PracticeId=2
END
'
 IF OBJECT_ID('usp_GetEPAccountInfo') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/**4**** Object:  StoredProcedure [dbo].[usp_GetEPCredentials]    Script Date: 12/15/2010 18:16:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_GetEPCredentials]
AS
BEGIN
select Partnername,Username,password,siteid,url,''IO PRACTICEWARE'' as ProductName, ''V7.0'' as productVersion ,Update1URL from EPrescription_Credentials
END
'
 IF OBJECT_ID('usp_GetEPCredentials') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/**5**** Object:  StoredProcedure [dbo].[usp_GetEPLocationInfo]    Script Date: 12/15/2010 18:17:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_GetEPLocationInfo]
(
@Filter Nvarchar(100)
)
AS
BEGIN
select PracticeId as ''LocationID'',rtrim(ltrim(replace(PracticeName,''&'',''''))) as ''LocationName'', 
rtrim(ltrim(replace(Practiceaddress,''&'',''''))) as ''Address1'',rtrim(ltrim(replace(Practicesuite,''&'',''''))) as ''Address2'',
rtrim(ltrim(Practicecity)) as ''City'',UPPER(Practicestate) as ''State'',substring(Practicezip,1,5) as ''Zip'',
replace(Practicephone,''&'','''') as ''phone'',replace(Practicefax,''&'','''') as ''fax'',replace(Practicephone,''&'','''') as ''PrimaryContactNumber'', ''US'' as ''Country'' 
from PracticeName where PracticeId=@Filter 
END
'

 IF OBJECT_ID('usp_GetEPLocationInfo') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)



/**6**** Object:  StoredProcedure [dbo].[usp_GetEPPatientInfo]    Script Date: 12/15/2010 18:17:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_GetEPPatientInfo]
(
@Filter NVarchar(100)
)
AS
BEGIN
select patientid, rtrim(ltrim(replace(replace(LastName,''"'',''''),''&'',''''))) as Lastname,rtrim(ltrim(replace(replace(firstname,''"'',''''),''&'',''''))) as Firstname,replace(MiddleInitial,''&'','''') as MI, 
replace(NameReference,''.'','''')as Suffix, '''' as prefix, 
patientid as ''EMRNumber'',replace(SocialSecurity,''-'','''') as ''SSN'', 
replace(address,''&'','''') as ''address1'','''' as ''address2'',replace(city,''&'','''') as city,upper(state) as state,REPLACE(substring(zip,1,5),''&'','''') as Zip,''US'' as country,
replace(HomePhone,''&'','''') as ''HomePhoneNumber'',Gender, 
convert(varchar(16),BirthDate,112)as DOB,'''' as ''memo'' from patientdemographics  where patientid=@Filter
END
'

 IF OBJECT_ID('usp_GetEPPatientInfo') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/*7***** Object:  StoredProcedure [dbo].[usp_GetEPPrescriberInfo]    Script Date: 12/15/2010 18:17:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_GetEPPrescriberInfo]
(
@Filter int
)
AS
BEGIN
select ResourceId as ''LicensedPrescriberId'',
rtrim(ltrim(ResourceDEA)) as DEA,rtrim(ltrim(ResourceUPIN)) as upin,
UPPER(ResourceState) as State,rtrim(ltrim(ResourceLicence)) as License,
rtrim(ltrim(replace(replace(ResourceLastName,''"'',''''),''&'',''''))) as LastName,rtrim(ltrim(replace(replace(ResourceFirstName,''"'',''''),''&'',''''))) as FirstName,rtrim(ltrim(replace(replace(ResourceMI,''"'',''''),''&'',''''))) as MI,
'''' as prefix,rtrim(ltrim(ResourceSuffix)) as suffix,rtrim(ltrim(NPI)) as NPI ,'''' as freeformCredentials from Resources where ResourceID=@Filter
END
'
 IF OBJECT_ID('usp_GetEPPrescriberInfo') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/**8**** Object:  StoredProcedure [dbo].[usp_GetEPStaffInfo]    Script Date: 12/15/2010 18:17:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_GetEPStaffInfo]
(
@Filter NVARCHAR(100)
)
AS
BEGIN
select ResourceId as ''StaffId'',
rtrim(ltrim(ResourceLicence)) as StaffLicense,
rtrim(ltrim(replace(ResourceLastName,''"'',''''))) as LastName,rtrim(ltrim(replace(ResourceFirstName,''"'',''''))) 
as FirstName,isnull(rtrim(ltrim(replace(ResourceMI,''"'',''''))),'''') as MI,
isnull(rtrim(ltrim(ResourceSuffix)),'''') as suffix,'''' as prefix,ResourceType
from Resources where ResourceID=@Filter
END
'

 IF OBJECT_ID('usp_GetEPStaffInfo') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/***9*** Object:  StoredProcedure [dbo].[usp_GetMeds]    Script Date: 01/17/2011 18:17:59 ******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_GetMeds]
(
@ClinicalIdList NVARCHAR(200),
@PatientID NVARCHAR(200)
)
AS
SET NOCOUNT ON

CREATE TABLE #RxSegments
(
	ClinicalId int,
	SegTwo int,
	SegThree int,
	SegFour int,
	SegFive int,
	SegSix int,
	SegSeven int,
	SegNine int,
	LegacyDrug bit
)

INSERT INTO #RxSegments
SELECT pc.clinicalid
	, CHARINDEX(''-2/'', pc.FindingDetail) + 3
	, CHARINDEX(''-3/'', pc.FindingDetail) + 3
	, CHARINDEX(''-4/'', pc.FindingDetail) + 3
	, CHARINDEX(''-5/'', pc.FindingDetail) + 3
	, CHARINDEX(''-6/'', pc.FindingDetail) + 3
	, CHARINDEX(''-7/'', pc.FindingDetail) + 3
	, CHARINDEX(''9-9/'', pc.FindingDetail)
	, CASE WHEN pc.FindingDetail like ''%()%'' THEN 0 ELSE 1 END 
FROM PatientClinical pc
WHERE pc.ClinicalId in (select number from IntTable(@ClinicalIdList))
	and pc.PatientId = @PatientID
	
select pc.ClinicalId as externalId,

	CASE WHEN pc.FindingDetail like ''RX-8/%-2/%'' 
		THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex(''-2/'', pc.findingdetail) - 6),''&'',''and''), ''+'',''Plus'') 
	ELSE '''' END AS DrugName,
	CASE WHEN LEN(pc.ImageDescriptor) > 0
		THEN RIGHT(pc.ImageDescriptor, 4) + SUBSTRING(pc.ImageDescriptor, LEN(pc.ImageDescriptor) - 9, 2) 
		+ SUBSTRING(pc.ImageDescriptor, len(pc.ImageDescriptor) - 6, 2)
	ELSE '''' END as Date,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegTwo + 1, CHARINDEX('')'', pc.FindingDetail, rx.SegTwo) - rx.SegTwo - 1)
	ELSE ''''
		END as Dosage,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegTwo+1) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegTwo+1)) - CHARINDEX(''('', pc.FindingDetail, rx.SegTwo+1) - 1) 
	ELSE ''''
		END as Strength,
	CASE rx.LegacyDrug WHEN 0 THEN 
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegTwo+1))) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegTwo+1)))) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegTwo+1))) - 1)
	ELSE ''''
		END as Eye,
	CASE rx.LegacyDrug WHEN 0 THEN 
		SUBSTRING(pc.FindingDetail, rx.SegThree + 1, CHARINDEX('')'', pc.FindingDetail, rx.SegThree) - rx.SegThree - 1)
	ELSE ''''
		END as Frequency,
	CASE rx.LegacyDrug WHEN 0 THEN 
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegThree+1) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegThree+1)) - CHARINDEX(''('', pc.FindingDetail, rx.SegThree+1) - 1)
	ELSE ''''
		END as PRN,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree+1)+1)) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree)+1)+1)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree)+1)) - 1)
	ELSE ''''
		END as PO,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree)+1)+1)) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree+1)+1)+1)+1)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree+1)+1)+1)) - 1)
	ELSE ''''
		END as OTC,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegFour + 1, CHARINDEX('')'', pc.FindingDetail, rx.SegFour) - rx.SegFour - 1)
	ELSE ''''
		END as Refills,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegFour+1) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegFour+1)) - CHARINDEX(''('', pc.FindingDetail, rx.SegFour+1) - 1)
	ELSE ''''
		END as DAW,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour+1)+1)) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour)+1)+1)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour)+1)) - 1)
	ELSE ''''
		END as PillsPackage,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour)+1)+1)) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour+1)+1)+1)+1)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour+1)+1)+1)) - 1)
	ELSE ''''
		END as LastTaken,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX('')'', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)
	ELSE ''''
		END as Duration,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegFive+1) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegFive+1)) - CHARINDEX(''('', pc.FindingDetail, rx.SegFive+1) - 1)
	ELSE ''''
		END as DaysSupply,
	CONVERT(NVARCHAR(100),pc.DrawFileName) as FormulationID, 		
	CASE WHEN rx.SegNine > 0
			THEN ''Pending''
		ELSE '''' END as PrescriptionStatus,
	replace(pn1.note1+pn1.note2+pn1.note3+pn1.note4, ''&'', '','') as SigNote,
	replace(pn2.note1+pn2.note2+pn2.note3+pn2.note4, ''&'', '','') as PharmNote,
	CASE rx.LegacyDrug WHEN 1 THEN
		CASE WHEN pc.FindingDetail like ''%(PILLS-PACKAGE %''
			THEN CASE WHEN CHARINDEX('' '', pc.FindingDetail, CHARINDEX(''(PILLS-PACKAGE '',pc.FindingDetail)+15) > 0
				THEN SUBSTRING(pc.FindingDetail, CHARINDEX(''(PILLS-PACKAGE '', pc.FindingDetail)+15, CHARINDEX('' '', pc.FindingDetail, CHARINDEX(''(PILLS-PACKAGE '',pc.FindingDetail)+15)-(CHARINDEX(''(PILLS-PACKAGE '',pc.FindingDetail)+15))
				ELSE SUBSTRING(pc.FindingDetail, CHARINDEX(''(PILLS-PACKAGE '', pc.FindingDetail)+15, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''(PILLS-PACKAGE '',pc.FindingDetail)+15)-(CHARINDEX(''(PILLS-PACKAGE '',pc.FindingDetail)+15))
				END
		ELSE '''' 
		END	
	END AS dispenseNumber,
	CASE rx.LegacyDrug WHEN 1 THEN
		CASE WHEN pn1.Note1 IS NULL 
			THEN CASE WHEN pc.FindingDetail like ''rx-8%-2/(%'' THEN Substring(pc.findingdetail, CHARINDEX(''-2/'',pc.findingdetail)+4, charindex('')'', pc.findingdetail, CHARINDEX(''-2/'',pc.findingdetail)+4)-CHARINDEX(''-2/'',pc.findingdetail)-4)
			ELSE '''' END + '' '' + CASE WHEN pc.FindingDetail like ''rx-8%-2/(%(O%-3/%'' THEN Substring(pc.findingdetail, CHARINDEX('')-3/'',pc.findingdetail)-2,  2) 
			ELSE ''''	END	 + '' '' + CASE WHEN pc.FindingDetail NOT like ''rx-8%-3/-4/%''  
			THEN SUBSTRING(pc.findingdetail ,CHARINDEX(''-3/('',pc.findingdetail)+4,CHARINDEX('')'',pc.findingdetail, charindex(''-3/('',pc.findingdetail))-CHARINDEX(''-3/('',pc.findingdetail)-4 )
			ELSE '''' END	+ '' '' + CASE WHEN pc.FindingDetail like ''rx-8/%-3/(%)%(%)-4/%'' THEN SUBSTRING(pc.findingdetail, CHARINDEX(''('',pc.findingdetail, CHARINDEX(''-3/('',pc.findingdetail)+4)+1, charindex('')-4/'', pc.findingdetail)-CHARINDEX(''('',pc.findingdetail, CHARINDEX(''-3/('',pc.findingdetail)+4)-1)
		ELSE '''' END	ELSE ''AS PRESCRIBED'' END 
	ELSE '''' END AS sig, 
	CASE rx.LegacyDrug WHEN 1 THEN
		case WHEN pc.FindingDetail like ''%(PRN)%'' THEN '''' 
			 WHEN pc.FindingDetail like ''rx-8%REFILLS%'' and pc.FindingDetail not like ''rx-8%REFILLS %(%-5/%'' 
				THEN SUBSTRING	(pc.findingdetail, charindex(''-4/REFILLS '',pc.findingdetail)+11, charindex(''-5/'', pc.findingdetail)- charindex(''-4/REFILLS '',pc.findingdetail)-11) 
			 WHEN pc.FindingDetail like ''rx-8%REFILLS %(%)-5/%'' THEN SUBSTRING(pc.findingdetail, charindex(''-4/REFILLS'',pc.findingdetail)+11, CHARINDEX(''('',pc.findingdetail, charindex(''-4/REFILLS'', pc.FindingDetail))-charindex(''-4/REFILLS'',pc.findingdetail)-11) 
			 ELSE '''' END	
	ELSE '''' END AS refillCount,
	rx.LegacyDrug as LegacyDrug
	
	 ,pc.FindingDetail
	 ,pc.ImageInstructions as PrescriptionGuid
	 ,pc.ImageDescriptor
	FROM PatientClinical pc 
		INNER JOIN #RxSegments rx on pc.ClinicalId = rx.ClinicalId
		LEFT OUTER JOIN PatientNotes pn1 on pn1.AppointmentId = pc.AppointmentId 
			AND Substring(PC.FindingDetail, 6, CHARINDEX(''-2/'',pc.findingdetail)-6) = pn1.NoteSystem 
			AND pn1.NoteType = ''R''
		LEFT OUTER JOIN PatientNotes pn2 on pn2.AppointmentId = pc.AppointmentId
			AND SUBSTRING(pc.FindingDetail, 6, CHARINDEX(''-2/'', pc.findingdetail)-6) = pn2.NoteSystem
			AND pn2.NoteType = ''X''
			
WHERE pc.ClinicalId in (select number from IntTable(@ClinicalIdList))
	and pc.PatientId = @PatientID
SET NOCOUNT OFF

'

 IF OBJECT_ID('usp_GetMeds') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/*10***** Object:  StoredProcedure [dbo].[usp_GetPatientAllergyDet]    Script Date: 01/12/2011 18:19:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_GetPatientAllergyDet]
(
@PatientID nvarchar(100)
)
AS
BEGIN
	select CASE SUBSTRING(findingdetail, 1, 1)
			WHEN ''*''
				THEN CASE
					WHEN FindingDetail like ''%=%''
						THEN SUBSTRING(findingdetail, 2, CHARINDEX(''='', findingdetail) - 2)
					ELSE
						SUBSTRING(findingdetail, 2, LEN(findingdetail)-1)
					END
			ELSE CASE
					WHEN FindingDetail like ''%=%''
						THEN SUBSTRING(findingdetail, 1, CHARINDEX(''='', findingdetail) - 1)
					ELSE
						SUBSTRING(findingdetail, 1, LEN(findingdetail))
					END
			END AS AllergyName
		, DrawFileName AS AllergyId
		, CASE drawfilename
			WHEN ''''
				THEN ''''
			ELSE
				''FDB''
		END AS AllergySource
	From PatientClinical 
	Where ClinicalType in (''h'', ''c'')
		and Symptom like ''%allerg%''
		and Symptom not in ('']ALLERGY STATUS'', ''[ALLERGY REACTION'', '']ALLERGY SEVERITY'')
		and PatientId = @PatientID
		and Status = ''a''

	UNION

Select substring(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Note1 + Note2 + Note3 + Note4,'';'','',''),''+'',''PLUS''),''?'','' ''),''%'','',''),''-'',''''),''='',''Equal''),''&'', ''and''),''\'',''''),''/'',''''),''"'',''''''''),CHAR(10), '',''),CHAR(13) , ''''), 1, 70) AS AllergyName, '''' AS AllergyId, '''' AS AllergySource
	From PatientNotes 
	Where PatientId = @PatientID
		and NoteType = ''C''
		and NoteSystem = ''9'' 
END
'
 IF OBJECT_ID('usp_GetPatientAllergyDet') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/**11**** Object:  StoredProcedure [dbo].[usp_GetPatientDet]    Script Date: 12/15/2010 18:19:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_GetPatientDet]
(
@Filter NVARCHAR(100)
)
AS
BEGIN
DECLARE @SQLSTR NVARCHAR(200)
if @Filter <> ''''
BEGIN
SET @SQLSTR=''SELECT * FROM PatientDemoGraphics WHERE ''+@Filter+''''
END
ELSE
BEGIN
SET @SQLSTR=''SELECT * FROM PatientDemoGraphics''
END
EXEC (@SQLSTR)
END
'
 IF OBJECT_ID('usp_GetPatientDet') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/*12***** Object:  StoredProcedure [dbo].[usp_InsertPatientMedXMl]    Script Date: 12/15/2010 18:19:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_InsertPatientMedXMl]
(
@MedXML Text,
@PatientID int
)
As
BEGIN
DECLARE @PatID int
SELECT @PatID=COUNT(PatientID) From Patient_MedicationsXMl Where PatientID=@PatientID
IF(@PatID=0)
BEGIN
INSERT INTO 
Patient_MedicationsXMl(PatientID,GeneratedXml,LastModifiedDate) 
VALUES(@PatientID,@MedXML,GetDate())
END
ELSE
BEGIN
UPDATE Patient_MedicationsXMl 
SET 
GeneratedXml=@MedXML,
LastModifiedDate=GetDate()
WHERE PatientID=@PatientID
END
END
'
 IF OBJECT_ID('usp_InsertPatientMedXMl') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/*13***** Object:  StoredProcedure [dbo].[usp_MatchPtnDetails]    Script Date: 12/15/2010 18:20:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_MatchPtnDetails]
(
@FirstName NVARCHAR(200),
@LastName NVARCHAR(200),
@MiddleName NVARCHAR(200),
@DOB NVARCHAR(200),
@Gender NVARCHAR(200)
)
AS
BEGIN
DECLARE @Filter NVARCHAR(1000)
DECLARE @SQLSTR NVARCHAR(1000)
SET @Filter=''''
if @FirstName <> ''''
BEGIN
SET @Filter ='' FirstName=''''''+@FirstName+'''''' ''
END
if @LastName <> ''''
BEGIN
SET @Filter =@Filter+'' AND LastName=''''''+@LastName+'''''' '' 
END
if @MiddleName <> ''''
BEGIN
SET @Filter =@Filter+'' AND MiddleInitial=''''''+@MiddleName+'''''' ''
END
if @DOB <> ''''
BEGIN
SET @Filter =@Filter+'' AND BirthDate=''''''+@DOB+'''''' ''
END
if @Gender <> ''''
BEGIN
SET @Filter =@Filter+'' AND Gender=''''''+@Gender+'''''' ''
END
PRINT @Filter
IF @Filter <> ''''
BEGIN
SET @SQLSTR='' SELECT COUNT(*) FROM PatientDemoGraphics WHERE  ''+@Filter+'' ''
PRINT @SQLSTR
END
EXEC (@SQLSTR)
END
'
 IF OBJECT_ID('usp_MatchPtnDetails') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/*14***** Object:  StoredProcedure [dbo].[usp_NewCrop_InsertNewRx]    Script Date: 12/17/2010 10:14:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_NewCrop_InsertNewRx]
(
@PatientId int,
@DrugId int,
@PrescriptionDate nvarchar(10),
--Third
@DosageNumber int,
@DosageForm int,
@Strength nvarchar(8),
@StrengthUOM nvarchar (16),
@Route int, 
--Fourth
@DosageFrequency int,
--Fifth
@RefillNo int,
@DAW nchar(1),
@DispenseNo NVARCHAR(20),
@DispenseNumberQualifier nvarchar(2),
--Sixth
@DaysSupply int,

@ResourceId int,
@PrescriptionGuid uniqueidentifier
)

AS
BEGIN
DECLARE @DrugName as nvarchar(64)
DECLARE @Third as nvarchar(64)
DECLARE @Fourth as nvarchar(64)
DECLARE @Fifth as nvarchar(64)
DECLARE @Sixth as nvarchar(64)
DECLARE @AppointmentId as int
DECLARE @Symptom as int

CREATE TABLE #LeftJoin
(
	TempId int
)

Insert #LeftJoin Select 1

Select @DrugName = MED_ROUTED_DF_MED_ID_DESC from FDB.tblCompositeDrug where MEDID = @DrugId
Select @Third = CASE @DosageNumber
					WHEN ''''
						THEN ''()''
					ELSE
						''('' + isnull(pctNumber.Code, '''') + '' '' + isnull(pctForm.Code, '''') + '')''
					END
	From #LeftJoin  
		Left Join PracticeCodeTable pctNumber ON pctNumber.ReferenceType = ''DOSAGENUMBERTYPE''
			and pctNumber.AlternateCode = @DosageNumber
		Left Join PracticeCodeTable pctForm ON pctForm.ReferenceType = ''DOSAGEFORMTYPE''
			and pctForm.AlternateCode = @DosageForm
		
Select @Third = @Third + CASE @Strength
							WHEN ''''
								THEN ''()''
							ELSE
								''('' + isnull(@Strength + '' '' + @StrengthUOM, '''') + '')''
							END
		
Select @Third = @Third + CASE
							WHEN @Route in (35, 34, 5)
								THEN ''('' + isnull(Code, '''') + '')''
							ELSE
								''()''
							END
	From #LeftJoin Left Join PracticeCodeTable ON ReferenceType = ''DOSAGEROUTETYPE''
		and AlternateCode = @Route

Select @Fourth = CASE 
					WHEN @DosageFrequency > 1
						THEN ''('' + isnull(Code, '''') + '')()''
					ELSE
						''()()''
					END
	From #LeftJoin LEFT JOIN PracticeCodeTable ON ReferenceType = ''DOSAGEFREQUENCYTYPE'' and AlternateCode = @DosageFrequency

Select @Fourth = @Fourth + CASE @Route
							WHEN 2
								THEN ''('' + isnull(Code, '''') + '')()()()''
							ELSE
								''()()()()''
							END
	From #LeftJoin Left Join PracticeCodeTable ON ReferenceType = ''DOSAGEROUTETYPE''
		and AlternateCode = @Route

Select @Fifth = CASE @RefillNo
					WHEN ''''
						THEN ''()''
					ELSE
						''('' + cast(@RefillNo as nvarchar(5)) + '' Refills)''
					END
					+
					CASE @DAW
						WHEN ''Y'' 
							THEN ''(DAW)''
						ELSE
							''()''
					END
					+
					CASE @DispenseNo
						WHEN ''''
							THEN ''()''
						ELSE
							''('' + cast(@DispenseNo as nvarchar(5)) + '' '' + isnull(Code, '''') + '')''
					END
					+ ''()''
	From #LeftJoin Left Join PracticeCodeTable ON ReferenceType = ''DISPENSENUMBERQUALIFIER''
		and AlternateCode = @DispenseNumberQualifier

Select @Sixth = CASE @DaysSupply
					WHEN ''''
						THEN ''()()''
					ELSE	
						''()('' + CAST(@DaysSupply as nvarchar(4)) + '' DAYS SUPPLY)''
					END

Select @AppointmentId = AppointmentId 
From PracticeActivity 
Where PatientId = @PatientId
	and Status in (''H'', ''G'', ''D'', ''U'')
	and ActivityDate <= @PrescriptionDate
Order By ActivityDate asc

select @Symptom = cast(Symptom as int) + 1
From PatientClinical
Where AppointmentId = @AppointmentId
	and ClinicalType = ''A''
Order By Symptom asc

Insert PatientClinical([AppointmentId]
           ,[PatientId]
           ,[ClinicalType]
           ,[EyeContext]
           ,[Symptom]
           ,[FindingDetail]
           ,[ImageDescriptor]
           ,[ImageInstructions]
           ,[Status]
           ,[DrawFileName]
           ,[Highlights]
           ,[FollowUp]
           ,[PermanentCondition]
           ,[PostOpPeriod]
           ,[Surgery]
           ,[Activity]) 
Select @AppointmentId as AppointmentId
, @PatientId as PatientId
, ''A'' as ClinicalType
, '''' as EyeContext
, @Symptom as Symptom
, ''RX-8/'' + @DrugName + ''-2/'' + @Third + ''-3/'' + @Fourth + ''-4/'' + @Fifth + ''-5/'' + @Sixth + ''-6/P-7/'' as FindingDetail
, ''!'' + SUBSTRING(@PrescriptionDate, 5, 2) + ''/'' + SUBSTRING(@PrescriptionDate, 7, 2) + ''/'' + SUBSTRING(@PrescriptionDate, 1, 4) as ImageDescriptor
, @PrescriptionGuid as ImageInstructions
, ''A'' as Status
, cast(@DrugId as nvarchar(16)) as DrawFileName
, ''A'' as Highlights
, '''' as FollowUp
, '''' as PermanentCondition
, 0 as PostOpPeriod
, '''' as Surgery
, '''' as Activity

END


--RX-8/BETAGAN EYE DROPS-2/(1 DROPS)(0.5 %)(OS)-3/(BID)()()()()()-4/(3 REFILLS)()(1 BOTTLE)()-5/(- ONGOING)(90 DAYS SUPPLY)-6/P-7/

'
 IF OBJECT_ID('usp_NewCrop_InsertNewRx') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)

/*15***** Object:  StoredProcedure [dbo].[usp_NewCrop_UpdateRx]    Script Date: 12/17/2010 10:15:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE procedure [dbo].[usp_NewCrop_UpdateRx]
(
@PatientId int, 
@DrugId int, 
@PrescriptionDate nvarchar(10), 
@DispenseNo int,
@DosageNumber int,
@DosageForm nvarchar(20),
@Route nvarchar(20),
@DosageFrequency nvarchar(20),
@DAW bit,
@RefillNo int,
@ResourceId int,
@ClinicalId int,
@PrescriptionGuId uniqueidentifier
)

AS
BEGIN
DECLARE @DrugName as nvarchar(32)
DECLARE @Third as nvarchar(32)
DECLARE @Fourth as nvarchar(32)
DECLARE @Fifth as nvarchar(32)
DECLARE @Sixth as nvarchar(32)
DECLARE @AppointmentId as int
DECLARE @Symptom as int

Select @DrugName = MED_NAME from FDB.tblCompositeDrug where MEDID = @DrugId
Select @Third = CASE @DosageNumber
					WHEN ''''
						THEN ''()()()''
					ELSE
						''('' + cast(@DosageNumber as nvarchar(8)) + '' '' + @DosageForm + '')()()''
					END
					
Select @Fourth = CASE @DosageFrequency
						WHEN ''''
							THEN ''()()()()()()''
						ELSE
							''('' + @DosageFrequency + '')()()()()()''
						END

Select @Fifth = CASE @RefillNo
					WHEN 0
						THEN ''()''
					ELSE
						''('' + cast(@RefillNo as nvarchar(5)) + ''REFILLS)''
					END
					+
					CASE @DAW
						WHEN 1 
							THEN ''(DAW)''
						ELSE
							''()''
					END
					+
					CASE @DispenseNo
						WHEN 0
							THEN ''()''
						ELSE
							''('' + cast(@DispenseNo as nvarchar(5)) + '')''
					END
					+ ''()''

Select @Sixth = ''()()''

Select @AppointmentId = AppointmentId 
From PracticeActivity 
Where PatientId = @PatientId
	and Status in (''H'', ''G'', ''D'', ''U'')
	and ActivityDate <= @PrescriptionDate
Order By ActivityDate asc

Update PatientClinical
	Set FindingDetail = ''RX-8/'' + @DrugName + ''-2/'' + @Third + ''-3/'' + @Fourth + ''-4/'' + @Fifth + ''-5/'' + @Sixth + ''-6/-7/''
		, ImageDescriptor = ''!'' + Convert(nvarchar(10), getdate(), 101) + ImageDescriptor
		,ImageInstructions=@PrescriptionGuId
Where ClinicalId = @ClinicalId

END
'
 IF OBJECT_ID('usp_NewCrop_UpdateRx') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/*16***** Object:  StoredProcedure [dbo].[usp_UpdateImgDesc]    Script Date: 12/15/2010 19:06:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE procedure [dbo].[usp_UpdateImgDesc] 
(
@ClinicalId NVARCHAR(200),
@ImageDescriptor NVARCHAR(200)
)
AS
BEGIN
DECLARE @ImgDesc NVARCHAR(1000)
SELECT @ImgDesc=ImageDescriptor from PatientClinical WHERE ClinicalId=@ClinicalId
IF @ImgDesc = NULL
BEGIN
UPDATE PatientClinical 
SET ImageDescriptor=@ImageDescriptor
WHERE ClinicalId=@ClinicalId
END
ELSE
BEGIN
SET @ImgDesc =@ImageDescriptor + @ImgDesc
UPDATE PatientClinical 
SET ImageDescriptor=@ImgDesc
WHERE ClinicalId=@ClinicalId
		AND ImageDescriptor <> ''D%''
END
END
'
 IF OBJECT_ID('usp_UpdateImgDesc') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/**17**** Object:  StoredProcedure [dbo].[usp_UpdatePatientImgDet]    Script Date: 12/15/2010 18:23:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX)
 SET @sql = N'
CREATE PROCEDURE [dbo].[usp_UpdatePatientImgDet]
(
@ClinicalId NVARCHAR(200),
@PrescriptionGuId UNIQUEIDENTIFIER,
@PatientId NVARCHAR(200),
@ImageDescriptor NVARCHAR(200)
)
AS
BEGIN
DECLARE @ImgDesc NVARCHAR(500)
SELECT @ImgDesc=ImageDescriptor FROM PatientClinical WHERE PatientId=@PatientId AND ClinicalId=@ClinicalId;
--PRINT  (''''+@ImgDesc+'''')
SET @ImgDesc=REPLACE(@ImgDesc,''C''+@ImageDescriptor+'''','''')
--PRINT  (''''+@ImgDesc+'''')
SET @ImgDesc=RTRIM(LTRIM(@ImgDesc))
--PRINT  (''''+@ImgDesc+'''')
UPDATE PatientClinical 
SET ImageInstructions=@PrescriptionGuId,
ImageDescriptor=@ImgDesc
WHERE ClinicalId=@ClinicalId AND PatientId=@PatientId
END
'
 IF OBJECT_ID('usp_UpdatePatientImgDet') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)


/*18***** Object:  StoredProcedure [dbo].[usp_GetSigMapData]    Script Date: 12/16/2010 10:27:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_GetSigMapData]
(
@ReferenceType NVARCHAR(200),
@Filter NVARCHAR(200)
)
AS
BEGIN
if @ReferenceType=''DOSAGEFORMTYPE''
BEGIN
SELECT * from PracticeCodeTable where ReferenceType=@ReferenceType AND Code LIKE ''''+@Filter+''%''
END
ELSE
BEGIN
SELECT * from PracticeCodeTable where ReferenceType=@ReferenceType AND Code=@Filter
END
END
'
 IF OBJECT_ID('usp_GetSigMapData') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)



/****** Object:  StoredProcedure [dbo].[usp_InsertRenewalReqXML]    Script Date: 07/04/2011 05:05:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX) 
SET @sql = N'
CREATE PROCEDURE [dbo].[usp_InsertRenewalReqXML]
(
@RenewalReqXML TEXT,
@RenewalReqID NVARCHAR(1000)
)
AS
BEGIN
UPDATE Patient_EPrescription_RenewalRequest
SET RenewalReqXMl=@RenewalReqXML  
WHERE renewalRequestId=@RenewalReqID
END
' 
 IF OBJECT_ID('usp_InsertRenewalReqXML') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)

