/****** Object:  StoredProcedure [qrda].[QRDA_BMIPop2_CMS069v3]    Script Date: 09/03/2014 16:33:47 ******/
/***FOR DROPPING (IF EXIST) *******/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_BMIPop2_CMS069v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [qrda].[QRDA_BMIPop2_CMS069v3]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_BMIPop2_CMS069v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [qrda].[QRDA_BMIPop2_CMS069v3]
(
@StartDate datetime,
@EndDate datetime,
@UserId int
)
AS
BEGIN
	
---Denominator 2 - Patients 65 and over, 1 OV
	SELECT DISTINCT pd.Id
		, apOV.AppointmentId
		, apOv.AppDate
		, FirstName +'' ''+ LastName as Name
	INTO #InitialPopulation
	FROM model.Patients pd
	INNER JOIN dbo.Appointments apOV ON apOV.PatientId = pd.Id
		AND @UserId = apOv.ResourceId1
		AND apOV.AppTime > 0
	INNER JOIN PatientReceivableServices prs ON prs.AppointmentId = apOV.AppointmentId
		AND prs.Service in (
			--
			''92002'', ''92004'', ''92012'', ''92014''
			--office visit
			, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''
			, ''99212'', ''99213'', ''99214'', ''99215''
			--
			, ''99241'', ''99242'', ''99243'', ''99244'', ''99245''
			, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310''
			, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''
			, ''99334'', ''99335'', ''99336'', ''99337'')  
		AND prs.Status=''A''
		AND CONVERT(datetime, prs.ServiceDate) BETWEEN @StartDate AND @EndDate
	WHERE DATEDIFF (yyyy, pd.DateOfBirth, @StartDate) >= 65
		AND pd.LastName <> ''TEST''

	SELECT DISTINCT Id
		, Name
		, 0 AS IsInNumerator
		, AppDate
	INTO #Denominator
	FROM #InitialPopulation

	---Numerator - 
	SELECT d.Id
		,d.Name
		,1 AS IsInNumerator
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN dbo.PatientClinical pc ON pc.PatientId = d.Id
		AND pc.ClinicalType = ''F''
		AND pc.Status = ''A''
		AND pc.Symptom = ''/HEIGHT AND WEIGHT''
		AND SUBSTRING(pc.FindingDetail, 1, 11) = ''*BMI=T14905''
	INNER JOIN dbo.Appointments aHW ON aHW.AppointmentId = pc.AppointmentId
	WHERE CAST(SUBSTRING(pc.FindingDetail, CHARINDEX(''<'', pc.FindingDetail)+1, CHARINDEX(''>'', pc.FindingDetail)-CHARINDEX(''<'', pc.FindingDetail)-1) AS decimal) >= 22
		AND CAST(SUBSTRING(pc.FindingDetail, CHARINDEX(''<'', pc.FindingDetail)+1, CHARINDEX(''>'', pc.FindingDetail)-CHARINDEX(''<'', pc.FindingDetail)-1) AS decimal) < 30
		AND DATEDIFF (month, CONVERT(datetime, aHW.AppDate), CONVERT(datetime, d.AppDate)) <= 6

	UNION

	SELECT d.Id
		,d.Name
		,1 AS IsInNumerator
	FROM #Denominator d
	INNER JOIN dbo.PatientClinical pcHW ON pcHW.PatientId = d.Id
	INNER JOIN dbo.Appointments aHW ON aHW.AppointmentId = pcHW.AppointmentId
	INNER JOIN dbo.PatientClinical pcInt ON pcInt.AppointmentId = aHW.AppointmentId
		AND pcInt.ClinicalType = ''F''
		AND pcInt.Status = ''A''
		AND pcInt.Symptom = ''/HEIGHT AND WEIGHT''
		AND pcInt.FindingDetail = ''*COUNSELING=T23109 <COUNSELING>''

	SELECT DISTINCT
		d.Id
		,d.Name
		,CONVERT(bit, d.IsInNumerator + COALESCE(n.IsInNumerator, 0)) AS IsInNumerator
	FROM #Denominator d
	LEFT JOIN #Numerator n ON n.id = d.id

	DROP TABLE #InitialPopulation
	DROP TABLE #Denominator
	DROP TABLE #Numerator
END
'
END
GO