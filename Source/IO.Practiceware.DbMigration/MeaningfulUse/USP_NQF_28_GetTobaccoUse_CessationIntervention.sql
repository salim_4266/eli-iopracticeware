---------------USP_NQF_28_GetTobaccoUse_CessationIntervention----------------

DECLARE @sql nvarchar(max)
SET @sql = N'

CREATE PROCEDURE [dbo].[USP_NQF_28_GetTobaccoUse_CessationIntervention]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;
	SET @TotParm=0;
	SET @CountParm=0;

	CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
	IF @ResourceIds=''''
	BEGIN
		INSERT INTO #TMPResources 
		SELECT Resources.ResourceId 
		FROM Resources 
		WHERE ResourceType IN (''D'',''Q'', ''Y'', ''Z'')
	END
	ELSE
		BEGIN
			DECLARE @TmpResourceIds VARCHAR(100)
			DECLARE @ResId VARCHAR(10)
		
			SELECT @TmpResourceIds =@ResourceIds
			WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
				BEGIN
					SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
					SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    
					INSERT INTO #TMPResources values(@ResId)
				END
				INSERT INTO #TMPResources values(@TmpResourceIds)
		END
		
		---Denominator - Patients 18 and over with 2+ office visits in the reporting period
		SELECT pd.PatientId
		INTO #Denominator
		FROM PatientDemographics pd
		INNER JOIN PatientReceivables pr ON pr.PatientId = pd.PatientId
			AND pr.InvoiceDate BETWEEN @StartDate AND @EndDate
		INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice
			AND prs.Service in (
				--
				''92002'', ''92004'', ''92012'', ''92014''
				--office visit
				, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''
				, ''99212'', ''99213'', ''99214'', ''99215''
				--
				, ''99241'', ''99242'', ''99243'', ''99244'', ''99245''
				, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310''
				, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''
				, ''99334'', ''99335'', ''99336'', ''99337'')  
			AND prs.Status=''A''
		INNER JOIN Resources re on re.ResourceId = pr.BillToDr 
			AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
		INNER JOIN PatientClinical pc on pc.AppointmentId = pr.AppointmentId
		WHERE DATEDIFF (yyyy, CONVERT(datetime, pd.BirthDate), CONVERT(datetime, @StartDate)) >= 18
		AND pc.Symptom = ''/SMOKING'' and FindingDetail in (''*LIGHTSMOKE=T22569 <LIGHTSMOKE>'',''*HEAVYSMOKE=T22568 <HEAVYSMOKE>'',''*CURRENTEVERYDAY=T14854 <CURRENTEVERYDAY>'',''*CURRENTSOMEDAY=T14855 <CURRENTSOMEDAY>'')
		GROUP BY pd.PatientId
		HAVING COUNT(*) > 1


		SELECT i.PatientId
		INTO #Numerator
		FROM #Denominator i
		INNER JOIN dbo.Appointments a ON i.PatientId = a.PatientId
		INNER JOIN PatientClinical pc ON pc.AppointmentId = a.AppointmentId
			AND pc.ClinicalType = ''F'' 
			AND pc.Status=''A''
			AND pc.Symptom = ''/SMOKING'' AND pc.FindingDetail = ''*CESSATION=T22892 <CESSATION>''
		WHERE DATEDIFF (month, CONVERT(datetime, a.AppDate), CONVERT(datetime, @EndDate)) <= 24

		SELECT @TotParm=COUNT(distinct patientid)  
		FROM #Denominator

		SELECT @CountParm=COUNT(distinct patientid)
		FROM #Numerator
		
		SELECT @TotParm as Denominator, @CountParm as Numerator

	END


'

IF OBJECT_ID('USP_NQF_28_GetTobaccoUse_CessationIntervention') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)