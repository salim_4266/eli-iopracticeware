/****** Object:  StoredProcedure [model].[USP_CMS_Stage2_CPOELaboratory]    Script Date: 12/17/2013 9:56:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--exec [model].[USP_CMS_Stage2_CPOELaboratory] '12/15/2013', '03/15/2014', 192
DECLARE @sql NVARCHAR(max)

SET @sql = 
	N'


CREATE PROCEDURE [model].[USP_CMS_Stage2_CPOELaboratory]  
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   

BEGIN    
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

--Denominator
--select e.PatientId, e.id as EncounterId, elto.Id as LaboratoryTestOrderId, a.datetime as OrderDate
--into #Denominator
--from model.EncounterLaboratoryTestOrders elto
--inner join model.encounters e on e.id = elto.EncounterId
--inner join model.Appointments a on a.EncounterId = e.Id
--where a.DateTime between @StartDate and DATEADD(mi,1440,@EndDate) --DATEADD so that EndDate includes the whole day of encounters
--and e.EncounterStatusId = 7
--and a.UserId = @ResourceIds
SELECT distinct hl7.ReportID as LaboratoryTestOrderId, hl7.PatientID as PatientId, hl7.ReportDate as OrderDate
INTO #Denominator
FROM HL7_LabReports hl7
WHERE hl7.ReportDate BETWEEN @StartDate and DATEADD(mi,1440,@EndDate)
and hl7.ProviderID = @ResourceIds

UNION ALL

--This query will get as many labs as possible that aren''''t in ITON that the doctor orders that are linked to lab test exam element
select e.PatientId, edto.Id as LaboratoryTestOrderId, a.datetime as OrderDate
from model.EncounterDiagnosticTestOrders edto
inner join model.DiagnosticTestOrders dto on dto.id = edto.DiagnosticTestOrderId
inner join model.ClinicalProcedures cp on cp.Id = dto.ClinicalProcedureId and  cp.Name in (''BLOOD WORK'',''LAB RESULTS'',''LAB AND MEDICAL RESULTS'',''BLOOD COUNT'',''CULTURE OF CORNEA/CONJUNCTIVA'',''OSMOLARITY TEST'')
inner join model.encounters e on e.id = edto.EncounterId
inner join model.Appointments a on a.EncounterId = e.Id
where a.DateTime between @StartDate and DATEADD(mi,1440,@EndDate) --DATEADD so that EndDate includes the whole day of encounters
and e.EncounterStatusId = 7
and a.UserId = @ResourceIds
  
--numerator  
select d.*
into #Numerator
from #Denominator d

SET @TotParm = (SELECT COUNT (DISTINCT LaboratoryTestOrderId) from #Denominator)
SET @CountParm = (SELECT COUNT (DISTINCT LaboratoryTestOrderId) from #Numerator)

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.OrderDate, d.LaboratoryTestOrderId
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.OrderDate, d.LaboratoryTestOrderId

select n.PatientId, p.FirstName, p.LastName, n.OrderDate, n.LaboratoryTestOrderId
from #Numerator n
inner join model.patients p on p.id = n.PatientId
order by p.LastName, p.FirstName, n.PatientId, n.OrderDate, n.LaboratoryTestOrderId

DROP TABLE #Denominator
DROP TABLE #Numerator

END '

IF OBJECT_ID('model.USP_CMS_Stage2_CPOELaboratory') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)