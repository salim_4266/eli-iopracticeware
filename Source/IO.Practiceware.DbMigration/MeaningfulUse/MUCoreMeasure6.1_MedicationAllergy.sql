/****** Object:  StoredProcedure [model].[USP_CMS_Stage1_MedicationAllergyList]    Script Date: 12/17/2013 6:51:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @sql NVARCHAR(MAX)
SET @sql = '
--exec [model].[USP_CMS_Stage1_MedicationAllergyList] ''0/01/2014'', ''12/31/2014'', 10

CREATE PROCEDURE [model].[USP_CMS_Stage1_MedicationAllergyList]
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   

BEGIN    
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
  
	--Numerator  
	select DISTINCT d.PatientId, pa.Id as PatientAllergenId
	into #Numerator
	from #Denominator d
	inner join PatientAllergens pa on pa.PatientId = d.PatientId
	
	UNION ALL
	
	select DISTINCT d.PatientId, ''0'' as PatientAllergenId
	from #Denominator d
	inner join PatientClinical pc on pc.PatientId = d.PatientId AND Symptom in (''/ALLERGIES'', ''/ANY DRUG ALLERGIES'') AND FindingDetail in (''NOTSURE=T98'',''NOTSURE=T171'',''NOTSURE=T171 <NOTSURE>'',''NONE=T14908 <NONE>'',''NONE=T14908'')

SET @TotParm = (SELECT COUNT (DISTINCT PatientID) from #Denominator)
SET @CountParm = (SELECT COUNT (DISTINCT PatientID) from #Numerator)

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.EncounterDate
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.EncounterDate, d.EncounterId

select n.PatientId, p.FirstName, p.LastName, n.PatientAllergenId
from #Numerator n
inner join model.patients p on p.id = n.PatientId
order by p.LastName, p.FirstName, n.PatientId, n.PatientAllergenId

select distinct d.patientid
from #Denominator d
left join #Numerator n on d.PatientId = n.PatientId
where n.patientid is null

DROP TABLE #Denominator
DROP TABLE #Numerator

END 
'

IF OBJECT_ID('model.USP_CMS_Stage1_MedicationAllergyList') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)