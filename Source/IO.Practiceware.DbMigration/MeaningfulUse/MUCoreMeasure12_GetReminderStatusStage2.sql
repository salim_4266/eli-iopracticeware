/****** Object:  StoredProcedure [model].[USP_CMS_Stage2_PatientReminders]    Script Date: 12/17/2013 5:38:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [model].[USP_CMS_Stage2_PatientReminders] '12/15/2013', '03/15/2014', 192
DECLARE @sql NVARCHAR(MAX)
SET @sql = '
CREATE PROCEDURE [model].[USP_CMS_Stage2_PatientReminders] 
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   

BEGIN    
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

--Denominator
SELECT p.id as PatientId,ConfirmationStatusId
into #Denominator 
from model.Encounters e
inner join model.Patients p on p.id = e.PatientId
inner join model.Appointments a on a.EncounterId = e.Id and userid = @ResourceIds
where DATEADD(yy,-2,@StartDate) <= a.DateTime and a.DateTime < @StartDate
and e.EncounterStatusId = 7
group by p.Id,ConfirmationStatusId
HAVING count(e.id) >= 2

--Numerator
select d.PatientId, ptj.TransactionID as ReminderId, CONVERT(datetime, TransactionDate) as ReminderSentDate
into #Numerator
from #Denominator d
inner join PracticeTransactionJournal PTJ on ptj.TransactionTypeId = d.PatientId
inner join Resources re on re.ResourceName = REVERSE(SUBSTRING(REVERSE(PTJ.TransactionRef),0,CHARINDEX(''/'',REVERSE(PTJ.TransactionRef),0)))
where PTJ.TransactionType = ''L'' and PTJ.TransactionStatus = ''S''
and ptj.transactiontypeid = d.PatientId
and re.ResourceId = @ResourceIds
and CONVERT(datetime, TransactionDate) between @StartDate and DATEADD(mi,1440,@EndDate)

UNION ALL

select d.PatientId, '''' AS ReminderId, NULL AS ReminderSentDate
from #Denominator d
where d.ConfirmationStatusId is not null

SET @TotParm = (SELECT COUNT (DISTINCT PatientID) from #Denominator)
SET @CountParm = (SELECT COUNT (DISTINCT PatientID) from #Numerator)

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId

select n.PatientId, p.FirstName, p.LastName, n.ReminderId, n.ReminderSentDate
from #Numerator n
inner join model.patients p on p.id = n.PatientId
order by p.LastName, p.FirstName, n.PatientId, n.ReminderSentDate

DROP TABLE #Denominator
DROP TABLE #Numerator

END 
'
IF OBJECT_ID('model.USP_CMS_Stage2_PatientReminders') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)