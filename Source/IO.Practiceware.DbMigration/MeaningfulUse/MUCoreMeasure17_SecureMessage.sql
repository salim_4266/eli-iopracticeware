/****** Object:  StoredProcedure [model].[USP_CMS_Stage2_SecureMessage]    Script Date: 12/17/2013 2:37:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
			
--exec [model].[USP_CMS_Stage2_SecureMessage] ''12/15/2013'', ''03/15/2014'', 192
DECLARE @sql NVARCHAR(MAX)
SET @sql = '
CREATE PROCEDURE [model].[USP_CMS_Stage2_SecureMessage]    
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   

BEGIN    

IF @ResourceIds LIKE ''%,%''
			RAISERROR (''Please ONLY send one doctor id at a TIME.'',16	,2)
			
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

--Denominator
select PatientId, e.Id as EncounterId, a.DateTime as AppointmentDate
into #Denominator
from model.encounters e
inner join model.appointments a on a.EncounterId = e.id
where a.DateTime between @StartDate and DATEADD(mi,1440,@EndDate) --DATEADD so that EndDate includes the whole day of encounters
and e.EncounterStatusId = 7
and a.UserId = @ResourceIds
   
--Numerator    
select d.*
into #Numerator
from #Denominator d
inner join model.patients p on p.id = d.PatientId
inner join model.Gender_Enumeration g on g.Id = p.GenderId
inner join [IO.Practiceware.SqlClr].GetNumeratorDetailsForSecureMessage(@StartDate,@EndDate,@ResourceIds,''OMEDIX'') sm on sm.PatientDob = p.DateOfBirth and sm.patientFirstName = p.FirstName and sm.patientLastName = p.LastName and sm.patientGender = SUBSTRING(g.Name,1,1)


SET @TotParm = (SELECT COUNT (DISTINCT PatientID) from #Denominator)
SET @CountParm = (SELECT COUNT (DISTINCT PatientID) from #Numerator)

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.AppointmentDate
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.AppointmentDate
from #Numerator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

DROP TABLE #Denominator
DROP TABLE #Numerator

END 
'
IF OBJECT_ID('model.USP_CMS_Stage2_SecureMessage') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)