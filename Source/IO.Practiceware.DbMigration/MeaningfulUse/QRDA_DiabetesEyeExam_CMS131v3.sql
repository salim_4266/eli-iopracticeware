/****** Object:  StoredProcedure [qrda].[QRDA_DiabetesEyeExam_CMS131v3]    Script Date: 9/4/2014 12:26:48 PM ******/
/***FOR DROPPING (IF EXIST) *******/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_DiabetesEyeExam_CMS131v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [qrda].[QRDA_DiabetesEyeExam_CMS131v3]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_DiabetesEyeExam_CMS131v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [qrda].[QRDA_DiabetesEyeExam_CMS131v3]
(
@StartDate datetime,
@EndDate datetime,
@UserId int
)
AS
BEGIN
	
		----Age 18-75 by reporting end date
		SELECT pd.Id
			,FirstName +'' '' + LastName AS Name
		INTO #Age
		FROM model.Patients pd
		WHERE DATEDIFF (yyyy,pd.DateOFBirth, CONVERT(datetime, @StartDate)) BETWEEN 18 AND 74
			AND pd.LastName <> ''TEST''

		----Visit within reporting period
		SELECT a.Id, Name
		INTO #Visits
		FROM #Age a
		INNER JOIN dbo.Appointments apOv ON apOV.PatientId = a.Id
			AND @UserId = apOv.ResourceId1
			AND apOV.AppTime > 0
		INNER JOIN PatientReceivableServices prs ON prs.AppointmentId = apOv.AppointmentId
			AND prs.Service IN (''99201'', ''99202'', ''99203'', ''99204'', ''99205'', ''99211'', ''99212'', ''99213'', ''99214'', ''99215'', ''99217'', ''99218'', ''99219'', ''99220''
				, ''99241'', ''99242'', ''99243'', ''99244'', ''99245''
				, ''99341'', ''99342'', ''99343'', ''99344'', ''99345'', ''99347'', ''99348'', ''99349'', ''99350''
				, ''99384'', ''99385'', ''99386'', ''99387''
				, ''99394'', ''99395'', ''99396'', ''99397'', ''99401'', ''99402'', ''99403'', ''99404'', ''99411'', ''99412'', ''99420'', ''99429'', ''99455'', ''99456''
				, ''92002'', ''92003'', ''92004'', ''92005'', ''92006'', ''92007'', ''92008'', ''92009'', ''92010'', ''92011'', ''92012'', ''92013'', ''92014''
				, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310'', ''99315'', ''99316'', ''99318''
				, ''99324'', ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337'')
			AND prs.Status=''A''
			AND CONVERT(datetime, prs.ServiceDate) between @StartDate and @EndDate
		GROUP BY a.Id, Name, apOv.AppointmentId

		-----Diagnosis of Diabetes starting before Reporting End Date
		SELECT v.Id
			, Name
			,0 AS IsInNumerator
		INTO #Denominator
		FROM #Visits v
		INNER JOIN Appointments ap2Y on ap2Y.patientid = v.Id
			AND @EndDate >= ap2Y.AppDate
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap2Y.AppointmentId
			AND pc.ClinicalType = ''Q'' 
			AND pc.Status=''A''
			AND CASE 
				WHEN LEN(FindingDetail) > 6
					THEN SUBSTRING(FindingDetail, 1, LEN(FindingDetail)-CHARINDEX(''.'',REVERSE(findingdetail)))
				ELSE FindingDetail
				END IN (''250'', ''250.0'', ''250.00'', ''250.01'', ''250.02'', ''250.03''
				, ''250.10'', ''250.11'', ''250.12'', ''250.13''
				, ''250.20'', ''250.21'', ''250.22'', ''250.23''
				, ''250.30'', ''250.31'', ''250.32'', ''250.33''
				, ''250.4'', ''250.40'', ''250.41'', ''250.42'', ''250.43''
				, ''250.50'', ''250.51'', ''250.52'', ''250.53''
				, ''250.60'', ''250.61'', ''250.62'', ''250.63''
				, ''250.7'', ''250.70'', ''250.71'', ''250.72'', ''250.73''
				, ''250.8'', ''250.80'', ''250.81'', ''250.82'', ''250.83''
				, ''250.9'', ''250.90'', ''250.91'', ''250.92'', ''250.93''
				, ''357.2''
				, ''362.0'', ''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'', ''362.07''
				, ''366.41''
				, ''648.0'', ''648.00'', ''648.01'', ''648.02'', ''648.03'', ''648.04'')
		

		--Eye Exam had during reporting period 
		----Dilation and Findings at back of eye 
		SELECT d.Id
			,Name
			,1 AS IsInNumerator
		INTO #Numerator
		FROM #Denominator d
			INNER JOIN Appointments ap on ap.PatientId = d.Id 
				AND ap.AppDate BETWEEN @StartDate AND @EndDate
			--Dilated
			INNER JOIN PatientClinical pcDilation on pcDilation.AppointmentId = ap.AppointmentId 
				AND (
						(pcDilation.ClinicalType = ''F'' 
							AND pcDilation.Symptom IN 
								(''/DILATION''
								, ''/DILATED IOP PROVOCATIVE''
								, ''/DILATED REFRACTION''
								, ''/FA''
								, ''/FA AND FP''
								, ''/FA AND FP TECH''
								, ''/FLUORESCEIN ANGIO''
								, ''/FLUORESCEIN ANGIO TECH''
								, ''/FLUORESCEIN ANGIOGRAPHY''
								, ''/ICG ANGIOGRAPHY''
								, ''/ICG TECH''
								, ''/INFRARED''
								, ''/INFRARED TECH''
								, ''/OPHTHALMOSCOPY INITIAL''
								, ''/OPHTHALMOSCOPY SUBSEQ''
								, ''/OPHTHALMOSCOPY, DETECTION''
								, ''/OPHTHALMOSCOPY, MANAGE''
								, ''/PQRI (ARMD DILATED MACULAR EXAM)''
								, ''/PQRI (DM RETINOPATHY DOCUMENT)''
								, ''/PQRI (DM RETINOPATHY INFO)''
								, ''/PQRI (DM1 DILATED EXAM IN DM)''
								, ''/PQRI (DM2 VF IN DM PATIENT)''
								, ''/PQRI (DM3 PHOTOS IN DM PATIENT)''
								, ''/PQRI (DM4 LOW RISK DM PATIENT)''
								)
						)
						OR 
						(pcDilation.ClinicalType = ''Q'' 
							AND pcDilation.Symptom LIKE ''%&%'')
					)
				AND pcDilation.Status = ''A''
			--Any Back of the Eye Findings
			INNER JOIN PatientClinical pcFindings ON pcFindings.AppointmentId = pcDilation.AppointmentId
				AND pcFindings.ClinicalType = ''Q''
				AND pcFindings.ImageDescriptor IN (''M'', ''N'', ''O'', ''P'')
				AND pcFindings.Status = ''A''
			GROUP BY d.Id, Name, IsInNumerator
	
			UNION

			----Imaging of back of eye is also a retinal exam
			SELECT d.Id
				,Name
				,1 AS IsInNumerator
			FROM #Denominator d
			INNER JOIN Appointments ap on ap.PatientId = d.Id 
				AND ap.AppDate BETWEEN @StartDate AND @EndDate	
			INNER JOIN PatientClinical pcImaging on pcImaging.AppointmentId = ap.AppointmentId 
				AND pcImaging.ClinicalType = ''F'' 
				AND pcImaging.Status = ''A''
				AND pcImaging.Symptom IN 
					(''/AUTOFLUORESCENCE''
					, ''/FA''
					, ''/FA AND FP''
					, ''/FLUORESCEIN ANGIO''
					, ''/FLUORESCEIN ANGIOGRAPHY''
					, ''/FUNDUS PHOTO''
					, ''/FUNDUS PHOTO (FP)''
					, ''/FUNDUS PHOTO (RETINA)''
					, ''/GDX''
					, ''/HRT''
					, ''/HRT, MACULA''
					, ''/ICG ANGIOGRAPHY''
					, ''/INFRARED''
					, ''/OCT''
					, ''/OCT, CENTRAL MACULAR THICKNESS''
					, ''/OCT H''
					, ''/OCT, MACULA''
					, ''/RED FREE''
					, ''/SCANNING LASER POLARIMETRY''
					, ''/STEREO  PHOTO'')

			UNION

			--or Low Risk -- no DR finding in year before reporting period
			SELECT d.Id
				,Name
				,1 AS IsInNumerator
			FROM #Denominator d
			INNER JOIN Appointments App1Year ON App1Year.PatientId = d.Id
				AND DATEDIFF (yyyy, CONVERT(datetime, App1Year.AppDate), CONVERT(datetime, @StartDate)) = 1
				AND App1Year.ScheduleStatus = ''D''
			INNER JOIN PatientClinical pcLowRisk ON pcLowRisk.AppointmentId = App1Year.AppointmentId
				AND pcLowRisk.Status = ''A''
				AND pcLowRisk.ClinicalType IN (''B'', ''K'')
				AND SUBSTRING(pcLowRisk.FindingDetail, 1, 6) NOT IN (''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
		
		--Output
			SELECT DISTINCT
			 d.Id as Id
			,d.Name
			,CONVERT(bit, d.IsInNumerator + COALESCE(n.IsInNumerator, 0)) AS IsInNumerator
		FROM #Denominator d
		LEFT JOIN #Numerator n ON n.Id = d.Id

		DROP TABLE #Age
		DROP TABLE #Visits
		DROP TABLE #Denominator
		DROP TABLE #Numerator

END
'
END
GO
