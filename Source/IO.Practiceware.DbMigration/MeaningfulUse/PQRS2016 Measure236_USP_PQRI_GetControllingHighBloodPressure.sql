--PQRS #236 USP_PQRI_GetControllingHighBloodPressure
DECLARE @sql nvarchar(max)
SET @sql = N'
Create PROCEDURE [dbo].[USP_PQRI_GetControllingHighBloodPressure]

(

@StartDate nvarchar(100),

@EndDate nvarchar(100),

@ResourceIds nvarchar(1000)

)

AS

BEGIN



	CREATE TABLE #TMPResources(ResourceId nvarchar(1000))

	IF @ResourceIds=''''

		BEGIN

			INSERT INTO #TMPResources 

			SELECT Resources.ResourceId 

			FROM Resources 

			WHERE ResourceType IN(''D'',''Q'')

		END

	ELSE

		BEGIN

			DECLARE @TmpResourceIds varchar(100)

			DECLARE @ResId varchar(10)

			SELECT @TmpResourceIds =@ResourceIds

			WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0

				BEGIN

					SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)

					SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))   
					 
					INSERT INTO #TMPResources values(@ResId)

				END

				INSERT INTO #TMPResources values(@TmpResourceIds)

		END

		--#141 POAG Reduce Pressure Denominator  

		SELECT p.PatientId AS patient_os_id

			,ap.AppointmentId AS OVAppointmentId

			,MIN(pcDx.Symptom) AS DxRank

			,p.FirstName AS tq_firstname

			,p.LastName AS tq_lastname

			,ap.AppDate AS tq_encounterdt

			,re.NPI AS tq_physiciannpi

			,p.BirthDate AS tq_dob

			,CASE p.Gender

				WHEN ''M'' THEN ''M''

				WHEN ''F'' THEN ''F''

				ELSE ''U''

				END AS tq_gender

			,''1'' AS tq_medicare2

			,ISNULL(MAX(SUBSTRING(icd10.FindingDetailIcd10, 1, 6)),MAX(SUBSTRING(pcDx.FindingDetail, 1, 6))) AS tq_icd  
			 
			,prs.Service AS tq_em

			,3 AS tq_iopmeasure

			,2 AS tq_glaucplan2

			,pcDx.EyeContext

		INTO #CHBPDenominator

		FROM PatientDemographics p

		INNER JOIN Appointments ap ON p.PatientId = ap.PatientId

		INNER JOIN PatientClinical pcDx ON ap.AppointmentId = pcDx.AppointmentId

			AND pcDx.ClinicalType IN (''B'',''K'')

			AND pcDx.Status = ''A''

		INNER JOIN PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId

		INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId

			AND pri.InsurerReferenceCode IN (''N'', ''M'', ''['')

		INNER JOIN Resources re ON re.ResourceId = pr.BillToDr

			AND re.ResourceId IN (SELECT #TMPResources.ResourceId from #TMPResources)

		INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice

			AND prs.Status = ''A''

		LEFT JOIN (SELECT AppointmentId,FindingDetailIcd10 FROM PatientClinicalIcd10   
					WHERE ClinicalType IN (''B'',''K'')
					AND Status = ''A''
					AND FindingDetailIcd10 IN (''I10'')
					) AS icd10 ON icd10.AppointmentId=ap.AppointmentId


		WHERE 

			--appointment during 2012

			ap.AppDate BETWEEN @StartDate AND @EndDate

			--over 18 years and below 85

			AND (DATEDIFF (yyyy, CONVERT(datetime, BirthDate), CONVERT(datetime, ap.AppDate)) >= 18 AND DATEDIFF (yyyy, CONVERT(datetime, BirthDate), CONVERT(datetime, ap.AppDate)) <=85)

			--diagnosis

			AND SUBSTRING(pcDx.FindingDetail, 1, 6) IN (''401.0'',''401.1'',''401.9'')

			--with office visit
			
			AND prs.Service IN (''99201'',''99202'',''99203'',''99204'',''99205'',''99211'',''99212'',''99213'',''99214'',''99215'',''G0402'',''G0438'',''G0439'') 

			AND p.LastName <> ''TEST''

		 GROUP BY  ap.AppointmentId

			,p.PatientId 

			,p.FirstName 

			,p.LastName 

			,ap.AppDate

			,re.NPI 

			,p.BirthDate

			,p.Gender

			,prs.Service

			,pcDx.EyeContext 


		--Denominator
		SELECT v.*
			, 0 AS Numerator
		INTO #Denominator 
		FROM #CHBPDenominator v

		--Numerator


			SELECT DISTINCT d.patient_os_id
			, (Case When SUBSTRING(FindingDetail, (CHARINDEX(''<'', FindingDetail)+1) ,(CHARINDEX(''/'',FindingDetail)-(CHARINDEX(''<'', FindingDetail)+1))) <140  Then 1
					When SUBSTRING(FindingDetail, (CHARINDEX(''<'', FindingDetail)+1) ,(CHARINDEX(''/'',FindingDetail)-(CHARINDEX(''<'', FindingDetail)+1))) >=140 THEN 2			
				END
				) AS systolic
			,(case WHEN SUBSTRING(FindingDetail, CHARINDEX(''/'', FindingDetail)+1, (CHARINDEX(''>'',FindingDetail)-CHARINDEX(''/'',FindingDetail)-1)) <90 Then 1
			  WHEN SUBSTRING(FindingDetail, CHARINDEX(''/'', FindingDetail)+1, (CHARINDEX(''>'',FindingDetail)-CHARINDEX(''/'',FindingDetail)-1))>=90 THEN 2
			  END) AS diastolic
		INTO #Numerator
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.patient_os_id
			AND DATEDIFF (month, CONVERT(datetime, ap.appDate), CONVERT(datetime, @EndDate)) <= 24
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
			AND pc.ClinicalType = ''F'' 
			AND pc.Status=''A''
			AND pc.Symptom = ''/Blood Pressure'' AND FindingDetail like ''%*BLOODPRESSURE%'' AND CHARINDEX(''/'',FindingDetail)>0
	
		UNION
	
		SELECT DISTINCT d.patient_os_id
			, 0 AS systolic
			,0 AS diastolic
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.patient_os_id
			AND DATEDIFF (month, CONVERT(datetime, ap.appDate), CONVERT(datetime, @EndDate)) <= 24
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
			AND pc.ClinicalType = ''F'' 
			AND pc.Status=''A''
			AND pc.Symptom = ''/Blood Pressure'' AND FindingDetail Not like ''%*BLOODPRESSURE%'' AND CHARINDEX(''/'',FindingDetail)<0

--Output
		SELECT DISTINCT
			 d.patient_os_id
			 ,tq_firstname
			 ,tq_lastname
			 ,CONVERT(nvarchar(10), CONVERT(datetime, tq_encounterdt),101) AS EncounterDate
			 ,tq_physiciannpi AS PhysicianNPI
			 ,tq_dob AS PatientDateOfBirth
			 ,tq_gender
			 ,1 AS IsMedicare
			 ,d.tq_icd AS ICDCode
			 ,tq_em AS EM
			,CASE WHEN n.systolic = 1 AND n.diastolic = 1  THEN ''G8752 G8754''
				WHEN n.systolic = 1 AND n.diastolic = 2  THEN ''G8752 G8755''
				WHEN n.systolic = 2 AND n.diastolic = 1  THEN ''G8753 G8754''
				WHEN n.systolic = 2 AND n.diastolic = 2  THEN ''G8753 G8755''
				ELSE ''G8756''
				END AS Numerator
		FROM #Denominator d
		LEFT JOIN #Numerator n ON n.patient_os_id = d.patient_os_id
	
		DROP TABLE #CHBPDenominator
		DROP TABLE #Denominator
		DROP TABLE #Numerator
		DROP TABLE #TMPResources


	END
'

IF OBJECT_ID('USP_PQRI_GetControllingHighBloodPressure') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)