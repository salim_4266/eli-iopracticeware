/****** Object:  StoredProcedure [model].[USP_CMS_Stage1_MedicationList]    Script Date: 12/17/2013 2:37:02 PM ******/
/****** Stage 1: C5 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--exec [model].[USP_CMS_Stage1_MedicationList] '07/01/2014', '09/30/2014', 3
DECLARE @sql NVARCHAR(MAX)
SET @sql = '
CREATE PROCEDURE [model].[USP_CMS_Stage1_MedicationList]  
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   

BEGIN    
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
  
--numerator  
select DISTINCT d.PatientId, pc.ClinicalId as PatientMedicationId
into #Numerator
from #Denominator d
inner join PatientCLinical pc on pc.patientid = d.patientid and ClinicalType = ''A'' and Status = ''A'' and FindingDetail like ''RX-%''

SET @TotParm = (SELECT COUNT (DISTINCT PatientID) from #Denominator)

--If they have MU ON, we will ask the physician if the patient is taking any meds. This can be activated if the practice had MUON then entire reporting period
--IF (select FieldValue from dbo.PracticeInterfaceConfiguration where FieldReference = ''MUON'') = ''T''
--BEGIN SET @CountParm = @TotParm
--END
--ELSE
BEGIN
SET @CountParm = (SELECT COUNT (DISTINCT PatientID) from #Numerator)
END

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.EncounterDate
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.EncounterDate, d.EncounterId

select n.PatientId, p.FirstName, p.LastName, n.PatientMedicationId
from #Numerator n
inner join model.patients p on p.id = n.PatientId
order by p.LastName, p.FirstName, n.PatientId, n.PatientMedicationId

select distinct d.patientid
from #Denominator d
left join #Numerator n on d.PatientId = n.PatientId
where n.patientid is null
order by d.patientid

DROP TABLE #Denominator
DROP TABLE #Numerator

END 
'
IF OBJECT_ID('model.USP_CMS_Stage1_MedicationList') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC (@sql)
