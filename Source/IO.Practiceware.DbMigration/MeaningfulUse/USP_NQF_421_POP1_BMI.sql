---------------USP_NQF_421_POP1_BMI----------------

DECLARE @sql nvarchar(max)
SET @sql = N'

CREATE PROCEDURE [dbo].[USP_NQF_421_POP1_BMI]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;
	SET @TotParm=0;
	SET @CountParm=0;

	CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
	IF @ResourceIds=''''
	BEGIN
		INSERT INTO #TMPResources 
		SELECT Resources.ResourceId 
		FROM Resources 
		WHERE ResourceType IN (''D'',''Q'', ''Y'', ''Z'')
	END
	ELSE
		BEGIN
			DECLARE @TmpResourceIds VARCHAR(100)
			DECLARE @ResId VARCHAR(10)
		
			SELECT @TmpResourceIds =@ResourceIds
			WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
				BEGIN
					SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
					SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    
					INSERT INTO #TMPResources values(@ResId)
				END
				INSERT INTO #TMPResources values(@TmpResourceIds)
		END
		
---Denominator 1 - Patients 65 and over, 1 OV
	SELECT DISTINCT pd.PatientId, pr.AppointmentId, pr.InvoiceDate
			INTO #InitialPopulation
			FROM PatientDemographics pd
			INNER JOIN PatientReceivables pr ON pr.PatientId = pd.PatientId
				AND pr.InvoiceDate BETWEEN @StartDate AND @EndDate
			INNER JOIN PatientReceivableServices prs ON pr.Invoice = prs.Invoice
				AND prs.Service in (
					--
					''92002'', ''92004'', ''92012'', ''92014''
					--office visit
					, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''
					, ''99212'', ''99213'', ''99214'', ''99215''
					--
					, ''99241'', ''99242'', ''99243'', ''99244'', ''99245''
					, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310''
					, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''
					, ''99334'', ''99335'', ''99336'', ''99337'')  
				AND prs.Status=''A''
			INNER JOIN Resources re on re.ResourceId = pr.BillToDr 
				AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
			WHERE DATEDIFF (yyyy, CONVERT(datetime, pd.BirthDate), CONVERT(datetime, @StartDate)) >= 65

	SELECT DISTINCT PatientId
	INTO #Denominator
	FROM #InitialPopulation

	---Numerator - 
	SELECT d.PatientId
	INTO #Numerator
	FROM #InitialPopulation d
	INNER JOIN dbo.PatientClinical pc ON pc.PatientId = d.PatientId
		AND pc.ClinicalType = ''F''
		AND pc.Status = ''A''
		AND pc.Symptom = ''/HEIGHT AND WEIGHT''
		AND SUBSTRING(pc.FindingDetail, 1, 11) = ''*BMI=T14905''
	INNER JOIN dbo.Appointments aHW ON aHW.AppointmentId = pc.AppointmentId
	WHERE CAST(SUBSTRING(pc.FindingDetail, CHARINDEX(''<'', pc.FindingDetail)+1, CHARINDEX(''>'', pc.FindingDetail)-CHARINDEX(''<'', pc.FindingDetail)-1) AS decimal) >= 22
		AND CAST(SUBSTRING(pc.FindingDetail, CHARINDEX(''<'', pc.FindingDetail)+1, CHARINDEX(''>'', pc.FindingDetail)-CHARINDEX(''<'', pc.FindingDetail)-1) AS decimal) < 30
		AND DATEDIFF (month, CONVERT(datetime, aHW.AppDate), CONVERT(datetime, d.InvoiceDate)) <= 6
	GROUP BY d.PatientId

	UNION

	SELECT d.PatientId
	FROM #InitialPopulation d
	INNER JOIN dbo.PatientClinical pcHW ON pcHW.PatientId = d.PatientId
	INNER JOIN dbo.Appointments aHW ON aHW.AppointmentId = pcHW.AppointmentId
	INNER JOIN dbo.PatientClinical pcLtr ON pcLtr.AppointmentId = aHW.AppointmentId
		AND pcLtr.ClinicalType = ''A''
		AND pcLtr.Status = ''A''
		AND SUBSTRING(pcLtr.FindingDetail, 1, 26) = ''SEND A CONSULTATION LETTER''
	WHERE CAST(SUBSTRING(pcHW.FindingDetail, CHARINDEX(''<'', pcHW.FindingDetail)+1, CHARINDEX(''>'', pcHW.FindingDetail)-CHARINDEX(''<'', pcHW.FindingDetail)-1) AS decimal) < 22
		AND CAST(SUBSTRING(pcHW.FindingDetail, CHARINDEX(''<'', pcHW.FindingDetail)+1, CHARINDEX(''>'', pcHW.FindingDetail)-CHARINDEX(''<'', pcHW.FindingDetail)-1) AS decimal) >= 30
		AND DATEDIFF (month, CONVERT(datetime, aHW.AppDate), CONVERT(datetime, d.InvoiceDate)) <= 6
	GROUP BY d.PatientId


		SELECT @TotParm=COUNT(*)  
		FROM #Denominator

		SELECT @CountParm=COUNT(*)
		FROM #Numerator
		
		SELECT @TotParm as Denominator, @CountParm as Numerator

	END
'

IF OBJECT_ID('USP_NQF_421_POP1_BMI') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)