--------------------CQM #86 POAG-----------------------------
DECLARE @sql nvarchar(max)
SET @sql = N'
CREATE PROCEDURE [dbo].[USP_NQM_86_POAG]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;
	SET @TotParm=0;
	SET @CountParm=0;

	CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
	IF @ResourceIds=''''
	BEGIN
	
	INSERT INTO #TMPResources 
	SELECT Resources.ResourceId 
	FROM Resources 
	WHERE ResourceType IN (''D'',''Q'')
	END
	ELSE
	BEGIN
	
		DECLARE @TmpResourceIds VARCHAR(100)
		DECLARE @ResId VARCHAR(10)

		SELECT @TmpResourceIds =@ResourceIds
		WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
			BEGIN
				SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
				SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    
				INSERT INTO #TMPResources values(@ResId)
			END
			INSERT INTO #TMPResources values(@TmpResourceIds)
		END

		-- CQM #86 aged 18 and over, POAG diagnosis
		SELECT pd.PatientId, pr.AppointmentId
		INTO #InitialPopulation
		FROM PatientDemographics pd
		INNER JOIN PatientClinical pc ON pc.PatientId = pd.PatientId
			AND pc.ClinicalType = ''Q'' 
			AND pc.Status=''A''
			AND SUBSTRING(pc.FindingDetail,1,6)  IN (''365.10'', ''365.11'', ''365.12'', ''365.15'')
		INNER JOIN PatientReceivables pr ON pr.PatientId = pd.PatientId
		INNER JOIN Resources re on re.ResourceId = pr.BillToDr 
			AND re.ResourceId IN (SELECT #TMPResources.ResourceId FROM #TMPResources)
		INNER JOIN PatientReceivableServices prs ON prs.Invoice=pr.Invoice 
			AND prs.Service IN
					(''92002'', ''92004'', ''92012'', ''92014''
					, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''
					, ''99212'', ''99213'', ''99214'', ''99215''
					, ''99241'', ''99242'', ''99243'', ''99244'', ''99245''
					, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310''
					, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''
					, ''99334'', ''99335'', ''99336'', ''99337'') 
			AND prs.Status=''A''
		WHERE DATEDIFF (yyyy, CONVERT(datetime, pd.BirthDate), CONVERT(datetime, @EndDate)) >= 18
		GROUP BY pd.patientid, pr.AppointmentId


		--Denominator - 2 office visits
		SELECT ip.PatientId
		INTO #Denominator
		FROM #InitialPopulation ip
		GROUP BY ip.PatientId 
		HAVING COUNT(*) > 1

		--Numerator - optic nerve head evaluation
		CREATE TABLE #Numerator(PatientId INT)

		INSERT INTO #Numerator

		SELECT d.PatientId 
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.PatientId
			AND DATEDIFF (mm, CONVERT(datetime, @EndDate), CONVERT(datetime, ap.appdate)) BETWEEN -12 AND 0
		INNER JOIN PatientClinical pcF On pcF.AppointmentId=ap.AppointmentId
			AND pcF.ClinicalType=''F'' 
			AND pcF.Status=''A''
			AND pcF.Symptom IN (''/CUP/DISC''
				, ''/PQRI (POAG, OPTIC NERVE EVAL)''
				, ''/OCT''
				, ''/OCT, OPTIC NERVE''
				, ''/OCT, RNFL'')
		GROUP BY d.PatientId
		
		UNION
		
		SELECT d.PatientId
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.PatientId
			AND DATEDIFF (mm, CONVERT(datetime, @EndDate), CONVERT(datetime, ap.appdate)) BETWEEN -12 AND 0
		INNER JOIN PatientClinical pcQ ON pcQ.AppointmentId=ap.AppointmentId 
			AND pcQ.ClinicalType=''Q'' 
			AND pcQ.Status=''A''
			AND pcQ.ImageDescriptor =''L'' 
			AND pcQ.FindingDetail NOT IN (''LN'', ''LX'', ''365.10'', ''365.11'', ''365.12'', ''365.15'')
		INNER JOIN PatientClinical pcU ON pcU.AppointmentId=ap.AppointmentId 
			AND pcU.ClinicalType = ''U''
			AND pcU.Status = ''A''
		GROUP BY d.PatientId


		SELECT @TotParm = COUNT(*)
		FROM #Denominator

		SELECT @CountParm = COUNT(*) 
		FROM #Numerator

		DROP TABLE #TMPResources
		DROP TABLE #InitialPopulation
		DROP TABLE #Denominator
		DROP TABLE #Numerator

		SELECT @TotParm as Denominator,@CountParm as Numerator

END
'

IF OBJECT_ID('USP_NQM_86_POAG') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)