/***FOR DROPPING (IF EXIST) *******/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[USP_CMS_Drug_Interactions_Enabled]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
       DROP PROCEDURE USP_CMS_Drug_Interactions_Enabled
END
GO
/**C6.2**** Object:  StoredProcedure [dbo].[USP_CMS_Drug_Interactions_Enabled]    Script Date: 07/04/2011 05:05:42 ******/

----------------------#6.2 Drug Interactions Enabled---------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[USP_CMS_Drug_Interactions_Enabled]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE USP_CMS_Drug_Interactions_Enabled
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN

DECLARE @CurrentValue NVARCHAR(1)
DECLARE @OldValue NVARCHAR(1)
DECLARE @CurrentDate DATETIME
DECLARE @OldDate DATETIME
DECLARE @Row INT
DECLARE @CountParm INT
DECLARE @TotParm INT
DECLARE @ErxId INT

SET @ErxID = (SELECT InterfaceId FROM PracticeInterfaceConfiguration WHERE FieldReference = ''ERX'')

SELECT ROW_NUMBER() OVER (ORDER BY ae.AuditDateTime DESC) AS Row
	,aec.NewValue
	,CONVERT(DATETIME, ae.AuditDateTime, 112) AS DATE
INTO #eRxValue
FROM AuditEntries ae
INNER JOIN auditentrychanges aec ON ae.id = aec.AuditEntryId
WHERE ae.ObjectName = ''dbo.PracticeInterfaceConfiguration''
	AND ae.KeyValueNumeric = @ErxId
	AND ae.ChangeTypeId = 1

IF @@ROWCOUNT > 1
BEGIN
	SET @Row = @@ROWCOUNT
	SET @OldValue = (SELECT NewValue FROM #eRxValue	WHERE Row = 2)
	SET @OldDate = (SELECT DATE	FROM #eRxValue WHERE Row = 2)
END

SET @CurrentValue = (SELECT NewValue FROM #eRxValue WHERE Row = 1)
SET @CurrentDate = (SELECT DATE FROM #eRxValue WHERE Row = 1)

IF @CurrentValue = ''F''
	SET @CountParm = 0;
ELSE IF @Row = 1
	SET @CountParm = 1;
ELSE IF @OldValue = ''F''
	IF DATEDIFF(DD, @CurrentDate, @OldDate) = 0
		SET @CountParm = 1;
	ELSE
		SET @CountParm = 0;

SET @TotParm = 1

DROP TABLE #eRxValue

SELECT @TotParm as Denominator,@CountParm as Numerator

END
'
END
GO
