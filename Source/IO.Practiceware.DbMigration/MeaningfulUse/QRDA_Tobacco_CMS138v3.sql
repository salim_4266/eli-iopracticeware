/****** Object:  StoredProcedure [qrda].[QRDA_Tobacco_CMS138v3]    Script Date: 09/03/2014 16:33:30 ******/
/***FOR DROPPING (IF EXIST) *******/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_Tobacco_CMS138v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [qrda].[QRDA_Tobacco_CMS138v3]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[qrda].[QRDA_Tobacco_CMS138v3]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [qrda].[QRDA_Tobacco_CMS138v3]
(
@StartDate datetime,
@EndDate datetime,
@UserId int
)
AS
BEGIN
		
		---Patients 18 and over with office visits in the reporting period
		SELECT pd.Id
			, FirstName +'' '' +LastName as Name
			, prs.Invoice
		INTO #Visits
		FROM model.Patients pd
		INNER JOIN dbo.Appointments apOV ON apOV.PatientId = pd.Id
			AND apOV.ResourceId1 = @UserId
			AND apOV.AppTime > 0
		INNER JOIN PatientReceivableServices prs ON apOv.AppointmentId = prs.AppointmentId
			AND prs.Service in (
				--
				''92002'', ''92004'', ''92012'', ''92014''
				--office visit
				, ''99201'', ''99202'', ''99203'', ''99204'', ''99205''
				, ''99212'', ''99213'', ''99214'', ''99215''
				--
				, ''99241'', ''99242'', ''99243'', ''99244'', ''99245''
				, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310''
				, ''99324'', ''99325'', ''99326'', ''99327'', ''99328''
				, ''99334'', ''99335'', ''99336'', ''99337'')  
			AND prs.Status=''A''
			AND CONVERT(datetime, prs.ServiceDate) BETWEEN @StartDate AND @EndDate
		WHERE DATEDIFF (yyyy, pd.DateOfBirth,  @StartDate) >= 18
			AND pd.LastName <> ''TEST''
		GROUP BY pd.Id, FirstName, LastName, prs.Invoice

		--Denominator - 2+ office visits during reporting period
		SELECT v.Id
			, v.Name
			, 0 AS IsInNumerator
		INTO #Denominator 
		FROM #Visits v
		GROUP BY v.Id, v.Name
		HAVING COUNT(*) > 1

		--Numerator
		SELECT d.Id
			, d.Name
			, 1 AS IsInNumerator
		INTO #Numerator
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.Id
			AND DATEDIFF (month, CONVERT(datetime, ap.AppDate), CONVERT(datetime, @EndDate)) <= 24 
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
			AND pc.ClinicalType = ''F'' 
			AND pc.Status=''A''
			AND pc.Symptom = ''/SMOKING'' AND pc.FindingDetail in (''*NEVER=T14857 <NEVER>'',''*FORMER=T14856 <FORMER>'')

		UNION
		
		SELECT d.Id
			, d.Name
			, 1 AS IsInNumerator
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.Id
			AND DATEDIFF (month, CONVERT(datetime, ap.appDate), CONVERT(datetime, @EndDate)) <= 24
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
			AND pc.ClinicalType = ''F'' 
			AND pc.Status=''A''
			AND pc.Symptom = ''/PQRS (TOBACCO NON-USER)''

		UNION

		SELECT d.Id
			, d.Name
			, 1 AS IsInNumerator
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.Id
			AND DATEDIFF (month, CONVERT(datetime, ap.appDate), CONVERT(datetime, @EndDate)) <= 24
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
			AND pc.ClinicalType = ''F'' 
			AND pc.Status=''A''
			AND pc.Symptom = ''/SMOKING'' AND pc.FindingDetail = ''*CESSATION=T22892 <CESSATION>''
		
		UNION
		
		SELECT d.Id
			, d.Name
			, 1 AS IsInNumerator
		FROM #Denominator d
		INNER JOIN Appointments ap ON ap.PatientId = d.Id
			AND DATEDIFF (month, CONVERT(datetime, ap.appDate), CONVERT(datetime, @EndDate)) <= 24
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
			AND pc.ClinicalType = ''F'' 
			AND pc.Status=''A''
			AND pc.Symptom = ''/PQRS (TOBACCO USER)''

		SELECT DISTINCT
			 d.Id as Id
			,d.Name
			,CONVERT(bit, d.IsInNumerator + COALESCE(n.IsInNumerator, 0)) AS IsInNumerator
		FROM #Denominator d
		LEFT JOIN #Numerator n ON n.Id = d.Id

		DROP TABLE #Visits
		DROP TABLE #Denominator
		DROP TABLE #Numerator

	END
'
END
GO