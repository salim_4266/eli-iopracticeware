﻿using System.Data;
using Soaf.Data;

namespace IO.Practiceware.DbMigration
{
    /// <summary>
    ///     A generic migration that executes SQL.
    /// </summary>
    /// <remarks>
    ///     We made SqlMigration implement IMigrationMetadata in an effort to allow MigrationRunner to print the version number
    ///     of the current migration.
    ///     Otherwise, MigrationRunner only printed the type name, through method GetType().
    /// </remarks>
    internal class SqlMigration : NonFluentTransactionalUpOnlyMigration
    {
        public SqlMigration(string key, string sql, long version)
            : base(version, key)
        {
            Sql = sql;
        }

        public string Sql { get; private set; }

        public override string GetName()
        {
            return Description;
        }

        protected override void MigrateUp(IDbConnection connection, IDbTransaction transaction)
        {
            connection.RunScript(Sql,
                false,
                createCommand: () =>
                {
                    IDbCommand command = connection.CreateCommand();
                    command.CommandTimeout = 0;
                    command.Transaction = transaction;
                    return command;
                },
                useSingleCommand: !Sql.Contains("#") && !Sql.Contains("IDENTITY_INSERT")
                /* can't use EXEC statements if there are temp tables or identity inserts involed */
                );
        }
    }
}