﻿IF (OBJECT_ID(N'dbo.GetInsurerIdInfoByItemIdAppointmentId','P')) IS NOT NULL
	DROP PROCEDURE [dbo].[GetInsurerIdInfoByItemIdAppointmentId] 
GO

CREATE PROCEDURE [dbo].[GetInsurerIdInfoByItemIdAppointmentId] 
	@AppointmentId INT
	,@ItemId INT
AS
BEGIN
SET NOCOUNT ON;

;WITH bstsExistsForInvoiceId  AS (
	SELECT 
	ir.InvoiceId
	,COUNT(*) AS NumOccurances
	FROM model.BillingServiceTransactions bst WITH (NOLOCK)
	INNER JOIN model.InvoiceReceivables ir WITH (NOLOCK) ON ir.Id = bst.InvoiceReceivableId
	INNER JOIN model.Invoices i WITH (NOLOCK) ON i.Id = ir.InvoiceId
		AND i.LegacyPatientReceivablesAppointmentId = @AppointmentId
	GROUP BY ir.InvoiceId
)
SELECT DISTINCT
ptj.TransactionTypeId AS ReceivableId
,CASE WHEN ir.OpenForReview = 0 THEN CASE WHEN bsti.NumOccurances IS NOT NULL THEN 'B' ELSE 'S' END WHEN ir.OpenForReview = 1 THEN 'I' END AS ReceivableType
,i.LegacyPatientReceivablesPatientId AS PatientId
,COALESCE(ip.PolicyHolderPatientId, i.LegacyPatientReceivablesPatientId) AS InsuredId
,COALESCE(ip.InsurerId, 0) AS InsurerId
,i.LegacyPatientReceivablesInvoice AS Invoice
,ptj.*
,bst.BillingServiceId AS ServiceId
,ptj.TransactionDate AS TransactionDate
,CONVERT(UNIQUEIDENTIFIER, NULL) AS ExternalSystemMessageId
FROM model.InvoiceReceivables ir WITH (NOLOCK)
INNER JOIN model.Invoices i WITH (NOLOCK) ON i.Id = ir.InvoiceId
INNER JOIN dbo.PracticeTransactionJournal ptj WITH (NOLOCK) ON ir.Id = ptj.TransactionTypeId 
	AND ptj.TransactionType = 'R'
	AND ptj.TransactionRef <> 'G'
	AND ptj.TransactionStatus <> 'Z'
INNER JOIN model.BillingServiceTransactions bst WITH (NOLOCK) ON bst.LegacyTransactionId = ptj.TransactionId
INNER JOIN ServiceTransactions st WITH (NOLOCK) ON ptj.TransactionId = st.TransactionId
LEFT JOIN model.PatientInsurances pi WITH (NOLOCK) ON pi.Id = ir.PatientInsuranceId
LEFT JOIN model.InsurancePolicies ip WITH (NOLOCK) ON ip.Id = pi.InsurancePolicyId
LEFT JOIN bstsExistsForInvoiceId bsti WITH (NOLOCK) ON bsti.InvoiceId = i.Id
WHERE i.LegacyPatientReceivablesAppointmentId = @AppointmentId 
	AND st.ServiceId = @ItemId
ORDER BY ptj.TransactionId
END

GO