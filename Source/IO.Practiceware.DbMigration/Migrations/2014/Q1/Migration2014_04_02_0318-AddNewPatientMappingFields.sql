﻿--Bugs #9867,10320,9781,9946,9947

--Create a row for 'Gender' in PracticeRepositoryEntities
	IF NOT EXISTS (
			SELECT TOP 1 *
			FROM model.PracticeRepositoryEntities
			WHERE Id = 62
				AND NAME = 'Gender'
			)
	BEGIN
		INSERT INTO model.PracticeRepositoryEntities (
			Id
			,NAME 
			,KeyPropertyName
			)
		VALUES (
			62
			,'Gender'
			,'Id'
			)
	END

--Just in case a row with PatientRace already Exists.
	UPDATE model.PracticeRepositoryEntities SET Name='Race' WHERE Name='PatientRace'
--Create a row for 'Race' in PracticeRepositoryEntities.Make sure Id is 37 
	IF NOT EXISTS (
			SELECT TOP 1 *
			FROM model.PracticeRepositoryEntities
			WHERE Id = 37
				AND NAME = 'Race'
			)
	BEGIN
		INSERT INTO model.PracticeRepositoryEntities (
			Id
			,NAME 
			,KeyPropertyName
			)
		VALUES (
			37
			,'Race'
			,'Id'
			)
	END

	UPDATE model.PracticeRepositoryEntities SET Name='Language' WHERE Name='PatientLanguage'
--Create a row for 'Language' in PracticeRepositoryEntities.Make sure Id is 9 
	IF NOT EXISTS (
			SELECT TOP 1 *
			FROM model.PracticeRepositoryEntities
			WHERE Id = 9
				AND NAME = 'Language'
			)
	BEGIN
		INSERT INTO model.PracticeRepositoryEntities (
			Id
			,NAME 
			,KeyPropertyName
			)
		VALUES (
			9
			,'Language'
			,'Id'
			)
	END

--Create a row for 'MaritalStatus' in PracticeRepositoryEntities.Make sure Id is 13 
	IF NOT EXISTS (
			SELECT TOP 1 *
			FROM model.PracticeRepositoryEntities
			WHERE Id = 13
				AND NAME = 'MaritalStatus'
			)
	BEGIN
		INSERT INTO model.PracticeRepositoryEntities (
			Id
			,NAME 
			,KeyPropertyName
			)
		VALUES (
			13
			,'MaritalStatus'
			,'Id'
			)
	END

--Create a row for 'Ethnicity' in PracticeRepositoryEntities.Make sure Id is 11 
	IF NOT EXISTS (
			SELECT TOP 1 *
			FROM model.PracticeRepositoryEntities
			WHERE Id = 11
				AND NAME = 'Ethnicity'
			)
	BEGIN
		INSERT INTO model.PracticeRepositoryEntities (
			Id
			,NAME 
			,KeyPropertyName
			)
		VALUES (
			11
			,'Ethnicity'
			,'Id'
			)
	END


DECLARE @ExternalSystemNames AS VARCHAR(MAX);
DECLARE @Counter INT;
DECLARE @ExternalSystemName VARCHAR(MAX);
DECLARE @ExternalSystemId INT
DECLARE @Delimiter VARCHAR(1);

SET @Delimiter = ',';
SET @Counter = 1;
SET @ExternalSystemNames='';

--Get Back all the Externalystem Names where EntityName is Patient.
IF EXISTS(SELECT * FROM model.ExternalSystemEntities WHERE Name = 'Patient')
BEGIN
	SELECT @ExternalSystemNames = COALESCE(@ExternalSystemNames + es.Name + ',',es.Name + ',') FROM model.ExternalSystemEntities ese INNER JOIN model.ExternalSystems es on es.Id=ese.ExternalSystemId WHERE ese.Name='Patient' Group by es.Name
END

IF (@ExternalSystemNames <> '')
BEGIN
	WHILE @Counter != 0
	BEGIN
		SET @Counter = CHARINDEX(@Delimiter, @ExternalSystemNames)

		IF @Counter != 0
			SET @ExternalSystemName = LEFT(@ExternalSystemNames, @Counter - 1)
		ELSE
			SET @ExternalSystemName = @ExternalSystemNames

		IF (@ExternalSystemName <> '')
		BEGIN
			SET @ExternalSystemId = (
					SELECT Id
					FROM model.ExternalSystems
					WHERE NAME = @ExternalSystemName
					)
			--Create ExternalSystemEntities for ExternalSystems

			--Create Race
			UPDATE model.ExternalSystemEntities SET Name='Race' WHERE Name='PatientRace' AND ExternalSystemId = @ExternalSystemId
		 
			IF NOT EXISTS (
					SELECT TOP 1 *
					FROM model.ExternalSystemEntities
					WHERE NAME = 'Race'
						AND ExternalSystemId = @ExternalSystemId
					)
			BEGIN
				INSERT INTO model.ExternalSystemEntities (
					ExternalSystemId
					,NAME
					)
				VALUES (
					@ExternalSystemId
					,'Race'
					)
			END

			--Create Ethnicity
			UPDATE model.ExternalSystemEntities SET Name='Ethnicity' WHERE Name='PatientEthnicity' AND ExternalSystemId = @ExternalSystemId

			IF NOT EXISTS (
					SELECT TOP 1 *
					FROM model.ExternalSystemEntities
					WHERE NAME = 'Ethnicity'
						AND ExternalSystemId = @ExternalSystemId
					)
			BEGIN
				INSERT INTO model.ExternalSystemEntities (
					ExternalSystemId
					,NAME
					)
				VALUES (
					@ExternalSystemId
					,'Ethnicity'
					)
			END

			--Create Language 
			UPDATE model.ExternalSystemEntities SET Name='Language' WHERE Name='PatientLanguage' AND ExternalSystemId = @ExternalSystemId

			IF NOT EXISTS (
					SELECT TOP 1 *
					FROM model.ExternalSystemEntities
					WHERE NAME = 'Language'
						AND ExternalSystemId = @ExternalSystemId
					)
			BEGIN
				INSERT INTO model.ExternalSystemEntities (
					ExternalSystemId
					,NAME
					)
				VALUES (
					@ExternalSystemId
					,'Language'
					)
			END

			--Create Gender
			IF NOT EXISTS (
					SELECT TOP 1 *
					FROM model.ExternalSystemEntities
					WHERE NAME = 'Gender'
						AND ExternalSystemId = @ExternalSystemId
					)
			BEGIN
				INSERT INTO model.ExternalSystemEntities (
					ExternalSystemId
					,NAME
					)
				VALUES (
					@ExternalSystemId
					,'Gender'
					)
			END

			--Create MaritalStatus
			IF NOT EXISTS (
					SELECT TOP 1 *
					FROM model.ExternalSystemEntities
					WHERE NAME = 'MaritalStatus'
						AND ExternalSystemId = @ExternalSystemId
					)
			BEGIN
				INSERT INTO model.ExternalSystemEntities (
					ExternalSystemId
					,NAME
					)
				VALUES (
					@ExternalSystemId
					,'MaritalStatus'
					)
			END



			--Mappings
		
			-- Create mappings for Race values
			DECLARE @externalRaceEntityId INT

			SET @externalRaceEntityId = (
							SELECT Id
							FROM model.ExternalSystemEntities
							WHERE NAME = 'RACE'
								AND ExternalSystemId = @ExternalSystemId
							)

			DECLARE @raceEntityId INT

			SET @raceEntityId = (
					SELECT Id
					FROM model.PracticeRepositoryEntities
					WHERE NAME = 'RACE'
					)

			--Remove all the mappings for Race.Just to make sure old values are deleted.
			DELETE model.ExternalSystemEntityMappings WHERE ExternalSystemEntityId=@externalRaceEntityId AND PracticeRepositoryEntityId=@raceEntityId

			INSERT INTO model.ExternalSystemEntityMappings (
				PracticeRepositoryEntityId
				,PracticeRepositoryEntityKey
				,ExternalSystemEntityId
				,ExternalSystemEntityKey
				)
			SELECT @raceEntityId
				,pr.Id
				,@externalRaceEntityId
				,pr.Name
			FROM model.Races pr
			LEFT JOIN model.ExternalSystemEntityMappings esm ON esm.ExternalSystemEntityKey = pr.Name
				AND esm.ExternalSystemEntityId = @externalRaceEntityId
			WHERE (pr.Name IS NOT NULL OR Pr.Name <> '') AND esm.Id IS NULL

			-- Create mappings for Language values
			DECLARE @externalLanguageEntityId INT

			SET @externalLanguageEntityId = (
					SELECT Id
					FROM model.ExternalSystemEntities
					WHERE NAME = 'Language'
						AND ExternalSystemId = @ExternalSystemId
					)

			DECLARE @languageEntityId INT

			SET @LanguageEntityId = (
					SELECT Id
					FROM model.PracticeRepositoryEntities
					WHERE NAME = 'Language'
					)
	
			--Remove all the mappings for Language.Just to make sure old values are deleted.
			DELETE model.ExternalSystemEntityMappings WHERE ExternalSystemEntityId=@externalLanguageEntityId AND PracticeRepositoryEntityId=@languageEntityId

			INSERT INTO model.ExternalSystemEntityMappings (
				PracticeRepositoryEntityId
				,PracticeRepositoryEntityKey
				,ExternalSystemEntityId
				,ExternalSystemEntityKey
				)
			SELECT @LanguageEntityId
				,MAX(pl.Id)
				,@externalLanguageEntityId
				,pl.Name
			FROM model.Languages pl
			LEFT JOIN model.ExternalSystemEntityMappings esm ON esm.ExternalSystemEntityKey = pl.Name
				AND esm.ExternalSystemEntityId = @externalLanguageEntityId
			WHERE (pl.Name IS NOT NULL OR pl.Name <>'')
				AND esm.Id IS NULL GROUP BY pl.Name

			-- Create mappings for ETHNICITY values
			DECLARE @externalEthnicityEntityId INT

			SET @externalEthnicityEntityId = (
					SELECT Id
					FROM model.ExternalSystemEntities
					WHERE NAME = 'ETHNICITY'
						AND ExternalSystemId = @ExternalSystemId
					)

			DECLARE @ethnicityEntityId INT

			SET @ethnicityEntityId = (
					SELECT Id
					FROM model.PracticeRepositoryEntities
					WHERE NAME = 'ETHNICITY'
					)
	
			--Remove all the mappings for Ethnicity.Just to make sure old values are deleted.
			DELETE model.ExternalSystemEntityMappings WHERE ExternalSystemEntityId=@externalEthnicityEntityId AND PracticeRepositoryEntityId=@ethnicityEntityId

			INSERT INTO model.ExternalSystemEntityMappings (
				PracticeRepositoryEntityId
				,PracticeRepositoryEntityKey
				,ExternalSystemEntityId
				,ExternalSystemEntityKey
				)
			SELECT @ethnicityEntityId
				,pe.Id
				,@externalEthnicityEntityId
				,pe.Name
			FROM model.Ethnicities pe
			LEFT JOIN model.ExternalSystemEntityMappings esm ON esm.ExternalSystemEntityKey = pe.Name
				AND esm.ExternalSystemEntityId = @externalEthnicityEntityId
			WHERE (pe.Name IS NOT NULL OR pe.Name <>'')
				AND esm.Id IS NULL

			--Create mappings for Gender
			DECLARE @externalgenderEntityId INT

			SET @externalgenderEntityId = (
					SELECT Id
					FROM model.ExternalSystemEntities
					WHERE NAME = 'GENDER'
						AND ExternalSystemId = @externalSystemId
					)

			DECLARE @genderEntityId INT

			SET @genderEntityId = (
					SELECT Id
					FROM model.PracticeRepositoryEntities
					WHERE NAME = 'GENDER'
					)

			INSERT INTO model.ExternalSystemEntityMappings (
				PracticeRepositoryEntityId
				,PracticeRepositoryEntityKey
				,ExternalSystemEntityId
				,ExternalSystemEntityKey
				)
			SELECT @genderEntityId
				,CASE Code
					WHEN 'Unknown'
						THEN 1
					WHEN 'Female'
						THEN 2
					WHEN 'Male'
						THEN 3
					WHEN 'Other'
						THEN 4
					END
				,@externalgenderEntityId
				,SubStringOneCode
			FROM dbo.PracticeCodeTable pc
			LEFT JOIN model.ExternalSystemEntityMappings esm ON esm.ExternalSystemEntityKey = pc.SubStringOneCode
				AND esm.ExternalSystemEntityId = @externalgenderEntityId
			WHERE pc.ReferenceType = 'GENDER'
				AND pc.Code <> ''
				AND pc.Code IS NOT NULL
	
			--Create mappings for MaritalStatus
			DECLARE @externalMaritalStatusEntityId INT

			SET @externalMaritalStatusEntityId = (
					SELECT Id
					FROM model.ExternalSystemEntities
					WHERE NAME = 'MaritalStatus'
						AND ExternalSystemId = @externalSystemId
					)

			DECLARE @maritalStatusEntityId INT

			SET @maritalStatusEntityId = (
					SELECT Id
					FROM model.PracticeRepositoryEntities
					WHERE NAME = 'MaritalStatus'
					)

			SELECT @genderEntityId
				,CASE Code
					WHEN 'Single'
						THEN 1
					WHEN 'Married'
						THEN 2
					WHEN 'Divorced'
						THEN 3
					WHEN 'Widowed'
						THEN 4
					WHEN 'Annulled'
						THEN 5
					WHEN 'DomesticPartner'
						THEN 6
					WHEN 'Interlocutory'
						THEN 7
					WHEN 'LegallySeparated'
						THEN 8
					WHEN 'Polygamous'
						THEN 9
					WHEN 'NeverMarried'
						THEN 10
					END
				,@externalgenderEntityId
				,SubStringOneCode
			FROM dbo.PracticeCodeTable pc
			LEFT JOIN model.ExternalSystemEntityMappings esm ON esm.ExternalSystemEntityKey = pc.SubStringOneCode
				AND esm.ExternalSystemEntityId = @externalgenderEntityId
			WHERE pc.ReferenceType = 'MARITAL'
				AND pc.Code <> ''
				AND pc.Code IS NOT NULL
		
			SET @ExternalSystemNames = RIGHT(@ExternalSystemNames, LEN(@ExternalSystemNames) - @Counter);
		END
	END
END