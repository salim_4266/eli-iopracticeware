﻿IF (OBJECT_ID('dbo.PatientDemographics','V') IS NOT NULL)
BEGIN
EXEC('ALTER VIEW [dbo].[PatientDemographics]	WITH VIEW_METADATA
	AS
	SELECT 
		p.Id AS PatientId,
		CASE
			WHEN DefaultUserId IS NULL
				THEN 0
			ELSE
				DefaultUserId
			END AS SchedulePrimaryDoctor,
		ISNULL(FirstName,'''') AS FirstName,
		CASE
			WHEN IsClinical = 0
				THEN ''I''
			WHEN PatientStatusId = 2
				THEN ''C''
			WHEN PatientStatusId = 3
				THEN ''D''
			WHEN PatientStatusId = 4
				THEN ''M''
			ELSE
				''A''
			END AS [Status],
		ISNULL(LastName,'''') AS LastName,
		ISNULL(MiddleName,'''') AS MiddleInitial,
		ISNULL(Prefix,'''') AS Salutation,
		ISNULL(Suffix,'''') AS NameReference,
		CASE
			WHEN DateOfBirth IS NULL
				THEN CONVERT(nvarchar(64), '''')
			ELSE
				DATENAME(yyyy, DateOfBirth) + 
				RIGHT(''00'' + CONVERT(nvarchar(2), DATEPART(mm, DateOfBirth)), 2) +
				RIGHT (''00'' + CONVERT(nvarchar(2), DATEPART(dd, DateOfBirth)), 2)
			END AS BirthDate,
		CASE
			WHEN GenderId = 1
				THEN ''U''
			WHEN GenderId = 2
				THEN ''F''
			WHEN GenderId = 3
				THEN ''M''
			WHEN GenderId = 4
				THEN ''O''
			ELSE
				''''
			END AS Gender,
		CASE
			WHEN MaritalStatusId = 1
				THEN ''S''
			WHEN MaritalStatusId = 2
				THEN ''M''
			WHEN MaritalStatusId = 3
				THEN ''D''
			WHEN MaritalStatusId = 4
				THEN ''W''
			ELSE
				''''
			END AS Marital,
		ISNULL(Occupation,'''') AS Occupation,
		ISNULL(PriorPatientCode,'''') AS OldPatient,
			'''' AS ReferralRequired,
		ISNULL(SocialSecurityNumber,'''') AS SocialSecurity,
		ISNULL(EmployerName,'''') AS BusinessName,
			CASE 
				WHEN IsHipaaConsentSigned = CONVERT(bit, 1)
					THEN ''T''
				WHEN IsHipaaConsentSigned = CONVERT(bit, 0)
					THEN ''F''
				ELSE
					CONVERT(nvarchar(1), NULL)
				END AS Hipaa,
			Ethnicity AS Ethnicity,
			[Language] AS [Language],
			ISNULL(p.Race,'''') AS Race,
		ISNULL(BusinessType,'''') AS BusinessType,
		ISNULL(p.Address,'''') As [Address],
		ISNULL(p.Suite,'''') As Suite,
		ISNULL(p.City,'''') As City,
		ISNULL(p.[State],'''') AS [State],
		ISNULL(p.Zip,'''') AS Zip,
		ISNULL(p.BusinessAddress,'''') AS BusinessAddress,
		ISNULL(p.BusinessSuite,'''') AS BusinessSuite,
		ISNULL(p.BusinessCity,'''') AS BusinessCity,
		ISNULL(p.BusinessState,'''') AS BusinessState,
		ISNULL(p.BusinessZip,'''') AS BusinessZip,
		ISNULL(p.HomePhone,'''') AS HomePhone,
		ISNULL(p.CellPhone,'''') AS CellPhone,
		ISNULL(p.BusinessPhone,'''') AS BusinessPhone,
		ISNULL(p.EmployerPhone,'''') AS EmployerPhone,
		ISNULL((SELECT TOP 1 COALESCE(ExternalProviderId, 0) FROM model.PatientExternalProviders pep WHERE pep.PatientId = p.Id AND IsReferringPhysician = 1), '''') AS ReferringPhysician,
		ISNULL((SELECT TOP 1 COALESCE(ExternalProviderId, 0) FROM model.PatientExternalProviders pep WHERE pep.PatientId = p.Id AND IsPrimaryCarePhysician = 1), '''') AS PrimaryCarePhysician,
		ISNULL(p.ProfileComment1,'''') AS ProfileComment1,
		ISNULL(p.ProfileComment2,'''') AS ProfileComment2,
		ISNULL(p.Email,'''') AS Email,
			'''' AS FinancialAssignment,
			'''' AS FinancialSignature,
			CONVERT(nvarchar(64),'''') AS MedicareSecondary,
			CASE
				WHEN p.PreferredAddressId = (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = p.PolicyPatientId ORDER BY pad.Id)
					THEN ''Y''
				ELSE
					''N''
				END AS BillParty,
			'''' AS SecondBillParty,
			CONVERT(nvarchar(32),'''') AS ReferralCatagory,
			CONVERT(nvarchar(max),'''') AS EmergencyName,
			CONVERT(nvarchar(max),'''') AS EmergencyPhone,
			'''' AS EmergencyRel,
			p.PolicyPatientId,
			p.SecondPolicyPatientId AS SecondPolicyPatientId,
			p.Relationship AS Relationship,
			p.SecondRelationship AS SecondRelationship,
			p.PatType AS PatType,
			CONVERT(int, NULL) AS BillingOffice,
			CONVERT(int, NULL) AS PostOpExpireDate,
			CONVERT(int, NULL) AS PostOpService,
			CONVERT(nvarchar(64), '''') AS FinancialSignatureDate,
			'''' AS Religion,
			'''' AS SecondRelationshipArchived,
			CONVERT(int, NULL) AS SecondPolicyPatientIdArchived,
			'''' AS RelationshipArchived,
			CONVERT(int, NULL) AS PolicyPatientIdArchived,
			'''' AS BulkMailing,
			'''' AS ContactType,
			'''' AS NationalOrigin,
			CASE IsStatementSuppressed
				WHEN CONVERT(bit, 0)
					THEN ''Y''
				ELSE
					''N''
				END AS SendStatements,
			Case IsCollectionLetterSuppressed
				WHEN CONVERT(bit, 0)
					THEN ''Y''
				ELSE
					''N''
			END AS SendConsults
	FROM (SELECT *, 
		  model.GetPairId(1, p1.Id, 110000000) As PrimaryCarePhysicianId,
		  ISNULL(dbo.GetPolicyHolderPatientId(p1.Id), p1.Id) AS PolicyPatientId,
		  ISNULL(dbo.GetSecondPolicyHolderPatientId(p1.Id),0) AS SecondPolicyPatientId,
		  ISNULL(dbo.GetPolicyHolderRelationshipType(p1.Id),''Y'') AS Relationship,
		  ISNULL(dbo.GetSecondPolicyHolderRelationshipType(p1.Id),'''') AS SecondRelationship,
		  (SELECT TOP 1 ExchangeAndSuffix FROM model.PatientPhoneNumbers ppn WHERE ppn.PatientId = p1.Id AND ppn.PatientPhoneNumberTypeId = 7 AND ppn.OrdinalId = 1) As HomePhone,
		  (SELECT TOP 1 ExchangeAndSuffix FROM model.PatientPhoneNumbers ppn WHERE ppn.PatientId = p1.Id AND ppn.PatientPhoneNumberTypeId = 3 AND ppn.OrdinalId = 1) AS CellPhone,
		  (SELECT TOP 1 ExchangeAndSuffix FROM model.PatientPhoneNumbers ppn WHERE ppn.PatientId = p1.Id AND ppn.PatientPhoneNumberTypeId = 2 AND ppn.OrdinalId = 1) AS BusinessPhone,
		  (SELECT TOP 1 ExchangeAndSuffix FROM model.PatientPhoneNumbers ppn WHERE ppn.PatientId = p1.Id AND ppn.OrdinalId = 1 AND ppn.PatientPhoneNumberTypeId IN (SELECT Id FROM dbo.PracticeCodeTable pct WHERE pct.Code = ''Employer'' AND ReferenceType = ''PhoneTypePatient'')) AS EmployerPhone,
		  (SELECT TOP 1 Line1 FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS [Address],
		  (SELECT TOP 1 Line2 FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS Suite,
		  (SELECT TOP 1 City FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS City,
		  (SELECT TOP 1 Abbreviation FROM model.StateOrProvinces sp WHERE sp.Id = (SELECT TOP 1 StateOrProvinceId FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1)) AS [State],
		  (SELECT TOP 1 PostalCode FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS Zip,
		  (SELECT TOP 1 Line1 FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1) AS BusinessAddress,
		  (SELECT TOP 1 Line2 FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1) AS BusinessSuite,
		  (SELECT TOP 1 City FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1) AS BusinessCity,
		  (SELECT TOP 1 Abbreviation FROM model.StateOrProvinces sp WHERE sp.Id = (SELECT TOP 1 StateOrProvinceId FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1)) AS BusinessState,
		  (SELECT TOP 1 PostalCode FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1) AS BusinessZip,
		  (SELECT TOP 1 Value FROM model.PatientEmailAddresses pea WHERE pea.PatientId = p1.Id AND pea.OrdinalId = 1) AS Email,
		  (SELECT TOP 1 Value FROM model.PatientComments pc WHERE pc.PatientCommentTypeId = 2 AND pc.PatientId = p1.Id ORDER BY pc.Id DESC) AS ProfileComment1,
		  (SELECT TOP 1 Comment FROM model.PatientReferralSources rs WHERE rs.PatientId = p1.Id AND rs.Comment IS NOT NULL) AS ProfileComment2,
		  (SELECT TOP 1 COALESCE(ExternalProviderId, 0) FROM model.PatientExternalProviders pep WHERE pep.Id = p1.Id) AS ReferringPhysician,
		  (SELECT TOP 1 PatientAddressId FROM model.PatientCommunicationPreferences pcp WHERE pcp.PatientId = p1.Id AND PatientCommunicationTypeId = 3) AS PreferredAddressId,
		  (SELECT TOP 1 DisplayName FROM model.Tags pt WHERE pt.Id = (SELECT TOP 1 TagId FROM model.PatientTags ppt WHERE ppt.PatientId = p1.Id AND ppt.OrdinalId = 1)) AS PatType,
		  (SELECT TOP 1 Name FROM model.Ethnicities pe WHERE pe.Id = EthnicityId) As Ethnicity,
		  (SELECT TOP 1 SUBSTRING(Name, 1, 10) FROM model.Languages pl WHERE pl.Id = LanguageId) As [Language],
		  (SELECT TOP 1 Name FROM model.Races WHERE Id = (SELECT TOP 1 Races_Id FROM model.PatientRace ppr WHERE ppr.Patients_Id = p1.Id)) AS Race,
		  (SELECT TOP 1 SUBSTRING(Name, 1, 1) FROM model.PatientEmploymentStatus pes WHERE pes.Id = PatientEmploymentStatusId) AS BusinessType
		FROM model.Patients p1) p
')
END