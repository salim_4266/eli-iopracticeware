﻿IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit


SELECT 1 AS Value INTO #NoAudit

UPDATE model.PatientPhoneNumbers 
SET ExchangeAndSuffix = SUBSTRING(ExchangeAndSuffix, 1, 3) + '-' + SUBSTRING(ExchangeAndSuffix, 4, 4)
WHERE IsNumeric(ExchangeAndSuffix) = 1 AND LEN(ExchangeAndSuffix) = 7

DROP TABLE #NoAudit