﻿DECLARE @sql NVARCHAR(MAX)
SELECT @sql = 
'ALTER TABLE dbo.PatientReceivableServices DROP CONSTRAINT ' + cc.name
FROM sys.check_constraints cc
INNER JOIN sys.columns c ON c.[object_id] = cc.parent_object_id
	AND c.column_id = cc.parent_column_id
WHERE [definition] LIKE '%CheckServiceFee%'
	AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivableServices]', 'U') 
	AND c.Name = 'Charge'
	AND cc.name LIKE '%Charge%'
EXEC(@sql)
GO

IF OBJECT_ID(N'[dbo].[CheckServiceFee]', 'FN') IS NOT NULL
	DROP FUNCTION [dbo].[CheckServiceFee]
GO

IF OBJECT_ID(N'dbo.PatientReceivableServicesQuantityChargeTrigger', 'TR') IS NOT NULL
BEGIN
	EXEC sp_rename 'dbo.PatientReceivableServicesQuantityChargeTrigger', 'PatientReceivableServicesChargesTrigger';
END


IF OBJECT_ID(N'dbo.PatientReceivableServicesChargesTrigger', 'TR') IS NOT NULL
BEGIN
	EXEC ('
	ALTER TRIGGER [dbo].[PatientReceivableServicesChargesTrigger]
	ON [dbo].[PatientReceivableServices]
	FOR INSERT, UPDATE 
	AS
	BEGIN
	SET NOCOUNT ON;

	;WITH CTE AS
	(
		SELECT
		i.ItemId
		,i.Charge
		,CASE 
			WHEN CHARINDEX(''.'', i.Charge, 0) > 0
				THEN LEN(SUBSTRING(CAST(i.Charge AS NVARCHAR(50)), CHARINDEX(''.'', i.Charge, 0) + 1, LEN(i.Charge) - (CHARINDEX(''.'', i.Charge, 0))))
			ELSE 0
		END AS [CheckCharge]
		FROM inserted i
		WHERE i.ItemId IN (SELECT ItemId FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i)
	)
	UPDATE prs
	SET prs.Charge = CONVERT(REAL,CONVERT(DECIMAL(18,2),c.Charge))
	FROM CTE c
	INNER JOIN dbo.PatientReceivableServices prs ON prs.ItemId = c.ItemId
	WHERE c.CheckCharge >= 3

	UPDATE prs
	SET 
		QuantityDecimal = Quantity,
		ChargeDecimal = Charge,
		FeeChargeDecimal = FeeCharge
	FROM PatientReceivableServices prs
	WHERE ItemId IN (SELECT ItemId FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i)
	AND (
		(QuantityDecimal IS NULL OR QuantityDecimal <> Quantity) 
		OR (ChargeDecimal IS NULL OR ChargeDecimal <> Charge) 
		OR (FeeChargeDecimal IS NULL OR FeeChargeDecimal <> FeeCharge)
	)
	END
	')
END
ELSE IF OBJECT_ID(N'dbo.PatientReceivableServicesChargesTrigger', 'TR') IS NULL
BEGIN
	EXEC ('
	CREATE TRIGGER [dbo].[PatientReceivableServicesChargesTrigger]
	ON [dbo].[PatientReceivableServices]
	FOR INSERT, UPDATE 
	AS
	BEGIN
	SET NOCOUNT ON;

	;WITH CTE AS
	(
		SELECT
		i.ItemId
		,i.Charge
		,CASE 
			WHEN CHARINDEX(''.'', i.Charge, 0) > 0
				THEN LEN(SUBSTRING(CAST(i.Charge AS NVARCHAR(50)), CHARINDEX(''.'', i.Charge, 0) + 1, LEN(i.Charge) - (CHARINDEX(''.'', i.Charge, 0))))
			ELSE 0
		END AS [CheckCharge]
		FROM inserted i
		WHERE i.ItemId IN (SELECT ItemId FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i)
	)
	UPDATE prs
	SET prs.Charge = CONVERT(REAL,CONVERT(DECIMAL(18,2),c.Charge))
	FROM CTE c
	INNER JOIN dbo.PatientReceivableServices prs ON prs.ItemId = c.ItemId
	WHERE c.CheckCharge >= 3

	UPDATE prs
	SET 
		QuantityDecimal = Quantity,
		ChargeDecimal = Charge,
		FeeChargeDecimal = FeeCharge
	FROM PatientReceivableServices prs
	WHERE ItemId IN (SELECT ItemId FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i)
	AND (
		(QuantityDecimal IS NULL OR QuantityDecimal <> Quantity) 
		OR (ChargeDecimal IS NULL OR ChargeDecimal <> Charge) 
		OR (FeeChargeDecimal IS NULL OR FeeChargeDecimal <> FeeCharge)
	)
END
	')
END