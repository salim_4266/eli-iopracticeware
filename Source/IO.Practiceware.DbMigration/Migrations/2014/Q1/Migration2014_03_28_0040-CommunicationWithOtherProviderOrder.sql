﻿IF NOT EXISTS (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'CommunicationWithOtherProviderOrder')
BEGIN
	INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName)
	SELECT
	74 AS Id
	,'CommunicationWithOtherProviderOrder' AS Name
	,'Id' AS KeyPropertyName

	UPDATE e
	SET e.PracticeRepositoryEntityId = 74
	FROM model.ExternalSystemEntityMappings e
	WHERE e.PracticeRepositoryEntityId = 73
END
GO