﻿IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit

SELECT 1 AS Value INTO #NoAudit

-- Fix identity on races and insert proprly named races

IF NOT EXISTS(SELECT * FROM model.Races WHERE Id < 7)
BEGIN
	SET IDENTITY_INSERT [model].[Races] ON

	INSERT INTO model.Races (Id, Name, IsArchived, OrdinalId)
	SELECT 1, 'American Indian or Alaska Native', CONVERT(bit, 0), 4
	UNION ALL 
	SELECT 2, 'Asian', CONVERT(bit, 0), 3
	UNION ALL
	SELECT 3, 'Black or African American', CONVERT(bit, 0), 2
	UNION ALL
	SELECT 4, 'Native Hawaiian or Other Pacific Islander', CONVERT(bit, 0), 4
	UNION ALL
	SELECT 5, 'White', CONVERT(bit, 0), 1
	UNION ALL
	SELECT 6, 'Declined to Specify', CONVERT(bit, 0), 5
	
	SET IDENTITY_INSERT [model].[Races] OFF
END
--Update all races to correct new values, for any entry that does not meet one of the above, will get Decline to specify
UPDATE model.PatientRace
SET Races_Id = 1
WHERE Races_Id IN (SELECT Id FROM model.Races WHERE Name = 'Native Amer' AND Id > 6)

UPDATE model.ExternalSystemEntityMappings
SET PracticeRepositoryEntityKey = CONVERT(nvarchar(max),1)
WHERE PracticeRepositoryEntityKey = (SELECT CONVERT(nvarchar(max),Id) FROM model.Races WHERE Name = 'Native Amer' AND Id > 6)
	AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Race')

UPDATE model.PatientRace
SET Races_Id = 2
WHERE Races_Id IN (SELECT Id FROM model.Races WHERE Name = 'Asian' AND Id > 6)

UPDATE model.ExternalSystemEntityMappings
SET PracticeRepositoryEntityKey = CONVERT(nvarchar(max),2)
WHERE PracticeRepositoryEntityKey = (SELECT CONVERT(nvarchar(max),Id) FROM model.Races WHERE Name = 'Asian' AND Id > CONVERT(nvarchar(max),6))
	AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Race')

UPDATE model.PatientRace
SET Races_Id = 3
WHERE Races_Id IN (SELECT Id FROM model.Races WHERE Name = 'Black' AND Id > 6)

UPDATE model.ExternalSystemEntityMappings
SET PracticeRepositoryEntityKey = CONVERT(nvarchar(max),3)
WHERE PracticeRepositoryEntityKey = (SELECT CONVERT(nvarchar(max),Id) FROM model.Races WHERE Name = 'Black' AND Id > CONVERT(nvarchar(max),6))
	AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Race')

UPDATE model.PatientRace
SET Races_Id = 4
WHERE Races_Id IN (SELECT Id FROM model.Races WHERE Name = 'Pacific Isl' AND Id > 6)

UPDATE model.ExternalSystemEntityMappings
SET PracticeRepositoryEntityKey = CONVERT(nvarchar(max),4)
WHERE PracticeRepositoryEntityKey = (SELECT CONVERT(nvarchar(max),Id) FROM model.Races WHERE Name = 'Pacific Isl' AND Id > CONVERT(nvarchar(max),6))
	AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Race')

UPDATE model.PatientRace
SET Races_Id = 5
WHERE Races_Id IN (SELECT Id FROM model.Races WHERE Name = 'White' AND Id > 6)

UPDATE model.ExternalSystemEntityMappings
SET PracticeRepositoryEntityKey = CONVERT(nvarchar(max),5)
WHERE PracticeRepositoryEntityKey = (SELECT CONVERT(nvarchar(max),Id) FROM model.Races WHERE Name = 'White' AND Id > CONVERT(nvarchar(max),6))
	AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Race')

UPDATE model.PatientRace
SET Races_Id = 6
WHERE Races_Id IN (SELECT Id FROM model.Races WHERE Name IN ('Other', 'Unknown'))

UPDATE model.ExternalSystemEntityMappings
SET PracticeRepositoryEntityKey = 6
WHERE PracticeRepositoryEntityKey > CONVERT(nvarchar(max),5)
	AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Race')

DELETE FROM model.PatientRace
WHERE Races_Id > 6

-- Delete all races not specified above
DELETE FROM model.Races WHERE Id > 6


-------------####Ethnicity

IF NOT EXISTS(SELECT * FROM model.Ethnicities WHERE Id < 3)
BEGIN

	SET IDENTITY_INSERT [model].[Ethnicities] ON

	INSERT INTO model.Ethnicities (Id, Name)
	SELECT 1, 'Hispanic or Latino'
	UNION ALL 
	SELECT 2, 'Not Hispanic or Latino'
	UNION ALL
	SELECT 3, 'Declined to Specify'
	
	SET IDENTITY_INSERT [model].[Ethnicities] OFF
END

UPDATE model.Patients 
SET EthnicityId = 1
WHERE EthnicityId = (SELECT Id FROM model.Ethnicities WHERE Name = 'Hisp')

UPDATE model.ExternalSystemEntityMappings
SET PracticeRepositoryEntityKey = CONVERT(nvarchar(max),1)
WHERE PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Ethnicity')
	AND PracticeRepositoryEntityKey = (SELECT CONVERT(nvarchar(max),Id) FROM model.Ethnicities WHERE Name = 'Hisp')

UPDATE model.Patients 
SET EthnicityId = 2
WHERE EthnicityId = (SELECT Id FROM model.Ethnicities WHERE Name = 'Not Hisp')

UPDATE model.ExternalSystemEntityMappings
SET PracticeRepositoryEntityKey = CONVERT(nvarchar(max),2)
WHERE PracticeRepositoryEntityKey = (SELECT CONVERT(nvarchar(max),Id) FROM model.Ethnicities WHERE Name = 'Not Hisp')
	AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Ethnicity')

UPDATE model.Patients
SET EthnicityId = 3
WHERE EthnicityId = (SELECT TOP 1 Id FROM model.Ethnicities WHERE Name LIKE '%Declines%' AND Id > 4)

UPDATE model.ExternalSystemEntityMappings
SET PracticeRepositoryEntityKey = CONVERT(nvarchar(max),3)
WHERE PracticeRepositoryEntityKey NOT IN (CONVERT(nvarchar(max),1), CONVERT(nvarchar(max),2))
	AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Ethnicity')

UPDATE model.Patients
SET EthnicityId = NULL
WHERE EthnicityId NOT IN (1, 2, 3)


DELETE FROM model.Ethnicities WHERE Id > 3

IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit