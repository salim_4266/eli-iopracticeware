﻿-- This changes the AlternateCode in PracticeCodeTable for the PaymentMethods view
-- The practice can choose which credit card types appear on the patient statement

UPDATE PracticeCodeTable
SET AlternateCode = 'IncludeOnStatement'
WHERE ReferenceType = 'PAYABLETYPE'
AND (Code LIKE '%Visa%' OR Code LIKE '%Discover%' OR Code LIKE '%MasterCard%' OR Code LIKE '%American Express%')