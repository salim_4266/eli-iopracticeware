﻿-- The sole purpose of this migration is a precursor to Migration2014_02_27_0580-PatientAggregateStatementsFromPracticeTransactionJournal and Migration2014_02_23_0860-MissingBillingServiceTransactionForComputedFinancialIdNull
--for main databases that have been migrated to an ealier version of main. 
--We can't make any changes to migrations that use this table because they will run again on live clients.

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PracticeTransactionJournalReceivablesBackup]') AND type in (N'U'))
CREATE TABLE dbo.PracticeTransactionJournalReceivablesBackup
	(TransactionId INT
	,ReceivableId  INT
	,OldTransactionTypeId INT
	,NewTransactionTypeId INT)