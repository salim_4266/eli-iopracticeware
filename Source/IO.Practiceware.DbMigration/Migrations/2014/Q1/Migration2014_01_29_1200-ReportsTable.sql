﻿IF EXISTS (SELECT OBJECT_ID FROM sys.tables WHERE name = 'TemplateDocuments' and schema_id = 6)
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = ''

	SELECT @sql = @sql + 'DROP TRIGGER ' + s.name + '.' + tr.name + '
	'
	FROM sys.triggers tr
	INNER JOIN sys.tables ta ON tr.parent_id = ta.object_id
	INNER JOIN sys.schemas s ON s.schema_id = ta.schema_id
	 WHERE ta.Name = 'TemplateDocuments'
	 AND tr.Name LIKE '%Audit%'

	EXEC(@Sql)
END

IF EXISTS (SELECT OBJECT_ID FROM sys.tables WHERE name = 'TemplateDocumentPrinter' and schema_id = 6) 
BEGIN
	SET @sql = ''

	SELECT @sql = @sql + 'DROP TRIGGER ' + s.name + '.' + tr.name + '
	'
	FROM sys.triggers tr
	INNER JOIN sys.tables ta ON tr.parent_id = ta.object_id
	INNER JOIN sys.schemas s ON s.schema_id = ta.schema_id
	 WHERE ta.Name = 'TemplateDocumentPrinter'
	 AND tr.Name LIKE '%Audit%'

	EXEC(@Sql)
END

-- Check if 'Reports' exists...
IF OBJECT_ID(N'model.Reports', 'U') IS NULL
BEGIN
-- 'Reports' does not exist, creating...
	CREATE TABLE [model].[Reports] (
    [Name] nvarchar(255)  NOT NULL,
    [ReportCategoryId] int  NOT NULL,
    [ReportTypeId] int  NOT NULL,
    [Content] nvarchar(max)  NULL,
    [Documentation] nvarchar(max)  NOT NULL,
    [PermissionId] int  NULL
);
END

GO

-- Creating primary key on [Name] in table 'Reports'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.Reports', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[Reports] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.Reports', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[Reports]
ADD CONSTRAINT [PK_Reports]
    PRIMARY KEY CLUSTERED ([Name] ASC);
GO


-- Creating foreign key on [PermissionId] in table 'Reports'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PermissionReport'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Reports] DROP CONSTRAINT FK_PermissionReport

IF OBJECTPROPERTY(OBJECT_ID('[model].[Permissions]'), 'IsUserTable') = 1
ALTER TABLE [model].[Reports]
ADD CONSTRAINT [FK_PermissionReport]
    FOREIGN KEY ([PermissionId])
    REFERENCES [model].[Permissions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PermissionReport'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PermissionReport')
	DROP INDEX IX_FK_PermissionReport ON [model].[Reports]
GO
CREATE INDEX [IX_FK_PermissionReport]
ON [model].[Reports]
    ([PermissionId]);
GO

DELETE FROM model.TemplateDocumentPrinter
WHERE TemplateDocuments_Id IN (SELECT Id FROM model.TemplateDocuments WHERE ContentTypeId = 5)