﻿IF OBJECT_ID('tempdb..#NoAudit') IS NULL
	SELECT 1 AS Id INTO #NoAudit

IF OBJECT_ID('tempdb..#NoCheck') IS NULL
SELECT 1 AS Value INTO #NoCheck

DECLARE @ArchivedAdjustmentType NVARCHAR(100)
DECLARE @InsertedArchivedAdjustmentType NVARCHAR(100)
IF OBJECT_ID('tempdb..#CharTable') IS NOT NULL
	DROP TABLE #CharTable

;WITH CTE AS 
(
	SELECT 65 AS Alpha

	UNION ALL 

	SELECT Alpha + 1 AS Alpha
	FROM CTE
	WHERE Alpha < 90
)
SELECT CHAR(Alpha) AS Alpha INTO #CharTable FROM CTE WHERE CHAR(Alpha) NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V','I')
UNION ALL
SELECT '[' AS [Alpha] UNION ALL SELECT ']' UNION ALL SELECT '{' UNION ALL SELECT '}' UNION ALL SELECT '<' UNION ALL SELECT '>' UNION ALL SELECT '\' UNION ALL SELECT '~' UNION ALL SELECT '`'
UNION ALL SELECT '1' UNION ALL SELECT '2' UNION ALL SELECT '3' UNION ALL SELECT '4' UNION ALL SELECT '5' UNION ALL SELECT '6' UNION ALL SELECT '7' UNION ALL SELECT '8' UNION ALL SELECT '9';

SELECT TOP 1 @ArchivedAdjustmentType = Alpha 
FROM #CharTable CTE
LEFT JOIN dbo.PracticeCodeTable pct ON CTE.Alpha = SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
	AND pct.ReferenceType = 'PAYMENTTYPE' 
WHERE pct.Id IS NULL

IF (@ArchivedAdjustmentType IS NOT NULL)
BEGIN
	INSERT INTO dbo.PracticeCodeTable (
		[ReferenceType]
		,[Code]
		,[AlternateCode]
		,[Exclusion]
		,[LetterTranslation]
		,[OtherLtrTrans]
		,[FormatMask]
		,[Signature]
		,[TestOrder]
		,[FollowUp]
		,[Rank]
		,[WebsiteUrl]
	)
	SELECT
	'PAYMENTTYPE' AS ReferenceType
	,'ArchivedCredit - '+@ArchivedAdjustmentType
	,'C' AS [AlternateCode]
	,'F' AS [Exclusion]
	,'UNKC' AS [LetterTranslation]
	,NULL AS [OtherLtrTrans]
	,NULL AS [FormatMask]
	,'F' AS [Signature]
	,NULL AS [TestOrder]
	,NULL AS [FollowUp]
	,99 AS [Rank]
	,NULL AS [WebsiteUrl]
END
ELSE IF (@ArchivedAdjustmentType IS NULL)
BEGIN
	SELECT TOP 1
	@ArchivedAdjustmentType = SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
	FROM dbo.PracticeCodeTable pct
	LEFT JOIN dbo.PatientReceivablePaymentsBackup p ON p.PaymentType = SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
	WHERE pct.ReferenceType = 'PAYMENTTYPE' 
	GROUP BY PaymentType,SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
	ORDER BY COUNT(PaymentType)

	UPDATE pct
	SET Code = 'ArchivedCredit - '+@ArchivedAdjustmentType
	,AlternateCode = 'C'
	,LetterTranslation = 'UNKC'
	,[Rank] = 99
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'
	AND SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) = @ArchivedAdjustmentType
END

SET @InsertedArchivedAdjustmentType = @ArchivedAdjustmentType
SET @ArchivedAdjustmentType = NULL

SELECT TOP 1 @ArchivedAdjustmentType = Alpha 
FROM #CharTable CTE
LEFT JOIN dbo.PracticeCodeTable pct ON CTE.Alpha = SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
	AND pct.ReferenceType = 'PAYMENTTYPE' 
WHERE pct.Id IS NULL

IF (@ArchivedAdjustmentType IS NOT NULL)
BEGIN
	INSERT INTO dbo.PracticeCodeTable (
		[ReferenceType]
		,[Code]
		,[AlternateCode]
		,[Exclusion]
		,[LetterTranslation]
		,[OtherLtrTrans]
		,[FormatMask]
		,[Signature]
		,[TestOrder]
		,[FollowUp]
		,[Rank]
		,[WebsiteUrl]
	)
	SELECT
	'PAYMENTTYPE' AS ReferenceType
	,'ArchivedDebit - '+@ArchivedAdjustmentType
	,'D' AS [AlternateCode]
	,'F' AS [Exclusion]
	,'UNKD' AS [LetterTranslation]
	,NULL AS [OtherLtrTrans]
	,NULL AS [FormatMask]
	,'F' AS [Signature]
	,NULL AS [TestOrder]
	,NULL AS [FollowUp]
	,99 AS [Rank]
	,NULL AS [WebsiteUrl]
	END
ELSE IF (@ArchivedAdjustmentType IS NULL)
BEGIN
	SELECT TOP 1 @ArchivedAdjustmentType = SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
	FROM dbo.PracticeCodeTable pct
	LEFT JOIN dbo.PatientReceivablePaymentsBackup p ON p.PaymentType = SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
	WHERE pct.ReferenceType = 'PAYMENTTYPE' AND SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) <> @InsertedArchivedAdjustmentType
	GROUP BY PaymentType,SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
	ORDER BY COUNT(PaymentType)

	UPDATE pct
	SET Code = 'ArchivedDebit - '+@ArchivedAdjustmentType
	,AlternateCode = 'D'
	,LetterTranslation = 'UNKD'
	,[Rank] = 99
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'
	AND SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) = @ArchivedAdjustmentType
END

;WITH CTE AS (
SELECT 
v.Id
,v.PaymentFinancialType
,v.AdjustmentTypeIdUnEdit
FROM (
	SELECT 
	adj.Id
	,adj.AdjustmentTypeId AS AdjustmentTypeIdUnEdit
	,CASE WHEN adj.AdjustmentTypeId > 1000
		THEN adj.AdjustmentTypeId - 1000
	END AS AdjustmentTypeId
	,p.*
	FROM model.Adjustments adj
	INNER JOIN (
		SELECT
		Id
		,CASE 
			WHEN (adj.PatientId IS NOT NULL AND adj.FinancialSourceTypeId = 2)
				THEN adj.PatientId
			WHEN (adj.InsurerId IS NOT NULL AND adj.FinancialSourceTypeId = 1)
				THEN adj.InsurerId
			ELSE 0
		END AS PayerId
		,CONVERT(NVARCHAR(1),CASE 
			WHEN adj.FinancialSourceTypeId = 1
				THEN 'I'
			WHEN adj.FinancialSourceTypeId = 2
				THEN 'P'
			WHEN adj.FinancialSourceTypeId = 3
				THEN 'O'
			WHEN adj.FinancialSourceTypeId = 4
				THEN ''
		END) AS PayerType
		FROM model.Adjustments adj
)v ON v.Id = adj.Id
INNER JOIN dbo.PatientReceivablePaymentsBackup p ON adj.BillingServiceId = p.PaymentServiceItem
	AND adj.Amount = p.PaymentAmountDecimal
	AND v.PayerId = p.PayerId
	AND v.PayerType = p.PayerType
WHERE adj.AdjustmentTypeId NOT IN (SELECT Id FROM model.AdjustmentTypes)
)v
INNER JOIN dbo.PracticeCodeTable pct ON v.AdjustmentTypeId = pct.Id
	AND pct.ReferenceType = 'PAYMENTTYPE'
WHERE SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = v.PaymentType
	AND v.PaymentType <> '' AND v.PaymentType <> ' ' AND v.PaymentType IS NOT NULL
GROUP BY v.Id
,v.PaymentFinancialType
,v.AdjustmentTypeIdUnEdit
)
UPDATE adj
SET adj.AdjustmentTypeId = 
	CASE 
		WHEN CTE.PaymentFinancialType = 'D' 
			THEN (SELECT TOP 1 Id FROM model.AdjustmentTypes WHERE Name = 'ArchivedDebit' AND IsDebit = 1)
		WHEN CTE.PaymentFinancialType = 'C' 
			THEN (SELECT TOP 1 Id FROM model.AdjustmentTypes WHERE Name = 'ArchivedCredit' AND IsDebit = 0)
	END
FROM model.Adjustments adj
INNER JOIN CTE ON CTE.Id = adj.Id
WHERE CTE.PaymentFinancialType IN ('C','D')

DROP TABLE #NoCheck