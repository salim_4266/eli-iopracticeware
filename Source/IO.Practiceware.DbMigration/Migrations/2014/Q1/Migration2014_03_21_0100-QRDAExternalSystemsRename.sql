﻿--Fix names to ExternalSystemEntities to match QualityDataModelValueSet ENUM

UPDATE model.ExternalSystemEntities SET Name = 'AttributeResultLevelOfSeverityOfRetinopathyFindings' WHERE Name = 'AttributeLevelOfSeverityOfRetinopathyFindings' AND ExternalSystemId = 23 
UPDATE model.ExternalSystemEntities SET Name = 'AttributeResultMacularEdemaFindingsAbsent' WHERE Name = 'AttributeMacularEdemaFindingsAbsent' AND ExternalSystemId = 23
UPDATE model.ExternalSystemEntities SET Name = 'AttributeResultMacularEdemaFindingsPresent' WHERE Name = 'AttributeMacularEdemaFindingsPresent' AND ExternalSystemId = 23
UPDATE model.ExternalSystemEntities SET Name = 'AttributeResultNegativeFinding' WHERE Name = 'AttributeNegativeFinding' AND ExternalSystemId = 23
UPDATE model.ExternalSystemEntities SET Name = 'AttributeReasonOverweight' WHERE Name = 'AttributeOverweight' AND ExternalSystemId = 23
UPDATE model.ExternalSystemEntities SET Name = 'AttributeReasonUnderweight' WHERE Name = 'AttributeUnderweight' AND ExternalSystemId = 23
UPDATE model.ExternalSystemEntities SET Name = 'CommunicationFromProviderToProviderConsultantReport' WHERE Name = 'CommunicationConsultantReport' AND ExternalSystemId = 23
UPDATE model.ExternalSystemEntities SET Name = 'CommunicationFromProviderToProviderLevelOfSeverityOfRetinopathyFindings' WHERE Name = 'CommunicationLevelOfSeverityOfRetinopathyFindings' AND ExternalSystemId = 23
UPDATE model.ExternalSystemEntities SET Name = 'CommunicationFromProviderToProviderMacularEdemaFindingsAbsent' WHERE Name = 'CommunicationMacularEdemaFindingsAbsent' AND ExternalSystemId = 23
UPDATE model.ExternalSystemEntities SET Name = 'CommunicationFromProviderToProviderMacularEdemaFindingsPresent' WHERE Name = 'CommunicationMacularEdemaFindingsPresent' AND ExternalSystemId = 23
UPDATE model.ExternalSystemEntities SET Name = 'CommunicationFromProviderToProviderNotDoneMedicalReason' WHERE Name = 'CommunicationMedicalReason' AND ExternalSystemId = 23
UPDATE model.ExternalSystemEntities SET Name = 'CommunicationFromProviderToProviderNotDonePatientReason' WHERE Name = 'CommunicationPatientReason' AND ExternalSystemId = 23
