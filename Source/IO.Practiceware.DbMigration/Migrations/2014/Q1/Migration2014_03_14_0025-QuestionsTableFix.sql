﻿IF OBJECT_ID(N'model.Questions','U') IS NOT NULL
BEGIN
	IF EXISTS (SELECT 1 FROM sys.triggers WHERE Name = 'AuditQuestionsDeleteTrigger') DROP TRIGGER model.AuditQuestionsDeleteTrigger
	IF EXISTS (SELECT 1 FROM sys.triggers WHERE Name = 'AuditQuestionsInsertTrigger') DROP TRIGGER model.AuditQuestionsInsertTrigger
	IF EXISTS (SELECT 1 FROM sys.triggers WHERE Name = 'AuditQuestionsUpdateTrigger') DROP TRIGGER model.AuditQuestionsUpdateTrigger
	
	IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('model.Questions','U') AND name = 'Label')
	BEGIN 
	EXEC sp_RENAME 'model.Questions.Name' , 'Label', 'COLUMN'
	END

	DECLARE @sql NVARCHAR(MAX)
	SET @sql = ''
	SELECT @sql = @sql + 'ALTER TABLE [model].[Questions] DROP CONSTRAINT ' + QUOTENAME(name) FROM sys.default_constraints WHERE parent_object_id = OBJECT_ID('model.Questions','U')
	EXEC sp_executesql @sql

	ALTER TABLE model.Questions DROP COLUMN OrdinalId
END
GO