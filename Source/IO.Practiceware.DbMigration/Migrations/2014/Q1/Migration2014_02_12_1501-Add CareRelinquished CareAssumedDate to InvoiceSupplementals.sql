﻿
IF NOT EXISTS (
SELECT 
COLUMN_NAME
FROM information_schema.COLUMNS 
WHERE TABLE_SCHEMA = 'model'
AND TABLE_NAME ='InvoiceSupplementals'
AND COLUMN_NAME = 'AssumedCareDateTime'
AND COLUMNPROPERTY(OBJECT_ID(N'model.InvoiceSupplementals'),COLUMN_NAME,'IsIdentity') = 0
AND DATA_TYPE = 'datetime'
AND IS_NULLABLE = 'YES'
) 
ALTER TABLE model.InvoiceSupplementals
ADD AssumedCareDateTime datetime NULL



IF NOT EXISTS (
SELECT 
COLUMN_NAME
FROM information_schema.COLUMNS 
WHERE TABLE_SCHEMA = 'model'
AND TABLE_NAME ='InvoiceSupplementals'
AND COLUMN_NAME = 'RelinquishedCareDateTime'
AND COLUMNPROPERTY(OBJECT_ID(N'model.InvoiceSupplementals'),COLUMN_NAME,'IsIdentity') = 0
AND DATA_TYPE = 'datetime'
AND IS_NULLABLE = 'YES'
) 
ALTER TABLE model.InvoiceSupplementals
ADD RelinquishedCareDateTime datetime NULL
