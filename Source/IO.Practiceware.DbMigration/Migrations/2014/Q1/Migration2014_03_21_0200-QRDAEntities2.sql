﻿IF NOT EXISTS(SELECT * FROM sys.columns WHERE name = 'OrganStructureId' AND object_id = OBJECT_ID('model.ClinicalConditions'))
BEGIN
	ALTER TABLE model.ClinicalConditions ADD OrganStructureId int NULL
	
	ALTER TABLE [model].[ClinicalConditions]
	ADD CONSTRAINT [FK_ClinicalConditionOrganStructure]
		FOREIGN KEY ([OrganStructureId])
		REFERENCES [model].[OrganStructures]
			([Id])
		ON DELETE NO ACTION ON UPDATE NO ACTION;
END

IF EXISTS(SELECT * FROM sys.columns WHERE name = 'Property1' AND object_id = OBJECT_ID('model.PatientExamPerformeds'))
	ALTER TABLE model.PatientExamPerformeds DROP COLUMN Property1
