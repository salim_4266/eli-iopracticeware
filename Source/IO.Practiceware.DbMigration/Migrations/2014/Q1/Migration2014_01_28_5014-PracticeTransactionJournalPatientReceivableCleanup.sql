﻿IF OBJECT_ID('UpdateBillingServiceTransactionStatus','TR') IS NOT NULL
BEGIN
	ALTER TABLE dbo.PracticeTransactionJournal DISABLE TRIGGER UpdateBillingServiceTransactionStatus
END

;WITH CTE AS (
SELECT 
ptj.TransactionId
,ptj.TransactionTypeId 
,ir2.Id
FROM dbo.PracticeTransactionJournal ptj
INNER JOIN model.InvoiceReceivables ir ON ir.Id = ptj.TransactionTypeId
	AND ir.PatientInsuranceId IS NULL
LEFT JOIN model.InvoiceReceivables ir2 ON ir2.InvoiceId = ir.InvoiceId
	AND ir2.PatientInsuranceId IS NOT NULL
LEFT JOIN model.PatientInsurances pi ON pi.Id = ir2.PatientInsuranceId
LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
WHERE ir.InvoiceId = ir2.InvoiceId
AND TransactionRef = 'P'
AND ip.StartDateTime <= CONVERT(DATETIME,ptj.TransactionDate)
AND (pi.EndDateTime >= CONVERT(DATETIME,ptj.TransactionDate) OR pi.EndDateTime IS NULL)
)
UPDATE ptj
SET ptj.TransactionTypeId = CTE.Id
FROM dbo.PracticeTransactionJournal ptj
INNER JOIN CTE ON CTE.TransactionId = ptj.TransactionId

IF OBJECT_ID('UpdateBillingServiceTransactionStatus','TR') IS NOT NULL
BEGIN
	ALTER TABLE dbo.PracticeTransactionJournal ENABLE TRIGGER UpdateBillingServiceTransactionStatus
END