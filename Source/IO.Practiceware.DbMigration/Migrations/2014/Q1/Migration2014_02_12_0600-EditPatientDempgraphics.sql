﻿IF NOT EXISTS (SELECT Id FROM [model].[Permissions] WHERE Id = 43)
BEGIN
	SET IDENTITY_INSERT [model].[Permissions] ON
	INSERT INTO [model].[Permissions] (Id, Name, PermissionCategoryId) VALUES (43, 'Edit patient demographics tab', 6)
	SET IDENTITY_INSERT [model].[Permissions] OFF

	INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)

	SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 107, 1) = 1 -- epPatientInfo
				THEN CONVERT(bit,0)
				ELSE CONVERT(bit,1)
		   END AS IsDenied, 
		   re.ResourceId AS UserId, pe.Id AS PermissionId
	FROM dbo.Resources re
	INNER JOIN model.Permissions pe ON pe.Id > 0
	WHERE ResourceType <> 'R'
	AND pe.Id = 43 -- 'EditPatientDemographics'
END

GO
