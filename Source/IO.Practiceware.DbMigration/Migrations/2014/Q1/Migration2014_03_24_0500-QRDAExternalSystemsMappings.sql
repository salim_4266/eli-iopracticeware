﻿DECLARE @QualityDataModel INT
SELECT @QualityDataModel = Id FROM model.ExternalSystems WHERE Name = 'QualityDataModel'
IF (@QualityDataModel IS NOT NULL) AND (ISNUMERIC(@QualityDataModel) = 1)
BEGIN
	IF NOT EXISTS (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'EncounterService')
	BEGIN
		INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName)
		SELECT
		63 AS Id
		,'EncounterService' AS Name
		,'Id' AS KeyPropertyName
	END

	IF NOT EXISTS (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Question')
	BEGIN		
		INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName)
		SELECT
		68 AS Id
		,'Question' AS Name
		,'Id' AS KeyPropertyName		
	END

	IF NOT EXISTS (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Choice')
	BEGIN		
		INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName)
		SELECT
		69 AS Id
		,'Choice' AS Name
		,'Id' AS KeyPropertyName		
	END

	SELECT 
	OID
	,e.Id
	INTO #ValueSetMapper
	FROM (
		SELECT
		OID
		,CASE 
			WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','') = 'AttributeLevelOfSeverityOfRetinopathyFindings'
				THEN 'AttributeResultLevelOfSeverityOfRetinopathyFindings'
			WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','') = 'AttributeMacularEdemaFindingsAbsent'
				THEN 'AttributeResultMacularEdemaFindingsAbsent'
			WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','') = 'AttributeMacularEdemaFindingsPresent'
				THEN 'AttributeResultMacularEdemaFindingsPresent'
			WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','') = 'AttributeNegativeFinding'
				THEN 'AttributeResultNegativeFinding'
			WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','') = 'AttributeOverweight'
				THEN 'AttributeReasonOverweight'
			WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','') = 'AttributeUnderweight'
				THEN 'AttributeReasonUnderweight'
			WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','') = 'CommunicationConsultantReport'
				THEN 'CommunicationFromProviderToProviderConsultantReport'
			WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','') = 'CommunicationLevelOfSeverityOfRetinopathyFindings'
				THEN 'CommunicationFromProviderToProviderLevelOfSeverityOfRetinopathyFindings'
			WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','') = 'CommunicationMacularEdemaFindingsAbsent'
				THEN 'CommunicationFromProviderToProviderMacularEdemaFindingsAbsent'
			WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','') = 'CommunicationMacularEdemaFindingsPresent'
				THEN 'CommunicationFromProviderToProviderMacularEdemaFindingsPresent'
			WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','') = 'CommunicationMedicalReason'
				THEN 'CommunicationFromProviderToProviderNotDoneMedicalReason'
			WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','') = 'CommunicationPatientReason'
				THEN 'CommunicationFromProviderToProviderNotDonePatientReason'
			ELSE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(QDMCategory+dbo.CamelCase(v.ActualValueSet),' ',''),'-',''),',',''),'&','And'),'/',''),'(poag)','')
		END AS Name
		FROM qdm.ValueSets v
		UNION ALL
		SELECT '2.16.840.1.113883.3.526.3.1488' AS OID, 'PhysicalExamFindingBestCorrectedVisualAcuity' AS Name
	)v
	INNER JOIN model.ExternalSystemEntities e ON e.Name = v.Name

	IF OBJECT_ID(N'model.Gender_Enumeration') IS NULL
	BEGIN
		EXEC('
		CREATE VIEW model.Gender_Enumeration
		AS 
		SELECT 1 AS Id, ''Unknown'' AS Name
		UNION ALL
		SELECT 2 AS Id, ''Female'' AS Name
		UNION ALL
		SELECT 3 AS Id, ''Male'' AS Name
		UNION ALL
		SELECT 4 AS Id, ''Other'' AS Name
		')
	END

	IF OBJECT_ID(N'model.Insurers', 'V') IS NULL
	BEGIN
		EXEC('
		CREATE VIEW model.Insurers
		AS 
		SELECT InsurerId AS Id
		,CASE 
			WHEN InsurerReferenceCode = ''P''
				THEN 1
			WHEN InsurerReferenceCode = ''K''
				THEN 2
			WHEN InsurerReferenceCode = ''X''
				THEN 4
			WHEN InsurerReferenceCode = ''F''
				THEN 5
			WHEN InsurerReferenceCode = ''G''
				THEN 6
			WHEN InsurerReferenceCode = ''I''
				THEN 7
			WHEN InsurerReferenceCode = ''Q''
				THEN 8
			WHEN InsurerReferenceCode = ''C''
				THEN 9
			WHEN InsurerReferenceCode IN (''N'', ''M'', ''['')
				THEN 10
			WHEN InsurerReferenceCode = ''Z''
				THEN 13
			WHEN InsurerReferenceCode = ''Y''
				THEN 14
			WHEN InsurerReferenceCode = ''L''
				THEN 15
			WHEN InsurerReferenceCode = ''A''
				THEN 16
			WHEN InsurerReferenceCode = ''T''
				THEN 17
			WHEN InsurerReferenceCode = ''H''
				THEN 18
			WHEN InsurerReferenceCode = ''V''
				THEN 19
			WHEN InsurerReferenceCode = ''U''
				THEN 20
			WHEN InsurerReferenceCode = ''W''
				THEN 21
			ELSE 3
			END AS ClaimFormatTypeId
		,pctClaimReceiver.Id AS ClaimFileReceiverId
		,MedicareCrossOverId AS MedigapCode
		,IsMedicareAdvantage AS IsMedicareAdvantage
		,Hpid AS Hpid
		,Oeid AS Oeid
		,Website AS Website
		,InsurerGroupName AS GroupName
		,CASE
			WHEN pctBusClass.Code = ''AMERIH''
				THEN 1
			WHEN pctBusClass.Code = ''BLUES''
				THEN 2
			WHEN pctBusClass.Code = ''COMM''
				THEN 3
			WHEN pctBusClass.Code = ''MCAIDFL''
				THEN 4
			WHEN pctBusClass.Code = ''MCAIDNC''
				THEN 5
			WHEN pctBusClass.Code = ''MCAIDNJ''
				THEN 6
			WHEN pctBusClass.Code = ''MCAIDNV''
				THEN 7
			WHEN pctBusClass.Code = ''MCAIDNY''
				THEN 8
			WHEN pctBusClass.Code = ''MCARENJ''
				THEN 9
			WHEN pctBusClass.Code = ''MCARENV''
				THEN 10
			WHEN pctBusClass.Code = ''MCARENY''
				THEN 11
			WHEN pctBusClass.Code = ''TEMPLATE''
				THEN 12
			WHEN pctBusClass.Code IS NULL
				THEN 3
			WHEN pctBusClass.Code = ''''
				THEN 3
			ELSE pctBusClass.Id + 1000
		END AS InsurerBusinessClassId
		,InsurerName AS [Name]
		,pctPayType.Id AS InsurerPayTypeId
		,pctPlanType.Id AS InsurerPlanTypeId
		,AllowDependents AS AllowDependents
		,CASE ReferralRequired
			WHEN ''Y''
				THEN CONVERT(BIT, 1)
			ELSE CONVERT(BIT, 0)
			END AS IsReferralRequired
		,CASE Availability
			WHEN ''A''
				THEN CONVERT(BIT, 1)
			ELSE CONVERT(BIT, 0)
			END AS IsSecondaryClaimPaper
		,st.Id AS JurisdictionStateorProvinceId
		,NeicNumber AS PayerCode
		,InsurerPlanName AS PlanName
		,InsurerPlanFormat AS PolicyNumberFormat
		,CASE InsurerServiceFilter
			WHEN ''N''
				THEN CONVERT(BIT, 0)
			ELSE CONVERT(BIT, 1)
			END AS SendZeroCharge
		,CONVERT(nvarchar(max), InsurerComment) AS Comment
		,IsArchived AS IsArchived
	FROM dbo.PracticeInsurers pri
	LEFT JOIN dbo.PracticeCodeTable pctBusClass ON pctBusClass.Code = pri.InsurerBusinessClass
		AND pctBusClass.ReferenceType = ''BUSINESSCLASS''
	LEFT JOIN dbo.PracticeCodeTable pctClaimReceiver ON SUBSTRING(pctClaimReceiver.Code, 1, 1) = pri.InsurerTFormat
		AND pctClaimReceiver.ReferenceType = ''TRANSMITTYPE''
	LEFT JOIN dbo.PracticeCodeTable pctPayType ON SUBSTRING(pctPayType.Code, 1, 2) = pri.InsurerPayType
		AND pctPayType.ReferenceType = ''INSURERPTYPE''
	LEFT JOIN dbo.PracticeCodeTable pctPlanType ON pctPlanType.Code = pri.InsurerPlanType
		AND pctPlanType.ReferenceType = ''PLANTYPE''
	LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = pri.StateJurisdiction
		')
	END

	INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId,PracticeRepositoryEntityKey,ExternalSystemEntityId,ExternalSystemEntityKey)
	SELECT * FROM (
	--	-- maps all model.EncounterServices to QDM Category Encounter
	--	SELECT 
	--	(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'EncounterService') AS PracticeRepositoryEntityId
	--	,CONVERT(NVARCHAR(MAX),es.Id) AS PracticeRepositoryEntityKey
	--	,e.Id AS ExternalSystemEntityId
	--	,CONVERT(NVARCHAR(MAX),m.Id) AS ExternalSystemEntityKey
	--	FROM qdm.MappingSets m
	--	INNER JOIN #ValueSetMapper e ON e.OID = m.[Value Set OID]
	--	INNER JOIN model.EncounterServices es ON es.Code = m.Code
	--	WHERE [QDM Category] = 'Encounter'
	--	AND [Code System] = 'CPT'

	--	UNION ALL

		-- maps all model.Gender_Enumeration to QDM category Individual Characteristic, ONC Administrative Sex
		SELECT 
		(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Gender') AS PracticeRepositoryEntityId
		,CONVERT(NVARCHAR(MAX),g.Id) AS PracticeRepositoryEntityKey
		,e.Id AS ExternalSystemEntityId
		,CONVERT(NVARCHAR(MAX),m.Id) AS ExternalSystemEntityKey
		FROM qdm.MappingSets m
		INNER JOIN #ValueSetMapper e ON e.OID = m.[Value Set OID]
		INNER JOIN model.Gender_Enumeration g ON g.Name = m.Description
		WHERE [QDM Category] = 'Individual Characteristic'
		AND [Code System] = 'AdministrativeSex'
		AND [Value Set Name] = 'ONC Administrative Sex'

		UNION ALL

		-- maps all model.Ethnicities to QDM category Individual Characteristic, Ethnicity
		SELECT 
		(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Ethnicity') AS PracticeRepositoryEntityId
		,CONVERT(NVARCHAR(MAX),eth.Id) AS PracticeRepositoryEntityKey
		,e.Id AS ExternalSystemEntityId
		,CONVERT(NVARCHAR(MAX),m.Id) AS ExternalSystemEntityKey
		FROM qdm.MappingSets m
		INNER JOIN #ValueSetMapper e ON e.OID = m.[Value Set OID]
		INNER JOIN model.Ethnicities eth ON eth.Name = m.Description
		WHERE [QDM Category] = 'Individual Characteristic'
		AND [Code System] = 'CDCREC'
		AND [Value Set Name] = 'Ethnicity'

		UNION ALL

		-- maps all model.Ethnicities to QDM category Individual Characteristic, Races
		SELECT 
		(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Race') AS PracticeRepositoryEntityId
		,CONVERT(NVARCHAR(MAX),ra.Id) AS PracticeRepositoryEntityKey
		,e.Id AS ExternalSystemEntityId
		,CONVERT(NVARCHAR(MAX),m.Id) AS ExternalSystemEntityKey
		FROM qdm.MappingSets m
		INNER JOIN #ValueSetMapper e ON e.OID = m.[Value Set OID]
		INNER JOIN model.Races ra ON ra.Name = m.Description
		WHERE [QDM Category] = 'Individual Characteristic'
		AND [Code System] = 'CDCREC'
		AND [Value Set Name] = 'Race'

		UNION ALL

		-- maps all model.Insurers to QDM category Individual Characteristic, Payers
		SELECT 
		(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Insurer') AS PracticeRepositoryEntityId
		,CONVERT(NVARCHAR(MAX),i.Id) AS PracticeRepositoryEntityKey
		,e.Id AS ExternalSystemEntityId
		,CONVERT(NVARCHAR(MAX),m.Id) AS ExternalSystemEntityKey
		FROM model.Insurers i
		LEFT JOIN qdm.MappingSets m ON m.Code = CASE 
				WHEN Name LIKE 'Medicare%POS%'
					THEN 113
				WHEN Name LIKE 'Medicare%HMO%'
					THEN 111
				WHEN Name LIKE 'Medicare%PPO%'
					THEN 112
				WHEN Name LIKE 'Medicare%'
					THEN 1
				WHEN Name LIKE 'Medicaid%HMO%'
					THEN 211
				WHEN Name LIKE 'Medicaid%PPO%'
					THEN 212
				WHEN Name LIKE 'Medicaid%'
					THEN 2
				WHEN Name LIKE '%Tricare%Champus%' 
					THEN 311
				WHEN Name LIKE '%Federal%' AND Name NOT LIKE '%Blue%Cross%' AND Name NOT LIKE '%Blue%Shield%' AND Name NOT LIKE '%BCBS%' AND Name NOT LIKE '%BC_BS%'
					THEN 3
				WHEN ((Name LIKE '%Blue%Cross%' OR Name LIKE '%Blue%Shield%' OR Name LIKE '%BCBS%' OR Name LIKE '%BC_BS%') AND Name LIKE '%HMO%')
					THEN 611
				WHEN ((Name LIKE '%Blue%Cross%' OR Name LIKE '%Blue%Shield%' OR Name LIKE '%BCBS%' OR Name LIKE '%BC_BS%') AND Name LIKE '%PPO%')
					THEN 612
				WHEN ((Name LIKE '%Blue%Cross%' OR Name LIKE '%Blue%Shield%' OR Name LIKE '%BCBS%' OR Name LIKE '%BC_BS%') AND Name LIKE '%POS%')
					THEN 613
				WHEN Name LIKE '%Blue%Cross%' OR Name LIKE '%Blue%Shield%' OR Name LIKE '%BCBS%' OR Name LIKE '%BC_BS%' OR Name LIKE '%Horizon[^s]%' 
					THEN 6
				WHEN Name LIKE '%Self%Pay%'
					THEN 8
				WHEN Name LIKE '%ZZZ%'
					THEN 9
				WHEN i.InsurerPlanTypeId = (SELECT Id FROM (SELECT Id AS Id,
		Code AS [Name]
	FROM dbo.PracticeCodeTable
	WHERE ReferenceType = 'PLANTYPE')v WHERE Name = 'HMO')
					THEN 511
				WHEN i.InsurerPlanTypeId = (SELECT Id FROM (SELECT Id AS Id,
		Code AS [Name]
	FROM dbo.PracticeCodeTable
	WHERE ReferenceType = 'PLANTYPE')v WHERE Name = 'PPO')
					THEN 512
				WHEN i.InsurerPlanTypeId = (SELECT Id FROM (SELECT Id AS Id,
		Code AS [Name]
	FROM dbo.PracticeCodeTable
	WHERE ReferenceType = 'PLANTYPE')v WHERE Name = 'POS')
					THEN 513
				ELSE 5 --PRIVATE HEALTH INSURANCE
			END
		AND m.[Value Set Name] = 'Payer'
		AND [Code System] = 'SOP'
		AND [QDM Category] = 'Individual Characteristic'
		INNER JOIN #ValueSetMapper e ON e.OID = m.[Value Set OID]
		WHERE i.Id > 0

	--	UNION ALL

	--	-- maps all model.ClinicalProcedures to QDM category Procedure
	--	SELECT 
	--	(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalProcedure') AS PracticeRepositoryEntityId
	--	,CONVERT(NVARCHAR(MAX),cp.Id) AS PracticeRepositoryEntityKey
	--	,e.Id AS ExternalSystemEntityId
	--	,CONVERT(NVARCHAR(MAX),m.Id) AS ExternalSystemEntityKey
	--	FROM df.Questions qu
	--	CROSS APPLY dbo.GetRowFromCommaSeparatedField(ControlName) z
	--	INNER JOIN model.ClinicalProcedures cp ON cp.Name = qu.Question
	--	INNER JOIN qdm.MappingSets m ON m.[QDM Category] = 'Procedure' 
	--		AND m.[Code System] = 'CPT'
	--		AND m.Code = z.someValue
	--	INNER JOIN #ValueSetMapper e ON e.OID = m.[Value Set OID]
	--	WHERE QuestionParty NOT IN ('I','P')
	--		AND ControlName IS NOT NULL

	--	UNION ALL
		
	--	-- maps model.Medications to QDM Categories Medication, Active and Medication, Order
	--	SELECT 
	--	(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Medication') AS PracticeRepositoryEntityId
	--	,CONVERT(NVARCHAR(MAX),esem.PracticeRepositoryEntityKey) AS PracticeRepositoryEntityKey
	--	,e.Id AS ExternalSystemEntityId
	--	,CONVERT(NVARCHAR(MAX),m.Id) AS ExternalSystemEntityKey
	--	FROM model.ExternalSystems es
	--	INNER JOIN model.ExternalSystemEntities ese ON es.Id = ese.ExternalSystemId
	--	INNER JOIN model.ExternalSystemEntityMappings esem ON esem.ExternalSystemEntityId = ese.Id
	--	INNER JOIN qdm.MappingSets m ON m.[QDM Category] = ('Medication')
	--		AND m.[Code System] = 'RXNorm'
	--	INNER JOIN #ValueSetMapper e ON e.OID = m.[Value Set OID]
	--	WHERE es.Name = 'Rxnorm' AND ese.Name = 'Medication'
	--		AND esem.ExternalSystemEntityKey = m.Code
		
		--UNION ALL

		----Medications needed for the test, not mapped 
		--SELECT 
		--(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Medication') AS PracticeRepositoryEntityId
		--,CONVERT(NVARCHAR(MAX),med.Id) AS PracticeRepositoryEntityKey
		--,e.Id AS ExternalSystemEntityId
		--,CONVERT(NVARCHAR(MAX),m.Id) AS ExternalSystemEntityKey
		--FROM model.Medications med
		--INNER JOIN qdm.MappingSets m ON m.[QDM Category] = ('Medication')
		--	AND m.[Code System] = 'RXNorm'
		--INNER JOIN #ValueSetMapper e ON e.OID = m.[Value Set OID]
		--WHERE med.Name = 'conjugated estrogens tablet'
		--	AND m.Id = '52645'

		--UNION ALL

		--SELECT 
		--(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Medication') AS PracticeRepositoryEntityId
		--,CONVERT(NVARCHAR(MAX),med.Id) AS PracticeRepositoryEntityKey
		--,e.Id AS ExternalSystemEntityId
		--,CONVERT(NVARCHAR(MAX),m.Id) AS ExternalSystemEntityKey
		--FROM model.Medications med
		--INNER JOIN qdm.MappingSets m ON m.[QDM Category] = ('Medication')
		--	AND m.[Code System] = 'RXNorm'
		--INNER JOIN #ValueSetMapper e ON e.OID = m.[Value Set OID]
		--WHERE med.Name = 'nicotine Daily Transderm Patch,Sequential'
		--	AND m.Id = '69321'

	--	UNION ALL

	--	-- maps all ClinicalConditions to QDM category Condition/Diagnosis/Problem
	--	SELECT 
	--	(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalCondition') AS PracticeRepositoryEntityId
	--	,CONVERT(NVARCHAR(MAX),c.Id) AS PracticeRepositoryEntityKey
	--	,e.Id AS ExternalSystemEntityId
	--	,CONVERT(NVARCHAR(MAX),m.Id) AS ExternalSystemEntityKey
	--	FROM qdm.MappingSets m
	--	INNER JOIN (SELECT 
	--	Id
	--	,CASE 
	--		WHEN Name = 'Type 1 diabetes mellitus with hyperosmolar coma'
	--			THEN '190330002'
	--		WHEN Name = 'Advanced open-angle glaucoma'
	--			THEN '111513000'
	--		WHEN Name = 'Precipitate labor and delivery'
	--			THEN 'O62.3'
	--		WHEN Name = 'Benign hypertension'
	--			THEN '10725009'
	--		WHEN Name = 'Diabetic retinopathy'
	--			THEN '4855003'
	--	END AS SnomedCode
	--	FROM model.ClinicalConditions
	--	)c ON c.SnomedCode = m.Code
	--	INNER JOIN #ValueSetMapper e ON e.OID = m.[Value Set OID]
	--	WHERE Code IN (
	--	'190330002',
	--	'4855003',
	--	'111513000',
	--	'10725009',
	--	'O62.3')

	--	UNION ALL
		
	--	-- maps the ClinicalConditions "Advanced diabetic maculopathy" to: Attribute, Communication, Condition/Diagnosis/Problem
	--	SELECT 
	--	(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalCondition') AS PracticeRepositoryEntityId
	--	,CONVERT(NVARCHAR(MAX),c.Id) AS PracticeRepositoryEntityKey
	--	,e.Id AS ExternalSystemEntityId
	--	,CONVERT(NVARCHAR(MAX),m.Id) AS ExternalSystemEntityKey
	--	FROM qdm.MappingSets m
	--	INNER JOIN (SELECT Id, '193350004' AS SnomedCode FROM model.ClinicalConditions WHERE Name = 'Advanced diabetic maculopathy')c ON c.SnomedCode = m.Code
	--	INNER JOIN #ValueSetMapper e ON e.OID = m.[Value Set OID]

	--	UNION ALL

	--	-- maps the ClinicalProcedure BLOOD PRESSURE to Systolic Blood Pressure, Diastolic Blood Pressure
	--	SELECT 
	--	(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalProcedure') AS PracticeRepositoryEntityId
	--	,CONVERT(NVARCHAR(MAX),c.Id) AS PracticeRepositoryEntityKey
	--	,e.Id AS ExternalSystemEntityId
	--	,CONVERT(NVARCHAR(MAX),q.Id) AS ExternalSystemEntityKey
	--	FROM qdm.MappingSets q
	--	INNER JOIN #ValueSetMapper e ON q.[Value Set OID] = e.OID
	--	INNER JOIN model.ClinicalProcedures c ON c.Name = 'BLOOD PRESSURE'
	--	WHERE Code IN ('8462-4','8480-6')

	--	UNION ALL

	--	--maps the EncounterCommunicationWithOtherProviderOrder to QDM Category Communication
	--	SELECT 
	--	(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'EncounterCommunicationWithOtherProviderOrder') AS PracticeRepositoryEntityId
	--	,CONVERT(NVARCHAR(MAX),c.Id) AS PracticeRepositoryEntityKey
	--	,e.Id AS ExternalSystemEntityId
	--	,CONVERT(NVARCHAR(MAX),q.Id) AS ExternalSystemEntityKey
	--	FROM qdm.MappingSets q
	--	INNER JOIN #ValueSetMapper e ON q.[Value Set OID] = e.OID
	--	INNER JOIN model.EncounterCommunicationWithOtherProviderOrders c ON 1=1
	--	WHERE [QDM Category] = 'Communication'
	--		AND Code IN ('193349004','428341000124108')

	--	UNION ALL

	--	-- maps the ClinicalProcedure OCT, OPTIC NERVE to Diagnostic Study Optic Disc Exam for Structural Abnormalities
	--	SELECT
	--	(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalProcedure') AS PracticeRepositoryEntityId
	--	,CONVERT(NVARCHAR(MAX),c.Id) AS PracticeRepositoryEntityKey
	--	,e.Id AS ExternalSystemEntityId
	--	,CONVERT(NVARCHAR(MAX),q.Id) AS ExternalSystemEntityKey
	--	FROM qdm.MappingSets q
	--	INNER JOIN #ValueSetMapper e ON q.[Value Set OID] = e.OID
	--	INNER JOIN model.ClinicalProcedures c ON c.Name = 'OCT, OPTIC NERVE'
	--	WHERE Code = '71486-5'

	--	UNION ALL

	--	SELECT
	--	(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalProcedure') AS PracticeRepositoryEntityId
	--	,CONVERT(NVARCHAR(MAX),c.Id) AS PracticeRepositoryEntityKey
	--	,e.Id AS ExternalSystemEntityId
	--	,CONVERT(NVARCHAR(MAX),q.Id) AS ExternalSystemEntityKey
	--	FROM qdm.MappingSets q
	--	INNER JOIN #ValueSetMapper e ON q.[Value Set OID] = e.OID
	--	INNER JOIN model.ClinicalProcedures c ON c.Name = 'MACULAR EXAM'
	--	WHERE Code = '32451-7'

	)v

	EXCEPT

	SELECT PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey FROM model.ExternalSystemEntityMappings

	IF OBJECT_ID(N'model.Gender_Enumeration', 'V') IS NOT NULL
	BEGIN
		DROP VIEW model.Gender_Enumeration
	END

END
GO

