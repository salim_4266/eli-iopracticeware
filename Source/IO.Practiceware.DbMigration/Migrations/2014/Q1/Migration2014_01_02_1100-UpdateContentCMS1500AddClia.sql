﻿IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	
	DECLARE @old1 VARCHAR(max), @new1 VARCHAR(max)
	SET @old1 = '<input type="text" id="AuthorizationCode" value="@Model.AuthorizationCode" style="z-index: 1; left: @(col3Left)px; top: @(row16Top)px; position: absolute; width: 264px; height: 13px;"/>'
				
	SET @new1 = '@if (!string.IsNullOrEmpty(Model.AuthorizationCode))
                {
                    <input type="text" id="AuthorizationCode" value="@Model.AuthorizationCode" style="z-index: 1; left: @(col3Left)px; top: @(row16Top)px; position: absolute; width: 264px; height: 13px;"/>
                }
                else if(Model.Clia != null && Model.Clia != string.Empty)
                {
                    <input type="text" id="Clia" value="@Model.Clia" style="z-index: 1; left: @(col3Left)px; top: @(row16Top)px; position: absolute; width: 264px; height: 13px;"/>
                }'
	
	UPDATE model.TemplateDocuments 	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), @old1, @new1)) 
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL		
	AND (CONVERT(varbinary(max),Content) NOT LIKE '%<input type="text" id="Clia" value="@Model.Clia"%')

END
GO

IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	
	DECLARE @old2 VARCHAR(max), @new2 VARCHAR(max)
	SET @old2 = '<input type="text" id="AuthorizationCode" value="@Model.AuthorizationCode" style="z-index: 1; left: @(col3Left)px; top: @(row16Top)px; position: absolute; width: 264px; height: 13px;" />'
				
	SET @new2 = '@if (!string.IsNullOrEmpty(Model.AuthorizationCode))
                {
                    <input type="text" id="AuthorizationCode" value="@Model.AuthorizationCode" style="z-index: 1; left: @(col3Left)px; top: @(row16Top)px; position: absolute; width: 264px; height: 13px;" />
                }
                else if(Model.Clia != null && Model.Clia != string.Empty)
                {
                    <input type="text" id="Clia" value="@Model.Clia" style="z-index: 1; left: @(col3Left)px; top: @(row16Top)px; position: absolute; width: 264px; height: 13px;"/>
                }'
	
	UPDATE model.TemplateDocuments 	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), @old2, @new2)) 
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL		
	AND (CONVERT(varbinary(max),Content) NOT LIKE '%<input type="text" id="Clia" value="@Model.Clia"%')

END
GO
 