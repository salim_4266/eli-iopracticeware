﻿IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	
	UPDATE model.TemplateDocuments 	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 
	'<input type="text" id="ServiceCode" value="@service.ServiceCode"',
	'<input type="text" id="ServiceCode" value="@(string.IsNullOrEmpty(service.ServiceCode) ? "" : service.ServiceCode.Substring(0,5))"' )) 
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL	
END

