﻿IF NOT EXISTS (SELECT * FROM sys.columns WHERE Name = 'LegacyClinicalDataSourceTypeId' AND object_id = OBJECT_ID(N'dbo.PatientClinical','U'))
BEGIN
	ALTER TABLE dbo.PatientClinical
	ADD  LegacyClinicalDataSourceTypeId INT NULL
END
GO