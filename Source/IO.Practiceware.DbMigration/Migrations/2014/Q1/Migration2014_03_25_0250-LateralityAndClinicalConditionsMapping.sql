﻿INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId,PracticeRepositoryEntityKey,ExternalSystemEntityId,ExternalSystemEntityKey)
SELECT 
PracticeRepositoryEntityId
,CONVERT(NVARCHAR(MAX),PracticeRepositoryEntityKey) AS PracticeRepositoryEntityKey
,ExternalSystemEntityId
,CONVERT(NVARCHAR(MAX),ExternalSystemEntityKey) AS ExternalSystemEntityKey
FROM (
SELECT 
(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Laterality') AS PracticeRepositoryEntityId
,1 AS PracticeRepositoryEntityKey --Right
,(SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'ClinicalQualifier' AND ExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'SnomedCt')) AS ExternalSystemEntityId
,24028007 AS ExternalSystemEntityKey
UNION ALL
SELECT 
(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Laterality') AS PracticeRepositoryEntityId
,2 AS PracticeRepositoryEntityKey --Left
,(SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'ClinicalQualifier' AND ExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'SnomedCt')) AS ExternalSystemEntityId
,7771000 AS ExternalSystemEntityKey
UNION ALL
SELECT 
(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Laterality') AS PracticeRepositoryEntityId
,3 AS PracticeRepositoryEntityKey --Both
,(SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'ClinicalQualifier' AND ExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'SnomedCt')) AS ExternalSystemEntityId
,51440002 AS ExternalSystemEntityKey
UNION ALL
SELECT 
(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalConditionStatus') AS PracticeRepositoryEntityId
,1 AS PracticeRepositoryEntityKey --Active
,(SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'ClinicalConditionStatus' AND ExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'SnomedCt')) AS ExternalSystemEntityId
,55561003 AS ExternalSystemEntityKey
UNION ALL
SELECT 
(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalConditionStatus') AS PracticeRepositoryEntityId
,2 AS PracticeRepositoryEntityKey --Inactive
,(SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'ClinicalConditionStatus' AND ExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'SnomedCt')) AS ExternalSystemEntityId
,73425007 AS ExternalSystemEntityKey
UNION ALL
SELECT 
(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalConditionStatus') AS PracticeRepositoryEntityId
,3 AS PracticeRepositoryEntityKey --Resolved
,(SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'ClinicalConditionStatus' AND ExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'SnomedCt')) AS ExternalSystemEntityId
,413322009 AS ExternalSystemEntityKey
)v 
EXCEPT
SELECT PracticeRepositoryEntityId,PracticeRepositoryEntityKey,ExternalSystemEntityId,ExternalSystemEntityKey FROM model.ExternalSystemEntityMappings