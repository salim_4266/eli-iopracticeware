﻿--This is from Migrations\2013\Q4\Migration2013_11_06_1600-ObjectsRequriedForMedicationView.sql

--Trigger to continually update.
IF OBJECT_ID('dbo.UpdateDrugDosageFormIdTrigger', 'TR') IS NOT NULL
DROP TRIGGER UpdateDrugDosageFormIdTrigger
GO


CREATE TRIGGER UpdateDrugDosageFormIdTrigger
ON dbo.Drug
FOR INSERT, UPDATE
NOT FOR REPLICATION
--This trigger maps drug dosage forms
AS
BEGIN
SET NOCOUNT ON
	
	UPDATE dru
	SET DrugDosageFormId = ddf.Id
	FROM dbo.Drug dru
	INNER JOIN fdb.tblCompositeDrug tb ON tb.MED_NAME = dru.DisplayName
	INNER JOIN model.DrugDosageForms ddf ON ddf.Name = tb.MED_DOSAGE_FORM_DESC
	INNER JOIN inserted i ON i.DrugId = dru.DrugId

END

