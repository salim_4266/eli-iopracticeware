﻿IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	
	UPDATE model.TemplateDocuments 	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 
	'@if (!string.IsNullOrEmpty(Model.AuthorizationCode))',
	'@if (!string.IsNullOrEmpty(Model.ReferralNumber))
                {
                    <input type="text" id="ReferralNumber" value="@Model.ReferralNumber" style="z-index: 1; left: @(col3Left)px; top: @(row16Top)px; position: absolute; width: 264px; height: 13px;" />
                }
                else if (!string.IsNullOrEmpty(Model.AuthorizationCode))' )) 
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL
	AND CONVERT(varbinary(max),Content) NOT LIKE '%(!string.IsNullOrEmpty(Model.ReferralNumber))%'		
END
GO


IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	
	UPDATE model.TemplateDocuments 	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 
	'@if (Model.AuthorizationCode != null && Model.AuthorizationCode != string.Empty)',
	'@if (!string.IsNullOrEmpty(Model.ReferralNumber))
                {
                    <input type="text" id="ReferralNumber" value="@Model.ReferralNumber" style="z-index: 1; left: @(col3Left)px; top: @(row16Top)px; position: absolute; width: 264px; height: 13px;" />
                }
                else if (!string.IsNullOrEmpty(Model.AuthorizationCode))' )) 
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL
	AND CONVERT(varbinary(max),Content) NOT LIKE '%(!string.IsNullOrEmpty(Model.ReferralNumber))%'	
END
GO
