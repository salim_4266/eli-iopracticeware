﻿IF OBJECT_ID(N'dbo.CamelCase','FN') IS NOT NULL
	DROP FUNCTION dbo.CamelCase
GO

CREATE FUNCTION [dbo].[CamelCase] (@Str VARCHAR(8000))
RETURNS VARCHAR(8000)
AS
BEGIN
	DECLARE @Result VARCHAR(2000)

	SET @Str = LOWER(@Str) + ' '
	SET @Result = ''

	WHILE 1 = 1
	BEGIN
		IF PATINDEX('% %', @Str) = 0
			BREAK

		SET @Result = @Result + UPPER(Left(@Str, 1)) + SubString(@Str, 2, CharIndex(' ', @Str) - 1)
		SET @Str = SubString(@Str, CharIndex(' ', @Str) + 1, Len(@Str))
	END

	SET @Result = Left(@Result, Len(@Result))

	RETURN @Result
END
GO

IF OBJECT_ID(N'dbo.GetRowFromCommaSeparatedField','TF') IS NOT NULL
	DROP FUNCTION dbo.GetRowFromCommaSeparatedField
GO

CREATE FUNCTION [dbo].[GetRowFromCommaSeparatedField] (@text NVARCHAR(MAX))
RETURNS @result TABLE (someValue NVARCHAR(MAX))

BEGIN
	DECLARE @x XML

	SELECT @x = CAST('<A>' + REPLACE(@text, ',', '</A><A>') + '</A>' AS XML)

	INSERT INTO @result (someValue)
	SELECT t.value('.', 'NVARCHAR(MAX)') AS inVal
	FROM @x.nodes('/A') AS x(t)

	RETURN
END