﻿/* Making Preferred Physician, Service Location, PCP and Signature Date fields IsRequiredconfigurable to true for Patient Demographics setup */

IF EXISTS (SELECT * FROM model.PatientDemographicsFieldConfigurations WHERE Id IN (22,23,24,27))
BEGIN
	UPDATE model.PatientDemographicsFieldConfigurations SET IsRequiredConfigurable = 1  WHERE Id IN (22,23,24,27)
END
GO
