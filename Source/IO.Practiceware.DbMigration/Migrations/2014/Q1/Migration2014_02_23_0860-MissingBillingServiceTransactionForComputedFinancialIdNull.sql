﻿CREATE TABLE #tempBst (
	[Id] [uniqueidentifier] NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[AmountSent] [decimal](18, 2) NOT NULL,
	[ClaimFileNumber] [int] NULL,
	[BillingServiceTransactionStatusId] [int] NOT NULL,
	[MethodSentId] [int] NOT NULL,
	[BillingServiceId] [int] NOT NULL,
	[InvoiceReceivableId] [bigint] NOT NULL,
	[ClaimFileReceiverId] [int] NULL,
	[ExternalSystemMessageId] [uniqueidentifier] NULL,
	[PatientAggregateStatementId] [int] NULL,
	[TransactionId] [int] NULL
)

INSERT INTO dbo.[BillingServiceTransactionsBackup] (Id, DateTime, AmountSent, ClaimFileNumber, 
	BillingServiceTransactionStatusId, MethodSentId, BillingServiceId, InvoiceReceivableId, ClaimFileReceiverId, 
	ExternalSystemMessageId, PatientAggregateStatementId, TransactionId)
	OUTPUT inserted.* INTO #tempBst
SELECT st.Id AS Id,
CONVERT(datetime, ptj.TransactionDate, 112) AS DateTime,
CONVERT(decimal(18,2), Amount) AS AmountSent,
NULL AS ClaimFileNumber,
CASE 
	WHEN ptj.TransactionStatus = 'P'
		AND TransportAction IN ('B', 'P')
		THEN 1
	WHEN ptj.TransactionStatus = 'P'
		AND TransportAction = 'V'
		THEN 3
	WHEN ptj.TransactionStatus = 'S'
		AND TransportAction = 'P'
		THEN 2
	WHEN ptj.TransactionStatus = 'S'
		AND TransportAction = 'V'
		THEN 3
	WHEN ptj.TransactionStatus = 'S'
		AND TransportAction = 'X'
		THEN 4
	WHEN ptj.TransactionStatus = 'Z'
		AND TransportAction = '1'
		THEN 6
	WHEN ptj.TransactionStatus = 'Z'
		AND TransportAction = '2'
		THEN 7
	ELSE 5
	END AS BillingServiceTransactionStatusId,
CASE 
	WHEN (ptj.TransactionRef = 'I' AND ptj.TransactionAction = 'B')
		THEN 1
	ELSE 2 
END AS MethodSentId,
ServiceId AS BillingServiceId,
ptj.InvoiceReceivableId,
NULL AS ClaimFileReceiverId,
st.ExternalSystemMessageId as ExternalSystemMessageId,
NULL AS PatientAggregateStatementId,
ptj.TransactionId
FROM (
	SELECT
	ptj.TransactionId
	,ptj.TransactionType
	,a.OldTransactionTypeId AS TransactionTypeId
	,ptj.TransactionStatus
	,ptj.TransactionDate
	,ptj.TransactionTime
	,ptj.TransactionRef
	,ptj.TransactionRemark
	,ptj.TransactionAction
	,ptj.TransactionBatch
	,ptj.TransactionServiceItem
	,ptj.TransactionRcvrId
	,ptj.TransactionAssignment
	,ir.Id AS InvoiceReceivableId
	FROM PracticeTransactionJournalReceivablesBackup a
	LEFT JOIN model.InvoiceReceivables ir ON ir.id = a.NewTransactionTypeId
	LEFT JOIN PracticeTransactionJournal ptj ON ptj.TransactionId = a.TransactionId
	WHERE ptj.TransactionType = 'R'
		AND TransactionRef <> 'G'
		AND ptj.TransactionTypeId = a.NewTransactionTypeId
) ptj
INNER JOIN dbo.ServiceTransactionsBackup st ON st.TransactionId = ptj.TransactionId
INNER JOIN dbo.PatientReceivableServices prs on prs.ItemId = st.ServiceId
	AND prs.[Status] = 'A'
INNER JOIN dbo.PatientReceivablesBackup pr ON pr.ReceivableId = ptj.TransactionTypeId	
	AND pr.PatientId = pr.InsuredId
	AND pr.InsurerId = 0
	AND pr.ComputedFinancialId IS NULL
WHERE ptj.TransactionType = 'R'
AND TransactionRef <> 'G'
GO

INSERT INTO model.[BillingServiceTransactions]
([DateTime]
	,AmountSent
	,ClaimFileNumber
	,BillingServiceTransactionStatusId
	,MethodSentId
	,BillingServiceId
	,InvoiceReceivableId
	,ClaimFileReceiverId
	,ExternalSystemMessageId
	,PatientAggregateStatementId
	,LegacyTransactionId)
SELECT 
bst.[DateTime]
,bst.AmountSent
,bst.ClaimFileNumber 
,bst.BillingServiceTransactionStatusId
,bst.MethodSentId
,bst.BillingServiceId
,bst.InvoiceReceivableId AS InvoiceReceivableId
,bst.ClaimFileReceiverId
,bst.ExternalSystemMessageId
,bst.PatientAggregateStatementId
,bst.TransactionId
FROM #tempBst bst