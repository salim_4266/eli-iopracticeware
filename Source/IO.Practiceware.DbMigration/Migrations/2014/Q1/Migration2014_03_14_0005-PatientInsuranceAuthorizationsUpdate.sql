﻿CREATE TRIGGER dbo.PatientInsuranceAuthorizationsUpdate
ON dbo.Appointments 
AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	;WITH CTE AS (
	SELECT 
	i.AppointmentId
	,i.PreCertId
	FROM inserted i 
	WHERE i.Comments <> 'ASC CLAIM'
	EXCEPT
	SELECT
	d.AppointmentId
	,d.PreCertId
	FROM deleted d
	WHERE d.Comments <> 'ASC CLAIM'
	)
	UPDATE ir
	SET ir.PatientInsuranceAuthorizationId = pia.Id
	FROM model.InvoiceReceivables ir 
	INNER JOIN model.Invoices i ON ir.InvoiceId = i.Id
	INNER JOIN dbo.Appointments a ON a.AppointmentId = i.EncounterId
	INNER JOIN model.PatientInsuranceAuthorizations pia ON pia.EncounterId = a.EncounterId
	INNER JOIN CTE c ON c.AppointmentId = a.AppointmentId
	WHERE ir.PatientInsuranceId = pia.PatientInsuranceId		
		AND i.InvoiceTypeId IN (1,3)

	;WITH CTE AS (
	SELECT 
	i.AppointmentId
	,i.PreCertId
	FROM inserted i 
	WHERE i.Comments = 'ASC CLAIM'
	EXCEPT
	SELECT
	d.AppointmentId
	,d.PreCertId
	FROM deleted d
	WHERE d.Comments = 'ASC CLAIM'
	)
	UPDATE ir
	SET ir.PatientInsuranceAuthorizationId = pia.Id
	FROM model.InvoiceReceivables ir 
	INNER JOIN model.Invoices i ON ir.InvoiceId = i.Id
	INNER JOIN dbo.Appointments a ON a.AppointmentId = i.EncounterId
	INNER JOIN model.PatientInsuranceAuthorizations pia ON pia.EncounterId = a.EncounterId
	INNER JOIN CTE c ON c.AppointmentId = a.AppointmentId
	WHERE ir.PatientInsuranceId = pia.PatientInsuranceId		
		AND i.InvoiceTypeId = 2 
END
GO