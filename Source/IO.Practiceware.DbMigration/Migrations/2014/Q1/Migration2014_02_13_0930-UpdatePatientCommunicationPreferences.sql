﻿-- Collections
UPDATE model.PatientCommunicationPreferences SET IsOptOut = 1 WHERE PatientCommunicationTypeId = 8 
AND PatientId IN (SELECT PatientId FROM dbo.PatientDemographics WHERE SendConsults = 'N')

-- Statements
UPDATE model.PatientCommunicationPreferences SET IsOptOut = 1 WHERE PatientCommunicationTypeId = 3 
AND PatientId IN (SELECT PatientId FROM dbo.PatientDemographics WHERE SendStatements = 'N')