﻿	IF NOT EXISTS (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Gender')
	BEGIN
		INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName)
		SELECT
		62 AS Id
		,'Gender' AS Name
		,'Id' AS KeyPropertyName
	END

	IF NOT EXISTS (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Ethnicity')
	BEGIN
		INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName)
		SELECT
		11 AS Id
		,'Ethnicity' AS Name
		,'Id' AS KeyPropertyName
	END

	IF NOT EXISTS (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Race')
	BEGIN
		INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName)
		SELECT
		37 AS Id
		,'Race' AS Name
		,'Id' AS KeyPropertyName
	END