﻿--Found that the field ReceivableId on ClOrders was never updated
UPDATE cl
SET ReceivableId = ir. Id
FROM CLOrders cl
INNER JOIN PatientReceivablesBackup prb ON cl.ReceivableId = prb.ReceivableId
INNER JOIN InvoiceReceivablesBackup irb ON prb.AppointmentId = irb.InvoiceId
INNER JOIN model.Invoices inv ON inv.EncounterId = prb.AppointmentId
INNER JOIN model.InvoiceReceivables ir ON inv.Id = ir.InvoiceId
WHERE irb.PatientInsuranceId IS NOT NULL AND ir.PatientInsuranceId IS NOT NULL
