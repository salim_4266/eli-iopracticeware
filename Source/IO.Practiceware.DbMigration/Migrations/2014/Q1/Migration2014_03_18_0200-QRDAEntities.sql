﻿---- renamed.
IF OBJECT_ID(N'model.PatientSpecialExamPerformeds','U') IS NOT NULL
	EXEC dbo.DropObject 'model.PatientSpecialExamPerformeds'
GO
IF OBJECT_ID(N'model.PatientOtherElementPerformeds', 'U') IS NOT NULL
	EXEC dbo.DropObject 'model.PatientOtherElementPerformeds'
GO
IF OBJECT_ID(N'model.SpecialExamPerformeds', 'U') IS NOT NULL
	EXEC dbo.DropObject 'model.SpecialExamPerformeds'
GO
IF OBJECT_ID(N'model.OtherElementPerformeds', 'U') IS NOT NULL
	EXEC dbo.DropObject 'model.OtherElementPerformeds'
GO

---- schema validation.
IF EXISTS (
SELECT 
* 
FROM sys.columns 
WHERE object_id = OBJECT_ID('model.RouteOfAdministrations', 'U')
AND name = 'Name'
AND max_length = 4000
)
BEGIN
	ALTER TABLE [model].[RouteOfAdministrations]
	ALTER COLUMN Name NVARCHAR(MAX) NOT NULL

	ALTER TABLE [model].[RouteOfAdministrations]
	ALTER COLUMN [OrdinalId] INT NOT NULL
END


IF OBJECT_ID(N'[model].[FK_PatientSpecialExamPerformedEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT [FK_PatientSpecialExamPerformedEncounter];
GO
IF OBJECT_ID(N'[model].[FK_PatientOtherElementPerformedEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientInterventionPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT [FK_PatientOtherElementPerformedEncounter];
GO
IF OBJECT_ID(N'[model].[FK_SpecialExamPerformedScreen]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ExamPerformeds] DROP CONSTRAINT [FK_SpecialExamPerformedScreen];
GO
IF OBJECT_ID(N'[model].[FK_PatientSpecialExamPerformedClinicalProcedure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT [FK_PatientSpecialExamPerformedClinicalProcedure];
GO
IF OBJECT_ID(N'[model].[FK_PatientOtherElementPerformedClinicalProcedure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientInterventionPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT [FK_PatientOtherElementPerformedClinicalProcedure];
GO
IF OBJECT_ID(N'[model].[FK_PatientOtherElementPerformedClinicalDataSourceType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientInterventionPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT [FK_PatientOtherElementPerformedClinicalDataSourceType];
GO
IF OBJECT_ID(N'[model].[FK_PatientSpecialExamPerformedClinicalDataSourceType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT [FK_PatientSpecialExamPerformedClinicalDataSourceType];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT [FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed];
GO
IF OBJECT_ID(N'[model].[FK_PatientInterventionPerformedPatient]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientInterventionPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT [FK_PatientInterventionPerformedPatient];
GO
IF OBJECT_ID(N'[model].[FK_PatientExamPerformedPatient]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT [FK_PatientExamPerformedPatient];
GO
IF OBJECT_ID(N'[model].[FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT [FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed];
GO
IF OBJECT_ID(N'[model].[FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT [FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed];
GO
IF OBJECT_ID(N'[model].[FK_PatientExamPerformedQuestionAnswerPatientExamPerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT [FK_PatientExamPerformedQuestionAnswerPatientExamPerformed];
GO
IF OBJECT_ID(N'[model].[FK_QuestionAnswerQuestion]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT [FK_QuestionAnswerQuestion];
GO
IF OBJECT_ID(N'[model].[FK_QuestionAnswerValueQuestionAnswer]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswerValues]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswerValues] DROP CONSTRAINT [FK_QuestionAnswerValueQuestionAnswer];
GO
IF OBJECT_ID(N'[model].[FK_LinkedQuestionAnswerValuePracticeRepositoryEntity]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswerValues]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswerValues] DROP CONSTRAINT [FK_LinkedQuestionAnswerValuePracticeRepositoryEntity];
GO
IF OBJECT_ID(N'[model].[FK_UnitQuestionAnswerValueUnitOfMeasurement]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswerValues]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswerValues] DROP CONSTRAINT [FK_UnitQuestionAnswerValueUnitOfMeasurement];
GO
IF OBJECT_ID(N'[model].[FK_PatientInterventionPerformedTimeQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientInterventionPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT [FK_PatientInterventionPerformedTimeQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientExamPerformedTimeQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT [FK_PatientExamPerformedTimeQualifier];
GO

-- Check if 'ExamPerformeds' exists...
IF OBJECT_ID(N'model.ExamPerformeds', 'U') IS NULL
BEGIN
-- 'ExamPerformeds' does not exist, creating...
	CREATE TABLE [model].[ExamPerformeds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ScreenId] int  NOT NULL
);
END

-- Check if 'InterventionPerformeds' exists...
IF OBJECT_ID(N'model.InterventionPerformeds', 'U') IS NULL
BEGIN
-- 'InterventionPerformeds' does not exist, creating...
	CREATE TABLE [model].[InterventionPerformeds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END

-- Check if 'PatientExamPerformeds' exists...
IF OBJECT_ID(N'model.PatientExamPerformeds', 'U') IS NULL
BEGIN
-- 'PatientExamPerformeds' does not exist, creating...
	CREATE TABLE [model].[PatientExamPerformeds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [ClinicalProcedureId] int  NOT NULL,
    [ClinicalDataSourceTypeId] int  NOT NULL,
    [IsBillable] bit  NOT NULL,
    [PatientId] int  NOT NULL,
    [PerformedDateTime] datetime  NOT NULL,
    [TimeQualifierId] int  NULL,
    [Property1] nvarchar(max)  NOT NULL
);
END

-- Check if 'PatientInterventionPerformeds' exists...
IF OBJECT_ID(N'model.PatientInterventionPerformeds', 'U') IS NULL
BEGIN
-- 'PatientInterventionPerformeds' does not exist, creating...
	CREATE TABLE [model].[PatientInterventionPerformeds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [ClinicalProcedureId] int  NOT NULL,
    [ClinicalDataSourceTypeId] int  NOT NULL,
    [PatientId] int  NOT NULL,
    [PerformedDateTime] datetime  NOT NULL,
    [TimeQualifierId] int  NULL,
    [IsBillable] bit  NOT NULL
);
END

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'IsBillable'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
ALTER TABLE model.PatientDiagnosisDetails
ADD IsBillable  bit NOT NULL

-- Check if 'QuestionAnswers' exists...
IF OBJECT_ID(N'model.QuestionAnswers', 'U') IS NULL
BEGIN
-- 'QuestionAnswers' does not exist, creating...
	CREATE TABLE [model].[QuestionAnswers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [QuestionId] int  NOT NULL,
    [LateralityId] int  NULL,
    [PatientDiagnosticTestPerformedId] int  NULL,
    [PatientInterventionPerformedId] int  NULL,
    [PatientProcedurePerformedId] int  NULL,
    [PatientExamPerformedId] int  NULL,
    [__EntityType__] nvarchar(100)  NOT NULL
);
END

-- Check if 'QuestionAnswerValues' exists...
IF OBJECT_ID(N'model.QuestionAnswerValues', 'U') IS NULL
BEGIN
-- 'QuestionAnswerValues' does not exist, creating...
	CREATE TABLE [model].[QuestionAnswerValues] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [QuestionAnswerId] int  NOT NULL,
    [PracticeRepositoryEntityId] int  NULL,
    [PracticeRepositoryEntityKey] nvarchar(max)  NULL,
    [Unit] decimal(18,0)  NULL,
    [UnitOfMeasurementId] int  NULL,
    [Text] nvarchar(max)  NULL,
    [__EntityType__] nvarchar(100)  NOT NULL
);
END

-- Creating primary key on [Id] in table 'ExamPerformeds'
ALTER TABLE [model].[ExamPerformeds]
ADD CONSTRAINT [PK_ExamPerformeds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InterventionPerformeds'
ALTER TABLE [model].[InterventionPerformeds]
ADD CONSTRAINT [PK_InterventionPerformeds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientExamPerformeds'
ALTER TABLE [model].[PatientExamPerformeds]
ADD CONSTRAINT [PK_PatientExamPerformeds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientInterventionPerformeds'
ALTER TABLE [model].[PatientInterventionPerformeds]
ADD CONSTRAINT [PK_PatientInterventionPerformeds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'QuestionAnswers'
ALTER TABLE [model].[QuestionAnswers]
ADD CONSTRAINT [PK_QuestionAnswers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'QuestionAnswerValues'
ALTER TABLE [model].[QuestionAnswerValues]
ADD CONSTRAINT [PK_QuestionAnswerValues]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [EncounterId] in table 'PatientExamPerformeds'
IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientExamPerformeds]
ADD CONSTRAINT [FK_PatientSpecialExamPerformedEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSpecialExamPerformedEncounter'
CREATE INDEX [IX_FK_PatientSpecialExamPerformedEncounter]
ON [model].[PatientExamPerformeds]
    ([EncounterId]);
GO

-- Creating foreign key on [EncounterId] in table 'PatientInterventionPerformeds'
IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInterventionPerformeds]
ADD CONSTRAINT [FK_PatientOtherElementPerformedEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientOtherElementPerformedEncounter'
CREATE INDEX [IX_FK_PatientOtherElementPerformedEncounter]
ON [model].[PatientInterventionPerformeds]
    ([EncounterId]);
GO

-- Creating foreign key on [ScreenId] in table 'ExamPerformeds'
IF OBJECTPROPERTY(OBJECT_ID('[model].[Screens]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExamPerformeds]
ADD CONSTRAINT [FK_SpecialExamPerformedScreen]
    FOREIGN KEY ([ScreenId])
    REFERENCES [model].[Screens]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SpecialExamPerformedScreen'
CREATE INDEX [IX_FK_SpecialExamPerformedScreen]
ON [model].[ExamPerformeds]
    ([ScreenId]);
GO

-- Creating foreign key on [ClinicalProcedureId] in table 'PatientExamPerformeds'
IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalProcedures]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientExamPerformeds]
ADD CONSTRAINT [FK_PatientSpecialExamPerformedClinicalProcedure]
    FOREIGN KEY ([ClinicalProcedureId])
    REFERENCES [model].[ClinicalProcedures]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSpecialExamPerformedClinicalProcedure'
CREATE INDEX [IX_FK_PatientSpecialExamPerformedClinicalProcedure]
ON [model].[PatientExamPerformeds]
    ([ClinicalProcedureId]);
GO

-- Creating foreign key on [ClinicalProcedureId] in table 'PatientInterventionPerformeds'
IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalProcedures]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInterventionPerformeds]
ADD CONSTRAINT [FK_PatientOtherElementPerformedClinicalProcedure]
    FOREIGN KEY ([ClinicalProcedureId])
    REFERENCES [model].[ClinicalProcedures]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientOtherElementPerformedClinicalProcedure'
CREATE INDEX [IX_FK_PatientOtherElementPerformedClinicalProcedure]
ON [model].[PatientInterventionPerformeds]
    ([ClinicalProcedureId]);
GO


-- Creating foreign key on [ClinicalDataSourceTypeId] in table 'PatientInterventionPerformeds'
IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalDataSourceTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInterventionPerformeds]
ADD CONSTRAINT [FK_PatientOtherElementPerformedClinicalDataSourceType]
    FOREIGN KEY ([ClinicalDataSourceTypeId])
    REFERENCES [model].[ClinicalDataSourceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientOtherElementPerformedClinicalDataSourceType'
CREATE INDEX [IX_FK_PatientOtherElementPerformedClinicalDataSourceType]
ON [model].[PatientInterventionPerformeds]
    ([ClinicalDataSourceTypeId]);
GO

-- Creating foreign key on [ClinicalDataSourceTypeId] in table 'PatientExamPerformeds'
IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalDataSourceTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientExamPerformeds]
ADD CONSTRAINT [FK_PatientSpecialExamPerformedClinicalDataSourceType]
    FOREIGN KEY ([ClinicalDataSourceTypeId])
    REFERENCES [model].[ClinicalDataSourceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSpecialExamPerformedClinicalDataSourceType'
CREATE INDEX [IX_FK_PatientSpecialExamPerformedClinicalDataSourceType]
ON [model].[PatientExamPerformeds]
    ([ClinicalDataSourceTypeId]);
GO

-- Creating foreign key on [PatientDiagnosticTestPerformedId] in table 'QuestionAnswers'
IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosticTestPerformeds]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswers]
ADD CONSTRAINT [FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed]
    FOREIGN KEY ([PatientDiagnosticTestPerformedId])
    REFERENCES [model].[PatientDiagnosticTestPerformeds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed'
CREATE INDEX [IX_FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed]
ON [model].[QuestionAnswers]
    ([PatientDiagnosticTestPerformedId]);
GO

-- Creating foreign key on [PatientId] in table 'PatientInterventionPerformeds'
IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInterventionPerformeds]
ADD CONSTRAINT [FK_PatientInterventionPerformedPatient]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientInterventionPerformedPatient'
CREATE INDEX [IX_FK_PatientInterventionPerformedPatient]
ON [model].[PatientInterventionPerformeds]
    ([PatientId]);
GO

-- Creating foreign key on [PatientId] in table 'PatientExamPerformeds'
IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientExamPerformeds]
ADD CONSTRAINT [FK_PatientExamPerformedPatient]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientExamPerformedPatient'
CREATE INDEX [IX_FK_PatientExamPerformedPatient]
ON [model].[PatientExamPerformeds]
    ([PatientId]);
GO

-- Creating foreign key on [PatientInterventionPerformedId] in table 'QuestionAnswers'
IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientInterventionPerformeds]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswers]
ADD CONSTRAINT [FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed]
    FOREIGN KEY ([PatientInterventionPerformedId])
    REFERENCES [model].[PatientInterventionPerformeds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed'
CREATE INDEX [IX_FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed]
ON [model].[QuestionAnswers]
    ([PatientInterventionPerformedId]);
GO

-- Creating foreign key on [PatientProcedurePerformedId] in table 'QuestionAnswers'
IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientProceduresPerformed]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswers]
ADD CONSTRAINT [FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed]
    FOREIGN KEY ([PatientProcedurePerformedId])
    REFERENCES [model].[PatientProceduresPerformed]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed'
CREATE INDEX [IX_FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed]
ON [model].[QuestionAnswers]
    ([PatientProcedurePerformedId]);
GO

-- Creating foreign key on [PatientExamPerformedId] in table 'QuestionAnswers'
IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientExamPerformeds]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswers]
ADD CONSTRAINT [FK_PatientExamPerformedQuestionAnswerPatientExamPerformed]
    FOREIGN KEY ([PatientExamPerformedId])
    REFERENCES [model].[PatientExamPerformeds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientExamPerformedQuestionAnswerPatientExamPerformed'
CREATE INDEX [IX_FK_PatientExamPerformedQuestionAnswerPatientExamPerformed]
ON [model].[QuestionAnswers]
    ([PatientExamPerformedId]);
GO

-- Creating foreign key on [QuestionId] in table 'QuestionAnswers'
IF OBJECTPROPERTY(OBJECT_ID('[model].[Questions]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswers]
ADD CONSTRAINT [FK_QuestionAnswerQuestion]
    FOREIGN KEY ([QuestionId])
    REFERENCES [model].[Questions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionAnswerQuestion'
CREATE INDEX [IX_FK_QuestionAnswerQuestion]
ON [model].[QuestionAnswers]
    ([QuestionId]);
GO

-- Creating foreign key on [QuestionAnswerId] in table 'QuestionAnswerValues'
IF OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswers]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswerValues]
ADD CONSTRAINT [FK_QuestionAnswerValueQuestionAnswer]
    FOREIGN KEY ([QuestionAnswerId])
    REFERENCES [model].[QuestionAnswers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionAnswerValueQuestionAnswer'
CREATE INDEX [IX_FK_QuestionAnswerValueQuestionAnswer]
ON [model].[QuestionAnswerValues]
    ([QuestionAnswerId]);
GO

-- Creating foreign key on [PracticeRepositoryEntityId] in table 'QuestionAnswerValues'
IF OBJECTPROPERTY(OBJECT_ID('[model].[PracticeRepositoryEntities]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswerValues]
ADD CONSTRAINT [FK_LinkedQuestionAnswerValuePracticeRepositoryEntity]
    FOREIGN KEY ([PracticeRepositoryEntityId])
    REFERENCES [model].[PracticeRepositoryEntities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkedQuestionAnswerValuePracticeRepositoryEntity'
CREATE INDEX [IX_FK_LinkedQuestionAnswerValuePracticeRepositoryEntity]
ON [model].[QuestionAnswerValues]
    ([PracticeRepositoryEntityId]);
GO

-- Creating foreign key on [UnitOfMeasurementId] in table 'QuestionAnswerValues'
IF OBJECTPROPERTY(OBJECT_ID('[model].[UnitsOfMeasurement]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswerValues]
ADD CONSTRAINT [FK_UnitQuestionAnswerValueUnitOfMeasurement]
    FOREIGN KEY ([UnitOfMeasurementId])
    REFERENCES [model].[UnitsOfMeasurement]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UnitQuestionAnswerValueUnitOfMeasurement'
CREATE INDEX [IX_FK_UnitQuestionAnswerValueUnitOfMeasurement]
ON [model].[QuestionAnswerValues]
    ([UnitOfMeasurementId]);
GO

-- Creating foreign key on [TimeQualifierId] in table 'PatientInterventionPerformeds'
IF OBJECTPROPERTY(OBJECT_ID('[model].[TimeQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInterventionPerformeds]
ADD CONSTRAINT [FK_PatientInterventionPerformedTimeQualifier]
    FOREIGN KEY ([TimeQualifierId])
    REFERENCES [model].[TimeQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientInterventionPerformedTimeQualifier'
CREATE INDEX [IX_FK_PatientInterventionPerformedTimeQualifier]
ON [model].[PatientInterventionPerformeds]
    ([TimeQualifierId]);
GO

-- Creating foreign key on [TimeQualifierId] in table 'PatientExamPerformeds'
IF OBJECTPROPERTY(OBJECT_ID('[model].[TimeQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientExamPerformeds]
ADD CONSTRAINT [FK_PatientExamPerformedTimeQualifier]
    FOREIGN KEY ([TimeQualifierId])
    REFERENCES [model].[TimeQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientExamPerformedTimeQualifier'
CREATE INDEX [IX_FK_PatientExamPerformedTimeQualifier]
ON [model].[PatientExamPerformeds]
    ([TimeQualifierId]);
GO