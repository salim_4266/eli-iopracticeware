﻿SELECT *
INTO #Reports
FROM model.Reports
WHERE Name IN (
'Adjustments',
'Adjustments Analysis',
'Charges Payments And Adjustments With Payment Method And Open Balance',
'Daily Production',
'Financial Informations',
'Last Patient Visit',
'On Account Credits',
'Payments Analysis',
'Orphaned Appointments',
'Patient Insurers',
'Patient Monthly Statement',
'Procedure Analysis',
'Unbilled Balances',
'Schedule',
'Account History Detailed')

DELETE FROM model.Reports 
WHERE PermissionId IN (SELECT PermissionId FROM #Reports)

DELETE FROM model.UserPermissions  
WHERE PermissionId IN (SELECT PermissionId FROM #Reports)

DELETE FROM model.[Permissions] 
WHERE Id IN (SELECT PermissionId FROM #Reports)

DROP TABLE #Reports