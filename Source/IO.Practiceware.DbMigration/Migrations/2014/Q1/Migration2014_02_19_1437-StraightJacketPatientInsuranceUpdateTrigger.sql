﻿
-- Update to Straight Jacket triggers as per conversation with Katherine, Rawle, and Pasquale on 2-4-2014.

/*
The update involves 2 changes:

1. When the new enddate for the policy holder has changed, check the linked policies and change their enddate to the new policy holder enddate,
   only if their enddate happens to be AFTER the new enddate.

2. Do not allow linked policies to un-end or place the enddate after it's policy holder's enddate (assuming the policy holder has one).
   (Note: this doesn't affect the trigger logic but just placing it here for reference).


*/

/* Examples Given:

			If Self Policy
			1. I'm Joe. I am a policy holder and am ending my policy. I need to end everyone else's 
               coverage under my policy too. (restricted based on certain conditions)

			If Linked Policy
			2. I'm Joe. I am covered under Jane's policy. I end my coverage under Jane's policies. 
               All of Jane's policies I am covered under need to end too. (restricted based on certain conditions)

		    */



IF OBJECT_ID('[model].[OnPatientInsurancesUpdate_StraightJacket]', 'TR') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER [model].[OnPatientInsurancesUpdate_StraightJacket]')
END

GO

CREATE TRIGGER [model].[OnPatientInsurancesUpdate_StraightJacket] ON [model].[PatientInsurances] AFTER UPDATE
NOT FOR REPLICATION
		AS 
		SET NOCOUNT ON 
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

		DECLARE 
		@insurancePolicyId INT,
		@patientId INT,
		@policyHolderPatientId INT,
		@newEndDateTime DATETIME,
		@oldEndDateTime DATETIME,
        @policyHoldersCurrentEndDateTime DATETIME

		IF UPDATE(EndDateTime) /* This logic applies only if the end date is being updated */
		BEGIN
			
			SELECT @newEndDateTime = i.EndDateTime, @insurancePolicyId = i.InsurancePolicyId, @patientId = i.InsuredPatientId FROM inserted i		
			SELECT @oldEndDateTime = d.EndDateTime FROM deleted d			
			SELECT @policyHolderPatientId = ip.PolicyHolderPatientId FROM [model].[InsurancePolicies] ip WHERE ip.Id = @insurancePolicyId
		
            -- Self Policy (Example 1)
			IF @patientId = @policyHolderPatientId 		
            BEGIN		
				UPDATE [model].[PatientInsurances] SET EndDateTime = @newEndDateTime 
                WHERE InsurancePolicyId = @insurancePolicyId and IsDeleted = 0 
                AND (
                        (EndDateTime = @oldEndDateTime) -- end dates are the same
                            OR 
                        (EndDateTime IS NULL AND @oldEndDateTime IS NULL) -- end dates are both null
                            OR 
                        (EndDateTime > @newEndDateTime) -- linked policy's enddate is greater than the new enddate
                     )
            END            
			ELSE -- Linked Policy (Example 2)
            BEGIN                

				UPDATE pin SET pin.EndDateTime = @newEndDateTime 
                FROM [model].[PatientInsurances] pin inner join [model].[InsurancePolicies] ip on pin.InsurancePolicyId = ip.Id
				WHERE pin.InsuredPatientId = @patientId 
                and ip.PolicyHolderPatientId = @policyHolderPatientId 
                and pin.IsDeleted = 0 
                and ip.StartDateTime <= @newEndDateTime          
                and
                 ((select pin2.EndDateTime from model.PatientInsurances pin2  WITH (NOLOCK)
                            inner join model.InsurancePolicies ip2 on pin2.InsurancePolicyId = ip2.Id 
                            WHERE ip2.PolicyholderPatientId = @policyHolderPatientId 
                            AND pin2.InsuredPatientId = @policyHolderPatientId 
                            AND pin2.InsurancePolicyId = pin.InsurancePolicyId
                            AND pin2.IsDeleted = 0) IS NULL)
                OR
                 (@newEndDateTime <=                          
                        (select pin2.EndDateTime from model.PatientInsurances pin2  WITH (NOLOCK)
                            inner join model.InsurancePolicies ip2 on pin2.InsurancePolicyId = ip2.Id 
                            WHERE ip2.PolicyholderPatientId = @policyHolderPatientId 
                            AND pin2.InsuredPatientId = @policyHolderPatientId 
                            AND pin2.InsurancePolicyId = pin.InsurancePolicyId
                            AND pin2.IsDeleted = 0))
                        			
            END
					
		END
		SET NOCOUNT OFF
	RETURN 
GO