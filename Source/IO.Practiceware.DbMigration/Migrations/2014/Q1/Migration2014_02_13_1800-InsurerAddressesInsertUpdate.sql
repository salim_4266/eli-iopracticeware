﻿IF EXISTS (SELECT * FROM sys.triggers WHERE Name = 'OnInsurerAddressesInsertOrUpdate') 
	DROP TRIGGER [model].[OnInsurerAddressesInsertOrUpdate];
GO

CREATE TRIGGER [OnInsurerAddressesInsertOrUpdate] ON model.InsurerAddresses
AFTER UPDATE, INSERT
AS
BEGIN
DECLARE @msrepl_object_id int
SET @msrepl_object_id = OBJECT_ID('[model].[InsurerAddresses]')
DECLARE @retcode int
EXEC @retcode = sp_check_for_sync_trigger @msrepl_object_id
IF @retcode = 1 RETURN
IF EXISTS (
SELECT * FROM inserted
EXCEPT
SELECT * FROM deleted
)
BEGIN
	UPDATE pins
	SET pins.InsurerAddress = 
		CONVERT(NVARCHAR(50),CASE WHEN dbo.IsNullOrEmpty(i.Line1) = 0
			THEN CASE WHEN dbo.IsNullOrEmpty(i.Line2) = 0 THEN i.Line1 + ', ' + i.Line2 ELSE i.Line1 END
			ELSE ''
		END)
		,pins.InsurerCity = CONVERT(NVARCHAR(32),CASE WHEN dbo.IsNullOrEmpty(i.City) = 0 THEN i.City ELSE '' END)
		,pins.InsurerState = CONVERT(NVARCHAR(2),CASE WHEN dbo.IsNullOrEmpty(i.StateOrProvinceId) = 0 THEN s.Abbreviation ELSE '' END)
		,pins.InsurerZip = CONVERT(NVARCHAR(10),CASE WHEN dbo.IsNullOrEmpty(i.PostalCode) = 0 THEN i.PostalCode ELSE '' END)
	FROM dbo.PracticeInsurers pins
	INNER JOIN inserted i ON i.InsurerId = pins.InsurerId
	LEFT JOIN model.StateOrProvinces s ON s.Id = i.StateOrProvinceId
END
END
GO

