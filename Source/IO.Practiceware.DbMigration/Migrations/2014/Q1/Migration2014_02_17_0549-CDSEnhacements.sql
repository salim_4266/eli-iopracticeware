﻿--PBI #9623.CDS enhacements.
--Task #9658.


--Create Row for Query  in IE_FieldParams
IF NOT EXISTS(SELECT TOP 1 * FROM IE_FieldParams WHERE FldArea='Query' AND FldName='Query')
BEGIN
	INSERT INTO IE_FieldParams(FldArea,FldName,FldDesc,FldType,LookupId) VALUES('Query','Query','SQL Queries',1,0)
END
GO

--Change the DataType of 'FieldValue' Column of IE_RulePhrases to Varchar(Max)
IF EXISTS(SELECT * FROM sys.columns WHERE name = 'FieldValue' AND object_id = OBJECT_ID('IE_RulesPhrases'))
BEGIN
	ALTER TABLE IE_RulesPhrases ALTER COLUMN FieldValue VARCHAR(MAX) NULL
END