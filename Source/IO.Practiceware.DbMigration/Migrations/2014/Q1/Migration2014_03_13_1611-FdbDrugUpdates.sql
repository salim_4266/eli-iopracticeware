﻿--Certain drugs had ,suspension added to them in FDB

UPDATE dr
SET DisplayName = DisplayName + 'let'
FROM dbo.drug dr
INNER JOIN fdb.tblCompositeDrug fd ON dr.DisplayName +'let'= fd.MED_ROUTED_DF_MED_ID_DESC
WHERE DrugCode = 'T'
	AND DisplayName LIKE '% Tab'

UPDATE dr
SET DisplayName = DisplayName + 'sule'
FROM dbo.drug dr
INNER JOIN fdb.tblCompositeDrug fd ON dr.DisplayName +'sule'= fd.MED_ROUTED_DF_MED_ID_DESC
WHERE DrugCode = 'T'
	AND DisplayName LIKE '% Cap'

UPDATE dr
SET DisplayName = DisplayName + ',suspension'
FROM dbo.drug dr
INNER JOIN fdb.tblCompositeDrug fd ON dr.DisplayName +',suspension'= fd.MED_ROUTED_DF_MED_ID_DESC
WHERE DrugCode = 'T'

--Other one off drugs that must be mapped manually
UPDATE dbo.Drug
SET DisplayName = 'Restasis eye drops in a dropperette'
WHERE DisplayName = 'Restasis Eye Dropperette'