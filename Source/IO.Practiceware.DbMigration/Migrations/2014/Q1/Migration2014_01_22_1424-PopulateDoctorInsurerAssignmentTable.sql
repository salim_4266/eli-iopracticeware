﻿IF NOT EXISTS (SELECT * FROM model.DoctorInsurerAssignments)
BEGIN
INSERT INTO model.DoctorInsurerAssignments ([IsAssignmentAccepted]
      ,[InsurerId]
      ,[DoctorId]
      ,[SuppressPatientPayments])
SELECT CASE WHEN FieldValue = 'T' THEN 0
		ELSE 1
		END AS IsAssignmentAccepted
	, pri.InsurerId AS InsurerId
	, ResourceId AS DoctorId
	, CASE WHEN FieldValue = 'T' THEN 0
		ELSE 1
		END  AS SuppressPatientPayments
FROM dbo.PracticeInterfaceConfiguration pic
INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerReferenceCode IN ('M', 'N', '[')
INNER JOIN dbo.Resources r ON r.ResourceType IN ('D', 'Q')
	AND len(r.NPI) =10
WHERE FieldReference = 'RPTPATPAY'
END