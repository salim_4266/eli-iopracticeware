﻿IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL
DROP TABLE #NoAudit
SELECT 1 AS Value INTO #NoAudit 

UPDATE model.Patients
SET ReleaseOfInformationDateTime = NULL
WHERE ReleaseOfInformationDateTime = '1900-01-01 00:00:00.000'

UPDATE model.Patients
SET GenderId = NULL
WHERE GenderId = 4

DROP TABLE #NoAudit