﻿IF EXISTS (SELECT OBJECT_ID FROM sys.tables WHERE name = 'TemplateDocuments' and schema_id = 6)
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = ''

	SELECT @sql = @sql + 'DROP TRIGGER ' + s.name + '.' + tr.name + '
	'
	FROM sys.triggers tr
	INNER JOIN sys.tables ta ON tr.parent_id = ta.object_id
	INNER JOIN sys.schemas s ON s.schema_id = ta.schema_id
	 WHERE ta.Name = 'TemplateDocuments'
	 AND tr.Name LIKE '%Audit%'

	EXEC(@Sql)
END

IF EXISTS (SELECT OBJECT_ID FROM sys.tables WHERE name = 'TemplateDocumentPrinter' and schema_id = 6) 
BEGIN
	SET @sql = ''

	SELECT @sql = @sql + 'DROP TRIGGER ' + s.name + '.' + tr.name + '
	'
	FROM sys.triggers tr
	INNER JOIN sys.tables ta ON tr.parent_id = ta.object_id
	INNER JOIN sys.schemas s ON s.schema_id = ta.schema_id
	 WHERE ta.Name = 'TemplateDocumentPrinter'
	 AND tr.Name LIKE '%Audit%'

	EXEC(@Sql)
END

IF NOT EXISTS ( SELECT Id FROM model.TemplateDocuments td WHERE DisplayName = 'CMS-1500-pre-2014')
BEGIN
INSERT INTO model.TemplateDocuments (TemplateDocumentCategoryId, Name, DisplayName, Content,ContentOriginal,ContentTypeId, IsEditable)
SELECT TemplateDocumentCategoryId, 'CMS-1500-pre-2014.cshtml', 'CMS-1500-pre-2014', Content, ContentOriginal, ContentTypeId, IsEditable 
FROM model.TemplateDocuments WHERE DisplayName = 'CMS-1500'
END

IF EXISTS (SELECT Id FROM model.TemplateDocuments WHERE DisplayName = 'CMS-1500' )
BEGIN
UPDATE model.TemplateDocuments SET Content = NULL WHERE DisplayName = 'CMS-1500'
END