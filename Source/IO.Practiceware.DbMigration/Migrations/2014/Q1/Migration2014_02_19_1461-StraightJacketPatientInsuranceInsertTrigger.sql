﻿IF OBJECT_ID('[model].[OnPatientInsurancesInsert_StraightJacket]', 'TR') IS NOT NULL
BEGIN
	EXEC ('DROP TRIGGER [model].[OnPatientInsurancesInsert_StraightJacket]')
END
GO

CREATE TRIGGER [model].[OnPatientInsurancesInsert_StraightJacket] ON [model].[PatientInsurances]
AFTER INSERT 
NOT FOR REPLICATION 
AS
SET NOCOUNT ON 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @insertedInsurancePolicyId INT
	,@insertedInsuredPatientId INT
	,@insertedPolicyHolderPatientId INT
	,@insertedPolicyHolderRelationshipTypeId INT
	,@insertedInsuranceTypeId INT
	,@insertedEndDateTime DATETIME
	,@insurerIdToInsert INT
	,@insurancePolicyIdToInsert INT
	,@currentPolicyHolderRelationshipTypeId INT
	,@currentLinkedPatientId INT
	,@allowDependants BIT
	,@insuranceTypeIdToInsert INT
	,@ordinalIdToInsert INT
	,@endDateTimeToInsert DATETIME

BEGIN
	SELECT @insertedInsurancePolicyId = i.InsurancePolicyId
		,@insertedInsuredPatientId = i.InsuredPatientId
		,@insertedPolicyHolderRelationshipTypeId = i.PolicyHolderRelationshipTypeId
		,@insertedInsuranceTypeId = i.InsuranceTypeId
		,@insertedEndDateTime = i.EndDateTime
	FROM inserted i;

	SELECT @insertedPolicyHolderPatientId = ip.PolicyHolderPatientId
	FROM [model].[InsurancePolicies] ip
	WHERE ip.Id = @insertedInsurancePolicyId;

	/* Example #1:					
			               I'm Joe (@insertedInsuredPatientId). I add Jane's (@insertedPolicyHolderPatientId) policy for Major Medical. I also 
                           need a PatientInsurance for all of Jane's other policies where she is the policyholder and 
			               those policies' insurers' allow dependents flag is set to true. (this is across ALL insurance types).
			*/
	/* NOT SELF POLICY - we need to add a new insurance for each policy the Policyholder has to the patient */
	IF @insertedInsuredPatientId <> @insertedPolicyHolderPatientId
	BEGIN
		-- create a cursor which will loop through the 'other' policies that belong to the policyholder.  we only grab the policies that do not already belong to the current patient
		DECLARE policyholdersOtherPoliciesCursor CURSOR
		FOR
		SELECT DISTINCT ip.InsurerId
			,ip.Id
			,pi.InsuranceTypeId
			,pi.EndDateTime
		FROM model.InsurancePolicies ip
		INNER JOIN model.PatientInsurances pi ON ip.Id = pi.InsurancePolicyId
		INNER JOIN model.Insurers ins ON ip.InsurerId = ins.Id
		WHERE ip.PolicyHolderPatientId = @insertedPolicyHolderPatientId -- get only the policyholders insurance policies
			AND ip.Id <> @insertedInsurancePolicyId -- exclude the policy that was just inserted for the patient
			AND pi.IsDeleted = 0 AND pi.InsuredPatientId = @insertedPolicyHolderPatientId AND ins.AllowDependents = 1 -- see if the insurer for the current policy supports dependents
			AND ip.Id NOT IN (
				SELECT pin.InsurancePolicyId
				FROM PatientInsurances pin
				WHERE pin.InsuredPatientId = @insertedInsuredPatientId AND pin.IsDeleted = 0
				) -- make sure to only get the policies that are NOT linked to the current patient

		OPEN policyholdersOtherPoliciesCursor

		FETCH NEXT
		FROM policyholdersOtherPoliciesCursor
		INTO @insurerIdToInsert
			,@insurancePolicyIdToInsert
			,@insuranceTypeIdToInsert
			,@endDateTimeToInsert

		WHILE @@FETCH_STATUS = 0 /* Loop through the Policy Holder's Insurance Policies  */
		BEGIN
			-- get the next ordinal id for this patient within the specific insurance type for active insurances
			SELECT @ordinalIdToInsert = (MAX(pin.OrdinalId) + 1)
			FROM model.PatientInsurances pin
			WHERE pin.InsuredPatientId = @insertedInsuredPatientId AND pin.InsuranceTypeId = @insuranceTypeIdToInsert AND pin.IsDeleted = 0 AND ((pin.EndDateTime IS NULL) OR (pin.EndDateTime >= DateAdd(yy, DATEPART(yy, model.GetClientDateNow()) - 1900, DateAdd(m, DATEPART(MONTH, model.GetClientDateNow()) - 1, DATEPART(DAY, model.GetClientDateNow()) - 1))));

			IF @ordinalIdToInsert IS NULL OR @ordinalIdToInsert = 0
			BEGIN
				SET @ordinalIdToInsert = 1;
			END

			INSERT INTO model.PatientInsurances (
				InsuranceTypeId
				,PolicyHolderRelationshipTypeId
				,OrdinalId
				,InsuredPatientId
				,InsurancePolicyId
				,EndDateTime
				,IsDeleted
				)
			VALUES (
				@insuranceTypeIdToInsert
				,@insertedPolicyHolderRelationshipTypeId
				,@ordinalIdToInsert
				,@insertedInsuredPatientId
				,@insurancePolicyIdToInsert
				,@endDateTimeToInsert
				,0
				)

			FETCH NEXT
			FROM policyholdersOtherPoliciesCursor
			INTO @insurerIdToInsert
				,@insurancePolicyIdToInsert
				,@insuranceTypeIdToInsert
				,@endDateTimeToInsert
		END

		CLOSE policyholdersOtherPoliciesCursor;

		DEALLOCATE policyholdersOtherPoliciesCursor;
	END

	/* Example #2:					
			               I'm Joe (@insertedInsuredPatientId). I add a SELF (@insertedPolicyHolderPatientId) policy for Major Medical. 
                           I need to check all patient's who have insurances that are linked to
                           ANY of my policies and add a new PatientInsurance to each of them for this new incoming policy
			*/
	/* SELF POLICY - we need to add a new insurance for all patients linked to this policyholder */
	IF @insertedInsuredPatientId = @insertedPolicyHolderPatientId
	BEGIN
		-- Check to see if the Insurer on the Policy allows dependants
		SELECT @allowDependants = AllowDependents
		FROM model.Insurers
		WHERE Id = (
				SELECT InsurerId
				FROM model.InsurancePolicies
				WHERE Id = @insertedInsurancePolicyId
				)

		IF @allowDependants = 1
		BEGIN
			-- Get list of linked patients
			DECLARE linkedPatientsCursor CURSOR
			FOR
			SELECT DISTINCT TBL1.cursorPatientId
				,TBL1.cursorPolicyHolderRelationshipTypeId
			FROM (
				SELECT pin.InsuredPatientId AS cursorPatientId
					,pin.PolicyHolderRelationshipTypeId AS cursorPolicyHolderRelationshipTypeId
					,pin.InsurancePolicyId AS cursorInsurancePolicyId
				FROM model.PatientInsurances pin
				INNER JOIN model.InsurancePolicies ip ON pin.InsurancePolicyId = ip.Id AND ip.PolicyholderPatientId = @insertedInsuredPatientId
				WHERE pin.IsDeleted = 0 AND pin.InsuredPatientId <> @insertedInsuredPatientId
				) AS TBL1
			WHERE TBL1.cursorPatientId NOT IN (
					SELECT pin2.InsuredPatientId
					FROM model.PatientInsurances pin2
					WHERE pin2.InsurancePolicyId = @insertedInsurancePolicyId
					);

			OPEN linkedPatientsCursor

			FETCH NEXT
			FROM linkedPatientsCursor
			INTO @currentLinkedPatientId
				,@currentPolicyHolderRelationshipTypeId

			WHILE @@FETCH_STATUS = 0 /* Loop through the patient's that are linked to the policyholder */
			BEGIN
				-- get the next ordinal id for this patient within the specific insurance type for active insurances
				SELECT @ordinalIdToInsert = (MAX(pin.OrdinalId) + 1)
				FROM model.PatientInsurances pin
				WHERE pin.InsuredPatientId = @currentLinkedPatientId AND pin.InsuranceTypeId = @insertedInsuranceTypeId AND pin.IsDeleted = 0 AND ((pin.EndDateTime IS NULL) OR (pin.EndDateTime >= DateAdd(yy, DATEPART(yy, model.GetClientDateNow()) - 1900, DateAdd(m, DATEPART(MONTH, model.GetClientDateNow()) - 1, DATEPART(DAY, model.GetClientDateNow()) - 1))));

				IF @ordinalIdToInsert IS NULL OR @ordinalIdToInsert = 0
				BEGIN
					SET @ordinalIdToInsert = 1;
				END

				INSERT INTO model.PatientInsurances (
					InsuranceTypeId
					,PolicyHolderRelationshipTypeId
					,OrdinalId
					,InsuredPatientId
					,InsurancePolicyId
					,EndDateTime
					,IsDeleted
					)
				VALUES (
					@insertedInsuranceTypeId
					,@currentPolicyHolderRelationshipTypeId
					,@ordinalIdToInsert
					,@currentLinkedPatientId
					,@insertedInsurancePolicyId
					,@insertedEndDateTime
					,0
					);

				FETCH NEXT
				FROM linkedPatientsCursor
				INTO @currentLinkedPatientId
					,@currentPolicyHolderRelationshipTypeId
			END

			CLOSE linkedPatientsCursor;

			DEALLOCATE linkedPatientsCursor;
		END
	END

	;WITH CTE AS (
		SELECT 
		ip.InsurerId
		,pi.InsuredPatientId
		,pi.InsuranceTypeId
		FROM model.InsurancePolicies ip
		INNER JOIN model.PatientInsurances pi ON pi.InsurancePolicyId = ip.Id
		WHERE pi.InsuredPatientId IN (SELECT DISTINCT i.InsuredPatientId FROM inserted i)
			AND ip.InsurerId IN (SELECT DISTINCT ip.InsurerId FROM inserted i INNER JOIN model.InsurancePolicies ip ON ip.Id = i.InsurancePolicyId)
			AND pi.InsuranceTypeId IN (SELECT i.InsuranceTypeId FROM inserted i)
			AND pi.PolicyHolderRelationshipTypeId = 1
		GROUP BY ip.InsurerId
			,pi.InsuredPatientId
			,pi.InsuranceTypeId
		HAVING COUNT(InsurerId) > 1
	)
	,CTE2 AS (
		SELECT 
		i.Id AS InvoiceId
		,ip.InsurerId
		,pi.Id AS PatientInsuranceId
		FROM model.PatientInsurances pi
		INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		INNER JOIN CTE ON CTE.InsurerId = ip.InsurerId	
			AND CTE.InsuredPatientId = pi.InsuredPatientId
			AND CTE.InsuranceTypeId = pi.InsuranceTypeId
		INNER JOIN dbo.Appointments e ON e.PatientId = pi.InsuredPatientId
		LEFT JOIN model.Invoices i ON i.EncounterId = e.EncounterId
		LEFT JOIN model.InvoiceReceivables ir ON ir.PatientInsuranceId = pi.Id
			AND ir.InvoiceId = i.Id
		WHERE ir.Id IS NULL
			AND i.LegacyDateTime >= ip.StartDateTime
			AND (i.LegacyDateTime <= pi.EndDateTime OR pi.EndDateTime IS NULL)
			AND pi.PolicyHolderRelationshipTypeId = 1
			
	)
	UPDATE ir
	SET ir.PatientInsuranceId = CTE2.PatientInsuranceId
	FROM model.InvoiceReceivables ir
	INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
	INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	INNER JOIN CTE2 ON CTE2.InvoiceId = i.Id
	WHERE ip.InsurerId = CTE2.InsurerId
END
GO

