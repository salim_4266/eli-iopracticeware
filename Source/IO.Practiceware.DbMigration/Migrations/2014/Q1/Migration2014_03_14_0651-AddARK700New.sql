﻿-- Add ARK700New Device names to the Devices Table.

--Bug #9862.
IF NOT EXISTS(Select TOP 1 * from dbo.Devices WHERE DeviceAlias='ARK700ANEW')
BEGIN
	INSERT INTO dbo.Devices (Make, Model, DeviceAlias, Type, Baud, Parity, DataBits, Stopbits) VALUES('NIDEK','ARK-700ANEW','ARK700ANEW','Autorefractor','9600','odd','8', '1') 
END 
GO
