﻿IF NOT EXISTS (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'EncounterCommunicationWithOtherProviderOrder')
BEGIN
	INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName)
	SELECT
	73 AS Id
	,'EncounterCommunicationWithOtherProviderOrder' AS Name
	,'Id' AS KeyPropertyName
END
GO

INSERT INTO model.CommunicationCommunicationReviewStatus (Name, OrdinalId, IsDeactivatedId)
SELECT * FROM (
SELECT 'Not Reviewed' AS Name,1 AS OrdinalId,CONVERT(BIT,0) AS IsDeactivatedId
UNION ALL
SELECT 'Ready To Send' AS Name,2 AS OrdinalId,CONVERT(BIT,0) AS IsDeactivatedId
)v
EXCEPT
SELECT Name, OrdinalId, IsDeactivatedId FROM model.CommunicationCommunicationReviewStatus

INSERT INTO model.CommunicationWithOtherProviderOrders (IsExcludedFromCommunications,IsFollowUpRequired,OrdinalId,SimpleDescription,TechnicalDescription,TemplateDocumentId,ExternalProviderCommunicationTypeId)
SELECT 
CASE 
	WHEN p.Exclusion <> 'T'
		THEN CONVERT(BIT,0)
	ELSE CONVERT(BIT,1)
END AS IsExcludedFromCommunications
,CASE 
	WHEN p.FollowUp <> 'T'
		THEN CONVERT(BIT,0)
	ELSE CONVERT(BIT,1)
END AS IsFollowUpRequired
,1 AS OrdinalId
,CASE WHEN dbo.IsNullOrEmpty(p.Code) = 0
	THEN p.Code
END AS SimpleDescription
,NULL AS TechnicalDescription
,NULL AS TemplateDocumentId
,3 AS ExternalProviderCommunicationTypeId --Letter
FROM dbo.PracticeCodeTable p
WHERE ReferenceType = 'CONSULTATIONLETTERS'
EXCEPT 
SELECT IsExcludedFromCommunications,IsFollowUpRequired,OrdinalId,SimpleDescription,TechnicalDescription,TemplateDocumentId,ExternalProviderCommunicationTypeId FROM model.CommunicationWithOtherProviderOrders

--IGNORE -- QRDA TESTS ONLY
--INSERT INTO model.EncounterCommunicationWithOtherProviderOrders (IsPatientCopied,EncounterId,CommunicationWithOtherProviderOrderId,Comment,CommunicationReviewStatusId,ReceiverExternalProviderId)
--SELECT 
--CONVERT(BIT,0) AS IsPatientCopied
--,Id AS EncounterId
--,(SELECT Id FROM model.CommunicationWithOtherProviderOrders WHERE SimpleDescription = 'Consultation') AS CommunicationWithOtherProviderOrderId
--,CONVERT(NVARCHAR(MAX),NULL) AS Comment
--,2 AS CommunicationReviewStatusId --Ready To Send
--,(SELECT TOP 1 Id FROM PracticeVendors WHERE VendorNPI IS NOT NULL) AS ReceiverExternalProviderId
--FROM model.Encounters
--WHERE PatientId IN (
--	SELECT Id
--	FROM model.Patients
--	WHERE LastName = 'Stephens'
--)
--AND StartDateTime = '2012-01-04 07:30:00.000'