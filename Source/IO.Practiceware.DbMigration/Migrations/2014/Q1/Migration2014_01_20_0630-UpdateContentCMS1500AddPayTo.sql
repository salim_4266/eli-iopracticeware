﻿IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	
	UPDATE model.TemplateDocuments	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 
	'if (s.Length > 5) return s.Substring(0, 5) + "-" + s.Substring(5);',
	'if (s != null && s.Length > 5) return s.Substring(0, 5) + "-" + s.Substring(5);' )) 
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL

	UPDATE model.TemplateDocuments	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 
	'</div>',
	'<input type="text" id="PayToName" value="@Model.PayToName" style="z-index: 1; left: @(col3Left)px; top: @(row20Top + 20)px; position: absolute; width: 280px; right: 97px; height: 8px;" />
     <input type="text" id="PayToAddressLine1Line2" value="@Model.PayToAddressLine1 @Model.PayToAddressLine2" style="z-index: 1; left: @(col3Left)px; top: @(row20Top + 40)px; position: absolute; width: 280px; right: 94px; height: 7px;" />
     <input type="text" id="PayToCityStateZip" value="@Model.PayToCity @Model.PayToState   @Model.PayToZip" style="z-index: 1; left: @(col3Left)px; top: @(row20Top + 60)px; position: absolute; width: 280px; right: 93px; height: 5px;" />

     </div>' )) 
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL
END