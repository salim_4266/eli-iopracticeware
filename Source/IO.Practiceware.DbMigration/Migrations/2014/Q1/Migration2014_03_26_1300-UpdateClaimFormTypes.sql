﻿SET IDENTITY_INSERT model.TemplateDocuments
ON

DECLARE @TemplateDocumentId int

SELECT @TemplateDocumentId = Id FROM
model.TemplateDocuments td
WHERE td.DisplayName = 'CMS-1500-pre-2014'

INSERT INTO model.ClaimFormTypes (Name, MethodSentId, TemplateDocumentId) VALUES ('CMS-1500-pre-2014',2,@TemplateDocumentId)

SET IDENTITY_INSERT model.TemplateDocuments
OFF