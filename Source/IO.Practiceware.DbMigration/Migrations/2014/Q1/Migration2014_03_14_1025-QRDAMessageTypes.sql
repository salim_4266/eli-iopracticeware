﻿SELECT 1 AS Value INTO #NoCheck

SET IDENTITY_INSERT model.ExternalSystemMessageTypes ON 
	
IF NOT EXISTS(SELECT * FROM model.ExternalSystemMessageTypes WHERE Id = 31)
INSERT INTO model.ExternalSystemMessageTypes (Id, Name, Description, IsOutbound)
VALUES (31, 'QRDA_Category_I_Outbound', 'QRDA_Category I', CONVERT(bit, 1))

IF NOT EXISTS(SELECT * FROM model.ExternalSystemMessageTypes WHERE Id = 32)
INSERT INTO model.ExternalSystemMessageTypes (Id, Name, Description, IsOutbound)
VALUES (32, 'QRDA_Category_III_Outbound', 'QRDA_Category III', CONVERT(bit, 1))

SET IDENTITY_INSERT model.ExternalSystemMessageTypes OFF

IF NOT EXISTS (
	SELECT TOP 1 [Id] 
	FROM [model].[ExternalSystemExternalSystemMessageTypes]
	WHERE [ExternalSystemMessageTypeId] = 31 AND [ExternalSystemId] =  1
)
BEGIN
	INSERT INTO [model].[ExternalSystemExternalSystemMessageTypes]
           ([ExternalSystemId]
           ,[ExternalSystemMessageTypeId]
           ,[IsDisabled])
     SELECT 1,31,CONVERT(BIT,0) -- IO Practiceware well-known id, QRDA_Category_I_Outbound
END

IF NOT EXISTS (
	SELECT TOP 1 [Id] 
	FROM [model].[ExternalSystemExternalSystemMessageTypes]
	WHERE [ExternalSystemMessageTypeId] = 32 AND [ExternalSystemId] =  1
)
BEGIN
	INSERT INTO [model].[ExternalSystemExternalSystemMessageTypes]
           ([ExternalSystemId]
           ,[ExternalSystemMessageTypeId]
           ,[IsDisabled])     
	SELECT 1,32,CONVERT(BIT,0) -- IO Practiceware well-known id, QRDA_Category_III_Outbound
END

DROP TABLE #NoCheck