﻿DECLARE c CURSOR
FOR
SELECT ResourceId
FROM dbo.resources
WHERE resourcetype = 'L'

OPEN c

DECLARE @uid INT

FETCH NEXT
FROM c
INTO @uid

WHILE @@Fetch_Status = 0
BEGIN
	IF NOT EXISTS (
			SELECT *
			FROM model.applicationsettings
			WHERE UserId = @uid
				AND NAME = 'UserWidgetLayoutSettings'
			)
	BEGIN
		INSERT INTO model.applicationsettings (
			Value
			,NAME
			,ApplicationSettingTypeId
			,UserId
			)
		VALUES (
			'<z:anyType xmlns:i="http://www.w3.org/2001/XMLSchema-instance" z:Id="1" xmlns:d1p1="http://schemas.datacontract.org/2004/07/IO.Practiceware.Presentation.ViewModels.Home" i:type="d1p1:UserWidgetLayoutSettings" xmlns:z="http://schemas.microsoft.com/2003/10/Serialization/">    <d1p1:Containers z:Id="2" z:Size="1">      <d1p1:UserWidgetLayoutSettings.WidgetContainer z:Id="3">        <d1p1:Name z:Id="4">CenterContainer</d1p1:Name>        <d1p1:Widgets z:Id="5" z:Size="2">          <d1p1:UserWidgetLayoutSettings.WidgetSettings z:Id="6">            <d1p1:CustomData z:Id="7" i:type="d1p1:UserPatientSearchWidgetSettings">              <d1p1:PatientInfoTabToOpen>Demographics</d1p1:PatientInfoTabToOpen>            </d1p1:CustomData>            <d1p1:WidgetTemplateKey z:Id="8">PatientSearch</d1p1:WidgetTemplateKey>          </d1p1:UserWidgetLayoutSettings.WidgetSettings>          <d1p1:UserWidgetLayoutSettings.WidgetSettings z:Id="9">            <d1p1:CustomData z:Id="10" i:type="d1p1:UserFeatureSelectionSettings">              <d1p1:Features z:Id="11" z:Size="20">                <d1p1:UserFeatureSelectionSettings.Feature z:Id="12">                  <d1p1:FeatureTypeKey z:Id="13">Administrative_Calendar</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="14">                  <d1p1:FeatureTypeKey z:Id="15">Administrative_Check In</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="16">                  <d1p1:FeatureTypeKey z:Id="17">Administrative_Check Out</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="18">                  <d1p1:FeatureTypeKey z:Id="19">Administrative_Patient Locator</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="20">                  <d1p1:FeatureTypeKey z:Id="21">Billing_Pending Claims</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="22">                  <d1p1:FeatureTypeKey z:Id="23">Billing_Submit Claims</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="24">                  <d1p1:FeatureTypeKey z:Id="25">Billing_Patient Statements</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="26">                  <d1p1:FeatureTypeKey z:Id="27">Billing_Patient Payments</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="28">                  <d1p1:FeatureTypeKey z:Id="29">Billing_Insurer Payments</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="30">                  <d1p1:FeatureTypeKey z:Id="31">Billing_Billing Sent</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="32">                  <d1p1:FeatureTypeKey z:Id="33">Billing_Remittances</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="34">                  <d1p1:FeatureTypeKey z:Id="35">Billing_Denials</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="36">                  <d1p1:FeatureTypeKey z:Id="37">Billing_Appeals</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="38">                  <d1p1:FeatureTypeKey z:Id="39">Billing_Unassigned Payments</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="40">                  <d1p1:FeatureTypeKey z:Id="41">Billing_Unallocated Balances</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="42">                  <d1p1:FeatureTypeKey z:Id="43">Administrative_Notes</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="44">                  <d1p1:FeatureTypeKey z:Id="45">Administrative_Reports</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="46">                  <d1p1:FeatureTypeKey z:Id="47">Administrative_Clock In</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="48">                  <d1p1:FeatureTypeKey z:Id="49">Administrative_Clock Out</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>                <d1p1:UserFeatureSelectionSettings.Feature z:Id="50">                  <d1p1:FeatureTypeKey z:Id="51">Administrative_Internet</d1p1:FeatureTypeKey>                </d1p1:UserFeatureSelectionSettings.Feature>              </d1p1:Features>            </d1p1:CustomData>            <d1p1:WidgetTemplateKey z:Id="52">FeatureAccess</d1p1:WidgetTemplateKey>          </d1p1:UserWidgetLayoutSettings.WidgetSettings>        </d1p1:Widgets>      </d1p1:UserWidgetLayoutSettings.WidgetContainer>    </d1p1:Containers>    <d1p1:LayoutTemplateKey i:nil="true" />  </z:anyType>'
			,'UserWidgetLayoutSettings'
			,10
			,@uid
			)
	END

	IF NOT EXISTS (
			SELECT *
			FROM model.applicationsettings
			WHERE UserId = @uid
				AND NAME = 'UserFeatureSelectionSettings'
			)
	BEGIN
		INSERT INTO model.applicationsettings (
			Value
			,NAME
			,ApplicationSettingTypeId
			,UserId
			)
		VALUES (
			'<z:anyType xmlns:i="http://www.w3.org/2001/XMLSchema-instance" z:Id="1" xmlns:d1p1="http://schemas.datacontract.org/2004/07/IO.Practiceware.Presentation.ViewModels.Home" i:type="d1p1:UserFeatureSelectionSettings" xmlns:z="http://schemas.microsoft.com/2003/10/Serialization/">    <d1p1:Features z:Id="2" z:Size="20">      <d1p1:UserFeatureSelectionSettings.Feature z:Id="3">        <d1p1:FeatureTypeKey z:Id="4">Administrative_Calendar</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="5">        <d1p1:FeatureTypeKey z:Id="6">Administrative_Check In</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="7">        <d1p1:FeatureTypeKey z:Id="8">Administrative_Check Out</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="9">        <d1p1:FeatureTypeKey z:Id="10">Administrative_Patient Locator</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="11">        <d1p1:FeatureTypeKey z:Id="12">Billing_Pending Claims</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="13">        <d1p1:FeatureTypeKey z:Id="14">Billing_Submit Claims</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="15">        <d1p1:FeatureTypeKey z:Id="16">Billing_Patient Statements</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="17">        <d1p1:FeatureTypeKey z:Id="18">Billing_Patient Payments</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="19">        <d1p1:FeatureTypeKey z:Id="20">Billing_Insurer Payments</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="21">        <d1p1:FeatureTypeKey z:Id="22">Billing_Billing Sent</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="23">        <d1p1:FeatureTypeKey z:Id="24">Billing_Remittances</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="25">        <d1p1:FeatureTypeKey z:Id="26">Billing_Denials</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="27">        <d1p1:FeatureTypeKey z:Id="28">Billing_Appeals</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="29">        <d1p1:FeatureTypeKey z:Id="30">Billing_Unassigned Payments</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="31">        <d1p1:FeatureTypeKey z:Id="32">Billing_Unallocated Balances</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="33">        <d1p1:FeatureTypeKey z:Id="34">Administrative_Notes</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="35">        <d1p1:FeatureTypeKey z:Id="36">Administrative_Reports</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="37">        <d1p1:FeatureTypeKey z:Id="38">Administrative_Clock In</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="39">        <d1p1:FeatureTypeKey z:Id="40">Administrative_Clock Out</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>      <d1p1:UserFeatureSelectionSettings.Feature z:Id="41">        <d1p1:FeatureTypeKey z:Id="42">Administrative_Internet</d1p1:FeatureTypeKey>      </d1p1:UserFeatureSelectionSettings.Feature>    </d1p1:Features>  </z:anyType>'
			,'UserFeatureSelectionSettings'
			,9
			,@uid
			)
	END

	FETCH NEXT
	FROM c
	INTO @uid
END

CLOSE c

DEALLOCATE c