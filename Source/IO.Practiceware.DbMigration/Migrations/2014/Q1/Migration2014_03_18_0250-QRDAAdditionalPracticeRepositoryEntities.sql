﻿IF NOT EXISTS (SELECT * FROM model.PracticeRepositoryEntities WHERE Name = 'BillingService')
BEGIN
INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName) VALUES (64,'BillingService','Id')
END
GO

IF NOT EXISTS (SELECT * FROM model.PracticeRepositoryEntities WHERE Name = 'PatientExamPerformed')
BEGIN
INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName) VALUES (65,'PatientExamPerformed','Id')
END
GO

IF NOT EXISTS (SELECT * FROM model.PracticeRepositoryEntities WHERE Name = 'PatientInterventionPerformed')
BEGIN
INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName) VALUES (66,'PatientInterventionPerformed','Id')
END
GO

IF NOT EXISTS (SELECT * FROM model.PracticeRepositoryEntities WHERE Name = 'EncounterMedicationOrder')
BEGIN
INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName) VALUES (67,'EncounterMedicationOrder','Id')
END
GO

IF NOT EXISTS (SELECT * FROM model.PracticeRepositoryEntities WHERE Name = 'Laterality')
BEGIN
INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName) VALUES (70,'Laterality','Id')
END
GO

IF NOT EXISTS (SELECT * FROM model.PracticeRepositoryEntities WHERE Name = 'QuestionAnswer')
BEGIN
INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName) VALUES (71,'QuestionAnswer','Id')
END
GO

IF NOT EXISTS (SELECT * FROM model.PracticeRepositoryEntities WHERE Name = 'QuestionAnswerValue')
BEGIN
INSERT INTO model.PracticeRepositoryEntities (Id,Name,KeyPropertyName) VALUES (72,'QuestionAnswerValue','Id')
END
GO

  