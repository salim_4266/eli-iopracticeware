﻿IF OBJECT_ID(N'[model].[PatientCommunicationPreferences]', 'U') IS NOT NULL	
BEGIN
	-- add column to IsOptOut
	IF NOT EXISTS(select * from sys.columns where Name = N'IsOptOut' and Object_ID = Object_ID(N'model.PatientCommunicationPreferences')) 
	BEGIN
		ALTER TABLE model.PatientCommunicationPreferences ADD [IsOptOut] bit NOT NULL DEFAULT(0)
	END	
END
GO