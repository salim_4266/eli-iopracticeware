﻿CREATE TABLE #tempCountries (Abbreviation NVARCHAR(max), Name NVARCHAR(max), CountryId NVARCHAR(max))

INSERT INTO #tempCountries (Abbreviation, Name, CountryId)

SELECT 'AG' AS Abbreviation, 'Aguascalientes' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'BC' AS Abbreviation, 'Baja California' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'BS' AS Abbreviation, 'Baja California Sur' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'CM' AS Abbreviation, 'Campeche' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'CS' AS Abbreviation, 'Chiapas' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'CH' AS Abbreviation, 'Chihuahua' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'CO' AS Abbreviation, 'Coahuila' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'CL' AS Abbreviation, 'Colima' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'DG' AS Abbreviation, 'Durango' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'GT' AS Abbreviation, 'Guanajuato' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'GR' AS Abbreviation, 'Guerrero' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'HG' AS Abbreviation, 'Hidalgo' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'JA' AS Abbreviation, 'Jalisco' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'ME' AS Abbreviation, 'México' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'DF' AS Abbreviation, 'Mexico City (Federal District)' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'MI' AS Abbreviation, 'Michoacán' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'MO' AS Abbreviation, 'Morelos' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'NA' AS Abbreviation, 'Nayarit' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'NL' AS Abbreviation, 'Nuevo León' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'OA' AS Abbreviation, 'Oaxaca' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'PU' AS Abbreviation, 'Puebla' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'SL' AS Abbreviation, 'San Luis Potosí' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'SI' AS Abbreviation, 'Sinaloa' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'SO' AS Abbreviation, 'Sonora' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'TB' AS Abbreviation, 'Tabasco' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'TM' AS Abbreviation, 'Tamaulipas' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'TL' AS Abbreviation, 'Tlaxcala' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'QE' AS Abbreviation, 'Querétaro' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'QR' AS Abbreviation, 'Quintana Roo' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'VE' AS Abbreviation, 'Veracruz' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'YU' AS Abbreviation, 'Yucatán' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'
UNION ALL
SELECT 'ZA' AS Abbreviation, 'Zacatecas' AS Name, Id AS CountryId FROM model.Countries WHERE Name = 'Mexico'


INSERT INTO model.StateOrProvinces (Abbreviation, Name, CountryId)
SELECT t.Abbreviation, t.Name, t.CountryId FROM #tempCountries t
LEFT JOIN model.StateOrProvinces sp ON sp.Name = t.Name AND t.CountryId = sp.CountryId
WHERE sp.Id IS NULL

DROP TABLE #tempCountries
