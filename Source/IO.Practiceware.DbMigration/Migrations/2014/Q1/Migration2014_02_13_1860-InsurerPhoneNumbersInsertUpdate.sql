﻿IF EXISTS (SELECT * FROM sys.triggers WHERE Name = 'OnInsurerPhoneNumbersInsertOrUpdate') 
	DROP TRIGGER [model].[OnInsurerPhoneNumbersInsertOrUpdate];
GO

CREATE TRIGGER [OnInsurerPhoneNumbersInsertOrUpdate] ON model.InsurerPhoneNumbers
AFTER UPDATE, INSERT
AS
BEGIN
DECLARE @msrepl_object_id int
SET @msrepl_object_id = OBJECT_ID('[model].[InsurerPhoneNumbers]')
DECLARE @retcode int
EXEC @retcode = sp_check_for_sync_trigger @msrepl_object_id
IF @retcode = 1 RETURN
IF EXISTS (
SELECT * FROM inserted
EXCEPT
SELECT * FROM deleted
)
BEGIN
	UPDATE pins
	SET pins.InsurerPhone = 
	CASE 
		WHEN i.InsurerPhoneNumberTypeId = (SELECT TOP 1 Id FROM model.InsurerPhoneNumberTypes WHERE Name = 'Main')
			THEN 
				CASE 
					WHEN dbo.IsNullOrEmpty(i.ExchangeAndSuffix) = 0 
						THEN 
							CASE WHEN dbo.IsNullOrEmpty(i.AreaCode) = 0 THEN i.AreaCode+'-'+i.ExchangeAndSuffix ELSE i.ExchangeAndSuffix END
					ELSE ''
				END
	END
		,pins.InsurerPrecPhone = 
	CASE 
		WHEN i.InsurerPhoneNumberTypeId = (SELECT TOP 1 Id FROM model.InsurerPhoneNumberTypes WHERE Name = 'Authorization')
			THEN 
				CASE 
					WHEN dbo.IsNullOrEmpty(i.ExchangeAndSuffix) = 0 
						THEN 
							CASE WHEN dbo.IsNullOrEmpty(i.AreaCode) = 0 THEN i.AreaCode+'-'+i.ExchangeAndSuffix ELSE i.ExchangeAndSuffix END
					ELSE ''
				END
	END
		,pins.InsurerEligPhone = 
	CASE 
		WHEN i.InsurerPhoneNumberTypeId = (SELECT TOP 1 Id FROM model.InsurerPhoneNumberTypes WHERE Name = 'Eligibility')
			THEN 
				CASE 
					WHEN dbo.IsNullOrEmpty(i.ExchangeAndSuffix) = 0 
						THEN 
							CASE WHEN dbo.IsNullOrEmpty(i.AreaCode) = 0 THEN i.AreaCode+'-'+i.ExchangeAndSuffix ELSE i.ExchangeAndSuffix END
					ELSE ''
				END
	END
		,pins.InsurerProvPhone = 
	CASE 
		WHEN i.InsurerPhoneNumberTypeId = (SELECT TOP 1 Id FROM model.InsurerPhoneNumberTypes WHERE Name = 'Provider')
			THEN 
				CASE 
					WHEN dbo.IsNullOrEmpty(i.ExchangeAndSuffix) = 0 
						THEN 
							CASE WHEN dbo.IsNullOrEmpty(i.AreaCode) = 0 THEN i.AreaCode+'-'+i.ExchangeAndSuffix ELSE i.ExchangeAndSuffix END
					ELSE ''
				END
	END
			,pins.InsurerClaimPhone = 
	CASE 
		WHEN i.InsurerPhoneNumberTypeId = (SELECT TOP 1 Id FROM model.InsurerPhoneNumberTypes WHERE Name = 'Claims')
			THEN 
				CASE 
					WHEN dbo.IsNullOrEmpty(i.ExchangeAndSuffix) = 0 
						THEN 
							CASE WHEN dbo.IsNullOrEmpty(i.AreaCode) = 0 THEN i.AreaCode+'-'+i.ExchangeAndSuffix ELSE i.ExchangeAndSuffix END
					ELSE ''
				END
	END
	FROM dbo.PracticeInsurers pins
	INNER JOIN inserted i ON i.InsurerId = pins.InsurerId
END
END
GO

