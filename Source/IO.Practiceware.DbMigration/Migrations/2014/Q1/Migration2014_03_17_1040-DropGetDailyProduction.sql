﻿--Bug 9940.

--After running TestStoredProcedures.sql,found that GetDailyProduction doesn't validate.So dropping the procedure

IF EXISTS (Select * from sysobjects where Id =OBJECT_ID('[model].[GetDailyProduction]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
	EXEC('DROP PROCEDURE model.GetDailyProduction')
END	
GO