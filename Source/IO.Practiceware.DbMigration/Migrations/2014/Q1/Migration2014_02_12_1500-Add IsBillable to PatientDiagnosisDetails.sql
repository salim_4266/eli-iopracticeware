﻿IF NOT EXISTS (
SELECT 
COLUMN_NAME
FROM information_schema.COLUMNS 
WHERE TABLE_SCHEMA = 'model'
AND TABLE_NAME ='PatientDiagnosisDetails'
AND COLUMN_NAME = 'IsBillable'
AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientDiagnosisDetails'),COLUMN_NAME,'IsIdentity') = 0
AND DATA_TYPE = 'bit'
AND IS_NULLABLE = 'NO'
) 
ALTER TABLE model.PatientDiagnosisDetails
ADD IsBillable bit DEFAULT(CONVERT(BIT,0)) NOT NULL


