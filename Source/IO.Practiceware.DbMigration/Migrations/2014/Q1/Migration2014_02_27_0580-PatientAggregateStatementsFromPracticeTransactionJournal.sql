﻿ALTER TABLE dbo.PracticeTransactionJournal DISABLE TRIGGER ALL
;WITH CTE AS (
SELECT 
ptjb.TransactionId
,ptjb.NewTransactionTypeId
FROM PracticeTransactionJournalReceivablesBackup ptjb 
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionId = ptjb.TransactionId
	AND ptj.TransactionType = 'R' 
	AND TransactionRef = 'G' 
	AND ptj.TransactionStatus = 'Z' 
	AND TransactionAction = 'P'
INNER JOIN model.InvoiceReceivables ir ON ir.Id = ptjb.NewTransactionTypeId
	AND ir.PatientInsuranceId IS NULL
)
UPDATE ptj
SET ptj.TransactionTypeId = CTE.NewTransactionTypeId
FROM PracticeTransactionJournal ptj 
INNER JOIN CTE ON CTE.TransactionId = ptj.TransactionId

ALTER TABLE dbo.PracticeTransactionJournal ENABLE TRIGGER ALL