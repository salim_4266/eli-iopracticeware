﻿--This is to migrate emergency relationships. If the there is no name or phone number the relationships will not be migrated.

UPDATE patc
SET patc.Value = patc.Value + 'Emergency Relationship: ' + 
	CASE SUBSTRING(pct.Code, 1, 1)
		WHEN 'C'
			THEN 'Child'
		WHEN 'D'
			THEN 'Dependent'
		WHEN 'O'
			THEN 'Other'
		WHEN 'P'
			THEN 'Parent'
		WHEN 'S'
			THEN 'Spouse'
		WHEN 'Y'
			THEN 'Yourself'
		ELSE 'Other' 
		END
FROM model.PatientComments patc
INNER JOIN dbo.PatientDemographicsTable pd ON pd.PatientId = patc.PatientId
INNER JOIN dbo.PracticeCodeTable pct ON SUBSTRING(pct.Code, 1, 1) = pd.EmergencyRel AND pct.ReferenceType = 'RELATIONSHIP'
WHERE dbo.IsNullOrEmpty(EmergencyRel) = 0 
	AND patc.PatientCommentTypeId = 2
