﻿IF OBJECT_ID('#NoAudit') IS NULL
	SELECT 1 AS Id INTO #NoAudit
GO

IF NOT EXISTS(
SELECT 
Code 
FROM dbo.PracticeCodeTable pct 
WHERE ReferenceType = 'REFERBY'
AND Code = 'Other'
)
INSERT INTO dbo.PracticeCodeTable(
      [ReferenceType]
      ,[Code]
      ,[AlternateCode]
      ,[Exclusion]
      ,[LetterTranslation]
      ,[OtherLtrTrans]
      ,[FormatMask]
      ,[Signature]
      ,[TestOrder]
      ,[FollowUp]
      ,[Rank]
      ,[WebsiteUrl]
      )
SELECT 
'REFERBY' AS [ReferenceType]
,'Other' AS [Code]
,NULL AS [AlternateCode]
,NULL AS [Exclusion]
,NULL AS [LetterTranslation]
,NULL AS [OtherLtrTrans]
,NULL AS [FormatMask]
,NULL AS [Signature]
,NULL AS [TestOrder]
,NULL AS [FollowUp]
,1 AS [Rank]
,NULL AS [WebsiteUrl]

DECLARE @OtherReferById INT
SELECT @OtherReferById = Id FROM dbo.PracticeCodeTable pct WHERE ReferenceType = 'REFERBY' AND Code = 'Other'

IF NOT EXISTS(SELECT Id FROM model.PatientReferralSourceTypes WHERE Name = 'Other')
BEGIN
IF (@OtherReferById IS NOT NULL)
	BEGIN
		INSERT INTO model.PatientReferralSourceTypes (Name,IsArchived,OrdinalId)
		SELECT
		'Other' AS Name
		,CONVERT(BIT,0) AS IsArchived
		,MAX(OrdinalId) + 1 AS OrdinalId
		FROM model.PatientReferralSourceTypes
	END
END

SELECT @OtherReferById = Id FROM model.PatientReferralSourceTypes WHERE Name = 'Other'

INSERT INTO model.PatientReferralSources (PatientId, ReferralSourceTypeId, Comment)
SELECT 	
pd.PatientId AS PatientId
,@OtherReferById AS ReferralSourceTypeId
,ProfileComment2 AS Comment
FROM dbo.PatientDemographicsTable pd
LEFT JOIN dbo.PracticeCodeTable pct ON pct.Code = pd.ReferralCatagory
	AND ReferenceType = 'REFERBY'
WHERE pct.Id IS NULL AND dbo.IsNullOrEmpty(ProfileComment2) = 0