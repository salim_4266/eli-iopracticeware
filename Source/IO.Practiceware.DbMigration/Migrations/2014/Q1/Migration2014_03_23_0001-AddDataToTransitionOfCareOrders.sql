﻿--Add a row to TransitionOfCareOrders so we can populate FK in model.EncounterTransitionOfCareOrders.

SET IDENTITY_INSERT [model].TransitionOfCareOrders ON
IF NOT EXISTS (SELECT Id FROM model.TransitionOfCareOrders WHERE Name = 'All Orders' ) INSERT INTO model.TransitionOfCareOrders (Id, Name, OrdinalId, IsExcludedFromCommunications, IsFollowUpRequired, SimpleDescription, TechnicalDescription) VALUES (1, 'All Orders', 1, 1, 1, 'All transition of care orders', 'All transition of care orders')
SET IDENTITY_INSERT [model].TransitionOfCareOrders OFF