﻿/* Change empty value in the Ethnicity to "Patient Declines" */

IF EXISTS (SELECT * FROM model.PatientEthnicities WHERE Name = '')
BEGIN
	IF NOT EXISTS (SELECT * FROM model.PatientEthnicities WHERE Name = 'Patient Declines')
	BEGIN
		--If there are multiple blank rows, we should consolidate those rows to 1 Patient Declines row
		UPDATE model.PatientEthnicities	SET Name= 'Patient Declines' WHERE Id = (SELECT Top 1 Id from model.PatientEthnicities WHERE Name = '');
		
		UPDATE model.Patients SET PatientEthnicityId = (SELECT Id from model.PatientEthnicities WHERE Name = 'Patient Declines') 
		WHERE Id in (SELECT Id from model.PatientEthnicities WHERE Name = '');
		--
		DELETE FROM model.PatientEthnicities WHERE Name = '';
	END
END
GO
