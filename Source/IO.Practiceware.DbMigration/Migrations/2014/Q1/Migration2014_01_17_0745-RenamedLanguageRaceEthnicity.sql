﻿--Fix for Bug #8677.
--Check if new model.PatientRace table has already rows present.if not create new rows in the table
IF OBJECT_ID(N'[model].[PatientPatientRace]', 'U') IS NOT NULL
BEGIN
	DECLARE @Sql NVARCHAR(MAX);

	SET @Sql = 'INSERT INTO model.PatientPatientRace 
				SELECT p.Id,p.PatientRaceId FROM model.Patients p
				LEFT JOIN model.PatientPatientRace pr on p.Id = pr.Patients_Id AND p.PatientRaceId =pr.PatientRaces_Id 
				WHERE pr.Patients_Id IS NULL AND p.PatientRaceId IS NOT NULL'

	EXEC (@Sql)
END

--For PatientEthnicity
IF EXISTS (
		SELECT Id
		FROM model.PracticeRepositoryEntities
		WHERE NAME = 'PatientEthnicity'
		)
BEGIN
	UPDATE model.PracticeRepositoryEntities
	SET NAME = 'Ethnicity'
	WHERE Id = 11
END

--PatientEthnicities
IF OBJECT_ID(N'[model].[PatientEthnicities]', 'U') IS NOT NULL
BEGIN
	-- rename column to PatientEthnicityId
	IF EXISTS (
			SELECT *
			FROM sys.columns
			WHERE NAME = N'PatientEthnicityId'
				AND Object_ID = Object_ID(N'model.PatientEthnicities')
			)
	BEGIN
		EXEC sp_rename '[model].[PatientEthnicities].[PatientEthnicityId]'
			,'EthnicityId'
			,'COLUMN';
	END
END

IF OBJECT_ID(N'[model].[PK_PatientEthnicities]', 'PK') IS NOT NULL
BEGIN
	-- Rename the primary key constraint.
	EXEC sp_rename '[model].[PK_PatientEthnicities]'
		,'PK_Ethnicities';
END

IF EXISTS (
		SELECT *
		FROM sysindexes
		WHERE NAME = 'IX_FK_PatientEthnicityPatient'
		)
BEGIN
	-- Rename the Index.
	EXEC sp_rename '[model].[Patients].[IX_FK_PatientEthnicityPatient]'
		,'IX_FK_EthnicityPatient'
		,'INDEX';
END

IF OBJECT_ID(N'[model].[Patients]', 'U') IS NOT NULL
BEGIN
	-- rename column to PatientEthnicityId
	IF EXISTS (
			SELECT *
			FROM sys.columns
			WHERE NAME = N'PatientEthnicityId'
				AND Object_ID = Object_ID(N'model.Patients')
			)
	BEGIN
		EXEC sp_rename '[model].[Patients].[PatientEthnicityId]'
			,'EthnicityId'
			,'COLUMN';
	END
END

IF OBJECT_ID(N'[model].[PatientEthnicities]', 'U') IS NOT NULL
BEGIN
	--Rename the PatientEthicities
	EXEC sp_rename '[model].[PatientEthnicities]'
		,'Ethnicities';
END

--Drop Audit Trigger's
IF OBJECT_ID(N'[model].[AuditPatientEthnicitiesDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientEthnicitiesDeleteTrigger];
END

IF OBJECT_ID(N'[model].[AuditPatientEthnicitiesInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientEthnicitiesInsertTrigger];
END

IF OBJECT_ID(N'[model].[AuditPatientEthnicitiesUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientEthnicitiesUpdateTrigger];
END
GO

------------------------------------------------------------------------------PatientRaces--------------------------------------------------------------------------------
IF OBJECT_ID(N'[model].[PK_PatientRaces]', 'PK') IS NOT NULL
BEGIN
	-- Rename the primary key constraint.
	EXEC sp_rename '[model].[PK_PatientRaces]'
		,'PK_Races';
END

--DROP PatientRace Index on model.Patients
IF EXISTS (
		SELECT *
		FROM sysindexes
		WHERE NAME = 'IX_FK_PatientRacePatient'
		)
BEGIN
	-- Drop the Index.
	DROP INDEX [model].[Patients].[IX_FK_PatientRacePatient]
END

--Drop Forgein Key
DECLARE @KeyName NVARCHAR(MAX);

SELECT TOP 1 @KeyName = f.NAME
FROM sys.foreign_keys AS f
INNER JOIN sys.foreign_key_columns AS fc ON f.OBJECT_ID = fc.constraint_object_id
INNER JOIN sys.tables t ON t.OBJECT_ID = fc.referenced_object_id
WHERE OBJECT_NAME(f.referenced_object_id) = 'PatientRaces'
	AND COL_NAME(fc.parent_object_id, fc.parent_column_id) = 'PatientRaceId'

IF @KeyName IS NOT NULL
	OR @KeyName <> ''
BEGIN
	EXEC ('ALTER TABLE [model].[Patients] DROP CONSTRAINT ' + @KeyName + '');
END

--DROP THE CLOUMN PatientRaceId
IF OBJECT_ID(N'[model].[Patients]', 'U') IS NOT NULL
BEGIN
	-- rename column to PatientRaceId
	IF EXISTS (
			SELECT *
			FROM sys.columns
			WHERE NAME = N'PatientRaceId'
				AND Object_ID = Object_ID(N'model.Patients')
			)
	BEGIN
		ALTER TABLE model.Patients

		DROP COLUMN PatientRaceId;
	END
END

--Change PatientDemographics VIEW
IF EXISTS (
		SELECT Id
		FROM model.PracticeRepositoryEntities
		WHERE NAME = 'PatientRace'
		)
BEGIN
	UPDATE model.PracticeRepositoryEntities
	SET NAME = 'Race'
	WHERE Id = 37
END

IF EXISTS (
		SELECT Id
		FROM model.ExternalSystemEntities
		WHERE NAME = 'PatientRace'
		)
BEGIN
	UPDATE model.ExternalSystemEntities
	SET NAME = 'Race'
	WHERE NAME = 'PatientRace'
END

IF OBJECT_ID(N'[model].[PatientRaces]', 'U') IS NOT NULL
BEGIN
	--Rename the PaitentRace
	EXEC sp_rename '[model].[PatientRaces]'
		,'Races';
END

--Drop Audit Trigger's
IF OBJECT_ID(N'[model].[AuditPatientRacesDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientRacesDeleteTrigger];
END

IF OBJECT_ID(N'[model].[AuditPatientRacesInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientRacesInsertTrigger];
END

IF OBJECT_ID(N'[model].[AuditPatientRacesUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientRacesUpdateTrigger];
END
GO

----------------------------------------------------------------PatientPatientRace---------------------------------------------------------
IF OBJECT_ID(N'[model].[PK_PatientPatientRace]', 'PK') IS NOT NULL
BEGIN
	-- Rename the primary key constraint.
	EXEC sp_rename '[model].[PK_PatientPatientRace]'
		,'[PK_PatientRace]';
END

IF OBJECT_ID(N'[model].[FK_PatientPatientRace_Patient]', 'F') IS NOT NULL
BEGIN
	-- Rename the Foreign key constraint.
	EXEC sp_rename '[model].[FK_PatientPatientRace_Patient]'
		,'FK_PatientRace_Patient';
END

IF OBJECT_ID(N'[model].[FK_PatientPatientRace_PatientRace]', 'F') IS NOT NULL
BEGIN
	-- Rename the Foreign key constraint.
	EXEC sp_rename '[model].[FK_PatientPatientRace_PatientRace]'
		,'FK_PatientRace_Race';
END

IF EXISTS (
		SELECT *
		FROM sysindexes
		WHERE NAME = 'IX_FK_PatientPatientRace_Patient'
		)
BEGIN
	-- Rename the Index.
	EXEC sp_rename '[model].[PatientPatientRace].[IX_FK_PatientPatientRace_Patient]'
		,'IX_FK_PatientRace_Patient'
		,'INDEX';
END

IF EXISTS (
		SELECT *
		FROM sysindexes
		WHERE NAME = 'IX_FK_PatientPatientRacePatientRace'
		)
BEGIN
	-- Rename the Index.
	EXEC sp_rename '[model].[PatientPatientRace].[IX_FK_PatientPatientRacePatientRace]'
		,'IX_FK_PatientRaceRace'
		,'INDEX';
END

IF EXISTS (
		SELECT *
		FROM sys.columns
		WHERE NAME = N'PatientRaces_Id'
			AND Object_ID = Object_ID(N'model.PatientPatientRace')
		)
BEGIN
	EXEC sp_rename '[model].[PatientPatientRace].[PatientRaces_Id]'
		,'Races_Id'
		,'COLUMN';
END

IF OBJECT_ID(N'[model].[PatientPatientRace]', 'U') IS NOT NULL
BEGIN
	--Rename the PatientPatientRace
	EXEC sp_rename '[model].[PatientPatientRace]'
		,'PatientRace';
END

--Drop Patient Audit Trigger's
IF OBJECT_ID(N'[model].[AuditPatientPatientRaceDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientPatientRaceDeleteTrigger];
END

IF OBJECT_ID(N'[model].[AuditPatientPatientRaceInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientPatientRaceInsertTrigger];
END

IF OBJECT_ID(N'[model].[AuditPatientPatientRaceUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientPatientRaceUpdateTrigger];
END
GO

------------------------------------------------------------------PatientLanguages-------------------------------------------------------------
IF EXISTS (
		SELECT Id
		FROM model.PracticeRepositoryEntities
		WHERE NAME = 'PatientLanguage'
		)
BEGIN
	UPDATE model.PracticeRepositoryEntities
	SET NAME = 'Language'
	WHERE Id = 9
END

IF EXISTS (
		SELECT Id
		FROM model.ExternalSystemEntities
		WHERE NAME = 'PatientLanguage'
		)
BEGIN
	UPDATE model.ExternalSystemEntities
	SET NAME = 'Language'
	WHERE NAME = 'PatientLanguage'
END

IF OBJECT_ID(N'[model].[PatientLanguages]', 'U') IS NOT NULL
BEGIN
	-- rename column to PatientLanguageId
	IF EXISTS (
			SELECT *
			FROM sys.columns
			WHERE NAME = N'PatientLanguageId'
				AND Object_ID = Object_ID(N'model.PatientLanguages')
			)
	BEGIN
		EXEC sp_rename '[model].[PatientLanguages].[PatientLanguageId]'
			,'LanguageId'
			,'COLUMN';
	END
END

IF OBJECT_ID(N'[model].[PK_PatientLanguages]', 'PK') IS NOT NULL
BEGIN
	-- Rename the primary key constraint.
	EXEC sp_rename '[model].[PK_PatientLanguages]'
		,'PK_Languages';
END

IF EXISTS (
		SELECT *
		FROM sysindexes
		WHERE NAME = 'IX_FK_PatientLanguagePatient'
		)
BEGIN
	-- Rename the Index.
	EXEC sp_rename '[model].[Patients].[IX_FK_PatientLanguagePatient]'
		,'IX_FK_LanguagePatient'
		,'INDEX';
END

IF OBJECT_ID(N'[model].[Patients]', 'U') IS NOT NULL
BEGIN
	-- rename column to PatientEthnicityId
	IF EXISTS (
			SELECT *
			FROM sys.columns
			WHERE NAME = N'PatientLanguageId'
				AND Object_ID = Object_ID(N'model.Patients')
			)
	BEGIN
		EXEC sp_rename '[model].[Patients].[PatientLanguageId]'
			,'LanguageId'
			,'COLUMN';
	END
END

IF OBJECT_ID(N'[model].[PatientLanguages]', 'U') IS NOT NULL
BEGIN
	--Rename the PatientLanguages
	EXEC sp_rename '[model].[PatientLanguages]'
		,'Languages';
END

--Drop Audit Trigger's
IF OBJECT_ID(N'[model].[AuditPatientLanguagesDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientLanguagesDeleteTrigger];
END

IF OBJECT_ID(N'[model].[AuditPatientLanguagesInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientLanguagesInsertTrigger];
END

IF OBJECT_ID(N'[model].[AuditPatientLanguagesUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientLanguagesUpdateTrigger];
END
GO

--Drop Patient Audit Trigger's
IF OBJECT_ID(N'[model].[AuditPatientsDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientsDeleteTrigger];
END

IF OBJECT_ID(N'[model].[AuditPatientsInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientsInsertTrigger];
END

IF OBJECT_ID(N'[model].[AuditPatientsUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientsUpdateTrigger];
END
GO

--Change PatientDemoGraphics View.
IF (OBJECT_ID('dbo.PatientDemographics', 'V') IS NOT NULL)
BEGIN
	DROP VIEW [dbo].[PatientDemographics]
END
GO

CREATE VIEW [dbo].[PatientDemographics]	WITH VIEW_METADATA
	AS

	SELECT 
		p.Id AS PatientId,
		CASE
			WHEN DefaultUserId IS NULL
				THEN 0
			ELSE
				DefaultUserId
			END AS SchedulePrimaryDoctor,
		FirstName AS FirstName,
		CASE
			WHEN IsClinical = 0
				THEN 'I'
			WHEN PatientStatusId = 2
				THEN 'C'
			WHEN PatientStatusId = 3
				THEN 'D'
			WHEN PatientStatusId = 4
				THEN 'M'
			ELSE
				'A'
			END AS [Status],
		LastName AS LastName,
		MiddleName AS MiddleInitial,
		Prefix AS Salutation,
		Suffix AS NameReference,
		CASE
			WHEN DateOfBirth IS NULL
				THEN CONVERT(nvarchar(64), '')
			ELSE
				DATENAME(yyyy, DateOfBirth) + 
				RIGHT('00' + CONVERT(nvarchar(2), DATEPART(mm, DateOfBirth)), 2) +
				RIGHT ('00' + CONVERT(nvarchar(2), DATEPART(dd, DateOfBirth)), 2)
			END AS BirthDate,
		CASE
			WHEN GenderId = 1
				THEN 'U'
			WHEN GenderId = 2
				THEN 'F'
			WHEN GenderId = 3
				THEN 'M'
			ELSE
				''
			END AS Gender,
		CASE
			WHEN MaritalStatusId = 1
				THEN 'S'
			WHEN MaritalStatusId = 2
				THEN 'M'
			WHEN MaritalStatusId = 3
				THEN 'D'
			WHEN MaritalStatusId = 4
				THEN 'W'
			ELSE
				''
			END AS Marital,
			Occupation AS Occupation,
			PriorPatientCode AS OldPatient,
			'' AS ReferralRequired,
			SocialSecurityNumber AS SocialSecurity,
			EmployerName AS BusinessName,
			CASE 
				WHEN IsHipaaConsentSigned = CONVERT(bit, 1)
					THEN 'T'
				WHEN IsHipaaConsentSigned = CONVERT(bit, 0)
					THEN 'F'
				ELSE
					CONVERT(nvarchar(1), NULL)
				END AS Hipaa,
			Ethnicity AS Ethnicity,
			[Language] AS [Language],
			Race AS Race,
			BusinessType AS BusinessType,
			p.Address As [Address],
			p.Suite As Suite,
			p.City As City,
			p.[State] AS [State],
			p.Zip AS Zip,
			p.BusinessAddress AS BusinessAddress,
			p.BusinessSuite AS BusinessSuite,
			p.BusinessCity AS BusinessCity,
			p.BusinessState AS BusinessState,
			p.BusinessZip AS BusinessZip,
			p.HomePhone AS HomePhone,
			p.CellPhone AS CellPhone,
			p.BusinessPhone AS BusinessPhone,
			p.EmployerPhone AS EmployerPhone,
			p.ReferringPhysician AS ReferringPhysician,
			(SELECT TOP 1 COALESCE(ExternalProviderId, 0) FROM model.PatientExternalProviders pep WHERE pep.Id = p.PrimaryCarePhysicianId) AS PrimaryCarePhysician,
			p.ProfileComment1 AS ProfileComment1,
			p.ProfileComment2 AS ProfileComment2,
			p.Email AS Email,
			'' AS FinancialAssignment,
			'' AS FinancialSignature,
			CONVERT(nvarchar(64),'') AS MedicareSecondary,
			CASE
				WHEN p.PreferredAddressId = (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = p.PolicyPatientId ORDER BY pad.Id)
					THEN 'Y'
				ELSE
					'N'
				END AS BillParty,
			'' AS SecondBillParty,
			CONVERT(nvarchar(32),'') AS ReferralCatagory,
			CONVERT(nvarchar(max),'') AS EmergencyName,
			CONVERT(nvarchar(max),'') AS EmergencyPhone,
			'' AS EmergencyRel,
			p.PolicyPatientId,
			p.SecondPolicyPatientId AS SecondPolicyPatientId,
			p.Relationship AS Relationship,
			p.SecondRelationship AS SecondRelationship,
			p.PatType AS PatType,
			CONVERT(int, NULL) AS BillingOffice,
			CONVERT(int, NULL) AS PostOpExpireDate,
			CONVERT(int, NULL) AS PostOpService,
			CONVERT(nvarchar(64), '') AS FinancialSignatureDate,
			'' AS Religion,
			'' AS SecondRelationshipArchived,
			CONVERT(int, NULL) AS SecondPolicyPatientIdArchived,
			'' AS RelationshipArchived,
			CONVERT(int, NULL) AS PolicyPatientIdArchived,
			'' AS BulkMailing,
			'' AS ContactType,
			'' AS NationalOrigin,
			CASE IsStatementSuppressed
				WHEN CONVERT(bit, 0)
					THEN 'Y'
				ELSE
					'N'
				END AS SendStatements,
			Case IsCollectionLetterSuppressed
				WHEN CONVERT(bit, 0)
					THEN 'Y'
				ELSE
					'N'
			END AS SendConsults
	FROM (SELECT *, 
		  model.GetPairId(1, p1.Id, 110000000) As PrimaryCarePhysicianId,
		  ISNULL(dbo.GetPolicyHolderPatientId(p1.Id), p1.Id) AS PolicyPatientId,
		  ISNULL(dbo.GetSecondPolicyHolderPatientId(p1.Id),0) AS SecondPolicyPatientId,
		  ISNULL(dbo.GetPolicyHolderRelationshipType(p1.Id),'Y') AS Relationship,
		  ISNULL(dbo.GetSecondPolicyHolderRelationshipType(p1.Id),'') AS SecondRelationship,
		  (SELECT TOP 1 ExchangeAndSuffix FROM model.PatientPhoneNumbers ppn WHERE ppn.PatientId = p1.Id AND ppn.PatientPhoneNumberTypeId = 7 AND ppn.OrdinalId = 1) As HomePhone,
		  (SELECT TOP 1 ExchangeAndSuffix FROM model.PatientPhoneNumbers ppn WHERE ppn.PatientId = p1.Id AND ppn.PatientPhoneNumberTypeId = 3 AND ppn.OrdinalId = 1) AS CellPhone,
		  (SELECT TOP 1 ExchangeAndSuffix FROM model.PatientPhoneNumbers ppn WHERE ppn.PatientId = p1.Id AND ppn.PatientPhoneNumberTypeId = 2 AND ppn.OrdinalId = 1) AS BusinessPhone,
		  (SELECT TOP 1 ExchangeAndSuffix FROM model.PatientPhoneNumbers ppn WHERE ppn.PatientId = p1.Id AND ppn.OrdinalId = 1 AND ppn.PatientPhoneNumberTypeId IN (SELECT Id FROM dbo.PracticeCodeTable pct WHERE pct.Code = 'Employer' AND ReferenceType = 'PhoneTypePatient')) AS EmployerPhone,
		  (SELECT TOP 1 Line1 FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS [Address],
		  (SELECT TOP 1 Line2 FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS Suite,
		  (SELECT TOP 1 City FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS City,
		  (SELECT TOP 1 Abbreviation FROM model.StateOrProvinces sp WHERE sp.Id = (SELECT TOP 1 StateOrProvinceId FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1)) AS [State],
		  (SELECT TOP 1 PostalCode FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS Zip,
		  (SELECT TOP 1 Line1 FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1) AS BusinessAddress,
		  (SELECT TOP 1 Line2 FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1) AS BusinessSuite,
		  (SELECT TOP 1 City FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1) AS BusinessCity,
		  (SELECT TOP 1 Abbreviation FROM model.StateOrProvinces sp WHERE sp.Id = (SELECT TOP 1 StateOrProvinceId FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1)) AS BusinessState,
		  (SELECT TOP 1 PostalCode FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1) AS BusinessZip,
		  (SELECT TOP 1 Value FROM model.PatientEmailAddresses pea WHERE pea.PatientId = p1.Id AND pea.OrdinalId = 1) AS Email,
		  (SELECT TOP 1 Value FROM model.PatientComments pc WHERE pc.PatientCommentTypeId = 2 AND pc.PatientId = p1.Id ORDER BY pc.Id DESC) AS ProfileComment1,
		  (SELECT TOP 1 Comment FROM model.PatientReferralSources rs WHERE rs.PatientId = p1.Id AND rs.Comment IS NOT NULL) AS ProfileComment2,
		  (SELECT TOP 1 COALESCE(ExternalProviderId, 0) FROM model.PatientExternalProviders pep WHERE pep.Id = p1.Id) AS ReferringPhysician,
		  (SELECT TOP 1 PatientAddressId FROM model.PatientCommunicationPreferences pcp WHERE pcp.PatientId = p1.Id AND PatientCommunicationTypeId = 3) AS PreferredAddressId,
		  (SELECT TOP 1 DisplayName FROM model.Tags pt WHERE pt.Id = (SELECT TOP 1 TagId FROM model.PatientTags ppt WHERE ppt.PatientId = p1.Id AND ppt.OrdinalId = 1)) AS PatType,
		  (SELECT TOP 1 Name FROM model.Ethnicities pe WHERE pe.Id = EthnicityId) As Ethnicity,
		  (SELECT TOP 1 SUBSTRING(Name, 1, 10) FROM model.Languages pl WHERE pl.Id = LanguageId) As [Language],
		  (SELECT TOP 1 Name FROM model.Races WHERE Id = (SELECT TOP 1 Races_Id FROM model.PatientRace ppr WHERE ppr.Patients_Id = p1.Id)) AS Race,
		  (SELECT TOP 1 SUBSTRING(Name, 1, 1) FROM model.PatientEmploymentStatus pes WHERE pes.Id = PatientEmploymentStatusId) AS BusinessType
		FROM model.Patients p1) p
GO

--Change OnPatientDemographicInsertAndUpdate Trigger
IF (OBJECT_ID('dbo.OnPatientDemographicInsertAndUpdate', 'TR') IS NOT NULL)
BEGIN
	DROP TRIGGER dbo.OnPatientDemographicInsertAndUpdate
END
GO

CREATE TRIGGER dbo.OnPatientDemographicInsertAndUpdate ON [dbo].[PatientDemographics]
INSTEAD OF INSERT
	,UPDATE
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @PatientId INT
	DECLARE @SchedulePrimaryDoctor INT
	DECLARE @FirstName NVARCHAR(max)
	DECLARE @Status NVARCHAR(1)
	DECLARE @LastName NVARCHAR(max)
	DECLARE @MiddleInitial NVARCHAR(max)
	DECLARE @Salutation NVARCHAR(max)
	DECLARE @NameReference NVARCHAR(max)
	DECLARE @BirthDate NVARCHAR(90)
	DECLARE @Gender NVARCHAR(1)
	DECLARE @Marital NVARCHAR(1)
	DECLARE @Occupation NVARCHAR(max)
	DECLARE @OldPatient NVARCHAR(max)
	DECLARE @ReferralRequired NVARCHAR(1)
	DECLARE @SocialSecurity NVARCHAR(max)
	DECLARE @BusinessName NVARCHAR(max)
	DECLARE @Hipaa NVARCHAR(1)
	DECLARE @Ethnicity NVARCHAR(64)
	DECLARE @Language NVARCHAR(64)
	DECLARE @Race NVARCHAR(64)
	DECLARE @BusinessType NVARCHAR(1)
	DECLARE @Address NVARCHAR(max)
	DECLARE @Suite NVARCHAR(max)
	DECLARE @City NVARCHAR(max)
	DECLARE @State NVARCHAR(max)
	DECLARE @Zip NVARCHAR(max)
	DECLARE @BusinessAddress NVARCHAR(max)
	DECLARE @BusinessSuite NVARCHAR(max)
	DECLARE @BusinessCity NVARCHAR(max)
	DECLARE @BusinessState NVARCHAR(max)
	DECLARE @BusinessZip NVARCHAR(max)
	DECLARE @HomePhone NVARCHAR(max)
	DECLARE @CellPhone NVARCHAR(max)
	DECLARE @BusinessPhone NVARCHAR(max)
	DECLARE @EmployerPhone NVARCHAR(max)
	DECLARE @ReferringPhysician INT
	DECLARE @PrimaryCarePhysician INT
	DECLARE @ProfileComment1 NVARCHAR(max)
	DECLARE @ProfileComment2 NVARCHAR(max)
	DECLARE @Email NVARCHAR(max)
	DECLARE @EmergencyPhone NVARCHAR(max)
	DECLARE @EmergencyRel NVARCHAR(1)
	DECLARE @FinancialAssignment NVARCHAR(1)
	DECLARE @FinancialSignature NVARCHAR(1)
	DECLARE @MedicareSecondary NVARCHAR(64)
	DECLARE @BillParty NVARCHAR(1)
	DECLARE @SecondBillParty NVARCHAR(1)
	DECLARE @ReferralCatagory NVARCHAR(max)
	DECLARE @EmergencyName NVARCHAR(max)
	DECLARE @PolicyPatientId INT
	DECLARE @SecondPolicyPatientId INT
	DECLARE @Relationship NVARCHAR(1)
	DECLARE @SecondRelationship NVARCHAR(1)
	DECLARE @PatType NVARCHAR(max)
	DECLARE @BillingOffice INT
	DECLARE @PostOpExpireDate INT
	DECLARE @PostOpService INT
	DECLARE @FinancialSignatureDate NVARCHAR(64)
	DECLARE @Religion NVARCHAR(1)
	DECLARE @SecondRelationshipArchived VARCHAR(1)
	DECLARE @SecondPolicyPatientIdArchived INT
	DECLARE @RelationshipArchived NVARCHAR(1)
	DECLARE @PolicyPatientIdArchived INT
	DECLARE @BulkMailing NVARCHAR(1)
	DECLARE @ContactType NVARCHAR(1)
	DECLARE @NationalOrigin NVARCHAR(1)
	DECLARE @SendStatements NVARCHAR(1)
	DECLARE @SendConsults NVARCHAR(1)

	DECLARE PatientDemographicsInsertedUpdatedCursor CURSOR
	FOR
	SELECT PatientId
		,SchedulePrimaryDoctor
		,FirstName
		,STATUS
		,LastName
		,MiddleInitial
		,Salutation
		,NameReference
		,BirthDate
		,Gender
		,Marital
		,Occupation
		,OldPatient
		,ReferralRequired
		,SocialSecurity
		,BusinessName
		,Hipaa
		,Ethnicity
		,LANGUAGE
		,Race
		,BusinessType
		,Address
		,Suite
		,City
		,STATE
		,Zip
		,BusinessAddress
		,BusinessSuite
		,BusinessCity
		,BusinessState
		,BusinessZip
		,HomePhone
		,CellPhone
		,BusinessPhone
		,EmployerPhone
		,ReferringPhysician
		,PrimaryCarePhysician
		,ProfileComment1
		,ProfileComment2
		,Email
		,EmergencyPhone
		,EmergencyRel
		,FinancialAssignment
		,FinancialSignature
		,MedicareSecondary
		,BillParty
		,SecondBillParty
		,ReferralCatagory
		,EmergencyName
		,PolicyPatientId
		,SecondPolicyPatientId
		,Relationship
		,SecondRelationship
		,PatType
		,BillingOffice
		,PostOpExpireDate
		,PostOpService
		,FinancialSignatureDate
		,Religion
		,SecondRelationshipArchived
		,SecondPolicyPatientIdArchived
		,RelationshipArchived
		,PolicyPatientIdArchived
		,BulkMailing
		,ContactType
		,NationalOrigin
		,SendStatements
		,SendConsults
	FROM inserted

	OPEN PatientDemographicsInsertedUpdatedCursor

	FETCH NEXT
	FROM PatientDemographicsInsertedUpdatedCursor
	INTO @PatientId
		,@SchedulePrimaryDoctor
		,@FirstName
		,@Status
		,@LastName
		,@MiddleInitial
		,@Salutation
		,@NameReference
		,@BirthDate
		,@Gender
		,@Marital
		,@Occupation
		,@OldPatient
		,@ReferralRequired
		,@SocialSecurity
		,@BusinessName
		,@Hipaa
		,@Ethnicity
		,@Language
		,@Race
		,@BusinessType
		,@Address
		,@Suite
		,@City
		,@State
		,@Zip
		,@BusinessAddress
		,@BusinessSuite
		,@BusinessCity
		,@BusinessState
		,@BusinessZip
		,@HomePhone
		,@CellPhone
		,@BusinessPhone
		,@EmployerPhone
		,@ReferringPhysician
		,@PrimaryCarePhysician
		,@ProfileComment1
		,@ProfileComment2
		,@Email
		,@EmergencyPhone
		,@EmergencyRel
		,@FinancialAssignment
		,@FinancialSignature
		,@MedicareSecondary
		,@BillParty
		,@SecondBillParty
		,@ReferralCatagory
		,@EmergencyName
		,@PolicyPatientId
		,@SecondPolicyPatientId
		,@Relationship
		,@SecondRelationship
		,@PatType
		,@BillingOffice
		,@PostOpExpireDate
		,@PostOpService
		,@FinancialSignatureDate
		,@Religion
		,@SecondRelationshipArchived
		,@SecondPolicyPatientIdArchived
		,@RelationshipArchived
		,@PolicyPatientIdArchived
		,@BulkMailing
		,@ContactType
		,@NationalOrigin
		,@SendStatements
		,@SendConsults

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE @employmentStatusId AS INT

		SET @employmentStatusId = (
				SELECT TOP 1 Id
				FROM model.PatientEmploymentStatus pes
				WHERE @BusinessType = SUBSTRING(pes.NAME, 1, 1)
				)

		DECLARE @maritalStatusId AS INT

		SET @maritalStatusId = CASE @Marital
				WHEN 'S'
					THEN 1
				WHEN 'M'
					THEN 2
				WHEN 'D'
					THEN 3
				WHEN 'W'
					THEN 4
				ELSE NULL
				END

		DECLARE @genderId AS INT

		SET @genderId = CASE @Gender
				WHEN 'U'
					THEN 1
				WHEN 'F'
					THEN 2
				WHEN 'M'
					THEN 3
				ELSE 4
				END

		DECLARE @languageId AS INT

		SET @languageId = (
				SELECT TOP 1 Id
				FROM model.Languages pl
				WHERE SUBSTRING(pl.NAME, 1, 10) = @Language
				)

		DECLARE @defaultUserId AS INT

		SET @defaultUserId = CASE @SchedulePrimaryDoctor
				WHEN 0
					THEN NULL
				ELSE @SchedulePrimaryDoctor
				END

		DECLARE @isClinical AS BIT

		SET @isClinical = CASE @Status
				WHEN 'I'
					THEN CONVERT(BIT, 0)
				ELSE CONVERT(BIT, 1)
				END

		DECLARE @patientStatusId AS INT

		SET @patientStatusId = CASE @Status
				WHEN 'C'
					THEN 2
				WHEN 'D'
					THEN 3
				WHEN 'M'
					THEN 4
				ELSE 1
				END

		DECLARE @isHipaaConsentSigned AS BIT

		SET @isHipaaConsentSigned = CASE @Hipaa
				WHEN 'T'
					THEN CONVERT(BIT, 1)
				ELSE CONVERT(BIT, 0)
				END

		DECLARE @isStatementSuppressed AS BIT

		SET @isStatementSuppressed = CASE @SendStatements
				WHEN 'Y'
					THEN CONVERT(BIT, 0)
				ELSE CONVERT(BIT, 1)
				END

		DECLARE @isCollectionLetterSuppressed AS BIT

		SET @isCollectionLetterSuppressed = CASE @SendConsults
				WHEN 'Y'
					THEN CONVERT(BIT, 0)
				ELSE CONVERT(BIT, 1)
				END

		DECLARE @RaceId AS INT

		SET @RaceId = (
				SELECT TOP 1 Id
				FROM model.Races pr
				WHERE pr.NAME = @Race
				)

		DECLARE @EthnicityId AS INT

		SET @EthnicityId = (
				SELECT TOP 1 Id
				FROM model.Ethnicities pe
				WHERE pe.NAME = @Ethnicity
				)

		DECLARE @releaseOfInformationCodeId AS INT

		SET @releaseOfInformationCodeId = CASE @FinancialSignature
				WHEN 'Y'
					THEN 1
				ELSE 0
				END

		DECLARE @dateOfBirth AS DATETIME

		SET @dateOfBirth = CONVERT(DATETIME, @BirthDate, 112)

		DECLARE @releaseOfInformationDateTime AS DATETIME

		SET @releaseOfInformationDateTime = CONVERT(DATETIME, @FinancialSignatureDate, 112)

		IF (COALESCE(@PatientId, 0) = 0)
		BEGIN
			INSERT INTO model.Patients (
				LastName
				,FirstName
				,MiddleName
				,Suffix
				,SocialSecurityNumber
				,Occupation
				,EmployerName
				,PatientEmploymentStatusId
				,GenderId
				,MaritalStatusId
				,DateOfBirth
				,LanguageId
				,DefaultUserId
				,ReleaseOfInformationCodeId
				,ReleaseOfInformationDateTime
				,IsClinical
				,PatientStatusId
				,Prefix
				,IsHipaaConsentSigned
				,IsStatementSuppressed
				,IsCollectionLetterSuppressed
				,PriorPatientCode
				,EthnicityId
				)
			VALUES (
				@LastName
				,@FirstName
				,@MiddleInitial
				,@NameReference
				,@SocialSecurity
				,@Occupation
				,@BusinessName
				,@employmentStatusId
				,@genderId
				,@maritalStatusId
				,@dateOfBirth
				,@languageId
				,@defaultUserId
				,@releaseOfInformationCodeId
				,@releaseOfInformationDateTime
				,@isClinical
				,@patientStatusId
				,@Salutation
				,@isHipaaConsentSigned
				,@isStatementSuppressed
				,@isCollectionLetterSuppressed
				,@OldPatient
				,@EthnicityId
				)

			SET @PatientId = IDENT_CURRENT('model.Patients')
		END
		ELSE
		BEGIN
			UPDATE model.Patients
			SET LastName = @LastName
				,FirstName = @FirstName
				,MiddleName = @MiddleInitial
				,Suffix = @NameReference
				,SocialSecurityNumber = @SocialSecurity
				,Occupation = @Occupation
				,EmployerName = @BusinessName
				,PatientEmploymentStatusId = @employmentStatusId
				,GenderId = @genderId
				,MaritalStatusId = @maritalStatusId
				,DateOfBirth = @dateOfBirth
				,LanguageId = @languageId
				,DefaultUserId = @defaultUserId
				,IsClinical = @isClinical
				,PatientStatusId = @patientStatusId
				,Prefix = @Salutation
				,IsHipaaConsentSigned = @isHipaaConsentSigned
				,IsStatementSuppressed = @isStatementSuppressed
				,IsCollectionLetterSuppressed = @isCollectionLetterSuppressed
				,PriorPatientCode = @OldPatient
				,EthnicityId = @EthnicityId
			WHERE Id = @PatientId
		END

		---- Update or insert Primary PatientEmailAddress
		IF (COALESCE(@Email, '') <> '')
		BEGIN
			IF EXISTS (
					SELECT *
					FROM model.PatientEmailAddresses
					WHERE PatientId = @PatientId
						AND OrdinalId = 1
						AND EmailAddressTypeId = 1
					)
			BEGIN
				UPDATE model.PatientEmailAddresses
				SET Value = @Email
				WHERE PatientId = @PatientId
					AND OrdinalId = 1
					AND EmailAddressTypeId = 1
			END
			ELSE
			BEGIN
				INSERT INTO model.PatientEmailAddresses (
					Value
					,OrdinalId
					,PatientId
					,EmailAddressTypeId
					)
				VALUES (
					@Email
					,1
					,@PatientId
					,1
					)
			END
		END
		ELSE
		BEGIN
			DELETE
			FROM model.PatientEmailAddresses
			WHERE PatientId = @PatientId
				AND OrdinalId = 1
				AND EmailAddressTypeId = 1
		END

		DECLARE @homeAddressId AS INT

		SET @homeAddressId = (
				SELECT TOP 1 Id
				FROM model.PatientAddresses pa
				WHERE pa.PatientId = @PatientId
					AND pa.PatientAddressTypeId = 1
				)

		---- If no home address exists, detect whether any home address values are set, and add a new home address if possible
		DECLARE @postalCode AS NVARCHAR(max)
		DECLARE @stateOrProvinceId AS INT

		SET @stateOrProvinceId = (
				SELECT TOP 1 Id
				FROM model.StateOrProvinces st
				WHERE st.Abbreviation = @State
				)

		IF (COALESCE(@homeAddressId, 0) = 0)
		BEGIN
			IF (
					(COALESCE(@Address, '') <> '')
					OR (COALESCE(@Suite, '') <> '')
					OR (COALESCE(@City, '') <> '')
					OR (COALESCE(@State, '') <> '')
					OR (COALESCE(@Zip, '') <> '')
					)
			BEGIN
				SET @postalCode = REPLACE(@Zip, '-', '')

				INSERT INTO model.PatientAddresses (
					Line1
					,Line2
					,City
					,PatientId
					,PostalCode
					,StateOrProvinceId
					,PatientAddressTypeId
					,OrdinalId
					)
				VALUES (
					@Address
					,@Suite
					,@City
					,@PatientId
					,@postalCode
					,@stateOrProvinceId
					,1
					,1
					)
			END
		END
				---- Otherwise, update the existing home address values
		ELSE
		BEGIN
			SET @postalCode = REPLACE(@Zip, '-', '')

			UPDATE model.PatientAddresses
			SET Line1 = @Address
				,Line2 = @Suite
				,City = @City
				,PostalCode = @postalCode
				,StateOrProvinceId = @stateOrProvinceId
			WHERE Id = @homeAddressId
		END

		DECLARE @businessAddressId AS INT

		SET @businessAddressId = (
				SELECT TOP 1 Id
				FROM model.PatientAddresses pa
				WHERE pa.PatientId = @PatientId
					AND pa.PatientAddressTypeId = 2
				)
		---- If no business address exists, detect whether any business address values are set, and add a new business address if possible
		SET @postalCode = REPLACE(@BusinessZip, '-', '')
		SET @stateOrProvinceId = (
				SELECT TOP 1 Id
				FROM model.StateOrProvinces st
				WHERE st.Abbreviation = @BusinessState
				)

		IF (COALESCE(@businessAddressId, 0) = 0)
		BEGIN
			IF (
					(COALESCE(@BusinessAddress, '') <> '')
					OR (COALESCE(@BusinessSuite, '') <> '')
					OR (COALESCE(@BusinessCity, '') <> '')
					OR (COALESCE(@BusinessState, '') <> '')
					OR (COALESCE(@BusinessZip, '') <> '')
					)
			BEGIN
				INSERT INTO model.PatientAddresses (
					Line1
					,Line2
					,City
					,PatientId
					,PostalCode
					,StateOrProvinceId
					,PatientAddressTypeId
					,OrdinalId
					)
				VALUES (
					@BusinessAddress
					,@BusinessSuite
					,@BusinessCity
					,@PatientId
					,@postalCode
					,@stateOrProvinceId
					,2
					,2
					)
			END
		END
				---- Otherwise, update the existing home address values
		ELSE
		BEGIN
			UPDATE model.PatientAddresses
			SET Line1 = @BusinessAddress
				,Line2 = @BusinessSuite
				,City = @BusinessCity
				,PostalCode = @postalCode
				,StateOrProvinceId = @stateOrProvinceId
			WHERE Id = @businessAddressId
		END

		---- Update or insert Primary PatientEmailAddress
		IF (COALESCE(@Email, '') <> '')
		BEGIN
			UPDATE model.PatientEmailAddresses
			SET Value = @Email
			WHERE PatientId = @PatientId
				AND OrdinalId = 1
				AND EmailAddressTypeId = 1
		END
		ELSE
		BEGIN
			DELETE
			FROM model.PatientEmailAddresses
			WHERE PatientId = @PatientId
				AND OrdinalId = 1
				AND EmailAddressTypeId = 1
		END

		DECLARE @homePhoneNumberId AS INT

		SET @homePhoneNumberId = (
				SELECT TOP 1 Id
				FROM model.PatientPhoneNumbers pn
				WHERE pn.PatientId = @PatientId
					AND pn.PatientPhoneNumberTypeId = 7
				)

		---- Insert new Home PatientPhoneNumber if possible
		IF (COALESCE(@homePhoneNumberId, 0) = 0)
		BEGIN
			IF (COALESCE(@HomePhone, '') <> '')
			BEGIN
				INSERT INTO model.PatientPhoneNumbers (
					ExchangeAndSuffix
					,IsInternational
					,OrdinalId
					,PatientId
					,PatientPhoneNumberTypeId
					)
				VALUES (
					@HomePhone
					,CONVERT(BIT, 0)
					,1
					,@PatientId
					,7
					)
			END
		END
				---- Otherwise update Home PatientPhoneNumber
		ELSE
		BEGIN
			IF (COALESCE(@HomePhone, '') <> '')
			BEGIN
				UPDATE model.PatientPhoneNumbers
				SET ExchangeAndSuffix = @HomePhone
				WHERE PatientId = @PatientId
					AND PatientPhoneNumberTypeId = 7
					AND OrdinalId = 1
			END
			ELSE
			BEGIN
				DELETE
				FROM model.PatientPhoneNumbers
				WHERE PatientId = @PatientId
					AND PatientPhoneNumberTypeId = 7
					AND OrdinalId = 1
			END
		END

		DECLARE @cellPhoneNumberId AS INT

		SET @cellPhoneNumberId = (
				SELECT TOP 1 Id
				FROM model.PatientPhoneNumbers pn
				WHERE pn.PatientId = @PatientId
					AND pn.PatientPhoneNumberTypeId = 3
				)

		---- Insert new Cell PatientPhoneNumber if possible
		IF (COALESCE(@cellPhoneNumberId, 0) = 0)
		BEGIN
			IF (COALESCE(@CellPhone, '') <> '')
			BEGIN
				INSERT INTO model.PatientPhoneNumbers (
					ExchangeAndSuffix
					,IsInternational
					,OrdinalId
					,PatientId
					,PatientPhoneNumberTypeId
					)
				VALUES (
					@CellPhone
					,CONVERT(BIT, 0)
					,3
					,@PatientId
					,3
					)
			END
		END
				---- Otherwise update Cell PatientPhoneNumber
		ELSE
		BEGIN
			IF (COALESCE(@CellPhone, '') <> '')
			BEGIN
				UPDATE model.PatientPhoneNumbers
				SET ExchangeAndSuffix = @CellPhone
				WHERE PatientId = @PatientId
					AND PatientPhoneNumberTypeId = 3
					AND OrdinalId = 3
			END
			ELSE
			BEGIN
				DELETE
				FROM model.PatientPhoneNumbers
				WHERE PatientId = @PatientId
					AND PatientPhoneNumberTypeId = 7
					AND OrdinalId = 3
			END
		END

		DECLARE @businessPhoneNumberId AS INT

		SET @businessPhoneNumberId = (
				SELECT TOP 1 Id
				FROM model.PatientPhoneNumbers pn
				WHERE pn.PatientId = @PatientId
					AND pn.PatientPhoneNumberTypeId = 2
				)

		---- Insert new Business PatientPhoneNumber if possible
		IF (COALESCE(@businessPhoneNumberId, 0) = 0)
		BEGIN
			IF (COALESCE(@BusinessPhone, '') <> '')
			BEGIN
				INSERT INTO model.PatientPhoneNumbers (
					ExchangeAndSuffix
					,IsInternational
					,OrdinalId
					,PatientId
					,PatientPhoneNumberTypeId
					)
				VALUES (
					@BusinessPhone
					,CONVERT(BIT, 0)
					,2
					,@PatientId
					,2
					)
			END
		END
				---- Otherwise update Business PatientPhoneNumber
		ELSE
		BEGIN
			IF (COALESCE(@BusinessPhone, '') <> '')
			BEGIN
				UPDATE model.PatientPhoneNumbers
				SET ExchangeAndSuffix = @BusinessPhone
				WHERE PatientId = @PatientId
					AND PatientPhoneNumberTypeId = 2
					AND OrdinalId = 2
			END
			ELSE
			BEGIN
				DELETE
				FROM model.PatientPhoneNumbers
				WHERE PatientId = @PatientId
					AND PatientPhoneNumberTypeId = 2
					AND OrdinalId = 2
			END
		END

		IF EXISTS (
				SELECT *
				FROM model.ExternalContacts
				WHERE Id = @ReferringPhysician
				)
		BEGIN
			DECLARE @referringPhysicianExternalProviderId AS INT

			SET @referringPhysicianExternalProviderId = (
					SELECT TOP 1 Id
					FROM model.PatientExternalProviders pep
					WHERE pep.Id = @PatientId
					)

			---- Insert new ReferringPhysician PatientExternalProvider if possible
			IF (COALESCE(@referringPhysicianExternalProviderId, 0) = 0)
			BEGIN
				SET IDENTITY_INSERT [model].[PatientExternalProviders] ON

				INSERT INTO model.PatientExternalProviders (
					Id
					,ExternalProviderId
					,IsPrimaryCarePhysician
					,IsReferringPhysician
					,PatientId
					)
				VALUES (
					@PatientId
					,@ReferringPhysician
					,CONVERT(BIT, 0)
					,CONVERT(BIT, 1)
					,@PatientId
					)

				SET IDENTITY_INSERT [model].[PatientExternalProviders] OFF
			END
					---- Otherwise update ReferringPhysician PatientExternalProvider
			ELSE
			BEGIN
				UPDATE model.PatientExternalProviders
				SET ExternalProviderId = @ReferringPhysician
				WHERE Id = @PatientId
			END
		END

		IF EXISTS (
				SELECT *
				FROM model.ExternalContacts
				WHERE Id = @PrimaryCarePhysician
				)
		BEGIN
			DECLARE @PatientExternalProviderMax INT

			SET @PatientExternalProviderMax = 110000000

			DECLARE @primaryCarePhysicianExternalProviderId AS INT

			SET @primaryCarePhysicianExternalProviderId = (
					SELECT TOP 1 Id
					FROM model.PatientExternalProviders pep
					WHERE pep.Id = model.GetPairId(1, @PatientId, @PatientExternalProviderMax)
					)

			---- Insert new PrimaryCarePhysician PatientExternalProvider if possible
			IF (COALESCE(@primaryCarePhysicianExternalProviderId, 0) = 0)
			BEGIN
				SET IDENTITY_INSERT [model].[PatientExternalProviders] ON

				INSERT INTO model.PatientExternalProviders (
					Id
					,ExternalProviderId
					,IsPrimaryCarePhysician
					,IsReferringPhysician
					,PatientId
					)
				VALUES (
					model.GetPairId(1, @PatientId, @PatientExternalProviderMax)
					,@PrimaryCarePhysician
					,CONVERT(BIT, 1)
					,CONVERT(BIT, 0)
					,@PatientId
					)

				SET IDENTITY_INSERT [model].[PatientExternalProviders] OFF
			END
			ELSE
				---- Otherwise update PrimaryCarePhysician PatientExternalProvider
			BEGIN
				UPDATE model.PatientExternalProviders
				SET ExternalProviderId = @PrimaryCarePhysician
				WHERE Id = model.GetPairId(1, @PatientId, @PatientExternalProviderMax)
			END
		END

		DECLARE @CommentMax INT

		SET @CommentMax = 110000000

		DECLARE @patientCommentId AS INT

		SET @patientCommentId = model.GetPairId(2, @PatientId, @CommentMax)

		---- Insert new PatientComment if possible
		IF NOT EXISTS (
				SELECT *
				FROM model.PatientComments
				WHERE Id = @patientCommentId
				)
		BEGIN
			IF (COALESCE(@ProfileComment1, '') <> '')
			BEGIN
				SET IDENTITY_INSERT [model].[PatientComments] ON

				INSERT INTO model.PatientComments (
					Id
					,PatientId
					,Value
					,PatientCommentTypeId
					)
				VALUES (
					@patientCommentId
					,@PatientId
					,@ProfileComment1
					,2
					)

				SET IDENTITY_INSERT [model].[PatientComments] OFF
			END
		END
				---- Otherwise update PatientComment
		ELSE
		BEGIN
			UPDATE model.PatientComments
			SET Value = @ProfileComment1
			WHERE Id = @patientCommentId
		END

		---- Insert new PatientReferralSource if possible
		DECLARE @referralSourceTypeId AS INT

		SET @referralSourceTypeId = (
				SELECT TOP 1 Id
				FROM dbo.PracticeCodeTable pct
				WHERE pct.Code = @ReferralCatagory
					AND pct.ReferenceType = 'REFERBY'
				)

		IF NOT EXISTS (
				SELECT *
				FROM model.PatientReferralSources prs
				WHERE prs.Id = @PatientId
				)
		BEGIN
			IF (
					COALESCE(@ReferralCatagory, '') <> ''
					AND COALESCE(@referralSourceTypeId, 0) <> 0
					)
			BEGIN
				SET IDENTITY_INSERT [model].[PatientReferralSources] ON

				INSERT INTO model.PatientReferralSources (
					Id
					,PatientId
					,ReferralSourceTypeId
					,Comment
					)
				VALUES (
					@PatientId
					,@PatientId
					,@referralSourceTypeId
					,@ProfileComment2
					)

				SET IDENTITY_INSERT [model].[PatientReferralSources] OFF
			END
		END
				---- Otherwise update PatientReferralSource
		ELSE
		BEGIN
			UPDATE model.PatientReferralSources
			SET Comment = @ProfileComment2
			WHERE Id = @PatientId
		END

		DECLARE @communicationPreferenceAddressId AS INT

		SET @communicationPreferenceAddressId = CASE @BillParty
				WHEN 'Y'
					THEN (
							SELECT TOP 1 pad.Id
							FROM model.PatientAddresses pad
							WHERE pad.PatientId = ISNULL(dbo.GetPolicyHolderPatientId(@PatientId), @PatientId)
							ORDER BY pad.Id
							)
				ELSE (
						SELECT TOP 1 pad.Id
						FROM model.PatientAddresses pad
						WHERE pad.PatientId = @PatientId
						ORDER BY pad.Id
						)
				END

		IF (COALESCE(@communicationPreferenceAddressId, 0) <> 0)
		BEGIN
			---- Insert new PatientCommunicationPreference if possible
			IF NOT EXISTS (
					SELECT *
					FROM model.PatientCommunicationPreferences pcp
					WHERE pcp.PatientId = @PatientId
						AND pcp.PatientCommunicationTypeId = 3
						AND pcp.CommunicationMethodTypeId = 3
						AND pcp.__EntityType__ = 'PatientAddressCommunicationPreference'
					)
			BEGIN
				INSERT INTO model.PatientCommunicationPreferences (
					PatientId
					,PatientCommunicationTypeId
					,CommunicationMethodTypeId
					,IsAddressedToRecipient
					,PatientAddressId
					,__EntityType__
					)
				VALUES (
					@PatientId
					,3
					,3
					,CONVERT(BIT, 1)
					,@communicationPreferenceAddressId
					,'PatientAddressCommunicationPreference'
					)
			END
					---- Otherwise update PatientCommunicationPreference
			ELSE
			BEGIN
				IF EXISTS (
						SELECT *
						FROM model.PatientCommunicationPreferences
						WHERE PatientId = @PatientId
							AND PatientCommunicationTypeId = 3
							AND CommunicationMethodTypeId = 3
							AND __EntityType__ = 'PatientAddressCommunicationPreference'
							AND PatientAddressId <> @communicationPreferenceAddressId
						)
				BEGIN
					UPDATE model.PatientCommunicationPreferences
					SET PatientAddressId = @communicationPreferenceAddressId
					WHERE PatientId = @PatientId
						AND PatientCommunicationTypeId = 3
						AND CommunicationMethodTypeId = 3
						AND __EntityType__ = 'PatientAddressCommunicationPreference'
						AND PatientAddressId <> @communicationPreferenceAddressId
				END
			END
		END

		DECLARE @patientTypeId AS INT

		SET @patientTypeId = (
				SELECT TOP 1 Id
				FROM model.Tags pt
				WHERE pt.DisplayName = @PatType
				)

		---- Insert new PatientTags if possible
		IF NOT EXISTS (
				SELECT *
				FROM model.PatientTags ppt
				WHERE ppt.PatientId = @PatientId
					AND ppt.OrdinalId = 1
				)
		BEGIN
			IF (COALESCE(@patientTypeId, 0) <> 0)
			BEGIN
				INSERT INTO model.PatientTags (
					PatientId
					,TagId
					,OrdinalId
					)
				VALUES (
					@PatientId
					,@patientTypeId
					,1
					)
			END
		END
		ELSE
			---- Otherwise update PatientTags
		BEGIN
			IF (COALESCE(@patientTypeId, 0) <> 0)
			BEGIN
				UPDATE model.PatientTags
				SET TagId = @patientTypeId
				WHERE PatientId = @PatientId
					AND OrdinalId = 1
			END
			ELSE
			BEGIN
				DELETE
				FROM model.PatientTags
				WHERE PatientId = @PatientId
					AND OrdinalId = 1
			END
		END

		DECLARE @employerPhoneNumberTypeId AS INT

		SET @employerPhoneNumberTypeId = (
				SELECT TOP 1 Id + 1000
				FROM dbo.PracticeCodeTable pct
				WHERE pct.Code = 'Employer'
					AND pct.ReferenceType = 'PHONETYPEPATIENT'
				)

		DECLARE @employerPhoneNumberId AS INT

		SET @employerPhoneNumberId = (
				SELECT TOP 1 Id
				FROM model.PatientPhoneNumbers pn
				WHERE pn.PatientId = @PatientId
					AND pn.PatientPhoneNumberTypeId = @employerPhoneNumberTypeId
				)

		---- Insert new Employer PatientPhoneNumber if possible
		IF (COALESCE(@employerPhoneNumberId, 0) = 0)
		BEGIN
			IF (COALESCE(@EmployerPhone, '') <> '')
			BEGIN
				INSERT INTO model.PatientPhoneNumbers (
					ExchangeAndSuffix
					,IsInternational
					,OrdinalId
					,PatientId
					,PatientPhoneNumberTypeId
					)
				VALUES (
					@EmployerPhone
					,CONVERT(BIT, 0)
					,6
					,@PatientId
					,@employerPhoneNumberTypeId
					)
			END
		END
				---- Otherwise update Business PatientPhoneNumber
		ELSE
		BEGIN
			UPDATE model.PatientPhoneNumbers
			SET ExchangeAndSuffix = @EmployerPhone
			WHERE PatientId = @PatientId
				AND PatientPhoneNumberTypeId = @employerPhoneNumberId
		END

		UPDATE model.InsurancePolicies
		SET MedicareSecondaryReasonCodeId = CASE @MedicareSecondary
				WHEN '12'
					THEN 1
				WHEN '13'
					THEN 2
				WHEN '14'
					THEN 3
				WHEN '15'
					THEN 4
				WHEN '16'
					THEN 5
				WHEN '41'
					THEN 6
				WHEN '42'
					THEN 7
				WHEN '43'
					THEN 8
				WHEN '47'
					THEN 9
				ELSE NULL
				END
		WHERE PolicyHolderPatientId = @PatientId
			AND EXISTS (
				SELECT *
				FROM dbo.PracticeInsurers pins
				WHERE pins.InsurerId = InsurerId
					AND pins.InsurerReferenceCode IN (
						'M'
						,'N'
						,'['
						)
				)

		---- Insert new PatientRace
		IF (COALESCE(@RaceId, 0) <> 0)
		BEGIN
			IF NOT EXISTS (
				SELECT *
				FROM model.PatientRace pr
				WHERE pr.Patients_Id = @PatientId
					AND Races_Id = @RaceId
				)
			BEGIN
			INSERT INTO model.PatientRace (
				Patients_Id
				,Races_Id
				)
			VALUES (
				@PatientId
				,@RaceId
				)
			END
		END

		FETCH NEXT
		FROM PatientDemographicsInsertedUpdatedCursor
		INTO @PatientId
			,@SchedulePrimaryDoctor
			,@FirstName
			,@Status
			,@LastName
			,@MiddleInitial
			,@Salutation
			,@NameReference
			,@BirthDate
			,@Gender
			,@Marital
			,@Occupation
			,@OldPatient
			,@ReferralRequired
			,@SocialSecurity
			,@BusinessName
			,@Hipaa
			,@Ethnicity
			,@Language
			,@Race
			,@BusinessType
			,@Address
			,@Suite
			,@City
			,@State
			,@Zip
			,@BusinessAddress
			,@BusinessSuite
			,@BusinessCity
			,@BusinessState
			,@BusinessZip
			,@HomePhone
			,@CellPhone
			,@BusinessPhone
			,@EmployerPhone
			,@ReferringPhysician
			,@PrimaryCarePhysician
			,@ProfileComment1
			,@ProfileComment2
			,@Email
			,@EmergencyPhone
			,@EmergencyRel
			,@FinancialAssignment
			,@FinancialSignature
			,@MedicareSecondary
			,@BillParty
			,@SecondBillParty
			,@ReferralCatagory
			,@EmergencyName
			,@PolicyPatientId
			,@SecondPolicyPatientId
			,@Relationship
			,@SecondRelationship
			,@PatType
			,@BillingOffice
			,@PostOpExpireDate
			,@PostOpService
			,@FinancialSignatureDate
			,@Religion
			,@SecondRelationshipArchived
			,@SecondPolicyPatientIdArchived
			,@RelationshipArchived
			,@PolicyPatientIdArchived
			,@BulkMailing
			,@ContactType
			,@NationalOrigin
			,@SendStatements
			,@SendConsults
	END

	CLOSE PatientDemographicsInsertedUpdatedCursor

	DEALLOCATE PatientDemographicsInsertedUpdatedCursor

	SET NOCOUNT OFF

	SELECT @PatientId AS SCOPE_ID_COLUMN
END
GO

--Change OnPatientDemographicDelete
IF (OBJECT_ID('dbo.OnPatientDemographicDelete', 'TR') IS NOT NULL)
BEGIN
	DROP TRIGGER dbo.OnPatientDemographicDelete
END
GO

CREATE TRIGGER dbo.OnPatientDemographicDelete ON [dbo].[PatientDemographics]
INSTEAD OF DELETE
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @PatientId INT
	DECLARE @SchedulePrimaryDoctor INT
	DECLARE @FirstName NVARCHAR(max)
	DECLARE @Status NVARCHAR(1)
	DECLARE @LastName NVARCHAR(max)
	DECLARE @MiddleInitial NVARCHAR(max)
	DECLARE @Salutation NVARCHAR(max)
	DECLARE @NameReference NVARCHAR(max)
	DECLARE @BirthDate NVARCHAR(90)
	DECLARE @Gender NVARCHAR(1)
	DECLARE @Marital NVARCHAR(1)
	DECLARE @Occupation NVARCHAR(max)
	DECLARE @OldPatient NVARCHAR(max)
	DECLARE @ReferralRequired NVARCHAR(1)
	DECLARE @SocialSecurity NVARCHAR(max)
	DECLARE @BusinessName NVARCHAR(max)
	DECLARE @Hipaa NVARCHAR(1)
	DECLARE @Ethnicity NVARCHAR(64)
	DECLARE @Language NVARCHAR(64)
	DECLARE @Race NVARCHAR(64)
	DECLARE @BusinessType NVARCHAR(1)
	DECLARE @Address NVARCHAR(max)
	DECLARE @Suite NVARCHAR(max)
	DECLARE @City NVARCHAR(max)
	DECLARE @State NVARCHAR(max)
	DECLARE @Zip NVARCHAR(max)
	DECLARE @BusinessAddress NVARCHAR(max)
	DECLARE @BusinessSuite NVARCHAR(max)
	DECLARE @BusinessCity NVARCHAR(max)
	DECLARE @BusinessState NVARCHAR(max)
	DECLARE @BusinessZip NVARCHAR(max)
	DECLARE @HomePhone NVARCHAR(max)
	DECLARE @CellPhone NVARCHAR(max)
	DECLARE @BusinessPhone NVARCHAR(max)
	DECLARE @EmployerPhone NVARCHAR(max)
	DECLARE @ReferringPhysician INT
	DECLARE @PrimaryCarePhysician INT
	DECLARE @ProfileComment1 NVARCHAR(max)
	DECLARE @ProfileComment2 NVARCHAR(max)
	DECLARE @Email NVARCHAR(max)
	DECLARE @EmergencyPhone NVARCHAR(max)
	DECLARE @EmergencyRel NVARCHAR(1)
	DECLARE @FinancialAssignment NVARCHAR(1)
	DECLARE @FinancialSignature NVARCHAR(1)
	DECLARE @MedicareSecondary NVARCHAR(64)
	DECLARE @BillParty NVARCHAR(1)
	DECLARE @SecondBillParty NVARCHAR(1)
	DECLARE @ReferralCatagory NVARCHAR(max)
	DECLARE @EmergencyName NVARCHAR(max)
	DECLARE @PolicyPatientId INT
	DECLARE @SecondPolicyPatientId INT
	DECLARE @Relationship NVARCHAR(1)
	DECLARE @SecondRelationship NVARCHAR(1)
	DECLARE @PatType NVARCHAR(max)
	DECLARE @BillingOffice INT
	DECLARE @PostOpExpireDate INT
	DECLARE @PostOpService INT
	DECLARE @FinancialSignatureDate NVARCHAR(64)
	DECLARE @Religion NVARCHAR(1)
	DECLARE @SecondRelationshipArchived VARCHAR(1)
	DECLARE @SecondPolicyPatientIdArchived INT
	DECLARE @RelationshipArchived NVARCHAR(1)
	DECLARE @PolicyPatientIdArchived INT
	DECLARE @BulkMailing NVARCHAR(1)
	DECLARE @ContactType NVARCHAR(1)
	DECLARE @NationalOrigin NVARCHAR(1)
	DECLARE @SendStatements NVARCHAR(1)
	DECLARE @SendConsults NVARCHAR(1)

	DECLARE PatientDemographicsDeletedCursor CURSOR
	FOR
	SELECT PatientId
		,SchedulePrimaryDoctor
		,FirstName
		,STATUS
		,LastName
		,MiddleInitial
		,Salutation
		,NameReference
		,BirthDate
		,Gender
		,Marital
		,Occupation
		,OldPatient
		,ReferralRequired
		,SocialSecurity
		,BusinessName
		,Hipaa
		,Ethnicity
		,LANGUAGE
		,Race
		,BusinessType
		,Address
		,Suite
		,City
		,STATE
		,Zip
		,BusinessAddress
		,BusinessSuite
		,BusinessCity
		,BusinessState
		,BusinessZip
		,HomePhone
		,CellPhone
		,BusinessPhone
		,EmployerPhone
		,ReferringPhysician
		,PrimaryCarePhysician
		,ProfileComment1
		,ProfileComment2
		,Email
		,EmergencyPhone
		,EmergencyRel
		,FinancialAssignment
		,FinancialSignature
		,MedicareSecondary
		,BillParty
		,SecondBillParty
		,ReferralCatagory
		,EmergencyName
		,PolicyPatientId
		,SecondPolicyPatientId
		,Relationship
		,SecondRelationship
		,PatType
		,BillingOffice
		,PostOpExpireDate
		,PostOpService
		,FinancialSignatureDate
		,Religion
		,SecondRelationshipArchived
		,SecondPolicyPatientIdArchived
		,RelationshipArchived
		,PolicyPatientIdArchived
		,BulkMailing
		,ContactType
		,NationalOrigin
		,SendStatements
		,SendConsults
	FROM deleted

	OPEN PatientDemographicsDeletedCursor

	FETCH NEXT
	FROM PatientDemographicsDeletedCursor
	INTO @PatientId
		,@SchedulePrimaryDoctor
		,@FirstName
		,@Status
		,@LastName
		,@MiddleInitial
		,@Salutation
		,@NameReference
		,@BirthDate
		,@Gender
		,@Marital
		,@Occupation
		,@OldPatient
		,@ReferralRequired
		,@SocialSecurity
		,@BusinessName
		,@Hipaa
		,@Ethnicity
		,@Language
		,@Race
		,@BusinessType
		,@Address
		,@Suite
		,@City
		,@State
		,@Zip
		,@BusinessAddress
		,@BusinessSuite
		,@BusinessCity
		,@BusinessState
		,@BusinessZip
		,@HomePhone
		,@CellPhone
		,@BusinessPhone
		,@EmployerPhone
		,@ReferringPhysician
		,@PrimaryCarePhysician
		,@ProfileComment1
		,@ProfileComment2
		,@Email
		,@EmergencyPhone
		,@EmergencyRel
		,@FinancialAssignment
		,@FinancialSignature
		,@MedicareSecondary
		,@BillParty
		,@SecondBillParty
		,@ReferralCatagory
		,@EmergencyName
		,@PolicyPatientId
		,@SecondPolicyPatientId
		,@Relationship
		,@SecondRelationship
		,@PatType
		,@BillingOffice
		,@PostOpExpireDate
		,@PostOpService
		,@FinancialSignatureDate
		,@Religion
		,@SecondRelationshipArchived
		,@SecondPolicyPatientIdArchived
		,@RelationshipArchived
		,@PolicyPatientIdArchived
		,@BulkMailing
		,@ContactType
		,@NationalOrigin
		,@SendStatements
		,@SendConsults

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE
		FROM model.PatientTags
		WHERE PatientId = @PatientId

		DELETE
		FROM model.PatientCommunicationPreferences
		WHERE PatientId = @PatientId

		DELETE
		FROM model.PatientReferralSources
		WHERE PatientId = @PatientId

		DELETE
		FROM model.PatientComments
		WHERE PatientId = @PatientId

		DELETE
		FROM model.PatientExternalProviders
		WHERE PatientId = @PatientId

		DELETE
		FROM model.PatientPhoneNumbers
		WHERE PatientId = @PatientId

		DELETE
		FROM model.PatientEmailAddresses
		WHERE PatientId = @PatientId

		DELETE
		FROM model.PatientAddresses
		WHERE PatientId = @PatientId

		DELETE
		FROM model.Patients
		WHERE Id = @PatientId

		DELETE
		FROM model.PatientRace
		WHERE Patients_Id = @PatientId

		FETCH NEXT
		FROM PatientDemographicsDeletedCursor
		INTO @PatientId
			,@SchedulePrimaryDoctor
			,@FirstName
			,@Status
			,@LastName
			,@MiddleInitial
			,@Salutation
			,@NameReference
			,@BirthDate
			,@Gender
			,@Marital
			,@Occupation
			,@OldPatient
			,@ReferralRequired
			,@SocialSecurity
			,@BusinessName
			,@Hipaa
			,@Ethnicity
			,@Language
			,@Race
			,@BusinessType
			,@Address
			,@Suite
			,@City
			,@State
			,@Zip
			,@BusinessAddress
			,@BusinessSuite
			,@BusinessCity
			,@BusinessState
			,@BusinessZip
			,@HomePhone
			,@CellPhone
			,@BusinessPhone
			,@EmployerPhone
			,@ReferringPhysician
			,@PrimaryCarePhysician
			,@ProfileComment1
			,@ProfileComment2
			,@Email
			,@EmergencyPhone
			,@EmergencyRel
			,@FinancialAssignment
			,@FinancialSignature
			,@MedicareSecondary
			,@BillParty
			,@SecondBillParty
			,@ReferralCatagory
			,@EmergencyName
			,@PolicyPatientId
			,@SecondPolicyPatientId
			,@Relationship
			,@SecondRelationship
			,@PatType
			,@BillingOffice
			,@PostOpExpireDate
			,@PostOpService
			,@FinancialSignatureDate
			,@Religion
			,@SecondRelationshipArchived
			,@SecondPolicyPatientIdArchived
			,@RelationshipArchived
			,@PolicyPatientIdArchived
			,@BulkMailing
			,@ContactType
			,@NationalOrigin
			,@SendStatements
			,@SendConsults
	END

	CLOSE PatientDemographicsDeletedCursor

	DEALLOCATE PatientDemographicsDeletedCursor

	SET NOCOUNT OFF
END