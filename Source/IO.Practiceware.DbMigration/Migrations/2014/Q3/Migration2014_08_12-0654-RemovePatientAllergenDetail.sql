﻿--Bug 13014.

--Remove PatientAllergenDetail from PracticeRepositroyEntity.
DELETE
FROM model.ExternalSystemMessagePracticeRepositoryEntities
WHERE PracticeRepositoryEntityId IN (
		SELECT Id
		FROM model.PracticeRepositoryEntities
		WHERE NAME = 'PatientAllergenDetail'
		)

DELETE
FROM model.PracticeRepositoryEntities
WHERE NAME = 'PatientAllergenDetail'