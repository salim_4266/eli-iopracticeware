﻿---PatientAddressTypes---
IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditPatientAddressTypesInsertTrigger'
		)
	EXEC dbo.DropObject N'model.AuditPatientAddressTypesInsertTrigger'

IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditPatientAddressTypesUpdateTrigger'
		)
	EXEC dbo.DropObject N'model.AuditPatientAddressTypesUpdateTrigger'

IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditPatientAddressTypesDeleteTrigger'
		)
	EXEC dbo.DropObject N'model.AuditPatientAddressTypesDeleteTrigger'

IF EXISTS (
		SELECT c.TABLE_NAME
			,c.COLUMN_NAME
		FROM INFORMATION_SCHEMA.TABLES t
		INNER JOIN INFORMATION_SCHEMA.COLUMNS c ON c.TABLE_NAME = t.TABLE_NAME
		WHERE c.COLUMN_NAME = '__EntityType__'
			AND t.TABLE_NAME = 'PatientAddressTypes'
			AND t.TABLE_SCHEMA= 'model'
		)
BEGIN
	ALTER TABLE model.PatientAddressTypes
	DROP COLUMN __entitytype__
END

---UserAddressTypes---
IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditUserAddressTypesInsertTrigger'
		)
	EXEC dbo.DropObject N'model.AuditUserAddressTypesInsertTrigger'

IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditUserAddressTypesUpdateTrigger'
		)
	EXEC dbo.DropObject N'model.AuditUserAddressTypesUpdateTrigger'

IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditUserAddressTypesDeleteTrigger'
		)
	EXEC dbo.DropObject N'model.AuditUserAddressTypesDeleteTrigger'

IF EXISTS (
		SELECT c.TABLE_NAME
			,c.COLUMN_NAME
		FROM INFORMATION_SCHEMA.TABLES t
		INNER JOIN INFORMATION_SCHEMA.COLUMNS c ON c.TABLE_NAME = t.TABLE_NAME
		WHERE c.COLUMN_NAME = '__EntityType__'
			AND t.TABLE_NAME = 'UserAddressTypes'
			AND t.TABLE_SCHEMA= 'model'
		)
BEGIN
	ALTER TABLE model.UserAddressTypes
	DROP COLUMN __entitytype__
END

---InsurerAddressTypes---
IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditInsurerAddressTypesInsertTrigger'
		)
	EXEC dbo.DropObject N'model.AuditInsurerAddressTypesInsertTrigger'

IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditInsurerAddressTypesUpdateTrigger'
		)
	EXEC dbo.DropObject N'model.AuditInsurerAddressTypesUpdateTrigger'

IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditInsurerAddressTypesDeleteTrigger'
		)
	EXEC dbo.DropObject N'model.AuditInsurerAddressTypesDeleteTrigger'


IF EXISTS (
		SELECT c.TABLE_NAME
			,c.COLUMN_NAME
		FROM INFORMATION_SCHEMA.TABLES t
		INNER JOIN INFORMATION_SCHEMA.COLUMNS c ON c.TABLE_NAME = t.TABLE_NAME
		WHERE c.COLUMN_NAME = '__EntityType__'
			AND t.TABLE_NAME = 'InsurerAddressTypes'
			AND t.TABLE_SCHEMA= 'model'
		)
BEGIN
	ALTER TABLE model.InsurerAddressTypes
	DROP COLUMN __entitytype__
END

---BillingOrganizationAddressTypes---
IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditBillingOrganizationAddressTypesInsertTrigger'
		)
	EXEC dbo.DropObject N'model.AuditBillingOrganizationAddressTypesInsertTrigger'

IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditBillingOrganizationAddressTypesUpdateTrigger'
		)
	EXEC dbo.DropObject N'model.AuditBillingOrganizationAddressTypesUpdateTrigger'

IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditBillingOrganizationAddressTypesDeleteTrigger'
		)
	EXEC dbo.DropObject N'model.AuditBillingOrganizationAddressTypesDeleteTrigger'



IF EXISTS (
		SELECT c.TABLE_NAME
			,c.COLUMN_NAME
		FROM INFORMATION_SCHEMA.TABLES t
		INNER JOIN INFORMATION_SCHEMA.COLUMNS c ON c.TABLE_NAME = t.TABLE_NAME
		WHERE c.COLUMN_NAME = '__EntityType__'
			AND t.TABLE_NAME = 'BillingOrganizationAddressTypes'
			AND t.TABLE_SCHEMA= 'model'
		)
BEGIN
	ALTER TABLE model.BillingOrganizationAddressTypes
	DROP COLUMN __entitytype__
END


---ExternalOrganizationAddressTypes---
IF EXISTS (
		SELECT OBJECT_ID(N'model.AuditExternalOrganizationAddressTypesInsertTrigger')
		)
	EXEC dbo.DropObject N'model.AuditExternalOrganizationAddressTypesInsertTrigger'

IF EXISTS (
		SELECT OBJECT_ID(N'model.AuditExternalOrganizationAddressTypesUpdateTrigger')
		)
	EXEC dbo.DropObject N'model.AuditExternalOrganizationAddressTypesUpdateTrigger'

IF EXISTS (
		SELECT OBJECT_ID(N'model.AuditExternalOrganizationAddressTypesDeleteTrigger')
		)
	EXEC dbo.DropObject N'model.AuditExternalOrganizationAddressTypesDeleteTrigger'


IF EXISTS (
		SELECT c.TABLE_NAME
			,c.COLUMN_NAME
		FROM INFORMATION_SCHEMA.TABLES t
		INNER JOIN INFORMATION_SCHEMA.COLUMNS c ON c.TABLE_NAME = t.TABLE_NAME
		WHERE c.COLUMN_NAME = '__EntityType__'
			AND t.TABLE_NAME = 'ExternalOrganizationAddressTypes'
			AND t.TABLE_SCHEMA= 'model'
		)
BEGIN
	ALTER TABLE model.ExternalOrganizationAddressTypes
	DROP COLUMN __entitytype__
END

---ServiceLocationAddressTypes---
IF EXISTS (
		SELECT OBJECT_ID(N'model.AuditServiceLocationAddressTypesInsertTrigger')
		)
	EXEC dbo.DropObject N'model.AuditServiceLocationAddressTypesInsertTrigger'

IF EXISTS (
		SELECT OBJECT_ID(N'model.AuditServiceLocationAddressTypesUpdateTrigger')
		)
	EXEC dbo.DropObject N'model.AuditServiceLocationAddressTypesUpdateTrigger'

IF EXISTS (
		SELECT OBJECT_ID(N'model.AuditServiceLocationAddressTypesDeleteTrigger')
		)
	EXEC dbo.DropObject N'model.AuditServiceLocationAddressTypesDeleteTrigger'


IF EXISTS (
		SELECT c.TABLE_NAME
			,c.COLUMN_NAME
		FROM INFORMATION_SCHEMA.TABLES t
		INNER JOIN INFORMATION_SCHEMA.COLUMNS c ON c.TABLE_NAME = t.TABLE_NAME
		WHERE c.COLUMN_NAME = '__EntityType__'
			AND t.TABLE_NAME = 'ServiceLocationAddressTypes'
			AND t.TABLE_SCHEMA= 'model'
		)
BEGIN
	ALTER TABLE model.ServiceLocationAddressTypes
	DROP COLUMN __entitytype__
END


---ExternalContactAddressTypes---
IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditExternalContactAddressTypesInsertTrigger'
		)
	EXEC dbo.DropObject N'model.AuditExternalContactAddressTypesInsertTrigger'

IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditExternalContactAddressTypesUpdateTrigger'
		)
	EXEC dbo.DropObject N'model.AuditExternalContactAddressTypesUpdateTrigger'

IF EXISTS (
		SELECT NAME
		FROM sys.triggers
		WHERE NAME = 'AuditExternalContactAddressTypesDeleteTrigger'
		)
	EXEC dbo.DropObject N'model.AuditExternalContactAddressTypesDeleteTrigger'

IF EXISTS (
		SELECT c.TABLE_NAME
			,c.COLUMN_NAME
		FROM INFORMATION_SCHEMA.TABLES t
		INNER JOIN INFORMATION_SCHEMA.COLUMNS c ON c.TABLE_NAME = t.TABLE_NAME
		WHERE c.COLUMN_NAME = '__EntityType__'
			AND t.TABLE_NAME = 'ExternalContactAddressTypes'
			AND t.TABLE_SCHEMA= 'model'
		)
BEGIN
	ALTER TABLE model.ExternalContactAddressTypes
	DROP COLUMN __entitytype__
END