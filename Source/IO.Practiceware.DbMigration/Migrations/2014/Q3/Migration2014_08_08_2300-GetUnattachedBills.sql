﻿IF OBJECT_ID ( 'dbo.GetUnattachedBills', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetUnattachedBills;
GO

CREATE PROCEDURE dbo.GetUnattachedBills
	@AppointmentId INT
AS
SET NOCOUNT ON;

SELECT Id INTO #InvoiceReceivableId FROM dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId)

--prs.service, st.id, st.serviceid, ptj.TransactionId , ptj.transactionstatus
SELECT
es.Code
,bst.Id
,bst.BillingServiceId
,ptj.TransactionId
,ptj.TransactionStatus
,*
FROM model.InvoiceReceivables ir 
INNER JOIN #InvoiceReceivableId irs ON irs.Id = ir.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = ir.Id
	AND ptj.TransactionType = 'R'
INNER JOIN model.BillingServiceTransactions bst ON bst.InvoiceReceivableId = ir.Id
INNER JOIN model.BillingServices bs ON bs.Id = bst.BillingServiceId
INNER JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId

GO