﻿IF OBJECT_ID('model.ServiceLocationCodes') IS NOT NULL
	EXEC dbo.DropObject 'model.ServiceLocationCodes'
GO

-- 'ServiceLocationCodes' does not exist, creating...
CREATE TABLE [model].[ServiceLocationCodes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PlaceOfServiceCode] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
	LegacyPracticeCodeTableId nvarchar(MAX) NULL 
);

-- Creating primary key on [Id] in table 'ServiceLocationCodes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ServiceLocationCodes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ServiceLocationCodes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ServiceLocationCodes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ServiceLocationCodes]
ADD CONSTRAINT [PK_ServiceLocationCodes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

INSERT INTO model.ServiceLocationCodes (Name, PlaceOfServiceCode, LegacyPracticeCodeTableId)
SELECT 
SUBSTRING(p.Code, 4, LEN(p.Code)) AS [Name]
,SUBSTRING(p.Code, 1, 2) AS PlaceOfServiceCode
,Id AS LegacyPracticeCodeTableId
FROM dbo.PracticeCodeTable p
WHERE p.ReferenceType = 'PLACEOFSERVICE'
	AND ISNUMERIC(SUBSTRING(p.Code, 1, 2)) = 1
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.Invoices'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE i
SET i.ServiceLocationCodeId = s.Id
FROM model.Invoices i
JOIN model.ServiceLocationCodes s ON s.LegacyPracticeCodeTableId = i.ServiceLocationCodeId 
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [ServiceLocationCodeId] in table 'Invoices'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ServiceLocationCodeInvoice'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Invoices] DROP CONSTRAINT FK_ServiceLocationCodeInvoice

IF OBJECTPROPERTY(OBJECT_ID('[model].[ServiceLocationCodes]'), 'IsUserTable') = 1
ALTER TABLE [model].[Invoices]
ADD CONSTRAINT [FK_ServiceLocationCodeInvoice]
    FOREIGN KEY ([ServiceLocationCodeId])
    REFERENCES [model].[ServiceLocationCodes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ServiceLocationCodeInvoice'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ServiceLocationCodeInvoice')
	DROP INDEX IX_FK_ServiceLocationCodeInvoice ON [model].[Invoices]
GO
CREATE INDEX [IX_FK_ServiceLocationCodeInvoice]
ON [model].[Invoices]
    ([ServiceLocationCodeId]);
GO