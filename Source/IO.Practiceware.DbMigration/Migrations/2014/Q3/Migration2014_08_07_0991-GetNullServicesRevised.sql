﻿IF OBJECT_ID ( 'dbo.GetNullServices', 'P' ) IS NOT NULL 
DROP PROCEDURE dbo.GetNullServices;
GO

CREATE PROCEDURE dbo.GetNullServices
	@Invoice NVARCHAR(100)
AS
SET NOCOUNT ON;

SELECT DISTINCT 
ptj.TransactionDate
,ptj.TransactionStatus
,ptj.TransactionRef
,ptj.transactionaction
,SUBSTRING(ptj.TransactionRemark, 6, 9) AS Amount
,st.TransportAction
,CASE ptj.TransactionRef
	WHEN 'I'
		THEN ins.Name
	WHEN 'P'
		THEN 'PATIENT'
	WHEN 'G'
		THEN 'MONTLY STMT'
	ELSE ''
END AS InsurerName
FROM model.InvoiceReceivables ir
INNER JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionTypeId = ir.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
LEFT JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
LEFT JOIN model.Insurers ins ON ins.Id = ip.InsurerId
LEFT JOIN ServiceTransactions st ON st.TransactionId = ptj.TransactionId
	AND st.ServiceId = bs.Id
WHERE ptj.TransactionType = 'R'
	AND st.ServiceId IS NULL
	AND i.LegacyPatientReceivablesInvoice = @Invoice
	AND TransportAction IS NOT NULL
	AND ptj.TransactionRef <> 'G' 

UNION ALL

SELECT DISTINCT 
ptj.TransactionDate
,ptj.TransactionStatus
,ptj.TransactionRef
,ptj.transactionaction
,SUBSTRING(ptj.TransactionRemark, 6, 9) AS Amount
,st.TransportAction
,CASE ptj.TransactionRef
	WHEN 'I'
		THEN ins.Name
	WHEN 'P'
		THEN 'PATIENT'
	WHEN 'G'
		THEN 'MONTLY STMT'
	ELSE ''
END AS InsurerName
FROM model.InvoiceReceivables ir
INNER JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionTypeId = ir.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
LEFT JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
LEFT JOIN model.Insurers ins ON ins.Id = ip.InsurerId
LEFT JOIN ServiceTransactions st ON st.TransactionId = ptj.TransactionId
	AND st.ServiceId = bs.Id
WHERE ptj.TransactionType = 'R'
	AND st.ServiceId IS NULL
	AND i.LegacyPatientReceivablesInvoice = @Invoice
	AND ptj.TransactionType = 'R' 
	AND ptj.TransactionRef = 'G' 
	AND ptj.TransactionStatus = 'Z' 
ORDER BY ptj.TransactionDate DESC

GO