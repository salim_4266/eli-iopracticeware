﻿IF EXISTS(SELECT * FROM sys.Triggers WHERE Name = 'AuditEncountersStatusInsertTrigger')
	DROP TRIGGER AuditEncountersStatusInsertTrigger
GO

CREATE TRIGGER [dbo].[AuditEncountersStatusInsertTrigger] ON [dbo].[PracticeActivity]
AFTER INSERT
NOT FOR REPLICATION AS
BEGIN

	SET NOCOUNT ON;

	-- The following trigger inserts new entries into dbo.AuditEntries when there is a new row with a Status in 
	-- dbo.PracticeActivity
	IF [dbo].[GetNoAudit]() = 0
	BEGIN
		DECLARE @__userIdContext NVARCHAR(max)

		IF OBJECT_ID('tempdb..#UserContext') IS NOT NULL
		BEGIN
			SELECT @__userIdContext = UserId
			FROM #UserContext
		END

		DECLARE @__userId INT

		IF (ISNUMERIC(@__userIdContext) = 1)
		BEGIN
			SELECT @__userId = CONVERT(INT, @__userIdContext)
		END

		DECLARE @__auditDate DATETIME

		SET @__auditDate = GETUTCDATE()

		DECLARE @__hostName NVARCHAR(500)

		BEGIN TRY
			EXEC sp_executesql N'SELECT @__hostName = HOST_NAME()'
				,N'@__hostName nvarchar(500) OUTPUT'
				,@__hostName = @__hostName OUTPUT
		END TRY

		BEGIN CATCH
			SET @__hostName = CURRENT_USER
		END CATCH

		DECLARE @EncounterStatus INT
		SELECT @EncounterStatus= EncounterStatusId FROM model.Encounters WHERE Id = (Select AppointmentId FROM inserted)

		SELECT NEWID() AS Id
			,'model.Encounters' AS ObjectName
			,1 AS ChangeTypeId
			,@__auditDate AS AuditDateTime
			,@__hostName AS HostName
			,@__userId AS UserId
			,CONVERT(NVARCHAR, ServerProperty('ServerName')) AS ServerName
			,'[AppointmentId]' AS KeyNames
			,CAST('[' + CAST([AppointmentId] AS NVARCHAR(max)) + ']' AS NVARCHAR(400)) AS KeyValues
			,PatientId AS PatientId
			,AppointmentId AS AppointmentId
		INTO #__AuditEntries
		FROM (
			SELECT i.[AppointmentId]
				,i.[PatientId]
				,COALESCE(CASE i.[Status]
						WHEN 'W'
							THEN 2
						WHEN 'M'
							THEN 3
						WHEN 'E'
							THEN 4
						WHEN 'G'
							THEN 5
						WHEN 'H'
							THEN 6
						WHEN 'U'
							THEN 15
						END, 14) AS EncounterStatusId
			FROM [inserted] i
			
			EXCEPT
			
			SELECT d.[AppointmentId]
				,d.[PatientId]
				,COALESCE(CASE d.[Status]
						WHEN 'W'
							THEN 2
						WHEN 'M'
							THEN 3
						WHEN 'E'
							THEN 4
						WHEN 'G'
							THEN 5
						WHEN 'H'
							THEN 6
						WHEN 'U'
							THEN 15
						END, 14) AS EncounterStatusId
			FROM deleted d
			) AS changedRows

		INSERT INTO dbo.AuditEntries (
			Id
			,ObjectName
			,ChangeTypeId
			,AuditDateTime
			,HostName
			,UserId
			,ServerName
			,KeyNames
			,KeyValues
			,PatientId
			,AppointmentId
			)
		SELECT *
		FROM #__AuditEntries

		
		DECLARE @AuditEntryId UNIQUEIDENTIFIER 
		DECLARE @NewEncounterStatus NVARCHAR(MAX)
		SELECT @AuditEntryId = Id FROM #__AuditEntries
		SELECT @NewEncounterStatus = COALESCE(CASE i.[Status]
						WHEN 'W'
							THEN 2
						WHEN 'M'
							THEN 3
						WHEN 'E'
							THEN 4
						WHEN 'G'
							THEN 5
						WHEN 'H'
							THEN 6
						WHEN 'U'
							THEN 15
						END, 14) FROM inserted i

		INSERT INTO dbo.AuditEntryChanges(
		AuditEntryId,
		ColumnName,
		OldValue,
		NewValue)
		VALUES (@AuditEntryId,
		'EncounterStatusId',
		@EncounterStatus,
		@NewEncounterStatus
		)
	END

	SET NOCOUNT OFF
END