﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ServiceModifiersInsert' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER ServiceModifiersInsert
GO

CREATE TRIGGER [dbo].[ServiceModifiersInsert] ON [dbo].[PracticeCodeTable]
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'SERVICEMODS'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		INSERT INTO model.ServiceModifiers (
			Code
			,[Description]
			,ClinicalOrdinalId
			,BillingOrdinalId
			,IsArchived
		)
		SELECT
		SUBSTRING(Code,1,2) AS Code
		,SUBSTRING(Code,CHARINDEX('-',Code) + 1,LEN(Code)) AS [Description]
		,CASE WHEN ISNUMERIC(AlternateCode) = 1 THEN AlternateCode ELSE 99 END AS ClinicalOrdinalId
		,[Rank] AS BillingOrdinalId
		,CONVERT(BIT,0) AS IsArchived
		FROM #inserted
	END
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ServiceModifiersUpdate' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER ServiceModifiersUpdate
GO

CREATE TRIGGER [dbo].[ServiceModifiersUpdate] ON [dbo].[PracticeCodeTable]
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'SERVICEMODS'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		UPDATE sm
		SET sm.Code = SUBSTRING(i.Code,1,2)
			,sm.[Description] = SUBSTRING(i.Code,CHARINDEX('-',i.Code) + 1,LEN(i.Code)) 
			,sm.ClinicalOrdinalId = CASE WHEN ISNUMERIC(i.AlternateCode) = 1 THEN i.AlternateCode ELSE 99 END
			,sm.[BillingOrdinalId] = i.[Rank]
		FROM model.ServiceModifiers sm
		JOIN deleted d ON SUBSTRING(d.Code,1,2) = sm.Code
			AND SUBSTRING(d.Code,CHARINDEX('-',d.Code) + 1,LEN(d.Code)) = sm.[Description]
		JOIN #inserted i ON i.Id = d.Id
	END
END
GO