﻿IF OBJECT_ID ( 'dbo.GetFindingDetail', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetFindingDetail;
GO

CREATE PROCEDURE dbo.GetFindingDetail
	@ItemId INT
	,@Symptom NVARCHAR(4)
AS
SET NOCOUNT ON;

DECLARE @AppointmentId INT
SET @AppointmentId = (
	SELECT TOP 1 i.LegacyPatientReceivablesAppointmentId 
	FROM model.Invoices i 
	JOIN model.BillingServices bs ON bs.InvoiceId = i.Id 
		AND bs.Id = @ItemId)

SELECT Id INTO #InvoiceReceivableIds FROM dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId)

SELECT
DISTINCT pc.FindingDetail 
FROM model.InvoiceReceivables ir 
INNER JOIN #InvoiceReceivableIds irs ON irs.Id = ir.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
	AND bs.Id = @ItemId
INNER JOIN PatientClinical pc ON pc.AppointmentId = i.LegacyPatientReceivablesAppointmentId
	AND pc.ClinicalType IN ('B','K')
	AND pc.Status = 'A'
	AND pc.Symptom = @Symptom

GO