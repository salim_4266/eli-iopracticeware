﻿IF EXISTS(SELECT * FROM sys.objects WHERE name = 'PatientReceivableServices' AND type = 'U')
BEGIN 
	UPDATE PatientReceivableServices
	SET Modifier = ''
	WHERE LEN(Modifier) IN (1, 3, 5, 7, 9, 11) 
END

IF EXISTS(SELECT * FROM sys.objects WHERE name = 'BillingServiceModifiers' AND type = 'U')
BEGIN
	DELETE FROM model.BillingServiceModifiers WHERE ServiceModifierId IN (SELECT Id FROM model.ServiceModifiers WHERE LEN(Code) IN (1, 3, 5, 7, 9, 11))
	DELETE FROM model.ServiceModifiers WHERE LEN(Code) IN (1, 3, 5, 7, 9, 11)
END
