﻿-- TFS 12267

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID('model.PatientRelationships','U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

IF OBJECT_ID('tempdb..#PreliminaryDelete','U') IS NOT NULL
	DROP TABLE #PreliminaryDelete
GO

;WITH CTE AS (
SELECT 
FromPatientId
,ToPatientId
FROM model.PatientRelationships p
GROUP BY FromPatientId
,ToPatientId
HAVING COUNT(*) > 1
)
SELECT 
p.*
,pin.PolicyHolderRelationshipTypeId
,pind.Id AS PatientInsurancesDeletedId
INTO #PreliminaryDelete
FROM CTE
JOIN model.PatientRelationships p ON p.FromPatientId = CTE.FromPatientId
	AND p.ToPatientId = CTE.ToPatientId
LEFT JOIN model.PatientInsurances pin ON pin.InsuredPatientId = p.FromPatientId
	AND pin.PolicyHolderRelationshipTypeId <> 1
LEFT JOIN model.PatientInsurances pind ON pind.InsuredPatientId = p.FromPatientId
	AND pind.PolicyHolderRelationshipTypeId <> 1
	AND pind.IsDeleted <> 1
LEFT JOIN model.InsurancePolicies ip ON ip.Id = pin.InsurancePolicyId
	AND p.ToPatientId = ip.PolicyHolderPatientId
LEFT JOIN (
	SELECT 1 AS Id, 'Brother' AS Name UNION ALL
	SELECT 2 AS Id, 'Sister' AS Name UNION ALL
	SELECT 3 AS Id, 'Mother' AS Name UNION ALL
	SELECT 4 AS Id, 'Father' AS Name UNION ALL
	SELECT 5 AS Id, 'Spouse' AS Name UNION ALL
	SELECT 6 AS Id, 'Guardian' AS Name UNION ALL
	SELECT 7 AS Id, 'Unknown' AS Name UNION ALL
	SELECT 8 AS Id, 'Self' AS Name
)v ON v.Id = pin.PolicyHolderRelationshipTypeId
	AND v.Name <> p.PatientRelationshipDescription


DELETE FROM model.PatientRelationships WHERE Id IN (SELECT DISTINCT Id FROM #PreliminaryDelete WHERE PatientInsurancesDeletedId IS NULL)
DELETE FROM #PreliminaryDelete WHERE PatientInsurancesDeletedId IS NULL
ALTER TABLE #PreliminaryDelete DROP COLUMN PatientInsurancesDeletedId, HasSignedHipaa

DELETE FROM model.PatientRelationships WHERE Id IN (
SELECT 
p.Id
FROM #PreliminaryDelete p
LEFT JOIN (
	SELECT 1 AS Id, 'Brother' AS Name UNION ALL
	SELECT 2 AS Id, 'Sister' AS Name UNION ALL
	SELECT 3 AS Id, 'Mother' AS Name UNION ALL
	SELECT 4 AS Id, 'Father' AS Name UNION ALL
	SELECT 5 AS Id, 'Spouse' AS Name UNION ALL
	SELECT 6 AS Id, 'Guardian' AS Name UNION ALL
	SELECT 7 AS Id, 'Unknown' AS Name UNION ALL
	SELECT 8 AS Id, 'Self' AS Name
)v ON v.Id = p.PolicyHolderRelationshipTypeId
	AND v.Name = p.PatientRelationshipDescription
WHERE v.Name IS NULL
GROUP BY 
p.Id
,p.FromPatientId
,p.ToPatientId
,p.PatientRelationshipDescription
,p.PolicyHolderRelationshipTypeId
,v.Name
)
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
GO