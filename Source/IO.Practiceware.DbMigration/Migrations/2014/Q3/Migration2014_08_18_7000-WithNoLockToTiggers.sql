﻿/****** Object:  Trigger [model].[OnPatientInsurancesUpdate_StraightJacket]    Script Date: 8/18/2014 12:27:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER TRIGGER [model].[OnPatientInsurancesUpdate_StraightJacket] ON [model].[PatientInsurances] AFTER UPDATE
NOT FOR REPLICATION
		AS 
		SET NOCOUNT ON 
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

		DECLARE 
		@insurancePolicyId INT,
		@patientId INT,
		@policyHolderPatientId INT,
		@newEndDateTime DATETIME,
		@oldEndDateTime DATETIME,
        @policyHoldersCurrentEndDateTime DATETIME

		IF UPDATE(EndDateTime) /* This logic applies only if the end date is being updated */
		BEGIN
DECLARE @msrepl_object_id int
SET @msrepl_object_id = OBJECT_ID('[model].[PatientInsurances]')
DECLARE @retcode int
EXEC @retcode = sp_check_for_sync_trigger @msrepl_object_id
IF @retcode = 1 RETURN
			
			SELECT @newEndDateTime = i.EndDateTime, @insurancePolicyId = i.InsurancePolicyId, @patientId = i.InsuredPatientId FROM inserted i		
			SELECT @oldEndDateTime = d.EndDateTime FROM deleted d			
			SELECT @policyHolderPatientId = ip.PolicyHolderPatientId FROM [model].[InsurancePolicies] ip WHERE ip.Id = @insurancePolicyId
		
            -- Self Policy (Example 1)
			IF @patientId = @policyHolderPatientId 		
            BEGIN		
				UPDATE [model].[PatientInsurances] SET EndDateTime = @newEndDateTime 
                WHERE InsurancePolicyId = @insurancePolicyId and IsDeleted = 0 
                AND (
                        (EndDateTime = @oldEndDateTime) -- end dates are the same
                            OR 
                        (EndDateTime IS NULL AND @oldEndDateTime IS NULL) -- end dates are both null
                            OR 
                        (EndDateTime > @newEndDateTime) -- linked policy's enddate is greater than the new enddate
                     )
            END            
			ELSE -- Linked Policy (Example 2)
            BEGIN                

				UPDATE pin SET pin.EndDateTime = @newEndDateTime 
                FROM [model].[PatientInsurances] pin WITH (NOLOCK) inner join [model].[InsurancePolicies] ip WITH (NOLOCK) on pin.InsurancePolicyId = ip.Id
				WHERE pin.InsuredPatientId = @patientId 
                and ip.PolicyHolderPatientId = @policyHolderPatientId 
                and pin.IsDeleted = 0 
                and ip.StartDateTime <= @newEndDateTime          
                and
                 ((select pin2.EndDateTime from model.PatientInsurances pin2  WITH (NOLOCK)
                            inner join model.InsurancePolicies ip2 WITH (NOLOCK) on pin2.InsurancePolicyId = ip2.Id 
                            WHERE ip2.PolicyholderPatientId = @policyHolderPatientId 
                            AND pin2.InsuredPatientId = @policyHolderPatientId 
                            AND pin2.InsurancePolicyId = pin.InsurancePolicyId
                            AND pin2.IsDeleted = 0) IS NULL)
                OR
                 (@newEndDateTime <=                          
                        (select pin2.EndDateTime from model.PatientInsurances pin2  WITH (NOLOCK)
                            inner join model.InsurancePolicies ip2 WITH (NOLOCK) on pin2.InsurancePolicyId = ip2.Id 
                            WHERE ip2.PolicyholderPatientId = @policyHolderPatientId 
                            AND pin2.InsuredPatientId = @policyHolderPatientId 
                            AND pin2.InsurancePolicyId = pin.InsurancePolicyId
                            AND pin2.IsDeleted = 0))
                        			
            END
					
		END
		SET NOCOUNT OFF
	RETURN 

GO
/****** Object:  Trigger [model].[OnPatientInsurancesInsert_StraightJacket]    Script Date: 8/18/2014 12:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER TRIGGER [model].[OnPatientInsurancesInsert_StraightJacket] ON [model].[PatientInsurances]
AFTER INSERT 
NOT FOR REPLICATION 
AS
SET NOCOUNT ON 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @insertedInsurancePolicyId INT
	,@insertedInsuredPatientId INT
	,@insertedPolicyHolderPatientId INT
	,@insertedPolicyHolderRelationshipTypeId INT
	,@insertedInsuranceTypeId INT
	,@insertedEndDateTime DATETIME
	,@insurerIdToInsert INT
	,@insurancePolicyIdToInsert INT
	,@currentPolicyHolderRelationshipTypeId INT
	,@currentLinkedPatientId INT
	,@allowDependants BIT
	,@insuranceTypeIdToInsert INT
	,@ordinalIdToInsert INT
	,@endDateTimeToInsert DATETIME

BEGIN
DECLARE @msrepl_object_id int
SET @msrepl_object_id = OBJECT_ID('[model].[PatientInsurances]')
DECLARE @retcode int
EXEC @retcode = sp_check_for_sync_trigger @msrepl_object_id
IF @retcode = 1 RETURN
	SELECT @insertedInsurancePolicyId = i.InsurancePolicyId
		,@insertedInsuredPatientId = i.InsuredPatientId
		,@insertedPolicyHolderRelationshipTypeId = i.PolicyHolderRelationshipTypeId
		,@insertedInsuranceTypeId = i.InsuranceTypeId
		,@insertedEndDateTime = i.EndDateTime
	FROM inserted i;

	SELECT @insertedPolicyHolderPatientId = ip.PolicyHolderPatientId
	FROM [model].[InsurancePolicies] ip
	WHERE ip.Id = @insertedInsurancePolicyId;

	/* Example #1:					
			               I'm Joe (@insertedInsuredPatientId). I add Jane's (@insertedPolicyHolderPatientId) policy for Major Medical. I also 
                           need a PatientInsurance for all of Jane's other policies where she is the policyholder and 
			               those policies' insurers' allow dependents flag is set to true. (this is across ALL insurance types).
			*/
	/* NOT SELF POLICY - we need to add a new insurance for each policy the Policyholder has to the patient */
	IF @insertedInsuredPatientId <> @insertedPolicyHolderPatientId
	BEGIN
		-- create a cursor which will loop through the 'other' policies that belong to the policyholder.  we only grab the policies that do not already belong to the current patient
		DECLARE policyholdersOtherPoliciesCursor CURSOR
		FOR
		SELECT DISTINCT ip.InsurerId
			,ip.Id
			,pi.InsuranceTypeId
			,pi.EndDateTime
		FROM model.InsurancePolicies ip WITH (NOLOCK)
		INNER JOIN model.PatientInsurances pi WITH (NOLOCK) ON ip.Id = pi.InsurancePolicyId
		INNER JOIN model.Insurers ins WITH (NOLOCK) ON ip.InsurerId = ins.Id
		WHERE ip.PolicyHolderPatientId = @insertedPolicyHolderPatientId -- get only the policyholders insurance policies
			AND ip.Id <> @insertedInsurancePolicyId -- exclude the policy that was just inserted for the patient
			AND pi.IsDeleted = 0 AND pi.InsuredPatientId = @insertedPolicyHolderPatientId AND ins.AllowDependents = 1 -- see if the insurer for the current policy supports dependents
			AND ip.Id NOT IN (
				SELECT pin.InsurancePolicyId
				FROM PatientInsurances pin
				WHERE pin.InsuredPatientId = @insertedInsuredPatientId AND pin.IsDeleted = 0
				) -- make sure to only get the policies that are NOT linked to the current patient

		OPEN policyholdersOtherPoliciesCursor

		FETCH NEXT
		FROM policyholdersOtherPoliciesCursor
		INTO @insurerIdToInsert
			,@insurancePolicyIdToInsert
			,@insuranceTypeIdToInsert
			,@endDateTimeToInsert

		WHILE @@FETCH_STATUS = 0 /* Loop through the Policy Holder's Insurance Policies  */
		BEGIN
			-- get the next ordinal id for this patient within the specific insurance type for active insurances
			SELECT @ordinalIdToInsert = (MAX(pin.OrdinalId) + 1)
			FROM model.PatientInsurances pin
			WHERE pin.InsuredPatientId = @insertedInsuredPatientId AND pin.InsuranceTypeId = @insuranceTypeIdToInsert AND pin.IsDeleted = 0 AND ((pin.EndDateTime IS NULL) OR (pin.EndDateTime >= DateAdd(yy, DATEPART(yy, model.GetClientDateNow()) - 1900, DateAdd(m, DATEPART(MONTH, model.GetClientDateNow()) - 1, DATEPART(DAY, model.GetClientDateNow()) - 1))));

			IF @ordinalIdToInsert IS NULL OR @ordinalIdToInsert = 0
			BEGIN
				SET @ordinalIdToInsert = 1;
			END

			INSERT INTO model.PatientInsurances (
				InsuranceTypeId
				,PolicyHolderRelationshipTypeId
				,OrdinalId
				,InsuredPatientId
				,InsurancePolicyId
				,EndDateTime
				,IsDeleted
				)
			VALUES (
				@insuranceTypeIdToInsert
				,@insertedPolicyHolderRelationshipTypeId
				,@ordinalIdToInsert
				,@insertedInsuredPatientId
				,@insurancePolicyIdToInsert
				,@endDateTimeToInsert
				,0
				)

			FETCH NEXT
			FROM policyholdersOtherPoliciesCursor
			INTO @insurerIdToInsert
				,@insurancePolicyIdToInsert
				,@insuranceTypeIdToInsert
				,@endDateTimeToInsert
		END

		CLOSE policyholdersOtherPoliciesCursor;

		DEALLOCATE policyholdersOtherPoliciesCursor;
	END

	/* Example #2:					
			               I'm Joe (@insertedInsuredPatientId). I add a SELF (@insertedPolicyHolderPatientId) policy for Major Medical. 
                           I need to check all patient's who have insurances that are linked to
                           ANY of my policies and add a new PatientInsurance to each of them for this new incoming policy
			*/
	/* SELF POLICY - we need to add a new insurance for all patients linked to this policyholder */
	IF @insertedInsuredPatientId = @insertedPolicyHolderPatientId
	BEGIN
		-- Check to see if the Insurer on the Policy allows dependants
		SELECT @allowDependants = AllowDependents
		FROM model.Insurers
		WHERE Id = (
				SELECT InsurerId
				FROM model.InsurancePolicies
				WHERE Id = @insertedInsurancePolicyId
				)

		IF @allowDependants = 1
		BEGIN
			-- Get list of linked patients
			DECLARE linkedPatientsCursor CURSOR
			FOR
			SELECT DISTINCT TBL1.cursorPatientId
				,TBL1.cursorPolicyHolderRelationshipTypeId
			FROM (
				SELECT pin.InsuredPatientId AS cursorPatientId
					,pin.PolicyHolderRelationshipTypeId AS cursorPolicyHolderRelationshipTypeId
					,pin.InsurancePolicyId AS cursorInsurancePolicyId
				FROM model.PatientInsurances pin WITH (NOLOCK)
				INNER JOIN model.InsurancePolicies ip  WITH (NOLOCK) ON pin.InsurancePolicyId = ip.Id AND ip.PolicyholderPatientId = @insertedInsuredPatientId
				WHERE pin.IsDeleted = 0 AND pin.InsuredPatientId <> @insertedInsuredPatientId
				) AS TBL1
			WHERE TBL1.cursorPatientId NOT IN (
					SELECT pin2.InsuredPatientId
					FROM model.PatientInsurances pin2
					WHERE pin2.InsurancePolicyId = @insertedInsurancePolicyId
					);

			OPEN linkedPatientsCursor

			FETCH NEXT
			FROM linkedPatientsCursor
			INTO @currentLinkedPatientId
				,@currentPolicyHolderRelationshipTypeId

			WHILE @@FETCH_STATUS = 0 /* Loop through the patient's that are linked to the policyholder */
			BEGIN
				-- get the next ordinal id for this patient within the specific insurance type for active insurances
				SELECT @ordinalIdToInsert = (MAX(pin.OrdinalId) + 1)
				FROM model.PatientInsurances pin WITH (NOLOCK)
				WHERE pin.InsuredPatientId = @currentLinkedPatientId AND pin.InsuranceTypeId = @insertedInsuranceTypeId AND pin.IsDeleted = 0 AND ((pin.EndDateTime IS NULL) OR (pin.EndDateTime >= DateAdd(yy, DATEPART(yy, model.GetClientDateNow()) - 1900, DateAdd(m, DATEPART(MONTH, model.GetClientDateNow()) - 1, DATEPART(DAY, model.GetClientDateNow()) - 1))));

				IF @ordinalIdToInsert IS NULL OR @ordinalIdToInsert = 0
				BEGIN
					SET @ordinalIdToInsert = 1;
				END

				INSERT INTO model.PatientInsurances (
					InsuranceTypeId
					,PolicyHolderRelationshipTypeId
					,OrdinalId
					,InsuredPatientId
					,InsurancePolicyId
					,EndDateTime
					,IsDeleted
					)
				VALUES (
					@insertedInsuranceTypeId
					,@currentPolicyHolderRelationshipTypeId
					,@ordinalIdToInsert
					,@currentLinkedPatientId
					,@insertedInsurancePolicyId
					,@insertedEndDateTime
					,0
					);

				FETCH NEXT
				FROM linkedPatientsCursor
				INTO @currentLinkedPatientId
					,@currentPolicyHolderRelationshipTypeId
			END

			CLOSE linkedPatientsCursor;

			DEALLOCATE linkedPatientsCursor;
		END
	END

	;WITH CTE AS (
		SELECT 
		ip.InsurerId
		,pi.InsuredPatientId
		,pi.InsuranceTypeId
		FROM model.InsurancePolicies ip
		INNER JOIN model.PatientInsurances pi ON pi.InsurancePolicyId = ip.Id
		WHERE pi.InsuredPatientId IN (SELECT DISTINCT i.InsuredPatientId FROM inserted i)
			AND ip.InsurerId IN (SELECT DISTINCT ip.InsurerId FROM inserted i INNER JOIN model.InsurancePolicies ip ON ip.Id = i.InsurancePolicyId)
			AND pi.InsuranceTypeId IN (SELECT i.InsuranceTypeId FROM inserted i)
			AND pi.PolicyHolderRelationshipTypeId = 1
		GROUP BY ip.InsurerId
			,pi.InsuredPatientId
			,pi.InsuranceTypeId
		HAVING COUNT(InsurerId) > 1
	)
	,CTE2 AS (
		SELECT 
		i.Id AS InvoiceId
		,ip.InsurerId
		,pi.Id AS PatientInsuranceId
		FROM model.PatientInsurances pi
		INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		INNER JOIN CTE ON CTE.InsurerId = ip.InsurerId	
			AND CTE.InsuredPatientId = pi.InsuredPatientId
			AND CTE.InsuranceTypeId = pi.InsuranceTypeId
		INNER JOIN dbo.Appointments e ON e.PatientId = pi.InsuredPatientId
		LEFT JOIN model.Invoices i ON i.EncounterId = e.EncounterId
		LEFT JOIN model.InvoiceReceivables ir ON ir.PatientInsuranceId = pi.Id
			AND ir.InvoiceId = i.Id
		WHERE ir.Id IS NULL
			AND i.LegacyDateTime >= ip.StartDateTime
			AND (i.LegacyDateTime <= pi.EndDateTime OR pi.EndDateTime IS NULL)
			AND pi.PolicyHolderRelationshipTypeId = 1
			
	)
	UPDATE ir
	SET ir.PatientInsuranceId = CTE2.PatientInsuranceId
	FROM model.InvoiceReceivables ir
	INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
	INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	INNER JOIN CTE2 ON CTE2.InvoiceId = i.Id
	WHERE ip.InsurerId = CTE2.InsurerId
END