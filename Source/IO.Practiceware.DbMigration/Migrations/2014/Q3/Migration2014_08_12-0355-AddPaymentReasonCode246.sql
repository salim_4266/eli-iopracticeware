﻿--Bug #13702.

-- Insert for ClaimAdjustmentReasonCode 246 if it doesn't exist
IF NOT EXISTS(SELECT * FROM PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '246')
BEGIN

	INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, [Signature], TestOrder, FollowUp, [Rank], WebsiteUrl)
	  VALUES('PAYMENTREASON', 
			 '246', 
			  'Non-payable code for required reporting',
			  'F', 
			  '', 
			  '', 
			  '', 
			  'F', 
			  '', 
			  '',
			  1, 
			  NULL)
END
