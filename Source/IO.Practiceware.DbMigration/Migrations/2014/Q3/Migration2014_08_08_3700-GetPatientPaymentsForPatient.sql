﻿IF (OBJECT_ID('dbo.GetPatientPaymentsForPatient', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE dbo.GetPatientPaymentsForPatient
END
GO

CREATE PROCEDURE dbo.GetPatientPaymentsForPatient (@PatientId INT)
AS
BEGIN
SET NOCOUNT ON;

IF (OBJECT_ID('tempdb..#adjusts','U')) IS NOT NULL
	DROP TABLE #adjusts

SELECT
SUM(
	CASE 
		WHEN adjT.IsDebit = 1 
			THEN (CASE WHEN adj.Amount > 0 THEN -adj.Amount ELSE adj.Amount END) 
		ELSE adj.Amount 
	END
	) AS Adjust
,ir.InvoiceId
INTO #adjusts
FROM model.Adjustments adj 
INNER JOIN model.AdjustmentTypes adjT ON adjT.Id = adj.AdjustmentTypeId
INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
INNER JOIN model.Invoices i ON ir.InvoiceId = i.Id 
	AND i.LegacyPatientReceivablesPatientId = @PatientId
GROUP BY ir.InvoiceId

SELECT
CONVERT(NVARCHAR(8),i.LegacyPatientBilledDateTime,112) AS ServiceDate
,bs.Id AS ItemId
,es.Code AS [Service]
,COALESCE(STUFF((
	SELECT sm.Code AS [text()]
	FROM model.BillingServiceModifiers mo
	JOIN model.ServiceModifiers sm ON sm.Id = mo.ServiceModifierId
	WHERE mo.BillingServiceId = m.BillingServiceId
	ORDER BY mo.OrdinalId ASC
	FOR XML PATH('')
), 1, 0, ''),'') AS Modifier
,bs.Unit AS Quantity
,(bs.Unit * bs.UnitCharge) AS Charge
,bs.UnitAllowableExpense AS FeeCharge
,i.LegacyPatientReceivablesAppointmentId
,i.LegacyPatientReceivablesInvoice AS Invoice
,ir.Id AS ReceivableId
,CONVERT(DECIMAL(18,2),(prs.UnitCharge * prs.Unit) - SUM(CASE WHEN at.IsDebit = 0 THEN adj.Amount WHEN at.IsDebit = 1 THEN -adj.Amount ELSE 0 END)) AS PreviousBalance
FROM model.BillingServices bs 
JOIN model.Invoices i ON i.Id = bs.InvoiceId
JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
LEFT JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
LEFT JOIN model.BillingServiceModifiers m ON m.BillingServiceId = bs.Id
LEFT JOIN model.Adjustments adj ON adj.BillingServiceId = bs.Id
LEFT JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
LEFT JOIN dbo.Charges WITH(NOEXPAND) ON charges.InvoiceId = ir.InvoiceId
LEFT JOIN #adjusts ON #adjusts.InvoiceId  = ir.InvoiceId
LEFT JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionTypeId = ir.Id
	AND ptj.TransactionType = 'R' 
	AND ptj.TransactionRef = 'P' 
	AND TransactionStatus IN ('S', 'P')
WHERE i.LegacyPatientReceivablesPatientId = @PatientId
	AND CASE
	WHEN charges.Charge IS NOT NULL
		THEN CASE
				WHEN #adjusts.Adjust IS NOT NULL
					THEN CONVERT(REAL, charges.Charge-#adjusts.Adjust)
				WHEN #adjusts.Adjust IS NULL
					THEN CONVERT(REAL, charges.Charge)
			END
	ELSE 0
	END > 0 
	AND ptj.TransactionId IS NOT NULL
GROUP BY i.LegacyDateTime
,bs.Id
,es.Code
,COALESCE(STUFF((
	SELECT sm.Code AS [text()]
	FROM model.BillingServiceModifiers mo
	JOIN model.ServiceModifiers sm ON sm.Id = mo.ServiceModifierId
	WHERE mo.BillingServiceId = m.BillingServiceId
	ORDER BY mo.OrdinalId ASC
	FOR XML PATH('')
), 1, 0, ''),'')
,bs.Unit
,(bs.Unit * bs.UnitCharge)
,bs.UnitAllowableExpense
,i.LegacyPatientReceivablesAppointmentId
,i.LegacyPatientReceivablesInvoice
,ir.Id
ORDER BY i.LegacyDateTime DESC
,i.LegacyPatientReceivablesInvoice


END
GO