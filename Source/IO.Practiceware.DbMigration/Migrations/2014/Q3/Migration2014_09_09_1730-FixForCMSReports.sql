﻿IF EXISTS (SELECT object_id FROM sys.tables WHERE Name = 'CMSReports')
BEGIN

	DECLARE @Constraint NVARCHAR(50)
	SET @Constraint = ''
	SELECT @Constraint = so.Name from sysconstraints sc
	INNER JOIN sys.objects so on sc.constid = so.object_id
	WHERE NAME LIKE '%CMSReports__IsSP%'

	DECLARE @ForeignConstraint NVARCHAR(50)
	SET @ForeignConstraint = ''
	SELECT @ForeignConstraint = name
	FROM sys.foreign_keys
	WHERE referenced_object_id = object_id('CMSReports')

	IF @Constraint <> '' 
	EXEC('ALTER TABLE dbo.CMSReports DROP CONSTRAINT ' + @Constraint)
	IF @ForeignConstraint <> ''
	EXEC('ALTER TABLE dbo.CMSReports_Params DROP CONSTRAINT ' + @ForeignConstraint)


	DROP TABLE [dbo].[CMSReports]
END

CREATE TABLE [dbo].[CMSReports](
	[CMSRepId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CMSRepCode] [varchar](200) NOT NULL,
	[CMSRepName] [varchar](400) NOT NULL,
	[CMSQuery] [text] NOT NULL,
	[CMSSubHeading] [varchar](400) NOT NULL,
	[IsSP] [bit] NOT NULL,
	[Numerator] [varchar](400) NOT NULL,
	[Denominator] [varchar](400) NOT NULL,
	[ReportType] [varchar](50) NULL,
	[Active_YN] [bit] NULL,
 CONSTRAINT [PK_CMSReports] PRIMARY KEY CLUSTERED 
(
	[CMSRepId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

ALTER TABLE [dbo].[CMSReports] ADD  DEFAULT ((1)) FOR [IsSP]

INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('Measure #012','1. #12 POAG: Optic Nerve Evaluation','USP_PQRI_GetPOAGDetails','Optic Nerve Evaluation',1,'Patients who have an optic nerve head evaluation during one or more office visits within 12 months','All patients aged 18 years and older with a diagnosis of primary open-angle glaucoma','PQRI',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('Measure #014','2. #14 AMD: Dilated Macular Examination','USP_PQRI_GetAMD_Dilated_Macular_Examination','Dilated Macular Examination',1,'Patients who had a dilated exam documentating macular thickening or hemorrhage plus severity of macular degeneration','All patients aged 50 years and older with a diagnosis of age-related macular degeneration','PQRI',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('Measure #018','3. #18 Diabetic Retinopathy: Documentation of Macular Edema and Severity of Retinopathy','USP_PQRI_GetDiabetic_Retinopathy_MacularEdema','Documentation of Macular Edema and Level of Severity of Retinopathy',1,'Patients who had a dilated exam documenting the severity of retinopathy AND the presence or absence of macular edema','All patients aged 18 years and older with diabetic retinopathy','PQRI',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('Measure #019','4. #19 Diabetic Retinopathy: Communication with the Physician Managing On-Diabetes Care','USP_PQRI_GetDiabetic_Retinopathy_Communication_Physician','Communication with the Physician Managing On-going Diabetes Care',1,'Patients with documentation of a dilated exam communicated to the physician managing the patient’s diabetic care','All patients aged 18 years and older with diabetic retinopathy who had a dilated macular or fundus exam performed','PQRI',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('Measure #117','5. #117 Diabetes Mellitus: Dilated Eye Exam in Diabetic Patient','USP_PQRI_Diabetes_GetMellitus_Dilated_Eye','Dilated Eye Exam in Diabetic Patient',1,'Patients who had a dilated eye exam for diabetic retinal disease at least once within 12 months','All patients aged 18 through 75 years with a diagnosis of diabetes','PQRI',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('Measure #140','7. #140 AMD: Counseling on Antioxidant Supplement','USP_PQRI_GetAMD_AREDS_Counseling','Counseling on Antioxidant Supplement',1,'Patients who were counseled within 12 months on the benefits and/or risks of the AREDS formulation','All patients aged 50 years and older with AMD','PQRI',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('Measure #141','8. #141 POAG: Reduction of IOP or Documentation of Plan','USP_PQRI_GetPOAG_ReducePressure','Reduction of Intraocular Pressure or Documentation of Plan of Care',1,'Patients whose most recent IOP was reduced by at least 15% or where a plan of care was documented','All patients aged 18 years and older with a diagnosis of primary open-angle glaucoma','PQRI',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('Measure #191','98. #191 Cataracts: 20/40 or Better Visual Acuity within 90 Days Following Cataract Surgery','USP_PQRI_GetCataracts_Visual_Acuity','20/40 or Better Visual Acuity within 90 Days Following Cataract Surgery',1,'Patients who had best-corrected visual acuity of 20/40 or better achieved within 90 days following cataract surgery','All patients aged 18 years and older without pre-operative ocular conditions who had cataract surgery','PQRI',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('Measure #192','99. #192 Cataracts: Complications within 30 Days Following Cataract Surgery Requiring Additional Surgery','USP_PQRI_GetCataracts_Complications','Complications within 30 Days of Cataract Surgery Requiring Additional Surgery',1,'Patients without complications within 30 days of cataract surgery','All patients aged 18 years and older without pre-operative ocular conditions who had cataract surgery','PQRI',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 001','Stage 1 - CORE 1: Computerized Provider Order Entry (CPOE)','model.USP_CMS_Stage1_CPOEMedications','Stage 1 - CORE 1: Computerized Provider Order Entry (CPOE)',1,'Number of patients in the denominator that have at least one medication order entered using CPOE','Number of unique patients with at least one medication in their medication list seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 001.1','Stage 1 - CORE 1: Computerized Provider Order Entry (CPOE) alternate','model.USP_CMS_Stage2_CPOEMedications','Stage 1 - CORE 1: Computerized Provider Order Entry (CPOE) alternate',1,'Number of medication orders in the denominator recorded using CPOE','Number of medication orders created by an EP during the EHR reporting period ','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 003','Stage 1 - CORE 3: Maintain Problem List','model.USP_CMS_Stage1_ProblemList','Stage 1 - CORE 3: Maintain Problem List',1,'Number of patients in the denominator who have at least one entry or an indication that no problems are known for the patient recorded as structured data in their problem list','Number of unique patients seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 004','Stage 1 - CORE 4: e-Prescribing (eRx)','dbo.USP_CMS_GetEPrescStatus','Stage 1 - CORE 4: e-Prescribing (eRx)',1,'Number of prescriptions in the denominator generated and transmitted electronically','Number of prescriptions written for drugs requiring a prescription in order to be dispensed other than controlled substances during the EHR reporting period ','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 005','Stage 1 - CORE 5: Active Medication List','model.USP_CMS_Stage1_MedicationList','Stage 1 - CORE 5: Active Medication List',1,'Number of patients in the denominator who have a medication (or an indication that the patient is not currently prescribed any medication) recorded as structured data ','Number of unique patients seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 006','Stage 1 - CORE 6: Medication Allergy List','model.USP_CMS_Stage1_MedicationAllergyList','Stage 1 - CORE 6: Medication Allergy List',1,'Number of unique patients in the denominator who have at least one entry (or an indication that the patient has no known medication allergies) recorded as structured data in their medication allergy list ','Number of unique patients seen by the EP during the EHR report period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 007','Stage 1 - CORE 7: Record Demographics','model.USP_CMS_Stage1_Demographics','Stage 1 - CORE 7: Record Demographics',1,'Number of patients in the denominator who have all the elements of demographics (or a specific exclusion if the patient declined to provide one or more elements or if recording an element is contrary to state law) recorded as structured data','Number of unique patients seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 008.21','Stage 1 - CORE 8: Record Vital Signs','model.USP_CMS_Stage2_VitalSigns','Stage 1 - CORE 8: Record Vital Signs',1,'Number of unique patients (age 3 or over for blood pressure) seen by the EP during the EHR reporting period','Number of patients in the denominator who have at least one entry of their height, weight and blood pressure (ages 3 and over) recorded as structured data. ','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 008.22','Stage 1 - CORE 8: Record Vital Signs (Exclusion 1 or 3)','model.USP_CMS_Stage2_VitalSignsNoBP','Stage 1 - CORE 8: Record Vital Signs (Exclusion 1 or 3)',1,'Number of unique patients seen by the EP during the EHR reporting period','Number of patients in the denominator who have at least one entry of their height and weight recorded as structured data. ','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 008.23','Stage 1 - CORE 8: Record Vital Signs (Exclusion 4)','model.USP_CMS_Stage2_VitalSignsNoHW','Stage 1 - CORE 8: Record Vital Signs (Exclusion 4)',1,'Number of unique patients age 3 or over seen by the EP during the EHR reporting period','Number of patients in the denominator who have at least one entry of their blood pressure recorded as structured data. ','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 009','Stage 1 - CORE 9: Record Smoking Status','model.USP_CMS_Stage1_SmokingStatus','Stage 1 - CORE 9: Record Smoking Status',1,'Number of patients in the denominator with smoking status recorded as structured data','Number of unique patients age 13 or older seen by the EP during the EHR reporting period ','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 012','Stage 1 - CORE 11: View, Download, Transmit Timely Access','model.USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess','Stage 1 - CORE 11: View, Download, Transmit Timely Access',1,'Number of patients in the denominator who have timely (within 4 business days after the information is available to the EP) online access to their health information to view, download, and transmit to a third party','Number of unique patients seen by the EP during the EHR reporting period ','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 013','Stage 1 - CORE 12: Clinical Summaries','model.USP_CMS_Stage1_ClinicalSummary','Stage 1 - CORE 12: Clinical Summaries',1,'Number of office visits in the denominator for which the patient is provided a clinical summary within three business days','Number of office visits by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 022','Stage 1 - MENU 2: Clinical Lab-Test Results','model.USP_CMS_Stage1_ClinicalLabTestResults','Stage 1 - MENU 2: Clinical Lab-Test Results',1,'Number of lab test results whose results are expressed in a positive or negative affirmation or as a number which are incorporated as structured data','Number of lab tests ordered during the EHR reporting period by the EP whose results are expressed in a positive or negative affirmation or as a number','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 024','Stage 1 - MENU 4: Patient Reminders','model.USP_CMS_Stage1_PatientReminders','Stage 1 - MENU 4: Patient Reminders',1,'Number of patients in the denominator who were sent the appropriate reminder','Number of unique patients 65 years old or older or 5 years older or younger','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 026','Stage 1 - MENU 5: Patient-specific Education Resources','model.USP_CMS_Stage1_PatientEducation','Stage 1 - MENU 5: Patient-specific Education Resources',1,'Number of patients in the denominator who are provided patient-specific education resources','Number of unique patients seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 027','Stage 1 - MENU 6: Perform medication reconciliation for referral patients','model.USP_CMS_Stage2_MedicationReconciliation','Stage 1 - MENU 6: Perform medication reconciliation for referral patients',1,'Number of transitions of care in the denominator where medication reconciliation was performed','Number of transitions of care during the EHR reporting period for which the EP was the receiving party of the transition','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 028','Stage 1 - MENU 7: Summary of Care','model.USP_CMS_Stage1_SummaryOfCare','Stage 1 - MENU 7: Summary of Care',1,'Number of transitions of care and referrals in the denominator where a summary of care record was provided','Number of transitions of care and referrals during the EHR reporting period for which the EP was the transferring or referring provider','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 201.1','Stage 2 - CORE 1 Computerized Provider Order Entry (CPOE) Measure 1: Medication Orders','model.USP_CMS_Stage2_CPOEMedications','Stage 2 - CORE 1 Computerized Provider Order Entry (CPOE) Measure 1: Medication Orders',1,'The number of orders in the denominator recorded using CPOE','Number of medication orders created by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 201.2','Stage 2 - CORE 1: Computerized Provider Order Entry (CPOE) Measure 2: Radiology Orders','model.USP_CMS_Stage2_CPOERadiology','Stage 2 - CORE 1: Computerized Provider Order Entry (CPOE) Measure 2: Radiology Orders',1,'The number of orders in the denominator recorded using CPOE','Number of radiology orders created by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 201.3','Stage 2 - CORE 1: Computerized Provider Order Entry (CPOE) Measure 3: Laboratory Orders','model.USP_CMS_Stage2_CPOELaboratory','Stage 2 - CORE 1: Computerized Provider Order Entry (CPOE) Measure 3: Laboratory Orders',1,'The number of orders in the denominator recorded using CPOE','Number of laboratory orders created by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 202','Stage 2 - CORE 2: e-Prescribing (eRx)','model.USP_CMS_Stage2_eRx','Stage 2 - CORE 2: e-Prescribing (eRx)',1,'The number of prescriptions in the denominator generated, queried for a drug formulary and transmitted electronically using CEHRT','Number of prescriptions written for drugs requiring a prescription in order to be dispensed other than controlled substances during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 204.1','Stage 2 - CORE 4: Record Vital Signs','model.USP_CMS_Stage2_VitalSigns','Stage 2 - CORE 4: Record Vital Signs',1,'Number of patients in the denominator who have at least one entry of their height/length and weight (all ages) and/or blood pressure (ages 3 and over) recorded as structured data','Number of unique patients seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 204.2','Stage 2 - CORE 4: Record Vital Signs (Exclusion 1 or 3)','model.USP_CMS_Stage2_VitalSignsNoBP','Stage 2 - CORE 4: Record Vital Signs (Exclusion 1 or 3)',1,'Number of patients in the denominator who have at least one entry of their height/length and weight (all ages) recorded as structured data','Number of unique patients seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 204.3','Stage 2 - CORE 4: Record Vital Signs (Exclusion 4)','model.USP_CMS_Stage2_VitalSignsNoHW','Stage 2 - CORE 4: Record Vital Signs (Exclusion 4)',1,'Number of patients in the denominator who have at least one entry of their blood pressure (ages 3 and over) recorded as structured data','Number of unique patients seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 205','Stage 2 - CORE 5: Record Smoking Status','model.USP_CMS_Stage1_SmokingStatus','Stage 2 - CORE 5: Record Smoking Status',1,'The number of patients in the denominator with smoking status recorded as structured data','Number of unique patients age 13 or older seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 203','Stage 2 - CORE 3: Record Demographics','model.USP_CMS_Stage1_Demographics','Stage 2 - CORE 3: Record Demographics',1,'The number of patients in the denominator who have all the elements of demographics (or a specific notation if the patient declined to provide one or more elements or if recording an element is contrary to state law) recorded as structured data','Number of unique patients seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 207.2','Stage 2 - CORE 7: Patient Electronic Access Measure 2: Patient Use','model.USP_CMS_Stage2_ViewDownloadTransmitPatientUse','Stage 2 - CORE 7: Patient Electronic Access Measure 2: Patient Use',1,'The number of unique patients (or their authorized representatives) in the denominator who have viewed online, downloaded, or transmitted to a third party the patient''s health information','Number of unique patients seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 208','Stage 2 - CORE 8: Clinical Summaries','model.USP_CMS_Stage2_ClinicalSummary','Stage 2 - CORE 8: Clinical Summaries',1,'The number of unique patients (or their authorized representatives) in the denominator who have viewed online, downloaded, or transmitted to a third party the patient''s health information','Number of office visits conducted by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 210','Stage 2 – CORE 10: Clinical Lab - Test Results','dbo.USP_CMS_HL7LabOrdersStats','Stage 2 – CORE 10: Clinical Lab - Test Results',1,'Number of lab test results which are expressed in a positive or negative affirmation or as a numeric result which are incorporated in CEHRT as structured data','Number of lab tests ordered during the EHR reporting period by the EP whose results are expressed in a positive or negative affirmation or as a number','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 212','Stage 2 - CORE 12: Patient Reminders','model.USP_CMS_Stage2_PatientReminders','Stage 2 - CORE 12: Patient Reminders',1,'Number of patients in the denominator who were sent a reminder per patient preference when available during the EHR reporting period','Number of unique patients who have had two or more office visits with the EP in the 24 months prior to the beginning of the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 213','Stage 2 – CORE 13: Patient - Specific Education Resources','model.USP_CMS_Stage1_PatientEducation','Stage 2 – CORE 13: Patient - Specific Education Resources',1,'Number of patients in the denominator who were provided patient-specific education resources identified by the Certified EHR Technology','Number of unique patients with office visits seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 214','Stage 2 – CORE 14: Medication Reconciliation','dbo.USP_CMS_GetMedicationReconStatus_Stage2','Stage 2 – CORE 14: Medication Reconciliation',1,'The number of transitions of care in the denominator where medication reconciliation was performed','Number of transitions of care during the EHR reporting period for which the EP was the receiving party of the transition','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 215.1','Stage 2 – CORE 15: Summary of Care Measure 1: Provide Records','model.USP_CMS_Stage1_SummaryOfCare','Stage 2 – CORE 15: Summary of Care Measure 1: Provide Records',1,'The number of transitions of care and referrals in the denominator where a summary of care record was provided','Number of transitions of care and referrals during the EHR reporting period for which the EP was the transferring or referring provider','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 215.2','Stage 2 - CORE 15: Summary of Care Measure 2: Provide records via Direct','model.USP_CMS_Stage2_SummaryOfCareB','Stage 2 - CORE 15: Summary of Care Measure 2: Provide records via Direct',1,'The number of transitions of care and referrals in the denominator where a summary of care record was electronically transmitted using CEHRT to a recipient','Number of transitions of care and referrals during the EHR reporting period for which the EP was the transferring or referring provider','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 217','Stage 2 - CORE 17: Use Secure Electronic Messaging','model.USP_CMS_Stage2_SecureMessage','Stage 2 - CORE 17: Use Secure Electronic Messaging',1,'The number of patients or patient-authorized representatives in the denominator who send a secure electronic message to the EP that is received using the electronic messaging function of CEHRT during the EHR reporting period','Number of unique patients seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 222','Stage 2 - MENU 2: Electronic Notes','model.USP_CMS_Stage2_ElectronicNotes','Stage 2 - MENU 2: Electronic Notes',1,'The number of unique patients in the denominator who have at least one electronic progress note from an eligible professional recorded as text searchable data','Number of unique patients with at least one office visit during the EHR reporting period for EPs during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 224','Stage 2 - MENU 4: Family Health History','model.USP_CMS_Stage2_FamilyHistory','Stage 2 - MENU 4: Family Health History',1,'The number of patients in the denominator with a structured data entry for one or more first-degree relatives','Number of unique patients seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 223','Stage 2 - MENU 3: Imaging results','model.USP_CMS_Stage2_Imaging','Stage 2 - MENU 3: Imaging results',1,'The number of results in the denominator that are accessible through CEHRT','Number of tests whose result is one or more images ordered by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 207.1','Stage 2 - CORE 7: Patient Electronic Access Measure 1: Timely Access','model.USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess','Stage 2 - CORE 7: Patient Electronic Access Measure 1: Timely Access',1,'The number of patients in the denominator who have timely (within 4 business days after the information is available to the EP) online access to their health information to view, download, and transmit to a third party','Number of unique patients seen by the EP during the EHR reporting period','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 999','Initial Patient Population','model.USP_CMS_InitialPatientPopulation','Initial Patient Population',1,'Number of unique patients during the reporting period','Number of visits during the EHR reporting period','CMS',0)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 1055','2013 CQM #55 DM w/ Dilated Exam','USP_NQM_55_DM_with_DilatedExam','2013 Edition: Diabetic Patients w/ Dilated Exam',1,'Patient with dilated exam','Total diabetic patients seen','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 1086','2013 CQM #86 POAG w/ Optic Nerve Eval','USP_NQM_86_POAG','2013 Edition: POAG Patients w/ Optic Nerve Evaluation',1,'Patients with optic nerve evaluation','Total POAG patients seen','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 1088','2013 CQM #88 DR Severity and Macular Edema','USP_NQM_88_GetDiabetic_Retinopathy_MacularEdema','2013 Edition: DR Patients w/ Severity and Macular Edema',1,'Patients with DR severity and Macular Edema recorded','Total DR patients seen','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 1089','2013 CQM #89 DR Communication w/ Managing Physician','USP_NQM_89_DR_Communication_Physician','2013 Edition: DR Patients Communication w/ Physician Managing DM',1,'Communication to managing physician','Total DR patients seen','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 012.1','2013 CORE 11: Provide patients with electronic copy of health information','USP_CMS_GetElectronicCopyStatus','2013 Edition: Providing Electronic Copies of Health Record',1,'Patients provided with e-copy of health information','Total patient information requests','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 025','2013 MENU 5: Provide patient with timely e-access to health information','USP_CMS_GetPatientEAccess','2013 Edition: Providing Electronic Access',1,'Patients provided with electronic access','Total patients seen','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 1013','2013 CQM #13 Hypertension: Blood Pressure ','USP_NQF_13_GetBloodPressure_Hypertension','2013 Edition Hypertension: Blood Pressure',1,'Patients with blood pressure recorded','All patients aged 18 and older with hypertension seen 2 or more times','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 1421','2013 CQM #421a Adult Weight Screening and Follow-Up Population 1','USP_NQF_421_POP1_BMI','2013 Edition: Adult Weight Screening and Follow-Up Population 1',1,'Patients with BMI calculated','All patients seen','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 1421b','2013 CQM #421b Adult Weight Screening and Follow-Up Population 2','USP_NQF_421_POP2_BMI','2013 Edition: Adult Weight Screening and Follow-Up Population 2',1,'Patients with BMI calculated','All patients seen','CMS',1)
INSERT INTO CMSReports (CMSRepCode,CMSRepName,CMSQuery,CMSSubHeading,IsSP,Numerator,Denominator,ReportType,Active_YN)
	VALUES ('CPOE 1028','2013 CQM #28 Preventative Care and Screening Measure Pair','USP_NQF_28_GetTobaccoUse_CessationIntervention','2013 Edition Preventative Care and Screening Measure Pair',1,'Patients queried about tobacco use','All patients seen','CMS',1)
