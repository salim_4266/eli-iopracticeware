﻿IF OBJECT_ID('model.ClaimFileReceivers') IS NOT NULL
	EXEC dbo.DropObject 'model.ClaimFileReceivers'
GO

-- 'ClaimFileReceivers' does not exist, creating...
CREATE TABLE [model].[ClaimFileReceivers] (
    [Id] int IDENTITY(1,1) NOT NULL,
	[Name] nvarchar(max)  NOT NULL,
    [GSApplicationReceiverCode] nvarchar(max)  NULL,
    [GSApplicationSenderCode] nvarchar(max)  NULL,
    [InterchangeControlVersionCode] nvarchar(max)  NULL,
    [InterchangeReceiverCode] nvarchar(max)  NULL,
    [InterchangeReceiverCodeQualifier] nvarchar(max)  NULL,
    [InterchangeSenderCode] nvarchar(max)  NULL,
    [InterchangeSenderCodeQualifier] nvarchar(max)  NULL,
    [InterchangeUsageIndicator] nvarchar(max)  NULL,
    [Loop1000ASubmitterCode] nvarchar(max)  NULL,
    [Loop1000BRecipientCode] nvarchar(max)  NULL,
    [ComponentElementSeparator] nvarchar(max)  NULL,
    [RepetitionSeparator] nvarchar(max)  NULL,
    [WebsiteUrl] nvarchar(max)  NULL,
	[LegacyPracticeCodeTableId] int NULL
);

-- Creating primary key on [Id] in table 'ClaimFileReceivers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ClaimFileReceivers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ClaimFileReceivers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ClaimFileReceivers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ClaimFileReceivers]
ADD CONSTRAINT [PK_ClaimFileReceivers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

INSERT INTO model.ClaimFileReceivers (
	Name, GSApplicationReceiverCode, GSApplicationSenderCode, InterchangeControlVersionCode,
    InterchangeReceiverCode, InterchangeReceiverCodeQualifier, InterchangeSenderCode,
    InterchangeSenderCodeQualifier, InterchangeUsageIndicator, Loop1000ASubmitterCode,
    Loop1000BRecipientCode, ComponentElementSeparator, RepetitionSeparator, WebsiteUrl,
	LegacyPracticeCodeTableId
)
SELECT 
CASE 
	WHEN Code LIKE '%:%'
		THEN SUBSTRING(Code, 5, (dbo.GetMax((CHARINDEX(':', Code) - 6),0)))
	ELSE SUBSTRING(code, 5, LEN(code))
END AS [Name]
,CASE 
	WHEN LetterTranslation = ''
		THEN CASE 
			WHEN AlternateCode in ('', 'T')
				THEN CONVERT(NVARCHAR, NULL)
			ELSE SUBSTRING(AlternateCode, 1, (dbo.GetMax((CHARINDEX('-', AlternateCode) - 1),0)))
			END 
	ELSE SUBSTRING(LetterTranslation, 1, (dbo.GetMax((CHARINDEX('-', LetterTranslation) - 1),0)))
END AS GSApplicationReceiverCode
,CASE 
	WHEN LetterTranslation = ''
		THEN CASE 
			WHEN LEN(AlternateCode) < 4
				THEN CONVERT(NVARCHAR, NULL)
			ELSE SUBSTRING(AlternateCode, (dbo.GetMax((CHARINDEX('-', AlternateCode) + 1),0)), LEN(AlternateCode))
			END
	ELSE SUBSTRING(LetterTranslation, (dbo.GetMax((CHARINDEX('-', LetterTranslation) + 1),0)), LEN(LetterTranslation))
END AS GSApplicationSenderCode
,'00501' AS InterchangeControlVersionCode
,CASE 
	WHEN AlternateCode in ('', 'T')
		THEN CONVERT(NVARCHAR, NULL)
	ELSE SUBSTRING(AlternateCode, 1, (dbo.GetMax((CHARINDEX('-', AlternateCode) - 1),0)))
END AS InterchangeReceiverCode
,CASE 
	WHEN SUBSTRING(Code, 1, 1) IN ('C','M','V')
		THEN '30'
	WHEN SUBSTRING(Code, 1, 1) IN ('E','T')
		THEN '27'
	WHEN SUBSTRING(Code, 1, 1) IN ('B')
		THEN '33'
	WHEN SUBSTRING(Code, 1, 1) IN ('Y')
		THEN '01'
	ELSE 'ZZ'
END AS InterchangeReceiverCodeQualifier
,CASE 
	WHEN LEN(AlternateCode) < 4
		THEN CONVERT(NVARCHAR, NULL)
	ELSE SUBSTRING(AlternateCode, (dbo.GetMax((CHARINDEX('-', AlternateCode) + 1),0)), LEN(AlternateCode))
END AS InterchangeSenderCode
,CASE 
	WHEN SUBSTRING(Code, 1, 1) IN ('C','M','V')
		THEN '30'
	ELSE 'ZZ'
END AS InterchangeSenderCodeQualifier
,'P' AS InterchangeUsageIndicator
,CASE 
	WHEN LEN(OtherLtrTrans) < 4
		THEN CASE 
			WHEN AlternateCode in ('', 'T')
				THEN CONVERT(NVARCHAR, NULL)
			ELSE SUBSTRING(AlternateCode, (dbo.GetMax((CHARINDEX('-', AlternateCode) + 1),0)), LEN(AlternateCode))
			END 
	ELSE SUBSTRING(OtherLtrTrans, (dbo.GetMax((CHARINDEX('-', OtherLtrTrans) + 1),0)), LEN(OtherLtrTrans))
END AS Loop1000ASubmitterCode
,CASE 
	WHEN LEN(OtherLtrTrans) < 4
		THEN CASE 
			WHEN LEN(AlternateCode) < 4
				THEN CONVERT(NVARCHAR, NULL)
			ELSE SUBSTRING(AlternateCode, 1, (dbo.GetMax((CHARINDEX('-', AlternateCode) - 1),0)))
			END 
	ELSE SUBSTRING(OtherLtrTrans, 1, (dbo.GetMax((CHARINDEX('-', OtherLtrTrans) - 1),0)))
END AS Loop1000BRecipientCode
,':' AS ComponentElementSeparator
,'^' AS RepetitionSeparator
,WebsiteUrl AS WebsiteUrl
,Id AS LegacyPracticeCodeTableId
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'TRANSMITTYPE'
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.Insurers'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE i
SET i.ClaimFileReceiverId = cr.Id
FROM model.Insurers i
JOIN model.ClaimFileReceivers cr ON i.ClaimFileReceiverId = cr.LegacyPracticeCodeTableId
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [ClaimFileReceiverId] in table 'Insurers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ClaimFileReceiverInsurer'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Insurers] DROP CONSTRAINT FK_ClaimFileReceiverInsurer

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClaimFileReceivers]'), 'IsUserTable') = 1
ALTER TABLE [model].[Insurers]
ADD CONSTRAINT [FK_ClaimFileReceiverInsurer]
    FOREIGN KEY ([ClaimFileReceiverId])
    REFERENCES [model].[ClaimFileReceivers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClaimFileReceiverInsurer'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ClaimFileReceiverInsurer')
	DROP INDEX IX_FK_ClaimFileReceiverInsurer ON [model].[Insurers]
GO
CREATE INDEX [IX_FK_ClaimFileReceiverInsurer]
ON [model].[Insurers]
    ([ClaimFileReceiverId]);
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.BillingServiceTransactions'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE b
SET b.ClaimFileReceiverId = cr.Id
FROM model.BillingServiceTransactions b
JOIN model.ClaimFileReceivers cr ON b.ClaimFileReceiverId = cr.LegacyPracticeCodeTableId
GO

UPDATE model.BillingServiceTransactions 
SET ClaimFileReceiverId = NULL 
WHERE ClaimFileReceiverId NOT IN 
	(SELECT Id FROM model.ClaimFileReceivers)
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [ClaimFileReceiverId] in table 'BillingServiceTransactions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ClaimFileReceiverBillingServiceTransaction'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingServiceTransactions] DROP CONSTRAINT FK_ClaimFileReceiverBillingServiceTransaction

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClaimFileReceivers]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingServiceTransactions]
ADD CONSTRAINT [FK_ClaimFileReceiverBillingServiceTransaction]
    FOREIGN KEY ([ClaimFileReceiverId])
    REFERENCES [model].[ClaimFileReceivers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClaimFileReceiverBillingServiceTransaction'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ClaimFileReceiverBillingServiceTransaction')
	DROP INDEX IX_FK_ClaimFileReceiverBillingServiceTransaction ON [model].[BillingServiceTransactions]
GO
CREATE INDEX [IX_FK_ClaimFileReceiverBillingServiceTransaction]
ON [model].[BillingServiceTransactions]
    ([ClaimFileReceiverId]);
GO