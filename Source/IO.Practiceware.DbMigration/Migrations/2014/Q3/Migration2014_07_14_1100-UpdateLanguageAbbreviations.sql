﻿IF EXISTS(SELECT * FROM model.Languages WHERE Abbreviation LIKE '%(B)%')
UPDATE model.Languages
SET Abbreviation = SUBSTRING(Abbreviation, 1, 3) 
WHERE Abbreviation LIKE '%(B)%'