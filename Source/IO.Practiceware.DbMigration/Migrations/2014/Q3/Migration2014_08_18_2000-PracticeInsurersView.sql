﻿SELECT 
*
INTO dbo.PracticeInsurersBackup
FROM dbo.PracticeInsurers
GO

EXEC dbo.DropObject 'dbo.PracticeInsurers'
GO

IF OBJECT_ID('dbo.PracticeInsurers','V') IS NOT NULL
	DROP VIEW dbo.PracticeInsurers
GO

CREATE VIEW dbo.PracticeInsurers
WITH VIEW_METADATA
AS
SELECT i.Id AS InsurerId
,CONVERT(NVARCHAR(32),CASE WHEN i.Name <> '' AND i.Name IS NOT NULL THEN i.Name ELSE '' END) AS InsurerName
,CONVERT(NVARCHAR(50),CASE WHEN ia.Line1 <> '' AND ia.Line1 IS NOT NULL THEN ia.Line1 ELSE '' END) AS InsurerAddress
,CONVERT(NVARCHAR(32),CASE WHEN ia.City <> '' AND ia.City IS NOT NULL THEN ia.City ELSE '' END) AS InsurerCity
,CONVERT(NVARCHAR(2),CASE WHEN s.Abbreviation <> '' AND s.Abbreviation IS NOT NULL THEN s.Abbreviation ELSE '' END) AS InsurerState
,CONVERT(NVARCHAR(16),CASE WHEN ia.PostalCode  <> '' AND ia.PostalCode  IS NOT NULL THEN ia.PostalCode  ELSE '' END) AS InsurerZip
,CONVERT(NVARCHAR(16),CONVERT(NVARCHAR(MAX),ip.AreaCode) + CONVERT(NVARCHAR(MAX),ip.ExchangeAndSuffix)) AS InsurerPhone
,'' AS InsurerPlanId
,CONVERT(NVARCHAR(16),CASE WHEN i.PlanName <> '' AND i.PlanName IS NOT NULL THEN i.PlanName ELSE '' END)AS InsurerPlanName
,CONVERT(NVARCHAR(16),CASE WHEN pt.Name <> '' AND pt.Name IS NOT NULL THEN pt.Name ELSE '' END) AS InsurerPlanType
,'' AS InsurerGroupId
,'' AS InsurerGroupName
,CASE i.ClaimFilingIndicatorCodeId
	WHEN 10
		THEN 'P'
	WHEN 1 
		THEN 'K'
	WHEN 12
		THEN 'L' 
	WHEN 14
		THEN 'F'
	WHEN 7 
		THEN 'I'
	WHEN 19
		THEN 'C'
	WHEN 18
		THEN 'N' 
	WHEN 24
		THEN 'Z'
	WHEN 20 
		THEN 'Y'
	WHEN 21
		THEN 'T'
	WHEN 11 
		THEN 'H'
	WHEN 22
		THEN 'V'
	WHEN 23
		THEN 'W'
	ELSE ''
END AS InsurerReferenceCode
,CASE i.IsReferralRequired WHEN 1 THEN 'Y' ELSE 'N' END  AS ReferralRequired
,CONVERT(REAL,0) AS Copay
,CONVERT(REAL,0) AS OutOfPocket
,CASE i.IsSecondaryClaimPaper WHEN 1 THEN 'A' ELSE '-' END AS [Availability]
,'' AS InsurerFeeSchedule
,'' AS InsurerTosTbl
,'' AS InsurerPosTbl
,CONVERT(NVARCHAR(32),CASE WHEN i.PayerCode <> '' AND i.PayerCode IS NOT NULL THEN i.PayerCode ELSE '' END) AS NEICNumber
,'' AS InsurerENumber
,'H' AS InsurerEFormat
,SUBSTRING(pctClaimReceiver.Code, 1, 1) AS InsurerTFormat
,CASE WHEN ipAuthorization.Id IS NOT NULL 
	THEN CONVERT(NVARCHAR(MAX),ipAuthorization.AreaCode) + CONVERT(NVARCHAR(MAX),ipAuthorization.ExchangeAndSuffix) 
END AS InsurerPrecPhone
,CASE WHEN ipEligibility.Id IS NOT NULL 
	THEN CONVERT(NVARCHAR(MAX),ipEligibility.AreaCode) + CONVERT(NVARCHAR(MAX),ipEligibility.ExchangeAndSuffix) 
END AS InsurerEligPhone
,CASE WHEN ipProvider.Id IS NOT NULL 
	THEN CONVERT(NVARCHAR(MAX),ipProvider.AreaCode) + CONVERT(NVARCHAR(MAX),ipProvider.ExchangeAndSuffix) 
END AS InsurerProvPhone
,CASE WHEN ipClaims.Id IS NOT NULL 
	THEN CONVERT(NVARCHAR(MAX),ipClaims.AreaCode) + CONVERT(NVARCHAR(MAX),ipClaims.ExchangeAndSuffix) 
END AS InsurerClaimPhone
,CASE i.InsurerPayTypeId
	WHEN 1 THEN 'SP'
	WHEN 2 THEN 'OT'
	WHEN 3 THEN 'LT'
	WHEN 4 THEN 'IP'
	WHEN 5 THEN 'GP'
	WHEN 6 THEN 'PP'
	WHEN 7 THEN 'MG'
	WHEN 8 THEN 'LD'
	WHEN 9 THEN 'AP'
END AS InsurerPayType
,'' AS InsurerAdjustmentDefault
,'N' AS PrecertRequired
,i.MedigapCode AS MedicareCrossOverId
,i.Comment AS InsurerComment
,i.PolicyNumberFormat AS InsurerPlanFormat
,'' AS InsurerClaimMap
,CASE i.InsurerBusinessClassId
	WHEN 1
		THEN 'AMERIH'
	WHEN 2
		THEN 'BLUES'
	WHEN 3
		THEN 'COMM'
	WHEN 4
		THEN 'MCAIDFL'
	WHEN 5
		THEN 'MCAIDNC'
	WHEN 6
		THEN 'MCAIDNJ'
	WHEN 7
		THEN 'MCAIDNV'
	WHEN 8
		THEN 'MCAIDNY'
	WHEN 9
		THEN 'MCARENJ'
	WHEN 10
		THEN 'MCARENV'
	WHEN 11
		THEN 'MCARENY'
	WHEN 12
		THEN 'TEMPLATE'
END AS InsurerBusinessClass
,'Y' AS OCode
,'' AS InsurerCPTClass
,CASE SendZeroCharge WHEN 1 THEN 'Y' ELSE 'N' END AS InsurerServiceFilter
,st.Abbreviation AS StateJurisdiction
,CASE WHEN ISNUMERIC(i.AllowDependents) = 1 THEN CONVERT(BIT,i.AllowDependents) ELSE CONVERT(BIT,0) END AS AllowDependents
,i.IsMedicareAdvantage AS IsMedicareAdvantage
,NULL AS Hpid
,NULL AS Oeid
,NULL AS Website
,CASE WHEN ISNUMERIC(i.IsArchived) = 1 THEN CONVERT(BIT,i.IsArchived) ELSE CONVERT(BIT,0) END AS IsArchived
,'Y' AS InsurerCrossOver
FROM model.Insurers i
LEFT JOIN (
	SELECT * 
	FROM model.InsurerAddresses 
	WHERE Id IN (
		SELECT MAX(a.Id) 
		FROM model.InsurerAddresses a
		JOIN model.InsurerAddressTypes at ON a.InsurerAddressTypeId = at.Id
		WHERE at.Name LIKE '%Claim%'
		GROUP BY InsurerId
	)) ia ON ia.InsurerId = i.Id
LEFT JOIN model.StateOrProvinces s ON s.Id = ia.StateOrProvinceId
LEFT JOIN (
	SELECT 
	* 
	FROM model.InsurerPhoneNumbers 
	WHERE Id IN (
		SELECT 
		MAX(ip.Id) 
		FROM model.InsurerPhoneNumbers ip 
		WHERE ip.InsurerPhoneNumberTypeId = (SELECT TOP 1 Id FROM model.InsurerPhoneNumberTypes WHERE Name = 'Main')
		GROUP BY ip.InsurerId
	))  ip ON ip.InsurerId = i.Id
LEFT JOIN model.InsurerPlanTypes pt ON pt.Id = i.InsurerPlanTypeId
LEFT JOIN model.ClaimFileReceivers cfr ON cfr.Id = i.ClaimFileReceiverId
LEFT JOIN dbo.PracticeCodeTable pctClaimReceiver ON cfr.LegacyPracticeCodeTableId = pctClaimReceiver.Id
	AND pctClaimReceiver.ReferenceType = 'TRANSMITTYPE'
LEFT JOIN (
	SELECT 
	* 
	FROM model.InsurerPhoneNumbers 
	WHERE Id IN (
		SELECT 
		MAX(ip.Id) 
		FROM model.InsurerPhoneNumbers ip 
		WHERE ip.InsurerPhoneNumberTypeId = (SELECT TOP 1 Id FROM model.InsurerPhoneNumberTypes WHERE Name = 'Authorization')
		GROUP BY ip.InsurerId
	)) ipAuthorization ON ipAuthorization.InsurerId = i.Id
LEFT JOIN (
	SELECT 
	* 
	FROM model.InsurerPhoneNumbers 
	WHERE Id IN (
		SELECT 
		MAX(ip.Id) 
		FROM model.InsurerPhoneNumbers ip 
		WHERE ip.InsurerPhoneNumberTypeId = (SELECT TOP 1 Id FROM model.InsurerPhoneNumberTypes WHERE Name = 'Eligibility')
		GROUP BY ip.InsurerId
	))  ipEligibility ON ipEligibility.InsurerId = i.Id
LEFT JOIN (
	SELECT 
	* 
	FROM model.InsurerPhoneNumbers 
	WHERE Id IN (
		SELECT 
		MAX(ip.Id) 
		FROM model.InsurerPhoneNumbers ip 
		WHERE ip.InsurerPhoneNumberTypeId = (SELECT TOP 1 Id FROM model.InsurerPhoneNumberTypes WHERE Name = 'Provider')
		GROUP BY ip.InsurerId
	))  ipProvider ON ipProvider.InsurerId = i.Id
LEFT JOIN (
	SELECT 
	* 
	FROM model.InsurerPhoneNumbers 
	WHERE Id IN (
		SELECT 
		MAX(ip.Id) 
		FROM model.InsurerPhoneNumbers ip 
		WHERE ip.InsurerPhoneNumberTypeId = (SELECT TOP 1 Id FROM model.InsurerPhoneNumberTypes WHERE Name = 'Claims')
		GROUP BY ip.InsurerId
	))  ipClaims ON ipClaims.InsurerId = i.Id
LEFT JOIN model.StateOrProvinces st ON st.Id = i.JurisdictionStateorProvinceId

GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PatientReceivableServicesInsert' AND parent_id = OBJECT_ID('dbo.PatientReceivableServices','V'))
	DROP TRIGGER PatientReceivableServicesInsert
GO
	
CREATE TRIGGER PatientReceivableServicesInsert ON dbo.PatientReceivableServices
INSTEAD OF INSERT
AS
BEGIN
SET NOCOUNT ON;
DECLARE @Invoice NVARCHAR(32)
DECLARE @Service NVARCHAR(16)
DECLARE @Modifier NVARCHAR(10)
DECLARE @Quantity REAL
DECLARE @Charge REAL
DECLARE @TypeOfService NVARCHAR(32)
DECLARE @PlaceOfService NVARCHAR(32)
DECLARE @ServiceDate NVARCHAR(10)
DECLARE @LinkedDiag NVARCHAR(4)
DECLARE @Status NVARCHAR(1)
DECLARE @OrderDoc NVARCHAR(1)
DECLARE @ConsultOn NVARCHAR(1)
DECLARE @FeeCharge REAL
DECLARE @ItemOrder INT
DECLARE @ECode INT
DECLARE @PostDate NVARCHAR(8) 
DECLARE @ExternalRefInfo NVARCHAR(16)
DECLARE @QuantityDecimal DECIMAL(12, 2) 
DECLARE @ChargeDecimal DECIMAL(12, 2) 
DECLARE @FeeChargeDecimal DECIMAL(12, 2) 

DECLARE PatientReceivableServicesInsertCursor CURSOR FOR
SELECT 
Invoice 
,[Service]
,Modifier
,Quantity
,Charge
,TypeOfService
,PlaceOfService
,ServiceDate
,LinkedDiag
,[Status]
,OrderDoc
,ConsultOn
,FeeCharge
,ItemOrder
,ECode
,PostDate
,ExternalRefInfo
,QuantityDecimal
,ChargeDecimal
,FeeChargeDecimal 
FROM inserted

OPEN PatientReceivableServicesInsertCursor FETCH NEXT FROM PatientReceivableServicesInsertCursor INTO
	@Invoice, @Service, @Modifier, @Quantity, @Charge, @TypeOfService, @PlaceOfService, @ServiceDate,
	@LinkedDiag, @Status, @OrderDoc, @ConsultOn, @FeeCharge, @ItemOrder, @ECode, @PostDate, @ExternalRefInfo,
	@QuantityDecimal, @ChargeDecimal, @FeeChargeDecimal
WHILE @@FETCH_STATUS = 0 
BEGIN
	DECLARE @EncounterServiceId INT
	SET @EncounterServiceId = (SELECT TOP 1 Id FROM model.EncounterServices WHERE Code = @Service)
	
	IF (@EncounterServiceId IS NULL) 
	BEGIN
		DECLARE @EncounterServiceTypeIdForMissingServiceId INT
		SET @EncounterServiceTypeIdForMissingServiceId = (
		SELECT TOP 1 Id FROM model.EncounterServiceTypes WHERE (Name LIKE 'Misc%' OR Name LIKE 'NEEDSTYPE') ORDER BY Name ASC)

		INSERT INTO model.EncounterServices (
			Code
			,[Description]
			,DefaultUnit
			,UnitFee
			,StartDateTime
			,EndDateTime
			,IsOrderingProvider
			,IsConsultation
			,IsFacilityInvoice
			,IsDoctorInvoice
			,IsZeroCharge
			,RelativeValueUnit
			,IsArchived
			,IsUnclassified
			,EncounterServiceTypeId
			,RevenueCodeId
			,ServiceUnitOfMeasurementId
			,IsCliaWaived
			,LegacyPracticeServicesCodeId
		)
		SELECT
		@Service AS Code
		,'UNKNOWN' AS [Description]
		,CONVERT(DECIMAL(18,2), 1) AS DefaultUnit
		,1000.00 AS UnitFee
		,CONVERT(DATETIME,'20000101') AS StartDateTime
		,NULL AS EndDateTime
		,CONVERT(BIT, 0) AS IsOrderingProvider
		,CONVERT(BIT, 0) AS IsConsultation
		,CONVERT(BIT, 0) AS IsFacilityInvoice
		,CONVERT(BIT, 1) AS IsDoctorInvoice
		,CONVERT(BIT, 0) AS IsZeroCharge
		,CONVERT(DECIMAL(18,2), NULL) AS RelativeValueUnit
		,CONVERT(BIT, 0) AS IsArchived
		,CONVERT(BIT, 0) AS IsUnclassified
		,@EncounterServiceTypeIdForMissingServiceId AS EncounterServiceTypeId
		,NULL AS RevenueCodeId
		,2 AS ServiceUnitOfMeasurementId
		,CONVERT(BIT,0) AS IsCliaWaived
		,NULL AS LegacyPracticeServicesCodeId

		SET @EncounterServiceId = SCOPE_IDENTITY()
	END

	DECLARE @InvoiceId INT
	SET @InvoiceId = (SELECT TOP 1 Id FROM model.Invoices WHERE LegacyPatientReceivablesInvoice = @Invoice)

	IF (@InvoiceId IS NULL) 
	BEGIN
		RAISERROR('Unable to locate a valid InvoiceId.',16,1)
		PRINT '@Invoice = ' + @Invoice
		PRINT '@ServiceDate = ' + @ServiceDate 
		PRINT '@Service = ' + @Service
		PRINT '@Status = ' + @Status
	END

	IF ((SELECT 
	TOP 1 PlaceOfServiceCode 
	FROM model.ServiceLocationCodes
	WHERE Id IN (SELECT TOP 1 ServiceLocationCodeId FROM model.Invoices WHERE Id = @InvoiceId)) <> @PlaceOfService)
	BEGIN
		UPDATE inv
		SET inv.ServiceLocationCodeId = (SELECT TOP 1 Id FROM model.ServiceLocationCodes WHERE PlaceOfServiceCode = @PlaceOfService)
		FROM model.Invoices inv
		WHERE inv.Id = @InvoiceId
	END

	DECLARE @BillingServices TABLE (Id INT NOT NULL)

	INSERT INTO model.BillingServices (
      Unit
      ,UnitCharge
      ,OrdinalId
      ,EncounterServiceId
      ,InvoiceId
      ,ProcedureCode835Description
      ,OrderingExternalProviderId
      ,UnitAllowableExpense
      ,LegacyOrderingProviderId
      ,LegacyPatientReceivableServicesItemId
      ,LegacyPatientReceivableServicesLinkedDiag
	)
	OUTPUT inserted.Id INTO @BillingServices (Id)
	SELECT 
	CONVERT(DECIMAL(18,2),@Quantity) AS Unit
	,CONVERT(DECIMAL(18,2),@Charge) AS Charge
	,CASE 
		WHEN @ItemOrder > 0
			THEN @ItemOrder
		ELSE 1
	END AS OrdinalId
	,@EncounterServiceId AS EncounterServiceId
	,@InvoiceId AS InvoiceId
	,CONVERT(NVARCHAR(MAX),NULL) AS ProcedureCode835Description
	,NULL AS OrderingExternalProviderId
	,CONVERT(DECIMAL(18,2),@FeeCharge) AS UnitAllowableExpense
	,NULL AS LegacyOrderingProviderId
	,NULL AS LegacyPatientReceivableServicesItemId
	,@LinkedDiag AS LegacyPatientReceivableServicesLinkedDiag
	
	;WITH CTE AS (
	SELECT 
	1 AS StartingIndex
	,2 AS LenghtIndex

	UNION ALL 

	SELECT StartingIndex + 2 AS StartingIndex
	,2 AS LenghtIndex
	FROM CTE
	WHERE StartingIndex < 7
	)
	INSERT INTO model.BillingServiceModifiers (
		OrdinalId
		,BillingServiceId
		,ServiceModifierId
	)
	SELECT 
	ROW_NUMBER() OVER (PARTITION BY b.Id ORDER BY b.Id, CTE.StartingIndex) AS OrdinalId
	,b.Id AS BillingServiceId
	,s.Id AS ServiceModifierId
	FROM CTE
	JOIN model.ServiceModifiers s ON s.Code = SUBSTRING(@Modifier,CTE.StartingIndex,CTE.LenghtIndex)
	FULL OUTER JOIN @BillingServices b ON b.Id = b.Id
	WHERE SUBSTRING(@Modifier,CTE.StartingIndex,CTE.LenghtIndex) <> ''
		AND SUBSTRING(@Modifier,CTE.StartingIndex,CTE.LenghtIndex) IS NOT NULL

	IF OBJECT_ID('dbo.ViewIdentities','U') IS NULL
	BEGIN	
		CREATE TABLE [dbo].[ViewIdentities](
		[Name] [nvarchar](max) NULL,
		[Value] [bigint] NULL
		)
	END

	IF OBJECT_ID('tempdb..#TempTableToHoldTheScopeIdentity') IS NOT NULL
		DROP TABLE #TempTableToHoldTheScopeIdentity

	CREATE TABLE #TempTableToHoldTheScopeIdentity (Id BIGINT IDENTITY(1,1) NOT NULL)
	SET IDENTITY_INSERT #TempTableToHoldTheScopeIdentity ON
	INSERT INTO #TempTableToHoldTheScopeIdentity (Id) SELECT Id AS SCOPE_ID_COLUMN FROM @BillingServices
	SET IDENTITY_INSERT #TempTableToHoldTheScopeIdentity OFF

	IF NOT EXISTS(SELECT * FROM ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
		INSERT ViewIdentities(Name, Value) VALUES ('dbo.PatientReceivableServices', @@IDENTITY)
	ELSE
		UPDATE ViewIdentities SET Value = @@IDENTITY WHERE Name = 'dbo.PatientReceivableServices'

	SELECT @@IDENTITY AS SCOPE_ID_COLUMN

	FETCH NEXT FROM PatientReceivableServicesInsertCursor INTO 
	@Invoice, @Service, @Modifier, @Quantity, @Charge, @TypeOfService, @PlaceOfService, @ServiceDate,
	@LinkedDiag, @Status, @OrderDoc, @ConsultOn, @FeeCharge, @ItemOrder, @ECode, @PostDate, @ExternalRefInfo,
	@QuantityDecimal, @ChargeDecimal, @FeeChargeDecimal

	CLOSE PatientReceivableServicesInsertCursor
	DEALLOCATE PatientReceivableServicesInsertCursor
END
END
GO
