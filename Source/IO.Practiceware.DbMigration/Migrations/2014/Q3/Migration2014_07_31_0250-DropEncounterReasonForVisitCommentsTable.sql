﻿-- Dropping object model.EncounterReasonForVisitComments as UserTable because it is a View
IF OBJECT_ID('model.EncounterReasonForVisitComments','U') IS NOT NULL
	EXEC dbo.DropObject 'model.EncounterReasonForVisitComments'
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'model.EncounterReasonForVisitComments') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC sp_executesql N'
	CREATE VIEW [model].[EncounterReasonForVisitComments]
	
	AS
	SELECT pn.NoteId AS Id
		,pn.AppointmentId AS EncounterId
		,pn.Note1 AS Value
		,NULL AS LateralityId
		,NULL AS BodyPartId
		,ap.ResourceId1 AS UserId
	FROM dbo.PatientNotes pn
	INNER JOIN dbo.Appointments ap ON pn.AppointmentId = ap.AppointmentId
	WHERE pn.NoteSystem = ''10''
		AND pn.NoteType = ''C'''
	
END
