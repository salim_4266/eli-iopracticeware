﻿--Fix dbo.Drug.DisplayName because FDB keeps changing fdb.tblCompositeDrug.med_routed_df_med_id_desc

IF OBJECT_ID('tempdb..#Mismatch') IS NOT NULL
	DROP TABLE #Mismatch
GO

DELETE FROM dbo.PracticeFavorites WHERE System = '-' AND ISNUMERIC(Diagnosis) = 0

SELECT d.* 
INTO #Mismatch
FROM dbo.Drug d
LEFT JOIN fdb.tblCompositeDrug f ON d.displayname = f.med_routed_df_med_id_desc
WHERE DrugCode <> 'F'
	AND f.MEDID is null

DELETE d
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
LEFT JOIN dbo.PracticeFavorites pf ON pf.Diagnosis = m.DrugId
	AND pf.System = '-'
WHERE pf.Id IS NULL

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc, d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON d.DisplayName + 'let' = f.med_routed_df_med_id_desc
WHERE RIGHT(d.DisplayName, 3) = 'tab'
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc, d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON d.DisplayName + 'let,extended release' = f.med_routed_df_med_id_desc
WHERE RIGHT(d.DisplayName, 3) = 'tab'
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc, d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON d.DisplayName + 'let,film-coated' = f.med_routed_df_med_id_desc
WHERE RIGHT(d.DisplayName, 3) = 'tab'
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc , d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON REPLACE(d.DisplayName, 'tab', 'monohydrate tablet') = f.med_routed_df_med_id_desc
WHERE SUBSTRING(f.med_routed_df_med_id_desc, 1, 11) = 'doxycycline'
	and SUBSTRING(d.displayname, 1, 11) = 'doxycycline'
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc, d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON d.DisplayName + 'sule' = f.med_routed_df_med_id_desc
WHERE RIGHT(d.DisplayName, 3) = 'cap'
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc, d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON d.DisplayName + 'ules' = f.med_routed_df_med_id_desc
WHERE RIGHT(d.DisplayName, 4) = 'caps'
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc, d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON d.DisplayName + 'sule,extended release' = f.med_routed_df_med_id_desc
WHERE RIGHT(d.DisplayName, 3) = 'Cap'
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc, d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON substring(m.DisplayName, 1, 15) = substring(f.med_routed_df_med_id_desc, 1, 15)
	AND RIGHT(m.DisplayName, 11) = RIGHT(f.med_routed_df_med_id_desc, 11)
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc, d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON d.DisplayName + ',suspension' = f.med_routed_df_med_id_desc
WHERE RIGHT(f.med_routed_df_med_id_desc, 10) = 'suspension' and RIGHT(d.displayname, 5) = 'drops'
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc, d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON d.DisplayName + 'ension' = f.med_routed_df_med_id_desc
WHERE RIGHT(f.med_routed_df_med_id_desc, 10) = 'suspension' and RIGHT(d.displayname, 4) = 'susp'
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc, d.DrugCode = 'T' 
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON REPLACE(d.DisplayName, ', Susp', ',suspension') = f.med_routed_df_med_id_desc
WHERE RIGHT(f.med_routed_df_med_id_desc, 10) = 'suspension' and RIGHT(d.displayname, 4) = 'susp'
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc , d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON REPLACE(d.DisplayName, 'Soln', 'solution') = f.med_routed_df_med_id_desc
WHERE RIGHT(f.med_routed_df_med_id_desc, 8) = 'solution' and RIGHT(d.displayname, 4) = 'soln'
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc , d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON REPLACE(d.DisplayName, 'oral', 'tablet') = f.med_routed_df_med_id_desc
WHERE f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc , d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON REPLACE(d.DisplayName, 'oral', 'capsule,extended release') = f.med_routed_df_med_id_desc
WHERE SUBSTRING(f.med_routed_df_med_id_desc, 1, 15) = SUBSTRING(d.displayname, 1, 15)
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc, d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON REPLACE(d.DisplayName, 'oral', 'oral suspension') = f.med_routed_df_med_id_desc
WHERE SUBSTRING(f.med_routed_df_med_id_desc, 1, 15) = SUBSTRING(d.displayname, 1, 15)
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc , d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON REPLACE(d.DisplayName, 'opht', 'eye drops,suspension') = f.med_routed_df_med_id_desc
WHERE SUBSTRING(f.med_routed_df_med_id_desc, 1, 7) = SUBSTRING(d.displayname, 1, 7)
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc , DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON REPLACE(d.DisplayName, 'oint', 'ointment') = f.med_routed_df_med_id_desc
WHERE SUBSTRING(f.med_routed_df_med_id_desc, 1, 7) = SUBSTRING(d.displayname, 1, 7)
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc , d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON REPLACE(d.DisplayName, 'ointment', 'topical ointment') = f.med_routed_df_med_id_desc
WHERE SUBSTRING(f.med_routed_df_med_id_desc, 1, 7) = SUBSTRING(d.displayname, 1, 7)
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc , d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON REPLACE(d.DisplayName, 'Optive', 'Refresh Optive') = f.med_routed_df_med_id_desc
	AND f.Status = 'A'

UPDATE d SET d.DisplayName = f.med_routed_df_med_id_desc , d.DrugCode = 'T'
FROM dbo.Drug d
INNER JOIN #Mismatch m ON m.DrugId = d.DrugId
INNER JOIN fdb.tblCompositeDrug f ON REPLACE(REPLACE(d.DisplayName, 'Optive', 'Refresh Optive'),'Dropperette', 'drops in a dropperette') = f.med_routed_df_med_id_desc
	AND f.Status = 'A'


IF OBJECT_ID('tempdb..#Mismatch') IS NOT NULL
	DROP TABLE #Mismatch
GO