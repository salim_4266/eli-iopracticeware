﻿IF OBJECT_ID ( 'dbo.ApplGetServiceBalance', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.ApplGetServiceBalance;
GO

CREATE PROCEDURE dbo.ApplGetServiceBalance
	@Invoice NVARCHAR(100)
	,@PaymentService NVARCHAR(10)
	,@PaymentServiceId NVARCHAR(100)
AS
SET NOCOUNT ON;

DECLARE @AppointmentId INT
SET @AppointmentId = SUBSTRING(@Invoice, (CHARINDEX('-', @Invoice) + 1), LEN(@Invoice))

IF OBJECT_ID('tempdb..#InvoiceReceivableIds') IS NOT NULL
BEGIN
	DROP TABLE #InvoiceReceivableIds
END
SELECT Id INTO #InvoiceReceivableIds FROM dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId)

IF (@@ROWCOUNT > 0)
BEGIN

SELECT 
v.Id AS Id
,v.PaymentType
,v.IsDebit AS IsDebit
,v.ShortName AS ShortName
INTO #AdjustmentTypes
FROM (
	SELECT 
		CASE
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'P'
				THEN 1
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'X'
				THEN 2
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '8'
				THEN 3
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'R'
				THEN 4
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'U'
				THEN 5
			ELSE pct.Id + 1000
		END AS Id
		,SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS PaymentType
		,CASE pct.AlternateCode
			WHEN 'D'
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
		END AS IsDebit
		,LetterTranslation AS ShortName
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'
	GROUP BY 
	Id
	,Code
	,pct.AlternateCode
	,LetterTranslation
) AS v

CREATE UNIQUE CLUSTERED INDEX IX_AdjustmentTypes_Id ON #AdjustmentTypes (Id)

SELECT 
v.Id AS Id
,v.FinancialInformationType
,v.ShortName AS ShortName
,v.PaymentFinancialType
INTO #FinancialInformationTypes
FROM (
	SELECT 
	CASE
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '!'
			THEN 1
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '|'
			THEN 2
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '?'
			THEN 3
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '%'
			THEN 4
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'D'
			THEN 5
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '&'
			THEN 6
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = ']'
			THEN 7
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '0'
			THEN 8
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'V'
			THEN 9
		ELSE pct.Id + 1000
	END AS Id
	,SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS FinancialInformationType
	,AlternateCode AS PaymentFinancialType
	,LetterTranslation AS ShortName
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'		
		AND (pct.AlternateCode = '' OR pct.AlternateCode IS NULL)
	GROUP BY 
	pct.Id
	,pct.Code
	,LetterTranslation
	,AlternateCode
) AS v

CREATE UNIQUE CLUSTERED INDEX IX_FinancialInformationTypes_Id ON #FinancialInformationTypes (Id)

SELECT * FROM (
SELECT
i.LegacyPatientReceivablesInvoice AS Invoice
,at.PaymentType
,adj.Amount AS PaymentAmount
,es.Code AS PaymentService
,CASE 
	WHEN at.IsDebit = 1
		THEN 'D'
	WHEN at.IsDebit = 0
		THEN 'C'
	ELSE ''
END AS PaymentFinancialType
FROM model.InvoiceReceivables ir
INNER JOIN model.Adjustments adj ON adj.InvoiceReceivableId = ir.Id
LEFT JOIN #AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
INNER JOIN #InvoiceReceivableIds irs ON irs.Id = ir.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.BillingServices bs ON bs.Id = adj.BillingServiceId
	AND bs.Id = @PaymentServiceId
INNER JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
	AND es.Code = @PaymentService

UNION ALL

SELECT
i.LegacyPatientReceivablesInvoice AS Invoice
,fit.FinancialInformationType
,fi.Amount AS PaymentAmount
,es.Code AS PaymentService
,'' AS PaymentFinancialType
FROM model.InvoiceReceivables ir
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.FinancialInformations fi ON fi.InvoiceReceivableId = ir.Id
LEFT JOIN #FinancialInformationTypes fit ON fit.Id = fi.FinancialInformationTypeId
INNER JOIN #InvoiceReceivableIds irs ON irs.Id = ir.Id
INNER JOIN model.BillingServices bs ON bs.Id = fi.BillingServiceId
	AND bs.Id = @PaymentServiceId
INNER JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
	AND es.Code = @PaymentService
) v
ORDER BY Invoice
END
GO