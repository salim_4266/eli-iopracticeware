﻿--Updating StartDateTime and EnteredDateTime to correct date of appDate in case of appDate = -1 or more less
UPDATE pdd SET pdd.StartDateTime = CONVERT(DATETIME, ap.AppDate, 112),
pdd.EnteredDateTime = CONVERT(DATETIME, ap.AppDate, 112) FROM model.PatientDiagnosisDetails pdd 
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pdd.EncounterId 
WHERE ap.AppTime < 0