﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ClaimFileReceiversInsert' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER ClaimFileReceiversInsert
GO

CREATE TRIGGER [dbo].[ClaimFileReceiversInsert] ON dbo.PracticeCodeTable
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'TRANSMITTYPE'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
	INSERT INTO model.ClaimFileReceivers (
		Name, GSApplicationReceiverCode, GSApplicationSenderCode, InterchangeControlVersionCode,
		InterchangeReceiverCode, InterchangeReceiverCodeQualifier, InterchangeSenderCode,
		InterchangeSenderCodeQualifier, InterchangeUsageIndicator, Loop1000ASubmitterCode,
		Loop1000BRecipientCode, ComponentElementSeparator, RepetitionSeparator, WebsiteUrl,
		LegacyPracticeCodeTableId
	)
	SELECT 
	CASE 
		WHEN Code LIKE '%:%'
			THEN SUBSTRING(Code, 5, (dbo.GetMax((CHARINDEX(':', Code) - 6),0)))
		ELSE SUBSTRING(code, 5, LEN(code))
	END AS [Name]
	,CASE 
		WHEN LetterTranslation = ''
			THEN CASE 
				WHEN AlternateCode in ('', 'T')
					THEN CONVERT(NVARCHAR, NULL)
				ELSE SUBSTRING(AlternateCode, 1, (dbo.GetMax((CHARINDEX('-', AlternateCode) - 1),0)))
				END 
		ELSE SUBSTRING(LetterTranslation, 1, (dbo.GetMax((CHARINDEX('-', LetterTranslation) - 1),0)))
	END AS GSApplicationReceiverCode
	,CASE 
		WHEN LetterTranslation = ''
			THEN CASE 
				WHEN LEN(AlternateCode) < 4
					THEN CONVERT(NVARCHAR, NULL)
				ELSE SUBSTRING(AlternateCode, (dbo.GetMax((CHARINDEX('-', AlternateCode) + 1),0)), LEN(AlternateCode))
				END
		ELSE SUBSTRING(LetterTranslation, (dbo.GetMax((CHARINDEX('-', LetterTranslation) + 1),0)), LEN(LetterTranslation))
	END AS GSApplicationSenderCode
	,'00501' AS InterchangeControlVersionCode
	,CASE 
		WHEN AlternateCode in ('', 'T')
			THEN CONVERT(NVARCHAR, NULL)
		ELSE SUBSTRING(AlternateCode, 1, (dbo.GetMax((CHARINDEX('-', AlternateCode) - 1),0)))
	END AS InterchangeReceiverCode
	,CASE 
		WHEN SUBSTRING(Code, 1, 1) IN ('C','M','V')
			THEN '30'
		WHEN SUBSTRING(Code, 1, 1) IN ('E','T')
			THEN '27'
		WHEN SUBSTRING(Code, 1, 1) IN ('B')
			THEN '33'
		WHEN SUBSTRING(Code, 1, 1) IN ('Y')
			THEN '01'
		ELSE 'ZZ'
	END AS InterchangeReceiverCodeQualifier
	,CASE 
		WHEN LEN(AlternateCode) < 4
			THEN CONVERT(NVARCHAR, NULL)
		ELSE SUBSTRING(AlternateCode, (dbo.GetMax((CHARINDEX('-', AlternateCode) + 1),0)), LEN(AlternateCode))
	END AS InterchangeSenderCode
	,CASE 
		WHEN SUBSTRING(Code, 1, 1) IN ('C','M','V')
			THEN '30'
		ELSE 'ZZ'
	END AS InterchangeSenderCodeQualifier
	,'P' AS InterchangeUsageIndicator
	,CASE 
		WHEN LEN(OtherLtrTrans) < 4
			THEN CASE 
				WHEN AlternateCode in ('', 'T')
					THEN CONVERT(NVARCHAR, NULL)
				ELSE SUBSTRING(AlternateCode, (dbo.GetMax((CHARINDEX('-', AlternateCode) + 1),0)), LEN(AlternateCode))
				END 
		ELSE SUBSTRING(OtherLtrTrans, (dbo.GetMax((CHARINDEX('-', OtherLtrTrans) + 1),0)), LEN(OtherLtrTrans))
	END AS Loop1000ASubmitterCode
	,CASE 
		WHEN LEN(OtherLtrTrans) < 4
			THEN CASE 
				WHEN LEN(AlternateCode) < 4
					THEN CONVERT(NVARCHAR, NULL)
				ELSE SUBSTRING(AlternateCode, 1, (dbo.GetMax((CHARINDEX('-', AlternateCode) - 1),0)))
				END 
		ELSE SUBSTRING(OtherLtrTrans, 1, (dbo.GetMax((CHARINDEX('-', OtherLtrTrans) - 1),0)))
	END AS Loop1000BRecipientCode
	,':' AS ComponentElementSeparator
	,'^' AS RepetitionSeparator
	,WebsiteUrl AS WebsiteUrl
	,Id AS LegacyPracticeCodeTableId
	FROM dbo.PracticeCodeTable p
	WHERE p.Id IN (SELECT Id FROM #inserted)
	END
END
GO