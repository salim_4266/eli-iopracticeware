﻿IF OBJECT_ID('[dbo].[usp_PaymentToDeleted]', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[usp_PaymentToDeleted];
GO

CREATE PROCEDURE [dbo].[usp_PaymentToDeleted] (@PatId INT)
AS
BEGIN
	SELECT 
	'' AS PaymentAmount
	,'' AS PaymentType
	,'' AS Invoice
	,'' AS InvoiceDate
	,'' AS LastName
	,'' AS FirstName
	,'' AS ReceivableId
	,'' AS PaymentId
	,'' AS AppointmentId
	,'' AS PatientId
	,'' AS PaymentServiceItem
	,'' AS PaymentDate
	,'' AS [Status]
	FROM (SELECT 1 AS V) Z 
	WHERE 1 = 0
END
GO

