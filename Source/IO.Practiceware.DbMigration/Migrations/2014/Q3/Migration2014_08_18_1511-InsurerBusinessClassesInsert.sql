﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'InsurerBusinessClassesInsert' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER InsurerBusinessClassesInsert
GO

CREATE TRIGGER [dbo].[InsurerBusinessClassesInsert] ON dbo.PracticeCodeTable
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'BUSINESSCLASS'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
	INSERT INTO model.InsurerBusinessClasses (
		[Name],
		[IsArchived],
		LegacyPracticeCodeTableId
	)
	SELECT 
	CASE Code
		WHEN 'AMERIH'
			THEN 'Amerihealth'
		WHEN 'BLUES'
			THEN 'BlueCrossBlueShield'
		WHEN 'COMM'
			THEN 'Commercial'
		WHEN 'MCAIDFL'
			THEN 'MedicaidFL'
		WHEN 'MCAIDNC'
			THEN 'MedicaidNC'
		WHEN 'MCAIDNJ'
			THEN 'MedicaidNJ'
		WHEN 'MCAIDNV'
			THEN 'MedicaidNV'
		WHEN 'MCAIDNY'
			THEN 'MedicaidNY'
		WHEN 'MCARENJ'
			THEN 'MedicareNJ'
		WHEN 'MCARENV'
			THEN 'MedicareNV'
		WHEN 'MCARENY'
			THEN 'MedicareNY'
		WHEN 'TEMPLATE'
			THEN 'Template'
		ELSE Code
	END AS [Name]
	,CONVERT(BIT, 0) AS IsArchived
	,CASE Code
		WHEN 'AMERIH'
			THEN 1
		WHEN 'BLUES'
			THEN 2
		WHEN 'COMM'
			THEN 3
		WHEN 'MCAIDFL'
			THEN 4
		WHEN 'MCAIDNC'
			THEN 5
		WHEN 'MCAIDNJ'
			THEN 6
		WHEN 'MCAIDNV'
			THEN 7
		WHEN 'MCAIDNY'
			THEN 8
		WHEN 'MCARENJ'
			THEN 9
		WHEN 'MCARENV'
			THEN 10
		WHEN 'MCARENY'
			THEN 11
		WHEN 'TEMPLATE'
			THEN 12
		ELSE Id + 1000
	END AS LegacyPracticeCodeTableId
	FROM dbo.PracticeCodeTable p
	WHERE p.Id IN (SELECT Id FROM #inserted)
	ORDER BY CASE p.Code
		WHEN 'AMERIH'
			THEN 1
		WHEN 'BLUES'
			THEN 2
		WHEN 'COMM'
			THEN 3
		WHEN 'MCAIDFL'
			THEN 4
		WHEN 'MCAIDNC'
			THEN 5
		WHEN 'MCAIDNJ'
			THEN 6
		WHEN 'MCAIDNV'
			THEN 7
		WHEN 'MCAIDNY'
			THEN 8
		WHEN 'MCARENJ'
			THEN 9
		WHEN 'MCARENV'
			THEN 10
		WHEN 'MCARENY'
			THEN 11
		WHEN 'TEMPLATE'
			THEN 12
		ELSE Id + 1000
	END
	END
END
GO