﻿IF NOT EXISTS(SELECT * FROM PracticeCodeTable pct WHERE pct.ReferenceType = 'ACCESSADS' AND SUBSTRING (Code, 6, 14) = 'Patient Portal')
BEGIN

INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, Signature, TestOrder, FollowUp, Rank, WebsiteUrl)
VALUES('ACCESSADS', '00 - Patient Portal : ','','F','','','','F','','','0','')

END