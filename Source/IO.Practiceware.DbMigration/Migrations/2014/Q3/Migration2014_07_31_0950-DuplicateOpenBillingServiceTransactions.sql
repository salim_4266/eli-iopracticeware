﻿--Step 1. Close duplicate open model.BillingServiceTransactions for insurance InvoiceReceivables.
-- This migration is for when there are BSTs linked to InvoiceReceivables where the invoiceReceivables
-- are inconsistent with the PatientInsurances.
;WITH CTE AS (
SELECT AmountSent
,BillingServiceTransactionStatusId
,MethodSentId
,BillingServiceId
,InvoiceReceivableId
,ClaimFileReceiverId
FROM model.BillingServiceTransactions b
JOIN model.InvoiceReceivables ir ON ir.Id = b.InvoiceReceivableId
	AND ir.PatientInsuranceId IS NOT NULL
WHERE BillingServiceTransactionStatusId IN (1, 2, 3, 4)
GROUP BY AmountSent
,BillingServiceTransactionStatusId
,MethodSentId
,BillingServiceId
,InvoiceReceivableId
,ClaimFileReceiverId
HAVING COUNT(*) > 1
)
,CTE2 AS (
SELECT 
b.* 
,ROW_NUMBER() OVER (PARTITION BY b.BillingServiceId ORDER BY b.[DateTime] DESC, b.ExternalSystemMessageId ASC) AS Rn
FROM model.BillingServiceTransactions b
JOIN CTE ON CTE.AmountSent = b.AmountSent
	AND CTE.BillingServiceTransactionStatusId = b.BillingServiceTransactionStatusId
	AND CTE.MethodSentId = b.MethodSentId
	AND CTE.BillingServiceId = b.BillingServiceId
	AND CTE.InvoiceReceivableId = b.InvoiceReceivableId
)
SELECT 
* 
INTO #DuplicateOpenBillingServiceTransactions
FROM CTE2 c
WHERE c.Rn > 1

;WITH CTE AS (
SELECT 
b.* 
FROM model.BillingServiceTransactions b 
WHERE b.Id IN (SELECT Id FROM #DuplicateOpenBillingServiceTransactions)
)
UPDATE CTE
SET CTE.BillingServiceTransactionStatusId = 
			CASE 
				WHEN CTE.BillingServiceTransactionStatusId IN (1,3)
					THEN 5
				WHEN CTE.BillingServiceTransactionStatusId = 2
					THEN 6
				WHEN CTE.BillingServiceTransactionStatusId = 4
					THEN 7
			END
FROM CTE
GO

--Step 2. Make sure [OnInsurancePoliciesUpdate_StraightJacket] does not exist.
-- This trigger should not exist anywhere, but was found on a cloud practice.
IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'OnInsurancePoliciesUpdate_StraightJacket' AND parent_id = OBJECT_ID('model.InsurancePolicies'))
	DROP TRIGGER [model].[OnInsurancePoliciesUpdate_StraightJacket]
GO