﻿--Fix reversal as adjustmentType
IF NOT EXISTS(SELECT * FROM dbo.PracticeCodeTable WHERE SUBSTRING(REVERSE(code),1,CHARINDEX(' - ',REVERSE(code))) = 'U' AND AlternateCode = 'D')
BEGIN
	UPDATE dbo.PracticeCodeTable 
	SET AlternateCode = 'D'
	WHERE SUBSTRING(REVERSE(code),1,CHARINDEX(' - ',REVERSE(code))) = 'U'
	AND ReferenceType = 'PAYMENTTYPE'
END

--Fix Void
IF NOT EXISTS(SELECT * FROM dbo.PracticeCodeTable WHERE SUBSTRING(REVERSE(code),1,CHARINDEX(' - ',REVERSE(code))) = 'V' AND AlternateCode = '')
BEGIN
	UPDATE dbo.PracticeCodeTable 
	SET AlternateCode = ''
	WHERE SUBSTRING(REVERSE(code),1,CHARINDEX(' - ',REVERSE(code))) = 'V'
	AND ReferenceType = 'PAYMENTTYPE'
END

--Add missing financial informations
IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'PAYMENTTYPE' AND SUBSTRING(REVERSE(code),1,CHARINDEX(' - ',REVERSE(code))) = '%')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'PAYMENTTYPE', N'Appeal Closed - %', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'PAYMENTTYPE' AND SUBSTRING(REVERSE(code),1,CHARINDEX(' - ',REVERSE(code))) = '&')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'PAYMENTTYPE', N'Denial Clsd - &', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

