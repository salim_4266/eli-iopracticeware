﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'InsurerPlanTypesInsert' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER InsurerPlanTypesInsert
GO

CREATE TRIGGER [dbo].[InsurerPlanTypesInsert] ON dbo.PracticeCodeTable
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'BUSINESSCLASS'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
	INSERT INTO model.InsurerPlanTypes (
		[Name],
		[LegacyPracticeCodeTableId]
	)
	SELECT 
	Code AS [Name]
	,Id AS LegacyPracticeCodeTableId
	FROM dbo.PracticeCodeTable p
	WHERE p.Id IN (SELECT Id FROM #inserted)
	END
END
GO