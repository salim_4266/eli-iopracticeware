﻿ALTER TABLE dbo.AuditEntries DISABLE TRIGGER ALL
UPDATE dbo.AuditEntries SET 
KeyNames = STUFF(
                   (SELECT
                        ',' + '[' + aek2.KeyName + ']'
                        FROM dbo.AuditEntryKeyValues aek2
                        WHERE aek.AuditEntryId = aek2.AuditEntryId
                        ORDER BY aek2.KeyValue
                        FOR XML PATH(''), TYPE
                   ).value('.','varchar(max)')
                   ,1,1, ''
              ),
KeyValues = STUFF(
                   (SELECT
                        ',' + '[' + aek2.KeyValue + ']'
                        FROM dbo.AuditEntryKeyValues aek2
                        WHERE aek.AuditEntryId = aek2.AuditEntryId
                        ORDER BY aek2.KeyValue
                        FOR XML PATH(''), TYPE
                   ).value('.','varchar(max)')
                   ,1,1, ''
              )
FROM dbo.AuditEntries ae
JOIN dbo.AuditEntryKeyValues aek on aek.AuditEntryId = ae.Id
WHERE ObjectName IN ('model.Appointments', 'dbo.Appointments', 'model.Encounters')
AND PatientID IS NULL
AND AppointmentId IS NULL
ALTER TABLE dbo.AuditEntries ENABLE TRIGGER ALL