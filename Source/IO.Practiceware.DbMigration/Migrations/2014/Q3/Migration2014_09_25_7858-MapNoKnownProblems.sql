﻿IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'NO KNOWN PROBLEMS')
BEGIN 
	INSERT INTO PracticeCodeTable (ReferenceType, Code, AlternateCode)
	VALUES ('NO KNOWN PROBLEMS', 'SYSTEMNORMALQ:1', '0001      No known problems                             Q:1')
END