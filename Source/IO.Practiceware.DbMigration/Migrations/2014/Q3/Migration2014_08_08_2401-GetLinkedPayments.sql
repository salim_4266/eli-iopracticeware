﻿IF OBJECT_ID ( 'dbo.[GetLinkedPayments]', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.[GetLinkedPayments];
GO

CREATE PROCEDURE [dbo].[GetLinkedPayments]
	@ItemId NVARCHAR(100)
AS
SET NOCOUNT ON;

DECLARE @AdjustmentIdMax INT
SET @AdjustmentIdMax = 11000000
DECLARE @FinancialIdMax INT
SET @FinancialIdMax = 11000000

DECLARE @AppointmentId INT
SET @AppointmentId = (
	SELECT TOP 1 i.LegacyPatientReceivablesAppointmentId 
	FROM model.Invoices i 
	JOIN model.BillingServices bs ON bs.InvoiceId = i.Id 
		AND bs.Id = @ItemId)

IF OBJECT_ID('tempdb..#InvoiceReceivableIds') IS NOT NULL
BEGIN
	DROP TABLE #InvoiceReceivableIds
END

SELECT Id INTO #InvoiceReceivableIds FROM dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId)

IF (@@ROWCOUNT > 0)
BEGIN

IF OBJECT_ID('tempdb..#AdjustmentTypes') IS NOT NULL
BEGIN
	DROP TABLE #AdjustmentTypes
END

SELECT 
v.Id AS Id
,v.PaymentType
,v.IsDebit AS IsDebit
,v.ShortName AS ShortName
INTO #AdjustmentTypes
FROM (
	SELECT 
		CASE
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'P'
				THEN 1
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'X'
				THEN 2
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '8'
				THEN 3
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'R'
				THEN 4
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'U'
				THEN 5
			ELSE pct.Id + 1000
		END AS Id
		,SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS PaymentType
		,CASE pct.AlternateCode
			WHEN 'D'
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
		END AS IsDebit
		,LetterTranslation AS ShortName
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'
	GROUP BY 
	Id
	,Code
	,pct.AlternateCode
	,LetterTranslation
) AS v

IF OBJECT_ID('tempdb..#FinancialInformationTypes') IS NOT NULL
BEGIN
	DROP TABLE #FinancialInformationTypes
END

SELECT 
v.Id AS Id
,v.FinancialInformationType
,v.ShortName AS ShortName
,v.PaymentFinancialType
INTO #FinancialInformationTypes
FROM (
	SELECT 
	CASE
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '!'
			THEN 1
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '|'
			THEN 2
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '?'
			THEN 3
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '%'
			THEN 4
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'D'
			THEN 5
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '&'
			THEN 6
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = ']'
			THEN 7
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '0'
			THEN 8
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'V'
			THEN 9
		ELSE pct.Id + 1000
	END AS Id
	,SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS FinancialInformationType
	,AlternateCode AS PaymentFinancialType
	,LetterTranslation AS ShortName
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'		
		AND (pct.AlternateCode = '' OR pct.AlternateCode IS NULL)
	GROUP BY 
	pct.Id
	,pct.Code
	,LetterTranslation
	,AlternateCode
) AS v

SELECT
adj.LegacyPaymentId AS PaymentId
,adj.LegacyInvoiceReceivableId AS ReceivableId
,CASE 
	WHEN (adj.FinancialSourceTypeId = 1)
		THEN adj.InsurerId
	WHEN (adj.FinancialSourceTypeId = 2)
		THEN adj.PatientId
	ELSE 0
END AS PayerId
,CASE adj.FinancialSourceTypeId
	WHEN 1
		THEN 'I'
	WHEN 2
		THEN 'P'
	WHEN 3
		THEN 'O'
END AS PayerType
,at.PaymentType AS PaymentType
,adj.Amount AS PaymentAmount
,CONVERT(NVARCHAR(8),adj.PostedDateTime,112) AS PaymentDate
,COALESCE(fb.CheckCode,'') AS PaymentCheck
,CASE at.IsDebit 
	WHEN 1
		THEN 'D'
	ELSE 'C'
END AS PaymentFinancialType
,COALESCE(pctRsnCode.Code,'') AS PaymentReason
,CASE 
	WHEN (adj.FinancialSourceTypeId = 1)
		THEN ins.Name
	WHEN (adj.FinancialSourceTypeId = 2)
		THEN 'PATIENT'
	ELSE 'OFFICE'
END AS InsurerName
,CASE 
	WHEN (adj.Comment = '' OR adj.Comment IS NULL)
		THEN ''
	ELSE 'N'
END AS NoteInd
,at.ShortName AS LetterTranslation
FROM model.Adjustments adj
INNER JOIN #InvoiceReceivableIds inv ON inv.Id = adj.InvoiceReceivableId
INNER JOIN #AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
INNER JOIN model.InvoiceReceivables ir ON ir.Id = inv.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
LEFT JOIN model.BillingServices bs ON bs.Id = adj.BillingServiceId
LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
LEFT JOIN dbo.PracticeCodeTable pctRsnCode ON pctRsnCode.Id = adj.ClaimAdjustmentReasonCodeId 
	AND pctRsnCode.ReferenceType = 'PAYMENTREASON'
LEFT JOIN model.Insurers ins ON ins.Id = adj.InsurerId
WHERE bs.Id = @ItemId

UNION ALL

SELECT
fi.LegacyPaymentId AS PaymentId
,fi.LegacyInvoiceReceivableId AS ReceivableId
,CASE 
	WHEN (fi.FinancialSourceTypeId = 1)
		THEN fi.InsurerId
	WHEN (fi.FinancialSourceTypeId = 2)
		THEN fi.PatientId
	ELSE 0
END AS PayerId
,CASE fi.FinancialSourceTypeId
	WHEN 1
		THEN 'I'
	WHEN 2
		THEN 'P'
	WHEN 3
		THEN 'O'
END AS PayerType
,fit.FinancialInformationType AS PaymentType
,fi.Amount AS PaymentAmount
,CONVERT(NVARCHAR(8),fi.PostedDateTime,112) AS PaymentDate
,COALESCE(fb.CheckCode,'') AS PaymentCheck
,COALESCE(fit.PaymentFinancialType,'') AS PaymentFinancialType
,COALESCE(pctRsnCode.Code,'') AS PaymentReason
,CASE 
	WHEN (fi.FinancialSourceTypeId = 1)
		THEN ins.Name
	WHEN (fi.FinancialSourceTypeId = 2)
		THEN 'PATIENT'
	ELSE 'OFFICE'
END AS InsurerName
,CASE 
	WHEN (fi.Comment = '' OR fi.Comment IS NULL)
		THEN ''
	ELSE 'N'
END AS NoteInd
,fit.ShortName AS LetterTranslation
FROM model.FinancialInformations fi
INNER JOIN #InvoiceReceivableIds inv ON inv.Id = fi.InvoiceReceivableId
INNER JOIN #FinancialInformationTypes fit ON fit.Id = fi.FinancialInformationTypeId
INNER JOIN model.InvoiceReceivables ir ON ir.Id = inv.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
LEFT JOIN model.BillingServices bs ON bs.Id = fi.BillingServiceId
LEFT JOIN model.FinancialBatches fb ON fb.Id = fi.FinancialBatchId
LEFT JOIN dbo.PracticeCodeTable pctRsnCode ON pctRsnCode.Id = fi.ClaimAdjustmentReasonCodeId 
	AND pctRsnCode.ReferenceType = 'PAYMENTREASON'
LEFT JOIN model.Insurers ins ON ins.Id = fi.InsurerId
WHERE bs.Id = @ItemId
END
GO