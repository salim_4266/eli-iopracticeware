﻿IF OBJECT_ID('model.Providers') IS NOT NULL
	EXEC dbo.DropObject 'model.Providers'
GO

-- 'Providers' does not exist, creating...
CREATE TABLE [model].[Providers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IsBillable] BIT  NOT NULL,
    [IsEmr] BIT  NOT NULL,
    [ServiceLocationId] int  NOT NULL,
    [BillingOrganizationId] int  NOT NULL,
    [UserId] int  NULL,
	[LegacyProviderId] BIGINT NULL
);
GO

-- Creating primary key on [Id] in table 'Providers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.Providers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[Providers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.Providers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[Providers]
ADD CONSTRAINT [PK_Providers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

INSERT INTO model.Providers ([IsBillable],
    [IsEmr],
    [ServiceLocationId],
    [BillingOrganizationId],
    [UserId],
	[LegacyProviderId])
---human and facility providers where ServiceLocation is from PracticeName table, including override 
SELECT 
CASE re.Billable
	WHEN 'Y'
		THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0)
	END AS IsBillable,
CASE re.GoDirect
	WHEN 'Y'
		THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0)
	END AS IsEMR,
CASE 
	WHEN re.ResourceType = 'R'
		THEN pnBillOrg.PracticeId
	ELSE pnServLoc.PracticeId 
	END AS ServiceLocationId,
CASE
	WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN (110000000 * 1 + re.ResourceId) 
	ELSE pnBillOrg.PracticeId
	END AS BillingOrganizationId,
CASE
	WHEN re.ResourceType = 'R'
		THEN NULL
	ELSE re.ResourceId 
	END AS UserId,
CASE 
	WHEN re.ResourceType = 'R' ---internal facility
		THEN (CONVERT(BIGINT, 110000000) * pnBillOrg.PracticeId + (CONVERT(BIGINT, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
	WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN (CONVERT(BIGINT, 110000000) * (110000000 * 2 + pnServLoc.PracticeId) + (CONVERT(BIGINT, 110000000) * re.ResourceId + re.ResourceId))
	ELSE (CONVERT(BIGINT, 110000000) * pnServLoc.PracticeId + (CONVERT(BIGINT, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
	END AS LegacyProviderId
FROM dbo.Resources re
INNER JOIN dbo.PracticeName AS pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
	AND pnBillOrg.PracticeType = 'P'
INNER JOIN dbo.PracticeName AS pnServLoc ON pnServLoc.PracticeType = 'P'
INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
		AND pa.ResourceType = 'R'
WHERE (re.ResourceType in ('D', 'Q', 'Z','Y')
	OR (re.ResourceType = 'R'
		AND re.ServiceCode = '02'
		AND re.Billable = 'Y'
		AND pic.FieldValue = 'T'
		AND pnBillOrg.LocationReference <> '')
		)
GROUP BY
CASE 
	WHEN re.ResourceType = 'R' ---internal facility
		THEN (CONVERT(BIGINT, 110000000) * pnBillOrg.PracticeId + (CONVERT(BIGINT, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
	WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN (CONVERT(BIGINT, 110000000) * (110000000 * 2 + pnServLoc.PracticeId) + (CONVERT(BIGINT, 110000000) * re.ResourceId + re.ResourceId))
	ELSE (CONVERT(BIGINT, 110000000) * pnServLoc.PracticeId + (CONVERT(BIGINT, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
	END, 
CASE
	WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN (110000000 * 1 + re.ResourceId) 
	ELSE pnBillOrg.PracticeId
	END,
CASE re.Billable
	WHEN 'Y'
		THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0)
	END ,
CASE re.GoDirect
	WHEN 'Y'
		THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0)
	END ,
CASE 
	WHEN re.ResourceType = 'R'
		THEN pnBillOrg.PracticeId
	ELSE pnServLoc.PracticeId 
	END ,
CASE
	WHEN re.ResourceType = 'R'
		THEN NULL
	ELSE re.ResourceId 
	END
	
UNION ALL
	
----Human Providers where ServiceLocation is from Resources table including override
SELECT
CASE re.Billable
	WHEN 'Y'
		THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0)
	END AS IsBillable,
CASE re.GoDirect
	WHEN 'Y'
		THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0)
	END AS IsEMR,
(110000000 * 1 + reServLoc.ResourceId) AS ServiceLocationId,
CASE 
	WHEN  pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN (110000000 * 1 + re.ResourceId)
	ELSE pnBillOrg.PracticeId 
	END AS BillingOrganizationId,
re.ResourceId AS UserId,
CASE 
	WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN (CONVERT(BIGINT, 110000000) * (110000000 * 3 + reServLoc.ResourceId) + (CONVERT(BIGINT, 110000000) * re.ResourceId + re.ResourceId))
	ELSE (CONVERT(BIGINT, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(BIGINT, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
	END AS LegacyProviderId
FROM dbo.Resources re
INNER JOIN dbo.PracticeName AS pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
	AND pnBillOrg.PracticeType = 'P'
INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = 'R'
	AND reServLoc.ServiceCode = '02'
LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
		AND pa.ResourceType = 'R'
WHERE re.ResourceType in ('D', 'Q', 'Z','Y')
GROUP BY
	CASE 
	WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN (CONVERT(BIGINT, 110000000) * (110000000 * 3 + reServLoc.ResourceId) + (CONVERT(BIGINT, 110000000) * re.ResourceId + re.ResourceId))
	ELSE (CONVERT(BIGINT, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(BIGINT, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
	END,
CASE 
	WHEN  pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN (110000000 * 1 + re.ResourceId)
	ELSE pnBillOrg.PracticeId 
	END,
CASE re.GoDirect
	WHEN 'Y'
		THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0)
	END ,
(110000000 * 1 + reServLoc.ResourceId),
re.ResourceId,
re.Billable,
pa.ResourceId,
reServLoc.ResourceId,
pnBillOrg.PracticeId

-- Creating non-clustered index for FOREIGN KEY 'FK_ServiceLocationProvider'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ServiceLocationProvider')
	DROP INDEX IX_FK_ServiceLocationProvider ON [model].[Providers]
GO
CREATE INDEX [IX_FK_ServiceLocationProvider]
ON [model].[Providers]
    ([ServiceLocationId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BillingOrganizationProvider'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_BillingOrganizationProvider')
	DROP INDEX IX_FK_BillingOrganizationProvider ON [model].[Providers]
GO
CREATE INDEX [IX_FK_BillingOrganizationProvider]
ON [model].[Providers]
    ([BillingOrganizationId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserProvider'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_UserProvider')
	DROP INDEX IX_FK_UserProvider ON [model].[Providers]
GO
CREATE INDEX [IX_FK_UserProvider]
ON [model].[Providers]
    ([UserId]);
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.Invoices'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE i
SET i.BillingProviderId = p.Id
FROM model.Invoices i
JOIN model.Providers p ON p.LegacyProviderId = i.BillingProviderId

UPDATE i
SET i.BillingProviderId = (SELECT TOP 1 Id FROM model.Providers WHERE IsBillable= 1)
FROM model.Invoices i
LEFT JOIN model.Providers p ON p.Id = i.BillingProviderid
WHERE p.Id IS NULL

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersInsertResources' AND parent_id = OBJECT_ID('dbo.Resources','U'))
	DROP TRIGGER ProvidersInsertResources
GO

CREATE TRIGGER dbo.ProvidersInsertResources ON dbo.Resources
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @ResourceId INT
	DECLARE @ResourceType NVARCHAR(1)
	DECLARE @PracticeId INT
	DECLARE @Billable NVARCHAR(1)
	DECLARE @GoDirect NVARCHAR(1)
	DECLARE @ServiceCode NVARCHAR(4)

	DECLARE ProvidersInsertResourcesCursor CURSOR FOR
	SELECT ResourceId, ResourceType, PracticeId, Billable, GoDirect, ServiceCode
	FROM inserted WHERE ResourceType in ('D', 'Q', 'Z','Y')

	OPEN ProvidersInsertResourcesCursor FETCH NEXT FROM ProvidersInsertResourcesCursor INTO
	@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect, @ServiceCode
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		INSERT INTO model.Providers (
			IsBillable
			,IsEmr
			,ServiceLocationId
			,BillingOrganizationId
			,UserId
			,LegacyProviderId
		)
		SELECT 
		* 
		FROM (
			SELECT DISTINCT
			CASE @Billable WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsBillable
			,CASE @GoDirect WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsEMR
			,CASE WHEN @ResourceType = 'R' THEN pnBillOrg.PracticeId ELSE pnServLoc.PracticeId END AS ServiceLocationId
			,CASE WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'
					THEN (110000000 * 1 + @ResourceId) 
				ELSE pnBillOrg.PracticeId
			END AS BillingOrganizationId
			,CASE WHEN @ResourceType = 'R' THEN NULL ELSE @ResourceId END AS UserId
			,CASE 
				WHEN re.ResourceType = 'R'
					THEN (CONVERT(BIGINT, 110000000) * pnBillOrg.PracticeId + (CONVERT(BIGINT, 110000000) * pnBillOrg.PracticeId + @ResourceId))
				WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'
					THEN (CONVERT(BIGINT, 110000000) * (110000000 * 2 + pnServLoc.PracticeId) + (CONVERT(BIGINT, 110000000) * @ResourceId + @ResourceId))
				ELSE (CONVERT(BIGINT, 110000000) * pnServLoc.PracticeId + (CONVERT(BIGINT, 110000000) * pnBillOrg.PracticeId + @ResourceId))
			END AS LegacyProviderId
			FROM inserted re
			INNER JOIN dbo.PracticeName AS pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
				AND pnBillOrg.PracticeType = 'P'
			INNER JOIN dbo.PracticeName AS pnServLoc ON pnServLoc.PracticeType = 'P'
			INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
			LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
					AND pa.ResourceType = 'R'
			WHERE (re.ResourceType IN ('D', 'Q', 'Z','Y')) 
				AND (re.ResourceId = @ResourceId
				OR (@ResourceType = 'R'
					AND @ServiceCode = '02'
					AND @Billable = 'Y'
					AND pic.FieldValue = 'T'
					AND pnBillOrg.LocationReference <> ''))
	
			UNION
	
			SELECT DISTINCT
			CASE @Billable WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsBillable
			,CASE @GoDirect WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsEMR
			,(110000000 * 1 + reServLoc.ResourceId) AS ServiceLocationId
			,CASE WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y' THEN (110000000 * 1 + @ResourceId) ELSE pnBillOrg.PracticeId END AS BillingOrganizationId
			,@ResourceId AS UserId
			,CASE WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'
					THEN (CONVERT(BIGINT, 110000000) * (110000000 * 3 + reServLoc.ResourceId) + (CONVERT(BIGINT, 110000000) * @ResourceId + @ResourceId))
				ELSE (CONVERT(BIGINT, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(BIGINT, 110000000) * pnBillOrg.PracticeId + @ResourceId))
			END AS LegacyProviderId
			FROM inserted re
			INNER JOIN dbo.PracticeName AS pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
				AND pnBillOrg.PracticeType = 'P'
			INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = 'R'
				AND reServLoc.ServiceCode = '02'
			LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
					AND pa.ResourceType = 'R'
			WHERE re.ResourceType IN ('D', 'Q', 'Z','Y')
				AND re.ResourceId = @ResourceId
		)v
		EXCEPT
		SELECT IsBillable ,IsEmr ,ServiceLocationId ,BillingOrganizationId ,UserId ,LegacyProviderId FROM model.Providers	

		FETCH NEXT FROM ProvidersInsertResourcesCursor INTO 
		@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect, @ServiceCode
	END
	CLOSE ProvidersInsertResourcesCursor
	DEALLOCATE ProvidersInsertResourcesCursor
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersUpdateResources' AND parent_id = OBJECT_ID('dbo.Resources','U'))
	DROP TRIGGER ProvidersUpdateResources
GO

CREATE TRIGGER dbo.ProvidersUpdateResources ON dbo.Resources
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @ResourceId int
	DECLARE @ResourceType nvarchar(1)
	DECLARE @PracticeId int
	DECLARE @Billable nvarchar(1)
	DECLARE @GoDirect nvarchar(1)

	DECLARE ProvidersUpdateResourcesCursor CURSOR FOR
	SELECT i.ResourceId, i.ResourceType, i.PracticeId, i.Billable, i.GoDirect
	FROM inserted i
	JOIN deleted d ON d.ResourceId = i.ResourceId
		AND d.ResourceType IN ('D', 'Q', 'Z','Y')
	WHERE i.ResourceType IN ('D', 'Q', 'Z','Y')

	OPEN ProvidersUpdateResourcesCursor FETCH NEXT FROM ProvidersUpdateResourcesCursor INTO
	@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		DECLARE @DeletedPracticeId INT
		DECLARE @DeletedResourceId INT
		SET @DeletedPracticeId = (SELECT TOP 1 PracticeId FROM deleted WHERE @ResourceId = @ResourceId)
		SET @DeletedResourceId = (SELECT TOP 1 ResourceId FROM deleted WHERE @ResourceId = @ResourceId)

		UPDATE p
		SET p.IsBillable = CASE WHEN @Billable = 'Y' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
		,p.IsEmr = CASE WHEN @GoDirect = 'Y' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
		,p.ServiceLocationId = CASE WHEN @ResourceType = 'R' THEN (
			SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId AND PracticeType = 'P')
			ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId)
		END
		,p.BillingOrganizationId = CASE WHEN 
			((SELECT TOP 1 [Override] FROM dbo.PracticeAffiliations WHERE ResourceId = @ResourceId AND ResourceType = 'R') = 'Y' OR
			(SELECT TOP 1 OrgOverride FROM dbo.PracticeAffiliations WHERE ResourceId = @ResourceId AND ResourceType = 'R') = 'Y')
			THEN (110000000 * 1 + @ResourceId) 
		ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId AND PracticeType = 'P')
		END
		FROM model.Providers p
		WHERE p.UserId = @ResourceId
			AND p.ServiceLocationId = CASE WHEN @ResourceType = 'R' THEN (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @DeletedPracticeId AND PracticeType = 'P')
				ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @DeletedPracticeId)
			END
			AND p.BillingOrganizationId = CASE WHEN 
				((SELECT TOP 1 [Override] FROM dbo.PracticeAffiliations WHERE ResourceId = @DeletedResourceId AND ResourceType = 'R') = 'Y' OR
				(SELECT TOP 1 OrgOverride FROM dbo.PracticeAffiliations WHERE ResourceId = @DeletedResourceId AND ResourceType = 'R') = 'Y')
				THEN (110000000 * 1 + @DeletedResourceId) 
			ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @DeletedPracticeId AND PracticeType = 'P')
			END

		FETCH NEXT FROM ProvidersUpdateResourcesCursor INTO 
		@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect
	END
	CLOSE ProvidersUpdateResourcesCursor
	DEALLOCATE ProvidersUpdateResourcesCursor
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersInsertPracticeAffiliations' AND parent_id = OBJECT_ID('dbo.PracticeAffiliations','U'))
	DROP TRIGGER ProvidersInsertPracticeAffiliations
GO

CREATE TRIGGER dbo.ProvidersInsertPracticeAffiliations ON dbo.PracticeAffiliations
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Override int
	DECLARE @OrgOverride nvarchar(1)
	DECLARE @ResourceId int
	DECLARE @ResourceType nvarchar(1)
	DECLARE @PracticeId int

	DECLARE ProvidersInsertPracticeAffiliationsCursor CURSOR FOR
	SELECT i.[Override], i.OrgOverride, i.ResourceId, r.ResourceType, r.PracticeId
	FROM inserted i
	JOIN dbo.Resources r ON r.ResourceId = i.ResourceId 
		AND r.ResourceType IN ('D', 'Q', 'Z','Y')
		AND i.ResourceType = 'R'

	OPEN ProvidersInsertPracticeAffiliationsCursor FETCH NEXT FROM ProvidersInsertPracticeAffiliationsCursor INTO
	@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		IF (@ResourceId IS NOT NULL) AND ((SELECT Id FROM model.Providers WHERE UserId = @ResourceId) IS NOT NULL)
		BEGIN
			UPDATE p
			SET p.BillingOrganizationId = CASE WHEN @Override = 'Y' OR @OrgOverride  = 'Y'
				THEN (110000000 * 1 + @ResourceId) 
			ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId AND PracticeType = 'P')
			END
			FROM model.Providers p
			WHERE p.UserId = @ResourceId
				AND p.ServiceLocationId = CASE WHEN @ResourceType = 'R' THEN (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId AND PracticeType = 'P')
					ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId)
				END
				AND p.BillingOrganizationId = CASE WHEN 
					((SELECT TOP 1 [Override] FROM dbo.PracticeAffiliations WHERE ResourceId = @ResourceId AND ResourceType = 'R') = 'Y' OR
					(SELECT TOP 1 OrgOverride FROM dbo.PracticeAffiliations WHERE ResourceId = @ResourceId AND ResourceType = 'R') = 'Y')
					THEN (110000000 * 1 + @ResourceId) 
				ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId AND PracticeType = 'P')
				END
		END
		FETCH NEXT FROM ProvidersInsertPracticeAffiliationsCursor INTO 
		@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId
	END
	CLOSE ProvidersInsertPracticeAffiliationsCursor
	DEALLOCATE ProvidersInsertPracticeAffiliationsCursor
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersUpdatePracticeAffiliations' AND parent_id = OBJECT_ID('dbo.PracticeAffiliations','U'))
	DROP TRIGGER ProvidersUpdatePracticeAffiliations
GO

CREATE TRIGGER dbo.ProvidersUpdatePracticeAffiliations ON dbo.PracticeAffiliations
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Override int
	DECLARE @OrgOverride nvarchar(1)
	DECLARE @ResourceId int
	DECLARE @ResourceType nvarchar(1)
	DECLARE @PracticeId int

	DECLARE ProvidersUpdatePracticeAffiliationsCursor CURSOR FOR
	SELECT i.[Override], i.OrgOverride, i.ResourceId, r.ResourceType, r.PracticeId
	FROM inserted i
	JOIN dbo.Resources r ON r.ResourceId = i.ResourceId 
		AND r.ResourceType IN ('D', 'Q', 'Z','Y')
		AND i.ResourceType = 'R'

	OPEN ProvidersUpdatePracticeAffiliationsCursor FETCH NEXT FROM ProvidersUpdatePracticeAffiliationsCursor INTO
	@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		DECLARE @DeletedPracticeId INT
		DECLARE @DeletedResourceId INT
		SET @DeletedPracticeId = (SELECT TOP 1 r.PracticeId
		FROM deleted d
		JOIN dbo.Resources r ON r.ResourceId = d.ResourceId 
			AND r.ResourceType IN ('D', 'Q', 'Z','Y')
			AND d.ResourceType = 'R')
		
		UPDATE p
		SET p.BillingOrganizationId = CASE WHEN @Override = 'Y' OR @OrgOverride  = 'Y'
			THEN (110000000 * 1 + @ResourceId) 
		ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId AND PracticeType = 'P')
		END
		FROM model.Providers p
		WHERE p.UserId = @ResourceId
			AND p.ServiceLocationId = CASE WHEN @ResourceType = 'R' THEN (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId AND PracticeType = 'P')
				ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId)
			END
			AND p.BillingOrganizationId = CASE WHEN 
				((SELECT TOP 1 [Override] FROM dbo.PracticeAffiliations WHERE ResourceId = @DeletedResourceId AND ResourceType = 'R') = 'Y' OR
				(SELECT TOP 1 OrgOverride FROM dbo.PracticeAffiliations WHERE ResourceId = @DeletedResourceId AND ResourceType = 'R') = 'Y')
				THEN (110000000 * 1 + @DeletedResourceId) 
			ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId AND PracticeType = 'P')
			END

		FETCH NEXT FROM ProvidersUpdatePracticeAffiliationsCursor INTO 
		@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId
	END
	CLOSE ProvidersUpdatePracticeAffiliationsCursor
	DEALLOCATE ProvidersUpdatePracticeAffiliationsCursor
END
GO