﻿IF NOT EXISTS (SELECT TOP 1 * FROM sys.columns WHERE object_id = OBJECT_ID('model.ExternalSystemEntities','U') AND name = 'PracticeRepositoryEntityId')
	BEGIN
		ALTER TABLE model.ExternalSystemEntities ADD [PracticeRepositoryEntityId] INT NULL
	END
GO

-- Creating foreign key on [PracticeRepositoryEntityId] in table 'ExternalSystemEntities'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalSystemEntityPracticeRepositoryEntity'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalSystemEntities] DROP CONSTRAINT FK_ExternalSystemEntityPracticeRepositoryEntity
	
IF OBJECTPROPERTY(OBJECT_ID('[model].[PracticeRepositoryEntities]'), 'IsUserTable') = 1
	ALTER TABLE [model].[ExternalSystemEntities]
	ADD CONSTRAINT [FK_ExternalSystemEntityPracticeRepositoryEntity]
    FOREIGN KEY ([PracticeRepositoryEntityId])
    REFERENCES [model].[PracticeRepositoryEntities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalSystemEntityPracticeRepositoryEntity'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ExternalSystemEntityPracticeRepositoryEntity')
	DROP INDEX IX_FK_ExternalSystemEntityPracticeRepositoryEntity ON [model].[ExternalSystemEntities]
GO
CREATE INDEX [IX_FK_ExternalSystemEntityPracticeRepositoryEntity]
ON [model].[ExternalSystemEntities]
    ([PracticeRepositoryEntityId]);
GO

IF EXISTS (SELECT TOP 1 * FROM sys.columns WHERE object_id = OBJECT_ID('model.ExternalSystemEntities','U') AND name = 'PracticeRepositoryEntityId')
	BEGIN
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Appointment') WHERE Name = 'Appointment'
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'AppointmentType') WHERE Name = 'AppointmentType'
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Patient') WHERE Name = 'Patient'
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'ServiceLocation') WHERE Name = 'LocationResource'
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'User') WHERE Name = 'AttendingDoctor'
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Insurer') WHERE Name = 'Insurer'
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Gender') WHERE Name = 'Gender'
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'MaritalStatus') WHERE Name = 'MaritalStatus'
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Race') WHERE Name = 'Race'
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Language') WHERE Name = 'Language'
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Ethnicity') WHERE Name = 'Ethnicity'
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'ExternalContact') WHERE Name = 'ReferringDoctor'
		UPDATE model.ExternalSystemEntities SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Appointment') WHERE Name = 'Visit'
	END
GO
