﻿IF EXISTS (SELECT * FROM sys.columns WHERE object_id =  OBJECT_ID('model.Invoices','U') AND name = 'LegacyDateTime' AND is_nullable = 0)
BEGIN
	ALTER TABLE model.Invoices
	ALTER COLUMN LegacyDateTime DATETIME NULL
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'LegacyPatientReceivablesColumns' AND parent_id = OBJECT_ID('model.Invoices','U'))
BEGIN
    DROP TRIGGER model.LegacyPatientReceivablesColumns
END
GO

CREATE TRIGGER model.LegacyPatientReceivablesColumns
ON model.Invoices 
AFTER INSERT 
AS
BEGIN 
SET NOCOUNT ON

UPDATE inv
SET LegacyDateTime = CONVERT(DATETIME,ap.AppDate)
	,LegacyPatientReceivablesPatientId = ap.PatientId
	,LegacyPatientReceivablesInvoice = ap.InvoiceNumber
	,LegacyPatientReceivablesAppointmentId = ap.AppointmentId
FROM model.Invoices inv
INNER JOIN inserted i ON inv.Id = i.Id 
INNER JOIN dbo.Appointments ap  ON i.EncounterId = ap.EncounterId
WHERE i.InvoiceTypeId <> 2 AND ap.EncounterId = ap.AppointmentId

UPDATE inv
SET LegacyDateTime = CONVERT(DATETIME,ap.AppDate)
	,LegacyPatientReceivablesPatientId = ap.PatientId
	,LegacyPatientReceivablesInvoice = ap.InvoiceNumber
	,LegacyPatientReceivablesAppointmentId = ap.AppointmentId
FROM model.Invoices inv
INNER JOIN inserted i ON inv.Id = i.Id 
INNER JOIN dbo.Appointments ap  ON i.EncounterId = ap.EncounterId
WHERE i.InvoiceTypeId = 2 AND ap.EncounterId <> ap.AppointmentId

END
GO