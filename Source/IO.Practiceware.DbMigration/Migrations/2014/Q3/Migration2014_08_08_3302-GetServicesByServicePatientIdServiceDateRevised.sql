﻿IF OBJECT_ID('[dbo].[GetServicesByServicePatientIdServiceDate]', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[GetServicesByServicePatientIdServiceDate];
GO

CREATE PROCEDURE [dbo].[GetServicesByServicePatientIdServiceDate] 
	@PatientId INT
	,@ServiceDate NVARCHAR(8)
	,@Service NVARCHAR(MAX)
AS
BEGIN

SELECT 
bs.Id AS ItemId
,i.LegacyPatientReceivablesInvoice AS [Invoice]
,CONVERT(NVARCHAR(16),es.Code) AS [Service]
,COALESCE(STUFF((
	SELECT sm.Code AS [text()]
	FROM model.BillingServiceModifiers mo
	JOIN model.ServiceModifiers sm ON sm.Id = mo.ServiceModifierId
	WHERE mo.BillingServiceId = m.BillingServiceId
	ORDER BY mo.OrdinalId ASC
	FOR XML PATH('')
), 1, 0, ''),'') AS [Modifier]
,CONVERT(REAL,bs.Unit) AS [Quantity]
,CONVERT(REAL,bs.UnitCharge) AS [Charge]
,CONVERT(NVARCHAR(2),'') AS [TypeOfService]
,sc.PlaceOfServiceCode AS [PlaceOfService]
,CONVERT(NVARCHAR(8),i.LegacyDateTime,112) AS [ServiceDate]
,bs.LegacyPatientReceivableServicesLinkedDiag AS [LinkedDiag]
,'A' AS [Status]
,CASE WHEN es.IsOrderingProvider = 1 THEN 'T' ELSE 'F' END AS [OrderDoc]
,CASE WHEN es.IsConsultation = 1 THEN 'T' ELSE 'F' END  AS [ConsultOn]
,CONVERT(REAL,bs.UnitAllowableExpense) AS [FeeCharge]
,bs.OrdinalId AS [ItemOrder]
,0 AS [ECode]
,CONVERT(NVARCHAR(8),i.LegacyDateTime,112) AS [PostDate]
,CONVERT(NVARCHAR(16),es.[Description]) AS [ExternalRefInfo]
,bs.Unit AS [QuantityDecimal]
,bs.UnitCharge AS [ChargeDecimal]
,bs.UnitAllowableExpense AS [FeeChargeDecimal]
,i.LegacyPatientReceivablesAppointmentId AS [AppointmentId]
FROM model.BillingServices bs
JOIN model.Invoices i ON i.Id = bs.InvoiceId
JOIN model.ServiceLocationCodes sc ON sc.Id = i.ServiceLocationCodeId
JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
LEFT JOIN dbo.Appointments ap ON ap.AppointmentId = i.LegacyPatientReceivablesAppointmentId
LEFT JOIN model.BillingServiceModifiers m ON m.BillingServiceId = bs.Id
WHERE es.Code LIKE @Service + '%'
	AND i.LegacyPatientReceivablesPatientId = @PatientId
	AND ap.AppDate = @ServiceDate
GROUP BY bs.Id
,i.LegacyPatientReceivablesInvoice
,CONVERT(NVARCHAR(16),es.Code)
,bs.LegacyPatientReceivableServicesLinkedDiag
,CONVERT(REAL,bs.Unit)
,CONVERT(REAL,bs.UnitCharge)
,sc.PlaceOfServiceCode
,CONVERT(NVARCHAR(8),i.LegacyDateTime,112)
,bs.LegacyPatientReceivableServicesLinkedDiag
,CASE WHEN es.IsOrderingProvider = 1 THEN 'T' ELSE 'F' END
,CASE WHEN es.IsConsultation = 1 THEN 'T' ELSE 'F' END
,CONVERT(REAL,bs.UnitAllowableExpense)
,bs.OrdinalId
,CONVERT(NVARCHAR(8),i.LegacyDateTime,112)
,CONVERT(NVARCHAR(16),es.[Description])
,bs.Unit
,bs.UnitCharge
,bs.UnitAllowableExpense
,i.LegacyPatientReceivablesAppointmentId
,m.BillingServiceId
END
GO

