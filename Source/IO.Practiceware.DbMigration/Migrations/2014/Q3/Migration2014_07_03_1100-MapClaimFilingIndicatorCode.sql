﻿DECLARE @InsurerId INT
SELECT @insurerId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'Insurer'

DECLARE @QDMId INT
SELECT @QDMId = Id FROM model.ExternalSystems WHERE Name ='QualityDataModel'

DECLARE @QDMInsurerId INT
SELECT @QDMInsurerId = Id FROM model.ExternalSystemEntities ese WHERE ese.ExternalSystemId = @QDMId 
AND ese.Name = 'PatientCharacteristicPayerPayer'

DELETE FROM model.ExternalSystemEntityMappings WHERE ExternalSystemEntityId = @QDMInsurerId AND PracticeRepositoryEntityId = @InsurerId

DECLARE @OID varchar(MAX)

SELECT @OID = OID 
FROM qdm.ValueSets
WHERE QDMCategory = 'Patient Characteristic Payer'

DECLARE @InsurerCategoryId INT
SELECT @InsurerCategoryId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClaimFilingIndicatorCode'

IF NOT EXISTS(
	SELECT * FROM model.ExternalSystemEntities
	WHERE ExternalSystemId = @QDMId 
	AND Name = 'PatientCharacteristicPayerPayer'
)
INSERT INTO model.ExternalSystemEntities (ExternalSystemId, Name) VALUES
(@QDMId, 'PatientCharacteristicPayerPayer')

DECLARE @QDMClaimFilingIndicatorCodeId INT
SELECT @QDMClaimFilingIndicatorCodeId = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @QDMId AND Name = 'PatientCharacteristicPayerPayer'

DECLARE @CodeId INT

SELECT  @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '1'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey IN (17, 18)
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
SELECT @QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 17 UNION ALL
SELECT @QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 18

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '111'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey =7
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 7)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '2'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey =19
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 19)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '3'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey IN (14, 20)
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
SELECT @QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 14 UNION ALL
SELECT @QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 20

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '31'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 11
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 11)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '32'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 22
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 22)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '341'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 21
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 21)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '36'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 2
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 2)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '5'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 12
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 12)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '511'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 15
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 15)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '512'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 3
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 3)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '513'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 4
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 4)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '514'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 5
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 5)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '52'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 6
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 6)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '6'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 10
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 10)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '9'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 24
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 24)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '92'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey IN (1, 16)
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) 
SELECT @QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 1 UNION ALL
SELECT @QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 16

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '93'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 13
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 13)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '95'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 23
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 23)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '96'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 9
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 9)

SELECT @CodeId = Id FROM qdm.MappingSets
WHERE [value set oid]= @OID
AND Code = '99'

IF NOT EXISTS( 
SELECT * FROM model.ExternalSystemEntityMappings 
WHERE ExternalSystemEntityId = @QDMClaimFilingIndicatorCodeId 
AND ExternalSystemEntityKey = @CodeId 
AND PracticeRepositoryEntityId = @InsurerCategoryId
AND PracticeRepositoryEntityKey = 8
)
INSERT INTO model.ExternalSystemEntityMappings (ExternalSystemEntityId, ExternalSystemEntityKey, PracticeRepositoryEntityId, PracticeRepositoryEntityKey) VALUES
(@QDMClaimFilingIndicatorCodeId, @CodeId, @InsurerCategoryId, 8)

GO

ALTER INDEX ALL ON model.ExternalSystemEntityMappings REBUILD WITH (FILLFACTOR = 90)

GO