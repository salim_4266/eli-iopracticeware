﻿IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit

SELECT 1 AS Value INTO #NoAudit

IF OBJECT_ID('model.PaymentMethods') IS NOT NULL
	EXEC dbo.DropObject 'model.PaymentMethods'
GO

-- 'PaymentMethods' does not exist, creating...
CREATE TABLE [model].[PaymentMethods] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IsArchived] bit  NOT NULL,
	IsReferenceRequired bit NOT NULL,
	LegacyPracticeCodeTableId INT NULL,
	LegacyPracticeCodeTablePaymentRefType NVARCHAR(1) NULL,
	LegacyDisplayEPaymentTypes BIT NULL
);

-- Creating primary key on [Id] in table 'PaymentMethods'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PaymentMethods', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PaymentMethods] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PaymentMethods', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PaymentMethods]
ADD CONSTRAINT [PK_PaymentMethods]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

INSERT INTO model.PaymentMethods (Name
	,IsArchived
	,LegacyPracticeCodeTableId
	,LegacyPracticeCodeTablePaymentRefType
	,LegacyDisplayEPaymentTypes
	,IsReferenceRequired
)
SELECT 
SUBSTRING(Code, 1, CHARINDEX(' - ', Code)) AS [Name]
,CONVERT(BIT, 0) AS IsArchived
,Id AS LegacyPracticeCodeTableId 
,SUBSTRING(Code, LEN(Code), 1) AS LegacyPracticeCodeTablePaymentRefType
,CASE WHEN AlternateCode = 'IncludeOnStatement' THEN 1 ELSE 0 END AS LegacyDisplayEPaymentTypes
,CASE WHEN SUBSTRING(Code, 1, CHARINDEX(' - ', Code)) = 'Cash' THEN CONVERT(BIT,0)  ELSE CONVERT(BIT,1) END AS IsReferenceRequired 
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'PAYABLETYPE'
GO

UPDATE a
SET a.PaymentMethodId = p.Id
FROM model.Adjustments a
JOIN model.PaymentMethods p ON p.LegacyPracticeCodeTableId = a.PaymentMethodId
GO

IF EXISTS(SELECT * FROM model.adjustments
			WHERE PaymentMethodId NOT IN (SELECT Id FROM model.PaymentMethods))
BEGIN
	IF NOT EXISTS (SELECT Id FROM model.PaymentMethods WHERE Name = 'Deleted PaymentMethod')
	BEGIN
		INSERT INTO model.PaymentMethods (Name, IsArchived, IsReferenceRequired)
		VALUES ('Deleted PaymentMethod', CONVERT(bit, 1), CONVERT(bit, 0))
	END

UPDATE ad
SET PaymentMethodId = (SELECT TOP 1 Id FROM model.PaymentMethods WHERE Name = 'Deleted PaymentMethod')
FROM model.Adjustments ad
WHERE PaymentMethodId NOT IN (SELECT Id FROM model.PaymentMethods)
END

-- Creating foreign key on [PaymentMethodId] in table 'Adjustments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_AdjustmentPaymentMethod'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Adjustments] DROP CONSTRAINT FK_AdjustmentPaymentMethod

IF OBJECTPROPERTY(OBJECT_ID('[model].[PaymentMethods]'), 'IsUserTable') = 1
ALTER TABLE [model].[Adjustments]
ADD CONSTRAINT [FK_AdjustmentPaymentMethod]
    FOREIGN KEY ([PaymentMethodId])
    REFERENCES [model].[PaymentMethods]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AdjustmentPaymentMethod'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_AdjustmentPaymentMethod')
	DROP INDEX IX_FK_AdjustmentPaymentMethod ON [model].[Adjustments]
GO
CREATE INDEX [IX_FK_AdjustmentPaymentMethod]
ON [model].[Adjustments]
    ([PaymentMethodId]);
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PaymentMethodsInsert' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER PaymentMethodsInsert
GO

CREATE TRIGGER [dbo].[PaymentMethodsInsert] ON [dbo].[PracticeCodeTable]
AFTER INSERT
AS
BEGIN
	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'PAYABLETYPE'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		INSERT INTO model.PaymentMethods (
			[Name]
           ,[IsArchived]
		   ,LegacyDisplayEPaymentTypes
		   ,IsReferenceRequired
		)
		SELECT 
		SUBSTRING(Code, 1, CHARINDEX(' - ', Code)) AS [Name]
		,CONVERT(BIT, 0) AS IsArchived
		,CASE WHEN AlternateCode = 'IncludeOnStatement' THEN 1 ELSE 0 END AS LegacyDisplayEPaymentTypes
		,CASE WHEN SUBSTRING(Code, 1, CHARINDEX(' - ', Code)) = 'Cash' THEN CONVERT(BIT,0)  ELSE CONVERT(BIT,1) END AS IsReferenceRequired
		FROM #inserted
	END
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PaymentMethodsUpdate' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER PaymentMethodsUpdate
GO

CREATE TRIGGER [dbo].[PaymentMethodsUpdate] ON [dbo].[PracticeCodeTable]
AFTER UPDATE
AS
BEGIN
	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'PAYABLETYPE'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		UPDATE p
		SET p.Name = SUBSTRING(i.Code, 1, CHARINDEX(' - ', i.Code))
			,p.LegacyDisplayEPaymentTypes = CASE WHEN i.AlternateCode = 'IncludeOnStatement' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
			,p.IsReferenceRequired = CASE WHEN SUBSTRING(i.Code, 1, CHARINDEX(' - ', i.Code)) = 'Cash' THEN CONVERT(BIT,0)  ELSE CONVERT(BIT,1) END 
		FROM model.PaymentMethods p
		JOIN deleted d ON SUBSTRING(d.Code, 1, CHARINDEX(' - ', d.Code)) = p.Name
		JOIN #inserted i ON i.Id = d.Id
	END
END
GO

IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit