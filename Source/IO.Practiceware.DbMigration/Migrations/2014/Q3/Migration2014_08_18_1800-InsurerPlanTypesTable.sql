﻿IF OBJECT_ID('model.InsurerPlanTypes') IS NOT NULL
	EXEC dbo.DropObject 'model.InsurerPlanTypes'
GO

-- 'InsurerPlanTypes' does not exist, creating...
CREATE TABLE [model].[InsurerPlanTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
	[LegacyPracticeCodeTableId] int null
);
GO

-- Creating primary key on [Id] in table 'InsurerPlanTypes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.InsurerPlanTypes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[InsurerPlanTypes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.InsurerPlanTypes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[InsurerPlanTypes]
ADD CONSTRAINT [PK_InsurerPlanTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

INSERT INTO [model].[InsurerPlanTypes] (
	[Name],
	[LegacyPracticeCodeTableId]
)
SELECT 
Code AS [Name]
,Id AS LegacyPracticeCodeTableId
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'PLANTYPE'
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.Insurers'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE i
SET i.InsurerPlanTypeId = pt.Id
FROM model.Insurers i
JOIN model.InsurerPlanTypes pt ON pt.LegacyPracticeCodeTableId = i.InsurerPlanTypeId
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [InsurerPlanTypeId] in table 'Insurers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InsurerPlanTypeInsurer'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Insurers] DROP CONSTRAINT FK_InsurerPlanTypeInsurer

IF OBJECTPROPERTY(OBJECT_ID('[model].[InsurerPlanTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[Insurers]
ADD CONSTRAINT [FK_InsurerPlanTypeInsurer]
    FOREIGN KEY ([InsurerPlanTypeId])
    REFERENCES [model].[InsurerPlanTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerPlanTypeInsurer'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InsurerPlanTypeInsurer')
	DROP INDEX IX_FK_InsurerPlanTypeInsurer ON [model].[Insurers]
GO
CREATE INDEX [IX_FK_InsurerPlanTypeInsurer]
ON [model].[Insurers]
    ([InsurerPlanTypeId]);
GO