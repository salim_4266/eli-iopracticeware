﻿ALTER TABLE model.[InsurerPhoneNumbers] DROP CONSTRAINT FK_InsurerPhoneNumberTypeInsurerPhoneNumber

UPDATE Model.InsurerPhoneNumbers 
SET InsurerPhoneNumberTypeId =
	CASE pnt.Name
		WHEN 'Main'
			THEN 1
		WHEN 'Authorization'
			THEN 2
		WHEN  'Eligibility'
			THEN 3
		WHEN  'Provider'
			THEN 4
		WHEN  'Claims'
			THEN 5
		WHEN  'Submissions'
			THEN 6
		END
FROM model.InsurerPhoneNumbers pn
INNER JOIN model.InsurerPhoneNumberTypes pnt ON pn.InsurerPhoneNumberTypeId = pnt.Id

SET IDENTITY_INSERT model.InsurerPhoneNumberTypes ON

IF NOT EXISTS (SELECT Id FROM model.InsurerPhoneNumberTypes WHERE Id = 1)
INSERT INTO model.InsurerPhoneNumberTypes (Id, Name, Abbreviation, IsArchived) VALUES
(1,'Main','',0)
ELSE 
UPDATE model.InsurerPhoneNumberTypes SET Name = 'Main', Abbreviation = '', IsArchived = 0 WHERE Id = 1

IF NOT EXISTS (SELECT Id FROM model.InsurerPhoneNumberTypes WHERE Id = 2)
INSERT INTO model.InsurerPhoneNumberTypes (Id, Name, Abbreviation, IsArchived) VALUES
(2,'Authorization','',0)
ELSE 
UPDATE model.InsurerPhoneNumberTypes SET Name = 'Authorization', Abbreviation = '', IsArchived = 0 WHERE Id = 2

IF NOT EXISTS (SELECT Id FROM model.InsurerPhoneNumberTypes WHERE Id = 3)
INSERT INTO model.InsurerPhoneNumberTypes (Id, Name, Abbreviation, IsArchived) VALUES
(3,'Eligibility','',0)
ELSE 
UPDATE model.InsurerPhoneNumberTypes SET Name = 'Eligibility', Abbreviation = '', IsArchived = 0 WHERE Id = 3

IF NOT EXISTS (SELECT Id FROM model.InsurerPhoneNumberTypes WHERE Id = 4)
INSERT INTO model.InsurerPhoneNumberTypes (Id, Name, Abbreviation, IsArchived) VALUES
(4,'Provider','',0)
ELSE 
UPDATE model.InsurerPhoneNumberTypes SET Name = 'Provider', Abbreviation = '', IsArchived = 0 WHERE Id = 4

IF NOT EXISTS (SELECT Id FROM model.InsurerPhoneNumberTypes WHERE Id = 5)
INSERT INTO model.InsurerPhoneNumberTypes (Id, Name, Abbreviation, IsArchived) VALUES
(5,'Claims','',0)
ELSE 
UPDATE model.InsurerPhoneNumberTypes SET Name = 'Claims', Abbreviation = '', IsArchived = 0 WHERE Id = 5

IF NOT EXISTS (SELECT Id FROM model.InsurerPhoneNumberTypes WHERE Id = 6)
INSERT INTO model.InsurerPhoneNumberTypes (Id, Name, Abbreviation, IsArchived) VALUES
(6,'Submissions','',0)
ELSE 
UPDATE model.InsurerPhoneNumberTypes SET Name = 'Submissions', Abbreviation = '', IsArchived = 0 WHERE Id = 6

DELETE FROM model.InsurerPhoneNumberTypes WHERE Id >= 7

ALTER TABLE model.InsurerPhoneNumbers WITH CHECK ADD
CONSTRAINT FK_InsurerPhoneNumberTypeInsurerPhoneNumber FOREIGN KEY (InsurerPhoneNumberTypeId) REFERENCES model.InsurerPhoneNumberTypes (Id)

SET IDENTITY_INSERT model.InsurerPhoneNumberTypes OFF

DBCC CHECKIDENT ('[model].[InsurerPhoneNumberTypes]', 'RESEED', 1000)

