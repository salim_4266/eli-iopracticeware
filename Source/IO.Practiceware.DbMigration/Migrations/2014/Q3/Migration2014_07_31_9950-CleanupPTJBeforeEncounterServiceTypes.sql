﻿IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'dbo.PracticeCodeTable'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

-- Make sure model.EncounterServiceTypes is not a yet a table.
IF OBJECT_ID('model.EncounterServiceTypes','U') IS NULL
BEGIN

	-- PracticeCodeTable
	;WITH CTE AS (
	SELECT 
	ROW_NUMBER() OVER (PARTITION BY pct.Code ORDER BY Id ASC) AS [Count]
	,* 
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'SERVICECATEGORY'
		AND pct.Code IN (
			SELECT
			Code AS [Name]
			FROM dbo.PracticeCodeTable
			WHERE ReferenceType = 'SERVICECATEGORY'
			GROUP BY Code HAVING COUNT(Code) > 1
		)
	)
	DELETE FROM PracticeCodeTable 
	WHERE Id IN (
		SELECT Id
		FROM CTE 
		WHERE [Count] <> 1
	)
END
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO