﻿IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit

SELECT 1 AS VALUE INTO #NoAudit

SELECT  Name, COUNT(*) AS Count
INTO #DuplicateLanguages
FROM model.Languages
GROUP BY Name
HAVING COUNT(*) > 1

SELECT ROW_NUMBER() OVER (PARTITION BY Name ORDER BY OrdinalId) AS Rank, Id, NAME
INTO #OrderedLanguages
FROM model.Languages
WHERE Name IN (SELECT NAME FROM #DuplicateLanguages)

SELECT * 
INTO #DeletedLanguages
FROM #OrderedLanguages
WHERE Rank = 2


UPDATE p
SET LanguageId = ol.Id
FROM model.Patients p
INNER JOIN model.Languages l ON l.ID = p.LanguageId
INNER JOIN #OrderedLanguages ol ON ol.Name = l.Name
	AND Rank = 1
WHERE LanguageId IN (SELECT Id FROM #DeletedLanguages)

DELETE FROM 
model.Languages WHERE Id IN (SELECT Id FROM #DeletedLanguages)