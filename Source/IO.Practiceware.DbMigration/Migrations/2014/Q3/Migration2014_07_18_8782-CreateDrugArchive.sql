﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE name = 'DrugArchive')
BEGIN
	
CREATE TABLE [dbo].[DrugArchive](
	[DrugId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Drugcode] [nvarchar](6) NULL,
	[DisplayName] [nvarchar](128) NULL,
	[Family1] [nvarchar](64) NULL,
	[Family2] [nvarchar](64) NULL,
	[Focus] [bit] NULL,
	[DrugDosageFormId] [int] NULL
 CONSTRAINT [PK_DrugArchive] PRIMARY KEY CLUSTERED 
(
	[DrugId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
		
END

INSERT INTO dbo.DrugArchive ([Drugcode]
      ,[DisplayName]
      ,[Family1]
      ,[Family2]
      ,[Focus]
      ,[DrugDosageFormId])

SELECT DISTINCT 'F' AS DrugCode
	,SUBSTRING(pc.FindingDetail, CHARINDEX('RX-8/', pc.FindingDetail) + 5,(CHARINDEX('-2/',pc.FindingDetail) - CHARINDEX('RX-8/', pc.FindingDetail) - 5)) AS DisplayName
	,'' AS Family1
	,'' AS Family2
	,0 AS Focus
	,NULL AS DrugDosageFormId
	FROM patientclinical pc
	WHERE clinicaltype = 'A'
	AND findingdetail like 'RX%'
	AND SUBSTRING(pc.FindingDetail, CHARINDEX('RX-8/', pc.FindingDetail) + 5,(CHARINDEX('-2/',pc.FindingDetail) - CHARINDEX('RX-8/', pc.FindingDetail) - 5))
		NOT IN (SELECT DisplayName FROM dbo.Drug)
	AND SUBSTRING(pc.FindingDetail, CHARINDEX('RX-8/', pc.FindingDetail) + 5,(CHARINDEX('-2/',pc.FindingDetail) - CHARINDEX('RX-8/', pc.FindingDetail) - 5))
		NOT IN (SELECT DisplayName FROM dbo.DrugArchive)
	AND pc.DrawFileName = '' AND pc.DrawFileName IS NOT NULL
	AND LEN(SUBSTRING(pc.FindingDetail, CHARINDEX('RX-8/', pc.FindingDetail) + 5,(CHARINDEX('-2/',pc.FindingDetail)))) > 0