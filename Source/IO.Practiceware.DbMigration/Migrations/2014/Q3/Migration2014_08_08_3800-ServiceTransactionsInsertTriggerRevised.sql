﻿ALTER TRIGGER [dbo].[ServiceTransactionsInsert] ON [dbo].[ServiceTransactions]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * INTO #inserted FROM inserted i WHERE i.Amount >= 0

	DECLARE @InsertedBillingServiceTransactions TABLE (LegacyTransactionId INT, AmountSent DECIMAL(18,2))

	INSERT INTO model.BillingServiceTransactions(
	[DateTime]
	,AmountSent
	,ClaimFileNumber
	,BillingServiceTransactionStatusId
	,MethodSentId
	,BillingServiceId
	,InvoiceReceivableId
	,ClaimFileReceiverId
	,ExternalSystemMessageId
	,PatientAggregateStatementId
	,LegacyTransactionId)
	OUTPUT inserted.LegacyTransactionId, inserted.AmountSent INTO @InsertedBillingServiceTransactions(LegacyTransactionId,AmountSent)
	SELECT
	DATEADD(mi,TransactionTime,(CONVERT(DATETIME,ptj.TransactionDate))) AS [DateTime]
	,i.Amount AS AmountSent
	,NULL AS ClaimFileNumber
	,CASE 
		WHEN ptj.TransactionStatus = 'P'
			AND i.TransportAction IN ('B', 'P')
			THEN 1
		WHEN ptj.TransactionStatus = 'P'
			AND i.TransportAction = 'V'
			THEN 3
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'P'
			THEN 2
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'V'
			THEN 3
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'X'
			THEN 4
		WHEN ptj.TransactionStatus = 'Z'
			AND i.TransportAction = '1'
			THEN 6
		WHEN ptj.TransactionStatus = 'Z'
			AND i.TransportAction = '2'
			THEN 7
		ELSE 5
	END AS BillingServiceTransactionStatusId
	,CASE 
		WHEN (ptj.TransactionRef = 'I' AND ptj.TransactionAction = 'B')
			THEN 1
		ELSE 2 
	END AS MethodSentId
	,i.ServiceId AS BillingServiceId
	,CASE 
		WHEN ptj.TransactionRef = 'P'
			THEN irPatient.Id
		ELSE ptj.TransactionTypeId
	END AS InvoiceReceivableId
	,CASE 
		WHEN ptj.TransactionRef = 'I' AND ptj.TransactionAction = 'B' 
			THEN pctMethod.Id 
		ELSE NULL 
	END AS ClaimFileReceiverId
	,CONVERT(UNIQUEIDENTIFIER,i.ExternalSystemMessageId) AS ExternalSystemMessageId
	,NULL AS PatientAggregateStatementId
	,i.TransactionId AS LegacyTransactionId
	FROM #inserted i 
	INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionId = i.TransactionId
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = ptj.TransactionTypeId
	INNER JOIN model.InvoiceReceivables irPatient ON irPatient.InvoiceId = ir.InvoiceId
		AND irPatient.PatientInsuranceId IS NULL
	LEFT JOIN model.PatientInsurances pi ON ir.PatientInsuranceId = pi.Id
	LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	LEFT JOIN dbo.PracticeInsurers pri on pri.InsurerId = ip.InsurerId
	LEFT JOIN dbo.PracticeCodeTable pctMethod ON pri.InsurerTFormat = SUBSTRING(pctMethod.Code, 1, 1) 
		AND pctMethod.ReferenceType = 'TRANSMITTYPE'
	WHERE i.Amount > 0

	UNION ALL

	SELECT 
	DATEADD(mi,TransactionTime,(CONVERT(DATETIME,ptj.TransactionDate))) AS [DateTime]
	,i.Amount AS AmountSent
	,NULL AS ClaimFileNumber
	,CASE 
		WHEN ptj.TransactionStatus = 'P'
			AND i.TransportAction IN ('B', 'P')
			THEN 1
		WHEN ptj.TransactionStatus = 'P'
			AND i.TransportAction = 'V'
			THEN 3
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'P'
			THEN 2
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'V'
			THEN 3
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'X'
			THEN 4
		WHEN ptj.TransactionStatus = 'Z'
			AND i.TransportAction = '1'
			THEN 6
		WHEN ptj.TransactionStatus = 'Z'
			AND i.TransportAction = '2'
			THEN 7
		ELSE 5
	END AS BillingServiceTransactionStatusId
	,CASE 
		WHEN (ptj.TransactionRef = 'I' AND ptj.TransactionAction = 'B')
			THEN 1
		ELSE 2 
	END AS MethodSentId
	,i.ServiceId AS BillingServiceId
	,ptj.TransactionTypeId AS InvoiceReceivableId
	,CASE 
		WHEN ptj.TransactionRef = 'I' AND ptj.TransactionAction = 'B' 
			THEN pctMethod.Id 
		ELSE NULL 
	END AS ClaimFileReceiverId
	,CONVERT(UNIQUEIDENTIFIER,i.ExternalSystemMessageId) AS ExternalSystemMessageId
	,NULL AS PatientAggregateStatementId
	,i.TransactionId AS LegacyTransactionId
	FROM #inserted i
	INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionId = i.TransactionId
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = ptj.TransactionTypeId
		AND ir.PatientInsuranceId IS NOT NULL
	INNER JOIN model.Invoices inv ON inv.Id = ir.InvoiceId
	LEFT JOIN model.BillingServices bs ON bs.InvoiceId = inv.Id
		AND bs.Id = i.ServiceId
	LEFT JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
		AND es.IsZeroCharge = 0
	LEFT JOIN model.PatientInsurances pi ON ir.PatientInsuranceId = pi.Id
	LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	LEFT JOIN dbo.PracticeInsurers pri on pri.InsurerId = ip.InsurerId
		AND pri.InsurerServiceFilter <> 'N'
	LEFT JOIN dbo.PracticeCodeTable pctMethod ON pri.InsurerTFormat = SUBSTRING(pctMethod.Code, 1, 1) 
		AND pctMethod.ReferenceType = 'TRANSMITTYPE'
	WHERE i.Amount = 0
END
GO