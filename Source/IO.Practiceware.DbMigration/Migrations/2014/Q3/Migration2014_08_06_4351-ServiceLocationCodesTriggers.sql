﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ServiceLocationCodesInsert' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER ServiceLocationCodesInsert
GO

CREATE TRIGGER [dbo].[ServiceLocationCodesInsert] ON [dbo].[PracticeCodeTable]
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'PLACEOFSERVICE'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		INSERT INTO model.ServiceLocationCodes (
			PlaceOfServiceCode
			,Name
			,LegacyPracticeCodeTableId
		)
		SELECT
		SUBSTRING(Code,1,2) AS PlaceOfServiceCode
		,SUBSTRING(Code,CHARINDEX('-',Code),LEN(Code)) AS Name
		,Id AS LegacyPracticeCodeTableId
		FROM #inserted
	END
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ServiceLocationCodesUpdate' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER ServiceLocationCodesUpdate
GO

CREATE TRIGGER [dbo].[ServiceLocationCodesUpdate] ON [dbo].[PracticeCodeTable]
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'SERVICECATEGORY'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		UPDATE slc
		SET slc.PlaceOfServiceCode = SUBSTRING(i.Code,1,2)
			,slc.Name = SUBSTRING(i.Code,CHARINDEX('-',i.Code),LEN(i.Code))
			,slc.LegacyPracticeCodeTableId = i.Id
		FROM model.ServiceLocationCodes slc
		JOIN deleted d ON d.Id = slc.LegacyPracticeCodeTableId
		JOIN #inserted i ON i.Id = d.Id
	END
END
GO