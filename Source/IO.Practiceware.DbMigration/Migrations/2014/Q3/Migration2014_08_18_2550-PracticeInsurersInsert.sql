﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PracticeInsurersInsert' AND parent_id = OBJECT_ID('dbo.PracticeInsurers','V'))
	DROP TRIGGER PracticeInsurersInsert
GO
	
CREATE TRIGGER PracticeInsurersInsert ON dbo.PracticeInsurers
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO model.Insurers ([Name], [GroupName], [PlanName], [IsReferralRequired], [PayerCode],
    [MedigapCode], [AllowDependents], [IsSecondaryClaimPaper], [PolicyNumberFormat], [JurisdictionStateOrProvinceId],
    [SendZeroCharge], [InsurerPayTypeId], [InsurerBusinessClassId], [InsurerPlanTypeId], [ClaimFileReceiverId],
	[IsArchived], [IsMedicareAdvantage], [Hpid], [Oeid], [Website], [Comment], [ClaimFilingIndicatorCodeId],
    [PriorInsurerCode],	[LegacyPracticeInsurersId])
	SELECT 
	InsurerName AS Name
	,CASE WHEN InsurerGroupName <> '' AND InsurerGroupName IS NOT NULL THEN InsurerGroupName ELSE NULL END AS GroupName
	,CASE WHEN InsurerPlanName <> '' AND InsurerPlanName IS NOT NULL THEN InsurerPlanName ELSE NULL END AS PlanName
	,CASE ReferralRequired WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsReferralRequired
	,CASE WHEN NeicNumber <> '' AND NeicNumber IS NOT NULL THEN NeicNumber ELSE NULL END AS PayerCode
	,CASE WHEN MedicareCrossOverId <> '' AND MedicareCrossOverId IS NOT NULL THEN MedicareCrossOverId ELSE NULL END MedigapCode
	,CASE WHEN AllowDependents <> '' AND AllowDependents IS NOT NULL THEN CASE WHEN AllowDependents = CONVERT(BIT, 1) THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END ELSE CONVERT(BIT, 0) END AS AllowDependents
	,CASE [Availability] WHEN 'A' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsSecondaryClaimPaper
	,CASE WHEN InsurerPlanFormat <> '' AND InsurerPlanFormat IS NOT NULL THEN InsurerPlanFormat END AS PolicyNumberFormat
	,st.Id AS JurisdictionStateorProvinceId
	,CASE InsurerServiceFilter WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS SendZeroCharge
	,CASE pri.InsurerPayType
		WHEN 'SP' THEN 1
		WHEN 'OT' THEN 2
		WHEN 'LT' THEN 3
		WHEN 'IP' THEN 4
		WHEN 'GP' THEN 5
		WHEN 'PP' THEN 6
		WHEN 'MG' THEN 7
		WHEN 'LD' THEN 8
		WHEN 'AP' THEN 9
	END AS InsurerPayTypeId
	,ibc.Id AS InsurerBusinessClassId
	,ips.Id AS InsurerPlanTypeId
	,cfr.Id AS ClaimFileReceiverId
	,CASE WHEN pri.IsArchived <> '' AND pri.IsArchived IS NOT NULL THEN CASE WHEN pri.IsArchived = CONVERT(BIT, 1) THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END ELSE CONVERT(BIT, 0) END AS IsArchived
	,CASE WHEN IsMedicareAdvantage = 1 THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END AS IsMedicareAdvantage
	,Hpid AS Hpid
	,Oeid AS Oeid
	,Website AS Website
	,CASE WHEN InsurerComment <> '' AND InsurerComment IS NOT NULL THEN CONVERT(NVARCHAR(MAX), InsurerComment) END AS Comment
	,CASE 
		WHEN InsurerReferenceCode = 'P'
			THEN 10
		WHEN InsurerReferenceCode = 'K'
			THEN 1
		WHEN InsurerReferenceCode IN ('X', 'G', 'Q', 'L', 'A', 'U', 'O', '', NULL)
			THEN 12
		WHEN InsurerReferenceCode = 'F'
			THEN 14
		WHEN InsurerReferenceCode = 'I'
			THEN 7
		WHEN InsurerReferenceCode = 'C'
			THEN 19
		WHEN InsurerReferenceCode IN ('N', 'M', '[')
			THEN 18
		WHEN InsurerReferenceCode = 'Z'
			THEN 24
		WHEN InsurerReferenceCode = 'Y'
			THEN 20
		WHEN InsurerReferenceCode = 'T'
			THEN 21
		WHEN InsurerReferenceCode = 'H'
			THEN 11
		WHEN InsurerReferenceCode = 'V'
			THEN 22
		WHEN InsurerReferenceCode = 'W'
			THEN 23
		ELSE 2
	END AS ClaimFilingIndicatorCodeId
	,CASE WHEN InsurerProvPhone <> '' AND InsurerProvPhone IS NOT NULL THEN InsurerProvPhone END AS PriorInsurerCode 
	,NULL AS LegacyPracticeInsurersId
	FROM inserted pri
	LEFT JOIN dbo.PracticeCodeTable pctBusClass ON pctBusClass.Code = pri.InsurerBusinessClass
		AND pctBusClass.ReferenceType = 'BUSINESSCLASS'
	LEFT JOIN model.InsurerBusinessClasses ibc ON ibc.LegacyPracticeCodeTableId = pctBusClass.Id

	LEFT JOIN dbo.PracticeCodeTable pctClaimReceiver ON SUBSTRING(pctClaimReceiver.Code, 1, 1) = pri.InsurerTFormat
		AND pctClaimReceiver.ReferenceType = 'TRANSMITTYPE'
	LEFT JOIN model.ClaimFileReceivers cfr ON cfr.LegacyPracticeCodeTableId = pctClaimReceiver.Id

	LEFT JOIN dbo.PracticeCodeTable pctPlanType ON pctPlanType.Code = pri.InsurerPlanType
		AND pctPlanType.ReferenceType = 'PLANTYPE'
	LEFT JOIN model.InsurerPlanTypes ips ON ips.LegacyPracticeCodeTableId = pctPlanType.Id

	LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = pri.StateJurisdiction
	WHERE InsurerName NOT IN ('ZZZ ON ACCOUNT INSURANCE', 'ZZZ No insurance')

	IF (@@ROWCOUNT > 0)
	BEGIN
		DECLARE @InsertedId INT
		SELECT @InsertedId = SCOPE_IDENTITY()

		IF OBJECT_ID('dbo.ViewIdentities','U') IS NULL
		BEGIN	
			CREATE TABLE [dbo].[ViewIdentities](
			[Name] [nvarchar](max) NULL,
			[Value] [bigint] NULL
			)
		END

		IF OBJECT_ID('tempdb..#TempTableToHoldTheScopeIdentity') IS NOT NULL
			DROP TABLE #TempTableToHoldTheScopeIdentity

		CREATE TABLE #TempTableToHoldTheScopeIdentity (Id BIGINT IDENTITY(1,1) NOT NULL)
		SET IDENTITY_INSERT #TempTableToHoldTheScopeIdentity ON
		INSERT INTO #TempTableToHoldTheScopeIdentity (Id) SELECT @InsertedId AS SCOPE_ID_COLUMN
		SET IDENTITY_INSERT #TempTableToHoldTheScopeIdentity OFF

		IF NOT EXISTS(SELECT * FROM ViewIdentities WHERE Name = 'dbo.PracticeInsurers')
			INSERT ViewIdentities(Name, Value) VALUES ('dbo.PracticeInsurers', @@IDENTITY)
		ELSE
			UPDATE ViewIdentities SET Value = @@IDENTITY WHERE Name = 'dbo.PracticeInsurers'

		SELECT @@IDENTITY AS SCOPE_ID_COLUMN
	END
END
GO
