﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'EncounterServiceTypesInsert' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER EncounterServiceTypesInsert
GO

CREATE TRIGGER [dbo].[EncounterServiceTypesInsert] ON [dbo].[PracticeCodeTable]
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'SERVICECATEGORY'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		INSERT INTO model.EncounterServiceTypes (
			Name
			,IsArchived
		)
		SELECT
		Code
		,CONVERT(BIT,0) AS IsArchived
		FROM #inserted
	END
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'EncounterServiceTypesUpdate' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER EncounterServiceTypesUpdate
GO

CREATE TRIGGER [dbo].[EncounterServiceTypesUpdate] ON [dbo].[PracticeCodeTable]
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'SERVICECATEGORY'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		UPDATE est
		SET est.Name = i.Code
		FROM model.EncounterServiceTypes est
		JOIN deleted d ON d.Code = est.Name
		JOIN #inserted i ON i.Id = d.Id
	END
END
GO