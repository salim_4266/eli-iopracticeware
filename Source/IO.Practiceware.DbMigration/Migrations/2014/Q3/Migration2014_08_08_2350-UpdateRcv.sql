﻿IF OBJECT_ID ( 'dbo.UpdateRcv', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.UpdateRcv;
GO

CREATE PROCEDURE dbo.UpdateRcv
	@ReceivableId NVARCHAR(100)
AS
SET NOCOUNT ON;

SELECT 
SUM(bs.Unit * bs.UnitCharge)
FROM model.InvoiceReceivables ir
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.EncounterId
INNER JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
WHERE ir.Id = @ReceivableId

GO
