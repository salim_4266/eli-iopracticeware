﻿IF OBJECT_ID('PatientInsuranceReferralsUpdate') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientInsuranceReferralsUpdate')
END
GO

CREATE TRIGGER dbo.PatientInsuranceReferralsUpdate
ON dbo.Appointments
AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	IF UPDATE(ReferralId)
	BEGIN
		;WITH CTE AS (
		SELECT 
		i.AppointmentId
		,i.ReferralId
		FROM inserted i 
		WHERE i.Comments <> 'ASC CLAIM'
		EXCEPT
		SELECT
		d.AppointmentId
		,d.ReferralId
		FROM deleted d
		WHERE d.Comments <> 'ASC CLAIM'
		)
		UPDATE ir
		SET ir.PatientInsuranceReferralId = a.ReferralId
		FROM model.InvoiceReceivables ir 
		INNER JOIN model.Invoices i ON ir.InvoiceId = i.Id
		INNER JOIN dbo.Appointments a ON a.AppointmentId = i.EncounterId
		INNER JOIN CTE c ON c.AppointmentId = a.AppointmentId
		INNER JOIN model.PatientInsuranceReferrals pir ON pir.PatientId = a.PatientId
		WHERE pir.PatientInsuranceId = ir.PatientInsuranceId		
			AND i.InvoiceTypeId IN (1,3)

		;WITH CTE AS (
		SELECT 
		i.AppointmentId
		,i.ReferralId
		FROM inserted i 
		WHERE i.Comments = 'ASC CLAIM'
		EXCEPT
		SELECT
		d.AppointmentId
		,d.ReferralId
		FROM deleted d
		WHERE d.Comments = 'ASC CLAIM'
		)
		UPDATE ir
		SET ir.PatientInsuranceReferralId = a.ReferralId
		FROM model.InvoiceReceivables ir 
		INNER JOIN model.Invoices i ON ir.InvoiceId = i.Id
		INNER JOIN dbo.Appointments a ON a.AppointmentId = i.EncounterId
		INNER JOIN CTE c ON c.AppointmentId = a.AppointmentId
		INNER JOIN model.PatientInsuranceReferrals pir ON pir.PatientId=a.PatientId
		WHERE pir.PatientInsuranceId = ir.PatientInsuranceId
			AND i.InvoiceTypeId = 2 
	END
END
GO