﻿IF OBJECT_ID ( 'dbo.GetUnLinkedBills', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetUnLinkedBills;
GO

CREATE PROCEDURE dbo.GetUnLinkedBills
	@Invoice NVARCHAR(100)
AS
SET NOCOUNT ON;

SELECT DISTINCT 
ptj.TransactionId
,ptj.TransactionDate
,ptj.TransactionStatus
,ptj.TransactionRef
,ptj.transactionaction
,st.TransportAction
,st.Amount
,CASE ptj.TransactionRef
	WHEN 'I'
		THEN ins.Name
	WHEN 'P'
		THEN 'PATIENT'
	WHEN 'G'
		THEN 'MONTHLY STMT'
	ELSE ''
END AS InsurerName
FROM PracticeTransactionJournal ptj
INNER JOIN PatientReceivables pr ON pr.ReceivableId = ptj.TransactionTypeId
INNER JOIN model.Invoices i ON i.LegacyPatientReceivablesInvoice = pr.Invoice
INNER JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
LEFT OUTER JOIN model.Insurers ins ON ins.Id = pr.InsurerId
INNER JOIN ServiceTransactions st ON st.TransactionId = ptj.TransactionId
	AND st.ServiceId = bs.Id
WHERE ptj.TransactionType = 'R'
	AND pr.Invoice = @Invoice
ORDER BY ptj.TransactionDate
	,ptj.TransactionId

GO