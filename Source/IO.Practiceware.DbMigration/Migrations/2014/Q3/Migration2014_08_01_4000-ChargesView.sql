﻿IF OBJECT_ID('dbo.Charges','V') IS NOT NULL
	DROP VIEW dbo.Charges
GO

CREATE VIEW dbo.Charges
WITH SCHEMABINDING
AS
SELECT 
SUM(ISNULL(b.Unit,0) * ISNULL(b.UnitCharge,0)) AS Charge
,SUM(ISNULL(b.Unit,0) * ISNULL(b.UnitAllowableExpense,0)) AS ChargesByFee
,b.InvoiceId
,COUNT_BIG(*) AS [Count]
FROM model.BillingServices b
GROUP BY b.InvoiceId
GO

CREATE UNIQUE CLUSTERED INDEX [IX_Charges] ON [dbo].[Charges] ([InvoiceId] ASC)
GO