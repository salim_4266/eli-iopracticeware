﻿IF OBJECT_ID ( 'dbo.GetUnLinkedPayments', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetUnLinkedPayments;
GO

CREATE PROCEDURE [dbo].[GetUnLinkedPayments]
	@Invoice NVARCHAR(100)
AS
SET NOCOUNT ON;

DECLARE @AppointmentId INT
SELECT @AppointmentId = SUBSTRING(@Invoice, (CHARINDEX('-', @Invoice) + 1), LEN(@Invoice))
SELECT Id INTO #InvoiceReceivableIds FROM dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId)

DECLARE @AdjustmentIdMax INT
SET @AdjustmentIdMax = 11000000
DECLARE @FinancialIdMax INT
SET @FinancialIdMax = 11000000

IF OBJECT_ID('tempdb..#AdjustmentTypes') IS NOT NULL
BEGIN
	DROP TABLE #AdjustmentTypes
END

SELECT 
v.Id AS Id
,v.PaymentType
,v.IsDebit AS IsDebit
,v.ShortName AS ShortName
INTO #AdjustmentTypes
FROM (
	SELECT 
		CASE
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'P'
				THEN 1
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'X'
				THEN 2
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '8'
				THEN 3
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'R'
				THEN 4
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'U'
				THEN 5
			ELSE pct.Id + 1000
		END AS Id
		,SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS PaymentType
		,CASE pct.AlternateCode
			WHEN 'D'
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
		END AS IsDebit
		,LetterTranslation AS ShortName
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'
	GROUP BY 
	Id
	,Code
	,pct.AlternateCode
	,LetterTranslation
) AS v

IF OBJECT_ID('tempdb..#FinancialInformationTypes') IS NOT NULL
BEGIN
	DROP TABLE #FinancialInformationTypes
END

SELECT 
v.Id AS Id
,v.FinancialInformationType
,v.ShortName AS ShortName
,v.PaymentFinancialType
INTO #FinancialInformationTypes
FROM (
	SELECT 
	CASE
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '!'
			THEN 1
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '|'
			THEN 2
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '?'
			THEN 3
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '%'
			THEN 4
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'D'
			THEN 5
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '&'
			THEN 6
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = ']'
			THEN 7
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '0'
			THEN 8
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'V'
			THEN 9
		ELSE pct.Id + 1000
	END AS Id
	,SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS FinancialInformationType
	,AlternateCode AS PaymentFinancialType
	,LetterTranslation AS ShortName
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'		
		AND (pct.AlternateCode = '' OR pct.AlternateCode IS NULL)
	GROUP BY 
	pct.Id
	,pct.Code
	,LetterTranslation
	,AlternateCode
) AS v

SELECT
model.GetPairId(1, adj.Id, 11000000) AS PaymentId
,CASE
	WHEN adj.FinancialSourceTypeId = 2 
		THEN COALESCE(v.InvoiceReceivableId,adj.InvoiceReceivableId)
	ELSE adj.InvoiceReceivableId 
END AS ReceivableId
,CASE 
	WHEN (adj.FinancialSourceTypeId = 1)
		THEN adj.InsurerId
	WHEN (adj.FinancialSourceTypeId = 2)
		THEN adj.PatientId
	ELSE 0
END AS PayerId
,CASE adj.FinancialSourceTypeId
	WHEN 1
		THEN 'I'
	WHEN 2
		THEN 'P'
	WHEN 3
		THEN 'O'
END AS PayerType
,at.PaymentType AS PaymentType
,adj.Amount AS PaymentAmount
,CONVERT(NVARCHAR(8),adj.PostedDateTime,112) AS PaymentDate
,COALESCE(fb.CheckCode,'') AS PaymentCheck
,CASE at.IsDebit 
	WHEN 1
		THEN 'D'
	ELSE 'C'
END AS PaymentFinancialType
,COALESCE(pctRsnCode.Code,'') AS PaymentReason
,prs.Service AS PaymentService
,CASE 
	WHEN (adj.FinancialSourceTypeId = 1)
		THEN ins.Name
	WHEN (adj.FinancialSourceTypeId = 2)
		THEN 'PATIENT'
	ELSE 'OFFICE'
END AS InsurerName
,CASE 
	WHEN (adj.Comment = '' OR adj.Comment IS NULL)
		THEN ''
	ELSE 'N'
END AS NoteInd
,at.ShortName AS LetterTranslation
FROM model.Adjustments adj
INNER JOIN #AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
LEFT JOIN PatientReceivableServices prs ON prs.ItemId = adj.BillingServiceId
	AND (prs.[Status] = 'X' OR prs.[Status] IS NULL)
INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
INNER JOIN #InvoiceReceivableIds irs ON irs.Id = ir.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN dbo.Appointments ap ON ap.EncounterId = i.EncounterId
LEFT JOIN (
	SELECT
	ir.Id AS InvoiceReceivableId
	,MIN(pi1.OrdinalId) AS OrdinalId
	,ir.InvoiceId
	,ips.StartDateTime
	,pi1.EndDateTime
	,pi1.InsuranceTypeId
	FROM model.PatientInsurances pi1
	INNER JOIN (
		SELECT 
		InsuredPatientId
		,MIN(pi.OrdinalId) AS LowestOrdinalId
		,MIN(InsuranceTypeId) AS LowestInsuranceTypeId
		FROM model.PatientInsurances pi 
		INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
		WHERE (pi.EndDateTime IS NULL AND pi.IsDeleted <> 1)
		GROUP BY InsuredPatientId
	) pi2 ON pi1.InsuredPatientId = pi2.InsuredPatientId
		AND pi1.OrdinalId = pi2.LowestOrdinalId
		AND pi1.InsuranceTypeId = pi2.LowestInsuranceTypeId
	INNER JOIN model.InsurancePolicies ips ON ips.Id = pi1.InsurancePolicyId
	INNER JOIN model.InvoiceReceivables ir ON ir.PatientInsuranceId = pi1.Id
	WHERE pi1.IsDeleted <> 1
	GROUP BY
	ir.Id
	,ir.InvoiceId
	,ips.StartDateTime
	,pi1.EndDateTime
	,pi1.InsuranceTypeId
) v ON v.InvoiceId = ir.InvoiceId
	AND v.StartDateTime <= CONVERT(DATETIME,ap.AppDate)
	AND (v.EndDateTime IS NULL OR v.EndDateTime >= CONVERT(DATETIME,ap.AppDate))
LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
LEFT JOIN dbo.PracticeCodeTable pctRsnCode ON pctRsnCode.Id = adj.ClaimAdjustmentReasonCodeId 
	AND pctRsnCode.ReferenceType = 'PAYMENTREASON'
LEFT JOIN model.Insurers ins ON ins.Id = adj.InsurerId
WHERE adj.BillingServiceId IS NULL

UNION ALL

SELECT
model.GetPairId(2, fi.Id, @FinancialIdMax) AS PaymentId
,CASE
	WHEN fi.FinancialSourceTypeId = 2 
		THEN COALESCE(v.InvoiceReceivableId,fi.InvoiceReceivableId)
	ELSE fi.InvoiceReceivableId 
END AS ReceivableId
,CASE 
	WHEN (fi.FinancialSourceTypeId = 1)
		THEN fi.InsurerId
	WHEN (fi.FinancialSourceTypeId = 2)
		THEN fi.PatientId
	ELSE 0
END AS PayerId
,CASE fi.FinancialSourceTypeId
	WHEN 1
		THEN 'I'
	WHEN 2
		THEN 'P'
	WHEN 3
		THEN 'O'
END AS PayerType
,fit.FinancialInformationType AS PaymentType
,fi.Amount AS PaymentAmount
,CONVERT(NVARCHAR(8),fi.PostedDateTime,112) AS PaymentDate
,COALESCE(fb.CheckCode,'') AS PaymentCheck
,COALESCE(fit.PaymentFinancialType,'') AS PaymentFinancialType
,COALESCE(pctRsnCode.Code,'') AS PaymentReason
,prs.Service AS PaymentService
,CASE 
	WHEN (fi.FinancialSourceTypeId = 1)
		THEN ins.Name
	WHEN (fi.FinancialSourceTypeId = 2)
		THEN 'PATIENT'
	ELSE 'OFFICE'
END AS InsurerName
,CASE 
	WHEN (fi.Comment = '' OR fi.Comment IS NULL)
		THEN ''
	ELSE 'N'
END AS NoteInd
,fit.ShortName AS LetterTranslation
FROM model.FinancialInformations fi
INNER JOIN #FinancialInformationTypes fit ON fit.Id = fi.FinancialInformationTypeId
LEFT JOIN PatientReceivableServices prs ON prs.ItemId = fi.BillingServiceId
	AND (prs.[Status] = 'X' OR prs.[Status] IS NULL)
INNER JOIN model.InvoiceReceivables ir ON ir.Id = fi.InvoiceReceivableId
INNER JOIN #InvoiceReceivableIds irs ON irs.Id = ir.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN dbo.Appointments ap ON ap.EncounterId = i.EncounterId
LEFT JOIN (
	SELECT
	ir.Id AS InvoiceReceivableId
	,MIN(pi1.OrdinalId) AS OrdinalId
	,ir.InvoiceId
	,ips.StartDateTime
	,pi1.EndDateTime
	,pi1.InsuranceTypeId
	FROM model.PatientInsurances pi1
	INNER JOIN (
		SELECT 
		InsuredPatientId
		,MIN(pi.OrdinalId) AS LowestOrdinalId
		,MIN(InsuranceTypeId) AS LowestInsuranceTypeId
		FROM model.PatientInsurances pi 
		INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
		WHERE (pi.EndDateTime IS NULL AND pi.IsDeleted <> 1)
		GROUP BY InsuredPatientId
	) pi2 ON pi1.InsuredPatientId = pi2.InsuredPatientId
		AND pi1.OrdinalId = pi2.LowestOrdinalId
		AND pi1.InsuranceTypeId = pi2.LowestInsuranceTypeId
	INNER JOIN model.InsurancePolicies ips ON ips.Id = pi1.InsurancePolicyId
	INNER JOIN model.InvoiceReceivables ir ON ir.PatientInsuranceId = pi1.Id
	WHERE pi1.IsDeleted <> 1
	GROUP BY
	ir.Id
	,ir.InvoiceId
	,ips.StartDateTime
	,pi1.EndDateTime
	,pi1.InsuranceTypeId
) v ON v.InvoiceId = ir.InvoiceId
	AND v.StartDateTime <= CONVERT(DATETIME,ap.AppDate)
	AND (v.EndDateTime IS NULL OR v.EndDateTime >= CONVERT(DATETIME,ap.AppDate))
LEFT JOIN model.FinancialBatches fb ON fb.Id = fi.FinancialBatchId
LEFT JOIN dbo.PracticeCodeTable pctRsnCode ON pctRsnCode.Id = fi.ClaimAdjustmentReasonCodeId 
	AND pctRsnCode.ReferenceType = 'PAYMENTREASON'
LEFT JOIN model.Insurers ins ON ins.Id = fi.InsurerId
WHERE fi.BillingServiceId IS NULL

GO