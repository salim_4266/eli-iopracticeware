﻿IF OBJECT_ID('model.InsurerBusinessClasses') IS NOT NULL
	EXEC dbo.DropObject 'model.InsurerBusinessClasses'
GO

-- 'InsurerBusinessClasses' does not exist, creating...
CREATE TABLE [model].[InsurerBusinessClasses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IsArchived] bit  NOT NULL,
	LegacyPracticeCodeTableId INT NULL
);

-- Creating primary key on [Id] in table 'InsurerBusinessClasses'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.InsurerBusinessClasses', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[InsurerBusinessClasses] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.InsurerBusinessClasses', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[InsurerBusinessClasses]
ADD CONSTRAINT [PK_InsurerBusinessClasses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

INSERT INTO model.InsurerBusinessClasses (
	[Name],
    [IsArchived],
	LegacyPracticeCodeTableId
)
SELECT 
CASE Code
	WHEN 'AMERIH'
		THEN 'Amerihealth'
	WHEN 'BLUES'
		THEN 'BlueCrossBlueShield'
	WHEN 'COMM'
		THEN 'Commercial'
	WHEN 'MCAIDFL'
		THEN 'MedicaidFL'
	WHEN 'MCAIDNC'
		THEN 'MedicaidNC'
	WHEN 'MCAIDNJ'
		THEN 'MedicaidNJ'
	WHEN 'MCAIDNV'
		THEN 'MedicaidNV'
	WHEN 'MCAIDNY'
		THEN 'MedicaidNY'
	WHEN 'MCARENJ'
		THEN 'MedicareNJ'
	WHEN 'MCARENV'
		THEN 'MedicareNV'
	WHEN 'MCARENY'
		THEN 'MedicareNY'
	WHEN 'TEMPLATE'
		THEN 'Template'
	ELSE Code
END AS [Name]
,CONVERT(BIT, 0) AS IsArchived
,CASE Code
	WHEN 'AMERIH'
		THEN 1
	WHEN 'BLUES'
		THEN 2
	WHEN 'COMM'
		THEN 3
	WHEN 'MCAIDFL'
		THEN 4
	WHEN 'MCAIDNC'
		THEN 5
	WHEN 'MCAIDNJ'
		THEN 6
	WHEN 'MCAIDNV'
		THEN 7
	WHEN 'MCAIDNY'
		THEN 8
	WHEN 'MCARENJ'
		THEN 9
	WHEN 'MCARENV'
		THEN 10
	WHEN 'MCARENY'
		THEN 11
	WHEN 'TEMPLATE'
		THEN 12
	ELSE Id + 1000
END AS LegacyPracticeCodeTableId
FROM dbo.PracticeCodeTable p
WHERE ReferenceType = 'BUSINESSCLASS'
ORDER BY CASE p.Code
		WHEN 'AMERIH'
			THEN 1
		WHEN 'BLUES'
			THEN 2
		WHEN 'COMM'
			THEN 3
		WHEN 'MCAIDFL'
			THEN 4
		WHEN 'MCAIDNC'
			THEN 5
		WHEN 'MCAIDNJ'
			THEN 6
		WHEN 'MCAIDNV'
			THEN 7
		WHEN 'MCAIDNY'
			THEN 8
		WHEN 'MCARENJ'
			THEN 9
		WHEN 'MCARENV'
			THEN 10
		WHEN 'MCARENY'
			THEN 11
		WHEN 'TEMPLATE'
			THEN 12
		ELSE Id + 1000
	END
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.Insurers'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE i
SET i.InsurerBusinessClassId = bc.Id
FROM model.Insurers i
JOIN model.InsurerBusinessClasses bc ON bc.LegacyPracticeCodeTableId = i.InsurerBusinessClassId
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [InsurerBusinessClassId] in table 'Insurers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InsurerBusinessClassInsurer'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Insurers] DROP CONSTRAINT FK_InsurerBusinessClassInsurer

IF OBJECTPROPERTY(OBJECT_ID('[model].[InsurerBusinessClasses]'), 'IsUserTable') = 1
ALTER TABLE [model].[Insurers]
ADD CONSTRAINT [FK_InsurerBusinessClassInsurer]
    FOREIGN KEY ([InsurerBusinessClassId])
    REFERENCES [model].[InsurerBusinessClasses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerBusinessClassInsurer'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InsurerBusinessClassInsurer')
	DROP INDEX IX_FK_InsurerBusinessClassInsurer ON [model].[Insurers]
GO
CREATE INDEX [IX_FK_InsurerBusinessClassInsurer]
ON [model].[Insurers]
    ([InsurerBusinessClassId]);
GO