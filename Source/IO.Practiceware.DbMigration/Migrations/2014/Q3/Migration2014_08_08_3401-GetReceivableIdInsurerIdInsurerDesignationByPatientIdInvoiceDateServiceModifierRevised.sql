﻿IF OBJECT_ID('dbo.GetReceivableIdInsurerIdInsurerDesignationByPatientIdInvoiceDateServiceModifier', 'P') IS NOT NULL
	DROP PROCEDURE dbo.GetReceivableIdInsurerIdInsurerDesignationByPatientIdInvoiceDateServiceModifier 
GO

CREATE PROCEDURE dbo.GetReceivableIdInsurerIdInsurerDesignationByPatientIdInvoiceDateServiceModifier 
	@PatientId INT
	,@InvoiceDate NVARCHAR(8)
	,@Service NVARCHAR(MAX)
	,@Modifier NVARCHAR(MAX)
AS
BEGIN

SELECT 
ir.Id AS ReceivableId
,ins.Id
,COALESCE(ir.LegacyInsurerDesignation,'F') AS InsurerDesignation
FROM model.InvoiceReceivables ir 
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
LEFT JOIN model.Insurers ins ON ins.Id = ip.InsurerId
LEFT JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
LEFT JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
LEFT JOIN model.BillingServiceModifiers m ON m.BillingServiceId = bs.Id
WHERE ir.InvoiceId IN (
	SELECT 
	i.Id 
	FROM model.Invoices i 
	WHERE i.EncounterId IN (
		SELECT 
		DISTINCT ap.EncounterId 
		FROM dbo.Appointments ap 
		WHERE ap.PatientId = @PatientId
	)
	AND CONVERT(NVARCHAR,i.LegacyDateTime,112) = @InvoiceDate
)
AND es.Code = @Service
AND COALESCE(STUFF((
	SELECT sm.Code AS [text()]
	FROM model.BillingServiceModifiers mo
	JOIN model.ServiceModifiers sm ON sm.Id = mo.ServiceModifierId
	WHERE mo.BillingServiceId = m.BillingServiceId
	ORDER BY mo.OrdinalId ASC
	FOR XML PATH('')
), 1, 0, ''),'') = @Modifier
END
GO