﻿IF OBJECT_ID ( 'dbo.GetUnattachedPayments', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetUnattachedPayments;
GO

CREATE PROCEDURE dbo.GetUnattachedPayments
	@AppointmentId INT
AS
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#InvoiceReceivableIds') IS NOT NULL
BEGIN
	DROP TABLE #InvoiceReceivableIds
END
SELECT Id INTO #InvoiceReceivableIds FROM dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId)

IF (@@ROWCOUNT > 0)
BEGIN

SELECT
DISTINCT 
irs.Id AS ReceivableId
,i.LegacyPatientReceivablesInvoice AS Invoice
,adj.LegacyPaymentId AS PaymentId
,adj.Amount AS PaymentAmount
,CONVERT(NVARCHAR,adj.PostedDateTime,112) AS PaymentDate
,prs.ItemId
,adj.BillingServiceId AS PaymentService
,i.LegacyPatientReceivablesPatientId AS PatientId
,i.LegacyPatientReceivablesAppointmentId  AS AppointmentId
FROM #InvoiceReceivableIds irs
INNER JOIN model.InvoiceReceivables ir ON ir.Id = irs.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.Adjustments adj ON adj.InvoiceReceivableId = irs.Id
INNER JOIN model.FinancialInformations fi ON fi.InvoiceReceivableId = irs.Id
LEFT OUTER JOIN PatientReceivableServices prs ON (prs.ItemId = adj.BillingServiceId)
WHERE adj.BillingServiceId = '' OR prs.[Status] = 'X' OR adj.BillingServiceId IS NULL

UNION ALL

SELECT
DISTINCT 
irs.Id AS ReceivableId
,i.LegacyPatientReceivablesInvoice AS Invoice
,fi.LegacyPaymentId AS PaymentId
,fi.Amount AS PaymentAmount
,CONVERT(NVARCHAR,fi.PostedDateTime,112) AS PaymentDate
,prs.ItemId
,fi.BillingServiceId AS PaymentService
,i.LegacyPatientReceivablesPatientId AS PatientId
,i.LegacyPatientReceivablesAppointmentId  AS AppointmentId
FROM #InvoiceReceivableIds irs
INNER JOIN model.InvoiceReceivables ir ON ir.Id = irs.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.FinancialInformations fi ON fi.InvoiceReceivableId = irs.Id
LEFT OUTER JOIN PatientReceivableServices prs ON prs.ItemId = fi.BillingServiceId
WHERE (fi.BillingServiceId = '' OR prs.[Status] = 'X' OR fi.BillingServiceId IS NULL)

END
GO