﻿IF EXISTS(SELECT * FROM model.ApplicationSettings WHERE Name='AllowTransmitClinicalDataFilesOnCheckout')
	UPDATE Model.ApplicationSettings SET Name = 'GenerateClinicalDataFilesOnCheckout' 
	WHERE Name = 'AllowTransmitClinicalDataFilesOnCheckout'
GO

IF NOT EXISTS(SELECT * FROM model.ApplicationSettings WHERE Name='GenerateClinicalDataFilesOnCheckout')
	INSERT INTO Model.ApplicationSettings (Value,Name,MachineName, ApplicationSettingTypeId, UserId)
	VALUES ('True','GenerateClinicalDataFilesOnCheckout', NULL, 11, NULL)
GO
