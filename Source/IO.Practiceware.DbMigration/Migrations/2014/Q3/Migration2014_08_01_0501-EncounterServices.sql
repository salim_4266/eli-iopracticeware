﻿IF OBJECT_ID('model.EncounterServices') IS NOT NULL
	EXEC dbo.DropObject 'model.EncounterServices'
GO

-- 'EncounterServices' does not exist, creating...
CREATE TABLE [model].[EncounterServices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [DefaultUnit] decimal(18,2)  NOT NULL,
    [UnitFee] decimal(18,2)  NOT NULL,
    [StartDateTime] datetime  NULL,
    [EndDateTime] datetime  NULL,
    [IsOrderingProvider] bit  NOT NULL,
    [IsConsultation] bit  NOT NULL,
    [IsFacilityInvoice] bit  NOT NULL,
    [IsDoctorInvoice] bit  NOT NULL,
    [IsZeroCharge] bit  NOT NULL,
    [RelativeValueUnit] decimal(18,2)  NULL,
    [IsArchived] bit  NOT NULL,
    [IsUnclassified] bit  NOT NULL,
    [EncounterServiceTypeId] int  NULL,
    [RevenueCodeId] int  NULL,
    [ServiceUnitOfMeasurementId] int  NOT NULL,
    [IsCliaWaived] bit  NOT NULL,
	[LegacyPracticeServicesCodeId] INT NULL
);

-- Creating primary key on [Id] in table 'EncounterServices'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterServices', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterServices] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterServices', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterServices]
ADD CONSTRAINT [PK_EncounterServices]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [EncounterServiceTypeId] in table 'EncounterServices'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterServiceTypeEncounterService'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterServices] DROP CONSTRAINT FK_EncounterServiceTypeEncounterService

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterServiceTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterServices]
ADD CONSTRAINT [FK_EncounterServiceTypeEncounterService]
    FOREIGN KEY ([EncounterServiceTypeId])
    REFERENCES [model].[EncounterServiceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterServiceTypeEncounterService'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_EncounterServiceTypeEncounterService')
	DROP INDEX IX_FK_EncounterServiceTypeEncounterService ON [model].[EncounterServices]
GO
CREATE INDEX [IX_FK_EncounterServiceTypeEncounterService]
ON [model].[EncounterServices]
    ([EncounterServiceTypeId]);
GO

-- Creating foreign key on [RevenueCodeId] in table 'EncounterServices'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_RevenueCodeEncounterService'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterServices] DROP CONSTRAINT FK_RevenueCodeEncounterService

IF OBJECTPROPERTY(OBJECT_ID('[model].[RevenueCodes]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterServices]
ADD CONSTRAINT [FK_RevenueCodeEncounterService]
    FOREIGN KEY ([RevenueCodeId])
    REFERENCES [model].[RevenueCodes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RevenueCodeEncounterService'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_RevenueCodeEncounterService')
	DROP INDEX IX_FK_RevenueCodeEncounterService ON [model].[EncounterServices]
GO
CREATE INDEX [IX_FK_RevenueCodeEncounterService]
ON [model].[EncounterServices]
    ([RevenueCodeId]);
GO

INSERT INTO model.EncounterServices (Code, DefaultUnit, [Description], EncounterServiceTypeId,
	EndDateTime, IsArchived, IsCliaWaived, IsConsultation, IsDoctorInvoice, IsFacilityInvoice
	,IsOrderingProvider, IsUnclassified, IsZeroCharge, RelativeValueUnit, RevenueCodeId
	,ServiceUnitOfMeasurementId, StartDateTime, UnitFee, LegacyPracticeServicesCodeId
)
SELECT 
ps.Code AS Code
,CONVERT(DECIMAL(18,2), 1) AS DefaultUnit
,ps.[Description]
,t.Id AS EncounterServiceTypeId
,CASE 
	WHEN ps.EndDate <> '' AND ps.EndDate IS NOT NULL
		THEN CONVERT(DATETIME, ps.EndDate, 112) 
	ELSE NULL
END AS EndDateTime
,CONVERT(BIT, 0) AS IsArchived
,ps.IsCliaWaived AS IsCliaWaived
,CASE ps.ConsultOn
	WHEN 'T'
		THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0)
END AS IsConsultation
,CONVERT(BIT, 1) AS IsDoctorInvoice
,CASE ps.POS
	WHEN '98'
		THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0)
END AS IsFacilityInvoice
,CASE OrderDoc
	WHEN 'T'
		THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0)
END AS IsOrderingProvider
,CASE 
	WHEN LEN(ClaimNote) > 0
		THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0)
END as IsUnclassified
,CASE ServiceFilter
	WHEN 'T'
		THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0)
END AS IsZeroCharge
,CONVERT(DECIMAL(18,2), RelativeValueUnit) AS RelativeValueUnit
,ps.RevenueCodeId
,CASE 
	WHEN ps.CodeCategory = 'ANESTHESIA'
		THEN 1
	ELSE 2
END AS ServiceUnitOfMeasurementId
,CASE 
	WHEN ps.StartDate <> '' AND ps.StartDate IS NOT NULL
		THEN CONVERT(DATETIME, ps.StartDate, 112) 
	ELSE NULL
END AS StartDateTime
,CONVERT(DECIMAL(18,2), Fee) AS UnitFee
,ps.CodeId AS LegacyPracticeServicesCodeId
FROM dbo.PracticeServices ps
LEFT JOIN model.EncounterServiceTypes t ON t.Name = ps.CodeCategory
WHERE ps.CodeType = 'P'

UNION ALL

SELECT DISTINCT
prs.[Service] AS Code
,CONVERT(DECIMAL(18,2), 1) AS DefaultUnit
,'Archived practice service' AS [Description]
,(SELECT TOP 1 Id FROM model.EncounterServiceTypes WHERE Name LIKE '%Misc%') AS EncounterServiceTypeId
,NULL AS EndDateTime
,CONVERT(BIT, 1) AS IsArchived
,CONVERT(BIT, 0) AS IsCliaWaived
,CONVERT(BIT, 0) AS IsConsultation
,CONVERT(BIT, 1) AS IsDoctorInvoice
,CONVERT(BIT, 0) AS IsFacilityInvoice
,CONVERT(BIT, 0) AS IsOrderingProvider
,CONVERT(BIT, 0) AS IsUnclassified
,CONVERT(BIT, 0) AS IsZeroCharge
,CONVERT(DECIMAL(18,2), 0) AS RelativeValueUnit
,NULL AS RevenueCodeId
,2 AS ServiceUnitOfMeasurementId
,NULL AS StartDateTime
,CONVERT(DECIMAL(18,2),0) AS UnitFee
,NULL AS LegacyPracticeServicesCodeId
FROM PatientReceivableServices prs
LEFT JOIN dbo.PracticeServices p ON UPPER(CONVERT(NVARCHAR(MAX),p.Code)) = UPPER(CONVERT(NVARCHAR(MAX),prs.Service))
WHERE prs.[Status] = 'A' AND Code NOT IN (
	SELECT DISTINCT ps.Code 
	FROM dbo.PracticeServices ps
	LEFT JOIN model.EncounterServiceTypes t ON t.Name = ps.CodeCategory
	WHERE ps.CodeType = 'P' 
)

GO