﻿-- Update DrugCode to 'F' if it is 'T'
IF EXISTS (SELECT * FROM dbo.Drug WHERE DisplayName = 'No known medications' AND DrugCode = 'T') 
BEGIN 
     UPDATE dbo.Drug SET DrugCode = 'F' WHERE DisplayName = 'No known medications' AND DrugCode = 'T'
END 
GO 

--Create a Drug Row for 'No known medications' with DrugCode as 'F' if it doesn't exists
 IF NOT EXISTS (SELECT * FROM dbo.Drug WHERE DisplayName = 'No known medications') 
 BEGIN 
     INSERT INTO dbo.Drug (
         DrugCode 
         ,DisplayName 
         ,Family1 
         ,Family2 
        ,Focus 
         ,DrugDosageFormId 
         ) 
     VALUES ( 
        'F' 
         ,'No known medications' 
        ,'UNKNOWN' 
        ,'' 
        ,1 
       ,NULL 
       ) 
END 
GO 
