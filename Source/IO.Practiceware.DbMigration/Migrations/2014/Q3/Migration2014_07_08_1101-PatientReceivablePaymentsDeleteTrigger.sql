﻿DECLARE @sql NVARCHAR(MAX)
SET @sql = ''

SET @sql = '
ALTER TRIGGER PatientReceivablePaymentsDelete ON dbo.PatientReceivablePayments
INSTEAD OF DELETE
AS
BEGIN
	SELECT 
	PaymentId 
	,CASE 
		WHEN PaymentType IN (''P'',''X'',''8'',''R'',''U'')
			THEN 1
		WHEN PaymentType IN (''!'',''|'',''?'',''%'',''D'',''&'','''',''0'',''V'')
			THEN 2
		WHEN (PaymentType NOT IN (''P'',''X'',''8'',''R'',''U'',''!'',''|'',''?'',''%'',''D'',''&'','''',''0'',''V'') AND PaymentType IN (
			SELECT 
			SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code)) 
			FROM PracticeCodeTable pct 
			WHERE pct.ReferenceType = ''PAYMENTTYPE'' AND pct.AlternateCode IN (''C'',''D'')
			AND (SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code)) 
			NOT IN (''P'',''X'',''8'',''R'',''U'',''!'',''|'',''?'',''%'',''D'',''&'','''',''0'',''V''))))
			THEN 1
		WHEN (PaymentType NOT IN (''P'',''X'',''8'',''R'',''U'',''!'',''|'',''?'',''%'',''D'',''&'','''',''0'',''V'') AND (PaymentType IN (
			SELECT 
			SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code)) 
			FROM PracticeCodeTable pct 
			WHERE pct.ReferenceType = ''PAYMENTTYPE'' AND pct.AlternateCode NOT IN (''C'',''D'')
			AND (SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code)) 
			NOT IN (''P'',''X'',''8'',''R'',''U'',''!'',''|'',''?'',''%'',''D'',''&'','''',''0'',''V''))))
			)
			THEN 2
		WHEN PaymentType = ''=''
			THEN 0
	END AS [TableToDeleteFrom]
	INTO #deleted
	FROM deleted d 
	
	;WITH CTE AS (
	SELECT f.Id
	FROM model.FinancialBatches f
	JOIN #deleted d ON d.PaymentId = f.Id
	WHERE d.TableToDeleteFrom = 0
	)
	DELETE FROM model.FinancialBatches WHERE Id IN (SELECT Id FROM CTE)

	;WITH CTE AS (
	SELECT a.Id
	FROM model.Adjustments a
	JOIN #deleted d ON d.PaymentId = a.LegacyPaymentId
	WHERE d.TableToDeleteFrom = 1
	)
	DELETE FROM model.Adjustments WHERE Id IN (SELECT Id FROM CTE)

	;WITH CTE AS (
	SELECT f.Id
	FROM model.FinancialInformations f
	JOIN #deleted d ON d.PaymentId = f.LegacyPaymentId
	WHERE d.TableToDeleteFrom = 2
	)
	DELETE FROM model.FinancialInformations WHERE Id IN (SELECT Id FROM CTE)
END
'


IF EXISTS (select * from sys.triggers where name = 'PatientReceivablePaymentsDelete'
	AND parent_id = OBJECT_ID('dbo.PatientReceivablePayments','V'))
BEGIN 
	EXEC sp_executesql @sql
END

IF NOT EXISTS (select * from sys.triggers where name = 'PatientReceivablePaymentsDelete'
	AND parent_id = OBJECT_ID('dbo.PatientReceivablePayments','V'))
BEGIN 
	SELECT @sql = REPLACE(@sql,'ALTER TRIGGER ', 'CREATE TRIGGER ')
	EXEC sp_executesql @sql
END


GO