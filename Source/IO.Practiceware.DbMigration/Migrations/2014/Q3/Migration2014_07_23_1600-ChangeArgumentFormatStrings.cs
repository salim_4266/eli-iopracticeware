﻿using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations._2014.Q3
{
    [Migration(201407231600)]
    public class Migration201407231600ChangeArgumentFormatStrings : NonFluentTransactionalUpOnlyMigration
    {
        private const string Query = @"SELECT * FROM model.ExternalApplications";
        private const string RegularExpressionArgumen = "(\\-\\w+\\s)|((\\#|\\&|\\-)?(\\w+)\\=)|\\\"?\\{\\w*\\}\\\"?";
        private const string UpdateQuery = "UPDATE model.ExternalApplications SET ArgumentFormatString = '{0}' WHERE argumentFormatString = '{1}'";

        protected override void MigrateUp(IDbConnection connection, IDbTransaction transaction)
        {
            var argumentMatcher = new Regex(RegularExpressionArgumen);

            using (var command = connection.CreateCommand())
            {
                command.CommandText = Query;
                command.Transaction = transaction;
                var dt = new DataTable();
                using (IDataReader reader = command.ExecuteReader())
                {
                    dt.Load(reader);
                    foreach (var row in dt.Rows.OfType<DataRow>())
                    {
                        var newArgumentFormatString = new StringBuilder();
                        if (!string.IsNullOrEmpty(row["ArgumentFormatString"].ToString()))
                        {
                            var value = row["ArgumentFormatString"].ToString();
                            var matches = argumentMatcher.Matches(value);
                            foreach (var match in matches)
                            {
                                newArgumentFormatString.Append(match);
                            }

                            command.CommandText = string.Format(UpdateQuery, newArgumentFormatString, value);
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
        }
    }
}
