﻿IF OBJECT_ID ( 'dbo.HasAscClaim', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.HasAscClaim;
GO

CREATE PROCEDURE dbo.HasAscClaim
	@ItemId NVARCHAR(12)
AS
SET NOCOUNT ON;

SELECT 
bs.Id AS ItemId
FROM model.BillingServices bs 
INNER JOIN model.Invoices i ON i.Id = bs.InvoiceId
WHERE bs.Id = @ItemId
	AND i.InvoiceTypeId = 2

GO