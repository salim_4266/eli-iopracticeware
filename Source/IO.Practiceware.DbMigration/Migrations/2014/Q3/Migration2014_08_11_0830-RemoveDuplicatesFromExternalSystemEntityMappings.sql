﻿-- delete duplicate ExternalSystemEntityMappings
DELETE FROM model.ExternalSystemEntityMappings WHERE Id IN (
SELECT Id FROM model.ExternalSystemEntityMappings WHERE Id NOT IN (
SELECT MIN(Id) FROM model.ExternalSystemEntityMappings GROUP BY PracticeRepositoryEntityId, ExternalSystemEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityKey))
AND PracticeRepositoryEntityId = (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'Allergen')