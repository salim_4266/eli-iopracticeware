﻿--Tags HexColor values are in aRGB (i.e., 7-digit) format.
UPDATE model.Tags SET HexColor = '#' + HexColor WHERE SUBSTRING(HexColor,1,1) <> '#' AND LEN(HexColor) < 7