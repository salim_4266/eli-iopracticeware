﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using FluentMigrator;
using Soaf;
using Soaf.Data;

namespace IO.Practiceware.DbMigration.Migrations._2014.Q3
{
    [Migration(201408291600)]
    public class Migration201408291600CompressPatientInsuranceDocuments : NonFluentTransactionalUpOnlyMigration
    {
        private const string NoAudit = @"IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit

SELECT 1 AS Value INTO #NoAudit";
        private const string IdsQuery = @"SELECT Id FROM model.PatientInsuranceDocuments";
        private const string ContentQuery = @"SELECT Content FROM model.PatientInsuranceDocuments WHERE Id = {0}";
        private const string UpdateQuery = @"UPDATE model.PatientInsuranceDocuments SET Content = @Content WHERE Id = {0}";

        protected override void MigrateUp(IDbConnection connection, IDbTransaction transaction)
        {
            var createCommand = new Func<IDbCommand>(() =>
            {
                var c = connection.CreateCommand();
                c.CommandTimeout = 60;
                c.Transaction = transaction;
                return c;
            });

            connection.Execute(NoAudit, createCommand: createCommand);

            var ids = connection.Execute<DataTable>(IdsQuery, createCommand: createCommand)
                .AsEnumerable().Select(r => r.Get<int>("Id"));

            foreach (var id in ids)
            {
                var content = connection.Execute<byte[]>(ContentQuery.FormatWith(id), createCommand: createCommand);

                content = CompressContent(content);

                var parameters = new Dictionary<string, object>();
                parameters["Content"] = content;

                connection.Execute(UpdateQuery.FormatWith(id), parameters, createCommand: createCommand);
            }
        }

        private static byte[] CompressContent(byte[] content)
        {
            using (var ms = new MemoryStream(content))
            using (var image = Image.FromStream(ms))
            {
                content = image.ToByteArray(ImageFormat.Jpeg, 30);
            }

            return content;
        }
    }
}
