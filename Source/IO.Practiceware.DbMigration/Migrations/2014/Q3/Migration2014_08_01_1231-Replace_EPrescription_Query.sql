﻿UPDATE dbo.CMSReports SET CMSQuery = 'dbo.USP_CMS_GetEPrescStatus'
WHERE CAST(CMSQuery AS NVARCHAR(100)) = 'model.USP_CMS_Stage2_eRx' 
AND CAST(CMSRepName AS NVARCHAR(100)) = 'Stage 2 - CORE 2: e-Prescribing (eRx)'
