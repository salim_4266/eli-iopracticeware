﻿INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)
SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 12, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id IN (47, 48, 49) -- Patient Financials: Add, Edit, Delete Payments, Adjustments, and Informational Transactions
WHERE ResourceType <> 'R'
	AND ResourceId > 0

EXCEPT

SELECT IsDenied, UserId, PermissionId 
FROM model.UserPermissions

