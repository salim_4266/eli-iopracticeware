﻿IF OBJECT_ID('dbo.PatientClinicalPatientDiagnosisDetailInsertQueue','U') IS NULL
BEGIN
	CREATE TABLE dbo.PatientClinicalPatientDiagnosisDetailInsertQueue (
		Id INT NOT NULL IDENTITY(1,1) NOT FOR REPLICATION CONSTRAINT PK_PatientClinicalPatientDiagnosisDetailInsertQueue_Id PRIMARY KEY
		,ClinicalId INT NOT NULL)
END
GO

IF OBJECT_ID('dbo.PatientClinicalPatientDiagnosisDetailDeleteQueue','U') IS NULL
BEGIN
	CREATE TABLE dbo.PatientClinicalPatientDiagnosisDetailDeleteQueue (
		Id INT NOT NULL IDENTITY(1,1) NOT FOR REPLICATION CONSTRAINT PK_PatientClinicalPatientDiagnosisDetailDeleteQueue_Id PRIMARY KEY
		,ClinicalId INT NOT NULL)
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PatientDiagnosisDetailsDataDelete')
	DROP TRIGGER dbo.PatientDiagnosisDetailsDataDelete
GO
	
CREATE TRIGGER dbo.PatientDiagnosisDetailsDataDelete ON dbo.PatientClinical 
AFTER DELETE
NOT FOR REPLICATION
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	INSERT INTO dbo.PatientClinicalPatientDiagnosisDetailDeleteQueue (ClinicalId)
		SELECT d.ClinicalId FROM deleted d WHERE d.ClinicalType IN ('Q','U')
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PatientDiagnosisDetailsDataInsert')
	DROP TRIGGER dbo.PatientDiagnosisDetailsDataInsert
GO
	
CREATE TRIGGER dbo.PatientDiagnosisDetailsDataInsert ON dbo.PatientClinical 
AFTER INSERT
NOT FOR REPLICATION
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	INSERT INTO dbo.PatientClinicalPatientDiagnosisDetailInsertQueue (ClinicalId)
		SELECT i.ClinicalId FROM inserted i WHERE i.ClinicalType IN ('Q','U') 
END
GO