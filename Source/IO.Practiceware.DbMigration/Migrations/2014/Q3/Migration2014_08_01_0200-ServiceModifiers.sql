﻿--To Fix Bug #15836.Removed Second Partition from model.ServiceModifers and addding those codes in PracticeCodeTable.
IF OBJECT_ID('tempdb..#tmpModifersToBeAdded') IS NOT NULL
	DROP TABLE #tmpModifersToBeAdded

SELECT T.BillingOrdinalId
	,T.Code
	,T.Description
INTO #tmpModifersToBeAdded
FROM (
	SELECT v.BillingOrdinalId
		,CASE v.QueryOrdinal
			WHEN 0
				THEN SUBSTRING(v.Code, 1, 2)
			WHEN 1
				THEN SUBSTRING(v.Modifier, 1, 2)
			WHEN 2
				THEN SUBSTRING(v.Modifier, 3, 2)
			WHEN 3
				THEN SUBSTRING(v.Modifier, 5, 2)
			WHEN 4
				THEN SUBSTRING(v.Modifier, 7, 2)
			END AS Code
		,CASE v.QueryOrdinal
			WHEN 0
				THEN SUBSTRING(Code, 6, LEN(code))
			ELSE CONVERT(NVARCHAR, NULL)
			END AS Description
		,CASE v.QueryOrdinal
			WHEN 0
				THEN CASE 
						WHEN ISNUMERIC(AlternateCode + '.0e0') = 1
							THEN CONVERT(INT, v.AlternateCode)
						ELSE 99
						END
			WHEN 1
				THEN 99
			END AS ClinicalOrdinalId
		,CONVERT(BIT, 0) AS IsArchived
	FROM (
		----First 2 bytes of prs.Modifier (prs.Modifier has up to 4 modifers concatenated)
		SELECT ItemId AS Id
			,99 AS BillingOrdinalId
			,Modifier AS Modifier
			,CONVERT(NVARCHAR, NULL) AS Code
			,CONVERT(NVARCHAR, NULL) AS AlternateCode
			,1 AS QueryOrdinal
			,COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivableServices
		WHERE SUBSTRING(REPLACE(REPLACE(Modifier, ' ', ''), ',', ''), 1, 2) <> ''
			AND SUBSTRING(REPLACE(REPLACE(Modifier, ' ', ''), ',', ''), 1, 2) NOT IN (
				SELECT SUBSTRING(Code, 1, 2) AS sm
				FROM dbo.PracticeCodeTable
				WHERE ReferenceType = 'SERVICEMODS'
				)
			AND [Status] = 'A'
		GROUP BY ItemId
			,Modifier
		
		UNION
		
		----Second set of 2 characters of prs.Modifier
		SELECT ItemId AS Id
			,99 AS BillingOrdinalId
			,Modifier
			,CONVERT(NVARCHAR, NULL) AS Code
			,CONVERT(NVARCHAR, NULL) AS AlternateCode
			,2 AS QueryOrdinal
			,COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivableServices
		WHERE SUBSTRING(REPLACE(REPLACE(Modifier, ' ', ''), ',', ''), 3, 2) <> ''
			AND SUBSTRING(REPLACE(REPLACE(Modifier, ' ', ''), ',', ''), 3, 2) NOT IN (
				SELECT SUBSTRING(Code, 1, 2) AS sm
				FROM dbo.PracticeCodeTable
				WHERE ReferenceType = 'SERVICEMODS'
				)
			AND [Status] = 'A'
		GROUP BY ItemId
			,Modifier
		
		UNION
		
		----Third set of 2-characters of prs.Modifier
		SELECT ItemId AS Id
			,99 AS BillingOrdinalId
			,Modifier AS Modifier
			,CONVERT(NVARCHAR, NULL) AS Code
			,CONVERT(NVARCHAR, NULL) AS AlternateCode
			,3 AS QueryOrdinal
			,COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivableServices
		WHERE SUBSTRING(REPLACE(REPLACE(Modifier, ' ', ''), ',', ''), 5, 2) <> ''
			AND SUBSTRING(REPLACE(REPLACE(Modifier, ' ', ''), ',', ''), 5, 2) NOT IN (
				SELECT SUBSTRING(Code, 1, 2) AS sm
				FROM dbo.PracticeCodeTable
				WHERE ReferenceType = 'SERVICEMODS'
				)
			AND [Status] = 'A'
		GROUP BY ItemId
			,Modifier
		
		UNION
		
		----Fourth set of 2-characters from prs.Modifier
		SELECT ItemId AS Id
			,99 AS BillingOrdinalId
			,Modifier AS Modifier
			,CONVERT(NVARCHAR, NULL) AS Code
			,CONVERT(NVARCHAR, NULL) AS AlternateCode
			,4 AS QueryOrdinal
			,COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivableServices
		WHERE SUBSTRING(REPLACE(REPLACE(Modifier, ' ', ''), ',', ''), 7, 2) <> ''
			AND SUBSTRING(REPLACE(REPLACE(Modifier, ' ', ''), ',', ''), 7, 2) NOT IN (
				SELECT SUBSTRING(Code, 1, 2) AS sm
				FROM dbo.PracticeCodeTable
				WHERE ReferenceType = 'SERVICEMODS'
				)
			AND [Status] = 'A'
		GROUP BY ItemId
			,Modifier
		) AS V
	) AS T
GROUP BY T.BillingOrdinalId
	,T.Code
	,T.Description

DECLARE @ModifierToBeAdded VARCHAR(MAX);
DECLARE @AlternateCode VARCHAR(100);

DECLARE CursorToAddModifer CURSOR FAST_FORWARD
FOR
SELECT Code
	,BillingOrdinalId
FROM #tmpModifersToBeAdded

OPEN CursorToAddModifer

FETCH NEXT
FROM CursorToAddModifer
INTO @ModifierToBeAdded
	,@AlternateCode

WHILE @@FETCH_STATUS = 0
BEGIN
	IF NOT EXISTS (
			SELECT *
			FROM PracticeCodeTable
			WHERE ReferenceType = 'SERVICEMODS'
				AND SUBSTRING(Code, 1, 2) = @ModifierToBeAdded
			)
	BEGIN
		INSERT INTO PracticeCodeTable (
			ReferenceType
			,Code
			,AlternateCode
			,Rank
			,Exclusion
			,Signature
			)
		VALUES (
			'SERVICEMODS'
			,@ModifierToBeAdded + ' - ' + ''
			,@AlternateCode
			,'99'
			,'F'
			,'F'
			)
	END

	DELETE
	FROM #tmpModifersToBeAdded
	WHERE Code = @ModifierToBeAdded

	FETCH NEXT
	FROM CursorToAddModifer
	INTO @ModifierToBeAdded
		,@AlternateCode
END

CLOSE CursorToAddModifer

DEALLOCATE CursorToAddModifer
GO


IF OBJECT_ID('model.ServiceModifiers') IS NOT NULL
	EXEC dbo.DropObject 'model.ServiceModifiers'
GO

-- 'ServiceModifiers' does not exist, creating...
CREATE TABLE [model].[ServiceModifiers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [ClinicalOrdinalId] int  NULL,
    [BillingOrdinalId] int  NULL,
    [IsArchived] bit  NOT NULL
);

-- Creating primary key on [Id] in table 'ServiceModifiers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ServiceModifiers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ServiceModifiers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ServiceModifiers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ServiceModifiers]
ADD CONSTRAINT [PK_ServiceModifiers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

INSERT INTO model.ServiceModifiers (BillingOrdinalId, Code, [Description], ClinicalOrdinalId, IsArchived)
SELECT v.BillingOrdinalId,
CASE v.QueryOrdinal
	WHEN 0
		THEN SUBSTRING(v.Code, 1, 2)
	WHEN 1
		THEN SUBSTRING(v.Modifier, 1, 2)
	WHEN 2
		THEN SUBSTRING(v.Modifier, 3, 2)
	WHEN 3
		THEN SUBSTRING(v.Modifier, 5, 2)
	WHEN 4
		THEN SUBSTRING(v.Modifier, 7, 2)
	END AS Code,
CASE v.QueryOrdinal
	WHEN 0
		THEN SUBSTRING(Code, 6, LEN(code))
	ELSE CONVERT(nvarchar, NULL) 
	END AS Description,
CASE v.QueryOrdinal
	WHEN 0
		THEN CASE
			WHEN ISNUMERIC(AlternateCode + '.0e0')=1
				THEN CONVERT(int, v.AlternateCode) 
			ELSE 99
			END
	WHEN 1
		THEN 99
	END AS ClinicalOrdinalId,
CONVERT(bit, 0) AS IsArchived
FROM (SELECT Id AS Id,
	CASE 
		WHEN [Rank] IS NULL
			THEN CONVERT(int, 0)
		ELSE [Rank]
		END AS BillingOrdinalId,
	CONVERT(nvarchar, NULL) AS Modifier,
	Code AS Code,
	AlternateCode AS AlternateCode,
	0 AS QueryOrdinal,
	COUNT_BIG(*) AS Count
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'SERVICEMODS'
GROUP BY [Rank],
	Id,
	Code,
	AlternateCode
) AS v