﻿IF OBJECT_ID('dbo.VerifyPrpRecord', 'P') IS NOT NULL
	DROP PROCEDURE dbo.VerifyPrpRecord;
GO

CREATE PROCEDURE dbo.VerifyPrpRecord @ItemId INT
AS
SET NOCOUNT ON;

DECLARE @AppointmentId INT
SET @AppointmentId = (
	SELECT TOP 1 i.LegacyPatientReceivablesAppointmentId 
	FROM model.Invoices i 
	JOIN model.BillingServices bs ON bs.InvoiceId = i.Id 
		AND bs.Id = @ItemId)
SELECT Id INTO #InvoiceReceivableIds FROM dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId)

SELECT adj.Id
	,bs.Id AS ItemId
	,ir.Id
FROM model.Adjustments adj
INNER JOIN model.BillingServices bs ON bs.Id = adj.BillingServiceId
INNER JOIN #InvoiceReceivableIds iri ON iri.Id = adj.InvoiceReceivableId
INNER JOIN model.InvoiceReceivables ir ON ir.Id = iri.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
WHERE i.LegacyPatientReceivablesInvoice <> i.LegacyPatientReceivablesInvoice
	AND adj.BillingServiceId = @ItemId

UNION ALL

SELECT fi.Id
	,bs.Id AS ItemId
	,ir.Id
FROM model.FinancialInformations fi
INNER JOIN model.BillingServices bs ON bs.Id = fi.BillingServiceId
INNER JOIN #InvoiceReceivableIds iri ON iri.Id = fi.InvoiceReceivableId
INNER JOIN model.InvoiceReceivables ir ON ir.Id = iri.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
WHERE i.LegacyPatientReceivablesInvoice <> i.LegacyPatientReceivablesInvoice
	AND fi.BillingServiceId = @ItemId
GO