﻿IF OBJECT_ID('model.EncounterServiceDrugs') IS NOT NULL
	EXEC dbo.DropObject 'model.EncounterServiceDrugs'
GO

IF OBJECT_ID(N'[model].[FK_EncounterServiceEncounterServiceDrug]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterServiceDrugs]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterServiceDrugs] DROP CONSTRAINT [FK_EncounterServiceEncounterServiceDrug];
GO

-- 'EncounterServiceDrugs' does not exist, creating...
CREATE TABLE [model].[EncounterServiceDrugs] (
    [Id] int NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [NDC] nvarchar(max)  NULL,
    [Quantity] decimal(18,2)  NOT NULL,
    [DrugUnitOfMeasurementId] int  NOT NULL
)
GO

-- Creating primary key on [Id] in table 'EncounterServiceDrugs'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterServiceDrugs', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterServiceDrugs] DROP CONSTRAINT ' + name + ';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterServiceDrugs', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterServiceDrugs]
ADD CONSTRAINT [PK_EncounterServiceDrugs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [Id] in table 'EncounterServiceDrugs'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterServiceEncounterServiceDrug'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterServiceDrugs] DROP CONSTRAINT FK_EncounterServiceEncounterServiceDrug

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterServices]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterServiceDrugs]
ADD CONSTRAINT [FK_EncounterServiceEncounterServiceDrug]
    FOREIGN KEY ([Id])
    REFERENCES [model].[EncounterServices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

INSERT INTO [model].[EncounterServiceDrugs] (
	[Id],
	[Name],
    [NDC],
    [Quantity],
    [DrugUnitOfMeasurementId]
)
SELECT 
es.Id AS Id
,CASE 
	WHEN ClaimNote <> '' 
		THEN ClaimNote 
	ELSE CASE WHEN ps.[Description] IS NOT NULL AND ps.[Description] <> '' THEN ps.[Description] ELSE '' END
END AS [Name],
CASE WHEN ps.NDC IS NOT NULL AND ps.NDC <> '' THEN ps.NDC END AS NDC
,CONVERT(DECIMAL(18,2), 1) AS Quantity,
5 AS DrugUnitOfMeasurementId
FROM dbo.PracticeServices ps 
INNER JOIN model.EncounterServices es ON es.LegacyPracticeServicesCodeId = ps.CodeId
WHERE SUBSTRING(ps.Code, 1, 1) = 'J'
	OR ps.CodeCategory like '%DRUG%'
	OR ps.CodeCategory like '%INJECT%'
	OR ps.CodeCategory like '%HCPCS%'
GO
