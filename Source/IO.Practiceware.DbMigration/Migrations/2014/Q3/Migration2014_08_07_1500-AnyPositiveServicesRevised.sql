﻿IF OBJECT_ID ( 'dbo.AnyPositiveServices', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.AnyPositiveServices;
GO

CREATE PROCEDURE dbo.AnyPositiveServices
	@ItemId INT
AS
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#AdjustmentTypes') IS NOT NULL
BEGIN
	DROP TABLE #AdjustmentTypes
END

SELECT 
v.Id AS Id
,v.PaymentType
,v.IsDebit AS IsDebit
,v.ShortName AS ShortName
INTO #AdjustmentTypes
FROM (
	SELECT 
		CASE
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'P'
				THEN 1
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'X'
				THEN 2
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '8'
				THEN 3
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'R'
				THEN 4
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'U'
				THEN 5
			ELSE pct.Id + 1000
		END AS Id
		,SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS PaymentType
		,CASE pct.AlternateCode
			WHEN 'D'
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
		END AS IsDebit
		,LetterTranslation AS ShortName
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'
	GROUP BY 
	Id
	,Code
	,pct.AlternateCode
	,LetterTranslation
) AS v

SELECT
(bs.Unit * bs.UnitCharge) - SUM(
	CASE at.IsDebit
		WHEN 1
			THEN - adj.Amount
		WHEN 0
			THEN adj.Amount
		ELSE 0
	END) AS PreviousBalance
FROM model.BillingServices bs
LEFT JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
LEFT JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
LEFT JOIN model.Adjustments adj ON bs.Id = adj.BillingServiceId
LEFT JOIN #AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
LEFT JOIN model.Invoices i ON bs.InvoiceId = i.Id
WHERE bs.Id = @ItemId
GROUP BY
i.LegacyDateTime
,bs.Id
,es.Code
,bsm.BillingServiceId
,bs.Unit
,bs.UnitCharge
,bs.UnitAllowableExpense
,i.Id

GO