﻿EXEC DropObject 'dbo.ServiceTransactions'
GO
EXEC DropObject 'dbo.ServiceTransactions_Internal'
GO

CREATE VIEW dbo.ServiceTransactions_Internal
WITH SCHEMABINDING, VIEW_METADATA
AS
SELECT 
bst.Id
,bst.LegacyTransactionId AS TransactionId
,bst.BillingServiceId AS ServiceId
,ptj.TransactionDate AS TransactionDate
,bst.AmountSent AS Amount
,CASE  
	WHEN (bst.BillingServiceTransactionStatusId = 1 AND ptj.TransactionStatus = 'P')
		THEN CASE 
				WHEN bst.AmountSent > 0
					THEN 'P' 
				ELSE 'B' 
			END
	WHEN (bst.BillingServiceTransactionStatusId = 2 AND ptj.TransactionStatus = 'S'
		AND bst.AmountSent = 0)
		THEN 'B'
	WHEN (bst.BillingServiceTransactionStatusId = 3 AND ptj.TransactionStatus IN ('P','S'))
		THEN 'V'
	WHEN (bst.BillingServiceTransactionStatusId = 2 AND ptj.TransactionStatus IN ('P','S'))
		THEN 'P'
	WHEN (bst.BillingServiceTransactionStatusId = 4 AND ptj.TransactionStatus = 'S')
		THEN 'X'
	WHEN (bst.BillingServiceTransactionStatusId = 6 AND ptj.TransactionStatus IN ('S','Z'))
		THEN '1'
	WHEN (bst.BillingServiceTransactionStatusId = 7 AND ptj.TransactionStatus = 'Z')
		THEN '2'
	ELSE '0'
END AS TransportAction
,bst.[DateTime] AS ModTime
,CONVERT(UNIQUEIDENTIFIER,NULL) AS ExternalSystemMessageId
FROM model.BillingServiceTransactions bst
INNER JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionId = bst.LegacyTransactionId
	AND ptj.TransactionType = 'R'
	AND ptj.TransactionRef <> 'G'
GO

CREATE UNIQUE CLUSTERED INDEX [IX_ServiceTransactions_TransactionIdTransportAction] ON 
	[dbo].[ServiceTransactions_Internal](Id, TransactionId) 
GO

CREATE VIEW dbo.ServiceTransactions
WITH SCHEMABINDING, VIEW_METADATA
AS
SELECT 
sti.Id 
,sti.TransactionId
,sti.ServiceId
,sti.TransactionDate
,sti.Amount
,sti.TransportAction
,sti.ModTime
,sti.ExternalSystemMessageId
FROM dbo.ServiceTransactions_Internal sti WITH (NOEXPAND)
GO

CREATE TRIGGER [dbo].[ServiceTransactionsInsert] ON [dbo].[ServiceTransactions]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @InsertedBillingServiceTransactions TABLE (LegacyTransactionId INT, AmountSent DECIMAL(18,2))

	INSERT INTO model.BillingServiceTransactions(
	[DateTime]
	,AmountSent
	,ClaimFileNumber
	,BillingServiceTransactionStatusId
	,MethodSentId
	,BillingServiceId
	,InvoiceReceivableId
	,ClaimFileReceiverId
	,ExternalSystemMessageId
	,PatientAggregateStatementId
	,LegacyTransactionId)
	OUTPUT inserted.LegacyTransactionId, inserted.AmountSent INTO @InsertedBillingServiceTransactions(LegacyTransactionId,AmountSent)
	SELECT
	DATEADD(mi,TransactionTime,(CONVERT(DATETIME,ptj.TransactionDate))) AS [DateTime]
	,i.Amount AS AmountSent
	,NULL AS ClaimFileNumber
	,CASE 
		WHEN ptj.TransactionStatus = 'P'
			AND i.TransportAction IN ('B', 'P')
			THEN 1
		WHEN ptj.TransactionStatus = 'P'
			AND i.TransportAction = 'V'
			THEN 3
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'P'
			THEN 2
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'V'
			THEN 3
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'X'
			THEN 4
		WHEN ptj.TransactionStatus = 'Z'
			AND i.TransportAction = '1'
			THEN 6
		WHEN ptj.TransactionStatus = 'Z'
			AND i.TransportAction = '2'
			THEN 7
		ELSE 5
	END AS BillingServiceTransactionStatusId
	,CASE 
		WHEN (ptj.TransactionRef = 'I' AND ptj.TransactionAction = 'B')
			THEN 1
		ELSE 2 
	END AS MethodSentId
	,i.ServiceId AS BillingServiceId
	,CASE 
		WHEN ptj.TransactionRef = 'P'
			THEN irPatient.Id
		ELSE ptj.TransactionTypeId
	END AS InvoiceReceivableId
	,CASE 
		WHEN ptj.TransactionRef = 'I' AND ptj.TransactionAction = 'B' 
			THEN cr.Id 
		ELSE NULL 
	END AS ClaimFileReceiverId
	,CONVERT(UNIQUEIDENTIFIER,i.ExternalSystemMessageId) AS ExternalSystemMessageId
	,NULL AS PatientAggregateStatementId
	,i.TransactionId AS LegacyTransactionId
	FROM inserted i 
	INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionId = i.TransactionId
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = ptj.TransactionTypeId
	INNER JOIN model.InvoiceReceivables irPatient ON irPatient.InvoiceId = ir.InvoiceId
		AND irPatient.PatientInsuranceId IS NULL
	LEFT JOIN model.PatientInsurances pi ON ir.PatientInsuranceId = pi.Id
	LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	LEFT JOIN dbo.PracticeInsurers pri on pri.InsurerId = ip.InsurerId
	LEFT JOIN dbo.PracticeCodeTable pctMethod ON pri.InsurerTFormat = SUBSTRING(pctMethod.Code, 1, 1) 
		AND pctMethod.ReferenceType = 'TRANSMITTYPE'
	LEFT JOIN model.ClaimFileReceivers cr ON cr.LegacyPracticeCodeTableId = pctMethod.Id
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ServiceTransactionsUpdate' AND parent_id = OBJECT_ID('dbo.ServiceTransactions','V'))
	DROP TRIGGER ServiceTransactionsUpdate 
GO

CREATE TRIGGER ServiceTransactionsUpdate ON dbo.ServiceTransactions
INSTEAD OF UPDATE
AS
BEGIN

IF EXISTS (
SELECT * FROM inserted
EXCEPT 
SELECT * FROM deleted
)
	BEGIN
		SET NOCOUNT ON
		UPDATE bst
		SET bst.BillingServiceId = i.ServiceId
		,bst.AmountSent = i.Amount
		,bst.BillingServiceTransactionStatusId = CASE 
			WHEN ptj.TransactionStatus = 'P'
				AND i.TransportAction IN ('B', 'P')
				THEN 1
			WHEN ptj.TransactionStatus = 'P'
				AND i.TransportAction = 'V'
				THEN 3
			WHEN ptj.TransactionStatus = 'S'
				AND i.TransportAction = 'P'
				THEN 2
			WHEN ptj.TransactionStatus = 'S'
				AND i.TransportAction = 'V'
				THEN 3
			WHEN ptj.TransactionStatus = 'S'
				AND i.TransportAction = 'X'
				THEN 4
			WHEN ptj.TransactionStatus = 'Z'
				AND i.TransportAction = '1'
				AND d.TransportAction = '0'
				THEN bst.BillingServiceTransactionStatusId
			WHEN ptj.TransactionStatus = 'Z'
				AND i.TransportAction = '1'
				THEN 6
			WHEN ptj.TransactionStatus = 'Z'
				AND i.TransportAction = '2'
				THEN 7
			ELSE 5
		END
		,bst.[DateTime] = CONVERT(DATETIME,i.ModTime,112)
		,bst.ExternalSystemMessageId = i.ExternalSystemMessageId
		FROM model.BillingServiceTransactions bst
		JOIN inserted i ON i.Id = bst.Id
		LEFT JOIN deleted d ON d.Id = bst.Id
		JOIN PracticeTransactionJournal ptj ON i.TransactionId = ptj.TransactionId

		UPDATE ptj
		SET ptj.TransactionDate = i.TransactionDate
		FROM PracticeTransactionJournal ptj
		JOIN inserted i ON i.TransactionId = ptj.TransactionId
	END
END
GO

CREATE TRIGGER ServiceTransactionsDelete ON dbo.ServiceTransactions
INSTEAD OF DELETE
AS
BEGIN

	DELETE bst
	FROM model.BillingServiceTransactions bst
	INNER JOIN deleted d ON d.Id = bst.Id

END
GO

IF OBJECT_ID('dbo.GetInsurerIdInfoByItemIdAppointmentId', 'P') IS NOT NULL
	DROP PROCEDURE dbo.GetInsurerIdInfoByItemIdAppointmentId;
GO

CREATE PROCEDURE dbo.GetInsurerIdInfoByItemIdAppointmentId 
	@AppointmentId INT
	,@ItemId INT
AS
BEGIN
SET NOCOUNT ON;

SELECT Id INTO #InvoiceReceivableId FROM dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId)

;WITH bstsExistsForInvoiceId  AS (
	SELECT 
	ir.InvoiceId
	,COUNT(*) AS NumOccurances
	FROM model.BillingServiceTransactions bst
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId
	GROUP BY ir.InvoiceId
)
SELECT
ir.Id AS ReceivableId
,CASE
	WHEN ir.OpenForReview = 0
		THEN 
			CASE
				WHEN bsti.NumOccurances IS NOT NULL
					THEN 'B'
				ELSE 'S'
			END
	WHEN ir.OpenForReview = 1
		THEN 'I'
END AS ReceivableType
,i.LegacyPatientReceivablesPatientId AS PatientId
,ip.PolicyHolderPatientId AS InsuredId
,ip.InsurerId
,i.LegacyPatientReceivablesAppointmentId AS AppointmentId
,i.LegacyPatientReceivablesInvoice AS Invoice
,pins.InsurerId
,ptj.*
,bst.Id
,bst.LegacyTransactionId AS TransactionId
,bst.BillingServiceId AS ServiceId
,ptj.TransactionDate AS TransactionDate
,bst.AmountSent AS Amount
,CASE  
	WHEN (bst.BillingServiceTransactionStatusId = 1 AND ptj.TransactionStatus = 'P')
		THEN CASE 
				WHEN bst.AmountSent > 0
					THEN 'P' 
				ELSE 'B' 
			END
	WHEN (bst.BillingServiceTransactionStatusId = 2 AND ptj.TransactionStatus = 'S'
		AND bst.AmountSent = 0)
		THEN 'B'
	WHEN (bst.BillingServiceTransactionStatusId = 3 AND ptj.TransactionStatus IN ('P','S'))
		THEN 'V'
	WHEN (bst.BillingServiceTransactionStatusId = 2 AND ptj.TransactionStatus IN ('P','S'))
		THEN 'P'
	WHEN (bst.BillingServiceTransactionStatusId = 4 AND ptj.TransactionStatus = 'S')
		THEN 'X'
	WHEN (bst.BillingServiceTransactionStatusId = 6 AND ptj.TransactionStatus IN ('S','Z'))
		THEN '1'
	WHEN (bst.BillingServiceTransactionStatusId = 7 AND ptj.TransactionStatus = 'Z')
		THEN '2'
	ELSE '0'
END AS TransportAction
,bst.DateTime AS ModTime
,CONVERT(UNIQUEIDENTIFIER,NULL) AS ExternalSystemMessageId
FROM model.InvoiceReceivables ir
INNER JOIN #InvoiceReceivableId irs ON irs.Id = ir.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
LEFT JOIN bstsExistsForInvoiceId bsti ON bsti.InvoiceId = i.Id
INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
INNER JOIN dbo.PracticeInsurers pins ON pins.InsurerId = ip.InsurerId
INNER JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionTypeId = irs.Id
	AND ptj.TransactionType = 'R'
	AND ptj.TransactionRef <> 'G'
	AND ptj.TransactionStatus <> 'Z'
INNER JOIN model.BillingServiceTransactions bst ON bst.InvoiceReceivableId = ir.Id
	AND bst.LegacyTransactionId = ptj.TransactionId
WHERE bst.BillingServiceId = @ItemId
ORDER BY ptj.TransactionId

END
GO