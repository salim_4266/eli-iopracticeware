﻿--Bug #12002.Adding Immunizations Script.

--Creating Tables
IF NOT EXISTS (
		SELECT *
		FROM SYSOBJECTS
		WHERE XTYPE = 'U'
			AND NAME = 'CPT_CVX'
		)
BEGIN
	CREATE TABLE [CPT_CVX] (
		[CPT_CODE] [nvarchar](255) NULL
		,[CPT_description] [nvarchar](255) NULL
		,[CPT_code_status] [nvarchar](255) NULL
		,[Short Description] [nvarchar](255) NULL
		,[CVX Code] [nvarchar](255) NULL
		,[comment] [nvarchar](255) NULL
		,[last_updated] [datetime] NULL
		,[CPT_code_ID] [int] NULL
		,[Id] [int] IDENTITY(1, 1) NOT NULL
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[Patient_Contacts]')
			AND type IN (N'U')
		)
	CREATE TABLE [dbo].[Patient_Contacts] (
		[PatContactID] [int] IDENTITY(1, 1) NOT FOR REPLICATION NOT NULL
		,[PatientID] [int] NULL
		,[ContactNo] [int] NULL
		,[Contact_FirstName] [varchar](40) NULL
		,[Contact_LastName] [varchar](40) NULL
		,[Contact_MI] [char](20) NULL
		,[Contact_Address1] [varchar](40) NULL
		,[Contact_Address2] [varchar](40) NULL
		,[Contact_City] [varchar](50) NULL
		,[Contact_State] [varchar](50) NULL
		,[Contact_Zip] [varchar](20) NULL
		,[Contact_Phone] [varchar](20) NULL
		,[Contact_CellPhone] [varchar](20) NULL
		,[Contact_RelationWithPatient] [varchar](50) NULL
		,[Contact_Type] [varchar](10) NULL
		,[Comments] [varchar](100) NULL
		,CONSTRAINT [PK_Patient_contacts] PRIMARY KEY CLUSTERED ([PatContactID] ASC)
		) ON [PRIMARY]
GO

IF NOT EXISTS (
		SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'dbo'
			AND TABLE_NAME = 'ImmunizationOBX'
		)
BEGIN
	CREATE TABLE [dbo].[ImmunizationOBX] (
		[EncounterID] [int] NULL
		,[DateGiven] [datetime] NULL
		,[VaccineName] [varchar](100) NOT NULL
		,[ImmOBXXML] [xml] NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[Immunization_HL7Data]')
			AND type IN (N'U')
		)
BEGIN
	CREATE TABLE [dbo].[Immunization_HL7Data] (
		[id] [int] IDENTITY(1, 1) NOT NULL
		,[FieldName] [varchar](100) NULL
		,[FieldValue] [varchar](100) NULL
		,CONSTRAINT [PK_Immunization_HL7Data] PRIMARY KEY CLUSTERED ([id] ASC)
		) ON [PRIMARY]
END

IF NOT EXISTS (
		SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'dbo'
			AND TABLE_NAME = 'ImmOBXInfo'
		)
BEGIN
	CREATE TABLE [dbo].[ImmOBXInfo] (
		[OBXId] [int] IDENTITY(1, 1) NOT NULL
		,[OBXCode] [varchar](10) NULL
		,[OBXDesc] [varchar](100) NULL
		,[OBXDataType] [varchar](3) NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[Manufacturer]')
			AND type IN (N'U')
		)
BEGIN
	CREATE TABLE [dbo].[Manufacturer] (
		[ManfId] [int] IDENTITY(1, 1) NOT FOR REPLICATION NOT NULL
		,[Code] [varchar](10) NOT NULL
		,[ManufacturerName] [varchar](200) NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[Immunizations]')
			AND NAME = 'COMPONENT'
		)
	ALTER TABLE [dbo].[Immunizations] ADD [Component] [int] NULL
GO

--Stored Procedure
IF EXISTS (
		SELECT *
		FROM sysobjects
		WHERE NAME = 'Sp_PatientMotherDetails'
			AND xtype = 'P'
		)
	DROP PROCEDURE Sp_PatientMotherDetails
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[ENCOUNTER_IMMUNIZATION]')
			AND type IN (N'U')
		)
BEGIN
	CREATE TABLE [dbo].[ENCOUNTER_IMMUNIZATION] (
		[id] [int] IDENTITY(1, 1) NOT NULL
		,[EncounterID] [int] NOT NULL
		,[IMMUNIZATION] [xml] NOT NULL
		,CONSTRAINT [PK_ENCOUNTER_IMMUNIZATION] PRIMARY KEY CLUSTERED ([id] ASC) WITH (
			PAD_INDEX = OFF
			,STATISTICS_NORECOMPUTE = OFF
			,IGNORE_DUP_KEY = OFF
			,ALLOW_ROW_LOCKS = ON
			,ALLOW_PAGE_LOCKS = ON
			) ON [PRIMARY]
		) ON [PRIMARY]
END
GO

CREATE PROCEDURE Sp_PatientMotherDetails (@PatID AS INT)
AS
BEGIN
	SELECT Contact_LastName
		,Contact_FirstName
		,Contact_Address1
		,Contact_Address2
		,Contact_City
		,Contact_State
		,Contact_Zip
		,Contact_Phone
		,'USA' AS Contact_Country
	FROM Patient_contacts
	WHERE PatientID = @PatID
		AND Contact_RelationWithPatient LIKE '%Mother'
END
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[Immunizations]')
			AND NAME = 'Active_YN'
		)
	ALTER TABLE [dbo].[Immunizations] ADD [Active_YN] [bit] NULL DEFAULT 1
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[Immunizations]')
			AND NAME = 'ImmType'
		)
	ALTER TABLE [dbo].[Immunizations] ADD [ImmType] [varchar] (30) NULL
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[Immunizations]')
			AND NAME = 'ICD'
		)
	ALTER TABLE [dbo].[Immunizations] ADD [ICD] [varchar] (50) NULL
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[Immunizations]')
			AND NAME = 'CPT'
		)
	ALTER TABLE [dbo].[Immunizations] ADD [CPT] [varchar] (50) NULL
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[Immunizations]')
			AND NAME = 'IsDeletedYN'
		)
	ALTER TABLE [dbo].[Immunizations] ADD [IsDeletedYN] [bit] NULL
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[Immunizations]')
			AND NAME = 'OldId'
		)
	ALTER TABLE [dbo].[Immunizations] ADD [OldId] [int] NULL
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[Patient_Immunizations]')
			AND NAME = 'ImmOrderPhy'
		)
	ALTER TABLE [dbo].[Patient_Immunizations] ADD [ImmOrderPhy] [varchar] (30) NULL
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[Patient_Immunizations]')
			AND NAME = 'ImmRoute'
		)
	ALTER TABLE [dbo].[Patient_Immunizations] ADD [ImmRoute] [varchar] (30) NULL
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[Patient_Immunizations]')
			AND NAME = 'ImmAdministered'
		)
	ALTER TABLE [dbo].[Patient_Immunizations] ADD [ImmAdministered] [varchar] (30) NULL
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[Patient_Immunizations]')
			AND NAME = 'ImmStatus'
		)
	ALTER TABLE [dbo].[Patient_Immunizations] ADD [ImmStatus] [varchar] (20) NULL
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[Patient_Immunizations]')
			AND NAME = 'ImmRefReason'
		)
	ALTER TABLE [dbo].[Patient_Immunizations] ADD [ImmRefReason] [varchar] (50) NULL
GO

IF NOT EXISTS (
		SELECT *
		FROM Immunization_HL7Data
		WHERE FieldName = 'MSH.7.1'
		)
	INSERT INTO Immunization_HL7Data (
		[FieldName]
		,[FieldValue]
		)
	VALUES (
		'MSH.7.1'
		,''
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Immunization_HL7Data
		WHERE FieldName = 'MSH 10'
		)
	INSERT INTO Immunization_HL7Data (
		[FieldName]
		,[FieldValue]
		)
	VALUES (
		'MSH 10'
		,''
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Immunization_HL7Data
		WHERE FieldName = 'MSH 11.1'
		)
	INSERT INTO Immunization_HL7Data (
		[FieldName]
		,[FieldValue]
		)
	VALUES (
		'MSH 11.1'
		,'P'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Immunization_HL7Data
		WHERE FieldName = 'MSH 15'
		)
	INSERT INTO Immunization_HL7Data (
		[FieldName]
		,[FieldValue]
		)
	VALUES (
		'MSH 15'
		,'AL'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Immunization_HL7Data
		WHERE FieldName = 'MSH 16'
		)
	INSERT INTO Immunization_HL7Data (
		[FieldName]
		,[FieldValue]
		)
	VALUES (
		'MSH 16'
		,'ER'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Immunization_HL7Data
		WHERE FieldName = 'PID 3.4'
		)
	INSERT INTO Immunization_HL7Data (
		[FieldName]
		,[FieldValue]
		)
	VALUES (
		'PID 3.4'
		,'NIST MPI'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Immunization_HL7Data
		WHERE FieldName = 'ORC 3.1'
		)
	INSERT INTO Immunization_HL7Data (
		[FieldName]
		,[FieldValue]
		)
	VALUES (
		'ORC 3.1'
		,'IZ-783278'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Immunization_HL7Data
		WHERE FieldName = 'ORC 3.2'
		)
	INSERT INTO Immunization_HL7Data (
		[FieldName]
		,[FieldValue]
		)
	VALUES (
		'ORC 3.2'
		,'NDA'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Immunization_HL7Data
		WHERE FieldName = 'ORC 12.9'
		)
	INSERT INTO Immunization_HL7Data (
		[FieldName]
		,[FieldValue]
		)
	VALUES (
		'ORC 12.9'
		,'NIST-AA-1'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Immunization_HL7Data
		WHERE FieldName = 'OBX 4'
		)
	INSERT INTO Immunization_HL7Data (
		[FieldName]
		,[FieldValue]
		)
	VALUES (
		'OBX 4'
		,''
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Immunization_HL7Data
		WHERE FieldName = 'OBX 14'
		)
	INSERT INTO Immunization_HL7Data (
		[FieldName]
		,[FieldValue]
		)
	VALUES (
		'OBX 14'
		,''
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Immunization_HL7Data
		WHERE FieldName = 'OBX 17.2'
		)
	INSERT INTO Immunization_HL7Data (
		[FieldName]
		,[FieldValue]
		)
	VALUES (
		'OBX 17.2'
		,''
		)
GO


GO

SET QUOTED_IDENTIFIER OFF
GO

SET IDENTITY_INSERT CPT_CVX ON
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90281'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90281'
		,'Immune globulin (IG), human, for intramuscular use'
		,'Active'
		,'IG'
		,'86'
		,NULL
		,NULL
		,169
		,1
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90283'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90283'
		,'Immune globulin (IGIV), human, for intravenous 

use'
		,'Active'
		,'IGIV'
		,'87'
		,NULL
		,NULL
		,170
		,2
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90287'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90287'
		,'Botulinum antitoxin, equine, any route'
		,'Active'
		,'botulinum 

antitoxin'
		,'27'
		,NULL
		,NULL
		,171
		,3
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90291'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90291'
		,'Cytomegalovirus immune globulin (CMV-IGIV), human, for intravenous 

use'
		,'Active'
		,'CMVIG'
		,'29'
		,NULL
		,NULL
		,172
		,4
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90296'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90296'
		,'Diphtheria antitoxin, equine, any route'
		,'Active'
		,'diphtheria 

antitoxin'
		,'12'
		,NULL
		,NULL
		,173
		,5
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90371'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90371'
		,'Hepatitis B immune globulin (HBIG), human, for intramuscular 

use'
		,'Active'
		,'HBIG'
		,'30'
		,NULL
		,NULL
		,174
		,6
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90375'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90375'
		,'Rabies immune globulin (RIG), human, for intramuscular and/or subcutaneous 

use'
		,'Active'
		,'RIG'
		,'34'
		,NULL
		,NULL
		,175
		,7
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90376'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90376'
		,'Rabies immune globulin, heat-treated (RIG-HT), human, for intramuscular and/or 

subcutaneous use'
		,'Active'
		,'RIG'
		,'34'
		,NULL
		,NULL
		,176
		,8
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90378'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90378'
		,'Respiratory syncytial virus immune globulin (RSV-IgIM), for intramuscular use, 50 mg, 

each'
		,'Active'
		,'RSV-MAb'
		,'93'
		,NULL
		,NULL
		,177
		,9
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90379'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90379'
		,'Respiratory syncytial virus immune globulin (RSV-IGIV), human, for intravenous 

use'
		,'Active'
		,'RSV-IGIV'
		,'71'
		,NULL
		,NULL
		,178
		,10
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90389'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90389'
		,'Tetanus immune globulin (TIG), human, for intramuscular 

use'
		,'Active'
		,'TIG'
		,'13'
		,NULL
		,NULL
		,179
		,11
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90393'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90393'
		,'Vaccinia immune globulin, human, for intramuscular use'
		,'Active'
		,'vaccinia immune 

globulin'
		,'79'
		,NULL
		,NULL
		,180
		,12
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90396'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90396'
		,'Varicella-zoster immune globulin, human, for intramuscular 

use'
		,'Active'
		,'VZIG'
		,'36'
		,NULL
		,NULL
		,150
		,13
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90470'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90470'
		,'H1N1 immunization administration (intramuscular, intranasal), including counseling when 

performed'
		,'Active'
		,'Novel Influenza-H1N1-09, all formulations'
		,'128'
		,NULL
		,NULL
		,297
		,14
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90476'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90476'
		,'Adenovirus vaccine, type 4, live, for oral use'
		,'Active'
		,'adenovirus, type 

4'
		,'54'
		,NULL
		,NULL
		,151
		,15
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90477'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90477'
		,'Adenovirus vaccine, type 7, live, for oral use'
		,'Active'
		,'adenovirus, type 

7'
		,'55'
		,NULL
		,NULL
		,152
		,16
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90581'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90581'
		,'Anthrax vaccine, for subcutaneous use'
		,'Active'
		,'anthrax'
		,'24'
		,NULL
		,NULL
		,153
		,17
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90585'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90585'
		,'Bacillus Calmette-Guerin vaccine (BCG) for tuberculosis, live, for percutaneous 

use'
		,'Active'
		,'BCG'
		,'19'
		,NULL
		,NULL
		,154
		,18
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90632'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90632'
		,'Hepatitis A vaccine, adult dosage, for intramuscular use'
		,'Active'
		,'Hep A, 

adult'
		,'52'
		,NULL
		,NULL
		,155
		,19
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90633'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90633'
		,'Hepatitis A vaccine, pediatric/adolescent dosage-2 dose schedule, for intramuscular 

use'
		,'Active'
		,'Hep A, ped/adol, 2 dose'
		,'83'
		,NULL
		,NULL
		,156
		,20
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90634'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90634'
		,'Hepatitis A vaccine, pediatric/adolescent dosage-3 dose schedule, for intramuscular 

use'
		,'Active'
		,'Hep A, ped/adol, 3 dose'
		,'84'
		,NULL
		,NULL
		,157
		,21
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90636'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90636'
		,'Hepatitis A and hepatitis B (HepA-HepB), adult dosage, for intramuscular 

use'
		,'Active'
		,'Hep A-Hep B'
		,'104'
		,NULL
		,NULL
		,158
		,22
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90645'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90645'
		,'Haemophilus influenza b vaccine (Hib), HbOC conjugate (4 dose schedule), for 

intramuscular use'
		,'Active'
		,'Hib (HbOC)'
		,'47'
		,NULL
		,NULL
		,159
		,23
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90646'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90646'
		,'Haemophilus influenza b vaccine (Hib), PRP-D conjugate, for booster use only, 

intramuscular use'
		,'Active'
		,'Hib (PRP-D)'
		,'46'
		,NULL
		,NULL
		,160
		,24
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90647'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90647'
		,'Haemophilus influenza b vaccine (Hib), PRP-OMP conjugate (3 dose schedule), for 

intramuscular use'
		,'Active'
		,'Hib (PRP-OMP)'
		,'49'
		,NULL
		,NULL
		,161
		,25
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90648'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90648'
		,'Haemophilus influenza b vaccine (Hib), PRP-T conjugate (4 dose schedule), for 

intramuscular use'
		,'Active'
		,'Hib (PRP-T)'
		,'48'
		,NULL
		,NULL
		,162
		,26
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90649'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90649'
		,'Human Papilloma virus (HPV) vaccine, types 6, 11, 16, 18 (quadrivalent) 3 dose schedule, 

for intramuscular use'
		,'Active'
		,'HPV, quadrivalent'
		,'62'
		,NULL
		,NULL
		,163
		,27
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90650'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90650'
		,'Human Papilloma virus (HPV) vaccine, types 16, 18, bivalent, 3 dose schedule, for 

intramuscular use'
		,'Active'
		,'HPV, bivalent'
		,'118'
		,NULL
		,NULL
		,164
		,28
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90654'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90654'
		,'Influenza virus vaccine, split virus, preservative free, for intradermal 

use'
		,'Active'
		,'influenza, seasonal, intradermal, preservative free'
		,'144'
		,NULL
		,NULL
		,302
		,29
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90655'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90655'
		,'Influenza virus vaccine, split virus, preservative free, for children 6-35 months of 

age, for intramuscular use'
		,'Active'
		,'Influenza, seasonal, injectable, preservative free'
		,'140'
		,NULL
		,NULL
		,165
		,30
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90657'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90657'
		,'Influenza virus vaccine, split virus, for children 6-35 months of age, for intramuscular 

use'
		,'Active'
		,'Influenza, seasonal, injectable'
		,'141'
		,NULL
		,NULL
		,167
		,32
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90658'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90658'
		,'Influenza virus vaccine, split virus, for use in individuals 3 years of age and above, 

for intramuscular use'
		,'Active'
		,'Influenza, seasonal, injectable'
		,'141'
		,NULL
		,NULL
		,168
		,33
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90656'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90656'
		,'Influenza virus vaccine, split virus, preservative free, for use in individuals 3 years 

of age and above, for intramuscular use'
		,'Active'
		,'Influenza, seasonal, injectable, preservative 

free'
		,'140'
		,NULL
		,NULL
		,166
		,31
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90659'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90659'
		,'Influenza virus vaccine, whole virus, for intramuscular or jet injection 

use'
		,'Active'
		,'influenza, whole'
		,'16'
		,NULL
		,NULL
		,181
		,34
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90660'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90660'
		,'Influenza virus vaccine, live, for intranasal use'
		,'Active'
		,'influenza, live, 

intranasal'
		,'111'
		,NULL
		,NULL
		,182
		,35
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90662'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90662'
		,'Influenza virus vaccine, split virus, preservative free, enhanced immunogenicity via 

increased antigen content, for intramuscular use'
		,'Active'
		,'Influenza, high dose seasonal'
		,'135'
		,NULL
		,NULL
		,292
		,36
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90663'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90663'
		,'Influenza virus vaccine, pandemic formulation, H1N1'
		,'Active'
		,'Novel Influenza-H1N1-09, 

all formulations'
		,'128'
		,NULL
		,NULL
		,293
		,37
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90664'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90664'
		,'Influenza virus vaccine, pandemic formulation, live, for intranasal use'
		,'Active'
		,'Novel 

Influenza-H1N1-09, nasal'
		,'125'
		,NULL
		,NULL
		,294
		,38
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90665'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90665'
		,'Lyme disease vaccine, adult dosage, for intramuscular use'
		,'Active'
		,'Lyme 

disease'
		,'66'
		,NULL
		,NULL
		,183
		,39
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90666'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90666'
		,'Influenza virus vaccine, pandemic formulation, split-virus, preservative free, for 

intramuscular use'
		,'Active'
		,'Novel influenza-H1N1-09, preservative-free'
		,'126'
		,NULL
		,NULL
		,300
		,40
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90668'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90668'
		,'Influenza virus vaccine, pandemic formulation, split-virus, for intramuscular 

use'
		,'Active'
		,'Novel influenza-H1N1-09'
		,'127'
		,NULL
		,NULL
		,296
		,41
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90669'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90669'
		,'Pneumococcal conjugate vaccine, polyvalent, for children under five years, for 

intramuscular use'
		,'Active'
		,'pneumococcal conjugate PCV 7'
		,'100'
		,NULL
		,NULL
		,184
		,42
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90670'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90670'
		,'Pneumococcal conjugate vaccine, 13 valent, for intramuscular use'
		,'Active'
		,'Pneumococcal 

conjugate PCV 13'
		,'133'
		,NULL
		,NULL
		,185
		,43
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90675'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90675'
		,'Rabies vaccine, for intramuscular use'
		,'Active'
		,'rabies, intramuscular 

injection'
		,'18'
		,NULL
		,NULL
		,186
		,44
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90676'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90676'
		,'Rabies vaccine, for intradermal use'
		,'Active'
		,'rabies, intradermal 

injection'
		,'40'
		,NULL
		,NULL
		,187
		,45
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90680'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90680'
		,'Rotavirus vaccine, pentavalent, 3 dose schedule, live, for oral 

use'
		,'Active'
		,'rotavirus, pentavalent'
		,'116'
		,NULL
		,NULL
		,188
		,46
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90681'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90681'
		,'Rotavirus vaccine, human, attenuated, 2 dose schedule, live, for oral 

use'
		,'Active'
		,'rotavirus, monovalent'
		,'119'
		,NULL
		,NULL
		,189
		,47
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90690'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90690'
		,'Typhoid vaccine, live, oral'
		,'Active'
		,'typhoid, oral'
		,'25'
		,NULL
		,NULL
		,190
		,48
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90691'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90691'
		,'Typhoid vaccine, Vi capsular polysaccharide (ViCPs), for intramuscular 

use'
		,'Active'
		,'typhoid, ViCPs'
		,'101'
		,NULL
		,NULL
		,191
		,49
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90692'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90692'
		,'Typhoid vaccine, heat- and phenol-inactivated (H-P), for subcutaneous or intradermal 

use'
		,'Active'
		,'typhoid, parenteral'
		,'41'
		,NULL
		,NULL
		,192
		,50
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90693'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90693'
		,'Typhoid vaccine, acetone-killed, dried (AKD), for subcutaneous use (U.S. 

military)'
		,'Active'
		,'typhoid, parenteral, AKD (U.S. military)'
		,'53'
		,NULL
		,NULL
		,193
		,51
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90696'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90696'
		,'Diphtheria, tetanus toxoids, acellular pertussis vaccine and poliovirus vaccine, 

inactivated (DTaP-IPV), when administered to children 4 years through 6 years of age, for intramuscular use'
		,'Active'
		,'DTaP-

IPV'
		,'130'
		,NULL
		,NULL
		,194
		,52
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90698'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90698'
		,'Diphtheria, tetanus toxoids, and acellular pertussis vaccine, haemophilus influenza Type 

B, and poliovirus vaccine, inactivated (DTaP - Hib - IPV), for intramuscular use'
		,'Active'
		,'DTaP-Hib-

IPV'
		,'120'
		,NULL
		,NULL
		,195
		,53
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90700'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90700'
		,'Diphtheria, tetanus toxoids, and acellular pertussis vaccine (DTaP), for use in 

individuals younger than seven years, for intramuscular use'
		,'Active'
		,'DTaP, 5 pertussis antigens'
		,'106'
		,NULL
		,NULL
		,301
		,54
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90701'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90701'
		,'Diphtheria, tetanus toxoids, and whole cell pertussis vaccine (DTP), for intramuscular 

use'
		,'Active'
		,'DTP'
		,'01'
		,NULL
		,NULL
		,197
		,55
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90702'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90702'
		,'Diphtheria and tetanus toxoids (DT) adsorbed for use in individuals younger than seven 

years, for intramuscular use'
		,'Active'
		,'DT (pediatric)'
		,'28'
		,NULL
		,NULL
		,198
		,56
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90703'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90703'
		,'Tetanus toxoid adsorbed, for intramuscular use'
		,'Active'
		,'tetanus toxoid, 

adsorbed'
		,'35'
		,NULL
		,NULL
		,199
		,57
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90704'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90704'
		,'Mumps virus vaccine, live, for subcutaneous use'
		,'Active'
		,'mumps'
		,'07'
		,NULL
		,NULL
		,200
		,58
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90705'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90705'
		,'Measles virus vaccine, live, for subcutaneous 

use'
		,'Active'
		,'measles'
		,'05'
		,NULL
		,NULL
		,201
		,59
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90706'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90706'
		,'Rubella virus vaccine, live, for subcutaneous 

use'
		,'Active'
		,'rubella'
		,'06'
		,NULL
		,NULL
		,202
		,60
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90707'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90707'
		,'Measles, mumps and rubella virus vaccine (MMR), live, for subcutaneous 

use'
		,'Active'
		,'MMR'
		,'03'
		,NULL
		,NULL
		,203
		,61
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90708'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90708'
		,'Measles and rubella virus vaccine, live, for subcutaneous 

use'
		,'Active'
		,'M/R'
		,'04'
		,NULL
		,NULL
		,204
		,62
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90710'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90710'
		,'Measles, mumps, rubella, and varicella vaccine (MMRV), live, for subcutaneous 

use'
		,'Active'
		,'MMRV'
		,'94'
		,NULL
		,NULL
		,205
		,63
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90712'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90712'
		,'Poliovirus vaccine, (any type(s)) (OPV), live, for oral 

use'
		,'Active'
		,'OPV'
		,'02'
		,NULL
		,NULL
		,206
		,64
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90711'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90711'
		,'Poliovirus vaccine, inactivated, (IPV), for subcutaneous or intramuscular 

use'
		,'Active'
		,'IPV'
		,'10'
		,NULL
		,NULL
		,207
		,65
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90714'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90714'
		,'Tetanus and diphtheria toxoids (Td) adsorbed, preservative free, for use in individuals 

seven years or older, for intramuscular use'
		,'Active'
		,'Td (adult) preservative free'
		,'113'
		,NULL
		,NULL
		,208
		,66
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90715'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90715'
		,'Tetanus, diphtheria toxoids and acellular pertussis vaccine (Tdap), for use in 

individuals 7 years or older, for intramuscular use'
		,'Active'
		,'Tdap'
		,'115'
		,NULL
		,NULL
		,210
		,67
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90716'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90716'
		,'Varicella virus vaccine, live, for subcutaneous 

use'
		,'Active'
		,'varicella'
		,'21'
		,NULL
		,NULL
		,211
		,68
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90717'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90717'
		,'Yellow fever vaccine, live, for subcutaneous use'
		,'Active'
		,'yellow 

fever'
		,'37'
		,NULL
		,NULL
		,212
		,69
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90718'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90718'
		,'Tetanus and diphtheria toxoids (Td) adsorbed for use in individuals seven years or 

older, for intramuscular use'
		,'Active'
		,'Td (adult), adsorbed'
		,'09'
		,NULL
		,NULL
		,213
		,70
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90720'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90720'
		,'Diphtheria, tetanus toxoids, and whole cell pertussis vaccine and Hemophilus influenza B 

vaccine (DTP-Hib), for intramuscular use'
		,'Active'
		,'DTP-Hib'
		,'22'
		,NULL
		,NULL
		,214
		,71
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90721'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90721'
		,'Diphtheria, tetanus toxoids, and acellular pertussis vaccine and Hemophilus influenza B 

vaccine (DTaP-Hib), for intramuscular use'
		,'Active'
		,'DTaP-Hib'
		,'50'
		,NULL
		,NULL
		,215
		,72
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90723'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90723'
		,'Diphtheria, tetanus toxoids, acellular pertussis vaccine, Hepatitis B, and poliovirus 

vaccine, inactivated (DTaP-HepB-IPV), for intramuscular use'
		,'Active'
		,'DTaP-Hep B-IPV'
		,'110'
		,NULL
		,NULL
		,216
		,73
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90724'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90724'
		,'Influenza virus vaccine'
		,'Active'
		,'influenza, unspecified 

formulation'
		,'88'
		,NULL
		,NULL
		,217
		,74
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90725'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90725'
		,'Cholera vaccine for injectable use'
		,'Active'
		,'cholera'
		,'26'
		,NULL
		,NULL
		,218
		,75
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90726'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90726'
		,'Rabies vaccine'
		,'Active'
		,'rabies, unspecified formulation'
		,'90'
		,NULL
		,NULL
		,219
		,76
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90727'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90727'
		,'Plague vaccine, for intramuscular use'
		,'Active'
		,'plague'
		,'23'
		,NULL
		,NULL
		,220
		,77
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90728'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90728'
		,'BCG vaccine'
		,'Active'
		,'BCG'
		,'19'
		,NULL
		,NULL
		,221
		,78
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90730'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90730'
		,'Hepatitis A vaccine'
		,'Active'
		,'Hep A, unspecified formulation'
		,'85'
		,NULL
		,NULL
		,222
		,79
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90731'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90731'
		,'Hepatitis B vaccine'
		,'Active'
		,'Hep B, unspecified formulation'
		,'45'
		,NULL
		,NULL
		,223
		,80
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90732'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90732'
		,'Pneumococcal polysaccharide vaccine, 23-valent, adult or immunosuppressed patient 

dosage, for use in individuals 2 years or older, for subcutaneous or intramuscular use'
		,'Active'
		,'pneumococcal polysaccharide 

PPV23'
		,'33'
		,NULL
		,NULL
		,224
		,81
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90733'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90733'
		,'Meningococcal polysaccharide vaccine (any group(s)), for subcutaneous 

use'
		,'Active'
		,'meningococcal MPSV4'
		,'32'
		,NULL
		,NULL
		,225
		,82
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90734'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90734'
		,'Meningococcal conjugate vaccine, serogroups A, C, Y and W-135 (tetravalent), for 

intramuscular use'
		,'Active'
		,'Meningococcal MCV4O'
		,'136'
		,'This CPT maps to 2 vaccines (MCV4P and MCV4O)'
		,NULL
		,285
		,83
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90735'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90735'
		,'Japanese encephalitis virus vaccine, for subcutaneous use'
		,'Active'
		,'Japanese 

encephalitis SC'
		,'39'
		,NULL
		,NULL
		,227
		,84
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90736'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90736'
		,'Zoster (shingles) vaccine, live, for subcutaneous 

injection'
		,'Active'
		,'zoster'
		,'121'
		,NULL
		,NULL
		,228
		,85
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90729'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90729'
		,'Hemophilus influenza B'
		,'Active'
		,'Hib, unspecified formulation'
		,'17'
		,NULL
		,NULL
		,229
		,86
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90738'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90738'
		,'Japanese encephalitis virus vaccine, inactivated, for intramuscular 

use'
		,'Active'
		,'Japanese Encephalitis IM'
		,'134'
		,NULL
		,NULL
		,230
		,87
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90740'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90740'
		,'Hepatitis B vaccine, dialysis or immunosuppressed patient dosage (3 dose schedule), for 

intramuscular use'
		,'Active'
		,'Hep B, dialysis'
		,'44'
		,NULL
		,NULL
		,231
		,88
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90741'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90741'
		,'Immunization, passive; immune serum globulin, human (ISG)'
		,'Active'
		,'IG, unspecified 

formulation'
		,'14'
		,NULL
		,NULL
		,232
		,89
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90743'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90743'
		,'Hepatitis B vaccine, adolescent (2 dose schedule), for intramuscular use'
		,'Active'
		,'Hep 

B, adult'
		,'43'
		,NULL
		,NULL
		,233
		,90
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90744'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90744'
		,'Hepatitis B vaccine, pediatric/adolescent dosage (3 dose schedule), for intramuscular 

use'
		,'Active'
		,'Hep B, adolescent or pediatric'
		,'08'
		,NULL
		,NULL
		,234
		,91
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90745'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90745'
		,'Hepatitis B vaccine, adolescent/high risk infant dosage, for intramuscular 

use'
		,'Active'
		,'Hep B, adolescent/high risk infant'
		,'42'
		,NULL
		,NULL
		,235
		,92
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90746'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90746'
		,'Hepatitis B vaccine, adult dosage, for intramuscular use'
		,'Active'
		,'Hep B, 

adult'
		,'43'
		,NULL
		,NULL
		,236
		,93
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90747'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90747'
		,'Hepatitis B vaccine, dialysis or immunosuppressed patient dosage (4 dose schedule), for 

intramuscular use'
		,'Active'
		,'Hep B, dialysis'
		,'44'
		,NULL
		,NULL
		,237
		,94
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90748'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90748'
		,'Hepatitis B and Hemophilus influenza b vaccine (HepB-Hib), for intramuscular 

use'
		,'Active'
		,'Hib-Hep B'
		,'51'
		,NULL
		,NULL
		,238
		,95
		)
GO

INSERT INTO CPT_CVX (
	[CPT_CODE]
	,[CPT_description]
	,[CPT_code_status]
	,[Short Description]
	,[CVX Code]
	,[comment]
	,[last_updated]
	,[CPT_code_ID]
	,[Id]
	)
VALUES (
	''
	,'Td (adult), unspecified formulation'
	,'Active'
	,'Td (adult), unspecified 

formulation'
	,'139'
	,NULL
	,NULL
	,0
	,96
	)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90749'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90749'
		,'DTaP, unspecified formulation '
		,'Active'
		,'DTaP, unspecified formulation 

'
		,'107'
		,NULL
		,NULL
		,303
		,97
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM CPT_CVX
		WHERE CPT_CODE = '90713'
		)
	INSERT INTO CPT_CVX (
		[CPT_CODE]
		,[CPT_description]
		,[CPT_code_status]
		,[Short Description]
		,[CVX Code]
		,[comment]
		,[last_updated]
		,[CPT_code_ID]
		,[Id]
		)
	VALUES (
		'90713'
		,'polio, unspecified formulation '
		,'Active'
		,'polio, unspecified formulation 

'
		,'89'
		,NULL
		,NULL
		,304
		,98
		)
GO

SET IDENTITY_INSERT cpt_cvx OFF
GO

IF NOT EXISTS (
		SELECT *
		FROM Manufacturer
		WHERE ManufacturerName = 'CSL Behring'
		)
	INSERT INTO Manufacturer (
		[Code]
		,[ManufacturerName]
		)
	VALUES (
		'CSL'
		,'CSL Behring'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Manufacturer
		WHERE ManufacturerName = 'Merck and Co'
		)
	INSERT INTO Manufacturer (
		[Code]
		,[ManufacturerName]
		)
	VALUES (
		'MSD'
		,'Merck and Co'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Manufacturer
		WHERE ManufacturerName = 'Sanofi Pasteurlll'
		)
	INSERT INTO Manufacturer (
		[Code]
		,[ManufacturerName]
		)
	VALUES (
		'PMC'
		,'Sanofi Pasteurlll'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Manufacturer
		WHERE ManufacturerName = 'GlaxoSmithKline'
		)
	INSERT INTO Manufacturer (
		[Code]
		,[ManufacturerName]
		)
	VALUES (
		'SKB'
		,'GlaxoSmithKline'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM Manufacturer
		WHERE ManufacturerName = 'Novartis'
		)
	INSERT INTO Manufacturer (
		[Code]
		,[ManufacturerName]
		)
	VALUES (
		'NOV'
		,'Novartis'
		)
GO


GO

SET QUOTED_IDENTIFIER OFF
GO

SET IDENTITY_INSERT ImmOBXInfo ON

IF NOT EXISTS (
		SELECT *
		FROM ImmOBXInfo
		WHERE OBXCode = '30956-7'
		)
	INSERT INTO ImmOBXInfo (
		[OBXID]
		,[OBXDataType]
		,[OBXCode]
		,[OBXDesc]
		)
	VALUES (
		1
		,'CE'
		,'30956-7'
		,'vaccine type'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM ImmOBXInfo
		WHERE OBXCode = '29768-9'
		)
	INSERT INTO ImmOBXInfo (
		[OBXID]
		,[OBXDataType]
		,[OBXCode]
		,[OBXDesc]
		)
	VALUES (
		2
		,'TS'
		,'29768-9'
		,'Date vaccine information statement published'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM ImmOBXInfo
		WHERE OBXCode = '29769-7'
		)
	INSERT INTO ImmOBXInfo (
		[OBXID]
		,[OBXDataType]
		,[OBXCode]
		,[OBXDesc]
		)
	VALUES (
		3
		,'TS'
		,'29769-7'
		,'Date vaccine information statement presented'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM ImmOBXInfo
		WHERE OBXCode = '64994-7'
		)
	INSERT INTO ImmOBXInfo (
		[OBXID]
		,[OBXDataType]
		,[OBXCode]
		,[OBXDesc]
		)
	VALUES (
		4
		,'CE'
		,'64994-7'
		,'Vaccine funding program eligibility category'
		)
GO

IF NOT EXISTS (
		SELECT *
		FROM ImmOBXInfo
		WHERE OBXCode = '59784-9'
		)
	INSERT INTO ImmOBXInfo (
		[OBXID]
		,[OBXDataType]
		,[OBXCode]
		,[OBXDesc]
		)
	VALUES (
		5
		,'CE'
		,'59784-9'
		,'Disease with presumed immunity'
		)
GO

SET IDENTITY_INSERT ImmOBXInfo OFF
GO

SET QUOTED_IDENTIFIER ON
GO

--Create Row for AdminRoute.We wil have a old Row with AdminRoute if so,uodate it to Admin-Route.
IF NOT EXISTS (
		SELECT TOP 1 LOOKUPID
		FROM Lookups
		WHERE NAME = 'AdminRoute'
		)
BEGIN
	INSERT INTO Lookups
	(
	Name,
	Description,
	ICDCPTExists,
	ImagesExists
	)
	VALUES (
		'ADMIN-ROUTE'
		,'AdminRoute'
		,0
		,NULL
		)
END
ELSE
BEGIN
	UPDATE Lookups
	SET NAME = 'ADMIN-ROUTE'
		,Description = 'AdminRoute'
	WHERE NAME LIKE 'Route%'
END
GO
IF NOT EXISTS (
		SELECT TOP 1 LOOKUPID
		FROM Lookups
		WHERE NAME = 'Site'
		)
	INSERT INTO Lookups
	(
	Name,
	Description,
	ICDCPTExists,
	ImagesExists
	)
	VALUES (
		'Site'
		,'Site'
		,0
		,NULL
		)
DECLARE @Lookupid INT;

SELECT @Lookupid = lookupid
FROM lookups
WHERE NAME = 'Site'

IF NOT EXISTS (
		SELECT *
		FROM LookupValues
		WHERE lookupid = @lookupid
		)
BEGIN
	INSERT [dbo].[LookupValues] (
		[LookupID]
		,[LookupValue]
		,[Type]
		,[Code1]
		,[Code2]
		,[ISDefault]
		)
	VALUES (
		@Lookupid
		,N'left arm'
		,0
		,NULL
		,NULL
		,NULL
		)

	INSERT [dbo].[LookupValues] (
		[LookupID]
		,[LookupValue]
		,[Type]
		,[Code1]
		,[Code2]
		,[ISDefault]
		)
	VALUES (
		@Lookupid
		,N'right arm'
		,0
		,NULL
		,NULL
		,NULL
		)
END
GO
IF NOT EXISTS (
		SELECT TOP 1 LOOKUPID
		FROM Lookups
		WHERE NAME = 'Units'
		)
	INSERT INTO Lookups
	(
	Name,
	Description,
	ICDCPTExists,
	ImagesExists
	)
	VALUES (
		'Units'
		,'Units'
		,0
		,NULL
		)
DECLARE @Lookupid INT;

SELECT @Lookupid = lookupid
FROM lookups
WHERE NAME = 'Units'

IF NOT EXISTS (
		SELECT *
		FROM LookupValues
		WHERE lookupid = @lookupid
		)
BEGIN
	INSERT [dbo].[LookupValues] (
		[LookupID]
		,[LookupValue]
		,[Type]
		,[Code1]
		,[Code2]
		,[ISDefault]
		)
	VALUES (
		@Lookupid
		,N'ml'
		,0
		,NULL
		,NULL
		,NULL
		)

	INSERT [dbo].[LookupValues] (
		[LookupID]
		,[LookupValue]
		,[Type]
		,[Code1]
		,[Code2]
		,[ISDefault]
		)
	VALUES (
		@Lookupid
		,N'mg'
		,0
		,NULL
		,NULL
		,NULL
		)
END
GO

DECLARE @Lookupid INT;

SELECT @Lookupid = lookupid
FROM lookups
WHERE NAME = 'ADMIN-ROUTE'

IF NOT EXISTS (
		SELECT *
		FROM LookupValues
		WHERE lookupid = @lookupid
		)
	INSERT [dbo].[LookupValues] (
		[LookupID]
		,[LookupValue]
		,[Type]
		,[Code1]
		,[Code2]
		,[ISDefault]
		)
	VALUES (
		@Lookupid
		,N'Intramuscular'
		,0
		,NULL
		,NULL
		,NULL
		)
GO
UPDATE dbo.Immunizations SET Active_YN=1