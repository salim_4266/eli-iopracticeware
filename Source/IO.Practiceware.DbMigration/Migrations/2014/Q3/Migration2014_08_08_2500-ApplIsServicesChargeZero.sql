﻿IF OBJECT_ID ( 'dbo.ApplIsServicesChargeZero', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.ApplIsServicesChargeZero;
GO

CREATE PROCEDURE dbo.ApplIsServicesChargeZero
	@ReceivableId INT
AS
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#InvoiceReceivableIds') IS NOT NULL
BEGIN
	DROP TABLE #InvoiceReceivableIds
END

SELECT 
* 
INTO #InvoiceReceivableIds
FROM (
	SELECT
	ir.Id
	FROM model.InvoiceReceivables ir
	WHERE ir.InvoiceId IN (
		SELECT
		ir.InvoiceId
		FROM model.InvoiceReceivables ir
		WHERE ir.Id = @ReceivableId
	)
)v

IF (@@ROWCOUNT > 0)
BEGIN

CREATE UNIQUE CLUSTERED INDEX IX_#InvoiceReceivableIds_Ids ON #InvoiceReceivableIds(Id)

SELECT 
ptj.TransactionStatus
,st.TransportAction
,bs.UnitCharge
FROM dbo.PracticeTransactionJournal ptj 
INNER JOIN #InvoiceReceivableIds irs ON irs.Id = ptj.TransactionTypeId 
INNER JOIN dbo.ServiceTransactions st ON st.TransactionId = ptj.TransactionId
INNER JOIN model.BillingServices bs ON bs.Id = st.ServiceId
WHERE ptj.TransactionStatus = 'P' 
	AND ptj.TransactionAction IN ('P','B') 
	AND ptj.TransactionType = 'R' 
	AND bs.UnitCharge > 0 

END
GO