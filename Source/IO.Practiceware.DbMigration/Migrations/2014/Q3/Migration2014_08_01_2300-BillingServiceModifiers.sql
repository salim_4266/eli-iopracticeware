﻿IF OBJECT_ID('model.BillingServiceModifiers') IS NOT NULL
	EXEC dbo.DropObject 'model.BillingServiceModifiers'
GO

-- 'BillingServiceModifiers' does not exist, creating...
CREATE TABLE [model].[BillingServiceModifiers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrdinalId] int  NULL,
    [BillingServiceId] int  NOT NULL,
    [ServiceModifierId] int  NOT NULL
);

-- Creating primary key on [Id] in table 'BillingServiceModifiers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.BillingServiceModifiers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[BillingServiceModifiers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.BillingServiceModifiers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[BillingServiceModifiers]
ADD CONSTRAINT [PK_BillingServiceModifiers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

DECLARE @BillingServiceModifierMax int
SET @BillingServiceModifierMax = 110000000
DECLARE @ServiceModifierMax int
SET @ServiceModifierMax = 110000000

INSERT INTO model.BillingServiceModifiers ([BillingServiceId], [OrdinalId], [ServiceModifierId])
-- BillingServiceModifier
---First modifier
SELECT bs.Id AS BillingServiceId
	,1 AS OrdinalId
	,sm.Id AS ServiceModifierId
FROM dbo.PatientReceivableServices prs
JOIN model.BillingServices bs ON bs.LegacyPatientReceivableServicesItemId = prs.ItemId
LEFT JOIN model.ServiceModifiers sm ON sm.Code = SUBSTRING(REPLACE(REPLACE(Modifier,' ',''),',',''), 1, 2)
WHERE SUBSTRING(REPLACE(REPLACE(Modifier,' ',''),',',''), 1, 2) <> ''
	AND prs.[Status] = 'A'
GROUP BY prs.ItemId
	,sm.Id
	,bs.Id
	
UNION ALL
	
---2nd modifier
SELECT bs.Id AS BillingServiceId
	,2 AS OrdinalId
	,sm.Id AS ServiceModifierId
FROM dbo.PatientReceivableServices prs
JOIN model.BillingServices bs ON bs.LegacyPatientReceivableServicesItemId = prs.ItemId
LEFT JOIN model.ServiceModifiers sm ON sm.Code = SUBSTRING(REPLACE(REPLACE(Modifier,' ',''),',',''), 3, 2)
WHERE SUBSTRING(REPLACE(REPLACE(Modifier,' ',''),',',''), 3, 2) <> ''
	AND prs.[Status] = 'A'
GROUP BY prs.ItemId
	,sm.Id
	,bs.Id
	
UNION ALL
	
---3rd modifier
SELECT bs.Id AS BillingServiceId
	,3 AS OrdinalId
	,sm.Id AS ServiceModifierId
FROM dbo.PatientReceivableServices prs
JOIN model.BillingServices bs ON bs.LegacyPatientReceivableServicesItemId = prs.ItemId
LEFT JOIN model.ServiceModifiers sm ON sm.Code = SUBSTRING(REPLACE(REPLACE(Modifier,' ',''),',',''), 5, 2)
WHERE SUBSTRING(REPLACE(REPLACE(Modifier,' ',''),',',''), 5, 2) <> ''
	AND prs.[Status] = 'A'
GROUP BY prs.ItemId
	,sm.Id
	,bs.Id
	
UNION ALL
	
---4th modifier
SELECT bs.Id AS BillingServiceId
	,4 AS OrdinalId
	,sm.Id AS ServiceModifierId
FROM dbo.PatientReceivableServices prs
JOIN model.BillingServices bs ON bs.LegacyPatientReceivableServicesItemId = prs.ItemId
LEFT JOIN model.ServiceModifiers sm ON sm.Code = SUBSTRING(REPLACE(REPLACE(Modifier,' ',''),',',''), 7, 2)
WHERE SUBSTRING(REPLACE(REPLACE(Modifier,' ',''),',',''), 7, 2) <> ''
	AND prs.[Status] = 'A'
GROUP BY prs.ItemId
	,sm.Id
	,bs.Id
GO

-- Creating foreign key on [BillingServiceId] in table 'BillingServiceModifiers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BillingServiceBillingServiceModifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingServiceModifiers] DROP CONSTRAINT FK_BillingServiceBillingServiceModifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[BillingServices]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingServiceModifiers]
ADD CONSTRAINT [FK_BillingServiceBillingServiceModifier]
    FOREIGN KEY ([BillingServiceId])
    REFERENCES [model].[BillingServices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BillingServiceBillingServiceModifier'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_BillingServiceBillingServiceModifier')
	DROP INDEX IX_FK_BillingServiceBillingServiceModifier ON [model].[BillingServiceModifiers]
GO
CREATE INDEX [IX_FK_BillingServiceBillingServiceModifier]
ON [model].[BillingServiceModifiers]
    ([BillingServiceId]);
GO

-- Creating foreign key on [ServiceModifierId] in table 'BillingServiceModifiers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ServiceModifierBillingServiceModifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingServiceModifiers] DROP CONSTRAINT FK_ServiceModifierBillingServiceModifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[ServiceModifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingServiceModifiers]
ADD CONSTRAINT [FK_ServiceModifierBillingServiceModifier]
    FOREIGN KEY ([ServiceModifierId])
    REFERENCES [model].[ServiceModifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ServiceModifierBillingServiceModifier'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ServiceModifierBillingServiceModifier')
	DROP INDEX IX_FK_ServiceModifierBillingServiceModifier ON [model].[BillingServiceModifiers]
GO
CREATE INDEX [IX_FK_ServiceModifierBillingServiceModifier]
ON [model].[BillingServiceModifiers]
    ([ServiceModifierId]);
GO

-- Creating foreign key on [BillingServiceId] in table 'BillingServiceModifiers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BillingServiceBillingServiceModifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingServiceModifiers] DROP CONSTRAINT FK_BillingServiceBillingServiceModifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[BillingServices]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingServiceModifiers]
ADD CONSTRAINT [FK_BillingServiceBillingServiceModifier]
    FOREIGN KEY ([BillingServiceId])
    REFERENCES [model].[BillingServices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BillingServiceBillingServiceModifier'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_BillingServiceBillingServiceModifier')
	DROP INDEX IX_FK_BillingServiceBillingServiceModifier ON [model].[BillingServiceModifiers]
GO
CREATE INDEX [IX_FK_BillingServiceBillingServiceModifier]
ON [model].[BillingServiceModifiers]
    ([BillingServiceId]);
GO