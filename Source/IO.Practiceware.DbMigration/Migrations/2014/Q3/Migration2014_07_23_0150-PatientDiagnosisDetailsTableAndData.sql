﻿EXEC dbo.DropObject 'model.PatientDiagnosisDetails'
GO

CREATE TABLE [model].[PatientDiagnosisDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientDiagnosisId] [int] NOT NULL,
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[IsDeactivated] [bit] NOT NULL,
	[EnteredDateTime] [datetime] NOT NULL,
	[UserId] [int] NULL,
	[Comment] [nvarchar](max) NULL,
	[RelevancyQualifierId] [int] NULL,
	[AccuracyQualifierId] [int] NULL,
	[ClinicalConditionStatusId] [int] NOT NULL,
	[BodyLocationId] [int] NULL,
	[IsOnProblemList] [bit] NOT NULL,
	[EncounterId] [int] NULL,
	[IsEncounterDiagnosis] [bit] NOT NULL,
	[EncounterTreatmentGoalAndInstructionId] [int] NULL,
	[IsBillable] [bit] NOT NULL,
	[LegacyClinicalId] [int] NULL
		CONSTRAINT [PK_PatientDiagnosisDetails] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)
)
GO

IF OBJECT_ID('tempdb..#NoAudit','U') IS NULL
	SELECT 1 AS Value INTO #NoAudit
GO
IF (OBJECT_ID('tempdb..#ClinicalConditions','U')) IS NOT NULL
	DROP TABLE #ClinicalConditions
GO
IF (OBJECT_ID('tempdb..#PatientDiagnoses','U')) IS NOT NULL
	DROP TABLE #PatientDiagnoses
GO
IF (OBJECT_ID('tempdb..#PatientDiagnosisIds','U')) IS NOT NULL
	DROP TABLE #PatientDiagnosisIds
GO
IF (OBJECT_ID('tempdb..#PatientDiagnosisDetailComments','U')) IS NOT NULL
	DROP TABLE #PatientDiagnosisDetailComments
GO

SET NOCOUNT ON;

SELECT 
MAX(d.PrimaryDrillId) AS ClinicalConditionId
,d.DiagnosisNextLevel AS LegacyDiagnosisNextLevel
,d.MedicalSystem AS LegacyMedicalSystem
,CASE 
	WHEN d.DiagnosisRank = 113 THEN CONVERT(BIT,1)
		ELSE CONVERT(BIT,0)
END AS IsDeactivated
INTO #ClinicalConditions
FROM dm.PrimaryDiagnosisTable d
WHERE dbo.IsNullOrEmpty(d.ReviewSystemLingo) = 0
	AND dbo.IsNullOrEmpty(MedicalSystem) = 0
GROUP BY d.DiagnosisNextLevel
,d.MedicalSystem
,CASE 
	WHEN d.DiagnosisRank = 113 THEN CONVERT(BIT,1)
		ELSE CONVERT(BIT,0)
END

UNION ALL

SELECT 
model.GetPairId(1,MAX(d.PrimaryDrillId),110000000) AS ClinicalConditionId
,d.DiagnosisNextLevel AS LegacyDiagnosisNextLevel
,NULL AS LegacyMedicalSystem
,CASE 
	WHEN d.DiagnosisRank = 113 THEN CONVERT(BIT,1)
		ELSE CONVERT(BIT,0)
END AS IsDeactivated
FROM dm.PrimaryDiagnosisTable d
WHERE dbo.IsNullOrEmpty(d.ReviewSystemLingo) = 0
GROUP BY d.DiagnosisNextLevel
,CASE 
	WHEN d.DiagnosisRank = 113
		THEN CONVERT(BIT,1)
	ELSE CONVERT(BIT,0)
END

SELECT
pc.PatientId
,c.LegacyDiagnosisNextLevel
,CASE
	WHEN pc.EyeContext = 'OD' 
		THEN 1
	WHEN pc.EyeContext = 'OS' 
		THEN 2
	ELSE 3
END AS LateralityId
INTO #PatientDiagnoses
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId 
INNER JOIN #ClinicalConditions c ON c.LegacyDiagnosisNextLevel = pc.FindingDetail
	AND c.LegacyMedicalSystem = pc.ImageDescriptor
	AND dbo.IsNullOrEmpty(c.LegacyMedicalSystem) = 0
WHERE pc.ClinicalType = 'Q'
	AND pc.[Status] = 'A'
	AND dbo.IsNullOrEmpty(pc.ImageDescriptor) = 0
GROUP BY pc.PatientId
,c.LegacyDiagnosisNextLevel
,CASE
	WHEN pc.EyeContext = 'OD' 
		THEN 1
	WHEN pc.EyeContext = 'OS' 
		THEN 2
	ELSE 3
END
,pc.LegacyClinicalDataSourceTypeId

SELECT
MIN(pc.ClinicalId) AS PatientDiagnosisId
,pc.PatientId
,c.ClinicalConditionId
,CASE
	WHEN pc.EyeContext = 'OD' 
		THEN 1
	WHEN pc.EyeContext = 'OS' 
		THEN 2
	ELSE 3
END AS LateralityId
INTO #PatientDiagnosisIds
FROM dbo.PatientClinical pc
INNER JOIN #ClinicalConditions c ON c.LegacyDiagnosisNextLevel = pc.FindingDetail
	AND c.LegacyMedicalSystem = pc.ImageDescriptor
	AND c.LegacyMedicalSystem <> ''
	AND c.LegacyMedicalSystem IS NOT NULL
	AND c.IsDeactivated = 0
WHERE pc.ClinicalType = 'Q'
	AND pc.[Status] = 'A'
	AND pc.ImageDescriptor <> ''
	AND pc.ImageDescriptor IS NOT NULL
GROUP BY pc.PatientId
,c.ClinicalConditionId
,CASE
	WHEN pc.EyeContext = 'OD' 
		THEN 1
	WHEN pc.EyeContext = 'OS' 
		THEN 2
	ELSE 3
END
	
UNION ALL

SELECT 
MIN(pc.ClinicalId) AS PatientDiagnosisId
,pc.PatientId
,c.ClinicalConditionId
,CASE
	WHEN pc.EyeContext = 'OD' 
		THEN 1
	WHEN pc.EyeContext = 'OS' 
		THEN 2
	ELSE 3
END AS LateralityId
FROM dbo.PatientClinical pc
INNER JOIN #ClinicalConditions c ON c.LegacyDiagnosisNextLevel = pc.FindingDetail
	AND (c.LegacyMedicalSystem = '' OR c.LegacyMedicalSystem IS NULL)
	AND c.IsDeactivated = 0
LEFT JOIN #PatientDiagnoses pd ON pd.PatientId = pc.PatientId
	AND pd.LateralityId = CASE
		WHEN pc.EyeContext = 'OD' 
			THEN 1
		WHEN pc.EyeContext = 'OS' 
			THEN 2
		ELSE 3
	END
	AND pd.LegacyDiagnosisNextLevel = pc.FindingDetail
WHERE pc.ClinicalType = 'U'
	AND pc.[Status] = 'A'
	AND pd.PatientId IS NULL
	AND (pc.ImageDescriptor = '' OR pc.ImageDescriptor IS NULL)	
GROUP BY pc.PatientId
,c.ClinicalConditionId
,CASE
	WHEN pc.EyeContext = 'OD' 
		THEN 1
	WHEN pc.EyeContext = 'OS' 
		THEN 2
	ELSE 3
END

SELECT pc.ClinicalId AS PatientDiagnosisDetailsId
	,SUBSTRING(Symptom, CHARINDEX('[', Symptom)  +1, CHARINDEX(']', Symptom)-CHARINDEX('[', Symptom)-1) AS Comment
INTO #PatientDiagnosisDetailComments
FROM dbo.PatientClinical pc
LEFT JOIN (
	SELECT 
	pct.Id AS Id
	,Code AS Name
	,q.Id AS ClinicalQualifierCategoryId
	,pct.OtherLtrTrans AS SimpleDescription
	,pct.LetterTranslation AS TechnicalDescription
	,CONVERT(BIT,0) AS IsDeactivated
	,1 AS OrdinalId
	FROM PracticeCodeTable pct
	INNER JOIN (
	SELECT
	MAX(pct.Id) AS Id
	,CASE 
		WHEN SUBSTRING(ReferenceType,LEN('QUANTIFIERS')+1,LEN(ReferenceType)) = 'DESCRIPTORS'
			THEN CONVERT(BIT,0) 
		WHEN ReferenceType = 'QUANTIFIERS'
			THEN CONVERT(BIT,0) 
		ELSE CONVERT(BIT,1) 
	END AS IsSingleUse
	,ReferenceType AS Name
	,1 AS OrdinalId
	FROM PracticeCodeTable pct
	WHERE ReferenceType LIKE '%QUANTIFIERS%' AND (AlternateCode <> '' OR LetterTranslation <> '' OR OtherLtrTrans <> '')
	GROUP BY ReferenceType

	UNION ALL

	SELECT
	MAX(pct.Id) AS Id
	,CONVERT(BIT,1) AS IsSingleUse
	,SUBSTRING(ReferenceType,19,LEN(ReferenceType)) AS Name
	,1 AS OrdinalId
	FROM PracticeCodeTable pct
	WHERE ReferenceType LIKE 'INSERTEDQUANTIFIERS%' 
	GROUP BY ReferenceType
) q ON q.Name = pct.ReferenceType
WHERE pct.ReferenceType LIKE '%QUANTIFIERS%' AND (AlternateCode <> '' OR LetterTranslation <> '' OR OtherLtrTrans <> '')
) q ON q.Name = SUBSTRING(Symptom, CHARINDEX('[', Symptom)  +1, CHARINDEX(']', Symptom)-CHARINDEX('[', Symptom)-1)
WHERE pc.ClinicalType IN ('Q', 'U')
    AND pc.Symptom LIKE '%]%'
	AND q.Id IS NULL

INSERT INTO model.PatientDiagnosisDetails (
      [PatientDiagnosisId]
      ,[StartDateTime]
      ,[EndDateTime]
      ,[IsDeactivated]
      ,[EnteredDateTime]
      ,[UserId]
      ,[Comment]
      ,[RelevancyQualifierId]
      ,[AccuracyQualifierId]
      ,[ClinicalConditionStatusId]
      ,[BodyLocationId]
      ,[IsOnProblemList]
      ,[EncounterId]
      ,[IsEncounterDiagnosis]
      ,[EncounterTreatmentGoalAndInstructionId]
      ,[IsBillable]
	  ,[LegacyClinicalId]
)
SELECT 
c3.PatientDiagnosisId
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(MI, ap.AppTime, CONVERT(DATETIME, ap.AppDate, 112)) 
	ELSE CONVERT(DATETIME, ap.AppDate, 112) 
END AS StartDateTime
,CONVERT(DATETIME,NULL) AS EndDateTime
,CONVERT(BIT,0) AS IsDeactivated
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(MI, ap.AppTime, CONVERT(DATETIME, ap.AppDate, 112)) 
	ELSE CONVERT(DATETIME, ap.AppDate, 112) 
END AS EnteredDateTime
,NULL AS UserId
,#PatientDiagnosisDetailComments.Comment
,NULL AS RelevancyQualifierId
,NULL AS AccuracyQualifierId
,1 AS ClinicalConditionStatusId
,NULL AS BodyLocationId
,COALESCE((SELECT TOP 1 CONVERT(BIT,1) FROM dbo.PatientClinical p WHERE p.PatientId = pc.PatientId AND p.AppointmentId = ap.AppointmentId AND p.FindingDetail = pc.FindingDetail AND ClinicalType = 'U' AND p.EyeContext = pc.EyeContext),CONVERT(BIT,0)) AS IsOnProblemList
,ap.AppointmentId AS EncounterId
,COALESCE((SELECT TOP 1 CONVERT(BIT,1) FROM dbo.PatientClinical p WHERE p.PatientId = pc.PatientId AND p.AppointmentId = ap.AppointmentId AND p.FindingDetail = pc.FindingDetail AND ClinicalType = 'U' AND p.EyeContext = pc.EyeContext),CONVERT(BIT,0)) AS IsEncounterDiagnosis
,NULL AS EncounterTreatmentGoalAndInstructionId
,CONVERT(BIT,0) AS IsBillable
,pc.ClinicalId AS LegacyClinicalId
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId 
INNER JOIN #ClinicalConditions c1 ON c1.LegacyDiagnosisNextLevel = pc.FindingDetail
	AND c1.LegacyMedicalSystem = pc.ImageDescriptor
	AND c1.LegacyMedicalSystem <> ''
	AND c1.LegacyMedicalSystem IS NOT NULL
	AND c1.IsDeactivated = 0
INNER JOIN #PatientDiagnosisIds c3 ON c3.ClinicalConditionId = c1.ClinicalConditionId
	AND c3.PatientId = pc.PatientId
	AND CASE
		WHEN pc.EyeContext = 'OD' 
			THEN 1
		WHEN pc.EyeContext = 'OS' 
			THEN 2
		ELSE 3
	END = c3.LateralityId
LEFT JOIN #PatientDiagnosisDetailComments ON #PatientDiagnosisDetailComments.PatientDiagnosisDetailsId = pc.ClinicalId
WHERE pc.ClinicalType = 'Q'
	AND pc.[Status] = 'A'
	AND pc.ImageDescriptor <> ''
	AND pc.ImageDescriptor IS NOT NULL
GROUP BY pc.ClinicalId
,c3.PatientDiagnosisId
,ap.AppDate
,ap.AppTime
,#PatientDiagnosisDetailComments.Comment
,ap.AppointmentId
,pc.PatientId
,pc.FindingDetail
,pc.EyeContext

UNION ALL

SELECT 
c3.PatientDiagnosisId
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(MI, ap.AppTime, CONVERT(DATETIME, ap.AppDate, 112)) 
	ELSE CONVERT(DATETIME, ap.AppDate, 112) 
END AS StartDateTime
,CONVERT(DATETIME,NULL) AS EndDateTime
,CONVERT(BIT,0) AS IsDeactivated
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(MI, ap.AppTime, CONVERT(DATETIME, ap.AppDate, 112)) 
	ELSE CONVERT(DATETIME, ap.AppDate, 112) 
END AS EnteredDateTime
,NULL AS UserId
,#PatientDiagnosisDetailComments.Comment
,NULL AS RelevancyQualifierId
,NULL AS AccuracyQualifierId
,1 AS ClinicalConditionStatusId
,NULL AS BodyLocationId
,CONVERT(BIT,1) AS IsOnProblemList
,ap.AppointmentId
,CONVERT(BIT,1) AS IsEncounterDiagnosis
,NULL AS EncounterTreatmentGoalAndInstructionId
,CONVERT(BIT,0) AS IsBillable
,pc.ClinicalId AS LegacyClinicalId
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId 
INNER JOIN #ClinicalConditions c1 ON c1.LegacyDiagnosisNextLevel = pc.FindingDetail
	AND (c1.LegacyMedicalSystem = '' OR c1.LegacyMedicalSystem IS NULL)
	AND c1.IsDeactivated = 0
INNER JOIN #PatientDiagnosisIds c3 ON c3.ClinicalConditionId = c1.ClinicalConditionId
	AND c3.PatientId = pc.PatientId
	AND c3.LateralityId = CASE
		WHEN pc.EyeContext = 'OD' 
			THEN 1
		WHEN pc.EyeContext = 'OS' 
			THEN 2
		ELSE 3
	END
LEFT JOIN #PatientDiagnoses pd ON pd.PatientId = pc.PatientId
	AND pd.LateralityId = CASE
		WHEN pc.EyeContext = 'OD' 
			THEN 1
		WHEN pc.EyeContext = 'OS' 
			THEN 2
		ELSE 3
	END
	AND pd.LegacyDiagnosisNextLevel = pc.FindingDetail
LEFT JOIN #PatientDiagnosisDetailComments ON #PatientDiagnosisDetailComments.PatientDiagnosisDetailsId = pc.ClinicalId
WHERE pc.ClinicalType = 'U'
	AND pc.[Status] = 'A'
	AND (pc.ImageDescriptor = '' OR pc.ImageDescriptor IS NULL)
	AND pd.PatientId IS NULL
GROUP BY pc.ClinicalId
,c3.PatientDiagnosisId
,ap.AppDate
,ap.AppTime
,#PatientDiagnosisDetailComments.Comment
,ap.AppointmentId
,pc.PatientId
,pc.FindingDetail
,pc.EyeContext

EXCEPT

SELECT 
[PatientDiagnosisId]
,[StartDateTime]
,[EndDateTime]
,[IsDeactivated]
,[EnteredDateTime]
,[UserId]
,[Comment]
,[RelevancyQualifierId]
,[AccuracyQualifierId]
,[ClinicalConditionStatusId]
,[BodyLocationId]
,[IsOnProblemList]
,[EncounterId]
,[IsEncounterDiagnosis]
,[EncounterTreatmentGoalAndInstructionId]
,[IsBillable]
,[LegacyClinicalId]
FROM model.PatientDiagnosisDetails
GO

-- Create default constraint
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT DF_model_PatientDiagnosisDetails_IsBillable DEFAULT (CONVERT([bit],(0),0)) FOR [IsBillable]
GO

-- Creating foreign key on [UserId] in table 'PatientDiagnosisDetails'
IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailUser]
    FOREIGN KEY ([UserId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [RelevancyQualifierId] in table 'PatientDiagnosisDetails'
IF OBJECTPROPERTY(OBJECT_ID('[model].[RelevancyQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailRelevancyQualifier]
    FOREIGN KEY ([RelevancyQualifierId])
    REFERENCES [model].[RelevancyQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [BodyLocationId] in table 'PatientDiagnosisDetails'
IF OBJECTPROPERTY(OBJECT_ID('[model].[BodyLocations]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailBodyLocation]
    FOREIGN KEY ([BodyLocationId])
    REFERENCES [model].[BodyLocations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [EncounterId] in table 'PatientDiagnosisDetails'
IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [EncounterTreatmentGoalAndInstructionId] in table 'PatientDiagnosisDetails'
IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterTreatmentGoalAndInstructions]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailEncounterTreatmentGoal]
    FOREIGN KEY ([EncounterTreatmentGoalAndInstructionId])
    REFERENCES [model].[EncounterTreatmentGoalAndInstructions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [AccuracyQualifierId] in table 'PatientDiagnosisDetails'
IF OBJECTPROPERTY(OBJECT_ID('[model].[AccuracyQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailAccuracyQualifier]
    FOREIGN KEY ([AccuracyQualifierId])
    REFERENCES [model].[AccuracyQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailUser'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientDiagnosisDetailUser')
	DROP INDEX IX_FK_PatientDiagnosisDetailUser ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailUser]
ON [model].[PatientDiagnosisDetails]
    ([UserId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailRelevancyQualifier'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientDiagnosisDetailRelevancyQualifier')
	DROP INDEX IX_FK_PatientDiagnosisDetailRelevancyQualifier ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailRelevancyQualifier]
ON [model].[PatientDiagnosisDetails]
    ([RelevancyQualifierId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailBodyLocation'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientDiagnosisDetailBodyLocation')
	DROP INDEX IX_FK_PatientDiagnosisDetailBodyLocation ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailBodyLocation]
ON [model].[PatientDiagnosisDetails]
    ([BodyLocationId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailEncounter'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientDiagnosisDetailEncounter')
	DROP INDEX IX_FK_PatientDiagnosisDetailEncounter ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailEncounter]
ON [model].[PatientDiagnosisDetails]
    ([EncounterId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailEncounterTreatmentGoal'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientDiagnosisDetailEncounterTreatmentGoal')
	DROP INDEX IX_FK_PatientDiagnosisDetailEncounterTreatmentGoal ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailEncounterTreatmentGoal]
ON [model].[PatientDiagnosisDetails]
    ([EncounterTreatmentGoalAndInstructionId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailAccuracyQualifier'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientDiagnosisDetailAccuracyQualifier')
	DROP INDEX IX_FK_PatientDiagnosisDetailAccuracyQualifier ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailAccuracyQualifier]
ON [model].[PatientDiagnosisDetails]
    ([AccuracyQualifierId]);
GO