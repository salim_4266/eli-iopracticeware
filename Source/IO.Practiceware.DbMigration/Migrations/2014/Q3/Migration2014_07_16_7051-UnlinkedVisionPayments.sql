﻿--IF OBJECT_ID('tempdb..#NoAudit','U') IS NOT NULL
--	DROP TABLE #NoAudit
--GO
--SELECT 1 AS Value INTO #NoAudit
--GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'PatientReceivablePaymentsBackup' AND Type = 'U')
BEGIN
	IF (
		SELECT (SELECT (SELECT SUM(PaymentAmount) FROM PatientReceivablePaymentsBackup WHERE PaymentType = 'P') 
		- SUM(PaymentAmount) FROM PatientReceivablePayments WHERE PaymentType = 'P') 
		/ (SELECT SUM(PaymentAmount) FROM PatientReceivablePaymentsBackup WHERE PaymentType = 'P') 
		* 100) >= 0.7 
	BEGIN
		IF OBJECT_ID('tempdb..#VisionAdjustmentsAndFinancialInformation','U') IS NOT NULL
			DROP TABLE #VisionAdjustmentsAndFinancialInformation

		IF OBJECT_ID('tempdb..#VisionAdjustmentsAndFinancialInformationWeak','U') IS NOT NULL
			DROP TABLE #VisionAdjustmentsAndFinancialInformationWeak

		DECLARE @AdjustmentTypes TABLE (AdjustmentTypeIds INT, PaymentType NVARCHAR(MAX), IsDebit INT, ShortName NVARCHAR(MAX))

		INSERT INTO @AdjustmentTypes (AdjustmentTypeIds, PaymentType, IsDebit, ShortName)
		SELECT 
		v.Id AS Id
		,v.PaymentType
		,v.IsDebit AS IsDebit
		,v.ShortName AS ShortName
		FROM (
			SELECT 
				CASE
					WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'P'
						THEN 1
					WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'X'
						THEN 2
					WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '8'
						THEN 3
					WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'R'
						THEN 4
					WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'U'
						THEN 5
					ELSE pct.Id + 1000
				END AS Id
				,SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS PaymentType
				,CASE pct.AlternateCode
					WHEN 'D'
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
				END AS IsDebit
				,LetterTranslation AS ShortName
			FROM dbo.PracticeCodeTable pct
			WHERE pct.ReferenceType = 'PAYMENTTYPE'
				AND (pct.AlternateCode <> '' AND pct.AlternateCode IS NOT NULL)
			GROUP BY 
			Id
			,Code
			,pct.AlternateCode
			,LetterTranslation
		) AS v

		DECLARE @FinancialInformationTypes TABLE (FinancialInformationTypeIds INT, FinancialInformationType NVARCHAR(MAX), ShortName NVARCHAR(MAX))
		INSERT INTO @FinancialInformationTypes (FinancialInformationTypeIds, FinancialInformationType, ShortName)
		SELECT 
		v.Id AS FinancialInformationTypeIds
		,v.FinancialInformationType
		,v.ShortName AS ShortName
		FROM (
			SELECT 
			CASE
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '!'
					THEN 1
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '|'
					THEN 2
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '?'
					THEN 3
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '%'
					THEN 4
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'D'
					THEN 5
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '&'
					THEN 6
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = ']'
					THEN 7
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '0'
					THEN 8
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'V'
					THEN 9
				ELSE pct.Id + 1000
			END AS Id
			,SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS FinancialInformationType
			,AlternateCode AS PaymentFinancialType
			,LetterTranslation AS ShortName
			FROM dbo.PracticeCodeTable pct
			WHERE pct.ReferenceType = 'PAYMENTTYPE'		
				AND (pct.AlternateCode = '' OR pct.AlternateCode IS NULL)
			GROUP BY 
			pct.Id
			,pct.Code
			,LetterTranslation
			,AlternateCode
		) AS v

		SELECT 
		p.PaymentId
		,p.PaymentType
		,p.PaymentAmountDecimal
		,p.PaymentServiceItem
		,p.PaymentFinancialType
		,p.PaymentCheck
		,p.PaymentDate
		,p.PaymentEOBDate
		,p.PaymentRefType
		,p.PaymentReason
		,p.ClaimAdjustmentGroupCodeId
		,p.PayerId
		,b1.PatientId
		,b1.Invoice
		,b1.InvoiceDate
		,b1.ReceivableId AS IncorrectReceivableId
		,b2.ReceivableId AS CorrectReceivableId
		,b2.InsuredId
		,b2.InsurerId
		,i.Id AS InvoiceId
		,ir.Id AS InvoiceReceivableId
		INTO #VisionAdjustmentsAndFinancialInformationWeak
		FROM dbo.PatientReceivablePaymentsBackup p
		JOIN dbo.PatientReceivablesBackup b1 ON b1.ReceivableId = p.ReceivableId
			AND b1.ComputedFinancialId IS NULL
		JOIN dbo.PatientReceivableServices prs ON (prs.ItemId = p.PaymentServiceItem OR p.PaymentServiceItem = '' OR p.PaymentServiceItem IS NULL)
		LEFT JOIN dbo.PatientDemographicsTable pdt ON b1.PatientId = pdt.PatientId
		LEFT JOIN dbo.PatientFinancialBackup f1 ON f1.PatientId = b1.PatientId
			AND f1.FinancialInsurerId = b1.InsurerId
		LEFT JOIN PatientReceivablesBackup b2 ON b2.Invoice = b1.Invoice
			AND b2.InsurerId = b1.InsurerId
			AND b2.ComputedFinancialId IS NOT NULL
			AND b2.InsurerId = p.PayerId
		LEFT JOIN dbo.PatientFinancialBackup f2 ON f2.PatientId = b2.InsuredId
			AND f2.FinancialInsurerId = b2.InsurerId
			AND f2.FinancialStartDate <= b1.InvoiceDate
			AND (f2.FinancialEndDate >= b1.InvoiceDate OR f2.FinancialEndDate = '' OR f2.FinancialEndDate IS NULL)
			AND f2.FinancialInsType = 'V'
		LEFT JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = b1.AppointmentId
		LEFT JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
			AND PatientInsuranceId IS NOT NULL
		LEFT JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
			AND (pi.EndDateTime IS NULL OR CONVERT(NVARCHAR(8),pi.EndDateTime,112) >= b1.InvoiceDate)
		LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			AND CONVERT(NVARCHAR(8),ip.StartDateTime,112) <= b1.InvoiceDate
		LEFT JOIN (
			SELECT 
			a.*
			,at.PaymentType
			,f.CheckCode
			,CONVERT(NVARCHAR(8),f.PaymentDateTime,112) AS CheckDateTime
			,z.PaymentMethod
			FROM model.Adjustments a 
			JOIN @AdjustmentTypes at ON at.AdjustmentTypeIds = a.AdjustmentTypeId
			JOIN model.FinancialBatches f ON f.Id = a.FinancialBatchId
			JOIN (SELECT TOP 1 Id
				,SUBSTRING(pm.Code, LEN(pm.Code), 1) AS PaymentMethod
				FROM dbo.PracticeCodeTable pm
				WHERE pm.ReferenceType = 'PAYABLETYPE') z ON z.Id = a.PaymentMethodId
			) v ON v.InvoiceReceivableId = ir.Id
			AND v.BillingServiceId = prs.ItemId
			AND v.FinancialSourceTypeId = 1
			AND v.Amount = p.PaymentAmountDecimal
			AND v.PostedDateTime = CONVERT(DATETIME,p.PaymentDate)
			AND v.PaymentType = p.PaymentType
			AND (v.BillingServiceId = p.PaymentServiceItem OR p.PaymentServiceItem IS NULL)
			AND v.InsurerId = p.PayerId
			AND v.PaymentMethod = p.PaymentRefType
		WHERE p.PayerType = 'I'
			AND p.PaymentType <> '='
			AND p.PayerId > 0
			AND f1.FinancialId IS NULL
			AND b2.ReceivableId IS NOT NULL
			AND b1.InsurerId > 0
			AND ir.Id IS NULL
			AND v.Id IS NULL
			AND f2.FinancialId IS NOT NULL
			AND (pdt.PolicyPatientId <> b1.InsuredId OR pdt.SecondPolicyPatientId <> b1.InsuredId)

		DECLARE @InsurancePolicies TABLE (InsurancePolicyId INT, GroupCode NVARCHAR(MAX), StartDateTime DATETIME, PolicyHolderPatientId INT, InsurerId INT)

		INSERT INTO model.InsurancePolicies (
			PolicyCode
			,GroupCode
			,Copay
			,Deductible
			,StartDateTime
			,PolicyHolderPatientId
			,InsurerId
			,MedicareSecondaryReasonCodeId
		)
		OUTPUT inserted.Id
		,inserted.GroupCode
		,inserted.StartDateTime
		,inserted.PolicyHolderPatientId
		,inserted.InsurerId
		INTO @InsurancePolicies (InsurancePolicyId, GroupCode, StartDateTime, PolicyHolderPatientId, InsurerId)
		SELECT DISTINCT
		'DELETEDVisionInsurance' AS PolicyCode
		,'' AS GroupCode
		,0 AS Copay
		,0 AS Deductible
		,CONVERT(DATETIME, v.InvoiceDate) AS StartDateTime
		,v.InsuredId AS PolicyHolderPatientId
		,v.InsurerId
		,NULL AS MedicareSecondaryReasonCodeId
		FROM #VisionAdjustmentsAndFinancialInformationWeak v
		LEFT JOIN model.InsurancePolicies ip ON ip.PolicyHolderPatientId = v.InsuredId
			AND ip.InsurerId = v.InsurerId
			AND CONVERT(NVARCHAR(8),ip.StartDateTime,112) <= v.InvoiceDate
		WHERE ip.Id IS NULL
		GROUP BY v.InsurerId
		,v.InsuredId 
		,v.InvoiceDate 

		DECLARE @PatientInsurances TABLE (Id INT)

		INSERT INTO model.PatientInsurances (
			InsuranceTypeId
			,PolicyHolderRelationshipTypeId
			,OrdinalId
			,InsuredPatientId
			,InsurancePolicyId
			,EndDateTime
			,IsDeleted
		)
		OUTPUT inserted.Id
		INTO @PatientInsurances (Id)
		SELECT DISTINCT
		2 AS InsuranceTypeId
		,7 AS PolicyHolderRelationshipTypeId
		,3 AS OrdinalId
		,v.PatientId AS InsuredPatientId
		,COALESCE(i.InsurancePolicyId,ip.Id) AS InsurancePolicyId
		,DATEADD(dd,1,CONVERT(DATETIME, v.InvoiceDate)) AS EndDateTime
		,CONVERT(BIT,0) AS IsDeleted
		FROM #VisionAdjustmentsAndFinancialInformationWeak v
		LEFT JOIN @InsurancePolicies i ON i.InsurerId = v.InsurerId
			AND i.StartDateTime = CONVERT(DATETIME, v.InvoiceDate)
			AND i.PolicyHolderPatientId = v.InsuredId
			AND i.GroupCode = v.PatientId
		LEFT JOIN model.InsurancePolicies ip ON ip.PolicyHolderPatientId = v.InsuredId
			AND ip.InsurerId = v.InsurerId
			AND CONVERT(NVARCHAR(8),ip.StartDateTime,112) <= v.InvoiceDate
		LEFT JOIN model.PatientInsurances pi ON pi.InsurancePolicyId = ip.Id
			AND pi.InsuredPatientId = ip.PolicyHolderPatientId
			AND pi.InsuranceTypeId = 2
			AND (CONVERT(NVARCHAR(8),pi.EndDateTime,112) >=  v.InvoiceDate OR pi.EndDateTime IS NULL)
		WHERE pi.Id IS NOT NULL 

		INSERT INTO model.InvoiceReceivables (
			InvoiceId
			,PatientInsuranceId
			,PayerClaimControlNumber
			,PatientInsuranceAuthorizationId
			,PatientInsuranceReferralId
			,OpenForReview
			,LegacyInsurerDesignation
		)
		SELECT DISTINCT
		v.InvoiceId
		,pi.Id
		,NULL AS PayerClaimControlNumber
		,NULL AS PatientInsuranceAuthorizationId
		,NULL AS PatientInsuranceReferralId
		,CONVERT(BIT,0) AS OpenForReview
		,NULL AS LegacyInsurerDesignation
		FROM #VisionAdjustmentsAndFinancialInformationWeak v
		JOIN model.PatientInsurances pi ON pi.InsuredPatientId = v.PatientId
		JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			AND ip.PolicyHolderPatientId = v.InsuredId
			AND ip.InsurerId = v.InsurerId
		JOIN @PatientInsurances p ON p.Id = pi.Id

		SELECT 
		p.PaymentId
		,p.PaymentType
		,p.PaymentAmountDecimal
		,p.PaymentServiceItem
		,p.PaymentFinancialType
		,p.PaymentCheck
		,p.PaymentDate
		,p.PaymentEOBDate
		,p.PaymentRefType
		,p.PaymentReason
		,p.ClaimAdjustmentGroupCodeId
		,p.PayerId
		,b1.Invoice
		,b1.InvoiceDate
		,b1.ReceivableId AS IncorrectReceivableId
		,b2.ReceivableId AS CorrectReceivableId
		,b2.InsurerId
		,b2.InsuredId
		,b2.PatientId
		,i.Id AS InvoiceId
		,ir.Id AS InvoiceReceivableId
		INTO #VisionAdjustmentsAndFinancialInformation
		FROM dbo.PatientReceivablePaymentsBackup p
		JOIN dbo.PatientReceivablesBackup b1 ON b1.ReceivableId = p.ReceivableId
			AND b1.ComputedFinancialId IS NULL
		JOIN dbo.PatientReceivableServices prs ON (prs.ItemId = p.PaymentServiceItem OR p.PaymentServiceItem = '' OR p.PaymentServiceItem IS NULL)
		LEFT JOIN dbo.PatientDemographicsTable pdt ON b1.PatientId = pdt.PatientId
		LEFT JOIN dbo.PatientFinancialBackup f1 ON f1.PatientId = b1.PatientId
			AND f1.FinancialInsurerId = b1.InsurerId
		LEFT JOIN PatientReceivablesBackup b2 ON b2.Invoice = b1.Invoice
			AND b2.InsurerId = b1.InsurerId
			AND b2.InsurerId = p.PayerId
			AND b2.ComputedFinancialId IS NOT NULL
		LEFT JOIN dbo.PatientFinancialBackup f2 ON f2.PatientId = b2.InsuredId
			AND f2.FinancialInsurerId = b2.InsurerId
			AND f2.FinancialStartDate <= b1.InvoiceDate
			AND (f2.FinancialEndDate >= b1.InvoiceDate OR f2.FinancialEndDate = '' OR f2.FinancialEndDate IS NULL)
			AND f2.FinancialInsType = 'V'
		LEFT JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = b1.AppointmentId
		LEFT JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
			AND PatientInsuranceId IS NOT NULL
		LEFT JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
			AND (pi.EndDateTime IS NULL OR CONVERT(NVARCHAR(8),pi.EndDateTime,112) >= b1.InvoiceDate)
		LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			AND CONVERT(NVARCHAR(8),ip.StartDateTime,112) <= b1.InvoiceDate
			AND ip.InsurerId = p.PayerId
		LEFT JOIN (
			SELECT 
			a.*
			,at.PaymentType
			,f.CheckCode
			,CONVERT(NVARCHAR(8),f.PaymentDateTime,112) AS CheckDateTime
			,z.PaymentMethod
			FROM model.Adjustments a 
			JOIN @AdjustmentTypes at ON at.AdjustmentTypeIds = a.AdjustmentTypeId
			JOIN model.FinancialBatches f ON f.Id = a.FinancialBatchId
			JOIN (SELECT TOP 1 Id
				,SUBSTRING(pm.Code, LEN(pm.Code), 1) AS PaymentMethod
				FROM dbo.PracticeCodeTable pm
				WHERE pm.ReferenceType = 'PAYABLETYPE') z ON z.Id = a.PaymentMethodId
			) v ON v.InvoiceReceivableId = ir.Id
			AND v.BillingServiceId = prs.ItemId
			AND v.FinancialSourceTypeId = 1
			AND v.Amount = p.PaymentAmountDecimal
			AND v.PostedDateTime = CONVERT(DATETIME,p.PaymentDate)
			AND v.PaymentType = p.PaymentType
			AND v.BillingServiceId = p.PaymentServiceItem
			AND v.InsurerId = p.PayerId
			AND v.PaymentMethod = p.PaymentRefType
		WHERE p.PayerType = 'I'
			AND p.PaymentType <> '='
			AND p.PayerId > 0
			AND f1.FinancialId IS NULL
			AND b2.ReceivableId IS NOT NULL
			AND b1.InsurerId > 0
			AND ir.Id IS NOT NULL
			AND ir.PatientInsuranceId = pi.Id
			AND ip.InsurerId = p.PayerId
			AND v.Id IS NULL
			AND f2.FinancialId IS NOT NULL
			AND (pdt.PolicyPatientId <> b1.InsuredId OR pdt.SecondPolicyPatientId <> b1.InsuredId)

		DECLARE @FinancialBatches TABLE (Id INT, InsurerId INT, PaymentDateTime DATETIME, ExplanationOfBenefitsDateTime DATETIME, Amount DECIMAL(18,2), CheckCode NVARCHAR(MAX))

		INSERT INTO model.FinancialBatches (
			FinancialSourceTypeId
			,PatientId
			,InsurerId
			,PaymentDateTime
			,ExplanationOfBenefitsDateTime
			,Amount
			,CheckCode
		)
		OUTPUT inserted.Id
		,inserted.InsurerId
		,inserted.PaymentDateTime
		,inserted.ExplanationOfBenefitsDateTime
		,inserted.Amount
		,inserted.CheckCode
			INTO @FinancialBatches(Id
			,InsurerId
			,PaymentDateTime
			,ExplanationOfBenefitsDateTime
			,Amount
			,CheckCode)
		SELECT 
		1 AS FinancialSourceTypeId
		,NULL AS PatientId
		,v.InsurerId
		,CASE 
			WHEN v.PaymentDate <> '' AND v.PaymentDate IS NOT NULL
				THEN CONVERT(DATETIME,v.PaymentDate)
		END AS PaymentDateTime
		,CASE 
			WHEN v.PaymentEOBDate <> '' AND v.PaymentEOBDate IS NOT NULL
				THEN CONVERT(DATETIME,v.PaymentEOBDate)
		END AS ExplanationOfBenefitsDateTime
		,SUM(PaymentAmountDecimal) AS Amount
		,PaymentCheck
		FROM #VisionAdjustmentsAndFinancialInformation v
		WHERE v.PaymentFinancialType <> ''
		GROUP BY v.PaymentCheck
		,v.InsurerId
		,v.PaymentDate
		,v.PaymentEOBDate
	
		DECLARE @Adjustments TABLE (Id INT)

		INSERT INTO model.Adjustments (
			FinancialSourceTypeId
			,Amount
			,PostedDateTime
			,AdjustmentTypeId
			,BillingServiceId
			,InvoiceReceivableId
			,PatientId
			,InsurerId
			,FinancialBatchId
			,PaymentMethodId
			,ClaimAdjustmentGroupCodeId
			,ClaimAdjustmentReasonCodeId
			,Comment
			,IncludeCommentOnStatement
		)
		OUTPUT inserted.Id INTO @Adjustments (Id)
		SELECT DISTINCT
		1 AS FinancialSourceTypeId
		,v.PaymentAmountDecimal AS Amount
		,CONVERT(DATETIME,v.PaymentDate) AS PostedDateTime
		,at.AdjustmentTypeIds AS AdjustmentTypeId
		,v.PaymentServiceItem AS BillingServiceId
		,ir.Id AS InvoiceReceivableId
		,NULL AS PatientId
		,v.PayerId AS InsurerId
		,f.Id AS FinancialBatchId
		,pctPaymentMethod.Id AS PaymentMethodId
		,CASE
			WHEN v.ClaimAdjustmentGroupCodeId IS NULL 
				THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
					-- These int values correspond to the ClaimAdjustmentGroupCode enum in .NET
					WHEN 'X' -- AdjustmentTypeId = 2
						THEN 1 -- ContractualObligation
					WHEN '8' -- AdjustmentTypeId = 3
						THEN 3 -- OtherAdjustment
					WHEN 'R' -- AdjustmentTypeId = 4
						THEN 4 -- CorrectionReversal
					WHEN 'U' -- AdjustmentTypeId = 5
						THEN 4 -- CorrectionReversal
					END
			ELSE
				v.ClaimAdjustmentGroupCodeId
		END AS ClaimAdjustmentGroupCodeId
		,CASE
			WHEN v.PaymentReason IS NULL 
				THEN 
					CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
						WHEN 'X' -- AdjustmentTypeId = 2
							THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45') -- ReasonCode 45
						WHEN '8' -- AdjustmentTypeId = 3
							THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45')-- ReasonCode 45
					END
			WHEN v.PaymentReason = ''
				THEN 
					CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
						WHEN 'X' -- AdjustmentTypeId = 2
							THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45')
						WHEN '8' -- AdjustmentTypeId = 3
							THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45')
					END
			ELSE (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = v.PaymentReason)
		END AS ClaimAdjustmentReasonCodeId
		,CONVERT(NVARCHAR(MAX),NULL) AS Comment
		,CONVERT(BIT,0) AS IncludeCommentOnStatement
		FROM #VisionAdjustmentsAndFinancialInformation v
		JOIN @AdjustmentTypes at ON at.PaymentType = v.PaymentType
		JOIN model.PatientInsurances p ON p.InsuredPatientId = v.PatientId
			AND p.InsuranceTypeId = 2
			AND (CONVERT(NVARCHAR(8),p.EndDateTime,112) >= v.InvoiceDate OR p.EndDateTime IS NULL)
		JOIN model.InsurancePolicies ip ON ip.Id = p.InsurancePolicyId
			AND ip.PolicyHolderPatientId = v.InsuredId
			AND ip.InsurerId = v.InsurerId
			AND CONVERT(NVARCHAR(8),ip.StartDateTime,112) <= v.InvoiceDate
		JOIN model.InvoiceReceivables ir ON ir.Id = v.InvoiceReceivableId
			AND ir.PatientInsuranceId = p.Id
		LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PAYMENTTYPE' AND 
			v.PaymentType = SUBSTRING(pct.Code, LEN(pct.Code), 1)
		LEFT JOIN @FinancialBatches f ON f.CheckCode = v.PaymentCheck
			AND f.InsurerId = v.InsurerId
			AND f.PaymentDateTime = v.PaymentDate
			AND (CONVERT(NVARCHAR(8),f.ExplanationOfBenefitsDateTime,112) = v.PaymentEOBDate OR f.ExplanationOfBenefitsDateTime IS NULL)
		LEFT JOIN dbo.PracticeCodeTable pctPaymentMethod ON pctPaymentMethod.ReferenceType = 'PAYABLETYPE' 
			AND v.PaymentRefType = SUBSTRING(pctPaymentMethod.Code, LEN(pctPaymentMethod.Code), 1)
		LEFT JOIN (
			SELECT a.*
			,f.CheckCode AS PaymentCheck
			,CONVERT(NVARCHAR(8),f.ExplanationOfBenefitsDateTime,112) AS PaymentEOBDate
			FROM model.Adjustments a 
			LEFT JOIN model.FinancialBatches f ON f.Id = a.FinancialBatchId
			) a ON (a.BillingServiceId = v.PaymentServiceItem OR a.BillingServiceId IS NULL)
			AND a.PostedDateTime = CONVERT(DATETIME,v.PaymentDate)
			AND a.Amount = v.PaymentAmountDecimal
			AND a.InsurerId = v.InsurerId
			AND a.PatientId IS NULL
			AND a.PaymentMethodId = pctPaymentMethod.Id
			AND a.AdjustmentTypeId = at.AdjustmentTypeIds
			AND a.FinancialSourceTypeId = 1
			AND (a.PaymentCheck = v.PaymentCheck OR v.PaymentCheck = '' OR v.PaymentCheck IS NULL)
			AND (a.PaymentEOBDate = v.PaymentEOBDate OR v.PaymentEOBDate = '' OR v.PaymentEOBDate IS NULL)
			AND (a.ClaimAdjustmentGroupCodeId IS NULL OR a.ClaimAdjustmentGroupCodeId = CASE
				WHEN v.ClaimAdjustmentGroupCodeId IS NULL 
					THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
						WHEN 'X'
							THEN 1
						WHEN '8'
							THEN 3
						WHEN 'R'
							THEN 4
						WHEN 'U'
							THEN 4
						END
				ELSE
					v.ClaimAdjustmentGroupCodeId
			END)
			AND (a.ClaimAdjustmentReasonCodeId IS NULL OR a.ClaimAdjustmentReasonCodeId = CASE
				WHEN v.PaymentReason IS NULL OR v.PaymentReason = ''
					THEN 
						CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
							WHEN 'X'
								THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45')
							WHEN '8'
								THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45')
						END
				ELSE (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = v.PaymentReason)
			END)
		WHERE v.PaymentFinancialType <> '' AND v.PaymentFinancialType IS NOT NULL
			AND a.Id IS NULL	
		ORDER BY v.PaymentServiceItem

		UPDATE adj
		SET [LegacyInvoiceReceivableId] = dbo.GetLegacyInvoiceReceivableId(adj.InvoiceReceivableId)
		FROM model.Adjustments adj
		WHERE adj.Id IN (SELECT Id FROM @Adjustments)

		DECLARE @FinancialInformations TABLE (Id INT)
		INSERT INTO model.FinancialInformations (
			Amount
			,PostedDateTime
			,FinancialInformationTypeId
			,BillingServiceId
			,InvoiceReceivableId
			,FinancialSourceTypeId
			,PatientId
			,InsurerId
			,FinancialBatchId
			,ClaimAdjustmentGroupCodeId
			,ClaimAdjustmentReasonCodeId
			,Comment
			,IncludeCommentOnStatement
		)
		OUTPUT inserted.Id INTO @FinancialInformations (Id)
		SELECT DISTINCT
		v.PaymentAmountDecimal AS Amount
		,CONVERT(DATETIME,v.PaymentDate) AS PostedDateTime
		,ft.FinancialInformationTypeIds AS FinancialInformationTypeId
		,v.PaymentServiceItem AS BillingServiceId
		,v.InvoiceReceivableId
		,1 AS FinancialSourceTypeId
		,NULL AS PatientId
		,v.PayerId AS InsurerId
		,f.Id AS FinancialBatchId
		,CASE 
			WHEN v.ClaimAdjustmentGroupCodeId IS NULL
				THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
					-- These int values correspond to the ClaimAdjustmentGroupCode enum in .NET
					WHEN '!' -- FinincialInformationTypeId = 1
						THEN 2 
					WHEN '|' -- FinincialInformationTypeId = 2
						THEN 2 
					WHEN '?' -- FinincialInformationTypeId = 3
						THEN 2 
					WHEN 'D' -- FinincialInformationTypeId = 5
						THEN 3 
					WHEN ']' -- FinincialInformationTypeId = 7
						THEN 3 
					WHEN '0' -- FinancialInformationTypeId = 8
						THEN 2 
					END
			ELSE v.ClaimAdjustmentGroupCodeId
		END AS ClaimAdjustmentGroupCodeId
		,CASE 
			WHEN v.PaymentReason IS NULL
				THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
					
					WHEN '!' -- FinancialInformationTypeId = 1
						THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '1') -- ReasonCode 1
					WHEN '|' -- FinancialInformationTypeId = 2
						THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '2') -- ReasonCode 2
					WHEN '?' -- FinancialInformationTypeId = 3
						THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '3') -- ReasonCode 3
					END
			WHEN v.PaymentReason = ''
				THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
					WHEN '!' 
						THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '1')
					WHEN '|' 
						THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '2')
					WHEN '?' 
						THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '3')
					END
			ELSE (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = v.PaymentReason)
		END AS ClaimAdjustmentReasonCodeId
		,CONVERT(NVARCHAR(MAX),NULL) AS Comment
		,CONVERT(BIT,0) AS IncludeCommentOnStatement
		FROM #VisionAdjustmentsAndFinancialInformation v
		JOIN @FinancialInformationTypes ft ON ft.FinancialInformationType = v.PaymentType
		LEFT JOIN @FinancialBatches f ON f.CheckCode = v.PaymentCheck
			AND f.InsurerId = v.InsurerId
			AND f.PaymentDateTime = v.PaymentDate
			AND (CONVERT(NVARCHAR(8),f.ExplanationOfBenefitsDateTime,112) = v.PaymentEOBDate OR f.ExplanationOfBenefitsDateTime IS NULL)
		LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PAYMENTTYPE'
			AND v.PaymentType = SUBSTRING(pct.Code, LEN(pct.Code), 1)
			AND (pct.AlternateCode = '' OR pct.AlternateCode IS NULL)
		LEFT JOIN (
			SELECT fi.* 
			,f.CheckCode AS PaymentCheck
			,CONVERT(NVARCHAR(8),f.ExplanationOfBenefitsDateTime,112) AS PaymentEOBDate
			FROM model.FinancialInformations fi
			LEFT JOIN model.FinancialBatches f ON f.Id = fi.FinancialBatchId
			) fi ON fi.Amount = v.PaymentAmountDecimal
			AND (fi.BillingServiceId = v.PaymentServiceItem OR fi.BillingServiceId IS NULL)
			AND fi.PostedDateTime = CONVERT(DATETIME,v.PaymentDate)
			AND fi.InsurerId = v.InsurerId
			AND fi.PatientId IS NULL
			AND fi.FinancialSourceTypeId = 1
			AND fi.FinancialInformationTypeId = ft.FinancialInformationTypeIds
			AND (fi.PaymentCheck = v.PaymentCheck OR v.PaymentCheck = '' OR v.PaymentCheck IS NULL)
			AND (fi.PaymentEOBDate = v.PaymentEOBDate OR v.PaymentEOBDate = '' OR v.PaymentEOBDate IS NULL)
			AND (fi.ClaimAdjustmentGroupCodeId IS NULL OR fi.ClaimAdjustmentGroupCodeId = CASE 
				WHEN v.ClaimAdjustmentGroupCodeId IS NULL
					THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
						WHEN '!'
							THEN 2 
						WHEN '|'
							THEN 2 
						WHEN '?'
							THEN 2 
						WHEN 'D'
							THEN 3 
						WHEN ']'
							THEN 3 
						WHEN '0'
							THEN 2 
						END
				ELSE v.ClaimAdjustmentGroupCodeId
			END)
			AND (fi.ClaimAdjustmentReasonCodeId IS NULL OR fi.ClaimAdjustmentReasonCodeId = CASE 
				WHEN v.PaymentReason IS NULL OR v.PaymentReason = ''
					THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
						WHEN '!'
							THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '1') -- ReasonCode 1
						WHEN '|'
							THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '2') -- ReasonCode 2
						WHEN '?'
							THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '3') -- ReasonCode 3
						END
				ELSE (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = v.PaymentReason)
			END)
		WHERE (v.PaymentFinancialType = '' OR v.PaymentFinancialType IS NULL)
			AND fi.Id IS NULL
	
		UPDATE fi
		SET [LegacyInvoiceReceivableId] = dbo.GetLegacyInvoiceReceivableId(fi.InvoiceReceivableId)
		FROM model.FinancialInformations fi
		WHERE fi.Id IN (SELECT Id FROM @FinancialInformations)
	END
END