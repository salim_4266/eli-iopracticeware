﻿IF OBJECT_ID('model.EncounterProcedureOrders','U') IS NOT NULL
	EXEC dbo.DropObject 'model.EncounterProcedureOrders'
GO

IF OBJECT_ID('model.ProcedureOrders','U') IS NOT NULL
	EXEC dbo.DropObject 'model.ProcedureOrders'
GO
