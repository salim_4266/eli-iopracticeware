﻿IF EXISTS(SELECT * FROM dbo.sysobjects WHERE Name = 'UpdateBillingServiceTransactionStatus' AND ObjectProperty(Id, 'IsTrigger') = 1)	
BEGIN
	DROP TRIGGER UpdateBillingServiceTransactionStatus
END

GO

CREATE TRIGGER UpdateBillingServiceTransactionStatus
ON dbo.PracticeTransactionJournal
AFTER UPDATE
AS
SET NOCOUNT ON;
--inserted is a PTJ row
--from the inserted rows, check to see if service transactions exists 
IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
BEGIN
	IF EXISTS (SELECT * FROM ServiceTransactions WHERE TransactionId = (SELECT TOP 1 TransactionId FROM inserted))
	BEGIN
		--only update if the PTJ rows are:
		IF EXISTS (SELECT * FROM inserted i WHERE i.TransactionType = 'R' AND TransactionRef <> 'G') 
			OR 
			EXISTS (SELECT * FROM inserted i INNER JOIN PracticeTransactionJournal ptj ON i.TransactionId = ptj.TransactionId 
						WHERE i.TransactionStatus <> ptj.TransactionStatus)	
						
	IF EXISTS(SELECT * FROM dbo.sysobjects WHERE Name = 'UpdatePracticeTransactionJournalTransactionStatus' AND ObjectProperty(Id, 'IsTrigger') = 1)	
	DISABLE TRIGGER UpdatePracticeTransactionJournalTransactionStatus ON model.BillingServiceTransactions
	
			UPDATE bst
			SET BillingServiceTransactionStatusId = 
			CASE 
				WHEN bst.BillingServiceTransactionStatusId = 1 AND i.TransactionStatus = 'S'
					THEN CASE
							WHEN bst.AmountSent > 0
								THEN 2
							ELSE 8
						END
				WHEN bst.BillingServiceTransactionStatusId IN (1,3) AND i.TransactionStatus = 'Z'
					THEN 5
				WHEN bst.BillingServiceTransactionStatusId IN (2,4) AND i.TransactionStatus = 'P' --8 is included in this because $0 claims that are sent get status of 8, not 2
					THEN 1
				ELSE bst.BillingServiceTransactionStatusId
			END
			,[DateTime] = DATEADD(mi,i.TransactionTime,CONVERT(DATETIME,i.TransactionDate))
			FROM model.BillingServiceTransactions bst
			INNER JOIN inserted i ON bst.LegacyTransactionId = i.TransactionId
	
	IF EXISTS(SELECT * FROM dbo.sysobjects WHERE Name = 'UpdatePracticeTransactionJournalTransactionStatus' AND ObjectProperty(Id, 'IsTrigger') = 1)	
	ENABLE TRIGGER UpdatePracticeTransactionJournalTransactionStatus ON model.BillingServiceTransactions
	
	END
END