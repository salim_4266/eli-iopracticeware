﻿IF ((SELECT TOP 1 PracticeId FROM PracticeName WHERE LocationReference = '' AND PracticeType = 'P') <> 1)
BEGIN
	UPDATE i
	SET i.AttributeToServiceLocationId = (SELECT TOP 1 PracticeId FROM PracticeName WHERE LocationReference = '' AND PracticeType = 'P')
	FROM model.Invoices i
	WHERE i.AttributeToServiceLocationId = 1
END
GO