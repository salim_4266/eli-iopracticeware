﻿
IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Name = 'View Patient Financial Summary Information' AND PermissionCategoryId = 9)
BEGIN

SET IDENTITY_INSERT model.Permissions ON
INSERT INTO model.Permissions (Id, Name, PermissionCategoryId) VALUES (50, 'View Patient Financial Summary Information', 9)
SET IDENTITY_INSERT model.Permissions OFF
END
GO

IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Name = 'Edit Invoice' AND PermissionCategoryId = 9)
BEGIN

SET IDENTITY_INSERT model.Permissions ON
INSERT INTO model.Permissions (Id, Name, PermissionCategoryId) VALUES (51, 'Edit Invoice', 9)
SET IDENTITY_INSERT model.Permissions OFF
END
GO

IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Name = 'Bill Invoice' AND PermissionCategoryId = 9)
BEGIN

SET IDENTITY_INSERT model.Permissions ON
INSERT INTO model.Permissions (Id, Name, PermissionCategoryId) VALUES (52, 'Bill Invoice', 9)
SET IDENTITY_INSERT model.Permissions OFF
END
GO

INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)
SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 12, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id IN (50, 51, 52) -- Patient FinancialsSummary Information
WHERE ResourceType <> 'R'
	AND ResourceId > 0

EXCEPT

SELECT IsDenied, UserId, PermissionId 
FROM model.UserPermissions
