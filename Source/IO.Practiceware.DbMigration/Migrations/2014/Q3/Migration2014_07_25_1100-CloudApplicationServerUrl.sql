﻿IF @@SERVERNAME = 'WIN-23KKEBBHESA' AND 
	NOT EXISTS (SELECT TOP 1 Value FROM model.ApplicationSettings WHERE Name = 'ApplicationServerUrl')
BEGIN
	INSERT INTO model.ApplicationSettings  (Value,Name,ApplicationSettingTypeId) VALUES ('https://cloud.iopracticeware.com/IOPracticeware/','ApplicationServerUrl',11)
END