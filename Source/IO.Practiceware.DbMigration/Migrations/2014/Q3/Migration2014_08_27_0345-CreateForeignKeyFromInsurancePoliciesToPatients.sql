﻿DELETE FROM model.PatientInsurances WHERE InsurancePolicyId NOT IN (SELECT Id FROM model.InsurancePolicies)
DELETE FROM model.PatientInsurances WHERE InsuredPatientId NOT IN (SELECT Id FROM model.Patients)
DELETE FROM model.InsurancePolicies WHERE PolicyHolderPatientId NOT IN (SELECT Id FROM model.Patients)

IF NOT EXISTS(SELECT * 
           FROM sys.foreign_keys 
           WHERE parent_object_id = OBJECT_ID(N'model.InsurancePolicies')
		   AND referenced_object_id = OBJECT_ID(N'model.Patients'))
ALTER TABLE model.InsurancePolicies
ADD CONSTRAINT FK_InsurancePoliciesPatientsPolicyHolderPatientId
FOREIGN KEY (PolicyHolderPatientId)
REFERENCES model.Patients(Id)

IF NOT EXISTS(SELECT * 
           FROM sys.foreign_keys 
           WHERE parent_object_id = OBJECT_ID(N'model.PatientInsurances')
		   AND referenced_object_id = OBJECT_ID(N'model.Patients'))
ALTER TABLE model.PatientInsurances
ADD CONSTRAINT FK_PatientInsurancesPatientsInsuredPatientId
FOREIGN KEY (InsuredPatientId)
REFERENCES model.Patients(Id)