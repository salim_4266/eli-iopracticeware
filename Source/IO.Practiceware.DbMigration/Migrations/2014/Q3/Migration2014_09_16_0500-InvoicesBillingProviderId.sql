﻿IF OBJECT_ID('DropAndCreateIndexesForColumnId', 'P') IS NOT NULL
	DROP PROCEDURE DropAndCreateIndexesForColumnId
GO

CREATE PROCEDURE DropAndCreateIndexesForColumnId (
	@TableName SYSNAME
	, @SchemaName SYSNAME = 'dbo'
	, @ColumnId INT = NULL
	, @SORT_IN_TEMPDB VARCHAR(3) = 'OFF'
	, @DROP_EXISTING VARCHAR(3) = 'OFF'
	, @STATISTICS_NORECOMPUTE VARCHAR(3) = 'OFF'
	, @ONLINE VARCHAR(3) = 'OFF'
)
AS
/* 
Parameters 
@Schemaname - SchemaName to which the table belongs to. Default value 'dbo'. 
@Tablename - TableName for which the Indexes need to be scripted. 
@SORT_IN_TEMPDB - Runtime value for SORT_IN_TEMPDB option in create index. Valid Values ON \ OFF. Default = 'OFF' 
@DROP_EXISTING - Runtime value for DROP_EXISTING option in create index. Valid Values ON \ OFF. Default = 'OFF' 
@STATISTICS_NORECOMPUTE - Runtime value for STATISTICS_NORECOMPUTE option in create index. Valid Values ON \ OFF. Default = 'OFF' 
@ONLINE - Runtime value for ONLINE option in create index. Valid Values ON \ OFF. Default = 'OFF' 
*/
SET NOCOUNT ON

IF @SORT_IN_TEMPDB NOT IN ('ON', 'OFF')
BEGIN
	RAISERROR ('Valid value for @SORT_IN_TEMPDB is ON \ OFF', 16, 1)

	RETURN
END

IF @DROP_EXISTING NOT IN ('ON', 'OFF')
BEGIN
	RAISERROR ('Valid value for @DROP_EXISTING is ON \ OFF', 16, 1)

	RETURN
END

IF @STATISTICS_NORECOMPUTE NOT IN ('ON', 'OFF')
BEGIN
	RAISERROR ('Valid value for @STATISTICS_NORECOMPUTE is ON \ OFF', 16, 1)

	RETURN
END

IF @ONLINE NOT IN ('ON', 'OFF')
BEGIN
	RAISERROR ('Valid value for @ONLINE is ON \ OFF', 16, 1)

	RETURN
END

DECLARE @IDXTable TABLE (Schema_ID INT, Object_ID INT, Index_ID INT, SchemaName SYSNAME, TableName SYSNAME, IndexName SYSNAME, IsPrimaryKey BIT, IndexType INT, CreateScript VARCHAR(MAX) NULL, DropScript VARCHAR(MAX) NULL, ExistsScript VARCHAR(MAX) NULL, Processed BIT NULL)

INSERT INTO @IDXTable (Schema_ID, Object_ID, Index_ID, SchemaName, TableName, IndexName, IsPrimaryKey, IndexType)
SELECT ST.Schema_id, ST.Object_id, SI.Index_id, SCH.NAME, ST.NAME, SI.NAME, SI.is_primary_key, SI.Type
FROM SYS.INDEXES SI
INNER JOIN SYS.TABLES ST ON SI.Object_ID = ST.Object_ID
INNER JOIN SYS.SCHEMAS SCH ON SCH.schema_id = ST.schema_id
LEFT JOIN sys.index_columns ic ON ic.index_id = si.index_id AND ic.object_id = si.object_id
WHERE SCH.NAME = @SchemaName AND ST.NAME = @TableName AND SI.Type IN (1, 2, 3) AND (ic.column_id = @columnId OR @columnId IS NULL)

DECLARE @SchemaID INT
DECLARE @TableID INT
DECLARE @IndexID INT
DECLARE @isPrimaryKey BIT
DECLARE @IndexType INT
DECLARE @CreateSQL VARCHAR(MAX)
DECLARE @IndexColsSQL VARCHAR(MAX)
DECLARE @WithSQL VARCHAR(MAX)
DECLARE @IncludeSQL VARCHAR(MAX)
DECLARE @WhereSQL VARCHAR(MAX)
DECLARE @SQLTODROP VARCHAR(MAX)
DECLARE @SQLTOCREATE VARCHAR(MAX)
DECLARE @DropSQL VARCHAR(MAX)
DECLARE @ExistsSQL VARCHAR(MAX)
DECLARE @IndexName SYSNAME
DECLARE @TblSchemaName SYSNAME

SET @TblSchemaName = QUOTENAME(@Schemaname) + '.' + QUOTENAME(@TableName)
SET @CreateSQL = ''
SET @IndexColsSQL = ''
SET @WithSQL = ''
SET @IncludeSQL = ''
SET @WhereSQL = ''

WHILE EXISTS (
		SELECT 1
		FROM @IDXTable
		WHERE CreateScript IS NULL
		)
BEGIN
	SELECT TOP 1 @SchemaID = Schema_ID, @TableID = Object_ID, @IndexID = Index_ID, @isPrimaryKey = IsPrimaryKey, @IndexName = IndexName, @IndexType = IndexType
	FROM @IDXTable
	WHERE CreateScript IS NULL AND SchemaName = @SchemaName AND TableName = @TableName
	ORDER BY Index_ID

	IF @isPrimaryKey = 1
	BEGIN
		SELECT @ExistsSQL = ' EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''' + @TblSchemaName + ''') AND name = N''' + @IndexName + ''')'

		SELECT @DropSQL = ' ALTER TABLE ' + @TblSchemaName + ' DROP CONSTRAINT [' + @IndexName + ']'
	END
	ELSE
	BEGIN
		SELECT @ExistsSQL = ' EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''' + @TblSchemaName + ''') AND name = N''' + @IndexName + ''')'

		SELECT @DropSQL = ' DROP INDEX [' + @IndexName + '] ON ' + @TblSchemaName + CASE WHEN @IndexType IN (1, 2) THEN ' WITH ( ONLINE = OFF )' ELSE '' END
	END

	IF @IndexType IN (1, 2)
	BEGIN
		SELECT @CreateSQL = CASE WHEN SI.is_Primary_Key = 1 THEN 'ALTER TABLE ' + @TblSchemaName + ' ADD  CONSTRAINT [' + @IndexName + '] PRIMARY KEY ' + SI.type_desc WHEN SI.Type IN (1, 2) THEN ' CREATE ' + CASE SI.is_Unique WHEN 1 THEN ' UNIQUE ' ELSE '' END + SI.type_desc + ' INDEX ' + QUOTENAME(SI.NAME) + ' ON ' + @TblSchemaName END, @IndexColsSQL = (
				SELECT SC.NAME + ' ' + CASE SIC.is_descending_key WHEN 0 THEN ' ASC ' ELSE 'DESC' END + ','
				FROM SYS.INDEX_COLUMNS SIC
				INNER JOIN SYS.COLUMNS SC ON SIC.Object_ID = SC.Object_ID AND SIC.Column_ID = SC.Column_ID
				WHERE SIC.OBJECT_ID = SI.Object_ID AND SIC.Index_ID = SI.Index_ID AND SIC.is_included_column = 0
				ORDER BY SIC.Key_Ordinal
				FOR XML PATH('')
				), @WithSQL = ' WITH (PAD_INDEX  = ' + CASE SI.is_padded WHEN 1 THEN 'ON' ELSE 'OFF' END + ',' + CHAR(13) + ' IGNORE_DUP_KEY = ' + CASE SI.ignore_dup_key WHEN 1 THEN 'ON' ELSE 'OFF' END + ',' + CHAR(13) + ' ALLOW_ROW_LOCKS = ' + CASE SI.Allow_Row_Locks WHEN 1 THEN 'ON' ELSE 'OFF' END + ',' + CHAR(13) + ' ALLOW_PAGE_LOCKS = ' + CASE SI.Allow_Page_Locks WHEN 1 THEN 'ON' ELSE 'OFF' END + ',' + CHAR(13) + CASE SI.Type WHEN 2 THEN 'SORT_IN_TEMPDB = ' + @SORT_IN_TEMPDB + ',DROP_EXISTING = ' + @DROP_EXISTING + ',' ELSE '' END + CASE WHEN SI.Fill_Factor > 0 THEN ' FILLFACTOR = ' + CONVERT(VARCHAR(3), SI.Fill_Factor) + ',' ELSE '' END + ' STATISTICS_NORECOMPUTE  = ' + @STATISTICS_NORECOMPUTE + ', SORT_IN_TEMPDB = ' + @SORT_IN_TEMPDB + ', ONLINE = ' + @ONLINE + ') ON ' + QUOTENAME(SFG.NAME), @IncludeSQL = (
				SELECT QUOTENAME(SC.NAME) + ','
				FROM SYS.INDEX_COLUMNS SIC
				INNER JOIN SYS.COLUMNS SC ON SIC.Object_ID = SC.Object_ID AND SIC.Column_ID = SC.Column_ID
				WHERE SIC.OBJECT_ID = SI.Object_ID AND SIC.Index_ID = SI.Index_ID AND SIC.is_included_column = 1
				ORDER BY SIC.Key_Ordinal
				FOR XML PATH('')
				), @WhereSQL = NULL
		FROM SYS.Indexes SI
		INNER JOIN SYS.FileGroups SFG ON SI.Data_Space_ID = SFG.Data_Space_ID
		WHERE Object_ID = @TableID AND Index_ID = @IndexID

		SELECT @IndexColsSQL = '(' + SUBSTRING(@IndexColsSQL, 1, LEN(@IndexColsSQL) - 1) + ')'

		IF LTRIM(RTRIM(@IncludeSQL)) <> ''
			SELECT @IncludeSQL = ' INCLUDE (' + SUBSTRING(@IncludeSQL, 1, LEN(@IncludeSQL) - 1) + ')'

		IF LTRIM(RTRIM(@WhereSQL)) <> ''
			SELECT @WhereSQL = ' WHERE (' + @WhereSQL + ')'
	END

	IF @IndexType = 3
	BEGIN
		SELECT @CreateSQL = ' CREATE ' + CASE WHEN SI.Using_xml_index_id IS NULL THEN ' PRIMARY ' ELSE '' END + SI.type_desc + ' INDEX ' + QUOTENAME(SI.NAME) + ' ON ' + @TblSchemaName, @IndexColsSQL = (
				SELECT SC.NAME + ' ' + ','
				FROM SYS.INDEX_COLUMNS SIC
				INNER JOIN SYS.COLUMNS SC ON SIC.Object_ID = SC.Object_ID AND SIC.Column_ID = SC.Column_ID
				WHERE SIC.OBJECT_ID = SI.Object_ID AND SIC.Index_ID = SI.Index_ID AND SIC.is_included_column = 0
				ORDER BY SIC.Key_Ordinal
				FOR XML PATH('')
				), @WithSQL = ' WITH (PAD_INDEX  = ' + CASE SI.is_padded WHEN 1 THEN 'ON' ELSE 'OFF' END + ',' + CHAR(13) + ' ALLOW_ROW_LOCKS = ' + CASE SI.Allow_Row_Locks WHEN 1 THEN 'ON' ELSE 'OFF' END + ',' + CHAR(13) + ' ALLOW_PAGE_LOCKS = ' + CASE SI.Allow_Page_Locks WHEN 1 THEN 'ON' ELSE 'OFF' END + ',' + CHAR(13) + CASE SI.Type WHEN 2 THEN 'SORT_IN_TEMPDB = OFF,DROP_EXISTING = OFF,' ELSE '' END + CASE WHEN SI.Fill_Factor > 0 THEN ' FILLFACTOR = ' + CONVERT(VARCHAR(3), SI.Fill_Factor) + ',' ELSE '' END + 'SORT_IN_TEMPDB = OFF ' + ') ', @IncludeSQL = ' USING XML INDEX [' + (
				SELECT NAME
				FROM SYS.XML_Indexes SIP
				WHERE SIP.Object_ID = SI.Object_ID AND SIP.Index_ID = SI.Using_XML_Index_ID
				) + '] FOR PATH '
		FROM SYS.XML_Indexes SI
		WHERE SI.Object_ID = @TableID AND SI.Index_ID = @IndexID

		SELECT @IndexColsSQL = '(' + SUBSTRING(@IndexColsSQL, 1, LEN(@IndexColsSQL) - 1) + ')'
	END

	SELECT @CreateSQL = @CreateSQL + @IndexColsSQL + CASE WHEN @IndexColsSQL <> '' THEN CHAR(13) ELSE '' END + ISNULL(@IncludeSQL, '') + CASE WHEN @IncludeSQL <> '' THEN CHAR(13) ELSE '' END + ISNULL(@WhereSQL, '') + CASE WHEN @WhereSQL <> '' THEN CHAR(13) ELSE '' END + @WithSQL

	UPDATE @IDXTable
	SET CreateScript = @CreateSQL, DropScript = @DropSQL, ExistsScript = @ExistsSQL
	WHERE Schema_ID = @SchemaID AND Object_ID = @TableID AND Index_ID = @IndexID
END

UPDATE @IDXTable
SET Processed = 0
WHERE SchemaName = @SchemaName AND TableName = @TableName

WHILE EXISTS (
		SELECT 1
		FROM @IDXTable
		WHERE ISNULL(Processed, 0) = 0 AND SchemaName = @SchemaName AND TableName = @TableName
		)
BEGIN
	SELECT @SQLTODROP = ''

	SELECT TOP 1 @SchemaID = Schema_ID, @TableID = Object_ID, @IndexID = Index_ID, @SQLTODROP = 'IF ' + ExistsScript + CHAR(13) + DropScript + CHAR(13)
	FROM @IDXTable
	WHERE ISNULL(Processed, 0) = 0 AND SchemaName = @SchemaName AND TableName = @TableName
	ORDER BY IndexType DESC, Index_id DESC

	SELECT @SQLTODROP, 'DROP'

	UPDATE @IDXTable
	SET Processed = 1
	WHERE Schema_ID = @SchemaID AND Object_ID = @TableID AND Index_ID = @IndexID
END

UPDATE @IDXTable
SET Processed = 0
WHERE SchemaName = @SchemaName AND TableName = @TableName

WHILE EXISTS (
		SELECT 1
		FROM @IDXTable
		WHERE ISNULL(Processed, 0) = 0 AND SchemaName = @SchemaName AND TableName = @TableName
		)
BEGIN
	SELECT @SQLTOCREATE = ''

	SELECT TOP 1 @SchemaID = Schema_ID, @TableID = Object_ID, @IndexID = Index_ID, @SQLTOCREATE = 'IF NOT ' + ExistsScript + CHAR(13) + CreateScript + CHAR(13)
	FROM @IDXTable
	WHERE ISNULL(Processed, 0) = 0 AND SchemaName = @SchemaName AND TableName = @TableName
	ORDER BY IndexType DESC, Index_id DESC

	SELECT @SQLTOCREATE, 'CREATE'

	UPDATE @IDXTable
	SET Processed = 1
	WHERE Schema_ID = @SchemaID AND Object_ID = @TableID AND Index_ID = @IndexID
END
GO

IF OBJECT_ID('tempdb..#sqlstatements','U') IS NOT NULL
	DROP TABLE #sqlstatements
GO

CREATE TABLE #sqlstatements ([sql] NVARCHAR(MAX), [Action] NVARCHAR(MAX))

DECLARE @columnId INT
SELECT @columnId = column_id FROM sys.index_columns WHERE object_id = OBJECT_ID('model.Invoices') AND column_id = (SELECT column_id FROM sys.columns WHERE object_id = OBJECT_ID('model.Invoices') AND name = 'BillingProviderId')

INSERT INTO #sqlstatements ([sql], [Action])
SELECT 'IF EXISTS (SELECT * FROM sys.stats WHERE name = ''' + s.name + ''') BEGIN DROP STATISTICS ' + QUOTENAME(OBJECT_SCHEMA_NAME(OBJECT_ID('model.Invoices'))) + '.' + QUOTENAME(OBJECT_NAME(OBJECT_ID('model.Invoices'))) + '.' + QUOTENAME(s.name) + ' END; ', 'DROP'
FROM sys.stats_columns sc 
JOIN sys.stats s ON s.stats_id = sc.stats_id AND sc.object_id = s.object_id 
LEFT JOIN sys.indexes i ON s.name = i.name
WHERE column_id = @columnId 
	AND sc.object_id = OBJECT_ID('model.Invoices')
	AND i.object_id IS NULL

INSERT INTO #sqlstatements ([sql], [Action])
EXEC dbo.DropAndCreateIndexesForColumnId  'Invoices','model', @columnId

DECLARE @loop INT
DECLARE @sql NVARCHAR(MAX)

SET @loop = 1
WHILE (@loop IS NOT NULL)
BEGIN
	SET @sql = ''
	SELECT TOP 1 @sql = [sql] FROM #sqlstatements WHERE [Action] = 'DROP'
	EXEC sp_executesql @sql
	DELETE FROM #sqlstatements WHERE [Action] = 'DROP' AND [sql] = @sql
	IF NOT EXISTS (SELECT * FROM #sqlstatements WHERE [Action] = 'DROP') 
	BEGIN
		SET @loop = NULL
	END
END

ALTER TABLE [model].[Invoices] ALTER COLUMN [BillingProviderId] INT NOT NULL 
GO

DECLARE @loop INT
DECLARE @sql NVARCHAR(MAX)
SET @loop = 1
WHILE (@loop IS NOT NULL)
BEGIN
	SET @sql = ''
	SELECT TOP 1 @sql = [sql] FROM #sqlstatements WHERE [Action] = 'CREATE'
	EXEC sp_executesql @sql
	DELETE FROM #sqlstatements WHERE [Action] = 'CREATE' AND [sql] = @sql
	IF NOT EXISTS (SELECT * FROM #sqlstatements WHERE [Action] = 'CREATE') 
	BEGIN
		SET @loop = NULL
	END
END
GO

IF OBJECT_ID('DropAndCreateIndexesForColumnId', 'P') IS NOT NULL
	DROP PROCEDURE DropAndCreateIndexesForColumnId
GO

-- Creating foreign key on [BillingProviderId] in table 'Invoices'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ProviderInvoice'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Invoices] DROP CONSTRAINT FK_ProviderInvoice
GO

IF OBJECTPROPERTY(OBJECT_ID('[model].[Providers]'), 'IsUserTable') = 1
ALTER TABLE [model].[Invoices]
ADD CONSTRAINT [FK_ProviderInvoice]
    FOREIGN KEY ([BillingProviderId])
    REFERENCES [model].[Providers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
-- Creating non-clustered index for FOREIGN KEY 'FK_ProviderInvoice'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ProviderInvoice')
	DROP INDEX IX_FK_ProviderInvoice ON [model].[Invoices]
GO

CREATE INDEX [IX_FK_ProviderInvoice]
ON [model].[Invoices]
    ([BillingProviderId]);
GO