﻿IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'dbo.PracticeTransactionJournal'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0
	AND name NOT LIKE '%Audit%'

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

;WITH CTE AS (
SELECT 
b.BillingServiceTransactionStatusId
,p.TransactionStatus
,s.TransportAction
,b.AmountSent
,i.LegacyPatientReceivablesInvoice
,p.TransactionId
FROM model.BillingServiceTransactions b
INNER JOIN PracticeTransactionJournal p ON b.LegacyTransactionId = p.TransactionId
INNER JOIN ServiceTransactions s ON s.id = b.Id
INNER JOIN model.InvoiceReceivables ir ON ir.id = b.InvoiceReceivableId
INNER JOIN model.invoices i ON i.id = ir.InvoiceId
INNER JOIN PatientReceivableServices prs ON prs.itemid = b.BillingServiceId
WHERE BillingServiceTransactionStatusId IN (5,6,7)
	AND p.TransactionStatus <> 'Z'
)
UPDATE p
SET p.TransactionStatus = 'Z'
FROM dbo.PracticeTransactionJournal p
JOIN CTE ON CTE.TransactionId = p.TransactionId

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.BillingServiceTransactions'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0
	AND name NOT LIKE '%Audit%'

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

;WITH CTE AS (
SELECT 
bst.Id
,bst.BillingServiceTransactionStatusId AS CurrentBillingServiceTransactionId
,CASE 
	WHEN bst.BillingServiceTransactionStatusId = 1 AND ptj.TransactionStatus = 'S'
		THEN CASE
				WHEN bst.AmountSent > 0
					THEN 2
				ELSE 8
			END
	WHEN bst.BillingServiceTransactionStatusId IN (1,3) AND ptj.TransactionStatus = 'Z'
		THEN 5
	WHEN bst.BillingServiceTransactionStatusId = 2 AND ptj.TransactionStatus = 'Z'
		THEN 6
	WHEN bst.BillingServiceTransactionStatusId = 4 AND ptj.TransactionStatus = 'Z'
		THEN 7
	WHEN bst.BillingServiceTransactionStatusId IN (2,4) AND ptj.TransactionStatus = 'P' --8 is included in this because $0 claims that are sent get status of 8, not 2
		THEN 1
	ELSE bst.BillingServiceTransactionStatusId
END AS WhatTheStatusShouldNowBe
FROM dbo.PracticeTransactionJournal ptj
INNER JOIN model.BillingServiceTransactions bst on ptj.TransactionId = bst.LegacyTransactionId
WHERE ptj.TransactionStatus = 'Z'
	AND bst.BillingServiceTransactionStatusId in (1,2,3,4)
)
UPDATE bst
SET bst.BillingServiceTransactionStatusId = CTE.WhatTheStatusShouldNowBe
FROM model.BillingServiceTransactions bst
JOIN CTE ON CTE.Id = bst.Id
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO