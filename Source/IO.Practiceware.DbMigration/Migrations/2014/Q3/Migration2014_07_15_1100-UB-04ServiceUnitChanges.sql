﻿
IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'UB-04'
		AND Content IS NOT NULL)
BEGIN	
	DECLARE @old NVARCHAR(MAX)
	DECLARE @new NVARCHAR(MAX)
    SET @old = '<input id="ServiceUnits" value="@service.ServiceUnits"'
	SET @new = 'if (service.ServiceUnits != null && service.ServiceUnits == (int) service.ServiceUnits)
                {
                    <input id="ServiceUnits" value="@service.ServiceUnits.Value.ToString("N0")" type="text" style="position: absolute; top: @(beginTop + (index*incrementalPixels))px; width: 67px; bottom: 734px; left: 606px; text-align: right;" />
                }
                else
                {
                    <input id="ServiceUnits" value="@service.ServiceUnits" type="text" style="position: absolute; top: @(beginTop + (index*incrementalPixels))px; width: 67px; bottom: 734px; left: 606px; text-align: right;" />
                }'

	DECLARE @Content NVARCHAR(MAX)
	SET @Content = (SELECT CONVERT(varchar(max),Content) FROM model.TemplateDocuments WHERE DisplayName = 'UB-04' AND Content IS NOT NULL) 
	DECLARE @StartIndex INT
	SET @StartIndex = CHARINDEX(@old, @Content)
	DECLARE @Length INT
	SET @Length = (CHARINDEX('/>', @Content, @StartIndex) - @StartIndex +2)
	SET @Content =  SUBSTRING(@Content, @StartIndex, @Length)
	UPDATE model.TemplateDocuments 	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 
	@Content,
	@new))
	WHERE DisplayName = 'UB-04' AND Content IS NOT NULL	
END