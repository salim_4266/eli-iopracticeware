﻿IF OBJECT_ID('tempdb..#DuplicatedBillingServiceTransactions','U') IS NOT NULL
	DROP TABLE #DuplicatedBillingServiceTransactions
GO

DELETE FROM model.BillingServiceTransactions WHERE Id IN (
SELECT 
b.Id
FROM model.BillingServiceTransactions b
JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionId = b.LegacyTransactionId
WHERE LegacyTransactionId IS NOT NULL
	AND ptj.TransactionType <> 'R'
)

;WITH CTE AS (
SELECT [DateTime]
,AmountSent
,BillingServiceTransactionStatusId
,MethodSentId
,BillingServiceId
,InvoiceReceivableId
,ClaimFileReceiverId
FROM model.BillingServiceTransactions
WHERE BillingServiceTransactionStatusId IN (1, 2, 3, 4)
GROUP BY [DateTime]
,AmountSent
,BillingServiceTransactionStatusId
,MethodSentId
,BillingServiceId
,InvoiceReceivableId
,ClaimFileReceiverId
HAVING COUNT(*) > 1
)
,CTE2 AS (
SELECT 
b.* 
,ROW_NUMBER() OVER (PARTITION BY b.BillingServiceId ORDER BY b.ExternalSystemMessageId DESC, b.Id) AS Rn
FROM model.BillingServiceTransactions b
JOIN CTE ON CTE.[DateTime] = b.[DateTime]
	AND CTE.AmountSent = b.AmountSent
	AND CTE.BillingServiceTransactionStatusId = b.BillingServiceTransactionStatusId
	AND CTE.MethodSentId = b.MethodSentId
	AND CTE.BillingServiceId = b.BillingServiceId
	AND CTE.InvoiceReceivableId = b.InvoiceReceivableId
)
SELECT 
* 
INTO #DuplicatedBillingServiceTransactions
FROM CTE2 c
WHERE c.Rn > 1

;WITH CTE AS (
SELECT 
b.* 
FROM model.BillingServiceTransactions b 
WHERE b.Id IN (SELECT Id FROM #DuplicatedBillingServiceTransactions)
)
DELETE FROM CTE