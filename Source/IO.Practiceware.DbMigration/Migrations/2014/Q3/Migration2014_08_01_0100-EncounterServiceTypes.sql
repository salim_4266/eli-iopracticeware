﻿IF OBJECT_ID('model.EncounterServiceTypes') IS NOT NULL
	EXEC dbo.DropObject 'model.EncounterServiceTypes'
GO

-- 'EncounterServiceTypes' does not exist, creating...
CREATE TABLE [model].[EncounterServiceTypes] (
	[Id] int IDENTITY(1,1) NOT NULL,
	[Name] varchar(max)  NULL,
	[IsArchived] bit  NOT NULL
);

-- Creating primary key on [Id] in table 'EncounterServiceTypes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterServiceTypes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterServiceTypes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterServiceTypes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterServiceTypes]
ADD CONSTRAINT [PK_EncounterServiceTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

INSERT INTO model.EncounterServiceTypes (Name, IsArchived)
SELECT
dbo.CamelCase(Code) AS [Name]
,CONVERT(BIT, 0) AS IsArchived
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'SERVICECATEGORY'
GO