﻿DECLARE @InvalidPatientInsurances TABLE (Id INT)
INSERT INTO @InvalidPatientInsurances (Id)
SELECT 
p.Id
FROM model.PatientInsurances p
LEFT JOIN model.InsurancePolicies i ON i.Id = p.InsurancePolicyId 
WHERE i.InsurerId IN (
		SELECT DISTINCT
		i.InsurerId
		FROM model.InsurancePolicies i
		LEFT JOIN model.Insurers ins ON ins.Id = i.InsurerId
		WHERE ins.Id IS NULL
	)

SELECT * INTO dbo.DeletedBillingServiceTransactionsMissingInsurerId FROM model.BillingServiceTransactions WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
SELECT * INTO dbo.DeletedAdjustmentsMissingInsurerId FROM model.Adjustments WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
SELECT * INTO dbo.DeletedFinancialInformationsMissingInsurerId FROM model.FinancialInformations WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
SELECT * INTO dbo.DeletedInvoiceReceivablesMissingInsurerId FROM model.InvoiceReceivables WHERE Id IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
SELECT * INTO dbo.DeletedPatientInsuranceDocumentsMissingInsurerId FROM model.PatientInsuranceDocuments WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances)
SELECT * INTO dbo.DeletedPatientInsurancesMissingInsurerId FROM model.PatientInsurances WHERE Id IN (SELECT Id FROM @InvalidPatientInsurances)
SELECT * INTO dbo.DeletedInsurancePoliciesInsurerId FROM model.InsurancePolicies WHERE InsurerId IN (
		SELECT DISTINCT
		i.InsurerId
		FROM model.InsurancePolicies i
		LEFT JOIN model.Insurers ins ON ins.Id = i.InsurerId
		WHERE ins.Id IS NULL
	)

DELETE FROM model.BillingServiceTransactions WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
DELETE FROM model.Adjustments WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
DELETE FROM model.FinancialInformations WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
DELETE FROM model.InvoiceReceivables WHERE Id IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
DELETE FROM model.PatientInsuranceDocuments WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances)
DELETE FROM model.PatientInsurances WHERE Id IN (SELECT Id FROM @InvalidPatientInsurances)
DELETE FROM model.InsurancePolicies WHERE InsurerId IN (
		SELECT DISTINCT
		i.InsurerId
		FROM model.InsurancePolicies i
		LEFT JOIN model.Insurers ins ON ins.Id = i.InsurerId
		WHERE ins.Id IS NULL
	)
GO

IF OBJECT_ID('model.Insurers') IS NOT NULL
	EXEC dbo.DropObject 'model.Insurers'
GO

-- 'Insurers' does not exist, creating...
CREATE TABLE [model].[Insurers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [GroupName] nvarchar(32)  NULL,
    [PlanName] nvarchar(32)  NULL,
    [IsReferralRequired] bit  NOT NULL,
    [PayerCode] nvarchar(max)  NULL,
    [MedigapCode] nvarchar(max)  NULL,
    [AllowDependents] bit  NOT NULL,
    [IsSecondaryClaimPaper] bit  NOT NULL,
    [PolicyNumberFormat] nvarchar(max)  NULL,
    [JurisdictionStateOrProvinceId] int  NULL,
    [SendZeroCharge] bit  NOT NULL,
    [InsurerPayTypeId] int  NULL,
    [InsurerBusinessClassId] int  NULL,
    [InsurerPlanTypeId] int  NULL,
    [ClaimFileReceiverId] int  NULL,
    [IsArchived] bit  NOT NULL,
    [IsMedicareAdvantage] bit  NOT NULL,
    [Hpid] nvarchar(255)  NULL,
    [Oeid] nvarchar(255)  NULL,
    [Website] nvarchar(2048)  NULL,
    [Comment] nvarchar(max)  NULL,
    [ClaimFilingIndicatorCodeId] int  NOT NULL,
    [PriorInsurerCode] nvarchar(max)  NULL,
	[LegacyPracticeInsurersId] int NULL
);
GO

-- Creating primary key on [Id] in table 'Insurers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.Insurers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[Insurers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.Insurers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[Insurers]
ADD CONSTRAINT [PK_Insurers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

SET IDENTITY_INSERT [model].[Insurers] ON
INSERT INTO model.Insurers ([Id], [Name], [IsReferralRequired], [AllowDependents], [IsSecondaryClaimPaper],
	[SendZeroCharge], [IsArchived], [IsMedicareAdvantage], [ClaimFilingIndicatorCodeId],
	[LegacyPracticeInsurersId])
SELECT 0, 'ZZZ No insurance', CONVERT(BIT,0), CONVERT(BIT,0), CONVERT(BIT,0), CONVERT(BIT,0), CONVERT(BIT,0), CONVERT(BIT,0), 12 AS ClaimFilingIndicatorCodeId, 0
UNION
SELECT 99999, 'ZZZ On account insurance', CONVERT(BIT,0), CONVERT(BIT,0), CONVERT(BIT,0), CONVERT(BIT,0), CONVERT(BIT,0), CONVERT(BIT,0), 12 AS ClaimFilingIndicatorCodeId, 99999
SET IDENTITY_INSERT [model].[Insurers] OFF

DBCC CHECKIDENT ('model.Insurers', RESEED, 0)

INSERT INTO model.Insurers ([Name], [GroupName], [PlanName], [IsReferralRequired], [PayerCode],
    [MedigapCode], [AllowDependents], [IsSecondaryClaimPaper], [PolicyNumberFormat], [JurisdictionStateOrProvinceId],
    [SendZeroCharge], [InsurerPayTypeId], [InsurerBusinessClassId], [InsurerPlanTypeId], [ClaimFileReceiverId],
	[IsArchived], [IsMedicareAdvantage], [Hpid], [Oeid], [Website], [Comment], [ClaimFilingIndicatorCodeId],
    [PriorInsurerCode],	[LegacyPracticeInsurersId])
SELECT 
InsurerName AS Name
,CASE WHEN InsurerGroupName <> '' AND InsurerGroupName IS NOT NULL THEN InsurerGroupName ELSE NULL END AS GroupName
,CASE WHEN InsurerPlanName <> '' AND InsurerPlanName IS NOT NULL THEN InsurerPlanName ELSE NULL END AS PlanName
,CASE ReferralRequired WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsReferralRequired
,CASE WHEN NeicNumber <> '' AND NeicNumber IS NOT NULL THEN NeicNumber ELSE NULL END AS PayerCode
,CASE WHEN MedicareCrossOverId <> '' AND MedicareCrossOverId IS NOT NULL THEN MedicareCrossOverId ELSE NULL END MedigapCode
,CASE WHEN AllowDependents <> '' AND AllowDependents IS NOT NULL THEN CASE WHEN AllowDependents = CONVERT(BIT, 1) THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END ELSE CONVERT(BIT, 0) END AS AllowDependents
,CASE [Availability] WHEN 'A' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsSecondaryClaimPaper
,CASE WHEN InsurerPlanFormat <> '' AND InsurerPlanFormat IS NOT NULL THEN InsurerPlanFormat END AS PolicyNumberFormat
,st.Id AS JurisdictionStateorProvinceId
,CASE InsurerServiceFilter WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS SendZeroCharge
,CASE InsurerPayType
	WHEN 'SP'
		THEN 1
	WHEN 'OT'
		THEN 2
	WHEN 'LT'
		THEN 3
	WHEN 'IP'
		THEN 4
	WHEN 'GP'
		THEN 5
	WHEN 'PP'
		THEN 6
	WHEN 'MG'
		THEN 7
	WHEN 'LD'
		THEN 8
	WHEN 'AP'
		THEN 9
	ELSE NULL
END AS InsurerPayTypeId
,CASE
	WHEN pctBusClass.Code = 'AMERIH'
		THEN 1
	WHEN pctBusClass.Code = 'BLUES'
		THEN 2
	WHEN pctBusClass.Code = 'COMM'
		THEN 3
	WHEN pctBusClass.Code = 'MCAIDFL'
		THEN 4
	WHEN pctBusClass.Code = 'MCAIDNC'
		THEN 5
	WHEN pctBusClass.Code = 'MCAIDNJ'
		THEN 6
	WHEN pctBusClass.Code = 'MCAIDNV'
		THEN 7
	WHEN pctBusClass.Code = 'MCAIDNY'
		THEN 8
	WHEN pctBusClass.Code = 'MCARENJ'
		THEN 9
	WHEN pctBusClass.Code = 'MCARENV'
		THEN 10
	WHEN pctBusClass.Code = 'MCARENY'
		THEN 11
	WHEN pctBusClass.Code = 'TEMPLATE'
		THEN 12
	WHEN pctBusClass.Code IS NULL
		THEN 3
	WHEN pctBusClass.Code = ''
		THEN 3
	ELSE pctBusClass.Id + 1000
END AS InsurerBusinessClassId
,pctPlanType.Id AS InsurerPlanTypeId
,pctClaimReceiver.Id AS ClaimFileReceiverId
,CASE WHEN IsArchived <> '' AND IsArchived IS NOT NULL THEN CASE WHEN IsArchived = CONVERT(BIT, 1) THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END ELSE CONVERT(BIT, 0) END AS IsArchived
,IsMedicareAdvantage AS IsMedicareAdvantage
,Hpid AS Hpid
,Oeid AS Oeid
,Website AS Website
,CASE WHEN InsurerComment <> '' AND InsurerComment IS NOT NULL THEN CONVERT(NVARCHAR(MAX), InsurerComment) END AS Comment
,CASE 
	WHEN InsurerReferenceCode = 'P'
		THEN 10
	WHEN InsurerReferenceCode = 'K'
		THEN 1
	WHEN InsurerReferenceCode IN ('X', 'G', 'Q', 'L', 'A', 'U', 'O', '', NULL)
		THEN 12
	WHEN InsurerReferenceCode = 'F'
		THEN 14
	WHEN InsurerReferenceCode = 'I'
		THEN 7
	WHEN InsurerReferenceCode = 'C'
		THEN 19
	WHEN InsurerReferenceCode IN ('N', 'M', '[')
		THEN 18
	WHEN InsurerReferenceCode = 'Z'
		THEN 24
	WHEN InsurerReferenceCode = 'Y'
		THEN 20
	WHEN InsurerReferenceCode = 'T'
		THEN 21
	WHEN InsurerReferenceCode = 'H'
		THEN 11
	WHEN InsurerReferenceCode = 'V'
		THEN 22
	WHEN InsurerReferenceCode = 'W'
		THEN 23
	ELSE 2
END AS ClaimFilingIndicatorCodeId
,CASE WHEN InsurerProvPhone <> '' AND InsurerProvPhone IS NOT NULL THEN InsurerProvPhone END AS PriorInsurerCode 
,pri.InsurerId AS LegacyPracticeInsurersId
FROM dbo.PracticeInsurers pri
LEFT JOIN dbo.PracticeCodeTable pctBusClass ON pctBusClass.Code = pri.InsurerBusinessClass
	AND pctBusClass.ReferenceType = 'BUSINESSCLASS'
LEFT JOIN dbo.PracticeCodeTable pctClaimReceiver ON SUBSTRING(pctClaimReceiver.Code, 1, 1) = pri.InsurerTFormat
	AND pctClaimReceiver.ReferenceType = 'TRANSMITTYPE'
LEFT JOIN dbo.PracticeCodeTable pctPayType ON SUBSTRING(pctPayType.Code, 1, 2) = pri.InsurerPayType
	AND pctPayType.ReferenceType = 'INSURERPTYPE'
LEFT JOIN dbo.PracticeCodeTable pctPlanType ON pctPlanType.Code = pri.InsurerPlanType
	AND pctPlanType.ReferenceType = 'PLANTYPE'
LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = pri.StateJurisdiction
	AND st.CountryId = 225
WHERE InsurerName NOT IN ('ZZZ ON ACCOUNT INSURANCE', 'ZZZ No insurance')

GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.InsurancePolicies'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE i
SET i.InsurerId = ins.Id
FROM model.InsurancePolicies i
JOIN model.Insurers ins ON ins.LegacyPracticeInsurersId = i.InsurerId
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [InsurerId] in table 'InsurancePolicies'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InsurerInsurancePolicy'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[InsurancePolicies] DROP CONSTRAINT FK_InsurerInsurancePolicy

IF OBJECTPROPERTY(OBJECT_ID('[model].[Insurers]'), 'IsUserTable') = 1
ALTER TABLE [model].[InsurancePolicies]
ADD CONSTRAINT [FK_InsurerInsurancePolicy]
    FOREIGN KEY ([InsurerId])
    REFERENCES [model].[Insurers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerInsurancePolicy'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InsurerInsurancePolicy')
	DROP INDEX IX_FK_InsurerInsurancePolicy ON [model].[InsurancePolicies]
GO
CREATE INDEX [IX_FK_InsurerInsurancePolicy]
ON [model].[InsurancePolicies]
    ([InsurerId]);
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.FinancialInformations'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE f
SET f.InsurerId = ins.Id
FROM model.FinancialInformations f
JOIN model.Insurers ins ON ins.LegacyPracticeInsurersId = f.InsurerId
GO

SELECT
*
INTO dbo.ArchivedFinancialInformationsInvalidInsurerId
FROM model.FinancialInformations 
WHERE InsurerId IS NOT NULL
	AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DELETE FROM model.FinancialInformations WHERE InsurerId IS NOT NULL AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [InsurerId] in table 'FinancialInformations'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InsurerFinancialInformation'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[FinancialInformations] DROP CONSTRAINT FK_InsurerFinancialInformation

IF OBJECTPROPERTY(OBJECT_ID('[model].[Insurers]'), 'IsUserTable') = 1
ALTER TABLE [model].[FinancialInformations]
ADD CONSTRAINT [FK_InsurerFinancialInformation]
    FOREIGN KEY ([InsurerId])
    REFERENCES [model].[Insurers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerFinancialInformation'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InsurerFinancialInformation')
	DROP INDEX IX_FK_InsurerFinancialInformation ON [model].[FinancialInformations]
GO
CREATE INDEX [IX_FK_InsurerFinancialInformation]
ON [model].[FinancialInformations]
    ([InsurerId]);
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.Adjustments'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE a
SET a.InsurerId = ins.Id
FROM model.Adjustments a
JOIN model.Insurers ins ON ins.LegacyPracticeInsurersId = a.InsurerId
GO

SELECT
*
INTO dbo.ArchivedAdjustmentsInvalidInsurerId
FROM model.Adjustments 
WHERE InsurerId IS NOT NULL
	AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DELETE FROM model.Adjustments WHERE InsurerId IS NOT NULL AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [InsurerId] in table 'Adjustments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InsurerAdjustment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Adjustments] DROP CONSTRAINT FK_InsurerAdjustment

IF OBJECTPROPERTY(OBJECT_ID('[model].[Insurers]'), 'IsUserTable') = 1
ALTER TABLE [model].[Adjustments]
ADD CONSTRAINT [FK_InsurerAdjustment]
    FOREIGN KEY ([InsurerId])
    REFERENCES [model].[Insurers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerAdjustment'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InsurerAdjustment')
	DROP INDEX IX_FK_InsurerAdjustment ON [model].[Adjustments]
GO
CREATE INDEX [IX_FK_InsurerAdjustment]
ON [model].[Adjustments]
    ([InsurerId]);
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.DoctorInsurerAssignments'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE d
SET d.InsurerId = ins.Id
FROM model.DoctorInsurerAssignments d
JOIN model.Insurers ins ON ins.LegacyPracticeInsurersId = d.InsurerId
GO

SELECT
*
INTO dbo.ArchivedDoctorInsurerAssignmentsInsurerId
FROM model.DoctorInsurerAssignments 
WHERE InsurerId IS NOT NULL
	AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DELETE FROM model.DoctorInsurerAssignments WHERE InsurerId IS NOT NULL AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [InsurerId] in table 'DoctorInsurerAssignments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InsurerDoctorInsurerAssignment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[DoctorInsurerAssignments] DROP CONSTRAINT FK_InsurerDoctorInsurerAssignment

IF OBJECTPROPERTY(OBJECT_ID('[model].[Insurers]'), 'IsUserTable') = 1
ALTER TABLE [model].[DoctorInsurerAssignments]
ADD CONSTRAINT [FK_InsurerDoctorInsurerAssignment]
    FOREIGN KEY ([InsurerId])
    REFERENCES [model].[Insurers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerDoctorInsurerAssignment'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InsurerDoctorInsurerAssignment')
	DROP INDEX IX_FK_InsurerDoctorInsurerAssignment ON [model].[DoctorInsurerAssignments]
GO
CREATE INDEX [IX_FK_InsurerDoctorInsurerAssignment]
ON [model].[DoctorInsurerAssignments]
    ([InsurerId]);
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.InsurerAddresses'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE ia
SET ia.InsurerId = ins.Id
FROM model.InsurerAddresses ia
JOIN model.Insurers ins ON ins.LegacyPracticeInsurersId = ia.InsurerId
GO

SELECT
*
INTO dbo.ArchivedInsurerAddressesInsurerId
FROM model.InsurerAddresses 
WHERE InsurerId IS NOT NULL
	AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DELETE FROM model.InsurerAddresses WHERE InsurerId IS NOT NULL AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [InsurerId] in table 'InsurerAddresses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InsurerInsurerAddress'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[InsurerAddresses] DROP CONSTRAINT FK_InsurerInsurerAddress

IF OBJECTPROPERTY(OBJECT_ID('[model].[Insurers]'), 'IsUserTable') = 1
ALTER TABLE [model].[InsurerAddresses]
ADD CONSTRAINT [FK_InsurerInsurerAddress]
    FOREIGN KEY ([InsurerId])
    REFERENCES [model].[Insurers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerInsurerAddress'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InsurerInsurerAddress')
	DROP INDEX IX_FK_InsurerInsurerAddress ON [model].[InsurerAddresses]
GO
CREATE INDEX [IX_FK_InsurerInsurerAddress]
ON [model].[InsurerAddresses]
    ([InsurerId]);
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.InsurerPhoneNumbers'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE ip
SET ip.InsurerId = ins.Id
FROM model.InsurerPhoneNumbers ip
JOIN model.Insurers ins ON ins.LegacyPracticeInsurersId = ip.InsurerId
GO

SELECT
*
INTO dbo.ArchivedInsurerPhoneNumbersInsurerId
FROM model.InsurerPhoneNumbers 
WHERE InsurerId IS NOT NULL
	AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DELETE FROM model.InsurerPhoneNumbers WHERE InsurerId IS NOT NULL AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [InsurerId] in table 'InsurerPhoneNumbers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InsurerInsurerPhoneNumber'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[InsurerPhoneNumbers] DROP CONSTRAINT FK_InsurerInsurerPhoneNumber

IF OBJECTPROPERTY(OBJECT_ID('[model].[Insurers]'), 'IsUserTable') = 1
ALTER TABLE [model].[InsurerPhoneNumbers]
ADD CONSTRAINT [FK_InsurerInsurerPhoneNumber]
    FOREIGN KEY ([InsurerId])
    REFERENCES [model].[Insurers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerInsurerPhoneNumber'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InsurerInsurerPhoneNumber')
	DROP INDEX IX_FK_InsurerInsurerPhoneNumber ON [model].[InsurerPhoneNumbers]
GO

CREATE INDEX [IX_FK_InsurerInsurerPhoneNumber]
ON [model].[InsurerPhoneNumbers]
    ([InsurerId]);
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.InsurerClaimForms'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE icf
SET icf.InsurerId = ins.Id
FROM model.InsurerClaimForms icf
JOIN model.Insurers ins ON ins.LegacyPracticeInsurersId = icf.InsurerId
GO

SELECT
*
INTO dbo.ArchivedInsurerClaimFormsInsurerId
FROM model.InsurerClaimForms 
WHERE InsurerId IS NOT NULL
	AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DELETE FROM model.InsurerClaimForms WHERE InsurerId IS NOT NULL AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [InsurerId] in table 'InsurerClaimForms'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InsurerClaimFormInsurer'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[InsurerClaimForms] DROP CONSTRAINT FK_InsurerClaimFormInsurer

IF OBJECTPROPERTY(OBJECT_ID('[model].[Insurers]'), 'IsUserTable') = 1
ALTER TABLE [model].[InsurerClaimForms]
ADD CONSTRAINT [FK_InsurerClaimFormInsurer]
    FOREIGN KEY ([InsurerId])
    REFERENCES [model].[Insurers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerClaimFormInsurer'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InsurerClaimFormInsurer')
	DROP INDEX IX_FK_InsurerClaimFormInsurer ON [model].[InsurerClaimForms]
GO
CREATE INDEX [IX_FK_InsurerClaimFormInsurer]
ON [model].[InsurerClaimForms]
    ([InsurerId]);
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.FinancialBatches'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE fb
SET fb.InsurerId = ins.Id
FROM model.FinancialBatches fb
JOIN model.Insurers ins ON ins.LegacyPracticeInsurersId = fb.InsurerId
GO

SELECT
*
INTO dbo.ArchivedFinancialBatchesInsurerId
FROM model.FinancialBatches 
WHERE InsurerId IS NOT NULL
	AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DELETE FROM model.Adjustments
WHERE FinancialBatchId IN (SELECT Id FROM model.FinancialBatches WHERE InsurerId IS NOT NULL AND InsurerId NOT IN (SELECT Id FROM model.Insurers))
 
DELETE FROM model.FinancialInformations
WHERE FinancialBatchId IN (SELECT Id FROM model.FinancialBatches WHERE InsurerId IS NOT NULL AND InsurerId NOT IN (SELECT Id FROM model.Insurers))

DELETE FROM model.FinancialBatches WHERE InsurerId IS NOT NULL AND InsurerId NOT IN (SELECT Id FROM model.Insurers)
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [InsurerId] in table 'FinancialBatches'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_FinancialBatchInsurer'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[FinancialBatches] DROP CONSTRAINT FK_FinancialBatchInsurer

IF OBJECTPROPERTY(OBJECT_ID('[model].[Insurers]'), 'IsUserTable') = 1
ALTER TABLE [model].[FinancialBatches]
ADD CONSTRAINT [FK_FinancialBatchInsurer]
    FOREIGN KEY ([InsurerId])
    REFERENCES [model].[Insurers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_FinancialBatchInsurer'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_FinancialBatchInsurer')
	DROP INDEX IX_FK_FinancialBatchInsurer ON [model].[FinancialBatches]
GO
CREATE INDEX [IX_FK_FinancialBatchInsurer]
ON [model].[FinancialBatches]
    ([InsurerId]);
GO

UPDATE pfs
SET pfs.PlanId = i.Id
FROM dbo.PracticeFeeSchedules pfs
JOIN model.Insurers i ON i.LegacyPracticeInsurersId = pfs.PlanId
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'OnInsurerAddressesInsertOrUpdate' AND parent_id = OBJECT_ID('model.InsurerAddresses'))
	DROP TRIGGER [model].[OnInsurerAddressesInsertOrUpdate]
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'OnInsurerPhoneNumbersInsertOrUpdate' AND parent_id = OBJECT_ID('model.InsurerPhoneNumbers'))
	DROP TRIGGER [model].[OnInsurerPhoneNumbersInsertOrUpdate]
GO


