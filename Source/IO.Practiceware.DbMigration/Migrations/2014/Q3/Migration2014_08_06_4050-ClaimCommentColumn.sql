﻿IF OBJECT_ID('model.BillingServices','U') IS NOT NULL
BEGIN
	IF NOT EXISTS (
	SELECT 
	name 
	FROM sys.columns 
	WHERE object_id = OBJECT_ID('model.BillingServices','U') 
		AND name = 'ClaimComment'
	)
	BEGIN 
		ALTER TABLE [model].[BillingServices]
		ADD [ClaimComment] NVARCHAR(MAX) NULL
	END
END
GO

IF OBJECT_ID('model.Invoices','U') IS NOT NULL
BEGIN
	IF NOT EXISTS (
	SELECT 
	name 
	FROM sys.columns 
	WHERE object_id = OBJECT_ID('model.Invoices','U') 
		AND name = 'ClaimComment'
	)
	BEGIN 
		ALTER TABLE [model].[Invoices]
		ADD [ClaimComment] NVARCHAR(MAX) NULL
	END
END
GO