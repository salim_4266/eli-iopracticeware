﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ServiceTransactionsUpdate' AND parent_id = OBJECT_ID('dbo.ServiceTransactions','V'))
	DROP TRIGGER ServiceTransactionsUpdate 
GO

CREATE TRIGGER ServiceTransactionsUpdate ON dbo.ServiceTransactions
INSTEAD OF UPDATE
AS
BEGIN

IF EXISTS (
SELECT * FROM inserted
EXCEPT 
SELECT * FROM deleted
)
	BEGIN
		SET NOCOUNT ON
		UPDATE bst
		SET bst.BillingServiceId = i.ServiceId
		,bst.AmountSent = i.Amount
		,bst.BillingServiceTransactionStatusId = CASE 
			WHEN ptj.TransactionStatus = 'P'
				AND i.TransportAction IN ('B', 'P')
				THEN 1
			WHEN ptj.TransactionStatus = 'P'
				AND i.TransportAction = 'V'
				THEN 3
			WHEN ptj.TransactionStatus = 'S'
				AND i.TransportAction = 'P'
				THEN 2
			WHEN ptj.TransactionStatus = 'S'
				AND i.TransportAction = 'V'
				THEN 3
			WHEN ptj.TransactionStatus = 'S'
				AND i.TransportAction = 'X'
				THEN 4
			WHEN ptj.TransactionStatus = 'Z'
				AND i.TransportAction = '1'
				AND d.TransportAction = '0'
				THEN CASE WHEN bst.BillingServiceTransactionStatusId = 2 THEN 6 ELSE BillingServiceTransactionStatusId END
			WHEN ptj.TransactionStatus = 'Z'
				AND i.TransportAction = '1'
				THEN 6
			WHEN ptj.TransactionStatus = 'Z'
				AND i.TransportAction = '2'
				THEN 7
			ELSE 5
		END
		,bst.[DateTime] = CONVERT(DATETIME,i.ModTime,112)
		,bst.ExternalSystemMessageId = i.ExternalSystemMessageId
		FROM model.BillingServiceTransactions bst
		JOIN inserted i ON i.Id = bst.Id
		LEFT JOIN deleted d ON d.Id = bst.Id
		JOIN PracticeTransactionJournal ptj ON i.TransactionId = ptj.TransactionId

		UPDATE ptj
		SET ptj.TransactionDate = i.TransactionDate
		FROM PracticeTransactionJournal ptj
		JOIN inserted i ON i.TransactionId = ptj.TransactionId
	END
END
GO