﻿IF OBJECT_ID ( 'dbo.GetLinkedBills', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetLinkedBills;
GO

CREATE PROCEDURE dbo.GetLinkedBills
	@ItemId INT
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @Invoice NVARCHAR(1000)
SELECT 
TOP 1 @Invoice = i.LegacyPatientReceivablesInvoice
FROM model.BillingServices bs 
JOIN model.Invoices i ON i.Id = bs.InvoiceId
WHERE bs.Id = @ItemId

SELECT DISTINCT 
ptj.TransactionId
,ptj.TransactionDate
,ptj.TransactionStatus
,ptj.TransactionRef
,ptj.transactionaction
,st.Amount
,st.TransportAction
,CASE ptj.TransactionRef
	WHEN 'I'
		THEN ins.Name
	WHEN 'P'
		THEN 'PATIENT'
	WHEN 'G'
		THEN 'MONTHLY STMT'
	ELSE ''
END AS InsurerName
FROM PatientReceivables pr
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
INNER JOIN model.Invoices i ON i.LegacyPatientReceivablesInvoice = @Invoice
LEFT JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
LEFT OUTER JOIN model.Insuers ins ON ins.Id = pr.InsurerId
INNER JOIN ServiceTransactions st ON st.TransactionId = ptj.TransactionId
	AND st.ServiceId = bs.Id
WHERE ptj.TransactionType = 'R'
AND bs.Id = @ItemId
ORDER BY ptj.TransactionDate DESC
,ptj.TransactionId DESC

GO