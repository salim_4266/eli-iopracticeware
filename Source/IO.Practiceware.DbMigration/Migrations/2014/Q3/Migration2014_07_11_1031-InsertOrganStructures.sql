IF (
		EXISTS (
			SELECT *
			FROM INFORMATION_SCHEMA.TABLES
			WHERE TABLE_SCHEMA = 'Model'
				AND TABLE_NAME = 'OrganStructures'
			)
		)
BEGIN
	DECLARE @OrganId INT
	DECLARE @FrontOfTheEye INT
	DECLARE @BackOfTheEye INT

	--Just in Case if 'Eye' rows didn't exists.
	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Eye'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Eye'
			,2
			,1
			)

	--Create OrganStructureGroups
	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[OrganStructureGroups]
			WHERE NAME = 'FrontOfTheEye'
			)
		INSERT INTO [Model].[OrganStructureGroups] (NAME)
		VALUES ('FrontOfTheEye')

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[OrganStructureGroups]
			WHERE NAME = 'BackOfTheEye'
			)
		INSERT INTO [Model].[OrganStructureGroups] (NAME)
		VALUES ('BackOfTheEye')
	
	SELECT TOP 1 @FrontOfTheEye=Id FROM model.OrganStructureGroups WHERE Name='FrontOfTheEye'
	SELECT TOP 1 @BackOfTheEye=Id FROM model.OrganStructureGroups WHERE Name='BackOfTheEye'
	SELECT TOP 1 @OrganId = id
		FROM model.Organs
		WHERE NAME = 'eye'

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'External'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'External'
			,@OrganId
			,NULL
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Pupils'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Pupils'
			,@OrganId
			,@FrontOfTheEye
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Extraocular Motility'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Extraocular Motility'
			,@OrganId
			,NULL
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Visual Field'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Visual Field'
			,@OrganId
			,NULL
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Lids/Lacrimal'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Lids/Lacrimal'
			,@OrganId
			,@FrontOfTheEye
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Conjunctiva/Sclera'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Conjunctiva/Sclera'
			,@OrganId
			,@FrontOfTheEye
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Cornea'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Cornea'
			,@OrganId
			,@FrontOfTheEye
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Anterior Chamber'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Anterior Chamber'
			,@OrganId
			,@FrontOfTheEye
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Iris'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Iris'
			,@OrganId
			,@FrontOfTheEye
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Lens'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Lens'
			,@OrganId
			,@FrontOfTheEye
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Vitreous'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Vitreous'
			,@OrganId
			,@BackOfTheEye
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Optic Nerve'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Optic Nerve'
			,@OrganId
			,@BackOfTheEye
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Blood Vessels'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Blood Vessels'
			,@OrganId
			,@BackOfTheEye
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Macula'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Macula'
			,@OrganId
			,@BackOfTheEye
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Retina/Choroid'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Retina/Choroid'
			,@OrganId
			,@BackOfTheEye
			,1
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Peripheral Retina'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Peripheral Retina'
			,@OrganId
			,@BackOfTheEye
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Systemic'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Systemic'
			,@OrganId
			,NULL
			,0
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [model].[OrganStructures]
			WHERE NAME = 'Visual Defects'
			)
	BEGIN
		INSERT INTO [model].[OrganStructures] (
			NAME
			,OrganId
			,OrganStructureGroupId
			,IsDeactivated
			)
		VALUES (
			'Visual Defects'
			,@OrganId
			,NULL
			,0
			)
	END
END
		--11631 : Import the data into temp table call OrganStructuresTemp$ and run the below query to generate the above query
		--Select Case Name when 'Retina/Choroid'
		--then
		--	COALESCE(('IF NOT EXISTS(SELECT Name FROM [model].[OrganStructures] WHERE Name ='''+ Name +''')
		--	INSERT INTO [model].[OrganStructures](Name, OrganId, OrganStructureGroupId, IsDeactivated) VALUES('''+ Name +''',' + CONVERT(nvarchar(max),(select id from model.Organs where Name='eye')) + ',' + CONVERT(nvarchar(max),OrganGroupId) + ',1)'),
		--	('IF NOT EXISTS(SELECT Name FROM [model].[OrganStructures] WHERE Name ='''+ Name +''')
		--	INSERT INTO [model].[OrganStructures](Name, OrganId, OrganStructureGroupId, IsDeactivated) VALUES('''+ Name +''',' + CONVERT(nvarchar(max),(select id from model.Organs where Name='eye')) + ', null ,1)'))
		--else
		--	COALESCE(('IF NOT EXISTS(SELECT Name FROM [model].[OrganStructures] WHERE Name ='''+ Name +''')
		--	INSERT INTO [model].[OrganStructures](Name, OrganId, OrganStructureGroupId, IsDeactivated) VALUES('''+ Name +''',' + CONVERT(nvarchar(max),(select id from model.Organs where Name='eye')) + ',' + CONVERT(nvarchar(max),OrganGroupId) + ',0)'),
		--	('IF NOT EXISTS(SELECT Name FROM [model].[OrganStructures] WHERE Name ='''+ Name +''')
		--	INSERT INTO [model].[OrganStructures](Name, OrganId, OrganStructureGroupId, IsDeactivated) VALUES('''+ Name +''',' + CONVERT(nvarchar(max),(select id from model.Organs where Name='eye')) + ',null,0)'))
		--end
		--from  OrganStructuresTemp$