﻿/* For Bug #13028 */
IF EXISTS (
		SELECT *
		FROM sys.triggers
		WHERE object_id = OBJECT_ID(N'[dbo].[AuditEntryChangesInsertOnlyTrigger]')
		)
BEGIN
	DROP TRIGGER AuditEntryChangesInsertOnlyTrigger
END
GO

IF OBJECT_ID(N'[dbo].[AuditEntryChangesTemp]', 'U') IS NULL
BEGIN
	--If Script file is running for the first time.Primary and foreign key will not be according to the hand convention.So,find them and remove first.
	DECLARE @ConstraintName NVARCHAR(500)

	SELECT TOP 1 @ConstraintName = Col.CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab
		,INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col
	WHERE Col.Constraint_Name = Tab.Constraint_Name
		AND Col.Table_Name = Tab.Table_Name
		AND Constraint_Type = 'FOREIGN KEY'
		AND Col.Table_Name = 'AuditEntryChanges'

	IF @ConstraintName IS NOT NULL
	BEGIN
		EXEC (
				'ALTER TABLE dbo.AuditEntryChanges
DROP CONSTRAINT ' + @ConstraintName + ''
				)
	END

	SELECT TOP 1 @ConstraintName = Col.CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab
		,INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col
	WHERE Col.Constraint_Name = Tab.Constraint_Name
		AND Col.Table_Name = Tab.Table_Name
		AND Constraint_Type = 'PRIMARY KEY'
		AND Col.Table_Name = 'AuditEntryChanges'

	IF @ConstraintName IS NOT NULL
	BEGIN
		EXEC (
				'ALTER TABLE dbo.AuditEntryChanges
	DROP CONSTRAINT ' + @ConstraintName + ''
				)
	END

	CREATE TABLE dbo.AuditEntryChangesTemp (
		AuditEntryId UNIQUEIDENTIFIER NOT NULL
		,ColumnName NVARCHAR(400) NOT NULL
		,OldValue NVARCHAR(MAX) NULL
		,NewValue NVARCHAR(MAX) NULL
		,OldValueNumeric AS (
			CASE 
				WHEN len([OldValue]) >= (1)
					AND len([OldValue]) <= (19)
					AND NOT CONVERT([varchar](19), [OldValue], 0) LIKE '%[^0-9]%'
					AND right(replicate('0', (19)) + replace(replace([OldValue], '[', ''), ']', ''), (19)) < '9223372036854775807'
					THEN CONVERT([bigint], CONVERT([varchar](19), [OldValue], 0), 0)
				END
			) PERSISTED
		,NewValueNumeric AS (
			CASE 
				WHEN len([NewValue]) >= (1)
					AND len([NewValue]) <= (19)
					AND NOT CONVERT([varchar](19), [NewValue], 0) LIKE '%[^0-9]%'
					AND right(replicate('0', (19)) + replace(replace([NewValue], '[', ''), ']', ''), (19)) < '9223372036854775807'
					THEN CONVERT([bigint], CONVERT([varchar](19), [NewValue], 0), 0)
				END
			) PERSISTED
		) ON AuditTables TEXTIMAGE_ON AuditTables

	IF EXISTS (
			SELECT *
			FROM sys.columns sc
			INNER JOIN sys.tables st ON sc.object_id = st.object_id
			WHERE sc.NAME = 'msrepl_tran_version'
				AND st.NAME = 'AuditEntryChanges'
			)
	BEGIN
		EXEC ('ALTER TABLE dbo.AuditEntryChangesTemp ADD msrepl_tran_version [uniqueidentifier] NOT NULL DEFAULT (newid())')

		EXEC (
				'INSERT INTO dbo.AuditEntryChangesTemp (AuditEntryId, ColumnName, OldValue, NewValue, msrepl_tran_version)
			SELECT aec.AuditEntryId, aec.ColumnName, aec.OldValue, aec.NewValue, aec.msrepl_tran_version
			FROM dbo.AuditEntryChanges aec
			INNER JOIN AuditEntries ae on ae.Id=aec.AuditEntryId AND ae.ObjectName IN(''model.Appointments'',''model.Encounters'',''model.Adjustments'')
			'
				)
	END
	ELSE
	BEGIN
		EXEC (
				'INSERT INTO dbo.AuditEntryChangesTemp (AuditEntryId, ColumnName, OldValue, NewValue)
			SELECT aec.AuditEntryId, aec.ColumnName, aec.OldValue, aec.NewValue
			FROM dbo.AuditEntryChanges aec
			INNER JOIN AuditEntries ae on ae.Id=aec.AuditEntryId AND ae.ObjectName IN(''model.Appointments'',''model.Encounters'',''model.Adjustments'')
			'
				)
	END

	EXEC ('DELETE FROM dbo.AuditEntryChanges WHERE AuditEntryId IN (SELECT AuditEntryId FROM dbo.AuditEntryChangesTemp)')

	--Just in case if already exists.(While Running the same script Second time)
	DECLARE @TableName NVARCHAR(MAX);

	IF EXISTS (
			SELECT *
			FROM sys.objects
			WHERE NAME = 'PK_AuditEntryChanges'
			)
	BEGIN
		SELECT TOP 1 @TableName = OBJECT_NAME(parent_object_id)
		FROM sys.objects
		WHERE NAME = 'PK_AuditEntryChanges';

		EXEC ('ALTER TABLE dbo.' + @TableName + ' DROP CONSTRAINT PK_AuditEntryChanges');
	END

	--Create Primary Key.
	EXEC (
			'ALTER TABLE dbo.AuditEntryChangesTemp ADD CONSTRAINT PK_AuditEntryChanges  PRIMARY KEY CLUSTERED 
	(
	AuditEntryId,
	ColumnName
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON AuditTables'
			)

	IF EXISTS (
			SELECT object_id
			FROM sys.indexes
			WHERE NAME = 'IX_AuditEntryChanges_ColumnName'
			)
	BEGIN
		SELECT TOP 1 @TableName = OBJECT_NAME(object_id)
		FROM sys.indexes
		WHERE NAME = 'IX_AuditEntryChanges_ColumnName';

		EXEC ('DROP INDEX IX_AuditEntryChanges_ColumnName ON dbo.' + @TableName + '');
	END

	EXEC (
			'
CREATE NONCLUSTERED INDEX IX_AuditEntryChanges_ColumnName ON dbo.AuditEntryChangesTemp ([ColumnName] ASC) INCLUDE (
	[OldValueNumeric]
	,[NewValueNumeric]
	)
	WITH (
			PAD_INDEX = OFF
			,FILLFACTOR = 90
			,STATISTICS_NORECOMPUTE = OFF
			,IGNORE_DUP_KEY = OFF
			,ALLOW_ROW_LOCKS = ON
			,ALLOW_PAGE_LOCKS = ON
			) ON AuditTables'
			)

	--Just in case if already exists.
	IF EXISTS (
			SELECT *
			FROM sys.objects
			WHERE NAME = 'FK_AuditEntryChanges_AuditEntries'
			)
	BEGIN
		SELECT TOP 1 @TableName = OBJECT_NAME(parent_object_id)
		FROM sys.objects
		WHERE NAME = 'FK_AuditEntryChanges_AuditEntries';

		EXEC ('ALTER TABLE dbo.' + @TableName + ' DROP CONSTRAINT FK_AuditEntryChanges_AuditEntries');
	END

	--Create Foreign Key.
	EXEC ('ALTER TABLE dbo.AuditEntryChangesTemp ADD CONSTRAINT FK_AuditEntryChanges_AuditEntries  FOREIGN KEY (AuditEntryId) REFERENCES dbo.AuditEntries (Id) ')

	EXECUTE sp_rename N'dbo.AuditEntryChanges'
		,N'AuditEntryChanges_Temp'
		,'OBJECT'

	EXECUTE sp_rename N'dbo.AuditEntryChangesTemp'
		,N'AuditEntryChanges'
		,'OBJECT'

	EXECUTE sp_rename N'dbo.AuditEntryChanges_Temp'
		,N'AuditEntryChangesTemp'
		,'OBJECT'
END

IF NOT EXISTS (
		SELECT *
		FROM sys.triggers
		WHERE NAME = 'dbo.AuditEntryChangesInsertOnlyTrigger'
		)
	EXEC dbo.sp_executesql @statement = N'

CREATE TRIGGER [dbo].[AuditEntryChangesInsertOnlyTrigger] ON [dbo].[AuditEntryChanges]
FOR UPDATE, DELETE
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (dbo.GetNoCheck() = 0)	   
BEGIN
	RAISERROR( ''AuditEntryChanges cannot be altered.'', 16, 1 )
	ROLLBACK TRANSACTION
END
'
GO

