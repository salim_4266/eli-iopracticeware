﻿--Cleanup PatientReceivableServices.PlaceOfService, match to invoices.

IF NOT EXISTS (SELECT * FROM sys.objects WHERE name = 'ServiceLocationCodes' AND schema_id = SCHEMA_ID('model')) 
BEGIN
EXEC('
CREATE VIEW [model].[ServiceLocationCodes]
AS
SELECT Id,
	SUBSTRING(Code, 4, LEN(code)) AS [Name],
	SUBSTRING(Code, 1, 2) AS PlaceOfServiceCode
FROM dbo.PracticeCodeTable
WHERE ReferenceType = ''PLACEOFSERVICE''
')
END

GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'dbo.PatientReceivableServices'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE p
SET p.PlaceOfService = s.PlaceOfServiceCode
FROM dbo.PatientReceivableServices p
JOIN model.Invoices i ON i.LegacyPatientReceivablesInvoice = p.Invoice
JOIN model.ServiceLocationCodes s ON s.Id = i.ServiceLocationCodeId
WHERE p.[Status] = 'A'
	AND (p.PlaceOfService <> s.PlaceOfServiceCode OR p.PlaceOfService = '')
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

IF OBJECT_ID('model.BillingServices') IS NOT NULL
	EXEC dbo.DropObject 'model.BillingServices'
GO

-- 'BillingServices' does not exist, creating...
CREATE TABLE [model].[BillingServices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Unit] decimal(18,2)  NULL,
    [UnitCharge] decimal(18,2)  NULL,
    [OrdinalId] int  NULL,
    [EncounterServiceId] int  NOT NULL,
    [InvoiceId] int  NULL,
    [ProcedureCode835Description] nvarchar(max)  NULL,
    [OrderingExternalProviderId] int  NULL,
    [UnitAllowableExpense] decimal(18,2)  NULL,
	[LegacyOrderingProviderId] INT NULL,
	[LegacyPatientReceivableServicesItemId] INT NULL,
	[LegacyPatientReceivableServicesLinkedDiag] NVARCHAR(4) NULL
);

-- Creating primary key on [Id] in table 'BillingServices'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.BillingServices', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[BillingServices] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.BillingServices', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[BillingServices]
ADD CONSTRAINT [PK_BillingServices]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

DECLARE @ProviderBillingOrganizationMax INT
DECLARE @ProviderBillingOrganizationServiceLocationMax INT
DECLARE @ServiceLocationMax INT
DECLARE @BillingOrganizationMax INT

SET @ProviderBillingOrganizationMax = 110000000
SET @ProviderBillingOrganizationServiceLocationMax = 110000000
SET @ServiceLocationMax = 110000000
SET @BillingOrganizationMax = 110000000

DECLARE @Providers TABLE (Id BIGINT, UserId INT)

INSERT INTO @Providers (Id, UserId)
SELECT 
CASE 
	WHEN re.ResourceType = 'R' ---internal facility
		THEN model.GetBigPairId(pnBillOrg.PracticeId, model.GetBigPairId(pnBillOrg.PracticeId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
	WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN model.GetBigPairId(model.GetPairId(2, pnServLoc.PracticeId, @ServiceLocationMax), model.GetBigPairId(re.ResourceId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
	ELSE model.GetBigPairId(pnServLoc.PracticeId, model.GetBigPairId(pnBillOrg.PracticeId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
END AS Id
,CASE
	WHEN re.ResourceType = 'R'
		THEN NULL
	ELSE re.ResourceId 
END AS UserId
FROM dbo.Resources re
INNER JOIN dbo.PracticeName AS pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
	AND pnBillOrg.PracticeType = 'P'
INNER JOIN dbo.PracticeName AS pnServLoc ON pnServLoc.PracticeType = 'P'
INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
		AND pa.ResourceType = 'R'
WHERE (re.ResourceType in ('D', 'Q', 'Z','Y')
	OR (re.ResourceType = 'R'
		AND re.ServiceCode = '02'
		AND re.Billable = 'Y'
		AND pic.FieldValue = 'T'
		AND pnBillOrg.LocationReference <> '')
		)
GROUP BY
CASE 
	WHEN re.ResourceType = 'R' ---internal facility
		THEN model.GetBigPairId(pnBillOrg.PracticeId, model.GetBigPairId(pnBillOrg.PracticeId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
	WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN model.GetBigPairId(model.GetPairId(2, pnServLoc.PracticeId, @ServiceLocationMax), model.GetBigPairId(re.ResourceId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
	ELSE model.GetBigPairId(pnServLoc.PracticeId, model.GetBigPairId(pnBillOrg.PracticeId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
END
,CASE
	WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN model.GetPairId(1, re.ResourceId, @BillingOrganizationMax) 
	ELSE pnBillOrg.PracticeId
END
,CASE re.Billable
	WHEN 'Y'
		THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END
,CASE re.GoDirect
	WHEN 'Y'
		THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END 
,CASE 
	WHEN re.ResourceType = 'R'
		THEN pnBillOrg.PracticeId
	ELSE pnServLoc.PracticeId 
END 
,CASE
	WHEN re.ResourceType = 'R'
		THEN NULL
	ELSE re.ResourceId 
END

UNION ALL

----Human Providers where ServiceLocation is from Resources table including override
SELECT
CASE 
	WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN model.GetBigPairId(model.GetPairId(3, reServLoc.ResourceId, @ServiceLocationMax), model.GetBigPairId(re.ResourceId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
	ELSE model.GetBigPairId(model.GetPairId(1, reServLoc.ResourceId, @ServiceLocationMax), model.GetBigPairId(pnBillOrg.PracticeId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
END AS Id
,re.ResourceId AS UserId
FROM dbo.Resources re
INNER JOIN dbo.PracticeName AS pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
	AND pnBillOrg.PracticeType = 'P'
INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = 'R'
	AND reServLoc.ServiceCode = '02'
LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
	AND pa.ResourceType = 'R'
WHERE re.ResourceType in ('D', 'Q', 'Z','Y')
GROUP BY
CASE 
	WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN model.GetBigPairId(model.GetPairId(3, reServLoc.ResourceId, @ServiceLocationMax), model.GetBigPairId(re.ResourceId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
	ELSE model.GetBigPairId(model.GetPairId(1, reServLoc.ResourceId, @ServiceLocationMax), model.GetBigPairId(pnBillOrg.PracticeId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
END
,CASE 
	WHEN  pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
		THEN model.GetPairId(1, re.ResourceId, @BillingOrganizationMax)
	ELSE pnBillOrg.PracticeId 
END
,CASE re.GoDirect
	WHEN 'Y'
		THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END
,model.GetPairId(1, reServLoc.ResourceId, @ServiceLocationMax)
,re.ResourceId
,re.Billable
,pa.ResourceId
,reServLoc.ResourceId
,pnBillOrg.PracticeId

INSERT INTO model.BillingServices (InvoiceId, EncounterServiceId, OrdinalId, Unit,
	UnitAllowableExpense, UnitCharge, ProcedureCode835Description, OrderingExternalProviderId,
	LegacyOrderingProviderId, LegacyPatientReceivableServicesItemId, LegacyPatientReceivableServicesLinkedDiag)
SELECT 
inv.Id AS InvoiceId
,es.Id AS EncounterServiceId
,CASE 
	WHEN ItemOrder > 0
		THEN ItemOrder
	ELSE 1
END AS OrdinalId
,prs.QuantityDecimal AS Unit
,prs.FeeChargeDecimal AS UnitAllowableExpense
,prs.ChargeDecimal AS UnitCharge
,CONVERT(NVARCHAR(MAX), NULL) AS ProcedureCode835Description
,NULL AS OrderingExternalProviderId
,CASE prs.OrderDoc
	WHEN 'T'
		THEN p.UserId
END AS LegacyOrderingProviderId
,prs.ItemId AS LegacyPatientReceivableServicesItemId
,prs.LinkedDiag AS  LegacyPatientReceivableServicesLinkedDiag
FROM dbo.PatientReceivableServices prs
INNER JOIN model.Invoices inv on inv.EncounterId = prs.AppointmentId 
	AND inv.InvoiceTypeId <> 2
INNER JOIN @Providers p ON p.Id = inv.BillingProviderId
INNER JOIN model.EncounterServices es ON es.Code = prs.[Service]
WHERE prs.[Status] = 'A'

UNION ALL 

SELECT 
inv.Id AS InvoiceId
,es.Id AS EncounterServiceId
,CASE 
	WHEN ItemOrder > 0
		THEN ItemOrder
	ELSE 1
END AS OrdinalId
,prs.QuantityDecimal AS Unit
,prs.FeeChargeDecimal AS UnitAllowableExpense
,prs.ChargeDecimal AS UnitCharge
,CONVERT(NVARCHAR(MAX), NULL) AS ProcedureCode835Description
,NULL AS OrderingExternalProviderId
,CASE prs.OrderDoc
	WHEN 'T'
		THEN p.UserId	
END AS LegacyOrderingProviderId
,prs.ItemId AS LegacyPatientReceivableServicesItemId
,prs.LinkedDiag AS  LegacyPatientReceivableServicesLinkedDiag
FROM dbo.PatientReceivableServices prs
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = prs.AppointmentId
	AND ap.EncounterId <> ap.AppointmentId
INNER JOIN model.Invoices inv on inv.EncounterId = ap.AppointmentId 
	AND inv.InvoiceTypeId = 2
INNER JOIN @Providers p ON p.Id = inv.BillingProviderId
INNER JOIN model.EncounterServices es ON es.Code = prs.[Service]
WHERE prs.[Status] = 'A'
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.Adjustments'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE a
SET a.BillingServiceId = b.Id
FROM model.Adjustments a
JOIN model.BillingServices b ON b.LegacyPatientReceivableServicesItemId = a.BillingServiceId
GO

UPDATE a
SET a.BillingServiceId = NULL
FROM model.Adjustments a
WHERE a.BillingServiceId NOT IN (SELECT Id FROM model.BillingServices)
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.FinancialInformations'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE f
SET f.BillingServiceId = b.Id
FROM model.FinancialInformations f
JOIN model.BillingServices b ON b.LegacyPatientReceivableServicesItemId = f.BillingServiceId
GO

UPDATE f
SET f.BillingServiceId = NULL
FROM model.FinancialInformations f
WHERE f.BillingServiceId NOT IN (SELECT Id FROM model.BillingServices)
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.BillingServiceTransactions'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

UPDATE bst
SET bst.BillingServiceId = b.Id
FROM model.BillingServiceTransactions bst 
JOIN model.BillingServices b ON b.LegacyPatientReceivableServicesItemId = bst.BillingServiceId
GO

DELETE FROM model.BillingServiceTransactions WHERE BillingServiceId NOT IN (SELECT Id FROM model.BillingServices)
GO

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [EncounterServiceId] in table 'BillingServices'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterServiceBillingService'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingServices] DROP CONSTRAINT FK_EncounterServiceBillingService

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterServices]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingServices]
ADD CONSTRAINT [FK_EncounterServiceBillingService]
    FOREIGN KEY ([EncounterServiceId])
    REFERENCES [model].[EncounterServices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterServiceBillingService'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_EncounterServiceBillingService')
	DROP INDEX IX_FK_EncounterServiceBillingService ON [model].[BillingServices]
GO
CREATE INDEX [IX_FK_EncounterServiceBillingService]
ON [model].[BillingServices]
    ([EncounterServiceId]);
GO

-- Creating foreign key on [BillingServiceId] in table 'Adjustments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BillingServiceAdjustment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Adjustments] DROP CONSTRAINT FK_BillingServiceAdjustment

IF OBJECTPROPERTY(OBJECT_ID('[model].[BillingServices]'), 'IsUserTable') = 1
ALTER TABLE [model].[Adjustments]
ADD CONSTRAINT [FK_BillingServiceAdjustment]
    FOREIGN KEY ([BillingServiceId])
    REFERENCES [model].[BillingServices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BillingServiceAdjustment'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_BillingServiceAdjustment')
	DROP INDEX IX_FK_BillingServiceAdjustment ON [model].[Adjustments]
GO
CREATE INDEX [IX_FK_BillingServiceAdjustment]
ON [model].[Adjustments]
    ([BillingServiceId]);
GO

-- Creating foreign key on [BillingServiceId] in table 'BillingServiceTransactions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BillingServiceBillingServiceTransaction'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingServiceTransactions] DROP CONSTRAINT FK_BillingServiceBillingServiceTransaction

IF OBJECTPROPERTY(OBJECT_ID('[model].[BillingServices]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingServiceTransactions]
ADD CONSTRAINT [FK_BillingServiceBillingServiceTransaction]
    FOREIGN KEY ([BillingServiceId])
    REFERENCES [model].[BillingServices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BillingServiceBillingServiceTransaction'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_BillingServiceBillingServiceTransaction')
	DROP INDEX IX_FK_BillingServiceBillingServiceTransaction ON [model].[BillingServiceTransactions]
GO
CREATE INDEX [IX_FK_BillingServiceBillingServiceTransaction]
ON [model].[BillingServiceTransactions]
    ([BillingServiceId]);
GO

-- Creating foreign key on [BillingServiceId] in table 'FinancialInformations'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BillingServiceFinancialInformation'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[FinancialInformations] DROP CONSTRAINT FK_BillingServiceFinancialInformation

IF OBJECTPROPERTY(OBJECT_ID('[model].[BillingServices]'), 'IsUserTable') = 1
ALTER TABLE [model].[FinancialInformations]
ADD CONSTRAINT [FK_BillingServiceFinancialInformation]
    FOREIGN KEY ([BillingServiceId])
    REFERENCES [model].[BillingServices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BillingServiceFinancialInformation'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_BillingServiceFinancialInformation')
	DROP INDEX IX_FK_BillingServiceFinancialInformation ON [model].[FinancialInformations]
GO
CREATE INDEX [IX_FK_BillingServiceFinancialInformation]
ON [model].[FinancialInformations]
    ([BillingServiceId]);
GO

-- Creating foreign key on [InvoiceId] in table 'BillingServices'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InvoiceBillingService'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingServices] DROP CONSTRAINT FK_InvoiceBillingService

IF OBJECTPROPERTY(OBJECT_ID('[model].[Invoices]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingServices]
ADD CONSTRAINT [FK_InvoiceBillingService]
    FOREIGN KEY ([InvoiceId])
    REFERENCES [model].[Invoices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InvoiceBillingService'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InvoiceBillingService')
	DROP INDEX IX_FK_InvoiceBillingService ON [model].[BillingServices]
GO
CREATE INDEX [IX_FK_InvoiceBillingService]
ON [model].[BillingServices]
    ([InvoiceId]);
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO