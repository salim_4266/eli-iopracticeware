﻿--Bug #13658.
IF EXISTS (
		SELECT *
		FROM model.TemplateDocuments
		WHERE DisplayName = 'CMS-1500'
			AND Content IS NOT NULL
		)
BEGIN
	DECLARE @content AS NVARCHAR(MAX);
	DECLARE @old VARCHAR(max);
	DECLARE @new VARCHAR(max);
	DECLARE @StartIndex INT;

	SET @new = '<input type="text" id="ServiceUnits" value="@(string.IsNullOrEmpty(service.ServiceUnits) ? "" : service.ServiceUnits)" '
	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = 'CMS-1500'
				AND Content IS NOT NULL
			)
	SET @StartIndex = CHARINDEX(LOWER('<input type="text" id="ServiceUnits"'), LOWER(@content), 1)

	IF @StartIndex > 0
	BEGIN
		DECLARE @EndIndex INT;

		SET @EndIndex = CHARINDEX('class', @content, @StartIndex) - 1
		SET @old = SUBSTRING(@content, @StartIndex, @EndIndex - @StartIndex)

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = 'CMS-1500'
			AND Content IS NOT NULL
	END
END
GO