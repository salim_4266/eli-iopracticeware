﻿IF OBJECT_ID(N'model.AdjustmentTypes_Internal','V') IS NOT NULL
	DROP VIEW model.AdjustmentTypes_Internal
GO
	
CREATE VIEW [model].[AdjustmentTypes_Internal]
WITH SCHEMABINDING, VIEW_METADATA
AS
SELECT 
CASE
	WHEN SUBSTRING(REVERSE(code),1,CHARINDEX(' - ',REVERSE(code))) = 'P'
		THEN 1
	WHEN SUBSTRING(REVERSE(code),1,CHARINDEX(' - ',REVERSE(code))) = 'X'
		THEN 2
	WHEN SUBSTRING(REVERSE(code),1,CHARINDEX(' - ',REVERSE(code))) = '8'
		THEN 3
	WHEN SUBSTRING(REVERSE(code),1,CHARINDEX(' - ',REVERSE(code))) = 'R'
		THEN 4
	WHEN SUBSTRING(REVERSE(code),1,CHARINDEX(' - ',REVERSE(code))) = 'U'
		THEN 5
	ELSE pct.Id + 1000
END AS Id,
NULL AS FinancialTypeGroupId,
CASE 
	WHEN SUBSTRING(Code, LEN(code), 1) IN (
			'U',
			'R',
			'P'
			)
		THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
	END AS IsCash,
CASE pct.AlternateCode
	WHEN 'D'
		THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
	END AS IsDebit,
CASE pct.Exclusion WHEN 'T' THEN CONVERT(bit, 0) ELSE CONVERT(bit, 1) END AS IsPrintOnStatement,
SUBSTRING(Code, 1, (dbo.GetMax((charindex('-', code) - 2),0))) AS [Name],
LetterTranslation AS ShortName,
CONVERT(bit, 0) AS IsArchived,
pct.Rank as OrdinalId,
COUNT_BIG(*) AS [Count]
FROM dbo.PracticeCodeTable pct
WHERE pct.ReferenceType = 'PAYMENTTYPE'
	AND pct.AlternateCode IN ('C', 'D')
GROUP BY Id,
	Code,
	pct.AlternateCode,
	LetterTranslation,
	pct.Rank,
	pct.Exclusion

GO

CREATE UNIQUE CLUSTERED INDEX [IX_AdjustmentTypes_Internal_Id] ON [model].[AdjustmentTypes_Internal]
(
	[Id] ASC
)
GO