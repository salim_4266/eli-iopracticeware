﻿IF (OBJECT_ID('model.USP_CMS_InitialPatientPopulation', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_InitialPatientPopulation
END
GO

CREATE PROCEDURE [model].[USP_CMS_InitialPatientPopulation] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@UserId INT
	)
AS
BEGIN
	SELECT E.Id AS EncounterId
		,e.StartDateTime AS EncounterDate
		,PatientId
		,DateOfBirth
		,CASE 
			WHEN (MONTH(@StartDate) * 100) + DAY(@StartDate) >= (MONTH(DateOfBirth) * 100) + DAY(DateOfBirth)
				THEN DATEDIFF(YEAR, DateOfBirth, @StartDate)
			ELSE DATEDIFF(YEAR, DateOfBirth, @StartDate) - 1
			END AS Age
	FROM model.Encounters E
	INNER JOIN model.Appointments A ON A.EncounterId = E.Id
	INNER JOIN model.Patients P ON P.Id = E.PatientId
	WHERE EncounterStatusId = 7
		AND LastName <> 'TEST'
		AND CONVERT(DATETIME,CONVERT(VARCHAR(10),E.StartDateTime,111)) BETWEEN CONVERT(DATETIME,CONVERT(VARCHAR(10),@StartDate,111))
			AND CONVERT(DATETIME,CONVERT(VARCHAR(10),@EndDate,111))
		AND A.UserId = @UserId
END
GO