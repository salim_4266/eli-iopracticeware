﻿IF OBJECT_ID ('dbo.usp_FinancialData6', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.usp_FinancialData6;
GO

CREATE PROCEDURE [dbo].[usp_FinancialData6]
(
@PatId int,
@InvId nvarchar(32),
@Bal int
)
AS
BEGIN
	SET NOCOUNT ON;
	--openbalance
	IF OBJECT_ID('tempdb..#openbalance') IS NOT NULL 
		DROP TABLE #openbalance

	SELECT 	
	CASE
		WHEN charges.Charge IS NOT NULL
			THEN CASE
					WHEN adjusts.Adjust IS NOT NULL
						THEN CONVERT(REAL, charges.Charge-adjusts.Adjust)
					WHEN adjusts.Adjust IS NULL
						THEN CONVERT(REAL, charges.Charge)
				END
		ELSE 0
	END AS OpenBalance
	,charges.InvoiceId
	INTO #openbalance
	FROM (
		SELECT
		SUM(
			CASE 
				WHEN adjT.IsDebit = 1 
					THEN (CASE WHEN adj.Amount > 0 THEN -adj.Amount ELSE adj.Amount END) 
				ELSE adj.Amount 
			END
			) AS Adjust
		,ir.InvoiceId
		FROM model.Adjustments adj 
		INNER JOIN model.AdjustmentTypes adjT ON adjT.Id=adj.AdjustmentTypeId
		JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
		JOIN model.Invoices i ON i.Id = ir.InvoiceId
		WHERE i.LegacyPatientReceivablesInvoice = @InvId
		GROUP BY ir.InvoiceId
	) adjusts
	LEFT JOIN dbo.Charges charges WITH(NOEXPAND) ON charges.InvoiceId = adjusts.InvoiceId
	LEFT JOIN model.Invoices iv ON iv.Id = Charges.InvoiceId
	WHERE iv.LegacyPatientReceivablesInvoice = @InvId

	IF OBJECT_ID('tempdb..#locations') IS NOT NULL 
		DROP TABLE #locations

	SELECT
	Id
	,CASE
		WHEN ShortName = 'Office' AND IsExternal = 0
			THEN 0
		WHEN ShortName LIKE 'Office-%' AND IsExternal = 0
			THEN Id+1000
		WHEN IsExternal = 1
			THEN Id-110000000
		ELSE Id
	END AS PrBillingOfficeId
	INTO #locations
	FROM (
		SELECT 
		pn.PracticeId AS Id
		,CONVERT(BIT, 0) AS IsExternal
		,CASE LocationReference
			WHEN ''
				THEN 'Office'
			ELSE 'Office-' + LocationReference
		END AS ShortName
		FROM dbo.PracticeName pn
		WHERE pn.PracticeType = 'P'

		UNION ALL

		SELECT re.ResourceId+110000000 AS Id
		,CONVERT(BIT, 1) AS IsExternal
		,re.ResourceName AS ShortName
		FROM dbo.Resources re
		LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE'
			AND SUBSTRING(pct.Code, 1, 2) = re.PlaceOfService
		WHERE re.ResourceType = 'R'
			AND re.ServiceCode = '02'
	)v

	SELECT 
	CONVERT(NVARCHAR(32),i.LegacyPatientReceivablesInvoice) AS Invoice, 
	CONVERT(NVARCHAR(30),i.LegacyDateTime,112) AS InvoiceDate, a.AppointmentId,
	pf.Referral, 
	CONVERT(INT,pro.UserId) AS BillDrId, 
	bd.ResourceName AS BillDrName,
	l.PrBillingOfficeId AS SchAttId,
	a.ResourceId1 AS SchDrId,
	sd.ResourceName AS SchDrName,
	a.ResourceId2 AS SchLocId,
	CONVERT(INT,CASE 
		WHEN ((i.ReferringExternalProviderId IS NULL) OR (i.ReferringExternalProviderId = ''))
			THEN 0
		ELSE i.ReferringExternalProviderId
	END) AS RefDrId,
	rf.VendorLastName AS RefDrName
	FROM model.Invoices i
	INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
	LEFT JOIN model.PatientInsurances p ON p.Id = ir.PatientInsuranceId
	LEFT JOIN model.InsurancePolicies ip ON ip.Id = p.InsurancePolicyId
	INNER JOIN dbo.Appointments a ON a.AppointmentId = i.EncounterId
	INNER JOIN PatientDemographics pd ON pd.PatientId = a.PatientId 
	LEFT JOIN #locations l ON l.Id = i.AttributeToServiceLocationId
	LEFT JOIN #openbalance o ON o.InvoiceId = ir.InvoiceId
	LEFT JOIN PatientReferral pf ON a.ReferralId=pf.ReferralId
	LEFT JOIN model.Providers pro ON i.BillingProviderId = pro.Id
	LEFT JOIN Resources as bd ON pro.UserId=bd.ResourceId 
	LEFT JOIN Resources as sd ON a.ResourceId1=sd.ResourceId 
	LEFT JOIN PracticeVendors as rf ON CASE 
		WHEN ((i.ReferringExternalProviderId IS NULL) OR (i.ReferringExternalProviderId = ''))
			THEN 0
		ELSE i.ReferringExternalProviderId
	END=rf.VendorId 
	WHERE (((o.OpenBalance <> @bal)  OR (@bal = -1)))
	AND ((i.LegacyPatientReceivablesInvoice = @InvId) OR (@InvId = '-1'))
	AND (a.PatientId = @PatId) 
	GROUP BY i.LegacyPatientReceivablesInvoice, 
	CONVERT(NVARCHAR(30),i.LegacyDateTime,112), a.AppointmentId,
	pf.Referral, 
	pro.UserId, 
	bd.ResourceName,
	a.ResourceId1,
	sd.ResourceName,
	a.ResourceId2,
	l.PrBillingOfficeId,
	CASE 
		WHEN ((i.ReferringExternalProviderId IS NULL) OR (i.ReferringExternalProviderId = ''))
			THEN 0
		ELSE i.ReferringExternalProviderId
	END
	,rf.VendorLastName 
	ORDER BY CONVERT(NVARCHAR(30),i.LegacyDateTime,112) DESC, i.LegacyPatientReceivablesInvoice DESC
END
GO