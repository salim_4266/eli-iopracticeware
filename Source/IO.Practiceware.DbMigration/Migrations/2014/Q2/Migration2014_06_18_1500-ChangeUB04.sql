﻿IF EXISTS (
		SELECT *
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
		)
BEGIN
	DECLARE @old VARCHAR(MAX)
		,@new VARCHAR(MAX)
		,@Content VARCHAR(MAX)

	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = 'UB-04'
				AND Content IS NOT NULL
			)
	SET @old = 'id="PrimaryInsuredFirstNameLastName" value="@Model.PrimaryInsuredFirstName @Model.PrimaryInsuredLastName'

	IF (CHARINDEX(@old, @Content) > 0)
	BEGIN
		SET @new = 'id="PrimaryInsuredLastNameFirstName" value="@Model.PrimaryInsuredLastName @Model.PrimaryInsuredFirstName'

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
	END
END
GO

IF EXISTS (
		SELECT *
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
		)
BEGIN
	DECLARE @old VARCHAR(MAX)
		,@new VARCHAR(MAX)
		,@Content VARCHAR(MAX)

	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = 'UB-04'
				AND Content IS NOT NULL
			)
	SET @old = 'id="SecondaryInsuredFirstNameLastName" value="@Model.SecondaryInsuredFirstName @Model.SecondaryInsuredLastName'

	IF (CHARINDEX(@old, @Content) > 0)
	BEGIN
		SET @new = 'id="SecondaryInsuredLastNameFirstName" value="@Model.SecondaryInsuredLastName @Model.SecondaryInsuredFirstName'

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
	END
END
GO

IF EXISTS (
		SELECT *
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
		)
BEGIN
	DECLARE @old VARCHAR(MAX)
		,@new VARCHAR(MAX)
		,@Content VARCHAR(MAX)

	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = 'UB-04'
				AND Content IS NOT NULL
			)
	SET @old = 'id="ThirdInsuredFirstNameLastName" value="@Model.ThirdInsuredFirstName @Model.ThirdInsuredLastName'

	IF (CHARINDEX(@old, @Content) > 0)
	BEGIN
		SET @new = 'id="ThirdInsuredLastNameFirstName" value="@Model.ThirdInsuredLastName @Model.ThirdInsuredFirstName'

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
	END
END
GO

IF EXISTS (
		SELECT *
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
		)
BEGIN
	DECLARE @old VARCHAR(MAX)
		,@new VARCHAR(MAX)
		,@Content VARCHAR(MAX)

	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = 'UB-04'
				AND Content IS NOT NULL
			)
	SET @old = '<input id="PrincipalProcedureCode" value="@Model.PrincipalProcedureCode"'

	IF (CHARINDEX(@old, @Content) > 0)
	BEGIN
		SET @new = ''

		DECLARE @StartIndex INT
			,@Length INT

		SET @StartIndex = (
				SELECT CHARINDEX(@old, @Content)
				)
		SET @Length = CHARINDEX('/>', @Content, @StartIndex) - @StartIndex + LEN('/>')
		SET @old = SUBSTRING(@Content, @StartIndex, @Length)

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
	END
END
GO

IF EXISTS (
		SELECT *
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
		)
BEGIN
	DECLARE @old VARCHAR(MAX)
		,@new VARCHAR(MAX)
		,@Content VARCHAR(MAX)

	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = 'UB-04'
				AND Content IS NOT NULL
			)
	SET @old = 'id="PatientLastNameFirstName" value="@Model.PatientLastName, @Model.PatientFirstName" type="text" style="position:absolute; top: 87px; left: 140px; width: 262px; bottom: 938px;"'

	IF (CHARINDEX(@old, @Content) > 0)
	BEGIN
		SET @new = 'id="PatientLastNameFirstName" value="@Model.PatientLastName, @Model.PatientFirstName" type="text" style="position:absolute; top: 107px; left: 25px; width: 262px; bottom: 938px;"'

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
	END
END
GO


IF EXISTS (
		SELECT *
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
		)
BEGIN
	DECLARE @old VARCHAR(MAX)
		,@new VARCHAR(MAX)
		,@Content VARCHAR(MAX)

	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = 'UB-04'
				AND Content IS NOT NULL
			)
	SET @old = 'id="PointOfOriginCode" value="3"'
	IF (CHARINDEX(@old, @Content) > 0)
	BEGIN
		SET @new = 'id="PointOfOriginCode" value="1"'

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
	END
END
GO

IF EXISTS (
		SELECT *
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
		)
BEGIN
	DECLARE @old VARCHAR(MAX)
		,@new VARCHAR(MAX)
		,@Content VARCHAR(MAX)

	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = 'UB-04'
				AND Content IS NOT NULL
			)
	SET @old = 'id="DischargeStatus" value="1"'
	IF (CHARINDEX(@old, @Content) > 0)
	BEGIN
		SET @new = 'id="DischargeStatus" value="01"'

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = 'UB-04'
			AND Content IS NOT NULL
	END
END
GO