﻿IF OBJECT_ID ('model.PatientDiagnosisDetails', 'U' ) IS NULL 
BEGIN
	EXEC dbo.DropObject 'model.PatientDiagnosisDetails'

	CREATE TABLE [model].[PatientDiagnosisDetails](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[PatientDiagnosisId] [int] NOT NULL,
		[StartDateTime] [datetime] NULL,
		[EndDateTime] [datetime] NULL,
		[IsDeactivated] [bit] NOT NULL,
		[EnteredDateTime] [datetime] NOT NULL,
		[UserId] [int] NOT NULL,
		[Comment] [nvarchar](max) NULL,
		[RelevancyQualifierId] [int] NULL,
		[AccuracyQualifierId] [int] NULL,
		[ClinicalConditionStatusId] [int] NOT NULL,
		[BodyLocationId] [int] NULL,
		[IsOnProblemList] [bit] NOT NULL,
		[EncounterId] [int] NULL,
		[IsEncounterDiagnosis] [bit] NOT NULL,
		[EncounterTreatmentGoalAndInstructionId] [int] NULL,
		[IsBillable] [bit] NOT NULL,
		 CONSTRAINT [PK_PatientDiagnosisDetails] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)
	)

	ALTER TABLE [model].[PatientDiagnosisDetails] ADD  DEFAULT (CONVERT([bit],(0),0)) FOR [IsBillable]
END
GO


