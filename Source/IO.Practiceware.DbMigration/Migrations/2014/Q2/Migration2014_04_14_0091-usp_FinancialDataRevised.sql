﻿IF OBJECT_ID ('dbo.usp_FinancialData', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.usp_FinancialData;
GO

CREATE PROCEDURE [dbo].[usp_FinancialData]
(
@PatId int,
@InvId nvarchar(32),
@Bal int
)
AS
BEGIN
	SET NOCOUNT ON;
	IF OBJECT_ID('tempdb..#Invoices') IS NOT NULL 
		DROP TABLE #Invoices

	SELECT
	i.Id
	INTO #Invoices
	FROM dbo.Appointments a
	INNER JOIN model.Invoices i ON a.AppointmentId = i.EncounterId
	WHERE a.PatientId = @PatId

	IF OBJECT_ID('tempdb..#openbalance') IS NOT NULL 
		DROP TABLE #openbalance

	SELECT 	
	charges.Charge AS Charge
	,CASE
		WHEN charges.Charge IS NOT NULL
			THEN CASE
					WHEN adjusts.Adjust IS NOT NULL
						THEN CONVERT(REAL, charges.Charge-adjusts.Adjust)
					WHEN adjusts.Adjust IS NULL
						THEN CONVERT(REAL, charges.Charge)
				END
		ELSE 0
	END AS OpenBalance
	,charges.InvoiceId
	INTO #openbalance
	FROM (
		SELECT
		SUM(
			CASE 
				WHEN adjT.IsDebit = 1 
					THEN (CASE WHEN adj.Amount > 0 THEN -adj.Amount ELSE adj.Amount END) 
				ELSE adj.Amount 
			END
			) AS Adjust
		,ir.InvoiceId
		FROM model.Adjustments adj 
		INNER JOIN model.AdjustmentTypes adjT ON adjT.Id=adj.AdjustmentTypeId
		LEFT JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
		INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
		INNER JOIN #Invoices inv ON inv.Id = i.Id
		GROUP BY ir.InvoiceId
	) adjusts
	LEFT JOIN dbo.Charges charges WITH(NOEXPAND) ON charges.InvoiceId = adjusts.InvoiceId
	INNER JOIN #Invoices inv ON inv.Id = charges.InvoiceId

	IF OBJECT_ID('tempdb..#locations') IS NOT NULL 
		DROP TABLE #locations

	SELECT
	Id
	,CASE
		WHEN ShortName = 'Office' AND IsExternal = 0
			THEN 0
		WHEN ShortName LIKE 'Office-%' AND IsExternal = 0
			THEN Id+1000
		WHEN IsExternal = 1
			THEN Id-110000000
		ELSE Id
	END AS PrBillingOfficeId
	INTO #locations
	FROM (
		SELECT 
		pn.PracticeId AS Id
		,CONVERT(BIT, 0) AS IsExternal
		,CASE LocationReference
			WHEN ''
				THEN 'Office'
			ELSE 'Office-' + LocationReference
		END AS ShortName
		FROM dbo.PracticeName pn
		WHERE pn.PracticeType = 'P'

		UNION ALL

		SELECT re.ResourceId+110000000 AS Id
		,CONVERT(BIT, 1) AS IsExternal
		,re.ResourceName AS ShortName
		FROM dbo.Resources re
		LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE'
			AND SUBSTRING(pct.Code, 1, 2) = re.PlaceOfService
		WHERE re.ResourceType = 'R'
			AND re.ServiceCode = '02'
	)v

	SELECT 
	ir.Id AS ReceivableId,
	i.LegacyPatientReceivablesInvoice AS Invoice, 
	CONVERT(NVARCHAR(8),i.LegacyDateTime,112) AS InvoiceDate, a.AppointmentId,
	ISNULL(CONVERT(REAL,o.OpenBalance),CONVERT(REAL,0)) AS OpenBalance ,ISNULL(CONVERT(REAL,o.Charge),CONVERT(REAL,0)) AS Charge,
	pf.Referral, 
	pro.UserId as BillDrId, 
	bd.ResourceName as BillDrName,
	l.PrBillingOfficeId as SchAttId,
	a.ResourceId1 as SchDrId,
	sd.ResourceName as SchDrName,
	a.ResourceId2 as SchLocId,
	CASE 
		WHEN ((i.ReferringExternalProviderId IS NULL) OR (i.ReferringExternalProviderId = ''))
			THEN 0
		ELSE i.ReferringExternalProviderId
	END as RefDrId,
	rf.VendorLastName as RefDrName,
	ISNULL(ip.InsurerId,0) AS InsurerId, ISNULL(pl.Name,'ZZZ No insurance') AS InsurerName
	FROM model.InvoiceReceivables ir
	INNER JOIN model.Invoices i ON ir.InvoiceId = i.Id
	INNER JOIN #Invoices inv ON inv.Id = i.Id
	INNER JOIN dbo.Appointments a ON a.AppointmentId = i.EncounterId
	INNER JOIN PatientDemographics pd ON pd.PatientId = a.PatientId 
	JOIN #locations l ON l.Id = i.AttributeToServiceLocationId
	LEFT JOIN model.PatientInsurances p ON p.Id = ir.PatientInsuranceId
	LEFT JOIN model.InsurancePolicies ip ON ip.Id = p.InsurancePolicyId
	LEFT JOIN #openbalance o ON o.InvoiceId = ir.InvoiceId
	LEFT JOIN PatientReferral pf ON a.ReferralId=pf.ReferralId
	LEFT JOIN model.Insurers pl ON ip.InsurerId=pl.Id
	LEFT JOIN model.Providers pro ON i.BillingProviderId = pro.Id
	LEFT JOIN Resources as bd ON pro.UserId=bd.ResourceId 
	LEFT JOIN Resources as sd ON a.ResourceId1=sd.ResourceId 
	LEFT JOIN PracticeVendors as rf ON CASE 
		WHEN ((i.ReferringExternalProviderId IS NULL) OR (i.ReferringExternalProviderId = ''))
			THEN 0
		ELSE i.ReferringExternalProviderId
	END=rf.VendorId 
	WHERE ((o.OpenBalance <> @bal)  OR (@bal = -1))
	AND ((i.LegacyPatientReceivablesInvoice = @InvId) OR (@InvId = '-1'))
	AND (a.PatientId = @PatId) 
	AND ir.Id IN (SELECT ReceivableId FROM PatientReceivables)
	GROUP BY ir.Id, i.LegacyPatientReceivablesInvoice, ir.PatientInsuranceId,
	i.LegacyDateTime, a.AppointmentId,
	pf.Referral, 
	pro.UserId, 
	bd.ResourceName,
	a.ResourceId1,
	sd.ResourceName,
	a.ResourceId2,
	l.PrBillingOfficeId,
	CASE 
		WHEN ((i.ReferringExternalProviderId IS NULL) OR (i.ReferringExternalProviderId = ''))
			THEN 0
		ELSE i.ReferringExternalProviderId
	END
	,rf.VendorLastName 
	,o.OpenBalance
	,o.Charge
	,ip.InsurerId
	,pl.Name
	,ir.LegacyInsurerDesignation
	ORDER BY i.LegacyDateTime DESC, i.LegacyPatientReceivablesInvoice DESC, ir.LegacyInsurerDesignation DESC
END
GO
