﻿--USP_Surveillance_GetPatientDetails
IF EXISTS (
		SELECT *
		FROM dbo.sysobjects
		WHERE id = OBJECT_ID(N'dbo.USP_Surveillance_GetPatientDetails')
			AND OBJECTPROPERTY(id, N'IsProcedure') = 1
		)
BEGIN
	DROP PROCEDURE dbo.USP_Surveillance_GetPatientDetails
END
GO

CREATE PROCEDURE [dbo].[USP_Surveillance_GetPatientDetails] (@Patient_Id VARCHAR(100))
AS
DECLARE @Sqlstr VARCHAR(MAX)

BEGIN
	SET @Sqlstr = 'DECLARE @RaceCode AS NVARCHAR(200)
DECLARE @EthnicityCode AS NVARCHAR(200)

Select TOP 1 @RaceCode= cdc.Code
from model.Patients pr
INNER JOIN model.PatientRace ppr ON ppr.Patients_Id=pr.Id
INNER JOIN model.Races r ON  r.Id=ppr.Races_Id
INNER JOIN model.ExternalSystemEntities ESE ON ese.Name=''RaceAndEthnicityCodeSet''
INNER JOIN model.PracticeRepositoryEntities PRE ON PRE.Name = ''Race''
INNER JOIN model.ExternalSystemEntityMappings ESEM ON ESEM.PracticeRepositoryEntityId = PRE.Id AND ESEM.PracticeRepositoryEntityKey = r.Id AND ISNUMERIC(ESEm.ExternalSystemEntityKey)=1
INNER JOIN cdc.RaceAndEthnicityCodeSet cdc ON cdc.Id = ESEM.ExternalSystemEntityKey AND cdc.Name=r.Name
WHERE pr.Id=' + @Patient_Id + 
		'

SELECT TOP 1 @EthnicityCode=cdc.Code
from model.Patients pr
INNER JOIN model.Ethnicities e ON pr.EthnicityId=e.Id
INNER JOIN model.ExternalSystemEntities ESE ON ese.Name=''RaceAndEthnicityCodeSet''
INNER JOIN model.PracticeRepositoryEntities PRE ON PRE.Name = ''Ethnicity''
INNER JOIN model.ExternalSystemEntityMappings ESEM ON ESEM.PracticeRepositoryEntityId = PRE.Id AND ESEM.PracticeRepositoryEntityKey = e.Id AND ISNUMERIC(ESEm.ExternalSystemEntityKey)=1
INNER JOIN cdc.RaceAndEthnicityCodeSet cdc ON cdc.Id = ESEM.ExternalSystemEntityKey AND cdc.Name=e.Name
WHERE pr.Id=' + @Patient_Id + 
		'


Select TOP 1 isnull(pd.LastName,'''') LastName,         
isnull(pd.FirstName,'''') FirstName, isnull(pd.MiddleInitial,'''') MI, isnull(pd.BirthDate,'''') DOB,         
Gender,isnull(pd.Address,'''') Address,isnull(pd.City,'''') City, isnull(pd.Race,'''') Race,isnull(pd.Ethnicity,'''') Ethnicity,       
isnull(pd.State,'''') State,isnull(pd.zip,'''') zip,isnull(pd.Homephone,'''') HomePh,isnull(pd.Religion,'''') Religion,        
isnull(pd.BusinessPhone, '''') WorkPh,isnull(pd.Marital, '''') MaritalStatus,isnull(pd.NationalOrigin ,'''') Nationality,     
isnull(pd.LastName, '''') + isnull('' ''+ pd.FirstName, '''')         
+ isnull('' ''+ pd.MiddleInitial, '''') PatientName,
@RaceCode As RaceCode,
@EthnicityCode as EthnicCode
from patientdemographics pd
where pd.patientid = ' + @Patient_Id + ''

	EXEC (@Sqlstr)
END
GO


IF EXISTS (
		SELECT *
		FROM sys.columns
		WHERE NAME = N'EthnicCode'
			AND Object_ID = Object_ID(N'model.Ethnicities')
		)
BEGIN
DECLARE @StrSql AS NVARCHAR(500);
	SET @StrSql=N'
	UPDATE model.Ethnicities
	SET EthnicCode = ''''

	ALTER TABLE model.Ethnicities

	DROP COLUMN EthnicCode'
	EXEC (@StrSql)
END
GO

IF OBJECT_ID(N'[model].[AuditEthnicitiesDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditEthnicitiesDeleteTrigger]
END
GO
IF OBJECT_ID(N'[model].[AuditEthnicitiesInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditEthnicitiesInsertTrigger]
END
GO
IF OBJECT_ID(N'[model].[AuditEthnicitiesUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditEthnicitiesUpdateTrigger]
END
GO

IF EXISTS (
		SELECT *
		FROM sys.columns
		WHERE NAME = N'RaceCode'
			AND Object_ID = Object_ID(N'model.Races')
		)
BEGIN
	DECLARE @StrSql AS NVARCHAR(500);
	SET @StrSql=N'
	UPDATE model.Races SET RaceCode = ''''

	ALTER TABLE model.Races

	DROP COLUMN RaceCode'
	EXEC (@StrSql)
END
GO

IF OBJECT_ID(N'[model].[AuditRacesDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditRacesDeleteTrigger]
END
GO
IF OBJECT_ID(N'[model].[AuditRacesInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditRacesInsertTrigger]
END
GO
IF OBJECT_ID(N'[model].[AuditRacesUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditRacesUpdateTrigger]
END
GO

IF EXISTS (
		SELECT Id
		FROM model.Races
		WHERE NAME = 'Other'
		)
BEGIN
		DECLARE @RaceIdToDelete NVARCHAR(250)
		
		IF OBJECT_ID('#tmpPatientRace') IS NOT NULL
		BEGIN
				DROP TABLE #tmpPatientRace
		END

		CREATE TABLE #tmpPatientRace(Patients_Id INT,Races_Id INT)

		DECLARE CursorToDropRace CURSOR FAST_FORWARD
		FOR SELECT Id FROM model.Races WHERE NAME = 'Other'
		OPEN CursorToDropRace 
		FETCH NEXT FROM CursorToDropRace INTO @RaceIdToDelete
		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO #tmpPatientRace 
			SELECT Patients_Id,Races_Id FROM model.PatientRace
			WHERE Races_Id = @RaceIdToDelete

			DELETE FROM model.PatientRace
			where Races_Id = 6
			and Patients_Id IN (SELECT Patients_Id FROM #tmpPatientRace)
			 
			UPDATE model.PatientRace
			SET Races_Id = 6
			WHERE Patients_Id IN (SELECT Patients_Id FROM #tmpPatientRace)
			and Races_Id = @RaceIdToDelete

			DELETE FROM model.Races WHERE Id = @RaceIdToDelete

			DELETE FROM #tmpPatientRace

			FETCH NEXT FROM CursorToDropRace INTO @RaceIdToDelete
		END
		CLOSE CursorToDropRace
END
GO