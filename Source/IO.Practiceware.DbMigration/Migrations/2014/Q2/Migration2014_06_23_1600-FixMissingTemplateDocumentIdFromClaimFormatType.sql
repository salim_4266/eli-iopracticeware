﻿IF EXISTS(SELECT * FROM model.ClaimFormTypes ctf 
WHERE ctf.Name = 'CMS-1500'
AND ctf.TemplateDocumentId IS NULL)
	UPDATE model.ClaimFormTypes
	SET TemplateDocumentId = (SELECT Id FROM model.TemplateDocuments WHERE DisplayName = 'CMS-1500')
	WHERE name = 'CMS-1500'
	AND TemplateDocumentId IS NULL