﻿IF EXISTS(SELECT OBJECT_ID (N'model.ClinicalQualifierCategories'))
	EXEC dbo.DropObject 'model.ClinicalQualifierCategories'
GO
IF EXISTS(SELECT OBJECT_ID (N'model.ClinicalQualifiers'))
	EXEC dbo.DropObject 'model.ClinicalQualifiers'
GO