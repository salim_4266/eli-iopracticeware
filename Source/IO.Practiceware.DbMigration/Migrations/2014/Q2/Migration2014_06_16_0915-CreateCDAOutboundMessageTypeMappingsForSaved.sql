﻿--Check just in case if Saved external system exists.If not create
IF NOT EXISTS (SELECT Id FROM model.ExternalSystems WHERE Id = 24)
BEGIN
	SET IDENTITY_INSERT model.ExternalSystems ON
	INSERT INTO model.ExternalSystems (Id,Name) VALUES (24,'Saved');
	SET IDENTITY_INSERT model.ExternalSystems OFF
END
GO

DECLARE @MessageTypeId INT

--EncounterClinicalSummaryCCDA
SET @MessageTypeId = (
		SELECT Id
		FROM model.ExternalSystemMessageTypes
		WHERE NAME = 'EncounterClinicalSummaryCCDA'			
			AND IsOutbound = 1
		)
-- An ExternalSystemExternalSystemMessageTypes mapping of Saved and Outbound EncounterClinicalSummaryCCDA. 
IF NOT EXISTS (
		SELECT *
		FROM model.ExternalSystemExternalSystemMessageTypes
		WHERE ExternalSystemId = 24
			AND ExternalSystemMessageTypeId = @MessageTypeId
		)
	INSERT INTO model.ExternalSystemExternalSystemMessageTypes (
		ExternalSystemId,ExternalSystemMessageTypeId
		)
	SELECT 24,@MessageTypeId

--PatientTransitionOfCareCCDA
SET @MessageTypeId = (
		SELECT Id
		FROM model.ExternalSystemMessageTypes
		WHERE NAME = 'PatientTransitionOfCareCCDA'			
			AND IsOutbound = 1
		)
-- An ExternalSystemExternalSystemMessageTypes mapping of Saved and Outbound Patient Transition of Care C-CDA. 
IF NOT EXISTS (
		SELECT *
		FROM model.ExternalSystemExternalSystemMessageTypes
		WHERE ExternalSystemId = 24
			AND ExternalSystemMessageTypeId = @MessageTypeId
		)
	INSERT INTO model.ExternalSystemExternalSystemMessageTypes (
		ExternalSystemId,ExternalSystemMessageTypeId
		)
	SELECT 24,@MessageTypeId
GO