﻿IF OBJECT_ID('[model].[UpdatePracticeTransactionJournalTransactionStatus]') IS NOT NULL
	BEGIN
		EXEC('DROP TRIGGER [model].[UpdatePracticeTransactionJournalTransactionStatus]')
	END

GO

CREATE TRIGGER [model].[UpdatePracticeTransactionJournalTransactionStatus] 
ON [model].[BillingServiceTransactions] 
AFTER UPDATE
AS 
BEGIN
	IF EXISTS(
	SELECT * FROM inserted
	EXCEPT
	SELECT * FROM deleted
	)
	BEGIN

	SELECT
	ptj.TransactionId
	,CASE
		WHEN d.BillingServiceTransactionStatusId = 1 AND i.BillingServiceTransactionStatusId = 2 AND ptj.TransactionStatus = 'P'
			THEN 'S'
		WHEN d.BillingServiceTransactionStatusId = 1 AND i.BillingServiceTransactionStatusId = 5 AND ptj.TransactionStatus = 'P'
			THEN 'Z'
		WHEN d.BillingServiceTransactionStatusId = 1 AND i.BillingServiceTransactionStatusId = 8 AND ptj.TransactionStatus = 'P'
			THEN 'S'
		WHEN d.BillingServiceTransactionStatusId = 2 AND i.BillingServiceTransactionStatusId = 6 AND ptj.TransactionStatus = 'S'
			THEN 'Z'
		WHEN d.BillingServiceTransactionStatusId = 3 AND i.BillingServiceTransactionStatusId = 5 AND ptj.TransactionStatus = 'V'
			THEN 'Z'
		WHEN d.BillingServiceTransactionStatusId = 4 AND i.BillingServiceTransactionStatusId = 7 AND ptj.TransactionStatus = 'S'
			THEN 'Z'
	END AS ThePTJStatusShouldNowBe
	,CONVERT(NVARCHAR(8),i.[DateTime],112) AS TransactionDate
	,(DATEPART(hh, i.[DateTime] ) * 60) + DATEPART(mi,i.[DateTime]) AS TransactionTime
	INTO #AffectedRows
	FROM dbo.PracticeTransactionJournal ptj
	INNER JOIN inserted i ON i.LegacyTransactionId = ptj.TransactionId
	INNER JOIN deleted d ON d.LegacyTransactionId = ptj.TransactionId
	
	IF EXISTS(SELECT * FROM dbo.sysobjects WHERE Name = 'UpdateBillingServiceTransactionStatus' AND ObjectProperty(Id, 'IsTrigger') = 1)	
	DISABLE TRIGGER [PracticeTransactionJournal].UpdateBillingServiceTransactionStatus ON 
	[dbo].PracticeTransactionJournal

	
	UPDATE ptj
	SET ptj.TransactionStatus = COALESCE(a.ThePTJStatusShouldNowBe,ptj.TransactionStatus)
	,ptj.TransactionDate = a.TransactionDate
	,ptj.TransactionTime = a.TransactionTime
	FROM dbo.PracticeTransactionJournal ptj
	INNER JOIN #AffectedRows a ON a.TransactionId = ptj.TransactionId
	
	IF EXISTS(SELECT * FROM dbo.sysobjects WHERE Name = 'UpdateBillingServiceTransactionStatus' AND ObjectProperty(Id, 'IsTrigger') = 1)	
	ENABLE TRIGGER [PracticeTransactionJournal].UpdateBillingServiceTransactionStatus ON 
	[dbo].PracticeTransactionJournal
	
	END		
END
GO
