﻿;WITH CTE AS (
	SELECT 
	ir2.Id AS CrossOverInvoiceReceivableId
	FROM model.InvoiceReceivables ir1
	INNER JOIN model.Invoices i ON i.Id = ir1.InvoiceId
	INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.EncounterId
	LEFT JOIN model.InvoiceReceivables ir2 ON ir2.InvoiceId = ir1.InvoiceId
		AND ir2.LegacyInsurerDesignation IS NULL
		AND ir2.OpenForReview = 1
	LEFT JOIN model.PatientInsurances pi1 ON pi1.Id = ir1.PatientInsuranceId
		AND pi1.InsuranceTypeId = ap.AppointmentInsuranceTypeId
	LEFT JOIN model.PatientInsurances pi2 ON pi2.Id = ir2.PatientInsuranceId
		AND pi2.InsuranceTypeId = ap.AppointmentInsuranceTypeId
	WHERE ir1.LegacyInsurerDesignation IS NOT NULL
		AND ir1.OpenForReview = 1
		AND ir2.PatientInsuranceId IS NOT NULL
		AND pi1.InsuranceTypeId = pi2.InsuranceTypeId
)
UPDATE ir
SET ir.OpenForReview = 0
FROM model.InvoiceReceivables ir
JOIN CTE ON CTE.CrossOverInvoiceReceivableId = ir.Id
	AND ir.OpenForReview = 1