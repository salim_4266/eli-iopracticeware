﻿IF OBJECT_ID('[dbo].[usp_UnassignPayments]', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[usp_UnassignPayments];
GO

CREATE PROCEDURE [dbo].[usp_UnassignPayments] (@PatId INT)
AS
SELECT
adj.Amount AS PaymentAmount
,'P' AS PaymentType
,i.LegacyPatientReceivablesInvoice AS Invoice
,CONVERT(NVARCHAR,i.LegacyDateTime,112) AS InvoiceDate
,p.LastName 
,p.FirstName
,adj.InvoiceReceivableId AS ReceivableId
,adj.LegacyPaymentId AS PaymentId
,i.LegacyPatientReceivablesAppointmentId AS AppointmentId
,p.Id AS PatientId
,adj.BillingServiceId AS PaymentServiceItem
,CONVERT(NVARCHAR,adj.PostedDateTime,112) AS PaymentDate
FROM model.Adjustments adj
INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.Patients p ON i.LegacyPatientReceivablesPatientId = p.Id
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.LegacyPatientReceivablesAppointmentId
WHERE adj.AdjustmentTypeId = 1
AND (adj.BillingServiceId <= 0 OR adj.BillingServiceId IS NULL)
AND (i.LegacyPatientReceivablesPatientId = @PatId OR @PatId = '0')
ORDER BY i.LegacyDateTime DESC
GO
