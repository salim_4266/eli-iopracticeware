﻿--Bug 11265.
IF EXISTS (
		SELECT OBJECT_ID
		FROM sys.objects o
		WHERE type IN ('V')
			AND SCHEMA_NAME(schema_id) = 'model'
			AND OBJECT_DEFINITION(object_id) LIKE '%110000000%'
			AND NAME = 'InvoiceReceivables'
		)
BEGIN
	SET NOCOUNT ON
	SET ANSI_NULLS ON

	DECLARE @rollback BIT

	SET @rollback = 0

	DECLARE @createIndexes BIT

	SET @createIndexes = 1

	EXEC [dbo].[RecreateIfDefinitionChanged] 'model.GetMasterFeeSchedules'
		,'
	CREATE FUNCTION model.GetMasterFeeSchedules ()
	RETURNS @temp TABLE (
		Id int,
		NAME nvarchar(255)
		)
		WITH SCHEMABINDING
	AS
	BEGIN
		INSERT INTO @temp
		SELECT min(pfs.ServiceId) AS Id,
			InsurerName + '' '' + InsurerPlanType + substring(StartDate, 1, 6) + substring(enddate, 5, 2) AS NAME
		FROM dbo.PracticeFeeSchedules pfs
		INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pfs.PlanId
		GROUP BY InsurerName,
			pfs.StartDate,
			pfs.EndDate,
			pri.InsurerPlanType

		RETURN
	END
	'
		,0
		,@error = @rollback OUTPUT

	EXEC [dbo].[RecreateIfDefinitionChanged] 'model.GetScheduleEntries'
		,
		'
	CREATE FUNCTION model.GetScheduleEntries(@Position AS int)
	RETURNS @Results TABLE ( Id int, EndDateTime datetime, EquipmentId int, RoomId int, ScheduleEntryAvailabilityTypeId int, ServiceLocationId int, StartDateTime datetime, UserId int) 
	WITH SCHEMABINDING
	AS 
	BEGIN

	INSERT INTO @Results   
	SELECT model.GetPairId(@Position, pc.CalendarId, 110000000) as Id, 
		CONVERT(DATETIME, CalendarDate + '' '' + SUBSTRING(CalendarEndTime, @Position * 8 + 1, 8)) AS EndDateTime,
		NULL as EquipmentId,
		CASE
			WHEN re.ResourceType = ''R''
				THEN CalendarResourceId
			ELSE NULL
			END AS RoomId,
		CASE
			WHEN RIGHT(CalendarPurpose, 2) = ''NT'' 
				THEN 2
			WHEN RIGHT(CalendarPurpose, 2) IN (''TT'', ''TN'') 
				THEN 3
			ELSE 1
			END AS ScheduleEntryAvailibilityTypeId,
		CASE 
			WHEN pnServLoc.PracticeId IS NULL
				THEN model.GetPairId(1, reServLoc.ResourceId, 110000000)			
			ELSE
				model.GetPairId(0, pnServLoc.PracticeId, 110000000)
			END AS ServiceLocationId,
		CONVERT(DATETIME, CalendarDate + '' '' + SUBSTRING(CalendarStartTime, @Position * 8 + 1, 8)) AS StartDateTime,
		CASE
			WHEN re.ResourceType <> ''R''
				THEN CalendarResourceId 
			ELSE NULL
			END AS UserId
	FROM dbo.PracticeCalendar pc
	INNER JOIN dbo.Resources re ON re.ResourceId = pc.CalendarResourceId
		AND pc.CalendarResourceId <> 0
	LEFT OUTER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P'' 	
		AND pnServLoc.PracticeId = CASE 
			WHEN 
				CASE @Position 
					WHEN 0 THEN CalendarLoc1
					WHEN 1 THEN CalendarLoc2			
					WHEN 2 THEN CalendarLoc3			
					WHEN 3 THEN CalendarLoc4			
					WHEN 4 THEN CalendarLoc5			
					WHEN 5 THEN CalendarLoc6			
					WHEN 6 THEN CalendarLoc7			
				END = 0
				THEN (SELECT PracticeId FROM dbo.PracticeName WHERE PracticeType = ''P'' AND LocationReference = '''')
			WHEN 
				CASE @Position 
					WHEN 0 THEN CalendarLoc1
					WHEN 1 THEN CalendarLoc2			
					WHEN 2 THEN CalendarLoc3			
					WHEN 3 THEN CalendarLoc4			
					WHEN 4 THEN CalendarLoc5			
					WHEN 5 THEN CalendarLoc6			
					WHEN 6 THEN CalendarLoc7			
				END = -1
				THEN (SELECT PracticeId FROM dbo.PracticeName WHERE PracticeType = ''P'' AND LocationReference = '''')
			WHEN @Position = 0 AND CalendarLoc1 > 1000 THEN CalendarLoc1 - 1000
			WHEN @Position = 1 AND CalendarLoc2 > 1000 THEN CalendarLoc2 - 1000
			WHEN @Position = 2 AND CalendarLoc3 > 1000 THEN CalendarLoc3 - 1000
			WHEN @Position = 3 AND CalendarLoc4 > 1000 THEN CalendarLoc4 - 1000
			WHEN @Position = 4 AND CalendarLoc5 > 1000 THEN CalendarLoc5 - 1000
			WHEN @Position = 5 AND CalendarLoc6 > 1000 THEN CalendarLoc6 - 1000
			WHEN @Position = 6 AND CalendarLoc7 > 1000 THEN CalendarLoc7 - 1000
			ELSE (SELECT PracticeId FROM dbo.PracticeName WHERE PracticeType = ''P'' AND LocationReference = '''')
			END

	LEFT OUTER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' 
			AND reServLoc.ResourceId = CASE
				WHEN @Position = 0 AND CalendarLoc1 < 1000
					THEN CalendarLoc1
				WHEN @Position = 1 AND CalendarLoc2 < 1000
					THEN CalendarLoc2
				WHEN @Position = 2 AND CalendarLoc3 < 1000
					THEN CalendarLoc3
				WHEN @Position = 3 AND CalendarLoc4 < 1000
					THEN CalendarLoc4
				WHEN @Position = 4 AND CalendarLoc5 < 1000
					THEN CalendarLoc5
				WHEN @Position = 5 AND CalendarLoc6 < 1000
					THEN CalendarLoc6
				WHEN @Position = 6 AND CalendarLoc7 < 1000
					THEN CalendarLoc7
				END
	WHERE SUBSTRING(CalendarEndTime, @Position * 8 + 1, 8) > ''0''
		AND (ISDATE(CalendarDate  + '' '' + SUBSTRING(CalendarEndTime, 1 * 8 + 1, 8)) = 1
			OR ISDATE(CalendarDate  + '' '' + SUBSTRING(CalendarStartTime, 1 * 8 + 1, 8)) = 1)
	RETURN
	END
	'
		,0
		,@error = @rollback OUTPUT

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Adjustments')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isAdjustmentsChanged BIT

		SET @isAdjustmentsChanged = 0

		DECLARE @AdjustmentsViewSql NVARCHAR(max)

		SET @AdjustmentsViewSql = 
			'
		CREATE VIEW [model].[Adjustments]
		WITH SCHEMABINDING
		AS
		SELECT v.Id AS Id
			, v.AdjustmentTypeId AS AdjustmentTypeId
			, v.Amount AS Amount
			, v.FinancialBatchId AS FinancialBatchId
			, InsurerId
			,CASE 
				WHEN pd.PatientId IS NULL 
					THEN NULL 
				ELSE v.PatientId 
				END AS PatientId
			,CASE 
				WHEN v.BillingServiceId  = 0
					THEN NULL
				WHEN prs.[Status] = ''X''
					THEN NULL
				ELSE 
					v.BillingServiceId
				END AS BillingServiceId
			,v.FinancialSourceTypeId AS FinancialSourceTypeId
			,v.InvoiceReceivableId AS InvoiceReceivableId
			,v.PaymentMethodId AS PaymentMethodId
			,COALESCE(v.PostedDateTime, CONVERT(datetime, ''20000101'')) AS PostedDateTime
		FROM (
			SELECT prp.PaymentId AS Id
			,CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
					WHEN  ''P''
						THEN 1
					WHEN ''X''
						THEN 2
					WHEN ''8''
						THEN 3
					WHEN ''R''
						THEN 4
					WHEN ''U''
						THEN 5
					ELSE pct.Id + 1000
			END AS AdjustmentTypeId
			,prp.PaymentAmountDecimal AS Amount
			,CASE WHEN PaymentBatchCheckId IN (0, -1) THEN NULL ELSE PaymentBatchCheckId END AS FinancialBatchId
			,CASE 
					WHEN prp.PayerType = ''I'' AND PayerId > 0
						THEN prp.PayerId
					ELSE NULL
				END AS InsurerId
				,CASE 
					WHEN prp.PayerType = ''P'' 
						THEN prp.PayerId 
					WHEN prp.PayerType = ''I'' AND pr.InsurerId = 99999 
						THEN pr.PatientId
					ELSE NULL 
					END AS PatientId
				,prp.PaymentServiceItem AS BillingServiceId
				,prp.PayerId AS PayerId
			,CASE prp.PayerType
					WHEN ''I''
						THEN 1
					WHEN ''P''
						THEN 2
					WHEN ''O''
						THEN 3
					ELSE 4
					END AS FinancialSourceTypeId
			,(CONVERT(bigint, 110000000) * (CASE 
										WHEN (ComputedFinancialId IS NULL OR ComputedFinancialId = 0 OR PayerType = ''P'') AND pr.InsurerId <> 99999
										THEN 0
									ELSE ComputedFinancialId 
									END) + pr.AppointmentId) AS InvoiceReceivableId
			,pctPaymentMethod.Id AS PaymentMethodId
			,CONVERT(datetime, prp.PaymentDate, 112) AS PostedDateTime
			FROM dbo.PatientReceivablePayments prp
			INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
			INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''PAYMENTTYPE'' AND prp.PaymentType = SUBSTRING(pct.Code, LEN(pct.Code), 1)
			LEFT JOIN dbo.PracticeCodeTable pctPaymentMethod ON pctPaymentMethod.ReferenceType = ''PAYABLETYPE'' and prp.PaymentRefType = SUBSTRING(pctPaymentMethod.Code, LEN(pctPaymentMethod.Code), 1)
			WHERE prp.PaymentFinancialType <> ''''
					AND ((prp.PayerType = ''I'' AND pr.ComputedFinancialId IS NOT NULL)
						OR
						prp.PayerType <> ''I''
						OR 
						(prp.PayerType = ''I'' AND pr.InsurerId = 99999 AND pr.ComputedFinancialId IS NULL))
			) v
		LEFT JOIN dbo.PatientReceivableServices prs ON prs.ItemId = v.BillingServiceId
		LEFT JOIN dbo.PatientDemographics pd ON pd.PatientId = v.PayerId'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Adjustments'
			,@AdjustmentsViewSql
			,@isChanged = @isAdjustmentsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAdjustmentDelete'
				,
				'
			CREATE TRIGGER model.OnAdjustmentDelete ON [model].[Adjustments] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @FinancialSourceTypeId int
			DECLARE @Amount decimal(18,0)
			DECLARE @PostedDateTime datetime
			DECLARE @AdjustmentTypeId int
			DECLARE @BillingServiceId int
			DECLARE @InvoiceReceivableId bigint
			DECLARE @PatientId int
			DECLARE @InsurerId int
			DECLARE @FinancialBatchId int
			DECLARE @PaymentMethodId int
		
			DECLARE AdjustmentsDeletedCursor CURSOR FOR
			SELECT Id, FinancialSourceTypeId, Amount, PostedDateTime, AdjustmentTypeId, BillingServiceId, InvoiceReceivableId, PatientId, InsurerId, FinancialBatchId, PaymentMethodId FROM deleted
		
			OPEN AdjustmentsDeletedCursor
			FETCH NEXT FROM AdjustmentsDeletedCursor INTO @Id, @FinancialSourceTypeId, @Amount, @PostedDateTime, @AdjustmentTypeId, @BillingServiceId, @InvoiceReceivableId, @PatientId, @InsurerId, @FinancialBatchId, @PaymentMethodId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for Adjustment --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @FinancialSourceTypeId AS FinancialSourceTypeId, @Amount AS Amount, @PostedDateTime AS PostedDateTime, @AdjustmentTypeId AS AdjustmentTypeId, @BillingServiceId AS BillingServiceId, @InvoiceReceivableId AS InvoiceReceivableId, @PatientId AS PatientId, @InsurerId AS InsurerId, @FinancialBatchId AS FinancialBatchId, @PaymentMethodId AS PaymentMethodId
			
				FETCH NEXT FROM AdjustmentsDeletedCursor INTO @Id, @FinancialSourceTypeId, @Amount, @PostedDateTime, @AdjustmentTypeId, @BillingServiceId, @InvoiceReceivableId, @PatientId, @InsurerId, @FinancialBatchId, @PaymentMethodId 
		
			END
		
			CLOSE AdjustmentsDeletedCursor
			DEALLOCATE AdjustmentsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAdjustmentInsert'
				,
				'
			CREATE TRIGGER model.OnAdjustmentInsert ON [model].[Adjustments] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @FinancialSourceTypeId int
			DECLARE @Amount decimal(18,0)
			DECLARE @PostedDateTime datetime
			DECLARE @AdjustmentTypeId int
			DECLARE @BillingServiceId int
			DECLARE @InvoiceReceivableId bigint
			DECLARE @PatientId int
			DECLARE @InsurerId int
			DECLARE @FinancialBatchId int
			DECLARE @PaymentMethodId int
		
			DECLARE AdjustmentsInsertedCursor CURSOR FOR
			SELECT Id, FinancialSourceTypeId, Amount, PostedDateTime, AdjustmentTypeId, BillingServiceId, InvoiceReceivableId, PatientId, InsurerId, FinancialBatchId, PaymentMethodId FROM inserted
		
			OPEN AdjustmentsInsertedCursor
			FETCH NEXT FROM AdjustmentsInsertedCursor INTO @Id, @FinancialSourceTypeId, @Amount, @PostedDateTime, @AdjustmentTypeId, @BillingServiceId, @InvoiceReceivableId, @PatientId, @InsurerId, @FinancialBatchId, @PaymentMethodId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for Adjustment --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @FinancialSourceTypeId AS FinancialSourceTypeId, @Amount AS Amount, @PostedDateTime AS PostedDateTime, @AdjustmentTypeId AS AdjustmentTypeId, @BillingServiceId AS BillingServiceId, @InvoiceReceivableId AS InvoiceReceivableId, @PatientId AS PatientId, @InsurerId AS InsurerId, @FinancialBatchId AS FinancialBatchId, @PaymentMethodId AS PaymentMethodId
				
				FETCH NEXT FROM AdjustmentsInsertedCursor INTO @Id, @FinancialSourceTypeId, @Amount, @PostedDateTime, @AdjustmentTypeId, @BillingServiceId, @InvoiceReceivableId, @PatientId, @InsurerId, @FinancialBatchId, @PaymentMethodId 
		
			END
		
			CLOSE AdjustmentsInsertedCursor
			DEALLOCATE AdjustmentsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAdjustmentUpdate'
				,
				'
			CREATE TRIGGER model.OnAdjustmentUpdate ON [model].[Adjustments] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @FinancialSourceTypeId int
			DECLARE @Amount decimal(18,0)
			DECLARE @PostedDateTime datetime
			DECLARE @AdjustmentTypeId int
			DECLARE @BillingServiceId int
			DECLARE @InvoiceReceivableId bigint
			DECLARE @PatientId int
			DECLARE @InsurerId int
			DECLARE @FinancialBatchId int
			DECLARE @PaymentMethodId int
		
			DECLARE AdjustmentsUpdatedCursor CURSOR FOR
			SELECT Id, FinancialSourceTypeId, Amount, PostedDateTime, AdjustmentTypeId, BillingServiceId, InvoiceReceivableId, PatientId, InsurerId, FinancialBatchId, PaymentMethodId FROM inserted
		
			OPEN AdjustmentsUpdatedCursor
			FETCH NEXT FROM AdjustmentsUpdatedCursor INTO @Id, @FinancialSourceTypeId, @Amount, @PostedDateTime, @AdjustmentTypeId, @BillingServiceId, @InvoiceReceivableId, @PatientId, @InsurerId, @FinancialBatchId, @PaymentMethodId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for Adjustment --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM AdjustmentsUpdatedCursor INTO @Id, @FinancialSourceTypeId, @Amount, @PostedDateTime, @AdjustmentTypeId, @BillingServiceId, @InvoiceReceivableId, @PatientId, @InsurerId, @FinancialBatchId, @PaymentMethodId 
		
			END
		
			CLOSE AdjustmentsUpdatedCursor
			DEALLOCATE AdjustmentsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.AdjustmentTypes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isAdjustmentTypesChanged BIT

		SET @isAdjustmentTypesChanged = 0

		DECLARE @AdjustmentTypesViewSql NVARCHAR(max)

		SET @AdjustmentTypesViewSql = 
			'
		CREATE VIEW [model].[AdjustmentTypes]
		WITH SCHEMABINDING
		AS
		SELECT v.Id AS Id,
			v.FinancialTypeGroupId AS FinancialTypeGroupId,
			v.IsCash AS IsCash,
			v.IsDebit AS IsDebit,
			v.IsPrintOnStatement AS IsPrintOnStatement,
			v.Name AS Name,
			v.ShortName AS ShortName,
			v.IsArchived AS IsArchived
		FROM (SELECT CASE
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''P''
					THEN 1
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''X''
					THEN 2
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''8''
					THEN 3
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''R''
					THEN 4
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''U''
					THEN 5
				ELSE pct.Id + 1000
				END AS Id,
			NULL AS FinancialTypeGroupId,
				CASE 
					WHEN SUBSTRING(Code, LEN(code), 1) IN (
							''U'',
							''R'',
							''P''
							)
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS IsCash,
				CASE pct.AlternateCode
					WHEN ''D''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS IsDebit,
				CONVERT(bit, 1) AS IsPrintOnStatement,
				SUBSTRING(Code, 1, (dbo.GetMax((charindex(''-'', code) - 2),0))) AS [Name],
				LetterTranslation AS ShortName,
				CONVERT(bit, 0) AS IsArchived
			FROM dbo.PracticeCodeTable pct
			WHERE pct.ReferenceType = ''PAYMENTTYPE''
			GROUP BY Id,
				Code,
				pct.AlternateCode,
				LetterTranslation
			) AS v'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.AdjustmentTypes'
			,@AdjustmentTypesViewSql
			,@isChanged = @isAdjustmentTypesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAdjustmentTypeDelete'
				,
				'
			CREATE TRIGGER model.OnAdjustmentTypeDelete ON [model].[AdjustmentTypes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @ShortName nvarchar(max)
			DECLARE @IsCash bit
			DECLARE @IsDebit bit
			DECLARE @IsPrintOnStatement bit
			DECLARE @FinancialTypeGroupId int
			DECLARE @IsArchived bit
		
			DECLARE AdjustmentTypesDeletedCursor CURSOR FOR
			SELECT Id, Name, ShortName, IsCash, IsDebit, IsPrintOnStatement, FinancialTypeGroupId, IsArchived FROM deleted
		
			OPEN AdjustmentTypesDeletedCursor
			FETCH NEXT FROM AdjustmentTypesDeletedCursor INTO @Id, @Name, @ShortName, @IsCash, @IsDebit, @IsPrintOnStatement, @FinancialTypeGroupId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for AdjustmentType --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name, @ShortName AS ShortName, @IsCash AS IsCash, @IsDebit AS IsDebit, @IsPrintOnStatement AS IsPrintOnStatement, @FinancialTypeGroupId AS FinancialTypeGroupId, @IsArchived AS IsArchived
			
				FETCH NEXT FROM AdjustmentTypesDeletedCursor INTO @Id, @Name, @ShortName, @IsCash, @IsDebit, @IsPrintOnStatement, @FinancialTypeGroupId, @IsArchived 
		
			END
		
			CLOSE AdjustmentTypesDeletedCursor
			DEALLOCATE AdjustmentTypesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAdjustmentTypeInsert'
				,
				'
			CREATE TRIGGER model.OnAdjustmentTypeInsert ON [model].[AdjustmentTypes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @ShortName nvarchar(max)
			DECLARE @IsCash bit
			DECLARE @IsDebit bit
			DECLARE @IsPrintOnStatement bit
			DECLARE @FinancialTypeGroupId int
			DECLARE @IsArchived bit
		
			DECLARE AdjustmentTypesInsertedCursor CURSOR FOR
			SELECT Id, Name, ShortName, IsCash, IsDebit, IsPrintOnStatement, FinancialTypeGroupId, IsArchived FROM inserted
		
			OPEN AdjustmentTypesInsertedCursor
			FETCH NEXT FROM AdjustmentTypesInsertedCursor INTO @Id, @Name, @ShortName, @IsCash, @IsDebit, @IsPrintOnStatement, @FinancialTypeGroupId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for AdjustmentType --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name, @ShortName AS ShortName, @IsCash AS IsCash, @IsDebit AS IsDebit, @IsPrintOnStatement AS IsPrintOnStatement, @FinancialTypeGroupId AS FinancialTypeGroupId, @IsArchived AS IsArchived
				
				FETCH NEXT FROM AdjustmentTypesInsertedCursor INTO @Id, @Name, @ShortName, @IsCash, @IsDebit, @IsPrintOnStatement, @FinancialTypeGroupId, @IsArchived 
		
			END
		
			CLOSE AdjustmentTypesInsertedCursor
			DEALLOCATE AdjustmentTypesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAdjustmentTypeUpdate'
				,
				'
			CREATE TRIGGER model.OnAdjustmentTypeUpdate ON [model].[AdjustmentTypes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @ShortName nvarchar(max)
			DECLARE @IsCash bit
			DECLARE @IsDebit bit
			DECLARE @IsPrintOnStatement bit
			DECLARE @FinancialTypeGroupId int
			DECLARE @IsArchived bit
		
			DECLARE AdjustmentTypesUpdatedCursor CURSOR FOR
			SELECT Id, Name, ShortName, IsCash, IsDebit, IsPrintOnStatement, FinancialTypeGroupId, IsArchived FROM inserted
		
			OPEN AdjustmentTypesUpdatedCursor
			FETCH NEXT FROM AdjustmentTypesUpdatedCursor INTO @Id, @Name, @ShortName, @IsCash, @IsDebit, @IsPrintOnStatement, @FinancialTypeGroupId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for AdjustmentType --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM AdjustmentTypesUpdatedCursor INTO @Id, @Name, @ShortName, @IsCash, @IsDebit, @IsPrintOnStatement, @FinancialTypeGroupId, @IsArchived 
		
			END
		
			CLOSE AdjustmentTypesUpdatedCursor
			DEALLOCATE AdjustmentTypesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Appointments')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isAppointmentsChanged BIT

		SET @isAppointmentsChanged = 0

		DECLARE @AppointmentsViewSql NVARCHAR(max)

		SET @AppointmentsViewSql = 
			'
		CREATE VIEW [model].[Appointments_Internal]
		WITH SCHEMABINDING
		AS
		----Scheduled with a User
		SELECT AppointmentId AS Id,
			ap.AppTypeId AS AppointmentTypeId,
			DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112)) AS DateTime,
			ap.Duration AS DurationMinutes,
			ap.AppointmentId AS EncounterId,
			CASE 
				WHEN ap.ScheduleStatus IN (
						''P'',
						''R'',
						''A'',
						''D''
						)
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsCanceled,
			CASE 
				WHEN (ap.AppTime > 0 AND apt.ResourceId7 <= 0 AND ap.ResourceId1 <> 0)
					THEN ap.ResourceId1
				ELSE NULL END AS UserId,
			CASE 
				WHEN (ap.AppTime > 0 AND apt.ResourceId7 > 0)
					THEN apt.AppTypeId 
				ELSE NULL END AS EquipmentId,
			NULL AS RoomId,
			CASE
				WHEN (ap.AppTime > 0 AND apt.ResourceId7 > 0)
					THEN ''EquipmentAppointment''
				ELSE ''UserAppointment'' END AS __EntityType__
		FROM dbo.Appointments ap
		INNER JOIN dbo.Resources re on re.ResourceId = ap.ResourceId1
		INNER JOIN dbo.AppointmentType apt ON ap.AppTypeId = apt.AppTypeId
		WHERE ap.AppTime > 0 
			AND ap.ResourceId1 <> 0'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Appointments_Internal'
			,@AppointmentsViewSql
			,@isChanged = @isAppointmentsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @AppointmentsViewSql = '	
			CREATE VIEW [model].[Appointments]
			WITH SCHEMABINDING
			AS
			SELECT Id, DateTime, DurationMinutes, AppointmentTypeId, EncounterId, IsCanceled, RoomId, EquipmentId, UserId, __EntityType__ FROM [model].[Appointments_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_Appointments'
							AND object_id = OBJECT_ID('[model].[Appointments_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[Appointments_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_Appointments] ON [model].[Appointments_Internal] ([Id]);
				END

				SET @AppointmentsViewSql = @AppointmentsViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Appointments'
				,@AppointmentsViewSql
				,@isChanged = @isAppointmentsChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAppointmentDelete'
				,
				'
			CREATE TRIGGER model.OnAppointmentDelete ON [model].[Appointments] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @DateTime datetime
			DECLARE @DurationMinutes int
			DECLARE @AppointmentTypeId int
			DECLARE @EncounterId int
			DECLARE @IsCanceled bit
			DECLARE @RoomId int
			DECLARE @EquipmentId int
			DECLARE @UserId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE AppointmentsDeletedCursor CURSOR FOR
			SELECT Id, DateTime, DurationMinutes, AppointmentTypeId, EncounterId, IsCanceled, RoomId, EquipmentId, UserId, __EntityType__ FROM deleted
		
			OPEN AppointmentsDeletedCursor
			FETCH NEXT FROM AppointmentsDeletedCursor INTO @Id, @DateTime, @DurationMinutes, @AppointmentTypeId, @EncounterId, @IsCanceled, @RoomId, @EquipmentId, @UserId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Appointment Delete
				DELETE
				FROM dbo.Appointments
				WHERE AppointmentId = @Id
			
				SELECT @Id AS Id, @DateTime AS DateTime, @DurationMinutes AS DurationMinutes, @AppointmentTypeId AS AppointmentTypeId, @EncounterId AS EncounterId, @IsCanceled AS IsCanceled, @RoomId AS RoomId, @EquipmentId AS EquipmentId, @UserId AS UserId, @__EntityType__ AS __EntityType__
			
				FETCH NEXT FROM AppointmentsDeletedCursor INTO @Id, @DateTime, @DurationMinutes, @AppointmentTypeId, @EncounterId, @IsCanceled, @RoomId, @EquipmentId, @UserId, @__EntityType__ 
		
			END
		
			CLOSE AppointmentsDeletedCursor
			DEALLOCATE AppointmentsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAppointmentInsert'
				,
				'
			CREATE TRIGGER model.OnAppointmentInsert ON [model].[Appointments] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @DateTime datetime
			DECLARE @DurationMinutes int
			DECLARE @AppointmentTypeId int
			DECLARE @EncounterId int
			DECLARE @IsCanceled bit
			DECLARE @RoomId int
			DECLARE @EquipmentId int
			DECLARE @UserId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE AppointmentsInsertedCursor CURSOR FOR
			SELECT Id, DateTime, DurationMinutes, AppointmentTypeId, EncounterId, IsCanceled, RoomId, EquipmentId, UserId, __EntityType__ FROM inserted
		
			OPEN AppointmentsInsertedCursor
			FETCH NEXT FROM AppointmentsInsertedCursor INTO @Id, @DateTime, @DurationMinutes, @AppointmentTypeId, @EncounterId, @IsCanceled, @RoomId, @EquipmentId, @UserId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- Appointment Insert
			
				-- Encounter will always be created already, so we don''t want to insert a new dbo.Appointments row
				SET @Id = @EncounterId
			
							
				-- Appointment Update/Insert
			
				-- @Id is a composite key...so we need to decompose it
				IF @__EntityType__ = ''EquipmentAppointment''
				BEGIN
					UPDATE dbo.Appointments
					SET AppTypeId = @EquipmentId
					WHERE AppointmentId = @Id
				END
				ELSE
				BEGIN
					UPDATE dbo.Appointments
					SET AppTypeId = @AppointmentTypeId,
						AppDate = CASE @DateTime
							WHEN NULL
								THEN CONVERT(VARCHAR, getdate(), 112)
							ELSE CONVERT(VARCHAR, @DateTime, 112)
							END,
						AppTime = DATEPART(hh, @DateTime)* 60 + DATEPART(mi, @DateTime),
						Duration = @DurationMinutes,		
						ResourceId1 = @UserId
					WHERE AppointmentId = @Id
				END
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @DateTime AS DateTime, @DurationMinutes AS DurationMinutes, @AppointmentTypeId AS AppointmentTypeId, @EncounterId AS EncounterId, @IsCanceled AS IsCanceled, @RoomId AS RoomId, @EquipmentId AS EquipmentId, @UserId AS UserId, @__EntityType__ AS __EntityType__
				
				FETCH NEXT FROM AppointmentsInsertedCursor INTO @Id, @DateTime, @DurationMinutes, @AppointmentTypeId, @EncounterId, @IsCanceled, @RoomId, @EquipmentId, @UserId, @__EntityType__ 
		
			END
		
			CLOSE AppointmentsInsertedCursor
			DEALLOCATE AppointmentsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAppointmentUpdate'
				,
				'
			CREATE TRIGGER model.OnAppointmentUpdate ON [model].[Appointments] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @DateTime datetime
			DECLARE @DurationMinutes int
			DECLARE @AppointmentTypeId int
			DECLARE @EncounterId int
			DECLARE @IsCanceled bit
			DECLARE @RoomId int
			DECLARE @EquipmentId int
			DECLARE @UserId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE AppointmentsUpdatedCursor CURSOR FOR
			SELECT Id, DateTime, DurationMinutes, AppointmentTypeId, EncounterId, IsCanceled, RoomId, EquipmentId, UserId, __EntityType__ FROM inserted
		
			OPEN AppointmentsUpdatedCursor
			FETCH NEXT FROM AppointmentsUpdatedCursor INTO @Id, @DateTime, @DurationMinutes, @AppointmentTypeId, @EncounterId, @IsCanceled, @RoomId, @EquipmentId, @UserId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- Appointment Update/Insert
			
				-- @Id is a composite key...so we need to decompose it
				IF @__EntityType__ = ''EquipmentAppointment''
				BEGIN
					UPDATE dbo.Appointments
					SET AppTypeId = @EquipmentId
					WHERE AppointmentId = @Id
				END
				ELSE
				BEGIN
					UPDATE dbo.Appointments
					SET AppTypeId = @AppointmentTypeId,
						AppDate = CASE @DateTime
							WHEN NULL
								THEN CONVERT(VARCHAR, getdate(), 112)
							ELSE CONVERT(VARCHAR, @DateTime, 112)
							END,
						AppTime = DATEPART(hh, @DateTime)* 60 + DATEPART(mi, @DateTime),
						Duration = @DurationMinutes,		
						ResourceId1 = @UserId
					WHERE AppointmentId = @Id
				END
				
				FETCH NEXT FROM AppointmentsUpdatedCursor INTO @Id, @DateTime, @DurationMinutes, @AppointmentTypeId, @EncounterId, @IsCanceled, @RoomId, @EquipmentId, @UserId, @__EntityType__ 
		
			END
		
			CLOSE AppointmentsUpdatedCursor
			DEALLOCATE AppointmentsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.AppointmentTypes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isAppointmentTypesChanged BIT

		SET @isAppointmentTypesChanged = 0

		DECLARE @ismodelAppointmentTypes_Partition1Changed BIT

		SET @ismodelAppointmentTypes_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.AppointmentTypes_Partition1'
			,'
		CREATE VIEW model.AppointmentTypes_Partition1
		WITH SCHEMABINDING
		AS
		--Change AppointemntCategory to NON-Nullable when screens are built!
		SELECT apt.AppTypeId AS Id
			,1 AS EncounterTypeId
			,apt.AppointmentType AS [Name]
			,apt.ResourceId6 AS OleColor
			,pct.Id AS PatientQuestionSetId
			,CONVERT(bit, 0) AS IsArchived
			,AppointmentCategoryId AS AppointmentCategoryId
		FROM dbo.AppointmentType apt
		INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''QUESTIONSETS''
			AND pct.Code = apt.Question
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		WHERE pic.FieldValue <> ''T''
			OR (
				pic.FieldValue = ''T''
				AND apt.ResourceId8 = 0
				)
		'
			,@isChanged = @ismodelAppointmentTypes_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_AppointmentTypes_Partition1'
					AND object_id = OBJECT_ID('model.AppointmentTypes_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.AppointmentTypes_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_AppointmentTypes_Partition1 ON model.AppointmentTypes_Partition1 ([Id]);
		END

		DECLARE @ismodelAppointmentTypes_Partition2Changed BIT

		SET @ismodelAppointmentTypes_Partition2Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.AppointmentTypes_Partition2'
			,'
		CREATE VIEW model.AppointmentTypes_Partition2
		WITH SCHEMABINDING
		AS
		--ASC on/Surgery appt
		SELECT apt.AppTypeId AS Id
			,3 AS EncounterTypeId
			,apt.AppointmentType AS [Name]
			,apt.ResourceId6 AS OleColor
			,pct.Id AS PatientQuestionSetId
			,CONVERT(bit, 0) AS IsArchived
			,AppointmentCategoryId AS AppointmentCategoryId
		FROM dbo.AppointmentType apt
		INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''QUESTIONSETS''
			AND pct.Code = apt.Question
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		WHERE pic.FieldValue = ''T''
			AND apt.ResourceId8 = 1
		'
			,@isChanged = @ismodelAppointmentTypes_Partition2Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_AppointmentTypes_Partition2'
					AND object_id = OBJECT_ID('model.AppointmentTypes_Partition2')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.AppointmentTypes_Partition2'

			CREATE UNIQUE CLUSTERED INDEX PK_AppointmentTypes_Partition2 ON model.AppointmentTypes_Partition2 ([Id]);
		END

		DECLARE @AppointmentTypesViewSql NVARCHAR(max)

		SET @AppointmentTypesViewSql = 
			'
		CREATE VIEW [model].[AppointmentTypes]
		WITH SCHEMABINDING
		AS
		SELECT model.AppointmentTypes_Partition1.Id AS Id, model.AppointmentTypes_Partition1.EncounterTypeId AS EncounterTypeId, model.AppointmentTypes_Partition1.[Name] AS [Name], model.AppointmentTypes_Partition1.OleColor AS OleColor, model.AppointmentTypes_Partition1.PatientQuestionSetId AS PatientQuestionSetId, model.AppointmentTypes_Partition1.IsArchived AS IsArchived, model.AppointmentTypes_Partition1.AppointmentCategoryId AS AppointmentCategoryId FROM model.AppointmentTypes_Partition1 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.AppointmentTypes_Partition2.Id AS Id, model.AppointmentTypes_Partition2.EncounterTypeId AS EncounterTypeId, model.AppointmentTypes_Partition2.[Name] AS [Name], model.AppointmentTypes_Partition2.OleColor AS OleColor, model.AppointmentTypes_Partition2.PatientQuestionSetId AS PatientQuestionSetId, model.AppointmentTypes_Partition2.IsArchived AS IsArchived, model.AppointmentTypes_Partition2.AppointmentCategoryId AS AppointmentCategoryId FROM model.AppointmentTypes_Partition2 WITH(NOEXPAND)'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.AppointmentTypes'
			,@AppointmentTypesViewSql
			,@isChanged = @isAppointmentTypesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAppointmentTypeDelete'
				,
				'
			CREATE TRIGGER model.OnAppointmentTypeDelete ON [model].[AppointmentTypes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(32)
			DECLARE @OleColor int
			DECLARE @EncounterTypeId int
			DECLARE @PatientQuestionSetId int
			DECLARE @IsArchived bit
			DECLARE @AppointmentCategoryId int
		
			DECLARE AppointmentTypesDeletedCursor CURSOR FOR
			SELECT Id, Name, OleColor, EncounterTypeId, PatientQuestionSetId, IsArchived, AppointmentCategoryId FROM deleted
		
			OPEN AppointmentTypesDeletedCursor
			FETCH NEXT FROM AppointmentTypesDeletedCursor INTO @Id, @Name, @OleColor, @EncounterTypeId, @PatientQuestionSetId, @IsArchived, @AppointmentCategoryId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- AppointmentType Delete
				DELETE
				FROM dbo.AppointmentType
				WHERE AppTypeId = @Id
			
				SELECT @Id AS Id, @Name AS Name, @OleColor AS OleColor, @EncounterTypeId AS EncounterTypeId, @PatientQuestionSetId AS PatientQuestionSetId, @IsArchived AS IsArchived, @AppointmentCategoryId AS AppointmentCategoryId
			
				FETCH NEXT FROM AppointmentTypesDeletedCursor INTO @Id, @Name, @OleColor, @EncounterTypeId, @PatientQuestionSetId, @IsArchived, @AppointmentCategoryId 
		
			END
		
			CLOSE AppointmentTypesDeletedCursor
			DEALLOCATE AppointmentTypesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAppointmentTypeInsert'
				,
				'
			CREATE TRIGGER model.OnAppointmentTypeInsert ON [model].[AppointmentTypes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(32)
			DECLARE @OleColor int
			DECLARE @EncounterTypeId int
			DECLARE @PatientQuestionSetId int
			DECLARE @IsArchived bit
			DECLARE @AppointmentCategoryId int
		
			DECLARE AppointmentTypesInsertedCursor CURSOR FOR
			SELECT Id, Name, OleColor, EncounterTypeId, PatientQuestionSetId, IsArchived, AppointmentCategoryId FROM inserted
		
			OPEN AppointmentTypesInsertedCursor
			FETCH NEXT FROM AppointmentTypesInsertedCursor INTO @Id, @Name, @OleColor, @EncounterTypeId, @PatientQuestionSetId, @IsArchived, @AppointmentCategoryId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- AppointmentType Insert
				INSERT INTO dbo.AppointmentType (Question)
				VALUES ('''')
			
				SET @Id = IDENT_CURRENT(''AppointmentType'')
			
							
				-- AppointmentType Update/Insert
				UPDATE dbo.AppointmentType
				SET AppointmentType = @Name,
					ResourceId6 = @OleColor,
					Question = (
						SELECT TOP 1 code
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientQuestionSetId
							AND ReferenceType = ''QUESTIONSETS''
						)
				WHERE AppTypeId = @Id
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @Name AS Name, @OleColor AS OleColor, @EncounterTypeId AS EncounterTypeId, @PatientQuestionSetId AS PatientQuestionSetId, @IsArchived AS IsArchived, @AppointmentCategoryId AS AppointmentCategoryId
				
				FETCH NEXT FROM AppointmentTypesInsertedCursor INTO @Id, @Name, @OleColor, @EncounterTypeId, @PatientQuestionSetId, @IsArchived, @AppointmentCategoryId 
		
			END
		
			CLOSE AppointmentTypesInsertedCursor
			DEALLOCATE AppointmentTypesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAppointmentTypeUpdate'
				,
				'
			CREATE TRIGGER model.OnAppointmentTypeUpdate ON [model].[AppointmentTypes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(32)
			DECLARE @OleColor int
			DECLARE @EncounterTypeId int
			DECLARE @PatientQuestionSetId int
			DECLARE @IsArchived bit
			DECLARE @AppointmentCategoryId int
		
			DECLARE AppointmentTypesUpdatedCursor CURSOR FOR
			SELECT Id, Name, OleColor, EncounterTypeId, PatientQuestionSetId, IsArchived, AppointmentCategoryId FROM inserted
		
			OPEN AppointmentTypesUpdatedCursor
			FETCH NEXT FROM AppointmentTypesUpdatedCursor INTO @Id, @Name, @OleColor, @EncounterTypeId, @PatientQuestionSetId, @IsArchived, @AppointmentCategoryId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- AppointmentType Update/Insert
				UPDATE dbo.AppointmentType
				SET AppointmentType = @Name,
					ResourceId6 = @OleColor,
					Question = (
						SELECT TOP 1 code
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientQuestionSetId
							AND ReferenceType = ''QUESTIONSETS''
						)
				WHERE AppTypeId = @Id
				
				FETCH NEXT FROM AppointmentTypesUpdatedCursor INTO @Id, @Name, @OleColor, @EncounterTypeId, @PatientQuestionSetId, @IsArchived, @AppointmentCategoryId 
		
			END
		
			CLOSE AppointmentTypesUpdatedCursor
			DEALLOCATE AppointmentTypesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.BillingDiagnosis')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isBillingDiagnosisChanged BIT

		SET @isBillingDiagnosisChanged = 0

		DECLARE @BillingDiagnosisViewSql NVARCHAR(max)

		SET @BillingDiagnosisViewSql = 
			'
		CREATE VIEW [model].[BillingDiagnosis_Internal]
		WITH SCHEMABINDING
		AS
		SELECT pc.ClinicalId AS Id,
			pc.AppointmentId AS InvoiceId,
			CASE Symptom
					WHEN ''1''
						THEN 1
					WHEN ''2''
						THEN 2
					WHEN ''3''
						THEN 3
					WHEN ''4''
						THEN 4
					WHEN ''5''
						THEN 5
					WHEN ''6''
						THEN 6
					WHEN ''7''
						THEN 7
					WHEN ''8''
						THEN 8
					WHEN ''9''
						THEN 9
					WHEN ''10''
						THEN 10
					WHEN ''11''
						THEN 11
					WHEN ''12''
						THEN 12
					ELSE NULL END AS OrdinalId,
			SUBSTRING(FindingDetail, 1, CASE 
					WHEN (dbo.GetMax((CHARINDEX(''.'', FindingDetail, 5)),0)) > 0
						THEN (dbo.GetMax((CHARINDEX(''.'', FindingDetail, 5) - 1),0))
					ELSE LEN(FindingDetail)
					END) AS Icd9DiagnosisCode,
					COUNT_BIG(*) AS Count
		FROM dbo.PatientClinical pc
		INNER JOIN dbo.Appointments ap on ap.AppointmentId = pc.AppointmentId 
			AND ap.ScheduleStatus <> ''A''
		INNER JOIN dbo.PatientReceivables pr on pr.AppointmentId = pc.AppointmentId
		WHERE pc.ClinicalType IN (
				''B'',
				''K''
				)
		AND pc.[Status] = ''A''
		AND pc.AppointmentId <> 0
		GROUP BY ap.AppointmentId, 
		pc.ClinicalId, 	
		pc.AppointmentId, 
		CASE Symptom
					WHEN ''1''
						THEN 1
					WHEN ''2''
						THEN 2
					WHEN ''3''
						THEN 3
					WHEN ''4''
						THEN 4
					WHEN ''5''
						THEN 5
					WHEN ''6''
						THEN 6
					WHEN ''7''
						THEN 7
					WHEN ''8''
						THEN 8
					WHEN ''9''
						THEN 9
					WHEN ''10''
						THEN 10
					WHEN ''11''
						THEN 11
					WHEN ''12''
						THEN 12
					ELSE NULL END,
			SUBSTRING(FindingDetail, 1, CASE 
					WHEN (dbo.GetMax((CHARINDEX(''.'', FindingDetail, 5)),0)) > 0
						THEN (dbo.GetMax((CHARINDEX(''.'', FindingDetail, 5) - 1),0))
					ELSE LEN(FindingDetail)
					END)'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.BillingDiagnosis_Internal'
			,@BillingDiagnosisViewSql
			,@isChanged = @isBillingDiagnosisChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @BillingDiagnosisViewSql = '	
			CREATE VIEW [model].[BillingDiagnosis]
			WITH SCHEMABINDING
			AS
			SELECT Id, OrdinalId, InvoiceId, Icd9DiagnosisCode FROM [model].[BillingDiagnosis_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_BillingDiagnosis'
							AND object_id = OBJECT_ID('[model].[BillingDiagnosis_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[BillingDiagnosis_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_BillingDiagnosis] ON [model].[BillingDiagnosis_Internal] ([Id]);
				END

				SET @BillingDiagnosisViewSql = @BillingDiagnosisViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.BillingDiagnosis'
				,@BillingDiagnosisViewSql
				,@isChanged = @isBillingDiagnosisChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingDiagnosisDelete'
				,'
			CREATE TRIGGER model.OnBillingDiagnosisDelete ON [model].[BillingDiagnosis] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @OrdinalId int
			DECLARE @InvoiceId int
			DECLARE @Icd9DiagnosisCode nvarchar(max)
		
			DECLARE BillingDiagnosisDeletedCursor CURSOR FOR
			SELECT Id, OrdinalId, InvoiceId, Icd9DiagnosisCode FROM deleted
		
			OPEN BillingDiagnosisDeletedCursor
			FETCH NEXT FROM BillingDiagnosisDeletedCursor INTO @Id, @OrdinalId, @InvoiceId, @Icd9DiagnosisCode 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for BillingDiagnosis --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @OrdinalId AS OrdinalId, @InvoiceId AS InvoiceId, @Icd9DiagnosisCode AS Icd9DiagnosisCode
			
				FETCH NEXT FROM BillingDiagnosisDeletedCursor INTO @Id, @OrdinalId, @InvoiceId, @Icd9DiagnosisCode 
		
			END
		
			CLOSE BillingDiagnosisDeletedCursor
			DEALLOCATE BillingDiagnosisDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingDiagnosisInsert'
				,
				'
			CREATE TRIGGER model.OnBillingDiagnosisInsert ON [model].[BillingDiagnosis] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @OrdinalId int
			DECLARE @InvoiceId int
			DECLARE @Icd9DiagnosisCode nvarchar(max)
		
			DECLARE BillingDiagnosisInsertedCursor CURSOR FOR
			SELECT Id, OrdinalId, InvoiceId, Icd9DiagnosisCode FROM inserted
		
			OPEN BillingDiagnosisInsertedCursor
			FETCH NEXT FROM BillingDiagnosisInsertedCursor INTO @Id, @OrdinalId, @InvoiceId, @Icd9DiagnosisCode 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for BillingDiagnosis --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @OrdinalId AS OrdinalId, @InvoiceId AS InvoiceId, @Icd9DiagnosisCode AS Icd9DiagnosisCode
				
				FETCH NEXT FROM BillingDiagnosisInsertedCursor INTO @Id, @OrdinalId, @InvoiceId, @Icd9DiagnosisCode 
		
			END
		
			CLOSE BillingDiagnosisInsertedCursor
			DEALLOCATE BillingDiagnosisInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingDiagnosisUpdate'
				,'
			CREATE TRIGGER model.OnBillingDiagnosisUpdate ON [model].[BillingDiagnosis] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @OrdinalId int
			DECLARE @InvoiceId int
			DECLARE @Icd9DiagnosisCode nvarchar(max)
		
			DECLARE BillingDiagnosisUpdatedCursor CURSOR FOR
			SELECT Id, OrdinalId, InvoiceId, Icd9DiagnosisCode FROM inserted
		
			OPEN BillingDiagnosisUpdatedCursor
			FETCH NEXT FROM BillingDiagnosisUpdatedCursor INTO @Id, @OrdinalId, @InvoiceId, @Icd9DiagnosisCode 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for BillingDiagnosis --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM BillingDiagnosisUpdatedCursor INTO @Id, @OrdinalId, @InvoiceId, @Icd9DiagnosisCode 
		
			END
		
			CLOSE BillingDiagnosisUpdatedCursor
			DEALLOCATE BillingDiagnosisUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.BillingOrganizations')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isBillingOrganizationsChanged BIT

		SET @isBillingOrganizationsChanged = 0

		DECLARE @ismodelBillingOrganizations_Partition1Changed BIT

		SET @ismodelBillingOrganizations_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.BillingOrganizations_Partition1'
			,'
		CREATE VIEW model.BillingOrganizations_Partition1
		WITH SCHEMABINDING
		AS
		----From PracticeName table
		SELECT pn.PracticeId AS Id,
			CONVERT(bit,0) AS IsArchived,
			CONVERT(bit,0) AS IsExternal,
			CASE pn.LocationReference
				WHEN ''''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsMain,
			PracticeName AS Name,
			CASE LocationReference
				WHEN ''''
					THEN ''Office''
				ELSE ''Office-'' + LocationReference
				END AS ShortName,
			CONVERT(NVARCHAR, NULL) AS LastName,
			CONVERT(NVARCHAR, NULL) AS FirstName,
			CONVERT(NVARCHAR, NULL) AS MiddleName,
			CONVERT(NVARCHAR, NULL) AS Suffix,
			''BillingOrganization'' AS __EntityType__
		FROM dbo.PracticeName pn
		WHERE pn.PracticeType = ''P''
		'
			,@isChanged = @ismodelBillingOrganizations_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_BillingOrganizations_Partition1'
					AND object_id = OBJECT_ID('model.BillingOrganizations_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.BillingOrganizations_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_BillingOrganizations_Partition1 ON model.BillingOrganizations_Partition1 ([Id]);
		END

		DECLARE @BillingOrganizationsViewSql NVARCHAR(max)

		SET @BillingOrganizationsViewSql = 
			'
		CREATE VIEW [model].[BillingOrganizations]
		WITH SCHEMABINDING
		AS
		SELECT model.BillingOrganizations_Partition1.Id AS Id, model.BillingOrganizations_Partition1.IsArchived AS IsArchived, model.BillingOrganizations_Partition1.IsExternal AS IsExternal, model.BillingOrganizations_Partition1.IsMain AS IsMain, model.BillingOrganizations_Partition1.Name AS Name, model.BillingOrganizations_Partition1.ShortName AS ShortName, model.BillingOrganizations_Partition1.LastName AS LastName, model.BillingOrganizations_Partition1.FirstName AS FirstName, model.BillingOrganizations_Partition1.MiddleName AS MiddleName, model.BillingOrganizations_Partition1.Suffix AS Suffix, model.BillingOrganizations_Partition1.__EntityType__ AS __EntityType__ FROM model.BillingOrganizations_Partition1 WITH(NOEXPAND)
	
		UNION ALL
	
		----BillingOrganization From Resources table - doctors w OrgOverride/Override only
		----has duplicates because of Override/OrgOverride
		SELECT (110000000 * 1 + v.ResourceId) AS Id,
			v.IsArchived,
			v.IsExternal,
			v.IsMain,
			v.Name,
			v.ShortName,
			v.LastName,
			v.FirstName,
			v.MiddleName,
			v.Suffix,
			v.__EntityType__
		FROM (SELECT re.ResourceId AS ResourceId,
				CONVERT(bit,0) AS IsArchived,
				CASE 
					WHEN re.ResourceType = ''R''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS IsExternal,
				CONVERT(bit, 0) AS IsMain,
				ResourceDescription AS [Name],
				ResourceName AS ShortName,
				ResourceLastName AS LastName,
				ResourceFirstName AS FirstName,
				ResourceMI AS MiddleName,
				ResourceSuffix AS Suffix,
				''PersonBillingOrganization'' AS __EntityType__,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources re
			LEFT OUTER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
			WHERE re.ResourceType in (''D'', ''Q'', ''Z'', ''Y'') 
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			GROUP BY re.ResourceDescription,
				re.ResourceName,
				re.ResourceLastName,
				re.ResourceFirstName,
				re.ResourceMI,
				re.ResourceSuffix,
				re.ResourceId,
				re.Billable,
				re.ResourceType
			) AS v'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.BillingOrganizations'
			,@BillingOrganizationsViewSql
			,@isChanged = @isBillingOrganizationsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingOrganizationDelete'
				,
				'
			CREATE TRIGGER model.OnBillingOrganizationDelete ON [model].[BillingOrganizations] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @ShortName nvarchar(max)
			DECLARE @IsArchived bit
			DECLARE @IsMain bit
			DECLARE @FirstName nvarchar(max)
			DECLARE @LastName nvarchar(max)
			DECLARE @MiddleName nvarchar(max)
			DECLARE @Suffix nvarchar(max)
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE BillingOrganizationsDeletedCursor CURSOR FOR
			SELECT Id, Name, ShortName, IsArchived, IsMain, FirstName, LastName, MiddleName, Suffix, __EntityType__ FROM deleted
		
			OPEN BillingOrganizationsDeletedCursor
			FETCH NEXT FROM BillingOrganizationsDeletedCursor INTO @Id, @Name, @ShortName, @IsArchived, @IsMain, @FirstName, @LastName, @MiddleName, @Suffix, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- BillingOrganization Delete
				DELETE
				FROM dbo.PracticeName
				WHERE PracticeId = @Id
			
				SELECT @Id AS Id, @Name AS Name, @ShortName AS ShortName, @IsArchived AS IsArchived, @IsMain AS IsMain, @FirstName AS FirstName, @LastName AS LastName, @MiddleName AS MiddleName, @Suffix AS Suffix, @__EntityType__ AS __EntityType__
			
				FETCH NEXT FROM BillingOrganizationsDeletedCursor INTO @Id, @Name, @ShortName, @IsArchived, @IsMain, @FirstName, @LastName, @MiddleName, @Suffix, @__EntityType__ 
		
			END
		
			CLOSE BillingOrganizationsDeletedCursor
			DEALLOCATE BillingOrganizationsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingOrganizationInsert'
				,
				'
			CREATE TRIGGER model.OnBillingOrganizationInsert ON [model].[BillingOrganizations] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @ShortName nvarchar(max)
			DECLARE @IsArchived bit
			DECLARE @IsMain bit
			DECLARE @FirstName nvarchar(max)
			DECLARE @LastName nvarchar(max)
			DECLARE @MiddleName nvarchar(max)
			DECLARE @Suffix nvarchar(max)
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE BillingOrganizationsInsertedCursor CURSOR FOR
			SELECT Id, Name, ShortName, IsArchived, IsMain, FirstName, LastName, MiddleName, Suffix, __EntityType__ FROM inserted
		
			OPEN BillingOrganizationsInsertedCursor
			FETCH NEXT FROM BillingOrganizationsInsertedCursor INTO @Id, @Name, @ShortName, @IsArchived, @IsMain, @FirstName, @LastName, @MiddleName, @Suffix, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- BillingOrganization Insert
				INSERT INTO dbo.PracticeName (PracticeName)
				VALUES ('''')
			
				SET @Id = IDENT_CURRENT(''PracticeName'')
			
							
				-- BillingOrganization Update/Insert
				UPDATE dbo.PracticeName
				SET PracticeName = @Name	
					,PracticeType = ''P''
					,LocationReference = CASE @IsMain WHEN 1 THEN '''' ELSE RTRIM(LTRIM(REPLACE(@ShortName, ''office-'', ''''))) END
				WHERE PracticeId = @Id
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @Name AS Name, @ShortName AS ShortName, @IsArchived AS IsArchived, @IsMain AS IsMain, @FirstName AS FirstName, @LastName AS LastName, @MiddleName AS MiddleName, @Suffix AS Suffix, @__EntityType__ AS __EntityType__
				
				FETCH NEXT FROM BillingOrganizationsInsertedCursor INTO @Id, @Name, @ShortName, @IsArchived, @IsMain, @FirstName, @LastName, @MiddleName, @Suffix, @__EntityType__ 
		
			END
		
			CLOSE BillingOrganizationsInsertedCursor
			DEALLOCATE BillingOrganizationsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingOrganizationUpdate'
				,
				'
			CREATE TRIGGER model.OnBillingOrganizationUpdate ON [model].[BillingOrganizations] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @ShortName nvarchar(max)
			DECLARE @IsArchived bit
			DECLARE @IsMain bit
			DECLARE @FirstName nvarchar(max)
			DECLARE @LastName nvarchar(max)
			DECLARE @MiddleName nvarchar(max)
			DECLARE @Suffix nvarchar(max)
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE BillingOrganizationsUpdatedCursor CURSOR FOR
			SELECT Id, Name, ShortName, IsArchived, IsMain, FirstName, LastName, MiddleName, Suffix, __EntityType__ FROM inserted
		
			OPEN BillingOrganizationsUpdatedCursor
			FETCH NEXT FROM BillingOrganizationsUpdatedCursor INTO @Id, @Name, @ShortName, @IsArchived, @IsMain, @FirstName, @LastName, @MiddleName, @Suffix, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- BillingOrganization Update/Insert
				UPDATE dbo.PracticeName
				SET PracticeName = @Name	
					,PracticeType = ''P''
					,LocationReference = CASE @IsMain WHEN 1 THEN '''' ELSE RTRIM(LTRIM(REPLACE(@ShortName, ''office-'', ''''))) END
				WHERE PracticeId = @Id
				
				FETCH NEXT FROM BillingOrganizationsUpdatedCursor INTO @Id, @Name, @ShortName, @IsArchived, @IsMain, @FirstName, @LastName, @MiddleName, @Suffix, @__EntityType__ 
		
			END
		
			CLOSE BillingOrganizationsUpdatedCursor
			DEALLOCATE BillingOrganizationsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.BillingServiceBillingDiagnosis')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isBillingServiceBillingDiagnosisChanged BIT

		SET @isBillingServiceBillingDiagnosisChanged = 0

		DECLARE @BillingServiceBillingDiagnosisViewSql NVARCHAR(max)

		SET @BillingServiceBillingDiagnosisViewSql = 
			'
		CREATE VIEW [model].[BillingServiceBillingDiagnosis]
		WITH SCHEMABINDING
		AS
		SELECT BillingServiceId, BillingDiagnosisId, 
		CASE
			WHEN Symptom = ''1'' AND LinkedDiag = '''' THEN 1
			WHEN Symptom = ''2'' AND LinkedDiag = ''''  THEN 2
			WHEN Symptom = ''3'' AND LinkedDiag = ''''  THEN 3
			WHEN Symptom = ''4'' AND LinkedDiag = ''''  THEN 4
			WHEN SUBSTRING(LinkedDiag, 1, 1) = Symptom THEN 1
			WHEN SUBSTRING(LinkedDiag, 2, 1) <> SUBSTRING(LinkedDiag, 1, 1) AND SUBSTRING(LinkedDiag, 2, 1) = Symptom THEN 2
			WHEN SUBSTRING(LinkedDiag, 3, 1) = Symptom THEN 3
			WHEN SUBSTRING(LinkedDiag, 4, 1) = Symptom THEN 4
		END
		AS OrdinalId FROM (SELECT pc.ClinicalId AS BillingDiagnosisId,
					prs.ItemId AS BillingServiceId, 
					Symptom AS Symptom, 
					LinkedDiag AS LinkedDiag, 
					COUNT_BIG(*) AS Count
				FROM dbo.PatientReceivableServices prs
				INNER JOIN dbo.PatientReceivables pr ON pr.Invoice = prs.Invoice
				INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = pr.AppointmentId
					AND ClinicalType IN (''B'', ''K'')
					AND ISNUMERIC(Symptom) = 1	
				INNER JOIN dbo.Appointments ap on ap.AppointmentId = pc.AppointmentId 
					AND ap.ScheduleStatus <> ''A''
				WHERE pc.[Status] = ''A''
					AND prs.[Status] = ''A''
					AND ((Symptom IN (''1'', ''2'', ''3'', ''4'') AND LinkedDiag = '''')
					OR SUBSTRING(LinkedDiag, 1, 1) = Symptom
					OR (SUBSTRING(LinkedDiag, 2, 1) <> SUBSTRING(LinkedDiag, 1, 1) AND SUBSTRING(LinkedDiag, 2, 1) = Symptom)
					OR SUBSTRING(LinkedDiag, 3, 1) = Symptom
					OR SUBSTRING(LinkedDiag, 4, 1) = Symptom)
				GROUP BY Symptom, ItemId, ClinicalId, LinkedDiag) AS q'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.BillingServiceBillingDiagnosis'
			,@BillingServiceBillingDiagnosisViewSql
			,@isChanged = @isBillingServiceBillingDiagnosisChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceBillingDiagnosisDelete'
				,
				'
			CREATE TRIGGER model.OnBillingServiceBillingDiagnosisDelete ON [model].[BillingServiceBillingDiagnosis] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @OrdinalId int
			DECLARE @BillingServiceId int
			DECLARE @BillingDiagnosisId int
		
			DECLARE BillingServiceBillingDiagnosisDeletedCursor CURSOR FOR
			SELECT OrdinalId, BillingServiceId, BillingDiagnosisId FROM deleted
		
			OPEN BillingServiceBillingDiagnosisDeletedCursor
			FETCH NEXT FROM BillingServiceBillingDiagnosisDeletedCursor INTO @OrdinalId, @BillingServiceId, @BillingDiagnosisId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for BillingServiceBillingDiagnosis --
				PRINT (''Not Implemented'')
			
				SELECT @BillingServiceId AS BillingServiceId, @BillingDiagnosisId AS BillingDiagnosisId, @OrdinalId AS OrdinalId
			
				FETCH NEXT FROM BillingServiceBillingDiagnosisDeletedCursor INTO @OrdinalId, @BillingServiceId, @BillingDiagnosisId 
		
			END
		
			CLOSE BillingServiceBillingDiagnosisDeletedCursor
			DEALLOCATE BillingServiceBillingDiagnosisDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceBillingDiagnosisInsert'
				,
				'
			CREATE TRIGGER model.OnBillingServiceBillingDiagnosisInsert ON [model].[BillingServiceBillingDiagnosis] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @OrdinalId int
			DECLARE @BillingServiceId int
			DECLARE @BillingDiagnosisId int
		
			DECLARE BillingServiceBillingDiagnosisInsertedCursor CURSOR FOR
			SELECT OrdinalId, BillingServiceId, BillingDiagnosisId FROM inserted
		
			OPEN BillingServiceBillingDiagnosisInsertedCursor
			FETCH NEXT FROM BillingServiceBillingDiagnosisInsertedCursor INTO @OrdinalId, @BillingServiceId, @BillingDiagnosisId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for BillingServiceBillingDiagnosis --
				PRINT (''Not Implemented'')
			
				SET @BillingServiceId = CONVERT(int, NULL)
				SET @BillingDiagnosisId = CONVERT(int, NULL)
			
				SELECT @BillingServiceId AS BillingServiceId, @BillingDiagnosisId AS BillingDiagnosisId, @OrdinalId AS OrdinalId
				
				FETCH NEXT FROM BillingServiceBillingDiagnosisInsertedCursor INTO @OrdinalId, @BillingServiceId, @BillingDiagnosisId 
		
			END
		
			CLOSE BillingServiceBillingDiagnosisInsertedCursor
			DEALLOCATE BillingServiceBillingDiagnosisInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceBillingDiagnosisUpdate'
				,'
			CREATE TRIGGER model.OnBillingServiceBillingDiagnosisUpdate ON [model].[BillingServiceBillingDiagnosis] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @OrdinalId int
			DECLARE @BillingServiceId int
			DECLARE @BillingDiagnosisId int
		
			DECLARE BillingServiceBillingDiagnosisUpdatedCursor CURSOR FOR
			SELECT OrdinalId, BillingServiceId, BillingDiagnosisId FROM inserted
		
			OPEN BillingServiceBillingDiagnosisUpdatedCursor
			FETCH NEXT FROM BillingServiceBillingDiagnosisUpdatedCursor INTO @OrdinalId, @BillingServiceId, @BillingDiagnosisId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for BillingServiceBillingDiagnosis --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM BillingServiceBillingDiagnosisUpdatedCursor INTO @OrdinalId, @BillingServiceId, @BillingDiagnosisId 
		
			END
		
			CLOSE BillingServiceBillingDiagnosisUpdatedCursor
			DEALLOCATE BillingServiceBillingDiagnosisUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.BillingServiceInsurerAllowedAmounts')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isBillingServiceInsurerAllowedAmountsChanged BIT

		SET @isBillingServiceInsurerAllowedAmountsChanged = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.BillingServiceInsurerAllowedAmounts'
			,'
		CREATE VIEW [model].[BillingServiceInsurerAllowedAmounts]
		WITH SCHEMABINDING
		AS
		SELECT CONVERT(bigint, NULL) AS Id, CONVERT(decimal(18,0), NULL) AS Units, CONVERT(decimal(18,0), NULL) AS AllowedUnitCharge, CONVERT(int, NULL) AS BillingServiceId, CONVERT(int, NULL) AS InsurerId
		-- View SQL not found for BillingServiceInsurerAllowedAmount --
		'
			,@isChanged = @isBillingServiceInsurerAllowedAmountsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceInsurerAllowedAmountDelete'
				,
				'
			CREATE TRIGGER model.OnBillingServiceInsurerAllowedAmountDelete ON [model].[BillingServiceInsurerAllowedAmounts] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @Units decimal(18,0)
			DECLARE @AllowedUnitCharge decimal(18,0)
			DECLARE @BillingServiceId int
			DECLARE @InsurerId int
		
			DECLARE BillingServiceInsurerAllowedAmountsDeletedCursor CURSOR FOR
			SELECT Id, Units, AllowedUnitCharge, BillingServiceId, InsurerId FROM deleted
		
			OPEN BillingServiceInsurerAllowedAmountsDeletedCursor
			FETCH NEXT FROM BillingServiceInsurerAllowedAmountsDeletedCursor INTO @Id, @Units, @AllowedUnitCharge, @BillingServiceId, @InsurerId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for BillingServiceInsurerAllowedAmount --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Units AS Units, @AllowedUnitCharge AS AllowedUnitCharge, @BillingServiceId AS BillingServiceId, @InsurerId AS InsurerId
			
				FETCH NEXT FROM BillingServiceInsurerAllowedAmountsDeletedCursor INTO @Id, @Units, @AllowedUnitCharge, @BillingServiceId, @InsurerId 
		
			END
		
			CLOSE BillingServiceInsurerAllowedAmountsDeletedCursor
			DEALLOCATE BillingServiceInsurerAllowedAmountsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceInsurerAllowedAmountInsert'
				,
				'
			CREATE TRIGGER model.OnBillingServiceInsurerAllowedAmountInsert ON [model].[BillingServiceInsurerAllowedAmounts] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @Units decimal(18,0)
			DECLARE @AllowedUnitCharge decimal(18,0)
			DECLARE @BillingServiceId int
			DECLARE @InsurerId int
		
			DECLARE BillingServiceInsurerAllowedAmountsInsertedCursor CURSOR FOR
			SELECT Id, Units, AllowedUnitCharge, BillingServiceId, InsurerId FROM inserted
		
			OPEN BillingServiceInsurerAllowedAmountsInsertedCursor
			FETCH NEXT FROM BillingServiceInsurerAllowedAmountsInsertedCursor INTO @Id, @Units, @AllowedUnitCharge, @BillingServiceId, @InsurerId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for BillingServiceInsurerAllowedAmount --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(bigint, NULL)
			
				SELECT @Id AS Id, @Units AS Units, @AllowedUnitCharge AS AllowedUnitCharge, @BillingServiceId AS BillingServiceId, @InsurerId AS InsurerId
				
				FETCH NEXT FROM BillingServiceInsurerAllowedAmountsInsertedCursor INTO @Id, @Units, @AllowedUnitCharge, @BillingServiceId, @InsurerId 
		
			END
		
			CLOSE BillingServiceInsurerAllowedAmountsInsertedCursor
			DEALLOCATE BillingServiceInsurerAllowedAmountsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceInsurerAllowedAmountUpdate'
				,
				'
			CREATE TRIGGER model.OnBillingServiceInsurerAllowedAmountUpdate ON [model].[BillingServiceInsurerAllowedAmounts] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @Units decimal(18,0)
			DECLARE @AllowedUnitCharge decimal(18,0)
			DECLARE @BillingServiceId int
			DECLARE @InsurerId int
		
			DECLARE BillingServiceInsurerAllowedAmountsUpdatedCursor CURSOR FOR
			SELECT Id, Units, AllowedUnitCharge, BillingServiceId, InsurerId FROM inserted
		
			OPEN BillingServiceInsurerAllowedAmountsUpdatedCursor
			FETCH NEXT FROM BillingServiceInsurerAllowedAmountsUpdatedCursor INTO @Id, @Units, @AllowedUnitCharge, @BillingServiceId, @InsurerId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for BillingServiceInsurerAllowedAmount --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM BillingServiceInsurerAllowedAmountsUpdatedCursor INTO @Id, @Units, @AllowedUnitCharge, @BillingServiceId, @InsurerId 
		
			END
		
			CLOSE BillingServiceInsurerAllowedAmountsUpdatedCursor
			DEALLOCATE BillingServiceInsurerAllowedAmountsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.BillingServiceModifiers')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isBillingServiceModifiersChanged BIT

		SET @isBillingServiceModifiersChanged = 0

		DECLARE @ismodelBillingServiceModifiers_Partition1Changed BIT

		SET @ismodelBillingServiceModifiers_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.BillingServiceModifiers_Partition1'
			,'
		CREATE VIEW model.BillingServiceModifiers_Partition1
		WITH SCHEMABINDING
		AS
		---2nd modifier
			SELECT 1 AS PartitionId
				,prs.ItemId AS BillingServiceId
				,2 AS OrdinalId
				,CASE 
					WHEN pctMod2.Id IS NOT NULL
						THEN (110000000 * 1 + pctMod2.Id)
					ELSE (110000000 * 3 + prs.ItemId)
					END AS ServiceModifierId
				,COUNT_BIG(*) AS COUNT
			FROM dbo.PatientReceivableServices prs
			INNER JOIN dbo.PracticeCodeTable pctMod2 ON SUBSTRING(pctMod2.Code, 1, 2) = SUBSTRING(modifier, 3, 2)
				AND ReferenceType = ''SERVICEMODS''
			WHERE prs.Modifier <> ''''
				AND prs.[Status] = ''A''
			GROUP BY prs.ItemId
				,pctMod2.id
		'
			,@isChanged = @ismodelBillingServiceModifiers_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_BillingServiceModifiers_Partition1'
					AND object_id = OBJECT_ID('model.BillingServiceModifiers_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.BillingServiceModifiers_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_BillingServiceModifiers_Partition1 ON model.BillingServiceModifiers_Partition1 (
				[PartitionId]
				,[BillingServiceId]
				,[OrdinalId]
				,[ServiceModifierId]
				);
		END

		DECLARE @ismodelBillingServiceModifiers_Partition2Changed BIT

		SET @ismodelBillingServiceModifiers_Partition2Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.BillingServiceModifiers_Partition2'
			,'
		CREATE VIEW model.BillingServiceModifiers_Partition2
		WITH SCHEMABINDING
		AS
		---3rd modifier
			SELECT 2 AS PartitionId
				,prs.ItemId AS BillingServiceId
				,3 AS OrdinalId
				,CASE 
					WHEN pctMod3.Id IS NOT NULL
						THEN (110000000 * 1 + pctMod3.Id)
					ELSE (110000000 * 4 + prs.ItemId)
					END AS ServiceModifierId
				,COUNT_BIG(*) AS COUNT
			FROM dbo.PatientReceivableServices prs
			INNER JOIN dbo.PracticeCodeTable pctMod3 ON SUBSTRING(pctMod3.Code, 1, 2) = SUBSTRING(modifier, 5, 2)
				AND ReferenceType = ''SERVICEMODS''
			WHERE prs.Modifier <> ''''
				AND prs.[Status] = ''A''
			GROUP BY prs.ItemId
				,pctMod3.id
		'
			,@isChanged = @ismodelBillingServiceModifiers_Partition2Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_BillingServiceModifiers_Partition2'
					AND object_id = OBJECT_ID('model.BillingServiceModifiers_Partition2')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.BillingServiceModifiers_Partition2'

			CREATE UNIQUE CLUSTERED INDEX PK_BillingServiceModifiers_Partition2 ON model.BillingServiceModifiers_Partition2 (
				[PartitionId]
				,[BillingServiceId]
				,[OrdinalId]
				,[ServiceModifierId]
				);
		END

		DECLARE @ismodelBillingServiceModifiers_Partition3Changed BIT

		SET @ismodelBillingServiceModifiers_Partition3Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.BillingServiceModifiers_Partition3'
			,'
		CREATE VIEW model.BillingServiceModifiers_Partition3
		WITH SCHEMABINDING
		AS
		---4th modifier
			SELECT 3 AS PartitionId
				,prs.ItemId AS BillingServiceId
				,4 AS OrdinalId
				,CASE 
					WHEN pctMod4.Id IS NOT NULL
						THEN (110000000 * 1 + pctMod4.Id)
					ELSE (110000000 * 2 + prs.ItemId)
					END AS ServiceModifierId
				,COUNT_BIG(*) AS COUNT
			FROM dbo.PatientReceivableServices prs
			INNER JOIN dbo.PracticeCodeTable pctMod4 ON SUBSTRING(pctMod4.Code, 1, 2) = SUBSTRING(modifier, 7, 2)
				AND ReferenceType = ''SERVICEMODS''
			WHERE prs.Modifier <> ''''
				AND prs.[Status] = ''A''
			GROUP BY prs.ItemId
				,pctMod4.id
		'
			,@isChanged = @ismodelBillingServiceModifiers_Partition3Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_BillingServiceModifiers_Partition3'
					AND object_id = OBJECT_ID('model.BillingServiceModifiers_Partition3')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.BillingServiceModifiers_Partition3'

			CREATE UNIQUE CLUSTERED INDEX PK_BillingServiceModifiers_Partition3 ON model.BillingServiceModifiers_Partition3 (
				[PartitionId]
				,[BillingServiceId]
				,[OrdinalId]
				,[ServiceModifierId]
				);
		END

		DECLARE @BillingServiceModifiersViewSql NVARCHAR(max)

		SET @BillingServiceModifiersViewSql = 
			'
		CREATE VIEW [model].[BillingServiceModifiers]
		WITH SCHEMABINDING
		AS
		---First modifier
		SELECT CASE v.PartitionId
				WHEN 0
					THEN v.BillingServiceId 
				WHEN 1
					THEN (110000000 * 1 + v.BillingServiceId)
				WHEN 2
					THEN (110000000 * 2 + v.BillingServiceId)
				WHEN 3
					THEN (110000000 * 3 + v.BillingServiceId)
				END AS Id
			,v.BillingServiceId
			,v.OrdinalId
			,v.ServiceModifierId
		FROM (
			SELECT 0 AS PartitionId
				,prs.ItemId AS BillingServiceId
				,1 AS OrdinalId
				,CASE 
					WHEN pctMod1.Id IS NOT NULL
						THEN (110000000 * 1 + pctMod1.Id) 
					ELSE (110000000 * 2 + prs.ItemId)
					END AS ServiceModifierId
				,COUNT_BIG(*) AS COUNT
			FROM dbo.PatientReceivableServices prs
			LEFT JOIN dbo.PracticeCodeTable pctMod1 ON SUBSTRING(pctMod1.Code, 1, 2) = SUBSTRING(modifier, 1, 2)
				AND ReferenceType = ''SERVICEMODS''
			WHERE prs.Modifier <> ''''
				AND prs.[Status] = ''A''
			GROUP BY prs.ItemId
				,pctMod1.id
		
			UNION ALL
	
		SELECT model.BillingServiceModifiers_Partition1.PartitionId AS PartitionId, model.BillingServiceModifiers_Partition1.BillingServiceId AS BillingServiceId, model.BillingServiceModifiers_Partition1.OrdinalId AS OrdinalId, model.BillingServiceModifiers_Partition1.ServiceModifierId AS ServiceModifierId, model.BillingServiceModifiers_Partition1.COUNT AS COUNT FROM model.BillingServiceModifiers_Partition1 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.BillingServiceModifiers_Partition2.PartitionId AS PartitionId, model.BillingServiceModifiers_Partition2.BillingServiceId AS BillingServiceId, model.BillingServiceModifiers_Partition2.OrdinalId AS OrdinalId, model.BillingServiceModifiers_Partition2.ServiceModifierId AS ServiceModifierId, model.BillingServiceModifiers_Partition2.COUNT AS COUNT FROM model.BillingServiceModifiers_Partition2 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.BillingServiceModifiers_Partition3.PartitionId AS PartitionId, model.BillingServiceModifiers_Partition3.BillingServiceId AS BillingServiceId, model.BillingServiceModifiers_Partition3.OrdinalId AS OrdinalId, model.BillingServiceModifiers_Partition3.ServiceModifierId AS ServiceModifierId, model.BillingServiceModifiers_Partition3.COUNT AS COUNT FROM model.BillingServiceModifiers_Partition3 WITH(NOEXPAND)
	
		) AS v
		GROUP BY PartitionId
			,v.BillingServiceId
			,v.OrdinalId
			,v.ServiceModifierId'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.BillingServiceModifiers'
			,@BillingServiceModifiersViewSql
			,@isChanged = @isBillingServiceModifiersChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceModifierDelete'
				,
				'
			CREATE TRIGGER model.OnBillingServiceModifierDelete ON [model].[BillingServiceModifiers] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @OrdinalId int
			DECLARE @BillingServiceId int
			DECLARE @ServiceModifierId int
		
			DECLARE BillingServiceModifiersDeletedCursor CURSOR FOR
			SELECT Id, OrdinalId, BillingServiceId, ServiceModifierId FROM deleted
		
			OPEN BillingServiceModifiersDeletedCursor
			FETCH NEXT FROM BillingServiceModifiersDeletedCursor INTO @Id, @OrdinalId, @BillingServiceId, @ServiceModifierId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for BillingServiceModifier --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @OrdinalId AS OrdinalId, @BillingServiceId AS BillingServiceId, @ServiceModifierId AS ServiceModifierId
			
				FETCH NEXT FROM BillingServiceModifiersDeletedCursor INTO @Id, @OrdinalId, @BillingServiceId, @ServiceModifierId 
		
			END
		
			CLOSE BillingServiceModifiersDeletedCursor
			DEALLOCATE BillingServiceModifiersDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceModifierInsert'
				,
				'
			CREATE TRIGGER model.OnBillingServiceModifierInsert ON [model].[BillingServiceModifiers] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @OrdinalId int
			DECLARE @BillingServiceId int
			DECLARE @ServiceModifierId int
		
			DECLARE BillingServiceModifiersInsertedCursor CURSOR FOR
			SELECT Id, OrdinalId, BillingServiceId, ServiceModifierId FROM inserted
		
			OPEN BillingServiceModifiersInsertedCursor
			FETCH NEXT FROM BillingServiceModifiersInsertedCursor INTO @Id, @OrdinalId, @BillingServiceId, @ServiceModifierId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for BillingServiceModifier --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @OrdinalId AS OrdinalId, @BillingServiceId AS BillingServiceId, @ServiceModifierId AS ServiceModifierId
				
				FETCH NEXT FROM BillingServiceModifiersInsertedCursor INTO @Id, @OrdinalId, @BillingServiceId, @ServiceModifierId 
		
			END
		
			CLOSE BillingServiceModifiersInsertedCursor
			DEALLOCATE BillingServiceModifiersInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceModifierUpdate'
				,'
			CREATE TRIGGER model.OnBillingServiceModifierUpdate ON [model].[BillingServiceModifiers] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @OrdinalId int
			DECLARE @BillingServiceId int
			DECLARE @ServiceModifierId int
		
			DECLARE BillingServiceModifiersUpdatedCursor CURSOR FOR
			SELECT Id, OrdinalId, BillingServiceId, ServiceModifierId FROM inserted
		
			OPEN BillingServiceModifiersUpdatedCursor
			FETCH NEXT FROM BillingServiceModifiersUpdatedCursor INTO @Id, @OrdinalId, @BillingServiceId, @ServiceModifierId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for BillingServiceModifier --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM BillingServiceModifiersUpdatedCursor INTO @Id, @OrdinalId, @BillingServiceId, @ServiceModifierId 
		
			END
		
			CLOSE BillingServiceModifiersUpdatedCursor
			DEALLOCATE BillingServiceModifiersUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.BillingServices')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isBillingServicesChanged BIT

		SET @isBillingServicesChanged = 0

		DECLARE @BillingServicesViewSql NVARCHAR(max)

		SET @BillingServicesViewSql = 
			'
		CREATE VIEW [model].[BillingServices]
		WITH SCHEMABINDING
		AS
		SELECT q.ItemId AS Id
			, q.InvoiceId
			, q.OrderingProviderId
			, q.OrdinalId
			, CONVERT(NVARCHAR, NULL) AS ProcedureCode835Description
			, q.Unit
			, q.UnitCharge
			, q.EncounterServiceId
			, NULL AS OrderingExternalProviderId
		FROM (
			SELECT ItemId AS ItemId,
				CASE 
					WHEN pr.AppointmentId  = 0
						THEN NULL
					ELSE pr.AppointmentId END AS InvoiceId,
				CASE prs.OrderDoc
					WHEN ''T''
						THEN pr.BillToDr
					ELSE 0
					END AS OrderingProviderId,
				CASE 
					WHEN ItemOrder > 0
						THEN ItemOrder
					ELSE 1
					END AS OrdinalId,
				prs.QuantityDecimal AS Unit,
				prs.ChargeDecimal AS UnitCharge,
				ps.CodeId AS EncounterServiceId,
				COUNT_BIG(*) AS Count
			FROM dbo.PatientReceivableServices prs
			INNER JOIN dbo.PatientReceivables pr ON pr.Invoice = prs.Invoice
			INNER JOIN dbo.PracticeServices ps ON ps.Code = prs.Service
			WHERE prs.[Status] = ''A''
			GROUP BY 
				prs.ItemId,
				CASE 
					WHEN pr.AppointmentId  = 0
						THEN NULL
					ELSE pr.AppointmentId END,
				CASE prs.OrderDoc
					WHEN ''T''
						THEN pr.BillToDr
					ELSE 0
					END,
				CASE 
					WHEN ItemOrder > 0
						THEN ItemOrder
					ELSE 1
					END,
				prs.QuantityDecimal,
				prs.ChargeDecimal,
				CodeId
		) q'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.BillingServices'
			,@BillingServicesViewSql
			,@isChanged = @isBillingServicesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceDelete'
				,
				'
			CREATE TRIGGER model.OnBillingServiceDelete ON [model].[BillingServices] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Unit decimal(18,0)
			DECLARE @UnitCharge decimal(18,0)
			DECLARE @OrdinalId int
			DECLARE @EncounterServiceId int
			DECLARE @InvoiceId int
			DECLARE @ProcedureCode835Description nvarchar(max)
			DECLARE @OrderingExternalProviderId int
		
			DECLARE BillingServicesDeletedCursor CURSOR FOR
			SELECT Id, Unit, UnitCharge, OrdinalId, EncounterServiceId, InvoiceId, ProcedureCode835Description, OrderingExternalProviderId FROM deleted
		
			OPEN BillingServicesDeletedCursor
			FETCH NEXT FROM BillingServicesDeletedCursor INTO @Id, @Unit, @UnitCharge, @OrdinalId, @EncounterServiceId, @InvoiceId, @ProcedureCode835Description, @OrderingExternalProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for BillingService --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Unit AS Unit, @UnitCharge AS UnitCharge, @OrdinalId AS OrdinalId, @EncounterServiceId AS EncounterServiceId, @InvoiceId AS InvoiceId, @ProcedureCode835Description AS ProcedureCode835Description, @OrderingExternalProviderId AS OrderingExternalProviderId
			
				FETCH NEXT FROM BillingServicesDeletedCursor INTO @Id, @Unit, @UnitCharge, @OrdinalId, @EncounterServiceId, @InvoiceId, @ProcedureCode835Description, @OrderingExternalProviderId 
		
			END
		
			CLOSE BillingServicesDeletedCursor
			DEALLOCATE BillingServicesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceInsert'
				,
				'
			CREATE TRIGGER model.OnBillingServiceInsert ON [model].[BillingServices] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Unit decimal(18,0)
			DECLARE @UnitCharge decimal(18,0)
			DECLARE @OrdinalId int
			DECLARE @EncounterServiceId int
			DECLARE @InvoiceId int
			DECLARE @ProcedureCode835Description nvarchar(max)
			DECLARE @OrderingExternalProviderId int
		
			DECLARE BillingServicesInsertedCursor CURSOR FOR
			SELECT Id, Unit, UnitCharge, OrdinalId, EncounterServiceId, InvoiceId, ProcedureCode835Description, OrderingExternalProviderId FROM inserted
		
			OPEN BillingServicesInsertedCursor
			FETCH NEXT FROM BillingServicesInsertedCursor INTO @Id, @Unit, @UnitCharge, @OrdinalId, @EncounterServiceId, @InvoiceId, @ProcedureCode835Description, @OrderingExternalProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for BillingService --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Unit AS Unit, @UnitCharge AS UnitCharge, @OrdinalId AS OrdinalId, @EncounterServiceId AS EncounterServiceId, @InvoiceId AS InvoiceId, @ProcedureCode835Description AS ProcedureCode835Description, @OrderingExternalProviderId AS OrderingExternalProviderId
				
				FETCH NEXT FROM BillingServicesInsertedCursor INTO @Id, @Unit, @UnitCharge, @OrdinalId, @EncounterServiceId, @InvoiceId, @ProcedureCode835Description, @OrderingExternalProviderId 
		
			END
		
			CLOSE BillingServicesInsertedCursor
			DEALLOCATE BillingServicesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceUpdate'
				,
				'
			CREATE TRIGGER model.OnBillingServiceUpdate ON [model].[BillingServices] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Unit decimal(18,0)
			DECLARE @UnitCharge decimal(18,0)
			DECLARE @OrdinalId int
			DECLARE @EncounterServiceId int
			DECLARE @InvoiceId int
			DECLARE @ProcedureCode835Description nvarchar(max)
			DECLARE @OrderingExternalProviderId int
		
			DECLARE BillingServicesUpdatedCursor CURSOR FOR
			SELECT Id, Unit, UnitCharge, OrdinalId, EncounterServiceId, InvoiceId, ProcedureCode835Description, OrderingExternalProviderId FROM inserted
		
			OPEN BillingServicesUpdatedCursor
			FETCH NEXT FROM BillingServicesUpdatedCursor INTO @Id, @Unit, @UnitCharge, @OrdinalId, @EncounterServiceId, @InvoiceId, @ProcedureCode835Description, @OrderingExternalProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for BillingService --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM BillingServicesUpdatedCursor INTO @Id, @Unit, @UnitCharge, @OrdinalId, @EncounterServiceId, @InvoiceId, @ProcedureCode835Description, @OrderingExternalProviderId 
		
			END
		
			CLOSE BillingServicesUpdatedCursor
			DEALLOCATE BillingServicesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.BillingServiceTransactions')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isBillingServiceTransactionsChanged BIT

		SET @isBillingServiceTransactionsChanged = 0

		DECLARE @BillingServiceTransactionsViewSql NVARCHAR(max)

		SET @BillingServiceTransactionsViewSql = 
			'
		CREATE VIEW [model].[BillingServiceTransactions]
		WITH SCHEMABINDING
		AS
		SELECT st.Id AS Id,
			CONVERT(decimal(18,2), Amount) AS AmountSent,
			ServiceId AS BillingServiceId,
			CASE 
				WHEN ptj.TransactionStatus = ''P''
					AND TransportAction IN (''B'', ''P'')
					THEN 1
				WHEN ptj.TransactionStatus = ''P''
					AND TransportAction = ''V''
					THEN 3
				WHEN ptj.TransactionStatus = ''S''
					AND TransportAction = ''P''
					THEN 2
				WHEN ptj.TransactionStatus = ''S''
					AND TransportAction = ''V''
					THEN 3
				WHEN ptj.TransactionStatus = ''S''
					AND TransportAction = ''X''
					THEN 4
				WHEN ptj.TransactionStatus = ''Z''
					AND TransportAction = ''1''
					THEN 6
				WHEN ptj.TransactionStatus = ''Z''
					AND TransportAction = ''2''
					THEN 7
				WHEN ptj.TransactionStatus = ''S''
					AND TransportAction = ''B''
					THEN 8
				ELSE 5
				END AS BillingServiceTransactionStatusId,
			NULL AS ClaimFileNumber,
			CASE 
				WHEN ptj.TransactionRef = ''I'' AND ptj.TransactionAction = ''B'' 
					THEN pctMethod.Id 
				ELSE NULL 
				END AS ClaimFileReceiverId,
			CONVERT(datetime, ptj.TransactionDate, 112) AS DateTime,
			(CONVERT(bigint, 110000000) * CASE
				WHEN ptj.TransactionRef = ''P'' then 0
				ELSE pr.ComputedFinancialId
				END + pr.AppointmentId)  AS InvoiceReceivableId,
			CASE 
				WHEN (ptj.TransactionRef = ''I'' AND ptj.TransactionAction = ''B'')
					THEN 1
				ELSE 2 
				END AS MethodSentId
		FROM dbo.PracticeTransactionJournal ptj
		INNER JOIN dbo.ServiceTransactions st ON st.TransactionId = ptj.TransactionId
		INNER JOIN dbo.PatientReceivableServices prs on prs.ItemId = st.ServiceId
			AND prs.[Status] = ''A''
		INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = ptj.TransactionTypeId	
			AND pr.ComputedFinancialId IS NOT NULL
		INNER JOIN dbo.PracticeInsurers pri on pri.InsurerId = pr.InsurerId
		INNER JOIN dbo.PracticeCodeTable pctMethod ON pri.InsurerTFormat = SUBSTRING(pctMethod.Code, 1, 1) 
			AND pctMethod.ReferenceType = ''TRANSMITTYPE''
		WHERE 
		ptj.TransactionType = ''R''
			AND TransactionRef <> ''G''
			AND 
				(ptj.TransactionStatus IN (''P'', ''S'')
					OR
					(ptj.TransactionStatus = ''Z'' AND st.TransportAction <> ''0'')
				)'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.BillingServiceTransactions'
			,@BillingServiceTransactionsViewSql
			,@isChanged = @isBillingServiceTransactionsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceTransactionDelete'
				,
				'
			CREATE TRIGGER model.OnBillingServiceTransactionDelete ON [model].[BillingServiceTransactions] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id uniqueidentifier
			DECLARE @DateTime datetime
			DECLARE @AmountSent decimal(18,0)
			DECLARE @ClaimFileNumber int
			DECLARE @BillingServiceTransactionStatusId int
			DECLARE @MethodSentId int
			DECLARE @BillingServiceId int
			DECLARE @InvoiceReceivableId bigint
			DECLARE @ClaimFileReceiverId int
		
			DECLARE BillingServiceTransactionsDeletedCursor CURSOR FOR
			SELECT Id, DateTime, AmountSent, ClaimFileNumber, BillingServiceTransactionStatusId, MethodSentId, BillingServiceId, InvoiceReceivableId, ClaimFileReceiverId FROM deleted
		
			OPEN BillingServiceTransactionsDeletedCursor
			FETCH NEXT FROM BillingServiceTransactionsDeletedCursor INTO @Id, @DateTime, @AmountSent, @ClaimFileNumber, @BillingServiceTransactionStatusId, @MethodSentId, @BillingServiceId, @InvoiceReceivableId, @ClaimFileReceiverId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for BillingServiceTransaction --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @DateTime AS DateTime, @AmountSent AS AmountSent, @ClaimFileNumber AS ClaimFileNumber, @BillingServiceTransactionStatusId AS BillingServiceTransactionStatusId, @MethodSentId AS MethodSentId, @BillingServiceId AS BillingServiceId, @InvoiceReceivableId AS InvoiceReceivableId, @ClaimFileReceiverId AS ClaimFileReceiverId
			
				FETCH NEXT FROM BillingServiceTransactionsDeletedCursor INTO @Id, @DateTime, @AmountSent, @ClaimFileNumber, @BillingServiceTransactionStatusId, @MethodSentId, @BillingServiceId, @InvoiceReceivableId, @ClaimFileReceiverId 
		
			END
		
			CLOSE BillingServiceTransactionsDeletedCursor
			DEALLOCATE BillingServiceTransactionsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceTransactionInsert'
				,
				'
			CREATE TRIGGER model.OnBillingServiceTransactionInsert ON [model].[BillingServiceTransactions] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id uniqueidentifier
			DECLARE @DateTime datetime
			DECLARE @AmountSent decimal(18,0)
			DECLARE @ClaimFileNumber int
			DECLARE @BillingServiceTransactionStatusId int
			DECLARE @MethodSentId int
			DECLARE @BillingServiceId int
			DECLARE @InvoiceReceivableId bigint
			DECLARE @ClaimFileReceiverId int
		
			DECLARE BillingServiceTransactionsInsertedCursor CURSOR FOR
			SELECT Id, DateTime, AmountSent, ClaimFileNumber, BillingServiceTransactionStatusId, MethodSentId, BillingServiceId, InvoiceReceivableId, ClaimFileReceiverId FROM inserted
		
			OPEN BillingServiceTransactionsInsertedCursor
			FETCH NEXT FROM BillingServiceTransactionsInsertedCursor INTO @Id, @DateTime, @AmountSent, @ClaimFileNumber, @BillingServiceTransactionStatusId, @MethodSentId, @BillingServiceId, @InvoiceReceivableId, @ClaimFileReceiverId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for BillingServiceTransaction --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(uniqueidentifier, NULL)
			
				SELECT @Id AS Id, @DateTime AS DateTime, @AmountSent AS AmountSent, @ClaimFileNumber AS ClaimFileNumber, @BillingServiceTransactionStatusId AS BillingServiceTransactionStatusId, @MethodSentId AS MethodSentId, @BillingServiceId AS BillingServiceId, @InvoiceReceivableId AS InvoiceReceivableId, @ClaimFileReceiverId AS ClaimFileReceiverId
				
				FETCH NEXT FROM BillingServiceTransactionsInsertedCursor INTO @Id, @DateTime, @AmountSent, @ClaimFileNumber, @BillingServiceTransactionStatusId, @MethodSentId, @BillingServiceId, @InvoiceReceivableId, @ClaimFileReceiverId 
		
			END
		
			CLOSE BillingServiceTransactionsInsertedCursor
			DEALLOCATE BillingServiceTransactionsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnBillingServiceTransactionUpdate'
				,
				'
			CREATE TRIGGER model.OnBillingServiceTransactionUpdate ON [model].[BillingServiceTransactions] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id uniqueidentifier
			DECLARE @DateTime datetime
			DECLARE @AmountSent decimal(18,0)
			DECLARE @ClaimFileNumber int
			DECLARE @BillingServiceTransactionStatusId int
			DECLARE @MethodSentId int
			DECLARE @BillingServiceId int
			DECLARE @InvoiceReceivableId bigint
			DECLARE @ClaimFileReceiverId int
		
			DECLARE BillingServiceTransactionsUpdatedCursor CURSOR FOR
			SELECT Id, DateTime, AmountSent, ClaimFileNumber, BillingServiceTransactionStatusId, MethodSentId, BillingServiceId, InvoiceReceivableId, ClaimFileReceiverId FROM inserted
		
			OPEN BillingServiceTransactionsUpdatedCursor
			FETCH NEXT FROM BillingServiceTransactionsUpdatedCursor INTO @Id, @DateTime, @AmountSent, @ClaimFileNumber, @BillingServiceTransactionStatusId, @MethodSentId, @BillingServiceId, @InvoiceReceivableId, @ClaimFileReceiverId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for BillingServiceTransaction --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM BillingServiceTransactionsUpdatedCursor INTO @Id, @DateTime, @AmountSent, @ClaimFileNumber, @BillingServiceTransactionStatusId, @MethodSentId, @BillingServiceId, @InvoiceReceivableId, @ClaimFileReceiverId 
		
			END
		
			CLOSE BillingServiceTransactionsUpdatedCursor
			DEALLOCATE BillingServiceTransactionsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ClaimAdjustmentReasonAdjustmentTypes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isClaimAdjustmentReasonAdjustmentTypesChanged BIT

		SET @isClaimAdjustmentReasonAdjustmentTypesChanged = 0

		DECLARE @ClaimAdjustmentReasonAdjustmentTypesViewSql NVARCHAR(max)

		SET @ClaimAdjustmentReasonAdjustmentTypesViewSql = 
			'
		CREATE VIEW [model].[ClaimAdjustmentReasonAdjustmentTypes]
		WITH SCHEMABINDING
		AS
		---For Adjustments entity
		SELECT pctReason.Id AS Id,
			2 AS AdjustmentTypeId,
			1 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			NULL AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctAdjType.Code, 1) = ''X''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.code = ''45''
	
		UNION ALL
	
		SELECT (CONVERT(bigint, 110000000) * 1 + pctReason.Id) AS Id,
			2 AS AdjustmentTypeId,
			1 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			NULL AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctAdjType.Code, 1) = ''X''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.Code = ''42''
	
		UNION ALL
	
		SELECT (CONVERT(bigint, 110000000) * 2 + pctReason.Id) AS Id,
			2 AS AdjustmentTypeId,
			1 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			NULL AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctAdjType.Code, 1) = ''X''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.Code = ''A2''
	
		UNION ALL
	
		SELECT (CONVERT(bigint, 110000000) * 3 + pctReason.Id) AS Id,
			2 AS AdjustmentTypeId,
			1 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			NULL AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctAdjType.Code, 1) = ''X''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.Code = ''59''
	
		UNION ALL
	
		SELECT (CONVERT(bigint, 110000000) * 4 + pctReason.Id) AS Id,
			2 AS AdjustmentTypeId,
			1 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			NULL AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctAdjType.Code, 1) = ''X''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.Code = ''94''
	
		UNION ALL
	
		SELECT (CONVERT(bigint, 110000000) * 5 + pctReason.Id) AS Id,
			2 AS AdjustmentTypeId,
			1 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			NULL AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctAdjType.Code, 1) = ''X''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.Code = ''172''
	
		UNION ALL
	
		SELECT (CONVERT(bigint, 110000000) * 6 + pctReason.Id) AS Id,
			2 AS AdjustmentTypeId,
			1 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			NULL AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctAdjType.Code, 1) = ''X''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.Code = ''B10''
	
		UNION ALL
	
		SELECT (CONVERT(bigint, 110000000) * 7 + pctReason.Id) AS Id,
			2 AS AdjustmentTypeId,
			3 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			NULL AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctAdjType.Code, 1) = ''X''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.code = ''45''
	
		UNION ALL
	
		SELECT pctReason.Id AS Id,
			2 AS AdjustmentTypeId,
			1 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			NULL AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctAdjType.Code, 1) = ''X''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.code = ''237''
	
		UNION ALL
	
		---For FinancialInformation entity
		SELECT (CONVERT(bigint, 110000000) * 8 + pctReason.Id) AS Id,
			NULL AS AdjustmentTypeId,
			2 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			CASE
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''!''
					THEN 1
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''|''
					THEN 2
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''?''
					THEN 3
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''%''
					THEN 4
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''D''
					THEN 5
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''&''
					THEN 6
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '']''
					THEN 7
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''0''
					THEN 8
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''V''
					THEN 9
				ELSE pctFinType.Id + 1000
			END AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctFinType ON pctFinType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctFinType.Code, 1) = ''!''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.code = ''1''
	
		UNION ALL
	
		SELECT (CONVERT(bigint, 110000000) * 9 + pctReason.Id) AS Id,
			NULL AS AdjustmentTypeId,
			2 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			CASE
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''!''
					THEN 1
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''|''
					THEN 2
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''?''
					THEN 3
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''%''
					THEN 4
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''D''
					THEN 5
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''&''
					THEN 6
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '']''
					THEN 7
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''0''
					THEN 8
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''V''
					THEN 9
				ELSE pctFinType.Id + 1000
			END AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctFinType ON pctFinType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctFinType.Code, 1) = ''|''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.code = ''2''
	
		UNION ALL
	
		SELECT (CONVERT(bigint, 110000000) * 10 + pctReason.Id) AS Id,
			NULL AS AdjustmentTypeId,
			2 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			CASE
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''!''
					THEN 1
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''|''
					THEN 2
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''?''
					THEN 3
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''%''
					THEN 4
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''D''
					THEN 5
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''&''
					THEN 6
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '']''
					THEN 7
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''0''
					THEN 8
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''V''
					THEN 9
				ELSE pctFinType.Id + 1000
			END AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctFinType ON pctFinType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctFinType.Code, 1) = ''|''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.code = ''3''
	
		UNION ALL
	
		SELECT (CONVERT(bigint, 110000000) * 11 + pctReason.Id) AS Id,
			NULL AS AdjustmentTypeId,
			1 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
			CASE
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''!''
					THEN 1
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''|''
					THEN 2
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''?''
					THEN 3
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''%''
					THEN 4
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''D''
					THEN 5
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''&''
					THEN 6
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '']''
					THEN 7
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''0''
					THEN 8
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''V''
					THEN 9
				ELSE pctFinType.Id + 1000
			END AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctFinType ON pctFinType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctFinType.Code, 1) = ''|''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.code = ''3''
	
		UNION ALL
	
		SELECT (CONVERT(bigint, 110000000) * 12 + pctReason.Id) AS Id,
			NULL AS AdjustmentTypeId,
			3 AS ClaimAdjustmentGroupCodeId,
			pctReason.Id AS ClaimAdjustmentReasonCodeId,
		CASE
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''!''
					THEN 1
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''|''
					THEN 2
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''?''
					THEN 3
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''%''
					THEN 4
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''D''
					THEN 5
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''&''
					THEN 6
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '']''
					THEN 7
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''0''
					THEN 8
				WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX('' - '', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ''V''
					THEN 9
				ELSE pctFinType.Id + 1000
			END AS FinancialInformationTypeId
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pctReason
		INNER JOIN dbo.PracticeCodeTable pctFinType ON pctFinType.ReferenceType = ''PAYMENTTYPE''
			AND RIGHT(pctFinType.Code, 1) = '']''
		WHERE pctReason.ReferenceType = ''PAYMENTREASON''
			AND pctReason.code = ''23'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ClaimAdjustmentReasonAdjustmentTypes'
			,@ClaimAdjustmentReasonAdjustmentTypesViewSql
			,@isChanged = @isClaimAdjustmentReasonAdjustmentTypesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClaimAdjustmentReasonAdjustmentTypeDelete'
				,
				'
			CREATE TRIGGER model.OnClaimAdjustmentReasonAdjustmentTypeDelete ON [model].[ClaimAdjustmentReasonAdjustmentTypes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @ClaimAdjustmentGroupCodeId int
			DECLARE @ClaimAdjustmentReasonCodeId int
			DECLARE @AdjustmentTypeId int
			DECLARE @FinancialInformationTypeId int
			DECLARE @IsArchived bit
		
			DECLARE ClaimAdjustmentReasonAdjustmentTypesDeletedCursor CURSOR FOR
			SELECT Id, ClaimAdjustmentGroupCodeId, ClaimAdjustmentReasonCodeId, AdjustmentTypeId, FinancialInformationTypeId, IsArchived FROM deleted
		
			OPEN ClaimAdjustmentReasonAdjustmentTypesDeletedCursor
			FETCH NEXT FROM ClaimAdjustmentReasonAdjustmentTypesDeletedCursor INTO @Id, @ClaimAdjustmentGroupCodeId, @ClaimAdjustmentReasonCodeId, @AdjustmentTypeId, @FinancialInformationTypeId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ClaimAdjustmentReasonAdjustmentType --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @ClaimAdjustmentGroupCodeId AS ClaimAdjustmentGroupCodeId, @ClaimAdjustmentReasonCodeId AS ClaimAdjustmentReasonCodeId, @AdjustmentTypeId AS AdjustmentTypeId, @FinancialInformationTypeId AS FinancialInformationTypeId, @IsArchived AS IsArchived
			
				FETCH NEXT FROM ClaimAdjustmentReasonAdjustmentTypesDeletedCursor INTO @Id, @ClaimAdjustmentGroupCodeId, @ClaimAdjustmentReasonCodeId, @AdjustmentTypeId, @FinancialInformationTypeId, @IsArchived 
		
			END
		
			CLOSE ClaimAdjustmentReasonAdjustmentTypesDeletedCursor
			DEALLOCATE ClaimAdjustmentReasonAdjustmentTypesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClaimAdjustmentReasonAdjustmentTypeInsert'
				,
				'
			CREATE TRIGGER model.OnClaimAdjustmentReasonAdjustmentTypeInsert ON [model].[ClaimAdjustmentReasonAdjustmentTypes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @ClaimAdjustmentGroupCodeId int
			DECLARE @ClaimAdjustmentReasonCodeId int
			DECLARE @AdjustmentTypeId int
			DECLARE @FinancialInformationTypeId int
			DECLARE @IsArchived bit
		
			DECLARE ClaimAdjustmentReasonAdjustmentTypesInsertedCursor CURSOR FOR
			SELECT Id, ClaimAdjustmentGroupCodeId, ClaimAdjustmentReasonCodeId, AdjustmentTypeId, FinancialInformationTypeId, IsArchived FROM inserted
		
			OPEN ClaimAdjustmentReasonAdjustmentTypesInsertedCursor
			FETCH NEXT FROM ClaimAdjustmentReasonAdjustmentTypesInsertedCursor INTO @Id, @ClaimAdjustmentGroupCodeId, @ClaimAdjustmentReasonCodeId, @AdjustmentTypeId, @FinancialInformationTypeId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ClaimAdjustmentReasonAdjustmentType --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(bigint, NULL)
			
				SELECT @Id AS Id, @ClaimAdjustmentGroupCodeId AS ClaimAdjustmentGroupCodeId, @ClaimAdjustmentReasonCodeId AS ClaimAdjustmentReasonCodeId, @AdjustmentTypeId AS AdjustmentTypeId, @FinancialInformationTypeId AS FinancialInformationTypeId, @IsArchived AS IsArchived
				
				FETCH NEXT FROM ClaimAdjustmentReasonAdjustmentTypesInsertedCursor INTO @Id, @ClaimAdjustmentGroupCodeId, @ClaimAdjustmentReasonCodeId, @AdjustmentTypeId, @FinancialInformationTypeId, @IsArchived 
		
			END
		
			CLOSE ClaimAdjustmentReasonAdjustmentTypesInsertedCursor
			DEALLOCATE ClaimAdjustmentReasonAdjustmentTypesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClaimAdjustmentReasonAdjustmentTypeUpdate'
				,
				'
			CREATE TRIGGER model.OnClaimAdjustmentReasonAdjustmentTypeUpdate ON [model].[ClaimAdjustmentReasonAdjustmentTypes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @ClaimAdjustmentGroupCodeId int
			DECLARE @ClaimAdjustmentReasonCodeId int
			DECLARE @AdjustmentTypeId int
			DECLARE @FinancialInformationTypeId int
			DECLARE @IsArchived bit
		
			DECLARE ClaimAdjustmentReasonAdjustmentTypesUpdatedCursor CURSOR FOR
			SELECT Id, ClaimAdjustmentGroupCodeId, ClaimAdjustmentReasonCodeId, AdjustmentTypeId, FinancialInformationTypeId, IsArchived FROM inserted
		
			OPEN ClaimAdjustmentReasonAdjustmentTypesUpdatedCursor
			FETCH NEXT FROM ClaimAdjustmentReasonAdjustmentTypesUpdatedCursor INTO @Id, @ClaimAdjustmentGroupCodeId, @ClaimAdjustmentReasonCodeId, @AdjustmentTypeId, @FinancialInformationTypeId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ClaimAdjustmentReasonAdjustmentType --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ClaimAdjustmentReasonAdjustmentTypesUpdatedCursor INTO @Id, @ClaimAdjustmentGroupCodeId, @ClaimAdjustmentReasonCodeId, @AdjustmentTypeId, @FinancialInformationTypeId, @IsArchived 
		
			END
		
			CLOSE ClaimAdjustmentReasonAdjustmentTypesUpdatedCursor
			DEALLOCATE ClaimAdjustmentReasonAdjustmentTypesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ClaimAdjustmentReasonCodes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isClaimAdjustmentReasonCodesChanged BIT

		SET @isClaimAdjustmentReasonCodesChanged = 0

		DECLARE @ClaimAdjustmentReasonCodesViewSql NVARCHAR(max)

		SET @ClaimAdjustmentReasonCodesViewSql = '
		CREATE VIEW [model].[ClaimAdjustmentReasonCodes_Internal]
		WITH SCHEMABINDING
		AS
		SELECT ID,
			Code,
			AlternateCode AS [Name],
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''PAYMENTREASON'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ClaimAdjustmentReasonCodes_Internal'
			,@ClaimAdjustmentReasonCodesViewSql
			,@isChanged = @isClaimAdjustmentReasonCodesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @ClaimAdjustmentReasonCodesViewSql = '	
			CREATE VIEW [model].[ClaimAdjustmentReasonCodes]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, Code, IsArchived FROM [model].[ClaimAdjustmentReasonCodes_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_ClaimAdjustmentReasonCodes'
							AND object_id = OBJECT_ID('[model].[ClaimAdjustmentReasonCodes_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[ClaimAdjustmentReasonCodes_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_ClaimAdjustmentReasonCodes] ON [model].[ClaimAdjustmentReasonCodes_Internal] ([Id]);
				END

				SET @ClaimAdjustmentReasonCodesViewSql = @ClaimAdjustmentReasonCodesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ClaimAdjustmentReasonCodes'
				,@ClaimAdjustmentReasonCodesViewSql
				,@isChanged = @isClaimAdjustmentReasonCodesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClaimAdjustmentReasonCodeDelete'
				,'
			CREATE TRIGGER model.OnClaimAdjustmentReasonCodeDelete ON [model].[ClaimAdjustmentReasonCodes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @Code nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE ClaimAdjustmentReasonCodesDeletedCursor CURSOR FOR
			SELECT Id, Name, Code, IsArchived FROM deleted
		
			OPEN ClaimAdjustmentReasonCodesDeletedCursor
			FETCH NEXT FROM ClaimAdjustmentReasonCodesDeletedCursor INTO @Id, @Name, @Code, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ClaimAdjustmentReasonCode --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name, @Code AS Code, @IsArchived AS IsArchived
			
				FETCH NEXT FROM ClaimAdjustmentReasonCodesDeletedCursor INTO @Id, @Name, @Code, @IsArchived 
		
			END
		
			CLOSE ClaimAdjustmentReasonCodesDeletedCursor
			DEALLOCATE ClaimAdjustmentReasonCodesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClaimAdjustmentReasonCodeInsert'
				,
				'
			CREATE TRIGGER model.OnClaimAdjustmentReasonCodeInsert ON [model].[ClaimAdjustmentReasonCodes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @Code nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE ClaimAdjustmentReasonCodesInsertedCursor CURSOR FOR
			SELECT Id, Name, Code, IsArchived FROM inserted
		
			OPEN ClaimAdjustmentReasonCodesInsertedCursor
			FETCH NEXT FROM ClaimAdjustmentReasonCodesInsertedCursor INTO @Id, @Name, @Code, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ClaimAdjustmentReasonCode --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name, @Code AS Code, @IsArchived AS IsArchived
				
				FETCH NEXT FROM ClaimAdjustmentReasonCodesInsertedCursor INTO @Id, @Name, @Code, @IsArchived 
		
			END
		
			CLOSE ClaimAdjustmentReasonCodesInsertedCursor
			DEALLOCATE ClaimAdjustmentReasonCodesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClaimAdjustmentReasonCodeUpdate'
				,'
			CREATE TRIGGER model.OnClaimAdjustmentReasonCodeUpdate ON [model].[ClaimAdjustmentReasonCodes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @Code nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE ClaimAdjustmentReasonCodesUpdatedCursor CURSOR FOR
			SELECT Id, Name, Code, IsArchived FROM inserted
		
			OPEN ClaimAdjustmentReasonCodesUpdatedCursor
			FETCH NEXT FROM ClaimAdjustmentReasonCodesUpdatedCursor INTO @Id, @Name, @Code, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ClaimAdjustmentReasonCode --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ClaimAdjustmentReasonCodesUpdatedCursor INTO @Id, @Name, @Code, @IsArchived 
		
			END
		
			CLOSE ClaimAdjustmentReasonCodesUpdatedCursor
			DEALLOCATE ClaimAdjustmentReasonCodesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ClaimCrossOvers')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isClaimCrossOversChanged BIT

		SET @isClaimCrossOversChanged = 0

		DECLARE @ClaimCrossOversViewSql NVARCHAR(max)

		SET @ClaimCrossOversViewSql = '
		CREATE VIEW [model].[ClaimCrossOvers]
		WITH SCHEMABINDING
		AS
		SELECT (CONVERT(bigint, 110000000) * priSend.InsurerId + priReceive.InsurerId) AS Id,
			priReceive.InsurerId AS ReceivingInsurerId,
			priSend.InsurerId AS SendingInsurerId
		FROM dbo.PracticeInsurers priSend
		INNER JOIN dbo.PracticeInsurers priReceive ON priReceive.InsurerReferenceCode NOT IN (
				''['',
				'']'',
				''m'',
				''n''
				)
			AND priReceive.InsurerCrossOver = ''Y''
			AND priSend.InsurerReferenceCode IN (
				''['',
				'']'',
				''m'',
				''n''
				)
			AND priSend.InsurerCrossOver = ''Y'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ClaimCrossOvers'
			,@ClaimCrossOversViewSql
			,@isChanged = @isClaimCrossOversChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClaimCrossOverDelete'
				,'
			CREATE TRIGGER model.OnClaimCrossOverDelete ON [model].[ClaimCrossOvers] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @SendingInsurerId int
			DECLARE @ReceivingInsurerId int
		
			DECLARE ClaimCrossOversDeletedCursor CURSOR FOR
			SELECT Id, SendingInsurerId, ReceivingInsurerId FROM deleted
		
			OPEN ClaimCrossOversDeletedCursor
			FETCH NEXT FROM ClaimCrossOversDeletedCursor INTO @Id, @SendingInsurerId, @ReceivingInsurerId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ClaimCrossOver --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @SendingInsurerId AS SendingInsurerId, @ReceivingInsurerId AS ReceivingInsurerId
			
				FETCH NEXT FROM ClaimCrossOversDeletedCursor INTO @Id, @SendingInsurerId, @ReceivingInsurerId 
		
			END
		
			CLOSE ClaimCrossOversDeletedCursor
			DEALLOCATE ClaimCrossOversDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClaimCrossOverInsert'
				,'
			CREATE TRIGGER model.OnClaimCrossOverInsert ON [model].[ClaimCrossOvers] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @SendingInsurerId int
			DECLARE @ReceivingInsurerId int
		
			DECLARE ClaimCrossOversInsertedCursor CURSOR FOR
			SELECT Id, SendingInsurerId, ReceivingInsurerId FROM inserted
		
			OPEN ClaimCrossOversInsertedCursor
			FETCH NEXT FROM ClaimCrossOversInsertedCursor INTO @Id, @SendingInsurerId, @ReceivingInsurerId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ClaimCrossOver --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(bigint, NULL)
			
				SELECT @Id AS Id, @SendingInsurerId AS SendingInsurerId, @ReceivingInsurerId AS ReceivingInsurerId
				
				FETCH NEXT FROM ClaimCrossOversInsertedCursor INTO @Id, @SendingInsurerId, @ReceivingInsurerId 
		
			END
		
			CLOSE ClaimCrossOversInsertedCursor
			DEALLOCATE ClaimCrossOversInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClaimCrossOverUpdate'
				,'
			CREATE TRIGGER model.OnClaimCrossOverUpdate ON [model].[ClaimCrossOvers] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @SendingInsurerId int
			DECLARE @ReceivingInsurerId int
		
			DECLARE ClaimCrossOversUpdatedCursor CURSOR FOR
			SELECT Id, SendingInsurerId, ReceivingInsurerId FROM inserted
		
			OPEN ClaimCrossOversUpdatedCursor
			FETCH NEXT FROM ClaimCrossOversUpdatedCursor INTO @Id, @SendingInsurerId, @ReceivingInsurerId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ClaimCrossOver --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ClaimCrossOversUpdatedCursor INTO @Id, @SendingInsurerId, @ReceivingInsurerId 
		
			END
		
			CLOSE ClaimCrossOversUpdatedCursor
			DEALLOCATE ClaimCrossOversUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ClaimFileReceivers')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isClaimFileReceiversChanged BIT

		SET @isClaimFileReceiversChanged = 0

		DECLARE @ClaimFileReceiversViewSql NVARCHAR(max)

		SET @ClaimFileReceiversViewSql = 
			'
		CREATE VIEW [model].[ClaimFileReceivers_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id,
			'':'' AS ComponentElementSeparator,
			CASE 
				WHEN LetterTranslation = ''''
					THEN CASE 
						WHEN AlternateCode in ('''', ''T'')
							THEN CONVERT(NVARCHAR, NULL)
						ELSE SUBSTRING(AlternateCode, 1, (dbo.GetMax((CHARINDEX(''-'', AlternateCode) - 1),0)))
						END 
				ELSE SUBSTRING(LetterTranslation, 1, (dbo.GetMax((CHARINDEX(''-'', LetterTranslation) - 1),0)))
				END AS GSApplicationReceiverCode,
			CASE 
				WHEN LetterTranslation = ''''
					THEN CASE 
						WHEN LEN(AlternateCode) < 4
							THEN CONVERT(NVARCHAR, NULL)
						ELSE SUBSTRING(AlternateCode, (dbo.GetMax((CHARINDEX(''-'', AlternateCode) + 1),0)), LEN(AlternateCode))
						END
				ELSE SUBSTRING(LetterTranslation, (dbo.GetMax((CHARINDEX(''-'', LetterTranslation) + 1),0)), LEN(LetterTranslation))
				END AS GSApplicationSenderCode,
				''00501'' AS InterchangeControlVersionCode,
			CASE 
				WHEN AlternateCode in ('''', ''T'')
					THEN CONVERT(NVARCHAR, NULL)
				ELSE SUBSTRING(AlternateCode, 1, (dbo.GetMax((CHARINDEX(''-'', AlternateCode) - 1),0)))
				END AS InterchangeReceiverCode,
			CASE 
				WHEN SUBSTRING(Code, 1, 1) IN (
						''C'',
						''M'',
						''V''
						)
					THEN ''30''
				WHEN SUBSTRING(Code, 1, 1) IN (
						''E'',
						''T''
						)
					THEN ''27''
				WHEN SUBSTRING(Code, 1, 1) IN (''B'')
					THEN ''33''
				WHEN SUBSTRING(Code, 1, 1) IN (''Y'')
					THEN ''01''
				ELSE ''ZZ''
				END AS InterchangeReceiverCodeQualifier,
			CASE 
				WHEN LEN(AlternateCode) < 4
					THEN CONVERT(NVARCHAR, NULL)
				ELSE SUBSTRING(AlternateCode, (dbo.GetMax((CHARINDEX(''-'', AlternateCode) + 1),0)), LEN(AlternateCode))
				END AS InterchangeSenderCode,
			CASE 
				WHEN SUBSTRING(Code, 1, 1) IN (
						''C'',
						''M'',
						''V''
						)
					THEN ''30''
				ELSE ''ZZ''
				END AS InterchangeSenderCodeQualifier,
			''P'' AS InterchangeUsageIndicator,
			CASE 
				WHEN LEN(OtherLtrTrans) < 4
					THEN CASE 
						WHEN AlternateCode in ('''', ''T'')
							THEN CONVERT(NVARCHAR, NULL)
						ELSE SUBSTRING(AlternateCode, (dbo.GetMax((CHARINDEX(''-'', AlternateCode) + 1),0)), LEN(AlternateCode))
						END 
				ELSE SUBSTRING(OtherLtrTrans, (dbo.GetMax((CHARINDEX(''-'', OtherLtrTrans) + 1),0)), LEN(OtherLtrTrans))
				END AS Loop1000ASubmitterCode,
			CASE 
				WHEN LEN(OtherLtrTrans) < 4
					THEN CASE 
						WHEN LEN(AlternateCode) < 4
							THEN CONVERT(NVARCHAR, NULL)
						ELSE SUBSTRING(AlternateCode, 1, (dbo.GetMax((CHARINDEX(''-'', AlternateCode) - 1),0)))
						END 
				ELSE SUBSTRING(OtherLtrTrans, 1, (dbo.GetMax((CHARINDEX(''-'', OtherLtrTrans) - 1),0)))
				END AS Loop1000BRecipientCode,
			CASE 
				WHEN Code LIKE ''%:%''
					THEN SUBSTRING(Code, 5, (dbo.GetMax((CHARINDEX('':'', Code) - 6),0)))
				ELSE SUBSTRING(code, 5, LEN(code))
				END AS [Name],
			''^'' AS RepetitionSeparator
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''TRANSMITTYPE'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ClaimFileReceivers_Internal'
			,@ClaimFileReceiversViewSql
			,@isChanged = @isClaimFileReceiversChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @ClaimFileReceiversViewSql = '	
			CREATE VIEW [model].[ClaimFileReceivers]
			WITH SCHEMABINDING
			AS
			SELECT Id, GSApplicationReceiverCode, GSApplicationSenderCode, InterchangeControlVersionCode, InterchangeReceiverCode, InterchangeReceiverCodeQualifier, InterchangeSenderCode, InterchangeSenderCodeQualifier, InterchangeUsageIndicator, Loop1000ASubmitterCode, Loop1000BRecipientCode, ComponentElementSeparator, RepetitionSeparator, Name FROM [model].[ClaimFileReceivers_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_ClaimFileReceivers'
							AND object_id = OBJECT_ID('[model].[ClaimFileReceivers_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[ClaimFileReceivers_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_ClaimFileReceivers] ON [model].[ClaimFileReceivers_Internal] ([Id]);
				END

				SET @ClaimFileReceiversViewSql = @ClaimFileReceiversViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ClaimFileReceivers'
				,@ClaimFileReceiversViewSql
				,@isChanged = @isClaimFileReceiversChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClaimFileReceiverDelete'
				,
				'
			CREATE TRIGGER model.OnClaimFileReceiverDelete ON [model].[ClaimFileReceivers] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @GSApplicationReceiverCode nvarchar(max)
			DECLARE @GSApplicationSenderCode nvarchar(max)
			DECLARE @InterchangeControlVersionCode nvarchar(max)
			DECLARE @InterchangeReceiverCode nvarchar(max)
			DECLARE @InterchangeReceiverCodeQualifier nvarchar(max)
			DECLARE @InterchangeSenderCode nvarchar(max)
			DECLARE @InterchangeSenderCodeQualifier nvarchar(max)
			DECLARE @InterchangeUsageIndicator nvarchar(max)
			DECLARE @Loop1000ASubmitterCode nvarchar(max)
			DECLARE @Loop1000BRecipientCode nvarchar(max)
			DECLARE @ComponentElementSeparator nvarchar(max)
			DECLARE @RepetitionSeparator nvarchar(max)
			DECLARE @Name nvarchar(max)
		
			DECLARE ClaimFileReceiversDeletedCursor CURSOR FOR
			SELECT Id, GSApplicationReceiverCode, GSApplicationSenderCode, InterchangeControlVersionCode, InterchangeReceiverCode, InterchangeReceiverCodeQualifier, InterchangeSenderCode, InterchangeSenderCodeQualifier, InterchangeUsageIndicator, Loop1000ASubmitterCode, Loop1000BRecipientCode, ComponentElementSeparator, RepetitionSeparator, Name FROM deleted
		
			OPEN ClaimFileReceiversDeletedCursor
			FETCH NEXT FROM ClaimFileReceiversDeletedCursor INTO @Id, @GSApplicationReceiverCode, @GSApplicationSenderCode, @InterchangeControlVersionCode, @InterchangeReceiverCode, @InterchangeReceiverCodeQualifier, @InterchangeSenderCode, @InterchangeSenderCodeQualifier, @InterchangeUsageIndicator, @Loop1000ASubmitterCode, @Loop1000BRecipientCode, @ComponentElementSeparator, @RepetitionSeparator, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ClaimFileReceiver --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @GSApplicationReceiverCode AS GSApplicationReceiverCode, @GSApplicationSenderCode AS GSApplicationSenderCode, @InterchangeControlVersionCode AS InterchangeControlVersionCode, @InterchangeReceiverCode AS InterchangeReceiverCode, @InterchangeReceiverCodeQualifier AS InterchangeReceiverCodeQualifier, @InterchangeSenderCode AS InterchangeSenderCode, @InterchangeSenderCodeQualifier AS InterchangeSenderCodeQualifier, @InterchangeUsageIndicator AS InterchangeUsageIndicator, @Loop1000ASubmitterCode AS Loop1000ASubmitterCode, @Loop1000BRecipientCode AS Loop1000BRecipientCode, @ComponentElementSeparator AS ComponentElementSeparator, @RepetitionSeparator AS RepetitionSeparator, @Name AS Name
			
				FETCH NEXT FROM ClaimFileReceiversDeletedCursor INTO @Id, @GSApplicationReceiverCode, @GSApplicationSenderCode, @InterchangeControlVersionCode, @InterchangeReceiverCode, @InterchangeReceiverCodeQualifier, @InterchangeSenderCode, @InterchangeSenderCodeQualifier, @InterchangeUsageIndicator, @Loop1000ASubmitterCode, @Loop1000BRecipientCode, @ComponentElementSeparator, @RepetitionSeparator, @Name 
		
			END
		
			CLOSE ClaimFileReceiversDeletedCursor
			DEALLOCATE ClaimFileReceiversDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClaimFileReceiverInsert'
				,
				'
			CREATE TRIGGER model.OnClaimFileReceiverInsert ON [model].[ClaimFileReceivers] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @GSApplicationReceiverCode nvarchar(max)
			DECLARE @GSApplicationSenderCode nvarchar(max)
			DECLARE @InterchangeControlVersionCode nvarchar(max)
			DECLARE @InterchangeReceiverCode nvarchar(max)
			DECLARE @InterchangeReceiverCodeQualifier nvarchar(max)
			DECLARE @InterchangeSenderCode nvarchar(max)
			DECLARE @InterchangeSenderCodeQualifier nvarchar(max)
			DECLARE @InterchangeUsageIndicator nvarchar(max)
			DECLARE @Loop1000ASubmitterCode nvarchar(max)
			DECLARE @Loop1000BRecipientCode nvarchar(max)
			DECLARE @ComponentElementSeparator nvarchar(max)
			DECLARE @RepetitionSeparator nvarchar(max)
			DECLARE @Name nvarchar(max)
		
			DECLARE ClaimFileReceiversInsertedCursor CURSOR FOR
			SELECT Id, GSApplicationReceiverCode, GSApplicationSenderCode, InterchangeControlVersionCode, InterchangeReceiverCode, InterchangeReceiverCodeQualifier, InterchangeSenderCode, InterchangeSenderCodeQualifier, InterchangeUsageIndicator, Loop1000ASubmitterCode, Loop1000BRecipientCode, ComponentElementSeparator, RepetitionSeparator, Name FROM inserted
		
			OPEN ClaimFileReceiversInsertedCursor
			FETCH NEXT FROM ClaimFileReceiversInsertedCursor INTO @Id, @GSApplicationReceiverCode, @GSApplicationSenderCode, @InterchangeControlVersionCode, @InterchangeReceiverCode, @InterchangeReceiverCodeQualifier, @InterchangeSenderCode, @InterchangeSenderCodeQualifier, @InterchangeUsageIndicator, @Loop1000ASubmitterCode, @Loop1000BRecipientCode, @ComponentElementSeparator, @RepetitionSeparator, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ClaimFileReceiver --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @GSApplicationReceiverCode AS GSApplicationReceiverCode, @GSApplicationSenderCode AS GSApplicationSenderCode, @InterchangeControlVersionCode AS InterchangeControlVersionCode, @InterchangeReceiverCode AS InterchangeReceiverCode, @InterchangeReceiverCodeQualifier AS InterchangeReceiverCodeQualifier, @InterchangeSenderCode AS InterchangeSenderCode, @InterchangeSenderCodeQualifier AS InterchangeSenderCodeQualifier, @InterchangeUsageIndicator AS InterchangeUsageIndicator, @Loop1000ASubmitterCode AS Loop1000ASubmitterCode, @Loop1000BRecipientCode AS Loop1000BRecipientCode, @ComponentElementSeparator AS ComponentElementSeparator, @RepetitionSeparator AS RepetitionSeparator, @Name AS Name
				
				FETCH NEXT FROM ClaimFileReceiversInsertedCursor INTO @Id, @GSApplicationReceiverCode, @GSApplicationSenderCode, @InterchangeControlVersionCode, @InterchangeReceiverCode, @InterchangeReceiverCodeQualifier, @InterchangeSenderCode, @InterchangeSenderCodeQualifier, @InterchangeUsageIndicator, @Loop1000ASubmitterCode, @Loop1000BRecipientCode, @ComponentElementSeparator, @RepetitionSeparator, @Name 
		
			END
		
			CLOSE ClaimFileReceiversInsertedCursor
			DEALLOCATE ClaimFileReceiversInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClaimFileReceiverUpdate'
				,
				'
			CREATE TRIGGER model.OnClaimFileReceiverUpdate ON [model].[ClaimFileReceivers] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @GSApplicationReceiverCode nvarchar(max)
			DECLARE @GSApplicationSenderCode nvarchar(max)
			DECLARE @InterchangeControlVersionCode nvarchar(max)
			DECLARE @InterchangeReceiverCode nvarchar(max)
			DECLARE @InterchangeReceiverCodeQualifier nvarchar(max)
			DECLARE @InterchangeSenderCode nvarchar(max)
			DECLARE @InterchangeSenderCodeQualifier nvarchar(max)
			DECLARE @InterchangeUsageIndicator nvarchar(max)
			DECLARE @Loop1000ASubmitterCode nvarchar(max)
			DECLARE @Loop1000BRecipientCode nvarchar(max)
			DECLARE @ComponentElementSeparator nvarchar(max)
			DECLARE @RepetitionSeparator nvarchar(max)
			DECLARE @Name nvarchar(max)
		
			DECLARE ClaimFileReceiversUpdatedCursor CURSOR FOR
			SELECT Id, GSApplicationReceiverCode, GSApplicationSenderCode, InterchangeControlVersionCode, InterchangeReceiverCode, InterchangeReceiverCodeQualifier, InterchangeSenderCode, InterchangeSenderCodeQualifier, InterchangeUsageIndicator, Loop1000ASubmitterCode, Loop1000BRecipientCode, ComponentElementSeparator, RepetitionSeparator, Name FROM inserted
		
			OPEN ClaimFileReceiversUpdatedCursor
			FETCH NEXT FROM ClaimFileReceiversUpdatedCursor INTO @Id, @GSApplicationReceiverCode, @GSApplicationSenderCode, @InterchangeControlVersionCode, @InterchangeReceiverCode, @InterchangeReceiverCodeQualifier, @InterchangeSenderCode, @InterchangeSenderCodeQualifier, @InterchangeUsageIndicator, @Loop1000ASubmitterCode, @Loop1000BRecipientCode, @ComponentElementSeparator, @RepetitionSeparator, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ClaimFileReceiver --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ClaimFileReceiversUpdatedCursor INTO @Id, @GSApplicationReceiverCode, @GSApplicationSenderCode, @InterchangeControlVersionCode, @InterchangeReceiverCode, @InterchangeReceiverCodeQualifier, @InterchangeSenderCode, @InterchangeSenderCodeQualifier, @InterchangeUsageIndicator, @Loop1000ASubmitterCode, @Loop1000BRecipientCode, @ComponentElementSeparator, @RepetitionSeparator, @Name 
		
			END
		
			CLOSE ClaimFileReceiversUpdatedCursor
			DEALLOCATE ClaimFileReceiversUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ClinicalInvoiceProviders')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isClinicalInvoiceProvidersChanged BIT

		SET @isClinicalInvoiceProvidersChanged = 0

		DECLARE @ClinicalInvoiceProvidersViewSql NVARCHAR(max)

		SET @ClinicalInvoiceProvidersViewSql = 
			'
		CREATE VIEW [model].[ClinicalInvoiceProviders]
		WITH SCHEMABINDING
		AS
		-------humans with service location from PracticeName [no override, handled in claim file only]
		SELECT (CONVERT(bigint, 110000000) * re.ResourceId + pr.AppointmentId) AS Id
			,ap.AppointmentId AS EncounterId
			,(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId
		FROM dbo.Appointments ap
		INNER JOIN dbo.PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId
		INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
		INNER JOIN dbo.PracticeName pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
			AND pnBillOrg.PracticeType = ''P''
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P'' 
			AND (ap.ResourceId2 > 1000 OR ap.ResourceId2 = 0)
			AND pnServLoc.PracticeId = CASE 
				WHEN ap.ResourceId2 > 1000
					THEN ap.ResourceId2 - 1000
				ELSE (SELECT PracticeId AS PracticeId FROM dbo.PracticeName WHERE LocationReference = '''' AND PracticeType = ''P'')
				END
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
		GROUP BY re.ResourceId
			,re.ResourceType
			,pr.AppointmentId
			,ap.AppointmentId
			,ap.ResourceId2
			,pnBillOrg.PracticeId
			,pnServLoc.PracticeId
	
		UNION ALL
	
		----humans where service location is from Resources [no override; handled in claim file only]
		SELECT (CONVERT(bigint, 110000000) * re.ResourceId + pr.AppointmentId) AS Id
			,ap.AppointmentId AS EncounterId
			,(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))  AS ProviderId
		FROM dbo.Appointments ap
		INNER JOIN dbo.PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId
		INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
		INNER JOIN dbo.PracticeName AS pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
			AND pnBillOrg.PracticeType = ''P''
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceId = ap.ResourceId2
			AND reServLoc.ResourceType = ''R''
			AND reServLoc.ServiceCode = ''02''
		WHERE re.ResourceType in (''D'', ''Q'', ''Z'',''Y'')
		GROUP BY re.ResourceId,
			pr.AppointmentId,
			ap.AppointmentId,
			reServLoc.ResourceId,
			re.ResourceId,
			pnBillOrg.PracticeId
	
		UNION ALL
	
		----Facility provider (PracticeName for billing organization and service location)
		SELECT (CONVERT(bigint, 110000000) * reASC.ResourceId + pr.AppointmentId) AS Id
			,apEncounter.AppointmentId AS EncounterId
			,(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + reASC.ResourceId)) AS ProviderId
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
			AND ap.Comments = ''ASC CLAIM'' 
		INNER JOIN dbo.AppointmentType apt ON ap.AppTypeId = apt.AppTypeId
			AND apt.ResourceId8 = 1
		INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
			AND apEncounter.AppTypeId = ap.AppTypeId
			AND apEncounter.AppDate = ap.AppDate
			AND apEncounter.AppTime > 0 
			AND ap.AppTime = 0
			AND apEncounter.ScheduleStatus = ap.ScheduleStatus
			AND ap.ScheduleStatus = ''D''
			AND apEncounter.ResourceId1 = ap.ResourceId1
			AND apEncounter.ResourceId2 = ap.ResourceId2
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
			AND PracticeType = ''P''
			AND LocationReference <> ''''
		INNER JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
			AND reASC.ResourceType = ''R''
			AND reASC.ServiceCode = ''02''
			AND reASC.Billable = ''Y''
			AND pic.FieldValue = ''T''
		GROUP BY pr.AppointmentId
			,ap.AppointmentId
			,ap.ResourceId2
			,pnServLoc.PracticeId
			,reASC.ResourceId
			,apEncounter.AppointmentId'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ClinicalInvoiceProviders'
			,@ClinicalInvoiceProvidersViewSql
			,@isChanged = @isClinicalInvoiceProvidersChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClinicalInvoiceProviderDelete'
				,'
			CREATE TRIGGER model.OnClinicalInvoiceProviderDelete ON [model].[ClinicalInvoiceProviders] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @EncounterId int
			DECLARE @ProviderId bigint
		
			DECLARE ClinicalInvoiceProvidersDeletedCursor CURSOR FOR
			SELECT Id, EncounterId, ProviderId FROM deleted
		
			OPEN ClinicalInvoiceProvidersDeletedCursor
			FETCH NEXT FROM ClinicalInvoiceProvidersDeletedCursor INTO @Id, @EncounterId, @ProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ClinicalInvoiceProvider --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @EncounterId AS EncounterId, @ProviderId AS ProviderId
			
				FETCH NEXT FROM ClinicalInvoiceProvidersDeletedCursor INTO @Id, @EncounterId, @ProviderId 
		
			END
		
			CLOSE ClinicalInvoiceProvidersDeletedCursor
			DEALLOCATE ClinicalInvoiceProvidersDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClinicalInvoiceProviderInsert'
				,'
			CREATE TRIGGER model.OnClinicalInvoiceProviderInsert ON [model].[ClinicalInvoiceProviders] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @EncounterId int
			DECLARE @ProviderId bigint
		
			DECLARE ClinicalInvoiceProvidersInsertedCursor CURSOR FOR
			SELECT Id, EncounterId, ProviderId FROM inserted
		
			OPEN ClinicalInvoiceProvidersInsertedCursor
			FETCH NEXT FROM ClinicalInvoiceProvidersInsertedCursor INTO @Id, @EncounterId, @ProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ClinicalInvoiceProvider --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(bigint, NULL)
			
				SELECT @Id AS Id, @EncounterId AS EncounterId, @ProviderId AS ProviderId
				
				FETCH NEXT FROM ClinicalInvoiceProvidersInsertedCursor INTO @Id, @EncounterId, @ProviderId 
		
			END
		
			CLOSE ClinicalInvoiceProvidersInsertedCursor
			DEALLOCATE ClinicalInvoiceProvidersInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClinicalInvoiceProviderUpdate'
				,'
			CREATE TRIGGER model.OnClinicalInvoiceProviderUpdate ON [model].[ClinicalInvoiceProviders] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @EncounterId int
			DECLARE @ProviderId bigint
		
			DECLARE ClinicalInvoiceProvidersUpdatedCursor CURSOR FOR
			SELECT Id, EncounterId, ProviderId FROM inserted
		
			OPEN ClinicalInvoiceProvidersUpdatedCursor
			FETCH NEXT FROM ClinicalInvoiceProvidersUpdatedCursor INTO @Id, @EncounterId, @ProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ClinicalInvoiceProvider --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ClinicalInvoiceProvidersUpdatedCursor INTO @Id, @EncounterId, @ProviderId 
		
			END
		
			CLOSE ClinicalInvoiceProvidersUpdatedCursor
			DEALLOCATE ClinicalInvoiceProvidersUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ClinicalServiceModifiers')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isClinicalServiceModifiersChanged BIT

		SET @isClinicalServiceModifiersChanged = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ClinicalServiceModifiers'
			,'
		CREATE VIEW [model].[ClinicalServiceModifiers]
		WITH SCHEMABINDING
		AS
		SELECT CONVERT(int, NULL) AS Id, CONVERT(int, NULL) AS ClinicalServiceId, CONVERT(int, NULL) AS ServiceModifierId
		-- View SQL not found for ClinicalServiceModifier --
		'
			,@isChanged = @isClinicalServiceModifiersChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClinicalServiceModifierDelete'
				,
				'
			CREATE TRIGGER model.OnClinicalServiceModifierDelete ON [model].[ClinicalServiceModifiers] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @ClinicalServiceId int
			DECLARE @ServiceModifierId int
		
			DECLARE ClinicalServiceModifiersDeletedCursor CURSOR FOR
			SELECT Id, ClinicalServiceId, ServiceModifierId FROM deleted
		
			OPEN ClinicalServiceModifiersDeletedCursor
			FETCH NEXT FROM ClinicalServiceModifiersDeletedCursor INTO @Id, @ClinicalServiceId, @ServiceModifierId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ClinicalServiceModifier --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @ClinicalServiceId AS ClinicalServiceId, @ServiceModifierId AS ServiceModifierId
			
				FETCH NEXT FROM ClinicalServiceModifiersDeletedCursor INTO @Id, @ClinicalServiceId, @ServiceModifierId 
		
			END
		
			CLOSE ClinicalServiceModifiersDeletedCursor
			DEALLOCATE ClinicalServiceModifiersDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClinicalServiceModifierInsert'
				,
				'
			CREATE TRIGGER model.OnClinicalServiceModifierInsert ON [model].[ClinicalServiceModifiers] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @ClinicalServiceId int
			DECLARE @ServiceModifierId int
		
			DECLARE ClinicalServiceModifiersInsertedCursor CURSOR FOR
			SELECT Id, ClinicalServiceId, ServiceModifierId FROM inserted
		
			OPEN ClinicalServiceModifiersInsertedCursor
			FETCH NEXT FROM ClinicalServiceModifiersInsertedCursor INTO @Id, @ClinicalServiceId, @ServiceModifierId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ClinicalServiceModifier --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @ClinicalServiceId AS ClinicalServiceId, @ServiceModifierId AS ServiceModifierId
				
				FETCH NEXT FROM ClinicalServiceModifiersInsertedCursor INTO @Id, @ClinicalServiceId, @ServiceModifierId 
		
			END
		
			CLOSE ClinicalServiceModifiersInsertedCursor
			DEALLOCATE ClinicalServiceModifiersInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClinicalServiceModifierUpdate'
				,'
			CREATE TRIGGER model.OnClinicalServiceModifierUpdate ON [model].[ClinicalServiceModifiers] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @ClinicalServiceId int
			DECLARE @ServiceModifierId int
		
			DECLARE ClinicalServiceModifiersUpdatedCursor CURSOR FOR
			SELECT Id, ClinicalServiceId, ServiceModifierId FROM inserted
		
			OPEN ClinicalServiceModifiersUpdatedCursor
			FETCH NEXT FROM ClinicalServiceModifiersUpdatedCursor INTO @Id, @ClinicalServiceId, @ServiceModifierId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ClinicalServiceModifier --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ClinicalServiceModifiersUpdatedCursor INTO @Id, @ClinicalServiceId, @ServiceModifierId 
		
			END
		
			CLOSE ClinicalServiceModifiersUpdatedCursor
			DEALLOCATE ClinicalServiceModifiersUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ClinicalServices')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isClinicalServicesChanged BIT

		SET @isClinicalServicesChanged = 0

		DECLARE @ClinicalServicesViewSql NVARCHAR(max)

		SET @ClinicalServicesViewSql = '
		CREATE VIEW [model].[ClinicalServices]
		WITH SCHEMABINDING
		AS
		SELECT
			ItemId AS Id,
			AppointmentId AS EncounterId,
			MAX(ps.CodeId) AS EncounterServiceId,
			CASE 
				WHEN ItemOrder IS NULL
					THEN 0
				ELSE ItemOrder
				END AS OrdinalId,
			QuantityDecimal AS Unit,
			prs.ChargeDecimal AS UnitCharge,
			ItemId,
			COUNT_BIG(*) AS Count
			FROM dbo.PatientReceivableServices prs
			INNER JOIN dbo.PatientReceivables pr ON pr.Invoice = prs.Invoice
			LEFT JOIN dbo.PracticeServices ps ON ps.Code = prs.Service
			WHERE prs.[Status] = ''A'' AND AppointmentId > 0
			GROUP BY AppointmentId,
				prs.[status],
				ItemOrder,
				QuantityDecimal,
				prs.ChargeDecimal,
				ItemId'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ClinicalServices'
			,@ClinicalServicesViewSql
			,@isChanged = @isClinicalServicesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClinicalServiceDelete'
				,
				'
			CREATE TRIGGER model.OnClinicalServiceDelete ON [model].[ClinicalServices] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Unit decimal(18,0)
			DECLARE @UnitCharge decimal(18,0)
			DECLARE @OrdinalId int
			DECLARE @EncounterId int
			DECLARE @EncounterServiceId int
		
			DECLARE ClinicalServicesDeletedCursor CURSOR FOR
			SELECT Id, Unit, UnitCharge, OrdinalId, EncounterId, EncounterServiceId FROM deleted
		
			OPEN ClinicalServicesDeletedCursor
			FETCH NEXT FROM ClinicalServicesDeletedCursor INTO @Id, @Unit, @UnitCharge, @OrdinalId, @EncounterId, @EncounterServiceId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ClinicalService --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Unit AS Unit, @UnitCharge AS UnitCharge, @OrdinalId AS OrdinalId, @EncounterId AS EncounterId, @EncounterServiceId AS EncounterServiceId
			
				FETCH NEXT FROM ClinicalServicesDeletedCursor INTO @Id, @Unit, @UnitCharge, @OrdinalId, @EncounterId, @EncounterServiceId 
		
			END
		
			CLOSE ClinicalServicesDeletedCursor
			DEALLOCATE ClinicalServicesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClinicalServiceInsert'
				,
				'
			CREATE TRIGGER model.OnClinicalServiceInsert ON [model].[ClinicalServices] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Unit decimal(18,0)
			DECLARE @UnitCharge decimal(18,0)
			DECLARE @OrdinalId int
			DECLARE @EncounterId int
			DECLARE @EncounterServiceId int
		
			DECLARE ClinicalServicesInsertedCursor CURSOR FOR
			SELECT Id, Unit, UnitCharge, OrdinalId, EncounterId, EncounterServiceId FROM inserted
		
			OPEN ClinicalServicesInsertedCursor
			FETCH NEXT FROM ClinicalServicesInsertedCursor INTO @Id, @Unit, @UnitCharge, @OrdinalId, @EncounterId, @EncounterServiceId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ClinicalService --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Unit AS Unit, @UnitCharge AS UnitCharge, @OrdinalId AS OrdinalId, @EncounterId AS EncounterId, @EncounterServiceId AS EncounterServiceId
				
				FETCH NEXT FROM ClinicalServicesInsertedCursor INTO @Id, @Unit, @UnitCharge, @OrdinalId, @EncounterId, @EncounterServiceId 
		
			END
		
			CLOSE ClinicalServicesInsertedCursor
			DEALLOCATE ClinicalServicesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClinicalServiceUpdate'
				,'
			CREATE TRIGGER model.OnClinicalServiceUpdate ON [model].[ClinicalServices] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Unit decimal(18,0)
			DECLARE @UnitCharge decimal(18,0)
			DECLARE @OrdinalId int
			DECLARE @EncounterId int
			DECLARE @EncounterServiceId int
		
			DECLARE ClinicalServicesUpdatedCursor CURSOR FOR
			SELECT Id, Unit, UnitCharge, OrdinalId, EncounterId, EncounterServiceId FROM inserted
		
			OPEN ClinicalServicesUpdatedCursor
			FETCH NEXT FROM ClinicalServicesUpdatedCursor INTO @Id, @Unit, @UnitCharge, @OrdinalId, @EncounterId, @EncounterServiceId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ClinicalService --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ClinicalServicesUpdatedCursor INTO @Id, @Unit, @UnitCharge, @OrdinalId, @EncounterId, @EncounterServiceId 
		
			END
		
			CLOSE ClinicalServicesUpdatedCursor
			DEALLOCATE ClinicalServicesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ClinicalSpecialtyTypes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isClinicalSpecialtyTypesChanged BIT

		SET @isClinicalSpecialtyTypesChanged = 0

		DECLARE @ClinicalSpecialtyTypesViewSql NVARCHAR(max)

		SET @ClinicalSpecialtyTypesViewSql = '
		CREATE VIEW [model].[ClinicalSpecialtyTypes_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id,
			Code AS [Name]
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''SPECIALTY'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ClinicalSpecialtyTypes_Internal'
			,@ClinicalSpecialtyTypesViewSql
			,@isChanged = @isClinicalSpecialtyTypesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @ClinicalSpecialtyTypesViewSql = '	
			CREATE VIEW [model].[ClinicalSpecialtyTypes]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, IsArchived FROM [model].[ClinicalSpecialtyTypes_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_ClinicalSpecialtyTypes'
							AND object_id = OBJECT_ID('[model].[ClinicalSpecialtyTypes_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[ClinicalSpecialtyTypes_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_ClinicalSpecialtyTypes] ON [model].[ClinicalSpecialtyTypes_Internal] ([Id]);
				END

				SET @ClinicalSpecialtyTypesViewSql = @ClinicalSpecialtyTypesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ClinicalSpecialtyTypes'
				,@ClinicalSpecialtyTypesViewSql
				,@isChanged = @isClinicalSpecialtyTypesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClinicalSpecialtyTypeDelete'
				,'
			CREATE TRIGGER model.OnClinicalSpecialtyTypeDelete ON [model].[ClinicalSpecialtyTypes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE ClinicalSpecialtyTypesDeletedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM deleted
		
			OPEN ClinicalSpecialtyTypesDeletedCursor
			FETCH NEXT FROM ClinicalSpecialtyTypesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ClinicalSpecialtyType --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
			
				FETCH NEXT FROM ClinicalSpecialtyTypesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE ClinicalSpecialtyTypesDeletedCursor
			DEALLOCATE ClinicalSpecialtyTypesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClinicalSpecialtyTypeInsert'
				,'
			CREATE TRIGGER model.OnClinicalSpecialtyTypeInsert ON [model].[ClinicalSpecialtyTypes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE ClinicalSpecialtyTypesInsertedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN ClinicalSpecialtyTypesInsertedCursor
			FETCH NEXT FROM ClinicalSpecialtyTypesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ClinicalSpecialtyType --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
				
				FETCH NEXT FROM ClinicalSpecialtyTypesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE ClinicalSpecialtyTypesInsertedCursor
			DEALLOCATE ClinicalSpecialtyTypesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnClinicalSpecialtyTypeUpdate'
				,'
			CREATE TRIGGER model.OnClinicalSpecialtyTypeUpdate ON [model].[ClinicalSpecialtyTypes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE ClinicalSpecialtyTypesUpdatedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN ClinicalSpecialtyTypesUpdatedCursor
			FETCH NEXT FROM ClinicalSpecialtyTypesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ClinicalSpecialtyType --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ClinicalSpecialtyTypesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE ClinicalSpecialtyTypesUpdatedCursor
			DEALLOCATE ClinicalSpecialtyTypesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Comments')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isCommentsChanged BIT

		SET @isCommentsChanged = 0

		DECLARE @CommentsViewSql NVARCHAR(max)

		SET @CommentsViewSql = 
			'
		CREATE VIEW [model].[Comments]
		WITH SCHEMABINDING
		AS
		----There are many other notes not yet included in the below query, including Patient Financial, Patient Collection, Practice, Red (CRM), Surgery and Clinical notes.
		----We also need additional properties to the comments (or a restructuring) to include StartDateTime, EndDateTime, IsDeleted, IsIncludedOnClaim, IsIncludedOnStatement, IsIncludedOnRx, IsAlertOn and additional granularity such as NoteSystem, NoteCategory.
		----Adjustment comment
		SELECT (CONVERT(bigint, 110000000) * 1 + PaymentId) AS Id,
			PaymentId AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			PaymentRefId AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientReceivablePayments prp
		INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
		LEFT JOIN dbo.PatientDemographics pd ON pd.PatientId = prp.PayerId AND prp.PayerId <> 0 AND prp.PayerType = ''P''
		LEFT JOIN dbo.PracticeInsurers pins ON pins.InsurerId = prp.PayerId AND prp.PayerType = ''I'' 
		LEFT JOIN dbo.PatientReceivableServices prs ON prs.ItemId = prp.PaymentServiceItem
		INNER JOIN dbo.PracticeCodeTable pct ON ReferenceType = ''PAYMENTTYPE'' AND prp.PaymentType = SUBSTRING(Code, LEN(Code), 1)
		LEFT JOIN dbo.PracticeCodeTable pctPaymentMethod ON pctPaymentMethod.ReferenceType = ''PAYABLETYPE'' and prp.PaymentRefType = SUBSTRING(pctPaymentMethod.Code, LEN(pctPaymentMethod.Code), 1)
		WHERE PaymentFinancialType <> ''''
			AND PaymentRefId <> ''''
			AND PaymentRefId IS NOT NULL
			AND PaymentRefId <> ''0''
			AND prp.PaymentFinancialType <> ''''
				AND ((prp.PayerType = ''I'' AND pr.ComputedFinancialId IS NOT NULL)
					OR
					prp.PayerType <> ''I'')
	
		UNION ALL
	
		----Appointment comment 
		SELECT (CONVERT(bigint, 110000000) * 2 + AppointmentId) AS Id,
			NULL AS AdjustmentId,
			AppointmentId AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			Comments AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.Appointments
		WHERE Comments NOT IN (
				''ADD VIA BILLING'',
				''ADD VIA OPTICAL'',
				''ADD VIA HIGHLIGHTS'',
				''''
				)
			AND Comments IS NOT NULL
			AND	AppTime > 0 
	
		UNION ALL
	
		--BillingService comment partition 1
		SELECT (CONVERT(bigint, 110000000) * 3 + NoteId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			MIN(prs.ItemId) AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			note1 + note2 + note3 + note4 AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientNotes pn
		INNER JOIN dbo.PatientReceivables pr ON pr.AppointmentId = pn.AppointmentId
		INNER JOIN dbo.PatientReceivableServices prs ON prs.invoice = pr.invoice
		WHERE NoteType = ''B''
			AND NoteSystem LIKE ''R%C%''
			AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX(''C'', NoteSystem) + 1),0)), LEN(NoteSystem)) = prs.Service
			AND prs.[Status] = ''A''
			AND CASE NoteEye
				WHEN ''OS''
					THEN ''LT''
				WHEN ''OD''
					THEN ''RT''
				WHEN ''OU''
					THEN ''50''
				ELSE NoteEye
				END = SUBSTRING(prs.modifier, 1, 2)
		GROUP BY NoteId,
			prs.[Status],
			NoteEye,
			Note1,
			Note2,
			Note3,
			Note4
	
			UNION ALL
	
		 --BillingService comment partition 2
		SELECT (CONVERT(bigint, 110000000) * 17 + NoteId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			MIN(prs.ItemId) AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			note1 + note2 + note3 + note4 AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientNotes pn
		INNER JOIN dbo.PatientReceivables pr ON pr.AppointmentId = pn.AppointmentId
		INNER JOIN dbo.PatientReceivableServices prs ON prs.invoice = pr.invoice
		WHERE NoteType = ''B''
			AND NoteSystem LIKE ''R%C%''
			AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX(''C'', NoteSystem) + 1),0)), LEN(NoteSystem)) = prs.Service
			AND prs.[Status] = ''A''
			AND CASE NoteEye
				WHEN ''OS''
					THEN ''LT''
				WHEN ''OD''
					THEN ''RT''
				WHEN ''OU''
					THEN ''50''
				ELSE NoteEye
				END = SUBSTRING(prs.modifier, 3, 2)
		GROUP BY NoteId,
			prs.[Status],
			NoteEye,
			Note1,
			Note2,
			Note3,
			Note4
	
		 UNION ALL
	
		 --BillingService comment partition 3
		SELECT (CONVERT(bigint, 110000000) * 18 + NoteId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			MIN(prs.ItemId) AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			note1 + note2 + note3 + note4 AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientNotes pn
		INNER JOIN dbo.PatientReceivables pr ON pr.AppointmentId = pn.AppointmentId
		INNER JOIN dbo.PatientReceivableServices prs ON prs.invoice = pr.invoice
		WHERE NoteType = ''B''
			AND NoteSystem LIKE ''R%C%''
			AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX(''C'', NoteSystem) + 1),0)), LEN(NoteSystem)) = prs.Service
			AND prs.[Status] = ''A''
			AND CASE NoteEye
				WHEN ''OS''
					THEN ''LT''
				WHEN ''OD''
					THEN ''RT''
				WHEN ''OU''
					THEN ''50''
				ELSE NoteEye
				END = SUBSTRING(prs.modifier, 5, 2)
		GROUP BY NoteId,
			prs.[Status],
			NoteEye,
			Note1,
			Note2,
			Note3,
			Note4
	
		UNION ALL
	
		--BillingService comment partition 4
		SELECT (CONVERT(bigint, 110000000) * 19 + NoteId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			MIN(prs.ItemId) AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			note1 + note2 + note3 + note4 AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientNotes pn
		INNER JOIN dbo.PatientReceivables pr ON pr.AppointmentId = pn.AppointmentId
		INNER JOIN dbo.PatientReceivableServices prs ON prs.invoice = pr.invoice
		WHERE NoteType = ''B''
			AND NoteSystem LIKE ''R%C%''
			AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX(''C'', NoteSystem) + 1),0)), LEN(NoteSystem)) = prs.Service
			AND prs.[Status] = ''A''
			AND CASE NoteEye
				WHEN ''OS''
					THEN ''LT''
				WHEN ''OD''
					THEN ''RT''
				WHEN ''OU''
					THEN ''50''
				ELSE NoteEye
				END = SUBSTRING(prs.modifier, 7, 2)
		GROUP BY NoteId,
			prs.[Status],
			NoteEye,
			Note1,
			Note2,
			Note3,
			Note4
	
		-- UNION ALL
	
		------ClinicalService comment (no good)
		--SELECT (CONVERT(bigint, 110000000) * 4 + NoteId) AS Id,
		--	NULL AS AdjustmentId,
		--	NULL AS AppointmentId,
		--	NULL AS BillingServiceId,
		--	MIN(prs.ItemId) AS ClinicalServiceId,
		--	NULL AS ContactId,
		--	NULL AS EncounterLensesPrescriptionId,
		--	NULL AS EncounterServiceId,
		--	NULL AS FinancialInformationId,
		--	NULL AS InsurancePolicyId,
		--	NULL AS InsurerId,
		--	NULL AS InvoiceId,
		--	NULL AS PatientId,
		--	NULL AS PatientInsuranceAuthorizationId,
		--	NULL AS PatientInsuranceReferralId,
		--	NULL AS PatientReferralSourceId,
		--	NULL AS ScheduleEntryId,
		--	note1 + note2 + note3 + note4 AS Value,
		--	CONVERT(bit, 0) AS IsRetired
		--FROM dbo.PatientNotes pn
		--INNER JOIN dbo.PatientReceivables pr ON pr.AppointmentId = pn.AppointmentId
		--INNER JOIN dbo.PatientReceivableServices prs ON prs.invoice = pr.invoice
		--WHERE NoteType = ''B''
		--	AND NoteSystem LIKE ''R%C%''
		--	AND SUBSTRING(NoteSystem, (dbo.GetMax((CHARINDEX(''C'', NoteSystem) + 1),0)), LEN(NoteSystem)) = prs.Service
		--	AND prs.[Status] = ''A''	
		--	AND CASE NoteEye
		--		WHEN ''OS''
		--			THEN ''LT''
		--		WHEN ''OD''
		--			THEN ''RT''
		--		WHEN ''OU''
		--			THEN ''50''
		--		ELSE NoteEye
		--		END = SUBSTRING(prs.modifier, 1, 2)
		--GROUP BY NoteId,
		--	prs.[Status],
		--	NoteEye,
		--	Note1,
		--	Note2,
		--	Note3,
		--	Note4
	
	
		----Emergency contacts and External providers don''t have notes currently in IO
	
		UNION ALL
	
		----EncounterLensesPrescription - spectacles comment
		SELECT (CONVERT(bigint, 110000000) * 7 + ClinicalId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			ClinicalId AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			SUBSTRING(ImageDescriptor, 5,(dbo.GetMax((CHARINDEX(''OS:'', imagedescriptor) - 5),0))) AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientClinical pc
		WHERE ClinicalType = ''A''
			AND SUBSTRING(FindingDetail, 1, 18) = ''DISPENSE SPECTACLE''
			AND SUBSTRING(ImageDescriptor, 1, 1) <> ''!''
			AND ImageDescriptor NOT IN (
				'''',
				''^OD: OS:'',
				''^''
				)
			AND [Status] = ''A''
			AND pc.AppointmentId <> 0
	
		UNION ALL
	
		----EncounterLensesPrescription - contact lens OD comment
		SELECT (CONVERT(bigint, 110000000) * 8 + ClinicalId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			ClinicalId AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			SUBSTRING(ImageDescriptor, 5, (dbo.GetMax((CHARINDEX(''OS:'', ImageDescriptor) - 5),0))) AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientClinical pc
		WHERE ClinicalType = ''A''
			AND SUBSTRING(FindingDetail, 1, 14) = ''DISPENSE CL RX''
			AND SUBSTRING(ImageDescriptor, 1, 1) <> ''!''
			AND ImageDescriptor NOT IN (
				'''',
				''^''
				)
			AND LEN(imagedescriptor) > 4
			AND SUBSTRING(ImageDescriptor, 1, 8) <> ''^OD: OS:''
			AND SUBSTRING(ImageDescriptor, 1, 7) <> ''^OD:OS:''
			AND SUBSTRING(ImageDescriptor, 1, 4) IN (''^OD:'', ''*OD:'')
			AND ImageDescriptor like ''%OS:%''
			AND [Status] = ''A''
			AND pc.AppointmentId <> 0
		
		UNION ALL
	
		----EncounterLensesPrescription - contact lens where no eye specified
		SELECT (CONVERT(bigint, 110000000) * 8 + ClinicalId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			ClinicalId AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			ImageDescriptor AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientClinical pc
		WHERE ClinicalType = ''A''
			AND SUBSTRING(FindingDetail, 1, 14) = ''DISPENSE CL RX''
			AND SUBSTRING(ImageDescriptor, 1, 1) <> ''!''
			AND ImageDescriptor NOT IN (
				'''',
				''^''
				)
			AND LEN(imagedescriptor) > 4
			AND SUBSTRING(ImageDescriptor, 1, 8) <> ''^OD: OS:''
			AND SUBSTRING(ImageDescriptor, 1, 7) <> ''^OD:OS:''
			AND SUBSTRING(ImageDescriptor, 1, 4) NOT IN (''^OD:'', ''*OD:'')
			AND [Status] = ''A''
			AND pc.AppointmentId <> 0
		
		UNION ALL
		----EncounterLensesPrescription - contact lens OS comment
		SELECT (CONVERT(bigint, 110000000) * 9 + ClinicalId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			ClinicalId AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			SUBSTRING(ImageDescriptor, dbo.GetMax((dbo.GetMax((CHARINDEX(''OS:'', ImageDescriptor) + 3),0)), LEN(imagedescriptor) - (dbo.GetMax((CHARINDEX(''OS:'', imagedescriptor) + 3),0))),0) AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientClinical pc
		WHERE ClinicalType = ''A''
			AND SUBSTRING(FindingDetail, 1, 14) = ''DISPENSE CL RX''
			AND SUBSTRING(ImageDescriptor, 1, 1) <> ''!''
			AND ImageDescriptor NOT IN (
				'''',
				''^OD: OS:'',
				''^''
				)
			AND LEN(ImageDescriptor) > 4
			AND ImageDescriptor like ''%OS:%''
			AND [Status] = ''A''	
			AND pc.AppointmentId <> 0
		
		UNION ALL
	
		----EncounterService comment
		SELECT (CONVERT(bigint, 110000000) * 10 + CodeId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			CodeId AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			ClaimNote AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PracticeServices
		WHERE ClaimNote <> ''''
			AND ClaimNote IS NOT NULL
	
		UNION ALL
	
		----Financial Information comment
		SELECT (CONVERT(bigint, 110000000) * 11 + prp.PaymentId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			prp.PaymentId AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			prp.PaymentRefId AS Value,
			CONVERT(bit, 0) AS IsRetired
			FROM dbo.PatientReceivablePayments prp
			INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
			INNER JOIN dbo.PatientFinancial pf ON pf.PatientId = pr.InsuredId
				AND prp.PayerId = pf.FinancialInsurerId
				AND pr.ComputedFinancialId = pf.FinancialId		
			INNER JOIN dbo.PracticeCodeTable pct ON ReferenceType = ''PAYMENTTYPE''
				AND prp.PaymentType = SUBSTRING(Code, LEN(code), 1)
				AND (pct.AlternateCode = '''' OR pct.AlternateCode IS NULL)
			LEFT JOIN dbo.PatientReceivablePayments prpBatch ON prpBatch.PaymentId = prp.PaymentBatchCheckId
				AND prpBatch.PaymentType = ''=''
			INNER JOIN dbo.PatientReceivableServices prs on prs.ItemId = prp.PaymentServiceItem
				AND prs.[Status] = ''A''
		WHERE prp.PaymentFinancialType = ''''
			AND prp.PaymentRefId <> ''''
			AND prp.PaymentRefId IS NOT NULL
			AND prp.PaymentRefId <> ''0''
	
		UNION ALL
	
		----Insurer comment
		SELECT (CONVERT(bigint, 110000000) * 12 + InsurerId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			InsurerId AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			InsurerComment AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PracticeInsurers
		WHERE InsurerComment <> ''''
			AND InsurerComment IS NOT NULL
	
		UNION ALL
	
		----Invoice comment
		SELECT (CONVERT(bigint, 110000000) * 13 + v.NoteId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			v.InvoiceId AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			v.Value,
			CONVERT(bit, 0) AS IsRetired
		FROM (
				SELECT 
				pr.AppointmentId AS InvoiceId,
				Note1 + Note2 + Note3 + Note4 AS Value,
				NoteId AS NoteId,
				COUNT_BIG(*) AS Count		
		FROM dbo.PatientNotes pn
		INNER JOIN dbo.PatientReceivables pr ON pn.AppointmentId = pr.AppointmentId
		WHERE NoteType = ''B''
			AND NoteSystem NOT LIKE ''%C%''
			AND pn.AppointmentId > 0
			GROUP BY pr.AppointmentId,
				Note1 + Note2 + Note3 + Note4,
				NoteId
			) AS v
	
		UNION ALL
	
		----Patient - Demographic Notes
		SELECT (CONVERT(bigint, 110000000) * 5 + NoteId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			pn.PatientId AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			note1 + note2 + note3 + note4 AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientNotes pn
		INNER JOIN dbo.PatientDemographics pd on pn.PatientId = pd.PatientId
		WHERE pn.NoteType = ''D'' AND pn.PatientId <> 0
		UNION ALL
	
		----Patient - PatientDemographics.ProfileComment1
		SELECT (CONVERT(bigint, 110000000) * 6 + PatientId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			PatientId AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			ProfileComment1 AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientDemographics pd
		WHERE ProfileComment1 <> ''''
			AND ProfileComment1 IS NOT NULL
	
		UNION ALL
	
		----PatientInsuranceAuthorization comment
		SELECT (CONVERT(bigint, 110000000) * 14 + MAX(ppc.PreCertId)) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			MAX(ppc.PreCertId) AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			PreCertComment AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientPreCerts ppc
		INNER JOIN dbo.Appointments ap ON ap.PreCertId = ppc.PreCertId
		   AND ap.ScheduleStatus = ''D''
		INNER JOIN dbo.PatientFinancial pf ON ppc.PreCertInsId = pf.FinancialInsurerId
		INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = ppc.PatientId
		INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = ppc.PreCertInsId
		   AND (
			   pf.PatientId = pd.PolicyPatientId
			   OR (pf.PatientId = pd.SecondPolicyPatientId and pri.AllowDependents = 1)
			   )
		   AND pf.FinancialStartDate <= PreCertDate
		   AND (
			   pf.FinancialEndDate >= PreCertDate
			   OR pf.FinancialEndDate = ''''
			   )
		WHERE ppc.PreCertComment <> ''''
			AND ppc.PreCertComment IS NOT NULL
		GROUP BY PreCert,
		   PreCertDate,
		   PreCertStatus,
		   pd.PatientId,
		   ScheduleStatus,
		   ap.AppointmentId,
		   PreCertComment
		UNION ALL
	
		----PatientInsuranceReferral Comment
		SELECT (CONVERT(bigint, 110000000) * 15 + ReferralId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			ReferralId AS PatientInsuranceReferralId,
			NULL AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			Reason AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientReferral pRef
		INNER JOIN dbo.PracticeVendors pv ON pv.VendorId = pRef.ReferredFromId
			AND VendorLastName <> '''' AND VendorLastName IS NOT NULL
		INNER JOIN dbo.PatientFinancial pf ON pf.FinancialId = pRef.ComputedFinancialId
		WHERE Reason <> ''''
			AND Reason IS NOT NULL
			AND pRef.ReferredTo <> 0
			AND (pf.PatientId = pRef.PatientId)
	
		UNION ALL
	
		----PatientReferralSource Comment
		SELECT (CONVERT(bigint, 110000000) * 16 + pd.PatientId) AS Id,
			NULL AS AdjustmentId,
			NULL AS AppointmentId,
			NULL AS BillingServiceId,
			NULL AS ClinicalServiceId,
			NULL AS ContactId,
			NULL AS EncounterLensesPrescriptionId,
			NULL AS EncounterServiceId,
			NULL AS FinancialInformationId,
			NULL AS InsurancePolicyId,
			NULL AS InsurerId,
			NULL AS InvoiceId,
			NULL AS PatientId,
			NULL AS PatientInsuranceAuthorizationId,
			NULL AS PatientInsuranceReferralId,
			pd.PatientId AS PatientReferralSourceId,
			NULL AS ScheduleEntryId,
			pd.ProfileComment2 AS Value,
			CONVERT(bit, 0) AS IsRetired
		FROM dbo.PatientDemographics pd
		INNER JOIN dbo.PracticeCodeTable pct ON pct.Code = pd.ReferralCatagory
			AND ReferenceType = ''REFERBY''
		WHERE ProfileComment2 <> ''''
			AND ProfileComment2 IS NOT NULL'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Comments'
			,@CommentsViewSql
			,@isChanged = @isCommentsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnCommentDelete'
				,
				'
			CREATE TRIGGER model.OnCommentDelete ON [model].[Comments] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @Value nvarchar(max)
			DECLARE @AppointmentId int
			DECLARE @InsurerId int
			DECLARE @PatientInsuranceAuthorizationId int
			DECLARE @PatientInsuranceReferralId int
			DECLARE @InvoiceId int
			DECLARE @PatientReferralSourceId int
			DECLARE @EncounterServiceId int
			DECLARE @AdjustmentId int
			DECLARE @ContactId int
			DECLARE @InsurancePolicyId int
			DECLARE @FinancialInformationId int
			DECLARE @BillingServiceId int
			DECLARE @ClinicalServiceId int
			DECLARE @ScheduleEntryId int
			DECLARE @IsRetired bit
			DECLARE @PatientId int
			DECLARE @EncounterLensesPrescriptionId int
		
			DECLARE CommentsDeletedCursor CURSOR FOR
			SELECT Id, Value, AppointmentId, InsurerId, PatientInsuranceAuthorizationId, PatientInsuranceReferralId, InvoiceId, PatientReferralSourceId, EncounterServiceId, AdjustmentId, ContactId, InsurancePolicyId, FinancialInformationId, BillingServiceId, ClinicalServiceId, ScheduleEntryId, IsRetired, PatientId, EncounterLensesPrescriptionId FROM deleted
		
			OPEN CommentsDeletedCursor
			FETCH NEXT FROM CommentsDeletedCursor INTO @Id, @Value, @AppointmentId, @InsurerId, @PatientInsuranceAuthorizationId, @PatientInsuranceReferralId, @InvoiceId, @PatientReferralSourceId, @EncounterServiceId, @AdjustmentId, @ContactId, @InsurancePolicyId, @FinancialInformationId, @BillingServiceId, @ClinicalServiceId, @ScheduleEntryId, @IsRetired, @PatientId, @EncounterLensesPrescriptionId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for Comment --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Value AS Value, @AppointmentId AS AppointmentId, @InsurerId AS InsurerId, @PatientInsuranceAuthorizationId AS PatientInsuranceAuthorizationId, @PatientInsuranceReferralId AS PatientInsuranceReferralId, @InvoiceId AS InvoiceId, @PatientReferralSourceId AS PatientReferralSourceId, @EncounterServiceId AS EncounterServiceId, @AdjustmentId AS AdjustmentId, @ContactId AS ContactId, @InsurancePolicyId AS InsurancePolicyId, @FinancialInformationId AS FinancialInformationId, @BillingServiceId AS BillingServiceId, @ClinicalServiceId AS ClinicalServiceId, @ScheduleEntryId AS ScheduleEntryId, @IsRetired AS IsRetired, @PatientId AS PatientId, @EncounterLensesPrescriptionId AS EncounterLensesPrescriptionId
			
				FETCH NEXT FROM CommentsDeletedCursor INTO @Id, @Value, @AppointmentId, @InsurerId, @PatientInsuranceAuthorizationId, @PatientInsuranceReferralId, @InvoiceId, @PatientReferralSourceId, @EncounterServiceId, @AdjustmentId, @ContactId, @InsurancePolicyId, @FinancialInformationId, @BillingServiceId, @ClinicalServiceId, @ScheduleEntryId, @IsRetired, @PatientId, @EncounterLensesPrescriptionId 
		
			END
		
			CLOSE CommentsDeletedCursor
			DEALLOCATE CommentsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnCommentInsert'
				,
				'
			CREATE TRIGGER model.OnCommentInsert ON [model].[Comments] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @Value nvarchar(max)
			DECLARE @AppointmentId int
			DECLARE @InsurerId int
			DECLARE @PatientInsuranceAuthorizationId int
			DECLARE @PatientInsuranceReferralId int
			DECLARE @InvoiceId int
			DECLARE @PatientReferralSourceId int
			DECLARE @EncounterServiceId int
			DECLARE @AdjustmentId int
			DECLARE @ContactId int
			DECLARE @InsurancePolicyId int
			DECLARE @FinancialInformationId int
			DECLARE @BillingServiceId int
			DECLARE @ClinicalServiceId int
			DECLARE @ScheduleEntryId int
			DECLARE @IsRetired bit
			DECLARE @PatientId int
			DECLARE @EncounterLensesPrescriptionId int
		
			DECLARE CommentsInsertedCursor CURSOR FOR
			SELECT Id, Value, AppointmentId, InsurerId, PatientInsuranceAuthorizationId, PatientInsuranceReferralId, InvoiceId, PatientReferralSourceId, EncounterServiceId, AdjustmentId, ContactId, InsurancePolicyId, FinancialInformationId, BillingServiceId, ClinicalServiceId, ScheduleEntryId, IsRetired, PatientId, EncounterLensesPrescriptionId FROM inserted
		
			OPEN CommentsInsertedCursor
			FETCH NEXT FROM CommentsInsertedCursor INTO @Id, @Value, @AppointmentId, @InsurerId, @PatientInsuranceAuthorizationId, @PatientInsuranceReferralId, @InvoiceId, @PatientReferralSourceId, @EncounterServiceId, @AdjustmentId, @ContactId, @InsurancePolicyId, @FinancialInformationId, @BillingServiceId, @ClinicalServiceId, @ScheduleEntryId, @IsRetired, @PatientId, @EncounterLensesPrescriptionId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for Comment --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(bigint, NULL)
			
				SELECT @Id AS Id, @Value AS Value, @AppointmentId AS AppointmentId, @InsurerId AS InsurerId, @PatientInsuranceAuthorizationId AS PatientInsuranceAuthorizationId, @PatientInsuranceReferralId AS PatientInsuranceReferralId, @InvoiceId AS InvoiceId, @PatientReferralSourceId AS PatientReferralSourceId, @EncounterServiceId AS EncounterServiceId, @AdjustmentId AS AdjustmentId, @ContactId AS ContactId, @InsurancePolicyId AS InsurancePolicyId, @FinancialInformationId AS FinancialInformationId, @BillingServiceId AS BillingServiceId, @ClinicalServiceId AS ClinicalServiceId, @ScheduleEntryId AS ScheduleEntryId, @IsRetired AS IsRetired, @PatientId AS PatientId, @EncounterLensesPrescriptionId AS EncounterLensesPrescriptionId
				
				FETCH NEXT FROM CommentsInsertedCursor INTO @Id, @Value, @AppointmentId, @InsurerId, @PatientInsuranceAuthorizationId, @PatientInsuranceReferralId, @InvoiceId, @PatientReferralSourceId, @EncounterServiceId, @AdjustmentId, @ContactId, @InsurancePolicyId, @FinancialInformationId, @BillingServiceId, @ClinicalServiceId, @ScheduleEntryId, @IsRetired, @PatientId, @EncounterLensesPrescriptionId 
		
			END
		
			CLOSE CommentsInsertedCursor
			DEALLOCATE CommentsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnCommentUpdate'
				,
				'
			CREATE TRIGGER model.OnCommentUpdate ON [model].[Comments] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @Value nvarchar(max)
			DECLARE @AppointmentId int
			DECLARE @InsurerId int
			DECLARE @PatientInsuranceAuthorizationId int
			DECLARE @PatientInsuranceReferralId int
			DECLARE @InvoiceId int
			DECLARE @PatientReferralSourceId int
			DECLARE @EncounterServiceId int
			DECLARE @AdjustmentId int
			DECLARE @ContactId int
			DECLARE @InsurancePolicyId int
			DECLARE @FinancialInformationId int
			DECLARE @BillingServiceId int
			DECLARE @ClinicalServiceId int
			DECLARE @ScheduleEntryId int
			DECLARE @IsRetired bit
			DECLARE @PatientId int
			DECLARE @EncounterLensesPrescriptionId int
		
			DECLARE CommentsUpdatedCursor CURSOR FOR
			SELECT Id, Value, AppointmentId, InsurerId, PatientInsuranceAuthorizationId, PatientInsuranceReferralId, InvoiceId, PatientReferralSourceId, EncounterServiceId, AdjustmentId, ContactId, InsurancePolicyId, FinancialInformationId, BillingServiceId, ClinicalServiceId, ScheduleEntryId, IsRetired, PatientId, EncounterLensesPrescriptionId FROM inserted
		
			OPEN CommentsUpdatedCursor
			FETCH NEXT FROM CommentsUpdatedCursor INTO @Id, @Value, @AppointmentId, @InsurerId, @PatientInsuranceAuthorizationId, @PatientInsuranceReferralId, @InvoiceId, @PatientReferralSourceId, @EncounterServiceId, @AdjustmentId, @ContactId, @InsurancePolicyId, @FinancialInformationId, @BillingServiceId, @ClinicalServiceId, @ScheduleEntryId, @IsRetired, @PatientId, @EncounterLensesPrescriptionId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for Comment --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM CommentsUpdatedCursor INTO @Id, @Value, @AppointmentId, @InsurerId, @PatientInsuranceAuthorizationId, @PatientInsuranceReferralId, @InvoiceId, @PatientReferralSourceId, @EncounterServiceId, @AdjustmentId, @ContactId, @InsurancePolicyId, @FinancialInformationId, @BillingServiceId, @ClinicalServiceId, @ScheduleEntryId, @IsRetired, @PatientId, @EncounterLensesPrescriptionId 
		
			END
		
			CLOSE CommentsUpdatedCursor
			DEALLOCATE CommentsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Contacts')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isContactsChanged BIT

		SET @isContactsChanged = 0

		DECLARE @ContactsViewSql NVARCHAR(max)

		SET @ContactsViewSql = '
		CREATE VIEW [model].[Contacts_Internal]
		WITH SCHEMABINDING
		AS
		----ExternalProvider contact
		SELECT VendorId AS Id,
			VendorName AS DisplayName,
			CASE 
				WHEN VendorFirmName <> '''' AND VendorFirmName IS NOT NULL
					THEN VendorId 
				ELSE NULL 
				END AS ExternalOrganizationId,
			VendorFirstName AS FirstName,
			VendorTitle AS Honorific,
			VendorLastName AS LastName,
			VendorMI AS MiddleName,
			VendorSalutation AS NickName,
			CONVERT(NVARCHAR, NULL) AS Prefix,
			VendorSalutation AS Salutation,
			CONVERT(NVARCHAR, NULL) AS Suffix,
			CONVERT(bit, 0) AS IsArchived,
			CASE  
				WHEN VendorExcRefDr = ''F'' OR VendorExcRefDr IS NULL OR VendorExcRefDr = ''''
					THEN CONVERT(bit, 0)
				WHEN VendorExcRefDr = ''T''
					THEN CONVERT(bit, 1)
				END AS ExcludeOnClaim,
			''ExternalProvider'' AS __EntityType__
		FROM dbo.PracticeVendors
		WHERE VendorLastName <> ''''
			AND VendorLastName IS NOT NULL'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Contacts_Internal'
			,@ContactsViewSql
			,@isChanged = @isContactsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @ContactsViewSql = '	
			CREATE VIEW [model].[Contacts]
			WITH SCHEMABINDING
			AS
			SELECT Id, FirstName, LastName, MiddleName, Suffix, Prefix, Honorific, Salutation, DisplayName, NickName, ExternalOrganizationId, IsArchived, ExcludeOnClaim, __EntityType__ FROM [model].[Contacts_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_Contacts'
							AND object_id = OBJECT_ID('[model].[Contacts_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[Contacts_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_Contacts] ON [model].[Contacts_Internal] ([Id]);
				END

				SET @ContactsViewSql = @ContactsViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Contacts'
				,@ContactsViewSql
				,@isChanged = @isContactsChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnContactDelete'
				,
				'
			CREATE TRIGGER model.OnContactDelete ON [model].[Contacts] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @FirstName nvarchar(max)
			DECLARE @LastName nvarchar(max)
			DECLARE @MiddleName nvarchar(max)
			DECLARE @Suffix nvarchar(max)
			DECLARE @Prefix nvarchar(max)
			DECLARE @Honorific nvarchar(max)
			DECLARE @Salutation nvarchar(max)
			DECLARE @DisplayName nvarchar(max)
			DECLARE @NickName nvarchar(max)
			DECLARE @ExternalOrganizationId int
			DECLARE @IsArchived bit
			DECLARE @ExcludeOnClaim bit
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE ContactsDeletedCursor CURSOR FOR
			SELECT Id, FirstName, LastName, MiddleName, Suffix, Prefix, Honorific, Salutation, DisplayName, NickName, ExternalOrganizationId, IsArchived, ExcludeOnClaim, __EntityType__ FROM deleted
		
			OPEN ContactsDeletedCursor
			FETCH NEXT FROM ContactsDeletedCursor INTO @Id, @FirstName, @LastName, @MiddleName, @Suffix, @Prefix, @Honorific, @Salutation, @DisplayName, @NickName, @ExternalOrganizationId, @IsArchived, @ExcludeOnClaim, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Contact Delete
				DELETE
				FROM dbo.PracticeVendors
				WHERE VendorId = @Id
			
				SELECT @Id AS Id, @FirstName AS FirstName, @LastName AS LastName, @MiddleName AS MiddleName, @Suffix AS Suffix, @Prefix AS Prefix, @Honorific AS Honorific, @Salutation AS Salutation, @DisplayName AS DisplayName, @NickName AS NickName, @ExternalOrganizationId AS ExternalOrganizationId, @IsArchived AS IsArchived, @ExcludeOnClaim AS ExcludeOnClaim, @__EntityType__ AS __EntityType__
			
				FETCH NEXT FROM ContactsDeletedCursor INTO @Id, @FirstName, @LastName, @MiddleName, @Suffix, @Prefix, @Honorific, @Salutation, @DisplayName, @NickName, @ExternalOrganizationId, @IsArchived, @ExcludeOnClaim, @__EntityType__ 
		
			END
		
			CLOSE ContactsDeletedCursor
			DEALLOCATE ContactsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnContactInsert'
				,
				'
			CREATE TRIGGER model.OnContactInsert ON [model].[Contacts] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @FirstName nvarchar(max)
			DECLARE @LastName nvarchar(max)
			DECLARE @MiddleName nvarchar(max)
			DECLARE @Suffix nvarchar(max)
			DECLARE @Prefix nvarchar(max)
			DECLARE @Honorific nvarchar(max)
			DECLARE @Salutation nvarchar(max)
			DECLARE @DisplayName nvarchar(max)
			DECLARE @NickName nvarchar(max)
			DECLARE @ExternalOrganizationId int
			DECLARE @IsArchived bit
			DECLARE @ExcludeOnClaim bit
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE ContactsInsertedCursor CURSOR FOR
			SELECT Id, FirstName, LastName, MiddleName, Suffix, Prefix, Honorific, Salutation, DisplayName, NickName, ExternalOrganizationId, IsArchived, ExcludeOnClaim, __EntityType__ FROM inserted
		
			OPEN ContactsInsertedCursor
			FETCH NEXT FROM ContactsInsertedCursor INTO @Id, @FirstName, @LastName, @MiddleName, @Suffix, @Prefix, @Honorific, @Salutation, @DisplayName, @NickName, @ExternalOrganizationId, @IsArchived, @ExcludeOnClaim, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- Contact Insert
				INSERT INTO dbo.PracticeVendors (vendorname)
				VALUES ('''')
			
				SET @Id = IDENT_CURRENT(''PracticeVendors'')
			
							
				-- Contact Update/Insert
				UPDATE dbo.PracticeVendors
				SET VendorName = @DisplayName,
					VendorType = ''D'',
					VendorLastName = @LastName,
					VendorFirstName = @FirstName,
					VendorMI = @MiddleName,
					VendorTitle = @Honorific,
					VendorSalutation = @Salutation,
					VendorExcRefDr = CASE
						WHEN @ExcludeOnClaim = 1
							THEN ''T''
						WHEN @ExcludeOnClaim = 0
							THEN ''F''
						ELSE NULL END
				WHERE VendorId = @Id
			
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @FirstName AS FirstName, @LastName AS LastName, @MiddleName AS MiddleName, @Suffix AS Suffix, @Prefix AS Prefix, @Honorific AS Honorific, @Salutation AS Salutation, @DisplayName AS DisplayName, @NickName AS NickName, @ExternalOrganizationId AS ExternalOrganizationId, @IsArchived AS IsArchived, @ExcludeOnClaim AS ExcludeOnClaim, @__EntityType__ AS __EntityType__
				
				FETCH NEXT FROM ContactsInsertedCursor INTO @Id, @FirstName, @LastName, @MiddleName, @Suffix, @Prefix, @Honorific, @Salutation, @DisplayName, @NickName, @ExternalOrganizationId, @IsArchived, @ExcludeOnClaim, @__EntityType__ 
		
			END
		
			CLOSE ContactsInsertedCursor
			DEALLOCATE ContactsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnContactUpdate'
				,
				'
			CREATE TRIGGER model.OnContactUpdate ON [model].[Contacts] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @FirstName nvarchar(max)
			DECLARE @LastName nvarchar(max)
			DECLARE @MiddleName nvarchar(max)
			DECLARE @Suffix nvarchar(max)
			DECLARE @Prefix nvarchar(max)
			DECLARE @Honorific nvarchar(max)
			DECLARE @Salutation nvarchar(max)
			DECLARE @DisplayName nvarchar(max)
			DECLARE @NickName nvarchar(max)
			DECLARE @ExternalOrganizationId int
			DECLARE @IsArchived bit
			DECLARE @ExcludeOnClaim bit
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE ContactsUpdatedCursor CURSOR FOR
			SELECT Id, FirstName, LastName, MiddleName, Suffix, Prefix, Honorific, Salutation, DisplayName, NickName, ExternalOrganizationId, IsArchived, ExcludeOnClaim, __EntityType__ FROM inserted
		
			OPEN ContactsUpdatedCursor
			FETCH NEXT FROM ContactsUpdatedCursor INTO @Id, @FirstName, @LastName, @MiddleName, @Suffix, @Prefix, @Honorific, @Salutation, @DisplayName, @NickName, @ExternalOrganizationId, @IsArchived, @ExcludeOnClaim, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- Contact Update/Insert
				UPDATE dbo.PracticeVendors
				SET VendorName = @DisplayName,
					VendorType = ''D'',
					VendorLastName = @LastName,
					VendorFirstName = @FirstName,
					VendorMI = @MiddleName,
					VendorTitle = @Honorific,
					VendorSalutation = @Salutation,
					VendorExcRefDr = CASE
						WHEN @ExcludeOnClaim = 1
							THEN ''T''
						WHEN @ExcludeOnClaim = 0
							THEN ''F''
						ELSE NULL END
				WHERE VendorId = @Id
			
				
				FETCH NEXT FROM ContactsUpdatedCursor INTO @Id, @FirstName, @LastName, @MiddleName, @Suffix, @Prefix, @Honorific, @Salutation, @DisplayName, @NickName, @ExternalOrganizationId, @IsArchived, @ExcludeOnClaim, @__EntityType__ 
		
			END
		
			CLOSE ContactsUpdatedCursor
			DEALLOCATE ContactsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.DoctorClinicalSpecialtyTypes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isDoctorClinicalSpecialtyTypesChanged BIT

		SET @isDoctorClinicalSpecialtyTypesChanged = 0

		DECLARE @DoctorClinicalSpecialtyTypesViewSql NVARCHAR(max)

		SET @DoctorClinicalSpecialtyTypesViewSql = '
		CREATE VIEW [model].[DoctorClinicalSpecialtyTypes]
		WITH SCHEMABINDING
		AS
		SELECT re.ResourceId AS Id,
			1 AS OrdinalId,
			re.ResourceId AS DoctorId,
			pct.Id AS ClinicalSpecialtyTypeId
		FROM dbo.Resources re
		LEFT JOIN dbo.PracticeCodeTable pct ON pct.code = re.ResourceSpecialty
			AND pct.ReferenceType = ''SPECIALTY''
		WHERE pct.Id <> ''''
			AND pct.Id IS NOT NULL'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.DoctorClinicalSpecialtyTypes'
			,@DoctorClinicalSpecialtyTypesViewSql
			,@isChanged = @isDoctorClinicalSpecialtyTypesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnDoctorClinicalSpecialtyTypeDelete'
				,
				'
			CREATE TRIGGER model.OnDoctorClinicalSpecialtyTypeDelete ON [model].[DoctorClinicalSpecialtyTypes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @OrdinalId int
			DECLARE @ClinicalSpecialtyTypeId int
			DECLARE @DoctorId int
		
			DECLARE DoctorClinicalSpecialtyTypesDeletedCursor CURSOR FOR
			SELECT Id, OrdinalId, ClinicalSpecialtyTypeId, DoctorId FROM deleted
		
			OPEN DoctorClinicalSpecialtyTypesDeletedCursor
			FETCH NEXT FROM DoctorClinicalSpecialtyTypesDeletedCursor INTO @Id, @OrdinalId, @ClinicalSpecialtyTypeId, @DoctorId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for DoctorClinicalSpecialtyType --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @OrdinalId AS OrdinalId, @ClinicalSpecialtyTypeId AS ClinicalSpecialtyTypeId, @DoctorId AS DoctorId
			
				FETCH NEXT FROM DoctorClinicalSpecialtyTypesDeletedCursor INTO @Id, @OrdinalId, @ClinicalSpecialtyTypeId, @DoctorId 
		
			END
		
			CLOSE DoctorClinicalSpecialtyTypesDeletedCursor
			DEALLOCATE DoctorClinicalSpecialtyTypesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnDoctorClinicalSpecialtyTypeInsert'
				,
				'
			CREATE TRIGGER model.OnDoctorClinicalSpecialtyTypeInsert ON [model].[DoctorClinicalSpecialtyTypes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @OrdinalId int
			DECLARE @ClinicalSpecialtyTypeId int
			DECLARE @DoctorId int
		
			DECLARE DoctorClinicalSpecialtyTypesInsertedCursor CURSOR FOR
			SELECT Id, OrdinalId, ClinicalSpecialtyTypeId, DoctorId FROM inserted
		
			OPEN DoctorClinicalSpecialtyTypesInsertedCursor
			FETCH NEXT FROM DoctorClinicalSpecialtyTypesInsertedCursor INTO @Id, @OrdinalId, @ClinicalSpecialtyTypeId, @DoctorId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for DoctorClinicalSpecialtyType --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @OrdinalId AS OrdinalId, @ClinicalSpecialtyTypeId AS ClinicalSpecialtyTypeId, @DoctorId AS DoctorId
				
				FETCH NEXT FROM DoctorClinicalSpecialtyTypesInsertedCursor INTO @Id, @OrdinalId, @ClinicalSpecialtyTypeId, @DoctorId 
		
			END
		
			CLOSE DoctorClinicalSpecialtyTypesInsertedCursor
			DEALLOCATE DoctorClinicalSpecialtyTypesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnDoctorClinicalSpecialtyTypeUpdate'
				,'
			CREATE TRIGGER model.OnDoctorClinicalSpecialtyTypeUpdate ON [model].[DoctorClinicalSpecialtyTypes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @OrdinalId int
			DECLARE @ClinicalSpecialtyTypeId int
			DECLARE @DoctorId int
		
			DECLARE DoctorClinicalSpecialtyTypesUpdatedCursor CURSOR FOR
			SELECT Id, OrdinalId, ClinicalSpecialtyTypeId, DoctorId FROM inserted
		
			OPEN DoctorClinicalSpecialtyTypesUpdatedCursor
			FETCH NEXT FROM DoctorClinicalSpecialtyTypesUpdatedCursor INTO @Id, @OrdinalId, @ClinicalSpecialtyTypeId, @DoctorId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for DoctorClinicalSpecialtyType --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM DoctorClinicalSpecialtyTypesUpdatedCursor INTO @Id, @OrdinalId, @ClinicalSpecialtyTypeId, @DoctorId 
		
			END
		
			CLOSE DoctorClinicalSpecialtyTypesUpdatedCursor
			DEALLOCATE DoctorClinicalSpecialtyTypesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.DoctorInsurerAssignments')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isDoctorInsurerAssignmentsChanged BIT

		SET @isDoctorInsurerAssignmentsChanged = 0

		DECLARE @DoctorInsurerAssignmentsViewSql NVARCHAR(max)

		SET @DoctorInsurerAssignmentsViewSql = '
		CREATE VIEW [model].[DoctorInsurerAssignments_Internal]
		WITH SCHEMABINDING
		AS
		SELECT (CONVERT(bigint, 110000000) * ResourceId + InsurerId) AS Id,
			InsurerId AS InsurerId,
			ResourceId AS DoctorId,
			CONVERT(bit, 0) AS IsNoAssignment,
			CASE
				WHEN pic.FieldValue <> ''T''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
			END AS SuppressPatientPayments
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerReferenceCode IN (''N'', ''M'', ''['')
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON FieldReference = ''RPTPATPAY''
		WHERE re.ResourceType in (''D'', ''Q'', ''Z'', ''Y'')'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.DoctorInsurerAssignments_Internal'
			,@DoctorInsurerAssignmentsViewSql
			,@isChanged = @isDoctorInsurerAssignmentsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @DoctorInsurerAssignmentsViewSql = '	
			CREATE VIEW [model].[DoctorInsurerAssignments]
			WITH SCHEMABINDING
			AS
			SELECT Id, IsNoAssignment, InsurerId, DoctorId, SuppressPatientPayments FROM [model].[DoctorInsurerAssignments_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_DoctorInsurerAssignments'
							AND object_id = OBJECT_ID('[model].[DoctorInsurerAssignments_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[DoctorInsurerAssignments_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_DoctorInsurerAssignments] ON [model].[DoctorInsurerAssignments_Internal] ([Id]);
				END

				SET @DoctorInsurerAssignmentsViewSql = @DoctorInsurerAssignmentsViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.DoctorInsurerAssignments'
				,@DoctorInsurerAssignmentsViewSql
				,@isChanged = @isDoctorInsurerAssignmentsChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnDoctorInsurerAssignmentDelete'
				,
				'
			CREATE TRIGGER model.OnDoctorInsurerAssignmentDelete ON [model].[DoctorInsurerAssignments] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @IsNoAssignment bit
			DECLARE @InsurerId int
			DECLARE @DoctorId int
			DECLARE @SuppressPatientPayments bit
		
			DECLARE DoctorInsurerAssignmentsDeletedCursor CURSOR FOR
			SELECT Id, IsNoAssignment, InsurerId, DoctorId, SuppressPatientPayments FROM deleted
		
			OPEN DoctorInsurerAssignmentsDeletedCursor
			FETCH NEXT FROM DoctorInsurerAssignmentsDeletedCursor INTO @Id, @IsNoAssignment, @InsurerId, @DoctorId, @SuppressPatientPayments 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for DoctorInsurerAssignment --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @IsNoAssignment AS IsNoAssignment, @InsurerId AS InsurerId, @DoctorId AS DoctorId, @SuppressPatientPayments AS SuppressPatientPayments
			
				FETCH NEXT FROM DoctorInsurerAssignmentsDeletedCursor INTO @Id, @IsNoAssignment, @InsurerId, @DoctorId, @SuppressPatientPayments 
		
			END
		
			CLOSE DoctorInsurerAssignmentsDeletedCursor
			DEALLOCATE DoctorInsurerAssignmentsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnDoctorInsurerAssignmentInsert'
				,
				'
			CREATE TRIGGER model.OnDoctorInsurerAssignmentInsert ON [model].[DoctorInsurerAssignments] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @IsNoAssignment bit
			DECLARE @InsurerId int
			DECLARE @DoctorId int
			DECLARE @SuppressPatientPayments bit
		
			DECLARE DoctorInsurerAssignmentsInsertedCursor CURSOR FOR
			SELECT Id, IsNoAssignment, InsurerId, DoctorId, SuppressPatientPayments FROM inserted
		
			OPEN DoctorInsurerAssignmentsInsertedCursor
			FETCH NEXT FROM DoctorInsurerAssignmentsInsertedCursor INTO @Id, @IsNoAssignment, @InsurerId, @DoctorId, @SuppressPatientPayments 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for DoctorInsurerAssignment --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(bigint, NULL)
			
				SELECT @Id AS Id, @IsNoAssignment AS IsNoAssignment, @InsurerId AS InsurerId, @DoctorId AS DoctorId, @SuppressPatientPayments AS SuppressPatientPayments
				
				FETCH NEXT FROM DoctorInsurerAssignmentsInsertedCursor INTO @Id, @IsNoAssignment, @InsurerId, @DoctorId, @SuppressPatientPayments 
		
			END
		
			CLOSE DoctorInsurerAssignmentsInsertedCursor
			DEALLOCATE DoctorInsurerAssignmentsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnDoctorInsurerAssignmentUpdate'
				,
				'
			CREATE TRIGGER model.OnDoctorInsurerAssignmentUpdate ON [model].[DoctorInsurerAssignments] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @IsNoAssignment bit
			DECLARE @InsurerId int
			DECLARE @DoctorId int
			DECLARE @SuppressPatientPayments bit
		
			DECLARE DoctorInsurerAssignmentsUpdatedCursor CURSOR FOR
			SELECT Id, IsNoAssignment, InsurerId, DoctorId, SuppressPatientPayments FROM inserted
		
			OPEN DoctorInsurerAssignmentsUpdatedCursor
			FETCH NEXT FROM DoctorInsurerAssignmentsUpdatedCursor INTO @Id, @IsNoAssignment, @InsurerId, @DoctorId, @SuppressPatientPayments 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for DoctorInsurerAssignment --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM DoctorInsurerAssignmentsUpdatedCursor INTO @Id, @IsNoAssignment, @InsurerId, @DoctorId, @SuppressPatientPayments 
		
			END
		
			CLOSE DoctorInsurerAssignmentsUpdatedCursor
			DEALLOCATE DoctorInsurerAssignmentsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.EmailAddresses')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isEmailAddressesChanged BIT

		SET @isEmailAddressesChanged = 0

		DECLARE @ismodelEmailAddresses_Partition1Changed BIT

		SET @ismodelEmailAddresses_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EmailAddresses_Partition1'
			,'
		CREATE VIEW model.EmailAddresses_Partition1
		WITH SCHEMABINDING
		AS
		----EmailAddress BillingOrganization PracticeName table
		SELECT (110000000 * 1 + pn.PracticeId) AS Id,
			pn.PracticeId AS BillingOrganizationId,
			NULL AS ClaimFileReceiverId,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			0 AS OrdinalId,
			NULL AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			pn.PracticeEmail AS Value
		FROM dbo.PracticeName pn
		WHERE PracticeEmail <> ''''
			AND PracticeEmail IS NOT NULL
			AND PracticeType = ''P''
		'
			,@isChanged = @ismodelEmailAddresses_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_EmailAddresses_Partition1'
					AND object_id = OBJECT_ID('model.EmailAddresses_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.EmailAddresses_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_EmailAddresses_Partition1 ON model.EmailAddresses_Partition1 ([Id]);
		END

		DECLARE @ismodelEmailAddresses_Partition2Changed BIT

		SET @ismodelEmailAddresses_Partition2Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EmailAddresses_Partition2'
			,'
		CREATE VIEW model.EmailAddresses_Partition2
		WITH SCHEMABINDING
		AS
		----EmailAddress BillingOrganization Resources table
		SELECT (110000000 * 2 + re.ResourceId) AS Id,
			(110000000 * 1 + re.ResourceId) AS BillingOrganizationId,
			NULL AS ClaimFileReceiverId,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			0 AS OrdinalId,
			NULL AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			re.ResourceEmail AS Value
		FROM dbo.Resources re
		WHERE re.ResourceType = ''R''
			AND re.ServiceCode = ''02''
			AND re.ResourceEmail <> ''''
			AND re.ResourceEmail IS NOT NULL
		'
			,@isChanged = @ismodelEmailAddresses_Partition2Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_EmailAddresses_Partition2'
					AND object_id = OBJECT_ID('model.EmailAddresses_Partition2')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.EmailAddresses_Partition2'

			CREATE UNIQUE CLUSTERED INDEX PK_EmailAddresses_Partition2 ON model.EmailAddresses_Partition2 ([Id]);
		END

		DECLARE @ismodelEmailAddresses_Partition3Changed BIT

		SET @ismodelEmailAddresses_Partition3Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EmailAddresses_Partition3'
			,'
		CREATE VIEW model.EmailAddresses_Partition3
		WITH SCHEMABINDING
		AS
		----EmailAddress BillingOrganization Resources table - OrgOverride [to do]
	
		----EmailAddress ClaimFileReceiverId - IO currently doesn''t support this email address
	
	
		----EmailAddress ExternalOrganization
		SELECT (110000000 * 6 + VendorId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ClaimFileReceiverId,
			NULL AS ContactId,
			VendorId AS ExternalOrganizationId,
			NULL AS InsurerId,
			0 AS OrdinalId,
			NULL AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			VendorEmail AS Value
		FROM dbo.PracticeVendors
		WHERE VendorEmail <> ''''
			AND VendorEmail IS NOT NULL
			AND VendorFirmName <> ''''
			AND VendorFirmName IS NOT NULL
		'
			,@isChanged = @ismodelEmailAddresses_Partition3Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_EmailAddresses_Partition3'
					AND object_id = OBJECT_ID('model.EmailAddresses_Partition3')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.EmailAddresses_Partition3'

			CREATE UNIQUE CLUSTERED INDEX PK_EmailAddresses_Partition3 ON model.EmailAddresses_Partition3 ([Id]);
		END

		DECLARE @ismodelEmailAddresses_Partition4Changed BIT

		SET @ismodelEmailAddresses_Partition4Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EmailAddresses_Partition4'
			,'
		CREATE VIEW model.EmailAddresses_Partition4
		WITH SCHEMABINDING
		AS
		----EmailAddress Contact-ExternalProvider
		SELECT (110000000 * 7 + VendorId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ClaimFileReceiverId,
			VendorId AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			0 AS OrdinalId,
			NULL AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			VendorEmail AS Value
		FROM dbo.PracticeVendors
		WHERE VendorEmail <> ''''
			AND VendorEmail IS NOT NULL
			AND VendorLastName <> ''''
			AND VendorLastName IS NOT NULL
		'
			,@isChanged = @ismodelEmailAddresses_Partition4Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_EmailAddresses_Partition4'
					AND object_id = OBJECT_ID('model.EmailAddresses_Partition4')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.EmailAddresses_Partition4'

			CREATE UNIQUE CLUSTERED INDEX PK_EmailAddresses_Partition4 ON model.EmailAddresses_Partition4 ([Id]);
		END

		DECLARE @ismodelEmailAddresses_Partition5Changed BIT

		SET @ismodelEmailAddresses_Partition5Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EmailAddresses_Partition5'
			,'
		CREATE VIEW model.EmailAddresses_Partition5
		WITH SCHEMABINDING
		AS
		----EmailAddress Patient 
		SELECT (110000000 * 5 + PatientId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ClaimFileReceiverId,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			0 AS OrdinalId,
			PatientId AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			Email AS Value
		FROM dbo.PatientDemographics
		WHERE Email <> ''''
			AND Email IS NOT NULL
		'
			,@isChanged = @ismodelEmailAddresses_Partition5Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_EmailAddresses_Partition5'
					AND object_id = OBJECT_ID('model.EmailAddresses_Partition5')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.EmailAddresses_Partition5'

			CREATE UNIQUE CLUSTERED INDEX PK_EmailAddresses_Partition5 ON model.EmailAddresses_Partition5 ([Id]);
		END

		DECLARE @ismodelEmailAddresses_Partition6Changed BIT

		SET @ismodelEmailAddresses_Partition6Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EmailAddresses_Partition6'
			,'
		CREATE VIEW model.EmailAddresses_Partition6
		WITH SCHEMABINDING
		AS
		----EmailAddress ServiceLocation PracticeName table
		SELECT (110000000 * 3 + pn.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ClaimFileReceiverId,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			0 AS OrdinalId,
			NULL AS PatientId,
			pn.PracticeId AS ServiceLocationId,
			NULL AS UserId,
			PracticeEmail AS Value
		FROM dbo.PracticeName pn
		WHERE PracticeEmail <> ''''
			AND PracticeEmail IS NOT NULL
			AND PracticeType = ''P''
		'
			,@isChanged = @ismodelEmailAddresses_Partition6Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_EmailAddresses_Partition6'
					AND object_id = OBJECT_ID('model.EmailAddresses_Partition6')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.EmailAddresses_Partition6'

			CREATE UNIQUE CLUSTERED INDEX PK_EmailAddresses_Partition6 ON model.EmailAddresses_Partition6 ([Id]);
		END

		DECLARE @ismodelEmailAddresses_Partition7Changed BIT

		SET @ismodelEmailAddresses_Partition7Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EmailAddresses_Partition7'
			,'
		CREATE VIEW model.EmailAddresses_Partition7
		WITH SCHEMABINDING
		AS
		----EmailAddress User
		SELECT re.ResourceId AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ClaimFileReceiverId,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			0 AS OrdinalId,
			NULL AS PatientId,
			NULL AS ServiceLocationId,
			re.ResourceId AS UserId,
			re.ResourceEmail AS Value
		FROM dbo.Resources re
		WHERE re.ResourceType <> ''R''
			AND re.ResourceEmail <> ''''
			AND re.ResourceEmail IS NOT NULL
		'
			,@isChanged = @ismodelEmailAddresses_Partition7Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_EmailAddresses_Partition7'
					AND object_id = OBJECT_ID('model.EmailAddresses_Partition7')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.EmailAddresses_Partition7'

			CREATE UNIQUE CLUSTERED INDEX PK_EmailAddresses_Partition7 ON model.EmailAddresses_Partition7 ([Id]);
		END

		DECLARE @EmailAddressesViewSql NVARCHAR(max)

		SET @EmailAddressesViewSql = 
			'
		CREATE VIEW [model].[EmailAddresses]
		WITH SCHEMABINDING
		AS
		SELECT model.EmailAddresses_Partition1.Id AS Id, model.EmailAddresses_Partition1.BillingOrganizationId AS BillingOrganizationId, model.EmailAddresses_Partition1.ClaimFileReceiverId AS ClaimFileReceiverId, model.EmailAddresses_Partition1.ContactId AS ContactId, model.EmailAddresses_Partition1.ExternalOrganizationId AS ExternalOrganizationId, model.EmailAddresses_Partition1.InsurerId AS InsurerId, model.EmailAddresses_Partition1.OrdinalId AS OrdinalId, model.EmailAddresses_Partition1.PatientId AS PatientId, model.EmailAddresses_Partition1.ServiceLocationId AS ServiceLocationId, model.EmailAddresses_Partition1.UserId AS UserId, model.EmailAddresses_Partition1.Value AS Value FROM model.EmailAddresses_Partition1 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.EmailAddresses_Partition2.Id AS Id, model.EmailAddresses_Partition2.BillingOrganizationId AS BillingOrganizationId, model.EmailAddresses_Partition2.ClaimFileReceiverId AS ClaimFileReceiverId, model.EmailAddresses_Partition2.ContactId AS ContactId, model.EmailAddresses_Partition2.ExternalOrganizationId AS ExternalOrganizationId, model.EmailAddresses_Partition2.InsurerId AS InsurerId, model.EmailAddresses_Partition2.OrdinalId AS OrdinalId, model.EmailAddresses_Partition2.PatientId AS PatientId, model.EmailAddresses_Partition2.ServiceLocationId AS ServiceLocationId, model.EmailAddresses_Partition2.UserId AS UserId, model.EmailAddresses_Partition2.Value AS Value FROM model.EmailAddresses_Partition2 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.EmailAddresses_Partition3.Id AS Id, model.EmailAddresses_Partition3.BillingOrganizationId AS BillingOrganizationId, model.EmailAddresses_Partition3.ClaimFileReceiverId AS ClaimFileReceiverId, model.EmailAddresses_Partition3.ContactId AS ContactId, model.EmailAddresses_Partition3.ExternalOrganizationId AS ExternalOrganizationId, model.EmailAddresses_Partition3.InsurerId AS InsurerId, model.EmailAddresses_Partition3.OrdinalId AS OrdinalId, model.EmailAddresses_Partition3.PatientId AS PatientId, model.EmailAddresses_Partition3.ServiceLocationId AS ServiceLocationId, model.EmailAddresses_Partition3.UserId AS UserId, model.EmailAddresses_Partition3.Value AS Value FROM model.EmailAddresses_Partition3 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.EmailAddresses_Partition4.Id AS Id, model.EmailAddresses_Partition4.BillingOrganizationId AS BillingOrganizationId, model.EmailAddresses_Partition4.ClaimFileReceiverId AS ClaimFileReceiverId, model.EmailAddresses_Partition4.ContactId AS ContactId, model.EmailAddresses_Partition4.ExternalOrganizationId AS ExternalOrganizationId, model.EmailAddresses_Partition4.InsurerId AS InsurerId, model.EmailAddresses_Partition4.OrdinalId AS OrdinalId, model.EmailAddresses_Partition4.PatientId AS PatientId, model.EmailAddresses_Partition4.ServiceLocationId AS ServiceLocationId, model.EmailAddresses_Partition4.UserId AS UserId, model.EmailAddresses_Partition4.Value AS Value FROM model.EmailAddresses_Partition4 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.EmailAddresses_Partition5.Id AS Id, model.EmailAddresses_Partition5.BillingOrganizationId AS BillingOrganizationId, model.EmailAddresses_Partition5.ClaimFileReceiverId AS ClaimFileReceiverId, model.EmailAddresses_Partition5.ContactId AS ContactId, model.EmailAddresses_Partition5.ExternalOrganizationId AS ExternalOrganizationId, model.EmailAddresses_Partition5.InsurerId AS InsurerId, model.EmailAddresses_Partition5.OrdinalId AS OrdinalId, model.EmailAddresses_Partition5.PatientId AS PatientId, model.EmailAddresses_Partition5.ServiceLocationId AS ServiceLocationId, model.EmailAddresses_Partition5.UserId AS UserId, model.EmailAddresses_Partition5.Value AS Value FROM model.EmailAddresses_Partition5 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.EmailAddresses_Partition6.Id AS Id, model.EmailAddresses_Partition6.BillingOrganizationId AS BillingOrganizationId, model.EmailAddresses_Partition6.ClaimFileReceiverId AS ClaimFileReceiverId, model.EmailAddresses_Partition6.ContactId AS ContactId, model.EmailAddresses_Partition6.ExternalOrganizationId AS ExternalOrganizationId, model.EmailAddresses_Partition6.InsurerId AS InsurerId, model.EmailAddresses_Partition6.OrdinalId AS OrdinalId, model.EmailAddresses_Partition6.PatientId AS PatientId, model.EmailAddresses_Partition6.ServiceLocationId AS ServiceLocationId, model.EmailAddresses_Partition6.UserId AS UserId, model.EmailAddresses_Partition6.Value AS Value FROM model.EmailAddresses_Partition6 WITH(NOEXPAND)
	
		UNION ALL
	
		----EmailAddress ServiceLocation Resources table
		SELECT (110000000 * 4 + re.ResourceId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ClaimFileReceiverId,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			0 AS OrdinalId,
			NULL AS PatientId,
			(110000000 * 1 + re.ResourceId) AS ServiceLocationId,
			NULL AS UserId,
			ResourceEmail AS Value
		FROM dbo.Resources re
		LEFT JOIN dbo.PracticeName pn ON pn.PracticeId = re.PracticeId
		LEFT JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		WHERE ResourceEmail <> ''''
			AND re.ResourceEmail IS NOT NULL
			AND re.ResourceType = ''R''
			AND re.ServiceCode = ''02''
			AND CASE 
				WHEN pic.FieldValue = ''T''
					AND re.Billable = ''Y''
					AND pn.LocationReference <> ''''
					THEN 1
				ELSE 0
				END <> 1
	
		UNION ALL
	
		SELECT model.EmailAddresses_Partition7.Id AS Id, model.EmailAddresses_Partition7.BillingOrganizationId AS BillingOrganizationId, model.EmailAddresses_Partition7.ClaimFileReceiverId AS ClaimFileReceiverId, model.EmailAddresses_Partition7.ContactId AS ContactId, model.EmailAddresses_Partition7.ExternalOrganizationId AS ExternalOrganizationId, model.EmailAddresses_Partition7.InsurerId AS InsurerId, model.EmailAddresses_Partition7.OrdinalId AS OrdinalId, model.EmailAddresses_Partition7.PatientId AS PatientId, model.EmailAddresses_Partition7.ServiceLocationId AS ServiceLocationId, model.EmailAddresses_Partition7.UserId AS UserId, model.EmailAddresses_Partition7.Value AS Value FROM model.EmailAddresses_Partition7 WITH(NOEXPAND)'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EmailAddresses'
			,@EmailAddressesViewSql
			,@isChanged = @isEmailAddressesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEmailAddressDelete'
				,
				'
			CREATE TRIGGER model.OnEmailAddressDelete ON [model].[EmailAddresses] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Value nvarchar(max)
			DECLARE @OrdinalId int
			DECLARE @UserId int
			DECLARE @BillingOrganizationId int
			DECLARE @ServiceLocationId int
			DECLARE @ContactId int
			DECLARE @ExternalOrganizationId int
			DECLARE @InsurerId int
			DECLARE @PatientId int
			DECLARE @ClaimFileReceiverId int
		
			DECLARE EmailAddressesDeletedCursor CURSOR FOR
			SELECT Id, Value, OrdinalId, UserId, BillingOrganizationId, ServiceLocationId, ContactId, ExternalOrganizationId, InsurerId, PatientId, ClaimFileReceiverId FROM deleted
		
			OPEN EmailAddressesDeletedCursor
			FETCH NEXT FROM EmailAddressesDeletedCursor INTO @Id, @Value, @OrdinalId, @UserId, @BillingOrganizationId, @ServiceLocationId, @ContactId, @ExternalOrganizationId, @InsurerId, @PatientId, @ClaimFileReceiverId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for EmailAddress --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Value AS Value, @OrdinalId AS OrdinalId, @UserId AS UserId, @BillingOrganizationId AS BillingOrganizationId, @ServiceLocationId AS ServiceLocationId, @ContactId AS ContactId, @ExternalOrganizationId AS ExternalOrganizationId, @InsurerId AS InsurerId, @PatientId AS PatientId, @ClaimFileReceiverId AS ClaimFileReceiverId
			
				FETCH NEXT FROM EmailAddressesDeletedCursor INTO @Id, @Value, @OrdinalId, @UserId, @BillingOrganizationId, @ServiceLocationId, @ContactId, @ExternalOrganizationId, @InsurerId, @PatientId, @ClaimFileReceiverId 
		
			END
		
			CLOSE EmailAddressesDeletedCursor
			DEALLOCATE EmailAddressesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEmailAddressInsert'
				,
				'
			CREATE TRIGGER model.OnEmailAddressInsert ON [model].[EmailAddresses] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Value nvarchar(max)
			DECLARE @OrdinalId int
			DECLARE @UserId int
			DECLARE @BillingOrganizationId int
			DECLARE @ServiceLocationId int
			DECLARE @ContactId int
			DECLARE @ExternalOrganizationId int
			DECLARE @InsurerId int
			DECLARE @PatientId int
			DECLARE @ClaimFileReceiverId int
		
			DECLARE EmailAddressesInsertedCursor CURSOR FOR
			SELECT Id, Value, OrdinalId, UserId, BillingOrganizationId, ServiceLocationId, ContactId, ExternalOrganizationId, InsurerId, PatientId, ClaimFileReceiverId FROM inserted
		
			OPEN EmailAddressesInsertedCursor
			FETCH NEXT FROM EmailAddressesInsertedCursor INTO @Id, @Value, @OrdinalId, @UserId, @BillingOrganizationId, @ServiceLocationId, @ContactId, @ExternalOrganizationId, @InsurerId, @PatientId, @ClaimFileReceiverId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- EmailAddress Insert
				IF @BillingOrganizationId IS NOT NULL
					SET @Id = (
							SELECT model.GetPairId(CASE x
										WHEN 0
											THEN 1
										WHEN 1
											THEN 2
										END, y, 110000000)
							FROM model.GetIdPair(@BillingOrganizationId, 110000000)
							)
			
				IF @ExternalOrganizationId IS NOT NULL
					SET @Id = model.GetPairId(6, @ExternalOrganizationId, 110000000)
			
				IF @ContactId IS NOT NULL
					SET @Id = model.GetPairId(7, @ContactId, 110000000)
			
				IF @PatientId IS NOT NULL
					SET @Id = model.GetPairId(5, @PatientId, 110000000)
			
				IF @ServiceLocationId IS NOT NULL
					SET @Id = (
							SELECT model.GetPairId(CASE x
										WHEN 0
											THEN 3
										WHEN 1
											THEN 4
										END, y, 110000000)
							FROM model.GetIdPair(@ServiceLocationId, 110000000)
							)
			
				IF @UserId IS NOT NULL
					SET @Id = model.GetPairId(0, @UserId, 110000000)
			
							
				-- EmailAddress Update/Insert
				DECLARE @pair TABLE (
					x int,
					y int
					)
				DECLARE @x int,
					@y int
			
				SELECT @x = x,
					@y = y
				FROM model.GetIdPair(@Id, 110000000)
			
				IF @x IN (
						0,
						2,
						4
						)
					UPDATE Resources
					SET ResourceEmail = @Value
					WHERE ResourceId = @y
			
				IF @x IN (
						1,
						3
						)
					UPDATE PracticeName
					SET PracticeEmail = @Value
					WHERE PracticeId = @y
			
				IF @x = 5
					UPDATE PatientDemographics
					SET Email = @Value
					WHERE PatientId = @y
			
				IF @x IN (
						6,
						7
						)
					UPDATE PracticeVendors
					SET VendorEmail = @Value
					WHERE VendorId = @y
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @Value AS Value, @OrdinalId AS OrdinalId, @UserId AS UserId, @BillingOrganizationId AS BillingOrganizationId, @ServiceLocationId AS ServiceLocationId, @ContactId AS ContactId, @ExternalOrganizationId AS ExternalOrganizationId, @InsurerId AS InsurerId, @PatientId AS PatientId, @ClaimFileReceiverId AS ClaimFileReceiverId
				
				FETCH NEXT FROM EmailAddressesInsertedCursor INTO @Id, @Value, @OrdinalId, @UserId, @BillingOrganizationId, @ServiceLocationId, @ContactId, @ExternalOrganizationId, @InsurerId, @PatientId, @ClaimFileReceiverId 
		
			END
		
			CLOSE EmailAddressesInsertedCursor
			DEALLOCATE EmailAddressesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEmailAddressUpdate'
				,
				'
			CREATE TRIGGER model.OnEmailAddressUpdate ON [model].[EmailAddresses] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Value nvarchar(max)
			DECLARE @OrdinalId int
			DECLARE @UserId int
			DECLARE @BillingOrganizationId int
			DECLARE @ServiceLocationId int
			DECLARE @ContactId int
			DECLARE @ExternalOrganizationId int
			DECLARE @InsurerId int
			DECLARE @PatientId int
			DECLARE @ClaimFileReceiverId int
		
			DECLARE EmailAddressesUpdatedCursor CURSOR FOR
			SELECT Id, Value, OrdinalId, UserId, BillingOrganizationId, ServiceLocationId, ContactId, ExternalOrganizationId, InsurerId, PatientId, ClaimFileReceiverId FROM inserted
		
			OPEN EmailAddressesUpdatedCursor
			FETCH NEXT FROM EmailAddressesUpdatedCursor INTO @Id, @Value, @OrdinalId, @UserId, @BillingOrganizationId, @ServiceLocationId, @ContactId, @ExternalOrganizationId, @InsurerId, @PatientId, @ClaimFileReceiverId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- EmailAddress Update/Insert
				DECLARE @pair TABLE (
					x int,
					y int
					)
				DECLARE @x int,
					@y int
			
				SELECT @x = x,
					@y = y
				FROM model.GetIdPair(@Id, 110000000)
			
				IF @x IN (
						0,
						2,
						4
						)
					UPDATE Resources
					SET ResourceEmail = @Value
					WHERE ResourceId = @y
			
				IF @x IN (
						1,
						3
						)
					UPDATE PracticeName
					SET PracticeEmail = @Value
					WHERE PracticeId = @y
			
				IF @x = 5
					UPDATE PatientDemographics
					SET Email = @Value
					WHERE PatientId = @y
			
				IF @x IN (
						6,
						7
						)
					UPDATE PracticeVendors
					SET VendorEmail = @Value
					WHERE VendorId = @y
				
				FETCH NEXT FROM EmailAddressesUpdatedCursor INTO @Id, @Value, @OrdinalId, @UserId, @BillingOrganizationId, @ServiceLocationId, @ContactId, @ExternalOrganizationId, @InsurerId, @PatientId, @ClaimFileReceiverId 
		
			END
		
			CLOSE EmailAddressesUpdatedCursor
			DEALLOCATE EmailAddressesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.EncounterLensesPrescriptions')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isEncounterLensesPrescriptionsChanged BIT

		SET @isEncounterLensesPrescriptionsChanged = 0

		DECLARE @EncounterLensesPrescriptionsViewSql NVARCHAR(max)

		SET @EncounterLensesPrescriptionsViewSql = '
		CREATE VIEW [model].[EncounterLensesPrescriptions_Internal]
		WITH SCHEMABINDING
		AS
		SELECT ClinicalId AS Id,
			pc.AppointmentId AS EncounterId,
			FindingDetail,
			ImageDescriptor
		FROM dbo.PatientClinical pc
		WHERE ClinicalType = ''A''
			AND (Substring(FindingDetail, 1, 8) = ''DISPENSE'')
			AND [Status] = ''A''
			AND pc.AppointmentId <> 0'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EncounterLensesPrescriptions_Internal'
			,@EncounterLensesPrescriptionsViewSql
			,@isChanged = @isEncounterLensesPrescriptionsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @EncounterLensesPrescriptionsViewSql = '	
			CREATE VIEW [model].[EncounterLensesPrescriptions]
			WITH SCHEMABINDING
			AS
			SELECT Id, FindingDetail, ImageDescriptor, EncounterId FROM [model].[EncounterLensesPrescriptions_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_EncounterLensesPrescriptions'
							AND object_id = OBJECT_ID('[model].[EncounterLensesPrescriptions_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[EncounterLensesPrescriptions_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_EncounterLensesPrescriptions] ON [model].[EncounterLensesPrescriptions_Internal] ([Id]);
				END

				SET @EncounterLensesPrescriptionsViewSql = @EncounterLensesPrescriptionsViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EncounterLensesPrescriptions'
				,@EncounterLensesPrescriptionsViewSql
				,@isChanged = @isEncounterLensesPrescriptionsChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterLensesPrescriptionDelete'
				,
				'
			CREATE TRIGGER model.OnEncounterLensesPrescriptionDelete ON [model].[EncounterLensesPrescriptions] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @FindingDetail nvarchar(255)
			DECLARE @ImageDescriptor nvarchar(512)
			DECLARE @EncounterId int
		
			DECLARE EncounterLensesPrescriptionsDeletedCursor CURSOR FOR
			SELECT Id, FindingDetail, ImageDescriptor, EncounterId FROM deleted
		
			OPEN EncounterLensesPrescriptionsDeletedCursor
			FETCH NEXT FROM EncounterLensesPrescriptionsDeletedCursor INTO @Id, @FindingDetail, @ImageDescriptor, @EncounterId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for EncounterLensesPrescription --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @FindingDetail AS FindingDetail, @ImageDescriptor AS ImageDescriptor, @EncounterId AS EncounterId
			
				FETCH NEXT FROM EncounterLensesPrescriptionsDeletedCursor INTO @Id, @FindingDetail, @ImageDescriptor, @EncounterId 
		
			END
		
			CLOSE EncounterLensesPrescriptionsDeletedCursor
			DEALLOCATE EncounterLensesPrescriptionsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterLensesPrescriptionInsert'
				,
				'
			CREATE TRIGGER model.OnEncounterLensesPrescriptionInsert ON [model].[EncounterLensesPrescriptions] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @FindingDetail nvarchar(255)
			DECLARE @ImageDescriptor nvarchar(512)
			DECLARE @EncounterId int
		
			DECLARE EncounterLensesPrescriptionsInsertedCursor CURSOR FOR
			SELECT Id, FindingDetail, ImageDescriptor, EncounterId FROM inserted
		
			OPEN EncounterLensesPrescriptionsInsertedCursor
			FETCH NEXT FROM EncounterLensesPrescriptionsInsertedCursor INTO @Id, @FindingDetail, @ImageDescriptor, @EncounterId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for EncounterLensesPrescription --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @FindingDetail AS FindingDetail, @ImageDescriptor AS ImageDescriptor, @EncounterId AS EncounterId
				
				FETCH NEXT FROM EncounterLensesPrescriptionsInsertedCursor INTO @Id, @FindingDetail, @ImageDescriptor, @EncounterId 
		
			END
		
			CLOSE EncounterLensesPrescriptionsInsertedCursor
			DEALLOCATE EncounterLensesPrescriptionsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterLensesPrescriptionUpdate'
				,'
			CREATE TRIGGER model.OnEncounterLensesPrescriptionUpdate ON [model].[EncounterLensesPrescriptions] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @FindingDetail nvarchar(255)
			DECLARE @ImageDescriptor nvarchar(512)
			DECLARE @EncounterId int
		
			DECLARE EncounterLensesPrescriptionsUpdatedCursor CURSOR FOR
			SELECT Id, FindingDetail, ImageDescriptor, EncounterId FROM inserted
		
			OPEN EncounterLensesPrescriptionsUpdatedCursor
			FETCH NEXT FROM EncounterLensesPrescriptionsUpdatedCursor INTO @Id, @FindingDetail, @ImageDescriptor, @EncounterId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for EncounterLensesPrescription --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM EncounterLensesPrescriptionsUpdatedCursor INTO @Id, @FindingDetail, @ImageDescriptor, @EncounterId 
		
			END
		
			CLOSE EncounterLensesPrescriptionsUpdatedCursor
			DEALLOCATE EncounterLensesPrescriptionsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.EncounterPatientInsuranceReferral')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isEncounterPatientInsuranceReferralChanged BIT

		SET @isEncounterPatientInsuranceReferralChanged = 0

		DECLARE @EncounterPatientInsuranceReferralViewSql NVARCHAR(max)

		SET @EncounterPatientInsuranceReferralViewSql = '
		CREATE VIEW [model].[EncounterPatientInsuranceReferral_Internal]
		WITH SCHEMABINDING
		AS
		SELECT ap.AppointmentId AS Encounters_Id
			,pRef.ReferralId AS PatientInsuranceReferrals_Id
		FROM dbo.PatientReferral pRef
		INNER JOIN dbo.PracticeVendors pv ON pv.VendorId = pRef.ReferredFromId
			AND VendorLastName <> '''' AND VendorLastName IS NOT NULL
		INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pref.PatientId
		INNER JOIN dbo.Appointments ap ON ap.ReferralId = pRef.ReferralId
		INNER JOIN dbo.PatientFinancial pf on pRef.ComputedFinancialId = pf.FinancialId
		WHERE pRef.ReferredTo <> 0'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EncounterPatientInsuranceReferral_Internal'
			,@EncounterPatientInsuranceReferralViewSql
			,@isChanged = @isEncounterPatientInsuranceReferralChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @EncounterPatientInsuranceReferralViewSql = '	
			CREATE VIEW [model].[EncounterPatientInsuranceReferral]
			WITH SCHEMABINDING
			AS
			SELECT Encounters_Id, PatientInsuranceReferrals_Id FROM [model].[EncounterPatientInsuranceReferral_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_EncounterPatientInsuranceReferral'
							AND object_id = OBJECT_ID('[model].[EncounterPatientInsuranceReferral_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[EncounterPatientInsuranceReferral_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_EncounterPatientInsuranceReferral] ON [model].[EncounterPatientInsuranceReferral_Internal] (
						[Encounters_Id]
						,[PatientInsuranceReferrals_Id]
						);
				END

				SET @EncounterPatientInsuranceReferralViewSql = @EncounterPatientInsuranceReferralViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EncounterPatientInsuranceReferral'
				,@EncounterPatientInsuranceReferralViewSql
				,@isChanged = @isEncounterPatientInsuranceReferralChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterPatientInsuranceReferralDelete'
				,
				'
			CREATE TRIGGER model.OnEncounterPatientInsuranceReferralDelete ON [model].[EncounterPatientInsuranceReferral] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Encounters_Id int
			DECLARE @PatientInsuranceReferrals_Id int
		
			DECLARE EncounterPatientInsuranceReferralDeletedCursor CURSOR FOR
			SELECT Encounters_Id, PatientInsuranceReferrals_Id FROM deleted
		
			OPEN EncounterPatientInsuranceReferralDeletedCursor
			FETCH NEXT FROM EncounterPatientInsuranceReferralDeletedCursor INTO @Encounters_Id, @PatientInsuranceReferrals_Id 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for EncounterPatientInsuranceReferral --
				PRINT (''Not Implemented'')
			
				SELECT @Encounters_Id AS Encounters_Id, @PatientInsuranceReferrals_Id AS PatientInsuranceReferrals_Id
			
				FETCH NEXT FROM EncounterPatientInsuranceReferralDeletedCursor INTO @Encounters_Id, @PatientInsuranceReferrals_Id 
		
			END
		
			CLOSE EncounterPatientInsuranceReferralDeletedCursor
			DEALLOCATE EncounterPatientInsuranceReferralDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterPatientInsuranceReferralInsert'
				,
				'
			CREATE TRIGGER model.OnEncounterPatientInsuranceReferralInsert ON [model].[EncounterPatientInsuranceReferral] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Encounters_Id int
			DECLARE @PatientInsuranceReferrals_Id int
		
			DECLARE EncounterPatientInsuranceReferralInsertedCursor CURSOR FOR
			SELECT Encounters_Id, PatientInsuranceReferrals_Id FROM inserted
		
			OPEN EncounterPatientInsuranceReferralInsertedCursor
			FETCH NEXT FROM EncounterPatientInsuranceReferralInsertedCursor INTO @Encounters_Id, @PatientInsuranceReferrals_Id 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for EncounterPatientInsuranceReferral --
				PRINT (''Not Implemented'')
			
				SET @Encounters_Id = CONVERT(int, NULL)
				SET @PatientInsuranceReferrals_Id = CONVERT(int, NULL)
			
				SELECT @Encounters_Id AS Encounters_Id, @PatientInsuranceReferrals_Id AS PatientInsuranceReferrals_Id
				
				FETCH NEXT FROM EncounterPatientInsuranceReferralInsertedCursor INTO @Encounters_Id, @PatientInsuranceReferrals_Id 
		
			END
		
			CLOSE EncounterPatientInsuranceReferralInsertedCursor
			DEALLOCATE EncounterPatientInsuranceReferralInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterPatientInsuranceReferralUpdate'
				,'
			CREATE TRIGGER model.OnEncounterPatientInsuranceReferralUpdate ON [model].[EncounterPatientInsuranceReferral] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Encounters_Id int
			DECLARE @PatientInsuranceReferrals_Id int
		
			DECLARE EncounterPatientInsuranceReferralUpdatedCursor CURSOR FOR
			SELECT Encounters_Id, PatientInsuranceReferrals_Id FROM inserted
		
			OPEN EncounterPatientInsuranceReferralUpdatedCursor
			FETCH NEXT FROM EncounterPatientInsuranceReferralUpdatedCursor INTO @Encounters_Id, @PatientInsuranceReferrals_Id 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for EncounterPatientInsuranceReferral --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM EncounterPatientInsuranceReferralUpdatedCursor INTO @Encounters_Id, @PatientInsuranceReferrals_Id 
		
			END
		
			CLOSE EncounterPatientInsuranceReferralUpdatedCursor
			DEALLOCATE EncounterPatientInsuranceReferralUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Encounters')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isEncountersChanged BIT

		SET @isEncountersChanged = 0

		DECLARE @EncountersViewSql NVARCHAR(max)

		SET @EncountersViewSql = 
			'
		CREATE VIEW [model].[Encounters]
		WITH SCHEMABINDING
		AS
		----From PracticeName table - satellite offices
		SELECT ap.AppointmentId AS Id
			,COALESCE(CASE ap.ScheduleStatus
				WHEN ''P''
					THEN 1
				WHEN ''R''
					THEN 2
				WHEN ''X''
					THEN 15
				WHEN ''Q''
					THEN 14
				WHEN ''F''
					THEN 13
				WHEN ''N''
					THEN 12
				WHEN ''S''
					THEN 11
				WHEN ''Y''
					THEN 10
				WHEN ''O''
					THEN 9
				WHEN ''D''
					THEN 8
				WHEN ''A''
					THEN CASE MIN(pa.[Status])
							WHEN ''W''
								THEN 3
							WHEN ''M''
								THEN 4
							WHEN ''E''
								THEN 5
							WHEN ''G''
								THEN 6
							WHEN ''H''
								THEN 7
							WHEN ''U''
								THEN 16			
							END
				END, 15) AS EncounterStatusId
			,CASE
				WHEN apt.ResourceId8 = 1 AND reASC.ResourceId IS NOT NULL
					THEN 3
				ELSE 1 
				END AS EncounterTypeId
			, CASE ap.ApptInsType
				WHEN ''M''
					THEN 1
				WHEN ''V''
					THEN 2
				WHEN ''A''
					THEN 3
				WHEN ''W''
					THEN 4
				ELSE 1
				END AS InsuranceTypeId
			,ap.PatientId AS PatientId
			,pctQU.Id AS PatientQuestionSetId
			,pn.PracticeId AS ServiceLocationId
		FROM dbo.Appointments ap
		INNER JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
		INNER JOIN dbo.PracticeName pn ON pn.PracticeType = ''P''
			AND ap.ResourceId2 - 1000 = pn.PracticeId
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		INNER JOIN dbo.PracticeCodeTable pctQU ON pctQU.Code = apt.Question
			AND pctQU.ReferenceType = ''QUESTIONSETS''
		LEFT JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
		LEFT JOIN dbo.Resources reASC ON reASC.PracticeId = pn.PracticeId
			AND pn.LocationReference <> ''''
			AND reASC.ResourceType = ''R''
			AND reASC.ServiceCode = ''02''
			AND FieldValue = ''T'' 
		WHERE ap.AppTime >= 1 
		GROUP BY ap.AppointmentId
			,ap.ScheduleStatus
			,ap.PatientId
			,ap.PreCertId
			,pctQU.Id
			,pn.PracticeId
			,ap.ApptInsType
			,apt.ResourceId8 
			,FieldValue 
			,reASC.ResourceId
	
		UNION ALL
	
		----At main office
		SELECT ap.AppointmentId AS Id
			,COALESCE(CASE ap.ScheduleStatus
				WHEN ''P''
					THEN 1
				WHEN ''R''
					THEN 2
				WHEN ''X''
					THEN 15
				WHEN ''Q''
					THEN 14
				WHEN ''F''
					THEN 13
				WHEN ''N''
					THEN 12
				WHEN ''S''
					THEN 11
				WHEN ''Y''
					THEN 10
				WHEN ''O''
					THEN 9
				WHEN ''D''
					THEN 8
				WHEN ''A''
					THEN CASE MIN(pa.[Status])
							WHEN ''W''
								THEN 3
							WHEN ''M''
								THEN 4
							WHEN ''E''
								THEN 5
							WHEN ''G''
								THEN 6
							WHEN ''H''
								THEN 7
							WHEN ''U''
								THEN 16
							END
				END, 15) AS EncounterStatusId
			,1 AS EncounterTypeId
			,CASE ap.ApptInsType
				WHEN ''M''
					THEN 1
				WHEN ''V''
					THEN 2
				WHEN ''A''
					THEN 3
				WHEN ''W''
					THEN 4
				ELSE 1
				END AS InsuranceTypeId
			,ap.PatientId AS PatientId
			,pctQU.Id AS PatientQuestionSetId
			,pn.PracticeId AS ServiceLocationId
		FROM dbo.Appointments ap
		INNER JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
		INNER JOIN dbo.PracticeName pn ON pn.PracticeType = ''P''
			AND pn.LocationReference = ''''
			AND ap.ResourceId2 = 0
		INNER JOIN dbo.PracticeCodeTable pctQU ON pctQU.Code = apt.Question
			AND pctQU.ReferenceType = ''QUESTIONSETS''
		LEFT JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
		WHERE ap.AppTime >= 1
		GROUP BY ap.AppointmentId
			,ap.ScheduleStatus
			,ap.PatientId
			,ap.PreCertId
			,pctQU.Id
			,pn.PracticeId
			,ap.ApptInsType
	
	
		UNION ALL
	
		----External Facility, not billing  (SL from Resources table)
		SELECT ap.AppointmentId AS Id
			,COALESCE(CASE ap.ScheduleStatus
				WHEN ''P''
					THEN 1
				WHEN ''R''
					THEN 2
				WHEN ''X''
					THEN 15
				WHEN ''Q''
					THEN 14
				WHEN ''F''
					THEN 13
				WHEN ''N''
					THEN 12
				WHEN ''S''
					THEN 11
				WHEN ''Y''
					THEN 10
				WHEN ''O''
					THEN 9
				WHEN ''D''
					THEN 8
				WHEN ''A''
					THEN CASE MIN(pa.[Status])
							WHEN ''W''
								THEN 3
							WHEN ''M''
								THEN 4
							WHEN ''E''
								THEN 5
							WHEN ''G''
								THEN 6
							WHEN ''H''
								THEN 7
							WHEN ''U''
								THEN 16
							END
				END, 15) AS EncounterStatusId
			,1 AS EncounterTypeId
			,CASE ap.ApptInsType
				WHEN ''M''
					THEN 1
				WHEN ''V''
					THEN 2
				WHEN ''A''
					THEN 3
				WHEN ''W''
					THEN 4
				ELSE 1
				END AS InsuranceTypeId
			,ap.PatientId AS PatientId
			,pctQU.Id AS PatientQuestionSetId
			,(110000000 * 1 + re.ResourceId) AS ServiceLocationId
		FROM dbo.Appointments ap
		INNER JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
		INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId2
			AND re.ResourceType = ''R''
			AND re.ServiceCode = ''02'' 
		INNER JOIN dbo.PracticeCodeTable pctQU ON pctQU.Code = apt.Question
			AND pctQU.ReferenceType = ''QUESTIONSETS''
		LEFT JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
		WHERE ap.AppTime >= 1
		GROUP BY ap.AppointmentId
			,ap.ScheduleStatus
			,ap.PatientId
			,ap.PreCertId
			,pctQU.Id
			,re.ResourceId
			,ap.ApptInsType
	
		UNION ALL
	
		----Appointments that are added, either via billing or optical. Note that sx order appointments are filtered out by ResourceId2, and "ASC CLAIM" filtered out because AppTypeId <> 0
		SELECT ap.AppointmentId AS Id
			,8 AS EncounterStatusId
			,CASE ap.Comments 
				WHEN ''ADD VIA OPTICAL''
					THEN 4
				ELSE 1 END AS EncounterTypeId
			,CASE ap.ApptInsType
				WHEN ''M''
					THEN 1
				WHEN ''V''
					THEN 2
				WHEN ''A''
					THEN 3
				WHEN ''W''
					THEN 4
				ELSE 1
				END AS InsuranceTypeId
			,ap.PatientId AS PatientId
			,pct.Id AS PatientQuestionSetId
			,(110000000 * CASE 
				WHEN ResourceId2 = 0 OR ResourceId2 > 1000 THEN 0 
				ELSE 1 
				END + CASE 
				WHEN ResourceId2 = 0 THEN pnMainOffice.PracticeId 
				WHEN ResourceId2 > 1000 THEN ResourceId2 - 1000 
				ELSE ResourceId2 
				END) AS ServiceLocationId
		FROM dbo.Appointments ap
		INNER JOIN dbo.PracticeCodeTable pct ON Referencetype = ''QUESTIONSETS'' and Code = ''No Questions''
		LEFT JOIN dbo.PracticeName pnMainOffice ON 
			PracticeType = ''P'' AND LocationReference = '''' 
		WHERE ap.AppTime < 1'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Encounters'
			,@EncountersViewSql
			,@isChanged = @isEncountersChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterDelete'
				,
				'
			CREATE TRIGGER model.OnEncounterDelete ON [model].[Encounters] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PatientId int
			DECLARE @EncounterTypeId int
			DECLARE @EncounterStatusId int
			DECLARE @PatientQuestionSetId int
			DECLARE @ServiceLocationId int
			DECLARE @InsuranceTypeId int
		
			DECLARE EncountersDeletedCursor CURSOR FOR
			SELECT Id, PatientId, EncounterTypeId, EncounterStatusId, PatientQuestionSetId, ServiceLocationId, InsuranceTypeId FROM deleted
		
			OPEN EncountersDeletedCursor
			FETCH NEXT FROM EncountersDeletedCursor INTO @Id, @PatientId, @EncounterTypeId, @EncounterStatusId, @PatientQuestionSetId, @ServiceLocationId, @InsuranceTypeId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Encounter Delete
				DECLARE @appointmentId INT
			
				SET @appointmentId = @Id
			
				DELETE
				FROM dbo.Appointments
				WHERE AppointmentId = @appointmentId
			
				SELECT @Id AS Id, @PatientId AS PatientId, @EncounterTypeId AS EncounterTypeId, @EncounterStatusId AS EncounterStatusId, @PatientQuestionSetId AS PatientQuestionSetId, @ServiceLocationId AS ServiceLocationId, @InsuranceTypeId AS InsuranceTypeId
			
				FETCH NEXT FROM EncountersDeletedCursor INTO @Id, @PatientId, @EncounterTypeId, @EncounterStatusId, @PatientQuestionSetId, @ServiceLocationId, @InsuranceTypeId 
		
			END
		
			CLOSE EncountersDeletedCursor
			DEALLOCATE EncountersDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterInsert'
				,
				'
			CREATE TRIGGER model.OnEncounterInsert ON [model].[Encounters] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PatientId int
			DECLARE @EncounterTypeId int
			DECLARE @EncounterStatusId int
			DECLARE @PatientQuestionSetId int
			DECLARE @ServiceLocationId int
			DECLARE @InsuranceTypeId int
		
			DECLARE EncountersInsertedCursor CURSOR FOR
			SELECT Id, PatientId, EncounterTypeId, EncounterStatusId, PatientQuestionSetId, ServiceLocationId, InsuranceTypeId FROM inserted
		
			OPEN EncountersInsertedCursor
			FETCH NEXT FROM EncountersInsertedCursor INTO @Id, @PatientId, @EncounterTypeId, @EncounterStatusId, @PatientQuestionSetId, @ServiceLocationId, @InsuranceTypeId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- Encounter Insert
				INSERT INTO dbo.Appointments (
					AppDate
					,PatientId
					)
				VALUES (
					''''
					,@PatientId
					)
			
				SET @Id = IDENT_CURRENT(''Appointments'')
			
							
				-- Encounter Update/Insert
				DECLARE @patientDemographicsId INT
			
				SET @patientDemographicsId = (
						SELECT y
						FROM model.GetIdPair(@PatientId, 202000000)
						)
			
				DECLARE @appointmentId INT
			
				SET @appointmentId = @Id
			
				DECLARE @resourceId2 INT
				DECLARE @x INT
				DECLARE @practiceCode NVARCHAR(255)
				DECLARE @shortName NVARCHAR(255)
			
				SET @practiceCode = (
						SELECT TOP 1 Code
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientQuestionSetId
						)
			
				SELECT @resourceId2 = (
						SELECT y
						FROM model.GetIdPair(@ServiceLocationId, 110000000)
						)
			
				SELECT @x = (
						SELECT x
						FROM model.GetIdPair(@ServiceLocationId, 110000000)
						)
			
				SELECT @shortName = (
						SELECT ShortName
						FROM model.ServiceLocations
						WHERE Id = @ServiceLocationId
						)
			
				DECLARE @status NVARCHAR(1)
				DECLARE @count INT
				DECLARE @ActivityStatusTime NVARCHAR(100)
				DECLARE @AppDate NVARCHAR(20)
				DECLARE @ActivityLocationId INT
				DECLARE @ActivityStatusTRef INT
			
				SELECT @AppDate = AppDate
				FROM dbo.Appointments
				WHERE AppointmentId = @appointmentId
			
				SELECT @ActivityLocationId = (
						SELECT TOP 1 ResourceId2
						FROM dbo.Appointments
						WHERE AppointmentId = @appointmentId
						)
			
				SELECT @ActivityStatusTRef = (
						SELECT DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0), GETDATE())
						)
			
				SELECT @ActivityStatusTime = (substring(convert(VARCHAR(20), GetDate(), 9), 13, 5) + substring(convert(VARCHAR(30), GetDate(), 9), 25, 2))
			
				SELECT @ActivityStatusTime = (LTRIM(RTRIM(@ActivityStatusTime)))
			
				IF LEN(@ActivityStatusTime) <> 7
				BEGIN
					SELECT @ActivityStatusTime = (''0'' + @ActivityStatusTime)
				END
			
				SELECT @count = (
						SELECT Count(AppointmentId)
						FROM dbo.PracticeActivity
						WHERE AppointmentId = @appointmentId
						)
			
				SET @status = CASE 
						WHEN @EncounterStatusId = 3
							THEN ''W''
						WHEN @EncounterStatusId = 4
							THEN ''M''
						WHEN @EncounterStatusId = 5
							THEN ''E''
						WHEN @EncounterStatusId = 6
							THEN ''G''
						WHEN @EncounterStatusId = 7
							THEN ''H''
						WHEN @EncounterStatusId = 8
							THEN ''D''
						WHEN @EncounterStatusId = 9
							THEN ''O''
						WHEN @EncounterStatusId = 10
							THEN ''Y''
						WHEN @EncounterStatusId = 11
							THEN ''S''
						WHEN @EncounterStatusId = 12
							THEN ''N''
						WHEN @EncounterStatusId = 13
							THEN ''F''
						WHEN @EncounterStatusId = 14
							THEN ''Q''
						WHEN @EncounterStatusId = 15
							THEN ''X''
						WHEN @EncounterStatusId = 16
							THEN ''U''
						END
			
				UPDATE dbo.Appointments
				SET ScheduleStatus = CASE 
						WHEN @EncounterStatusId = 1
							THEN ''P''
						WHEN @EncounterStatusId = 2
							THEN ''R''
						WHEN @EncounterStatusId = 15
							THEN ''X''
						WHEN @EncounterStatusId = 14
							THEN ''Q''
						WHEN @EncounterStatusId = 13
							THEN ''F''
						WHEN @EncounterStatusId = 12
							THEN ''N''
						WHEN @EncounterStatusId = 11
							THEN ''S''
						WHEN @EncounterStatusId = 10
							THEN ''Y''
						WHEN @EncounterStatusId = 9
							THEN ''O''
						WHEN @EncounterStatusId = 8
							THEN ''D''
						WHEN @status IN (
								''W''
								,''M''
								,''E''
								,''G''
								,''H''
								,''U''
								)
							THEN ''A''
						END
					,PatientId = @patientDemographicsId
					,ResourceId2 = CASE 
						WHEN @x = 0
							AND @ShortName = ''Office''
							THEN 0
						WHEN @x = 0
							AND @ShortName <> ''Office''
							THEN @resourceId2 + 1000
						WHEN @x = 1
							THEN @resourceId2
						END
					,ApptInsType = CASE CAST(@InsuranceTypeId AS NVARCHAR)
						WHEN ''1''
							THEN ''M''
						WHEN ''2''
							THEN ''V''
						WHEN ''3''
							THEN ''A''
						WHEN ''4''
							THEN ''W''
						ELSE ''1''
						END
				WHERE AppointmentId = @appointmentId
			
				IF @EncounterStatusId > 2
				BEGIN
					IF (
							@count = 0
							AND (
								@EncounterStatusId < 9
								OR @EncounterStatusId > 15
								)
							)
						INSERT INTO dbo.PracticeActivity (
							AppointmentId
							,STATUS
							,Questionset
							,ActivityDate
							,PatientId
							,ActivityStatusTime
							,TechConfirmed
							,LocationId
							,ActivityStatusTRef
							)
						VALUES (
							@appointmentId
							,@status
							,@practiceCode
							,@AppDate
							,@patientDemographicsId
							,@ActivityStatusTime
							,''N''
							,@ActivityLocationId
							,@ActivityStatusTRef
							)
					ELSE
						IF @count > 0
							UPDATE dbo.PracticeActivity
							SET STATUS = @status
								,Questionset = (
									SELECT TOP 1 Code
									FROM dbo.PracticeCodeTable
									WHERE Id = @PatientQuestionSetId
									)
								,ActivityDate = @AppDate
								,PatientId = @patientDemographicsId
								,ActivityStatusTime = @ActivityStatusTime
								,TechConfirmed = ''N''
								,LocationId = @ActivityLocationId
								,ActivityStatusTRef = @ActivityStatusTRef
							WHERE AppointmentId = @appointmentId;
				END
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @PatientId AS PatientId, @EncounterTypeId AS EncounterTypeId, @EncounterStatusId AS EncounterStatusId, @PatientQuestionSetId AS PatientQuestionSetId, @ServiceLocationId AS ServiceLocationId, @InsuranceTypeId AS InsuranceTypeId
				
				FETCH NEXT FROM EncountersInsertedCursor INTO @Id, @PatientId, @EncounterTypeId, @EncounterStatusId, @PatientQuestionSetId, @ServiceLocationId, @InsuranceTypeId 
		
			END
		
			CLOSE EncountersInsertedCursor
			DEALLOCATE EncountersInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterUpdate'
				,
				'
			CREATE TRIGGER model.OnEncounterUpdate ON [model].[Encounters] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PatientId int
			DECLARE @EncounterTypeId int
			DECLARE @EncounterStatusId int
			DECLARE @PatientQuestionSetId int
			DECLARE @ServiceLocationId int
			DECLARE @InsuranceTypeId int
		
			DECLARE EncountersUpdatedCursor CURSOR FOR
			SELECT Id, PatientId, EncounterTypeId, EncounterStatusId, PatientQuestionSetId, ServiceLocationId, InsuranceTypeId FROM inserted
		
			OPEN EncountersUpdatedCursor
			FETCH NEXT FROM EncountersUpdatedCursor INTO @Id, @PatientId, @EncounterTypeId, @EncounterStatusId, @PatientQuestionSetId, @ServiceLocationId, @InsuranceTypeId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- Encounter Update/Insert
				DECLARE @patientDemographicsId INT
			
				SET @patientDemographicsId = (
						SELECT y
						FROM model.GetIdPair(@PatientId, 202000000)
						)
			
				DECLARE @appointmentId INT
			
				SET @appointmentId = @Id
			
				DECLARE @resourceId2 INT
				DECLARE @x INT
				DECLARE @practiceCode NVARCHAR(255)
				DECLARE @shortName NVARCHAR(255)
			
				SET @practiceCode = (
						SELECT TOP 1 Code
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientQuestionSetId
						)
			
				SELECT @resourceId2 = (
						SELECT y
						FROM model.GetIdPair(@ServiceLocationId, 110000000)
						)
			
				SELECT @x = (
						SELECT x
						FROM model.GetIdPair(@ServiceLocationId, 110000000)
						)
			
				SELECT @shortName = (
						SELECT ShortName
						FROM model.ServiceLocations
						WHERE Id = @ServiceLocationId
						)
			
				DECLARE @status NVARCHAR(1)
				DECLARE @count INT
				DECLARE @ActivityStatusTime NVARCHAR(100)
				DECLARE @AppDate NVARCHAR(20)
				DECLARE @ActivityLocationId INT
				DECLARE @ActivityStatusTRef INT
			
				SELECT @AppDate = AppDate
				FROM dbo.Appointments
				WHERE AppointmentId = @appointmentId
			
				SELECT @ActivityLocationId = (
						SELECT TOP 1 ResourceId2
						FROM dbo.Appointments
						WHERE AppointmentId = @appointmentId
						)
			
				SELECT @ActivityStatusTRef = (
						SELECT DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0), GETDATE())
						)
			
				SELECT @ActivityStatusTime = (substring(convert(VARCHAR(20), GetDate(), 9), 13, 5) + substring(convert(VARCHAR(30), GetDate(), 9), 25, 2))
			
				SELECT @ActivityStatusTime = (LTRIM(RTRIM(@ActivityStatusTime)))
			
				IF LEN(@ActivityStatusTime) <> 7
				BEGIN
					SELECT @ActivityStatusTime = (''0'' + @ActivityStatusTime)
				END
			
				SELECT @count = (
						SELECT Count(AppointmentId)
						FROM dbo.PracticeActivity
						WHERE AppointmentId = @appointmentId
						)
			
				SET @status = CASE 
						WHEN @EncounterStatusId = 3
							THEN ''W''
						WHEN @EncounterStatusId = 4
							THEN ''M''
						WHEN @EncounterStatusId = 5
							THEN ''E''
						WHEN @EncounterStatusId = 6
							THEN ''G''
						WHEN @EncounterStatusId = 7
							THEN ''H''
						WHEN @EncounterStatusId = 8
							THEN ''D''
						WHEN @EncounterStatusId = 9
							THEN ''O''
						WHEN @EncounterStatusId = 10
							THEN ''Y''
						WHEN @EncounterStatusId = 11
							THEN ''S''
						WHEN @EncounterStatusId = 12
							THEN ''N''
						WHEN @EncounterStatusId = 13
							THEN ''F''
						WHEN @EncounterStatusId = 14
							THEN ''Q''
						WHEN @EncounterStatusId = 15
							THEN ''X''
						WHEN @EncounterStatusId = 16
							THEN ''U''
						END
			
				UPDATE dbo.Appointments
				SET ScheduleStatus = CASE 
						WHEN @EncounterStatusId = 1
							THEN ''P''
						WHEN @EncounterStatusId = 2
							THEN ''R''
						WHEN @EncounterStatusId = 15
							THEN ''X''
						WHEN @EncounterStatusId = 14
							THEN ''Q''
						WHEN @EncounterStatusId = 13
							THEN ''F''
						WHEN @EncounterStatusId = 12
							THEN ''N''
						WHEN @EncounterStatusId = 11
							THEN ''S''
						WHEN @EncounterStatusId = 10
							THEN ''Y''
						WHEN @EncounterStatusId = 9
							THEN ''O''
						WHEN @EncounterStatusId = 8
							THEN ''D''
						WHEN @status IN (
								''W''
								,''M''
								,''E''
								,''G''
								,''H''
								,''U''
								)
							THEN ''A''
						END
					,PatientId = @patientDemographicsId
					,ResourceId2 = CASE 
						WHEN @x = 0
							AND @ShortName = ''Office''
							THEN 0
						WHEN @x = 0
							AND @ShortName <> ''Office''
							THEN @resourceId2 + 1000
						WHEN @x = 1
							THEN @resourceId2
						END
					,ApptInsType = CASE CAST(@InsuranceTypeId AS NVARCHAR)
						WHEN ''1''
							THEN ''M''
						WHEN ''2''
							THEN ''V''
						WHEN ''3''
							THEN ''A''
						WHEN ''4''
							THEN ''W''
						ELSE ''1''
						END
				WHERE AppointmentId = @appointmentId
			
				IF @EncounterStatusId > 2
				BEGIN
					IF (
							@count = 0
							AND (
								@EncounterStatusId < 9
								OR @EncounterStatusId > 15
								)
							)
						INSERT INTO dbo.PracticeActivity (
							AppointmentId
							,STATUS
							,Questionset
							,ActivityDate
							,PatientId
							,ActivityStatusTime
							,TechConfirmed
							,LocationId
							,ActivityStatusTRef
							)
						VALUES (
							@appointmentId
							,@status
							,@practiceCode
							,@AppDate
							,@patientDemographicsId
							,@ActivityStatusTime
							,''N''
							,@ActivityLocationId
							,@ActivityStatusTRef
							)
					ELSE
						IF @count > 0
							UPDATE dbo.PracticeActivity
							SET STATUS = @status
								,Questionset = (
									SELECT TOP 1 Code
									FROM dbo.PracticeCodeTable
									WHERE Id = @PatientQuestionSetId
									)
								,ActivityDate = @AppDate
								,PatientId = @patientDemographicsId
								,ActivityStatusTime = @ActivityStatusTime
								,TechConfirmed = ''N''
								,LocationId = @ActivityLocationId
								,ActivityStatusTRef = @ActivityStatusTRef
							WHERE AppointmentId = @appointmentId;
				END
				
				FETCH NEXT FROM EncountersUpdatedCursor INTO @Id, @PatientId, @EncounterTypeId, @EncounterStatusId, @PatientQuestionSetId, @ServiceLocationId, @InsuranceTypeId 
		
			END
		
			CLOSE EncountersUpdatedCursor
			DEALLOCATE EncountersUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.EncounterServiceDrugs')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isEncounterServiceDrugsChanged BIT

		SET @isEncounterServiceDrugsChanged = 0

		DECLARE @EncounterServiceDrugsViewSql NVARCHAR(max)

		SET @EncounterServiceDrugsViewSql = '
		CREATE VIEW [model].[EncounterServiceDrugs_Internal]
		WITH SCHEMABINDING
		AS
		SELECT CodeId AS Id,
			CASE 
				WHEN ClaimNote <> '''' 
					THEN ClaimNote 
				ELSE [Description] 
				END AS [Name],
			NDC,
			CONVERT(decimal(18,2), 1) AS Quantity,
			5 AS UnitOfMeasurementId
		FROM dbo.PracticeServices ps 
		WHERE SUBSTRING(Code, 1, 1) = ''J''
			OR CodeCategory like ''%DRUG%''
			OR CodeCategory like ''%INJECT%''
			OR CodeCategory like ''%HCPCS%'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EncounterServiceDrugs_Internal'
			,@EncounterServiceDrugsViewSql
			,@isChanged = @isEncounterServiceDrugsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @EncounterServiceDrugsViewSql = '	
			CREATE VIEW [model].[EncounterServiceDrugs]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, NDC, Quantity, UnitOfMeasurementId FROM [model].[EncounterServiceDrugs_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_EncounterServiceDrugs'
							AND object_id = OBJECT_ID('[model].[EncounterServiceDrugs_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[EncounterServiceDrugs_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_EncounterServiceDrugs] ON [model].[EncounterServiceDrugs_Internal] ([Id]);
				END

				SET @EncounterServiceDrugsViewSql = @EncounterServiceDrugsViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EncounterServiceDrugs'
				,@EncounterServiceDrugsViewSql
				,@isChanged = @isEncounterServiceDrugsChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterServiceDrugDelete'
				,
				'
			CREATE TRIGGER model.OnEncounterServiceDrugDelete ON [model].[EncounterServiceDrugs] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @NDC nvarchar(max)
			DECLARE @Quantity decimal(18,0)
			DECLARE @UnitOfMeasurementId int
		
			DECLARE EncounterServiceDrugsDeletedCursor CURSOR FOR
			SELECT Id, Name, NDC, Quantity, UnitOfMeasurementId FROM deleted
		
			OPEN EncounterServiceDrugsDeletedCursor
			FETCH NEXT FROM EncounterServiceDrugsDeletedCursor INTO @Id, @Name, @NDC, @Quantity, @UnitOfMeasurementId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for EncounterServiceDrug --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name, @NDC AS NDC, @Quantity AS Quantity, @UnitOfMeasurementId AS UnitOfMeasurementId
			
				FETCH NEXT FROM EncounterServiceDrugsDeletedCursor INTO @Id, @Name, @NDC, @Quantity, @UnitOfMeasurementId 
		
			END
		
			CLOSE EncounterServiceDrugsDeletedCursor
			DEALLOCATE EncounterServiceDrugsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterServiceDrugInsert'
				,
				'
			CREATE TRIGGER model.OnEncounterServiceDrugInsert ON [model].[EncounterServiceDrugs] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @NDC nvarchar(max)
			DECLARE @Quantity decimal(18,0)
			DECLARE @UnitOfMeasurementId int
		
			DECLARE EncounterServiceDrugsInsertedCursor CURSOR FOR
			SELECT Id, Name, NDC, Quantity, UnitOfMeasurementId FROM inserted
		
			OPEN EncounterServiceDrugsInsertedCursor
			FETCH NEXT FROM EncounterServiceDrugsInsertedCursor INTO @Id, @Name, @NDC, @Quantity, @UnitOfMeasurementId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for EncounterServiceDrug --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name, @NDC AS NDC, @Quantity AS Quantity, @UnitOfMeasurementId AS UnitOfMeasurementId
				
				FETCH NEXT FROM EncounterServiceDrugsInsertedCursor INTO @Id, @Name, @NDC, @Quantity, @UnitOfMeasurementId 
		
			END
		
			CLOSE EncounterServiceDrugsInsertedCursor
			DEALLOCATE EncounterServiceDrugsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterServiceDrugUpdate'
				,'
			CREATE TRIGGER model.OnEncounterServiceDrugUpdate ON [model].[EncounterServiceDrugs] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @NDC nvarchar(max)
			DECLARE @Quantity decimal(18,0)
			DECLARE @UnitOfMeasurementId int
		
			DECLARE EncounterServiceDrugsUpdatedCursor CURSOR FOR
			SELECT Id, Name, NDC, Quantity, UnitOfMeasurementId FROM inserted
		
			OPEN EncounterServiceDrugsUpdatedCursor
			FETCH NEXT FROM EncounterServiceDrugsUpdatedCursor INTO @Id, @Name, @NDC, @Quantity, @UnitOfMeasurementId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for EncounterServiceDrug --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM EncounterServiceDrugsUpdatedCursor INTO @Id, @Name, @NDC, @Quantity, @UnitOfMeasurementId 
		
			END
		
			CLOSE EncounterServiceDrugsUpdatedCursor
			DEALLOCATE EncounterServiceDrugsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.EncounterServices')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isEncounterServicesChanged BIT

		SET @isEncounterServicesChanged = 0

		DECLARE @EncounterServicesViewSql NVARCHAR(max)

		SET @EncounterServicesViewSql = 
			'
		CREATE VIEW [model].[EncounterServices]
		WITH SCHEMABINDING
		AS
		SELECT CodeId AS Id,
			ps.Code AS Code,
			CONVERT(decimal(18,2), 1) AS DefaultUnit,
			Description AS Description,
			pct.Id AS EncounterServiceTypeId,
			CASE WHEN EndDate = '''' THEN NULL ELSE CONVERT(datetime, EndDate, 112) END AS EndDateTime,
			CASE ConsultOn
				WHEN ''T''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsConsultation,
			CONVERT(bit, 1) AS IsDoctorInvoice,
			CASE 
				WHEN SUBSTRING(ps.Code, 1, 1) = ''J''
					OR ps.CodeCategory LIKE ''%inject%''
					OR ps.CodeCategory LIKE ''%drug%''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsDrug,
			CASE ps.POS
				WHEN ''98''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsFacilityInvoice,
			CASE 
				WHEN len(ClaimNote) > 0
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END as IsUnclassified,
			CASE OrderDoc
				WHEN ''T''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsOrderingProvider,
			CASE 
				WHEN SUBSTRING(ps.Code, 1, 2) = ''V2''
					OR ps.CodeCategory LIKE ''%dispens%''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsVisionInvoice,
			CASE ServiceFilter
				WHEN ''F''
					THEN CONVERT(bit, 0)
				ELSE CONVERT(bit, 1)
				END AS IsZeroCharge,
			CONVERT(decimal(18,2), RelativeValueUnit) AS RelativeValueUnit,
			ps.RevenueCodeId AS RevenueCodeId,
			CONVERT(datetime, StartDate, 112) AS StartDateTime,
			CONVERT(decimal(18,2), Fee) AS UnitFee,
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeServices ps
		LEFT JOIN dbo.PracticeCodeTable pct ON pct.Code = ps.CodeCategory
			AND pct.ReferenceType = ''SERVICECATEGORY'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EncounterServices'
			,@EncounterServicesViewSql
			,@isChanged = @isEncounterServicesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterServiceDelete'
				,
				'
			CREATE TRIGGER model.OnEncounterServiceDelete ON [model].[EncounterServices] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Code nvarchar(max)
			DECLARE @Description nvarchar(max)
			DECLARE @DefaultUnit decimal(18,0)
			DECLARE @UnitFee decimal(18,0)
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @IsOrderingProvider bit
			DECLARE @IsConsultation bit
			DECLARE @IsFacilityInvoice bit
			DECLARE @IsDoctorInvoice bit
			DECLARE @IsVisionInvoice bit
			DECLARE @IsZeroCharge bit
			DECLARE @RelativeValueUnit decimal(18,0)
			DECLARE @IsArchived bit
			DECLARE @IsUnclassified bit
			DECLARE @EncounterServiceTypeId int
			DECLARE @RevenueCodeId int
		
			DECLARE EncounterServicesDeletedCursor CURSOR FOR
			SELECT Id, Code, Description, DefaultUnit, UnitFee, StartDateTime, EndDateTime, IsOrderingProvider, IsConsultation, IsFacilityInvoice, IsDoctorInvoice, IsVisionInvoice, IsZeroCharge, RelativeValueUnit, IsArchived, IsUnclassified, EncounterServiceTypeId, RevenueCodeId FROM deleted
		
			OPEN EncounterServicesDeletedCursor
			FETCH NEXT FROM EncounterServicesDeletedCursor INTO @Id, @Code, @Description, @DefaultUnit, @UnitFee, @StartDateTime, @EndDateTime, @IsOrderingProvider, @IsConsultation, @IsFacilityInvoice, @IsDoctorInvoice, @IsVisionInvoice, @IsZeroCharge, @RelativeValueUnit, @IsArchived, @IsUnclassified, @EncounterServiceTypeId, @RevenueCodeId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for EncounterService --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Code AS Code, @Description AS Description, @DefaultUnit AS DefaultUnit, @UnitFee AS UnitFee, @StartDateTime AS StartDateTime, @EndDateTime AS EndDateTime, @IsOrderingProvider AS IsOrderingProvider, @IsConsultation AS IsConsultation, @IsFacilityInvoice AS IsFacilityInvoice, @IsDoctorInvoice AS IsDoctorInvoice, @IsVisionInvoice AS IsVisionInvoice, @IsZeroCharge AS IsZeroCharge, @RelativeValueUnit AS RelativeValueUnit, @IsArchived AS IsArchived, @IsUnclassified AS IsUnclassified, @EncounterServiceTypeId AS EncounterServiceTypeId, @RevenueCodeId AS RevenueCodeId
			
				FETCH NEXT FROM EncounterServicesDeletedCursor INTO @Id, @Code, @Description, @DefaultUnit, @UnitFee, @StartDateTime, @EndDateTime, @IsOrderingProvider, @IsConsultation, @IsFacilityInvoice, @IsDoctorInvoice, @IsVisionInvoice, @IsZeroCharge, @RelativeValueUnit, @IsArchived, @IsUnclassified, @EncounterServiceTypeId, @RevenueCodeId 
		
			END
		
			CLOSE EncounterServicesDeletedCursor
			DEALLOCATE EncounterServicesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterServiceInsert'
				,
				'
			CREATE TRIGGER model.OnEncounterServiceInsert ON [model].[EncounterServices] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Code nvarchar(max)
			DECLARE @Description nvarchar(max)
			DECLARE @DefaultUnit decimal(18,0)
			DECLARE @UnitFee decimal(18,0)
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @IsOrderingProvider bit
			DECLARE @IsConsultation bit
			DECLARE @IsFacilityInvoice bit
			DECLARE @IsDoctorInvoice bit
			DECLARE @IsVisionInvoice bit
			DECLARE @IsZeroCharge bit
			DECLARE @RelativeValueUnit decimal(18,0)
			DECLARE @IsArchived bit
			DECLARE @IsUnclassified bit
			DECLARE @EncounterServiceTypeId int
			DECLARE @RevenueCodeId int
		
			DECLARE EncounterServicesInsertedCursor CURSOR FOR
			SELECT Id, Code, Description, DefaultUnit, UnitFee, StartDateTime, EndDateTime, IsOrderingProvider, IsConsultation, IsFacilityInvoice, IsDoctorInvoice, IsVisionInvoice, IsZeroCharge, RelativeValueUnit, IsArchived, IsUnclassified, EncounterServiceTypeId, RevenueCodeId FROM inserted
		
			OPEN EncounterServicesInsertedCursor
			FETCH NEXT FROM EncounterServicesInsertedCursor INTO @Id, @Code, @Description, @DefaultUnit, @UnitFee, @StartDateTime, @EndDateTime, @IsOrderingProvider, @IsConsultation, @IsFacilityInvoice, @IsDoctorInvoice, @IsVisionInvoice, @IsZeroCharge, @RelativeValueUnit, @IsArchived, @IsUnclassified, @EncounterServiceTypeId, @RevenueCodeId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for EncounterService --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Code AS Code, @Description AS Description, @DefaultUnit AS DefaultUnit, @UnitFee AS UnitFee, @StartDateTime AS StartDateTime, @EndDateTime AS EndDateTime, @IsOrderingProvider AS IsOrderingProvider, @IsConsultation AS IsConsultation, @IsFacilityInvoice AS IsFacilityInvoice, @IsDoctorInvoice AS IsDoctorInvoice, @IsVisionInvoice AS IsVisionInvoice, @IsZeroCharge AS IsZeroCharge, @RelativeValueUnit AS RelativeValueUnit, @IsArchived AS IsArchived, @IsUnclassified AS IsUnclassified, @EncounterServiceTypeId AS EncounterServiceTypeId, @RevenueCodeId AS RevenueCodeId
				
				FETCH NEXT FROM EncounterServicesInsertedCursor INTO @Id, @Code, @Description, @DefaultUnit, @UnitFee, @StartDateTime, @EndDateTime, @IsOrderingProvider, @IsConsultation, @IsFacilityInvoice, @IsDoctorInvoice, @IsVisionInvoice, @IsZeroCharge, @RelativeValueUnit, @IsArchived, @IsUnclassified, @EncounterServiceTypeId, @RevenueCodeId 
		
			END
		
			CLOSE EncounterServicesInsertedCursor
			DEALLOCATE EncounterServicesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterServiceUpdate'
				,
				'
			CREATE TRIGGER model.OnEncounterServiceUpdate ON [model].[EncounterServices] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Code nvarchar(max)
			DECLARE @Description nvarchar(max)
			DECLARE @DefaultUnit decimal(18,0)
			DECLARE @UnitFee decimal(18,0)
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @IsOrderingProvider bit
			DECLARE @IsConsultation bit
			DECLARE @IsFacilityInvoice bit
			DECLARE @IsDoctorInvoice bit
			DECLARE @IsVisionInvoice bit
			DECLARE @IsZeroCharge bit
			DECLARE @RelativeValueUnit decimal(18,0)
			DECLARE @IsArchived bit
			DECLARE @IsUnclassified bit
			DECLARE @EncounterServiceTypeId int
			DECLARE @RevenueCodeId int
		
			DECLARE EncounterServicesUpdatedCursor CURSOR FOR
			SELECT Id, Code, Description, DefaultUnit, UnitFee, StartDateTime, EndDateTime, IsOrderingProvider, IsConsultation, IsFacilityInvoice, IsDoctorInvoice, IsVisionInvoice, IsZeroCharge, RelativeValueUnit, IsArchived, IsUnclassified, EncounterServiceTypeId, RevenueCodeId FROM inserted
		
			OPEN EncounterServicesUpdatedCursor
			FETCH NEXT FROM EncounterServicesUpdatedCursor INTO @Id, @Code, @Description, @DefaultUnit, @UnitFee, @StartDateTime, @EndDateTime, @IsOrderingProvider, @IsConsultation, @IsFacilityInvoice, @IsDoctorInvoice, @IsVisionInvoice, @IsZeroCharge, @RelativeValueUnit, @IsArchived, @IsUnclassified, @EncounterServiceTypeId, @RevenueCodeId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for EncounterService --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM EncounterServicesUpdatedCursor INTO @Id, @Code, @Description, @DefaultUnit, @UnitFee, @StartDateTime, @EndDateTime, @IsOrderingProvider, @IsConsultation, @IsFacilityInvoice, @IsDoctorInvoice, @IsVisionInvoice, @IsZeroCharge, @RelativeValueUnit, @IsArchived, @IsUnclassified, @EncounterServiceTypeId, @RevenueCodeId 
		
			END
		
			CLOSE EncounterServicesUpdatedCursor
			DEALLOCATE EncounterServicesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.EncounterServiceTypes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isEncounterServiceTypesChanged BIT

		SET @isEncounterServiceTypesChanged = 0

		DECLARE @EncounterServiceTypesViewSql NVARCHAR(max)

		SET @EncounterServiceTypesViewSql = '
		CREATE VIEW [model].[EncounterServiceTypes_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id,
			Code AS [Name]
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''SERVICECATEGORY'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EncounterServiceTypes_Internal'
			,@EncounterServiceTypesViewSql
			,@isChanged = @isEncounterServiceTypesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @EncounterServiceTypesViewSql = '	
			CREATE VIEW [model].[EncounterServiceTypes]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, IsArchived FROM [model].[EncounterServiceTypes_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_EncounterServiceTypes'
							AND object_id = OBJECT_ID('[model].[EncounterServiceTypes_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[EncounterServiceTypes_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_EncounterServiceTypes] ON [model].[EncounterServiceTypes_Internal] ([Id]);
				END

				SET @EncounterServiceTypesViewSql = @EncounterServiceTypesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.EncounterServiceTypes'
				,@EncounterServiceTypesViewSql
				,@isChanged = @isEncounterServiceTypesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterServiceTypeDelete'
				,'
			CREATE TRIGGER model.OnEncounterServiceTypeDelete ON [model].[EncounterServiceTypes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name varchar(1)
			DECLARE @IsArchived bit
		
			DECLARE EncounterServiceTypesDeletedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM deleted
		
			OPEN EncounterServiceTypesDeletedCursor
			FETCH NEXT FROM EncounterServiceTypesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for EncounterServiceType --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
			
				FETCH NEXT FROM EncounterServiceTypesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE EncounterServiceTypesDeletedCursor
			DEALLOCATE EncounterServiceTypesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterServiceTypeInsert'
				,'
			CREATE TRIGGER model.OnEncounterServiceTypeInsert ON [model].[EncounterServiceTypes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name varchar(1)
			DECLARE @IsArchived bit
		
			DECLARE EncounterServiceTypesInsertedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN EncounterServiceTypesInsertedCursor
			FETCH NEXT FROM EncounterServiceTypesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for EncounterServiceType --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
				
				FETCH NEXT FROM EncounterServiceTypesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE EncounterServiceTypesInsertedCursor
			DEALLOCATE EncounterServiceTypesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEncounterServiceTypeUpdate'
				,'
			CREATE TRIGGER model.OnEncounterServiceTypeUpdate ON [model].[EncounterServiceTypes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name varchar(1)
			DECLARE @IsArchived bit
		
			DECLARE EncounterServiceTypesUpdatedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN EncounterServiceTypesUpdatedCursor
			FETCH NEXT FROM EncounterServiceTypesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for EncounterServiceType --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM EncounterServiceTypesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE EncounterServiceTypesUpdatedCursor
			DEALLOCATE EncounterServiceTypesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Equipments')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isEquipmentsChanged BIT

		SET @isEquipmentsChanged = 0

		DECLARE @EquipmentsViewSql NVARCHAR(max)

		SET @EquipmentsViewSql = '
		CREATE VIEW [model].[Equipments_Internal]
		WITH SCHEMABINDING
		AS
		SELECT AppTypeId AS Id,
			AppointmentType AS Abbreviation,
			CONVERT(bit, 1) AS IsSchedulable,
			AppointmentType AS [Name]
			,CONVERT(bit, 0) AS IsArchived
		FROM dbo.AppointmentType
		WHERE ResourceId7 > 0'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Equipments_Internal'
			,@EquipmentsViewSql
			,@isChanged = @isEquipmentsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @EquipmentsViewSql = '	
			CREATE VIEW [model].[Equipments]
			WITH SCHEMABINDING
			AS
			SELECT Abbreviation, Name, IsSchedulable, Id, IsArchived FROM [model].[Equipments_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_Equipments'
							AND object_id = OBJECT_ID('[model].[Equipments_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[Equipments_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_Equipments] ON [model].[Equipments_Internal] ([Id]);
				END

				SET @EquipmentsViewSql = @EquipmentsViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Equipments'
				,@EquipmentsViewSql
				,@isChanged = @isEquipmentsChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEquipmentDelete'
				,'
			CREATE TRIGGER model.OnEquipmentDelete ON [model].[Equipments] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Abbreviation nvarchar(max)
			DECLARE @Name nvarchar(max)
			DECLARE @IsSchedulable bit
			DECLARE @Id int
			DECLARE @IsArchived bit
		
			DECLARE EquipmentsDeletedCursor CURSOR FOR
			SELECT Abbreviation, Name, IsSchedulable, Id, IsArchived FROM deleted
		
			OPEN EquipmentsDeletedCursor
			FETCH NEXT FROM EquipmentsDeletedCursor INTO @Abbreviation, @Name, @IsSchedulable, @Id, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for Equipment --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Abbreviation AS Abbreviation, @Name AS Name, @IsSchedulable AS IsSchedulable, @IsArchived AS IsArchived
			
				FETCH NEXT FROM EquipmentsDeletedCursor INTO @Abbreviation, @Name, @IsSchedulable, @Id, @IsArchived 
		
			END
		
			CLOSE EquipmentsDeletedCursor
			DEALLOCATE EquipmentsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEquipmentInsert'
				,
				'
			CREATE TRIGGER model.OnEquipmentInsert ON [model].[Equipments] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Abbreviation nvarchar(max)
			DECLARE @Name nvarchar(max)
			DECLARE @IsSchedulable bit
			DECLARE @Id int
			DECLARE @IsArchived bit
		
			DECLARE EquipmentsInsertedCursor CURSOR FOR
			SELECT Abbreviation, Name, IsSchedulable, Id, IsArchived FROM inserted
		
			OPEN EquipmentsInsertedCursor
			FETCH NEXT FROM EquipmentsInsertedCursor INTO @Abbreviation, @Name, @IsSchedulable, @Id, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for Equipment --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Abbreviation AS Abbreviation, @Name AS Name, @IsSchedulable AS IsSchedulable, @IsArchived AS IsArchived
				
				FETCH NEXT FROM EquipmentsInsertedCursor INTO @Abbreviation, @Name, @IsSchedulable, @Id, @IsArchived 
		
			END
		
			CLOSE EquipmentsInsertedCursor
			DEALLOCATE EquipmentsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnEquipmentUpdate'
				,'
			CREATE TRIGGER model.OnEquipmentUpdate ON [model].[Equipments] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Abbreviation nvarchar(max)
			DECLARE @Name nvarchar(max)
			DECLARE @IsSchedulable bit
			DECLARE @Id int
			DECLARE @IsArchived bit
		
			DECLARE EquipmentsUpdatedCursor CURSOR FOR
			SELECT Abbreviation, Name, IsSchedulable, Id, IsArchived FROM inserted
		
			OPEN EquipmentsUpdatedCursor
			FETCH NEXT FROM EquipmentsUpdatedCursor INTO @Abbreviation, @Name, @IsSchedulable, @Id, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for Equipment --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM EquipmentsUpdatedCursor INTO @Abbreviation, @Name, @IsSchedulable, @Id, @IsArchived 
		
			END
		
			CLOSE EquipmentsUpdatedCursor
			DEALLOCATE EquipmentsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ExternalOrganizations')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isExternalOrganizationsChanged BIT

		SET @isExternalOrganizationsChanged = 0

		DECLARE @ExternalOrganizationsViewSql NVARCHAR(max)

		SET @ExternalOrganizationsViewSql = '
		CREATE VIEW [model].[ExternalOrganizations_Internal]
		WITH SCHEMABINDING
		AS
		SELECT VendorId AS Id,
			VendorFirmName AS DisplayName,
			CASE VendorType
				WHEN ''L''
					THEN 2
				WHEN ''H''
					THEN 3
				WHEN ''V''
					THEN 7
				ELSE 8
				END AS ExternalOrganizationTypeId,
			VendorFirmName NAME,
			CONVERT(bit, 0) AS IsArchived,
			''ExternalProviderExternalOrganization'' as __EntityType__
		FROM dbo.PracticeVendors pv
		WHERE VendorFirmName <> ''''
			AND VendorFirmName IS NOT NULL'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ExternalOrganizations_Internal'
			,@ExternalOrganizationsViewSql
			,@isChanged = @isExternalOrganizationsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @ExternalOrganizationsViewSql = '	
			CREATE VIEW [model].[ExternalOrganizations]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, DisplayName, ExternalOrganizationTypeId, IsArchived FROM [model].[ExternalOrganizations_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_ExternalOrganizations'
							AND object_id = OBJECT_ID('[model].[ExternalOrganizations_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[ExternalOrganizations_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_ExternalOrganizations] ON [model].[ExternalOrganizations_Internal] ([Id]);
				END

				SET @ExternalOrganizationsViewSql = @ExternalOrganizationsViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ExternalOrganizations'
				,@ExternalOrganizationsViewSql
				,@isChanged = @isExternalOrganizationsChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnExternalOrganizationDelete'
				,
				'
			CREATE TRIGGER model.OnExternalOrganizationDelete ON [model].[ExternalOrganizations] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @DisplayName nvarchar(max)
			DECLARE @ExternalOrganizationTypeId int
			DECLARE @IsArchived bit
		
			DECLARE ExternalOrganizationsDeletedCursor CURSOR FOR
			SELECT Id, Name, DisplayName, ExternalOrganizationTypeId, IsArchived FROM deleted
		
			OPEN ExternalOrganizationsDeletedCursor
			FETCH NEXT FROM ExternalOrganizationsDeletedCursor INTO @Id, @Name, @DisplayName, @ExternalOrganizationTypeId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- ExternalOrganization Delete
				DECLARE @PracticeVendorId int 
				SET @PracticeVendorId = @Id
			
				DELETE
				FROM dbo.PracticeVendors
				WHERE VendorId = @PracticeVendorId
			
				SELECT @Id AS Id, @Name AS Name, @DisplayName AS DisplayName, @ExternalOrganizationTypeId AS ExternalOrganizationTypeId, @IsArchived AS IsArchived
			
				FETCH NEXT FROM ExternalOrganizationsDeletedCursor INTO @Id, @Name, @DisplayName, @ExternalOrganizationTypeId, @IsArchived 
		
			END
		
			CLOSE ExternalOrganizationsDeletedCursor
			DEALLOCATE ExternalOrganizationsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnExternalOrganizationInsert'
				,
				'
			CREATE TRIGGER model.OnExternalOrganizationInsert ON [model].[ExternalOrganizations] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @DisplayName nvarchar(max)
			DECLARE @ExternalOrganizationTypeId int
			DECLARE @IsArchived bit
		
			DECLARE ExternalOrganizationsInsertedCursor CURSOR FOR
			SELECT Id, Name, DisplayName, ExternalOrganizationTypeId, IsArchived FROM inserted
		
			OPEN ExternalOrganizationsInsertedCursor
			FETCH NEXT FROM ExternalOrganizationsInsertedCursor INTO @Id, @Name, @DisplayName, @ExternalOrganizationTypeId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- ExternalOrganization Insert
				INSERT INTO dbo.PracticeVendors (vendorname)
				VALUES ('''')
			
				SET @Id = IDENT_CURRENT(''PracticeVendors'') 
			
							
				-- ExternalOrganization Update/Insert
				DECLARE @PracticeVendorId int 
				SET @PracticeVendorId = @Id
			
				UPDATE dbo.PracticeVendors
				SET VendorFirmName = @DisplayName,
			
					VendorType = CASE @ExternalOrganizationTypeId
						WHEN 2
							THEN ''L''
						WHEN 3
							THEN ''H''
						WHEN 7
							THEN ''V''			
						ELSE  ''O''
						END
			
					WHERE VendorId = @PracticeVendorId
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @Name AS Name, @DisplayName AS DisplayName, @ExternalOrganizationTypeId AS ExternalOrganizationTypeId, @IsArchived AS IsArchived
				
				FETCH NEXT FROM ExternalOrganizationsInsertedCursor INTO @Id, @Name, @DisplayName, @ExternalOrganizationTypeId, @IsArchived 
		
			END
		
			CLOSE ExternalOrganizationsInsertedCursor
			DEALLOCATE ExternalOrganizationsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnExternalOrganizationUpdate'
				,
				'
			CREATE TRIGGER model.OnExternalOrganizationUpdate ON [model].[ExternalOrganizations] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @DisplayName nvarchar(max)
			DECLARE @ExternalOrganizationTypeId int
			DECLARE @IsArchived bit
		
			DECLARE ExternalOrganizationsUpdatedCursor CURSOR FOR
			SELECT Id, Name, DisplayName, ExternalOrganizationTypeId, IsArchived FROM inserted
		
			OPEN ExternalOrganizationsUpdatedCursor
			FETCH NEXT FROM ExternalOrganizationsUpdatedCursor INTO @Id, @Name, @DisplayName, @ExternalOrganizationTypeId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- ExternalOrganization Update/Insert
				DECLARE @PracticeVendorId int 
				SET @PracticeVendorId = @Id
			
				UPDATE dbo.PracticeVendors
				SET VendorFirmName = @DisplayName,
			
					VendorType = CASE @ExternalOrganizationTypeId
						WHEN 2
							THEN ''L''
						WHEN 3
							THEN ''H''
						WHEN 7
							THEN ''V''			
						ELSE  ''O''
						END
			
					WHERE VendorId = @PracticeVendorId
				
				FETCH NEXT FROM ExternalOrganizationsUpdatedCursor INTO @Id, @Name, @DisplayName, @ExternalOrganizationTypeId, @IsArchived 
		
			END
		
			CLOSE ExternalOrganizationsUpdatedCursor
			DEALLOCATE ExternalOrganizationsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ExternalProviderClinicalSpecialtyTypes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isExternalProviderClinicalSpecialtyTypesChanged BIT

		SET @isExternalProviderClinicalSpecialtyTypesChanged = 0

		DECLARE @ExternalProviderClinicalSpecialtyTypesViewSql NVARCHAR(max)

		SET @ExternalProviderClinicalSpecialtyTypesViewSql = 
			'
		CREATE VIEW [model].[ExternalProviderClinicalSpecialtyTypes]
		WITH SCHEMABINDING
		AS
		----PracticeVendors specialty #1
		SELECT CASE v.Id 
			WHEN 0
				THEN VendorId 
			WHEN 1
				THEN (110000000 * 1 + VendorId)
			END AS Id,
			v.OrdinalId AS OrdinalId,
			VendorId AS ExternalProviderId,
			v.ClinicalSpecialtyTypeId AS ClinicalSpecialtyTypeId
		FROM (SELECT
				0 AS Id,
				1 AS OrdinalId,
				VendorId AS VendorId,
				pct.id AS ClinicalSpecialtyTypeId,
				COUNT_BIG(*) AS Count
			FROM dbo.PracticeVendors pv
			LEFT JOIN dbo.PracticeCodeTable pct ON pv.VendorSpecialty = pct.Code
				AND pct.ReferenceType = ''SPECIALTY''
			WHERE pct.Id <> ''''
				AND pct.Id IS NOT NULL
				AND VendorLastName <> ''''
				AND VendorLastName IS NOT NULL
			GROUP BY VendorId,
				pct.Id
			
		--referring drs spec 2
	
			UNION ALL
	
		----PracticeVendors specialty #2
	
			SELECT
				1 AS Id,	
				2 AS OrdinalId,
				VendorId AS VendorId,
				pct.id AS ClinicalSpecialtyTypeId,
				COUNT_BIG(*) AS Count
			FROM dbo.PracticeVendors pv
			LEFT JOIN dbo.PracticeCodeTable pct ON pv.VendorSpecOther = pct.Code
				AND pct.ReferenceType = ''SPECIALTY''
			WHERE pct.Id <> ''''
				AND pct.Id IS NOT NULL
				AND VendorLastName <> ''''
				AND VendorLastName IS NOT NULL
			GROUP BY VendorId,
				pct.Id
			) AS v'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ExternalProviderClinicalSpecialtyTypes'
			,@ExternalProviderClinicalSpecialtyTypesViewSql
			,@isChanged = @isExternalProviderClinicalSpecialtyTypesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnExternalProviderClinicalSpecialtyTypeDelete'
				,
				'
			CREATE TRIGGER model.OnExternalProviderClinicalSpecialtyTypeDelete ON [model].[ExternalProviderClinicalSpecialtyTypes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @ClinicalSpecialtyTypeId int
			DECLARE @ExternalProviderId int
			DECLARE @OrdinalId int
		
			DECLARE ExternalProviderClinicalSpecialtyTypesDeletedCursor CURSOR FOR
			SELECT Id, ClinicalSpecialtyTypeId, ExternalProviderId, OrdinalId FROM deleted
		
			OPEN ExternalProviderClinicalSpecialtyTypesDeletedCursor
			FETCH NEXT FROM ExternalProviderClinicalSpecialtyTypesDeletedCursor INTO @Id, @ClinicalSpecialtyTypeId, @ExternalProviderId, @OrdinalId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ExternalProviderClinicalSpecialtyType --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @ClinicalSpecialtyTypeId AS ClinicalSpecialtyTypeId, @ExternalProviderId AS ExternalProviderId, @OrdinalId AS OrdinalId
			
				FETCH NEXT FROM ExternalProviderClinicalSpecialtyTypesDeletedCursor INTO @Id, @ClinicalSpecialtyTypeId, @ExternalProviderId, @OrdinalId 
		
			END
		
			CLOSE ExternalProviderClinicalSpecialtyTypesDeletedCursor
			DEALLOCATE ExternalProviderClinicalSpecialtyTypesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnExternalProviderClinicalSpecialtyTypeInsert'
				,
				'
			CREATE TRIGGER model.OnExternalProviderClinicalSpecialtyTypeInsert ON [model].[ExternalProviderClinicalSpecialtyTypes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @ClinicalSpecialtyTypeId int
			DECLARE @ExternalProviderId int
			DECLARE @OrdinalId int
		
			DECLARE ExternalProviderClinicalSpecialtyTypesInsertedCursor CURSOR FOR
			SELECT Id, ClinicalSpecialtyTypeId, ExternalProviderId, OrdinalId FROM inserted
		
			OPEN ExternalProviderClinicalSpecialtyTypesInsertedCursor
			FETCH NEXT FROM ExternalProviderClinicalSpecialtyTypesInsertedCursor INTO @Id, @ClinicalSpecialtyTypeId, @ExternalProviderId, @OrdinalId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ExternalProviderClinicalSpecialtyType --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @ClinicalSpecialtyTypeId AS ClinicalSpecialtyTypeId, @ExternalProviderId AS ExternalProviderId, @OrdinalId AS OrdinalId
				
				FETCH NEXT FROM ExternalProviderClinicalSpecialtyTypesInsertedCursor INTO @Id, @ClinicalSpecialtyTypeId, @ExternalProviderId, @OrdinalId 
		
			END
		
			CLOSE ExternalProviderClinicalSpecialtyTypesInsertedCursor
			DEALLOCATE ExternalProviderClinicalSpecialtyTypesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnExternalProviderClinicalSpecialtyTypeUpdate'
				,
				'
			CREATE TRIGGER model.OnExternalProviderClinicalSpecialtyTypeUpdate ON [model].[ExternalProviderClinicalSpecialtyTypes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @ClinicalSpecialtyTypeId int
			DECLARE @ExternalProviderId int
			DECLARE @OrdinalId int
		
			DECLARE ExternalProviderClinicalSpecialtyTypesUpdatedCursor CURSOR FOR
			SELECT Id, ClinicalSpecialtyTypeId, ExternalProviderId, OrdinalId FROM inserted
		
			OPEN ExternalProviderClinicalSpecialtyTypesUpdatedCursor
			FETCH NEXT FROM ExternalProviderClinicalSpecialtyTypesUpdatedCursor INTO @Id, @ClinicalSpecialtyTypeId, @ExternalProviderId, @OrdinalId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ExternalProviderClinicalSpecialtyType --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ExternalProviderClinicalSpecialtyTypesUpdatedCursor INTO @Id, @ClinicalSpecialtyTypeId, @ExternalProviderId, @OrdinalId 
		
			END
		
			CLOSE ExternalProviderClinicalSpecialtyTypesUpdatedCursor
			DEALLOCATE ExternalProviderClinicalSpecialtyTypesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.FeeScheduleContractAmounts')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isFeeScheduleContractAmountsChanged BIT

		SET @isFeeScheduleContractAmountsChanged = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.FeeScheduleContractAmounts'
			,'
		CREATE VIEW [model].[FeeScheduleContractAmounts]
		WITH SCHEMABINDING
		AS
		SELECT CONVERT(int, NULL) AS Id, CONVERT(decimal(18,0), NULL) AS InvoiceUnitFee, CONVERT(decimal(18,0), NULL) AS InsurerPaidPercent, CONVERT(decimal(18,0), NULL) AS OfficeAllowable, CONVERT(decimal(18,0), NULL) AS FacilityAllowable, CONVERT(int, NULL) AS FeeScheduleContractId, CONVERT(int, NULL) AS EncounterServiceId, CONVERT(int, NULL) AS ServiceLocationId, CONVERT(bigint, NULL) AS ProviderId
		-- View SQL not found for FeeScheduleContractAmount --
		'
			,@isChanged = @isFeeScheduleContractAmountsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFeeScheduleContractAmountDelete'
				,
				'
			CREATE TRIGGER model.OnFeeScheduleContractAmountDelete ON [model].[FeeScheduleContractAmounts] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @InvoiceUnitFee decimal(18,0)
			DECLARE @InsurerPaidPercent decimal(18,0)
			DECLARE @OfficeAllowable decimal(18,0)
			DECLARE @FacilityAllowable decimal(18,0)
			DECLARE @FeeScheduleContractId int
			DECLARE @EncounterServiceId int
			DECLARE @ServiceLocationId int
			DECLARE @ProviderId bigint
		
			DECLARE FeeScheduleContractAmountsDeletedCursor CURSOR FOR
			SELECT Id, InvoiceUnitFee, InsurerPaidPercent, OfficeAllowable, FacilityAllowable, FeeScheduleContractId, EncounterServiceId, ServiceLocationId, ProviderId FROM deleted
		
			OPEN FeeScheduleContractAmountsDeletedCursor
			FETCH NEXT FROM FeeScheduleContractAmountsDeletedCursor INTO @Id, @InvoiceUnitFee, @InsurerPaidPercent, @OfficeAllowable, @FacilityAllowable, @FeeScheduleContractId, @EncounterServiceId, @ServiceLocationId, @ProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for FeeScheduleContractAmount --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @InvoiceUnitFee AS InvoiceUnitFee, @InsurerPaidPercent AS InsurerPaidPercent, @OfficeAllowable AS OfficeAllowable, @FacilityAllowable AS FacilityAllowable, @FeeScheduleContractId AS FeeScheduleContractId, @EncounterServiceId AS EncounterServiceId, @ServiceLocationId AS ServiceLocationId, @ProviderId AS ProviderId
			
				FETCH NEXT FROM FeeScheduleContractAmountsDeletedCursor INTO @Id, @InvoiceUnitFee, @InsurerPaidPercent, @OfficeAllowable, @FacilityAllowable, @FeeScheduleContractId, @EncounterServiceId, @ServiceLocationId, @ProviderId 
		
			END
		
			CLOSE FeeScheduleContractAmountsDeletedCursor
			DEALLOCATE FeeScheduleContractAmountsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFeeScheduleContractAmountInsert'
				,
				'
			CREATE TRIGGER model.OnFeeScheduleContractAmountInsert ON [model].[FeeScheduleContractAmounts] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @InvoiceUnitFee decimal(18,0)
			DECLARE @InsurerPaidPercent decimal(18,0)
			DECLARE @OfficeAllowable decimal(18,0)
			DECLARE @FacilityAllowable decimal(18,0)
			DECLARE @FeeScheduleContractId int
			DECLARE @EncounterServiceId int
			DECLARE @ServiceLocationId int
			DECLARE @ProviderId bigint
		
			DECLARE FeeScheduleContractAmountsInsertedCursor CURSOR FOR
			SELECT Id, InvoiceUnitFee, InsurerPaidPercent, OfficeAllowable, FacilityAllowable, FeeScheduleContractId, EncounterServiceId, ServiceLocationId, ProviderId FROM inserted
		
			OPEN FeeScheduleContractAmountsInsertedCursor
			FETCH NEXT FROM FeeScheduleContractAmountsInsertedCursor INTO @Id, @InvoiceUnitFee, @InsurerPaidPercent, @OfficeAllowable, @FacilityAllowable, @FeeScheduleContractId, @EncounterServiceId, @ServiceLocationId, @ProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for FeeScheduleContractAmount --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @InvoiceUnitFee AS InvoiceUnitFee, @InsurerPaidPercent AS InsurerPaidPercent, @OfficeAllowable AS OfficeAllowable, @FacilityAllowable AS FacilityAllowable, @FeeScheduleContractId AS FeeScheduleContractId, @EncounterServiceId AS EncounterServiceId, @ServiceLocationId AS ServiceLocationId, @ProviderId AS ProviderId
				
				FETCH NEXT FROM FeeScheduleContractAmountsInsertedCursor INTO @Id, @InvoiceUnitFee, @InsurerPaidPercent, @OfficeAllowable, @FacilityAllowable, @FeeScheduleContractId, @EncounterServiceId, @ServiceLocationId, @ProviderId 
		
			END
		
			CLOSE FeeScheduleContractAmountsInsertedCursor
			DEALLOCATE FeeScheduleContractAmountsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFeeScheduleContractAmountUpdate'
				,
				'
			CREATE TRIGGER model.OnFeeScheduleContractAmountUpdate ON [model].[FeeScheduleContractAmounts] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @InvoiceUnitFee decimal(18,0)
			DECLARE @InsurerPaidPercent decimal(18,0)
			DECLARE @OfficeAllowable decimal(18,0)
			DECLARE @FacilityAllowable decimal(18,0)
			DECLARE @FeeScheduleContractId int
			DECLARE @EncounterServiceId int
			DECLARE @ServiceLocationId int
			DECLARE @ProviderId bigint
		
			DECLARE FeeScheduleContractAmountsUpdatedCursor CURSOR FOR
			SELECT Id, InvoiceUnitFee, InsurerPaidPercent, OfficeAllowable, FacilityAllowable, FeeScheduleContractId, EncounterServiceId, ServiceLocationId, ProviderId FROM inserted
		
			OPEN FeeScheduleContractAmountsUpdatedCursor
			FETCH NEXT FROM FeeScheduleContractAmountsUpdatedCursor INTO @Id, @InvoiceUnitFee, @InsurerPaidPercent, @OfficeAllowable, @FacilityAllowable, @FeeScheduleContractId, @EncounterServiceId, @ServiceLocationId, @ProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for FeeScheduleContractAmount --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM FeeScheduleContractAmountsUpdatedCursor INTO @Id, @InvoiceUnitFee, @InsurerPaidPercent, @OfficeAllowable, @FacilityAllowable, @FeeScheduleContractId, @EncounterServiceId, @ServiceLocationId, @ProviderId 
		
			END
		
			CLOSE FeeScheduleContractAmountsUpdatedCursor
			DEALLOCATE FeeScheduleContractAmountsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.FeeScheduleContracts')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isFeeScheduleContractsChanged BIT

		SET @isFeeScheduleContractsChanged = 0

		DECLARE @FeeScheduleContractsViewSql NVARCHAR(max)

		SET @FeeScheduleContractsViewSql = '
		CREATE VIEW [model].[FeeScheduleContracts]
		WITH SCHEMABINDING
		AS
		SELECT MIN(pfs.ServiceId) AS Id,
			InsurerName + '' '' + CASE 
				WHEN InsurerPlanType IS NULL THEN ''''
				ELSE InsurerPlanType
				END + substring(StartDate, 1, 6) + substring(enddate, 5, 2) AS [Name]
		FROM dbo.PracticeFeeSchedules pfs
		INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pfs.PlanId
		GROUP BY pri.InsurerName,
			pri.InsurerPlanType,
			pfs.StartDate,
			pfs.EndDate'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.FeeScheduleContracts'
			,@FeeScheduleContractsViewSql
			,@isChanged = @isFeeScheduleContractsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFeeScheduleContractDelete'
				,'
			CREATE TRIGGER model.OnFeeScheduleContractDelete ON [model].[FeeScheduleContracts] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
		
			DECLARE FeeScheduleContractsDeletedCursor CURSOR FOR
			SELECT Id, Name FROM deleted
		
			OPEN FeeScheduleContractsDeletedCursor
			FETCH NEXT FROM FeeScheduleContractsDeletedCursor INTO @Id, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for FeeScheduleContract --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name
			
				FETCH NEXT FROM FeeScheduleContractsDeletedCursor INTO @Id, @Name 
		
			END
		
			CLOSE FeeScheduleContractsDeletedCursor
			DEALLOCATE FeeScheduleContractsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFeeScheduleContractInsert'
				,'
			CREATE TRIGGER model.OnFeeScheduleContractInsert ON [model].[FeeScheduleContracts] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
		
			DECLARE FeeScheduleContractsInsertedCursor CURSOR FOR
			SELECT Id, Name FROM inserted
		
			OPEN FeeScheduleContractsInsertedCursor
			FETCH NEXT FROM FeeScheduleContractsInsertedCursor INTO @Id, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for FeeScheduleContract --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name
				
				FETCH NEXT FROM FeeScheduleContractsInsertedCursor INTO @Id, @Name 
		
			END
		
			CLOSE FeeScheduleContractsInsertedCursor
			DEALLOCATE FeeScheduleContractsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFeeScheduleContractUpdate'
				,'
			CREATE TRIGGER model.OnFeeScheduleContractUpdate ON [model].[FeeScheduleContracts] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
		
			DECLARE FeeScheduleContractsUpdatedCursor CURSOR FOR
			SELECT Id, Name FROM inserted
		
			OPEN FeeScheduleContractsUpdatedCursor
			FETCH NEXT FROM FeeScheduleContractsUpdatedCursor INTO @Id, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for FeeScheduleContract --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM FeeScheduleContractsUpdatedCursor INTO @Id, @Name 
		
			END
		
			CLOSE FeeScheduleContractsUpdatedCursor
			DEALLOCATE FeeScheduleContractsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.FinancialBatches')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isFinancialBatchesChanged BIT

		SET @isFinancialBatchesChanged = 0

		DECLARE @FinancialBatchesViewSql NVARCHAR(max)

		SET @FinancialBatchesViewSql = '
		CREATE VIEW [model].[FinancialBatches_Internal]
		WITH SCHEMABINDING
		AS
		SELECT PaymentId AS Id,
			prp.PaymentAmountDecimal AS Amount,
			prp.PaymentCheck AS CheckCode,
			CASE prp.PayerType
				WHEN ''I''
					THEN 1
				WHEN ''P''
					THEN 2
				WHEN ''O''
					THEN 3
				ELSE 4
				END AS FinancialSourceTypeId,
			CASE WHEN prp.PaymentEOBDate = '''' THEN NULL ELSE CONVERT(datetime, prp.PaymentEOBDate, 112) END AS PaymentDateTime
		FROM dbo.PatientReceivablePayments prp
		WHERE prp.PaymentType = ''='''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.FinancialBatches_Internal'
			,@FinancialBatchesViewSql
			,@isChanged = @isFinancialBatchesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @FinancialBatchesViewSql = '	
			CREATE VIEW [model].[FinancialBatches]
			WITH SCHEMABINDING
			AS
			SELECT Id, PaymentDateTime, Amount, CheckCode, FinancialSourceTypeId FROM [model].[FinancialBatches_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_FinancialBatches'
							AND object_id = OBJECT_ID('[model].[FinancialBatches_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[FinancialBatches_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_FinancialBatches] ON [model].[FinancialBatches_Internal] ([Id]);
				END

				SET @FinancialBatchesViewSql = @FinancialBatchesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.FinancialBatches'
				,@FinancialBatchesViewSql
				,@isChanged = @isFinancialBatchesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFinancialBatchDelete'
				,
				'
			CREATE TRIGGER model.OnFinancialBatchDelete ON [model].[FinancialBatches] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PaymentDateTime datetime
			DECLARE @Amount decimal(18,0)
			DECLARE @CheckCode nvarchar(max)
			DECLARE @FinancialSourceTypeId int
		
			DECLARE FinancialBatchesDeletedCursor CURSOR FOR
			SELECT Id, PaymentDateTime, Amount, CheckCode, FinancialSourceTypeId FROM deleted
		
			OPEN FinancialBatchesDeletedCursor
			FETCH NEXT FROM FinancialBatchesDeletedCursor INTO @Id, @PaymentDateTime, @Amount, @CheckCode, @FinancialSourceTypeId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for FinancialBatch --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @PaymentDateTime AS PaymentDateTime, @Amount AS Amount, @CheckCode AS CheckCode, @FinancialSourceTypeId AS FinancialSourceTypeId
			
				FETCH NEXT FROM FinancialBatchesDeletedCursor INTO @Id, @PaymentDateTime, @Amount, @CheckCode, @FinancialSourceTypeId 
		
			END
		
			CLOSE FinancialBatchesDeletedCursor
			DEALLOCATE FinancialBatchesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFinancialBatchInsert'
				,
				'
			CREATE TRIGGER model.OnFinancialBatchInsert ON [model].[FinancialBatches] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PaymentDateTime datetime
			DECLARE @Amount decimal(18,0)
			DECLARE @CheckCode nvarchar(max)
			DECLARE @FinancialSourceTypeId int
		
			DECLARE FinancialBatchesInsertedCursor CURSOR FOR
			SELECT Id, PaymentDateTime, Amount, CheckCode, FinancialSourceTypeId FROM inserted
		
			OPEN FinancialBatchesInsertedCursor
			FETCH NEXT FROM FinancialBatchesInsertedCursor INTO @Id, @PaymentDateTime, @Amount, @CheckCode, @FinancialSourceTypeId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for FinancialBatch --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @PaymentDateTime AS PaymentDateTime, @Amount AS Amount, @CheckCode AS CheckCode, @FinancialSourceTypeId AS FinancialSourceTypeId
				
				FETCH NEXT FROM FinancialBatchesInsertedCursor INTO @Id, @PaymentDateTime, @Amount, @CheckCode, @FinancialSourceTypeId 
		
			END
		
			CLOSE FinancialBatchesInsertedCursor
			DEALLOCATE FinancialBatchesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFinancialBatchUpdate'
				,'
			CREATE TRIGGER model.OnFinancialBatchUpdate ON [model].[FinancialBatches] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PaymentDateTime datetime
			DECLARE @Amount decimal(18,0)
			DECLARE @CheckCode nvarchar(max)
			DECLARE @FinancialSourceTypeId int
		
			DECLARE FinancialBatchesUpdatedCursor CURSOR FOR
			SELECT Id, PaymentDateTime, Amount, CheckCode, FinancialSourceTypeId FROM inserted
		
			OPEN FinancialBatchesUpdatedCursor
			FETCH NEXT FROM FinancialBatchesUpdatedCursor INTO @Id, @PaymentDateTime, @Amount, @CheckCode, @FinancialSourceTypeId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for FinancialBatch --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM FinancialBatchesUpdatedCursor INTO @Id, @PaymentDateTime, @Amount, @CheckCode, @FinancialSourceTypeId 
		
			END
		
			CLOSE FinancialBatchesUpdatedCursor
			DEALLOCATE FinancialBatchesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.FinancialInformations')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isFinancialInformationsChanged BIT

		SET @isFinancialInformationsChanged = 0

		DECLARE @FinancialInformationsViewSql NVARCHAR(max)

		SET @FinancialInformationsViewSql = 
			'
		CREATE VIEW [model].[FinancialInformations]
		WITH SCHEMABINDING
		AS
		SELECT prp.PaymentId AS Id,
			prp.PaymentAmountDecimal AS Amount,
			prp.PaymentServiceItem AS BillingServiceId,
			CASE SUBSTRING(Code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) 
				WHEN ''!''
					THEN 1
				WHEN ''|''
					THEN 2
				WHEN ''?''
					THEN 3
				WHEN ''%''
					THEN 4
				WHEN ''D''
					THEN 5
				WHEN ''&''
					THEN 6
				WHEN '']''
					THEN 7
				WHEN ''0''
					THEN 8
				WHEN ''V''
					THEN 9
				ELSE pct.Id + 1000 
				END AS FinancialInformationTypeId,
			CASE prp.PayerType
				WHEN ''I''
					THEN 1
				WHEN ''P''
					THEN 2
				WHEN ''O''
					THEN 3
				ELSE 4
				END AS FinancialSourceTypeId,
			CASE prp.PayerType
				WHEN ''I''
					THEN Prp.PayerId
				ELSE NULL
				END AS InsurerId,
			(CONVERT(bigint, 110000000) * (CASE 
								WHEN pf.FinancialId IS NULL OR pf.FinancialId = 0
									THEN 0
								ELSE pf.FinancialId
								END) + pr.AppointmentId) AS InvoiceReceivableId,
			CASE prp.PayerType
				WHEN ''P''
					THEN prp.PayerId
				ELSE NULL
				END AS PatientId,
			CONVERT(datetime, prp.PaymentDate, 112) AS PostedDateTime,
			CONVERT(bit, 0) AS IsArchived,
			CASE WHEN prp.PaymentBatchCheckId IS NULL THEN NULL WHEN prp.PaymentBatchCheckId = 0 THEN NULL WHEN prp.PaymentBatchCheckId = -1 THEN NULL ELSE prp.PaymentBatchCheckId END AS FinancialBatchId
		FROM dbo.PatientReceivablePayments prp
		INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
		INNER JOIN dbo.PatientFinancial pf ON pf.FinancialId = pr.ComputedFinancialId
		INNER JOIN dbo.PracticeCodeTable pct ON ReferenceType = ''PAYMENTTYPE''
			AND prp.PaymentType = SUBSTRING(Code, LEN(code), 1)
			AND (pct.AlternateCode = '''' 
				OR pct.AlternateCode IS NULL)
		INNER JOIN dbo.PatientReceivableServices prs on prs.ItemId = prp.PaymentServiceItem
			AND prs.[Status] = ''A''
		WHERE prp.PaymentFinancialType = ''''	AND prp.PayerId <> 0'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.FinancialInformations'
			,@FinancialInformationsViewSql
			,@isChanged = @isFinancialInformationsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFinancialInformationDelete'
				,
				'
			CREATE TRIGGER model.OnFinancialInformationDelete ON [model].[FinancialInformations] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Amount decimal(18,0)
			DECLARE @PostedDateTime datetime
			DECLARE @FinancialInformationTypeId int
			DECLARE @BillingServiceId int
			DECLARE @InvoiceReceivableId bigint
			DECLARE @FinancialSourceTypeId int
			DECLARE @PatientId int
			DECLARE @InsurerId int
			DECLARE @IsArchived bit
			DECLARE @FinancialBatchId int
		
			DECLARE FinancialInformationsDeletedCursor CURSOR FOR
			SELECT Id, Amount, PostedDateTime, FinancialInformationTypeId, BillingServiceId, InvoiceReceivableId, FinancialSourceTypeId, PatientId, InsurerId, IsArchived, FinancialBatchId FROM deleted
		
			OPEN FinancialInformationsDeletedCursor
			FETCH NEXT FROM FinancialInformationsDeletedCursor INTO @Id, @Amount, @PostedDateTime, @FinancialInformationTypeId, @BillingServiceId, @InvoiceReceivableId, @FinancialSourceTypeId, @PatientId, @InsurerId, @IsArchived, @FinancialBatchId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for FinancialInformation --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Amount AS Amount, @PostedDateTime AS PostedDateTime, @FinancialInformationTypeId AS FinancialInformationTypeId, @BillingServiceId AS BillingServiceId, @InvoiceReceivableId AS InvoiceReceivableId, @FinancialSourceTypeId AS FinancialSourceTypeId, @PatientId AS PatientId, @InsurerId AS InsurerId, @IsArchived AS IsArchived, @FinancialBatchId AS FinancialBatchId
			
				FETCH NEXT FROM FinancialInformationsDeletedCursor INTO @Id, @Amount, @PostedDateTime, @FinancialInformationTypeId, @BillingServiceId, @InvoiceReceivableId, @FinancialSourceTypeId, @PatientId, @InsurerId, @IsArchived, @FinancialBatchId 
		
			END
		
			CLOSE FinancialInformationsDeletedCursor
			DEALLOCATE FinancialInformationsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFinancialInformationInsert'
				,
				'
			CREATE TRIGGER model.OnFinancialInformationInsert ON [model].[FinancialInformations] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Amount decimal(18,0)
			DECLARE @PostedDateTime datetime
			DECLARE @FinancialInformationTypeId int
			DECLARE @BillingServiceId int
			DECLARE @InvoiceReceivableId bigint
			DECLARE @FinancialSourceTypeId int
			DECLARE @PatientId int
			DECLARE @InsurerId int
			DECLARE @IsArchived bit
			DECLARE @FinancialBatchId int
		
			DECLARE FinancialInformationsInsertedCursor CURSOR FOR
			SELECT Id, Amount, PostedDateTime, FinancialInformationTypeId, BillingServiceId, InvoiceReceivableId, FinancialSourceTypeId, PatientId, InsurerId, IsArchived, FinancialBatchId FROM inserted
		
			OPEN FinancialInformationsInsertedCursor
			FETCH NEXT FROM FinancialInformationsInsertedCursor INTO @Id, @Amount, @PostedDateTime, @FinancialInformationTypeId, @BillingServiceId, @InvoiceReceivableId, @FinancialSourceTypeId, @PatientId, @InsurerId, @IsArchived, @FinancialBatchId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for FinancialInformation --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Amount AS Amount, @PostedDateTime AS PostedDateTime, @FinancialInformationTypeId AS FinancialInformationTypeId, @BillingServiceId AS BillingServiceId, @InvoiceReceivableId AS InvoiceReceivableId, @FinancialSourceTypeId AS FinancialSourceTypeId, @PatientId AS PatientId, @InsurerId AS InsurerId, @IsArchived AS IsArchived, @FinancialBatchId AS FinancialBatchId
				
				FETCH NEXT FROM FinancialInformationsInsertedCursor INTO @Id, @Amount, @PostedDateTime, @FinancialInformationTypeId, @BillingServiceId, @InvoiceReceivableId, @FinancialSourceTypeId, @PatientId, @InsurerId, @IsArchived, @FinancialBatchId 
		
			END
		
			CLOSE FinancialInformationsInsertedCursor
			DEALLOCATE FinancialInformationsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFinancialInformationUpdate'
				,
				'
			CREATE TRIGGER model.OnFinancialInformationUpdate ON [model].[FinancialInformations] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Amount decimal(18,0)
			DECLARE @PostedDateTime datetime
			DECLARE @FinancialInformationTypeId int
			DECLARE @BillingServiceId int
			DECLARE @InvoiceReceivableId bigint
			DECLARE @FinancialSourceTypeId int
			DECLARE @PatientId int
			DECLARE @InsurerId int
			DECLARE @IsArchived bit
			DECLARE @FinancialBatchId int
		
			DECLARE FinancialInformationsUpdatedCursor CURSOR FOR
			SELECT Id, Amount, PostedDateTime, FinancialInformationTypeId, BillingServiceId, InvoiceReceivableId, FinancialSourceTypeId, PatientId, InsurerId, IsArchived, FinancialBatchId FROM inserted
		
			OPEN FinancialInformationsUpdatedCursor
			FETCH NEXT FROM FinancialInformationsUpdatedCursor INTO @Id, @Amount, @PostedDateTime, @FinancialInformationTypeId, @BillingServiceId, @InvoiceReceivableId, @FinancialSourceTypeId, @PatientId, @InsurerId, @IsArchived, @FinancialBatchId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for FinancialInformation --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM FinancialInformationsUpdatedCursor INTO @Id, @Amount, @PostedDateTime, @FinancialInformationTypeId, @BillingServiceId, @InvoiceReceivableId, @FinancialSourceTypeId, @PatientId, @InsurerId, @IsArchived, @FinancialBatchId 
		
			END
		
			CLOSE FinancialInformationsUpdatedCursor
			DEALLOCATE FinancialInformationsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.FinancialInformationTypes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isFinancialInformationTypesChanged BIT

		SET @isFinancialInformationTypesChanged = 0

		DECLARE @ismodelFinancialInformationTypes_Partition1Changed BIT

		SET @ismodelFinancialInformationTypes_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.FinancialInformationTypes_Partition1'
			,
			'
		CREATE VIEW model.FinancialInformationTypes_Partition1
		WITH SCHEMABINDING
		AS
		SELECT CASE
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''!''
					THEN 1
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''|''
					THEN 2
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''?''
					THEN 3
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''%''
					THEN 4
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''D''
					THEN 5
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''&''
					THEN 6
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = '']''
					THEN 7
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''0''
					THEN 8
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''V''
					THEN 9
				ELSE pct.Id + 1000
				END AS Id,
				NULL AS FinancialTypeGroupId,
				LetterTranslation AS ShortName,
				Code AS Code,
				COUNT_BIG(*) AS Count,
				CONVERT(bit, 0) AS IsArchived
			FROM dbo.PracticeCodeTable pct
			WHERE pct.ReferenceType = ''PAYMENTTYPE''		
				AND (pct.AlternateCode = '''' OR pct.AlternateCode IS NULL)
			GROUP BY pct.Id,
				LetterTranslation,
				Code
		'
			,@isChanged = @ismodelFinancialInformationTypes_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_FinancialInformationTypes_Partition1'
					AND object_id = OBJECT_ID('model.FinancialInformationTypes_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.FinancialInformationTypes_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_FinancialInformationTypes_Partition1 ON model.FinancialInformationTypes_Partition1 ([Id]);
		END

		DECLARE @FinancialInformationTypesViewSql NVARCHAR(max)

		SET @FinancialInformationTypesViewSql = '
		CREATE VIEW [model].[FinancialInformationTypes]
		WITH SCHEMABINDING
		AS
		SELECT v.Id AS Id,
			v.FinancialTypeGroupId AS FinancialTypeGroupId,
			CONVERT(bit, 1) AS IsPrintOnStatement,
			SUBSTRING(v.Code, 1, (dbo.GetMax((CHARINDEX(''-'', v.code) - 2),0))) AS [Name],
			v.ShortName AS ShortName,
			v.IsArchived
		FROM (
	
		SELECT model.FinancialInformationTypes_Partition1.Id AS Id, model.FinancialInformationTypes_Partition1.FinancialTypeGroupId AS FinancialTypeGroupId, model.FinancialInformationTypes_Partition1.ShortName AS ShortName, model.FinancialInformationTypes_Partition1.Code AS Code, model.FinancialInformationTypes_Partition1.Count AS Count, model.FinancialInformationTypes_Partition1.IsArchived AS IsArchived FROM model.FinancialInformationTypes_Partition1 WITH(NOEXPAND)
	
		) AS v'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.FinancialInformationTypes'
			,@FinancialInformationTypesViewSql
			,@isChanged = @isFinancialInformationTypesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFinancialInformationTypeDelete'
				,
				'
			CREATE TRIGGER model.OnFinancialInformationTypeDelete ON [model].[FinancialInformationTypes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @ShortName nvarchar(max)
			DECLARE @IsPrintOnStatement bit
			DECLARE @FinancialTypeGroupId int
			DECLARE @IsArchived bit
		
			DECLARE FinancialInformationTypesDeletedCursor CURSOR FOR
			SELECT Id, Name, ShortName, IsPrintOnStatement, FinancialTypeGroupId, IsArchived FROM deleted
		
			OPEN FinancialInformationTypesDeletedCursor
			FETCH NEXT FROM FinancialInformationTypesDeletedCursor INTO @Id, @Name, @ShortName, @IsPrintOnStatement, @FinancialTypeGroupId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for FinancialInformationType --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name, @ShortName AS ShortName, @IsPrintOnStatement AS IsPrintOnStatement, @FinancialTypeGroupId AS FinancialTypeGroupId, @IsArchived AS IsArchived
			
				FETCH NEXT FROM FinancialInformationTypesDeletedCursor INTO @Id, @Name, @ShortName, @IsPrintOnStatement, @FinancialTypeGroupId, @IsArchived 
		
			END
		
			CLOSE FinancialInformationTypesDeletedCursor
			DEALLOCATE FinancialInformationTypesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFinancialInformationTypeInsert'
				,
				'
			CREATE TRIGGER model.OnFinancialInformationTypeInsert ON [model].[FinancialInformationTypes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @ShortName nvarchar(max)
			DECLARE @IsPrintOnStatement bit
			DECLARE @FinancialTypeGroupId int
			DECLARE @IsArchived bit
		
			DECLARE FinancialInformationTypesInsertedCursor CURSOR FOR
			SELECT Id, Name, ShortName, IsPrintOnStatement, FinancialTypeGroupId, IsArchived FROM inserted
		
			OPEN FinancialInformationTypesInsertedCursor
			FETCH NEXT FROM FinancialInformationTypesInsertedCursor INTO @Id, @Name, @ShortName, @IsPrintOnStatement, @FinancialTypeGroupId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for FinancialInformationType --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name, @ShortName AS ShortName, @IsPrintOnStatement AS IsPrintOnStatement, @FinancialTypeGroupId AS FinancialTypeGroupId, @IsArchived AS IsArchived
				
				FETCH NEXT FROM FinancialInformationTypesInsertedCursor INTO @Id, @Name, @ShortName, @IsPrintOnStatement, @FinancialTypeGroupId, @IsArchived 
		
			END
		
			CLOSE FinancialInformationTypesInsertedCursor
			DEALLOCATE FinancialInformationTypesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnFinancialInformationTypeUpdate'
				,
				'
			CREATE TRIGGER model.OnFinancialInformationTypeUpdate ON [model].[FinancialInformationTypes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @ShortName nvarchar(max)
			DECLARE @IsPrintOnStatement bit
			DECLARE @FinancialTypeGroupId int
			DECLARE @IsArchived bit
		
			DECLARE FinancialInformationTypesUpdatedCursor CURSOR FOR
			SELECT Id, Name, ShortName, IsPrintOnStatement, FinancialTypeGroupId, IsArchived FROM inserted
		
			OPEN FinancialInformationTypesUpdatedCursor
			FETCH NEXT FROM FinancialInformationTypesUpdatedCursor INTO @Id, @Name, @ShortName, @IsPrintOnStatement, @FinancialTypeGroupId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for FinancialInformationType --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM FinancialInformationTypesUpdatedCursor INTO @Id, @Name, @ShortName, @IsPrintOnStatement, @FinancialTypeGroupId, @IsArchived 
		
			END
		
			CLOSE FinancialInformationTypesUpdatedCursor
			DEALLOCATE FinancialInformationTypesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.IdentifierCodes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isIdentifierCodesChanged BIT

		SET @isIdentifierCodesChanged = 0

		DECLARE @ismodelIdentifierCodes_Partition1Changed BIT

		SET @ismodelIdentifierCodes_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.IdentifierCodes_Partition1'
			,'
		CREATE VIEW model.IdentifierCodes_Partition1
		WITH SCHEMABINDING
		AS
		---Taxonomy IdentifierCode - Index 0
		----Identifier Code Taxonomy BillingOrganization from PracticeName (0)
		SELECT ''0'' + ''_'' + ''0'' + ''_'' + ''10'' + ''_'' + CONVERT(nvarchar, pnBillOrg.PracticeId) AS Id,
			pnBillOrg.PracticeId AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			1 AS IdentifierCodeTypeId,
			NULL AS ProviderId,
			NULL AS ServiceLocationId,
			pnBillOrg.PracticeTaxonomy AS Value
		FROM dbo.PracticeName pnBillOrg
		WHERE pnBillOrg.PracticeType = ''P''
			AND pnBillOrg.PracticeTaxonomy <> ''''
			AND pnBillOrg.PracticeTaxonomy IS NOT NULL
		'
			,@isChanged = @ismodelIdentifierCodes_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_IdentifierCodes_Partition1'
					AND object_id = OBJECT_ID('model.IdentifierCodes_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.IdentifierCodes_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_IdentifierCodes_Partition1 ON model.IdentifierCodes_Partition1 ([Id]);
		END

		DECLARE @IdentifierCodesViewSql NVARCHAR(max)

		SET @IdentifierCodesViewSql = 
			'
		CREATE VIEW [model].[IdentifierCodes]
		WITH SCHEMABINDING
		AS
		SELECT model.IdentifierCodes_Partition1.Id AS Id, model.IdentifierCodes_Partition1.BillingOrganizationId AS BillingOrganizationId, model.IdentifierCodes_Partition1.ExternalProviderId AS ExternalProviderId, model.IdentifierCodes_Partition1.IdentifierCodeTypeId AS IdentifierCodeTypeId, model.IdentifierCodes_Partition1.ProviderId AS ProviderId, model.IdentifierCodes_Partition1.ServiceLocationId AS ServiceLocationId, model.IdentifierCodes_Partition1.Value AS Value FROM model.IdentifierCodes_Partition1 WITH(NOEXPAND)
	
		UNION ALL
	
		----Identifier Code Taxonomy BillingOrganization w PracticeAffiliations.OrgOverride/Override - Resources (1)
		--*/*(no index)*/*
		SELECT ''0'' + ''_'' + ''1'' + ''_'' + ''10'' + ''_'' + CONVERT(nvarchar, v.ResourceId) AS Id, 
			(110000000 * 1 + v.ResourceId) AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			v.ProviderId AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value
		FROM (SELECT
				NULL AS ExternalProviderId,
				1 AS IdentifierCodeTypeId,
				NULL AS ProviderId,
				reBillOrg.Vacation AS Value, 
				reBillOrg.ResourceId AS ResourceId,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources reBillOrg
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = reBillOrg.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			WHERE reBillOrg.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND reBillOrg.Vacation <> ''''
				AND reBillOrg.Vacation IS NOT NULL
				GROUP BY reBillOrg.Vacation,
					reBillOrg.ResourceId
			) AS v
		
		UNION ALL
	
		----Identifier Code Taxonomy Provider (human) w ServiceLocation from PracticeName (Index 0)
		SELECT ''0'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, pnServLoc.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			1 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			re.Vacation AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.Vacation <> ''''
			AND re.Vacation IS NOT NULL
	
		UNION ALL
	
		-----Identifier Code Taxonomy Provider (facility) ServiceLocation from PracticeName (always); IdentifierCodes from Resources
		SELECT ''0'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, reASC.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnServLoc.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, pnServLoc.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			1 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + reASC.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			reASC.Vacation AS Value
		FROM dbo.Resources reASC
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeId = reASC.PracticeId
			AND pnServLoc.PracticeType = ''P''
			AND LocationReference <> ''''
		WHERE reASC.ResourceType = ''R''
			AND reASC.ServiceCode = ''02''
			AND reASC.Billable = ''Y''
			AND pic.FieldValue = ''T''
			AND reASC.Vacation <> ''''
			AND reASC.Vacation IS NOT NULL
	
		UNION ALL
	
		----Identifier Code Taxonomy Provider w Resources ServiceLocation  (Index 1)
		SELECT ''0'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, reServLoc.ResourceId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			1 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			re.Vacation AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ServiceCode = ''02''
			AND reServLoc.ResourceType = ''R''
		INNER JOIN dbo.PracticeName pnFacBillOrg ON pnFacBillOrg.PracticeId = reServLoc.PracticeId
			AND pnFacBillOrg.PracticeType = ''P''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.Vacation <> ''''
			AND re.Vacation IS NOT NULL
	
		UNION ALL
	
		----Identifier Code Taxonomy Provider w OrgOverride/Override w ServLoc from PracticeName (Index 0)
		SELECT ''0'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,v.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, v.PracticeId) AS Id,
			v.BillingOrganizationId AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 2 + v.PracticeId) + (CONVERT(bigint, 110000000) * v.ResourceId + v.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value
			FROM(SELECT 
				NULL AS BillingOrganizationId,
				NULL AS ExternalProviderId,
				1 AS IdentifierCodeTypeId,
				re.Vacation AS Value,
				re.ResourceId AS ResourceId,
				pnServLoc.PracticeId AS PracticeId,
				re.ResourceType AS ResourceType,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources re
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND re.Vacation <> ''''
				AND re.Vacation IS NOT NULL
			GROUP BY re.Vacation,
				re.ResourceId,
				pnServLoc.PracticeId,
				re.ResourceTYpe
			) AS v
		UNION ALL
	
		----Identifier Code Taxonomy Provider  w OrgOverride/Override w ServLoc from Resources (index 1)
		SELECT ''0'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, v.ResourceIdreServLoc) AS Id,
			v.BillingOrganizationId AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 3 + v.ResourceIdreServLoc) + (CONVERT(bigint, 110000000) * v.ResourceId + v.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value
		FROM (SELECT
				NULL AS BillingOrganizationId,
				NULL AS ExternalProviderId,
				1 AS IdentifierCodeTypeId,
				re.Vacation AS Value,
				reServLoc.ResourceId AS ResourceIdreServLoc,
				re.ResourceId ResourceId,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources re
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' AND reServLoc.ServiceCode = ''02''
			WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND re.Vacation <> ''''
				AND re.Vacation IS NOT NULL
			GROUP BY re.Vacation,
				reServLoc.ResourceId,
				re.ResourceId,
				re.ResourceTYpe
			) AS v
	
		UNION ALL
	
		---NPI IdentifierCode (Index 1)
		----BillingOrganization from PracticeName table (Index 0)
		SELECT ''1'' + ''_'' + ''0'' + ''_'' + ''10'' + ''_'' + CONVERT(nvarchar, pnBillOrg.PracticeId) AS Id,
			pnBillOrg.PracticeId AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			2 AS IdentifierCodeTypeId,
			NULL AS ProviderId,
			NULL AS ServiceLocationId,
			pnBillOrg.PracticeNPI AS Value
		FROM dbo.PracticeName pnBillOrg
		WHERE pnBillOrg.PracticeType = ''P''
			AND pnBillOrg.PracticeNPI <> ''''
			AND pnBillOrg.PracticeNPI IS NOT NULL
	
		UNION ALL
	
		----IdentifierCode NPI BillingOrganization - Resources table where PracticeAffiliation.Override or OrgOverride ONLY
		SELECT ''1'' + ''_'' + ''1'' + ''_'' + ''10'' + ''_'' + CONVERT(nvarchar, v.ResourceId) AS Id,
			(110000000 * 1 + v.ResourceId) AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			v.ProviderId AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value
		FROM (SELECT 
				NULL AS ExternalProviderId,
				2 AS IdentifierCodeTypeId,
				NULL AS ProviderId,
				reBillOrg.NPI AS Value,
				reBillOrg.ResourceId AS ResourceId,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources reBillOrg
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = reBillOrg.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			WHERE reBillOrg.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND reBillOrg.NPI <> ''''
				AND reBillOrg.NPI IS NOT NULL
			GROUP BY reBillOrg.ResourceId,
				reBillOrg.NPI,
				reBillOrg.ResourceType
			) AS v
		
		UNION ALL
	
		---IdentifierCode NPI Provider  (human providers) service location is from PracticeName table 
		SELECT ''1'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, pnServLoc.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			2 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			re.NPI AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.NPI <> ''''
			AND re.NPI IS NOT NULL
	
		UNION ALL
	
		---IdentifierCode NPI Provider (Facility) service location from PracticeName table, identifier code from Resources 
		SELECT ''1'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, reASC.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnServLoc.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, pnServLoc.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			2 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + reASC.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			reASC.NPI AS Value
		FROM dbo.Resources reASC
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeId = reASC.PracticeId
			AND pnServLoc.PracticeType = ''P''
		WHERE reASC.ResourceType = ''R''
			AND reASC.ServiceCode = ''02''
			AND reASC.Billable = ''Y''
			AND pic.FieldValue = ''T''
			AND reASC.NPI <> ''''
			AND reASC.NPI IS NOT NULL
			AND pnServLoc.LocationReference <> ''''
	
		UNION ALL
	
		----IdentifierCode NPI Provider W service location from Resources (not Prov Internal Fac; ServLocation only from PracticeName)
		SELECT ''1'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, reServLoc.ResourceId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			2 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			re.NPI AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ServiceCode = ''02''
			AND reServLoc.ResourceType = ''R''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.NPI <> ''''
			AND re.NPI IS NOT NULL
	
		UNION ALL
	
		----IdentifierCode NPI Provider  w OrgOverride/Override w ServLoc fr PracticeName (no internal facilities w OrgOverride)
		SELECT ''1'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,v.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, v.PracticeId) AS Id,
			v.BillingOrganizationId AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 2 + v.PracticeId) + (CONVERT(bigint, 110000000) * v.ResourceId + v.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value
		FROM(SELECT	
				NULL AS BillingOrganizationId,
				NULL AS ExternalProviderId,
				2 AS IdentifierCodeTypeId,
				re.NPI AS Value,
				pnServLoc.PracticeId AS PracticeId,
				re.ResourceId AS ResourceId,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources re
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND re.NPI <> ''''
				AND re.NPI IS NOT NULL
			GROUP BY re.NPI,
				pnServLoc.PracticeId,
				re.ResourceId,
				re.ResourceType
			) AS v
	
		UNION ALL
	
		----IdentifierCode NPI Provider  w OrgOverride/Override w ServiceLoc fr Resources (no Provider internal facilities w OrgOverride)
		SELECT ''1'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,v.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, v.reServLocResourceId) AS Id,
			v.BillingOrganizationId AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 3 + v.reServLocResourceId) + (CONVERT(bigint, 110000000) * v.ResourceId + v.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value
		FROM(SELECT
				NULL AS BillingOrganizationId,
				NULL AS ExternalProviderId,
				2 AS IdentifierCodeTypeId,
				re.NPI AS Value,
				reServLoc.ResourceId AS reServLocResourceId,
				re.ResourceId AS ResourceId,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources re
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' 
				AND reServLoc.ServiceCode = ''02''
			WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND re.NPI <> ''''
				AND re.NPI IS NOT NULL
			GROUP BY re.NPI,
				reServLoc.ResourceId,
				re.ResourceId,
				re.ResourceType
		) AS v
		
		UNION ALL
	
		---IdentifierCode NPI ExternalProvider (Index 5)
		SELECT ''1'' + ''_'' + ''5'' + ''_'' + CONVERT(nvarchar, VendorId) AS Id,
			NULL AS BillingOrganizationId,
			VendorId AS ExternalProviderId,
			2 AS IdentifierCodeTypeId,
			NULL AS ProviderId,
			NULL AS ServiceLocationId,
			pv.VendorNPI AS Value
		FROM dbo.PracticeVendors pv
		WHERE VendorType = ''D''
			AND len(pv.VendorNPI) = 10 
			AND ISNUMERIC(pv.VendorNPI) = 1
			AND VendorLastName <> ''''
			AND VendorLastName IS NOT NULL
	
		UNION ALL
	
		---IdentifierCode NPI ExternalOrganization (maybe a clinic is the referral source) IdentifierCode --we don''t support NPIs for external organizations in old IO
	
		---IdentifierCode NPI ServiceLocation  from PracticeName table (Index 0)
		SELECT ''1'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, pnServLoc.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			2 AS IdentifierCodeTypeId,
			NULL AS ProviderId,
			pnServLoc.PracticeId AS ServiceLocationId,
			CASE
				WHEN len(pnServLoc.PracticeNPI) = 10
					THEN pnServLoc.PracticeNPI
				ELSE pnMainLocation.PracticeNPI
				END AS Value
		FROM dbo.PracticeName pnServLoc
		INNER JOIN dbo.PracticeName pnMainLocation ON pnMainLocation.PracticeType = ''P''
			AND pnMainLocation.LocationReference = ''''
			AND len(pnMainLocation.PracticeNPI) = 10
		WHERE  pnServLoc.PracticeType = ''P''
			AND pnServLoc.PracticeNPI <> ''''
			AND pnServLoc.PracticeNPI IS NOT NULL
	
		UNION ALL
	
		----IdentifierCode NPI ServiceLocation - Resources table 
		SELECT ''1'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, reServLoc.ResourceId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			2 AS IdentifierCodeTypeId,
			NULL AS ProviderId,
			(110000000 * 1 + reServLoc.ResourceId) AS ServiceLocationId,
			reServLoc.NPI AS Value
			FROM dbo.Resources reServLoc
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = reServLoc.PracticeId
			AND pnBillOrg.PracticeType = ''P''
		WHERE reServLoc.ResourceType = ''R''
			AND reServLoc.ServiceCode = ''02''
			AND reServLoc.NPI <> ''''
			AND reServLoc.NPI IS NOT NULL
	
		UNION ALL
	
		---IdentifierCode SSN (Index 2) - BillingOrganization - From PracticeName table (Index 0)
		SELECT ''2'' + ''_'' + ''0'' + ''_'' + ''10'' + ''_'' + CONVERT(nvarchar, pnBillOrg.PracticeId) AS Id,
			pnBillOrg.PracticeId AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			3 AS IdentifierCodeTypeId,
			NULL AS ProviderId,
			NULL AS ServiceLocationId,
			SUBSTRING(pnBillOrg.PracticeTaxId, 1, 3) + SUBSTRING(pnBillOrg.PracticeTaxId, 5, 2) + SUBSTRING(pnBillOrg.PracticeTaxId, 8, 4) AS Value
		FROM dbo.PracticeName pnBillOrg
		WHERE pnBillOrg.PracticeType = ''P''
			AND pnBillOrg.PracticeTaxId LIKE ''%-%-%''
	
		UNION ALL
	
		----IdentifierCode SSN - BillingOrganization - From Resources (human) where PracticeAffiliation.OrgOverride/Override = T
		SELECT ''2'' + ''_'' + ''1'' + ''_'' + ''10'' + ''_'' + CONVERT(nvarchar, reBillOrg.ResourceId) AS Id,
			(110000000 * 1 + reBillOrg.ResourceId) AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			3 AS IdentifierCodeTypeId,
			NULL AS ProviderId,
			NULL AS ServiceLocationId,
			SUBSTRING(reBillOrg.ResourceTaxId, 1, 3) + SUBSTRING(reBillOrg.ResourceTaxId, 5, 2) + SUBSTRING(reBillOrg.ResourceTaxId, 8, 4) AS Value
		FROM dbo.Resources reBillOrg
		INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = reBillOrg.ResourceId
			AND pa.ResourceType = ''R''
			AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
		WHERE reBillOrg.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND reBillOrg.ResourceTaxId LIKE ''%-%-%''
		GROUP BY  ''2'' + ''_'' + ''1'' + ''_'' + ''10'' + ''_'' + CONVERT(nvarchar, reBillOrg.ResourceId) 
			,(110000000 * 1 + reBillOrg.ResourceId) 
			,SUBSTRING(reBillOrg.ResourceTaxId, 1, 3) + SUBSTRING(reBillOrg.ResourceTaxId, 5, 2) + SUBSTRING(reBillOrg.ResourceTaxId, 8, 4) 
	
		----BillingOrganization Resources - external facilities wouldn''t have an SSN
		----Patient SSN saved in Patient entity
	
		UNION ALL
	
		----IdentifierCode SSN - Provider - ServiceLocation from PracticeName table (index 0)
		SELECT ''2'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar,re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, pnServLoc.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			3 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			SUBSTRING(re.ResourceTaxId, 1, 3) + SUBSTRING(re.ResourceTaxId, 5, 2) + SUBSTRING(re.ResourceTaxId, 8, 4) AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.ResourceTaxId LIKE ''%-%-%''
	
		UNION ALL
	
		---IdentifierCode SSN - Provider  - ServiceLocation from Resources table (index 1)
		SELECT ''2'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, reServLoc.ResourceId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			3 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			SUBSTRING(re.ResourceTaxId, 1, 3) + SUBSTRING(re.ResourceTaxId, 5, 2) + SUBSTRING(re.ResourceTaxId, 8, 4) AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
			AND pnBillOrg.PracticeType = ''P''
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' 
			AND reServLoc.ServiceCode = ''02''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.ResourceTaxId LIKE ''%-%-%''
	
		UNION ALL
	
		---IdentifierCode SSN - Provider  w PracticeAffiliation.OrgOverride/Override - ServiceLocation from PracticeName table
		SELECT ''2'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,re.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, pnServLoc.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			3 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 2 + pnServLoc.PracticeId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			SUBSTRING(re.ResourceTaxId, 1, 3) + SUBSTRING(re.ResourceTaxId, 5, 2) + SUBSTRING(re.ResourceTaxId, 8, 4) AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
			AND pa.ResourceType = ''R''
			AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.ResourceTaxId LIKE ''%-%-%''
		GROUP BY ''2'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,re.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, pnServLoc.PracticeId)
			,(CONVERT(bigint, 110000000) * (110000000 * 2 + pnServLoc.PracticeId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId)) 
			,SUBSTRING(re.ResourceTaxId, 1, 3) + SUBSTRING(re.ResourceTaxId, 5, 2) + SUBSTRING(re.ResourceTaxId, 8, 4) 
	
		UNION ALL
	
		---IdentifierCode SSN - Provider  w PracticeAffiliation.OrgOverride/Override - ServiceLocation from Resource table
		SELECT ''2'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,re.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, reServLoc.ResourceId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			3 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 3 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			SUBSTRING(re.ResourceTaxId, 1, 3) + SUBSTRING(re.ResourceTaxId, 5, 2) + SUBSTRING(re.ResourceTaxId, 8, 4) AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
			AND pa.ResourceType = ''R''
			AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' AND reServLoc.ServiceCode = ''02''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.ResourceTaxId LIKE ''%-%-%''
		GROUP BY ''2'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,re.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, reServLoc.ResourceId) 
			,(CONVERT(bigint, 110000000) * (110000000 * 3 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId)) 
			,SUBSTRING(re.ResourceTaxId, 1, 3) + SUBSTRING(re.ResourceTaxId, 5, 2) + SUBSTRING(re.ResourceTaxId, 8, 4)  
	
		UNION ALL
	
		--UPIN IdentifierCode - supposed to no longer be used; won''t script unless we have to - for ProviderBillingOrganizationServiceLocation and ExternalProvider
	
		---IdentifierCode License (Index 4) Provider - ServiceLocation from PracticeName table
		SELECT ''4'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, pnServLoc.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			5 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			re.ResourceLicence AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.ResourceLicence <> ''''
			AND re.ResourceLicence IS NOT NULL --sic
	
		UNION ALL
	
		----IdentifierCode License Provider  - ServiceLocation from Resources table (omitting internal facilities from Resources)
		SELECT ''4'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, reServLoc.ResourceId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			5 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			re.ResourceLicence AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' 
			AND reServLoc.ServiceCode = ''02''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.ResourceLicence <> ''''
			AND re.ResourceLicence IS NOT NULL --sic
	
		UNION ALL
	
		---IdentifierCode License Provider  w PracticeAffiliation.OrgOverride/Override - ServiceLocation from PracticeName table
		SELECT ''4'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,v.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, v.PracticeId) AS Id,
			v.BillingOrganizationId AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 2 + v.PracticeId) + (CONVERT(bigint, 110000000) * v.ResourceId + v.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value
		FROM(SELECT 
				NULL AS BillingOrganizationId,
				NULL AS ExternalProviderId,
				5 AS IdentifierCodeTypeId,
				re.ResourceLicence AS Value,
				pnServLoc.PracticeId AS PracticeId,
				re.ResourceId AS ResourceId,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources re
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND re.ResourceLicence <> ''''
				AND re.ResourceLicence IS NOT NULL --sic
			GROUP BY pnServLoc.PracticeId,
				re.ResourceLicence,
				re.ResourceId,
				re.ResourceType
		) AS v
	
		UNION ALL
	
		---IdentifierCode License Provider  w PracticeAffiliation.OrgOverride/Override - ServiceLocation from Resource table 
		SELECT ''4'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,v.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, v.reServLocResourceId) AS Id,
			v.BillingOrganizationId AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 3 + v.reServLocResourceId) + (CONVERT(bigint, 110000000) * v.ResourceId + v.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value
		FROM (SELECT 
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			5 AS IdentifierCodeTypeId,
			re.ResourceLicence AS Value,
			reServLoc.ResourceId AS reServLocResourceId,
			re.ResourceId AS ResourceId,
			COUNT_BIG(*) AS Count
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
			AND pa.ResourceType = ''R''
			AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' AND reServLoc.ServiceCode = ''02''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
	 		AND re.ResourceLicence <> ''''
			AND re.ResourceLicence IS NOT NULL --sic
		GROUP BY re.ResourceLicence,
			reServLoc.ResourceId,
			re.ResourceId,
			re.ResourceType
		) AS v
	
		UNION ALL
	
		---IdentifierCode DEA (Index 5) Provider - ServiceLocation from PracticeName
		SELECT ''5'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, pnServLoc.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			6 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			re.ResourceDEA AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.ResourceDEA <> ''''
			AND re.ResourceDEA IS NOT NULL
	
		UNION ALL
	
		----IdentifierCode DEA Provider - ServiceLocation from Resources
		SELECT ''5'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, reServLoc.ResourceId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			6 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			re.ResourceDEA AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' AND reServLoc.ServiceCode = ''02''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.ResourceDEA <> ''''
			AND re.ResourceDEA IS NOT NULL
	
	
		UNION ALL
	
		----IdentifierCode DEA Provider  w PracticeAffiliation.OrgOverride/Override - ServiceLocation from PracticeName table 
		SELECT ''5'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,v.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, v.PracticeId) AS Id,
			v.BillingOrganizationId AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 2 + v.PracticeId) + (CONVERT(bigint, 110000000) * v.ResourceId + v.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value
		FROM(SELECT 
				NULL AS BillingOrganizationId,
				NULL AS ExternalProviderId,
				6 AS IdentifierCodeTypeId,
				re.ResourceDEA AS Value,
				pnServLoc.PracticeId AS PracticeId,
				re.ResourceId AS ResourceId,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources re
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND re.ResourceDEA <> ''''
				AND re.ResourceDEA IS NOT NULL 
			GROUP BY re.ResourceDEA,
				pnServLoc.PracticeId,
				re.ResourceId,
				re.ResourceType
		) AS v
	
		UNION ALL
	
		----IdentifierCode DEA Provider  w PracticeAffiliation.OrgOverride/Override - ServiceLocation from Resource table 
		SELECT ''5'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,v.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, v.reServLocResourceId) AS Id,
			v.BillingOrganizationId AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 3 + v.reServLocResourceId) + (CONVERT(bigint, 110000000) * v.ResourceId + v.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value
		FROM (SELECT
				NULL AS BillingOrganizationId,
				NULL AS ExternalProviderId,
				6 AS IdentifierCodeTypeId,
				re.ResourceDEA AS Value,
				reServLoc.ResourceId AS reServLocResourceId,
				re.ResourceId AS ResourceId,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources re
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' 
				AND reServLoc.ServiceCode = ''02''
			WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
	 			AND re.ResourceDEA <> ''''
				AND re.ResourceDEA IS NOT NULL --sic
			GROUP BY re.ResourceDEA,
				re.ResourceId,
				reServLoc.ResourceId,
				re.ResourceType
			) AS v
		UNION ALL
	
		----IdentifiderCode TaxId (Index 6) BillingOrganization From PracticeName table (Index 1)
		SELECT ''6'' + ''_'' + ''0'' + ''_'' + ''10'' + ''_'' + CONVERT(nvarchar, pnBillOrg.PracticeId) AS Id,
			pnBillOrg.PracticeId AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			7 AS IdentifierCodeTypeId,
			NULL AS ProviderId,
			NULL AS ServiceLocationId,
			pnBillOrg.PracticeTaxId AS Value
		FROM dbo.PracticeName pnBillOrg
		WHERE pnBillOrg.PracticeType = ''P''
			AND pnBillOrg.PracticeTaxId <> ''''
			AND pnBillOrg.PracticeTaxId IS NOT NULL
			AND pnBillOrg.PracticeTaxId NOT LIKE ''%-%-%''
		
		UNION ALL
	
		----IdentifierCode TaxId BillingOrganization w OrgOverride/Override (Resources)
		SELECT ''6'' + ''_'' + ''1'' + ''_'' + ''10'' + ''_'' + CONVERT(nvarchar, v.ResourceId)  AS Id,
			(110000000 * 1 + v.ResourceId) AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			v.ProviderId AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value
		FROM(SELECT
				NULL AS ExternalProviderId,
				7 AS IdentifierCodeTypeId,
				NULL AS ProviderId,
				reBillOrg.ResourceTaxId AS Value,
				reBillOrg.ResourceId AS ResourceId
			FROM dbo.Resources reBillOrg
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = reBillOrg.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			WHERE reBillOrg.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND reBillOrg.ResourceTaxId <> ''''
				AND reBillOrg.ResourceTaxId IS NOT NULL
				AND reBillOrg.ResourceTaxId NOT LIKE ''%-%-%''
			GROUP BY reBillOrg.ResourceId,
				reBillOrg.ResourceTaxId		
			) AS v
	
		UNION ALL
	
		----IdentifierCode TaxId Provider - ServiceLocation from PracticeName table - includes Internal Facilities as providers
		SELECT ''6'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, pnServLoc.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			7 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			re.ResourceTaxId AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
			AND pnBillOrg.PracticeType = ''P''
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			AND pnServLoc.PracticeId = CASE
					WHEN re.ResourceType = ''R'' 
						THEN pnBillOrg.PracticeId
					ELSE pnServLoc.PracticeId
					END
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.ResourceTaxId <> ''''
			AND re.ResourceTaxId IS NOT NULL
			AND re.ResourceTaxId NOT LIKE ''%-%-%''
	
		UNION ALL
	
		---IdentifierCode TaxId Provider  - ServiceLocation from Resources table
		----Only human providers; internal facilities have ServiceLocations from PracticeName table
		SELECT ''6'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, reServLoc.ResourceId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			7 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			re.ResourceTaxId AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' AND reServLoc.ServiceCode = ''02''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.ResourceTaxId <> ''''
			AND re.ResourceTaxId IS NOT NULL 
			AND re.ResourceTaxId NOT LIKE ''%-%-%''
	
		UNION ALL
	
		---IdentifierCode TaxId Provider w PracticeAffiliation.OrgOverride/Override - ServiceLocation from PracticeName table 
		SELECT ''6'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,v.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, v.PracticeId) AS Id,
			v.BillingOrganizationId AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 2 + v.PracticeId) + (CONVERT(bigint, 110000000) * v.ResourceId + v.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value	
		FROM (SELECT
				NULL AS BillingOrganizationId,
				NULL AS ExternalProviderId,
				7 AS IdentifierCodeTypeId,
				re.ResourceTaxId AS Value,
				pnServLoc.PracticeId AS PracticeId,
				re.ResourceId AS ResourceId,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources re
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND re.ResourceTaxId <> ''''
				AND re.ResourceTaxId IS NOT NULL 
				AND re.ResourceTaxId NOT LIKE ''%-%-%''
			GROUP BY re.ResourceTaxId,
				re.ResourceId, 
				pnServLoc.PracticeId
			) AS v
	
		UNION ALL
	
		---IdentifierCode TaxId Provider  w PracticeAffiliation.OrgOverride/Override - ServiceLocation from Resource table
		SELECT ''6'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,v.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, v.ReServLocResourceId) AS Id,
			v.BillingOrganizationId AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 3 + v.ReServLocResourceId) + (CONVERT(bigint, 110000000) * v.ResourceId + v.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value
		FROM (SELECT
				NULL AS BillingOrganizationId,
				NULL AS ExternalProviderId,
				7 AS IdentifierCodeTypeId,
				re.ResourceTaxId AS Value,
				reServLoc.ResourceId AS ReServLocResourceId,
				re.ResourceId AS ResourceId,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources re
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' AND reServLoc.ServiceCode = ''02''
			WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND re.ResourceTaxId <> ''''
				AND re.ResourceTaxId IS NOT NULL 
				AND re.ResourceTaxId NOT LIKE ''%-%-%''
			 GROUP BY re.ResourceTaxId,
				ReServLoc.ResourceId,
				re.ResourceId
		) AS v
	
		 UNION ALL
	
		----IdentifierCode EValidation (Index 7) - Provider - ServLoc from PracticeName
		SELECT ''7'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, pnServLoc.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			8 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			re.StartTime7 AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND re.StartTime7 <> ''''
			AND re.StartTime7 IS NOT NULL
	
		UNION ALL
	
		----IdentifierCode EValidation - Provider  - ServLoc from Resources
		SELECT ''7'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, re.ResourceId) + ''10'' + ''_'' + CONVERT(nvarchar,pnBillOrg.PracticeId) + ''13'' + ''_'' + CONVERT(nvarchar, reServLoc.ResourceId) AS Id,
			NULL AS BillingOrganizationId,
			NULL AS ExternalProviderId,
			8 AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			re.StartTime7 AS Value
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' AND reServLoc.ServiceCode = ''02''
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
	 		AND re.StartTime7 <> ''''
			AND re.StartTime7 IS NOT NULL 
	
		UNION ALL
	
		----IdentifierCode EValidation - Provider w Override - ServLoc from PracticeName
		SELECT ''7'' + ''_'' + ''0'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,v.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, v.PracticeId) AS Id,
			v.BillingOrganizationId AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 2 + v.PracticeId) + (CONVERT(bigint, 110000000) * v.ResourceId + v.ResourceId)) AS ProviderId,
			NULL AS ServiceLocationId,
			v.Value AS Value
		FROM (SELECT 
				NULL AS BillingOrganizationId,
				NULL AS ExternalProviderId,
				8 AS IdentifierCodeTypeId,
				re.StartTime7 AS Value,
				pnServLoc.PracticeId AS PracticeId,
				re.ResourceId AS ResourceId,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources re
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND re.StartTime7 <> ''''
				AND re.StartTime7 IS NOT NULL 
			GROUP BY re.StartTime7,
				pnServLoc.PracticeId,
				re.ResourceId
			) AS v
		UNION ALL
	
		----IdentifierCode EValidation - Provider w Override - ServLoc from Resources
		SELECT ''7'' + ''_'' + ''1'' + ''_'' + ''7'' + ''_'' + CONVERT(nvarchar, v.ResourceId) + ''11'' + ''_'' + CONVERT(nvarchar,v.ResourceId) + ''13'' + ''_'' + CONVERT(nvarchar, v.ReServLocResourceId) AS Id,
			v.BillingOrganizationId AS BillingOrganizationId,
			v.ExternalProviderId AS ExternalProviderId,
			v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
			(CONVERT(bigint, 110000000) * (110000000 * 3 + v.ReServLocResourceId) + (CONVERT(bigint, 110000000) * v.ResourceId + v.ResourceId)) AS ProviderId,	
			NULL AS ServiceLocationId,
			v.Value AS Value
		FROM (SELECT
				NULL AS BillingOrganizationId,
				NULL AS ExternalProviderId,
				8 AS IdentifierCodeTypeId,
				re.StartTime7 AS Value,
				reServLoc.ResourceId AS ReServLocResourceId,
				re.ResourceId AS ResourceId,
				COUNT_BIG(*) AS Count
			FROM dbo.Resources re
			INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
				AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
			INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' AND reServLoc.ServiceCode = ''02''
			WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				AND re.StartTime7 <> ''''
				AND re.StartTime7 IS NOT NULL 
			GROUP BY re.StartTime7,
				reServLoc.ResourceId,
				re.ResourceId		
			) AS v'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.IdentifierCodes'
			,@IdentifierCodesViewSql
			,@isChanged = @isIdentifierCodesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnIdentifierCodeDelete'
				,
				'
			CREATE TRIGGER model.OnIdentifierCodeDelete ON [model].[IdentifierCodes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id nvarchar(max)
			DECLARE @Value nvarchar(max)
			DECLARE @IdentifierCodeTypeId int
			DECLARE @BillingOrganizationId int
			DECLARE @ServiceLocationId int
			DECLARE @ProviderId bigint
			DECLARE @ExternalProviderId int
		
			DECLARE IdentifierCodesDeletedCursor CURSOR FOR
			SELECT Id, Value, IdentifierCodeTypeId, BillingOrganizationId, ServiceLocationId, ProviderId, ExternalProviderId FROM deleted
		
			OPEN IdentifierCodesDeletedCursor
			FETCH NEXT FROM IdentifierCodesDeletedCursor INTO @Id, @Value, @IdentifierCodeTypeId, @BillingOrganizationId, @ServiceLocationId, @ProviderId, @ExternalProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- IdentifierCode Delete
				DELETE
				FROM dbo.PracticeVendors
				WHERE VendorId = @Id
			
				SELECT @Id AS Id, @Value AS Value, @IdentifierCodeTypeId AS IdentifierCodeTypeId, @BillingOrganizationId AS BillingOrganizationId, @ServiceLocationId AS ServiceLocationId, @ProviderId AS ProviderId, @ExternalProviderId AS ExternalProviderId
			
				FETCH NEXT FROM IdentifierCodesDeletedCursor INTO @Id, @Value, @IdentifierCodeTypeId, @BillingOrganizationId, @ServiceLocationId, @ProviderId, @ExternalProviderId 
		
			END
		
			CLOSE IdentifierCodesDeletedCursor
			DEALLOCATE IdentifierCodesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnIdentifierCodeInsert'
				,
				'
			CREATE TRIGGER model.OnIdentifierCodeInsert ON [model].[IdentifierCodes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id nvarchar(max)
			DECLARE @Value nvarchar(max)
			DECLARE @IdentifierCodeTypeId int
			DECLARE @BillingOrganizationId int
			DECLARE @ServiceLocationId int
			DECLARE @ProviderId bigint
			DECLARE @ExternalProviderId int
		
			DECLARE IdentifierCodesInsertedCursor CURSOR FOR
			SELECT Id, Value, IdentifierCodeTypeId, BillingOrganizationId, ServiceLocationId, ProviderId, ExternalProviderId FROM inserted
		
			OPEN IdentifierCodesInsertedCursor
			FETCH NEXT FROM IdentifierCodesInsertedCursor INTO @Id, @Value, @IdentifierCodeTypeId, @BillingOrganizationId, @ServiceLocationId, @ProviderId, @ExternalProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- IdentifierCode Insert
				IF NOT EXISTS(SELECT TOP 1 * FROM dbo.PracticeVendors WHERE VendorId=@ExternalProviderId)
				BEGIN
					INSERT INTO dbo.PracticeVendors (vendorname)
					VALUES ('''')
			
					SET @Id = IDENT_CURRENT(''PracticeVendors'')
				END
				ELSE
				BEGIN
					SET @Id=@ExternalProviderId
				END
			
							
				-- IdentifierCode Update/Insert
				UPDATE dbo.PracticeVendors
				SET VendorNPI = @Value
					,VendorType = ''D''
				WHERE VendorId = @Id
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @Value AS Value, @IdentifierCodeTypeId AS IdentifierCodeTypeId, @BillingOrganizationId AS BillingOrganizationId, @ServiceLocationId AS ServiceLocationId, @ProviderId AS ProviderId, @ExternalProviderId AS ExternalProviderId
				
				FETCH NEXT FROM IdentifierCodesInsertedCursor INTO @Id, @Value, @IdentifierCodeTypeId, @BillingOrganizationId, @ServiceLocationId, @ProviderId, @ExternalProviderId 
		
			END
		
			CLOSE IdentifierCodesInsertedCursor
			DEALLOCATE IdentifierCodesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnIdentifierCodeUpdate'
				,
				'
			CREATE TRIGGER model.OnIdentifierCodeUpdate ON [model].[IdentifierCodes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id nvarchar(max)
			DECLARE @Value nvarchar(max)
			DECLARE @IdentifierCodeTypeId int
			DECLARE @BillingOrganizationId int
			DECLARE @ServiceLocationId int
			DECLARE @ProviderId bigint
			DECLARE @ExternalProviderId int
		
			DECLARE IdentifierCodesUpdatedCursor CURSOR FOR
			SELECT Id, Value, IdentifierCodeTypeId, BillingOrganizationId, ServiceLocationId, ProviderId, ExternalProviderId FROM inserted
		
			OPEN IdentifierCodesUpdatedCursor
			FETCH NEXT FROM IdentifierCodesUpdatedCursor INTO @Id, @Value, @IdentifierCodeTypeId, @BillingOrganizationId, @ServiceLocationId, @ProviderId, @ExternalProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- IdentifierCode Update/Insert
				UPDATE dbo.PracticeVendors
				SET VendorNPI = @Value
					,VendorType = ''D''
				WHERE VendorId = @Id
				
				FETCH NEXT FROM IdentifierCodesUpdatedCursor INTO @Id, @Value, @IdentifierCodeTypeId, @BillingOrganizationId, @ServiceLocationId, @ProviderId, @ExternalProviderId 
		
			END
		
			CLOSE IdentifierCodesUpdatedCursor
			DEALLOCATE IdentifierCodesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.InsurancePolicies')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isInsurancePoliciesChanged BIT

		SET @isInsurancePoliciesChanged = 0

		DECLARE @InsurancePoliciesViewSql NVARCHAR(max)

		SET @InsurancePoliciesViewSql = 
			'
		CREATE VIEW [model].[InsurancePolicies_Internal]
		WITH SCHEMABINDING
		AS
		SELECT pf.FinancialId AS Id,
			CONVERT(decimal(18,2), FinancialCopay) AS Copay,
			CONVERT(decimal(18,2), 0) AS Deductible,
			CASE WHEN FinancialEndDate = '''' THEN NULL ELSE CONVERT(datetime, FinancialEndDate, 112) END AS EndDateTime,
			FinancialGroupId AS GroupCode,
			FinancialInsurerId AS InsurerId,
			CONVERT(bit, 0) AS IsDeleted,
				CASE
				WHEN pd.MedicareSecondary = ''12'' AND pins.InsurerReferenceCode IN (''M'', ''N'', ''['')
					THEN 1
				WHEN pd.MedicareSecondary = ''13'' AND pins.InsurerReferenceCode IN (''M'', ''N'', ''['')
					THEN 2
				WHEN pd.MedicareSecondary = ''14'' AND pins.InsurerReferenceCode IN (''M'', ''N'', ''['')
					THEN 3
				WHEN pd.MedicareSecondary = ''15'' AND pins.InsurerReferenceCode IN (''M'', ''N'', ''['')
					THEN 4
				WHEN pd.MedicareSecondary = ''16'' AND pins.InsurerReferenceCode IN (''M'', ''N'', ''['')
					THEN 5
				WHEN pd.MedicareSecondary = ''41'' AND pins.InsurerReferenceCode IN (''M'', ''N'', ''['')
					THEN 6
				WHEN pd.MedicareSecondary = ''42'' AND pins.InsurerReferenceCode IN (''M'', ''N'', ''['')
					THEN 7
				WHEN pd.MedicareSecondary = ''43'' AND pins.InsurerReferenceCode IN (''M'', ''N'', ''['')
					THEN 8
				WHEN pd.MedicareSecondary = ''47'' AND pins.InsurerReferenceCode IN (''M'', ''N'', ''['')
					THEN 9
				ELSE NULL 
				END AS MedicareSecondaryReasonCodeId,
			FinancialPerson AS PolicyCode,
			pf.PatientId AS PolicyHolderPatientId,
			CONVERT(datetime,FinancialStartDate, 112) AS StartDateTime
		FROM dbo.PatientFinancial pf
		INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pf.PatientId
		INNER JOIN dbo.PracticeInsurers pins on pins.InsurerId = pf.FinancialInsurerId'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InsurancePolicies_Internal'
			,@InsurancePoliciesViewSql
			,@isChanged = @isInsurancePoliciesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @InsurancePoliciesViewSql = '	
			CREATE VIEW [model].[InsurancePolicies]
			WITH SCHEMABINDING
			AS
			SELECT Id, PolicyCode, GroupCode, Copay, Deductible, StartDateTime, EndDateTime, IsDeleted, PolicyHolderPatientId, InsurerId, MedicareSecondaryReasonCodeId FROM [model].[InsurancePolicies_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_InsurancePolicies'
							AND object_id = OBJECT_ID('[model].[InsurancePolicies_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[InsurancePolicies_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_InsurancePolicies] ON [model].[InsurancePolicies_Internal] ([Id]);
				END

				SET @InsurancePoliciesViewSql = @InsurancePoliciesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InsurancePolicies'
				,@InsurancePoliciesViewSql
				,@isChanged = @isInsurancePoliciesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurancePolicyDelete'
				,
				'
			CREATE TRIGGER model.OnInsurancePolicyDelete ON [model].[InsurancePolicies] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PolicyCode nvarchar(max)
			DECLARE @GroupCode nvarchar(max)
			DECLARE @Copay decimal(18,0)
			DECLARE @Deductible decimal(18,0)
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @IsDeleted bit
			DECLARE @PolicyHolderPatientId int
			DECLARE @InsurerId int
			DECLARE @MedicareSecondaryReasonCodeId int
		
			DECLARE InsurancePoliciesDeletedCursor CURSOR FOR
			SELECT Id, PolicyCode, GroupCode, Copay, Deductible, StartDateTime, EndDateTime, IsDeleted, PolicyHolderPatientId, InsurerId, MedicareSecondaryReasonCodeId FROM deleted
		
			OPEN InsurancePoliciesDeletedCursor
			FETCH NEXT FROM InsurancePoliciesDeletedCursor INTO @Id, @PolicyCode, @GroupCode, @Copay, @Deductible, @StartDateTime, @EndDateTime, @IsDeleted, @PolicyHolderPatientId, @InsurerId, @MedicareSecondaryReasonCodeId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- InsurancePolicy Delete
				DECLARE @financialId int 
				SET @financialId = @Id
			
				DELETE
				FROM PatientFinancial
				WHERE FinancialId = @financialId
			
				SELECT @Id AS Id, @PolicyCode AS PolicyCode, @GroupCode AS GroupCode, @Copay AS Copay, @Deductible AS Deductible, @StartDateTime AS StartDateTime, @EndDateTime AS EndDateTime, @IsDeleted AS IsDeleted, @PolicyHolderPatientId AS PolicyHolderPatientId, @InsurerId AS InsurerId, @MedicareSecondaryReasonCodeId AS MedicareSecondaryReasonCodeId
			
				FETCH NEXT FROM InsurancePoliciesDeletedCursor INTO @Id, @PolicyCode, @GroupCode, @Copay, @Deductible, @StartDateTime, @EndDateTime, @IsDeleted, @PolicyHolderPatientId, @InsurerId, @MedicareSecondaryReasonCodeId 
		
			END
		
			CLOSE InsurancePoliciesDeletedCursor
			DEALLOCATE InsurancePoliciesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurancePolicyInsert'
				,
				'
			CREATE TRIGGER model.OnInsurancePolicyInsert ON [model].[InsurancePolicies] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PolicyCode nvarchar(max)
			DECLARE @GroupCode nvarchar(max)
			DECLARE @Copay decimal(18,0)
			DECLARE @Deductible decimal(18,0)
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @IsDeleted bit
			DECLARE @PolicyHolderPatientId int
			DECLARE @InsurerId int
			DECLARE @MedicareSecondaryReasonCodeId int
		
			DECLARE InsurancePoliciesInsertedCursor CURSOR FOR
			SELECT Id, PolicyCode, GroupCode, Copay, Deductible, StartDateTime, EndDateTime, IsDeleted, PolicyHolderPatientId, InsurerId, MedicareSecondaryReasonCodeId FROM inserted
		
			OPEN InsurancePoliciesInsertedCursor
			FETCH NEXT FROM InsurancePoliciesInsertedCursor INTO @Id, @PolicyCode, @GroupCode, @Copay, @Deductible, @StartDateTime, @EndDateTime, @IsDeleted, @PolicyHolderPatientId, @InsurerId, @MedicareSecondaryReasonCodeId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- InsurancePolicy Insert
				INSERT INTO PatientFinancial (FinancialPerson)
				VALUES ('''')
			
				SET @Id = IDENT_CURRENT(''PatientFinancial'')
			
							
				-- InsurancePolicy Update/Insert
				DECLARE @financialId int 
				SET @financialId = @Id
				-- @Id is a composite key...so we need to decompose it
				DECLARE @policyHolderPatientDemographicsId int
			
				SELECT @policyHolderPatientDemographicsId = y
				FROM model.GetIdPair(@PolicyHolderPatientId, 202000000)
			
				UPDATE PatientFinancial
				-- This will get called for Updates AND after the Insert procedure above (an "upsert")
				SET FinancialCopay = @Copay,
					FinancialPerson = @PolicyCode,
					PatientId = @policyHolderPatientDemographicsId,
					FinancialGroupId = @GroupCode,
					FinancialStartDate = CASE @StartDateTime
						WHEN NULL
							THEN ''''
						ELSE CONVERT(VARCHAR, @StartDateTime, 112)
						END,
					FinancialEndDate = CASE @EndDateTime
						WHEN NULL
							THEN ''''
						ELSE CONVERT(VARCHAR, @EndDateTime, 112)
						END,
					FinancialInsurerId = @InsurerId
				WHERE FinancialId = @financialId
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @PolicyCode AS PolicyCode, @GroupCode AS GroupCode, @Copay AS Copay, @Deductible AS Deductible, @StartDateTime AS StartDateTime, @EndDateTime AS EndDateTime, @IsDeleted AS IsDeleted, @PolicyHolderPatientId AS PolicyHolderPatientId, @InsurerId AS InsurerId, @MedicareSecondaryReasonCodeId AS MedicareSecondaryReasonCodeId
				
				FETCH NEXT FROM InsurancePoliciesInsertedCursor INTO @Id, @PolicyCode, @GroupCode, @Copay, @Deductible, @StartDateTime, @EndDateTime, @IsDeleted, @PolicyHolderPatientId, @InsurerId, @MedicareSecondaryReasonCodeId 
		
			END
		
			CLOSE InsurancePoliciesInsertedCursor
			DEALLOCATE InsurancePoliciesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurancePolicyUpdate'
				,
				'
			CREATE TRIGGER model.OnInsurancePolicyUpdate ON [model].[InsurancePolicies] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PolicyCode nvarchar(max)
			DECLARE @GroupCode nvarchar(max)
			DECLARE @Copay decimal(18,0)
			DECLARE @Deductible decimal(18,0)
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @IsDeleted bit
			DECLARE @PolicyHolderPatientId int
			DECLARE @InsurerId int
			DECLARE @MedicareSecondaryReasonCodeId int
		
			DECLARE InsurancePoliciesUpdatedCursor CURSOR FOR
			SELECT Id, PolicyCode, GroupCode, Copay, Deductible, StartDateTime, EndDateTime, IsDeleted, PolicyHolderPatientId, InsurerId, MedicareSecondaryReasonCodeId FROM inserted
		
			OPEN InsurancePoliciesUpdatedCursor
			FETCH NEXT FROM InsurancePoliciesUpdatedCursor INTO @Id, @PolicyCode, @GroupCode, @Copay, @Deductible, @StartDateTime, @EndDateTime, @IsDeleted, @PolicyHolderPatientId, @InsurerId, @MedicareSecondaryReasonCodeId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- InsurancePolicy Update/Insert
				DECLARE @financialId int 
				SET @financialId = @Id
				-- @Id is a composite key...so we need to decompose it
				DECLARE @policyHolderPatientDemographicsId int
			
				SELECT @policyHolderPatientDemographicsId = y
				FROM model.GetIdPair(@PolicyHolderPatientId, 202000000)
			
				UPDATE PatientFinancial
				-- This will get called for Updates AND after the Insert procedure above (an "upsert")
				SET FinancialCopay = @Copay,
					FinancialPerson = @PolicyCode,
					PatientId = @policyHolderPatientDemographicsId,
					FinancialGroupId = @GroupCode,
					FinancialStartDate = CASE @StartDateTime
						WHEN NULL
							THEN ''''
						ELSE CONVERT(VARCHAR, @StartDateTime, 112)
						END,
					FinancialEndDate = CASE @EndDateTime
						WHEN NULL
							THEN ''''
						ELSE CONVERT(VARCHAR, @EndDateTime, 112)
						END,
					FinancialInsurerId = @InsurerId
				WHERE FinancialId = @financialId
				
				FETCH NEXT FROM InsurancePoliciesUpdatedCursor INTO @Id, @PolicyCode, @GroupCode, @Copay, @Deductible, @StartDateTime, @EndDateTime, @IsDeleted, @PolicyHolderPatientId, @InsurerId, @MedicareSecondaryReasonCodeId 
		
			END
		
			CLOSE InsurancePoliciesUpdatedCursor
			DEALLOCATE InsurancePoliciesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.InsurerBusinessClasses')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isInsurerBusinessClassesChanged BIT

		SET @isInsurerBusinessClassesChanged = 0

		DECLARE @InsurerBusinessClassesViewSql NVARCHAR(max)

		SET @InsurerBusinessClassesViewSql = '
		CREATE VIEW [model].[InsurerBusinessClasses_Internal]
		WITH SCHEMABINDING
		AS
		SELECT CASE Code
				WHEN ''AMERIH''
					THEN 1
				WHEN ''BLUES''
					THEN 2
				WHEN ''COMM''
					THEN 3
				WHEN ''MCAIDFL''
					THEN 4
				WHEN ''MCAIDNC''
					THEN 5
				WHEN ''MCAIDNJ''
					THEN 6
				WHEN ''MCAIDNV''
					THEN 7
				WHEN ''MCAIDNY''
					THEN 8
				WHEN ''MCARENJ''
					THEN 9
				WHEN ''MCARENV''
					THEN 10
				WHEN ''MCARENY''
					THEN 11
				WHEN ''TEMPLATE''
					THEN 12
				ELSE Id + 1000
			END AS Id,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''BUSINESSCLASS'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InsurerBusinessClasses_Internal'
			,@InsurerBusinessClassesViewSql
			,@isChanged = @isInsurerBusinessClassesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @InsurerBusinessClassesViewSql = '	
			CREATE VIEW [model].[InsurerBusinessClasses]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, IsArchived FROM [model].[InsurerBusinessClasses_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_InsurerBusinessClasses'
							AND object_id = OBJECT_ID('[model].[InsurerBusinessClasses_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[InsurerBusinessClasses_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_InsurerBusinessClasses] ON [model].[InsurerBusinessClasses_Internal] ([Id]);
				END

				SET @InsurerBusinessClassesViewSql = @InsurerBusinessClassesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InsurerBusinessClasses'
				,@InsurerBusinessClassesViewSql
				,@isChanged = @isInsurerBusinessClassesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerBusinessClassDelete'
				,'
			CREATE TRIGGER model.OnInsurerBusinessClassDelete ON [model].[InsurerBusinessClasses] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE InsurerBusinessClassesDeletedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM deleted
		
			OPEN InsurerBusinessClassesDeletedCursor
			FETCH NEXT FROM InsurerBusinessClassesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for InsurerBusinessClass --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
			
				FETCH NEXT FROM InsurerBusinessClassesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE InsurerBusinessClassesDeletedCursor
			DEALLOCATE InsurerBusinessClassesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerBusinessClassInsert'
				,'
			CREATE TRIGGER model.OnInsurerBusinessClassInsert ON [model].[InsurerBusinessClasses] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE InsurerBusinessClassesInsertedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN InsurerBusinessClassesInsertedCursor
			FETCH NEXT FROM InsurerBusinessClassesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for InsurerBusinessClass --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
				
				FETCH NEXT FROM InsurerBusinessClassesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE InsurerBusinessClassesInsertedCursor
			DEALLOCATE InsurerBusinessClassesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerBusinessClassUpdate'
				,'
			CREATE TRIGGER model.OnInsurerBusinessClassUpdate ON [model].[InsurerBusinessClasses] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE InsurerBusinessClassesUpdatedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN InsurerBusinessClassesUpdatedCursor
			FETCH NEXT FROM InsurerBusinessClassesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for InsurerBusinessClass --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM InsurerBusinessClassesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE InsurerBusinessClassesUpdatedCursor
			DEALLOCATE InsurerBusinessClassesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.InsurerFeeScheduleContracts')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isInsurerFeeScheduleContractsChanged BIT

		SET @isInsurerFeeScheduleContractsChanged = 0

		DECLARE @InsurerFeeScheduleContractsViewSql NVARCHAR(max)

		SET @InsurerFeeScheduleContractsViewSql = '
		CREATE VIEW [model].[InsurerFeeScheduleContracts]
		WITH SCHEMABINDING
		AS
		SELECT MIN(pfs.ServiceId) AS Id,
			CASE 
				WHEN EndDate = ''''
					THEN NULL
				ELSE CONVERT(datetime, EndDate, 112)
				END AS EndDateTime,
			mfs.Id AS FeeScheduleContractId,
			PlanId AS InsurerId,
			CONVERT(datetime, StartDate, 112) AS StartDateTime
		FROM dbo.PracticeInsurers pri
		INNER JOIN dbo.PracticeFeeSchedules pfs ON pri.InsurerId = pfs.PlanId
		INNER JOIN model.GetMasterFeeSchedules() mfs ON mfs.NAME = pri.InsurerName + SUBSTRING(startdate, 1, 6) + SUBSTRING(enddate, 3, 2)
		GROUP BY EndDate,
			InsurerId,
			StartDate,
			InsurerName,
			Id,
			PlanId'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InsurerFeeScheduleContracts'
			,@InsurerFeeScheduleContractsViewSql
			,@isChanged = @isInsurerFeeScheduleContractsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerFeeScheduleContractDelete'
				,
				'
			CREATE TRIGGER model.OnInsurerFeeScheduleContractDelete ON [model].[InsurerFeeScheduleContracts] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @InsurerId int
			DECLARE @FeeScheduleContractId int
		
			DECLARE InsurerFeeScheduleContractsDeletedCursor CURSOR FOR
			SELECT Id, StartDateTime, EndDateTime, InsurerId, FeeScheduleContractId FROM deleted
		
			OPEN InsurerFeeScheduleContractsDeletedCursor
			FETCH NEXT FROM InsurerFeeScheduleContractsDeletedCursor INTO @Id, @StartDateTime, @EndDateTime, @InsurerId, @FeeScheduleContractId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for InsurerFeeScheduleContract --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @StartDateTime AS StartDateTime, @EndDateTime AS EndDateTime, @InsurerId AS InsurerId, @FeeScheduleContractId AS FeeScheduleContractId
			
				FETCH NEXT FROM InsurerFeeScheduleContractsDeletedCursor INTO @Id, @StartDateTime, @EndDateTime, @InsurerId, @FeeScheduleContractId 
		
			END
		
			CLOSE InsurerFeeScheduleContractsDeletedCursor
			DEALLOCATE InsurerFeeScheduleContractsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerFeeScheduleContractInsert'
				,
				'
			CREATE TRIGGER model.OnInsurerFeeScheduleContractInsert ON [model].[InsurerFeeScheduleContracts] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @InsurerId int
			DECLARE @FeeScheduleContractId int
		
			DECLARE InsurerFeeScheduleContractsInsertedCursor CURSOR FOR
			SELECT Id, StartDateTime, EndDateTime, InsurerId, FeeScheduleContractId FROM inserted
		
			OPEN InsurerFeeScheduleContractsInsertedCursor
			FETCH NEXT FROM InsurerFeeScheduleContractsInsertedCursor INTO @Id, @StartDateTime, @EndDateTime, @InsurerId, @FeeScheduleContractId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for InsurerFeeScheduleContract --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @StartDateTime AS StartDateTime, @EndDateTime AS EndDateTime, @InsurerId AS InsurerId, @FeeScheduleContractId AS FeeScheduleContractId
				
				FETCH NEXT FROM InsurerFeeScheduleContractsInsertedCursor INTO @Id, @StartDateTime, @EndDateTime, @InsurerId, @FeeScheduleContractId 
		
			END
		
			CLOSE InsurerFeeScheduleContractsInsertedCursor
			DEALLOCATE InsurerFeeScheduleContractsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerFeeScheduleContractUpdate'
				,
				'
			CREATE TRIGGER model.OnInsurerFeeScheduleContractUpdate ON [model].[InsurerFeeScheduleContracts] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @InsurerId int
			DECLARE @FeeScheduleContractId int
		
			DECLARE InsurerFeeScheduleContractsUpdatedCursor CURSOR FOR
			SELECT Id, StartDateTime, EndDateTime, InsurerId, FeeScheduleContractId FROM inserted
		
			OPEN InsurerFeeScheduleContractsUpdatedCursor
			FETCH NEXT FROM InsurerFeeScheduleContractsUpdatedCursor INTO @Id, @StartDateTime, @EndDateTime, @InsurerId, @FeeScheduleContractId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for InsurerFeeScheduleContract --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM InsurerFeeScheduleContractsUpdatedCursor INTO @Id, @StartDateTime, @EndDateTime, @InsurerId, @FeeScheduleContractId 
		
			END
		
			CLOSE InsurerFeeScheduleContractsUpdatedCursor
			DEALLOCATE InsurerFeeScheduleContractsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.InsurerIdentifiers')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isInsurerIdentifiersChanged BIT

		SET @isInsurerIdentifiersChanged = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InsurerIdentifiers'
			,'
		CREATE VIEW [model].[InsurerIdentifiers]
		WITH SCHEMABINDING
		AS
		SELECT CONVERT(int, NULL) AS Id, CONVERT(nvarchar(max), NULL) AS InsurerIdentifierCode, CONVERT(int, NULL) AS BillingOrganizationId, CONVERT(bigint, NULL) AS ProviderId
		-- View SQL not found for InsurerIdentifier --
		'
			,@isChanged = @isInsurerIdentifiersChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerIdentifierDelete'
				,
				'
			CREATE TRIGGER model.OnInsurerIdentifierDelete ON [model].[InsurerIdentifiers] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @InsurerIdentifierCode nvarchar(max)
			DECLARE @BillingOrganizationId int
			DECLARE @ProviderId bigint
		
			DECLARE InsurerIdentifiersDeletedCursor CURSOR FOR
			SELECT Id, InsurerIdentifierCode, BillingOrganizationId, ProviderId FROM deleted
		
			OPEN InsurerIdentifiersDeletedCursor
			FETCH NEXT FROM InsurerIdentifiersDeletedCursor INTO @Id, @InsurerIdentifierCode, @BillingOrganizationId, @ProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for InsurerIdentifier --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @InsurerIdentifierCode AS InsurerIdentifierCode, @BillingOrganizationId AS BillingOrganizationId, @ProviderId AS ProviderId
			
				FETCH NEXT FROM InsurerIdentifiersDeletedCursor INTO @Id, @InsurerIdentifierCode, @BillingOrganizationId, @ProviderId 
		
			END
		
			CLOSE InsurerIdentifiersDeletedCursor
			DEALLOCATE InsurerIdentifiersDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerIdentifierInsert'
				,
				'
			CREATE TRIGGER model.OnInsurerIdentifierInsert ON [model].[InsurerIdentifiers] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @InsurerIdentifierCode nvarchar(max)
			DECLARE @BillingOrganizationId int
			DECLARE @ProviderId bigint
		
			DECLARE InsurerIdentifiersInsertedCursor CURSOR FOR
			SELECT Id, InsurerIdentifierCode, BillingOrganizationId, ProviderId FROM inserted
		
			OPEN InsurerIdentifiersInsertedCursor
			FETCH NEXT FROM InsurerIdentifiersInsertedCursor INTO @Id, @InsurerIdentifierCode, @BillingOrganizationId, @ProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for InsurerIdentifier --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @InsurerIdentifierCode AS InsurerIdentifierCode, @BillingOrganizationId AS BillingOrganizationId, @ProviderId AS ProviderId
				
				FETCH NEXT FROM InsurerIdentifiersInsertedCursor INTO @Id, @InsurerIdentifierCode, @BillingOrganizationId, @ProviderId 
		
			END
		
			CLOSE InsurerIdentifiersInsertedCursor
			DEALLOCATE InsurerIdentifiersInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerIdentifierUpdate'
				,'
			CREATE TRIGGER model.OnInsurerIdentifierUpdate ON [model].[InsurerIdentifiers] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @InsurerIdentifierCode nvarchar(max)
			DECLARE @BillingOrganizationId int
			DECLARE @ProviderId bigint
		
			DECLARE InsurerIdentifiersUpdatedCursor CURSOR FOR
			SELECT Id, InsurerIdentifierCode, BillingOrganizationId, ProviderId FROM inserted
		
			OPEN InsurerIdentifiersUpdatedCursor
			FETCH NEXT FROM InsurerIdentifiersUpdatedCursor INTO @Id, @InsurerIdentifierCode, @BillingOrganizationId, @ProviderId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for InsurerIdentifier --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM InsurerIdentifiersUpdatedCursor INTO @Id, @InsurerIdentifierCode, @BillingOrganizationId, @ProviderId 
		
			END
		
			CLOSE InsurerIdentifiersUpdatedCursor
			DEALLOCATE InsurerIdentifiersUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.InsurerPlanTypes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isInsurerPlanTypesChanged BIT

		SET @isInsurerPlanTypesChanged = 0

		DECLARE @InsurerPlanTypesViewSql NVARCHAR(max)

		SET @InsurerPlanTypesViewSql = '
		CREATE VIEW [model].[InsurerPlanTypes_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id AS Id,
			Code AS [Name]
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''PLANTYPE'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InsurerPlanTypes_Internal'
			,@InsurerPlanTypesViewSql
			,@isChanged = @isInsurerPlanTypesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @InsurerPlanTypesViewSql = '	
			CREATE VIEW [model].[InsurerPlanTypes]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name FROM [model].[InsurerPlanTypes_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_InsurerPlanTypes'
							AND object_id = OBJECT_ID('[model].[InsurerPlanTypes_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[InsurerPlanTypes_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_InsurerPlanTypes] ON [model].[InsurerPlanTypes_Internal] ([Id]);
				END

				SET @InsurerPlanTypesViewSql = @InsurerPlanTypesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InsurerPlanTypes'
				,@InsurerPlanTypesViewSql
				,@isChanged = @isInsurerPlanTypesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerPlanTypeDelete'
				,'
			CREATE TRIGGER model.OnInsurerPlanTypeDelete ON [model].[InsurerPlanTypes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
		
			DECLARE InsurerPlanTypesDeletedCursor CURSOR FOR
			SELECT Id, Name FROM deleted
		
			OPEN InsurerPlanTypesDeletedCursor
			FETCH NEXT FROM InsurerPlanTypesDeletedCursor INTO @Id, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- InsurerPlanType Delete
				DELETE
				FROM dbo.PracticeCodeTable
				WHERE Id = @Id
			
				SELECT @Id AS Id, @Name AS Name
			
				FETCH NEXT FROM InsurerPlanTypesDeletedCursor INTO @Id, @Name 
		
			END
		
			CLOSE InsurerPlanTypesDeletedCursor
			DEALLOCATE InsurerPlanTypesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerPlanTypeInsert'
				,
				'
			CREATE TRIGGER model.OnInsurerPlanTypeInsert ON [model].[InsurerPlanTypes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
		
			DECLARE InsurerPlanTypesInsertedCursor CURSOR FOR
			SELECT Id, Name FROM inserted
		
			OPEN InsurerPlanTypesInsertedCursor
			FETCH NEXT FROM InsurerPlanTypesInsertedCursor INTO @Id, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- InsurerPlanType Insert
				DECLARE @checkExists int
			
				SELECT @checkExists = COUNT(*)
				FROM dbo.PracticeCodeTable
				WHERE ReferenceType = ''PLANTYPE''
					AND Code = @Name
			
				IF @checkExists = 0
				BEGIN
					INSERT INTO dbo.PracticeCodeTable (ReferenceType)
					VALUES (''PLANTYPE'')
			
					SET @Id = IDENT_CURRENT(''PracticeCodeTable'')
				END
				ELSE
				BEGIN
					SELECT @Id = Id
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLANTYPE''
						AND Code = @Name
				END
			
							
				-- InsurerPlanType Update/Insert
				UPDATE dbo.PracticeCodeTable
				SET Code = @Name
				WHERE ReferenceType = ''PLANTYPE''
					AND Id = @Id
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @Name AS Name
				
				FETCH NEXT FROM InsurerPlanTypesInsertedCursor INTO @Id, @Name 
		
			END
		
			CLOSE InsurerPlanTypesInsertedCursor
			DEALLOCATE InsurerPlanTypesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerPlanTypeUpdate'
				,'
			CREATE TRIGGER model.OnInsurerPlanTypeUpdate ON [model].[InsurerPlanTypes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
		
			DECLARE InsurerPlanTypesUpdatedCursor CURSOR FOR
			SELECT Id, Name FROM inserted
		
			OPEN InsurerPlanTypesUpdatedCursor
			FETCH NEXT FROM InsurerPlanTypesUpdatedCursor INTO @Id, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- InsurerPlanType Update/Insert
				UPDATE dbo.PracticeCodeTable
				SET Code = @Name
				WHERE ReferenceType = ''PLANTYPE''
					AND Id = @Id
				
				FETCH NEXT FROM InsurerPlanTypesUpdatedCursor INTO @Id, @Name 
		
			END
		
			CLOSE InsurerPlanTypesUpdatedCursor
			DEALLOCATE InsurerPlanTypesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.InvoiceReceivables')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isInvoiceReceivablesChanged BIT

		SET @isInvoiceReceivablesChanged = 0

		DECLARE @ismodelInvoiceReceivables_Partition1Changed BIT

		SET @ismodelInvoiceReceivables_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InvoiceReceivables_Partition1'
			,
			'
		CREATE VIEW model.InvoiceReceivables_Partition1
		WITH SCHEMABINDING
		AS
		SELECT (CONVERT(bigint, 110000000) * prBilled.ComputedFinancialId + ap.AppointmentId) AS CompositeId
				---ACTIVE BILLING TRANSACTION; NO DATE VALIDATION
				,prBilled.AppointmentId AS InvoiceId
				,CASE pfBilled.FinancialInsType
					WHEN ''V''
						THEN 200
					WHEN ''A''
						THEN 3000			
					WHEN ''W''
						THEN 40000
					ELSE 10
					END + CASE 
					WHEN pfBilled.PatientId = pd.PolicyPatientId
						THEN 0
					ELSE 3
					END + CONVERT(INT, pfBilled.FinancialPIndicator) AS OrdinalId
				,(CONVERT(bigint, 110000000) * prBilled.PatientId + pfBilled.FinancialId) AS PatientInsuranceId
				,prBilled.ExternalRefInfo AS ExternalRefInfo
				,ap.ReferralId AS ReferralId
				,ap.PreCertId AS PreCertId
				,pfBilled.FinancialInsurerId AS FinancialInsurerId
				,COUNT_BIG(*) AS [Count]
			FROM dbo.PatientDemographics pd
			INNER JOIN dbo.Appointments ap ON pd.PatientId = ap.PatientId
			INNER JOIN dbo.PatientReceivables prBilled ON prBilled.AppointmentId = ap.AppointmentId
			INNER JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionTypeId = prBilled.ReceivableId
				AND ptj.TransactionType = ''R''
				AND ptj.TransactionRef = ''I''
			INNER JOIN dbo.ServiceTransactions st ON st.TransactionId = ptj.TransactionId
				AND (
					ptj.TransactionStatus IN (
						''P''
						,''S''
						)
					OR (
						ptj.TransactionStatus = ''Z''
						AND st.TransportAction <> ''0''
						)
					)
			INNER JOIN dbo.PatientFinancial pfBilled ON pfBilled.FinancialId = prBilled.ComputedFinancialId
			GROUP BY (CONVERT(bigint, 110000000) * prBilled.ComputedFinancialId + ap.AppointmentId)
				,prBilled.AppointmentId
				,CASE pfBilled.FinancialInsType
					WHEN ''V''
						THEN 200
					WHEN ''A''
						THEN 3000			
					WHEN ''W''
						THEN 40000
					ELSE 10
					END + CASE 
					WHEN pfBilled.PatientId = pd.PolicyPatientId
						THEN 0
					ELSE 3
					END + CONVERT(INT, pfBilled.FinancialPIndicator)
				,(CONVERT(bigint, 110000000) * prBilled.PatientId + pfBilled.FinancialId)
				,prBilled.ExternalRefInfo
				,ap.ReferralId
				,ap.PreCertId
				,pfBilled.FinancialInsurerId
		'
			,@isChanged = @ismodelInvoiceReceivables_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_InvoiceReceivables_Partition1'
					AND object_id = OBJECT_ID('model.InvoiceReceivables_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.InvoiceReceivables_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_InvoiceReceivables_Partition1 ON model.InvoiceReceivables_Partition1 (
				[CompositeId]
				,[InvoiceId]
				,[OrdinalId]
				,[PatientInsuranceId]
				,[ReferralId]
				,[PreCertId]
				,[FinancialInsurerId]
				);
		END

		DECLARE @ismodelInvoiceReceivables_Partition2Changed BIT

		SET @ismodelInvoiceReceivables_Partition2Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InvoiceReceivables_Partition2'
			,
			'
		CREATE VIEW model.InvoiceReceivables_Partition2
		WITH SCHEMABINDING
		AS
		SELECT (CONVERT(bigint, 110000000) * prPaid.ComputedFinancialId + ap.AppointmentId) AS CompositeId
				---PAYMENTS, perhaps no ServiceTransactions
				,prPaid.AppointmentId AS InvoiceId
				,CASE pfPaid.FinancialInsType
					WHEN ''V''
						THEN 200
					WHEN ''A''
						THEN 3000			
					WHEN ''W''
						THEN 40000
					ELSE 10
				END + CASE 
					WHEN pfPaid.PatientId = pd.PolicyPatientId
						THEN 0
					ELSE 3
				END + CONVERT(INT, pfPaid.FinancialPIndicator) AS OrdinalId
				,(CONVERT(bigint, 110000000) * prPaid.PatientId + pfPaid.FinancialId) AS PatientInsuranceId
				,prPaid.ExternalRefInfo AS ExternalRefInfo
				,ap.ReferralId AS ReferralId
				,ap.PreCertId AS PreCertId
				,pfPaid.FinancialInsurerId AS FinancialInsurerId
				,COUNT_BIG(*) AS [Count]	
			FROM dbo.PatientDemographics pd
			INNER JOIN dbo.Appointments ap ON pd.PatientId = ap.PatientId
			INNER JOIN dbo.PatientReceivables prPaid ON ap.AppointmentId = prPaid.AppointmentId
			INNER JOIN dbo.PatientFinancial pfPaid ON pfPaid.FinancialId = prPaid.ComputedFinancialId
			INNER JOIN dbo.PatientReceivablePayments prp on prp.ReceivableId = prPaid.ReceivableId
			GROUP BY (CONVERT(bigint, 110000000) * prPaid.ComputedFinancialId + ap.AppointmentId) 
				,prPaid.AppointmentId 
				,CASE pfPaid.FinancialInsType
					WHEN ''V''
						THEN 200
					WHEN ''A''
						THEN 3000			
					WHEN ''W''
						THEN 40000
					ELSE 10
				END + CASE 
					WHEN pfPaid.PatientId = pd.PolicyPatientId
						THEN 0
					ELSE 3
				END + CONVERT(INT, pfPaid.FinancialPIndicator)
				,(CONVERT(bigint, 110000000) * prPaid.PatientId + pfPaid.FinancialId) 
				,prPaid.ExternalRefInfo 
				,ap.ReferralId
				,ap.PreCertId
				,pfPaid.FinancialInsurerId
		'
			,@isChanged = @ismodelInvoiceReceivables_Partition2Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_InvoiceReceivables_Partition2'
					AND object_id = OBJECT_ID('model.InvoiceReceivables_Partition2')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.InvoiceReceivables_Partition2'

			CREATE UNIQUE CLUSTERED INDEX PK_InvoiceReceivables_Partition2 ON model.InvoiceReceivables_Partition2 (
				[CompositeId]
				,[InvoiceId]
				,[OrdinalId]
				,[PatientInsuranceId]
				,[ReferralId]
				,[PreCertId]
				,[FinancialInsurerId]
				);
		END

		DECLARE @ismodelInvoiceReceivables_Partition3Changed BIT

		SET @ismodelInvoiceReceivables_Partition3Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InvoiceReceivables_Partition3'
			,
			'
		CREATE VIEW model.InvoiceReceivables_Partition3
		WITH SCHEMABINDING
		AS
		SELECT (CONVERT(bigint, 110000000) * pf.FinancialId + ap.AppointmentId) AS CompositeId
					,prBilled.AppointmentId AS InvoiceId
					,prBilled.ExternalRefInfo AS ExternalRefInfo
					,prBilled.ComputedFinancialId AS BilledFinancialId
					,pd.PatientId AS PatientId
					,(CONVERT(bigint, 110000000) * ap.PatientId + pf.FinancialId) AS PatientInsuranceId
					,CASE pf.FinancialInsType
						WHEN ''V''
							THEN 200					
						WHEN ''A''
							THEN 3000				
						WHEN ''W''
							THEN 40000
						ELSE 10
						END + CASE 
						WHEN pf.PatientId = pd.PolicyPatientId
							THEN 0
						ELSE 3
						END + CONVERT(INT, pf.FinancialPIndicator) AS OrdinalId
					,FinancialInsType AS FinancialInsType
					,ap.ReferralId AS ReferralId
					,ap.PreCertId AS PreCertId
					,pf.FinancialInsurerId AS FinancialInsurerId
					,COUNT_BIG(*) AS [Count]
				FROM dbo.PatientDemographics pd
				INNER JOIN dbo.Appointments ap ON pd.PatientId = ap.PatientId
				--Find the active insurer type
				INNER JOIN dbo.PatientReceivables prBilled ON ap.AppointmentId = prBilled.AppointmentId
				INNER JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionTypeId = prBilled.ReceivableId
					AND ptj.TransactionType = ''R''
				INNER JOIN dbo.ServiceTransactions st ON st.TransactionId = ptj.TransactionId
					AND (
						ptj.TransactionStatus IN (
							''P''
							,''S''
							)
						OR (
							ptj.TransactionStatus = ''Z''
							AND st.TransportAction <> ''0''
							)
						)
				--Find other active insurance policies with the same insurance type
				INNER JOIN dbo.PatientFinancial pf ON (
						pf.PatientId = pd.PolicyPatientId
						OR pf.PatientId = pd.SecondPolicyPatientId
						)
					AND pf.PatientId <> 0
					AND ap.AppDate >= pf.FinancialStartDate
					AND (
						ap.AppDate <= pf.FinancialEndDate
						OR pf.FinancialEndDate = ''''
						OR pf.FinancialEndDate IS NULL
						)
				INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
				WHERE prBilled.Insurerid <> 99999
				AND NOT (
				(
					pd.PatientId <> pd.PolicyPatientId
					AND pd.PolicyPatientId <> 0
					)
				OR (
					pd.PatientId <> pd.SecondPolicyPatientId
					AND pd.SecondPolicyPatientId <> 0
					)
				)
				GROUP BY (CONVERT(bigint, 110000000) * pf.FinancialId + ap.AppointmentId)
					,prBilled.AppointmentId
					,prBilled.ExternalRefInfo
					,prBilled.ComputedFinancialId
					,pd.PatientId
					,(CONVERT(bigint, 110000000) * ap.PatientId + pf.FinancialId)
					,CASE pf.FinancialInsType
						WHEN ''V''
							THEN 200
						WHEN ''A''
							THEN 3000				
						WHEN ''W''
							THEN 40000
						ELSE 10
						END + CASE 
						WHEN pf.PatientId = pd.PolicyPatientId
							THEN 0
						ELSE 3
						END + CONVERT(INT, pf.FinancialPIndicator)
					,FinancialInsType
					,ap.ReferralId
					,ap.PreCertId
					,pf.FinancialInsurerId
		'
			,@isChanged = @ismodelInvoiceReceivables_Partition3Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_InvoiceReceivables_Partition3'
					AND object_id = OBJECT_ID('model.InvoiceReceivables_Partition3')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.InvoiceReceivables_Partition3'

			CREATE UNIQUE CLUSTERED INDEX PK_InvoiceReceivables_Partition3 ON model.InvoiceReceivables_Partition3 (
				[CompositeId]
				,[InvoiceId]
				,[BilledFinancialId]
				,[PatientId]
				,[PatientInsuranceId]
				,[OrdinalId]
				,[ReferralId]
				,[PreCertId]
				,[FinancialInsurerId]
				);
		END

		DECLARE @ismodelInvoiceReceivables_Partition4Changed BIT

		SET @ismodelInvoiceReceivables_Partition4Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InvoiceReceivables_Partition4'
			,
			'
		CREATE VIEW model.InvoiceReceivables_Partition4
		WITH SCHEMABINDING
		AS
		SELECT (CONVERT(bigint, 110000000) * pf.FinancialId + ap.AppointmentId) AS CompositeId
					,prBilled.AppointmentId AS InvoiceId
					,prBilled.ExternalRefInfo AS ExternalRefInfo
					,prBilled.ComputedFinancialId AS BilledFinancialId
					,pd.PatientId AS PatientId
					,(CONVERT(bigint, 110000000) * ap.PatientId + pf.FinancialId) AS PatientInsuranceId
					,CASE pf.FinancialInsType
						WHEN ''V''
							THEN 200
						WHEN ''A''
							THEN 3000				
						WHEN ''W''
							THEN 40000
						ELSE 10
						END + CASE 
						WHEN pf.PatientId = pd.PolicyPatientId
							THEN 0
						ELSE 3
						END + CONVERT(INT, pf.FinancialPIndicator) AS OrdinalId
					,FinancialInsType AS FinancialInsType
					,ap.ReferralId AS ReferralId
					,ap.PreCertId AS PreCertId
					,pf.FinancialInsurerId AS FinancialInsurerId
					,COUNT_BIG(*) AS [Count]
				FROM dbo.PatientDemographics pd
				INNER JOIN dbo.Appointments ap ON pd.PatientId = ap.PatientId
				--Find the active insurer type
				INNER JOIN dbo.PatientReceivables prBilled ON ap.AppointmentId = prBilled.AppointmentId
				INNER JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionTypeId = prBilled.ReceivableId
					AND ptj.TransactionType = ''R''
				INNER JOIN dbo.ServiceTransactions st ON st.TransactionId = ptj.TransactionId
					AND (
						ptj.TransactionStatus IN (
							''P''
							,''S''
							)
						OR (
							ptj.TransactionStatus = ''Z''
							AND st.TransportAction <> ''0''
							)
						)
				--Find other active insurance policies with the same insurance type
				INNER JOIN dbo.PatientFinancial pf ON (
						pf.PatientId = pd.PolicyPatientId
						OR pf.PatientId = pd.SecondPolicyPatientId
						)
					AND pf.PatientId <> 0
					AND ap.AppDate >= pf.FinancialStartDate
					AND (
						ap.AppDate <= pf.FinancialEndDate
						OR pf.FinancialEndDate = ''''
						OR pf.FinancialEndDate IS NULL
						)
				INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
					AND pri.AllowDependents = 1
				WHERE prBilled.Insurerid <> 99999
				AND (
						(
							pd.PatientId <> pd.PolicyPatientId
							AND pd.PolicyPatientId <> 0
							)
						OR (
							pd.PatientId <> pd.SecondPolicyPatientId
							AND pd.SecondPolicyPatientId <> 0
							)
					)
				GROUP BY (CONVERT(bigint, 110000000) * pf.FinancialId + ap.AppointmentId)
					,prBilled.AppointmentId
					,prBilled.ExternalRefInfo
					,prBilled.ComputedFinancialId
					,pd.PatientId
					,(CONVERT(bigint, 110000000) * ap.PatientId + pf.FinancialId)
					,CASE pf.FinancialInsType
						WHEN ''V''
							THEN 200
						WHEN ''A''
							THEN 3000				
						WHEN ''W''
							THEN 40000
						ELSE 10
						END + CASE 
						WHEN pf.PatientId = pd.PolicyPatientId
							THEN 0
						ELSE 3
						END + CONVERT(INT, pf.FinancialPIndicator)
					,FinancialInsType
					,ap.ReferralId
					,ap.PreCertId
					,pf.FinancialInsurerId
		'
			,@isChanged = @ismodelInvoiceReceivables_Partition4Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_InvoiceReceivables_Partition4'
					AND object_id = OBJECT_ID('model.InvoiceReceivables_Partition4')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.InvoiceReceivables_Partition4'

			CREATE UNIQUE CLUSTERED INDEX PK_InvoiceReceivables_Partition4 ON model.InvoiceReceivables_Partition4 (
				[CompositeId]
				,[InvoiceId]
				,[BilledFinancialId]
				,[PatientId]
				,[PatientInsuranceId]
				,[OrdinalId]
				,[ReferralId]
				,[PreCertId]
				,[FinancialInsurerId]
				);
		END

		DECLARE @ismodelInvoiceReceivables_Partition5Changed BIT

		SET @ismodelInvoiceReceivables_Partition5Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InvoiceReceivables_Partition5'
			,'
		CREATE VIEW model.InvoiceReceivables_Partition5
		WITH SCHEMABINDING
		AS
		SELECT VendorId AS VendorId FROM dbo.PracticeVendors
		'
			,@isChanged = @ismodelInvoiceReceivables_Partition5Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_InvoiceReceivables_Partition5'
					AND object_id = OBJECT_ID('model.InvoiceReceivables_Partition5')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.InvoiceReceivables_Partition5'

			CREATE UNIQUE CLUSTERED INDEX PK_InvoiceReceivables_Partition5 ON model.InvoiceReceivables_Partition5 ([VendorId]);
		END

		DECLARE @ismodelInvoiceReceivables_Partition6Changed BIT

		SET @ismodelInvoiceReceivables_Partition6Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InvoiceReceivables_Partition6'
			,'
		CREATE VIEW model.InvoiceReceivables_Partition6
		WITH SCHEMABINDING
		AS
		SELECT ap.AppointmentId AS Id
				,pr.AppointmentId AS InvoiceId
				,COUNT_BIG(*) AS [Count]
			FROM dbo.PatientReceivables pr
			INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
			WHERE pr.insurerid <> 99999
			GROUP BY ap.AppointmentId
				,pr.AppointmentId
		'
			,@isChanged = @ismodelInvoiceReceivables_Partition6Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_InvoiceReceivables_Partition6'
					AND object_id = OBJECT_ID('model.InvoiceReceivables_Partition6')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.InvoiceReceivables_Partition6'

			CREATE UNIQUE CLUSTERED INDEX PK_InvoiceReceivables_Partition6 ON model.InvoiceReceivables_Partition6 ([Id]);
		END

		DECLARE @InvoiceReceivablesViewSql NVARCHAR(max)

		SET @InvoiceReceivablesViewSql = 
			'
		CREATE VIEW [model].[InvoiceReceivables]
		WITH SCHEMABINDING
		AS
		SELECT v.CompositeId AS Id
			,v.InvoiceId AS InvoiceId
			,v.OrdinalId AS OrdinalId
			,MAX(v.PatientInsuranceId) AS PatientInsuranceId
			,MAX(v.ExternalRefInfo) AS PayerClaimControlNumber
			,MAX(pCert.PreCertId) AS PatientInsuranceAuthorizationId
			,MAX(CASE WHEN pRef.ReferredInsurer <> v.FinancialInsurerId THEN NULL ELSE pRef.ReferralId END) AS PatientInsuranceReferralId
		FROM (
	
		SELECT model.InvoiceReceivables_Partition1.CompositeId AS CompositeId, model.InvoiceReceivables_Partition1.InvoiceId AS InvoiceId, model.InvoiceReceivables_Partition1.OrdinalId AS OrdinalId, model.InvoiceReceivables_Partition1.PatientInsuranceId AS PatientInsuranceId, model.InvoiceReceivables_Partition1.ExternalRefInfo AS ExternalRefInfo, model.InvoiceReceivables_Partition1.ReferralId AS ReferralId, model.InvoiceReceivables_Partition1.PreCertId AS PreCertId, model.InvoiceReceivables_Partition1.FinancialInsurerId AS FinancialInsurerId, model.InvoiceReceivables_Partition1.[Count] AS [Count] FROM model.InvoiceReceivables_Partition1 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.InvoiceReceivables_Partition2.CompositeId AS CompositeId, model.InvoiceReceivables_Partition2.InvoiceId AS InvoiceId, model.InvoiceReceivables_Partition2.OrdinalId AS OrdinalId, model.InvoiceReceivables_Partition2.PatientInsuranceId AS PatientInsuranceId, model.InvoiceReceivables_Partition2.ExternalRefInfo AS ExternalRefInfo, model.InvoiceReceivables_Partition2.ReferralId AS ReferralId, model.InvoiceReceivables_Partition2.PreCertId AS PreCertId, model.InvoiceReceivables_Partition2.FinancialInsurerId AS FinancialInsurerId, model.InvoiceReceivables_Partition2.[Count] AS [Count] FROM model.InvoiceReceivables_Partition2 WITH(NOEXPAND)
	
		UNION ALL
		
			--All insurance policies that match insurer type and invoice date
			SELECT z.CompositeId
				,z.InvoiceId
				,z.OrdinalId
				,z.PatientInsuranceId AS PatientInsuranceId
				,z.ExternalRefInfo
				,z.ReferralId AS ReferralId
				,z.PreCertId AS PreCertId
				,z.FinancialInsurerId AS FinancialInsurerId
				,0 AS [Count]
			FROM dbo.PatientFinancial pfBilled
			INNER JOIN (
	
		SELECT model.InvoiceReceivables_Partition3.CompositeId AS CompositeId, model.InvoiceReceivables_Partition3.InvoiceId AS InvoiceId, model.InvoiceReceivables_Partition3.ExternalRefInfo AS ExternalRefInfo, model.InvoiceReceivables_Partition3.BilledFinancialId AS BilledFinancialId, model.InvoiceReceivables_Partition3.PatientId AS PatientId, model.InvoiceReceivables_Partition3.PatientInsuranceId AS PatientInsuranceId, model.InvoiceReceivables_Partition3.OrdinalId AS OrdinalId, model.InvoiceReceivables_Partition3.FinancialInsType AS FinancialInsType, model.InvoiceReceivables_Partition3.ReferralId AS ReferralId, model.InvoiceReceivables_Partition3.PreCertId AS PreCertId, model.InvoiceReceivables_Partition3.FinancialInsurerId AS FinancialInsurerId, model.InvoiceReceivables_Partition3.[Count] AS [Count] FROM model.InvoiceReceivables_Partition3 WITH(NOEXPAND)
	
		) z ON pfBilled.FinancialId = z.BilledFinancialId
				AND z.FinancialInsType = pfBilled.FinancialInsType
	
			UNION ALL
	
		--All insurance policies that match insurer type and invoice date
			SELECT z.CompositeId
				,z.InvoiceId
				,z.OrdinalId
				,z.PatientInsuranceId AS PatientInsuranceId
				,z.ExternalRefInfo
				,z.ReferralId AS ReferralId
				,z.PreCertId AS PreCertId
				,z.FinancialInsurerId AS FinancialInsurerId
				,0 AS [Count]
			FROM dbo.PatientFinancial pfBilled
			INNER JOIN (
	
		SELECT model.InvoiceReceivables_Partition4.CompositeId AS CompositeId, model.InvoiceReceivables_Partition4.InvoiceId AS InvoiceId, model.InvoiceReceivables_Partition4.ExternalRefInfo AS ExternalRefInfo, model.InvoiceReceivables_Partition4.BilledFinancialId AS BilledFinancialId, model.InvoiceReceivables_Partition4.PatientId AS PatientId, model.InvoiceReceivables_Partition4.PatientInsuranceId AS PatientInsuranceId, model.InvoiceReceivables_Partition4.OrdinalId AS OrdinalId, model.InvoiceReceivables_Partition4.FinancialInsType AS FinancialInsType, model.InvoiceReceivables_Partition4.ReferralId AS ReferralId, model.InvoiceReceivables_Partition4.PreCertId AS PreCertId, model.InvoiceReceivables_Partition4.FinancialInsurerId AS FinancialInsurerId, model.InvoiceReceivables_Partition4.[Count] AS [Count] FROM model.InvoiceReceivables_Partition4 WITH(NOEXPAND)
	
		) z ON pfBilled.FinancialId = z.BilledFinancialId
				AND z.FinancialInsType = pfBilled.FinancialInsType
			) AS v
		LEFT JOIN dbo.PatientReferral pRef ON pRef.ReferralId = v.ReferralId
			AND pRef.ReferredFromId IN (
	
		SELECT model.InvoiceReceivables_Partition5.VendorId AS VendorId FROM model.InvoiceReceivables_Partition5 WITH(NOEXPAND)
	
		)
			AND pRef.ComputedFinancialId IS NOT NULL
		LEFT JOIN dbo.PatientPreCerts pCert ON pCert.PreCertId = v.PreCertId 
			AND pCert.ComputedFinancialId IS NOT NULL 
			AND COALESCE(pCert.PreCertDate, '''') <> ''''
		GROUP BY v.CompositeId
			,v.InvoiceId
			,v.OrdinalId
	
		UNION ALL
	
		------Patient  InvoiceReceivable
		SELECT v.Id AS Id
			,v.InvoiceId AS InvoiceId
			,99999 AS OrdinalId
			,NULL AS PatientInsuranceId
			,CONVERT(NVARCHAR, NULL) AS PayerClaimControlNumber
			,NULL AS PatientInsuranceAuthorizationId
			,NULL AS PatientInsuranceReferralId
		FROM (
	
		SELECT model.InvoiceReceivables_Partition6.Id AS Id, model.InvoiceReceivables_Partition6.InvoiceId AS InvoiceId, model.InvoiceReceivables_Partition6.[Count] AS [Count] FROM model.InvoiceReceivables_Partition6 WITH(NOEXPAND)
	
		) v'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InvoiceReceivables'
			,@InvoiceReceivablesViewSql
			,@isChanged = @isInvoiceReceivablesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInvoiceReceivableDelete'
				,
				'
			CREATE TRIGGER model.OnInvoiceReceivableDelete ON [model].[InvoiceReceivables] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @OrdinalId int
			DECLARE @InvoiceId int
			DECLARE @PatientInsuranceAuthorizationId int
			DECLARE @PatientInsuranceReferralId int
			DECLARE @PayerClaimControlNumber nvarchar(max)
			DECLARE @PatientInsuranceId bigint
		
			DECLARE InvoiceReceivablesDeletedCursor CURSOR FOR
			SELECT Id, OrdinalId, InvoiceId, PatientInsuranceAuthorizationId, PatientInsuranceReferralId, PayerClaimControlNumber, PatientInsuranceId FROM deleted
		
			OPEN InvoiceReceivablesDeletedCursor
			FETCH NEXT FROM InvoiceReceivablesDeletedCursor INTO @Id, @OrdinalId, @InvoiceId, @PatientInsuranceAuthorizationId, @PatientInsuranceReferralId, @PayerClaimControlNumber, @PatientInsuranceId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for InvoiceReceivable --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @OrdinalId AS OrdinalId, @InvoiceId AS InvoiceId, @PatientInsuranceAuthorizationId AS PatientInsuranceAuthorizationId, @PatientInsuranceReferralId AS PatientInsuranceReferralId, @PayerClaimControlNumber AS PayerClaimControlNumber, @PatientInsuranceId AS PatientInsuranceId
			
				FETCH NEXT FROM InvoiceReceivablesDeletedCursor INTO @Id, @OrdinalId, @InvoiceId, @PatientInsuranceAuthorizationId, @PatientInsuranceReferralId, @PayerClaimControlNumber, @PatientInsuranceId 
		
			END
		
			CLOSE InvoiceReceivablesDeletedCursor
			DEALLOCATE InvoiceReceivablesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInvoiceReceivableInsert'
				,
				'
			CREATE TRIGGER model.OnInvoiceReceivableInsert ON [model].[InvoiceReceivables] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @OrdinalId int
			DECLARE @InvoiceId int
			DECLARE @PatientInsuranceAuthorizationId int
			DECLARE @PatientInsuranceReferralId int
			DECLARE @PayerClaimControlNumber nvarchar(max)
			DECLARE @PatientInsuranceId bigint
		
			DECLARE InvoiceReceivablesInsertedCursor CURSOR FOR
			SELECT Id, OrdinalId, InvoiceId, PatientInsuranceAuthorizationId, PatientInsuranceReferralId, PayerClaimControlNumber, PatientInsuranceId FROM inserted
		
			OPEN InvoiceReceivablesInsertedCursor
			FETCH NEXT FROM InvoiceReceivablesInsertedCursor INTO @Id, @OrdinalId, @InvoiceId, @PatientInsuranceAuthorizationId, @PatientInsuranceReferralId, @PayerClaimControlNumber, @PatientInsuranceId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for InvoiceReceivable --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(bigint, NULL)
			
				SELECT @Id AS Id, @OrdinalId AS OrdinalId, @InvoiceId AS InvoiceId, @PatientInsuranceAuthorizationId AS PatientInsuranceAuthorizationId, @PatientInsuranceReferralId AS PatientInsuranceReferralId, @PayerClaimControlNumber AS PayerClaimControlNumber, @PatientInsuranceId AS PatientInsuranceId
				
				FETCH NEXT FROM InvoiceReceivablesInsertedCursor INTO @Id, @OrdinalId, @InvoiceId, @PatientInsuranceAuthorizationId, @PatientInsuranceReferralId, @PayerClaimControlNumber, @PatientInsuranceId 
		
			END
		
			CLOSE InvoiceReceivablesInsertedCursor
			DEALLOCATE InvoiceReceivablesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInvoiceReceivableUpdate'
				,
				'
			CREATE TRIGGER model.OnInvoiceReceivableUpdate ON [model].[InvoiceReceivables] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @OrdinalId int
			DECLARE @InvoiceId int
			DECLARE @PatientInsuranceAuthorizationId int
			DECLARE @PatientInsuranceReferralId int
			DECLARE @PayerClaimControlNumber nvarchar(max)
			DECLARE @PatientInsuranceId bigint
		
			DECLARE InvoiceReceivablesUpdatedCursor CURSOR FOR
			SELECT Id, OrdinalId, InvoiceId, PatientInsuranceAuthorizationId, PatientInsuranceReferralId, PayerClaimControlNumber, PatientInsuranceId FROM inserted
		
			OPEN InvoiceReceivablesUpdatedCursor
			FETCH NEXT FROM InvoiceReceivablesUpdatedCursor INTO @Id, @OrdinalId, @InvoiceId, @PatientInsuranceAuthorizationId, @PatientInsuranceReferralId, @PayerClaimControlNumber, @PatientInsuranceId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for InvoiceReceivable --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM InvoiceReceivablesUpdatedCursor INTO @Id, @OrdinalId, @InvoiceId, @PatientInsuranceAuthorizationId, @PatientInsuranceReferralId, @PayerClaimControlNumber, @PatientInsuranceId 
		
			END
		
			CLOSE InvoiceReceivablesUpdatedCursor
			DEALLOCATE InvoiceReceivablesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Invoices')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isInvoicesChanged BIT

		SET @isInvoicesChanged = 0

		DECLARE @InvoicesViewSql NVARCHAR(max)

		SET @InvoicesViewSql = 
			'
		CREATE VIEW [model].[Invoices]
		WITH SCHEMABINDING
		AS
		----ServiceLocation used for the AttributeTo property
	
		----Appt is main or satellite office (PracticeName), AttributeTo is main or satellite office (PracticeName) [NO OVERRIDE - HANDLED IN CLAIM FILE ONLY]
		SELECT v.Id AS Id, 
			MIN(v.AttributeTo) AS AttributeToServiceLocationId,
			v.BillingProviderId,
			v.[DateTime],
			v.DoesProviderRefuseAssignment,
			v.EncounterId,
			v.HasPatientAssignedBenefits,
			v.ClinicalInvoiceProviderId as ClinicalInvoiceProviderId,
			v.ReferringExternalProviderId,
			v.InvoiceTypeId,
			CONVERT(bit, v.IsNoProviderSignatureOnFile) AS IsNoProviderSignatureOnFile,
			v.IsReleaseOfInformationNotSigned,
			v.ServiceLocationCodeId
		FROM (
			SELECT pr.AppointmentId AS Id,
				pnAttribute.PracticeId AS AttributeTo,
				(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + pr.BillToDr)) AS BillingProviderId,
				CONVERT(datetime, InvoiceDate, 112) AS [DateTime],
				CASE 
					WHEN pd.FinancialAssignment = ''N''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS DoesProviderRefuseAssignment,
				ap.AppointmentId AS EncounterId,
				CASE pd.FinancialSignature
					WHEN ''Y''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS HasPatientAssignedBenefits,
				(CONVERT(bigint, 110000000) * ap.ResourceId1 + ap.AppointmentId) AS ClinicalInvoiceProviderId,
				CASE 
					WHEN pvExternalProvider.VendorId IS NULL
						THEN NULL
					ELSE pr.ReferDr 
					END AS ReferringExternalProviderId,
				CASE 
					WHEN re.ResourceType IN (''Q'', ''Y'')
						THEN 3
					ELSE 1
					END AS InvoiceTypeId,
				CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
				CASE pd.FinancialSignature
					WHEN ''N''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS IsReleaseOfInformationNotSigned,
				CASE 
					WHEN MAX(pctSLCode.Id) IS NOT NULL AND MAX(pctSLCode.Id) <> ''''
						THEN MAX(pctSLCode.Id)
					ELSE (SELECT MAX(Id) AS Id FROM dbo.PracticeCodeTable
						WHERE ReferenceType = ''PLACEOFSERVICE''
							AND SUBSTRING(Code, 1, 2) = ''11''
						GROUP BY ReferenceType, Code
						)
				END AS ServiceLocationCodeId,
				COUNT_BIG(*) AS Count
			FROM dbo.PatientReceivables pr
			INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
			INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
			INNER JOIN dbo.Resources re ON re.ResourceId = pr.BillToDr
			INNER JOIN dbo.Resources clinicalRe ON clinicalRe.ResourceId = ap.ResourceId1
			INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
			INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
				AND pnServLoc.PracticeId =
					CASE 
						WHEN ap.ResourceId2 = 0
							THEN (SELECT PracticeId AS PracticeId
								FROM dbo.PracticeName
								WHERE PracticeType = ''P''
								AND LocationReference = '''')
						ELSE ap.ResourceId2 - 1000
						END 
			INNER JOIN dbo.PracticeName pnAttribute ON pnAttribute.PracticeType = ''P''
				AND pnAttribute.PracticeId =
					CASE 
						WHEN pr.BillingOffice = 0
							THEN (SELECT PracticeId AS PracticeId
								FROM dbo.PracticeName
								WHERE PracticeType = ''P''
								AND LocationReference = '''')
						ELSE pr.BillingOffice - 1000
						END
			LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
				AND prs.Status = ''A''
			LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = prs.PlaceOfService
				AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
			LEFT JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
			LEFT JOIN dbo.PracticeVendors pvExternalProvider ON pr.ReferDr = pvExternalProvider.VendorId AND pvExternalProvider.VendorLastName <> '''' AND pvExternalProvider.VendorLastName IS NOT NULL
			WHERE ap.Comments <> ''ASC CLAIM''
				AND (
					ap.ResourceId2 = 0
					OR ap.ResourceId2 > 1000
					)
				AND (
					pr.BillingOffice = 0
					OR pr.BillingOffice > 1000
					)
			GROUP BY pr.AppointmentId,
				pnAttribute.PracticeId,
				pnServLoc.PracticeId,
				pnBillOrg.PracticeId, 
				pr.BillToDr,
				pr.InvoiceDate,
				ap.AppointmentId,
				pd.FinancialSignature,
				pd.FinancialAssignment,
				ap.ResourceId1,
				pr.ReferDr,
				re.ResourceType,
				clinicalRe.ResourceType,
				apt.ResourceId8,
				pvExternalProvider.VendorId
	
			UNION ALL
	
			----Provider is ASC, appt satellite office (PracticeName), AttributeTo is main or satellite office (PracticeName)
			SELECT pr.AppointmentId AS Id,
				pnAttribute.PracticeId AS AttributeTo,
				(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + reASC.ResourceId)) AS BillingProviderId,
				CONVERT(datetime, pr.InvoiceDate, 112) AS [DateTime],
				CASE 
					WHEN pd.FinancialAssignment = ''N''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS DoesProviderRefuseAssignment,
				apEncounter.AppointmentId AS EncounterId,
				CASE pd.FinancialSignature
					WHEN ''Y''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS HasPatientAssignedBenefits,
				(CONVERT(bigint, 110000000) * reASC.ResourceId + ap.AppointmentId) AS ClinicalInvoiceProviderId,
				CASE 
					WHEN pvExternalProvider.VendorId IS NULL
						THEN NULL
					ELSE pr.ReferDr 
					END AS ReferringExternalProviderId,
				2 AS InvoiceTypeId,
				CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
				CASE pd.FinancialSignature
					WHEN ''N''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS IsReleaseOfInformationNotSigned,
				CASE 
					WHEN MAX(pctSLCode.Id) IS NOT NULL AND MAX(pctSLCode.Id) <> ''''
						THEN MAX(pctSLCode.Id)
					ELSE (SELECT MAX(Id) AS Id FROM dbo.PracticeCodeTable
						WHERE ReferenceType = ''PLACEOFSERVICE''
							AND SUBSTRING(Code, 1, 2) = ''11''
						GROUP BY ReferenceType, Code
						)
				END AS ServiceLocationCodeId,
				COUNT_BIG(*) AS Count
			FROM dbo.PatientReceivables pr
			INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
			INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
			INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
				AND ap.Comments = ''ASC CLAIM'' 
			INNER JOIN dbo.AppointmentType apt ON ap.AppTypeId = apt.AppTypeId
				AND apt.ResourceId8 = 1
			INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
				AND apEncounter.AppTypeId = ap.AppTypeId
				AND apEncounter.AppDate = ap.AppDate
				AND apEncounter.AppTime > 0 
				AND ap.AppTime = 0
				AND apEncounter.ScheduleStatus = ap.ScheduleStatus
				AND ap.ScheduleStatus = ''D''
				AND apEncounter.ResourceId1 = ap.ResourceId1
				AND apEncounter.ResourceId2 = ap.ResourceId2
			INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
				AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
				AND PracticeType = ''P''
				AND LocationReference <> ''''
			INNER JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
				AND reASC.ResourceType = ''R''
				AND reASC.ServiceCode = ''02''
				AND reASC.Billable = ''Y''
				AND pic.FieldValue = ''T''
			INNER JOIN dbo.PracticeName pnAttribute ON pnAttribute.PracticeType = ''P''
				AND pnAttribute.PracticeId =
					CASE 
						WHEN pr.BillingOffice = 0
							THEN (SELECT PracticeId AS PracticeId
								FROM dbo.PracticeName
								WHERE PracticeType = ''P''
								AND LocationReference = '''')
						ELSE pr.BillingOffice - 1000
						END
			LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
				AND prs.Status = ''A''
			LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = reASC.PlaceOfService
				AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
			LEFT JOIN dbo.PracticeVendors pvExternalProvider ON pr.ReferDr = pvExternalProvider.VendorId AND pvExternalProvider.VendorLastName <> '''' AND pvExternalProvider.VendorLastName IS NOT NULL
			WHERE ap.ResourceId2 > 1000
				AND (
					pr.BillingOffice = 0
					OR pr.BillingOffice > 1000
					)
			GROUP BY pr.AppointmentId,
				apEncounter.AppointmentId,
				ap.AppointmentId,
				pnAttribute.PracticeId,
				pnServLoc.PracticeId,
				pr.BillToDr,
				pr.InvoiceDate,
				pd.FinancialSignature,
				pd.FinancialAssignment,
				ap.ResourceId1,
				pr.ReferDr,
				reASC.ResourceType,
				reASC.ResourceId,
				apt.ResourceId8,
				pvExternalProvider.VendorId
	
			UNION ALL
	
			----Provider is ASC, appt is satellite office (PracticeName), AttributeTo a facility (Resources)
			SELECT pr.AppointmentId AS Id,
				reAttribute.ResourceId AS AttributeTo,
				(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + reASC.ResourceId)) AS BillingProviderId,
				CONVERT(datetime, pr.InvoiceDate, 112) AS [DateTime],
				CASE 
					WHEN pd.FinancialAssignment = ''N''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS DoesProviderRefuseAssignment,
				apEncounter.AppointmentId AS EncounterId,
				CASE pd.FinancialSignature
					WHEN ''Y''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS HasPatientAssignedBenefits,
				(CONVERT(bigint, 110000000) * reASC.ResourceId + ap.AppointmentId) AS ClinicalInvoiceProviderId,
				CASE 
					WHEN pvExternalProvider.VendorId IS NULL
						THEN NULL
					ELSE pr.ReferDr 
					END AS ReferringExternalProviderId,
				2 AS InvoiceTypeId,
				CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
				CASE pd.FinancialSignature
					WHEN ''N''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS IsReleaseOfInformationNotSigned,
				CASE 
					WHEN MAX(pctSLCode.Id) IS NOT NULL AND MAX(pctSLCode.Id) <> ''''
						THEN MAX(pctSLCode.Id)
					ELSE (SELECT MAX(Id) AS Id FROM dbo.PracticeCodeTable
						WHERE ReferenceType = ''PLACEOFSERVICE''
							AND SUBSTRING(Code, 1, 2) = ''11''
						GROUP BY ReferenceType, Code
						)
				END AS ServiceLocationCodeId,
				COUNT_BIG(*) AS Count
			FROM dbo.PatientReceivables pr
			INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
			INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
			INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
				AND ap.Comments = ''ASC CLAIM'' 
			INNER JOIN dbo.AppointmentType apt ON ap.AppTypeId = apt.AppTypeId
				AND apt.ResourceId8 = 1
			INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
				AND apEncounter.AppTypeId = ap.AppTypeId
				AND apEncounter.AppDate = ap.AppDate
				AND apEncounter.AppTime > 0 
				AND ap.AppTime = 0
				AND apEncounter.ScheduleStatus = ap.ScheduleStatus
				AND ap.ScheduleStatus = ''D''
				AND apEncounter.ResourceId1 = ap.ResourceId1
				AND apEncounter.ResourceId2 = ap.ResourceId2
			INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
				AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
				AND PracticeType = ''P''
				AND LocationReference <> ''''
			INNER JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
				AND reASC.ResourceType = ''R''
				AND reASC.ServiceCode = ''02''
				AND reASC.Billable = ''Y''
				AND pic.FieldValue = ''T''
			INNER JOIN dbo.Resources reAttribute ON reAttribute.ResourceId = pr.BillingOffice
				AND reAttribute.ResourceType = ''R''
				AND reAttribute.ServiceCode = ''02''
				AND pr.BillingOffice between 1 and 999
			LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
				AND prs.Status = ''A''
			LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = reASC.PlaceOfService
				AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
			LEFT JOIN dbo.PracticeVendors pvExternalProvider ON pr.ReferDr = pvExternalProvider.VendorId AND pvExternalProvider.VendorLastName <> '''' AND pvExternalProvider.VendorLastName IS NOT NULL
			WHERE ap.ResourceId2 > 1000
				AND (
					pr.BillingOffice = 0
					OR pr.BillingOffice > 1000
					)
			GROUP BY pr.AppointmentId,
				apEncounter.AppointmentId,
				ap.AppointmentId,
				reAttribute.ResourceId,
				pnServLoc.PracticeId,
				pr.BillToDr,
				pr.InvoiceDate,
				pd.FinancialSignature,
				pd.FinancialAssignment,
				ap.ResourceId1,
				pr.ReferDr,
				reASC.ResourceType,
				reASC.ResourceId,
				apt.ResourceId8,
				pvExternalProvider.VendorId
			
			UNION ALL
	
			----Encounter is at main or satellite office (PracticeName); AttributeTo a facility (Resources) [NO OVERRIDE, OVERRIDE IS HANDLED IN CLAIM FILE ONLY]
			SELECT  pr.AppointmentId AS Id,
				(110000000 * 1 + reAttribute.ResourceId) AS AttributeTo,
				(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS BillingProviderId,
				CONVERT(datetime, InvoiceDate, 112) AS DateTime,
				CASE 
					WHEN pd.FinancialAssignment = ''N''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS DoesProviderRefuseAssignment,
				ap.AppointmentId AS EncounterId,
				CASE pd.FinancialSignature
					WHEN ''Y''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS HasPatientAssignedBenefits,
				(CONVERT(bigint, 110000000) * ap.ResourceId1 + ap.AppointmentId) AS ClinicalInvoiceProviderId,
				CASE 
					WHEN pr.ReferDr = 0 
						OR pr.ReferDr IS NULL 
						OR pr.ReferDr = ''''
						THEN NULL
					ELSE pr.ReferDr
					END AS ReferringExternalProviderId,
				CASE 
					WHEN re.ResourceType IN (''Q'', ''Y'')
						THEN 3
					ELSE 1
				END AS InvoiceTypeId,
				CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
				CASE pd.FinancialSignature
					WHEN ''N''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS IsReleaseOfInformationNotSigned,
				CASE 
					WHEN MAX(pctSLCode.Id) IS NOT NULL
						THEN MAX(pctSLCode.Id)
					ELSE (SELECT MAX(Id) AS Id FROM dbo.PracticeCodeTable
						WHERE ReferenceType = ''PLACEOFSERVICE''
							AND SUBSTRING(Code, 1, 2) = ''11''
						GROUP BY ReferenceType, Code)
					END AS ServiceLocationCodeId,
				COUNT_BIG(*) AS Count
			FROM dbo.PatientReceivables pr
			INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
			INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
			INNER JOIN dbo.Resources re ON re.ResourceId = pr.BillToDr
			INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
			INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
					   AND pnServLoc.PracticeId = CASE 
							   WHEN ap.ResourceId2 = 0
									   THEN (SELECT PracticeId AS PracticeId FROM dbo.PracticeName WHERE LocationReference = '''' AND PracticeType = ''P'')
							   ELSE ap.ResourceId2 - 1000
							   END
			INNER JOIN dbo.Resources reAttribute ON reAttribute.ResourceId = pr.BillingOffice
				AND reAttribute.ResourceType = ''R''
			INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
			LEFT JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
			LEFT JOIN dbo.PatientFinancial pf ON pf.PatientId = pr.InsuredId
				AND pr.InsurerId = pf.FinancialInsurerId
				AND InvoiceDate >= FinancialStartDate
				AND (
					InvoiceDate <= FinancialEndDate
					OR FinancialEndDate = ''''
					OR FinancialEndDate IS NULL
					)
			LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
				AND prs.Status = ''A''
			LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = prs.PlaceOfService
				AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
			WHERE (
					ap.ResourceId2 = 0
					OR ap.ResourceId2 > 1000
					)
				AND (pr.BillingOffice BETWEEN 1 AND 999 OR pr.BillingOffice > 2000)
			GROUP BY pr.AppointmentId,
				reAttribute.ResourceId,
				pnServLoc.PracticeId,
				pnBillOrg.PracticeId, 
				re.ResourceId,
				pr.InvoiceDate,
				ap.AppointmentId,
				pd.FinancialSignature,
				pd.FinancialAssignment,
				ap.ResourceId1,
				pr.ReferDr,
				re.ResourceType,
				apt.ResourceId8,
				re.PracticeId
	
			UNION ALL
	
			----Encounter is at an external facility (Resources); AttributeTo is facility (Resources)  [OVERRIDE IS HANDLED IN CLAIM FILE ONLY]
			SELECT  pr.AppointmentId AS Id,
				(110000000 * 1 + reAttribute.ResourceId) AS AttributeTo,
				(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS BillingProviderId,
				CONVERT(datetime, InvoiceDate, 112) AS DateTime,
				CASE 
					WHEN pd.FinancialAssignment = ''N''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS DoesProviderRefuseAssignment,
				ap.AppointmentId AS EncounterId,
				CASE pd.FinancialSignature
					WHEN ''Y''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS HasPatientAssignedBenefits,
				(CONVERT(bigint, 110000000) * ap.ResourceId1 + ap.AppointmentId) AS ClinicalInvoiceProviderId,
				CASE 
					WHEN pr.ReferDr = 0 
						OR pr.ReferDr IS NULL 
						OR pr.ReferDr = ''''
						THEN NULL
					ELSE pr.ReferDr
					END AS ReferringExternalProviderId,
				CASE 
					WHEN re.ResourceType IN (''Q'', ''Y'')
						THEN 3
					ELSE 1
					END AS InvoiceTypeId,
				CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
				CASE pd.FinancialSignature
					WHEN ''N''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS IsReleaseOfInformationNotSigned,
				CASE 
					WHEN MAX(pctSLCode.Id) IS NOT NULL
						THEN MAX(pctSLCode.Id)
					ELSE (SELECT MAX(Id)  AS Id FROM dbo.PracticeCodeTable
						WHERE ReferenceType = ''PLACEOFSERVICE''
							AND SUBSTRING(Code, 1, 2) = ''11''
						GROUP BY ReferenceType, Code)
					END AS ServiceLocationCodeId,
				COUNT_BIG(*) AS Count
			FROM dbo.PatientReceivables pr
			INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
			INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
			INNER JOIN dbo.Resources re ON re.ResourceId = pr.BillToDr
			INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
			INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceId = ap.ResourceId2
				AND reServLoc.ResourceType = ''R''
			INNER JOIN dbo.Resources reAttribute ON reAttribute.ResourceId = pr.BillingOffice
			INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
			LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
				AND prs.Status = ''A''
			LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = prs.PlaceOfService
					AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
			LEFT JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
			LEFT JOIN dbo.PatientFinancial pf ON pf.PatientId = pr.InsuredId
				AND pr.InsurerId = pf.FinancialInsurerId
				AND InvoiceDate >= FinancialStartDate
				AND (
					InvoiceDate <= FinancialEndDate
					OR FinancialEndDate = ''''
					OR FinancialEndDate IS NULL
					)
			WHERE (ap.ResourceId2 BETWEEN 1 AND 999 OR ap.ResourceId2 > 2000)
				AND (pr.BillingOffice BETWEEN 1 AND 999 OR pr.BillingOffice > 2000)
			GROUP BY pr.AppointmentId,
				reAttribute.ResourceId,
				reServLoc.ResourceId,
				pnBillOrg.PracticeId, 
				re.ResourceId,
				pr.InvoiceDate,
				ap.AppointmentId,
				pd.FinancialSignature,
				pd.FinancialAssignment,
				ap.ResourceId1,
				pr.ReferDr,
				re.ResourceType,
				apt.ResourceId8,
				reServLoc.PracticeId
	
			UNION ALL
	
			----appt at facility (Resources); AttributeTo at main or satellite office (PracticeName) [NO OVERRIDE, HANDLED IN CLAIM FILE ONLY]
			SELECT  pr.AppointmentId AS Id,
				pnAttribute.PracticeId AS AttributeTo,
				(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS BillingProviderId,
				CONVERT(datetime, InvoiceDate, 112) AS DateTime,
				CASE 
					WHEN pd.FinancialAssignment = ''N''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS DoesProviderRefuseAssignment,
				ap.AppointmentId AS EncounterId,
				CASE pd.FinancialSignature
					WHEN ''Y''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS HasPatientAssignedBenefits,
				(CONVERT(bigint, 110000000) * ap.ResourceId1 + ap.AppointmentId) AS ClinicalInvoiceProviderId,
				CASE 
					WHEN pr.ReferDr = 0 
						OR pr.ReferDr IS NULL 
						OR pr.ReferDr = ''''
						THEN NULL
					ELSE pr.ReferDr
					END AS ReferringExternalProviderId,
				CASE 
					WHEN re.ResourceType IN (''Q'', ''Y'')
						THEN 3
					ELSE 1
					END AS InvoiceTypeId,
				CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
				CASE pd.FinancialSignature
					WHEN ''N''
						THEN CONVERT(bit, 1)
					ELSE CONVERT(bit, 0)
					END AS IsReleaseOfInformationNotSigned,
				CASE 
					WHEN MAX(pctSLCode.Id) IS NOT NULL
						THEN MAX(pctSLCode.Id)
					ELSE (SELECT MAX(Id)  AS Id FROM dbo.PracticeCodeTable
						WHERE ReferenceType = ''PLACEOFSERVICE''
							AND SUBSTRING(Code, 1, 2) = ''11''
						GROUP BY ReferenceType, Code)
					END AS ServiceLocationCodeId,
				COUNT_BIG(*) AS Count
			FROM dbo.PatientReceivables pr
			INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
			INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
			INNER JOIN dbo.Resources re ON re.ResourceId = pr.BillToDr
			INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
			INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceId = ap.ResourceId2
				AND reServLoc.ResourceType = ''R''
			INNER JOIN dbo.PracticeName pnAttribute ON pnAttribute.PracticeType = ''P''
				AND pnAttribute.PracticeId = CASE 
					WHEN pr.BillingOffice = 0
						THEN (SELECT PracticeId AS PracticeId
							FROM dbo.PracticeName
							WHERE PracticeType = ''P''
							AND LocationReference = '''')
					ELSE pr.BillingOffice - 1000
					END
			INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
			LEFT JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
			LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
				AND prs.Status = ''A''
			LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = prs.PlaceOfService
					AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
			LEFT JOIN dbo.PatientFinancial pf ON pf.PatientId = pr.InsuredId
				AND pr.InsurerId = pf.FinancialInsurerId
				AND InvoiceDate >= FinancialStartDate
				AND (
					InvoiceDate <= FinancialEndDate
					OR FinancialEndDate = ''''
					OR FinancialEndDate IS NULL
					)
			WHERE (ap.ResourceId2 BETWEEN 1 AND 999 OR ap.ResourceId2 > 2000)
				AND (
					pr.BillingOffice = 0
					OR pr.BillingOffice > 1000
					) 
			GROUP BY pr.AppointmentId,
				pnAttribute.PracticeId,
				reServLoc.ResourceId,
				pnBillOrg.PracticeId, 
				re.ResourceId,
				pr.InvoiceDate,
				ap.AppointmentId,
				pd.FinancialAssignment,
				pd.FinancialSignature,
				ap.ResourceId1,
				pr.ReferDr,
				re.ResourceType,
				pic.FieldValue,
				apt.ResourceId8,
				reServLoc.PracticeId
	
				) AS v
		GROUP BY v.Id,
			v.BillingProviderId,
			v.[DateTime],
			v.DoesProviderRefuseAssignment,
			v.EncounterId,
			v.HasPatientAssignedBenefits,
			v.ClinicalInvoiceProviderId,
			v.ReferringExternalProviderId,
			v.InvoiceTypeId,
			v.IsNoProviderSignatureOnFile,
			v.IsReleaseOfInformationNotSigned,
			v.ServiceLocationCodeId'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Invoices'
			,@InvoicesViewSql
			,@isChanged = @isInvoicesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInvoiceDelete'
				,
				'
			CREATE TRIGGER model.OnInvoiceDelete ON [model].[Invoices] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @DateTime datetime
			DECLARE @AttributeToServiceLocationId int
			DECLARE @EncounterId int
			DECLARE @InvoiceTypeId int
			DECLARE @ClinicalInvoiceProviderId bigint
			DECLARE @HasPatientAssignedBenefits bit
			DECLARE @IsReleaseOfInformationNotSigned bit
			DECLARE @IsNoProviderSignatureOnFile bit
			DECLARE @BillingProviderId bigint
			DECLARE @ReferringExternalProviderId int
			DECLARE @ServiceLocationCodeId int
			DECLARE @DoesProviderRefuseAssignment bit
		
			DECLARE InvoicesDeletedCursor CURSOR FOR
			SELECT Id, DateTime, AttributeToServiceLocationId, EncounterId, InvoiceTypeId, ClinicalInvoiceProviderId, HasPatientAssignedBenefits, IsReleaseOfInformationNotSigned, IsNoProviderSignatureOnFile, BillingProviderId, ReferringExternalProviderId, ServiceLocationCodeId, DoesProviderRefuseAssignment FROM deleted
		
			OPEN InvoicesDeletedCursor
			FETCH NEXT FROM InvoicesDeletedCursor INTO @Id, @DateTime, @AttributeToServiceLocationId, @EncounterId, @InvoiceTypeId, @ClinicalInvoiceProviderId, @HasPatientAssignedBenefits, @IsReleaseOfInformationNotSigned, @IsNoProviderSignatureOnFile, @BillingProviderId, @ReferringExternalProviderId, @ServiceLocationCodeId, @DoesProviderRefuseAssignment 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for Invoice --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @DateTime AS DateTime, @AttributeToServiceLocationId AS AttributeToServiceLocationId, @EncounterId AS EncounterId, @InvoiceTypeId AS InvoiceTypeId, @ClinicalInvoiceProviderId AS ClinicalInvoiceProviderId, @HasPatientAssignedBenefits AS HasPatientAssignedBenefits, @IsReleaseOfInformationNotSigned AS IsReleaseOfInformationNotSigned, @IsNoProviderSignatureOnFile AS IsNoProviderSignatureOnFile, @BillingProviderId AS BillingProviderId, @ReferringExternalProviderId AS ReferringExternalProviderId, @ServiceLocationCodeId AS ServiceLocationCodeId, @DoesProviderRefuseAssignment AS DoesProviderRefuseAssignment
			
				FETCH NEXT FROM InvoicesDeletedCursor INTO @Id, @DateTime, @AttributeToServiceLocationId, @EncounterId, @InvoiceTypeId, @ClinicalInvoiceProviderId, @HasPatientAssignedBenefits, @IsReleaseOfInformationNotSigned, @IsNoProviderSignatureOnFile, @BillingProviderId, @ReferringExternalProviderId, @ServiceLocationCodeId, @DoesProviderRefuseAssignment 
		
			END
		
			CLOSE InvoicesDeletedCursor
			DEALLOCATE InvoicesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInvoiceInsert'
				,
				'
			CREATE TRIGGER model.OnInvoiceInsert ON [model].[Invoices] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @DateTime datetime
			DECLARE @AttributeToServiceLocationId int
			DECLARE @EncounterId int
			DECLARE @InvoiceTypeId int
			DECLARE @ClinicalInvoiceProviderId bigint
			DECLARE @HasPatientAssignedBenefits bit
			DECLARE @IsReleaseOfInformationNotSigned bit
			DECLARE @IsNoProviderSignatureOnFile bit
			DECLARE @BillingProviderId bigint
			DECLARE @ReferringExternalProviderId int
			DECLARE @ServiceLocationCodeId int
			DECLARE @DoesProviderRefuseAssignment bit
		
			DECLARE InvoicesInsertedCursor CURSOR FOR
			SELECT Id, DateTime, AttributeToServiceLocationId, EncounterId, InvoiceTypeId, ClinicalInvoiceProviderId, HasPatientAssignedBenefits, IsReleaseOfInformationNotSigned, IsNoProviderSignatureOnFile, BillingProviderId, ReferringExternalProviderId, ServiceLocationCodeId, DoesProviderRefuseAssignment FROM inserted
		
			OPEN InvoicesInsertedCursor
			FETCH NEXT FROM InvoicesInsertedCursor INTO @Id, @DateTime, @AttributeToServiceLocationId, @EncounterId, @InvoiceTypeId, @ClinicalInvoiceProviderId, @HasPatientAssignedBenefits, @IsReleaseOfInformationNotSigned, @IsNoProviderSignatureOnFile, @BillingProviderId, @ReferringExternalProviderId, @ServiceLocationCodeId, @DoesProviderRefuseAssignment 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for Invoice --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @DateTime AS DateTime, @AttributeToServiceLocationId AS AttributeToServiceLocationId, @EncounterId AS EncounterId, @InvoiceTypeId AS InvoiceTypeId, @ClinicalInvoiceProviderId AS ClinicalInvoiceProviderId, @HasPatientAssignedBenefits AS HasPatientAssignedBenefits, @IsReleaseOfInformationNotSigned AS IsReleaseOfInformationNotSigned, @IsNoProviderSignatureOnFile AS IsNoProviderSignatureOnFile, @BillingProviderId AS BillingProviderId, @ReferringExternalProviderId AS ReferringExternalProviderId, @ServiceLocationCodeId AS ServiceLocationCodeId, @DoesProviderRefuseAssignment AS DoesProviderRefuseAssignment
				
				FETCH NEXT FROM InvoicesInsertedCursor INTO @Id, @DateTime, @AttributeToServiceLocationId, @EncounterId, @InvoiceTypeId, @ClinicalInvoiceProviderId, @HasPatientAssignedBenefits, @IsReleaseOfInformationNotSigned, @IsNoProviderSignatureOnFile, @BillingProviderId, @ReferringExternalProviderId, @ServiceLocationCodeId, @DoesProviderRefuseAssignment 
		
			END
		
			CLOSE InvoicesInsertedCursor
			DEALLOCATE InvoicesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInvoiceUpdate'
				,
				'
			CREATE TRIGGER model.OnInvoiceUpdate ON [model].[Invoices] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @DateTime datetime
			DECLARE @AttributeToServiceLocationId int
			DECLARE @EncounterId int
			DECLARE @InvoiceTypeId int
			DECLARE @ClinicalInvoiceProviderId bigint
			DECLARE @HasPatientAssignedBenefits bit
			DECLARE @IsReleaseOfInformationNotSigned bit
			DECLARE @IsNoProviderSignatureOnFile bit
			DECLARE @BillingProviderId bigint
			DECLARE @ReferringExternalProviderId int
			DECLARE @ServiceLocationCodeId int
			DECLARE @DoesProviderRefuseAssignment bit
		
			DECLARE InvoicesUpdatedCursor CURSOR FOR
			SELECT Id, DateTime, AttributeToServiceLocationId, EncounterId, InvoiceTypeId, ClinicalInvoiceProviderId, HasPatientAssignedBenefits, IsReleaseOfInformationNotSigned, IsNoProviderSignatureOnFile, BillingProviderId, ReferringExternalProviderId, ServiceLocationCodeId, DoesProviderRefuseAssignment FROM inserted
		
			OPEN InvoicesUpdatedCursor
			FETCH NEXT FROM InvoicesUpdatedCursor INTO @Id, @DateTime, @AttributeToServiceLocationId, @EncounterId, @InvoiceTypeId, @ClinicalInvoiceProviderId, @HasPatientAssignedBenefits, @IsReleaseOfInformationNotSigned, @IsNoProviderSignatureOnFile, @BillingProviderId, @ReferringExternalProviderId, @ServiceLocationCodeId, @DoesProviderRefuseAssignment 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for Invoice --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM InvoicesUpdatedCursor INTO @Id, @DateTime, @AttributeToServiceLocationId, @EncounterId, @InvoiceTypeId, @ClinicalInvoiceProviderId, @HasPatientAssignedBenefits, @IsReleaseOfInformationNotSigned, @IsNoProviderSignatureOnFile, @BillingProviderId, @ReferringExternalProviderId, @ServiceLocationCodeId, @DoesProviderRefuseAssignment 
		
			END
		
			CLOSE InvoicesUpdatedCursor
			DEALLOCATE InvoicesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientCommunicationPreferences')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientCommunicationPreferencesChanged BIT

		SET @isPatientCommunicationPreferencesChanged = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientCommunicationPreferences'
			,'
		CREATE VIEW [model].[PatientCommunicationPreferences]
		WITH SCHEMABINDING
		AS
		SELECT CONVERT(int, NULL) AS Id, CONVERT(int, NULL) AS PatientId, CONVERT(int, NULL) AS PatientCommunicationTypeId, CONVERT(bigint, NULL) AS PatientAddressId, CONVERT(bit, NULL) AS IsAddressedToRecipient, CONVERT(int, NULL) AS EmailAddressId, CONVERT(int, NULL) AS PatientPhoneNumberId, CONVERT(int, NULL) AS PhoneNumberCommunicationMethodId, CONVERT(nvarchar(100), NULL) AS __EntityType__
		-- View SQL not found for PatientCommunicationPreference --
		'
			,@isChanged = @isPatientCommunicationPreferencesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientCommunicationPreferenceDelete'
				,
				'
			CREATE TRIGGER model.OnPatientCommunicationPreferenceDelete ON [model].[PatientCommunicationPreferences] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PatientId int
			DECLARE @PatientCommunicationTypeId int
			DECLARE @PatientAddressId bigint
			DECLARE @IsAddressedToRecipient bit
			DECLARE @EmailAddressId int
			DECLARE @PatientPhoneNumberId int
			DECLARE @PhoneNumberCommunicationMethodId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE PatientCommunicationPreferencesDeletedCursor CURSOR FOR
			SELECT Id, PatientId, PatientCommunicationTypeId, PatientAddressId, IsAddressedToRecipient, EmailAddressId, PatientPhoneNumberId, PhoneNumberCommunicationMethodId, __EntityType__ FROM deleted
		
			OPEN PatientCommunicationPreferencesDeletedCursor
			FETCH NEXT FROM PatientCommunicationPreferencesDeletedCursor INTO @Id, @PatientId, @PatientCommunicationTypeId, @PatientAddressId, @IsAddressedToRecipient, @EmailAddressId, @PatientPhoneNumberId, @PhoneNumberCommunicationMethodId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PatientCommunicationPreference --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @PatientId AS PatientId, @PatientCommunicationTypeId AS PatientCommunicationTypeId, @PatientAddressId AS PatientAddressId, @IsAddressedToRecipient AS IsAddressedToRecipient, @EmailAddressId AS EmailAddressId, @PatientPhoneNumberId AS PatientPhoneNumberId, @PhoneNumberCommunicationMethodId AS PhoneNumberCommunicationMethodId, @__EntityType__ AS __EntityType__
			
				FETCH NEXT FROM PatientCommunicationPreferencesDeletedCursor INTO @Id, @PatientId, @PatientCommunicationTypeId, @PatientAddressId, @IsAddressedToRecipient, @EmailAddressId, @PatientPhoneNumberId, @PhoneNumberCommunicationMethodId, @__EntityType__ 
		
			END
		
			CLOSE PatientCommunicationPreferencesDeletedCursor
			DEALLOCATE PatientCommunicationPreferencesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientCommunicationPreferenceInsert'
				,
				'
			CREATE TRIGGER model.OnPatientCommunicationPreferenceInsert ON [model].[PatientCommunicationPreferences] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PatientId int
			DECLARE @PatientCommunicationTypeId int
			DECLARE @PatientAddressId bigint
			DECLARE @IsAddressedToRecipient bit
			DECLARE @EmailAddressId int
			DECLARE @PatientPhoneNumberId int
			DECLARE @PhoneNumberCommunicationMethodId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE PatientCommunicationPreferencesInsertedCursor CURSOR FOR
			SELECT Id, PatientId, PatientCommunicationTypeId, PatientAddressId, IsAddressedToRecipient, EmailAddressId, PatientPhoneNumberId, PhoneNumberCommunicationMethodId, __EntityType__ FROM inserted
		
			OPEN PatientCommunicationPreferencesInsertedCursor
			FETCH NEXT FROM PatientCommunicationPreferencesInsertedCursor INTO @Id, @PatientId, @PatientCommunicationTypeId, @PatientAddressId, @IsAddressedToRecipient, @EmailAddressId, @PatientPhoneNumberId, @PhoneNumberCommunicationMethodId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for PatientCommunicationPreference --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @PatientId AS PatientId, @PatientCommunicationTypeId AS PatientCommunicationTypeId, @PatientAddressId AS PatientAddressId, @IsAddressedToRecipient AS IsAddressedToRecipient, @EmailAddressId AS EmailAddressId, @PatientPhoneNumberId AS PatientPhoneNumberId, @PhoneNumberCommunicationMethodId AS PhoneNumberCommunicationMethodId, @__EntityType__ AS __EntityType__
				
				FETCH NEXT FROM PatientCommunicationPreferencesInsertedCursor INTO @Id, @PatientId, @PatientCommunicationTypeId, @PatientAddressId, @IsAddressedToRecipient, @EmailAddressId, @PatientPhoneNumberId, @PhoneNumberCommunicationMethodId, @__EntityType__ 
		
			END
		
			CLOSE PatientCommunicationPreferencesInsertedCursor
			DEALLOCATE PatientCommunicationPreferencesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientCommunicationPreferenceUpdate'
				,
				'
			CREATE TRIGGER model.OnPatientCommunicationPreferenceUpdate ON [model].[PatientCommunicationPreferences] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PatientId int
			DECLARE @PatientCommunicationTypeId int
			DECLARE @PatientAddressId bigint
			DECLARE @IsAddressedToRecipient bit
			DECLARE @EmailAddressId int
			DECLARE @PatientPhoneNumberId int
			DECLARE @PhoneNumberCommunicationMethodId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE PatientCommunicationPreferencesUpdatedCursor CURSOR FOR
			SELECT Id, PatientId, PatientCommunicationTypeId, PatientAddressId, IsAddressedToRecipient, EmailAddressId, PatientPhoneNumberId, PhoneNumberCommunicationMethodId, __EntityType__ FROM inserted
		
			OPEN PatientCommunicationPreferencesUpdatedCursor
			FETCH NEXT FROM PatientCommunicationPreferencesUpdatedCursor INTO @Id, @PatientId, @PatientCommunicationTypeId, @PatientAddressId, @IsAddressedToRecipient, @EmailAddressId, @PatientPhoneNumberId, @PhoneNumberCommunicationMethodId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for PatientCommunicationPreference --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM PatientCommunicationPreferencesUpdatedCursor INTO @Id, @PatientId, @PatientCommunicationTypeId, @PatientAddressId, @IsAddressedToRecipient, @EmailAddressId, @PatientPhoneNumberId, @PhoneNumberCommunicationMethodId, @__EntityType__ 
		
			END
		
			CLOSE PatientCommunicationPreferencesUpdatedCursor
			DEALLOCATE PatientCommunicationPreferencesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientEmploymentStatus')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientEmploymentStatusChanged BIT

		SET @isPatientEmploymentStatusChanged = 0

		DECLARE @PatientEmploymentStatusViewSql NVARCHAR(max)

		SET @PatientEmploymentStatusViewSql = '
		CREATE VIEW [model].[PatientEmploymentStatus_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id AS Id,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType LIKE ''EMPLOYEESTATUS'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientEmploymentStatus_Internal'
			,@PatientEmploymentStatusViewSql
			,@isChanged = @isPatientEmploymentStatusChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @PatientEmploymentStatusViewSql = '	
			CREATE VIEW [model].[PatientEmploymentStatus]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, IsArchived FROM [model].[PatientEmploymentStatus_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_PatientEmploymentStatus'
							AND object_id = OBJECT_ID('[model].[PatientEmploymentStatus_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[PatientEmploymentStatus_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_PatientEmploymentStatus] ON [model].[PatientEmploymentStatus_Internal] ([Id]);
				END

				SET @PatientEmploymentStatusViewSql = @PatientEmploymentStatusViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientEmploymentStatus'
				,@PatientEmploymentStatusViewSql
				,@isChanged = @isPatientEmploymentStatusChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientEmploymentStatusDelete'
				,'
			CREATE TRIGGER model.OnPatientEmploymentStatusDelete ON [model].[PatientEmploymentStatus] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientEmploymentStatusDeletedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM deleted
		
			OPEN PatientEmploymentStatusDeletedCursor
			FETCH NEXT FROM PatientEmploymentStatusDeletedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PatientEmploymentStatus --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
			
				FETCH NEXT FROM PatientEmploymentStatusDeletedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientEmploymentStatusDeletedCursor
			DEALLOCATE PatientEmploymentStatusDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientEmploymentStatusInsert'
				,'
			CREATE TRIGGER model.OnPatientEmploymentStatusInsert ON [model].[PatientEmploymentStatus] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientEmploymentStatusInsertedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN PatientEmploymentStatusInsertedCursor
			FETCH NEXT FROM PatientEmploymentStatusInsertedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for PatientEmploymentStatus --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
				
				FETCH NEXT FROM PatientEmploymentStatusInsertedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientEmploymentStatusInsertedCursor
			DEALLOCATE PatientEmploymentStatusInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientEmploymentStatusUpdate'
				,'
			CREATE TRIGGER model.OnPatientEmploymentStatusUpdate ON [model].[PatientEmploymentStatus] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientEmploymentStatusUpdatedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN PatientEmploymentStatusUpdatedCursor
			FETCH NEXT FROM PatientEmploymentStatusUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for PatientEmploymentStatus --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM PatientEmploymentStatusUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientEmploymentStatusUpdatedCursor
			DEALLOCATE PatientEmploymentStatusUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientEthnicities')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientEthnicitiesChanged BIT

		SET @isPatientEthnicitiesChanged = 0

		DECLARE @PatientEthnicitiesViewSql NVARCHAR(max)

		SET @PatientEthnicitiesViewSql = '
		CREATE VIEW [model].[PatientEthnicities_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''ETHNICITY'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientEthnicities_Internal'
			,@PatientEthnicitiesViewSql
			,@isChanged = @isPatientEthnicitiesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @PatientEthnicitiesViewSql = '	
			CREATE VIEW [model].[PatientEthnicities]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, isArchived FROM [model].[PatientEthnicities_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_PatientEthnicities'
							AND object_id = OBJECT_ID('[model].[PatientEthnicities_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[PatientEthnicities_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_PatientEthnicities] ON [model].[PatientEthnicities_Internal] ([Id]);
				END

				SET @PatientEthnicitiesViewSql = @PatientEthnicitiesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientEthnicities'
				,@PatientEthnicitiesViewSql
				,@isChanged = @isPatientEthnicitiesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientEthnicityDelete'
				,'
			CREATE TRIGGER model.OnPatientEthnicityDelete ON [model].[PatientEthnicities] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @isArchived bit
		
			DECLARE PatientEthnicitiesDeletedCursor CURSOR FOR
			SELECT Id, Name, isArchived FROM deleted
		
			OPEN PatientEthnicitiesDeletedCursor
			FETCH NEXT FROM PatientEthnicitiesDeletedCursor INTO @Id, @Name, @isArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- PatientEthnicity Delete
				DELETE
				FROM dbo.PracticeCodeTable
				WHERE Id = @Id
			
				SELECT @Id AS Id, @Name AS Name, @isArchived AS isArchived
			
				FETCH NEXT FROM PatientEthnicitiesDeletedCursor INTO @Id, @Name, @isArchived 
		
			END
		
			CLOSE PatientEthnicitiesDeletedCursor
			DEALLOCATE PatientEthnicitiesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientEthnicityInsert'
				,
				'
			CREATE TRIGGER model.OnPatientEthnicityInsert ON [model].[PatientEthnicities] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @isArchived bit
		
			DECLARE PatientEthnicitiesInsertedCursor CURSOR FOR
			SELECT Id, Name, isArchived FROM inserted
		
			OPEN PatientEthnicitiesInsertedCursor
			FETCH NEXT FROM PatientEthnicitiesInsertedCursor INTO @Id, @Name, @isArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- PatientEthnicity Insert
				DECLARE @checkExists int
			
				SELECT @checkExists = COUNT(*)
				FROM dbo.PracticeCodeTable
				WHERE ReferenceType = ''ETHNICITY''
					AND Code = @Name
			
				IF @checkExists = 0
				BEGIN
					INSERT INTO dbo.PracticeCodeTable (ReferenceType)
					VALUES (''ETHNICITY'')
			
					SET @Id = IDENT_CURRENT(''PracticeCodeTable'')
				END
				ELSE
				BEGIN
					SELECT @Id = Id
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''ETHNICITY''
						AND Code = @Name
				END
			
							
				-- PatientEthnicity Update/Insert
				UPDATE dbo.PracticeCodeTable
				SET Code = @Name
				WHERE ReferenceType = ''ETHNICITY''
					AND Id = @Id
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @Name AS Name, @isArchived AS isArchived
				
				FETCH NEXT FROM PatientEthnicitiesInsertedCursor INTO @Id, @Name, @isArchived 
		
			END
		
			CLOSE PatientEthnicitiesInsertedCursor
			DEALLOCATE PatientEthnicitiesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientEthnicityUpdate'
				,'
			CREATE TRIGGER model.OnPatientEthnicityUpdate ON [model].[PatientEthnicities] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @isArchived bit
		
			DECLARE PatientEthnicitiesUpdatedCursor CURSOR FOR
			SELECT Id, Name, isArchived FROM inserted
		
			OPEN PatientEthnicitiesUpdatedCursor
			FETCH NEXT FROM PatientEthnicitiesUpdatedCursor INTO @Id, @Name, @isArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- PatientEthnicity Update/Insert
				UPDATE dbo.PracticeCodeTable
				SET Code = @Name
				WHERE ReferenceType = ''ETHNICITY''
					AND Id = @Id
				
				FETCH NEXT FROM PatientEthnicitiesUpdatedCursor INTO @Id, @Name, @isArchived 
		
			END
		
			CLOSE PatientEthnicitiesUpdatedCursor
			DEALLOCATE PatientEthnicitiesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientExternalProviders')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientExternalProvidersChanged BIT

		SET @isPatientExternalProvidersChanged = 0

		DECLARE @ismodelPatientExternalProviders_Partition1Changed BIT

		SET @ismodelPatientExternalProviders_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientExternalProviders_Partition1'
			,'
		CREATE VIEW model.PatientExternalProviders_Partition1
		WITH SCHEMABINDING
		AS
		----ReferringPhysician
		SELECT pd.PatientId AS Id,
			ReferringPhysician AS ExternalProviderId,
			CONVERT(bit, 0) AS IsPrimaryCarePhysician,
			CONVERT(bit, 1) AS IsReferringPhysician,
			PatientId AS PatientId
		FROM dbo.PatientDemographics pd
		INNER JOIN dbo.PracticeVendors pv ON pd.ReferringPhysician = pv.VendorId
		WHERE pd.ReferringPhysician <> ''''
			AND pv.VendorLastName <> ''''
			AND pv.VendorLastName IS NOT NULL
		'
			,@isChanged = @ismodelPatientExternalProviders_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PatientExternalProviders_Partition1'
					AND object_id = OBJECT_ID('model.PatientExternalProviders_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PatientExternalProviders_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_PatientExternalProviders_Partition1 ON model.PatientExternalProviders_Partition1 ([Id]);
		END

		DECLARE @ismodelPatientExternalProviders_Partition2Changed BIT

		SET @ismodelPatientExternalProviders_Partition2Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientExternalProviders_Partition2'
			,'
		CREATE VIEW model.PatientExternalProviders_Partition2
		WITH SCHEMABINDING
		AS
		----PrimaryCarePhysician
		SELECT (110000000 * 1 + pd.PatientId) AS Id,
			PrimaryCarePhysician AS ExternalProviderId,
			CONVERT(bit, 1) AS IsPrimaryCarePhysician,
			CONVERT(bit, 0) AS IsReferringPhysician,
			PatientId AS PatientId
		FROM dbo.PatientDemographics pd
		INNER JOIN dbo.PracticeVendors pv ON pd.PrimaryCarePhysician = pv.VendorId
		WHERE PrimaryCarePhysician <> ''''
			AND pv.VendorLastName <> ''''
			AND pv.VendorLastName IS NOT NULL
		'
			,@isChanged = @ismodelPatientExternalProviders_Partition2Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PatientExternalProviders_Partition2'
					AND object_id = OBJECT_ID('model.PatientExternalProviders_Partition2')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PatientExternalProviders_Partition2'

			CREATE UNIQUE CLUSTERED INDEX PK_PatientExternalProviders_Partition2 ON model.PatientExternalProviders_Partition2 ([Id]);
		END

		DECLARE @PatientExternalProvidersViewSql NVARCHAR(max)

		SET @PatientExternalProvidersViewSql = 
			'
		CREATE VIEW [model].[PatientExternalProviders]
		WITH SCHEMABINDING
		AS
		SELECT model.PatientExternalProviders_Partition1.Id AS Id, model.PatientExternalProviders_Partition1.ExternalProviderId AS ExternalProviderId, model.PatientExternalProviders_Partition1.IsPrimaryCarePhysician AS IsPrimaryCarePhysician, model.PatientExternalProviders_Partition1.IsReferringPhysician AS IsReferringPhysician, model.PatientExternalProviders_Partition1.PatientId AS PatientId FROM model.PatientExternalProviders_Partition1 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PatientExternalProviders_Partition2.Id AS Id, model.PatientExternalProviders_Partition2.ExternalProviderId AS ExternalProviderId, model.PatientExternalProviders_Partition2.IsPrimaryCarePhysician AS IsPrimaryCarePhysician, model.PatientExternalProviders_Partition2.IsReferringPhysician AS IsReferringPhysician, model.PatientExternalProviders_Partition2.PatientId AS PatientId FROM model.PatientExternalProviders_Partition2 WITH(NOEXPAND)'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientExternalProviders'
			,@PatientExternalProvidersViewSql
			,@isChanged = @isPatientExternalProvidersChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientExternalProviderDelete'
				,
				'
			CREATE TRIGGER model.OnPatientExternalProviderDelete ON [model].[PatientExternalProviders] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @IsPrimaryCarePhysician bit
			DECLARE @IsReferringPhysician bit
			DECLARE @ExternalProviderId int
			DECLARE @PatientId int
		
			DECLARE PatientExternalProvidersDeletedCursor CURSOR FOR
			SELECT Id, IsPrimaryCarePhysician, IsReferringPhysician, ExternalProviderId, PatientId FROM deleted
		
			OPEN PatientExternalProvidersDeletedCursor
			FETCH NEXT FROM PatientExternalProvidersDeletedCursor INTO @Id, @IsPrimaryCarePhysician, @IsReferringPhysician, @ExternalProviderId, @PatientId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PatientExternalProvider --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @IsPrimaryCarePhysician AS IsPrimaryCarePhysician, @IsReferringPhysician AS IsReferringPhysician, @ExternalProviderId AS ExternalProviderId, @PatientId AS PatientId
			
				FETCH NEXT FROM PatientExternalProvidersDeletedCursor INTO @Id, @IsPrimaryCarePhysician, @IsReferringPhysician, @ExternalProviderId, @PatientId 
		
			END
		
			CLOSE PatientExternalProvidersDeletedCursor
			DEALLOCATE PatientExternalProvidersDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientExternalProviderInsert'
				,
				'
			CREATE TRIGGER model.OnPatientExternalProviderInsert ON [model].[PatientExternalProviders] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @IsPrimaryCarePhysician bit
			DECLARE @IsReferringPhysician bit
			DECLARE @ExternalProviderId int
			DECLARE @PatientId int
		
			DECLARE PatientExternalProvidersInsertedCursor CURSOR FOR
			SELECT Id, IsPrimaryCarePhysician, IsReferringPhysician, ExternalProviderId, PatientId FROM inserted
		
			OPEN PatientExternalProvidersInsertedCursor
			FETCH NEXT FROM PatientExternalProvidersInsertedCursor INTO @Id, @IsPrimaryCarePhysician, @IsReferringPhysician, @ExternalProviderId, @PatientId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- PatientExternalProvider Insert
				DECLARE @patientDemographicsId int
			
				SET @Id = @PatientId
			
			
							
				-- PatientExternalProvider Update/Insert
				UPDATE dbo.PatientDemographics
				SET ReferringPhysician = @ExternalProviderId
				WHERE Patientid = @PatientId
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @IsPrimaryCarePhysician AS IsPrimaryCarePhysician, @IsReferringPhysician AS IsReferringPhysician, @ExternalProviderId AS ExternalProviderId, @PatientId AS PatientId
				
				FETCH NEXT FROM PatientExternalProvidersInsertedCursor INTO @Id, @IsPrimaryCarePhysician, @IsReferringPhysician, @ExternalProviderId, @PatientId 
		
			END
		
			CLOSE PatientExternalProvidersInsertedCursor
			DEALLOCATE PatientExternalProvidersInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientExternalProviderUpdate'
				,
				'
			CREATE TRIGGER model.OnPatientExternalProviderUpdate ON [model].[PatientExternalProviders] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @IsPrimaryCarePhysician bit
			DECLARE @IsReferringPhysician bit
			DECLARE @ExternalProviderId int
			DECLARE @PatientId int
		
			DECLARE PatientExternalProvidersUpdatedCursor CURSOR FOR
			SELECT Id, IsPrimaryCarePhysician, IsReferringPhysician, ExternalProviderId, PatientId FROM inserted
		
			OPEN PatientExternalProvidersUpdatedCursor
			FETCH NEXT FROM PatientExternalProvidersUpdatedCursor INTO @Id, @IsPrimaryCarePhysician, @IsReferringPhysician, @ExternalProviderId, @PatientId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- PatientExternalProvider Update/Insert
				UPDATE dbo.PatientDemographics
				SET ReferringPhysician = @ExternalProviderId
				WHERE Patientid = @PatientId
				
				FETCH NEXT FROM PatientExternalProvidersUpdatedCursor INTO @Id, @IsPrimaryCarePhysician, @IsReferringPhysician, @ExternalProviderId, @PatientId 
		
			END
		
			CLOSE PatientExternalProvidersUpdatedCursor
			DEALLOCATE PatientExternalProvidersUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientInsuranceAuthorizations')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientInsuranceAuthorizationsChanged BIT

		SET @isPatientInsuranceAuthorizationsChanged = 0

		DECLARE @PatientInsuranceAuthorizationsViewSql NVARCHAR(max)

		SET @PatientInsuranceAuthorizationsViewSql = '
		CREATE VIEW [model].[PatientInsuranceAuthorizations_Internal]
		WITH SCHEMABINDING
		AS
		SELECT ppc.PreCertId AS Id
			,ppc.PreCert AS AuthorizationCode
			,CASE
				WHEN COALESCE(PreCertDate, '''') <> ''''
					THEN CONVERT(datetime, PreCertDate, 112) 
				ELSE NULL END AS AuthorizationDateTime
			,ap.AppointmentId AS EncounterId
			,CONVERT(bit, CASE PreCertStatus
				   WHEN ''A''
					   THEN CONVERT(bit, 0)
				   ELSE CONVERT(bit, 1)
				END) AS IsArchived
			,(CONVERT(bigint, 110000000) * pd.PatientId + ppc.ComputedFinancialId) AS PatientInsuranceId
		FROM dbo.PatientPreCerts ppc
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = ppc.PreCertApptId
			AND ap.ScheduleStatus IN (''D'', ''P'', ''R'', ''A'')
		INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = ppc.PatientId
		WHERE COALESCE(PreCertDate, '''') <> ''''
			 AND ppc.ComputedFinancialId IS NOT NULL'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientInsuranceAuthorizations_Internal'
			,@PatientInsuranceAuthorizationsViewSql
			,@isChanged = @isPatientInsuranceAuthorizationsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @PatientInsuranceAuthorizationsViewSql = '	
			CREATE VIEW [model].[PatientInsuranceAuthorizations]
			WITH SCHEMABINDING
			AS
			SELECT Id, AuthorizationCode, AuthorizationDateTime, IsArchived, PatientInsuranceId, EncounterId FROM [model].[PatientInsuranceAuthorizations_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_PatientInsuranceAuthorizations'
							AND object_id = OBJECT_ID('[model].[PatientInsuranceAuthorizations_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[PatientInsuranceAuthorizations_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_PatientInsuranceAuthorizations] ON [model].[PatientInsuranceAuthorizations_Internal] ([Id]);
				END

				SET @PatientInsuranceAuthorizationsViewSql = @PatientInsuranceAuthorizationsViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientInsuranceAuthorizations'
				,@PatientInsuranceAuthorizationsViewSql
				,@isChanged = @isPatientInsuranceAuthorizationsChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientInsuranceAuthorizationDelete'
				,
				'
			CREATE TRIGGER model.OnPatientInsuranceAuthorizationDelete ON [model].[PatientInsuranceAuthorizations] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @AuthorizationCode nvarchar(max)
			DECLARE @AuthorizationDateTime datetime
			DECLARE @IsArchived bit
			DECLARE @PatientInsuranceId bigint
			DECLARE @EncounterId int
		
			DECLARE PatientInsuranceAuthorizationsDeletedCursor CURSOR FOR
			SELECT Id, AuthorizationCode, AuthorizationDateTime, IsArchived, PatientInsuranceId, EncounterId FROM deleted
		
			OPEN PatientInsuranceAuthorizationsDeletedCursor
			FETCH NEXT FROM PatientInsuranceAuthorizationsDeletedCursor INTO @Id, @AuthorizationCode, @AuthorizationDateTime, @IsArchived, @PatientInsuranceId, @EncounterId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PatientInsuranceAuthorization --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @AuthorizationCode AS AuthorizationCode, @AuthorizationDateTime AS AuthorizationDateTime, @IsArchived AS IsArchived, @PatientInsuranceId AS PatientInsuranceId, @EncounterId AS EncounterId
			
				FETCH NEXT FROM PatientInsuranceAuthorizationsDeletedCursor INTO @Id, @AuthorizationCode, @AuthorizationDateTime, @IsArchived, @PatientInsuranceId, @EncounterId 
		
			END
		
			CLOSE PatientInsuranceAuthorizationsDeletedCursor
			DEALLOCATE PatientInsuranceAuthorizationsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientInsuranceAuthorizationInsert'
				,
				'
			CREATE TRIGGER model.OnPatientInsuranceAuthorizationInsert ON [model].[PatientInsuranceAuthorizations] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @AuthorizationCode nvarchar(max)
			DECLARE @AuthorizationDateTime datetime
			DECLARE @IsArchived bit
			DECLARE @PatientInsuranceId bigint
			DECLARE @EncounterId int
		
			DECLARE PatientInsuranceAuthorizationsInsertedCursor CURSOR FOR
			SELECT Id, AuthorizationCode, AuthorizationDateTime, IsArchived, PatientInsuranceId, EncounterId FROM inserted
		
			OPEN PatientInsuranceAuthorizationsInsertedCursor
			FETCH NEXT FROM PatientInsuranceAuthorizationsInsertedCursor INTO @Id, @AuthorizationCode, @AuthorizationDateTime, @IsArchived, @PatientInsuranceId, @EncounterId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for PatientInsuranceAuthorization --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @AuthorizationCode AS AuthorizationCode, @AuthorizationDateTime AS AuthorizationDateTime, @IsArchived AS IsArchived, @PatientInsuranceId AS PatientInsuranceId, @EncounterId AS EncounterId
				
				FETCH NEXT FROM PatientInsuranceAuthorizationsInsertedCursor INTO @Id, @AuthorizationCode, @AuthorizationDateTime, @IsArchived, @PatientInsuranceId, @EncounterId 
		
			END
		
			CLOSE PatientInsuranceAuthorizationsInsertedCursor
			DEALLOCATE PatientInsuranceAuthorizationsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientInsuranceAuthorizationUpdate'
				,
				'
			CREATE TRIGGER model.OnPatientInsuranceAuthorizationUpdate ON [model].[PatientInsuranceAuthorizations] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @AuthorizationCode nvarchar(max)
			DECLARE @AuthorizationDateTime datetime
			DECLARE @IsArchived bit
			DECLARE @PatientInsuranceId bigint
			DECLARE @EncounterId int
		
			DECLARE PatientInsuranceAuthorizationsUpdatedCursor CURSOR FOR
			SELECT Id, AuthorizationCode, AuthorizationDateTime, IsArchived, PatientInsuranceId, EncounterId FROM inserted
		
			OPEN PatientInsuranceAuthorizationsUpdatedCursor
			FETCH NEXT FROM PatientInsuranceAuthorizationsUpdatedCursor INTO @Id, @AuthorizationCode, @AuthorizationDateTime, @IsArchived, @PatientInsuranceId, @EncounterId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for PatientInsuranceAuthorization --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM PatientInsuranceAuthorizationsUpdatedCursor INTO @Id, @AuthorizationCode, @AuthorizationDateTime, @IsArchived, @PatientInsuranceId, @EncounterId 
		
			END
		
			CLOSE PatientInsuranceAuthorizationsUpdatedCursor
			DEALLOCATE PatientInsuranceAuthorizationsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientInsuranceReferrals')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientInsuranceReferralsChanged BIT

		SET @isPatientInsuranceReferralsChanged = 0

		DECLARE @PatientInsuranceReferralsViewSql NVARCHAR(max)

		SET @PatientInsuranceReferralsViewSql = 
			'
		CREATE VIEW [model].[PatientInsuranceReferrals]
		WITH SCHEMABINDING
		AS
		SELECT pRef.ReferralId AS Id,
			CASE 
				WHEN COALESCE(ReferralExpireDate, '''') <> ''''
					THEN CONVERT(datetime, ReferralExpireDate, 112)
				ELSE NULL END AS EndDateTime,
			ReferredFromId AS ExternalProviderId,
			CONVERT(bit, CASE pRef.[Status]
					WHEN ''A''
						THEN CONVERT(bit, 0)
					ELSE CONVERT(bit, 1)
					END) AS IsArchived,
			pd.PatientId AS PatientId,
			(CONVERT(bigint, 110000000) * pd.PatientId + pRef.ComputedFinancialId) AS PatientInsuranceId,
			pRef.ReferredTo AS DoctorId,
			pRef.Referral AS ReferralCode,
			pRef.ReferralVisits AS TotalEncountersCovered,
			CASE 
				WHEN COALESCE(ReferralDate, '''') <> ''''
					THEN CONVERT(datetime, ReferralDate, 112) 
				ELSE NULL END AS StartDateTime
		FROM dbo.PatientReferral pRef
		INNER JOIN dbo.PracticeVendors pv ON pv.VendorId = pRef.ReferredFromId
			AND VendorLastName <> '''' AND VendorLastName IS NOT NULL
		INNER JOIN dbo.PatientDemographics pd on pd.PatientId = pRef.PatientId
		INNER JOIN dbo.PatientFinancial pf ON pf.FinancialId = pRef.ComputedFinancialId
		INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
		WHERE pRef.ReferredFromId > 0
			AND (pf.PatientId = pRef.PatientId OR pri.AllowDependents = 1)'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientInsuranceReferrals'
			,@PatientInsuranceReferralsViewSql
			,@isChanged = @isPatientInsuranceReferralsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientInsuranceReferralDelete'
				,
				'
			CREATE TRIGGER model.OnPatientInsuranceReferralDelete ON [model].[PatientInsuranceReferrals] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @TotalEncountersCovered int
			DECLARE @ReferralCode nvarchar(max)
			DECLARE @IsArchived bit
			DECLARE @ExternalProviderId int
			DECLARE @PatientInsuranceId bigint
			DECLARE @PatientId int
			DECLARE @DoctorId int
		
			DECLARE PatientInsuranceReferralsDeletedCursor CURSOR FOR
			SELECT Id, StartDateTime, EndDateTime, TotalEncountersCovered, ReferralCode, IsArchived, ExternalProviderId, PatientInsuranceId, PatientId, DoctorId FROM deleted
		
			OPEN PatientInsuranceReferralsDeletedCursor
			FETCH NEXT FROM PatientInsuranceReferralsDeletedCursor INTO @Id, @StartDateTime, @EndDateTime, @TotalEncountersCovered, @ReferralCode, @IsArchived, @ExternalProviderId, @PatientInsuranceId, @PatientId, @DoctorId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- PatientInsuranceReferral Delete
				DELETE
				FROM dbo.PatientReferral
				WHERE ReferralId = @Id
			
				SELECT @Id AS Id, @StartDateTime AS StartDateTime, @EndDateTime AS EndDateTime, @TotalEncountersCovered AS TotalEncountersCovered, @ReferralCode AS ReferralCode, @IsArchived AS IsArchived, @ExternalProviderId AS ExternalProviderId, @PatientInsuranceId AS PatientInsuranceId, @PatientId AS PatientId, @DoctorId AS DoctorId
			
				FETCH NEXT FROM PatientInsuranceReferralsDeletedCursor INTO @Id, @StartDateTime, @EndDateTime, @TotalEncountersCovered, @ReferralCode, @IsArchived, @ExternalProviderId, @PatientInsuranceId, @PatientId, @DoctorId 
		
			END
		
			CLOSE PatientInsuranceReferralsDeletedCursor
			DEALLOCATE PatientInsuranceReferralsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientInsuranceReferralInsert'
				,
				'
			CREATE TRIGGER model.OnPatientInsuranceReferralInsert ON [model].[PatientInsuranceReferrals] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @TotalEncountersCovered int
			DECLARE @ReferralCode nvarchar(max)
			DECLARE @IsArchived bit
			DECLARE @ExternalProviderId int
			DECLARE @PatientInsuranceId bigint
			DECLARE @PatientId int
			DECLARE @DoctorId int
		
			DECLARE PatientInsuranceReferralsInsertedCursor CURSOR FOR
			SELECT Id, StartDateTime, EndDateTime, TotalEncountersCovered, ReferralCode, IsArchived, ExternalProviderId, PatientInsuranceId, PatientId, DoctorId FROM inserted
		
			OPEN PatientInsuranceReferralsInsertedCursor
			FETCH NEXT FROM PatientInsuranceReferralsInsertedCursor INTO @Id, @StartDateTime, @EndDateTime, @TotalEncountersCovered, @ReferralCode, @IsArchived, @ExternalProviderId, @PatientInsuranceId, @PatientId, @DoctorId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- PatientInsuranceReferral Insert
				INSERT INTO dbo.PatientReferral (Referral)
				VALUES ('''')
			
				SET @Id = IDENT_CURRENT(''PatientReferral'')
			
							
				-- PatientInsuranceReferral Update/Insert
				DECLARE @x int, @y int
			
				SELECT @x = x,
					@y = y
				FROM model.GetIdPair(@PatientId, 202000000)
			
				DECLARE @PatientDemographicsId int 
				SET @PatientDemographicsId = @y
			
				SELECT @x = x,
					@y = y
				FROM model.GetBigIdPair(@PatientInsuranceId, 110000000)
			
				DECLARE @FinancialId int 
				SET @FinancialId = @y
			
				UPDATE dbo.PatientReferral
				SET ReferralExpireDate = CONVERT(nvarchar, @EndDateTime, 112),
					ReferredFromId = @ExternalProviderId,
					STATUS = ''A'',
					PatientId = @PatientDemoGraphicsId,
					ReferredInsurer = (
						SELECT TOP 1 FinancialInsurerId
						FROM dbo.PatientFinancial
						WHERE PatientId = @PatientDemographicsId
							AND FinancialId = @FinancialId
						),
					ReferredTo = @DoctorId,
					Referral = @ReferralCode,
					ReferralVisits = @TotalEncountersCovered,
					ReferralDate = CONVERT(nvarchar, @StartDateTime, 112)
				WHERE ReferralId = @Id
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @StartDateTime AS StartDateTime, @EndDateTime AS EndDateTime, @TotalEncountersCovered AS TotalEncountersCovered, @ReferralCode AS ReferralCode, @IsArchived AS IsArchived, @ExternalProviderId AS ExternalProviderId, @PatientInsuranceId AS PatientInsuranceId, @PatientId AS PatientId, @DoctorId AS DoctorId
				
				FETCH NEXT FROM PatientInsuranceReferralsInsertedCursor INTO @Id, @StartDateTime, @EndDateTime, @TotalEncountersCovered, @ReferralCode, @IsArchived, @ExternalProviderId, @PatientInsuranceId, @PatientId, @DoctorId 
		
			END
		
			CLOSE PatientInsuranceReferralsInsertedCursor
			DEALLOCATE PatientInsuranceReferralsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientInsuranceReferralUpdate'
				,
				'
			CREATE TRIGGER model.OnPatientInsuranceReferralUpdate ON [model].[PatientInsuranceReferrals] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @TotalEncountersCovered int
			DECLARE @ReferralCode nvarchar(max)
			DECLARE @IsArchived bit
			DECLARE @ExternalProviderId int
			DECLARE @PatientInsuranceId bigint
			DECLARE @PatientId int
			DECLARE @DoctorId int
		
			DECLARE PatientInsuranceReferralsUpdatedCursor CURSOR FOR
			SELECT Id, StartDateTime, EndDateTime, TotalEncountersCovered, ReferralCode, IsArchived, ExternalProviderId, PatientInsuranceId, PatientId, DoctorId FROM inserted
		
			OPEN PatientInsuranceReferralsUpdatedCursor
			FETCH NEXT FROM PatientInsuranceReferralsUpdatedCursor INTO @Id, @StartDateTime, @EndDateTime, @TotalEncountersCovered, @ReferralCode, @IsArchived, @ExternalProviderId, @PatientInsuranceId, @PatientId, @DoctorId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- PatientInsuranceReferral Update/Insert
				DECLARE @x int, @y int
			
				SELECT @x = x,
					@y = y
				FROM model.GetIdPair(@PatientId, 202000000)
			
				DECLARE @PatientDemographicsId int 
				SET @PatientDemographicsId = @y
			
				SELECT @x = x,
					@y = y
				FROM model.GetBigIdPair(@PatientInsuranceId, 110000000)
			
				DECLARE @FinancialId int 
				SET @FinancialId = @y
			
				UPDATE dbo.PatientReferral
				SET ReferralExpireDate = CONVERT(nvarchar, @EndDateTime, 112),
					ReferredFromId = @ExternalProviderId,
					STATUS = ''A'',
					PatientId = @PatientDemoGraphicsId,
					ReferredInsurer = (
						SELECT TOP 1 FinancialInsurerId
						FROM dbo.PatientFinancial
						WHERE PatientId = @PatientDemographicsId
							AND FinancialId = @FinancialId
						),
					ReferredTo = @DoctorId,
					Referral = @ReferralCode,
					ReferralVisits = @TotalEncountersCovered,
					ReferralDate = CONVERT(nvarchar, @StartDateTime, 112)
				WHERE ReferralId = @Id
				
				FETCH NEXT FROM PatientInsuranceReferralsUpdatedCursor INTO @Id, @StartDateTime, @EndDateTime, @TotalEncountersCovered, @ReferralCode, @IsArchived, @ExternalProviderId, @PatientInsuranceId, @PatientId, @DoctorId 
		
			END
		
			CLOSE PatientInsuranceReferralsUpdatedCursor
			DEALLOCATE PatientInsuranceReferralsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientInsurances')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientInsurancesChanged BIT

		SET @isPatientInsurancesChanged = 0

		DECLARE @ismodelPatientInsurances_Partition1Changed BIT

		SET @ismodelPatientInsurances_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientInsurances_Partition1'
			,
			'
		CREATE VIEW model.PatientInsurances_Partition1
		WITH SCHEMABINDING
		AS
		SELECT (CONVERT(bigint, 110000000) * pd.PatientId + pr.ComputedFinancialId) AS Id
				,pf.FinancialId AS InsurancePolicyId
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 2
					WHEN ''A''
						THEN 3			
					WHEN ''W''
						THEN 4
					ELSE 1
					END AS InsuranceTypeId
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 200
					WHEN ''A''
						THEN 3000			
					WHEN ''W''
						THEN 40000
					ELSE 10
					END + CASE 
						WHEN pf.PatientId = pd.PolicyPatientId
							THEN 0
						ELSE 3
						END + CONVERT(INT, FinancialPIndicator) AS OrdinalId
				,pd.PatientId AS InsuredPatientId
				,CASE (
						CASE 
							WHEN pd.PolicyPatientId = pf.PatientId
								THEN pd.Relationship
							ELSE pd.SecondRelationship
							END
						)
					WHEN ''Y''
						THEN 1
					WHEN ''S''
						THEN 2
					WHEN ''C''
						THEN 3
					WHEN ''E''
						THEN 4
					WHEN ''L''
						THEN 6
					WHEN ''O''
						THEN 7
					WHEN ''D''
						THEN 7
					WHEN ''P'' 
						THEN 7
					ELSE 5
					END AS PolicyHolderRelationshipTypeId
				,COUNT_BIG(*) AS Count
			FROM dbo.PatientDemographics pd
			INNER JOIN dbo.PatientReceivables pr ON pr.PatientId = pd.PatientId
			INNER JOIN dbo.PatientFinancial pf ON pf.FinancialId = pr.ComputedFinancialId
			INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
			GROUP BY (CONVERT(bigint, 110000000) * pd.PatientId + pr.ComputedFinancialId)
				,pf.FinancialId
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 2
					WHEN ''A''
						THEN 3			
					WHEN ''W''
						THEN 4
					ELSE 1
					END
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 200
					WHEN ''A''
						THEN 3000			
					WHEN ''W''
						THEN 40000
					ELSE 10
					END + CASE 
						WHEN pf.PatientId = pd.PolicyPatientId
							THEN 0
						ELSE 3
						END + CONVERT(INT, FinancialPIndicator)
				,pd.PatientId
				,CASE (
						CASE 
							WHEN pd.PolicyPatientId = pf.PatientId
								THEN pd.Relationship
							ELSE pd.SecondRelationship
							END
						)
					WHEN ''Y''
						THEN 1
					WHEN ''S''
						THEN 2
					WHEN ''C''
						THEN 3
					WHEN ''E''
						THEN 4
					WHEN ''L''
						THEN 6
					WHEN ''O''
						THEN 7
					WHEN ''D''
						THEN 7
					WHEN ''P'' 
						THEN 7
					ELSE 5
					END
		'
			,@isChanged = @ismodelPatientInsurances_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PatientInsurances_Partition1'
					AND object_id = OBJECT_ID('model.PatientInsurances_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PatientInsurances_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_PatientInsurances_Partition1 ON model.PatientInsurances_Partition1 ([Id]);
		END

		DECLARE @ismodelPatientInsurances_Partition2Changed BIT

		SET @ismodelPatientInsurances_Partition2Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientInsurances_Partition2'
			,
			'
		CREATE VIEW model.PatientInsurances_Partition2
		WITH SCHEMABINDING
		AS
		SELECT (CONVERT(bigint, 110000000) * pd.PatientId + pf.FinancialId) AS Id
				,pf.FinancialId AS InsurancePolicyId
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 2
					WHEN ''A''
						THEN 3
					WHEN ''W''
						THEN 4
					ELSE 1
					END AS InsuranceTypeId
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 200
					WHEN ''A''
						THEN 3000
					WHEN ''W''
						THEN 40000
					ELSE 10
					END + CASE 
						WHEN pf.PatientId = pd.PolicyPatientId
							THEN 0
						ELSE 3
						END + CONVERT(INT, FinancialPIndicator) AS OrdinalId
				,pd.PatientId AS InsuredPatientId
				,CASE (
						CASE 
							WHEN pd.PolicyPatientId = pf.PatientId
								THEN pd.Relationship
							ELSE pd.SecondRelationship
							END
						)
					WHEN ''Y''
						THEN 1
					WHEN ''S''
						THEN 2
					WHEN ''C''
						THEN 3
					WHEN ''E''
						THEN 4
					WHEN ''L''
						THEN 6
					WHEN ''O''
						THEN 7
					WHEN ''D''
						THEN 7
					WHEN ''P'' 
						THEN 7
					ELSE 5  
					END AS PolicyHolderRelationshipTypeId
				,pf.FinancialId AS FinancialId
				,COUNT_BIG(*) AS Count
			FROM dbo.PatientDemographics pd
			INNER JOIN dbo.PatientFinancial pf ON pf.PatientId = pd.PolicyPatientId
				OR pf.PatientId = pd.SecondPolicyPatientId
			INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
			WHERE 
				(pf.PatientId = pd.PolicyPatientId OR pf.PatientId = pd.SecondPolicyPatientId)
				AND pf.PatientId = pd.PatientId			
			GROUP BY (CONVERT(bigint, 110000000) * pd.PatientId + pf.FinancialId)
				,pf.FinancialId
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 2
					WHEN ''A''
						THEN 3			
					WHEN ''W''
						THEN 4
					ELSE 1
					END
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 200
					WHEN ''A''
						THEN 3000			
					WHEN ''W''
						THEN 40000
					ELSE 10
					END + CASE 
						WHEN pf.PatientId = pd.PolicyPatientId
							THEN 0
						ELSE 3
						END + CONVERT(INT, FinancialPIndicator)
				,pd.PatientId
				,CASE (
						CASE 
							WHEN pd.PolicyPatientId = pf.PatientId
								THEN pd.Relationship
							ELSE pd.SecondRelationship
							END
						)
					WHEN ''Y''
						THEN 1
					WHEN ''S''
						THEN 2
					WHEN ''C''
						THEN 3
					WHEN ''E''
						THEN 4
					WHEN ''L''
						THEN 6
					WHEN ''O''
						THEN 7
					WHEN ''D''
						THEN 7
					WHEN ''P'' 
						THEN 7
					ELSE 5
					END
				,pf.FinancialId
		'
			,@isChanged = @ismodelPatientInsurances_Partition2Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PatientInsurances_Partition2'
					AND object_id = OBJECT_ID('model.PatientInsurances_Partition2')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PatientInsurances_Partition2'

			CREATE UNIQUE CLUSTERED INDEX PK_PatientInsurances_Partition2 ON model.PatientInsurances_Partition2 ([Id]);
		END

		DECLARE @ismodelPatientInsurances_Partition3Changed BIT

		SET @ismodelPatientInsurances_Partition3Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientInsurances_Partition3'
			,
			'
		CREATE VIEW model.PatientInsurances_Partition3
		WITH SCHEMABINDING
		AS
		SELECT (CONVERT(bigint, 110000000) * pd.PatientId + pf.FinancialId) AS Id
				,pf.FinancialId AS InsurancePolicyId
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 2
					WHEN ''A''
						THEN 3
					WHEN ''W''
						THEN 4
					ELSE 1
					END AS InsuranceTypeId
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 200
					WHEN ''A''
						THEN 3000
					WHEN ''W''
						THEN 40000
					ELSE 10
					END + 0 + CONVERT(INT, FinancialPIndicator) AS OrdinalId
				,pd.PatientId AS InsuredPatientId
				,CASE pd.Relationship
					WHEN ''Y''
						THEN 1
					WHEN ''S''
						THEN 2
					WHEN ''C''
						THEN 3
					WHEN ''E''
						THEN 4
					WHEN ''L''
						THEN 6
					WHEN ''O''
						THEN 7
					WHEN ''D''
						THEN 7
					WHEN ''P'' 
						THEN 7
					ELSE 5  
					END AS PolicyHolderRelationshipTypeId
				,pf.FinancialId AS FinancialId
				,COUNT_BIG(*) AS Count
			FROM dbo.PatientDemographics pd
			INNER JOIN dbo.PatientFinancial pf ON pf.PatientId = pd.PolicyPatientId
			INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
			WHERE 
				pd.PolicyPatientId <> pd.PatientId
				AND pd.PolicyPatientId = pf.PatientId
				AND pd.PolicyPatientId <> 0			
				AND pri.AllowDependents = 1				
			GROUP BY (CONVERT(bigint, 110000000) * pd.PatientId + pf.FinancialId)
				,pf.FinancialId
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 2
					WHEN ''A''
						THEN 3			
					WHEN ''W''
						THEN 4
					ELSE 1
					END
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 200
					WHEN ''A''
						THEN 3000			
					WHEN ''W''
						THEN 40000
					ELSE 10
					END + 0 + CONVERT(INT, FinancialPIndicator)
				,pd.PatientId
				,CASE pd.Relationship
					WHEN ''Y''
						THEN 1
					WHEN ''S''
						THEN 2
					WHEN ''C''
						THEN 3
					WHEN ''E''
						THEN 4
					WHEN ''L''
						THEN 6
					WHEN ''O''
						THEN 7
					WHEN ''D''
						THEN 7
					WHEN ''P'' 
						THEN 7
					ELSE 5
					END
				,pf.FinancialId
		'
			,@isChanged = @ismodelPatientInsurances_Partition3Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PatientInsurances_Partition3'
					AND object_id = OBJECT_ID('model.PatientInsurances_Partition3')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PatientInsurances_Partition3'

			CREATE UNIQUE CLUSTERED INDEX PK_PatientInsurances_Partition3 ON model.PatientInsurances_Partition3 ([Id]);
		END

		DECLARE @ismodelPatientInsurances_Partition4Changed BIT

		SET @ismodelPatientInsurances_Partition4Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientInsurances_Partition4'
			,
			'
		CREATE VIEW model.PatientInsurances_Partition4
		WITH SCHEMABINDING
		AS
		SELECT (CONVERT(bigint, 110000000) * pd.PatientId + pf.FinancialId) AS Id
				,pf.FinancialId AS InsurancePolicyId
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 2
					WHEN ''A''
						THEN 3
					WHEN ''W''
						THEN 4
					ELSE 1
					END AS InsuranceTypeId
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 200
					WHEN ''A''
						THEN 3000
					WHEN ''W''
						THEN 40000
					ELSE 10
					END + 3 + CONVERT(INT, FinancialPIndicator) AS OrdinalId
				,pd.PatientId AS InsuredPatientId
				,CASE pd.SecondRelationship
					WHEN ''Y''
						THEN 1
					WHEN ''S''
						THEN 2
					WHEN ''C''
						THEN 3
					WHEN ''E''
						THEN 4
					WHEN ''L''
						THEN 6
					WHEN ''O''
						THEN 7
					WHEN ''D''
						THEN 7
					WHEN ''P'' 
						THEN 7
					ELSE 5  
					END AS PolicyHolderRelationshipTypeId
				,pf.FinancialId AS FinancialId
				,COUNT_BIG(*) AS Count
			FROM dbo.PatientDemographics pd
			INNER JOIN dbo.PatientFinancial pf ON pf.PatientId = pd.PolicyPatientId
				OR pf.PatientId = pd.SecondPolicyPatientId
			INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
			WHERE pd.SecondPolicyPatientId <> pd.PatientId
				AND pd.SecondPolicyPatientId = pf.PatientId
				AND pd.SecondPolicyPatientId <> 0			
				AND pri.AllowDependents = 1				
			GROUP BY (CONVERT(bigint, 110000000) * pd.PatientId + pf.FinancialId)
				,pf.FinancialId
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 2
					WHEN ''A''
						THEN 3			
					WHEN ''W''
						THEN 4
					ELSE 1
					END
				,CASE pf.FinancialInsType
					WHEN ''V''
						THEN 200
					WHEN ''A''
						THEN 3000			
					WHEN ''W''
						THEN 40000
					ELSE 10
					END + 3 + CONVERT(INT, FinancialPIndicator)
				,pd.PatientId
				,CASE pd.SecondRelationship
					WHEN ''Y''
						THEN 1
					WHEN ''S''
						THEN 2
					WHEN ''C''
						THEN 3
					WHEN ''E''
						THEN 4
					WHEN ''L''
						THEN 6
					WHEN ''O''
						THEN 7
					WHEN ''D''
						THEN 7
					WHEN ''P'' 
						THEN 7
					ELSE 5
					END
				,pf.FinancialId
		'
			,@isChanged = @ismodelPatientInsurances_Partition4Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PatientInsurances_Partition4'
					AND object_id = OBJECT_ID('model.PatientInsurances_Partition4')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PatientInsurances_Partition4'

			CREATE UNIQUE CLUSTERED INDEX PK_PatientInsurances_Partition4 ON model.PatientInsurances_Partition4 ([Id]);
		END

		DECLARE @PatientInsurancesViewSql NVARCHAR(max)

		SET @PatientInsurancesViewSql = 
			'
		CREATE VIEW [model].[PatientInsurances]
		WITH SCHEMABINDING
		AS
		----All policies where a receivable exists
		SELECT v.Id AS Id
			,CONVERT(datetime, NULL) AS EndDateTime
			,v.InsurancePolicyId AS InsurancePolicyId
			,v.InsuranceTypeId AS InsuranceTypeId
			,v.OrdinalId AS OrdinalId
			,v.InsuredPatientId AS InsuredPatientId
			,v.PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId
		FROM (
	
		SELECT model.PatientInsurances_Partition1.Id AS Id, model.PatientInsurances_Partition1.InsurancePolicyId AS InsurancePolicyId, model.PatientInsurances_Partition1.InsuranceTypeId AS InsuranceTypeId, model.PatientInsurances_Partition1.OrdinalId AS OrdinalId, model.PatientInsurances_Partition1.InsuredPatientId AS InsuredPatientId, model.PatientInsurances_Partition1.PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId, model.PatientInsurances_Partition1.Count AS Count FROM model.PatientInsurances_Partition1 WITH(NOEXPAND)
	
		) v
	
		UNION ALL
	
		-------all patient insurance policies where a receivable does not exist
		---Policies where a receivable doesn''t exist; patient is policyholder
		SELECT v.Id AS Id
			,CONVERT(datetime, NULL) AS EndDateTime
			,v.InsurancePolicyId AS InsurancePolicyId
			,v.InsuranceTypeId AS InsuranceTypeId
			,v.OrdinalId AS OrdinalId
			,v.InsuredPatientId AS InsuredPatientId
			,v.PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId
		FROM (
	
		SELECT model.PatientInsurances_Partition2.Id AS Id, model.PatientInsurances_Partition2.InsurancePolicyId AS InsurancePolicyId, model.PatientInsurances_Partition2.InsuranceTypeId AS InsuranceTypeId, model.PatientInsurances_Partition2.OrdinalId AS OrdinalId, model.PatientInsurances_Partition2.InsuredPatientId AS InsuredPatientId, model.PatientInsurances_Partition2.PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId, model.PatientInsurances_Partition2.FinancialId AS FinancialId, model.PatientInsurances_Partition2.Count AS Count FROM model.PatientInsurances_Partition2 WITH(NOEXPAND)
	
		) v
		LEFT JOIN dbo.PatientReceivables pr ON pr.PatientId = v.InsuredPatientId
			AND pr.ComputedFinancialId = v.FinancialId
		WHERE pr.ReceivableId IS NULL
	
		UNION ALL
	
		---all patient insurance policies where a receivable does not exist; first policyholder is not the patient
		SELECT v.Id AS Id
			,CONVERT(datetime, NULL) AS EndDateTime
			,v.InsurancePolicyId AS InsurancePolicyId
			,v.InsuranceTypeId AS InsuranceTypeId
			,v.OrdinalId AS OrdinalId
			,v.InsuredPatientId AS InsuredPatientId
			,v.PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId
		FROM (
	
		SELECT model.PatientInsurances_Partition3.Id AS Id, model.PatientInsurances_Partition3.InsurancePolicyId AS InsurancePolicyId, model.PatientInsurances_Partition3.InsuranceTypeId AS InsuranceTypeId, model.PatientInsurances_Partition3.OrdinalId AS OrdinalId, model.PatientInsurances_Partition3.InsuredPatientId AS InsuredPatientId, model.PatientInsurances_Partition3.PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId, model.PatientInsurances_Partition3.FinancialId AS FinancialId, model.PatientInsurances_Partition3.Count AS Count FROM model.PatientInsurances_Partition3 WITH(NOEXPAND)
	
		) v
		LEFT JOIN dbo.PatientReceivables pr ON pr.PatientId = v.InsuredPatientId
			AND pr.ComputedFinancialId = v.FinancialId
		WHERE pr.ReceivableId IS NULL
	
		UNION ALL
	
		---all patient insurance policies where a receivable does not exist; second policyholder is not patient
		SELECT v.Id AS Id
			,CONVERT(datetime, NULL) AS EndDateTime
			,v.InsurancePolicyId AS InsurancePolicyId
			,v.InsuranceTypeId AS InsuranceTypeId
			,v.OrdinalId AS OrdinalId
			,v.InsuredPatientId AS InsuredPatientId
			,v.PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId
		FROM (
	
		SELECT model.PatientInsurances_Partition4.Id AS Id, model.PatientInsurances_Partition4.InsurancePolicyId AS InsurancePolicyId, model.PatientInsurances_Partition4.InsuranceTypeId AS InsuranceTypeId, model.PatientInsurances_Partition4.OrdinalId AS OrdinalId, model.PatientInsurances_Partition4.InsuredPatientId AS InsuredPatientId, model.PatientInsurances_Partition4.PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId, model.PatientInsurances_Partition4.FinancialId AS FinancialId, model.PatientInsurances_Partition4.Count AS Count FROM model.PatientInsurances_Partition4 WITH(NOEXPAND)
	
		) v
		LEFT JOIN dbo.PatientReceivables pr ON pr.PatientId = v.InsuredPatientId
			AND pr.ComputedFinancialId = v.FinancialId
		WHERE pr.ReceivableId IS NULL'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientInsurances'
			,@PatientInsurancesViewSql
			,@isChanged = @isPatientInsurancesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientInsuranceDelete'
				,
				'
			CREATE TRIGGER model.OnPatientInsuranceDelete ON [model].[PatientInsurances] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @InsuranceTypeId int
			DECLARE @PolicyHolderRelationshipTypeId int
			DECLARE @OrdinalId int
			DECLARE @InsuredPatientId int
			DECLARE @InsurancePolicyId int
			DECLARE @EndDateTime datetime
		
			DECLARE PatientInsurancesDeletedCursor CURSOR FOR
			SELECT Id, InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime FROM deleted
		
			OPEN PatientInsurancesDeletedCursor
			FETCH NEXT FROM PatientInsurancesDeletedCursor INTO @Id, @InsuranceTypeId, @PolicyHolderRelationshipTypeId, @OrdinalId, @InsuredPatientId, @InsurancePolicyId, @EndDateTime 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PatientInsurance --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @InsuranceTypeId AS InsuranceTypeId, @PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId, @OrdinalId AS OrdinalId, @InsuredPatientId AS InsuredPatientId, @InsurancePolicyId AS InsurancePolicyId, @EndDateTime AS EndDateTime
			
				FETCH NEXT FROM PatientInsurancesDeletedCursor INTO @Id, @InsuranceTypeId, @PolicyHolderRelationshipTypeId, @OrdinalId, @InsuredPatientId, @InsurancePolicyId, @EndDateTime 
		
			END
		
			CLOSE PatientInsurancesDeletedCursor
			DEALLOCATE PatientInsurancesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientInsuranceInsert'
				,
				'
			CREATE TRIGGER model.OnPatientInsuranceInsert ON [model].[PatientInsurances] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @InsuranceTypeId int
			DECLARE @PolicyHolderRelationshipTypeId int
			DECLARE @OrdinalId int
			DECLARE @InsuredPatientId int
			DECLARE @InsurancePolicyId int
			DECLARE @EndDateTime datetime
		
			DECLARE PatientInsurancesInsertedCursor CURSOR FOR
			SELECT Id, InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime FROM inserted
		
			OPEN PatientInsurancesInsertedCursor
			FETCH NEXT FROM PatientInsurancesInsertedCursor INTO @Id, @InsuranceTypeId, @PolicyHolderRelationshipTypeId, @OrdinalId, @InsuredPatientId, @InsurancePolicyId, @EndDateTime 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- PatientInsurance Insert
				DECLARE @patientInsuranceIdPair TABLE (
					x int,
					y int
					)
			
				INSERT INTO @patientInsuranceIdPair
				SELECT x,
					y
				FROM model.GetIdPair(@InsuredPatientId, 110000000)
			
				-- @Id is a composite key...so we need to decompose it
				DECLARE @PatientInsuredDemographicsId int 
				SET @PatientInsuredDemographicsId = (SELECT TOP 1 y FROM @patientInsuranceIdPair)
				DECLARE @patientFinancialInsuredPatientId int 
				SET @patientFinancialInsuredPatientId = (SELECT PatientId FROM PatientFinancial WHERE FinancialId = @InsurancePolicyId)
			
				SET @Id = model.GetBigPairId(@PatientInsuredDemographicsId, @InsurancePolicyId, 110000000)
			
							
				-- PatientInsurance Update/Insert
				DECLARE @patientIdPair TABLE (
					x int,
					y int
					)
			
				INSERT INTO @patientIdPair
				SELECT x,
					y
				FROM model.GetIdPair(@InsuredPatientId, 110000000)
			
				-- @Id is a composite key...so we need to decompose it
				DECLARE @InsuredPatientDemographicsId int 
				SET @InsuredPatientDemographicsId = (SELECT TOP 1 y FROM @patientIdPair)
				DECLARE @patientFinancialPatientId int 
				SET @patientFinancialPatientId = (SELECT PatientId FROM PatientFinancial WHERE FinancialId = @InsurancePolicyId)
			
				IF @patientFinancialPatientId = @InsuredPatientDemographicsId
				BEGIN
					UPDATE PatientFinancial
					-- This will get called for Updates AND after the Insert procedure above (an "upsert")
					SET FinancialInsType = CASE @InsuranceTypeId
							WHEN 1
								THEN ''M''
							WHEN 2
								THEN ''V''
							WHEN 3
								THEN ''A''
							WHEN 4
								THEN ''W''
							ELSE ''M''
							END,
						FinancialPIndicator = CASE @InsuranceTypeId
							WHEN 1
								THEN 1
							WHEN 2
								THEN @OrdinalId - 200
							WHEN 3
								THEN @OrdinalId - 3000
							WHEN 4
								THEN @OrdinalId - 40000
							ELSE @OrdinalId -10
							END,
						Status=''C''
					WHERE FinancialId = @InsurancePolicyId;
			
					UPDATE PatientDemographics
					SET Relationship = CASE @PolicyHolderRelationshipTypeId
							WHEN 1
								THEN ''Y''
							WHEN 2
								THEN ''S''
							WHEN 3
								THEN ''C''
							WHEN 4
								THEN ''E''
							WHEN 6
								THEN ''L''
							WHEN 7
								THEN ''O''
							WHEN 11
								THEN ''D''
							WHEN 12
								THEN ''P''
							ELSE ''Y''
							END,
						PolicyPatientId = @InsuredPatientDemographicsId
					WHERE PatientId = @InsuredPatientDemographicsId;
				END
				ELSE
				BEGIN
					UPDATE PatientFinancial
					-- This will get called for Updates AND after the Insert procedure above (an "upsert")
					SET FinancialInsType = CASE @InsuranceTypeId
							WHEN 1
								THEN ''M''
							WHEN 2
								THEN ''V''
							WHEN 3
								THEN ''A''
							WHEN 4
								THEN ''W''
							ELSE ''M''
							END,
						FinancialPIndicator = CASE @InsuranceTypeId
							WHEN 1 
								THEN 1
							WHEN 2
								THEN @OrdinalId - 203
							WHEN 3
								THEN @OrdinalId - 3003
							WHEN 4
								THEN @OrdinalId - 4003
							ELSE @OrdinalId -13
							END,
						Status=''C''
					WHERE FinancialId = @InsurancePolicyId;
			
					UPDATE PatientDemographics
					SET SecondRelationship = CASE @PolicyHolderRelationshipTypeId
							WHEN 1
								THEN ''Y''
							WHEN 2
								THEN ''S''
							WHEN 3
								THEN ''C''
							WHEN 4
								THEN ''E''
							WHEN 6
								THEN ''L''
							WHEN 7
								THEN ''O''
							WHEN 11
								THEN ''D''
							WHEN 12
								THEN ''P''
							ELSE ''Y''
							END,
						SecondPolicyPatientId = @patientFinancialPatientId
					WHERE PatientId = @InsuredPatientDemographicsId;
				END
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @InsuranceTypeId AS InsuranceTypeId, @PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId, @OrdinalId AS OrdinalId, @InsuredPatientId AS InsuredPatientId, @InsurancePolicyId AS InsurancePolicyId, @EndDateTime AS EndDateTime
				
				FETCH NEXT FROM PatientInsurancesInsertedCursor INTO @Id, @InsuranceTypeId, @PolicyHolderRelationshipTypeId, @OrdinalId, @InsuredPatientId, @InsurancePolicyId, @EndDateTime 
		
			END
		
			CLOSE PatientInsurancesInsertedCursor
			DEALLOCATE PatientInsurancesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientInsuranceUpdate'
				,
				'
			CREATE TRIGGER model.OnPatientInsuranceUpdate ON [model].[PatientInsurances] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @InsuranceTypeId int
			DECLARE @PolicyHolderRelationshipTypeId int
			DECLARE @OrdinalId int
			DECLARE @InsuredPatientId int
			DECLARE @InsurancePolicyId int
			DECLARE @EndDateTime datetime
		
			DECLARE PatientInsurancesUpdatedCursor CURSOR FOR
			SELECT Id, InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime FROM inserted
		
			OPEN PatientInsurancesUpdatedCursor
			FETCH NEXT FROM PatientInsurancesUpdatedCursor INTO @Id, @InsuranceTypeId, @PolicyHolderRelationshipTypeId, @OrdinalId, @InsuredPatientId, @InsurancePolicyId, @EndDateTime 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- PatientInsurance Update/Insert
				DECLARE @patientIdPair TABLE (
					x int,
					y int
					)
			
				INSERT INTO @patientIdPair
				SELECT x,
					y
				FROM model.GetIdPair(@InsuredPatientId, 110000000)
			
				-- @Id is a composite key...so we need to decompose it
				DECLARE @InsuredPatientDemographicsId int 
				SET @InsuredPatientDemographicsId = (SELECT TOP 1 y FROM @patientIdPair)
				DECLARE @patientFinancialPatientId int 
				SET @patientFinancialPatientId = (SELECT PatientId FROM PatientFinancial WHERE FinancialId = @InsurancePolicyId)
			
				IF @patientFinancialPatientId = @InsuredPatientDemographicsId
				BEGIN
					UPDATE PatientFinancial
					-- This will get called for Updates AND after the Insert procedure above (an "upsert")
					SET FinancialInsType = CASE @InsuranceTypeId
							WHEN 1
								THEN ''M''
							WHEN 2
								THEN ''V''
							WHEN 3
								THEN ''A''
							WHEN 4
								THEN ''W''
							ELSE ''M''
							END,
						FinancialPIndicator = CASE @InsuranceTypeId
							WHEN 1
								THEN 1
							WHEN 2
								THEN @OrdinalId - 200
							WHEN 3
								THEN @OrdinalId - 3000
							WHEN 4
								THEN @OrdinalId - 40000
							ELSE @OrdinalId -10
							END,
						Status=''C''
					WHERE FinancialId = @InsurancePolicyId;
			
					UPDATE PatientDemographics
					SET Relationship = CASE @PolicyHolderRelationshipTypeId
							WHEN 1
								THEN ''Y''
							WHEN 2
								THEN ''S''
							WHEN 3
								THEN ''C''
							WHEN 4
								THEN ''E''
							WHEN 6
								THEN ''L''
							WHEN 7
								THEN ''O''
							WHEN 11
								THEN ''D''
							WHEN 12
								THEN ''P''
							ELSE ''Y''
							END,
						PolicyPatientId = @InsuredPatientDemographicsId
					WHERE PatientId = @InsuredPatientDemographicsId;
				END
				ELSE
				BEGIN
					UPDATE PatientFinancial
					-- This will get called for Updates AND after the Insert procedure above (an "upsert")
					SET FinancialInsType = CASE @InsuranceTypeId
							WHEN 1
								THEN ''M''
							WHEN 2
								THEN ''V''
							WHEN 3
								THEN ''A''
							WHEN 4
								THEN ''W''
							ELSE ''M''
							END,
						FinancialPIndicator = CASE @InsuranceTypeId
							WHEN 1 
								THEN 1
							WHEN 2
								THEN @OrdinalId - 203
							WHEN 3
								THEN @OrdinalId - 3003
							WHEN 4
								THEN @OrdinalId - 4003
							ELSE @OrdinalId -13
							END,
						Status=''C''
					WHERE FinancialId = @InsurancePolicyId;
			
					UPDATE PatientDemographics
					SET SecondRelationship = CASE @PolicyHolderRelationshipTypeId
							WHEN 1
								THEN ''Y''
							WHEN 2
								THEN ''S''
							WHEN 3
								THEN ''C''
							WHEN 4
								THEN ''E''
							WHEN 6
								THEN ''L''
							WHEN 7
								THEN ''O''
							WHEN 11
								THEN ''D''
							WHEN 12
								THEN ''P''
							ELSE ''Y''
							END,
						SecondPolicyPatientId = @patientFinancialPatientId
					WHERE PatientId = @InsuredPatientDemographicsId;
				END
				
				FETCH NEXT FROM PatientInsurancesUpdatedCursor INTO @Id, @InsuranceTypeId, @PolicyHolderRelationshipTypeId, @OrdinalId, @InsuredPatientId, @InsurancePolicyId, @EndDateTime 
		
			END
		
			CLOSE PatientInsurancesUpdatedCursor
			DEALLOCATE PatientInsurancesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientLanguages')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientLanguagesChanged BIT

		SET @isPatientLanguagesChanged = 0

		DECLARE @PatientLanguagesViewSql NVARCHAR(max)

		SET @PatientLanguagesViewSql = '
		CREATE VIEW [model].[PatientLanguages_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id,
			SUBSTRING(Code, 1, 10) AS [Name],
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''LANGUAGE'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientLanguages_Internal'
			,@PatientLanguagesViewSql
			,@isChanged = @isPatientLanguagesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @PatientLanguagesViewSql = '	
			CREATE VIEW [model].[PatientLanguages]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, IsArchived FROM [model].[PatientLanguages_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_PatientLanguages'
							AND object_id = OBJECT_ID('[model].[PatientLanguages_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[PatientLanguages_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_PatientLanguages] ON [model].[PatientLanguages_Internal] ([Id]);
				END

				SET @PatientLanguagesViewSql = @PatientLanguagesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientLanguages'
				,@PatientLanguagesViewSql
				,@isChanged = @isPatientLanguagesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientLanguageDelete'
				,'
			CREATE TRIGGER model.OnPatientLanguageDelete ON [model].[PatientLanguages] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientLanguagesDeletedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM deleted
		
			OPEN PatientLanguagesDeletedCursor
			FETCH NEXT FROM PatientLanguagesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- PatientLanguage Delete
				DELETE
				FROM dbo.PracticeCodeTable
				WHERE Id = @Id
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
			
				FETCH NEXT FROM PatientLanguagesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientLanguagesDeletedCursor
			DEALLOCATE PatientLanguagesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientLanguageInsert'
				,
				'
			CREATE TRIGGER model.OnPatientLanguageInsert ON [model].[PatientLanguages] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientLanguagesInsertedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN PatientLanguagesInsertedCursor
			FETCH NEXT FROM PatientLanguagesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- PatientLanguage Insert
				DECLARE @checkExists int
			
				SELECT @checkExists = COUNT(*)
				FROM dbo.PracticeCodeTable
				WHERE ReferenceType = ''LANGUAGE''
					AND Code = @Name
			
				IF @checkExists = 0
				BEGIN
					INSERT INTO dbo.PracticeCodeTable (ReferenceType)
					VALUES (''LANGUAGE'')
			
					SET @Id = IDENT_CURRENT(''PracticeCodeTable'')
				END
				ELSE
				BEGIN
					SELECT @Id = Id
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''LANGUAGE''
						AND Code = @Name
				END
			
							
				-- PatientLanguage Update/Insert
				UPDATE dbo.PracticeCodeTable
				SET Code = @Name
				WHERE ReferenceType = ''LANGUAGE''
					AND Id = @Id
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
				
				FETCH NEXT FROM PatientLanguagesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientLanguagesInsertedCursor
			DEALLOCATE PatientLanguagesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientLanguageUpdate'
				,'
			CREATE TRIGGER model.OnPatientLanguageUpdate ON [model].[PatientLanguages] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientLanguagesUpdatedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN PatientLanguagesUpdatedCursor
			FETCH NEXT FROM PatientLanguagesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- PatientLanguage Update/Insert
				UPDATE dbo.PracticeCodeTable
				SET Code = @Name
				WHERE ReferenceType = ''LANGUAGE''
					AND Id = @Id
				
				FETCH NEXT FROM PatientLanguagesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientLanguagesUpdatedCursor
			DEALLOCATE PatientLanguagesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientPatientType')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientPatientTypeChanged BIT

		SET @isPatientPatientTypeChanged = 0

		DECLARE @PatientPatientTypeViewSql NVARCHAR(max)

		SET @PatientPatientTypeViewSql = '
		CREATE VIEW [model].[PatientPatientType_Internal]
		WITH SCHEMABINDING
		AS
		SELECT PatientId AS Patients_Id,
			pct.Id AS PatientTypes_Id
		FROM dbo.PatientDemographics pd
		INNER JOIN dbo.PracticeCodeTable pct ON SUBSTRING(pct.Code, 1, (dbo.GetMax((CHARINDEX('' '', Code)),0))) = pd.PatType
			AND ReferenceType = ''PATIENTTYPE''
		WHERE pd.PatType <> ''''
			AND pd.PatType IS NOT NULL'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientPatientType_Internal'
			,@PatientPatientTypeViewSql
			,@isChanged = @isPatientPatientTypeChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @PatientPatientTypeViewSql = '	
			CREATE VIEW [model].[PatientPatientType]
			WITH SCHEMABINDING
			AS
			SELECT Patients_Id, PatientTypes_Id FROM [model].[PatientPatientType_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_PatientPatientType'
							AND object_id = OBJECT_ID('[model].[PatientPatientType_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[PatientPatientType_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_PatientPatientType] ON [model].[PatientPatientType_Internal] (
						[Patients_Id]
						,[PatientTypes_Id]
						);
				END

				SET @PatientPatientTypeViewSql = @PatientPatientTypeViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientPatientType'
				,@PatientPatientTypeViewSql
				,@isChanged = @isPatientPatientTypeChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientPatientTypeDelete'
				,'
			CREATE TRIGGER model.OnPatientPatientTypeDelete ON [model].[PatientPatientType] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Patients_Id int
			DECLARE @PatientTypes_Id int
		
			DECLARE PatientPatientTypeDeletedCursor CURSOR FOR
			SELECT Patients_Id, PatientTypes_Id FROM deleted
		
			OPEN PatientPatientTypeDeletedCursor
			FETCH NEXT FROM PatientPatientTypeDeletedCursor INTO @Patients_Id, @PatientTypes_Id 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PatientPatientType --
				PRINT (''Not Implemented'')
			
				SELECT @Patients_Id AS Patients_Id, @PatientTypes_Id AS PatientTypes_Id
			
				FETCH NEXT FROM PatientPatientTypeDeletedCursor INTO @Patients_Id, @PatientTypes_Id 
		
			END
		
			CLOSE PatientPatientTypeDeletedCursor
			DEALLOCATE PatientPatientTypeDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientPatientTypeInsert'
				,'
			CREATE TRIGGER model.OnPatientPatientTypeInsert ON [model].[PatientPatientType] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Patients_Id int
			DECLARE @PatientTypes_Id int
		
			DECLARE PatientPatientTypeInsertedCursor CURSOR FOR
			SELECT Patients_Id, PatientTypes_Id FROM inserted
		
			OPEN PatientPatientTypeInsertedCursor
			FETCH NEXT FROM PatientPatientTypeInsertedCursor INTO @Patients_Id, @PatientTypes_Id 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for PatientPatientType --
				PRINT (''Not Implemented'')
			
				SET @Patients_Id = CONVERT(int, NULL)
				SET @PatientTypes_Id = CONVERT(int, NULL)
			
				SELECT @Patients_Id AS Patients_Id, @PatientTypes_Id AS PatientTypes_Id
				
				FETCH NEXT FROM PatientPatientTypeInsertedCursor INTO @Patients_Id, @PatientTypes_Id 
		
			END
		
			CLOSE PatientPatientTypeInsertedCursor
			DEALLOCATE PatientPatientTypeInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientPatientTypeUpdate'
				,'
			CREATE TRIGGER model.OnPatientPatientTypeUpdate ON [model].[PatientPatientType] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Patients_Id int
			DECLARE @PatientTypes_Id int
		
			DECLARE PatientPatientTypeUpdatedCursor CURSOR FOR
			SELECT Patients_Id, PatientTypes_Id FROM inserted
		
			OPEN PatientPatientTypeUpdatedCursor
			FETCH NEXT FROM PatientPatientTypeUpdatedCursor INTO @Patients_Id, @PatientTypes_Id 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for PatientPatientType --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM PatientPatientTypeUpdatedCursor INTO @Patients_Id, @PatientTypes_Id 
		
			END
		
			CLOSE PatientPatientTypeUpdatedCursor
			DEALLOCATE PatientPatientTypeUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientQuestionSets')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientQuestionSetsChanged BIT

		SET @isPatientQuestionSetsChanged = 0

		DECLARE @PatientQuestionSetsViewSql NVARCHAR(max)

		SET @PatientQuestionSetsViewSql = '
		CREATE VIEW [model].[PatientQuestionSets_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id AS Id,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived,
			CASE 
				WHEN [rank] IS NOT NULL
					THEN rank
				ELSE 0
				END AS Ordinal
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''QUESTIONSETS'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientQuestionSets_Internal'
			,@PatientQuestionSetsViewSql
			,@isChanged = @isPatientQuestionSetsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @PatientQuestionSetsViewSql = '	
			CREATE VIEW [model].[PatientQuestionSets]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name FROM [model].[PatientQuestionSets_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_PatientQuestionSets'
							AND object_id = OBJECT_ID('[model].[PatientQuestionSets_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[PatientQuestionSets_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_PatientQuestionSets] ON [model].[PatientQuestionSets_Internal] ([Id]);
				END

				SET @PatientQuestionSetsViewSql = @PatientQuestionSetsViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientQuestionSets'
				,@PatientQuestionSetsViewSql
				,@isChanged = @isPatientQuestionSetsChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientQuestionSetDelete'
				,'
			CREATE TRIGGER model.OnPatientQuestionSetDelete ON [model].[PatientQuestionSets] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
		
			DECLARE PatientQuestionSetsDeletedCursor CURSOR FOR
			SELECT Id, Name FROM deleted
		
			OPEN PatientQuestionSetsDeletedCursor
			FETCH NEXT FROM PatientQuestionSetsDeletedCursor INTO @Id, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PatientQuestionSet --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name
			
				FETCH NEXT FROM PatientQuestionSetsDeletedCursor INTO @Id, @Name 
		
			END
		
			CLOSE PatientQuestionSetsDeletedCursor
			DEALLOCATE PatientQuestionSetsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientQuestionSetInsert'
				,'
			CREATE TRIGGER model.OnPatientQuestionSetInsert ON [model].[PatientQuestionSets] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
		
			DECLARE PatientQuestionSetsInsertedCursor CURSOR FOR
			SELECT Id, Name FROM inserted
		
			OPEN PatientQuestionSetsInsertedCursor
			FETCH NEXT FROM PatientQuestionSetsInsertedCursor INTO @Id, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for PatientQuestionSet --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name
				
				FETCH NEXT FROM PatientQuestionSetsInsertedCursor INTO @Id, @Name 
		
			END
		
			CLOSE PatientQuestionSetsInsertedCursor
			DEALLOCATE PatientQuestionSetsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientQuestionSetUpdate'
				,'
			CREATE TRIGGER model.OnPatientQuestionSetUpdate ON [model].[PatientQuestionSets] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
		
			DECLARE PatientQuestionSetsUpdatedCursor CURSOR FOR
			SELECT Id, Name FROM inserted
		
			OPEN PatientQuestionSetsUpdatedCursor
			FETCH NEXT FROM PatientQuestionSetsUpdatedCursor INTO @Id, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for PatientQuestionSet --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM PatientQuestionSetsUpdatedCursor INTO @Id, @Name 
		
			END
		
			CLOSE PatientQuestionSetsUpdatedCursor
			DEALLOCATE PatientQuestionSetsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientRaces')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientRacesChanged BIT

		SET @isPatientRacesChanged = 0

		DECLARE @PatientRacesViewSql NVARCHAR(max)

		SET @PatientRacesViewSql = '
		CREATE VIEW [model].[PatientRaces_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''RACE'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientRaces_Internal'
			,@PatientRacesViewSql
			,@isChanged = @isPatientRacesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @PatientRacesViewSql = '	
			CREATE VIEW [model].[PatientRaces]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, IsArchived FROM [model].[PatientRaces_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_PatientRaces'
							AND object_id = OBJECT_ID('[model].[PatientRaces_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[PatientRaces_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_PatientRaces] ON [model].[PatientRaces_Internal] ([Id]);
				END

				SET @PatientRacesViewSql = @PatientRacesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientRaces'
				,@PatientRacesViewSql
				,@isChanged = @isPatientRacesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientRaceDelete'
				,'
			CREATE TRIGGER model.OnPatientRaceDelete ON [model].[PatientRaces] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientRacesDeletedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM deleted
		
			OPEN PatientRacesDeletedCursor
			FETCH NEXT FROM PatientRacesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- PatientRace Delete
				DELETE
				FROM dbo.PracticeCodeTable
				WHERE Id = @Id
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
			
				FETCH NEXT FROM PatientRacesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientRacesDeletedCursor
			DEALLOCATE PatientRacesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientRaceInsert'
				,
				'
			CREATE TRIGGER model.OnPatientRaceInsert ON [model].[PatientRaces] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientRacesInsertedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN PatientRacesInsertedCursor
			FETCH NEXT FROM PatientRacesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- PatientRace Insert
				DECLARE @checkExists int
			
				SELECT @checkExists = COUNT(*)
				FROM dbo.PracticeCodeTable
				WHERE ReferenceType = ''RACE''
					AND Code = @Name
			
				IF @checkExists = 0
				BEGIN
					INSERT INTO dbo.PracticeCodeTable (ReferenceType)
					VALUES (''RACE'')
			
					SET @Id = IDENT_CURRENT(''PracticeCodeTable'')
				END
				ELSE
				BEGIN
					SELECT @Id = Id
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''RACE''
						AND Code = @Name
				END
			
							
				-- PatientRace Update/Insert
				UPDATE dbo.PracticeCodeTable
				SET Code = @Name
				WHERE ReferenceType = ''RACE''
					AND Id = @Id
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
				
				FETCH NEXT FROM PatientRacesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientRacesInsertedCursor
			DEALLOCATE PatientRacesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientRaceUpdate'
				,'
			CREATE TRIGGER model.OnPatientRaceUpdate ON [model].[PatientRaces] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientRacesUpdatedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN PatientRacesUpdatedCursor
			FETCH NEXT FROM PatientRacesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- PatientRace Update/Insert
				UPDATE dbo.PracticeCodeTable
				SET Code = @Name
				WHERE ReferenceType = ''RACE''
					AND Id = @Id
				
				FETCH NEXT FROM PatientRacesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientRacesUpdatedCursor
			DEALLOCATE PatientRacesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientReferralSources')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientReferralSourcesChanged BIT

		SET @isPatientReferralSourcesChanged = 0

		DECLARE @PatientReferralSourcesViewSql NVARCHAR(max)

		SET @PatientReferralSourcesViewSql = '
		CREATE VIEW [model].[PatientReferralSources_Internal]
		WITH SCHEMABINDING
		AS
		SELECT 	PatientId AS Id,
		PatientId AS PatientId,
			pct.Id AS ReferralSourceTypeId
		FROM dbo.PatientDemographics pd
		INNER JOIN dbo.PracticeCodeTable pct ON pct.Code = pd.ReferralCatagory
			AND ReferenceType = ''REFERBY'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientReferralSources_Internal'
			,@PatientReferralSourcesViewSql
			,@isChanged = @isPatientReferralSourcesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @PatientReferralSourcesViewSql = '	
			CREATE VIEW [model].[PatientReferralSources]
			WITH SCHEMABINDING
			AS
			SELECT Id, ReferralSourceTypeId, PatientId FROM [model].[PatientReferralSources_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_PatientReferralSources'
							AND object_id = OBJECT_ID('[model].[PatientReferralSources_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[PatientReferralSources_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_PatientReferralSources] ON [model].[PatientReferralSources_Internal] ([Id]);
				END

				SET @PatientReferralSourcesViewSql = @PatientReferralSourcesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientReferralSources'
				,@PatientReferralSourcesViewSql
				,@isChanged = @isPatientReferralSourcesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientReferralSourceDelete'
				,'
			CREATE TRIGGER model.OnPatientReferralSourceDelete ON [model].[PatientReferralSources] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @ReferralSourceTypeId int
			DECLARE @PatientId int
		
			DECLARE PatientReferralSourcesDeletedCursor CURSOR FOR
			SELECT Id, ReferralSourceTypeId, PatientId FROM deleted
		
			OPEN PatientReferralSourcesDeletedCursor
			FETCH NEXT FROM PatientReferralSourcesDeletedCursor INTO @Id, @ReferralSourceTypeId, @PatientId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PatientReferralSource --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @ReferralSourceTypeId AS ReferralSourceTypeId, @PatientId AS PatientId
			
				FETCH NEXT FROM PatientReferralSourcesDeletedCursor INTO @Id, @ReferralSourceTypeId, @PatientId 
		
			END
		
			CLOSE PatientReferralSourcesDeletedCursor
			DEALLOCATE PatientReferralSourcesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientReferralSourceInsert'
				,'
			CREATE TRIGGER model.OnPatientReferralSourceInsert ON [model].[PatientReferralSources] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @ReferralSourceTypeId int
			DECLARE @PatientId int
		
			DECLARE PatientReferralSourcesInsertedCursor CURSOR FOR
			SELECT Id, ReferralSourceTypeId, PatientId FROM inserted
		
			OPEN PatientReferralSourcesInsertedCursor
			FETCH NEXT FROM PatientReferralSourcesInsertedCursor INTO @Id, @ReferralSourceTypeId, @PatientId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for PatientReferralSource --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @ReferralSourceTypeId AS ReferralSourceTypeId, @PatientId AS PatientId
				
				FETCH NEXT FROM PatientReferralSourcesInsertedCursor INTO @Id, @ReferralSourceTypeId, @PatientId 
		
			END
		
			CLOSE PatientReferralSourcesInsertedCursor
			DEALLOCATE PatientReferralSourcesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientReferralSourceUpdate'
				,'
			CREATE TRIGGER model.OnPatientReferralSourceUpdate ON [model].[PatientReferralSources] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @ReferralSourceTypeId int
			DECLARE @PatientId int
		
			DECLARE PatientReferralSourcesUpdatedCursor CURSOR FOR
			SELECT Id, ReferralSourceTypeId, PatientId FROM inserted
		
			OPEN PatientReferralSourcesUpdatedCursor
			FETCH NEXT FROM PatientReferralSourcesUpdatedCursor INTO @Id, @ReferralSourceTypeId, @PatientId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for PatientReferralSource --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM PatientReferralSourcesUpdatedCursor INTO @Id, @ReferralSourceTypeId, @PatientId 
		
			END
		
			CLOSE PatientReferralSourcesUpdatedCursor
			DEALLOCATE PatientReferralSourcesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientReferralSourceTypes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientReferralSourceTypesChanged BIT

		SET @isPatientReferralSourceTypesChanged = 0

		DECLARE @PatientReferralSourceTypesViewSql NVARCHAR(max)

		SET @PatientReferralSourceTypesViewSql = '
		CREATE VIEW [model].[PatientReferralSourceTypes_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''REFERBY'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientReferralSourceTypes_Internal'
			,@PatientReferralSourceTypesViewSql
			,@isChanged = @isPatientReferralSourceTypesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @PatientReferralSourceTypesViewSql = '	
			CREATE VIEW [model].[PatientReferralSourceTypes]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, IsArchived FROM [model].[PatientReferralSourceTypes_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_PatientReferralSourceTypes'
							AND object_id = OBJECT_ID('[model].[PatientReferralSourceTypes_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[PatientReferralSourceTypes_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_PatientReferralSourceTypes] ON [model].[PatientReferralSourceTypes_Internal] ([Id]);
				END

				SET @PatientReferralSourceTypesViewSql = @PatientReferralSourceTypesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientReferralSourceTypes'
				,@PatientReferralSourceTypesViewSql
				,@isChanged = @isPatientReferralSourceTypesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientReferralSourceTypeDelete'
				,'
			CREATE TRIGGER model.OnPatientReferralSourceTypeDelete ON [model].[PatientReferralSourceTypes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientReferralSourceTypesDeletedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM deleted
		
			OPEN PatientReferralSourceTypesDeletedCursor
			FETCH NEXT FROM PatientReferralSourceTypesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PatientReferralSourceType --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
			
				FETCH NEXT FROM PatientReferralSourceTypesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientReferralSourceTypesDeletedCursor
			DEALLOCATE PatientReferralSourceTypesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientReferralSourceTypeInsert'
				,'
			CREATE TRIGGER model.OnPatientReferralSourceTypeInsert ON [model].[PatientReferralSourceTypes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientReferralSourceTypesInsertedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN PatientReferralSourceTypesInsertedCursor
			FETCH NEXT FROM PatientReferralSourceTypesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for PatientReferralSourceType --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
				
				FETCH NEXT FROM PatientReferralSourceTypesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientReferralSourceTypesInsertedCursor
			DEALLOCATE PatientReferralSourceTypesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientReferralSourceTypeUpdate'
				,'
			CREATE TRIGGER model.OnPatientReferralSourceTypeUpdate ON [model].[PatientReferralSourceTypes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientReferralSourceTypesUpdatedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN PatientReferralSourceTypesUpdatedCursor
			FETCH NEXT FROM PatientReferralSourceTypesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for PatientReferralSourceType --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM PatientReferralSourceTypesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientReferralSourceTypesUpdatedCursor
			DEALLOCATE PatientReferralSourceTypesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Patients')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientsChanged BIT

		SET @isPatientsChanged = 0

		DECLARE @ismodelPatients_Partition1Changed BIT

		SET @ismodelPatients_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Patients_Partition1'
			,
			'
		CREATE VIEW model.Patients_Partition1
		WITH SCHEMABINDING
		AS
		SELECT PatientId AS Id,
				FirstName AS FirstName,
				CONVERT(nvarchar, NULL) AS Honorific,
				CASE pd.Status
					WHEN ''I''
						THEN CONVERT(bit, 0)
					ELSE CONVERT(bit, 1)
					END AS IsClinical,
				LastName AS LastName,
				MiddleInitial AS MiddleName,
				CONVERT(nvarchar, NULL) AS NickName,
				Salutation AS Prefix,
				CONVERT(nvarchar, NULL) AS Salutation,
				NameReference AS Suffix,
				CASE WHEN BirthDate = '''' THEN NULL ELSE CONVERT(datetime,BirthDate, 112) END AS DateOfBirth,
				CONVERT(datetime, NULL, 112) AS DateOfDeath,
				CASE pd.Gender
					WHEN ''M''
						THEN 3
					WHEN ''F''
						THEN 2
					WHEN ''U''
						THEN 1
					ELSE 4
					END AS GenderId,
				CASE pd.Marital
					WHEN ''S''
						THEN 1
					WHEN ''M''
						THEN 2
					WHEN ''D''
						THEN 3
					WHEN ''W''
						THEN 4
					ELSE NULL
					END AS MaritalStatusId,
				pd.Occupation AS Occupation,
				pd.BusinessType AS BusinessType,
				pd.Ethnicity AS Ethnicity,
				pd.[Language] AS [Language],
				NULL AS PatientNationalOriginId,
				pd.Race AS Race,
				NULL AS PatientReligionId,
				CASE pd.Status
					WHEN ''C''
						THEN 2
					WHEN ''D''
						THEN 3
					WHEN ''M''
						THEN 4
					ELSE 1
					END	AS PatientStatusId,
				CONVERT(nvarchar, NULL) AS PriorFirstName,
				CONVERT(nvarchar, NULL) AS PriorLastName,
				pd.OldPatient AS PriorPatientCode,
				SocialSecurity AS SocialSecurityNumber
			FROM dbo.PatientDemographics pd
		'
			,@isChanged = @ismodelPatients_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_Patients_Partition1'
					AND object_id = OBJECT_ID('model.Patients_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.Patients_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_Patients_Partition1 ON model.Patients_Partition1 ([Id]);
		END

		DECLARE @ismodelPatients_Partition2Changed BIT

		SET @ismodelPatients_Partition2Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Patients_Partition2'
			,
			'
		CREATE VIEW model.Patients_Partition2
		WITH SCHEMABINDING
		AS
		---emergency contact
			SELECT  (202000000 * 1 + PatientId) AS Id,
				SUBSTRING(EmergencyName, 1, (dbo.GetMax((CHARINDEX('' '', emergencyname)),0))) AS FirstName,
				CONVERT(nvarchar, NULL) AS Honorific,
				CONVERT(bit, 0) AS IsClinical,
				SUBSTRING(EmergencyName, LEN(EmergencyName) - (dbo.GetMax((CHARINDEX('' '', REVERSE(EmergencyName)) + 2),0)), LEN(EmergencyName)) AS LastName,
				CONVERT(nvarchar, NULL) AS MiddleName,
				CONVERT(nvarchar, NULL) AS NickName,
				CONVERT(nvarchar, NULL) AS Prefix,
				CONVERT(nvarchar, NULL) AS Salutation,
				CONVERT(nvarchar, NULL) AS Suffix,
				CONVERT(datetime, NULL, 112) AS DateOfBirth,
				CONVERT(datetime, NULL, 112) AS DateOfDeath,
				CONVERT(int, NULL) AS GenderId,
				CONVERT(int, NULL) AS MaritalStatusId,
				CONVERT(nvarchar, NULL) AS Occupation,
				CONVERT(nvarchar, NULL) AS BusinessType,
				CONVERT(nvarchar, NULL) AS Ethnicity,
				CONVERT(nvarchar, NULL) AS Language,
				CONVERT(int, NULL) AS PatientNationalOriginId,
				CONVERT(nvarchar, NULL) AS Race,
				CONVERT(int, NULL) AS PatientReligionId,
				1 as PatientStatusId,
				CONVERT(nvarchar, NULL) AS PriorFirstName,
				CONVERT(nvarchar, NULL) AS PriorLastName,
				CONVERT(nvarchar, NULL) AS PriorPatientCode,
				CONVERT(nvarchar, NULL) AS SocialSecurityNumber
			FROM dbo.PatientDemographics pd
			WHERE EmergencyName <> ''''
				AND EmergencyName IS NOT NULL
		'
			,@isChanged = @ismodelPatients_Partition2Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_Patients_Partition2'
					AND object_id = OBJECT_ID('model.Patients_Partition2')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.Patients_Partition2'

			CREATE UNIQUE CLUSTERED INDEX PK_Patients_Partition2 ON model.Patients_Partition2 ([Id]);
		END

		DECLARE @PatientsViewSql NVARCHAR(max)

		SET @PatientsViewSql = 
			'
		CREATE VIEW [model].[Patients]
		WITH SCHEMABINDING
		AS
		----We have the pd.BillingOffice field in PatientDemographics for BillingOrganization, but it''s always 0 or NULL, so I''m just pulling the main office out of PracticeName table without bothering to analyze value in pd.BillingOffice.
		SELECT q.Id, FirstName, 
				Honorific, 
				IsClinical, 
				LastName, 
				MiddleName, 
				NickName, 
				Prefix, 
				Salutation, 
				Suffix, 
				pnMainOffice.PracticeId AS BillingOrganizationId, 
				DateOfBirth, 
				DateOfDeath, 
				GenderId, 
				MaritalStatusId, 
				Occupation, 
				pctEmployment.Id AS PatientEmploymentStatusId,
				pctEthnicity.Id AS PatientEthnicityId,
				pctLanguage.Id AS PatientLanguageId,
				PatientNationalOriginId,
				pctRace.Id AS PatientRaceid,
				PatientReligionId,
				PatientStatusId,
				PriorFirstName,
				PriorLastName,
				PriorPatientCode,
				SocialSecurityNumber
		FROM (
	
		SELECT model.Patients_Partition1.Id AS Id, model.Patients_Partition1.FirstName AS FirstName, model.Patients_Partition1.Honorific AS Honorific, model.Patients_Partition1.IsClinical AS IsClinical, model.Patients_Partition1.LastName AS LastName, model.Patients_Partition1.MiddleName AS MiddleName, model.Patients_Partition1.NickName AS NickName, model.Patients_Partition1.Prefix AS Prefix, model.Patients_Partition1.Salutation AS Salutation, model.Patients_Partition1.Suffix AS Suffix, model.Patients_Partition1.DateOfBirth AS DateOfBirth, model.Patients_Partition1.DateOfDeath AS DateOfDeath, model.Patients_Partition1.GenderId AS GenderId, model.Patients_Partition1.MaritalStatusId AS MaritalStatusId, model.Patients_Partition1.Occupation AS Occupation, model.Patients_Partition1.BusinessType AS BusinessType, model.Patients_Partition1.Ethnicity AS Ethnicity, model.Patients_Partition1.[Language] AS [Language], model.Patients_Partition1.PatientNationalOriginId AS PatientNationalOriginId, model.Patients_Partition1.Race AS Race, model.Patients_Partition1.PatientReligionId AS PatientReligionId, model.Patients_Partition1.PatientStatusId AS PatientStatusId, model.Patients_Partition1.PriorFirstName AS PriorFirstName, model.Patients_Partition1.PriorLastName AS PriorLastName, model.Patients_Partition1.PriorPatientCode AS PriorPatientCode, model.Patients_Partition1.SocialSecurityNumber AS SocialSecurityNumber FROM model.Patients_Partition1 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.Patients_Partition2.Id AS Id, model.Patients_Partition2.FirstName AS FirstName, model.Patients_Partition2.Honorific AS Honorific, model.Patients_Partition2.IsClinical AS IsClinical, model.Patients_Partition2.LastName AS LastName, model.Patients_Partition2.MiddleName AS MiddleName, model.Patients_Partition2.NickName AS NickName, model.Patients_Partition2.Prefix AS Prefix, model.Patients_Partition2.Salutation AS Salutation, model.Patients_Partition2.Suffix AS Suffix, model.Patients_Partition2.DateOfBirth AS DateOfBirth, model.Patients_Partition2.DateOfDeath AS DateOfDeath, model.Patients_Partition2.GenderId AS GenderId, model.Patients_Partition2.MaritalStatusId AS MaritalStatusId, model.Patients_Partition2.Occupation AS Occupation, model.Patients_Partition2.BusinessType AS BusinessType, model.Patients_Partition2.Ethnicity AS Ethnicity, model.Patients_Partition2.Language AS Language, model.Patients_Partition2.PatientNationalOriginId AS PatientNationalOriginId, model.Patients_Partition2.Race AS Race, model.Patients_Partition2.PatientReligionId AS PatientReligionId, model.Patients_Partition2.PatientStatusId AS PatientStatusId, model.Patients_Partition2.PriorFirstName AS PriorFirstName, model.Patients_Partition2.PriorLastName AS PriorLastName, model.Patients_Partition2.PriorPatientCode AS PriorPatientCode, model.Patients_Partition2.SocialSecurityNumber AS SocialSecurityNumber FROM model.Patients_Partition2 WITH(NOEXPAND)
	
		) q
		-- Separate to allow indexing -- the below contains self and left joins
		LEFT JOIN dbo.PracticeName pnMainOffice ON PracticeType = ''P'' and LocationReference = ''''
		LEFT JOIN dbo.PracticeCodeTable pctEthnicity ON pctEthnicity.ReferenceType = ''Ethnicity'' 
			AND Ethnicity = pctEthnicity.Code
		LEFT JOIN dbo.PracticeCodeTable pctLanguage ON pctLanguage.ReferenceType = ''Language''
			AND Language = SUBSTRING(pctLanguage.Code, 1, 10)
		LEFT JOIN dbo.PracticeCodeTable pctRace ON pctRace.ReferenceType = ''Race''
			AND Race = pctRace.Code
		LEFT JOIN dbo.PracticeCodeTable pctEmployment ON pctEmployment.ReferenceType = ''EmployeeStatus''
			AND BusinessType = SUBSTRING(pctEmployment.Code, 1, 1)'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Patients'
			,@PatientsViewSql
			,@isChanged = @isPatientsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientDelete'
				,
				'
			CREATE TRIGGER model.OnPatientDelete ON [model].[Patients] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @MaritalStatusId int
			DECLARE @DateOfBirth datetime
			DECLARE @GenderId int
			DECLARE @SocialSecurityNumber nvarchar(max)
			DECLARE @Occupation nvarchar(max)
			DECLARE @PriorLastName nvarchar(max)
			DECLARE @PriorFirstName nvarchar(max)
			DECLARE @BillingOrganizationId int
			DECLARE @Id int
			DECLARE @PatientEthnicityId int
			DECLARE @PatientRaceId int
			DECLARE @PatientLanguageId int
			DECLARE @PatientNationalOriginId int
			DECLARE @PriorPatientCode nvarchar(max)
			DECLARE @PatientEmploymentStatusId int
			DECLARE @PatientReligionId int
			DECLARE @DateOfDeath datetime
			DECLARE @FirstName nvarchar(max)
			DECLARE @LastName nvarchar(max)
			DECLARE @MiddleName nvarchar(max)
			DECLARE @Suffix nvarchar(max)
			DECLARE @Prefix nvarchar(max)
			DECLARE @Honorific nvarchar(max)
			DECLARE @Salutation nvarchar(max)
			DECLARE @NickName nvarchar(max)
			DECLARE @PatientStatusId int
			DECLARE @IsClinical bit
		
			DECLARE PatientsDeletedCursor CURSOR FOR
			SELECT MaritalStatusId, DateOfBirth, GenderId, SocialSecurityNumber, Occupation, PriorLastName, PriorFirstName, BillingOrganizationId, Id, PatientEthnicityId, PatientRaceId, PatientLanguageId, PatientNationalOriginId, PriorPatientCode, PatientEmploymentStatusId, PatientReligionId, DateOfDeath, FirstName, LastName, MiddleName, Suffix, Prefix, Honorific, Salutation, NickName, PatientStatusId, IsClinical FROM deleted
		
			OPEN PatientsDeletedCursor
			FETCH NEXT FROM PatientsDeletedCursor INTO @MaritalStatusId, @DateOfBirth, @GenderId, @SocialSecurityNumber, @Occupation, @PriorLastName, @PriorFirstName, @BillingOrganizationId, @Id, @PatientEthnicityId, @PatientRaceId, @PatientLanguageId, @PatientNationalOriginId, @PriorPatientCode, @PatientEmploymentStatusId, @PatientReligionId, @DateOfDeath, @FirstName, @LastName, @MiddleName, @Suffix, @Prefix, @Honorific, @Salutation, @NickName, @PatientStatusId, @IsClinical 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Patient Delete
				DECLARE @pair TABLE (
					x int,
					y int
					)
			
				INSERT INTO @pair
				SELECT x,
					y
				FROM model.GetIdPair(@Id, 202000000)
			
				DECLARE @patientDemographicsId int 
				SET @patientDemographicsId = (SELECT TOP 1 y FROM @pair)
			
				DELETE
				FROM PatientDemographics
				WHERE PatientId = @patientDemographicsId
			
				SELECT @Id AS Id, @MaritalStatusId AS MaritalStatusId, @DateOfBirth AS DateOfBirth, @GenderId AS GenderId, @SocialSecurityNumber AS SocialSecurityNumber, @Occupation AS Occupation, @PriorLastName AS PriorLastName, @PriorFirstName AS PriorFirstName, @BillingOrganizationId AS BillingOrganizationId, @PatientEthnicityId AS PatientEthnicityId, @PatientRaceId AS PatientRaceId, @PatientLanguageId AS PatientLanguageId, @PatientNationalOriginId AS PatientNationalOriginId, @PriorPatientCode AS PriorPatientCode, @PatientEmploymentStatusId AS PatientEmploymentStatusId, @PatientReligionId AS PatientReligionId, @DateOfDeath AS DateOfDeath, @FirstName AS FirstName, @LastName AS LastName, @MiddleName AS MiddleName, @Suffix AS Suffix, @Prefix AS Prefix, @Honorific AS Honorific, @Salutation AS Salutation, @NickName AS NickName, @PatientStatusId AS PatientStatusId, @IsClinical AS IsClinical
			
				FETCH NEXT FROM PatientsDeletedCursor INTO @MaritalStatusId, @DateOfBirth, @GenderId, @SocialSecurityNumber, @Occupation, @PriorLastName, @PriorFirstName, @BillingOrganizationId, @Id, @PatientEthnicityId, @PatientRaceId, @PatientLanguageId, @PatientNationalOriginId, @PriorPatientCode, @PatientEmploymentStatusId, @PatientReligionId, @DateOfDeath, @FirstName, @LastName, @MiddleName, @Suffix, @Prefix, @Honorific, @Salutation, @NickName, @PatientStatusId, @IsClinical 
		
			END
		
			CLOSE PatientsDeletedCursor
			DEALLOCATE PatientsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientInsert'
				,
				'
			CREATE TRIGGER model.OnPatientInsert ON [model].[Patients] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @MaritalStatusId int
			DECLARE @DateOfBirth datetime
			DECLARE @GenderId int
			DECLARE @SocialSecurityNumber nvarchar(max)
			DECLARE @Occupation nvarchar(max)
			DECLARE @PriorLastName nvarchar(max)
			DECLARE @PriorFirstName nvarchar(max)
			DECLARE @BillingOrganizationId int
			DECLARE @Id int
			DECLARE @PatientEthnicityId int
			DECLARE @PatientRaceId int
			DECLARE @PatientLanguageId int
			DECLARE @PatientNationalOriginId int
			DECLARE @PriorPatientCode nvarchar(max)
			DECLARE @PatientEmploymentStatusId int
			DECLARE @PatientReligionId int
			DECLARE @DateOfDeath datetime
			DECLARE @FirstName nvarchar(max)
			DECLARE @LastName nvarchar(max)
			DECLARE @MiddleName nvarchar(max)
			DECLARE @Suffix nvarchar(max)
			DECLARE @Prefix nvarchar(max)
			DECLARE @Honorific nvarchar(max)
			DECLARE @Salutation nvarchar(max)
			DECLARE @NickName nvarchar(max)
			DECLARE @PatientStatusId int
			DECLARE @IsClinical bit
		
			DECLARE PatientsInsertedCursor CURSOR FOR
			SELECT MaritalStatusId, DateOfBirth, GenderId, SocialSecurityNumber, Occupation, PriorLastName, PriorFirstName, BillingOrganizationId, Id, PatientEthnicityId, PatientRaceId, PatientLanguageId, PatientNationalOriginId, PriorPatientCode, PatientEmploymentStatusId, PatientReligionId, DateOfDeath, FirstName, LastName, MiddleName, Suffix, Prefix, Honorific, Salutation, NickName, PatientStatusId, IsClinical FROM inserted
		
			OPEN PatientsInsertedCursor
			FETCH NEXT FROM PatientsInsertedCursor INTO @MaritalStatusId, @DateOfBirth, @GenderId, @SocialSecurityNumber, @Occupation, @PriorLastName, @PriorFirstName, @BillingOrganizationId, @Id, @PatientEthnicityId, @PatientRaceId, @PatientLanguageId, @PatientNationalOriginId, @PriorPatientCode, @PatientEmploymentStatusId, @PatientReligionId, @DateOfDeath, @FirstName, @LastName, @MiddleName, @Suffix, @Prefix, @Honorific, @Salutation, @NickName, @PatientStatusId, @IsClinical 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- Patient Insert
				INSERT INTO PatientDemographics (FirstName)
				VALUES ('''')
			
				SET @Id = IDENT_CURRENT(''PatientDemographics'')
			
				-- @Id is a composite key (combined from two values)...so we need to compose it
				IF @IsClinical = 1
					SET @Id = @Id
				ELSE
					SET @Id = model.GetPairId(1, @Id, 202000000)
			
							
				-- Patient Update/Insert
				DECLARE @x int,
					@y int
			
				SELECT @x = x,
					@y = y
				FROM model.GetIdPair(@Id, 202000000)
			
				-- @Id is a composite key...so we need to decompose it
				DECLARE @patientDemographicsId int 
				SET @patientDemographicsId = @y
				IF @IsClinical = 1 
				UPDATE PatientDemographics
				-- This will get called for Updates AND after the Insert procedure above (an "upsert")
				-- NOT HANDLED: BillingOrganizationId (not updatable)
				SET FirstName = @FirstName,
					LastName = @LastName,
					MiddleInitial = @MiddleName,
					Salutation = @Prefix,
					NameReference = @Suffix,
					BirthDate = CASE @DateOfBirth
						WHEN NULL
							THEN ''''
						ELSE CONVERT(VARCHAR, @DateOfBirth, 112)
						END,
					Marital = CASE @MaritalStatusId
						WHEN 1
							THEN ''S''
						WHEN 2
							THEN ''M''
						WHEN 3
							THEN ''D''
						WHEN 4
							THEN ''W''
						ELSE ''''
						END,
					Gender = CASE @GenderId
						WHEN 3
							THEN ''M''
						WHEN 2
							THEN ''F''
						ELSE ''''
						END,
					Occupation = @Occupation,
					BusinessType = (
						SELECT TOP 1 SUBSTRING(Code, 1, 1)
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientEmploymentStatusId
						),
					Ethnicity = (
						SELECT TOP 1 Code
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientEthnicityId
						),
					[Language] = (
						SELECT TOP 1 SUBSTRING(Code, 1, 10)
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientLanguageId
						),
					Race = (
						SELECT TOP 1 Code
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientRaceId
						),
					Religion = (
						SELECT TOP 1 Code
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientReligionId
						),
					OldPatient = @PriorPatientCode,
					SocialSecurity = @SocialSecurityNumber,
					[Status] = CASE 
						WHEN @IsClinical = 0
							THEN ''I''
						WHEN @PatientStatusId = 2
							THEN ''C''
						WHEN @PatientStatusId = 3
							THEN ''D''
						WHEN @PatientStatusId = 4
							THEN ''M''
						ELSE ''A''
						END,
					Relationship = ''Y'',
					PolicyPatientId = @patientDemographicsId,
					SecondPolicyPatientId = 0,
					SecondRelationship = ''''
				WHERE PatientId = @patientDemographicsId
				ELSE
			
				UPDATE PatientDemographics
				SET EmergencyName = @FirstName + '' '' + @LastName
				WHERE PatientId = @patientDemographicsId
			
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @MaritalStatusId AS MaritalStatusId, @DateOfBirth AS DateOfBirth, @GenderId AS GenderId, @SocialSecurityNumber AS SocialSecurityNumber, @Occupation AS Occupation, @PriorLastName AS PriorLastName, @PriorFirstName AS PriorFirstName, @BillingOrganizationId AS BillingOrganizationId, @PatientEthnicityId AS PatientEthnicityId, @PatientRaceId AS PatientRaceId, @PatientLanguageId AS PatientLanguageId, @PatientNationalOriginId AS PatientNationalOriginId, @PriorPatientCode AS PriorPatientCode, @PatientEmploymentStatusId AS PatientEmploymentStatusId, @PatientReligionId AS PatientReligionId, @DateOfDeath AS DateOfDeath, @FirstName AS FirstName, @LastName AS LastName, @MiddleName AS MiddleName, @Suffix AS Suffix, @Prefix AS Prefix, @Honorific AS Honorific, @Salutation AS Salutation, @NickName AS NickName, @PatientStatusId AS PatientStatusId, @IsClinical AS IsClinical
				
				FETCH NEXT FROM PatientsInsertedCursor INTO @MaritalStatusId, @DateOfBirth, @GenderId, @SocialSecurityNumber, @Occupation, @PriorLastName, @PriorFirstName, @BillingOrganizationId, @Id, @PatientEthnicityId, @PatientRaceId, @PatientLanguageId, @PatientNationalOriginId, @PriorPatientCode, @PatientEmploymentStatusId, @PatientReligionId, @DateOfDeath, @FirstName, @LastName, @MiddleName, @Suffix, @Prefix, @Honorific, @Salutation, @NickName, @PatientStatusId, @IsClinical 
		
			END
		
			CLOSE PatientsInsertedCursor
			DEALLOCATE PatientsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientUpdate'
				,
				'
			CREATE TRIGGER model.OnPatientUpdate ON [model].[Patients] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @MaritalStatusId int
			DECLARE @DateOfBirth datetime
			DECLARE @GenderId int
			DECLARE @SocialSecurityNumber nvarchar(max)
			DECLARE @Occupation nvarchar(max)
			DECLARE @PriorLastName nvarchar(max)
			DECLARE @PriorFirstName nvarchar(max)
			DECLARE @BillingOrganizationId int
			DECLARE @Id int
			DECLARE @PatientEthnicityId int
			DECLARE @PatientRaceId int
			DECLARE @PatientLanguageId int
			DECLARE @PatientNationalOriginId int
			DECLARE @PriorPatientCode nvarchar(max)
			DECLARE @PatientEmploymentStatusId int
			DECLARE @PatientReligionId int
			DECLARE @DateOfDeath datetime
			DECLARE @FirstName nvarchar(max)
			DECLARE @LastName nvarchar(max)
			DECLARE @MiddleName nvarchar(max)
			DECLARE @Suffix nvarchar(max)
			DECLARE @Prefix nvarchar(max)
			DECLARE @Honorific nvarchar(max)
			DECLARE @Salutation nvarchar(max)
			DECLARE @NickName nvarchar(max)
			DECLARE @PatientStatusId int
			DECLARE @IsClinical bit
		
			DECLARE PatientsUpdatedCursor CURSOR FOR
			SELECT MaritalStatusId, DateOfBirth, GenderId, SocialSecurityNumber, Occupation, PriorLastName, PriorFirstName, BillingOrganizationId, Id, PatientEthnicityId, PatientRaceId, PatientLanguageId, PatientNationalOriginId, PriorPatientCode, PatientEmploymentStatusId, PatientReligionId, DateOfDeath, FirstName, LastName, MiddleName, Suffix, Prefix, Honorific, Salutation, NickName, PatientStatusId, IsClinical FROM inserted
		
			OPEN PatientsUpdatedCursor
			FETCH NEXT FROM PatientsUpdatedCursor INTO @MaritalStatusId, @DateOfBirth, @GenderId, @SocialSecurityNumber, @Occupation, @PriorLastName, @PriorFirstName, @BillingOrganizationId, @Id, @PatientEthnicityId, @PatientRaceId, @PatientLanguageId, @PatientNationalOriginId, @PriorPatientCode, @PatientEmploymentStatusId, @PatientReligionId, @DateOfDeath, @FirstName, @LastName, @MiddleName, @Suffix, @Prefix, @Honorific, @Salutation, @NickName, @PatientStatusId, @IsClinical 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- Patient Update/Insert
				DECLARE @x int,
					@y int
			
				SELECT @x = x,
					@y = y
				FROM model.GetIdPair(@Id, 202000000)
			
				-- @Id is a composite key...so we need to decompose it
				DECLARE @patientDemographicsId int 
				SET @patientDemographicsId = @y
				IF @IsClinical = 1 
				UPDATE PatientDemographics
				-- This will get called for Updates AND after the Insert procedure above (an "upsert")
				-- NOT HANDLED: BillingOrganizationId (not updatable)
				SET FirstName = @FirstName,
					LastName = @LastName,
					MiddleInitial = @MiddleName,
					Salutation = @Prefix,
					NameReference = @Suffix,
					BirthDate = CASE @DateOfBirth
						WHEN NULL
							THEN ''''
						ELSE CONVERT(VARCHAR, @DateOfBirth, 112)
						END,
					Marital = CASE @MaritalStatusId
						WHEN 1
							THEN ''S''
						WHEN 2
							THEN ''M''
						WHEN 3
							THEN ''D''
						WHEN 4
							THEN ''W''
						ELSE ''''
						END,
					Gender = CASE @GenderId
						WHEN 3
							THEN ''M''
						WHEN 2
							THEN ''F''
						ELSE ''''
						END,
					Occupation = @Occupation,
					BusinessType = (
						SELECT TOP 1 SUBSTRING(Code, 1, 1)
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientEmploymentStatusId
						),
					Ethnicity = (
						SELECT TOP 1 Code
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientEthnicityId
						),
					[Language] = (
						SELECT TOP 1 SUBSTRING(Code, 1, 10)
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientLanguageId
						),
					Race = (
						SELECT TOP 1 Code
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientRaceId
						),
					Religion = (
						SELECT TOP 1 Code
						FROM dbo.PracticeCodeTable
						WHERE Id = @PatientReligionId
						),
					OldPatient = @PriorPatientCode,
					SocialSecurity = @SocialSecurityNumber,
					[Status] = CASE 
						WHEN @IsClinical = 0
							THEN ''I''
						WHEN @PatientStatusId = 2
							THEN ''C''
						WHEN @PatientStatusId = 3
							THEN ''D''
						WHEN @PatientStatusId = 4
							THEN ''M''
						ELSE ''A''
						END,
					Relationship = ''Y'',
					PolicyPatientId = @patientDemographicsId,
					SecondPolicyPatientId = 0,
					SecondRelationship = ''''
				WHERE PatientId = @patientDemographicsId
				ELSE
			
				UPDATE PatientDemographics
				SET EmergencyName = @FirstName + '' '' + @LastName
				WHERE PatientId = @patientDemographicsId
			
				
				FETCH NEXT FROM PatientsUpdatedCursor INTO @MaritalStatusId, @DateOfBirth, @GenderId, @SocialSecurityNumber, @Occupation, @PriorLastName, @PriorFirstName, @BillingOrganizationId, @Id, @PatientEthnicityId, @PatientRaceId, @PatientLanguageId, @PatientNationalOriginId, @PriorPatientCode, @PatientEmploymentStatusId, @PatientReligionId, @DateOfDeath, @FirstName, @LastName, @MiddleName, @Suffix, @Prefix, @Honorific, @Salutation, @NickName, @PatientStatusId, @IsClinical 
		
			END
		
			CLOSE PatientsUpdatedCursor
			DEALLOCATE PatientsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PatientTypes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPatientTypesChanged BIT

		SET @isPatientTypesChanged = 0

		DECLARE @PatientTypesViewSql NVARCHAR(max)

		SET @PatientTypesViewSql = '
		CREATE VIEW [model].[PatientTypes_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''PATIENTTYPE'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientTypes_Internal'
			,@PatientTypesViewSql
			,@isChanged = @isPatientTypesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @PatientTypesViewSql = '	
			CREATE VIEW [model].[PatientTypes]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, IsArchived FROM [model].[PatientTypes_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_PatientTypes'
							AND object_id = OBJECT_ID('[model].[PatientTypes_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[PatientTypes_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_PatientTypes] ON [model].[PatientTypes_Internal] ([Id]);
				END

				SET @PatientTypesViewSql = @PatientTypesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientTypes'
				,@PatientTypesViewSql
				,@isChanged = @isPatientTypesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientTypeDelete'
				,'
			CREATE TRIGGER model.OnPatientTypeDelete ON [model].[PatientTypes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientTypesDeletedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM deleted
		
			OPEN PatientTypesDeletedCursor
			FETCH NEXT FROM PatientTypesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PatientType --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
			
				FETCH NEXT FROM PatientTypesDeletedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientTypesDeletedCursor
			DEALLOCATE PatientTypesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientTypeInsert'
				,'
			CREATE TRIGGER model.OnPatientTypeInsert ON [model].[PatientTypes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientTypesInsertedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN PatientTypesInsertedCursor
			FETCH NEXT FROM PatientTypesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for PatientType --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
				
				FETCH NEXT FROM PatientTypesInsertedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientTypesInsertedCursor
			DEALLOCATE PatientTypesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPatientTypeUpdate'
				,'
			CREATE TRIGGER model.OnPatientTypeUpdate ON [model].[PatientTypes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PatientTypesUpdatedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN PatientTypesUpdatedCursor
			FETCH NEXT FROM PatientTypesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for PatientType --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM PatientTypesUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PatientTypesUpdatedCursor
			DEALLOCATE PatientTypesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PaymentMethods')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPaymentMethodsChanged BIT

		SET @isPaymentMethodsChanged = 0

		DECLARE @PaymentMethodsViewSql NVARCHAR(max)

		SET @PaymentMethodsViewSql = '
		CREATE VIEW [model].[PaymentMethods_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''PAYABLETYPE'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PaymentMethods_Internal'
			,@PaymentMethodsViewSql
			,@isChanged = @isPaymentMethodsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @PaymentMethodsViewSql = '	
			CREATE VIEW [model].[PaymentMethods]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name, IsArchived FROM [model].[PaymentMethods_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_PaymentMethods'
							AND object_id = OBJECT_ID('[model].[PaymentMethods_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[PaymentMethods_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_PaymentMethods] ON [model].[PaymentMethods_Internal] ([Id]);
				END

				SET @PaymentMethodsViewSql = @PaymentMethodsViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PaymentMethods'
				,@PaymentMethodsViewSql
				,@isChanged = @isPaymentMethodsChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPaymentMethodDelete'
				,'
			CREATE TRIGGER model.OnPaymentMethodDelete ON [model].[PaymentMethods] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PaymentMethodsDeletedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM deleted
		
			OPEN PaymentMethodsDeletedCursor
			FETCH NEXT FROM PaymentMethodsDeletedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PaymentMethod --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
			
				FETCH NEXT FROM PaymentMethodsDeletedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PaymentMethodsDeletedCursor
			DEALLOCATE PaymentMethodsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPaymentMethodInsert'
				,'
			CREATE TRIGGER model.OnPaymentMethodInsert ON [model].[PaymentMethods] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PaymentMethodsInsertedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN PaymentMethodsInsertedCursor
			FETCH NEXT FROM PaymentMethodsInsertedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for PaymentMethod --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name, @IsArchived AS IsArchived
				
				FETCH NEXT FROM PaymentMethodsInsertedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PaymentMethodsInsertedCursor
			DEALLOCATE PaymentMethodsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPaymentMethodUpdate'
				,'
			CREATE TRIGGER model.OnPaymentMethodUpdate ON [model].[PaymentMethods] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE PaymentMethodsUpdatedCursor CURSOR FOR
			SELECT Id, Name, IsArchived FROM inserted
		
			OPEN PaymentMethodsUpdatedCursor
			FETCH NEXT FROM PaymentMethodsUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for PaymentMethod --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM PaymentMethodsUpdatedCursor INTO @Id, @Name, @IsArchived 
		
			END
		
			CLOSE PaymentMethodsUpdatedCursor
			DEALLOCATE PaymentMethodsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PhoneNumbers')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPhoneNumbersChanged BIT

		SET @isPhoneNumbersChanged = 0

		DECLARE @ismodelPhoneNumbers_Partition1Changed BIT

		SET @ismodelPhoneNumbers_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumbers_Partition1'
			,
			'
		CREATE VIEW model.PhoneNumbers_Partition1
		WITH SCHEMABINDING
		AS
		----PhoneNumber BillingOrganization PracticeName #1
		SELECT (55000000 * 1 + pn.PracticeId) AS Id,
			NULL AS ContactId,
			pn.PracticeId AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			PracticePhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			NULL AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			''BillingOrganizationPhoneNumber'' as __EntityType__,
			9 AS BillingOrganizationPhoneNumberTypeId
			,NULL AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,NULL AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeName pn
		WHERE PracticePhone <> ''''
			AND PracticePhone IS NOT NULL
			AND PracticeType = ''P''
		'
			,@isChanged = @ismodelPhoneNumbers_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumbers_Partition1'
					AND object_id = OBJECT_ID('model.PhoneNumbers_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumbers_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumbers_Partition1 ON model.PhoneNumbers_Partition1 ([Id]);
		END

		DECLARE @ismodelPhoneNumbers_Partition2Changed BIT

		SET @ismodelPhoneNumbers_Partition2Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumbers_Partition2'
			,
			'
		CREATE VIEW model.PhoneNumbers_Partition2
		WITH SCHEMABINDING
		AS
		----PhoneNumber BillingOrganization PracticeName #2
		SELECT (55000000 * 2 + pn.PracticeId) AS Id,
			NULL AS ContactId,
			pn.PracticeId AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			PracticeOtherPhone  AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			NULL AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			''BillingOrganizationPhoneNumber'' as __EntityType__,
			1 AS BillingOrganizationPhoneNumberTypeId
			,NULL AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,NULL AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeName pn
		WHERE PracticeOtherPhone <> ''''
			AND PracticeOtherPhone IS NOT NULL
			AND PracticeType = ''P''
		'
			,@isChanged = @ismodelPhoneNumbers_Partition2Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumbers_Partition2'
					AND object_id = OBJECT_ID('model.PhoneNumbers_Partition2')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumbers_Partition2'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumbers_Partition2 ON model.PhoneNumbers_Partition2 ([Id]);
		END

		DECLARE @ismodelPhoneNumbers_Partition3Changed BIT

		SET @ismodelPhoneNumbers_Partition3Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumbers_Partition3'
			,
			'
		CREATE VIEW model.PhoneNumbers_Partition3
		WITH SCHEMABINDING
		AS
		----PhoneNumber BillingOrganization PracticeName Fax
		SELECT (55000000 * 3 + pn.PracticeId) AS Id,
			NULL AS ContactId,
			pn.PracticeId AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			PracticeFax AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			NULL AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			''BillingOrganizationPhoneNumber'' as __EntityType__,
			6 AS BillingOrganizationPhoneNumberTypeId
			,NULL AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,NULL AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeName pn
		WHERE PracticeFax <> ''''
			AND PracticeFax IS NOT NULL
			AND PracticeType = ''P''
		'
			,@isChanged = @ismodelPhoneNumbers_Partition3Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumbers_Partition3'
					AND object_id = OBJECT_ID('model.PhoneNumbers_Partition3')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumbers_Partition3'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumbers_Partition3 ON model.PhoneNumbers_Partition3 ([Id]);
		END

		DECLARE @ismodelPhoneNumbers_Partition4Changed BIT

		SET @ismodelPhoneNumbers_Partition4Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumbers_Partition4'
			,
			'
		CREATE VIEW model.PhoneNumbers_Partition4
		WITH SCHEMABINDING
		AS
		----PhoneNumber Contact (all) PHONE
		SELECT (55000000 * 19 + VendorId) AS Id
			,VendorId AS ContactId
			,NULL AS BillingOrganizationId
			,CONVERT(NVARCHAR, NULL) AS CountryCode
			,CONVERT(NVARCHAR, NULL) AS AreaCode
			,VendorPhone AS ExchangeAndSuffix
			,CONVERT(NVARCHAR, NULL) AS Extension
			,NULL AS ExternalOrganizationId
			,NULL AS InsurerId
			,CONVERT(bit, 0) AS IsInternational
			,0 AS OrdinalId
			,NULL AS PatientId
			,NULL AS ServiceLocationID
			,NULL AS UserId
			,''ContactPhoneNumber'' as __EntityType__
			,NULL AS BillingOrganizationPhoneNumberTypeId
			,5 AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,NULL AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeVendors
		WHERE VendorPhone <> ''''
			AND VendorPhone IS NOT NULL
			AND VendorLastName <> ''''
			AND VendorLastName IS NOT NULL
		'
			,@isChanged = @ismodelPhoneNumbers_Partition4Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumbers_Partition4'
					AND object_id = OBJECT_ID('model.PhoneNumbers_Partition4')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumbers_Partition4'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumbers_Partition4 ON model.PhoneNumbers_Partition4 ([Id]);
		END

		DECLARE @ismodelPhoneNumbers_Partition5Changed BIT

		SET @ismodelPhoneNumbers_Partition5Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumbers_Partition5'
			,
			'
		CREATE VIEW model.PhoneNumbers_Partition5
		WITH SCHEMABINDING
		AS
		----PhoneNumber Contact  (all) FAX
		SELECT (55000000 * 20 + VendorId) AS Id
			,VendorId AS ContactId
			,NULL AS BillingOrganizationId
			,CONVERT(NVARCHAR, NULL) AS CountryCode
			,CONVERT(NVARCHAR, NULL) AS AreaCode
			,VendorFax AS ExchangeAndSuffix
			,CONVERT(NVARCHAR, NULL) AS Extension
			,NULL AS ExternalOrganizationId
			,NULL AS InsurerId
			,CONVERT(bit, 0) AS IsInternational
			,0 AS OrdinalId
			,NULL AS PatientId
			,NULL AS ServiceLocationID
			,NULL AS UserId
			,''ContactPhoneNumber'' as __EntityType__
			,NULL AS BillingOrganizationPhoneNumberTypeId
			,4 AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,NULL AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeVendors
		WHERE VendorFax <> ''''
			AND VendorFax IS NOT NULL
			AND VendorLastName <> ''''
			AND VendorLastName IS NOT NULL
		'
			,@isChanged = @ismodelPhoneNumbers_Partition5Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumbers_Partition5'
					AND object_id = OBJECT_ID('model.PhoneNumbers_Partition5')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumbers_Partition5'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumbers_Partition5 ON model.PhoneNumbers_Partition5 ([Id]);
		END

		DECLARE @ismodelPhoneNumbers_Partition6Changed BIT

		SET @ismodelPhoneNumbers_Partition6Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumbers_Partition6'
			,'
		CREATE VIEW model.PhoneNumbers_Partition6
		WITH SCHEMABINDING
		AS
		----PhoneNumberInsurer
		SELECT (55000000 * 25 + InsurerId) AS Id
			,NULL AS ContactId
			,NULL AS BillingOrganizationId
			,CONVERT(NVARCHAR, NULL) AS CountryCode
			,CONVERT(NVARCHAR, NULL) AS AreaCode
			,InsurerPhone AS ExchangeAndSuffix
			,CONVERT(NVARCHAR, NULL) AS Extension
			,NULL AS ExternalOrganizationId
			,InsurerId AS InsurerId
			,CONVERT(bit, 0) AS IsInternational
			,0 AS OrdinalId
			,NULL AS PatientId
			,NULL AS ServiceLocationId
			,NULL AS UserId
			,''InsurerPhoneNumber'' as __EntityType__
			,NULL AS BillingOrganizationPhoneNumberTypeId
			,NULL AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,8 AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeInsurers
		WHERE InsurerPhone <> ''''
			AND InsurerPhone IS NOT NULL
		'
			,@isChanged = @ismodelPhoneNumbers_Partition6Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumbers_Partition6'
					AND object_id = OBJECT_ID('model.PhoneNumbers_Partition6')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumbers_Partition6'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumbers_Partition6 ON model.PhoneNumbers_Partition6 ([Id]);
		END

		DECLARE @ismodelPhoneNumbers_Partition7Changed BIT

		SET @ismodelPhoneNumbers_Partition7Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumbers_Partition7'
			,'
		CREATE VIEW model.PhoneNumbers_Partition7
		WITH SCHEMABINDING
		AS
		----PhoneNumber Patient Home
		SELECT (55000000 * 9 + PatientId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			pd.HomePhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,	
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			PatientId AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			''PatientPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			7 AS PatientPhoneNumberTypeId,
			NULL AS ServiceLocationPhoneNumberTypeId,
			NULL AS UserPhoneNumberTypeId
		FROM dbo.PatientDemographics pd
		WHERE pd.HomePhone <> ''''
			AND pd.HomePhone IS NOT NULL
		'
			,@isChanged = @ismodelPhoneNumbers_Partition7Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumbers_Partition7'
					AND object_id = OBJECT_ID('model.PhoneNumbers_Partition7')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumbers_Partition7'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumbers_Partition7 ON model.PhoneNumbers_Partition7 ([Id]);
		END

		DECLARE @ismodelPhoneNumbers_Partition8Changed BIT

		SET @ismodelPhoneNumbers_Partition8Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumbers_Partition8'
			,
			'
		CREATE VIEW model.PhoneNumbers_Partition8
		WITH SCHEMABINDING
		AS
		----PhoneNumber Patient Work
		SELECT (55000000 * 10 + PatientId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			pd.BusinessPhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			PatientId AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			''PatientPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			2 AS PatientPhoneNumberTypeId,
			NULL AS ServiceLocationPhoneNumberTypeId,
			NULL AS UserPhoneNumberTypeId
		FROM dbo.PatientDemographics pd
		WHERE pd.BusinessPhone <> ''''
			AND pd.BusinessPhone IS NOT NULL
		'
			,@isChanged = @ismodelPhoneNumbers_Partition8Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumbers_Partition8'
					AND object_id = OBJECT_ID('model.PhoneNumbers_Partition8')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumbers_Partition8'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumbers_Partition8 ON model.PhoneNumbers_Partition8 ([Id]);
		END

		DECLARE @ismodelPhoneNumbers_Partition9Changed BIT

		SET @ismodelPhoneNumbers_Partition9Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumbers_Partition9'
			,'
		CREATE VIEW model.PhoneNumbers_Partition9
		WITH SCHEMABINDING
		AS
		----PhoneNumber Patient Cell
		SELECT (55000000 * 11 + PatientId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			pd.CellPhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			PatientId AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			''PatientPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			3 AS PatientPhoneNumberTypeId,
			NULL AS ServiceLocationPhoneNumberTypeId,
			NULL AS UserPhoneNumberTypeId
		FROM dbo.PatientDemographics pd
		WHERE pd.CellPhone <> ''''
			AND pd.CellPhone IS NOT NULL
		'
			,@isChanged = @ismodelPhoneNumbers_Partition9Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumbers_Partition9'
					AND object_id = OBJECT_ID('model.PhoneNumbers_Partition9')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumbers_Partition9'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumbers_Partition9 ON model.PhoneNumbers_Partition9 ([Id]);
		END

		DECLARE @PhoneNumbersViewSql NVARCHAR(max)

		SET @PhoneNumbersViewSql = 
			'
		CREATE VIEW [model].[PhoneNumbers]
		WITH SCHEMABINDING
		AS
		SELECT model.PhoneNumbers_Partition1.Id AS Id, model.PhoneNumbers_Partition1.ContactId AS ContactId, model.PhoneNumbers_Partition1.BillingOrganizationId AS BillingOrganizationId, model.PhoneNumbers_Partition1.CountryCode AS CountryCode, model.PhoneNumbers_Partition1.AreaCode AS AreaCode, model.PhoneNumbers_Partition1.ExchangeAndSuffix AS ExchangeAndSuffix, model.PhoneNumbers_Partition1.Extension AS Extension, model.PhoneNumbers_Partition1.ExternalOrganizationId AS ExternalOrganizationId, model.PhoneNumbers_Partition1.InsurerId AS InsurerId, model.PhoneNumbers_Partition1.IsInternational AS IsInternational, model.PhoneNumbers_Partition1.OrdinalId AS OrdinalId, model.PhoneNumbers_Partition1.PatientId AS PatientId, model.PhoneNumbers_Partition1.ServiceLocationId AS ServiceLocationId, model.PhoneNumbers_Partition1.UserId AS UserId, model.PhoneNumbers_Partition1.__EntityType__ AS __EntityType__, model.PhoneNumbers_Partition1.BillingOrganizationPhoneNumberTypeId AS BillingOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition1.ContactPhoneNumberTypeId AS ContactPhoneNumberTypeId, model.PhoneNumbers_Partition1.ExternalOrganizationPhoneNumberTypeId AS ExternalOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition1.InsurerPhoneNumberTypeId AS InsurerPhoneNumberTypeId, model.PhoneNumbers_Partition1.PatientPhoneNumberTypeId AS PatientPhoneNumberTypeId, model.PhoneNumbers_Partition1.ServiceLocationPhoneNumberTypeId AS ServiceLocationPhoneNumberTypeId, model.PhoneNumbers_Partition1.UserPhoneNumberTypeId AS UserPhoneNumberTypeId FROM model.PhoneNumbers_Partition1 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PhoneNumbers_Partition2.Id AS Id, model.PhoneNumbers_Partition2.ContactId AS ContactId, model.PhoneNumbers_Partition2.BillingOrganizationId AS BillingOrganizationId, model.PhoneNumbers_Partition2.CountryCode AS CountryCode, model.PhoneNumbers_Partition2.AreaCode AS AreaCode, model.PhoneNumbers_Partition2.ExchangeAndSuffix AS ExchangeAndSuffix, model.PhoneNumbers_Partition2.Extension AS Extension, model.PhoneNumbers_Partition2.ExternalOrganizationId AS ExternalOrganizationId, model.PhoneNumbers_Partition2.InsurerId AS InsurerId, model.PhoneNumbers_Partition2.IsInternational AS IsInternational, model.PhoneNumbers_Partition2.OrdinalId AS OrdinalId, model.PhoneNumbers_Partition2.PatientId AS PatientId, model.PhoneNumbers_Partition2.ServiceLocationId AS ServiceLocationId, model.PhoneNumbers_Partition2.UserId AS UserId, model.PhoneNumbers_Partition2.__EntityType__ AS __EntityType__, model.PhoneNumbers_Partition2.BillingOrganizationPhoneNumberTypeId AS BillingOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition2.ContactPhoneNumberTypeId AS ContactPhoneNumberTypeId, model.PhoneNumbers_Partition2.ExternalOrganizationPhoneNumberTypeId AS ExternalOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition2.InsurerPhoneNumberTypeId AS InsurerPhoneNumberTypeId, model.PhoneNumbers_Partition2.PatientPhoneNumberTypeId AS PatientPhoneNumberTypeId, model.PhoneNumbers_Partition2.ServiceLocationPhoneNumberTypeId AS ServiceLocationPhoneNumberTypeId, model.PhoneNumbers_Partition2.UserPhoneNumberTypeId AS UserPhoneNumberTypeId FROM model.PhoneNumbers_Partition2 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PhoneNumbers_Partition3.Id AS Id, model.PhoneNumbers_Partition3.ContactId AS ContactId, model.PhoneNumbers_Partition3.BillingOrganizationId AS BillingOrganizationId, model.PhoneNumbers_Partition3.CountryCode AS CountryCode, model.PhoneNumbers_Partition3.AreaCode AS AreaCode, model.PhoneNumbers_Partition3.ExchangeAndSuffix AS ExchangeAndSuffix, model.PhoneNumbers_Partition3.Extension AS Extension, model.PhoneNumbers_Partition3.ExternalOrganizationId AS ExternalOrganizationId, model.PhoneNumbers_Partition3.InsurerId AS InsurerId, model.PhoneNumbers_Partition3.IsInternational AS IsInternational, model.PhoneNumbers_Partition3.OrdinalId AS OrdinalId, model.PhoneNumbers_Partition3.PatientId AS PatientId, model.PhoneNumbers_Partition3.ServiceLocationId AS ServiceLocationId, model.PhoneNumbers_Partition3.UserId AS UserId, model.PhoneNumbers_Partition3.__EntityType__ AS __EntityType__, model.PhoneNumbers_Partition3.BillingOrganizationPhoneNumberTypeId AS BillingOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition3.ContactPhoneNumberTypeId AS ContactPhoneNumberTypeId, model.PhoneNumbers_Partition3.ExternalOrganizationPhoneNumberTypeId AS ExternalOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition3.InsurerPhoneNumberTypeId AS InsurerPhoneNumberTypeId, model.PhoneNumbers_Partition3.PatientPhoneNumberTypeId AS PatientPhoneNumberTypeId, model.PhoneNumbers_Partition3.ServiceLocationPhoneNumberTypeId AS ServiceLocationPhoneNumberTypeId, model.PhoneNumbers_Partition3.UserPhoneNumberTypeId AS UserPhoneNumberTypeId FROM model.PhoneNumbers_Partition3 WITH(NOEXPAND)
	
		UNION ALL
	
		----PhoneNumber BillingOrganization Resources (override/orgoverride)
		SELECT (55000000 * 4 + re.ResourceId) AS Id
			,NULL AS ContactId
			,(110000000 * 1 + re.ResourceId) AS BillingOrganizationId
			,CONVERT(NVARCHAR, NULL) AS CountryCode
			,CONVERT(NVARCHAR, NULL) AS AreaCode
			,re.ResourcePhone AS ExchangeAndSuffix
			,CONVERT(NVARCHAR, NULL) AS Extension
			,NULL AS ExternalOrganizationId
			,NULL AS InsurerId
			,CONVERT(bit, 0) AS IsInternational
			,0 AS OrdinalId
			,NULL AS PatientId
			,NULL AS ServiceLocationId
			,NULL AS UserId
			,''BillingOrganizationPhoneNumber'' AS __EntityType__
			,9 AS BillingOrganizationPhoneNumberTypeId
			,NULL AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,NULL AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.Resources re
		LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
			AND pa.ResourceType = ''R''
		WHERE re.ResourcePhone <> ''''
			AND re.ResourcePhone IS NOT NULL
			AND re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			AND (
				pa.[Override] = ''Y''
				OR pa.OrgOverride = ''Y''
				)
		GROUP BY re.ResourceId
			,re.ResourcePhone
	
		UNION ALL
	
		SELECT model.PhoneNumbers_Partition4.Id AS Id, model.PhoneNumbers_Partition4.ContactId AS ContactId, model.PhoneNumbers_Partition4.BillingOrganizationId AS BillingOrganizationId, model.PhoneNumbers_Partition4.CountryCode AS CountryCode, model.PhoneNumbers_Partition4.AreaCode AS AreaCode, model.PhoneNumbers_Partition4.ExchangeAndSuffix AS ExchangeAndSuffix, model.PhoneNumbers_Partition4.Extension AS Extension, model.PhoneNumbers_Partition4.ExternalOrganizationId AS ExternalOrganizationId, model.PhoneNumbers_Partition4.InsurerId AS InsurerId, model.PhoneNumbers_Partition4.IsInternational AS IsInternational, model.PhoneNumbers_Partition4.OrdinalId AS OrdinalId, model.PhoneNumbers_Partition4.PatientId AS PatientId, model.PhoneNumbers_Partition4.ServiceLocationID AS ServiceLocationID, model.PhoneNumbers_Partition4.UserId AS UserId, model.PhoneNumbers_Partition4.__EntityType__ AS __EntityType__, model.PhoneNumbers_Partition4.BillingOrganizationPhoneNumberTypeId AS BillingOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition4.ContactPhoneNumberTypeId AS ContactPhoneNumberTypeId, model.PhoneNumbers_Partition4.ExternalOrganizationPhoneNumberTypeId AS ExternalOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition4.InsurerPhoneNumberTypeId AS InsurerPhoneNumberTypeId, model.PhoneNumbers_Partition4.PatientPhoneNumberTypeId AS PatientPhoneNumberTypeId, model.PhoneNumbers_Partition4.ServiceLocationPhoneNumberTypeId AS ServiceLocationPhoneNumberTypeId, model.PhoneNumbers_Partition4.UserPhoneNumberTypeId AS UserPhoneNumberTypeId FROM model.PhoneNumbers_Partition4 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PhoneNumbers_Partition5.Id AS Id, model.PhoneNumbers_Partition5.ContactId AS ContactId, model.PhoneNumbers_Partition5.BillingOrganizationId AS BillingOrganizationId, model.PhoneNumbers_Partition5.CountryCode AS CountryCode, model.PhoneNumbers_Partition5.AreaCode AS AreaCode, model.PhoneNumbers_Partition5.ExchangeAndSuffix AS ExchangeAndSuffix, model.PhoneNumbers_Partition5.Extension AS Extension, model.PhoneNumbers_Partition5.ExternalOrganizationId AS ExternalOrganizationId, model.PhoneNumbers_Partition5.InsurerId AS InsurerId, model.PhoneNumbers_Partition5.IsInternational AS IsInternational, model.PhoneNumbers_Partition5.OrdinalId AS OrdinalId, model.PhoneNumbers_Partition5.PatientId AS PatientId, model.PhoneNumbers_Partition5.ServiceLocationID AS ServiceLocationID, model.PhoneNumbers_Partition5.UserId AS UserId, model.PhoneNumbers_Partition5.__EntityType__ AS __EntityType__, model.PhoneNumbers_Partition5.BillingOrganizationPhoneNumberTypeId AS BillingOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition5.ContactPhoneNumberTypeId AS ContactPhoneNumberTypeId, model.PhoneNumbers_Partition5.ExternalOrganizationPhoneNumberTypeId AS ExternalOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition5.InsurerPhoneNumberTypeId AS InsurerPhoneNumberTypeId, model.PhoneNumbers_Partition5.PatientPhoneNumberTypeId AS PatientPhoneNumberTypeId, model.PhoneNumbers_Partition5.ServiceLocationPhoneNumberTypeId AS ServiceLocationPhoneNumberTypeId, model.PhoneNumbers_Partition5.UserPhoneNumberTypeId AS UserPhoneNumberTypeId FROM model.PhoneNumbers_Partition5 WITH(NOEXPAND)
	
		UNION ALL
	
		----PhoneNumber Contact  (all) CELL
		SELECT (55000000 * 21 + VendorId) AS Id
			,VendorId AS ContactId
			,NULL AS BillingOrganizationId
			,CONVERT(NVARCHAR, NULL) AS CountryCode
			,CONVERT(NVARCHAR, NULL) AS AreaCode
			,VendorCellPhone AS ExchangeAndSuffix
			,CONVERT(NVARCHAR, NULL) AS Extension
			,NULL AS ExternalOrganizationId
			,NULL AS InsurerId
			,CONVERT(bit, 0) AS IsInternational
			,0 AS OrdinalId
			,NULL AS PatientId
			,NULL AS ServiceLocationID
			,NULL AS UserId
			,''ContactPhoneNumber'' as __EntityType__
			,NULL AS BillingOrganizationPhoneNumberTypeId
			,pct.Id + 1000 AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,NULL AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeVendors pv
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Cell''
			AND pct.ReferenceType = ''PHONETYPECONTACT''
		WHERE VendorCellPhone <> ''''
			AND VendorCellPhone IS NOT NULL
			AND VendorLastName <> ''''
			AND VendorLastName IS NOT NULL
	
		UNION ALL
	
		----PhoneNumber ExternalOrganization phone
		SELECT (55000000 * 22 + VendorId) AS Id
			,NULL AS ContactId
			,NULL AS BillingOrganizationId
			,CONVERT(NVARCHAR, NULL) AS CountryCode
			,CONVERT(NVARCHAR, NULL) AS AreaCode
			,VendorPhone AS ExchangeAndSuffix
			,CONVERT(NVARCHAR, NULL) AS Extension
			,VendorId AS ExternalOrganizationId
			,NULL AS InsurerId
			,CONVERT(bit, 0) AS IsInternational
			,0 AS OrdinalId
			,NULL AS PatientId
			,NULL AS ServiceLocationId
			,NULL AS UserId
			,''ExternalOrganizationPhoneNumber'' as __EntityType__
			,NULL AS BillingOrganizationPhoneNumberTypeId
			,NULL AS ContactPhoneNumberTypeId
			,pct.Id + 1000 AS ExternalOrganizationPhoneNumberTypeId
			,NULL AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeVendors pv
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Primary''
			AND pct.ReferenceType = ''PHONETYPEEXTERNALORGANIZATION''
		WHERE VendorPhone <> ''''
			AND VendorPhone IS NOT NULL
			AND VendorFirmName <> ''''
			AND VendorFirmName IS NOT NULL
	
		UNION ALL
	
		----PhoneNumber ExternalOrganization fax
		SELECT (55000000 * 23 + VendorId) AS Id
			,NULL AS ContactId
			,NULL AS BillingOrganizationId
			,CONVERT(NVARCHAR, NULL) AS CountryCode
			,CONVERT(NVARCHAR, NULL) AS AreaCode
			,VendorFax AS ExchangeAndSuffix
			,CONVERT(NVARCHAR, NULL) AS Extension
			,VendorId AS ExternalOrganizationId
			,NULL AS InsurerId
			,CONVERT(bit, 0) AS IsInternational
			,0 AS OrdinalId
			,NULL AS PatientId
			,NULL AS ServiceLocationId
			,NULL AS UserId
			,''ExternalOrganizationPhoneNumber'' as __EntityType__
			,NULL AS BillingOrganizationPhoneNumberTypeId
			,NULL AS ContactPhoneNumberTypeId
			,pct.Id + 1000 AS ExternalOrganizationPhoneNumberTypeId
			,NULL AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeVendors
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Fax''
			AND pct.ReferenceType = ''PHONETYPEEXTERNALORGANIZATION''
		WHERE VendorFax <> ''''
			AND VendorFax IS NOT NULL
			AND VendorFirmName <> ''''
			AND VendorFirmName IS NOT NULL
	
		UNION ALL
	
		SELECT model.PhoneNumbers_Partition6.Id AS Id, model.PhoneNumbers_Partition6.ContactId AS ContactId, model.PhoneNumbers_Partition6.BillingOrganizationId AS BillingOrganizationId, model.PhoneNumbers_Partition6.CountryCode AS CountryCode, model.PhoneNumbers_Partition6.AreaCode AS AreaCode, model.PhoneNumbers_Partition6.ExchangeAndSuffix AS ExchangeAndSuffix, model.PhoneNumbers_Partition6.Extension AS Extension, model.PhoneNumbers_Partition6.ExternalOrganizationId AS ExternalOrganizationId, model.PhoneNumbers_Partition6.InsurerId AS InsurerId, model.PhoneNumbers_Partition6.IsInternational AS IsInternational, model.PhoneNumbers_Partition6.OrdinalId AS OrdinalId, model.PhoneNumbers_Partition6.PatientId AS PatientId, model.PhoneNumbers_Partition6.ServiceLocationId AS ServiceLocationId, model.PhoneNumbers_Partition6.UserId AS UserId, model.PhoneNumbers_Partition6.__EntityType__ AS __EntityType__, model.PhoneNumbers_Partition6.BillingOrganizationPhoneNumberTypeId AS BillingOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition6.ContactPhoneNumberTypeId AS ContactPhoneNumberTypeId, model.PhoneNumbers_Partition6.ExternalOrganizationPhoneNumberTypeId AS ExternalOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition6.InsurerPhoneNumberTypeId AS InsurerPhoneNumberTypeId, model.PhoneNumbers_Partition6.PatientPhoneNumberTypeId AS PatientPhoneNumberTypeId, model.PhoneNumbers_Partition6.ServiceLocationPhoneNumberTypeId AS ServiceLocationPhoneNumberTypeId, model.PhoneNumbers_Partition6.UserPhoneNumberTypeId AS UserPhoneNumberTypeId FROM model.PhoneNumbers_Partition6 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT (55000000 * 26 + InsurerId) AS Id
			,NULL AS ContactId
			,NULL AS BillingOrganizationId
			,CONVERT(NVARCHAR, NULL) AS CountryCode
			,CONVERT(NVARCHAR, NULL) AS AreaCode
			,InsurerPrecPhone AS ExchangeAndSuffix
			,CONVERT(NVARCHAR, NULL) AS Extension
			,NULL AS ExternalOrganizationId
			,InsurerId AS InsurerId
			,CONVERT(bit, 0) AS IsInternational
			,0 AS OrdinalId
			,NULL AS PatientId
			,NULL AS ServiceLocationId
			,NULL AS UserId
			,''InsurerPhoneNumber'' as __EntityType__
			,NULL AS BillingOrganizationPhoneNumberTypeId
			,NULL AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,pct.Id + 1000 AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeInsurers
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Authorization''
			AND pct.ReferenceType = ''PHONETYPEINSURER''
		WHERE InsurerPrecPhone <> ''''
			AND InsurerPrecPhone IS NOT NULL
	
		UNION ALL
	
		SELECT (55000000 * 27 + InsurerId) AS Id
			,NULL AS ContactId
			,NULL AS BillingOrganizationId
			,CONVERT(NVARCHAR, NULL) AS CountryCode
			,CONVERT(NVARCHAR, NULL) AS AreaCode
			,InsurerEligPhone AS ExchangeAndSuffix
			,CONVERT(NVARCHAR, NULL) AS Extension
			,NULL AS ExternalOrganizationId
			,InsurerId AS InsurerId
			,CONVERT(bit, 0) AS IsInternational
			,0 AS OrdinalId
			,NULL AS PatientId
			,NULL AS ServiceLocationId
			,NULL AS UserId
			,''InsurerPhoneNumber'' as __EntityType__
			,NULL AS BillingOrganizationPhoneNumberTypeId
			,NULL AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,pct.Id + 1000 AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeInsurers
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Eligibility''
			AND pct.ReferenceType = ''PHONETYPEINSURER''
		WHERE InsurerEligPhone <> ''''
			AND InsurerEligPhone IS NOT NULL
	
		UNION ALL
	
		SELECT (55000000 * 28 + InsurerId) AS Id
			,NULL AS ContactId
			,NULL AS BillingOrganizationId
			,CONVERT(NVARCHAR, NULL) AS CountryCode
			,CONVERT(NVARCHAR, NULL) AS AreaCode
			,InsurerProvPhone AS ExchangeAndSuffix
			,CONVERT(NVARCHAR, NULL) AS Extension
			,NULL AS ExternalOrganizationId
			,InsurerId AS InsurerId
			,CONVERT(bit, 0) AS IsInternational
			,0 AS OrdinalId
			,NULL AS PatientId
			,NULL AS ServiceLocationId
			,NULL AS UserId
			,''InsurerPhoneNumber'' as __EntityType__
			,NULL AS BillingOrganizationPhoneNumberTypeId
			,NULL AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,pct.Id + 1000 AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeInsurers
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Provider''
			AND pct.ReferenceType = ''PHONETYPEINSURER''
		WHERE InsurerProvPhone <> ''''
			AND InsurerProvPhone IS NOT NULL
	
		UNION ALL
	
		SELECT (55000000 * 29 + InsurerId) AS Id
			,NULL AS ContactId
			,NULL AS BillingOrganizationId
			,CONVERT(NVARCHAR, NULL) AS CountryCode
			,CONVERT(NVARCHAR, NULL) AS AreaCode
			,InsurerClaimPhone AS ExchangeAndSuffix
			,CONVERT(NVARCHAR, NULL) AS Extension
			,NULL AS ExternalOrganizationId
			,InsurerId AS InsurerId
			,CONVERT(bit, 0) AS IsInternational
			,0 AS OrdinalId
			,NULL AS PatientId
			,NULL AS ServiceLocationId
			,NULL AS UserId
			,''InsurerPhoneNumber'' as __EntityType__
			,NULL AS BillingOrganizationPhoneNumberTypeId
			,NULL AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,pct.Id + 1000 AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeInsurers
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Claims''
			AND pct.ReferenceType = ''PHONETYPEINSURER''
		WHERE InsurerClaimPhone <> ''''
			AND InsurerClaimPhone IS NOT NULL
	
		UNION ALL
	
		SELECT (55000000 * 30 + InsurerId) AS Id
			,NULL AS ContactId
			,NULL AS BillingOrganizationId
			,CONVERT(NVARCHAR, NULL) AS CountryCode
			,CONVERT(NVARCHAR, NULL) AS AreaCode
			,InsurerENumber AS ExchangeAndSuffix
			,CONVERT(NVARCHAR, NULL) AS Extension
			,NULL AS ExternalOrganizationId
			,InsurerId AS InsurerId
			,CONVERT(bit, 0) AS IsInternational
			,0 AS OrdinalId
			,NULL AS PatientId
			,NULL AS ServiceLocationId
			,NULL AS UserId
			,''InsurerPhoneNumber'' as __EntityType__
			,NULL AS BillingOrganizationPhoneNumberTypeId
			,NULL AS ContactPhoneNumberTypeId
			,NULL AS ExternalOrganizationPhoneNumberTypeId
			,pct.Id + 1000 AS InsurerPhoneNumberTypeId
			,NULL AS PatientPhoneNumberTypeId
			,NULL AS ServiceLocationPhoneNumberTypeId
			,NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeInsurers
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Submissions''
			AND pct.ReferenceType = ''PHONETYPEINSURER''
		WHERE InsurerENumber <> ''''
			AND InsurerENumber IS NOT NULL
	
		UNION ALL
	
		SELECT model.PhoneNumbers_Partition7.Id AS Id, model.PhoneNumbers_Partition7.ContactId AS ContactId, model.PhoneNumbers_Partition7.BillingOrganizationId AS BillingOrganizationId, model.PhoneNumbers_Partition7.CountryCode AS CountryCode, model.PhoneNumbers_Partition7.AreaCode AS AreaCode, model.PhoneNumbers_Partition7.ExchangeAndSuffix AS ExchangeAndSuffix, model.PhoneNumbers_Partition7.Extension AS Extension, model.PhoneNumbers_Partition7.ExternalOrganizationId AS ExternalOrganizationId, model.PhoneNumbers_Partition7.InsurerId AS InsurerId, model.PhoneNumbers_Partition7.IsInternational AS IsInternational, model.PhoneNumbers_Partition7.OrdinalId AS OrdinalId, model.PhoneNumbers_Partition7.PatientId AS PatientId, model.PhoneNumbers_Partition7.ServiceLocationId AS ServiceLocationId, model.PhoneNumbers_Partition7.UserId AS UserId, model.PhoneNumbers_Partition7.__EntityType__ AS __EntityType__, model.PhoneNumbers_Partition7.BillingOrganizationPhoneNumberTypeId AS BillingOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition7.ContactPhoneNumberTypeId AS ContactPhoneNumberTypeId, model.PhoneNumbers_Partition7.ExternalOrganizationPhoneNumberTypeId AS ExternalOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition7.InsurerPhoneNumberTypeId AS InsurerPhoneNumberTypeId, model.PhoneNumbers_Partition7.PatientPhoneNumberTypeId AS PatientPhoneNumberTypeId, model.PhoneNumbers_Partition7.ServiceLocationPhoneNumberTypeId AS ServiceLocationPhoneNumberTypeId, model.PhoneNumbers_Partition7.UserPhoneNumberTypeId AS UserPhoneNumberTypeId FROM model.PhoneNumbers_Partition7 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PhoneNumbers_Partition8.Id AS Id, model.PhoneNumbers_Partition8.ContactId AS ContactId, model.PhoneNumbers_Partition8.BillingOrganizationId AS BillingOrganizationId, model.PhoneNumbers_Partition8.CountryCode AS CountryCode, model.PhoneNumbers_Partition8.AreaCode AS AreaCode, model.PhoneNumbers_Partition8.ExchangeAndSuffix AS ExchangeAndSuffix, model.PhoneNumbers_Partition8.Extension AS Extension, model.PhoneNumbers_Partition8.ExternalOrganizationId AS ExternalOrganizationId, model.PhoneNumbers_Partition8.InsurerId AS InsurerId, model.PhoneNumbers_Partition8.IsInternational AS IsInternational, model.PhoneNumbers_Partition8.OrdinalId AS OrdinalId, model.PhoneNumbers_Partition8.PatientId AS PatientId, model.PhoneNumbers_Partition8.ServiceLocationId AS ServiceLocationId, model.PhoneNumbers_Partition8.UserId AS UserId, model.PhoneNumbers_Partition8.__EntityType__ AS __EntityType__, model.PhoneNumbers_Partition8.BillingOrganizationPhoneNumberTypeId AS BillingOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition8.ContactPhoneNumberTypeId AS ContactPhoneNumberTypeId, model.PhoneNumbers_Partition8.ExternalOrganizationPhoneNumberTypeId AS ExternalOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition8.InsurerPhoneNumberTypeId AS InsurerPhoneNumberTypeId, model.PhoneNumbers_Partition8.PatientPhoneNumberTypeId AS PatientPhoneNumberTypeId, model.PhoneNumbers_Partition8.ServiceLocationPhoneNumberTypeId AS ServiceLocationPhoneNumberTypeId, model.PhoneNumbers_Partition8.UserPhoneNumberTypeId AS UserPhoneNumberTypeId FROM model.PhoneNumbers_Partition8 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PhoneNumbers_Partition9.Id AS Id, model.PhoneNumbers_Partition9.ContactId AS ContactId, model.PhoneNumbers_Partition9.BillingOrganizationId AS BillingOrganizationId, model.PhoneNumbers_Partition9.CountryCode AS CountryCode, model.PhoneNumbers_Partition9.AreaCode AS AreaCode, model.PhoneNumbers_Partition9.ExchangeAndSuffix AS ExchangeAndSuffix, model.PhoneNumbers_Partition9.Extension AS Extension, model.PhoneNumbers_Partition9.ExternalOrganizationId AS ExternalOrganizationId, model.PhoneNumbers_Partition9.InsurerId AS InsurerId, model.PhoneNumbers_Partition9.IsInternational AS IsInternational, model.PhoneNumbers_Partition9.OrdinalId AS OrdinalId, model.PhoneNumbers_Partition9.PatientId AS PatientId, model.PhoneNumbers_Partition9.ServiceLocationId AS ServiceLocationId, model.PhoneNumbers_Partition9.UserId AS UserId, model.PhoneNumbers_Partition9.__EntityType__ AS __EntityType__, model.PhoneNumbers_Partition9.BillingOrganizationPhoneNumberTypeId AS BillingOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition9.ContactPhoneNumberTypeId AS ContactPhoneNumberTypeId, model.PhoneNumbers_Partition9.ExternalOrganizationPhoneNumberTypeId AS ExternalOrganizationPhoneNumberTypeId, model.PhoneNumbers_Partition9.InsurerPhoneNumberTypeId AS InsurerPhoneNumberTypeId, model.PhoneNumbers_Partition9.PatientPhoneNumberTypeId AS PatientPhoneNumberTypeId, model.PhoneNumbers_Partition9.ServiceLocationPhoneNumberTypeId AS ServiceLocationPhoneNumberTypeId, model.PhoneNumbers_Partition9.UserPhoneNumberTypeId AS UserPhoneNumberTypeId FROM model.PhoneNumbers_Partition9 WITH(NOEXPAND)
	
		UNION ALL
	
		----PhoneNumber Patient - Emergency
		SELECT (55000000 * 12 + PatientId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			pd.EmergencyPhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			(202000000 * 1 + PatientId) AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			''PatientPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			pct.Id + 1000 AS PatientPhoneNumberTypeId,
			NULL AS ServiceLocationPhoneNumberTypeId,
			NULL AS UserPhoneNumberTypeId
		FROM dbo.PatientDemographics pd
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Emergency''
			AND pct.ReferenceType = ''PHONETYPEPATIENT''
		WHERE EmergencyName <> ''''
			AND EmergencyName IS NOT NULL
			AND EmergencyPhone <> ''''
			AND EmergencyPhone IS NOT NULL
	
		UNION ALL
	
		----PhoneNumber Patient - PatientDemoAlt 1 and 2 HomePhone  (x = 13 and 14)
		--*/*(no index)*/*
		SELECT (55000000 * (pdAlt.AltId + 12) + PatientId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			pdAlt.HomePhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			PatientId AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			''PatientPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			CASE pdAlt.PhoneType1
				WHEN 0
					THEN (SELECT Id + 1000 AS PctId FROM dbo.PracticeCodeTable WHERE ReferenceType = ''PHONETYPEPATIENT'' AND Code = ''Unknown'')
				ELSE pct.Id + 1000 	END AS PatientPhoneNumberTypeId,
			NULL AS ServiceLocationPhoneNumberTypeId,
			NULL AS UserPhoneNumberTypeId
		FROM dbo.PatientDemoAlt pdAlt
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Id = pdAlt.PhoneType1
			AND pct.ReferenceType = ''PHONETYPE''
		WHERE pdAlt.HomePhone <> ''''
			AND pdAlt.HomePhone IS NOT NULL
	
		UNION ALL
	
		----PhoneNumber - Patient - EmployerPhone
		--*/*(no index)*/*
		SELECT (55000000 * 31 + PatientId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			pd.EmployerPhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			PatientId AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			''PatientPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			pct.Id + 1000 AS PatientPhoneNumberTypeId,
			null AS ServiceLocationPhoneNumberTypeId,
			NULL AS UserPhoneNumberType
		FROM dbo.PatientDemographics pd
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Employer''
			AND pct.ReferenceType = ''PHONETYPEPATIENT''
		WHERE EmployerPhone <> ''''
			AND EmployerPhone IS NOT NULL
	
		UNION ALL
	
		----PhoneNumber Patient - PatientDemoAlt 1 and 2 .WorkPhone (x = 15 and 16)
		--*/*(no index)*/*
		SELECT (55000000 * (pdAlt.AltId + 14) + PatientId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			pdAlt.WorkPhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			PatientId AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			''PatientPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			CASE pdAlt.PhoneType2
				WHEN 0
					THEN (SELECT Id + 1000 AS PctId FROM dbo.PracticeCodeTable WHERE ReferenceType = ''PHONETYPEPATIENT'' AND Code = ''Unknown'')
				ELSE pct.Id + 1000 END AS PatientPhoneNumberTypeId,
			NULL AS ServiceLocationPhoneNumberTypeId,
			NULL AS UserPhoneNumberTypeId
		FROM dbo.PatientDemoAlt pdAlt
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Id = pdAlt.PhoneType2
			AND pct.ReferenceType = ''PHONETYPE''
		WHERE pdAlt.WorkPhone <> ''''
			AND pdAlt.WorkPhone IS NOT NULL
	
		UNION ALL
	
		----PhoneNumber Patient - PatientDemoAlt 1 and 2 .CellPhone (x = 17 and 18)
	
		SELECT (55000000 * (pdAlt.AltId + 16) + PatientId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			pdAlt.CellPhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			PatientId AS PatientId,
			NULL AS ServiceLocationId,
			NULL AS UserId,
			''PatientPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			CASE pdAlt.PhoneType3
				WHEN 0
					THEN (SELECT Id + 1000 AS PctId FROM dbo.PracticeCodeTable WHERE ReferenceType = ''PHONETYPEPATIENT'' AND Code = ''Unknown'')
				ELSE pct.Id + 1000 END AS PatientPhoneNumberTypeId,
			NULL AS ServiceLocationPhoneNumberTypeId,
			NULL AS UserPhoneNumberTypeId
		FROM dbo.PatientDemoAlt pdAlt
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Id = pdAlt.PhoneType3
			AND pct.ReferenceType = ''PHONETYPE''
		WHERE pdAlt.CellPhone <> ''''
			AND pdAlt.CellPhone IS NOT NULL
	
		UNION ALL
	
		----PhoneNumber ServiceLocation PracticeName Phone 1
		SELECT (55000000 * 5 + pnServLoc.PracticeId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			PracticePhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			NULL AS PatientId,
			pnServLoc.PracticeId AS ServiceLocationId,
			NULL AS UserId,
			''ServiceLocationPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			NULL AS PatientPhoneNumberTypeId,
			pct.Id + 1000 AS ServiceLocationPhoneNumberTypeId,
			NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeName pnServLoc
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Primary''
			AND pct.ReferenceType = ''PHONETYPESERVICELOCATION''
		WHERE PracticePhone <> ''''
			AND PracticePhone IS NOT NULL
			AND PracticeType = ''P''
	
		UNION ALL
	
		----PhoneNumber ServiceLocation PracticeName Phone 2
		SELECT (55000000 * 6 + pnServLoc.PracticeId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			PracticeOtherPhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			NULL AS PatientId,
			pnServLoc.PracticeId AS ServiceLocationId,
			NULL AS UserId,
			''ServiceLocationPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			NULL AS PatientPhoneNumberTypeId,
			pct.Id + 1000 AS ServiceLocationPhoneNumberTypeId,
			NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeName pnServLoc
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Other''
			AND pct.ReferenceType = ''PHONETYPESERVICELOCATION''
		WHERE PracticeOtherPhone <> ''''
			AND PracticeOtherPhone IS NOT NULL
			AND PracticeType = ''P''
	
		UNION ALL
	
		----PhoneNumber ServiceLocation PracticeName Fax
		SELECT (55000000 * 7 + pnServLoc.PracticeId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			PracticeFax AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			NULL AS PatientId,
			pnServLoc.PracticeId AS ServiceLocationId,
			NULL AS UserId,
			''ServiceLocationPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			NULL AS PatientPhoneNumberTypeId,
			pct.Id + 1000 AS ServiceLocationPhoneNumberTypeId,
			NULL AS UserPhoneNumberTypeId
		FROM dbo.PracticeName pnServLoc
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Fax''
			AND pct.ReferenceType = ''PHONETYPESERVICELOCATION''
		WHERE PracticeFax <> ''''
			AND PracticeFax IS NOT NULL
			AND PracticeType = ''P''
	
		UNION ALL
	
		----PhoneNumber ServiceLocation Resources (external facilities) (not override which is never a service location)
		SELECT (55000000 * 8 + reServLoc.ResourceId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			reServLoc.ResourcePhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			NULL AS PatientId,
			(110000000 * 1 + reServLoc.ResourceId) AS ServiceLocationId,
			NULL AS UserId,
			''ServiceLocationPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			NULL AS PatientPhoneNumberTypeId,
			pct.Id + 1000 AS ServiceLocationPhoneNumberTypeId,
			NULL AS UserPhoneNumberTypeId
		FROM dbo.Resources reServLoc
		LEFT OUTER JOIN dbo.PracticeCodeTable pct 
			ON pct.Code = ''Primary''
			AND pct.ReferenceType = ''PHONETYPESERVICELOCATION''
		WHERE reServLoc.ResourcePhone <> ''''
			AND reServLoc.ResourcePhone IS NOT NULL
			AND reServLoc.ResourceType = ''R''
			AND reServLoc.ServiceCode = ''02''
		GROUP BY reServLoc.ResourceId,
			reServLoc.ResourcePhone,
			pct.Id
	
		UNION ALL
	
		----PhoneNumber User 
		SELECT ResourceId AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			re.ResourcePhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			NULL AS PatientId,
			NULL AS ServiceLocationId,
			re.ResourceId AS UserId,
			''UserPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			NULL AS PatientPhoneNumberTypeId,
			NULL AS ServiceLocationPhoneNumberTypeId,
			10 AS UserPhoneNumberTypeId
		FROM dbo.Resources re
		WHERE re.ResourceType <> ''R''
			AND ResourcePhone <> ''''
			AND ResourcePhone IS NOT NULL
	
		UNION ALL
	
		----PhoneNumber User Fax
		SELECT (55000000 * 32 + ResourceId) AS Id,
			NULL AS ContactId,
			NULL AS BillingOrganizationId,
			CONVERT(NVARCHAR, NULL) AS CountryCode,
			CONVERT(NVARCHAR, NULL) AS AreaCode,
			re.ResourcePhone AS ExchangeAndSuffix,
			CONVERT(NVARCHAR, NULL) AS Extension,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			CONVERT(bit, 0) AS IsInternational,
			0 AS OrdinalId,
			NULL AS PatientId,
			NULL AS ServiceLocationId,
			re.ResourceId AS UserId,
			''UserPhoneNumber'' as __EntityType__,
			NULL AS BillingOrganizationPhoneNumberTypeId,
			NULL AS ContactPhoneNumberTypeId,
			NULL AS ExternalOrganizationPhoneNumberTypeId,
			NULL AS InsurerPhoneNumberTypeId,
			NULL AS PatientPhoneNumberTypeId,
			NULL AS ServiceLocationPhoneNumberTypeId,
			11 AS UserPhoneNumberTypeId
		FROM dbo.Resources re
		WHERE re.ResourceType <> ''R''
			AND ResourcePhone <> ''''
			AND ResourcePhone IS NOT NULL'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumbers'
			,@PhoneNumbersViewSql
			,@isChanged = @isPhoneNumbersChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPhoneNumberDelete'
				,
				'
			CREATE TRIGGER model.OnPhoneNumberDelete ON [model].[PhoneNumbers] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @ExchangeAndSuffix nvarchar(max)
			DECLARE @Extension nvarchar(max)
			DECLARE @IsInternational bit
			DECLARE @OrdinalId int
			DECLARE @AreaCode nvarchar(max)
			DECLARE @CountryCode nvarchar(max)
			DECLARE @UserId int
			DECLARE @UserPhoneNumberTypeId int
			DECLARE @BillingOrganizationId int
			DECLARE @BillingOrganizationPhoneNumberTypeId int
			DECLARE @ServiceLocationId int
			DECLARE @ServiceLocationPhoneNumberTypeId int
			DECLARE @ContactId int
			DECLARE @ContactPhoneNumberTypeId int
			DECLARE @ExternalOrganizationId int
			DECLARE @ExternalOrganizationPhoneNumberTypeId int
			DECLARE @InsurerId int
			DECLARE @InsurerPhoneNumberTypeId int
			DECLARE @PatientId int
			DECLARE @PatientPhoneNumberTypeId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE PhoneNumbersDeletedCursor CURSOR FOR
			SELECT Id, ExchangeAndSuffix, Extension, IsInternational, OrdinalId, AreaCode, CountryCode, UserId, UserPhoneNumberTypeId, BillingOrganizationId, BillingOrganizationPhoneNumberTypeId, ServiceLocationId, ServiceLocationPhoneNumberTypeId, ContactId, ContactPhoneNumberTypeId, ExternalOrganizationId, ExternalOrganizationPhoneNumberTypeId, InsurerId, InsurerPhoneNumberTypeId, PatientId, PatientPhoneNumberTypeId, __EntityType__ FROM deleted
		
			OPEN PhoneNumbersDeletedCursor
			FETCH NEXT FROM PhoneNumbersDeletedCursor INTO @Id, @ExchangeAndSuffix, @Extension, @IsInternational, @OrdinalId, @AreaCode, @CountryCode, @UserId, @UserPhoneNumberTypeId, @BillingOrganizationId, @BillingOrganizationPhoneNumberTypeId, @ServiceLocationId, @ServiceLocationPhoneNumberTypeId, @ContactId, @ContactPhoneNumberTypeId, @ExternalOrganizationId, @ExternalOrganizationPhoneNumberTypeId, @InsurerId, @InsurerPhoneNumberTypeId, @PatientId, @PatientPhoneNumberTypeId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PhoneNumber --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @ExchangeAndSuffix AS ExchangeAndSuffix, @Extension AS Extension, @IsInternational AS IsInternational, @OrdinalId AS OrdinalId, @AreaCode AS AreaCode, @CountryCode AS CountryCode, @UserId AS UserId, @UserPhoneNumberTypeId AS UserPhoneNumberTypeId, @BillingOrganizationId AS BillingOrganizationId, @BillingOrganizationPhoneNumberTypeId AS BillingOrganizationPhoneNumberTypeId, @ServiceLocationId AS ServiceLocationId, @ServiceLocationPhoneNumberTypeId AS ServiceLocationPhoneNumberTypeId, @ContactId AS ContactId, @ContactPhoneNumberTypeId AS ContactPhoneNumberTypeId, @ExternalOrganizationId AS ExternalOrganizationId, @ExternalOrganizationPhoneNumberTypeId AS ExternalOrganizationPhoneNumberTypeId, @InsurerId AS InsurerId, @InsurerPhoneNumberTypeId AS InsurerPhoneNumberTypeId, @PatientId AS PatientId, @PatientPhoneNumberTypeId AS PatientPhoneNumberTypeId, @__EntityType__ AS __EntityType__
			
				FETCH NEXT FROM PhoneNumbersDeletedCursor INTO @Id, @ExchangeAndSuffix, @Extension, @IsInternational, @OrdinalId, @AreaCode, @CountryCode, @UserId, @UserPhoneNumberTypeId, @BillingOrganizationId, @BillingOrganizationPhoneNumberTypeId, @ServiceLocationId, @ServiceLocationPhoneNumberTypeId, @ContactId, @ContactPhoneNumberTypeId, @ExternalOrganizationId, @ExternalOrganizationPhoneNumberTypeId, @InsurerId, @InsurerPhoneNumberTypeId, @PatientId, @PatientPhoneNumberTypeId, @__EntityType__ 
		
			END
		
			CLOSE PhoneNumbersDeletedCursor
			DEALLOCATE PhoneNumbersDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPhoneNumberInsert'
				,
				'
			CREATE TRIGGER model.OnPhoneNumberInsert ON [model].[PhoneNumbers] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @ExchangeAndSuffix nvarchar(max)
			DECLARE @Extension nvarchar(max)
			DECLARE @IsInternational bit
			DECLARE @OrdinalId int
			DECLARE @AreaCode nvarchar(max)
			DECLARE @CountryCode nvarchar(max)
			DECLARE @UserId int
			DECLARE @UserPhoneNumberTypeId int
			DECLARE @BillingOrganizationId int
			DECLARE @BillingOrganizationPhoneNumberTypeId int
			DECLARE @ServiceLocationId int
			DECLARE @ServiceLocationPhoneNumberTypeId int
			DECLARE @ContactId int
			DECLARE @ContactPhoneNumberTypeId int
			DECLARE @ExternalOrganizationId int
			DECLARE @ExternalOrganizationPhoneNumberTypeId int
			DECLARE @InsurerId int
			DECLARE @InsurerPhoneNumberTypeId int
			DECLARE @PatientId int
			DECLARE @PatientPhoneNumberTypeId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE PhoneNumbersInsertedCursor CURSOR FOR
			SELECT Id, ExchangeAndSuffix, Extension, IsInternational, OrdinalId, AreaCode, CountryCode, UserId, UserPhoneNumberTypeId, BillingOrganizationId, BillingOrganizationPhoneNumberTypeId, ServiceLocationId, ServiceLocationPhoneNumberTypeId, ContactId, ContactPhoneNumberTypeId, ExternalOrganizationId, ExternalOrganizationPhoneNumberTypeId, InsurerId, InsurerPhoneNumberTypeId, PatientId, PatientPhoneNumberTypeId, __EntityType__ FROM inserted
		
			OPEN PhoneNumbersInsertedCursor
			FETCH NEXT FROM PhoneNumbersInsertedCursor INTO @Id, @ExchangeAndSuffix, @Extension, @IsInternational, @OrdinalId, @AreaCode, @CountryCode, @UserId, @UserPhoneNumberTypeId, @BillingOrganizationId, @BillingOrganizationPhoneNumberTypeId, @ServiceLocationId, @ServiceLocationPhoneNumberTypeId, @ContactId, @ContactPhoneNumberTypeId, @ExternalOrganizationId, @ExternalOrganizationPhoneNumberTypeId, @InsurerId, @InsurerPhoneNumberTypeId, @PatientId, @PatientPhoneNumberTypeId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- PhoneNumber Insert
				IF @BillingOrganizationId IS NOT NULL
					SET @Id = (
							SELECT model.GetPairId(CASE x
										WHEN 0
											THEN (
													CASE @BillingOrganizationPhoneNumberTypeId
														WHEN 9
															THEN 1
														WHEN 1
															THEN 2
														WHEN 6
															THEN 3
														END
													)
										WHEN 1
											THEN 4
										END, y, 55000000)
							FROM model.GetIdPair(@BillingOrganizationId, 110000000)
							)
			
				IF @ServiceLocationId IS NOT NULL
					SET @Id = (
							SELECT model.GetPairId(CASE x
										WHEN 0
											THEN (
													CASE @ServiceLocationPhoneNumberTypeId
														WHEN (
																SELECT Id + 1000
																FROM dbo.PracticeCodeTable
																WHERE Code = ''Primary''
																	AND ReferenceType = ''PHONETYPESERVICELOCATION''
																)
															THEN 5
														WHEN (
																SELECT Id + 1000
																FROM dbo.PracticeCodeTable
																WHERE Code = ''Other''
																	AND ReferenceType = ''PHONETYPESERVICELOCATION''
																)
															THEN 6
														WHEN (
																SELECT Id + 1000
																FROM dbo.PracticeCodeTable
																WHERE Code = ''Fax''
																	AND ReferenceType = ''PHONETYPESERVICELOCATION''
																)
															THEN 7
														END
													)
										WHEN 1
											THEN 8
										END, y, 55000000)
							FROM model.GetIdPair(@ServiceLocationId, 110000000)
							)
			
				IF @PatientId IS NOT NULL
				BEGIN
					IF @PatientPhoneNumberTypeId IN (
							7
							,2
							,3
							,31
							,(
								SELECT Id + 1000
								FROM dbo.PracticeCodeTable
								WHERE Code = ''Emergency''
									AND ReferenceType = ''PHONETYPEPATIENT''
								)
							,(
								SELECT Id + 1000
								FROM dbo.PracticeCodeTable
								WHERE Code = ''Employer''
									AND ReferenceType = ''PHONETYPEPATIENT''
								)
							)
						SET @Id = (
								SELECT model.GetPairId(CASE @PatientPhoneNumberTypeId
											WHEN 7
												THEN 9
											WHEN 2
												THEN 10
											WHEN 3
												THEN 11
											WHEN (
													SELECT Id + 1000
													FROM dbo.PracticeCodeTable
													WHERE Code = ''Emergency''
														AND ReferenceType = ''PHONETYPEPATIENT''
													)
												THEN 12
											WHEN (
													SELECT Id + 1000
													FROM dbo.PracticeCodeTable
													WHERE Code = ''Employer''
														AND ReferenceType = ''PHONETYPEPATIENT''
													)
												THEN 31
											END, y, 55000000)
								FROM GetIdPair(@PatientId, 202000000)
								)
					ELSE
						RAISERROR (
								''Updating patient this type of patient phone number is not yet supported, please chose another phone type.''
								,10
								,1
								)
				END
			
				IF @ContactId IS NOT NULL
					SET @Id = (
							SELECT model.GetPairId(CASE @ContactPhoneNumberTypeId
										WHEN 5
											THEN 19
										WHEN 4
											THEN 20
										WHEN (
												SELECT Id + 1000
												FROM dbo.PracticeCodeTable
												WHERE Code = ''Cell''
													AND ReferenceType = ''PHONETYPECONTACT''
												)
											THEN 21
										END, @ContactId, 55000000)
								
							)
			
				IF @ExternalOrganizationId IS NOT NULL
					SET @Id = (
							SELECT model.GetPairId(CASE @ExternalOrganizationPhoneNumberTypeId
										WHEN (
												SELECT Id + 1000
												FROM dbo.PracticeCodeTable
												WHERE Code = ''Primary''
													AND ReferenceType = ''PHONETYPEEXTERNALORGANIZATION''
												)
											THEN 22
										WHEN (
												SELECT Id + 1000
												FROM dbo.PracticeCodeTable
												WHERE Code = ''Fax''
													AND ReferenceType = ''PHONETYPEEXTERNALORGANIZATION''
												)
											THEN 23
										END, @ExternalOrganizationId, 55000000)
							)
			
				IF @InsurerId IS NOT NULL
					SET @Id = (
							SELECT model.GetPairId(CASE @InsurerPhoneNumberTypeId
										WHEN 8
											THEN 25
										WHEN (
												SELECT Id + 1000
												FROM dbo.PracticeCodeTable
												WHERE Code = ''Authorization''
													AND ReferenceType = ''PHONETYPEINSURER''
												)
											THEN 26
										WHEN (
												SELECT Id + 1000
												FROM dbo.PracticeCodeTable
												WHERE Code = ''Eligibility''
													AND ReferenceType = ''PHONETYPEINSURER''
												)
											THEN 27
										WHEN (
												SELECT Id + 1000
												FROM dbo.PracticeCodeTable
												WHERE Code = ''Provider''
													AND ReferenceType = ''PHONETYPEINSURER''
												)
											THEN 28
										WHEN (
												SELECT Id + 1000
												FROM dbo.PracticeCodeTable
												WHERE Code = ''Claims''
													AND ReferenceType = ''PHONETYPEINSURER''
												)
											THEN 29
										WHEN (
												SELECT Id + 1000
												FROM dbo.PracticeCodeTable
												WHERE Code = ''Submissions''
													AND ReferenceType = ''PHONETYPEINSURER''
												)
											THEN 30
										END, @InsurerId, 55000000)
							)
			
				IF @UserId IS NOT NULL
					SET @Id = model.GetPairId(0, @UserId, 55000000)
			
							
				-- PhoneNumber Update/Insert
				DECLARE @x int
					,@y int
			
				SELECT @x = x
					,@y = y
				FROM model.GetIdPair(@Id, 55000000)
			
				---BillingOrganization
				IF @x IN (
						1
						,5
						)
					UPDATE PracticeName
					SET PracticePhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PracticeId = @y
			
				IF @x IN (
						2
						,6
						)
					UPDATE PracticeName
					SET PracticeOtherPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PracticeId = @y
			
				IF @x IN (
						3
						,7
						)
					UPDATE PracticeName
					SET PracticeFax = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PracticeId = @y
			
				IF @x IN (
						4
						,8
						,0
						)
					UPDATE Resources
					SET ResourcePhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE ResourceId = @y
			
				IF @x = 9
					UPDATE PatientDemographics
					SET HomePhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PatientId = @y
			
				IF @x = 10
					UPDATE PatientDemographics
					SET BusinessPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PatientId = @y
			
				IF @x = 11
					UPDATE PatientDemographics
					SET CellPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PatientId = @y
			
				IF @x = 12
					UPDATE PatientDemographics
					SET EmergencyPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PatientId = @y
			
				IF @x = 31
					UPDATE PatientDemographics
					SET EmployerPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PatientId = @y
			
				IF @x IN (
						19
						,22
						)
					UPDATE PracticeVendors
					SET VendorPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE VendorId = @y
			
				IF @x IN (
						20
						,23
						)
					UPDATE PracticeVendors
					SET VendorFax = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE VendorId = @y
			
				IF @x = 21
					UPDATE PracticeVendors
					SET VendorCellPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE VendorId = @y
			
				IF @x = 25
					UPDATE PracticeInsurers
					SET InsurerPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE InsurerId = @y
			
				IF @x = 26
					UPDATE PracticeInsurers
					SET InsurerPrecPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE InsurerId = @y
			
				IF @x = 27
					UPDATE PracticeInsurers
					SET InsurerEligPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE InsurerId = @y
			
				IF @x = 28
					UPDATE PracticeInsurers
					SET InsurerProvPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE InsurerId = @y
			
				IF @x = 29
					UPDATE PracticeInsurers
					SET InsurerClaimPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE InsurerId = @y
			
				IF @x = 30
					UPDATE PracticeInsurers
					SET InsurerENumber = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE InsurerId = @y
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @ExchangeAndSuffix AS ExchangeAndSuffix, @Extension AS Extension, @IsInternational AS IsInternational, @OrdinalId AS OrdinalId, @AreaCode AS AreaCode, @CountryCode AS CountryCode, @UserId AS UserId, @UserPhoneNumberTypeId AS UserPhoneNumberTypeId, @BillingOrganizationId AS BillingOrganizationId, @BillingOrganizationPhoneNumberTypeId AS BillingOrganizationPhoneNumberTypeId, @ServiceLocationId AS ServiceLocationId, @ServiceLocationPhoneNumberTypeId AS ServiceLocationPhoneNumberTypeId, @ContactId AS ContactId, @ContactPhoneNumberTypeId AS ContactPhoneNumberTypeId, @ExternalOrganizationId AS ExternalOrganizationId, @ExternalOrganizationPhoneNumberTypeId AS ExternalOrganizationPhoneNumberTypeId, @InsurerId AS InsurerId, @InsurerPhoneNumberTypeId AS InsurerPhoneNumberTypeId, @PatientId AS PatientId, @PatientPhoneNumberTypeId AS PatientPhoneNumberTypeId, @__EntityType__ AS __EntityType__
				
				FETCH NEXT FROM PhoneNumbersInsertedCursor INTO @Id, @ExchangeAndSuffix, @Extension, @IsInternational, @OrdinalId, @AreaCode, @CountryCode, @UserId, @UserPhoneNumberTypeId, @BillingOrganizationId, @BillingOrganizationPhoneNumberTypeId, @ServiceLocationId, @ServiceLocationPhoneNumberTypeId, @ContactId, @ContactPhoneNumberTypeId, @ExternalOrganizationId, @ExternalOrganizationPhoneNumberTypeId, @InsurerId, @InsurerPhoneNumberTypeId, @PatientId, @PatientPhoneNumberTypeId, @__EntityType__ 
		
			END
		
			CLOSE PhoneNumbersInsertedCursor
			DEALLOCATE PhoneNumbersInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPhoneNumberUpdate'
				,
				'
			CREATE TRIGGER model.OnPhoneNumberUpdate ON [model].[PhoneNumbers] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @ExchangeAndSuffix nvarchar(max)
			DECLARE @Extension nvarchar(max)
			DECLARE @IsInternational bit
			DECLARE @OrdinalId int
			DECLARE @AreaCode nvarchar(max)
			DECLARE @CountryCode nvarchar(max)
			DECLARE @UserId int
			DECLARE @UserPhoneNumberTypeId int
			DECLARE @BillingOrganizationId int
			DECLARE @BillingOrganizationPhoneNumberTypeId int
			DECLARE @ServiceLocationId int
			DECLARE @ServiceLocationPhoneNumberTypeId int
			DECLARE @ContactId int
			DECLARE @ContactPhoneNumberTypeId int
			DECLARE @ExternalOrganizationId int
			DECLARE @ExternalOrganizationPhoneNumberTypeId int
			DECLARE @InsurerId int
			DECLARE @InsurerPhoneNumberTypeId int
			DECLARE @PatientId int
			DECLARE @PatientPhoneNumberTypeId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE PhoneNumbersUpdatedCursor CURSOR FOR
			SELECT Id, ExchangeAndSuffix, Extension, IsInternational, OrdinalId, AreaCode, CountryCode, UserId, UserPhoneNumberTypeId, BillingOrganizationId, BillingOrganizationPhoneNumberTypeId, ServiceLocationId, ServiceLocationPhoneNumberTypeId, ContactId, ContactPhoneNumberTypeId, ExternalOrganizationId, ExternalOrganizationPhoneNumberTypeId, InsurerId, InsurerPhoneNumberTypeId, PatientId, PatientPhoneNumberTypeId, __EntityType__ FROM inserted
		
			OPEN PhoneNumbersUpdatedCursor
			FETCH NEXT FROM PhoneNumbersUpdatedCursor INTO @Id, @ExchangeAndSuffix, @Extension, @IsInternational, @OrdinalId, @AreaCode, @CountryCode, @UserId, @UserPhoneNumberTypeId, @BillingOrganizationId, @BillingOrganizationPhoneNumberTypeId, @ServiceLocationId, @ServiceLocationPhoneNumberTypeId, @ContactId, @ContactPhoneNumberTypeId, @ExternalOrganizationId, @ExternalOrganizationPhoneNumberTypeId, @InsurerId, @InsurerPhoneNumberTypeId, @PatientId, @PatientPhoneNumberTypeId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- PhoneNumber Update/Insert
				DECLARE @x int
					,@y int
			
				SELECT @x = x
					,@y = y
				FROM model.GetIdPair(@Id, 55000000)
			
				---BillingOrganization
				IF @x IN (
						1
						,5
						)
					UPDATE PracticeName
					SET PracticePhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PracticeId = @y
			
				IF @x IN (
						2
						,6
						)
					UPDATE PracticeName
					SET PracticeOtherPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PracticeId = @y
			
				IF @x IN (
						3
						,7
						)
					UPDATE PracticeName
					SET PracticeFax = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PracticeId = @y
			
				IF @x IN (
						4
						,8
						,0
						)
					UPDATE Resources
					SET ResourcePhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE ResourceId = @y
			
				IF @x = 9
					UPDATE PatientDemographics
					SET HomePhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PatientId = @y
			
				IF @x = 10
					UPDATE PatientDemographics
					SET BusinessPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PatientId = @y
			
				IF @x = 11
					UPDATE PatientDemographics
					SET CellPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PatientId = @y
			
				IF @x = 12
					UPDATE PatientDemographics
					SET EmergencyPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PatientId = @y
			
				IF @x = 31
					UPDATE PatientDemographics
					SET EmployerPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE PatientId = @y
			
				IF @x IN (
						19
						,22
						)
					UPDATE PracticeVendors
					SET VendorPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE VendorId = @y
			
				IF @x IN (
						20
						,23
						)
					UPDATE PracticeVendors
					SET VendorFax = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE VendorId = @y
			
				IF @x = 21
					UPDATE PracticeVendors
					SET VendorCellPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE VendorId = @y
			
				IF @x = 25
					UPDATE PracticeInsurers
					SET InsurerPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE InsurerId = @y
			
				IF @x = 26
					UPDATE PracticeInsurers
					SET InsurerPrecPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE InsurerId = @y
			
				IF @x = 27
					UPDATE PracticeInsurers
					SET InsurerEligPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE InsurerId = @y
			
				IF @x = 28
					UPDATE PracticeInsurers
					SET InsurerProvPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE InsurerId = @y
			
				IF @x = 29
					UPDATE PracticeInsurers
					SET InsurerClaimPhone = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE InsurerId = @y
			
				IF @x = 30
					UPDATE PracticeInsurers
					SET InsurerENumber = CASE 
							WHEN @AreaCode IS NOT NULL
								THEN @AreaCode
							ELSE ''''
							END + CASE 
							WHEN @ExchangeAndSuffix IS NOT NULL
								THEN @ExchangeAndSuffix
							ELSE ''''
							END + CASE 
							WHEN @Extension IS NOT NULL
								THEN @Extension
							ELSE ''''
							END
					WHERE InsurerId = @y
				
				FETCH NEXT FROM PhoneNumbersUpdatedCursor INTO @Id, @ExchangeAndSuffix, @Extension, @IsInternational, @OrdinalId, @AreaCode, @CountryCode, @UserId, @UserPhoneNumberTypeId, @BillingOrganizationId, @BillingOrganizationPhoneNumberTypeId, @ServiceLocationId, @ServiceLocationPhoneNumberTypeId, @ContactId, @ContactPhoneNumberTypeId, @ExternalOrganizationId, @ExternalOrganizationPhoneNumberTypeId, @InsurerId, @InsurerPhoneNumberTypeId, @PatientId, @PatientPhoneNumberTypeId, @__EntityType__ 
		
			END
		
			CLOSE PhoneNumbersUpdatedCursor
			DEALLOCATE PhoneNumbersUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.PhoneNumberTypes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isPhoneNumberTypesChanged BIT

		SET @isPhoneNumberTypesChanged = 0

		DECLARE @ismodelPhoneNumberTypes_Partition1Changed BIT

		SET @ismodelPhoneNumberTypes_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumberTypes_Partition1'
			,'
		CREATE VIEW model.PhoneNumberTypes_Partition1
		WITH SCHEMABINDING
		AS
		SELECT CASE
				WHEN Code = ''Billing''
					THEN 1
				WHEN Code = ''Primary''
					THEN 9
				WHEN Code = ''Fax''
					THEN 6
				ELSE pctBillingOrganization.Id + 1000
				END AS Id,
			AlternateCode AS Abbreviation,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived,
			''BillingOrganizationPhoneNumberType'' AS __EntityType__
		FROM dbo.PracticeCodeTable pctBillingOrganization 
		WHERE pctBillingOrganization.ReferenceType = ''PHONETYPEBILLINGORGANIZATION''
		'
			,@isChanged = @ismodelPhoneNumberTypes_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumberTypes_Partition1'
					AND object_id = OBJECT_ID('model.PhoneNumberTypes_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumberTypes_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumberTypes_Partition1 ON model.PhoneNumberTypes_Partition1 ([Id]);
		END

		DECLARE @ismodelPhoneNumberTypes_Partition2Changed BIT

		SET @ismodelPhoneNumberTypes_Partition2Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumberTypes_Partition2'
			,'
		CREATE VIEW model.PhoneNumberTypes_Partition2
		WITH SCHEMABINDING
		AS
		SELECT CASE 
			WHEN Code = ''Primary''
				THEN 5
			WHEN Code = ''Fax''
				THEN 4
			ELSE pctContact.Id + 1000
			END AS Id,
			AlternateCode AS Abbreviation,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived,
			''ContactPhoneNumberType'' AS __EntityType__
		FROM dbo.PracticeCodeTable pctContact
		WHERE pctContact.ReferenceType = ''PHONETYPECONTACT''
		'
			,@isChanged = @ismodelPhoneNumberTypes_Partition2Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumberTypes_Partition2'
					AND object_id = OBJECT_ID('model.PhoneNumberTypes_Partition2')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumberTypes_Partition2'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumberTypes_Partition2 ON model.PhoneNumberTypes_Partition2 ([Id]);
		END

		DECLARE @ismodelPhoneNumberTypes_Partition3Changed BIT

		SET @ismodelPhoneNumberTypes_Partition3Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumberTypes_Partition3'
			,'
		CREATE VIEW model.PhoneNumberTypes_Partition3
		WITH SCHEMABINDING
		AS
		SELECT pctExternalOrganization.Id + 1000 AS Id,
			AlternateCode AS Abbreviation,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived,
			''ExternalOrganizationPhoneNumberType'' AS __EntityType__
		FROM dbo.PracticeCodeTable pctExternalOrganization
		WHERE pctExternalOrganization.ReferenceType = ''PHONETYPEEXTERNALORGANIZATION''
		'
			,@isChanged = @ismodelPhoneNumberTypes_Partition3Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumberTypes_Partition3'
					AND object_id = OBJECT_ID('model.PhoneNumberTypes_Partition3')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumberTypes_Partition3'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumberTypes_Partition3 ON model.PhoneNumberTypes_Partition3 ([Id]);
		END

		DECLARE @ismodelPhoneNumberTypes_Partition4Changed BIT

		SET @ismodelPhoneNumberTypes_Partition4Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumberTypes_Partition4'
			,'
		CREATE VIEW model.PhoneNumberTypes_Partition4
		WITH SCHEMABINDING
		AS
		SELECT CASE 
			WHEN Code = ''Primary''
				THEN 8
			ELSE pctInsurer.Id + 1000 
			END AS Id,
			AlternateCode AS Abbreviation,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived,
			''InsurerPhoneNumberType'' AS __EntityType__
		FROM dbo.PracticeCodeTable pctInsurer
		WHERE pctInsurer.ReferenceType = ''PHONETYPEINSURER''
		'
			,@isChanged = @ismodelPhoneNumberTypes_Partition4Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumberTypes_Partition4'
					AND object_id = OBJECT_ID('model.PhoneNumberTypes_Partition4')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumberTypes_Partition4'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumberTypes_Partition4 ON model.PhoneNumberTypes_Partition4 ([Id]);
		END

		DECLARE @ismodelPhoneNumberTypes_Partition5Changed BIT

		SET @ismodelPhoneNumberTypes_Partition5Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumberTypes_Partition5'
			,'
		CREATE VIEW model.PhoneNumberTypes_Partition5
		WITH SCHEMABINDING
		AS
		SELECT CASE 
			WHEN Code = ''Home''
				THEN 7
			WHEN Code = ''Business''
				THEN 2
			WHEN Code = ''Cell''
				THEN 3
			ELSE pctPatient.Id + 1000
			END AS Id,
			AlternateCode AS Abbreviation,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived,
			''PatientPhoneNumberType'' AS __EntityType__
		FROM dbo.PracticeCodeTable pctPatient
		WHERE pctPatient.ReferenceType = ''PHONETYPEPATIENT''
		'
			,@isChanged = @ismodelPhoneNumberTypes_Partition5Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumberTypes_Partition5'
					AND object_id = OBJECT_ID('model.PhoneNumberTypes_Partition5')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumberTypes_Partition5'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumberTypes_Partition5 ON model.PhoneNumberTypes_Partition5 ([Id]);
		END

		DECLARE @ismodelPhoneNumberTypes_Partition6Changed BIT

		SET @ismodelPhoneNumberTypes_Partition6Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumberTypes_Partition6'
			,'
		CREATE VIEW model.PhoneNumberTypes_Partition6
		WITH SCHEMABINDING
		AS
		SELECT pctSureScripts.Id + 1000 AS Id,
			AlternateCode AS Abbreviation,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived,
			''PatientPhoneNumberType'' AS __EntityType__
		FROM dbo.PracticeCodeTable pctSureScripts 
		WHERE pctSureScripts.ReferenceType = ''PHONETYPE''
		'
			,@isChanged = @ismodelPhoneNumberTypes_Partition6Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumberTypes_Partition6'
					AND object_id = OBJECT_ID('model.PhoneNumberTypes_Partition6')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumberTypes_Partition6'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumberTypes_Partition6 ON model.PhoneNumberTypes_Partition6 ([Id]);
		END

		DECLARE @ismodelPhoneNumberTypes_Partition7Changed BIT

		SET @ismodelPhoneNumberTypes_Partition7Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumberTypes_Partition7'
			,'
		CREATE VIEW model.PhoneNumberTypes_Partition7
		WITH SCHEMABINDING
		AS
		SELECT pctServiceLocation.Id + 1000 AS Id,
			AlternateCode AS Abbreviation,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived,
			''ServiceLocationPhoneNumberType'' AS __EntityType__
		FROM dbo.PracticeCodeTable pctServiceLocation
		WHERE pctServiceLocation.ReferenceType = ''PHONETYPESERVICELOCATION''
		'
			,@isChanged = @ismodelPhoneNumberTypes_Partition7Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumberTypes_Partition7'
					AND object_id = OBJECT_ID('model.PhoneNumberTypes_Partition7')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumberTypes_Partition7'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumberTypes_Partition7 ON model.PhoneNumberTypes_Partition7 ([Id]);
		END

		DECLARE @ismodelPhoneNumberTypes_Partition8Changed BIT

		SET @ismodelPhoneNumberTypes_Partition8Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumberTypes_Partition8'
			,'
		CREATE VIEW model.PhoneNumberTypes_Partition8
		WITH SCHEMABINDING
		AS
		SELECT CASE
			WHEN Code = ''Primary''
				THEN 10
			WHEN Code = ''Fax''
				THEN 11
			ELSE pctUser.Id + 1000 
			END AS Id,
			AlternateCode AS Abbreviation,
			Code AS [Name],
			CONVERT(bit, 0) AS IsArchived,
			''UserPhoneNumberType'' AS __EntityType__
		FROM dbo.PracticeCodeTable pctUser
		WHERE pctUser.ReferenceType = ''PHONETYPEUSER''
		'
			,@isChanged = @ismodelPhoneNumberTypes_Partition8Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_PhoneNumberTypes_Partition8'
					AND object_id = OBJECT_ID('model.PhoneNumberTypes_Partition8')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.PhoneNumberTypes_Partition8'

			CREATE UNIQUE CLUSTERED INDEX PK_PhoneNumberTypes_Partition8 ON model.PhoneNumberTypes_Partition8 ([Id]);
		END

		DECLARE @PhoneNumberTypesViewSql NVARCHAR(max)

		SET @PhoneNumberTypesViewSql = 
			'
		CREATE VIEW [model].[PhoneNumberTypes]
		WITH SCHEMABINDING
		AS
		SELECT model.PhoneNumberTypes_Partition1.Id AS Id, model.PhoneNumberTypes_Partition1.Abbreviation AS Abbreviation, model.PhoneNumberTypes_Partition1.[Name] AS [Name], model.PhoneNumberTypes_Partition1.IsArchived AS IsArchived, model.PhoneNumberTypes_Partition1.__EntityType__ AS __EntityType__ FROM model.PhoneNumberTypes_Partition1 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PhoneNumberTypes_Partition2.Id AS Id, model.PhoneNumberTypes_Partition2.Abbreviation AS Abbreviation, model.PhoneNumberTypes_Partition2.[Name] AS [Name], model.PhoneNumberTypes_Partition2.IsArchived AS IsArchived, model.PhoneNumberTypes_Partition2.__EntityType__ AS __EntityType__ FROM model.PhoneNumberTypes_Partition2 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PhoneNumberTypes_Partition3.Id AS Id, model.PhoneNumberTypes_Partition3.Abbreviation AS Abbreviation, model.PhoneNumberTypes_Partition3.[Name] AS [Name], model.PhoneNumberTypes_Partition3.IsArchived AS IsArchived, model.PhoneNumberTypes_Partition3.__EntityType__ AS __EntityType__ FROM model.PhoneNumberTypes_Partition3 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PhoneNumberTypes_Partition4.Id AS Id, model.PhoneNumberTypes_Partition4.Abbreviation AS Abbreviation, model.PhoneNumberTypes_Partition4.[Name] AS [Name], model.PhoneNumberTypes_Partition4.IsArchived AS IsArchived, model.PhoneNumberTypes_Partition4.__EntityType__ AS __EntityType__ FROM model.PhoneNumberTypes_Partition4 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PhoneNumberTypes_Partition5.Id AS Id, model.PhoneNumberTypes_Partition5.Abbreviation AS Abbreviation, model.PhoneNumberTypes_Partition5.[Name] AS [Name], model.PhoneNumberTypes_Partition5.IsArchived AS IsArchived, model.PhoneNumberTypes_Partition5.__EntityType__ AS __EntityType__ FROM model.PhoneNumberTypes_Partition5 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PhoneNumberTypes_Partition6.Id AS Id, model.PhoneNumberTypes_Partition6.Abbreviation AS Abbreviation, model.PhoneNumberTypes_Partition6.[Name] AS [Name], model.PhoneNumberTypes_Partition6.IsArchived AS IsArchived, model.PhoneNumberTypes_Partition6.__EntityType__ AS __EntityType__ FROM model.PhoneNumberTypes_Partition6 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PhoneNumberTypes_Partition7.Id AS Id, model.PhoneNumberTypes_Partition7.Abbreviation AS Abbreviation, model.PhoneNumberTypes_Partition7.[Name] AS [Name], model.PhoneNumberTypes_Partition7.IsArchived AS IsArchived, model.PhoneNumberTypes_Partition7.__EntityType__ AS __EntityType__ FROM model.PhoneNumberTypes_Partition7 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.PhoneNumberTypes_Partition8.Id AS Id, model.PhoneNumberTypes_Partition8.Abbreviation AS Abbreviation, model.PhoneNumberTypes_Partition8.[Name] AS [Name], model.PhoneNumberTypes_Partition8.IsArchived AS IsArchived, model.PhoneNumberTypes_Partition8.__EntityType__ AS __EntityType__ FROM model.PhoneNumberTypes_Partition8 WITH(NOEXPAND)'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PhoneNumberTypes'
			,@PhoneNumberTypesViewSql
			,@isChanged = @isPhoneNumberTypesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPhoneNumberTypeDelete'
				,
				'
			CREATE TRIGGER model.OnPhoneNumberTypeDelete ON [model].[PhoneNumberTypes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @Abbreviation nvarchar(max)
			DECLARE @IsArchived bit
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE PhoneNumberTypesDeletedCursor CURSOR FOR
			SELECT Id, Name, Abbreviation, IsArchived, __EntityType__ FROM deleted
		
			OPEN PhoneNumberTypesDeletedCursor
			FETCH NEXT FROM PhoneNumberTypesDeletedCursor INTO @Id, @Name, @Abbreviation, @IsArchived, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for PhoneNumberType --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name, @Abbreviation AS Abbreviation, @IsArchived AS IsArchived, @__EntityType__ AS __EntityType__
			
				FETCH NEXT FROM PhoneNumberTypesDeletedCursor INTO @Id, @Name, @Abbreviation, @IsArchived, @__EntityType__ 
		
			END
		
			CLOSE PhoneNumberTypesDeletedCursor
			DEALLOCATE PhoneNumberTypesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPhoneNumberTypeInsert'
				,
				'
			CREATE TRIGGER model.OnPhoneNumberTypeInsert ON [model].[PhoneNumberTypes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @Abbreviation nvarchar(max)
			DECLARE @IsArchived bit
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE PhoneNumberTypesInsertedCursor CURSOR FOR
			SELECT Id, Name, Abbreviation, IsArchived, __EntityType__ FROM inserted
		
			OPEN PhoneNumberTypesInsertedCursor
			FETCH NEXT FROM PhoneNumberTypesInsertedCursor INTO @Id, @Name, @Abbreviation, @IsArchived, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for PhoneNumberType --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name, @Abbreviation AS Abbreviation, @IsArchived AS IsArchived, @__EntityType__ AS __EntityType__
				
				FETCH NEXT FROM PhoneNumberTypesInsertedCursor INTO @Id, @Name, @Abbreviation, @IsArchived, @__EntityType__ 
		
			END
		
			CLOSE PhoneNumberTypesInsertedCursor
			DEALLOCATE PhoneNumberTypesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnPhoneNumberTypeUpdate'
				,'
			CREATE TRIGGER model.OnPhoneNumberTypeUpdate ON [model].[PhoneNumberTypes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @Abbreviation nvarchar(max)
			DECLARE @IsArchived bit
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE PhoneNumberTypesUpdatedCursor CURSOR FOR
			SELECT Id, Name, Abbreviation, IsArchived, __EntityType__ FROM inserted
		
			OPEN PhoneNumberTypesUpdatedCursor
			FETCH NEXT FROM PhoneNumberTypesUpdatedCursor INTO @Id, @Name, @Abbreviation, @IsArchived, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for PhoneNumberType --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM PhoneNumberTypesUpdatedCursor INTO @Id, @Name, @Abbreviation, @IsArchived, @__EntityType__ 
		
			END
		
			CLOSE PhoneNumberTypesUpdatedCursor
			DEALLOCATE PhoneNumberTypesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ProviderInsurerClaimFileOverride')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isProviderInsurerClaimFileOverrideChanged BIT

		SET @isProviderInsurerClaimFileOverrideChanged = 0

		DECLARE @ProviderInsurerClaimFileOverrideViewSql NVARCHAR(max)

		SET @ProviderInsurerClaimFileOverrideViewSql = 
			'
		CREATE VIEW [model].[ProviderInsurerClaimFileOverride]
		WITH SCHEMABINDING
		AS
		-----Provider-Doctors with Override where ServiceLocation is from PracticeName table
		SELECT (CONVERT(bigint, 110000000) * (110000000 * 2 + pnServLoc.PracticeId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId)) AS ClaimFileOverrideProviders_Id
			,pins.InsurerId AS ClaimFileOverrideInsurers_Id
			,COUNT_BIG(*) AS Count
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId	
			AND pa.ResourceType = ''R''
			AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
		INNER JOIN dbo.PracticeName AS pnServLoc ON pnServLoc.PracticeType = ''P''
		INNER JOIN dbo.PracticeInsurers pins ON pins.InsurerId = pa.PlanId
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'') 
		GROUP BY (CONVERT(bigint, 110000000) * (110000000 * 2 + pnServLoc.PracticeId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId)),
			pins.InsurerId
	
		UNION ALL
	
		----Provider-Doctors w Override where ServiceLocation is from Resources table
		SELECT (CONVERT(bigint, 110000000) * (110000000 * 3 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId)) AS ClaimFileOverrideProviders_Id,
			pins.InsurerId AS ClaimFileOverrideInsurers_Id,	
			COUNT_BIG(*) AS Count
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId	
			AND pa.ResourceType = ''R''
			AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
		INNER JOIN dbo.PracticeInterfaceConfiguration AS pic ON pic.FieldReference = ''ASC''
		INNER JOIN dbo.PracticeName AS pnFacServLoc ON pnFacServLoc.PracticeType = ''P''
		INNER JOIN dbo.PracticeInsurers pins ON pins.InsurerId = pa.PlanId
		INNER JOIN dbo.Resources AS reServLoc ON pnFacServLoc.PracticeId = reServLoc.PracticeId
			AND reServLoc.ResourceType = ''R''
			AND reServLoc.ServiceCode = ''02''
			AND 1 = 
			CASE 
				WHEN reServLoc.Billable = ''Y''
					AND pic.FieldValue = ''T''
					AND pnFacServLoc.LocationReference <> ''''
					THEN 0
				ELSE 1
				END
		WHERE re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'') 
		GROUP BY reServLoc.ResourceId,
			re.ResourceId,
			re.Billable,
			re.GoDirect,
			pins.InsurerId'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ProviderInsurerClaimFileOverride'
			,@ProviderInsurerClaimFileOverrideViewSql
			,@isChanged = @isProviderInsurerClaimFileOverrideChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnProviderInsurerClaimFileOverrideDelete'
				,
				'
			CREATE TRIGGER model.OnProviderInsurerClaimFileOverrideDelete ON [model].[ProviderInsurerClaimFileOverride] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @ClaimFileOverrideProviders_Id bigint
			DECLARE @ClaimFileOverrideInsurers_Id int
		
			DECLARE ProviderInsurerClaimFileOverrideDeletedCursor CURSOR FOR
			SELECT ClaimFileOverrideProviders_Id, ClaimFileOverrideInsurers_Id FROM deleted
		
			OPEN ProviderInsurerClaimFileOverrideDeletedCursor
			FETCH NEXT FROM ProviderInsurerClaimFileOverrideDeletedCursor INTO @ClaimFileOverrideProviders_Id, @ClaimFileOverrideInsurers_Id 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ProviderInsurerClaimFileOverride --
				PRINT (''Not Implemented'')
			
				SELECT @ClaimFileOverrideProviders_Id AS ClaimFileOverrideProviders_Id, @ClaimFileOverrideInsurers_Id AS ClaimFileOverrideInsurers_Id
			
				FETCH NEXT FROM ProviderInsurerClaimFileOverrideDeletedCursor INTO @ClaimFileOverrideProviders_Id, @ClaimFileOverrideInsurers_Id 
		
			END
		
			CLOSE ProviderInsurerClaimFileOverrideDeletedCursor
			DEALLOCATE ProviderInsurerClaimFileOverrideDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnProviderInsurerClaimFileOverrideInsert'
				,
				'
			CREATE TRIGGER model.OnProviderInsurerClaimFileOverrideInsert ON [model].[ProviderInsurerClaimFileOverride] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @ClaimFileOverrideProviders_Id bigint
			DECLARE @ClaimFileOverrideInsurers_Id int
		
			DECLARE ProviderInsurerClaimFileOverrideInsertedCursor CURSOR FOR
			SELECT ClaimFileOverrideProviders_Id, ClaimFileOverrideInsurers_Id FROM inserted
		
			OPEN ProviderInsurerClaimFileOverrideInsertedCursor
			FETCH NEXT FROM ProviderInsurerClaimFileOverrideInsertedCursor INTO @ClaimFileOverrideProviders_Id, @ClaimFileOverrideInsurers_Id 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ProviderInsurerClaimFileOverride --
				PRINT (''Not Implemented'')
			
				SET @ClaimFileOverrideProviders_Id = CONVERT(bigint, NULL)
				SET @ClaimFileOverrideInsurers_Id = CONVERT(int, NULL)
			
				SELECT @ClaimFileOverrideProviders_Id AS ClaimFileOverrideProviders_Id, @ClaimFileOverrideInsurers_Id AS ClaimFileOverrideInsurers_Id
				
				FETCH NEXT FROM ProviderInsurerClaimFileOverrideInsertedCursor INTO @ClaimFileOverrideProviders_Id, @ClaimFileOverrideInsurers_Id 
		
			END
		
			CLOSE ProviderInsurerClaimFileOverrideInsertedCursor
			DEALLOCATE ProviderInsurerClaimFileOverrideInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnProviderInsurerClaimFileOverrideUpdate'
				,
				'
			CREATE TRIGGER model.OnProviderInsurerClaimFileOverrideUpdate ON [model].[ProviderInsurerClaimFileOverride] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @ClaimFileOverrideProviders_Id bigint
			DECLARE @ClaimFileOverrideInsurers_Id int
		
			DECLARE ProviderInsurerClaimFileOverrideUpdatedCursor CURSOR FOR
			SELECT ClaimFileOverrideProviders_Id, ClaimFileOverrideInsurers_Id FROM inserted
		
			OPEN ProviderInsurerClaimFileOverrideUpdatedCursor
			FETCH NEXT FROM ProviderInsurerClaimFileOverrideUpdatedCursor INTO @ClaimFileOverrideProviders_Id, @ClaimFileOverrideInsurers_Id 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ProviderInsurerClaimFileOverride --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ProviderInsurerClaimFileOverrideUpdatedCursor INTO @ClaimFileOverrideProviders_Id, @ClaimFileOverrideInsurers_Id 
		
			END
		
			CLOSE ProviderInsurerClaimFileOverrideUpdatedCursor
			DEALLOCATE ProviderInsurerClaimFileOverrideUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Providers')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isProvidersChanged BIT

		SET @isProvidersChanged = 0

		DECLARE @ProvidersViewSql NVARCHAR(max)

		SET @ProvidersViewSql = 
			'
		CREATE VIEW [model].[Providers]
		WITH SCHEMABINDING
		AS
		---human and facility providers where ServiceLocation is from PracticeName table, including override 
		SELECT 
			CASE 
				WHEN re.ResourceType = ''R'' ---internal facility
					THEN (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
				WHEN pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
					THEN (CONVERT(bigint, 110000000) * (110000000 * 2 + pnServLoc.PracticeId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId))
				ELSE (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
				END AS Id,
			CASE
				WHEN pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
					THEN (110000000 * 1 + re.ResourceId) 
				ELSE pnBillOrg.PracticeId
				END AS BillingOrganizationId,
			CASE re.Billable
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsBillable,
			CASE re.GoDirect
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsEMR,
			CASE 
				WHEN re.ResourceType = ''R''
					THEN pnBillOrg.PracticeId
				ELSE pnServLoc.PracticeId 
				END AS ServiceLocationId,
			CASE
				WHEN re.ResourceType = ''R''
					THEN NULL
				ELSE re.ResourceId 
				END AS UserId
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName AS pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
			AND pnBillOrg.PracticeType = ''P''
		INNER JOIN dbo.PracticeName AS pnServLoc ON pnServLoc.PracticeType = ''P''
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
		WHERE (re.ResourceType in (''D'', ''Q'', ''Z'',''Y'')
			OR (re.ResourceType = ''R''
				AND re.ServiceCode = ''02''
				AND re.Billable = ''Y''
				AND pic.FieldValue = ''T''
				AND pnBillOrg.LocationReference <> '''')
				)
		GROUP BY
			CASE 
				WHEN re.ResourceType = ''R'' ---internal facility
					THEN (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
				WHEN pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
					THEN (CONVERT(bigint, 110000000) * (110000000 * 2 + pnServLoc.PracticeId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId))
				ELSE (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
				END, 
			CASE
				WHEN pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
					THEN (110000000 * 1 + re.ResourceId) 
				ELSE pnBillOrg.PracticeId
				END,
			CASE re.Billable
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END ,
			CASE re.GoDirect
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END ,
			CASE 
				WHEN re.ResourceType = ''R''
					THEN pnBillOrg.PracticeId
				ELSE pnServLoc.PracticeId 
				END ,
			CASE
				WHEN re.ResourceType = ''R''
					THEN NULL
				ELSE re.ResourceId 
				END
	
		UNION ALL
	
		----Human Providers where ServiceLocation is from Resources table including override
		SELECT
			CASE 
				WHEN pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
					THEN (CONVERT(bigint, 110000000) * (110000000 * 3 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId))
				ELSE (CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
				END AS Id,
			CASE 
				WHEN  pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
					THEN (110000000 * 1 + re.ResourceId)
				ELSE pnBillOrg.PracticeId 
				END AS BillingOrganizationId,
			CASE re.Billable
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsBillable,
			CASE re.GoDirect
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsEMR,
			(110000000 * 1 + reServLoc.ResourceId) AS ServiceLocationId,
			re.ResourceId AS UserId
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName AS pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
			AND pnBillOrg.PracticeType = ''P''
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R''
			AND reServLoc.ServiceCode = ''02''
		LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
				AND pa.ResourceType = ''R''
		WHERE re.ResourceType in (''D'', ''Q'', ''Z'',''Y'')
		GROUP BY
				CASE 
				WHEN pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
					THEN (CONVERT(bigint, 110000000) * (110000000 * 3 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId))
				ELSE (CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
				END,
			CASE 
				WHEN  pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
					THEN (110000000 * 1 + re.ResourceId)
				ELSE pnBillOrg.PracticeId 
				END,
			CASE re.GoDirect
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END ,
			(110000000 * 1 + reServLoc.ResourceId),
			re.ResourceId,
			re.Billable,
			pa.ResourceId,
			reServLoc.ResourceId,
			pnBillOrg.PracticeId'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Providers'
			,@ProvidersViewSql
			,@isChanged = @isProvidersChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnProviderDelete'
				,
				'
			CREATE TRIGGER model.OnProviderDelete ON [model].[Providers] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @IsBillable bit
			DECLARE @IsEmr bit
			DECLARE @ServiceLocationId int
			DECLARE @BillingOrganizationId int
			DECLARE @UserId int
		
			DECLARE ProvidersDeletedCursor CURSOR FOR
			SELECT Id, IsBillable, IsEmr, ServiceLocationId, BillingOrganizationId, UserId FROM deleted
		
			OPEN ProvidersDeletedCursor
			FETCH NEXT FROM ProvidersDeletedCursor INTO @Id, @IsBillable, @IsEmr, @ServiceLocationId, @BillingOrganizationId, @UserId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for Provider --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @IsBillable AS IsBillable, @IsEmr AS IsEmr, @ServiceLocationId AS ServiceLocationId, @BillingOrganizationId AS BillingOrganizationId, @UserId AS UserId
			
				FETCH NEXT FROM ProvidersDeletedCursor INTO @Id, @IsBillable, @IsEmr, @ServiceLocationId, @BillingOrganizationId, @UserId 
		
			END
		
			CLOSE ProvidersDeletedCursor
			DEALLOCATE ProvidersDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnProviderInsert'
				,
				'
			CREATE TRIGGER model.OnProviderInsert ON [model].[Providers] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @IsBillable bit
			DECLARE @IsEmr bit
			DECLARE @ServiceLocationId int
			DECLARE @BillingOrganizationId int
			DECLARE @UserId int
		
			DECLARE ProvidersInsertedCursor CURSOR FOR
			SELECT Id, IsBillable, IsEmr, ServiceLocationId, BillingOrganizationId, UserId FROM inserted
		
			OPEN ProvidersInsertedCursor
			FETCH NEXT FROM ProvidersInsertedCursor INTO @Id, @IsBillable, @IsEmr, @ServiceLocationId, @BillingOrganizationId, @UserId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for Provider --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(bigint, NULL)
			
				SELECT @Id AS Id, @IsBillable AS IsBillable, @IsEmr AS IsEmr, @ServiceLocationId AS ServiceLocationId, @BillingOrganizationId AS BillingOrganizationId, @UserId AS UserId
				
				FETCH NEXT FROM ProvidersInsertedCursor INTO @Id, @IsBillable, @IsEmr, @ServiceLocationId, @BillingOrganizationId, @UserId 
		
			END
		
			CLOSE ProvidersInsertedCursor
			DEALLOCATE ProvidersInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnProviderUpdate'
				,'
			CREATE TRIGGER model.OnProviderUpdate ON [model].[Providers] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @IsBillable bit
			DECLARE @IsEmr bit
			DECLARE @ServiceLocationId int
			DECLARE @BillingOrganizationId int
			DECLARE @UserId int
		
			DECLARE ProvidersUpdatedCursor CURSOR FOR
			SELECT Id, IsBillable, IsEmr, ServiceLocationId, BillingOrganizationId, UserId FROM inserted
		
			OPEN ProvidersUpdatedCursor
			FETCH NEXT FROM ProvidersUpdatedCursor INTO @Id, @IsBillable, @IsEmr, @ServiceLocationId, @BillingOrganizationId, @UserId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for Provider --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ProvidersUpdatedCursor INTO @Id, @IsBillable, @IsEmr, @ServiceLocationId, @BillingOrganizationId, @UserId 
		
			END
		
			CLOSE ProvidersUpdatedCursor
			DEALLOCATE ProvidersUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Roles')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isRolesChanged BIT

		SET @isRolesChanged = 0

		DECLARE @RolesViewSql NVARCHAR(max)

		SET @RolesViewSql = '
		CREATE VIEW [model].[Roles_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id AS Id,
			Code AS [Name]
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''RESOURCETYPE'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Roles_Internal'
			,@RolesViewSql
			,@isChanged = @isRolesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @RolesViewSql = '	
			CREATE VIEW [model].[Roles]
			WITH SCHEMABINDING
			AS
			SELECT Id, Name FROM [model].[Roles_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_Roles'
							AND object_id = OBJECT_ID('[model].[Roles_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[Roles_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_Roles] ON [model].[Roles_Internal] ([Id]);
				END

				SET @RolesViewSql = @RolesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Roles'
				,@RolesViewSql
				,@isChanged = @isRolesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnRoleDelete'
				,'
			CREATE TRIGGER model.OnRoleDelete ON [model].[Roles] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
		
			DECLARE RolesDeletedCursor CURSOR FOR
			SELECT Id, Name FROM deleted
		
			OPEN RolesDeletedCursor
			FETCH NEXT FROM RolesDeletedCursor INTO @Id, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for Role --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Name AS Name
			
				FETCH NEXT FROM RolesDeletedCursor INTO @Id, @Name 
		
			END
		
			CLOSE RolesDeletedCursor
			DEALLOCATE RolesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnRoleInsert'
				,'
			CREATE TRIGGER model.OnRoleInsert ON [model].[Roles] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
		
			DECLARE RolesInsertedCursor CURSOR FOR
			SELECT Id, Name FROM inserted
		
			OPEN RolesInsertedCursor
			FETCH NEXT FROM RolesInsertedCursor INTO @Id, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for Role --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Name AS Name
				
				FETCH NEXT FROM RolesInsertedCursor INTO @Id, @Name 
		
			END
		
			CLOSE RolesInsertedCursor
			DEALLOCATE RolesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnRoleUpdate'
				,'
			CREATE TRIGGER model.OnRoleUpdate ON [model].[Roles] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
		
			DECLARE RolesUpdatedCursor CURSOR FOR
			SELECT Id, Name FROM inserted
		
			OPEN RolesUpdatedCursor
			FETCH NEXT FROM RolesUpdatedCursor INTO @Id, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for Role --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM RolesUpdatedCursor INTO @Id, @Name 
		
			END
		
			CLOSE RolesUpdatedCursor
			DEALLOCATE RolesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Rooms')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isRoomsChanged BIT

		SET @isRoomsChanged = 0

		DECLARE @RoomsViewSql NVARCHAR(max)

		SET @RoomsViewSql = '
		CREATE VIEW [model].[Rooms_Internal]
		WITH SCHEMABINDING
		AS
		SELECT re.ResourceId AS Id
			,ResourceName AS Abbreviation
			,CONVERT(bit, 0) AS IsSchedulable
			,ResourceDescription AS NAME
			,pn.PracticeId AS ServiceLocationId,
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeName pn ON pn.PracticeId = re.PracticeId
		WHERE re.ResourceType = ''R''
			AND re.ServiceCode <> ''02'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Rooms_Internal'
			,@RoomsViewSql
			,@isChanged = @isRoomsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @RoomsViewSql = '	
			CREATE VIEW [model].[Rooms]
			WITH SCHEMABINDING
			AS
			SELECT Abbreviation, Name, IsSchedulable, ServiceLocationId, Id, IsArchived FROM [model].[Rooms_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_Rooms'
							AND object_id = OBJECT_ID('[model].[Rooms_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[Rooms_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_Rooms] ON [model].[Rooms_Internal] ([Id]);
				END

				SET @RoomsViewSql = @RoomsViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Rooms'
				,@RoomsViewSql
				,@isChanged = @isRoomsChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnRoomDelete'
				,
				'
			CREATE TRIGGER model.OnRoomDelete ON [model].[Rooms] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Abbreviation nvarchar(16)
			DECLARE @Name nvarchar(max)
			DECLARE @IsSchedulable bit
			DECLARE @ServiceLocationId int
			DECLARE @Id int
			DECLARE @IsArchived bit
		
			DECLARE RoomsDeletedCursor CURSOR FOR
			SELECT Abbreviation, Name, IsSchedulable, ServiceLocationId, Id, IsArchived FROM deleted
		
			OPEN RoomsDeletedCursor
			FETCH NEXT FROM RoomsDeletedCursor INTO @Abbreviation, @Name, @IsSchedulable, @ServiceLocationId, @Id, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for Room --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Abbreviation AS Abbreviation, @Name AS Name, @IsSchedulable AS IsSchedulable, @ServiceLocationId AS ServiceLocationId, @IsArchived AS IsArchived
			
				FETCH NEXT FROM RoomsDeletedCursor INTO @Abbreviation, @Name, @IsSchedulable, @ServiceLocationId, @Id, @IsArchived 
		
			END
		
			CLOSE RoomsDeletedCursor
			DEALLOCATE RoomsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnRoomInsert'
				,
				'
			CREATE TRIGGER model.OnRoomInsert ON [model].[Rooms] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Abbreviation nvarchar(16)
			DECLARE @Name nvarchar(max)
			DECLARE @IsSchedulable bit
			DECLARE @ServiceLocationId int
			DECLARE @Id int
			DECLARE @IsArchived bit
		
			DECLARE RoomsInsertedCursor CURSOR FOR
			SELECT Abbreviation, Name, IsSchedulable, ServiceLocationId, Id, IsArchived FROM inserted
		
			OPEN RoomsInsertedCursor
			FETCH NEXT FROM RoomsInsertedCursor INTO @Abbreviation, @Name, @IsSchedulable, @ServiceLocationId, @Id, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for Room --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Abbreviation AS Abbreviation, @Name AS Name, @IsSchedulable AS IsSchedulable, @ServiceLocationId AS ServiceLocationId, @IsArchived AS IsArchived
				
				FETCH NEXT FROM RoomsInsertedCursor INTO @Abbreviation, @Name, @IsSchedulable, @ServiceLocationId, @Id, @IsArchived 
		
			END
		
			CLOSE RoomsInsertedCursor
			DEALLOCATE RoomsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnRoomUpdate'
				,'
			CREATE TRIGGER model.OnRoomUpdate ON [model].[Rooms] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Abbreviation nvarchar(16)
			DECLARE @Name nvarchar(max)
			DECLARE @IsSchedulable bit
			DECLARE @ServiceLocationId int
			DECLARE @Id int
			DECLARE @IsArchived bit
		
			DECLARE RoomsUpdatedCursor CURSOR FOR
			SELECT Abbreviation, Name, IsSchedulable, ServiceLocationId, Id, IsArchived FROM inserted
		
			OPEN RoomsUpdatedCursor
			FETCH NEXT FROM RoomsUpdatedCursor INTO @Abbreviation, @Name, @IsSchedulable, @ServiceLocationId, @Id, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for Room --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM RoomsUpdatedCursor INTO @Abbreviation, @Name, @IsSchedulable, @ServiceLocationId, @Id, @IsArchived 
		
			END
		
			CLOSE RoomsUpdatedCursor
			DEALLOCATE RoomsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ScheduleEntries')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isScheduleEntriesChanged BIT

		SET @isScheduleEntriesChanged = 0

		DECLARE @ScheduleEntriesViewSql NVARCHAR(max)

		SET @ScheduleEntriesViewSql = 
			'
		CREATE VIEW [model].[ScheduleEntries]
		WITH SCHEMABINDING
		AS
		SELECT Id AS Id
			,EndDateTime AS EndDateTime
			,NULL AS EquipmentId
			,NULL as RoomId
			,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
			,StartDateTime AS StartDateTime
			,ServiceLocationId AS ServiceLocationId
			,UserId AS UserId
			,''UserScheduleEntry'' AS __EntityType__ 
		FROM model.GetScheduleEntries(0)
	
		UNION ALL
	
		SELECT Id AS Id
			,EndDateTime AS EndDateTime
			,NULL AS EquipmentId
			,NULL as RoomId
			,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
			,StartDateTime AS StartDateTime
			,ServiceLocationId AS ServiceLocationId
			,UserId AS UserId
			,''UserScheduleEntry'' AS __EntityType__ 
		FROM model.GetScheduleEntries(1)
	
		UNION ALL
	
		SELECT Id AS Id
			,EndDateTime AS EndDateTime
			,NULL AS EquipmentId
			,NULL as RoomId
			,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
			,StartDateTime AS StartDateTime
			,ServiceLocationId AS ServiceLocationId
			,UserId AS UserId
			,''UserScheduleEntry'' AS __EntityType__ 
		FROM model.GetScheduleEntries(2)
	
		UNION ALL
	
		SELECT Id AS Id
			,EndDateTime AS EndDateTime
			,NULL AS EquipmentId
			,NULL as RoomId
			,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
			,StartDateTime AS StartDateTime
			,ServiceLocationId AS ServiceLocationId
			,UserId AS UserId
			,''UserScheduleEntry'' AS __EntityType__ 
		FROM model.GetScheduleEntries(3)
	
		UNION ALL
	
		SELECT Id AS Id
			,EndDateTime AS EndDateTime
			,NULL AS EquipmentId
			,NULL as RoomId
			,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
			,StartDateTime AS StartDateTime
			,ServiceLocationId AS ServiceLocationId
			,UserId AS UserId
			,''UserScheduleEntry'' AS __EntityType__ 
		FROM model.GetScheduleEntries(4)
	
		UNION ALL
	
		SELECT Id AS Id
			,EndDateTime AS EndDateTime
			,NULL AS EquipmentId
			,NULL as RoomId
			,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
			,StartDateTime AS StartDateTime
			,ServiceLocationId AS ServiceLocationId
			,UserId AS UserId
			,''UserScheduleEntry'' AS __EntityType__ 
		FROM model.GetScheduleEntries(5)
	
		UNION ALL
	
		SELECT Id AS Id
			,EndDateTime AS EndDateTime
			,NULL AS EquipmentId
			,NULL as RoomId
			,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
			,StartDateTime AS StartDateTime
			,ServiceLocationId AS ServiceLocationId
			,UserId AS UserId
			,''UserScheduleEntry'' AS __EntityType__ 
		FROM model.GetScheduleEntries(6)
	
		UNION ALL
	
		SELECT (110000000 * 7 + NoteId) AS Id
			,CASE 
				WHEN COALESCE(NoteDate, '''') <> ''''
					THEN CONVERT(datetime, NoteDate, 112) 
				ELSE NULL END AS EndDateTime
			,NULL AS EquipmentId
			,NULL as RoomId
			,1 AS ScheduleEntryAvailabilityTypeId
			,CASE 
				WHEN COALESCE(NoteDate, '''') <> ''''
					THEN CONVERT(datetime, NoteDate, 112) 
				ELSE NULL END AS StartDateTime
			,NULL AS ServiceLocationId
			,NULL AS UserId
			,''ScheduleEntry'' AS __EntityType__ 
		FROM dbo.PatientNotes
		WHERE PatientId = -1 and AppointmentId = -1 and NoteType = ''/'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ScheduleEntries'
			,@ScheduleEntriesViewSql
			,@isChanged = @isScheduleEntriesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnScheduleEntryDelete'
				,
				'
			CREATE TRIGGER model.OnScheduleEntryDelete ON [model].[ScheduleEntries] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @ScheduleEntryAvailabilityTypeId int
			DECLARE @UserId int
			DECLARE @ServiceLocationId int
			DECLARE @RoomId int
			DECLARE @EquipmentId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE ScheduleEntriesDeletedCursor CURSOR FOR
			SELECT Id, StartDateTime, EndDateTime, ScheduleEntryAvailabilityTypeId, UserId, ServiceLocationId, RoomId, EquipmentId, __EntityType__ FROM deleted
		
			OPEN ScheduleEntriesDeletedCursor
			FETCH NEXT FROM ScheduleEntriesDeletedCursor INTO @Id, @StartDateTime, @EndDateTime, @ScheduleEntryAvailabilityTypeId, @UserId, @ServiceLocationId, @RoomId, @EquipmentId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ScheduleEntry --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @StartDateTime AS StartDateTime, @EndDateTime AS EndDateTime, @ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId, @UserId AS UserId, @ServiceLocationId AS ServiceLocationId, @RoomId AS RoomId, @EquipmentId AS EquipmentId, @__EntityType__ AS __EntityType__
			
				FETCH NEXT FROM ScheduleEntriesDeletedCursor INTO @Id, @StartDateTime, @EndDateTime, @ScheduleEntryAvailabilityTypeId, @UserId, @ServiceLocationId, @RoomId, @EquipmentId, @__EntityType__ 
		
			END
		
			CLOSE ScheduleEntriesDeletedCursor
			DEALLOCATE ScheduleEntriesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnScheduleEntryInsert'
				,
				'
			CREATE TRIGGER model.OnScheduleEntryInsert ON [model].[ScheduleEntries] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @ScheduleEntryAvailabilityTypeId int
			DECLARE @UserId int
			DECLARE @ServiceLocationId int
			DECLARE @RoomId int
			DECLARE @EquipmentId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE ScheduleEntriesInsertedCursor CURSOR FOR
			SELECT Id, StartDateTime, EndDateTime, ScheduleEntryAvailabilityTypeId, UserId, ServiceLocationId, RoomId, EquipmentId, __EntityType__ FROM inserted
		
			OPEN ScheduleEntriesInsertedCursor
			FETCH NEXT FROM ScheduleEntriesInsertedCursor INTO @Id, @StartDateTime, @EndDateTime, @ScheduleEntryAvailabilityTypeId, @UserId, @ServiceLocationId, @RoomId, @EquipmentId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ScheduleEntry --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @StartDateTime AS StartDateTime, @EndDateTime AS EndDateTime, @ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId, @UserId AS UserId, @ServiceLocationId AS ServiceLocationId, @RoomId AS RoomId, @EquipmentId AS EquipmentId, @__EntityType__ AS __EntityType__
				
				FETCH NEXT FROM ScheduleEntriesInsertedCursor INTO @Id, @StartDateTime, @EndDateTime, @ScheduleEntryAvailabilityTypeId, @UserId, @ServiceLocationId, @RoomId, @EquipmentId, @__EntityType__ 
		
			END
		
			CLOSE ScheduleEntriesInsertedCursor
			DEALLOCATE ScheduleEntriesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnScheduleEntryUpdate'
				,
				'
			CREATE TRIGGER model.OnScheduleEntryUpdate ON [model].[ScheduleEntries] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @StartDateTime datetime
			DECLARE @EndDateTime datetime
			DECLARE @ScheduleEntryAvailabilityTypeId int
			DECLARE @UserId int
			DECLARE @ServiceLocationId int
			DECLARE @RoomId int
			DECLARE @EquipmentId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE ScheduleEntriesUpdatedCursor CURSOR FOR
			SELECT Id, StartDateTime, EndDateTime, ScheduleEntryAvailabilityTypeId, UserId, ServiceLocationId, RoomId, EquipmentId, __EntityType__ FROM inserted
		
			OPEN ScheduleEntriesUpdatedCursor
			FETCH NEXT FROM ScheduleEntriesUpdatedCursor INTO @Id, @StartDateTime, @EndDateTime, @ScheduleEntryAvailabilityTypeId, @UserId, @ServiceLocationId, @RoomId, @EquipmentId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ScheduleEntry --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ScheduleEntriesUpdatedCursor INTO @Id, @StartDateTime, @EndDateTime, @ScheduleEntryAvailabilityTypeId, @UserId, @ServiceLocationId, @RoomId, @EquipmentId, @__EntityType__ 
		
			END
		
			CLOSE ScheduleEntriesUpdatedCursor
			DEALLOCATE ScheduleEntriesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ServiceLocationCodes')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isServiceLocationCodesChanged BIT

		SET @isServiceLocationCodesChanged = 0

		DECLARE @ServiceLocationCodesViewSql NVARCHAR(max)

		SET @ServiceLocationCodesViewSql = '
		CREATE VIEW [model].[ServiceLocationCodes_Internal]
		WITH SCHEMABINDING
		AS
		SELECT Id,
			SUBSTRING(Code, 4, LEN(code)) AS [Name],
			SUBSTRING(Code, 1, 2) AS PlaceOfServiceCode
		FROM dbo.PracticeCodeTable
		WHERE ReferenceType = ''PLACEOFSERVICE'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ServiceLocationCodes_Internal'
			,@ServiceLocationCodesViewSql
			,@isChanged = @isServiceLocationCodesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @ServiceLocationCodesViewSql = '	
			CREATE VIEW [model].[ServiceLocationCodes]
			WITH SCHEMABINDING
			AS
			SELECT Id, PlaceOfServiceCode, Name FROM [model].[ServiceLocationCodes_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_ServiceLocationCodes'
							AND object_id = OBJECT_ID('[model].[ServiceLocationCodes_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[ServiceLocationCodes_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_ServiceLocationCodes] ON [model].[ServiceLocationCodes_Internal] ([Id]);
				END

				SET @ServiceLocationCodesViewSql = @ServiceLocationCodesViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ServiceLocationCodes'
				,@ServiceLocationCodesViewSql
				,@isChanged = @isServiceLocationCodesChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnServiceLocationCodeDelete'
				,'
			CREATE TRIGGER model.OnServiceLocationCodeDelete ON [model].[ServiceLocationCodes] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PlaceOfServiceCode nvarchar(max)
			DECLARE @Name nvarchar(max)
		
			DECLARE ServiceLocationCodesDeletedCursor CURSOR FOR
			SELECT Id, PlaceOfServiceCode, Name FROM deleted
		
			OPEN ServiceLocationCodesDeletedCursor
			FETCH NEXT FROM ServiceLocationCodesDeletedCursor INTO @Id, @PlaceOfServiceCode, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ServiceLocationCode --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @PlaceOfServiceCode AS PlaceOfServiceCode, @Name AS Name
			
				FETCH NEXT FROM ServiceLocationCodesDeletedCursor INTO @Id, @PlaceOfServiceCode, @Name 
		
			END
		
			CLOSE ServiceLocationCodesDeletedCursor
			DEALLOCATE ServiceLocationCodesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnServiceLocationCodeInsert'
				,'
			CREATE TRIGGER model.OnServiceLocationCodeInsert ON [model].[ServiceLocationCodes] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PlaceOfServiceCode nvarchar(max)
			DECLARE @Name nvarchar(max)
		
			DECLARE ServiceLocationCodesInsertedCursor CURSOR FOR
			SELECT Id, PlaceOfServiceCode, Name FROM inserted
		
			OPEN ServiceLocationCodesInsertedCursor
			FETCH NEXT FROM ServiceLocationCodesInsertedCursor INTO @Id, @PlaceOfServiceCode, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ServiceLocationCode --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @PlaceOfServiceCode AS PlaceOfServiceCode, @Name AS Name
				
				FETCH NEXT FROM ServiceLocationCodesInsertedCursor INTO @Id, @PlaceOfServiceCode, @Name 
		
			END
		
			CLOSE ServiceLocationCodesInsertedCursor
			DEALLOCATE ServiceLocationCodesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnServiceLocationCodeUpdate'
				,'
			CREATE TRIGGER model.OnServiceLocationCodeUpdate ON [model].[ServiceLocationCodes] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @PlaceOfServiceCode nvarchar(max)
			DECLARE @Name nvarchar(max)
		
			DECLARE ServiceLocationCodesUpdatedCursor CURSOR FOR
			SELECT Id, PlaceOfServiceCode, Name FROM inserted
		
			OPEN ServiceLocationCodesUpdatedCursor
			FETCH NEXT FROM ServiceLocationCodesUpdatedCursor INTO @Id, @PlaceOfServiceCode, @Name 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ServiceLocationCode --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ServiceLocationCodesUpdatedCursor INTO @Id, @PlaceOfServiceCode, @Name 
		
			END
		
			CLOSE ServiceLocationCodesUpdatedCursor
			DEALLOCATE ServiceLocationCodesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ServiceLocations')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isServiceLocationsChanged BIT

		SET @isServiceLocationsChanged = 0

		DECLARE @ismodelServiceLocations_Partition1Changed BIT

		SET @ismodelServiceLocations_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ServiceLocations_Partition1'
			,'
		CREATE VIEW model.ServiceLocations_Partition1
		WITH SCHEMABINDING
		AS
		SELECT Id AS Id
							FROM dbo.PracticeCodeTable
							WHERE SUBSTRING(Code, 1, 2) = ''11''
								AND ReferenceType = ''PLACEOFSERVICE''
		'
			,@isChanged = @ismodelServiceLocations_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_ServiceLocations_Partition1'
					AND object_id = OBJECT_ID('model.ServiceLocations_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.ServiceLocations_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_ServiceLocations_Partition1 ON model.ServiceLocations_Partition1 ([Id]);
		END

		DECLARE @ServiceLocationsViewSql NVARCHAR(max)

		SET @ServiceLocationsViewSql = 
			'
		CREATE VIEW [model].[ServiceLocations]
		WITH SCHEMABINDING
		AS
		----First number of GetPairId for ServiceLocationId indicates where it comes from (0 - PracticeName, 1 - Resources)
		----If a surgery center is a provider, I''m using the PracticeName row for ServiceLocation because encounters are scheduled in the PracticeName ServiceLocation.
		SELECT v.PracticeId AS Id
			,CONVERT(BIT, 0) AS IsExternal
			,v.NAME AS [Name]
			,v.ServiceLocationCodeId AS ServiceLocationCodeId
			,v.ShortName AS ShortName,
			CONVERT(bit, 0) AS IsArchived
		FROM (
			SELECT pn.PracticeId AS PracticeId
				,pic.FieldValue AS FieldValue
				,re.Billable AS Billable
				,re.ServiceCode AS ServiceCode
				,CASE 
					WHEN pic.FieldValue = ''T''
						AND re.Billable = ''Y''
						AND re.ServiceCode = ''02''
						AND pn.LocationReference <> ''''
						THEN pctRe.Id
					ELSE pct.Id
					END AS ServiceLocationCodeId
				,CASE LocationReference
					WHEN ''''
						THEN ''Office''
					ELSE ''Office-'' + LocationReference
					END AS ShortName
				,PracticeName AS NAME
				,COUNT_BIG(*) AS Count
			FROM dbo.PracticeName pn
			INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
			LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''PLACEOFSERVICE''
				AND SUBSTRING(pct.Code, 1, 2) = ''11''
			LEFT JOIN dbo.Resources re ON re.PracticeId = pn.PracticeId
				AND re.ResourceType = ''R''
				AND re.ServiceCode = ''02''
				AND re.Billable = ''Y''
				AND pic.FieldValue = ''T''
				AND pn.LocationReference <> ''''
			LEFT JOIN dbo.PracticeCodeTable pctRE ON pctRE.ReferenceType = ''PLACEOFSERVICE''
				AND SUBSTRING(pctRE.Code, 1, 2) = re.PlaceOfService
			WHERE pn.PracticeType = ''P''
			GROUP BY pn.PracticeId
				,pct.Id
				,LocationReference
				,pic.FieldValue
				,re.Billable
				,re.ServiceCode
				,pctRe.Id
				,PracticeName
			) AS v
	
		UNION ALL
	
		----Getting ServiceLocations from Resources table (ResourceType R).  THESE ARE ALL FACILITIES.
		SELECT (110000000 * 1 + re.ResourceId) AS Id
			,CONVERT(BIT, 1) AS IsExternal
			,re.ResourceDescription AS [Name]
			,CASE 
				WHEN pct.id IS NULL
					OR pct.Id = ''''
					THEN (
	
		SELECT model.ServiceLocations_Partition1.Id AS Id FROM model.ServiceLocations_Partition1 WITH(NOEXPAND)
	
		)
				ELSE pct.id
				END AS ServiceLocationCodeId
			,re.ResourceName AS ShortName,
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.Resources re
		LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''PLACEOFSERVICE''
			AND SUBSTRING(pct.Code, 1, 2) = re.PlaceOfService
		WHERE re.ResourceType = ''R''
			AND re.ServiceCode = ''02'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ServiceLocations'
			,@ServiceLocationsViewSql
			,@isChanged = @isServiceLocationsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnServiceLocationDelete'
				,
				'
			CREATE TRIGGER model.OnServiceLocationDelete ON [model].[ServiceLocations] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsExternal bit
			DECLARE @ServiceLocationCodeId int
			DECLARE @ShortName nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE ServiceLocationsDeletedCursor CURSOR FOR
			SELECT Id, Name, IsExternal, ServiceLocationCodeId, ShortName, IsArchived FROM deleted
		
			OPEN ServiceLocationsDeletedCursor
			FETCH NEXT FROM ServiceLocationsDeletedCursor INTO @Id, @Name, @IsExternal, @ServiceLocationCodeId, @ShortName, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- ServiceLocation Delete
				DELETE
				FROM dbo.PracticeName
				WHERE PracticeId = @Id
			
				SELECT @Id AS Id, @Name AS Name, @IsExternal AS IsExternal, @ServiceLocationCodeId AS ServiceLocationCodeId, @ShortName AS ShortName, @IsArchived AS IsArchived
			
				FETCH NEXT FROM ServiceLocationsDeletedCursor INTO @Id, @Name, @IsExternal, @ServiceLocationCodeId, @ShortName, @IsArchived 
		
			END
		
			CLOSE ServiceLocationsDeletedCursor
			DEALLOCATE ServiceLocationsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnServiceLocationInsert'
				,
				'
			CREATE TRIGGER model.OnServiceLocationInsert ON [model].[ServiceLocations] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsExternal bit
			DECLARE @ServiceLocationCodeId int
			DECLARE @ShortName nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE ServiceLocationsInsertedCursor CURSOR FOR
			SELECT Id, Name, IsExternal, ServiceLocationCodeId, ShortName, IsArchived FROM inserted
		
			OPEN ServiceLocationsInsertedCursor
			FETCH NEXT FROM ServiceLocationsInsertedCursor INTO @Id, @Name, @IsExternal, @ServiceLocationCodeId, @ShortName, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- ServiceLocation Insert
				INSERT INTO dbo.PracticeName (PracticeName)
				VALUES ('''')
			
				SET @Id = IDENT_CURRENT(''PracticeName'')
				SET @Id = model.GetPairId(0, @Id, 110000000)
			
			
							
				-- ServiceLocation Update/Insert
				UPDATE dbo.PracticeName
				SET PracticeName = @Name
					,LocationReference = CASE @ShortName
						WHEN ''Office''
							THEN ''''
						ELSE RTRIM(LTRIM(REPLACE(@shortName, ''office-'', '''')))
						END
					,PracticeType = ''P''
					,PracticeNPI = ''2''
				WHERE PracticeId = @Id
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @Name AS Name, @IsExternal AS IsExternal, @ServiceLocationCodeId AS ServiceLocationCodeId, @ShortName AS ShortName, @IsArchived AS IsArchived
				
				FETCH NEXT FROM ServiceLocationsInsertedCursor INTO @Id, @Name, @IsExternal, @ServiceLocationCodeId, @ShortName, @IsArchived 
		
			END
		
			CLOSE ServiceLocationsInsertedCursor
			DEALLOCATE ServiceLocationsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnServiceLocationUpdate'
				,
				'
			CREATE TRIGGER model.OnServiceLocationUpdate ON [model].[ServiceLocations] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(max)
			DECLARE @IsExternal bit
			DECLARE @ServiceLocationCodeId int
			DECLARE @ShortName nvarchar(max)
			DECLARE @IsArchived bit
		
			DECLARE ServiceLocationsUpdatedCursor CURSOR FOR
			SELECT Id, Name, IsExternal, ServiceLocationCodeId, ShortName, IsArchived FROM inserted
		
			OPEN ServiceLocationsUpdatedCursor
			FETCH NEXT FROM ServiceLocationsUpdatedCursor INTO @Id, @Name, @IsExternal, @ServiceLocationCodeId, @ShortName, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- ServiceLocation Update/Insert
				UPDATE dbo.PracticeName
				SET PracticeName = @Name
					,LocationReference = CASE @ShortName
						WHEN ''Office''
							THEN ''''
						ELSE RTRIM(LTRIM(REPLACE(@shortName, ''office-'', '''')))
						END
					,PracticeType = ''P''
					,PracticeNPI = ''2''
				WHERE PracticeId = @Id
				
				FETCH NEXT FROM ServiceLocationsUpdatedCursor INTO @Id, @Name, @IsExternal, @ServiceLocationCodeId, @ShortName, @IsArchived 
		
			END
		
			CLOSE ServiceLocationsUpdatedCursor
			DEALLOCATE ServiceLocationsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.ServiceModifiers')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isServiceModifiersChanged BIT

		SET @isServiceModifiersChanged = 0

		DECLARE @ServiceModifiersViewSql NVARCHAR(max)

		SET @ServiceModifiersViewSql = 
			'
		CREATE VIEW [model].[ServiceModifiers]
		WITH SCHEMABINDING
		AS
		--Master list from PracticeCodeTable 
		SELECT CASE v.QueryOrdinal
			WHEN 0
				THEN (110000000 * 1 + v.Id)
			WHEN 1
				THEN (110000000 * 2 + v.Id)
			WHEN 2
				THEN (110000000 * 3 + v.Id)
			WHEN 3
				THEN (110000000 * 4 + v.Id)
			WHEN 4 
				THEN (110000000 * 5 + v.Id)
			END AS Id,
			v.BillingOrdinalId,
			CASE v.QueryOrdinal
				WHEN 0
					THEN SUBSTRING(v.Code, 1, 2)
				WHEN 1
					THEN SUBSTRING(v.Modifier, 1, 2)
				WHEN 2
					THEN SUBSTRING(v.Modifier, 3, 2)
				WHEN 3
					THEN SUBSTRING(v.Modifier, 5, 2)
				WHEN 4
					THEN SUBSTRING(v.Modifier, 7, 2)
				END AS Code,
			CASE v.QueryOrdinal
				WHEN 0
					THEN SUBSTRING(Code, 6, LEN(code))
				ELSE CONVERT(nvarchar, NULL) 
				END AS Description,
			CASE v.QueryOrdinal
				WHEN 0
					THEN CASE
						WHEN ISNUMERIC(AlternateCode + ''.0e0'')=1
							THEN CONVERT(int, v.AlternateCode) 
						ELSE 99
						END
				WHEN 1
					THEN 99
				END AS ClinicalOrdinalId,
			CONVERT(bit, 0) AS IsArchived
		FROM (SELECT Id AS Id,
				CASE 
					WHEN [Rank] IS NULL
						THEN CONVERT(int, 0)
					ELSE [Rank]
					END AS BillingOrdinalId,
				CONVERT(nvarchar, NULL) AS Modifier,
				Code AS Code,
				AlternateCode AS AlternateCode,
				0 AS QueryOrdinal,
				COUNT_BIG(*) AS Count
			FROM dbo.PracticeCodeTable
			WHERE ReferenceType = ''SERVICEMODS''
			GROUP BY [Rank],
				Id,
				Code,
				AlternateCode
	
			UNION ALL
			--Also getting any modifier ever used in PatientReceivableServices
			----First 2 bytes of prs.Modifier (prs.Modifier has up to 4 modifers concatenated)
			SELECT ItemId AS Id,
				99 AS BillingOrdinalId,
				Modifier AS Modifier,
				CONVERT(nvarchar, NULL) AS Code,
				CONVERT(nvarchar, NULL) AS AlternateCode,
				1 AS QueryOrdinal,
				COUNT_BIG(*) AS Count
			FROM dbo.PatientReceivableServices
			WHERE SUBSTRING(modifier, 1, 2) <> ''''
				AND SUBSTRING(modifier, 1, 2) NOT IN 
					(
					SELECT SUBSTRING(Code, 1, 2) AS sm
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''SERVICEMODS''
					)
			GROUP BY ItemId,
				Modifier
			
			UNION ALL
			----Second set of 2 characters of prs.Modifier
			SELECT ItemId AS Id,
				99 AS BillingOrdinalId,
				Modifier,
				CONVERT(nvarchar, NULL) AS Code,
				CONVERT(nvarchar, NULL) AS AlternateCode,
				2 AS QueryOrdinal,
				COUNT_BIG(*) AS Count
			FROM dbo.PatientReceivableServices
			WHERE SUBSTRING(Modifier, 3, 2) <> ''''
				AND SUBSTRING(Modifier, 3, 2) NOT IN (
					SELECT SUBSTRING(Code, 1, 2) AS sm
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''SERVICEMODS''
					)
			GROUP BY ItemId,
				Modifier
	
			UNION ALL
			----Third set of 2-characters of prs.Modifier
			SELECT ItemId AS Id,
				99 AS BillingOrdinalId,
				Modifier AS Modifier,
				CONVERT(nvarchar, NULL) AS Code,
				CONVERT(nvarchar, NULL) AS AlternateCode,
				3 AS QueryOrdinal,
				COUNT_BIG(*) AS Count
			FROM dbo.PatientReceivableServices
			WHERE SUBSTRING(Modifier, 5, 2) <> ''''
				AND SUBSTRING(Modifier, 5, 2) NOT IN (
					SELECT SUBSTRING(Code, 1, 2) AS sm
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''SERVICEMODS''
					)
			GROUP BY ItemId,
				Modifier
	
			UNION ALL
			----Fourth set of 2-characters from prs.Modifier
			SELECT ItemId AS Id,
				99 AS BillingOrdinalId,
				Modifier AS Modifier,
				CONVERT(nvarchar, NULL) AS Code,
				CONVERT(nvarchar, NULL) AS AlternateCode,
				4 AS QueryOrdinal,
				COUNT_BIG(*) AS Count
			FROM dbo.PatientReceivableServices
			WHERE SUBSTRING(Modifier, 7, 2) <> ''''
				AND SUBSTRING(Modifier, 7, 2) NOT IN (
					SELECT SUBSTRING(Code, 1, 2) AS sm
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''SERVICEMODS''
					)
			GROUP BY ItemId,
				Modifier
				) AS v'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.ServiceModifiers'
			,@ServiceModifiersViewSql
			,@isChanged = @isServiceModifiersChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnServiceModifierDelete'
				,
				'
			CREATE TRIGGER model.OnServiceModifierDelete ON [model].[ServiceModifiers] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Code nvarchar(max)
			DECLARE @Description nvarchar(max)
			DECLARE @ClinicalOrdinalId int
			DECLARE @BillingOrdinalId int
			DECLARE @IsArchived bit
		
			DECLARE ServiceModifiersDeletedCursor CURSOR FOR
			SELECT Id, Code, Description, ClinicalOrdinalId, BillingOrdinalId, IsArchived FROM deleted
		
			OPEN ServiceModifiersDeletedCursor
			FETCH NEXT FROM ServiceModifiersDeletedCursor INTO @Id, @Code, @Description, @ClinicalOrdinalId, @BillingOrdinalId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for ServiceModifier --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Code AS Code, @Description AS Description, @ClinicalOrdinalId AS ClinicalOrdinalId, @BillingOrdinalId AS BillingOrdinalId, @IsArchived AS IsArchived
			
				FETCH NEXT FROM ServiceModifiersDeletedCursor INTO @Id, @Code, @Description, @ClinicalOrdinalId, @BillingOrdinalId, @IsArchived 
		
			END
		
			CLOSE ServiceModifiersDeletedCursor
			DEALLOCATE ServiceModifiersDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnServiceModifierInsert'
				,
				'
			CREATE TRIGGER model.OnServiceModifierInsert ON [model].[ServiceModifiers] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Code nvarchar(max)
			DECLARE @Description nvarchar(max)
			DECLARE @ClinicalOrdinalId int
			DECLARE @BillingOrdinalId int
			DECLARE @IsArchived bit
		
			DECLARE ServiceModifiersInsertedCursor CURSOR FOR
			SELECT Id, Code, Description, ClinicalOrdinalId, BillingOrdinalId, IsArchived FROM inserted
		
			OPEN ServiceModifiersInsertedCursor
			FETCH NEXT FROM ServiceModifiersInsertedCursor INTO @Id, @Code, @Description, @ClinicalOrdinalId, @BillingOrdinalId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for ServiceModifier --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @Code AS Code, @Description AS Description, @ClinicalOrdinalId AS ClinicalOrdinalId, @BillingOrdinalId AS BillingOrdinalId, @IsArchived AS IsArchived
				
				FETCH NEXT FROM ServiceModifiersInsertedCursor INTO @Id, @Code, @Description, @ClinicalOrdinalId, @BillingOrdinalId, @IsArchived 
		
			END
		
			CLOSE ServiceModifiersInsertedCursor
			DEALLOCATE ServiceModifiersInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnServiceModifierUpdate'
				,
				'
			CREATE TRIGGER model.OnServiceModifierUpdate ON [model].[ServiceModifiers] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Code nvarchar(max)
			DECLARE @Description nvarchar(max)
			DECLARE @ClinicalOrdinalId int
			DECLARE @BillingOrdinalId int
			DECLARE @IsArchived bit
		
			DECLARE ServiceModifiersUpdatedCursor CURSOR FOR
			SELECT Id, Code, Description, ClinicalOrdinalId, BillingOrdinalId, IsArchived FROM inserted
		
			OPEN ServiceModifiersUpdatedCursor
			FETCH NEXT FROM ServiceModifiersUpdatedCursor INTO @Id, @Code, @Description, @ClinicalOrdinalId, @BillingOrdinalId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for ServiceModifier --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM ServiceModifiersUpdatedCursor INTO @Id, @Code, @Description, @ClinicalOrdinalId, @BillingOrdinalId, @IsArchived 
		
			END
		
			CLOSE ServiceModifiersUpdatedCursor
			DEALLOCATE ServiceModifiersUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.UserRole')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isUserRoleChanged BIT

		SET @isUserRoleChanged = 0

		DECLARE @UserRoleViewSql NVARCHAR(max)

		SET @UserRoleViewSql = '
		CREATE VIEW [model].[UserRole_Internal]
		WITH SCHEMABINDING
		AS
		SELECT pct.Id AS Roles_Id,
			re.ResourceId AS Users_Id
		FROM dbo.Resources re
		INNER JOIN dbo.PracticeCodeTable pct ON SUBSTRING(pct.Code, 1, 1) = re.ResourceType
			AND ReferenceType = ''RESOURCETYPE''
		WHERE ResourceType <> ''R'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.UserRole_Internal'
			,@UserRoleViewSql
			,@isChanged = @isUserRoleChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @UserRoleViewSql = '	
			CREATE VIEW [model].[UserRole]
			WITH SCHEMABINDING
			AS
			SELECT Users_Id, Roles_Id FROM [model].[UserRole_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_UserRole'
							AND object_id = OBJECT_ID('[model].[UserRole_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[UserRole_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_UserRole] ON [model].[UserRole_Internal] (
						[Users_Id]
						,[Roles_Id]
						);
				END

				SET @UserRoleViewSql = @UserRoleViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.UserRole'
				,@UserRoleViewSql
				,@isChanged = @isUserRoleChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnUserRoleDelete'
				,'
			CREATE TRIGGER model.OnUserRoleDelete ON [model].[UserRole] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Users_Id int
			DECLARE @Roles_Id int
		
			DECLARE UserRoleDeletedCursor CURSOR FOR
			SELECT Users_Id, Roles_Id FROM deleted
		
			OPEN UserRoleDeletedCursor
			FETCH NEXT FROM UserRoleDeletedCursor INTO @Users_Id, @Roles_Id 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for UserRole --
				PRINT (''Not Implemented'')
			
				SELECT @Users_Id AS Users_Id, @Roles_Id AS Roles_Id
			
				FETCH NEXT FROM UserRoleDeletedCursor INTO @Users_Id, @Roles_Id 
		
			END
		
			CLOSE UserRoleDeletedCursor
			DEALLOCATE UserRoleDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnUserRoleInsert'
				,'
			CREATE TRIGGER model.OnUserRoleInsert ON [model].[UserRole] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Users_Id int
			DECLARE @Roles_Id int
		
			DECLARE UserRoleInsertedCursor CURSOR FOR
			SELECT Users_Id, Roles_Id FROM inserted
		
			OPEN UserRoleInsertedCursor
			FETCH NEXT FROM UserRoleInsertedCursor INTO @Users_Id, @Roles_Id 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- UserRole Update/Insert
				UPDATE Resources
				SET ResourceType = (
						SELECT SUBSTRING(model.roles.NAME, 1, 1)
						FROM model.roles
						WHERE @roles_Id = model.roles.id
						)
				WHERE @Users_Id = ResourceId
			
				DROP TABLE #NoCheck
			
			
				SELECT @Users_Id AS Users_Id, @Roles_Id AS Roles_Id
				
				FETCH NEXT FROM UserRoleInsertedCursor INTO @Users_Id, @Roles_Id 
		
			END
		
			CLOSE UserRoleInsertedCursor
			DEALLOCATE UserRoleInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnUserRoleUpdate'
				,'
			CREATE TRIGGER model.OnUserRoleUpdate ON [model].[UserRole] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Users_Id int
			DECLARE @Roles_Id int
		
			DECLARE UserRoleUpdatedCursor CURSOR FOR
			SELECT Users_Id, Roles_Id FROM inserted
		
			OPEN UserRoleUpdatedCursor
			FETCH NEXT FROM UserRoleUpdatedCursor INTO @Users_Id, @Roles_Id 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- UserRole Update/Insert
				UPDATE Resources
				SET ResourceType = (
						SELECT SUBSTRING(model.roles.NAME, 1, 1)
						FROM model.roles
						WHERE @roles_Id = model.roles.id
						)
				WHERE @Users_Id = ResourceId
				
				FETCH NEXT FROM UserRoleUpdatedCursor INTO @Users_Id, @Roles_Id 
		
			END
		
			CLOSE UserRoleUpdatedCursor
			DEALLOCATE UserRoleUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Users')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isUsersChanged BIT

		SET @isUsersChanged = 0

		DECLARE @UsersViewSql NVARCHAR(max)

		SET @UsersViewSql = 
			'
		CREATE VIEW [model].[Users_Internal]
		WITH SCHEMABINDING
		AS
		SELECT re.ResourceId AS Id,
			re.ResourceDescription AS DisplayName,
			re.ResourceFirstName AS FirstName,
			re.ResourceSuffix AS Honorific,
			CASE 
				WHEN re.ResourceType IN (''Z'', ''X'', ''Y'')
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsArchived,
			re.IsLoggedIn AS IsLoggedIn,
			CASE 
				WHEN ResourceType NOT IN (''Q'', ''D'') 
					THEN CONVERT(bit, 0) 
				WHEN re.ResourceType IN (''D'', ''Q'')
					THEN CONVERT(bit, 1)
				END AS IsScheduleable,
			re.ResourceLastName AS LastName,
			re.ResourceMI AS MiddleName,
			re.ResourceColor as OleColor,
			ResourcePid AS PID,
			re.ResourcePrefix AS Prefix,
			CONVERT(nvarchar, NULL) AS Suffix,
			re.ResourceName AS UserName,
			dbo.TryConvertUniqueIdentifier(ResourceId) AS UniqueId
			,CASE 
			 WHEN ResourceType NOT IN (''D'', ''Q'', ''Z'', ''Y'')
				THEN ''User'' 
			WHEN re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
				THEN ''Doctor'' 
			END AS __EntityType__
		FROM dbo.Resources re
		WHERE ResourceType <> ''R'' AND ResourceId > 0'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Users_Internal'
			,@UsersViewSql
			,@isChanged = @isUsersChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @UsersViewSql = '	
			CREATE VIEW [model].[Users]
			WITH SCHEMABINDING
			AS
			SELECT Id, UserName, LastName, FirstName, MiddleName, Suffix, Pid, IsLoggedIn, UniqueId, IsArchived, IsScheduleable, Prefix, Honorific, DisplayName, OleColor, __EntityType__ FROM [model].[Users_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_Users'
							AND object_id = OBJECT_ID('[model].[Users_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[Users_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_Users] ON [model].[Users_Internal] ([Id]);
				END

				SET @UsersViewSql = @UsersViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Users'
				,@UsersViewSql
				,@isChanged = @isUsersChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnUserDelete'
				,
				'
			CREATE TRIGGER model.OnUserDelete ON [model].[Users] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @UserName nvarchar(35)
			DECLARE @LastName nvarchar(35)
			DECLARE @FirstName nvarchar(35)
			DECLARE @MiddleName nvarchar(35)
			DECLARE @Suffix nvarchar(10)
			DECLARE @Pid nvarchar(4)
			DECLARE @IsLoggedIn bit
			DECLARE @UniqueId uniqueidentifier
			DECLARE @IsArchived bit
			DECLARE @IsScheduleable bit
			DECLARE @Prefix nvarchar(max)
			DECLARE @Honorific nvarchar(max)
			DECLARE @DisplayName nvarchar(max)
			DECLARE @OleColor int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE UsersDeletedCursor CURSOR FOR
			SELECT Id, UserName, LastName, FirstName, MiddleName, Suffix, Pid, IsLoggedIn, UniqueId, IsArchived, IsScheduleable, Prefix, Honorific, DisplayName, OleColor, __EntityType__ FROM deleted
		
			OPEN UsersDeletedCursor
			FETCH NEXT FROM UsersDeletedCursor INTO @Id, @UserName, @LastName, @FirstName, @MiddleName, @Suffix, @Pid, @IsLoggedIn, @UniqueId, @IsArchived, @IsScheduleable, @Prefix, @Honorific, @DisplayName, @OleColor, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for User --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @UserName AS UserName, @LastName AS LastName, @FirstName AS FirstName, @MiddleName AS MiddleName, @Suffix AS Suffix, @Pid AS Pid, @IsLoggedIn AS IsLoggedIn, @UniqueId AS UniqueId, @IsArchived AS IsArchived, @IsScheduleable AS IsScheduleable, @Prefix AS Prefix, @Honorific AS Honorific, @DisplayName AS DisplayName, @OleColor AS OleColor, @__EntityType__ AS __EntityType__
			
				FETCH NEXT FROM UsersDeletedCursor INTO @Id, @UserName, @LastName, @FirstName, @MiddleName, @Suffix, @Pid, @IsLoggedIn, @UniqueId, @IsArchived, @IsScheduleable, @Prefix, @Honorific, @DisplayName, @OleColor, @__EntityType__ 
		
			END
		
			CLOSE UsersDeletedCursor
			DEALLOCATE UsersDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnUserInsert'
				,
				'
			CREATE TRIGGER model.OnUserInsert ON [model].[Users] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @UserName nvarchar(35)
			DECLARE @LastName nvarchar(35)
			DECLARE @FirstName nvarchar(35)
			DECLARE @MiddleName nvarchar(35)
			DECLARE @Suffix nvarchar(10)
			DECLARE @Pid nvarchar(4)
			DECLARE @IsLoggedIn bit
			DECLARE @UniqueId uniqueidentifier
			DECLARE @IsArchived bit
			DECLARE @IsScheduleable bit
			DECLARE @Prefix nvarchar(max)
			DECLARE @Honorific nvarchar(max)
			DECLARE @DisplayName nvarchar(max)
			DECLARE @OleColor int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE UsersInsertedCursor CURSOR FOR
			SELECT Id, UserName, LastName, FirstName, MiddleName, Suffix, Pid, IsLoggedIn, UniqueId, IsArchived, IsScheduleable, Prefix, Honorific, DisplayName, OleColor, __EntityType__ FROM inserted
		
			OPEN UsersInsertedCursor
			FETCH NEXT FROM UsersInsertedCursor INTO @Id, @UserName, @LastName, @FirstName, @MiddleName, @Suffix, @Pid, @IsLoggedIn, @UniqueId, @IsArchived, @IsScheduleable, @Prefix, @Honorific, @DisplayName, @OleColor, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- User Insert
				INSERT INTO Resources (ResourceLastName)
				VALUES ('''')
			
				SET @Id = IDENT_CURRENT(''Resources'')
			
							
				-- User Update/Insert
				UPDATE Resources
				SET ResourceDescription = @DisplayName,
					ResourceFirstName = @FirstName,
					ResourceSuffix = @Honorific,
					[Status] = CASE @IsArchived
						WHEN 1
							THEN ''Z''
						WHEN 0
							THEN ''A''
						END,
					IsLoggedIn = @IsLoggedIn,
					ResourceType = (
						SELECT SUBSTRING(r.NAME, 1, 1)
						FROM model.UserRole ur
						INNER JOIN model.Roles r ON r.Id = ur.Roles_Id
						WHERE ur.Users_Id = @Id
						),
					ResourceLastName = @LastName,
					ResourceMI = @MiddleName,
					ResourceColor = @OleColor,
					ResourcePid = @PID,
					ResourcePrefix = @Prefix,
					ResourceName = @UserName,
					@UniqueId = CONVERT(UNIQUEIDENTIFIER, ''00000000-0000-0000-0000-00'' + LEFT(''0000000000'', (10 - LEN(CONVERT(VARCHAR, @Id)))) + CONVERT(VARCHAR, @Id))
				WHERE ResourceId = @Id
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @UserName AS UserName, @LastName AS LastName, @FirstName AS FirstName, @MiddleName AS MiddleName, @Suffix AS Suffix, @Pid AS Pid, @IsLoggedIn AS IsLoggedIn, @UniqueId AS UniqueId, @IsArchived AS IsArchived, @IsScheduleable AS IsScheduleable, @Prefix AS Prefix, @Honorific AS Honorific, @DisplayName AS DisplayName, @OleColor AS OleColor, @__EntityType__ AS __EntityType__
				
				FETCH NEXT FROM UsersInsertedCursor INTO @Id, @UserName, @LastName, @FirstName, @MiddleName, @Suffix, @Pid, @IsLoggedIn, @UniqueId, @IsArchived, @IsScheduleable, @Prefix, @Honorific, @DisplayName, @OleColor, @__EntityType__ 
		
			END
		
			CLOSE UsersInsertedCursor
			DEALLOCATE UsersInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnUserUpdate'
				,
				'
			CREATE TRIGGER model.OnUserUpdate ON [model].[Users] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @UserName nvarchar(35)
			DECLARE @LastName nvarchar(35)
			DECLARE @FirstName nvarchar(35)
			DECLARE @MiddleName nvarchar(35)
			DECLARE @Suffix nvarchar(10)
			DECLARE @Pid nvarchar(4)
			DECLARE @IsLoggedIn bit
			DECLARE @UniqueId uniqueidentifier
			DECLARE @IsArchived bit
			DECLARE @IsScheduleable bit
			DECLARE @Prefix nvarchar(max)
			DECLARE @Honorific nvarchar(max)
			DECLARE @DisplayName nvarchar(max)
			DECLARE @OleColor int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE UsersUpdatedCursor CURSOR FOR
			SELECT Id, UserName, LastName, FirstName, MiddleName, Suffix, Pid, IsLoggedIn, UniqueId, IsArchived, IsScheduleable, Prefix, Honorific, DisplayName, OleColor, __EntityType__ FROM inserted
		
			OPEN UsersUpdatedCursor
			FETCH NEXT FROM UsersUpdatedCursor INTO @Id, @UserName, @LastName, @FirstName, @MiddleName, @Suffix, @Pid, @IsLoggedIn, @UniqueId, @IsArchived, @IsScheduleable, @Prefix, @Honorific, @DisplayName, @OleColor, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- User Update/Insert
				UPDATE Resources
				SET ResourceDescription = @DisplayName,
					ResourceFirstName = @FirstName,
					ResourceSuffix = @Honorific,
					[Status] = CASE @IsArchived
						WHEN 1
							THEN ''Z''
						WHEN 0
							THEN ''A''
						END,
					IsLoggedIn = @IsLoggedIn,
					ResourceType = (
						SELECT SUBSTRING(r.NAME, 1, 1)
						FROM model.UserRole ur
						INNER JOIN model.Roles r ON r.Id = ur.Roles_Id
						WHERE ur.Users_Id = @Id
						),
					ResourceLastName = @LastName,
					ResourceMI = @MiddleName,
					ResourceColor = @OleColor,
					ResourcePid = @PID,
					ResourcePrefix = @Prefix,
					ResourceName = @UserName,
					@UniqueId = CONVERT(UNIQUEIDENTIFIER, ''00000000-0000-0000-0000-00'' + LEFT(''0000000000'', (10 - LEN(CONVERT(VARCHAR, @Id)))) + CONVERT(VARCHAR, @Id))
				WHERE ResourceId = @Id
				
				FETCH NEXT FROM UsersUpdatedCursor INTO @Id, @UserName, @LastName, @FirstName, @MiddleName, @Suffix, @Pid, @IsLoggedIn, @UniqueId, @IsArchived, @IsScheduleable, @Prefix, @Honorific, @DisplayName, @OleColor, @__EntityType__ 
		
			END
		
			CLOSE UsersUpdatedCursor
			DEALLOCATE UsersUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Addresses')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isAddressesChanged BIT

		SET @isAddressesChanged = 0

		DECLARE @ismodelAddresses_Partition1Changed BIT

		SET @ismodelAddresses_Partition1Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Addresses_Partition1'
			,
			'
		CREATE VIEW model.Addresses_Partition1
		WITH SCHEMABINDING
		AS
		----Contact ExternalProvider address 1
		SELECT (CONVERT(bigint, 110000000) * 9 + pv.VendorId) AS Id,
			NULL AS BillingOrganizationId,
			pv.VendorCity AS City,
			VendorId AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			pv.VendorAddress AS Line1,
			pv.VendorSuite AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			NULL as PatientId,
			REPLACE(pv.VendorZip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.Id AS StateOrProvinceId,
			NULL AS UserId,
			''ContactAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			8 AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PracticeVendors pv
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pv.VendorState
		WHERE pv.VendorType = ''D''
			AND pv.VendorAddress <> ''''
			AND pv.VendorAddress IS NOT NULL
			AND pv.VendorCity <> ''''
			AND pv.VendorCity IS NOT NULL
			AND pv.VendorLastName <> ''''
			AND pv.VendorLastName IS NOT NULL
		'
			,@isChanged = @ismodelAddresses_Partition1Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_Addresses_Partition1'
					AND object_id = OBJECT_ID('model.Addresses_Partition1')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.Addresses_Partition1'

			CREATE UNIQUE CLUSTERED INDEX PK_Addresses_Partition1 ON model.Addresses_Partition1 ([Id]);
		END

		DECLARE @ismodelAddresses_Partition2Changed BIT

		SET @ismodelAddresses_Partition2Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Addresses_Partition2'
			,
			'
		CREATE VIEW model.Addresses_Partition2
		WITH SCHEMABINDING
		AS
		----ExternalOrganization address 1 for Doctors, Hosp, Labs, Vendors, Other (VendorType D, H, L, V, ~) 
		SELECT  (CONVERT(bigint, 110000000) * 12 + pv.VendorId) AS Id,
			NULL AS BillingOrganizationId,
			pv.VendorCity AS City,
			NULL AS ContactId,
			pv.VendorId AS ExternalOrganizationId,
			NULL AS InsurerId,
			pv.VendorAddress AS Line1,
			pv.VendorSuite AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			NULL as PatientId,
			REPLACE(pv.VendorZip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.ID AS StateOrProvinceId,
			NULL AS UserId,
			''ExternalOrganizationAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			11 AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PracticeVendors pv
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pv.VendorState
		WHERE pv.VendorFirmName <> ''''
			AND pv.VendorFirmName IS NOT NULL
			AND pv.VendorAddress <> ''''
			AND pv.VendorAddress IS NOT NULL
		'
			,@isChanged = @ismodelAddresses_Partition2Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_Addresses_Partition2'
					AND object_id = OBJECT_ID('model.Addresses_Partition2')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.Addresses_Partition2'

			CREATE UNIQUE CLUSTERED INDEX PK_Addresses_Partition2 ON model.Addresses_Partition2 ([Id]);
		END

		DECLARE @ismodelAddresses_Partition3Changed BIT

		SET @ismodelAddresses_Partition3Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Addresses_Partition3'
			,
			'
		CREATE VIEW model.Addresses_Partition3
		WITH SCHEMABINDING
		AS
		----InsurerAddress
		SELECT (CONVERT(bigint, 110000000) * 15 + InsurerId) AS Id,
			NULL AS BillingOrganizationId,
			pri.InsurerCity AS City,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			pri.InsurerId AS InsurerId,
			pri.Insureraddress AS Line1,
			CONVERT(NVARCHAR, NULL) AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			NULL as PatientId,
			REPLACE(pri.InsurerZip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.Id AS StateOrProvinceId,
			NULL AS UserId,
			''InsurerAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			3 AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PracticeInsurers pri
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pri.InsurerState
		WHERE pri.InsurerAddress <> ''''
			AND pri.InsurerAddress IS NOT NULL
		'
			,@isChanged = @ismodelAddresses_Partition3Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_Addresses_Partition3'
					AND object_id = OBJECT_ID('model.Addresses_Partition3')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.Addresses_Partition3'

			CREATE UNIQUE CLUSTERED INDEX PK_Addresses_Partition3 ON model.Addresses_Partition3 ([Id]);
		END

		DECLARE @ismodelAddresses_Partition4Changed BIT

		SET @ismodelAddresses_Partition4Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Addresses_Partition4'
			,
			'
		CREATE VIEW model.Addresses_Partition4
		WITH SCHEMABINDING
		AS
		----Patient''s first address
		SELECT  pd.PatientId AS Id,
			NULL AS BillingOrganizationId,
			COALESCE(pd.City, '''') AS City,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			COALESCE(pd.Address, '''') AS Line1,
			pd.Suite AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			PatientId as PatientId,
			COALESCE(REPLACE(pd.Zip,''-'',''''), '''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.ID AS StateOrProvinceId,
			NULL AS UserId,
			''PatientAddress'' AS __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			1 AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PatientDemographics pd
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pd.STATE
		WHERE pd.Address <> ''''
			OR pd.City <> ''''
			OR pd.State <> ''''
		'
			,@isChanged = @ismodelAddresses_Partition4Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_Addresses_Partition4'
					AND object_id = OBJECT_ID('model.Addresses_Partition4')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.Addresses_Partition4'

			CREATE UNIQUE CLUSTERED INDEX PK_Addresses_Partition4 ON model.Addresses_Partition4 ([Id]);
		END

		DECLARE @ismodelAddresses_Partition5Changed BIT

		SET @ismodelAddresses_Partition5Changed = 0

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Addresses_Partition5'
			,
			'
		CREATE VIEW model.Addresses_Partition5
		WITH SCHEMABINDING
		AS
		----Patient''s work address
		SELECT (CONVERT(bigint, 110000000) * 3 + pdWork.PatientId) AS Id,
			NULL AS BillingOrganizationId,
			pdWork.BusinessCity AS City,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			pdWork.BusinessAddress AS Line1,
			pdWork.BusinessSuite AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			pdWork.PatientId AS PatientId,
			REPLACE(pdWork.BusinessZip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.ID AS StateOrProvinceId,
			NULL AS UserId,
			''PatientAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			2 AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PatientDemographics pdWork
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pdWork.BusinessState
		WHERE pdWork.BusinessAddress <> ''''
			and pdWork.BusinessAddress IS NOT NULL
		'
			,@isChanged = @ismodelAddresses_Partition5Changed OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
			AND @createIndexes = 1
			AND NOT EXISTS (
				SELECT *
				FROM sys.indexes
				WHERE NAME = 'PK_Addresses_Partition5'
					AND object_id = OBJECT_ID('model.Addresses_Partition5')
				)
		BEGIN
			EXEC [dbo].[DropIndexes] 'model.Addresses_Partition5'

			CREATE UNIQUE CLUSTERED INDEX PK_Addresses_Partition5 ON model.Addresses_Partition5 ([Id]);
		END

		DECLARE @AddressesViewSql NVARCHAR(max)

		SET @AddressesViewSql = 
			'
		CREATE VIEW [model].[Addresses]
		WITH SCHEMABINDING
		AS
		----BillingOrganization address; 0 for PracticeName table   
		SELECT (CONVERT(bigint, 110000000) * 4 + pn.PracticeId) AS Id,
			pn.PracticeId AS BillingOrganizationId,
			pn.PracticeCity AS City,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			pn.PracticeAddress AS Line1,
			pn.PracticeSuite AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			NULL as PatientId,
			REPLACE(pn.PracticeZip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.Id AS StateOrProvinceId,
			NULL AS UserId,
			''BillingOrganizationAddress'' as __EntityType__,
			5 AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PracticeName pn
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pn.PracticeState
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		INNER JOIN dbo.Resources re ON re.PracticeId = pn.PracticeId
			AND (
				re.ResourceType in (''D'', ''Q'', ''Z'',''Y'') AND re.Billable = ''Y''
					OR
				(re.ResourceType = ''R''
					AND pic.FieldValue = ''T''
					AND re.ServiceCode = ''02''
					AND re.Billable = ''Y'' 
					AND pn.LocationReference <> '''')
				)
		WHERE pn.PracticeType = ''P''
			AND pn.PracticeAddress <> ''''
			AND pn.PracticeAddress IS NOT NULL
		GROUP BY pn.PracticeId,
			pn.LocationReference,
			pn.PracticeCity,
			pn.PracticeAddress,
			pn.PracticeSuite,
			pn.PracticeState,
			pn.PracticeZip,
			st.Id
	
		UNION ALL
	
		----BillingOrganization address; 1 for Resources table (OrgOverride/Override doctors)
		SELECT (CONVERT(bigint, 110000000) * 5 + re.ResourceId) AS Id
			,(110000000 * 1 + re.ResourceId) AS BillingOrganizationId
			,re.ResourceCity AS City
			,NULL AS ContactId
			,NULL AS ExternalOrganizationId
			,NULL AS InsurerId
			,re.ResourceAddress AS Line1
			,re.ResourceSuite AS Line2
			,CONVERT(NVARCHAR, NULL) AS Line3
			,NULL AS PatientId
			,REPLACE(re.ResourceZip,''-'','''') AS PostalCode
			,NULL AS ServiceLocationId
			,st.Id AS StateOrProvinceId
			,NULL AS UserId
			,''BillingOrganizationAddress'' AS __EntityType__
			,5 AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.Resources re
		LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = re.ResourceState
		INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
			AND pa.ResourceType = ''R''
		WHERE re.ResourceType IN (
				''D''
				,''Q''
				,''Z''
				,''Y''
				)
			AND (
				pa.[Override] = ''Y''
				OR pa.OrgOverride = ''Y''
				)
			AND re.ResourceAddress <> ''''
			AND re.ResourceAddress IS NOT NULL
		GROUP BY re.ResourceId
			,re.ResourceAddress
			,re.ResourceCity
			,re.PlaceOfService
			,re.ResourceSuite
			,st.Id
			,re.ResourceZip
	
	
		UNION ALL
	
		----BillingOrganization PayTo Address
		SELECT (CONVERT(bigint, 110000000) * 17 + pnBO.PracticeId) AS Id,
			pnBO.PracticeId AS BillingOrganizationId,
			pnPayTo.PracticeCity AS City,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			pnPayTo.PracticeAddress AS Line1,
			pnPayTo.PracticeSuite AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			NULL as PatientId,
			REPLACE(pnPayTo.PracticeZip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.Id AS StateOrProvinceId,
			NULL AS UserId,
			''BillingOrganizationAddress'' as __EntityType__,
			6 AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PracticeName pnPayTo
		INNER JOIN dbo.PracticeName pnBO on pnPayTo.PracticeId = pnBO.CatArray2
			AND pnBO.PracticeType = ''P''
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pnPayTo.PracticeState
		WHERE pnPayTo.PracticeType = ''P''
			AND pnPayTo.PracticeAddress <> ''''
			AND pnPayTo.PracticeAddress IS NOT NULL
		GROUP BY pnBO.PracticeId,
			pnPayTo.PracticeId,
			pnPayTo.PracticeCity,
			pnPayTo.PracticeAddress,
			pnPayTo.PracticeSuite,
			pnPayTo.PracticeZip,
			st.Id
	
		UNION ALL
	
		SELECT model.Addresses_Partition1.Id AS Id, model.Addresses_Partition1.BillingOrganizationId AS BillingOrganizationId, model.Addresses_Partition1.City AS City, model.Addresses_Partition1.ContactId AS ContactId, model.Addresses_Partition1.ExternalOrganizationId AS ExternalOrganizationId, model.Addresses_Partition1.InsurerId AS InsurerId, model.Addresses_Partition1.Line1 AS Line1, model.Addresses_Partition1.Line2 AS Line2, model.Addresses_Partition1.Line3 AS Line3, model.Addresses_Partition1.PatientId AS PatientId, model.Addresses_Partition1.PostalCode AS PostalCode, model.Addresses_Partition1.ServiceLocationId AS ServiceLocationId, model.Addresses_Partition1.StateOrProvinceId AS StateOrProvinceId, model.Addresses_Partition1.UserId AS UserId, model.Addresses_Partition1.__EntityType__ AS __EntityType__, model.Addresses_Partition1.BillingOrganizationAddressTypeId AS BillingOrganizationAddressTypeId, model.Addresses_Partition1.ContactAddressTypeId AS ContactAddressTypeId, model.Addresses_Partition1.ExternalOrganizationAddressTypeId AS ExternalOrganizationAddressTypeId, model.Addresses_Partition1.InsurerAddressTypeId AS InsurerAddressTypeId, model.Addresses_Partition1.PatientAddressTypeId AS PatientAddressTypeId, model.Addresses_Partition1.ServiceLocationAddressTypeId AS ServiceLocationAddressTypeId, model.Addresses_Partition1.UserAddressTypeId AS UserAddressTypeId FROM model.Addresses_Partition1 WITH(NOEXPAND)
	
		UNION ALL
	
		----Contact ExternalProvider address 2
		SELECT (CONVERT(bigint, 110000000) * 10 + pv.VendorId) AS Id,
			NULL AS BillingOrganizationId,
			pv.VendorCity AS City,
			VendorId AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			pv.VendorOtherOffice1 AS Line1,
			CONVERT(NVARCHAR, NULL) AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			NULL as PatientId,
			REPLACE(pv.VendorZip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.Id AS StateOrProvinceId,
			NULL AS UserId,
			''ContactAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			at.Id AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PracticeVendors pv
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pv.VendorState
		LEFT OUTER JOIN model.AddressTypes at ON at.Name = ''Other1''
			AND at.__EntityType__ = ''ContactAddressType'' 
		WHERE VendorType = ''D''
			AND pv.VendorOtherOffice1 <> ''''
			AND pv.VendorOtherOffice1 IS NOT NULL
			AND pv.VendorLastName <> ''''
			AND pv.VendorLastName IS NOT NULL
	
		UNION ALL
		----Contact ExternalProvider address3
		SELECT (CONVERT(bigint, 110000000) * 11 + pv.VendorId) as Id,
			NULL AS BillingOrganizationId,
			pv.VendorCity AS City,
			VendorId AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			pv.VendorOtherOffice2 AS Line1,
			CONVERT(NVARCHAR, NULL) AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			NULL as PatientId,
			REPLACE(pv.VendorZip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.Id AS StateOrProvinceId,
			NULL AS UserId,
			''ContactAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			at.Id AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PracticeVendors pv
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pv.VendorState
		LEFT OUTER JOIN model.AddressTypes at ON  at.Name = ''Other2''
			AND at.__EntityType__ = ''ContactAddressType'' 
			WHERE VendorType = ''D''
			AND pv.VendorOtherOffice2 <> ''''
			AND pv.VendorOtherOffice2 IS NOT NULL
			AND pv.VendorLastName <> ''''
			AND pv.VendorLastName IS NOT NULL
	
		UNION ALL
	
		SELECT model.Addresses_Partition2.Id AS Id, model.Addresses_Partition2.BillingOrganizationId AS BillingOrganizationId, model.Addresses_Partition2.City AS City, model.Addresses_Partition2.ContactId AS ContactId, model.Addresses_Partition2.ExternalOrganizationId AS ExternalOrganizationId, model.Addresses_Partition2.InsurerId AS InsurerId, model.Addresses_Partition2.Line1 AS Line1, model.Addresses_Partition2.Line2 AS Line2, model.Addresses_Partition2.Line3 AS Line3, model.Addresses_Partition2.PatientId AS PatientId, model.Addresses_Partition2.PostalCode AS PostalCode, model.Addresses_Partition2.ServiceLocationId AS ServiceLocationId, model.Addresses_Partition2.StateOrProvinceId AS StateOrProvinceId, model.Addresses_Partition2.UserId AS UserId, model.Addresses_Partition2.__EntityType__ AS __EntityType__, model.Addresses_Partition2.BillingOrganizationAddressTypeId AS BillingOrganizationAddressTypeId, model.Addresses_Partition2.ContactAddressTypeId AS ContactAddressTypeId, model.Addresses_Partition2.ExternalOrganizationAddressTypeId AS ExternalOrganizationAddressTypeId, model.Addresses_Partition2.InsurerAddressTypeId AS InsurerAddressTypeId, model.Addresses_Partition2.PatientAddressTypeId AS PatientAddressTypeId, model.Addresses_Partition2.ServiceLocationAddressTypeId AS ServiceLocationAddressTypeId, model.Addresses_Partition2.UserAddressTypeId AS UserAddressTypeId FROM model.Addresses_Partition2 WITH(NOEXPAND)
	
		UNION ALL
	
		----ExternalOrganization address 2 (Doctors only)
		SELECT  (CONVERT(bigint, 110000000) * 13 + pv.VendorId) AS Id,
			NULL AS BillingOrganizationId,
			pv.VendorCity AS City,
			NULL AS ContactId,
			pv.VendorId AS ExternalOrganizationId,
			NULL AS InsurerId,
			pv.VendorOtherOffice1 AS Line1,
			CONVERT(NVARCHAR, NULL) AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			NULL as PatientId,
			REPLACE(pv.VendorZip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.ID AS StateOrProvinceId,
			NULL AS UserId,
			''ExternalOrganizationAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			at.Id AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PracticeVendors pv
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pv.VendorState
		LEFT OUTER JOIN model.AddressTypes at ON at.Name = ''Other1''
			AND at.__EntityType__ = ''ExternalOrganizationAddressType'' 
		WHERE pv.VendorType = ''D''
			AND pv.VendorFirmName <> ''''
			AND pv.VendorFirmName IS NOT NULL
			AND pv.VendorOtherOffice1 <> ''''
			AND pv.VendorOtherOffice1 IS NOT NULL
	
		UNION ALL
	
		----ExternalOrganization address 3 (Doctors only)
		SELECT (CONVERT(bigint, 110000000) * 14 + pv.VendorId) AS Id,
			NULL AS BillingOrganizationId,
			pv.VendorCity AS City,
			NULL AS ContactId,
			pv.VendorId AS ExternalOrganizationId,
			NULL AS InsurerId,
			pv.VendorOtherOffice2 AS Line1,
			CONVERT(NVARCHAR, NULL) AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			NULL as PatientId,
			REPLACE(pv.VendorZip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.ID AS StateOrProvinceId,
			NULL AS UserId,
			''ExternalOrganizationAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			at.Id AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PracticeVendors pv
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pv.VendorState
		LEFT OUTER JOIN model.AddressTypes at ON at.Name = ''Other2''
			AND at.__EntityType__ = ''ExternalOrganizationAddressType'' 
		WHERE pv.VendorType = ''D''
			AND pv.VendorFirmName <> ''''
			AND pv.VendorFirmName IS NOT NULL
			AND pv.VendorOtherOffice2 <> ''''
			AND pv.VendorOtherOffice2 IS NOT NULL
	
		UNION ALL
	
		SELECT model.Addresses_Partition3.Id AS Id, model.Addresses_Partition3.BillingOrganizationId AS BillingOrganizationId, model.Addresses_Partition3.City AS City, model.Addresses_Partition3.ContactId AS ContactId, model.Addresses_Partition3.ExternalOrganizationId AS ExternalOrganizationId, model.Addresses_Partition3.InsurerId AS InsurerId, model.Addresses_Partition3.Line1 AS Line1, model.Addresses_Partition3.Line2 AS Line2, model.Addresses_Partition3.Line3 AS Line3, model.Addresses_Partition3.PatientId AS PatientId, model.Addresses_Partition3.PostalCode AS PostalCode, model.Addresses_Partition3.ServiceLocationId AS ServiceLocationId, model.Addresses_Partition3.StateOrProvinceId AS StateOrProvinceId, model.Addresses_Partition3.UserId AS UserId, model.Addresses_Partition3.__EntityType__ AS __EntityType__, model.Addresses_Partition3.BillingOrganizationAddressTypeId AS BillingOrganizationAddressTypeId, model.Addresses_Partition3.ContactAddressTypeId AS ContactAddressTypeId, model.Addresses_Partition3.ExternalOrganizationAddressTypeId AS ExternalOrganizationAddressTypeId, model.Addresses_Partition3.InsurerAddressTypeId AS InsurerAddressTypeId, model.Addresses_Partition3.PatientAddressTypeId AS PatientAddressTypeId, model.Addresses_Partition3.ServiceLocationAddressTypeId AS ServiceLocationAddressTypeId, model.Addresses_Partition3.UserAddressTypeId AS UserAddressTypeId FROM model.Addresses_Partition3 WITH(NOEXPAND)
	
		UNION ALL
	
		SELECT model.Addresses_Partition4.Id AS Id, model.Addresses_Partition4.BillingOrganizationId AS BillingOrganizationId, model.Addresses_Partition4.City AS City, model.Addresses_Partition4.ContactId AS ContactId, model.Addresses_Partition4.ExternalOrganizationId AS ExternalOrganizationId, model.Addresses_Partition4.InsurerId AS InsurerId, model.Addresses_Partition4.Line1 AS Line1, model.Addresses_Partition4.Line2 AS Line2, model.Addresses_Partition4.Line3 AS Line3, model.Addresses_Partition4.PatientId AS PatientId, model.Addresses_Partition4.PostalCode AS PostalCode, model.Addresses_Partition4.ServiceLocationId AS ServiceLocationId, model.Addresses_Partition4.StateOrProvinceId AS StateOrProvinceId, model.Addresses_Partition4.UserId AS UserId, model.Addresses_Partition4.__EntityType__ AS __EntityType__, model.Addresses_Partition4.BillingOrganizationAddressTypeId AS BillingOrganizationAddressTypeId, model.Addresses_Partition4.ContactAddressTypeId AS ContactAddressTypeId, model.Addresses_Partition4.ExternalOrganizationAddressTypeId AS ExternalOrganizationAddressTypeId, model.Addresses_Partition4.InsurerAddressTypeId AS InsurerAddressTypeId, model.Addresses_Partition4.PatientAddressTypeId AS PatientAddressTypeId, model.Addresses_Partition4.ServiceLocationAddressTypeId AS ServiceLocationAddressTypeId, model.Addresses_Partition4.UserAddressTypeId AS UserAddressTypeId FROM model.Addresses_Partition4 WITH(NOEXPAND)
	
		UNION ALL
	
		----Patient''s 2nd address, PatientDemoAlt table
		SELECT (CONVERT(bigint, 110000000) * 1 + pdAlt.PatientId) AS Id,
			NULL AS BillingOrganizationId,
			pdAlt.City AS City,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			pdAlt.Address AS Line1,
			pdAlt.Suite AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			PatientId as PatientId,
			REPLACE(pdAlt.Zip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.ID AS StateOrProvinceId,
			NULL AS UserId,
			''PatientAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			at.Id AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PatientDemoAlt pdAlt
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pdAlt.STATE
		LEFT OUTER JOIN model.AddressTypes at ON at.Name = ''Other1''
			AND at.__EntityType__ = ''PatientAddressType'' 
		WHERE pdAlt.AltId = 1
			AND pdalt.Address <> ''''
	
		UNION ALL
	
		----Patient''s 3rd address, PatientDemoAlt table
		SELECT (CONVERT(bigint, 110000000) * 2 + pdAlt.PatientId) AS Id,
			NULL AS BillingOrganizationId,
			pdAlt.City AS City,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			pdAlt.Address AS Line1,
			pdAlt.Suite AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			PatientId AS PatientId,
			REPLACE(pdAlt.Zip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.ID AS StateOrProvinceId,
			NULL AS UserId,
			''PatientAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			at.Id AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PatientDemoAlt pdAlt
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pdAlt.STATE
		LEFT OUTER JOIN model.AddressTypes at ON at.Name = ''Other2''
			AND at.__EntityType__ = ''PatientAddressType'' 
		WHERE pdAlt.AltId = 2
			AND pdalt.Address <> ''''
	
		UNION ALL
	
		SELECT model.Addresses_Partition5.Id AS Id, model.Addresses_Partition5.BillingOrganizationId AS BillingOrganizationId, model.Addresses_Partition5.City AS City, model.Addresses_Partition5.ContactId AS ContactId, model.Addresses_Partition5.ExternalOrganizationId AS ExternalOrganizationId, model.Addresses_Partition5.InsurerId AS InsurerId, model.Addresses_Partition5.Line1 AS Line1, model.Addresses_Partition5.Line2 AS Line2, model.Addresses_Partition5.Line3 AS Line3, model.Addresses_Partition5.PatientId AS PatientId, model.Addresses_Partition5.PostalCode AS PostalCode, model.Addresses_Partition5.ServiceLocationId AS ServiceLocationId, model.Addresses_Partition5.StateOrProvinceId AS StateOrProvinceId, model.Addresses_Partition5.UserId AS UserId, model.Addresses_Partition5.__EntityType__ AS __EntityType__, model.Addresses_Partition5.BillingOrganizationAddressTypeId AS BillingOrganizationAddressTypeId, model.Addresses_Partition5.ContactAddressTypeId AS ContactAddressTypeId, model.Addresses_Partition5.ExternalOrganizationAddressTypeId AS ExternalOrganizationAddressTypeId, model.Addresses_Partition5.InsurerAddressTypeId AS InsurerAddressTypeId, model.Addresses_Partition5.PatientAddressTypeId AS PatientAddressTypeId, model.Addresses_Partition5.ServiceLocationAddressTypeId AS ServiceLocationAddressTypeId, model.Addresses_Partition5.UserAddressTypeId AS UserAddressTypeId FROM model.Addresses_Partition5 WITH(NOEXPAND)
	
		UNION ALL
	
		----ServiceLocation address; PracticeName table
		SELECT  (CONVERT(bigint, 110000000) * 6 + pn.PracticeId) AS Id,
			NULL AS BillingOrganizationId,
			pn.PracticeCity AS City,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			pn.PracticeAddress AS Line1,
			pn.PracticeSuite AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			NULL as PatientId,
			REPLACE(pn.PracticeZip,''-'','''') AS PostalCode,
			pn.PracticeId AS ServiceLocationId,
			st.Id AS StateOrProvinceId,
			NULL AS UserId,
			''ServiceLocationAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			7 AS ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.PracticeName pn
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pn.PracticeState
		WHERE pn.PracticeType = ''P''
			AND pn.PracticeAddress <> ''''
			AND pn.PracticeAddress IS NOT NULL
		GROUP BY pn.PracticeId,
			pn.LocationReference,
			pn.PracticeCity,
			pn.PracticeAddress,
			pn.PracticeSuite,
			pn.PracticeState,
			pn.PracticeZip,
			st.Id
	
		UNION ALL
	
		----ServiceLocation address; Resources table
		SELECT  (CONVERT(bigint, 110000000) * 7 + re.ResourceId) AS Id,
			NULL AS BillingOrganizationId,
			re.ResourceCity AS City,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			re.ResourceAddress AS Line1,
			re.ResourceSuite AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			NULL as PatientId,
			REPLACE(re.ResourceZip,''-'','''') AS PostalCode,
			(110000000 * 1 + re.ResourceId) AS ServiceLocationId,
			st.Id AS StateOrProvinceId,
			NULL AS UserId,
			''ServiceLocationAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			7 ServiceLocationAddressTypeId,
			NULL AS UserAddressTypeId
		FROM dbo.Resources re
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = re.ResourceState
		LEFT OUTER JOIN model.AddressTypes at ON at.Name = ''Other1''
			AND at.__EntityType__ = ''ServiceLocationAddressType'' 
		LEFT OUTER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		LEFT OUTER JOIN dbo.PracticeName pn on re.PracticeId = pn.PracticeId
			AND pic.FieldValue = ''T''
			AND re.Billable = ''Y''
			AND pn.LocationReference <> ''''
		WHERE re.ResourceType = ''R''
			AND re.ServiceCode = ''02''
			AND pn.PracticeId IS NULL
			AND re.ResourceAddress <> ''''
			AND re.ResourceAddress IS NOT NULL
		GROUP BY re.ResourceId,
			re.ResourceAddress,
			re.ResourceCity,
			re.PlaceOfService,
			re.ResourceSuite,
			st.Id,
			re.ResourceZip
	
		UNION ALL
	
		----User address; 
		SELECT (CONVERT(bigint, 110000000) * 8 + re.ResourceId) AS Id,
			NULL AS BillingOrganizationId,
			re.ResourceCity AS City,
			NULL AS ContactId,
			NULL AS ExternalOrganizationId,
			NULL AS InsurerId,
			re.ResourceAddress AS Line1,
			re.ResourceSuite AS Line2,
			CONVERT(NVARCHAR, NULL) AS Line3,
			NULL as PatientId,
			REPLACE(re.ResourceZip,''-'','''') AS PostalCode,
			NULL AS ServiceLocationId,
			st.Id AS StateOrProvinceId,
			re.ResourceId AS UserId,
			''UserAddress'' as __EntityType__,
			NULL AS BillingOrganizationAddressTypeId,
			NULL AS ContactAddressTypeId,
			NULL AS ExternalOrganizationAddressTypeId,
			NULL AS InsurerAddressTypeId,
			NULL AS PatientAddressTypeId,
			NULL AS ServiceLocationAddressTypeId,
			at.Id AS UserAddressTypeId
		FROM dbo.Resources re
		INNER JOIN model.StateOrProvinces st ON st.Abbreviation = re.ResourceState
		LEFT OUTER JOIN model.AddressTypes at ON at.Name = ''User1''
		WHERE re.ResourceType <> ''R''
			AND re.ResourceAddress <> ''''
			AND re.ResourceAddress IS NOT NULL'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Addresses'
			,@AddressesViewSql
			,@isChanged = @isAddressesChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAddressDelete'
				,
				'
			CREATE TRIGGER model.OnAddressDelete ON [model].[Addresses] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @Line1 nvarchar(max)
			DECLARE @Line2 nvarchar(max)
			DECLARE @City nvarchar(max)
			DECLARE @PostalCode nvarchar(max)
			DECLARE @Line3 nvarchar(max)
			DECLARE @StateOrProvinceId int
			DECLARE @UserId int
			DECLARE @UserAddressTypeId int
			DECLARE @BillingOrganizationId int
			DECLARE @BillingOrganizationAddressTypeId int
			DECLARE @ServiceLocationId int
			DECLARE @ServiceLocationAddressTypeId int
			DECLARE @ContactId int
			DECLARE @ContactAddressTypeId int
			DECLARE @ExternalOrganizationId int
			DECLARE @ExternalOrganizationAddressTypeId int
			DECLARE @InsurerId int
			DECLARE @InsurerAddressTypeId int
			DECLARE @PatientId int
			DECLARE @PatientAddressTypeId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE AddressesDeletedCursor CURSOR FOR
			SELECT Id, Line1, Line2, City, PostalCode, Line3, StateOrProvinceId, UserId, UserAddressTypeId, BillingOrganizationId, BillingOrganizationAddressTypeId, ServiceLocationId, ServiceLocationAddressTypeId, ContactId, ContactAddressTypeId, ExternalOrganizationId, ExternalOrganizationAddressTypeId, InsurerId, InsurerAddressTypeId, PatientId, PatientAddressTypeId, __EntityType__ FROM deleted
		
			OPEN AddressesDeletedCursor
			FETCH NEXT FROM AddressesDeletedCursor INTO @Id, @Line1, @Line2, @City, @PostalCode, @Line3, @StateOrProvinceId, @UserId, @UserAddressTypeId, @BillingOrganizationId, @BillingOrganizationAddressTypeId, @ServiceLocationId, @ServiceLocationAddressTypeId, @ContactId, @ContactAddressTypeId, @ExternalOrganizationId, @ExternalOrganizationAddressTypeId, @InsurerId, @InsurerAddressTypeId, @PatientId, @PatientAddressTypeId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for Address --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @Line1 AS Line1, @Line2 AS Line2, @City AS City, @PostalCode AS PostalCode, @Line3 AS Line3, @StateOrProvinceId AS StateOrProvinceId, @UserId AS UserId, @UserAddressTypeId AS UserAddressTypeId, @BillingOrganizationId AS BillingOrganizationId, @BillingOrganizationAddressTypeId AS BillingOrganizationAddressTypeId, @ServiceLocationId AS ServiceLocationId, @ServiceLocationAddressTypeId AS ServiceLocationAddressTypeId, @ContactId AS ContactId, @ContactAddressTypeId AS ContactAddressTypeId, @ExternalOrganizationId AS ExternalOrganizationId, @ExternalOrganizationAddressTypeId AS ExternalOrganizationAddressTypeId, @InsurerId AS InsurerId, @InsurerAddressTypeId AS InsurerAddressTypeId, @PatientId AS PatientId, @PatientAddressTypeId AS PatientAddressTypeId, @__EntityType__ AS __EntityType__
			
				FETCH NEXT FROM AddressesDeletedCursor INTO @Id, @Line1, @Line2, @City, @PostalCode, @Line3, @StateOrProvinceId, @UserId, @UserAddressTypeId, @BillingOrganizationId, @BillingOrganizationAddressTypeId, @ServiceLocationId, @ServiceLocationAddressTypeId, @ContactId, @ContactAddressTypeId, @ExternalOrganizationId, @ExternalOrganizationAddressTypeId, @InsurerId, @InsurerAddressTypeId, @PatientId, @PatientAddressTypeId, @__EntityType__ 
		
			END
		
			CLOSE AddressesDeletedCursor
			DEALLOCATE AddressesDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAddressInsert'
				,
				'
			CREATE TRIGGER model.OnAddressInsert ON [model].[Addresses] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @Line1 nvarchar(max)
			DECLARE @Line2 nvarchar(max)
			DECLARE @City nvarchar(max)
			DECLARE @PostalCode nvarchar(max)
			DECLARE @Line3 nvarchar(max)
			DECLARE @StateOrProvinceId int
			DECLARE @UserId int
			DECLARE @UserAddressTypeId int
			DECLARE @BillingOrganizationId int
			DECLARE @BillingOrganizationAddressTypeId int
			DECLARE @ServiceLocationId int
			DECLARE @ServiceLocationAddressTypeId int
			DECLARE @ContactId int
			DECLARE @ContactAddressTypeId int
			DECLARE @ExternalOrganizationId int
			DECLARE @ExternalOrganizationAddressTypeId int
			DECLARE @InsurerId int
			DECLARE @InsurerAddressTypeId int
			DECLARE @PatientId int
			DECLARE @PatientAddressTypeId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE AddressesInsertedCursor CURSOR FOR
			SELECT Id, Line1, Line2, City, PostalCode, Line3, StateOrProvinceId, UserId, UserAddressTypeId, BillingOrganizationId, BillingOrganizationAddressTypeId, ServiceLocationId, ServiceLocationAddressTypeId, ContactId, ContactAddressTypeId, ExternalOrganizationId, ExternalOrganizationAddressTypeId, InsurerId, InsurerAddressTypeId, PatientId, PatientAddressTypeId, __EntityType__ FROM inserted
		
			OPEN AddressesInsertedCursor
			FETCH NEXT FROM AddressesInsertedCursor INTO @Id, @Line1, @Line2, @City, @PostalCode, @Line3, @StateOrProvinceId, @UserId, @UserAddressTypeId, @BillingOrganizationId, @BillingOrganizationAddressTypeId, @ServiceLocationId, @ServiceLocationAddressTypeId, @ContactId, @ContactAddressTypeId, @ExternalOrganizationId, @ExternalOrganizationAddressTypeId, @InsurerId, @InsurerAddressTypeId, @PatientId, @PatientAddressTypeId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- Address Insert
				IF @BillingOrganizationId IS NOT NULL
					SET @Id = (
							SELECT model.GetBigPairId(CASE x
										WHEN 0
											THEN 4
										WHEN 1
											THEN 5
										WHEN 2
											THEN 17
										END, y, 110000000)
							FROM model.GetBigIdPair(@BillingOrganizationId, 110000000)
							)
			
				IF @PatientId IS NOT NULL
			
				BEGIN
					DECLARE @addressTypeName nvarchar(255)
					SET @addressTypeName = (
						SELECT NAME FROM model.AddressTypes WHERE __EntityType__ = ''PatientAddressType''
						AND Id = @PatientAddressTypeId
						)
					DECLARE @patientPairId int
					SET @patientPairId = (
								SELECT y
								FROM model.GetIdPair(@PatientId, 202000000)
								)
					IF @addressTypeName = ''Other1''
					BEGIN
						INSERT INTO PatientDemoAlt (
							PatientId
							,AltId
							,Selected
							)
						VALUES (
							@patientPairId
							,1
							,0
							)
			
						SET @Id = model.GetPairId(1, @patientPairId, 110000000)
					END
					ELSE
						IF @AddresstypeName = ''Other2''
							BEGIN
								INSERT INTO PatientDemoAlt (
									PatientId
									,AltId
									,Selected
									)
								VALUES (
									@patientPairId
									,2
									,0
									)
			
								SET @Id = model.GetPairId(2, @patientPairId, 110000000)
							END
						ELSE
						BEGIN
							SET @Id = model.GetPairId(CASE @PatientAddressTypeId
												WHEN 1
													THEN 0
												WHEN 2
													THEN 3
												END, @patientPairId, 110000000)
						END
				END
			
				IF @ContactId IS NOT NULL
					SET @Id = (
							model.GetPairId(CASE @ContactAddressTypeId
									WHEN 8
										THEN 9
									WHEN (
											SELECT Id
											FROM model.AddressTypes
											WHERE NAME = ''Other1''
												AND __EntityType__ = ''ContactAddressType''
											)
										THEN 10
									WHEN (
											SELECT Id
											FROM model.AddressTypes
											WHERE NAME = ''Other2''
												AND __EntityType__ = ''ContactAddressType''
											)
										THEN 11
									END, @ContactId, 110000000)
							)
			
				IF @ExternalOrganizationId IS NOT NULL
					SET @Id = (
							model.GetPairId(CASE @ExternalOrganizationAddressTypeId
									WHEN 11
										THEN 12
									WHEN (
											SELECT Id
											FROM model.AddressTypes
											WHERE NAME = ''Other1''
												AND __EntityType__ = ''ExternalOrganizationAddressType''
											)
										THEN 13
									WHEN (
											SELECT Id
											FROM model.AddressTypes
											WHERE NAME = ''Other2''
												AND __EntityType__ = ''ExternalOrganizationAddressType''
											)
										THEN 14
									END, @ExternalOrganizationId, 110000000)
							)
			
				IF @InsurerId IS NOT NULL
					SET @Id = (model.GetPairId(15, @InsurerId, 110000000))
			
				IF @ServiceLocationId IS NOT NULL
					SET @Id = (
							SELECT model.GetPairId(CASE x
										WHEN 0
											THEN 6
										WHEN 1
											THEN 7
										END, y, 110000000)
							FROM model.GetIdPair(@ServiceLocationId, 110000000)
							)
			
				IF @UserId IS NOT NULL
					SET @Id = model.GetPairId(8, @UserId, 110000000)
			
							
				-- Address Update/Insert
				DECLARE @pair TABLE (
					x int
					,y int
					)
				DECLARE @x int
					,@y int
			
				SELECT @x = x
					,@y = y
				FROM model.GetBigIdPair(@Id, 110000000)
			
				---Patient Primary
				IF @x = 0
					BEGIN
						UPDATE PatientDemographics
						SET City = @City
							,Address = @Line1
							,Suite = @Line2
							,Zip = @PostalCode
							,STATE = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE PatientId = @y
					END
				---Patient 2nd Address
				IF @x = 1
					BEGIN
						UPDATE PatientDemoAlt
						SET City = @City
							,Address = @Line1
							,Suite = @Line2
							,Zip = @PostalCode
							,STATE = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE PatientId = @y
					END
				---Patient''s 3rd address
				IF @x = 2
					BEGIN
						UPDATE PatientDemoAlt
						SET City = @City
							,Address = @Line1
							,Suite = @Line2
							,Zip = @PostalCode
							,STATE = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE PatientId = @y
					END
				---Patient''s Work Address
				IF @x = 3
					BEGIN
						UPDATE PatientDemographics
						SET BusinessCity = @City
							,BusinessAddress = @Line1
							,BusinessSuite = @Line2
							,BusinessZip = @PostalCode
							,BusinessState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE PatientId = @y
					END
				---BillingOrganization, ServiceLocation -PracticeName Table
				IF @x IN (
						4
						,6
						, 17
						)
					BEGIN
						UPDATE PracticeName
						SET PracticeCity = @City
							,PracticeAddress = @Line1
							,PracticeSuite = @Line2
							,PracticeZip = @PostalCode
							,PracticeState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE PracticeId = @y
					END
				---BillingOrganization, ServiceLocation, User -Resources Table
				IF @x IN (
						5
						,7
						,8
						)
					BEGIN
					IF @x = 7 AND @ServiceLocationAddressTypeId = 7
						RAISERROR(''Cannot update resource table with main office'', 16, 1)
					ELSE
						UPDATE Resources
						SET ResourceCity = @City
							,ResourceAddress = @Line1
							,ResourceSuite = @Line2
							,ResourceZip = @PostalCode
							,ResourceState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE ResourceId = @y
					END
				---ExternalProvider -PracticeVendors
				IF @x IN (
						9
						,12
						)
					BEGIN
						UPDATE PracticeVendors
						SET VendorCity = @City
							,VendorAddress = @Line1
							,VendorSuite = @Line2
							,VendorZip = @PostalCode
							,VendorState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE VendorId = @y
					END
				---ExternalProvider Address 2 -PracticeVendors
				IF @x IN (
						10
						,13
						)
					BEGIN
						UPDATE PracticeVendors
						SET VendorCity = @City
							,VendorOtherOffice1 = @Line1
							,VendorSuite = @Line2
							,VendorZip = @PostalCode
							,VendorState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE VendorId = @y
					END
				---ExternalProvider Address 3 -PracticeVendors
				IF @x IN (
						11
						,14
						)
					BEGIN
						UPDATE PracticeVendors
						SET VendorCity = @City
							,VendorOtherOffice2 = @Line1
							,VendorSuite = @Line2
							,VendorZip = @PostalCode
							,VendorState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE VendorId = @y
					END
				---InsurerAddress -PracticeInsurers
				IF @x = 15
					BEGIN
						UPDATE PracticeInsurers
						SET InsurerCity = @City
							,Insureraddress = @Line1
							,InsurerZip = @PostalCode
							,InsurerState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE InsurerId = @y
					END
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @Line1 AS Line1, @Line2 AS Line2, @City AS City, @PostalCode AS PostalCode, @Line3 AS Line3, @StateOrProvinceId AS StateOrProvinceId, @UserId AS UserId, @UserAddressTypeId AS UserAddressTypeId, @BillingOrganizationId AS BillingOrganizationId, @BillingOrganizationAddressTypeId AS BillingOrganizationAddressTypeId, @ServiceLocationId AS ServiceLocationId, @ServiceLocationAddressTypeId AS ServiceLocationAddressTypeId, @ContactId AS ContactId, @ContactAddressTypeId AS ContactAddressTypeId, @ExternalOrganizationId AS ExternalOrganizationId, @ExternalOrganizationAddressTypeId AS ExternalOrganizationAddressTypeId, @InsurerId AS InsurerId, @InsurerAddressTypeId AS InsurerAddressTypeId, @PatientId AS PatientId, @PatientAddressTypeId AS PatientAddressTypeId, @__EntityType__ AS __EntityType__
				
				FETCH NEXT FROM AddressesInsertedCursor INTO @Id, @Line1, @Line2, @City, @PostalCode, @Line3, @StateOrProvinceId, @UserId, @UserAddressTypeId, @BillingOrganizationId, @BillingOrganizationAddressTypeId, @ServiceLocationId, @ServiceLocationAddressTypeId, @ContactId, @ContactAddressTypeId, @ExternalOrganizationId, @ExternalOrganizationAddressTypeId, @InsurerId, @InsurerAddressTypeId, @PatientId, @PatientAddressTypeId, @__EntityType__ 
		
			END
		
			CLOSE AddressesInsertedCursor
			DEALLOCATE AddressesInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnAddressUpdate'
				,
				'
			CREATE TRIGGER model.OnAddressUpdate ON [model].[Addresses] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id bigint
			DECLARE @Line1 nvarchar(max)
			DECLARE @Line2 nvarchar(max)
			DECLARE @City nvarchar(max)
			DECLARE @PostalCode nvarchar(max)
			DECLARE @Line3 nvarchar(max)
			DECLARE @StateOrProvinceId int
			DECLARE @UserId int
			DECLARE @UserAddressTypeId int
			DECLARE @BillingOrganizationId int
			DECLARE @BillingOrganizationAddressTypeId int
			DECLARE @ServiceLocationId int
			DECLARE @ServiceLocationAddressTypeId int
			DECLARE @ContactId int
			DECLARE @ContactAddressTypeId int
			DECLARE @ExternalOrganizationId int
			DECLARE @ExternalOrganizationAddressTypeId int
			DECLARE @InsurerId int
			DECLARE @InsurerAddressTypeId int
			DECLARE @PatientId int
			DECLARE @PatientAddressTypeId int
			DECLARE @__EntityType__ nvarchar(100)
		
			DECLARE AddressesUpdatedCursor CURSOR FOR
			SELECT Id, Line1, Line2, City, PostalCode, Line3, StateOrProvinceId, UserId, UserAddressTypeId, BillingOrganizationId, BillingOrganizationAddressTypeId, ServiceLocationId, ServiceLocationAddressTypeId, ContactId, ContactAddressTypeId, ExternalOrganizationId, ExternalOrganizationAddressTypeId, InsurerId, InsurerAddressTypeId, PatientId, PatientAddressTypeId, __EntityType__ FROM inserted
		
			OPEN AddressesUpdatedCursor
			FETCH NEXT FROM AddressesUpdatedCursor INTO @Id, @Line1, @Line2, @City, @PostalCode, @Line3, @StateOrProvinceId, @UserId, @UserAddressTypeId, @BillingOrganizationId, @BillingOrganizationAddressTypeId, @ServiceLocationId, @ServiceLocationAddressTypeId, @ContactId, @ContactAddressTypeId, @ExternalOrganizationId, @ExternalOrganizationAddressTypeId, @InsurerId, @InsurerAddressTypeId, @PatientId, @PatientAddressTypeId, @__EntityType__ 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- Address Update/Insert
				DECLARE @pair TABLE (
					x int
					,y int
					)
				DECLARE @x int
					,@y int
			
				SELECT @x = x
					,@y = y
				FROM model.GetBigIdPair(@Id, 110000000)
			
				---Patient Primary
				IF @x = 0
					BEGIN
						UPDATE PatientDemographics
						SET City = @City
							,Address = @Line1
							,Suite = @Line2
							,Zip = @PostalCode
							,STATE = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE PatientId = @y
					END
				---Patient 2nd Address
				IF @x = 1
					BEGIN
						UPDATE PatientDemoAlt
						SET City = @City
							,Address = @Line1
							,Suite = @Line2
							,Zip = @PostalCode
							,STATE = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE PatientId = @y
					END
				---Patient''s 3rd address
				IF @x = 2
					BEGIN
						UPDATE PatientDemoAlt
						SET City = @City
							,Address = @Line1
							,Suite = @Line2
							,Zip = @PostalCode
							,STATE = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE PatientId = @y
					END
				---Patient''s Work Address
				IF @x = 3
					BEGIN
						UPDATE PatientDemographics
						SET BusinessCity = @City
							,BusinessAddress = @Line1
							,BusinessSuite = @Line2
							,BusinessZip = @PostalCode
							,BusinessState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE PatientId = @y
					END
				---BillingOrganization, ServiceLocation -PracticeName Table
				IF @x IN (
						4
						,6
						, 17
						)
					BEGIN
						UPDATE PracticeName
						SET PracticeCity = @City
							,PracticeAddress = @Line1
							,PracticeSuite = @Line2
							,PracticeZip = @PostalCode
							,PracticeState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE PracticeId = @y
					END
				---BillingOrganization, ServiceLocation, User -Resources Table
				IF @x IN (
						5
						,7
						,8
						)
					BEGIN
					IF @x = 7 AND @ServiceLocationAddressTypeId = 7
						RAISERROR(''Cannot update resource table with main office'', 16, 1)
					ELSE
						UPDATE Resources
						SET ResourceCity = @City
							,ResourceAddress = @Line1
							,ResourceSuite = @Line2
							,ResourceZip = @PostalCode
							,ResourceState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE ResourceId = @y
					END
				---ExternalProvider -PracticeVendors
				IF @x IN (
						9
						,12
						)
					BEGIN
						UPDATE PracticeVendors
						SET VendorCity = @City
							,VendorAddress = @Line1
							,VendorSuite = @Line2
							,VendorZip = @PostalCode
							,VendorState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE VendorId = @y
					END
				---ExternalProvider Address 2 -PracticeVendors
				IF @x IN (
						10
						,13
						)
					BEGIN
						UPDATE PracticeVendors
						SET VendorCity = @City
							,VendorOtherOffice1 = @Line1
							,VendorSuite = @Line2
							,VendorZip = @PostalCode
							,VendorState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE VendorId = @y
					END
				---ExternalProvider Address 3 -PracticeVendors
				IF @x IN (
						11
						,14
						)
					BEGIN
						UPDATE PracticeVendors
						SET VendorCity = @City
							,VendorOtherOffice2 = @Line1
							,VendorSuite = @Line2
							,VendorZip = @PostalCode
							,VendorState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE VendorId = @y
					END
				---InsurerAddress -PracticeInsurers
				IF @x = 15
					BEGIN
						UPDATE PracticeInsurers
						SET InsurerCity = @City
							,Insureraddress = @Line1
							,InsurerZip = @PostalCode
							,InsurerState = (
								SELECT Abbreviation
								FROM model.StateOrProvinces
								WHERE Id = @StateOrProvinceId
								)
						WHERE InsurerId = @y
					END
				
				FETCH NEXT FROM AddressesUpdatedCursor INTO @Id, @Line1, @Line2, @City, @PostalCode, @Line3, @StateOrProvinceId, @UserId, @UserAddressTypeId, @BillingOrganizationId, @BillingOrganizationAddressTypeId, @ServiceLocationId, @ServiceLocationAddressTypeId, @ContactId, @ContactAddressTypeId, @ExternalOrganizationId, @ExternalOrganizationAddressTypeId, @InsurerId, @InsurerAddressTypeId, @PatientId, @PatientAddressTypeId, @__EntityType__ 
		
			END
		
			CLOSE AddressesUpdatedCursor
			DEALLOCATE AddressesUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.Insurers')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isInsurersChanged BIT

		SET @isInsurersChanged = 0

		DECLARE @InsurersViewSql NVARCHAR(max)

		SET @InsurersViewSql = 
			'
		CREATE VIEW [model].[Insurers]
		WITH SCHEMABINDING
		AS
		SELECT InsurerId AS Id
			,CASE 
				WHEN InsurerReferenceCode = ''P''
					THEN 1
				WHEN InsurerReferenceCode = ''K''
					THEN 2
				WHEN InsurerReferenceCode = ''X''
					THEN 4
				WHEN InsurerReferenceCode = ''F''
					THEN 5
				WHEN InsurerReferenceCode = ''G''
					THEN 6
				WHEN InsurerReferenceCode = ''I''
					THEN 7
				WHEN InsurerReferenceCode = ''Q''
					THEN 8
				WHEN InsurerReferenceCode = ''C''
					THEN 9
				WHEN InsurerReferenceCode = ''N''
					THEN 10
				WHEN InsurerReferenceCode = ''M''
					THEN 11
				WHEN InsurerReferenceCode = ''[''
					THEN 12
				WHEN InsurerReferenceCode = ''Z''
					THEN 13
				WHEN InsurerReferenceCode = ''Y''
					THEN 14
				WHEN InsurerReferenceCode = ''L''
					THEN 15
				WHEN InsurerReferenceCode = ''A''
					THEN 16
				WHEN InsurerReferenceCode = ''T''
					THEN 17
				WHEN InsurerReferenceCode = ''H''
					THEN 18
				WHEN InsurerReferenceCode = ''V''
					THEN 19
				WHEN InsurerReferenceCode = ''U''
					THEN 20
				WHEN InsurerReferenceCode = ''W''
					THEN 21
				ELSE 3
				END AS ClaimFormatTypeId
			,pctClaimReceiver.Id AS ClaimFileReceiverId
			,MedicareCrossOverId AS CrossOverCode
			,InsurerGroupName AS GroupName
			,CASE
				WHEN pctBusClass.Code = ''AMERIH''
					THEN 1
				WHEN pctBusClass.Code = ''BLUES''
					THEN 2
				WHEN pctBusClass.Code = ''COMM''
					THEN 3
				WHEN pctBusClass.Code = ''MCAIDFL''
					THEN 4
				WHEN pctBusClass.Code = ''MCAIDNC''
					THEN 5
				WHEN pctBusClass.Code = ''MCAIDNJ''
					THEN 6
				WHEN pctBusClass.Code = ''MCAIDNV''
					THEN 7
				WHEN pctBusClass.Code = ''MCAIDNY''
					THEN 8
				WHEN pctBusClass.Code = ''MCARENJ''
					THEN 9
				WHEN pctBusClass.Code = ''MCARENV''
					THEN 10
				WHEN pctBusClass.Code = ''MCARENY''
					THEN 11
				WHEN pctBusClass.Code = ''TEMPLATE''
					THEN 12
				WHEN pctBusClass.Code IS NULL
					THEN 3
				WHEN pctBusClass.Code = ''''
					THEN 3
				ELSE pctBusClass.Id + 1000
			END AS InsurerBusinessClassId
			,InsurerName AS [Name]
			,pctPayType.Id AS InsurerPayTypeId
			,pctPlanType.Id AS InsurerPlanTypeId
			,AllowDependents AS AllowDependents
			,CASE InsurerCrossOver
				WHEN ''Y''
					THEN CONVERT(BIT, 1)
				ELSE CONVERT(BIT, 0)
				END AS IsMedicareCrossOver
			,CASE ReferralRequired
				WHEN ''Y''
					THEN CONVERT(BIT, 1)
				ELSE CONVERT(BIT, 0)
				END AS IsReferralRequired
			,CASE Availability
				WHEN ''A''
					THEN CONVERT(BIT, 1)
				ELSE CONVERT(BIT, 0)
				END AS IsSecondaryClaimPaper
			,st.Id AS JurisdictionStateorProvinceId
			,NeicNumber AS PayerCode
			,InsurerPlanName AS PlanName
			,InsurerPlanType AS PlanType
			,InsurerPlanFormat AS PolicyNumberFormat
			,CASE InsurerServiceFilter
				WHEN ''N''
					THEN CONVERT(BIT, 0)
				ELSE CONVERT(BIT, 1)
				END AS SendZeroCharge,
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeInsurers pri
		LEFT JOIN dbo.PracticeCodeTable pctBusClass ON pctBusClass.Code = pri.InsurerBusinessClass
			AND pctBusClass.ReferenceType = ''BUSINESSCLASS''
		LEFT JOIN dbo.PracticeCodeTable pctClaimReceiver ON SUBSTRING(pctClaimReceiver.Code, 1, 1) = pri.InsurerTFormat
			AND pctClaimReceiver.ReferenceType = ''TRANSMITTYPE''
		LEFT JOIN dbo.PracticeCodeTable pctPayType ON SUBSTRING(pctPayType.Code, 1, 2) = pri.InsurerPayType
			AND pctPayType.ReferenceType = ''INSURERPTYPE''
		LEFT JOIN dbo.PracticeCodeTable pctPlanType ON pctPlanType.Code = pri.InsurerPlanType
			AND pctPlanType.ReferenceType = ''PLANTYPE''
		LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = pri.StateJurisdiction'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.Insurers'
			,@InsurersViewSql
			,@isChanged = @isInsurersChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerDelete'
				,
				'
			CREATE TRIGGER model.OnInsurerDelete ON [model].[Insurers] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(32)
			DECLARE @GroupName nvarchar(32)
			DECLARE @PlanName nvarchar(32)
			DECLARE @IsReferralRequired bit
			DECLARE @PayerCode nvarchar(max)
			DECLARE @CrossOverCode nvarchar(max)
			DECLARE @AllowDependents bit
			DECLARE @IsSecondaryClaimPaper bit
			DECLARE @PolicyNumberFormat nvarchar(max)
			DECLARE @JurisdictionStateOrProvinceId int
			DECLARE @SendZeroCharge bit
			DECLARE @InsurerPayTypeId int
			DECLARE @InsurerBusinessClassId int
			DECLARE @InsurerPlanTypeId int
			DECLARE @ClaimFormatTypeId int
			DECLARE @ClaimFileReceiverId int
			DECLARE @IsArchived bit
		
			DECLARE InsurersDeletedCursor CURSOR FOR
			SELECT Id, Name, GroupName, PlanName, IsReferralRequired, PayerCode, CrossOverCode, AllowDependents, IsSecondaryClaimPaper, PolicyNumberFormat, JurisdictionStateOrProvinceId, SendZeroCharge, InsurerPayTypeId, InsurerBusinessClassId, InsurerPlanTypeId, ClaimFormatTypeId, ClaimFileReceiverId, IsArchived FROM deleted
		
			OPEN InsurersDeletedCursor
			FETCH NEXT FROM InsurersDeletedCursor INTO @Id, @Name, @GroupName, @PlanName, @IsReferralRequired, @PayerCode, @CrossOverCode, @AllowDependents, @IsSecondaryClaimPaper, @PolicyNumberFormat, @JurisdictionStateOrProvinceId, @SendZeroCharge, @InsurerPayTypeId, @InsurerBusinessClassId, @InsurerPlanTypeId, @ClaimFormatTypeId, @ClaimFileReceiverId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insurer Delete
				DELETE
				FROM dbo.PracticeInsurers
				WHERE InsurerId = @Id
			
				SELECT @Id AS Id, @Name AS Name, @GroupName AS GroupName, @PlanName AS PlanName, @IsReferralRequired AS IsReferralRequired, @PayerCode AS PayerCode, @CrossOverCode AS CrossOverCode, @AllowDependents AS AllowDependents, @IsSecondaryClaimPaper AS IsSecondaryClaimPaper, @PolicyNumberFormat AS PolicyNumberFormat, @JurisdictionStateOrProvinceId AS JurisdictionStateOrProvinceId, @SendZeroCharge AS SendZeroCharge, @InsurerPayTypeId AS InsurerPayTypeId, @InsurerBusinessClassId AS InsurerBusinessClassId, @InsurerPlanTypeId AS InsurerPlanTypeId, @ClaimFormatTypeId AS ClaimFormatTypeId, @ClaimFileReceiverId AS ClaimFileReceiverId, @IsArchived AS IsArchived
			
				FETCH NEXT FROM InsurersDeletedCursor INTO @Id, @Name, @GroupName, @PlanName, @IsReferralRequired, @PayerCode, @CrossOverCode, @AllowDependents, @IsSecondaryClaimPaper, @PolicyNumberFormat, @JurisdictionStateOrProvinceId, @SendZeroCharge, @InsurerPayTypeId, @InsurerBusinessClassId, @InsurerPlanTypeId, @ClaimFormatTypeId, @ClaimFileReceiverId, @IsArchived 
		
			END
		
			CLOSE InsurersDeletedCursor
			DEALLOCATE InsurersDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerInsert'
				,
				'
			CREATE TRIGGER model.OnInsurerInsert ON [model].[Insurers] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(32)
			DECLARE @GroupName nvarchar(32)
			DECLARE @PlanName nvarchar(32)
			DECLARE @IsReferralRequired bit
			DECLARE @PayerCode nvarchar(max)
			DECLARE @CrossOverCode nvarchar(max)
			DECLARE @AllowDependents bit
			DECLARE @IsSecondaryClaimPaper bit
			DECLARE @PolicyNumberFormat nvarchar(max)
			DECLARE @JurisdictionStateOrProvinceId int
			DECLARE @SendZeroCharge bit
			DECLARE @InsurerPayTypeId int
			DECLARE @InsurerBusinessClassId int
			DECLARE @InsurerPlanTypeId int
			DECLARE @ClaimFormatTypeId int
			DECLARE @ClaimFileReceiverId int
			DECLARE @IsArchived bit
		
			DECLARE InsurersInsertedCursor CURSOR FOR
			SELECT Id, Name, GroupName, PlanName, IsReferralRequired, PayerCode, CrossOverCode, AllowDependents, IsSecondaryClaimPaper, PolicyNumberFormat, JurisdictionStateOrProvinceId, SendZeroCharge, InsurerPayTypeId, InsurerBusinessClassId, InsurerPlanTypeId, ClaimFormatTypeId, ClaimFileReceiverId, IsArchived FROM inserted
		
			OPEN InsurersInsertedCursor
			FETCH NEXT FROM InsurersInsertedCursor INTO @Id, @Name, @GroupName, @PlanName, @IsReferralRequired, @PayerCode, @CrossOverCode, @AllowDependents, @IsSecondaryClaimPaper, @PolicyNumberFormat, @JurisdictionStateOrProvinceId, @SendZeroCharge, @InsurerPayTypeId, @InsurerBusinessClassId, @InsurerPlanTypeId, @ClaimFormatTypeId, @ClaimFileReceiverId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 1 AS Value INTO #NoCheck
							
				-- Insurer Insert
				INSERT INTO dbo.PracticeInsurers (InsurerName,InsurerBusinessClass)
				VALUES ('''',''COMM'')
			
				SET @Id = IDENT_CURRENT(''PracticeInsurers'')
			
							
				-- Insurer Update/Insert
				DECLARE @InsurerReferenceCode nvarchar(2)
				DECLARE @InsurerEFormat nvarchar(2)
				DECLARE @InsurerTFormat nvarchar(2)
				DECLARE @StateJurisdiction nvarchar(2)
				DECLARE @NeicNumber nvarchar(64)
				DECLARE @MedicareCrossOverId nvarchar(32)
				DECLARE @InsurerGroupName nvarchar(64)
				DECLARE @InsurerBusinessClass nvarchar(16)
				DECLARE @InsurerName nvarchar(64)
				DECLARE @InsurerPlanName nvarchar(64)
				DECLARE @InsurerPlanType nvarchar(16)
				DECLARE @InsurerPlanFormat nvarchar(64)
				DECLARE @InsurerPayType nvarchar(4)
			
				SELECT @InsurerEFormat = InsurerEFormat,
					@InsurerTFormat = InsurerTFormat,
					@StateJurisdiction = StateJurisdiction,
					@NeicNumber = NeicNumber,
					@MedicareCrossOverId = MedicareCrossOverId,
					@InsurerGroupName = InsurerGroupName,
					@InsurerBusinessClass = InsurerBusinessClass,
					@InsurerName = InsurerName,
					@InsurerPlanName = InsurerPlanName,
					@InsurerPlanType = InsurerPlanType,
					@InsurerPlanFormat = InsurerPlanFormat,
					@InsurerPayType = InsurerPayType
				FROM dbo.PracticeInsurers
				WHERE InsurerId = @Id
			
			
				IF @ClaimFileReceiverId IS NOT NULL
				BEGIN
					SELECT TOP 1 @InsurerTFormat = SUBSTRING(Code, 1, 1)
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''TRANSMITTYPE''
						AND Id = @ClaimFileReceiverId
				END
			
				IF @CrossOverCode IS NOT NULL
				BEGIN
					SELECT @MedicareCrossOverId = @CrossOverCode
				END
			
				IF @GroupName IS NOT NULL
				BEGIN
					SELECT @InsurerGroupName = @GroupName
				END
			
				IF @InsurerBusinessClassId IS NOT NULL
				BEGIN
					SELECT @InsurerBusinessClass = Code
					FROM dbo.PracticeCodeTable 
					WHERE Id = CASE
						WHEN @InsurerBusinessClassId =  1
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''AMERIH'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  2
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''BLUES'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  4
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCAIDFL'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  5
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCAIDNC'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  6
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCAIDNJ'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  7
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCAIDNV'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  8
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCAIDNY'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  9
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCARENJ'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  10
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCARENV'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  11
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCARENY'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  12
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''TEMPLATE'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId > 1000
							THEN @InsurerBusinessClassId - 1000
						ELSE (SELECT Id FROM PracticeCodeTable WHERE Code = ''COMM'' and ReferenceType = ''BUSINESSCLASS'')
					END
				END
			
				IF @Name IS NOT NULL
				BEGIN
					SELECT @InsurerName = @Name
				END
			
				IF @InsurerPayTypeId IS NOT NULL
				BEGIN
					SELECT @InsurerPayType = SUBSTRING(Code, 1, 2)
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''INSURERPTYPE''
						AND Id = @InsurerPayTypeId
				END
			
				IF @InsurerPlanTypeId IS NOT NULL
				BEGIN
					SELECT @InsurerPlanType = Code
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLANTYPE''
						AND Id = @InsurerPlanTypeId
				END
			
				IF @JurisdictionStateorProvinceId IS NOT NULL
				BEGIN
					SELECT TOP 1 @StateJurisdiction = Abbreviation
					FROM model.StateOrProvinces
					WHERE Id = @JurisdictionStateorProvinceId
				END
			
				IF @PayerCode IS NOT NULL
				BEGIN
					SELECT @NeicNumber = @PayerCode
				END
			
				IF @PlanName IS NOT NULL
				BEGIN
					SELECT @InsurerPlanName = @PlanName
				END
			
				IF @PolicyNumberFormat IS NOT NULL
				BEGIN
					SELECT @InsurerPlanFormat = @PolicyNumberFormat
				END
			
				UPDATE dbo.PracticeInsurers
				SET InsurerReferenceCode = CASE @ClaimFormatTypeId
						WHEN 1
							THEN ''P''
						WHEN 2
							THEN ''A''
						WHEN 3
							THEN ''O''
						WHEN 4
							THEN ''X''
						WHEN 5
							THEN ''F''
						WHEN 6
							THEN ''G''
						WHEN 7
							THEN ''I''
						WHEN 8
							THEN ''Q''
						WHEN 9
							THEN ''X''
						WHEN 10
							THEN ''N''
						WHEN 11
							THEN ''M''
						WHEN 12
							THEN ''[''
						WHEN 13
							THEN ''Z''
						WHEN 14
							THEN ''Y''
						WHEN 15
							THEN ''L''
						WHEN 16
							THEN ''A''
						WHEN 17
							THEN ''T''
						WHEN 18
							THEN ''H''
						WHEN 19
							THEN ''V''
						WHEN 20
							THEN ''U''
						WHEN 21
							THEN ''W''
						ELSE ''O''
						END,
					InsurerEFormat = @InsurerEFormat,
					InsurerTFormat = @InsurerTFormat,
					MedicareCrossOverId = @MedicareCrossOverId,
					InsurerGroupName = @InsurerGroupName,
					InsurerBusinessClass = @InsurerBusinessClass,
					InsurerName = @InsurerName,
					InsurerPayType = @InsurerPayType,
					InsurerPlanType = @InsurerPlanType,
					OutOfPocket = CASE @AllowDependents
						WHEN 0
							THEN CONVERT(BIT, 1)
						ELSE CONVERT(BIT, 0)
						END,
					InsurerCrossOver = CASE @CrossOverCode
						WHEN ''1''
							THEN ''Y''
						ELSE ''''
						END,
					ReferralRequired = CASE @IsReferralRequired
						WHEN 1
							THEN ''Y''
						ELSE ''''
						END,
					Availability = CASE @IsSecondaryClaimPaper
						WHEN 1
							THEN ''A''
						ELSE ''''
						END,
					StateJurisdiction = @StateJurisdiction,
					NeicNumber = @NeicNumber,
					InsurerPlanName = @InsurerPlanName,
					InsurerPlanFormat = @InsurerPlanFormat,
					InsurerServiceFilter = CASE @SendZeroCharge
						WHEN 0
							THEN ''N''
						ELSE ''''
						END
				WHERE InsurerId = @Id
			
				DROP TABLE #NoCheck
			
			
				SELECT @Id AS Id, @Name AS Name, @GroupName AS GroupName, @PlanName AS PlanName, @IsReferralRequired AS IsReferralRequired, @PayerCode AS PayerCode, @CrossOverCode AS CrossOverCode, @AllowDependents AS AllowDependents, @IsSecondaryClaimPaper AS IsSecondaryClaimPaper, @PolicyNumberFormat AS PolicyNumberFormat, @JurisdictionStateOrProvinceId AS JurisdictionStateOrProvinceId, @SendZeroCharge AS SendZeroCharge, @InsurerPayTypeId AS InsurerPayTypeId, @InsurerBusinessClassId AS InsurerBusinessClassId, @InsurerPlanTypeId AS InsurerPlanTypeId, @ClaimFormatTypeId AS ClaimFormatTypeId, @ClaimFileReceiverId AS ClaimFileReceiverId, @IsArchived AS IsArchived
				
				FETCH NEXT FROM InsurersInsertedCursor INTO @Id, @Name, @GroupName, @PlanName, @IsReferralRequired, @PayerCode, @CrossOverCode, @AllowDependents, @IsSecondaryClaimPaper, @PolicyNumberFormat, @JurisdictionStateOrProvinceId, @SendZeroCharge, @InsurerPayTypeId, @InsurerBusinessClassId, @InsurerPlanTypeId, @ClaimFormatTypeId, @ClaimFileReceiverId, @IsArchived 
		
			END
		
			CLOSE InsurersInsertedCursor
			DEALLOCATE InsurersInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInsurerUpdate'
				,
				'
			CREATE TRIGGER model.OnInsurerUpdate ON [model].[Insurers] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @Name nvarchar(32)
			DECLARE @GroupName nvarchar(32)
			DECLARE @PlanName nvarchar(32)
			DECLARE @IsReferralRequired bit
			DECLARE @PayerCode nvarchar(max)
			DECLARE @CrossOverCode nvarchar(max)
			DECLARE @AllowDependents bit
			DECLARE @IsSecondaryClaimPaper bit
			DECLARE @PolicyNumberFormat nvarchar(max)
			DECLARE @JurisdictionStateOrProvinceId int
			DECLARE @SendZeroCharge bit
			DECLARE @InsurerPayTypeId int
			DECLARE @InsurerBusinessClassId int
			DECLARE @InsurerPlanTypeId int
			DECLARE @ClaimFormatTypeId int
			DECLARE @ClaimFileReceiverId int
			DECLARE @IsArchived bit
		
			DECLARE InsurersUpdatedCursor CURSOR FOR
			SELECT Id, Name, GroupName, PlanName, IsReferralRequired, PayerCode, CrossOverCode, AllowDependents, IsSecondaryClaimPaper, PolicyNumberFormat, JurisdictionStateOrProvinceId, SendZeroCharge, InsurerPayTypeId, InsurerBusinessClassId, InsurerPlanTypeId, ClaimFormatTypeId, ClaimFileReceiverId, IsArchived FROM inserted
		
			OPEN InsurersUpdatedCursor
			FETCH NEXT FROM InsurersUpdatedCursor INTO @Id, @Name, @GroupName, @PlanName, @IsReferralRequired, @PayerCode, @CrossOverCode, @AllowDependents, @IsSecondaryClaimPaper, @PolicyNumberFormat, @JurisdictionStateOrProvinceId, @SendZeroCharge, @InsurerPayTypeId, @InsurerBusinessClassId, @InsurerPlanTypeId, @ClaimFormatTypeId, @ClaimFileReceiverId, @IsArchived 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
							
				-- Insurer Update/Insert
				DECLARE @InsurerReferenceCode nvarchar(2)
				DECLARE @InsurerEFormat nvarchar(2)
				DECLARE @InsurerTFormat nvarchar(2)
				DECLARE @StateJurisdiction nvarchar(2)
				DECLARE @NeicNumber nvarchar(64)
				DECLARE @MedicareCrossOverId nvarchar(32)
				DECLARE @InsurerGroupName nvarchar(64)
				DECLARE @InsurerBusinessClass nvarchar(16)
				DECLARE @InsurerName nvarchar(64)
				DECLARE @InsurerPlanName nvarchar(64)
				DECLARE @InsurerPlanType nvarchar(16)
				DECLARE @InsurerPlanFormat nvarchar(64)
				DECLARE @InsurerPayType nvarchar(4)
			
				SELECT @InsurerEFormat = InsurerEFormat,
					@InsurerTFormat = InsurerTFormat,
					@StateJurisdiction = StateJurisdiction,
					@NeicNumber = NeicNumber,
					@MedicareCrossOverId = MedicareCrossOverId,
					@InsurerGroupName = InsurerGroupName,
					@InsurerBusinessClass = InsurerBusinessClass,
					@InsurerName = InsurerName,
					@InsurerPlanName = InsurerPlanName,
					@InsurerPlanType = InsurerPlanType,
					@InsurerPlanFormat = InsurerPlanFormat,
					@InsurerPayType = InsurerPayType
				FROM dbo.PracticeInsurers
				WHERE InsurerId = @Id
			
			
				IF @ClaimFileReceiverId IS NOT NULL
				BEGIN
					SELECT TOP 1 @InsurerTFormat = SUBSTRING(Code, 1, 1)
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''TRANSMITTYPE''
						AND Id = @ClaimFileReceiverId
				END
			
				IF @CrossOverCode IS NOT NULL
				BEGIN
					SELECT @MedicareCrossOverId = @CrossOverCode
				END
			
				IF @GroupName IS NOT NULL
				BEGIN
					SELECT @InsurerGroupName = @GroupName
				END
			
				IF @InsurerBusinessClassId IS NOT NULL
				BEGIN
					SELECT @InsurerBusinessClass = Code
					FROM dbo.PracticeCodeTable 
					WHERE Id = CASE
						WHEN @InsurerBusinessClassId =  1
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''AMERIH'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  2
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''BLUES'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  4
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCAIDFL'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  5
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCAIDNC'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  6
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCAIDNJ'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  7
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCAIDNV'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  8
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCAIDNY'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  9
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCARENJ'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  10
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCARENV'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  11
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''MCARENY'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId =  12
							THEN (SELECT Id FROM PracticeCodeTable WHERE Code = ''TEMPLATE'' and ReferenceType = ''BUSINESSCLASS'')
						WHEN @InsurerBusinessClassId > 1000
							THEN @InsurerBusinessClassId - 1000
						ELSE (SELECT Id FROM PracticeCodeTable WHERE Code = ''COMM'' and ReferenceType = ''BUSINESSCLASS'')
					END
				END
			
				IF @Name IS NOT NULL
				BEGIN
					SELECT @InsurerName = @Name
				END
			
				IF @InsurerPayTypeId IS NOT NULL
				BEGIN
					SELECT @InsurerPayType = SUBSTRING(Code, 1, 2)
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''INSURERPTYPE''
						AND Id = @InsurerPayTypeId
				END
			
				IF @InsurerPlanTypeId IS NOT NULL
				BEGIN
					SELECT @InsurerPlanType = Code
					FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLANTYPE''
						AND Id = @InsurerPlanTypeId
				END
			
				IF @JurisdictionStateorProvinceId IS NOT NULL
				BEGIN
					SELECT TOP 1 @StateJurisdiction = Abbreviation
					FROM model.StateOrProvinces
					WHERE Id = @JurisdictionStateorProvinceId
				END
			
				IF @PayerCode IS NOT NULL
				BEGIN
					SELECT @NeicNumber = @PayerCode
				END
			
				IF @PlanName IS NOT NULL
				BEGIN
					SELECT @InsurerPlanName = @PlanName
				END
			
				IF @PolicyNumberFormat IS NOT NULL
				BEGIN
					SELECT @InsurerPlanFormat = @PolicyNumberFormat
				END
			
				UPDATE dbo.PracticeInsurers
				SET InsurerReferenceCode = CASE @ClaimFormatTypeId
						WHEN 1
							THEN ''P''
						WHEN 2
							THEN ''A''
						WHEN 3
							THEN ''O''
						WHEN 4
							THEN ''X''
						WHEN 5
							THEN ''F''
						WHEN 6
							THEN ''G''
						WHEN 7
							THEN ''I''
						WHEN 8
							THEN ''Q''
						WHEN 9
							THEN ''X''
						WHEN 10
							THEN ''N''
						WHEN 11
							THEN ''M''
						WHEN 12
							THEN ''[''
						WHEN 13
							THEN ''Z''
						WHEN 14
							THEN ''Y''
						WHEN 15
							THEN ''L''
						WHEN 16
							THEN ''A''
						WHEN 17
							THEN ''T''
						WHEN 18
							THEN ''H''
						WHEN 19
							THEN ''V''
						WHEN 20
							THEN ''U''
						WHEN 21
							THEN ''W''
						ELSE ''O''
						END,
					InsurerEFormat = @InsurerEFormat,
					InsurerTFormat = @InsurerTFormat,
					MedicareCrossOverId = @MedicareCrossOverId,
					InsurerGroupName = @InsurerGroupName,
					InsurerBusinessClass = @InsurerBusinessClass,
					InsurerName = @InsurerName,
					InsurerPayType = @InsurerPayType,
					InsurerPlanType = @InsurerPlanType,
					OutOfPocket = CASE @AllowDependents
						WHEN 0
							THEN CONVERT(BIT, 1)
						ELSE CONVERT(BIT, 0)
						END,
					InsurerCrossOver = CASE @CrossOverCode
						WHEN ''1''
							THEN ''Y''
						ELSE ''''
						END,
					ReferralRequired = CASE @IsReferralRequired
						WHEN 1
							THEN ''Y''
						ELSE ''''
						END,
					Availability = CASE @IsSecondaryClaimPaper
						WHEN 1
							THEN ''A''
						ELSE ''''
						END,
					StateJurisdiction = @StateJurisdiction,
					NeicNumber = @NeicNumber,
					InsurerPlanName = @InsurerPlanName,
					InsurerPlanFormat = @InsurerPlanFormat,
					InsurerServiceFilter = CASE @SendZeroCharge
						WHEN 0
							THEN ''N''
						ELSE ''''
						END
				WHERE InsurerId = @Id
				
				FETCH NEXT FROM InsurersUpdatedCursor INTO @Id, @Name, @GroupName, @PlanName, @IsReferralRequired, @PayerCode, @CrossOverCode, @AllowDependents, @IsSecondaryClaimPaper, @PolicyNumberFormat, @JurisdictionStateOrProvinceId, @SendZeroCharge, @InsurerPayTypeId, @InsurerBusinessClassId, @InsurerPlanTypeId, @ClaimFormatTypeId, @ClaimFileReceiverId, @IsArchived 
		
			END
		
			CLOSE InsurersUpdatedCursor
			DEALLOCATE InsurersUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.InvoiceSupplementals')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isInvoiceSupplementalsChanged BIT

		SET @isInvoiceSupplementalsChanged = 0

		DECLARE @InvoiceSupplementalsViewSql NVARCHAR(max)

		SET @InvoiceSupplementalsViewSql = 
			'
		CREATE VIEW [model].[InvoiceSupplementals]
		WITH SCHEMABINDING
		AS
		SELECT pr.AppointmentId AS Id,
			CASE WHEN MAX(FirstConsDate) = '''' THEN NULL ELSE CONVERT(datetime,MAX(FirstConsDate), 101) END AS AccidentDateTime,
			CASE MAX(st.id)
				WHEN 0
					THEN NULL
				ELSE Max(st.id)
				END AS AccidentStateOrProvinceId,
			CASE WHEN MAX(HspAdmDate) = '''' THEN NULL ELSE CONVERT(datetime,MAX(HspAdmDate), 101) END AS AdmissionDateTime,
			CASE MAX(Over90)
				WHEN 1
					THEN 1
				WHEN 2
					THEN 2
				WHEN 3
					THEN 3
				WHEN 4
					THEN 4
				WHEN 5
					THEN 5
				WHEN 6
					THEN 6
				WHEN 7
					THEN 7
				WHEN 8
					THEN 8
				WHEN 9
					THEN 9
				WHEN 10
					THEN 10
				WHEN 11
					THEN 11
				WHEN 15
					THEN 12
				ELSE NULL
				END AS ClaimDelayReasonId,
			CASE 
				WHEN COALESCE(MAX(RsnDate), '''') <> ''''
					THEN CONVERT(datetime,MAX(RsnDate), 101) 
				ELSE NULL END AS DisabilityDateTime,
			CASE 
				WHEN COALESCE(MAX(HspDisDate), '''') <> ''''
					THEN CONVERT(datetime, MAX(HspDisDate), 101)
				ELSE NULL END AS DischargeDateTime,
			CASE MAX(AccType)
				WHEN ''A''
					THEN 1
				WHEN ''E''
					THEN 2
				WHEN ''O''
					THEN 3
				ELSE NULL
				END AS RelatedCause1Id,
			MAX(CONVERT(datetime, NULL, 101)) AS VisionPrescriptionDateTime,
			MAX(ReceivableId) AS ReceivableId
		FROM dbo.PatientReceivables pr 
		LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = pr.AccState
		WHERE (
				FirstConsDate <> ''''
				OR AccType <> ''''
				OR AccState <> ''''
				OR HspAdmDate <> ''''
				OR HspDisDate <> ''''
				OR Over90 <> ''''
				OR RsnDate <> ''''
				)
			AND (
				FirstConsDate IS NOT NULL
				OR AccType IS NOT NULL
				OR AccState IS NOT NULL
				OR HspAdmDate IS NOT NULL
				OR HspDisDate IS NOT NULL
				OR Over90 IS NOT NULL
				OR RsnDate IS NOT NULL
				)
		GROUP BY AppointmentId'

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.InvoiceSupplementals'
			,@InvoiceSupplementalsViewSql
			,@isChanged = @isInvoiceSupplementalsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInvoiceSupplementalDelete'
				,
				'
			CREATE TRIGGER model.OnInvoiceSupplementalDelete ON [model].[InvoiceSupplementals] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @AccidentDateTime datetime
			DECLARE @VisionPrescriptionDateTime datetime
			DECLARE @DisabilityDateTime datetime
			DECLARE @AdmissionDateTime datetime
			DECLARE @DischargeDateTime datetime
			DECLARE @ClaimDelayReasonId int
			DECLARE @RelatedCause1Id int
			DECLARE @AccidentStateOrProvinceId int
		
			DECLARE InvoiceSupplementalsDeletedCursor CURSOR FOR
			SELECT Id, AccidentDateTime, VisionPrescriptionDateTime, DisabilityDateTime, AdmissionDateTime, DischargeDateTime, ClaimDelayReasonId, RelatedCause1Id, AccidentStateOrProvinceId FROM deleted
		
			OPEN InvoiceSupplementalsDeletedCursor
			FETCH NEXT FROM InvoiceSupplementalsDeletedCursor INTO @Id, @AccidentDateTime, @VisionPrescriptionDateTime, @DisabilityDateTime, @AdmissionDateTime, @DischargeDateTime, @ClaimDelayReasonId, @RelatedCause1Id, @AccidentStateOrProvinceId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for InvoiceSupplemental --
				PRINT (''Not Implemented'')
			
				SELECT @Id AS Id, @AccidentDateTime AS AccidentDateTime, @VisionPrescriptionDateTime AS VisionPrescriptionDateTime, @DisabilityDateTime AS DisabilityDateTime, @AdmissionDateTime AS AdmissionDateTime, @DischargeDateTime AS DischargeDateTime, @ClaimDelayReasonId AS ClaimDelayReasonId, @RelatedCause1Id AS RelatedCause1Id, @AccidentStateOrProvinceId AS AccidentStateOrProvinceId
			
				FETCH NEXT FROM InvoiceSupplementalsDeletedCursor INTO @Id, @AccidentDateTime, @VisionPrescriptionDateTime, @DisabilityDateTime, @AdmissionDateTime, @DischargeDateTime, @ClaimDelayReasonId, @RelatedCause1Id, @AccidentStateOrProvinceId 
		
			END
		
			CLOSE InvoiceSupplementalsDeletedCursor
			DEALLOCATE InvoiceSupplementalsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInvoiceSupplementalInsert'
				,
				'
			CREATE TRIGGER model.OnInvoiceSupplementalInsert ON [model].[InvoiceSupplementals] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @AccidentDateTime datetime
			DECLARE @VisionPrescriptionDateTime datetime
			DECLARE @DisabilityDateTime datetime
			DECLARE @AdmissionDateTime datetime
			DECLARE @DischargeDateTime datetime
			DECLARE @ClaimDelayReasonId int
			DECLARE @RelatedCause1Id int
			DECLARE @AccidentStateOrProvinceId int
		
			DECLARE InvoiceSupplementalsInsertedCursor CURSOR FOR
			SELECT Id, AccidentDateTime, VisionPrescriptionDateTime, DisabilityDateTime, AdmissionDateTime, DischargeDateTime, ClaimDelayReasonId, RelatedCause1Id, AccidentStateOrProvinceId FROM inserted
		
			OPEN InvoiceSupplementalsInsertedCursor
			FETCH NEXT FROM InvoiceSupplementalsInsertedCursor INTO @Id, @AccidentDateTime, @VisionPrescriptionDateTime, @DisabilityDateTime, @AdmissionDateTime, @DischargeDateTime, @ClaimDelayReasonId, @RelatedCause1Id, @AccidentStateOrProvinceId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for InvoiceSupplemental --
				PRINT (''Not Implemented'')
			
				SET @Id = CONVERT(int, NULL)
			
				SELECT @Id AS Id, @AccidentDateTime AS AccidentDateTime, @VisionPrescriptionDateTime AS VisionPrescriptionDateTime, @DisabilityDateTime AS DisabilityDateTime, @AdmissionDateTime AS AdmissionDateTime, @DischargeDateTime AS DischargeDateTime, @ClaimDelayReasonId AS ClaimDelayReasonId, @RelatedCause1Id AS RelatedCause1Id, @AccidentStateOrProvinceId AS AccidentStateOrProvinceId
				
				FETCH NEXT FROM InvoiceSupplementalsInsertedCursor INTO @Id, @AccidentDateTime, @VisionPrescriptionDateTime, @DisabilityDateTime, @AdmissionDateTime, @DischargeDateTime, @ClaimDelayReasonId, @RelatedCause1Id, @AccidentStateOrProvinceId 
		
			END
		
			CLOSE InvoiceSupplementalsInsertedCursor
			DEALLOCATE InvoiceSupplementalsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnInvoiceSupplementalUpdate'
				,
				'
			CREATE TRIGGER model.OnInvoiceSupplementalUpdate ON [model].[InvoiceSupplementals] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @Id int
			DECLARE @AccidentDateTime datetime
			DECLARE @VisionPrescriptionDateTime datetime
			DECLARE @DisabilityDateTime datetime
			DECLARE @AdmissionDateTime datetime
			DECLARE @DischargeDateTime datetime
			DECLARE @ClaimDelayReasonId int
			DECLARE @RelatedCause1Id int
			DECLARE @AccidentStateOrProvinceId int
		
			DECLARE InvoiceSupplementalsUpdatedCursor CURSOR FOR
			SELECT Id, AccidentDateTime, VisionPrescriptionDateTime, DisabilityDateTime, AdmissionDateTime, DischargeDateTime, ClaimDelayReasonId, RelatedCause1Id, AccidentStateOrProvinceId FROM inserted
		
			OPEN InvoiceSupplementalsUpdatedCursor
			FETCH NEXT FROM InvoiceSupplementalsUpdatedCursor INTO @Id, @AccidentDateTime, @VisionPrescriptionDateTime, @DisabilityDateTime, @AdmissionDateTime, @DischargeDateTime, @ClaimDelayReasonId, @RelatedCause1Id, @AccidentStateOrProvinceId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for InvoiceSupplemental --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM InvoiceSupplementalsUpdatedCursor INTO @Id, @AccidentDateTime, @VisionPrescriptionDateTime, @DisabilityDateTime, @AdmissionDateTime, @DischargeDateTime, @ClaimDelayReasonId, @RelatedCause1Id, @AccidentStateOrProvinceId 
		
			END
		
			CLOSE InvoiceSupplementalsUpdatedCursor
			DEALLOCATE InvoiceSupplementalsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END

	IF NOT EXISTS (
			SELECT *
			FROM dbo.sysobjects
			WHERE id = OBJECT_ID(N'model.UserPermissions')
				AND OBJECTPROPERTY(id, N'IsUserTable') = 1
			)
	BEGIN
		DECLARE @isUserPermissionsChanged BIT

		SET @isUserPermissionsChanged = 0

		DECLARE @UserPermissionsViewSql NVARCHAR(max)

		SET @UserPermissionsViewSql = '
		CREATE VIEW [model].[UserPermissions_Internal]
		WITH SCHEMABINDING
		AS
		-- no index because this referneces another view.
		SELECT --(@UserPermissionMax * pe.Id + re.ResourceId) AS Id,
			CONVERT(bit, 0) AS IsDenied,
			pe.Id AS PermissionId,
			re.ResourceId AS UserId
		FROM dbo.Resources re
		INNER JOIN model.Permissions pe ON pe.Id > 0
		WHERE ResourceType <> ''R'''

		EXEC [dbo].[RecreateIfDefinitionChanged] 'model.UserPermissions_Internal'
			,@UserPermissionsViewSql
			,@isChanged = @isUserPermissionsChanged OUTPUT
			,@error = @rollback OUTPUT

		IF @rollback = 0
		BEGIN
			SET @UserPermissionsViewSql = '	
			CREATE VIEW [model].[UserPermissions]
			WITH SCHEMABINDING
			AS
			SELECT IsDenied, UserId, PermissionId FROM [model].[UserPermissions_Internal]'

			IF @createIndexes = 1
			BEGIN
				IF NOT EXISTS (
						SELECT *
						FROM sys.indexes
						WHERE NAME = 'PK_UserPermissions'
							AND object_id = OBJECT_ID('[model].[UserPermissions_Internal]')
						)
				BEGIN
					EXEC [dbo].[DropIndexes] '[model].[UserPermissions_Internal]'

					CREATE UNIQUE CLUSTERED INDEX [PK_UserPermissions] ON [model].[UserPermissions_Internal] (
						[UserId]
						,[PermissionId]
						);
				END

				SET @UserPermissionsViewSql = @UserPermissionsViewSql + ' WITH(NOEXPAND)'
			END

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.UserPermissions'
				,@UserPermissionsViewSql
				,@isChanged = @isUserPermissionsChanged OUTPUT
				,@error = @rollback OUTPUT
		END

		IF @rollback = 0
		BEGIN
			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnUserPermissionDelete'
				,'
			CREATE TRIGGER model.OnUserPermissionDelete ON [model].[UserPermissions] INSTEAD OF DELETE
			AS 
			BEGIN
		
			DECLARE @IsDenied bit
			DECLARE @UserId int
			DECLARE @PermissionId int
		
			DECLARE UserPermissionsDeletedCursor CURSOR FOR
			SELECT IsDenied, UserId, PermissionId FROM deleted
		
			OPEN UserPermissionsDeletedCursor
			FETCH NEXT FROM UserPermissionsDeletedCursor INTO @IsDenied, @UserId, @PermissionId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Delete SQL not found for UserPermission --
				PRINT (''Not Implemented'')
			
				SELECT @UserId AS UserId, @PermissionId AS PermissionId, @IsDenied AS IsDenied
			
				FETCH NEXT FROM UserPermissionsDeletedCursor INTO @IsDenied, @UserId, @PermissionId 
		
			END
		
			CLOSE UserPermissionsDeletedCursor
			DEALLOCATE UserPermissionsDeletedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnUserPermissionInsert'
				,'
			CREATE TRIGGER model.OnUserPermissionInsert ON [model].[UserPermissions] INSTEAD OF INSERT
			AS 
			BEGIN
		
			DECLARE @IsDenied bit
			DECLARE @UserId int
			DECLARE @PermissionId int
		
			DECLARE UserPermissionsInsertedCursor CURSOR FOR
			SELECT IsDenied, UserId, PermissionId FROM inserted
		
			OPEN UserPermissionsInsertedCursor
			FETCH NEXT FROM UserPermissionsInsertedCursor INTO @IsDenied, @UserId, @PermissionId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Insert SQL not found for UserPermission --
				PRINT (''Not Implemented'')
			
				SET @UserId = CONVERT(int, NULL)
				SET @PermissionId = CONVERT(int, NULL)
			
				SELECT @UserId AS UserId, @PermissionId AS PermissionId, @IsDenied AS IsDenied
				
				FETCH NEXT FROM UserPermissionsInsertedCursor INTO @IsDenied, @UserId, @PermissionId 
		
			END
		
			CLOSE UserPermissionsInsertedCursor
			DEALLOCATE UserPermissionsInsertedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT

			EXEC [dbo].[RecreateIfDefinitionChanged] 'model.OnUserPermissionUpdate'
				,'
			CREATE TRIGGER model.OnUserPermissionUpdate ON [model].[UserPermissions] INSTEAD OF UPDATE
			AS 
			BEGIN
		
			DECLARE @IsDenied bit
			DECLARE @UserId int
			DECLARE @PermissionId int
		
			DECLARE UserPermissionsUpdatedCursor CURSOR FOR
			SELECT IsDenied, UserId, PermissionId FROM inserted
		
			OPEN UserPermissionsUpdatedCursor
			FETCH NEXT FROM UserPermissionsUpdatedCursor INTO @IsDenied, @UserId, @PermissionId 
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				-- Update SQL not found for UserPermission --
				PRINT (''Not Implemented'')
				
				FETCH NEXT FROM UserPermissionsUpdatedCursor INTO @IsDenied, @UserId, @PermissionId 
		
			END
		
			CLOSE UserPermissionsUpdatedCursor
			DEALLOCATE UserPermissionsUpdatedCursor
		
			END
			'
				,0
				,@error = @rollback OUTPUT
		END
	END
END