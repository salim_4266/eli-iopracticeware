﻿IF ( EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
				WHERE TABLE_SCHEMA = 'Model'
				AND TABLE_NAME = 'OrganStructureGroups'))
BEGIN
	IF NOT EXISTS(SELECT Name FROM [Model].[OrganStructureGroups] WHERE Name ='FrontOfTheEye')
	INSERT INTO [Model].[OrganStructureGroups] (Name) VALUES ('FrontOfTheEye') 
	IF NOT EXISTS(SELECT Name FROM [Model].[OrganStructureGroups] WHERE Name ='BackOfTheEye')
	INSERT INTO [Model].[OrganStructureGroups] (Name) VALUES ('BackOfTheEye') 
End

--11630 : Import the data into temp table call OrganStructureGroupTemp$ and run the below query to generate the above query
--Select ('IF NOT EXISTS(SELECT Name FROM [Model].[OrganStructureGroups] WHERE Name ='''+ Name + ''')
--INSERT INTO [Model].[OrganStructureGroups] (Name) VALUES ('''+ CONVERT(nvarchar(max), Name)+''') ') 
--from [dbo].[OrganStructureGroupTemp$]