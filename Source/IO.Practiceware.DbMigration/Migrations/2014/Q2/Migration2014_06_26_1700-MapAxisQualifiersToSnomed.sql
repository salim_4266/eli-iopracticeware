﻿DECLARE @AxisQualifierId INT
SELECT @AxisQualifierId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'AxisQualifier'

DECLARE @SnomedId INT
SELECT @SnomedId = Id FROM model.ExternalSystems WHERE Name = 'SnomedCt'

DECLARE @SnomedAxisQualifier INT
SELECT @SnomedAxisQualifier = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @SnomedId AND Name = 'AxisQualifier'
	

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey) 
SELECT @AxisQualifierId, 1, @SnomedAxisQualifier, '260385009' UNION ALL
SELECT @AxisQualifierId, 2, @SnomedAxisQualifier, '77765009' UNION ALL
SELECT @AxisQualifierId, 3, @SnomedAxisQualifier, '392521001' UNION ALL
SELECT @AxisQualifierId, 4, @SnomedAxisQualifier, '237679004' 
