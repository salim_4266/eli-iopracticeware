﻿
IF EXISTS(SELECT OBJECT_ID (N'model.AuditTreatmentGoalsInsertTrigger'))
	EXEC dbo.DropObject 'model.AuditTreatmentGoalsInsertTrigger'
GO
IF EXISTS(SELECT OBJECT_ID (N'model.AuditTreatmentGoalsUpdateTrigger'))
	EXEC dbo.DropObject 'model.AuditTreatmentGoalsUpdateTrigger'
GO
IF EXISTS(SELECT OBJECT_ID (N'model.AuditTreatmentGoalsDeleteTrigger'))
	EXEC dbo.DropObject 'AuditTreatmentGoalsDeleteTrigger'
GO

IF EXISTS(SELECT * FROM sys.columns 
	WHERE  Object_ID = Object_ID(N'model.TreatmentGoals') AND name = 'Value')
	BEGIN
		ALTER TABLE model.TreatmentGoals
		DROP COLUMN Value
	END
GO

DECLARE @Constraint NVARCHAR(MAX)
SET @Constraint =  (SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
WHERE CONSTRAINT_TYPE = 'FOREIGN KEY' 
AND TABLE_NAME = 'TreatmentGoalAndInstructions')

IF EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'model' 
                 AND  TABLE_NAME = 'TreatmentGoalAndInstructions')
IF @Constraint IS NOT NULL 
	BEGIN
		-- Create and execute the command
		DECLARE @Cmd NVARCHAR(MAX)
		SELECT @Cmd = 'ALTER TABLE model.TreatmentGoalAndInstructions DROP CONSTRAINT '+@Constraint+''
		EXEC(@Cmd)
	END
GO

IF(SELECT COUNT(*) FROM model.TreatmentGoals) > 1
TRUNCATE TABLE model.TreatmentGoals
GO 

SET IDENTITY_INSERT model.TreatmentGoals ON
GO
IF NOT EXISTS (SELECT * FROM model.TreatmentGoals WHERE Id = 1 AND DisplayName = 'Maintain or improve patient condition.')
	BEGIN
		INSERT INTO model.TreatmentGoals (Id, DisplayName)
		SELECT 1 AS Id, 'Maintain or improve patient condition.' AS DisplayName
	END
SET IDENTITY_INSERT model.TreatmentGoals OFF
GO