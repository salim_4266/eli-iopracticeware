﻿SET NOCOUNT ON;
ALTER TABLE model.ExternalSystemEntityMappings DISABLE TRIGGER ALL;
ALTER TABLE model.ExternalSystemEntityMappings NOCHECK CONSTRAINT ALL;
ALTER INDEX ALL ON model.ExternalSystemEntityMappings
REBUILD WITH (FILLFACTOR = 80, SORT_IN_TEMPDB = ON,
              STATISTICS_NORECOMPUTE = ON);
GO
IF EXISTS(SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_NAME = 'Medications'
				 AND TABLE_SCHEMA = 'model')
BEGIN
DECLARE @RxNormExternalSystemId INT
SELECT @RxNormExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'RxNorm'

DECLARE @MedicationId INT
SELECT @MedicationId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'Medication'

DECLARE @RxNormMedication INT
SELECT @RxNormMedication = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @RxNormExternalSystemId
	AND Name = 'Medication'
	
CREATE TABLE #MedicationMappingToRxNormQueries (
	Id INT IDENTITY,
	Query nvarchar(MAX)
)

;WITH CTE AS (
	SELECT 
	ROW_NUMBER() OVER (PARTITION BY RXCUI,[STR],Suppressor,Name,Id
		ORDER BY RXCUI,[STR],Suppressor,Name,Id
		) AS rn
	,RXCUI 
	,[STR]
	,Suppressor
	,Name
	,Id
	FROM (
		SELECT 
		MAX(RXCUI) AS RXCUI
		,[STR]
		,MIN(CASE SUPPRESS
			WHEN 'N'
				THEN 1
			WHEN 'E'
				THEN 2
			WHEN 'Y'
				THEN 3
			WHEN 'O'
				THEN 4
		END) AS Suppressor
		,a.Name
		,a.Id
		FROM rxNorm.RXNCONSO b
		INNER JOIN model.Medications a ON a.Name = b.[STR]
		GROUP BY [STR]
		,a.Name
		,a.Id
	)v
)
INSERT INTO #MedicationMappingToRxNormQueries (Query)
SELECT 
'SELECT @MedicationId, '+CONVERT(NVARCHAR(MAX),c.Id)+', @RxNormMedication, '+RXCUI AS Query
FROM CTE c
INNER JOIN model.Medications a ON a.Id = c.Id
WHERE c.rn = 1
GROUP BY c.Id, RXCUI

DECLARE @Sql NVARCHAR(MAX)
SET @Sql = 'INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey) '
DECLARE @FullSql NVARCHAR(MAX)

DECLARE @MaxId INT
SET @MaxId = (SELECT MAX(Id) FROM #MedicationMappingToRxNormQueries)

DECLARE @Id INT
SET @Id = 1

WHILE (@Id <@MaxId)
	BEGIN
		SET @FullSql = @SQL + (SELECT Query FROM #MedicationMappingToRxNormQueries WHERE Id = @Id)
		EXEC sp_executesql @FullSql, N'@MedicationId INT, @RxNormMedication INT', @MedicationId, @RxNormMedication
		SET @Id = @Id + 1
	END

DROP TABLE #MedicationMappingToRxNormQueries

END

ALTER TABLE model.ExternalSystemEntityMappings CHECK CONSTRAINT ALL;
ALTER TABLE model.ExternalSystemEntityMappings ENABLE TRIGGER ALL;