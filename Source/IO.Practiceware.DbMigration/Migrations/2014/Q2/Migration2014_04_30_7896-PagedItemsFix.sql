--Found that if name too long SP doesn't run. So taking substring of name.

IF EXISTS(SELECT * FROM sys.objects WHERE name= 'usp_PagedItems')
BEGIN

	EXEC('
	
	ALTER PROCEDURE [dbo].[usp_PagedItems]
		(
		 @Page int,
		 @RecsPerPage int,
		 @TransType nvarchar(5),
		 @TransStatus nvarchar(5),
		 @Status nvarchar(50),
		 @Staff nvarchar(1000),
		 @Loc nvarchar(500),
		 @SDate nvarchar(8),
		 @EDate nvarchar(8)
		)
	AS

	SET NOCOUNT ON

	-- Create a temporary table
	CREATE TABLE #TempItems
	(
		ID int IDENTITY,
		Status nchar(1),
		TranDate nchar(10),
		PatName nchar(34),
		TranType nchar(64),
		Doctor nchar(16),
		RefDoc nchar(64),
		TranId int,
		PatId int,
		TransTypeId int
	)

	-- Insert the rows into temp table
	INSERT INTO #TempItems (Status, TranDate, PatName, TranType, Doctor, RefDoc, TranId, PatId, TransTypeId)
	SELECT case ptj.transactionbatch when '''' then ''N'' else ptj.transactionbatch end
	, substring(app.appdate,5,2)+''/''+substring(app.appdate,7,2)+''/''+substring(app.appdate,1,4)
	, SUBSTRING (case pd.middleinitial when '''' then LTrim(pd.firstname) + '' '' + LTrim(pd.lastname) else LTrim(pd.firstname) + '' '' + pd.middleinitial + '' '' + LTrim(pd.lastname) end, 1, 34)
	, ptj.transactionremark
	, SUBSTRING(r.resourcelastname, 1, 15)
	, SUBSTRING(pv.vendorname, 1, 63)
	, ptj.transactionid
	, pd.patientid
	, ptj.transactiontypeid
	FROM practicetransactionjournal ptj 
		inner join appointments app on ptj.transactiontypeid = app.appointmentid
		inner join patientdemographics pd on app.patientid = pd.patientid
		left join practicevendors pv on ptj.transactionrcvrid = pv.vendorid
		left join resources r on app.resourceid1 = r.resourceid
	WHERE ptj.transactiontype = @TransType and ptj.transactionstatus = @TransStatus
	and ptj.transactionbatch in (select nstr from CharTable(@Status, '',''))
	and ((app.appdate <= @SDate) or (@SDate = ''-''))
	and ((app.appdate >= @EDate) or (@EDate = ''-'')) 
	and app.resourceid1 in (select number from IntTable(@Staff))
	and app.resourceid2 in (select number from IntTable(@Loc))
	ORDER BY app.appdate, pd.lastname, ptj.transactionid

	-- Calculate record range for return.
	DECLARE @FirstRec int, @LastRec int
	SELECT @FirstRec = (@Page - 1) * @RecsPerPage
	SELECT @LastRec = (@Page * @RecsPerPage + 1)

	-- Add AllRecords on to the end so that we have a total rec count.
	SELECT *,
		   AllRecords = 
		(
		 SELECT COUNT(*) 
		 FROM #TempItems TI
		) 
	FROM #TempItems
	WHERE ID > @FirstRec AND ID < @LastRec

	-- Turn NOCOUNT back OFF
	SET NOCOUNT OFF')

END
GO