﻿IF EXISTS(SELECT * FROM sys.objects WHERE name = 'ViewIdentities')
BEGIN 
DROP TABLE dbo.ViewIdentities
END
GO
 
CREATE TABLE [dbo].[ViewIdentities](
[Name] [nvarchar](max) NULL,
[Value] [bigint] NULL,
[Id] [int] IDENTITY(1,1) NOT NULL,
CONSTRAINT [PK_ViewIdentities] PRIMARY KEY CLUSTERED ([Id] ASC))
GO
 
IF EXISTS(SELECT * FROM sys.objects WHERE name = 'GetIdentCurrent')
BEGIN 
DROP FUNCTION dbo.GetIdentCurrent
END
GO
 
CREATE FUNCTION GetIdentCurrent(@name nvarchar(max))
RETURNS bigint
AS
BEGIN
RETURN (SELECT COALESCE(IDENT_CURRENT(@name), (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = @name)))
END
GO
