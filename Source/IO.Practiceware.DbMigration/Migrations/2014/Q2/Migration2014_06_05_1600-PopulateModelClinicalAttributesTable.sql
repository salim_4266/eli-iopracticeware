﻿If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'FINDING SITE')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('FINDING SITE', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'ASSOCIATE MORPHOLOGY')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('ASSOCIATE MORPHOLOGY', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'ASSOCIATED WITH')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('ASSOCIATED WITH', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'CAUSATIVE AGENT')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('CAUSATIVE AGENT', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'DUE TO')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('DUE TO', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'AFTER')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('AFTER', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'SEVERITY')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('SEVERITY', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'CLINICAL COURSE')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('CLINICAL COURSE', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'EPISODICITY')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('EPISODICITY', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'INTERPRETS')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('INTERPRETS', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'HAS INTERPRETATION')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('HAS INTERPRETATION', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'PATHOLOGICAL PROCESS')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('PATHOLOGICAL PROCESS', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'HAS DEFINITIONAL MANIFESTATION')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('HAS DEFINITIONAL MANIFESTATION', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'OCCURRENCE')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('OCCURRENCE', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'FINDING METHOD')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('FINDING METHOD', 0, 1);
GO

If NOT EXISTS(SELECT * FROM model.ClinicalAttributes WHERE Name = 'FINDING INFORMER')
	INSERT INTO model.ClinicalAttributes (Name, IsDeactivated, OrdinalId) VALUES ('FINDING INFORMER', 0, 1);
GO
