﻿DECLARE @TreatmentGoalPREId INT
SELECT @TreatmentGoalPREId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'TreatmentGoal'

DECLARE @SnomedId INT
SELECT @SnomedId = Id FROM model.ExternalSystems WHERE Name = 'SnomedCt'

DECLARE @SnowmedTreatmentGoal INT
SELECT @SnowmedTreatmentGoal = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @SnomedId
	AND Name = 'TreatmentGoal'
SELECT @TreatmentGoalPREId, @SnomedId, @SnowmedTreatmentGoal

--DECLARE @ExternalEntityKey INT
--SELECT @ExternalEntityKey = Id FROM snomed.Description WHERE term = 'treatment intent' AND conceptId = 395077000

IF NOT EXISTS (
SELECT * FROM model.ExternalSystemEntityMappings esem 
WHERE esem.ExternalSystemEntityId = @SnowmedTreatmentGoal 
AND esem.ExternalSystemEntityKey = '395077000'
AND esem.PracticeRepositoryEntityId = @TreatmentGoalPREId
AND esem.PracticeRepositoryEntityKey = 1)
BEGIN
	INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
	VALUES (@TreatmentGoalPREId, 1, @SnowmedTreatmentGoal, '395077000')
END