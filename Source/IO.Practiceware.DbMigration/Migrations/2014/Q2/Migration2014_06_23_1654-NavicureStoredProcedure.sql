﻿/****** Object:  StoredProcedure [dbo].[NavicureBatchEligibility]    Script Date: 6/23/2014 4:56:23 PM ******/
IF EXISTS(SELECT * FROM sys.objects WHERE Name = 'NavicureBatchEligibility')
BEGIN
	DROP PROCEDURE [dbo].[NavicureBatchEligibility]
END
GO

/****** Object:  StoredProcedure [dbo].[NavicureBatchEligibility]    Script Date: 6/23/2014 4:56:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NavicureBatchEligibility] 
	@rundate nvarchar(8)
AS
BEGIN

SET NOCOUNT ON;

--exec navicurebatcheligibility 20140522

CREATE TABLE #tempLocTable
( LocationId Int, LocationDescription nvarchar(250))
insert into #tempLocTable

select practiceid + 1000 as LocationId, PracticeCity as LocationDescription
from practicename 
where practicetype = 'P'
--and practiceid > 1
and LocationReference <> ''

insert into #tempLocTable
select resourceid as LocationId, resourcedescription as LocationDescription
from resources where resourcetype = 'R'
and servicecode = 02
--and locationreference <>
--and resourceid < 100

insert into #tempLocTable
select 0 as LocationId, PracticeCity as LocationDescription
from practicename
where LocationReference = ''
and BillingOffice = 'T'

select loc.LocationDescription, em.EligibilityID,re.NPI,inspol.PolicyCode,
SSN = CASE 
	WHEN sub.SocialSecurityNumber IS NULL 
	THEN '|'
	ELSE sub.SocialSecurityNumber+'|'
	END,
CASE 
	WHEN sub.FirstName IS NULL 
	THEN ''
	ELSE sub.FirstName
	END as FirstName,
CASE 
	WHEN sub.MiddleName IS NULL 
	THEN ''
	ELSE sub.MiddleName
	END as MiddleName,
CASE 
	WHEN sub.LastName IS NULL 
	THEN ''
	ELSE sub.LastName
	END as LastName,
SUBSTRING(subge.Name,0,1),
CONVERT(VARCHAR, sub.DateOfBirth, 112),
relationship = CASE pti.PolicyHolderRelationshipTypeId
		WHEN '1'
			THEN '18'
		WHEN '2'
			THEN '01'
		WHEN '3'	
			THEN '19'
		ELSE 'G8'
		END,
soc = CASE 
		WHEN pd.Id <> inspol.PolicyHolderPatientId
			AND pd.SocialSecurityNumber IS NOT NULL
			THEN pd.SocialSecurityNumber+'|'
		WHEN pd.Id <> inspol.PolicyHolderPatientId AND pd.SocialSecurityNumber IS NULL	
			THEN '|'
			else '|'
		END,
	CASE 
		WHEN pd.Id <> inspol.PolicyHolderPatientId
			THEN pd.FirstName
			else ''
		END,
	CASE 
		WHEN pd.Id <> inspol.PolicyHolderPatientId
			THEN pd.MiddleName
			else ''
		END,
	CASE 
		WHEN pd.Id <> inspol.PolicyHolderPatientId
			THEN pd.LastName
			else ''
		END,
	CASE 
		WHEN pd.Id <> inspol.PolicyHolderPatientId
			THEN SUBSTRING(ge.Name,0,1)
			else ''
		END,
	CASE 
		WHEN pd.Id <> inspol.PolicyHolderPatientId
			THEN CONVERT(VARCHAR, pd.DateOfBirth, 112)
			else ''
		END,+'|'+ap.AppDate+'||'
from Appointments ap
inner join model.Patients pd on pd.Id=ap.PatientId
inner join model.Gender_Enumeration ge on ge.Id = pd.GenderId
inner join Resources re on re.ResourceId=ap.ResourceId1
inner join model.PatientInsurances pti on pti.InsuredPatientId = pd.Id
inner join model.InsurancePolicies inspol on inspol.Id = pti.InsurancePolicyId
inner join model.Insurers ins on ins.Id = inspol.InsurerId
inner join EligibilityMapping em on em.NEICNumber=ISNULL(ins.PayerCode,'')
inner join model.Patients sub on inspol.PolicyHolderPatientId = sub.Id
inner join model.Gender_Enumeration subge on subge.Id = sub.GenderId
inner join #tempLocTable loc on loc.LocationId=ap.ResourceId2
where (ap.AppDate = @rundate)
--and inspol.InsurerId in ('283','988','266','267')
and ap.ScheduleStatus in ('p','r')
and pti.IsDeleted <> 1
and (inspol.StartDateTime <= CONVERT(datetime,@rundate))
and (pti.EndDateTime >= CONVERT(datetime,@rundate) or pti.EndDateTime is null)
and (ins.PayerCode <> '' OR ins.PayerCode IS NOT NULL)
and re.NPI not in ('') 
and (inspol.PolicyCode <> '' OR inspol.PolicyCode IS NOT NULL)
and (pd.FirstName <> '' OR  pd.FirstName IS NOT NULL)
and (pd.LastName <> '' OR pd.LastName IS NOT NULL)
order by sub.LastName, sub.FirstName, sub.DateOfBirth

drop table #tempLocTable
END


GO