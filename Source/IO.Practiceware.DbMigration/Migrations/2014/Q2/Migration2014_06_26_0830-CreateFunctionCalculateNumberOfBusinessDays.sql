﻿IF OBJECT_ID('dbo.CalculateNumberOfBusinessDays') IS NOT NULL
	DROP FUNCTION [dbo].[CalculateNumberOfBusinessDays]
GO

CREATE FUNCTION [dbo].[CalculateNumberOfBusinessDays] (@StartDate datetime, @EndDate datetime)
RETURNS int
AS
BEGIN
     
     SET @StartDate = DATEADD(dd, DATEDIFF(dd, 0, @StartDate), 0) 
     SET @EndDate = DATEADD(dd, DATEDIFF(dd, 0, @EndDate), 0) 
          
     DECLARE @BUSINESSDAYS INT
     SELECT @BUSINESSDAYS = (DATEDIFF(dd, @StartDate, @EndDate) + 1)
						   -(DATEDIFF(wk, @StartDate, @EndDate) * 2)
   						   -(CASE WHEN DATENAME(dw, @StartDate) = 'Sunday' THEN 1 ELSE 0 END)
						   -(CASE WHEN DATENAME(dw, @EndDate) = 'Saturday' THEN 1 ELSE 0 END)
  
     RETURN @BUSINESSDAYS
END
GO


--model.USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess
IF (OBJECT_ID('model.USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess
END
GO
--exec [model].[USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess] '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	IF @ResourceIds LIKE '%,%'
		RAISERROR (
				'Please ONLY send one doctor id at a TIME.'
				,16
				,2
				)

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	--Numerator    
	SELECT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpre ON esmpre.PracticeRepositoryEntityKey = d.PatientId
	INNER JOIN model.ExternalSystemMessages esm ON esm.id = esmpre.ExternalSystemMessageId
	WHERE ExternalSystemExternalSystemMessageTypeId IN (
			SELECT id
			FROM model.ExternalSystemExternalSystemMessageTypes
			WHERE ExternalSystemId IN (
					SELECT Id
					FROM model.ExternalSystems
					WHERE NAME = 'Omedix'
					)
				AND ExternalSystemMessageTypeId IN (
					SELECT id
					FROM model.ExternalSystemMessageTypes
					WHERE NAME = 'PatientTransitionOfCareCCDA'
						AND IsOutbound = 1
					)
			)
		AND [dbo].[CalculateNumberOfBusinessDays](d.EncounterDate, [IO.Practiceware.SqlClr].ConvertToClientDateTime([IO.Practiceware.SqlClr].ConvertToLocalDateTime(esm.CreatedDateTime))) <= 4

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_ClinicalSummary
IF (OBJECT_ID('model.USP_CMS_Stage2_ClinicalSummary', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_ClinicalSummary
END
GO
--exec [model].[USP_CMS_Stage2_ClinicalSummary] '10/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage2_ClinicalSummary] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	IF @ResourceIds LIKE '%,%'
		RAISERROR (
				'Please select only one provider at a TIME.'
				,16
				,2
				)

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;
	DECLARE @Status NVARCHAR(1)

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	--Numerator 
	----1.  Is autoprinting on?  If so, 100%
	--SELECT @Status=FieldValue 
	--FROM PracticeInterfaceConfiguration 
	--WHERE FieldReference = 'PRINT'
	--SELECT @TotParm = COUNT(DISTINCT EncounterId)
	--FROM #Denominator
	--IF @Status='T'  
	--	BEGIN		  
	--		SELECT @CountParm = @TotParm
	--	END 
	--ELSE 
	--	BEGIN
	-- Patient Declines
	SELECT d.*
		,CONVERT(NVARCHAR(100), ClinicalId) AS ClinicalSummaryId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientClinical pc ON pc.AppointmentId = d.EncounterId
	WHERE ClinicalType = 'A'
		AND FindingDetail LIKE 'CLIN SUMMARY, PATIENT DECLINES%'
	
	UNION ALL
	
	-- Clinical Data Files
	-- portal, print, or save as
	SELECT d.*
		,CONVERT(NVARCHAR(100), esm.Id) AS ClinicalSummaryId
	FROM #Denominator d
	INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpre ON esmpre.PracticeRepositoryEntityKey = d.EncounterId
	INNER JOIN model.ExternalSystemMessages esm ON esm.id = esmpre.ExternalSystemMessageId
	INNER JOIN model.ExternalSystemExternalSystemMessageTypes esesmt ON esesmt.id = esm.ExternalSystemExternalSystemMessageTypeId
	INNER JOIN model.ExternalSystemMessageTypes esmt ON esmt.Id = esesmt.ExternalSystemMessageTypeId
		AND esmt.NAME = 'EncounterClinicalSummaryCCDA'
		AND IsOutbound = 1
	INNER JOIN model.Appointments ap ON ap.Id = esmpre.PracticeRepositoryEntityKey
		AND [dbo].[CalculateNumberOfBusinessDays](ap.DATETIME, [IO.Practiceware.SqlClr].ConvertToClientDateTime([IO.Practiceware.SqlClr].ConvertToLocalDateTime(esm.CreatedDateTime))) <= 1
	
	UNION ALL
	
	-- 7.2 letters
	SELECT d.*
		,CONVERT(NVARCHAR(100), lep.Id) AS ClinicalSummaryId
	FROM #Denominator d
	INNER JOIN LogEntryProperties LEP ON LEP.Value = d.EncounterId
	INNER JOIN LogEntries LE ON LE.Id = LEP.LogEntryId
		AND LEP.NAME = 'AppointmentId'
	WHERE Message LIKE 'Printed misc ltr%Clin%Sum%'
		AND [dbo].[CalculateNumberOfBusinessDays](d.EncounterDate, [IO.Practiceware.SqlClr].ConvertToClientDateTime([IO.Practiceware.SqlClr].ConvertToLocalDateTime(le.DATETIME))) <= 1

	SET @CountParm = (
			SELECT COUNT(DISTINCT EncounterId)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT EncounterId)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.EncounterId
		,n.EncounterDate
		,n.ClinicalSummaryId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.EncounterDate
		,n.EncounterId

	DROP TABLE #Numerator

	DROP TABLE #Denominator
END
GO


--model.USP_CMS_Stage1_ClinicalSummary
IF (OBJECT_ID('model.USP_CMS_Stage1_ClinicalSummary', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage1_ClinicalSummary
END
GO
--exec [model].[USP_CMS_Stage1_ClinicalSummary] '10/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage1_ClinicalSummary] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	IF @ResourceIds LIKE '%,%'
		RAISERROR (
				'Please select only one provider at a TIME.'
				,16
				,2
				)

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;
	DECLARE @Status NVARCHAR(1)

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	--Numerator 
	----1.  Is autoprinting on?  If so, 100%
	--SELECT @Status=FieldValue 
	--FROM PracticeInterfaceConfiguration 
	--WHERE FieldReference = 'PRINT'
	--SELECT @TotParm = COUNT(DISTINCT EncounterId)
	--FROM #Denominator
	--IF @Status='T'  
	--	BEGIN		  
	--		SELECT @CountParm = @TotParm
	--	END 
	--ELSE 
	--	BEGIN
	-- Patient Declines
	SELECT d.*
		,CONVERT(NVARCHAR(100), ClinicalId) AS ClinicalSummaryId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientClinical pc ON pc.AppointmentId = d.EncounterId
	WHERE ClinicalType = 'A'
		AND FindingDetail LIKE 'CLIN SUMMARY, PATIENT DECLINES%'
	
	UNION ALL
	
	-- Clinical Data Files
	-- portal, print, or save as
	SELECT d.*
		,CONVERT(NVARCHAR(100), esm.Id) AS ClinicalSummaryId
	FROM #Denominator d
	INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpre ON esmpre.PracticeRepositoryEntityKey = d.EncounterId
	INNER JOIN model.ExternalSystemMessages esm ON esm.id = esmpre.ExternalSystemMessageId
	INNER JOIN model.ExternalSystemExternalSystemMessageTypes esesmt ON esesmt.id = esm.ExternalSystemExternalSystemMessageTypeId
	INNER JOIN model.ExternalSystemMessageTypes esmt ON esmt.Id = esesmt.ExternalSystemMessageTypeId
		AND esmt.NAME = 'EncounterClinicalSummaryCCDA'
		AND IsOutbound = 1
	INNER JOIN model.Appointments ap ON ap.Id = esmpre.PracticeRepositoryEntityKey
		AND [dbo].[CalculateNumberOfBusinessDays](ap.DATETIME, [IO.Practiceware.SqlClr].ConvertToClientDateTime([IO.Practiceware.SqlClr].ConvertToLocalDateTime(esm.CreatedDateTime))) <= 3
	
	UNION ALL
	
	-- 7.2 letters
	SELECT d.*
		,CONVERT(NVARCHAR(100), lep.Id) AS ClinicalSummaryId
	FROM #Denominator d
	INNER JOIN LogEntryProperties LEP ON LEP.Value = d.EncounterId
	INNER JOIN LogEntries LE ON LE.Id = LEP.LogEntryId
		AND LEP.NAME = 'AppointmentId'
	WHERE Message LIKE 'Printed misc ltr%Clin%Sum%'
		AND [dbo].[CalculateNumberOfBusinessDays](d.EncounterDate, [IO.Practiceware.SqlClr].ConvertToClientDateTime([IO.Practiceware.SqlClr].ConvertToLocalDateTime(le.DATETIME))) <= 3

	SET @CountParm = (
			SELECT COUNT(DISTINCT EncounterId)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT EncounterId)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.EncounterId
		,n.EncounterDate
		,n.ClinicalSummaryId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.EncounterDate
		,n.EncounterId

	DROP TABLE #Numerator

	DROP TABLE #Denominator
END
GO