﻿IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[model].[CK_ScheduleBlockAppointmentCategories_NonCancelledUniqueAppointmentId]') AND parent_object_id = OBJECT_ID(N'[model].[ScheduleBlockAppointmentCategories]'))
ALTER TABLE [model].[ScheduleBlockAppointmentCategories] DROP CONSTRAINT [CK_ScheduleBlockAppointmentCategories_NonCancelledUniqueAppointmentId]
GO

/****** Object:  Index [IX_FK_ScheduleBlockScheduleBlockAppointmentCategory]    Script Date: 5/7/2014 12:33:11 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[model].[ScheduleBlockAppointmentCategories]') AND name = N'IX_FK_AppointmentScheduleBlockAppointmentCategory')
CREATE NONCLUSTERED INDEX [IX_FK_AppointmentScheduleBlockAppointmentCategory] ON [model].[ScheduleBlockAppointmentCategories]
(
	[AppointmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[IsAppointmentCancelledOrNotUnique]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
ALTER FUNCTION [model].[IsAppointmentCancelledOrNotUnique](@appointmentId int)
RETURNS bit
AS
BEGIN
	IF @appointmentId IS NULL
		RETURN 0

	DECLARE @returnValue AS bit
	SET @returnValue = 
			(SELECT CASE 
				WHEN EXISTS(SELECT AppointmentId FROM model.ScheduleBlockAppointmentCategories 
					WHERE AppointmentId IS NOT NULL AND AppointmentId = @appointmentId
					GROUP BY AppointmentId
					HAVING COUNT(*) > 1)
					THEN 1
				ELSE CAST(0 AS bit)
				END FROM model.Appointments a
				WHERE a.Id = @appointmentId)

	IF (@returnValue = 1)
		RETURN 1

	SET @returnValue = 
			(SELECT CASE WHEN EXISTS(
				SELECT * FROM model.Encounters e
				WHERE EncounterStatusId >=9 AND EncounterStatusId <= 15
				AND e.Id = a.EncounterId) THEN 1 ELSE 0 END
			FROM model.Appointments a 
			WHERE a.Id = @appointmentId)

	RETURN @returnValue
END
' 
END


IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[model].[CK_ScheduleBlockAppointmentCategories_NonCancelledUniqueAppointmentId]') AND parent_object_id = OBJECT_ID(N'[model].[ScheduleBlockAppointmentCategories]'))
ALTER TABLE [model].[ScheduleBlockAppointmentCategories]  WITH NOCHECK ADD  CONSTRAINT [CK_ScheduleBlockAppointmentCategories_NonCancelledUniqueAppointmentId] CHECK  (([AppointmentId] IS NULL OR [model].[IsAppointmentCancelledOrNotUnique]([AppointmentId])=(0)))
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[model].[CK_ScheduleBlockAppointmentCategories_NonCancelledUniqueAppointmentId]') AND parent_object_id = OBJECT_ID(N'[model].[ScheduleBlockAppointmentCategories]'))
ALTER TABLE [model].[ScheduleBlockAppointmentCategories] CHECK CONSTRAINT [CK_ScheduleBlockAppointmentCategories_NonCancelledUniqueAppointmentId]
GO
