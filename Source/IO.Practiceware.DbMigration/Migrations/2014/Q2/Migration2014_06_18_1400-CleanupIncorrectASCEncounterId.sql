﻿--TFS 11434

;WITH CTE AS (
SELECT 
ap.PatientId, ap.AppDate
FROM dbo.Appointments ap
JOIN dbo.AppointmentType at ON at.AppTypeId = ap.AppTypeId
	AND at.ResourceId8 = 1
GROUP BY ap.PatientId, ap.AppDate, ap.AppTypeId
	HAVING COUNT(ap.AppTypeId) > 1
)
,CTE2 AS (
SELECT 
ap.PatientId
,ap.AppDate
,ap.Comments
,ap.AppointmentId
,ap.EncounterId
,ROW_NUMBER() OVER (PARTITION BY ap.PatientId ORDER BY ap.AppointmentId) AS Num
,DENSE_RANK() OVER (ORDER BY ap.PatientId) AS Num2
FROM CTE c
JOIN dbo.Appointments ap ON ap.PatientId = c.PatientId
	AND ap.AppDate = c.AppDate
WHERE ap.ScheduleStatus = 'D'
)
SELECT 
CTE2.PatientId
,CTE2.AppointmentId
,CTE2.EncounterId
,CTE2.AppDate
,MAX(pa.AppointmentId) AS AppointmentId2
INTO #CTE3
FROM CTE2
JOIN dbo.Appointments ap ON ap.PatientId = CTE2.PatientId
	AND ap.AppDate = CTE2.AppDate
	AND ap.Comments <> 'ASC CLAIM'
JOIN dbo.AppointmentType at ON at.AppTypeId = ap.AppTypeId
	AND at.ResourceId8 = 1
JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
WHERE Num > 2
AND CTE2.AppointmentId <> CTE2.EncounterId
	AND CTE2.EncounterId <> (CTE2.AppointmentId - 1)
GROUP BY 
CTE2.Num2
,CTE2.PatientId
,CTE2.AppointmentId
,CTE2.EncounterId
,CTE2.AppDate

DECLARE @sqlDisable NVARCHAR(MAX)
DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlDisable = ''
SET @sqlEnable = ''

SELECT 
@sqlDisable = @sqlDisable + 'ALTER TABLE [dbo].[Appointments] DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID('dbo.Appointments') 
	AND name NOT LIKE 'Audit%' 
	AND is_disabled = 0
	
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

EXEC sp_executesql @sqlDisable

UPDATE a
SET a.EncounterId = #CTE3.AppointmentId2
FROM dbo.Appointments a
JOIN #CTE3 ON #CTE3.AppointmentId = a.AppointmentId

EXEC sp_executesql @sqlEnable

UPDATE inv
SET LegacyPatientReceivablesInvoice = ap.InvoiceNumber
FROM model.Invoices inv
JOIN Appointments ap ON inv.EncounterId = ap.EncounterId
JOIN #CTE3 ON #CTE3.AppointmentId = ap.AppointmentId
WHERE ap.AppointmentId = ap.EncounterId 
	AND LegacyPatientReceivablesInvoice IS NULL

UPDATE inv
SET LegacyPatientReceivablesAppointmentId = ap.AppointmentId
FROM model.Invoices inv
JOIN Appointments ap ON inv.EncounterId = ap.EncounterId
JOIN #CTE3 ON #CTE3.AppointmentId = ap.AppointmentId
WHERE ap.AppointmentId = ap.EncounterId 
	AND LegacyPatientReceivablesAppointmentId IS NULL

UPDATE inv
SET LegacyPatientReceivablesPatientId = ap.PatientId
FROM model.Invoices inv
JOIN Appointments ap ON inv.EncounterId = ap.EncounterId
JOIN #CTE3 ON #CTE3.AppointmentId = ap.AppointmentId
WHERE ap.AppointmentId = ap.EncounterId 
	AND LegacyPatientReceivablesPatientId IS NULL