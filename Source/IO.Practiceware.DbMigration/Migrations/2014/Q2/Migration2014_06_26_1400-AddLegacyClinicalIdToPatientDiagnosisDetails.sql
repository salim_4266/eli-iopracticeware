﻿IF NOT EXISTS (SELECT TOP 1 * FROM sys.columns WHERE object_id = OBJECT_ID('model.PatientDiagnosisDetails','U') AND name = 'LegacyClinicalId')
	BEGIN
		ALTER TABLE model.PatientDiagnosisDetails ADD [LegacyClinicalId] INT NULL
	END
GO