﻿--Dropping these triggers to improve speed of DI checkout.
IF EXISTS (SELECT * FROM sys.triggers WHERE Name = '[AuditPatientClinicalDeleteTrigger]') 
	DROP TRIGGER [dbo].[AuditPatientClinicalDeleteTrigger];
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE Name = '[AuditPatientClinicalInsertTrigger]') 
	DROP TRIGGER [dbo].[AuditPatientClinicalInsertTrigger];
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE Name = '[AuditPatientClinicalUpdateTrigger]') 
	DROP TRIGGER [dbo].[AuditPatientClinicalUpdateTrigger];
GO