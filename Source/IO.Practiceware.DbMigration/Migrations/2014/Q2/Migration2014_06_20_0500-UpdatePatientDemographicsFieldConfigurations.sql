﻿/* Making Signature Date field IsRequiredconfigurable to false and also IsRequired to false for Patient Demographics setup.
The reason behind this is : Signature Date field's Required option is depending on ReleaseMedicalInformation selected value 'Yes'*/

IF EXISTS (SELECT * FROM model.PatientDemographicsFieldConfigurations WHERE Id = 27)
BEGIN
	UPDATE model.PatientDemographicsFieldConfigurations SET IsRequiredConfigurable = 0, IsRequired = 0  WHERE Id = 27
END
GO
