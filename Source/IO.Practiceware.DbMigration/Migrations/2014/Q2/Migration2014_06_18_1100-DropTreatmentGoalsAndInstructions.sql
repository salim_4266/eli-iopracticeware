﻿IF EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'model' 
                 AND  TABLE_NAME = 'TreatmentPlanTreatmentGoal')
	IF EXISTS (SELECT * 
    FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='FK_TreatmentPlanTreatmentGoal_TreatmentGoal')
		ALTER TABLE model.TreatmentPlanTreatmentGoal DROP CONSTRAINT FK_TreatmentPlanTreatmentGoal_TreatmentGoal
GO

IF EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'model' 
                 AND  TABLE_NAME = 'TreatmentGoalClinicalCondition')
	IF EXISTS (SELECT * 
    FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='FK_TreatmentGoalClinicalCondition_TreatmentGoal')
		ALTER TABLE model.TreatmentGoalClinicalCondition DROP CONSTRAINT FK_TreatmentGoalClinicalCondition_TreatmentGoal
GO

IF EXISTS(SELECT OBJECT_ID (N'model.TreatmentGoalAndInstructions'))
	EXEC dbo.DropObject 'model.TreatmentGoalAndInstructions'
GO


