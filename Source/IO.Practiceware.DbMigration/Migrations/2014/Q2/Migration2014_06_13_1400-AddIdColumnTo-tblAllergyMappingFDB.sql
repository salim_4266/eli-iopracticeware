﻿-- Get the constraint name
DECLARE @Constraint NVARCHAR(MAX)
SET @Constraint =  (SELECT TOP 1 CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
WHERE TABLE_NAME ='tblAllergyMappingFDB' AND CONSTRAINT_TYPE = 'PRIMARY KEY')

IF EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'fdb_rxnorm_mapping' 
                 AND  TABLE_NAME = 'tblAllergyMappingFDB')
IF @Constraint IS NOT NULL 
	BEGIN
		-- Create and execute the command
		DECLARE @Cmd NVARCHAR(MAX)
		SELECT @Cmd = 'ALTER TABLE fdb_rxnorm_mapping.tblAllergyMappingFDB DROP CONSTRAINT '+@Constraint+''
		EXEC(@Cmd)
	END
GO

IF EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'fdb_rxnorm_mapping' 
                 AND  TABLE_NAME = 'tblAllergyMappingFDB')
IF EXISTS(SELECT * FROM sys.columns 
	WHERE  Object_ID = Object_ID(N'fdb_rxnorm_mapping.tblAllergyMappingFDB') AND name = 'MappedId')
	BEGIN
		--Alter the MappedId column so it can be null
		ALTER TABLE fdb_rxnorm_mapping.tblAllergyMappingFDB
		ALTER COLUMN MappedId INT NULL
	END
GO

IF EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'fdb_rxnorm_mapping' 
                 AND  TABLE_NAME = 'tblAllergyMappingFDB')
IF NOT EXISTS(SELECT * FROM sys.columns 
WHERE  Object_ID = Object_ID(N'fdb_rxnorm_mapping.tblAllergyMappingFDB') AND name = 'Id')
	BEGIN
		ALTER TABLE fdb_rxnorm_mapping.tblAllergyMappingFDB
		ADD Id INT IDENTITY
	END

GO

IF EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'fdb_rxnorm_mapping' 
                 AND  TABLE_NAME = 'tblAllergyMappingFDB')
IF ((SELECT is_nullable FROM sys.columns 
WHERE  Object_ID = Object_ID(N'fdb_rxnorm_mapping.tblAllergyMappingFDB') AND name = 'CompositeAllergyId') = 1)
	BEGIN

		ALTER TABLE fdb_rxnorm_mapping.tblAllergyMappingFDB
		ALTER COLUMN CompositeAllergyId INT NOT NULL

	END
GO


IF EXISTS(
SELECT * 
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' 
AND TABLE_NAME = 'tblAllergyMappingFDB' )

BEGIN 
DECLARE @sql nvarchar(max)
	SELECT @sql = 'ALTER TABLE ['  + CONSTRAINT_SCHEMA + '].[' + TABLE_NAME + '] DROP CONSTRAINT ' + CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
	WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' 
		AND TABLE_NAME = 'tblAllergyMappingFDB'

EXEC sp_executesql @sql
END

GO


IF EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'fdb_rxnorm_mapping' 
                 AND  TABLE_NAME = 'tblAllergyMappingFDB')
	BEGIN
		-- Add the new composite key
		ALTER TABLE fdb_rxnorm_mapping.tblAllergyMappingFDB
		ADD CONSTRAINT PK_tblAllergyMappingFDB PRIMARY KEY (Id, CompositeAllergyId)
	END
GO