﻿DECLARE @PatientRelationshipTypeId_Enumeration TABLE (Id INT, Name NVARCHAR(MAX))

INSERT INTO @PatientRelationshipTypeId_Enumeration (Id, Name)
SELECT 1 AS Id, 'Brother' AS Name UNION ALL
SELECT 2 AS Id, 'Sister' AS Name UNION ALL
SELECT 3 AS Id, 'Mother' AS Name UNION ALL
SELECT 4 AS Id, 'Father' AS Name UNION ALL
SELECT 5 AS Id, 'Spouse' AS Name UNION ALL
SELECT 6 AS Id, 'Guardian' AS Name UNION ALL
SELECT 7 AS Id, 'Unknown' AS Name UNION ALL
SELECT 8 AS Id, 'Self' AS Name

INSERT INTO model.PatientRelationships (FromPatientId,ToPatientId,PatientRelationshipDescription)
SELECT 
[pi].InsuredPatientId AS FromPatientId
,ip.PolicyHolderPatientId AS ToPatientId
,e.Name AS PatientRelationshipDescription
FROM model.PatientInsurances [pi]
JOIN model.InsurancePolicies ip ON [pi].InsurancePolicyId = ip.Id
LEFT JOIN @PatientRelationshipTypeId_Enumeration e ON e.Id = [pi].PolicyHolderRelationshipTypeId
WHERE ip.PolicyHolderPatientId <> [pi].InsuredPatientId
	AND [pi].IsDeleted = 0
GROUP BY 
ip.PolicyHolderPatientId
,[pi].InsuredPatientId
,e.Name

UNION ALL

SELECT 
ip.PolicyHolderPatientId AS FromPatientId
,[pi].InsuredPatientId AS ToPatientId
,e.Name AS PatientRelationshipDescription
FROM model.PatientInsurances [pi]
JOIN model.InsurancePolicies ip ON [pi].InsurancePolicyId = ip.Id
LEFT JOIN @PatientRelationshipTypeId_Enumeration e ON e.Id = [pi].PolicyHolderRelationshipTypeId
WHERE ip.PolicyHolderPatientId <> [pi].InsuredPatientId
	AND [pi].IsDeleted = 0
	AND [pi].InsuredPatientId NOT IN (
		SELECT 
		ip.PolicyHolderPatientId AS FromPatientId
		FROM model.PatientInsurances [pi]
		JOIN model.InsurancePolicies ip ON [pi].InsurancePolicyId = ip.Id
		WHERE ip.PolicyHolderPatientId <> [pi].InsuredPatientId
		AND [pi].IsDeleted = 0
		GROUP BY 
		ip.PolicyHolderPatientId
	)
GROUP BY 
[pi].InsuredPatientId
,ip.PolicyHolderPatientId
,e.Name

EXCEPT

SELECT p.FromPatientId
,p.ToPatientId
,p.PatientRelationshipDescription
FROM model.PatientRelationships p