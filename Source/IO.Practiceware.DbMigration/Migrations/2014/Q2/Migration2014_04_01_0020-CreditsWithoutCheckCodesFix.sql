﻿IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit
GO

SELECT 1 AS Value INTO #NoAudit
GO

ALTER TABLE model.FinancialBatches
ADD TemporaryAdjustmentId BIGINT NULL

;WITH CTE AS (
SELECT
adj.FinancialSourceTypeId
,adj.PatientId
,adj.InsurerId
,adj.PostedDateTime
,adj.Amount
,adj.Id
,CASE 
	WHEN adj.PatientId IS NOT NULL
		THEN CONVERT(NVARCHAR(MAX),adj.PatientId)+'-'+CONVERT(NVARCHAR(8),adj.PostedDateTime,112)
	WHEN adj.InsurerId IS NOT NULL
		THEN CONVERT(NVARCHAR(5),p.InsurerName)+'-'+CONVERT(NVARCHAR(8),adj.PostedDateTime,112)
END AS CheckCode
FROM model.Adjustments adj
LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
LEFT JOIN model.PaymentMethods paym ON paym.Id = adj.PaymentMethodId
LEFT JOIN dbo.PracticeCodeTable pctRefType ON pctRefType.Id = paym.Id
	AND pctRefType.ReferenceType = 'PAYABLETYPE'
LEFT JOIN dbo.PracticeInsurers p ON p.InsurerId = adj.InsurerId
WHERE adj.InvoiceReceivableId IS NULL
AND SUBSTRING(pctRefType.Code, (dbo.GetMax((CHARINDEX(' - ', pctRefType.Code)+3), 0)), LEN(pctRefType.Code)) NOT IN ('C')
AND fb.Id IS NULL
)
SELECT
FinancialSourceTypeId
,PatientId
,InsurerId
,PostedDateTime AS PaymentDateTime
,CONVERT(DATETIME,NULL) AS ExplanationOfBenefitsDateTime
,Amount
,CheckCode
,Id AS TemporaryAdjustmentId
INTO #tempFinancialBatches
FROM CTE

INSERT INTO [model].[FinancialBatches]
	([FinancialSourceTypeId]
	,[PatientId]
	,[InsurerId]
	,[PaymentDateTime]
	,[ExplanationOfBenefitsDateTime]
	,[Amount]
	,[CheckCode]
	,[TemporaryAdjustmentId])
SELECT
FinancialSourceTypeId
,PatientId
,InsurerId
,PaymentDateTime
,ExplanationOfBenefitsDateTime
,Amount
,CheckCode
,TemporaryAdjustmentId
FROM #tempFinancialBatches
GO

UPDATE adj
SET adj.FinancialBatchId = fb.Id
FROM model.Adjustments adj
INNER JOIN model.FinancialBatches fb ON fb.TemporaryAdjustmentId = adj.Id

ALTER TABLE model.FinancialBatches DROP COLUMN TemporaryAdjustmentId

GO