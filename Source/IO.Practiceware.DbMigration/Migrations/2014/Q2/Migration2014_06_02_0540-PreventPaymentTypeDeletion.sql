﻿--Bug #11392.

--On PracticeCode table to prevent deletion of payment types in use
IF OBJECT_ID('PreventPaymentTypeDeletion') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PreventPaymentTypeDeletion')
END

GO

CREATE TRIGGER PreventPaymentTypeDeletion
ON PracticeCodeTable
INSTEAD OF DELETE
NOT FOR REPLICATION
AS 
	SET NOCOUNT ON;
	IF EXISTS (
		SELECT d.Id
		FROM deleted d  
		INNER JOIN PatientReceivablePayments prp WITH(NOLOCK) 
			ON SUBSTRING(Code, LEN(Code), 1) = Prp.PaymentType
		WHERE ReferenceType ='PaymentType'
			)	
		BEGIN
			RAISERROR ('Cannot delete payment types that are in use.',16,1)
			ROLLBACK TRANSACTION
		END
	ELSE
		BEGIN
			DELETE dbo.PracticeCodeTable WHERE Id IN(SELECT Id FROM deleted)
		END
GO