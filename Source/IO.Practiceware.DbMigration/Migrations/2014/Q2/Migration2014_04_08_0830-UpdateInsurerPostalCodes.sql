﻿-- Remove '-' from insurer zip codes
UPDATE PracticeInsurers SET InsurerZip = REPLACE(InsurerZip, '-', '') WHERE LEN(InsurerZip) = 10
