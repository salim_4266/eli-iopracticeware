﻿-- Remove '-', '(', ')', 'X' from ExchangeAndSuffix of model.InsurerPhoneNumbers
UPDATE model.InsurerPhoneNumbers SET ExchangeAndSuffix = REPLACE(ExchangeAndSuffix, '-', '') WHERE LEN(ExchangeAndSuffix) > 10 AND ExchangeAndSuffix LIKE '%-%' AND ISNULL(AreaCode,'') = '';
UPDATE model.InsurerPhoneNumbers SET ExchangeAndSuffix = REPLACE(ExchangeAndSuffix, '(', '') WHERE LEN(ExchangeAndSuffix) > 10 AND ExchangeAndSuffix LIKE '%(%' AND ISNULL(AreaCode,'') = '';
UPDATE model.InsurerPhoneNumbers SET ExchangeAndSuffix = REPLACE(ExchangeAndSuffix, ')', '') WHERE LEN(ExchangeAndSuffix) > 10 AND ExchangeAndSuffix LIKE '%)%' AND ISNULL(AreaCode,'') = '';
UPDATE model.InsurerPhoneNumbers SET ExchangeAndSuffix = REPLACE(ExchangeAndSuffix, 'X', '') WHERE LEN(ExchangeAndSuffix) > 10 AND ExchangeAndSuffix LIKE '%X%' AND ISNULL(AreaCode,'') = '';

-- Update AreaCode, ExchangeAndSuffix and Extension if length of ExchangeAndSuffix >=10 and AreaCode is null
UPDATE model.InsurerPhoneNumbers SET AreaCode = SUBSTRING(ExchangeAndSuffix, 1,3), ExchangeAndSuffix = SUBSTRING(ExchangeAndSuffix, 4,7), Extension = SUBSTRING(ExchangeAndSuffix, 11,2) WHERE LEN(ExchangeAndSuffix) = 12 AND ISNULL(AreaCode,'') = '';
UPDATE model.InsurerPhoneNumbers SET AreaCode = SUBSTRING(ExchangeAndSuffix, 1,3), ExchangeAndSuffix = SUBSTRING(ExchangeAndSuffix, 4,7) WHERE LEN(ExchangeAndSuffix) = 10 AND ISNULL(AreaCode,'') = '';
