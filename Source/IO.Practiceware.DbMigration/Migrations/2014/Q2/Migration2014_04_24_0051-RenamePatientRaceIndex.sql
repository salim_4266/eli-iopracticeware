﻿IF EXISTS (SELECT * FROM sys.indexes where Name = '[PK_PatientRace]')
BEGIN
	ALTER TABLE [model].[PatientRace] DROP CONSTRAINT [[PK_PatientRace]]]
	
	ALTER TABLE [model].[PatientRace] ADD  CONSTRAINT PK_PatientRace PRIMARY KEY NONCLUSTERED 
	(
		[Patients_Id] ASC,
		[Races_Id] ASC
	)
END
GO