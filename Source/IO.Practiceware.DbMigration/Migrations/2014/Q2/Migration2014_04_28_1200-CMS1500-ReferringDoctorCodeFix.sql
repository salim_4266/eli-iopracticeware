﻿-- if ReferringDoctorFullName is null we don't want to include the code DN
-- replacing the first line with the second

IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	
	UPDATE model.TemplateDocuments 	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 
	'<input type="text" id="ReferringDoctorFullName" value="@(Model.ReferringDoctorNpi == Model.RenderingProviderNpi ? "DK    " + Model.ReferringDoctorFullName : "DN     " + Model.ReferringDoctorFullName)"',
	'<input type="text" id="ReferringDoctorFullName" value="@(string.IsNullOrEmpty(Model.ReferringDoctorFullName) ? "" : Model.ReferringDoctorNpi == Model.RenderingProviderNpi ? "DK    " + Model.ReferringDoctorFullName : "DN     " + Model.ReferringDoctorFullName)"'))
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL	
END

