﻿SET IDENTITY_INSERT [model].[AxisQualifiers] ON

-- Confirmation of Negative Findings
IF NOT EXISTS (SELECT Id FROM [model].[AxisQualifiers] WHERE [Id] = 1)
	INSERT INTO [model].[AxisQualifiers] ([Id], [Name], [OrdinalId], [IsDeactivated]) 
	VALUES (1,'Confirmation of Negative Findings', 1, 0);
ELSE
	UPDATE [model].[AxisQualifiers]
	   SET [Name] = 'Confirmation of Negative Findings'
		  ,[OrdinalId] = 1
		  ,[IsDeactivated] = 0
	 WHERE [Id] = 1

-- Status Post Surgery
IF NOT EXISTS (SELECT Id FROM [model].[AxisQualifiers] WHERE [Id] = 2)
	INSERT INTO [model].[AxisQualifiers] ([Id], [Name], [OrdinalId], [IsDeactivated]) 
	VALUES (2,'Status Post Surgery', 1, 0);
ELSE
	UPDATE [model].[AxisQualifiers]
	   SET [Name] = 'Status Post Surgery'
		  ,[OrdinalId] = 1
		  ,[IsDeactivated] = 0
	 WHERE [Id] = 2

-- History of
IF NOT EXISTS (SELECT Id FROM [model].[AxisQualifiers] WHERE [Id] = 3)
	INSERT INTO [model].[AxisQualifiers] ([Id], [Name], [OrdinalId], [IsDeactivated]) 
	VALUES (3,'History of', 1, 0);
ELSE
	UPDATE [model].[AxisQualifiers]
	   SET [Name] = 'History of'
		  ,[OrdinalId] = 1
		  ,[IsDeactivated] = 0
	 WHERE [Id] = 3

-- Rule out
IF NOT EXISTS (SELECT Id FROM [model].[AxisQualifiers] WHERE [Id] = 4)
	INSERT INTO [model].[AxisQualifiers] ([Id], [Name], [OrdinalId], [IsDeactivated]) 
	VALUES (4,'Rule out', 1, 0);
ELSE
	UPDATE [model].[AxisQualifiers]
	   SET [Name] = 'Rule out'
		  ,[OrdinalId] = 1
		  ,[IsDeactivated] = 0
	 WHERE [Id] = 4

SET IDENTITY_INSERT [model].[AxisQualifiers] OFF