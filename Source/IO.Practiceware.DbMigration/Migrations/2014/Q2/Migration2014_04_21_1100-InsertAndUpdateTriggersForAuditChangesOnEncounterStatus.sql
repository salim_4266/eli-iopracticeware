﻿IF EXISTS(SELECT * FROM sys.Triggers WHERE Name = 'AuditEncountersStatusInsertTrigger')
	DROP TRIGGER AuditEncountersStatusInsertTrigger
GO



CREATE TRIGGER [dbo].[AuditEncountersStatusInsertTrigger] ON [dbo].[PracticeActivity]
AFTER INSERT
NOT FOR REPLICATION AS
BEGIN
	SET NOCOUNT ON;
	
	-- The following trigger inserts new entries into dbo.AuditEntries when there is a new row with a Status in 
	-- dbo.PracticeActivity
	IF [dbo].[GetNoAudit]() = 0
	BEGIN
		DECLARE @__userIdContext NVARCHAR(max)

		IF OBJECT_ID('tempdb..#UserContext') IS NOT NULL
		BEGIN
			SELECT @__userIdContext = UserId
			FROM #UserContext
		END

		DECLARE @__userId INT

		IF (ISNUMERIC(@__userIdContext) = 1)
		BEGIN
			SELECT @__userId = CONVERT(INT, @__userIdContext)
		END

		DECLARE @__auditDate DATETIME

		SET @__auditDate = GETUTCDATE()

		DECLARE @__hostName NVARCHAR(500)

		BEGIN TRY
			EXEC sp_executesql N'SELECT @__hostName = HOST_NAME()'
				,N'@__hostName nvarchar(500) OUTPUT'
				,@__hostName = @__hostName OUTPUT
		END TRY

		BEGIN CATCH
			SET @__hostName = CURRENT_USER
		END CATCH

		SELECT NEWID() AS Id
			,'model.Encounters' AS ObjectName
			,0 AS ChangeTypeId
			,@__auditDate AS AuditDateTime
			,@__hostName AS HostName
			,@__userId AS UserId
			,CONVERT(NVARCHAR, ServerProperty('ServerName')) AS ServerName
			,'[AppointmentId]' AS KeyNames
			,CAST('[' + CAST([AppointmentId] AS NVARCHAR(max)) + ']' AS NVARCHAR(400)) AS KeyValues
			,PatientId AS PatientId
			,AppointmentId AS AppointmentId
		INTO #__AuditEntries
		FROM (
		SELECT i.[AppointmentId]
			,i.[PatientId]
			,COALESCE(CASE i.[Status]
						WHEN 'W'
							THEN 2
						WHEN 'M'
							THEN 3
						WHEN 'E'
							THEN 4
						WHEN 'G'
							THEN 5
						WHEN 'H'
							THEN 6
						WHEN 'U'
							THEN 15
						END
			, 14) AS EncounterStatusId
		FROM [inserted] i 
					
		EXCEPT
					
		SELECT d.[AppointmentId]
			,d.[PatientId]
			,COALESCE(CASE d.[Status]
						WHEN 'W'
							THEN 2
						WHEN 'M'
							THEN 3
						WHEN 'E'
							THEN 4
						WHEN 'G'
							THEN 5
						WHEN 'H'
							THEN 6
						WHEN 'U'
							THEN 15
						END
			, 14) AS EncounterStatusId
		FROM deleted d
			) AS changedRows

		INSERT INTO dbo.AuditEntries (
			Id
			,ObjectName
			,ChangeTypeId
			,AuditDateTime
			,HostName
			,UserId
			,ServerName
			,KeyNames
			,KeyValues
			,PatientId
			,AppointmentId
			)
		SELECT *
		FROM #__AuditEntries
		
	END

	SET NOCOUNT OFF
END
GO


IF EXISTS(SELECT * FROM sys.Triggers WHERE Name = 'AuditEncountersStatusUpdateTrigger')
	DROP TRIGGER AuditEncountersStatusUpdateTrigger
GO
CREATE TRIGGER [dbo].[AuditEncountersStatusUpdateTrigger] ON [dbo].[PracticeActivity]
AFTER UPDATE 
NOT FOR REPLICATION AS
BEGIN
	SET NOCOUNT ON;
	
	-- The following trigger inserts new entries into dbo.AuditEntryChanges when there is an updated row with a changed Status in 
	-- dbo.PracticeActivity
	IF [dbo].[GetNoAudit]() = 0
	BEGIN
		DECLARE @__userIdContext NVARCHAR(max)

		IF OBJECT_ID('tempdb..#UserContext') IS NOT NULL
		BEGIN
			SELECT @__userIdContext = UserId
			FROM #UserContext
		END

		DECLARE @__userId INT

		IF (ISNUMERIC(@__userIdContext) = 1)
		BEGIN
			SELECT @__userId = CONVERT(INT, @__userIdContext)
		END

		DECLARE @__auditDate DATETIME

		SET @__auditDate = GETUTCDATE()

		DECLARE @__hostName NVARCHAR(500)

		BEGIN TRY
			EXEC sp_executesql N'SELECT @__hostName = HOST_NAME()'
				,N'@__hostName nvarchar(500) OUTPUT'
				,@__hostName = @__hostName OUTPUT
		END TRY

		BEGIN CATCH
			SET @__hostName = CURRENT_USER
		END CATCH

		SELECT NEWID() AS Id
			,'model.Encounters' AS ObjectName
			,1 AS ChangeTypeId
			,@__auditDate AS AuditDateTime
			,@__hostName AS HostName
			,@__userId AS UserId
			,CONVERT(NVARCHAR, ServerProperty('ServerName')) AS ServerName
			,'[AppointmentId]' AS KeyNames
			,CAST('[' + CAST([AppointmentId] AS NVARCHAR(max)) + ']' AS NVARCHAR(400)) AS KeyValues
			,PatientId AS PatientId
			,AppointmentId AS AppointmentId
		INTO #__AuditEntries
		FROM (
		SELECT i.[AppointmentId]
			,i.[PatientId]
			,COALESCE(CASE i.[Status] WHEN 'W' THEN 2 WHEN 'M' THEN 3 WHEN 'E' THEN 4 WHEN 'G' THEN 5 WHEN 'H' THEN 6 WHEN 'U' THEN 15 END, 14) AS EncounterStatusId
		FROM [inserted] i 
			) AS changedRows




		INSERT INTO dbo.AuditEntries (
			Id
			,ObjectName
			,ChangeTypeId
			,AuditDateTime
			,HostName
			,UserId
			,ServerName
			,KeyNames
			,KeyValues
			,PatientId
			,AppointmentId
			)
		SELECT *
		FROM #__AuditEntries

		INSERT INTO dbo.AuditEntryChanges (AuditEntryId,ColumnName,OldValue,NewValue)
		SELECT ae.Id
			,'EncounterStatusId'
			,CASE 
				WHEN OldValue = '__NULL__'
					THEN NULL
				ELSE OldValue
				END
			,CASE 
				WHEN NewValue = '__NULL__'
					THEN NULL
				ELSE NewValue
				END
		FROM (
			SELECT KeyValues
				,InsertedColumnName AS ColumnName
				,OldValue
				,NewValue
			FROM (
				SELECT CAST('[' + CAST(inserted.[AppointmentId] AS NVARCHAR(max)) + ']' AS NVARCHAR(400)) AS KeyValues
					,(COALESCE(CAST(inserted.[AppointmentId] AS NVARCHAR(max)), '__NULL__')) AS [AppointmentId]
					,(COALESCE(CAST(inserted.[PatientId] AS NVARCHAR(max)), '__NULL__')) AS [PatientId]
					,(COALESCE(CAST(inserted.[EncounterStatusId] AS NVARCHAR(max)), '__NULL__')) AS [EncounterStatusId]
					,(COALESCE(CAST(d.[AppointmentId] AS NVARCHAR(max)), '__NULL__')) AS [DeletedAppointmentId]
					,(COALESCE(CAST(d.[PatientId] AS NVARCHAR(max)), '__NULL__')) AS [DeletedPatientId]
					,(COALESCE(CAST(COALESCE(CASE d.[Status] WHEN 'W' THEN 2 WHEN 'M' THEN 3 WHEN 'E' THEN 4 WHEN 'G' THEN 5 WHEN 'H' THEN 6 WHEN 'U' THEN 15 END, 14) AS NVARCHAR(max)), '__NULL__')) AS [DeletedEncounterStatusId]
					
				FROM (
					SELECT i.[ActivityId]
					,i.[AppointmentId]
					,i.[PatientId]
					,COALESCE(CASE i.[Status] WHEN 'W' THEN 2 WHEN 'M' THEN 3 WHEN 'E' THEN 4 WHEN 'G' THEN 5 WHEN 'H' THEN 6 WHEN 'U' THEN 15 END, 14) AS EncounterStatusId
				FROM [inserted] i
				
					
				EXCEPT
					
				SELECT d.[ActivityId]
					,d.[AppointmentId]
					,d.[PatientId]
					,COALESCE(CASE d.[Status] WHEN 'W' THEN 2 WHEN 'M' THEN 3 WHEN 'E' THEN 4 WHEN 'G' THEN 5 WHEN 'H' THEN 6 WHEN 'U' THEN 15 END, 14) AS EncounterStatusId
					
					FROM deleted d
					) AS inserted
				INNER JOIN deleted d ON inserted.[ActivityId] = d.[ActivityId]
				
				) AS insertedJoinedWithDeleted
			UNPIVOT(NewValue FOR InsertedColumnName IN (
						[AppointmentId]
						,[PatientId]
						,[EncounterStatusId]
						)) AS unpivotNewValue
			UNPIVOT(OldValue FOR DeletedColumnName IN (
						[DeletedAppointmentId]
						,[DeletedPatientId]
						,[DeletedEncounterStatusId]
						)) AS unpivotOldValue
			WHERE 'Deleted' + InsertedColumnName = DeletedColumnName
				AND ((OldValue IS NULL
						AND NewValue IS NOT NULL)
					OR (NewValue IS NOT NULL
						AND OldValue IS NULL)
					OR OldValue <> NewValue)

			) AS q
		INNER JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues

	END

	SET NOCOUNT OFF
END

