﻿IF OBJECT_ID('model.DropExternalSystemEntityMapping','P') IS NOT NULL
	DROP PROCEDURE model.DropExternalSystemEntityMapping
GO

CREATE PROCEDURE model.DropExternalSystemEntityMapping (@PracticeRepositoryEntityId INT)
AS
BEGIN
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL
		SELECT 1 AS Value INTO #NoAudit

	;WITH CTE AS (
	SELECT 
	* 
	FROM model.ExternalSystemEntityMappings esem 
	WHERE esem.PracticeRepositoryEntityId = @PracticeRepositoryEntityId
	)
	DELETE FROM CTE

END
GO

-- model.Tables turned into views
DECLARE @Views TABLE (PracticeRepositoryEntityId INT)

INSERT INTO @Views (PracticeRepositoryEntityId)
SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalCondition'

DECLARE @sql NVARCHAR(MAX)
SET @sql = ''

SELECT @sql = @sql + 'EXEC model.DropExternalSystemEntityMapping ' + CONVERT(NVARCHAR(MAX),PracticeRepositoryEntityId)
FROM @Views

EXEC sp_executesql @sql