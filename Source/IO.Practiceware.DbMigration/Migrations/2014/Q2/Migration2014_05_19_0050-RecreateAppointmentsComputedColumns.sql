﻿IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_Appointments_InvoiceNumber' AND object_id = OBJECT_ID(N'[dbo].[Appointments]'))
BEGIN
	DROP INDEX IX_Appointments_InvoiceNumber ON [dbo].[Appointments]
END
GO

IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_Appointments_AppointmentInsuranceTypeId' AND object_id = OBJECT_ID(N'[dbo].[Appointments]'))
BEGIN
	DROP INDEX IX_Appointments_AppointmentInsuranceTypeId ON [dbo].[Appointments]
END
GO
IF COL_LENGTH(N'dbo.Appointments', N'AppointmentInsuranceTypeId') IS NOT NULL
BEGIN
	ALTER TABLE dbo.Appointments DROP COLUMN AppointmentInsuranceTypeId
END
GO

IF COL_LENGTH(N'dbo.Appointments', N'InvoiceNumber') IS NOT NULL
BEGIN
	ALTER TABLE dbo.Appointments DROP COLUMN InvoiceNumber
END
GO

ALTER TABLE dbo.Appointments
ADD AppointmentInsuranceTypeId AS 
(CASE
	WHEN ApptInsType = 'M'
		THEN 1
	WHEN ApptInsType = 'V'
		THEN 2
	WHEN ApptInsType = 'A'
		THEN 3
	WHEN ApptInsType = 'W'
		THEN 4
	ELSE NULL
END
) PERSISTED
GO

ALTER TABLE dbo.Appointments 
ADD InvoiceNumber AS 
(CONVERT(NVARCHAR(30),CASE WHEN (LEN(CONVERT(NVARCHAR,PatientId)) <= 5 AND LEN(CONVERT(NVARCHAR,AppointmentId)) <= 5)
		THEN REPLACE(STR(CONVERT(NVARCHAR,PatientId), 5, 0), ' ', '0')+'-'+REPLACE(STR(CONVERT(NVARCHAR,AppointmentId) , 5, 0), ' ', '0')
	WHEN (LEN(CONVERT(NVARCHAR,PatientId)) > 5 AND LEN(CONVERT(NVARCHAR,AppointmentId)) <= 5)
		THEN CONVERT(NVARCHAR,PatientId)+'-'+REPLACE(STR(CONVERT(NVARCHAR,AppointmentId) , 5, 0), ' ', '0')
	WHEN (LEN(CONVERT(NVARCHAR,PatientId)) <= 5 AND LEN(CONVERT(NVARCHAR,AppointmentId)) > 5)
		THEN REPLACE(STR(CONVERT(NVARCHAR,PatientId), 5, 0), ' ', '0')+'-'+CONVERT(NVARCHAR,AppointmentId)
	ELSE CONVERT(NVARCHAR,PatientId)+'-'+CONVERT(NVARCHAR,AppointmentId)END)
) PERSISTED
GO

CREATE INDEX IX_Appointments_InvoiceNumber ON dbo.Appointments (InvoiceNumber)
GO

CREATE INDEX IX_Appointments_AppointmentInsuranceTypeId ON dbo.Appointments(AppointmentInsuranceTypeId);
GO


