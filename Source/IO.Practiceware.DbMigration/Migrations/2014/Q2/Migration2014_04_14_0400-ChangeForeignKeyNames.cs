﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations._2014.Q2
{
    [Migration(201404140400)]
    public class Migration201404140400 : NonFluentTransactionalUpOnlyMigration
    {
        private const string GetForeignKeys = @"SELECT f.name AS ForeignKey," +
                                        "OBJECT_SCHEMA_NAME(f.parent_object_id) AS SchemaName," +
                                        "OBJECT_NAME(f.parent_object_id) AS TableName," +
                                        "COL_NAME(fc.parent_object_id," +
                                        "fc.parent_column_id) AS ColumnName," +
                                        "OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName,"+
                                        "OBJECT_SCHEMA_NAME(f.referenced_object_id) AS ReferenceSchemaName," +
                                        "COL_NAME(fc.referenced_object_id," +
                                        "fc.referenced_column_id) AS ReferenceColumnName " +
                                        "FROM sys.foreign_keys AS f " +
                                        "INNER JOIN sys.foreign_key_columns AS fc " +
                                        "ON f.OBJECT_ID = fc.constraint_object_id";
        private const string DropForeignKey = @"IF EXISTS " +
                                                    "(SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N\'{2}.{0}\') " +
                                                    "AND parent_object_id = OBJECT_ID(N\'{2}.{1}\')) " +
                                                    "ALTER TABLE {2}.{1} DROP CONSTRAINT {0}";
        private const string CreateNewForeignKey = @"IF EXISTS(SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'{5}.{1}')) " +
                                                        "IF NOT EXISTS " +
                                                        "(SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N\'{5}.{0}\') " +
                                                        "AND parent_object_id = OBJECT_ID(N\'{5}.{1}\') ) " +
                                                        "ALTER TABLE {5}.{1} WITH NOCHECK ADD CONSTRAINT {0} FOREIGN KEY ({2}) REFERENCES {6}.{3}({4})";

        public class ForeignKey
        {
            public string FormerForeignKeyName { get; set; }
            public string SchemaName { get; set; }
            public string NewForeignKeyName { get; set; }
            public string TableName { get; set; }
            public string ColumnName { get; set; }
            public string ReferenceTableName { get; set; }
            public string ReferenceSchemaName { get; set; }
            public string ReferenceColumnName { get; set; }
        }

        private List<ForeignKey> _fks;

        protected override void MigrateUp(IDbConnection connection, IDbTransaction transaction)
        {
            _fks = new List<ForeignKey>();
            using (var command = connection.CreateCommand())
            {
                command.CommandText = GetForeignKeys;
                command.Transaction = transaction;
                var dt = new DataTable();
                using (IDataReader reader = command.ExecuteReader())
                {
                    dt.Load(reader);
                    foreach (var row in dt.Rows.OfType<DataRow>())
                    {
                        var foreignKey = new ForeignKey
                        {
                            FormerForeignKeyName = row["ForeignKey"].ToString(),
                            ColumnName = row["ColumnName"].ToString(),
                            TableName = row["TableName"].ToString(),
                            SchemaName = row["SchemaName"].ToString(),
                            ReferenceColumnName = row["ReferenceColumnName"].ToString(),
                            ReferenceSchemaName = row["ReferenceSchemaName"].ToString(),
                            ReferenceTableName = row["ReferenceTableName"].ToString()
                        };

                        foreignKey.NewForeignKeyName = "FK_" + foreignKey.TableName + foreignKey.ReferenceTableName + foreignKey.ColumnName;
                        //We're checking for this pattern because all of the foreign keys that start with FK__ end with numbers 
                        //and otherwise do not follow any standard naming convention
                        if (foreignKey.FormerForeignKeyName.StartsWith("FK__"))
                            _fks.Add(foreignKey);
                    }
                }
            }
            foreach (var fk in _fks)
            {
                using (var dropConstraintCommand = connection.CreateCommand())
                {
                    dropConstraintCommand.Transaction = transaction;
                    dropConstraintCommand.CommandText = string.Format(DropForeignKey, fk.FormerForeignKeyName, fk.TableName, fk.SchemaName);
                    dropConstraintCommand.ExecuteNonQuery();
                }
                using (var createConstraintCommand = connection.CreateCommand())
                {
                    createConstraintCommand.Transaction = transaction;
                    createConstraintCommand.CommandText = string.Format(CreateNewForeignKey, fk.NewForeignKeyName, fk.TableName, fk.ColumnName, fk.ReferenceTableName, fk.ReferenceColumnName, fk.SchemaName, fk.ReferenceSchemaName);
                    createConstraintCommand.ExecuteNonQuery();
                }
            }
        }
    }
}
