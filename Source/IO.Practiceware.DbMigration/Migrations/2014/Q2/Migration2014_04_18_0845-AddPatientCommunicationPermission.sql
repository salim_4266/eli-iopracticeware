﻿--Task 9337.Permissions controlling patient's contacts and communication preferences

--Link a Patient Communications Contact
IF NOT EXISTS (SELECT Id FROM [model].[Permissions] WHERE Id = 44)
BEGIN
	SET IDENTITY_INSERT [model].[Permissions] ON
	INSERT INTO [model].[Permissions] (Id, Name, PermissionCategoryId) VALUES (44, 'Link Contact', 10)
	SET IDENTITY_INSERT [model].[Permissions] OFF

	
	INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)

	SELECT CONVERT(bit,0) IsDenied, 
	re.ResourceId AS UserId, pe.Id AS PermissionId
	FROM dbo.Resources re
	INNER JOIN model.Permissions pe ON pe.Id > 0
	WHERE ResourceType <> 'R'
	AND pe.Id = 44 AND SUBSTRING(re.ResourcePermissions, 107, 1) = 1
END

GO

--Edit a Patient Communications Contact
IF NOT EXISTS (SELECT Id FROM [model].[Permissions] WHERE Id = 45)
BEGIN
	SET IDENTITY_INSERT [model].[Permissions] ON
	INSERT INTO [model].[Permissions] (Id, Name, PermissionCategoryId) VALUES (45, 'Edit Contact', 10)
	SET IDENTITY_INSERT [model].[Permissions] OFF

	INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)

	SELECT CONVERT(bit,0) IsDenied, 
	re.ResourceId AS UserId, pe.Id AS PermissionId
	FROM dbo.Resources re
	INNER JOIN model.Permissions pe ON pe.Id > 0
	WHERE ResourceType <> 'R'
	AND pe.Id = 45 AND SUBSTRING(re.ResourcePermissions, 107, 1) = 1
END

GO

--UnLink a Patient Communications Contact
IF NOT EXISTS (SELECT Id FROM [model].[Permissions] WHERE Id = 46)
BEGIN
	SET IDENTITY_INSERT [model].[Permissions] ON
	INSERT INTO [model].[Permissions] (Id, Name, PermissionCategoryId) VALUES (46, 'UnLink Contact', 10)
	SET IDENTITY_INSERT [model].[Permissions] OFF

	INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)

	SELECT CONVERT(bit,0) IsDenied, 
	re.ResourceId AS UserId, pe.Id AS PermissionId
	FROM dbo.Resources re
	INNER JOIN model.Permissions pe ON pe.Id > 0
	WHERE ResourceType <> 'R'
	AND pe.Id = 46 AND SUBSTRING(re.ResourcePermissions, 107, 1) = 1
END

GO
