﻿--Task #9178.
--Incase if 32,33,34 rows were already created with SIU,then delete them and create new rows.
--Create a Temp Table for not losing any messages.
SELECT esm.*
	,esmt.Id AS MessageTypeId
INTO #tmpExtMsg
FROM model.ExternalSystemMessages esm
INNER JOIN model.ExternalSystemExternalSystemMessageTypes esesm ON esesm.Id = esm.ExternalSystemExternalSystemMessageTypeId
INNER JOIN model.ExternalSystemMessageTypes esmt ON esesm.ExternalSystemMessageTypeId = esmt.Id
INNER JOIN model.Externalsystems es ON es.Id = esesm.ExternalSystemId
WHERE (
		esmt.Id = 31
		AND esmt.NAME = 'SIU_S12_V23_Outbound'
		)
	OR (
		esmt.Id = 32
		AND esmt.NAME = 'SIU_S14_V23_Outbound'
		)
	OR (
		esmt.Id = 33
		AND esmt.NAME = 'SIU_S15_V23_Outbound'
		)

--Create a Temp Table for not losing any model.ExternalSystemMessagePracticeRepositoryEntities rows
SELECT *
INTO #tmpEsmp
FROM model.ExternalSystemMessagePracticeRepositoryEntities
WHERE ExternalSystemMessageId IN (
		SELECT #tmpExtMsg.Id
		FROM #tmpExtMsg
		)

--Remove rows from model.ExternalSystemMessagePracticeRepositoryEntities
DELETE
FROM model.ExternalSystemMessagePracticeRepositoryEntities
WHERE ExternalSystemMessageId IN (
		SELECT #tmpExtMsg.Id
		FROM #tmpExtMsg
		)

--Remove rows from ExternalSystemMessages if any
DELETE
FROM model.ExternalSystemMessages
WHERE Id IN (
		SELECT esm.Id
		FROM model.ExternalSystemMessages esm
		INNER JOIN model.ExternalSystemExternalSystemMessageTypes esesm ON esesm.Id = esm.ExternalSystemExternalSystemMessageTypeId
		INNER JOIN model.ExternalSystemMessageTypes esmt ON esesm.ExternalSystemMessageTypeId = esmt.Id
		INNER JOIN model.Externalsystems es ON es.Id = esesm.ExternalSystemId
		WHERE (
				esmt.Id = 31
				AND esmt.NAME = 'SIU_S12_V23_Outbound'
				)
			OR (
				esmt.Id = 32
				AND esmt.NAME = 'SIU_S14_V23_Outbound'
				)
			OR (
				esmt.Id = 33
				AND esmt.NAME = 'SIU_S15_V23_Outbound'
				)
		)

--Remove ExternalSystemExternalSystemMessageTypes rows if any
DELETE
FROM model.ExternalSystemExternalSystemMessageTypes
WHERE Id IN (
		SELECT esesm.Id
		FROM model.ExternalSystemExternalSystemMessageTypes esesm
		INNER JOIN model.ExternalSystemMessageTypes esm ON esesm.ExternalSystemMessageTypeId = esm.Id
		INNER JOIN model.Externalsystems es ON es.Id = esesm.ExternalSystemId
		WHERE (
				esm.Id = 31
				AND esm.NAME = 'SIU_S12_V23_Outbound'
				)
			OR (
				esm.Id = 32
				AND esm.NAME = 'SIU_S14_V23_Outbound'
				)
			OR (
				esm.Id = 33
				AND esm.NAME = 'SIU_S15_V23_Outbound'
				)
		)

--Remove ExternalSystemMessageTypes rows if any
DELETE
FROM model.ExternalSystemMessageTypes
WHERE Id IN (
		SELECT esm.Id
		FROM model.ExternalSystemMessageTypes esm
		WHERE (
				esm.Id = 31
				AND esm.NAME = 'SIU_S12_V23_Outbound'
				)
			OR (
				esm.Id = 32
				AND esm.NAME = 'SIU_S14_V23_Outbound'
				)
			OR (
				esm.Id = 33
				AND esm.NAME = 'SIU_S15_V23_Outbound'
				)
		)

--Create SIU 12,14,15 Outbound MessageTypes.
--Disable Readonly Trigger to insert the new MessageTypes.
ALTER TABLE model.ExternalSystemMessageTypes DISABLE TRIGGER ExternalSystemMessageTypesReadOnlyTrigger

SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] ON

IF NOT EXISTS (
		SELECT *
		FROM model.ExternalSystemMessageTypes
		WHERE Id = 33
		)
	INSERT INTO model.ExternalSystemMessageTypes (
		Id
		,NAME
		,Description
		,IsOutbound
		)
	SELECT 33
		,'SIU_S12_V23_Outbound'
		,'Add Appointment'
		,1
ELSE
	UPDATE model.ExternalSystemMessageTypes
	SET NAME = 'SIU_S12_V23_Outbound'
		,Description = 'Add Appointment'
		,IsOutbound = 1
	WHERE Id = 33

IF NOT EXISTS (
		SELECT *
		FROM model.ExternalSystemMessageTypes
		WHERE Id = 34
		)
	INSERT INTO model.ExternalSystemMessageTypes (
		Id
		,NAME
		,Description
		,IsOutbound
		)
	SELECT 34
		,'SIU_S14_V23_Outbound'
		,'Check-in/Check-out Appointment'
		,1
ELSE
	UPDATE model.ExternalSystemMessageTypes
	SET NAME = 'SIU_S14_V23_Outbound'
		,Description = 'Check-in/Check-out Appointment'
		,IsOutbound = 1
	WHERE Id = 34

IF NOT EXISTS (
		SELECT *
		FROM model.ExternalSystemMessageTypes
		WHERE Id = 35
		)
	INSERT INTO model.ExternalSystemMessageTypes (
		Id
		,NAME
		,Description
		,IsOutbound
		)
	SELECT 35
		,'SIU_S15_V23_Outbound'
		,'Cancel Appointment'
		,1
ELSE
	UPDATE model.ExternalSystemMessageTypes
	SET NAME = 'SIU_S15_V23_Outbound'
		,Description = 'Cancel Appointment'
		,IsOutbound = 1
	WHERE Id = 35

SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] OFF

ALTER TABLE model.ExternalSystemMessageTypes ENABLE TRIGGER ExternalSystemMessageTypesReadOnlyTrigger
GO

--Check just in case if Omedix exists.If not create
IF NOT EXISTS (
		SELECT *
		FROM model.ExternalSystems
		WHERE NAME = 'Omedix'
		)
	INSERT INTO model.ExternalSystems (NAME)
	VALUES ('Omedix')

DECLARE @OmedixExternal INT

SET @OmedixExternal = (
		SELECT Id
		FROM model.ExternalSystems
		WHERE NAME = 'Omedix'
		)

DECLARE @MessageType INT

--SIU_S12_V23_Outbound
SET @MessageType = (
		SELECT Id
		FROM model.ExternalSystemMessageTypes
		WHERE NAME = 'SIU_S12_V23_Outbound'
			AND Id = 33
			AND IsOutbound = 1
		)

IF NOT EXISTS (
		SELECT *
		FROM model.ExternalSystemExternalSystemMessageTypes
		WHERE ExternalSystemId = @OmedixExternal
			AND ExternalSystemMessageTypeId = @MessageType
		)
	INSERT INTO model.ExternalSystemExternalSystemMessageTypes (
		ExternalSystemId
		,ExternalSystemMessageTypeId
		)
	SELECT @OmedixExternal
		,@MessageType

--SIU_S14_V23_Outbound
SET @MessageType = (
		SELECT Id
		FROM model.ExternalSystemMessageTypes
		WHERE NAME = 'SIU_S14_V23_Outbound'
			AND Id = 34
			AND IsOutbound = 1
		)

IF NOT EXISTS (
		SELECT *
		FROM model.ExternalSystemExternalSystemMessageTypes
		WHERE ExternalSystemId = @OmedixExternal
			AND ExternalSystemMessageTypeId = @MessageType
		)
	INSERT INTO model.ExternalSystemExternalSystemMessageTypes (
		ExternalSystemId
		,ExternalSystemMessageTypeId
		)
	SELECT @OmedixExternal
		,@MessageType

--SIU_S15_V23_Outbound
SET @MessageType = (
		SELECT Id
		FROM model.ExternalSystemMessageTypes
		WHERE NAME = 'SIU_S15_V23_Outbound'
			AND Id = 35
			AND IsOutbound = 1
		)

IF NOT EXISTS (
		SELECT *
		FROM model.ExternalSystemExternalSystemMessageTypes
		WHERE ExternalSystemId = @OmedixExternal
			AND ExternalSystemMessageTypeId = @MessageType
		)
	INSERT INTO model.ExternalSystemExternalSystemMessageTypes (
		ExternalSystemId
		,ExternalSystemMessageTypeId
		)
	SELECT @OmedixExternal
		,@MessageType


--Insert Rows in ExternalSystemMessagePracticeRepositoryEntities
INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities
SELECT *
FROM #tmpEsmp
WHERE #tmpEsmp.Id NOT IN (
		SELECT ID
		FROM model.ExternalSystemMessagePracticeRepositoryEntities
		)

--Insert Back Rows in externalSytemMessages Table.
UPDATE #tmpExtMsg
SET #tmpExtMsg.ExternalSystemExternalSystemMessageTypeId = CASE #tmpExtMsg.MessageTypeId
		WHEN 31
			THEN (
					SELECT Id
					FROM model.ExternalSystemExternalSystemMessageTypes
					WHERE ExternalSystemId = (
							SELECT ID
							FROM model.Externalsystems
							WHERE NAME = 'Omedix'
							)
						AND ExternalSystemMessageTypeId = 33
					)
		WHEN 32
			THEN (
					SELECT Id
					FROM model.ExternalSystemExternalSystemMessageTypes
					WHERE ExternalSystemId = (
							SELECT ID
							FROM model.Externalsystems
							WHERE NAME = 'Omedix'
							)
						AND ExternalSystemMessageTypeId = 34
					)
		WHEN 33
			THEN (
					SELECT Id
					FROM model.ExternalSystemExternalSystemMessageTypes
					WHERE ExternalSystemId = (
							SELECT ID
							FROM model.Externalsystems
							WHERE NAME = 'Omedix'
							)
						AND ExternalSystemMessageTypeId = 35
					)
		END
	,#tmpExtMsg.MessageTypeId = ''

ALTER TABLE #tmpExtMsg

DROP COLUMN MessageTypeId;

SELECT *
INTO #tmpExtMsgCopy
FROM #tmpExtMsg

INSERT INTO model.ExternalSystemMessages
SELECT *
FROM #tmpExtMsgCopy
WHERE #tmpExtMsgCopy.Id NOT IN (
		SELECT Id
		FROM model.ExternalSystemMessages
		)
GO

--Updating these rows because Migration201308200001.cs file will create all externalSystemMessageTypeId's,but will exculde/removes names with _Inbound and _outBound and inserts without that.
ALTER TABLE model.ExternalSystemMessageTypes DISABLE TRIGGER ExternalSystemMessageTypesReadOnlyTrigger

UPDATE model.ExternalSystemMessageTypes
SET NAME = 'ACK_V231_Inbound',Description='Acknowledgement',IsOutBound=0
WHERE ID = 30

UPDATE model.ExternalSystemMessageTypes
SET NAME = 'QRDA_Category_I_Outbound',Description='QRDA_Category I',IsOutBound=1
WHERE ID = 31

UPDATE model.ExternalSystemMessageTypes
SET NAME = 'QRDA_Category_III_Outbound',Description='QRDA_Category III',IsOutBound=1
WHERE ID = 32

ALTER TABLE model.ExternalSystemMessageTypes ENABLE TRIGGER ExternalSystemMessageTypesReadOnlyTrigger