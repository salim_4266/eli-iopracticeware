﻿ALTER TRIGGER [dbo].[UpdateEncounterId] ON [dbo].[Appointments]
AFTER INSERT
AS
SET NOCOUNT ON;
BEGIN
	IF EXISTS (SELECT * FROM inserted i WHERE i.Comments <> 'ASC CLAIM')
	BEGIN
		UPDATE ap
		SET ap.EncounterId = i.AppointmentId
		FROM Appointments ap
		INNER JOIN inserted i ON i.AppointmentId = ap.AppointmentId
	END

	IF EXISTS (SELECT * FROM inserted i WHERE i.Comments = 'ASC CLAIM')
	BEGIN
		;WITH CTE AS (
		SELECT ap.AppointmentId
		FROM Appointments apASC
		INNER JOIN inserted i ON apASC.AppointmentId = i.AppointmentId
		INNER JOIN dbo.Appointments ap ON apASC.PatientId = ap.PatientId 
			AND apASC.AppTypeId = ap.AppTypeId 
			AND apASC.AppDate = ap.AppDate 
			AND apASC.AppTime = 0 
			AND ap.AppTime > 0 
			AND apASC.ScheduleStatus = ap.ScheduleStatus 
			AND ap.ScheduleStatus = 'D' 
			AND apASC.ResourceId1 = ap.ResourceId1 
			AND apASC.ResourceId2 = ap.ResourceId2 
			AND ap.Comments <> 'ASC CLAIM' 
			AND ap.AppointmentId = (
				SELECT TOP 1 pa.AppointmentId
				FROM dbo.PracticeActivity pa
				INNER JOIN inserted i ON i.PatientId = pa.PatientId 
					AND i.AppDate = pa.ActivityDate 
					AND i.ResourceId2 = pa.LocationId
				WHERE pa.ActivityStatusTime <> '' 
					AND pa.ActivityStatusTime IS NOT NULL
				ORDER BY pa.ActivityStatusTime DESC
			)
		WHERE apASC.Comments = 'ASC CLAIM' 
			AND i.AppointmentId = apASC.AppointmentId
		)
		UPDATE apASC
		SET apASC.EncounterId = (SELECT AppointmentId FROM CTE)
		FROM Appointments apASC
		INNER JOIN inserted i ON apASC.AppointmentId = i.AppointmentId
		WHERE apASC.Comments = 'ASC CLAIM' 
	END
END