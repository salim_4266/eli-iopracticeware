﻿IF NOT EXISTS(SELECT * FROM model.ApplicationSettings WHERE Name = 'OmedixPracticeId')
	INSERT INTO [model].[ApplicationSettings]
           ([Value]
           ,[Name]
           ,[MachineName]
           ,[ApplicationSettingTypeId]
           ,[UserId])
     VALUES
           (''
           ,'OmedixPracticeId'
           ,NULL
           ,11
           ,NULL)