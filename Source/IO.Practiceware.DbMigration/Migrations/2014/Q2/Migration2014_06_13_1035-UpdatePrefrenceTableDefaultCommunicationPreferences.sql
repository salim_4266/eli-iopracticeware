﻿IF (
		EXISTS (
			SELECT *
			FROM information_schema.tables
			WHERE table_schema = 'model'
				AND table_name = 'patients'
			)
		AND EXISTS (
			SELECT *
			FROM information_schema.tables
			WHERE table_schema = 'model'
				AND table_name = 'patientaddresses'
			)
		AND EXISTS (
			SELECT *
			FROM information_schema.tables
			WHERE table_schema = 'model'
				AND table_name = 'patientcommunicationpreferences'
			)
		)
BEGIN
	---drop the temp table if exist.
	IF object_id('tempdb..#patientcommunicationpreferencestemp') IS NOT NULL
		DROP TABLE #patientcommunicationpreferencestemp

	---insert the missing preference patient details to temp table
	SELECT missingpreference.id AS patientid
		,1 AS communicationmethodtypeid
		,patientadd.id AS patientaddressid
		,0 AS isaddressedtorecipient
		,'patientaddresscommunicationpreference' AS __entitytype__
		,0 AS isoptout
	INTO #patientcommunicationpreferencestemp
	FROM (
		SELECT p.id
		FROM [model].[patients] p
		LEFT JOIN [model].[patientcommunicationpreferences] pc ON p.id = pc.patientid
		WHERE pc.patientid IS NULL
		) AS missingpreference
	INNER JOIN [model].[patientaddresses] patientadd ON missingpreference.id = patientadd.patientid
	WHERE ordinalid = 1

	---insert the data to preference table from filterd data in temp table with communication type as recall
	INSERT INTO [model].[patientcommunicationpreferences] (
		patientid
		,patientcommunicationtypeid
		,communicationmethodtypeid
		,patientaddressid
		,isaddressedtorecipient
		,__entitytype__
		,isoptout
		)
	SELECT patientid
		,1 AS patientcommunicationtypeid
		,communicationmethodtypeid
		,patientaddressid
		,isaddressedtorecipient
		,__entitytype__
		,isoptout
	FROM #patientcommunicationpreferencestemp

	---insert the data to preference table from filterd data in temp table with communication type as statement
	INSERT INTO [model].[patientcommunicationpreferences] (
		patientid
		,patientcommunicationtypeid
		,communicationmethodtypeid
		,patientaddressid
		,isaddressedtorecipient
		,__entitytype__
		,isoptout
		)
	SELECT patientid
		,3 AS patientcommunicationtypeid
		,communicationmethodtypeid
		,patientaddressid
		,isaddressedtorecipient
		,__entitytype__
		,isoptout
	FROM #patientcommunicationpreferencestemp

	---drop the temp table after insertion is done.
	DROP TABLE #patientcommunicationpreferencestemp
END