﻿--Fix for #11503.

IF EXISTS (SELECT * FROM sys.indexes WHERE Name = 'UC_Resources_ResourceName')
	ALTER TABLE [dbo].[Resources] DROP CONSTRAINT [UC_Resources_ResourceName]
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE Name = 'ResourcesResourceNameTrigger')
	DROP TRIGGER [ResourcesResourceNameTrigger]
GO

CREATE TRIGGER ResourcesResourceNameTrigger
ON dbo.Resources 
AFTER INSERT, UPDATE
AS BEGIN
SET NOCOUNT ON
	DECLARE @TotalCount INT
	DECLARE @Counter INT
	DECLARE @CurrentDuplicate NVARCHAR(MAX)
	DECLARE @CurrentResourceId INT

	SELECT r.ResourceName
	INTO #TMPResourceName
	FROM dbo.Resources r
	JOIN (
		SELECT ResourceName 
		FROM inserted 
		EXCEPT 
		SELECT ResourceName 
		FROM deleted
	) b ON r.ResourceName = b.ResourceName
	GROUP BY r.ResourceName
	HAVING COUNT(*) > 1

	SELECT @TotalCount = COUNT(#TMPResourceName.ResourceName)
	FROM #TMPResourceName

	WHILE (@TotalCount > 0)
	BEGIN
		SELECT TOP 1 @CurrentDuplicate = #TMPResourceName.ResourceName
		FROM #TMPResourceName

		SELECT ResourceId
		INTO #TMPResId
		FROM dbo.Resources
		WHERE ResourceName IN (@CurrentDuplicate)
		ORDER BY ResourceType DESC

		SELECT @Counter = COUNT(*) - 1
		FROM #TMPResId

		WHILE (@Counter > 0)
		BEGIN
			SELECT TOP 1 @CurrentResourceId = #TMPResId.ResourceId
			FROM #TMPResId ORDER BY ResourceId DESC

			UPDATE dbo.Resources
			SET ResourceName = ResourceName + CAST(@counter AS VARCHAR(3))
			FROM dbo.Resources r
			WHERE ResourceId = @CurrentResourceId

			DELETE
			FROM #TMPResId
			WHERE #TMPResId.ResourceId = @CurrentResourceId

			SELECT @Counter = @Counter - 1
		END

		DELETE
		FROM #TMPResourceName
		WHERE #TMPResourceName.ResourceName = @CurrentDuplicate

		SELECT @TotalCount = @TotalCount - 1

		DROP TABLE #TMPResId
	END

	DROP TABLE #TMPResourceName
END