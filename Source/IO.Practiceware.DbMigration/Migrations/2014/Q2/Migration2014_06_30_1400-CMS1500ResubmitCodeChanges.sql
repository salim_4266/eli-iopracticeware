﻿DECLARE @old VARCHAR(max), @new VARCHAR(max)
SET @old = '<input type="text" id="ResubmitCode" value="@Model.ResubmitCode" style="z-index: 1; left: @(col3Left)px; top: @(row15Top)px; position: absolute; width: 109px; right: 268px; height: 9px;" />'
				
SET @new = '@if (!String.IsNullOrEmpty(@Model.ResubmitCode))
            {
                <input type="text" id="ResubmitCodePreQualifier" value="7" style="z-index: 1; left: @(col3Left)px; top: @(row15Top)px; position: absolute; width: 109px; right: 268px; height: 9px;" />
            }
            <input type="text" id="ResubmitCode" value="@Model.ResubmitCode" style="z-index: 1; left: 791px; top: @(row15Top)px; position: absolute; width: 109px; right: 268px; height: 9px;" />'

IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	

	UPDATE model.TemplateDocuments 	
	SET ContentOriginal=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), @old, @new)) 
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL	

END