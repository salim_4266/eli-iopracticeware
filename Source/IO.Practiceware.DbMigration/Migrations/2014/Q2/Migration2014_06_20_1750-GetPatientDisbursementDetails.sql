﻿IF OBJECT_ID ( 'dbo.GetPatientDisbursementDetails', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetPatientDisbursementDetails;
GO

CREATE PROCEDURE dbo.GetPatientDisbursementDetails
	@PaymentCheck NVARCHAR(200)
	,@EOBDate NVARCHAR(10)
	,@PaymentBatchId INT
AS
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#LocateChecks') IS NOT NULL
BEGIN
	DROP TABLE #LocateChecks
END

SELECT 
fb.Id
,CONVERT(NVARCHAR(MAX),fb.CheckCode) AS CheckCode
INTO #LocateChecks
FROM model.FinancialBatches fb
WHERE CheckCode = @PaymentCheck
	AND CONVERT(DATETIME,ExplanationOfBenefitsDateTime,112) = CONVERT(DATETIME,@EOBDate,112)
	AND fb.Id = @PaymentBatchId
	AND fb.FinancialSourceTypeId = 2
	
IF (@@ROWCOUNT > 0)
BEGIN
SELECT * FROM (
SELECT
adj.Id AS PaymentId
,p.Id AS PatientId
,p.LastName
,p.FirstName
,p.FirstName + ' ' + p.LastName as InsurerName
,adj.Amount AS PaymentAmount
,CONVERT(NVARCHAR,adj.PostedDateTime,112) AS PaymentDate
,CASE 
	WHEN adj.AdjustmentTypeId = 1
		THEN 'Payment'
	WHEN adj.AdjustmentTypeId = 2
		THEN 'Cont Adj'
	ELSE 'Other'
END AS PaymentType
FROM #LocateChecks lc
INNER JOIN model.Adjustments adj ON lc.Id = adj.FinancialBatchId
INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN Appointments ap ON ap.AppointmentId = i.EncounterId
INNER JOIN model.Patients p ON p.Id = ap.PatientId

UNION ALL 

SELECT
fi.Id AS PaymentId
,p.Id AS PatientId
,p.LastName
,p.FirstName
,p.FirstName + ' ' + p.LastName as InsurerName
,fi.Amount AS PaymentAmount
,CONVERT(NVARCHAR,fi.PostedDateTime,112) AS PaymentDate
,CASE
	WHEN fi.FinancialInformationTypeId = 1
		THEN 'Deduct'
	WHEN fi.FinancialInformationTypeId = 2
		THEN 'CoIns'
	ELSE 'Other'
END AS PaymentType
FROM #LocateChecks lc
INNER JOIN model.FinancialInformations fi ON lc.Id = fi.FinancialBatchId
INNER JOIN model.InvoiceReceivables ir ON ir.Id = fi.InvoiceReceivableId
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN Appointments ap ON ap.AppointmentId = i.EncounterId
INNER JOIN model.Patients p ON p.Id = ap.PatientId
)v
ORDER BY v.LastName,v.FirstName
END
GO