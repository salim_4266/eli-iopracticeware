﻿IF EXISTS(SELECT * FROM sys.objects WHERE name = 'PatientDemographicsTable' AND Type = 'U')
BEGIN
	UPDATE ptag
	SET TagId = t.Id
	FROM dbo.PatientDemographicsTable pd
	INNER JOIN model.Tags t ON t.DisplayName = pd.PatType
	INNER JOIN model.PatientTags ptag ON ptag.PatientId = pd.PatientId	
	WHERE ptag.TagId <> t.Id
END