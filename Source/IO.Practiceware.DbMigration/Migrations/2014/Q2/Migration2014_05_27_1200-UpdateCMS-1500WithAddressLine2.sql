﻿IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	
	DECLARE @old VARCHAR(max), @new VARCHAR(max)
	SET @old = 'id="BilledInsuranceAddressLine1" value="@Model.BilledInsuranceAddressLine1"'
				
	SET @new = 'id="BilledInsuranceAddressLine1" value="@Model.BilledInsuranceAddressLine1 @Model.BilledInsuranceAddressLine2 @Model.BilledInsuranceAddressLine3"'
	
	UPDATE model.TemplateDocuments 	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), @old, @new)) 
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL		

END
GO
 