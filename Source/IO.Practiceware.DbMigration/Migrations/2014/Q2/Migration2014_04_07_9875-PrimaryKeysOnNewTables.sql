﻿IF OBJECTPROPERTY(OBJECT_ID(N'[model].[RouteOfAdministrations]','U'), 'TableHasPrimaryKey') = 0
BEGIN
	EXEC('ALTER TABLE [model].RouteOfAdministrations ADD CONSTRAINT [PK_RouteOfAdministrations] PRIMARY KEY (Id)')
END
GO

--Dynamic Forms tables

IF EXISTS (	SELECT * FROM sys.schemas WHERE name = 'df')
BEGIN
	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'Forms' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].[Forms]
			ADD CONSTRAINT [PK_Forms] PRIMARY KEY (FormId)')

	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'FourthForms' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].[FourthForms]
			ADD CONSTRAINT [PK_FourthForms] PRIMARY KEY (FourthFormId)')

	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'FourthFormsControls' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].[FourthFormsControls]
			ADD CONSTRAINT [PK_FourthFormsControls] PRIMARY KEY (FourthControlId)')
	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'FourthFormsLanguage' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].[FourthFormsLanguage]
			ADD CONSTRAINT [PK_FourthFormsLanguage] PRIMARY KEY (LanguageId)')
	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'FormsControls' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].[FormsControls]
			ADD CONSTRAINT [PK_FormsControls] PRIMARY KEY (ControlId)')
	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'FormsLanguage' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].FormsLanguage
			ADD CONSTRAINT [PK_FormsLanguage] PRIMARY KEY (LanguageId)')
	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'Questions' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].Questions
			ADD CONSTRAINT [PK_Questions] PRIMARY KEY (ClassId)')
	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'SecondaryForms' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].SecondaryForms
			ADD CONSTRAINT [PK_SecondaryForms] PRIMARY KEY (SecondaryFormId)')
	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'SecondaryFormsControls' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].SecondaryFormsControls
			ADD CONSTRAINT [PK_SecondaryFormsControls] PRIMARY KEY (SecondaryControlId)')
	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'SecondaryFormsLanguage' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].SecondaryFormsLanguage
			ADD CONSTRAINT [PK_SecondaryFormsLanguage] PRIMARY KEY (LanguageId)')
	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'ThirdForms' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].ThirdForms
			ADD CONSTRAINT [PK_ThirdForms] PRIMARY KEY (ThirdFormId)')
	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'ThirdFormsControls' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].ThirdFormsControls
			ADD CONSTRAINT [PK_ThirdFormsControls] PRIMARY KEY (ThirdControlId)')
	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'ThirdFormsLanguage' 
			AND TABLE_SCHEMA ='df'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY')
	BEGIN
		EXEC('ALTER TABLE [df].ThirdFormsLanguage
			ADD CONSTRAINT [PK_ThirdFormsLanguage] PRIMARY KEY (LanguageId)')
	END

END

--Diagnosis Master tables
IF EXISTS (
	SELECT * 
	FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
	WHERE TABLE_NAME = 'AssociatedDiagnosisTable' 
		AND TABLE_SCHEMA ='dm'
	)
BEGIN
	EXEC('ALTER TABLE dm.AssociatedDiagnosisTable
		ALTER COLUMN AssociatedId INT NOT NULL')
END
GO

IF EXISTS (
	SELECT * 
	FROM sys.objects so
	WHERE name= 'Drugstable' 
		AND OBJECT_SCHEMA_NAME(Object_Id) ='dm'
	)
BEGIN
	EXEC('ALTER TABLE dm.Drugstable
		ALTER COLUMN DrugId INT NOT NULL')
END
GO

IF EXISTS (
	SELECT * 
	FROM sys.objects so
	WHERE name= 'ICD9CPTLinkTable' 
		AND OBJECT_SCHEMA_NAME(Object_Id) ='dm'

	)
BEGIN
	EXEC('ALTER TABLE dm.ICD9CPTLinkTable
		ALTER COLUMN CPTLinkId INT NOT NULL')
END
GO


IF EXISTS (
	SELECT * 
	FROM sys.objects so
	WHERE name= 'PaintbrushTable' 
		AND OBJECT_SCHEMA_NAME(Object_Id) ='dm'

	)
BEGIN
	EXEC('ALTER TABLE dm.PaintbrushTable
		ALTER COLUMN BrushId INT NOT NULL')
END
GO

IF EXISTS (
	SELECT * 
	FROM sys.objects so
	WHERE name= 'PrimaryDiagnosisTable' 
		AND OBJECT_SCHEMA_NAME(Object_Id) ='dm'
	)
BEGIN
	EXEC('ALTER TABLE dm.PrimaryDiagnosisTable
		ALTER COLUMN PrimaryDrillID INT NOT NULL')
END
GO

IF EXISTS (	SELECT * FROM sys.schemas WHERE name = 'dm')
BEGIN
	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'AssociatedDiagnosisTable' 
			AND TABLE_SCHEMA ='dm'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY') AND OBJECT_ID('dm.AssociatedDiagnosisTable') IS NOT NULL
	BEGIN
		EXEC('ALTER TABLE dm.AssociatedDiagnosisTable
			ALTER COLUMN AssociatedId INT NOT NULL')

		EXEC('ALTER TABLE [dm].AssociatedDiagnosisTable
			ADD CONSTRAINT [PK_AssociatedDiagnosisTable] PRIMARY KEY (AssociatedId)')
	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'Drugstable' 
			AND TABLE_SCHEMA ='dm'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY') AND OBJECT_ID('dm.Drugstable') IS NOT NULL
	BEGIN
		EXEC('ALTER TABLE [dm].Drugstable
			ADD CONSTRAINT [PK_Drugstable] PRIMARY KEY (DrugId)')
	END


	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'ICD9CPTLinkTable' 
			AND TABLE_SCHEMA ='dm'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY') AND OBJECT_ID('dm.ICD9CPTLinkTable') IS NOT NULL
	BEGIN
		EXEC('ALTER TABLE [dm].ICD9CPTLinkTable
			ADD CONSTRAINT [PK_ICD9CPTLinkTable] PRIMARY KEY (CPTLinkId)')
	END

	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'PaintbrushTable' 
			AND TABLE_SCHEMA ='dm'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY') AND OBJECT_ID('dm.PaintbrushTable') IS NOT NULL
	BEGIN
		EXEC('ALTER TABLE [dm].PaintbrushTable
			ADD CONSTRAINT [PK_PaintbrushTable] PRIMARY KEY (BrushId)')
	END


	IF NOT EXISTS (
		SELECT * 
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
		WHERE TABLE_NAME = 'PrimaryDiagnosisTable' 
			AND TABLE_SCHEMA ='dm'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY') AND OBJECT_ID('dm.PrimaryDiagnosisTable') IS NOT NULL
	BEGIN
		EXEC('ALTER TABLE [dm].PrimaryDiagnosisTable
			ADD CONSTRAINT [PK_PrimaryDiagnosisTable] PRIMARY KEY (PrimaryDrillID)')
	END
END

IF OBJECT_ID('fdb_rxnorm_mapping.tblDrugMappingFDB','U') IS NOT NULL
BEGIN
	DECLARE @int_system_type_id INT
	SET @int_system_type_id = (SELECT TOP 1 system_type_id FROM sys.types WHERE name = 'int')

	DECLARE @object_id_tblDrugMappingFDB INT
	SET @object_id_tblDrugMappingFDB = OBJECT_ID('fdb_rxnorm_mapping.tblDrugMappingFDB','U')

	IF NOT EXISTS (
	SELECT * 
	FROM sys.columns c
	WHERE c.object_id = @object_id_tblDrugMappingFDB
		AND c.system_type_id = @int_system_type_id
		AND name = 'DrugID'
		AND c.is_nullable = 0
	)
	BEGIN
		EXEC ('ALTER TABLE fdb_rxnorm_mapping.tblDrugMappingFDB ALTER COLUMN DrugID INT NOT NULL')
	END

	IF NOT EXISTS (
	SELECT * 
	FROM sys.columns c
	WHERE c.object_id = @object_id_tblDrugMappingFDB
		AND c.system_type_id = @int_system_type_id
		AND name = 'MappedID'
		AND c.is_nullable = 0
	)
	BEGIN
		EXEC ('ALTER TABLE fdb_rxnorm_mapping.tblDrugMappingFDB ALTER COLUMN MappedID INT NOT NULL')
	END

	IF OBJECTPROPERTY(@object_id_tblDrugMappingFDB, 'TableHasPrimaryKey') = 0
	BEGIN
		EXEC ('ALTER TABLE fdb_rxnorm_mapping.tblDrugMappingFDB ADD CONSTRAINT PK_tblDrugMappingFDB PRIMARY KEY (DrugID,MappedID)')
	END
END
GO

IF OBJECT_ID('fdb_rxnorm_mapping.tblAllergyMappingFDB','U') IS NOT NULL
BEGIN
	DECLARE @int_system_type_id INT
	SET @int_system_type_id = (SELECT TOP 1 system_type_id FROM sys.types WHERE name = 'int')

	DELETE FROM fdb_rxnorm_mapping.tblAllergyMappingFDB WHERE MappedID IS NULL

	;WITH CTE AS (
	SELECT 
	ROW_NUMBER() OVER (PARTITION BY CompositeAllergyID, MappedId ORDER BY CompositeAllergyID, MappedId) AS Id
	,CompositeAllergyID
	,MappedId
	FROM fdb_rxnorm_mapping.tblAllergyMappingFDB
	)
	DELETE FROM CTE WHERE Id > 1
	
	DECLARE @object_id_tblAllergyMappingFDB INT
	SET @object_id_tblAllergyMappingFDB = OBJECT_ID('fdb_rxnorm_mapping.tblAllergyMappingFDB','U')

	IF NOT EXISTS (
	SELECT * 
	FROM sys.columns c
	WHERE c.object_id = @object_id_tblAllergyMappingFDB
		AND c.system_type_id = @int_system_type_id
		AND name = 'CompositeAllergyID'
		AND c.is_nullable = 0
	)
	BEGIN
		EXEC ('ALTER TABLE fdb_rxnorm_mapping.tblAllergyMappingFDB ALTER COLUMN CompositeAllergyID INT NOT NULL')
	END

	IF NOT EXISTS (
	SELECT * 
	FROM sys.columns c
	WHERE c.object_id = @object_id_tblAllergyMappingFDB
		AND c.system_type_id = @int_system_type_id
		AND name = 'MappedID'
		AND c.is_nullable = 0
	)
	BEGIN
		EXEC ('ALTER TABLE fdb_rxnorm_mapping.tblAllergyMappingFDB ALTER COLUMN MappedID INT NOT NULL')
	END

	IF OBJECTPROPERTY(@object_id_tblAllergyMappingFDB, 'TableHasPrimaryKey') = 0
	BEGIN
		ALTER TABLE fdb_rxnorm_mapping.tblAllergyMappingFDB ADD CONSTRAINT PK_tblAllergyMappingFDB PRIMARY KEY (CompositeAllergyID,MappedID)
	END
END
GO