﻿--TFS 12166
UPDATE ptj
SET TransactionRemark = z.CorrectedTransactionRemark
FROM dbo.PracticeTransactionJournal ptj
JOIN (
	SELECT
	TransactionId
	,'/Amt/'+CONVERT(NVARCHAR(MAX),v.CorrectTransactionRemark) AS CorrectedTransactionRemark
	FROM (
		SELECT 
		TransactionId
		,CASE ISNUMERIC(RTRIM(SUBSTRING(TransactionRemark,
			CHARINDEX('/Amt/',TransactionRemark)+5,
			LEN(TransactionRemark)))) 
		WHEN 1
			THEN NULL
		ELSE 
			CONVERT(DECIMAL(18,2),RTRIM(SUBSTRING(TransactionRemark,
			CHARINDEX('/Amt/',TransactionRemark)+5,
			LEN(TransactionRemark)))) 
		END AS PtjAmountSent
		,SUM(bst.AmountSent) AS CorrectTransactionRemark
		FROM PracticeTransactionJournal ptj 
		JOIN dbo.PatientReceivables pr ON pr.ReceivableId = ptj.TransactionTypeId
		JOIN model.BillingServiceTransactions bst ON bst.InvoiceReceivableId = pr.ReceivableId
			AND bst.BillingServiceTransactionStatusId IN (1,2,3,4)
		WHERE TransactionType = 'R'
			AND TransactionDate >= '20140618'
			AND TransactionStatus <> 'Z'
			AND TransactionRef = 'I'
			AND ISNUMERIC(RTRIM(SUBSTRING(TransactionRemark,
			CHARINDEX('/Amt/',TransactionRemark)+5,
			LEN(TransactionRemark)))) =1 
		GROUP BY 
		TransactionId
		,TransactionRemark

		UNION

		SELECT 
		TransactionId
		,CASE ISNUMERIC(RTRIM(SUBSTRING(TransactionRemark,
			CHARINDEX('/Amt/',TransactionRemark)+5,
			LEN(TransactionRemark)))) 
		WHEN 1
			THEN NULL
		ELSE 
		CONVERT(DECIMAL(18,2),RTRIM(SUBSTRING(TransactionRemark,
			CHARINDEX('/Amt/',TransactionRemark)+5,
			LEN(TransactionRemark)))) 
		END AS PtjAmountSent
		,SUM(bst.AmountSent) AS CorrectTransactionRemark
		FROM PracticeTransactionJournal ptj 
		JOIN dbo.PatientReceivables pr ON pr.ReceivableId = ptj.TransactionTypeId
		JOIN model.Invoices i ON i.EncounterId = pr.AppointmentId
		JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
			AND ir.PatientInsuranceId IS NULL
		JOIN model.BillingServiceTransactions bst ON bst.InvoiceReceivableId = ir.Id
			AND bst.BillingServiceTransactionStatusId IN (1,2,3,4)
		WHERE TransactionType = 'R'
			AND TransactionDate >= '20140618'
			AND TransactionStatus <> 'Z'
			AND TransactionRef = 'P'
			AND ISNUMERIC(RTRIM(SUBSTRING(TransactionRemark,
			CHARINDEX('/Amt/',TransactionRemark)+5,
			LEN(TransactionRemark)))) = 1 
		GROUP BY 
		TransactionId
		,TransactionRemark
	)v 
	WHERE PtjAmountSent <> CorrectTransactionRemark
)z ON z.TransactionId = ptj.TransactionId