﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Xml;
using FluentMigrator;
using IO.Practiceware.Configuration;

namespace IO.Practiceware.DbMigration.Migrations._2014.Q2
{
    [Migration(201406201100)]
    public class Migration201406201100 : NonFluentTransactionalUpOnlyMigration
    {

        private const string CustomConfigFile = @"IO.Practiceware.Custom.config";
        private const string InsertQuery = @"
IF NOT EXISTS(SELECT * FROM model.ApplicationSettings WHERE Name = 'MessagingConfiguration')
BEGIN
    INSERT INTO model.ApplicationSettings (Value, Name, ApplicationSettingTypeId) VALUES ('{0}', 'MessagingConfiguration', {1})
END
";
        
        protected override void MigrateUp(IDbConnection connection, IDbTransaction transaction)
        {
            var customConfigFilePath = Path.Combine(Path.GetDirectoryName(ConfigurationManager.Configuration.FilePath) ?? string.Empty, CustomConfigFile);
            Trace.TraceInformation("Custom Config File For the Server Installer should be at: {0}", customConfigFilePath);
            var xDoc = new XmlDocument();
            xDoc.Load(customConfigFilePath);
                
            XmlNode configNode = xDoc.SelectSingleNode("//configuration");
            if (configNode != null)
            {
                Trace.TraceInformation("Configuration Node found In Server Installer Custom Config file");
                //Remove End Points in Service Model Node
                XmlNode messagingNode = xDoc.SelectSingleNode("//configuration/messagingConfiguration");

                if (messagingNode != null)
                {
                    Trace.TraceInformation("Messaging node found For Server Installer Custom Config File");
                    using (var command = connection.CreateCommand())
                    {
                        command.Transaction = transaction;
                        command.CommandText = String.Format(InsertQuery, messagingNode.OuterXml, 14);
                        command.ExecuteNonQuery();
                        Trace.TraceInformation("Messaging Node inserted into database");
                    }
                    configNode.RemoveChild(messagingNode);
                    Trace.TraceInformation("Messaging Node removed from configuration file");
                    xDoc.Save(customConfigFilePath); // save the config file 
                }
                else
                {
                    Trace.TraceInformation("Messaging node not found For Server Installer Custom Config File");
                }
            }
            else
            {
                Trace.TraceInformation("Configuration Node Found In Server Custom Config File");
            }
        }
    }
}
