﻿--Bug #11823.

--Delete all the Rows First
IF OBJECT_ID('dbo.CMSReports_Params') IS NOT NULL
BEGIN
	EXEC ('DELETE FROM dbo.CMSReports_Params WHERE CMSRepId IN(SELECT CMSRepId FROM [dbo].[CMSReports] WHERE ReportType = ''CMS'')')
END
GO
DELETE FROM [dbo].[CMSReports] WHERE ReportType='CMS'
GO
--Create Rows for dbo.CmsReports Table
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #012', N'1. #12 POAG: Optic Nerve Evaluation', N'USP_PQRI_GetPOAGDetails', N'Optic Nerve Evaluation', 1, N'Patients who have an optic nerve head evaluation during one or more office visits within 12 months', N'All patients aged 18 years and older with a diagnosis of primary open-angle glaucoma', N'PQRI', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #014', N'2. #14 AMD: Dilated Macular Examination', N'USP_PQRI_GetAMD_Dilated_Macular_Examination', N'Dilated Macular Examination', 1, N'Patients who had a dilated exam documentating macular thickening or hemorrhage plus severity of macular degeneration', N'All patients aged 50 years and older with a diagnosis of age-related macular degeneration', N'PQRI', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #018', N'3. #18 Diabetic Retinopathy: Documentation of Macular Edema and Severity of Retinopathy', N'USP_PQRI_GetDiabetic_Retinopathy_MacularEdema', N'Documentation of Macular Edema and Level of Severity of Retinopathy', 1, N'Patients who had a dilated exam documenting the severity of retinopathy AND the presence or absence of macular edema', N'All patients aged 18 years and older with diabetic retinopathy', N'PQRI', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #019', N'4. #19 Diabetic Retinopathy: Communication with the Physician Managing On-Diabetes Care', N'USP_PQRI_GetDiabetic_Retinopathy_Communication_Physician', N'Communication with the Physician Managing On-going Diabetes Care', 1, N'Patients with documentation of a dilated exam communicated to the physician managing the patient’s diabetic care', N'All patients aged 18 years and older with diabetic retinopathy who had a dilated macular or fundus exam performed', N'PQRI', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #117', N'5. #117 Diabetes Mellitus: Dilated Eye Exam in Diabetic Patient', N'USP_PQRI_Diabetes_GetMellitus_Dilated_Eye', N'Dilated Eye Exam in Diabetic Patient', 1, N'Patients who had a dilated eye exam for diabetic retinal disease at least once within 12 months', N'All patients aged 18 through 75 years with a diagnosis of diabetes', N'PQRI', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #140', N'7. #140 AMD: Counseling on Antioxidant Supplement', N'USP_PQRI_GetAMD_AREDS_Counseling', N'Counseling on Antioxidant Supplement', 1, N'Patients who were counseled within 12 months on the benefits and/or risks of the AREDS formulation', N'All patients aged 50 years and older with AMD', N'PQRI', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #141', N'8. #141 POAG: Reduction of IOP or Documentation of Plan', N'USP_PQRI_GetPOAG_ReducePressure', N'Reduction of Intraocular Pressure or Documentation of Plan of Care', 1, N'Patients whose most recent IOP was reduced by at least 15% or where a plan of care was documented', N'All patients aged 18 years and older with a diagnosis of primary open-angle glaucoma', N'PQRI', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #191', N'98. #191 Cataracts: 20/40 or Better Visual Acuity within 90 Days Following Cataract Surgery', N'USP_PQRI_GetCataracts_Visual_Acuity', N'20/40 or Better Visual Acuity within 90 Days Following Cataract Surgery', 1, N'Patients who had best-corrected visual acuity of 20/40 or better achieved within 90 days following cataract surgery', N'All patients aged 18 years and older without pre-operative ocular conditions who had cataract surgery', N'PQRI', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #192', N'99. #192 Cataracts: Complications within 30 Days Following Cataract Surgery Requiring Additional Surgery', N'USP_PQRI_GetCataracts_Complications', N'Complications within 30 Days of Cataract Surgery Requiring Additional Surgery', 1, N'Patients without complications within 30 days of cataract surgery', N'All patients aged 18 years and older without pre-operative ocular conditions who had cataract surgery', N'PQRI', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 001', N'Stage 1 - CORE 1: Computerized Provider Order Entry (CPOE)', N'model.USP_CMS_Stage1_CPOEMedications', N'Stage 1 - CORE 1: Computerized Provider Order Entry (CPOE)', 1, N'Number of patients in the denominator that have at least one medication order 
entered using CPOE', N'Number of unique patients with at least one medication in their medication list 
seen by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 001.1', N'Stage 1 - CORE 1: Computerized Provider Order Entry (CPOE) alternate', N'model.USP_CMS_Stage2_CPOEMedications', N'Stage 1 - CORE 1: Computerized Provider Order Entry (CPOE) alternate', 1, N'Number of medication orders in the denominator 
recorded using CPOE', N'Number of medication orders created by an 
EP during the EHR reporting period 
', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 003', N'Stage 1 - CORE 3: Maintain Problem List', N'model.USP_CMS_Stage1_ProblemList', N'Stage 1 - CORE 3: Maintain Problem List', 1, N'Number of patients in the denominator who have at least one entry or an 
indication that no problems are known for the patient recorded as structured data in their 
problem list', N'Number of unique patients seen by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 004', N'Stage 1 - CORE 4: e-Prescribing (eRx)', N'dbo.USP_CMS_GetEPrescStatus', N'Stage 1 - CORE 4: e-Prescribing (eRx)', 1, N'Number of prescriptions in the denominator generated and 
transmitted electronically 
', N'Number of prescriptions written for drugs requiring a 
prescription in order to be dispensed other than controlled substances 
during the EHR reporting period 
', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 005', N'Stage 1 - CORE 5: Active Medication List', N'model.USP_CMS_Stage1_MedicationList', N'Stage 1 - CORE 5: Active Medication List', 1, N'Number of patients in the denominator who have a medication 
(or an indication that the patient is not currently prescribed any medication) 
recorded as structured data 
', N'Number of unique patients seen by the EP during the EHR 
reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 006', N'Stage 1 - CORE 6: Medication Allergy List', N'model.USP_CMS_Stage1_MedicationAllergyList', N'Stage 1 - CORE 6: Medication Allergy List', 1, N'Number of unique patients in the denominator who have at 
least one entry (or an indication that the patient has no known medication 
allergies) recorded as structured data in their medication allergy list 
', N'Number of unique patients seen by the EP during the EHR 
report period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 007', N'Stage 1 - CORE 7: Record Demographics', N'model.USP_CMS_Stage1_Demographics', N'Stage 1 - CORE 7: Record Demographics', 1, N'Number of patients in the denominator who have all the 
elements of demographics (or a specific exclusion if the patient declined to 
provide one or more elements or if recording an element is contrary to state 
law) recorded as structured data', N'Number of unique patients seen by the EP during the EHR 
reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 008.21', N'Stage 1 - CORE 8: Record Vital Signs', N'model.USP_CMS_Stage2_VitalSigns', N'Stage 1 - CORE 8: Record Vital Signs', 1, N'Number of unique patients (age 3 or over for blood pressure) seen by the EP 
during the EHR reporting period', N'Number of patients in the denominator who have at least one entry of their 
height, weight and blood pressure (ages 3 and over) recorded as structured data. 
', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 008.22', N'Stage 1 - CORE 8: Record Vital Signs (Exclusion 1 or 3)', N'model.USP_CMS_Stage2_VitalSignsNoBP', N'Stage 1 - CORE 8: Record Vital Signs (Exclusion 1 or 3)', 1, N'Number of unique patients seen by the EP 
during the EHR reporting period', N'Number of patients in the denominator who have at least one entry of their 
height and weight recorded as structured data. 
', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 008.23', N'Stage 1 - CORE 8: Record Vital Signs (Exclusion 4)', N'model.USP_CMS_Stage2_VitalSignsNoHW', N'Stage 1 - CORE 8: Record Vital Signs (Exclusion 4)', 1, N'Number of unique patients age 3 or over seen by the EP 
during the EHR reporting period', N'Number of patients in the denominator who have at least one entry of their 
blood pressure recorded as structured data. 
', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 009', N'Stage 1 - CORE 9: Record Smoking Status', N'model.USP_CMS_Stage1_SmokingStatus', N'Stage 1 - CORE 9: Record Smoking Status', 1, N'Number of patients in the denominator with smoking status 
recorded as structured data', N'Number of unique patients age 13 or older seen by the EP 
during the EHR reporting period 
', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 012', N'Stage 1 - CORE 11: View, Download, Transmit Timely Access', N'model.USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess', N'Stage 1 - CORE 11: View, Download, Transmit Timely Access', 1, N'Number of patients in the denominator who have timely 
(within 4 business days after the information is available to the EP) online 
access to their health information to view, download, and transmit to a 
third party', N'Number of unique patients seen by the EP during the EHR 
reporting period 
', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 013', N'Stage 1 - CORE 12: Clinical Summaries', N'model.USP_CMS_Stage1_ClinicalSummary', N'Stage 1 - CORE 12: Clinical Summaries', 1, N'Number of office visits in the denominator for which the 
patient is provided a clinical summary within three business days', N'Number of office visits by the EP during the EHR reporting 
period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 022', N'Stage 1 - MENU 2: Clinical Lab-Test Results', N'model.USP_CMS_Stage1_ClinicalLabTestResults', N'Stage 1 - MENU 2: Clinical Lab-Test Results', 1, N'Number of lab test results whose results are expressed in a positive or negative 
affirmation or as a number which are incorporated as structured data', N'Number of lab tests ordered during the EHR reporting period by the EP whose 
results are expressed in a positive or negative affirmation or as a number', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 024', N'Stage 1 - MENU 4: Patient Reminders', N'model.USP_CMS_Stage1_PatientReminders', N'Stage 1 - MENU 4: Patient Reminders', 1, N'Number of patients in the denominator who were sent the appropriate reminder', N'Number of unique patients 65 years old or older or 5 years older or younger', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 026', N'Stage 1 - MENU 5: Patient-specific Education Resources', N'model.USP_CMS_Stage1_PatientEducation', N'Stage 1 - MENU 5: Patient-specific Education Resources', 1, N'Number of patients in the denominator who are provided patient-specific 
education resources', N'Number of unique patients seen by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 027', N'Stage 1 - MENU 6: Perform medication reconciliation for referral patients', N'model.USP_CMS_Stage2_MedicationReconciliation', N'Stage 1 - MENU 6: Perform medication reconciliation for referral patients', 1, N'Number of transitions of care in the denominator where medication 
reconciliation was performed', N'Number of transitions of care during the EHR reporting period for which the EP 
was the receiving party of the transition', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 028', N'Stage 1 - MENU 7: Summary of Care', N'model.USP_CMS_Stage1_SummaryOfCare', N'Stage 1 - MENU 7: Summary of Care', 1, N'Number of transitions of care and referrals in the denominator where a summary 
of care record was provided', N'Number of transitions of care and referrals during the EHR reporting period for 
which the EP was the transferring or referring provider', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 200', N'Stage 2 Reports are Below:', N'', N'Stage 2 Reports are Below:', 0, N'', N'', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 201.1', N'Stage 2 - CORE 1 Computerized Provider Order Entry (CPOE) Measure 1: Medication Orders', N'model.USP_CMS_Stage2_CPOEMedications', N'Stage 2 - CORE 1 Computerized Provider Order Entry (CPOE) Measure 1: Medication Orders', 1, N'The number of orders in the denominator recorded using CPOE', N'Number of medication orders created by the EP during the EHR reporting 
period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 201.2', N'Stage 2 - CORE 1: Computerized Provider Order Entry (CPOE) Measure 2: Radiology Orders', N'model.USP_CMS_Stage2_CPOERadiology', N'Stage 2 - CORE 1: Computerized Provider Order Entry (CPOE) Measure 2: Radiology Orders', 1, N'The number of orders in the denominator recorded using CPOE', N'Number of radiology orders created by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 201.3', N'Stage 2 - CORE 1: Computerized Provider Order Entry (CPOE) Measure 3: Laboratory Orders', N'model.USP_CMS_Stage2_CPOELaboratory', N'Stage 2 - CORE 1: Computerized Provider Order Entry (CPOE) Measure 3: Laboratory Orders', 1, N'The number of orders in the denominator recorded using CPOE', N'Number of laboratory orders created by the EP during the EHR reporting 
period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 202', N'Stage 2 - CORE 2: e-Prescribing (eRx)', N'model.USP_CMS_Stage2_eRx', N'Stage 2 - CORE 2: e-Prescribing (eRx)', 1, N': The number of prescriptions in the denominator generated, queried for a drug 
formulary and transmitted electronically using CEHRT', N'Number of prescriptions written for drugs requiring a prescription in order to 
be dispensed other than controlled substances during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 204.1', N'Stage 2 - CORE 4: Record Vital Signs', N'model.USP_CMS_Stage2_VitalSigns', N'Stage 2 - CORE 4: Record Vital Signs', 1, N'Number of patients in the denominator who have at least one entry of their 
height/length and weight (all ages) and/or blood pressure (ages 3 and over) recorded as 
structured data', N'Number of unique patients seen by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 204.2', N'Stage 2 - CORE 4: Record Vital Signs (Exclusion 1 or 3)', N'model.USP_CMS_Stage2_VitalSignsNoBP', N'Stage 2 - CORE 4: Record Vital Signs (Exclusion 1 or 3)', 1, N'Number of patients in the denominator who have at least one entry of their 
height/length and weight (all ages) recorded as 
structured data', N'Number of unique patients seen by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 204.3', N'Stage 2 - CORE 4: Record Vital Signs (Exclusion 4)', N'model.USP_CMS_Stage2_VitalSignsNoHW', N'Stage 2 - CORE 4: Record Vital Signs (Exclusion 4)', 1, N'Number of patients in the denominator who have at least one entry of their blood pressure (ages 3 and over) recorded as 
structured data', N'Number of unique patients seen by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 205', N'Stage 2 - CORE 5: Record Smoking Status', N'model.USP_CMS_Stage1_SmokingStatus', N'Stage 2 - CORE 5: Record Smoking Status', 1, N'The number of patients in the denominator with smoking status recorded as 
structured data', N'Number of unique patients age 13 or older seen by the EP during the EHR 
reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 203', N'Stage 2 - CORE 3: Record Demographics', N'model.USP_CMS_Stage1_Demographics', N'Stage 2 - CORE 3: Record Demographics', 1, N'The number of patients in the denominator who have all the elements of 
demographics (or a specific notation if the patient declined to provide one or more elements or 
if recording an element is contrary to state law) recorded as structured data', N'Number of unique patients seen by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 207.2', N'Stage 2 - CORE 7: Patient Electronic Access Measure 2: Patient Use', N'model.USP_CMS_Stage2_ViewDownloadTransmitPatientUse', N'Stage 2 - CORE 7: Patient Electronic Access Measure 2: Patient Use', 1, N'The number of unique patients (or their authorized representatives) in the 
denominator who have viewed online, downloaded, or transmitted to a third party the patient''s 
health information', N'Number of unique patients seen by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 208', N'Stage 2 - CORE 8: Clinical Summaries', N'model.USP_CMS_Stage2_ClinicalSummary', N'Stage 2 - CORE 8: Clinical Summaries', 1, N'The number of unique patients (or their authorized representatives) in the 
denominator who have viewed online, downloaded, or transmitted to a third party the patient''s 
health information', N'Number of office visits conducted by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 210', N'Stage 2 – CORE 10: Clinical Lab - Test Results', N'dbo.USP_CMS_HL7LabOrdersStats', N'Stage 2 – CORE 10: Clinical Lab - Test Results', 1, N'Number of lab test results which are expressed in a positive or negative 
affirmation or as a numeric result which are incorporated in CEHRT as structured data', N'Number of lab tests ordered during the EHR reporting period by the EP whose 
results are expressed in a positive or negative affirmation or as a number', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 212', N'Stage 2 - CORE 12: Patient Reminders', N'model.USP_CMS_Stage2_PatientReminders', N'Stage 2 - CORE 12: Patient Reminders', 1, N'Number of patients in the denominator who were sent a reminder per patient 
preference when available during the EHR reporting period', N'Number of unique patients who have had two or more office visits with the EP 
in the 24 months prior to the beginning of the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 213', N'Stage 2 – CORE 13: Patient - Specific Education Resources', N'model.USP_CMS_Stage1_PatientEducation', N'Stage 2 – CORE 13: Patient - Specific Education Resources', 1, N'Number of patients in the denominator who were provided patient-specific 
education resources identified by the Certified EHR Technology', N'Number of unique patients with office visits seen by the EP during the EHR 
reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 214', N'Stage 2 – CORE 14: Medication Reconciliation', N'dbo.USP_CMS_GetMedicationReconStatus_Stage2', N'Stage 2 – CORE 14: Medication Reconciliation', 1, N'The number of transitions of care in the denominator where medication 
reconciliation was performed', N'Number of transitions of care during the EHR reporting period for which the EP 
was the receiving party of the transition', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 215.1', N'Stage 2 – CORE 15: Summary of Care Measure 1: Provide Records', N'model.USP_CMS_Stage1_SummaryOfCare', N'Stage 2 – CORE 15: Summary of Care Measure 1: Provide Records', 1, N'The number of transitions of care and referrals in the denominator where a 
summary of care record was provided', N'Number of transitions of care and referrals during the EHR reporting period for 
which the EP was the transferring or referring provider', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 215.2', N'Stage 2 - CORE 15: Summary of Care Measure 2: Provide records via Direct', N'model.USP_CMS_Stage2_SummaryOfCareB', N'Stage 2 - CORE 15: Summary of Care Measure 2: Provide records via Direct', 1, N'The number of transitions of care and referrals in the denominator where a 
summary of care record was electronically transmitted using CEHRT to a recipient', N'Number of transitions of care and referrals during the EHR reporting period for 
which the EP was the transferring or referring provider', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 217', N'Stage 2 - CORE 17: Use Secure Electronic Messaging', N'model.USP_CMS_Stage2_SecureMessage', N'Stage 2 - CORE 17: Use Secure Electronic Messaging', 1, N'The number of patients or patient-authorized representatives in the 
denominator who send a secure electronic message to the EP that is received using the 
electronic messaging function of CEHRT during the EHR reporting period', N'Number of unique patients seen by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 222', N'Stage 2 - MENU 2: Electronic Notes', N'model.USP_CMS_Stage2_ElectronicNotes', N'Stage 2 - MENU 2: Electronic Notes', 1, N'The number of unique patients in the denominator who have at least one 
electronic progress note from an eligible professional recorded as text searchable data', N'Number of unique patients with at least one office visit during the EHR 
reporting period for EPs during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 224', N'Stage 2 - MENU 4: Family Health History', N'model.USP_CMS_Stage2_FamilyHistory', N'Stage 2 - MENU 4: Family Health History', 1, N'The number of patients in the denominator with a structured data entry for one 
or more first-degree relatives', N'Number of unique patients seen by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 223', N'Stage 2 - MENU 3: Imaging results', N'model.USP_CMS_Stage2_Imaging', N'Stage 2 - MENU 3: Imaging results', 1, N'The number of results in the denominator that are accessible through CEHRT', N'Number of tests whose result is one or more images ordered by the EP during 
the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 207.1', N'Stage 2 - CORE 7: Patient Electronic Access Measure 1: Timely Access', N'model.USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess', N'Stage 2 - CORE 7: Patient Electronic Access Measure 1: Timely Access', 1, N'The number of patients in the denominator who have timely (within 4 business 
days after the information is available to the EP) online access to their health information to 
view, download, and transmit to a third party', N'Number of unique patients seen by the EP during the EHR reporting period', N'CMS', 1)
GO
INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'CPOE 999', N'Initial Patient Population', N'model.USP_CMS_InitialPatientPopulation', N'Initial Patient Population', 1, N'Number of unique patients during the reporting period', N'Number of visits during the EHR reporting period', N'CMS', 0)
GO

--StoredProcedures

--model.USP_CMS_Stage1_CPOEMedications

IF (OBJECT_ID('model.USP_CMS_Stage1_CPOEMedications', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage1_CPOEMedications
END
GO
--exec model.USP_CMS_Stage1_CPOEMedications '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage1_CPOEMedications] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT DISTINCT ipp.PatientId
		,ipp.EncounterId
		,ipp.EncounterDate
		,pm.id AS PatientMedicationId
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
	INNER JOIN PatientMedications pm ON pm.PatientId = ipp.PatientId

	--numerator  
	SELECT DISTINCT d.PatientId
		,pm.Id AS PatientMedicationId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientMedications pm ON pm.PatientId = d.PatientId

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
		,PatientMedicationId
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.PatientMedicationId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.PatientMedicationId

	DROP TABLE #InitialPatientPopulation

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_CPOEMedications

IF (OBJECT_ID('model.USP_CMS_Stage2_CPOEMedications', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_CPOEMedications
END
GO
--exec model.USP_CMS_Stage2_CPOEMedications '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage2_CPOEMedications] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT DISTINCT ipp.PatientId
		,ipp.EncounterId
		,ipp.EncounterDate
		,pm.id AS PatientMedicationId
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
	INNER JOIN PatientMedications pm ON pm.PatientId = ipp.PatientId

	--numerator  
	SELECT DISTINCT d.PatientId
		,pm.Id AS PatientMedicationId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientMedications pm ON pm.PatientId = d.PatientId

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientMedicationID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientMedicationID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
		,PatientMedicationId
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.PatientMedicationId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.PatientMedicationId

	DROP TABLE #InitialPatientPopulation

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage1_ProblemList

IF (OBJECT_ID('model.USP_CMS_Stage1_ProblemList', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage1_ProblemList
END
GO
--exec model.USP_CMS_Stage1_ProblemList '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage1_ProblemList] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation ipp

	--Numerator    
	SELECT DISTINCT pd.PatientId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.PatientDiagnoses pd ON pd.PatientId = d.PatientId
	INNER JOIN model.PatientDiagnosisDetails pdd ON pd.id = pdd.PatientDiagnosisId
		AND IsOnProblemList = 1

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId

	DROP TABLE #InitialPatientPopulation

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--dbo.USP_CMS_GetEPrescStatus

IF (OBJECT_ID('dbo.USP_CMS_GetEPrescStatus', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE dbo.USP_CMS_GetEPrescStatus
END
GO

CREATE PROCEDURE [dbo].[USP_CMS_GetEPrescStatus] (
	@StartDate NVARCHAR(200)
	,@EndDate NVARCHAR(200)
	,@ResourceIds NVARCHAR(1000)
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	CREATE TABLE #TMPResources (ResourceId NVARCHAR(1000))

	IF @ResourceIds = ''
	BEGIN
		INSERT INTO #TMPResources
		SELECT Resources.ResourceId
		FROM Resources
		WHERE ResourceType IN (
				'D'
				,'Q'
				)
	END
	ELSE
	BEGIN
		DECLARE @TmpResourceIds VARCHAR(100)
		DECLARE @ResId VARCHAR(10)

		SELECT @TmpResourceIds = @ResourceIds

		WHILE PATINDEX('%,%', @TmpResourceIds) <> 0
		BEGIN
			SELECT @ResId = LEFT(@TmpResourceIds, PATINDEX('%,%', @TmpResourceIds) - 1)

			SELECT @TmpResourceIds = SUBSTRING(@TmpResourceIds, PATINDEX('%,%', @TmpResourceIds) + 1, LEN(@TmpResourceIds))

			INSERT INTO #TMPResources
			VALUES (@ResId)
		END

		INSERT INTO #TMPResources
		VALUES (@TmpResourceIds)
	END

	--denominator before exclusions
	SELECT AppDate
		,pc.PatientId
		,FindingDetail
		,ImageDescriptor
		,ImageInstructions
		,pc.ClinicalId
	INTO #rx
	FROM Appointments ap
	INNER JOIN PatientDemographics pd ON pd.PatientId = ap.PatientId
		AND pd.LastName <> 'TEST'
	INNER JOIN Resources re ON re.ResourceId = ap.ResourceId1
		AND re.ResourceId IN (
			SELECT #TMPResources.ResourceId
			FROM #TMPResources
			)
	INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
	WHERE AppDate BETWEEN @StartDate
			AND @EndDate
		AND pc.ClinicalType = 'A'
		AND pc.STATUS = 'A'
		AND pc.FindingDetail LIKE 'RX-%/P-7/%'

	--exclude changed rx's
	---changed
	SELECT r.clinicalid
	INTO #exclusions
	FROM #rx r
	INNER JOIN Appointments apChange ON apChange.PatientId = r.PatientId
		AND apChange.AppDate < r.AppDate
	INNER JOIN PatientClinical pcChange ON pcChange.AppointmentId = apChange.AppointmentId
		AND pcChange.ClinicalType = 'A'
		AND SUBSTRING(pcchange.FindingDetail, 6, CHARINDEX('-2/', pcchange.FindingDetail) - 6) = SUBSTRING(r.FindingDetail, 6, CHARINDEX('-2/', r.FindingDetail) - 6)
		AND SUBSTRING(pcchange.Imagedescriptor, 1, 1) = 'C'
	GROUP BY r.ClinicalId
	
	UNION
	
	--otc
	SELECT r.ClinicalId
	FROM #rx r
	INNER JOIN fdb.tblcompositedrug t ON t.MED_ROUTED_DF_MED_ID_DESC = SUBSTRING(r.FindingDetail, 6, CHARINDEX('-2/', FindingDetail) - 6)
	WHERE MED_REF_FED_LEGEND_IND_DESC = 'OTC'
	GROUP BY r.ClinicalId

	--Denominator
	SELECT r.ClinicalId
		,r.ImageInstructions
	INTO #denominator
	FROM #rx r
	LEFT JOIN #exclusions e ON e.Clinicalid = r.Clinicalid
	WHERE e.ClinicalId IS NULL

	--numerator
	SELECT d.ClinicalId
	INTO #Numerator
	FROM #denominator d
	WHERE len(d.ImageInstructions) > 10

	SELECT @TotParm = COUNT(*)
	FROM #Denominator

	SELECT @CountParm = COUNT(*)
	FROM #Numerator

	DROP TABLE #TMPResources

	DROP TABLE #RX

	DROP TABLE #Exclusions

	DROP TABLE #Denominator

	DROP TABLE #Numerator

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator
END
GO

--model.USP_CMS_Stage1_MedicationList

IF (OBJECT_ID('model.USP_CMS_Stage1_MedicationList', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage1_MedicationList
END
GO
--exec [model].[USP_CMS_Stage1_MedicationList] '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage1_MedicationList] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	--Numerator
	SELECT d.PatientId
		,pm.Id AS PatientMedicationId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientMedications pm ON pm.PatientId = d.PatientId

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)

	--If they have MU ON, we will ask the physician if the patient is taking any meds. This can be activated if the practice had MUON then entire reporting period
	--IF (select FieldValue from dbo.PracticeInterfaceConfiguration where FieldReference = 'MUON') = 'T'
	--BEGIN SET @CountParm = @TotParm
	--END
	--ELSE
	BEGIN
		SET @CountParm = (
				SELECT COUNT(DISTINCT PatientID)
				FROM #Numerator
				)
	END

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.PatientMedicationId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.PatientMedicationId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage1_MedicationAllergyList
IF (OBJECT_ID('model.USP_CMS_Stage1_MedicationAllergyList', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage1_MedicationAllergyList
END
GO
--exec [model].[USP_CMS_Stage1_MedicationAllergyList] '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage1_MedicationAllergyList] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation ipp

	--Numerator  
	SELECT d.PatientId
		,pa.Id AS PatientAllergenId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientAllergens pa ON pa.PatientId = d.PatientId
	
	UNION ALL
	
	SELECT d.PatientId
		,0 AS PatientAllergenId
	FROM #Denominator d
	INNER JOIN PatientClinical pc ON pc.PatientId = d.PatientId
	WHERE FindingDetail LIKE '%NONE%'
		AND Symptom LIKE '/ANY DRUG ALLERGIES'

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.PatientAllergenId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.PatientAllergenId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage1_Demographics
IF (OBJECT_ID('model.USP_CMS_Stage1_Demographics', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage1_Demographics
END
GO
--exec [model].[USP_CMS_Stage1_Demographics] '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage1_Demographics] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	--Numerator    
	SELECT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.Patients p ON p.Id = d.PatientId
	INNER JOIN model.PatientRace ppr ON ppr.Patients_Id = p.Id
	WHERE p.DateOfBirth IS NOT NULL
		AND p.genderid IS NOT NULL
		AND p.EthnicityId IS NOT NULL
		AND p.LanguageId IS NOT NULL

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_VitalSigns

IF (OBJECT_ID('model.USP_CMS_Stage2_VitalSigns', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_VitalSigns
END
GO
CREATE PROCEDURE [model].[USP_CMS_Stage2_VitalSigns] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	--Numerator    
	SELECT DISTINCT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.PatientHeightAndWeights hw ON hw.PatientId = d.PatientId
	INNER JOIN model.PatientBloodPressures bp ON bp.PatientId = d.PatientId
	WHERE (
			HeightUnit IS NOT NULL
			AND WeightUnit IS NOT NULL
			AND SystolicPressure IS NOT NULL
			AND DiastolicPressure IS NOT NULL
			)
		AND Age > 2
	
	UNION ALL
	
	SELECT DISTINCT d.*
	FROM #Denominator d
	INNER JOIN model.PatientHeightAndWeights hw ON hw.PatientId = d.PatientId
	WHERE (
			HeightUnit IS NOT NULL
			AND WeightUnit IS NOT NULL
			)
		AND Age <= 2

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_VitalSignsNoBP
IF (OBJECT_ID('model.USP_CMS_Stage2_VitalSignsNoBP', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_VitalSignsNoBP
END
GO
--exec [model].[USP_CMS_Stage2_VitalSignsNoBP] '12/15/2013', '03/15/2014', 192
CREATE PROCEDURE [model].[USP_CMS_Stage2_VitalSignsNoBP] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	--Numerator    
	SELECT DISTINCT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.PatientHeightAndWeights hw ON hw.PatientId = d.PatientId
	WHERE (
			HeightUnit IS NOT NULL
			AND WeightUnit IS NOT NULL
			)

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_VitalSignsNoHW
IF (OBJECT_ID('model.USP_CMS_Stage2_VitalSignsNoHW', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_VitalSignsNoHW
END
GO
--exec [model].[USP_CMS_Stage2_VitalSignsNoHW] '12/15/2013', '03/15/2014', 192
CREATE PROCEDURE [model].[USP_CMS_Stage2_VitalSignsNoHW] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation
	WHERE Age > 2

	--Numerator    
	SELECT DISTINCT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.PatientBloodPressures bp ON bp.PatientId = d.PatientId
	WHERE (
			SystolicPressure IS NOT NULL
			AND DiastolicPressure IS NOT NULL
			)

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage1_SmokingStatus

IF (OBJECT_ID('model.USP_CMS_Stage1_SmokingStatus', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage1_SmokingStatus
END
GO
--exec model.USP_CMS_Stage1_SmokingStatus '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage1_SmokingStatus] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation
	WHERE Age > 12

	--Numerator    
	SELECT DISTINCT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.PatientSmokingStatus pss ON pss.PatientId = d.PatientId

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess
IF (OBJECT_ID('model.USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess
END
GO
--exec [model].[USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess] '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	IF @ResourceIds LIKE '%,%'
		RAISERROR (
				'Please ONLY send one doctor id at a TIME.'
				,16
				,2
				)

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	--Numerator    
	SELECT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpre ON esmpre.PracticeRepositoryEntityKey = d.PatientId
	INNER JOIN model.ExternalSystemMessages esm ON esm.id = esmpre.ExternalSystemMessageId
	WHERE ExternalSystemExternalSystemMessageTypeId IN (
			SELECT id
			FROM model.ExternalSystemExternalSystemMessageTypes
			WHERE ExternalSystemId IN (
					SELECT Id
					FROM model.ExternalSystems
					WHERE NAME = 'Omedix'
					)
				AND ExternalSystemMessageTypeId IN (
					SELECT id
					FROM model.ExternalSystemMessageTypes
					WHERE NAME = 'PatientTransitionOfCareCCDA'
						AND IsOutbound = 1
					)
			)
		AND DATEDIFF(dd, d.EncounterDate, [IO.Practiceware.SqlClr].ConvertToClientDateTime([IO.Practiceware.SqlClr].ConvertToLocalDateTime(esm.CreatedDateTime))) <= 4

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage1_ClinicalSummary
IF (OBJECT_ID('model.USP_CMS_Stage1_ClinicalSummary', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage1_ClinicalSummary
END
GO
--exec [model].[USP_CMS_Stage1_ClinicalSummary] '10/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage1_ClinicalSummary] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	IF @ResourceIds LIKE '%,%'
		RAISERROR (
				'Please select only one provider at a TIME.'
				,16
				,2
				)

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;
	DECLARE @Status NVARCHAR(1)

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	--Numerator 
	----1.  Is autoprinting on?  If so, 100%
	--SELECT @Status=FieldValue 
	--FROM PracticeInterfaceConfiguration 
	--WHERE FieldReference = 'PRINT'
	--SELECT @TotParm = COUNT(DISTINCT EncounterId)
	--FROM #Denominator
	--IF @Status='T'  
	--	BEGIN		  
	--		SELECT @CountParm = @TotParm
	--	END 
	--ELSE 
	--	BEGIN
	-- Patient Declines
	SELECT d.*
		,CONVERT(NVARCHAR(100), ClinicalId) AS ClinicalSummaryId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientClinical pc ON pc.AppointmentId = d.EncounterId
	WHERE ClinicalType = 'A'
		AND FindingDetail LIKE 'CLIN SUMMARY, PATIENT DECLINES%'
	
	UNION ALL
	
	-- Clinical Data Files
	-- portal, print, or save as
	SELECT d.*
		,CONVERT(NVARCHAR(100), esm.Id) AS ClinicalSummaryId
	FROM #Denominator d
	INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpre ON esmpre.PracticeRepositoryEntityKey = d.EncounterId
	INNER JOIN model.ExternalSystemMessages esm ON esm.id = esmpre.ExternalSystemMessageId
	INNER JOIN model.ExternalSystemExternalSystemMessageTypes esesmt ON esesmt.id = esm.ExternalSystemExternalSystemMessageTypeId
	INNER JOIN model.ExternalSystemMessageTypes esmt ON esmt.Id = esesmt.ExternalSystemMessageTypeId
		AND esmt.NAME = 'EncounterClinicalSummaryCCDA'
		AND IsOutbound = 1
	INNER JOIN model.Appointments ap ON ap.Id = esmpre.PracticeRepositoryEntityKey
		AND DATEDIFF(dd, ap.DATETIME, [IO.Practiceware.SqlClr].ConvertToClientDateTime([IO.Practiceware.SqlClr].ConvertToLocalDateTime(esm.CreatedDateTime))) <= 3
	
	UNION ALL
	
	-- 7.2 letters
	SELECT d.*
		,CONVERT(NVARCHAR(100), lep.Id) AS ClinicalSummaryId
	FROM #Denominator d
	INNER JOIN LogEntryProperties LEP ON LEP.Value = d.EncounterId
	INNER JOIN LogEntries LE ON LE.Id = LEP.LogEntryId
		AND LEP.NAME = 'AppointmentId'
	WHERE Message LIKE 'Printed misc ltr%Clin%Sum%'
		AND DATEDIFF(dd, d.EncounterDate, [IO.Practiceware.SqlClr].ConvertToClientDateTime([IO.Practiceware.SqlClr].ConvertToLocalDateTime(le.DATETIME))) <= 3

	SET @CountParm = (
			SELECT COUNT(DISTINCT EncounterId)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT EncounterId)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.EncounterId
		,n.EncounterDate
		,n.ClinicalSummaryId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.EncounterDate
		,n.EncounterId

	DROP TABLE #Numerator

	DROP TABLE #Denominator
END
GO

--model.USP_CMS_Stage1_ClinicalLabTestResults
IF (OBJECT_ID('model.USP_CMS_Stage1_ClinicalLabTestResults', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage1_ClinicalLabTestResults
END
GO
--exec [model].[USP_CMS_Stage1_ClinicalLabTestResults]  '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage1_ClinicalLabTestResults] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Denominator
	SELECT DISTINCT hl7.ReportID AS LaboratoryTestOrderId
		,hl7.PatientID AS PatientId
		,hl7.ReportDate AS OrderDate
	INTO #Denominator
	FROM HL7_LabReports hl7
	WHERE hl7.ReportDate BETWEEN @StartDate
			AND @EndDate
		AND hl7.ProviderID = @ResourceIds

	--numerator  
	SELECT d.PatientId
		,d.LaboratoryTestOrderId
		,d.OrderDate
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN HL7_Observation orderdetails ON orderdetails.ReportID = d.LaboratoryTestOrderId
	INNER JOIN HL7_Observation_Details results ON results.OBRID = orderdetails.OBRID

	SET @TotParm = (
			SELECT COUNT(DISTINCT LaboratoryTestOrderId)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT LaboratoryTestOrderId)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.OrderDate
		,d.LaboratoryTestOrderId
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.OrderDate
		,d.LaboratoryTestOrderId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.OrderDate
		,n.LaboratoryTestOrderId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.OrderDate
		,n.LaboratoryTestOrderId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage1_PatientReminders
IF (OBJECT_ID('model.USP_CMS_Stage1_PatientReminders', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage1_PatientReminders
END
GO
--exec [model].[USP_CMS_Stage1_PatientReminders] '01/01/2013', '12/31/13', 192
CREATE PROCEDURE [model].[USP_CMS_Stage1_PatientReminders] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	--Denominator
	SELECT DISTINCT P.Id AS PatientId
	INTO #Denominator
	FROM model.Patients P
	INNER JOIN PatientClinical PC ON PC.PatientId = P.Id
		AND ClinicalType = 'Q' --This determines if their records are mantained in the EHR
	WHERE DATEDIFF(YY, CONVERT(DATETIME, DateOfBirth), CONVERT(DATETIME, @EndDate)) >= 65
		OR DATEDIFF(YY, CONVERT(DATETIME, DateOfBirth), CONVERT(DATETIME, @EndDate)) <= 5
		AND PatientStatusId = 1
		AND p.DefaultUserId = @ResourceIds

	--Numerator
	--Recalls
	SELECT d.PatientId
		,ptj.TransactionID AS ReminderId
		,CONVERT(DATETIME, TransactionDate) AS ReminderSentDate
		,'RECALL' AS Type
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PracticeTransactionJournal PTJ ON ptj.TransactionTypeId = d.PatientId
	INNER JOIN Resources re ON re.ResourceName = REVERSE(SUBSTRING(REVERSE(PTJ.TransactionRef), 0, CHARINDEX('/', REVERSE(PTJ.TransactionRef), 0)))
	WHERE PTJ.TransactionType = 'L'
		AND PTJ.TransactionStatus = 'S'
		AND ptj.transactiontypeid = d.PatientId
		AND re.ResourceId = @ResourceIds
		AND CONVERT(DATETIME, TransactionDate) BETWEEN @StartDate
			AND @EndDate
	
	UNION ALL
	
	SELECT D.PatientId
		,Id AS ReminderId
		,NULL AS ReminderSentDate
		,'CONFIRMATION' AS Type
	FROM #Denominator D
	INNER JOIN model.Encounters E ON E.PatientId = D.PatientId
	WHERE ConfirmationStatusId IS NOT NULL

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.ReminderId
		,n.ReminderSentDate
		,Type
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.ReminderSentDate

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage1_PatientEducation
IF (OBJECT_ID('model.USP_CMS_Stage1_PatientEducation', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage1_PatientEducation
END
GO
--exec [model].[USP_CMS_Stage1_PatientEducation]    '01/01/2000', '12/31/2015'
CREATE PROCEDURE [model].[USP_CMS_Stage1_PatientEducation] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	--Numerator    
	SELECT DISTINCT d.*
		,'Omedix' AS Type
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	INNER JOIN model.Gender_Enumeration g ON g.Id = p.GenderId
	INNER JOIN [IO.Practiceware.SqlClr].GetNumeratorDetailsForPatientEducation(@StartDate, @EndDate, @ResourceIds, 'OMEDIX') pe ON pe.PatientDob = p.DateOfBirth
		AND pe.patientFirstName = p.FirstName
		AND pe.patientLastName = p.LastName
		AND pe.patientGender = SUBSTRING(g.NAME, 1, 1)
	
	UNION ALL
	
	SELECT d.*
		,'Written Instructions' AS Type
	FROM #Denominator D
	INNER JOIN PatientClinical pc ON pc.AppointmentId = d.EncounterId
	WHERE ClinicalType = 'A'
		AND SUBSTRING(FindingDetail, 1, 20) = 'WRITTEN INSTRUCTIONS'
		AND STATUS = 'A'

	SELECT @TotParm = COUNT(DISTINCT PatientId)
	FROM #Denominator

	SELECT @CountParm = COUNT(DISTINCT PatientID)
	FROM #Numerator

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
		,Type
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_MedicationReconciliation
IF (OBJECT_ID('model.USP_CMS_Stage2_MedicationReconciliation', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_MedicationReconciliation
END
GO
--exec model.USP_CMS_Stage2_MedicationReconciliation '2013-12-15', '2014-03-15', 192
CREATE PROCEDURE [model].[USP_CMS_Stage2_MedicationReconciliation] (
	@StartDate NVARCHAR(200)
	,@EndDate NVARCHAR(200)
	,@ResourceIds NVARCHAR(1000)
	)
AS
BEGIN
	---#15-Medication Reconclication--------------------------------------------------
	DECLARE @TotParm INT
	DECLARE @CountParm INT

	SET @TotParm = 0
	SET @CountParm = 0

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT ipp.*
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
	INNER JOIN PatientClinical pc ON pc.AppointmentId = ipp.EncounterId
	WHERE Symptom = '/MEDS RECONCILIATION'
		AND FindingDetail IN (
			'*SUMMARYOFCARENONELECTRONIC-MEDREC=T23294 <SUMMARYOFCARENONELECTRONIC-MEDREC>'
			,'*SUMMARYOFCAREELECTRONIC-MEDREC=T23293 <SUMMARYOFCAREELECTRONIC-MEDREC>'
			)

	--Numerator
	SELECT d.EncounterId
		,pc.PatientId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientClinical pc ON pc.AppointmentId = d.EncounterId
	WHERE Symptom = '/MEDS RECONCILIATION'
		AND FindingDetail = '*MEDRECPERFORMED-MEDREC=T14913 <MEDRECPERFORMED-MEDREC>'

	SET @TotParm = (
			SELECT COUNT(DISTINCT EncounterId)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT EncounterId)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.EncounterId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage1_SummaryOfCare
IF (OBJECT_ID('model.USP_CMS_Stage1_SummaryOfCare', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage1_SummaryOfCare
END
GO
--exec [model].[USP_CMS_Stage1_SummaryOfCare] '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage1_SummaryOfCare] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	DECLARE @userId NVARCHAR(20)
	DECLARE @userType NVARCHAR(20)
	DECLARE @transactionid NVARCHAR(max)
	DECLARE @SummaryOfCareNumerator INT

	SET @userId = 'demo'
	SET @userType = 'D'

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT ipp.*
		,etoco.Id AS TransitionOfCareOrderId
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
	INNER JOIN model.EncounterTransitionOfCareOrders etoco ON etoco.EncounterId = ipp.EncounterId

	--Numerator
	--Letters printed
	SELECT d.*
		,SentDateTime AS SendDate
		,'Letter Printed' AS Type
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.EncounterTransitionOfCareOrders etoco ON etoco.id = d.TransitionOfCareOrderId
		AND SentDateTime IS NOT NULL
	
	UNION ALL
	
	--C-CDAs Printed
	SELECT d.*
		,CreatedDateTime AS SendDate
		,'C-CDA Printed' AS Type
	FROM #Denominator d
	INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpre ON esmpre.PracticeRepositoryEntityKey = d.PatientId
	INNER JOIN model.ExternalSystemMessages esm ON esm.id = esmpre.ExternalSystemMessageId
	WHERE ExternalSystemExternalSystemMessageTypeId IN (
			SELECT id
			FROM model.ExternalSystemExternalSystemMessageTypes
			WHERE ExternalSystemId IN (
					SELECT Id
					FROM model.ExternalSystems
					WHERE NAME = 'Printer'
					)
				AND ExternalSystemMessageTypeId IN (
					SELECT id
					FROM model.ExternalSystemMessageTypes
					WHERE NAME = 'PatientTransitionOfCareCCDA'
						AND IsOutbound = 1
					)
			)
	
	UNION ALL
	
	--C-CDAs Saved
	SELECT d.*
		,CreatedDateTime AS SendDate
		,'C-CDA Saved' AS Type
	FROM #Denominator d
	INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpre ON esmpre.PracticeRepositoryEntityKey = d.PatientId
	INNER JOIN model.ExternalSystemMessages esm ON esm.id = esmpre.ExternalSystemMessageId
	WHERE ExternalSystemExternalSystemMessageTypeId IN (
			SELECT id
			FROM model.ExternalSystemExternalSystemMessageTypes
			WHERE ExternalSystemId IN (
					SELECT Id
					FROM model.ExternalSystems
					WHERE NAME = 'Saved'
					)
				AND ExternalSystemMessageTypeId IN (
					SELECT id
					FROM model.ExternalSystemMessageTypes
					WHERE NAME = 'PatientTransitionOfCareCCDA'
						AND IsOutbound = 1
					)
			)

	SELECT @TotParm = COUNT(DISTINCT TransitionOfCareOrderId)
	FROM #Denominator

	SELECT @CountParm = COUNT(DISTINCT TransitionOfCareOrderId)
	FROM #Numerator

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
		,d.TransitionOfCareOrderId
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
		,d.TransitionOfCareOrderId
		,d.SendDate
		,Type
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_CPOERadiology
IF (OBJECT_ID('model.USP_CMS_Stage2_CPOERadiology', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_CPOERadiology
END
GO
CREATE PROCEDURE [model].[USP_CMS_Stage2_CPOERadiology] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Denominator
	SELECT e.PatientId
		,e.id AS EncounterId
		,edto.Id AS DiagnosticTestOrderId
		,a.DATETIME AS OrderDate
	INTO #Denominator
	FROM model.EncounterDiagnosticTestOrders edto
	INNER JOIN model.encounters e ON e.id = edto.EncounterId
	INNER JOIN model.Appointments a ON a.EncounterId = e.Id
	INNER JOIN model.DiagnosticTestOrders dto ON dto.id = edto.DiagnosticTestOrderId
	INNER JOIN dbo.PracticeCodeTable pct ON pct.Id = dto.Id
	INNER JOIN dbo.PracticeCodeTable pct2 ON pct.TestOrder = pct2.AlternateCode
		AND pct2.ReferenceType = 'SCANTYPE'
	WHERE a.DATETIME BETWEEN @StartDate
			AND DATEADD(mi, 1440, @EndDate) --DATEADD so that EndDate includes the whole day of encounters
		AND e.EncounterStatusId = 7
		AND a.UserId = @ResourceIds

	--numerator  
	SELECT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.EncounterDiagnosticTestOrders edto ON edto.id = d.DiagnosticTestOrderId

	SET @TotParm = (
			SELECT COUNT(DISTINCT DiagnosticTestOrderId)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT DiagnosticTestOrderId)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.OrderDate
		,d.DiagnosticTestOrderId
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterId
		,d.OrderDate
		,d.DiagnosticTestOrderId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.EncounterId
		,n.OrderDate
		,n.DiagnosticTestOrderId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.EncounterId
		,n.OrderDate
		,n.DiagnosticTestOrderId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_CPOELaboratory
IF (OBJECT_ID('model.USP_CMS_Stage2_CPOELaboratory', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_CPOELaboratory
END
GO
CREATE PROCEDURE [model].[USP_CMS_Stage2_CPOELaboratory] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Denominator
	--select e.PatientId, e.id as EncounterId, elto.Id as LaboratoryTestOrderId, a.datetime as OrderDate
	--into #Denominator
	--from model.EncounterLaboratoryTestOrders elto
	--inner join model.encounters e on e.id = elto.EncounterId
	--inner join model.Appointments a on a.EncounterId = e.Id
	--where a.DateTime between @StartDate and DATEADD(mi,1440,@EndDate) --DATEADD so that EndDate includes the whole day of encounters
	--and e.EncounterStatusId = 7
	--and a.UserId = @ResourceIds
	SELECT DISTINCT hl7.ReportID AS LaboratoryTestOrderId
		,hl7.PatientID AS PatientId
		,hl7.ReportDate AS OrderDate
	INTO #Denominator
	FROM HL7_LabReports hl7
	WHERE hl7.ReportDate BETWEEN @StartDate
			AND DATEADD(mi, 1440, @EndDate)
		AND hl7.ProviderID = @ResourceIds
	
	UNION ALL
	
	--This query will get as many labs as possible that aren''t in ITON that the doctor orders that are linked to lab test exam element
	SELECT e.PatientId
		,edto.Id AS LaboratoryTestOrderId
		,a.DATETIME AS OrderDate
	FROM model.EncounterDiagnosticTestOrders edto
	INNER JOIN model.DiagnosticTestOrders dto ON dto.id = edto.DiagnosticTestOrderId
	INNER JOIN model.ClinicalProcedures cp ON cp.Id = dto.ClinicalProcedureId
		AND cp.NAME IN (
			'BLOOD WORK'
			,'LAB RESULTS'
			,'LAB AND MEDICAL RESULTS'
			,'BLOOD COUNT'
			,'CULTURE OF CORNEA/CONJUNCTIVA'
			,'OSMOLARITY TEST'
			)
	INNER JOIN model.encounters e ON e.id = edto.EncounterId
	INNER JOIN model.Appointments a ON a.EncounterId = e.Id
	WHERE a.DATETIME BETWEEN @StartDate
			AND DATEADD(mi, 1440, @EndDate) --DATEADD so that EndDate includes the whole day of encounters
		AND e.EncounterStatusId = 7
		AND a.UserId = @ResourceIds

	--numerator  
	SELECT d.*
	INTO #Numerator
	FROM #Denominator d

	SET @TotParm = (
			SELECT COUNT(DISTINCT LaboratoryTestOrderId)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT LaboratoryTestOrderId)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.OrderDate
		,d.LaboratoryTestOrderId
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.OrderDate
		,d.LaboratoryTestOrderId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.OrderDate
		,n.LaboratoryTestOrderId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.OrderDate
		,n.LaboratoryTestOrderId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_eRx
IF (OBJECT_ID('model.USP_CMS_Stage2_eRx', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_eRx
END
GO
CREATE PROCEDURE [model].[USP_CMS_Stage2_eRx] (
	@StartDate NVARCHAR(200)
	,@EndDate NVARCHAR(200)
	,@ResourceIds NVARCHAR(1000)
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	IF @ResourceIds LIKE '%,%'
		RAISERROR (
				'Please ONLY send one doctor id at a TIME.'
				,16
				,2
				)

	DECLARE @userId NVARCHAR(20)
	DECLARE @userType NVARCHAR(20)
	DECLARE @transactionid NVARCHAR(max)
	DECLARE @eRxStage2Numerator INT
	DECLARE @eRxStage2Denominator INT

	SET @userId = 'demo'
	SET @userType = 'D'

	--numerator
	SELECT TOP 1 @transactionid = transactionid
	FROM model.NewCropUtilizationReports
	WHERE userid = @userId
		AND UserType = @userType
		AND LicensedPrescriberId = @ResourceIds
	ORDER BY TIMESTAMP DESC

	EXEC [IO.Practiceware.SqlClr].GetMeaningfulUseUtilizationReport @transactionId
		,@userId
		,@userType
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,@eRxStage2Numerator OUTPUT
		,@eRxStage2Denominator OUTPUT
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL

	SELECT @TotParm = @eRxStage2Denominator

	IF @CountParm > @TotParm
		SET @CountParm = @TotParm
	ELSE
		SELECT @CountParm = @eRxStage2Numerator

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator
END
GO

--model.USP_CMS_Stage2_ViewDownloadTransmitPatientUse
IF (OBJECT_ID('model.USP_CMS_Stage2_ViewDownloadTransmitPatientUse', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_ViewDownloadTransmitPatientUse
END
GO
CREATE PROCEDURE [model].[USP_CMS_Stage2_ViewDownloadTransmitPatientUse] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	IF @ResourceIds LIKE '%,%'
		RAISERROR (
				'Please ONLY send one doctor id at a TIME.'
				,16
				,2
				)

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Denominator
	SELECT DISTINCT p.Id AS PatientId
		,p.FirstName
		,p.LastName
		,p.DateOfBirth
		,SUBSTRING(g.NAME, 1, 1) AS Gender
		,e.Id AS EncounterId
		,a.DATETIME AS AppointmentDate
	INTO #Denominator
	FROM model.encounters e
	INNER JOIN model.appointments a ON a.EncounterId = e.id
	INNER JOIN model.patients p ON p.id = e.PatientId
	INNER JOIN model.Gender_Enumeration g ON g.Id = p.GenderId
	WHERE a.DATETIME BETWEEN @StartDate
			AND DATEADD(mi, 1440, @EndDate) --DATEADD so that EndDate includes the whole day of encounters
		AND e.EncounterStatusId = 7
		AND a.UserId = @ResourceIds

	-- Numerator
	--select DISTINCT id
	--into #Numerator
	--from #Denominator d
	--inner join dbo.GetNumeratorDetailsForViewDownloadTransmit vdt on vdt.PatientDob = d.DateOfBirth and vdt.patientFirstName = d.FirstName and vdt.patientLastName = d.LastName and vdt.patientGender = d.Gender
	--where vdt.patientRemoteId = @ResourceIds
	SELECT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN [IO.Practiceware.SqlClr].GetNumeratorDetailsForViewDownloadTransmit(@StartDate, @EndDate, @ResourceIds, 'OMEDIX') vdt ON vdt.PatientDob = d.DateOfBirth
		AND vdt.patientFirstName = d.FirstName
		AND vdt.patientLastName = d.LastName
		AND vdt.patientGender = d.Gender

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.AppointmentDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.AppointmentDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.AppointmentDate
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.AppointmentDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_ClinicalSummary
IF (OBJECT_ID('model.USP_CMS_Stage2_ClinicalSummary', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_ClinicalSummary
END
GO
--exec [model].[USP_CMS_Stage2_ClinicalSummary] '10/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage2_ClinicalSummary] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	IF @ResourceIds LIKE '%,%'
		RAISERROR (
				'Please select only one provider at a TIME.'
				,16
				,2
				)

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;
	DECLARE @Status NVARCHAR(1)

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	--Numerator 
	----1.  Is autoprinting on?  If so, 100%
	--SELECT @Status=FieldValue 
	--FROM PracticeInterfaceConfiguration 
	--WHERE FieldReference = 'PRINT'
	--SELECT @TotParm = COUNT(DISTINCT EncounterId)
	--FROM #Denominator
	--IF @Status='T'  
	--	BEGIN		  
	--		SELECT @CountParm = @TotParm
	--	END 
	--ELSE 
	--	BEGIN
	-- Patient Declines
	SELECT d.*
		,CONVERT(NVARCHAR(100), ClinicalId) AS ClinicalSummaryId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientClinical pc ON pc.AppointmentId = d.EncounterId
	WHERE ClinicalType = 'A'
		AND FindingDetail LIKE 'CLIN SUMMARY, PATIENT DECLINES%'
	
	UNION ALL
	
	-- Clinical Data Files
	-- portal, print, or save as
	SELECT d.*
		,CONVERT(NVARCHAR(100), esm.Id) AS ClinicalSummaryId
	FROM #Denominator d
	INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpre ON esmpre.PracticeRepositoryEntityKey = d.EncounterId
	INNER JOIN model.ExternalSystemMessages esm ON esm.id = esmpre.ExternalSystemMessageId
	INNER JOIN model.ExternalSystemExternalSystemMessageTypes esesmt ON esesmt.id = esm.ExternalSystemExternalSystemMessageTypeId
	INNER JOIN model.ExternalSystemMessageTypes esmt ON esmt.Id = esesmt.ExternalSystemMessageTypeId
		AND esmt.NAME = 'EncounterClinicalSummaryCCDA'
		AND IsOutbound = 1
	INNER JOIN model.Appointments ap ON ap.Id = esmpre.PracticeRepositoryEntityKey
		AND DATEDIFF(dd, ap.DATETIME, [IO.Practiceware.SqlClr].ConvertToClientDateTime([IO.Practiceware.SqlClr].ConvertToLocalDateTime(esm.CreatedDateTime))) <= 1
	
	UNION ALL
	
	-- 7.2 letters
	SELECT d.*
		,CONVERT(NVARCHAR(100), lep.Id) AS ClinicalSummaryId
	FROM #Denominator d
	INNER JOIN LogEntryProperties LEP ON LEP.Value = d.EncounterId
	INNER JOIN LogEntries LE ON LE.Id = LEP.LogEntryId
		AND LEP.NAME = 'AppointmentId'
	WHERE Message LIKE 'Printed misc ltr%Clin%Sum%'
		AND DATEDIFF(dd, d.EncounterDate, [IO.Practiceware.SqlClr].ConvertToClientDateTime([IO.Practiceware.SqlClr].ConvertToLocalDateTime(le.DATETIME))) <= 1

	SET @CountParm = (
			SELECT COUNT(DISTINCT EncounterId)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT EncounterId)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.EncounterId
		,n.EncounterDate
		,n.ClinicalSummaryId
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.EncounterDate
		,n.EncounterId

	DROP TABLE #Numerator

	DROP TABLE #Denominator
END
GO

--dbo.USP_CMS_HL7LabOrdersStats
IF (OBJECT_ID('dbo.USP_CMS_HL7LabOrdersStats', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE dbo.USP_CMS_HL7LabOrdersStats
END
GO
CREATE PROCEDURE [dbo].[USP_CMS_HL7LabOrdersStats] (
	@StartDate NVARCHAR(200)
	,@EndDate NVARCHAR(200)
	,@ResourceIds NVARCHAR(1000)
	)
AS
BEGIN
	---#11--Clinical lab results entered as structured data > 40%--------------------------
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	CREATE TABLE #TMPResources (ResourceId NVARCHAR(1000))

	IF @ResourceIds = ''
	BEGIN
		INSERT INTO #TMPResources
		SELECT Resources.ResourceId
		FROM Resources
		WHERE ResourceType IN (
				'D'
				,'Q'
				,'Z'
				,'X'
				)
	END
	ELSE
	BEGIN
		DECLARE @TmpResourceIds VARCHAR(100)
		DECLARE @ResId VARCHAR(10)

		SELECT @TmpResourceIds = @ResourceIds

		WHILE PATINDEX('%,%', @TmpResourceIds) <> 0
		BEGIN
			SELECT @ResId = LEFT(@TmpResourceIds, PATINDEX('%,%', @TmpResourceIds) - 1)

			SELECT @TmpResourceIds = SUBSTRING(@TmpResourceIds, PATINDEX('%,%', @TmpResourceIds) + 1, LEN(@TmpResourceIds))

			INSERT INTO #TMPResources
			VALUES (@ResId)
		END

		INSERT INTO #TMPResources
		VALUES (@TmpResourceIds)
	END

	--Denominator
	SELECT @TotParm = COUNT(DISTINCT lr.ReportId)
	FROM HL7_LabReports LR
	INNER JOIN Resources re ON re.ResourceId = lr.providerid
		AND re.ResourceId IN (
			SELECT #TMPResources.ResourceId
			FROM #TMPResources
			)
	WHERE CONVERT(NVARCHAR, LR.ReportDate, 112) BETWEEN @StartDate
			AND @EndDate

	--Numerator
	SELECT @CountParm = COUNT(DISTINCT lr.ReportId)
	FROM HL7_LabReports LR
	INNER JOIN HL7_Observation OBR ON LR.ReportId = OBR.ReportId
	INNER JOIN HL7_Observation_Details DET ON OBR.OBRID = DET.OBRID
	INNER JOIN Resources re ON re.ResourceId = lr.ProviderId
		AND re.ResourceId IN (
			SELECT #TMPResources.ResourceId
			FROM #TMPResources
			)
	WHERE CONVERT(NVARCHAR, LR.ReportDate, 112) BETWEEN @StartDate
			AND @EndDate

	DROP TABLE #TMPResources

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator
END
GO

--model.USP_CMS_Stage2_PatientReminders
IF (OBJECT_ID('model.USP_CMS_Stage2_PatientReminders', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_PatientReminders
END
GO
CREATE PROCEDURE [model].[USP_CMS_Stage2_PatientReminders] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	--Denominator
	SELECT p.id AS PatientId
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
	INNER JOIN model.Patients p ON p.id = ipp.PatientId
	INNER JOIN model.Appointments a ON a.EncounterId = ipp.EncounterId
	INNER JOIN PatientClinical PC ON PC.PatientId = P.Id
		AND ClinicalType = 'Q' --This determines if their records are mantained in the EHR
	WHERE DATEADD(yy, - 2, @StartDate) <= a.DATETIME
		AND a.DATETIME < @StartDate
		AND PatientStatusId = 1
	GROUP BY p.Id
	HAVING count(ipp.EncounterId) >= 2

	--Numerator
	--Numerator
	--Recalls
	SELECT d.PatientId
		,ptj.TransactionID AS ReminderId
		,CONVERT(DATE, TransactionDate) AS ReminderSentDate
		,'RECALL' AS Type
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PracticeTransactionJournal PTJ ON ptj.TransactionTypeId = d.PatientId
	INNER JOIN Resources re ON re.ResourceName = REVERSE(SUBSTRING(REVERSE(PTJ.TransactionRef), 0, CHARINDEX('/', REVERSE(PTJ.TransactionRef), 0)))
	WHERE PTJ.TransactionType = 'L'
		AND PTJ.TransactionStatus = 'S'
		AND ptj.transactiontypeid = d.PatientId
		AND re.ResourceId = @ResourceIds
		AND CONVERT(DATETIME, TransactionDate) BETWEEN @StartDate
			AND @EndDate
	
	UNION ALL
	
	SELECT D.PatientId
		,Id AS ReminderId
		,NULL AS ReminderSentDate
		,'CONFIRMATION' AS Type
	FROM #Denominator D
	INNER JOIN model.Encounters E ON E.PatientId = D.PatientId
	WHERE ConfirmationStatusId IS NOT NULL

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId

	SELECT n.PatientId
		,p.FirstName
		,p.LastName
		,n.ReminderId
		,n.ReminderSentDate
	FROM #Numerator n
	INNER JOIN model.patients p ON p.id = n.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,n.PatientId
		,n.ReminderSentDate

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--dbo.USP_CMS_GetMedicationReconStatus_Stage2
IF (OBJECT_ID('dbo.USP_CMS_GetMedicationReconStatus_Stage2', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE dbo.USP_CMS_GetMedicationReconStatus_Stage2
END
GO
CREATE PROCEDURE [dbo].[USP_CMS_GetMedicationReconStatus_Stage2] (
	@StartDate NVARCHAR(200)
	,@EndDate NVARCHAR(200)
	,@ResourceIds NVARCHAR(1000)
	)
AS
BEGIN
	---#15-Medication Reconclication--------------------------------------------------
	DECLARE @TotParm INT
	DECLARE @CountParm INT

	SET @TotParm = 0
	SET @CountParm = 0

	DECLARE @PatientId NVARCHAR(20)
	DECLARE @path NVARCHAR(max)

	IF @ResourceIds LIKE '%,%'
		RAISERROR (
				'Please ONLY send one doctor id at a TIME.'
				,16
				,2
				)

	DECLARE @userId NVARCHAR(20)
	DECLARE @userType NVARCHAR(20)
	DECLARE @transactionid NVARCHAR(max)
	DECLARE @medReconciliationNumerator INT

	SET @userId = 'demo'
	SET @userType = 'D'

	--Denominator
	SELECT DISTINCT ap.PatientId
	INTO #patients
	FROM Appointments ap
	WHERE AppDate BETWEEN @StartDate
			AND @EndDate
		AND ap.ResourceId1 = @ResourceIDs
		AND ScheduleStatus = 'D'
		AND AppTime > 0

	IF EXISTS (
			SELECT *
			FROM model.ApplicationSettings
			WHERE NAME = 'ServerDataPath'
			)
		SELECT @Path = Value
		FROM model.ApplicationSettings
		WHERE NAME = 'ServerDataPath'
	ELSE
		SET @Path = @@SERVERNAME + '\IOP\Pinpoint\MyScan'

	CREATE TABLE #DenominatorScans (
		PatientId INT
		,DATETIME DATETIME
		,Path NVARCHAR(MAX)
		)

	DECLARE patCursor CURSOR
	FOR
	SELECT PatientId
	FROM #patients

	OPEN patCursor

	FETCH NEXT
	FROM patCursor
	INTO @PatientId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT ROW_NUMBER() OVER (
				ORDER BY path
				) AS Row
			,REVERSE(dbo.ExtractTextValue('-', REVERSE(path), '_')) AS PatientId
			,LEFT(REVERSE(dbo.ExtractTextValue('.', REVERSE(path), '-')), 8) AS DATETIME
			,path
		INTO #paths
		FROM [IO.Practiceware.SqlClr].GetFiles(@Path + CONVERT(NVARCHAR(10), @PatientId), '*.*')
		WHERE dbo.ExtractTextValue(@PatientId + '\', path, '_') IN (
				'C-CDA'
				,'CCR-'
				,'CCR'
				,'CCD'
				)

		INSERT INTO #DenominatorScans
		SELECT p.patientId
			,p.DATETIME
			,p.path
		FROM #paths p
		WHERE CONVERT(NVARCHAR(8), p.DATETIME, 112) BETWEEN @StartDate
				AND @EndDate

		DROP TABLE #paths

		FETCH NEXT
		FROM patCursor
		INTO @PatientId
	END

	CLOSE patCursor

	DEALLOCATE patCursor

	SELECT @TotParm = COUNT(*)
	FROM Appointments ap
	INNER JOIN Resources re ON re.ResourceId = ap.ResourceId1
	INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
		AND re.ResourceId = @ResourceIds
	WHERE AppDate BETWEEN @StartDate
			AND @EndDate
		AND ScheduleStatus = 'D'
		AND AppTime > 0
		AND pc.ClinicalType = 'F'
		AND pc.Symptom = '/MEDS RECONCILIATION'
		AND pc.FindingDetail = '*TRANSITIONOFCARE-MEDREC=T14911 <TRANSITIONOFCARE-MEDREC>'
		AND pc.STATUS = 'A'

	DECLARE @TotParmScans INT

	SELECT @TotParmScans = COUNT(DISTINCT Path)
	FROM #DenominatorScans ds
	INNER JOIN model.Patients p ON ds.PatientId = p.Id
	WHERE p.DefaultUserId = @ResourceIDs

	--numerator
	SELECT TOP 1 @transactionid = transactionid
	FROM model.NewCropUtilizationReports
	WHERE userid = @userId
		AND UserType = @userType
		AND LicensedPrescriberId = @ResourceIds
	ORDER BY TIMESTAMP DESC

	EXEC [IO.Practiceware.SqlClr].GetMeaningfulUseUtilizationReport @transactionId
		,@userId
		,@userType
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,@medReconciliationNumerator OUTPUT
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL

	SELECT @TotParm = @TotParm + @TotParmScans

	IF @CountParm > @TotParm
		SET @CountParm = @TotParm
	ELSE
		SELECT @CountParm = @medReconciliationNumerator

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator
END
GO

--model.USP_CMS_Stage2_SummaryOfCareB
IF (OBJECT_ID('model.USP_CMS_Stage2_SummaryOfCareB', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_SummaryOfCareB
END
GO
--exec [model].[USP_CMS_Stage2_SummaryOfCareB] '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage2_SummaryOfCareB] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	DECLARE @userId NVARCHAR(20)
	DECLARE @userType NVARCHAR(20)
	DECLARE @transactionid NVARCHAR(max)
	DECLARE @SummaryOfCareNumerator INT

	SET @userId = 'demo'
	SET @userType = 'D'

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT ipp.*
		,etoco.Id AS TransitionOfCareOrderId
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
	INNER JOIN model.EncounterTransitionOfCareOrders etoco ON etoco.EncounterId = ipp.EncounterId

	--Numerator
	--C-CDAs Saved
	SELECT d.*
		,CreatedDateTime AS SendDate
		,'C-CDA Saved' AS Type
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpre ON esmpre.PracticeRepositoryEntityKey = d.PatientId
	INNER JOIN model.ExternalSystemMessages esm ON esm.id = esmpre.ExternalSystemMessageId
	WHERE ExternalSystemExternalSystemMessageTypeId IN (
			SELECT id
			FROM model.ExternalSystemExternalSystemMessageTypes
			WHERE ExternalSystemId IN (
					SELECT Id
					FROM model.ExternalSystems
					WHERE NAME = 'Saved'
					)
				AND ExternalSystemMessageTypeId IN (
					SELECT id
					FROM model.ExternalSystemMessageTypes
					WHERE NAME = 'PatientTransitionOfCareCCDA'
						AND IsOutbound = 1
					)
			)

	SELECT @TotParm = COUNT(DISTINCT TransitionOfCareOrderId)
	FROM #Denominator

	SELECT @CountParm = COUNT(DISTINCT TransitionOfCareOrderId)
	FROM #Numerator

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
		,d.TransitionOfCareOrderId
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
		,d.TransitionOfCareOrderId
		,d.SendDate
		,Type
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_SecureMessage
IF (OBJECT_ID('model.USP_CMS_Stage2_SecureMessage', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_SecureMessage
END
GO
CREATE PROCEDURE [model].[USP_CMS_Stage2_SecureMessage] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	IF @ResourceIds LIKE '%,%'
		RAISERROR (
				'Please ONLY send one doctor id at a TIME.'
				,16
				,2
				)

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Denominator
	SELECT PatientId
		,e.Id AS EncounterId
		,a.DATETIME AS AppointmentDate
	INTO #Denominator
	FROM model.encounters e
	INNER JOIN model.appointments a ON a.EncounterId = e.id
	WHERE a.DATETIME BETWEEN @StartDate
			AND DATEADD(mi, 1440, @EndDate) --DATEADD so that EndDate includes the whole day of encounters
		AND e.EncounterStatusId = 7
		AND a.UserId = @ResourceIds

	--Numerator    
	SELECT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	INNER JOIN model.Gender_Enumeration g ON g.Id = p.GenderId
	INNER JOIN [IO.Practiceware.SqlClr].GetNumeratorDetailsForSecureMessage(@StartDate, @EndDate, @ResourceIds, 'OMEDIX') sm ON sm.PatientDob = p.DateOfBirth
		AND sm.patientFirstName = p.FirstName
		AND sm.patientLastName = p.LastName
		AND sm.patientGender = SUBSTRING(g.NAME, 1, 1)

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.AppointmentDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.AppointmentDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.AppointmentDate
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.AppointmentDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_ElectronicNotes
IF (OBJECT_ID('model.USP_CMS_Stage2_ElectronicNotes', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_ElectronicNotes
END
GO
CREATE PROCEDURE [model].[USP_CMS_Stage2_ElectronicNotes] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	IF @ResourceIds LIKE '%,%'
		RAISERROR (
				'Please ONLY send one doctor id at a TIME.'
				,16
				,2
				)

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Denominator
	SELECT PatientId
		,e.Id AS EncounterId
		,a.DATETIME AS AppointmentDate
	INTO #Denominator
	FROM model.encounters e
	INNER JOIN model.appointments a ON a.EncounterId = e.id
	WHERE a.DATETIME BETWEEN @StartDate
			AND DATEADD(mi, 1440, @EndDate) --DATEADD so that EndDate includes the whole day of encounters
		AND e.EncounterStatusId = 7
		AND a.UserId = @ResourceIds

	--Numerator
	SELECT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientNotes pn ON pn.PatientId = d.PatientId
	WHERE pn.UserName = (
			SELECT resourcename
			FROM resources
			WHERE resourceid = @ResourceIds
			)
		AND NoteType = 'C'

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.AppointmentDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.AppointmentDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.AppointmentDate
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.AppointmentDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_FamilyHistory
IF (OBJECT_ID('model.USP_CMS_Stage2_FamilyHistory', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_FamilyHistory
END
GO
--exec [model].[USP_CMS_Stage2_FamilyHistory] '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_Stage2_FamilyHistory] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ResourceIds INT
	)
AS
BEGIN
	IF @ResourceIds LIKE '%,%'
		RAISERROR (
				'Please ONLY send one doctor id at a TIME.'
				,16
				,2
				)

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;

	SET @TotParm = 0;
	SET @CountParm = 0;

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT *
	INTO #Denominator
	FROM #InitialPatientPopulation

	-- numerator
	SELECT d.*
	INTO #numerator
	FROM #Denominator d
	INNER JOIN model.PatientFamilyHistoryEntries pfhe ON pfhe.PatientId = d.PatientId
	INNER JOIN model.PatientFamilyHistoryEntryDetails pfhed ON pfhed.PatientFamilyHistoryEntryId = pfhe.id
		AND IsDeactivated = 0
	INNER JOIN model.PatientFamilyHistoryEntryDetailFamilyRelationship pfhedfr ON pfhedfr.PatientFamilyHistoryEntryDetails_Id = pfhed.Id
		AND pfhedfr.FamilyRelationships_Id IN (
			2
			,3
			,5
			,11
			,15
			,16
			,17
			,19
			,20
			) --1st degree relatives

	SET @TotParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Denominator
			)
	SET @CountParm = (
			SELECT COUNT(DISTINCT PatientID)
			FROM #Numerator
			)

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
		,d.EncounterId
		,d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	SELECT d.PatientId
		,p.FirstName
		,p.LastName
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.EncounterDate
		,d.EncounterId

	DROP TABLE #Denominator

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_Stage2_Imaging
IF (OBJECT_ID('model.USP_CMS_Stage2_Imaging', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_Stage2_Imaging
END
GO
CREATE PROCEDURE [model].[USP_CMS_Stage2_Imaging] (
	@StartDate NVARCHAR(200)
	,@EndDate NVARCHAR(200)
	,@ResourceIds NVARCHAR(1000)
	)
AS
BEGIN
	--patient diagnostic tests performed
	SELECT MAX(ClinicalId) AS Id
		,1 AS ClinicalDataSourceTypeId
		,cp.Id AS ClinicalProcedureId
		,ap.EncounterId AS EncounterId
		,pc.PatientId AS PatientId
		,DATEADD(mi, AppTime, (CONVERT(DATETIME, AppDate))) AS PerformedDateTime
		,NULL AS TimeQualifierId
	INTO #PatientDiagnosticTestPerformed
	FROM dbo.PatientClinical pc
	INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
	INNER JOIN model.ClinicalProcedures cp ON '/' + cp.NAME = pc.Symptom
	INNER JOIN Resources re ON re.ResourceId = ap.ResourceId1
		AND re.ResourceId = @ResourceIds
	WHERE pc.ClinicalType = 'F'
		AND pc.[Status] = 'A'
		AND ClinicalProcedureCategoryId = 2
	GROUP BY cp.Id
		,ap.EncounterId
		,pc.PatientId
		,cp.NAME
		,DATEADD(mi, AppTime, (CONVERT(DATETIME, AppDate)))

	--Denominator
	SELECT p.PatientId
		,ePerformed.Id AS PerformedEncounterId
		,o.Id OrderId
		,p.ClinicalProcedureId
		,p.Id AS PerformedId
		,eOrdered.StartDateTime AS OrderedStartDateTime
		,ePerformed.StartDateTime AS PerformedStartDateTime
	INTO #Denominator
	FROM model.EncounterDiagnosticTestOrders ed
	INNER JOIN model.DiagnosticTestOrders o ON o.Id = ed.DiagnosticTestOrderId
	INNER JOIN model.Encounters eOrdered ON eOrdered.Id = ed.EncounterId
		AND eOrdered.StartDateTime BETWEEN @StartDate
			AND @EndDate
	INNER JOIN #PatientDiagnosticTestPerformed p ON p.PatientId = eOrdered.PatientId
		AND p.ClinicalProcedureId = o.ClinicalProcedureId
	INNER JOIN model.Encounters ePerformed ON p.EncounterId = ePerformed.Id
		AND ePerformed.StartDateTime >= eOrdered.StartDateTime

	--Numerator - 
	SELECT d.PatientId
		,d.PerformedEncounterId
		,d.OrderId
		,d.ClinicalProcedureId
		,d.PerformedId
		,OrderedStartDateTime
		,PerformedStartDateTime
		,pct.Code AS ScanType
	INTO #GetFilesHelp
	FROM #Denominator d
	INNER JOIN model.ClinicalProcedures c ON c.Id = d.ClinicalProcedureId
	INNER JOIN df.Questions q ON c.NAME = q.Question
		AND q.QuestionParty <> 'P'
	INNER JOIN PracticeCodeTable pct ON pct.AlternateCode = q.QuestionOrder
	WHERE ReferenceType = 'SCANTYPE'

	/*File named in this format:  ScanType-_EncounterId_PatientId-YYYYMMDD-##.@@@

	If EncounterId = ePerformed.Id then it's a match

	If no EncounterId and PatientId = d.PatientId and YYYYMMDD = ePerformed.StartDateTime, then it's a match

	*/
	DECLARE @PatientId NVARCHAR(20)
	DECLARE @EPerformedId NVARCHAR(20)
	DECLARE @Scantype NVARCHAR(50)
	DECLARE @CountParm INT
	DECLARE @TotParm INT
	DECLARE @path NVARCHAR(max)

	IF EXISTS (
			SELECT *
			FROM model.ApplicationSettings
			WHERE NAME = 'ServerDataPath'
			)
		SELECT @Path = Value
		FROM model.ApplicationSettings
		WHERE NAME = 'ServerDataPath'
	ELSE
		SET @Path = @@SERVERNAME + '\IOP\Pinpoint\MyScan'

	CREATE TABLE #Numerator (
		PatientId INT
		,OrderId INT
		,ClinicalProcedureId INT
		,PerformedId INT
		,OrderedStartDateTime DATETIME
		,PerformedStartDateTime DATETIME
		,DATE DATETIME
		,Path NVARCHAR(MAX)
		)

	DECLARE numCursor CURSOR
	FOR
	SELECT PatientId
	FROM #GetFilesHelp

	OPEN numCursor

	FETCH NEXT
	FROM numCursor
	INTO @PatientId

	SELECT @Scantype = Scantype
	FROM #GetFilesHelp
	WHERE PatientId = @PatientId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT ROW_NUMBER() OVER (
				ORDER BY path
				) AS Row
			,CASE 
				WHEN dbo.[IsNullOrEmpty]([IO.Practiceware.SqlClr].[SearchRegexPattern](path, '[0-9]+_')) = 0
					THEN LEFT([IO.Practiceware.SqlClr].[SearchRegexPattern](path, '[0-9]+_'), LEN([IO.Practiceware.SqlClr].[SearchRegexPattern](path, '[0-9]+_')) - 1)
				ELSE ''
				END AS EncounterID
			,CASE 
				WHEN dbo.[IsNullOrEmpty]([IO.Practiceware.SqlClr].[SearchRegexPattern](path, '[0-9]+-')) = 0
					THEN LEFT([IO.Practiceware.SqlClr].[SearchRegexPattern](path, '[0-9]+-'), LEN([IO.Practiceware.SqlClr].[SearchRegexPattern](path, '[0-9]+-')) - 1)
				ELSE ''
				END AS PatientId
			,CASE 
				WHEN dbo.[IsNullOrEmpty]([IO.Practiceware.SqlClr].[SearchRegexPattern](path, '[0-9]+-')) = 0
					THEN RIGHT([IO.Practiceware.SqlClr].[SearchRegexPattern](path, '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'), 8)
				ELSE ''
				END AS DATETIME
			,path
		INTO #paths
		FROM [IO.Practiceware.SqlClr].GetFiles(@Path + CONVERT(NVARCHAR(10), @PatientId), '*.*')
		WHERE path LIKE '%' + @Scantype + '%'

		INSERT INTO #Numerator
		SELECT d.PatientId
			,d.OrderId
			,d.ClinicalProcedureId
			,d.PerformedId
			,d.OrderedStartDateTime
			,d.PerformedStartDateTime
			,p.DATETIME
			,p.Path
		FROM #Denominator d
		INNER JOIN #paths p ON d.PatientId = CONVERT(INT, p.PatientId)
			AND CONVERT(INT, p.EncounterId) = d.PerformedEncounterId
		WHERE p.EncounterId <> ''
			OR p.DATETIME = CONVERT(NVARCHAR(8), d.PerformedStartDateTime, 112)

		DROP TABLE #paths

		FETCH NEXT
		FROM numCursor
		INTO @PatientId
	END

	CLOSE numCursor

	DEALLOCATE numCursor

	SELECT @CountParm = COUNT(DISTINCT Path)
	FROM #Numerator

	SELECT @TotParm = COUNT(*)
	FROM #Denominator

	SELECT @TotParm AS Denominator
		,@CountParm AS Numerator

	SELECT d.PatientId AS DENOMINATOR
		,p.FirstName
		,p.LastName
		,d.OrderId
		,d.OrderedStartDateTime
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId
		,d.OrderId
		,d.OrderedStartDateTime

	SELECT DISTINCT d.PatientId AS NUMERATOR
		,p.FirstName
		,p.LastName
		,d.Path
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName
		,p.FirstName
		,d.PatientId

	-- drop table #paths
	DROP TABLE #Denominator

	DROP TABLE #GetFilesHelp

	DROP TABLE #PatientDiagnosticTestPerformed

	DROP TABLE #Numerator
END
GO

--model.USP_CMS_InitialPatientPopulation
IF (OBJECT_ID('model.USP_CMS_InitialPatientPopulation', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE model.USP_CMS_InitialPatientPopulation
END
GO
--exec [model].[USP_CMS_InitialPatientPopulation] '01/01/2013', '12/31/2013', 192
CREATE PROCEDURE [model].[USP_CMS_InitialPatientPopulation] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@UserId INT
	)
AS
BEGIN
	SELECT E.Id AS EncounterId
		,A.DATETIME AS EncounterDate
		,PatientId
		,CONVERT(DATETIME, DateOfBirth) AS DOB
		,DATEDIFF(YY, CONVERT(DATETIME, DateOfBirth), CONVERT(DATETIME, a.DATETIME)) AS Age
	FROM MODEL.Encounters E
	INNER JOIN MODEL.Appointments A ON A.EncounterId = E.Id
	INNER JOIN MODEL.Patients P ON P.Id = E.PatientId
	WHERE EncounterStatusId = 7
		AND DATEPART(MI, StartDateTime) >= 1
		AND CONVERT(DATETIME, StartDateTime) BETWEEN @StartDate
			AND @EndDate
		AND A.UserId = @UserId
END
GO