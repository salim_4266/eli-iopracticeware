﻿IF (
		EXISTS (
			SELECT *
			FROM INFORMATION_SCHEMA.TABLES
			WHERE TABLE_SCHEMA = 'Model'
				AND TABLE_NAME = 'BodyParts'
			)
		)
BEGIN
	DECLARE @OrganId INT

	--Creating the rows for organs used only in this migration.Just in Case 20140605_InsertOrgans didn't create rows.
	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Appendix'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Appendix'
			,6
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Bladder'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Bladder'
			,7
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Brain'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Brain'
			,10
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Ear'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Ear'
			,3
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Esophagus'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Esophagus'
			,3
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Eye'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Eye'
			,2
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Heart'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Heart'
			,4
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Kidney'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Kidney'
			,7
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Liver'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Liver'
			,6
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Mouth'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Mouth'
			,3
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Nose'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Nose'
			,3
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Ovaries'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Ovaries'
			,7
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Penis'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Penis'
			,7
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Spleen'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Spleen'
			,13
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Stomach'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Stomach'
			,6
			,1
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[Organs]
			WHERE NAME = 'Thymus'
			)
		INSERT INTO [Model].[Organs] (
			NAME
			,BodySystemId
			,OrdinalId
			)
		VALUES (
			'Thymus'
			,14
			,1
			)

	--Insert Body Parts
	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Abdomen'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			6
			,'Abdomen'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Adam''s apple'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Adam''s apple'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Adrenals'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			12
			,'Adrenals'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Ankle'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Ankle'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Appendix'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Appendix'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			6
			,'Appendix'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Arm'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Arm'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Bladder'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Bladder'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			7
			,'Bladder'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Brain'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Brain'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			10
			,'Brain'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Breast'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			7
			,'Breast'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Buttocks'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Buttocks'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Calf'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Calf'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Cheek'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			9
			,'Cheek'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Chest'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			9
			,'Chest'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Chin'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			9
			,'Chin'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Clitoris'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			7
			,'Clitoris'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Ear'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Ear'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			3
			,'Ear'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Elbow'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Elbow'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Esophagus'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Esophagus'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			6
			,'Esophagus'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Eye'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Eye'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			2
			,'Eye'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Fingers'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Fingers'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Foot'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Foot'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Forehead'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			9
			,'Forehead'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Gallbladder'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			6
			,'Gallbladder'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Groin'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			7
			,'Groin'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Hand'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Hand'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Head'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Head'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Heart'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Heart'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			4
			,'Heart'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Heel'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Heel'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Hip'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Hip'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Intestines'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			6
			,'Intestines'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Jaw'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Jaw'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Kidney'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Kidney'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			7
			,'Kidney'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Knee'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Knee'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Leg'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Leg'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Liver'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Liver'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			6
			,'Liver'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Lung'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			5
			,'Lung'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Mouth'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Mouth'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			3
			,'Mouth'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Navel'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			9
			,'Navel'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Neck'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Neck'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Nose'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Nose'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			3
			,'Nose'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Ovaries'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Ovaries'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			7
			,'Ovaries'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Pancreas'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			12
			,'Pancreas'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Parathyroids'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			12
			,'Parathyroids'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Penis'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Penis'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			7
			,'Penis'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Pituitary'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			12
			,'Pituitary'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Prostate'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			7
			,'Prostate'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Scrotum'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			7
			,'Scrotum'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Shoulders'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Shoulders'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Spine'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			10
			,'Spine'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Spleen'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Spleen'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			13
			,'Spleen'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Stomach'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Stomach'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			6
			,'Stomach'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Teeth'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			3
			,'Teeth'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Testicles'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			7
			,'Testicles'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Thigh'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Thigh'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Thorax'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Thorax'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Throat'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			3
			,'Throat'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Thumb'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Thumb'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Thymus'
			)
	BEGIN
		SELECT TOP 1 @OrganId = Id
		FROM model.Organs
		WHERE NAME = 'Thymus'

		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			13
			,'Thymus'
			,1
			,0
			,@OrganId
			)
	END

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Thyroid'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			12
			,'Thyroid'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Toes'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Toes'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Tongue'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			3
			,'Tongue'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Uterus'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			7
			,'Uterus'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Veins'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			4
			,'Veins'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Vulva'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			7
			,'Vulva'
			,1
			,0
			,NULL
			)

	IF NOT EXISTS (
			SELECT NAME
			FROM [Model].[BodyParts]
			WHERE NAME = 'Wrist'
			)
		INSERT INTO [Model].[BodyParts] (
			BodySystemId
			,NAME
			,OrdinalId
			,IsDeactivated
			,OrganId
			)
		VALUES (
			8
			,'Wrist'
			,1
			,0
			,NULL
			)
END
		--Import the data into temp table call BodyPart$ and run the below query to generate the above query
		--If you use this, will need to update to select the correct @OrganId.
		--SELECT CASE  WHEN orgn.ID is NULL THEN
		--(' IF NOT EXISTS(SELECT Name FROM [Model].[BodyParts] WHERE Name =  '''+ bodyprt.Name +''') 
		--INSERT INTO [Model].[BodyParts] (BodySystemId, Name, OrdinalId, IsDeactivated, OrganId) 
		--VALUES ('+ CONVERT(nvarchar(max), bodyprt.BodySystemId) +' ,'''+ CONVERT(nvarchar(max), bodyprt.Name) + ''', 1, 0)')
		--else
		--(' IF NOT EXISTS(SELECT Name FROM [Model].[BodyParts] where Name =  '''+ bodyprt.Name +''') 
		--INSERT INTO [Model].[BodyParts] (BodySystemId, Name, OrdinalId, IsDeactivated, OrganId) 
		--VALUES ('+ CONVERT(nvarchar(max), bodyprt.BodySystemId) +' ,'''+ CONVERT(nvarchar(max), bodyprt.Name) + ''', 1, 0,'+ CONVERT(nvarchar(max), orgn.id) +')')
		--end
		--FROM [dbo].[BodyPart$]  bodyprt
		--left join [model].[Organs] orgn on
		--bodyprt.Name = orgn.Name