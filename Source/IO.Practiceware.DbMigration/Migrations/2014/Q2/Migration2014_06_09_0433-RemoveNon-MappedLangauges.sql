﻿--Task 11669.

--Just in case.
IF OBJECT_ID('tempdb..#Languages') IS NOT NULL
	DROP TABLE #Languages

--Creating a temp table to get a list of MU list
CREATE TABLE #Languages (
	[IsArchived] [bit] NOT NULL
	,[Name] [nvarchar](max) NULL
	,[OrdinalId] [int] NOT NULL
	,[Abbreviation] [nvarchar](max) NULL
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Afar'
	,1
	,'aar'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Abkhazian'
	,1
	,'abk'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Afrikaans'
	,1
	,'afr'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Akan'
	,1
	,'aka'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Albanian'
	,1
	,'alb (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Amharic'
	,1
	,'amh'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Arabic'
	,1
	,'ara'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Aragonese'
	,1
	,'arg'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Armenian'
	,1
	,'arm (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Assamese'
	,1
	,'asm'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Avaric'
	,1
	,'ava'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Avestan'
	,1
	,'ave'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Aymara'
	,1
	,'aym'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Azerbaijani'
	,1
	,'aze'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Bashkir'
	,1
	,'bak'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Bambara'
	,1
	,'bam'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Basque'
	,1
	,'baq (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Belarusian'
	,1
	,'bel'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Bengali'
	,1
	,'ben'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Bihari languages'
	,1
	,'bih'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Bislama'
	,1
	,'bis'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Tibetan'
	,1
	,'tib (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Bosnian'
	,1
	,'bos'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Breton'
	,1
	,'bre'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Bulgarian'
	,1
	,'bul'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Burmese'
	,1
	,'bur (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Catalan; Valencian'
	,1
	,'cat'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Czech'
	,1
	,'cze (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Chamorro'
	,1
	,'cha'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Chechen'
	,1
	,'che'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Chinese'
	,1
	,'chi (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic'
	,1
	,'chu'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Chuvash'
	,1
	,'chv'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Cornish'
	,1
	,'cor'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Corsican'
	,1
	,'cos'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Cree'
	,1
	,'cre'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Welsh'
	,1
	,'wel (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Czech'
	,1
	,'cze (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Danish'
	,1
	,'dan'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'German'
	,1
	,'ger (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Divehi; Dhivehi; Maldivian'
	,1
	,'div'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Dutch; Flemish'
	,1
	,'dut (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Dzongkha'
	,1
	,'dzo'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Greek, Modern (1453-)'
	,1
	,'gre (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'English'
	,1
	,'eng'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Esperanto'
	,1
	,'epo'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Estonian'
	,1
	,'est'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Basque'
	,1
	,'baq (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Ewe'
	,1
	,'ewe'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Faroese'
	,1
	,'fao'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Persian'
	,1
	,'per (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Fijian'
	,1
	,'fij'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Finnish'
	,1
	,'fin'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'French'
	,1
	,'fre (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'French'
	,1
	,'fre (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Western Frisian'
	,1
	,'fry'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Fulah'
	,1
	,'ful'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Georgian'
	,1
	,'geo (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'German'
	,1
	,'ger (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Gaelic; Scottish Gaelic'
	,1
	,'gla'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Irish'
	,1
	,'gle'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Galician'
	,1
	,'glg'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Manx'
	,1
	,'glv'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Greek, Modern (1453-)'
	,1
	,'gre (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Guarani'
	,1
	,'grn'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Gujarati'
	,1
	,'guj'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Haitian; Haitian Creole'
	,1
	,'hat'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Hausa'
	,1
	,'hau'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Hebrew'
	,1
	,'heb'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Herero'
	,1
	,'her'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Hindi'
	,1
	,'hin'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Hiri Motu'
	,1
	,'hmo'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Croatian'
	,1
	,'hrv'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Hungarian'
	,1
	,'hun'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Armenian'
	,1
	,'arm (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Igbo'
	,1
	,'ibo'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Icelandic'
	,1
	,'ice (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Ido'
	,1
	,'ido'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Sichuan Yi; Nuosu'
	,1
	,'iii'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Inuktitut'
	,1
	,'iku'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Interlingue; Occidental'
	,1
	,'ile'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Interlingua (International Auxiliary Language Association)'
	,1
	,'ina'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Indonesian'
	,1
	,'ind'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Inupiaq'
	,1
	,'ipk'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Icelandic'
	,1
	,'ice (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Italian'
	,1
	,'ita'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Javanese'
	,1
	,'jav'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Japanese'
	,1
	,'jpn'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Kalaallisut; Greenlandic'
	,1
	,'kal'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Kannada'
	,1
	,'kan'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Kashmiri'
	,1
	,'kas'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Georgian'
	,1
	,'geo (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Kanuri'
	,1
	,'kau'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Kazakh'
	,1
	,'kaz'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Central Khmer'
	,1
	,'khm'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Kikuyu; Gikuyu'
	,1
	,'kik'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Kinyarwanda'
	,1
	,'kin'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Kirghiz; Kyrgyz'
	,1
	,'kir'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Komi'
	,1
	,'kom'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Kongo'
	,1
	,'kon'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Korean'
	,1
	,'kor'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Kuanyama; Kwanyama'
	,1
	,'kua'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Kurdish'
	,1
	,'kur'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Lao'
	,1
	,'lao'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Latin'
	,1
	,'lat'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Latvian'
	,1
	,'lav'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Limburgan; Limburger; Limburgish'
	,1
	,'lim'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Lingala'
	,1
	,'lin'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Lithuanian'
	,1
	,'lit'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Luxembourgish; Letzeburgesch'
	,1
	,'ltz'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Luba-Katanga'
	,1
	,'lub'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Ganda'
	,1
	,'lug'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Macedonian'
	,1
	,'mac (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Marshallese'
	,1
	,'mah'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Malayalam'
	,1
	,'mal'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Maori'
	,1
	,'mao (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Marathi'
	,1
	,'mar'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Malay'
	,1
	,'may (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Macedonian'
	,1
	,'mac (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Malagasy'
	,1
	,'mlg'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Maltese'
	,1
	,'mlt'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Mongolian'
	,1
	,'mon'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Maori'
	,1
	,'mao (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Malay'
	,1
	,'may (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Burmese'
	,1
	,'bur (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Nauru'
	,1
	,'nau'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Navajo; Navaho'
	,1
	,'nav'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Ndebele, South; South Ndebele'
	,1
	,'nbl'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Ndebele, North; North Ndebele'
	,1
	,'nde'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Ndonga'
	,1
	,'ndo'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Nepali'
	,1
	,'nep'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Dutch; Flemish'
	,1
	,'dut (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Norwegian Nynorsk; Nynorsk, Norwegian'
	,1
	,'nno'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Bokmål, Norwegian; Norwegian Bokmål'
	,1
	,'nob'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Norwegian'
	,1
	,'nor'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Chichewa; Chewa; Nyanja'
	,1
	,'nya'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Occitan (post 1500)'
	,1
	,'oci'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Ojibwa'
	,1
	,'oji'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Oriya'
	,1
	,'ori'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Oromo'
	,1
	,'orm'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Ossetian; Ossetic'
	,1
	,'oss'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Panjabi; Punjabi'
	,1
	,'pan'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Persian'
	,1
	,'per (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Pali'
	,1
	,'pli'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Polish'
	,1
	,'pol'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Portuguese'
	,1
	,'por'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Pushto; Pashto'
	,1
	,'pus'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Quechua'
	,1
	,'que'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Romansh'
	,1
	,'roh'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Romanian; Moldavian; Moldovan'
	,1
	,'rum (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Romanian; Moldavian; Moldovan'
	,1
	,'rum (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Rundi'
	,1
	,'run'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Russian'
	,1
	,'rus'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Sango'
	,1
	,'sag'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Sanskrit'
	,1
	,'san'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Sinhala; Sinhalese'
	,1
	,'sin'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Slovak'
	,1
	,'slo (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Slovak'
	,1
	,'slo (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Slovenian'
	,1
	,'slv'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Northern Sami'
	,1
	,'sme'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Samoan'
	,1
	,'smo'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Shona'
	,1
	,'sna'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Sindhi'
	,1
	,'snd'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Somali'
	,1
	,'som'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Sotho, Southern'
	,1
	,'sot'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Spanish; Castilian'
	,1
	,'spa'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Albanian'
	,1
	,'alb (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Sardinian'
	,1
	,'srd'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Serbian'
	,1
	,'srp'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Swati'
	,1
	,'ssw'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Sundanese'
	,1
	,'sun'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Swahili'
	,1
	,'swa'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Swedish'
	,1
	,'swe'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Tahitian'
	,1
	,'tah'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Tamil'
	,1
	,'tam'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Tatar'
	,1
	,'tat'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Telugu'
	,1
	,'tel'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Tajik'
	,1
	,'tgk'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Tagalog'
	,1
	,'tgl'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Thai'
	,1
	,'tha'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Tibetan'
	,1
	,'tib (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Tigrinya'
	,1
	,'tir'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Tonga (Tonga Islands)'
	,1
	,'ton'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Tswana'
	,1
	,'tsn'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Tsonga'
	,1
	,'tso'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Turkmen'
	,1
	,'tuk'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Turkish'
	,1
	,'tur'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Twi'
	,1
	,'twi'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Uighur; Uyghur'
	,1
	,'uig'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Ukrainian'
	,1
	,'ukr'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Urdu'
	,1
	,'urd'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Uzbek'
	,1
	,'uzb'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Venda'
	,1
	,'ven'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Vietnamese'
	,1
	,'vie'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Volapük'
	,1
	,'vol'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Welsh'
	,1
	,'wel (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Walloon'
	,1
	,'wln'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Wolof'
	,1
	,'wol'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Xhosa'
	,1
	,'xho'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Yiddish'
	,1
	,'yid'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Yoruba'
	,1
	,'yor'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Zhuang; Chuang'
	,1
	,'zha'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Chinese'
	,1
	,'chi (B)'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Zulu'
	,1
	,'zul'
	)

INSERT INTO [#Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Spanish'
	,1
	,'spa'
	)

DECLARE @LanguageToBeDeleted VARCHAR(MAX);
DECLARE @languageEntityId INT;

--List of Languages that's needs to removed.
IF OBJECT_ID('tempdb..#tmpLanguagesToBeDeleted') IS NOT NULL
	DROP TABLE #tmpLanguagesToBeDeleted

SELECT *
INTO #tmpLanguagesToBeDeleted
FROM model.Languages
WHERE NAME NOT IN (
		SELECT NAME
		FROM #Languages
		)

DECLARE CursorToDropLanguage CURSOR FAST_FORWARD
FOR
SELECT NAME
FROM #tmpLanguagesToBeDeleted

OPEN CursorToDropLanguage

FETCH NEXT
FROM CursorToDropLanguage
INTO @LanguageToBeDeleted

WHILE @@FETCH_STATUS = 0
BEGIN
	IF EXISTS (
			SELECT Id
			FROM model.Languages
			WHERE NAME = @LanguageToBeDeleted
			)
	BEGIN
		SET @LanguageEntityId = (
				SELECT Id
				FROM model.PracticeRepositoryEntities
				WHERE NAME = 'Language'
				)

		--Remove Mappings
		DELETE
		FROM model.ExternalSystemEntityMappings
		WHERE ExternalSystemEntityKey = @LanguageToBeDeleted
			AND PracticeRepositoryEntityId = @LanguageEntityId

		--Remove Language Reference from Patient
		UPDATE model.Patients
		SET LanguageId = NULL
		WHERE LanguageId IN (
				SELECT Id
				FROM model.Languages
				WHERE NAME = @LanguageToBeDeleted
				);

		--Remove the Actual Row
		DELETE
		FROM model.Languages
		WHERE NAME = @LanguageToBeDeleted;
	END

	DELETE
	FROM #tmpLanguagesToBeDeleted
	WHERE NAME = @LanguageToBeDeleted

	FETCH NEXT
	FROM CursorToDropLanguage
	INTO @LanguageToBeDeleted
END

CLOSE CursorToDropLanguage
GO

--Create a Spanish row if doesn't exists.Initially we were removing Spanish too.
IF NOT EXISTS(SELECT * FROM model.Languages WHERE Name='Spanish')
INSERT INTO [model].[Languages] (
	[IsArchived]
	,[Name]
	,[OrdinalId]
	,[Abbreviation]
	)
VALUES (
	0
	,'Spanish'
	,1
	,'spa'
	)