﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_ApplicationSettings_UniqueSetting' AND object_id = OBJECT_ID(N'[model].[ApplicationSettings]'))
BEGIN
	-- Dropping obsolete settings with Type = 6 (PatientSearchPatientViewModel)
	DELETE FROM [model].[ApplicationSettings]
	WHERE [ApplicationSettingTypeId] = 6

	-- Dropping duplicate settings (a setting with MAX(Id) is kept)
	DELETE FROM [model].[ApplicationSettings]
	WHERE [Id] IN (
		SELECT appSettings.[Id]
		FROM [model].[ApplicationSettings] appSettings
		JOIN (
			SELECT [Name]
				,[MachineName]
				,[ApplicationSettingTypeId]
				,[UserId]
				,MAX(Id) As IdToKeep
			FROM [model].[ApplicationSettings]
			GROUP BY [Name], [MachineName], [ApplicationSettingTypeId], [UserId]
			HAVING COUNT(*) > 1
		) duplicateSettings ON
			appSettings.[Name] = duplicateSettings.[Name]
			AND appSettings.[ApplicationSettingTypeId] = duplicateSettings.[ApplicationSettingTypeId]
			AND (appSettings.[MachineName] = duplicateSettings.[MachineName] OR (appSettings.[MachineName] IS NULL AND duplicateSettings.[MachineName] IS NULL))
			AND (appSettings.[UserId] = duplicateSettings.[UserId] OR (appSettings.[UserId] IS NULL AND duplicateSettings.[UserId] IS NULL))
		WHERE appSettings.[Id] != duplicateSettings.IdToKeep)

	-- Unique index cannot be created on nvarchar(max) columns, so limiting to 1024
	ALTER TABLE [model].[ApplicationSettings] ALTER COLUMN [Name] nvarchar(1024) NOT NULL
	ALTER TABLE [model].[ApplicationSettings] ALTER COLUMN [MachineName] nvarchar(1024)

	-- Creating unique index
	CREATE UNIQUE NONCLUSTERED INDEX IX_ApplicationSettings_UniqueSetting ON [model].[ApplicationSettings] (
		   [Name]
		  ,[MachineName]
		  ,[ApplicationSettingTypeId]
		  ,[UserId])
END
GO