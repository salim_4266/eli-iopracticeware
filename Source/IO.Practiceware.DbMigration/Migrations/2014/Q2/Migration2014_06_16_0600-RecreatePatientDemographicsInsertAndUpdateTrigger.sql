﻿IF (OBJECT_ID('dbo.OnPatientDemographicInsertAndUpdate', 'TR') IS NOT NULL)
BEGIN
	DROP TRIGGER dbo.OnPatientDemographicInsertAndUpdate
END
GO

CREATE TRIGGER dbo.OnPatientDemographicInsertAndUpdate ON [dbo].[PatientDemographics]
INSTEAD OF INSERT
	,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PatientId INT
	DECLARE @SchedulePrimaryDoctor INT
	DECLARE @FirstName NVARCHAR(max)
	DECLARE @Status NVARCHAR(1)
	DECLARE @LastName NVARCHAR(max)
	DECLARE @MiddleInitial NVARCHAR(max)
	DECLARE @Salutation NVARCHAR(max)
	DECLARE @NameReference NVARCHAR(max)
	DECLARE @BirthDate NVARCHAR(90)
	DECLARE @Gender NVARCHAR(1)
	DECLARE @Marital NVARCHAR(1)
	DECLARE @Occupation NVARCHAR(max)
	DECLARE @OldPatient NVARCHAR(max)
	DECLARE @ReferralRequired NVARCHAR(1)
	DECLARE @SocialSecurity NVARCHAR(max)
	DECLARE @BusinessName NVARCHAR(max)
	DECLARE @Hipaa NVARCHAR(1)
	DECLARE @Ethnicity NVARCHAR(64)
	DECLARE @Language NVARCHAR(64)
	DECLARE @Race NVARCHAR(64)
	DECLARE @BusinessType NVARCHAR(1)
	DECLARE @Address NVARCHAR(max)
	DECLARE @Suite NVARCHAR(max)
	DECLARE @City NVARCHAR(max)
	DECLARE @State NVARCHAR(max)
	DECLARE @Zip NVARCHAR(max)
	DECLARE @BusinessAddress NVARCHAR(max)
	DECLARE @BusinessSuite NVARCHAR(max)
	DECLARE @BusinessCity NVARCHAR(max)
	DECLARE @BusinessState NVARCHAR(max)
	DECLARE @BusinessZip NVARCHAR(max)
	DECLARE @HomePhone NVARCHAR(max)
	DECLARE @CellPhone NVARCHAR(max)
	DECLARE @BusinessPhone NVARCHAR(max)
	DECLARE @EmployerPhone NVARCHAR(max)
	DECLARE @ReferringPhysician INT
	DECLARE @PrimaryCarePhysician INT
	DECLARE @ProfileComment1 NVARCHAR(max)
	DECLARE @ProfileComment2 NVARCHAR(max)
	DECLARE @Email NVARCHAR(max)
	DECLARE @EmergencyPhone NVARCHAR(max)
	DECLARE @EmergencyRel NVARCHAR(1)
	DECLARE @FinancialAssignment NVARCHAR(1)
	DECLARE @FinancialSignature NVARCHAR(1)
	DECLARE @MedicareSecondary NVARCHAR(64)
	DECLARE @BillParty NVARCHAR(1)
	DECLARE @SecondBillParty NVARCHAR(1)
	DECLARE @ReferralCatagory NVARCHAR(max)
	DECLARE @EmergencyName NVARCHAR(max)
	DECLARE @PolicyPatientId INT
	DECLARE @SecondPolicyPatientId INT
	DECLARE @Relationship NVARCHAR(1)
	DECLARE @SecondRelationship NVARCHAR(1)
	DECLARE @PatType NVARCHAR(max)
	DECLARE @BillingOffice INT
	DECLARE @PostOpExpireDate INT
	DECLARE @PostOpService INT
	DECLARE @FinancialSignatureDate NVARCHAR(64)
	DECLARE @Religion NVARCHAR(1)
	DECLARE @SecondRelationshipArchived VARCHAR(1)
	DECLARE @SecondPolicyPatientIdArchived INT
	DECLARE @RelationshipArchived NVARCHAR(1)
	DECLARE @PolicyPatientIdArchived INT
	DECLARE @BulkMailing NVARCHAR(1)
	DECLARE @ContactType NVARCHAR(1)
	DECLARE @NationalOrigin NVARCHAR(1)
	DECLARE @SendStatements NVARCHAR(1)
	DECLARE @SendConsults NVARCHAR(1)

	DECLARE PatientDemographicsInsertedUpdatedCursor CURSOR
	FOR
	SELECT PatientId
		,SchedulePrimaryDoctor
		,FirstName
		,STATUS
		,LastName
		,MiddleInitial
		,Salutation
		,NameReference
		,BirthDate
		,Gender
		,Marital
		,Occupation
		,OldPatient
		,ReferralRequired
		,SocialSecurity
		,BusinessName
		,Hipaa
		,Ethnicity
		,LANGUAGE
		,Race
		,BusinessType
		,Address
		,Suite
		,City
		,STATE
		,Zip
		,BusinessAddress
		,BusinessSuite
		,BusinessCity
		,BusinessState
		,BusinessZip
		,HomePhone
		,CellPhone
		,BusinessPhone
		,EmployerPhone
		,ReferringPhysician
		,PrimaryCarePhysician
		,ProfileComment1
		,ProfileComment2
		,Email
		,EmergencyPhone
		,EmergencyRel
		,FinancialAssignment
		,FinancialSignature
		,MedicareSecondary
		,BillParty
		,SecondBillParty
		,ReferralCatagory
		,EmergencyName
		,PolicyPatientId
		,SecondPolicyPatientId
		,Relationship
		,SecondRelationship
		,PatType
		,BillingOffice
		,PostOpExpireDate
		,PostOpService
		,FinancialSignatureDate
		,Religion
		,SecondRelationshipArchived
		,SecondPolicyPatientIdArchived
		,RelationshipArchived
		,PolicyPatientIdArchived
		,BulkMailing
		,ContactType
		,NationalOrigin
		,SendStatements
		,SendConsults
	FROM inserted

	OPEN PatientDemographicsInsertedUpdatedCursor

	FETCH NEXT
	FROM PatientDemographicsInsertedUpdatedCursor
	INTO @PatientId
		,@SchedulePrimaryDoctor
		,@FirstName
		,@Status
		,@LastName
		,@MiddleInitial
		,@Salutation
		,@NameReference
		,@BirthDate
		,@Gender
		,@Marital
		,@Occupation
		,@OldPatient
		,@ReferralRequired
		,@SocialSecurity
		,@BusinessName
		,@Hipaa
		,@Ethnicity
		,@Language
		,@Race
		,@BusinessType
		,@Address
		,@Suite
		,@City
		,@State
		,@Zip
		,@BusinessAddress
		,@BusinessSuite
		,@BusinessCity
		,@BusinessState
		,@BusinessZip
		,@HomePhone
		,@CellPhone
		,@BusinessPhone
		,@EmployerPhone
		,@ReferringPhysician
		,@PrimaryCarePhysician
		,@ProfileComment1
		,@ProfileComment2
		,@Email
		,@EmergencyPhone
		,@EmergencyRel
		,@FinancialAssignment
		,@FinancialSignature
		,@MedicareSecondary
		,@BillParty
		,@SecondBillParty
		,@ReferralCatagory
		,@EmergencyName
		,@PolicyPatientId
		,@SecondPolicyPatientId
		,@Relationship
		,@SecondRelationship
		,@PatType
		,@BillingOffice
		,@PostOpExpireDate
		,@PostOpService
		,@FinancialSignatureDate
		,@Religion
		,@SecondRelationshipArchived
		,@SecondPolicyPatientIdArchived
		,@RelationshipArchived
		,@PolicyPatientIdArchived
		,@BulkMailing
		,@ContactType
		,@NationalOrigin
		,@SendStatements
		,@SendConsults

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE @employmentStatusId AS INT

		SET @employmentStatusId = (
				SELECT TOP 1 Id
				FROM model.PatientEmploymentStatus pes
				WHERE @BusinessType = SUBSTRING(pes.NAME, 1, 1)
				)

		DECLARE @maritalStatusId AS INT

		SET @maritalStatusId = CASE @Marital
				WHEN 'S'
					THEN 1
				WHEN 'M'
					THEN 2
				WHEN 'D'
					THEN 3
				WHEN 'W'
					THEN 4
				ELSE NULL
				END

		DECLARE @genderId AS INT

		SET @genderId = CASE @Gender
				WHEN 'U'
					THEN 1
				WHEN 'F'
					THEN 2
				WHEN 'M'
					THEN 3
				ELSE 4
				END

		DECLARE @languageId AS INT

		SET @languageId = (
				SELECT TOP 1 Id
				FROM model.Languages pl
				WHERE SUBSTRING(pl.NAME, 1, 10) = @Language
				)

		DECLARE @defaultUserId AS INT

		SET @defaultUserId = CASE @SchedulePrimaryDoctor
				WHEN 0
					THEN NULL
				ELSE @SchedulePrimaryDoctor
				END

		DECLARE @isClinical AS BIT

		SET @isClinical = CASE @Status
				WHEN 'I'
					THEN CONVERT(BIT, 0)
				ELSE CONVERT(BIT, 1)
				END

		DECLARE @patientStatusId AS INT

		SET @patientStatusId = CASE @Status
				WHEN 'C'
					THEN 2
				WHEN 'D'
					THEN 3
				WHEN 'M'
					THEN 4
				ELSE 1
				END

		DECLARE @isHipaaConsentSigned AS BIT

		SET @isHipaaConsentSigned = CASE @Hipaa
				WHEN 'T'
					THEN CONVERT(BIT, 1)
				ELSE CONVERT(BIT, 0)
				END

		IF EXISTS (
			SELECT TOP 1 * 
			FROM model.PatientCommunicationPreferences pcp
			WHERE pcp.PatientId = @PatientId 
				AND PatientCommunicationTypeId = 3
		)
		BEGIN
			UPDATE pcp
			SET pcp.IsOptOut = CASE @SendStatements
					WHEN 'Y'
						THEN CONVERT(BIT, 0)
					ELSE CONVERT(BIT, 1)
					END
			FROM model.PatientCommunicationPreferences pcp
			WHERE pcp.PatientId = @PatientId 
				AND PatientCommunicationTypeId = 3
		END

		DECLARE @isCollectionLetterSuppressed AS BIT

		SET @isCollectionLetterSuppressed = CASE @SendConsults
				WHEN 'Y'
					THEN CONVERT(BIT, 0)
				ELSE CONVERT(BIT, 1)
				END

		DECLARE @RaceId AS INT

		SET @RaceId = (
				SELECT TOP 1 Id
				FROM model.Races pr
				WHERE pr.NAME = @Race
				)

		DECLARE @EthnicityId AS INT

		SET @EthnicityId = (
				SELECT TOP 1 Id
				FROM model.Ethnicities pe
				WHERE pe.NAME = @Ethnicity
				)

		DECLARE @releaseOfInformationCodeId AS INT

		SET @releaseOfInformationCodeId = CASE @FinancialSignature
				WHEN 'Y'
					THEN 1
				ELSE 0
				END

		DECLARE @dateOfBirth AS DATETIME

		SET @dateOfBirth = CONVERT(DATETIME, @BirthDate, 112)

		DECLARE @releaseOfInformationDateTime AS DATETIME

		SET @releaseOfInformationDateTime = CONVERT(DATETIME, @FinancialSignatureDate, 112)

		IF (COALESCE(@PatientId, 0) = 0)
		BEGIN
			INSERT INTO model.Patients (
				LastName
				,FirstName
				,MiddleName
				,Suffix
				,SocialSecurityNumber
				,Occupation
				,EmployerName
				,PatientEmploymentStatusId
				,GenderId
				,MaritalStatusId
				,DateOfBirth
				,LanguageId
				,DefaultUserId
				,ReleaseOfInformationCodeId
				,ReleaseOfInformationDateTime
				,IsClinical
				,PatientStatusId
				,Prefix
				,IsHipaaConsentSigned
				,IsCollectionLetterSuppressed
				,PriorPatientCode
				,EthnicityId
				)
			VALUES (
				@LastName
				,@FirstName
				,@MiddleInitial
				,@NameReference
				,@SocialSecurity
				,@Occupation
				,@BusinessName
				,@employmentStatusId
				,@genderId
				,@maritalStatusId
				,@dateOfBirth
				,@languageId
				,@defaultUserId
				,@releaseOfInformationCodeId
				,@releaseOfInformationDateTime
				,@isClinical
				,@patientStatusId
				,@Salutation
				,@isHipaaConsentSigned
				,@isCollectionLetterSuppressed
				,@OldPatient
				,@EthnicityId
				)

			SET @PatientId = IDENT_CURRENT('model.Patients')
		END
		ELSE
		BEGIN
			UPDATE model.Patients
			SET LastName = @LastName
				,FirstName = @FirstName
				,MiddleName = @MiddleInitial
				,Suffix = @NameReference
				,SocialSecurityNumber = @SocialSecurity
				,Occupation = @Occupation
				,EmployerName = @BusinessName
				,PatientEmploymentStatusId = @employmentStatusId
				,GenderId = @genderId
				,MaritalStatusId = @maritalStatusId
				,DateOfBirth = @dateOfBirth
				,LanguageId = @languageId
				,DefaultUserId = @defaultUserId
				,IsClinical = @isClinical
				,PatientStatusId = @patientStatusId
				,Prefix = @Salutation
				,IsHipaaConsentSigned = @isHipaaConsentSigned
				,IsCollectionLetterSuppressed = @isCollectionLetterSuppressed
				,PriorPatientCode = @OldPatient
				,EthnicityId = @EthnicityId
			WHERE Id = @PatientId
		END

		---- Update or insert Primary PatientEmailAddress
		IF (COALESCE(@Email, '') <> '')
		BEGIN
			IF EXISTS (
					SELECT *
					FROM model.PatientEmailAddresses
					WHERE PatientId = @PatientId
						AND OrdinalId = 1
						AND EmailAddressTypeId = 1
					)
			BEGIN
				UPDATE model.PatientEmailAddresses
				SET Value = @Email
				WHERE PatientId = @PatientId
					AND OrdinalId = 1
					AND EmailAddressTypeId = 1
			END
			ELSE
			BEGIN
				INSERT INTO model.PatientEmailAddresses (
					Value
					,OrdinalId
					,PatientId
					,EmailAddressTypeId
					)
				VALUES (
					@Email
					,1
					,@PatientId
					,1
					)
			END
		END
		ELSE
		BEGIN
			DELETE
			FROM model.PatientEmailAddresses
			WHERE PatientId = @PatientId
				AND OrdinalId = 1
				AND EmailAddressTypeId = 1
		END

		DECLARE @homeAddressId AS INT

		SET @homeAddressId = (
				SELECT TOP 1 Id
				FROM model.PatientAddresses pa
				WHERE pa.PatientId = @PatientId
					AND pa.PatientAddressTypeId = 1
				)

		---- If no home address exists, detect whether any home address values are set, and add a new home address if possible
		DECLARE @postalCode AS NVARCHAR(max)
		DECLARE @stateOrProvinceId AS INT

		SET @stateOrProvinceId = (
				SELECT TOP 1 Id
				FROM model.StateOrProvinces st
				WHERE st.Abbreviation = @State
				)

		IF (COALESCE(@homeAddressId, 0) = 0)
		BEGIN
			IF (
					(COALESCE(@Address, '') <> '')
					OR (COALESCE(@Suite, '') <> '')
					OR (COALESCE(@City, '') <> '')
					OR (COALESCE(@State, '') <> '')
					OR (COALESCE(@Zip, '') <> '')
					)
			BEGIN
				SET @postalCode = REPLACE(@Zip, '-', '')

				INSERT INTO model.PatientAddresses (
					Line1
					,Line2
					,City
					,PatientId
					,PostalCode
					,StateOrProvinceId
					,PatientAddressTypeId
					,OrdinalId
					)
				VALUES (
					@Address
					,@Suite
					,@City
					,@PatientId
					,@postalCode
					,@stateOrProvinceId
					,1
					,1
					)
			END
		END
				---- Otherwise, update the existing home address values
		ELSE
		BEGIN
			SET @postalCode = REPLACE(@Zip, '-', '')

			UPDATE model.PatientAddresses
			SET Line1 = @Address
				,Line2 = @Suite
				,City = @City
				,PostalCode = @postalCode
				,StateOrProvinceId = @stateOrProvinceId
			WHERE Id = @homeAddressId
		END

		DECLARE @businessAddressId AS INT

		SET @businessAddressId = (
				SELECT TOP 1 Id
				FROM model.PatientAddresses pa
				WHERE pa.PatientId = @PatientId
					AND pa.PatientAddressTypeId = 2
				)
		---- If no business address exists, detect whether any business address values are set, and add a new business address if possible
		SET @postalCode = REPLACE(@BusinessZip, '-', '')
		SET @stateOrProvinceId = (
				SELECT TOP 1 Id
				FROM model.StateOrProvinces st
				WHERE st.Abbreviation = @BusinessState
				)

		IF (COALESCE(@businessAddressId, 0) = 0)
		BEGIN
			IF (
					(COALESCE(@BusinessAddress, '') <> '')
					OR (COALESCE(@BusinessSuite, '') <> '')
					OR (COALESCE(@BusinessCity, '') <> '')
					OR (COALESCE(@BusinessState, '') <> '')
					OR (COALESCE(@BusinessZip, '') <> '')
					)
			BEGIN
				INSERT INTO model.PatientAddresses (
					Line1
					,Line2
					,City
					,PatientId
					,PostalCode
					,StateOrProvinceId
					,PatientAddressTypeId
					,OrdinalId
					)
				VALUES (
					@BusinessAddress
					,@BusinessSuite
					,@BusinessCity
					,@PatientId
					,@postalCode
					,@stateOrProvinceId
					,2
					,2
					)
			END
		END
				---- Otherwise, update the existing home address values
		ELSE
		BEGIN
			UPDATE model.PatientAddresses
			SET Line1 = @BusinessAddress
				,Line2 = @BusinessSuite
				,City = @BusinessCity
				,PostalCode = @postalCode
				,StateOrProvinceId = @stateOrProvinceId
			WHERE Id = @businessAddressId
		END

		---- Update or insert Primary PatientEmailAddress
		IF (COALESCE(@Email, '') <> '')
		BEGIN
			UPDATE model.PatientEmailAddresses
			SET Value = @Email
			WHERE PatientId = @PatientId
				AND OrdinalId = 1
				AND EmailAddressTypeId = 1
		END
		ELSE
		BEGIN
			DELETE
			FROM model.PatientEmailAddresses
			WHERE PatientId = @PatientId
				AND OrdinalId = 1
				AND EmailAddressTypeId = 1
		END

		DECLARE @homePhoneNumberId AS INT

		SET @homePhoneNumberId = (
				SELECT TOP 1 Id
				FROM model.PatientPhoneNumbers pn
				WHERE pn.PatientId = @PatientId
					AND pn.PatientPhoneNumberTypeId = 7
				)

		---- Insert new Home PatientPhoneNumber if possible
		IF (COALESCE(@homePhoneNumberId, 0) = 0)
		BEGIN
			IF (COALESCE(@HomePhone, '') <> '')
			BEGIN
				INSERT INTO model.PatientPhoneNumbers (
					ExchangeAndSuffix
					,IsInternational
					,OrdinalId
					,PatientId
					,PatientPhoneNumberTypeId
					)
				VALUES (
					@HomePhone
					,CONVERT(BIT, 0)
					,1
					,@PatientId
					,7
					)
			END
		END
				---- Otherwise update Home PatientPhoneNumber
		ELSE
		BEGIN
			IF (COALESCE(@HomePhone, '') <> '')
			BEGIN
				UPDATE model.PatientPhoneNumbers
				SET ExchangeAndSuffix = @HomePhone
				WHERE PatientId = @PatientId
					AND PatientPhoneNumberTypeId = 7
					AND OrdinalId = 1
			END
			ELSE
			BEGIN
				DELETE
				FROM model.PatientPhoneNumbers
				WHERE PatientId = @PatientId
					AND PatientPhoneNumberTypeId = 7
					AND OrdinalId = 1
			END
		END

		DECLARE @cellPhoneNumberId AS INT

		SET @cellPhoneNumberId = (
				SELECT TOP 1 Id
				FROM model.PatientPhoneNumbers pn
				WHERE pn.PatientId = @PatientId
					AND pn.PatientPhoneNumberTypeId = 3
				)

		---- Insert new Cell PatientPhoneNumber if possible
		IF (COALESCE(@cellPhoneNumberId, 0) = 0)
		BEGIN
			IF (COALESCE(@CellPhone, '') <> '')
			BEGIN
				INSERT INTO model.PatientPhoneNumbers (
					ExchangeAndSuffix
					,IsInternational
					,OrdinalId
					,PatientId
					,PatientPhoneNumberTypeId
					)
				VALUES (
					@CellPhone
					,CONVERT(BIT, 0)
					,3
					,@PatientId
					,3
					)
			END
		END
				---- Otherwise update Cell PatientPhoneNumber
		ELSE
		BEGIN
			IF (COALESCE(@CellPhone, '') <> '')
			BEGIN
				UPDATE model.PatientPhoneNumbers
				SET ExchangeAndSuffix = @CellPhone
				WHERE PatientId = @PatientId
					AND PatientPhoneNumberTypeId = 3
					AND OrdinalId = 3
			END
			ELSE
			BEGIN
				DELETE
				FROM model.PatientPhoneNumbers
				WHERE PatientId = @PatientId
					AND PatientPhoneNumberTypeId = 7
					AND OrdinalId = 3
			END
		END

		DECLARE @businessPhoneNumberId AS INT

		SET @businessPhoneNumberId = (
				SELECT TOP 1 Id
				FROM model.PatientPhoneNumbers pn
				WHERE pn.PatientId = @PatientId
					AND pn.PatientPhoneNumberTypeId = 2
				)

		---- Insert new Business PatientPhoneNumber if possible
		IF (COALESCE(@businessPhoneNumberId, 0) = 0)
		BEGIN
			IF (COALESCE(@BusinessPhone, '') <> '')
			BEGIN
				INSERT INTO model.PatientPhoneNumbers (
					ExchangeAndSuffix
					,IsInternational
					,OrdinalId
					,PatientId
					,PatientPhoneNumberTypeId
					)
				VALUES (
					@BusinessPhone
					,CONVERT(BIT, 0)
					,2
					,@PatientId
					,2
					)
			END
		END
				---- Otherwise update Business PatientPhoneNumber
		ELSE
		BEGIN
			IF (COALESCE(@BusinessPhone, '') <> '')
			BEGIN
				UPDATE model.PatientPhoneNumbers
				SET ExchangeAndSuffix = @BusinessPhone
				WHERE PatientId = @PatientId
					AND PatientPhoneNumberTypeId = 2
					AND OrdinalId = 2
			END
			ELSE
			BEGIN
				DELETE
				FROM model.PatientPhoneNumbers
				WHERE PatientId = @PatientId
					AND PatientPhoneNumberTypeId = 2
					AND OrdinalId = 2
			END
		END

		IF EXISTS (
				SELECT *
				FROM model.ExternalContacts
				WHERE Id = @ReferringPhysician
				)
		BEGIN
			DECLARE @referringPhysicianExternalProviderId AS INT

			SET @referringPhysicianExternalProviderId = (
					SELECT TOP 1 Id
					FROM model.PatientExternalProviders pep
					WHERE pep.Id = @PatientId
					)

			---- Insert new ReferringPhysician PatientExternalProvider if possible
			IF (COALESCE(@referringPhysicianExternalProviderId, 0) = 0)
			BEGIN
				SET IDENTITY_INSERT [model].[PatientExternalProviders] ON

				INSERT INTO model.PatientExternalProviders (
					Id
					,ExternalProviderId
					,IsPrimaryCarePhysician
					,IsReferringPhysician
					,PatientId
					)
				VALUES (
					@PatientId
					,@ReferringPhysician
					,CONVERT(BIT, 0)
					,CONVERT(BIT, 1)
					,@PatientId
					)

				SET IDENTITY_INSERT [model].[PatientExternalProviders] OFF
			END
					---- Otherwise update ReferringPhysician PatientExternalProvider
			ELSE
			BEGIN
				UPDATE model.PatientExternalProviders
				SET ExternalProviderId = @ReferringPhysician
				WHERE Id = @PatientId
			END
		END

		IF EXISTS (
				SELECT *
				FROM model.ExternalContacts
				WHERE Id = @PrimaryCarePhysician
				)
		BEGIN
			DECLARE @PatientExternalProviderMax INT

			SET @PatientExternalProviderMax = 110000000

			DECLARE @primaryCarePhysicianExternalProviderId AS INT

			SET @primaryCarePhysicianExternalProviderId = (
					SELECT TOP 1 Id
					FROM model.PatientExternalProviders pep
					WHERE pep.Id = model.GetPairId(1, @PatientId, @PatientExternalProviderMax)
					)

			---- Insert new PrimaryCarePhysician PatientExternalProvider if possible
			IF (COALESCE(@primaryCarePhysicianExternalProviderId, 0) = 0)
			BEGIN
				SET IDENTITY_INSERT [model].[PatientExternalProviders] ON

				INSERT INTO model.PatientExternalProviders (
					Id
					,ExternalProviderId
					,IsPrimaryCarePhysician
					,IsReferringPhysician
					,PatientId
					)
				VALUES (
					model.GetPairId(1, @PatientId, @PatientExternalProviderMax)
					,@PrimaryCarePhysician
					,CONVERT(BIT, 1)
					,CONVERT(BIT, 0)
					,@PatientId
					)

				SET IDENTITY_INSERT [model].[PatientExternalProviders] OFF
			END
			ELSE
				---- Otherwise update PrimaryCarePhysician PatientExternalProvider
			BEGIN
				UPDATE model.PatientExternalProviders
				SET ExternalProviderId = @PrimaryCarePhysician
				WHERE Id = model.GetPairId(1, @PatientId, @PatientExternalProviderMax)
			END
		END

		DECLARE @CommentMax INT

		SET @CommentMax = 110000000

		DECLARE @patientCommentId AS INT

		SET @patientCommentId = model.GetPairId(2, @PatientId, @CommentMax)

		---- Insert new PatientComment if possible
		IF NOT EXISTS (
				SELECT *
				FROM model.PatientComments
				WHERE Id = @patientCommentId
				)
		BEGIN
			IF (COALESCE(@ProfileComment1, '') <> '')
			BEGIN
				SET IDENTITY_INSERT [model].[PatientComments] ON

				INSERT INTO model.PatientComments (
					Id
					,PatientId
					,Value
					,PatientCommentTypeId
					)
				VALUES (
					@patientCommentId
					,@PatientId
					,@ProfileComment1
					,2
					)

				SET IDENTITY_INSERT [model].[PatientComments] OFF
			END
		END
				---- Otherwise update PatientComment
		ELSE
		BEGIN
			UPDATE model.PatientComments
			SET Value = @ProfileComment1
			WHERE Id = @patientCommentId
		END

		---- Insert new PatientReferralSource if possible
		DECLARE @referralSourceTypeId AS INT

		SET @referralSourceTypeId = (
				SELECT TOP 1 Id
				FROM dbo.PracticeCodeTable pct
				WHERE pct.Code = @ReferralCatagory
					AND pct.ReferenceType = 'REFERBY'
				)

		IF NOT EXISTS (
				SELECT *
				FROM model.PatientReferralSources prs
				WHERE prs.Id = @PatientId
				)
		BEGIN
			IF (
					COALESCE(@ReferralCatagory, '') <> ''
					AND COALESCE(@referralSourceTypeId, 0) <> 0
					)
			BEGIN
				SET IDENTITY_INSERT [model].[PatientReferralSources] ON

				INSERT INTO model.PatientReferralSources (
					Id
					,PatientId
					,ReferralSourceTypeId
					,Comment
					)
				VALUES (
					@PatientId
					,@PatientId
					,@referralSourceTypeId
					,@ProfileComment2
					)

				SET IDENTITY_INSERT [model].[PatientReferralSources] OFF
			END
		END
				---- Otherwise update PatientReferralSource
		ELSE
		BEGIN
			UPDATE model.PatientReferralSources
			SET Comment = @ProfileComment2
			WHERE Id = @PatientId
		END

		DECLARE @communicationPreferenceAddressId AS INT

		SET @communicationPreferenceAddressId = CASE @BillParty
				WHEN 'Y'
					THEN (
							SELECT TOP 1 pad.Id
							FROM model.PatientAddresses pad
							WHERE pad.PatientId = ISNULL(dbo.GetPolicyHolderPatientId(@PatientId), @PatientId)
							ORDER BY pad.Id
							)
				ELSE (
						SELECT TOP 1 pad.Id
						FROM model.PatientAddresses pad
						WHERE pad.PatientId = @PatientId
						ORDER BY pad.Id
						)
				END

		IF (COALESCE(@communicationPreferenceAddressId, 0) <> 0)
		BEGIN
			---- Insert new PatientCommunicationPreference if possible
			IF NOT EXISTS (
					SELECT *
					FROM model.PatientCommunicationPreferences pcp
					WHERE pcp.PatientId = @PatientId
						AND pcp.PatientCommunicationTypeId = 3
						AND pcp.CommunicationMethodTypeId = 3
						AND pcp.__EntityType__ = 'PatientAddressCommunicationPreference'
					)
			BEGIN
				INSERT INTO model.PatientCommunicationPreferences (
					PatientId
					,PatientCommunicationTypeId
					,CommunicationMethodTypeId
					,IsAddressedToRecipient
					,PatientAddressId
					,__EntityType__
					)
				VALUES (
					@PatientId
					,3
					,3
					,CONVERT(BIT, 1)
					,@communicationPreferenceAddressId
					,'PatientAddressCommunicationPreference'
					)
			END
					---- Otherwise update PatientCommunicationPreference
			ELSE
			BEGIN
				IF EXISTS (
						SELECT *
						FROM model.PatientCommunicationPreferences
						WHERE PatientId = @PatientId
							AND PatientCommunicationTypeId = 3
							AND CommunicationMethodTypeId = 3
							AND __EntityType__ = 'PatientAddressCommunicationPreference'
							AND PatientAddressId <> @communicationPreferenceAddressId
						)
				BEGIN
					UPDATE model.PatientCommunicationPreferences
					SET PatientAddressId = @communicationPreferenceAddressId
					WHERE PatientId = @PatientId
						AND PatientCommunicationTypeId = 3
						AND CommunicationMethodTypeId = 3
						AND __EntityType__ = 'PatientAddressCommunicationPreference'
						AND PatientAddressId <> @communicationPreferenceAddressId
				END
			END
		END

		DECLARE @patientTypeId AS INT

		SET @patientTypeId = (
				SELECT TOP 1 Id
				FROM model.Tags pt
				WHERE pt.DisplayName = @PatType
				)

		---- Insert new PatientTags if possible
		IF NOT EXISTS (
				SELECT *
				FROM model.PatientTags ppt
				WHERE ppt.PatientId = @PatientId
					AND ppt.OrdinalId = 1
				)
		BEGIN
			IF (COALESCE(@patientTypeId, 0) <> 0)
			BEGIN
				INSERT INTO model.PatientTags (
					PatientId
					,TagId
					,OrdinalId
					)
				VALUES (
					@PatientId
					,@patientTypeId
					,1
					)
			END
		END
		ELSE
			---- Otherwise update PatientTags
		BEGIN
			IF (COALESCE(@patientTypeId, 0) <> 0)
			BEGIN
				UPDATE model.PatientTags
				SET TagId = @patientTypeId
				WHERE PatientId = @PatientId
					AND OrdinalId = 1
			END
			ELSE
			BEGIN
				DELETE
				FROM model.PatientTags
				WHERE PatientId = @PatientId
					AND OrdinalId = 1
			END
		END

		DECLARE @employerPhoneNumberTypeId AS INT

		SET @employerPhoneNumberTypeId = (
				SELECT TOP 1 Id + 1000
				FROM dbo.PracticeCodeTable pct
				WHERE pct.Code = 'Employer'
					AND pct.ReferenceType = 'PHONETYPEPATIENT'
				)

		DECLARE @employerPhoneNumberId AS INT

		SET @employerPhoneNumberId = (
				SELECT TOP 1 Id
				FROM model.PatientPhoneNumbers pn
				WHERE pn.PatientId = @PatientId
					AND pn.PatientPhoneNumberTypeId = @employerPhoneNumberTypeId
				)

		---- Insert new Employer PatientPhoneNumber if possible
		IF (COALESCE(@employerPhoneNumberId, 0) = 0)
		BEGIN
			IF (COALESCE(@EmployerPhone, '') <> '')
			BEGIN
				INSERT INTO model.PatientPhoneNumbers (
					ExchangeAndSuffix
					,IsInternational
					,OrdinalId
					,PatientId
					,PatientPhoneNumberTypeId
					)
				VALUES (
					@EmployerPhone
					,CONVERT(BIT, 0)
					,6
					,@PatientId
					,@employerPhoneNumberTypeId
					)
			END
		END
				---- Otherwise update Business PatientPhoneNumber
		ELSE
		BEGIN
			UPDATE model.PatientPhoneNumbers
			SET ExchangeAndSuffix = @EmployerPhone
			WHERE PatientId = @PatientId
				AND PatientPhoneNumberTypeId = @employerPhoneNumberId
		END

		UPDATE model.InsurancePolicies
		SET MedicareSecondaryReasonCodeId = CASE @MedicareSecondary
				WHEN '12'
					THEN 1
				WHEN '13'
					THEN 2
				WHEN '14'
					THEN 3
				WHEN '15'
					THEN 4
				WHEN '16'
					THEN 5
				WHEN '41'
					THEN 6
				WHEN '42'
					THEN 7
				WHEN '43'
					THEN 8
				WHEN '47'
					THEN 9
				ELSE NULL
				END
		WHERE PolicyHolderPatientId = @PatientId
			AND EXISTS (
				SELECT *
				FROM dbo.PracticeInsurers pins
				WHERE pins.InsurerId = InsurerId
					AND pins.InsurerReferenceCode IN (
						'M'
						,'N'
						,'['
						)
				)

		---- Insert new PatientRace
		IF (COALESCE(@RaceId, 0) <> 0)
		BEGIN
			IF NOT EXISTS (
				SELECT *
				FROM model.PatientRace pr
				WHERE pr.Patients_Id = @PatientId
					AND Races_Id = @RaceId
				)
			BEGIN
			INSERT INTO model.PatientRace (
				Patients_Id
				,Races_Id
				)
			VALUES (
				@PatientId
				,@RaceId
				)
			END
		END

		FETCH NEXT
		FROM PatientDemographicsInsertedUpdatedCursor
		INTO @PatientId
			,@SchedulePrimaryDoctor
			,@FirstName
			,@Status
			,@LastName
			,@MiddleInitial
			,@Salutation
			,@NameReference
			,@BirthDate
			,@Gender
			,@Marital
			,@Occupation
			,@OldPatient
			,@ReferralRequired
			,@SocialSecurity
			,@BusinessName
			,@Hipaa
			,@Ethnicity
			,@Language
			,@Race
			,@BusinessType
			,@Address
			,@Suite
			,@City
			,@State
			,@Zip
			,@BusinessAddress
			,@BusinessSuite
			,@BusinessCity
			,@BusinessState
			,@BusinessZip
			,@HomePhone
			,@CellPhone
			,@BusinessPhone
			,@EmployerPhone
			,@ReferringPhysician
			,@PrimaryCarePhysician
			,@ProfileComment1
			,@ProfileComment2
			,@Email
			,@EmergencyPhone
			,@EmergencyRel
			,@FinancialAssignment
			,@FinancialSignature
			,@MedicareSecondary
			,@BillParty
			,@SecondBillParty
			,@ReferralCatagory
			,@EmergencyName
			,@PolicyPatientId
			,@SecondPolicyPatientId
			,@Relationship
			,@SecondRelationship
			,@PatType
			,@BillingOffice
			,@PostOpExpireDate
			,@PostOpService
			,@FinancialSignatureDate
			,@Religion
			,@SecondRelationshipArchived
			,@SecondPolicyPatientIdArchived
			,@RelationshipArchived
			,@PolicyPatientIdArchived
			,@BulkMailing
			,@ContactType
			,@NationalOrigin
			,@SendStatements
			,@SendConsults
	END

	CLOSE PatientDemographicsInsertedUpdatedCursor

	DEALLOCATE PatientDemographicsInsertedUpdatedCursor

	SET NOCOUNT OFF

	SELECT @PatientId AS SCOPE_ID_COLUMN
END
GO

-- Drop IsStatementSuppressed column from model.Patients
IF EXISTS (SELECT * FROM sys.columns c WHERE c.object_id = OBJECT_ID('model.Patients','U') AND c.name = 'IsStatementSuppressed')
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = ''

	SELECT @sql = 'ALTER TABLE model.Patients DROP CONSTRAINT ' + name FROM sys.default_constraints WHERE parent_object_id = OBJECT_ID('model.Patients','U') 
	AND parent_column_id = (SELECT c.column_id FROM sys.columns c WHERE c.object_id = OBJECT_ID('model.Patients','U') AND c.name = 'IsStatementSuppressed')

	EXEC sp_executesql @statement = @sql

	ALTER TABLE model.Patients DROP COLUMN [IsStatementSuppressed]
END
GO

--Drop Patient Audit Trigger's
IF OBJECT_ID(N'[model].[AuditPatientsDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientsDeleteTrigger];
END

IF OBJECT_ID(N'[model].[AuditPatientsInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientsInsertTrigger];
END

IF OBJECT_ID(N'[model].[AuditPatientsUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditPatientsUpdateTrigger];
END
GO