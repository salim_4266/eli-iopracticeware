﻿--Bug #11813.
--Adding no Problems
IF NOT EXISTS (
		SELECT *
		FROM PracticeCodeTable
		WHERE ReferenceType = 'CLINICALTEMPLATES'
			AND Code = 'No Known Problems'
		)
BEGIN
	DECLARE @MaxRank AS INT;

	SELECT @MaxRank = MAX(Rank)
	FROM PracticeCodeTable
	WHERE ReferenceType = 'CLINICALTEMPLATES';

	INSERT INTO [PracticeCodeTable] (
		ReferenceType
		,Code
		,Rank
		,AlternateCode
		)
	VALUES (
		'CLINICALTEMPLATES'
		,'No Known Problems'
		,@MaxRank + 1
		,'F'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM PracticeCodeTable
		WHERE ReferenceType = 'NO KNOWN PROBLEMS'
		)
BEGIN
	DECLARE @MedicalSystem AS NVARCHAR(1);
	DECLARE @AlternateCode AS NVARCHAR(128);
	DECLARE @Rank AS INT;
	DECLARE @Name AS NVARCHAR(100);

	SET @Name = 'No known problems									  ';

	SELECT TOP 1 @MedicalSystem = MedicalSystem
	FROM dm.PrimaryDiagnosisTable
	WHERE ReviewSystemLingo = 'No known problems'

	SELECT TOP 1 @Rank = Rank
	FROM PracticeCodeTable
	WHERE ReferenceType = 'CLINICALTEMPLATES'
		AND AlternateCode = 'F'
		AND Code = 'No Known Problems';

	SET @AlternateCode = '0001' + '       ' + @Name + CAST(@MedicalSystem AS NVARCHAR(1)) + ':' + CAST(@Rank AS NVARCHAR(20));

	IF @AlternateCode IS NOT NULL
		INSERT INTO [PracticeCodeTable] (
			ReferenceType
			,Code
			,Rank
			,AlternateCode
			)
		VALUES (
			'NO KNOWN PROBLEMS'
			,'SYSTEMNORMAL' + CAST(@MedicalSystem AS NVARCHAR(1)) + ':' + CAST(@Rank AS NVARCHAR(20)) + ''
			,0
			,@AlternateCode
			)
END
GO

--Map No Known Problems to 160245001.
DECLARE @PracticeRepositoryEntityId INT

SELECT TOP 1 @PracticeRepositoryEntityId = Id
FROM model.PracticeRepositoryEntities
WHERE NAME = 'ClinicalCondition'

DECLARE @ExternalSystemId INT

SELECT TOP 1 @ExternalSystemId = Id
FROM model.ExternalSystems
WHERE NAME = 'SnomedCt'

DECLARE @ExternalSystemEntityId INT

SELECT TOP 1 @ExternalSystemEntityId = Id
FROM model.ExternalSystemEntities
WHERE ExternalSystemId = @ExternalSystemId
	AND NAME = 'ClinicalCondition'

DECLARE @PracticeRepositoryEntityKey INT

EXEC sp_executesql N'IF OBJECT_ID(''model.ClinicalConditions'') IS NOT NULL
				SELECT TOP 1  @PracticeRepositoryEntityKey1=Id FROM model.ClinicalConditions WHERE Name=''No Known Problems'' ORDER BY Id ASC'
	,N'@PracticeRepositoryEntityKey1 INT OUTPUT'
	,@PracticeRepositoryEntityKey1 = @PracticeRepositoryEntityKey OUTPUT

SELECT @PracticeRepositoryEntityId, @ExternalSystemId, @PracticeRepositoryEntityKey,@ExternalSystemEntityId

IF NOT EXISTS (
		SELECT *
		FROM model.ExternalSystemEntityMappings esem
		WHERE esem.ExternalSystemEntityId = @ExternalSystemEntityId
			AND esem.ExternalSystemEntityKey = '160245001'
			AND esem.PracticeRepositoryEntityId = @PracticeRepositoryEntityId
			AND esem.PracticeRepositoryEntityKey = @PracticeRepositoryEntityKey
		)
BEGIN
	INSERT INTO model.ExternalSystemEntityMappings (
		PracticeRepositoryEntityId
		,PracticeRepositoryEntityKey
		,ExternalSystemEntityId
		,ExternalSystemEntityKey
		)
	SELECT  @PracticeRepositoryEntityId As PracticeRepositoryEntityId
		,@PracticeRepositoryEntityKey As PracticeRepositoryEntityKey
		,@ExternalSystemEntityId As ExternalSystemEntityId
		,'160245001' As ExternalSystemEntityKey
	WHERE @PracticeRepositoryEntityId IS NOT NULL AND @PracticeRepositoryEntityKey IS NOT NULL
	AND @ExternalSystemEntityId IS NOT NULL
END

--Create a Drug Row
IF NOT EXISTS (
		SELECT *
		FROM dbo.Drug
		WHERE DisplayName = 'No known medications'
		)
BEGIN
	INSERT INTO dbo.Drug (
		DrugCode
		,DisplayName
		,Family1
		,Family2
		,Focus
		,DrugDosageFormId
		)
	VALUES (
		'T'
		,'No known medications'
		,'UNKNOWN'
		,''
		,1
		,NULL
		)
END
GO

