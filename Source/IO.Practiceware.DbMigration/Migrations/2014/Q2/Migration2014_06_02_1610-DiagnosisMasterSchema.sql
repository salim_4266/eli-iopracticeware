﻿IF SCHEMA_ID('dm') IS NULL
	EXEC ('CREATE SCHEMA dm')
GO

IF OBJECT_ID('[dm].[AssociatedDiagnosisTable]','U') IS NOT NULL
	DROP TABLE [dm].[AssociatedDiagnosisTable] 
GO 
IF OBJECT_ID('[dm].[Drugstable]','U') IS NOT NULL
	DROP TABLE [dm].[Drugstable] 
GO 
IF OBJECT_ID('[dm].[PaintbrushTable]','U') IS NOT NULL
	DROP TABLE [dm].[PaintbrushTable] 
GO 
IF OBJECT_ID('[dm].[ICD9CPTLinkTable]','U') IS NOT NULL
	DROP TABLE [dm].[ICD9CPTLinkTable] 
GO 
IF OBJECT_ID('[dm].[PrimaryDiagnosisTable]','U') IS NOT NULL
	DROP TABLE [dm].[PrimaryDiagnosisTable] 
GO 

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dm].[AssociatedDiagnosisTable]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dm].[AssociatedDiagnosisTable](
		[AssociatedId] [int] NOT NULL,
		[AssociatedMedicalSystem] [nvarchar](max) NULL,
		[AssociatedDiagnosis] [nvarchar](max) NULL,
		[AssociatedRank] [int] NULL,
		[PrimaryDiagnosis] [nvarchar](max) NULL,
		[AssociatedName] [nvarchar](max) NULL,
		[ButtonLingo] [nvarchar](max) NULL,
		[BrushId] [nvarchar](max) NULL,
		[DefaultColor] [nvarchar](max) NULL,
		[LocationSupport] [nvarchar](max) NULL,
		[BaseImage] [nvarchar](max) NULL,
		[Billable] [nvarchar](max) NULL,
		[NextLevel] [nvarchar](max) NULL,
		[ShortName] [nvarchar](max) NULL,
		[Exclusion] [nvarchar](max) NULL,
		[LetterTranslation] [nvarchar](max) NULL,
		[OtherLtrTrans] [nvarchar](max) NULL,
		[PermanentCondition] [nvarchar](max) NULL,
		[ImpressionOn] [nvarchar](max) NULL,
	 CONSTRAINT [PK_AssociatedDiagnosisTable] PRIMARY KEY CLUSTERED 
	(
		[AssociatedId] ASC
	)
	) 
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dm].[Drugstable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dm].[Drugstable](
	[DrugId] [int] IDENTITY NOT NULL,
	[Rank] [int] NULL,
	[DrugName] [nvarchar](max) NULL,
	[GenericName] [nvarchar](max) NULL,
	[Company] [nvarchar](max) NULL,
	[Company2] [nvarchar](max) NULL,
	[Dispense1] [nvarchar](max) NULL,
	[Dispense2] [nvarchar](max) NULL,
	[Dispense3] [nvarchar](max) NULL,
	[Dispense4] [nvarchar](max) NULL,
	[Dispense5] [nvarchar](max) NULL,
	[Dispense6] [nvarchar](max) NULL,
	[Dispense7] [nvarchar](max) NULL,
	[Dispense8] [nvarchar](max) NULL,
	[ICDAlias1] [nvarchar](max) NULL,
	[ICDAlias2] [nvarchar](max) NULL,
	[ICDAlias3] [nvarchar](max) NULL,
	[ICDAlias4] [nvarchar](max) NULL,
	[Package1] [nvarchar](max) NULL,
	[Package2] [nvarchar](max) NULL,
	[Package3] [nvarchar](max) NULL,
	[Package4] [nvarchar](max) NULL,
	[Package5] [nvarchar](max) NULL,
	[Package6] [nvarchar](max) NULL,
	[Webpage] [nvarchar](max) NULL,
 CONSTRAINT [PK_Drugstable] PRIMARY KEY CLUSTERED 
(
	[DrugId] ASC
)
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dm].[ICD9CPTLinkTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dm].[ICD9CPTLinkTable](
	[CPTLinkId] [int] NOT NULL,
	[ICD9] [nvarchar](max) NULL,
	[CPT] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Rank] [int] NULL,
	[EMY] [bit] NULL,
	[EM] [nvarchar](max) NULL,
	[Extra] [int] NULL,
	[SurgOrOffice] [nvarchar](max) NULL,
	[LetterTranslation] [nvarchar](max) NULL,
	[OrderDoc] [nvarchar](max) NULL,
	[ConsultOn] [nvarchar](max) NULL,
	[InsClass1] [nvarchar](max) NULL,
	[InsClass2] [nvarchar](max) NULL,
	[InsClass3] [nvarchar](max) NULL,
	[InsClass4] [nvarchar](max) NULL,
	[InsClass5] [nvarchar](max) NULL,
	[InsClass6] [nvarchar](max) NULL,
 CONSTRAINT [PK_ICD9CPTLinkTable] PRIMARY KEY CLUSTERED 
(
	[CPTLinkId] ASC
)
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dm].[PaintbrushTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dm].[PaintbrushTable](
	[BrushId] [int] NOT NULL,
	[BrushName] [nvarchar](max) NULL,
	[BrushLingo] [nvarchar](max) NULL,
	[Diagnosis] [nvarchar](max) NULL,
	[Methods] [nvarchar](max) NULL,
	[MinValue] [int] NULL,
	[MaxValue] [int] NULL,
	[Steps] [int] NULL,
	[ImageType] [nvarchar](max) NULL,
	[RotateOnY] [bit] NULL,
	[RotateOn] [nvarchar](max) NULL,
	[ScalingOnY] [bit] NULL,
	[ScalingOn] [nvarchar](max) NULL,
	[SizingOnY] [bit] NULL,
	[SizingOn] [nvarchar](max) NULL,
	[LocateXOn] [int] NULL,
	[LocateYOn] [int] NULL,
	[FrameWidth] [int] NULL,
	[FrameHeight] [int] NULL,
	[LeftRight] [nvarchar](max) NULL,
	[TestTrigger] [nvarchar](max) NULL,
	[DefaultColor] [nvarchar](max) NULL,
	[LocationSupport] [nvarchar](max) NULL,
 CONSTRAINT [PK_PaintbrushTable] PRIMARY KEY CLUSTERED 
(
	[BrushId] ASC
)
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dm].[PrimaryDiagnosisTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dm].[PrimaryDiagnosisTable](
	[PrimaryDrillID] [int] NOT NULL,
	[MedicalSystem] [nvarchar](max) NULL,
	[Diagnosis] [nvarchar](max) NULL,
	[DiagnosisRank] [int] NULL,
	[DiagnosisDrillDownLevel] [int] NULL,
	[DiagnosisNextLevel] [nvarchar](max) NULL,
	[DiagnosisName] [nvarchar](max) NULL,
	[ReviewSystemLingo] [nvarchar](max) NULL,
	[Image1] [nvarchar](max) NULL,
	[Image2] [nvarchar](max) NULL,
	[Discipline] [nvarchar](max) NULL,
	[Billable] [nvarchar](max) NULL,
	[NextLevel] [nvarchar](max) NULL,
	[ShortName] [nvarchar](max) NULL,
	[Exclusion] [nvarchar](max) NULL,
	[LetterTranslation] [nvarchar](max) NULL,
	[OtherLtrTrans] [nvarchar](max) NULL,
	[PermanentCondition] [nvarchar](max) NULL,
	[ImpressionOn] [nvarchar](max) NULL,
 CONSTRAINT [PK_PrimaryDiagnosisTable] PRIMARY KEY CLUSTERED 
(
	[PrimaryDrillID] ASC
)
)
END
GO

