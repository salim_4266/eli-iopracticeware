﻿EXEC dbo.DropObject 'model.InvoiceComments'
GO

CREATE TABLE [model].[InvoiceComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
	[Value] nvarchar(max) NULL,
    [IncludeOnStatement] bit NULL,
    [InvoiceId] int NOT NULL
);
GO

INSERT INTO model.InvoiceComments (Value, IncludeOnStatement, InvoiceId)
--Partition 1 non-ASC Claims
SELECT Note1 + Note2 + Note3 + Note4 AS Value,
		CASE
			WHEN NoteCommentOn = 'T'
				THEN CONVERT(bit, 1) 
			ELSE CONVERT(bit, 0)
		END AS IncludeOnStatement,
		inv.Id AS InvoiceId
FROM dbo.PatientNotes pn
INNER JOIN model.Invoices inv ON pn.AppointmentId = inv.EncounterId
WHERE NoteType = 'B'
	AND NoteSystem NOT LIKE '%C%'
	AND pn.AppointmentId > 0
	AND inv.InvoiceTypeId <> 2

UNION ALL 
--ASC claims
SELECT Note1 + Note2 + Note3 + Note4 AS Value,
		CASE
			WHEN NoteCommentOn = 'T'
				THEN CONVERT(bit, 1) 
			ELSE CONVERT(bit, 0)
		END AS IncludeOnStatement,
		inv.Id AS InvoiceId
FROM dbo.PatientNotes pn
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pn.Appointmentid
	AND ap.Comments = 'ASC CLAIM'
INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
	AND apEncounter.AppTypeId = ap.AppTypeId
	AND apEncounter.AppDate = ap.AppDate
	AND apEncounter.AppTime > 0 
	AND ap.AppTime = 0
	AND apEncounter.ScheduleStatus = ap.ScheduleStatus
	AND ap.ScheduleStatus = 'D'
	AND apEncounter.ResourceId1 = ap.ResourceId1
	AND apEncounter.ResourceId2 = ap.ResourceId2
INNER JOIN model.Invoices inv on inv.EncounterId = apEncounter.AppointmentId AND inv.InvoiceTypeId = 2
WHERE NoteType = 'B'
	AND NoteSystem NOT LIKE '%C%'
	AND pn.AppointmentId > 0
	AND inv.InvoiceTypeId = 2
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------
-- Creating primary key on [Id] in table 'InvoiceComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.InvoiceComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[InvoiceComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.InvoiceComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[InvoiceComments]
ADD CONSTRAINT [PK_InvoiceComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------
-- Creating foreign key on [InvoiceId] in table 'InvoiceComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InvoiceInvoiceComments'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[InvoiceComments] DROP CONSTRAINT FK_InvoiceInvoiceComments

IF OBJECTPROPERTY(OBJECT_ID('[model].[Invoices]'), 'IsUserTable') = 1
ALTER TABLE [model].[InvoiceComments]
ADD CONSTRAINT [FK_InvoiceInvoiceComments]
    FOREIGN KEY ([InvoiceId])
    REFERENCES [model].[Invoices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InvoiceInvoiceComments'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InvoiceInvoiceComments')
	DROP INDEX IX_FK_InvoiceInvoiceComments ON [model].[InvoiceComments]
GO
CREATE INDEX [IX_FK_InvoiceInvoiceComments]
ON [model].[InvoiceComments]
    ([InvoiceId]);
GO