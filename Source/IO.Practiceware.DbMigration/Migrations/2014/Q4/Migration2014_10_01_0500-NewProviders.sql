﻿EXEC sp_rename 'model.Providers.PK_Providers', 'PK_ProvidersOld';
GO

CREATE TABLE [model].[ProvidersNew](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsBillable] [bit] NOT NULL,
	[IsEmr] [bit] NOT NULL,
	[BillingOrganizationId] [int] NOT NULL,
	[UserId] [int] NULL,
	[ServiceLocationId] [int] NULL
 CONSTRAINT [PK_Providers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO model.ProvidersNew (IsBillable, IsEmr, BillingOrganizationId, UserId, ServiceLocationId)
SELECT 
IsBillable
,IsEmr
,BillingOrganizationId
,UserId
,NULL AS ServiceLocationId
FROM model.Providers
	WHERE UserId IS NOT NULL
GROUP BY IsBillable
,IsEmr
,BillingOrganizationId
,UserId

UNION ALL

SELECT 
IsBillable
,IsEmr
,BillingOrganizationId
,NULL AS UserId
,ServiceLocationId
FROM model.Providers
	WHERE UserId IS NULL
GROUP BY IsBillable
,IsEmr
,BillingOrganizationId
,UserId
,ServiceLocationId
GO

DECLARE @triggerNames xml
EXEC dbo.DisableEnabledTriggers 'model.Invoices', @names = @triggerNames OUTPUT

DECLARE @constraintNames xml
EXEC dbo.DisableEnabledCheckConstraints 'model.Invoices', @names = @constraintNames OUTPUT

UPDATE i
SET i.BillingProviderId = v.TheNewId
FROM model.Invoices i
JOIN (
	SELECT 
	p.*
	,COALESCE(v.Id,vv.Id) AS TheNewId
	FROM model.Providers p
	LEFT JOIN model.ProvidersNew v ON v.UserId = p.UserId
		AND v.UserId IS NOT NULL
		AND v.ServiceLocationId IS NULL
		AND v.BillingOrganizationId = p.BillingOrganizationId
	LEFT JOIN model.ProvidersNew vv ON vv.UserId IS NULL
		AND vv.ServiceLocationId IS NOT NULL
		AND vv.ServiceLocationId = p.ServiceLocationId
		AND vv.BillingOrganizationId = p.BillingOrganizationId
		AND p.UserId IS NULL
) v ON v.Id = i.BillingProviderId

EXEC dbo.EnableTriggers @triggerNames
EXEC dbo.EnableCheckConstraints @constraintNames
GO


IF OBJECT_ID('model.Providers') IS NOT NULL
	EXEC dbo.DropObject 'model.Providers'
GO

EXEC sp_rename 'model.ProvidersNew', 'Providers';
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BillingOrganizationProvider'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_BillingOrganizationProvider')
	DROP INDEX IX_FK_BillingOrganizationProvider ON [model].[Providers]
GO
CREATE INDEX [IX_FK_BillingOrganizationProvider]
ON [model].[Providers]
    ([BillingOrganizationId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserProvider'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_UserProvider')
	DROP INDEX IX_FK_UserProvider ON [model].[Providers]
GO
CREATE INDEX [IX_FK_UserProvider]
ON [model].[Providers]
    ([UserId]);
GO

-- Creating foreign key on [BillingProviderId] in table 'Invoices'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ProviderInvoice'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Invoices] DROP CONSTRAINT FK_ProviderInvoice
GO

IF OBJECTPROPERTY(OBJECT_ID('[model].[Providers]'), 'IsUserTable') = 1
ALTER TABLE [model].[Invoices]
ADD CONSTRAINT [FK_ProviderInvoice]
    FOREIGN KEY ([BillingProviderId])
    REFERENCES [model].[Providers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
-- Creating non-clustered index for FOREIGN KEY 'FK_ProviderInvoice'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ProviderInvoice')
	DROP INDEX IX_FK_ProviderInvoice ON [model].[Invoices]
GO

CREATE INDEX [IX_FK_ProviderInvoice]
ON [model].[Invoices]
    ([BillingProviderId]);
GO