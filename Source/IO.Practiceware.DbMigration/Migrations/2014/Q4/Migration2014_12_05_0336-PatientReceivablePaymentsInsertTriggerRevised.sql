﻿IF EXISTS (SELECT * FROM sys.triggers WHERE Name = 'PatientReceivablePaymentsInsert') 
	DROP TRIGGER [dbo].[PatientReceivablePaymentsInsert];
GO

CREATE TRIGGER [dbo].[PatientReceivablePaymentsInsert] ON [dbo].[PatientReceivablePayments]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON
	-- The following trigger checks the updated or inserted PaymentReason value on the PatientReceivablePayments table
	-- and inserts it into the practiceCodeTable if it does not exist at the PCT.
	
	IF ((SELECT i.PaymentType FROM inserted i) = '')
	BEGIN
		RAISERROR ('Attempting to insert a PaymentType of '', which is unacceptable. ',16,1)
		ROLLBACK TRANSACTION
	END
	
	DECLARE @InsertedFinancialBatchIdOfNonChkRow INT

	DECLARE @invalidPaymentReasons TABLE(PaymentReason nvarchar(32))
	INSERT INTO @invalidPaymentReasons(PaymentReason)
	SELECT 
	DISTINCT PaymentReason 
	FROM inserted
	LEFT OUTER JOIN (
		SELECT 
		* 
		FROM dbo.PracticeCodeTable pct 
		WHERE pct.ReferenceType = 'PAYMENTREASON'
		) pct ON pct.Code = inserted.PaymentReason
	WHERE inserted.PaymentReason <> '' AND pct.Id IS NULL

	IF EXISTS (SELECT * FROM @invalidPaymentReasons)
	BEGIN
		INSERT INTO dbo.PracticeCodeTable 
		(ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask,
		[Signature], TestOrder, FollowUp, [Rank], WebsiteUrl)
		SELECT 
		'PAYMENTREASON', -- ReferenceType
		PaymentReason, -- Code
		'', -- AlternateCode (this contains description)
		'F', -- Exclusion
		NULL, -- LetterTranslation
		NULL, -- OtherLtrTrans
		NULL, -- FormatMask
		NULL, -- Signature
		NULL, -- TestOrder
		NULL, -- FollowUp
		1, -- Rank
		NULL -- WebsiteUrl
		FROM @invalidPaymentReasons
	END


	DECLARE @InsertedFinancialBatchId INT
	DECLARE @PaymentType NVARCHAR(1)
	DECLARE @InsertedId BIGINT

	SET @PaymentType = (SELECT TOP 1 PaymentType FROM inserted)
	SET @InsertedId = NULL
	
	DECLARE @TableToInsertIntoBasedOnPaymentType INT

	IF (@PaymentType IN ('P','X','8','R','U'))
		SET @TableToInsertIntoBasedOnPaymentType = 1
	ELSE IF (@PaymentType IN ('!','|','?','%','D','&','','0','V'))
		SET @TableToInsertIntoBasedOnPaymentType = 2
	ELSE IF (@PaymentType NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V') AND (@PaymentType IN (SELECT 
		SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
		FROM PracticeCodeTable pct 
		WHERE pct.ReferenceType = 'PAYMENTTYPE' AND pct.AlternateCode IN ('C','D')
		AND (SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
		NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V'))))
		)
		SET @TableToInsertIntoBasedOnPaymentType = 1
	ELSE IF (@PaymentType NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V') AND (@PaymentType IN (SELECT 
		SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
		FROM PracticeCodeTable pct 
		WHERE pct.ReferenceType = 'PAYMENTTYPE' AND pct.AlternateCode NOT IN ('C','D')
		AND (SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
		NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V'))))
		)
		SET @TableToInsertIntoBasedOnPaymentType = 2
	ELSE SET @TableToInsertIntoBasedOnPaymentType = NULL

	IF (@PaymentType = '=')
		BEGIN
			INSERT INTO model.FinancialBatches (FinancialSourceTypeId, PatientId, InsurerId, PaymentDateTime, ExplanationOfBenefitsDateTime, Amount, CheckCode, PaymentMethodId)
			SELECT
				CASE i.PayerType
					WHEN 'I'
						THEN 1
					WHEN 'P'
						THEN 2
					WHEN 'O'
						THEN 3
					ELSE 4
				END AS FinancialSourceTypeId
				,CASE 
					WHEN (i.PayerId IS NOT NULL AND i.PayerId <> '' AND i.PayerType = 'P')
						THEN i.PayerId
					ELSE NULL
				END AS PatientId
				,CASE
					WHEN (i.PayerId IS NOT NULL AND i.PayerId <> '' AND i.PayerType = 'I')
						THEN i.PayerId
					ELSE NULL
				END AS InsurerId
				,CASE 
					WHEN (i.PaymentDate = '' OR i.PaymentDate IS NULL)
						THEN NULL 
					ELSE CONVERT(DATETIME, i.PaymentDate, 112) 
				END AS PaymentDateTime
				,CASE 
					WHEN i.PaymentEOBDate IS NOT NULL AND i.PaymentEOBDate <> ''
						THEN CONVERT(DATETIME,i.PaymentEOBDate,112)
					ELSE CONVERT(DATETIME,NULL)
				END AS ExplanationOfBenefitsDateTime
				,i.PaymentAmount AS Amount
				,i.PaymentCheck AS CheckCode
				,p.Id AS PaymentMethodId
			FROM inserted i
			LEFT JOIN model.PaymentMethods p ON i.PaymentRefType = p.LegacyPracticeCodeTablePaymentRefType
			
			IF (@@ROWCOUNT > 0) SET @InsertedFinancialBatchId = SCOPE_IDENTITY()
			SET @TableToInsertIntoBasedOnPaymentType = NULL
		END
	
	IF (@TableToInsertIntoBasedOnPaymentType = 1)
		BEGIN
		--1. do some checks for bad data
			IF EXISTS (
				SELECT * FROM model.Adjustments adj WITH(NOLOCK) 
				INNER JOIN (SELECT * FROM inserted i WHERE i.PayerType = 'I' EXCEPT SELECT * FROM deleted d WHERE d.PayerType = 'I') i ON i.PaymentId = adj.Id
				INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
					AND PatientInsuranceId IS NOT NULL
				INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
				INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
				LEFT JOIN model.Insurers ins ON ins.Id = ip.InsurerId
				WHERE adj.FinancialSourceTypeId = 1
					AND adj.InsurerId IS NOT NULL
					AND ins.Id IS NULL
					AND dbo.GetNoCheck() = 0
			)
			BEGIN
				ROLLBACK TRANSACTION
				RAISERROR ('The payer must be valid, when payment from Insurer (PayerType = i, valid PatientInsurers)', 16,1)
			END
		-- 2. more checks
			IF EXISTS (
				SELECT * FROM model.Adjustments adj
				INNER JOIN (SELECT * FROM inserted i WHERE i.PayerType = 'P' EXCEPT SELECT * FROM deleted d WHERE d.PayerType = 'P' ) i ON adj.Id = i.PaymentId
				LEFT JOIN model.Patients p ON p.Id = adj.PatientId
				WHERE adj.FinancialSourceTypeId = 2 
					AND adj.PatientId IS NOT NULL 
					AND p.Id IS NULL
					AND dbo.GetNoCheck() = 0
			)
			BEGIN
				ROLLBACK TRANSACTION
				RAISERROR ('The payer must be valid, when payment from Patient (PayerType = P, Valid PatientDemographics)', 16,1)
			END
		--3. look for row in adjustments that is invoicereceivableid null row
			DECLARE @AdjustmentId BIGINT
			SET @AdjustmentId = (
				SELECT TOP 1 Id
				FROM model.Adjustments adj
				INNER JOIN inserted i ON (i.ReceivableId - (1 * 1000000000)) = adj.Id
				WHERE adj.InvoiceReceivableId IS NULL
			)

			IF EXISTS(
			SELECT 
			Id 
			FROM model.Adjustments 
			WHERE Id  = @AdjustmentId
			AND InvoiceReceivableId IS NULL
			AND Amount = -0.01
			)
			BEGIN				
				--it found it, so check for a paymentcheck
				IF EXISTS (
				SELECT
				i.PaymentCheck
				FROM inserted i
				WHERE (i.PaymentBatchCheckId IS NULL 
					OR i.PaymentBatchCheckId = 0 
					OR i.PaymentBatchCheckId = -1
					OR i.PaymentBatchCheckId = '')
					AND i.PaymentCheck <> ''
				) AND (@InsertedFinancialBatchId IS NULL)
				--it found a check, but no batch, create one.
				BEGIN
					INSERT INTO model.FinancialBatches ([FinancialSourceTypeId],[PatientId],[InsurerId],[PaymentDateTime],[ExplanationOfBenefitsDateTime],[Amount],[CheckCode],[PaymentMethodId])
					SELECT
					CASE
						WHEN i.PayerType = 'P'
							THEN 2
						WHEN i.PayerType = 'I'
							THEN 1
						WHEN i.PayerType = 'O'
							THEN 3
						ELSE 4
					END AS FinancialSourceTypeId
					,CASE 
						WHEN i.PayerType = 'P' AND PayerId > 0
							THEN i.PayerId 
						ELSE NULL 
					END AS PatientId
					,CASE 
						WHEN i.PayerType = 'I' AND PayerId > 0
							THEN i.PayerId
						ELSE NULL
					END AS InsurerId
					,CONVERT(DATETIME,i.PaymentDate,112) AS PaymentDateTime
					,CASE 
						WHEN i.PaymentEOBDate IS NOT NULL AND i.PaymentEOBDate <> ''
							THEN CONVERT(DATETIME,i.PaymentEOBDate,112)
						ELSE CONVERT(DATETIME,NULL)
					END AS ExplanationOfBenefitsDateTime
					,i.PaymentAmount AS Amount
					,CONVERT(NVARCHAR(MAX),i.PaymentCheck) AS CheckCode
					,p.Id AS PaymentMethodId
					FROM inserted i
					LEFT JOIN model.PaymentMethods p ON i.PaymentRefType = p.LegacyPracticeCodeTablePaymentRefType

					IF (@@ROWCOUNT > 0)
					BEGIN
						SET @InsertedFinancialBatchId = SCOPE_IDENTITY()
					END
					ELSE SET @InsertedFinancialBatchId = NULL
				END
				UPDATE adj
				SET adj.AdjustmentTypeId = CASE i.PaymentType
					WHEN 'P'
						THEN 1
					WHEN 'X'
						THEN 2
					WHEN '8'
						THEN 3
					WHEN 'R'
						THEN 4
					WHEN 'U'
						THEN 5
					ELSE pct.Id + 1000
				END
				,adj.Amount = i.PaymentAmount
				,adj.FinancialBatchId = @InsertedFinancialBatchId
				,adj.InsurerId = CASE 
					WHEN i.PayerType = 'I' AND PayerId > 0
						THEN i.PayerId
					ELSE NULL
				END
				,adj.BillingServiceId = NULL
				,adj.FinancialSourceTypeId = CASE i.PayerType
					WHEN 'I'
						THEN 1
					WHEN 'P'
						THEN 2
					WHEN 'O'
						THEN 3
					ELSE 4
				END
				,adj.InvoiceReceivableId = NULL
				,adj.PostedDateTime = CONVERT(DATETIME, i.PaymentDate, 112)
				,adj.ClaimAdjustmentReasonCodeId = NULL
				,adj.ClaimAdjustmentGroupCodeId = NULL
				,adj.IncludeCommentOnStatement = CASE WHEN i.PaymentCommentOn = 'T' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
				,adj.Comment = CASE
					WHEN i.PaymentRefId <> '' AND i.PaymentRefId IS NOT NULL
						THEN i.PaymentRefId
					ELSE NULL
				END
				,adj.LegacyInvoiceReceivableId = dbo.GetAdjustmentIdAndConvertAsReceivableId(adj.Id)
				FROM model.Adjustments adj
				INNER JOIN inserted i ON (i.ReceivableId - (1 * 1000000000)) = adj.Id
				JOIN PracticeCodeTable pct ON SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) = i.PaymentType
					AND pct.ReferenceType = 'PAYMENTTYPE'
				WHERE adj.Id = @AdjustmentId

				DECLARE @InsertedIdOfInvoiceReceivableIdNullRow INT
				IF (@@ROWCOUNT > 0) SET @InsertedIdOfInvoiceReceivableIdNullRow = @AdjustmentId
				ELSE SET @InsertedIdOfInvoiceReceivableIdNullRow = NULL
			END	

			ELSE IF EXISTS(
			SELECT 
			* 
			FROM model.Adjustments adj
			WHERE adj.Id = @AdjustmentId
			AND InvoiceReceivableId IS NULL
			AND adj.Amount > 0
			) OR EXISTS (
				SELECT
				*
				FROM inserted i
				WHERE i.ReceivableId > 1000000000
				AND i.PayerType IN ('P','I')
				)
			BEGIN
				--it found it, so check for a paymentcheck
				IF  EXISTS (
				SELECT
				i.PaymentCheck
				FROM inserted i
				WHERE (i.PaymentBatchCheckId IS NULL 
					OR i.PaymentBatchCheckId = 0 
					OR i.PaymentBatchCheckId = -1
					OR i.PaymentBatchCheckId = '')
					AND i.PaymentCheck <> ''
				) AND (@InsertedFinancialBatchId IS NULL)
				--it found a check, but no batch, create one.
				BEGIN
					INSERT INTO model.FinancialBatches ([FinancialSourceTypeId],[PatientId],[InsurerId],[PaymentDateTime],[ExplanationOfBenefitsDateTime],[Amount],[CheckCode],[PaymentMethodId])
					SELECT
					CASE
						WHEN i.PayerType = 'P'
							THEN 2
						WHEN i.PayerType = 'I'
							THEN 1
						WHEN i.PayerType = 'O'
							THEN 3
						ELSE 4
					END AS FinancialSourceTypeId
					,CASE 
						WHEN i.PayerType = 'P' AND PayerId > 0
							THEN i.PayerId 
						ELSE NULL 
					END AS PatientId
					,CASE 
						WHEN i.PayerType = 'I' AND PayerId > 0
							THEN i.PayerId
						ELSE NULL
					END AS InsurerId
					,CONVERT(DATETIME,i.PaymentDate,112) AS PaymentDateTime
					,CASE 
						WHEN i.PaymentEOBDate IS NOT NULL AND i.PaymentEOBDate <> ''
							THEN CONVERT(DATETIME,i.PaymentEOBDate,112)
						ELSE CONVERT(DATETIME,NULL)
					END AS ExplanationOfBenefitsDateTime
					,i.PaymentAmount AS Amount
					,CONVERT(NVARCHAR(MAX),i.PaymentCheck) AS CheckCode
					,p.Id as PaymentMethodId
				FROM inserted i
				LEFT JOIN model.PaymentMethods p ON i.PaymentRefType = p.LegacyPracticeCodeTablePaymentRefType

				IF (@@ROWCOUNT > 0)
				BEGIN
					SET @InsertedFinancialBatchId = SCOPE_IDENTITY()
				END
				ELSE SET @InsertedFinancialBatchId = NULL
				END

				DECLARE @InsurerId INT
				SELECT 
				@InsurerId = i.PayerId
				FROM inserted i
				WHERE i.ReceivableId > 1000000000
					AND i.PayerType = 'I'

				DECLARE @PatientId INT
				SELECT
				@PatientId = CASE 
					WHEN i.PayerType = 'P' 
						THEN i.PayerId 
					WHEN i.PayerType = 'I' AND i.PayerId = 99999 
						THEN i.PayerId
					ELSE NULL 
				END 
				FROM inserted i
				WHERE i.ReceivableId > 1000000000
				
				INSERT INTO model.Adjustments ([FinancialSourceTypeId],[Amount],[PostedDateTime],[AdjustmentTypeId]
				,[BillingServiceId],[InvoiceReceivableId],[PatientId],[InsurerId],[FinancialBatchId]
				,[ClaimAdjustmentGroupCodeId],[ClaimAdjustmentReasonCodeId],[Comment],[IncludeCommentOnStatement])
				SELECT
				CASE i.PayerType
					WHEN 'P'
						THEN 2
					WHEN 'I'
						THEN 1
					WHEN 'O'
						THEN 3
					ELSE 4
				END AS FinancialSourceTypeId
				,i.PaymentAmount AS Amount
				,CONVERT(DATETIME,i.PaymentDate,112) AS PostedDateTime
				,CASE PaymentType
					WHEN 'P'
						THEN 1
					WHEN 'X'
						THEN 2
					WHEN '8'
						THEN 3
					WHEN 'R'
						THEN 4
					WHEN 'U'
						THEN 5
					ELSE pct.Id + 1000
				END AS AdjustmentTypeId
				,NULL AS BillingServiceId
				,NULL AS InvoiceReceivableId
				,CASE 
					WHEN i.PayerType = 'I'
						THEN i.PaymentAppealNumber
					ELSE @PatientId 
				END AS PatientId
				,@InsurerId AS InsuredId
				,@InsertedFinancialBatchId AS FinancialBatchId
				,NULL AS ClaimAdjustmentGroupCodeId
				,NULL AS ClaimAdjustmentReasonCodeId
				,CASE 
					WHEN i.PaymentRefId IS NOT NULL AND i.PaymentRefId <> '' 
						THEN i.PaymentRefId 
					ELSE NULL 
				END AS Comment
				,CASE 
					WHEN i.PaymentCommentOn = 'T' 
						THEN CONVERT(BIT,1) 
					ELSE CONVERT(BIT,0) 
				END AS IncludeCommentOnStatement
				FROM inserted i
				JOIN PracticeCodeTable pct ON SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) = i.PaymentType
					AND pct.ReferenceType = 'PAYMENTTYPE'
				LEFT JOIN model.PaymentMethods p ON i.PaymentRefType = p.LegacyPracticeCodeTablePaymentRefType

				DECLARE @InsertedIdOfInvoiceReceivableIdNullRowWithExisting BIGINT

				IF (@@ROWCOUNT > 0) 
				BEGIN
					SET @InsertedIdOfInvoiceReceivableIdNullRowWithExisting = SCOPE_IDENTITY()
					SELECT @InsertedIdOfInvoiceReceivableIdNullRowWithExisting
					UPDATE adj
					SET adj.LegacyInvoiceReceivableId = dbo.GetAdjustmentIdAndConvertAsReceivableId(adj.Id)
					FROM model.Adjustments adj
					WHERE adj.Id = @InsertedIdOfInvoiceReceivableIdNullRowWithExisting

					DECLARE @PatientOnAccountCredit INT
					SELECT 
					@PatientOnAccountCredit = a.PatientId 
					FROM model.Adjustments a 
					WHERE a.Id = SCOPE_IDENTITY()
						AND a.InvoiceReceivableId IS NULL 
						AND a.InsurerId IS NULL

					DECLARE @InsurerOnAccountCredit INT
					SELECT 
					@InsurerOnAccountCredit = a.InsurerId 
					FROM model.Adjustments a 
					WHERE a.Id = SCOPE_IDENTITY()
						AND a.InvoiceReceivableId IS NULL 
						AND a.PatientId IS NOT NULL
					
					IF @PatientOnAccountCredit IS NOT NULL
					BEGIN
						UPDATE a
						SET a.Amount = 0
						FROM model.Adjustments a
						WHERE a.Id <> SCOPE_IDENTITY()
							AND a.InvoiceReceivableId IS NULL
							AND a.InsurerId IS NULL
							AND a.PatientId = (SELECT TOP 1
								COALESCE(@PatientOnAccountCredit,i.PaymentAppealNumber)
								FROM inserted i)
							AND a.Amount !> 0
							
					END

					IF @InsurerOnAccountCredit IS NOT NULL
					BEGIN
						UPDATE a
						SET a.Amount = 0
						FROM model.Adjustments a
						WHERE a.Id <> SCOPE_IDENTITY()
							AND a.InvoiceReceivableId IS NULL
							AND a.InsurerId = @InsurerOnAccountCredit
							AND a.PatientId IS NOT NULL
							AND a.Amount !> 0
					END
				END 
				ELSE SET @InsertedIdOfInvoiceReceivableIdNullRowWithExisting = NULL
				
			END

			--else it did not find a row, so this is a plain insert
			ELSE
			BEGIN 
			--qualify as non credit row
			IF NOT EXISTS (
			SELECT 
			* 
			FROM model.Adjustments 
			WHERE Id = @AdjustmentId
			AND InvoiceReceivableId IS NULL
			)
			BEGIN
			IF(@InsertedFinancialBatchId IS NULL) 
				AND NOT EXISTS (SELECT f.Id FROM model.FinancialBatches f 
								INNER JOIN inserted i ON i.PaymentBatchCheckId = f.Id)
				BEGIN
					INSERT INTO model.FinancialBatches ([FinancialSourceTypeId],[PatientId],[InsurerId],[PaymentDateTime],[ExplanationOfBenefitsDateTime],[Amount],[CheckCode],[PaymentMethodId])
					SELECT
					CASE
						WHEN i.PayerType = 'P'
							THEN 2
						WHEN i.PayerType = 'I'
							THEN 1
						WHEN i.PayerType = 'O'
							THEN 3
						ELSE 4
					END AS FinancialSourceTypeId
					,CASE 
						WHEN i.PayerType = 'P' AND PayerId > 0
							THEN i.PayerId 
						ELSE NULL 
					END AS PatientId
					,CASE 
						WHEN i.PayerType = 'I' AND PayerId > 0
							THEN i.PayerId
						ELSE NULL
					END AS InsurerId
					,CONVERT(DATETIME,i.PaymentDate,112) AS PaymentDateTime
					,CASE 
						WHEN i.PaymentEOBDate IS NOT NULL AND i.PaymentEOBDate <> ''
							THEN CONVERT(DATETIME,i.PaymentEOBDate,112)
						ELSE CONVERT(DATETIME,NULL)
					END AS ExplanationOfBenefitsDateTime
					,i.PaymentAmount AS Amount
					,CONVERT(NVARCHAR(MAX),i.PaymentCheck) AS CheckCode
					,p.Id AS PaymentMethodId
					FROM inserted i
					LEFT JOIN model.PaymentMethods p ON i.PaymentRefType = p.LegacyPracticeCodeTablePaymentRefType

					SET @InsertedFinancialBatchId = SCOPE_IDENTITY()
				END
				ELSE IF EXISTS (
					SELECT * 
					FROM inserted i 
					JOIN model.FinancialBatches f ON f.Id = i.PaymentBatchCheckId 
					AND f.CheckCode = i.PaymentCheck)
				BEGIN
					UPDATE f
					SET f.ExplanationOfBenefitsDateTime = CASE 
						WHEN i.PaymentEOBDate <> '' AND i.PaymentEOBDate IS NOT NULL 
							THEN CONVERT(DATETIME,i.PaymentEOBDate) 
						ELSE CONVERT(DATETIME,NULL) 
						END
					FROM model.FinancialBatches f
					JOIN inserted i ON f.Id = i.PaymentBatchCheckId 
						AND f.CheckCode = i.PaymentCheck
				END

				DECLARE @PatientInvoiceReceivableId INT
				SET @PatientInvoiceReceivableId  = (
				SELECT 
				Id
				FROM model.InvoiceReceivables ir
				WHERE InvoiceId IN (
					SELECT 
					InvoiceId 
					FROM model.InvoiceReceivables 
					WHERE Id IN (
					(SELECT
					i.ReceivableId
					FROM inserted i))
					)
				AND PatientInsuranceId IS NULL)

				INSERT INTO model.Adjustments (AdjustmentTypeId,Amount,FinancialBatchId,InsurerId,PatientId,BillingServiceId
							,FinancialSourceTypeId,InvoiceReceivableId, PostedDateTime,ClaimAdjustmentReasonCodeId,
							ClaimAdjustmentGroupCodeId,IncludeCommentOnStatement,Comment)
					SELECT
						CASE PaymentType
							WHEN 'P'
								THEN 1
							WHEN 'X'
								THEN 2
							WHEN '8'
								THEN 3
							WHEN 'R'
								THEN 4
							WHEN 'U'
								THEN 5
							ELSE pct.Id + 1000
						END AS AdjustmentTypeId
						,i.PaymentAmount AS Amount
						,CASE 
							WHEN i.PaymentBatchCheckId IN (0, -1, NULL) 
								THEN @InsertedFinancialBatchId
							WHEN i.PaymentBatchCheckId IS NOT NULL AND i.PaymentBatchCheckId <> '' 
								THEN i.PaymentBatchCheckId
						END AS FinancialBatchId
						,CASE 
							WHEN i.PayerType = 'I' AND PayerId > 0
								THEN i.PayerId
							ELSE NULL
						END AS InsurerId
						,CASE 
							WHEN i.PayerType = 'P' 
								THEN i.PayerId 
							WHEN i.PayerType = 'I' AND i.PayerId = 99999 
								THEN i.PayerId
							ELSE NULL 
						END AS PatientId
						,CASE 
							WHEN i.PaymentServiceItem = 0
								THEN NULL
							ELSE i.PaymentServiceItem END AS BillingServiceId
						,CASE i.PayerType
							WHEN 'I'
								THEN 1
							WHEN 'P'
								THEN 2
							WHEN 'O'
								THEN 3
							ELSE 4
						END AS FinancialSourceTypeId
						,CASE 
							WHEN i.PayerType = 'P'
								THEN @PatientInvoiceReceivableId--get the primary insurance invoice receivable
							ELSE i.ReceivableId
						END AS InvoiceReceivableId
						,CONVERT(DATETIME, i.PaymentDate, 112) AS PostedDateTime
						,CASE
							WHEN (i.PaymentReason IS NULL OR i.PaymentReason = '')
								THEN 
									CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
										WHEN 'X' -- AdjustmentTypeId = 2
											THEN paymentReason45.Id -- ReasonCode 45
										WHEN '8' -- AdjustmentTypeId = 3
											THEN paymentReason45.Id -- ReasonCode 45
										ELSE 
											CAST(NULL AS int)
									END
							ELSE
								existingPaymentReason.Id --(SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'PAYMENTREASON' AND Code = prp.PaymentReason)
						 END AS ClaimAdjustmentReasonCodeId
						,CASE
							WHEN i.ClaimAdjustmentGroupCodeId IS NULL 
								THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
									 -- These int values correspond to the ClaimAdjustmentGroupCode enum in .NET
										WHEN 'X' -- AdjustmentTypeId = 2
											THEN 1 -- ContractualObligation
										WHEN '8' -- AdjustmentTypeId = 3
											THEN 3 -- OtherAdjustment
										WHEN 'R' -- AdjustmentTypeId = 4
											THEN 4 -- CorrectionReversal
										WHEN 'U' -- AdjustmentTypeId = 5
											THEN 4 -- CorrectionReversal
										ELSE 
											CAST(NULL AS int)
									 END
							ELSE
								i.ClaimAdjustmentGroupCodeId
						 END AS ClaimAdjustmentGroupCodeId
						,CASE 
							WHEN i.PaymentCommentOn = 'T'
								THEN CONVERT(BIT,1)
							ELSE CONVERT(BIT,0)
						END AS IncludeCommentOnStatement
						,i.PaymentRefId AS Comment
					FROM inserted i
					JOIN PracticeCodeTable pct ON SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) = i.PaymentType
						AND pct.ReferenceType = 'PAYMENTTYPE'
					LEFT JOIN model.PaymentMethods p ON i.PaymentRefType = p.LegacyPracticeCodeTablePaymentRefType
					LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason45 ON paymentReason45.Id = 
						(SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45')
					LEFT OUTER JOIN dbo.PracticeCodeTable existingPaymentReason ON existingPaymentReason.Id = 
						(SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = i.PaymentReason)

					IF (@@ROWCOUNT > 0)
					BEGIN
						SET @InsertedId = SCOPE_IDENTITY()
					END
					ELSE SET @InsertedId = NULL
				END
			END
		END

	ELSE IF (@TableToInsertIntoBasedOnPaymentType = 2)
		BEGIN
			IF EXISTS (
				SELECT * FROM model.FinancialInformations fi WITH(NOLOCK) 
				INNER JOIN (SELECT * FROM inserted i WHERE i.PayerType = 'I' EXCEPT SELECT * FROM deleted d WHERE d.PayerType = 'I') i ON i.PaymentId = fi.Id
				INNER JOIN model.InvoiceReceivables ir ON ir.Id = fi.InvoiceReceivableId
					AND PatientInsuranceId IS NOT NULL
				INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
				INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
				LEFT JOIN model.Insurers ins ON ins.Id = ip.InsurerId
				WHERE fi.FinancialSourceTypeId = 1
					AND fi.InsurerId IS NOT NULL
					AND ins.Id IS NULL
					AND dbo.GetNoCheck() = 0
			)
			BEGIN
				ROLLBACK TRANSACTION
				RAISERROR ('The payer must be valid, when payment from Insurer (PayerType = i, valid PatientInsurers)', 16,1)
			END

			IF EXISTS (
				SELECT * FROM model.FinancialInformations fi
				INNER JOIN (SELECT * FROM inserted i WHERE i.PayerType = 'P' EXCEPT SELECT * FROM deleted d WHERE d.PayerType = 'P' ) i ON fi.Id = i.PaymentId
				LEFT JOIN model.Patients p ON p.Id = fi.PatientId
				WHERE fi.FinancialSourceTypeId = 2 
					AND fi.PatientId IS NOT NULL 
					AND p.Id IS NULL
					AND dbo.GetNoCheck() = 0
			)
			BEGIN
				ROLLBACK TRANSACTION
				RAISERROR ('The payer must be valid, when payment from Patient (PayerType = P, Valid PatientDemographics)', 16,1)
			END

			IF EXISTS (
			SELECT 
			i.PaymentCheck
			,i.PaymentBatchCheckId 
			FROM inserted i 
			WHERE i.PaymentCheck IS NOT NULL AND i.PaymentCheck <> '' -- so inserted has a paymentcheck
				AND i.PaymentBatchCheckId IS NULL AND (i.PaymentBatchCheckId = 0 OR i.PaymentBatchCheckId = -1) -- check for payment check in inserted row
				AND i.PaymentRefType IN ('C') -- cash
			)
			BEGIN

				INSERT INTO model.FinancialBatches ([FinancialSourceTypeId],[PatientId],[InsurerId],[PaymentDateTime],[ExplanationOfBenefitsDateTime],[Amount],[CheckCode],[PaymentMethodId])
				SELECT
				CASE
					WHEN i.PayerType = 'P'
						THEN 2
					WHEN i.PayerType = 'I'
						THEN 1
					WHEN i.PayerType = 'O'
						THEN 3
					ELSE 4
				END AS FinancialSourceTypeId
				,CASE 
					WHEN i.PayerType = 'P' AND PayerId > 0
						THEN i.PayerId 
					ELSE NULL 
				END AS PatientId
				,CASE 
					WHEN i.PayerType = 'I' AND PayerId > 0
						THEN i.PayerId
					ELSE NULL
				END AS InsurerId
				,CONVERT(DATETIME,i.PaymentDate,112) AS PaymentDateTime
				,CASE 
					WHEN i.PaymentEOBDate IS NOT NULL AND i.PaymentEOBDate <> ''
						THEN CONVERT(DATETIME,i.PaymentEOBDate,112)
					ELSE CONVERT(DATETIME,NULL)
				END AS ExplanationOfBenefitsDateTime
				,i.PaymentAmount AS Amount
				,CONVERT(NVARCHAR(MAX),i.PaymentCheck) AS CheckCode
				,p.Id AS PaymentMethodId
				FROM inserted i
				LEFT JOIN model.PaymentMethods p ON i.PaymentRefType = p.LegacyPracticeCodeTablePaymentRefType

				SET @InsertedFinancialBatchId = SCOPE_IDENTITY()		
			END

			INSERT INTO model.FinancialInformations (Amount,BillingServiceId,FinancialInformationTypeId,FinancialSourceTypeId,InsurerId
						,PatientId,InvoiceReceivableId,PostedDateTime, FinancialBatchId,ClaimAdjustmentReasonCodeId
						,ClaimAdjustmentGroupCodeId,IncludeCommentOnStatement,Comment)
				SELECT
					i.PaymentAmount AS Amount
					,i.PaymentServiceItem AS BillingServiceId
					,CASE PaymentType
						WHEN '!'
							THEN 1
						WHEN '|'
							THEN 2
						WHEN '?'
							THEN 3
						WHEN '%'
							THEN 4
						WHEN 'D'
							THEN 5
						WHEN '&'
							THEN 6
						WHEN ''
							THEN 7
						WHEN '0'
							THEN 8
						WHEN 'V'
							THEN 9
						ELSE pct.Id + 1000 
					END AS FinancialInformationTypeId
					,CASE i.PayerType
						WHEN 'I'
							THEN 1
						WHEN 'P'
							THEN 2
						WHEN 'O'
							THEN 3
						ELSE 4
					END AS FinancialSourceTypeId
					,CASE 
						WHEN i.PayerType = 'I' AND PayerId > 0
							THEN i.PayerId
						ELSE NULL
					END AS InsurerId
					,CASE 
						WHEN i.PayerType = 'P' 
							THEN i.PayerId 
						WHEN i.PayerType = 'I' AND i.PayerId = 99999 
							THEN i.PayerId
						ELSE NULL 
					END AS PatientId
					,i.ReceivableId AS InvoiceReceivableId
					,CONVERT(DATETIME, i.PaymentDate, 112) AS PostedDateTime
					,CASE 
						WHEN i.PaymentBatchCheckId IN (0, -1, NULL) 
							THEN @InsertedFinancialBatchId
						WHEN i.PaymentBatchCheckId IS NOT NULL AND i.PaymentBatchCheckId <> '' 
							THEN i.PaymentBatchCheckId
					END AS FinancialBatchId
					,CASE 
						WHEN (i.PaymentReason IS NULL OR i.PaymentReason = '')
							THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
								WHEN '!' -- FinancialInformationTypeId = 1
									THEN paymentReason1.Id -- ReasonCode 1
								WHEN '|' -- FinancialInformationTypeId = 2
									THEN paymentReason2.Id -- ReasonCode 2
								WHEN '?' -- FinancialInformationTypeId = 3
									THEN paymentReason3.Id -- ReasonCode 3
								ELSE 
									existingPaymentReason.Id
								END
						ELSE existingPaymentReason.Id
					END AS ClaimAdjustmentReasonCodeId
					,CASE 
						WHEN i.ClaimAdjustmentGroupCodeId IS NULL
							THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
								-- These int values correspond to the ClaimAdjustmentGroupCode enum in .NET
								WHEN '!' -- FinincialInformationTypeId = 1
									THEN 2 
								WHEN '|' -- FinincialInformationTypeId = 2
									THEN 2 
								WHEN '?' -- FinincialInformationTypeId = 3
									THEN 2 
								WHEN 'D' -- FinincialInformationTypeId = 5
									THEN 3 
								WHEN ']' -- FinincialInformationTypeId = 7
									THEN 3 
								WHEN '0' -- FinancialInformationTypeId = 8
									THEN 2 
								ELSE 
									CAST(NULL AS int)
								END
						ELSE
							i.ClaimAdjustmentGroupCodeId
					END
					,CASE 
						WHEN i.PaymentCommentOn = 'T'
							THEN CONVERT(BIT,1)
						ELSE CONVERT(BIT,0)
					END AS IncludeCommentOnStatement
					,i.PaymentRefId AS Comment
				FROM inserted i
				JOIN PracticeCodeTable pct ON SUBSTRING(pct.Code, LEN(pct.Code), 1) = i.PaymentType
					AND pct.ReferenceType = 'PAYMENTTYPE' AND (pct.AlternateCode = '' OR pct.AlternateCode IS NULL)
				LEFT JOIN model.PaymentMethods p ON i.PaymentRefType = p.LegacyPracticeCodeTablePaymentRefType
				LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason1 ON paymentReason1.Id = 
					(SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '1')
				LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason2 ON paymentReason2.Id = 
					(SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '2')
				LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason3 ON paymentReason3.Id = 
					(SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '3')
				LEFT OUTER JOIN dbo.PracticeCodeTable existingPaymentReason ON existingPaymentReason.Id = 
					(SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = i.PaymentReason)

				IF (@@ROWCOUNT > 0) SET @InsertedId = SCOPE_IDENTITY()
				ELSE SET @InsertedId = NULL
		END

	--determines scope identity
	IF OBJECT_ID('dbo.ViewIdentities','U') IS NULL
	BEGIN	
		CREATE TABLE [dbo].[ViewIdentities](
		[Name] [nvarchar](max) NULL,
		[Value] [bigint] NULL
		)
	END

	IF (@PaymentType = '=')
	BEGIN
		IF NOT EXISTS(SELECT * FROM ViewIdentities WHERE Name = 'dbo.PatientReceivablePayments')
		INSERT ViewIdentities(Name, Value) VALUES ('dbo.PatientReceivablePayments', @InsertedFinancialBatchId)
	ELSE
		UPDATE ViewIdentities SET Value = @InsertedFinancialBatchId WHERE Name = 'dbo.PatientReceivablePayments'
	SELECT @InsertedFinancialBatchId AS SCOPE_ID_COLUMN
	END
	
	IF (@TableToInsertIntoBasedOnPaymentType IN (1,2))
	BEGIN
		IF OBJECT_ID('tempdb..#TempTableToHoldTheScopeIdentity') IS NOT NULL
			DROP TABLE #TempTableToHoldTheScopeIdentity

		CREATE TABLE #TempTableToHoldTheScopeIdentity (Id BIGINT IDENTITY(1,1) NOT NULL)
		SET IDENTITY_INSERT #TempTableToHoldTheScopeIdentity ON

		IF (@InsertedId IS NOT NULL)
		INSERT INTO #TempTableToHoldTheScopeIdentity (Id)
		SELECT model.GetPairId(@TableToInsertIntoBasedOnPaymentType,@InsertedId,11000000) AS SCOPE_ID_COLUMN

		IF NOT EXISTS(SELECT * FROM ViewIdentities WHERE Name = 'dbo.PatientReceivablePayments')
			INSERT ViewIdentities(Name, Value) VALUES ('dbo.PatientReceivablePayments', @@IDENTITY)
		ELSE
			UPDATE ViewIdentities SET Value = @@IDENTITY WHERE Name = 'dbo.PatientReceivablePayments'

		IF (@InsertedId IS NULL AND @InsertedIdOfInvoiceReceivableIdNullRow IS NOT NULL)
		BEGIN
			SELECT model.GetPairId(7,@InsertedIdOfInvoiceReceivableIdNullRow,11000000) AS SCOPE_ID_COLUMN
		END
		ELSE IF (@InsertedId IS NOT NULL AND @InsertedIdOfInvoiceReceivableIdNullRow IS NULL)
		BEGIN
			SELECT @@IDENTITY AS SCOPE_ID_COLUMN
			IF (@TableToInsertIntoBasedOnPaymentType = 1)
			BEGIN
				UPDATE adj
				SET [LegacyInvoiceReceivableId] = dbo.GetLegacyInvoiceReceivableId(adj.InvoiceReceivableId)
				FROM model.Adjustments adj
				WHERE adj.Id = @InsertedId
			END
			ELSE IF (@TableToInsertIntoBasedOnPaymentType = 2)
			BEGIN
				UPDATE fi
				SET [LegacyInvoiceReceivableId] = dbo.GetLegacyInvoiceReceivableId(fi.InvoiceReceivableId)
				FROM model.FinancialInformations fi
				WHERE fi.Id = @InsertedId
			END
		END
		ELSE IF (@InsertedIdOfInvoiceReceivableIdNullRowWithExisting IS NOT NULL)
			SELECT model.GetPairId(7,@InsertedIdOfInvoiceReceivableIdNullRowWithExisting,11000000) AS SCOPE_ID_COLUMN
		ELSE SELECT NULL AS SCOPE_ID_COLUMN
	END
END
GO