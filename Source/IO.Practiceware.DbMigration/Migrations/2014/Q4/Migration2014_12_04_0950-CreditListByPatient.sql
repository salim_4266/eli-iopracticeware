﻿IF OBJECT_ID ( 'dbo.CreditListByPatient', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.CreditListByPatient;
GO

CREATE PROCEDURE dbo.CreditListByPatient
	@ReceivableId NVARCHAR(100)
AS
SET NOCOUNT ON;

DECLARE @PatientId INT
SET @PatientId = (SELECT TOP 1 PatientId FROM PatientReceivables WHERE ReceivableId = @ReceivableId)

SELECT
adj.LegacyPaymentId AS PaymentId
,(1 * 1000000000 + adj.Id) AS ReceivableId 
,CASE 
	WHEN (adj.InsurerId IS NOT NULL AND adj.PatientId IS NOT NULL)
		THEN adj.InsurerId
	WHEN (adj.PatientId IS NOT NULL)
		THEN adj.PatientId
	ELSE 0
END AS PayerId
,CONVERT(NVARCHAR(1),CASE 
	WHEN adj.FinancialSourceTypeId = 1
		THEN 'I'
	WHEN adj.FinancialSourceTypeId = 2
		THEN 'P'
	WHEN adj.FinancialSourceTypeId = 3
		THEN 'O'
	WHEN adj.FinancialSourceTypeId = 4
		THEN ''
END) AS PayerType
,CASE 
	WHEN adj.AdjustmentTypeId = 1
		THEN 'P'
	WHEN adj.AdjustmentTypeId = 2
		THEN 'X'
	WHEN adj.AdjustmentTypeId = 3
		THEN '8'
	WHEN adj.AdjustmentTypeId = 4
		THEN 'R'
	WHEN adj.AdjustmentTypeId = 5
		THEN 'U'
	WHEN adj.AdjustmentTypeId > 1000
		THEN SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
END AS PaymentType
,CONVERT(REAL,adj.Amount) AS PaymentAmount
,CONVERT(NVARCHAR,adj.PostedDateTime,112) AS PaymentDate
,CONVERT(NVARCHAR,fb.CheckCode) AS PaymentCheck
,CONVERT(NVARCHAR(64),CASE
	WHEN adj.Comment IS NOT NULL
		THEN adj.Comment
	ELSE ''
END) AS PaymentRefId
,COALESCE(SUBSTRING(pctRefType.Code, (dbo.GetMax((CHARINDEX(' - ', pctRefType.Code)+3), 0)), LEN(pctRefType.Code)),'') AS PaymentRefType
,'A' AS [Status]
,'0' AS PaymentService
,'F' AS PaymentCommentOn
,CASE at.IsDebit 
	WHEN 1
		THEN 'D'
	ELSE 'C'
END AS PaymentFinancialType
,0 AS PaymentServiceItem
,0 AS PaymentAssignBy
,CASE 
	WHEN pctRsnCode.Code IS NOT NULL
		THEN pctRsnCode.Code
	ELSE ''		
END AS PaymentReason
,CASE
	WHEN fb.Id IS NULL
		THEN 0
	ELSE fb.Id
END AS PaymentBatchCheckId
,'' AS PaymentAppealNumber
,CASE 
	WHEN fb.PaymentDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,fb.PaymentDateTime,112)
	ELSE ''
END AS PaymentEOBDate
,adj.Amount AS PaymentAmountDecimal
,CONVERT(INT,NULL) AS ClaimAdjustmentGroupCodeId
FROM model.Adjustments adj
LEFT JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
LEFT JOIN dbo.PracticeCodeTable pct ON pct.Id = at.Id-1000
	AND pct.ReferenceType = 'PAYMENTTYPE' 
LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
LEFT JOIN model.PaymentMethods paym ON paym.Id = fb.PaymentMethodId
LEFT JOIN dbo.PracticeCodeTable pctRefType ON SUBSTRING(pctRefType.Code, 1, CHARINDEX(' - ', pctRefType.Code)) = paym.Name
	AND pctRefType.ReferenceType = 'PAYABLETYPE'
LEFT JOIN dbo.PracticeCodeTable pctRsnCode ON pctRsnCode.Id = adj.ClaimAdjustmentReasonCodeId 
	AND pctRsnCode.ReferenceType = 'PAYMENTREASON'
WHERE InvoiceReceivableId IS NULL
AND (adj.Amount > 0)
AND adj.PatientId = @PatientId
GO