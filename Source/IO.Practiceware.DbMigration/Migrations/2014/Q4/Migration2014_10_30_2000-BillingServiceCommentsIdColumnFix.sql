﻿
IF EXISTS(SELECT * FROM sys.key_constraints WHERE type = 'PK' AND Name = 'PK_BillingServiceComments')
	ALTER TABLE model.BillingServiceComments DROP CONSTRAINT PK_BillingServiceComments
GO
IF EXISTS (select * from sys.columns 
            where Name = N'Id' and Object_ID = Object_ID(N'model.BillingServiceComments') AND is_Identity = 1) 
	ALTER TABLE model.BillingServiceComments ALTER COLUMN Id BIGINT NOT NULL
GO
IF NOT EXISTS(SELECT * FROM sys.key_constraints WHERE type = 'PK' AND Name = 'PK_BillingServiceComments')
ALTER TABLE model.BillingServiceComments
ADD CONSTRAINT PK_BillingServiceComments PRIMARY KEY (Id)
GO
