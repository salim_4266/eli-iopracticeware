﻿UPDATE PracticeCodeTable SET WebsiteUrl = 'https://onetouch.apexedi.com/secure/Login.aspx?redir=%2fsecure%2fDefault.aspx'
WHERE ReferenceType = 'TRANSMITTYPE'
AND Code LIKE '%Apex EDI%'
AND WebsiteUrl IS NULL

UPDATE PracticeCodeTable SET WebsiteUrl = 'https://apps.availity.com/availity/web/public.elegant.login'
WHERE ReferenceType = 'TRANSMITTYPE'
AND Code LIKE '%Availity%'
AND WebsiteUrl IS NULL

UPDATE PracticeCodeTable SET WebsiteUrl = 'https://portal.capario.net/ws_portal/login.jsp;JSESSIONID=0dd1c5da-665a-4db1-bbe5-58ff39651962'
WHERE ReferenceType = 'TRANSMITTYPE'
AND Code LIKE '%Capario%'
AND WebsiteUrl IS NULL

UPDATE PracticeCodeTable SET WebsiteUrl = 'https://office.emdeon.com/secure/scripts/inq.dll?MfcISAPICommand=DoLogout'
WHERE ReferenceType = 'TRANSMITTYPE'
AND Code LIKE '%Emdeon%'
AND WebsiteUrl IS NULL

UPDATE PracticeCodeTable SET WebsiteUrl = 'http://www.gatewayedi.com/'
WHERE ReferenceType = 'TRANSMITTYPE'
AND Code LIKE '%Gateway%'
AND WebsiteUrl IS NULL

UPDATE PracticeCodeTable SET WebsiteUrl = 'https://clearinghouse.greenwaymedical.com/'
WHERE ReferenceType = 'TRANSMITTYPE'
AND Code LIKE '%GHN Online%'
AND WebsiteUrl IS NULL

UPDATE PracticeCodeTable SET WebsiteUrl = 'https://www.mdon-line.com/mdonline/default.asp'
WHERE ReferenceType = 'TRANSMITTYPE'
AND Code LIKE '%MDOnline%'
AND WebsiteUrl IS NULL

UPDATE PracticeCodeTable SET WebsiteUrl = 'https://ww3.navicure.com/login.html'
WHERE ReferenceType = 'TRANSMITTYPE'
AND Code LIKE '%Navicure%'
AND WebsiteUrl IS NULL

UPDATE PracticeCodeTable SET WebsiteUrl = 'https://www.payerpath.com/UI/Account/Login'
WHERE ReferenceType = 'TRANSMITTYPE'
AND Code LIKE '%Payer Path%'
AND WebsiteUrl IS NULL

UPDATE PracticeCodeTable SET WebsiteUrl = 'https://www.visionweb.com/login/login.jsp'
WHERE ReferenceType = 'TRANSMITTYPE'
AND Code LIKE '%VisionWeb%'
AND WebsiteUrl IS NULL

UPDATE PracticeCodeTable SET WebsiteUrl = 'https://public.zirmed.com/login/'
WHERE ReferenceType = 'TRANSMITTYPE'
AND Code LIKE '%Zirmed%'
AND WebsiteUrl IS NULL