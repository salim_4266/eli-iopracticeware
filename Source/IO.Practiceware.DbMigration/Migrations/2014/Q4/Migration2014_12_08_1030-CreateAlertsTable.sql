﻿IF (OBJECT_ID('model.Alerts') IS NOT NULL)
BEGIN
	EXEC dbo.DropObject 'model.Alerts'
END

CREATE TABLE model.Alerts (
	Id INT identity(1, 1) NOT NULL
	,PatientFinancialCommentId INT NULL
	,BillingServiceCommentId BIGINT NULL
	,InvoiceCommentId INT NULL
	,IsActive BIT NOT NULL
	,ExpirationDateTime DATETIME NULL
	)

GO



-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------
-- Creating primary key on [Id] in table 'Alerts'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.Alerts', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[Alerts] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.Alerts', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[Alerts]
ADD CONSTRAINT [PK_Alerts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO


-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------
-- Creating foreign key on [PatientFinancialCommentId] in table 'Alerts'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_AlertPatientFinancialComment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Alerts] DROP CONSTRAINT FK_AlertPatientFinancialComment

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientFinancialComments]'), 'IsUserTable') = 1
ALTER TABLE [model].[Alerts]
ADD CONSTRAINT [FK_AlertPatientFinancialComment]
    FOREIGN KEY ([PatientFinancialCommentId])
    REFERENCES [model].[PatientFinancialComments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AlertPatientFinancialComment'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_AlertPatientFinancialComment')
	DROP INDEX IX_FK_AlertPatientFinancialComment ON [model].[Alerts]
GO
CREATE INDEX [IX_FK_AlertPatientFinancialComment]
ON [model].[Alerts]
    ([PatientFinancialCommentId]);
GO

-- Creating foreign key on [BillingServiceCommentId] in table 'Alerts'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_AlertBillingServiceComment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Alerts] DROP CONSTRAINT FK_AlertBillingServiceComment

IF OBJECTPROPERTY(OBJECT_ID('[model].[BillingServiceComments]'), 'IsUserTable') = 1
ALTER TABLE [model].[Alerts]
ADD CONSTRAINT [FK_AlertBillingServiceComment]
    FOREIGN KEY ([BillingServiceCommentId])
    REFERENCES [model].[BillingServiceComments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AlertBillingServiceComment'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_AlertBillingServiceComment')
	DROP INDEX IX_FK_AlertBillingServiceComment ON [model].[Alerts]
GO
CREATE INDEX [IX_FK_AlertBillingServiceComment]
ON [model].[Alerts]
    ([BillingServiceCommentId]);
GO

-- Creating foreign key on [InvoiceCommentId] in table 'Alerts'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_AlertInvoiceComment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Alerts] DROP CONSTRAINT FK_AlertInvoiceComment

IF OBJECTPROPERTY(OBJECT_ID('[model].[InvoiceComments]'), 'IsUserTable') = 1
ALTER TABLE [model].[Alerts]
ADD CONSTRAINT [FK_AlertInvoiceComment]
    FOREIGN KEY ([InvoiceCommentId])
    REFERENCES [model].[InvoiceComments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AlertInvoiceComment'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_AlertInvoiceComment')
	DROP INDEX IX_FK_AlertInvoiceComment ON [model].[Alerts]
GO
CREATE INDEX [IX_FK_AlertInvoiceComment]
ON [model].[Alerts]
    ([InvoiceCommentId]);
GO
