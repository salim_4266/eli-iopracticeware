﻿UPDATE e
SET e.Error = NULL
FROM model.ExternalSystemMessages e
WHERE ExternalSystemExternalSystemMessageTypeId = (
		SELECT TOP 1 Id
		FROM model.ExternalSystemExternalSystemMessageTypes
		WHERE ExternalSystemId = (SELECT TOP 1 Id FROM model.ExternalSystems WHERE Name = 'Payer')
			AND ExternalSystemMessageTypeId = (SELECT TOP 1 Id FROM model.ExternalSystemMessageTypes WHERE Id = 36 AND Name = 'X12_835')
	)
	AND Error IS NOT NULL 
	AND Error <> ''
	AND SUBSTRING(Error, 1, 1) <> '<'
	AND SUBSTRING(Error, LEN(Error) , 1) <> '>'