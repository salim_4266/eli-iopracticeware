﻿IF EXISTS (SELECT * FROM sys.Triggers WHERE name = 'AppointmentReferralTrigger') BEGIN
DROP TRIGGER [dbo].[AppointmentReferralTrigger]
END
GO

IF EXISTS (SELECT * FROM sys.Triggers WHERE name = 'AppointmentsPreCertTrigger') BEGIN
DROP TRIGGER [dbo].[AppointmentsPreCertTrigger]
END
GO

IF EXISTS (SELECT * FROM sys.Triggers WHERE name = 'AppointmentsResourceId1ResourcesTrigger') BEGIN
DROP TRIGGER [dbo].[AppointmentsResourceId1ResourcesTrigger]
END
GO

IF EXISTS (SELECT * FROM sys.Triggers WHERE name = 'AppointmentsResourceId2PracticeSatelliteTrigger') BEGIN
DROP TRIGGER [dbo].[AppointmentsResourceId2PracticeSatelliteTrigger]
END
GO

IF EXISTS (SELECT * FROM sys.Triggers WHERE name = 'AppointmentsResourceId2ResourcesTrigger') BEGIN
DROP TRIGGER [dbo].[AppointmentsResourceId2ResourcesTrigger]
END
GO

IF EXISTS (SELECT * FROM sys.Triggers WHERE name = 'AppointmentsTrigger') BEGIN
DROP TRIGGER [dbo].[AppointmentsTrigger]
END
GO

IF EXISTS (SELECT * FROM sys.Triggers WHERE name = 'PatientDemoAltPatientIdTrigger') BEGIN
DROP TRIGGER [dbo].[PatientDemoAltPatientIdTrigger]
END
GO

IF EXISTS (SELECT * FROM sys.Triggers WHERE name = 'PatientDemographicsPatientIdPatientReceivablePaymentsTrigger') BEGIN
DROP TRIGGER [dbo].[PatientDemographicsPatientIdPatientReceivablePaymentsTrigger]
END
GO

IF EXISTS (SELECT * FROM sys.Triggers WHERE name = 'PracticeCodeTableEmployeeStatusTrigger') BEGIN
DROP TRIGGER [dbo].[PracticeCodeTableEmployeeStatusTrigger]
END
GO

IF EXISTS (SELECT * FROM sys.Triggers WHERE name = 'PracticeCodeTableLanguageTrigger') BEGIN
DROP TRIGGER [dbo].[PracticeCodeTableLanguageTrigger]
END
GO

--SELECT 'IF EXISTS (SELECT * FROM sys.Triggers WHERE name = ''' + st.name + ''') BEGIN
--DROP TRIGGER ['+  SCHEMA_NAME(so.schema_id) + '].[' + st.name + ']
--END
--GO
--'
--FROM sys.triggers st
--INNER JOIN sys.objects so ON so.object_id = st.object_id
--WHERE st.name IN ('AppointmentsPreCertTrigger',
--'AppointmentReferralTrigger',
--'AppointmentsResourceId2PracticeSatelliteTrigger',
--'AppointmentsTrigger',
--'AppointmentsResourceId2ResourcesTrigger',
--'PatientDemographicsPatientIdPatientReceivablePaymentsTrigger',
--'PatientDemoAltPatientIdTrigger',
--'PracticeCodeTableEmployeeStatusTrigger',
--'PracticeCodeTableLanguageTrigger',
--'AppointmentsResourceId1ResourcesTrigger')
