﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using FluentMigrator;
using IO.Practiceware.Configuration;
using IO.Practiceware.Storage;
using Soaf.Collections;

namespace IO.Practiceware.DbMigration.Migrations._2014.Q4
{
    [Migration(201411110600)]
    public class Migration201411110600CleanupServerDataPathRemitFolder : NonFluentTransactionalUpOnlyMigration
    {
        protected override void MigrateUp(IDbConnection connection, IDbTransaction transaction)
        {
            var remitDirectory = Path.Combine(ConfigurationManager.ServerDataPath, @"Remit");

            if (Directory.Exists(remitDirectory))
            {
                // Archive all unposted files existing files.
                var archivedDirectory = Path.Combine(remitDirectory, @"Archived_" + DateTime.Now.ToString("yyyy.MM.dd_HH.mm.ss"));
                Directory.CreateDirectory(archivedDirectory);

                Directory.GetFiles(remitDirectory).Where(f => Regex.IsMatch(f, @"\-\d\d\d\d\d\d\d\d") || f.Contains(@"Reject")).ToArray().ForEachWithSinglePropertyChangedNotification(invalidFile =>
                {
                    try
                    {
                        // ReSharper disable once AssignNullToNotNullAttribute
                        FileSystem.TryMoveFile(invalidFile, Path.Combine(archivedDirectory, Path.GetFileName(invalidFile)));
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError(ex.ToString());
                    }
                });

                Directory.GetDirectories(remitDirectory).ToArray().Where(d => GetDirectoryName(d) != GetDirectoryName(archivedDirectory)).ForEachWithSinglePropertyChangedNotification(directory =>
                {
                    try
                    {
                        FileSystem.TryMoveDirectory(directory, Path.Combine(Directory.GetParent(directory).FullName, GetDirectoryName(archivedDirectory), GetDirectoryName(directory)));
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError(ex.ToString());
                    }
                });
            }
  
        }

        public string GetDirectoryName(string path)
        {
            return new DirectoryInfo(path).Name;
        }
    }
}
