﻿--BEGIN TRAN
--SELECT 1 AS Value INTO #NoAudit
--GO
--COMMIT
--ROLLBACK

IF OBJECT_ID('dbo.PatientReceivablePaymentsBackup','U') IS NOT NULL
	AND OBJECT_ID('dbo.PatientReceivablesBackup','U') IS NOT NULL
	AND OBJECT_ID('dbo.PatientReceivablePaymentsMissingComputedFinancialIdAdjustments','U') IS NULL
BEGIN
	DECLARE @AdjustmentTypes TABLE (AdjustmentTypeIds INT, PaymentType NVARCHAR(MAX), IsDebit INT, ShortName NVARCHAR(MAX))
	RAISERROR('@AdjustmentTypes', 1, 1) WITH NOWAIT
	INSERT INTO @AdjustmentTypes (AdjustmentTypeIds, PaymentType, IsDebit, ShortName)
	SELECT 
	v.Id AS Id
	,v.PaymentType
	,v.IsDebit AS IsDebit
	,v.ShortName AS ShortName
	FROM (
		SELECT 
			CASE
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'P'
					THEN 1
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'X'
					THEN 2
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '8'
					THEN 3
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'R'
					THEN 4
				WHEN SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'U'
					THEN 5
				ELSE pct.Id + 1000
			END AS Id
			,SUBSTRING(Code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS PaymentType
			,CASE pct.AlternateCode
				WHEN 'D'
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
			END AS IsDebit
			,LetterTranslation AS ShortName
		FROM dbo.PracticeCodeTable pct
		WHERE pct.ReferenceType = 'PAYMENTTYPE'
			AND (pct.AlternateCode <> '' AND pct.AlternateCode IS NOT NULL)
		GROUP BY 
		Id
		,Code
		,pct.AlternateCode
		,LetterTranslation
	) AS v

	RAISERROR('Finding bad guys.', 1, 1) WITH NOWAIT
	;WITH CTE AS (
	SELECT DISTINCT
	a.PatientId
	,i.Id AS InsurerId
	,b.InvoiceDate
	,a.PatientId AS InsuredId
	,iv.Id AS InvoiceId
	,p.PaymentDate
	,CONVERT(DECIMAL(18,2),p.PaymentAmount) AS Amount
	,1 AS FinancialSourceTypeId
	,pay.Id AS PaymentMethodId
	,p.PaymentType
	,bs.Id AS PaymentServiceItem
	,p.ClaimAdjustmentGroupCodeId
	,p.PaymentReason
	,p.PaymentFinancialType
	,at.AdjustmentTypeIds AS AdjustmentTypeId
	,p.PaymentCheck AS CheckCode
	FROM dbo.PatientReceivablePaymentsBackup p
	JOIN dbo.PatientReceivablesBackup b on p.ReceivableId = b.ReceivableId
		AND b.InsurerId NOT IN (0, 99999)
		AND b.InsurerId IS NOT NULL
	JOIN dbo.Appointments a ON a.AppointmentId = b.AppointmentId
	JOIN model.Invoices iv ON iv.LegacyPatientReceivablesInvoice = b.Invoice
	LEFT JOIN dbo.PatientReceivableServicesBackup prsb ON prsb.ItemId = p.PaymentServiceItem
	LEFT JOIN model.BillingServices bs ON bs.LegacyPatientReceivableServicesItemId = prsb.ItemId
	LEFT JOIN model.Insurers i ON i.LegacyPracticeInsurersId = p.PayerId
		AND i.IsArchived <> 1
	LEFT JOIN model.PaymentMethods pay ON pay.LegacyPracticeCodeTablePaymentRefType = p.PaymentRefType
	JOIN @AdjustmentTypes at ON at.PaymentType = p.PaymentType
	WHERE b.ComputedFinancialId IS NULL
		AND p.PayerType = 'I'
		AND (p.PaymentFinancialType <> '' AND p.PaymentFinancialType IS NOT NULL)
	)
	SELECT CTE.*
	INTO #MissingPaymentsComputedFinancialId
	FROM CTE
	LEFT JOIN model.Adjustments a ON a.Amount = CTE.Amount
		AND a.FinancialSourceTypeId = 1
		AND CONVERT(NVARCHAR(8),a.PostedDateTime,112) = CTE.PaymentDate
		AND a.AdjustmentTypeId = CTE.AdjustmentTypeId
		AND a.InsurerId = CTE.InsurerId
		AND a.BillingServiceId = CTE.PaymentServiceItem
		AND a.PatientId IS NULL
	WHERE a.Id IS NULL
		
	CREATE TABLE dbo.PatientReceivablePaymentsMissingComputedFinancialIdInsurancePolicies (InsurancePolicyId INT)

	RAISERROR('InsurancePolicies', 1, 1) WITH NOWAIT
	INSERT INTO model.InsurancePolicies (
		PolicyCode
		,GroupCode
		,Copay
		,Deductible
		,StartDateTime
		,PolicyHolderPatientId
		,InsurerId
		,MedicareSecondaryReasonCodeId
	)
	OUTPUT inserted.Id INTO PatientReceivablePaymentsMissingComputedFinancialIdInsurancePolicies (InsurancePolicyId)
	SELECT DISTINCT
	'DudPolicy' AS PolicyCode
	,'' AS GroupCode
	,0 AS Copay
	,0 AS Deductible
	,CONVERT(DATETIME, '20030101') AS StartDateTime
	,mpc.InsuredId AS PolicyHolderPatientId
	,mpc.InsurerId
	,NULL AS MedicareSecondaryReasonCodeId
	FROM #MissingPaymentsComputedFinancialId mpc
	INNER JOIN model.Insurers ins ON ins.Id = mpc.InsurerId

	DECLARE @PatientInsurances TABLE (Id INT)

	RAISERROR('PatientInsurances', 1, 1) WITH NOWAIT
	INSERT INTO model.PatientInsurances (
		InsuranceTypeId
		,PolicyHolderRelationshipTypeId
		,OrdinalId
		,InsuredPatientId
		,InsurancePolicyId
		,EndDateTime
		,IsDeleted
	)
	OUTPUT inserted.Id
	INTO @PatientInsurances (Id)
	SELECT DISTINCT
	4 AS InsuranceTypeId
	,1 AS PolicyHolderRelationshipTypeId
	,3 AS OrdinalId
	,PatientId AS InsuredPatientId
	,ip.Id AS InsurancePolicyId
	,CONVERT(DATETIME, '20030102') AS EndDateTime
	,CONVERT(BIT,0) AS IsDeleted
	FROM #MissingPaymentsComputedFinancialId m
	JOIN model.InsurancePolicies ip ON ip.PolicyHolderPatientId = m.PatientId
		AND ip.PolicyCode = 'DudPolicy'
		AND ip.StartDateTime = CONVERT(DATETIME, '20030101')
		AND ip.InsurerId = m.InsurerId
	
	CREATE TABLE dbo.PatientReceivablePaymentsMissingComputedFinancialIdPatientInsurances (PatientInsuranceId INT)
	INSERT INTO PatientReceivablePaymentsMissingComputedFinancialIdPatientInsurances (PatientInsuranceId )
	SELECT Id FROM @PatientInsurances 

	DECLARE @InvoiceReceivables TABLE (Id INT)
	RAISERROR('InvoiceReceivables', 1, 1) WITH NOWAIT
	INSERT INTO model.InvoiceReceivables (
		InvoiceId
		,PatientInsuranceId
		,PayerClaimControlNumber
		,PatientInsuranceAuthorizationId
		,PatientInsuranceReferralId
		,OpenForReview
		,LegacyInsurerDesignation
	)
	OUTPUT inserted.Id
	INTO @InvoiceReceivables (Id)
	SELECT DISTINCT
	m.InvoiceId
	,p.Id
	,NULL AS PayerClaimControlNumber
	,NULL AS PatientInsuranceAuthorizationId
	,NULL AS PatientInsuranceReferralId
	,CONVERT(BIT,0) AS OpenForReview
	,NULL AS LegacyInsurerDesignation
	FROM #MissingPaymentsComputedFinancialId m
	JOIN model.PatientInsurances p ON p.InsuredPatientId = m.PatientId
	JOIN model.InsurancePolicies ip ON ip.Id = p.InsurancePolicyId
		AND m.InsurerId = ip.InsurerId
	JOIN @PatientInsurances pi ON pi.Id = p.Id
	JOIN dbo.PatientReceivablePaymentsMissingComputedFinancialIdInsurancePolicies i ON i.InsurancePolicyId = ip.Id

	CREATE TABLE dbo.PatientReceivablePaymentsMissingComputedFinancialIdInvoiceReceivables (InvoiceReceivableId INT)
	INSERT INTO dbo.PatientReceivablePaymentsMissingComputedFinancialIdInvoiceReceivables (InvoiceReceivableId)
	SELECT Id FROM @InvoiceReceivables

	DECLARE @FinancialBatches TABLE (Id INT)
	RAISERROR('FinancialBatches', 1, 1) WITH NOWAIT
	INSERT INTO model.FinancialBatches (
		FinancialSourceTypeId
		,InsurerId
		,PaymentDateTime
		,Amount
		,CheckCode
	)
	OUTPUT inserted.Id
	INTO @FinancialBatches (Id)
	SELECT DISTINCT
	1 AS FinancialSourceTypeId
	,InsurerId
	,CONVERT(DATETIME,PaymentDate) AS PaymentDate
	,Amount
	,CASE WHEN CheckCode = '' THEN PaymentDate ELSE CheckCode END AS CheckCode
	FROM #MissingPaymentsComputedFinancialId

	CREATE TABLE dbo.PatientReceivablePaymentsMissingComputedFinancialIdFinancialBatches (FinancialBatchId INT)
	INSERT INTO PatientReceivablePaymentsMissingComputedFinancialIdFinancialBatches (FinancialBatchId )
	SELECT Id FROM @FinancialBatches 

	CREATE TABLE dbo.PatientReceivablePaymentsMissingComputedFinancialIdAdjustments (AdjustmentId INT)

	RAISERROR('Adjustments', 1, 1) WITH NOWAIT
	INSERT INTO model.Adjustments (
		FinancialSourceTypeId
		,Amount
		,PostedDateTime
		,AdjustmentTypeId
		,BillingServiceId
		,InvoiceReceivableId
		,PatientId
		,InsurerId
		,FinancialBatchId
		,ClaimAdjustmentGroupCodeId
		,ClaimAdjustmentReasonCodeId
		,Comment
		,IncludeCommentOnStatement
	)
	OUTPUT inserted.Id
	INTO dbo.PatientReceivablePaymentsMissingComputedFinancialIdAdjustments (AdjustmentId)
	SELECT
	1 AS FinancialSourceTypeId
	,v.Amount AS Amount
	,CONVERT(DATETIME,v.PaymentDate) AS PostedDateTime
	,v.AdjustmentTypeId AS AdjustmentTypeId
	,v.PaymentServiceItem AS BillingServiceId
	,ir.Id AS InvoiceReceivableId
	,NULL AS PatientId
	,v.InsurerId AS InsurerId
	,f.Id AS FinancialBatchId
	,CASE
		WHEN v.ClaimAdjustmentGroupCodeId IS NULL 
			THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
				-- These int values correspond to the ClaimAdjustmentGroupCode enum in .NET
				WHEN 'X' -- AdjustmentTypeId = 2
					THEN 1 -- ContractualObligation
				WHEN '8' -- AdjustmentTypeId = 3
					THEN 3 -- OtherAdjustment
				WHEN 'R' -- AdjustmentTypeId = 4
					THEN 4 -- CorrectionReversal
				WHEN 'U' -- AdjustmentTypeId = 5
					THEN 4 -- CorrectionReversal
				END
		ELSE
			v.ClaimAdjustmentGroupCodeId
	END AS ClaimAdjustmentGroupCodeId
	,CASE
		WHEN v.PaymentReason IS NULL 
			THEN 
				CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
					WHEN 'X' -- AdjustmentTypeId = 2
						THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45') -- ReasonCode 45
					WHEN '8' -- AdjustmentTypeId = 3
						THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45')-- ReasonCode 45
				END
		WHEN v.PaymentReason = ''
			THEN 
				CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
					WHEN 'X' -- AdjustmentTypeId = 2
						THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45')
					WHEN '8' -- AdjustmentTypeId = 3
						THEN (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45')
				END
		ELSE (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = v.PaymentReason)
	END AS ClaimAdjustmentReasonCodeId
	,CONVERT(NVARCHAR(MAX),NULL) AS Comment
	,CONVERT(BIT,0) AS IncludeCommentOnStatement
	FROM #MissingPaymentsComputedFinancialId v
	JOIN model.Invoices inv ON inv.Id = v.InvoiceId
	JOIN model.PatientInsurances p ON p.InsuredPatientId = v.PatientId
	JOIN model.InsurancePolicies ip ON ip.Id = p.InsurancePolicyId
		AND v.InsurerId = ip.InsurerId
	JOIN @PatientInsurances pi ON pi.Id = p.Id
	JOIN dbo.PatientReceivablePaymentsMissingComputedFinancialIdInsurancePolicies i ON i.InsurancePolicyId = ip.Id
	JOIN model.InvoiceReceivables ir ON ir.PatientInsuranceId = pi.Id
		AND ir.InvoiceId = inv.Id
	JOIN @InvoiceReceivables invr ON invr.Id = ir.Id
	JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PAYMENTTYPE' AND 
		v.PaymentType = SUBSTRING(pct.Code, LEN(pct.Code), 1)
	JOIN model.FinancialBatches f ON f.InsurerId = v.InsurerId
		AND f.PaymentDateTime = v.PaymentDate
		AND f.Amount = v.Amount
		AND (f.CheckCode = v.PaymentDate OR f.CheckCode = v.CheckCode)
	JOIN @FinancialBatches ifb ON ifb.Id = f.Id
	WHERE v.PaymentFinancialType <> '' 
		AND v.PaymentFinancialType IS NOT NULL


END
GO