﻿-- Add column PaymentMethodId to model.FinancialBatches
IF NOT EXISTS (SELECT * from sys.columns WHERE Name = N'PaymentMethodId' AND Object_ID = Object_ID(N'model.FinancialBatches')) 
	ALTER TABLE model.FinancialBatches ADD PaymentMethodId int NULL
GO

--update rows of model.FinancialBatches
IF EXISTS (SELECT * from sys.columns WHERE Name = N'PaymentMethodId' AND Object_ID = Object_ID(N'model.Adjustments')) 
	UPDATE f
	SET f.PaymentMethodId = a.PaymentMethodId
	FROM model.FinancialBatches f
	JOIN model.Adjustments a ON a.FinancialBatchId = f.Id
GO

-- Creating foreign key on [PaymentMethodId] in table 'FinancialBatches'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_FinancialBatchPaymentMethod'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[FinancialBatches] DROP CONSTRAINT FK_FinancialBatchPaymentMethod

IF OBJECTPROPERTY(OBJECT_ID('[model].[PaymentMethods]'), 'IsUserTable') = 1
ALTER TABLE [model].[FinancialBatches]
ADD CONSTRAINT [FK_FinancialBatchPaymentMethod]
    FOREIGN KEY ([PaymentMethodId])
    REFERENCES [model].[PaymentMethods]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_FinancialBatchPaymentMethod'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_FinancialBatchPaymentMethod')
	DROP INDEX IX_FK_FinancialBatchPaymentMethod ON [model].[FinancialBatches]
GO
CREATE INDEX [IX_FK_FinancialBatchPaymentMethod]
ON [model].[FinancialBatches]
    ([PaymentMethodId]);
GO

-- Dropping FOREIGN KEY 'FK_AdjustmentPaymentMethod'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_AdjustmentPaymentMethod'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Adjustments] DROP CONSTRAINT FK_AdjustmentPaymentMethod
GO

-- Dropping non-clustered index for FOREIGN KEY 'FK_AdjustmentPaymentMethod'
IF EXISTS (SELECT * FROM sys.indexes WHERE NAME = N'IX_FK_AdjustmentPaymentMethod' AND object_id = OBJECT_ID(N'[model].[Adjustments]'))
	DROP INDEX IX_FK_AdjustmentPaymentMethod ON [model].[Adjustments]
GO

-- Drop column PaymentMethodId from model.Adjustments
IF EXISTS (SELECT * from sys.columns WHERE Name = N'PaymentMethodId' AND Object_ID = Object_ID(N'model.Adjustments')) 
	ALTER TABLE model.Adjustments DROP COLUMN PaymentMethodId
GO
