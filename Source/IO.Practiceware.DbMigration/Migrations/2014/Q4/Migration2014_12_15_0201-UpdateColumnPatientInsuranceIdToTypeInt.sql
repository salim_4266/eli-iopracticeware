﻿DECLARE @sql NVARCHAR(MAX)
SET @sql = '
DECLARE @InvalidPatientInsurances TABLE (Id INT)

INSERT INTO @InvalidPatientInsurances (Id)
SELECT PatientInsuranceId FROM model.PatientInsuranceDocuments WHERE PatientInsuranceId NOT IN (SELECT Id FROM model.PatientInsurances)
UNION
SELECT PatientInsuranceId FROM model.InvoiceReceivables WHERE PatientInsuranceId NOT IN (SELECT Id FROM model.PatientInsurances) AND PatientInsuranceId IS NOT NULL

IF OBJECT_ID(''dbo.DeletedBillingServiceTransactionsMissingInsurerId'',''U'') IS NULL
BEGIN
	SELECT * INTO dbo.DeletedBillingServiceTransactionsMissingInsurerId FROM model.BillingServiceTransactions WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
END
ELSE 
BEGIN
	INSERT INTO dbo.DeletedBillingServiceTransactionsMissingInsurerId (Id,DateTime,AmountSent,ClaimFileNumber,BillingServiceTransactionStatusId,MethodSentId,BillingServiceId,InvoiceReceivableId,ClaimFileReceiverId,ExternalSystemMessageId,PatientAggregateStatementId,LegacyTransactionId)
	SELECT Id,DateTime,AmountSent,ClaimFileNumber,BillingServiceTransactionStatusId,MethodSentId,BillingServiceId,InvoiceReceivableId,ClaimFileReceiverId,ExternalSystemMessageId,PatientAggregateStatementId,LegacyTransactionId FROM model.BillingServiceTransactions WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
END

IF OBJECT_ID(''dbo.DeletedAdjustmentsMissingInsurerId'',''U'') IS NULL
BEGIN
	SELECT * INTO dbo.DeletedAdjustmentsMissingInsurerId FROM model.Adjustments WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
END
ELSE
BEGIN
	SET IDENTITY_INSERT dbo.DeletedAdjustmentsMissingInsurerId ON
	INSERT INTO dbo.DeletedAdjustmentsMissingInsurerId (Id,FinancialSourceTypeId,Amount,PostedDateTime,AdjustmentTypeId,BillingServiceId,InvoiceReceivableId,PatientId,InsurerId,FinancialBatchId,ClaimAdjustmentGroupCodeId,ClaimAdjustmentReasonCodeId,Comment,IncludeCommentOnStatement,LegacyInvoiceReceivableId,LegacyPaymentId)
	SELECT Id,FinancialSourceTypeId,Amount,PostedDateTime,AdjustmentTypeId,BillingServiceId,InvoiceReceivableId,PatientId,InsurerId,FinancialBatchId,ClaimAdjustmentGroupCodeId,ClaimAdjustmentReasonCodeId,Comment,IncludeCommentOnStatement,LegacyInvoiceReceivableId,LegacyPaymentId FROM model.Adjustments WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
	SET IDENTITY_INSERT dbo.DeletedAdjustmentsMissingInsurerId OFF
END

IF OBJECT_ID(''dbo.DeletedFinancialInformationsMissingInsurerId'',''U'') IS NULL
BEGIN
	SELECT * INTO dbo.DeletedFinancialInformationsMissingInsurerId FROM model.FinancialInformations WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
END
ELSE
BEGIN
	SET IDENTITY_INSERT dbo.DeletedFinancialInformationsMissingInsurerId ON
	INSERT INTO dbo.DeletedFinancialInformationsMissingInsurerId (Id,Amount,PostedDateTime,FinancialInformationTypeId,BillingServiceId,InvoiceReceivableId,FinancialSourceTypeId,PatientId,InsurerId,FinancialBatchId,ClaimAdjustmentGroupCodeId,ClaimAdjustmentReasonCodeId,Comment,IncludeCommentOnStatement,LegacyInvoiceReceivableId,LegacyPaymentId)
	SELECT Id,Amount,PostedDateTime,FinancialInformationTypeId,BillingServiceId,InvoiceReceivableId,FinancialSourceTypeId,PatientId,InsurerId,FinancialBatchId,ClaimAdjustmentGroupCodeId,ClaimAdjustmentReasonCodeId,Comment,IncludeCommentOnStatement,LegacyInvoiceReceivableId,LegacyPaymentId FROM model.FinancialInformations WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
	SET IDENTITY_INSERT dbo.DeletedFinancialInformationsMissingInsurerId OFF
END

IF OBJECT_ID(''dbo.DeletedInvoiceReceivablesMissingInsurerId'',''U'') IS NULL
BEGIN
	SELECT * INTO dbo.DeletedInvoiceReceivablesMissingInsurerId FROM model.InvoiceReceivables WHERE Id IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
END
ELSE
BEGIN
	SET IDENTITY_INSERT dbo.DeletedInvoiceReceivablesMissingInsurerId ON
	INSERT INTO dbo.DeletedInvoiceReceivablesMissingInsurerId (Id,InvoiceId,PatientInsuranceId,PayerClaimControlNumber,PatientInsuranceAuthorizationId,PatientInsuranceReferralId,OpenForReview,LegacyInsurerDesignation)
	SELECT Id,InvoiceId,PatientInsuranceId,PayerClaimControlNumber,PatientInsuranceAuthorizationId,PatientInsuranceReferralId,OpenForReview,LegacyInsurerDesignation FROM model.InvoiceReceivables WHERE Id IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
	SET IDENTITY_INSERT dbo.DeletedInvoiceReceivablesMissingInsurerId OFF
END

IF OBJECT_ID(''dbo.DeletedPatientInsuranceDocumentsMissingInsurerId'',''U'') IS NULL
BEGIN
	SELECT * INTO dbo.DeletedPatientInsuranceDocumentsMissingInsurerId FROM model.PatientInsuranceDocuments WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances)
END
ELSE
BEGIN
	SET IDENTITY_INSERT dbo.DeletedPatientInsuranceDocumentsMissingInsurerId ON
	INSERT INTO dbo.DeletedPatientInsuranceDocumentsMissingInsurerId (Id,Content,PatientInsuranceId)
	SELECT Id,Content,PatientInsuranceId FROM model.PatientInsuranceDocuments WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances)
	SET IDENTITY_INSERT dbo.DeletedPatientInsuranceDocumentsMissingInsurerId OFF
END

IF OBJECT_ID(''dbo.DeletedPatientInsurancesMissingInsurerId'',''U'') IS NULL
	SELECT * INTO dbo.DeletedPatientInsurancesMissingInsurerId FROM model.PatientInsurances WHERE Id IN (SELECT Id FROM @InvalidPatientInsurances)
ELSE
BEGIN
	SET IDENTITY_INSERT dbo.DeletedPatientInsurancesMissingInsurerId ON
	INSERT INTO dbo.DeletedPatientInsurancesMissingInsurerId (Id,InsuranceTypeId,PolicyHolderRelationshipTypeId,OrdinalId,InsuredPatientId,InsurancePolicyId,EndDateTime,IsDeleted)
	SELECT Id,InsuranceTypeId,PolicyHolderRelationshipTypeId,OrdinalId,InsuredPatientId,InsurancePolicyId,EndDateTime,IsDeleted FROM model.PatientInsurances WHERE Id IN (SELECT Id FROM @InvalidPatientInsurances)
	SET IDENTITY_INSERT dbo.DeletedPatientInsurancesMissingInsurerId OFF
END

IF OBJECT_ID(''dbo.DeletedInsurancePoliciesInsurerId'',''U'') IS NULL
BEGIN
	SELECT * INTO dbo.DeletedInsurancePoliciesInsurerId FROM model.InsurancePolicies WHERE InsurerId IN (
		SELECT DISTINCT
		i.InsurerId
		FROM model.InsurancePolicies i
		LEFT JOIN model.Insurers ins ON ins.Id = i.InsurerId
		WHERE ins.Id IS NULL
	)
END
ELSE
BEGIN
	SET IDENTITY_INSERT dbo.DeletedInsurancePoliciesInsurerId ON
	INSERT INTO dbo.DeletedInsurancePoliciesInsurerId (Id,PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
	SELECT Id,PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId FROM model.InsurancePolicies WHERE InsurerId IN (
		SELECT DISTINCT
		i.InsurerId
		FROM model.InsurancePolicies i
		LEFT JOIN model.Insurers ins ON ins.Id = i.InsurerId
		WHERE ins.Id IS NULL
	)
	SET IDENTITY_INSERT dbo.DeletedInsurancePoliciesInsurerId OFF
END

DELETE FROM model.BillingServiceTransactions WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
DELETE FROM model.Adjustments WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
DELETE FROM model.FinancialInformations WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
DELETE FROM model.InvoiceReceivables WHERE Id IN (SELECT Id FROM model.InvoiceReceivables WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances))
DELETE FROM model.PatientInsuranceDocuments WHERE PatientInsuranceId IN (SELECT Id FROM @InvalidPatientInsurances)
DELETE FROM model.PatientInsurances WHERE Id IN (SELECT Id FROM @InvalidPatientInsurances)
DELETE FROM model.InsurancePolicies WHERE InsurerId IN (
		SELECT DISTINCT
		i.InsurerId
		FROM model.InsurancePolicies i
		LEFT JOIN model.Insurers ins ON ins.Id = i.InsurerId
		WHERE ins.Id IS NULL
	)
'
EXEC sp_executesql @sql
GO

IF OBJECT_ID('dbo.PatientFinancial','V') IS NOT NULL
	DROP VIEW dbo.PatientFinancial
GO

-- Drop all FK CONSTRAINTS and INDEX referring PatientInsuranceId
-- PatientInsuranceReferrals Changes
IF EXISTS (SELECT *	FROM sys.indexes WHERE NAME = N'IX_FK_PatientInsurancePatientInsuranceReferral' AND object_id = OBJECT_ID(N'model.PatientInsuranceReferrals'))
	DROP INDEX [IX_FK_PatientInsurancePatientInsuranceReferral] ON [model].[PatientInsuranceReferrals]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS	WHERE CONSTRAINT_NAME = 'FK_PatientInsurancePatientInsuranceReferral')
	ALTER TABLE model.PatientInsuranceReferrals DROP CONSTRAINT FK_PatientInsurancePatientInsuranceReferral
GO

-- PatientInsuranceAuthorizations Changes
IF EXISTS (SELECT *	FROM sys.indexes WHERE NAME = N'IX_FK_PatientInsurancePatientInsuranceAuthorization' AND object_id = OBJECT_ID(N'model.PatientInsuranceAuthorizations'))
	DROP INDEX [IX_FK_PatientInsurancePatientInsuranceAuthorization] ON [model].[PatientInsuranceAuthorizations]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS	WHERE CONSTRAINT_NAME = 'FK_PatientInsurancePatientInsuranceAuthorization')
	ALTER TABLE model.PatientInsuranceAuthorizations DROP CONSTRAINT FK_PatientInsurancePatientInsuranceAuthorization
GO

-- PatientInsuranceDocuments Changes
IF EXISTS (SELECT *	FROM sys.indexes WHERE NAME = N'IX_FK_PatientInsurancePatientInsuranceDocument' AND object_id = OBJECT_ID(N'model.PatientInsuranceDocuments'))
	DROP INDEX [IX_FK_PatientInsurancePatientInsuranceDocument] ON [model].[PatientInsuranceDocuments]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS	WHERE CONSTRAINT_NAME = 'FK_PatientInsurancePatientInsuranceDocument')
	ALTER TABLE model.PatientInsuranceDocuments DROP CONSTRAINT FK_PatientInsurancePatientInsuranceDocument
GO

-- InvoiceReceivables Changes
IF EXISTS (SELECT *	FROM sys.indexes WHERE NAME = N'IX_FK_InvoiceReceivablePatientInsurance' AND object_id = OBJECT_ID(N'model.InvoiceReceivables'))
	DROP INDEX [IX_FK_InvoiceReceivablePatientInsurance] ON [model].[InvoiceReceivables]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS	WHERE CONSTRAINT_NAME = 'FK_InvoiceReceivablePatientInsurance')
	ALTER TABLE model.InvoiceReceivables DROP CONSTRAINT FK_InvoiceReceivablePatientInsurance
GO

---- Updating Id column type from 'bigint' to 'int' for PatientInsurance model entity
IF EXISTS(SELECT * FROM sys.key_constraints WHERE type = 'PK' AND Name = 'PK_PatientInsurances')
	ALTER TABLE model.PatientInsurances DROP CONSTRAINT PK_PatientInsurances
GO

IF EXISTS (SELECT * FROM sys.columns WHERE Name = N'Id' and Object_ID = Object_ID(N'model.PatientInsurances') AND is_Identity = 1) 
	--SET ANSI_WARNINGS OFF
	
	ALTER TABLE model.PatientInsurances ALTER COLUMN Id INT NOT NULL

	--SET ANSI_WARNINGS ON
GO

IF NOT EXISTS(SELECT * FROM sys.key_constraints WHERE type = 'PK' AND Name = 'PK_PatientInsurances')
	ALTER TABLE model.PatientInsurances	ADD CONSTRAINT PK_PatientInsurances PRIMARY KEY (Id)
GO

-- Updating PatientInsuranceId column type from 'bigint' to 'int' for all model entities
-- PatientInsuranceReferrals Changes
IF EXISTS (SELECT *	FROM sys.columns WHERE NAME = N'PatientInsuranceId' AND Object_ID = Object_ID(N'model.PatientInsuranceReferrals'))
	ALTER TABLE model.PatientInsuranceReferrals ALTER COLUMN PatientInsuranceId INT NOT NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_PatientInsurancePatientInsuranceReferral')
	ALTER TABLE model.PatientInsuranceReferrals ADD CONSTRAINT FK_PatientInsurancePatientInsuranceReferral FOREIGN KEY (PatientInsuranceId) REFERENCES model.PatientInsurances (Id)
GO

IF NOT EXISTS (SELECT *	FROM sys.indexes WHERE NAME = N'IX_FK_PatientInsurancePatientInsuranceReferral' AND object_id = OBJECT_ID(N'model.PatientInsuranceReferrals'))
	CREATE NONCLUSTERED INDEX [IX_FK_PatientInsurancePatientInsuranceReferral] ON [model].[PatientInsuranceReferrals] ([PatientInsuranceId])
GO

-- PatientInsuranceAuthorizations Changes
IF EXISTS (SELECT *	FROM sys.columns WHERE NAME = N'PatientInsuranceId' AND Object_ID = Object_ID(N'model.PatientInsuranceAuthorizations'))
	ALTER TABLE model.PatientInsuranceAuthorizations ALTER COLUMN PatientInsuranceId INT NOT NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_PatientInsurancePatientInsuranceAuthorization')
	ALTER TABLE model.PatientInsuranceAuthorizations ADD CONSTRAINT FK_PatientInsurancePatientInsuranceAuthorization FOREIGN KEY (PatientInsuranceId) REFERENCES model.PatientInsurances (Id)
GO

IF NOT EXISTS (SELECT *	FROM sys.indexes WHERE NAME = N'IX_FK_PatientInsurancePatientInsuranceAuthorization' AND object_id = OBJECT_ID(N'model.PatientInsuranceAuthorizations'))
	CREATE NONCLUSTERED INDEX [IX_FK_PatientInsurancePatientInsuranceAuthorization] ON [model].[PatientInsuranceAuthorizations] ([PatientInsuranceId])
GO

-- PatientInsuranceDocuments Changes
IF EXISTS (SELECT *	FROM sys.columns WHERE NAME = N'PatientInsuranceId' AND Object_ID = Object_ID(N'model.PatientInsuranceDocuments'))
	ALTER TABLE model.PatientInsuranceDocuments ALTER COLUMN PatientInsuranceId INT NOT NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_PatientInsurancePatientInsuranceDocument')
	ALTER TABLE model.PatientInsuranceDocuments ADD CONSTRAINT FK_PatientInsurancePatientInsuranceDocument FOREIGN KEY (PatientInsuranceId) REFERENCES model.PatientInsurances (Id)
GO

IF NOT EXISTS (SELECT *	FROM sys.indexes WHERE NAME = N'IX_FK_PatientInsurancePatientInsuranceDocument' AND object_id = OBJECT_ID(N'model.PatientInsuranceDocuments'))
	CREATE NONCLUSTERED INDEX [IX_FK_PatientInsurancePatientInsuranceDocument] ON [model].[PatientInsuranceDocuments] ([PatientInsuranceId])
GO
 
-- InvoiceReceivables Changes
IF EXISTS (SELECT *	FROM sys.columns WHERE NAME = N'PatientInsuranceId' AND Object_ID = Object_ID(N'model.InvoiceReceivables'))
	ALTER TABLE model.InvoiceReceivables ALTER COLUMN PatientInsuranceId INT NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_InvoiceReceivablePatientInsurance')
	ALTER TABLE model.InvoiceReceivables ADD CONSTRAINT FK_InvoiceReceivablePatientInsurance FOREIGN KEY (PatientInsuranceId) REFERENCES model.PatientInsurances (Id)
GO

IF NOT EXISTS (SELECT *	FROM sys.indexes WHERE NAME = N'IX_FK_InvoiceReceivablePatientInsurance' AND object_id = OBJECT_ID(N'model.InvoiceReceivables'))
	CREATE NONCLUSTERED INDEX [IX_FK_InvoiceReceivablePatientInsurance] ON [model].[InvoiceReceivables] ([PatientInsuranceId])
GO

-- create PatientFinancial view
CREATE VIEW [dbo].[PatientFinancial] WITH SCHEMABINDING, VIEW_METADATA
AS
SELECT
pi.Id AS FinancialId
,InsuredPatientId AS PatientId
,ip.InsurerId AS FinancialInsurerId
,CONVERT(NVARCHAR(32),PolicyCode) AS FinancialPerson
,CONVERT(NVARCHAR(32),GroupCode) AS FinancialGroupId
,CONVERT(REAL,Copay) AS FinancialCopay
,pi.OrdinalId AS FinancialPIndicator
,CONVERT(NVARCHAR(10),ip.StartDateTime,112) AS FinancialStartDate
,CASE
	WHEN EndDateTime IS NULL
		THEN ''
	ELSE CONVERT(NVARCHAR(10),pi.EndDateTime,112) END AS FinancialEndDate
,CASE 
	WHEN (pi.EndDateTime IS NULL OR pi.EndDateTime >= GETDATE()) AND pi.IsDeleted = 0
		THEN CONVERT(NVARCHAR(1),'C')
	ELSE CONVERT(NVARCHAR(1),'X')
END AS Status
,CASE pi.InsuranceTypeId
	WHEN '2'
		THEN CONVERT(NVARCHAR(1),'V')
	WHEN '3'
		THEN CONVERT(NVARCHAR(1),'A')			
	WHEN '4'
		THEN CONVERT(NVARCHAR(1),'W')
	ELSE CONVERT(NVARCHAR(1),'M')
END AS FinancialInsType
FROM model.PatientInsurances pi
JOIN model.InsurancePolicies ip ON ip.Id=pi.InsurancePolicyId
WHERE PolicyHolderPatientId=InsuredPatientId
GO
