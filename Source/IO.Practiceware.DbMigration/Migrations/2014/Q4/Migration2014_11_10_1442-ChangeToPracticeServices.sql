﻿IF OBJECT_ID('dbo.PracticeServices','V') IS NOT NULL
	DROP VIEW dbo.PracticeServices
GO

CREATE VIEW [dbo].[PracticeServices]
WITH VIEW_METADATA
AS
SELECT 
CONVERT(INT,es.Id) AS CodeId
,CONVERT(NVARCHAR(16),es.Code) AS Code
,CONVERT(NVARCHAR(1),'P') AS CodeType
,CONVERT(NVARCHAR(32),COALESCE(UPPER(est.Name),'')) AS CodeCategory
,CONVERT(NVARCHAR(64),es.Description) AS Description
,CONVERT(REAL,es.UnitFee) AS Fee
,CONVERT(NVARCHAR(32),'') AS TOS
,CONVERT(NVARCHAR(32),'') AS POS
,COALESCE(CONVERT(NVARCHAR(10),es.StartDateTime,112),'') AS StartDate
,COALESCE(CONVERT(NVARCHAR(10),es.EndDateTime,112),'') AS EndDate
,CONVERT(REAL,es.RelativeValueUnit) AS RelativeValueUnit
,CONVERT(NVARCHAR(1),CASE WHEN es.IsOrderingProviderRequiredOnClaim = 1 THEN 'T' ELSE 'F' END) AS OrderDoc
,CONVERT(NVARCHAR(1),CASE WHEN es.IsReferringDoctorRequiredOnClaim = 1 THEN 'T' ELSE 'F' END) AS ConsultOn
,CONVERT(NVARCHAR(128),COALESCE(es.UnclassifiedServiceDescription,'')) AS ClaimNote
,CONVERT(NVARCHAR(1),CASE WHEN es.IsZeroChargeAllowedOnClaim = 1 THEN 'T' ELSE 'F' END) AS ServiceFilter
,CONVERT(NVARCHAR(16),COALESCE(es.NDC,'')) AS NDC
,CONVERT(INT,'') AS RevenueCodeId
,CONVERT(BIT,0) AS IsCliaCertificateRequiredOnClaim
FROM model.EncounterServices es
LEFT JOIN model.EncounterServiceTypes est ON est.Id = es.EncounterServiceTypeId

UNION ALL

SELECT 
CONVERT(INT,(1* 110000000) + fes.Id) AS CodeId
,CONVERT(NVARCHAR(16),fes.Code) AS Code
,CONVERT(NVARCHAR(1),'P') AS CodeType
,CONVERT(NVARCHAR(32),COALESCE(UPPER(est.Name),'')) AS CodeCategory
,CONVERT(NVARCHAR(64),fes.Description) AS Description
,CONVERT(REAL,fes.UnitFee) AS Fee
,CONVERT(NVARCHAR(32),'') AS TOS
,CONVERT(NVARCHAR(32),'98') AS POS
,COALESCE(CONVERT(NVARCHAR(10),fes.StartDateTime,112),'') AS StartDate
,COALESCE(CONVERT(NVARCHAR(10),fes.EndDateTime,112),'') AS EndDate
,CONVERT(REAL,fes.RelativeValueUnit) AS RelativeValueUnit
,CONVERT(NVARCHAR(1),'F') AS OrderDoc
,CONVERT(NVARCHAR(1),'F') AS ConsultOn
,CONVERT(NVARCHAR(128),COALESCE(fes.UnclassifiedServiceDescription,'')) AS ClaimNote
,CONVERT(NVARCHAR(1),CASE WHEN fes.IsZeroChargeAllowedOnClaim = 1 THEN 'T' ELSE 'F' END) AS ServiceFilter
,CONVERT(NVARCHAR(16),COALESCE(fes.NDC,'')) AS NDC
,CONVERT(INT,'') AS RevenueCodeId
,CONVERT(BIT,fes.IsCliaCertificateRequiredOnClaim) AS IsCliaCertificateRequiredOnClaim
FROM model.FacilityEncounterServices fes
LEFT JOIN model.EncounterServiceTypes est ON est.Id = fes.EncounterServiceTypeId

GO


