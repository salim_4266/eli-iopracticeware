﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PracticeServicesUpdate' AND parent_id = OBJECT_ID('dbo.PracticeServices','V'))
	DROP TRIGGER PracticeServicesUpdate
GO
	
CREATE TRIGGER PracticeServicesUpdate ON dbo.PracticeServices
INSTEAD OF UPDATE
AS
BEGIN
SET NOCOUNT ON;
DECLARE @CodeId INT
DECLARE @Code NVARCHAR(16)
DECLARE @CodeType NVARCHAR(1)
DECLARE @CodeCategory NVARCHAR(32)
DECLARE @Description NVARCHAR(64)
DECLARE @Fee REAL
DECLARE @TOS NVARCHAR(32)
DECLARE @POS NVARCHAR(32)
DECLARE @StartDate NVARCHAR(10)
DECLARE @EndDate NVARCHAR(10)
DECLARE @RelativeValueUnit REAL 
DECLARE @OrderDoc NVARCHAR(1)
DECLARE @ConsultOn NVARCHAR(1)
DECLARE @ClaimNote NVARCHAR(128)
DECLARE @ServiceFilter NVARCHAR(1)
DECLARE @NDC NVARCHAR(16)
DECLARE @RevenueCodeId INT
DECLARE @IsCliaCertificateRequiredOnClaim BIT

DECLARE PracticeServicesUpdateCursor CURSOR FOR
SELECT CodeId
,Code
,CodeType
,CodeCategory
,[Description]
,Fee
,TOS
,StartDate
,EndDate
,RelativeValueUnit
,OrderDoc
,ConsultOn
,ServiceFilter
,NDC
,IsCliaCertificateRequiredOnClaim
FROM inserted

OPEN PracticeServicesUpdateCursor FETCH NEXT FROM PracticeServicesUpdateCursor INTO
	@CodeId, @Code, @CodeType, @CodeCategory, @Description, @Fee, @TOS, @StartDate,
	@EndDate, @RelativeValueUnit, @OrderDoc, @ConsultOn, @ServiceFilter,
	@NDC, @IsCliaCertificateRequiredOnClaim
WHILE @@FETCH_STATUS = 0 
BEGIN
	IF ((SELECT Code FROM deleted WHERE CodeId = @CodeId) <> @Code)
	BEGIN
		UPDATE es
		SET es.Code = @Code
		FROM model.EncounterServices es
		WHERE es.Id = @CodeId
	END

	IF ((SELECT CodeCategory FROM deleted WHERE CodeId = @CodeId) <> @CodeCategory)
	BEGIN
		DECLARE @EncounterServiceTypeId INT
		SELECT @EncounterServiceTypeId = Id FROM model.EncounterServiceTypes WHERE Name = @CodeCategory

		UPDATE es
		SET es.EncounterServiceTypeId = @EncounterServiceTypeId
		FROM model.EncounterServices es
		WHERE es.Id = @CodeId
	END

	IF ((SELECT [Description] FROM deleted WHERE CodeId = @CodeId) <> @Description)
	BEGIN
		UPDATE es
		SET es.[Description] = @Description
		FROM model.EncounterServices es
		WHERE es.Id = @CodeId
	END

	IF ((SELECT Fee FROM deleted WHERE CodeId = @CodeId) <> @Fee)
	BEGIN
		UPDATE es
		SET es.[UnitFee] = CONVERT(DECIMAL(18,2),@Fee)
		FROM model.EncounterServices es
		WHERE es.Id = @CodeId
	END

	IF ((SELECT StartDate FROM deleted WHERE CodeId = @CodeId) <> @StartDate)
	BEGIN
		UPDATE es
		SET es.StartDateTime = CASE WHEN @StartDate <> '' AND @StartDate IS NOT NULL THEN CONVERT(DATETIME,@StartDate) END
		FROM model.EncounterServices es
		WHERE es.Id = @CodeId
	END

	IF ((SELECT EndDate FROM deleted WHERE CodeId = @CodeId) <> @EndDate)
	BEGIN
		UPDATE es
		SET es.EndDateTime = CASE WHEN @EndDate <> '' AND @EndDate IS NOT NULL THEN CONVERT(DATETIME,@EndDate) END
		FROM model.EncounterServices es
		WHERE es.Id = @CodeId
	END

	IF ((SELECT RelativeValueUnit FROM deleted WHERE CodeId = @CodeId) <> @RelativeValueUnit)
	BEGIN
		UPDATE es
		SET es.RelativeValueUnit = @RelativeValueUnit
		FROM model.EncounterServices es
		WHERE es.Id = @CodeId
	END
	
	IF ((SELECT OrderDoc FROM deleted WHERE CodeId = @CodeId) <> @OrderDoc)
	BEGIN
		UPDATE es
		SET es.IsOrderingProviderRequiredOnClaim = CASE @OrderDoc WHEN 'T' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END
		FROM model.EncounterServices es
		WHERE es.Id = @CodeId
	END

	IF ((SELECT ConsultOn FROM deleted WHERE CodeId = @CodeId) <> @ConsultOn)
	BEGIN
		UPDATE es
		SET es.IsReferringDoctorRequiredOnClaim = CASE @ConsultOn WHEN 'T' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END
		FROM model.EncounterServices es
		WHERE es.Id = @CodeId
	END

	IF ((SELECT ServiceFilter FROM deleted WHERE CodeId = @CodeId) <> @ServiceFilter)
	BEGIN
		UPDATE es
		SET es.IsZeroChargeAllowedOnClaim = CASE @ServiceFilter WHEN 'F' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END
		FROM model.EncounterServices es
		WHERE es.Id = @CodeId
	END

	IF ((SELECT NDC FROM deleted WHERE CodeId = @CodeId) <> @NDC)
	BEGIN
		IF EXISTS (SELECT Id FROM model.EncounterServiceDrugs WHERE Id = @CodeId)
		BEGIN
			UPDATE esd
			SET esd.NDC = @NDC
			FROM model.EncounterServiceDrugs esd
			WHERE esd.Id = @CodeId
		END
	END

	IF ((SELECT IsCliaCertificateRequiredOnClaim FROM deleted WHERE CodeId = @CodeId) <> @IsCliaCertificateRequiredOnClaim)
	BEGIN
		UPDATE es
		SET es.IsCliaCertificateRequiredOnClaim = @IsCliaCertificateRequiredOnClaim
		FROM model.EncounterServices es
		WHERE es.Id = @CodeId
	END

	FETCH NEXT FROM PracticeServicesUpdateCursor INTO 
	@CodeId, @Code, @CodeType, @CodeCategory, @Description, @Fee, @TOS, @StartDate,
	@EndDate, @RelativeValueUnit, @OrderDoc, @ConsultOn, @ServiceFilter,
	@NDC, @IsCliaCertificateRequiredOnClaim

	CLOSE PracticeServicesUpdateCursor
	DEALLOCATE PracticeServicesUpdateCursor
END
END
GO
