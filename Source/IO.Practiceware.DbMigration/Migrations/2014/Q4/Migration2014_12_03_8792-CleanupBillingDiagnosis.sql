﻿IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit

SELECT 1 AS Value INTO #NoAudit

SELECT bd.Id AS BillingDiagnosisId
INTO #DeleteBillingDiagnosisId
FROM model.BillingDiagnosis bd
INNER JOIN PatientClinical pc ON bd.LegacyPatientClinicalId = pc.ClinicalId
	WHERE Status = 'X'

DELETE FROM model.BillingServiceBillingDiagnosis
WHERE BillingDiagnosisId IN (SELECT BillingDiagnosisId FROM #DeleteBillingDiagnosisId)

DELETE FROM model.BillingDiagnosis
WHERE Id IN (SELECT BillingDiagnosisId FROM #DeleteBillingDiagnosisId)

SELECT MIN(Id) AS BillingServiceIdToKeep, InvoiceId, OrdinalId, ExternalSystemEntityMappingId
INTO #KeepBillingDiagnosis
FROM model.BillingDiagnosis
GROUP BY InvoiceId, OrdinalId, ExternalSystemEntityMappingId
HAVING COUNT(*) > 1

SELECT Id AS DeleteBillingDiagnosisId
INTO #DeleteBillingDiagnosis
FROM model.BillingDiagnosis db
INNER JOIN #KeepBillingDiagnosis kbd ON kbd.InvoiceId = db.InvoiceId 
	AND kbd.OrdinalId = db.OrdinalId 
	AND kbd.ExternalSystemEntityMappingId = db.ExternalSystemEntityMappingId
WHERE db.Id <> kbd.BillingServiceIdToKeep

DELETE FROM model.BillingServiceBillingDiagnosis WHERE BillingDiagnosisId IN (SELECT DeleteBillingDiagnosisId FROM #DeleteBillingDiagnosis)
DELETE FROM model.BillingDiagnosis WHERE Id IN (SELECT DeleteBillingDiagnosisId FROM #DeleteBillingDiagnosis)

IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit