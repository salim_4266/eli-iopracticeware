﻿--Fix for Bug #15835
--To fix the services that were incorrectly changed column 'IsZeroChargeAllowedOnClaim' to True
IF OBJECT_ID(N'dbo.PracticeServicesBackup') IS NOT NULL
BEGIN
	UPDATE es
	SET es.IsZeroChargeAllowedOnClaim = 0
	FROM model.EncounterServices es
	INNER JOIN dbo.PracticeServicesBackup psb ON psb.Code = es.Code
	WHERE es.IsZeroChargeAllowedOnClaim = 1
	AND psb.ServiceFilter <> 'T'
END
GO