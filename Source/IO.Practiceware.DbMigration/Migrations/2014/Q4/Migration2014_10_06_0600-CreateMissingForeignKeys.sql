﻿DELETE FROM model.CommunicationWithOtherProviderOrders
WHERE TemplateDocumentId IS NOT NULL 
	AND TemplateDocumentId NOT IN (SELECT Id FROM model.TemplateDocuments)

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_TemplateDocumentCommunicationWithOtherProviderOrder')  
ALTER TABLE [model].[CommunicationWithOtherProviderOrders] ADD CONSTRAINT [FK_TemplateDocumentCommunicationWithOtherProviderOrder] 
FOREIGN KEY ([TemplateDocumentId]) REFERENCES [model].[TemplateDocuments] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELETE FROM model.Adjustments
WHERE FinancialBatchId IN (SELECT Id FROM model.FinancialBatches WHERE PatientId IS NOT NULL 
							AND PatientId NOT IN (SELECT Id FROM model.Patients))
 
DELETE FROM model.FinancialInformations
WHERE FinancialBatchId IN (SELECT Id FROM model.FinancialBatches WHERE PatientId IS NOT NULL 
							AND PatientId NOT IN (SELECT Id FROM model.Patients))

DELETE FROM model.FinancialBatches
WHERE PatientId IS NOT NULL 
	AND PatientId NOT IN (SELECT Id FROM model.Patients)
		
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_PatientFinancialBatch')  
ALTER TABLE [model].[FinancialBatches] ADD CONSTRAINT [FK_PatientFinancialBatch] 
FOREIGN KEY ([PatientId]) REFERENCES [model].[Patients] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELETE FROM model.X12EligibilityPatients
WHERE PatientId IS NOT NULL 
	AND PatientId NOT IN (SELECT Id FROM model.Patients)
		
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_PatientX12EligibilityPatient')  
ALTER TABLE [model].[X12EligibilityPatients] ADD CONSTRAINT [FK_PatientX12EligibilityPatient] 
FOREIGN KEY ([PatientId]) REFERENCES [model].[Patients] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELETE FROM model.FinancialInformations
WHERE PatientId IS NOT NULL 
	AND PatientId NOT IN (SELECT Id FROM model.Patients)
		
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_PatientFinancialInformation')  
ALTER TABLE [model].[FinancialInformations] ADD CONSTRAINT [FK_PatientFinancialInformation] 
FOREIGN KEY ([PatientId]) REFERENCES [model].[Patients] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELETE FROM model.Adjustments
WHERE PatientId IS NOT NULL 
	AND PatientId NOT IN (SELECT Id FROM model.Patients)
		
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_PatientAdjustment')  
ALTER TABLE [model].[Adjustments] ADD CONSTRAINT [FK_PatientAdjustment] 
FOREIGN KEY ([PatientId]) REFERENCES [model].[Patients] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELETE FROM model.MedicationOrderFavorites
WHERE RouteOfAdministrationId IS NOT NULL 
	AND RouteOfAdministrationId NOT IN (SELECT Id FROM model.RouteOfAdministrations)
		
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_RouteOfAdministrationMedicationOrderFavorites')  
ALTER TABLE [model].[MedicationOrderFavorites] ADD CONSTRAINT [FK_RouteOfAdministrationMedicationOrderFavorites] 
FOREIGN KEY ([RouteOfAdministrationId]) REFERENCES [model].[RouteOfAdministrations] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELETE FROM model.BodyParts
WHERE OrganId IS NOT NULL 
	AND OrganId NOT IN (SELECT Id FROM model.Organs)
		
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_OrganBodyPart')  
ALTER TABLE [model].[BodyParts] ADD CONSTRAINT [FK_OrganBodyPart] 
FOREIGN KEY ([OrganId]) REFERENCES [model].[Organs] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELETE FROM model.Screens
WHERE ScreenTypeId IS NOT NULL 
	AND ScreenTypeId NOT IN (SELECT Id FROM model.ScreenTypes)
		
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_ScreenTypeScreen')  
ALTER TABLE [model].[Screens] ADD CONSTRAINT [FK_ScreenTypeScreen] 
FOREIGN KEY ([ScreenTypeId]) REFERENCES [model].[ScreenTypes] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELETE FROM model.OrganStructures
WHERE OrganId IS NOT NULL 
	AND OrganId NOT IN (SELECT Id FROM model.Organs)
			
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_OrganOrganStructure')  
ALTER TABLE [model].[OrganStructures] ADD CONSTRAINT [FK_OrganOrganStructure] 
FOREIGN KEY ([OrganId]) REFERENCES [model].[Organs] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELETE FROM model.TaskActivityUsers
WHERE TaskActivityId IN (SELECT Id FROM model.TaskActivities
						WHERE TaskId IN (SELECT Id FROM model.Tasks
							WHERE PatientId IS NOT NULL 
									AND PatientId NOT IN (SELECT Id FROM model.Patients)))

DELETE FROM model.TaskActivities
WHERE TaskId IN (SELECT Id FROM model.Tasks
				WHERE PatientId IS NOT NULL 
						AND PatientId NOT IN (SELECT Id FROM model.Patients))

DELETE FROM model.Tasks
WHERE PatientId IS NOT NULL 
	AND PatientId NOT IN (SELECT Id FROM model.Patients)
		
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_PatientTask')  
ALTER TABLE [model].[Tasks] ADD CONSTRAINT [FK_PatientTask] 
FOREIGN KEY ([PatientId]) REFERENCES [model].[Patients] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
		
