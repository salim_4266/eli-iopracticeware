﻿IF NOT EXISTS(SELECT * 
FROM model.ApplicationSettings 
WHERE Name = 'FileCompressionLevel') 
INSERT INTO model.ApplicationSettings(Name, Value, ApplicationSettingTypeId)
VALUES ('FileCompressionLevel', '90', 11)