﻿-- Add column PaymentMethodId to model.PatientFinancialComments
IF NOT EXISTS (
		SELECT *
		FROM sys.columns
		WHERE NAME = N'IsArchived'
			AND Object_ID = Object_ID(N'model.PatientFinancialComments')
		)
	ALTER TABLE model.PatientFinancialComments ADD IsArchived BIT NOT NULL DEFAULT(0)
GO


-- Add column PaymentMethodId to model.InvoiceComments
IF NOT EXISTS (
		SELECT *
		FROM sys.columns
		WHERE NAME = N'IsArchived'
			AND Object_ID = Object_ID(N'model.InvoiceComments')
		)
	ALTER TABLE model.InvoiceComments ADD IsArchived BIT NOT NULL DEFAULT(0)
GO


