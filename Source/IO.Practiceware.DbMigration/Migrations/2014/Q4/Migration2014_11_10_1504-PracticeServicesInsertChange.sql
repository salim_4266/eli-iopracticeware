﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PracticeServicesInsert' AND parent_id = OBJECT_ID('dbo.PracticeServices','V'))
	DROP TRIGGER PracticeServicesInsert
GO
	
CREATE TRIGGER PracticeServicesInsert ON dbo.PracticeServices
INSTEAD OF INSERT
AS
BEGIN
SET NOCOUNT ON;
DECLARE @Code NVARCHAR(16)
DECLARE @CodeType NVARCHAR(1)
DECLARE @CodeCategory NVARCHAR(32)
DECLARE @Description NVARCHAR(64)
DECLARE @Fee REAL
DECLARE @TOS NVARCHAR(32)
DECLARE @POS NVARCHAR(32)
DECLARE @StartDate NVARCHAR(10)
DECLARE @EndDate NVARCHAR(10)
DECLARE @RelativeValueUnit REAL 
DECLARE @OrderDoc NVARCHAR(1)
DECLARE @ConsultOn NVARCHAR(1)
DECLARE @ClaimNote NVARCHAR(128)
DECLARE @ServiceFilter NVARCHAR(1)
DECLARE @NDC NVARCHAR(16)
DECLARE @RevenueCodeId INT
DECLARE @IsCliaCertificateRequiredOnClaim BIT

DECLARE PracticeServicesInsertCursor CURSOR FOR
SELECT 
Code
,CodeType
,CodeCategory
,[Description]
,Fee
,TOS
,POS
,StartDate
,EndDate
,RelativeValueUnit
,OrderDoc
,ConsultOn
,ServiceFilter
,NDC
,IsCliaCertificateRequiredOnClaim
,ClaimNote
FROM inserted

OPEN PracticeServicesInsertCursor FETCH NEXT FROM PracticeServicesInsertCursor INTO
	@Code, @CodeType, @CodeCategory, @Description, @Fee, @TOS, @POS, @StartDate,
	@EndDate, @RelativeValueUnit, @OrderDoc, @ConsultOn, @ServiceFilter,
	@NDC, @IsCliaCertificateRequiredOnClaim, @ClaimNote
WHILE @@FETCH_STATUS = 0 
BEGIN
	IF NOT EXISTS (SELECT * FROM model.EncounterServices WHERE Code = @Code)
	BEGIN
		DECLARE @EncounterServiceTypeId INT
		SELECT @EncounterServiceTypeId = Id FROM model.EncounterServiceTypes WHERE Name = @CodeCategory

		DECLARE @EncounterServices TABLE (Id INT)

		INSERT INTO model.EncounterServices (
			Code
			,[Description]
			,DefaultUnit
			,UnitFee
			,StartDateTime
			,EndDateTime
			,IsOrderingProviderRequiredOnClaim
			,IsReferringDoctorRequiredOnClaim
			,IsZeroChargeAllowedOnClaim
			,RelativeValueUnit
			,IsArchived
			,EncounterServiceTypeId
			,ServiceUnitOfMeasurementId
			,IsCliaCertificateRequiredOnClaim
			,LegacyPracticeServicesCodeId
			,NDC
			,DrugQuantity
			,DrugUnitOfMeasurementId
			,UnclassifiedServiceDescription
		)
		OUTPUT inserted.Id INTO @EncounterServices (Id)
		SELECT
		@Code AS Code
		,CASE WHEN @Description <> '' AND @Description IS NOT NULL THEN @Description END AS [Description]
		,CONVERT(DECIMAL(18,2), 1) AS DefaultUnit
		,CONVERT(DECIMAL(12,2), @Fee) AS UnitFee
		,CASE WHEN @StartDate <> '' AND @StartDate IS NOT NULL THEN CONVERT(DATETIME,@StartDate) END AS StartDateTime
		,CASE WHEN @EndDate <> '' AND @EndDate IS NOT NULL THEN CONVERT(DATETIME,@EndDate) END AS EndDateTime
		,CASE @OrderDoc WHEN 'T' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsOrderingProviderRequiredOnClaim
		,CASE @ConsultOn WHEN 'T' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsReferringDoctorRequiredOnClaim
		,CASE @ServiceFilter WHEN 'F' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS IsZeroChargeAllowedOnClaim
		,CONVERT(DECIMAL(18,2), @RelativeValueUnit) AS RelativeValueUnit
		,CONVERT(BIT, 0) AS IsArchived
		,@EncounterServiceTypeId AS EncounterServiceTypeId
		,CASE WHEN @CodeCategory = 'ANESTHESIA' THEN 1 ELSE 2 END AS ServiceUnitOfMeasurementId
		,COALESCE(@IsCliaCertificateRequiredOnClaim, 0) AS IsCliaCertificateRequiredOnClaim
		,NULL AS LegacyPracticeServicesCodeId
		,@NDC AS NDC
		,CONVERT(DECIMAL(18,2), 1) AS DrugQuantity
		,5 AS DrugUnitOfMeasurementId
		,@ClaimNote as UnclassifiedServiceDescription
	END
	ELSE PRINT 'The inserted Code, already exists in model.EncounterServices'

	IF OBJECT_ID('dbo.ViewIdentities','U') IS NULL
	BEGIN	
		CREATE TABLE [dbo].[ViewIdentities](
		[Name] [nvarchar](max) NULL,
		[Value] [bigint] NULL
		)
	END

	IF OBJECT_ID('tempdb..#TempTableToHoldTheScopeIdentity') IS NOT NULL
		DROP TABLE #TempTableToHoldTheScopeIdentity

	CREATE TABLE #TempTableToHoldTheScopeIdentity (Id BIGINT IDENTITY(1,1) NOT NULL)
	SET IDENTITY_INSERT #TempTableToHoldTheScopeIdentity ON
	INSERT INTO #TempTableToHoldTheScopeIdentity (Id) SELECT Id AS SCOPE_ID_COLUMN FROM @EncounterServices
	SET IDENTITY_INSERT #TempTableToHoldTheScopeIdentity OFF

	IF NOT EXISTS(SELECT * FROM ViewIdentities WHERE Name = 'dbo.PracticeServices')
		INSERT ViewIdentities(Name, Value) VALUES ('dbo.PracticeServices', @@IDENTITY)
	ELSE
		UPDATE ViewIdentities SET Value = @@IDENTITY WHERE Name = 'dbo.PracticeServices'

	SELECT @@IDENTITY AS SCOPE_ID_COLUMN

	FETCH NEXT FROM PracticeServicesInsertCursor INTO 
	@Code, @CodeType, @CodeCategory, @Description, @Fee, @POS, @TOS,  @StartDate,
	@EndDate, @RelativeValueUnit, @OrderDoc, @ConsultOn, @ServiceFilter,
	@NDC, @IsCliaCertificateRequiredOnClaim, @ClaimNote

END
CLOSE PracticeServicesInsertCursor
DEALLOCATE PracticeServicesInsertCursor
END
