﻿--Bug 15620
--Updating dbo.PracticeCodeTable so that all FinancialInformationTypes are excluded from patient statement except deductibles and coinsurances
UPDATE pct
SET pct.Exclusion = 'T'
FROM dbo.PracticeCodeTable pct
WHERE ReferenceType = 'PAYMENTTYPE'
	AND (AlternateCode = '' OR AlternateCode IS NULL)
	AND RIGHT(Code, 1) NOT IN ('!','|')