﻿IF OBJECT_ID('dbo.PatientReceivablePaymentsBackup','U') IS NOT NULL
	AND OBJECT_ID('dbo.PatientReceivablePaymentsFinancialBatchCleanup','U') IS NULL
	AND OBJECT_ID('dbo.PatientReceivablesBackup','U') IS NOT NULL
BEGIN
	EXEC('
	DECLARE @CurrentPatientReceivablePaymentsBackupIdentity INT
	SET @CurrentPatientReceivablePaymentsBackupIdentity = IDENT_CURRENT(''dbo.PatientReceivablePaymentsBackup'')

	IF (@CurrentPatientReceivablePaymentsBackupIdentity IS NOT NULL) 
		AND ((SELECT COUNT(*) FROM dbo.PatientReceivablePaymentsBackup ) > 0)
	BEGIN
		
		DECLARE @LastPatientReceivablePaymentsIdentity INT

		SELECT TOP 1 
		@LastPatientReceivablePaymentsIdentity = PaymentId
		FROM dbo.PatientReceivablePaymentsBackup p
		WHERE p.PaymentId < @CurrentPatientReceivablePaymentsBackupIdentity 
			AND PaymentType <> ''=''
		ORDER BY p.PaymentId DESC
		
		SELECT DISTINCT 
		prpBackup.PaymentAmount
		,prpBackup.PaymentCheck
		,prpBackup.PaymentBatchCheckId
		,fbBackup.Amount AS PrpFinancialBatchAmount
		,fbBackup.PatientId AS PrpFinancialBatchfbbPatientId
		,adj.Amount
		,adj.FinancialBatchId
		,adj.PatientId AS CurrentAdjustmentPatientId
		,fb.PatientId AS CurrentFinancialBatchPatientId
		,fb.Amount AS CurrentFinancialBatchAmount
		,fbBackup.CheckCode AS CurrentFinancialBatchCheckCode
		,e.PatientId AS CurrentEncounterPatientId
		,adj.Id AS AdjustmentsId
		,e.Id AS EncountersId
		,ir.Id AS InvoiceReceivablesId
		INTO dbo.PatientReceivablePaymentsFinancialBatchCleanup
		FROM dbo.PatientReceivablePaymentsBackup prpBackup
		JOIN dbo.AdjustmentsBackup adjBackup ON adjBackup.FinancialBatchId = prpBackup.PaymentId
		JOIN dbo.FinancialBatchesBackUp fbBackup ON fbBackup.Id = adjBackup.FinancialBatchId
		JOIN dbo.PatientReceivablePaymentsBackup fbpayment ON fbpayment.PaymentBatchCheckId = prpBackup.PaymentId
		JOIN dbo.PatientReceivablesBackup prBackup ON prBackup.ReceivableId = fbpayment.ReceivableId
		JOIN model.Invoices i ON i.LegacyPatientReceivablesInvoice = prBackup.Invoice
		JOIN model.Encounters e ON e.Id = i.EncounterId
		JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
		JOIN model.Adjustments adj ON adj.InvoiceReceivableId = ir.id
		JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
		WHERE (prpBackup.PaymentId > @LastPatientReceivablePaymentsIdentity 
				AND prpBackup.PaymentId  < @CurrentPatientReceivablePaymentsBackupIdentity)
			AND fbBackup.PatientId <> adjBackup.PatientId 
			AND e.PatientId <> fb.PatientId

		UPDATE f
		SET f.PatientId = p.CurrentEncounterPatientId
		FROM model.FinancialBatches f
		JOIN dbo.PatientReceivablePaymentsFinancialBatchCleanup p ON p.FinancialBatchId = f.Id
	END
		')
END
GO