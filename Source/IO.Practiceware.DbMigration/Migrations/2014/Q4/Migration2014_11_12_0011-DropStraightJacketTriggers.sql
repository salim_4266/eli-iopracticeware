﻿DECLARE @sql NVARCHAR(MAX)
SET @sql = ''
SELECT @sql = @sql +
'DROP TRIGGER ' + QUOTENAME(OBJECT_SCHEMA_NAME(object_id)) + '.' + QUOTENAME(OBJECT_NAME(object_id)) + '; 
'
FROM sys.triggers 
WHERE name IN ('OnPatientInsurancesInsert_StraightJacket','OnPatientInsurancesUpdate_StraightJacket')

EXEC sys.sp_executesql @sql