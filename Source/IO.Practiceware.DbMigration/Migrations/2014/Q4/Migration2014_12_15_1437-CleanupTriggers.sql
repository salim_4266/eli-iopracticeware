﻿--Found these triggers to cause problems for practice.

IF EXISTS(SELECT * FROM sys.triggers WHERE name = 'OnPatientClinicalInserted')
BEGIN
	DROP TRIGGER OnPatientClinicalInserted
END

IF EXISTS(SELECT * FROM sys.triggers WHERE name = 'OnPatientClinicalDeleted')
BEGIN
	DROP TRIGGER OnPatientClinicalDeleted
END

--Drop Audit Trigger's
IF OBJECT_ID(N'[model].[AuditDocumentsDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditDocumentsDeleteTrigger];
END
GO
IF OBJECT_ID(N'[model].[AuditDocumentsInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditDocumentsInsertTrigger];
END
GO
IF OBJECT_ID(N'[model].[AuditDocumentsUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditDocumentsUpdateTrigger];
END
GO
IF OBJECT_ID(N'[model].[AuditDocumentPrinterDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditDocumentPrinterDeleteTrigger];
END
GO
IF OBJECT_ID(N'[model].[AuditDocumentPrinterInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditDocumentPrinterInsertTrigger];
END
GO
IF OBJECT_ID(N'[model].[AuditDocumentPrinterUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditDocumentPrinterUpdateTrigger];
END
GO