﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE name = 'FinancialInformationTypes' AND schema_id = schema_id('Model'))
BEGIN
EXEC('CREATE VIEW [model].[FinancialInformationTypes]
	
	AS
	SELECT v.Id AS Id,
		v.FinancialTypeGroupId AS FinancialTypeGroupId,
		CONVERT(bit, 1) AS IsPrintOnStatement,
		SUBSTRING(v.Code, 1, (dbo.GetMax((CHARINDEX(''-'', v.code) - 2),0))) AS [Name],
		v.ShortName AS ShortName,
		v.IsArchived
	FROM (SELECT CASE
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''!''
				THEN 1
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''|''
				THEN 2
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''?''
				THEN 3
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''%''
				THEN 4
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''D''
				THEN 5
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''&''
				THEN 6
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = '']''
				THEN 7
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''0''
				THEN 8
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''V''
				THEN 9
			ELSE pct.Id + 1000
			END AS Id,
			NULL AS FinancialTypeGroupId,
			LetterTranslation AS ShortName,
			Code AS Code,
			COUNT_BIG(*) AS Count,
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pct
		WHERE pct.ReferenceType = ''PAYMENTTYPE''		
			AND (pct.AlternateCode = '''' OR pct.AlternateCode IS NULL)
		GROUP BY pct.Id,
			LetterTranslation,
			Code
		) AS v')
END

--- Delete the adjustments where adjustmenttypeid exists but no correspondent entry exist in model.AdjustmentTypes view.
IF Exists
  (SELECT id
   FROM model.Adjustments
   WHERE AdjustmentTypeId NOT IN
       (SELECT id
        FROM model.AdjustmentTypes)) 
BEGIN

SELECT * 
INTO DeletedAdjustments81
FROM model.Adjustments
WHERE AdjustmentTypeId NOT IN
    (SELECT id
     FROM model.AdjustmentTypes)

DELETE model.Adjustments
WHERE AdjustmentTypeId NOT IN
    (SELECT id
     FROM model.AdjustmentTypes) 

END

UPDATE model.FinancialInformations SET FinancialInformationTypeId = 7
WHERE Id  IN (SELECT Id FROM model.FinancialInformations WHERE FinancialInformationTypeId
NOT IN( SELECT Id FROM model.FinancialInformationTypes))