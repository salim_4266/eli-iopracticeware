﻿IF NOT EXISTS(SELECT * FROM sys.objects WHERE name = 'BillingServiceComments')
BEGIN 
	EXEC( 'CREATE VIEW [model].[BillingServiceComments]
	
	AS
	SELECT 
	(CONVERT(bigint, 110000000) * 3 + NoteId) AS Id
	,MIN(bs.Id) AS BillingServiceId
	,note1 + note2 + note3 + note4 AS Value
	,CONVERT(bit, 0) AS IsArchived
	,CASE 
		WHEN NoteClaimOn = ''T''
		THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
	END AS IsIncludedOnClaim
	,CASE 
		WHEN NoteCommentOn = ''T''
		THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
	END AS IsIncludedOnStatement 
	FROM dbo.PatientNotes pn
	JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = pn.AppointmentId 
	JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
	JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
	JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
		AND bsm.OrdinalId = 1
	JOIN model.ServiceModifiers sm ON sm.Id = bsm.ServiceModifierId
	WHERE NoteType = ''B''
	AND NoteSystem LIKE ''R%C%''
	AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX(''C'', NoteSystem) + 1),0)), LEN(NoteSystem)) = es.Code
	AND NoteEye <> '''' AND CASE NoteEye
		WHEN ''OS''
			THEN ''LT''
		WHEN ''OD''
			THEN ''RT''
		WHEN ''OU''
			THEN ''50''
		ELSE NoteEye
		END = sm.Code
	GROUP BY NoteId,
	NoteEye,
	Note1,
	Note2,
	Note3,
	Note4,
	NoteCommentOn,
	NoteClaimOn
	
	UNION ALL
	
	--BillingService comment partition 2
	SELECT 
	(CONVERT(bigint, 110000000) * 17 + NoteId) AS Id
	,MIN(bs.Id) AS BillingServiceId
	,note1 + note2 + note3 + note4 AS Value
	,CONVERT(bit, 0) AS IsArchived
	,CASE 
		WHEN NoteClaimOn = ''T''
		THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
	END AS IsIncludedOnClaim
	,CASE 
		WHEN NoteCommentOn = ''T''
		THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
	END AS IsIncludedOnStatement 
	FROM dbo.PatientNotes pn
	JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = pn.AppointmentId 
	JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
	JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
	JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
		AND bsm.OrdinalId = 2
	JOIN model.ServiceModifiers sm ON sm.Id = bsm.ServiceModifierId
	WHERE NoteType = ''B''
	AND NoteSystem LIKE ''R%C%''
	AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX(''C'', NoteSystem) + 1),0)), LEN(NoteSystem)) = es.Code
	AND NoteEye <> '''' AND CASE NoteEye
		WHEN ''OS''
			THEN ''LT''
		WHEN ''OD''
			THEN ''RT''
		WHEN ''OU''
			THEN ''50''
		ELSE NoteEye
		END = sm.Code
	GROUP BY NoteId,
	NoteEye,
	Note1,
	Note2,
	Note3,
	Note4,
	NoteCommentOn,
	NoteClaimOn
	
	UNION ALL
	
	--BillingService comment partition 3
	SELECT 
	(CONVERT(bigint, 110000000) * 18 + NoteId) AS Id
	,MIN(bs.Id) AS BillingServiceId
	,note1 + note2 + note3 + note4 AS Value
	,CONVERT(bit, 0) AS IsArchived
	,CASE 
		WHEN NoteClaimOn = ''T''
		THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
	END AS IsIncludedOnClaim
	,CASE 
		WHEN NoteCommentOn = ''T''
		THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
	END AS IsIncludedOnStatement 
	FROM dbo.PatientNotes pn
	JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = pn.AppointmentId 
	JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
	JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
	JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
		AND bsm.OrdinalId = 3
	JOIN model.ServiceModifiers sm ON sm.Id = bsm.ServiceModifierId
	WHERE NoteType = ''B''
	AND NoteSystem LIKE ''R%C%''
	AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX(''C'', NoteSystem) + 1),0)), LEN(NoteSystem)) = es.Code
	AND NoteEye <> '''' AND CASE NoteEye
		WHEN ''OS''
			THEN ''LT''
		WHEN ''OD''
			THEN ''RT''
		WHEN ''OU''
			THEN ''50''
		ELSE NoteEye
		END = sm.Code
	GROUP BY NoteId,
	NoteEye,
	Note1,
	Note2,
	Note3,
	Note4,
	NoteCommentOn,
	NoteClaimOn
	
	UNION ALL
	
	--BillingService comment partition 4
	SELECT 
	(CONVERT(bigint, 110000000) * 19 + NoteId) AS Id
	,MIN(bs.Id) AS BillingServiceId
	,note1 + note2 + note3 + note4 AS Value
	,CONVERT(bit, 0) AS IsArchived
	,CASE 
		WHEN NoteClaimOn = ''T''
		THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
	END AS IsIncludedOnClaim
	,CASE 
		WHEN NoteCommentOn = ''T''
		THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
	END AS IsIncludedOnStatement 
	FROM dbo.PatientNotes pn
	JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = pn.AppointmentId 
	JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
	JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
	JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
		AND bsm.OrdinalId = 4
	JOIN model.ServiceModifiers sm ON sm.Id = bsm.ServiceModifierId
	WHERE NoteType = ''B''
	AND NoteSystem LIKE ''R%C%''
	AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX(''C'', NoteSystem) + 1),0)), LEN(NoteSystem)) = es.Code
	AND NoteEye <> '''' AND CASE NoteEye
		WHEN ''OS''
			THEN ''LT''
		WHEN ''OD''
			THEN ''RT''
		WHEN ''OU''
			THEN ''50''
		ELSE NoteEye
		END = sm.Code
	GROUP BY NoteId,
	NoteEye,
	Note1,
	Note2,
	Note3,
	Note4,
	NoteCommentOn,
	NoteClaimOn
	
	UNION ALL
	
	SELECT 
	(CONVERT(bigint, 110000000) * 20 + NoteId) AS Id
	,MIN(bs.Id) AS BillingServiceId
	,note1 + note2 + note3 + note4 AS Value
	,CONVERT(bit, 0) AS IsArchived
	,CASE 
		WHEN NoteClaimOn = ''T''
		THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
	END AS IsIncludedOnClaim
	,CASE 
		WHEN NoteCommentOn = ''T''
		THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
	END AS IsIncludedOnStatement 
	FROM dbo.PatientNotes pn
	JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = pn.AppointmentId 
	JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
	JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
	LEFT JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
	LEFT JOIN model.ServiceModifiers sm ON sm.Id = bsm.ServiceModifierId
	WHERE NoteType = ''B''
	AND NoteSystem LIKE ''R%C%''
	AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX(''C'', NoteSystem) + 1),0)), LEN(NoteSystem)) = es.Code
	AND NoteEye = ''''
	GROUP BY NoteId,
	NoteEye,
	Note1,
	Note2,
	Note3,
	Note4,
	NoteCommentOn,
	NoteClaimOn')
END

GO

ALTER TABLE model.BillingServices DISABLE TRIGGER ALL
SELECT * INTO #billingServiceComments FROM model.BillingServiceComments

IF OBJECT_ID(N'[model].[BillingServices]', 'U') IS NOT NULL	
BEGIN
	IF NOT EXISTS(select * from sys.columns where Name = N'UnclassifiedServiceDescription' and Object_ID = Object_ID(N'model.BillingServices')) 
	BEGIN
		ALTER TABLE model.BillingServices ADD UnclassifiedServiceDescription nvarchar(MAX) NULL		
	END
	
	BEGIN

		SELECT DISTINCT ROW_NUMBER() OVER (PARTITION BY bs.Id ORDER BY bs.Id, bsc.Id) AS [Rank],
			bs.Id, bsc.Value
		INTO #Temp
		FROM  model.BillingServices bs
		INNER JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
		INNER JOIN #billingServiceComments bsc ON bsc.BillingServiceId = bs.Id
		WHERE es.IsUnclassified = 1 AND bsc.IsIncludedOnClaim = 1
		ORDER BY bs.Id
		
		UPDATE bs SET [UnclassifiedServiceDescription] = Value
		FROM model.BillingServices bs
		INNER JOIN #Temp t ON t.Id = bs.Id
	--	WHERE t.Rank = 1
	END	
END
GO 

-- First we need to make a table of the Unclassified Service
-- Descriptions so we can exclude them from the new table

SELECT 	bs.Id,bsc.Value,bsc.IsArchived,bsc.IsIncludedOnClaim,bsc.IsIncludedOnStatement
INTO #UnclassifiedServiceDescriptions
FROM model.EncounterServices es
INNER JOIN model.BillingServices bs ON bs.EncounterServiceId = es.Id
AND es.IsUnclassified = 1
INNER JOIN #billingServiceComments bsc ON bsc.BillingServiceId = bs.Id
AND bsc.IsIncludedOnClaim = 1
ORDER BY bs.Id

IF OBJECT_ID('model.BillingServiceComments') IS NOT NULL
	EXEC dbo.DropObject 'model.BillingServiceComments'
GO

-- 'BillingServiceComments' does not exist, creating...
CREATE TABLE [model].[BillingServiceComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
	[BillingServiceId] INT not null,
	[Value] NVARCHAR(1020),
	[IsArchived] bit not null,
	[IsIncludedOnClaim] bit not null,
	[IsIncludedOnStatement] bit not null
);

-- Creating primary key on [Id] in table 'BillingServiceComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.BillingServiceComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[BillingServiceComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.BillingServiceComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[BillingServiceComments]
ADD CONSTRAINT [PK_BillingServiceComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [BillingServiceId] in table 'BillingServiceComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BillingServiceIdBillingServiceComment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingServiceComments] DROP CONSTRAINT FK_BillingServiceIdBillingServiceComment

IF OBJECTPROPERTY(OBJECT_ID('[model].[BillingServices]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingServiceComments]
ADD CONSTRAINT [FK_BillingServiceIdBillingServiceComment]
    FOREIGN KEY ([BillingServiceId])
    REFERENCES [model].[BillingServices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BillingServiceIdBillingServiceComment'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_BillingServiceIdBillingServiceComment')
	DROP INDEX IX_FK_BillingServiceIdBillingServiceComment ON [model].[BillingServiceComments]
GO
CREATE INDEX [IX_FK_BillingServiceIdBillingServiceComment]
ON [model].[BillingServiceComments]
    ([BillingServiceId]);
GO

INSERT INTO model.BillingServiceComments (BillingServiceId,Value,IsArchived,IsIncludedOnClaim,IsIncludedOnStatement)

SELECT 
MIN(bs.Id) AS BillingServiceId
,note1 + note2 + note3 + note4 AS Value
,CONVERT(bit, 0) AS IsArchived
,CASE 
	WHEN NoteClaimOn = 'T'
	THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END AS IsIncludedOnClaim
,CASE 
	WHEN NoteCommentOn = 'T'
	THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END AS IsIncludedOnStatement 
FROM dbo.PatientNotes pn
JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = pn.AppointmentId 
JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
	AND bsm.OrdinalId = 1
JOIN model.ServiceModifiers sm ON sm.Id = bsm.ServiceModifierId
WHERE NoteType = 'B'
AND NoteSystem LIKE 'R%C%'
AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX('C', NoteSystem) + 1),0)), LEN(NoteSystem)) = es.Code
AND NoteEye <> '' AND CASE NoteEye
	WHEN 'OS'
		THEN 'LT'
	WHEN 'OD'
		THEN 'RT'
	WHEN 'OU'
		THEN '50'
	ELSE NoteEye
	END = sm.Code
GROUP BY NoteId,
NoteEye,
Note1,
Note2,
Note3,
Note4,
NoteCommentOn,
NoteClaimOn

UNION ALL

--BillingService comment partition 2
SELECT 
MIN(bs.Id) AS BillingServiceId
,note1 + note2 + note3 + note4 AS Value
,CONVERT(bit, 0) AS IsArchived
,CASE 
	WHEN NoteClaimOn = 'T'
	THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END AS IsIncludedOnClaim
,CASE 
	WHEN NoteCommentOn = 'T'
	THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END AS IsIncludedOnStatement 
FROM dbo.PatientNotes pn
JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = pn.AppointmentId 
JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
	AND bsm.OrdinalId = 2
JOIN model.ServiceModifiers sm ON sm.Id = bsm.ServiceModifierId
WHERE NoteType = 'B'
AND NoteSystem LIKE 'R%C%'
AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX('C', NoteSystem) + 1),0)), LEN(NoteSystem)) = es.Code
AND NoteEye <> '' AND CASE NoteEye
	WHEN 'OS'
		THEN 'LT'
	WHEN 'OD'
		THEN 'RT'
	WHEN 'OU'
		THEN '50'
	ELSE NoteEye
	END = sm.Code
GROUP BY NoteId,
NoteEye,
Note1,
Note2,
Note3,
Note4,
NoteCommentOn,
NoteClaimOn

UNION ALL

--BillingService comment partition 3
SELECT 
MIN(bs.Id) AS BillingServiceId
,note1 + note2 + note3 + note4 AS Value
,CONVERT(bit, 0) AS IsArchived
,CASE 
	WHEN NoteClaimOn = 'T'
	THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END AS IsIncludedOnClaim
,CASE 
	WHEN NoteCommentOn = 'T'
	THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END AS IsIncludedOnStatement 
FROM dbo.PatientNotes pn
JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = pn.AppointmentId 
JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
	AND bsm.OrdinalId = 3
JOIN model.ServiceModifiers sm ON sm.Id = bsm.ServiceModifierId
WHERE NoteType = 'B'
AND NoteSystem LIKE 'R%C%'
AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX('C', NoteSystem) + 1),0)), LEN(NoteSystem)) = es.Code
AND NoteEye <> '' AND CASE NoteEye
	WHEN 'OS'
		THEN 'LT'
	WHEN 'OD'
		THEN 'RT'
	WHEN 'OU'
		THEN '50'
	ELSE NoteEye
	END = sm.Code
GROUP BY NoteId,
NoteEye,
Note1,
Note2,
Note3,
Note4,
NoteCommentOn,
NoteClaimOn

UNION ALL

--BillingService comment partition 4
SELECT 
MIN(bs.Id) AS BillingServiceId
,note1 + note2 + note3 + note4 AS Value
,CONVERT(bit, 0) AS IsArchived
,CASE 
	WHEN NoteClaimOn = 'T'
	THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END AS IsIncludedOnClaim
,CASE 
	WHEN NoteCommentOn = 'T'
	THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END AS IsIncludedOnStatement 
FROM dbo.PatientNotes pn
JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = pn.AppointmentId 
JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
	AND bsm.OrdinalId = 4
JOIN model.ServiceModifiers sm ON sm.Id = bsm.ServiceModifierId
WHERE NoteType = 'B'
AND NoteSystem LIKE 'R%C%'
AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX('C', NoteSystem) + 1),0)), LEN(NoteSystem)) = es.Code
AND NoteEye <> '' AND CASE NoteEye
	WHEN 'OS'
		THEN 'LT'
	WHEN 'OD'
		THEN 'RT'
	WHEN 'OU'
		THEN '50'
	ELSE NoteEye
	END = sm.Code
GROUP BY NoteId,
NoteEye,
Note1,
Note2,
Note3,
Note4,
NoteCommentOn,
NoteClaimOn

UNION ALL

SELECT 
MIN(bs.Id) AS BillingServiceId
,note1 + note2 + note3 + note4 AS Value
,CONVERT(bit, 0) AS IsArchived
,CASE 
	WHEN NoteClaimOn = 'T'
	THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END AS IsIncludedOnClaim
,CASE 
	WHEN NoteCommentOn = 'T'
	THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
END AS IsIncludedOnStatement 
FROM dbo.PatientNotes pn
JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = pn.AppointmentId 
JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
LEFT JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
LEFT JOIN model.ServiceModifiers sm ON sm.Id = bsm.ServiceModifierId
WHERE NoteType = 'B'
AND NoteSystem LIKE 'R%C%'
AND SUBSTRING(NoteSystem, (dbo.GetMax ((CHARINDEX('C', NoteSystem) + 1),0)), LEN(NoteSystem)) = es.Code
AND NoteEye = ''
GROUP BY NoteId,
NoteEye,
Note1,
Note2,
Note3,
Note4,
NoteCommentOn,
NoteClaimOn

EXCEPT 

SELECT 	Id,Value,IsArchived,IsIncludedOnClaim,IsIncludedOnStatement
FROM #UnclassifiedServiceDescriptions

DROP TABLE #UnclassifiedServiceDescriptions
DROP TABLE #billingServiceComments
DROP TABLE #Temp
ALTER TABLE model.BillingServices ENABLE TRIGGER ALL