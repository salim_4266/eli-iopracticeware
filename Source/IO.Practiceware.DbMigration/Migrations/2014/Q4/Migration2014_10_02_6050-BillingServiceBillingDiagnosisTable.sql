﻿EXEC dbo.DropObject 'model.BillingServiceBillingDiagnosis'
GO

-- 'BillingServiceBillingDiagnosis' does not exist, creating...
CREATE TABLE [model].[BillingServiceBillingDiagnosis] (
	[BillingServiceId] int  NOT NULL,
	[BillingDiagnosisId] int  NOT NULL,
	[OrdinalId] int  NOT NULL
);

-- Creating primary key on [BillingServiceId], [BillingDiagnosisId] in table 'BillingServiceBillingDiagnosis'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.BillingServiceBillingDiagnosis', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[BillingServiceBillingDiagnosis] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.BillingServiceBillingDiagnosis', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[BillingServiceBillingDiagnosis]
ADD CONSTRAINT [PK_BillingServiceBillingDiagnosis]
    PRIMARY KEY CLUSTERED ([BillingServiceId], [BillingDiagnosisId] ASC);
GO

INSERT INTO model.BillingServiceBillingDiagnosis ([BillingServiceId], [BillingDiagnosisId],	[OrdinalId])
SELECT BillingServiceId, BillingDiagnosisId, 
CASE
	WHEN Symptom = '1' AND LinkedDiag = '' THEN 1
	WHEN Symptom = '2' AND LinkedDiag = ''  THEN 2
	WHEN Symptom = '3' AND LinkedDiag = ''  THEN 3
	WHEN Symptom = '4' AND LinkedDiag = ''  THEN 4
	WHEN SUBSTRING(LinkedDiag, 1, 1) = Symptom THEN 1
	WHEN SUBSTRING(LinkedDiag, 2, 1) <> SUBSTRING(LinkedDiag, 1, 1) AND SUBSTRING(LinkedDiag, 2, 1) = Symptom THEN 2
	WHEN SUBSTRING(LinkedDiag, 3, 1) = Symptom THEN 3
	WHEN SUBSTRING(LinkedDiag, 4, 1) = Symptom THEN 4
END
AS OrdinalId FROM (SELECT 
pc.ClinicalId AS BillingDiagnosisId,
bs.Id AS BillingServiceId, 
Symptom AS Symptom, 
bs.LegacyPatientReceivableServicesLinkedDiag AS LinkedDiag, 
COUNT_BIG(*) AS Count
FROM model.BillingServices bs
JOIN model.Invoices i ON i.Id = bs.InvoiceId
INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = i.LegacyPatientReceivablesAppointmentId
	AND ClinicalType IN ('B', 'K')
	AND ISNUMERIC(Symptom) = 1	
INNER JOIN dbo.Appointments ap on ap.AppointmentId = pc.AppointmentId 
	AND ap.ScheduleStatus <> 'A'
WHERE pc.[Status] = 'A'
	AND ((Symptom IN ('1', '2', '3', '4') AND bs.LegacyPatientReceivableServicesLinkedDiag = '')
	OR SUBSTRING(bs.LegacyPatientReceivableServicesLinkedDiag, 1, 1) = Symptom
	OR (SUBSTRING(bs.LegacyPatientReceivableServicesLinkedDiag, 2, 1) <> SUBSTRING(bs.LegacyPatientReceivableServicesLinkedDiag, 1, 1) AND SUBSTRING(bs.LegacyPatientReceivableServicesLinkedDiag, 2, 1) = Symptom)
	OR SUBSTRING(bs.LegacyPatientReceivableServicesLinkedDiag, 3, 1) = Symptom
	OR SUBSTRING(bs.LegacyPatientReceivableServicesLinkedDiag, 4, 1) = Symptom)
GROUP BY Symptom, bs.Id, ClinicalId, bs.LegacyPatientReceivableServicesLinkedDiag) AS q
	
GO