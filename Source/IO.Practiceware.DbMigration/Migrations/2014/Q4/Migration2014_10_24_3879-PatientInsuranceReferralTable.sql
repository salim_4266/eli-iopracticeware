﻿--Create table
EXEC dbo.DropObject 'model.PatientInsuranceReferrals'
GO

EXEC dbo.DropObject 'model.EncounterPatientInsuranceReferral'
GO


-- Check if 'PatientInsuranceReferrals' exists...
IF OBJECT_ID(N'model.PatientInsuranceReferrals', 'U') IS NULL
BEGIN
-- 'PatientInsuranceReferrals' does not exist, creating...
	CREATE TABLE [model].[PatientInsuranceReferrals] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [StartDateTime] datetime  NULL,
    [EndDateTime] datetime  NULL,
    [TotalEncountersCovered] int  NULL,
    [ReferralCode] nvarchar(max)  NOT NULL,
    [IsArchived] bit  NOT NULL,
    [ExternalProviderId] int  NULL,
    [PatientInsuranceId] bigint  NOT NULL,
    [PatientId] int  NOT NULL,
    [DoctorId] int  NOT NULL,
	[Comment] nvarchar(max) NULL,
	[LegacyPatientInsuranceReferralId] int NULL
);
END

--Update view definition to new table schema
INSERT INTO model.PatientInsuranceReferrals
(EndDateTime, ExternalProviderId, IsArchived, PatientId, PatientInsuranceId, DoctorId, ReferralCode, TotalEncountersCovered,StartDateTime, Comment, LegacyPatientInsuranceReferralId)-- PatientInsuranceReferral
SELECT
	CASE 
		WHEN COALESCE(ReferralExpireDate, '') <> ''
			THEN CONVERT(datetime, ReferralExpireDate, 112)
		ELSE NULL END AS EndDateTime,
	ReferredFromId AS ExternalProviderId,
	CONVERT(bit, CASE pRef.[Status]
			WHEN 'A'
				THEN CONVERT(bit, 0)
			ELSE CONVERT(bit, 1)
			END) AS IsArchived,
	p.Id AS PatientId,
	pi.Id AS PatientInsuranceId,
	pRef.ReferredTo AS DoctorId,
	pRef.Referral AS ReferralCode,
	pRef.ReferralVisits AS TotalEncountersCovered,
	CASE 
		WHEN COALESCE(ReferralDate, '') <> ''
			THEN CONVERT(datetime, ReferralDate, 112) 
		ELSE NULL END AS StartDateTime
	,pRef.Reason AS Comment
	,pRef.ReferralId AS LegacyPatientInsuranceReferralId
FROM dbo.PatientReferral pRef
INNER JOIN dbo.PracticeVendors pv ON pv.VendorId = pRef.ReferredFromId
	AND VendorLastName <> '' AND VendorLastName IS NOT NULL
INNER JOIN model.Patients p on p.Id = pRef.PatientId
INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = pRef.PatientId
	AND pi.IsDeleted = 0
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	AND (CONVERT(DATETIME,pRef.ReferralDate) >= ip.StartDateTime
	AND (CONVERT(DATETIME,pRef.ReferralDate) <= pi.EndDateTime OR pi.EndDateTime IS NULL))
INNER JOIN model.Insurers ins ON ins.LegacyPracticeInsurersId = ip.InsurerId
WHERE pRef.ReferredFromId > 0
	AND (pi.InsuredPatientId = pRef.PatientId OR ins.AllowDependents = 1)
	AND ((ip.InsurerId=ins.LegacyPracticeInsurersId) OR pRef.ReferredInsurer IS NULL) 
GO

-- Creating primary key on [Id] in table 'PatientInsuranceReferrals'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientInsuranceReferrals', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientInsuranceReferrals] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientInsuranceReferrals', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientInsuranceReferrals]
ADD CONSTRAINT [PK_PatientInsuranceReferrals]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO


-- Check if 'EncounterPatientInsuranceReferral' exists...
IF OBJECT_ID(N'model.EncounterPatientInsuranceReferral', 'U') IS NULL
BEGIN
-- 'EncounterPatientInsuranceReferral' does not exist, creating...
	CREATE TABLE [model].[EncounterPatientInsuranceReferral] (
    [Encounters_Id] int  NOT NULL,
    [PatientInsuranceReferrals_Id] int  NOT NULL
);
END

-- Creating primary key on [Id] in table 'EncounterPatientInsuranceReferral'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterPatientInsuranceReferral', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterPatientInsuranceReferral] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterPatientInsuranceReferral', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterPatientInsuranceReferral]
ADD CONSTRAINT [PK_EncounterPatientInsuranceReferrals]
    PRIMARY KEY CLUSTERED (Encounters_Id, PatientInsuranceReferrals_Id);
GO

INSERT INTO model.EncounterPatientInsuranceReferral
(Encounters_Id, PatientInsuranceReferrals_Id)
-- EncounterPatientInsuranceReferral
SELECT 
ap.AppointmentId AS Encounters_Id
	,pinsr.Id AS PatientInsuranceReferrals_Id
FROM dbo.PatientReferral pRef
INNER JOIN dbo.PracticeVendors pv ON pv.VendorId = pRef.ReferredFromId
	AND VendorLastName <> '' AND VendorLastName IS NOT NULL
INNER JOIN model.Patients p ON p.Id = pref.PatientId
INNER JOIN dbo.Appointments ap ON ap.ReferralId = pRef.ReferralId
INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = pRef.PatientId
	AND IsDeleted = 0
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	AND (CONVERT(DATETIME,pRef.ReferralDate) >= ip.StartDateTime 
	AND (CONVERT(DATETIME,pRef.ReferralDate) <= pi.EndDateTime OR pi.EndDateTime IS NULL))
INNER JOIN model.Insurers ins ON ins.Id = ip.InsurerId
INNER JOIN model.PatientInsuranceReferrals pinsr ON pinsr.LegacyPatientInsuranceReferralId = pRef.ReferralId
WHERE pRef.ReferredTo <> 0
	AND ((ins.LegacyPracticeInsurersId=pRef.ReferredInsurer) OR pRef.ReferredInsurer IS NULL) 	
GROUP BY ap.AppointmentId
,pinsr.Id

-- Creating foreign key on [ExternalProviderId] in table 'PatientInsuranceReferrals'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalProviderPatientInsuranceReferral'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientInsuranceReferrals] DROP CONSTRAINT FK_ExternalProviderPatientInsuranceReferral

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContacts]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInsuranceReferrals]
ADD CONSTRAINT [FK_ExternalProviderPatientInsuranceReferral]
    FOREIGN KEY ([ExternalProviderId])
    REFERENCES [model].[ExternalContacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalProviderPatientInsuranceReferral'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalProviderPatientInsuranceReferral')
	DROP INDEX IX_FK_ExternalProviderPatientInsuranceReferral ON [model].[PatientInsuranceReferrals]
GO
CREATE INDEX [IX_FK_ExternalProviderPatientInsuranceReferral]
ON [model].[PatientInsuranceReferrals]
    ([ExternalProviderId]);
GO

-- Creating foreign key on [PatientInsuranceId] in table 'PatientInsuranceReferrals'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientInsurancePatientInsuranceReferral'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientInsuranceReferrals] DROP CONSTRAINT FK_PatientInsurancePatientInsuranceReferral

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientInsurances]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInsuranceReferrals]
ADD CONSTRAINT [FK_PatientInsurancePatientInsuranceReferral]
    FOREIGN KEY ([PatientInsuranceId])
    REFERENCES [model].[PatientInsurances]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientInsurancePatientInsuranceReferral'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientInsurancePatientInsuranceReferral')
	DROP INDEX IX_FK_PatientInsurancePatientInsuranceReferral ON [model].[PatientInsuranceReferrals]
GO
CREATE INDEX [IX_FK_PatientInsurancePatientInsuranceReferral]
ON [model].[PatientInsuranceReferrals]
    ([PatientInsuranceId]);
GO


-- Creating foreign key on [PatientId] in table 'PatientInsuranceReferrals'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientPatientInsuranceReferral'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientInsuranceReferrals] DROP CONSTRAINT FK_PatientPatientInsuranceReferral

IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInsuranceReferrals]
ADD CONSTRAINT [FK_PatientPatientInsuranceReferral]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPatientInsuranceReferral'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientPatientInsuranceReferral')
	DROP INDEX IX_FK_PatientPatientInsuranceReferral ON [model].[PatientInsuranceReferrals]
GO
CREATE INDEX [IX_FK_PatientPatientInsuranceReferral]
ON [model].[PatientInsuranceReferrals]
    ([PatientId]);
GO

-- Creating foreign key on [Encounters_Id] in table 'EncounterPatientInsuranceReferral'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterPatientInsuranceReferral_Encounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterPatientInsuranceReferral] DROP CONSTRAINT FK_EncounterPatientInsuranceReferral_Encounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterPatientInsuranceReferral]
ADD CONSTRAINT [FK_EncounterPatientInsuranceReferral_Encounter]
    FOREIGN KEY ([Encounters_Id])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [PatientInsuranceReferrals_Id] in table 'EncounterPatientInsuranceReferral'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterPatientInsuranceReferral_PatientInsuranceReferral'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterPatientInsuranceReferral] DROP CONSTRAINT FK_EncounterPatientInsuranceReferral_PatientInsuranceReferral

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientInsuranceReferrals]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterPatientInsuranceReferral]
ADD CONSTRAINT [FK_EncounterPatientInsuranceReferral_PatientInsuranceReferral]
    FOREIGN KEY ([PatientInsuranceReferrals_Id])
    REFERENCES [model].[PatientInsuranceReferrals]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterPatientInsuranceReferral_PatientInsuranceReferral'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_EncounterPatientInsuranceReferral_PatientInsuranceReferral')
	DROP INDEX IX_FK_EncounterPatientInsuranceReferral_PatientInsuranceReferral ON [model].[EncounterPatientInsuranceReferral]
GO
CREATE INDEX [IX_FK_EncounterPatientInsuranceReferral_PatientInsuranceReferral]
ON [model].[EncounterPatientInsuranceReferral]
    ([PatientInsuranceReferrals_Id]);
GO

-- Creating foreign key on [DoctorId] in table 'PatientInsuranceReferrals'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_DoctorPatientInsuranceReferral'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientInsuranceReferrals] DROP CONSTRAINT FK_DoctorPatientInsuranceReferral

IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInsuranceReferrals]
ADD CONSTRAINT [FK_DoctorPatientInsuranceReferral]
    FOREIGN KEY ([DoctorId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DoctorPatientInsuranceReferral'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_DoctorPatientInsuranceReferral')
	DROP INDEX IX_FK_DoctorPatientInsuranceReferral ON [model].[PatientInsuranceReferrals]
GO
CREATE INDEX [IX_FK_DoctorPatientInsuranceReferral]
ON [model].[PatientInsuranceReferrals]
    ([DoctorId]);
GO

-- Update PatientInsuranceReferralId of InvoiceReceivables with new Ids inserted into PatientInsuranceReferrals
UPDATE ir
SET ir.PatientInsuranceReferralId = pir.Id
FROM model.InvoiceReceivables ir
INNER JOIN model.PatientInsuranceReferrals pir ON ir.PatientInsuranceReferralId = pir.LegacyPatientInsuranceReferralId
GO

--Also update rows of PatientInsuranceReferralId in model.InvoiceReceivables which may contain zero's
UPDATE model.InvoiceReceivables
SET PatientInsuranceReferralId = NULL
WHERE PatientInsuranceReferralId = 0
GO

--This is to fix bad policies
UPDATE ir 
SET ir.PatientInsuranceReferralId = NULL
FROM model.InvoiceReceivables ir
LEFT JOIN model.PatientInsuranceReferrals pir  on pir.Id = ir.PatientInsuranceReferralId
WHERE pir.Id IS NULL AND ir.PatientInsuranceReferralId IS NOT NULL


-- Creating foreign key on [PatientInsuranceReferralId] in table 'InvoiceReceivables'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientInsuranceReferralInvoiceReceivable'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[InvoiceReceivables] DROP CONSTRAINT FK_PatientInsuranceReferralInvoiceReceivable

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientInsuranceReferrals]'), 'IsUserTable') = 1
ALTER TABLE [model].[InvoiceReceivables]
ADD CONSTRAINT [FK_PatientInsuranceReferralInvoiceReceivable]
    FOREIGN KEY ([PatientInsuranceReferralId])
    REFERENCES [model].[PatientInsuranceReferrals]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientInsuranceReferralInvoiceReceivable'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientInsuranceReferralInvoiceReceivable')
	DROP INDEX IX_FK_PatientInsuranceReferralInvoiceReceivable ON [model].[InvoiceReceivables]
GO
CREATE INDEX [IX_FK_PatientInsuranceReferralInvoiceReceivable]
ON [model].[InvoiceReceivables]
    ([PatientInsuranceReferralId]);
GO