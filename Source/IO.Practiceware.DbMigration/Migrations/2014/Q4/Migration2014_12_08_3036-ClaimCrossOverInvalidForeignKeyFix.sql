﻿DELETE FROM model.ClaimCrossOvers
WHERE ReceivingInsurerId NOT IN (SELECT Id FROM model.Insurers)

DELETE FROM model.ClaimCrossOvers
WHERE SendingInsurerId NOT IN (SELECT Id FROM model.Insurers)

IF OBJECT_ID('tempdb..#NoAudit','U') IS NOT NULL
	DROP TABLE #NoAudit
GO

SELECT 1 AS Value INTO #NoAudit
GO

IF OBJECT_ID('tempdb..#EnableTriggers','U') IS NOT NULL
	DROP TABLE #EnableTriggers
GO

DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.ClaimCrossOvers'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

IF (OBJECTPROPERTY(OBJECT_ID('model.Insurers'),'IsUserTable') = 1 AND OBJECT_ID('dbo.PracticeInsurersBackup','U') IS NOT NULL
	AND NOT EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ClaimCrossOverInsurer'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	AND NOT EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ClaimCrossOverInsurer1'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT'))
BEGIN
	;WITH CTE AS (
	SELECT
	 c.*
	,i2.Id AS NewSendingInsurerId
	,i1.Id AS NewReceivingInsurerId
	FROM model.ClaimCrossOvers c
	JOIN dbo.PracticeInsurersBackup b1 on c.ReceivingInsurerId = b1.InsurerId
	JOIN dbo.PracticeInsurersBackup b2 on c.SendingInsurerId = b2.InsurerId
	JOIN model.Insurers i1 on i1.LegacyPracticeInsurersId = b1.InsurerId
		AND i1.LegacyPracticeInsurersId IS NOT NULL
	JOIN model.Insurers i2 on i2.LegacyPracticeInsurersId = b2.InsurerId
		AND i1.LegacyPracticeInsurersId IS NOT NULL
	WHERE (b1.InsurerId IS NOT NULL AND b2.InsurerId IS NOT NULL)
	)
	UPDATE c
	SET c.SendingInsurerId = CTE.NewSendingInsurerId
	,c.ReceivingInsurerId = CTE.NewReceivingInsurerId
	FROM model.ClaimCrossOvers c
	JOIN CTE ON CTE.Id = c.Id

END

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = ''
SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
EXEC sp_executesql @sqlEnable
DROP TABLE #EnableTriggers
GO

-- Creating foreign key on [ReceivingInsurerId] in table 'ClaimCrossOvers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ClaimCrossOverInsurer'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ClaimCrossOvers] DROP CONSTRAINT FK_ClaimCrossOverInsurer

IF OBJECTPROPERTY(OBJECT_ID('[model].[Insurers]'), 'IsUserTable') = 1
ALTER TABLE [model].[ClaimCrossOvers]
ADD CONSTRAINT [FK_ClaimCrossOverInsurer]
    FOREIGN KEY ([ReceivingInsurerId])
    REFERENCES [model].[Insurers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClaimCrossOverInsurer'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ClaimCrossOverInsurer')
	DROP INDEX IX_FK_ClaimCrossOverInsurer ON [model].[ClaimCrossOvers]
GO
CREATE INDEX [IX_FK_ClaimCrossOverInsurer]
ON [model].[ClaimCrossOvers]
    ([ReceivingInsurerId]);
GO

-- Creating foreign key on [SendingInsurerId] in table 'ClaimCrossOvers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ClaimCrossOverInsurer1'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ClaimCrossOvers] DROP CONSTRAINT FK_ClaimCrossOverInsurer1

IF OBJECTPROPERTY(OBJECT_ID('[model].[Insurers]'), 'IsUserTable') = 1
ALTER TABLE [model].[ClaimCrossOvers]
ADD CONSTRAINT [FK_ClaimCrossOverInsurer1]
    FOREIGN KEY ([SendingInsurerId])
    REFERENCES [model].[Insurers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClaimCrossOverInsurer1'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ClaimCrossOverInsurer1')
	DROP INDEX IX_FK_ClaimCrossOverInsurer1 ON [model].[ClaimCrossOvers]
GO
CREATE INDEX [IX_FK_ClaimCrossOverInsurer1]
ON [model].[ClaimCrossOvers]
    ([SendingInsurerId]);
GO