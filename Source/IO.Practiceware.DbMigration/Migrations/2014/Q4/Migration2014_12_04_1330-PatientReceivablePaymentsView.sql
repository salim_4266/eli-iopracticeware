﻿IF OBJECT_ID('dbo.PatientReceivablePayments','V') IS NOT NULL
BEGIN
	EXEC('
	ALTER VIEW [dbo].[PatientReceivablePayments]
	WITH VIEW_METADATA
	AS
	SELECT
	fb.Id AS PaymentId
	,0 AS ReceivableId
	,CASE 
		WHEN (fb.PatientId IS NOT NULL AND fb.FinancialSourceTypeId = 2)
			THEN fb.PatientId
		WHEN (fb.InsurerId IS NOT NULL AND fb.FinancialSourceTypeId = 1)
			THEN fb.InsurerId
		ELSE 0
	END AS PayerId
	,CASE 
		WHEN fb.FinancialSourceTypeId = 1
			THEN ''I''
		WHEN fb.FinancialSourceTypeId = 2
			THEN ''P''
		WHEN fb.FinancialSourceTypeId = 3
			THEN ''O''
		WHEN fb.FinancialSourceTypeId = 4
			THEN ''''
	END AS PayerType
	,''='' AS PaymentType
	,fb.Amount AS PaymentAmount
	,CONVERT(NVARCHAR,fb.PaymentDateTime,112) AS PaymentDate
	,COALESCE(fb.CheckCode,'''') AS PaymentCheck
	,''0'' AS PaymentRefId
	,COALESCE(SUBSTRING(pctRefType.Code, (dbo.GetMax((CHARINDEX('' - '', pctRefType.Code)+3), 0)), LEN(pctRefType.Code)),'''') AS PaymentRefType
	,''A'' AS [Status]
	,'''' AS PaymentService
	,''F''AS PaymentCommentOn
	,'''' AS PaymentFinancialType
	,0 AS PaymentServiceItem
	,0 AS PaymentAssignBy
	,''''	AS PaymentReason
	,0 AS PaymentBatchCheckId
	,CONVERT(NVARCHAR(32),'''') AS PaymentAppealNumber
	,CASE 
		WHEN fb.ExplanationOfBenefitsDateTime IS NOT NULL AND fb.ExplanationOfBenefitsDateTime <> ''''
			THEN CONVERT(NVARCHAR,fb.ExplanationOfBenefitsDateTime,112)
		WHEN fb.PaymentDateTime IS NOT NULL AND fb.PaymentDateTime <> ''''
			THEN CONVERT(NVARCHAR,fb.PaymentDateTime,112)
		ELSE ''''
	END AS PaymentEOBDate
	,CONVERT(DECIMAL(18,2),fb.Amount) AS PaymentAmountDecimal
	,CONVERT(INT,NULL) AS ClaimAdjustmentGroupCodeId
	FROM model.FinancialBatches fb
	LEFT JOIN model.PaymentMethods paym ON paym.Id = fb.PaymentMethodId
	LEFT JOIN dbo.PracticeCodeTable pctRefType ON SUBSTRING(pctRefType.Code, 1, CHARINDEX('' - '', pctRefType.Code)) = paym.Name
		AND pctRefType.ReferenceType = ''PAYABLETYPE''

	UNION ALL

	SELECT
	adj.LegacyPaymentId AS PaymentId
	,adj.LegacyInvoiceReceivableId AS ReceivableId
	,CASE 
		WHEN (adj.PatientId IS NOT NULL AND adj.FinancialSourceTypeId = 2)
			THEN adj.PatientId
		WHEN (adj.InsurerId IS NOT NULL AND adj.FinancialSourceTypeId = 1)
			THEN adj.InsurerId
		ELSE 0
	END AS PayerId
	,CONVERT(NVARCHAR(1),CASE 
		WHEN adj.FinancialSourceTypeId = 1
			THEN ''I''
		WHEN adj.FinancialSourceTypeId = 2
			THEN ''P''
		WHEN adj.FinancialSourceTypeId = 3
			THEN ''O''
		WHEN adj.FinancialSourceTypeId = 4
			THEN ''''
	END) AS PayerType
	,CASE 
		WHEN adj.AdjustmentTypeId = 1
			THEN ''P''
		WHEN adj.AdjustmentTypeId = 2
			THEN ''X''
		WHEN adj.AdjustmentTypeId = 3
			THEN ''8''
		WHEN adj.AdjustmentTypeId = 4
			THEN ''R''
		WHEN adj.AdjustmentTypeId = 5
			THEN ''U''
		WHEN adj.AdjustmentTypeId > 1000
			THEN SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
	END AS PaymentType
	,CONVERT(REAL,adj.Amount) AS PaymentAmount
	,CONVERT(NVARCHAR,adj.PostedDateTime,112) AS PaymentDate
	,COALESCE(fb.CheckCode,'''') AS PaymentCheck
	,CONVERT(NVARCHAR(64),CASE
		WHEN adj.Comment IS NOT NULL
			THEN adj.Comment
		ELSE ''''
	END) AS PaymentRefId
	,COALESCE(SUBSTRING(pctRefType.Code, (dbo.GetMax((CHARINDEX('' - '', pctRefType.Code)+3), 0)), LEN(pctRefType.Code)),'''') AS PaymentRefType
	,''A'' AS [Status]
	,ps.Code AS PaymentService
	,CASE 
		WHEN adj.IncludeCommentOnStatement = 1
			THEN ''T'' 
		ELSE ''F''
	END AS PaymentCommentOn
	,CASE at.IsDebit 
		WHEN 1
			THEN ''D''
		ELSE ''C''
	END AS PaymentFinancialType
	,prs.ItemId AS PaymentServiceItem
	,0 AS PaymentAssignBy
	,CASE 
		WHEN pctRsnCode.Code IS NOT NULL
			THEN pctRsnCode.Code
		ELSE ''''		
	END AS PaymentReason
	,CASE
		WHEN fb.Id IS NULL
			THEN 0
		ELSE fb.Id
	END AS PaymentBatchCheckId
	,CONVERT(NVARCHAR(32),'''') AS PaymentAppealNumber
	,CASE 
		WHEN fb.ExplanationOfBenefitsDateTime IS NOT NULL AND fb.ExplanationOfBenefitsDateTime <> ''''
			THEN CONVERT(NVARCHAR,fb.ExplanationOfBenefitsDateTime,112)
		WHEN fb.PaymentDateTime IS NOT NULL AND fb.PaymentDateTime <> ''''
			THEN CONVERT(NVARCHAR,fb.PaymentDateTime,112)
		ELSE ''''
	END AS PaymentEOBDate
	,adj.Amount AS PaymentAmountDecimal
	,CONVERT(INT,NULL) AS ClaimAdjustmentGroupCodeId
	FROM model.Adjustments adj
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
	LEFT JOIN PatientReceivableServices prs ON prs.ItemId= adj.BillingServiceId
	LEFT JOIN dbo.PracticeServices ps ON ps.Code = prs.Service
	LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
	LEFT JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
	LEFT JOIN dbo.PracticeCodeTable pct ON pct.Id = at.Id-1000
		AND pct.ReferenceType = ''PAYMENTTYPE'' 
	LEFT JOIN model.PaymentMethods paym ON paym.Id = fb.PaymentMethodId
	LEFT JOIN dbo.PracticeCodeTable pctRefType ON SUBSTRING(pctRefType.Code, 1, CHARINDEX('' - '', pctRefType.Code)) = paym.Name
		AND pctRefType.ReferenceType = ''PAYABLETYPE''
	LEFT JOIN dbo.PracticeCodeTable pctRsnCode ON pctRsnCode.Id = adj.ClaimAdjustmentReasonCodeId 
		AND pctRsnCode.ReferenceType = ''PAYMENTREASON''
	WHERE adj.InvoiceReceivableId IS NOT NULL

	UNION ALL

	SELECT
	fi.LegacyPaymentId AS PaymentId
	,fi.LegacyInvoiceReceivableId AS ReceivableId
	,CASE 
		WHEN (fi.PatientId IS NOT NULL AND fi.FinancialSourceTypeId = 2)
			THEN fi.PatientId
		WHEN (fi.InsurerId IS NOT NULL AND fi.FinancialSourceTypeId = 1)
			THEN fi.InsurerId
		ELSE 0
	END AS PayerId
	,CASE 
		WHEN fi.FinancialSourceTypeId = 1
			THEN ''I''
		WHEN fi.FinancialSourceTypeId = 2
			THEN ''P''
		WHEN fi.FinancialSourceTypeId = 3
			THEN ''O''
		WHEN fi.FinancialSourceTypeId = 4
			THEN ''''
	END AS PayerType
	,CASE 
		WHEN fi.FinancialInformationTypeId = 1
			THEN ''!''
		WHEN fi.FinancialInformationTypeId = 2
			THEN ''|''
		WHEN fi.FinancialInformationTypeId = 3
			THEN ''?''
		WHEN fi.FinancialInformationTypeId = 4
			THEN ''%''
		WHEN fi.FinancialInformationTypeId = 5
			THEN ''D''
		WHEN fi.FinancialInformationTypeId = 6
			THEN ''&''
		WHEN fi.FinancialInformationTypeId = 7
			THEN '']''
		WHEN fi.FinancialInformationTypeId = 8
			THEN ''0''
		WHEN fi.FinancialInformationTypeId = 9
			THEN ''V''
		WHEN fi.FinancialInformationTypeId > 1000
			THEN SUBSTRING(pctPymtType.Code, (dbo.GetMax((CHARINDEX('' - '', pctPymtType.Code)+3), 0)), LEN(pctPymtType.Code))
	END AS PaymentType
	,fi.Amount AS PaymentAmount
	,CONVERT(NVARCHAR,PostedDateTime,112) AS PaymentDate
	,COALESCE(fb.CheckCode,'''') AS PaymentCheck
	,CONVERT(NVARCHAR(64),CASE
		WHEN fi.Comment IS NOT NULL
			THEN fi.Comment
		ELSE ''''
	END) AS PaymentRefId
	,''K'' AS PaymentRefType
	,''A'' AS [Status]
	,ps.Code AS PaymentService
	,CASE 
		WHEN fi.IncludeCommentOnStatement = 1
			THEN ''T'' 
		ELSE ''F''
	END AS PaymentCommentOn
	,CASE 
		WHEN pctPymtType.AlternateCode IS NOT NULL
			THEN pctPymtType.AlternateCode 
		ELSE ''''
	END AS PaymentFinancialType
	,prs.ItemId AS PaymentServiceItem
	,0 AS PaymentAssignBy
	,CASE 
		WHEN pctRsnCode.Code IS NOT NULL
			THEN pctRsnCode.Code
		ELSE ''''		
	END AS PaymentReason
	,CASE
		WHEN fb.Id IS NULL
			THEN 0
		ELSE fb.Id
	END AS PaymentBatchCheckId
	,CONVERT(NVARCHAR(32),'''') AS PaymentAppealNumber
	,CASE 
		WHEN fb.ExplanationOfBenefitsDateTime IS NOT NULL AND fb.ExplanationOfBenefitsDateTime <> ''''
			THEN CONVERT(NVARCHAR,fb.ExplanationOfBenefitsDateTime,112)
		WHEN fb.PaymentDateTime IS NOT NULL AND fb.PaymentDateTime <> ''''
			THEN CONVERT(NVARCHAR,fb.PaymentDateTime,112)
		ELSE ''''
	END AS PaymentEOBDate
	,fi.Amount AS PaymentAmountDecimal
	,CONVERT(INT,NULL) AS ClaimAdjustmentGroupCodeId
	FROM model.FinancialInformations fi
	INNER JOIN PatientReceivableServices prs ON prs.ItemId= fi.BillingServiceId
	INNER JOIN dbo.PracticeServices ps ON ps.Code = prs.Service
	LEFT JOIN model.FinancialBatches fb ON fb.Id = fi.FinancialBatchId
	LEFT JOIN PracticeCodeTable pctPymtType ON pctPymtType.Id = fi.FinancialInformationTypeId-1000
		AND pctPymtType.ReferenceType = ''PAYMENTTYPE'' 
	LEFT JOIN dbo.PracticeCodeTable pctRsnCode ON pctRsnCode.Id = fi.ClaimAdjustmentReasonCodeId 
		AND pctRsnCode.ReferenceType = ''PAYMENTREASON''

	UNION ALL

	SELECT
	adj.LegacyPaymentId AS PaymentId
	,adj.LegacyInvoiceReceivableId AS ReceivableId
	,CASE 
		WHEN (adj.PatientId IS NOT NULL AND adj.FinancialSourceTypeId = 2)
			THEN adj.PatientId
		WHEN (adj.InsurerId IS NOT NULL AND adj.FinancialSourceTypeId = 1)
			THEN adj.InsurerId
		ELSE 0
	END AS PayerId
	,CONVERT(NVARCHAR(1),CASE 
		WHEN adj.FinancialSourceTypeId = 1
			THEN ''I''
		WHEN adj.FinancialSourceTypeId = 2
			THEN ''P''
		WHEN adj.FinancialSourceTypeId = 3
			THEN ''O''
		WHEN adj.FinancialSourceTypeId = 4
			THEN ''''
	END) AS PayerType
	,CASE 
		WHEN adj.AdjustmentTypeId = 1
			THEN ''P''
		WHEN adj.AdjustmentTypeId = 2
			THEN ''X''
		WHEN adj.AdjustmentTypeId = 3
			THEN ''8''
		WHEN adj.AdjustmentTypeId = 4
			THEN ''R''
		WHEN adj.AdjustmentTypeId = 5
			THEN ''U''
		WHEN adj.AdjustmentTypeId > 1000
			THEN SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
	END AS PaymentType
	,CONVERT(REAL,adj.Amount) AS PaymentAmount
	,CONVERT(NVARCHAR,adj.PostedDateTime,112) AS PaymentDate
	,COALESCE(fb.CheckCode,'''') AS PaymentCheck
	,CONVERT(NVARCHAR(64),CASE
		WHEN adj.Comment IS NOT NULL
			THEN adj.Comment
		ELSE ''''
	END) AS PaymentRefId
	,COALESCE(SUBSTRING(pctRefType.Code, (dbo.GetMax((CHARINDEX('' - '', pctRefType.Code)+3), 0)), LEN(pctRefType.Code)),'''') AS PaymentRefType
	,''A'' AS [Status]
	,''0'' AS PaymentService
	,''F'' AS PaymentCommentOn
	,CASE at.IsDebit 
		WHEN 1
			THEN ''D''
		ELSE ''C''
	END AS PaymentFinancialType
	,0 AS PaymentServiceItem
	,0 AS PaymentAssignBy
	,CASE 
		WHEN pctRsnCode.Code IS NOT NULL
			THEN pctRsnCode.Code
		ELSE ''''		
	END AS PaymentReason
	,CASE
		WHEN fb.Id IS NULL
			THEN 0
		ELSE fb.Id
	END AS PaymentBatchCheckId
	,CONVERT(NVARCHAR(32),'''') AS PaymentAppealNumber
	,CASE 
		WHEN fb.ExplanationOfBenefitsDateTime IS NOT NULL AND fb.ExplanationOfBenefitsDateTime <> ''''
			THEN CONVERT(NVARCHAR,fb.ExplanationOfBenefitsDateTime,112)
		WHEN fb.PaymentDateTime IS NOT NULL AND fb.PaymentDateTime <> ''''
			THEN CONVERT(NVARCHAR,fb.PaymentDateTime,112)
		ELSE ''''
	END AS PaymentEOBDate
	,adj.Amount AS PaymentAmountDecimal
	,CONVERT(INT,NULL) AS ClaimAdjustmentGroupCodeId
	FROM model.Adjustments adj
	LEFT JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
	LEFT JOIN dbo.PracticeCodeTable pct ON pct.Id = at.Id-1000
		AND pct.ReferenceType = ''PAYMENTTYPE'' 
	LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
	LEFT JOIN model.PaymentMethods paym ON paym.Id = fb.PaymentMethodId
	LEFT JOIN dbo.PracticeCodeTable pctRefType ON SUBSTRING(pctRefType.Code, 1, CHARINDEX('' - '', pctRefType.Code)) = paym.Name
		AND pctRefType.ReferenceType = ''PAYABLETYPE''
	LEFT JOIN dbo.PracticeCodeTable pctRsnCode ON pctRsnCode.Id = adj.ClaimAdjustmentReasonCodeId 
		AND pctRsnCode.ReferenceType = ''PAYMENTREASON''
	WHERE InvoiceReceivableId IS NULL
')
END
GO