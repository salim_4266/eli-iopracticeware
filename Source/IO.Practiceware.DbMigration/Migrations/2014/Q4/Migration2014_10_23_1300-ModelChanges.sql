﻿-- model.EncounterServices changes 

IF EXISTS (select * from sys.columns 
            where Name = N'IsConsultation' and Object_ID = Object_ID(N'model.EncounterServices')) 
		EXEC sp_rename 'model.EncounterServices.IsConsultation', 'IsReferringDoctorRequiredOnClaim', 'COLUMN'
GO

IF EXISTS (select * from sys.columns 
            where Name = N'IsOrderingProvider' and Object_ID = Object_ID(N'model.EncounterServices')) 
		EXEC sp_rename 'model.EncounterServices.IsOrderingProvider', 'IsOrderingProviderRequiredOnClaim', 'COLUMN'
GO

IF EXISTS (select * from sys.columns 
            where Name = N'IsZeroCharge' and Object_ID = Object_ID(N'model.EncounterServices')) 
		EXEC sp_rename 'model.EncounterServices.IsZeroCharge', 'IsZeroChargeAllowedOnClaim', 'COLUMN'
GO

IF EXISTS (select * from sys.columns 
            where Name = N'IsCliaWaived' and Object_ID = Object_ID(N'model.EncounterServices')) 
		EXEC sp_rename 'model.EncounterServices.IsCliaWaived', 'IsCliaCertificateRequiredOnClaim', 'COLUMN'
GO

IF EXISTS (select * from sys.columns 
            where Name = N'IsUnclassified' and Object_ID = Object_ID(N'model.EncounterServices')) 
	ALTER TABLE model.EncounterServices DROP COLUMN IsUnclassified
GO


IF EXISTS(SELECT * FROM sys.indexes WHERE name='IX_FK_RevenueCodeEncounterService' AND object_id = OBJECT_ID('model.EncounterServices'))
	DROP INDEX IX_FK_RevenueCodeEncounterService ON model.EncounterServices
GO


IF EXISTS(SELECT * 
    FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS 
    WHERE CONSTRAINT_NAME='FK_RevenueCodeEncounterService')
	ALTER TABLE model.EncounterServices DROP CONSTRAINT FK_RevenueCodeEncounterService
GO

IF EXISTS (select * from sys.columns 
            where Name = N'RevenueCodeId' and Object_ID = Object_ID(N'model.EncounterServices')) 
	ALTER TABLE model.EncounterServices DROP COLUMN RevenueCodeId
GO

IF EXISTS (select * from sys.columns 
            where Name = N'IsDoctorInvoice' and Object_ID = Object_ID(N'model.EncounterServices')) 
	ALTER TABLE model.EncounterServices DROP COLUMN IsDoctorInvoice
GO

IF EXISTS (select * from sys.columns 
            where Name = N'IsFacilityInvoice' and Object_ID = Object_ID(N'model.EncounterServices')) 
	ALTER TABLE model.EncounterServices DROP COLUMN IsFacilityInvoice
GO

IF EXISTS (select * from sys.columns 
            where Name = N'IsDrug' and Object_ID = Object_ID(N'model.EncounterServices')) 
	ALTER TABLE model.EncounterServices DROP COLUMN IsDrug
GO

IF NOT EXISTS (select * from sys.columns 
            where Name = N'Comment' and Object_ID = Object_ID(N'model.EncounterServices')) 
	ALTER TABLE model.EncounterServices ADD Comment NVARCHAR(MAX) NULL
GO

IF NOT EXISTS (select * from sys.columns 
            where Name = N'UnclassifiedServiceDescription' and Object_ID = Object_ID(N'model.EncounterServices')) 
	ALTER TABLE model.EncounterServices ADD UnclassifiedServiceDescription NVARCHAR(MAX) NULL
GO

IF NOT EXISTS (select * from sys.columns 
            where Name = N'FacilityEncounterServiceId' and Object_ID = Object_ID(N'model.BillingServices')) 
	ALTER TABLE model.BillingServices ADD FacilityEncounterServiceId INT NULL
GO


IF EXISTS (select * from sys.columns 
            where Name = N'EncounterServiceId' and Object_ID = Object_ID(N'model.BillingServices')) 
	ALTER TABLE model.BillingServices ALTER COLUMN EncounterServiceId INT NULL
GO

IF EXISTS(SELECT * 
    FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS 
    WHERE CONSTRAINT_NAME='FK_BillingServiceFacilityEncounterService')	

	-- Creating Foreign Key Between BillingServices And FacilityEncounterServices
ALTER TABLE model.BillingServices
	ADD CONSTRAINT FK_BillingServiceFacilityEncounterService FOREIGN KEY (FacilityEncounterServiceId) 
    REFERENCES model.FacilityEncounterServices (Id) 
GO

-- model.FacilityEncounterServices changes 

IF OBJECT_ID(N'[model].[FacilityEncounterServices]', 'U') IS NULL	
BEGIN
	CREATE TABLE model.FacilityEncounterServices
	(
		Id INT IDENTITY(1, 1) NOT NULL,
		Code NVARCHAR(MAX) NULL,
		DefaultUnit DECIMAL NOT NULL,
		Description NVARCHAR(MAX) NULL,
		EndDateTime DATETIME NULL,
		IsZeroChargeAllowedOnClaim BIT NOT NULL,
		RelativeValueUnit DECIMAL NULL,
		StartDateTime DATETIME NULL,
		UnitFee DECIMAL NOT NULL,
		IsArchived BIT NOT NULL,
		IsCliaCertificateRequiredOnClaim BIT  NOT NULL,
		UnclassifiedServiceDescription NVARCHAR(MAX) NULL,
		EncounterServiceTypeId INT NULL,
		RevenueCodeId INT NULL,
		ServiceUnitOfMeasurementId INT NOT NULL
	)
	-- Creating primary key on Id in table 'FacilityEncounterServices'
ALTER TABLE model.FacilityEncounterServices
	ADD CONSTRAINT PK_FacilityEncounterServices
    PRIMARY KEY (Id)



	-- Creating Foreign Key Between RevenueCodes And FacilityEncounterServices
ALTER TABLE model.FacilityEncounterServices
	ADD CONSTRAINT FK_RevenueCodeFacilityEncounterService FOREIGN KEY (RevenueCodeId) 
    REFERENCES model.RevenueCodes (Id) 

END
GO 
	
-- Creating foreign key on [EncounterServiceTypeId] in table 'FacilityEncounterServices'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterServiceTypeFacilityEncounterService'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[FacilityEncounterServices] DROP CONSTRAINT FK_EncounterServiceTypeFacilityEncounterService

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterServiceTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[FacilityEncounterServices]
ADD CONSTRAINT [FK_EncounterServiceTypeFacilityEncounterService]
    FOREIGN KEY ([EncounterServiceTypeId])
    REFERENCES [model].[EncounterServiceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterServiceTypeFacilityEncounterService'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_EncounterServiceTypeFacilityEncounterService')
	DROP INDEX IX_FK_EncounterServiceTypeFacilityEncounterService ON [model].[FacilityEncounterServices]
GO
CREATE INDEX [IX_FK_EncounterServiceTypeFacilityEncounterService]
ON [model].[FacilityEncounterServices]
    ([EncounterServiceTypeId]);
GO


-- Creating foreign key on [FacilityEncounterServiceId] in table 'BillingServices'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_FacilityEncounterServiceBillingService'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingServices] DROP CONSTRAINT FK_FacilityEncounterServiceBillingService

IF OBJECTPROPERTY(OBJECT_ID('[model].[FacilityEncounterServices]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingServices]
ADD CONSTRAINT [FK_FacilityEncounterServiceBillingService]
    FOREIGN KEY ([FacilityEncounterServiceId])
    REFERENCES [model].[FacilityEncounterServices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_FacilityEncounterServiceBillingService'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_FacilityEncounterServiceBillingService')
	DROP INDEX IX_FK_FacilityEncounterServiceBillingService ON [model].[BillingServices]
GO
CREATE INDEX [IX_FK_FacilityEncounterServiceBillingService]
ON [model].[BillingServices]
    ([FacilityEncounterServiceId]);
GO

