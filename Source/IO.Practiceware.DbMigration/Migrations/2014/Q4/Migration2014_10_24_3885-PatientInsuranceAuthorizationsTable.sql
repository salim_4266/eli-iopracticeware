﻿EXEC dbo.DropObject 'model.PatientInsuranceAuthorizations'
GO

-- Check if 'PatientInsuranceAuthorizations' exists...
IF OBJECT_ID(N'model.PatientInsuranceAuthorizations', 'U') IS NULL
BEGIN
-- 'PatientInsuranceAuthorizations' does not exist, creating...
	CREATE TABLE [model].[PatientInsuranceAuthorizations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [AuthorizationCode] nvarchar(max)  NOT NULL,
    [AuthorizationDateTime] datetime  NULL,
    [IsArchived] bit  NOT NULL,
    [PatientInsuranceId] bigint  NOT NULL,
    [EncounterId] int  NULL,
	[Comment] nvarchar(max) NULL,
	[LegacyPatientInsuranceAuthorizationsId] int NULL
);
END

INSERT INTO model.PatientInsuranceAuthorizations
(AuthorizationCode, AuthorizationDateTime, EncounterId, IsArchived, PatientInsuranceId, Comment, LegacyPatientInsuranceAuthorizationsId)
-- PatientInsuranceAuthorization
SELECT ppc.PreCert AS AuthorizationCode
	,CASE
		WHEN COALESCE(PreCertDate, '') <> ''
			THEN CONVERT(datetime, PreCertDate, 112) 
		ELSE NULL END AS AuthorizationDateTime
	,ap.AppointmentId AS EncounterId
	,CONVERT(bit, CASE PreCertStatus
			WHEN 'A'
				THEN CONVERT(bit, 0)
			ELSE CONVERT(bit, 1)
		END) AS IsArchived
	,pi.Id AS PatientInsuranceId
	,ppc.PreCertComment AS Comment
	,ppc.PreCertId AS LegacyPatientInsuranceAuthorizationsId	
FROM dbo.PatientPreCerts ppc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = ppc.PreCertApptId
	AND ap.ScheduleStatus IN ('D', 'P', 'R', 'A')
	AND ap.Comments <> 'ASC CLAIM'
INNER JOIN model.Patients p ON p.Id = ppc.PatientId
INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = ppc.PatientId
	AND IsDeleted = 0
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	AND (CONVERT(DATETIME,ppc.PreCertDate,112) >= ip.StartDateTime 
	AND (CONVERT(DATETIME,ppc.PreCertDate,112) <= pi.EndDateTime OR pi.EndDateTime IS NULL))
INNER JOIN model.Insurers ins ON ins.LegacyPracticeInsurersId = ppc.PreCertInsId
WHERE COALESCE(PreCertDate, '') <> ''
	
	UNION ALL

-----Second partition is for ASC claims.  You can have a preauthorization on both the facility invoice and the surgeon invoice but the EncounterId is the same for both invoices.
--*/*(no index)*/*
SELECT ppc.PreCert AS AuthorizationCode
	,CASE
		WHEN COALESCE(PreCertDate, '') <> ''
			THEN CONVERT(datetime, PreCertDate, 112) 
		ELSE NULL END AS AuthorizationDateTime
	,apEncounter.AppointmentId AS EncounterId
	,CONVERT(bit, CASE PreCertStatus
           WHEN 'A'
               THEN CONVERT(bit, 0)
           ELSE CONVERT(bit, 1)
		END) AS IsArchived
	,pi.Id AS PatientInsuranceId
	,ppc.PreCertComment AS Comment
	,ppc.PreCertId AS LegacyPatientInsuranceAuthorizationsId
FROM dbo.PatientPreCerts ppc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = ppc.PreCertApptId
	AND ap.ScheduleStatus IN ('D', 'P', 'R', 'A')
	AND ap.Comments = 'ASC CLAIM'
INNER JOIN model.Patients p ON p.Id = ppc.PatientId
INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = ppc.PatientId
	AND pi.IsDeleted = 0
INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
	AND apEncounter.AppTypeId = ap.AppTypeId
	AND apEncounter.AppDate = ap.AppDate
	AND apEncounter.AppTime > 0 
	AND ap.AppTime = 0
	AND apEncounter.ScheduleStatus = ap.ScheduleStatus
	AND ap.ScheduleStatus = 'D'
	AND apEncounter.ResourceId1 = ap.ResourceId1
	AND apEncounter.ResourceId2 = ap.ResourceId2
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	AND (CONVERT(DATETIME,ppc.PreCertDate,112) >= ip.StartDateTime 
	AND (CONVERT(DATETIME,ppc.PreCertDate,112) <= pi.EndDateTime OR pi.EndDateTime IS NULL))
INNER JOIN model.Insurers ins ON ins.LegacyPracticeInsurersId = ppc.PreCertInsId
WHERE COALESCE(PreCertDate, '') <> ''

GO

-- Creating primary key on [Id] in table 'PatientInsuranceAuthorizations'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientInsuranceAuthorizations', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientInsuranceAuthorizations] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientInsuranceAuthorizations', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientInsuranceAuthorizations]
ADD CONSTRAINT [PK_PatientInsuranceAuthorizations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [PatientInsuranceId] in table 'PatientInsuranceAuthorizations'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientInsurancePatientInsuranceAuthorization'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientInsuranceAuthorizations] DROP CONSTRAINT FK_PatientInsurancePatientInsuranceAuthorization

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientInsurances]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInsuranceAuthorizations]
ADD CONSTRAINT [FK_PatientInsurancePatientInsuranceAuthorization]
    FOREIGN KEY ([PatientInsuranceId])
    REFERENCES [model].[PatientInsurances]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientInsurancePatientInsuranceAuthorization'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientInsurancePatientInsuranceAuthorization')
	DROP INDEX IX_FK_PatientInsurancePatientInsuranceAuthorization ON [model].[PatientInsuranceAuthorizations]
GO
CREATE INDEX [IX_FK_PatientInsurancePatientInsuranceAuthorization]
ON [model].[PatientInsuranceAuthorizations]
    ([PatientInsuranceId]);
GO

-- Creating foreign key on [EncounterId] in table 'PatientInsuranceAuthorizations'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterPatientInsuranceAuthorization'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientInsuranceAuthorizations] DROP CONSTRAINT FK_EncounterPatientInsuranceAuthorization

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInsuranceAuthorizations]
ADD CONSTRAINT [FK_EncounterPatientInsuranceAuthorization]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterPatientInsuranceAuthorization'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_EncounterPatientInsuranceAuthorization')
	DROP INDEX IX_FK_EncounterPatientInsuranceAuthorization ON [model].[PatientInsuranceAuthorizations]
GO
CREATE INDEX [IX_FK_EncounterPatientInsuranceAuthorization]
ON [model].[PatientInsuranceAuthorizations]
    ([EncounterId]);
GO

-- Update PatientInsuranceAuthorizationId of InvoiceReceivables with new Ids inserted into PatientInsuranceAuthorizations
UPDATE ir SET ir.PatientInsuranceAuthorizationId = pia.Id 
   FROM model.InvoiceReceivables ir  
   INNER JOIN model.PatientInsuranceAuthorizations pia ON ir.PatientInsuranceAuthorizationId = pia.LegacyPatientInsuranceAuthorizationsId 
GO

UPDATE model.InvoiceReceivables
SET PatientInsuranceAuthorizationId = NULL
WHERE PatientInsuranceAuthorizationId NOT IN (SELECT Id FROM model.PatientInsuranceAuthorizations)

-- Creating foreign key on [PatientInsuranceAuthorizationId] in table 'InvoiceReceivables'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientInsuranceAuthorizationInvoiceReceivable'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[InvoiceReceivables] DROP CONSTRAINT FK_PatientInsuranceAuthorizationInvoiceReceivable

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientInsuranceAuthorizations]'), 'IsUserTable') = 1
ALTER TABLE [model].[InvoiceReceivables]
ADD CONSTRAINT [FK_PatientInsuranceAuthorizationInvoiceReceivable]
    FOREIGN KEY ([PatientInsuranceAuthorizationId])
    REFERENCES [model].[PatientInsuranceAuthorizations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientInsuranceAuthorizationInvoiceReceivable'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientInsuranceAuthorizationInvoiceReceivable')
	DROP INDEX IX_FK_PatientInsuranceAuthorizationInvoiceReceivable ON [model].[InvoiceReceivables]
GO
CREATE INDEX [IX_FK_PatientInsuranceAuthorizationInvoiceReceivable]
ON [model].[InvoiceReceivables]
    ([PatientInsuranceAuthorizationId]);