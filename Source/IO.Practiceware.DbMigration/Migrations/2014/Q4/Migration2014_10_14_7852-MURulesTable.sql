﻿DELETE FROM IE_RulesPhrases
DELETE FROM IE_Rules

SET IDENTITY_INSERT [dbo].[IE_Events] ON
IF NOT EXISTS (SELECT * FROM dbo.IE_Events WHERE EventId = 1) INSERT INTO IE_Events (EventId, EventName) VALUES (1, 'When Opening Chart')
IF NOT EXISTS (SELECT * FROM dbo.IE_Events WHERE EventId = 2) INSERT INTO IE_Events (EventId, EventName) VALUES (2, 'When Closing Chart')
IF NOT EXISTS (SELECT * FROM dbo.IE_Events WHERE EventId = 3) INSERT INTO IE_Events (EventId, EventName) VALUES (3, 'While Adding ICD')
IF NOT EXISTS (SELECT * FROM dbo.IE_Events WHERE EventId = 4) INSERT INTO IE_Events (EventId, EventName) VALUES (4, 'When Adding Exam Element')
IF NOT EXISTS (SELECT * FROM dbo.IE_Events WHERE EventId = 5) INSERT INTO IE_Events (EventId, EventName) VALUES (5, 'When Adding Medication')
IF NOT EXISTS (SELECT * FROM dbo.IE_Events WHERE EventId = 6) INSERT INTO IE_Events (EventId, EventName) VALUES (6, 'While Adding Lab Tests')
SET IDENTITY_INSERT [dbo].[IE_Events] OFF

SET IDENTITY_INSERT [dbo].[IE_Rules] ON 

GO
INSERT [dbo].[IE_Rules] ([RuleId], [RuleName], [EventId], [Result], [Priority], [ShowAlert_YN], [PatientRule_YN], [CreateBy], [CreateDt], [ModifyBy], [ModifyDt], [VersionNo], [PatientId], [Emcoder_Result]) 
VALUES (11, N'Patients with High Blood Pressure', 3, N'This patient has essential hypertension. Blood pressure is not controlled or has not been documented. To document, go to the exam element "Blood Pressure"

For Reference Info, click below.
https://support.iopracticeware.com/portal/helpcenter/articles/controlling-high-blood-pressure

Developer: National Committee for Quality Assurance - Health Care Accreditation Organization
Funding Source: Unspecified
Release: Version 3, revision date: 06/2014

Citation:
"Controlling High Blood Pressure (Measure)" National Quality Measures Clearinghouse (NQMC). Version 3. Agency for Healthcare Research and Quality (AHRQ), Jun. 2014. Web. 9 Jul. 2014.', 4, 1, 0, 2, CAST(0x0000A2F100C99468 AS DateTime), 2, CAST(0x0000A3710101D040 AS DateTime), N'', 0, N',,')
GO

INSERT [dbo].[IE_Rules] ([RuleId], [RuleName], [EventId], [Result], [Priority], [ShowAlert_YN], [PatientRule_YN], [CreateBy], [CreateDt], [ModifyBy], [ModifyDt], [VersionNo], [PatientId], [Emcoder_Result]) 
VALUES (14, N'Closing the Referral Loop: Receipt of Specialist Report', 3, N'This patient was referred to another provider, but communication may have not been made from that provider. If the provider has communicated back to you, note this in the exam element "Referral Response Received."

For Reference Info click below:
https://support.iopracticeware.com/portal/helpcenter/articles/closing-the-referral-loop-receipt-of-specialist-report

Developer: National Committee for Quality Assurance
Funding Source: Unspecified
Release: Version 3, revision date: 06/2014

Citation:
"Closing the Referral Loop: Receipt of Specialist Report" United States Health Information Knowledgebase. Version 3. Agency for Healthcare Research and Quality (AHRQ), Jun. 2014. Web. 30 June. 2014.', 4, 1, 0, 2, CAST(0x0000A35A01092BCD AS DateTime), 2, CAST(0x0000A3710101AD58 AS DateTime), N'', 0, N',,')
GO

INSERT [dbo].[IE_Rules] ([RuleId], [RuleName], [EventId], [Result], [Priority], [ShowAlert_YN], [PatientRule_YN], [CreateBy], [CreateDt], [ModifyBy], [ModifyDt], [VersionNo], [PatientId], [Emcoder_Result]) 
VALUES (16, N'Diabetes: Eye Exam', 3, N'This patient has a diagnosis of diabetes, therefore a dilated exam should be performed.

For Reference Info click below:
https://support.iopracticeware.com/portal/helpcenter/articles/diabetes-eye-exam

Developer: National Committee for Quality Assurance
Funding Source: Unspecified
Release: Version 3, revision date: 06/2014

Citation:
"Diabetes: Eye Exam" United States Health Information Knowledgebase. Version 3. Agency for Healthcare Research and Quality (AHRQ), Jun. 2014. Web. 30 June. 2014.', 4, 1, 0, 2, CAST(0x0000A35A014A1F43 AS DateTime), 2, CAST(0x0000A3710101B5CC AS DateTime), N'', 0, N',,')
GO

INSERT [dbo].[IE_Rules] ([RuleId], [RuleName], [EventId], [Result], [Priority], [ShowAlert_YN], [PatientRule_YN], [CreateBy], [CreateDt], [ModifyBy], [ModifyDt], [VersionNo], [PatientId], [Emcoder_Result]) 
VALUES (17, N'Cataracts: 20/40 or Better Visual Acuity within 90 Days Following Cataract Surgery', 3, N'This patient has had cataract surgery performed in the past 90 days and a vision hasn''t yet been recorded prior to this visit. Add a vision into the Vision Uncorrected or Best Corrected Visual Acuity exam element.

For Reference Info click below:
https://support.iopracticeware.com/portal/helpcenter/articles/cataracts-20-40-or-better-visual-acuity-within-90-days-following-cataract-surgery

Developer: American Medical Association-convened Physician Consortium for Performance Improvement(R) (AMA-PCPI), National Committee for Quality Assurance
Funding Source: Unspecified
Release: Version 3, revision date: 06/2014

Citation:
"Cataracts: 20/40 or Better Visual Acuity within 90 Days Following Cataract Surgery" United States Health Information Knowledgebase. Version 3. Agency for Healthcare Research and Quality (AHRQ), Jun. 2014. Web. 8 July. 2014.', 4, 1, 0, 2, CAST(0x0000A36201383503 AS DateTime), 2, CAST(0x0000A3710101A4F6 AS DateTime), N'', 0, N',,')
GO

INSERT [dbo].[IE_Rules] ([RuleId], [RuleName], [EventId], [Result], [Priority], [ShowAlert_YN], [PatientRule_YN], [CreateBy], [CreateDt], [ModifyBy], [ModifyDt], [VersionNo], [PatientId], [Emcoder_Result]) 
VALUES (18, N'Diabetic Retinopathy: Documentation of Presence or Absence ME and Severity', 3, N'This patient has been seen more than once this year and has diabetic retinopathy. Dilation or severity of retinopathy or presence/absence of macular edema have not been fully documented prior to this visit.

For Reference Info click below:
https://support.iopracticeware.com/portal/helpcenter/articles/diabetic-retinopathy-documentation-of-presence-or-absence-of-macular-edema-and-level-of-severity-of-retinopathy

Developer: American Medical Association-convened Physician Consortium for Performance Improvement(R) (AMA-PCPI), National Committee for Quality Assurance
Funding Source: Unspecified
Release: Version 3, revision date: 06/2014

Citation:
"Diabetic Retinopathy: Documentation of Presence or Absence of Macular Edema and Level of Severity of Retinopathy (Measure)" United States Health Information Knowledgebase. Version 3. Agency for Healthcare Research and Quality (AHRQ), Jun. 2014. Web. 8 July. 2014.', 4, 1, 0, 2, CAST(0x0000A362014EDECD AS DateTime), 2, CAST(0x0000A3710101C5FD AS DateTime), N'', 0, N',,')
GO

INSERT [dbo].[IE_Rules] ([RuleId], [RuleName], [EventId], [Result], [Priority], [ShowAlert_YN], [PatientRule_YN], [CreateBy], [CreateDt], [ModifyBy], [ModifyDt], [VersionNo], [PatientId], [Emcoder_Result]) 
VALUES (19, N'Diabetic Retinopathy: Communication with the Physician Managing Ongoing Diabetes Care', 3, N'This patient has been seen more than once this year and has diabetic retinopathy with dilation performed. Severity of retinopathy or presence/absence of macular edema have not been fully documented or communication with the referring physician has not been made prior to this visit.

For Reference Info click below:
https://support.iopracticeware.com/portal/helpcenter/articles/diabetic-retinopathy-documentation-of-presence-or-absence-of-macular-edema-and-level-of-severity-of-retinopathy

Developer: American Medical Association-convened Physician Consortium for Performance Improvement(R) (AMA-PCPI), National Committee for Quality Assurance
Funding Source: Unspecified
Release: Version 3, revision date: 06/2014

Citation:
"Diabetic Retinopathy: Communication with the Physician Managing Ongoing Diabetes Care" United States Health Information Knowledgebase. Version 3. Agency for Healthcare Research and Quality (AHRQ), Jun. 2014. Web. 8 July. 2014.', 4, 1, 0, 2, CAST(0x0000A3630128FF04 AS DateTime), 2, CAST(0x0000A3710101BF03 AS DateTime), N'', 0, N',,')
GO

INSERT [dbo].[IE_Rules] ([RuleId], [RuleName], [EventId], [Result], [Priority], [ShowAlert_YN], [PatientRule_YN], [CreateBy], [CreateDt], [ModifyBy], [ModifyDt], [VersionNo], [PatientId], [Emcoder_Result]) 
VALUES (21, N'Preventive Care and Screening: Body Mass Index (BMI) Screening', 3, N'This patient has a BMI that is outside of normal range for their age. Document counseling and surveillance in the "Height and Weight" exam element.

For Reference Info, click below.
https://support.iopracticeware.com/portal/helpcenter/articles/preventive-care-and-screening-body-mass-index-bmi-screening-and-follow-up

Developer: Quality Insights of Pennsylvania
Funding Source: Unspecified
Release: Version 3, revision date: 06/2014

Citation:
"Preventive Care and Screening: Body Mass Index (BMI) Screening and
Follow-Up Plan (Measure)
" National Quality Measures Clearinghouse (NQMC). Version 3. Agency for Healthcare Research and Quality (AHRQ), Jun. 2014. Web. 22 Jul. 2014.', 4, 1, 0, 2, CAST(0x0000A3700126457B AS DateTime), 2, CAST(0x0000A3710101D848 AS DateTime), N'', 0, N',,')
GO

INSERT [dbo].[IE_Rules] ([RuleId], [RuleName], [EventId], [Result], [Priority], [ShowAlert_YN], [PatientRule_YN], [CreateBy], [CreateDt], [ModifyBy], [ModifyDt], [VersionNo], [PatientId], [Emcoder_Result]) 
VALUES (22, N'Primary Open Angle Glaucoma (POAG): Optic Nerve Evaluation', 3, N'This patient has POAG and an optic nerve evaluation hasn''t been performed in the past 12 months. Document in the "Cup/Disc" ratio element, along with optic nerve findings in the scratch pad.

For Reference Info click below:
https://support.iopracticeware.com/portal/helpcenter/articles/primary-open-angle-glaucoma-poag-optic-nerve-evaluation

Developer: American Medical Association-convened Physician Consortium for Performance Improvement(R) (AMA-PCPI)
Funding Source: Unspecified
Release: Version 3, revision date: 06/2014

Citation:
"Primary Open-Angle Glaucoma (POAG): Optic Nerve Evaluation
(Measure)
" United States Health Information Knowledgebase. Version 3. Agency for Healthcare Research and Quality (AHRQ), Jun. 2014. Web. 22 July. 2014.', 4, 1, 0, 2, CAST(0x0000A370013541B3 AS DateTime), 2, CAST(0x0000A3710101DFE0 AS DateTime), N'', 0, N',,')
GO

INSERT [dbo].[IE_Rules] ([RuleId], [RuleName], [EventId], [Result], [Priority], [ShowAlert_YN], [PatientRule_YN], [CreateBy], [CreateDt], [ModifyBy], [ModifyDt], [VersionNo], [PatientId], [Emcoder_Result]) 
VALUES (23, N'High IOP', 1, N'The patient''s intraocular pressure is above 21 mmHg.

For Reference Info, click below.
http://one.aao.org/bcscsnippetdetail.aspx?id=f010bbf6-3f3e-486b-b5cd-0ad86ddb9d74

Developer: IO Practiceware, Inc.
Funding Source: Unspecified
Release: Version 8.1, revision date: 07/2014

Citation:
"INTRAOCULAR PRESSURE" ONE Network. American Academy of Ophthalmology, 2013. Web. 23 Jul. 2014.', 1, 1, 0, 2, CAST(0x0000A37100FEC8BE AS DateTime), 2, CAST(0x0000A37101019BF6 AS DateTime), N'', 0, N',,')
GO

INSERT [dbo].[IE_Rules] ([RuleId], [RuleName], [EventId], [Result], [Priority], [ShowAlert_YN], [PatientRule_YN], [CreateBy], [CreateDt], [ModifyBy], [ModifyDt], [VersionNo], [PatientId], [Emcoder_Result]) 
VALUES (24, N'Low IOP', 1, N'The patient''s intraocular pressure is below 10 mmHg.

For Reference Info, click below.
http://one.aao.org/bcscsnippetdetail.aspx?id=f010bbf6-3f3e-486b-b5cd-0ad86ddb9d74

Developer: IO Practiceware, Inc.
Funding Source: Unspecified
Release: Version 8.1, revision date: 07/2014

Citation:
"INTRAOCULAR PRESSURE" ONE Network. American Academy of Ophthalmology, 2013. Web. 23 Jul. 2014.
', 1, 1, 0, 2, CAST(0x0000A371010190EE AS DateTime), NULL, NULL, N'', 0, N',,')

GO
SET IDENTITY_INSERT [dbo].[IE_Rules] OFF
GO


SET IDENTITY_INSERT [dbo].[IE_RulesPhrases] ON 

GO
INSERT [dbo].[IE_RulesPhrases] ([FldPhraseId], [RuleId], [SlNo], [FieldArea], [FieldName], [FieldDesp], [Operation], [FieldValue], [AndOrFlag]) 
VALUES (374, 24, 1, N'ENCOUNTER', N'IOP', N'IOP', N'<', N'10', N'')
GO
INSERT [dbo].[IE_RulesPhrases] ([FldPhraseId], [RuleId], [SlNo], [FieldArea], [FieldName], [FieldDesp], [Operation], [FieldValue], [AndOrFlag]) 
VALUES (375, 23, 1, N'ENCOUNTER', N'IOP', N'IOP', N'>', N'21', N'')
GO
INSERT [dbo].[IE_RulesPhrases] ([FldPhraseId], [RuleId], [SlNo], [FieldArea], [FieldName], [FieldDesp], [Operation], [FieldValue], [AndOrFlag]) 
VALUES (376, 17, 1, N'QUERY', N'QUERY', N'QUERY.QUERY', N'IN', N'select distinct CAST (pc.patientid as BIT)
from PatientClinical pc
inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
inner join model.Patients p on p.Id = @PatientId
inner join df.Questions q on q.Question = SUBSTRING(Symptom, 2, LEN(Symptom))
left join
	(select pc.AppointmentId, AppDate
	from PatientClinical pc
	inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
	where 
	pc.PatientId = @PatientId
	and ClinicalType = ''F''
	and (SUBSTRING(Symptom,2,18) = ''Vision UnCorrected''
		or SUBSTRING(Symptom,2,28) = ''Best Corrected Visual Acuity''
		)
		) va on va.AppDate > ap.AppDate
where 
model.GetAgeFromDob(DateOfBirth) >= 18
and ClinicalType = ''F''
and (q.ControlName like ''%66840%'' 
	or q.ControlName like ''%66850%''
	or q.ControlName like ''%66852%''
	or q.ControlName like ''%66920%''
	or q.ControlName like ''%66930%''
	or q.ControlName like ''%66940%''
	or q.ControlName like ''%66982%''
	or q.ControlName like ''%66983%''
	or q.ControlName like ''%66984%''
	)
and DATEDIFF(dd,CONVERT(date,ap.AppDate),CONVERT(date,GETDATE())) <= 90
and va.AppointmentId is NULL', N'')
GO

INSERT [dbo].[IE_RulesPhrases] ([FldPhraseId], [RuleId], [SlNo], [FieldArea], [FieldName], [FieldDesp], [Operation], [FieldValue], [AndOrFlag]) 
VALUES (377, 14, 1, N'QUERY', N'QUERY', N'QUERY.QUERY', N'IN', N'select distinct CAST (pc.patientid as BIT)
from PatientClinical pc
inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
left join (
	select distinct pc.PatientId
	from PatientClinical pc
	inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
	where pc.PatientId = @PatientId
	and SUBSTRING(appdate,1,4) = DATEPART (yyyy,getdate())
	and ClinicalType = ''F''
	and Symptom = ''/REFERRAL RESPONSE RECEIVED''
	) recd on recd.PatientId = pc.PatientId
WHERE pc.PatientId = @PatientId
AND SUBSTRING(appdate,1,4) = DATEPART (yyyy,getdate())
AND ClinicalType = ''A''
AND (SUBSTRING(FindingDetail,1,34) = ''REFER PATIENT TO ANOTHER PHYSICIAN''
		OR (SUBSTRING (FindingDetail, 1, 26) = ''SEND A CONSULTATION LETTER'' 
			AND SUBSTRING (FindingDetail, 30, 5) = ''REFER''
			)
		)
AND recd.PatientId is NULL', N'')
GO

INSERT [dbo].[IE_RulesPhrases] ([FldPhraseId], [RuleId], [SlNo], [FieldArea], [FieldName], [FieldDesp], [Operation], [FieldValue], [AndOrFlag]) 
VALUES (378, 16, 1, N'QUERY', N'QUERY', N'QUERY.QUERY', N'IN', N'select DISTINCT CAST (pc.PatientId AS BIT) 
from PatientClinical pc 
	inner join model.Patients p on p.id=pc.PatientId 
	left join (
		select ap.PatientId
		from patientclinical pc
		inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
		where ap.patientid = @PatientId
		and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
		and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
		and Symptom in (''/DILATION'',''/DILATED IOP PROVOCATIVE'',''/DILATED REFRACTION'',''/FA'',''/FA AND FP'',''/FA AND FP TECH'',''/FLUORESCEIN ANGIO'',''/FLUORESCEIN ANGIO TECH'',''/FLUORESCEIN ANGIOGRAPHY'',''/ICG ANGIOGRAPHY '',''/ICG TECH '',''/INFRARED'',''/INFRARED TECH'',''/OPHTHALMOSCOPY INITIAL'',''/OPHTHALMOSCOPY SUBSEQ'',''/OPHTHALMOSCOPY, DETECTION'',''/OPHTHALMOSCOPY, MANAGE'',''PQRI (ARMD DILATED MACULAR EXAM)'',''PQRI (DM RETINOPATHY DOCUMENT)'',''PQRI (DM RETINOPATHY INFO)'',''PQRI (DM1 DILATED EXAM IN DM)'')
		) dilate on dilate.PatientId = pc.PatientId 	
where pc.PatientId = @PatientID
AND model.GetAgeFromDob(DateOfBirth) between 18 and 75
AND ClinicalType = ''Q'' 
AND Status = ''A'' 
AND SUBSTRING (FindingDetail, 1,6) in (''250.00'', ''250.01'', ''250.02'', ''250.03'', ''250.10'', ''250.11'', ''250.12'', ''250.13'', ''250.20'', ''250.21'', ''250.22'', ''250.23'',''250.30'', ''250.31'', ''250.32'', ''250.33'', ''250.41'', ''250.42'', ''250.43'', ''250.50'', ''250.51'', ''250.52'', ''250.53'', ''250.60'', ''250.61'', ''250.62'', ''250.63'', ''250.70'', ''250.71'', ''250.72'', ''250.73'', ''250.80'', ''250.81'', ''250.82'', ''250.83'', ''250.90'', ''250.91'', ''250.92'', ''250.93'', ''648.01'', ''648.02'', ''648.03'', ''648.04'')
AND dilate.PatientId is NULL', N'')
GO

INSERT [dbo].[IE_RulesPhrases] ([FldPhraseId], [RuleId], [SlNo], [FieldArea], [FieldName], [FieldDesp], [Operation], [FieldValue], [AndOrFlag]) 
VALUES (379, 19, 1, N'QUERY', N'QUERY', N'QUERY.QUERY', N'IN', N'select distinct CAST (pc.patientid as BIT)
from PatientClinical pc
	inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
	inner join model.Patients p on p.Id = @PatientId
	inner join (
		select patientid
		from Appointments
		where patientid = @PatientId
		and AppTime > 0
		and SUBSTRING(appdate,1,4) = DATEPART (yyyy,getdate())
		group by patientid
		having count(patientid) >= 2
		) appcount on appcount.PatientId = pc.PatientId
	inner join (
		select ap.PatientId
		from patientclinical pc
		inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
		where ap.patientid = @PatientId
		and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
		and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
		and Symptom in (''/DILATION'',''/DILATED IOP PROVOCATIVE'',''/DILATED REFRACTION'',''/FA'',''/FA AND FP'',''/FA AND FP TECH'',''/FLUORESCEIN ANGIO'',''/FLUORESCEIN ANGIO TECH'',''/FLUORESCEIN ANGIOGRAPHY'',''/ICG ANGIOGRAPHY '',''/ICG TECH '',''/INFRARED'',''/INFRARED TECH'',''/OPHTHALMOSCOPY INITIAL'',''/OPHTHALMOSCOPY SUBSEQ'',''/OPHTHALMOSCOPY, DETECTION'',''/OPHTHALMOSCOPY, MANAGE'',''PQRI (ARMD DILATED MACULAR EXAM)'',''PQRI (DM RETINOPATHY DOCUMENT)'',''PQRI (DM RETINOPATHY INFO)'',''PQRI (DM1 DILATED EXAM IN DM)'')
		) dilate on dilate.PatientId = pc.PatientId 
	left join (
		select ap.PatientId
		from patientclinical pc
		inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
		where ap.patientid = @PatientId
		and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
		and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
		and Symptom = ''/CQM EXCEPTIONS''
		and SUBSTRING(FindingDetail,2,16) = ''DRCOMMUNICATION''
		) cqm on cqm.PatientId = pc.PatientId
	left join (
		select ap.patientid
		from PatientClinical pc
		inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
		where ap.patientid = @PatientId
		and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
		and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
		and ClinicalType = ''U''
		and FindingDetail in (''362.04'', ''362.05'', ''362.06'')
		) severity on severity.PatientId = pc.PatientId
	left join (
		select ap.patientid
		from PatientClinical pc
		inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
		where ap.patientid = @PatientId
		and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
		and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
		and ClinicalType = ''Q''
		and FindingDetail in (''362.07'',''362.07.01'',''362.07.02'',''362.07.03'',''362.07.04'',''362.07.05'',''362.16.11'',''362.53'',''362.53.1'',''362.83'',''362.83.2'',''362.83.23'',''362.83.4'',''362.83.5'',''362.83.77'',''366.16.44'',''377.0'',''377.00'',''377.00.11'',''377.00.12'',''377.00.13'',''377.00.14'',''377.01'',''377.02'',''377.03'',''377.24'',''377.49.01'',''P158'',''P405'',''P427'',''P978'',''P979'',''Q009'',''Q027'',''Q029'',''Q100'',''Q151'',''Q173'',''Q909'',''S612'',''S743'')
		) me on me.PatientId = pc.PatientId
	left join (
		select ap.PatientId
		from patientclinical pc
		inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
		where ap.patientid = @PatientId
		and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
		and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
		and SUBSTRING (FindingDetail, 1, 26) = ''SEND A CONSULTATION LETTER''
		) comm on comm.PatientId = pc.PatientId 
where model.GetAgeFromDob(DateOfBirth) >= 18
	and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
	and ClinicalType = ''U''
	and FindingDetail in (''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
	and cqm.PatientId is NULL
	and (severity.PatientId is NULL
		OR me.PatientId is NULL
		OR comm.PatientId is NULL)', N'')
GO

INSERT [dbo].[IE_RulesPhrases] ([FldPhraseId], [RuleId], [SlNo], [FieldArea], [FieldName], [FieldDesp], [Operation], [FieldValue], [AndOrFlag]) 
VALUES (380, 18, 1, N'QUERY', N'QUERY', N'QUERY.QUERY', N'IN', N'QUERY.QUERY IN select distinct CAST (pc.patientid as BIT)
from PatientClinical pc
inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
inner join model.Patients p on p.Id = @PatientId
inner join (
select patientid
from Appointments
where patientid = @PatientId
and AppTime > 0
and SUBSTRING(appdate,1,4) = DATEPART (yyyy,getdate())
group by patientid
having count(patientid) >= 2
) appcount on appcount.PatientId = pc.PatientId
left join (
select ap.PatientId
from patientclinical pc
inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
where ap.patientid = @PatientId
and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
and Symptom = ''/CQM EXCEPTIONS''
and SUBSTRING(FindingDetail,2,15) = ''DRDOCUMENTATION''
) cqm on cqm.PatientId = pc.PatientId
left join (
select ap.PatientId
from patientclinical pc
inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
where ap.patientid = @PatientId
and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
and Symptom in (''/DILATION'',''/DILATED IOP PROVOCATIVE'',''/DILATED REFRACTION'',''/FA'',''/FA AND FP'',''/FA AND FP TECH'',''/FLUORESCEIN ANGIO'',''/FLUORESCEIN ANGIO TECH'',''/FLUORESCEIN ANGIOGRAPHY'',''/ICG ANGIOGRAPHY '',''/ICG TECH '',''/INFRARED'',''/INFRARED TECH'',''/OPHTHALMOSCOPY INITIAL'',''/OPHTHALMOSCOPY SUBSEQ'',''/OPHTHALMOSCOPY, DETECTION'',''/OPHTHALMOSCOPY, MANAGE'',''PQRI (ARMD DILATED MACULAR EXAM)'',''PQRI (DM RETINOPATHY DOCUMENT)'',''PQRI (DM RETINOPATHY INFO)'',''PQRI (DM1 DILATED EXAM IN DM)'')
) dilate on dilate.PatientId = pc.PatientId 
left join (
select ap.patientid
from PatientClinical pc
inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
where ap.patientid = @PatientId
and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
and ClinicalType = ''U''
and FindingDetail in (''362.04'', ''362.05'', ''362.06'')
) severity on severity.PatientId = pc.PatientId
left join (
select ap.patientid
from PatientClinical pc
inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
where ap.patientid = @PatientId
and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
and ClinicalType = ''Q''
and FindingDetail in (''362.07'',''362.07.01'',''362.07.02'',''362.07.03'',''362.07.04'',''362.07.05'',''362.16.11'',''362.53'',''362.53.1'',''362.83'',''362.83.2'',''362.83.23'',''362.83.4'',''362.83.5'',''362.83.77'',''366.16.44'',''377.0'',''377.00'',''377.00.11'',''377.00.12'',''377.00.13'',''377.00.14'',''377.01'',''377.02'',''377.03'',''377.24'',''377.49.01'',''P158'',''P405'',''P427'',''P978'',''P979'',''Q009'',''Q027'',''Q029'',''Q100'',''Q151'',''Q173'',''Q909'',''S612'',''S743'')
) me on me.PatientId = pc.PatientId
where model.GetAgeFromDob(DateOfBirth) >= 18
and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
and ClinicalType = ''U''
and FindingDetail in (''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
and cqm.PatientId is NULL
and (dilate.PatientId is NULL 
OR severity.PatientId is NULL
OR me.PatientId is NULL) ', N'')
GO

INSERT [dbo].[IE_RulesPhrases] ([FldPhraseId], [RuleId], [SlNo], [FieldArea], [FieldName], [FieldDesp], [Operation], [FieldValue], [AndOrFlag]) 
VALUES (381, 11, 1, N'QUERY', N'QUERY', N'QUERY.QUERY', N'IN', N'select distinct CAST (pc.patientid as BIT)
from PatientClinical pc
	inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
	left join model.PatientBloodPressures pbp on pbp.PatientId = @PatientId
where CONVERT(date,AppDate) <= GETDATE()
	and pc.patientid = @PatientId
	and clinicaltype = ''Q''
	and SUBSTRING(FindingDetail,1,6) in (''401.0'', ''401.1'', ''401.9'')
	and (pbp.PatientId is NULL
		OR pbp.SystolicPressure is NULL
		OR pbp.DiastolicPressure is NULL
		OR (pbp.SystolicPressure >= 140 AND pbp.DiastolicPressure >= 90)
		)', N'')
GO
INSERT [dbo].[IE_RulesPhrases] ([FldPhraseId], [RuleId], [SlNo], [FieldArea], [FieldName], [FieldDesp], [Operation], [FieldValue], [AndOrFlag]) VALUES (382, 21, 1, N'QUERY', N'QUERY', N'QUERY.QUERY', N'IN', N'select distinct CAST (pc.patientid as BIT) from 
PatientClinical pc
inner join model.patients p on p.id = pc.PatientId
where PatientId = @PatientId
and ClinicalType = ''F''
and Symptom = ''/Height and Weight''
and Status = ''A''
and SUBSTRING(FindingDetail,2,3) = ''BMI''
and (
		(
			(model.GetAgeFromDob(DateOfBirth) between 18 and 64)
			and
			(
				(dbo.ExtractTextValue(''<'', FindingDetail,''>'') < ''18.5'')
				or
				(dbo.ExtractTextValue(''<'', FindingDetail,''>'') >= ''25'')
			)
		)
		or
		(
			(model.GetAgeFromDob(DateOfBirth) >= 65)
			and
			(
				(dbo.ExtractTextValue(''<'', FindingDetail,''>'') < ''23'')
				or
				(dbo.ExtractTextValue(''<'', FindingDetail,''>'') >= ''30'')
			)
		)
	)', N'')
GO

INSERT [dbo].[IE_RulesPhrases] ([FldPhraseId], [RuleId], [SlNo], [FieldArea], [FieldName], [FieldDesp], [Operation], [FieldValue], [AndOrFlag]) 
VALUES (383, 22, 1, N'QUERY', N'QUERY', N'QUERY.QUERY', N'IN', N'select DISTINCT CAST (pc.PatientId AS BIT) 
from PatientClinical pc 
	inner join model.Patients p on p.id=pc.PatientId 
	left join (
		select ap.PatientId
		from patientclinical pc
		inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
		where ap.patientid = @PatientId
		and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
		and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
		and Symptom in (''/CUP/DISC'')
		) cd on cd.PatientId = pc.PatientId 
	left join (
		select ap.PatientId
		from patientclinical pc
		inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
		where ap.patientid = @PatientId
		and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
		and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
		and Symptom in (''/OCT, OPTIC NERVE'')
		) ontesting on ontesting.PatientId = pc.PatientId 
	left join (
		select ap.PatientId
		from patientclinical pc
		inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
		where ap.patientid = @PatientId
		and CONVERT(date,ap.AppDate) <= CONVERT(date,GETDATE())
		and DATEDIFF (mm,ap.AppDate,getdate()) <= 12
		and ClinicalType = ''Q''
		and ImageDescriptor = ''L''
		and FindingDetail not in (''365.10'', ''365.11'', ''365.12'', ''365.15'')
		) onfindings on onfindings.PatientId = pc.PatientId
where pc.PatientId = @PatientId
AND model.GetAgeFromDob(DateOfBirth) >= 18
AND ClinicalType = ''Q'' 
AND Status = ''A'' 
AND FindingDetail in (''365.10'', ''365.11'', ''365.12'', ''365.15'')
AND (cd.PatientId is NULL OR onfindings.PatientId is NULL)
', N'')
GO
SET IDENTITY_INSERT [dbo].[IE_RulesPhrases] OFF
GO