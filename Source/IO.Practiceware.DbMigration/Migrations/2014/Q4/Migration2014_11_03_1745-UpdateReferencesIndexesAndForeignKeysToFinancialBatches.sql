﻿DELETE FROM model.Adjustments WHERE FinancialBatchId NOT IN (SELECT Id FROM model.FinancialBatches)
DELETE FROM model.FinancialInformations WHERE FinancialBatchId NOT IN (SELECT Id FROM model.FinancialBatches)

-- Adjustments Changes
IF EXISTS (
		SELECT *
		FROM sys.indexes
		WHERE NAME = N'IX_FK_FinancialBatchAdjustment'
			AND object_id = OBJECT_ID(N'model.Adjustments')
		)
	DROP INDEX [IX_FK_FinancialBatchAdjustment] ON [model].[Adjustments]
GO

IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS
		WHERE CONSTRAINT_NAME = 'FK_FinancialBatchAdjustment'
		)
	ALTER TABLE model.Adjustments

DROP CONSTRAINT FK_FinancialBatchAdjustment
GO

IF EXISTS (
		SELECT *
		FROM sys.columns
		WHERE NAME = N'FinancialBatchId'
			AND Object_ID = Object_ID(N'model.Adjustments')
			AND is_nullable = 1
		)
	ALTER TABLE model.Adjustments

ALTER COLUMN FinancialBatchId INT NOT NULL
GO

IF NOT EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS
		WHERE CONSTRAINT_NAME = 'FK_FinancialBatchAdjustment'
		)
	ALTER TABLE model.Adjustments ADD CONSTRAINT FK_FinancialBatchAdjustment FOREIGN KEY (FinancialBatchId) REFERENCES model.FinancialBatches (Id)
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.indexes
		WHERE NAME = N'IX_FK_FinancialBatchAdjustment'
			AND object_id = OBJECT_ID(N'model.Adjustments')
		)
BEGIN
	CREATE NONCLUSTERED INDEX [IX_FK_FinancialBatchAdjustment] ON [model].[Adjustments] ([FinancialBatchId] ASC)
		WITH (
				PAD_INDEX = OFF
				,STATISTICS_NORECOMPUTE = OFF
				,SORT_IN_TEMPDB = OFF
				,DROP_EXISTING = OFF
				,ONLINE = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				,FILLFACTOR = 90
				) ON [PRIMARY]
END
GO







-- Financial Informations Changes
IF EXISTS (
		SELECT *
		FROM sys.indexes
		WHERE NAME = N'IX_FK_FinancialBatchFinancialInformation'
			AND object_id = OBJECT_ID(N'model.FinancialInformations')
		)
	DROP INDEX [IX_FK_FinancialBatchFinancialInformation] ON [model].[FinancialInformations]
GO

IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS
		WHERE CONSTRAINT_NAME = 'FK_FinancialBatchFinancialInformation'
		)
	ALTER TABLE model.FinancialInformations

DROP CONSTRAINT FK_FinancialBatchFinancialInformation
GO

IF EXISTS (
		SELECT *
		FROM sys.columns
		WHERE NAME = N'FinancialBatchId'
			AND Object_ID = Object_ID(N'model.FinancialInformations')
			AND is_nullable = 1
		)
	ALTER TABLE model.FinancialInformations
	ALTER COLUMN FinancialBatchId INT NOT NULL
GO

IF NOT EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS
		WHERE CONSTRAINT_NAME = 'FK_FinancialBatchFinancialInformation'
		)
	ALTER TABLE model.FinancialInformations ADD CONSTRAINT FK_FinancialBatchFinancialInformation FOREIGN KEY (FinancialBatchId) REFERENCES model.FinancialBatches (Id)
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.indexes
		WHERE NAME = N'IX_FK_FinancialBatchFinancialInformation'
			AND object_id = OBJECT_ID(N'model.FinancialInformations')
		)
BEGIN
	CREATE NONCLUSTERED INDEX [IX_FK_FinancialBatchFinancialInformation] ON [model].[FinancialInformations] ([FinancialBatchId] ASC)
		WITH (
				PAD_INDEX = OFF
				,STATISTICS_NORECOMPUTE = OFF
				,SORT_IN_TEMPDB = OFF
				,DROP_EXISTING = OFF
				,ONLINE = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				,FILLFACTOR = 90
				) ON [PRIMARY]
END
GO
