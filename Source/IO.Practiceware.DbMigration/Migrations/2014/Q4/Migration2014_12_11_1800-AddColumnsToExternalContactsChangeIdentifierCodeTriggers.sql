﻿-- Create a new column on model.ExternalContacts named LegacyNpi, nullable nvarchar(max) column.
IF NOT EXISTS (		
SELECT *
		FROM sys.columns sc
		INNER JOIN sys.tables st ON sc.Object_Id = st.Object_Id
		INNER JOIN sys.schemas sch ON st.schema_id = sch.schema_id
		WHERE sc.NAME = 'LegacyNpi'
			AND st.NAME = 'ExternalContacts'
			AND sch.name = 'model'
		)
BEGIN
	ALTER TABLE model.ExternalContacts ADD LegacyNpi NVARCHAR(max) NULL 
END
GO

-- Create a new column on model.ExternalContacts named LegacyTaxonomy, nullable nvarchar(max) column.
IF NOT EXISTS (		
SELECT *
		FROM sys.columns sc
		INNER JOIN sys.tables st ON sc.Object_Id = st.Object_Id
		INNER JOIN sys.schemas sch ON st.schema_id = sch.schema_id
		WHERE sc.NAME = 'LegacyTaxonomy'
			AND st.NAME = 'ExternalContacts'
			AND sch.name = 'model'
		)
BEGIN
	ALTER TABLE model.ExternalContacts ADD LegacyTaxonomy NVARCHAR(max) NULL 
END
GO

-- Populate these columns during the migration from dbo.PracticeVendors.VendorNPI and VendorTaxonomy
UPDATE model.ExternalContacts 
SET LegacyNpi = VendorNpi, 
	LegacyTaxonomy = VendorTaxonomy
FROM dbo.PracticeVendors pv
INNER JOIN model.ExternalContacts ec ON ec.Id = pv.VendorId
GO