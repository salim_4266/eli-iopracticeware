﻿IF OBJECT_ID('dbo.NewPendingClaims', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[NewPendingClaims]

SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[NewPendingClaims] @SDate VARCHAR(8)
	,@EDate VARCHAR(8)
	,@RscId INT
	,@LocId INT
	,@RType VARCHAR(1)
AS
SET NOCOUNT ON

DECLARE @StartDate VARCHAR(8)
	,@EndDate VARCHAR(8)
	,@ResourceId INT
	,@LocationId INT
	,@ResourceType VARCHAR(1)

SET @StartDate = @SDate
SET @EndDate = @EDate
SET @ResourceId = @RscId
SET @LocationId = @LocId
SET @ResourceType = @RType;

WITH bstsExists
AS (
	SELECT TOP 100 PERCENT InvoiceReceivableId
		,COUNT(Id) AS NumOccurancesOfThisInvoiceReceivableId
	FROM model.BillingServiceTransactions
	GROUP BY InvoiceReceivableId
	ORDER BY InvoiceReceivableId
	)
SELECT CONVERT(NVARCHAR(8), i.LegacyDateTime, 112) AS ServiceDate
	,ap.PatientId
	,CASE 
		WHEN (ip.PolicyHolderPatientId IS NULL)
			THEN ap.PatientId
		ELSE ip.PolicyHolderPatientId
		END AS InsuredId
	,ap.ResourceId2
	,ap.ResourceId1
	,ap.ReferralId
	,CASE 
		WHEN (ip.InsurerId IS NULL)
			THEN 0
		ELSE ip.InsurerId
		END AS InsurerId
	,bs.LegacyPatientReceivableServicesLinkedDiag AS LinkedDiag
	,es.Code AS [Service]
	,COALESCE(STUFF((
				SELECT sm.Code AS [text()]
				FROM model.BillingServiceModifiers mo
				INNER JOIN model.ServiceModifiers sm ON sm.Id = mo.ServiceModifierId
				WHERE mo.BillingServiceId = m.BillingServiceId
				ORDER BY mo.OrdinalId ASC
				FOR XML PATH('')
				), 1, 0, ''), '') AS Modifier
	,bs.Unit AS Quantity
	,bs.UnitCharge AS Charge
	,CASE 
		WHEN (
				(i.ReferringExternalProviderId IS NULL)
				OR (i.ReferringExternalProviderId = '')
				)
			THEN 0
		ELSE i.ReferringExternalProviderId
		END AS ReferDr
	,ir.Id AS ReceivableId
	,ap.InvoiceNumber AS Invoice
	,CONVERT(NVARCHAR, DATEADD(n, ap.AppTime, CONVERT(DATETIME, ap.AppDate, 112)), 112) AS InvoiceDate
	,bs.Id
	,ap.AppointmentId
	,'A' AS [Status]
	,CASE 
		WHEN (ins.Name IS NULL)
			THEN 'ZZZ No insurance'
		ELSE Ins.Name
		END AS InsurerName
	,CONVERT(NVARCHAR(50), CASE 
			WHEN ia.Line1 <> ''
				AND ia.Line1 IS NOT NULL
				THEN ia.Line1
			ELSE ''
			END) AS InsurerAddress
FROM model.InvoiceReceivables ir
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
	AND i.InvoiceTypeId IN (
		1
		,3
		)
INNER JOIN model.Encounters e ON e.Id = i.EncounterId
INNER JOIN Appointments ap ON i.EncounterId = ap.AppointmentId
LEFT JOIN model.PatientInsurances [pi] ON [pi].Id = ir.PatientInsuranceId
LEFT JOIN model.InsurancePolicies ip ON ip.Id = [pi].InsurancePolicyId
LEFT JOIN model.Insurers ins ON ins.Id = ip.InsurerId
LEFT JOIN model.InsurerAddresses ia ON ia.InsurerId = ins.Id
LEFT JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
LEFT JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
LEFT JOIN model.BillingServiceModifiers m ON m.BillingServiceId = bs.Id
LEFT JOIN bstsExists ON bstsExists.InvoiceReceivableId = ir.Id
WHERE (ap.AppDate >= @StartDate)
	AND (ap.AppDate <= @EndDate)
	AND (
		(ap.ResourceId1 = @ResourceId)
		OR (@ResourceId = - 1)
		)
	AND (
		(ap.ResourceId2 = @LocationId)
		OR (@LocationId = - 1)
		)
	AND (
		(
			CASE 
				WHEN ir.OpenForReview = 0
					THEN CASE 
							WHEN bstsExists.NumOccurancesOfThisInvoiceReceivableId IS NOT NULL
								THEN 'B'
							ELSE 'S'
							END
				WHEN ir.OpenForReview = 1
					THEN 'I'
				END = @ResourceType
			)
		OR (@ResourceType = '0')
		)

UNION ALL

SELECT CONVERT(NVARCHAR(8), i.LegacyDateTime, 112) AS ServiceDate
	,apAsc.PatientId
	,CASE 
		WHEN (ip.PolicyHolderPatientId IS NULL)
			THEN ap.PatientId
		ELSE ip.PolicyHolderPatientId
		END AS InsuredId
	,apAsc.ResourceId2
	,apAsc.ResourceId1
	,apAsc.ReferralId
	,CASE 
		WHEN (ip.InsurerId IS NULL)
			THEN 0
		ELSE ip.InsurerId
		END AS InsurerId
	,bs.LegacyPatientReceivableServicesLinkedDiag AS LinkedDiag
	,es.Code AS [Service]
	,COALESCE(STUFF((
				SELECT sm.Code AS [text()]
				FROM model.BillingServiceModifiers mo
				INNER JOIN model.ServiceModifiers sm ON sm.Id = mo.ServiceModifierId
				WHERE mo.BillingServiceId = m.BillingServiceId
				ORDER BY mo.OrdinalId ASC
				FOR XML PATH('')
				), 1, 0, ''), '') AS Modifier
	,bs.Unit AS Quantity
	,bs.UnitCharge AS Charge
	,CASE 
		WHEN (
				(i.ReferringExternalProviderId IS NULL)
				OR (i.ReferringExternalProviderId = '')
				)
			THEN 0
		ELSE i.ReferringExternalProviderId
		END AS ReferDr
	,ir.Id AS ReceivableId
	,apAsc.InvoiceNumber AS Invoice
	,CONVERT(NVARCHAR, DATEADD(n, apAsc.AppTime, CONVERT(DATETIME, apAsc.AppDate, 112)), 112) AS InvoiceDate
	,bs.Id AS ItemId
	,apASC.AppointmentId
	,'A' AS [Status]
	,CASE 
		WHEN (ins.Name IS NULL)
			THEN 'ZZZ No insurance'
		ELSE Ins.Name
		END AS InsurerName
	,CONVERT(NVARCHAR(50), CASE 
			WHEN ia.Line1 <> ''
				AND ia.Line1 IS NOT NULL
				THEN ia.Line1
			ELSE ''
			END) AS InsurerAddress
FROM model.InvoiceReceivables ir
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
	AND i.InvoiceTypeId = 2
INNER JOIN model.Encounters e ON e.Id = i.EncounterId
INNER JOIN Appointments apASC ON apASC.AppointmentId <> apASC.EncounterId
	AND apASC.EncounterId = i.EncounterId
	AND apASC.Comments = 'ASC CLAIM'
INNER JOIN Appointments ap ON ap.EncounterId = apASC.EncounterId
	AND ap.EncounterId = ap.AppointmentId
LEFT JOIN model.PatientInsurances [pi] ON [pi].Id = ir.PatientInsuranceId
LEFT JOIN model.InsurancePolicies ip ON ip.Id = [pi].InsurancePolicyId
LEFT JOIN model.Insurers ins ON ins.Id = ip.InsurerId
LEFT JOIN model.InsurerAddresses ia ON ia.InsurerId = ins.Id
LEFT JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
LEFT JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
LEFT JOIN model.BillingServiceModifiers m ON m.BillingServiceId = bs.Id
LEFT JOIN bstsExists ON bstsExists.InvoiceReceivableId = ir.Id
WHERE (ap.AppDate >= @StartDate)
	AND (ap.AppDate <= @EndDate)
	AND (
		(ap.ResourceId1 = @ResourceId)
		OR (@ResourceId = - 1)
		)
	AND (
		(ap.ResourceId2 = @LocationId)
		OR (@LocationId = - 1)
		)
	AND (
		(
			CASE 
				WHEN ir.OpenForReview = 0
					THEN CASE 
							WHEN bstsExists.NumOccurancesOfThisInvoiceReceivableId IS NOT NULL
								THEN 'B'
							ELSE 'S'
							END
				WHEN ir.OpenForReview = 1
					THEN 'I'
				END = @ResourceType
			)
		OR (@ResourceType = '0')
		)
ORDER BY CONVERT(NVARCHAR, DATEADD(n, ap.AppTime, CONVERT(DATETIME, ap.AppDate, 112)), 112)
	,ap.PatientId

GO