﻿-- --------------------------------------------------
-- Delete Existing Views
-- --------------------------------------------------

IF OBJECT_ID(N'model.ExternalContactPhoneNumbers') IS NOT NULL
BEGIN
	exec dbo.DropObject 'model.ExternalContactPhoneNumbers'
END
GO

IF OBJECT_ID(N'model.ExternalContactAddresses') IS NOT NULL
BEGIN
	exec dbo.DropObject 'model.ExternalContactAddresses'
END
GO

IF OBJECT_ID(N'model.ExternalContacts') IS NOT NULL
BEGIN
	exec dbo.DropObject 'model.ExternalContacts'
END
GO

IF OBJECT_ID(N'model.ExternalOrganizationPhoneNumbers') IS NOT NULL
BEGIN
	exec dbo.DropObject 'model.ExternalOrganizationPhoneNumbers'
END
GO

IF OBJECT_ID(N'model.ExternalOrganizationAddresses') IS NOT NULL
BEGIN
	exec dbo.DropObject 'model.ExternalOrganizationAddresses'
END
GO

IF OBJECT_ID(N'model.ExternalOrganizations') IS NOT NULL
BEGIN
	exec dbo.DropObject 'model.ExternalOrganizations'
END
GO

IF OBJECT_ID(N'model.ExternalContactEmailAddresses') IS NOT NULL
BEGIN
	exec dbo.DropObject 'model.ExternalContactEmailAddresses'
END
GO

IF OBJECT_ID(N'model.ExternalOrganizationEmailAddresses') IS NOT NULL
BEGIN
	exec dbo.DropObject 'model.ExternalOrganizationEmailAddresses'
END
GO

IF OBJECT_ID(N'model.ExternalProviderClinicalSpecialtyTypes') IS NOT NULL
BEGIN
	exec dbo.DropObject 'model.ExternalProviderClinicalSpecialtyTypes'
END


-- --------------------------------------------------
-- Create corresponding tables
-- --------------------------------------------------
-- 'ExternalContactAddresses' does not exist, creating...
	CREATE TABLE [model].[ExternalContactAddresses] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [ExternalContactId] int  NOT NULL,
    [Line1] nvarchar(max)  NOT NULL,
    [Line2] nvarchar(max)  NULL,
    [Line3] nvarchar(max)  NULL,
    [City] nvarchar(max)  NOT NULL,
    [StateOrProvinceId] int  NOT NULL,
    [PostalCode] nvarchar(max)  NOT NULL,
    [ContactAddressTypeId] int  NOT NULL
);
GO

-- 'ExternalContactPhoneNumbers' does not exist, creating...
	CREATE TABLE [model].[ExternalContactPhoneNumbers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ExternalContactId] int  NOT NULL,
    [IsInternational] bit  NOT NULL,
    [CountryCode] nvarchar(max)  NULL,
    [AreaCode] nvarchar(max)  NULL,
    [ExchangeAndSuffix] nvarchar(max)  NOT NULL,
    [Extension] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL,
    [ExternalContactPhoneNumberTypeId] int  NOT NULL
);
GO

-- 'ExternalContacts' does not exist, creating...
	CREATE TABLE [model].[ExternalContacts] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ExternalOrganizationId] int  NULL,
    [FirstName] nvarchar(max)  NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [MiddleName] nvarchar(max)  NULL,
    [Suffix] nvarchar(max)  NULL,
    [Prefix] nvarchar(max)  NULL,
    [Honorific] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [DisplayName] nvarchar(max)  NOT NULL,
    [NickName] nvarchar(max)  NULL,
    [IsArchived] bit  NOT NULL,
    [IsEntity] bit  NOT NULL,
    [ExcludeOnClaim] bit  NULL,
    [OrdinalId] int  NOT NULL,
	[Comment] nvarchar(max) NULL,
    [__EntityType__] nvarchar(100)  NOT NULL
);
GO

-- 'ExternalOrganizationPhoneNumbers' does not exist, creating...
	CREATE TABLE [model].[ExternalOrganizationPhoneNumbers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ExternalOrganizationId] int  NOT NULL,
    [IsInternational] bit  NOT NULL,
    [CountryCode] nvarchar(max)  NULL,
    [AreaCode] nvarchar(max)  NULL,
    [ExchangeAndSuffix] nvarchar(max)  NOT NULL,
    [Extension] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL,
    [ExternalOrganizationPhoneNumberTypeId] int  NOT NULL
);
GO

-- 'ExternalOrganizationAddresses' does not exist, creating...
	CREATE TABLE [model].[ExternalOrganizationAddresses] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [ExternalOrganizationId] int  NOT NULL,
    [Line1] nvarchar(max)  NOT NULL,
    [Line2] nvarchar(max)  NULL,
    [Line3] nvarchar(max)  NULL,
    [City] nvarchar(max)  NOT NULL,
    [StateOrProvinceId] int  NOT NULL,
    [PostalCode] nvarchar(max)  NOT NULL,
    [ExternalOrganizationAddressTypeId] int  NOT NULL
);
GO

-- 'ExternalOrganizations' does not exist, creating...
	CREATE TABLE [model].[ExternalOrganizations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [DisplayName] nvarchar(max)  NOT NULL,
    [ExternalOrganizationTypeId] int  NULL,
    [IsArchived] bit  NOT NULL
);
GO

-- 'ExternalContactEmailAddresses' does not exist, creating...
	CREATE TABLE [model].[ExternalContactEmailAddresses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ExternalContactId] int  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [EmailAddressTypeId] int  NOT NULL,
    [OrdinalId] int  NOT NULL
);

-- 'ExternalOrganizationEmailAddresses' does not exist, creating...
	CREATE TABLE [model].[ExternalOrganizationEmailAddresses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ExternalOrganizationId] int  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [EmailAddressTypeId] int  NOT NULL,
    [OrdinalId] int  NOT NULL
);

-- 'ExternalProviderClinicalSpecialtyTypes does not exist, creating...
	CREATE TABLE model.ExternalProviderClinicalSpecialtyTypes (
	[Id] INT IDENTITY(1,1) NOT NULL,
	[OrdinalId] INT NOT NULL,
	[ExternalProviderId] INT NOT NULL,
	[ClinicalSpecialtyTypeId] INT NOT NULL
);

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'ExternalContacts'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ExternalContacts', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ExternalContacts] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ExternalContacts', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ExternalContacts]
ADD CONSTRAINT [PK_ExternalContacts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalOrganizations'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ExternalOrganizations', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ExternalOrganizations] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ExternalOrganizations', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ExternalOrganizations]
ADD CONSTRAINT [PK_ExternalOrganizations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalContactPhoneNumbers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ExternalContactPhoneNumbers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ExternalContactPhoneNumbers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ExternalContactPhoneNumbers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ExternalContactPhoneNumbers]
ADD CONSTRAINT [PK_ExternalContactPhoneNumbers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalOrganizationPhoneNumbers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ExternalOrganizationPhoneNumbers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ExternalOrganizationPhoneNumbers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ExternalOrganizationPhoneNumbers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ExternalOrganizationPhoneNumbers]
ADD CONSTRAINT [PK_ExternalOrganizationPhoneNumbers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalContactAddresses'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ExternalContactAddresses', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ExternalContactAddresses] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ExternalContactAddresses', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ExternalContactAddresses]
ADD CONSTRAINT [PK_ExternalContactAddresses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalOrganizationAddresses'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ExternalOrganizationAddresses', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ExternalOrganizationAddresses] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ExternalOrganizationAddresses', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ExternalOrganizationAddresses]
ADD CONSTRAINT [PK_ExternalOrganizationAddresses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalOrganizationEmailAddresses'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ExternalOrganizationEmailAddresses', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ExternalOrganizationEmailAddresses] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ExternalOrganizationEmailAddresses', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ExternalOrganizationEmailAddresses]
ADD CONSTRAINT [PK_ExternalOrganizationEmailAddresses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalContactEmailAddresses'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ExternalContactEmailAddresses', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ExternalContactEmailAddresses] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ExternalContactEmailAddresses', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ExternalContactEmailAddresses]
ADD CONSTRAINT [PK_ExternalContactEmailAddresses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalProviderClinicalSpecialtyTypes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ExternalProviderClinicalSpecialtyTypes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ExternalProviderClinicalSpecialtyTypes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ExternalProviderClinicalSpecialtyTypes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].ExternalProviderClinicalSpecialtyTypes
ADD CONSTRAINT [PK_ExternalProviderClinicalSpecialtyTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Insert Previous view values into tables
-- --------------------------------------------------
SET IDENTITY_INSERT model.ExternalOrganizations ON
INSERT INTO model.ExternalOrganizations (Id, DisplayName, ExternalOrganizationTypeId, Name, IsArchived)
SELECT VendorId AS Id,
	CASE 
		WHEN VendorFirmName <> '' AND VendorFirmName IS NOT NULL
			THEN VendorFirmName
		WHEN VendorName <> '' AND VendorName IS NOT NULL
			THEN VendorName
		WHEN VendorLastName <> '' AND VendorLastName IS NOT NULL
			THEN VendorLastName
			END AS DisplayName,
		CASE VendorType
			WHEN 'L'
				THEN 2
			WHEN 'H'
				THEN 3
			WHEN 'V'
				THEN 7
			ELSE 8
			END AS ExternalOrganizationTypeId,
		VendorName AS Name,
		CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeVendors pv
WHERE VendorType IN ('L', 'H', 'V')
	AND (
			(VendorFirmName <> '' AND VendorFirmName IS NOT NULL)
			OR (VendorName <> '' AND VendorName IS NOT NULL)
			OR (VendorLastName <> '' AND VendorLastName IS NOT NULL)
		)

SET IDENTITY_INSERT model.ExternalOrganizations OFF
GO

SET IDENTITY_INSERT model.ExternalContacts ON
INSERT INTO model.ExternalContacts (Id, DisplayName, ExternalOrganizationId, FirstName, Honorific,
									LastName, MiddleName, NickName, Prefix, Salutation, Suffix, IsArchived,
									ExcludeOnClaim, OrdinalId, __EntityType__, IsEntity)

	SELECT VendorId AS Id,
		VendorName AS DisplayName,
		eo.Id AS ExternalOrganizationId,
		VendorFirstName AS FirstName,
		VendorTitle AS Honorific,
		VendorLastName AS LastName,
		VendorMI AS MiddleName,
		VendorSalutation AS NickName,
		CONVERT(NVARCHAR, NULL) AS Prefix,
		VendorSalutation AS Salutation,
		CONVERT(NVARCHAR, NULL) AS Suffix,
		CONVERT(bit, 0) AS IsArchived,
		CASE  
			WHEN VendorExcRefDr = 'F' OR VendorExcRefDr IS NULL OR VendorExcRefDr = ''
				THEN CONVERT(bit, 0)
			WHEN VendorExcRefDr = 'T'
				THEN CONVERT(bit, 1)
			END AS ExcludeOnClaim,
		OrdinalId,
		CASE 
			WHEN VendorType = 'D'
				THEN 'ExternalProvider' 
			ELSE 'ExternalContact'
		END AS __EntityType__,
		CONVERT(BIT, 0)
	FROM dbo.PracticeVendors pv
	LEFT JOIN model.ExternalOrganizations eo ON pv.VendorId = eo.Id
	WHERE VendorLastName <> ''
		AND VendorLastName IS NOT NULL
		AND VendorType IN ('D', '~')
		

SET IDENTITY_INSERT model.ExternalContacts OFF
GO

INSERT INTO model.ExternalOrganizationPhoneNumbers(CountryCode, AreaCode, ExchangeAndSuffix, Extension, ExternalOrganizationId, 
												IsInternational ,OrdinalId, ExternalOrganizationPhoneNumberTypeId)
		----PhoneNumber ExternalOrganization phone
	SELECT CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE len(VendorPhone)
			WHEN 10
				THEN substring(VendorPhone, 1, 3)
				ELSE NULL
			END AS AreaCode

		,CASE len(VendorPhone)
			WHEN 10
				THEN substring(VendorPhone, 4, len(VendorPhone) - 3)
			ELSE VendorPhone
			END AS ExchangeAndSuffix

		,CONVERT(NVARCHAR, NULL) AS Extension
		,eo.Id AS ExternalOrganizationId
		,CONVERT(bit, 0) AS IsInternational
		,1 AS OrdinalId
		,pct.Id + 1000 AS ExternalOrganizationPhoneNumberTypeId
	FROM dbo.PracticeVendors pv
	INNER JOIN model.ExternalOrganizations eo ON eo.Id = pv.VendorId
	LEFT OUTER JOIN dbo.PracticeCodeTable pct 
		ON pct.Code = 'Primary'
		AND pct.ReferenceType = 'PHONETYPEEXTERNALORGANIZATION'
	WHERE VendorPhone <> ''
		AND VendorPhone IS NOT NULL
		AND VendorFirmName <> ''
		AND VendorFirmName IS NOT NULL
	
	UNION ALL
	
	----PhoneNumber ExternalOrganization fax
	SELECT CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE len(VendorFax)
			WHEN 10
				THEN substring(VendorFax, 1, 3)
				ELSE NULL
			END AS AreaCode
		,CASE len(VendorFax)
			WHEN 10
				THEN substring(VendorFax, 4, len(VendorFax) - 3)
			ELSE VendorFax
			END AS ExchangeAndSuffix
		,CONVERT(NVARCHAR, NULL) AS Extension
		,eo.Id AS ExternalOrganizationId
		,CONVERT(bit, 0) AS IsInternational
		,2 AS OrdinalId
		,pct.Id + 1000 AS ExternalOrganizationPhoneNumberTypeId
	FROM dbo.PracticeVendors pv
	INNER JOIN model.ExternalOrganizations eo ON eo.Id = pv.VendorId
	LEFT OUTER JOIN dbo.PracticeCodeTable pct 
		ON pct.Code = 'Fax'
		AND pct.ReferenceType = 'PHONETYPEEXTERNALORGANIZATION'
	WHERE VendorFax <> ''
		AND VendorFax IS NOT NULL
		AND VendorFirmName <> ''
		AND VendorFirmName IS NOT NULL


INSERT INTO model.ExternalOrganizationAddresses(City, ExternalOrganizationId, Line1, Line2, Line3, PostalCode,
												StateOrProvinceId, ExternalOrganizationAddressTypeId)
----ExternalOrganization address 1 for Doctors, Hosp, Labs, Vendors, Other (VendorType D, H, L, V, ~) 
	SELECT  pv.VendorCity AS City,
		eo.Id AS ExternalOrganizationId,
		pv.VendorAddress AS Line1,
		pv.VendorSuite AS Line2,
		CONVERT(NVARCHAR, NULL) AS Line3,
		REPLACE(pv.VendorZip,'-','') AS PostalCode,
		st.ID AS StateOrProvinceId,
		11 AS ExternalOrganizationAddressTypeId
	FROM dbo.PracticeVendors pv
	INNER JOIN model.ExternalOrganizations eo ON pv.VendorId = eo.Id
	INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pv.VendorState
		AND st.CountryId = 225
	WHERE pv.VendorFirmName <> ''
		AND pv.VendorFirmName IS NOT NULL
		AND pv.VendorAddress <> ''
		AND pv.VendorAddress IS NOT NULL
	
	UNION ALL
	
	----ExternalOrganization address 2 (Doctors only)
	SELECT  pv.VendorCity AS City,
		eo.Id AS ExternalOrganizationId,
		pv.VendorOtherOffice1 AS Line1,
		CONVERT(NVARCHAR, NULL) AS Line2,
		CONVERT(NVARCHAR, NULL) AS Line3,
		REPLACE(pv.VendorZip,'-','') AS PostalCode,
		st.ID AS StateOrProvinceId,
		at.Id AS ExternalOrganizationAddressTypeId
	FROM dbo.PracticeVendors pv
	INNER JOIN model.ExternalOrganizations eo ON pv.VendorId = eo.Id
	INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pv.VendorState
		AND st.CountryId = 225
	LEFT OUTER JOIN model.ExternalOrganizationAddressTypes at ON at.Name = 'Other1'
	WHERE pv.VendorType = 'D'
		AND pv.VendorFirmName <> ''
		AND pv.VendorFirmName IS NOT NULL
		AND pv.VendorOtherOffice1 <> ''
		AND pv.VendorOtherOffice1 IS NOT NULL
	
	UNION ALL
	
	----ExternalOrganization address 3 (Doctors only)
	SELECT pv.VendorCity AS City,
		eo.Id AS ExternalOrganizationId,
		pv.VendorOtherOffice2 AS Line1,
		CONVERT(NVARCHAR, NULL) AS Line2,
		CONVERT(NVARCHAR, NULL) AS Line3,
		REPLACE(pv.VendorZip,'-','') AS PostalCode,
		st.ID AS StateOrProvinceId,
		at.Id AS ExternalOrganizationAddressTypeId
	FROM dbo.PracticeVendors pv
	INNER JOIN model.ExternalOrganizations eo ON pv.VendorId = eo.Id
	INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pv.VendorState
		AND st.CountryId = 225
	LEFT OUTER JOIN model.ExternalOrganizationAddressTypes at ON at.Name = 'Other2'
	WHERE pv.VendorType = 'D'
		AND pv.VendorFirmName <> ''
		AND pv.VendorFirmName IS NOT NULL
		AND pv.VendorOtherOffice2 <> ''
		AND pv.VendorOtherOffice2 IS NOT NULL
GO

INSERT INTO model.ExternalContactAddresses(City, ExternalContactId, Line1, Line2, Line3, PostalCode,
												StateOrProvinceId, ContactAddressTypeId)
----Contact ExternalProvider address 1
	SELECT pv.VendorCity AS City,
		ec.Id AS ExternalContactId,
		pv.VendorAddress AS Line1,
		pv.VendorSuite AS Line2,
		CONVERT(NVARCHAR, NULL) AS Line3,
		REPLACE(pv.VendorZip,'-','') AS PostalCode,
		st.Id AS StateOrProvinceId,
		8 AS ContactAddressTypeId
	FROM dbo.PracticeVendors pv
	INNER JOIN model.ExternalContacts ec ON pv.VendorId = ec.Id
	INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pv.VendorState
		AND st.CountryId = 225
	WHERE pv.VendorType = 'D'
		AND pv.VendorAddress <> ''
		AND pv.VendorAddress IS NOT NULL
		AND pv.VendorCity <> ''
		AND pv.VendorCity IS NOT NULL
		AND pv.VendorLastName <> ''
		AND pv.VendorLastName IS NOT NULL
	
	UNION ALL
	
	----Contact ExternalProvider address 2
	SELECT pv.VendorCity AS City,
		ec.Id AS ExternalContactId,
		pv.VendorOtherOffice1 AS Line1,
		CONVERT(NVARCHAR, NULL) AS Line2,
		CONVERT(NVARCHAR, NULL) AS Line3,
		REPLACE(pv.VendorZip,'-','') AS PostalCode,
		st.Id AS StateOrProvinceId,
		at.Id AS ContactAddressTypeId
	FROM dbo.PracticeVendors pv
	INNER JOIN model.ExternalContacts ec ON pv.VendorId = ec.Id
	INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pv.VendorState
		AND st.CountryId = 225
	LEFT OUTER JOIN model.ExternalContactAddressTypes at ON at.Name = 'Other1'
	WHERE VendorType = 'D'
		AND pv.VendorOtherOffice1 <> ''
		AND pv.VendorOtherOffice1 IS NOT NULL
		AND pv.VendorLastName <> ''
		AND pv.VendorLastName IS NOT NULL
	
	UNION ALL
	----Contact ExternalProvider address3
	SELECT pv.VendorCity AS City,
		ec.Id AS ExternalContactId,
		pv.VendorOtherOffice2 AS Line1,
		CONVERT(NVARCHAR, NULL) AS Line2,
		CONVERT(NVARCHAR, NULL) AS Line3,
		REPLACE(pv.VendorZip,'-','') AS PostalCode,
		st.Id AS StateOrProvinceId,
		at.Id AS ContactAddressTypeId
	FROM dbo.PracticeVendors pv
	INNER JOIN model.ExternalContacts ec ON pv.VendorId = ec.Id
	INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pv.VendorState
		AND st.CountryId = 225
	LEFT OUTER JOIN model.ExternalContactAddressTypes at ON  at.Name = 'Other2'
		WHERE VendorType = 'D'
		AND pv.VendorOtherOffice2 <> ''
		AND pv.VendorOtherOffice2 IS NOT NULL
		AND pv.VendorLastName <> ''
		AND pv.VendorLastName IS NOT NULL
GO

INSERT INTO model.ExternalContactPhoneNumbers(ExternalContactId, CountryCode, AreaCode, ExchangeAndSuffix, Extension, 
												IsInternational ,OrdinalId, ExternalContactPhoneNumberTypeId)
----PhoneNumber Contact (all) PHONE
	SELECT ec.Id AS ExternalContactId
		,CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE len(VendorPhone)
			WHEN 10
				THEN substring(VendorPhone, 1, 3)
				ELSE NULL
			END AS AreaCode
		,CASE len(VendorPhone)
			WHEN 10
				THEN substring(VendorPhone, 4, len(VendorPhone) - 3)
			ELSE VendorPhone
			END AS ExchangeAndSuffix
		,CONVERT(NVARCHAR, NULL) AS Extension
		,CONVERT(bit, 0) AS IsInternational
		,1 AS OrdinalId
		,5 AS ExternalContactPhoneNumberTypeId
	FROM dbo.PracticeVendors pv
	INNER JOIN model.ExternalContacts ec ON ec.Id = pv.VendorId
	WHERE VendorPhone <> ''
		AND VendorPhone IS NOT NULL
		AND VendorLastName <> ''
		AND VendorLastName IS NOT NULL
		AND VendorType = 'D'
	
	UNION ALL
	
	----PhoneNumber Contact  (all) FAX
	SELECT ec.Id AS ExternalContactId
		,CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE len(VendorFax)
			WHEN 10
				THEN substring(VendorFax, 1, 3)
				ELSE NULL
			END AS AreaCode
		,CASE len(VendorFax)
			WHEN 10
				THEN substring(VendorFax, 4, len(VendorFax) - 3)
			ELSE VendorFax
			END AS ExchangeAndSuffix
		,CONVERT(NVARCHAR, NULL) AS Extension
		,CONVERT(bit, 0) AS IsInternational
		,2 AS OrdinalId
		,4 AS ExternalContactPhoneNumberTypeId
	FROM dbo.PracticeVendors pv
	INNER JOIN model.ExternalContacts ec ON ec.Id = pv.VendorId
	WHERE VendorFax <> ''
		AND VendorFax IS NOT NULL
	    AND VendorLastName <> ''
		AND VendorLastName IS NOT NULL
		AND VendorType = 'D'
	
	UNION ALL
	
	----PhoneNumber Contact  (all) CELL
	SELECT ec.Id AS ExternalContactId
		,CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE len(VendorCellPhone)
			WHEN 10
				THEN substring(VendorCellPhone, 1, 3)
				ELSE NULL
			END AS AreaCode
		,CASE len(VendorCellPhone)
			WHEN 10
				THEN substring(VendorCellPhone, 4, len(VendorCellPhone) - 3)
			ELSE VendorCellPhone
			END AS ExchangeAndSuffix
		,CONVERT(NVARCHAR, NULL) AS Extension
		,CONVERT(bit, 0) AS IsInternational
		,3 AS OrdinalId
		,pct.Id + 1000 AS ExternalContactPhoneNumberTypeId
	FROM dbo.PracticeVendors pv
	INNER JOIN model.ExternalContacts ec ON ec.Id = pv.VendorId
	LEFT OUTER JOIN dbo.PracticeCodeTable pct 
		ON pct.Code = 'Cell'
		AND pct.ReferenceType = 'PHONETYPECONTACT'
	WHERE VendorCellPhone <> ''
		AND VendorCellPhone IS NOT NULL
		AND VendorLastName <> ''
		AND VendorLastName IS NOT NULL
		AND VendorType = 'D'
GO
		
INSERT INTO model.ExternalOrganizationEmailAddresses(ExternalOrganizationId, OrdinalId, Value, EmailAddressTypeId)
----EmailAddress ExternalOrganization
SELECT eo.Id AS ExternalContactId,
	0 AS OrdinalId,
	VendorEmail AS Value,
	1 AS EmailAddressTypeId
FROM dbo.PracticeVendors pv
INNER JOIN model.ExternalOrganizations eo ON eo.Id = pv.VendorId
WHERE VendorEmail <> ''
	AND VendorEmail IS NOT NULL
	AND VendorFirmName <> ''
	AND VendorFirmName IS NOT NULL
GO


INSERT INTO model.ExternalContactEmailAddresses(ExternalContactId, OrdinalId, Value, EmailAddressTypeId)
----EmailAddress Contact-ExternalProvider
SELECT ec.Id AS ExternalContactId,
	0 AS OrdinalId,
	VendorEmail AS Value,
	1 AS EmailAddressTypeId
FROM dbo.PracticeVendors pv
INNER JOIN model.ExternalContacts ec ON ec.Id = pv.VendorId
WHERE VendorEmail <> ''
	AND VendorEmail IS NOT NULL
	AND VendorLastName <> ''
	AND VendorLastName IS NOT NULL
	AND VendorType = 'D'
GO

-- model.ExternalProviderClinicalSpecialtyTypes

INSERT INTO model.ExternalProviderClinicalSpecialtyTypes  (OrdinalId, ExternalProviderId, ClinicalSpecialtyTypeId)
SELECT 
	v.OrdinalId AS OrdinalId,
	VendorId AS ExternalProviderId,
	v.ClinicalSpecialtyTypeId AS ClinicalSpecialtyTypeId
FROM (SELECT
		1 AS OrdinalId,
		VendorId,
		pct.id AS ClinicalSpecialtyTypeId,
		COUNT_BIG(*) AS Count
	FROM dbo.PracticeVendors pv
	LEFT JOIN dbo.PracticeCodeTable pct ON pv.VendorSpecialty = pct.Code
		AND pct.ReferenceType = 'SPECIALTY'
	WHERE pct.Id <> ''
		AND pct.Id IS NOT NULL
		AND VendorLastName <> ''
		AND VendorLastName IS NOT NULL
		AND VendorType = 'D'
	GROUP BY VendorId,
		pct.Id

	UNION ALL

----PracticeVendorsBackup specialty #2

	SELECT
		2 AS OrdinalId,
		VendorId AS VendorId,
		pct.id AS ClinicalSpecialtyTypeId,
		COUNT_BIG(*) AS Count
	FROM dbo.PracticeVendors pv
	LEFT JOIN dbo.PracticeCodeTable pct ON pv.VendorSpecOther = pct.Code
		AND pct.ReferenceType = 'SPECIALTY'
	WHERE pct.Id <> ''
		AND pct.Id IS NOT NULL
		AND VendorLastName <> ''
		AND VendorLastName IS NOT NULL
		AND VendorType = 'D'
	GROUP BY VendorId,
		pct.Id
	) AS v

GO

UPDATE model.EncounterLaboratoryTestOrders
SET ExternalOrganizationId = NULL
WHERE ExternalOrganizationId NOT IN (SELECT ID FROM model.ExternalOrganizations)

UPDATE model.ExternalContacts
SET ExternalOrganizationId = NULL
WHERE ExternalOrganizationId NOT IN (SELECT ID FROM model.ExternalOrganizations)

DELETE FROM model.ExternalContactAddresses WHERE ExternalContactId NOT IN (SELECT Id FROM model.ExternalContacts)

DELETE FROM model.ExternalOrganizationAddresses WHERE ExternalOrganizationId NOT IN (SELECT Id FROM model.ExternalOrganizations)

DELETE FROM model.ExternalContactPhoneNumbers WHERE ExternalContactId NOT IN (SELECT Id FROM model.ExternalContacts)

DELETE FROM model.ExternalOrganizationPhoneNumbers WHERE ExternalOrganizationId NOT IN (SELECT Id FROM model.ExternalOrganizations)

DELETE FROM model.ExternalContactEmailAddresses WHERE ExternalContactId NOT IN (SELECT Id FROM model.ExternalContacts)

DELETE FROM model.ExternalOrganizationEmailAddresses WHERE ExternalOrganizationId NOT IN (SELECT Id FROM model.ExternalOrganizations)

DELETE FROM model.ExternalProviderClinicalSpecialtyTypes WHERE ExternalProviderId NOT IN (SELECT Id FROM model.ExternalContacts)

UPDATE model.CommunicationTransactions
SET ExternalProviderId = NULL
WHERE ExternalProviderId NOT IN (SELECT ID FROM model.ExternalContacts)

UPDATE model.Invoices
SET ReferringExternalProviderId = NULL
WHERE ReferringExternalProviderId NOT IN (SELECT ID FROM model.ExternalContacts)

UPDATE model.BillingServices
SET OrderingExternalProviderId = NULL
WHERE OrderingExternalProviderId NOT IN (SELECT ID FROM model.ExternalContacts)

DELETE FROM model.PatientExternalProviders WHERE ExternalProviderId NOT IN (SELECT Id FROM model.ExternalContacts)

UPDATE model.PatientInsuranceReferrals
SET ExternalProviderId = NULL
WHERE ExternalProviderId NOT IN (SELECT ID FROM model.ExternalContacts)

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------


IF EXISTS(SELECT * FROM sys.Tables WHERE name = 'EncounterLaboratoryTestOrders')
BEGIN
	-- Creating foreign key on [ExternalOrganizationId] in table 'EncounterLaboratoryTestOrders'
	IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterLaboratoryTestOrderExternalOrganization'
		AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
		ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT FK_EncounterLaboratoryTestOrderExternalOrganization

	IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalOrganizations]'), 'IsUserTable') = 1
	ALTER TABLE [model].[EncounterLaboratoryTestOrders]
	ADD CONSTRAINT [FK_EncounterLaboratoryTestOrderExternalOrganization]
		FOREIGN KEY ([ExternalOrganizationId])
		REFERENCES [model].[ExternalOrganizations]
			([Id])
		ON DELETE NO ACTION ON UPDATE NO ACTION;
END

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterLaboratoryTestOrderExternalOrganization'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_EncounterLaboratoryTestOrderExternalOrganization') 
	DROP INDEX IX_FK_EncounterLaboratoryTestOrderExternalOrganization ON [model].[EncounterLaboratoryTestOrders]
GO

IF EXISTS(SELECT * FROM sys.Tables WHERE name = 'EncounterLaboratoryTestOrders')
BEGIN
	CREATE INDEX [IX_FK_EncounterLaboratoryTestOrderExternalOrganization]
	ON [model].[EncounterLaboratoryTestOrders]
		([ExternalOrganizationId]);
END
GO

-- Creating foreign key on [ExternalOrganizationTypeId] in table 'ExternalOrganizations'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalOrganizationTypeExternalOrganization'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalOrganizations] DROP CONSTRAINT FK_ExternalOrganizationTypeExternalOrganization

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalOrganizationTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalOrganizations]
ADD CONSTRAINT [FK_ExternalOrganizationTypeExternalOrganization]
    FOREIGN KEY ([ExternalOrganizationTypeId])
    REFERENCES [model].[ExternalOrganizationTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalOrganizationTypeExternalOrganization'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalOrganizationTypeExternalOrganization')
	DROP INDEX IX_FK_ExternalOrganizationTypeExternalOrganization ON [model].[ExternalOrganizations]
GO
CREATE INDEX [IX_FK_ExternalOrganizationTypeExternalOrganization]
ON [model].[ExternalOrganizations]
    ([ExternalOrganizationTypeId]);
GO

-- Creating foreign key on [ExternalOrganizationId] in table 'ExternalContacts'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalOrganizationContact'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalContacts] DROP CONSTRAINT FK_ExternalOrganizationContact

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalOrganizations]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalContacts]
ADD CONSTRAINT [FK_ExternalOrganizationContact]
    FOREIGN KEY ([ExternalOrganizationId])
    REFERENCES [model].[ExternalOrganizations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
	
-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalOrganizationContact'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalOrganizationContact')
	DROP INDEX IX_FK_ExternalOrganizationContact ON [model].[ExternalContacts]
GO
CREATE INDEX [IX_FK_ExternalOrganizationContact]
ON [model].[ExternalContacts]
    ([ExternalOrganizationId]);
GO

-- Creating foreign key on [ExternalContactId] in table 'ExternalContactAddresses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalContactExternalContactAddress'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalContactAddresses] DROP CONSTRAINT FK_ExternalContactExternalContactAddress

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContacts]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalContactAddresses]
ADD CONSTRAINT [FK_ExternalContactExternalContactAddress]
    FOREIGN KEY ([ExternalContactId])
    REFERENCES [model].[ExternalContacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalContactExternalContactAddress'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalContactExternalContactAddress')
	DROP INDEX IX_FK_ExternalContactExternalContactAddress ON [model].[ExternalContactAddresses]
GO
CREATE INDEX [IX_FK_ExternalContactExternalContactAddress]
ON [model].[ExternalContactAddresses]
    ([ExternalContactId]);
GO

-- Creating foreign key on [ExternalOrganizationId] in table 'ExternalOrganizationAddresses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalOrganizationExternalOrganizationAddress'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalOrganizationAddresses] DROP CONSTRAINT FK_ExternalOrganizationExternalOrganizationAddress

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalOrganizations]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalOrganizationAddresses]
ADD CONSTRAINT [FK_ExternalOrganizationExternalOrganizationAddress]
    FOREIGN KEY ([ExternalOrganizationId])
    REFERENCES [model].[ExternalOrganizations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalOrganizationExternalOrganizationAddress'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalOrganizationExternalOrganizationAddress')
	DROP INDEX IX_FK_ExternalOrganizationExternalOrganizationAddress ON [model].[ExternalOrganizationAddresses]
GO
CREATE INDEX [IX_FK_ExternalOrganizationExternalOrganizationAddress]
ON [model].[ExternalOrganizationAddresses]
    ([ExternalOrganizationId]);
GO

-- Creating foreign key on [ExternalContactId] in table 'ExternalContactPhoneNumbers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalContactExternalContactPhoneNumber'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalContactPhoneNumbers] DROP CONSTRAINT FK_ExternalContactExternalContactPhoneNumber

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContacts]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalContactPhoneNumbers]
ADD CONSTRAINT [FK_ExternalContactExternalContactPhoneNumber]
    FOREIGN KEY ([ExternalContactId])
    REFERENCES [model].[ExternalContacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalContactExternalContactPhoneNumber'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalContactExternalContactPhoneNumber')
	DROP INDEX IX_FK_ExternalContactExternalContactPhoneNumber ON [model].[ExternalContactPhoneNumbers]
GO
CREATE INDEX [IX_FK_ExternalContactExternalContactPhoneNumber]
ON [model].[ExternalContactPhoneNumbers]
    ([ExternalContactId]);
GO

-- Creating foreign key on [ExternalOrganizationId] in table 'ExternalOrganizationPhoneNumbers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalOrganizationExternalOrganizationPhoneNumber'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalOrganizationPhoneNumbers] DROP CONSTRAINT FK_ExternalOrganizationExternalOrganizationPhoneNumber

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalOrganizations]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalOrganizationPhoneNumbers]
ADD CONSTRAINT [FK_ExternalOrganizationExternalOrganizationPhoneNumber]
    FOREIGN KEY ([ExternalOrganizationId])
    REFERENCES [model].[ExternalOrganizations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalOrganizationExternalOrganizationPhoneNumber'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalOrganizationExternalOrganizationPhoneNumber')
	DROP INDEX IX_FK_ExternalOrganizationExternalOrganizationPhoneNumber ON [model].[ExternalOrganizationPhoneNumbers]
GO
CREATE INDEX [IX_FK_ExternalOrganizationExternalOrganizationPhoneNumber]
ON [model].[ExternalOrganizationPhoneNumbers]
    ([ExternalOrganizationId]);
GO

-- Creating foreign key on [ExternalContactPhoneNumberTypeId] in table 'ExternalContactPhoneNumbers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalContactPhoneNumberTypeExternalContactPhoneNumber'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalContactPhoneNumbers] DROP CONSTRAINT FK_ExternalContactPhoneNumberTypeExternalContactPhoneNumber

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContactPhoneNumberTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalContactPhoneNumbers]
ADD CONSTRAINT [FK_ExternalContactPhoneNumberTypeExternalContactPhoneNumber]
    FOREIGN KEY ([ExternalContactPhoneNumberTypeId])
    REFERENCES [model].[ExternalContactPhoneNumberTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalContactPhoneNumberTypeExternalContactPhoneNumber'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalContactPhoneNumberTypeExternalContactPhoneNumber')
	DROP INDEX IX_FK_ExternalContactPhoneNumberTypeExternalContactPhoneNumber ON [model].[ExternalContactPhoneNumbers]
GO
CREATE INDEX [IX_FK_ExternalContactPhoneNumberTypeExternalContactPhoneNumber]
ON [model].[ExternalContactPhoneNumbers]
    ([ExternalContactPhoneNumberTypeId]);
GO

-- Creating foreign key on [ExternalOrganizationPhoneNumberTypeId] in table 'ExternalOrganizationPhoneNumbers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalOrganizationPhoneNumberTypeExternalOrganizationPhoneNumber'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalOrganizationPhoneNumbers] DROP CONSTRAINT FK_ExternalOrganizationPhoneNumberTypeExternalOrganizationPhoneNumber

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalOrganizationPhoneNumberTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalOrganizationPhoneNumbers]
ADD CONSTRAINT [FK_ExternalOrganizationPhoneNumberTypeExternalOrganizationPhoneNumber]
    FOREIGN KEY ([ExternalOrganizationPhoneNumberTypeId])
    REFERENCES [model].[ExternalOrganizationPhoneNumberTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalOrganizationPhoneNumberTypeExternalOrganizationPhoneNumber'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalOrganizationPhoneNumberTypeExternalOrganizationPhoneNumber')
	DROP INDEX IX_FK_ExternalOrganizationPhoneNumberTypeExternalOrganizationPhoneNumber ON [model].[ExternalOrganizationPhoneNumbers]
GO
CREATE INDEX [IX_FK_ExternalOrganizationPhoneNumberTypeExternalOrganizationPhoneNumber]
ON [model].[ExternalOrganizationPhoneNumbers]
    ([ExternalOrganizationPhoneNumberTypeId]);
GO

-- Creating foreign key on [ExternalOrganizationAddressTypeId] in table 'ExternalOrganizationAddresses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalOrganizationAddressTypeExternalOrganizationAddress'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalOrganizationAddresses] DROP CONSTRAINT FK_ExternalOrganizationAddressTypeExternalOrganizationAddress

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalOrganizationAddressTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalOrganizationAddresses]
ADD CONSTRAINT [FK_ExternalOrganizationAddressTypeExternalOrganizationAddress]
    FOREIGN KEY ([ExternalOrganizationAddressTypeId])
    REFERENCES [model].[ExternalOrganizationAddressTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalOrganizationAddressTypeExternalOrganizationAddress'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalOrganizationAddressTypeExternalOrganizationAddress')
	DROP INDEX IX_FK_ExternalOrganizationAddressTypeExternalOrganizationAddress ON [model].[ExternalOrganizationAddresses]
GO
CREATE INDEX [IX_FK_ExternalOrganizationAddressTypeExternalOrganizationAddress]
ON [model].[ExternalOrganizationAddresses]
    ([ExternalOrganizationAddressTypeId]);
GO

-- Creating foreign key on [ContactAddressTypeId] in table 'ExternalContactAddresses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalContactAddressTypeExternalContactAddress'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalContactAddresses] DROP CONSTRAINT FK_ExternalContactAddressTypeExternalContactAddress

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContactAddressTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalContactAddresses]
ADD CONSTRAINT [FK_ExternalContactAddressTypeExternalContactAddress]
    FOREIGN KEY ([ContactAddressTypeId])
    REFERENCES [model].[ExternalContactAddressTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalContactAddressTypeExternalContactAddress'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalContactAddressTypeExternalContactAddress')
	DROP INDEX IX_FK_ExternalContactAddressTypeExternalContactAddress ON [model].[ExternalContactAddresses]
GO
CREATE INDEX [IX_FK_ExternalContactAddressTypeExternalContactAddress]
ON [model].[ExternalContactAddresses]
    ([ContactAddressTypeId]);
GO

-- Creating foreign key on [StateOrProvinceId] in table 'ExternalContactAddresses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalContactAddressStateOrProvince'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalContactAddresses] DROP CONSTRAINT FK_ExternalContactAddressStateOrProvince

IF OBJECTPROPERTY(OBJECT_ID('[model].[StateOrProvinces]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalContactAddresses]
ADD CONSTRAINT [FK_ExternalContactAddressStateOrProvince]
    FOREIGN KEY ([StateOrProvinceId])
    REFERENCES [model].[StateOrProvinces]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalContactAddressStateOrProvince'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalContactAddressStateOrProvince')
	DROP INDEX IX_FK_ExternalContactAddressStateOrProvince ON [model].[ExternalContactAddresses]
GO
CREATE INDEX [IX_FK_ExternalContactAddressStateOrProvince]
ON [model].[ExternalContactAddresses]
    ([StateOrProvinceId]);
GO

-- Creating foreign key on [StateOrProvinceId] in table 'ExternalOrganizationAddresses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalOrganizationAddressStateOrProvince'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalOrganizationAddresses] DROP CONSTRAINT FK_ExternalOrganizationAddressStateOrProvince

IF OBJECTPROPERTY(OBJECT_ID('[model].[StateOrProvinces]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalOrganizationAddresses]
ADD CONSTRAINT [FK_ExternalOrganizationAddressStateOrProvince]
    FOREIGN KEY ([StateOrProvinceId])
    REFERENCES [model].[StateOrProvinces]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalOrganizationAddressStateOrProvince'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalOrganizationAddressStateOrProvince')
	DROP INDEX IX_FK_ExternalOrganizationAddressStateOrProvince ON [model].[ExternalOrganizationAddresses]
GO
CREATE INDEX [IX_FK_ExternalOrganizationAddressStateOrProvince]
ON [model].[ExternalOrganizationAddresses]
    ([StateOrProvinceId]);
GO

-- Creating foreign key on [ExternalOrganizationId] in table 'ExternalOrganizationEmailAddresses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalOrganizationExternalOrganizationEmailAddress'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalOrganizationEmailAddresses] DROP CONSTRAINT FK_ExternalOrganizationExternalOrganizationEmailAddress

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalOrganizations]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalOrganizationEmailAddresses]
ADD CONSTRAINT [FK_ExternalOrganizationExternalOrganizationEmailAddress]
    FOREIGN KEY ([ExternalOrganizationId])
    REFERENCES [model].[ExternalOrganizations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalOrganizationExternalOrganizationEmailAddress'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalOrganizationExternalOrganizationEmailAddress')
	DROP INDEX IX_FK_ExternalOrganizationExternalOrganizationEmailAddress ON [model].[ExternalOrganizationEmailAddresses]
GO
CREATE INDEX [IX_FK_ExternalOrganizationExternalOrganizationEmailAddress]
ON [model].[ExternalOrganizationEmailAddresses]
    ([ExternalOrganizationId]);
GO

-- Creating foreign key on [ExternalContactId] in table 'ExternalContactEmailAddresses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalContactExternalContactEmailAddress'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalContactEmailAddresses] DROP CONSTRAINT FK_ExternalContactExternalContactEmailAddress

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContacts]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalContactEmailAddresses]
ADD CONSTRAINT [FK_ExternalContactExternalContactEmailAddress]
    FOREIGN KEY ([ExternalContactId])
    REFERENCES [model].[ExternalContacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalContactExternalContactEmailAddress'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalContactExternalContactEmailAddress')
	DROP INDEX IX_FK_ExternalContactExternalContactEmailAddress ON [model].[ExternalContactEmailAddresses]
GO
CREATE INDEX [IX_FK_ExternalContactExternalContactEmailAddress]
ON [model].[ExternalContactEmailAddresses]
    ([ExternalContactId]);
GO

-- Creating foreign key on [ExternalProviderId] in table 'ExternalProviderClinicalSpecialtyTypes'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalProviderExternalProviderClinicalSpecialtyType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalProviderClinicalSpecialtyTypes] DROP CONSTRAINT FK_ExternalProviderExternalProviderClinicalSpecialtyType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalProviders]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalProviderClinicalSpecialtyTypes]
ADD CONSTRAINT [FK_ExternalProviderExternalProviderClinicalSpecialtyType]
    FOREIGN KEY ([ExternalProviderId])
    REFERENCES [model].[ExternalProviders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalProviderExternalProviderClinicalSpecialtyType'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalProviderExternalProviderClinicalSpecialtyType')
	DROP INDEX IX_FK_ExternalProviderExternalProviderClinicalSpecialtyType ON [model].[ExternalProviderClinicalSpecialtyTypes]
GO
CREATE INDEX [IX_FK_ExternalProviderExternalProviderClinicalSpecialtyType]
ON [model].[ExternalProviderClinicalSpecialtyTypes]
    ([ExternalProviderId]);
GO

-- Creating foreign key on [ClinicalSpecialtyTypeId] in table 'ExternalProviderClinicalSpecialtyTypes'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ClinicalSpecialtyTypeExternalProviderClinicalSpecialtyType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ExternalProviderClinicalSpecialtyTypes] DROP CONSTRAINT FK_ClinicalSpecialtyTypeExternalProviderClinicalSpecialtyType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalSpecialtyTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[ExternalProviderClinicalSpecialtyTypes]
ADD CONSTRAINT [FK_ClinicalSpecialtyTypeExternalProviderClinicalSpecialtyType]
    FOREIGN KEY ([ClinicalSpecialtyTypeId])
    REFERENCES [model].[ClinicalSpecialtyTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalProviderExternalProviderClinicalSpecialtyType'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ClinicalSpecialtyTypeExternalProviderClinicalSpecialtyType')
	DROP INDEX IX_FK_ClinicalSpecialtyTypeExternalProviderClinicalSpecialtyType ON [model].[ExternalProviderClinicalSpecialtyTypes]
GO
CREATE INDEX [IX_FK_ClinicalSpecialtyTypeExternalProviderClinicalSpecialtyType]
ON [model].[ExternalProviderClinicalSpecialtyTypes]
    ([ClinicalSpecialtyTypeId]);
GO


-- Creating foreign key on [ExternalProviderId] in table 'CommunicationTransactions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_CommunicationTransactionExternalProvider'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[CommunicationTransactions] DROP CONSTRAINT FK_CommunicationTransactionExternalProvider

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContacts]'), 'IsUserTable') = 1
ALTER TABLE [model].[CommunicationTransactions]
ADD CONSTRAINT [FK_CommunicationTransactionExternalProvider]
    FOREIGN KEY ([ExternalProviderId])
    REFERENCES [model].[ExternalContacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CommunicationTransactionExternalProvider'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_CommunicationTransactionExternalProvider')
	DROP INDEX IX_FK_CommunicationTransactionExternalProvider ON [model].[CommunicationTransactions]
GO
CREATE INDEX [IX_FK_CommunicationTransactionExternalProvider]
ON [model].[CommunicationTransactions]
    ([ExternalProviderId]);
GO


-- Creating foreign key on [ReferringExternalProviderId] in table 'Invoices'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InvoiceExternalProvider'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Invoices] DROP CONSTRAINT FK_InvoiceExternalProvider

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContacts]'), 'IsUserTable') = 1
ALTER TABLE [model].[Invoices]
ADD CONSTRAINT [FK_InvoiceExternalProvider]
    FOREIGN KEY ([ReferringExternalProviderId])
    REFERENCES [model].[ExternalContacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InvoiceExternalProvider'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InvoiceExternalProvider')
	DROP INDEX IX_FK_InvoiceExternalProvider ON [model].[Invoices]
GO
CREATE INDEX [IX_FK_InvoiceExternalProvider]
ON [model].[Invoices]
    ([ReferringExternalProviderId]);
GO
-- Creating foreign key on [OrderingExternalProviderId] in table 'BillingServices'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalProviderBillingService'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingServices] DROP CONSTRAINT FK_ExternalProviderBillingService

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContacts]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingServices]
ADD CONSTRAINT [FK_ExternalProviderBillingService]
    FOREIGN KEY ([OrderingExternalProviderId])
    REFERENCES [model].[ExternalContacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalProviderBillingService'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalProviderBillingService')
	DROP INDEX IX_FK_ExternalProviderBillingService ON [model].[BillingServices]
GO
CREATE INDEX [IX_FK_ExternalProviderBillingService]
ON [model].[BillingServices]
    ([OrderingExternalProviderId]);
GO
-- Creating foreign key on [ExternalProviderId] in table 'PatientExternalProviders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientExternalProviderExternalProvider'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientExternalProviders] DROP CONSTRAINT FK_PatientExternalProviderExternalProvider

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContacts]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientExternalProviders]
ADD CONSTRAINT [FK_PatientExternalProviderExternalProvider]
    FOREIGN KEY ([ExternalProviderId])
    REFERENCES [model].[ExternalContacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientExternalProviderExternalProvider'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientExternalProviderExternalProvider')
	DROP INDEX IX_FK_PatientExternalProviderExternalProvider ON [model].[PatientExternalProviders]
GO
CREATE INDEX [IX_FK_PatientExternalProviderExternalProvider]
ON [model].[PatientExternalProviders]
    ([ExternalProviderId]);
GO

-- Creating foreign key on [ExternalProviderId] in table 'PatientInsuranceReferrals'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ExternalProviderPatientInsuranceReferral'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientInsuranceReferrals] DROP CONSTRAINT FK_ExternalProviderPatientInsuranceReferral

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContacts]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInsuranceReferrals]
ADD CONSTRAINT [FK_ExternalProviderPatientInsuranceReferral]
    FOREIGN KEY ([ExternalProviderId])
    REFERENCES [model].[ExternalContacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalProviderPatientInsuranceReferral'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ExternalProviderPatientInsuranceReferral')
	DROP INDEX IX_FK_ExternalProviderPatientInsuranceReferral ON [model].[PatientInsuranceReferrals]
GO
CREATE INDEX [IX_FK_ExternalProviderPatientInsuranceReferral]
ON [model].[PatientInsuranceReferrals]
    ([ExternalProviderId]);
GO
