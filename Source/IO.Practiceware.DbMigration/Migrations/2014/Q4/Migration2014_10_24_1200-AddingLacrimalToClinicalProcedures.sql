﻿IF OBJECT_ID('model.ClinicalProcedures','U') IS NOT NULL AND ((SELECT COALESCE(Name,NULL) FROM model.ClinicalProcedures
	WHERE Name = 'LACRIMAL PROBE, CANALICULA') IS NULL)
BEGIN
INSERT INTO [model].[ClinicalProcedures] (Name, IsSocial,ClinicalProcedureCategoryId,IsDeactivated) VALUES ('LACRIMAL PROBE, CANALICULA',0,3,0)
END