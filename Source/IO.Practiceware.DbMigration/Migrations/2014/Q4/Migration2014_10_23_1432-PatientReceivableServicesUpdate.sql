﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PatientReceivableServicesUpdate' AND parent_id = OBJECT_ID('dbo.PatientReceivableServices','V'))
	DROP TRIGGER PatientReceivableServicesUpdate
GO
	
CREATE TRIGGER PatientReceivableServicesUpdate ON dbo.PatientReceivableServices
INSTEAD OF UPDATE
AS
BEGIN
SET NOCOUNT ON;
DECLARE @ItemId INT
DECLARE @Invoice NVARCHAR(32)
DECLARE @Service NVARCHAR(16)
DECLARE @Modifier NVARCHAR(10)
DECLARE @Quantity REAL
DECLARE @Charge REAL
DECLARE @TypeOfService NVARCHAR(32)
DECLARE @PlaceOfService NVARCHAR(32)
DECLARE @ServiceDate NVARCHAR(10)
DECLARE @LinkedDiag NVARCHAR(4)
DECLARE @Status NVARCHAR(1)
DECLARE @OrderDoc NVARCHAR(1)
DECLARE @ConsultOn NVARCHAR(1)
DECLARE @FeeCharge REAL
DECLARE @ItemOrder INT
DECLARE @ECode INT
DECLARE @PostDate NVARCHAR(8) 
DECLARE @ExternalRefInfo NVARCHAR(16)
DECLARE @QuantityDecimal DECIMAL(12, 2) 
DECLARE @ChargeDecimal DECIMAL(12, 2) 
DECLARE @FeeChargeDecimal DECIMAL(12, 2) 

DECLARE PatientReceivableServicesUpdateCursor CURSOR FOR
SELECT 
ItemId
,Invoice 
,[Service]
,Modifier
,Quantity
,Charge
,TypeOfService
,PlaceOfService
,ServiceDate
,LinkedDiag
,[Status]
,OrderDoc
,ConsultOn
,FeeCharge
,ItemOrder
,ECode
,PostDate
,ExternalRefInfo
,QuantityDecimal
,ChargeDecimal
,FeeChargeDecimal 
FROM inserted

OPEN PatientReceivableServicesUpdateCursor FETCH NEXT FROM PatientReceivableServicesUpdateCursor INTO
	@ItemId, @Invoice, @Service, @Modifier, @Quantity, @Charge, @TypeOfService, @PlaceOfService, @ServiceDate,
	@LinkedDiag, @Status, @OrderDoc, @ConsultOn, @FeeCharge, @ItemOrder, @ECode, @PostDate, @ExternalRefInfo,
	@QuantityDecimal, @ChargeDecimal, @FeeChargeDecimal
WHILE @@FETCH_STATUS = 0 
BEGIN
	DECLARE @AppointmentId INT
	SELECT @AppointmentId =
	CASE WHEN ISNUMERIC(SUBSTRING(@Invoice,CHARINDEX('-', @Invoice) + 1,LEN(@Invoice))) = 1 
		THEN CONVERT(INT,SUBSTRING(@Invoice,CHARINDEX('-', @Invoice) +  1,LEN(@Invoice))) 
		ELSE NULL 
	END

	IF ((@Status = 'X') AND ((SELECT [Status] FROM deleted WHERE ItemId = @ItemId) <> @Status))
	BEGIN
		DELETE FROM model.BillingServiceTransactions WHERE BillingServiceId = @ItemId

		UPDATE f
		SET f.BillingServiceId = NULL
		FROM model.FinancialInformations f
		WHERE f.BillingServiceId = @ItemId

		UPDATE a
		SET a.BillingServiceId = NULL
		FROM model.Adjustments a
		WHERE a.BillingServiceId = @ItemId

		UPDATE b
		SET b.BillingServiceTransactionStatusId = 
			CASE 
				WHEN b.BillingServiceTransactionStatusId IN (1,3)
					THEN 5
				WHEN b.BillingServiceTransactionStatusId = 2
					THEN 6
				WHEN b.BillingServiceTransactionStatusId = 4
					THEN 7
			END
		FROM model.BillingServiceTransactions b
		JOIN model.InvoiceReceivables ir ON ir.Id = b.InvoiceReceivableId
		JOIN model.Invoices i ON i.Id = ir.InvoiceId
		WHERE i.EncounterId IN (SELECT EncounterId FROM dbo.Appointments WHERE AppointmentId = @AppointmentId)
			AND b.BillingServiceTransactionStatusId IN (1,2,3,4)

		DELETE FROM model.BillingServiceModifiers WHERE BillingServiceId = @ItemId
		DELETE FROM model.BillingServices WHERE Id = @ItemId
	END
	
	IF((SELECT PlaceOfService FROM deleted WHERE ItemId = @ItemId) <> @PlaceOfService AND @PlaceOfService <> '') AND (@Status <> 'X') 
	BEGIN
		UPDATE inv
		SET inv.ServiceLocationCodeId = (
				SELECT 
				Id 
				FROM model.ServiceLocationCodes slc 
				WHERE PlaceOfServiceCode = @PlaceOfService
			)
		FROM model.Invoices inv
		WHERE inv.LegacyPatientReceivablesInvoice = @Invoice
	END

	--BillingServices
	IF((SELECT ItemOrder FROM deleted WHERE ItemId = @ItemId) <> @ItemOrder) AND (@Status <> 'X')
	BEGIN
		UPDATE model.BillingServices
		SET OrdinalId = @ItemOrder
		WHERE Id = @ItemId
	END

	FETCH NEXT FROM PatientReceivableServicesUpdateCursor INTO 
	@ItemId, @Invoice, @Service, @Modifier, @Quantity, @Charge, @TypeOfService, @PlaceOfService, @ServiceDate,
	@LinkedDiag, @Status, @OrderDoc, @ConsultOn, @FeeCharge, @ItemOrder, @ECode, @PostDate, @ExternalRefInfo,
	@QuantityDecimal, @ChargeDecimal, @FeeChargeDecimal

	CLOSE PatientReceivableServicesUpdateCursor
	DEALLOCATE PatientReceivableServicesUpdateCursor
END
END
GO
