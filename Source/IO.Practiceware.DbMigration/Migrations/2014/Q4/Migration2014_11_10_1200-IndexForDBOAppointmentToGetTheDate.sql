﻿--Create an index to retrieve the start date much faster through the model.encounters view.

IF NOT EXISTS(SELECT name FROM sysindexes where name = 'Appointment_StartDate_Idx') 
CREATE NONCLUSTERED INDEX [Appointment_StartDate_Idx]
ON [dbo].[Appointments] ([AppTime])
INCLUDE ([AppointmentId],[AppTypeId],[AppDate],[ResourceId2],[ConfirmStatus])
GO
