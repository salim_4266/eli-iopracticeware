﻿IF  EXISTS (SELECT * FROM model.BillingServiceTransactions WHERE BillingServiceTransactionStatusId = 8)
BEGIN

UPDATE model.BillingServiceTransactions
SET BillingServiceTransactionStatusId = 5
WHERE BillingServiceTransactionStatusId = 8

END
GO