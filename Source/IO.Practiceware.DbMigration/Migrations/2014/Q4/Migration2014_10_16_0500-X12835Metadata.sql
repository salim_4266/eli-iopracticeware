﻿DECLARE @tableName NVARCHAR(MAX)
SET @tableName = 'model.ExternalSystemMessageTypes'

DECLARE @sqlDisable NVARCHAR(MAX)
SET @sqlDisable = ''
SELECT @sqlDisable = @sqlDisable +
'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_id)) + '.' + QUOTENAME(OBJECT_NAME(parent_id)) + ' DISABLE TRIGGER ' + QUOTENAME(name) + '; '
FROM sys.triggers 
WHERE parent_id = OBJECT_ID(@tableName,'U') AND is_disabled = 0

EXEC sp_executesql @sqlDisable

DECLARE @sqlEnable NVARCHAR(MAX)
SET @sqlEnable = REPLACE(@sqlDisable,'DISABLE','ENABLE')

SELECT @sqlEnable AS [Enable] INTO #EnableTriggers
GO

DECLARE @sqlEnable NVARCHAR(MAX)
IF NOT EXISTS (SELECT Id FROM model.ExternalSystemMessageTypes WHERE Id = 36 AND Name = 'X12_835')
BEGIN
	SET IDENTITY_INSERT model.ExternalSystemMessageTypes ON

	INSERT model.ExternalSystemMessageTypes (Id, Name, [Description], IsOutbound)
	SELECT 
	36 AS Id
	,'X12_835' AS Name
	,'Electronic 835 remittance file' AS [Description]
	,CONVERT(BIT,0) AS IsOutbound

	SET IDENTITY_INSERT model.ExternalSystemMessageTypes OFF
	SET @sqlEnable = ''
	SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
	EXEC sp_executesql @sqlEnable
END
ELSE IF EXISTS (SELECT Id FROM model.ExternalSystemMessageTypes WHERE Id = 36 AND Name <> 'X12_835')
BEGIN
	SET @sqlEnable = ''
	SELECT @sqlEnable = @sqlEnable + [Enable] FROM #EnableTriggers
	EXEC sp_executesql @sqlEnable
	RAISERROR('There already exists an entry with the Id 36 in model.ExternalSystemMessageTypes, where Name is not X12_835. INSERT fails.',16,1)
END
GO

IF NOT EXISTS (SELECT Id FROM model.ExternalSystems WHERE Name = 'Payer')
BEGIN
	SET IDENTITY_INSERT model.ExternalSystems ON

	INSERT model.ExternalSystems (Id, Name)
	SELECT 
	25 AS Id, 'Payer' AS Name

	SET IDENTITY_INSERT model.ExternalSystems OFF
END
GO

INSERT model.ExternalSystemExternalSystemMessageTypes (ExternalSystemId, ExternalSystemMessageTypeId, TemplateFile, IsDisabled)
SELECT 
(SELECT TOP 1 Id FROM model.ExternalSystems WHERE Name = 'Payer') AS ExternalSystemId
,(SELECT TOP 1 Id FROM model.ExternalSystemMessageTypes WHERE Id = 36 AND Name = 'X12_835') AS ExternalSystemMessageTypeId
,NULL AS TemplateFile
,CONVERT(BIT,0) AS IsDisabled
EXCEPT SELECT ExternalSystemId, ExternalSystemMessageTypeId, TemplateFile, IsDisabled FROM model.ExternalSystemExternalSystemMessageTypes
GO

IF NOT EXISTS (SELECT Id FROM model.PracticeRepositoryEntities WHERE Id = 75 AND Name = 'ExternalSystemMessage')
BEGIN
	INSERT model.PracticeRepositoryEntities (Id, Name, KeyPropertyName)
	SELECT 
	75 AS Id
	,'ExternalSystemMessage' AS Name
	,'Id' AS KeyPropertyName
END
ELSE IF EXISTS (SELECT Id FROM model.PracticeRepositoryEntities WHERE Id = 75 AND Name <> 'ExternalSystemMessage')
RAISERROR('There already exists an entry with the Id 75 in model.PracticeRepositoryEntities, where Name is not ExternalSystemMessage. INSERT fails.',16,1)
GO



--BEGIN TRAN
--INSERT INTO model.ApplicationSettings (Value, Name, MachineName, ApplicationSettingTypeId, UserId)
--SELECT 
--'<z:anyType xmlns:i="http://www.w3.org/2001/XMLSchema-instance" z:Id="1" xmlns:d1p1="http://schemas.datacontract.org/2004/07/IO.Practiceware.Model" i:type="d1p1:RemittanceConfigurationApplicationSetting" xmlns:z="http://schemas.microsoft.com/2003/10/Serialization/">
  --<d1p1:RemittanceFileDirectory z:Id="2">H:\Pinpoint\Remit</d1p1:RemittanceFileDirectory>
--</z:anyType>' AS Value
--,'RemittanceFileDirectory' AS Name
--,'IOP-DT129' AS MachineName
--,16 AS ApplicationSettingTypeId
--,NULL AS UserId

--COMMIT
