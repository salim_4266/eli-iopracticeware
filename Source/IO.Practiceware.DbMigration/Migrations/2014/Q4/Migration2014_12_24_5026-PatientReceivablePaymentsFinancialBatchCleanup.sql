﻿IF OBJECT_ID('dbo.PatientReceivablePaymentsBackup','U') IS NOT NULL
	AND OBJECT_ID('dbo.PatientReceivablePaymentsFinancialBatchCleanup2','U') IS NULL
	AND OBJECT_ID('dbo.PatientReceivablesBackup','U') IS NOT NULL
BEGIN
	EXEC('
	SELECT 
	pr.PatientId
	,p1.PaymentCheck AS BatchPaymentCheck
	,p2.PaymentCheck AS PostedPaymentCheck
	,fb.PatientId AS CurrentFinancialBatchPatientId
	,e.PatientId AS CurrentEncounterPatientId
	,fb.CheckCode AS CurrentFinancialBatchCheckCode
	,fb.Id AS FinancialBatchId
	INTO dbo.PatientReceivablePaymentsFinancialBatchCleanup2
	FROM PatientReceivablePaymentsBackup p1
	JOIN PatientReceivablePaymentsBackup p2 ON p1.PaymentId = p2.PaymentBatchCheckId 
		AND p1.PaymentCheck <> p2.PaymentCheck 
		AND p1.PayerId <> p2.PayerId 
		AND p2.PayerId <> 0
	JOIN dbo.PatientReceivablesBackup pr ON pr.ReceivableId = p2.ReceivableId
	JOIN model.Invoices i ON i.LegacyPatientReceivablesInvoice = pr.Invoice
	JOIN model.Encounters e ON e.Id = i.EncounterId
	JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
	JOIN model.Adjustments adj ON adj.InvoiceReceivableId = ir.id
	JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
	WHERE p1.ReceivableId = 0 
		AND p1.PaymentType = ''=''
		AND EXISTS (
			SELECT TOP 1 PaymentId
			FROM dbo.PatientReceivablePayments
			WHERE PaymentCheck = p1.PaymentCheck 
				AND PayerId = p1.PayerId
		) 
		AND fb.PatientId <> e.PatientId 
		AND pr.PatientId = e.PatientId
	GROUP BY p1.PayerId
	,p1.PaymentCheck
	,p2.PayerId
	,p2.PaymentCheck
	,p2.ReceivableId
	,fb.PatientId
	,e.PatientId
	,fb.Id
	,pr.PatientId
	,fb.CheckCode
		
	UPDATE f
	SET f.CheckCode = p.PostedPaymentCheck
		,f.PatientId = p.CurrentEncounterPatientId
	FROM model.FinancialBatches f
	JOIN dbo.PatientReceivablePaymentsFinancialBatchCleanup2 p ON p.FinancialBatchId = f.Id
	')
END
GO