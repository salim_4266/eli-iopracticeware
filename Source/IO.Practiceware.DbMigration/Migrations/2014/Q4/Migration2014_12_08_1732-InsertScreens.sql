﻿DECLARE @ScreenTypeId INT

SELECT @ScreenTypeId = id
FROM Model.ScreenType_Enumeration
WHERE NAME = 'Financials'

IF NOT EXISTS (
		SELECT id
		FROM Model.Screens
		WHERE NAME = 'Patient Financials'
			AND ScreenTypeId = @ScreenTypeId
		)
	INSERT INTO Model.Screens (
		Id
		,NAME
		,ScreenTypeId
		,IsDeactivated
		)
	VALUES (
		8
		,'Patient Financials'
		,@ScreenTypeId
		,0
		)

IF NOT EXISTS (
		SELECT id
		FROM Model.Screens
		WHERE NAME = 'Edit & Bill'
			AND ScreenTypeId = @ScreenTypeId
		)
	INSERT INTO Model.Screens (
		Id
		,NAME
		,ScreenTypeId
		,IsDeactivated
		)
	VALUES (
		9
		,'Edit & Bill'
		,@ScreenTypeId
		,0
		)

IF NOT EXISTS (
		SELECT id
		FROM Model.Screens
		WHERE NAME = 'Post Adjustments'
			AND ScreenTypeId = @ScreenTypeId
		)
	INSERT INTO Model.Screens (
		Id
		,NAME
		,ScreenTypeId
		,IsDeactivated
		)
	VALUES (
		10
		,'Post Adjustments'
		,@ScreenTypeId
		,0
		)

