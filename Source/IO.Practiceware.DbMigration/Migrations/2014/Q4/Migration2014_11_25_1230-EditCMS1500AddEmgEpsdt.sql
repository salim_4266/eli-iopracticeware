﻿IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	
	UPDATE model.TemplateDocuments	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 
	'<input type="text" id="ServiceCode" value="@(string.IsNullOrEmpty(service.ServiceCode) ? "" : service.ServiceCode.Length > 5 ? service.ServiceCode.Substring(0,5) : service.ServiceCode)"',

	'<input type="text" id="Emg" value="@(service.Emg)" class="smallText" style="z-index: 1; left: 255px; top: @(beginTop + (index * incrementalPixels))px; position: absolute; width: 22px; height: 14px;" />
     <input type="text" id="ServiceCode" value="@(string.IsNullOrEmpty(service.ServiceCode) ? "" : service.ServiceCode.Length > 5 ? service.ServiceCode.Substring(0,5) : service.ServiceCode)"' )) 
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL

	UPDATE model.TemplateDocuments	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 
	'<input type="text" id="RenderingProviderTaxonomy" class="smallText" value="@(Model.InsuranceTypeIsMedicaid ? "ZZ" + Model.RenderingProviderTaxonomy : "")"',

	'<input type="text" id="Epsdt" value="@(service.Epsdt)" class="smallText" style="z-index: 1; top: @(beginTop + (index * incrementalPixels))px; position: absolute; width: 18px; left: 740px; height: 14px;" />
     <input type="text" id="RenderingProviderTaxonomy" class="smallText" value="@(Model.InsuranceTypeIsMedicaid ? "ZZ" + Model.RenderingProviderTaxonomy : "")"' )) 
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL
END