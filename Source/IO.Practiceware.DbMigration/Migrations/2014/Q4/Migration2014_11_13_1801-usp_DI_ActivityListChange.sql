﻿IF EXISTS (SELECT TOP 1 * FROM sys.procedures WHERE Name = 'usp_DI_ActivityList')
BEGIN
	DROP PROCEDURE usp_DI_ActivityList
END
GO

CREATE PROCEDURE usp_DI_ActivityList
	(
	@PatId int,
	@CurDate nchar(8),
	@Status nchar(15)
	)
AS
BEGIN
SET NOCOUNT ON

select pa.ActivityId, pa.AppointmentId
	, app.AppDate, app.AppTime, app.AppTypeId, app.Comments
	, apt.AppointmentType
	, r.ResourceName AS ScheduledDoctor
from PracticeActivity pa
	inner join Appointments app on pa.AppointmentId = app.AppointmentId
	inner join AppointmentType apt on app.AppTypeId = apt.AppTypeId
	left join Resources r on app.ResourceId1 = r.ResourceId
where pa.PatientId = @PatId
	and pa.Status in (select nstr from CharTable(@Status, ',')) 
	and app.Comments <> 'ADD VIA BILLING'
	and app.AppTypeId <> 0
order by pa.ActivityDate desc


-- Turn NOCOUNT back OFF
SET NOCOUNT OFF
END
GO