﻿IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit
GO

SELECT 1 AS Value INTO #NoAudit


UPDATE model.ExternalSystemEntityMappings
SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Encounter')
WHERE ExternalSystemEntityId = (SELECT TOP 1 ese.Id FROM model.ExternalSystemEntities ese WHERE ese.Name = 'Visit')

UPDATE model.ExternalSystemEntities 
SET PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'Encounter') 
WHERE Id = (SELECT TOP 1 ese.Id FROM model.ExternalSystemEntities ese WHERE ese.Name = 'Visit')


IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit
GO
