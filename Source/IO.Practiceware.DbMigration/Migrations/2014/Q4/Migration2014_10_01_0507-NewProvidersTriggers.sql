﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersInsertResources' AND parent_id = OBJECT_ID('dbo.Resources','U'))
	DROP TRIGGER ProvidersInsertResources
GO

CREATE TRIGGER dbo.ProvidersInsertResources ON dbo.Resources
AFTER INSERT
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @ResourceId INT
	DECLARE @ResourceType NVARCHAR(1)
	DECLARE @PracticeId INT
	DECLARE @Billable NVARCHAR(1)
	DECLARE @GoDirect NVARCHAR(1)
	DECLARE @ServiceCode NVARCHAR(4)

	DECLARE ProvidersInsertResourcesCursor CURSOR FOR
	SELECT ResourceId, ResourceType, PracticeId, Billable, GoDirect, ServiceCode
	FROM inserted WHERE ResourceType in ('D', 'Q', 'Z', 'Y', 'R')

	OPEN ProvidersInsertResourcesCursor FETCH NEXT FROM ProvidersInsertResourcesCursor INTO
	@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect, @ServiceCode
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		INSERT INTO model.Providers (
			IsBillable
			,IsEmr
			,BillingOrganizationId
			,UserId
			,ServiceLocationId
		)
		SELECT 
		* 
		FROM (
			SELECT DISTINCT
			CASE @Billable WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsBillable
			,CASE @GoDirect WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsEMR
			,CASE WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'
					THEN (110000000 * 1 + @ResourceId) 
				ELSE pnBillOrg.PracticeId
			END AS BillingOrganizationId
			,NULL AS UserId
			,pnBillOrg.PracticeId AS ServiceLocationId
			FROM inserted re
			INNER JOIN dbo.PracticeName pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
				AND pnBillOrg.PracticeType = 'P'
			LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
					AND pa.ResourceType = 'R'
			WHERE re.ResourceId = @ResourceId
					AND @ResourceType = 'R'
					AND @ServiceCode = '02'
					AND @Billable = 'Y'
					AND pnBillOrg.LocationReference <> ''
	
			UNION
	
			SELECT DISTINCT
			CASE @Billable WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsBillable
			,CASE @GoDirect WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsEMR
			,CASE WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y' THEN (110000000 * 1 + @ResourceId) ELSE pnBillOrg.PracticeId END AS BillingOrganizationId
			,@ResourceId AS UserId
			,NULL AS ServiceLocationId
			FROM inserted re
			INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
				AND pnBillOrg.PracticeType = 'P'
			LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
					AND pa.ResourceType = 'R'
			WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
				AND re.ResourceId = @ResourceId
		)v
		EXCEPT
		SELECT IsBillable, IsEmr, BillingOrganizationId, UserId, ServiceLocationId FROM model.Providers	

		FETCH NEXT FROM ProvidersInsertResourcesCursor INTO 
		@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect, @ServiceCode
	END
	CLOSE ProvidersInsertResourcesCursor
	DEALLOCATE ProvidersInsertResourcesCursor
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersUpdateResources' AND parent_id = OBJECT_ID('dbo.Resources','U'))
	DROP TRIGGER ProvidersUpdateResources
GO

CREATE TRIGGER dbo.ProvidersUpdateResources ON dbo.Resources
AFTER UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @ResourceId int
	DECLARE @ResourceType nvarchar(1)
	DECLARE @PracticeId int
	DECLARE @Billable nvarchar(1)
	DECLARE @GoDirect nvarchar(1)

	DECLARE ProvidersUpdateResourcesCursor CURSOR FOR
	SELECT i.ResourceId, i.ResourceType, i.PracticeId, i.Billable, i.GoDirect
	FROM inserted i
	JOIN deleted d ON d.ResourceId = i.ResourceId
		AND d.ResourceType IN ('D', 'Q', 'Z','Y', 'R')
	WHERE i.ResourceType IN ('D', 'Q', 'Z','Y', 'R')

	OPEN ProvidersUpdateResourcesCursor FETCH NEXT FROM ProvidersUpdateResourcesCursor INTO
	@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		DECLARE @DeletedPracticeId INT
		DECLARE @DeletedResourceId INT
		SET @DeletedPracticeId = (SELECT TOP 1 PracticeId FROM deleted WHERE @ResourceId = @ResourceId)
		SET @DeletedResourceId = (SELECT TOP 1 ResourceId FROM deleted WHERE @ResourceId = @ResourceId)

		UPDATE p
		SET p.IsBillable = CASE WHEN @Billable = 'Y' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
		,p.IsEmr = CASE WHEN @GoDirect = 'Y' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
		,p.BillingOrganizationId = CASE WHEN 
			((SELECT TOP 1 [Override] FROM dbo.PracticeAffiliations WHERE ResourceId = @ResourceId AND ResourceType = 'R') = 'Y' OR
			(SELECT TOP 1 OrgOverride FROM dbo.PracticeAffiliations WHERE ResourceId = @ResourceId AND ResourceType = 'R') = 'Y')
			THEN (110000000 * 1 + @ResourceId) 
		ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId AND PracticeType = 'P')
		END
		,p.UserId = CASE WHEN @ResourceType = 'R' THEN NULL ELSE @ResourceId END
		,p.ServiceLocationId = CASE WHEN @ResourceType = 'R' THEN (SELECT TOP 1 PracticeId FROM dbo.PracticeName pnBillOrg WHERE pnBillOrg.PracticeId = @PracticeId
				AND pnBillOrg.PracticeType = 'P') END
		FROM model.Providers p
		WHERE p.BillingOrganizationId = CASE WHEN 
				((SELECT TOP 1 [Override] FROM dbo.PracticeAffiliations WHERE ResourceId = @DeletedResourceId AND ResourceType = 'R') = 'Y' OR
				(SELECT TOP 1 OrgOverride FROM dbo.PracticeAffiliations WHERE ResourceId = @DeletedResourceId AND ResourceType = 'R') = 'Y')
				THEN (110000000 * 1 + @DeletedResourceId) 
			ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @DeletedPracticeId AND PracticeType = 'P')
			END
			AND p.UserId = CASE WHEN @ResourceType = 'R' THEN NULL ELSE @ResourceId END
			AND p.ServiceLocationId = CASE WHEN @ResourceType = 'R' THEN (SELECT TOP 1 PracticeId FROM dbo.PracticeName pnBillOrg WHERE pnBillOrg.PracticeId = @DeletedPracticeId
				AND pnBillOrg.PracticeType = 'P') END

		FETCH NEXT FROM ProvidersUpdateResourcesCursor INTO 
		@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect
	END
	CLOSE ProvidersUpdateResourcesCursor
	DEALLOCATE ProvidersUpdateResourcesCursor
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersInsertPracticeAffiliations' AND parent_id = OBJECT_ID('dbo.PracticeAffiliations','U'))
	DROP TRIGGER ProvidersInsertPracticeAffiliations
GO

CREATE TRIGGER dbo.ProvidersInsertPracticeAffiliations ON dbo.PracticeAffiliations
AFTER INSERT
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Override nvarchar(1)
	DECLARE @OrgOverride nvarchar(1)
	DECLARE @ResourceId int
	DECLARE @ResourceType nvarchar(1)
	DECLARE @PracticeId int

	DECLARE ProvidersInsertPracticeAffiliationsCursor CURSOR FOR
	SELECT i.[Override], i.OrgOverride, i.ResourceId, r.ResourceType, r.PracticeId
	FROM inserted i
	JOIN dbo.Resources r ON r.ResourceId = i.ResourceId 
		AND r.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND i.ResourceType = 'R'

	OPEN ProvidersInsertPracticeAffiliationsCursor FETCH NEXT FROM ProvidersInsertPracticeAffiliationsCursor INTO
	@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		IF (@ResourceId IS NOT NULL) AND 
			((SELECT TOP 1 Id FROM model.Providers WHERE UserId = @ResourceId) IS NOT NULL)
			AND NOT EXISTS(SELECT TOP 1 * FROM model.Providers 
				WHERE UserId = @ResourceId 
					AND BillingOrganizationId = (110000000 * 1 + @ResourceId)) 
			AND (@Override = 'Y' OR @OrgOverride  = 'Y')
		BEGIN
			INSERT INTO model.Providers (UserId, BillingOrganizationId, IsBillable, IsEmr)
				SELECT @ResourceId AS UserId
				,(110000000 * 1 + @ResourceId) AS BillingOrganizationId
				,CONVERT(bit, 1) AS IsBillable
				,CONVERT(bit, 0) AS IsEmr
		END
		FETCH NEXT FROM ProvidersInsertPracticeAffiliationsCursor INTO 
		@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId
	END
	CLOSE ProvidersInsertPracticeAffiliationsCursor
	DEALLOCATE ProvidersInsertPracticeAffiliationsCursor
END

GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersUpdatePracticeAffiliations' AND parent_id = OBJECT_ID('dbo.PracticeAffiliations','U'))
	DROP TRIGGER ProvidersUpdatePracticeAffiliations
GO

CREATE TRIGGER dbo.ProvidersUpdatePracticeAffiliations ON dbo.PracticeAffiliations
AFTER UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Override nvarchar(1)
	DECLARE @OrgOverride nvarchar(1)
	DECLARE @ResourceId int
	DECLARE @ResourceType nvarchar(1)
	DECLARE @PracticeId int

	DECLARE ProvidersUpdatePracticeAffiliationsCursor CURSOR FOR
	SELECT i.[Override], i.OrgOverride, i.ResourceId, r.ResourceType, r.PracticeId
	FROM inserted i
	JOIN dbo.Resources r ON r.ResourceId = i.ResourceId 
		AND r.ResourceType IN ('D', 'Q', 'Z','Y')
		AND i.ResourceType = 'R'

	OPEN ProvidersUpdatePracticeAffiliationsCursor FETCH NEXT FROM ProvidersUpdatePracticeAffiliationsCursor INTO
	@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		DECLARE @DeletedPracticeId INT
		DECLARE @DeletedResourceId INT
		SET @DeletedPracticeId = (SELECT TOP 1 r.PracticeId
		FROM deleted d
		JOIN dbo.Resources r ON r.ResourceId = d.ResourceId 
			AND r.ResourceType IN ('D', 'Q', 'Z','Y')
			AND d.ResourceType = 'R')
		
		IF (@ResourceId IS NOT NULL) AND 
			((SELECT TOP 1 Id FROM model.Providers WHERE UserId = @ResourceId) IS NOT NULL)
			AND NOT EXISTS(SELECT TOP 1 * FROM model.Providers 
			WHERE UserId = @ResourceId 
					AND BillingOrganizationId = (110000000 * 1 + @ResourceId)) 
			AND (@Override = 'Y' OR @OrgOverride  = 'Y')
		BEGIN
			INSERT INTO model.Providers (UserId, BillingOrganizationId, IsBillable, IsEmr)
				SELECT @ResourceId AS UserId
				,(110000000 * 1 + @ResourceId) AS BillingOrganizationId
				,CONVERT(bit, 1) AS IsBillable
				,CONVERT(bit, 0) AS IsEmr
		END

		FETCH NEXT FROM ProvidersUpdatePracticeAffiliationsCursor INTO 
		@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId
	END
	CLOSE ProvidersUpdatePracticeAffiliationsCursor
	DEALLOCATE ProvidersUpdatePracticeAffiliationsCursor
END
GO