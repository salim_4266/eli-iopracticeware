﻿IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Name = 'Process Pending Claims' AND PermissionCategoryId = 9)
BEGIN

SET IDENTITY_INSERT model.Permissions ON
INSERT INTO model.Permissions (Id, Name, PermissionCategoryId) VALUES (53, 'Process Pending Claims', 9)
SET IDENTITY_INSERT model.Permissions OFF
END
GO

IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Name = 'View Pending Claims' AND PermissionCategoryId = 9)
BEGIN

SET IDENTITY_INSERT model.Permissions ON
INSERT INTO model.Permissions (Id, Name, PermissionCategoryId) VALUES (54, 'View Pending Claims', 9)
SET IDENTITY_INSERT model.Permissions OFF
END
GO

INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)
SELECT 0 AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id IN (53, 54)
WHERE ResourceType <> 'R' AND
 SUBSTRING(re.ResourcePermissions, 12, 1) = 1
 AND ResourceId > 0

EXCEPT

SELECT 0, UserId, PermissionId 
FROM model.UserPermissions
