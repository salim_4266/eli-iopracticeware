﻿--Task 10253
--Adding diagnosis 5-9 for CM51500.
IF EXISTS (
		SELECT *
		FROM model.TemplateDocuments
		WHERE DisplayName = 'CMS-1500'
			AND Content IS NOT NULL
		)
BEGIN

DECLARE @sqlCMS nvarchar(MAX)
SET @sqlCMS = 
'
	DECLARE @content AS NVARCHAR(MAX);
	DECLARE @old VARCHAR(max);
	DECLARE @new VARCHAR(max);
	DECLARE @StartIndex INT;

	SET @new = ''<input type="text" id="DiagnosisA" value="@Model.Diagnosis1.Replace(".","")" style="z-index: 1; left: 50px;  top: @(row15Top)px; position: absolute; width: 78px; right: 818px; height: 11px;" />
                <input type="text" id="DiagnosisB" value="@Model.Diagnosis2.Replace(".","")" style="z-index: 1; left: 200px; top: @(row15Top)px; position: absolute; width: 78px; right: 718px; height: 11px;" />
                <input type="text" id="DiagnosisC" value="@Model.Diagnosis3.Replace(".","")" style="z-index: 1; left: 350px; top: @(row15Top)px; position: absolute; width: 84px; right: 618px; height: 11px;" />
                <input type="text" id="DiagnosisD" value="@Model.Diagnosis4.Replace(".","")" style="z-index: 1; left: 500px; top: @(row15Top)px; position: absolute; width: 84px; right: 518px; height: 11px;" />
                <input type="text" id="DiagnosisE" value="@Model.Diagnosis5.Replace(".","")" style="z-index: 1; left: 50px;  top: @(row15Top+20)px; position: absolute; width: 78px; right: 818px; height: 11px;" />
                <input type="text" id="DiagnosisF" value="@Model.Diagnosis6.Replace(".","")" style="z-index: 1; left: 200px; top: @(row15Top+20)px; position: absolute; width: 78px; right: 718px; height: 11px;" />
                <input type="text" id="DiagnosisG" value="@Model.Diagnosis7.Replace(".","")" style="z-index: 1; left: 350px; top: @(row15Top+20)px; position: absolute; width: 84px; right: 618px; height: 11px;" />
                <input type="text" id="DiagnosisH" value="@Model.Diagnosis8.Replace(".","")" style="z-index: 1; left: 500px; top: @(row15Top+20)px; position: absolute; width: 84px; right: 518px; height: 11px;" />
                <input type="text" id="DiagnosisI" value="@Model.Diagnosis9.Replace(".","")" style="z-index: 1; left: 50px;  top: @(row15Top+40)px; position: absolute; width: 78px; right: 818px; height: 11px;" />''

	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = ''CMS-1500''
				AND Content IS NOT NULL
			)
	SET @StartIndex = CHARINDEX(LOWER(''<input type="text" id="DiagnosisA"''), LOWER(@content), 1)

	IF @StartIndex > 0
	BEGIN
		DECLARE @EndIndex INT;

		IF (@content LIKE ''%@if (!String.IsNullOrEmpty(@Model.ResubmitCode))%'')
		SET @EndIndex = CHARINDEX(''@if (!String.IsNullOrEmpty(@Model.ResubmitCode))'', @content, @StartIndex) - 1

		IF (@content LIKE ''%<input type="text" id="ResubmitCode"%'')
		SET @EndIndex = CHARINDEX(''<input type="text" id="ResubmitCode"'', @content, @StartIndex) - 1

		SET @old = SUBSTRING(@content, @StartIndex, @EndIndex - @StartIndex)

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = ''CMS-1500''
			AND Content IS NOT NULL
	END

	--Look for DiagnosisPointer and Update it.
	SET @new =	''<input type="text" id="DiagnosisPointerA" value="@(string.IsNullOrEmpty(service.DiagnosisPointer1) ? "" : "A")" class="smallText" style="font-size:9px;z-index: 1; top: @(beginTop + (index * incrementalPixels))px; position: absolute; width: 9px; left: 525px; height: 14px;" />
                    <input type="text" id="DiagnosisPointerB" value="@(string.IsNullOrEmpty(service.DiagnosisPointer2) ? "" : "B")" class="smallText" style="font-size:9px;z-index: 1; top: @(beginTop + (index * incrementalPixels))px; position: absolute; width: 9px; left: 535px; height: 14px;" />
                    <input type="text" id="DiagnosisPointerC" value="@(string.IsNullOrEmpty(service.DiagnosisPointer3) ? "" : "C")" class="smallText" style="font-size:9px;z-index: 1; top: @(beginTop + (index * incrementalPixels))px; position: absolute; width: 9px; left: 545px; height: 14px;" />
                    <input type="text" id="DiagnosisPointerD" value="@(string.IsNullOrEmpty(service.DiagnosisPointer4) ? "" : "D")" class="smallText" style="font-size:9px;z-index: 1; top: @(beginTop + (index * incrementalPixels))px; position: absolute; width: 9px; left: 555px; height: 14px;" />
                    <input type="text" id="DiagnosisPointerE" value="@(string.IsNullOrEmpty(service.DiagnosisPointer5) ? "" : "E")" class="smallText" style="font-size:9px;z-index: 1; top: @(beginTop + (index * incrementalPixels))px; position: absolute; width: 9px; left: 565px; height: 14px;" />
                    <input type="text" id="DiagnosisPointerF" value="@(string.IsNullOrEmpty(service.DiagnosisPointer6) ? "" : "F")" class="smallText" style="font-size:9px;z-index: 1; top: @(beginTop + (index * incrementalPixels))px; position: absolute; width: 9px; left: 575px; height: 14px;" />
                    <input type="text" id="DiagnosisPointerG" value="@(string.IsNullOrEmpty(service.DiagnosisPointer7) ? "" : "G")" class="smallText" style="font-size:9px;z-index: 1; top: @(beginTop + (index * incrementalPixels))px; position: absolute; width: 11px; left: 585px; height: 14px;" />
                    <input type="text" id="DiagnosisPointerH" value="@(string.IsNullOrEmpty(service.DiagnosisPointer8) ? "" : "H")" class="smallText" style="font-size:9px;z-index: 1; top: @(beginTop + (index * incrementalPixels))px; position: absolute; width: 9px; left: 600px; height: 14px;" />
                    <input type="text" id="DiagnosisPointerI" value="@(string.IsNullOrEmpty(service.DiagnosisPointer9) ? "" : "I")" class="smallText" style="font-size:9px;z-index: 1; top: @(beginTop + (index * incrementalPixels))px; position: absolute; width: 9px; left: 610px; height: 14px;" />''

	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = ''CMS-1500''
				AND Content IS NOT NULL
			)
	SET @StartIndex = CHARINDEX(LOWER(''<input type="text" id="DiagnosisPointerA"''), LOWER(@content), 1)

	SET @EndIndex = 0

	IF @StartIndex > 0
	BEGIN
		SET @EndIndex = CHARINDEX(''<input type="text" id="ServiceCharge"'', @content, @StartIndex) - 1
		SET @old = SUBSTRING(@content, @StartIndex, @EndIndex - @StartIndex)

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = ''CMS-1500''
			AND Content IS NOT NULL
	END
'

EXEC sp_executesql @sqlCMS
END
GO