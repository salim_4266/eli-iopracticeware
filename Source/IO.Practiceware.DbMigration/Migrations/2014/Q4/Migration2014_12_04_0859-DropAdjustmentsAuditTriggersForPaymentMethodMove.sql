﻿-- Drop auditing triggers, since we are about to move PaymentMethod to FinancialBatch table in next migration

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'AuditAdjustmentsUpdateTrigger')
BEGIN 
	DROP TRIGGER model.AuditAdjustmentsUpdateTrigger
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'AuditAdjustmentsDeleteTrigger')
BEGIN 
	DROP TRIGGER model.AuditAdjustmentsDeleteTrigger
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'AuditAdjustmentsInsertTrigger')
BEGIN 
	DROP TRIGGER model.AuditAdjustmentsInsertTrigger
END
GO