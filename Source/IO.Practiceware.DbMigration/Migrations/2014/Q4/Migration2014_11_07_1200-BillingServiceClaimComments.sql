﻿-- Move BillingServiceComments where IsIncludedOnClaim = 1 to BillingServices.ClaimComment
-- take note with highest id if more than one

SELECT MAX(bsc.Id) AS CommentId,
	bsc.BillingServiceId,
	MAX(Value) AS Value
INTO #BillingServiceComments
FROM model.BillingServiceComments bsc
WHERE bsc.IsIncludedOnClaim = 1
GROUP BY bsc.BillingServiceId

UPDATE model.BillingServices
SET ClaimComment = bsc.Value
FROM model.BillingServices bs 
INNER JOIN #BillingServiceComments bsc ON bs.Id = bsc.BillingServiceId


-- Delete BillingServiceComments where IsIncludedOnClaim = 1

DELETE FROM model.BillingServiceComments 
WHERE Id IN (SELECT CommentId FROM #BillingServiceComments)
AND IsIncludedOnStatement = 0

DROP TABLE #BillingServiceComments


-- Remove column IsIncludedOnClaim
IF EXISTS (SELECT object_name(object_id) FROM sys.columns WHERE Name = 'IsIncludedOnClaim' AND object_name(object_id) = 'BillingServiceComments')
BEGIN
ALTER TABLE model.BillingServiceComments DROP COLUMN IsIncludedOnClaim
END
