﻿
--To Create BillingDiagnosis Insert Mappings
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId,PracticeRepositoryEntityKey,ExternalSystemEntityId,ExternalSystemEntityKey) 
SELECT
(SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalCondition') AS PracticeRepositoryEntityId
,CONVERT(NVARCHAR(MAX),c.DrillId) AS PracticeRepositoryEntityKey
,(SELECT es.Id 
FROM model.ExternalSystemEntities es
JOIN model.ExternalSystems e on e.Id = es.ExternalSystemId
WHERE es.name = 'ClinicalCondition'
AND e.Name = 'Icd9') AS ExternalSystemEntityId
,CONVERT(NVARCHAR(MAX),i.DiagnosisCode) AS ExternalSystemEntityKey
FROM dm.PrimaryDiagnosisTable p
JOIN (
SELECT 
d.PrimaryDrillId AS DrillId
FROM dm.PrimaryDiagnosisTable d
LEFT JOIN model.OrganStructures o ON d.MedicalSystem = 
CASE
WHEN o.Name = 'External' THEN 'A'
WHEN o.Name = 'Pupils' THEN 'B'
WHEN o.Name = 'Extraocular Motility' THEN 'C'
WHEN o.Name = 'Visual Field' THEN 'D'
WHEN o.Name = 'Lids/Lacrimal' THEN 'E'
WHEN o.Name = 'Conjunctiva/Sclera' THEN 'F'
WHEN o.Name = 'Cornea' THEN 'G'
WHEN o.Name = 'Anterior Chamber' THEN 'H'
WHEN o.Name = 'Iris' THEN 'I'
WHEN o.Name = 'Lens' THEN 'J'
WHEN o.Name = 'Vitreous' THEN 'K'
WHEN o.Name = 'Optic Nerve' THEN 'L'
WHEN o.Name = 'Blood Vessels' THEN 'M'
WHEN o.Name = 'Macula' THEN 'N'
WHEN o.Name = 'Retina/Choroid' THEN 'O'
WHEN o.Name = 'Peripheral Retina' THEN 'P'
WHEN o.Name = 'Visual Defects' THEN 'R'
END
WHERE dbo.IsNullOrEmpty(d.ReviewSystemLingo) = 0
AND dbo.IsNullOrEmpty(MedicalSystem) = 0
UNION ALL
SELECT 
MAX(d.PrimaryDrillId) AS DrillId
FROM dm.PrimaryDiagnosisTable d
WHERE dbo.IsNullOrEmpty(d.ReviewSystemLingo) = 0
GROUP BY d.DiagnosisNextLevel
,d.DiagnosisRank) c on c.DrillId = p.PrimaryDrillID
JOIN [IOPUBLICDATA].[ExternalData].[icd9].[ICD9] i on i.DiagnosisCode = p.DiagnosisNextLevel 
WHERE p.DiagnosisNextLevel <> '' 
AND p.DiagnosisNextLevel IS NOT NULL
GROUP BY c.DrillId
,i.DiagnosisCode

EXCEPT

SELECT 
PracticeRepositoryEntityId
,CONVERT(NVARCHAR(MAX),PracticeRepositoryEntityKey)
,ExternalSystemEntityId
,CONVERT(NVARCHAR(MAX),ExternalSystemEntityKey) 
FROM model.ExternalSystemEntityMappings