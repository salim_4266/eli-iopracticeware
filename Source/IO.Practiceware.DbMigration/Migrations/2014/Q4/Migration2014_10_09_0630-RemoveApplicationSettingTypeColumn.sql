﻿--TASK #13341.
--Remove the column from the unique setting Index.
IF EXISTS (
		SELECT *
		FROM sys.indexes
		WHERE NAME = N'IX_ApplicationSettings_UniqueSetting'
			AND object_id = OBJECT_ID(N'[model].[ApplicationSettings]')
		)
BEGIN
	DROP INDEX IX_ApplicationSettings_UniqueSetting ON [model].[ApplicationSettings]
END

CREATE UNIQUE NONCLUSTERED INDEX IX_ApplicationSettings_UniqueSetting ON [model].[ApplicationSettings] (
	[Name]
	,[MachineName]
	,[UserId]
	)
GO

--Drop the column ApplicationSettingTypeId.
IF EXISTS (
		SELECT *
		FROM sys.columns
		WHERE NAME = N'ApplicationSettingTypeId'
			AND Object_ID = Object_ID(N'model.ApplicationSettings')
		)
BEGIN
	DECLARE @StrSql AS NVARCHAR(500);

	SET @StrSql = N'
	ALTER TABLE model.ApplicationSettings DROP COLUMN ApplicationSettingTypeId'

	EXEC (@StrSql)
END
GO

IF OBJECT_ID(N'[model].[AuditApplicationSettingsDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditApplicationSettingsDeleteTrigger]
END
GO

IF OBJECT_ID(N'[model].[AuditApplicationSettingsInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditApplicationSettingsInsertTrigger]
END
GO

IF OBJECT_ID(N'[model].[AuditApplicationSettingsUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditApplicationSettingsUpdateTrigger]
END
GO

--Drop the Enum Table if Exists.
IF OBJECT_ID(N'[model].[ApplicationSettingType_Enumeration]', 'U') IS NOT NULL
BEGIN
	EXEC ('DROP TABLE [model].[ApplicationSettingType_Enumeration]')
END
GO

--Create Table Variable.
DECLARE @ApplicationSettingsTemp TABLE (
	OldApplicationSetting NVARCHAR(500)
	,NewApplicationSetting NVARCHAR(500)
	)

--Insert ONLY those ApplicationSettings Names which needs to be renamed.
INSERT INTO @ApplicationSettingsTemp (
	OldApplicationSetting
	,NewApplicationSetting
	)
SELECT * FROM (
SELECT 'SubmitTransactionsApplicationSetting' AS OldApplicationSetting
	,'SubmitTransactions' AS NewApplicationSetting

UNION ALL

SELECT 'AppointmentMessageHandlerSettings' AS OldApplicationSetting
	,'AppointmentMessageHandler' AS NewApplicationSetting

UNION ALL

SELECT 'PatientImageImporterCriteriaViewModel' AS OldApplicationSetting
	,'PatientImageImporterCriteria' AS NewApplicationSetting

UNION ALL

SELECT 'PatientImageImporterIndexTypeViewModel' AS OldApplicationSetting
	,'PatientImageImporterIndexType' AS NewApplicationSetting

UNION ALL

SELECT 'PatientAppointmentsSettingsViewModel' AS OldApplicationSetting
	,'PatientAppointments' AS NewApplicationSetting

UNION ALL

SELECT 'UserFeatureSelectionSettings' AS OldApplicationSetting
	,'UserFeatureSelection' AS NewApplicationSetting

UNION ALL

SELECT 'UserWidgetLayoutSettings' AS OldApplicationSetting
	,'UserWidgetLayout' AS NewApplicationSetting

UNION ALL

SELECT 'PatientSearchAdvancedOptionsSettings' AS OldApplicationSetting
	,'PatientSearchAdvancedOptions' AS NewApplicationSetting
) v
WHERE v.NewApplicationSetting NOT IN (SELECT Name FROM model.ApplicationSettings)


DECLARE @NewApplicationSettingName AS NVARCHAR(2048);
DECLARE @OldApplicationSettingName AS NVARCHAR(2048);

DECLARE CursorToRenameApplicationSettings CURSOR FAST_FORWARD
FOR
SELECT OldApplicationSetting
	,NewApplicationSetting
FROM @ApplicationSettingsTemp

OPEN CursorToRenameApplicationSettings

FETCH NEXT
FROM CursorToRenameApplicationSettings
INTO @OldApplicationSettingName
	,@NewApplicationSettingName

WHILE @@FETCH_STATUS = 0
BEGIN
	UPDATE model.ApplicationSettings
	SET NAME = @NewApplicationSettingName
	WHERE NAME = @OldApplicationSettingName;

	FETCH NEXT
	FROM CursorToRenameApplicationSettings
	INTO @OldApplicationSettingName
		,@NewApplicationSettingName
END

CLOSE CursorToRenameApplicationSettings

DEALLOCATE CursorToRenameApplicationSettings

--Old GridReportSettings Rows will be of this format {GridReportSettings (Appointments (grid))}.So,handle them specially.
IF EXISTS (
		SELECT *
		FROM model.ApplicationSettings
		WHERE NAME LIKE 'GridReportSettings%'
		)
BEGIN
	UPDATE model.ApplicationSettings
	SET NAME = REPLACE(NAME, 'GridReportSettings', 'GridReport')
	WHERE NAME LIKE 'GridReportSettings%'
END