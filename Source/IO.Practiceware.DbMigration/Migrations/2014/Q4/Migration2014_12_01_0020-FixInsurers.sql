﻿SELECT 
i.Id AS IdToKeep
,x.Id AS IdToDelete
INTO #tempins
FROM model.Insurers i
JOIN model.StateOrProvinces s ON i.JurisdictionStateOrProvinceId = s.Id
	AND s.CountryId = 225
JOIN (
	SELECT i.Id, s.CountryId, i.LegacyPracticeInsurersId
	FROM model.Insurers i
	JOIN model.StateOrProvinces s ON i.JurisdictionStateOrProvinceId = s.Id
		AND s.CountryId <> 225
	WHERE LegacyPracticeInsurersId IN (
		SELECT LegacyPracticeInsurersId 
		FROM model.Insurers 
		GROUP BY LegacyPracticeInsurersId HAVING COUNT(LegacyPracticeInsurersId) > 1
	)
)x ON x.LegacyPracticeInsurersId = i.LegacyPracticeInsurersId

UPDATE i
SET i.IsArchived = 1
FROM model.Insurers i
JOIN #tempins ip ON ip.IdToDelete = i.Id