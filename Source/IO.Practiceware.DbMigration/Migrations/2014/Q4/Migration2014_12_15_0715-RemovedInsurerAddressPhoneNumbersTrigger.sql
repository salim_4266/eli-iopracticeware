﻿--We no longer use dbo.PracticeInsurers table..So,remove the triggers which posts the data in the dbo.PracticeInsurers

IF EXISTS (SELECT * FROM sys.triggers WHERE Name = 'OnInsurerAddressesInsertOrUpdate') 
	EXEC('DROP TRIGGER [model].[OnInsurerAddressesInsertOrUpdate]');
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE Name = 'OnInsurerPhoneNumbersInsertOrUpdate') 
	EXEC('DROP TRIGGER [model].[OnInsurerPhoneNumbersInsertOrUpdate]')
GO