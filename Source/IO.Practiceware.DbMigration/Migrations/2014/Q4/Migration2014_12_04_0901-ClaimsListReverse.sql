﻿IF OBJECT_ID('[dbo].[ClaimsListReverse]', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[ClaimsListReverse];
GO

CREATE PROCEDURE [dbo].[ClaimsListReverse] @AptDate VARCHAR(8)
	,@RscId INT
	,@LocId INT
	,@RType VARCHAR(1)
AS
SET NOCOUNT ON
SELECT DISTINCT Appointments.AppointmentId
	,Appointments.AppDate
	,Appointments.AppTime
	,p.Id AS PatientId
	,p.LastName
	,p.FirstName
	,p.MiddleName AS MiddleInitial
	,p.Suffix AS NameReference
	,p.Prefix AS Salutation
	,CONVERT(NVARCHAR, p.DateOfBirth, 112) AS BirthDate
	,Appointments.ScheduleStatus
	,Resources.ResourceName
	,Resources.ResourceId
	,PatientReceivables.ReceivableId
	,PatientReceivables.ReceivableType
	,PatientReceivables.Charge
	,PatientReceivables.OpenBalance
	,PatientReceivables.UnallocatedBalance
	,PatientReceivables.BillToDr
	,PatientReceivables.Invoice
	,AppointmentType.AppointmentType
	,i.Name AS InsurerName
FROM PatientReceivables
INNER JOIN Appointments ON PatientReceivables.AppointmentId = Appointments.AppointmentId
INNER JOIN model.Patients p ON p.Id = Appointments.PatientId
INNER JOIN PatientDemographics ON PatientDemographics.PatientId = Appointments.PatientId
LEFT JOIN Resources ON Resources.ResourceId = Appointments.ResourceId1
LEFT JOIN Resources AS Resources_1 ON Appointments.ResourceId2 = Resources_1.ResourceId
LEFT JOIN model.Insurers i ON PatientReceivables.InsurerId = i.Id
LEFT JOIN AppointmentType ON AppointmentType.AppTypeId = Appointments.AppTypeId
WHERE ((Appointments.AppDate <= @AptDate))
	AND (
		(Appointments.ResourceId1 = @RscId)
		OR (@RscId = - 1)
		)
	AND (
		(Appointments.ResourceId2 = @LocId)
		OR (@LocId = - 1)
		)
	AND (
		(PatientReceivables.ReceivableType = @RType)
		OR (@RType = '0')
		)
	AND ((PatientReceivables.UnallocatedBalance > 0))
	AND ((PatientReceivables.OpenBalance > 0))
ORDER BY Appointments.AppDate ASC
	,Appointments.AppTime ASC

GO