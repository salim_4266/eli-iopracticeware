﻿-- Adding rows to model.EncounterServices
ALTER TABLE model.EncounterServices ADD NDC NVARCHAR(255) NULL,
										DrugQuantity DECIMAL(18,2)  NOT NULL CONSTRAINT DF_EncounterServices_DrugQuantity DEFAULT CONVERT(DECIMAL(18,2),1),
										DrugUnitOfMeasurementId INT NOT NULL CONSTRAINT DF_EncounterServices_DrugUnitOfMeasurementId DEFAULT 5
GO

-- Populating rows to model.EncounterServices
UPDATE model.EncounterServices 
SET EncounterServices.NDC = esd.NDC,
	EncounterServices.DrugQuantity = COALESCE(esd.Quantity, CONVERT(DECIMAL(18,2),1)),
	EncounterServices.DrugUnitOfMeasurementId = COALESCE(esd.DrugUnitOfMeasurementId,5)
FROM model.EncounterServices es
INNER JOIN model.EncounterServiceDrugs esd on es.Id = esd.Id


-- Adding rows to model.FacilityEncounterServices
ALTER TABLE model.FacilityEncounterServices 
				ADD NDC NVARCHAR(255) NULL,
				DrugQuantity DECIMAL(18,2)  NOT NULL CONSTRAINT DF_FacilityEncounterServices_DrugQuantity DEFAULT CONVERT(DECIMAL(18,2),1),
				DrugUnitOfMeasurementId INT NOT NULL CONSTRAINT DF_FacilityEncounterServices_DrugUnitOfMeasurementId DEFAULT 5
GO

-- Dropping constraints on model.EncounterServiceDrugs
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterServiceEncounterServiceDrug'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterServiceDrugs] DROP CONSTRAINT FK_EncounterServiceEncounterServiceDrug

IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterServiceDrugs', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterServiceDrugs] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterServiceDrugs', 'U')
EXEC(@sql)
END

-- Dropping model.EncounterServiceDrugs 
DROP TABLE model.EncounterServiceDrugs
GO
