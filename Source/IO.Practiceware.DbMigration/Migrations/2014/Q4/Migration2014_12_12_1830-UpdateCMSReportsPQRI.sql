﻿UPDATE dbo.CMSReports SET CMSRepName = '012 POAG: Optic Nerve Evaluation' 
WHERE CONVERT(NVARCHAR(100),CMSQuery) = 'USP_PQRI_GetPOAGDetails'

UPDATE dbo.CMSReports SET CMSRepName = '014 AMD: Dilated Macular Examination'
WHERE CONVERT(NVARCHAR(100),CMSQuery) = 'USP_PQRI_GetAMD_Dilated_Macular_Examination'

UPDATE dbo.CMSReports SET CMSRepName = '018 Diabetic Retinopathy: Documentation of Macular Edema and Severity of Retinopathy' 
WHERE CONVERT(NVARCHAR(100),CMSQuery) = 'USP_PQRI_GetDiabetic_Retinopathy_MacularEdema'

UPDATE dbo.CMSReports SET CMSRepName = '019 Diabetic Retinopathy: Communication with the Physician Managing On-Diabetes Care' 
WHERE CONVERT(NVARCHAR(100),CMSQuery) = 'USP_PQRI_GetDiabetic_Retinopathy_Communication_Physician'

UPDATE dbo.CMSReports SET CMSRepName = '117 Diabetes Mellitus: Dilated Eye Exam in Diabetic Patient' 
WHERE CONVERT(NVARCHAR(100),CMSQuery) = 'USP_PQRI_Diabetes_GetMellitus_Dilated_Eye'

UPDATE dbo.CMSReports SET CMSRepName = '140 AMD: Counseling on Antioxidant Supplement' 
WHERE CONVERT(NVARCHAR(100),CMSQuery) = 'USP_PQRI_GetAMD_AREDS_Counseling'

UPDATE dbo.CMSReports SET CMSRepName = '141 POAG: Reduction of IOP or Documentation of Plan' 
WHERE CONVERT(NVARCHAR(100),CMSQuery) = 'USP_PQRI_GetPOAG_ReducePressure'

UPDATE dbo.CMSReports SET CMSRepName = '191 Cataracts: 20/40 or Better Visual Acuity within 90 Days Following Cataract Surgery' 
WHERE CONVERT(NVARCHAR(100),CMSQuery) = 'USP_PQRI_GetCataracts_Visual_Acuity'

UPDATE dbo.CMSReports SET CMSRepName = '192 Cataracts: Complications within 30 Days Following Cataract Surgery Requiring Additional Surgery' 
WHERE CONVERT(NVARCHAR(100),CMSQuery) = 'USP_PQRI_GetCataracts_Complications'

UPDATE dbo.CMSReports SET CMSRepName = '130 Documentation of Current Medications in the Medical Record' 
WHERE CONVERT(NVARCHAR(100),CMSQuery) = 'USP_PQRI_GetCurrentMeds'

UPDATE dbo.CMSReports SET CMSRepName = '226 Tobacco Use Screening and Cessation Intervention' 
WHERE CONVERT(NVARCHAR(100),CMSQuery) = 'USP_PQRI_GetTobaccoCessation'
