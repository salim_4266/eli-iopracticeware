﻿--Removing Old dbo.PracticeInsurers and PatientFinancials References.

--For Bug #15142.To Fix BillingSent or Review Claims Screen
IF OBJECT_ID('dbo.RetrieveTransactionTypeR1', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[RetrieveTransactionTypeR1]
GO

CREATE PROCEDURE [dbo].[RetrieveTransactionTypeR1] @SDate VARCHAR(8)
	,@EDate VARCHAR(8)
	,@LocId INT
	,@ResId INT
	,@TransRef VARCHAR(1)
	,@TransType VARCHAR(1)
	,@AStat1 VARCHAR(1)
	,@AStat2 VARCHAR(1)
	,@AAct VARCHAR(1)
AS
SET NOCOUNT ON
SELECT Appointments.AppointmentId
	,Appointments.AppDate
	,Appointments.AppTime
	,Appointments.ResourceId2
	,Appointments.ScheduleStatus
	,PatientDemographics.PatientId
	,PatientDemographics.Salutation
	,PatientDemographics.LastName
	,PatientDemographics.FirstName
	,PatientDemographics.MiddleInitial
	,PatientDemographics.NameReference
	,PatientDemographics.HomePhone
	,PatientDemographics.BusinessPhone
	,PatientDemographics.BirthDate
	,Resources.ResourceName
	,AppointmentType.AppointmentType
	,PracticeTransactionJournal.TransactionId
	,PracticeTransactionJournal.TransactionType
	,PracticeTransactionJournal.TransactionStatus
	,PracticeTransactionJournal.TransactionDate
	,PracticeTransactionJournal.TransactionRef
	,PracticeTransactionJournal.TransactionRemark
	,PracticeTransactionJournal.TransactionBatch
	,PracticeTransactionJournal.TransactionAction
	,Resources_1.ResourceName
	,i.Name AS InsurerName
	,PracticeTransactionJournal.TransactionRcvrId
FROM (
	model.Insurers i INNER JOIN (
		(
			Resources INNER JOIN (
				Appointments INNER JOIN (
					PracticeTransactionJournal INNER JOIN (
						PatientDemographics INNER JOIN PatientReceivables ON PatientDemographics.PatientId = PatientReceivables.PatientId
						) ON PracticeTransactionJournal.TransactionTypeId = PatientReceivables.ReceivableId
					) ON (Appointments.PatientId = PatientDemographics.PatientId)
					AND (Appointments.AppointmentId = PatientReceivables.AppointmentId)
				) ON Resources.ResourceId = Appointments.ResourceId1
			) LEFT JOIN Resources AS Resources_1 ON Appointments.ResourceId2 = Resources_1.ResourceId
		) ON i.Id = PatientReceivables.InsurerId
	)
LEFT JOIN AppointmentType ON Appointments.AppTypeId = AppointmentType.AppTypeId
WHERE (
		(PracticeTransactionJournal.TransactionRef = @TransRef)
		OR (@TransRef = '0')
		)
	AND (
		(PracticeTransactionJournal.TransactionType = @TransType)
		OR (@TransType = '0')
		)
	AND ((PracticeTransactionJournal.TransactionDate <= @SDate))
	AND (
		(PracticeTransactionJournal.TransactionDate >= @EDate)
		OR (@EDate = '0')
		)
	AND (
		(PracticeTransactionJournal.TransactionAction = @AAct)
		OR (@AAct = '0')
		)
	AND (
		(
			(PracticeTransactionJournal.TransactionStatus = @AStat1)
			OR (@AStat1 = '0')
			)
		OR (
			(PracticeTransactionJournal.TransactionStatus = @AStat2)
			OR (@AStat2 = '0')
			)
		)
	AND (
		(Appointments.ResourceId1 = @ResId)
		OR (@ResId = - 1)
		)
	AND (
		(Appointments.ResourceId2 = @LocId)
		OR (@LocId = - 1)
		)
ORDER BY PracticeTransactionJournal.TransactionDate DESC
GO

IF OBJECT_ID('dbo.RetrieveTransactionType1', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[RetrieveTransactionType1]
GO

CREATE PROCEDURE RetrieveTransactionType1 @SDate VARCHAR(8)
	,@EDate VARCHAR(8)
	,@LocId INT
	,@ResId INT
	,@TransRef VARCHAR(1)
	,@TransType VARCHAR(1)
	,@AStat1 VARCHAR(1)
	,@AStat2 VARCHAR(1)
	,@AAct VARCHAR(1)
AS
SET NOCOUNT ON
SELECT Appointments.AppointmentId
	,Appointments.AppDate
	,Appointments.AppTime
	,Appointments.ResourceId2
	,Appointments.ScheduleStatus
	,PatientDemographics.PatientId
	,PatientDemographics.Salutation
	,PatientDemographics.LastName
	,PatientDemographics.FirstName
	,PatientDemographics.MiddleInitial
	,PatientDemographics.NameReference
	,PatientDemographics.HomePhone
	,PatientDemographics.BusinessPhone
	,PatientDemographics.BirthDate
	,Resources.ResourceName
	,AppointmentType.AppointmentType
	,PracticeTransactionJournal.TransactionId
	,PracticeTransactionJournal.TransactionType
	,PracticeTransactionJournal.TransactionStatus
	,PracticeTransactionJournal.TransactionDate
	,PracticeTransactionJournal.TransactionRef
	,PracticeTransactionJournal.TransactionRemark
	,PracticeTransactionJournal.TransactionBatch
	,PracticeTransactionJournal.TransactionAction
	,Resources_1.ResourceName
	,i.Name AS InsurerName
	,PracticeTransactionJournal.TransactionRcvrId
FROM (
	model.Insurers i INNER JOIN (
		(
			Resources INNER JOIN (
				Appointments INNER JOIN (
					PracticeTransactionJournal INNER JOIN (
						PatientDemographics INNER JOIN PatientReceivables ON PatientDemographics.PatientId = PatientReceivables.PatientId
						) ON PracticeTransactionJournal.TransactionTypeId = PatientReceivables.ReceivableId
					) ON (Appointments.PatientId = PatientDemographics.PatientId)
					AND (Appointments.AppointmentId = PatientReceivables.AppointmentId)
				) ON Resources.ResourceId = Appointments.ResourceId1
			) LEFT JOIN Resources AS Resources_1 ON Appointments.ResourceId2 = Resources_1.ResourceId
		) ON i.Id = PatientReceivables.InsurerId
	)
LEFT JOIN AppointmentType ON Appointments.AppTypeId = AppointmentType.AppTypeId
WHERE (
		(PracticeTransactionJournal.TransactionRef = @TransRef)
		OR (@TransRef = '0')
		)
	AND (
		(PracticeTransactionJournal.TransactionType = @TransType)
		OR (@TransType = '0')
		)
	AND ((PracticeTransactionJournal.TransactionDate <= @SDate))
	AND (
		(PracticeTransactionJournal.TransactionDate >= @EDate)
		OR (@EDate = '0')
		)
	AND (
		(PatientDemographics.STATUS = 'A')
		OR (PatientDemographics.STATUS = 'I')
		)
	AND (
		(PracticeTransactionJournal.TransactionAction = @AAct)
		OR (@AAct = '0')
		)
	AND (
		(
			(PracticeTransactionJournal.TransactionStatus = @AStat1)
			OR (@AStat1 = '0')
			)
		OR (
			(PracticeTransactionJournal.TransactionStatus = @AStat2)
			OR (@AStat2 = '0')
			)
		)
	AND (
		(Appointments.ResourceId1 = @ResId)
		OR (@ResId = - 1)
		)
	AND (
		(Appointments.ResourceId2 = @LocId)
		OR (@LocId = - 1)
		)
ORDER BY PracticeTransactionJournal.TransactionDate
GO

IF OBJECT_ID('[dbo].[ClaimsList]', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[ClaimsList];
GO

CREATE PROCEDURE [dbo].[ClaimsList] @AptDate VARCHAR(8)
	,@RscId INT
	,@LocId INT
	,@PatId INT
	,@RType VARCHAR(1)
	,@BalOn INT
AS
SET NOCOUNT ON
SELECT DISTINCT Appointments.AppointmentId
	,Appointments.AppDate
	,Appointments.AppTime
	,PatientDemographics.PatientId
	,PatientDemographics.LastName
	,PatientDemographics.FirstName
	,PatientDemographics.MiddleInitial
	,PatientDemographics.NameReference
	,PatientDemographics.Salutation
	,PatientDemographics.BirthDate
	,Appointments.ScheduleStatus
	,Resources.ResourceName
	,Resources.ResourceId
	,PatientReceivables.ReceivableId
	,PatientReceivables.ReceivableType
	,PatientReceivables.Charge
	,PatientReceivables.OpenBalance
	,PatientReceivables.UnallocatedBalance
	,PatientReceivables.BillToDr
	,PatientReceivables.Invoice
	,AppointmentType.AppointmentType
	,i.Name AS InsuerName
FROM (
	(
		AppointmentType RIGHT JOIN (
			(
				Resources INNER JOIN (
					PatientDemographics INNER JOIN Appointments ON PatientDemographics.PatientId = Appointments.PatientId
					) ON Resources.ResourceId = Appointments.ResourceId1
				) INNER JOIN PatientReceivables ON (Appointments.AppointmentId = PatientReceivables.AppointmentId)
				AND (PatientDemographics.PatientId = PatientReceivables.PatientId)
			) ON AppointmentType.AppTypeId = Appointments.AppTypeId
		) LEFT JOIN Resources AS Resources_1 ON Appointments.ResourceId2 = Resources_1.ResourceId
	)
LEFT JOIN model.Insurers i ON PatientReceivables.InsurerId = i.Id
WHERE (
		(PatientDemographics.PatientId = @PatId)
		OR (@PatId = - 1)
		)
	AND ((Appointments.AppDate <= @AptDate))
	AND (
		(Appointments.ResourceId1 = @RscId)
		OR (@RscId = - 1)
		)
	AND (
		(Appointments.ResourceId2 = @LocId)
		OR (@LocId = - 1)
		)
	AND (
		(PatientReceivables.ReceivableType = @RType)
		OR (@RType = '0')
		)
	AND (
		(PatientReceivables.UnallocatedBalance > 0)
		OR (@BalOn = - 1)
		)
	AND (
		(PatientReceivables.OpenBalance > 0)
		OR (@BalOn = - 1)
		)
ORDER BY Appointments.AppDate ASC
	,Appointments.AppTime ASC
GO

IF OBJECT_ID('[dbo].[usp_DI_ExamPatInfo]', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[usp_DI_ExamPatInfo];
GO

CREATE PROCEDURE [dbo].[usp_DI_ExamPatInfo] (@ActId INT)
AS
BEGIN
	SET NOCOUNT ON

	SELECT TOP 1 *
	FROM (
		SELECT TOP 2 app1.apptinstype
			,patientinsurance.instypeone
			,pa1.activitydate
			,pa1.patientid
			,pa1.questionset
			,pa1.appointmentid
			,pd.firstname
			,pd.middleinitial
			,pd.lastname
			,pd.namereference
			,pd.policypatientid
			,pd.secondpolicypatientid
			,pd.birthdate
			,pd.gender
			,pd.nationalorigin
			,pd.pattype
			,pd.referringphysician
			,app1.visitreason
			,app1.appdate
			,app1.apptime
			,app1.apptypeid
			,CASE 
				WHEN pv.vendorname IS NULL
					THEN ''
				ELSE pv.vendorname
				END AS VendorName
			,apt.appointmenttype
			,apt.drrequired
			,apt.embasis
			,apt.question
			,CASE 
				WHEN EXISTS (
						SELECT *
						FROM patientnotes
						WHERE notetype = 'P'
							AND patientid = pa1.patientid
						)
					THEN 'Yes'
				ELSE 'No'
				END AS RedNotes
			,CASE 
				WHEN EXISTS (
						SELECT *
						FROM practiceactivity
						WHERE activitydate <= pa1.activitydate
							AND patientid = pa1.patientid
						)
					THEN CASE pa2.activityid
							WHEN @ActId
								THEN 0
							ELSE pa2.appointmentid
							END
				ELSE 0
				END AS PreviousAppointmentId
			,CASE 
				WHEN PatientInsurance.primaryinsurerone IS NULL
					THEN ''
				ELSE patientinsurance.primaryinsurerone
				END AS PrimaryInsurer
			,CASE PatientInsurance.PrimaryBusinessClass
				WHEN 1
					THEN 'AMERIH'
				WHEN 2
					THEN 'BLUES'
				WHEN 3
					THEN 'COMM'
				WHEN 4
					THEN 'MCAIDFL'
				WHEN 5
					THEN 'MCAIDNC'
				WHEN 6
					THEN 'MCAIDNJ'
				WHEN 7
					THEN 'MCAIDNV'
				WHEN 8
					THEN 'MCAIDNY'
				WHEN 9
					THEN 'MCARENJ'
				WHEN 10
					THEN 'MCARENV'
				WHEN 11
					THEN 'MCARENY'
				WHEN 12
					THEN 'TEMPLATE'
				END AS PrimaryBusinessClass
			,CASE 
				WHEN PatientInsurance.InsurerCPTClass IS NULL
					THEN ''
				ELSE PatientInsurance.InsurerCPTClass
				END AS InsurerCPTClass
			,CASE 
				WHEN PatientInsurance.PrimaryInsurerTwo IS NOT NULL
					THEN PatientInsurance.PrimaryInsurerTwo
				WHEN PatientInsurance.PrimaryInsurerTwo IS NULL
					AND PatientInsurance.SecondaryInsurerOne IS NOT NULL
					THEN PatientInsurance.SecondaryInsurerOne
				ELSE ''
				END AS SecondaryInsurer
		FROM practiceactivity pa1
		INNER JOIN patientdemographics pd ON pa1.patientid = pd.patientid
		INNER JOIN appointments app1 ON pa1.appointmentid = app1.appointmentid
		LEFT JOIN practicevendors pv ON pd.referringphysician = pv.vendorid
		INNER JOIN appointmenttype apt ON app1.apptypeid = apt.apptypeid
		LEFT JOIN practiceactivity pa2 ON pa1.patientid = pa2.patientid
		LEFT JOIN appointments app2 ON pa2.appointmentid = app2.appointmentid
		LEFT JOIN (
			SELECT pd1.patientid AS PatientId
				,i1.Name AS PrimaryInsurerOne
				,CASE pi1.InsuranceTypeId
					WHEN '2'
						THEN CONVERT(NVARCHAR(1), 'V')
					WHEN '3'
						THEN CONVERT(NVARCHAR(1), 'A')
					WHEN '4'
						THEN CONVERT(NVARCHAR(1), 'W')
					ELSE CONVERT(NVARCHAR(1), 'M')
					END AS InsTypeOne
				,'' AS InsurerCPTClass
				,i1.InsurerBusinessClassId AS PrimaryBusinessClass
				,i2.Name AS PrimaryInsurerTwo
				,CASE pi2.InsuranceTypeId
					WHEN '2'
						THEN CONVERT(NVARCHAR(1), 'V')
					WHEN '3'
						THEN CONVERT(NVARCHAR(1), 'A')
					WHEN '4'
						THEN CONVERT(NVARCHAR(1), 'W')
					ELSE CONVERT(NVARCHAR(1), 'M')
					END AS InsTypeTwo
				,i3.Name AS SecondaryInsurerOne
				,CASE pi3.InsuranceTypeId
					WHEN '2'
						THEN CONVERT(NVARCHAR(1), 'V')
					WHEN '3'
						THEN CONVERT(NVARCHAR(1), 'A')
					WHEN '4'
						THEN CONVERT(NVARCHAR(1), 'W')
					ELSE CONVERT(NVARCHAR(1), 'M')
					END AS InsTypeThree
			FROM patientdemographics pd1
			LEFT JOIN model.PatientInsurances pi1 ON pd1.policypatientid = pi1.InsuredPatientId
				AND pi1.OrdinalId = 1
				AND (
					(
						pi1.EndDateTime IS NULL
						OR pi1.EndDateTime >= GETDATE()
						)
					AND pi1.IsDeleted = 0
					)
			LEFT JOIN model.InsurancePolicies ip1 ON ip1.Id = pi1.InsurancePolicyId
			LEFT JOIN model.Insurers i1 ON ip1.InsurerId = i1.Id
			LEFT JOIN model.PatientInsurances pi2 ON pd1.policypatientid = pi2.InsuredPatientId
				AND pi2.OrdinalId = 2
				AND (
					(
						pi2.EndDateTime IS NULL
						OR pi2.EndDateTime >= GETDATE()
						)
					AND pi2.IsDeleted = 0
					)
			LEFT JOIN model.InsurancePolicies ip2 ON ip2.Id = pi2.InsurancePolicyId
			LEFT JOIN model.Insurers i2 ON ip2.InsurerId = i2.Id
			LEFT JOIN model.PatientInsurances pi3 ON pd1.secondpolicypatientid = pi3.InsuredPatientId
				AND pi3.OrdinalId = 1
				AND (
					(
						pi3.EndDateTime IS NULL
						OR pi3.EndDateTime >= GETDATE()
						)
					AND pi3.IsDeleted = 0
					)
			LEFT JOIN model.InsurancePolicies ip3 ON ip3.Id = pi3.InsurancePolicyId
			LEFT JOIN model.Insurers i3 ON ip3.InsurerId = i3.Id
			) AS PatientInsurance ON pd.patientid = patientinsurance.patientid
			AND app1.ApptInsType = PatientInsurance.InsTypeOne
		WHERE pa1.activityid = @ActId
			AND (
				pa2.activitydate <= pa1.activitydate
				OR pa2.activitydate IS NULL
				)
			AND (
				app2.comments <> 'ADD VIA BILLING'
				OR app2.comments IS NULL
				)
			AND (
				pa2.STATUS IN (
					'D'
					,'G'
					,'H'
					,'W'
					,'M'
					,'E'
					)
				OR pa2.STATUS IS NULL
				)
		ORDER BY pa2.activitydate DESC
			,app2.apptime DESC
		) AS Top2
	ORDER BY PreviousAppointmentId DESC
END