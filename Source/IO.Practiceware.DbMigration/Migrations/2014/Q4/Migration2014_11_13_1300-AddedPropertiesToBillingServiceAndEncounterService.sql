﻿IF NOT EXISTS (select * from sys.columns 
            where Name = N'PatientBillDate' and Object_ID = Object_ID(N'model.BillingServices')) 
		ALTER TABLE model.BillingServices ADD PatientBillDate DateTime NULL
GO

IF NOT EXISTS (select * from sys.columns 
            where Name = N'Ndc' and Object_ID = Object_ID(N'model.BillingServices')) 
		ALTER TABLE model.BillingServices ADD Ndc nvarchar(max) NULL
GO

IF NOT EXISTS (select * from sys.columns 
            where Name = N'Ndc' and Object_ID = Object_ID(N'model.EncounterServices')) 
		ALTER TABLE model.EncounterServices ADD Ndc nvarchar(MAX) NULL
GO

IF NOT EXISTS (select * from sys.columns 
            where Name = N'Emg' and Object_ID = Object_ID(N'model.BillingServices')) 
		ALTER TABLE model.BillingServices ADD Emg nvarchar(max) NULL
GO

IF NOT EXISTS (select * from sys.columns 
            where Name = N'Emg' and Object_ID = Object_ID(N'model.EncounterServices')) 
		ALTER TABLE model.EncounterServices ADD Emg nvarchar(MAX) NULL
GO

IF NOT EXISTS (select * from sys.columns 
            where Name = N'Epsdt' and Object_ID = Object_ID(N'model.BillingServices')) 
		ALTER TABLE model.BillingServices ADD Epsdt nvarchar(max) NULL
GO

IF NOT EXISTS (select * from sys.columns 
            where Name = N'Epsdt' and Object_ID = Object_ID(N'model.EncounterServices')) 
		ALTER TABLE model.EncounterServices ADD Epsdt nvarchar(MAX) NULL
GO

IF NOT EXISTS (select * from sys.columns 
            where Name = N'UnclassifiedServiceDescription' and Object_ID = Object_ID(N'model.BillingServices')) 
		ALTER TABLE model.BillingServices ADD UnclassifiedServiceDescription nvarchar(max) NULL
GO

IF NOT EXISTS (select * from sys.columns 
            where Name = N'UnclassifiedServiceDescription' and Object_ID = Object_ID(N'model.EncounterServices')) 
		ALTER TABLE model.EncounterServices ADD UnclassifiedServiceDescription nvarchar(MAX) NULL
GO


