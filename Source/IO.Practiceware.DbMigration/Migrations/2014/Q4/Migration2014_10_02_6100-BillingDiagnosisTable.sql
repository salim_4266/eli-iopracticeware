﻿EXEC dbo.DropObject 'model.BillingDiagnosis'
GO

CREATE TABLE [model].[BillingDiagnosis] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrdinalId] int  NULL,
    [InvoiceId] int  NOT NULL,
    [ExternalSystemEntityMappingId] int  NOT NULL,
	[LegacyPatientClinicalId] int NULL
);
GO

INSERT INTO model.BillingDiagnosis (InvoiceId, OrdinalId, ExternalSystemEntityMappingId, LegacyPatientClinicalId)
SELECT InvoiceId, OrdinalId, MIN(ExternalSystemEntityMappingId), LegacyPatientClinicalId FROM (
	SELECT 
	inv.Id AS InvoiceId
	,CASE Symptom			
		WHEN '1'
			THEN 1
		WHEN '2'
			THEN 2
		WHEN '3'
			THEN 3
		WHEN '4'
			THEN 4
		WHEN '5'
			THEN 5
		WHEN '6'
			THEN 6
		WHEN '7'
			THEN 7
		WHEN '8'
			THEN 8
		WHEN '9'
			THEN 9
		WHEN '10'
			THEN 10
		WHEN '11'
			THEN 11
		WHEN '12'
			THEN 12
	END AS OrdinalId
	,e.Id AS ExternalSystemEntityMappingId
	,pc.ClinicalId AS LegacyPatientClinicalId
	FROM dbo.PatientClinical pc
	INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
		AND ap.ScheduleStatus <> 'A'
	INNER JOIN model.Invoices inv on inv.EncounterId = pc.AppointmentId AND inv.InvoiceTypeId <> 2
	INNER JOIN model.ExternalSystemEntityMappings e ON e.ExternalSystemEntityKey = SUBSTRING(FindingDetail, 1, CASE 
				WHEN (dbo.GetMax(CHARINDEX('.', FindingDetail, CHARINDEX('.', FindingDetail, 0) + 1) - 1, 0)) > 0
					THEN (dbo.GetMax(CHARINDEX('.', FindingDetail, CHARINDEX('.', FindingDetail, 0) + 1) - 1, 0))
				ELSE LEN(FindingDetail)
				END)
		AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE name = 'ClinicalCondition')
		AND ExternalSystemEntityId = (SELECT TOP 1 Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = (SELECT TOP 1 Id FROM model.ExternalSystems WHERE name = 'Icd9')
			AND Name = 'ClinicalCondition')
	WHERE ClinicalType IN ('B', 'K')
		AND pc.Status = 'A'
	GROUP BY ap.AppointmentId, 
	inv.Id, 
	CASE Symptom
		WHEN '1'
			THEN 1
		WHEN '2'
			THEN 2
		WHEN '3'
			THEN 3
		WHEN '4'
			THEN 4
		WHEN '5'
			THEN 5
		WHEN '6'
			THEN 6
		WHEN '7'
			THEN 7
		WHEN '8'
			THEN 8
		WHEN '9'
			THEN 9
		WHEN '10'
			THEN 10
		WHEN '11'
			THEN 11
		WHEN '12'
			THEN 12
	END
	,e.Id
	,pc.ClinicalId

	UNION ALL

	SELECT
	inv.Id AS InvoiceId
	,CASE Symptom
		WHEN '1'
			THEN 1
		WHEN '2'
			THEN 2
		WHEN '3'
			THEN 3
		WHEN '4'
			THEN 4
		WHEN '5'
			THEN 5
		WHEN '6'
			THEN 6
		WHEN '7'
			THEN 7
		WHEN '8'
			THEN 8
		WHEN '9'
			THEN 9
		WHEN '10'
			THEN 10
		WHEN '11'
			THEN 11
		WHEN '12'
			THEN 12
	END AS OrdinalId
	,e.Id AS ExternalSystemEntityMappingId
	,pc.ClinicalId AS LegacyPatientClinicalId
	FROM dbo.PatientClinical pc
	INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.Appointmentid
		AND ap.Comments = 'ASC CLAIM'
	INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
		AND apEncounter.AppTypeId = ap.AppTypeId
		AND apEncounter.AppDate = ap.AppDate
		AND apEncounter.AppTime > 0 
		AND ap.AppTime = 0
		AND apEncounter.ScheduleStatus = ap.ScheduleStatus
		AND ap.ScheduleStatus = 'D'
		AND apEncounter.ResourceId1 = ap.ResourceId1
		AND apEncounter.ResourceId2 = ap.ResourceId2
	INNER JOIN model.Invoices inv on inv.EncounterId = apEncounter.AppointmentId AND inv.InvoiceTypeId = 2
	INNER JOIN model.ExternalSystemEntityMappings e ON e.ExternalSystemEntityKey = SUBSTRING(FindingDetail, 1, CASE 
				WHEN (dbo.GetMax(CHARINDEX('.', FindingDetail, CHARINDEX('.', FindingDetail, 0) + 1) - 1, 0)) > 0
					THEN (dbo.GetMax(CHARINDEX('.', FindingDetail, CHARINDEX('.', FindingDetail, 0) + 1) - 1, 0))
				ELSE LEN(FindingDetail)
				END)
		AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE name = 'ClinicalCondition')
		AND ExternalSystemEntityId = (SELECT TOP 1 Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = (SELECT TOP 1 Id FROM model.ExternalSystems WHERE name = 'Icd9')
			AND Name = 'ClinicalCondition')
	WHERE ClinicalType IN ('B', 'K')
		AND pc.Status = 'A'
	GROUP BY apEncounter.AppointmentId, 	
	inv.Id,
	CASE Symptom
		WHEN '1'
			THEN 1
		WHEN '2'
			THEN 2
		WHEN '3'
			THEN 3
		WHEN '4'
			THEN 4
		WHEN '5'
			THEN 5
		WHEN '6'
			THEN 6
		WHEN '7'
			THEN 7
		WHEN '8'
			THEN 8
		WHEN '9'
			THEN 9
		WHEN '10'
			THEN 10
		WHEN '11'
			THEN 11
		WHEN '12'
			THEN 12
	END
	,e.Id
	,pc.ClinicalId
)v 
GROUP BY v.InvoiceId, v.OrdinalId, v.LegacyPatientClinicalId 
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------
-- Creating primary key on [Id] in table 'BillingDiagnosis'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.BillingDiagnosis', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[BillingDiagnosis] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.BillingDiagnosis', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[BillingDiagnosis]
ADD CONSTRAINT [PK_BillingDiagnosis]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------
-- Creating foreign key on [InvoiceId] in table 'BillingDiagnosis'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InvoiceBillingDiagnosis'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingDiagnosis] DROP CONSTRAINT FK_InvoiceBillingDiagnosis

IF OBJECTPROPERTY(OBJECT_ID('[model].[Invoices]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingDiagnosis]
ADD CONSTRAINT [FK_InvoiceBillingDiagnosis]
    FOREIGN KEY ([InvoiceId])
    REFERENCES [model].[Invoices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InvoiceBillingDiagnosis'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InvoiceBillingDiagnosis')
	DROP INDEX IX_FK_InvoiceBillingDiagnosis ON [model].[BillingDiagnosis]
GO
CREATE INDEX [IX_FK_InvoiceBillingDiagnosis]
ON [model].[BillingDiagnosis]
    ([InvoiceId]);
GO

-- Creating foreign key on [ExternalSystemEntityMappingId] in table 'BillingDiagnosis'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BillingDiagnosisExternalSystemEntityMapping'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingDiagnosis] DROP CONSTRAINT FK_BillingDiagnosisExternalSystemEntityMapping

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalSystemEntityMappings]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingDiagnosis]
ADD CONSTRAINT [FK_BillingDiagnosisExternalSystemEntityMapping]
    FOREIGN KEY ([ExternalSystemEntityMappingId])
    REFERENCES [model].[ExternalSystemEntityMappings]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BillingDiagnosisExternalSystemEntityMapping'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_BillingDiagnosisExternalSystemEntityMapping')
	DROP INDEX IX_FK_BillingDiagnosisExternalSystemEntityMapping ON [model].[BillingDiagnosis]
GO
CREATE INDEX [IX_FK_BillingDiagnosisExternalSystemEntityMapping]
ON [model].[BillingDiagnosis]
    ([ExternalSystemEntityMappingId]);
GO

UPDATE bsbd
SET bsbd.BillingDiagnosisId = bd.Id
FROM model.BillingServiceBillingDiagnosis bsbd
JOIN model.BillingDiagnosis bd ON bd.LegacyPatientClinicalId = bsbd.BillingDiagnosisId
GO

SELECT * 
INTO dbo.DeletedBillingServiceBillingDiagnosisInvalidForeignKey
FROM model.BillingServiceBillingDiagnosis 
WHERE BillingDiagnosisId NOT IN (SELECT Id FROM model.BillingDiagnosis)
GO

DELETE FROM model.BillingServiceBillingDiagnosis WHERE BillingDiagnosisId NOT IN (SELECT Id FROM model.BillingDiagnosis) 
GO


-- Creating foreign key on [BillingServiceId] in table 'BillingServiceBillingDiagnosis'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BillingServiceBillingServiceBillingDiagnosis'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingServiceBillingDiagnosis] DROP CONSTRAINT FK_BillingServiceBillingServiceBillingDiagnosis

IF OBJECTPROPERTY(OBJECT_ID('[model].[BillingServices]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingServiceBillingDiagnosis]
ADD CONSTRAINT [FK_BillingServiceBillingServiceBillingDiagnosis]
    FOREIGN KEY ([BillingServiceId])
    REFERENCES [model].[BillingServices]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [BillingDiagnosisId] in table 'BillingServiceBillingDiagnosis'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BillingDiagnosisBillingServiceBillingDiagnosis'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BillingServiceBillingDiagnosis] DROP CONSTRAINT FK_BillingDiagnosisBillingServiceBillingDiagnosis

IF OBJECTPROPERTY(OBJECT_ID('[model].[BillingDiagnosis]'), 'IsUserTable') = 1
ALTER TABLE [model].[BillingServiceBillingDiagnosis]
ADD CONSTRAINT [FK_BillingDiagnosisBillingServiceBillingDiagnosis]
    FOREIGN KEY ([BillingDiagnosisId])
    REFERENCES [model].[BillingDiagnosis]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BillingDiagnosisBillingServiceBillingDiagnosis'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_BillingDiagnosisBillingServiceBillingDiagnosis')
	DROP INDEX IX_FK_BillingDiagnosisBillingServiceBillingDiagnosis ON [model].[BillingServiceBillingDiagnosis]
GO
CREATE INDEX [IX_FK_BillingDiagnosisBillingServiceBillingDiagnosis]
ON [model].[BillingServiceBillingDiagnosis]
    ([BillingDiagnosisId]);
GO
