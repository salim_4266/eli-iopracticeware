﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PatientReceivablePaymentsUpdate' AND parent_id = OBJECT_ID('dbo.PatientReceivablePayments'))
	DROP TRIGGER PatientReceivablePaymentsUpdate
GO

CREATE TRIGGER [dbo].[PatientReceivablePaymentsUpdate] ON [dbo].[PatientReceivablePayments]
INSTEAD OF UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @PatientReceivablePaymentsId INT
	SET @PatientReceivablePaymentsId = 11000000
	
	DECLARE @TableIdToUpdate INT
	DECLARE @IdOfThatTable INT

	DECLARE @TableToUpdate TABLE (TableT INT)
	INSERT INTO @TableToUpdate (TableT)
	SELECT 
	CASE 
		WHEN PaymentType IN ('P','X','8','R','U')
			THEN 1
		WHEN PaymentType IN ('!','|','?','%','D','&','','0','V')
			THEN 2
		WHEN (PaymentType NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V') AND PaymentType IN (
			SELECT 
			SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
			FROM PracticeCodeTable pct 
			WHERE pct.ReferenceType = 'PAYMENTTYPE' AND pct.AlternateCode IN ('C','D')
			AND (SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
			NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V'))))
			THEN 1
		WHEN (PaymentType NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V') AND (PaymentType IN (
			SELECT 
			SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
			FROM PracticeCodeTable pct 
			WHERE pct.ReferenceType = 'PAYMENTTYPE' AND pct.AlternateCode NOT IN ('C','D')
			AND (SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
			NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V'))))
			)
			THEN 2
		WHEN PaymentType = '='
			THEN 0
	END AS AffectedTable
	FROM inserted i
	
	SET @TableIdToUpdate = (SELECT TOP 1 TableT FROM @TableToUpdate)
	IF @TableIdToUpdate = 1
		SET @IdOfThatTable = (SELECT TOP 1 Id FROM model.Adjustments WHERE LegacyPaymentId = (SELECT TOP 1 PaymentId FROM inserted))
	IF @TableIdToUpdate = 2
		SET @IdOfThatTable = (SELECT TOP 1 Id FROM model.FinancialInformations WHERE LegacyPaymentId = (SELECT TOP 1 PaymentId FROM inserted))

	IF ((SELECT 
	decomposed.x
	FROM inserted i
		CROSS APPLY (
		SELECT 
		* 
		FROM model.GetIdPair(i.PaymentId,@PatientReceivablePaymentsId)
		) decomposed) = 7) -- on account credits
	BEGIN
		
		IF ((SELECT TOP 1 PaymentType FROM inserted) = '')
			DELETE FROM model.Adjustments WHERE Id = (SELECT TOP 1 Id FROM model.Adjustments WHERE LegacyPaymentId = (SELECT TOP 1 PaymentId FROM inserted))

		IF UPDATE(PaymentServiceItem)
		BEGIN
			IF ((SELECT TOP 1 ReceivableId FROM inserted) < 1000000000)
			BEGIN
				SET @TableIdToUpdate = (SELECT TOP 1 TableT FROM @TableToUpdate)
				IF @TableIdToUpdate = 1
					SET @IdOfThatTable = (SELECT TOP 1 Id FROM model.Adjustments WHERE LegacyPaymentId = (SELECT TOP 1 PaymentId FROM inserted))
				IF @TableIdToUpdate = 2
					SET @IdOfThatTable = (SELECT TOP 1 Id FROM model.FinancialInformations WHERE LegacyPaymentId = (SELECT TOP 1 PaymentId FROM inserted))
			END
		END
	END

	IF (@TableIdToUpdate = 0) --financialbatches
	BEGIN
		UPDATE fb
		SET fb.ExplanationOfBenefitsDateTime = CASE 
			WHEN i.PaymentEOBDate = '' 
				THEN NULL 
			ELSE CONVERT(DATETIME, i.PaymentEOBDate, 112) 
		END
		,fb.Amount = i.PaymentAmount
		,fb.CheckCode = i.PaymentCheck 
		,fb.FinancialSourceTypeId = CASE i.PayerType
			WHEN 'I'
				THEN 1
			WHEN 'P'
				THEN 2
			WHEN 'O'
				THEN 3
			ELSE 4
		END
		,fb.PatientId = CASE 
			WHEN (i.PayerId IS NOT NULL AND i.PayerId <> '' AND i.PayerType = 'P')
				THEN i.PayerId
		END
		,fb.InsurerId = CASE
			WHEN (i.PayerId IS NOT NULL AND i.PayerId <> '' AND i.PayerType = 'I')
				THEN i.PayerId
			END
		,fb.PaymentDateTime = CASE 
			WHEN (i.PaymentDate = '' OR i.PaymentDate IS NULL)
				THEN NULL 
			ELSE CONVERT(DATETIME, i.PaymentDate, 112) 
			END
		,fb.PaymentMethodId = pctPaymentMethod.Id
		FROM model.FinancialBatches fb
		INNER JOIN inserted i ON i.PaymentId = (11000000 * 0 + fb.Id)
		JOIN PracticeCodeTable pct ON SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) = i.PaymentType
			AND pct.ReferenceType = 'PAYMENTTYPE'
		LEFT JOIN dbo.PracticeCodeTable pctPaymentMethod ON pctPaymentMethod.ReferenceType = 'PAYABLETYPE' 
			AND i.PaymentRefType = SUBSTRING(pctPaymentMethod.Code, LEN(pctPaymentMethod.Code), 1)
		
	END
	
	ELSE IF (@TableIdToUpdate = 1) --adjustments
	BEGIN
		UPDATE adj
		SET adj.AdjustmentTypeId = CASE i.PaymentType
			WHEN 'P'
				THEN 1
			WHEN 'X'
				THEN 2
			WHEN '8'
				THEN 3
			WHEN 'R'
				THEN 4
			WHEN 'U'
				THEN 5
			ELSE pct.Id + 1000
		END
		,adj.Amount = i.PaymentAmount
		,adj.FinancialBatchId = CASE 
			WHEN i.PaymentBatchCheckId IN (0, -1) 
				THEN NULL 
			ELSE i.PaymentBatchCheckId 
		END 
		,adj.InsurerId = CASE 
			WHEN i.PayerType = 'I' AND PayerId > 0
				THEN i.PayerId
			ELSE NULL
		END
		,adj.PatientId = CASE 
			WHEN i.PayerType = 'P' 
				THEN i.PayerId 
			WHEN i.PayerType = 'I' AND i.PayerId = 99999 
				THEN i.PayerId
			ELSE NULL 
		END
		,adj.BillingServiceId = i.PaymentServiceItem
		,adj.FinancialSourceTypeId = CASE i.PayerType
			WHEN 'I'
				THEN 1
			WHEN 'P'
				THEN 2
			WHEN 'O'
				THEN 3
			ELSE 4
		END
		,adj.InvoiceReceivableId = CASE 
			WHEN i.PayerType = 'P'
				THEN (
					SELECT
					ir.Id
					FROM model.InvoiceReceivables ir
					WHERE InvoiceId = (SELECT 
					ir.InvoiceId 
					FROM model.InvoiceReceivables ir 
					INNER JOIN inserted i ON i.ReceivableId = ir.Id)
					AND ir.PatientInsuranceId IS NULL
					)
			ELSE i.ReceivableId
			END
		,adj.PostedDateTime = CONVERT(DATETIME, i.PaymentDate, 112)
		,adj.ClaimAdjustmentReasonCodeId = CASE
					WHEN (i.PaymentReason IS NULL OR i.PaymentReason = '')
						THEN 
							CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
								WHEN 'X' -- AdjustmentTypeId = 2
									THEN paymentReason45.Id -- ReasonCode 45
								WHEN '8' -- AdjustmentTypeId = 3
									THEN paymentReason45.Id -- ReasonCode 45
								ELSE 
									CAST(NULL AS int)
							END
					ELSE
						existingPaymentReason.Id --(SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'PAYMENTREASON' AND Code = prp.PaymentReason)
				 END
		,adj.ClaimAdjustmentGroupCodeId = CASE
					WHEN i.ClaimAdjustmentGroupCodeId IS NULL 
						THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
							 -- These int values correspond to the ClaimAdjustmentGroupCode enum in .NET
								WHEN 'X' -- AdjustmentTypeId = 2
									THEN 1 -- ContractualObligation
								WHEN '8' -- AdjustmentTypeId = 3
									THEN 3 -- OtherAdjustment
								WHEN 'R' -- AdjustmentTypeId = 4
									THEN 4 -- CorrectionReversal
								WHEN 'U' -- AdjustmentTypeId = 5
									THEN 4 -- CorrectionReversal
								ELSE 
									CAST(NULL AS int)
							 END
					ELSE
						i.ClaimAdjustmentGroupCodeId
				 END
		,adj.IncludeCommentOnStatement = CASE WHEN i.PaymentCommentOn = 'T' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
		,adj.Comment = i.PaymentRefId
		FROM model.Adjustments adj
		INNER JOIN inserted i ON i.PaymentId = (11000000 * 1 + adj.Id)				
		JOIN PracticeCodeTable pct ON SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) = i.PaymentType
			AND pct.ReferenceType = 'PAYMENTTYPE'
		LEFT JOIN dbo.PracticeCodeTable pctPaymentMethod ON pctPaymentMethod.ReferenceType = 'PAYABLETYPE' 
			AND i.PaymentRefType = SUBSTRING(pctPaymentMethod.Code, LEN(pctPaymentMethod.Code), 1)
		LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason45 ON paymentReason45.Id = 
			(SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45')
		LEFT OUTER JOIN dbo.PracticeCodeTable existingPaymentReason ON existingPaymentReason.Id = 
			(SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = i.PaymentReason)
		WHERE adj.Id = @IdOfThatTable
	END	

	ELSE IF (@TableIdToUpdate = 2) --financialinformations
	BEGIN
		UPDATE fi
		SET fi.Amount = i.PaymentAmountDecimal 
		,fi.BillingServiceId = i.PaymentServiceItem 
		,fi.FinancialInformationTypeId = CASE PaymentType
			WHEN '!'
				THEN 1
			WHEN '|'
				THEN 2
			WHEN '?'
				THEN 3
			WHEN '%'
				THEN 4
			WHEN 'D'
				THEN 5
			WHEN '&'
				THEN 6
			WHEN ''
				THEN 7
			WHEN '0'
				THEN 8
			WHEN 'V'
				THEN 9
			ELSE pct.Id + 1000 
		END
		,fi.FinancialSourceTypeId = CASE i.PayerType
			WHEN 'I'
				THEN 1
			WHEN 'P'
				THEN 2
			WHEN 'O'
				THEN 3
			ELSE 4
		END
		,fi.InsurerId = CASE 
		WHEN i.PayerType = 'I' AND PayerId > 0
			THEN i.PayerId
		ELSE NULL
		END
		,fi.PatientId = CASE 
			WHEN i.PayerType = 'P' 
				THEN i.PayerId 
			WHEN i.PayerType = 'I' AND i.PayerId = 99999 
				THEN i.PayerId
			ELSE NULL 
		END 
		,fi.InvoiceReceivableId = CASE 
			WHEN i.PayerType = 'P'
				THEN (
					SELECT
					ir.Id
					FROM model.InvoiceReceivables ir
					WHERE InvoiceId = (SELECT 
					ir.InvoiceId 
					FROM model.InvoiceReceivables ir 
					INNER JOIN inserted i ON i.ReceivableId = ir.Id)
					AND ir.PatientInsuranceId IS NULL
					)
			ELSE i.ReceivableId
			END
		,fi.PostedDateTime = CONVERT(DATETIME, i.PaymentDate, 112) 
		,fi.FinancialBatchId = CASE 
		WHEN i.PaymentBatchCheckId IN (0, -1) 
			THEN NULL 
		ELSE i.PaymentBatchCheckId 
		END
		,fi.ClaimAdjustmentReasonCodeId = CASE 
					WHEN i.PaymentReason IS NULL
						THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
							WHEN '!' -- FinancialInformationTypeId = 1
								THEN paymentReason1.Id -- ReasonCode 1
							WHEN '|' -- FinancialInformationTypeId = 2
								THEN paymentReason2.Id -- ReasonCode 2
							WHEN '?' -- FinancialInformationTypeId = 3
								THEN paymentReason3.Id -- ReasonCode 3
							ELSE 
								CAST(NULL AS int)
							END
					WHEN i.PaymentReason = ''
						THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
							WHEN '!' -- FinancialInformationTypeId = 1
								THEN paymentReason1.Id -- ReasonCode 1
							WHEN '|' -- FinancialInformationTypeId = 2
								THEN paymentReason2.Id -- ReasonCode 2
							WHEN '?' -- FinancialInformationTypeId = 3
								THEN paymentReason3.Id -- ReasonCode 3
							ELSE 
								CAST(NULL AS int)
							END
					ELSE
						existingPaymentReason.Id
				END
		,fi.ClaimAdjustmentGroupCodeId = CASE 
					WHEN i.ClaimAdjustmentGroupCodeId IS NULL
						THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
							-- These int values correspond to the ClaimAdjustmentGroupCode enum in .NET
							WHEN '!' -- FinincialInformationTypeId = 1
								THEN 2 
							WHEN '|' -- FinincialInformationTypeId = 2
								THEN 2 
							WHEN '?' -- FinincialInformationTypeId = 3
								THEN 2 
							WHEN 'D' -- FinincialInformationTypeId = 5
								THEN 3 
							WHEN ']' -- FinincialInformationTypeId = 7
								THEN 3 
							WHEN '0' -- FinancialInformationTypeId = 8
								THEN 2 
							ELSE 
								CAST(NULL AS int)
							END
					ELSE
						i.ClaimAdjustmentGroupCodeId
				END
		,fi.IncludeCommentOnStatement = CASE WHEN i.PaymentCommentOn = 'T' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
		,fi.Comment = i.PaymentRefId
		FROM model.FinancialInformations fi
		INNER JOIN inserted i ON i.PaymentId = (11000000 * 2 + fi.Id)		
		LEFT JOIN PracticeCodeTable pct ON SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) = i.PaymentType
			AND pct.ReferenceType = 'PAYMENTTYPE' AND (pct.AlternateCode = '' OR pct.AlternateCode IS NULL)
		LEFT JOIN dbo.PracticeCodeTable pctPaymentMethod ON pctPaymentMethod.ReferenceType = 'PAYABLETYPE' 
			AND i.PaymentRefType = SUBSTRING(pctPaymentMethod.Code, LEN(pctPaymentMethod.Code), 1)
		LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason1 ON paymentReason1.Id = 
			(SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '1')
		LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason2 ON paymentReason2.Id = 
			(SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '2')
		LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason3 ON paymentReason3.Id = 
			(SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '3')
		LEFT OUTER JOIN dbo.PracticeCodeTable existingPaymentReason ON existingPaymentReason.Id = 
			(SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = i.PaymentReason)
		WHERE fi.Id = @IdOfThatTable
	END
END

GO


