﻿IF OBJECT_ID('dbo.GetBillingServiceModifiers') IS NOT NULL
	DROP FUNCTION dbo.GetBillingServiceModifiers
GO


CREATE FUNCTION [dbo].[GetBillingServiceModifiers] (@BillingServiceId INT)
RETURNS NVARCHAR(100)
AS
BEGIN
DECLARE @Modifiers NVARCHAR(20)
SET @Modifiers = ''
SELECT @Modifiers = @Modifiers + sm.Code + ' '
FROM model.BillingServices bs
INNER JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
INNER JOIN model.ServiceModifiers sm ON sm.Id = bsm.ServiceModifierId
WHERE bs.Id = @BillingServiceId
ORDER BY bsm.OrdinalId

RETURN @Modifiers
END
