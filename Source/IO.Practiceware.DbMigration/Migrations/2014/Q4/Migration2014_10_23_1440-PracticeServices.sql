﻿IF OBJECT_ID('dbo.PracticeServices','U') IS NOT NULL
BEGIN
	SELECT *
	INTO dbo.PracticeServicesBackup
	FROM dbo.PracticeServices 

	EXEC dbo.DropObject 'dbo.PracticeServices'
END
GO

IF OBJECT_ID('dbo.PracticeServices','V') IS NOT NULL
	DROP VIEW dbo.PracticeServices
GO

CREATE VIEW dbo.PracticeServices
WITH VIEW_METADATA
AS
SELECT 
es.Id AS CodeId
,es.Code
,'P' AS CodeType
,COALESCE(UPPER(est.Name),'') AS CodeCategory
,es.Description
,CONVERT(REAL,es.UnitFee) AS Fee
,'' AS TOS
,''AS POS
,COALESCE(CONVERT(NVARCHAR(8),es.StartDateTime,112),'') AS StartDate
,COALESCE(CONVERT(NVARCHAR(8),es.EndDateTime,112),'') AS EndDate
,es.RelativeValueUnit
,CASE WHEN es.IsOrderingProviderRequiredOnClaim = 1 THEN 'T' ELSE 'F' END AS OrderDoc
,CASE WHEN es.IsReferringDoctorRequiredOnClaim = 1 THEN 'T' ELSE 'F' END AS ConsultOn
,NULL AS ClaimNote
,CASE WHEN es.IsZeroChargeAllowedOnClaim = 1 THEN 'T' ELSE 'F' END AS ServiceFilter
,COALESCE(esd.NDC,'') AS NDC
,'' AS RevenueCodeId
,0 AS IsCliaCertificateRequiredOnClaim
FROM model.EncounterServices es
LEFT JOIN model.EncounterServiceTypes est ON est.Id = es.EncounterServiceTypeId
LEFT JOIN model.EncounterServiceDrugs esd ON esd.Id = es.Id

UNION ALL

SELECT 
(1* 110000000) + fes.Id AS CodeId
,fes.Code AS Code
,'P' AS CodeType
,COALESCE(UPPER(est.Name),'') AS CodeCategory
,fes.Description
,CONVERT(REAL,fes.UnitFee) AS Fee
,'' AS TOS
,'98' POS
,COALESCE(CONVERT(NVARCHAR(8),fes.StartDateTime,112),'') AS StartDate
,COALESCE(CONVERT(NVARCHAR(8),fes.EndDateTime,112),'') AS EndDate
,fes.RelativeValueUnit
,'F' OrderDoc
,'F' AS ConsultOn
,NULL AS ClaimNote
,CASE WHEN fes.IsZeroChargeAllowedOnClaim = 1 THEN 'T' ELSE 'F' END AS ServiceFilter
,COALESCE(esd.NDC,'') AS NDC
,fes.RevenueCodeId
,fes.IsCliaCertificateRequiredOnClaim
FROM model.FacilityEncounterServices fes
LEFT JOIN model.EncounterServiceTypes est ON est.Id = fes.EncounterServiceTypeId
LEFT JOIN model.EncounterServiceDrugs esd ON esd.Id = fes.Id

GO