﻿
-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[model].[FK_AlertScreen_Alert]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[AlertScreen]'), 'IsUserTable') = 1
    ALTER TABLE [model].[AlertScreen] DROP CONSTRAINT [FK_AlertScreen_Alert];
GO
IF OBJECT_ID(N'[model].[FK_AlertScreen_Screen]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[AlertScreen]'), 'IsUserTable') = 1
    ALTER TABLE [model].[AlertScreen] DROP CONSTRAINT [FK_AlertScreen_Screen];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Check if 'AlertScreen' exists...
IF OBJECT_ID(N'model.AlertScreen', 'U') IS NULL
BEGIN
-- 'AlertScreen' does not exist, creating...
	CREATE TABLE [model].[AlertScreen] (
    [Alerts_Id] int  NOT NULL,
    [Screens_Id] int  NOT NULL
);
END


-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Alerts_Id], [Screens_Id] in table 'AlertScreen'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.AlertScreen', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[AlertScreen] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.AlertScreen', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[AlertScreen]
ADD CONSTRAINT [PK_AlertScreen]
    PRIMARY KEY NONCLUSTERED ([Alerts_Id], [Screens_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Alerts_Id] in table 'AlertScreen'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_AlertScreen_Alert'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[AlertScreen] DROP CONSTRAINT FK_AlertScreen_Alert

IF OBJECTPROPERTY(OBJECT_ID('[model].[Alerts]'), 'IsUserTable') = 1
ALTER TABLE [model].[AlertScreen]
ADD CONSTRAINT [FK_AlertScreen_Alert]
    FOREIGN KEY ([Alerts_Id])
    REFERENCES [model].[Alerts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Screens_Id] in table 'AlertScreen'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_AlertScreen_Screen'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[AlertScreen] DROP CONSTRAINT FK_AlertScreen_Screen

IF OBJECTPROPERTY(OBJECT_ID('[model].[Screens]'), 'IsUserTable') = 1
ALTER TABLE [model].[AlertScreen]
ADD CONSTRAINT [FK_AlertScreen_Screen]
    FOREIGN KEY ([Screens_Id])
    REFERENCES [model].[Screens]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AlertScreen_Screen'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_AlertScreen_Screen')
	DROP INDEX IX_FK_AlertScreen_Screen ON [model].[AlertScreen]
GO
CREATE INDEX [IX_FK_AlertScreen_Screen]
ON [model].[AlertScreen]
    ([Screens_Id]);
GO
