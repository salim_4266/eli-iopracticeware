﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PaymentMethodsInsert' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER PaymentMethodsInsert
GO

CREATE TRIGGER [dbo].[PaymentMethodsInsert] ON [dbo].[PracticeCodeTable]
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'PAYABLETYPE'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		INSERT INTO model.PaymentMethods (
			[Name]
           ,[IsArchived]
		   ,LegacyDisplayEPaymentTypes
		   ,IsReferenceRequired
		)
		SELECT 
		SUBSTRING(Code, 1, CHARINDEX(' - ', Code)) AS [Name]
		,CONVERT(BIT, 0) AS IsArchived
		,CASE WHEN AlternateCode = 'IncludeOnStatement' THEN 1 ELSE 0 END AS LegacyDisplayEPaymentTypes
		,CASE WHEN SUBSTRING(Code, 1, CHARINDEX(' - ', Code)) = 'Cash' THEN CONVERT(BIT,0)  ELSE CONVERT(BIT,1) END AS IsReferenceRequired
		FROM #inserted
	END
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PaymentMethodsUpdate' AND parent_id = OBJECT_ID('dbo.PracticeCodeTable','U'))
	DROP TRIGGER PaymentMethodsUpdate
GO

CREATE TRIGGER [dbo].[PaymentMethodsUpdate] ON [dbo].[PracticeCodeTable]
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'PAYABLETYPE'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		UPDATE p
		SET p.Name = SUBSTRING(i.Code, 1, CHARINDEX(' - ', i.Code))
			,p.LegacyDisplayEPaymentTypes = CASE WHEN i.AlternateCode = 'IncludeOnStatement' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
			,p.IsReferenceRequired = CASE WHEN SUBSTRING(i.Code, 1, CHARINDEX(' - ', i.Code)) = 'Cash' THEN CONVERT(BIT,0)  ELSE CONVERT(BIT,1) END 
		FROM model.PaymentMethods p
		JOIN deleted d ON SUBSTRING(d.Code, 1, CHARINDEX(' - ', d.Code)) = p.Name
		JOIN #inserted i ON i.Id = d.Id
	END
END
GO
