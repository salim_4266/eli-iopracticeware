﻿IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PatientReceivableServicesInsert' AND parent_id = OBJECT_ID('dbo.PatientReceivableServices','V'))
	DROP TRIGGER PatientReceivableServicesInsert
GO
	
CREATE TRIGGER PatientReceivableServicesInsert ON dbo.PatientReceivableServices
INSTEAD OF INSERT
AS
BEGIN
SET NOCOUNT ON;
DECLARE @Invoice NVARCHAR(32)
DECLARE @Service NVARCHAR(16)
DECLARE @Modifier NVARCHAR(10)
DECLARE @Quantity REAL
DECLARE @Charge REAL
DECLARE @TypeOfService NVARCHAR(32)
DECLARE @PlaceOfService NVARCHAR(32)
DECLARE @ServiceDate NVARCHAR(10)
DECLARE @LinkedDiag NVARCHAR(4)
DECLARE @Status NVARCHAR(1)
DECLARE @OrderDoc NVARCHAR(1)
DECLARE @ConsultOn NVARCHAR(1)
DECLARE @FeeCharge REAL
DECLARE @ItemOrder INT
DECLARE @ECode INT
DECLARE @PostDate NVARCHAR(8) 
DECLARE @ExternalRefInfo NVARCHAR(16)
DECLARE @QuantityDecimal DECIMAL(12, 2) 
DECLARE @ChargeDecimal DECIMAL(12, 2) 
DECLARE @FeeChargeDecimal DECIMAL(12, 2) 

DECLARE PatientReceivableServicesInsertCursor CURSOR FOR
SELECT 
Invoice 
,[Service]
,Modifier
,Quantity
,Charge
,TypeOfService
,PlaceOfService
,ServiceDate
,LinkedDiag
,[Status]
,OrderDoc
,ConsultOn
,FeeCharge
,ItemOrder
,ECode
,PostDate
,ExternalRefInfo
,QuantityDecimal
,ChargeDecimal
,FeeChargeDecimal 
FROM inserted

OPEN PatientReceivableServicesInsertCursor FETCH NEXT FROM PatientReceivableServicesInsertCursor INTO
	@Invoice, @Service, @Modifier, @Quantity, @Charge, @TypeOfService, @PlaceOfService, @ServiceDate,
	@LinkedDiag, @Status, @OrderDoc, @ConsultOn, @FeeCharge, @ItemOrder, @ECode, @PostDate, @ExternalRefInfo,
	@QuantityDecimal, @ChargeDecimal, @FeeChargeDecimal
WHILE @@FETCH_STATUS = 0 
BEGIN
	DECLARE @EncounterServiceId INT
	SET @EncounterServiceId = (SELECT TOP 1 Id FROM model.EncounterServices WHERE Code = @Service)

	DECLARE @FacilityEncounterServiceId INT
	SET @FacilityEncounterServiceId = (SELECT TOP 1 Id FROM model.FacilityEncounterServices WHERE Code = @Service)
	
	IF (@EncounterServiceId IS NULL) 
	BEGIN
		DECLARE @EncounterServiceTypeIdForMissingServiceId INT
		SET @EncounterServiceTypeIdForMissingServiceId = (
		SELECT TOP 1 Id FROM model.EncounterServiceTypes WHERE (Name LIKE 'Misc%' OR Name LIKE 'NEEDSTYPE') ORDER BY Name ASC)

		INSERT INTO model.EncounterServices (
			Code
			,[Description]
			,DefaultUnit
			,UnitFee
			,StartDateTime
			,EndDateTime
			,IsOrderingProviderRequiredOnClaim
			,IsReferringDoctorRequiredOnClaim
			,IsZeroChargeAllowedOnClaim
			,RelativeValueUnit
			,IsArchived
			,EncounterServiceTypeId
			,ServiceUnitOfMeasurementId
			,IsCliaCertificateRequiredOnClaim
			,LegacyPracticeServicesCodeId
		)
		SELECT
		@Service AS Code
		,'UNKNOWN' AS [Description]
		,CONVERT(DECIMAL(18,2), 1) AS DefaultUnit
		,1000.00 AS UnitFee
		,CONVERT(DATETIME,'20000101') AS StartDateTime
		,NULL AS EndDateTime
		,CONVERT(BIT, 0) AS IsOrderingProviderRequiredOnClaim
		,CONVERT(BIT, 0) AS IsReferringDoctorRequiredOnClaim
		,CONVERT(BIT, 0) AS IsZeroChargeAllowedOnClaim
		,CONVERT(DECIMAL(18,2), NULL) AS RelativeValueUnit
		,CONVERT(BIT, 0) AS IsArchived
		,@EncounterServiceTypeIdForMissingServiceId AS EncounterServiceTypeId
		,2 AS ServiceUnitOfMeasurementId
		,CONVERT(BIT,0) AS IsCliaCertificateRequiredOnClaim
		,NULL AS LegacyPracticeServicesCodeId

		SET @EncounterServiceId = SCOPE_IDENTITY()
	END

	DECLARE @InvoiceId INT
	SET @InvoiceId = (SELECT TOP 1 Id FROM model.Invoices WHERE LegacyPatientReceivablesInvoice = @Invoice)

	IF (@InvoiceId IS NULL) 
	BEGIN
		RAISERROR('Unable to locate a valid InvoiceId.',16,1)
		PRINT '@Invoice = ' + @Invoice
		PRINT '@ServiceDate = ' + @ServiceDate 
		PRINT '@Service = ' + @Service
		PRINT '@Status = ' + @Status
	END

	DECLARE @InvoiceType INT
	SET @InvoiceType = (SELECT TOP 1 InvoiceTypeId FROM model.Invoices WHERE Id = @InvoiceId)

	IF ((SELECT 
	TOP 1 PlaceOfServiceCode 
	FROM model.ServiceLocationCodes
	WHERE Id IN (SELECT TOP 1 ServiceLocationCodeId FROM model.Invoices WHERE Id = @InvoiceId)) <> @PlaceOfService)
	BEGIN
		UPDATE inv
		SET inv.ServiceLocationCodeId = (SELECT TOP 1 Id FROM model.ServiceLocationCodes WHERE PlaceOfServiceCode = @PlaceOfService)
		FROM model.Invoices inv
		WHERE inv.Id = @InvoiceId
	END

	DECLARE @BillingServices TABLE (Id INT NOT NULL)

	INSERT INTO model.BillingServices (
      Unit
      ,UnitCharge
      ,OrdinalId
      ,EncounterServiceId
	  ,FacilityEncounterServiceId
      ,InvoiceId
      ,ProcedureCode835Description
      ,OrderingExternalProviderId
      ,UnitAllowableExpense
      ,LegacyOrderingProviderId
      ,LegacyPatientReceivableServicesItemId
      ,LegacyPatientReceivableServicesLinkedDiag
	)
	OUTPUT inserted.Id INTO @BillingServices (Id)
	SELECT 
	CONVERT(DECIMAL(18,2),@Quantity) AS Unit
	,CONVERT(DECIMAL(18,2),@Charge) AS Charge
	,CASE 
		WHEN @ItemOrder > 0
			THEN @ItemOrder
		ELSE 1
	END AS OrdinalId
	,CASE WHEN @InvoiceType <> 2
		THEN @EncounterServiceId 
	ELSE NULL END AS EncounterServiceId
	,CASE WHEN @InvoiceType = 2
		THEN @FacilityEncounterServiceId 
	ELSE NULL END AS FacilityEncounterServiceId
	,@InvoiceId AS InvoiceId
	,CONVERT(NVARCHAR(MAX),NULL) AS ProcedureCode835Description
	,NULL AS OrderingExternalProviderId
	,CONVERT(DECIMAL(18,2),@FeeCharge) AS UnitAllowableExpense
	,NULL AS LegacyOrderingProviderId
	,NULL AS LegacyPatientReceivableServicesItemId
	,@LinkedDiag AS LegacyPatientReceivableServicesLinkedDiag
	
	;WITH CTE AS (
	SELECT 
	1 AS StartingIndex
	,2 AS LenghtIndex

	UNION ALL 

	SELECT StartingIndex + 2 AS StartingIndex
	,2 AS LenghtIndex
	FROM CTE
	WHERE StartingIndex < 7
	)
	INSERT INTO model.BillingServiceModifiers (
		OrdinalId
		,BillingServiceId
		,ServiceModifierId
	)
	SELECT 
	ROW_NUMBER() OVER (PARTITION BY b.Id ORDER BY b.Id, CTE.StartingIndex) AS OrdinalId
	,b.Id AS BillingServiceId
	,s.Id AS ServiceModifierId
	FROM CTE
	JOIN model.ServiceModifiers s ON s.Code = SUBSTRING(@Modifier,CTE.StartingIndex,CTE.LenghtIndex)
	FULL OUTER JOIN @BillingServices b ON b.Id = b.Id
	WHERE SUBSTRING(@Modifier,CTE.StartingIndex,CTE.LenghtIndex) <> ''
		AND SUBSTRING(@Modifier,CTE.StartingIndex,CTE.LenghtIndex) IS NOT NULL

	IF OBJECT_ID('dbo.ViewIdentities','U') IS NULL
	BEGIN	
		CREATE TABLE [dbo].[ViewIdentities](
		[Name] [nvarchar](max) NULL,
		[Value] [bigint] NULL
		)
	END

	IF OBJECT_ID('tempdb..#TempTableToHoldTheScopeIdentity') IS NOT NULL
		DROP TABLE #TempTableToHoldTheScopeIdentity

	CREATE TABLE #TempTableToHoldTheScopeIdentity (Id BIGINT IDENTITY(1,1) NOT NULL)
	SET IDENTITY_INSERT #TempTableToHoldTheScopeIdentity ON
	INSERT INTO #TempTableToHoldTheScopeIdentity (Id) SELECT Id AS SCOPE_ID_COLUMN FROM @BillingServices
	SET IDENTITY_INSERT #TempTableToHoldTheScopeIdentity OFF

	IF NOT EXISTS(SELECT * FROM ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
		INSERT ViewIdentities(Name, Value) VALUES ('dbo.PatientReceivableServices', @@IDENTITY)
	ELSE
		UPDATE ViewIdentities SET Value = @@IDENTITY WHERE Name = 'dbo.PatientReceivableServices'
	
	DECLARE @PatientReceivableServicesIdentity INT
	SELECT @PatientReceivableServicesIdentity = @@IDENTITY 


	INSERT INTO model.BillingServiceBillingDiagnosis ([BillingServiceId], [BillingDiagnosisId],	[OrdinalId])
	SELECT 
	*
	FROM (
		SELECT 
		BillingServiceId
		,BillingDiagnosisId
		,CASE
			WHEN Symptom = '1' AND LinkedDiag = '' THEN 1
			WHEN Symptom = '2' AND LinkedDiag = ''  THEN 2
			WHEN Symptom = '3' AND LinkedDiag = ''  THEN 3
			WHEN Symptom = '4' AND LinkedDiag = ''  THEN 4
			WHEN SUBSTRING(LinkedDiag, 1, 1) = Symptom THEN 1
			WHEN SUBSTRING(LinkedDiag, 2, 1) <> SUBSTRING(LinkedDiag, 1, 1) AND SUBSTRING(LinkedDiag, 2, 1) = Symptom THEN 2
			WHEN SUBSTRING(LinkedDiag, 3, 1) = Symptom THEN 3
			WHEN SUBSTRING(LinkedDiag, 4, 1) = Symptom THEN 4
		END AS OrdinalId 
		FROM (
			SELECT 
			bd.Id AS BillingDiagnosisId,
			bs.Id AS BillingServiceId, 
			Symptom AS Symptom, 
			bs.LegacyPatientReceivableServicesLinkedDiag AS LinkedDiag
			FROM model.BillingServices bs
			JOIN model.Invoices i ON i.Id = bs.InvoiceId
			INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = i.LegacyPatientReceivablesAppointmentId
				AND ClinicalType IN ('B', 'K')
				AND ISNUMERIC(Symptom) = 1	
			INNER JOIN dbo.Appointments ap on ap.AppointmentId = pc.AppointmentId
			JOIN @BillingServices ibs ON ibs.Id = bs.Id
			JOIN model.BillingDiagnosis bd ON bd.LegacyPatientClinicalId = pc.ClinicalId
			WHERE pc.[Status] = 'A'
				AND ((Symptom IN ('1', '2', '3', '4') AND bs.LegacyPatientReceivableServicesLinkedDiag = '')
				OR SUBSTRING(bs.LegacyPatientReceivableServicesLinkedDiag, 1, 1) = Symptom
				OR (SUBSTRING(bs.LegacyPatientReceivableServicesLinkedDiag, 2, 1) <> SUBSTRING(bs.LegacyPatientReceivableServicesLinkedDiag, 1, 1) AND SUBSTRING(bs.LegacyPatientReceivableServicesLinkedDiag, 2, 1) = Symptom)
				OR SUBSTRING(bs.LegacyPatientReceivableServicesLinkedDiag, 3, 1) = Symptom
				OR SUBSTRING(bs.LegacyPatientReceivableServicesLinkedDiag, 4, 1) = Symptom)
			GROUP BY Symptom, bs.Id, bd.Id, bs.LegacyPatientReceivableServicesLinkedDiag
		) AS q
	)z 
	EXCEPT 
	SELECT BillingServiceId, BillingDiagnosisId, OrdinalId FROM model.BillingServiceBillingDiagnosis

	SELECT @PatientReceivableServicesIdentity AS SCOPE_ID_COLUMN

	FETCH NEXT FROM PatientReceivableServicesInsertCursor INTO 
	@Invoice, @Service, @Modifier, @Quantity, @Charge, @TypeOfService, @PlaceOfService, @ServiceDate,
	@LinkedDiag, @Status, @OrderDoc, @ConsultOn, @FeeCharge, @ItemOrder, @ECode, @PostDate, @ExternalRefInfo,
	@QuantityDecimal, @ChargeDecimal, @FeeChargeDecimal

	CLOSE PatientReceivableServicesInsertCursor
	DEALLOCATE PatientReceivableServicesInsertCursor
END
END
GO
