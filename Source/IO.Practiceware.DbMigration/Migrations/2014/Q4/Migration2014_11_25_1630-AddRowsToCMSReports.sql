﻿IF EXISTS (SELECT object_id FROM sys.tables WHERE Name = 'CMSReports')
BEGIN

INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) 
VALUES (N'Measure #130', N'1. #130 Documentation of Current Medications in the Medical Record', N'USP_PQRI_GetCurrentMeds', N'Documentation of Current Medications in the Medical Record',
 1, N'Patients with documentation of current medications', N'All patients aged 18 years and older seen', N'PQRI', 1)

INSERT [dbo].[CMSReports] ( [CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) 
VALUES (N'Measure #226', N'1. #226 Tobacco Use Screening and Cessation Intervention', N'USP_PQRI_GetTobaccoCessation', N'Tobacco Use Screening and Cessation Intervention', 
1, N'Patients screened for tobacco use', N'All patients aged 18 years and older seen', N'PQRI', 1)

END
GO