﻿IF OBJECT_ID('dbo.NewPendingPayClaims', 'P') IS NOT NULL
	DROP PROCEDURE dbo.NewPendingPayClaims;
GO

CREATE PROCEDURE [dbo].[NewPendingPayClaims] 
 	@SDate VARCHAR(8)
	,@EDate VARCHAR(8)
	,@RscId INT
	,@LocId INT
	,@PType VARCHAR(1)
AS
SET NOCOUNT ON
DECLARE @PaymentTypeTableDeterminator INT

IF (@PType IN ('P','X','8','R','U'))
	SET @PaymentTypeTableDeterminator = 1
ELSE IF (@PType IN ('!','|','?','%','D','&','','0','V'))
	SET @PaymentTypeTableDeterminator = 2
ELSE IF (@PType NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V') AND (@PType IN (SELECT 
	SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
	FROM PracticeCodeTable pct 
	WHERE pct.ReferenceType = 'PAYMENTTYPE' AND pct.AlternateCode IN ('C','D')
	AND (SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
	NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V'))))
	)
	SET @PaymentTypeTableDeterminator = 1
ELSE IF (@PType NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V') AND (@PType IN (SELECT 
	SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
	FROM PracticeCodeTable pct 
	WHERE pct.ReferenceType = 'PAYMENTTYPE' AND pct.AlternateCode NOT IN ('C','D')
	AND (SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code)) 
	NOT IN ('P','X','8','R','U','!','|','?','%','D','&','','0','V'))))
	)
	SET @PaymentTypeTableDeterminator = 2
ELSE IF (@PType = '=')
	SET @PaymentTypeTableDeterminator = 3
ELSE SET @PaymentTypeTableDeterminator = NULL

IF (@PaymentTypeTableDeterminator = 1)
BEGIN
	;WITH CTE AS (
	SELECT
	prs.ServiceDate
	,i.LegacyPatientReceivablesPatientId AS PatientId
	,pi.InsuredPatientId AS InsuredId
	,ap.ResourceId2
	,ap.ResourceId1
	,ap.ReferralId
	,ip.InsurerId
	,prs.LinkedDiag
	,prs.Service
	,prs.Modifier
	,prs.Quantity
	,CONVERT(NVARCHAR,i.LegacyDateTime,112) AS InvoiceDate
	,prs.Charge
	,CASE 
	WHEN ((i.ReferringExternalProviderId IS NULL) OR (i.ReferringExternalProviderId = ''))
		THEN 0
	ELSE i.ReferringExternalProviderId
	END AS ReferDr
	,i.LegacyPatientReceivablesInvoice AS Invoice
	,prs.ItemId
	,ap.AppointmentId
	,prs.Status AS prsStatus
	,CASE 
		WHEN adj.AdjustmentTypeId = 1
			THEN 'P'
		WHEN adj.AdjustmentTypeId = 2
			THEN 'X'
		WHEN adj.AdjustmentTypeId = 3
			THEN '8'
		WHEN adj.AdjustmentTypeId = 4
			THEN 'R'
		WHEN adj.AdjustmentTypeId = 5
			THEN 'U'
		WHEN adj.AdjustmentTypeId > 1000
			THEN SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))
	END AS PaymentType
	,CONVERT(NVARCHAR,adj.PostedDateTime,112) AS PaymentDate
	,adj.InvoiceReceivableId AS ReceivableId
	,'A' AS prpStatus
	,adj.LegacyPaymentId AS PaymentId
	,CASE 
		WHEN adj.IncludeCommentOnStatement = 1
			THEN 'T' 
		ELSE 'F'
	END AS PaymentCommentOn
	,CONVERT(NVARCHAR(64),CASE
		WHEN adj.Comment IS NOT NULL
			THEN adj.Comment
		ELSE ''
	END) AS PaymentRefId
	,CASE 
		WHEN pctRsnCode.Code IS NOT NULL
			THEN pctRsnCode.Code
		ELSE ''		
	END AS PaymentReason
	FROM dbo.Appointments ap
	INNER JOIN model.Invoices i ON i.EncounterId = ap.AppointmentId
	INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
	INNER JOIN model.Adjustments adj ON adj.InvoiceReceivableId = ir.Id
	LEFT JOIN dbo.PatientReceivableServices prs ON adj.BillingServiceId = prs.ItemId
	INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	LEFT JOIN dbo.PracticeCodeTable pctRsnCode ON pctRsnCode.Id = adj.ClaimAdjustmentReasonCodeId 
	AND pctRsnCode.ReferenceType = 'PAYMENTREASON'
	LEFT JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
	LEFT JOIN dbo.PracticeCodeTable pct ON pct.Id = at.Id-1000
		AND pct.ReferenceType = 'PAYMENTTYPE' 
	)
	SELECT
	CTE.ServiceDate
	,CTE.PatientId
	,CTE.InsuredId
	,CTE.ResourceId2
	,CTE.ResourceId1
	,CTE.ReferralId
	,CTE.InsurerId
	,CTE.LinkedDiag
	,CTE.Service
	,CTE.Modifier
	,CTE.Quantity
	,CTE.InvoiceDate
	,CTE.Charge
	,CTE.ReferDr
	,CTE.Invoice
	,CTE.ItemId
	,CTE.AppointmentId
	,CTE.prsStatus AS [Status]
	,i.Name AS InsurerName
	,CONVERT(NVARCHAR(50), CASE  WHEN ia.Line1 <> '' AND ia.Line1 IS NOT NULL THEN ia.Line1 ELSE '' END) AS InsurerAddress
	,CTE.PaymentType
	,CTE.PaymentDate
	,CTE.ReceivableId
	,CTE.prpStatus AS [Status]
	,CTE.PaymentId
	,CTE.PaymentCommentOn
	,CTE.PaymentRefId
	,CTE.PaymentReason
	FROM model.Insurers i
	LEFT JOIN model.InsurerAddresses ia ON ia.InsurerId = i.Id
	RIGHT JOIN CTE ON i.Id = cte.InsurerId
	WHERE CTE.prpStatus <> 'X'
	AND CTE.PaymentDate <= @SDate
	AND CTE.PaymentDate >= @EDate
	AND (CTE.PaymentType = @PType OR @PType = '0')
	AND (CTE.ResourceId1 = @RscId OR @RscId = - 1)
	AND (CTE.ResourceId2 = @LocId	OR @LocId = - 1)
	ORDER BY CTE.InvoiceDate
		,CTE.PatientId
		,CTE.AppointmentId
END
IF (@PaymentTypeTableDeterminator = 2)
BEGIN
	;WITH CTE AS (
	SELECT
	prs.ServiceDate
	,i.LegacyPatientReceivablesPatientId AS PatientId
	,pi.InsuredPatientId AS InsuredId
	,ap.ResourceId2
	,ap.ResourceId1
	,ap.ReferralId
	,ip.InsurerId
	,prs.LinkedDiag
	,prs.Service
	,prs.Modifier
	,prs.Quantity
	,CONVERT(NVARCHAR,i.LegacyDateTime,112) AS InvoiceDate
	,prs.Charge
	,CASE 
	WHEN ((i.ReferringExternalProviderId IS NULL) OR (i.ReferringExternalProviderId = ''))
		THEN 0
	ELSE i.ReferringExternalProviderId
	END AS ReferDr
	,i.LegacyPatientReceivablesInvoice AS Invoice
	,prs.ItemId
	,ap.AppointmentId
	,prs.Status AS prsStatus
	,CASE 
		WHEN fi.FinancialInformationTypeId = 1
			THEN '!'
		WHEN fi.FinancialInformationTypeId = 2
			THEN '|'
		WHEN fi.FinancialInformationTypeId = 3
			THEN '?'
		WHEN fi.FinancialInformationTypeId = 4
			THEN '%'
		WHEN fi.FinancialInformationTypeId = 5
			THEN 'D'
		WHEN fi.FinancialInformationTypeId = 6
			THEN '&'
		WHEN fi.FinancialInformationTypeId = 7
			THEN ']'
		WHEN fi.FinancialInformationTypeId = 8
			THEN '0'
		WHEN fi.FinancialInformationTypeId = 9
			THEN 'V'
		WHEN fi.FinancialInformationTypeId > 1000
			THEN SUBSTRING(pctPymtType.Code, (dbo.GetMax((CHARINDEX(' - ', pctPymtType.Code)+3), 0)), LEN(pctPymtType.Code))
	END AS PaymentType
	,CONVERT(NVARCHAR,fi.PostedDateTime,112) AS PaymentDate
	,fi.InvoiceReceivableId AS ReceivableId
	,'A' AS prpStatus
	,fi.LegacyPaymentId AS PaymentId
	,CASE 
		WHEN fi.IncludeCommentOnStatement = 1
			THEN 'T' 
		ELSE 'F'
	END AS PaymentCommentOn
	,CONVERT(NVARCHAR(64),CASE
		WHEN fi.Comment IS NOT NULL
			THEN fi.Comment
		ELSE ''
	END) AS PaymentRefId
	,CASE 
		WHEN pctRsnCode.Code IS NOT NULL
			THEN pctRsnCode.Code
		ELSE ''		
	END AS PaymentReason
	FROM dbo.Appointments ap
	INNER JOIN model.Invoices i ON i.EncounterId = ap.AppointmentId
	INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
	INNER JOIN model.FinancialInformations fi ON fi.InvoiceReceivableId = ir.Id
	LEFT JOIN dbo.PatientReceivableServices prs ON fi.BillingServiceId = prs.ItemId
	INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	LEFT JOIN PracticeCodeTable pctPymtType ON pctPymtType.Id = fi.FinancialInformationTypeId-1000
		AND pctPymtType.ReferenceType = 'PAYMENTTYPE' 
	LEFT JOIN dbo.PracticeCodeTable pctRsnCode ON pctRsnCode.Id = fi.ClaimAdjustmentReasonCodeId 
		AND pctRsnCode.ReferenceType = 'PAYMENTREASON'
	)
	SELECT
	CTE.ServiceDate
	,CTE.PatientId
	,CTE.InsuredId
	,CTE.ResourceId2
	,CTE.ResourceId1
	,CTE.ReferralId
	,CTE.InsurerId
	,CTE.LinkedDiag
	,CTE.Service
	,CTE.Modifier
	,CTE.Quantity
	,CTE.InvoiceDate
	,CTE.Charge
	,CTE.ReferDr
	,CTE.Invoice
	,CTE.ItemId
	,CTE.AppointmentId
	,CTE.prsStatus AS [Status]
	,i.Name AS InsurerName
	,CONVERT(NVARCHAR(50), CASE  WHEN ia.Line1 <> '' AND ia.Line1 IS NOT NULL THEN ia.Line1 ELSE '' END) AS InsurerAddress
	,CTE.PaymentType
	,CTE.PaymentDate
	,CTE.ReceivableId
	,CTE.prpStatus AS [Status]
	,CTE.PaymentId
	,CTE.PaymentCommentOn
	,CTE.PaymentRefId
	,CTE.PaymentReason
	FROM model.Insurers i
	LEFT JOIN model.InsurerAddresses ia ON ia.InsurerId = i.Id
	RIGHT JOIN CTE ON i.Id = cte.InsurerId
	WHERE CTE.prpStatus <> 'X'
	AND CTE.PaymentDate <= @SDate --paymentdate is less than startdate
	AND CTE.PaymentDate >= @EDate --paymentdate is greater than enddate
	AND (CTE.PaymentType = @PType OR @PType = '0')
	AND (CTE.ResourceId1 = @RscId OR @RscId = - 1)
	AND (CTE.ResourceId2 = @LocId	OR @LocId = - 1)
	ORDER BY CTE.InvoiceDate
		,CTE.PatientId
		,CTE.AppointmentId
END
GO