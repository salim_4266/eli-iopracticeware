﻿IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL
DROP TABLE #NoAudit
GO

SELECT 1 AS Value INTO #NoAudit 
IF EXISTS (
		SELECT *
		FROM model.Adjustments
		WHERE FinancialBatchId IS NULL
		)
BEGIN
	SELECT *
	INTO #AdjustmentsWithoutBatchIds
	FROM model.Adjustments
	WHERE FinancialBatchId IS NULL

	SELECT awb.Id AS AdjustmentId
		,FinancialSourceTypeId
		,CASE 
			WHEN awb.PatientId IS NOT NULL
				AND p.Id IS NULL
				THEN NULL
			ELSE awb.PatientId
			END AS PatientId
		,CASE 
			WHEN awb.InsurerId IS NOT NULL
				AND i.Id IS NULL
				THEN NULL
			ELSE awb.InsurerId
			END AS InsurerId
		,PostedDateTime
		,NULL AS ExplanationOfBenefitsDateTime
		,Amount
		,NULL AS CheckCode
	INTO #NewFinancialBatches
	FROM #AdjustmentsWithoutBatchIds awb
	LEFT JOIN model.Insurers i ON i.Id = awb.InsurerId
	LEFT JOIN model.Patients p ON p.Id = awb.PatientId

	INSERT INTO model.FinancialBatches (
		FinancialSourceTypeId
		,PatientId
		,InsurerId
		,PaymentDateTime
		,ExplanationOfBenefitsDateTime
		,Amount
		,CheckCode
		)
	SELECT FinancialSourceTypeId
		,PatientId
		,InsurerId
		,PostedDateTime
		,ExplanationOfBenefitsDateTime
		,Amount
		,CheckCode
	FROM #NewFinancialBatches

	SELECT MAX(fb.Id) AS FinancialBatchId
		,nfb.AdjustmentId
	INTO #RowsToUpdate
	FROM #NewFinancialBatches nfb
	INNER JOIN model.Adjustments adj ON nfb.AdjustmentId = adj.Id
	LEFT JOIN model.FinancialBatches fb ON fb.Amount = nfb.Amount
		AND fb.CheckCode IS NULL
		AND fb.ExplanationOfBenefitsDateTime IS NULL
		AND fb.FinancialSourceTypeId = nfb.FinancialSourceTypeId
		AND (fb.InsurerId = nfb.InsurerId OR (fb.InsurerId IS NULL))
		AND (fb.PatientId = nfb.PatientId OR (fb.PatientId IS NULL))
		AND fb.PaymentDateTime = nfb.PostedDateTime
	GROUP BY nfb.FinancialSourceTypeId,
		nfb.InsurerId,
		nfb.PatientId,
		nfb.PostedDateTime,
		nfb.Amount,
		nfb.AdjustmentId

	UPDATE ad
	SET ad.FinancialBatchId = rtu.FinancialBatchId
	FROM #RowsToUpdate rtu
	INNER JOIN model.Adjustments ad ON ad.Id = rtu.AdjustmentId

	DROP TABLE #AdjustmentsWithoutBatchIds
	DROP TABLE #NewFinancialBatches
	DROP TABLE #RowsToUpdate
END
GO

IF EXISTS (
		SELECT *
		FROM model.FinancialInformations
		WHERE FinancialBatchId IS NULL
		)
BEGIN

	SELECT *
	INTO #FinancialInformationsWithoutBatchIds
	FROM model.FinancialInformations
	WHERE FinancialBatchId IS NULL

	SELECT awb.Id AS AdjustmentId
		,FinancialSourceTypeId
		,CASE 
			WHEN awb.PatientId IS NOT NULL
				AND p.Id IS NULL
				THEN NULL
			ELSE awb.PatientId
			END AS PatientId
		,CASE 
			WHEN awb.InsurerId IS NOT NULL
				AND i.Id IS NULL
				THEN NULL
			ELSE awb.InsurerId
			END AS InsurerId
		,PostedDateTime
		,NULL AS ExplanationOfBenefitsDateTime
		,Amount
		,NULL AS CheckCode
	INTO #NewFinancialBatches
	FROM #FinancialInformationsWithoutBatchIds awb
	LEFT JOIN model.Insurers i ON i.Id = awb.InsurerId
	LEFT JOIN model.Patients p ON p.Id = awb.PatientId

	INSERT INTO model.FinancialBatches (
		FinancialSourceTypeId
		,PatientId
		,InsurerId
		,PaymentDateTime
		,ExplanationOfBenefitsDateTime
		,Amount
		,CheckCode
		)
	SELECT FinancialSourceTypeId
		,PatientId
		,InsurerId
		,PostedDateTime
		,ExplanationOfBenefitsDateTime
		,Amount
		,CheckCode
	FROM #NewFinancialBatches

	SELECT MAX(fb.Id) AS FinancialBatchId
		,nfb.AdjustmentId
	INTO #RowsToUpdate
	FROM #NewFinancialBatches nfb
	INNER JOIN model.FinancialInformations fi ON nfb.AdjustmentId = fi.Id
	LEFT JOIN model.FinancialBatches fb ON fb.Amount = nfb.Amount
		AND fb.CheckCode IS NULL
		AND fb.ExplanationOfBenefitsDateTime IS NULL
		AND fb.FinancialSourceTypeId = nfb.FinancialSourceTypeId
		AND (fb.InsurerId = nfb.InsurerId OR (fb.InsurerId IS NULL))
		AND (fb.PatientId = nfb.PatientId OR (fb.PatientId IS NULL))
		AND fb.PaymentDateTime = nfb.PostedDateTime
	GROUP BY nfb.FinancialSourceTypeId,
		nfb.InsurerId,
		nfb.PatientId,
		nfb.PostedDateTime,
		nfb.Amount,
		nfb.AdjustmentId

	UPDATE fi
	SET fi.FinancialBatchId = rtu.FinancialBatchId
	FROM #RowsToUpdate rtu
	INNER JOIN model.FinancialInformations fi ON fi.Id = rtu.AdjustmentId

	DROP TABLE #FinancialInformationsWithoutBatchIds
	DROP TABLE #NewFinancialBatches
	DROP TABLE #RowsToUpdate
END
GO

IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL
DROP TABLE #NoAudit
SELECT 1 AS Value INTO #NoAudit 
GO