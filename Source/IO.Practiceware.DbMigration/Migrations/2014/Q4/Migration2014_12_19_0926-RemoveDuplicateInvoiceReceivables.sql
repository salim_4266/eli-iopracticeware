﻿declare @survivingInvoiceReceivables table (
	[InvoiceId] int,
	[PatientInsuranceId] bigint,
	[SurvivingInvoiceReceivableId] int
)

insert into @survivingInvoiceReceivables
select [InvoiceId]
	,[PatientInsuranceId]
	,min(Id) As [SurvivingInvoiceReceivableId]
from model.InvoiceReceivables
group by InvoiceId, PatientInsuranceId
having count(*) > 1

update t
set t.InvoiceReceivableId = sir.SurvivingInvoiceReceivableId
from model.BillingServiceTransactions t
join model.InvoiceReceivables ir on
	ir.Id = t.InvoiceReceivableId
join @survivingInvoiceReceivables sir on
	sir.InvoiceId = ir.InvoiceId
	and ((sir.PatientInsuranceId = ir.PatientInsuranceId) or (sir.PatientInsuranceId is null and ir.PatientInsuranceId is null))
where sir.SurvivingInvoiceReceivableId != ir.Id

update t
set t.InvoiceReceivableId = sir.SurvivingInvoiceReceivableId
from model.Adjustments t
join model.InvoiceReceivables ir on
	ir.Id = t.InvoiceReceivableId
join @survivingInvoiceReceivables sir on
	sir.InvoiceId = ir.InvoiceId
	and ((sir.PatientInsuranceId = ir.PatientInsuranceId) or (sir.PatientInsuranceId is null and ir.PatientInsuranceId is null))
where sir.SurvivingInvoiceReceivableId != ir.Id

update t
set t.InvoiceReceivableId = sir.SurvivingInvoiceReceivableId
from model.FinancialInformations t
join model.InvoiceReceivables ir on
	ir.Id = t.InvoiceReceivableId
join @survivingInvoiceReceivables sir on
	sir.InvoiceId = ir.InvoiceId
	and ((sir.PatientInsuranceId = ir.PatientInsuranceId) or (sir.PatientInsuranceId is null and ir.PatientInsuranceId is null))
where sir.SurvivingInvoiceReceivableId != ir.Id

delete ir 
from model.InvoiceReceivables ir
join @survivingInvoiceReceivables sir on
	sir.InvoiceId = ir.InvoiceId
	and ((sir.PatientInsuranceId = ir.PatientInsuranceId) or (sir.PatientInsuranceId is null and ir.PatientInsuranceId is null))
where sir.SurvivingInvoiceReceivableId != ir.Id