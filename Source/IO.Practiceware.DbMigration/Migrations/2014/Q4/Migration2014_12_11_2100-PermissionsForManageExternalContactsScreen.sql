﻿-- Create Individual Permissions
IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Id = 57)
BEGIN

SET IDENTITY_INSERT model.Permissions ON
INSERT INTO model.Permissions (Id, Name, PermissionCategoryId) VALUES (57, 'View Manage External Providers', 11)
SET IDENTITY_INSERT model.Permissions OFF
END
GO

IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Id = 58)
BEGIN

SET IDENTITY_INSERT model.Permissions ON
INSERT INTO model.Permissions (Id, Name, PermissionCategoryId) VALUES (58, 'Edit External Providers', 11)
SET IDENTITY_INSERT model.Permissions OFF
END
GO

IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Id = 59)
BEGIN

SET IDENTITY_INSERT model.Permissions ON
INSERT INTO model.Permissions (Id, Name, PermissionCategoryId) VALUES (59, 'Create External Providers', 11)
SET IDENTITY_INSERT model.Permissions OFF
END
GO

IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Id = 60)
BEGIN

SET IDENTITY_INSERT model.Permissions ON
INSERT INTO model.Permissions (Id, Name, PermissionCategoryId) VALUES (60, 'Delete External Providers', 9)
SET IDENTITY_INSERT model.Permissions OFF
END
GO

-- Migrate VB6 permssions
INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)
SELECT CONVERT(bit, 0) AS IsDenied, -- Set where is Denied = false
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id IN (57, 58, 59, 60) -- Manage External Providers: View, Edit, Create, Delete
WHERE ResourceType <> 'R'
	AND ResourceId > 0
	AND SUBSTRING(re.ResourcePermissions, 39, 1) = 1 -- Set where is Denied = false

EXCEPT

SELECT IsDenied, UserId, PermissionId 
FROM model.UserPermissions
