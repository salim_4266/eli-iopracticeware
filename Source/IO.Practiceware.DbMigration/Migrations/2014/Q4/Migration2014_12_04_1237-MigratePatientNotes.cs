﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using FluentMigrator;
using IO.Practiceware.Configuration;
using Soaf.Collections;
using Soaf.Data;

namespace IO.Practiceware.DbMigration.Migrations._2014.Q4
{
    [Migration(201412041237)]
    public class Migration201412041237MigratePatientNotes : NonFluentTransactionalUpOnlyMigration
    {
        protected override void MigrateUp(IDbConnection connection, IDbTransaction transaction)
        {
            var directoryPath = Path.Combine(ConfigurationManager.ServerDataPath, @"\Notes");
            if (Directory.Exists(directoryPath))
            {
                var files = Directory.GetFiles(directoryPath);
                if (files.Length > 0)
                {
                    var query = new StringBuilder();
                    var parameters = new Dictionary<string, object>();
                    int index = 1;
                    //Get the codes from the file.
                    var codes = connection.Execute<DataTable>(
                        "SELECT Code FROM PracticeCodeTable WHERE ReferenceType = 'BILLINGNOTES'", 
                        transaction).AsEnumerable()
                        .Select(r => r[0]);

                    files.ForEach(f =>
                    {
                        var fileName = Path.GetFileNameWithoutExtension(f.ToUpper());
                        codes.ForEach(c =>
                        {
                            if (c.ToString().ToUpper().Equals(fileName))
                            {
                                parameters.Add("@name" + index.ToString(), c.ToString());
                                parameters.Add("@value" + index.ToString(), Soaf.IO.IO.ReadAllText(f));
                                query.AppendFormat(@"INSERT INTO model.NoteTemplates(Name, Value) VALUES(@name{0},@value{0}) ", index.ToString());
                                index++;
                            }
                        });
                    });
                    if (!string.IsNullOrEmpty(query.ToString()))
                    {
                        connection.Execute(query.ToString(), transaction, parameters);
                    }
                }
            }
        }
    }
}
