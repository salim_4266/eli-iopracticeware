﻿SELECT Code, ClaimNote
INTO #TempPracticeServicesBackup
FROM PracticeServicesBackup
WHERE ClaimNote <> ''

UPDATE es
Set UnclassifiedServiceDescription = ClaimNote
FROM #TempPracticeServicesBackup t
INNER JOIN model.EncounterServices es ON es.Code = t.Code
WHERE UnclassifiedServiceDescription IS NULL OR UnclassifiedServiceDescription <> ''

DROP TABLE #TempPracticeServicesBackup