﻿--Bug #14729.

--PatientStatements
IF OBJECT_ID('dbo.PatientStatements', 'P') IS NOT NULL
	DROP PROCEDURE dbo.PatientStatements
GO


CREATE PROCEDURE [dbo].[PatientStatements] (
	@SDate VARCHAR(8)
	,@EDate VARCHAR(8)
	,@LocId INT
	,@ResId INT
	)
AS
BEGIN
	SET NOCOUNT ON;
	
	IF (OBJECT_ID('tempdb..#MaxHomePhone') IS NOT NULL)
		EXEC ('DROP TABLE #MaxHomePhone')

	IF (OBJECT_ID('tempdb..#PatientHomePhoneNumber') IS NOT NULL)
		EXEC ('DROP TABLE #PatientHomePhoneNumber')

	IF (OBJECT_ID('tempdb..#MaxBusinessPhone') IS NOT NULL)
		EXEC ('DROP TABLE #MaxBusinessPhone')

	IF (OBJECT_ID('tempdb..#PatientBusinessPhoneNumber') IS NOT NULL)
		EXEC ('DROP TABLE #PatientBusinessPhoneNumber')
	
	IF (OBJECT_ID('tempdb..#tmpBillingServiceTransactions') IS NOT NULL)
	EXEC ('DROP TABLE #tmpBillingServiceTransactions')
	
	SELECT MAX(Id) AS Id
		,PatientId
	INTO #MaxHomePhone
	FROM model.PatientPhoneNumbers
	WHERE PatientPhoneNumberTypeId = 7
	GROUP BY PatientId

	SELECT pn.AreaCode + '-' + pn.ExchangeAndSuffix AS HomePhone
		,pn.PatientId AS PatientId
	INTO #PatientHomePhoneNumber
	FROM model.PatientPhoneNumbers pn
	INNER JOIN #MaxHomePhone mhp ON mhp.Id = pn.Id
		AND mhp.PatientId = pn.PatientId

	SELECT MAX(Id) AS Id
		,PatientId
	INTO #MaxBusinessPhone
	FROM model.PatientPhoneNumbers
	WHERE PatientPhoneNumberTypeId = 2
	GROUP BY PatientId

	
	SELECT pn.AreaCode + '-' + pn.ExchangeAndSuffix AS BusinessPhone
		,pn.PatientId AS PatientId
	INTO #PatientBusinessPhoneNumber
	FROM model.PatientPhoneNumbers pn
	INNER JOIN #MaxBusinessPhone mbp ON mbp.Id = pn.Id
		AND mbp.PatientId = pn.PatientId

	SELECT ap.AppointmentId
		,ap.AppDate
		,ap.AppTime
		,ap.ResourceId2
		,ap.ScheduleStatus
		,p.Id AS PatientId
		,COALESCE(p.Salutation, '') AS Salutation
		,COALESCE(p.LastName, '') AS LastName
		,COALESCE(p.FirstName, '') AS FirstName
		,COALESCE(SUBSTRING(p.MiddleName, 1, 1), '') AS MiddleInitial
		,COALESCE(p.Suffix, '') AS NameReference
		,COALESCE(php.HomePhone, '') AS HomePhone
		,COALESCE(pbp.BusinessPhone, '') AS BusinessPhone
		,COALESCE(CONVERT(NVARCHAR(8), p.DateOfBirth, 112), '') As BirthDate
		,Resources.ResourceName AS ResourceName
		,'' AS TransactionId
		,'' AS TransactionType
		,'' AS TransactionStatus
		, CONVERT(NVARCHAR(8), bst.DATETIME, 112) AS TransactionDate
		,'' AS TransactionRef
		,'' AS TransactionRemark
		,'' AS TransactionBatch
		,'' AS TransactionAction
		,Resources.ResourceName AS ResourceName
		,'' AS TransactionRcvrId
	FROM model.BillingServiceTransactions bst
	INNER JOIN model.InvoiceReceivables ir ON bst.InvoiceReceivableId = ir.Id
	INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
	INNER JOIN model.Providers pr ON pr.Id = i.BillingProviderId
	INNER JOIN Appointments ap ON i.EncounterId = ap.AppointmentId
	INNER JOIN model.Patients p ON ap.PatientId = p.Id
	LEFT JOIN #PatientBusinessPhoneNumber pbp ON pbp.PatientId = p.Id
	LEFT JOIN #PatientHomePhoneNumber php ON php.PatientId = p.Id
	INNER JOIN Resources ON pr.UserId = Resources.ResourceId
	INNER JOIN PracticeName ON Resources.PracticeId = PracticeName.PracticeId
	WHERE ir.PatientInsuranceId IS NULL
		AND bst.BillingServiceTransactionStatusId = 1
		AND ((CONVERT(NVARCHAR(8), bst.DATETIME, 112)) <= @SDate OR (@SDate='0'))
		AND ((CONVERT(NVARCHAR(8), bst.DATETIME, 112) >= @EDate) OR (@EDate='0'))
		AND (
			(Resources.ResourceId = @ResId)
			OR (@ResId = - 1)
			)
		AND (
			(ap.ResourceId2 = @LocId)
			OR (@LocId = - 1)
			)
	GROUP BY ap.AppointmentId
		,ap.AppDate
		,ap.AppTime
		,ap.ResourceId2
		,ap.ScheduleStatus
		,p.Id
		,COALESCE(p.Salutation, '')
		,COALESCE(p.LastName, '')
		,COALESCE(p.FirstName, '')
		,COALESCE(SUBSTRING(p.MiddleName, 1, 1), '')
		,COALESCE(p.Suffix, '')
		,COALESCE(php.HomePhone, '')
		,COALESCE(pbp.BusinessPhone, '')
		,COALESCE(CONVERT(NVARCHAR(8), p.DateOfBirth, 112), '')
		,Resources.ResourceName
		,Resources.ResourceName
		,p.LastName
		,p.FirstName
		,CONVERT(NVARCHAR(8), bst.DATETIME, 112)
	ORDER BY p.LastName
		,p.FirstName
	
END
GO

--Monthly Statements
IF OBJECT_ID('dbo.MonthlyPatientStatements', 'P') IS NOT NULL
	DROP PROCEDURE dbo.MonthlyPatientStatements
GO

CREATE PROCEDURE [dbo].[MonthlyPatientStatements] (
	@SDate VARCHAR(8)
	,@EDate VARCHAR(8)
	,@LocId INT
	,@ResId INT
	)
AS
BEGIN
	SET NOCOUNT ON;

	IF (OBJECT_ID('tempdb..#MaxHomePhone') IS NOT NULL)
		EXEC ('DROP TABLE #MaxHomePhone')

	IF (OBJECT_ID('tempdb..#PatientHomePhoneNumber') IS NOT NULL)
		EXEC ('DROP TABLE #PatientHomePhoneNumber')

	IF (OBJECT_ID('tempdb..#MaxBusinessPhone') IS NOT NULL)
		EXEC ('DROP TABLE #MaxBusinessPhone')

	IF (OBJECT_ID('tempdb..#PatientBusinessPhoneNumber') IS NOT NULL)
		EXEC ('DROP TABLE #PatientBusinessPhoneNumber')

	IF (OBJECT_ID('tempdb..#tmpBillingServiceTransactions') IS NOT NULL)
		EXEC ('DROP TABLE #tmpBillingServiceTransactions')

	SELECT MAX(Id) AS Id
		,PatientId
	INTO #MaxHomePhone
	FROM model.PatientPhoneNumbers
	WHERE PatientPhoneNumberTypeId = 7
	GROUP BY PatientId

	SELECT pn.AreaCode + '-' + pn.ExchangeAndSuffix AS HomePhone
		,pn.PatientId AS PatientId
	INTO #PatientHomePhoneNumber
	FROM model.PatientPhoneNumbers pn
	INNER JOIN #MaxHomePhone mhp ON mhp.Id = pn.Id
		AND mhp.PatientId = pn.PatientId

	SELECT MAX(Id) AS Id
		,PatientId
	INTO #MaxBusinessPhone
	FROM model.PatientPhoneNumbers
	WHERE PatientPhoneNumberTypeId = 2
	GROUP BY PatientId

	SELECT pn.AreaCode + '-' + pn.ExchangeAndSuffix AS BusinessPhone
		,pn.PatientId AS PatientId
	INTO #PatientBusinessPhoneNumber
	FROM model.PatientPhoneNumbers pn
	INNER JOIN #MaxBusinessPhone mbp ON mbp.Id = pn.Id
		AND mbp.PatientId = pn.PatientId;

	WITH adjusts
	AS (
		SELECT SUM(CASE 
					WHEN adjT.IsDebit = 1
						THEN (
								CASE 
									WHEN adj.Amount > 0
										THEN - adj.Amount
									ELSE adj.Amount
									END
								)
					ELSE adj.Amount
					END) AS Adjust
			,ir.InvoiceId
		FROM model.Adjustments adj WITH (NOLOCK)
		INNER JOIN model.AdjustmentTypes adjT WITH (NOLOCK) ON adjT.Id = adj.AdjustmentTypeId
		INNER JOIN model.InvoiceReceivables ir WITH (NOLOCK) ON ir.Id = adj.InvoiceReceivableId
		GROUP BY ir.InvoiceId
		)
	SELECT ap.AppointmentId
		,ap.AppDate
		,ap.AppTime
		,ap.ResourceId2
		,ap.ScheduleStatus
		,p.Id AS PatientId
		,COALESCE(p.Salutation, '') AS Salutation
		,COALESCE(p.LastName, '') AS LastName
		,COALESCE(p.FirstName, '') AS FirstName
		,COALESCE(SUBSTRING(p.MiddleName, 1, 1), '') AS MiddleInitial
		,COALESCE(p.Suffix, '') AS NameReference
		,COALESCE(php.HomePhone, '') AS HomePhone
		,COALESCE(pbp.BusinessPhone, '') AS BusinessPhone
		,COALESCE(CONVERT(NVARCHAR(8), p.DateOfBirth, 112), '') AS BirthDate
		,'' AS TransactionId
		,'' AS TransactionType
		,'' AS TransactionStatus
		,CONVERT(NVARCHAR(8), bst.DATETIME, 112) AS TransactionDate
		,'' AS TransactionRef
		,'' AS TransactionRemark
		,'' AS TransactionBatch
		,'' AS TransactionAction
		,Resources.ResourceName AS ResourceName
		,'' AS TransactionRcvrId
	FROM model.BillingServiceTransactions bst
	INNER JOIN model.InvoiceReceivables ir ON bst.InvoiceReceivableId = ir.Id
	INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
	INNER JOIN model.Providers pr ON pr.Id = i.BillingProviderId
	INNER JOIN Appointments ap ON i.EncounterId = ap.AppointmentId
	INNER JOIN model.Patients p ON ap.PatientId = p.Id
	LEFT JOIN #PatientBusinessPhoneNumber pbp ON pbp.PatientId = p.Id
	LEFT JOIN #PatientHomePhoneNumber php ON php.PatientId = p.Id
	INNER JOIN Resources ON pr.UserId = Resources.ResourceId
	INNER JOIN PracticeName ON Resources.PracticeId = PracticeName.PracticeId
	LEFT JOIN model.PatientCommunicationPreferences pcp ON p.Id = pcp.PatientId AND pcp.PatientCommunicationTypeId = 3
	LEFT JOIN dbo.Charges WITH(NOEXPAND, NOLOCK) ON charges.InvoiceId = ir.InvoiceId
	LEFT JOIN adjusts ON adjusts.InvoiceId = ir.InvoiceId
	WHERE ir.PatientInsuranceId IS NULL
		AND bst.BillingServiceTransactionStatusId < 5
		AND (CONVERT(NVARCHAR(8), bst.DATETIME, 112) <= @SDate OR (@SDate = '0'))
		AND (
			(CONVERT(NVARCHAR(8), bst.DATETIME, 112) >= @EDate)
			OR (@EDate = '0')
			)
		AND (
			(Resources.ResourceId = @ResId)
			OR (@ResId = - 1)
			)
		AND (
			(ap.ResourceId2 = @LocId)
			OR (@LocId = - 1)
			)
		AND (charges.Charge - COALESCE(adjusts.Adjust, 0) > 0)
		AND (pcp.IsOptOut <> 1 OR pcp.Id IS NULL)
	GROUP BY ap.AppointmentId
		,ap.AppDate
		,ap.AppTime
		,ap.ResourceId2
		,ap.ScheduleStatus
		,p.Id
		,COALESCE(p.Salutation, '')
		,COALESCE(p.LastName, '')
		,COALESCE(p.FirstName, '')
		,COALESCE(SUBSTRING(p.MiddleName, 1, 1), '')
		,COALESCE(p.Suffix, '')
		,COALESCE(php.HomePhone, '')
		,COALESCE(pbp.BusinessPhone, '')
		,COALESCE(CONVERT(NVARCHAR(8), p.DateOfBirth, 112), '')
		,Resources.ResourceName
		,Resources.ResourceName
		,p.LastName
		,p.FirstName
		,CONVERT(NVARCHAR(8), bst.DATETIME, 112)
	ORDER BY p.LastName
		,p.FirstName
END
GO

