﻿IF OBJECT_ID('model.NoteTemplates') IS NOT NULL
	EXEC dbo.DropObject 'model.NoteTemplates'
GO

-- 'NoteTemplates' does not exist, creating...
	CREATE TABLE [model].[NoteTemplates] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Value] nvarchar(max)  NOT NULL
);
GO
-- Creating primary key on [Id] in table 'NoteTemplates'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.NoteTemplates', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[NoteTemplates] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.NoteTemplates', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[NoteTemplates]
ADD CONSTRAINT [PK_NoteTemplates]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO