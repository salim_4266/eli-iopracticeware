﻿IF (OBJECT_ID('dbo.PatientFinancialCommentBackup') IS NULL)
BEGIN
	SELECT *
	INTO dbo.PatientFinancialCommentBackup
	FROM (SELECT NoteId AS Id
	,PatientId AS PatientId
	,COALESCE(Note1,'') + ' ' + COALESCE(Note2,'') + ' ' + COALESCE(Note3,'') + ' ' + COALESCE(Note4,'') AS Value
	,CASE NoteCommentOn 
		WHEN 'T' 
		THEN CONVERT(BIT,1) 
		ELSE CONVERT(BIT,0) 
		END AS IsIncludedOnStatement
FROM dbo.PatientNotes
WHERE NoteType = 'B'
	AND PatientId <> 0
	AND AppointmentId = 0) v
END
GO

IF (OBJECT_ID('model.PatientFinancialComments') IS NOT NULL)
	AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientFinancialComments]'), 'IsView') = 1
BEGIN
	EXEC dbo.dropObject 'model.PatientFinancialComments'
END
GO

IF (OBJECT_ID('model.PatientFinancialComments') IS NULL)
BEGIN
	CREATE TABLE [model].[PatientFinancialComments] (
		Id INT identity(1, 1) NOT NULL
		,PatientId INT NOT NULL
		,Value NVARCHAR(max) NOT NULL
		,IsIncludedOnStatement BIT NOT NULL
		)
END
GO

INSERT INTO model.PatientFinancialComments (
	PatientId
	,Value
	,IsIncludedOnStatement
	)
SELECT patientid
	,COALESCE(Note1, '') + ' ' + COALESCE(Note2, '') + ' ' + COALESCE(Note3, '') + ' ' + COALESCE(Note4, '')
	,CASE NoteCommentOn
		WHEN 'T'
			THEN CONVERT(BIT, 1)
		ELSE CONVERT(BIT, 0)
		END
FROM dbo.PatientNotes pn
INNER JOIN model.Patients p ON p.Id = pn.PatientId
WHERE NoteType = 'B'
	AND PatientId <> 0
	AND AppointmentId = 0

EXCEPT

SELECT Patientid
	,Value
	,IsIncludedOnStatement
FROM model.PatientFinancialComments
GO


-- Creating primary key on [Id] in table 'PatientFinancialComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientFinancialComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientFinancialComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientFinancialComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientFinancialComments]
ADD CONSTRAINT [PK_PatientFinancialComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [PatientId] in table 'PatientFinancialComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientPatientFinancialComment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientFinancialComments] DROP CONSTRAINT FK_PatientPatientFinancialComment

IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientFinancialComments]
ADD CONSTRAINT [FK_PatientPatientFinancialComment]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPatientFinancialComment'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientPatientFinancialComment')
	DROP INDEX IX_FK_PatientPatientFinancialComment ON [model].[PatientFinancialComments]
GO
CREATE INDEX [IX_FK_PatientPatientFinancialComment]
ON [model].[PatientFinancialComments]
    ([PatientId]);
GO
