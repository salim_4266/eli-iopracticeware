UPDATE model.Reports 
SET Content = '<?xml version="1.0" encoding="utf-8"?>
<Report DataSourceName="GetPaymentsByPrimaryInsurer" Width="10.8000001907349in" Name="Payments By Primary Insurer" xmlns="http://schemas.telerik.com/reporting/2012/3.5">
  <Style>
    <Font Size="8pt" />
  </Style>
  <DataSources>
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="model.GetPaymentsByPrimaryInsurers" SelectCommandType="StoredProcedure" CommandTimeout="240" Name="GetPaymentsByPrimaryInsurer">
      <Parameters>
        <SqlDataSourceParameter DbType="DateTime" Name="@StartDate">
          <Value>
            <String>=Parameters.StartDate.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="DateTime" Name="@EndDate">
          <Value>
            <String>=Parameters.EndDate.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="String" Name="@BillingOrganizations">
          <Value>
            <String>=Join(",",Parameters.BillingOrganization.Value)</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="String" Name="@AttributeToServiceLocations">
          <Value>
            <String>=Join(",", Parameters.AttributeToServiceLocation.Value)</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="String" Name="@ServiceLocations">
          <Value>
            <String>=Join(",",Parameters.ServiceLocation.Value)</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="String" Name="@Providers">
          <Value>
            <String>=Join(",",Parameters.Provider.Value)</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="String" Name="@Insurers">
          <Value>
            <String>=Join(",",Parameters.Insurer.Value)</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="String" Name="@ServiceCategories">
          <Value>
            <String>=Join(",",Parameters.ServiceCategory.Value)</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="Boolean" Name="@ShowBillingOrganizationBreakdown">
          <Value>
            <String>=Parameters.ShowBillingOrganization.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="Boolean" Name="@ShowAttributeToServiceLocationBreakdown">
          <Value>
            <String>=Parameters.ShowAttributeToServiceLocation.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="Boolean" Name="@ShowServiceLocationBreakdown">
          <Value>
            <String>=Parameters.ShowServiceLocation.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="Boolean" Name="@ShowProviderBreakdown">
          <Value>
            <String>=Parameters.ShowDoctor.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="Boolean" Name="@ShowInsurerBreakdown">
          <Value>
            <String>=Parameters.ShowInsurer.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="Boolean" Name="@ShowServiceCategoryBreakdown">
          <Value>
            <String>=Parameters.ShowServiceCategory.Value</String>
          </Value>
        </SqlDataSourceParameter>
      </Parameters>
      <DefaultValues>
        <SqlDataSourceParameter DbType="DateTime" Name="@StartDate" />
        <SqlDataSourceParameter DbType="DateTime" Name="@EndDate" />
        <SqlDataSourceParameter DbType="String" Name="@BillingOrganizations" />
        <SqlDataSourceParameter DbType="String" Name="@AttributeToServiceLocations" />
        <SqlDataSourceParameter DbType="String" Name="@ServiceLocations" />
        <SqlDataSourceParameter DbType="String" Name="@Providers" />
        <SqlDataSourceParameter DbType="String" Name="@Insurers" />
        <SqlDataSourceParameter DbType="String" Name="@ServiceCategories" />
        <SqlDataSourceParameter DbType="Boolean" Name="@ShowBillingOrganizationBreakdown" />
        <SqlDataSourceParameter DbType="Boolean" Name="@ShowAttributeToServiceLocationBreakdown" />
        <SqlDataSourceParameter DbType="Boolean" Name="@ShowServiceLocationBreakdown" />
        <SqlDataSourceParameter DbType="Boolean" Name="@ShowProviderBreakdown" />
        <SqlDataSourceParameter DbType="Boolean" Name="@ShowInsurerBreakdown" />
        <SqlDataSourceParameter DbType="Boolean" Name="@ShowServiceCategoryBreakdown" />
      </DefaultValues>
    </SqlDataSource>
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT DISTINCT Id, ShortName &#xD;&#xA;FROM model.BillingOrganizations &#xD;&#xA;WHERE __EntityType__ = ''BillingOrganization''&#xD;&#xA;ORDER BY ShortName ASC" Name="BillingOrganizations" />
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT Id, ShortName FROM model.ServiceLocations" Name="AttributeToServiceLocation" />
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT Id, ShortName FROM model.ServiceLocations" Name="ServiceLocations" />
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT &#xD;&#xA;Id, UserName &#xD;&#xA;FROM model.Users WHERE __EntityType__ = ''Doctor''&#xD;&#xA;AND IsArchived = 0" Name="Providers" />
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT DISTINCT Id, Name FROM model.Insurers" Name="Insurers" />
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT Id, Name&#xD;&#xA;FROM model.EncounterServiceTypes" Name="ServiceCategory" />
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT TOP 1 &#xD;&#xA;&#x9;a.Id&#xD;&#xA;&#x9;,bo.Name AS MainOfficeName&#xD;&#xA;&#x9;,a.Line1 AS MainOfficeLine1&#xD;&#xA;&#x9;,a.Line2 AS MainOfficeLine2&#xD;&#xA;&#x9;,a.City AS MainOfficeCity&#xD;&#xA;&#x9;,sop.Abbreviation AS MainOfficeState&#xD;&#xA;&#x9;,SUBSTRING(a.PostalCode, 0,6) AS MainOfficePostalCode&#xD;&#xA;FROM model.BillingOrganizationAddresses a&#xD;&#xA;JOIN model.StateOrProvinces sop ON a.StateOrProvinceId = sop.Id&#xD;&#xA;JOIN model.BillingOrganizations bo ON bo.Id = a.BillingOrganizationId&#xD;&#xA;WHERE bo.IsMain = 1" CommandTimeout="240" Name="GetMainOfficeAddress" />
  </DataSources>
  <Items>
    <PageHeaderSection PrintOnFirstPage="True" Height="0.200000047683716in" Name="pageHeaderSection1">
      <Items>
        <TextBox Width="10.8000005086263in" Height="0.199999968210856in" Left="0.00000000000000067in" Top="0in" Value="Page {PageNumber}" Anchoring="Right" Name="textBox19">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <HtmlTextBox Width="1.18110239505768in" Height="0.199999978144963in" Left="0.00003941853841146in" Top="0in" Value="PaymentsAndAdjustments" Name="ReportCategory">
          <Style Visible="False" />
        </HtmlTextBox>
      </Items>
    </PageHeaderSection>
    <DetailSection PageBreak="None" Height="0.200000127156576in" Name="detailSection1">
      <Items>
        <TextBox Width="2.3957545598348in" Height="0.2in" Left="0in" Top="0in" Value="=Fields.Service" CanGrow="False" Name="textBox39">
          <Style VerticalAlign="Middle" />
        </TextBox>
        <HtmlTextBox Width="0.104087829589844in" Height="0.199958483378092in" Left="2.39583333333333in" Top="0in" Value="This report lists services with payments and adjustments of the payer designated as primary. Charges and quantity that were linked to those payments are also displayed, but are not restricted t to the payment date range. Additional column of Other Payments and Adjustments are those where the payer is not designated as primary posted during the time period.This report lists services with payments and adjustments of the payer designated as primary. Charges and quantity that were linked to those payments are also displayed, but are not restricted t to the payment date range. Additional column of Other Payments and Adjustments are those where the payer is not designated as primary posted during the time period." Name="htmlTextBox1">
          <Style Visible="False" />
        </HtmlTextBox>
        <TextBox Width="1.10000002384186in" Height="0.200000002980232in" Left="7.5in" Top="0in" Value="= Sum(Fields.PrimaryAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox22">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.09216272830963in" Height="0.200000002980232in" Left="8.60271199544271in" Top="0in" Value="= Sum(Fields.OtherAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox40">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.09559845924377in" Height="0.200000002980232in" Left="6.40023549397786in" Top="0in" Value="= Sum(Fields.TotalPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox41">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.09591190020243in" Height="0.200000002980232in" Left="5.30424499511719in" Top="0in" Value="= Sum(Fields.OtherPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox43">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.09663387139638in" Height="0.200000002980232in" Left="4.20128281911214in" Top="0in" Value="= Sum(Fields.PrimaryPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox44">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.09254515171051in" Height="0.200000002980232in" Left="3.10216172536214in" Top="0in" Value="= Sum(Fields.Charges)" Format="{0:N2}" Anchoring="Right" Name="textBox45">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="0.602083067099253in" Height="0.200000002980232in" Left="2.5in" Top="0in" Value="= Sum(Fields.Quantity)" Format="{0:N0}" Anchoring="Right" Name="textBox46">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.10000002384186in" Height="0.200000002980232in" Left="9.69791666666667in" Top="0in" Value="= Sum(Fields.TotalAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox47">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
      </Items>
    </DetailSection>
    <ReportHeaderSection Height="2.60000006357829in" Name="reportHeaderSection1">
      <Items>
        <TextBox Width="1.79996061325073in" Height="0.199921295046806in" Left="0in" Top="1.39999993642171in" Value="AttributeToServiceLocation(s):" Name="textBox9">
          <Style VerticalAlign="Middle">
            <Font Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="1.79996061325073in" Height="0.19999997317791in" Left="0in" Top="0.799920717875163in" Value="StartDate:" Name="textBox8">
          <Style VerticalAlign="Middle">
            <Font Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="1.79996061325073in" Height="0.199921295046806in" Left="0in" Top="0.999999682108561in" Value="EndDate:" Name="textBox7">
          <Style VerticalAlign="Middle">
            <Font Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="1.79996061325073in" Height="0.199921131134033in" Left="0in" Top="1.19999980926514in" Value="BillingOrg(s):" Name="textBox6">
          <Style VerticalAlign="Middle">
            <Font Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="1.79996061325073in" Height="0.199921131134033in" Left="0in" Top="1.60000006357829in" Value="ServiceLocation(s)" Name="textBox5">
          <Style VerticalAlign="Middle">
            <Font Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="1.79996061325073in" Height="0.199921295046806in" Left="0in" Top="1.80000019073486in" Value="Provider(s):" Name="textBox4">
          <Style VerticalAlign="Middle">
            <Font Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="1.79996061325073in" Height="0.199921131134033in" Left="0in" Top="2.00000031789144in" Value="Insurer(s):" Name="textBox3">
          <Style VerticalAlign="Middle">
            <Font Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="1.79996061325073in" Height="0.199999809265137in" Left="0in" Top="2.20000060399373in" Value="ServiceCategor(y/ies):" Name="textBox2">
          <Style VerticalAlign="Middle">
            <Font Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="2.800000111262in" Height="0.300000041723252in" Left="4.20000012715658in" Top="0in" Value="Payments By Primary Insurer" Name="textBox10">
          <Style VerticalAlign="Middle">
            <Font Size="14pt" Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="9in" Height="0.2in" Left="1.80000003178914in" Top="2.00000031789144in" Value="=IsNull(Join(&quot;,&quot;, Parameters.Insurer.Label),&quot;All&quot;)" Name="textBox30">
          <Style VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="9in" Height="0.199842643737793in" Left="1.79999983310699in" Top="1.80007886886597in" Value="=IsNull(Join(&quot;,&quot;, Parameters.Provider.Label),&quot;All&quot;)" Name="textBox31">
          <Style VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="9in" Height="0.2in" Left="1.79999987284342in" Top="1.60000006357829in" Value="=IsNull(Join(&quot;,&quot;, Parameters.ServiceLocation.Label),&quot;All&quot;)" Name="textBox32">
          <Style VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="9in" Height="0.2in" Left="1.79999987284342in" Top="1.40007877349854in" Value="=IsNull(Join(&quot;,&quot;, Parameters.AttributeToServiceLocation.Label),&quot;All&quot;)" Name="textBox33">
          <Style VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="9in" Height="0.2in" Left="1.79999987284342in" Top="2.20000060399373in" Value="=IsNull(Join(&quot;,&quot;, Parameters.ServiceCategory.Label),&quot;All&quot;)" Name="textBox34">
          <Style VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="9in" Height="0.2in" Left="1.79999987284342in" Top="1.19999980926514in" Value="=IsNull(Join(&quot;,&quot;, Parameters.BillingOrganization.Label),&quot;All&quot;)" Name="textBox35">
          <Style VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="2.79992135365804in" Height="0.199921290079753in" Left="1.80000007152557in" Top="0.999999682108561in" Value="= Parameters.EndDate.Value.ToString(&quot;MM/dd/yyyy&quot;)" Name="textBox36">
          <Style VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="2.79992135365804in" Height="0.199999968210856in" Left="1.79999987284342in" Top="0.799920717875163in" Value="= Parameters.StartDate.Value.ToString(&quot;MM/dd/yyyy&quot;)" Name="textBox37">
          <Style VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.59995913505554in" Height="0.199921354651451in" Left="9.20420710245768in" Top="0in" Value="Run Date: {Now().ToString(&quot;MM/dd/yyyy&quot;)}" Name="textBox106">
          <Style TextAlign="Right" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="1.59995913505554in" Height="0.19999997317791in" Left="9.20420710245768in" Top="0.200000047683716in" Value="Run Time: {Now().ToString(&quot;hh:mm tt&quot;)}" Name="textBox107">
          <Style TextAlign="Right" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <List DataSourceName="GetMainOfficeAddress" Width="3.20000012715658in" Height="0.599960883458455in" Left="0in" Top="0in" Name="list1">
          <Body>
            <Cells>
              <TableCell RowIndex="0" ColumnIndex="0" RowSpan="1" ColumnSpan="1">
                <ReportItem>
                  <Panel Width="3.20000012715658in" Height="0.599960883458455in" Left="0in" Top="0in" Name="panel1">
                    <Items>
                      <TextBox Width="3.20000004768372in" Height="0.200000002980232in" Left="0in" Top="0in" Value="{MainOfficeName}" Name="textBox108">
                        <Style>
                          <Font Name="Arial" Size="10pt" Bold="True" />
                        </Style>
                      </TextBox>
                      <TextBox Width="3.20000004768372in" Height="0.200000002980232in" Left="0in" Top="0.399921258290609in" Value="{MainOfficeCity}, {MainOfficeState} {MainOfficePostalCode}" Name="textBox109">
                        <Style>
                          <Font Name="Arial" Size="10pt" Bold="True" />
                        </Style>
                      </TextBox>
                      <TextBox Width="3.20000004768372in" Height="0.200000002980232in" Left="0in" Top="0.199960867563883in" Value="{MainOfficeLine1}" Name="textBox110">
                        <Style>
                          <Font Name="Arial" Size="10pt" Bold="True" />
                        </Style>
                      </TextBox>
                    </Items>
                  </Panel>
                </ReportItem>
              </TableCell>
            </Cells>
            <Columns>
              <Column Width="3.20000012715658in" />
            </Columns>
            <Rows>
              <Row Height="0.599960883458455in" />
            </Rows>
          </Body>
          <Corner />
          <RowGroups>
            <TableGroup Name="DetailGroup">
              <Groupings>
                <Grouping />
              </Groupings>
            </TableGroup>
          </RowGroups>
          <ColumnGroups>
            <TableGroup Name="ColumnGroup" />
          </ColumnGroups>
        </List>
      </Items>
    </ReportHeaderSection>
    <ReportFooterSection Height="0.199999491373702in" Name="reportFooterSection1">
      <Items>
        <TextBox Width="2.4in" Height="0.2in" Left="0in" Top="0in" Value="Sum For Report:" Name="textBox18">
          <Style VerticalAlign="Middle">
            <Font Size="10pt" Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="1.09734092156093in" Height="0.200000002980232in" Left="7.50529225667318in" Top="0in" Value="= Sum(Fields.PrimaryAdjustments)" Format="{0:C2}" Anchoring="Right" Name="textBox96">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.10258040825526in" Height="0.200000002980232in" Left="8.60271199544271in" Top="0in" Value="= Sum(Fields.OtherAdjustments)" Format="{0:C2}" Anchoring="Right" Name="textBox99">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.09999976555506in" Height="0.200000002980232in" Left="6.40023676554362in" Top="0in" Value="= Sum(Fields.TotalPayments)" Format="{0:C2}" Anchoring="Right" Name="textBox100">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.09591146310171in" Height="0.200000002980232in" Left="5.30424531300863in" Top="0in" Value="= Sum(Fields.OtherPayments)" Format="{0:C2}" Anchoring="Right" Name="textBox101">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.10288345813751in" Height="0.200000002980232in" Left="4.20128281911214in" Top="0in" Value="= Sum(Fields.PrimaryPayments)" Format="{0:C2}" Anchoring="Right" Name="textBox102">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.09254562854767in" Height="0.200000002980232in" Left="3.10216172536214in" Top="0in" Value="= Sum(Fields.Charges)" Format="{0:C2}" Anchoring="Right" Name="textBox103">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="0.7009583512942in" Height="0.200000002980232in" Left="2.40112463633219in" Top="0in" Value="= Sum(Fields.Quantity)" Format="{0:N0}" Anchoring="Right" Name="textBox104">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
        <TextBox Width="1.09879622856776in" Height="0.200000002980232in" Left="9.7053705851237in" Top="0in" Value="= Sum(Fields.TotalAdjustments)" Format="{0:C2}" Anchoring="Right" Name="textBox105">
          <Style TextAlign="Right" VerticalAlign="Middle" />
        </TextBox>
      </Items>
    </ReportFooterSection>
  </Items>
  <StyleSheet>
    <StyleRule>
      <Style>
        <Padding Left="2pt" Right="2pt" />
      </Style>
      <Selectors>
        <TypeSelector Type="TextItemBase" />
        <TypeSelector Type="HtmlTextBox" />
      </Selectors>
    </StyleRule>
  </StyleSheet>
  <PageSettings>
    <PageSettings PaperKind="Letter" Landscape="True">
      <Margins>
        <MarginsU Left="0.100000001490116in" Right="0.100000001490116in" Top="0.100000001490116in" Bottom="0.100000001490116in" />
      </Margins>
    </PageSettings>
  </PageSettings>
  <Sortings>
    <Sorting Expression="= Fields.Service" Direction="Asc" />
  </Sortings>
  <Groups>
    <Group Name="group6">
      <GroupHeader>
        <GroupHeaderSection PrintOnEveryPage="True" Height="0.401962757110596in" Name="groupHeaderSection6">
          <Items>
            <TextBox Width="0.697837829589844in" Height="0.199999168515205in" Left="2.39583325386047in" Top="0.199999809265137in" Value="Quantity" Name="textBox21">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="2.39575433731079in" Height="0.200000002980232in" Left="0in" Top="0.199999809265137in" Value="Service" Name="textBox23">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.10000002384186in" Height="0.401962776978811in" Left="5.30208349227905in" Top="0in" Value="Other Payments" Name="textBox26">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.09367136160533in" Height="0.399879634380341in" Left="7.5in" Top="0in" Value="Primary Adjustments" Name="textBox29">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.10000002384186in" Height="0.401962776978811in" Left="4.19791650772095in" Top="0in" Value="Primary Payments" Name="textBox24">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.10095687707265in" Height="0.200000002980232in" Left="3.09375in" Top="0.199999809265137in" Value="Charges" Name="textBox20">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.10000002384186in" Height="0.399879634380341in" Left="9.69791666666667in" Top="0in" Value="Total Adjustments" Name="textBox25">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.10112531979879in" Height="0.399879634380341in" Left="8.59375in" Top="0in" Value="Other Adjustments" Name="textBox27">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.09775948524475in" Height="0.199999049305916in" Left="6.40216207504272in" Top="0.199999809265137in" Value="Total Payments" Name="textBox62">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
          </Items>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.0520833333333333in" Name="groupFooterSection6">
          <Style Visible="False" />
        </GroupFooterSection>
      </GroupFooter>
    </Group>
    <Group Name="group">
      <GroupHeader>
        <GroupHeaderSection Height="0.200039545694987in" Name="groupHeaderSection">
          <Items>
            <TextBox Width="4.19466781616211in" Height="0.200000127156576in" Left="0.00003941853841146in" Top="0in" Value="Billing Organization: {BillingOrganization}" Name="textBox12">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.ShowBillingOrganization.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.199920654296875in" Name="groupFooterSection">
          <Items>
            <TextBox Width="2.4in" Height="0.2in" Left="0in" Top="0in" Value="Sum For {BillingOrganization}:" Name="textBox97">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.09583330806345in" Height="0.200000002980232in" Left="7.50529098510742in" Top="0in" Value="= Sum(Fields.PrimaryAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox87">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09367122904708in" Height="0.200000002980232in" Left="8.60120391845703in" Top="0in" Value="= Sum(Fields.OtherAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox89">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10000022904326in" Height="0.200000002980232in" Left="6.40023549397786in" Top="0in" Value="= Sum(Fields.TotalPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox90">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09686941560358in" Height="0.200000002980232in" Left="5.30328782399495in" Top="0in" Value="= Sum(Fields.OtherPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox91">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09879584113756in" Height="0.200000002980232in" Left="4.20128281911214in" Top="0in" Value="= Sum(Fields.PrimaryPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox92">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09783751765886in" Height="0.200000002980232in" Left="3.10216204325358in" Top="0in" Value="= Sum(Fields.Charges)" Format="{0:N2}" Anchoring="Right" Name="textBox93">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="0.700958530108134in" Height="0.200000002980232in" Left="2.40112463633219in" Top="0in" Value="= Sum(Fields.Quantity)" Format="{0:N0}" Anchoring="Right" Name="textBox94">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10000044107437in" Height="0.200000002980232in" Left="9.70047124226888in" Top="0in" Value="= Sum(Fields.TotalAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox95">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.ShowBillingOrganization.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="=Fields.BillingOrganization" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= Fields.BillingOrganization" Direction="Asc" />
      </Sortings>
    </Group>
    <Group Name="group1">
      <GroupHeader>
        <GroupHeaderSection Height="0.200039545694987in" Name="groupHeaderSection1">
          <Items>
            <TextBox Width="4.19466781616211in" Height="0.200000127156576in" Left="0.00003941853841161in" Top="0in" Value="Attributed Service Location: {AttributedServiceLocation}" Name="textBox11">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.ShowAttributeToServiceLocation.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.200078520302971in" Name="groupFooterSection1">
          <Items>
            <TextBox Width="2.4in" Height="0.2in" Left="0in" Top="0in" Value="Sum For{AttributedServiceLocation}:" Name="textBox98">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.09583284457525in" Height="0.200000002980232in" Left="7.50529225667318in" Top="0in" Value="= Sum(Fields.PrimaryAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox79">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.0936711827914in" Height="0.200000002980232in" Left="8.60120391845703in" Top="0in" Value="= Sum(Fields.OtherAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox80">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.1006431778272in" Height="0.200000002980232in" Left="6.40023676554362in" Top="0in" Value="= Sum(Fields.TotalPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox81">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09686938921611in" Height="0.200000002980232in" Left="5.30328718821208in" Top="0in" Value="= Sum(Fields.OtherPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox82">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10192554529446in" Height="0.200000002980232in" Left="4.20128281911214in" Top="0in" Value="= Sum(Fields.PrimaryPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox83">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09879622856776in" Height="0.200000002980232in" Left="3.10120328267415in" Top="0in" Value="= Sum(Fields.Charges)" Format="{0:N2}" Anchoring="Right" Name="textBox84">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="0.700000027815501in" Height="0.200000002980232in" Left="2.40112463633219in" Top="0in" Value="= Sum(Fields.Quantity)" Format="{0:N0}" Anchoring="Right" Name="textBox85">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10369557142258in" Height="0.200000002980232in" Left="9.70047124226888in" Top="0in" Value="= Sum(Fields.TotalAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox86">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.ShowAttributeToServiceLocation.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="=Fields.AttributedServiceLocation" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= Fields.AttributedServiceLocation" Direction="Asc" />
      </Sortings>
    </Group>
    <Group Name="group2">
      <GroupHeader>
        <GroupHeaderSection Height="0.199920654296875in" Name="groupHeaderSection2">
          <Items>
            <TextBox Width="4.19466781616211in" Height="0.199841817220052in" Left="0.00003941853841146in" Top="0in" Value="Service Location: {ServiceLocation}" Name="textBox13">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.ShowServiceLocation.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.199999809265137in" Name="groupFooterSection2">
          <Items>
            <TextBox Width="2.4in" Height="0.2in" Left="0in" Top="0in" Value="Sum For {ServiceLocation}:" Name="textBox88">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.09583289114138in" Height="0.200000002980232in" Left="7.50529225667318in" Top="0in" Value="= Sum(Fields.PrimaryAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox69">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09367101701597in" Height="0.200000002980232in" Left="8.60120391845703in" Top="0in" Value="= Sum(Fields.OtherAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox72">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10000065962473in" Height="0.200000002980232in" Left="6.40023676554362in" Top="0in" Value="= Sum(Fields.TotalPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox73">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09686942926298in" Height="0.200000002980232in" Left="5.30328718821208in" Top="0in" Value="= Sum(Fields.OtherPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox74">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10192517439525in" Height="0.200000002980232in" Left="4.20128281911214in" Top="0in" Value="= Sum(Fields.PrimaryPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox75">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09879627466822in" Height="0.200000002980232in" Left="3.10120328267415in" Top="0in" Value="= Sum(Fields.Charges)" Format="{0:N2}" Anchoring="Right" Name="textBox76">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="0.699999988079071in" Height="0.200000002980232in" Left="2.40112463633219in" Top="0in" Value="= Sum(Fields.Quantity)" Format="{0:N0}" Anchoring="Right" Name="textBox77">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10273770491282in" Height="0.200000002980232in" Left="9.70047124226888in" Top="0in" Value="= Sum(Fields.TotalAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox78">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.ShowServiceLocation.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="=Fields.ServiceLocation" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= Fields.ServiceLocation" Direction="Asc" />
      </Sortings>
    </Group>
    <Group Name="group3">
      <GroupHeader>
        <GroupHeaderSection Height="0.199999496340752in" Name="groupHeaderSection3">
          <Style Visible="True" />
          <Items>
            <TextBox Width="4.1947070757548in" Height="0.199999496340752in" Left="0in" Top="0in" Value="Provider: {Provider}" Name="textBox14">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.ShowDoctor.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.199999804298083in" Name="groupFooterSection3">
          <Items>
            <TextBox Width="2.4in" Height="0.2in" Left="0in" Top="0in" Value="Sum For {Provider}:" Name="textBox71">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.10000002384186in" Height="0.200000002980232in" Left="7.50031534830729in" Top="0in" Value="= Sum(Fields.PrimaryAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox53">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09367144107819in" Height="0.200000002980232in" Left="8.60120391845703in" Top="0in" Value="= Sum(Fields.OtherAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox54">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10000060002009in" Height="0.200000002980232in" Left="6.40023549397786in" Top="0in" Value="= Sum(Fields.TotalPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox63">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09999960040053in" Height="0.200000002980232in" Left="5.30015722910563in" Top="0in" Value="= Sum(Fields.OtherPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox64">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09879595724245in" Height="0.200000002980232in" Left="4.20128281911214in" Top="0in" Value="= Sum(Fields.PrimaryPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox65">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09879597028097in" Height="0.200000002980232in" Left="3.10120360056559in" Top="0in" Value="= Sum(Fields.Charges)" Format="{0:N2}" Anchoring="Right" Name="textBox66">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="0.699921363033354in" Height="0.200000002980232in" Left="2.39999993642171in" Top="0in" Value="= Sum(Fields.Quantity)" Format="{0:N0}" Anchoring="Right" Name="textBox67">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10000065962474in" Height="0.200000002980232in" Left="9.70047124226888in" Top="0in" Value="= Sum(Fields.TotalAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox68">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.ShowDoctor.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="=Fields.Provider" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= Fields.Provider" Direction="Asc" />
      </Sortings>
    </Group>
    <Group Name="group4">
      <GroupHeader>
        <GroupHeaderSection Height="0.199999809265137in" Name="groupHeaderSection4">
          <Items>
            <TextBox Width="4.19466781616211in" Height="0.199999809265137in" Left="0.00003941853841146in" Top="0in" Value="Insurer: {Insurer}" Name="textBox15">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.ShowInsurer.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.200000007947287in" Name="groupFooterSection4">
          <Items>
            <TextBox Width="2.4in" Height="0.2in" Left="0in" Top="0in" Value="Sum For {Insurer}:" Name="textBox70">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="0.699999988079071in" Height="0.200000002980232in" Left="2.40112463633219in" Top="0in" Value="= Sum(Fields.Quantity)" Format="{0:N0}" Anchoring="Right" Name="textBox51">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10000002384186in" Height="0.200000002980232in" Left="3.10120360056559in" Top="0in" Value="= Sum(Fields.Charges)" Format="{0:N2}" Anchoring="Right" Name="textBox50">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10000002384186in" Height="0.199762538075447in" Left="4.20128281911214in" Top="0in" Value="= Sum(Fields.PrimaryPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox49">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09879542724229in" Height="0.200000002980232in" Left="5.30136203765869in" Top="0in" Value="= Sum(Fields.OtherPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox48">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10015748607111in" Height="0.200000002980232in" Left="6.40023549397786in" Top="0in" Value="= Sum(Fields.TotalPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox42">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09984319739548in" Height="0.200000002980232in" Left="7.50047175089518in" Top="0in" Value="= Sum(Fields.PrimaryAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox17">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10000002384186in" Height="0.200000002980232in" Left="8.60055033365885in" Top="0in" Value="= Sum(Fields.OtherAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox28">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10000002384186in" Height="0.200000002980232in" Left="9.70063018798828in" Top="0in" Value="= Sum(Fields.TotalAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox52">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.ShowInsurer.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="=Fields.Insurer" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= Fields.Insurer" Direction="Asc" />
      </Sortings>
    </Group>
    <Group Name="group5">
      <GroupHeader>
        <GroupHeaderSection Height="0.200000127156575in" Name="groupHeaderSection5">
          <Items>
            <TextBox Width="4.19466781616211in" Height="0.199921290079753in" Left="0.00003941853841146in" Top="0in" Value="ServiceCategory: {ServiceCategory}" Name="textBox16">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.ShowServiceCategory.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.199880917867025in" Name="groupFooterSection5">
          <Items>
            <TextBox Width="1.1in" Height="0.2in" Left="9.70063018798828in" Top="0in" Value="= Sum(Fields.TotalAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox61">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="0.7in" Height="0.2in" Left="2.40112463633219in" Top="0in" Value="= Sum(Fields.Quantity)" Format="{0:N0}" Anchoring="Right" Name="textBox38">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.1in" Height="0.2in" Left="3.10120360056559in" Top="0in" Value="= Sum(Fields.Charges)" Format="{0:N2}" Anchoring="Right" Name="textBox55">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.1in" Height="0.2in" Left="4.2012825012207in" Top="0in" Value="= Sum(Fields.PrimaryPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox56">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.09879709879557in" Height="0.2in" Left="5.30136140187581in" Top="0in" Value="= Sum(Fields.OtherPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox57">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.10000063578288in" Height="0.2in" Left="6.40023549397786in" Top="0in" Value="= Sum(Fields.TotalPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox58">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="1.1in" Height="0.2in" Left="8.60039393107096in" Top="0in" Value="= Sum(Fields.OtherAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox60">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
            <TextBox Width="2.4in" Height="0.2in" Left="0in" Top="0in" Value="Sum For {ServiceCategory}:" Name="textBox1">
              <Style VerticalAlign="Middle">
                <Font Size="10pt" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.1in" Height="0.2in" Left="7.50031534830729in" Top="0in" Value="= Sum(Fields.PrimaryAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox59">
              <Style TextAlign="Right" VerticalAlign="Middle" />
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.ShowServiceCategory.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="=Fields.ServiceCategory" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= Fields.Service" Direction="Asc" />
      </Sortings>
    </Group>
  </Groups>
  <ReportParameters>
    <ReportParameter Name="StartDate" Type="DateTime" Text="Start Date" Visible="True">
      <AvailableValues DisplayMember="= Parameters.StartDate.Value" ValueMember="= Parameters.StartDate.Value" />
    </ReportParameter>
    <ReportParameter Name="EndDate" Type="DateTime" Text="End Date" Visible="True">
      <AvailableValues DisplayMember="= Parameters.EndDate.Value" ValueMember="= Parameters.EndDate.Value" />
    </ReportParameter>
    <ReportParameter Name="BillingOrganization" Text="Billing Organization" Visible="True" MultiValue="True" AllowNull="True">
      <AvailableValues DataSourceName="BillingOrganizations" DisplayMember="= Fields.ShortName" ValueMember="= Fields.Id" />
    </ReportParameter>
    <ReportParameter Name="AttributeToServiceLocation" Text="Attributed Service Location" Visible="True" MultiValue="True" AllowNull="True">
      <AvailableValues DataSourceName="AttributeToServiceLocation" DisplayMember="= Fields.ShortName" ValueMember="= Fields.Id" />
    </ReportParameter>
    <ReportParameter Name="ServiceLocation" Text="Service Location" Visible="True" MultiValue="True" AllowNull="True">
      <AvailableValues DataSourceName="ServiceLocations" DisplayMember="= Fields.ShortName" ValueMember="= Fields.Id" />
    </ReportParameter>
    <ReportParameter Name="Provider" Text="Provider" Visible="True" MultiValue="True" AllowNull="True">
      <AvailableValues DataSourceName="Providers" DisplayMember="= Fields.UserName" ValueMember="= Fields.Id" />
    </ReportParameter>
    <ReportParameter Name="Insurer" Text="Insurer" Visible="True" MultiValue="True" AllowNull="True">
      <AvailableValues DataSourceName="Insurers" DisplayMember="=  Fields.Name" ValueMember="= Fields.Id" />
    </ReportParameter>
    <ReportParameter Name="ServiceCategory" Text="Service Category" Visible="True" MultiValue="True" AllowNull="True">
      <AvailableValues DataSourceName="ServiceCategory" DisplayMember="= Fields.Name" ValueMember="= Fields.Id" />
    </ReportParameter>
    <ReportParameter Name="ShowBillingOrganization" Type="Boolean" Text="Show Billing Organization Breakdown" Visible="True">
      <Value>
        <String>=False</String>
      </Value>
    </ReportParameter>
    <ReportParameter Name="ShowAttributeToServiceLocation" Type="Boolean" Text="Show Attributed Service Location Breakdown" Visible="True">
      <Value>
        <String>=False</String>
      </Value>
    </ReportParameter>
    <ReportParameter Name="ShowServiceLocation" Type="Boolean" Text="Show Service Location Breakdown" Visible="True">
      <Value>
        <String>=False</String>
      </Value>
    </ReportParameter>
    <ReportParameter Name="ShowDoctor" Type="Boolean" Text="Show Provider Breakdown" Visible="True">
      <Value>
        <String>=False</String>
      </Value>
    </ReportParameter>
    <ReportParameter Name="ShowInsurer" Type="Boolean" Text="Show Insurer Breakdown" Visible="True">
      <Value>
        <String>=False</String>
      </Value>
    </ReportParameter>
    <ReportParameter Name="ShowServiceCategory" Type="Boolean" Text="Show Service Category Breakdown" Visible="True">
      <Value>
        <String>=False</String>
      </Value>
    </ReportParameter>
  </ReportParameters>
</Report>'
WHERE Name = 'Payments By Primary Insurer'