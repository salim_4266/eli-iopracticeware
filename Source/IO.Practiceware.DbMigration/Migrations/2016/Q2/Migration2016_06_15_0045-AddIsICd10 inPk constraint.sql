-- 1.RUN THIS QUERY :
IF EXISTS (SELECT 1 FROM sys.Columns WHERE name = 'IsIcd10' AND Object_Name([object_Id]) = 'BillingServiceBillingDiagnosis')
 BEGIN 
  ALTER TABLE [model].[BillingServiceBillingDiagnosis] ALTER COLUMN IsIcd10 BIT NOT NULL
END
Go
--2. RIGHT CLICK ON KEYS > sCRIPT KEY AS > dROP & CREATE > nEW QUERY(IST OPTION) :
/****** Object:  Index [PK_BillingServiceBillingDiagnosis]    Script Date: 3/31/2016 1:33:03 AM ******/
IF EXISTS (SELECT 1 FROM sys.key_constraints  WHERE name = 'PK_BillingServiceBillingDiagnosis')
BEGIN

ALTER TABLE [model].[BillingServiceBillingDiagnosis] DROP CONSTRAINT [PK_BillingServiceBillingDiagnosis]
END

GO

--3. RUN THIS AT LAST
/****** Object:  Index [PK_BillingServiceBillingDiagnosis]    Script Date: 3/31/2016 1:33:03 AM ******/
ALTER TABLE [model].[BillingServiceBillingDiagnosis] ADD  CONSTRAINT [PK_BillingServiceBillingDiagnosis] PRIMARY KEY CLUSTERED 
(
	[BillingServiceId] ASC,
	[BillingDiagnosisId] ASC,
	[IsIcd10] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO


