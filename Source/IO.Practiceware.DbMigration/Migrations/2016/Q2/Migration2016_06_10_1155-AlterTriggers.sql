If EXists (Select 1 From SYS.triggers where name = 'AppointmentExternalSystemMessagesTrigger')
BEGIN 
	DROP Trigger [dbo].[AppointmentExternalSystemMessagesTrigger]
END

GO
CREATE TRIGGER [dbo].[AppointmentExternalSystemMessagesTrigger]
   ON  [dbo].[Appointments]
   FOR INSERT, UPDATE NOT FOR REPLICATION
AS 
BEGIN

SET NOCOUNT ON;

	DECLARE @now as DATETIME
	SET @now = GETUTCDATE()

	DECLARE @insertTable TABLE(Id UNIQUEIDENTIFIER, MessageType INT, AppointmentId INT, [Description] NVARCHAR(255))
	DECLARE @createdBy nvarchar(max)

	INSERT INTO @insertTable 
	SELECT NEWID(), esesmt.Id, i.AppointmentId, ESMT.Name
	FROM (SELECT [AppointmentId],[PatientId],[AppTypeId],[AppDate],[AppTime],[Duration],[ReferralId],[PreCertId],[TechApptTypeId],[ScheduleStatus],[ActivityStatus],[Comments],[ResourceId1],[ResourceId2],[ResourceId3],[ResourceId4],[ResourceId5],[ResourceId6],[ResourceId7],[ResourceId8],[ApptInsType],[ConfirmStatus],[ApptTypeCat],[SetDate],[VisitReason] FROM inserted EXCEPT SELECT [AppointmentId],[PatientId],[AppTypeId],[AppDate],[AppTime],[Duration],[ReferralId],[PreCertId],[TechApptTypeId],[ScheduleStatus],[ActivityStatus],[Comments],[ResourceId1],[ResourceId2],[ResourceId3],[ResourceId4],[ResourceId5],[ResourceId6],[ResourceId7],[ResourceId8],[ApptInsType],[ConfirmStatus],[ApptTypeCat],[SetDate],[VisitReason] FROM deleted) i
	CROSS JOIN model.ExternalSystemExternalSystemMessageTypes esesmt WITH(NOLOCK) 
	INNER JOIN model.ExternalSystemMessageTypes esmt WITH(NOLOCK) ON esesmt.ExternalSystemMessageTypeId = esmt.Id AND esmt.Name IN ('ADT_A28_V23') AND esmt.IsOutbound = 1
	WHERE esesmt.IsDisabled = 0

	-- Create rows for each ExternalSystemExternalSystemMessageType subscribed for ADT_A28_V23
	-- If deleted table contains rows, then this was an UPDATE
	IF EXISTS(SELECT * FROM deleted)
	BEGIN
		SET @createdBy = 'AppointmentUpdate'
	END
	ELSE
	BEGIN
		SET @createdBy = 'AppointmentInsert'
	END

	-- Create the messages
	INSERT INTO model.ExternalSystemMessages(Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId, ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId)
	-- Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId (1 = Unprocessed, 2 = Processing, 3 = Processed),
	-- ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId
	SELECT Id, [Description], '', MessageType, 1, 0, @createdBy, @now, @now, ''
	FROM @insertTable

	-- Add entity associated with each message for AppointmentId
	INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
	-- Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey
	SELECT NEWID(), it.Id, pre.Id, AppointmentId
	FROM
	@insertTable it INNER JOIN model.PracticeRepositoryEntities pre ON pre.Name = 'Appointment'
END

GO

If EXists (Select 1 From SYS.triggers where name = 'UpdateEncounterId')
BEGIN 
	DROP Trigger [dbo].[UpdateEncounterId] 
END

GO
CREATE TRIGGER [dbo].[UpdateEncounterId] ON [dbo].[Appointments]
AFTER INSERT
NOT FOR REPLICATION 
AS
SET NOCOUNT ON;
BEGIN

	IF EXISTS (SELECT * FROM inserted i WHERE i.Comments <> 'ASC CLAIM')
	BEGIN
		UPDATE ap
		SET ap.EncounterId = i.AppointmentId
		FROM Appointments ap WITH(NOLOCK)
		INNER JOIN inserted i  ON i.AppointmentId = ap.AppointmentId
	END

	IF EXISTS (SELECT * FROM inserted i WHERE i.Comments = 'ASC CLAIM')
	BEGIN
		;WITH CTE AS (
		SELECT ap.AppointmentId
		FROM Appointments apASC WITH(NOLOCK)
		INNER JOIN inserted i ON apASC.AppointmentId = i.AppointmentId
		INNER JOIN dbo.Appointments ap ON apASC.PatientId = ap.PatientId 
			AND apASC.AppTypeId = ap.AppTypeId 
			AND apASC.AppDate = ap.AppDate 
			AND apASC.AppTime = 0 
			AND ap.AppTime > 0 
			AND apASC.ScheduleStatus = ap.ScheduleStatus 
			AND ap.ScheduleStatus = 'D' 
			AND apASC.ResourceId1 = ap.ResourceId1 
			AND apASC.ResourceId2 = ap.ResourceId2 
			AND ap.Comments <> 'ASC CLAIM' 
			AND ap.AppointmentId = (
				SELECT TOP 1 pa.AppointmentId
				FROM dbo.PracticeActivity pa WITH(NOLOCK)
				INNER JOIN inserted i ON i.PatientId = pa.PatientId 
					AND i.AppDate = pa.ActivityDate 
					AND i.ResourceId2 = pa.LocationId
				WHERE pa.ActivityStatusTime <> '' 
					AND pa.ActivityStatusTime IS NOT NULL
				ORDER BY pa.ActivityStatusTime DESC
			)
		WHERE apASC.Comments = 'ASC CLAIM' 
			AND i.AppointmentId = apASC.AppointmentId
		)
		UPDATE apASC
		SET apASC.EncounterId = (SELECT AppointmentId FROM CTE)
		FROM Appointments apASC WITH(NOLOCK)
		INNER JOIN inserted i ON apASC.AppointmentId = i.AppointmentId
		WHERE apASC.Comments = 'ASC CLAIM' 
	END
END

GO

If EXists (Select 1 From SYS.triggers where name = 'UpdateDrugDosageFormIdTrigger')
BEGIN 
	DROP Trigger [dbo].[UpdateDrugDosageFormIdTrigger]
END
GO
CREATE TRIGGER [dbo].[UpdateDrugDosageFormIdTrigger]
ON [dbo].[Drug]
FOR INSERT, UPDATE
NOT FOR REPLICATION
--This trigger maps drug dosage forms
AS
BEGIN

SET NOCOUNT ON
 
 UPDATE dru
 SET DrugDosageFormId = ddf.Id
 FROM dbo.Drug dru
 INNER JOIN fdb.tblCompositeDrug tb WITH(NOLOCK) ON tb.MED_NAME = dru.DisplayName
 INNER JOIN model.DrugDosageForms ddf WITH(NOLOCK) ON ddf.Name = tb.MED_DOSAGE_FORM_DESC
 INNER JOIN inserted i ON i.DrugId = dru.DrugId

END

GO


If EXists (Select 1 From SYS.triggers where name = 'UpdatePatientNotesWithClinicalId')
BEGIN 
	DROP Trigger [dbo].[UpdatePatientNotesWithClinicalId]
END
GO
CREATE TRIGGER [dbo].[UpdatePatientNotesWithClinicalId]
ON [dbo].[PatientClinical]
FOR INSERT, UPDATE
NOT FOR REPLICATION
--This trigger helps with the join from PatientClincial to PatientNotes.
AS
BEGIN

SET NOCOUNT ON
--Gets clinical Id for RX notes.
	IF EXISTS (SELECT i.ClinicalId 
				FROM inserted i 
				WHERE i.ClinicalType = 'A'
				AND LEN(i.FindingDetail) > 6
				AND SUBSTRING(i.FindingDetail, 1, 3) = 'RX-'
				)
	BEGIN 
		UPDATE pn
		SET pn.ClinicalId = i.ClinicalId
		FROM dbo.PatientNotes pn WITH(NOLOCK)
		INNER JOIN inserted i ON i.AppointmentId = pn.AppointmentId AND pn.NoteSystem = SUBSTRING(i.FindingDetail, 6, CHARINDEX('-2', i.FindingDetail) - 6) -- 6 is hardcoded because it will always be 'RX-8/'
		WHERE pn.NoteType = 'R'
			AND i.ClinicalType = 'A'
			AND LEN(i.FindingDetail) > 6
			AND SUBSTRING(i.FindingDetail, 1, 3) = 'RX-'
	END

END

GO

If EXists (Select 1 From SYS.triggers where name = 'PracticeActivityCheckoutExternalSystemMessagesTrigger')
BEGIN 
	DROP Trigger [dbo].[PracticeActivityCheckoutExternalSystemMessagesTrigger] 
END
GO
CREATE TRIGGER [dbo].[PracticeActivityCheckoutExternalSystemMessagesTrigger] 
   ON  [dbo].[PracticeActivity]
   FOR UPDATE NOT FOR REPLICATION
AS 
BEGIN

SET NOCOUNT ON;

IF UPDATE(Status)
	BEGIN
		DECLARE @now as DATETIME
		SET @now = GETUTCDATE()

		DECLARE @insertTable TABLE(Id UNIQUEIDENTIFIER, MessageType INT, AppointmentId INT, Description NVARCHAR(255))

		-- Create rows for each ExternalSystemExternalSystemMessageType subscribed for DFT_P03_V231s, ORM_O01_V231
		INSERT INTO @insertTable 
		SELECT NEWID(), esesmt.Id, i.AppointmentId, ESMT.Name
		FROM 
		DELETED d INNER JOIN inserted i on d.ActivityId = i.ActivityId
		CROSS JOIN model.ExternalSystemExternalSystemMessageTypes esesmt WITH(NOLOCK) 
		INNER JOIN model.ExternalSystemMessageTypes esmt WITH(NOLOCK) ON esesmt.ExternalSystemMessageTypeId = esmt.Id AND esmt.Name IN ('DFT_P03_V231', 'ORM_O01_V231') AND esmt.IsOutbound = 1
		WHERE 
		i.Status = 'D' AND (d.Status = 'G' OR d.Status = 'H' OR d.Status = 'U') AND esesmt.IsDisabled = 0


		-- Create the messags
		INSERT INTO model.ExternalSystemMessages(Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId, ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId)
		SELECT Id, Description, '', MessageType, 1, 0, 'Checkout', @now, @now, ''
		FROM @insertTable

		-- Add entity associated with each message for AppointmentId
		INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
		SELECT NEWID(), it.Id, pre.Id, AppointmentId
		FROM
		@insertTable it INNER JOIN model.PracticeRepositoryEntities pre ON pre.Name = 'Appointment'
	END
END

GO

If EXists (Select 1 From SYS.triggers where name = 'PracticeActivityMultipleActivityRecordsTrigger')
BEGIN 
	DROP Trigger [dbo].[PracticeActivityMultipleActivityRecordsTrigger]
END
GO
CREATE TRIGGER [dbo].[PracticeActivityMultipleActivityRecordsTrigger]
	ON [dbo].[PracticeActivity]
	FOR INSERT
	AS
	BEGIN

		SET NOCOUNT ON
		IF EXISTS(
				SELECT * 
				FROM PracticeActivity pa WITH(NOLOCK) 
				INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON i.AppointmentId = pa.AppointmentId
				GROUP BY pa.AppointmentId
				HAVING COUNT(*) > 1)
			AND (SELECT COUNT(*) FROM inserted) = 1
			AND dbo.GetNoCheck() = 0
	
		BEGIN
			WITH EncounterStatuses AS (
				SELECT pa.ActivityId, 
					CASE pa.[Status]
							WHEN 'W'
								THEN 1
							WHEN 'M'
								THEN 2
							WHEN 'E'
								THEN 3
							WHEN 'G'
								THEN 4
							WHEN 'H'
								THEN 5
							WHEN 'U'
								THEN 6
							WHEN 'D'
								THEN 7
							ELSE 0
						END
							AS StatusId
				FROM PracticeActivity pa WITH(NOLOCK) 
				JOIN inserted i ON i.AppointmentId = pa.AppointmentId AND pa.ActivityId <> i.ActivityId
			)

			DELETE FROM PracticeActivity WHERE ActivityId = (SELECT TOP 1 es2.ActivityId FROM EncounterStatuses es2 ORDER BY es2.StatusId)
		END
	END

GO

If EXists (Select 1 From SYS.triggers where name = 'PracticeActivityStatusTrigger')
BEGIN 
	DROP Trigger [dbo].[PracticeActivityStatusTrigger]
END

GO
CREATE TRIGGER [dbo].[PracticeActivityStatusTrigger]
ON [dbo].[PracticeActivity]
FOR UPDATE, INSERT
NOT FOR REPLICATION 
AS
SET NOCOUNT ON
IF EXISTS (
	SELECT i.ActivityId
	FROM PracticeActivity pa WITH(NOLOCK) 
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON i.ActivityId = pa.ActivityId
	INNER JOIN Appointments ap WITH(NOLOCK) ON pa.AppointmentId = ap.AppointmentId 
	WHERE i.[Status] IN (
			'E'
			,'G'
			,'W'
			,'M'
			)
	AND ap.ScheduleStatus NOT IN ('A', 'P', 'R')

) AND dbo.GetNoCheck() = 0

BEGIN

	RAISERROR ('Invalid practice activity status. Check the Appointment ScheduleStatus and compare to the practice activity status.',16,1)
	ROLLBACK TRANSACTION
END

GO

If EXists (Select 1 From SYS.triggers where name = 'ProvidersDeletePracticeAffiliations')
BEGIN 
	DROP Trigger [dbo].[ProvidersDeletePracticeAffiliations]
END
GO
CREATE TRIGGER [dbo].[ProvidersDeletePracticeAffiliations] ON [dbo].[PracticeAffiliations]
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	DELETE 
	FROM model.ProviderInsurerClaimFileOverride
	WHERE (ClaimFileOverrideProviders_Id IN (
		SELECT p.Id 
		FROM model.Providers p WITH(NOLOCK)
		WHERE p.UserId IN (
		SELECT DISTINCT ResourceId FROM deleted
		)
	) 
		AND ClaimFileOverrideInsurers_Id IN (
		SELECT PlanId FROM deleted
		)
	)

END

GO

If EXists (Select 1 From SYS.triggers where name = 'ProvidersDeletePracticeAffiliations')
BEGIN 
	DROP Trigger [dbo].[ProvidersDeletePracticeAffiliations]
END

GO
CREATE TRIGGER [dbo].[ProvidersDeletePracticeAffiliations] ON [dbo].[PracticeAffiliations]
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	DELETE 
	FROM model.ProviderInsurerClaimFileOverride
	WHERE (ClaimFileOverrideProviders_Id IN (
		SELECT p.Id 
		FROM model.Providers p WITH(NOLOCK)
		WHERE p.UserId IN (
		SELECT DISTINCT ResourceId FROM deleted
		)
	) 
		AND ClaimFileOverrideInsurers_Id IN (
		SELECT PlanId FROM deleted
		)
	)

END

GO

If EXists (Select 1 From SYS.triggers where name = 'ProvidersInsertPracticeAffiliations')
BEGIN 
	DROP Trigger [dbo].[ProvidersInsertPracticeAffiliations]
END

GO
CREATE TRIGGER [dbo].[ProvidersInsertPracticeAffiliations] ON [dbo].[PracticeAffiliations]
AFTER INSERT
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Override nvarchar(1)
	DECLARE @OrgOverride nvarchar(1)
	DECLARE @ResourceId int
	DECLARE @ResourceType nvarchar(1)
	DECLARE @PracticeId int

	DECLARE ProvidersInsertPracticeAffiliationsCursor CURSOR FOR
	SELECT i.[Override], i.OrgOverride, i.ResourceId, r.ResourceType, r.PracticeId
	FROM inserted i
	JOIN dbo.Resources r ON r.ResourceId = i.ResourceId 
		AND r.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND i.ResourceType = 'R'

	OPEN ProvidersInsertPracticeAffiliationsCursor FETCH NEXT FROM ProvidersInsertPracticeAffiliationsCursor INTO
	@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		IF (@ResourceId IS NOT NULL) AND 
			((SELECT TOP 1 Id FROM model.Providers WHERE UserId = @ResourceId) IS NOT NULL)
			AND NOT EXISTS(SELECT TOP 1 * FROM model.Providers 
				WHERE UserId = @ResourceId 
					AND BillingOrganizationId = (110000000 * 1 + @ResourceId)) 
			AND (@Override = 'Y' OR @OrgOverride  = 'Y')
		BEGIN
			INSERT INTO model.Providers (UserId, BillingOrganizationId, IsBillable, IsEmr)
				SELECT @ResourceId AS UserId
				,(110000000 * 1 + @ResourceId) AS BillingOrganizationId
				,CONVERT(bit, 1) AS IsBillable
				,CONVERT(bit, 0) AS IsEmr
		END

		INSERT INTO model.ProviderInsurerClaimFileOverride (ClaimFileOverrideProviders_Id, ClaimFileOverrideInsurers_Id)
		SELECT 
		p.Id AS ClaimFileOverrideProviders_Id
		,ins.Id AS ClaimFileOverrideInsurers_Id
		FROM dbo.Resources re WITH(NOLOCK) 
		JOIN dbo.PracticeAffiliations pa WITH(NOLOCK) ON pa.ResourceId = re.ResourceId	
			AND pa.ResourceType = 'R'
			AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
		JOIN model.Insurers ins WITH(NOLOCK) ON ins.Id = pa.PlanId
		JOIN model.Providers p WITH(NOLOCK) ON p.UserId = re.ResourceId
			AND p.IsBillable = CASE WHEN re.Billable = 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END 
			AND p.IsEMR = CASE WHEN re.GoDirect = 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END
			AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'PersonBillingOrganization')
		WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y') 
			AND re.ResourceId = @ResourceId
		GROUP BY p.Id
			,ins.Id

		EXCEPT 
		
		SELECT ClaimFileOverrideProviders_Id, ClaimFileOverrideInsurers_Id FROM model.ProviderInsurerClaimFileOverride

		FETCH NEXT FROM ProvidersInsertPracticeAffiliationsCursor INTO 
		@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId
	END
	CLOSE ProvidersInsertPracticeAffiliationsCursor
	DEALLOCATE ProvidersInsertPracticeAffiliationsCursor
END

GO

If EXists (Select 1 From SYS.triggers where name = 'ProvidersUpdatePracticeAffiliations')
BEGIN 
	DROP Trigger [dbo].[ProvidersUpdatePracticeAffiliations]
END

GO
CREATE TRIGGER [dbo].[ProvidersUpdatePracticeAffiliations] ON [dbo].[PracticeAffiliations]
AFTER UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON;

	DELETE 
	FROM model.ProviderInsurerClaimFileOverride 
	WHERE ClaimFileOverrideProviders_Id IN (
		SELECT p.Id 
		FROM model.Providers p WITH(NOLOCK) 
		WHERE p.UserId IN (SELECT DISTINCT ResourceId FROM deleted)
	)
		AND ClaimFileOverrideInsurers_Id IN (
		SELECT PlanId FROM deleted
	)

	DECLARE @Override nvarchar(1)
	DECLARE @OrgOverride nvarchar(1)
	DECLARE @ResourceId int
	DECLARE @ResourceType nvarchar(1)
	DECLARE @PracticeId int
	DECLARE @PlanId int

	DECLARE ProvidersUpdatePracticeAffiliationsCursor CURSOR FOR
	SELECT i.[Override], i.OrgOverride, i.ResourceId, r.ResourceType, r.PracticeId, i.PlanId
	FROM inserted i
	JOIN dbo.Resources r ON r.ResourceId = i.ResourceId 
		AND r.ResourceType IN ('D', 'Q', 'Z','Y')
		AND i.ResourceType = 'R'

	OPEN ProvidersUpdatePracticeAffiliationsCursor FETCH NEXT FROM ProvidersUpdatePracticeAffiliationsCursor INTO
	@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId, @PlanId
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		DECLARE @DeletedPracticeId INT
		DECLARE @DeletedResourceId INT
		SET @DeletedPracticeId = (SELECT TOP 1 r.PracticeId
		FROM deleted d
		JOIN dbo.Resources r ON r.ResourceId = d.ResourceId 
			AND r.ResourceType IN ('D', 'Q', 'Z','Y')
			AND d.ResourceType = 'R')
		
		IF (@ResourceId IS NOT NULL) AND 
			((SELECT TOP 1 Id FROM model.Providers WHERE UserId = @ResourceId) IS NOT NULL)
			AND NOT EXISTS(SELECT TOP 1 * FROM model.Providers 
			WHERE UserId = @ResourceId 
					AND BillingOrganizationId = (110000000 * 1 + @ResourceId)) 
			AND (@Override = 'Y' OR @OrgOverride  = 'Y')
		BEGIN
			INSERT INTO model.Providers (UserId, BillingOrganizationId, IsBillable, IsEmr)
				SELECT @ResourceId AS UserId
				,(110000000 * 1 + @ResourceId) AS BillingOrganizationId
				,CONVERT(bit, 1) AS IsBillable
				,CONVERT(bit, 0) AS IsEmr
		END

		INSERT INTO model.ProviderInsurerClaimFileOverride (ClaimFileOverrideProviders_Id, ClaimFileOverrideInsurers_Id)
		SELECT 
		p.Id AS ClaimFileOverrideProviders_Id
		,ins.Id AS ClaimFileOverrideInsurers_Id
		FROM dbo.Resources re WITH(NOLOCK) 
		JOIN dbo.PracticeAffiliations pa WITH(NOLOCK) ON pa.ResourceId = re.ResourceId	
			AND pa.ResourceType = 'R'
			AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
		JOIN model.Insurers ins WITH(NOLOCK) ON ins.Id = pa.PlanId
		JOIN model.Providers p WITH(NOLOCK) ON p.UserId = re.ResourceId
			AND p.IsBillable = CASE WHEN re.Billable = 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END 
			AND p.IsEMR = CASE WHEN re.GoDirect = 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END
			AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'PersonBillingOrganization')
		WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y') 
			AND re.ResourceId = @ResourceId
		GROUP BY p.Id
			,ins.Id
			
		EXCEPT 
		
		SELECT ClaimFileOverrideProviders_Id, ClaimFileOverrideInsurers_Id FROM model.ProviderInsurerClaimFileOverride

		FETCH NEXT FROM ProvidersUpdatePracticeAffiliationsCursor INTO 
		@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId, @PlanId
	END
	CLOSE ProvidersUpdatePracticeAffiliationsCursor
	DEALLOCATE ProvidersUpdatePracticeAffiliationsCursor
END


GO

If EXists (Select 1 From SYS.triggers where name = 'InsurerPlanTypesInsert')
BEGIN 
	DROP Trigger [dbo].[InsurerPlanTypesInsert]
END

GO
CREATE TRIGGER [dbo].[InsurerPlanTypesInsert] ON [dbo].[PracticeCodeTable]
AFTER INSERT
NOT FOR REPLICATION 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'BUSINESSCLASS'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
	INSERT INTO model.InsurerPlanTypes (
		[Name],
		[LegacyPracticeCodeTableId]
	)
	SELECT 
	Code AS [Name]
	,Id AS LegacyPracticeCodeTableId
	FROM dbo.PracticeCodeTable p WITH(NOLOCK) 
	WHERE p.Id IN (SELECT Id FROM #inserted)
	END
END

GO

If EXists (Select 1 From SYS.triggers where name = 'InsurerBusinessClassesInsert')
BEGIN 
	DROP Trigger [dbo].[InsurerBusinessClassesInsert] 
END

GO
CREATE TRIGGER [dbo].[InsurerBusinessClassesInsert] ON [dbo].[PracticeCodeTable]
AFTER INSERT
NOT FOR REPLICATION 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'BUSINESSCLASS'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
	INSERT INTO model.InsurerBusinessClasses (
		[Name],
		[IsArchived],
		LegacyPracticeCodeTableId
	)
	SELECT 
	CASE Code
		WHEN 'AMERIH'
			THEN 'Amerihealth'
		WHEN 'BLUES'
			THEN 'BlueCrossBlueShield'
		WHEN 'COMM'
			THEN 'Commercial'
		WHEN 'MCAIDFL'
			THEN 'MedicaidFL'
		WHEN 'MCAIDNC'
			THEN 'MedicaidNC'
		WHEN 'MCAIDNJ'
			THEN 'MedicaidNJ'
		WHEN 'MCAIDNV'
			THEN 'MedicaidNV'
		WHEN 'MCAIDNY'
			THEN 'MedicaidNY'
		WHEN 'MCARENJ'
			THEN 'MedicareNJ'
		WHEN 'MCARENV'
			THEN 'MedicareNV'
		WHEN 'MCARENY'
			THEN 'MedicareNY'
		WHEN 'TEMPLATE'
			THEN 'Template'
		ELSE Code
	END AS [Name]
	,CONVERT(BIT, 0) AS IsArchived
	,CASE Code
		WHEN 'AMERIH'
			THEN 1
		WHEN 'BLUES'
			THEN 2
		WHEN 'COMM'
			THEN 3
		WHEN 'MCAIDFL'
			THEN 4
		WHEN 'MCAIDNC'
			THEN 5
		WHEN 'MCAIDNJ'
			THEN 6
		WHEN 'MCAIDNV'
			THEN 7
		WHEN 'MCAIDNY'
			THEN 8
		WHEN 'MCARENJ'
			THEN 9
		WHEN 'MCARENV'
			THEN 10
		WHEN 'MCARENY'
			THEN 11
		WHEN 'TEMPLATE'
			THEN 12
		ELSE Id + 1000
	END AS LegacyPracticeCodeTableId
	FROM dbo.PracticeCodeTable p WITH(NOLOCK) 
	WHERE p.Id IN (SELECT Id FROM #inserted)
	ORDER BY CASE p.Code
		WHEN 'AMERIH'
			THEN 1
		WHEN 'BLUES'
			THEN 2
		WHEN 'COMM'
			THEN 3
		WHEN 'MCAIDFL'
			THEN 4
		WHEN 'MCAIDNC'
			THEN 5
		WHEN 'MCAIDNJ'
			THEN 6
		WHEN 'MCAIDNV'
			THEN 7
		WHEN 'MCAIDNY'
			THEN 8
		WHEN 'MCARENJ'
			THEN 9
		WHEN 'MCARENV'
			THEN 10
		WHEN 'MCARENY'
			THEN 11
		WHEN 'TEMPLATE'
			THEN 12
		ELSE Id + 1000
	END
	END
END

GO

If EXists (Select 1 From SYS.triggers where name = 'EncounterServiceTypesUpdate')
BEGIN 
	DROP Trigger [dbo].[EncounterServiceTypesUpdate]
END

GO
CREATE TRIGGER [dbo].[EncounterServiceTypesUpdate] ON [dbo].[PracticeCodeTable]
AFTER UPDATE
NOT FOR REPLICATION 
AS
BEGIN

	SET NOCOUNT ON;
	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'SERVICECATEGORY'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		UPDATE est
		SET est.Name = i.Code
		FROM model.EncounterServiceTypes est WITH(NOLOCK) 
		JOIN deleted d ON d.Code = est.Name
		JOIN #inserted i ON i.Id = d.Id
	END
END

GO

If EXists (Select 1 From SYS.triggers where name = 'InsurerPlanTypesInsert')
BEGIN 
	DROP Trigger [dbo].[InsurerPlanTypesInsert]
END

GO
CREATE TRIGGER [dbo].[InsurerPlanTypesInsert] ON [dbo].[PracticeCodeTable]
AFTER INSERT
NOT FOR REPLICATION 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'BUSINESSCLASS'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
	INSERT INTO model.InsurerPlanTypes (
		[Name],
		[LegacyPracticeCodeTableId]
	)
	SELECT 
	Code AS [Name]
	,Id AS LegacyPracticeCodeTableId
	FROM dbo.PracticeCodeTable p WITH(NOLOCK) 
	WHERE p.Id IN (SELECT Id FROM #inserted)
	END
END

GO

If EXists (Select 1 From SYS.triggers where name = 'PaymentMethodsUpdate')
BEGIN 
	DROP Trigger [dbo].[PaymentMethodsUpdate]
END

GO
CREATE TRIGGER [dbo].[PaymentMethodsUpdate] ON [dbo].[PracticeCodeTable]
AFTER UPDATE
NOT FOR REPLICATION 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'PAYABLETYPE'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		UPDATE p
		SET p.Name = SUBSTRING(i.Code, 1, CHARINDEX(' - ', i.Code))
			,p.LegacyDisplayEPaymentTypes = CASE WHEN i.AlternateCode = 'IncludeOnStatement' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
			,p.IsReferenceRequired = CASE WHEN SUBSTRING(i.Code, 1, CHARINDEX(' - ', i.Code)) = 'Cash' THEN CONVERT(BIT,0)  ELSE CONVERT(BIT,1) END 
		FROM model.PaymentMethods p WITH(NOLOCK)
		JOIN deleted d ON SUBSTRING(d.Code, 1, CHARINDEX(' - ', d.Code)) = p.Name
		JOIN #inserted i ON i.Id = d.Id
	END
END

GO

If EXists (Select 1 From SYS.triggers where name = 'PracticeCodeTableConfirmationStatusCodeTrigger')
BEGIN 
	DROP Trigger [dbo].[PracticeCodeTableConfirmationStatusCodeTrigger]
END

GO
CREATE TRIGGER [dbo].[PracticeCodeTableConfirmationStatusCodeTrigger]
ON [dbo].[PracticeCodeTable]
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
BEGIN

SET NOCOUNT ON
IF (
		(
			(
				SELECT COUNT(*) 
				FROM PracticeCodeTable pc WITH(NOLOCK) 
				INNER JOIN (
								SELECT * 
								FROM inserted 
								
								EXCEPT 
								
								SELECT * FROM deleted
								
				) i  ON SUBSTRING(i.Code, 1, 1) = SUBSTRING(pc.Code, 1, 1) 
								AND pc.ReferenceType = 'CONFIRMSTATUS' 
								WHERE i.ReferenceType = 'CONFIRMSTATUS' 
								GROUP BY i.Code 
								HAVING COUNT(*) > 1
			) > 1
		) 
	AND dbo.GetNoCheck() = 0
)
	
BEGIN
	RAISERROR ('Cannot insert duplicate payment types.',16,1)
	ROLLBACK TRANSACTION
END

END

GO

If EXists (Select 1 From SYS.triggers where name = 'PracticeCodeTableDocumentsTrigger')
BEGIN 
	DROP Trigger [dbo].[PracticeCodeTableDocumentsTrigger]
END

GO
CREATE TRIGGER [dbo].[PracticeCodeTableDocumentsTrigger] ON [dbo].[PracticeCodeTable]
FOR UPDATE
	,INSERT
	,DELETE NOT
FOR REPLICATION AS

SET NOCOUNT ON

DECLARE @DocCatgId INT;
DECLARE @DocName NVARCHAR(MAX);

SELECT @DocName = pc.Code
	,@DocCatgId = CASE 
		WHEN pc.ReferenceType = 'MISCELLANEOUSLETTERS'
			THEN 1
		WHEN pc.ReferenceType = 'CONSULTATIONLETTERS'
			THEN 2
		WHEN pc.ReferenceType = 'REFERRALLETTERS'
			THEN 3
		WHEN pc.ReferenceType = 'COLLECTIONLETTERS'
			THEN 4
		WHEN pc.ReferenceType = 'FACILITYADMISSION'
			THEN 7
		ELSE 0
		END
FROM PracticeCodeTable pc WITH(NOLOCK)
INNER JOIN Inserted i ON i.Id = pc.Id
WHERE i.ReferenceType LIKE '%Letters'

IF EXISTS (
		SELECT *
		FROM inserted
		)
BEGIN

	IF EXISTS (
			SELECT *
			FROM deleted
			)
	BEGIN
		IF @DocCatgId <> 0
		BEGIN
			UPDATE model.TemplateDocuments
			SET TemplateDocumentCategoryId = @DocCatgId
				,NAME = @DocName + '.doc'
				,DisplayNAME = @DocName
			WHERE Id IN (
					SELECT TOP 1 Id
					FROM model.TemplateDocuments
					WHERE DisplayName IN (
							SELECT Code
							FROM Deleted
							)
					)
		END
	END
	ELSE
	BEGIN
		IF @DocCatgId <> 0
		BEGIN
			INSERT INTO model.TemplateDocuments (
				TemplateDocumentCategoryId
				,NAME
				,DisplayName 
				)
			VALUES (
				@DocCatgId
				,@DocName + '.doc'
				,@DocName
				)
		END
	END
END
ELSE
BEGIN
	DELETE model.TemplateDocuments
	WHERE Id IN (
			SELECT TOP 1 Id
			FROM model.TemplateDocuments WITH(NOLOCK)
			WHERE DisplayName IN (
					SELECT Code
					FROM Deleted
					WHERE ReferenceType LIKE '%Letters'
					)
			)
END

GO

If EXists (Select 1 From SYS.triggers where name = 'PracticeCodeTablePaymentTypeTrigger')
BEGIN 
	DROP Trigger [dbo].[PracticeCodeTablePaymentTypeTrigger]
END

GO
CREATE TRIGGER [dbo].[PracticeCodeTablePaymentTypeTrigger]
ON [dbo].[PracticeCodeTable]
FOR UPDATE, INSERT
NOT FOR REPLICATION 
AS
SET NOCOUNT ON
IF (((SELECT COUNT(*) 
	FROM PracticeCodeTable pc WITH(NOLOCK) 
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i  ON SUBSTRING(i.Code, LEN(i.Code), 1) = SUBSTRING(pc.Code, LEN(pc.Code), 1) AND pc.ReferenceType = 'PAYMENTTYPE' 
	WHERE i.ReferenceType = 'PAYMENTTYPE' 
	GROUP BY i.Code HAVING COUNT(*) > 1) > 1) 
	AND dbo.GetNoCheck() = 0)
	
BEGIN

	RAISERROR ('Cannot insert duplicate payment types.',16,1)
	ROLLBACK TRANSACTION
END


GO

If EXists (Select 1 From SYS.triggers where name = 'PracticeCodeTableTransmissionTypeTrigger')
BEGIN 
	DROP Trigger [dbo].[PracticeCodeTableTransmissionTypeTrigger]
END

GO
CREATE TRIGGER [dbo].[PracticeCodeTableTransmissionTypeTrigger]
ON [dbo].[PracticeCodeTable]
FOR UPDATE, INSERT
NOT FOR REPLICATION 
AS
	SET NOCOUNT ON
	IF (((SELECT COUNT(*) 
		FROM PracticeCodeTable pc WITH(NOLOCK) 
		INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i  ON SUBSTRING(i.Code, LEN(i.Code), 1) = SUBSTRING(pc.Code, LEN(pc.Code), 1) AND pc.ReferenceType = 'PAYMENTTYPE' 
		WHERE i.ReferenceType = 'TRANSMITTYPE' ) > 1) 
		AND dbo.GetNoCheck() = 0)
	
	BEGIN

		RAISERROR ('Cannot insert duplicate transmission types.',16,1)
		ROLLBACK TRANSACTION
	END

GO

If EXists (Select 1 From SYS.triggers where name = 'PreventPaymentTypeDeletion')
BEGIN 
	DROP Trigger [dbo].[PreventPaymentTypeDeletion]
END

GO
CREATE TRIGGER [dbo].[PreventPaymentTypeDeletion]
ON [dbo].[PracticeCodeTable]
INSTEAD OF DELETE
NOT FOR REPLICATION
AS 
	SET NOCOUNT ON;
	IF EXISTS (
		SELECT d.Id
		FROM deleted d  
		INNER JOIN PatientReceivablePayments prp WITH(NOLOCK) 
			ON SUBSTRING(Code, LEN(Code), 1) = Prp.PaymentType
		WHERE ReferenceType ='PaymentType'
			)	
		BEGIN

			RAISERROR ('Cannot delete payment types that are in use.',16,1)
			ROLLBACK TRANSACTION
		END
	ELSE
		BEGIN
			DELETE dbo.PracticeCodeTable WHERE Id IN(SELECT Id FROM deleted)
		END

GO

If EXists (Select 1 From SYS.triggers where name = 'PreventTransmissionTypeDeletion')
BEGIN 
	DROP Trigger [dbo].[PreventTransmissionTypeDeletion]
END

GO
CREATE TRIGGER [dbo].[PreventTransmissionTypeDeletion]
ON [dbo].[PracticeCodeTable]
FOR DELETE
NOT FOR REPLICATION
AS 
	SET NOCOUNT ON;
	IF EXISTS (
		SELECT Id
		FROM deleted d
		INNER JOIN PracticeTransactionJournal ptj WITH(NOLOCK) ON SUBSTRING(ptj.TransactionBatch, 25, 1) = SUBSTRING(d.Code, 1, 1)
		WHERE ptj.TransactionBatch LIKE '_:\Pinpoint\Submissions\%' 
			AND ptj.TransactionBatch NOT LIKE '_:\Pinpoint\Submissions\Hcfa-00000-00000.txt'
			AND d.ReferenceType = 'TRANSMITTYPE'
			)
			
	BEGIN

	RAISERROR('Cannot delete Transmit Type, it has an associated submission.', 16, 1)
	ROLLBACK
	END
	
GO

If EXists (Select 1 From SYS.triggers where name = 'ResourceTypeDeletion')
BEGIN 
	DROP Trigger [dbo].[ResourceTypeDeletion]
END

GO
CREATE TRIGGER [dbo].[ResourceTypeDeletion]
ON [dbo].[PracticeCodeTable]
FOR DELETE
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (SELECT COUNT(*)
	FROM deleted d
	INNER JOIN dbo.Resources re WITH(NOLOCK) ON re.ResourceType = SUBSTRING(d.Code,1,1)
	WHERE d.Referencetype = 'ResourceType' AND SUBSTRING(d.Code,1,1) IN ('D', 'Q', 'U', 'R', 'E'))
	<> 0
	
BEGIN

	RAISERROR ('Cannot delete ResourceTypes that are in use.', 16,1)
	ROLLBACK TRANSACTION
END

GO

If EXists (Select 1 From SYS.triggers where name = 'ServiceLocationCodesUpdate')
BEGIN 
	DROP Trigger [dbo].[ServiceLocationCodesUpdate]
END

GO
CREATE TRIGGER [dbo].[ServiceLocationCodesUpdate] ON [dbo].[PracticeCodeTable]
AFTER UPDATE
NOT FOR REPLICATION 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'SERVICECATEGORY'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		UPDATE slc
		SET slc.PlaceOfServiceCode = SUBSTRING(i.Code,1,2)
			,slc.Name = SUBSTRING(i.Code,CHARINDEX('-',i.Code),LEN(i.Code))
			,slc.LegacyPracticeCodeTableId = i.Id
		FROM model.ServiceLocationCodes slc WITH(NOLOCK) 
		JOIN deleted d ON d.Id = slc.LegacyPracticeCodeTableId
		JOIN #inserted i ON i.Id = d.Id
	END
END

GO

If EXists (Select 1 From SYS.triggers where name = 'ServiceModifiersInsert')
BEGIN 
	DROP Trigger [dbo].[ServiceModifiersInsert]
END

GO
CREATE TRIGGER [dbo].[ServiceModifiersInsert] ON [dbo].[PracticeCodeTable]
AFTER INSERT
NOT FOR REPLICATION 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'SERVICEMODS'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		INSERT INTO model.ServiceModifiers (
			Code
			,[Description]
			,ClinicalOrdinalId
			,BillingOrdinalId
			,IsArchived
		)
		SELECT
		SUBSTRING(Code,1,2) AS Code
		,SUBSTRING(Code,CHARINDEX('-',Code) + 1,LEN(Code)) AS [Description]
		,CASE WHEN ISNUMERIC(AlternateCode) = 1 THEN AlternateCode ELSE 99 END AS ClinicalOrdinalId
		,[Rank] AS BillingOrdinalId
		,CONVERT(BIT,0) AS IsArchived
		FROM #inserted
	END
END

GO

If EXists (Select 1 From SYS.triggers where name = 'ServiceModifiersUpdate')
BEGIN 
	DROP Trigger [dbo].[ServiceModifiersUpdate]
END

GO
CREATE TRIGGER [dbo].[ServiceModifiersUpdate] ON [dbo].[PracticeCodeTable]
AFTER UPDATE
NOT FOR REPLICATION 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * 
	INTO #inserted 
	FROM inserted 
	WHERE ReferenceType = 'SERVICEMODS'

	IF EXISTS (SELECT * FROM #inserted)
	BEGIN
		UPDATE sm
		SET sm.Code = SUBSTRING(i.Code,1,2)
			,sm.[Description] = SUBSTRING(i.Code,CHARINDEX('-',i.Code) + 1,LEN(i.Code)) 
			,sm.ClinicalOrdinalId = CASE WHEN ISNUMERIC(i.AlternateCode) = 1 THEN i.AlternateCode ELSE 99 END
			,sm.[BillingOrdinalId] = i.[Rank]
		FROM model.ServiceModifiers sm WITH(NOLOCK) 
		JOIN deleted d ON SUBSTRING(d.Code,1,2) = sm.Code
			AND SUBSTRING(d.Code,CHARINDEX('-',d.Code) + 1,LEN(d.Code)) = sm.[Description]
		JOIN #inserted i ON i.Id = d.Id
	END
END

GO

If EXists (Select 1 From SYS.triggers where name = 'PracticeNameBlankLocationReferenceTrigger')
BEGIN 
	DROP Trigger [dbo].[PracticeNameBlankLocationReferenceTrigger]
END

GO
CREATE TRIGGER [dbo].[PracticeNameBlankLocationReferenceTrigger]
ON [dbo].[PracticeName]
FOR UPDATE, INSERT
NOT FOR REPLICATION 
AS
SET NOCOUNT ON
IF EXISTS (
		SELECT i.PracticeId
		FROM (SELECT * FROM inserted  EXCEPT SELECT * FROM deleted) i
		INNER JOIN Appointments ap WITH(NOLOCK) ON ap.ResourceId2 = 0
		INNER JOIN PracticeName pn WITH(NOLOCK) ON i.PracticeId = pn.PracticeId
		WHERE pn.LocationReference = ''
			AND i.LocationReference <> ''
		)
	AND dbo.GetNoCheck() = 0
	   
BEGIN

	RAISERROR ('Cannot add a location reference to the main office if appointments have been scheduled to the main office. (ap.ResourceId2 = 0)',16,1)
	ROLLBACK TRANSACTION
END

GO

If EXists (Select 1 From SYS.triggers where name = 'PracticeNameLocationReferenceTrigger')
BEGIN 
	DROP Trigger [dbo].[PracticeNameLocationReferenceTrigger]
END

GO
CREATE TRIGGER [dbo].[PracticeNameLocationReferenceTrigger]
ON [dbo].[PracticeName]
FOR UPDATE, INSERT
NOT FOR REPLICATION 
AS
SET NOCOUNT ON
IF (SELECT COUNT(*) 
	FROM PracticeName pn WITH(NOLOCK) 
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON pn.LocationReference = i.LocationReference
	WHERE i.PracticeType = 'P' AND pn.PracticeType = 'P'
	GROUP BY pn.LocationReference 
	HAVING COUNT(*) > 1) <> 0
	AND dbo.GetNoCheck() = 0
	
BEGIN

	RAISERROR ('Duplicate PracticeName.LocationReference.',16,1)
	ROLLBACK TRANSACTION
END

GO

If EXists (Select 1 From SYS.triggers where name = 'ProvidersInsertResources')
BEGIN 
	DROP Trigger [dbo].[ProvidersInsertResources]
END

GO
CREATE TRIGGER [dbo].[ProvidersInsertResources] ON [dbo].[Resources]
AFTER INSERT
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @ResourceId INT
	DECLARE @ResourceType NVARCHAR(1)
	DECLARE @PracticeId INT
	DECLARE @Billable NVARCHAR(1)
	DECLARE @GoDirect NVARCHAR(1)
	DECLARE @ServiceCode NVARCHAR(4)

	DECLARE ProvidersInsertResourcesCursor CURSOR FOR
	SELECT ResourceId, ResourceType, PracticeId, Billable, GoDirect, ServiceCode
	FROM inserted WHERE ResourceType in ('D', 'Q', 'Z', 'Y', 'R')

	OPEN ProvidersInsertResourcesCursor FETCH NEXT FROM ProvidersInsertResourcesCursor INTO
	@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect, @ServiceCode
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		INSERT INTO model.Providers (
			IsBillable
			,IsEmr
			,BillingOrganizationId
			,UserId
			,ServiceLocationId
		)
		SELECT 
		* 
		FROM (
			SELECT DISTINCT
			CASE @Billable WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsBillable
			,CASE @GoDirect WHEN 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS IsEMR
			,CASE WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'
					THEN (110000000 * 1 + @ResourceId) 
				ELSE pnBillOrg.PracticeId
			END AS BillingOrganizationId
			,NULL AS UserId
			,pnBillOrg.PracticeId AS ServiceLocationId
			FROM inserted re
			INNER JOIN dbo.PracticeName pnBillOrg WITH(NOLOCK) ON re.PracticeId = pnBillOrg.PracticeId
				AND pnBillOrg.PracticeType = 'P'
			LEFT JOIN dbo.PracticeAffiliations pa WITH(NOLOCK) ON pa.ResourceId = re.ResourceId
					AND pa.ResourceType = 'R'
			WHERE re.ResourceId = @ResourceId
					AND @ResourceType = 'R'
					AND @ServiceCode = '02'
					AND @Billable = 'Y'
					AND pnBillOrg.LocationReference <> ''
	
			UNION
	
			SELECT DISTINCT
			CASE @Billable WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsBillable
			,CASE @GoDirect WHEN 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS IsEMR
			,CASE WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y' THEN (110000000 * 1 + @ResourceId) ELSE pnBillOrg.PracticeId END AS BillingOrganizationId
			,@ResourceId AS UserId
			,NULL AS ServiceLocationId
			FROM inserted re
			INNER JOIN dbo.PracticeName pnBillOrg WITH(NOLOCK) ON pnBillOrg.PracticeId = re.PracticeId
				AND pnBillOrg.PracticeType = 'P'
			LEFT JOIN dbo.PracticeAffiliations pa WITH(NOLOCK) ON pa.ResourceId = re.ResourceId
					AND pa.ResourceType = 'R'
			WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
				AND re.ResourceId = @ResourceId
		)v
		EXCEPT
		SELECT IsBillable, IsEmr, BillingOrganizationId, UserId, ServiceLocationId FROM model.Providers	WITH(NOLOCK)

		FETCH NEXT FROM ProvidersInsertResourcesCursor INTO 
		@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect, @ServiceCode
	END
	CLOSE ProvidersInsertResourcesCursor
	DEALLOCATE ProvidersInsertResourcesCursor
END

GO

If EXists (Select 1 From SYS.triggers where name = 'LegacyPatientReceivablesColumns')
BEGIN 
	DROP Trigger [model].[LegacyPatientReceivablesColumns]
END

GO
CREATE TRIGGER [model].[LegacyPatientReceivablesColumns]
ON [model].[Invoices] 
AFTER INSERT 
NOT FOR REPLICATION 
AS
BEGIN

SET NOCOUNT ON

UPDATE inv
SET LegacyDateTime = CONVERT(DATETIME,ap.AppDate)
	,LegacyPatientReceivablesPatientId = ap.PatientId
	,LegacyPatientReceivablesInvoice = ap.InvoiceNumber
	,LegacyPatientReceivablesAppointmentId = ap.AppointmentId
FROM model.Invoices inv WITH(NOLOCK)
INNER JOIN inserted i ON inv.Id = i.Id 
INNER JOIN dbo.Appointments ap WITH(NOLOCK) ON i.EncounterId = ap.EncounterId
WHERE i.InvoiceTypeId <> 2 AND ap.EncounterId = ap.AppointmentId

UPDATE inv
SET LegacyDateTime = CONVERT(DATETIME,ap.AppDate)
	,LegacyPatientReceivablesPatientId = ap.PatientId
	,LegacyPatientReceivablesInvoice = ap.InvoiceNumber
	,LegacyPatientReceivablesAppointmentId = ap.AppointmentId
FROM model.Invoices inv
INNER JOIN inserted i ON inv.Id = i.Id 
INNER JOIN dbo.Appointments ap WITH(NOLOCK) ON i.EncounterId = ap.EncounterId
WHERE i.InvoiceTypeId = 2 AND ap.EncounterId <> ap.AppointmentId

END


GO

If EXists (Select 1 From SYS.triggers where name = 'PatientsExternalSystemMessagesTrigger')
BEGIN 
	DROP Trigger [model].[PatientsExternalSystemMessagesTrigger]
END

GO
CREATE TRIGGER [model].[PatientsExternalSystemMessagesTrigger]
	ON  [model].[Patients]
	FOR INSERT, UPDATE NOT FOR REPLICATION
AS 
BEGIN

SET NOCOUNT ON;

	DECLARE @now as DATETIME
	SET @now = GETUTCDATE()

	DECLARE @insertTable TABLE(Id UNIQUEIDENTIFIER, MessageType INT, PatientId INT, [Description] NVARCHAR(255))
	DECLARE @createdBy nvarchar(max)

		INSERT INTO @insertTable 
		SELECT NEWID(), esesmt.Id, i.Id AS PatientId, ESMT.Name
		FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i
		CROSS JOIN model.ExternalSystemExternalSystemMessageTypes esesmt WITH(NOLOCK)
		INNER JOIN model.ExternalSystemMessageTypes esmt WITH(NOLOCK) ON esesmt.ExternalSystemMessageTypeId = esmt.Id AND esmt.Name IN ('ADT_A28_V23') AND esmt.IsOutbound = 1
		WHERE esesmt.IsDisabled = 0

	-- Create rows for each ExternalSystemExternalSystemMessageType subscribed for ADT_A28_V23
	-- If deleted table contains rows, then this was an UPDATE
	IF EXISTS(SELECT * FROM deleted)
	BEGIN
		SET @createdBy = 'PatientUpdate'
	END
	ELSE
	BEGIN
		SET @createdBy = 'PatientInsert'
	END

	-- Create the messages
	INSERT INTO model.ExternalSystemMessages(Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId, ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId)
	-- Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId (1 = Unprocessed, 2 = Processing, 3 = Processed),
	-- ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId
	SELECT Id, [Description], '', MessageType, 1, 0, @createdBy, @now, @now, ''
	FROM @insertTable

	-- Add entity associated with each message for PatientId
	INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
	-- Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey
	SELECT NEWID(), it.Id, pre.Id, PatientId
	FROM
	@insertTable it INNER JOIN model.PracticeRepositoryEntities pre ON pre.Name = 'Patient'
END

GO

