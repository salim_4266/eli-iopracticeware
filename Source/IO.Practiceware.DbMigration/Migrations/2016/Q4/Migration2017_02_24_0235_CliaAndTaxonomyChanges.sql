
 
Go
IF EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.Insurers') AND name = 'IsCLIAIncludedOnClaims')
BEGIN
	UPDATE model.Insurers SET IsCLIAIncludedOnClaims=1
	WHERE NAME IN ('MEDICARE','MEDICAID')
END

IF EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.Insurers')  AND name = 'IsTaxonomyIncludedOnClaims')
BEGIN
	UPDATE model.Insurers SET IsTaxonomyIncludedOnClaims=1
	WHERE NAME IN ('MEDICARE','MEDICAID')
END
Go






