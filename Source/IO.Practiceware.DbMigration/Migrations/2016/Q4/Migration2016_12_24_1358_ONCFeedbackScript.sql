--Script to create TABLE [Model].PsychologicalFeedback
--12/29/2016

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = 'Model' ) 

BEGIN
EXEC sp_executesql N'CREATE SCHEMA Model'
END



 IF NOT EXISTS ( SELECT 1 FROM SYSOBJECTS WHERE XTYPE = 'U'  AND NAME='PsychologicalFeedback') 
 BEGIN 
 Create TABLE [Model].PsychologicalFeedback ([Id] [int] IDENTITY (1,1) NOT NULL ,
  [QuestionId] [int] NOT NULL,
  [AnswerId] [int] NOT NULL , 
  [PatientId] [int] NOT NULL , 
  [LocationId] [int] NOT NULL ,  
  [CreatedDate] [DateTime] NOT NULL,
  [LastUpdatedDate] [DateTime] NOT NULL ,

CONSTRAINT [PK_ONCPsychologyFeedback]   PRIMARY KEY 
( 
   [Id]  
)   )  ON [PRIMARY] 

ALTER TABLE [model].PsychologicalFeedback  WITH NOCHECK ADD  CONSTRAINT [FK_ONCQuestionsId] FOREIGN KEY([QuestionId])
REFERENCES [model].[PsychologicalQuestions] ([QuestionId])

ALTER TABLE [model].PsychologicalFeedback  WITH NOCHECK ADD  CONSTRAINT [FK_ONCAnswersId] FOREIGN KEY([AnswerId])
REFERENCES [model].[PsychologicalAnswers] ([AnswerId])

ALTER TABLE [model].PsychologicalFeedback  WITH NOCHECK ADD  CONSTRAINT [FK_ONCPatientsId] FOREIGN KEY([PatientId])
REFERENCES [model].[Patients] ([Id])

END

 
