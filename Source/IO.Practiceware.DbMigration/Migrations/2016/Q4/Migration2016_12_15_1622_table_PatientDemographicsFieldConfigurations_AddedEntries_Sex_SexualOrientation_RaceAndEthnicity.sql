


--Script to add entries in [model].[PatientDemographicsFieldConfigurations]  for new fields(Sex, SexualOrientation, Race & Ethnicity)
--12/7/2016

DECLARE @currentIdentityValue INT;
SET @currentIdentityValue = IDENT_CURRENT('model.PatientDemographicsFieldConfigurations');


WHILE @currentIdentityValue < 40
BEGIN

INSERT INTO model.PatientDemographicsFieldConfigurations(IsVisibilityConfigurable,IsVisible,IsRequiredConfigurable,IsRequired,DefaultValue)
VALUES(1,0,0,0,NULL)

Set @currentIdentityValue = @currentIdentityValue + 1;

END;

GO

