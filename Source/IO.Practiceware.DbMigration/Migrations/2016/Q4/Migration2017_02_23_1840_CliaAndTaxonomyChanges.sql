Go
IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.Insurers') AND name = 'IsCLIAIncludedOnClaims')
BEGIN
	ALTER TABLE  model.Insurers
	ADD IsCLIAIncludedOnClaims bit DEFAULT 0 NOT NULL
END


IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.Insurers')  AND name = 'IsTaxonomyIncludedOnClaims')
BEGIN
	ALTER TABLE  model.Insurers
	ADD IsTaxonomyIncludedOnClaims bit DEFAULT 0 NOT NULL
END

Go







