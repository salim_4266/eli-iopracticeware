GO
UPDATE model.TemplateDocuments	
SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 	'@(Model.InsuranceTypeIsMedicaid', 
'@(Model.IsTaxonomyIncludedOnClaims' )) 
FROM model.TemplateDocuments 	 
WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL

UPDATE model.TemplateDocuments	
SET ContentOriginal=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),ContentOriginal), 	'@(Model.InsuranceTypeIsMedicaid',
'@(Model.IsTaxonomyIncludedOnClaims' )) 
FROM model.TemplateDocuments 	 
WHERE DisplayName = 'CMS-1500' AND ContentOriginal IS NOT NULL

UPDATE model.TemplateDocuments	
SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 	'<input type="text" id="ICDIndicator" value="9"', 
'<input type="text" id="ICDIndicator" value="@Model.DiagnosisTypeId"' )) 
FROM model.TemplateDocuments 	 
WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL

UPDATE model.TemplateDocuments	
SET ContentOriginal=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),ContentOriginal), 	'<input type="text" id="ICDIndicator" value="9"',
'<input type="text" id="ICDIndicator" value="@Model.DiagnosisTypeId"' )) 
FROM model.TemplateDocuments 	 
WHERE DisplayName = 'CMS-1500' AND ContentOriginal IS NOT NULL




GO
