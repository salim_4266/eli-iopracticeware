
--Script to create TABLE [Model].PsychologicalAnswers
--12/29/2016

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = 'Model' ) 

BEGIN
EXEC sp_executesql N'CREATE SCHEMA Model'
END



 IF NOT EXISTS ( SELECT 1 FROM SYSOBJECTS WHERE XTYPE = 'U'  AND NAME='PsychologicalAnswers') 
 BEGIN 
 CREATE TABLE [Model].PsychologicalAnswers ([AnswerId] [int] IDENTITY (1,1) NOT NULL ,  
  [AnswerDescription] [varchar] (500) NOT NULL , 
  [QuestionId] [int] NOT NULL ,   
CONSTRAINT [PK_ONCPsychologyAnswers]   PRIMARY KEY 
( 
   [AnswerId]  
)   )  ON [PRIMARY] 

ALTER TABLE [model].[PsychologicalAnswers]  WITH NOCHECK ADD  CONSTRAINT [FK_ONCQuestionId] FOREIGN KEY([QuestionId])
REFERENCES [model].[PsychologicalQuestions] ([QuestionId])

SET IDENTITY_INSERT Model.PsychologicalAnswers ON 

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (1,'Elementary school (1-8)',1)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (2,'High school (9-12)',1)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (3,'College (13-16)',1)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (4,'Graduate professional school (17-20+)',1)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (5,'High school diploma',2)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (6,'GED',2)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (7,'Vocational certificate (post high school or GED)',2)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (8,'Association degree (junior college)',2)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (9,'Bachelorís degree',2)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (10,'Masterís degree',2)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (11,'Doctorate',2)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (12,'White',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (13,'Black, African American, or Negro',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (14,'American Indian or Alaskan Native',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (15,'Asian Indian',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (16,'Chinese',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (17,'Filipino',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (18,'Japanese',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (19,'Korean',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (20,'Vietnamese',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (21,'Native Hawaiian',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (22,'Guamanian or Chamorro',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (23,'Samoan',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (24,'Other Pacific Islander',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (25,'Other Asian',3)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (26,'Some Other Race',3)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (27,'No, not Hispanic, Latino or Spanish Origin',4)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (28,'Yes, Mexican, Mexican American, Chicano',4)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (29,'Yes, Puerto Rican',4)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (30,'Yes, Cuban',4)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (31,'Yes, another Hispanic, Latino, or Spanish Origin',4)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (32,'Very hard',5)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (33,'Somewhat hard',5)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (34,'Not hard at all',5)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (35,'0',6)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (36,'1',6)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (37,'2',6)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (38,'3',6)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (39,'4',6)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (40,'5',6)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (41,'6',6)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (42,'7',6)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (43,'0',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (44,'10',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (45,'20',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (46,'30',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (47,'40',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (48,'50',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (49,'60',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (50,'70',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (51,'80',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (52,'90',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (53,'100',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (54,'110',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (55,'120',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (56,'130',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (57,'140',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (58,'150',7)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (59,'More than 150 minutes',7)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (60,'Every day',8)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (61,'Some days',8)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (62,'Not at all',8)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (63,'Do not Know',8)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (64,'Never',8)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (65,'Monthly or less',8)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (66,'2-4 times per month',8)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (67,'2-3 times per week',8)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (68,'4+ times per week',8)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (69,'Never',9)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (70,'Monthly or less',9)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (71,'2-4 times per month',9)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (72,'2-3 times per week',9)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (73,'4+ times per week',9)

--------------------------------------------------------------------------------------------------------------
INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (131,'None',10)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (74,'1 or 2',10)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (75,'3 or 4',10)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (76,'5 or 6',10)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (77,'7 to 9',10)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (78,'10 or more',10)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (79,'Never',11)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (80,'Less than monthly',11)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (81,'Monthly',11)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (82,'Weekly',11)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (83,'Daily or almost daily',11)

--------------------------------------------------------------------------------------------------------------
INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (130,'Single',12)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (84,'Married',12)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (85,'Separated',12)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (86,'Divorced (marriage annulled)',12)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (87,'Widowed',12)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (88,'More than once a day',13)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (89,'Once a day',13)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (90,'2 or 3 times a week',13)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (91,'About once a week',13)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (92,'Less than once a week',13)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (93,'Never or no phone',13)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (94,'More than once a week',14)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (95,'Once a week',14)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (96,'2 or 3 times a month',14)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (97,'About once a month',14)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (98,'Never',14)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (99,'More than once a week',15)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (100,'Once a week',15)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (101,'2 or 3 times a month',15)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (102,'About once a month',15)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (103,'Never',15)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (104,'More than once a week',16)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (105,'Once a week',16)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (106,'2 or 3 times a month',16)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (107,'About once a month',16)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (108,'Never',16)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (109,'Not at all',17)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (110,'To some extent',17)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (111,'Very much',17)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (112,'Not at all',18)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (113,'Several days',18)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (114,'More than half the days',18)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (115,'Nearly every day',18)

--------------------------------------------------------------------------------------------------------------


INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (116,'Not at all',19)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (117,'Several days',19)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (118,'More than half the days',19)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (119,'Nearly every day',19)

--------------------------------------------------------------------------------------------------------------


INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (120,'Not at all',20)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (121,'Several days',20)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (122,'More than half the days',20)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (123,'Nearly every day',20)

--------------------------------------------------------------------------------------------------------------


INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (124,'Yes',21)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (125,'No',21)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (126,'Yes',22)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (127,'No',22)

--------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (128,'Yes',23)

INSERT INTO Model.PsychologicalAnswers([AnswerId],[AnswerDescription],[QuestionId]) 
VALUES (129,'No',23)

SET IDENTITY_INSERT Model.PsychologicalAnswers OFF 
--------------------------------------------------------------------------------------------------------------

 END