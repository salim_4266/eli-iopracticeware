--Script to create TABLE [Model].PsychologicalQuestions
--12/29/2016

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = 'Model' ) 

BEGIN
EXEC sp_executesql N'CREATE SCHEMA Model'
END



 IF NOT EXISTS ( SELECT 1 FROM SYSOBJECTS WHERE XTYPE = 'U'  AND NAME='PsychologicalQuestions') 
 BEGIN 
 CREATE TABLE [Model].PsychologicalQuestions ([QuestionId] [int] IDENTITY (1,1) NOT NULL ,  
  [QuestionDescription] [varchar] (500) NOT NULL ,  
  [IsActive] [bit] NOT NULL ,  
  [QuestionCategory] [int] NOT NULL ,   
CONSTRAINT [PK_ONCPsychologyQuestions]   PRIMARY KEY 
( 
   [QuestionId]  
)   )  ON [PRIMARY] 

SET IDENTITY_INSERT Model.PsychologicalQuestions ON 

INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (1,'What is the highest level of school you have completed?',1,1)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (2,'What is the highest degree you have earned?',1,1)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (3,'What is your race?',1,1)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (4,'Are you of Hispanic, Latino or Spanish origin?',1,1)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (5,'How hard is it for you to pay for very basics like food, housing, medical care and eating? Would you say it is...',1,1)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (6,'On average, how many days per week do you engage in moderate to strenuous exercise (like walking fast, running, jogging, dancing, swimming, biking, or other activities that cause a heavy sweat)?',1,1)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (7,'On average, how many minutes do you engage in exercise at this level?',1,1)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (8,'Do you smoke cigarettes every day, some day or not at all?',1,1)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (9,'How often do you have a drink containing alcohol?',1,1)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (10,'How many standard drinks containing alcohol do you have on a typical day?',1,1)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (11,'How often do you have six or more drinks on one occasion?',1,1)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (12,'What is your current marital status?',1,2)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (13,'In a typical week, how many times do you talk on the telephone with family, friends, or neighbors?',1,2)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (14,'How often do you get together with friends, neighbors or relatives?',1,2)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (15,'How often do you attend meetings or programs of groups, clubs or organizations that you belong to?',1,2)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (16,'How often do you attend church or religious services?',1,2)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (17,'Stress refers to a situation where a person feels tensed, restless, nervous, or anxious, or is unable to sleep at night because his/her mind is troubled all the time. Do you feel that kind of stress these days?',1,3)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (18,'Little interest or pleasure in doing things?',1,3)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (19,'Feeling down, depressed, or hopeless?',1,3)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (20,'Within the last year, have you been humiliated or emotionally abused in other ways by your partner or ex-partner?',1,3)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (21,'Within the last year, have you been afraid of your partner or ex-partner?',1,3)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (22,'Within the last year, have you been raped or forced to have any kind of sexual activity by your partner or ex-partner?',1,3)
INSERT INTO Model.PsychologicalQuestions([QuestionId],[QuestionDescription],[IsActive],[QuestionCategory]) 
VALUES (23,'Within the last year, have you been kicked, hit, slapped or otherwise physically hurt by your partner or ex-partner?',1,3)

SET IDENTITY_INSERT Model.PsychologicalQuestions OFF 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 END