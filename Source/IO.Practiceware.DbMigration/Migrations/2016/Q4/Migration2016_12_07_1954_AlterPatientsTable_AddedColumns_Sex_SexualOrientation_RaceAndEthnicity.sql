


--Script to add columns in [model].[Patients] 
--12/7/2016



IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'[model].[Patients]') 
		AND name = 'SexId')
BEGIN
ALTER TABLE  [model].[Patients]
  ADD [SexId] [int] DEFAULT NULL 
END

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'[model].[Patients]') 
		AND name = 'SexualOrientationId')
BEGIN
ALTER TABLE  [model].[Patients]
  ADD [SexualOrientationId] [int] DEFAULT NULL 
END

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'[model].[Patients]') 
		AND name = 'RaceAndEthnicityId')
BEGIN
ALTER TABLE  [model].[Patients]
  ADD [RaceAndEthnicityId] [int] DEFAULT NULL 
END


