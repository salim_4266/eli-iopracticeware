GO
Update model.reports set content='<?xml version="1.0" encoding="utf-8"?>
<Report DataSourceName="PatientInsurers" Width="6.49996058146159in" Name="Patient Insurers" SnapToGrid="False" xmlns="http://schemas.telerik.com/reporting/2012/3.5">
  <Style Color="Black" LineStyle="Dashed">
    <Padding Left="0.02in" Top="2pt" />
  </Style>
  <DataSources>
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="model.GetPatientInsurers" SelectCommandType="StoredProcedure" CommandTimeout="600" Name="PatientInsurers">
      <Parameters>
        <SqlDataSourceParameter DbType="DateTime" Name="@StartDate">
          <Value>
            <String>= Parameters.StartDate.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="DateTime" Name="@EndDate">
          <Value>
            <String>= Parameters.EndDate.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="String" Name="@Insurers">
          <Value>
            <String>=JOIN(",",Parameters.Insurers.Value)</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="String" Name="@EncounterServices">
          <Value>
            <String>=JOIN(",",Parameters.EncounterServices.Value)</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="String" Name="@DiagnosisCodes">
          <Value>
            <String>=JOIN(",",Parameters.BillingDiagnosis.Value)</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="Boolean" Name="@BreakdownByInsurer">
          <Value>
            <String>=Parameters.BreakdownByInsurer.Value</String>
          </Value>
        </SqlDataSourceParameter>
      </Parameters>
      <DefaultValues>
        <SqlDataSourceParameter DbType="DateTime" Name="@StartDate" />
        <SqlDataSourceParameter DbType="DateTime" Name="@EndDate" />
        <SqlDataSourceParameter DbType="String" Name="@Insurers" />
        <SqlDataSourceParameter DbType="String" Name="@EncounterServices" />
        <SqlDataSourceParameter DbType="String" Name="@DiagnosisCodes" />
        <SqlDataSourceParameter DbType="Boolean" Name="@BreakdownByInsurer" />
      </DefaultValues>
    </SqlDataSource>
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT DISTINCT Name FROM model.Insurers ORDER BY Name" CommandTimeout="600" Name="Insurers" />
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT DISTINCT Code, Description from model.EncounterServices&#xD;&#xA;WHERE Code &lt;&gt; ''''&#xD;&#xA;ORDER BY Code" CommandTimeout="600" Name="EncounterServices" />
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT DISTINCT esem.ExternalSystemEntityKey AS Icd9DiagnosisCode &#xD;&#xA;FROM model.BillingDiagnosis bd JOIN model.ExternalSystemEntityMappings esem ON esem.Id = bd.ExternalSystemEntityMappingId &#xD;&#xA;&#xD;&#xA;Union&#xD;&#xA;&#xD;&#xA;SELECT DISTINCT icd10.DiagnosisCode as Icd10DiagnosisCode from &#xD;&#xA;model.BillingDiagnosisIcd10 icd10 Join ICD10.ICD10 icd on icd.DiagnosisCode=icd10.DiagnosisCode &#xD;&#xA;&#xD;&#xA;&#xD;&#xA;&#xD;&#xA;&#xD;&#xA;" CommandTimeout="600" Name="BillingDiagnosis">
      <CalculatedFields>
        <CalculatedField Name="Icd10DiagnosisCode">
          <DataType>System.String</DataType>
        </CalculatedField>
      </CalculatedFields>
    </SqlDataSource>
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="&#xD;&#xA;SELECT TOP 1 &#xD;&#xA;&#x9;bo.Name AS MainOfficeName&#xD;&#xA;&#x9;,a.Line1 AS MainOfficeLine1&#xD;&#xA;&#x9;,a.Line2 AS MainOfficeLine2&#xD;&#xA;&#x9;,a.Line3 AS MainOfficeLine3&#xD;&#xA;&#x9;,a.City AS MainOfficeCity&#xD;&#xA;&#x9;,sop.Abbreviation AS MainOfficeState&#xD;&#xA;&#x9;,SUBSTRING(a.PostalCode, 0,6) AS MainOfficePostalCode&#xD;&#xA;&#x9;,bop.AreaCode + ''-'' +  SUBSTRING(bop.ExchangeAndSuffix,0,4) + ''-'' + SUBSTRING(bop.ExchangeAndSuffix,4,8) AS ExchangeAndSuffix&#xD;&#xA;FROM model.BillingOrganizationAddresses a&#xD;&#xA;INNER JOIN model.StateOrProvinces sop ON a.StateOrProvinceId = sop.Id&#xD;&#xA;INNER JOIN model.BillingOrganizations bo ON a.BillingOrganizationId = bo.Id&#xD;&#xA;INNER JOIN model.BillingOrganizationPhoneNumbers bop ON bop.BillingOrganizationId = bo.Id&#xD;&#xA;WHERE bo.IsMain = 1&#xD;&#xA;" Name="OfficeAddress" />
  </DataSources>
  <Items>
    <DetailSection Height="0.199999968210856in" Name="detail">
      <Style Visible="True" />
      <Items>
        <TextBox Width="0.899999777475993in" Height="0.199999968210856in" Left="0.382213115692139in" Top="0in" Value="= Fields.StartDate" Name="textBox18" StyleName="">
          <Style BackgroundColor="White" TextAlign="Right" VerticalAlign="Middle">
            <BorderStyle Default="None" Top="None" Bottom="None" Left="None" Right="None" />
            <BorderColor Top="Black" Bottom="Black" Left="Black" Right="Black" />
            <BorderWidth Top="1pt" Bottom="1pt" Left="1pt" Right="1pt" />
            <Font Size="8pt" Bold="False" />
            <Padding Left="0.02in" Top="0.02in" />
          </Style>
        </TextBox>
        <TextBox Width="1.08841280142466in" Height="0.199999978144963in" Left="1.54316902160645in" Top="0in" Value="=Fields.Icd9DiagnosisCode" Name="textBox11" StyleName="Office.TableBody">
          <Style TextAlign="Center" VerticalAlign="Middle">
            <BorderWidth Default="1pt" Top="1pt" Bottom="1pt" Left="1pt" Right="1pt" />
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="1.16162729263306in" Height="0.19999997317791in" Left="5.23412704467773in" Top="0in" Value="=Fields.Description" Name="textBox13" StyleName="Office.TableBody">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <BorderWidth Default="1pt" Top="1pt" Bottom="1pt" Left="1pt" Right="1pt" />
            <Font Name="Arial" Size="8pt" />
            <Padding Left="0.0500000007450581in" />
          </Style>
        </TextBox>
        <TextBox Width="0.949684182802836in" Height="0.199999978144963in" Left="2.92724068959554in" Top="0in" Value="=Fields.Code" Name="textBox12" StyleName="Office.TableBody">
          <Style TextAlign="Center" VerticalAlign="Middle">
            <BorderStyle Right="None" />
            <BorderWidth Default="1pt" Top="1pt" Bottom="1pt" Left="1pt" Right="1pt" />
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
      </Items>
    </DetailSection>
    <ReportHeaderSection Height="2.12211000919342in" Name="reportHeaderSection1">
      <Style>
        <Font Size="8pt" />
      </Style>
      <Items>
        <TextBox Width="3.62708393732706in" Height="0.212421039740245in" Left="1.43549505869548in" Top="0in" Value="Patients by Insurer, Diagnosis, and Service" Name="reportNameTextBox" StyleName="PageInfo">
          <Style TextAlign="Center" VerticalAlign="Middle">
            <Font Size="12pt" Bold="True" />
          </Style>
        </TextBox>
        <HtmlTextBox Width="0.187500000000005in" Height="0.200000007947286in" Left="5.68117141723633in" Top="0.212499777475993in" Value="Billing" Name="ReportCategory">
          <Style Visible="False" VerticalAlign="Middle">
            <Padding Left="2pt" Right="2pt" />
          </Style>
        </HtmlTextBox>
        <TextBox Width="1.1811021566391in" Height="0.212420930465062in" Left="5.31885862350464in" Top="0in" Value="Date run: {Now().ToString(&quot;MM/dd/yyyy&quot;)}&#xD;&#xA;{Now().ToString(&quot;hh:mm tt&quot;)}" Name="textBox17">
          <Style TextAlign="Right" VerticalAlign="Middle">
            <Font Name="Arial" />
            <Padding Left="2pt" Right="2pt" />
          </Style>
        </TextBox>
        <List DataSourceName="OfficeAddress" Width="3.20416680971781in" Height="0.800157070159912in" Left="0in" Top="0.212499698003133in" Name="list1">
          <Body>
            <Cells>
              <TableCell RowIndex="0" ColumnIndex="0" RowSpan="1" ColumnSpan="1">
                <ReportItem>
                  <Panel Width="3.20416680971781in" Height="0.800157070159912in" Left="0in" Top="0in" Name="panel1">
                    <Items>
                      <TextBox Width="3.20416688919067in" Height="0.199921295046806in" Left="0in" Top="0in" Value="{MainOfficeName}" Name="textBox92">
                        <Style TextAlign="Left" VerticalAlign="Middle">
                          <Font Size="10pt" Bold="True" />
                        </Style>
                      </TextBox>
                      <TextBox Width="3.20416688919067in" Height="0.19999997317791in" Left="0in" Top="0.199999968210856in" Value="{MainOfficeLine1} {MainOfficeLine2}" Name="textBox93">
                        <Style TextAlign="Left" VerticalAlign="Middle">
                          <Font Size="10pt" Bold="True" />
                        </Style>
                      </TextBox>
                      <TextBox Width="3.20416688919067in" Height="0.19999997317791in" Left="0in" Top="0.400078614552816in" Value="{MainOfficeCity}, {MainOfficeState} {MainOfficePostalCode}" Name="textBox94">
                        <Style TextAlign="Left" VerticalAlign="Middle">
                          <Font Size="10pt" Bold="True" />
                        </Style>
                      </TextBox>
                      <TextBox Width="3.20416688919067in" Height="0.19999997317791in" Left="0in" Top="0.600117683410645in" Value="{ExchangeAndSuffix}" Name="textBox95">
                        <Style TextAlign="Left" VerticalAlign="Middle">
                          <Font Size="10pt" Bold="True" />
                        </Style>
                      </TextBox>
                    </Items>
                  </Panel>
                </ReportItem>
              </TableCell>
            </Cells>
            <Columns>
              <Column Width="3.20416680971781in" />
            </Columns>
            <Rows>
              <Row Height="0.800157070159912in" />
            </Rows>
          </Body>
          <Corner />
          <Style>
            <Padding Left="2pt" />
          </Style>
          <RowGroups>
            <TableGroup Name="DetailGroup">
              <Groupings>
                <Grouping />
              </Groupings>
            </TableGroup>
          </RowGroups>
          <ColumnGroups>
            <TableGroup Name="ColumnGroup" />
          </ColumnGroups>
        </List>
        <TextBox Width="2.74853563308716in" Height="0.197837829589844in" Left="1.28229141235352in" Top="1.13541666666667in" Value="{Parameters.StartDate.Value.ToString(&quot;MM/dd/yyyy&quot;)}" Format="{0:d}" Name="textBox145">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Size="8pt" Bold="False" />
            <Padding Left="2pt" />
          </Style>
        </TextBox>
        <TextBox Width="2.74853587150574in" Height="0.198957845568657in" Left="1.28229141235352in" Top="1.33333333333333in" Value="{Parameters.EndDate.Value.ToString(&quot;MM/dd/yyyy&quot;)}" Format="{0:d}" Name="textBox146">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Size="8pt" Bold="False" />
            <Padding Left="2pt" />
          </Style>
        </TextBox>
        <TextBox Width="1.2822128534317in" Height="0.197916507720947in" Left="0in" Top="1.33333333333333in" Value="End Date:" Format="{0:d}" Name="textBox84">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Size="8pt" Bold="True" />
            <Padding Left="2pt" />
          </Style>
        </TextBox>
        <TextBox Width="1.28221249580383in" Height="0.196796059608459in" Left="0in" Top="1.13541666666667in" Value="Start Date:" Format="{0:d}" Name="textBox81">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Size="8pt" Bold="True" />
            <Padding Left="2pt" />
          </Style>
        </TextBox>
        <TextBox Width="5.21766916910807in" Height="0.196527540683746in" Left="1.28229141235352in" Top="1.53237009048462in" Value="= IsNull(Join(&quot;,&quot;,Parameters.Insurers.Value),&quot;All&quot;)" Format="" Name="textBox91">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Size="8pt" Bold="False" />
            <Padding Left="2pt" />
          </Style>
        </TextBox>
        <TextBox Width="1.28221261501312in" Height="0.196527540683746in" Left="0in" Top="1.53237009048462in" Value="Insurer(s):" Format="" Name="textBox90">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Size="8pt" Bold="True" />
            <Padding Left="2pt" />
          </Style>
        </TextBox>
        <TextBox Width="1.28221249580383in" Height="0.196527540683746in" Left="0in" Top="1.72897640864054in" Value="Service Code(s):" Format="" Name="textBox5">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Size="8pt" Bold="True" />
            <Padding Left="2pt" />
          </Style>
        </TextBox>
        <TextBox Width="5.2102993329366in" Height="0.196527540683746in" Left="1.28966172536214in" Top="1.72897640864054in" Value="= IsNull(Join(&quot;,&quot;,Parameters.EncounterServices.Value),&quot;All&quot;)" Format="" Name="textBox6">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Size="8pt" Bold="False" />
            <Padding Left="2pt" />
          </Style>
        </TextBox>
        <TextBox Width="1.28221265474955in" Height="0.196527540683746in" Left="0.00000000124176344in" Top="1.92558252811432in" Value="Diagnos(is/es):" Format="" Name="textBox7">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Size="8pt" Bold="True" />
            <Padding Left="2pt" />
          </Style>
        </TextBox>
        <TextBox Width="5.21766932805379in" Height="0.196527540683746in" Left="1.28229141235352in" Top="1.92558252811432in" Value="= IsNull(Join(&quot;,&quot;,Parameters.BillingDiagnosis.Value),&quot;All&quot;)" Format="" Name="textBox8">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Size="8pt" Bold="False" />
            <Padding Left="2pt" />
          </Style>
        </TextBox>
      </Items>
    </ReportHeaderSection>
    <PageHeaderSection Height="0.174911379814148in" Name="pageHeaderSection1">
      <Items>
        <TextBox Width="1.18110243479411in" Height="0.174911379814148in" Left="5.31881904602051in" Top="0in" Value="Page {PageNumber}" Anchoring="Right" Name="textBox4">
          <Style TextAlign="Right" VerticalAlign="Middle">
            <Font Size="8pt" />
            <Padding Left="2pt" Right="2pt" />
          </Style>
        </TextBox>
      </Items>
    </PageHeaderSection>
  </Items>
  <PageSettings>
    <PageSettings PaperKind="Letter" Landscape="False">
      <Margins>
        <MarginsU Left="1in" Right="1in" Top="1in" Bottom="1in" />
      </Margins>
    </PageSettings>
  </PageSettings>
  <Sortings>
    <Sorting Expression="= Fields.StartDateTime" Direction="Asc" />
    <Sorting Expression="= Fields.Code" Direction="Asc" />
    <Sorting Expression="= Fields.Icd9DiagnosisCode" Direction="Asc" />
  </Sortings>
  <Groups>
    <Group Name="group3">
      <GroupHeader>
        <GroupHeaderSection Height="0.208333333333333in" Name="groupHeaderSection3">
          <Items>
            <TextBox Width="1.09182486279557in" Height="0.199921136101087in" Left="1.54316902160645in" Top="0in" Value="Billing Diagnosis" Name="textBox3" StyleName="">
              <Style BackgroundColor="White" TextAlign="Center" VerticalAlign="Middle">
                <BorderStyle Default="None" Top="None" Bottom="None" Left="None" Right="None" />
                <BorderColor Top="Black" Bottom="Black" Left="Black" Right="Black" />
                <BorderWidth Top="1pt" Bottom="1pt" Left="1pt" Right="1pt" />
                <Font Size="9pt" Bold="True" />
                <Padding Left="0.0199999995529652in" Top="0.0199999995529652in" />
              </Style>
            </TextBox>
            <TextBox Width="0.947837948799133in" Height="0.199921136101087in" Left="2.92724068959554in" Top="0in" Value="Billed Services" Name="textBox20" StyleName="">
              <Style BackgroundColor="White" TextAlign="Center" VerticalAlign="Middle">
                <BorderStyle Default="None" Top="None" Bottom="None" Left="None" Right="None" />
                <BorderColor Top="Black" Bottom="Black" Left="Black" Right="Black" />
                <BorderWidth Top="1pt" Bottom="1pt" Left="1pt" Right="1pt" />
                <Font Size="9pt" Bold="True" />
                <Padding Left="0.0199999995529652in" Top="0.0199999995529652in" />
              </Style>
            </TextBox>
            <TextBox Width="1.25537776947021in" Height="0.19999997317791in" Left="5.14037704467773in" Top="0in" Value="Service Description" Name="textBox21" StyleName="">
              <Style BackgroundColor="White" TextAlign="Left" VerticalAlign="Middle">
                <BorderStyle Default="None" Top="None" Bottom="None" Left="None" Right="None" />
                <BorderColor Top="Black" Bottom="Black" Left="Black" Right="Black" />
                <BorderWidth Top="1pt" Bottom="1pt" Left="1pt" Right="1pt" />
                <Font Size="9pt" Bold="True" />
                <Padding Left="0.0199999995529652in" Top="0.0199999995529652in" />
              </Style>
            </TextBox>
          </Items>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.0902778307596843in" Name="groupFooterSection3">
          <Style Visible="False" />
        </GroupFooterSection>
      </GroupFooter>
    </Group>
    <Group Name="group1">
      <GroupHeader>
        <GroupHeaderSection Height="0.205516080061595in" Name="groupHeaderSection1">
          <Items>
            <TextBox Width="3.09992082913717in" Height="0.199960549672445in" Left="0in" Top="0in" Value="= Fields.Name" Name="textBox1" StyleName="">
              <Style BackgroundColor="White" TextAlign="Left" VerticalAlign="Middle">
                <BorderStyle Default="None" Top="None" Bottom="None" Left="None" Right="None" />
                <BorderColor Top="Black" Bottom="Black" Left="Black" Right="Black" />
                <BorderWidth Top="1pt" Bottom="1pt" Left="1pt" Right="1pt" />
                <Font Size="9pt" Bold="True" />
                <Padding Left="0.02in" Top="0.02in" />
              </Style>
            </TextBox>
          </Items>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="= Parameters.BreakdownByInsurer.Value" Operator="Equal" Value="= 0" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.0520833333333333in" Name="groupFooterSection1">
          <Style Visible="False" />
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="= Fields.Name" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= Fields.Name" Direction="Asc" />
      </Sortings>
    </Group>
    <Group Name="group">
      <GroupHeader>
        <GroupHeaderSection Height="0.199999968210856in" Name="groupHeaderSection">
          <Items>
            <TextBox Width="2.89992070198059in" Height="0.199999968210856in" Left="0.199999968210856in" Top="0in" Value="{PatientsName} " Name="textBox2" StyleName="">
              <Style BackgroundColor="White" TextAlign="Left" VerticalAlign="Middle">
                <BorderStyle Default="None" Top="None" Bottom="None" Left="None" Right="None" />
                <BorderColor Top="Black" Bottom="Black" Left="Black" Right="Black" />
                <BorderWidth Top="1pt" Bottom="1pt" Left="1pt" Right="1pt" />
                <Font Size="8pt" Bold="True" />
                <Padding Left="0.02in" Top="0.02in" />
              </Style>
            </TextBox>
          </Items>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.0520833333333333in" Name="groupFooterSection">
          <Style Visible="False" />
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="= Fields.Name" />
        <Grouping Expression="= Fields.PatientsName" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= Fields.PatientsName" Direction="Asc" />
      </Sortings>
    </Group>
  </Groups>
  <ReportParameters>
    <ReportParameter Name="StartDate" Type="DateTime" Text="Start Date" Visible="True">
      <Value>
        <String></String>
      </Value>
    </ReportParameter>
    <ReportParameter Name="EndDate" Type="DateTime" Text="End Date" Visible="True">
      <Value>
        <String></String>
      </Value>
    </ReportParameter>
    <ReportParameter Name="Insurers" Text="Insurer(s)" Visible="True" MultiValue="True" AllowNull="True">
      <AvailableValues DataSourceName="Insurers" ValueMember="Name" />
      <Value>
        <String></String>
      </Value>
    </ReportParameter>
    <ReportParameter Name="EncounterServices" Text="Service(s)" Visible="True" MultiValue="True" AllowNull="True">
      <AvailableValues DataSourceName="EncounterServices" DisplayMember="= Fields.Code + ''- '' + Fields.Description" ValueMember="Code" />
      <Value>
        <String></String>
      </Value>
    </ReportParameter>
    <ReportParameter Name="BillingDiagnosis" Text="Diagnos(is/es)" Visible="True" MultiValue="True" AllowNull="True">
      <AvailableValues DataSourceName="BillingDiagnosis" ValueMember="Icd9DiagnosisCode" />
      <Value>
        <String></String>
      </Value>
    </ReportParameter>
    <ReportParameter Name="BreakdownByInsurer" Type="Boolean" Text="Breakdown By Insurer" Visible="True">
      <Value>
        <String>= True</String>
      </Value>
    </ReportParameter>
  </ReportParameters>
</Report>'
WHERE Name='Patients by Insurer, Diagnosis, and Service'
Go

Update model.reports set content='<?xml version="1.0" encoding="utf-8"?> 
 <Report DataSourceName="Query" Width="6.49996058146159in" 
 Name="Patients Without Future Appointments Or Recalls"
  xmlns="http://schemas.telerik.com/reporting/2012/3.5">   
   <DataSources>      <SqlDataSource ConnectionString="PracticeRepository"
    SelectCommand="model.GetPatientsWithoutFutureAppointmentsOrRecalls" 
    SelectCommandType="StoredProcedure" CommandTimeout="240" Name="Query">      
      <Parameters>        
        <SqlDataSourceParameter DbType="DateTime" Name="@StartDate">          
		  <Value>              <String>=Parameters.StartDate.Value</String>          
		    </Value>          </SqlDataSourceParameter>          <SqlDataSourceParameter DbType="DateTime" Name="@EndDate">            <Value>              <String>= Parameters.EndDate.Value</String>            </Value>          </SqlDataSourceParameter>          <SqlDataSourceParameter DbType="String" Name="@DefaultProviders">            <Value>            
			  <String>= Join(",",Parameters.DefaultProviders.Value)</String>            </Value>          </SqlDataSourceParameter>          <SqlDataSourceParameter DbType="String" Name="@Diagnoses">            <Value>              <String>= Join(",",Parameters.Diagnoses.Value)</String>            </Value>          </SqlDataSourceParameter>          <SqlDataSourceParameter DbType="Boolean" Name="@DefaultProviderBreakdown">            <Value>              <String>=Parameters.DefaultProviderBreakdown.Value</String>            </Value>          </SqlDataSourceParameter>          <SqlDataSourceParameter DbType="String" Name="@PreferedLocation">            <Value>              <String>= Join(",",Parameters.ServiceLocation.Value)</String>            </Value>          </SqlDataSourceParameter>        </Parameters>        <DefaultValues>          <SqlDataSourceParameter DbType="DateTime" Name="@StartDate" />          <SqlDataSourceParameter DbType="DateTime" Name="@EndDate" />          <SqlDataSourceParameter DbType="String" Name="@DefaultProviders" />          <SqlDataSourceParameter DbType="String" Name="@Diagnoses" />          <SqlDataSourceParameter DbType="Boolean" Name="@DefaultProviderBreakdown" />        </DefaultValues>      </SqlDataSource>      <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT DISTINCT u.Id, u.UserName FROM model.Patients p&#xD;&#xA;INNER JOIN model.Users u ON u.Id = p.DefaultUserId" Name="DefaultProviders" />      <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT DISTINCT esem.ExternalSystemEntityKey AS &#xD;&#xA;Icd9DiagnosisCode FROM model.BillingDiagnosis bd &#xD;&#xA;JOIN model.ExternalSystemEntityMappings esem &#xD;&#xA;ON esem.Id = bd.ExternalSystemEntityMappingId &#xD;&#xA;Union&#xD;&#xA;SELECT DISTINCT icd10.DiagnosisCode as Icd10DiagnosisCode&#xD;&#xA; from model.BillingDiagnosisIcd10 icd10 Join ICD10.ICD10 icd&#xD;&#xA; on icd.DiagnosisCode=icd10.DiagnosisCode" CommandTimeout="60" Name="Diagnoses" />      <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT DISTINCT Name,id FROM model.ServiceLocations&#xD;&#xA;ORDER BY Name" Name="ServiceLocation" />    </DataSources>    <Items>      <PageHeaderSection Height="0.200000047683716in" Name="pageHeaderSection1">        <Items>          <TextBox Width="1.59996032714844in" Height="0.199960708618164in" Left="4.90000025431315in" Top="0in" Value="Page {PageNumber}" Name="textBox1">            <Style TextAlign="Right" VerticalAlign="Middle" />          </TextBox>        </Items>      </PageHeaderSection>      <DetailSection Height="0.199999968210856in" Name="detailSection1">        <Items>          <HtmlTextBox Width="0.354166746139527in" Height="0.199999968210856in" Left="0.00007915496826172in" Top="0in" Value="To filter appointments/recalls by patients'' preferred location, select the locations you wish to filter before running the report.  To set a patient''s preferred service location, go to the patient''s demographics tab and select a preferred location.&amp;nbsp;" Name="Documentation">            <Style Visible="False">              <Font Size="8pt" />            </Style>          </HtmlTextBox>          <TextBox Width="1.19928872585297in" Height="0.199763298034668in" Left="5.30031585693359in" Top="0in" Value="{ThirdPhoneType}" Name="textBox15">            <Style TextAlign="Left" VerticalAlign="Middle" />          </TextBox>          <TextBox Width="1.30007906754812in" Height="0.199763298034668in" Left="4.00015799204508in" Top="0in" Value="{ThirdPhoneNumber}{ThirdExtension}" Name="textBox16">            <Style TextAlign="Left" VerticalAlign="Middle" />          </TextBox>        </Items>        <ConditionalFormatting>          <FormattingRule>            <Style Visible="False" />            <Filters>              <Filter Expression="= Fields.ThirdPhoneNumber" Operator="Equal" Value="0" />            </Filters>          </FormattingRule>        </ConditionalFormatting>      </DetailSection>      <ReportHeaderSection Height="0.300000031789144in" Name="reportHeaderSection1">        <Items>          <TextBox Width="6.49996058146159in" Height="0.299960613250732in" Left="0.00003941853841146in" Top="0in" Value="Patients Without Future Appointments or Recalls" Name="textBox2">            <Style TextAlign="Center" VerticalAlign="Middle">              <Font Size="14pt" />            </Style>          </TextBox>        </Items>      </ReportHeaderSection>    </Items>    <StyleSheet>      <StyleRule>        <Style>          <Padding Left="2pt" Right="2pt" />        </Style>        <Selectors>          <TypeSelector Type="TextItemBase" />          <TypeSelector Type="HtmlTextBox" />        </Selectors>      </StyleRule>    </StyleSheet>    <PageSettings>      <PageSettings PaperKind="Letter" Landscape="False">        <Margins>          <MarginsU Left="1in" Right="1in" Top="1in" Bottom="1in" />        </Margins>      </PageSettings>    </PageSettings>    <Groups>      <Group Name="group">        <GroupHeader>          <GroupHeaderSection PrintOnEveryPage="True" Height="0.200078884760539in" Name="groupHeaderSection">            <Items>              <TextBox Width="2.19984213511149in" Height="0.200000127156576in" Left="0in" Top="0in" Value="Patient" Name="textBox3">                <Style VerticalAlign="Middle">                  <Font Bold="True" />                </Style>              </TextBox>              <TextBox Width="0.700000127156575in" Height="0.199999968210856in" Left="2.19992097218831in" Top="0in" Value="Id" Name="textBox4">                <Style VerticalAlign="Middle">                  <Font Bold="True" />                </Style>              </TextBox>              <TextBox Width="1.2997620900472in" Height="0.200000127156576in" Left="4.00015799204508in" Top="0in" Value="Phone Number" Name="textBox5">                <Style VerticalAlign="Middle">                  <Font Bold="True" />                </Style>              </TextBox>              <TextBox Width="1.20000012715658in" Height="0.199999968210856in" Left="5.29999923706055in" Top="0in" Value="Phone Type" Name="textBox6">                <Style VerticalAlign="Middle">                  <Font Bold="True" />                </Style>              </TextBox>              <TextBox Width="1.10007921854655in" Height="0.200000127156576in" Left="2.89999993642172in" Top="0in" Value="Last Visit" Name="textBox7">                <Style VerticalAlign="Middle">                  <Font Bold="True" />                </Style>              </TextBox>            </Items>          </GroupHeaderSection>        </GroupHeader>        <GroupFooter>          <GroupFooterSection Height="0.0999999046325682in" Name="groupFooterSection">            <Style Visible="False" />          </GroupFooterSection>        </GroupFooter>      </Group>      <Group Name="group4">        <GroupHeader>          <GroupHeaderSection Height="0.200039386749268in" Name="groupHeaderSection4">            <Items>              <TextBox Width="2.19984217484792in" Height="0.19999997317791in" Left="0in" Top="0in" Value="{DefaultProvider}" Name="textBox18">                <Style VerticalAlign="Middle">                  <Font Bold="True" />                </Style>              </TextBox>            </Items>            <ConditionalFormatting>              <FormattingRule>                <Style Visible="False" />                <Filters>                  <Filter Expression="= Parameters.DefaultProviderBreakdown.Value" Operator="Equal" Value="= 0" />                </Filters>              </FormattingRule>            </ConditionalFormatting>          </GroupHeaderSection>        </GroupHeader>        <GroupFooter>          <GroupFooterSection Height="0.0520833333333333in" Name="groupFooterSection4">            <Style Visible="False" />          </GroupFooterSection>        </GroupFooter>        <Groupings>          <Grouping Expression="= Fields.DefaultProvider" />        </Groupings>      </Group>      <Group Name="group1">        <GroupHeader>          <GroupHeaderSection Height="0.200039386749268in" Name="groupHeaderSection1">            <Items>              <TextBox Width="1.99996058146159in" Height="0.199920972188314in" Left="0.199881553649902in" Top="0in" Value="{LastName} {Suffix}{FirstName} {MiddleName}" Name="textBox8">                <Style VerticalAlign="Middle" />              </TextBox>              <TextBox Width="0.700000127156575in" Height="0.199881553649902in" Left="2.19992097218831in" Top="0in" Value="{Id}" Name="textBox9">                <Style VerticalAlign="Middle" />              </TextBox>              <TextBox Width="1.10007921854655in" Height="0.199999968210856in" Left="2.89999993642172in" Top="0in" Value="{LastVisit}" Name="textBox10">                <Style VerticalAlign="Middle" />              </TextBox>              <TextBox Width="1.29976212978363in" Height="0.199763298034668in" Left="4.00015799204508in" Top="0in" Value="{MainPhoneNumber}{MainExtension}" Name="textBox14">                <Style TextAlign="Left" VerticalAlign="Middle" />              </TextBox>              <TextBox Width="1.20023858547211in" Height="0.19999997317791in" Left="5.29999923706055in" Top="0in" Value="{MainPhoneType}" Name="textBox13">                <Style TextAlign="Left" VerticalAlign="Middle" />              </TextBox>            </Items>          </GroupHeaderSection>        </GroupHeader>        <GroupFooter>          <GroupFooterSection Height="0.0520833333333334in" Name="groupFooterSection1">            <Style Visible="False" />          </GroupFooterSection>        </GroupFooter>        <Groupings>          <Grouping Expression="= Fields.Id" />        </Groupings>        <Sortings>          <Sorting Expression="= Fields.LastName" Direction="Asc" />        </Sortings>      </Group>      <Group Name="group2">        <GroupHeader>          <GroupHeaderSection Height="0.199881553649902in" Name="groupHeaderSection2">            <Items>              <TextBox Width="1.2997620900472in" Height="0.199763298034668in" Left="4.00047461191813in" Top="0in" Value="{SecondPhoneNumber}{SecondExtension}" Name="textBox11">                <Style TextAlign="Left" VerticalAlign="Middle" />              </TextBox>              <TextBox Width="1.19960530598959in" Height="0.199763298034668in" Left="5.30031585693359in" Top="0in" Value="{SecondPhoneType}" Name="textBox12">                <Style TextAlign="Left" VerticalAlign="Middle" />              </TextBox>            </Items>            <ConditionalFormatting>              <FormattingRule>                <Style Visible="False" />                <Filters>                  <Filter Expression="= Fields.SecondPhoneNumber" Operator="Equal" Value="0" />                </Filters>              </FormattingRule>            </ConditionalFormatting>          </GroupHeaderSection>        </GroupHeader>        <GroupFooter>          <GroupFooterSection Height="0.0520833333333328in" Name="groupFooterSection2">            <Style Visible="False" />          </GroupFooterSection>        </GroupFooter>        <Groupings>          <Grouping Expression="= Fields.SecondPhoneNumber" />        </Groupings>      </Group>    </Groups>    <ReportParameters>      <ReportParameter Name="StartDate" Type="DateTime" Text="Last Visit Start Date" Visible="True" />      <ReportParameter Name="EndDate" Type="DateTime" Text="Last Visit End Date" Visible="True" />      <ReportParameter Name="DefaultProviders" Text="Default Provider(s)" Visible="True" MultiValue="True" AllowNull="True">        <AvailableValues DataSourceName="DefaultProviders" DisplayMember="= Fields.UserName" ValueMember="= Fields.Id" />      </ReportParameter>      <ReportParameter Name="Diagnoses" Text="Diagnos(is/es)" Visible="True" MultiValue="True" AllowNull="True">        <AvailableValues DataSourceName="Diagnoses" DisplayMember="= Fields.Icd9DiagnosisCode" ValueMember="= Fields.Icd9DiagnosisCode" />      </ReportParameter>      <ReportParameter Name="DefaultProviderBreakdown" Type="Boolean" Text="Default Provider Breakdown" Visible="True">        <Value>          <String>= False</String>        </Value>      </ReportParameter>      <ReportParameter Name="ServiceLocation" Text="ServiceLocation(s)" Visible="True" MultiValue="True" AllowNull="True">        
<AvailableValues DataSourceName="ServiceLocation" DisplayMember="= Fields.Name" ValueMember="= Fields.id">      <Sortings>           
<Sorting Expression="=Fields.Name" Direction="Asc" />         
</Sortings>        
</AvailableValues>      
</ReportParameter>    
</ReportParameters>  </Report>'

Where Name='Patients Without Future Appointments Or Recalls'
GO




