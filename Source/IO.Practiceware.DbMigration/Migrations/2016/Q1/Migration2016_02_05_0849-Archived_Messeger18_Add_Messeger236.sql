

IF EXISTS( SELECT 1 FROM CMSReports WHERE ReportType = 'PQRI' AND cmsrepCode = 'Measure #018')
BEGIN
	UPDATE CMSReports SET Active_YN = 0 WHERE ReportType = 'PQRI' AND cmsrepCode = 'Measure #018'
END


IF NOT EXISTS( SELECT 1 FROM [dbo].[CMSReports] WHERE [CMSRepCode] = 'Measure #236')
BEGIN
	INSERT INTO [dbo].[CMSReports]
		(
			[CMSRepCode] ,[CMSRepName] ,[CMSQuery]
			,[CMSSubHeading],[IsSP],[Numerator]
			,[Denominator],[ReportType],[Active_YN]
		)
	VALUES
		(
			'Measure #236','236 Controlling High Blood Pressure','USP_PQRI_GetControllingHighBloodPressure'
			,'Controlling High Blood Pressure',1,'Patients whose most recent blood pressure is adequately controlled'
			,'All patients aged 18 years and older seen','PQRI',1
		)
END
IF NOT EXISTS( SELECT 1 FROM [dbo].[CMSReports] WHERE [CMSRepCode] = 'Measure #173')
BEGIN
	INSERT INTO [dbo].[CMSReports]
		(
			[CMSRepCode] ,[CMSRepName] ,[CMSQuery]
			,[CMSSubHeading],[IsSP],[Numerator]
			,[Denominator],[ReportType],[Active_YN]
		)
	VALUES
		(
			'Measure #173','173 Unhealthy Alcohol Use � Screening','USP_PQRI_GetUnhealthyAlcoholUseScreening'
			,'Unhealthy Alcohol Use � Screening',1,'Patients screened for unhealthy alcohol use at least once within 24 months using a systematic screening method'
			,'All patients aged 18 years and older seen','PQRI',1
		)
END
IF NOT EXISTS( SELECT 1 FROM [dbo].[CMSReports] WHERE [CMSRepCode] = 'Measure #402')
BEGIN
	INSERT INTO [dbo].[CMSReports]
		(
			[CMSRepCode] ,[CMSRepName] ,[CMSQuery]
			,[CMSSubHeading],[IsSP],[Numerator]
			,[Denominator],[ReportType],[Active_YN]
		)
	VALUES
		(
			'Measure #402','402 Tobacco Use and Help with Quitting Among Adolescents','USP_PQRI_GetTobaccoUseAndHelpWithQuittingAmongAdolescents'
			,'Tobacco Use and Help with Quitting Among Adolescents',1,'The percentage of adolescents 12 to 20 years of age with a primary care visit during the measurement year for whom tobacco use status was documented and received help with quitting if identified as a tobacco user'
			,'All patients aged 12 - 20 years seen','PQRI',1
		)
END
GO