

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = 'Medflow' ) 

BEGIN
EXEC sp_executesql N'CREATE SCHEMA Medflow'
END



 IF NOT EXISTS ( SELECT 1 FROM SYSOBJECTS WHERE XTYPE = 'U'  AND NAME='PortalActionTypeLookup') 
 BEGIN 
 CREATE TABLE [Medflow].[PortalActionTypeLookup] ([Id] [int] IDENTITY (1,1) NOT NULL ,  
  [ActionType] [varchar] (50) NOT NULL ,  
  [ActionReasonType] [varchar] (150) NOT NULL ,  
  [Stage1CoolDownTimeHours] [int] NOT NULL  DEFAULT ((0)),  
  [Stage2CoolDownTimeHours] [int] NOT NULL  DEFAULT ((0)),  
CONSTRAINT [PK_PortalActionTypeLookup]   PRIMARY KEY 
( 
   [Id]  
)   )  ON [PRIMARY] 



SET QUOTED_IDENTIFIER OFF 
SET IDENTITY_INSERT Medflow.PortalActionTypeLookup ON 
INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (1,'Message','Request Doctor Response',0,0)
 
INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (2,'Message','Deliver Doctor Response',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (3,'Message','Patient Request Amendment',0,0)
 
INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (4,'Activity','Patient Logged Into Portal',0,0)
 
INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (5,'Activity','Patient Viewed Health Information',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (6,'Activity','Patient Downloaded History Information',0,0)
 
INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (7,'Document','Clinical Summary',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (8,'Document','Patient Health Information',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (9,'Document','EducationDocument',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (10,'Document','Education Link',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (11,'Document','Summary of Care',0,0)
 
INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (12,'Document','Preventative Care',0,0)
 
INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (13,'Activity','Smoking Cessation',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (14,'Activity','Patient Registration',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (15,'Activity','Portal Representative Registration',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (16,'Message','Patient Appointments',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (17,'Message','Reschedule Appointment Request',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (18,'Message','Cancel Appointment Request',0,0)
 
INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (19,'Message','Appointment Response',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (20,'Messsage','Patient Ocular Medications',0,0)
 
INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (21,'Message','Refill Request',0,0)
 
INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (22,'Message','Refill Response',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (23,'Message','Patient Complete Demographics',0,0)
 
INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (24,'Message','Demographics Change Request',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (25,'Message','Update Demographics Without Image',0,0)

INSERT INTO Medflow.PortalActionTypeLookup([Id],[ActionType],[ActionReasonType],[Stage1CoolDownTimeHours],[Stage2CoolDownTimeHours]) VALUES (26,'Message','Appointment Request',0,0)

 SET IDENTITY_INSERT Medflow.PortalActionTypeLookup OFF 

 SET QUOTED_IDENTIFIER ON 


 END 

 --------------------------------------------------Table Creation-PortalQueueXML-------------------------------------------------------------------------------------------

if EXISTS (select 1 From sys.objects where name = 'PortalQueueXML')
BEGIN 

EXEC('ALTER TABLE [Medflow].[PortalQueueXML] DROP CONSTRAINT [PortalQueueXML_ActionTypeLookup_FK]')

EXEC('DROP TABLE [Medflow].[PortalQueueXML]')

END


CREATE TABLE [Medflow].[PortalQueueXML](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MessageData] [nvarchar](max) NULL,
	[ActionTypeLookupId] [int] NULL,
	[ProcessedDate] [datetime] NULL,
	[PatientId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ExternalId] [int] NULL,
 CONSTRAINT [PK_PortalQueueXML] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [Medflow].[PortalQueueXML] ADD  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [Medflow].[PortalQueueXML]  WITH CHECK ADD  CONSTRAINT [PortalQueueXML_ActionTypeLookup_FK] FOREIGN KEY([ActionTypeLookupId])
REFERENCES [Medflow].[PortalActionTypeLookup] ([Id])
GO

ALTER TABLE [Medflow].[PortalQueueXML] CHECK CONSTRAINT [PortalQueueXML_ActionTypeLookup_FK]
GO


--------------------------------------------------------Table Creation - PortalQueueXMLException-------------------------------------------------------------------

if EXISTS (select 1 From sys.objects where name = 'PortalQueueXMLException')
BEGIN
EXEC('ALTER TABLE [Medflow].[PortalQueueXMLException] DROP CONSTRAINT [PortalQueueXMLException_ActionTypeLookup_FK]')

EXEC('DROP TABLE [Medflow].[PortalQueueXMLException]')

END


CREATE TABLE [Medflow].[PortalQueueXMLException](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MessageData] [nvarchar](max) NULL,
	[ActionTypeLookupId] [int] NULL,
	[ProcessedDate] [datetime] NULL,
	[PatientId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ExternalId] [int] NULL,
	[Exception] [text] NULL,
 CONSTRAINT [PK_PortalQueueXMLException] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [Medflow].[PortalQueueXMLException] ADD  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [Medflow].[PortalQueueXMLException]  WITH CHECK ADD  CONSTRAINT [PortalQueueXMLException_ActionTypeLookup_FK] FOREIGN KEY([ActionTypeLookupId])
REFERENCES [Medflow].[PortalActionTypeLookup] ([Id])
GO

ALTER TABLE [Medflow].[PortalQueueXMLException] CHECK CONSTRAINT [PortalQueueXMLException_ActionTypeLookup_FK]
GO

-------------------------------Table Creation - PortalQueueXMLResponse -----------------------------------------------------------------------------------------

if EXISTS (select 1 From sys.objects where name = 'PortalQueueXMLResponse')
BEGIN
EXEC('ALTER TABLE [Medflow].[PortalQueueXMLResponse] DROP CONSTRAINT [PortalQueueXMLResponse_ActionTypeLookup_FK]')

/****** Object:  Table [Medflow].[PortalQueueXMLResponse]    Script Date: 4/27/2016 8:58:36 PM ******/
DROP TABLE [Medflow].[PortalQueueXMLResponse]
END

CREATE TABLE [Medflow].[PortalQueueXMLResponse](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MessageData] [nvarchar](max) NULL,
	[ActionTypeLookupId] [int] NULL,
	[ProcessedDate] [datetime] NULL,
	[PatientId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[OriginalId] [int] NULL,
 CONSTRAINT [PK_PortalQueueXMLResponse] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [Medflow].[PortalQueueXMLResponse] ADD  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [Medflow].[PortalQueueXMLResponse]  WITH CHECK ADD  CONSTRAINT [PortalQueueXMLResponse_ActionTypeLookup_FK] FOREIGN KEY([ActionTypeLookupId])
REFERENCES [Medflow].[PortalActionTypeLookup] ([Id])
GO

ALTER TABLE [Medflow].[PortalQueueXMLResponse] CHECK CONSTRAINT [PortalQueueXMLResponse_ActionTypeLookup_FK]
GO

