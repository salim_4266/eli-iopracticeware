IF EXISTS (SELECT *  FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'model.FK_BillingDiagnosisBillingServiceBillingDiagnosis') 
 AND parent_object_id = OBJECT_ID(N'[model].[BillingServiceBillingDiagnosis]'))
	ALTER TABLE [model].[BillingServiceBillingDiagnosis] DROP CONSTRAINT [FK_BillingDiagnosisBillingServiceBillingDiagnosis]
GO