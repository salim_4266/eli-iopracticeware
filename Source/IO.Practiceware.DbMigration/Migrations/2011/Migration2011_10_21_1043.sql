﻿--Trigger on practice name to prevent duplicate location references
IF OBJECT_ID('PracticeNameLocationReferenceTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeNameLocationReferenceTrigger')
END

GO

CREATE TRIGGER PracticeNameLocationReferenceTrigger
ON PracticeName
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (SELECT COUNT(*) 
	FROM PracticeName pn WITH(NOLOCK) 
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON pn.LocationReference = i.LocationReference
	WHERE i.PracticeType = 'P' AND pn.PracticeType = 'P'
	GROUP BY pn.LocationReference 
	HAVING COUNT(*) > 1) <> 0
	AND dbo.GetNoCheck() = 0
	
BEGIN
	RAISERROR ('Duplicate PracticeName.LocationReference.',16,1)
	ROLLBACK TRANSACTION
END

GO



--Prevents addition of location reference to main office when appointemnts are attached to the main office
IF OBJECT_ID('PracticeNameBlankLocationReferenceTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeNameBlankLocationReferenceTrigger')
END

GO

CREATE TRIGGER PracticeNameBlankLocationReferenceTrigger
ON PracticeName
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF EXISTS (
		SELECT i.PracticeId
		FROM (SELECT * FROM inserted  EXCEPT SELECT * FROM deleted) i
		INNER JOIN Appointments ap WITH(NOLOCK) ON ap.ResourceId2 = 0
		INNER JOIN PracticeName pn WITH(NOLOCK) ON i.PracticeId = pn.PracticeId
		WHERE pn.LocationReference = ''
			AND i.LocationReference <> ''
		)
	AND dbo.GetNoCheck() = 0
	   
BEGIN
	RAISERROR ('Cannot add a location reference to the main office if appointments have been scheduled to the main office. (ap.ResourceId2 = 0)',16,1)
	ROLLBACK TRANSACTION
END

GO

-- Prevents insertion of invalid patient Demo Alt rows
IF OBJECT_ID('PatientDemoAltPatientIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientDemoAltPatientIdTrigger')
END

GO

CREATE TRIGGER PatientDemoAltPatientIdTrigger
ON PatientDemoAlt
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (((SELECT COUNT(*)
	FROM (SELECT * FROM inserted  EXCEPT SELECT * FROM deleted) i
	LEFT JOIN PatientDemographics pd WITH(NOLOCK) ON i.PatientId = pd.PatientId
	WHERE  pd.PatientId IS NULL) <> 0) 
	AND dbo.GetNoCheck() = 0)
	   
BEGIN
	RAISERROR ('Cannot insert into PatientDemoAlt without a valid PatientId',16,1)
	ROLLBACK TRANSACTION
END

GO


--Duplicate PracticeServices
IF OBJECT_ID('PracticeServicesDuplicateServiceTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeServicesDuplicateServiceTrigger')
END

GO

CREATE TRIGGER PracticeServicesDuplicateServiceTrigger
ON PracticeServices
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (((SELECT COUNT(*) 
	FROM PracticeServices ps WITH(NOLOCK) 
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON ps.Code = i.Code
	GROUP BY ps.Code 
	HAVING COUNT(*) > 1
	) <> 0) 
	AND dbo.GetNoCheck() = 0)
	   
BEGIN
	RAISERROR ('Cannot enter duplicate codes in PracticeServices.',16,1)
	ROLLBACK TRANSACTION
END

GO


--Prevent entry of duplicate payment types
IF OBJECT_ID('PracticeCodeTablePaymentTypeTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeCodeTablePaymentTypeTrigger')
END

GO

CREATE TRIGGER PracticeCodeTablePaymentTypeTrigger
ON PracticeCodeTable
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (((SELECT COUNT(*) 
	FROM PracticeCodeTable pc WITH(NOLOCK) 
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i  ON SUBSTRING(i.Code, LEN(i.Code), 1) = SUBSTRING(pc.Code, LEN(pc.Code), 1) AND pc.ReferenceType = 'PAYMENTTYPE' 
	WHERE i.ReferenceType = 'PAYMENTTYPE' 
	GROUP BY i.Code HAVING COUNT(*) > 1) > 1) 
	AND dbo.GetNoCheck() = 0)
	
BEGIN
	RAISERROR ('Cannot insert duplicate payment types.',16,1)
	ROLLBACK TRANSACTION
END

GO


--Trigger on practicecode table to prevent entry of duplicate employee statuses
IF OBJECT_ID('PracticeCodeTableEmployeeStatusTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeCodeTableEmployeeStatusTrigger')
END

GO

CREATE TRIGGER PracticeCodeTableEmployeeStatusTrigger
ON practiceCodeTable
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (((SELECT COUNT(*) 
	FROM PracticeCodeTable pc WITH(NOLOCK) 
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i  ON SUBSTRING(pc.Code, 1, 1) = SUBSTRING(i.Code, 1, 1)
	WHERE i.ReferenceType = 'EMPLOYEESTATUS'
	AND pc.ReferenceType = 'EMPLOYEESTATUS' 
	GROUP BY i.Code HAVING COUNT(*) > 1) > 1) 
	AND dbo.GetNoCheck() = 0)
	
BEGIN
	RAISERROR ('Cannot insert duplicate employee statuses.',16,1)
	ROLLBACK TRANSACTION
END

GO

--Trigger on practicecode table to prevent entry of duplicate languages
IF OBJECT_ID('PracticeCodeTableLanguageTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeCodeTableLanguageTrigger')
END

GO

CREATE TRIGGER PracticeCodeTableLanguageTrigger
ON practiceCodeTable
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (((SELECT COUNT(*) 
	FROM PracticeCodeTable pc WITH(NOLOCK) 
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i  ON pc.Code = i.Code
	WHERE i.ReferenceType = 'LANGUAGE'
	AND pc.ReferenceType = 'LANGUAGE'
	GROUP BY i.Code HAVING COUNT(*) > 1) > 1) 
	AND dbo.GetNoCheck() = 0)
	
BEGIN
	RAISERROR ('Cannot insert duplicate languages.',16,1)
	ROLLBACK TRANSACTION
END

GO


--On PracticeCode table to prevent deletion of trasmission types in use
IF OBJECT_ID('PreventTransmissionTypeDeletion') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PreventTransmissionTypeDeletion')
END

GO

CREATE TRIGGER PreventTransmissionTypeDeletion
ON PracticeCodeTable
FOR DELETE
NOT FOR REPLICATION
AS 
	SET NOCOUNT ON;
	IF EXISTS (
		SELECT Id
		FROM deleted d
		INNER JOIN PracticeTransactionJournal ptj WITH(NOLOCK) ON SUBSTRING(ptj.TransactionBatch, 25, 1) = SUBSTRING(d.Code, 1, 1)
		WHERE ptj.TransactionBatch LIKE '_:\Pinpoint\Submissions\%' 
			AND ptj.TransactionBatch NOT LIKE '_:\Pinpoint\Submissions\Hcfa-00000-00000.txt'
			AND d.ReferenceType = 'TRANSMITTYPE'
			)
			
	BEGIN
	RAISERROR('Cannot delete Transmit Type, it has an associated submission.', 16, 1)
	ROLLBACK
	END
	
GO

--Prevent entry of duplicate transmission types
IF OBJECT_ID('PracticeCodeTableTransmissionTypeTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeCodeTableTransmissionTypeTrigger')
END

GO

CREATE TRIGGER PracticeCodeTableTransmissionTypeTrigger
ON PracticeCodeTable
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
	SET NOCOUNT ON
	IF (((SELECT COUNT(*) 
		FROM PracticeCodeTable pc WITH(NOLOCK) 
		INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i  ON SUBSTRING(i.Code, LEN(i.Code), 1) = SUBSTRING(pc.Code, LEN(pc.Code), 1) AND pc.ReferenceType = 'PAYMENTTYPE' 
		WHERE i.ReferenceType = 'TRANSMITTYPE' ) > 1) 
		AND dbo.GetNoCheck() = 0)
	
	BEGIN
		RAISERROR ('Cannot insert duplicate transmission types.',16,1)
		ROLLBACK TRANSACTION
	END

GO

--On PracticeCode table to prevent deletion of payment types in use
IF OBJECT_ID('PreventPaymentTypeDeletion') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PreventPaymentTypeDeletion')
END

GO

CREATE TRIGGER PreventPaymentTypeDeletion
ON PracticeCodeTable
FOR DELETE
NOT FOR REPLICATION
AS 
	SET NOCOUNT ON;
	IF EXISTS (
		SELECT d.Id
		FROM deleted d  
		INNER JOIN PatientReceivablePayments prp WITH(NOLOCK) 
			ON SUBSTRING(Code, LEN(Code), 1) = Prp.PaymentType
		WHERE ReferenceType ='PaymentType'
			)	
	BEGIN
		RAISERROR ('Cannot delete payment types that are in use.',16,1)
		ROLLBACK TRANSACTION
	END
GO

--Prevent deletion of resourcetypes in use or values that the software depends upon
IF OBJECT_ID('ResourceTypeDeletion') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER ResourceTypeDeletion')
END

GO 

CREATE TRIGGER ResourceTypeDeletion
ON dbo.PracticeCodeTable
FOR DELETE
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (SELECT COUNT(*)
	FROM deleted d
	INNER JOIN dbo.Resources re WITH(NOLOCK) ON re.ResourceType = SUBSTRING(d.Code,1,1)
	WHERE d.Referencetype = 'ResourceType' AND SUBSTRING(d.Code,1,1) IN ('D', 'Q', 'U', 'R', 'E'))
	<> 0
	
BEGIN
	RAISERROR ('Cannot delete ResourceTypes that are in use.', 16,1)
	ROLLBACK TRANSACTION
END
GO


--Practice Calendar
--Valid ResourceId
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PracticeCalendar_Resources]') AND parent_object_id = OBJECT_ID(N'[dbo].[PracticeCalendar]'))
ALTER TABLE [dbo].[PracticeCalendar] DROP CONSTRAINT [FK_PracticeCalendar_Resources]
GO

ALTER TABLE [dbo].[PracticeCalendar]  WITH CHECK ADD  CONSTRAINT [FK_PracticeCalendar_Resources] FOREIGN KEY ([CalendarResourceId])
REFERENCES [dbo].[Resources] ([ResourceId])
GO

ALTER TABLE [dbo].[PracticeCalendar] CHECK CONSTRAINT [FK_PracticeCalendar_Resources]
GO

