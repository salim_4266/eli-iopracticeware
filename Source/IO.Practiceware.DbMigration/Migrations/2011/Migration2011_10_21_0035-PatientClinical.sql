﻿---PatientClinical

--Remove duplicate ClinicalType B and K
DELETE
FROM PatientClinical
WHERE ClinicalId IN (
		SELECT pcdelete.ClinicalId
		FROM PatientClinical pc
		INNER JOIN PatientClinical pcDelete ON pc.AppointmentId = pcdelete.appointmentid
			AND pc.PatientId = pcDelete.PatientId
			AND pc.EyeContext = pcdelete.EyeContext
			AND pc.Symptom = pcdelete.symptom
			AND pc.FindingDetail = pcdelete.FindingDetail
			AND pc.STATUS = pcdelete.STATUS
			AND pc.clinicaltype IN (
				'B'
				,'K'
				)
			AND pcDelete.ClinicalType IN (
				'B'
				,'K'
				)
			AND pc.ClinicalId < pcDelete.ClinicalId
		)