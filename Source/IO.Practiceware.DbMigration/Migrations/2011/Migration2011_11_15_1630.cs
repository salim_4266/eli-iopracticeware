﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Adds tables PatientRelationship, PatientRelationshipTypes, PaymentRequestForms
    /// </summary>
    [Migration(201111141630)]
    public class Migration201111141630 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"

-- Creating table 'PatientRelationships'
CREATE TABLE [model].[PatientRelationships] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FromContactId] int  NOT NULL,
    [ToContactId] int  NOT NULL,
    [PatientRelationshipTypeId] int  NOT NULL
);
GO

-- Creating table 'PatientRelationshipTypes'
CREATE TABLE [model].[PatientRelationshipTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PaymentRequestForms'
CREATE TABLE [model].[PaymentRequestForms] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'PatientRelationships'
ALTER TABLE [model].[PatientRelationships]
ADD CONSTRAINT [PK_PatientRelationships]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientRelationshipTypes'
ALTER TABLE [model].[PatientRelationshipTypes]
ADD CONSTRAINT [PK_PatientRelationshipTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PaymentRequestForms'
ALTER TABLE [model].[PaymentRequestForms]
ADD CONSTRAINT [PK_PaymentRequestForms]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ContactPatientRelationship'
CREATE INDEX [IX_FK_ContactPatientRelationship]
ON [model].[PatientRelationships]
    ([FromContactId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ContactPatientRelationshipSecondary'
CREATE INDEX [IX_FK_ContactPatientRelationshipSecondary]
ON [model].[PatientRelationships]
    ([ToContactId]);
GO

-- Creating foreign key on [PatientRelationshipTypeId] in table 'PatientRelationships'
ALTER TABLE [model].[PatientRelationships]
ADD CONSTRAINT [FK_PatientRelationshipTypePatientRelationship]
    FOREIGN KEY ([PatientRelationshipTypeId])
    REFERENCES [model].[PatientRelationshipTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientRelationshipTypePatientRelationship'
CREATE INDEX [IX_FK_PatientRelationshipTypePatientRelationship]
ON [model].[PatientRelationships]
    ([PatientRelationshipTypeId]);
GO

");

            Execute.Sql(@"
SET IDENTITY_INSERT [model].[PatientRelationshipTypes] ON

INSERT [model].[PatientRelationshipTypes] (Id, Name) VALUES (1, N'HIPAA Disclosure')
INSERT [model].[PatientRelationshipTypes] (Id, Name) VALUES (2, N'Responsible Party')
INSERT [model].[PatientRelationshipTypes] (Id, Name) VALUES (3, N'Recall')
INSERT [model].[PatientRelationshipTypes] (Id, Name) VALUES (4, N'Claims')
INSERT [model].[PatientRelationshipTypes] (Id, Name) VALUES (5, N'Patient Statement')
INSERT [model].[PatientRelationshipTypes] (Id, Name) VALUES (6, N'Policyholder')

SET IDENTITY_INSERT [model].[PatientRelationshipTypes] OFF

DBCC CHECKIDENT ('[model].[PatientRelationshipTypes]', 'RESEED', 1000)

INSERT [model].[PatientRelationshipTypes] (Name) VALUES (N'Appointment Confirmation')
INSERT [model].[PatientRelationshipTypes] (Name) VALUES (N'Clinical Followup')
INSERT [model].[PatientRelationshipTypes] (Name) VALUES (N'Emergency Contact')
INSERT [model].[PatientRelationshipTypes] (Name) VALUES (N'Employer')
INSERT [model].[PatientRelationshipTypes] (Name) VALUES (N'Family Member')
INSERT [model].[PatientRelationshipTypes] (Name) VALUES (N'General Communication')
INSERT [model].[PatientRelationshipTypes] (Name) VALUES (N'Letter Copies')
INSERT [model].[PatientRelationshipTypes] (Name) VALUES (N'Medical Decisions')
");
        }

        public override void Down()
        {
        }
    }
}