﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Adds External Systems tables.
    /// </summary>
    [Migration(201110220050)]
    public class Migration201110220050 : Migration
    {
        public override void Up()
        {
            Execute.Sql(
@"
-- Creating table 'ExternalSystemMessageTypes'
CREATE TABLE [model].[ExternalSystemMessageTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(250)  NOT NULL,
    [Description] nvarchar(1000)  NOT NULL,
    [IsOutbound] bit NOT NULL DEFAULT 0,
);
GO

-- Creating table 'ExternalSystems'
CREATE TABLE [model].[ExternalSystems] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(255)  NOT NULL
);
GO

-- Creating table 'ExternalSystemEntities'
CREATE TABLE [model].[ExternalSystemEntities] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ExternalSystemId] int  NOT NULL,
    [Name] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'ExternalSystemEntityMappings'
CREATE TABLE [model].[ExternalSystemEntityMappings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PracticeRepositoryEntityId] int  NOT NULL,
    [PracticeRepositoryEntityKey] nvarchar(100)  NOT NULL,
    [ExternalSystemEntityId] int  NOT NULL,
    [ExternalSystemEntityKey] nvarchar(100)  NULL
);
GO

-- Creating table 'ExternalSystemExternalSystemMessageTypes'
CREATE TABLE [model].[ExternalSystemExternalSystemMessageTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ExternalSystemId] int  NOT NULL,
    [ExternalSystemMessageTypeId] int  NOT NULL,
    [TemplateFile] nvarchar(max) NULL
);
GO

-- Creating table 'ExternalSystemMessages'
CREATE TABLE [model].[ExternalSystemMessages] (
    [Id] uniqueidentifier  NOT NULL DEFAULT (NEWID()),
    [Description] varchar(max)  NOT NULL,
    [Value] varchar(max)  NOT NULL,
    [ExternalSystemExternalSystemMessageTypeId] int  NOT NULL,
    [ExternalSystemMessageProcessingStateId] int  NOT NULL,
    [ProcessingAttemptCount] int  NOT NULL,
    [CreatedBy] nvarchar(255)  NOT NULL,
    [CreatedDateTime] datetime  NOT NULL,
    [UpdatedDateTime] datetime  NOT NULL,
    [CorrelationId] nvarchar(200)  NULL
);
GO

-- Creating table 'ExternalSystemMessagePracticeRepositoryEntities'
CREATE TABLE [model].[ExternalSystemMessagePracticeRepositoryEntities] (
    [Id] uniqueidentifier  NOT NULL DEFAULT (NEWID()),
    [ExternalSystemMessageId] uniqueidentifier  NOT NULL,
    [PracticeRepositoryEntityId] int  NOT NULL,
    [PracticeRepositoryEntityKey] nvarchar(100)  NOT NULL
);
GO

-- Creating table 'ExternalSystemMessageProcessingStates'
CREATE TABLE [model].[ExternalSystemMessageProcessingStates] (
    [Id] int NOT NULL,
    [Name] nvarchar(250)  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'ExternalSystemMessageTypes'
ALTER TABLE [model].[ExternalSystemMessageTypes]
ADD CONSTRAINT [PK_ExternalSystemMessageTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalSystems'
ALTER TABLE [model].[ExternalSystems]
ADD CONSTRAINT [PK_ExternalSystems]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalSystemEntities'
ALTER TABLE [model].[ExternalSystemEntities]
ADD CONSTRAINT [PK_ExternalSystemEntities]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalSystemEntityMappings'
ALTER TABLE [model].[ExternalSystemEntityMappings]
ADD CONSTRAINT [PK_ExternalSystemEntityMappings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalSystemExternalSystemMessageTypes'
ALTER TABLE [model].[ExternalSystemExternalSystemMessageTypes]
ADD CONSTRAINT [PK_ExternalSystemExternalSystemMessageTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalSystemMessages'
ALTER TABLE [model].[ExternalSystemMessages]
ADD CONSTRAINT [PK_ExternalSystemMessages]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalSystemMessagePracticeRepositoryEntities'
ALTER TABLE [model].[ExternalSystemMessagePracticeRepositoryEntities]
ADD CONSTRAINT [PK_ExternalSystemMessagePracticeRepositoryEntities]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExternalSystemMessageProcessingStates'
ALTER TABLE [model].[ExternalSystemMessageProcessingStates]
ADD CONSTRAINT [PK_ExternalSystemMessageProcessingStates]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [ExternalSystemId] in table 'ExternalSystemEntities'
ALTER TABLE [model].[ExternalSystemEntities]
ADD CONSTRAINT [FK_ExternalSystemEntity_ExternalSystem]
    FOREIGN KEY ([ExternalSystemId])
    REFERENCES [model].[ExternalSystems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalSystemEntity_ExternalSystem'
CREATE INDEX [IX_FK_ExternalSystemEntity_ExternalSystem]
ON [model].[ExternalSystemEntities]
    ([ExternalSystemId]);
GO

-- Creating foreign key on [ExternalSystemId] in table 'ExternalSystemExternalSystemMessageTypes'
ALTER TABLE [model].[ExternalSystemExternalSystemMessageTypes]
ADD CONSTRAINT [FK_ExternalSystemExternalSystemMessageType_ExternalSystem]
    FOREIGN KEY ([ExternalSystemId])
    REFERENCES [model].[ExternalSystems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalSystemExternalSystemMessageType_ExternalSystem'
CREATE INDEX [IX_FK_ExternalSystemExternalSystemMessageType_ExternalSystem]
ON [model].[ExternalSystemExternalSystemMessageTypes]
    ([ExternalSystemId]);
GO

-- Creating foreign key on [ExternalSystemEntityId] in table 'ExternalSystemEntityMappings'
ALTER TABLE [model].[ExternalSystemEntityMappings]
ADD CONSTRAINT [FK_ExternalSystemEntityMapping_ExternalSystemEntity]
    FOREIGN KEY ([ExternalSystemEntityId])
    REFERENCES [model].[ExternalSystemEntities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalSystemEntityMapping_ExternalSystemEntity'
CREATE INDEX [IX_FK_ExternalSystemEntityMapping_ExternalSystemEntity]
ON [model].[ExternalSystemEntityMappings]
    ([ExternalSystemEntityId]);
GO

-- Creating foreign key on [PracticeRepositoryEntityId] in table 'ExternalSystemEntityMappings'
ALTER TABLE [model].[ExternalSystemEntityMappings]
ADD CONSTRAINT [FK_ExternalSystemEntityMapping_PracticeRepositoryEntity]
    FOREIGN KEY ([PracticeRepositoryEntityId])
    REFERENCES [model].[PracticeRepositoryEntities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalSystemEntityMapping_PracticeRepositoryEntity'
CREATE INDEX [IX_FK_ExternalSystemEntityMapping_PracticeRepositoryEntity]
ON [model].[ExternalSystemEntityMappings]
    ([PracticeRepositoryEntityId]);
GO

-- Creating foreign key on [ExternalSystemMessageTypeId] in table 'ExternalSystemExternalSystemMessageTypes'
ALTER TABLE [model].[ExternalSystemExternalSystemMessageTypes]
ADD CONSTRAINT [FK_ExternalSystemExternalSystemMessageType_ExternalSystemMessageType]
    FOREIGN KEY ([ExternalSystemMessageTypeId])
    REFERENCES [model].[ExternalSystemMessageTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalSystemExternalSystemMessageType_ExternalSystemMessageType'
CREATE INDEX [IX_FK_ExternalSystemExternalSystemMessageType_ExternalSystemMessageType]
ON [model].[ExternalSystemExternalSystemMessageTypes]
    ([ExternalSystemMessageTypeId]);
GO

-- Creating foreign key on [ExternalSystemExternalSystemMessageTypeId] in table 'ExternalSystemMessages'
ALTER TABLE [model].[ExternalSystemMessages]
ADD CONSTRAINT [FK_ExternalSystemMessage_ExternalSystemExternalSystemMessageType]
    FOREIGN KEY ([ExternalSystemExternalSystemMessageTypeId])
    REFERENCES [model].[ExternalSystemExternalSystemMessageTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalSystemMessage_ExternalSystemExternalSystemMessageType'
CREATE INDEX [IX_FK_ExternalSystemMessage_ExternalSystemExternalSystemMessageType]
ON [model].[ExternalSystemMessages]
    ([ExternalSystemExternalSystemMessageTypeId]);
GO

-- Creating foreign key on [ExternalSystemMessageProcessingStateId] in table 'ExternalSystemMessages'
ALTER TABLE [model].[ExternalSystemMessages]
ADD CONSTRAINT [FK_ExternalSystemMessage_ExternalSystemMessageProcessingState]
    FOREIGN KEY ([ExternalSystemMessageProcessingStateId])
    REFERENCES [model].[ExternalSystemMessageProcessingStates]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalSystemMessage_ExternalSystemMessageProcessingState'
CREATE INDEX [IX_FK_ExternalSystemMessage_ExternalSystemMessageProcessingState]
ON [model].[ExternalSystemMessages]
    ([ExternalSystemMessageProcessingStateId]);
GO

-- Creating foreign key on [ExternalSystemMessageId] in table 'ExternalSystemMessagePracticeRepositoryEntities'
ALTER TABLE [model].[ExternalSystemMessagePracticeRepositoryEntities]
ADD CONSTRAINT [FK_ExternalSystemMessagePracticeRepositoryEntity_ExternalSystemMessage]
    FOREIGN KEY ([ExternalSystemMessageId])
    REFERENCES [model].[ExternalSystemMessages]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalSystemMessagePracticeRepositoryEntity_ExternalSystemMessage'
CREATE INDEX [IX_FK_ExternalSystemMessagePracticeRepositoryEntity_ExternalSystemMessage]
ON [model].[ExternalSystemMessagePracticeRepositoryEntities]
    ([ExternalSystemMessageId]);
GO

-- Creating foreign key on [PracticeRepositoryEntityId] in table 'ExternalSystemMessagePracticeRepositoryEntities'
ALTER TABLE [model].[ExternalSystemMessagePracticeRepositoryEntities]
ADD CONSTRAINT [FK_ExternalSystemMessagePracticeRepositoryEntity_PracticeRepositoryEntity]
    FOREIGN KEY ([PracticeRepositoryEntityId])
    REFERENCES [model].[PracticeRepositoryEntities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalSystemMessagePracticeRepositoryEntity_PracticeRepositoryEntity'
CREATE INDEX [IX_FK_ExternalSystemMessagePracticeRepositoryEntity_PracticeRepositoryEntity]
ON [model].[ExternalSystemMessagePracticeRepositoryEntities]
    ([PracticeRepositoryEntityId]);
GO

SET IDENTITY_INSERT[model].[ExternalSystems] ON

INSERT INTO model.ExternalSystems(Id,Name)
VALUES (1, 'IO Practiceware')
INSERT INTO model.ExternalSystems(Id,Name)
VALUES (2, 'Email')
INSERT INTO model.ExternalSystems(Id,Name)
VALUES (3, 'TextMessage')
SET IDENTITY_INSERT[model].[ExternalSystems] OFF


INSERT INTO model.ExternalSystemMessageProcessingStates(Id, Name)
VALUES (1, 'Unprocessed')
INSERT INTO model.ExternalSystemMessageProcessingStates(Id, Name)
VALUES (2, 'Processing')
INSERT INTO model.ExternalSystemMessageProcessingStates(Id, Name)
VALUES (3, 'Processed')

SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] ON

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description])
VALUES (1, 'ACK_V23', '')

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description])
VALUES (2, 'ADT_A04_V23', '')

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description])
VALUES (3, 'ADT_A08_V23', '')

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description])
VALUES (4, 'ADT_A28_V23', '')

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description])
VALUES (5, 'ADT_A31_V23', '')

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description])
VALUES (6, 'SIU_S12_V23', '')

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description])
VALUES (7, 'SIU_S13_V23', '')

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description])
VALUES (8, 'SIU_S14_V23', '')

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description])
VALUES (9, 'SIU_S15_V23', '')

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description])
VALUES(10, 'SIU_S17_V23', '')

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description])
VALUES (11, 'SIU_S26_V23', '')

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description])
VALUES (13, 'MFN_M02_V23', '')

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description], IsOutbound)
VALUES (14, 'DFT_P03_V231', '', 1)

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description], IsOutbound)
VALUES (15, 'ORM_O01_V231', '', 1)

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description], IsOutbound)
VALUES (16, 'ORM_O01_V231', '', 0)

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description], IsOutbound)
VALUES (17, 'X12_837', '', 1)

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description], IsOutbound)
VALUES (18, 'ADT_A28_V23', '', 1)

INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description], IsOutbound)
VALUES (19, 'Confirmation', '', 1)

SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] OFF
");
        }

        public override void Down()
        {
        }
    }
}