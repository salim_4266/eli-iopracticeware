﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Country/StateProvince
    /// </summary>
    [Migration(201111082230)]
    public class Migration201111082230 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"

-- Creating table 'StateOrProvinces'
CREATE TABLE [model].[StateOrProvinces] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Abbreviation] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [CountryId] int  NOT NULL
);
GO

-- Creating table 'Countries'
CREATE TABLE [model].[Countries] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Abbreviation2Letters] nvarchar(max)  NOT NULL,
    [Abbreviation3Letters] nvarchar(max)  NOT NULL,
    [NumberCode] int  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'StateOrProvinces'
ALTER TABLE [model].[StateOrProvinces]
ADD CONSTRAINT [PK_StateOrProvinces]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Countries'
ALTER TABLE [model].[Countries]
ADD CONSTRAINT [PK_Countries]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CountryStateProvince'
CREATE INDEX [IX_FK_CountryStateProvince]
ON [model].[StateOrProvinces]
    ([CountryId]);
GO

-- Creating foreign key on [CountryId] in table 'StateOrProvinces'
ALTER TABLE [model].[StateOrProvinces]
ADD CONSTRAINT [FK_CountryStateProvince]
    FOREIGN KEY ([CountryId])
    REFERENCES [model].[Countries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

");

            Execute.Sql(@"

--Countries
SET IDENTITY_INSERT [model].[Countries] ON

INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (1, N'AFGHANISTAN', N'AF', N'AFG', 4)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (2, N'ALBANIA', N'AL', N'ALB', 8)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (3, N'ALGERIA', N'DZ', N'DZA', 12)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (4, N'AMERICAN SAMOA', N'AS', N'ASM', 16)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (5, N'ANDORRA', N'AD', N'AND', 20)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (6, N'ANGOLA', N'AO', N'AGO', 24)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (7, N'ANGUILLA', N'AI', N'AIA', 660)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (8, N'ANTARCTICA', N'AQ', N'ATA', 10)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (9, N'ANTIGUA AND BARBUDA', N'AG', N'ATG', 28)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (10, N'ARGENTINA', N'AR', N'ARG', 32)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (11, N'ARMENIA', N'AM', N'ARM', 51)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (12, N'ARUBA', N'AW', N'ABW', 533)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (13, N'AUSTRALIA', N'AU', N'AUS', 36)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (14, N'AUSTRIA', N'AT', N'AUT', 40)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (15, N'AZERBAIJAN', N'AZ', N'AZE', 31)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (16, N'BAHAMAS', N'BS', N'BHS', 44)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (17, N'BAHRAIN', N'BH', N'BHR', 48)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (18, N'BANGLADESH', N'BD', N'BGD', 50)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (19, N'BARBADOS', N'BB', N'BRB', 52)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (20, N'BELARUS', N'BY', N'BLR', 112)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (21, N'BELGIUM', N'BE', N'BEL', 56)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (22, N'BELIZE', N'BZ', N'BLZ', 84)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (23, N'BENIN', N'BJ', N'BEN', 204)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (24, N'BERMUDA', N'BM', N'BMU', 60)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (25, N'BHUTAN', N'BT', N'BTN', 64)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (26, N'BOLIVIA', N'BO', N'BOL', 68)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (27, N'BOSNIA AND HERZEGOWINA', N'BA', N'BIH', 70)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (28, N'BOTSWANA', N'BW', N'BWA', 72)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (29, N'BOUVET ISLAND', N'BV', N'BVT', 74)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (30, N'BRAZIL', N'BR', N'BRA', 76)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (31, N'BRITISH INDIAN OCEAN TERRITORY', N'IO', N'IOT', 86)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (32, N'BRUNEI DARUSSALAM', N'BN', N'BRN', 96)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (33, N'BULGARIA', N'BG', N'BGR', 100)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (34, N'BURKINA FASO', N'BF', N'BFA', 854)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (35, N'BURUNDI', N'BI', N'BDI', 108)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (36, N'CAMBODIA', N'KH', N'KHM', 116)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (37, N'CAMEROON', N'CM', N'CMR', 120)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (38, N'CANADA', N'CA', N'CAN', 124)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (39, N'CAPE VERDE', N'CV', N'CPV', 132)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (40, N'CAYMAN ISLANDS', N'KY', N'CYM', 136)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (41, N'CENTRAL AFRICAN REPUBLIC', N'CF', N'CAF', 140)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (42, N'CHAD', N'TD', N'TCD', 148)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (43, N'CHILE', N'CL', N'CHL', 152)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (44, N'CHINA', N'CN', N'CHN', 156)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (45, N'CHRISTMAS ISLAND', N'CX', N'CXR', 162)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (46, N'COCOS (KEELING) ISLANDS', N'CC', N'CCK', 166)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (47, N'COLOMBIA', N'CO', N'COL', 170)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (48, N'COMOROS', N'KM', N'COM', 174)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (49, N'CONGO', N'CG', N'COG', 178)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (50, N'CONGO, THE DRC', N'CD', N'COD', 180)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (51, N'COOK ISLANDS', N'CK', N'COK', 184)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (52, N'COSTA RICA', N'CR', N'CRI', 188)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (53, N'COTE D''IVOIRE', N'CI', N'CIV', 384)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (54, N'CROATIA (local name: Hrvatska)', N'HR', N'HRV', 191)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (55, N'CUBA', N'CU', N'CUB', 192)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (56, N'CYPRUS', N'CY', N'CYP', 196)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (57, N'CZECH REPUBLIC', N'CZ', N'CZE', 203)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (58, N'DENMARK', N'DK', N'DNK', 208)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (59, N'DJIBOUTI', N'DJ', N'DJI', 262)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (60, N'DOMINICA', N'DM', N'DMA', 212)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (61, N'DOMINICAN REPUBLIC', N'DO', N'DOM', 214)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (62, N'EAST TIMOR', N'TP', N'TMP', 626)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (63, N'ECUADOR', N'EC', N'ECU', 218)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (64, N'EGYPT', N'EG', N'EGY', 818)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (65, N'EL SALVADOR', N'SV', N'SLV', 222)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (66, N'EQUATORIAL GUINEA', N'GQ', N'GNQ', 226)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (67, N'ERITREA', N'ER', N'ERI', 232)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (68, N'ESTONIA', N'EE', N'EST', 233)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (69, N'ETHIOPIA', N'ET', N'ETH', 231)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (70, N'FALKLAND ISLANDS (MALVINAS)', N'FK', N'FLK', 238)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (71, N'FAROE ISLANDS', N'FO', N'FRO', 234)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (72, N'FIJI', N'FJ', N'FJI', 242)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (73, N'FINLAND', N'FI', N'FIN', 246)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (74, N'FRANCE', N'FR', N'FRA', 250)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (75, N'FRANCE, METROPOLITAN', N'FX', N'FXX', 249)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (76, N'FRENCH GUIANA', N'GF', N'GUF', 254)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (77, N'FRENCH POLYNESIA', N'PF', N'PYF', 258)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (78, N'FRENCH SOUTHERN TERRITORIES', N'TF', N'ATF', 260)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (79, N'GABON', N'GA', N'GAB', 266)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (80, N'GAMBIA', N'GM', N'GMB', 270)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (81, N'GEORGIA', N'GE', N'GEO', 268)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (82, N'GERMANY', N'DE', N'DEU', 276)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (83, N'GHANA', N'GH', N'GHA', 288)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (84, N'GIBRALTAR', N'GI', N'GIB', 292)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (85, N'GREECE', N'GR', N'GRC', 300)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (86, N'GREENLAND', N'GL', N'GRL', 304)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (87, N'GRENADA', N'GD', N'GRD', 308)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (88, N'GUADELOUPE', N'GP', N'GLP', 312)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (89, N'GUAM', N'GU', N'GUM', 316)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (90, N'GUATEMALA', N'GT', N'GTM', 320)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (91, N'GUINEA', N'GN', N'GIN', 324)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (92, N'GUINEA-BISSAU', N'GW', N'GNB', 624)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (93, N'GUYANA', N'GY', N'GUY', 328)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (94, N'HAITI', N'HT', N'HTI', 332)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (95, N'HEARD AND MC DONALD ISLANDS', N'HM', N'HMD', 334)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (96, N'HOLY SEE (VATICAN CITY STATE)', N'VA', N'VAT', 336)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (97, N'HONDURAS', N'HN', N'HND', 340)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (98, N'HONG KONG', N'HK', N'HKG', 344)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (99, N'HUNGARY', N'HU', N'HUN', 348)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (100, N'ICELAND', N'IS', N'ISL', 352)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (101, N'INDIA', N'IN', N'IND', 356)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (102, N'INDONESIA', N'ID', N'IDN', 360)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (103, N'IRAN (ISLAMIC REPUBLIC OF)', N'IR', N'IRN', 364)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (104, N'IRAQ', N'IQ', N'IRQ', 368)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (105, N'IRELAND', N'IE', N'IRL', 372)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (106, N'ISRAEL', N'IL', N'ISR', 376)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (107, N'ITALY', N'IT', N'ITA', 380)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (108, N'JAMAICA', N'JM', N'JAM', 388)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (109, N'JAPAN', N'JP', N'JPN', 392)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (110, N'JORDAN', N'JO', N'JOR', 400)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (111, N'KAZAKHSTAN', N'KZ', N'KAZ', 398)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (112, N'KENYA', N'KE', N'KEN', 404)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (113, N'KIRIBATI', N'KI', N'KIR', 296)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (114, N'KOREA, D.P.R.O.', N'KP', N'PRK', 408)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (115, N'KOREA, REPUBLIC OF', N'KR', N'KOR', 410)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (116, N'KUWAIT', N'KW', N'KWT', 414)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (117, N'KYRGYZSTAN', N'KG', N'KGZ', 417)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (118, N'LAOS', N'LA', N'LAO', 418)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (119, N'LATVIA', N'LV', N'LVA', 428)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (120, N'LEBANON', N'LB', N'LBN', 422)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (121, N'LESOTHO', N'LS', N'LSO', 426)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (122, N'LIBERIA', N'LR', N'LBR', 430)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (123, N'LIBYAN ARAB JAMAHIRIYA', N'LY', N'LBY', 434)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (124, N'LIECHTENSTEIN', N'LI', N'LIE', 438)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (125, N'LITHUANIA', N'LT', N'LTU', 440)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (126, N'LUXEMBOURG', N'LU', N'LUX', 442)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (127, N'MACAU', N'MO', N'MAC', 446)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (128, N'MACEDONIA', N'MK', N'MKD', 807)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (129, N'MADAGASCAR', N'MG', N'MDG', 450)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (130, N'MALAWI', N'MW', N'MWI', 454)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (131, N'MALAYSIA', N'MY', N'MYS', 458)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (132, N'MALDIVES', N'MV', N'MDV', 462)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (133, N'MALI', N'ML', N'MLI', 466)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (134, N'MALTA', N'MT', N'MLT', 470)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (135, N'MARSHALL ISLANDS', N'MH', N'MHL', 584)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (136, N'MARTINIQUE', N'MQ', N'MTQ', 474)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (137, N'MAURITANIA', N'MR', N'MRT', 478)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (138, N'MAURITIUS', N'MU', N'MUS', 480)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (139, N'MAYOTTE', N'YT', N'MYT', 175)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (140, N'MEXICO', N'MX', N'MEX', 484)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (141, N'MICRONESIA, FEDERATED STATES OF', N'FM', N'FSM', 583)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (142, N'MOLDOVA, REPUBLIC OF', N'MD', N'MDA', 498)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (143, N'MONACO', N'MC', N'MCO', 492)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (144, N'MONGOLIA', N'MN', N'MNG', 496)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (145, N'MONTSERRAT', N'MS', N'MSR', 500)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (146, N'MOROCCO', N'MA', N'MAR', 504)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (147, N'MOZAMBIQUE', N'MZ', N'MOZ', 508)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (148, N'MYANMAR (Burma)', N'MM', N'MMR', 104)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (149, N'NAMIBIA', N'NA', N'NAM', 516)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (150, N'NAURU', N'NR', N'NRU', 520)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (151, N'NEPAL', N'NP', N'NPL', 524)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (152, N'NETHERLANDS', N'NL', N'NLD', 528)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (153, N'NETHERLANDS ANTILLES', N'AN', N'ANT', 530)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (154, N'NEW CALEDONIA', N'NC', N'NCL', 540)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (155, N'NEW ZEALAND', N'NZ', N'NZL', 554)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (156, N'NICARAGUA', N'NI', N'NIC', 558)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (157, N'NIGER', N'NE', N'NER', 562)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (158, N'NIGERIA', N'NG', N'NGA', 566)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (159, N'NIUE', N'NU', N'NIU', 570)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (160, N'NORFOLK ISLAND', N'NF', N'NFK', 574)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (161, N'NORTHERN MARIANA ISLANDS', N'MP', N'MNP', 580)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (162, N'NORWAY', N'NO', N'NOR', 578)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (163, N'OMAN', N'OM', N'OMN', 512)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (164, N'PAKISTAN', N'PK', N'PAK', 586)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (165, N'PALAU', N'PW', N'PLW', 585)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (166, N'PANAMA', N'PA', N'PAN', 591)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (167, N'PAPUA NEW GUINEA', N'PG', N'PNG', 598)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (168, N'PARAGUAY', N'PY', N'PRY', 600)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (169, N'PERU', N'PE', N'PER', 604)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (170, N'PHILIPPINES', N'PH', N'PHL', 608)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (171, N'PITCAIRN', N'PN', N'PCN', 612)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (172, N'POLAND', N'PL', N'POL', 616)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (173, N'PORTUGAL', N'PT', N'PRT', 620)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (174, N'PUERTO RICO', N'PR', N'PRI', 630)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (175, N'QATAR', N'QA', N'QAT', 634)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (176, N'REUNION', N'RE', N'REU', 638)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (177, N'ROMANIA', N'RO', N'ROM', 642)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (178, N'RUSSIAN FEDERATION', N'RU', N'RUS', 643)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (179, N'RWANDA', N'RW', N'RWA', 646)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (180, N'SAINT KITTS AND NEVIS', N'KN', N'KNA', 659)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (181, N'SAINT LUCIA', N'LC', N'LCA', 662)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (182, N'SAINT VINCENT AND THE GRENADINES', N'VC', N'VCT', 670)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (183, N'SAMOA', N'WS', N'WSM', 882)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (184, N'SAN MARINO', N'SM', N'SMR', 674)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (185, N'SAO TOME AND PRINCIPE', N'ST', N'STP', 678)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (186, N'SAUDI ARABIA', N'SA', N'SAU', 682)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (187, N'SENEGAL', N'SN', N'SEN', 686)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (188, N'SEYCHELLES', N'SC', N'SYC', 690)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (189, N'SIERRA LEONE', N'SL', N'SLE', 694)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (190, N'SINGAPORE', N'SG', N'SGP', 702)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (191, N'SLOVAKIA (Slovak Republic)', N'SK', N'SVK', 703)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (192, N'SLOVENIA', N'SI', N'SVN', 705)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (193, N'SOLOMON ISLANDS', N'SB', N'SLB', 90)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (194, N'SOMALIA', N'SO', N'SOM', 706)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (195, N'SOUTH AFRICA', N'ZA', N'ZAF', 710)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (196, N'SOUTH GEORGIA AND SOUTH S.S.', N'GS', N'SGS', 239)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (197, N'SPAIN', N'ES', N'ESP', 724)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (198, N'SRI LANKA', N'LK', N'LKA', 144)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (199, N'ST. HELENA', N'SH', N'SHN', 654)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (200, N'ST. PIERRE AND MIQUELON', N'PM', N'SPM', 666)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (201, N'SUDAN', N'SD', N'SDN', 736)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (202, N'SURINAME', N'SR', N'SUR', 740)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (203, N'SVALBARD AND JAN MAYEN ISLANDS', N'SJ', N'SJM', 744)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (204, N'SWAZILAND', N'SZ', N'SWZ', 748)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (205, N'SWEDEN', N'SE', N'SWE', 752)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (206, N'SWITZERLAND', N'CH', N'CHE', 756)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (207, N'SYRIAN ARAB REPUBLIC', N'SY', N'SYR', 760)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (208, N'TAIWAN, PROVINCE OF CHINA', N'TW', N'TWN', 158)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (209, N'TAJIKISTAN', N'TJ', N'TJK', 762)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (210, N'TANZANIA, UNITED REPUBLIC OF', N'TZ', N'TZA', 834)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (211, N'THAILAND', N'TH', N'THA', 764)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (212, N'TOGO', N'TG', N'TGO', 768)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (213, N'TOKELAU', N'TK', N'TKL', 772)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (214, N'TONGA', N'TO', N'TON', 776)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (215, N'TRINIDAD AND TOBAGO', N'TT', N'TTO', 780)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (216, N'TUNISIA', N'TN', N'TUN', 788)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (217, N'TURKEY', N'TR', N'TUR', 792)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (218, N'TURKMENISTAN', N'TM', N'TKM', 795)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (219, N'TURKS AND CAICOS ISLANDS', N'TC', N'TCA', 796)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (220, N'TUVALU', N'TV', N'TUV', 798)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (221, N'UGANDA', N'UG', N'UGA', 800)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (222, N'UKRAINE', N'UA', N'UKR', 804)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (223, N'UNITED ARAB EMIRATES', N'AE', N'ARE', 784)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (224, N'UNITED KINGDOM', N'GB', N'GBR', 826)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (225, N'UNITED STATES', N'US', N'USA', 840)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (226, N'U.S. MINOR ISLANDS', N'UM', N'UMI', 581)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (227, N'URUGUAY', N'UY', N'URY', 858)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (228, N'UZBEKISTAN', N'UZ', N'UZB', 860)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (229, N'VANUATU', N'VU', N'VUT', 548)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (230, N'VENEZUELA', N'VE', N'VEN', 862)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (231, N'VIET NAM', N'VN', N'VNM', 704)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (232, N'VIRGIN ISLANDS (BRITISH)', N'VG', N'VGB', 92)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (233, N'VIRGIN ISLANDS (U.S.)', N'VI', N'VIR', 850)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (234, N'WALLIS AND FUTUNA ISLANDS', N'WF', N'WLF', 876)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (235, N'WESTERN SAHARA', N'EH', N'ESH', 732)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (236, N'YEMEN', N'YE', N'YEM', 887)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (237, N'ZAMBIA', N'ZM', N'ZMB', 894)
INSERT [model].[Countries] ([Id], [Name], [Abbreviation2Letters], [Abbreviation3Letters], [NumberCode]) VALUES (238, N'ZIMBABWE', N'ZW', N'ZWE', 716)

SET IDENTITY_INSERT [model].[Countries] OFF

SET IDENTITY_INSERT [model].[StateOrProvinces] ON

INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (2, N'AL', N'ALABAMA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (3, N'AK', N'ALASKA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (4, N'AS', N'AMERICAN SAMOA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (5, N'AZ', N'ARIZONA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (6, N'AR', N'ARKANSAS', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (7, N'CA', N'CALIFORNIA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (8, N'CO', N'COLORADO', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (9, N'CT', N'CONNECTICUT', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (10, N'DE', N'DELAWARE', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (11, N'DC', N'DISTRICT OF COLUMBIA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (12, N'FM', N'FEDERATED [model].StateOrProvincesS OF MICRONESIA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (13, N'FL', N'FLORIDA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (14, N'GA', N'GEORGIA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (15, N'GU', N'GUAM GU', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (16, N'HI', N'HAWAII', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (17, N'ID', N'IDAHO', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (18, N'IL', N'ILLINOIS', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (19, N'IN', N'INDIANA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (20, N'IA', N'IOWA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (21, N'KS', N'KANSAS', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (22, N'KY', N'KENTUCKY', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (23, N'LA', N'LOUISIANA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (24, N'ME', N'MAINE', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (25, N'MH', N'MARSHALL ISLANDS', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (26, N'MD', N'MARYLAND', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (27, N'MA', N'MASSACHUSETTS', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (28, N'MI', N'MICHIGAN', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (29, N'MN', N'MINNESOTA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (30, N'MS', N'MISSISSIPPI', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (31, N'MO', N'MISSOURI', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (32, N'MT', N'MONTANA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (33, N'NE', N'NEBRASKA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (34, N'NV', N'NEVADA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (35, N'NH', N'NEW HAMPSHIRE', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (36, N'NJ', N'NEW JERSEY', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (37, N'NM', N'NEW MEXICO', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (38, N'NY', N'NEW YORK', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (39, N'NC', N'NORTH CAROLINA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (40, N'ND', N'NORTH DAKOTA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (41, N'MP', N'NORTHERN MARIANA ISLANDS', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (42, N'OH', N'OHIO', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (43, N'OK', N'OKLAHOMA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (44, N'OR', N'OREGON', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (45, N'PW', N'PALAU', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (46, N'PA', N'PENNSYLVANIA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (47, N'PR', N'PUERTO RICO', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (48, N'RI', N'RHODE ISLAND', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (49, N'SC', N'SOUTH CAROLINA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (50, N'SD', N'SOUTH DAKOTA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (51, N'TN', N'TENNESSEE', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (52, N'TX', N'TEXAS', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (53, N'UT', N'UTAH', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (54, N'VT', N'VERMONT', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (55, N'VI', N'VIRGIN ISLANDS', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (56, N'VA', N'VIRGINIA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (57, N'WA', N'WASHINGTON', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (58, N'WV', N'WEST VIRGINIA', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (59, N'WI', N'WISCONSIN', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (60, N'WY', N'WYOMING', 225)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (61, N'AB', N'Alberta', 38)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (62, N'BC', N'British Columbia', 38)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (63, N'MB', N'Manitoba', 38)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (64, N'NB', N'New Brunswick', 38)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (65, N'NL', N'Newfoundland and Labrador', 38)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (66, N'NT', N'Northwest Territories', 38)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (67, N'NS', N'Nova Scotia', 38)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (68, N'NU', N'Nunavut', 38)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (69, N'ON', N'Ontario', 38)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (70, N'PE', N'Prince Edward Island', 38)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (71, N'QC', N'Quebec', 38)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (72, N'SK', N'Saskatchewan', 38)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (73, N'YT', N'Yukon', 38)

SET IDENTITY_INSERT [model].[StateOrProvinces] OFF
");
        }

        public override void Down()
        {
        }
    }
}