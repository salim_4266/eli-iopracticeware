﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Permission, Role, RolePermission
    /// </summary>
    [Migration(201112121008)]
    public class Migration201112121008 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"
    -- Creating table 'Permissions'
    CREATE TABLE [model].[Permissions] (
        [Id] int IDENTITY(1,1) NOT NULL,
        [Name] nvarchar(max)  NOT NULL);
    GO

    -- Creating table 'RolePermissions'
    CREATE TABLE [model].[RolePermissions] (
        [IsDenied] bit  NOT NULL,
        [RoleId] int  NOT NULL,
        [PermissionId] int  NOT NULL);
    GO

    -- Creating primary key on [Id] in table 'Permissions'
    ALTER TABLE [model].[Permissions]
    ADD CONSTRAINT [PK_Permissions]
        PRIMARY KEY CLUSTERED ([Id] ASC);
    GO

    -- Creating primary key on [RoleId], [PermissionId] in table 'RolePermissions'
    ALTER TABLE [model].[RolePermissions]
    ADD CONSTRAINT [PK_RolePermissions]
        PRIMARY KEY CLUSTERED ([RoleId], [PermissionId] ASC);
    GO

    -- Creating foreign key on [PermissionId] in table 'RolePermissions'
    ALTER TABLE [model].[RolePermissions]
    ADD CONSTRAINT [FK_RolePermissionPermission]
        FOREIGN KEY ([PermissionId])
        REFERENCES [model].[Permissions]
            ([Id])
        ON DELETE NO ACTION ON UPDATE NO ACTION;

    -- Creating non-clustered index for FOREIGN KEY 'FK_RolePermissionPermission'
    CREATE INDEX [IX_FK_RolePermissionPermission]
    ON [model].[RolePermissions]
        ([PermissionId]);
    GO


-- Add this meta data to PracticeRepository for databases that don't have ResourceTypes

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'A')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Accountant', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'C')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Clinical Study', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)
 
IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'D')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Doctor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'F')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Front Desk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'L')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'L Biller', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'M')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Manager Front Desk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'N')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Nurse', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'O')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)
 
IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'P')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Practice Manager', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'Q')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Q Optical Supplier', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'R')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Room', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'S')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'S Billing Supervisor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'T')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Technician', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'X')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'X Archived User', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'Y')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Y Archived Optical Supplier', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

IF NOT EXISTS (SELECT Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) = 'Z')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'RESOURCETYPE', N'Z Archived Doctor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)

--any others from Resources table 
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) 
SELECT DISTINCT N'RESOURCETYPE', ResourceType, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0
FROM Resources 
WHERE ResourceType NOT IN
(SELECT SUBSTRING(Code, 1, 1) FROM PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE')



--Permission
SET IDENTITY_INSERT [model].[Permissions] ON
INSERT INTO [model].[Permissions] (Id, Name) VALUES (1, 'Admin Report User')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (2, 'Allow Aggr Insr Changes')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (3, 'Appointments')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (4, 'Change Scheduled Doctor')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (5, 'CheckIn')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (6, 'CheckOut')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (7, 'Contacts')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (8, 'DI Edit Previous Visit')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (9, 'Disbursements')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (10, 'Display Schedules')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (11, 'Exam CheckOut')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (12, 'Exam Elements')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (13, 'Exam Room')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (14, 'Findings')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (15, 'Follow Up And Surgery')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (16, 'Force')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (17, 'History, Allergies')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (18, 'Maintain Billing Notes')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (19, 'Manual Claim Entry')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (20, 'Meds')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (21, 'Office Monitor')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (22, 'Open Chart')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (23, 'Past Visits')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (24, 'Patient Clinical Data Admin')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (25, 'Patient Financials')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (26, 'Patient Info')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (27, 'Patient Locator')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (28, 'Patient Notes')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (29, 'Plan')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (30, 'Practice Notes')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (31, 'Remove Patient')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (32, 'Reports')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (33, 'Review')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (34, 'Schedule Notes')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (35, 'Send eRx')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (36, 'Set Doctor Schedule')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (37, 'Set Power User')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (38, 'Set/Clear Alerts')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (39, 'Set/Clear Send Collections')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (40, 'Set/Clear Send Statements')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (41, 'Setup Appt Slots')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (42, 'Setup Appt Types')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (43, 'Setup Codes')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (44, 'Setup Configuration')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (45, 'Setup Diagnosis')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (46, 'Setup Favorites')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (47, 'Setup Fee Schedules')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (48, 'Setup Notes, Actions, Tests')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (49, 'Setup Other Favorites')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (50, 'Setup Patient Access')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (51, 'Setup Place of Service')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (52, 'Setup Plans')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (53, 'Setup Practice')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (54, 'Setup Referring Doctors')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (55, 'Setup Reports')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (56, 'Setup Resources')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (57, 'Setup Sched Templates')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (58, 'Setup Services')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (59, 'Setup Type of Service')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (60, 'Setup Vendors')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (61, 'Submit')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (62, 'Waiting for Questionnaire')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (63, 'Waiting Room')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (64, 'Write Prescriptions')
SET IDENTITY_INSERT [model].[Permissions] OFF

--RolePermission
DECLARE @frontDesk int
SET @frontDesk = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) LIKE 'F%')

INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 3, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 5, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 6, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 33, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 24, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 62, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 63, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 13, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 21, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 10, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 25, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 34, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 35, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 27, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 7, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 28, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 32, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 26, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 9, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 38, 0)

DECLARE @manager int
SET @manager = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) LIKE 'M%')

INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 3, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 5, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 6, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 33, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 24, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 62, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 63, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 13, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 21, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 10, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 25, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 34, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 35, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 27, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 7, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 28, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 32, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 26, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 9, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 38, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 31, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 16, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 36, 0)

DECLARE @admin int
SET @admin = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) LIKE 'A%')

INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 3, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 5, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 6, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 33, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 24, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 62, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 63, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 13, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 21, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 10, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 25, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 34, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 35, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 27, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 7, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 28, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 32, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 26, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 9, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 38, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 61, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 19, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 39, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 40, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 18, 0)

DECLARE @tech int
SET @tech = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) LIKE 'T%')

INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 8, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 15, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 64, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 11, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 12, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 22, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 17, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 20, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 14, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 29, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 23, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 28, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 35, 0)
 
DECLARE @doctor int
SET @doctor = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) LIKE 'D%')
   
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 8, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 15, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 64, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 11, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 22, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 12, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 17, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 20, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 29, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 23, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 28, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 35, 0)

DECLARE @permission int
SET @permission = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) LIKE 'P%')
 
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 3, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 5, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 6, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 33, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 24, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 62, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 63, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 13, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 21, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 10, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 25, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 34, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 35, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 27, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 7, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 32, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 26, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 28, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 9, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 38, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 31, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 16, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 36, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 4, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 53, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 56, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 42, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 1, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 57, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 41, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 44, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 55, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 60, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 52, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 46, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 48, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 47, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 43, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 54, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 59, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 51, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 58, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 45, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 2, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 49, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 50, 0)
");
           
        }

        public override void Down()
        {
        }
    }
}
