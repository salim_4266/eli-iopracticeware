﻿-----Fix data in PatientReceivables, PracticeInsurers, PracticeTransactionJournal, ServiceTransactions


------INSURERID = 0 FIXES
-- InsurerId = 0 - try to move patient billing to primary receivable
DECLARE @ToReplaceBilling table(old int, new int) 
INSERT @ToReplaceBilling
SELECT ptj.transactiontypeid, MAX(pr.ReceivableId) 
FROM PatientReceivables pr0
INNER JOIN PatientReceivables pr ON pr.AppointmentId = pr0.AppointmentId
	AND pr.ReceivableId <> pr0.ReceivableId
	AND pr.InsurerId <> 0
	AND pr.InsurerId <> 99999
	AND pr.InsurerDesignation = 'T' 
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr0.ReceivableId
	AND TransactionType = 'R' AND TransactionRef IN ('P', 'G')
WHERE pr0.InsurerId = 0
GROUP BY ptj.TransactionTypeId

UPDATE PracticeTransactionJournal SET TransactionTypeId = 
	(SELECT New FROM @ToReplaceBilling
	WHERE old = TransactionTypeId)
WHERE TransactionTypeId IN (SELECT old FROM @ToReplaceBilling)

--InsurerId = 0 - move patient billing to any other receivable
DELETE FROM @ToReplaceBilling
INSERT @ToReplaceBilling
SELECT ptj.transactiontypeid, MAX(pr.ReceivableId) 
FROM PatientReceivables pr0
INNER JOIN PatientReceivables pr ON pr.AppointmentId = pr0.AppointmentId
	AND pr.ReceivableId <> pr0.ReceivableId
	AND pr.InsuredId = pr0.InsuredId
	AND pr.InsurerId <> 0
	AND pr.InsurerId <> 99999
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr0.ReceivableId
	AND TransactionType = 'R' AND TransactionRef IN ('P', 'G')
WHERE pr0.InsurerId = 0
GROUP BY ptj.TransactionTypeId

UPDATE PracticeTransactionJournal SET TransactionTypeId = 
	(SELECT New FROM @ToReplaceBilling
	WHERE old = TransactionTypeId)
WHERE TransactionTypeId IN (SELECT old FROM @ToReplaceBilling)

--InsurerId = 0 - move insurance billing to primary receivable
DELETE FROM @ToReplaceBilling
INSERT @ToReplaceBilling
SELECT ptj.transactiontypeid, MAX(pr.ReceivableId) 
FROM PatientReceivables pr0
INNER JOIN PatientReceivables pr ON pr.AppointmentId = pr0.AppointmentId
	AND pr.ReceivableId <> pr0.ReceivableId
	AND pr.InsurerId <> 0
	AND pr.InsurerId <> 99999
	AND pr.InsuredId = pr0.InsuredId
	AND pr.InsurerDesignation = 'T' 
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr0.ReceivableId
	AND TransactionType = 'R' AND TransactionRef = 'I'
WHERE pr0.InsurerId = 0
GROUP BY ptj.TransactionTypeId

UPDATE PracticeTransactionJournal SET TransactionTypeId = 
	(SELECT New FROM @ToReplaceBilling
	WHERE old = TransactionTypeId)
WHERE TransactionTypeId IN (SELECT old FROM @ToReplaceBilling)


-- InsurerId = 0 - move insurance billing to ANY other receivable - last ditch effort
DELETE FROM @ToReplaceBilling
INSERT @ToReplaceBilling
SELECT ptj.transactiontypeid, MAX(pr.ReceivableId) 
FROM PatientReceivables pr0
INNER JOIN PatientReceivables pr ON pr.AppointmentId = pr0.AppointmentId
	AND pr.ReceivableId <> pr0.ReceivableId
	AND pr.InsuredId = pr0.InsuredId
	AND pr.InsurerId <> 0
	AND pr.InsurerId <> 99999
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr0.ReceivableId
	AND TransactionType = 'R' AND TransactionRef = 'I'
WHERE pr0.InsurerId = 0
GROUP BY ptj.TransactionTypeId

UPDATE PracticeTransactionJournal SET TransactionTypeId = 
	(SELECT New FROM @ToReplaceBilling
	WHERE old = TransactionTypeId)
WHERE TransactionTypeId IN (SELECT old FROM @ToReplaceBilling)

-- InsurerId = 0 - move patient payments to primary receivable
DECLARE @ToReplacePayments table(old int, new int) 
INSERT @ToReplacePayments
SELECT  prp.ReceivableId, MAX(pr.ReceivableId)
FROM PatientReceivables pr0
INNER JOIN PatientReceivables pr ON pr0.AppointmentId = pr.AppointmentId
	AND pr0.ReceivableId <> pr.ReceivableId
	AND pr.InsurerId <> 0 
	AND pr.InsurerId <> 99999
	AND pr.InsurerDesignation = 'T'
INNER JOIN PatientReceivablePayments prp ON prp.ReceivableId = pr0.ReceivableId
	AND PayerType <> 'I'
WHERE pr0.InsurerId = 0
GROUP BY prp.ReceivableId

UPDATE PatientReceivablePayments SET ReceivableId = 
	(SELECT new FROM @ToReplacePayments
	WHERE old = ReceivableId)
WHERE ReceivableId IN (SELECT old FROM @ToReplacePayments)

-- InsurerId = 0 - move patient payments to ANY other receivable WHERE insurerid <> 0 - LAST DITCH EFFORT
DELETE FROM @ToReplacePayments
INSERT @ToReplacePayments
SELECT prp.ReceivableId, MAX(pr.ReceivableId)
FROM PatientReceivables pr0
INNER JOIN PatientReceivables pr ON pr0.AppointmentId = pr.AppointmentId 
	AND pr0.ReceivableId <> pr.ReceivableId
	AND pr.InsurerId <> 0 and pr.InsurerId <> 99999 
INNER JOIN PatientReceivablePayments prp ON prp.ReceivableId = pr0.ReceivableId
	AND PayerType <> 'I'
WHERE pr0.InsurerId = 0
GROUP BY prp.ReceivableId

UPDATE PatientReceivablePayments SET ReceivableId = 
	(SELECT new FROM @ToReplacePayments
	WHERE old = ReceivableId)
WHERE ReceivableId IN (SELECT old FROM @ToReplacePayments)


--InsurerId = 0 receivables -- DELETE
DELETE FROM PatientReceivables WHERE ReceivableId in
(SELECT pr0.ReceivableId FROM PatientReceivables pr0
LEFT JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr0.ReceivableId
	AND ptj.TransactionType = 'R'
LEFT JOIN PatientReceivablePayments prp ON prp.ReceivableId = pr0.ReceivableId
WHERE pr0.InsurerId = 0
	AND pr0.ReceivableId in 
		(SELECT pr1.ReceivableId FROM PatientReceivables pr1
		INNER JOIN PatientReceivables pr2 ON pr1.AppointmentId = pr2.AppointmentId
		WHERE pr1.ReceivableId <> pr2.receivableid
		AND pr1.InsurerId = 0
		AND pr2.InsurerId <> 99999
		AND pr2.InsurerID <> 0
		)
AND ptj.TransactionId is null
AND prp.PaymentId is null)



------ duplicate receivables fixes
----Update Billing Transactions - keep the lowest receivableid
DECLARE @ToReplaceBilling2 table(old int, new int) 
DELETE FROM @ToReplaceBilling2
INSERT @ToReplaceBilling2
SELECT ptj.TransactionTypeId, MIN(prKeep.ReceivableId) 
FROM PatientReceivables prDelete
INNER JOIN PatientReceivables prKeep ON prKeep.AppointmentId = prDelete.AppointmentId
	AND prKeep.ReceivableId < prDelete.ReceivableId
	AND prKeep.InsurerId = prDelete.InsurerId
	AND prKeep.InsuredId = prDelete.InsuredId
	AND prKeep.InsurerId <> 99999
	AND prKeep.InsurerId <> 0
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = prDelete.ReceivableId
	AND TransactionType = 'R'
GROUP BY ptj.TransactionTypeId

UPDATE PracticeTransactionJournal SET TransactionTypeId = 
	(SELECT New FROM @ToReplaceBilling2
	WHERE old = TransactionTypeId)
WHERE TransactionTypeId IN (SELECT old FROM @ToReplaceBilling2)


----move all payments AND adjustments to lowest receivable 
DELETE FROM @ToReplacePayments
INSERT @ToReplacePayments
SELECT  prp.ReceivableId, MIN(prKeep.ReceivableId)
FROM PatientReceivables prDelete
INNER JOIN PatientReceivables prKeep ON prDelete.AppointmentId = prKeep.AppointmentId
	AND prKeep.ReceivableId < prDelete.ReceivableId
	AND prKeep.InsuredId = prDelete.InsuredId
	AND prKeep.InsurerId = prDelete.InsurerId
	AND prKeep.InsurerId <> 99999
	AND prKeep.InsurerId <> 0
INNER JOIN PatientReceivablePayments prp ON prp.ReceivableId = prDelete.ReceivableId
WHERE prp.PayerId = CASE
	WHEN prp.PayerType = 'I'
		THEN prKeep.InsurerId 
	WHEN prp.PayerType = 'P' 
		THEN prKeep.PatientId
	ELSE 0
	END
	AND prp.PaymentType <> '='
GROUP BY prp.ReceivableId

UPDATE PatientReceivablePayments SET ReceivableId = 
	(SELECT new FROM @ToReplacePayments
	WHERE old = ReceivableId)
WHERE ReceivableId IN (SELECT old FROM @ToReplacePayments)

--move PatientPayments to the MIN receivable WHERE payertype isn't patient
DELETE FROM @ToReplacePayments
INSERT @ToReplacePayments
SELECT  prp.ReceivableId, MIN(prKeep.ReceivableId)
FROM PatientReceivables prDelete
INNER JOIN PatientReceivables prKeep ON prDelete.AppointmentId = prKeep.AppointmentId
	AND prKeep.ReceivableId < prDelete.ReceivableId
	AND prKeep.InsuredId = prDelete.InsuredId
	AND prKeep.InsurerId = prDelete.InsurerId
	AND prKeep.InsurerId <> 99999
	AND prKeep.InsurerId <> 0
INNER JOIN PatientReceivablePayments prp ON prp.ReceivableId = prDelete.ReceivableId
WHERE prp.PayerType = 'P'
	AND prp.PaymentType <> '='
GROUP BY prp.ReceivableId

UPDATE PatientReceivablePayments SET ReceivableId = 
	(SELECT new FROM @ToReplacePayments
	WHERE old = ReceivableId)
WHERE ReceivableId IN (SELECT old FROM @ToReplacePayments)

----delete duplicate receivables
DELETE 
FROM PatientReceivables
WHERE ReceivableId IN
	(SELECT prDelete.ReceivableId 
	FROM PatientReceivables prDelete
	INNER JOIN PatientReceivables prKeep ON prDelete.AppointmentId = prKeep.AppointmentId
	LEFT JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = prDelete.ReceivableId
		AND ptj.TransactionType = 'R'
	LEFT JOIN PatientReceivablePayments prp ON prp.ReceivableId = prDelete.ReceivableId
	WHERE prKeep.ReceivableId < prDelete.receivableid
		AND prKeep.InsurerId = prDelete.InsurerId
		AND prKeep.InsuredId = prDelete.InsuredId
		AND prKeep.InsurerId <> 99999
		AND prKeep.InsurerId <> 0
		AND prp.PaymentId IS NULL
		AND ptj.TransactionId IS NULL)

---PatientReceivablePayments - remove if receivable not present
DELETE prp
FROM PatientReceivablePayments prp
LEFT JOIN PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
WHERE pr.ReceivableId IS NULL
	AND prp.PaymentType <> '='

--Clean dates
UPDATE PatientReceivablePayments SET PaymentEOBDate = ''  WHERE  (ISDATE(PaymentEOBDate) = 0) AND PaymentEOBDate <> ''
UPDATE PatientReceivables SET LastPayDate= ''  WHERE  (ISDATE( LastPayDate) = 0 OR LastPayDate LIKE '%/%') AND LastPayDate <> ''

--PatientReceivables - remove dud receivables
DELETE FROM PatientReceivables
WHERE PatientId = 0 
	AND ReceivableId NOT IN (SELECT ReceivableId FROM PatientReceivablePayments)
	AND ReceivableId NOT IN (SELECT TransactionTypeId FROM PracticeTransactionJournal WHERE TransactionType = 'R')

DELETE pr
FROM PatientReceivables pr
LEFT JOIN PatientDemographics pd ON pd.PatientId = pr.PatientId
WHERE pd.PatientId IS NULL


--PatientReceivables - InsurerId = 0 when other receivables present
DELETE pr
FROM PatientReceivables pr
LEFT JOIN PatientReceivablePayments prp ON prp.ReceivableId = pr.ReceivableId
LEFT JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId	
	AND TransactionType = 'R'
WHERE InsurerId = 0 
	AND AppointmentId In
		(select AppointmentId 
			from patientreceivables 
			where appointmentid in 
				(select AppointmentId 
					from patientreceivables 
					where insurerid = 0)
			group by AppointmentId having count(*) > 1
		)
	AND ptj.TransactionId IS NULL
	AND prp.PaymentId IS NULL


--PatientReceivables - missing invoice date (exclude on account receivables)
UPDATE pr SET pr.InvoiceDate = ap.AppDate
FROM PatientReceivables pr
INNER JOIN Appointments ap on ap.AppointmentId = pr.AppointmentId
WHERE pr.InvoiceDate <> ap.AppDate
	AND pr.InsurerId <> 99999

--PatientReceivables - InsuredId set to PatientId if InsurerId is 0 and PolicyPatientId = PatientId
UPDATE pr SET pr.InsuredId = pr.PatientId
FROM PatientReceivables pr
INNER JOIN PatientDemographics pd ON pd.PatientId = pr.PatientId
	AND pd.PolicyPatientId = pd.PatientId	
WHERE InsuredId = 0 and InsurerId = 0

--PatientReceivables - InsuredId changed to PatientId if InsuredId doesn't exist
UPDATE pr SET InsuredId = pr.PatientId
FROM PatientReceivables pr
LEFT JOIN PatientDemographics pd ON pd.PatientId = pr.InsuredId
WHERE pr.InsuredId <> 0
	AND pr.InsurerId <> 99999
	AND pd.PatientId IS NULL


---PatientReceivables - two different billing doctors for same invoice
UPDATE pr SET pr.BillToDr = pr2.BillToDr
FROM dbo.PatientReceivables pr
INNER JOIN dbo.PatientReceivables pr2 ON pr.AppointmentId = pr2.AppointmentId
WHERE pr.BillToDr <> pr2.BillToDr
	AND pr.InsurerDesignation <> 'T'

---PatientReceivables - BillingOffice from Resources but does not have ServiceCode 02
UPDATE pr SET pr.BillingOffice = 0
FROM dbo.PatientReceivables pr
INNER JOIN dbo.Resources re ON pr.BillingOffice = re.ResourceId
WHERE re.ServiceCode <> '02'
	AND pr.BillingOffice BETWEEN 1 AND 999

--PatientReceivables - BillingOffice > 1000 but not from PracticeName
UPDATE PatientReceivables
SET BillingOffice = 0
WHERE BillingOffice NOT IN
   (SELECT PracticeId + 1000
   FROM PracticeName
   WHERE PracticeType = 'P')
AND BillingOffice > 1000

--PatientReceivables - Two different referring doctor for one invoice
UPDATE pr SET pr.ReferDr = pr2.ReferDr
FROM dbo.PatientReceivables pr
INNER JOIN dbo.PatientReceivables pr2 ON pr.AppointmentId = pr2.AppointmentId
WHERE pr.ReferDr <> pr2.ReferDr
	AND pr2.InsurerDesignation = 'T'

--Invalid billing offices from resources table
UPDATE pr
SET pr.BillingOffice = 0
FROM dbo.PatientReceivables pr
LEFT JOIN dbo.Resources re ON re.ResourceId = pr.BillingOffice
WHERE re.ResourceId IS NULL 
	AND pr.BillingOffice <> 0 
	AND BillingOffice BETWEEN 1 AND 999

-----------PatientReceivableServices
DELETE FROM PatientReceivableServices WHERE Invoice = ''

UPDATE prs SET prs.Status = 'X'
FROM PatientReceivableServices prs
LEFT JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
WHERE pr.Invoice IS NULL

-----------PracticeTransactionJournal
DELETE ptj
FROM PracticeTransactionJournal ptj
LEFT JOIN PatientReceivables pr ON pr.ReceivableId = ptj.TransactionTypeId
WHERE ptj.TransactionType = 'R'
	AND pr.ReceivableId IS NULL

-----------ServiceTransactions
DELETE st
FROM ServiceTransactions st
LEFT JOIN PracticeTransactionJournal ptj ON ptj.TransactionId = st.TransactionId
WHERE ptj.TransactionId IS NULL


