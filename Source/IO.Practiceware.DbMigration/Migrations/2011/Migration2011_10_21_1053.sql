﻿---CK Constraints to prevent ambiguous data


--Valid Provider, ResourceId1
--marked NOT FOR REPLICATION because HL7 INSERT/UPDATE query inserts ResourceId1 = 0 before the update.
IF OBJECT_ID('AppointmentsResourceId1ResourcesTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER AppointmentsResourceId1ResourcesTrigger')
END

GO

CREATE TRIGGER AppointmentsResourceId1ResourcesTrigger
ON Appointments
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF EXISTS (SELECT *
	FROM (SELECT * FROM inserted  EXCEPT SELECT * FROM deleted) i
	LEFT JOIN dbo.Resources re WITH(NOLOCK) ON i.ResourceId1 = re.ResourceId 
		AND re.ResourceType IN ('D', 'Q', 'Z', 'Y') 
	WHERE re.ResourceId IS NULL
	AND dbo.GetNoCheck() = 0)
	   
BEGIN
	RAISERROR ('Invalid appointment provider check ResourceId1.',16,1)
	ROLLBACK TRANSACTION
END

GO
