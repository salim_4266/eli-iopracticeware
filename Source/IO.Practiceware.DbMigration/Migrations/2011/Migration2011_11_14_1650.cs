﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Adds tables ExternalOrganizationTypes, FinancialTypeGroups
    /// </summary>
    [Migration(201111151650)]
    public class Migration201111151650 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"

-- Creating table 'ExternalOrganizationTypes'
CREATE TABLE [model].[ExternalOrganizationTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IsArchived] bit  NOT NULL
);
GO

-- Creating table 'FinancialTypeGroups'
CREATE TABLE [model].[FinancialTypeGroups] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'ExternalOrganizationTypes'
ALTER TABLE [model].[ExternalOrganizationTypes]
ADD CONSTRAINT [PK_ExternalOrganizationTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FinancialTypeGroups'
ALTER TABLE [model].[FinancialTypeGroups]
ADD CONSTRAINT [PK_FinancialTypeGroups]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
");

            Execute.Sql(@"
SET IDENTITY_INSERT [model].[FinancialTypeGroups] ON

INSERT [model].[FinancialTypeGroups] (Id, Name) VALUES (1, 'Insurer Adjustments')
INSERT [model].[FinancialTypeGroups] (Id, Name) VALUES (2, 'Office Adjustments')
INSERT [model].[FinancialTypeGroups] (Id, Name) VALUES (3, 'Patient Adjustments')

SET IDENTITY_INSERT [model].[FinancialTypeGroups] OFF
");

            Execute.Sql(@"
SET IDENTITY_INSERT [model].[ExternalOrganizationTypes] ON

INSERT [model].[ExternalOrganizationTypes] (Id, Name, [IsArchived]) VALUES (1, N'Clinic', 0)
INSERT [model].[ExternalOrganizationTypes] (Id, Name, [IsArchived]) VALUES (2, N'Diagnostic Test Laboratory', 0)
INSERT [model].[ExternalOrganizationTypes] (Id, Name, [IsArchived]) VALUES (3, N'Hospital', 0)
INSERT [model].[ExternalOrganizationTypes] (Id, Name, [IsArchived]) VALUES (4, N'Institution', 0)
INSERT [model].[ExternalOrganizationTypes] (Id, Name, [IsArchived]) VALUES (5, N'Medical Practice', 0)
INSERT [model].[ExternalOrganizationTypes] (Id, Name, [IsArchived]) VALUES (6, N'Nursing Home', 0)
INSERT [model].[ExternalOrganizationTypes] (Id, Name, [IsArchived]) VALUES (7, N'Vendor', 0)
INSERT [model].[ExternalOrganizationTypes] (Id, Name, [IsArchived]) VALUES (8, N'Other', 0)

SET IDENTITY_INSERT [model].[ExternalOrganizationTypes] OFF
");   
        }

        public override void Down()
        {
        }
    }
}