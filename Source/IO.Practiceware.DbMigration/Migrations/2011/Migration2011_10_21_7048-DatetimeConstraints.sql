﻿--Date validity, creating constraints on date fields (not MU tables)

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Appointments_AppDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Appointments]'))
ALTER TABLE [dbo].Appointments DROP CONSTRAINT CK_Appointments_AppDate
GO
ALTER TABLE dbo.Appointments WITH CHECK  ADD CONSTRAINT CK_Appointments_AppDate
    CHECK NOT FOR REPLICATION ((ISDATE(AppDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, AppDate, 112), 112) = AppDate) OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Appointments_SetDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Appointments]'))
ALTER TABLE [dbo].Appointments DROP CONSTRAINT CK_Appointments_SetDate
GO
ALTER TABLE dbo.Appointments WITH CHECK  ADD CONSTRAINT CK_Appointments_SetDate
    CHECK NOT FOR REPLICATION ((ISDATE(SetDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, SetDate, 112), 112) = SetDate) OR (SetDate = '' OR SetDate IS NULL)  OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_AppointmentTrack_ApptTrackDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppointmentTrack]'))
ALTER TABLE [dbo].AppointmentTrack DROP CONSTRAINT CK_AppointmentTrack_ApptTrackDate
GO
ALTER TABLE dbo.AppointmentTrack WITH CHECK  ADD CONSTRAINT CK_AppointmentTrack_ApptTrackDate
    CHECK NOT FOR REPLICATION ((ISDATE(ApptTrackDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, ApptTrackDate, 112), 112) = ApptTrackDate) OR dbo.GetNoCheck() = 1)
GO
	
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_AppointmentTrack_ApptDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppointmentTrack]'))
ALTER TABLE [dbo].AppointmentTrack DROP CONSTRAINT CK_AppointmentTrack_ApptDate
GO
ALTER TABLE dbo.AppointmentTrack WITH CHECK  ADD CONSTRAINT CK_AppointmentTrack_ApptDate
    CHECK NOT FOR REPLICATION ((ISDATE(ApptDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, ApptDate, 112), 112) = ApptDate) OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_CLOrders_OrderDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[CLOrders]'))
ALTER TABLE [dbo].CLOrders DROP CONSTRAINT CK_CLOrders_OrderDate
GO
ALTER TABLE dbo.CLOrders WITH CHECK  ADD CONSTRAINT CK_CLOrders_OrderDate
    CHECK NOT FOR REPLICATION ((ISDATE(OrderDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, OrderDate, 112), 112) = OrderDate) OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientDemographics_FinancialSignatureDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientDemographics]'))
ALTER TABLE [dbo].PatientDemographics DROP CONSTRAINT CK_PatientDemographics_FinancialSignatureDate
GO
ALTER TABLE dbo.PatientDemographics WITH CHECK  ADD CONSTRAINT CK_PatientDemographics_FinancialSignatureDate
    CHECK NOT FOR REPLICATION ((ISDATE(FinancialSignatureDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, FinancialSignatureDate, 112), 112) = FinancialSignatureDate) OR (FinancialSignatureDate IS NULL OR FinancialSignatureDate = '') OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientFinancial_FinancialStartDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientFinancial]'))
ALTER TABLE [dbo].PatientFinancial DROP CONSTRAINT CK_PatientFinancial_FinancialStartDate
GO
ALTER TABLE dbo.PatientFinancial WITH CHECK  ADD CONSTRAINT CK_PatientFinancial_FinancialStartDate
    CHECK NOT FOR REPLICATION ((ISDATE(FinancialStartDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, FinancialStartDate, 112), 112) = FinancialStartDate) OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientFinancial_FinancialEndDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientFinancial]'))
ALTER TABLE [dbo].PatientFinancial DROP CONSTRAINT CK_PatientFinancial_FinancialEndDate
GO
ALTER TABLE dbo.PatientFinancial WITH CHECK  ADD CONSTRAINT CK_PatientFinancial_FinancialEndDate
    CHECK NOT FOR REPLICATION ((ISDATE(FinancialEndDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, FinancialEndDate, 112), 112) = FinancialEndDate) OR (FinancialEndDate IS NULL OR FinancialEndDate = '') OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientNotes_NoteDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientNotes]'))
ALTER TABLE [dbo].PatientNotes DROP CONSTRAINT CK_PatientNotes_NoteDate
GO
ALTER TABLE dbo.PatientNotes WITH CHECK  ADD CONSTRAINT CK_PatientNotes_NoteDate
    CHECK NOT FOR REPLICATION ((ISDATE(NoteDate) = 1  AND CONVERT(nvarchar, CONVERT(datetime, NoteDate, 112), 112) = NoteDate) OR (NoteDate = '' OR NoteDate IS NULL) OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientNotes_NoteOffDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientNotes]'))
ALTER TABLE [dbo].PatientNotes DROP CONSTRAINT CK_PatientNotes_NoteOffDate
GO
ALTER TABLE dbo.PatientNotes WITH CHECK  ADD CONSTRAINT CK_PatientNotes_NoteOffDate
    CHECK NOT FOR REPLICATION ((ISDATE(NoteOffDate) = 1  AND CONVERT(nvarchar, CONVERT(datetime, NoteOffDate, 112), 112) = NoteOffDate)  OR (NoteOffDate = '' OR NoteOffDate IS NULL) OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientPreCerts_PreCertDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientPreCerts]'))
ALTER TABLE [dbo].PatientPreCerts DROP CONSTRAINT CK_PatientPreCerts_PreCertDate
GO
ALTER TABLE dbo.PatientPreCerts WITH CHECK  ADD CONSTRAINT CK_PatientPreCerts_PreCertDate
    CHECK NOT FOR REPLICATION ((ISDATE(PreCertDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, PreCertDate, 112), 112) = PreCertDate) OR (PreCertDate = '') OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReceivablePayments_PaymentDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivablePayments]'))
ALTER TABLE [dbo].PatientReceivablePayments DROP CONSTRAINT CK_PatientReceivablePayments_PaymentDate
GO
ALTER TABLE dbo.PatientReceivablePayments WITH CHECK ADD CONSTRAINT CK_PatientReceivablePayments_PaymentDate
    CHECK NOT FOR REPLICATION ((ISDATE(PaymentDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, PaymentDate, 112), 112) = PaymentDate)  OR (PaymentDate = '' OR PaymentDate IS NULL) OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReceivablePayments_PaymentEOBDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivablePayments]'))
ALTER TABLE [dbo].PatientReceivablePayments DROP CONSTRAINT CK_PatientReceivablePayments_PaymentEOBDate
GO
ALTER TABLE dbo.PatientReceivablePayments WITH CHECK ADD CONSTRAINT CK_PatientReceivablePayments_PaymentEOBDate
    CHECK NOT FOR REPLICATION ((ISDATE(PaymentEOBDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, PaymentEOBDate, 112), 112) = PaymentEOBDate)  OR (PaymentEOBDate = '' OR PaymentEOBDate IS NULL) OR dbo.GetNoCheck() = 1)
GO

--Have the is null or blank inclusion here for on account credits that do not have dates.
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReceivables_InvoiceDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivables]'))
ALTER TABLE [dbo].PatientReceivables DROP CONSTRAINT CK_PatientReceivables_InvoiceDate
GO
ALTER TABLE dbo.PatientReceivables WITH CHECK  ADD CONSTRAINT CK_PatientReceivables_InvoiceDate
    CHECK NOT FOR REPLICATION ((ISDATE(InvoiceDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, InvoiceDate, 112), 112) = InvoiceDate)  OR (InvoiceDate = '' OR InvoiceDate IS NULL) OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReceivables_PatientBillDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivables]'))
ALTER TABLE [dbo].PatientReceivables DROP CONSTRAINT CK_PatientReceivables_PatientBillDate
GO
ALTER TABLE dbo.PatientReceivables WITH CHECK  ADD CONSTRAINT CK_PatientReceivables_PatientBillDate
    CHECK NOT FOR REPLICATION ((ISDATE(PatientBillDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, PatientBillDate, 112), 112) = PatientBillDate)  OR (PatientBillDate = '' OR PatientBillDate IS NULL) OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReceivables_LastPayDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivables]'))
ALTER TABLE [dbo].PatientReceivables DROP CONSTRAINT CK_PatientReceivables_LastPayDate
GO
ALTER TABLE dbo.PatientReceivables WITH CHECK  ADD CONSTRAINT CK_PatientReceivables_LastPayDate
    CHECK NOT FOR REPLICATION ((ISDATE(LastPayDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, LastPayDate, 112), 112) = LastPayDate)  OR (LastPayDate = '' OR LastPayDate IS NULL) OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReceivableServices_ServiceDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivableServices]'))
ALTER TABLE [dbo].PatientReceivableServices DROP CONSTRAINT CK_PatientReceivableServices_ServiceDate
GO
ALTER TABLE dbo.PatientReceivableServices WITH CHECK  ADD CONSTRAINT CK_PatientReceivableServices_ServiceDate
    CHECK NOT FOR REPLICATION ((ISDATE(ServiceDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, ServiceDate, 112), 112) = ServiceDate)  OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReceivableServices_PostDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivableServices]'))
ALTER TABLE [dbo].PatientReceivableServices DROP CONSTRAINT CK_PatientReceivableServices_PostDate
GO
ALTER TABLE dbo.PatientReceivableServices WITH CHECK  ADD CONSTRAINT CK_PatientReceivableServices_PostDate
    CHECK NOT FOR REPLICATION ((ISDATE(PostDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, PostDate, 112), 112) = PostDate)  OR (PostDate = '' OR PostDate IS NULL) OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReferral_ReferralDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReferral]'))
ALTER TABLE [dbo].PatientReferral DROP CONSTRAINT CK_PatientReferral_ReferralDate
GO
ALTER TABLE dbo.PatientReferral WITH CHECK  ADD CONSTRAINT CK_PatientReferral_ReferralDate
    CHECK NOT FOR REPLICATION ((ISDATE(ReferralDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, ReferralDate, 112), 112) = ReferralDate)  OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReferral_ReferralExpireDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReferral]'))
ALTER TABLE [dbo].PatientReferral DROP CONSTRAINT CK_PatientReferral_ReferralExpireDate
GO
ALTER TABLE dbo.PatientReferral WITH CHECK  ADD CONSTRAINT CK_PatientReferral_ReferralExpireDate
    CHECK NOT FOR REPLICATION ((ISDATE(ReferralExpireDate) = 1  AND CONVERT(nvarchar, CONVERT(datetime, ReferralExpireDate, 112), 112) = ReferralExpireDate)  OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PracticeActivity_ActivityDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PracticeActivity]'))
ALTER TABLE [dbo].PracticeActivity DROP CONSTRAINT CK_PracticeActivity_ActivityDate
GO
ALTER TABLE dbo.PracticeActivity WITH CHECK  ADD CONSTRAINT CK_PracticeActivity_ActivityDate
    CHECK NOT FOR REPLICATION ((ISDATE(ActivityDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, ActivityDate, 112), 112) = ActivityDate)  OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PracticeCalendar_CalendarDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PracticeCalendar]'))
ALTER TABLE [dbo].PracticeCalendar DROP CONSTRAINT CK_PracticeCalendar_CalendarDate
GO
ALTER TABLE dbo.PracticeCalendar WITH CHECK  ADD CONSTRAINT CK_PracticeCalendar_CalendarDate
    CHECK NOT FOR REPLICATION ((ISDATE(CalendarDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, CalendarDate, 112), 112) = CalendarDate)  OR dbo.GetNoCheck() = 1)
GO


--THESE HAVE A DATE FORMAT OF mm/dd/yyyy
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReceivables_HspAdmDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivables]'))
ALTER TABLE [dbo].PatientReceivables DROP CONSTRAINT CK_PatientReceivables_HspAdmDate
GO
ALTER TABLE dbo.PatientReceivables WITH CHECK  ADD CONSTRAINT CK_PatientReceivables_HspAdmDate
    CHECK NOT FOR REPLICATION ((ISDATE(HspAdmDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, HspAdmDate, 101), 101) = HspAdmDate)  OR (HspAdmDate IS NULL OR HspAdmDate = '') OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReceivables_HspDisDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivables]'))
ALTER TABLE [dbo].PatientReceivables DROP CONSTRAINT CK_PatientReceivables_HspDisDate
GO
ALTER TABLE dbo.PatientReceivables WITH CHECK  ADD CONSTRAINT CK_PatientReceivables_HspDisDate
    CHECK NOT FOR REPLICATION ((ISDATE(HspDisDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, HspDisDate, 101), 101) = HspDisDate)  OR (HspDisDate IS NULL OR HspDisDate = '') OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReceivables_FirstConsDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivables]'))
ALTER TABLE [dbo].PatientReceivables DROP CONSTRAINT CK_PatientReceivables_FirstConsDate
GO
ALTER TABLE dbo.PatientReceivables WITH CHECK  ADD CONSTRAINT CK_PatientReceivables_FirstConsDate
    CHECK NOT FOR REPLICATION ((ISDATE(FirstConsDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, FirstConsDate, 101), 101) = FirstConsDate)  OR (FirstConsDate IS NULL OR FirstConsDate = '') OR dbo.GetNoCheck() = 1)
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReceivables_RsnDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivables]'))
ALTER TABLE [dbo].PatientReceivables DROP CONSTRAINT CK_PatientReceivables_RsnDate
GO
ALTER TABLE dbo.PatientReceivables WITH CHECK  ADD CONSTRAINT CK_PatientReceivables_RsnDate
    CHECK NOT FOR REPLICATION ((ISDATE(RsnDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, RsnDate, 101), 101) = RsnDate)  OR (RsnDate IS NULL OR RsnDate = '') OR dbo.GetNoCheck() = 1)
GO
