﻿--PatientDemographics, data clean up.

UPDATE dbo.PatientDemographics
SET SecondPolicyPatientId = 0
WHERE SecondPolicyPatientId IS NULL

UPDATE dbo.PatientDemographics
SET Status = 'A'
WHERE Status = '' OR Status IS NULL

UPDATE PatientDemographics
SET SecondPolicyPatientId = 0, SecondRelationship = ''
WHERE PolicyPatientId = SecondPolicyPatientId