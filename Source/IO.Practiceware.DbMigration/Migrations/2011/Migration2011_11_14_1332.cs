﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// AddressType
    /// </summary>
    [Migration(201111141331)]
    public class Migration201111141331 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"

-- Creating table 'AddressTypes'
CREATE TABLE [model].[AddressTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [__EntityType__] nvarchar(100)  NOT NULL,
    [IsArchived] bit  NOT NULL,
);
GO

-- Creating primary key on [Id] in table 'AddressTypes'
ALTER TABLE [model].[AddressTypes]
ADD CONSTRAINT [PK_AddressTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
");

            Execute.Sql(@"
SET IDENTITY_INSERT [model].[AddressTypes] ON
INSERT [model].[AddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (1, 'Home', 'PatientAddressType', 0)
INSERT [model].[AddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (2, 'Business', 'PatientAddressType', 0)
INSERT [model].[AddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (3, 'Claims', 'InsurerAddressType', 0)
INSERT [model].[AddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (4, 'Appeals', 'InsurerAddressType', 0)
INSERT [model].[AddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (5, 'Physical Location','BillingOrganizationAddressType', 0)
INSERT [model].[AddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (6, 'Pay To','BillingOrganizationAddressType', 0)
INSERT [model].[AddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (7, 'Main Office', 'ServiceLocationAddressType', 0)
INSERT [model].[AddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (8, 'Main Office', 'ContactAddressType', 0)
INSERT [model].[AddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (9, 'Home', 'UserAddressType', 0)
INSERT [model].[AddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (10, 'Work', 'UserAddressType', 0)
INSERT [model].[AddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (11, 'Main Office', 'ExternalOrganizationAddressType', 0)

SET IDENTITY_INSERT [model].[AddressTypes] OFF

DBCC CHECKIDENT ('[model].[AddressTypes]', 'RESEED', 1000)

-- These can be edited or deleted by user
INSERT [model].[AddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('Other1', 'PatientAddressType', 0)
INSERT [model].[AddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('Other2', 'PatientAddressType', 0)
INSERT [model].[AddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('User1', 'UserAddressType', 0)
INSERT [model].[AddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('Other1', 'ContactAddressType', 0)
INSERT [model].[AddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('Other2', 'ContactAddressType', 0)
INSERT [model].[AddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('Other1', 'ExternalOrganizationAddressType', 0)
INSERT [model].[AddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('Other2', 'ExternalOrganizationAddressType', 0)
");


        }

        public override void Down()
        {
        }
    }
}