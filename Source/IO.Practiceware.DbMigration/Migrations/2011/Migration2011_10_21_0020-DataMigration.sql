﻿-----Fix data in PatientReceivablesPayments, PatientReceivables - see Migration2011_10_21_0021 for more PatientReceivables migrations

--PatientReceivablePayments - fix batch if PayerId = 0 and PayerType <> Office
UPDATE prpBatch SET prpBatch.PayerId = prpPaid.PayerId
FROM PatientReceivablePayments prpBatch
INNER JOIN PatientReceivablePayments prpPaid on prpPaid.PaymentBatchCheckId = prpBatch.PaymentId
	AND prpBatch.PaymentType = '='
	AND prpBatch.PayerType <> 'O'
	AND prpBatch.PayerId = 0

---PatientReceivablePayments - Invalid batch id
UPDATE prp SET PaymentBatchCheckId = 0
FROM dbo.PatientReceivablePayments prp
LEFT JOIN dbo.PatientReceivablePayments prpBatch ON prpBatch.PaymentId = prp.PaymentBatchCheckId
	AND prpBatch.PaymentType = '='
WHERE prpBatch.PaymentId IS NULL 
	AND prp.PaymentType <> '='
	AND prp.PaymentBatchCheckId NOT IN (-1, 0)
	AND prp.PaymentBatchCheckId IS NOT NULL

--Fix payments against invalid Services
UPDATE prp
SET PaymentServiceItem = 0
FROM PatientReceivablePayments prp
LEFT JOIN PatientReceivableServices prs ON prs.ItemId = prp.PaymentServiceItem
WHERE prs.ItemId IS NULL 
	AND PaymentServiceItem <> 0


--Used to ensure payment dates are correct.
UPDATE PatientReceivablePayments 
SET PaymentDate = 20010101
WHERE ISDATE(PaymentDate) = 0

UPDATE PatientReceivablePayments 
SET PaymentDate = 20010101
WHERE (ISDATE(PaymentDate) = 1 AND CONVERT(nvarchar, CONVERT(datetime, PaymentDate, 112), 112) <> PaymentDate)

--PayerType I but PayerId is PatientId -- change to InsurerId
UPDATE prp SET prp.PayerId = pr.InsurerId
FROM PatientReceivablePayments prp
INNER JOIN PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
WHERE PayerType = 'I'
AND PaymentType <> '='
AND PayerId NOT IN (SELECT InsurerId FROM PracticeInsurers)
AND PayerId = pr.PatientId


----Fix insurance payments from missing insurers
IF (SELECT COUNT(*)  FROM PatientReceivablePayments prp
LEFT JOIN PracticeInsurers pins ON prp.PayerId = pins.InsurerId 
WHERE pins.InsurerId IS NULL
AND prp.PayerType = 'I') > 0

BEGIN
	IF NOT EXISTS(SELECT * FROM PracticeInsurers WHERE InsurerName = 'ZZZ Deleted Insurer')
	INSERT INTO PracticeInsurers (InsurerName, InsurerBusinessClass) VALUES ('ZZZ Deleted Insurer', 'COMM')

	UPDATE prp
	SET PayerId = (SELECT InsurerId FROM PracticeInsurers WHERE InsurerName = 'ZZZ Deleted Insurer')
	FROM PatientReceivablePayments prp
	LEFT JOIN PracticeInsurers pins ON prp.PayerId = pins.InsurerId 
	WHERE pins.InsurerId IS NULL
	AND prp.PayerType = 'I'

END

----Fix receivables with missing insurers
IF (SELECT COUNT(*) FROM PatientReceivables pr
LEFT JOIN PracticeInsurers pins ON pr.InsurerId = pins.InsurerId
WHERE pins.InsurerId IS NULL) > 1

BEGIN
	IF NOT EXISTS(SELECT * FROM PracticeInsurers WHERE InsurerName = 'ZZZ Deleted Insurer')
	INSERT INTO PracticeInsurers (InsurerName, InsurerBusinessClass) VALUES ('ZZZ Deleted Insurer', 'COMM')

	UPDATE pr
	SET InsurerId = (SELECT InsurerId FROM PracticeInsurers WHERE InsurerName = 'ZZZ Deleted Insurer')
	FROM PatientReceivables pr
	LEFT JOIN PracticeInsurers pins ON pr.InsurerId = pins.InsurerId
	WHERE pins.InsurerId IS NULL
	AND pr.InsurerId <> 99999
	AND pr.InsurerId <> 0

END


--1.  Insurance payment attached to a receivable with a different insurer id.  Insurer names match.  Update prp.PayerId to pr.InsurerId
UPDATE prp
SET PayerId = pr.InsurerId
FROM PatientReceivablePayments prp
INNER JOIN PatientReceivables pr ON prp.ReceivableId = pr.ReceivableId
INNER JOIN PracticeInsurers pri ON pr.InsurerId = pri.InsurerId
INNER JOIN PracticeInsurers pri2 ON prp.PayerId = pri2.InsurerId
WHERE prp.PayerId <> pr.InsurerId
	AND prp.PayerType = 'i'
	AND PaymentType <> '='
	AND pri2.InsurerName = pri.InsurerName

--2.  Insurance payment has PayerId = 0.
UPDATE prp
SET payerid = insurerid
FROM PatientReceivablePayments prp
INNER JOIN PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
WHERE prp.PayerId <> pr.InsurerId
	AND PayerType = 'i'
	AND PaymentType <> '='
	AND PayerId = 0

--3.  Insurance payment attached to a receivable with a different insurer id.  Names don't match.  Update prp.ReceivableId to a receivable id that has the matching InsurerId.
UPDATE prp
SET prp.receivableid = prnew.receivableid
FROM PatientReceivables prNew
INNER JOIN PatientReceivables prOld ON prnew.AppointmentId = prold.AppointmentId
	AND prnew.ReceivableId <> prold.ReceivableId
INNER JOIN PatientReceivablePayments prp ON prp.ReceivableId = prOld.ReceivableId
WHERE prp.PayerType = 'i'
	AND PaymentType <> '='
	AND prp.PayerId <> prOld.InsurerId
	AND prp.PayerId = prNew.InsurerId

--4.  Insurance payment attached to a receivable with a different insurer id.  Names don't match.  No other receivable for this appointment with matching insurer id.  Update prp.ReceivableId to receivable that has the matching InsurerName.
UPDATE prp
SET prp.receivableid = prnew.receivableid
FROM PatientReceivables prNew
INNER JOIN PatientReceivables prOld ON prnew.AppointmentId = prold.AppointmentId
	AND prnew.ReceivableId <> prold.ReceivableId
INNER JOIN PatientReceivablePayments prp ON prp.ReceivableId = prOld.ReceivableId
INNER JOIN PracticeInsurers pri ON prOld.InsurerId = pri.InsurerId
INNER JOIN PracticeInsurers pri2 ON prp.PayerId = pri2.InsurerId
LEFT JOIN PracticeInsurers pri3 ON prNew.InsurerId = pri3.InsurerId
WHERE prp.PayerType = 'i'
	AND PaymentType <> '='
	AND prp.PayerId <> prOld.InsurerId
	AND prp.PayerId <> prNew.InsurerId
	AND pri.InsurerName <> pri2.InsurerName
	AND pri2.InsurerName = pri3.InsurerName
	
--5.  Insurance payment attached to receivable with a different insurer id.  The pr.InsurerId is wrong.  Update pr.InsurerId = prp.PayerId
UPDATE pr
SET pr.insurerid = prp.payerid
FROM PatientReceivables pr
INNER JOIN PatientReceivablePayments prp ON prp.ReceivableId = pr.ReceivableId
	AND prp.PayerId <> pr.InsurerId
	AND prp.PayerType = 'I'
WHERE Invoice IN (
		SELECT pr1.invoice
		FROM PatientReceivables pr1
		INNER JOIN PatientReceivableS pr2 ON pr1.Invoice = pr2.Invoice
		WHERE pr1.ReceivableId <> pr2.receivableid
			AND pr1.InsurerId = pr2.InsurerId
			AND pr1.InsuredId = pr2.InsuredId
			AND pr1.Invoice <> ''
			AND pr2.Invoice <> ''
		)

--6. Clean patient payments from patients that do not exist in Patient demographics table
UPDATE prp
SET prp.PayerId = pr.PatientId
FROM PatientReceivablePayments prp
INNER JOIN PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
LEFT JOIN PatientDemographics pd ON pd.PatientId = prp.PayerId
	AND prp.PayerType = 'P' 
WHERE prp.payertype = 'P'
	AND pd.PatientId IS NULL
	
DELETE prp
FROM PatientReceivablePayments prp
WHERE prp.ReceivableId = 0 
	AND prp.PayerId = 0
	AND prp.PayerType = 'P'


--BillToDr = 0
UPDATE pr
SET BillToDr = Ap.ResourceId1
FROM PatientReceivables pr
INNER JOIN Appointments ap ON ap.AppointmentId = pr.AppointmentId
WHERE pr.BillToDr = 0


