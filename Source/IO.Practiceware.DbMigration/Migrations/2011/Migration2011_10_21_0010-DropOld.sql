﻿-- Drops old model views and tables (pre 7.20)

-- Logging tables
EXEC dbo.DropObject 'LogEntryProperty'
EXEC dbo.DropObject 'LogEntryLogCategory'
EXEC dbo.DropObject 'LogEntry'
EXEC dbo.DropObject 'LogCategory'

EXEC dbo.DropObject 'LogEntryProperties'
EXEC dbo.DropObject 'LogCategories'
EXEC dbo.DropObject 'LogEntries'

-- Surgery tables
EXEC dbo.DropObject 'dbo.UserSugeryTemplatePreference'
EXEC dbo.DropObject 'dbo.UserSurgeryTemplatePreference'
EXEC dbo.DropObject 'dbo.SugeryTemplateScreenQuestion'
EXEC dbo.DropObject 'dbo.SurgeryTemplateScreenQuestion'
EXEC dbo.DropObject 'dbo.PatientSugeryQuestionChoice'
EXEC dbo.DropObject 'dbo.PatientSurgeryQuestionChoice'
EXEC dbo.DropObject 'dbo.PatientSugeryQuestionAnswer'
EXEC dbo.DropObject 'dbo.PatientSurgeryQuestionAnswer'
EXEC dbo.DropObject 'dbo.PatientSugery'
EXEC dbo.DropObject 'dbo.PatientSurgery'
EXEC dbo.DropObject 'dbo.Screen'
EXEC dbo.DropObject 'dbo.SugeryTemplate'
EXEC dbo.DropObject 'dbo.SurgeryTemplate'
EXEC dbo.DropObject 'dbo.SurgeryType'
EXEC dbo.DropObject 'dbo.Choice'
EXEC dbo.DropObject 'dbo.Question'

-- Obsolete model views
EXEC dbo.DropObject 'dbo.UserModelView'
EXEC dbo.DropObject 'dbo.PatientModelView'
EXEC dbo.DropObject 'dbo.UserScheduleEntryModelView'
EXEC dbo.DropObject 'dbo.ExternalProviderModelView'