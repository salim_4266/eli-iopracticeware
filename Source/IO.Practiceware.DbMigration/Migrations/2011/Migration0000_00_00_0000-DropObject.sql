﻿
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DropObject]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[DropObject]

GO

CREATE PROCEDURE [dbo].[DropObject](@objectName varchar(500))
AS
BEGIN	
	SET NOCOUNT ON
	
	DECLARE @id int 
	SET @id = OBJECT_ID(@objectName)

	IF @id IS NOT NULL
	BEGIN
		DECLARE @dependentObjectNames table(Name nvarchar(max))
		DECLARE @dependentObjectName nvarchar(max);

		WITH DependentObjectCTE (DependentObjectId, DependentObjectName, ReferencedObjectName, ReferencedObjectId)
		AS
		(
		SELECT DISTINCT
			sd.object_id,
			ds.name + '.' + do.name,
			ReferencedObject = rs.name + '.' + ro.name,
			ReferencedObjectId = sd.referenced_major_id
		FROM    
			sys.sql_dependencies sd
			JOIN sys.objects ro ON sd.referenced_major_id = ro.object_id
			JOIN sys.schemas rs ON ro.schema_id = rs.schema_id
			JOIN sys.objects do ON sd.object_id = do.object_id
			JOIN sys.schemas ds ON do.schema_id = ds.schema_id			
		WHERE   
			sd.referenced_major_id = OBJECT_ID(@objectName)
		UNION ALL
		SELECT
			sd.object_id,
			ds.name + '.' + do.name,
			rs.name + '.' + ro.name,
			sd.referenced_major_id
		FROM    
			sys.sql_dependencies sd
			JOIN sys.objects ro ON sd.referenced_major_id = ro.object_id
			JOIN sys.schemas rs ON ro.schema_id = rs.schema_id
			JOIN sys.objects do ON sd.object_id = do.object_id
			JOIN sys.schemas ds ON do.schema_id = ds.schema_id	
			JOIN DependentObjectCTE doCTE ON sd.referenced_major_id = doCTE.DependentObjectId       
		WHERE
			sd.referenced_major_id <> sd.object_id     
		)
		INSERT @dependentObjectNames
		SELECT DISTINCT
			DependentObjectName
		FROM   
			DependentObjectCTE c

		DECLARE	c CURSOR LOCAL
		FOR SELECT Name
		FROM @dependentObjectNames
		OPEN c

		FETCH NEXT FROM c into @dependentObjectName
		WHILE @@fetch_status = 0

		BEGIN		
			EXEC('IF OBJECT_ID(N''' + @dependentObjectName + ''') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID(N''' + @dependentObjectName + '''), ''IsSchemaBound'') = 1 EXEC dbo.DropObject ''' + @dependentObjectName + '''')
			FETCH NEXT FROM c into @dependentObjectName
		END

		CLOSE c
		DEALLOCATE c

		DECLARE @objectType nvarchar(255)
		SET @objectType = 
			CASE WHEN OBJECTPROPERTY(@id, N'IsTrigger') = 1 THEN 'TRIGGER'
			WHEN OBJECTPROPERTY(@id, N'IsView') = 1 THEN 'VIEW'
			WHEN OBJECTPROPERTY(@id, N'IsProcedure') = 1 THEN 'PROCEDURE'
			WHEN OBJECTPROPERTY(@id, N'IsTable') = 1 THEN 'TABLE'
			WHEN EXISTS(SELECT xtype FROM dbo.sysobjects WHERE id = @id AND xtype in (N'FN', N'IF', N'TF')) THEN 'FUNCTION'
			END

		IF @objectType = 'TABLE'
		BEGIN
			DECLARE @dropForeignKeys nvarchar(max)
			SET @dropForeignKeys = ''
			SELECT @dropForeignKeys = @dropForeignKeys + 
				'ALTER TABLE [' + OBJECT_SCHEMA_NAME(parent_object_id) +
					'].[' + OBJECT_NAME(parent_object_id) + 
				'] DROP CONSTRAINT ' + name + ';
				'
			FROM sys.foreign_keys
			WHERE referenced_object_id = OBJECT_ID(@objectName)

			EXEC(@dropForeignKeys)
		END 
		
		IF @objectType IS NOT NULL
			EXEC('DROP ' + @objectType + ' ' + @objectName)

	END
				
	RETURN 0
END

GO
