﻿--Date validity

CREATE FUNCTION dbo.IsDate112(@value nvarchar(max))
RETURNS int
WITH SCHEMABINDING
AS
BEGIN
	RETURN CASE WHEN ISDATE(@value) = 1 AND CONVERT(nvarchar, CONVERT(datetime, @value), 112) = @value THEN 1 ELSE 0 END
END

GO

CREATE FUNCTION dbo.IsDate101(@value nvarchar(max))
RETURNS int
WITH SCHEMABINDING
AS
BEGIN
	RETURN CASE WHEN ISDATE(@value) = 1 AND CONVERT(nvarchar, CONVERT(datetime, @value), 101) = @value THEN 1 ELSE 0 END
END

GO

CREATE FUNCTION dbo.TryConvertDateTime112 (@value NVARCHAR(12))
RETURNS DATETIME
	WITH SCHEMABINDING
AS
BEGIN
	RETURN CASE 
			WHEN LEN(@value) = 8
				AND isnumeric(@value) = 1
				AND @value > '17530101'
				AND @value < '99991231'
				AND RIGHT(@value, 2) > '00'
				AND RIGHT(@value, 2) < '32'
				AND LEFT(RIGHT(@value, 4), 2) > '00'
				AND LEFT(RIGHT(@value, 4), 2) < '13'
				THEN CONVERT(datetime, @value, 112)
			ELSE NULL
			END
END

GO

CREATE FUNCTION dbo.TryConvertDateTime101 (@value nvarchar(12))
RETURNS DATETIME
	WITH SCHEMABINDING
AS
BEGIN
	SET @value = REPLACE(@value, '/','')
	SET @value = RIGHT(@value, 4) + LEFT(@value, 2) + LEFT(RIGHT(@value, 6), 2)
	RETURN CASE WHEN LEN(@value) = 8 THEN dbo.TryConvertDateTime112(@value) ELSE NULL END
END

GO

CREATE FUNCTION dbo.GetMax(@x int, @y int)
RETURNS int
WITH SCHEMABINDING
AS
BEGIN
	RETURN CASE 
		WHEN @x > @y
			THEN @x
		ELSE @y
	END
END

GO

CREATE FUNCTION dbo.TryConvertUniqueIdentifier(@value nvarchar(32))
RETURNS uniqueidentifier
WITH SCHEMABINDING
AS
BEGIN
	SET @value = STUFF(@value, 1, 0, REPLICATE('0', 32 - LEN(@value)))

	IF ISNUMERIC(@value) = 0 OR @value < '0'
		RETURN NULL

	RETURN CONVERT(uniqueidentifier,LEFT(@value, 8)
                                + '-' +RIGHT(LEFT(@value, 12), 4)
                                + '-' +RIGHT(LEFT(@value, 16), 4)
                                + '-' +RIGHT(LEFT(@value, 20), 4)
                                + '-' +RIGHT(@value, 12))
END

GO
