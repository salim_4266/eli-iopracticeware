﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Adds Resources.IsLoggedIn
    /// </summary>
    [Migration(201110212350)]
    public class Migration201110212350 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"
IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'Resources' AND sc.Name = 'IsLoggedIn') 
    ALTER TABLE RESOURCES ADD IsLoggedIn BIT NOT NULL DEFAULT(0)
");
        }

        public override void Down()
        {
        }
    }
}