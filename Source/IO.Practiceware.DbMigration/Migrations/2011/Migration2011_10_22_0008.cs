﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Adds Task Management tables.
    /// </summary>
    [Migration(201110220008)]
    public class Migration201110220008 : Migration
    {
        public override void Up()
        {
            Execute.Sql(
                @"
-- Creating table 'Tasks'
CREATE TABLE [model].[Tasks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PatientId] int  NULL,
    [Title] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'TaskActivities'
CREATE TABLE [model].[TaskActivities] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TaskId] int  NOT NULL,
    [TaskActivityTypeId] int  NOT NULL,
    [AssignedIndividually] bit  NOT NULL,
    [Title] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [CreatedByUserId] int  NOT NULL,
    [CreatedDateTime] datetime  NOT NULL
);
GO

-- Creating table 'TaskActivityTypes'
CREATE TABLE [model].[TaskActivityTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(250)  NOT NULL,
    [DefaultContent] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL,
    [AlertDefault] bit  NOT NULL,
    [AssignIndividuallyDefault] bit  NOT NULL,
    [LinkToPatientDefault] bit  NOT NULL
);
GO

-- Creating table 'TaskActivityUsers'
CREATE TABLE [model].[TaskActivityUsers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TaskActivityId] int  NOT NULL,
    [AssignedToUserId] int  NOT NULL,
    [Active] bit  NOT NULL,
    [AlertActive] bit  NOT NULL,
    [CompletedDateTime] datetime  NULL,
    [AlertDeactivatedDateTime] datetime  NULL
);
GO

-- Creating primary key on [Id] in table 'Tasks'
ALTER TABLE [model].[Tasks]
ADD CONSTRAINT [PK_Tasks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TaskActivities'
ALTER TABLE [model].[TaskActivities]
ADD CONSTRAINT [PK_TaskActivities]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TaskActivityTypes'
ALTER TABLE [model].[TaskActivityTypes]
ADD CONSTRAINT [PK_TaskActivityTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TaskActivityUsers'
ALTER TABLE [model].[TaskActivityUsers]
ADD CONSTRAINT [PK_TaskActivityUsers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [TaskId] in table 'TaskActivities'
ALTER TABLE [model].[TaskActivities]
ADD CONSTRAINT [FK_TaskActivity_Task]
    FOREIGN KEY ([TaskId])
    REFERENCES [model].[Tasks]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TaskActivity_Task'
CREATE INDEX [IX_FK_TaskActivity_Task]
ON [model].[TaskActivities]
    ([TaskId]);
GO

-- Creating foreign key on [TaskActivityTypeId] in table 'TaskActivities'
ALTER TABLE [model].[TaskActivities]
ADD CONSTRAINT [FK_TaskActivity_TaskActivityType]
    FOREIGN KEY ([TaskActivityTypeId])
    REFERENCES [model].[TaskActivityTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TaskActivity_TaskActivityType'
CREATE INDEX [IX_FK_TaskActivity_TaskActivityType]
ON [model].[TaskActivities]
    ([TaskActivityTypeId]);
GO

-- Creating foreign key on [TaskActivityId] in table 'TaskActivityUsers'
ALTER TABLE [model].[TaskActivityUsers]
ADD CONSTRAINT [FK_TaskActivityUser_TaskActivity]
    FOREIGN KEY ([TaskActivityId])
    REFERENCES [model].[TaskActivities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TaskActivityUser_TaskActivity'
CREATE INDEX [IX_FK_TaskActivityUser_TaskActivity]
ON [model].[TaskActivityUsers]
    ([TaskActivityId]);
GO

-- Creating table 'Groups'
CREATE TABLE [model].[Groups] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(250)  NOT NULL
);
GO

-- Creating table 'GroupUsers'
CREATE TABLE [model].[GroupUsers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [GroupId] int  NOT NULL,
    [UserId] int  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'Groups'
ALTER TABLE [model].[Groups]
ADD CONSTRAINT [PK_Groups]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GroupUsers'
ALTER TABLE [model].[GroupUsers]
ADD CONSTRAINT [PK_GroupUsers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [GroupId] in table 'GroupUsers'
ALTER TABLE [model].[GroupUsers]
ADD CONSTRAINT [FK_GroupUser_Group]
    FOREIGN KEY ([GroupId])
    REFERENCES [model].[Groups]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_GroupUser_Group'
CREATE INDEX [IX_FK_GroupUser_Group]
ON [model].[GroupUsers]
    ([GroupId]);
GO
");
        }

        public override void Down()
        {
        }
    }
}