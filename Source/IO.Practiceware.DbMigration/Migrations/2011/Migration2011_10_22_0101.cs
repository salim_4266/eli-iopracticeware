﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    ///   Adds Question/Choice/Screen and Surgery tables.
    /// </summary>
    [Migration(201110220101)]
    public class Migration201110220101 : Migration
    {
        public override void Up()
        {
            Execute.Sql(
                @"
-- Creating table 'Choices'
CREATE TABLE [model].[Choices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(255)  NOT NULL,
    [QuestionId] int  NOT NULL,
    [IsDefault] bit  NOT NULL,
    [OrdinalId] int  NOT NULL,
    [IsArchived] bit  NOT NULL
);
GO

-- Creating table 'Questions'
CREATE TABLE [model].[Questions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(255)  NOT NULL,
    [OrdinalId] int  NOT NULL,
    [IsArchived] bit  NOT NULL
);
GO

-- Creating table 'Screens'
CREATE TABLE [model].[Screens] (
    [Id] int NOT NULL,
    [Name] nvarchar(255)  NOT NULL
);
GO
-- Creating primary key on [Id] in table 'Choices'
ALTER TABLE [model].[Choices]
ADD CONSTRAINT [PK_Choices]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Questions'
ALTER TABLE [model].[Questions]
ADD CONSTRAINT [PK_Questions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Screens'
ALTER TABLE [model].[Screens]
ADD CONSTRAINT [PK_Screens]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [QuestionId] in table 'Choices'
ALTER TABLE [model].[Choices]
ADD CONSTRAINT [FK_Choice_Question1]
    FOREIGN KEY ([QuestionId])
    REFERENCES [model].[Questions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Choice_Question1'
CREATE INDEX [IX_FK_Choice_Question1]
ON [model].[Choices]
    ([QuestionId]);
GO

INSERT [model].[Screens] ([Id], [Name]) VALUES (1, N'Health-Assessment')
INSERT [model].[Screens] ([Id], [Name]) VALUES (2, N'Height-Weight')
INSERT [model].[Screens] ([Id], [Name]) VALUES (3, N'Current Medications')
INSERT [model].[Screens] ([Id], [Name]) VALUES (4, N'Allergies')
INSERT [model].[Screens] ([Id], [Name]) VALUES (5, N'Work-Up')
INSERT [model].[Screens] ([Id], [Name]) VALUES (6, N'Operating-Room')
INSERT [model].[Screens] ([Id], [Name]) VALUES (7, N'Post-OP')

SET IDENTITY_INSERT [model].[Questions] ON
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (1, N'Eye Context', 1, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (2, N'Note Box Bottom', 2, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (3, N'Note Box Top', 1, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (4, N'Height-Feet', 1, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (5, N'Height-Inches', 2, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (6, N'Height-M', 3, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (7, N'Height-CM', 4, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (8, N'Weight-Lbs', 1, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (9, N'Weight-Ounces', 2, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (10, N'Weight-Kgs', 3, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (11, N'Weight-Grams', 4, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (12, N'IV In', 1, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (13, N'IV In Notes', 2, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (14, N'Work-up BP Systolic', 1, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (15, N'Work-up BP Diastolic', 2, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (16, N'Work-up BP Type', 1, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (17, N'Work-up Resp', 5, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (18, N'Work-up O2 Saturation', 7, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (19, N'Work-up Pulse', 8, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (20, N'Surgeon', 1, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (21, N'Assistant Surgeon', 2, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (22, N'Scrub Tech', 3, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (23, N'Circulator', 4, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (24, N'Other Team Members', 5, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (25, N'Anesthesia', 6, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (26, N'Anesthesiologist', 7, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (27, N'Equipment', 8, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (28, N'Drugs', 9, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (29, N'IV Out', 2, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (30, N'IV Out Notes', 1, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (31, N'Post-op BP Systolic', 1, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (32, N'Post-op BP Diastolic', 2, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (33, N'Post-op BP Type', 1, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (34, N'Post-op Resp', 5, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (35, N'Post-op Pulse', 8, 0)
INSERT [model].[Questions] ([Id], [Name], [OrdinalId], [IsArchived]) VALUES (36, N'Post-op O2 Saturation', 7, 0)
SET IDENTITY_INSERT [model].[Questions] OFF

-- Creating table 'PatientSurgeries'
CREATE TABLE [model].[PatientSurgeries] (
    [SurgeryTypeId] int  NOT NULL,
    [SurgeonUserId] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'PatientSurgeryQuestionAnswers'
CREATE TABLE [model].[PatientSurgeryQuestionAnswers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [QuestionId] int  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NULL,
    [ModifiedByUserId] int  NOT NULL,
    [ModifiedDateTime] datetime  NOT NULL,
    [PatientSurgeryId] int  NOT NULL
);
GO

-- Creating table 'PatientSurgeryQuestionChoices'
CREATE TABLE [model].[PatientSurgeryQuestionChoices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ChoiceId] int  NOT NULL,
    [Note] nvarchar(max)  NULL,
    [ModifiedByUserId] int  NOT NULL,
    [ModifiedDateTime] datetime  NOT NULL,
    [PatientSurgeryId] int  NOT NULL
);
GO

-- Creating table 'SurgeryTemplates'
CREATE TABLE [model].[SurgeryTemplates] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(255)  NOT NULL,
    [SurgeryTypeId] int  NOT NULL
);
GO

-- Creating table 'SurgeryTemplateScreenQuestions'
CREATE TABLE [model].[SurgeryTemplateScreenQuestions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ScreenId] int  NOT NULL,
    [SurgeryTemplateId] int  NOT NULL,
    [QuestionId] int  NOT NULL
);
GO

-- Creating table 'SurgeryTypes'
CREATE TABLE [model].[SurgeryTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(255)  NOT NULL,
    [IsArchived] bit  NOT NULL
);
GO

-- Creating table 'UserSurgeryTemplatePreferences'
CREATE TABLE [model].[UserSurgeryTemplatePreferences] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserId] int  NOT NULL,
    [SurgeryTemplateId] int  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'PatientSurgeries'
ALTER TABLE [model].[PatientSurgeries]
ADD CONSTRAINT [PK_PatientSurgeries]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientSurgeryQuestionAnswers'
ALTER TABLE [model].[PatientSurgeryQuestionAnswers]
ADD CONSTRAINT [PK_PatientSurgeryQuestionAnswers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientSurgeryQuestionChoices'
ALTER TABLE [model].[PatientSurgeryQuestionChoices]
ADD CONSTRAINT [PK_PatientSurgeryQuestionChoices]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SurgeryTemplates'
ALTER TABLE [model].[SurgeryTemplates]
ADD CONSTRAINT [PK_SurgeryTemplates]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SurgeryTemplateScreenQuestions'
ALTER TABLE [model].[SurgeryTemplateScreenQuestions]
ADD CONSTRAINT [PK_SurgeryTemplateScreenQuestions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SurgeryTypes'
ALTER TABLE [model].[SurgeryTypes]
ADD CONSTRAINT [PK_SurgeryTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UserSurgeryTemplatePreferences'
ALTER TABLE [model].[UserSurgeryTemplatePreferences]
ADD CONSTRAINT [PK_UserSurgeryTemplatePreferences]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [ChoiceId] in table 'PatientSurgeryQuestionChoices'
ALTER TABLE [model].[PatientSurgeryQuestionChoices]
ADD CONSTRAINT [FK_PatientSurgeryQuestionChoice_Choice]
    FOREIGN KEY ([ChoiceId])
    REFERENCES [model].[Choices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSurgeryQuestionChoice_Choice'
CREATE INDEX [IX_FK_PatientSurgeryQuestionChoice_Choice]
ON [model].[PatientSurgeryQuestionChoices]
    ([ChoiceId]);
GO

-- Creating foreign key on [SurgeryTypeId] in table 'PatientSurgeries'
ALTER TABLE [model].[PatientSurgeries]
ADD CONSTRAINT [FK_PatientSurgery_SurgeryType]
    FOREIGN KEY ([SurgeryTypeId])
    REFERENCES [model].[SurgeryTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSurgery_SurgeryType'
CREATE INDEX [IX_FK_PatientSurgery_SurgeryType]
ON [model].[PatientSurgeries]
    ([SurgeryTypeId]);
GO

-- Creating foreign key on [QuestionId] in table 'PatientSurgeryQuestionAnswers'
ALTER TABLE [model].[PatientSurgeryQuestionAnswers]
ADD CONSTRAINT [FK_PatientSurgeryQuestionAnswer_Question]
    FOREIGN KEY ([QuestionId])
    REFERENCES [model].[Questions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSurgeryQuestionAnswer_Question'
CREATE INDEX [IX_FK_PatientSurgeryQuestionAnswer_Question]
ON [model].[PatientSurgeryQuestionAnswers]
    ([QuestionId]);
GO

-- Creating foreign key on [QuestionId] in table 'SurgeryTemplateScreenQuestions'
ALTER TABLE [model].[SurgeryTemplateScreenQuestions]
ADD CONSTRAINT [FK_SurgeryTemplateScreenQuestion_Question]
    FOREIGN KEY ([QuestionId])
    REFERENCES [model].[Questions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SurgeryTemplateScreenQuestion_Question'
CREATE INDEX [IX_FK_SurgeryTemplateScreenQuestion_Question]
ON [model].[SurgeryTemplateScreenQuestions]
    ([QuestionId]);
GO

-- Creating foreign key on [ScreenId] in table 'SurgeryTemplateScreenQuestions'
ALTER TABLE [model].[SurgeryTemplateScreenQuestions]
ADD CONSTRAINT [FK_SurgeryTemplateScreenQuestion_Screen]
    FOREIGN KEY ([ScreenId])
    REFERENCES [model].[Screens]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SurgeryTemplateScreenQuestion_Screen'
CREATE INDEX [IX_FK_SurgeryTemplateScreenQuestion_Screen]
ON [model].[SurgeryTemplateScreenQuestions]
    ([ScreenId]);
GO

-- Creating foreign key on [SurgeryTemplateId] in table 'SurgeryTemplateScreenQuestions'
ALTER TABLE [model].[SurgeryTemplateScreenQuestions]
ADD CONSTRAINT [FK_SurgeryTemplateScreenQuestion_SurgeryTemplate]
    FOREIGN KEY ([SurgeryTemplateId])
    REFERENCES [model].[SurgeryTemplates]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SurgeryTemplateScreenQuestion_SurgeryTemplate'
CREATE INDEX [IX_FK_SurgeryTemplateScreenQuestion_SurgeryTemplate]
ON [model].[SurgeryTemplateScreenQuestions]
    ([SurgeryTemplateId]);
GO

-- Creating foreign key on [SurgeryTypeId] in table 'SurgeryTemplates'
ALTER TABLE [model].[SurgeryTemplates]
ADD CONSTRAINT [FK_Template_SurgeryType]
    FOREIGN KEY ([SurgeryTypeId])
    REFERENCES [model].[SurgeryTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Template_SurgeryType'
CREATE INDEX [IX_FK_Template_SurgeryType]
ON [model].[SurgeryTemplates]
    ([SurgeryTypeId]);
GO

-- Creating foreign key on [SurgeryTemplateId] in table 'UserSurgeryTemplatePreferences'
ALTER TABLE [model].[UserSurgeryTemplatePreferences]
ADD CONSTRAINT [FK_UserSurgeryTemplatePreference_SurgeryTemplate]
    FOREIGN KEY ([SurgeryTemplateId])
    REFERENCES [model].[SurgeryTemplates]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UserSurgeryTemplatePreference_SurgeryTemplate'
CREATE INDEX [IX_FK_UserSurgeryTemplatePreference_SurgeryTemplate]
ON [model].[UserSurgeryTemplatePreferences]
    ([SurgeryTemplateId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSurgeryQuestionAnswerUser'
CREATE INDEX [IX_FK_PatientSurgeryQuestionAnswerUser]
ON [model].[PatientSurgeryQuestionAnswers]
    ([ModifiedByUserId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSurgeryQuestionChoiceUser'
CREATE INDEX [IX_FK_PatientSurgeryQuestionChoiceUser]
ON [model].[PatientSurgeryQuestionChoices]
    ([ModifiedByUserId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSurgeryUser'
CREATE INDEX [IX_FK_PatientSurgeryUser]
ON [model].[PatientSurgeries]
    ([SurgeonUserId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserSurgeryTemplatePreferenceUser'
CREATE INDEX [IX_FK_UserSurgeryTemplatePreferenceUser]
ON [model].[UserSurgeryTemplatePreferences]
    ([UserId]);
GO

-- Creating foreign key on [PatientSurgeryId] in table 'PatientSurgeryQuestionChoices'
ALTER TABLE [model].[PatientSurgeryQuestionChoices]
ADD CONSTRAINT [FK_PatientSurgeryPatientSurgeryQuestionChoice]
    FOREIGN KEY ([PatientSurgeryId])
    REFERENCES [model].[PatientSurgeries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSurgeryPatientSurgeryQuestionChoice'
CREATE INDEX [IX_FK_PatientSurgeryPatientSurgeryQuestionChoice]
ON [model].[PatientSurgeryQuestionChoices]
    ([PatientSurgeryId]);
GO

-- Creating foreign key on [PatientSurgeryId] in table 'PatientSurgeryQuestionAnswers'
ALTER TABLE [model].[PatientSurgeryQuestionAnswers]
ADD CONSTRAINT [FK_PatientSurgeryPatientSurgeryQuestionAnswer]
    FOREIGN KEY ([PatientSurgeryId])
    REFERENCES [model].[PatientSurgeries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSurgeryPatientSurgeryQuestionAnswer'
CREATE INDEX [IX_FK_PatientSurgeryPatientSurgeryQuestionAnswer]
ON [model].[PatientSurgeryQuestionAnswers]
    ([PatientSurgeryId]);
GO

-- Add ASC row to Database in the off position
IF NOT EXISTS (SELECT * FROM dbo.practiceinterfaceconfiguration WHERE FieldReference = 'ASC')
INSERT INTO dbo.PracticeInterfaceConfiguration (FieldReference, FieldValue) VALUES ('ASC', 'F')
GO
");
        }

        public override void Down()
        {
        }
    }
}