﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Add tables EncounterTypes, EncounterTypeInvoiceTypes
    /// </summary>
    [Migration(201111141336)]
    public class Migration201111141336 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"
SET IDENTITY_INSERT [model].[EncounterTypes] ON


IF NOT EXISTS (SELECT * FROM model.EncounterTypes WHERE Id = 4)
INSERT [model].[EncounterTypes] (Id, Name, IsArchived) VALUES (4, 'Vision Encounter', 0)

SET IDENTITY_INSERT [model].[EncounterTypes] OFF

--EncounterTypesInvoiceType 
IF NOT EXISTS (SELECT * FROM model.EncounterTypeInvoiceTypes WHERE EncounterTypeId = 4 AND InvoiceTypeId = 3)
INSERT [model].[EncounterTypeInvoiceTypes] (EncounterTypeId, InvoiceTypeId) VALUES (4, 3)
");
        }

        public override void Down()
        {
        }
    }
}