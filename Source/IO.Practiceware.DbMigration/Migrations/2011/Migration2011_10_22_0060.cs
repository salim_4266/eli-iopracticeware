﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Update the column lengths for addresses
    /// </summary>
    [Migration(201110220060)]
    public class Migration201110220060 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"

ALTER TABLE dbo.PatientDemographics
ALTER COLUMN BusinessAddress nvarchar (35)

ALTER TABLE dbo.PatientDemographics
ALTER COLUMN BusinessSuite nvarchar (35)

ALTER TABLE dbo.PatientDemographics
ALTER COLUMN BusinessCity nvarchar (35)

ALTER TABLE dbo.Resources
ALTER COLUMN ResourceAddress nvarchar (35)

ALTER TABLE dbo.Resources
ALTER COLUMN ResourceSuite nvarchar (35)

ALTER TABLE dbo.Resources
ALTER COLUMN ResourceCity nvarchar (35)

ALTER TABLE dbo.PracticeVendors
ALTER COLUMN VendorSuite nvarchar (35)

ALTER TABLE dbo.PracticeVendors
ALTER COLUMN VendorCity nvarchar (35)    
");
           
        }

        public override void Down()
        {
        }
    }
}
