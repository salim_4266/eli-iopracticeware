﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Creates logging tables.
    /// </summary>
    [Migration(201110222000)]
    public class Migration201110222000 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"
-- Creating table 'LogEntries'
CREATE TABLE [dbo].[LogEntries] (
    [Id] uniqueidentifier  NOT NULL,
    [Source] nvarchar(255)  NOT NULL,
    [Title] nvarchar(255)  NULL,
    [Message] nvarchar(max)  NOT NULL,
    [SeverityId] int  NOT NULL,
    [ActivityId] uniqueidentifier  NULL,
    [RelatedActivityId] uniqueidentifier  NULL,
    [EventId] int  NULL,
    [MachineName] nvarchar(50)  NULL,
    [ThreadName] nvarchar(255)  NULL,
    [ProcessId] int  NULL,
    [ProcessName] nvarchar(50)  NULL,
    [DateTime] datetime  NOT NULL,
    [UserId] nvarchar(50)  NULL
);
GO

-- Creating table 'LogEntryProperties'
CREATE TABLE [dbo].[LogEntryProperties] (
    [Id] uniqueidentifier  NOT NULL,
    [LogEntryId] uniqueidentifier  NOT NULL,
    [Name] nvarchar(255)  NOT NULL,
    [Value] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'LogCategories'
CREATE TABLE [dbo].[LogCategories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(255)  NOT NULL
);
GO

-- Creating table 'LogEntryLogCategory'
CREATE TABLE [dbo].[LogEntryLogCategory] (
    [LogEntryLogCategory_LogCategory_Id] uniqueidentifier  NOT NULL,
    [Categories_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'LogEntries'
ALTER TABLE [dbo].[LogEntries]
ADD CONSTRAINT [PK_LogEntries]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LogEntryProperties'
ALTER TABLE [dbo].[LogEntryProperties]
ADD CONSTRAINT [PK_LogEntryProperties]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LogCategories'
ALTER TABLE [dbo].[LogCategories]
ADD CONSTRAINT [PK_LogCategories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [LogEntryLogCategory_LogCategory_Id], [Categories_Id] in table 'LogEntryLogCategory'
ALTER TABLE [dbo].[LogEntryLogCategory]
ADD CONSTRAINT [PK_LogEntryLogCategory]
    PRIMARY KEY NONCLUSTERED ([LogEntryLogCategory_LogCategory_Id], [Categories_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [LogEntryId] in table 'LogEntryProperties'
ALTER TABLE [dbo].[LogEntryProperties]
ADD CONSTRAINT [FK_LogEntryPropertyLogEntry]
    FOREIGN KEY ([LogEntryId])
    REFERENCES [dbo].[LogEntries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LogEntryPropertyLogEntry'
CREATE INDEX [IX_FK_LogEntryPropertyLogEntry]
ON [dbo].[LogEntryProperties]
    ([LogEntryId]);
GO

-- Creating foreign key on [LogEntryLogCategory_LogCategory_Id] in table 'LogEntryLogCategory'
ALTER TABLE [dbo].[LogEntryLogCategory]
ADD CONSTRAINT [FK_LogEntryLogCategory_LogEntry]
    FOREIGN KEY ([LogEntryLogCategory_LogCategory_Id])
    REFERENCES [dbo].[LogEntries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Categories_Id] in table 'LogEntryLogCategory'
ALTER TABLE [dbo].[LogEntryLogCategory]
ADD CONSTRAINT [FK_LogEntryLogCategory_LogCategory]
    FOREIGN KEY ([Categories_Id])
    REFERENCES [dbo].[LogCategories]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LogEntryLogCategory_LogCategory'
CREATE INDEX [IX_FK_LogEntryLogCategory_LogCategory]
ON [dbo].[LogEntryLogCategory]
    ([Categories_Id]);
GO
");
        }

        public override void Down()
        {
        }
    }
}