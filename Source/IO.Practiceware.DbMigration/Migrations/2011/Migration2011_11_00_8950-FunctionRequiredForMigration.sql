﻿
--We use this function in migrations. 

EXEC [dbo].[RecreateIfDefinitionChanged] 'dbo.IsNullOrEmpty', '
	CREATE FUNCTION [dbo].[IsNullOrEmpty] (@entryString NVARCHAR(MAX))
	RETURNS BIT
	AS 
	BEGIN
		RETURN 
			CASE 
				WHEN @entryString IS NULL OR @entryString = ''''
					THEN CONVERT(BIT,1)
				ELSE CONVERT(BIT,0)
			END
	END'



