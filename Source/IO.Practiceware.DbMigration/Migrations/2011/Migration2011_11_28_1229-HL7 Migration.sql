﻿--Clean up data for HL7 Migration
IF EXISTS (SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'dbo.ExternalSystemEntityMapping'))
BEGIN
		
		DELETE em
		FROM  ExternalSystemEntityMapping em
		LEFT JOIN patientfinancial pf ON pf.Financialid = em.practicerepositoryentitykey
		WHERE PracticeRepositoryEntityId = (SELECT Id FROM PracticeRepositoryEntity WHERE Name = 'PatientInsurance')
		AND pf.financialid IS NULL 

END