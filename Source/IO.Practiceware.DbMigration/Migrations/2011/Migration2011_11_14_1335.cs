﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// AddressType
    /// </summary>
    [Migration(201111141335)]
    public class Migration201111141335 : Migration
    {
        public override void Up()
        {

            Execute.Sql(@"

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEBILLINGORGANIZATION' and Code = 'Billing')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEBILLINGORGANIZATION', 'Billing')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEBILLINGORGANIZATION' and Code = 'Primary' )
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEBILLINGORGANIZATION', 'Primary')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEBILLINGORGANIZATION' and Code = 'Fax' )
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEBILLINGORGANIZATION', 'Fax')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPECONTACT' and Code = 'Primary' )
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPECONTACT', 'Primary')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPECONTACT' and Code = 'Fax')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPECONTACT', 'Fax')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEINSURER' and Code = 'Primary')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEINSURER', 'Primary')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEPATIENT' and Code = 'Home')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEPATIENT', 'Home')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEPATIENT' and Code = 'Business')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEPATIENT', 'Business')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEPATIENT' and Code = 'Cell')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEPATIENT', 'Cell')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEPATIENT' and Code = 'Unknown')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEPATIENT', 'Unknown')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEUSER' and Code = 'Primary' )
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEUSER', 'Primary')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEUSER' and Code = 'Fax' )
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEUSER', 'Fax')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEUSER' and Code = 'Fax' )
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEUSER', 'User1')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPECONTACT' and Code = 'Cell')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPECONTACT', 'Cell')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEEXTERNALORGANIZATION' and Code = 'Primary' )
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEEXTERNALORGANIZATION', 'Primary')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEEXTERNALORGANIZATION' and Code = 'Fax' )
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEEXTERNALORGANIZATION', 'Fax')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEINSURER' and Code = 'Authorization')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEINSURER', 'Authorization')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEINSURER' and Code = 'Eligibility')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEINSURER', 'Eligibility')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEINSURER' and Code = 'Provider')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEINSURER', 'Provider')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEINSURER' and Code = 'Claims')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEINSURER', 'Claims')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEINSURER' and Code = 'Submissions')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEINSURER', 'Submissions')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEPATIENT' and Code = 'Emergency')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEPATIENT', 'Emergency')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPEPATIENT' and Code = 'Employer')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPEPATIENT', 'Employer')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPESERVICELOCATION' and Code = 'Primary')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPESERVICELOCATION', 'Primary')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPESERVICELOCATION' and Code = 'Other')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPESERVICELOCATION', 'Other')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PHONETYPESERVICELOCATION' and Code = 'Fax')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('PHONETYPESERVICELOCATION', 'Fax')

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'QUESTIONSETS' and Code = 'No Questions')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('QUESTIONSETS', 'No Questions')
");
        }

        public override void Down()
        {
        }
    }
}