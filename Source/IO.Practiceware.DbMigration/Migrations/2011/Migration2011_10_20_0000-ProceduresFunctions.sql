﻿-- Creates procedures and functions used across the entire system.
IF SCHEMA_ID('model') IS NULL
BEGIN
	EXEC('CREATE SCHEMA model')
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RecreateIfDefinitionChanged]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[RecreateIfDefinitionChanged]

GO

CREATE PROCEDURE [dbo].[RecreateIfDefinitionChanged](@objectName nvarchar(max), @createSql nvarchar(max), @isChanged bit = 0 OUTPUT, @error bit = 0 OUTPUT)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @message nvarchar(max) 
	SET @message = @objectName + ' has not changed. Not recreating.'
	DECLARE @definition nvarchar(max) 
	SET @definition = LTRIM(RTRIM(OBJECT_DEFINITION(OBJECT_ID(@objectName))))

	IF @definition IS NULL OR @definition != LTRIM(RTRIM(@createSql))
	BEGIN
		EXEC dbo.DropObject @objectName
		EXEC(@createSql)

		IF (@@error != 0)
			SET @error = 1

		SET @isChanged = 1

		SET @message = @objectName + ' has changed. Recreating.'
	END	

	RAISERROR(@message, 0, 1) WITH NOWAIT
END

GO

EXEC [dbo].[RecreateIfDefinitionChanged] 'model.GetPairId', '
CREATE FUNCTION model.GetPairId(@x int, @y int, @max int = 50000000)
RETURNS int
WITH SCHEMABINDING
AS
BEGIN
	RETURN @max * @x + @y
END
'

EXEC [dbo].[RecreateIfDefinitionChanged] 'model.GetBigPairId', '
CREATE FUNCTION model.GetBigPairId(@x bigint, @y bigint, @max int = 50000000)
RETURNS bigint
WITH SCHEMABINDING
AS
BEGIN
	RETURN @max * @x + @y
END
'

EXEC [dbo].[RecreateIfDefinitionChanged] 'model.GetIdPair', '
CREATE FUNCTION model.GetIdPair(@id int, @max int = 50000000)
RETURNS @results table(x int, y int)
WITH SCHEMABINDING
AS
BEGIN
	DECLARE @y int
	SET @y = @id % @max
	INSERT INTO @results(x, y)
	VALUES ((@id - @y)/@max, @y) 
	RETURN
END
'

EXEC [dbo].[RecreateIfDefinitionChanged] 'model.GetBigIdPair', '
CREATE FUNCTION model.GetBigIdPair(@id bigint, @max int = 50000000)
RETURNS @results table(x bigint, y bigint)
WITH SCHEMABINDING
AS
BEGIN
	DECLARE @y bigint
	SET @y = @id % @max
	INSERT INTO @results(x, y)
	VALUES ((@id - @y)/@max, @y)
	RETURN
END
'