﻿CREATE PROCEDURE [dbo].[DropIndexes](@objectName nvarchar(max))
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @dropIndex nvarchar(max) 
	SET @dropIndex = (
	SELECT TOP 1 'DROP INDEX [' + sys.indexes.name + '] ON [' + sys.schemas.name + '].[' + sys.objects.name + '] ' FROM
	sys.indexes
	JOIN 
		sys.objects 
		ON sys.indexes.object_id = sys.objects.object_id
	JOIN 
		sys.schemas
		ON sys.schemas.schema_id = sys.objects.schema_id
	WHERE sys.indexes.type = 1 AND OBJECT_ID(@objectName) = sys.objects.object_id
	)
		
	EXEC(@dropIndex)
END
GO

CREATE PROCEDURE dbo.DisableEnabledTriggers(@objectName nvarchar(max), @names xml OUTPUT)
AS
BEGIN
	DECLARE @triggers table(Name nvarchar(max))
	INSERT @triggers
	SELECT '[' + s.name + '].[' + t.name + ']' AS Name
	FROM sys.triggers t 
	INNER JOIN sys.objects o ON o.object_id = t.object_id
	INNER JOIN sys.schemas s ON s.schema_id = o.schema_id
	WHERE t.parent_id = OBJECT_ID(@objectName)
		AND t.is_disabled = 0

	DECLARE @sql nvarchar(max)

	DECLARE c CURSOR FOR
	SELECT 'DISABLE TRIGGER ' + Name + ' ON ' + @objectName FROM @triggers
	OPEN c
	FETCH NEXT FROM c INTO @sql
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC(@sql)

		FETCH NEXT FROM c INTO @sql
	END
	CLOSE c
	DEALLOCATE c	
	
	SET @names = (
		SELECT Name FROM @triggers
		FOR XML PATH('')
	)
END
GO

CREATE PROCEDURE dbo.EnableTriggers(@names xml)
AS
BEGIN
	DECLARE @sql nvarchar(max)

	DECLARE c CURSOR FOR
	SELECT 'ENABLE TRIGGER ' + n.Id.value('.', 'nvarchar(max)') + ' ON [' + s.name + '].[' + po.name + ']'
	FROM @names.nodes('//*') as n(Id)
	INNER JOIN sys.objects o ON o.object_id = OBJECT_ID(n.Id.value('.', 'nvarchar(max)'))
	INNER JOIN sys.triggers t ON t.object_id = o.object_id
	INNER JOIN sys.objects po ON po.object_id = t.parent_id
	INNER JOIN sys.schemas s ON s.schema_id = po.schema_id
	WHERE t.object_id IS NOT NULL
	OPEN c
	FETCH NEXT FROM c INTO @sql
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC(@sql)

		FETCH NEXT FROM c INTO @sql
	END
	CLOSE c
	DEALLOCATE c	
	
	RETURN
END
GO

CREATE PROCEDURE dbo.DisableEnabledCheckConstraints(@tableName nvarchar(max), @names xml OUTPUT)
AS
BEGIN
	DECLARE @constraints table(SchemaName nvarchar(max), ConstraintName nvarchar(max))
	INSERT @constraints
	SELECT s.name, c.name
	FROM sys.check_constraints c
	INNER JOIN sys.objects o ON o.object_id = c.object_id
	INNER JOIN sys.schemas s ON s.schema_id = o.schema_id
	WHERE c.parent_object_id = OBJECT_ID(@tableName)
		AND c.is_disabled = 0

	DECLARE @sql nvarchar(max)

	DECLARE c CURSOR FOR
	SELECT 'ALTER TABLE ' + @tableName + ' NOCHECK CONSTRAINT ' + ConstraintName
	FROM @constraints
	OPEN c
	FETCH NEXT FROM c INTO @sql
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC(@sql)

		FETCH NEXT FROM c INTO @sql
	END
	CLOSE c
	DEALLOCATE c	
	
	SET @names = (
		SELECT '[' + SchemaName + '].[' + ConstraintName + ']' AS Name FROM @constraints
		FOR XML PATH('')
	)
END
GO

CREATE PROCEDURE dbo.EnableCheckConstraints(@names xml)
AS
BEGIN
	DECLARE @sql nvarchar(max)

	DECLARE c CURSOR FOR
	SELECT 'ALTER TABLE [' + s.name + '].[' + po.name + '] CHECK CONSTRAINT ' + c.name
	FROM @names.nodes('//*') as n(Id)
	INNER JOIN sys.objects o ON o.object_id = OBJECT_ID(n.Id.value('.', 'nvarchar(max)'))
	INNER JOIN sys.check_constraints c ON c.object_id = o.object_id
	INNER JOIN sys.objects po ON po.object_id = c.parent_object_id
	INNER JOIN sys.schemas s ON s.schema_id = po.schema_id
	WHERE c.object_id IS NOT NULL
	OPEN c
	FETCH NEXT FROM c INTO @sql
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC(@sql)

		FETCH NEXT FROM c INTO @sql
	END
	CLOSE c
	DEALLOCATE c	
	
	RETURN
END
GO
