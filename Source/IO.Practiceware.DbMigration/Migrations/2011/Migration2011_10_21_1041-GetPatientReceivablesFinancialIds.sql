﻿DECLARE @triggerNames xml
EXEC dbo.DisableEnabledTriggers 'dbo.PatientReceivables', @names = @triggerNames OUTPUT

DECLARE @constraintNames xml
EXEC dbo.DisableEnabledCheckConstraints 'dbo.PatientReceivables', @names = @constraintNames OUTPUT

EXEC dbo.DropIndexes 'model.Adjustments_Partition1'
EXEC dbo.DropIndexes 'model.BillingServiceBillingDiagnosis_Partition1'
EXEC dbo.DropIndexes 'model.BillingServices_Partition1'
EXEC dbo.DropIndexes 'model.BillingServiceTransactions_Internal'
EXEC dbo.DropIndexes 'model.Comments_Partition8'
EXEC dbo.DropIndexes 'model.FinancialInformations_Internal'
EXEC dbo.DropIndexes 'model.InvoiceReceivables_Partition1'
EXEC dbo.DropIndexes 'model.InvoiceReceivables_Partition2'
EXEC dbo.DropIndexes 'model.InvoiceReceivables_Partition3'
EXEC dbo.DropIndexes 'model.InvoiceReceivables_Partition5'
EXEC dbo.DropIndexes 'model.PatientInsurances_Partition1'
EXEC dbo.DropIndexes 'model.PatientInsuranceAuthorizations_Internal'

IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'PracticeInsurers' AND sc.Name = 'AllowDependents')
BEGIN
    ALTER TABLE PracticeInsurers
    ADD AllowDependents bit
END

IF OBJECT_ID('PracticeInsurersAllowDependentsTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeInsurersAllowDependentsTrigger')
END

EXEC('
UPDATE PracticeInsurers
SET AllowDependents = CASE WHEN OutOfPocket = 0 THEN 1 ELSE 0 END
')

EXEC('CREATE TRIGGER PracticeInsurersAllowDependentsTrigger
ON PracticeInsurers
FOR INSERT, UPDATE 
NOT FOR REPLICATION
AS
    SET NOCOUNT ON;
	UPDATE PracticeInsurers
	SET AllowDependents = CASE WHEN OutOfPocket = 0 THEN 1 ELSE 0 END
	WHERE InsurerId IN (SELECT InsurerId FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i) AND (AllowDependents IS NULL OR AllowDependents <> (CASE WHEN OutOfPocket = 0 THEN 1 ELSE 0 END))
	')



IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'PatientReceivablePayments' AND sc.Name = 'PaymentAmountDecimal')
BEGIN
    ALTER TABLE PatientReceivablePayments
    ADD PaymentAmountDecimal decimal(12, 2)
END

IF OBJECT_ID('PatientReceivablePaymentsPaymentAmount') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablePaymentsPaymentAmount')
END

IF OBJECT_ID('PatientReceivablePaymentsPaymentAmountTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablePaymentsPaymentAmountTrigger')
END


EXEC('
UPDATE PatientReceivablePayments
SET PaymentAmountDecimal = CONVERT(decimal(12, 2), PaymentAmount)
WHERE PaymentAmount < 9999999999.99 AND (PaymentAmount <> PaymentAmountDecimal OR PaymentAmountDecimal IS NULL)
')

EXEC('CREATE TRIGGER PatientReceivablePaymentsPaymentAmountTrigger
ON PatientReceivablePayments
FOR INSERT, UPDATE 
NOT FOR REPLICATION
AS
    SET NOCOUNT ON
    UPDATE PatientReceivablePayments
	SET PaymentAmountDecimal = PaymentAmount
	WHERE PaymentId IN (SELECT PaymentId FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i) AND (PaymentAmountDecimal IS NULL OR PaymentAmountDecimal <> PaymentAmount)
')

IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'PatientReceivables' AND sc.Name = 'ComputedFinancialId')
BEGIN
    ALTER TABLE dbo.PatientReceivables
    ADD ComputedFinancialId int
END

IF OBJECT_ID('dbo.GetPatientReceivablesFinancialIds') IS NOT NULL
	EXEC('DROP FUNCTION dbo.GetPatientReceivablesFinancialIds')

IF OBJECT_ID('dbo.GetBulkPatientReceivablesFinancialIds') IS NOT NULL
	EXEC('DROP FUNCTION dbo.GetBulkPatientReceivablesFinancialIds')
	

DECLARE @createGetPatientReceivablesFinancialIds nvarchar(max)
SET @createGetPatientReceivablesFinancialIds = '
	CREATE FUNCTION dbo.GetPatientReceivablesFinancialIds(@idsXml xml)
	RETURNS @financialIds table(Id int PRIMARY KEY CLUSTERED, FinancialId int)
	AS
	BEGIN
		
	DECLARE @ids table(Id int PRIMARY KEY CLUSTERED)

	INSERT @ids(Id)
	SELECT DISTINCT n.Id.value(''.'', ''int'') 
	FROM @idsXml.nodes(''//Id'') as n(Id)

	INSERT @financialIds(Id, FinancialId)
	-- This is for valid policies
		--Patient is policyholder

	SELECT pr.ReceivableId AS Id, MAX(pf.FinancialId) AS FinancialId
	FROM dbo.PatientReceivables pr WITH(NOLOCK)
	INNER JOIN @ids i ON pr.ReceivableId = i.Id
	INNER JOIN dbo.PatientDemographics pd  WITH(NOLOCK) ON pr.PatientId = pd.PatientId
	INNER JOIN dbo.PatientFinancial pf WITH(NOLOCK) ON pr.InsurerId = pf.FinancialInsurerId
	WHERE dbo.IsDate112(pf.FinancialStartDate) = 1
	AND pr.InvoiceDate >= pf.FinancialStartDate
	AND (
		pr.InvoiceDate <= pf.FinancialEndDate
		OR pf.FinancialEndDate = ''''
		OR pf.FinancialEndDate IS NULL
		)
	AND pr.InsuredId = pf.PatientId
	AND pf.PatientId = pd.PatientId
	AND pf.PatientId = pd.PolicyPatientId
	GROUP BY pr.ReceivableId
				
	UNION 

	SELECT pr.ReceivableId AS Id, MAX(pf.FinancialId) AS FinancialId
	FROM dbo.PatientReceivables pr WITH(NOLOCK)
	INNER JOIN @ids i ON pr.ReceivableId = i.Id
	INNER JOIN dbo.PatientDemographics pd WITH(NOLOCK) ON pr.PatientId = pd.PatientId
	INNER JOIN dbo.PatientFinancial pf WITH(NOLOCK) ON pr.InsurerId = pf.FinancialInsurerId
	WHERE dbo.IsDate112(pf.FinancialStartDate) = 1
	AND pr.InvoiceDate >= pf.FinancialStartDate
	AND (
		pr.InvoiceDate <= pf.FinancialEndDate
		OR pf.FinancialEndDate = ''''
		OR pf.FinancialEndDate IS NULL
		)
	AND pr.InsuredId = pf.PatientId
	AND pf.PatientId = pd.PatientId
	AND pf.PatientId = pd.SecondPolicyPatientId
	GROUP BY pr.ReceivableId
				
	UNION 

	--First policyholder is not the patient
	SELECT pr.ReceivableId AS Id, MAX(pf.FinancialId) AS FinancialId
	FROM dbo.PatientReceivables pr WITH(NOLOCK)
	INNER JOIN @ids i ON pr.ReceivableId = i.Id
	INNER JOIN dbo.PatientDemographics pd WITH(NOLOCK) ON pr.PatientId = pd.PatientId
	INNER JOIN dbo.PatientFinancial pf WITH(NOLOCK) ON pr.InsurerId = pf.FinancialInsurerId
	INNER JOIN dbo.PracticeInsurers pri WITH(NOLOCK) ON pri.InsurerId = pr.InsurerId
	WHERE dbo.IsDate112(pf.FinancialStartDate) = 1
	AND pr.InsuredId = pf.PatientId
	AND pr.InvoiceDate >= pf.FinancialStartDate
	AND (
		pr.InvoiceDate <= pf.FinancialEndDate
		OR pf.FinancialEndDate = ''''
		OR pf.FinancialEndDate IS NULL
		)
	AND pd.PolicyPatientId <> pd.PatientId
	AND pd.PolicyPatientId = pf.PatientId
	AND pd.PolicyPatientId <> 0
	AND pri.AllowDependents = 1
	GROUP BY pr.ReceivableId
		
	UNION 

	--Second policyholder is not the patient
	SELECT pr.ReceivableId AS Id, MAX(pf.FinancialId) AS FinancialId
	FROM dbo.PatientReceivables pr WITH(NOLOCK)
	INNER JOIN @ids i ON pr.ReceivableId = i.Id
	INNER JOIN dbo.PatientDemographics pd WITH(NOLOCK) ON pr.PatientId = pd.PatientId
	INNER JOIN dbo.PatientFinancial pf WITH(NOLOCK) ON pr.InsurerId = pf.FinancialInsurerId
	INNER JOIN dbo.PracticeInsurers pri WITH(NOLOCK) ON pri.InsurerId = pr.InsurerId
	WHERE dbo.IsDate112(pf.FinancialStartDate) = 1
	AND pr.InsuredId = pf.PatientId
	AND pr.InvoiceDate >= pf.FinancialStartDate
	AND (
		pr.InvoiceDate <= pf.FinancialEndDate
		OR pf.FinancialEndDate = ''''
		OR pf.FinancialEndDate IS NULL
		)
	AND pd.SecondPolicyPatientId <> pd.PatientId
	AND pd.SecondPolicyPatientId = pf.PatientId
	AND pd.SecondPolicyPatientId <> 0
	AND pri.AllowDependents = 1
	GROUP BY pr.ReceivableId
	
			
	DELETE @ids
	WHERE Id IN (SELECT Id FROM @financialIds)

	--This is for receivables that were billed
	INSERT @financialIds(Id, FinancialId)
	SELECT pr.ReceivableId, MAX(pf.FinancialId)
	FROM dbo.PatientReceivables pr WITH(NOLOCK)
	INNER MERGE JOIN @ids i ON pr.ReceivableId = i.Id
	INNER JOIN dbo.PatientFinancial pf WITH(NOLOCK) ON pf.PatientId = pr.PatientId AND pf.FinancialInsurerId = pr.InsurerId
	INNER JOIN dbo.PracticeTransactionJournal ptj WITH(NOLOCK) ON ptj.TransactionTypeId = pr.ReceivableId
	INNER JOIN dbo.ServiceTransactions st WITH(NOLOCK) ON st.TransactionId = ptj.TransactionId
	WHERE ptj.TransactionType = ''R'' 
		AND st.TransportAction IN (''1'', ''2'')
		AND dbo.IsDate112(pf.FinancialStartDate) = 1
	GROUP BY pr.ReceivableId
									
	DELETE @ids
	WHERE Id IN (SELECT Id FROM @financialIds)

	INSERT @financialIds(Id, FinancialId)
	-- This is for payments where the policy is invalid
	SELECT pr.ReceivableId, MAX(pf.FinancialId)
	FROM dbo.PatientReceivables pr WITH(NOLOCK)
	INNER MERGE JOIN @ids i ON pr.ReceivableId = i.Id
	INNER JOIN dbo.PatientFinancial pf WITH(NOLOCK) ON pf.PatientId = pr.InsuredId AND pf.FinancialInsurerId = pr.InsurerId
	INNER JOIN dbo.PatientReceivablePayments prp WITH(NOLOCK) ON prp.ReceivableId = pr.ReceivableId
	WHERE dbo.IsDate112(pf.FinancialStartDate) = 1
	GROUP BY pr.ReceivableId
		
	DELETE @ids
	WHERE Id IN (SELECT Id FROM @financialIds)
		
	INSERT @financialIds(Id, FinancialId)
	SELECT i.Id, NULL
	FROM @ids i
		
	RETURN
END	
'

EXEC(@createGetPatientReceivablesFinancialIds)

DECLARE @createGetBulkPatientReceivablesFinancialIds nvarchar(max)
SET @createGetBulkPatientReceivablesFinancialIds = REPLACE(REPLACE(@createGetPatientReceivablesFinancialIds, 'INNER JOIN', 'INNER HASH JOIN'), 'GetPatientReceivablesFinancialIds', 'GetBulkPatientReceivablesFinancialIds')

EXEC (@createGetBulkPatientReceivablesFinancialIds)

IF OBJECT_ID('PatientReceivablesFinancialId') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablesFinancialId')
END

IF OBJECT_ID('PatientReceivablesFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablesFinancialIdTrigger')
END

IF OBJECT_ID('PatientFinancialPatientReceivablesFinancialId') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientFinancialPatientReceivablesFinancialId')
END

IF OBJECT_ID('PatientFinancialPatientReceivablesFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientFinancialPatientReceivablesFinancialIdTrigger')
END

EXEC('
DECLARE @xml xml
SET @xml = (
	SELECT ReceivableId AS Id
	FROM PatientReceivables
	FOR XML PATH('''')
)

UPDATE pr
SET pr.ComputedFinancialId = prpf.FinancialId
FROM PatientReceivables pr
INNER JOIN 
(
	SELECT Id, FinancialId 
	FROM
	dbo.GetBulkPatientReceivablesFinancialIds(@xml)
) prpf
ON prpf.Id = pr.ReceivableId 
')

EXEC('CREATE TRIGGER [dbo].[PatientReceivablesFinancialIdTrigger]
ON [dbo].[PatientReceivables]
FOR INSERT, UPDATE
NOT FOR REPLICATION
AS
SET NOCOUNT ON;

DECLARE @changedIds table(Id int)
INSERT @changedIds
SELECT ReceivableId FROM
(SELECT * FROM inserted EXCEPT SELECT * FROM deleted) c

IF EXISTS(SELECT * FROM @changedIds) AND dbo.GetNoRecompute() = 0
BEGIN
	SELECT 1 AS Value INTO #NoRecompute

	DECLARE @xml xml
	SET @xml = (
		SELECT Id AS Id
		FROM @changedIds
		FOR XML PATH('''')
	)

	SELECT Id, FinancialId 
	INTO #financialIds
	FROM dbo.GetPatientReceivablesFinancialIds(@xml) f
	JOIN PatientReceivables pr WITH(NOLOCK) ON f.Id = pr.ReceivableId
	WHERE COALESCE(ComputedFinancialId, '''') <> COALESCE(f.FinancialId, '''')
	
	IF EXISTS(SELECT * FROM #financialIds)	
	BEGIN
		EXEC(''
		UPDATE pr
		SET pr.ComputedFinancialId = f.FinancialId
		FROM #financialIds f
		JOIN PatientReceivables pr ON f.Id = pr.ReceivableId'')
	END
	
	DROP TABLE #financialIds
			
	DROP TABLE #NoRecompute
END
')

EXEC('CREATE TRIGGER [dbo].[PatientFinancialPatientReceivablesFinancialIdTrigger]
ON [dbo].[PatientFinancial]
FOR INSERT, UPDATE
NOT FOR REPLICATION
AS
SET NOCOUNT ON;


DECLARE @changedIds table(Id int, UNIQUE CLUSTERED(Id))
INSERT @changedIds
SELECT DISTINCT PatientId FROM
(SELECT * FROM inserted EXCEPT SELECT * FROM deleted) c

IF EXISTS(SELECT * FROM @changedIds) AND dbo.GetNoRecompute() = 0
BEGIN
	SELECT 1 AS Value INTO #NoRecompute

	DECLARE @xml xml
	SET @xml = (
		SELECT pr.ReceivableId AS Id
		FROM @changedIds i
		INNER JOIN PatientReceivables pr ON i.Id = pr.InsuredId
		FOR XML PATH('''')
	)

	SELECT Id, FinancialId 
	INTO #financialIds
	FROM dbo.GetPatientReceivablesFinancialIds(@xml)

	IF EXISTS (SELECT * FROM #financialIds)
	BEGIN
		EXEC(''
		UPDATE pr
		SET pr.ComputedFinancialId = f.FinancialId
		FROM #financialIds f
		JOIN PatientReceivables pr ON f.Id = pr.ReceivableId 	
		WHERE COALESCE(ComputedFinancialId, '''''''') <> COALESCE(f.FinancialId, '''''''')
		'')
	END

	DROP TABLE #financialIds
    
	DROP TABLE #NoRecompute
END
')

IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'PatientReceivableServices' AND sc.Name = 'QuantityDecimal')
BEGIN
    ALTER TABLE PatientReceivableServices
    ADD QuantityDecimal decimal(12, 2)
END

IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'PatientReceivableServices' AND sc.Name = 'ChargeDecimal')
BEGIN
    ALTER TABLE PatientReceivableServices
    ADD ChargeDecimal decimal(12, 2)
END

IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'PatientReceivableServices' AND sc.Name = 'FeeChargeDecimal')
BEGIN
    ALTER TABLE PatientReceivableServices
    ADD FeeChargeDecimal decimal(12, 2)
END

EXEC('
UPDATE PatientReceivableServices
SET 
QuantityDecimal = Quantity,
ChargeDecimal = Charge,
FeeChargeDecimal = FeeCharge
WHERE QuantityDecimal <> Quantity OR ChargeDecimal <> Charge OR FeeChargeDecimal <> FeeCharge OR QuantityDecimal IS NULL OR ChargeDecimal IS NULL OR FeeChargeDecimal IS NULL
')

IF OBJECT_ID('PatientReceivableServicesQuantityCharge') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivableServicesQuantityCharge')
END

IF OBJECT_ID('PatientReceivableServicesQuantityChargeTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivableServicesQuantityChargeTrigger')
END

EXEC('CREATE TRIGGER PatientReceivableServicesQuantityChargeTrigger
ON PatientReceivableServices
FOR INSERT, UPDATE 
NOT FOR REPLICATION
AS
    SET NOCOUNT ON;
	    UPDATE PatientReceivableServices
	    SET 
            QuantityDecimal = Quantity,
            ChargeDecimal = Charge,
            FeeChargeDecimal = FeeCharge
	    WHERE ItemId IN (SELECT ItemId FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i)
            AND (
                (QuantityDecimal IS NULL OR QuantityDecimal <> Quantity) 
                OR (ChargeDecimal IS NULL OR ChargeDecimal <> Charge) 
                OR (FeeChargeDecimal IS NULL OR FeeChargeDecimal <> FeeCharge)
            )
	    ')

EXEC dbo.EnableTriggers @triggerNames
EXEC dbo.EnableCheckConstraints @constraintNames