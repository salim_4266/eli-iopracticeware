﻿--Found that some clients have a different ExternalSystemEntityMapping table than what is defined in the previous migration

ALTER TABLE model.ExternalSystemEntityMappings
	ALTER COLUMN ExternalSystemEntityKey nvarchar(100)  NULL