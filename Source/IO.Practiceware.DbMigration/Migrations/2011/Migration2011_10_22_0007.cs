﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Creates the PracticeRepositoryEntities table.
    /// </summary>
    [Migration(201110220007)]
    public class Migration201110220007 : Migration
    {
        public override void Up()
        {
            Execute.Sql(
@"CREATE TABLE [model].[PracticeRepositoryEntities] (
    [Id] int NOT NULL,
    [Name] nvarchar(250)  NOT NULL,
    [KeyPropertyName] nvarchar(250)  NOT NULL)
GO

-- Creating primary key on [Id] in table 'PracticeRepositoryEntities'
ALTER TABLE [model].[PracticeRepositoryEntities]
ADD CONSTRAINT [PK_PracticeRepositoryEntities]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
");
            Execute.Sql(@"
INSERT [model].[PracticeRepositoryEntities] ([Id], [Name], [KeyPropertyName]) VALUES (1, N'Appointment', N'Id')
INSERT [model].[PracticeRepositoryEntities] ([Id], [Name], [KeyPropertyName]) VALUES (2, N'AppointmentType', N'Id')
INSERT [model].[PracticeRepositoryEntities] ([Id], [Name], [KeyPropertyName]) VALUES (3, N'Patient', N'Id')
INSERT [model].[PracticeRepositoryEntities] ([Id], [Name], [KeyPropertyName]) VALUES (4, N'PatientInsurance', N'Id')
INSERT [model].[PracticeRepositoryEntities] ([Id], [Name], [KeyPropertyName]) VALUES (5, N'Insurer', N'Id')
INSERT [model].[PracticeRepositoryEntities] ([Id], [Name], [KeyPropertyName]) VALUES (6, N'User', N'Id')
INSERT [model].[PracticeRepositoryEntities] ([Id], [Name], [KeyPropertyName]) VALUES (7, N'Contact', N'Id')
INSERT [model].[PracticeRepositoryEntities] ([Id], [Name], [KeyPropertyName]) VALUES (8, N'ServiceLocation', N'Id') 
");
        }

        public override void Down()
        {
        }
    }
}