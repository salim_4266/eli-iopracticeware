﻿IF NOT EXISTS(SELECT name FROM sysindexes where name = 'IX_PatientFinancial_PatientId') 
CREATE NONCLUSTERED INDEX [IX_PatientFinancial_PatientId] ON [dbo].[PatientFinancial]
(
	[PatientId] ASC,
	[FinancialInsurerId] ASC,
	[FinancialStartDate] ASC,
	[FinancialEndDate] ASC
)
INCLUDE ([FinancialId])

IF NOT EXISTS(SELECT name FROM sysindexes where name = 'IX_PatientReceivables_InsurerId') 
CREATE NONCLUSTERED INDEX IX_PatientReceivables_InsurerId
ON PatientReceivables(InsurerId) INCLUDE (ReceivableId, AppointmentId, ExternalRefInfo, ComputedFinancialId)      

IF NOT EXISTS(SELECT name FROM sysindexes where name = 'IX_PatientReceivables_AppointmentId') 
CREATE NONCLUSTERED INDEX IX_PatientReceivables_AppointmentId
ON PatientReceivables(AppointmentId) INCLUDE (ReceivableId, InsurerId, ExternalRefInfo, ComputedFinancialId)          

IF NOT EXISTS(SELECT name FROM sysindexes where name = 'IX_ServiceTransactions_TransactionId') 
CREATE NONCLUSTERED INDEX IX_ServiceTransactions_TransactionId
ON ServiceTransactions(TransactionId) INCLUDE(TransportAction)

IF NOT EXISTS(SELECT name FROM sysindexes where name = 'IX_PatientReceivables_Invoice') 
CREATE NONCLUSTERED INDEX IX_PatientReceivables_Invoice
ON PatientReceivables (Invoice)