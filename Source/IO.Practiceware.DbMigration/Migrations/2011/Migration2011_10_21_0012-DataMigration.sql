﻿-- MORE Migrate ambiguous data in  PracticeActivity , PracticeTransactionJournal

--Dud insurance policies
IF OBJECT_ID('dbo.PatientFinancial','U') IS NOT NULL
BEGIN
	DELETE FROM PatientFinancial
	WHERE PatientId = 0
		OR FinancialInsurerId = 0
END

--Try to recover messed up activity records
UPDATE pa
SET AppointmentId = ap.AppointmentId
FROM PracticeActivity pa
INNER JOIN Appointments ap ON pa.PatientId = ap.PatientId
	AND pa.ActivityDate = ap.AppDate
WHERE (pa.AppointmentId = 0)
	AND pa.STATUS <> 'x'
	AND pa.patientid <> 0
	AND ap.AppointmentId NOT IN (
		SELECT AppointmentId
		FROM PracticeActivity
		WHERE appointmentid <> 0
		)
	AND ap.ScheduleStatus = pa.STATUS

--Clean up Practice Activity with blank dates
UPDATE pa
SET pa.ActivityDate = ap.Appdate
FROM PracticeActivity pa
INNER JOIN Appointments ap ON ap.AppointmentId = pa.AppointmentId
WHERE pa.ActivityDate = 0
	OR pa.ActivityDate = ''
	
--Cleanup PracticeActivity with no AppointmentId (cancelled appointments)
DELETE FROM PracticeActivity
WHERE PatientId <> 0
	AND AppointmentId = 0 
	AND [Status] = 'X'

--Cleanup PracticeActivity with no PatientId
DELETE FROM PracticeActivity
WHERE (PatientId = 0 OR PatientId IS NULL)
AND (AppointmentId = 0 OR AppointmentId IS NULL)


--Try to fix messed up claim transactions
UPDATE ptj
SET TransactionDate = SUBSTRING(TransactionBatch, CHARINDEX('-', TransactionBatch) + 1, 8)
FROM PracticeTransactionJournal ptj
WHERE TransactionType = 'R'
	AND isdate(TransactionDate) = 0

