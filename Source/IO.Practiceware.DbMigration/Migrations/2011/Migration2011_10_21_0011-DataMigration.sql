﻿-- Migrate ambiguous data in Resources, PatientDemographics, PatientFinancial, Appointments, PracticeActivity


----Resources
--Check the resource table for resource type B
--If B for Biller, change to A for Admin
UPDATE re SET ResourceType = 'A'
FROM Resources re
INNER JOIN PracticeCodeTable pct ON pct.ReferenceType = 'ResourceType' and pct.Code LIKE 'Bill%'
WHERE re.ResourceType = 'B'

--If the B isn't a Biller, we better check it first.
IF EXISTS (SELECT ResourceId FROM Resources WHERE ResourceType = 'B')
BEGIN
RAISERROR('Resource table has a resource type of B, please fix and then re-run.',16, 1)
END


--Fan out archived ResourceType Zs into more specific X (non-doctor), Y (optical supplier) and Z (doctor)
UPDATE re
SET re.ResourceType = CASE 
	WHEN ap.AppointmentId IS NULL
		THEN 'X'
	WHEN re.ResourceId = ap.ResourceId1 AND ap.Comments = 'ADD VIA OPTICAL'
		THEN 'Y'
	WHEN re.ResourceId = ap.ResourceId1
		THEN 'Z'
	END
FROM dbo.Resources re
LEFT JOIN dbo.Appointments ap ON ap.ResourceId1 = re.ResourceId
WHERE ResourceType = 'Z' AND ap.ScheduleStatus = 'D'


--Resource Type = 'T's with a discharged appointment - copy to a new Resource Row with ResourceType T; update ResourceType of original row to Z and remove PID. 
IF EXISTS (SELECT DISTINCT re.ResourceId, re.[ResourceName], 'T' AS ResourceType, re.[ResourceDescription], re.[ResourcePid], re.[ResourcePermissions], re.[ResourceColor], re.[ResourceOverLoad], re.[ResourceCost], re.[ResourceSpecialty], re.[ResourceTaxId], re.[ResourceAddress], re.[ResourceSuite], re.[ResourceCity], re.[ResourceState], re.[ResourceZip], re.[ResourcePhone], re.[ResourceEmail], re.[ServiceCode], re.[ResourceSSN], re.[ResourceDEA], re.[ResourceLicence], re.[ResourceUPIN], re.[ResourceScheduleTemplate1], re.[ResourceScheduleTemplate2], re.[ResourceScheduleTemplate3], re.[PracticeId], re.[StartTime1], re.[EndTime1], re.[StartTime2], re.[EndTime2], re.[StartTime3], re.[EndTime3], re.[StartTime4], re.[EndTime4], re.[StartTime5], re.[EndTime5], re.[StartTime6], re.[EndTime6], re.[StartTime7], re.[EndTime7], re.[Vacation], re.[Status], re.[ResourceLastName], re.[ResourceFirstName], re.[SignatureFile], re.[PlaceOfService], re.[Billable], re.[GoDirect], re.[NPI], re.[ResourceMI], re.[ResourceSuffix], re.[ResourcePrefix]
FROM Resources re
LEFT JOIN Appointments ap ON ap.ResourceId1 = re.ResourceId 
LEFT JOIN PatientReceivables pr ON pr.BillToDr = re.ResourceId
WHERE (ap.ScheduleStatus = 'D' OR pr.ReceivableId IS NOT NULL)
	AND ResourceType NOT IN ('D', 'Q', 'Z', 'Y', 'R'))

BEGIN

	SELECT DISTINCT re.ResourceId, re.[ResourceName], 'T' AS ResourceType, re.[ResourceDescription], re.[ResourcePid], re.[ResourcePermissions], re.[ResourceColor], re.[ResourceOverLoad], re.[ResourceCost], re.[ResourceSpecialty], re.[ResourceTaxId], re.[ResourceAddress], re.[ResourceSuite], re.[ResourceCity], re.[ResourceState], re.[ResourceZip], re.[ResourcePhone], re.[ResourceEmail], re.[ServiceCode], re.[ResourceSSN], re.[ResourceDEA], re.[ResourceLicence], re.[ResourceUPIN], re.[ResourceScheduleTemplate1], re.[ResourceScheduleTemplate2], re.[ResourceScheduleTemplate3], re.[PracticeId], re.[StartTime1], re.[EndTime1], re.[StartTime2], re.[EndTime2], re.[StartTime3], re.[EndTime3], re.[StartTime4], re.[EndTime4], re.[StartTime5], re.[EndTime5], re.[StartTime6], re.[EndTime6], re.[StartTime7], re.[EndTime7], re.[Vacation], re.[Status], re.[ResourceLastName], re.[ResourceFirstName], re.[SignatureFile], re.[PlaceOfService], re.[Billable], re.[GoDirect], re.[NPI], re.[ResourceMI], re.[ResourceSuffix], re.[ResourcePrefix]
	INTO #tempResource
	FROM Resources re
	LEFT JOIN Appointments ap ON ap.ResourceId1 = re.ResourceId 
	LEFT JOIN PatientReceivables pr ON pr.BillToDr = re.ResourceId
	WHERE (ap.ScheduleStatus = 'D' OR pr.ReceivableId IS NOT NULL)
		AND ResourceType NOT IN ('D', 'Q', 'Z', 'Y', 'R')
	
	INSERT INTO dbo.Resources  ([ResourceName], [ResourceType], [ResourceDescription], [ResourcePid], [ResourcePermissions], [ResourceColor], [ResourceOverLoad], [ResourceCost], [ResourceSpecialty], [ResourceTaxId], [ResourceAddress], [ResourceSuite], [ResourceCity], [ResourceState], [ResourceZip], [ResourcePhone], [ResourceEmail], [ServiceCode], [ResourceSSN], [ResourceDEA], [ResourceLicence], [ResourceUPIN], [ResourceScheduleTemplate1], [ResourceScheduleTemplate2], [ResourceScheduleTemplate3], [PracticeId], [StartTime1], [EndTime1], [StartTime2], [EndTime2], [StartTime3], [EndTime3], [StartTime4], [EndTime4], [StartTime5], [EndTime5], [StartTime6], [EndTime6], [StartTime7], [EndTime7], [Vacation], [Status], [ResourceLastName], [ResourceFirstName], [SignatureFile], [PlaceOfService], [Billable], [GoDirect], [NPI], [ResourceMI], [ResourceSuffix], [ResourcePrefix]) 
	SELECT [ResourceName], [ResourceType], [ResourceDescription], [ResourcePid], [ResourcePermissions], [ResourceColor], [ResourceOverLoad], [ResourceCost], [ResourceSpecialty], [ResourceTaxId], [ResourceAddress], [ResourceSuite], [ResourceCity], [ResourceState], [ResourceZip], [ResourcePhone], [ResourceEmail], [ServiceCode], [ResourceSSN], [ResourceDEA], [ResourceLicence], [ResourceUPIN], [ResourceScheduleTemplate1], [ResourceScheduleTemplate2], [ResourceScheduleTemplate3], [PracticeId], [StartTime1], [EndTime1], [StartTime2], [EndTime2], [StartTime3], [EndTime3], [StartTime4], [EndTime4], [StartTime5], [EndTime5], [StartTime6], [EndTime6], [StartTime7], [EndTime7], [Vacation], [Status], [ResourceLastName], [ResourceFirstName], [SignatureFile], [PlaceOfService], [Billable], [GoDirect], [NPI], [ResourceMI], [ResourceSuffix], [ResourcePrefix]  FROM #tempResource

	UPDATE Resources
	SET ResourceType = 'Z', ResourcePid = 'xxxx'
	WHERE ResourceId IN (SELECT DISTINCT ResourceId 
	FROM Resources re
	LEFT JOIN Appointments ap ON ap.ResourceId1 = re.ResourceId 
	LEFT JOIN PatientReceivables pr ON pr.BillToDr = re.ResourceId
	WHERE (ap.ScheduleStatus = 'D' OR pr.ReceivableId IS NOT NULL)
		AND re.ResourceType NOT IN ('D', 'Q', 'Z', 'Y', 'R')
	)
END




-----PatientDemographics
UPDATE dbo.PatientDemographics
SET BirthDate = '19000101'
WHERE dbo.IsDate112(BirthDate) = 0

UPDATE dbo.PatientDemographics
SET Relationship = 'O' 
WHERE PolicyPatientId <> PatientId AND Relationship = 'Y'

UPDATE dbo.PatientDemographics
SET Relationship = 'Y'
FROM PatientDemographics
WHERE PolicyPatientId = PatientId AND Relationship <> 'Y'

UPDATE dbo.PatientDemographics
SET PolicyPatientId = PatientId, Relationship = 'Y'
WHERE PolicyPatientId = 0 OR PolicyPatientId IS NULL OR PolicyPatientId = '' 

-- PolicyPatientId is the same as the SecondPolicyPatientId (should never occur)
UPDATE dbo.PatientDemographics
SET SecondPolicyPatientId = 0
WHERE PolicyPatientId = SecondPolicyPatientId

--Dud financial signature dates
UPDATE PatientDemographics
SET FinancialSignatureDate = ''
WHERE ISDate(FinancialSignatureDate) = 0
AND FinancialSignatureDate <> ''


------PatientDemoAlt
--Deletes saved patient addresses and phone numbers that are not valid patient Ids.
DELETE pda
FROM dbo.PatientDemoAlt pda
LEFT JOIN PatientDemographics pd ON pd.PatientId = pda.PatientId
WHERE pda.PatientId IS NOT NULL AND pd.PatientId IS NULL


---------PatientFinancial
-- Invalid Financial Start Date
UPDATE dbo.PatientFinancial
SET FinancialStartDate = '20000101'
WHERE dbo.IsDate112(FinancialStartDate) = 0

-- Invalid Financial End Date
UPDATE dbo.PatientFinancial
SET FinancialEndDate = '20000101' 
WHERE FinancialEndDate IS NOT NULL AND FinancialEndDate <> '' AND dbo.IsDate112(FinancialEndDate) = 0 

--Remove if patient not present in PatientDemographics
DELETE pf
FROM dbo.PatientFinancial pf
LEFT JOIN PatientDemographics pd ON pf.PatientId = pd.PatientId
WHERE pd.PatientId IS NULL AND pf.PatientId IS NOT NULL


----Fix insurance policies with missing insurers
IF (SELECT COUNT(*) FROM dbo.PatientFinancial pf
LEFT JOIN dbo.PracticeInsurers pins ON pf.FinancialInsurerId = pins.InsurerId
WHERE pins.InsurerId IS NULL) > 0

BEGIN
	IF NOT EXISTS(SELECT * FROM PracticeInsurers WHERE InsurerName = 'ZZZ Deleted Insurer')
		INSERT INTO PracticeInsurers (InsurerName, InsurerBusinessClass) VALUES ('ZZZ Deleted Insurer', 'COMM')

	UPDATE pf
	SET FinancialInsurerId = (SELECT InsurerId FROM PracticeInsurers WHERE InsurerName = 'ZZZ Deleted Insurer')
	FROM PatientFinancial pf
	LEFT JOIN PracticeInsurers pins ON pf.FinancialInsurerId = pins.InsurerId
	WHERE pins.InsurerId IS NULL

END

---archived policies without an enddate where there's another another policy with same insurance type and FinancialPIndicator
UPDATE pf
SET pf.financialenddate = CONVERT(VARCHAR, CONVERT(DATETIME, pf2.FinancialStartDate) - 1, 112)
	,pf.FinancialStartDate = CASE 
		WHEN CONVERT(DATETIME, pf.FinancialStartDate) > CONVERT(DATETIME, pf2.FinancialStartDate) - 1
			THEN CONVERT(VARCHAR, CONVERT(DATETIME, pf.FinancialStartDate) - 1, 112)
		ELSE pf.FinancialStartDate
		END
FROM dbo.PatientFinancial pf
LEFT JOIN PatientFinancial pf2 ON pf.PatientId = pf2.PatientId
	AND pf.FinancialInsType = pf2.FinancialInsType
	AND pf.FinancialPIndicator = pf2.FinancialPIndicator
	AND pf.FinancialId <> pf2.FinancialId
WHERE pf.STATUS = 'x'
	AND pf.FinancialEndDate = ''
	AND pf2.STATUS = 'c'

--deal with the rest of the archived policies where there's no replacement policy
UPDATE dbo.PatientFinancial SET FinancialEndDate = FinancialStartDate
WHERE Status = 'X' 
AND FinancialEndDate = ''


--Remove duplicate policies
----same policyholder, insurer,  insurance type, start date, end date, status
DELETE FROM dbo.PatientFinancial 
WHERE FinancialId IN 
	(SELECT pfDelete.FinancialId FROM PatientFinancial pfDelete
	INNER JOIN PatientFinancial pfKeep ON pfDelete.PatientId = pfKeep.PatientId
		AND pfDelete.FinancialInsurerId = pfKeep.FinancialInsurerId
		AND pfDelete.FinancialInsType = pfKeep.FinancialInsType
		AND pfDelete.FinancialStartDate = pfKeep.FinancialStartDate
		AND pfDelete.FinancialEndDate = pfKeep.FinancialEndDate
		AND pfDelete.[Status] = pfKeep.[Status]
		AND pfDelete.FinancialId < pfKeep.FinancialId
	)


----same policyholder, insurer, policy #, insurance type, date range of deleted within date range of keep
DELETE FROM dbo.PatientFinancial 
WHERE FinancialId IN 
	(SELECT pfDelete.FinancialId FROM PatientFinancial pfDelete
	INNER JOIN PatientFinancial pfKeep ON pfDelete.patientid = pfKeep.patientid
		AND pfDelete.financialinsurerid = pfKeep.financialinsurerid
		AND pfDelete.financialperson = pfKeep.financialperson
		AND pfDelete.financialinstype = pfKeep.financialinstype
		AND pfDelete.FinancialId <> pfKeep.FinancialId
	WHERE pfDelete.financialstartdate >= pfKeep.financialstartdate
		AND (
			(pfDelete.FinancialStartDate <= pfKeep.FinancialEndDate)
			OR (
				pfKeep.FinancialEndDate = ''
				AND pfDelete.FinancialStartDate <> ''
				AND pfDelete.FinancialStartDate IS NOT NULL
				)
			OR (
				pfKeep.FinancialEndDate IS NULL
				AND pfDelete.FinancialStartDate <> ''
				AND pfDelete.FinancialStartDate IS NOT NULL
				)
			)
		AND (
			(
				pfDelete.financialenddate <= pfKeep.financialenddate
				AND pfDelete.FinancialEndDate <> ''
				AND pfDelete.FinancialEndDate IS NOT NULL
				)
			OR pfKeep.financialenddate = ''
			OR pfKeep.financialenddate IS NULL
			)
	)

----same policyholder, insurer,  insurance type, financialpindicator
DELETE FROM dbo.PatientFinancial 
WHERE FinancialId IN 
	(SELECT pfDelete.FinancialId FROM PatientFinancial pfDelete
	INNER JOIN PatientFinancial pfKeep ON pfDelete.patientid = pfKeep.patientid
		AND pfDelete.financialinsurerid = pfKeep.financialinsurerid
		AND pfDelete.financialpindicator = pfKeep.financialpindicator
		AND pfDelete.financialinstype = pfKeep.financialinstype
		AND pfDelete.FinancialId <> pfKeep.FinancialId
	WHERE pfDelete.financialstartdate >= pfKeep.financialstartdate
		AND (
			(pfDelete.FinancialStartDate <= pfKeep.FinancialEndDate)
			OR (
				pfKeep.FinancialEndDate = ''
				AND pfDelete.FinancialStartDate <> ''
				AND pfDelete.FinancialStartDate IS NOT NULL
				)
			OR (
				pfKeep.FinancialEndDate IS NULL
				AND pfDelete.FinancialStartDate <> ''
				AND pfDelete.FinancialStartDate IS NOT NULL
				)
			)
		AND (
				(
				pfDelete.financialenddate <= pfKeep.financialenddate
				AND pfDelete.FinancialEndDate <> ''
				AND pfDelete.FinancialEndDate IS NOT NULL
				)
			OR pfKeep.financialenddate = ''
			OR pfKeep.financialenddate IS NULL
			)
	)

--fix overlapping enddate/startdate of policies for same patient
UPDATE pf SET pf.FinancialStartDate = CASE
		WHEN pf.FinancialStartDate = pf.FinancialEndDate
			THEN CONVERT(varchar,CONVERT(datetime, pf.FinancialStartDate)-1, 112)
		ELSE pf.FinancialStartDate 
		END
, pf.FinancialEndDate = CONVERT(varchar,CONVERT(datetime, pf.FinancialEndDate)-1, 112)
FROM PatientFinancial pf
INNER JOIN PatientFinancial pf2 ON pf.PatientId = pf2.PatientId
	AND pf.FinancialId < pf2.FinancialId
WHERE pf.financialenddate = pf2.FinancialStartDate


--delete policies for same patient/insurer/insurance type where one policy fits entirely within date range of another
DELETE FROM PatientFinancial 
WHERE FinancialId IN 
	(SELECT pfDelete.FinancialId FROM PatientFinancial pfDelete
	INNER JOIN PatientFinancial pfKeep ON pfDelete.PatientId = pfKeep.PatientId
		AND pfDelete.FinancialInsurerId = pfKeep.FinancialInsurerId
		AND pfDelete.FinancialInsType = pfKeep.FinancialInsType
		AND pfDelete.FinancialStartDate >= pfKeep.FinancialStartDate
			AND pfDelete.FinancialStartDate <= pfKeep.FinancialEndDate
		AND pfDelete.FinancialEndDate <= pfKeep.FinancialEndDate
			AND pfDelete.FinancialEndDate >= pfKeep.FinancialStartDate
		AND pfDelete.[Status] = 'X'
		AND pfDelete.FinancialId <> pfKeep.FinancialId
	)

--If two policies match on patient/insurer, insurance type, financialpindicator, and 1 is closed and 1 is current, and the closed one ends the day before the current one starts,
--- and either the policy #s match or the closed policy was active for no more than 2 days, then make the current policy start on the date of the closed policy, and then delete the closed policy.
UPDATE pfUpdate set pfupdate.FinancialStartDate = pfx.FinancialStartDate
FROM PatientFinancial pfUpdate
	INNER JOIN PatientFinancial pfX ON pfUpdate.PatientId = pfX.PatientId
		AND pfUpdate.FinancialInsurerId = pfX.FinancialInsurerId
		AND pfUpdate.FinancialInsType = pfX.FinancialInsType
		AND (pfUpdate.FinancialPerson = pfX.FinancialPerson
			OR CONVERT(datetime,pfX.FinancialStartDate) <= CONVERT(datetime,pfX.FinancialEndDate)-1)
		AND pfUpdate.FinancialPIndicator = pfX.FinancialPIndicator
		AND CONVERT(datetime, pfUpdate.FinancialStartDate) = CONVERT(datetime, pfX.FinancialEndDate)+1
		AND pfUpdate.[Status]='C' AND pfX.[Status] = 'X' 
		AND pfUpdate.FinancialId <> pfX.FinancialId

DELETE FROM PatientFinancial 
WHERE FinancialId IN 
	(SELECT pfDelete.FinancialId FROM PatientFinancial pfDelete
	INNER JOIN PatientFinancial pfKeep ON pfDelete.PatientId = pfKeep.PatientId
		AND pfDelete.FinancialInsurerId = pfKeep.FinancialInsurerId
		AND pfDelete.FinancialInsType = pfKeep.FinancialInsType
		AND pfDelete.FinancialStartDate >= pfKeep.FinancialStartDate
			AND (pfDelete.FinancialStartDate <= pfKeep.FinancialEndDate OR pfKeep.FinancialEndDate = '')
		AND (pfDelete.FinancialEndDate <= pfKeep.FinancialEndDate OR pfKeep.FinancialEndDate = '')
			AND pfDelete.FinancialEndDate >= pfKeep.FinancialStartDate
		AND pfDelete.[Status] = 'X'
		AND pfDelete.FinancialId <> pfKeep.FinancialId
	)



--Find all remaining insurance policies that overlap, causing duplicate ordinals, on receivables with open balances and put them in a temp table
SELECT pd.PatientId AS PatientId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
		WHEN 'M' THEN 10
		WHEN 'V' THEN 20
		WHEN 'A' THEN 30
		WHEN 'W' THEN 40
		ELSE 0
		END
		+ 1) + CONVERT(nvarchar, FinancialPIndicator) AS Ordinal
	, FinancialInsType AS InsuranceType
	, ap.AppointmentId AS AppointmentId
	, pf.PatientId AS Policyholder
INTO #NESTEDPLANS
FROM PatientDemographics pd 
INNER JOIN Appointments ap ON ap.PatientId = pd.PatientId
INNER JOIN PatientFinancial pf ON pd.PolicyPatientId = pf.PatientId 
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
	AND pri.OutOfPocket = 0
	AND pf.Patientid <> pd.PatientId
WHERE pf.FinancialStartDate <= ap.AppDate
	AND (pf.FinancialEndDate >= ap.AppDate OR pf.FinancialEndDate = '' OR pf.FinancialEndDate IS NULL)
	AND ap.AppointmentId IN (SELECT AppointmentId FROM PatientReceivables WHERE OpenBalance > 0)
GROUP BY pd.PatientId
	, ap.AppointmentId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
			WHEN 'M' THEN 10
			WHEN 'V' THEN 20
			WHEN 'A' THEN 30
			WHEN 'W' THEN 40
			ELSE 0
			END
		+ 1) + CONVERT(nvarchar, FinancialPIndicator)
		, FinancialInsType
	 , pf.PatientId
HAVING COUNT(*) > 1

UNION 

SELECT pd.PatientId AS PatientId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
		WHEN 'M' THEN 10
		WHEN 'V' THEN 20
		WHEN 'A' THEN 30
		WHEN 'W' THEN 40
		ELSE 0
		END
		+ 1) + CONVERT(nvarchar, FinancialPIndicator) AS Ordinal
	, FinancialInsType AS InsuranceType
	, ap.AppointmentId AS AppointmentId
	, pf.PatientId AS PolicyHolder
FROM PatientDemographics pd 
INNER JOIN Appointments ap ON pd.PatientId = ap.PatientId
INNER JOIN PatientFinancial pf ON pd.PolicyPatientId = pf.PatientId 
	AND pf.Patientid = pd.PatientId
WHERE pf.FinancialStartDate <= ap.AppDate
	AND (pf.FinancialEndDate >= ap.AppDate OR pf.FinancialEndDate = '' OR pf.FinancialEndDate IS NULL)
	AND ap.AppointmentId IN (SELECT AppointmentId FROM PatientReceivables WHERE OpenBalance > 0)
GROUP BY pd.PatientId
	, ap.AppointmentId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
			WHEN 'M' THEN 10
			WHEN 'V' THEN 20
			WHEN 'A' THEN 30
			WHEN 'W' THEN 40
			ELSE 0
			END
		+ 1) + CONVERT(nvarchar, FinancialPIndicator)
		, FinancialInsType
		, pf.PatientId
HAVING COUNT(*) > 1

UNION

SELECT pd.PatientId AS PatientId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
		WHEN 'M' THEN 10
		WHEN 'V' THEN 20
		WHEN 'A' THEN 30
		WHEN 'W' THEN 40
		ELSE 0
		END + '2') + CONVERT(nvarchar, FinancialPIndicator) AS Ordinal
	, FinancialInsType AS InsuranceType
	, ap.AppointmentId AS AppointmentId
	, pf.PatientId AS PolicyHolder
FROM PatientDemographics pd 
INNER JOIN Appointments ap ON pd.PatientId = ap.PatientId
INNER JOIN PatientFinancial pf ON pd.SecondPolicyPatientId = pf.PatientId AND pd.SecondPolicyPatientId <> 0
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
	AND pri.OutOfPocket = 0
	AND pd.SecondPolicyPatientId <> pd.PatientId 
	AND pd.SecondPolicyPatientId <> 0
WHERE pf.FinancialStartDate <= ap.AppDate
	AND (pf.FinancialEndDate >= ap.AppDate OR pf.FinancialEndDate = '' OR pf.FinancialEndDate IS NULL)
	AND ap.AppointmentId IN (SELECT AppointmentId FROM PatientReceivables WHERE OpenBalance > 0)
GROUP BY pd.PatientId
	, ap.AppointmentId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
			WHEN 'M' THEN 10
			WHEN 'V' THEN 20
			WHEN 'A' THEN 30
			WHEN 'W' THEN 40
			ELSE 0
			END + '2') + CONVERT(nvarchar, FinancialPIndicator)
	, FinancialInsType
	, pf.PatientId
HAVING COUNT(*) > 1

UNION

SELECT pd.PatientId AS PatientId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
		WHEN 'M' THEN 10
		WHEN 'V' THEN 20
		WHEN 'A' THEN 30
		WHEN 'W' THEN 40
		ELSE 0
		END + '2') + CONVERT(nvarchar, FinancialPIndicator) AS Ordinal
	, FinancialInsType AS InsuranceType
	, ap.AppointmentId AS AppointmentId
	, pf.PatientId
FROM PatientDemographics pd 
INNER JOIN Appointments ap ON pd.PatientId = ap.PatientId
INNER JOIN PatientFinancial pf ON pd.SecondPolicyPatientId = pf.PatientId AND pd.SecondPolicyPatientId <> 0
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
	AND pri.OutOfPocket = 0
	AND pd.SecondPolicyPatientId <> pd.PatientId 
	AND pd.SecondPolicyPatientId <> 0
WHERE pf.FinancialStartDate <= ap.AppDate
	AND (pf.FinancialEndDate >= ap.AppDate OR pf.FinancialEndDate = '' OR pf.FinancialEndDate IS NULL)
GROUP BY pd.PatientId
	, ap.AppointmentId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
			WHEN 'M' THEN 10
			WHEN 'V' THEN 20
			WHEN 'A' THEN 30
			WHEN 'W' THEN 40
			ELSE 0
			END + '2') + CONVERT(nvarchar, FinancialPIndicator)
	, FinancialInsType
	, pf.PatientId
HAVING COUNT(*) > 1

----Change archived policy with end date before active policy's start date
UPDATE pfArchive SET pfArchive.FinancialStartDate = CONVERT(varchar,CONVERT(datetime, pfCurrent.FinancialStartDate)-1, 112), pfArchive.FinancialEndDate = CONVERT(varchar,CONVERT(datetime, pfCurrent.FinancialStartDate)-1, 112)
FROM PatientFinancial pfARCHIVE
INNER JOIN #NESTEDPLANS n ON pfARCHIVE.FinancialInsType = n.InsuranceType
	AND pfARCHIVE.PatientId = n.Policyholder
	AND pfARCHIVE.FinancialPIndicator = RIGHT(n.Ordinal, 1)
	AND pfARCHIVE.Status = 'X' 
INNER JOIN PatientFinancial pfCURRENT ON pfCurrent.FinancialInsType = n.InsuranceType
	AND pfCurrent.PatientId = n.Policyholder
	AND pfCurrent.FinancialPIndicator = RIGHT(n.Ordinal, 1)
	AND pfCurrent.Status = 'C' 
	AND pfCurrent.PatientId = pfArchive.PatientId
	AND pfArchive.FinancialEndDate >= pfCurrent.FinancialStartDate


----Active Insurance policies with same insurance type and ordinal, different insurers - archive one of them (doesn't use #NESTEDPLANS)
UPDATE pf2 SET pf2.FinancialStartDate = CONVERT(varchar,CONVERT(datetime, pf1.FinancialStartDate)-1, 112), pf2.FinancialEndDate = CONVERT(varchar,CONVERT(datetime, pf1.FinancialStartDate)-1, 112), pf2.Status = 'X'
FROM PatientFinancial pf1
INNER JOIN PatientFinancial pf2 ON pf1.PatientId = pf2.PatientId
	AND pf1.FinancialInsType = pf2.FinancialInsType
	AND pf1.FinancialPIndicator = pf2.FinancialPIndicator
	AND pf1.FinancialEndDate = ''
	AND pf2.FinancialEndDate = ''
	AND pf1.Status = 'C'
	AND pf2.Status = 'C'
	AND pf1.FinancialId < pf2.FinancialId


--Update list of overlapping insurance policies, causing duplicate ordinals, on receivables with open balances -- put them in #MORENESTEDPLANS temp table
SELECT pd.PatientId AS PatientId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
		WHEN 'M' THEN 10
		WHEN 'V' THEN 20
		WHEN 'A' THEN 30
		WHEN 'W' THEN 40
		ELSE 0
		END
		+ 1) + CONVERT(nvarchar, FinancialPIndicator) AS Ordinal
	, FinancialInsType AS InsuranceType
	, ap.AppointmentId AS AppointmentId
	, pf.PatientId AS Policyholder
INTO #MORENESTEDPLANS
FROM PatientDemographics pd 
INNER JOIN Appointments ap ON ap.PatientId = pd.PatientId
INNER JOIN PatientFinancial pf ON pd.PolicyPatientId = pf.PatientId 
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
	AND pri.OutOfPocket = 0
	AND pf.Patientid <> pd.PatientId
WHERE pf.FinancialStartDate <= ap.AppDate
	AND (pf.FinancialEndDate >= ap.AppDate OR pf.FinancialEndDate = '' OR pf.FinancialEndDate IS NULL)
	AND ap.AppointmentId IN (SELECT AppointmentId FROM PatientReceivables WHERE OpenBalance > 0)
GROUP BY pd.PatientId
	, ap.AppointmentId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
			WHEN 'M' THEN 10
			WHEN 'V' THEN 20
			WHEN 'A' THEN 30
			WHEN 'W' THEN 40
			ELSE 0
			END
		+ 1) + CONVERT(nvarchar, FinancialPIndicator)
		, FinancialInsType
	 , pf.PatientId
HAVING COUNT(*) > 1

UNION 

SELECT pd.PatientId AS PatientId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
		WHEN 'M' THEN 10
		WHEN 'V' THEN 20
		WHEN 'A' THEN 30
		WHEN 'W' THEN 40
		ELSE 0
		END
		+ 1) + CONVERT(nvarchar, FinancialPIndicator) AS Ordinal
	, FinancialInsType AS InsuranceType
	, ap.AppointmentId AS AppointmentId
	, pf.PatientId AS PolicyHolder
FROM dbo.PatientDemographics pd 
INNER JOIN dbo.Appointments ap ON pd.PatientId = ap.PatientId
INNER JOIN dbo.PatientFinancial pf ON pd.PolicyPatientId = pf.PatientId 
	AND pf.Patientid = pd.PatientId
WHERE pf.FinancialStartDate <= ap.AppDate
	AND (pf.FinancialEndDate >= ap.AppDate OR pf.FinancialEndDate = '' OR pf.FinancialEndDate IS NULL)
	AND ap.AppointmentId IN (SELECT AppointmentId FROM PatientReceivables WHERE OpenBalance > 0)
GROUP BY pd.PatientId
	, ap.AppointmentId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
			WHEN 'M' THEN 10
			WHEN 'V' THEN 20
			WHEN 'A' THEN 30
			WHEN 'W' THEN 40
			ELSE 0
			END
		+ 1) + CONVERT(nvarchar, FinancialPIndicator)
		, FinancialInsType
		, pf.PatientId
HAVING COUNT(*) > 1

UNION

SELECT pd.PatientId AS PatientId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
		WHEN 'M' THEN 10
		WHEN 'V' THEN 20
		WHEN 'A' THEN 30
		WHEN 'W' THEN 40
		ELSE 0
		END + '2') + CONVERT(nvarchar, FinancialPIndicator) AS Ordinal
	, FinancialInsType AS InsuranceType
	, ap.AppointmentId AS AppointmentId
	, pf.PatientId AS PolicyHolder
FROM dbo.PatientDemographics pd 
INNER JOIN dbo.Appointments ap ON pd.PatientId = ap.PatientId
INNER JOIN dbo.PatientFinancial pf ON pd.SecondPolicyPatientId = pf.PatientId AND pd.SecondPolicyPatientId <> 0
INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
	AND pri.OutOfPocket = 0
	AND pd.SecondPolicyPatientId <> pd.PatientId 
	AND pd.SecondPolicyPatientId <> 0
WHERE pf.FinancialStartDate <= ap.AppDate
	AND (pf.FinancialEndDate >= ap.AppDate OR pf.FinancialEndDate = '' OR pf.FinancialEndDate IS NULL)
	AND ap.AppointmentId IN (SELECT AppointmentId FROM PatientReceivables WHERE OpenBalance > 0)
GROUP BY pd.PatientId
	, ap.AppointmentId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
			WHEN 'M' THEN 10
			WHEN 'V' THEN 20
			WHEN 'A' THEN 30
			WHEN 'W' THEN 40
			ELSE 0
			END + '2') + CONVERT(nvarchar, FinancialPIndicator)
	, FinancialInsType
	, pf.PatientId
HAVING COUNT(*) > 1

UNION

SELECT pd.PatientId AS PatientId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
		WHEN 'M' THEN 10
		WHEN 'V' THEN 20
		WHEN 'A' THEN 30
		WHEN 'W' THEN 40
		ELSE 0
		END + '2') + CONVERT(nvarchar, FinancialPIndicator) AS Ordinal
	, FinancialInsType AS InsuranceType
	, ap.AppointmentId AS AppointmentId
	, pf.PatientId
FROM dbo.PatientDemographics pd 
INNER JOIN dbo.Appointments ap ON pd.PatientId = ap.PatientId
INNER JOIN dbo.PatientFinancial pf ON pd.SecondPolicyPatientId = pf.PatientId AND pd.SecondPolicyPatientId <> 0
INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
	AND pri.OutOfPocket = 0
	AND pd.SecondPolicyPatientId <> pd.PatientId 
	AND pd.SecondPolicyPatientId <> 0
WHERE pf.FinancialStartDate <= ap.AppDate
	AND (pf.FinancialEndDate >= ap.AppDate OR pf.FinancialEndDate = '' OR pf.FinancialEndDate IS NULL)
GROUP BY pd.PatientId
	, ap.AppointmentId
	, CONVERT(nvarchar, CASE pf.FinancialInsType
			WHEN 'M' THEN 10
			WHEN 'V' THEN 20
			WHEN 'A' THEN 30
			WHEN 'W' THEN 40
			ELSE 0
			END + '2') + CONVERT(nvarchar, FinancialPIndicator)
	, FinancialInsType
	, pf.PatientId
HAVING COUNT(*) > 1

--Try to change end date of overlapping plans left in temp table
UPDATE pf1
SET pf1.FinancialEndDate = CONVERT(VARCHAR, CONVERT(DATETIME, pf2.FinancialStartDate) - 1, 112)
FROM dbo.PatientFinancial pf1
INNER JOIN #MORENESTEDPLANS n ON n.Policyholder = pf1.PatientId
INNER JOIN Appointments a ON a.AppointmentId = n.AppointmentId
	AND n.InsuranceType = pf1.FinancialInsType
	AND pf1.FinancialPIndicator = RIGHT(n.ordinal, 1)
INNER JOIN PatientFinancial pf2 ON n.Policyholder = pf2.PatientId
	AND pf1.FinancialInstype = pf2.FinancialInsType
	AND pf2.FinancialPIndicator = RIGHT(n.ordinal, 1)
WHERE pf1.FinancialStartDate <= a.AppDate
	AND (
		pf1.FinancialEndDate >= a.AppDate
		OR pf1.FinancialEndDate = ''
		OR pf1.FinancialEndDate IS NULL
		)
	AND pf2.FinancialStartDate <= a.AppDate
	AND (
		pf2.FinancialEndDate >= a.AppDate
		OR pf1.FinancialEndDate = ''
		OR pf2.FinancialEndDate IS NULL
		)
	AND pf1.FinancialStartDate < pf2.FinancialStartDate



----Appointments

--Duds
DELETE FROM dbo.Appointments WHERE PatientId = 0

DELETE ap
FROM dbo.Appointments ap
LEFT JOIN PatientDemographics pd ON ap.PatientId = pd.PatientId
WHERE pd.PatientId IS NULL AND ap.PatientID IS NOT NULL


-- This is to clean up appointments that have a doctor instead of a location as ResourceId2, and not in a discharged state
UPDATE ap SET ResourceId2 = 0
FROM dbo.Appointments ap
LEFT JOIN Resources re on re.ResourceId = ap.ResourceId2
	AND re.ResourceType = 'R'
WHERE ap.resourceid2 between 1 AND 999 
	AND re.ResourceId IS NULL 
	AND ap.AppTime <> -1
	AND ap.ScheduleStatus <> 'D'

--This is to make sure all appointments have a valid appointment type. If no valid appointment type, then a new app type is created and assinged.
IF  (SELECT COUNT(ap.AppointmentId) FROM Appointments ap
	LEFT JOIN AppointmentType At ON ap.AppTypeId = at.AppTypeId 
	WHERE at.AppTypeId IS NULL
	AND ap.AppTypeId <> 0) > 0 
BEGIN

	INSERT [dbo].[AppointmentType] ([AppointmentType], [Question], [Duration], [DurationOther], [Recursion], [ResourceId1], [ResourceId2], [ResourceId3], [ResourceId4], [ResourceId5], [ResourceId6], [ResourceId7], [ResourceId8], [DrRequired], [PcRequired], [Status], [Period], [Rank], [OtherRank], [RecallFreq], [RecallMax], [Category], [FirstVisitIndicator], [AvailableInPlan], [EMBasis]) VALUES (N'Deleted Appointment Type', N'First Visit', 0, 0, 0, 0, 0, 0, 0, 0, 16777215, 0, 0, N'N', N'N', N'A', N'', 99, 99, 0, 0, N'', N'N', N'F', N'')

	UPDATE dbo.Appointments
	SET AppTypeId = (SELECT ApptypeId FROM AppointmentType WHERE AppointmentType = 'Deleted Appointment Type')
	FROM dbo.Appointments ap
	LEFT JOIN AppointmentType At ON ap.AppTypeId = at.AppTypeId 
	WHERE at.AppTypeId IS NULL
		AND ap.AppTypeId <> 0

END


--This is to make sure all appointments have a valid status. If no valid status a status is created and assigned
IF  (SELECT COUNT(ap.AppointmentId) FROM Appointments ap
	LEFT JOIN PracticeCodeTable pct ON SUBSTRING(pct.Code, 1, 1) = ap.ScheduleStatus
		AND pct.ReferenceType = 'APPOINTMENTSTATUS'
	WHERE pct.Id IS NULL) > 0 
	
BEGIN

	IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'AppointmentStatus' AND SUBSTRING(Code, 1, 1) = 'X')
	INSERT INTO [PracticeCodeTable] (ReferenceType, Code) VALUES ('AppointmentStatus', 'X Deleted Status')

	UPDATE dbo.Appointments
	SET ScheduleStatus = (SELECT TOP 1 SUBSTRING(Code, 1, 1) FROM PracticeCodeTable WHERE ReferenceType = 'AppointmentStatus' AND SUBSTRING(Code, 1, 1) = 'X')
	FROM dbo.Appointments ap
	LEFT JOIN PracticeCodeTable pct ON SUBSTRING(pct.Code, 1, 1) = ap.ScheduleStatus
		AND pct.ReferenceType = 'AppointmentStatus'
	WHERE pct.Id IS NULL

END


--Add to Resources for appointments with a missing resource
IF (SELECT COUNT(*) FROM Appointments ap
LEFT JOIN Resources re ON re.ResourceId = ap.ResourceId1  
WHERE re.ResourceId IS NULL) > 0

BEGIN 
	IF NOT EXISTS (SELECT * FROM Resources WHERE ResourceName = 'DeletedDoctor')
	BEGIN
		DECLARE @practiceId INt 
		SET @practiceId = (SELECT Practiceid FROM PracticeName WHERE LocationReference = '' AND PracticeType = 'P')
		INSERT INTO Resources (ResourceName, ResourceType, ResourceDescription, ResourceLastName, ResourceFirstName, PracticeId)
		VALUES ('DeletedDoctor', 'Z', 'Deleted Doctor', 'Deleted', 'Doctor', @practiceId)
	END

	UPDATE ap
	SET ResourceId1 = (SELECT ResourceId FROM Resources WHERE ResourceName = 'DeletedDoctor')
	FROM Appointments ap
	LEFT JOIN Resources re ON re.ResourceId = ap.ResourceId1  
	WHERE re.ResourceId IS NULL

END

--Handles Appointment.ResourceId2 with ServiceCode of 03, not 02. Appointments should never be scheduled with Resources.ServiceCode of 03.
UPDATE ap
SET ap.ResourceId2 = 0
FROM dbo.Appointments ap
INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId2
WHERE re.ServiceCode <> '02'
	AND ap.ResourceId2 BETWEEN 1 AND 999

--Found an instance of -1 on ResourceId2 on the Appointment tables
UPDATE Appointments
SET ResourceId2 = 0
WHERE ResourceId2 = -1

--appointments that aren't discharged that have receivable
UPDATE ap SET ap.ScheduleStatus = 'D'
FROM dbo.PracticeActivity pa
INNER JOIN dbo.Appointments ap ON ap.Appointmentid = pa.Appointmentid
LEFT JOIN dbo.PatientReceivables pr ON pr.Appointmentid = ap.Appointmentid
WHERE ScheduleStatus IN  ('A', 'R', 'P')
	AND ReceivableId IS NOT NULL

--Referral attached to appointments for a patient that doesn't have anything to do with the appointmment
UPDATE ap SET ap.ReferralId = 0
FROM dbo.Appointments ap
INNER JOIN dbo.PatientReferral pRef ON ap.ReferralId = pRef.ReferralId
WHERE pRef.PatientId <> ap.PatientId

--Fix appointments linked to missing referral
UPDATE ap
SET ap.ReferralId = 0 
FROM dbo.Appointments ap
LEFT JOIN dbo.PatientReferral pRef ON ap.ReferralId = pRef.ReferralId
WHERE ap.ReferralId > 0 
	AND pRef.ReferralId IS NULL

--Fix appointments linked to missing precert
UPDATE ap
SET ap.PreCertId = 0 
FROM dbo.Appointments ap
LEFT JOIN dbo.PatientPreCerts pCert ON ap.PreCertId = pCert.PreCertId
WHERE ap.PreCertId > 0 
	AND pCert.PreCertId IS NULL




--Pre-certs that are attached to two appointments. 
--This was a bug in the Optical Software that allowed this to happen
UPDATE ap SET PreCertId = 0 
FROM AppointmentS ap
WHERE PreCertId IN (
	SELECT PreCertId FROM Appointments
	WHERE PreCertId > 0
	GROUP BY PreCertId
	HAVING COUNT(*) > 1)
	AND Comments = 'add via billing'

--Missing PreCertDate
UPDATE ppc SET ppc.PreCertDate = ap.AppDate
FROM dbo.PatientPreCerts ppc
INNER JOIN Appointments ap ON ap.PreCertId = ppc.PreCertId
WHERE ppc.PreCertDate = '' OR ppc.PreCertDate IS NULL


--Precert for missing patient
DELETE ppc
FROM dbo.PatientPrecerts ppc
LEFT JOIN PatientDemographics pd ON pd.PatientId = ppc.PatientId
WHERE pd.PatientId IS NULL


--PatientReferral - fix ReferredTo doctor if invalid
UPDATE dbo.PatientReferral
SET ReferredTo = (SELECT TOP 1 ResourceId1 FROM Appointments a WHERE a.ReferralId = ReferralId)
WHERE ReferredTo = 0

--Fix patient referrals
DELETE FROM PatientReferral WHERE PatientId = 0 

DELETE pRef
FROM PatientReferral pRef
LEFT JOIN PatientDemographics pd ON pd.PatientId = pRef.PatientId
WHERE pd.PatientId IS NULL

UPDATE PatientReferral
SET ReferralDate = 20000101
WHERE dbo.Isdate112(ReferralDate) = 0

UPDATE  PatientReferral 
SET ReferralExpireDate = 20200101 
WHERE ISDATE(ReferralExpireDate) = 0


--Fix PracticeActivity Status
UPDATE pa
SET pa.[Status] = ap.ScheduleStatus
FROM Appointments ap
INNER JOIN PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
WHERE pa.[Status] IN (
		'E'
		,'G'
		,'H'
		,'W'
		,'M'
		,'U'
		)
	AND ap.ScheduleStatus <> 'A'

--Fix PracticeActivityStatus
UPDATE pa
SET pa.[Status] = 'H'
FROM PracticeActivity pa
INNER JOIN Appointments ap ON ap.Appointmentid = pa.Appointmentid
LEFT JOIN PatientReceivables pr ON pr.Appointmentid = ap.Appointmentid
WHERE pa.[Status] = 'U'
	AND ap.ScheduleStatus = 'A'


--Fix PracticeActivityStatus
UPDATE pa 
SET pa.[Status] = 'H' 
FROM PracticeActivity pa 
INNER JOIN Appointments ap ON ap.AppointmentId = pa.AppointmentId
LEFT JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
LEFT JOIN PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId
WHERE pc.ClinicalId IS NOT NULL 
	AND pr.ReceivableId IS NULL
	AND pa.[Status] NOT IN ('G', 'H')
	AND ap.ScheduleStatus = 'A'	

--Fix PracticeActivityStatus
UPDATE pa 
SET pa.[Status] = 'W' 
FROM PracticeActivity pa 
INNER JOIN Appointments ap ON ap.AppointmentId = pa.AppointmentId
LEFT JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
LEFT JOIN PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId
WHERE pc.ClinicalId IS NULL 
	AND pr.ReceivableId IS NULL
	AND pa.[Status] = 'D'
	AND ap.ScheduleStatus = 'A'	

--Multiple activity records
DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
		AND pa.activityid < pa2.activityid
		AND pa.patientid = pa2.patientid
		AND pa.appointmentid <> 0
		AND pa.status = 'D' AND pa2.status <> 'D')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
		AND pa.activityid > pa2.activityid
		AND pa.patientid = pa2.patientid
		AND pa.appointmentid <> 0
		AND pa.status = 'D' 
		AND pa2.status <> 'D')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
		AND pa.activityid < pa2.activityid
		AND pa.patientid = pa2.patientid
		AND pa.appointmentid <> 0
		AND pa.status = 'D' 
		AND pa2.status = 'D')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
		AND pa.activityid > pa2.activityid
		AND pa.patientid = pa2.patientid
		AND pa.appointmentid <> 0
		AND pa.status = 'H' 
		AND pa2.status = 'W')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
		AND pa.activityid < pa2.activityid
		AND pa.patientid = pa2.patientid
		AND pa.appointmentid <> 0
		AND pa.status = 'H' 
		AND pa2.status = 'W')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
		AND pa.activityid < pa2.activityid
		AND pa.patientid = pa2.patientid
		AND pa.appointmentid <> 0
		AND pa.status = 'G' 
		AND pa2.status = 'G')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid > pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'H' 
	AND pa2.status = 'E')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid < pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'H' 
	AND pa2.status = 'E')


DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid > pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'H'
	AND pa2.status = 'H')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid > pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'X'
	AND pa2.status = 'X')


DELETE FROM practiceactivity WHERE activityid IN
(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid <> pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'H' 
	AND pa2.status = 'G')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid <> pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'E' 
	AND pa2.status = 'W')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid <> pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'G'
	AND pa2.status = 'W')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid < pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'G'
	AND pa2.status = 'E')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid <> pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'W'
	AND pa2.status = 'W')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid <> pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'E'
	AND pa2.status = 'E')

--Clean up Practice Activity with blank dates
UPDATE pa
SET pa.ActivityDate = ap.Appdate
FROM PracticeActivity pa
INNER JOIN Appointments ap ON ap.AppointmentId = pa.AppointmentId
WHERE pa.ActivityDate = 0
	OR pa.ActivityDate = ''
	
--AppointmentType
--The id of this row needs to be 0 for manual claims to enable us to use a FK constraint

IF NOT EXISTS(SELECT AppTypeId FROM AppointmentType WHERE AppTypeId = 0)
BEGIN
	DECLARE @constraintNames xml
	EXEC dbo.DisableEnabledCheckConstraints 'dbo.AppointmentType', @names = @constraintNames OUTPUT

	SET IDENTITY_INSERT [dbo].[AppointmentType] ON
	INSERT INTO [dbo].[AppointmentType] ([AppTypeId], [AppointmentType], [Question], [Duration], [DurationOther], [Recursion], [ResourceId1], [ResourceId2], [ResourceId3], [ResourceId4], [ResourceId5], [ResourceId6], [ResourceId7], [ResourceId8], [DrRequired], [PcRequired], [Status], [Period], [Rank], [OtherRank], [RecallFreq], [RecallMax], [Category], [FirstVisitIndicator], [AvailableInPlan], [EMBasis]) VALUES (0, N'Manual Claim', N'No Questions', 0, 0, 0, 0, 0, 0, 0, 0, 16777215, 0, 0, N'N', N'N', N'A', N'', 99, 99, 0, 0, N'', N'N', N'F', N'')
	SET IDENTITY_INSERT [dbo].[AppointmentType] OFF

	EXEC dbo.EnableCheckConstraints @constraintNames
END


--CL Order
DELETE FROM CLOrders WHERE PatientId = 0
