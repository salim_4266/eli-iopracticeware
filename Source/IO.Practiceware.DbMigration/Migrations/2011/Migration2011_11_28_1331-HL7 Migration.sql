﻿--Map MVE message type
IF EXISTS (SELECT * FROM Model.ExternalSystems
	WHERE Name = 'MVE')
BEGIN 
	IF NOT EXISTS (SELECT Id FROM model.ExternalSystemExternalSystemMessageTypes
		WHERE ExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'IO Practiceware')
			AND ExternalSystemMessageTypeId = (SELECT Id FROM model.ExternalSystemMessageTypes WHERE Name = 'ORM_O01_V231' AND IsOutbound = 0))
		
		BEGIN
		DECLARE @io int
		SET @io = (SELECT Id FROM model.ExternalSystems WHERE Name = 'IO Practiceware')
		
		DECLARE @orm int
		SET @orm = (SELECT Id FROM model.ExternalSystemMessageTypes WHERE Name = 'ORM_O01_V231' AND IsOutbound = 0)
		
		
		INSERT INTO model.ExternalSystemExternalSystemMessageTypes (ExternalSystemId,ExternalSystemMessageTypeId)
		VALUES (@io,@orm )

		END
END

