﻿--Valid Appointments.ResourceId2
IF OBJECT_ID('AppointmentsResourceId2ResourcesTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER AppointmentsResourceId2ResourcesTrigger')
END

GO

CREATE TRIGGER AppointmentsResourceId2ResourcesTrigger
ON Appointments
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (((SELECT COUNT(*)
	FROM (SELECT * FROM inserted  EXCEPT SELECT * FROM deleted) i
	WHERE i.ResourceId2 NOT IN (SELECT ResourceId FROM Resources WHERE ServiceCode = '02' AND ResourceType = 'R') 
	AND (i.ResourceId2 BETWEEN 1 AND 999 OR i.ResourceId2 > 1999)) <> 0) 
	AND dbo.GetNoCheck() = 0)
	   
BEGIN
	RAISERROR ('Invalid appointment location (Appointments.ResourceId2) Location from Resources Table',16,1)
	ROLLBACK TRANSACTION
END

GO
