﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Add tables EncounterTypes, EncounterTypeInvoiceTypes
    /// </summary>
    [Migration(201111141330)]
    public class Migration201111141330 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"

-- Creating table 'EncounterTypes'
CREATE TABLE [model].[EncounterTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IsArchived] bit  NOT NULL
);
GO

-- Creating table 'EncounterTypeInvoiceTypes'
CREATE TABLE [model].[EncounterTypeInvoiceTypes] (
    [EncounterTypeId] int  NOT NULL,
    [InvoiceTypeId] int  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'EncounterTypes'
ALTER TABLE [model].[EncounterTypes]
ADD CONSTRAINT [PK_EncounterTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterTypeInvoiceTypes'
ALTER TABLE [model].[EncounterTypeInvoiceTypes]
ADD CONSTRAINT [PK_EncounterTypeInvoiceTypes]
    PRIMARY KEY CLUSTERED ([EncounterTypeId], [InvoiceTypeId] ASC);
GO

-- Creating foreign key on [EncounterTypeId] in table 'EncounterTypeInvoiceTypes'
ALTER TABLE [model].[EncounterTypeInvoiceTypes]
ADD CONSTRAINT [FK_EncounterTypeEncounterTypeInvoiceType]
    FOREIGN KEY ([EncounterTypeId])
    REFERENCES [model].[EncounterTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterTypeEncounterTypeInvoiceType'
CREATE INDEX [IX_FK_EncounterTypeEncounterTypeInvoiceType]
ON [model].[EncounterTypeInvoiceTypes]
    ([EncounterTypeId]);
GO
");

            Execute.Sql(@"
SET IDENTITY_INSERT [model].[EncounterTypes] ON

INSERT [model].[EncounterTypes] (Id, Name, IsArchived) VALUES (1, 'Clinician Encounter', 0)
INSERT [model].[EncounterTypes] (Id, Name, IsArchived) VALUES (2, 'Facility Encounter', 0)
INSERT [model].[EncounterTypes] (Id, Name, IsArchived) VALUES (3, 'Clinician Facility Encounter', 0)

SET IDENTITY_INSERT [model].[EncounterTypes] OFF

--EncounterTypesInvoiceType 
INSERT [model].[EncounterTypeInvoiceTypes] (EncounterTypeId, InvoiceTypeId) VALUES (1, 1)
INSERT [model].[EncounterTypeInvoiceTypes] (EncounterTypeId, InvoiceTypeId) VALUES (2, 2)
INSERT [model].[EncounterTypeInvoiceTypes] (EncounterTypeId, InvoiceTypeId) VALUES (3, 1)
INSERT [model].[EncounterTypeInvoiceTypes] (EncounterTypeId, InvoiceTypeId) VALUES (3, 2)
");
        }

        public override void Down()
        {
        }
    }
}