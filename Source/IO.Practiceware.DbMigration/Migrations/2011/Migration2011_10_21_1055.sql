﻿--CK Constraints for PracticeInsurers, PatientReceivables, PatientReceivablePayments, PracticeTransactionJournal 

---Invalid InsurerId
-- PatientFinancial
---FK on patient financial to prevent invalid insurances
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientFinancial_PracticeInsurers]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientFinancial]'))
ALTER TABLE [dbo].[PatientFinancial] DROP CONSTRAINT [FK_PatientFinancial_PracticeInsurers]
GO

ALTER TABLE [dbo].[PatientFinancial] WITH CHECK ADD  CONSTRAINT [FK_PatientFinancial_PracticeInsurers] FOREIGN KEY([FinancialInsurerId])
REFERENCES [dbo].[PracticeInsurers] ([InsurerId])
GO

ALTER TABLE [dbo].[PatientFinancial] CHECK CONSTRAINT [FK_PatientFinancial_PracticeInsurers]
GO

DECLARE @constraintNames xml

--Insert a 0 insurnace, used to enable FK where the software inserts a 0 for InsurerId
IF NOT EXISTS(SELECT * FROM PracticeInsurers WHERE InsurerId = 0)
BEGIN
	EXEC dbo.DisableEnabledCheckConstraints 'dbo.PracticeInsurers', @names = @constraintNames OUTPUT

	SET IDENTITY_INSERT [dbo].[PracticeInsurers] ON
	INSERT INTO PracticeInsurers (Insurerid, InsurerName) VALUES (0, 'ZZZ No insurance')
	SET IDENTITY_INSERT [dbo].[PracticeInsurers] OFF
	
	EXEC dbo.EnableCheckConstraints @constraintNames
END

--Insert a 99999 insurnace, used to enable FK for on account payments
IF NOT EXISTS(SELECT * FROM PracticeInsurers WHERE InsurerId = 99999)
BEGIN
	DECLARE @PracticeInsurerSeed int
	SET @PracticeInsurerSeed = (SELECT IDENT_CURRENT('PracticeInsurers'))

	EXEC dbo.DisableEnabledCheckConstraints 'dbo.PracticeInsurers', @names = @constraintNames OUTPUT

	SET IDENTITY_INSERT [dbo].[PracticeInsurers] ON
	INSERT INTO PracticeInsurers (Insurerid, InsurerName) VALUES (99999, 'ZZZ On account insurance')
	SET IDENTITY_INSERT [dbo].[PracticeInsurers] OFF

	DBCC CHECKIDENT ('PracticeInsurers', 'RESEED', @PracticeInsurerSeed)
	
	EXEC dbo.EnableCheckConstraints @constraintNames
END

---FK on patient receivables to prevent invalid insurances
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientReceivables_PracticeInsurers]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivables]'))
ALTER TABLE [dbo].[PatientReceivables] DROP CONSTRAINT [FK_PatientReceivables_PracticeInsurers]
GO

ALTER TABLE [dbo].[PatientReceivables] WITH CHECK ADD  CONSTRAINT [FK_PatientReceivables_PracticeInsurers] FOREIGN KEY([InsurerId])
REFERENCES [dbo].[PracticeInsurers] ([InsurerId])
GO

ALTER TABLE [dbo].[PatientReceivables] CHECK CONSTRAINT [FK_PatientReceivables_PracticeInsurers]
GO

--Valid insurer receivable insurer payer
IF OBJECT_ID('PatientReceivablePaymentsValidInsurerTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablePaymentsValidInsurerTrigger')
END

GO 

CREATE TRIGGER PatientReceivablePaymentsValidInsurerTrigger
ON dbo.PatientReceivablePayments
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF EXISTS (SELECT *
		FROM PatientReceivablePayments prp WITH(NOLOCK) 
		INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON prp.PaymentId = i.PaymentId
		LEFT JOIN PracticeInsurers pins ON pins.InsurerId = prp.PayerId
			AND prp.PayerType = 'I'
		WHERE prp.PayerType = 'I' AND pins.InsurerId IS NULL) 
		AND dbo.GetNoCheck() = 0

BEGIN
ROLLBACK TRANSACTION
RAISERROR ('The payer must be valid, when payment from Insurer (PayerType = i, valid PracticeInsurers)', 16,1)
END
GO


--Appointment date must be the same as Patient Receivable date
IF OBJECT_ID('PatientReceivablesInvoiceDate') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablesInvoiceDate')
END

GO

CREATE TRIGGER PatientReceivablesInvoiceDate
ON PatientReceivables
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (((SELECT COUNT(*) 
	FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i
	INNER JOIN Appointments ap WITH(NOLOCK) ON i.AppointmentId = ap.AppointmentId 
	WHERE ap.Comments like 'ADD VIA%'
	AND i.InvoiceDate <> ap.AppDate) <> 0)
	AND dbo.GetNoCheck() = 0)
	
BEGIN
	RAISERROR ('Invoice date and Appointment date cannot be different. ',16,1)
	ROLLBACK TRANSACTION
END
GO


--Valid patient receivable Patient payer
IF OBJECT_ID('PatientReceivablePaymentsValidPatientTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablePaymentsValidPatientTrigger')
END

GO 

CREATE TRIGGER PatientReceivablePaymentsValidPatientTrigger
ON dbo.PatientReceivablePayments
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF EXISTS (SELECT *
		FROM PatientReceivablePayments prp
		INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON prp.PaymentId = i.PaymentId
		LEFT JOIN PatientDemographics pd WITH(NOLOCK) ON pd.PatientId = prp.PayerId
			AND prp.PayerType = 'P'
		WHERE prp.payertype = 'P' AND pd.PatientId IS NULL) 
		AND dbo.GetNoCheck() = 0
	
BEGIN

ROLLBACK TRANSACTION
RAISERROR ('The payer must be valid, when payment from Patient (PayerType = P, Valid PatientDemographics)', 16,1)

END
GO

--Valid PatientReceivables Billing Office
IF OBJECT_ID('PatientReceivablesBillingOfficeTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablesBillingOfficeTrigger')
END

GO

CREATE TRIGGER PatientReceivablesBillingOfficeTrigger
ON PatientReceivables
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF EXISTS (
		SELECT * 
		FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i
		LEFT JOIN Resources re WITH(NOLOCK) ON re.ResourceId = i.BillingOffice
			AND re.ServiceCode = '02' 
			AND re.ResourceType = 'R'
		WHERE (BillingOffice BETWEEN 1 and 999 OR BillingOffice > 1999)
			AND re.ResourceId IS NULL
		
		)
	AND dbo.GetNoCheck() = 0
	   
BEGIN
	RAISERROR ('Invalid patient receivables billing office. ',16,1)
	ROLLBACK TRANSACTION
END
GO


--Prevent deletion of patients linked to payments
IF OBJECT_ID('PatientDemographicsPatientIdPatientReceivablePaymentsTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientDemographicsPatientIdPatientReceivablePaymentsTrigger')
END

GO

CREATE TRIGGER PatientDemographicsPatientIdPatientReceivablePaymentsTrigger
ON PatientDemographics
FOR DELETE
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (SELECT COUNT(*)
	FROM deleted d
	INNER JOIN PatientReceivablePayments prp WITH(NOLOCK) ON d.PatientId = prp.PayerId AND prp.PayerType = 'P'
	) <> 0
	 
BEGIN
	RAISERROR ('Patients linked to payments cannot be deleted.',16,1)
	ROLLBACK TRANSACTION
END
GO


--Insurer not covering dependents has policyholder who is not patient
IF OBJECT_ID('PatientReceivablesOutOfPocketTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablesOutOfPocketTrigger')
END

GO

CREATE TRIGGER PatientReceivablesOutOfPocketTrigger
ON PatientReceivables
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF EXISTS (
		SELECT * 
		FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i
		INNER JOIN PracticeInsurers pri WITH(NOLOCK) ON pri.InsurerId = i.InsurerId
		WHERE pri.OutOfPocket <> 0
			AND i.InsuredId <> i.PatientId)
	AND dbo.GetNoCheck() = 0
	   
BEGIN
	RAISERROR ('You cannot connect a patient to another person''s insurance policy if the insurer does not cover dependents',16,1)
	ROLLBACK TRANSACTION
END
GO


----Invoice where receivables have different referring doctors
---Invoice where receivables have different billing doctors
--Invoice where receivables have different invoicedates
IF OBJECT_ID('PatientReceivablesDifferentInvoiceInformationTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablesDifferentInvoiceInformationTrigger')
END

GO

CREATE TRIGGER PatientReceivablesDifferentInvoiceInformationTrigger
ON PatientReceivables
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF EXISTS (
		SELECT * 
		FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i
		INNER JOIN PatientReceivables pr WITH(NOLOCK) ON pr.Invoice = i.Invoice
		WHERE i.BillToDr <> pr.BillToDr
			OR i.InvoiceDate <> pr.InvoiceDate
			OR i.ReferDr <> pr.ReferDr)
	AND dbo.GetNoCheck() = 0
	   
BEGIN
	RAISERROR ('1 Invoice cannot have different: BillToDr, InvoiceDate, or ReferDr. (PatientReceivables)',16,1)
	ROLLBACK TRANSACTION
END
GO


--Invoice where there's a receivables to an insurer, and also a receivable where InsurerId = 0
IF OBJECT_ID('PatientReceivablesInsurerIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablesInsurerIdTrigger')
END

GO

CREATE TRIGGER PatientReceivablesInsurerIdTrigger
ON PatientReceivables
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF EXISTS (
		SELECT * 
		FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i
		INNER JOIN PatientReceivables pr WITH(NOLOCK) ON pr.Invoice = i.Invoice
		WHERE pr.ReceivableId <> i.ReceivableId
			AND ((i.InsurerId = 0
			AND pr.InsurerId <> 0)
			OR (pr.InsurerId = 0
			AND i.InsurerId <> 0)))
	AND dbo.GetNoCheck() = 0
	   
BEGIN
	RAISERROR ('Cannot have an invoice where there is a receivables to an insurer, and another receivable with the same invoice where InsurerId = 0(PatientReceivables)',16,1)
	ROLLBACK TRANSACTION
END
GO

