﻿--Fix data in PracticeFeeSchedules, PatientReferrals, PracticeVendors

DELETE FROM PracticeFeeSchedules
WHERE Cpt = '' or PlanId < 1

DELETE FROM PatientReferral
WHERE PatientId = 0

--Fix Invalid Referred To Doctor
UPDATE pRef SET ReferredTo =  v.ResourceId
FROM PatientReferral pRef
INNER JOIN (SELECT ReferralId, MIN(ResourceId) AS ResourceId
	FROM PatientReferral pRef
	INNER JOIN Resources r ON ResourceType = 'D'
	WHERE ReferredTo NOT IN (SELECT ResourceId FROM Resources WHERE ResourceType IN ('D', 'Q', 'Z', 'Y'))
	GROUP BY ReferralId) v ON v.ReferralId = pRef.ReferralId

--No last name for referring doctors
UPDATE PracticeVendors
SET VendorLastName = SUBSTRING(VendorLastName,1,16)
WHERE VendorLastName = '' OR VendorLastName IS NULL
	AND VendorType = 'D' 