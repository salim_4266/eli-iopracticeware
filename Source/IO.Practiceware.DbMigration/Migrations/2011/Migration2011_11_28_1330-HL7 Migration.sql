﻿--Migrate HL7 data
-- FOR RA ONLY
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'dbo.PracticeRepositoryTables')
		)
BEGIN
    	    -- ExternalSystems
	   INSERT model.ExternalSystems (
          [Name]
		    )
	    SELECT [Name]
	    FROM [dbo].[ExternalSystems]

	    -- ExternalSystemEntity
	    SET IDENTITY_INSERT [model].[ExternalSystemEntities] ON

	    INSERT model.ExternalSystemEntities (
		    Id
		    ,ExternalSystemId
		    ,[Name]
		    )
	    SELECT ExternalSystemEntityID
		    ,(SELECT Id FROM dbo.ExternalSystem WHERE Name = 'Athena')
		    ,[Name]
	    FROM [dbo].[ExternalSystemEntities]

	    SET IDENTITY_INSERT [model].[ExternalSystemEntities] OFF
	    
-- ExternalSystemEntityMapping
	    SET IDENTITY_INSERT [model].[ExternalSystemEntityMappings] ON

	    INSERT model.ExternalSystemEntityMappings (
		    Id
		    ,PracticeRepositoryEntityId
		    ,PracticeRepositoryEntityKey
		    ,ExternalSystemEntityId
		    ,ExternalSystemEntityKey
		    )
	    SELECT ExternalSystemMappingId
		    ,CASE PracticeRepositoryTableID
			    WHEN (
					    SELECT PracticeRepositoryTableID
					    FROM [dbo].[PracticeRepositoryTables]
					    WHERE [Name] = 'Appointments'
					    )
				    THEN 1
			    WHEN (
					    SELECT PracticeRepositoryTableID
					    FROM [dbo].[PracticeRepositoryTables]
					    WHERE [Name] = 'AppointmentType'
					    )
				    THEN 2
			    WHEN (
					    SELECT PracticeRepositoryTableID
					    FROM [dbo].[PracticeRepositoryTables]
					    WHERE [Name] = 'PatientDemographics'
					    )
				    THEN 3
			    WHEN (
					    SELECT PracticeRepositoryTableID
					    FROM [dbo].[PracticeRepositoryTables]
					    WHERE [Name] = 'PracticeInsurers'
					    )
				    THEN 5
			    WHEN (
					    SELECT PracticeRepositoryTableID
					    FROM [dbo].[PracticeRepositoryTables]
					    WHERE [Name] = 'PracticeName'
					    )
				    THEN 7
			    WHEN (SELECT PracticeRepositoryTableKey 
					FROM ExternalSystemMappings Em
					INNER JOIN Resources Re on Re.ResourceId = Em.PracticeRepositoryTableKey
					WHERE Re.ResourceType IN ('D','Q')
					AND PracticeRepositoryTableId = (
							SELECT PracticeRepositoryTableID
							FROM [dbo].[PracticeRepositoryTables]
							WHERE [Name] = 'Resources'
							))
				    THEN 6
				 WHEN (SELECT PracticeRepositoryTableKey 
					FROM ExternalSystemMappings Em
					INNER JOIN Resources Re on Re.ResourceId = Em.PracticeRepositoryTableKey
					WHERE Re.ResourceType IN ('R')
					AND PracticeRepositoryTableId = (
							SELECT PracticeRepositoryTableID
							FROM [dbo].[PracticeRepositoryTables]
							WHERE [Name] = 'Resources'
							))
				    THEN 7
			    WHEN (
					    SELECT PracticeRepositoryTableID
					    FROM [dbo].[PracticeRepositoryTables]
					    WHERE [Name] = 'PracticeVendors'
					    )
				    THEN 8
			    END AS PracticeRepositoryEntityId
		    ,CASE PracticeRepositoryTableKey
				WHEN (
					    SELECT PracticeRepositoryTableID
					    FROM [dbo].[PracticeRepositoryTables]
					    WHERE [Name] = 'Appointments'
					    )
					THEN CONVERT(nvarchar, model.GetBigPairId(0, PracticeRepositoryTableKey, 20000000))
				WHEN (
					    SELECT PracticeRepositoryTableID
					    FROM [dbo].[PracticeRepositoryTables]
					    WHERE [Name] = 'PatientDemographics'
					    )
					THEN CONVERT(nvarchar, model.GetBigPairId(3, PracticeRepositoryTableKey, 20000000))
				WHEN (SELECT PracticeRepositoryTableKey 
					FROM ExternalSystemMappings Em
					INNER JOIN Resources Re on Re.ResourceId = Em.PracticeRepositoryTableKey
					WHERE Re.ResourceType IN ('R')
					AND PracticeRepositoryTableId = 
							(
							SELECT PracticeRepositoryTableID
							FROM [dbo].[PracticeRepositoryTables]
							WHERE [Name] = 'Resources'
							))
					THEN CONVERT(nvarchar, model.GetBigPairId(1, PracticeRepositoryTableKey, 1000000))
				WHEN (
					    SELECT PracticeRepositoryTableID
					    FROM [dbo].[PracticeRepositoryTables]
					    WHERE [Name] = 'PracticeName'
					    )
					THEN CONVERT(nvarchar, model.GetBigPairId(0, PracticeRepositoryTableKey, 1000000))
				ELSE CONVERT(nvarchar,PracticeRepositoryTableKey)
				END AS PracticeRepositoryEntityKey
		    ,(SELECT Id FROM dbo.ExternalSystem WHERE Name = 'Athena')
		    ,ExternalSystemEntityKey
	    FROM [dbo].[ExternalSystemMappings]

	    SET IDENTITY_INSERT [model].[ExternalSystemEntityMappings] OFF     
    
END

ELSE IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'dbo.ExternalSystem')
		)

BEGIN
        -- ExternalSystems
        INSERT model.ExternalSystems ([Name])
        SELECT [Name]
        FROM [dbo].[externalsystem]
        WHERE [Name] <> 'IO Practiceware'

        -- ExternalSystemEntity
        SET IDENTITY_INSERT [model].[ExternalSystemEntities] ON

        INSERT model.ExternalSystemEntities (
	        Id
	        ,ExternalSystemId
	        ,[Name]
	        )
        SELECT Id
	        ,(SELECT Id FROM model.ExternalSystems WHERE Name = (SELECT Name FROM dbo.ExternalSystem WHERE Id = ExternalSystemId))
	        ,[Name]
        FROM [dbo].[externalsystementity]

        SET IDENTITY_INSERT [model].[ExternalSystemEntities] OFF
        
        -- ExternalSystemEntityMapping
        SET IDENTITY_INSERT [model].[ExternalSystemEntityMappings] ON

        INSERT model.ExternalSystemEntityMappings (
	        Id
	        ,PracticeRepositoryEntityId
	        ,PracticeRepositoryEntityKey
	        ,ExternalSystemEntityId
	        ,ExternalSystemEntityKey
	        )
        SELECT Id
	        ,CASE PracticeRepositoryEntityId
		        WHEN (
				        SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'Appointment'
				        )
			        THEN 1
		        WHEN (
				        SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'AppointmentType'
				        )
			        THEN 2
		        WHEN (
				        SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'Patient'
				        )
			        THEN 3
		        WHEN (
				        SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'Insurer'
				        )
			        THEN 5
		        WHEN (
				        SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'PatientInsurance'
				        )
			        THEN 4
		        WHEN (
				        SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'Office'
				        )
			        THEN 8
		        WHEN (
				        SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'Provider'
				        )
			        THEN 6
		        WHEN (
				        SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'ExternalProvider'
				        )
			        THEN 7
		        END AS PracticeRepositoryEntityId
	        ,CASE PracticeRepositoryEntityId
		        WHEN (
				        SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'Appointment'
				        )
					THEN CONVERT(nvarchar, model.GetBigPairId(0, PracticeRepositoryEntityKey, 20000000))
				WHEN (
				        SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'Patient'
				        )
				    THEN CONVERT(nvarchar,PracticeRepositoryEntityKey)
				WHEN (SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'PatientInsurance')
					THEN CONVERT(nvarchar, (model.GetBigPairId((SELECT PatientId FROM dbo.PatientFinancial WHERE FinancialId = PracticeRepositoryEntityKey), PracticeRepositoryEntityKey, 20000000)))
				WHEN (
				        SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'Office'
				        )
					THEN 
						CASE  
							WHEN PracticeRepositoryEntityKey = 0
								THEN CONVERT(nvarchar,(SELECT PracticeId FROM PracticeName WHERE PracticeType = 'P' AND LocationReference = ''))
							WHEN PracticeRepositoryEntityKey < 1000
								THEN CONVERT(nvarchar, model.GetBigPairId(1, PracticeRepositoryEntityKey, 1000000))
							WHEN PracticeRepositoryEntityKey > 1000
								THEN CONVERT(nvarchar,(SELECT PracticeId FROM PracticeName WHERE PracticeType = 'P' AND PracticeId = PracticeRepositoryEntityKey - 1000))
						END
				WHEN (
				        SELECT Id
				        FROM [dbo].[PracticeRepositoryEntity]
				        WHERE [Name] = 'ExternalProvider'
				        )
					THEN CONVERT(nvarchar, model.GetBigPairId(14, PracticeRepositoryEntityKey, 20000000))
				ELSE CONVERT(nvarchar,PracticeRepositoryEntityKey)
	        END AS PracticeRepositoryEntityKey
	        ,ExternalSystemEntityId
	        ,ExternalSystemEntityKey
        FROM [dbo].[ExternalSystemEntityMapping]

        SET IDENTITY_INSERT [model].[ExternalSystemEntityMappings] OFF

        -- ExternalSystemExternalSystemMessageTypes
        SET IDENTITY_INSERT [model].[ExternalSystemExternalSystemMessageTypes] ON

        INSERT [model].[ExternalSystemExternalSystemMessageTypes] (
	        Id
	        ,ExternalSystemId
	        ,ExternalSystemMessageTypeId
	        )
		(
        SELECT v.Id, v.ExternalSystemId, v.ExternalSystemMessageTypeId
        FROM
        (SELECT Id
	        ,(SELECT Id FROM model.ExternalSystems WHERE Name = (SELECT Name FROM dbo.ExternalSystem WHERE Id = ExternalSystemId)) AS ExternalSystemId
	        ,CASE ExternalSystemMessageTypeId
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'ACK_V23'
				        )
			        THEN 1
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'ADT_A04_V23'
				        )
			        THEN 2
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'ADT_A08_V23'
				        )
			        THEN 3
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'ADT_A28_V23'
				        )
			        THEN 4
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'ADT_A31_V23'
				        )
			        THEN 5
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'SIU_S12_V23'
				        )
			        THEN 6
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'SIU_S13_V23'
				        )
			        THEN 7
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'SIU_S14_V23'
				        )
			        THEN 8
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'SIU_S15_V23'
				        )
			        THEN 9
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'SIU_S17_V23'
				        )
			        THEN 10
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'SIU_S26_V23'
				        )
			        THEN 11
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'MFN_M02_V23'
				        )
			        THEN 13
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'DFT_P03_V231'
				        )
			        THEN 14
		        WHEN (
				        SELECT TOP 1 Id
				        FROM [dbo].[ExternalSystemMessageType]
				        WHERE [Name] = 'ORM_O01_V231'
				        )
			        THEN 15
		        END AS ExternalSystemMessageTypeId          
        FROM dbo.[ExternalSystemExternalSystemMessageType]) v
        WHERE v.ExternalSystemMessageTypeId IS NOT NULL)
	END
        SET IDENTITY_INSERT [model].[ExternalSystemExternalSystemMessageTypes] OFF
        