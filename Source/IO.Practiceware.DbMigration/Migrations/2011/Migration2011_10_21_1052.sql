﻿---CK Constraints to prevent ambiguous data

----Resources
--No ResourceType Bs
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Resources_ResourceType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Resources]'))
ALTER TABLE [dbo].Resources DROP CONSTRAINT CK_Resources_ResourceType
GO

ALTER TABLE dbo.Resources WITH CHECK  ADD CONSTRAINT CK_Resources_ResourceType
    CHECK (ResourceType <> 'B')
GO

--Change D to Z; Q to Y; others to X
IF OBJECT_ID('ResourceResourceTypeDoctorTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER ResourceResourceTypeDoctorTrigger')
END

GO 

CREATE TRIGGER ResourceResourceTypeDoctorTrigger
ON dbo.Resources
FOR UPDATE
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (SELECT COUNT(*)
	FROM deleted d
	INNER JOIN inserted i ON d.ResourceId = i.ResourceId
	WHERE d.ResourceType = 'D' AND i.ResourceType NOT IN ('Z', 'D'))
	<> 0
	
BEGIN
	RAISERROR ('ResourceType D (Doctor) can only be changed to Archived (Z) ResourceType', 16,1)
	ROLLBACK TRANSACTION
END
GO


----PatientDemogrpahics
---- When PolicyPatientId = PatientId, Relationship must be 'Y' and patient cannot equal the policy holder with relationship not self
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientDemographics_ValidPolicyHolderRelationship]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientDemographics]'))
ALTER TABLE [dbo].PatientDemographics DROP CONSTRAINT [CK_PatientDemographics_ValidPolicyHolderRelationship]
GO

ALTER TABLE [dbo].PatientDemographics WITH CHECK  ADD CONSTRAINT [CK_PatientDemographics_ValidPolicyHolderRelationship]
    CHECK ((Relationship = 'Y' AND PolicyPatientId = PatientId) OR (Relationship <> 'Y' AND PolicyPatientId <> PatientId) OR dbo.GetNoCheck() = 1)
GO

-- The VB6 code inserts a row with PolicyPatientId = 0. So we cannot check for PolicyPatient=0.
-- PolicyPatientId <> SecondPolicyPatientId
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientDemographics_SecondPolicyPatientId]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientDemographics]'))
ALTER TABLE [dbo].PatientDemographics DROP CONSTRAINT CK_PatientDemographics_SecondPolicyPatientId
GO
ALTER TABLE dbo.PatientDemographics WITH CHECK  ADD CONSTRAINT CK_PatientDemographics_SecondPolicyPatientId
    CHECK (PolicyPatientId <> SecondPolicyPatientId OR PolicyPatientId = 0)
GO


IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientFinancial_FinancialEndDateStatusX]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientFinancial]'))
ALTER TABLE [dbo].PatientFinancial DROP CONSTRAINT CK_PatientFinancial_FinancialEndDateStatusX
GO
ALTER TABLE dbo.PatientFinancial WITH CHECK  ADD CONSTRAINT CK_PatientFinancial_FinancialEndDateStatusX
    CHECK ((Status <> 'X' AND FinancialEndDate <> '' OR FinancialEndDate IS NOT NULL)  OR dbo.GetNoCheck() = 1)
GO


--Appointments
--Valid Patient
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Appointments_PatientDemographics]') AND parent_object_id = OBJECT_ID(N'[dbo].[Appointments]'))
ALTER TABLE [dbo].Appointments DROP CONSTRAINT [FK_Appointments_PatientDemographics]
GO

ALTER TABLE [dbo].[Appointments]  WITH CHECK ADD  CONSTRAINT [FK_Appointments_PatientDemographics] FOREIGN KEY([PatientId])
REFERENCES [dbo].[PatientDemographics] ([PatientId])
GO

ALTER TABLE [dbo].[Appointments] CHECK CONSTRAINT [FK_Appointments_PatientDemographics]
GO

--valid appointmenttype
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Appointments_AppointmentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Appointments]'))
ALTER TABLE [dbo].Appointments DROP CONSTRAINT [FK_Appointments_AppointmentType]
GO

ALTER TABLE [dbo].[Appointments]  WITH CHECK ADD  CONSTRAINT [FK_Appointments_AppointmentType] FOREIGN KEY([AppTypeId])
REFERENCES [dbo].[AppointmentType] ([ApptypeId])
GO

ALTER TABLE [dbo].[Appointments] CHECK CONSTRAINT [FK_Appointments_AppointmentType]
GO


--pre-certs are not used for two different patients and valid precert
IF OBJECT_ID('AppointmentsPreCertTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER AppointmentsPreCertTrigger')
END

GO

CREATE TRIGGER AppointmentsPreCertTrigger
ON Appointments
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF EXISTS (SELECT i.AppointmentId
	FROM Appointments ap WITH(NOLOCK) 
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON i.PreCertId = ap.PreCertId 
	WHERE i.PatientId <> ap.PatientId 
		AND i.PreCertId <> 0 AND i.PrecertId IS NOT NULL
		
	UNION ALL
	
	SELECT i.AppointmentId
	FROM Appointments ap
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON i.PreCertId = ap.PreCertId
	LEFT JOIN PatientPreCerts pCert ON pCert.PreCertId = i.Precertid
	WHERE (i.PreCertId <> 0 AND i.PreCertId IS NOT NULL)
	AND pCert.PrecertId IS NULL )
	AND dbo.GetNoCheck() = 0
	
BEGIN
	RAISERROR ('Pre-certs must have a valid patient and be linked to only one patient.',16,1)
	ROLLBACK TRANSACTION
END

GO

--Trigger appointments to ensure referrals are valid and not used for two different patients 
IF OBJECT_ID('AppointmentReferralTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER AppointmentReferralTrigger')
END

GO

CREATE TRIGGER AppointmentReferralTrigger
ON Appointments
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF EXISTS (SELECT ap.AppointmentId
	FROM Appointments ap WITH(NOLOCK) 
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON i.ReferralId = ap.ReferralId
	WHERE i.PatientId <> ap.PatientId 
		AND i.ReferralId <> 0 AND i.ReferralId IS NOT NULL
		
	UNION ALL
	
	SELECT ap.AppointmentId
	FROM Appointments ap
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON i.ReferralId = ap.ReferralId
	LEFT JOIN PatientReferral pref ON pref.ReferralId = i.ReferralId
	WHERE (i.ReferralId <> 0 AND i.ReferralId IS NOT NULL)
	AND pref.ReferralId IS NULL)
	AND dbo.GetNoCheck() = 0
	
BEGIN
	RAISERROR ('A single referral cannot be used for more than 1 patient and referral must be valid.',16,1)
	ROLLBACK TRANSACTION
END

GO



--PatientReceivables Bill to Doctors don't match
IF OBJECT_ID('PatientReceivablesBillToDrTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablesBillToDrTrigger')
END

GO

CREATE TRIGGER PatientReceivablesBillToDrTrigger
ON PatientReceivables
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (((SELECT COUNT(*) 
	FROM PatientReceivables pr WITH (NOLOCK)
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON pr.Invoice = i.Invoice
	WHERE i.BillToDr <> pr.BillToDr) <> 0) 
	AND dbo.GetNoCheck() = 0)
	   
BEGIN
	RAISERROR ('BillToDr must be the same for all receivables attached to an invoice.',16,1)
	ROLLBACK TRANSACTION
END

GO


--Valid Appointments.ResourceId2
IF OBJECT_ID('AppointmentsResourceId2ResourcesTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER AppointmentsResourceId2ResourcesTrigger')
END

GO

CREATE TRIGGER AppointmentsResourceId2ResourcesTrigger
ON Appointments
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (((SELECT COUNT(*)
	FROM (SELECT * FROM inserted  EXCEPT SELECT * FROM deleted) i
	WHERE i.ResourceId2 NOT IN (SELECT ResourceId FROM Resources WHERE ServiceCode = '02' AND ResourceType = 'R') 
	AND i.ResourceId2 BETWEEN 1 AND 999 OR i.ResourceId2 > 1999) <> 0) 
	AND dbo.GetNoCheck() = 0)
	   
BEGIN
	RAISERROR ('Invalid appointment location (Appointments.ResourceId2) Location from Resources Table',16,1)
	ROLLBACK TRANSACTION
END

GO

IF OBJECT_ID('AppointmentsResourceId2PracticeSatelliteTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER AppointmentsResourceId2PracticeSatelliteTrigger')
END

GO

CREATE TRIGGER AppointmentsResourceId2PracticeSatelliteTrigger
ON Appointments
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF EXISTS (SELECT *
		FROM (SELECT * FROM inserted  EXCEPT SELECT * FROM deleted) i
		LEFT JOIN PracticeName pn WITH(NOLOCK) ON (i.ResourceId2 -1000) = pn.PracticeId 
		WHERE i.ResourceId2 BETWEEN 1000 AND 1999
			AND pn.PracticeId IS NULL)
	AND dbo.GetNoCheck() = 0

BEGIN
	RAISERROR ('Invalid appointment location (Appointments.ResourceId2) where practiceType = P and ResourceId2 between 1 and 2 thousand',16,1)
	ROLLBACK TRANSACTION
END
GO


--Valid appointemnts.ScheduleStatus
IF OBJECT_ID('AppointmentsTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER AppointmentsTrigger')
END

GO

CREATE TRIGGER AppointmentsTrigger
ON Appointments
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF  EXISTS (SELECT *
	FROM (SELECT * FROM inserted  EXCEPT SELECT * FROM deleted) i
	LEFT JOIN PracticeCodeTable pct WITH(NOLOCK) ON i.ScheduleStatus = SUBSTRING(pct.Code, 1, 1)
	WHERE i.Comments LIKE 'Add%'
	AND pct.id IS NULL) 
	AND dbo.GetNoCheck() = 0
	   
BEGIN
	RAISERROR ('Appointments must have valid ScheduleStatus.',16,1)
	ROLLBACK TRANSACTION
END
GO

--Valid Precerts


--practice activity invalid statuses
IF OBJECT_ID('PracticeActivityStatusTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeActivityStatusTrigger')
END

GO

CREATE TRIGGER PracticeActivityStatusTrigger
ON PracticeActivity
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF EXISTS (
	SELECT i.ActivityId
	FROM PracticeActivity pa WITH(NOLOCK) 
	INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON i.ActivityId = pa.ActivityId
	INNER JOIN Appointments ap ON pa.AppointmentId = ap.AppointmentId 
	WHERE i.[Status] IN (
			'E'
			,'G'
			,'W'
			,'M'
			)
	AND ap.ScheduleStatus NOT IN ('A', 'P', 'R')

) AND dbo.GetNoCheck() = 0

BEGIN
	RAISERROR ('Invalid practice activity status. Check the Appointment ScheduleStatus and compare to the practice activity status.',16,1)
	ROLLBACK TRANSACTION
END
GO


--Trigger on practice activity to prevent duplicate activity records
IF OBJECT_ID('PracticeActivityMultipleActivityRecordsTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeActivityMultipleActivityRecordsTrigger')
END

GO

CREATE TRIGGER PracticeActivityMultipleActivityRecordsTrigger
ON PracticeActivity
FOR INSERT
AS
SET NOCOUNT ON
IF EXISTS(
		SELECT * 
		FROM PracticeActivity pa WITH(NOLOCK) 
		INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON i.AppointmentId = pa.AppointmentId
		GROUP BY pa.AppointmentId
		HAVING COUNT(*) > 1)
	AND dbo.GetNoCheck() = 0
	
BEGIN
	RAISERROR ('Cannot create duplicate activity record for this appointment.',16,1)
	ROLLBACK TRANSACTION
END
GO


--PatientReferrals
---FK on Resources Table and ReferredTo column
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PatientReferral_Resources]') AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReferral]'))
ALTER TABLE [dbo].[PatientReferral] DROP CONSTRAINT [FK_PatientReferral_Resources]
GO

ALTER TABLE [dbo].[PatientReferral] WITH CHECK ADD  CONSTRAINT [FK_PatientReferral_Resources] FOREIGN KEY([ReferredTo])
REFERENCES [dbo].[Resources] ([ResourceId])
GO

ALTER TABLE [dbo].[PatientReferral] CHECK CONSTRAINT [FK_PatientReferral_Resources]
GO