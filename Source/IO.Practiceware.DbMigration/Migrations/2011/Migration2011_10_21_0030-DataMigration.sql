﻿---PracticeName, PracticeServices, PracticeCodeTable data cleanup


---------PracticeCalendar
DELETE pCal
FROM PracticeCalendar pCal
LEFT JOIN Resources re ON re.ResourceId = pCal.CalendarResourceId
WHERE re.ResourceId IS NULL


----------PracticeName
-- Validate LocationReferenceType
----Try to fix
UPDATE pn2 SET pn2.LocationReference = pn2.LocationReference + '2'
FROM PracticeName pn
INNER JOIN PracticeName pn2 ON pn2.LocationReference = pn.LocationReference
AND pn.PracticeType = 'P'
AND pn2.PracticeType = 'P'
AND pn2.PracticeId > pn.PracticeId

---Make sure there are no other dupes
IF (SELECT COUNT(*) FROM PracticeName
WHERE PracticeType = 'P'
GROUP BY LocationReference
HAVING COUNT(*) > 1) <> 0 
	RAISERROR('Duplicate LocationReference. Please fix then continue.', 16, 1)



------PracticeServices
--Deletes duplicate services in the PracticeService Table
DECLARE @toReplace table (Original int, Replacement int)

INSERT INTO @toReplace
SELECT * FROM (
	SELECT prs.CodeId AS Original, v.CodeId AS Replacement FROM (
		SELECT MAX(CodeId) AS CodeId, v.Code FROM (
			SELECT prs.Code AS Code, MAX(prs.Fee) AS Fee
			FROM PracticeServices Prs
			GROUP BY prs.Code
			HAVING COUNT(*) >1
		) v 
		INNER JOIN PracticeServices prs ON v.Code = prs.Code AND v.Fee = prs.Fee
		GROUP BY v.Code
	) v
	INNER JOIN PracticeServices prs ON v.Code = prs.Code
) v
WHERE v.Original <> v.Replacement

DELETE ps 
FROM PracticeServices ps
INNER JOIN @toReplace tr on tr.Original = ps.CodeId




----PracticeCodeTable


-- This is to delete duplicate PaymentTypes in the Practice Code Table
DECLARE @duplicates table (Id int)
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, LEN(pct1.Code), 1) = SUBSTRING(pct2.Code, LEN(pct2.Code), 1) AND pct2.ReferenceType = 'PAYMENTTYPE' 
	WHERE pct1.ReferenceType like 'PAYMENTTYPE%' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)

GO


----Clean up null or blank Employee statuses and races
DELETE
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'ETHNICITY' AND (Code IS NULL OR Code = '')

DELETE
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'RACE' AND (Code IS NULL OR Code = '')

DELETE
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'EMPLOYEESTATUS' AND (Code IS NULL OR Code = '')



-- This is to delete duplicate employee statuses in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1, 1) = SUBSTRING(pct2.Code, 1, 1) AND pct2.ReferenceType = 'EMPLOYEESTATUS' 
	WHERE pct1.ReferenceType like 'EMPLOYEESTATUS' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- This is to delete duplicate RACEs in the Practice Code Table
DECLARE @duplicates table (Id int)
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON pct1.Code = pct2.Code AND pct2.ReferenceType = 'RACE' 
	WHERE pct1.ReferenceType like 'RACE' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- This is to delete duplicate ETHNICITIES in the Practice Code Table
DECLARE @duplicates table (Id int)
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON pct1.Code = pct2.Code AND pct2.ReferenceType = 'ETHNICITY' 
	WHERE pct1.ReferenceType like 'ETHNICITY' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- Delete null insurerBusiness class names
DELETE FROM PracticeCodeTable 
WHERE ReferenceType = 'BUSINESSCLASS'
	AND (Code IS NULL  OR Code = '')

--Adds business classes to the Practiecode table that are in the ENUM, ModifierRules or PracticeInsurers tables
INSERT INTO PracticeCodeTable (ReferenceType, Code, [Rank])
SELECT distinct * FROM (
		select 'BUSINESSCLASS' AS ReferenceType, BusinessClassId as bc, 99 AS [Rank] from ModifierRules
	union
		select 'BUSINESSCLASS' AS ReferenceType, insurerbusinessclass as bc, 99 AS [Rank] from PracticeInsurers
	union
		select 'BUSINESSCLASS' AS ReferenceType, 'AMERIH' as bc, 99 AS [Rank] 
	union
		select 'BUSINESSCLASS' AS ReferenceType, 'BLUES' as bc , 99 AS [Rank] 
	union
		select 'BUSINESSCLASS' AS ReferenceType, 'COMM' as bc, 99 AS [Rank] 
	union
		select 'BUSINESSCLASS' AS ReferenceType, 'MCAIDFL' as bc, 99 AS [Rank] 
	union
		select 'BUSINESSCLASS' AS ReferenceType, 'MCAIDNC' as bc, 99 AS [Rank] 
	union
		select 'BUSINESSCLASS' AS ReferenceType, 'MCAIDNJ' as bc, 99 AS [Rank] 
	union
		select 'BUSINESSCLASS' AS ReferenceType, 'MCAIDNV' as bc, 99 AS [Rank] 
	union
		select 'BUSINESSCLASS' AS ReferenceType, 'MCAIDNY' as bc, 99 AS [Rank] 
	union
		select 'BUSINESSCLASS' AS ReferenceType, 'MCARENJ' as bc, 99 AS [Rank] 
	union
		select 'BUSINESSCLASS' AS ReferenceType, 'MCARENV' as bc, 99 AS [Rank] 
	union
		select 'BUSINESSCLASS' AS ReferenceType, 'MCARENY' as bc, 99 AS [Rank] 
	union
		select 'BUSINESSCLASS' AS ReferenceType, 'TEMPLATE' as bc, 99 AS [Rank]
 	) AS v
WHERE v.bc not in (select code from PracticeCodeTable where ReferenceType = 'BUSINESSCLASS')
	AND v.bc <> '' 
	AND v.bc IS NOT NULL


--Doctor Specialty types that are not in PCT table
INSERT INTO PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, Signature, TestOrder, FollowUp, [Rank])
SELECT distinct 'SPECIALTY', VendorSpecialty, '1', 'F', '', '', '', 'F', '', '', 1
FROM PracticeVendors pv
WHERE VendorSpecialty NOT IN (SELECT Code FROM PracticeCodeTable pct WHERE ReferenceType = 'SPECIALTY')
AND VendorSpecialty <> '' and VendorSpecialty IS NOT NULL


-- This is to delete duplicate laguanges in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON pct1.Code = pct2.Code AND pct2.ReferenceType = 'LANGUAGE' 
	WHERE pct1.ReferenceType like 'LANGUAGE' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- Trim languages less than 10 char
UPDATE PracticeCodeTable 
SET Code = SUBSTRING(Code, 1, 10)
WHERE (ReferenceType = 'Language') AND (LEN(Code) > 10)

-- This is to delete duplicate transmit types in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1, 1)= SUBSTRING(pct2.Code, 1, 1) AND pct2.ReferenceType = 'TRANSMITTYPE' 
	WHERE pct1.ReferenceType like 'TRANSMITTYPE' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- This is to delete duplicate BUSINESSCLASS in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON pct1.Code = pct2.Code AND pct2.ReferenceType = 'BUSINESSCLASS' 
	WHERE pct1.ReferenceType like 'BUSINESSCLASS' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- This is to delete duplicate VENDORTYPE in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1, 1) = SUBSTRING(pct2.Code, 1, 1) AND pct2.ReferenceType = 'VENDORTYPE' 
	WHERE pct1.ReferenceType like 'VENDORTYPE' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- This is to delete duplicate SCANTYPE in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON pct1.Code = pct2.Code AND pct2.ReferenceType = 'SCANTYPE' 
	WHERE pct1.ReferenceType like 'SCANTYPE' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- This is to delete duplicate REFERBY in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON pct1.Code = pct2.Code AND pct2.ReferenceType = 'REFERBY' 
	WHERE pct1.ReferenceType like 'REFERBY' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO


-- This is to delete duplicate PLANTYPE in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON pct1.Code = pct2.Code AND pct2.ReferenceType = 'PLANTYPE' 
	WHERE pct1.ReferenceType like 'PLANTYPE' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- This is to delete duplicate PCCHOICES in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON pct1.Code = pct2.Code AND pct2.ReferenceType = 'PCCHOICES' 
	WHERE pct1.ReferenceType like 'PCCHOICES' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- This is to delete duplicate PAYMENTREASON in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON pct1.Code = pct2.Code AND pct2.ReferenceType = 'PAYMENTREASON' 
	WHERE pct1.ReferenceType like 'PAYMENTREASON' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- This is to delete duplicate PATIENTSTATUS in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1, 1) = SUBSTRING(pct2.Code, 1, 1) AND pct2.ReferenceType = 'PATIENTSTATUS' 
	WHERE pct1.ReferenceType like 'PATIENTSTATUS' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO


-- This is to delete duplicate MARITAL in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1, 1) = SUBSTRING(pct2.Code, 1, 1)
		AND pct2.ReferenceType = 'MARITAL' 
	WHERE pct1.ReferenceType like 'MARITAL' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- This is to delete duplicate INSURERREF in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1, 1) = SUBSTRING(pct2.Code, 1, 1)
		AND pct2.ReferenceType = 'INSURERREF' 
	WHERE pct1.ReferenceType like 'INSURERREF' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO


-- This is to delete duplicate GENDER in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1, 1) = SUBSTRING(pct2.Code, 1, 1)
		AND pct2.ReferenceType = 'GENDER' 
	WHERE pct1.ReferenceType like 'GENDER' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO


-- This is to delete duplicate CONTACTTYPE in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1, 1) = SUBSTRING(pct2.Code, 1, 1)
		AND pct2.ReferenceType = 'CONTACTTYPE' 
	WHERE pct1.ReferenceType like 'CONTACTTYPE' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO


-- This is to delete duplicate CONFIRMSTATUS in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1, 1) = SUBSTRING(pct2.Code, 1, 1)
		AND pct2.ReferenceType = 'CONFIRMSTATUS' 
	WHERE pct1.ReferenceType like 'CONFIRMSTATUS' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO


-- This is to delete duplicate CLORDERSTATUS in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.AlternateCode AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON pct1.AlternateCode = pct2.AlternateCode
		AND pct2.ReferenceType = 'CLORDERSTATUS' 
	WHERE pct1.ReferenceType like 'CLORDERSTATUS' 
	) v
GROUP BY v.Code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

-- Delete duplicate SERVICEMODS from PracticeCodeTable
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT SUBSTRING(pct1.Code, 1,2) AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1,2) = SUBSTRING(pct2.Code, 1,2)
		AND pct2.ReferenceType = 'SERVICEMODS' 
	WHERE pct1.ReferenceType like 'SERVICEMODS' 
	) v
GROUP BY v.Code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO


-- This is to delete duplicate MEDICARESECONDARY in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1, (dbo.GetMax((CHARINDEX(' ', pct1.Code)),0))) = SUBSTRING(pct2.Code, 1, (dbo.GetMax((CHARINDEX(' ', pct2.Code)),0)))  
		AND pct2.ReferenceType = 'MEDICARESECONDARY' 
	WHERE pct1.ReferenceType like 'MEDICARESECONDARY' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

--Payment types without letter translations
IF EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PAYMENTTYPE' AND (LetterTranslation IS NULL OR LetterTranslation = ''))

BEGIN
	DECLARE @temp table (Id int, Code nvarchar(Max), ShortName nvarchar(max))
	
	INSERT INTO @temp (Id, Code, ShortName) 
	SELECT Id, Code, SUBSTRING(Code, 1,5) FROM PracticeCodeTable WHERE ReferenceType = 'PAYMENTTYPE' AND (LetterTranslation IS NULL OR LetterTranslation = '')
	
	UPDATE pct
	SET LetterTranslation = ShortName
	FROM PracticeCodeTable pct
	INNER JOIN @temp t ON t.Id = pct.Id
	
END
GO

--PracticeCalendar
DELETE FROM PracticeCalendar WHERE  CalendarResourceId = 0