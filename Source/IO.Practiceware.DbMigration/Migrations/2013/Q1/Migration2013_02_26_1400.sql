﻿IF EXISTS (SELECT * FROM sys.objects so INNER JOIN sys.columns sc ON so.object_id =sc.object_id WHERE so.name = 'InsurerClaimForms' AND sc.Name = 'IsArchived')
BEGIN

--Add more metadata for table InsurerClaimForms

EXEC('
--cms1500

INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived) 
SELECT 1 AS ClaimFormTypeId
    , InsurerId As InsurerId
    , 1 AS InvoiceTypeId
    , 0 AS IsArchived    
FROM dbo.PracticeInsurers
EXCEPT SELECT ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived FROM [model].[InsurerClaimForms]


--cms1500
INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived)
SELECT 1 AS ClaimFormTypeId
    , InsurerId As InsurerId
    , 3 AS InvoiceTypeId
    , 0 AS IsArchived    
FROM dbo.PracticeInsurers
EXCEPT SELECT ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived FROM [model].[InsurerClaimForms]




--837p
INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived)
SELECT 3 AS ClaimFormTypeId
    , InsurerId As InsurerId
    , 1 AS InvoiceTypeId
    , 0 AS IsArchived    
FROM dbo.PracticeInsurers
EXCEPT SELECT ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived FROM [model].[InsurerClaimForms]


--837p
INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived)
SELECT 3 AS ClaimFormTypeId
    , InsurerId As InsurerId
    , 3 AS InvoiceTypeId
    , 0 AS IsArchived    
FROM dbo.PracticeInsurers
EXCEPT SELECT ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived FROM [model].[InsurerClaimForms]


--facility cms1500
INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived)
SELECT 1 AS ClaimFormTypeId
    , InsurerId As InsurerId
    , 2 AS InvoiceTypeId
    , 0 AS IsArchived    
FROM dbo.PracticeInsurers
WHERE InsurerReferenceCode <> ''P'' AND InsurerName NOT LIKE ''%OXFORD%''
EXCEPT SELECT ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived FROM [model].[InsurerClaimForms]


--facility 837p
INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived)
SELECT 3 AS ClaimFormTypeId
    , InsurerId As InsurerId
    , 2 AS InvoiceTypeId
    , 0 AS IsArchived    
FROM dbo.PracticeInsurers
WHERE InsurerReferenceCode <> ''P'' AND InsurerName NOT LIKE ''%OXFORD%''
EXCEPT SELECT ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived FROM [model].[InsurerClaimForms]

IF EXISTS(SELECT * FROM model.Permissions WHERE Id  = 9 AND Name = ''Disbursements'') --Found a row on Zelko database that had this value
BEGIN
	UPDATE model.Permissions SET Name = ''Submit Transactions'' WHERE Id = 9
END

IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Name = ''Submit Transactions'' AND Id  = 9)  AND NOT EXISTS(SELECT * FROM model.[Permissions] WHERE Name = ''CreateGlobalAndResourceScheduleComments'' AND Id= 9)
BEGIN

-- add new ''submit'' permission to model.permissions
	SET IDENTITY_INSERT [model].[Permissions] ON

	INSERT INTO [model].[Permissions] (Id, Name) VALUES (9, ''Submit Transactions'')

	SET IDENTITY_INSERT [model].[Permissions] OFF
END


IF NOT EXISTS(SELECT * FROM model.ExternalSystems WHERE Name = ''ClaimFileMessage'' AND Id = 4)
BEGIN 
-- add new ExternalSystem to model.ExternalSystems
	SET IDENTITY_INSERT[model].[ExternalSystems] ON

	INSERT INTO model.ExternalSystems(Id,Name)
	VALUES (4, ''ClaimFileMessage'')

	SET IDENTITY_INSERT[model].[ExternalSystems] OFF
END

-- add new ExternalSystemMessageType to model.ExternalSystemMessageTypes

IF EXISTS (SELECT * FROM model.ExternalSystemMessageTypes WHERE Name <> ''PaperClaim_Outbound'' AND Id = 30)
BEGIN 
	DECLARE @SeedValue int
	SET @SeedValue = (SELECT CASE WHEN MAX(Id) > 1000 THEN MAX(Id) ELSE 1000 END FROM model.ExternalSystemMessageTypes)

	SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] ON

	INSERT INTO model.ExternalSystemMessageTypes (Id, Name, Description, IsOutBound)
	SELECT @SeedValue, Name, Description, IsOutBound
	FROM model.ExternalSystemMessageTypes
	WHERE Name <> ''PaperClaim_Outbound'' AND Id = 30
	
	UPDATE model.ExternalSystemExternalSystemMessageTypes
	SET ExternalSystemMessageTypeId = @SeedValue 
	WHERE ExternalSystemMessageTypeId = 30

	SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] OFF

	UPDATE model.ExternalSystemMessageTypes
	SET Name = ''PaperClaim_Outbound''
	WHERE Id = 30 AND Name <> ''PaperClaim_Outbound''

END


IF NOT EXISTS(SELECT * FROM model.ExternalSystemMessageTypes WHERE Name = ''PaperClaim_Outbound'' AND Id = 30)
BEGIN 
	SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] ON

	INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description], IsOutbound)
	VALUES (30, ''PaperClaim_Outbound'', '''', 1)

	SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] OFF
END


-- add 2 rows to ExternalSystemExternalSystemMessageType table. one for paper claims and one for x12 messages.
IF NOT EXISTS(SELECT * FROM model.ExternalSystemExternalSystemMessageTypes WHERE ExternalSystemId = 4 AND ExternalSystemMessageTypeId = 17)
BEGIN 
	INSERT INTO model.ExternalSystemExternalSystemMessageTypes
	(ExternalSystemId, ExternalSystemMessageTypeId)
	values (4, 17)
END

IF NOT EXISTS(SELECT * FROM model.ExternalSystemExternalSystemMessageTypes WHERE ExternalSystemId = 4 AND ExternalSystemMessageTypeId = 30)
BEGIN 
	INSERT INTO model.ExternalSystemExternalSystemMessageTypes
	(ExternalSystemId, ExternalSystemMessageTypeId)
	values (4, 30)
END


IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = ''PracticeCodeTable'' AND sc.Name = ''WebsiteUrl'')
BEGIN
	-- add WebsiteUrl column to claim file receivers table
	ALTER TABLE dbo.PracticeCodeTable
	ADD WebsiteUrl nvarchar(512) null
END

IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = ''ServiceTransactions'' AND sc.Name = ''ExternalSystemMessageId'')
BEGIN
	-- add foreign key externalSystemMessageId to BillingServiceTransactions table
	ALTER TABLE dbo.ServiceTransactions
	ADD ExternalSystemMessageId uniqueidentifier null
END

')

END

GO