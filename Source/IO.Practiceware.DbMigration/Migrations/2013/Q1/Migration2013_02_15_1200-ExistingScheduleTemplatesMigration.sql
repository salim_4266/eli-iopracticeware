﻿IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit


SELECT 1 AS Value INTO #NoAudit

IF OBJECT_ID('model.ServiceLocations') IS NULL
EXEC('CREATE VIEW model.ServiceLocations AS SELECT 0 AS Id, '''' AS ShortName')

--model.ScheduleTemplate
--Delete any templates that are named Unavailable
DELETE FROM PracticeName WHERE PracticeName = 'Unavailable'
--Set the Unavailable template
INSERT INTO model.ScheduleTemplates
(StartTime,Name)
VALUES
('2000-01-01 07:00:00.000','Unavailable')
--Grab all the other templates the practice has
INSERT INTO model.ScheduleTemplates
(StartTime,Name)
	SELECT
	DATEADD(yy,100,CONVERT(DATETIME,StartTime1))
	,PracticeName+'|'+CONVERT(NVARCHAR,PracticeId) 
	FROM PracticeName 
	WHERE PracticeType = 'T'
		AND PracticeId NOT IN
		(
			SELECT 
			PracticeId 
			FROM PracticeName 
			WHERE CONVERT(DATETIME,StartTime1)>CONVERT(DATETIME,StartTime2) 
				AND StartTime2<>'' 
				AND PracticeType='T'
		)
	ORDER BY PracticeName

--model.ScheduleTemplateBlocks
--Break down PracticeName.PracticeCity into block locations with respective times and store into #DerivedTablewithIntervals

IF OBJECT_ID(N'dbo.Chunk', N'TF') IS NOT NULL
    DROP FUNCTION dbo.Chunk
GO
EXEC('
CREATE FUNCTION dbo.Chunk(@text nvarchar(max), @size int)
RETURNS @result table(Value nvarchar(max))
AS
BEGIN

DECLARE @currentChunk nvarchar(max)

WHILE @text <> '''' AND @text IS NOT NULL
BEGIN
	SET @currentChunk = SUBSTRING(@text, 1, CASE WHEN LEN(@text) > @size THEN @size ELSE LEN(@text) END)
	INSERT @result(Value) VALUES(@currentChunk)
	SET @text = SUBSTRING(@text, LEN(@currentChunk) + 1, LEN(@text) - LEN(@currentChunk))
END
RETURN 
END
')

DECLARE @X XML
SET @X= (
SELECT 
CONVERT(XML,'<s id="'+cast(PracticeId as varchar)+'">' + REPLACE(
STUFF(
		(
		SELECT 
		Value + ' ' 
		FROM dbo.Chunk(
			(SELECT 
			PracticeCity
			FROM PracticeName pn2 
			WHERE PracticeType = 'T'
			AND pn2.PracticeId = pn1.PracticeId)
		,4) 
		FOR XML PATH(''), TYPE)
		.value('.','VARCHAR(MAX)'),1,0,''
	)
,' ','</s><s id="'+cast(PracticeId as varchar)+'">') + '</s>') 
FROM PracticeName pn1 WHERE PracticeType='T'		
AND PracticeId NOT IN
(
SELECT 
PracticeId 
FROM PracticeName 
WHERE CONVERT(DATETIME,StartTime1)>CONVERT(DATETIME,StartTime2) 
	AND StartTime2<>'' 
	AND PracticeType='T'
)
FOR XML RAW)

;WITH CTE AS
(
SELECT 
	ROW_NUMBER() OVER (ORDER BY c.value('(@id)[1]','int')) AS RowNum,
	c.value('(@id)[1]','int') AS PracticeId, 
	CASE 
	WHEN c.value('.','varchar(50)') IN ('0','-1') --main office
		THEN (SELECT TOP 1 Id FROM model.ServiceLocations WHERE ShortName='Office') 
	WHEN c.value('.','varchar(50)') > 1000 --secondary office
		THEN c.value('.','varchar(50)')-1000 
	WHEN c.value('.','varchar(50)') > 0 --surgical center
		THEN c.value('.','varchar(50)')+110000000 
	END AS Location,
	ROW_NUMBER() OVER (PARTITION BY c.value('(@id)[1]','int') ORDER BY c.value('(@id)[1]','int')) AS Interval
FROM @X.nodes('/row/s') T(c)
WHERE c.value('.','varchar(50)')<>''
)
SELECT 
Q.*
,CASE 
	WHEN Q.Interval = 1
	THEN CONVERT(DATETIME,StartTime1)
	WHEN Q.Interval = 2
	THEN CONVERT(DATETIME,StartTime2)
	WHEN Q.Interval = 3
	THEN CONVERT(DATETIME,StartTime3)
	WHEN Q.Interval = 4 
	THEN CONVERT(DATETIME,StartTime4)
END AS StartTime
,CASE
	WHEN Q.Interval = 1
	THEN CONVERT(DATETIME,EndTime1)
	WHEN Q.Interval = 2
	THEN CONVERT(DATETIME,EndTime2)
	WHEN Q.Interval = 3
	THEN CONVERT(DATETIME,EndTime3)
	WHEN Q.Interval = 4
	THEN CONVERT(DATETIME,EndTime4)
END AS EndTime
INTO #DerivedTablewithIntervals
FROM CTE Q
JOIN PracticeName pn ON pn.PracticeId=Q.PracticeId

--update endtimes where exists starttime in another interval and both are equal
UPDATE de2
SET de2.EndTime=DATEADD(mi,(SELECT DATEDIFF(MINUTE,'2000-01-01 00:00:00.000',(SELECT SpanSize FROM model.ScheduleTemplateBuilderConfigurations)))*-1,de2.EndTime)
FROM #DerivedTablewithIntervals de1
INNER JOIN #DerivedTablewithIntervals de2 ON de2.PracticeId=de1.PracticeId AND de2.EndTime=de1.StartTime
WHERE de1.Interval>1

--2.Start building the ScheduleTemplateblocks
;WITH CTE(RowNum,PracticeId,Location,Interval,StartTime,EndTime)
AS
(
	SELECT
	RowNum,
	PracticeId,
	Location,
	Interval,
	StartTime,
	EndTime
	FROM #DerivedTablewithIntervals

	UNION ALL
	
	SELECT 
	RowNum
	,PracticeId
	,Location
	,Interval
	,DATEADD(mi,(SELECT DATEDIFF(MINUTE,'2000-01-01 00:00:00.000',(SELECT SpanSize FROM model.ScheduleTemplateBuilderConfigurations))),CTE.StartTime)
	,EndTime 
	FROM CTE
	WHERE CTE.StartTime < (SELECT EndTime FROM #DerivedTablewithIntervals WHERE EndTime>=CTE.StartTime AND CTE.RowNum=#DerivedTablewithIntervals.RowNum)
)
SELECT
ROW_NUMBER() OVER (PARTITION BY st.Id ORDER BY CTE.PracticeId,CTE.StartTime) AS OrdinalId
,st.Id AS ScheduleTemplateId
,NULL AS Comment
,sl.Id AS ServiceLocationId
,CASE
	WHEN PracticeName = 'Unavailable'
	THEN 1
	ELSE 0
END AS IsUnavailable
,cte.StartTime
,cte.PracticeId
INTO #LocationsForSTB
FROM CTE
JOIN PracticeName pn ON pn.PracticeId=CTE.PracticeId
JOIN model.ScheduleTemplates st ON st.Name=PracticeName+'|'+CONVERT(NVARCHAR,pn.PracticeId)
JOIN model.ServiceLocations sl ON sl.Id=CTE.Location

--Makes the scheduleblocks
;WITH CTE AS
(
SELECT 
MIN(StartTime) AS StartTime
,MAX(StartTime) AS EndTime
,ScheduleTemplateId
,PracticeId
FROM #LocationsForSTB
GROUP BY ScheduleTemplateId,PracticeId

UNION ALL

SELECT 
DATEADD(mi,(SELECT DATEDIFF(MINUTE,'2000-01-01 00:00:00.000',(SELECT SpanSize FROM model.ScheduleTemplateBuilderConfigurations))),StartTime)
,EndTime
,ScheduleTemplateId
,PracticeId
FROM CTE
WHERE StartTime < EndTime AND EndTime>=StartTime
)
SELECT 
*
INTO #BlocksNoLocations
FROM CTE
ORDER BY ScheduleTemplateId,StartTime

--left join where one table has all the scheduleblocks with the other that has locations
SELECT 
ROW_NUMBER() OVER (PARTITION BY A.ScheduleTemplateId ORDER BY A.StartTime) AS OrdinalId
,A.ScheduleTemplateId 
,NULL AS Comment
,ServiceLocationId
,0 AS IsUnavailable
INTO #FinalSTB
FROM #BlocksNoLocations A
LEFT JOIN #LocationsForSTB B 
	ON B.PracticeId=A.PracticeId 
	AND A.ScheduleTemplateId=B.ScheduleTemplateId 
	AND A.StartTime=B.StartTime

--Fills in the slots where servicelocationid is null
;WITH CTE AS
(
SELECT 
* 
FROM model.#FinalSTB
)
INSERT INTO model.ScheduleTemplateBlocks
(OrdinalId,ScheduleTemplateId,Comment,ServiceLocationId,IsUnavailable)
SELECT 
A.OrdinalId
,A.ScheduleTemplateId
,A.Comment
,ISNULL(A.ServiceLocationId,B.ServiceLocationId) ServiceLocationId
,A.IsUnavailable
FROM CTE A
OUTER APPLY (
	SELECT TOP 1 *
	FROM CTE
	WHERE ScheduleTemplateId=A.ScheduleTemplateId AND OrdinalId<A.OrdinalId
	AND ServiceLocationId IS NOT NULL
	ORDER BY ScheduleTemplateId,OrdinalId DESC
	)B

--model.ScheduleTemplateBlockAppointmentCategories
---------------------------------------------------------------------------------------------
/*
Categories S,I,L,E
*/
----------------------------------------------------------------------------------------------
DECLARE @T DATETIME 
SET @T = '1900-01-01 00:00:00.000'
--Combine CatArrays, split them into Categories
;WITH CTE
AS
(
SELECT 
ROW_NUMBER() OVER (ORDER BY PracticeId) AS RowNum
,SUBSTRING(A.SILE, v.Number + 1, 1) AS NumSlots
,PracticeId
,PracticeName
,@T AS SlotTime
FROM   (
		SELECT PracticeId,
			PracticeName,
			CatArray1+CatArray2 AS SILE
		FROM   PracticeName
		WHERE  PracticeType='T'
			AND PracticeId NOT IN
			(
				SELECT 
				PracticeId 
				FROM PracticeName 
				WHERE CONVERT(DATETIME,StartTime1)>CONVERT(DATETIME,StartTime2) 
					AND StartTime2<>'' 
					AND PracticeType='T'
			)
		) A
JOIN master..Spt_values v ON v.Number < DATALENGTH(A.SILE) / 2
WHERE  v.Type = 'P' AND (DATALENGTH(A.SILE) / 2) = 384
)
SELECT 
	CASE
	WHEN RowNum % 384 <> 0
	THEN DATEADD(mi,(((((RowNum % 384)-1)/4)*15)+15),SlotTime)
	WHEN RowNum % 384 = 0
	THEN DATEADD(mi,1440,CONVERT(DateTime,SlotTime))
	END AS [Time]
,RowNum
,NumSlots
,PracticeId
,PracticeName+'|'+convert(NVARCHAR,PracticeId) AS STName
,Category = 
	CASE RowNum % 4 
	WHEN 1 
	THEN 'S'
	WHEN 2 
	THEN 'I'
	WHEN 3 
	THEN 'L'
	WHEN 0 
	THEN 'E'
	END
INTO #STBAC
FROM CTE
WHERE NumSlots<>''

---------------------------------------------------------------------------------------------
/*
Categories W,X,Y,Z
*/
----------------------------------------------------------------------------------------------
;WITH CTE
AS
(
SELECT 
ROW_NUMBER() OVER (ORDER BY PracticeId) AS RowNum
,SUBSTRING(A.WXYZ, v.Number + 1, 1) AS NumSlots
,PracticeId
,PracticeName
,@T AS SlotTime
FROM   (
		SELECT PracticeId,
			PracticeName,
			CatArray3+CatArray4 AS WXYZ
		FROM   PracticeName
		WHERE  PracticeType='T'
			AND PracticeId NOT IN
			(
				SELECT 
				PracticeId 
				FROM PracticeName 
				WHERE CONVERT(DATETIME,StartTime1)>CONVERT(DATETIME,StartTime2) 
					AND StartTime2<>'' 
					AND PracticeType='T'
			)
		) A
JOIN master..Spt_values v ON v.Number < DATALENGTH(A.WXYZ) / 2
WHERE  v.Type = 'P' AND (DATALENGTH(A.WXYZ) / 2) = 384
)
INSERT INTO #STBAC
([Time],RowNum,NumSlots,PracticeId,STName,Category)
SELECT 
	CASE
	WHEN RowNum % 384 <> 0
	THEN DATEADD(mi,(((((RowNum % 384)-1)/4)*15)+15),SlotTime)
	WHEN RowNum % 384 = 0
	THEN DATEADD(mi,1440,CONVERT(DateTime,SlotTime))
	END AS [Time]
,RowNum
,NumSlots
,PracticeId
,PracticeName+'|'+convert(NVARCHAR,PracticeId) AS STName
,Category = 
	CASE RowNum % 4 
	WHEN 1 
	THEN 'W'
	WHEN 2 
	THEN 'X'
	WHEN 3 
	THEN 'Y'
	WHEN 0 
	THEN 'Z'
	END
FROM CTE
WHERE NumSlots<>''

---------------------------------------------------------------------------------------------
/*
Categories A,B,C,D
*/
----------------------------------------------------------------------------------------------
;WITH CTE
AS
(
SELECT 
ROW_NUMBER() OVER (ORDER BY PracticeId) AS RowNum
,SUBSTRING(A.ABCD, v.Number + 1, 1) AS NumSlots
,PracticeId
,PracticeName
,@T AS SlotTime
FROM   (
		SELECT PracticeId,
			PracticeName,
			CatArray5+CatArray6 AS ABCD
		FROM   PracticeName
		WHERE  PracticeType='T'
			AND PracticeId NOT IN
			(
				SELECT 
				PracticeId 
				FROM PracticeName 
				WHERE CONVERT(DATETIME,StartTime1)>CONVERT(DATETIME,StartTime2) 
					AND StartTime2<>'' 
					AND PracticeType='T'
			)
		) A
JOIN master..Spt_values v ON v.Number < DATALENGTH(A.ABCD) / 2
WHERE  v.Type = 'P' AND (DATALENGTH(A.ABCD) / 2) = 384
)
INSERT INTO #STBAC
([Time],RowNum,NumSlots,PracticeId,STName,Category)
SELECT 
	CASE
	WHEN RowNum % 384 <> 0
	THEN DATEADD(mi,(((((RowNum % 384)-1)/4)*15)+15),SlotTime)
	WHEN RowNum % 384 = 0
	THEN DATEADD(mi,1440,CONVERT(DateTime,SlotTime))
	END AS [Time]
,RowNum
,NumSlots
,PracticeId
,PracticeName+'|'+convert(NVARCHAR,PracticeId) AS STName
,Category = 
	CASE RowNum % 4 
	WHEN 1 
	THEN 'A'
	WHEN 2 
	THEN 'B'
	WHEN 3 
	THEN 'C'
	WHEN 0 
	THEN 'D'
	END
FROM CTE
WHERE NumSlots<>''

---------------------------------------------------------------------------------------------
/*
Categories M,N,O,P
*/
----------------------------------------------------------------------------------------------
;WITH CTE
AS
(
SELECT 
ROW_NUMBER() OVER (ORDER BY PracticeId) AS RowNum
,SUBSTRING(A.MNOP, v.Number + 1, 1) AS NumSlots
,PracticeId
,PracticeName
,@T AS SlotTime
FROM   (
		SELECT PracticeId,
			PracticeName,
			CatArray7+CatArray8 AS MNOP
		FROM   PracticeName
		WHERE  PracticeType='T'
		AND PracticeId NOT IN
			(
				SELECT 
				PracticeId 
				FROM PracticeName 
				WHERE CONVERT(DATETIME,StartTime1)>CONVERT(DATETIME,StartTime2) 
					AND StartTime2<>'' 
					AND PracticeType='T'
			)
		) A
JOIN master..Spt_values v ON v.Number < DATALENGTH(A.MNOP) / 2
WHERE  v.Type = 'P' AND (DATALENGTH(A.MNOP) / 2) = 384
)
INSERT INTO #STBAC
([Time],RowNum,NumSlots,PracticeId,STName,Category)
SELECT 
	CASE
	WHEN RowNum % 384 <> 0
	THEN DATEADD(mi,(((((RowNum % 384)-1)/4)*15)+15),SlotTime)
	WHEN RowNum % 384 = 0
	THEN DATEADD(mi,1440,CONVERT(DateTime,SlotTime))
	END AS [Time]
,RowNum
,NumSlots
,PracticeId
,PracticeName+'|'+convert(NVARCHAR,PracticeId) AS STName
,Category = 
	CASE RowNum % 4 
	WHEN 1 
	THEN 'M'
	WHEN 2 
	THEN 'N'
	WHEN 3 
	THEN 'O'
	WHEN 0 
	THEN 'P'
	END
FROM CTE
WHERE NumSlots<>''

--Expand the stb table to include times
SELECT 
stb.*
,DATEADD(mi,(ROW_NUMBER() OVER (PARTITION BY stb.ScheduleTemplateId ORDER BY stb.OrdinalId)*15)-15,st.StartTime) AS BlockTime
INTO #STBwithTimes
FROM model.ScheduleTemplateBlocks stb
JOIN model.ScheduleTemplates st ON st.Id=stb.ScheduleTemplateId
ORDER BY ScheduleTemplateId

--Make a table that has the max endtime to each ScheduleTemplateId
SELECT 
MAX(DATEADD(yy,100,DATEADD(mi,-15,StartTime))) AS EndTime
,ScheduleTemplateId
,PracticeId
INTO #FinalEndTimesForEachScheduleTempalate
FROM #LocationsForSTB
GROUP BY ScheduleTemplateId,PracticeId
ORDER BY ScheduleTemplateId

--with those times, left join the query that broke apart the times+locations
;WITH CTE AS 
(
SELECT 
DATEADD(yy,+100,S.[Time]) AS [Time]
,NumSlots
,S.PracticeId
,STName
,Category
,st.Id
FROM #STBAC S
JOIN model.ScheduleTemplates st ON st.Name = S.STName
)
INSERT INTO model.ScheduleTemplateBlockAppointmentCategories
(ScheduleTemplateBlockOrdinalId,ScheduleTemplateBlockScheduleTemplateId,AppointmentCategoryId,[Count])
SELECT 
OrdinalId
,S.ScheduleTemplateId
,ac.Id
,NumSlots
FROM #STBwithTimes S
LEFT JOIN CTE ON CTE.Time=S.BlockTime AND CTE.Id=S.ScheduleTemplateId
LEFT JOIN model.AppointmentCategories ac ON ac.Name=CTE.Category
INNER JOIN #FinalEndTimesForEachScheduleTempalate et ON et.ScheduleTemplateId=S.ScheduleTemplateId
WHERE ac.Id IS NOT NULL AND BlockTime<=EndTime --Does not include the last slot as a schedulable slot.
ORDER BY BlockTime

--Cleanup template names (remove pipes and add's {#. Name})
UPDATE model.ScheduleTemplates
SET Name = LEFT(Name, CHARINDEX('|',Name)-1)
WHERE CHARINDEX(N'|', Name) > 1

;WITH CTE AS (
SELECT 
Id
,CONVERT(NVARCHAR,ROW_NUMBER() OVER (ORDER BY Name))+'. '+Name AS Name 
FROM model.ScheduleTemplates
WHERE Name='Unavailable'
UNION
SELECT 
Id
,CONVERT(NVARCHAR,ROW_NUMBER() OVER (ORDER BY Name)+1)+'. '+Name AS Name 
FROM model.ScheduleTemplates
WHERE Name<>'Unavailable'
)
SELECT 
* 
INTO #E
FROM 
(
	SELECT 
	Id
	,'0'+Name AS Name
	FROM CTE
	WHERE SUBSTRING(Name, 0,CHARINDEX('. ',Name)) BETWEEN 0 AND 9
	UNION
	SELECT Id,Name FROM CTE WHERE SUBSTRING(Name, 0,CHARINDEX('. ',Name)) > 9
) G
ORDER BY Name

UPDATE model.ScheduleTemplates
SET Name=#E.Name
FROM model.ScheduleTemplates
INNER JOIN #E
ON model.ScheduleTemplates.Id=#E.Id

DROP TABLE #E

--Set Holidays&Vacations
ALTER TABLE model.ScheduleBlocks NOCHECK CONSTRAINT FK_ScheduleTemplateScheduleBlock
;WITH CTE AS
(
SELECT 
CASE 
	WHEN RIGHT(CalendarPurpose,2) = 'NT'
		THEN 'VacationDay'
	WHEN RIGHT(CalendarPurpose,2) = 'TN'
		THEN 'Holiday'
	WHEN RIGHT(CalendarPurpose,2) = 'TT'
		THEN 'Holiday'
	ELSE NULL
END AS Comment
,CONVERT(DATETIME,CalendarDate) AS StartDateTime
,CalendarResourceId AS UserId
,IsUnavailable = 1
FROM PracticeCalendar 
WHERE RIGHT(CalendarPurpose,2) <> 'NN'
)
UPDATE model.ScheduleBlocks
SET Comment = CTE.Comment
,IsUnavailable = CTE.IsUnavailable
FROM model.ScheduleBlocks sb
LEFT JOIN CTE ON CTE.UserId=sb.UserId 
	AND CONVERT(NVARCHAR,CTE.StartDateTime,112)=CONVERT(NVARCHAR,sb.StartDateTime,112)
WHERE CTE.UserId=sb.UserId
ALTER TABLE model.ScheduleBlocks WITH CHECK CHECK CONSTRAINT FK_ScheduleTemplateScheduleBlock

--Build the unavailable template blocks
DECLARE @DefaultLocationId INT 
SET @DefaultLocationId = (SELECT DefaultLocationId FROM model.ScheduleTemplateBuilderConfigurations)

;WITH CTE
AS 
(
	SELECT Id as ScheduleTemplateId,DATEADD(mi,15,StartTime) AS A,Name AS Comment FROM model.ScheduleTemplates AS D1
	UNION ALL
	SELECT ScheduleTemplateId,DATEADD(mi,15,A),Comment
	FROM CTE
	WHERE A < (SELECT AreaEnd FROM model.ScheduleTemplateBuilderConfigurations AS D2 WHERE AreaEnd>=CTE.A)
)
INSERT INTO model.ScheduleTemplateBlocks
(OrdinalId,ScheduleTemplateId,Comment,ServiceLocationId,IsUnavailable)
SELECT
ROW_NUMBER() OVER (ORDER BY Comment) AS OrdinalId
,ScheduleTemplateId
,NULL AS Comment
,@DefaultLocationId AS ServiceLocationId
,1 AS IsUnavailable
FROM CTE
WHERE Comment='01. Unavailable'

--Remove the last endtime to comes in as part of the migration, the new calendar reflects this already.
;WITH CTE AS
(
	SELECT 
	MAX(OrdinalId) AS MaxOrdinalId
	,stb.ScheduleTemplateId
	FROM model.ScheduleTemplateBlocks stb
	LEFT JOIN model.ScheduleTemplateBlockAppointmentCategories S ON S.ScheduleTemplateBlockOrdinalId=stb.OrdinalId 
	AND S.ScheduleTemplateBlockScheduleTemplateId=stb.ScheduleTemplateId
	WHERE stb.ScheduleTemplateId<>1
	GROUP BY ScheduleTemplateId
)
DELETE model.ScheduleTemplateBlockAppointmentCategories
FROM model.ScheduleTemplateBlockAppointmentCategories stbac
JOIN CTE ON stbac.ScheduleTemplateBlockOrdinalId = CTE.MaxOrdinalId
		 AND stbac.ScheduleTemplateBlockScheduleTemplateId = CTE.ScheduleTemplateId

;WITH CTE AS
(
	SELECT 
	MAX(OrdinalId) AS MaxOrdinalId
	,stb.ScheduleTemplateId
	FROM model.ScheduleTemplateBlocks stb
	LEFT JOIN model.ScheduleTemplateBlockAppointmentCategories S ON S.ScheduleTemplateBlockOrdinalId=stb.OrdinalId 
	AND S.ScheduleTemplateBlockScheduleTemplateId=stb.ScheduleTemplateId
	WHERE stb.ScheduleTemplateId<>1
	GROUP BY ScheduleTemplateId
)
DELETE model.ScheduleTemplateBlocks
FROM model.ScheduleTemplateBlocks A
JOIN CTE ON A.ScheduleTemplateId=CTE.ScheduleTemplateId
AND A.OrdinalId=CTE.MaxOrdinalId

--updates the scheduletemplateblockappointmentcategories to remove appointmentIds that have unavailable scheduleblocks.
;WITH CTE AS 
(
SELECT 
* 
FROM model.ScheduleBlockAppointmentCategories
WHERE 
ScheduleBlockId IN 
	(
	SELECT Id FROM model.ScheduleBlocks
	WHERE IsUnavailable = 1 
	)
AND AppointmentId IS NOT NULL
)
UPDATE model.ScheduleBlockAppointmentCategories 
SET AppointmentId = NULL
WHERE AppointmentId IN 
(
	SELECT AppointmentId FROM CTE
)

UPDATE model.ScheduleTemplates
SET Name = SUBSTRING(Name, CHARINDEX('. ',Name)+2,LEN(Name))