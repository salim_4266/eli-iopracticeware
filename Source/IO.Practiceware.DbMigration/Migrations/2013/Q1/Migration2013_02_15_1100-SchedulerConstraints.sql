﻿
-- Remove duplicate ScheduleBlocks (same StartDateTime and User) and prevent against entry 

IF OBJECT_ID('model.Appointments') IS NULL
EXEC('CREATE VIEW model.Appointments AS SELECT 0 AS Id, 0 AS EncounterId')

IF OBJECT_ID('model.Encounters') IS NULL
EXEC('CREATE VIEW model.Encounters AS SELECT 0 AS Id, 0 AS EncounterStatusId')

WHILE EXISTS(SELECT MAX(Id) FROM model.ScheduleBlocks 
				GROUP BY StartDateTime, UserId
				HAVING COUNT(*) > 1)
BEGIN
	DELETE FROM model.ScheduleBlocks 
	WHERE Id IN (SELECT MAX(Id) FROM model.ScheduleBlocks 
					GROUP BY StartDateTime, UserId
					HAVING COUNT(*) > 1)
END



IF NOT EXISTS (select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_TYPE='UNIQUE' AND CONSTRAINT_NAME = 'UC_ScheduleBlocks_User_StartDateTime')
BEGIN
ALTER TABLE model.ScheduleBlocks WITH CHECK ADD CONSTRAINT UC_ScheduleBlocks_User_StartDateTime
	UNIQUE(StartDateTime, UserId)
END

-- Remove duplicate ScheduleBlockAppointmentCategories (same AppointmentId assigned to multiple ScheduleBlockAppointmentCategories) and prevent against entry 

WHILE EXISTS(SELECT MAX(CAST(Id AS nvarchar(36))) FROM model.ScheduleBlockAppointmentCategories
WHERE AppointmentId IS NOT NULL
GROUP BY AppointmentId 
HAVING COUNT(*) > 1)
BEGIN
	UPDATE model.ScheduleBlockAppointmentCategories
	SET AppointmentId = NULL
	WHERE Id IN (SELECT MAX(CAST(Id AS nvarchar(36))) FROM model.ScheduleBlockAppointmentCategories
		WHERE AppointmentId IS NOT NULL
		GROUP BY AppointmentId 
		HAVING COUNT(*) > 1)
END

-- Remove and prevent ScheduleBlockAppointmentCategories with an Appointment with a Cancelled status.

IF OBJECT_ID('model.Appointments') IS NOT NULL
EXEC('
	UPDATE model.ScheduleBlockAppointmentCategories 
	SET AppointmentId = NULL 
	FROM model.ScheduleBlockAppointmentCategories sbac 
	JOIN model.Appointments a ON sbac.AppointmentId = a.Id
	JOIN model.Encounters e ON a.EncounterId = e.Id
	WHERE EncounterStatusId >=8 AND EncounterStatusId <= 14
')

IF EXISTS(select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_TYPE='CHECK' AND CONSTRAINT_NAME = 'CK_ScheduleBlockAppointmentCategories_NonCancelledUniqueAppointmentId')
BEGIN
	ALTER TABLE model.ScheduleBlockAppointmentCategories DROP CONSTRAINT CK_ScheduleBlockAppointmentCategories_NonCancelledUniqueAppointmentId
END

IF EXISTS (SELECT * FROM sys.objects 
			WHERE object_id = OBJECT_ID(N'[model].[IsAppointmentCancelledOrNotUnique]') 
			AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	EXEC('DROP FUNCTION model.IsAppointmentCancelledOrNotUnique')
END
	
EXEC('
CREATE FUNCTION model.IsAppointmentCancelledOrNotUnique(@appointmentId int)
RETURNS bit
AS
BEGIN
	IF @appointmentId IS NULL
		RETURN 0

	DECLARE @returnValue AS bit
	SET @returnValue = 
			(SELECT CASE 
				WHEN EncounterStatusId >=8 AND EncounterStatusId <= 14 
					THEN 1 
				WHEN EXISTS(SELECT AppointmentId FROM model.ScheduleBlockAppointmentCategories 
					WHERE AppointmentId IS NOT NULL AND AppointmentId = @appointmentId
					GROUP BY AppointmentId
					HAVING COUNT(*) > 1)
					THEN 1
				ELSE CAST(0 AS bit)
				END FROM model.Appointments a JOIN model.Encounters e ON a.EncounterId = e.Id WHERE a.Id = @appointmentId)

	RETURN @returnValue
END
')

GO

IF NOT EXISTS (select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_TYPE='CHECK' AND CONSTRAINT_NAME = 'CK_ScheduleBlockAppointmentCategories_NonCancelledUniqueAppointmentId')
BEGIN
ALTER TABLE model.ScheduleBlockAppointmentCategories WITH NOCHECK ADD CONSTRAINT CK_ScheduleBlockAppointmentCategories_NonCancelledUniqueAppointmentId
	CHECK(AppointmentId IS NULL OR model.IsAppointmentCancelledOrNotUnique(AppointmentId) = 0)
END
