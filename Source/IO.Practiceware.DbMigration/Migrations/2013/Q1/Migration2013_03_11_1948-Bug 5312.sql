﻿-- migrates PatientReceivableServices.FeeCharge having nulls to  zero for services

UPDATE dbo.PatientReceivableServices
SET FeeCharge = 0
WHERE FeeCharge IS NULL