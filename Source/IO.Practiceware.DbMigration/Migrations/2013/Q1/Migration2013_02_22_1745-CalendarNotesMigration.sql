﻿-- migrates the Doctor Notes and Practice calendar notes to the new model table


INSERT INTO model.CalendarComments
(Date,Value,UserId)
	SELECT
	CONVERT(DATETIME,CalendarDate) AS Date
	,CalendarComment AS Value
	,CalendarResourceId AS UserId
	FROM PracticeCalendar pc
		INNER JOIN Resources re ON re.ResourceId=pc.CalendarResourceId
	WHERE CalendarComment IS NOT NULL 
	AND CalendarComment <> ''
	AND ResourceType='D'

	UNION ALL
	
	SELECT 
	CONVERT(DateTime,NoteDate) AS Date
	,Note1 
	,NULL AS UserId
	FROM PatientNotes 
	WHERE NoteType = '/'