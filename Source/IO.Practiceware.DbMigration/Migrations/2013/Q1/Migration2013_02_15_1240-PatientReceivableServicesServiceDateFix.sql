﻿IF OBJECT_ID('PatientReceivableServicesUpdate','TR') IS NOT NULL
BEGIN
	ALTER TABLE dbo.PatientReceivableServices DISABLE TRIGGER PatientReceivableServicesUpdate
END

;WITH CTE AS (
SELECT
prs.ItemId
,ap.AppDate
FROM dbo.PatientReceivableServices prs
INNER JOIN dbo.PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
WHERE prs.ServiceDate <> ap.AppDate
)
UPDATE prs
SET prs.ServiceDate = CTE.AppDate
FROM dbo.PatientReceivableServices prs
INNER JOIN CTE ON CTE.ItemId = prs.ItemId

IF OBJECT_ID('PatientReceivableServicesUpdate','TR') IS NOT NULL
BEGIN
	ALTER TABLE dbo.PatientReceivableServices ENABLE TRIGGER PatientReceivableServicesUpdate
END
