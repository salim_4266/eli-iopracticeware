﻿
IF OBJECT_ID(N'[model].[EncounterStatusConfigurations]', 'U') IS NOT NULL
    DROP TABLE [model].[EncounterStatusConfigurations];
GO

-- Creating table 'EncounterStatusConfigurations'
CREATE TABLE [model].[EncounterStatusConfigurations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [HexColor] nvarchar(max)  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'EncounterStatuses'
ALTER TABLE [model].[EncounterStatusConfigurations]
ADD CONSTRAINT [PK_EncounterStatusConfigurations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Inserting values into table 'EncounterStatuses'
SET IDENTITY_INSERT [model].[EncounterStatusConfigurations] ON

INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'F5EAF7', 1)--pending
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'F5EAF7', 2)--rescheduled
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'F27D27', 3)--questions
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'E1F227', 4)--waitingroom
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'5BF227', 5)--exam
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'279FF2', 6)--exp co
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'279FF2', 7)--checkout
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FF331689', 8)--discharged
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 9)--cx ofc
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 10)--cx pat
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 11)--cx sd
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 12)--cx ns
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 13)--cx left
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 14)--cx spec
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 15)--cx other
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'B81AA7', 16)--In the checkout screen of Admin

SET IDENTITY_INSERT [model].[EncounterStatusConfigurations] OFF