﻿IF OBJECT_ID(N'[model].[FK_PermissionUserPermission]', 'F') IS NOT NULL
    ALTER TABLE [model].[UserPermissions] DROP CONSTRAINT [FK_PermissionUserPermission];
GO

IF OBJECT_ID(N'[model].[FK_RolePermissionPermission]', 'F') IS NOT NULL
    ALTER TABLE [model].[RolePermissions] DROP CONSTRAINT [FK_RolePermissionPermission];
GO

IF EXISTS(SELECT *
     FROM sys.views
     WHERE name = 'UserPermissions' AND
     schema_id = SCHEMA_ID('model'))
     DROP VIEW model.UserPermissions
GO

IF EXISTS(SELECT *
     FROM sys.views
     WHERE name = 'UserPermissions_Internal' AND
     schema_id = SCHEMA_ID('model'))
     DROP VIEW model.UserPermissions_Internal
GO

IF EXISTS(SELECT *
     FROM sys.views
     WHERE name = 'UserPermissions_Partition1' AND
     schema_id = SCHEMA_ID('model'))
     DROP VIEW model.UserPermissions_Partition1
GO


IF EXISTS(SELECT *
     FROM sys.views
     WHERE name = 'UserPermissions_Partition2' AND
     schema_id = SCHEMA_ID('model'))
     DROP VIEW model.UserPermissions_Partition2
GO

IF EXISTS(SELECT *
     FROM sys.views
     WHERE name = 'UserPermissions_Partition3' AND
     schema_id = SCHEMA_ID('model'))
     DROP VIEW model.UserPermissions_Partition3
GO

IF EXISTS(SELECT *
     FROM sys.views
     WHERE name = 'UserPermissions_Partition4' AND
     schema_id = SCHEMA_ID('model'))
     DROP VIEW model.UserPermissions_Partition4
GO

IF EXISTS(SELECT *
     FROM sys.views
     WHERE name = 'UserPermissions_Partition5' AND
     schema_id = SCHEMA_ID('model'))
     DROP VIEW model.UserPermissions_Partition5
GO

IF EXISTS(SELECT *
     FROM sys.views
     WHERE name = 'UserPermissions_Partition6' AND
     schema_id = SCHEMA_ID('model'))
     DROP VIEW model.UserPermissions_Partition6
GO

IF EXISTS(SELECT *
     FROM sys.views
     WHERE name = 'UserPermissions_Partition7' AND
     schema_id = SCHEMA_ID('model'))
     DROP VIEW model.UserPermissions_Partition7
GO

IF EXISTS(SELECT *
     FROM sys.views
     WHERE name = 'UserPermissions_Partition8' AND
     schema_id = SCHEMA_ID('model'))
     DROP VIEW model.UserPermissions_Partition8
GO

TRUNCATE TABLE [model].[RolePermissions]
GO

TRUNCATE TABLE [model].[Permissions]
GO

-- Creating foreign key on [PermissionId] in table 'RolePermissions'
ALTER TABLE [model].[RolePermissions]
ADD CONSTRAINT [FK_RolePermissionPermission]
    FOREIGN KEY ([PermissionId])
    REFERENCES [model].[Permissions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

SET IDENTITY_INSERT [model].[Permissions] ON

INSERT INTO [model].[Permissions] (Id, Name) VALUES (1, 'View Schedule')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (2, 'Make Appointments')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (3, 'Force Appointments')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (4, 'Change Scheduled Doctor')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (5, 'Change Categories')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (6, 'Schedule Comments')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (7, 'Set Doctor Schedule')
INSERT INTO [model].[Permissions] (Id, Name) VALUES (8, 'Setup Sched Tmplts')

SET IDENTITY_INSERT [model].[Permissions] OFF
GO

--RolePermission
DECLARE @frontDesk int
SET @frontDesk = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) LIKE 'F%')

INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 1, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 2, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 3, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 4, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 5, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 6, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 7, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@frontDesk, 8, 0)

DECLARE @manager int
SET @manager = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) LIKE 'M%')

INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 1, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 2, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 3, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 4, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 5, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 6, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 7, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@manager, 8, 0)

DECLARE @admin int
SET @admin = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) LIKE 'A%')

INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 1, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 2, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 3, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 4, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 5, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 6, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 7, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@admin, 8, 0)

DECLARE @tech int
SET @tech = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) LIKE 'T%')

INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 1, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 2, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 3, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 4, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 5, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 6, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 7, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@tech, 8, 0)

DECLARE @doctor int
SET @doctor = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) LIKE 'D%')
   
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 1, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 2, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 3, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 4, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 5, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 6, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 7, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@doctor, 8, 0)

DECLARE @permission int
SET @permission = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = 'RESOURCETYPE' AND SUBSTRING(Code, 1, 1) LIKE 'P%')
 
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 1, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 2, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 3, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 4, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 5, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 6, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 7, 0)
INSERT INTO [model].[RolePermissions] (RoleId, PermissionId, IsDenied) VALUES (@permission, 8, 0)

GO