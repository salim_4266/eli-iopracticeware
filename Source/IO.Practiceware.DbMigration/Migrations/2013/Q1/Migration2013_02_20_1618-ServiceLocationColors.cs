﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Updates the model.ServiceLocations.HexColor column with a random known "beautiful" color .
    /// </summary>
    [Migration(201302201618)]
    public class Migration201302201618 : NonFluentTransactionalUpOnlyMigration
    {
        private const string IsHexColorDefinedPracticeNameSql =
        @"
        SELECT 
        CASE 
            WHEN EXISTS(
                SELECT * FROM sys.columns 
                WHERE Name= N'HexColor' and Object_ID = Object_ID(N'dbo.PracticeName'))
            THEN 1
            ELSE 0
        END
        ";

        private const string IsHexColorDefinedResourcesSql =
        @"
        SELECT 
        CASE 
            WHEN EXISTS(
                SELECT * FROM sys.columns 
                WHERE Name= N'HexColor' and Object_ID = Object_ID(N'dbo.Resources'))
            THEN 1
            ELSE 0
        END
        ";

        private const string GetServiceLocationColorsSql =
        @"
        SELECT pn.PracticeId AS PracticeId,
			NULL AS ResourceId,
			pn.HexColor AS HexColor
		FROM dbo.PracticeName pn
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
		LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE'
			AND SUBSTRING(pct.Code, 1, 2) = '11'
		LEFT JOIN dbo.Resources re ON re.PracticeId = pn.PracticeId
			AND re.ResourceType = 'R'
			AND re.ServiceCode = '02'
			AND re.Billable = 'Y'
			AND pic.FieldValue = 'T'
			AND pn.LocationReference <> ''
		LEFT JOIN dbo.PracticeCodeTable pctRE ON pctRE.ReferenceType = 'PLACEOFSERVICE'
			AND SUBSTRING(pctRE.Code, 1, 2) = re.PlaceOfService
		WHERE pn.PracticeType = 'P'

		UNION

	    SELECT NULL AS PracticeId,
			ResourceId,
			HexColor AS HexColor
	    FROM dbo.Resources re
	    LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE'
		    AND SUBSTRING(pct.Code, 1, 2) = re.PlaceOfService
	    WHERE re.ResourceType = 'R'
		    AND re.ServiceCode = '02'
        ";

        private const string UpdateHexColorInPracticeNameSql = @"UPDATE PracticeName SET HexColor = '{0}' WHERE PracticeId = '{1}'";
        private const string UpdateHexColorInResourcesSql = @"UPDATE Resources SET HexColor = '{0}' WHERE ResourceId = '{1}'";

        private List<KnownColor> _validColors;

        public List<KnownColor> GetValidColors()
        {
            var colors = new List<KnownColor>();
            colors.Add(KnownColor.BurlyWood);
            colors.Add(KnownColor.IndianRed);
            colors.Add(KnownColor.LightGreen);
            colors.Add(KnownColor.LightSkyBlue);
            colors.Add(KnownColor.Salmon);
            colors.Add(KnownColor.SlateBlue);
            colors.Add(KnownColor.Violet);
            colors.Add(KnownColor.YellowGreen);
            colors.Add(KnownColor.DarkBlue);
            colors.Add(KnownColor.Gold);
            colors.Add(KnownColor.DarkSlateBlue);
            colors.Add(KnownColor.MediumVioletRed);
            colors.Add(KnownColor.Crimson);
            colors.Add(KnownColor.Lavender);
            colors.Add(KnownColor.MediumBlue);
            return colors;
        }

        public Color GetNextColor()
        {
            var indexGenerator = new Random();
            if (_validColors == null)
            {
                _validColors = GetValidColors();
            }
            if (_validColors.Count == 0)
            {
                var color = Color.FromArgb(255, (byte)indexGenerator.Next(255), (byte)indexGenerator.Next(255), (byte)indexGenerator.Next(255));
                return color;
            }
            int index = indexGenerator.Next(0, _validColors.Count - 1);
            
            KnownColor randomColorName = _validColors[index];
            Color myCoolColor = Color.FromKnownColor(randomColorName);

            _validColors.RemoveAt(index);

            return myCoolColor;
        }


        protected override void MigrateUp(IDbConnection connection, IDbTransaction transaction)
        {
            using (var command = connection.CreateCommand())
            {
                command.Transaction = transaction;

                command.CommandText = IsHexColorDefinedPracticeNameSql;
                var existsInPn = (int) command.ExecuteScalar() == 1;
                if (!existsInPn) return;

                command.CommandText = IsHexColorDefinedResourcesSql;
                var existsInRe = (int) command.ExecuteScalar() == 1;
                if (!existsInRe) return;

                command.CommandText = GetServiceLocationColorsSql;


                var dt = new DataTable();
                using (IDataReader reader = command.ExecuteReader())
                {
                    dt.Load(reader);

                    foreach (var row in dt.Rows.OfType<DataRow>())
                    {
                        var practiceIdData = row["PracticeId"];
                        var resourceIdData = row["ResourceId"];
                        var hexColor = ToHex(GetNextColor());

                        if (!DBNull.Value.Equals(practiceIdData))
                        {
                            using (var updateCommand = connection.CreateCommand())
                            {
                                updateCommand.Transaction = transaction;
                                updateCommand.CommandText = string.Format(UpdateHexColorInPracticeNameSql, hexColor, (int) practiceIdData);
                                updateCommand.ExecuteNonQuery();
                            }
                        }
                        else if (!DBNull.Value.Equals(resourceIdData))
                        {
                            using (var updateCommand = connection.CreateCommand())
                            {
                                updateCommand.Transaction = transaction;
                                updateCommand.CommandText = string.Format(UpdateHexColorInResourcesSql, hexColor, (int) resourceIdData);
                                updateCommand.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
        }

        private static string ToHex(Color c)
        {
            return c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }
    }
}
