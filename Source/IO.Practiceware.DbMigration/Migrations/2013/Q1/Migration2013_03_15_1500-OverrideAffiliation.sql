﻿/* Prevents addition of override affiliation without 
ensuring that there is another override affiliation that is false*/

IF OBJECT_ID('OverrideAffiliationTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER OverrideAffiliationTrigger')
END

GO

CREATE TRIGGER OverrideAffiliationTrigger
ON PracticeAffiliations
FOR INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON

IF EXISTS (SELECT * FROM inserted i
			WHERE i.[Override] = 'Y'
			AND i.OrgOverride = 'Y' 
			AND i.ResourceId NOT IN (SELECT DISTINCT ResourceId 
									FROM PracticeAffiliations pa 
									WHERE i.ResourceType = pa.ResourceType
									AND ([Override] = 'N' OR OrgOverride = 'N')))

BEGIN

	;WITH CTE AS
	(
		SELECT * 
		FROM inserted i
		WHERE 
		i.[Override] = 'Y'
		AND i.OrgOverride = 'Y' 
		AND i.ResourceId NOT IN (SELECT DISTINCT ResourceId 
								FROM PracticeAffiliations pa 
								WHERE i.ResourceType = pa.ResourceType
								AND	([Override] = 'N' OR OrgOverride = 'N')
		)
	)
	INSERT INTO PracticeAffiliations
		(ResourceId, ResourceType, PIN, [Override], OrgOverride, LocationId, AlternatePin, PlanId)
	SELECT CTE.ResourceId
		,CTE.ResourceType
		,123 AS PIN
		,'N' AS [Override]
		,'N' AS OrgOverride
		,0 AS LocationId
		,'' AS AlternatePin
		,(SELECT TOP 1 InsurerId FROM PracticeInsurers WHERE InsurerName LIKE '%No insurance%') AS PlanId
	FROM CTE

END