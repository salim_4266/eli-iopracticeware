﻿IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Appointments_AppDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Appointments]'))
ALTER TABLE [dbo].[Appointments] DROP CONSTRAINT [CK_Appointments_AppDate]
GO

ALTER TABLE [dbo].[Appointments]  WITH NOCHECK ADD  CONSTRAINT [CK_Appointments_AppDate] CHECK NOT FOR REPLICATION ((isdate([AppDate])=(1) AND CONVERT([nvarchar],CONVERT([datetime],[AppDate],(112)),(112))=[AppDate] OR [dbo].[GetNoCheck]()=(1)))
GO

ALTER TABLE [dbo].[Appointments] CHECK CONSTRAINT [CK_Appointments_AppDate]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Appointments_SetDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Appointments]'))
ALTER TABLE [dbo].[Appointments] DROP CONSTRAINT [CK_Appointments_SetDate]
GO

ALTER TABLE [dbo].[Appointments]  WITH NOCHECK ADD  CONSTRAINT [CK_Appointments_SetDate] CHECK NOT FOR REPLICATION ((isdate([SetDate])=(1) AND CONVERT([nvarchar],CONVERT([datetime],[SetDate],(112)),(112))=[SetDate] OR ([SetDate]='' OR [SetDate] IS NULL) OR [dbo].[GetNoCheck]()=(1)))
GO

ALTER TABLE [dbo].[Appointments] CHECK CONSTRAINT [CK_Appointments_SetDate]
GO

