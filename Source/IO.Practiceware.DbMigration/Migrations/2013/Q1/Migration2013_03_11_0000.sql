﻿IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'model.ClaimFormTypes' AND sc.Name = 'DocumentId')
BEGIN
    ALTER TABLE model.ClaimFormTypes
    ADD DocumentId int NULL
END

-- FK Constraint
ALTER TABLE [model].[ClaimFormTypes]
ADD CONSTRAINT [FK_DocumentClaimFormType]
    FOREIGN KEY ([DocumentId])
    REFERENCES [model].[Documents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DocumentClaimFormType'
CREATE INDEX [IX_FK_DocumentClaimFormType]
ON [model].[ClaimFormTypes]
    ([DocumentId]);
GO

  
UPDATE model.Documents
SET NAME = 'CMS-1500.Doc'
	,DisplayName = 'CMS-1500'
WHERE NAME = 'Hcfa1500A.Doc'


INSERT INTO model.Documents (
	DocumentTypeId
	,NAME
	,DisplayName
	)
VALUES (
	'6'
	,'UB-04.Doc'
	,'UB-04'
	)

UPDATE c
SET c.DocumentId = d.Id
FROM model.ClaimFormTypes c
INNER JOIN model.Documents d ON SUBSTRING(d.NAME, 1, 5) = SUBSTRING(c.NAME, 1, 5)
	AND c.MethodSentId = 2


