﻿IF NOT EXISTS(SELECT * FROM sys.columns WHERE name = 'EncounterStatusChangeReasonId' AND object_id = OBJECT_ID('Appointments'))
	ALTER TABLE Appointments ADD EncounterStatusChangeReasonId int NULL


IF NOT EXISTS(SELECT * FROM sys.columns WHERE name = 'EncounterStatusChangeComment' AND object_id = OBJECT_ID('Appointments'))
	ALTER TABLE Appointments ADD EncounterStatusChangeComment nvarchar(max) NULL
