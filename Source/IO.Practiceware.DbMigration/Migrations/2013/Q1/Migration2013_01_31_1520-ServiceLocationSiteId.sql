﻿IF NOT EXISTS(SELECT * FROM sys.columns WHERE name = 'SiteId' AND object_id = OBJECT_ID('Resources'))
	ALTER TABLE Resources ADD SiteId int NULL

IF NOT EXISTS(SELECT * FROM sys.columns WHERE name = 'SiteId' AND object_id = OBJECT_ID('PracticeName'))
	ALTER TABLE PracticeName ADD SiteId int NULL

GO

--Update PracticeName set the siteid to ID 1 from model.Sites
UPDATE PracticeName
SET SiteId = 1
WHERE PracticeType = 'P' AND LocationReference = ''

--Attempts to update PracticeName where the Name from model.Sites = LocationReference of PracticeName
UPDATE PracticeName
SET SiteId = S.Id
FROM PracticeName pn
JOIN model.Sites S ON S.Name=pn.LocationReference
WHERE pn.LocationReference<>''
AND pn.PracticeType = 'P'

--Attempts to update PracticeName where the Name from model.Sites = PracticeCity of PracticeName
UPDATE PracticeName
SET SiteId = S.Id
FROM PracticeName pn
JOIN model.Sites S ON S.Name=pn.PracticeCity
WHERE pn.LocationReference<>''
AND pn.PracticeType = 'P'

--Attempts to update PracticeName where string after "-" in ShortName from model.Sites = LocationReference of PracticeName
;WITH CTE AS
(
SELECT 
Id
,LTRIM(SUBSTRING(ShortName,(CHARINDEX('-',ShortName)+1),20))  AS LocRef
FROM model.Sites 
WHERE Id>1
)
UPDATE PracticeName
SET SiteId=CTE.Id
FROM PracticeName pn
JOIN CTE ON CTE.LocRef=pn.LocationReference