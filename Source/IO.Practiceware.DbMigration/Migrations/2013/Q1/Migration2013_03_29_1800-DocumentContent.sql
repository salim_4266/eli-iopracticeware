﻿
-- Add new Content column to the documents table to store the actual template document
IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'Content' and Object_ID = Object_ID(N'model.documents')) 
BEGIN
	ALTER TABLE model.documents
	ADD Content VARBINARY(MAX) NULL
END
	
GO

-- Add new ContentOriginal column to the documents table to store the original template document
IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'ContentOriginal' and Object_ID = Object_ID(N'model.documents')) 
BEGIN
	ALTER TABLE model.documents
	ADD ContentOriginal VARBINARY(MAX) NULL
END
	
GO


-- Add new ContentTypeId column to the documents table to store the type of content
IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'ContentTypeId' and Object_ID = Object_ID(N'model.documents')) 
BEGIN
	ALTER TABLE model.documents
	ADD ContentTypeId INT NOT NULL DEFAULT 1
END
	
GO

-- Add new IsEditable column to the documents table which determines whether or not a user can edit this document.
IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'IsEditable' and Object_ID = Object_ID(N'model.documents')) 
BEGIN
	ALTER TABLE model.documents
	ADD IsEditable BIT NOT NULL DEFAULT 0
END
	
GO
