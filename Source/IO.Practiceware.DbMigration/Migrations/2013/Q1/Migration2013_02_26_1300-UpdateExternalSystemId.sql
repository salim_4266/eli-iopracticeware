﻿DECLARE @rowsOver3 int
DECLARE @nextRow int
DECLARE @counter int

SET @nextRow = (SELECT TOP 1 Id FROM model.ExternalSystems WHERE Id > 3 AND Id < 999)
SET @rowsOver3 = ((SELECT COUNT(*) FROM model.externalsystems WHERE Id < 999) - 3)
SET @counter = 0

DBCC CHECKIDENT ("Model.ExternalSystems", RESEED, 1000)

WHILE (@counter < @rowsOver3)
BEGIN
	INSERT INTO model.ExternalSystems (Name) (SELECT Name FROM model.ExternalSystems WHERE Id = @nextRow)
	UPDATE model.ExternalSystemEntities
		SET ExternalSystemId = (SELECT IDENT_CURRENT('model.externalsystems')) 
		WHERE ExternalSystemId = @nextRow
	UPDATE model.ExternalSystemExternalSystemMessageTypes
		SET ExternalSystemId = (SELECT IDENT_CURRENT('model.externalsystems')) 
		WHERE ExternalSystemId = @nextRow
	SET @nextRow = (SELECT TOP 1 Id FROM model.ExternalSystems where Id > @nextRow AND Id < 999)
	SET @counter = @counter + 1
END
DELETE FROM model.ExternalSystems
WHERE Id BETWEEN 4 AND 999
