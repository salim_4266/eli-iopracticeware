﻿
IF OBJECT_ID(N'[model].[ApplicationSettings]', 'U') IS NOT NULL
    DROP TABLE [model].[ApplicationSettings];
GO

-- Creating table 'ApplicationSettings'
CREATE TABLE [model].[ApplicationSettings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Xml] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [MachineName] nvarchar(max)  NULL,
    [ApplicationSettingTypeId] int  NOT NULL,
    [UserId] int  NULL
);
GO


-- Creating primary key on [Id] in table 'ApplicationSettings'
ALTER TABLE [model].[ApplicationSettings]
ADD CONSTRAINT [PK_ApplicationSettings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO


-- Creating non-clustered index for FOREIGN KEY 'FK_ApplicationSettingUser'
CREATE INDEX [IX_FK_ApplicationSettingUser]
ON [model].[ApplicationSettings]
    ([UserId]);
GO