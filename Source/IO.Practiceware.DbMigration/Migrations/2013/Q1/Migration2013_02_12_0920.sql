﻿-- Add IsCliaWaived to dbo.PracticeServices
IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'PracticeServices' AND sc.Name = 'IsCliaWaived')
BEGIN
    ALTER TABLE dbo.PracticeServices
    ADD IsCliaWaived bit NOT NULL default 0 
END
GO