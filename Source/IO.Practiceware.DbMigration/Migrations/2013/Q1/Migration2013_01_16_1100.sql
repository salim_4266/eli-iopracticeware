﻿-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------
IF OBJECT_ID(N'[model].[CalendarComments]', 'U') IS NOT NULL
    DROP TABLE [model].[CalendarComments];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------
-- Creating table 'CalendarComments'
CREATE TABLE [model].[CalendarComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [UserId] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------
-- Creating primary key on [Id] in table 'CalendarComments'
ALTER TABLE [model].[CalendarComments]
ADD CONSTRAINT [PK_CalendarComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------
-- Creating non-clustered index for FOREIGN KEY 'FK_UserCalendarComment'
CREATE INDEX [IX_FK_UserCalendarComment]
ON [model].[CalendarComments]
    ([UserId]);
GO

-- --------------------------------------------------
-- Back fill data from dbo
-- --------------------------------------------------

