﻿IF EXISTS (	
	SELECT 
	CalendarResourceId
	,CalendarDate
	FROM PracticeCalendar 
	GROUP BY 
	CalendarResourceId
	,CalendarDate
	HAVING COUNT(CalendarResourceId) > 1
)
BEGIN
	DELETE pc
	FROM
	PracticeCalendar pc
	INNER JOIN (
		SELECT 
		CalendarResourceId
		,CalendarDate
		FROM PracticeCalendar 
		GROUP BY 
		CalendarResourceId
		,CalendarDate
		HAVING COUNT(CalendarResourceId) > 1
	)v ON (v.CalendarResourceId = pc.CalendarResourceId AND v.CalendarDate = pc.CalendarDate)
END

IF EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'appointments' and schemas.name = 'model') 
BEGIN


IF(OBJECT_ID('model.ScheduleEntries') IS NOT NULL)
EXEC ('DROP VIEW model.ScheduleEntries')


EXEC ('CREATE FUNCTION model.GetScheduleEntriesConversion(@Position AS int)
RETURNS @Results TABLE ( Id int, EndDateTime datetime, EquipmentId int, RoomId int, ScheduleEntryAvailabilityTypeId int, ServiceLocationId int, StartDateTime datetime, UserId int, CalendarId int) 
WITH SCHEMABINDING
AS 
BEGIN

INSERT INTO @Results   
SELECT model.GetPairId(@Position, pc.CalendarId, 110000000) AS Id, 
	CONVERT(DATETIME, CalendarDate + '' '' + SUBSTRING(CalendarEndTime, @Position * 8 + 1, 8)) AS EndDateTime,
	NULL AS EquipmentId,
	CASE
		WHEN re.ResourceType = ''R''
			THEN CalendarResourceId
		ELSE NULL
		END AS RoomId,
	CASE
		WHEN RIGHT(CalendarPurpose, 2) = ''NT'' 
			THEN 2
		WHEN RIGHT(CalendarPurpose, 2) IN (''TT'', ''TN'') 
			THEN 3
		ELSE 1
		END AS ScheduleEntryAvailibilityTypeId,
	CASE 
		WHEN pnServLoc.PracticeId IS NULL
			THEN model.GetPairId(1, reServLoc.ResourceId, 110000000)			
		ELSE
			model.GetPairId(0, pnServLoc.PracticeId, 110000000)
		END AS ServiceLocationId,
	CONVERT(DATETIME, CalendarDate + '' '' + SUBSTRING(CalendarStartTime, @Position * 8 + 1, 8)) AS StartDateTime,
	CASE
		WHEN re.ResourceType <> ''R''
			THEN CalendarResourceId 
		ELSE NULL
		END AS UserId, pc.CalendarId
FROM dbo.PracticeCalendar pc
INNER JOIN dbo.Resources re ON re.ResourceId = pc.CalendarResourceId
	AND pc.CalendarResourceId <> 0
LEFT OUTER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P'' 	
	AND pnServLoc.PracticeId = CASE 
		WHEN 
			CASE @Position 
				WHEN 0 THEN CalendarLoc1
				WHEN 1 THEN CalendarLoc2			
				WHEN 2 THEN CalendarLoc3			
				WHEN 3 THEN CalendarLoc4			
				WHEN 4 THEN CalendarLoc5			
				WHEN 5 THEN CalendarLoc6			
				WHEN 6 THEN CalendarLoc7			
			END = 0
			THEN (SELECT PracticeId FROM dbo.PracticeName WHERE PracticeType = ''P'' AND LocationReference = '''')
		WHEN 
			CASE @Position 
				WHEN 0 THEN CalendarLoc1
				WHEN 1 THEN CalendarLoc2			
				WHEN 2 THEN CalendarLoc3			
				WHEN 3 THEN CalendarLoc4			
				WHEN 4 THEN CalendarLoc5			
				WHEN 5 THEN CalendarLoc6			
				WHEN 6 THEN CalendarLoc7			
			END = -1
			THEN (SELECT PracticeId FROM dbo.PracticeName WHERE PracticeType = ''P'' AND LocationReference = '''')
		WHEN @Position = 0 AND CalendarLoc1 > 1000 THEN CalendarLoc1 - 1000
		WHEN @Position = 1 AND CalendarLoc2 > 1000 THEN CalendarLoc2 - 1000
		WHEN @Position = 2 AND CalendarLoc3 > 1000 THEN CalendarLoc3 - 1000
		WHEN @Position = 3 AND CalendarLoc4 > 1000 THEN CalendarLoc4 - 1000
		WHEN @Position = 4 AND CalendarLoc5 > 1000 THEN CalendarLoc5 - 1000
		WHEN @Position = 5 AND CalendarLoc6 > 1000 THEN CalendarLoc6 - 1000
		WHEN @Position = 6 AND CalendarLoc7 > 1000 THEN CalendarLoc7 - 1000
		ELSE NULL
		END

LEFT OUTER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R'' 
		AND reServLoc.ResourceId = CASE
			WHEN @Position = 0 AND CalendarLoc1 < 1000
				THEN CalendarLoc1
			WHEN @Position = 1 AND CalendarLoc2 < 1000
				THEN CalendarLoc2
			WHEN @Position = 2 AND CalendarLoc3 < 1000
				THEN CalendarLoc3
			WHEN @Position = 3 AND CalendarLoc4 < 1000
				THEN CalendarLoc4
			WHEN @Position = 4 AND CalendarLoc5 < 1000
				THEN CalendarLoc5
			WHEN @Position = 5 AND CalendarLoc6 < 1000
				THEN CalendarLoc6
			WHEN @Position = 6 AND CalendarLoc7 < 1000
				THEN CalendarLoc7
			END
WHERE SUBSTRING(CalendarEndTime, @Position * 8 + 1, 8) > ''0''
	AND (ISDATE(CalendarDate  + '' '' + SUBSTRING(CalendarEndTime, 1 * 8 + 1, 8)) = 1
		OR ISDATE(CalendarDate  + '' '' + SUBSTRING(CalendarStartTime, 1 * 8 + 1, 8)) = 1)
RETURN
END')

SET NOCOUNT ON

IF (OBJECT_ID('tempdb..#MinutesDifferent') IS NOT NULL)
	EXEC('DROP TABLE #MinutesDifferent')

CREATE TABLE #MinutesDifferent(Minutes int)
DECLARE @offset int
SET @offset = 0
WHILE @offset < 96
BEGIN
	INSERT #MinutesDifferent VALUES (@offset * 15)
	SET @offset = @offset + 1
END

UPDATE model.ScheduleTemplateBuilderConfigurations
SET SpanSize = '2000-01-01 00:15:00.000'


IF (OBJECT_ID('tempdb..#ScheduleEntries') IS NOT NULL)
	EXEC('DROP TABLE #ScheduleEntries')


SELECT Id, DATEADD(mi, md.Minutes, StartDateTime) AS StartDateTime, ServiceLocationId, CAST(0 AS bit) AS IsUnavailable, UserId, CAST('UserScheduleBlock' AS nvarchar(100)) AS __EntityType__, CalendarId 
INTO #ScheduleEntries
FROM (
		SELECT Id, StartDateTime, EndDateTime, UserId, ServiceLocationId, CalendarId FROM model.GetScheduleEntriesConversion(0) 
		UNION ALL
		SELECT Id, StartDateTime, EndDateTime, UserId, ServiceLocationId, CalendarId FROM model.GetScheduleEntriesConversion(1) 
		UNION ALL
		SELECT Id, StartDateTime, EndDateTime, UserId, ServiceLocationId, CalendarId FROM model.GetScheduleEntriesConversion(2) 
		UNION ALL
		SELECT Id, StartDateTime, EndDateTime, UserId, ServiceLocationId, CalendarId FROM model.GetScheduleEntriesConversion(3) 
		UNION ALL
		SELECT Id, StartDateTime, EndDateTime, UserId, ServiceLocationId, CalendarId FROM model.GetScheduleEntriesConversion(4) 
		UNION ALL
		SELECT Id, StartDateTime, EndDateTime, UserId, ServiceLocationId, CalendarId FROM model.GetScheduleEntriesConversion(5) 
		UNION ALL
		SELECT Id, StartDateTime, EndDateTime, UserId, ServiceLocationId, CalendarId FROM model.GetScheduleEntriesConversion(6)
	) q
JOIN
#MinutesDifferent md
ON md.Minutes < DATEDIFF(mi, StartDateTime, EndDateTime)
WHERE ServiceLocationId IS NOT NULL

DECLARE @insertedScheduleBlocks TABLE(Id int NOT NULL PRIMARY KEY CLUSTERED)

INSERT model.ScheduleBlocks(StartDateTime, ServiceLocationId, IsUnavailable, UserId, __EntityType__)
OUTPUT inserted.Id INTO @insertedScheduleBlocks
SELECT se.StartDateTime, se.ServiceLocationId, se.IsUnavailable, se.UserId, __EntityType__ FROM #ScheduleEntries se

CREATE INDEX IX_ScheduleEntries ON #ScheduleEntries(StartDateTime,UserId,IsUnavailable,ServiceLocationId)
----------------------------------------------------------------------------------------------
/*
model.AppointmentCategories
*/
----------------------------------------------------------------------------------------------
UPDATE at
SET Question = 'No Questions'
FROM dbo.AppointmentType at
WHERE AppTypeId > 0 AND AppointmentType <> 'Manual Claim'
AND AppTypeId IN (
	SELECT 
	AppTypeId 
	FROM AppointmentType
	WHERE Question IS NULL OR Question = ''
)

;WITH CTE AS (
SELECT 
*
FROM AppointmentType
WHERE Category NOT IN (
	SELECT
	DISTINCT Code 
	FROM PracticeCodeTable 
	WHERE ReferenceType = 'APPOINTMENT CATEGORY COLORS'
)
AND AppTypeId > 0
)
INSERT INTO PracticeCodeTable (
  [ReferenceType]
  ,[Code]
  ,[AlternateCode]
  ,[Exclusion]
  ,[LetterTranslation]
  ,[OtherLtrTrans]
  ,[FormatMask]
  ,[Signature]
  ,[TestOrder]
  ,[FollowUp]
  ,[Rank]
)
SELECT 
'APPOINTMENT CATEGORY COLORS' AS ReferenceType 
,Category AS Code
,SUBSTRING(LEFT(SUBSTRING (RTRIM(RAND()) + SUBSTRING(RTRIM(RAND()),3,11), 3,11),9),0,5) AS AlternateCode
,'F' AS Exclusion
,'' AS LetterTranslation
,'' AS OtherLtrTrans
,'' AS FormatMask
,'F' AS [Signature]
,'' AS TestOrder
,'' AS FollowUp
,0 AS [Rank]
FROM CTE 

INSERT INTO model.AppointmentCategories 
            (Name) 
SELECT DISTINCT(Category) 
FROM   AppointmentType at 
       INNER JOIN PracticeCodeTable pc ON pc.Code = at.Category 
       INNER JOIN PracticeCodeTable pct ON pct.ReferenceType = 'QUESTIONSETS' 
            AND pct.Code = at.Question 
WHERE  pc.ReferenceType = 'APPOINTMENT CATEGORY COLORS' 

UPDATE AppointmentType 
SET AppointmentCategoryId = model.AppointmentCategories.Id 
FROM Appointments, model.AppointmentCategories 
WHERE  model.AppointmentCategories.Name = Appointmenttype.Category 

----------------------------------------------------------------------------------------------
/*
model.ScheduleBlockAppointmentCategories
*/
----------------------------------------------------------------------------------------------
IF EXISTS (
    SELECT * FROM tempdb.dbo.sysobjects o
    WHERE o.xtype IN ('U') 
	AND o.id = object_id(N'tempdb..#SBAC')
)
DROP TABLE #SBAC;
---------------------------------------------------------------------------------------------
/*
Categories S,I,L,E
*/
----------------------------------------------------------------------------------------------
;WITH CTE AS
(
SELECT 
ROW_NUMBER() OVER(ORDER BY (CalendarId)) AS RowNumber
,SUBSTRING(A.SILE, v.Number + 1, 1) AS NumSlots
,CalendarId
,CalendarDate
,CalendarResourceId
 FROM
	(
	SELECT 
	CatArray1+CatArray2 AS SILE
	,CalendarId
	,CalendarResourceId
	,CalendarDate
	FROM PracticeCalendar
	
	) A
JOIN master..Spt_values v ON v.Number < DATALENGTH(A.SILE) / 2
WHERE  v.Type = 'P' AND (DATALENGTH(A.SILE) / 2) = 384
)
SELECT 
sb.Id AS ScheduleBlockId
,ac.Id AS AppointmentCategoryId
,sb.StartDateTime
INTO #SBAC
FROM 
(
	SELECT
		CASE
			WHEN RowNumber % 384 > 0
				THEN DATEADD(mi,(((((RowNumber % 384)-1)/4)*15)+15),CONVERT(DateTime,CalendarDate))
			WHEN RowNumber % 384 = 0
				THEN DATEADD(mi,1440,CONVERT(DateTime,CalendarDate))
		END AS ScheduleBlockStartTime
		,NumSlots
		,Category = 
		CASE RowNumber % 4 
			WHEN 1 
				THEN 'S'
			WHEN 2 
				THEN 'I'
			WHEN 3 
				THEN 'L'
			WHEN 0 
				THEN 'E'
		END
		,CalendarResourceId
		,CalendarDate
		,CalendarId
	FROM CTE
	WHERE NumSlots<>''
) C
JOIN model.ScheduleBlocks sb ON c.ScheduleBlockStartTime=sb.StartDateTime 
	AND c.CalendarResourceId=sb.UserId
JOIN model.AppointmentCategories ac ON c.Category=ac.Name
JOIN master..spt_values V ON number > 0 and number <=NumSlots
WHERE Type ='P'
ORDER BY sb.Id
----------------------------------------------------------------------------------------------
/*
Categories W,X,Y,Z
*/
----------------------------------------------------------------------------------------------
;WITH CTE AS
(
SELECT 
ROW_NUMBER() OVER(ORDER BY (CalendarId)) AS RowNumber
,SUBSTRING(A.WXYZ, v.Number + 1, 1) AS NumSlots
,CalendarId
,CalendarDate
,CalendarResourceId
 FROM
	(
	SELECT 
	CatArray3+CatArray4 AS WXYZ
	,CalendarId
	,CalendarResourceId
	,CalendarDate
	FROM PracticeCalendar
	
	) A
JOIN master..Spt_values v ON v.Number < DATALENGTH(A.WXYZ) / 2
WHERE  v.Type = 'P' AND (DATALENGTH(A.WXYZ) / 2) = 384
)
INSERT INTO #SBAC
(
	ScheduleBlockId,AppointmentCategoryId,sb.StartDateTime
)
SELECT 
sb.Id AS ScheduleBlockId
,ac.Id AS AppointmentCategoryId
,sb.StartDateTime
FROM 
(
	SELECT
		CASE
			WHEN RowNumber % 384 > 0
				THEN DATEADD(mi,(((((RowNumber % 384)-1)/4)*15)+15),CONVERT(DateTime,CalendarDate))
			WHEN RowNumber % 384 = 0
				THEN DATEADD(mi,1440,CONVERT(DateTime,CalendarDate))
		END AS ScheduleBlockStartTime
		,NumSlots
		,Category = 
		CASE RowNumber % 4 
			WHEN 1 
				THEN 'W'
			WHEN 2 
				THEN 'X'
			WHEN 3 
				THEN 'Y'
			WHEN 0 
				THEN 'Z'
		END
		,CalendarResourceId
		,CalendarDate
		,CalendarId
	FROM CTE
	WHERE NumSlots<>''
) C
JOIN model.ScheduleBlocks sb ON c.ScheduleBlockStartTime=sb.StartDateTime 
	AND c.CalendarResourceId=sb.UserId
JOIN model.AppointmentCategories ac ON c.Category=ac.Name
JOIN master..spt_values V ON number > 0 and number <=NumSlots
WHERE Type ='P'
ORDER BY sb.Id
----------------------------------------------------------------------------------------------
/*
Categories A,B,C,D
*/
----------------------------------------------------------------------------------------------
;WITH CTE AS
(
SELECT 
ROW_NUMBER() OVER(ORDER BY (CalendarId)) AS RowNumber
,SUBSTRING(A.ABCD, v.Number + 1, 1) AS NumSlots
,CalendarId
,CalendarDate
,CalendarResourceId
 FROM
	(
	SELECT 
	CatArray5+CatArray6 AS ABCD
	,CalendarId
	,CalendarResourceId
	,CalendarDate
	FROM PracticeCalendar
	
	) A
JOIN master..Spt_values v ON v.Number < DATALENGTH(A.ABCD) / 2
WHERE  v.Type = 'P' AND (DATALENGTH(A.ABCD) / 2) = 384
)
INSERT INTO #SBAC
(
	ScheduleBlockId,AppointmentCategoryId,sb.StartDateTime
)
SELECT 
sb.Id AS ScheduleBlockId
,ac.Id AS AppointmentCategoryId
,sb.StartDateTime
FROM 
(
	SELECT
		CASE
			WHEN RowNumber % 384 > 0
				THEN DATEADD(mi,(((((RowNumber % 384)-1)/4)*15)+15),CONVERT(DateTime,CalendarDate))
			WHEN RowNumber % 384 = 0
				THEN DATEADD(mi,1440,CONVERT(DateTime,CalendarDate))
		END AS ScheduleBlockStartTime
		,NumSlots
		,Category = 
		CASE RowNumber % 4 
			WHEN 1 
				THEN 'A'
			WHEN 2 
				THEN 'B'
			WHEN 3 
				THEN 'C'
			WHEN 0 
				THEN 'D'
		END
		,CalendarResourceId
		,CalendarDate
		,CalendarId
	FROM CTE
	WHERE NumSlots<>''
) C
JOIN model.ScheduleBlocks sb ON c.ScheduleBlockStartTime=sb.StartDateTime 
	AND c.CalendarResourceId=sb.UserId
JOIN model.AppointmentCategories ac ON c.Category=ac.Name
JOIN master..spt_values V ON number > 0 and number <=NumSlots
WHERE Type ='P'
ORDER BY sb.Id
----------------------------------------------------------------------------------------------
/*
Categories M,N,O,P
*/
----------------------------------------------------------------------------------------------
;WITH CTE AS
(
SELECT 
ROW_NUMBER() OVER(ORDER BY (CalendarId)) AS RowNumber
,SUBSTRING(A.MNOP, v.Number + 1, 1) AS NumSlots
,CalendarId
,CalendarDate
,CalendarResourceId
 FROM
	(
	SELECT 
	CatArray7+CatArray8 AS MNOP
	,CalendarId
	,CalendarResourceId
	,CalendarDate
	FROM PracticeCalendar
	
	) A
JOIN master..Spt_values v ON v.Number < DATALENGTH(A.MNOP) / 2
WHERE  v.Type = 'P' AND (DATALENGTH(A.MNOP) / 2) = 384
)
INSERT INTO #SBAC
(
	ScheduleBlockId,AppointmentCategoryId,sb.StartDateTime
)
SELECT 
sb.Id AS ScheduleBlockId
,ac.Id AS AppointmentCategoryId
,sb.StartDateTime
FROM 
(
	SELECT
		CASE
			WHEN RowNumber % 384 > 0
				THEN DATEADD(mi,(((((RowNumber % 384)-1)/4)*15)+15),CONVERT(DateTime,CalendarDate))
			WHEN RowNumber % 384 = 0
				THEN DATEADD(mi,1440,CONVERT(DateTime,CalendarDate))
		END AS ScheduleBlockStartTime
		,NumSlots
		,Category = 
		CASE RowNumber % 4 
			WHEN 1 
				THEN 'M'
			WHEN 2 
				THEN 'N'
			WHEN 3 
				THEN 'O'
			WHEN 0 
				THEN 'P'
		END
		,CalendarResourceId
		,CalendarDate
		,CalendarId
	FROM CTE
	WHERE NumSlots<>''
) C
JOIN model.ScheduleBlocks sb ON c.ScheduleBlockStartTime=sb.StartDateTime 
	AND c.CalendarResourceId=sb.UserId
JOIN model.AppointmentCategories ac ON c.Category=ac.Name
JOIN master..spt_values V ON number > 0 and number <=NumSlots
WHERE Type ='P'
ORDER BY sb.Id
-------------------------------------------------------------------------------------------------
ALTER TABLE #SBAC
ADD AppointmentId int NULL

;WITH 
SBAC_AppointmentId_NULL
	AS
	(
		SELECT ScheduleBlockId
		,AppointmentCategoryId
		,AppointmentId
		,ROW_NUMBER () OVER (PARTITION BY ScheduleBlockId,AppointmentCategoryId ORDER BY ScheduleBlockId,AppointmentCategoryId) rn
		FROM #SBAC
		JOIN model.ScheduleBlocks sb ON sb.Id=#SBAC.ScheduleBlockId
	),
AppointmentIds
AS
	(
		SELECT
		sb.Id AS ScheduleBlockId
		,at.AppointmentCategoryId
		,ap.Id AS AppointmentId
		,ROW_NUMBER () OVER (PARTITION BY sb.Id,at.AppointmentCategoryId ORDER BY sb.Id,at.AppointmentCategoryId) rn
		FROM model.Appointments ap
		INNER JOIN model.AppointmentTypes at ON at.Id=ap.AppointmentTypeId
		INNER JOIN model.ScheduleBlocks sb ON sb.StartDateTime=ap.DateTime AND sb.UserId=ap.UserId
		INNER JOIN model.Encounters e ON e.Id=ap.EncounterId
		WHERE e.EncounterStatusId IN (1,2)     
	)
INSERT INTO model.ScheduleBlockAppointmentCategories
(ScheduleBlockId,AppointmentCategoryId,AppointmentId) 
SELECT 
A.ScheduleBlockId,A.AppointmentCategoryId,B.AppointmentId 
FROM SBAC_AppointmentId_NULL A
LEFT JOIN AppointmentIds B ON 
	A.ScheduleBlockId=B.ScheduleBlockId 
	AND A.AppointmentCategoryId=B.AppointmentCategoryId 
	AND A.rn=B.rn

DROP TABLE #SBAC
DROP FUNCTION model.GetScheduleEntriesConversion

END