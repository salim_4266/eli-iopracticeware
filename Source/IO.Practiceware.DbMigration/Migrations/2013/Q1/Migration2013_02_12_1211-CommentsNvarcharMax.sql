﻿DECLARE @constraintname nvarchar(max)
SET @constraintname = (SELECT d.name  FROM sys.tables t
  JOIN    sys.default_constraints d  ON d.parent_object_id = t.object_id  
  JOIN    sys.columns c ON c.object_id = t.object_id  AND c.column_id = d.parent_column_id
 WHERE t.name = 'Appointments'
	AND c.name = 'Comments')

IF EXISTS (SELECT @constraintname WHERE @constraintname IS NOT NULL)
BEGIN
	EXEC ( 'ALTER TABLE Appointments DROP CONSTRAINT ' + @constraintname)
END

	EXEC dbo.DropObject  'model.Comments'
	EXEC dbo.DropObject  'model.Appointments_Internal'
	EXEC dbo.DropObject  'model.ClinicalInvoiceProviders'
	EXEC dbo.DropObject  'model.Invoices'
	EXEC dbo.DropObject  'model.Encounters'
	
	
	ALTER TABLE Appointments
	ALTER COLUMN Comments nvarchar(max) 

	ALTER TABLE Appointments
	ADD  DEFAULT ('') FOR [Comments]