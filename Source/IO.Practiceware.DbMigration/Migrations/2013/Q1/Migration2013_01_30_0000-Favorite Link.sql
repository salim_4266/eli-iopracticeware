﻿--Add IO Twitter to favorite links page.
IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'ACCESSADS' AND Code LIKE '%twitter.com/iopracticeware')
INSERT INTO PracticeCodeTable (ReferenceType, Code, Exclusion, Signature, Rank) VALUES ('ACCESSADS', '10 - IO Twitter : https://twitter.com/iopracticeware', 'F', 'F', 10)