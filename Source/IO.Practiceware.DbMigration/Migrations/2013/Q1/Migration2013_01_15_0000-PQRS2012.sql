﻿DELETE FROM CMSReports_Params

DELETE FROM CMSReports WHERE ReportType = 'PQRI'

INSERT [dbo].[CMSReports] ([CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #125', N'eRx Health Information Technology (HIT): Adoption/Use of Electronic Prescribing (e-Rx)', N'USP_PQRI_GetHIT_eRX', N'Adoption/Use of Medication Electronic Prescribing (e-Rx)', 1, N'Patient encounter documentation substantiates use of certified/qualified EHR', N'All patient encounters', N'PQRI', 1)
INSERT [dbo].[CMSReports] ([CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #12', N'1. #12 POAG: Optic Nerve Evaluation', N'USP_PQRI_GetPOAGDetails', N'Optic Nerve Evaluation', 1, N'Patients who have an optic nerve head evaluation during one or more office visits within 12 months', N'All patients aged 18 years and older with a diagnosis of primary open-angle glaucoma', N'PQRI', 1)
INSERT [dbo].[CMSReports] ([CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #14', N'2. #14 AMD: Dilated Macular Examination', N'USP_PQRI_GetAMD_Dilated_Macular_Examination', N'Dilated Macular Examination', 1, N'Patients who had a dilated exam documentating macular thickening or hemorrhage plus severity of macular degeneration', N'All patients aged 50 years and older with a diagnosis of age-related macular degeneration', N'PQRI', 1)
INSERT [dbo].[CMSReports] ([CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #18', N'3. #18 Diabetic Retinopathy: Documentation of Macular Edema and Severity of Retinopathy', N'USP_PQRI_GetDiabetic_Retinopathy_MacularEdema', N'Documentation of Macular Edema and Level of Severity of Retinopathy', 1, N'Patients who had a dilated exam documenting the severity of retinopathy AND the presence or absence of macular edema', N'All patients aged 18 years and older with diabetic retinopathy', N'PQRI', 1)
INSERT [dbo].[CMSReports] ([CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #19', N'4. #19 Diabetic Retinopathy: Communication with the Physician Managing On-Diabetes Care', N'USP_PQRI_GetDiabetic_Retinopathy_Communication_Physician', N'Communication with the Physician Managing On-going Diabetes Care', 1, N'Patients with documentation of a dilated exam communicated to the physician managing the patient’s diabetic care', N'All patients aged 18 years and older with diabetic retinopathy who had a dilated macular or fundus exam performed', N'PQRI', 1)
INSERT [dbo].[CMSReports] ([CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #117', N'5. #117 Diabetes Mellitus: Dilated Eye Exam in Diabetic Patient', N'USP_PQRI_Diabetes_GetMellitus_Dilated_Eye', N'Dilated Eye Exam in Diabetic Patient', 1, N'Patients who had a dilated eye exam for diabetic retinal disease at least once within 12 months', N'All patients aged 18 through 75 years with a diagnosis of diabetes', N'PQRI', 1)
INSERT [dbo].[CMSReports] ([CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #124', N'6. #124 Health Information Technology (HIT): Adoption/Use of Electronic Health Records (EHR)', N'USP_PQRI_GetHIT_USE_EHR', N'Adoption/Use of Electronic Health Records (EHR)', 1, N'Patient encounter documentation substantiates use of certified/qualified EHR', N'All patient encounters', N'PQRI', 1)
INSERT [dbo].[CMSReports] ([CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #140', N'7. #140 AMD: Counseling on Antioxidant Supplement', N'USP_PQRI_GetAMD_AREDS_Counseling', N'Counseling on Antioxidant Supplement', 1, N'Patients who were counseled within 12 months on the benefits and/or risks of the AREDS formulation', N'All patients aged 50 years and older with AMD', N'PQRI', 1)
INSERT [dbo].[CMSReports] ([CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #141', N'8. #141 POAG: Reduction of IOP or Documentation of Plan', N'USP_PQRI_GetPOAG_ReducePressure', N'Reduction of Intraocular Pressure or Documentation of Plan of Care', 1, N'Patients whose most recent IOP was reduced by at least 15% or where a plan of care was documented', N'All patients aged 18 years and older with a diagnosis of primary open-angle glaucoma', N'PQRI', 1)
INSERT [dbo].[CMSReports] ([CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #191', N'98. #191 Cataracts: 20/40 or Better Visual Acuity within 90 Days Following Cataract Surgery', N'USP_PQRI_GetCataracts_Visual_Acuity', N'20/40 or Better Visual Acuity within 90 Days Following Cataract Surgery', 1, N'Patients who had best-corrected visual acuity of 20/40 or better achieved within 90 days following cataract surgery', N'All patients aged 18 years and older without pre-operative ocular conditions who had cataract surgery', N'PQRI', 1)
INSERT [dbo].[CMSReports] ([CMSRepCode], [CMSRepName], [CMSQuery], [CMSSubHeading], [IsSP], [Numerator], [Denominator], [ReportType], [Active_YN]) VALUES (N'Measure #192', N'99. #192 Cataracts: Complications within 30 Days Following Cataract Surgery Requiring Additional Surgery', N'USP_PQRI_GetCataracts_Complications', N'Complications within 30 Days of Cataract Surgery Requiring Additional Surgery', 1, N'Patients without complications within 30 days of cataract surgery', N'All patients aged 18 years and older without pre-operative ocular conditions who had cataract surgery', N'PQRI', 1)


INSERT [dbo].[CMSReports_Params] ([CMSRepId], [CMSParamCode], [CMSParamDesc], [CMSParamValue], [CMSParamUnits])
SELECT CMSRepId, N'POAG_Age', N'Age >=', N'18', N'years'
FROM CMSReports WHERE CMSRepCode = 'Measure #12' 

INSERT [dbo].[CMSReports_Params] ([CMSRepId], [CMSParamCode], [CMSParamDesc], [CMSParamValue], [CMSParamUnits]) 
SELECT CMSRepId, N'AMD_Age', N'Age >=', N'50', N'years'
FROM CMSReports WHERE CMSRepCode = 'Measure #14'

INSERT [dbo].[CMSReports_Params] ([CMSRepId], [CMSParamCode], [CMSParamDesc], [CMSParamValue], [CMSParamUnits]) 
SELECT CMSRepId, N'Diabetic_Retinopathy_Age', N'Age >=', N'18', N'years'
FROM CMSReports WHERE CMSRepCode = 'Measure #18'

INSERT [dbo].[CMSReports_Params] ([CMSRepId], [CMSParamCode], [CMSParamDesc], [CMSParamValue], [CMSParamUnits]) 
SELECT CMSRepId, N'Diabetic_Retinopathy_MacularEdema_Age', N'Age >=', N'18', N'years'
FROM CMSReports WHERE CMSRepCode = 'Measure #19'

INSERT [dbo].[CMSReports_Params] ([CMSRepId], [CMSParamCode], [CMSParamDesc], [CMSParamValue], [CMSParamUnits]) 
SELECT CMSRepId, N'Diabetes_Mellitus_Age1', N'Age >=', N'18', N'years'
FROM CMSReports WHERE CMSRepCode = 'Measure #117'

INSERT [dbo].[CMSReports_Params] ([CMSRepId], [CMSParamCode], [CMSParamDesc], [CMSParamValue], [CMSParamUnits]) 
SELECT CMSRepId, N'Diabetes_Mellitus_Age2', N'Age <=', N'75', N'years'
FROM CMSReports WHERE CMSRepCode = 'Measure #117'

INSERT [dbo].[CMSReports_Params] ([CMSRepId], [CMSParamCode], [CMSParamDesc], [CMSParamValue], [CMSParamUnits]) 
SELECT CMSRepId, N'AMD_AREDS_Age', N'Age >=', N'50', N'years'
FROM CMSReports WHERE CMSRepCode = 'Measure #140'

INSERT [dbo].[CMSReports_Params] ([CMSRepId], [CMSParamCode], [CMSParamDesc], [CMSParamValue], [CMSParamUnits]) 
SELECT CMSRepId, N'POAG_Pressure_Reduction_Age', N'Age >=', N'18', N'years'
FROM CMSReports WHERE CMSRepCode = 'Measure #141'

INSERT [dbo].[CMSReports_Params] ([CMSRepId], [CMSParamCode], [CMSParamDesc], [CMSParamValue], [CMSParamUnits]) 
SELECT CMSRepId, N'Cataracts_Age_20/40', N'Age >=', N'18', N'years'
FROM CMSReports WHERE CMSRepCode = 'Measure #191'

INSERT [dbo].[CMSReports_Params] ([CMSRepId], [CMSParamCode], [CMSParamDesc], [CMSParamValue], [CMSParamUnits]) 
SELECT CMSRepId, N'Cataracts_Age/40', N'Age >=', N'18', N'years'
FROM CMSReports WHERE CMSRepCode = 'Measure #192'
