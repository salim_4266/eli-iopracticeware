﻿using FluentMigrator;
using System.Data;
using System.Drawing;
using System.Linq;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Convert OLE Colors to HEX Colors in the model.AppointmentCategories table.
    /// </summary>
    [Migration(201302151600)]
    public class Migration201302151600 : NonFluentTransactionalUpOnlyMigration
    {
        private const string GetCategoryColorsSql = @"SELECT DISTINCT(at.Category),CONVERT(INT,pct.AlternateCode) AS OleColor 
        FROM PracticeCodeTable pct 
        INNER JOIN AppointmentType at ON at.Category=pct.Code
        WHERE ReferenceType = 'APPOINTMENT CATEGORY COLORS' ";

        private const string UpdateCategoryColorSql = @"UPDATE model.AppointmentCategories SET HexColor = '{0}' WHERE Name = '{1}'";

        protected override void MigrateUp(IDbConnection connection, IDbTransaction transaction)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = GetCategoryColorsSql;
                command.Transaction = transaction;
                var dt = new DataTable();
                using (IDataReader reader = command.ExecuteReader())
                {
                    dt.Load(reader);

                    foreach (var row in dt.Rows.OfType<DataRow>())
                    {
                        var oleColor = (int) row["OleColor"];

                        var categoryName = row["Category"];

                        var hexColor = ToHex(ColorTranslator.FromOle(oleColor));

                        using (var updateCommand = connection.CreateCommand())
                        {
                            updateCommand.Transaction = transaction;
                            updateCommand.CommandText = string.Format(UpdateCategoryColorSql, hexColor, categoryName);
                            updateCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
        }

        private static string ToHex(Color c)
        {
            return c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }
    }
}
