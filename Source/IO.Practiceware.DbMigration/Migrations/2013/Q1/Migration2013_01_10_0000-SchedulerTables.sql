﻿IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'AppointmentType' AND sc.Name = 'AppointmentCategoryId') 
    ALTER TABLE AppointmentType ADD AppointmentCategoryId int

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[model].[FK_ScheduleTemplateScheduleTemplateBlock]') AND parent_object_id = OBJECT_ID(N'[model].[ScheduleTemplateBlocks]'))
ALTER TABLE [model].[ScheduleTemplateBlocks] DROP CONSTRAINT [FK_ScheduleTemplateScheduleTemplateBlock]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[model].[FK_ScheduleTemplateBlockScheduleTemplateBlockAppointmentCategory]') AND parent_object_id = OBJECT_ID(N'[model].[ScheduleTemplateBlockAppointmentCategories]'))
ALTER TABLE [model].[ScheduleTemplateBlockAppointmentCategories] DROP CONSTRAINT [FK_ScheduleTemplateBlockScheduleTemplateBlockAppointmentCategory]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[model].[FK_AppointmentCategoryScheduleTemplateBlockAppointmentCategory]') AND parent_object_id = OBJECT_ID(N'[model].[ScheduleTemplateBlockAppointmentCategories]'))
ALTER TABLE [model].[ScheduleTemplateBlockAppointmentCategories] DROP CONSTRAINT [FK_AppointmentCategoryScheduleTemplateBlockAppointmentCategory]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[model].[FK_ScheduleTemplateScheduleBlock]') AND parent_object_id = OBJECT_ID(N'[model].[ScheduleBlocks]'))
ALTER TABLE [model].[ScheduleBlocks] DROP CONSTRAINT [FK_ScheduleTemplateScheduleBlock]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[model].[FK_ScheduleBlockScheduleBlockAppointmentCategory]') AND parent_object_id = OBJECT_ID(N'[model].[ScheduleBlockAppointmentCategories]'))
ALTER TABLE [model].[ScheduleBlockAppointmentCategories] DROP CONSTRAINT [FK_ScheduleBlockScheduleBlockAppointmentCategory]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[model].[FK_AppointmentCategoryScheduleBlockAppointmentCategory]') AND parent_object_id = OBJECT_ID(N'[model].[ScheduleBlockAppointmentCategories]'))
ALTER TABLE [model].[ScheduleBlockAppointmentCategories] DROP CONSTRAINT [FK_AppointmentCategoryScheduleBlockAppointmentCategory]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[model].[DF_ScheduleBlockAppointmentCategories_Id]') AND type = 'D')
BEGIN
ALTER TABLE [model].[ScheduleBlockAppointmentCategories] DROP CONSTRAINT [DF_ScheduleBlockAppointmentCategories_Id]
END
GO

/****** Object:  Table [model].[ScheduleTemplates]    Script Date: 1/21/2013 4:47:24 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[ScheduleTemplates]') AND type in (N'U'))
DROP TABLE [model].[ScheduleTemplates]
GO
/****** Object:  Table [model].[ScheduleTemplateBlocks]    Script Date: 1/21/2013 4:47:24 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[ScheduleTemplateBlocks]') AND type in (N'U'))
DROP TABLE [model].[ScheduleTemplateBlocks]
GO
/****** Object:  Table [model].[ScheduleTemplateBlockAppointmentCategories]    Script Date: 1/21/2013 4:47:24 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[ScheduleTemplateBlockAppointmentCategories]') AND type in (N'U'))
DROP TABLE [model].[ScheduleTemplateBlockAppointmentCategories]
GO
/****** Object:  Table [model].[ScheduleBlocks]    Script Date: 1/21/2013 4:47:24 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[ScheduleBlocks]') AND type in (N'U'))
DROP TABLE [model].[ScheduleBlocks]
GO
/****** Object:  Table [model].[ScheduleBlockAppointmentCategories]    Script Date: 1/21/2013 4:47:24 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[ScheduleBlockAppointmentCategories]') AND type in (N'U'))
DROP TABLE [model].[ScheduleBlockAppointmentCategories]
GO
/****** Object:  Table [model].[AppointmentCategories]    Script Date: 1/21/2013 4:47:24 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[AppointmentCategories]') AND type in (N'U'))
DROP TABLE [model].[AppointmentCategories]
GO

--Tables and fields for scheduling.


IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'AppointmentType' AND sc.Name = 'AppointmentCategoryId') 
    ALTER TABLE AppointmentType ADD AppointmentCategoryId int
    

-- Creating table 'ScheduleTemplates'
CREATE TABLE [model].[ScheduleTemplates] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [StartTime] datetime  NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ScheduleTemplateBlocks'
CREATE TABLE [model].[ScheduleTemplateBlocks] (
    [OrdinalId] int  NOT NULL,
    [ScheduleTemplateId] int  NOT NULL DEFAULT (0),
    [Comment] nvarchar(max)  NULL,
    [ServiceLocationId] int  NULL,
    [IsUnavailable] bit  NOT NULL
);
GO

-- Creating table 'ScheduleTemplateBlockAppointmentCategories'
CREATE TABLE [model].[ScheduleTemplateBlockAppointmentCategories] (
    [ScheduleTemplateBlockOrdinalId] int  NOT NULL,
    [ScheduleTemplateBlockScheduleTemplateId] int  NOT NULL,
    [AppointmentCategoryId] int  NOT NULL,
    [Count] int NOT NULL
);
GO

-- Creating table 'AppointmentCategories'
CREATE TABLE [model].[AppointmentCategories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(255)  NOT NULL,
	[HexColor] nvarchar(max) NOT NULL DEFAULT('FFFFFF'),
	[IsArchived] bit NOT NULL DEFAULT 0,
	[Description] nvarchar(1024)
);
GO

-- Creating table 'ScheduleBlocks'
CREATE TABLE [model].[ScheduleBlocks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [StartDateTime] datetime  NOT NULL,
    [ScheduleTemplateId] int  NULL,
    [ServiceLocationId] int  NOT NULL,
    [IsUnavailable] bit  NOT NULL,
    [Comment] nvarchar(max)  NULL,
    [UserId] int  NULL,
    [RoomId] int  NULL,
    [EquipmentId] int  NULL,
    [__EntityType__] nvarchar(100)  NOT NULL
);
GO

-- Creating table 'ScheduleBlockAppointmentCategories'
CREATE TABLE [model].[ScheduleBlockAppointmentCategories] (
    [ScheduleBlockId] int  NOT NULL,
    [AppointmentCategoryId] int  NOT NULL,
    [AppointmentId] int  NULL,
    [Id] uniqueidentifier  NOT NULL
);
GO


-- Creating primary key on [Id] in table 'ScheduleTemplates'
ALTER TABLE [model].[ScheduleTemplates]
ADD CONSTRAINT [PK_ScheduleTemplates]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [OrdinalId], [ScheduleTemplateId] in table 'ScheduleTemplateBlocks'
ALTER TABLE [model].[ScheduleTemplateBlocks]
ADD CONSTRAINT [PK_ScheduleTemplateBlocks]
    PRIMARY KEY CLUSTERED ([OrdinalId], [ScheduleTemplateId] ASC);
GO

-- Creating primary key on [ScheduleTemplateBlockOrdinalId], [ScheduleTemplateBlockScheduleTemplateId], [AppointmentCategoryId] in table 'ScheduleTemplateBlockAppointmentCategories'
ALTER TABLE [model].[ScheduleTemplateBlockAppointmentCategories]
ADD CONSTRAINT [PK_ScheduleTemplateBlockAppointmentCategories]
    PRIMARY KEY CLUSTERED ([ScheduleTemplateBlockOrdinalId], [ScheduleTemplateBlockScheduleTemplateId], [AppointmentCategoryId] ASC);
GO

-- Creating primary key on [Id] in table 'AppointmentCategories'
ALTER TABLE [model].[AppointmentCategories]
ADD CONSTRAINT [PK_AppointmentCategories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ScheduleBlocks'
ALTER TABLE [model].[ScheduleBlocks]
ADD CONSTRAINT [PK_ScheduleBlocks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ScheduleBlockAppointmentCategories'
ALTER TABLE [model].[ScheduleBlockAppointmentCategories]
ADD CONSTRAINT [PK_ScheduleBlockAppointmentCategories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

--Set default value for GUID column
ALTER TABLE [model].[ScheduleBlockAppointmentCategories] 
ADD  CONSTRAINT [DF_ScheduleBlockAppointmentCategories_Id]  
	DEFAULT (newid()) FOR [Id]
GO

-- Creating foreign key on [ScheduleTemplateId] in table 'ScheduleTemplateBlocks'
ALTER TABLE [model].[ScheduleTemplateBlocks]
ADD CONSTRAINT [FK_ScheduleTemplateScheduleTemplateBlock]
    FOREIGN KEY ([ScheduleTemplateId])
    REFERENCES [model].[ScheduleTemplates]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ScheduleTemplateScheduleTemplateBlock'
CREATE INDEX [IX_FK_ScheduleTemplateScheduleTemplateBlock]
ON [model].[ScheduleTemplateBlocks]
    ([ScheduleTemplateId]);
GO

-- Creating foreign key on [ScheduleTemplateBlockOrdinalId], [ScheduleTemplateBlockScheduleTemplateId] in table 'ScheduleTemplateBlockAppointmentCategories'
ALTER TABLE [model].[ScheduleTemplateBlockAppointmentCategories]
ADD CONSTRAINT [FK_ScheduleTemplateBlockScheduleTemplateBlockAppointmentCategory]
    FOREIGN KEY ([ScheduleTemplateBlockOrdinalId], [ScheduleTemplateBlockScheduleTemplateId])
    REFERENCES [model].[ScheduleTemplateBlocks]
        ([OrdinalId], [ScheduleTemplateId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [AppointmentCategoryId] in table 'ScheduleTemplateBlockAppointmentCategories'
ALTER TABLE [model].[ScheduleTemplateBlockAppointmentCategories]
ADD CONSTRAINT [FK_AppointmentCategoryScheduleTemplateBlockAppointmentCategory]
    FOREIGN KEY ([AppointmentCategoryId])
    REFERENCES [model].[AppointmentCategories]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AppointmentCategoryScheduleTemplateBlockAppointmentCategory'
CREATE INDEX [IX_FK_AppointmentCategoryScheduleTemplateBlockAppointmentCategory]
ON [model].[ScheduleTemplateBlockAppointmentCategories]
    ([AppointmentCategoryId]);
GO

-- Creating foreign key on [ScheduleTemplateId] in table 'ScheduleBlocks'
ALTER TABLE [model].[ScheduleBlocks]
ADD CONSTRAINT [FK_ScheduleTemplateScheduleBlock]
    FOREIGN KEY ([ScheduleTemplateId])
    REFERENCES [model].[ScheduleTemplates]
        ([Id])
    ON DELETE SET NULL ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ScheduleTemplateScheduleBlock'
CREATE INDEX [IX_FK_ScheduleTemplateScheduleBlock]
ON [model].[ScheduleBlocks]
    ([ScheduleTemplateId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ServiceLocationScheduleBlock'
CREATE INDEX [IX_FK_ServiceLocationScheduleBlock]
ON [model].[ScheduleBlocks]
    ([ServiceLocationId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserUserScheduleBlock'
CREATE INDEX [IX_FK_UserUserScheduleBlock]
ON [model].[ScheduleBlocks]
    ([UserId]);
GO


-- Creating non-clustered index for FOREIGN KEY 'FK_RoomRoomScheduleBlock'
CREATE INDEX [IX_FK_RoomRoomScheduleBlock]
ON [model].[ScheduleBlocks]
    ([RoomId]);
GO


-- Creating non-clustered index for FOREIGN KEY 'FK_EquipmentEquipmentScheduleBlock'
CREATE INDEX [IX_FK_EquipmentEquipmentScheduleBlock]
ON [model].[ScheduleBlocks]
    ([EquipmentId]);
GO

-- Creating foreign key on [ScheduleBlockId] in table 'ScheduleBlockAppointmentCategories'
ALTER TABLE [model].[ScheduleBlockAppointmentCategories]
ADD CONSTRAINT [FK_ScheduleBlockScheduleBlockAppointmentCategory]
    FOREIGN KEY ([ScheduleBlockId])
    REFERENCES [model].[ScheduleBlocks]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [AppointmentCategoryId] in table 'ScheduleBlockAppointmentCategories'
ALTER TABLE [model].[ScheduleBlockAppointmentCategories]
ADD CONSTRAINT [FK_AppointmentCategoryScheduleBlockAppointmentCategory]
    FOREIGN KEY ([AppointmentCategoryId])
    REFERENCES [model].[AppointmentCategories]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AppointmentCategoryScheduleBlockAppointmentCategory'
CREATE INDEX [IX_FK_AppointmentCategoryScheduleBlockAppointmentCategory]
ON [model].[ScheduleBlockAppointmentCategories]
    ([AppointmentCategoryId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ScheduleBlockScheduleBlockAppointmentCategory' on table model.ScheduleBlockAppointmentCategories
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name='IX_FK_ScheduleBlockScheduleBlockAppointmentCategory' AND object_id = OBJECT_ID('model.ScheduleBlockAppointmentCategories'))
BEGIN
	CREATE INDEX [IX_FK_ScheduleBlockScheduleBlockAppointmentCategory]
	ON [model].[ScheduleBlockAppointmentCategories]
		([ScheduleBlockId]);
END
GO