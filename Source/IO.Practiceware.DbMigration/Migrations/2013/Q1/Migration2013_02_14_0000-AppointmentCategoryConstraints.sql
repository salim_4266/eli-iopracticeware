﻿IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit

SELECT 1 AS Value INTO #NoAudit

WHILE EXISTS(SELECT MAX(Id) FROM model.AppointmentCategories
				GROUP BY Name
				HAVING COUNT(*) > 1)
BEGIN
	DELETE FROM model.ScheduleBlockAppointmentCategories
	WHERE AppointmentCategoryId IN (SELECT MAX(Id) FROM model.AppointmentCategories
					GROUP BY Name
					HAVING COUNT(*) > 1)

	DELETE FROM model.ScheduleTemplateBlockAppointmentCategories
	WHERE AppointmentCategoryId IN (SELECT MAX(Id) FROM model.AppointmentCategories
					GROUP BY Name
					HAVING COUNT(*) > 1)


	DELETE FROM model.AppointmentCategories
	WHERE Id IN (SELECT MAX(Id) FROM model.AppointmentCategories
					GROUP BY Name
					HAVING COUNT(*) > 1)
END

IF NOT EXISTS (select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_TYPE='UNIQUE' AND CONSTRAINT_NAME = 'UC_AppointmentCategories_Name')
BEGIN
ALTER TABLE model.AppointmentCategories WITH CHECK ADD CONSTRAINT UC_AppointmentCategories_Name
	UNIQUE(Name)
END

GO