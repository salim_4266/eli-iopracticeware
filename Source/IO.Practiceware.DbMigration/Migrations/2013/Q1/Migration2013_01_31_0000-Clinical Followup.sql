﻿IF OBJECT_ID('dbo.RetrieveClnTransactions') IS NOT NULL
	EXEC('DROP PROCEDURE dbo.RetrieveClnTransactions')
GO

CREATE PROCEDURE [dbo].[RetrieveClnTransactions]

@SDate varchar(8),

@LocId Int,

@ResId Int,

@StaId varchar(1)

AS

SELECT

Appointments.AppDate, Appointments.ResourceId1,

Resources.ResourceName, Appointments.ResourceId2, 

Appointments.AppointmentId, PatientDemographics.LastName, 

PatientDemographics.FirstName, PatientDemographics.MiddleInitial, 

PatientDemographics.HomePhone, PatientDemographics.PatientId, 

MIN(PatientClinical.ClinicalId) AS ClinicalId, PatientClinical.ClinicalType, 

PatientClinical.Symptom, PatientClinical.FindingDetail, 

PatientClinical.ImageDescriptor, PatientClinical.ImageInstructions, 

PatientClinical.Status, PatientClinical.FollowUp

FROM Resources 

INNER JOIN (PatientDemographics 
INNER JOIN (Appointments 
INNER JOIN PatientClinical 

ON Appointments.AppointmentId = PatientClinical.AppointmentId) 

ON PatientDemographics.PatientId = PatientClinical.PatientId) 

ON Resources.ResourceId = Appointments.ResourceId1

WHERE ((PatientClinical.ClinicalType = 'A') Or (PatientClinical.ClinicalType ='F')) And

((PatientClinical.Status) = 'A') And

((PatientClinical.Followup) > ' ') And

((Appointments.AppDate) <= @SDate) And 

((Appointments.ResourceId2 = @LocId) OR (@LocId = -1)) And

((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) And

((PatientClinical.FollowUp = @StaId) OR (@StaId = '^'))

GROUP BY Appointments.AppDate, Appointments.ResourceId1,

Resources.ResourceName, Appointments.ResourceId2, 

Appointments.AppointmentId, PatientDemographics.LastName, 

PatientDemographics.FirstName, PatientDemographics.MiddleInitial, 

PatientDemographics.HomePhone, PatientDemographics.PatientId, 

PatientClinical.ClinicalType, 

PatientClinical.Symptom, PatientClinical.FindingDetail, 

PatientClinical.ImageDescriptor, PatientClinical.ImageInstructions, 

PatientClinical.Status, PatientClinical.FollowUp

ORDER BY PatientDemographics.LastName ASC , PatientDemographics.FirstName ASC, ClinicalId ASC

GO
