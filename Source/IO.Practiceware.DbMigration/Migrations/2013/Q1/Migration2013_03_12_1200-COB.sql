﻿IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'ClaimAdjustmentGroupCodeId' and Object_ID = Object_ID(N'PatientReceivablePayments')) 
BEGIN
	ALTER TABLE dbo.PatientReceivablePayments
	ADD ClaimAdjustmentGroupCodeId int NULL
END
	
GO

-- Insert for ClaimAdjustmentReasonCode 1 if it doesn't exist
IF NOT EXISTS(SELECT * FROM PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '1')
BEGIN

	INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, [Signature], TestOrder, FollowUp, [Rank], WebsiteUrl)
	  VALUES('PAYMENTREASON', -- ReferenceType
			 '1', -- Code
			  'Deductible', -- AlternateCode
			  'F', -- Exclusion
			  '', -- LetterTranslation
			  '', -- OtherLtrTrans
			  '', -- FormatMask
			  'F', -- Signature
			  '', -- TestOrder
			  '', -- FollowUp
			  1, -- Rank
			  NULL) -- WebsiteUrl
END

-- Insert for ClaimAdjustmentReasonCode 2 if it doesn't exist
IF NOT EXISTS(SELECT * FROM PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '2')
BEGIN

	INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, [Signature], TestOrder, FollowUp, [Rank], WebsiteUrl)
	  VALUES('PAYMENTREASON', -- ReferenceType
			 '2', -- Code
			  'CoInsurance', -- AlternateCode
			  'F', -- Exclusion
			  '', -- LetterTranslation
			  '', -- OtherLtrTrans
			  '', -- FormatMask
			  'F', -- Signature
			  '', -- TestOrder
			  '', -- FollowUp
			  1, -- Rank
			  NULL) -- WebsiteUrl
END

-- Insert for ClaimAdjustmentReasonCode 3 if it doesn't exist
IF NOT EXISTS(SELECT * FROM PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '3')
BEGIN

	INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, [Signature], TestOrder, FollowUp, [Rank], WebsiteUrl)
	  VALUES('PAYMENTREASON', -- ReferenceType
			 '3', -- Code
			  'Copay', -- AlternateCode
			  'F', -- Exclusion
			  '', -- LetterTranslation
			  '', -- OtherLtrTrans
			  '', -- FormatMask
			  'F', -- Signature
			  '', -- TestOrder
			  '', -- FollowUp
			  1, -- Rank
			  NULL) -- WebsiteUrl
END

-- Insert for ClaimAdjustmentReasonCode 45 if it doesn't exist
IF NOT EXISTS(SELECT * FROM PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '45')
BEGIN

	INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, [Signature], TestOrder, FollowUp, [Rank], WebsiteUrl)
	  VALUES('PAYMENTREASON', -- ReferenceType
			 '45', -- Code
			  'Contractual Adjustment', -- AlternateCode
			  'F', -- Exclusion
			  '', -- LetterTranslation
			  '', -- OtherLtrTrans
			  '', -- FormatMask
			  'F', -- Signature
			  '', -- TestOrder
			  '', -- FollowUp
			  1, -- Rank
			  NULL) -- WebsiteUrl
END

GO

-- Search the PatientReceivablePayments table for any PaymentReasons that 
-- do not exist in the PracticeCodeTable, and insert them into the PracticeCodeTable

DECLARE @invalidPaymentReasons TABLE(PaymentReason nvarchar(32))
INSERT INTO @invalidPaymentReasons(PaymentReason)
SELECT DISTINCT PaymentReason FROM [dbo].[PatientReceivablePayments] prp 
		   LEFT OUTER JOIN (SELECT * FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON') pct ON pct.Code = prp.PaymentReason
		   WHERE prp.PaymentReason <> '' AND pct.Id IS NULL

IF EXISTS (SELECT * FROM @invalidPaymentReasons)
BEGIN
	INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, [Signature], TestOrder, FollowUp, [Rank], WebsiteUrl)
	SELECT 'PAYMENTREASON', -- ReferenceType
			PaymentReason, -- Code
			'', -- AlternateCode (this contains description)
			'F', -- Exclusion
			'', -- LetterTranslation
			'', -- OtherLtrTrans
			'', -- FormatMask
			'F', -- Signature
			'', -- TestOrder
			'', -- FollowUp
			1, -- Rank
			NULL -- WebsiteUrl
			FROM @invalidPaymentReasons
END

GO

-- The following trigger checks the updated or inserted PaymentReason value on the PatientReceivablePayments table
-- and inserts it into the practiceCodeTable if it does not exist at the PCT.

CREATE TRIGGER dbo.PatientReceivablePaymentsValidPaymentReasonTrigger 
ON dbo.PatientReceivablePayments 
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
SET NOCOUNT ON

DECLARE @invalidPaymentReasons TABLE(PaymentReason nvarchar(32))
INSERT INTO @invalidPaymentReasons(PaymentReason)
SELECT DISTINCT PaymentReason FROM inserted
		   LEFT OUTER JOIN (SELECT * FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON') pct ON pct.Code = inserted.PaymentReason
		   WHERE inserted.PaymentReason <> '' AND pct.Id IS NULL

IF EXISTS (SELECT * FROM @invalidPaymentReasons)
BEGIN
	INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, [Signature], TestOrder, FollowUp, [Rank], WebsiteUrl)
	SELECT 'PAYMENTREASON', -- ReferenceType
			PaymentReason, -- Code
			'', -- AlternateCode (this contains description)
			'F', -- Exclusion
			'', -- LetterTranslation
			'', -- OtherLtrTrans
			'', -- FormatMask
			'F', -- Signature
			'', -- TestOrder
			'', -- FollowUp
			1, -- Rank
			NULL -- WebsiteUrl
			FROM @invalidPaymentReasons
END

GO