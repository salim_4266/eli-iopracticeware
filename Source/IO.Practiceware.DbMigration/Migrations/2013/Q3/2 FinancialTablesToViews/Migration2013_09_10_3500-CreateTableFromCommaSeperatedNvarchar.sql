﻿IF OBJECT_ID(N'CreateTableFromCommaSeperatedNvarchar', N'TF') IS NOT NULL
    DROP FUNCTION CreateTableFromCommaSeperatedNvarchar
GO

CREATE FUNCTION [dbo].CreateTableFromCommaSeperatedNvarchar(@input AS NVARCHAR(MAX))
RETURNS
      @Result TABLE([Value] NVARCHAR(MAX))
AS
BEGIN
      DECLARE @segment NVARCHAR(MAX)
      DECLARE @index INT
      IF(@input IS NOT NULL)
      BEGIN
            SET @index = CHARINDEX(',',@input)
            WHILE @index > 0
            BEGIN
                  SET @segment = SUBSTRING(@input,1,@index-1)
                  SET @input = SUBSTRING(@input,@index+1,LEN(@input)-@index)
                  INSERT INTO @Result VALUES (@segment)
                  SET @index = CHARINDEX(',',@input)
            END
            SET @segment = @input
            INSERT INTO @Result VALUES (@segment)
      END
      RETURN
END
GO