﻿IF OBJECT_ID ( 'dbo.GetLinkedBills', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetLinkedBills;
GO

CREATE PROCEDURE dbo.GetLinkedBills
	@ItemId INT
AS
SET NOCOUNT ON;

DECLARE @AppointmentId INT
SELECT TOP 1 @AppointmentId = prs.AppointmentId FROM PatientReceivableServices prs WHERE prs.ItemId = @ItemId
SELECT Id INTO #InvoiceReceivableId FROM dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId)

SELECT 
ir.Id
,ir.PatientInsuranceId
,ir.InvoiceId
INTO #InvoiceReceivableIds
FROM model.InvoiceReceivables ir 
INNER JOIN #InvoiceReceivableId ivs ON ivs.Id = ir.Id

;WITH 
maxDateBST AS (
	SELECT
	InvoiceReceivableId
	,LegacyTransactionId
	,BillingServiceTransactionStatusId
	,MethodSentId
	,AmountSent AS Amount
	,bst.DateTime
	,bst.BillingServiceId
	,ROW_NUMBER() OVER(PARTITION BY bst.InvoiceReceivableId ORDER BY bst.[DateTime] DESC) AS RowNum
	FROM model.BillingServiceTransactions bst 
	INNER JOIN #InvoiceReceivableIds i ON i.Id = bst.InvoiceReceivableId
	WHERE bst.BillingServiceId = @ItemId
)
,LastestBST AS (
	SELECT 
	InvoiceReceivableId
	,LegacyTransactionId
	,BillingServiceTransactionStatusId
	,MethodSentId
	,Amount
	,DateTime
	,BillingServiceId
	FROM maxDateBST 
	WHERE RowNum = 1
)
SELECT 
ptj.TransactionId 
,ptj.TransactionDate
,ptj.TransactionStatus
,ptj.TransactionRef
,ptj.TransactionAction
,lbst.Amount
,st.TransportAction
,CASE ptj.TransactionRef 
    WHEN 'I' THEN pins.InsurerName 
    WHEN 'P' THEN 'PATIENT' 
    WHEN 'G' THEN 'MONTHLY STMT' 
    ELSE '' 
END AS InsurerName
FROM LastestBST lbst
INNER JOIN #InvoiceReceivableIds irs ON irs.Id = lbst.InvoiceReceivableId
INNER JOIN model.Invoices i ON i.Id = irs.InvoiceId
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = irs.Id
INNER JOIN dbo.PatientReceivableServices prs ON lbst.BillingServiceId = prs.ItemId 
INNER JOIN dbo.ServiceTransactions st ON st.ServiceId = prs.ItemId
	AND st.TransactionId = ptj.TransactionId
LEFT JOIN model.PatientInsurances pi ON pi.Id = irs.PatientInsuranceId
LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
LEFT JOIN dbo.PracticeInsurers pins ON pins.InsurerId = ip.InsurerId
WHERE prs.Status = 'A' 
	AND prs.ItemId = @ItemId
	AND ptj.TransactionType = 'R' 
ORDER BY ptj.TransactionDate DESC
	,ptj.TransactionId DESC

GO