﻿CREATE VIEW [dbo].[PatientReceivables]
WITH VIEW_METADATA
AS
WITH 
adjusts AS (
	SELECT
	SUM(
		CASE 
			WHEN adjT.IsDebit = 1 
				THEN (CASE WHEN adj.Amount > 0 THEN -adj.Amount ELSE adj.Amount END) 
			ELSE adj.Amount 
		END
		) AS Adjust
	,ir.InvoiceId
	FROM model.Adjustments adj 
	INNER JOIN model.AdjustmentTypes adjT ON adjT.Id=adj.AdjustmentTypeId
	JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
	GROUP BY ir.InvoiceId
)
,lastPayDate AS (
	SELECT
	ir.InvoiceId
	,CONVERT(NVARCHAR,MAX(PostedDateTime),112) AS PostedDateTime
	FROM model.Adjustments adj 
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
	WHERE FinancialSourceTypeId = 2 --patientonly
	GROUP BY ir.InvoiceId
)
,bstAmountSent AS (
	SELECT 
	SUM(AmountSent) AS AmountSent
	,ir.InvoiceId
	FROM model.BillingServiceTransactions bst
	JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId
	WHERE BillingServiceTransactionStatusId IN (1,2,3,4,8)
	GROUP BY ir.InvoiceId
)
,bstsExistsForInvoiceId AS (
	SELECT 
	ir.InvoiceId
	,COUNT(*) AS NumOccurances
	FROM model.BillingServiceTransactions bst
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId
	GROUP BY ir.InvoiceId
)
,attributeToServiceLocations AS (
	SELECT
	Id
	,CASE
		WHEN ShortName = 'Office' AND IsExternal = 0
			THEN 0
		WHEN ShortName LIKE 'Office-%' AND IsExternal = 0
			THEN Id+1000
		WHEN IsExternal = 1
			THEN Id-110000000
		ELSE Id
	END AS PrBillingOfficeId
	FROM (
		SELECT 
		pn.PracticeId AS Id
		,CONVERT(BIT, 0) AS IsExternal
		,CASE LocationReference
			WHEN ''
				THEN 'Office'
			ELSE 'Office-' + LocationReference
		END AS ShortName
		FROM dbo.PracticeName pn
		WHERE pn.PracticeType = 'P'

		UNION ALL

		SELECT re.ResourceId+110000000 AS Id
		,CONVERT(BIT, 1) AS IsExternal
		,re.ResourceName AS ShortName
		FROM dbo.Resources re
		LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE'
			AND SUBSTRING(pct.Code, 1, 2) = re.PlaceOfService
		WHERE re.ResourceType = 'R'
			AND re.ServiceCode = '02'
	)v
)
SELECT 
ir.Id AS ReceivableId
,CASE
	WHEN charges.Charge IS NOT NULL
		THEN CASE
				WHEN adjusts.Adjust IS NOT NULL
					THEN CONVERT(REAL, charges.Charge-adjusts.Adjust)
				WHEN adjusts.Adjust IS NULL
					THEN CONVERT(REAL, charges.Charge)
			END
	ELSE 0
END AS OpenBalance
,CASE
	WHEN ir.OpenForReview = 0
		THEN 
			CASE
				WHEN bsti.NumOccurances IS NOT NULL
					THEN 'B'
				ELSE 'S'
			END
	WHEN ir.OpenForReview = 1
		THEN 'I'
END AS ReceivableType
,i.LegacyPatientReceivablesPatientId AS PatientId
,ip.PolicyHolderPatientId AS InsuredId
,ip.InsurerId
,i.LegacyPatientReceivablesAppointmentId AS AppointmentId
,i.LegacyPatientReceivablesInvoice AS Invoice
,CONVERT(NVARCHAR,i.DateTime,112) AS InvoiceDate
,COALESCE(CONVERT(REAL,CONVERT(NUMERIC(36,2),(charges.Charge))),0) AS Charge
,COALESCE(prov.UserId, (apASC.ResourceId2 -1000)) AS BillToDr
,CASE sup.RelatedCause1Id
	WHEN 1
		THEN 'A'
	WHEN 2
		THEN 'E'
	WHEN 3
		THEN 'O'
	ELSE ''
END AS AccType
,CASE
	WHEN sup.AccidentStateOrProvinceId IS NOT NULL
		THEN st.Abbreviation
	ELSE ''
END AS AccState
,CASE 
	WHEN AdmissionDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,AdmissionDateTime,101)
	ELSE ''
END AS HspAdmDate
,CASE 
	WHEN DischargeDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,DischargeDateTime,101)
	ELSE ''
END AS HspDisDate
,'' AS Disability
,'' AS RsnType
,CASE 
	WHEN DisabilityDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,DisabilityDateTime,101)
	ELSE ''
END AS RsnDate
,CASE
	WHEN AccidentDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,AccidentDateTime,101)
	ELSE ''
END AS FirstConsDate
,'' AS PrevCond
,'A' AS Status
,CASE 
	WHEN ((i.ReferringExternalProviderId IS NULL) OR (i.ReferringExternalProviderId = ''))
		THEN 0
	ELSE i.ReferringExternalProviderId
END AS ReferDr
,CASE
	WHEN charges.Charge IS NOT NULL
		THEN CASE
				WHEN adjusts.Adjust IS NOT NULL
					THEN CASE
							WHEN charges.Charge - adjusts.Adjust <> 0--WHEN charges.Charge - adjusts.Adjust > 0
								THEN CASE
										WHEN bstAmountSent.AmountSent IS NOT NULL
											THEN CONVERT(REAL,((charges.Charge - adjusts.Adjust)-bstAmountSent.AmountSent))
										WHEN bstAmountSent.AmountSent IS NULL
											THEN CONVERT(REAL,charges.Charge - adjusts.Adjust)
										END
							WHEN charges.Charge - adjusts.Adjust = 0--WHEN charges.Charge - adjusts.Adjust <= 0
								THEN 0
							END
				WHEN adjusts.Adjust IS NULL
					THEN CASE 
							WHEN bstAmountSent.AmountSent IS NOT NULL
								THEN CONVERT(REAL,charges.Charge-bstAmountSent.AmountSent)
							WHEN bstAmountSent.AmountSent IS NULL
								THEN CONVERT(REAL,charges.Charge)
							END
				END			
	ELSE 0
END AS UnallocatedBalance
,CASE 
	WHEN (ir.PayerClaimControlNumber IS NULL)
		THEN ''
	ELSE ir.PayerClaimControlNumber
END AS ExternalRefInfo
,CASE sup.ClaimDelayReasonId
	WHEN 1
		THEN '1'
	WHEN 2
		THEN '2'
	WHEN 3
		THEN '3'
	WHEN 4
		THEN '4'
	WHEN 5
		THEN '5'
	WHEN 6
		THEN '6'
	WHEN 7
		THEN '7'
	WHEN 8
		THEN '8'
	WHEN 9
		THEN '9'
	WHEN 10
		THEN '10'
	WHEN 11
		THEN '11'
	WHEN 12
		THEN '15'
	ELSE ''
END AS Over90
,asl.PrBillingOfficeId AS BillingOffice
,CASE
	WHEN i.LegacyPatientBilledDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,i.LegacyPatientBilledDateTime,112)
	ELSE ''
END AS PatientBillDate
,CASE
	WHEN lastPayDate.PostedDateTime IS NULL
		THEN ''
	ELSE lastPayDate.PostedDateTime 
END AS LastPayDate
,CASE 
	WHEN charges.ChargesByFee IS NOT NULL
		THEN CONVERT(REAL, charges.ChargesByFee) 
	ELSE '0.00'
END AS ChargesByFee
,'' AS WriteOff
,COALESCE(ir.LegacyInsurerDesignation,'F') AS InsurerDesignation
FROM model.InvoiceReceivables ir 
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.Providers prov ON prov.Id = i.BillingProviderId
INNER JOIN attributeToServiceLocations asl ON asl.Id = i.AttributeToServiceLocationId
LEFT JOIN dbo.Appointments apASC ON apASC.EncounterId = i.EncounterId 
	AND apASC.AppointmentId <> apASC.EncounterId
	AND i.InvoiceTypeId = 2
LEFT JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
LEFT JOIN dbo.Charges WITH(NOEXPAND) ON charges.InvoiceId = ir.InvoiceId
LEFT JOIN adjusts ON adjusts.InvoiceId = ir.InvoiceId
LEFT JOIN lastPayDate ON lastPayDate.InvoiceId = ir.InvoiceId
LEFT JOIN bstAmountSent ON bstAmountSent.InvoiceId = i.Id
LEFT JOIN model.InvoiceSupplementals sup ON i.Id = sup.Id
LEFT JOIN model.StateOrProvinces st ON st.Id = sup.AccidentStateOrProvinceId
LEFT JOIN bstsExistsForInvoiceId bsti ON bsti.InvoiceId = ir.InvoiceId
WHERE ir.PatientInsuranceId IS NOT NULL

UNION ALL

SELECT
ir.Id AS ReceivableId
,CASE
	WHEN charges.Charge IS NOT NULL
		THEN CASE
				WHEN adjusts.Adjust IS NOT NULL
					THEN CONVERT(REAL,charges.Charge-adjusts.Adjust)
				WHEN adjusts.Adjust IS NULL
					THEN CONVERT(REAL,charges.Charge)
			END
	ELSE 0
END AS OpenBalance
,CASE
	WHEN ir.OpenForReview = 0
		THEN 
			CASE
				WHEN bsti.NumOccurances IS NOT NULL
					THEN 'B'
				ELSE 'S'
			END
	WHEN ir.OpenForReview = 1
		THEN 'I'
END AS ReceivableType
,i.LegacyPatientReceivablesPatientId AS PatientId
,i.LegacyPatientReceivablesPatientId AS InsuredId
,0 AS InsurerId
,i.LegacyPatientReceivablesAppointmentId AS AppointmentId
,i.LegacyPatientReceivablesInvoice AS Invoice
,CONVERT(NVARCHAR,i.DateTime,112) AS InvoiceDate
,COALESCE(CONVERT(REAL,CONVERT(NUMERIC(36,2),(charges.Charge))),0) AS Charge
,COALESCE(prov.UserId, (apASC.ResourceId2 -1000)) AS BillToDr
,CASE sup.RelatedCause1Id
	WHEN 1
		THEN 'A'
	WHEN 2
		THEN 'E'
	WHEN 3
		THEN 'O'
	ELSE ''
END AS AccType
,CASE
	WHEN sup.AccidentStateOrProvinceId IS NOT NULL
		THEN st.Abbreviation
	ELSE ''
END AS AccState
,CASE 
	WHEN AdmissionDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,AdmissionDateTime,101)
	ELSE ''
END AS HspAdmDate
,CASE 
	WHEN DischargeDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,DischargeDateTime,101)
	ELSE ''
END AS HspDisDate
,'' AS Disability
,'' AS RsnType
,CASE 
	WHEN DisabilityDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,DisabilityDateTime,101)
	ELSE ''
END AS RsnDate
,CASE
	WHEN AccidentDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,AccidentDateTime,101)
	ELSE ''
END AS FirstConsDate
,'' AS PrevCond
,'A' AS Status
,CASE 
	WHEN ((i.ReferringExternalProviderId IS NULL) OR (i.ReferringExternalProviderId = ''))
		THEN 0
	ELSE i.ReferringExternalProviderId
END AS ReferDr
,CASE
	WHEN (charges.Charge IS NOT NULL)
		THEN CASE
				WHEN adjusts.Adjust IS NOT NULL
					THEN CASE
							WHEN charges.Charge - adjusts.Adjust <> 0
								THEN CASE
										WHEN bstAmountSent.AmountSent IS NOT NULL
											THEN ((charges.Charge - adjusts.Adjust)-bstAmountSent.AmountSent)
										WHEN bstAmountSent.AmountSent IS NULL
											THEN charges.Charge - adjusts.Adjust
										END
							WHEN charges.Charge - adjusts.Adjust = 0
								THEN 0
							END
				WHEN adjusts.Adjust IS NULL
					THEN CASE 
							WHEN bstAmountSent.AmountSent IS NOT NULL
								THEN CONVERT(REAL,charges.Charge-bstAmountSent.AmountSent)
							WHEN bstAmountSent.AmountSent IS NULL
								THEN CONVERT(REAL,charges.Charge)
							END
				END			
	ELSE 0
END AS UnallocatedBalance
,CASE 
	WHEN (ir.PayerClaimControlNumber IS NULL)
		THEN ''
	ELSE ir.PayerClaimControlNumber
END AS ExternalRefInfo
,CASE sup.ClaimDelayReasonId
	WHEN 1
		THEN '1'
	WHEN 2
		THEN '2'
	WHEN 3
		THEN '3'
	WHEN 4
		THEN '4'
	WHEN 5
		THEN '5'
	WHEN 6
		THEN '6'
	WHEN 7
		THEN '7'
	WHEN 8
		THEN '8'
	WHEN 9
		THEN '9'
	WHEN 10
		THEN '10'
	WHEN 11
		THEN '11'
	WHEN 12
		THEN '15'
	ELSE ''
END AS Over90
,asl.PrBillingOfficeId AS BillingOffice
,CASE
	WHEN i.LegacyPatientBilledDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,i.LegacyPatientBilledDateTime,112)
	ELSE ''
END AS PatientBillDate
,CASE
	WHEN lastPayDate.PostedDateTime IS NULL
		THEN ''
	ELSE lastPayDate.PostedDateTime 
END AS LastPayDate
,CASE 
	WHEN charges.ChargesByFee IS NOT NULL
		THEN CONVERT(REAL, charges.ChargesByFee) 
	ELSE '0.00'
END AS ChargesByFee
,'' AS WriteOff
,'T' AS InsurerDesignation
FROM model.InvoiceReceivables ir 
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
LEFT JOIN dbo.Appointments apASC ON apASC.EncounterId = i.EncounterId AND apASC.AppointmentId <> apASC.EncounterId
	AND i.InvoiceTypeId = 2
INNER JOIN attributeToServiceLocations asl ON asl.Id = i.AttributeToServiceLocationId
LEFT JOIN dbo.Charges WITH(NOEXPAND) ON charges.InvoiceId = ir.InvoiceId
LEFT JOIN adjusts ON adjusts.InvoiceId = ir.InvoiceId
LEFT JOIN lastPayDate ON lastPayDate.InvoiceId = ir.InvoiceId
LEFT JOIN bstAmountSent ON bstAmountSent.InvoiceId = i.Id
LEFT JOIN model.InvoiceSupplementals sup ON i.Id = sup.Id
LEFT JOIN model.StateOrProvinces st ON st.Id = sup.AccidentStateOrProvinceId
INNER JOIN model.Providers prov ON prov.Id = i.BillingProviderId
LEFT JOIN bstsExistsForInvoiceId bsti ON bsti.InvoiceId = ir.InvoiceId
WHERE ir.Id IN (
	SELECT Id FROM model.InvoiceReceivables
	WHERE InvoiceId IN (
		SELECT 
		InvoiceId
		FROM model.InvoiceReceivables
		GROUP BY InvoiceId HAVING COUNT(InvoiceId) = 1
	)
	AND PatientInsuranceId IS NULL
)

UNION ALL

SELECT 
(1 * 1000000000 + MIN(adj.Id)) AS ReceivableId
,CASE 
	WHEN SUM(ABS(adj.Amount) * -1) = -0.01
		THEN 0
	ELSE SUM(ABS(adj.Amount) * -1)
END AS OpenBalance
,'S' AS ReceivableType
,adj.PatientId
,0 AS InsuredId
,99999 AS InsurerId
,0 AS AppointmentId
,'' AS Invoice
,'' AS InvoiceDate
,CONVERT(REAL,0) AS Charge
,0 AS BillToDr
,'' AS AccType
,'' AS AccState
,'' AS HspAdmDate
,'' AS HspDisDate
,CONVERT(NVARCHAR,MIN(adj.Id)) AS Disability
,'' AS RsnType
,'' AS RsnDate
,'' AS FirstConsDate
,'' AS PrevCond
,'A' AS Status
,0 AS ReferDr
,0 AS UnallocatedBalance
,'' AS ExternalRefInfo
,'' AS Over90
,0 AS BillingOffice
,'' AS PatientBillDate
,'' AS LastPayDate
,0 AS ChargesByFee
,'' AS WriteOff
,'F' AS InsurerDesignation
FROM model.Adjustments adj
WHERE adj.InvoiceReceivableId IS NULL
GROUP BY adj.PatientId

GO