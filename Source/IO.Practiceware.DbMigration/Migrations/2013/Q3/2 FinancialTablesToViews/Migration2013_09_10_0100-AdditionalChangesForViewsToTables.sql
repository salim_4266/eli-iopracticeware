﻿--Need to add column to PatientReceivableServices so queries can join on appointmentId

IF NOT EXISTS (
	SELECT * FROM sys.columns 
	WHERE Name = N'AppointmentId' 
		AND Object_ID = Object_ID(N'dbo.PatientReceivableServices'))
BEGIN
    ALTER TABLE dbo.PatientReceivableServices 
	ADD AppointmentId AS SUBSTRING(Invoice, (CHARINDEX('-', Invoice) + 1), LEN(Invoice)) 
	PERSISTED
END

