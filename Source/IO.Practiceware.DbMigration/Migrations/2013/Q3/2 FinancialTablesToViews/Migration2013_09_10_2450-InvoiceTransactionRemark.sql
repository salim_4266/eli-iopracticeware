﻿IF OBJECT_ID ( 'dbo.InvoiceTransactionRemark', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.InvoiceTransactionRemark;
GO

CREATE PROCEDURE dbo.InvoiceTransactionRemark
	@ReceivableId INT
AS
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#InvoiceReceivableIds') IS NOT NULL
BEGIN
	DROP TABLE #InvoiceReceivableIds
END

SELECT 
* 
INTO #InvoiceReceivableIds
FROM (
	SELECT
	ir.Id
	FROM model.InvoiceReceivables ir
	WHERE ir.InvoiceId IN (
		SELECT
		ir.InvoiceId
		FROM model.InvoiceReceivables ir
		WHERE ir.Id = @ReceivableId
	)
)v

IF (@@ROWCOUNT > 0)
BEGIN

CREATE UNIQUE CLUSTERED INDEX IX_#InvoiceReceivableIds_Ids ON #InvoiceReceivableIds(Id)

SELECT 
SUBSTRING(ptj.TransactionRemark, 6,10) 
FROM PracticeTransactionJournal ptj
INNER JOIN #InvoiceReceivableIds irs ON irs.Id = ptj.TransactionTypeId
WHERE ptj.TransactionType = 'R' 
	AND ptj.TransactionStatus IN ('P', 'S')

END
GO