﻿--Financial Batches
EXEC dbo.DropObject 'model.FinancialBatches'
GO

-- Creating table 'FinancialBatches'
CREATE TABLE [model].[FinancialBatches] (
    [Id] int IDENTITY(1,1) NOT NULL,
	[FinancialSourceTypeId] int  NOT NULL,
    [PatientId] int NULL,
	[InsurerId] int NULL,
	[PaymentDateTime] datetime NULL,
	[ExplanationOfBenefitsDateTime] datetime  NULL,
    [Amount] decimal(18,2)  NOT NULL,
    [CheckCode] nvarchar(max)  NOT NULL,
	[ComputedFinancialBatchId] int NULL
);
GO

INSERT INTO model.FinancialBatches (FinancialSourceTypeId, PatientId, InsurerId, PaymentDateTime, ExplanationOfBenefitsDateTime, Amount, CheckCode, ComputedFinancialBatchId)
SELECT
    FinancialSourceTypeId,
    PatientId,
    InsurerId,
    PaymentDateTime,
	ExplanationOfBenefitsDateTime,
	Amount, 
	CheckCode, 
	ComputedFinancialBatchId
FROM dbo.FinancialBatchesBackup
GO

-- Creating primary key on [Id] in table 'FinancialBatches'
ALTER TABLE [model].[FinancialBatches]
ADD CONSTRAINT [PK_FinancialBatches]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO