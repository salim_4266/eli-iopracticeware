﻿IF EXISTS(SELECT * FROM dbo.sysobjects WHERE Name = 'AuditPracticeTransactionJournalUpdateTrigger' AND ObjectProperty(Id, 'IsTrigger') = 1)	
	DISABLE TRIGGER [PracticeTransactionJournal].[AuditPracticeTransactionJournalUpdateTrigger] ON 
	[dbo].[PracticeTransactionJournal]
GO

EXEC('
;WITH CTE AS (
SELECT 
ptj.TransactionId
,ir.Id
FROM dbo.BillingServiceTransactionsBackup bst 
INNER JOIN ServiceTransactions st ON bst.Id = st.id
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionId = st.TransactionId
	AND ptj.TransactionType = ''R'' AND TransactionRef <> ''G''
INNER JOIN PatientReceivablesBackup pr ON ptj.TransactionTypeId = pr.ReceivableId
INNER JOIN model.InvoiceReceivables ir ON ir.ComputedInvoiceReceivableId = bst.InvoiceReceivableId
)
UPDATE ptj
SET TransactionTypeId = Id
FROM PracticeTransactionJournal ptj
JOIN CTE ON CTE.TransactionId = ptj.TransactionId
')
GO

IF EXISTS(SELECT * FROM dbo.sysobjects WHERE Name = 'AuditPracticeTransactionJournalUpdateTrigger' AND ObjectProperty(Id, 'IsTrigger') = 1)
	ENABLE TRIGGER [PracticeTransactionJournal].[AuditPracticeTransactionJournalUpdateTrigger] ON 
	[dbo].[PracticeTransactionJournal]
GO

EXEC DropObject ServiceTransactions
GO

CREATE VIEW dbo.ServiceTransactions_Internal
WITH SCHEMABINDING, VIEW_METADATA
AS
SELECT 
bst.Id
,bst.LegacyTransactionId AS TransactionId
,bst.BillingServiceId AS ServiceId
,ptj.TransactionDate AS TransactionDate
,bst.AmountSent AS Amount
,CASE  
	WHEN (bst.BillingServiceTransactionStatusId = 1 AND ptj.TransactionStatus = 'P')
		THEN CASE 
				WHEN bst.AmountSent > 0
					THEN 'B' 
				ELSE 'P' 
			END
	WHEN (bst.BillingServiceTransactionStatusId = 3 AND ptj.TransactionStatus IN ('P','S'))
		THEN 'V'
	WHEN (bst.BillingServiceTransactionStatusId = 2 AND ptj.TransactionStatus IN ('P','S'))
		THEN 'P'
	WHEN (bst.BillingServiceTransactionStatusId = 4 AND ptj.TransactionStatus = 'S')
		THEN 'X'
	WHEN (bst.BillingServiceTransactionStatusId = 6 AND ptj.TransactionStatus = 'Z')
		THEN '1'
	WHEN (bst.BillingServiceTransactionStatusId = 6 AND ptj.TransactionStatus = 'S')
		THEN '1'
	WHEN (bst.BillingServiceTransactionStatusId = 7 AND ptj.TransactionStatus = 'Z')
		THEN '2'
	ELSE '0'
END AS TransportAction
,bst.[DateTime] AS ModTime
,CONVERT(UNIQUEIDENTIFIER,NULL) AS ExternalSystemMessageId
FROM model.BillingServiceTransactions bst
INNER JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionId = bst.LegacyTransactionId
	AND ptj.TransactionType = 'R'
	AND ptj.TransactionRef <> 'G'

GO

CREATE UNIQUE CLUSTERED INDEX [IX_ServiceTransactions_TransactionIdTransportAction] ON 
	[dbo].[ServiceTransactions_Internal](Id, TransactionId) 
GO

CREATE VIEW dbo.ServiceTransactions
WITH SCHEMABINDING, VIEW_METADATA
AS
SELECT 
sti.Id 
,sti.TransactionId
,sti.ServiceId
,sti.TransactionDate
,sti.Amount
,sti.TransportAction
,sti.ModTime
,sti.ExternalSystemMessageId
FROM dbo.ServiceTransactions_Internal sti WITH (NOEXPAND)
GO
