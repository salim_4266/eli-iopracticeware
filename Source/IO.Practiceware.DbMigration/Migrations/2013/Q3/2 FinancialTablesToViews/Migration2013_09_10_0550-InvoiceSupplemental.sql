﻿EXEC dbo.DropObject 'model.InvoiceSupplementals'
GO

-- Creating table 'InvoiceSupplementals'
CREATE TABLE [model].[InvoiceSupplementals] (
    [Id] int NOT NULL,
    [AccidentDateTime] datetime  NULL,
    [VisionPrescriptionDateTime] datetime  NULL,
    [DisabilityDateTime] datetime  NULL,
    [AdmissionDateTime] datetime  NULL,
    [DischargeDateTime] datetime  NULL,
    [ClaimDelayReasonId] int  NULL,
    [RelatedCause1Id] int  NULL,
    [AccidentStateOrProvinceId] int  NULL
);
GO

INSERT INTO model.InvoiceSupplementals (Id, AccidentDateTime, VisionPrescriptionDateTime, DisabilityDateTime, AdmissionDateTime, DischargeDateTime, ClaimDelayReasonId, RelatedCause1Id, AccidentStateOrProvinceId)
SELECT Id,
	AccidentDateTime, 
	VisionPrescriptionDateTime,
	DisabilityDateTime,
	AdmissionDateTime,
	DischargeDateTime,
	ClaimDelayReasonId,
	RelatedCause1Id,
	AccidentStateOrProvinceId
FROM dbo.InvoiceSupplementalsBackup
GO

-- Creating primary key on [Id] in table 'InvoiceSupplementals'
ALTER TABLE [model].[InvoiceSupplementals]
ADD CONSTRAINT [PK_InvoiceSupplementals]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

--ALTER TABLE [model].[InvoiceSupplementals]
--ADD CONSTRAINT [FK_InvoiceInvoiceSupplemental]
--    FOREIGN KEY ([Id])
--    REFERENCES [model].[Invoices]
--        ([Id])
--    ON DELETE NO ACTION ON UPDATE NO ACTION;
--GO