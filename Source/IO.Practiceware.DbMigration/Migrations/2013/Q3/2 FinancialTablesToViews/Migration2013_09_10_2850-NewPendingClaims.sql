﻿IF OBJECT_ID('dbo.NewPendingClaims', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[NewPendingClaims]

SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[NewPendingClaims] @SDate VARCHAR(8)
	,@EDate VARCHAR(8)
	,@RscId INT
	,@LocId INT
	,@RType VARCHAR(1)
AS
SET NOCOUNT ON

;WITH bstsExists AS (
	SELECT TOP 100 PERCENT
	InvoiceReceivableId
	,COUNT(Id) AS NumOccurancesOfThisInvoiceReceivableId
	FROM model.BillingServiceTransactions 
	GROUP BY InvoiceReceivableId
	ORDER BY InvoiceReceivableId
)

SELECT  prs.ServiceDate
	,ap.PatientId
	,CASE WHEN (ip.PolicyHolderPatientId IS NULL)
		THEN ap.PatientId
		ELSE ip.PolicyHolderPatientId
		END AS InsuredId
	,ap.ResourceId2
	,ap.ResourceId1
	,ap.ReferralId
	,CASE WHEN (ip.InsurerId IS NULL) 
		THEN 0 
		ELSE ip.InsurerId 
		END AS InsurerId
	,prs.LinkedDiag
	,prs.Service
	,prs.Modifier
	,prs.Quantity
	,prs.Charge
	,CASE 
		WHEN (
				(i.ReferringExternalProviderId IS NULL)
				OR (i.ReferringExternalProviderId = '')
				)
			THEN 0
		ELSE i.ReferringExternalProviderId
		END AS ReferDr
	,ir.Id as ReceivableId
	,ap.InvoiceNumber as Invoice
	,CONVERT(nvarchar, i.DATETIME, 112) as InvoiceDate
	,prs.ItemId
	,ap.AppointmentId
	,prs.Status
	,CASE WHEN (pin.InsurerName IS NULL)
		THEN 'ZZZ No insurance'
		ELSE pin.InsurerName
		END AS InsurerName
	,CASE WHEN (pin.InsurerAddress IS NULL)
		THEN ''
		ELSE pin.InsurerAddress
		END AS InsurerAddress
FROM model.InvoiceReceivables ir
LEFT JOIN model.PatientInsurances [pi] ON [pi].Id = ir.PatientInsuranceId
LEFT JOIN model.InsurancePolicies ip ON ip.Id = [pi].InsurancePolicyId
LEFT JOIN PracticeInsurers pin on pin.InsurerId = ip.InsurerId
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
RIGHT JOIN Appointments ap ON i.EncounterId = ap.AppointmentId
LEFT JOIN PatientReceivableServices prs ON prs.Invoice = ap.InvoiceNumber
LEFT JOIN bstsExists ON bstsExists.InvoiceReceivableId = ir.Id
WHERE 
(ap.AppDate >= @SDate)
	AND (ap.AppDate <= @EDate)
	AND (
		(ap.ResourceId1 = @RscId)
		OR (@RscId = - 1)
		)
	AND (
		(ap.ResourceId2 = @LocId)
		OR (@LocId = - 1)
		)
	AND (
			(CASE 
				WHEN ir.OpenForReview = 0
					THEN CASE 
							WHEN bstsExists.NumOccurancesOfThisInvoiceReceivableId IS NOT NULL
								THEN 'B'
							ELSE 'S'
							END
				WHEN ir.OpenForReview = 1
					THEN 'I'
				END = @RType)
			OR (@RType = '0')
		)
UNION ALL

SELECT 
 prs.ServiceDate
	,ap.PatientId
	,CASE WHEN (ip.PolicyHolderPatientId IS NULL)
		THEN ap.PatientId
		ELSE ip.PolicyHolderPatientId
		END AS InsuredId
	,ap.ResourceId2
	,ap.ResourceId1
	,ap.ReferralId
	,CASE WHEN (ip.InsurerId IS NULL) 
		THEN 0 
		ELSE ip.InsurerId 
		END AS InsurerId
	,prs.LinkedDiag
	,prs.Service
	,prs.Modifier
	,prs.Quantity
	,prs.Charge
	,CASE 
		WHEN (
				(i.ReferringExternalProviderId IS NULL)
				OR (i.ReferringExternalProviderId = '')
				)
			THEN 0
		ELSE i.ReferringExternalProviderId
		END AS ReferDr
	,ir.Id as ReceivableId
	,ap.InvoiceNumber as Invoice
	,CONVERT(nvarchar, i.DATETIME, 112) as InvoiceId
	,prs.ItemId
	,apASC.AppointmentId
	,prs.Status
	,CASE WHEN (pin.InsurerName IS NULL)
		THEN 'ZZZ No insurance'
		ELSE pin.InsurerName
		END AS InsurerName
	,CASE WHEN (pin.InsurerAddress IS NULL)
		THEN ''
		ELSE pin.InsurerAddress
		END AS InsurerAddress
FROM model.InvoiceReceivables ir
LEFT JOIN model.PatientInsurances [pi] ON [pi].Id = ir.PatientInsuranceId
LEFT JOIN model.InsurancePolicies ip ON ip.Id = [pi].InsurancePolicyId
LEFT JOIN PracticeInsurers pin on pin.InsurerId = ip.InsurerId
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
RIGHT JOIN Appointments ap ON i.EncounterId = ap.AppointmentId
INNER JOIN Appointments apASC ON apASC.PatientId = ap.PatientId
	AND apASC.AppTypeId = ap.AppTypeId
	AND apASC.AppDate = ap.AppDate
	AND apASC.AppTime = 0 AND ap.AppTime > 0 
	AND apASC.ScheduleStatus = ap.ScheduleStatus
	AND ap.ScheduleStatus = 'D'
	AND apASC.ResourceId1 = ap.ResourceId1
	AND apASC.ResourceId2 = ap.ResourceId2
	AND apASC.Comments = 'ASC CLAIM'
LEFT JOIN PatientReceivableServices prs ON prs.Invoice = ap.InvoiceNumber
LEFT JOIN bstsExists ON bstsExists.InvoiceReceivableId = ir.Id
WHERE (ap.AppDate >= @SDate)
	AND (ap.AppDate <= @EDate)
	AND (
		(ap.ResourceId1 = @RscId)
		OR (@RscId = - 1)
		)
	AND (
		(ap.ResourceId2 = @LocId)
		OR (@LocId = - 1)
		)
	AND (
			(CASE 
				WHEN ir.OpenForReview = 0
					THEN CASE 
							WHEN bstsExists.NumOccurancesOfThisInvoiceReceivableId IS NOT NULL
								THEN 'B'
							ELSE 'S'
							END
				WHEN ir.OpenForReview = 1
					THEN 'I'
				END = @RType)
			OR (@RType = '0')
		)
ORDER BY CONVERT(nvarchar, i.DATETIME, 112)
	,ap.PatientId
	
GO
--a