﻿EXEC dbo.DropObject 'model.BillingServiceTransactions'
GO

CREATE TABLE model.[BillingServiceTransactions] (
    [Id] uniqueidentifier DEFAULT NEWID() NOT NULL,
    [DateTime] datetime  NOT NULL,
    [AmountSent] decimal(18,2)  NOT NULL,
    [ClaimFileNumber] int  NULL,
    [BillingServiceTransactionStatusId] int  NOT NULL,
    [MethodSentId] int  NOT NULL,
    [BillingServiceId] int  NOT NULL,
    [InvoiceReceivableId] bigint  NULL,
    [ClaimFileReceiverId] int  NULL,
    [ExternalSystemMessageId] uniqueidentifier  NULL,
    [PatientAggregateStatementId] int  NULL,
	[LegacyTransactionId] int NULL
);
GO

INSERT INTO model.[BillingServiceTransactions]
([DateTime],AmountSent,ClaimFileNumber,BillingServiceTransactionStatusId,MethodSentId,BillingServiceId,InvoiceReceivableId,
	ClaimFileReceiverId,ExternalSystemMessageId,PatientAggregateStatementId,LegacyTransactionId)
SELECT 
bst.[DateTime]
,bst.AmountSent
,bst.ClaimFileNumber 
,bst.BillingServiceTransactionStatusId
,bst.MethodSentId
,bst.BillingServiceId
,ir.Id AS InvoiceReceivableId
,bst.ClaimFileReceiverId
,bst.ExternalSystemMessageId
,bst.PatientAggregateStatementId
,bst.TransactionId
FROM BillingServiceTransactionsBackup bst
JOIN InvoiceReceivablesBackup irb ON bst.InvoiceReceivableId = irb.Id
JOIN model.InvoiceReceivables ir ON ir.ComputedInvoiceReceivableId = irb.Id
GO

IF EXISTS(SELECT * FROM dbo.sysobjects WHERE Name = 'AuditPracticeTransactionJournalUpdateTrigger' AND ObjectProperty(Id, 'IsTrigger') = 1)	
	DISABLE TRIGGER [PracticeTransactionJournal].[AuditPracticeTransactionJournalUpdateTrigger] ON 
	[dbo].[PracticeTransactionJournal]
GO

SELECT 
ptj.TransactionId
,pr.ReceivableId AS OldTransactionTypeId
,ir.Id AS NewTransactionTypeId
INTO dbo.PracticeTransactionJournalReceivablesBackup
FROM model.InvoiceReceivables ir
JOIN model.Invoices i ON i.Id = ir.InvoiceId
LEFT JOIN PatientReceivablesBackup pr ON pr.AppointmentId = i.EncounterId
LEFT JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId

;WITH CTE AS (
SELECT 
ir.Id AS InvoiceReceivableId
,pr.ReceivableId
FROM model.InvoiceReceivables ir
JOIN model.Invoices i ON i.Id = ir.InvoiceId
LEFT JOIN PatientReceivablesBackup pr ON pr.AppointmentId = i.EncounterId
)
UPDATE ptj
SET ptj.TransactionTypeId = CTE.InvoiceReceivableId
FROM PracticeTransactionJournal ptj
JOIN CTE ON CTE.ReceivableId = ptj.TransactionTypeId
WHERE ptj.TransactionType = 'R'
	AND TransactionRef <> 'G'
GO

IF EXISTS(SELECT * FROM dbo.sysobjects WHERE Name = 'AuditPracticeTransactionJournalUpdateTrigger' AND ObjectProperty(Id, 'IsTrigger') = 1)
	ENABLE TRIGGER [PracticeTransactionJournal].[AuditPracticeTransactionJournalUpdateTrigger] ON 
	[dbo].[PracticeTransactionJournal]
GO

ALTER TABLE [model].[BillingServiceTransactions]
ADD CONSTRAINT [PK_BillingServiceTransactions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

CREATE INDEX [IX_FK_BillingServiceBillingServiceTransaction]
ON [model].[BillingServiceTransactions]
    ([BillingServiceId]);
GO

UPDATE bst 
SET bst.InvoiceReceivableId = NULL
FROM model.BillingServiceTransactions bst
WHERE bst.InvoiceReceivableId NOT IN (SELECT Id FROM model.InvoiceReceivables)

SELECT
*
INTO #DeletedBillingServiceTransactions
FROM model.BillingServiceTransactions
WHERE InvoiceReceivableId NOT IN (SELECT Id FROM model.InvoiceReceivables)

IF (@@ROWCOUNT > 0)
BEGIN
	RAISERROR('There are BillingServiceTransactions.InvoiceReceivableId that are not in (SELECT Id FROM model.InvoiceReceivables)... Invalid foreign keys.',16,2) WITH SETERROR
END

ALTER TABLE [model].[BillingServiceTransactions]
ALTER COLUMN InvoiceReceivableId INT NULL
GO

ALTER TABLE [model].[BillingServiceTransactions]
ADD CONSTRAINT [FK_InvoiceReceivableBillingServiceTransaction]
    FOREIGN KEY ([InvoiceReceivableId])
    REFERENCES [model].[InvoiceReceivables]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX [IX_FK_InvoiceReceivableBillingServiceTransaction]
ON [model].[BillingServiceTransactions]
    ([InvoiceReceivableId]);
GO

CREATE INDEX [IX_FK_ClaimFileReceiverBillingServiceTransaction]
ON [model].[BillingServiceTransactions]
    ([ClaimFileReceiverId]);
GO

ALTER TABLE [model].[BillingServiceTransactions]
ADD CONSTRAINT [FK_ExternalSystemMessageBillingServiceTransaction]
    FOREIGN KEY ([ExternalSystemMessageId])
    REFERENCES [model].[ExternalSystemMessages]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX [IX_FK_ExternalSystemMessageBillingServiceTransaction]
ON [model].[BillingServiceTransactions]
    ([ExternalSystemMessageId]);
GO

CREATE INDEX IX_BillingServiceTransactions_LegacyTransactionId ON model.BillingServiceTransactions (LegacyTransactionId)