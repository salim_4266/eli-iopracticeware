﻿IF OBJECT_ID('OnPatientReceivableUpdate','TR') IS NOT NULL
	DROP TRIGGER OnPatientReceivableUpdate
GO

CREATE TRIGGER OnPatientReceivableUpdate ON dbo.PatientReceivables 
INSTEAD OF UPDATE
AS
SET NOCOUNT ON;

IF EXISTS (
SELECT * FROM inserted
EXCEPT 
SELECT * FROM deleted
)
BEGIN

DECLARE @ProviderBillingOrganizationMax int
DECLARE @ProviderBillingOrganizationServiceLocationMax int
DECLARE @MainLocationId int
DECLARE @ServiceLocationMax int
DECLARE @ClinicalInvoiceProviderMax int

SET @ClinicalInvoiceProviderMax = 110000000
SET @ServiceLocationMax = 110000000
SET @ProviderBillingOrganizationMax = 110000000
SET @ProviderBillingOrganizationServiceLocationMax = 110000000
SET @MainLocationId = (SELECT TOP 1 PracticeId AS PracticeId FROM dbo.PracticeName WHERE PracticeType = 'P' AND LocationReference = '')

--step 1: let's get all the invoice id's we're updating.
SELECT 
ir.InvoiceId
,CASE 
	WHEN COUNT(ir.InvoiceId) = 1
		THEN 1 --only patientinsurance row
	WHEN COUNT(ir.InvoiceId) > 1
		THEN 2 -- patient has insurance
END AS InvoiceIdCounter 
INTO #InvoiceIds
FROM model.InvoiceReceivables ir 
INNER JOIN inserted i ON i.ReceivableId = ir.Id
INNER JOIN Appointments ap ON i.AppointmentId = ap.AppointmentId
GROUP BY ir.InvoiceId
,ap.ResourceId2
,ap.Comments

--make a table to handle our servicelocation codes
SELECT
ReceivableId
,AppointmentId
,CONVERT(BIT,0) AS IsExternal
,NULL AS ServiceLocationCodeId
INTO #ServiceLocationCodes
FROM inserted

--set the is external switch to on when :
UPDATE slc
SET IsExternal = 1
FROM #ServiceLocationCodes slc
JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
WHERE ResourceId2 BETWEEN 1 AND 999

UPDATE slc
SET ServiceLocationCodeId = (
	SELECT TOP 1 pct.Id
	FROM PracticeName pn
	INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE' 
		AND SUBSTRING(pct.Code, 1, 2) = '11')
FROM #ServiceLocationCodes slc
JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
WHERE IsExternal = 0
	AND ap.Comments <> 'ASC CLAIM'

UPDATE slc
SET ServiceLocationCodeId = pct.Id
FROM #ServiceLocationCodes slc
INNER JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
INNER JOIN Resources re ON re.ResourceId = ap.ResourceId2
INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE' 
		AND SUBSTRING(pct.Code, 1, 2) = re.PlaceOfService
WHERE IsExternal = 1
	AND ap.Comments <> 'ASC CLAIM'

DECLARE @pctSL11 INT
SET @pctSL11 = (SELECT 
			MAX(Id) 
			AS Id FROM dbo.PracticeCodeTable
			WHERE ReferenceType = 'PLACEOFSERVICE'
				AND SUBSTRING(Code, 1, 2) = '11'
			GROUP BY ReferenceType, Code
)

;WITH pctSLCode AS (
	SELECT
	MAX(pctSLCode.Id) AS Id
	,ap.AppointmentId
	FROM dbo.PracticeCodeTable pctSLCode 
	INNER JOIN #ServiceLocationCodes slc ON slc.AppointmentId IS NOT NULL
	INNER JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
		AND ap.Comments = 'ASC CLAIM' 
	INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
	INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P'
		AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
		AND PracticeType = 'P'
		AND LocationReference <> ''
	INNER JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
		AND reASC.ResourceType = 'R'
		AND reASC.ServiceCode = '02'
		AND reASC.Billable = 'Y'
		AND pic.FieldValue = 'T'
	WHERE SUBSTRING(pctSLCode.Code, 1, 2) = reASC.PlaceOfService
		AND pctSLCode.ReferenceType = 'PLACEOFSERVICE'
	GROUP BY ap.AppointmentId
)
UPDATE slc
SET ServiceLocationCodeId = CASE 
		WHEN pctSLCode.Id IS NOT NULL AND pctSLCode.Id <> ''
			THEN pctSLCode.Id
		ELSE @pctSL11
	END
FROM #ServiceLocationCodes slc
INNER JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
	AND ap.Comments = 'ASC CLAIM' 
LEFT OUTER JOIN pctSLCode ON pctSLCode.AppointmentId = ap.AppointmentId

IF EXISTS (
	SELECT
	*
	FROM inserted i
	WHERE i.ReceivableType = 'S'
		AND i.OpenBalance = 0
		AND i.InsuredId = 0
		AND i.InsurerId = 99999
		AND i.AppointmentId = 0
		AND i.Charge = 0
		AND i.BillToDr = 0
		AND i.BillingOffice = 0
		AND i.Status = 'A'
		AND i.ReceivableId - (1 * 1000000000) IN 
		(SELECT Id FROM model.Adjustments WHERE InvoiceReceivableId IS NULL AND Id IN 
		(SELECT ReceivableId-(1 * 1000000000) FROM inserted))
	)
BEGIN
	DECLARE @iOpenBalance DECIMAL(18,2)
	SET @iOpenBalance = (SELECT ABS(i.OpenBalance) FROM inserted i)

	IF (@iOpenBalance = 0)
	BEGIN
		UPDATE adj
		SET adj.Amount = -0.01
		FROM model.Adjustments adj
		INNER JOIN inserted i ON i.ReceivableId - (1 * 1000000000) = adj.Id
	END
	ELSE 
	BEGIN
		UPDATE adj
		SET adj.Amount = @iOpenBalance
		FROM model.Adjustments adj
		INNER JOIN inserted i ON i.ReceivableId - (1 * 1000000000) = adj.Id
	END
END

ELSE
BEGIN
--Patient does not have insurance and will not get insurance
IF EXISTS 
	(SELECT * FROM inserted i
		INNER JOIN model.InvoiceReceivables ir ON i.ReceivableId = ir.Id
		WHERE ir.PatientInsuranceId IS NULL AND i.InsurerId = 0)
BEGIN
	UPDATE ir
	SET OpenForReview = CASE 
		WHEN i.ReceivableType = 'I'
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0) END
	FROM model.InvoiceReceivables ir 
	INNER JOIN inserted i ON ir.Id = i.ReceivableId	
END

--Patient has insurance and will continue to have insurance
IF EXISTS (
		SELECT * FROM inserted i
		INNER JOIN model.InvoiceReceivables ir ON i.ReceivableId = ir.Id
		WHERE ir.PatientInsuranceId IS NOT NULL AND i.InsurerId <> 0)
BEGIN 
	UPDATE ir
	SET ir.OpenForReview = CASE 
		WHEN i.ReceivableType = 'I'
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0) 
		END
		,ir.PayerClaimControlNumber = i.ExternalRefInfo
	FROM model.InvoiceReceivables ir 
	INNER JOIN inserted i ON ir.Id = i.ReceivableId
	INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		AND ip.InsurerId = i.InsurerId 
		AND ip.PolicyHolderPatientId = i.InsuredId
END

--Patient did not have insurance and now has insurance.
DECLARE @ReceivableId INT
SELECT @ReceivableId = ReceivableId FROM inserted i

DECLARE @InvoiceId INT
SELECT @InvoiceId = ir.InvoiceId FROM model.InvoiceReceivables ir WHERE ir.Id = @ReceivableId

IF EXISTS (
	SELECT 
	PatientId
	FROM inserted i
	JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
	WHERE Id NOT IN (
		SELECT 
		PatientInsuranceId 
		FROM model.InvoiceReceivables 
		WHERE InvoiceId = 
			(SELECT 
			InvoiceId 
			FROM model.InvoiceReceivables 
			WHERE Id = 
				(SELECT 
				ReceivableId 
				FROM inserted
				)
			)
		)
	)
OR EXISTS (
	SELECT 
	d.ReceivableId 
	FROM deleted d
	INNER JOIN inserted i On i.ReceivableId = d.ReceivableId
		AND i.PatientId = d.PatientId
		AND i.AppointmentId = d.AppointmentId
	WHERE d.InsurerId = 0 AND i.InsurerId > 0
)
BEGIN
	CREATE TABLE #OutputPatientInsuranceId (PatientInsuranceId INT)
	INSERT INTO [model].[InvoiceReceivables] (
		[InvoiceId]
		,[PatientInsuranceId]
		,[PayerClaimControlNumber]
		,[PatientInsuranceAuthorizationId]
		,[PatientInsuranceReferralId]
		,[OpenForReview]
		)
	OUTPUT inserted.PatientInsuranceId INTO #OutputPatientInsuranceId
	SELECT @InvoiceId
		,pi.Id AS PatientInsuranceId
		,i.ExternalRefInfo AS PayerClaimControlNumber
		,CASE 
			WHEN (ap.PreCertId = '') OR (ap.PreCertId = 0) 
				THEN NULL
			ELSE ap.PreCertId 
		END AS PatientInsuranceAuthorizationId
		,CASE WHEN (ap.ReferralId = '') OR (ap.ReferralId = 0)
			THEN NULL
		ELSE ap.ReferralId
		END AS PatientInsuranceReferralId
		,CASE 
			WHEN i.InsurerDesignation = 'T'
				THEN CONVERT(BIT, 1)
			ELSE CONVERT(BIT, 0)
		END AS OpenForReview
	FROM inserted i
	LEFT JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
	LEFT JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
		AND (pi.EndDateTime IS NULL OR pi.EndDateTime = '' OR CONVERT(NVARCHAR,pi.EndDateTime,112) >= i.InvoiceDate)
		AND pi.IsDeleted <> 1
	LEFT JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id 
		AND ip.InsurerId = i.InsurerId
		AND CONVERT(NVARCHAR,ip.StartDateTime,112) <= i.InvoiceDate
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = i.ReceivableId
	INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId AND InvoiceIdCounter = 1

--set is open for review open false when not the minimum ordinalId, per insurance type.
	UPDATE ir
	SET ir.OpenForReview = CONVERT(BIT,0)
	,ir.PatientInsuranceAuthorizationId = NULL
	,ir.PatientInsuranceReferralId = NULL
	FROM model.InvoiceReceivables ir
	INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId AND InvoiceIdCounter = 1
	INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
	WHERE pi.OrdinalId NOT IN (
		SELECT MIN(pi.OrdinalId) 
		FROM model.InvoiceReceivables ir
		INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId 
		INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
		GROUP BY 
		ir.InvoiceId
		,pi.InsuranceTypeId)
	AND PatientInsuranceId IS NOT NULL

--Patient does not have insurance before udpate, the update is giving insurnace. Need to update all references to old receivableId
	UPDATE ptj
	SET ptj.TransactionTypeId = ir.Id
	FROM PracticeTransactionJournal ptj
	INNER JOIN inserted i ON ptj.TransactionTypeId = i.ReceivableId
	INNER JOIN model.InvoiceReceivables ir ON i.ReceivableId = ir.Id
	INNER JOIN model.Invoices inv ON ir.InvoiceId = inv.Id
	LEFT JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
	LEFT JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id AND ip.InsurerId = i.InsurerId
	LEFT JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
	WHERE TransactionType = 'R' AND TransactionRef <> 'G'
END

---invoices
	UPDATE iv
	SET ClinicalInvoiceProviderId = 
			CASE 
				WHEN ap.Comments <> 'ASC CLAIM' 
					THEN model.GetBigPairId(ap.ResourceId1, iv.EncounterId, @ClinicalInvoiceProviderMax)
				ELSE model.GetBigPairId(reASC.ResourceId, iv.EncounterId, @ClinicalInvoiceProviderMax) 
			END
		,BillingProviderId = 
			model.GetBigPairId(CASE --1st
				WHEN ap.ResourceId2 = 0
					THEN @MainLocationId
				WHEN ap.ResourceId2 > 999
					THEN (ap.ResourceId2 - 1000)
				ELSE model.GetPairId(1, ap.ResourceId2, @ServiceLocationMax)
			END 
			,model.GetBigPairId(CASE --2nd number
				WHEN ap.Comments <> 'ASC CLAIM'
					THEN re.PracticeId
				WHEN ap.Comments = 'ASC CLAIM'
					THEN (ap.ResourceId2 - 1000)
				END 
			,CASE 
			WHEN ap.Comments <> 'ASC CLAIM'
				THEN i.BillToDr
			ELSE (
					SELECT TOP 1 ResourceId
					FROM Resources
					WHERE PracticeId = (ap.ResourceId2 - 1000) AND ResourceType = 'R' AND ServiceCode = '02' AND Billable = 'Y'
				)
			END, 
			@ProviderBillingOrganizationMax), 
			@ProviderBillingOrganizationServiceLocationMax)
		,ReferringExternalProviderId = 
			CASE
				WHEN (i.ReferDr = 0 OR i.ReferDr = '')
					THEN NULL
				ELSE i.ReferDr 
			END
		,ServiceLocationCodeId = slc.ServiceLocationCodeId
		,iv.LegacyPatientBilledDateTime = 
			CASE 
				WHEN i.PatientBillDate = ''
					THEN NULL
				ELSE CONVERT(datetime, i.PatientBillDate, 112) 
			END				
		,AttributeToServiceLocationId = 
			CASE 
				WHEN i.BillingOffice = 0
					THEN 1
				WHEN i.BillingOffice BETWEEN 1 AND 999
					THEN model.GetPairId(1, reAtribTo.ResourceId, @ServiceLocationMax)
				ELSE i.BillingOffice - 1000
			END
	FROM inserted i
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = i.ReceivableId
	INNER JOIN model.Invoices iv ON iv.Id = ir.InvoiceId
	LEFT JOIN #ServiceLocationCodes slc ON slc.ReceivableId = i.ReceivableId
	INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = iv.Id
	INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
	INNER JOIN model.Patients p ON p.Id = i.PatientId
	LEFT JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
	LEFT JOIN dbo.Resources reAtribTo ON i.BillingOffice = reAtribTo.ResourceId
	LEFT JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId 
		AND apEncounter.AppTypeId = ap.AppTypeId 
		AND apEncounter.AppDate = ap.AppDate 
		AND apEncounter.AppTime > 0 
		AND ap.AppTime = 0 
		AND apEncounter.ScheduleStatus = ap.ScheduleStatus 
		AND ap.ScheduleStatus = 'D' 
		AND apEncounter.ResourceId1 = ap.ResourceId1 
		AND apEncounter.ResourceId2 = ap.ResourceId2
	LEFT JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
	LEFT JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P'
		AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
		AND PracticeType = 'P'
		AND LocationReference <> ''
	LEFT JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
		AND reASC.ResourceType = 'R'
		AND reASC.ServiceCode = '02'
		AND reASC.Billable = 'Y'
		AND pic.FieldValue = 'T'

-- invoice supplemental logic
IF EXISTS (
SELECT *
FROM inserted i
WHERE FirstConsDate <> '' OR HspAdmDate <> '' OR Over90 <> '' OR Over90 <> '' OR RsnDate <> '' OR HspDisDate <> '' OR AccType <> ''
)
AND NOT EXISTS(SELECT * FROM model.InvoiceSupplementals WHERE Id = @InvoiceId)
BEGIN
	INSERT INTO model.InvoiceSupplementals (
		Id
		,AccidentDateTime
		,AccidentStateOrProvinceId
		,AdmissionDateTime
		,ClaimDelayReasonId
		,DisabilityDateTime
		,DischargeDateTime
		,RelatedCause1Id
		,VisionPrescriptionDateTime
		)
	SELECT @InvoiceId AS Id
		,CASE 
			WHEN FirstConsDate = ''
				THEN NULL
			ELSE CONVERT(DATETIME, FirstConsDate, 101)
			END AS AccidentDateTime
		,CASE st.id
			WHEN 0
				THEN NULL
			ELSE st.id
			END AS AccidentStateOrProvinceId
		,CASE 
			WHEN HspAdmDate = ''
				THEN NULL
			ELSE CONVERT(DATETIME, HspAdmDate, 101)
			END AS AdmissionDateTime
		,CASE Over90
			WHEN 1
				THEN 1
			WHEN 2
				THEN 2
			WHEN 3
				THEN 3
			WHEN 4
				THEN 4
			WHEN 5
				THEN 5
			WHEN 6
				THEN 6
			WHEN 7
				THEN 7
			WHEN 8
				THEN 8
			WHEN 9
				THEN 9
			WHEN 10
				THEN 10
			WHEN 11
				THEN 11
			WHEN 15
				THEN 12
			ELSE NULL
			END AS ClaimDelayReasonId
		,CASE 
			WHEN COALESCE(RsnDate, '') <> ''
				THEN CONVERT(DATETIME, RsnDate, 101)
			ELSE NULL
			END AS DisabilityDateTime
		,CASE 
			WHEN COALESCE(HspDisDate, '') <> ''
				THEN CONVERT(DATETIME, HspDisDate, 101)
			ELSE NULL
			END AS DischargeDateTime
		,CASE AccType
			WHEN 'A'
				THEN 1
			WHEN 'E'
				THEN 2
			WHEN 'O'
				THEN 3
			ELSE NULL
			END AS RelatedCause1Id
		,CONVERT(DATETIME, NULL, 101) AS VisionPrescriptionDateTime
	FROM inserted i
	INNER JOIN model.InvoiceReceivables ir ON i.ReceivableId = ir.Id
	LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = i.AccState
	WHERE NOT EXISTS (SELECT * FROM model.InvoiceSupplementals WHERE Id = @InvoiceId)
END
	
ELSE IF EXISTS (
	SELECT *
	FROM inserted i
	WHERE FirstConsDate <> '' OR HspAdmDate <> '' OR Over90 <> '' OR Over90 <> '' OR RsnDate <> '' OR HspDisDate <> '' OR AccType <> '')
AND EXISTS(SELECT * FROM model.InvoiceSupplementals WHERE Id = @InvoiceId)
BEGIN
	UPDATE ivs
	SET AccidentDateTime = CASE WHEN i.FirstConsDate = '' THEN NULL ELSE CONVERT(datetime,i.FirstConsDate, 101) END
	,AccidentStateOrProvinceId = CASE 
		WHEN st.id IS NULL
			THEN NULL
		ELSE st.id
		END
	,AdmissionDateTime = CASE WHEN i.HspAdmDate = '' THEN NULL ELSE CONVERT(datetime,i.HspAdmDate, 101) END
	,ClaimDelayReasonId = CASE Over90
		WHEN 1
			THEN 1
		WHEN 2
			THEN 2
		WHEN 3
			THEN 3
		WHEN 4
			THEN 4
		WHEN 5
			THEN 5
		WHEN 6
			THEN 6
		WHEN 7
			THEN 7
		WHEN 8
			THEN 8
		WHEN 9
			THEN 9
		WHEN 10
			THEN 10
		WHEN 11
			THEN 11
		WHEN 15
			THEN 12
		ELSE NULL
		END
	,DisabilityDateTime = CASE
		WHEN COALESCE(RsnDate, '') <> ''
			THEN CONVERT(datetime,RsnDate, 101) 
		ELSE NULL END
	,DischargeDateTime = CASE 
		WHEN COALESCE(HspDisDate, '') <> ''
			THEN CONVERT(datetime, HspDisDate, 101)
		ELSE NULL END
	,RelatedCause1Id = CASE AccType
		WHEN 'A'
			THEN 1
		WHEN 'E'
			THEN 2
		WHEN 'O'
			THEN 3
		ELSE NULL
		END
	,VisionPrescriptionDateTime = CONVERT(datetime, NULL, 101)
	FROM inserted i 
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = i.ReceivableId
	INNER JOIN model.InvoiceSupplementals ivs ON @InvoiceId = ivs.Id
	LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = i.AccState
END

-- for updating the InsurerDesignation.
SELECT ReceivableId
,InsurerDesignation
INTO #NeedsLegacyInsurerDesignationUpdate
FROM inserted

EXCEPT

SELECT ReceivableId
,InsurerDesignation
FROM deleted

UPDATE ir
SET LegacyInsurerDesignation = CASE WHEN z.Id IS NOT NULL THEN 'T' ELSE NULL END
FROM model.InvoiceReceivables ir
LEFT JOIN (
	SELECT 
	v.Id
	,v.InvoiceId
	FROM (
			SELECT
			ROW_NUMBER() OVER (PARTITION BY pi.InsuranceTypeId
				ORDER BY pi.InsuranceTypeId, pi.OrdinalId, PolicyHolderRelationshipTypeId) as rn
			,ir.InvoiceId
			,pi.Id
			FROM model.InvoiceReceivables ir
			INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
			INNER JOIN model.Encounters e ON e.Id = i.EncounterId
			INNER JOIN dbo.Appointments ap ON ap.AppointmentId = e.Id
			INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
				AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,ap.Appdate))
				AND pi.IsDeleted = 0
			INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
				AND ip.StartDateTime <= CONVERT(DATETIME,ap.Appdate)
			INNER JOIN #NeedsLegacyInsurerDesignationUpdate nlidu ON nlidu.ReceivableId = ir.Id
			WHERE PatientInsuranceId IS NOT NULL
	) v WHERE v.rn = 1
)z ON z.InvoiceId = ir.InvoiceId
	AND z.Id = ir.PatientInsuranceId
INNER JOIN #NeedsLegacyInsurerDesignationUpdate nlidu ON nlidu.ReceivableId = ir.Id
WHERE ir.PatientInsuranceId IS NOT NULL

END
END
GO