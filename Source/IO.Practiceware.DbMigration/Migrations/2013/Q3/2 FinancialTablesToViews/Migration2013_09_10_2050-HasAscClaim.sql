﻿IF OBJECT_ID ( 'dbo.HasAscClaim', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.HasAscClaim;
GO

CREATE PROCEDURE dbo.HasAscClaim
	@ItemId NVARCHAR(12)
AS
SET NOCOUNT ON;

SELECT
*
FROM dbo.Appointments ap
INNER JOIN model.Invoices i ON i.InvoiceTypeId = 2
	AND i.EncounterId = ap.AppointmentId
INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
INNER JOIN model.Adjustments adj ON adj.InvoiceReceivableId = ir.Id
INNER JOIN PatientReceivableServices prs ON adj.BillingServiceId = prs.ItemId
	AND prs.ItemId = @ItemId
WHERE ap.Comments = 'ASC CLAIM'
GO