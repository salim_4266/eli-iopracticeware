﻿IF OBJECT_ID('[dbo].[GetServicesByServicePatientIdServiceDate]', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[GetServicesByServicePatientIdServiceDate];
GO

CREATE PROCEDURE [dbo].[GetServicesByServicePatientIdServiceDate] 
	@PatientId INT
	,@ServiceDate NVARCHAR(8)
	,@Service NVARCHAR(MAX)
AS
BEGIN

SELECT DISTINCT
* 
FROM PatientReceivableServices prs
INNER JOIN model.Invoices i ON i.LegacyPatientReceivablesInvoice = prs.Invoice
WHERE prs.[Status] = 'A'
AND prs.[Service] LIKE @Service + '%'
AND i.LegacyPatientReceivablesPatientId = @PatientId
AND CONVERT(NVARCHAR,i.[DateTime],112) = @ServiceDate
END
GO