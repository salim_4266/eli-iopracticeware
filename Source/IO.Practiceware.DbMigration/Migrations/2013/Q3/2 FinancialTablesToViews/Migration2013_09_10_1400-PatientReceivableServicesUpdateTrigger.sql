﻿IF OBJECT_ID('dbo.PatientReceivableServicesUpdate', 'TR') IS NOT NULL
	DROP TRIGGER dbo.PatientReceivableServicesUpdate
GO

CREATE TRIGGER PatientReceivableServicesUpdate ON dbo.PatientReceivableServices
FOR UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @ItemId INT
	SET @ItemId = (SELECT TOP 1 ItemId FROM inserted)

	DECLARE @NewPlaceOfService INT
	SET @NewPlaceOfService = (SELECT TOP 1 PlaceOfService FROM inserted)
	
	DECLARE @OldPlaceOfService INT
	SET @OldPlaceOfService = (SELECT PlaceOfService FROM PatientReceivableServices WHERE ItemId = @ItemId)

	DECLARE @AppointmentId INT
	SET @AppointmentId = (SELECT TOP 1 AppointmentId FROM inserted)

	IF (@NewPlaceOfService = @OldPlaceOfService)
	BEGIN
		SELECT 
		* 
		INTO #InvoiceReceivableIds
		FROM (
			SELECT
			ir.Id 
			FROM model.Invoices i
			INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
			WHERE i.EncounterId = @AppointmentId

			UNION ALL 
			
			SELECT 
			ir.Id
			FROM dbo.Appointments ap
			INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
				AND apEncounter.AppTypeId = ap.AppTypeId
				AND apEncounter.AppDate = ap.AppDate
				AND apEncounter.AppTime > 0 
				AND ap.AppTime = 0
				AND apEncounter.ScheduleStatus = ap.ScheduleStatus
				AND ap.ScheduleStatus = 'D'
				AND apEncounter.ResourceId1 = ap.ResourceId1
				AND apEncounter.ResourceId2 = ap.ResourceId2
			INNER JOIN model.Invoices inv on inv.EncounterId = apEncounter.AppointmentId AND inv.InvoiceTypeId = 2
			INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = inv.Id
			WHERE ap.AppointmentId = @AppointmentId AND ap.Comments = 'ASC CLAIM'
		) v
	
		UPDATE inv
		SET inv.ServiceLocationCodeId = 
			(
				SELECT 
				Id 
				FROM model.ServiceLocationCodes slc 
				WHERE PlaceOfServiceCode = @NewPlaceOfService
			)
		FROM model.Invoices inv
		INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = inv.Id
		INNER JOIN #InvoiceReceivableIds irs ON irs.Id = ir.Id
	END
		
	IF EXISTS (
		SELECT 
		*
		FROM model.Adjustments adj
		INNER JOIN PatientReceivableServices prs ON adj.BillingServiceId = prs.ItemId
		INNER JOIN inserted i ON i.ItemId = prs.ItemId
		WHERE prs.Status = 'X'
	)
	BEGIN
		;WITH CTE AS (
		SELECT
		adj.Id
		,adj.BillingServiceId
		,prs.Status
		,prs.ItemId
		,prs.Invoice
		FROM PatientReceivableServices prs
		INNER JOIN inserted i ON i.Invoice = prs.Invoice
			AND i.Service = prs.Service
			AND i.ServiceDate = prs.ServiceDate
		LEFT JOIN model.Adjustments adj ON adj.BillingServiceId = prs.ItemId
		)
		UPDATE adj
		SET adj.BillingServiceId = y.ItemId
		--SELECT
		--adj.BillingServiceId
		--,x.*
		--,y.*
		FROM model.Adjustments adj
		INNER JOIN CTE x ON x.Id = adj.Id
		INNER JOIN PatientReceivableServices prs ON prs.ItemId = x.ItemId
		LEFT JOIN CTE y ON prs.Invoice = y.Invoice
			AND y.Status = 'A'
	END

	IF EXISTS (
		SELECT 
		*
		FROM model.FinancialInformations fi
		INNER JOIN PatientReceivableServices prs ON fi.BillingServiceId = prs.ItemId
		INNER JOIN inserted i ON i.ItemId = prs.ItemId
		WHERE prs.Status = 'X'
	)
	BEGIN
		;WITH CTE AS (
		SELECT
		fi.Id
		,fi.BillingServiceId
		,prs.Status
		,prs.ItemId
		,prs.Invoice
		FROM PatientReceivableServices prs
		INNER JOIN inserted i ON i.Invoice = prs.Invoice
			AND i.Service = prs.Service
			AND i.ServiceDate = prs.ServiceDate
		LEFT JOIN model.FinancialInformations fi ON fi.BillingServiceId = prs.ItemId
		)
		UPDATE fi
		SET fi.BillingServiceId = y.ItemId
		FROM model.FinancialInformations fi
		INNER JOIN CTE x ON x.Id = fi.Id
		INNER JOIN PatientReceivableServices prs ON prs.ItemId = x.ItemId
		LEFT JOIN CTE y ON prs.Invoice = y.Invoice
			AND y.Status = 'A'
	END

END