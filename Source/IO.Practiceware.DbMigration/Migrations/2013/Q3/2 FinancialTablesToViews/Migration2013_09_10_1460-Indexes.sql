﻿CREATE INDEX [IX_FK_InvoiceSupplementalStateOrProvinceClaimDelayReasonId]
ON [model].[InvoiceSupplementals]
    ([AccidentStateOrProvinceId]) INCLUDE ([ClaimDelayReasonId]);
GO

CREATE INDEX [IX_FK_AdjustmentAdjustmentType]
ON [model].[Adjustments]
    ([AdjustmentTypeId]);
GO

CREATE INDEX [IX_FK_InvoiceAttributeToServiceLocationIdBillingProviderIdEncounterId]
ON [model].[Invoices]
    (AttributeToServiceLocationId) INCLUDE (BillingProviderId, [EncounterId]);
GO

CREATE INDEX [IX_FK_BillingServiceTransactionStatus]
ON [model].[BillingServiceTransactions]
    ([BillingServiceTransactionStatusId]);
GO

CREATE INDEX [IX_PatientInsuranceEndDateTimeOrdinalId]
ON model.PatientInsurances
 (EndDateTime) INCLUDE (OrdinalId);
GO

CREATE INDEX [IX_FK_AdjustmentPostedDateTime]
ON [model].[Adjustments]
    ([PostedDateTime]);
GO

CREATE INDEX [IX_AppointmentsAppTimeResourceId1AppTypeIdScheduleStatusCommentsResourceId2]
ON Appointments
 (AppTime) INCLUDE (ResourceId1,AppTypeId,ScheduleStatus,Comments,ResourceId2);
GO

CREATE INDEX IX_PatientRecievableServices_AppointmentId ON dbo.PatientReceivableServices (AppointmentId)
GO

ALTER TABLE model.FinancialBatches ALTER COLUMN CheckCode NVARCHAR(200)
GO

CREATE INDEX IX_FinancialBatches_CheckCode ON model.FinancialBatches (CheckCode)
GO