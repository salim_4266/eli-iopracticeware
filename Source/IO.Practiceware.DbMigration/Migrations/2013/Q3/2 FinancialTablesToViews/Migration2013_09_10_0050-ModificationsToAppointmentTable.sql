﻿--Create column
IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'EncounterId' and Object_ID = Object_ID(N'Appointments')) 
BEGIN
	ALTER TABLE dbo.Appointments
	ADD EncounterId int NULL
END
	
GO

DECLARE @triggerNames xml
EXEC dbo.DisableEnabledTriggers 'dbo.Appointments', @names = @triggerNames OUTPUT

--Fill column with correct EncounterId
UPDATE Appointments
SET EncounterId = AppointmentId
WHERE Comments <> 'ASC CLAIM'

UPDATE apASC
SET apASC.EncounterId = ap.AppointmentId
FROM Appointments apASC
INNER JOIN dbo.Appointments ap ON apASC.PatientId = ap.PatientId
	AND apASC.AppTypeId = ap.AppTypeId
	AND apASC.AppDate = ap.AppDate
	AND apASC.AppTime = 0 AND ap.AppTime > 0 
	AND apASC.ScheduleStatus = ap.ScheduleStatus
	AND ap.ScheduleStatus = 'D'
	AND apASC.ResourceId1 = ap.ResourceId1
	AND apASC.ResourceId2 = ap.ResourceId2
	AND ap.Comments <> 'ASC CLAIM'
WHERE apasc.Comments = 'ASC CLAIM'

EXEC dbo.EnableTriggers @triggerNames

GO

--Create trigger on Insert
IF OBJECT_ID('UpdateEncounterId') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER UpdateEncounterId')
END

GO

CREATE TRIGGER UpdateEncounterId
ON dbo.Appointments
AFTER INSERT
AS
SET NOCOUNT ON
BEGIN

IF EXISTS(SELECT * FROM inserted i WHERE i.Comments <> 'ASC CLAIM')
BEGIN 
	UPDATE ap
	SET ap.EncounterId = i.AppointmentId
	FROM Appointments ap
	INNER JOIN inserted i ON i.AppointmentId = ap.AppointmentId
END

IF EXISTS(SELECT * FROM inserted i WHERE i.Comments = 'ASC CLAIM')
BEGIN
	UPDATE apASC
	SET apASC.EncounterId = ap.AppointmentId
	FROM Appointments apASC
	INNER JOIN inserted i ON apASC.AppointmentId = i.AppointmentId
	INNER JOIN dbo.Appointments ap ON apASC.PatientId = ap.PatientId
		AND apASC.AppTypeId = ap.AppTypeId
		AND apASC.AppDate = ap.AppDate
		AND apASC.AppTime = 0 AND ap.AppTime > 0 
		AND apASC.ScheduleStatus = ap.ScheduleStatus
		AND ap.ScheduleStatus = 'D'
		AND apASC.ResourceId1 = ap.ResourceId1
		AND apASC.ResourceId2 = ap.ResourceId2
		AND ap.Comments <> 'ASC CLAIM'
	WHERE apasc.Comments = 'ASC CLAIM'
		AND i.AppointmentId = apASC.AppointmentId
END
END

GO

IF NOT EXISTS(SELECT name FROM sysindexes where name = 'IX_EncounterId') 
CREATE NONCLUSTERED INDEX [IX_EncounterId] ON [dbo].[Appointments]
(
	[EncounterId] ASC
)
GO

IF NOT EXISTS(
SELECT * 
FROM sys.columns 
WHERE Name = N'InvoiceNumber' and Object_Id = Object_Id(N'Appointments'))    
BEGIN
   ALTER TABLE dbo.Appointments ADD InvoiceNumber AS 
	(CONVERT(NVARCHAR(30),CASE WHEN (LEN(CONVERT(NVARCHAR,PatientId)) <= 5 AND LEN(CONVERT(NVARCHAR,AppointmentId)) <= 5)
			THEN REPLACE(STR(CONVERT(NVARCHAR,PatientId), 5, 0), ' ', '0')+'-'+REPLACE(STR(CONVERT(NVARCHAR,AppointmentId) , 5, 0), ' ', '0')
		WHEN (LEN(CONVERT(NVARCHAR,PatientId)) > 5 AND LEN(CONVERT(NVARCHAR,AppointmentId)) <= 5)
			THEN CONVERT(NVARCHAR,PatientId)+'-'+REPLACE(STR(CONVERT(NVARCHAR,AppointmentId) , 5, 0), ' ', '0')
		WHEN (LEN(CONVERT(NVARCHAR,PatientId)) <= 5 AND LEN(CONVERT(NVARCHAR,AppointmentId)) > 5)
			THEN REPLACE(STR(CONVERT(NVARCHAR,PatientId), 5, 0), ' ', '0')+'-'+CONVERT(NVARCHAR,AppointmentId)
		ELSE CONVERT(NVARCHAR,PatientId)+'-'+CONVERT(NVARCHAR,AppointmentId)END)) PERSISTED
END


