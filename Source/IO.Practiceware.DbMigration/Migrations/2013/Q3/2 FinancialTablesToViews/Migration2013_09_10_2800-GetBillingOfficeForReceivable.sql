﻿IF OBJECT_ID('dbo.GetBillingOfficeForReceivable', 'P') IS NOT NULL
	DROP PROCEDURE dbo.GetBillingOfficeForReceivable;
GO

CREATE PROCEDURE dbo.GetBillingOfficeForReceivable @InvoiceReceivableId BIGINT
AS
SET NOCOUNT ON;

DECLARE @invoice VARCHAR(255)

SET @invoice = (
		SELECT TOP 1 invoice
		FROM PatientReceivables
		WHERE ReceivableId = @InvoiceReceivableId
		)

;WITH attributeToServiceLocations
AS (
	SELECT TOP 100 PERCENT Id
		,CASE 
			WHEN ShortName = 'Office'
				THEN 0
			WHEN ShortName LIKE 'Office-%'
				THEN Id + 1000
			WHEN IsExternal = 1
				THEN (
						SELECT y
						FROM model.GetIdPair(sl.Id, 110000000)
						)
			ELSE Id
			END AS PrBillingOfficeId
	FROM model.ServiceLocations sl
	ORDER BY Id
	)
	,invoices
AS (
	SELECT InvoiceId
	FROM model.InvoiceReceivables
	WHERE Id = @InvoiceReceivableId
	)
SELECT ir.Id AS ReceivableId
	,asl.PrBillingOfficeId AS BillingOffice
FROM model.InvoiceReceivables ir
INNER JOIN model.Invoices i ON ir.InvoiceId = i.Id
INNER JOIN attributeToServiceLocations asl ON i.AttributeToServiceLocationId = asl.Id
WHERE InvoiceId IN (
		SELECT InvoiceId
		FROM invoices
		)
	AND (
		-- there is insurance
		ir.PatientInsuranceId IS NOT NULL
		-- or, this is the only receivable for the invoice
		OR 1 = (
			SELECT COUNT(*)
			FROM model.InvoiceReceivables
			WHERE InvoiceId IN (
					SELECT InvoiceId
					FROM invoices
					)
			)
		)
GO

