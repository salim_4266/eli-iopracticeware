﻿IF EXISTS(SELECT * FROM sys.columns 
        WHERE [name] = N'OldId' AND [object_id] = OBJECT_ID(N'model.Adjustments'))
BEGIN
EXEC (
'INSERT INTO AuditEntries (Id, ChangeTypeId, AuditDateTime, HostName, ServerName, ObjectName, UserId, KeyNames, KeyValues)
SELECT 
	NEWID()
	,0
	,CONVERT(DATETIME, prb.PaymentDate)
	,@@servername
	,@@servername
	,''model.Adjustments''
	,prb.PaymentAssignBy
	,''Id''
	,ad.Id
FROM model.Adjustments ad
INNER JOIN PatientReceivablePaymentsBackup prb ON ad.OldId = prb.PaymentId')

END