﻿ALTER TABLE model.PatientInsurances
DROP COLUMN ComputedPatientInsuranceId

ALTER TABLE model.InvoiceReceivables
DROP COLUMN ComputedInvoiceReceivableId

ALTER TABLE model.FinancialBatches
DROP COLUMN ComputedFinancialBatchId

ALTER TABLE model.Invoices
DROP COLUMN ComputedInvoiceId

ALTER TABLE [model].[Adjustments]
DROP COLUMN [OldId]

ALTER TABLE [model].[FinancialInformations]
DROP COLUMN [OldId]


IF EXISTS (
SELECT
MAX(ir.Id) AS Id
,ir.InvoiceId
,pi.OrdinalId
,ir.PatientInsuranceId
FROM model.InvoiceReceivables ir
INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
GROUP BY ir.InvoiceId, pi.OrdinalId, ir.PatientInsuranceId
HAVING COUNT(*) > 1
)
BEGIN
	;WITH CTE AS (
	SELECT ir.Id
		,ir.InvoiceId
		,ir.PatientInsuranceId
		,COUNT(adj.InvoiceReceivableId) AS AssociatedAdjustments
		,COUNT(bst.InvoiceReceivableId) AS AssociatedBillingServiceTransactions
		,COUNT(fi.InvoiceReceivableId) AS AssociatedFinancialInformations
	FROM model.InvoiceReceivables ir
	LEFT JOIN model.Adjustments adj ON adj.InvoiceReceivableId = ir.Id
	LEFT JOIN model.BillingServiceTransactions bst ON bst.InvoiceReceivableId = ir.Id
	LEFT JOIN model.FinancialInformations fi ON fi.InvoiceReceivableId = ir.Id
	WHERE ir.InvoiceId IN (
			SELECT ir.InvoiceId
			FROM model.InvoiceReceivables ir
			INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
			GROUP BY ir.InvoiceId
				,pi.OrdinalId
				,ir.PatientInsuranceId
			HAVING COUNT(*) > 1
			)
		AND PatientInsuranceId IS NOT NULL
	GROUP BY adj.InvoiceReceivableId
		,ir.Id
		,ir.PatientInsuranceId
		,ir.InvoiceId
	)
	DELETE ir
	FROM model.InvoiceReceivables ir
	INNER JOIN (
	SELECT 
	MAX(Id) AS InvoiceReceivableId
	,InvoiceId
	FROM CTE
	WHERE AssociatedAdjustments = 0
		AND AssociatedBillingServiceTransactions = 0
		AND AssociatedFinancialInformations = 0
	GROUP BY CTE.InvoiceId
	HAVING COUNT(CTE.InvoiceId) = 1
	) v ON v.InvoiceId = ir.InvoiceId
		AND v.InvoiceReceivableId = ir.Id
END