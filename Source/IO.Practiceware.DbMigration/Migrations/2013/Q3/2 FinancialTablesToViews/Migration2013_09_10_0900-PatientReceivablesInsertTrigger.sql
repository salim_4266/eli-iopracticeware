﻿IF OBJECT_ID('OnPatientReceivableInsert','TR') IS NOT NULL
	DROP TRIGGER OnPatientReceivableInsert
GO

CREATE TRIGGER OnPatientReceivableInsert ON dbo.PatientReceivables
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @QualifyInsertedInvoiceReceivableAsTypeId INT

	SELECT TOP 1 @QualifyInsertedInvoiceReceivableAsTypeId =
	CASE i.InsurerId
		WHEN 0
			THEN 1 --Patient InvoiceReceivable
		WHEN 99999
			THEN 2 --Credit
		ELSE 3 --Patient + Insurance InvoiceReceivable
	END
	FROM inserted i
	
	DECLARE @ClinicalInvoiceProviderMax INT
	DECLARE @ProviderBillingOrganizationMax INT
	DECLARE @ProviderBillingOrganizationServiceLocationMax INT
	DECLARE @MainLocationId INT
	DECLARE @ServiceLocationCodeId INT
	DECLARE @InvoiceId INT
	DECLARE @ServiceLocationMax INT
	DECLARE @PatientId INT
	DECLARE @NumberOfInsuranceRowsInserted  INT
	DECLARE @InvoiceCreatedOnThisInsertTrigger INT

	IF (@QualifyInsertedInvoiceReceivableAsTypeId = 2)
	BEGIN
		IF NOT EXISTS (
		SELECT i.* FROM inserted i 
		INNER JOIN model.Adjustments adj ON adj.PatientId = i.PatientId 
		AND adj.InvoiceReceivableId IS NULL)
			BEGIN
				INSERT INTO model.Adjustments (FinancialSourceTypeId,Amount,PostedDateTime,AdjustmentTypeId
				  ,BillingServiceId,InvoiceReceivableId,PatientId,InsurerId,FinancialBatchId,PaymentMethodId
				  ,ClaimAdjustmentGroupCodeId,ClaimAdjustmentReasonCodeId,Comment,IncludeCommentOnStatement)
				  SELECT
					CASE
						WHEN i.PatientId IS NOT NULL
							THEN 2
						WHEN i.InsurerId IS NOT NULL
							THEN 1
						ELSE 4
					END AS FinancialSourceTypeId
					,-0.01 AS Amount
					,CONVERT(NVARCHAR,GETDATE(),112) AS PostedDateTime
					,1 AS AdjustmentTypeId
					,NULL AS BillingServiceId
					,NULL AS InvoiceReceivableId
					,i.PatientId 
					,NULL AS InsurerId
					,NULL AS FinancialBatchId
					,NULL AS PaymentMethodId
					,NULL AS ClaimAdjustmentReasonCodeId
					,NULL AS ClaimAdjustmentGroupCodeId
					,NULL AS PaymentRefId
					,0 AS IncludeCommentOnStatement
					FROM inserted i 

					DECLARE @InsertedCreditRowScopeIdentity INT
					IF (@@ROWCOUNT > 0) SET @InsertedCreditRowScopeIdentity = SCOPE_IDENTITY()
			END
		ELSE IF EXISTS (
			SELECT 
			i.* 
			FROM inserted i 
			INNER JOIN model.Adjustments adj ON adj.PatientId = i.PatientId 
			AND adj.InvoiceReceivableId IS NULL)
			BEGIN
				UPDATE adj
				SET adj.Amount = ABS(i.OpenBalance)
				FROM model.Adjustments adj
				INNER JOIN inserted i ON i.Disability = adj.Id
			END
	END
	ELSE
	BEGIN

	SET @InvoiceCreatedOnThisInsertTrigger = 1
	SET @PatientId = (SELECT TOP 1 PatientId FROM inserted)
	SET @ClinicalInvoiceProviderMax = 110000000
	SET @ProviderBillingOrganizationMax = 110000000
	SET @ProviderBillingOrganizationServiceLocationMax = 110000000
	SET @ServiceLocationMax = 110000000
	SET @NumberOfInsuranceRowsInserted = 0
	SET @MainLocationId = (
			SELECT TOP 1 PracticeId AS PracticeId
			FROM dbo.PracticeName
			WHERE PracticeType = 'P' AND LocationReference = ''
			)
	SET @ServiceLocationCodeId = (
			SELECT TOP 1 CASE 
					WHEN (ap.ResourceId2 = 0) OR (ap.ResourceId2 > 1000) AND (ap.Comments <> 'ASC CLAIM')
						THEN (
								SELECT TOP 1 pct.Id
								FROM PracticeName pn
								INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE' 
									AND SUBSTRING(pct.Code, 1, 2) = '11'
								)
					WHEN ap.ResourceId2 < 999
						THEN (
								SELECT TOP 1 pct.Id
								FROM inserted i
								INNER JOIN dbo.Appointments ap ON i.AppointmentId = ap.AppointmentId 
								INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId2
								INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE' 
									AND SUBSTRING(pct.Code, 1, 2) = re.PlaceOfService
								)
					WHEN ap.Comments = 'ASC CLAIM'
						THEN (
								SELECT TOP 1 pct.Id
								FROM inserted i
								INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
								INNER JOIN dbo.Appointments ap ON i.AppointmentId = ap.AppointmentId
								INNER JOIN dbo.Resources reASC ON reASC.PracticeId = ap.ResourceId2 - 1000 
									AND reASC.ResourceType = 'R' 
									AND reASC.ServiceCode = '02' 
									AND reASC.Billable = 'Y' 
									AND pic.FieldValue = 'T'
								LEFT JOIN dbo.PracticeCodeTable pct ON SUBSTRING(pct.Code, 1, 2) = reASC.PlaceOfService 
								AND pct.ReferenceType = 'PLACEOFSERVICE'
								)
					END
			FROM inserted i 
			INNER JOIN Appointments ap ON i.AppointmentId = ap.AppointmentId
			)

	IF OBJECT_ID(N'tempdb..#HoldOrdinals') IS NOT NULL
		DROP TABLE #HoldOrdinals

	CREATE TABLE #HoldOrdinals (PatientInsuranceId INT)

--step 1, does the invoice exsits for the PR you're trying to insert?
--no -- then create it, yes -- then set variable @InvoiceId = to the invoiceid
	IF EXISTS (
			SELECT Id
			FROM model.Invoices inv
			INNER JOIN inserted i ON inv.EncounterId = i.AppointmentId

			UNION ALL 
			
			SELECT inv.Id
			FROM inserted i 
			INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
				AND ap.Comments = 'ASC CLAIM'
			INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
				AND apEncounter.AppTypeId = ap.AppTypeId
				AND apEncounter.AppDate = ap.AppDate
				AND apEncounter.AppTime > 0 
				AND ap.AppTime = 0
				AND apEncounter.ScheduleStatus = ap.ScheduleStatus
				AND ap.ScheduleStatus = 'D'
				AND apEncounter.ResourceId1 = ap.ResourceId1
				AND apEncounter.ResourceId2 = ap.ResourceId2
			INNER JOIN model.Invoices inv on inv.EncounterId = apEncounter.AppointmentId AND inv.InvoiceTypeId = 2
			)
			BEGIN
				SET @InvoiceCreatedOnThisInsertTrigger = 0
				SET @InvoiceId = (
				SELECT Id
				FROM model.Invoices inv
				INNER JOIN inserted i ON inv.EncounterId = i.AppointmentId

				UNION ALL 
			
				SELECT inv.Id
				FROM inserted i 
				INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
					AND ap.Comments = 'ASC CLAIM'
				INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
					AND apEncounter.AppTypeId = ap.AppTypeId
					AND apEncounter.AppDate = ap.AppDate
					AND apEncounter.AppTime > 0 
					AND ap.AppTime = 0
					AND apEncounter.ScheduleStatus = ap.ScheduleStatus
					AND ap.ScheduleStatus = 'D'
					AND apEncounter.ResourceId1 = ap.ResourceId1
					AND apEncounter.ResourceId2 = ap.ResourceId2
				INNER JOIN model.Invoices inv on inv.EncounterId = apEncounter.AppointmentId AND inv.InvoiceTypeId = 2
				)
			END
	ELSE
	BEGIN
	--set this variable to 1, or True so that we know the invoice was inserted on this instance of the insert trigger.
		SET @InvoiceCreatedOnThisInsertTrigger = 1
		--First insert into invoices   
		DECLARE @BillToDr INT

		SET @BillToDr = (
				SELECT TOP 1 BillToDr
				FROM inserted
				)

		DECLARE @PrimaryInsurerId INT

		SET @PrimaryInsurerId = (
				SELECT TOP 1 InsurerId
				FROM inserted
				)

		DECLARE @IsAssignmentRefused BIT

		SET @IsAssignmentRefused = CONVERT(BIT, 0) --assume assignment is always accepted.

		IF EXISTS (
				SELECT d.DoctorId
				FROM model.DoctorInsurerAssignments d
				WHERE d.DoctorId = @BillToDr
					AND d.InsurerId = @PrimaryInsurerId
					AND d.IsAssignmentAccepted = 0 --indicates Assignment is not accepted, therefore @IsAssignmentRefused = 1
				)
			SET @IsAssignmentRefused = CONVERT(BIT, 1)

		INSERT INTO model.Invoices (
			DATETIME
			,AttributeToServiceLocationId
			,EncounterId
			,InvoiceTypeId
			,ClinicalInvoiceProviderId
			,HasPatientAssignedBenefits
			,IsReleaseOfInformationNotSigned
			,IsNoProviderSignatureOnFile
			,BillingProviderId
			,ReferringExternalProviderId
			,ServiceLocationCodeId
			,IsAssignmentRefused
			)
		SELECT CONVERT(DATETIME, i.InvoiceDate, 112) AS [DateTime]
			,CASE 
				WHEN i.BillingOffice = 0
					THEN 1
				WHEN i.BillingOffice BETWEEN 1
						AND 999
					THEN model.GetPairId(1, reAtribTo.ResourceId, @ServiceLocationMax)
				ELSE i.BillingOffice - 1000
				END AS AttributeToServiceLocationId
			,CASE 
				WHEN ap.Comments <> 'ASC CLAIM'
					THEN i.AppointmentId
				ELSE apEncounter.AppointmentId
				END AS EncounterId
			,CASE 
				WHEN re.ResourceType IN (
						'Q'
						,'Y'
						)
					THEN 3
				WHEN ap.Comments = 'ASC CLAIM'
					THEN 2
				ELSE 1
				END AS InvoiceTypeId
			,CASE 
				WHEN ap.Comments <> 'ASC CLAIM'
					THEN model.GetBigPairId(ap.ResourceId1, i.AppointmentId, @ClinicalInvoiceProviderMax)
				ELSE model.GetBigPairId(reASC.ResourceId, apEncounter.AppointmentId, @ClinicalInvoiceProviderMax)
				END AS ClinicalInvoiceProviderId
			,CASE p.PaymentOfBenefitsToProviderId
				WHEN 1
					THEN CONVERT(BIT, 1)
				ELSE CONVERT(BIT, 0)
				END AS HasPatientAssignedBenefits
			,CASE p.ReleaseOfInformationCodeId
				WHEN 1
					THEN CONVERT(BIT, 0)
				ELSE CONVERT(BIT, 1)
				END AS IsReleaseOfInformationNotSigned
			,CONVERT(BIT, 0) AS IsNoProviderSignatureOnFile
			,model.GetBigPairId(CASE --1st
					WHEN ap.ResourceId2 = 0
						THEN @MainLocationId
					WHEN ap.ResourceId2 > 999
						THEN (ap.ResourceId2 - 1000)
					ELSE model.GetPairId(1, ap.ResourceId2, @ServiceLocationMax)
					END, model.GetBigPairId(CASE 
						WHEN ap.Comments <> 'ASC CLAIM'
							THEN re.PracticeId
						WHEN ap.Comments = 'ASC CLAIM'
							THEN (ap.ResourceId2 - 1000)
						END, --2nd number
					CASE 
						WHEN ap.Comments <> 'ASC CLAIM'
							THEN i.BillToDr
						ELSE (
								SELECT TOP 1 ResourceId
								FROM Resources
								WHERE PracticeId = (ap.ResourceId2 - 1000)
									AND ResourceType = 'R'
									AND ServiceCode = '02'
									AND Billable = 'Y'
								)
						END, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax) AS BillingProviderId
			,CASE 
				WHEN i.ReferDr = ''
					THEN NULL
				ELSE i.ReferDr
				END AS ReferringExternalProviderId
			,@ServiceLocationCodeId AS ServiceLocationCodeId
			,@IsAssignmentRefused AS IsAssignmentRefused
		FROM inserted i
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
		INNER JOIN model.Patients p ON p.Id = i.PatientId
		LEFT JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
		LEFT JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
		LEFT JOIN dbo.Resources reAtribTo ON i.BillingOffice = reAtribTo.ResourceId
		LEFT JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
			AND apEncounter.AppTypeId = ap.AppTypeId
			AND apEncounter.AppDate = ap.AppDate
			AND apEncounter.AppTime > 0
			AND ap.AppTime = 0
			AND apEncounter.ScheduleStatus = ap.ScheduleStatus
			AND ap.ScheduleStatus = 'D'
			AND apEncounter.ResourceId1 = ap.ResourceId1
			AND apEncounter.ResourceId2 = ap.ResourceId2
		LEFT JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P'
			AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
			AND PracticeType = 'P'
			AND LocationReference <> ''
		LEFT JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
			AND reASC.ResourceType = 'R'
			AND reASC.ServiceCode = '02'
			AND reASC.Billable = 'Y'
			AND pic.FieldValue = 'T'

		--Set the invoice variable to the new row that was just inserted
		SET @InvoiceId = (
				SELECT IDENT_CURRENT('model.Invoices')
				)
	END
	
	IF (@InvoiceCreatedOnThisInsertTrigger = 0)
	BEGIN

		DECLARE @DetermineInvoiceApptInsType NVARCHAR(1)
		SET @DetermineInvoiceApptInsType = (
		SELECT 
		CASE v.ApptInsType
			WHEN 1
				THEN 'M'
			WHEN 2
				THEN 'V'
			WHEN 3
				THEN 'A'
			WHEN 4
				THEN 'W'
		END AS ApptInsType
		FROM (
			SELECT TOP 1
			CASE ap.ApptInsType
				WHEN 'M'
					THEN 1
				WHEN 'V'
					THEN 2
				WHEN 'A'
					THEN 3
				WHEN 'W'
					THEN 4
			END AS ApptInsType
			FROM model.InvoiceReceivables ir
			INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
			INNER JOIN dbo.Appointments ap ON ap.EncounterId = i.EncounterId
			WHERE ir.InvoiceId = @InvoiceId
			ORDER BY CASE ap.ApptInsType
				WHEN 'M'
					THEN 1
				WHEN 'V'
					THEN 2
				WHEN 'A'
					THEN 3
				WHEN 'W'
					THEN 4
			END ASC
			)v
		)
		
		DECLARE @DetermineInsertedPatientInsuranceInsType NVARCHAR(1)
		SET @DetermineInsertedPatientInsuranceInsType = (
		SELECT DISTINCT(
		CASE pi.InsuranceTypeId
			WHEN 1
				THEN 'M'
			WHEN 2
				THEN 'V'
			WHEN 3
				THEN 'A'
			WHEN 4
				THEN 'W'
			ELSE NULL
		END) AS InsuranceType
		FROM inserted i
		INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
			AND (pi.EndDateTime IS NULL OR pi.EndDateTime = '' OR pi.EndDateTime >= CONVERT(DATETIME,i.InvoiceDate,112))
			AND pi.IsDeleted = 0
		INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			AND ip.StartDateTime <= CONVERT(DATETIME,i.InvoiceDate,112)
			AND ip.InsurerId = i.InsurerId
			AND ip.PolicyHolderPatientId = i.InsuredId
		)
		
		IF (@DetermineInsertedPatientInsuranceInsType <> @DetermineInvoiceApptInsType)
		BEGIN
			UPDATE ap
			SET ap.ApptInsType = @DetermineInsertedPatientInsuranceInsType
			FROM dbo.Appointments ap
			WHERE ap.AppointmentId = (
				SELECT 
				ap.EncounterId
				FROM inserted i 
				INNER JOIN dbo.Appointments ap 
				ON ap.AppointmentId = i.AppointmentId)
		END
	END

		--Insert InvoiceSupplimentals table if neccessary
		IF EXISTS (
				SELECT *
				FROM inserted i
				WHERE FirstConsDate <> '' OR HspAdmDate <> '' OR Over90 <> '' OR Over90 <> '' OR RsnDate <> '' OR HspDisDate <> '' OR AccType <> ''
				)
				AND NOT EXISTS(SELECT * FROM model.InvoiceSupplementals WHERE Id = @InvoiceId)
		BEGIN
			INSERT INTO model.InvoiceSupplementals (
				Id
				,AccidentDateTime
				,AccidentStateOrProvinceId
				,AdmissionDateTime
				,ClaimDelayReasonId
				,DisabilityDateTime
				,DischargeDateTime
				,RelatedCause1Id
				,VisionPrescriptionDateTime
				)
			SELECT @invoiceId AS Id
				,CASE 
					WHEN FirstConsDate = ''
						THEN NULL
					ELSE CONVERT(DATETIME, FirstConsDate, 101)
					END AS AccidentDateTime
				,CASE st.id
					WHEN 0
						THEN NULL
					ELSE st.id
					END AS AccidentStateOrProvinceId
				,CASE 
					WHEN HspAdmDate = ''
						THEN NULL
					ELSE CONVERT(DATETIME, HspAdmDate, 101)
					END AS AdmissionDateTime
				,CASE Over90
					WHEN 1
						THEN 1
					WHEN 2
						THEN 2
					WHEN 3
						THEN 3
					WHEN 4
						THEN 4
					WHEN 5
						THEN 5
					WHEN 6
						THEN 6
					WHEN 7
						THEN 7
					WHEN 8
						THEN 8
					WHEN 9
						THEN 9
					WHEN 10
						THEN 10
					WHEN 11
						THEN 11
					WHEN 15
						THEN 12
					ELSE NULL
					END AS ClaimDelayReasonId
				,CASE 
					WHEN COALESCE(RsnDate, '') <> ''
						THEN CONVERT(DATETIME, RsnDate, 101)
					ELSE NULL
					END AS DisabilityDateTime
				,CASE 
					WHEN COALESCE(HspDisDate, '') <> ''
						THEN CONVERT(DATETIME, HspDisDate, 101)
					ELSE NULL
					END AS DischargeDateTime
				,CASE i.AccType
					WHEN 'A'
						THEN 1
					WHEN 'E'
						THEN 2
					WHEN 'O'
						THEN 3
					ELSE NULL
					END AS RelatedCause1Id
				,CONVERT(DATETIME, NULL, 101) AS VisionPrescriptionDateTime
			FROM inserted i
			LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = i.AccState
	END

	--now let's insert into the invoice receivables (the actual PR row(s))
	IF NOT EXISTS (
			SELECT *
			FROM model.InvoiceReceivables ir
			INNER JOIN model.Invoices iv ON ir.InvoiceId = @InvoiceId
			WHERE ir.PatientInsuranceId IS NULL 
			)
	BEGIN
		IF EXISTS (SELECT i.InsurerId FROM inserted i WHERE i.InsurerId = 0 AND i.ReceivableType = 'I')
		BEGIN
			INSERT INTO model.InvoiceReceivables (
			InvoiceId
			,PatientInsuranceId
			,PayerClaimControlNumber
			,PatientInsuranceAuthorizationId
			,PatientInsuranceReferralId
			,OpenForReview
			)
		SELECT @InvoiceId AS InvoiceId
			,NULL AS PatientInsuranceId
			,NULL AS PayerClaimControlNumber
			,NULL AS PatientInsuranceAuthorizationId				
			,NULL AS PatientInsuranceReferralId
			,CONVERT(BIT, 1) AS OpenForReview
		FROM inserted i
		INNER JOIN Appointments ap ON ap.AppointmentId = i.AppointmentId
		END

		ELSE BEGIN
		--Insert into InvoiceReceivables, the patient row
		INSERT INTO model.InvoiceReceivables (
			InvoiceId
			,PatientInsuranceId
			,PayerClaimControlNumber
			,PatientInsuranceAuthorizationId
			,PatientInsuranceReferralId
			,OpenForReview
			)
		SELECT @InvoiceId AS InvoiceId
			,NULL AS PatientInsuranceId
			,NULL AS PayerClaimControlNumber
			,NULL AS PatientInsuranceAuthorizationId				
			,NULL AS PatientInsuranceReferralId
			,CONVERT(BIT, 0) AS OpenForReview
		FROM inserted i
		INNER JOIN Appointments ap ON ap.AppointmentId = i.AppointmentId
		END
	END

	DECLARE @DoesExistsInvoiceReceivableForInsurer INT
	SELECT 
	@DoesExistsInvoiceReceivableForInsurer = ir.Id
	FROM model.InvoiceReceivables ir
	WHERE InvoiceId = @InvoiceId 
		AND PatientInsuranceId IN (
				SELECT
				pins.Id 
				FROM inserted i
				INNER JOIN model.PatientInsurances pins ON i.PatientId = pins.InsuredPatientId
				INNER JOIN model.InsurancePolicies ip ON ip.Id = pins.InsurancePolicyId
				WHERE ip.InsurerId = i.InsurerId
		)

	IF (@QualifyInsertedInvoiceReceivableAsTypeId = 3 AND @DoesExistsInvoiceReceivableForInsurer IS NULL)
	BEGIN
		IF (@InvoiceCreatedOnThisInsertTrigger = 1)
		--ok so this variable being one means we've created the invoice on THIS insert. (it didn't
		--already exist.)
		BEGIN
			INSERT INTO model.InvoiceReceivables (
				InvoiceId
				,PatientInsuranceId
				,PayerClaimControlNumber
				,PatientInsuranceAuthorizationId
				,PatientInsuranceReferralId
				,OpenForReview
				)
			OUTPUT INSERTED.PatientInsuranceId INTO #HoldOrdinals
			SELECT @InvoiceId AS InvoiceId	
					,pi.Id AS PatientInsuranceId
					,i.ExternalRefInfo AS PayerClaimControlNumber
					,CASE 
						WHEN (ap.PreCertId = '') OR (ap.PreCertId = 0) 
							THEN NULL
						ELSE ap.PreCertId 
					END AS PatientInsuranceAuthorizationId
					,CASE WHEN (ap.ReferralId = '') OR (ap.ReferralId = 0)
						THEN NULL
					ELSE ap.ReferralId
					END AS PatientInsuranceReferralId
					,CONVERT(BIT, 0) AS OpenForReview
				FROM inserted i
				LEFT JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
				LEFT JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
					AND (pi.EndDateTime IS NULL OR pi.EndDateTime = '' OR CONVERT(NVARCHAR,pi.EndDateTime,112) >= i.InvoiceDate)
					AND pi.IsDeleted <> 1
				LEFT JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id AND ip.InsurerId = i.InsurerId
					AND CONVERT(NVARCHAR,ip.StartDateTime,112) <= i.InvoiceDate
			SET @NumberOfInsuranceRowsInserted = (SELECT @@ROWCOUNT)
			
			UPDATE ir
			SET ir.OpenForReview = CONVERT(BIT,1)
			FROM model.InvoiceReceivables ir
			WHERE ir.InvoiceId = @InvoiceId
			AND ir.PatientInsuranceId = dbo.GetPrimaryInsuranceId(@InvoiceId)

			UPDATE ir
			SET ir.PatientInsuranceAuthorizationId = NULL
			,ir.PatientInsuranceReferralId = NULL
			FROM model.InvoiceReceivables ir
			WHERE ir.InvoiceId = @InvoiceId
			AND ir.PatientInsuranceId <> dbo.GetPrimaryInsuranceId(@InvoiceId)

		END
		ELSE IF (@InvoiceCreatedOnThisInsertTrigger = 0)
		BEGIN
		--so if the invoice already exists, then look through invoicereceivables for the patientinsurance
		--only insert into IR, these new insurances, or what has changed.
		--ex: patient only has primary, later adds a secondary. (only insert secondary)
			INSERT INTO model.InvoiceReceivables (
			InvoiceId			
			,PatientInsuranceId
			,PayerClaimControlNumber
			,PatientInsuranceAuthorizationId
			,PatientInsuranceReferralId
			,OpenForReview
			)
			OUTPUT INSERTED.PatientInsuranceId INTO #HoldOrdinals
			SELECT
			@InvoiceId AS InvoiceId
			,pi.Id
			,i.ExternalRefInfo AS PayerClaimControlNumber
			,CASE 
				WHEN (ap.PreCertId = '') OR (ap.PreCertId = 0) 
					THEN NULL
				ELSE ap.PreCertId 
			END AS PatientInsuranceAuthorizationId
			,CASE WHEN (ap.ReferralId = '') OR (ap.ReferralId = 0)
				THEN NULL
			ELSE ap.ReferralId
			END AS PatientInsuranceReferralId
			,CASE 
				WHEN i.InsurerDesignation = 'T'
					THEN CONVERT(BIT, 1)
				ELSE CONVERT(BIT, 0)
			END AS OpenForReview
			FROM inserted i 
			INNER JOIN dbo.Appointments ap on ap.AppointmentId = i.AppointmentId
			INNER JOIN model.Invoices iv ON iv.EncounterId = ap.EncounterId AND iv.Id = @InvoiceId
			INNER JOIN model.PatientInsurances pi on pi.InsuredPatientId = ap.PatientId
				AND (pi.EndDateTime IS NULL 
						OR pi.EndDateTime = '' 
						OR pi.EndDateTime >= iv.DateTime)
					AND pi.IsDeleted <> 1
			INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
				AND ip.InsurerId = i.InsurerId
			LEFT JOIN model.InvoiceReceivables ir on ir.InvoiceId = iv.id and pi.id = ir.PatientInsuranceId
			WHERE ir.id IS NULL

			SET @NumberOfInsuranceRowsInserted = (SELECT @@ROWCOUNT)
			--to handle when new insurances are added, and previously billed.
		END
	
		UPDATE ir
		SET LegacyInsurerDesignation = CASE WHEN z.Id IS NOT NULL THEN 'T' ELSE NULL END
		FROM model.InvoiceReceivables ir
		LEFT JOIN (
			SELECT 
			v.Id
			,v.InvoiceId
			FROM (
					SELECT
					ROW_NUMBER() OVER (PARTITION BY pi.InsuranceTypeId
						ORDER BY pi.InsuranceTypeId, pi.OrdinalId, PolicyHolderRelationshipTypeId) as rn
					,ir.InvoiceId
					,pi.Id
					FROM model.InvoiceReceivables ir
					INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
					INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
						AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= i.DateTime)
						AND pi.IsDeleted = 0
					INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
						AND ip.StartDateTime <= i.DateTime
					WHERE PatientInsuranceId IS NOT NULL
					AND ir.InvoiceId = @InvoiceId
			) v WHERE v.rn = 1
		)z ON z.InvoiceId = ir.InvoiceId
			AND z.Id = ir.PatientInsuranceId
		WHERE ir.InvoiceId = @InvoiceId
			AND ir.PatientInsuranceId IS NOT NULL
	END

	IF EXISTS(
	SELECT 
	Id 
	FROM model.BillingServiceTransactions 
	WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE InvoiceId = @InvoiceId AND PatientInsuranceId IS NOT NULL)
	) OR EXISTS (SELECT 
				* 
				FROM PracticeTransactionJournal 
				WHERE TransactionTypeId IN (SELECT Id FROM model.InvoiceReceivables WHERE InvoiceId = @InvoiceId AND PatientInsuranceId IS NOT NULL) 
					AND TransactionType ='R')
	BEGIN
		UPDATE ir
		SET ir.OpenForReview = CONVERT(BIT,0)
		FROM model.InvoiceReceivables ir
		WHERE ir.InvoiceId = @InvoiceId
		AND PatientInsuranceId IS NOT NULL
	END
	END
	IF OBJECT_ID('tempdb..#TempTableToHoldTheScopeIdentity') IS NOT NULL
	DROP TABLE #TempTableToHoldTheScopeIdentity

	CREATE TABLE #TempTableToHoldTheScopeIdentity (Id BIGINT IDENTITY(1,1) NOT NULL)
	SET IDENTITY_INSERT #TempTableToHoldTheScopeIdentity ON

	IF (@QualifyInsertedInvoiceReceivableAsTypeId = 1)
	BEGIN
		INSERT INTO #TempTableToHoldTheScopeIdentity (Id) 
		SELECT 
		ir.Id AS SCOPE_ID_COLUMN 
		FROM model.InvoiceReceivables ir
		WHERE ir.InvoiceId = @InvoiceId 
	END

	ELSE IF (@QualifyInsertedInvoiceReceivableAsTypeId = 2)
	BEGIN
		INSERT INTO #TempTableToHoldTheScopeIdentity (Id)
		SELECT (1 * 1000000000 + @InsertedCreditRowScopeIdentity) AS SCOPE_ID_COLUMN
	END
	ELSE IF (@QualifyInsertedInvoiceReceivableAsTypeId = 3)
	BEGIN
		IF (@InvoiceCreatedOnThisInsertTrigger = 1)
		BEGIN
			INSERT INTO #TempTableToHoldTheScopeIdentity (Id) 
			SELECT 
			ir.Id AS SCOPE_ID_COLUMN
			FROM #HoldOrdinals ho
			INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = @InvoiceId
				AND ir.PatientInsuranceId = ho.PatientInsuranceId
			WHERE ho.PatientInsuranceId = dbo.GetPrimaryInsuranceId(@InvoiceId)
				AND ir.PatientInsuranceId IS NOT NULL
		END
		ELSE IF (@InvoiceCreatedOnThisInsertTrigger = 0 AND @NumberOfInsuranceRowsInserted > 0)
		BEGIN
			DECLARE @InsurerIdVB INT
			SELECT @InsurerIdVB = i.InsurerId FROM inserted i
			
			INSERT INTO #TempTableToHoldTheScopeIdentity (Id) 
			SELECT 
			TOP 1 ir.Id
			FROM #HoldOrdinals ho
			INNER JOIN model.InvoiceReceivables ir ON ir.Id = ho.PatientInsuranceId
			INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
			INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			WHERE ip.InsurerId = @InsurerIdVB
				AND ir.InvoiceId = @InvoiceId
		END
		ELSE IF (@InvoiceCreatedOnThisInsertTrigger = 0 AND @NumberOfInsuranceRowsInserted = 0)
			INSERT INTO #TempTableToHoldTheScopeIdentity (Id) 
			SELECT TOP 1 ir.Id AS SCOPE_ID_COLUMN
			FROM model.InvoiceReceivables ir
			INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
			INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			WHERE ir.InvoiceId = @InvoiceId 
				AND ip.InsurerId = (SELECT i.InsurerId FROM inserted i)
	END
		
	IF NOT EXISTS(SELECT * FROM ViewIdentities WHERE Name = 'dbo.PatientReceivables')
		INSERT ViewIdentities(Name, Value) VALUES ('dbo.PatientReceivables', @@IDENTITY)
	ELSE
		UPDATE ViewIdentities SET Value = @@IDENTITY WHERE Name = 'dbo.PatientReceivables'

	SELECT @@IDENTITY AS SCOPE_ID_COLUMN

END