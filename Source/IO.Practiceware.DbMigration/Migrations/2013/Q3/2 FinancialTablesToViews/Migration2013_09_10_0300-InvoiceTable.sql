﻿--Invoice Table
--Drop Model.Invoices and model.InvoiceReceivables
EXEC dbo.DropObject 'model.Invoices'
GO

-- Creating table 'Invoices'
CREATE TABLE [model].[Invoices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DateTime] datetime  NOT NULL,
    [AttributeToServiceLocationId] int  NULL,
    [EncounterId] int  NOT NULL,
    [InvoiceTypeId] int  NOT NULL,
    [ClinicalInvoiceProviderId] bigint  NOT NULL,
    [HasPatientAssignedBenefits] bit  NOT NULL,
    [IsReleaseOfInformationNotSigned] bit  NOT NULL,
    [IsNoProviderSignatureOnFile] bit  NOT NULL,
    [BillingProviderId] bigint  NOT NULL,
    [ReferringExternalProviderId] int  NULL,
    [ServiceLocationCodeId] int  NOT NULL,
    [IsAssignmentRefused] bit  NOT NULL,
	[LegacyPatientBilledDateTime] datetime NULL,
	[LegacyPatientReceivablesPatientId] int NULL,
	[LegacyPatientReceivablesAppointmentId] int NULL,
	[LegacyPatientReceivablesInvoice] nvarchar(32),
	[ComputedInvoiceId] int NULL
);
GO
--Insert data into invoice table
INSERT INTO model.[Invoices] (DateTime, AttributeToServiceLocationId, EncounterId, InvoiceTypeId, ClinicalInvoiceProviderId, HasPatientAssignedBenefits, IsReleaseOfInformationNotSigned, IsNoProviderSignatureOnFile, BillingProviderId, ReferringExternalProviderId, ServiceLocationCodeId, IsAssignmentRefused, ComputedInvoiceId)
SELECT 
	DateTime, 
	AttributeToServiceLocationId, 
	EncounterId, 
	InvoiceTypeId, 
	ClinicalInvoiceProviderId, 
	HasPatientAssignedBenefits, 
	IsReleaseOfInformationNotSigned, 
	IsNoProviderSignatureOnFile, 
	BillingProviderId, 
	ReferringExternalProviderId, 
	ServiceLocationCodeId, 
	IsAssignmentRefused,
	Id
FROM dbo.InvoicesBackup
GO

--Update InvoiceReceivable Fk reference to invoiceId
UPDATE ir
SET InvoiceId = iv.Id
FROM model.InvoiceReceivables ir
INNER JOIN model.invoices iv ON ir.InvoiceId = iv.ComputedInvoiceId

UPDATE model.Invoices
SET LegacyPatientBilledDateTime = CONVERT(DATETIME,pr.PatientBillDate,112)
FROM model.Invoices i
INNER JOIN PatientReceivables pr ON pr.AppointmentId = i.EncounterId
WHERE dbo.IsDate112(pr.PatientBillDate) = 1

UPDATE inv
SET LegacyPatientReceivablesPatientId = ap.PatientId
FROM model.Invoices inv
INNER JOIN Appointments ap  ON inv.EncounterId = ap.EncounterId

UPDATE inv
SET LegacyPatientReceivablesInvoice = ap.InvoiceNumber
	,LegacyPatientReceivablesAppointmentId = ap.AppointmentId
FROM model.Invoices inv
INNER JOIN Appointments ap ON inv.EncounterId = ap.EncounterId
WHERE ap.AppointmentId = ap.EncounterId AND inv.InvoiceTypeId <> 2

UPDATE inv
SET LegacyPatientReceivablesInvoice = ap.InvoiceNumber
	,LegacyPatientReceivablesAppointmentId = ap.AppointmentId
FROM model.Invoices inv
INNER JOIN Appointments ap ON inv.EncounterId = ap.EncounterId
WHERE ap.AppointmentId <> ap.EncounterId AND inv.InvoiceTypeId = 2

SELECT * 
INTO dbo.DeletedInvoiceReceivables
FROM model.InvoiceReceivables
WHERE PatientInsuranceId NOT IN (SELECT Id FROM model.PatientInsurances) AND PatientInsuranceId IS NOT NULL

DELETE FROM model.InvoiceReceivables
WHERE PatientInsuranceId NOT IN (SELECT Id FROM model.PatientInsurances) AND PatientInsuranceId IS NOT NULL

-- Add constraints to tables
-- Creating primary key on [Id] in table 'InvoiceReceivables'
ALTER TABLE [model].[InvoiceReceivables]
ADD CONSTRAINT [PK_InvoiceReceivables]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Invoices'
ALTER TABLE [model].[Invoices]
ADD CONSTRAINT [PK_Invoices]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

ALTER TABLE [model].[InvoiceReceivables]
ADD CONSTRAINT [FK_InvoiceReceivablePatientInsurance]
    FOREIGN KEY ([PatientInsuranceId])
    REFERENCES [model].[PatientInsurances]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InvoiceReceivablePatientInsurance'
CREATE INDEX [IX_FK_InvoiceReceivablePatientInsurance]
ON [model].[InvoiceReceivables]
    ([PatientInsuranceId]);
GO

-- Creating foreign key on [InvoiceId] in table 'InvoiceReceivables'
ALTER TABLE [model].[InvoiceReceivables]
ADD CONSTRAINT [FK_InvoiceInvoiceReceivable]
    FOREIGN KEY ([InvoiceId])
    REFERENCES [model].[Invoices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InvoiceInvoiceReceivable'
CREATE INDEX [IX_FK_InvoiceInvoiceReceivable]
ON [model].[InvoiceReceivables]
    ([InvoiceId]);
GO