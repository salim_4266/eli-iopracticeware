﻿----To be run after FinancialBatches and InvoiceReceivables
----1.Create table
EXEC dbo.DropObject 'model.FinancialInformations'
GO

 ----Creating table 'FinancialInformations'
CREATE TABLE [model].[FinancialInformations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Amount] decimal(18,2)  NOT NULL,
    [PostedDateTime] datetime  NOT NULL,
    [FinancialInformationTypeId] int  NOT NULL,
    [BillingServiceId] int  NULL,
    [InvoiceReceivableId] bigint  NULL,
    [FinancialSourceTypeId] int  NOT NULL,
    [PatientId] int  NULL,
    [InsurerId] int  NULL,
    [FinancialBatchId] int  NULL,
    [ClaimAdjustmentGroupCodeId] int  NULL,
    [ClaimAdjustmentReasonCodeId] int  NULL,
	[Comment] nvarchar(max) NULL,
	[OldId] bigint NULL,
	IncludeCommentOnStatement bit NOT NULL,
	[LegacyInvoiceReceivableId] INT NULL
);
GO

----2. Insert data from backup table  
INSERT INTO model.FinancialInformations (Amount, PostedDateTime, FinancialInformationTypeId, BillingServiceId,  InvoiceReceivableId,  FinancialSourceTypeId, PatientId, InsurerId, FinancialBatchId, ClaimAdjustmentGroupCodeId, ClaimAdjustmentReasonCodeId, OldId, IncludeCommentOnStatement)
SELECT
    Amount,
    PostedDateTime,
    FinancialInformationTypeId,
    BillingServiceId,
    InvoiceReceivableId,
    FinancialSourceTypeId,
    PatientId,
    InsurerId,
    FinancialBatchId,
    ClaimAdjustmentGroupCodeId,
    ClaimAdjustmentReasonCodeId,
	Id,
	CASE WHEN PaymentCommentOn = 'T' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END AS IncludeCommentOnStatement
FROM dbo.FinancialInformationsBackup

IF NOT EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'Comments' and schemas.name = 'model') 
BEGIN
EXEC('
CREATE VIEW [model].[Comments]
	WITH SCHEMABINDING
	AS
	----There are many other notes not yet included in the below query, including Patient Financial, Patient Collection, Practice, Red (CRM), Surgery and Clinical notes.
	----We also need additional properties to the comments (or a restructuring) to include StartDateTime, EndDateTime, IsDeleted, IsIncludedOnClaim, IsIncludedOnStatement, IsIncludedOnRx, IsAlertOn and additional granularity such as NoteSystem, NoteCategory.
	----Adjustment comment
	SELECT (CONVERT(bigint, 110000000) * 1 + PaymentId) AS Id,
		PaymentId AS AdjustmentId,
		NULL AS ClinicalServiceId,
		NULL AS ContactId,
		NULL AS EncounterLensesPrescriptionId,
		NULL AS EncounterServiceId,
		NULL AS FinancialInformationId,
		NULL AS InsurancePolicyId,
		NULL AS InsurerId,
		NULL AS InvoiceId,
		NULL AS PatientInsuranceAuthorizationId,
		NULL AS PatientInsuranceReferralId,
		NULL AS PatientReferralSourceId,
		PaymentRefId AS Value,
		CONVERT(bit, 0) AS IsArchived
	FROM dbo.PatientReceivablePayments prp
	INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
	LEFT JOIN dbo.PatientDemographicsTable pd ON pd.PatientId = prp.PayerId AND prp.PayerId <> 0 AND prp.PayerType = ''P''
	LEFT JOIN dbo.PracticeInsurers pins ON pins.InsurerId = prp.PayerId AND prp.PayerType = ''I'' 
	LEFT JOIN dbo.PatientReceivableServices prs ON prs.ItemId = prp.PaymentServiceItem
	INNER JOIN dbo.PracticeCodeTable pct ON ReferenceType = ''PAYMENTTYPE'' AND prp.PaymentType = SUBSTRING(Code, LEN(Code), 1)
	LEFT JOIN dbo.PracticeCodeTable pctPaymentMethod ON pctPaymentMethod.ReferenceType = ''PAYABLETYPE'' and prp.PaymentRefType = SUBSTRING(pctPaymentMethod.Code, LEN(pctPaymentMethod.Code), 1)
	WHERE PaymentFinancialType <> ''''
		AND PaymentRefId <> ''''
		AND PaymentRefId IS NOT NULL
		AND PaymentRefId <> ''0''
		AND prp.PaymentFinancialType <> ''''
	
	-- UNION ALL
	
	------ClinicalService comment (no good)
	--SELECT (CONVERT(bigint, 110000000) * 4 + NoteId) AS Id,
	--	NULL AS AdjustmentId,
	--	MIN(prs.ItemId) AS ClinicalServiceId,
	--	NULL AS ContactId,
	--	NULL AS EncounterLensesPrescriptionId,
	--	NULL AS EncounterServiceId,
	--	NULL AS FinancialInformationId,
	--	NULL AS InsurancePolicyId,
	--	NULL AS InsurerId,
	--	NULL AS InvoiceId,
	--	NULL AS PatientId,
	--	NULL AS PatientInsuranceAuthorizationId,
	--	NULL AS PatientInsuranceReferralId,
	--	NULL AS PatientReferralSourceId,
	----	note1 + note2 + note3 + note4 AS Value,
	--	CONVERT(bit, 0) AS IsRetired
	--FROM dbo.PatientNotes pn
	--INNER JOIN dbo.PatientReceivables pr ON pr.AppointmentId = pn.AppointmentId
	--INNER JOIN dbo.PatientReceivableServices prs ON prs.invoice = pr.invoice
	--WHERE NoteType = ''B''
	--	AND NoteSystem LIKE ''R%C%''
	--	AND SUBSTRING(NoteSystem, (dbo.GetMax((CHARINDEX(''C'', NoteSystem) + 1),0)), LEN(NoteSystem)) = prs.Service
	--	AND prs.[Status] = ''A''	
	--	AND CASE NoteEye
	--		WHEN ''OS''
	--			THEN ''LT''
	--		WHEN ''OD''
	--			THEN ''RT''
	--		WHEN ''OU''
	--			THEN ''50''
	--		ELSE NoteEye
	--		END = SUBSTRING(prs.modifier, 1, 2)
	--GROUP BY NoteId,
	--	prs.[Status],
	--	NoteEye,
	--	Note1,
	--	Note2,
	--	Note3,
	--	Note4
	
	
	----Emergency contacts and External providers don''t have notes currently in IO
	
	UNION ALL
	
	----EncounterLensesPrescription - spectacles comment
	SELECT (CONVERT(bigint, 110000000) * 7 + ClinicalId) AS Id,
		NULL AS AdjustmentId,
		NULL AS ClinicalServiceId,
		NULL AS ContactId,
		ClinicalId AS EncounterLensesPrescriptionId,
		NULL AS EncounterServiceId,
		NULL AS FinancialInformationId,
		NULL AS InsurancePolicyId,
		NULL AS InsurerId,
		NULL AS InvoiceId,
		NULL AS PatientInsuranceAuthorizationId,
		NULL AS PatientInsuranceReferralId,
		NULL AS PatientReferralSourceId,
		SUBSTRING(ImageDescriptor, 5,(dbo.GetMax((CHARINDEX(''OS:'', imagedescriptor) - 5),0))) AS Value,
		CONVERT(bit, 0) AS IsArchived
	FROM dbo.PatientClinical pc
	WHERE ClinicalType = ''A''
		AND SUBSTRING(FindingDetail, 1, 18) = ''DISPENSE SPECTACLE''
		AND SUBSTRING(ImageDescriptor, 1, 1) <> ''!''
		AND ImageDescriptor NOT IN (
			'''',
			''^OD: OS:'',
			''^''
			)
		AND [Status] = ''A''
		AND pc.AppointmentId <> 0
	
	UNION ALL
	
	----EncounterLensesPrescription - contact lens OD comment
	SELECT (CONVERT(bigint, 110000000) * 8 + ClinicalId) AS Id,
		NULL AS AdjustmentId,
		NULL AS ClinicalServiceId,
		NULL AS ContactId,
		ClinicalId AS EncounterLensesPrescriptionId,
		NULL AS EncounterServiceId,
		NULL AS FinancialInformationId,
		NULL AS InsurancePolicyId,
		NULL AS InsurerId,
		NULL AS InvoiceId,
		NULL AS PatientInsuranceAuthorizationId,
		NULL AS PatientInsuranceReferralId,
		NULL AS PatientReferralSourceId,
		SUBSTRING(ImageDescriptor, 5, (dbo.GetMax((CHARINDEX(''OS:'', ImageDescriptor) - 5),0))) AS Value,
		CONVERT(bit, 0) AS IsArchived
	FROM dbo.PatientClinical pc
	WHERE ClinicalType = ''A''
		AND SUBSTRING(FindingDetail, 1, 14) = ''DISPENSE CL RX''
		AND SUBSTRING(ImageDescriptor, 1, 1) <> ''!''
		AND ImageDescriptor NOT IN (
			'''',
			''^''
			)
		AND LEN(imagedescriptor) > 4
		AND SUBSTRING(ImageDescriptor, 1, 8) <> ''^OD: OS:''
		AND SUBSTRING(ImageDescriptor, 1, 7) <> ''^OD:OS:''
		AND SUBSTRING(ImageDescriptor, 1, 4) IN (''^OD:'', ''*OD:'')
		AND ImageDescriptor like ''%OS:%''
		AND [Status] = ''A''
		AND pc.AppointmentId <> 0
		
	UNION ALL
	
	----EncounterLensesPrescription - contact lens where no eye specified
	SELECT (CONVERT(bigint, 110000000) * 8 + ClinicalId) AS Id,
		NULL AS AdjustmentId,
		NULL AS ClinicalServiceId,
		NULL AS ContactId,
		ClinicalId AS EncounterLensesPrescriptionId,
		NULL AS EncounterServiceId,
		NULL AS FinancialInformationId,
		NULL AS InsurancePolicyId,
		NULL AS InsurerId,
		NULL AS InvoiceId,
		NULL AS PatientInsuranceAuthorizationId,
		NULL AS PatientInsuranceReferralId,
		NULL AS PatientReferralSourceId,
		ImageDescriptor AS Value,
		CONVERT(bit, 0) AS IsArchived
	FROM dbo.PatientClinical pc
	WHERE ClinicalType = ''A''
		AND SUBSTRING(FindingDetail, 1, 14) = ''DISPENSE CL RX''
		AND SUBSTRING(ImageDescriptor, 1, 1) <> ''!''
		AND ImageDescriptor NOT IN (
			'''',
			''^''
			)
		AND LEN(imagedescriptor) > 4
		AND SUBSTRING(ImageDescriptor, 1, 8) <> ''^OD: OS:''
		AND SUBSTRING(ImageDescriptor, 1, 7) <> ''^OD:OS:''
		AND SUBSTRING(ImageDescriptor, 1, 4) NOT IN (''^OD:'', ''*OD:'')
		AND [Status] = ''A''
		AND pc.AppointmentId <> 0
		
	UNION ALL
	----EncounterLensesPrescription - contact lens OS comment
	SELECT (CONVERT(bigint, 110000000) * 9 + ClinicalId) AS Id,
		NULL AS AdjustmentId,
		NULL AS ClinicalServiceId,
		NULL AS ContactId,
		ClinicalId AS EncounterLensesPrescriptionId,
		NULL AS EncounterServiceId,
		NULL AS FinancialInformationId,
		NULL AS InsurancePolicyId,
		NULL AS InsurerId,
		NULL AS InvoiceId,
		NULL AS PatientInsuranceAuthorizationId,
		NULL AS PatientInsuranceReferralId,
		NULL AS PatientReferralSourceId,
		SUBSTRING(ImageDescriptor, dbo.GetMax((dbo.GetMax((CHARINDEX(''OS:'', ImageDescriptor) + 3),0)), LEN(imagedescriptor) - (dbo.GetMax((CHARINDEX(''OS:'', imagedescriptor) + 3),0))),0) AS Value,
		CONVERT(bit, 0) AS IsArchived
	FROM dbo.PatientClinical pc
	WHERE ClinicalType = ''A''
		AND SUBSTRING(FindingDetail, 1, 14) = ''DISPENSE CL RX''
		AND SUBSTRING(ImageDescriptor, 1, 1) <> ''!''
		AND ImageDescriptor NOT IN (
			'''',
			''^OD: OS:'',
			''^''
			)
		AND LEN(ImageDescriptor) > 4
		AND ImageDescriptor like ''%OS:%''
		AND [Status] = ''A''	
		AND pc.AppointmentId <> 0
		
	UNION ALL
	
	----EncounterService comment
	SELECT (CONVERT(bigint, 110000000) * 10 + CodeId) AS Id,
		NULL AS AdjustmentId,
		NULL AS ClinicalServiceId,
		NULL AS ContactId,
		NULL AS EncounterLensesPrescriptionId,
		CodeId AS EncounterServiceId,
		NULL AS FinancialInformationId,
		NULL AS InsurancePolicyId,
		NULL AS InsurerId,
		NULL AS InvoiceId,
		NULL AS PatientInsuranceAuthorizationId,
		NULL AS PatientInsuranceReferralId,
		NULL AS PatientReferralSourceId,
		ClaimNote AS Value,
		CONVERT(bit, 0) AS IsArchived
	FROM dbo.PracticeServices
	WHERE ClaimNote <> ''''
		AND ClaimNote IS NOT NULL
	
	UNION ALL
	
	----Financial Information comment
	SELECT (CONVERT(bigint, 110000000) * 11 + prp.PaymentId) AS Id,
		NULL AS AdjustmentId,
		NULL AS ClinicalServiceId,
		NULL AS ContactId,
		NULL AS EncounterLensesPrescriptionId,
		NULL AS EncounterServiceId,
		prp.PaymentId AS FinancialInformationId,
		NULL AS InsurancePolicyId,
		NULL AS InsurerId,
		NULL AS InvoiceId,
		NULL AS PatientInsuranceAuthorizationId,
		NULL AS PatientInsuranceReferralId,
		NULL AS PatientReferralSourceId,
		prp.PaymentRefId AS Value,
		CONVERT(bit, 0) AS IsArchived
		FROM dbo.PatientReceivablePayments prp
		INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
		INNER JOIN model.InsurancePolicies ip ON ip.PolicyHolderPatientId = pr.InsuredId
			AND CONVERT(DATETIME,pr.InvoiceDate) >= ip.StartDateTime 
		INNER JOIN model.PatientInsurances pi ON pi.InsurancePolicyId = ip.Id
			AND IsDeleted = 0
			AND (CONVERT(DATETIME,pr.InvoiceDate) <= pi.EndDateTime OR pi.EndDateTime IS NULL)
		INNER JOIN dbo.PracticeCodeTable pct ON ReferenceType = ''PAYMENTTYPE''
			AND prp.PaymentType = SUBSTRING(Code, LEN(code), 1)
			AND (pct.AlternateCode = '''' OR pct.AlternateCode IS NULL)
		LEFT JOIN dbo.PatientReceivablePayments prpBatch ON prpBatch.PaymentId = prp.PaymentBatchCheckId
			AND prpBatch.PaymentType = ''=''
		INNER JOIN dbo.PatientReceivableServices prs on prs.ItemId = prp.PaymentServiceItem
			AND prs.[Status] = ''A''
	WHERE prp.PaymentFinancialType = ''''
		AND prp.PaymentRefId <> ''''
		AND prp.PaymentRefId IS NOT NULL
		AND prp.PaymentRefId <> ''0''
	
	UNION ALL
	
	----Insurer comment
	SELECT (CONVERT(bigint, 110000000) * 12 + InsurerId) AS Id,
		NULL AS AdjustmentId,
		NULL AS ClinicalServiceId,
		NULL AS ContactId,
		NULL AS EncounterLensesPrescriptionId,
		NULL AS EncounterServiceId,
		NULL AS FinancialInformationId,
		NULL AS InsurancePolicyId,
		InsurerId AS InsurerId,
		NULL AS InvoiceId,
		NULL AS PatientInsuranceAuthorizationId,
		NULL AS PatientInsuranceReferralId,
		NULL AS PatientReferralSourceId,
		InsurerComment AS Value,
		CONVERT(bit, 0) AS IsArchived
	FROM dbo.PracticeInsurers
	WHERE InsurerComment <> ''''
		AND InsurerComment IS NOT NULL
	
	UNION ALL
	
	----Invoice comment
	SELECT (CONVERT(bigint, 110000000) * 13 + v.NoteId) AS Id,
		NULL AS AdjustmentId,
		NULL AS ClinicalServiceId,
		NULL AS ContactId,
		NULL AS EncounterLensesPrescriptionId,
		NULL AS EncounterServiceId,
		NULL AS FinancialInformationId,
		NULL AS InsurancePolicyId,
		NULL AS InsurerId,
		v.InvoiceId AS InvoiceId,
		NULL AS PatientInsuranceAuthorizationId,
		NULL AS PatientInsuranceReferralId,
		NULL AS PatientReferralSourceId,
		v.Value,
		CONVERT(bit, 0) AS IsArchived
	FROM (
			SELECT 
			pr.AppointmentId AS InvoiceId,
			Note1 + Note2 + Note3 + Note4 AS Value,
			NoteId AS NoteId,
			COUNT_BIG(*) AS Count		
	FROM dbo.PatientNotes pn
	INNER JOIN dbo.PatientReceivables pr ON pn.AppointmentId = pr.AppointmentId
	WHERE NoteType = ''B''
		AND NoteSystem NOT LIKE ''%C%''
		AND pn.AppointmentId > 0
		GROUP BY pr.AppointmentId,
			Note1 + Note2 + Note3 + Note4,
			NoteId
		) AS v
	
	UNION ALL
	
	----PatientInsuranceAuthorization comment
	SELECT (CONVERT(bigint, 110000000) * 14 + MAX(ppc.PreCertId)) AS Id,
		NULL AS AdjustmentId,
		NULL AS ClinicalServiceId,
		NULL AS ContactId,
		NULL AS EncounterLensesPrescriptionId,
		NULL AS EncounterServiceId,
		NULL AS FinancialInformationId,
		NULL AS InsurancePolicyId,
		NULL AS InsurerId,
		NULL AS InvoiceId,
		MAX(ppc.PreCertId) AS PatientInsuranceAuthorizationId,
		NULL AS PatientInsuranceReferralId,
		NULL AS PatientReferralSourceId,
		PreCertComment AS Value,
		CONVERT(bit, 0) AS IsArchived
	FROM dbo.PatientPreCerts ppc
	INNER JOIN dbo.Appointments ap ON ap.appointmentid = ppc.PreCertApptId
		AND ap.ScheduleStatus IN (''D'', ''P'', ''R'', ''A'')
	INNER JOIN dbo.PatientDemographicsTable pd ON pd.PatientId = ppc.PatientId
	INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = pd.PatientId
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		AND ppc.PreCertInsId = ip.InsurerId
	INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = ppc.PreCertInsId
	   AND (
	       pi.InsuredPatientId = pd.PolicyPatientId
	       OR (pi.InsuredPatientId = pd.SecondPolicyPatientId and pri.AllowDependents = 1)
	       )
	   AND CONVERT(NVARCHAR(10),ip.StartDateTime,112) <= PreCertDate
	   AND (
	       CONVERT(NVARCHAR(10),pi.EndDateTime,112) >= PreCertDate
	       OR CONVERT(NVARCHAR(10),pi.EndDateTime,112) = ''''
	       )
	WHERE ppc.PreCertComment <> ''''
		AND ppc.PreCertComment IS NOT NULL
	GROUP BY PreCert,
	   PreCertDate,
	   PreCertStatus,
	   ScheduleStatus,
	   ap.AppointmentId,
	   PreCertComment
	
	UNION ALL
	
	----PatientInsuranceReferral Comment
	SELECT (CONVERT(bigint, 110000000) * 15 + ReferralId) AS Id,
		NULL AS AdjustmentId,
		NULL AS ClinicalServiceId,
		NULL AS ContactId,
		NULL AS EncounterLensesPrescriptionId,
		NULL AS EncounterServiceId,
		NULL AS FinancialInformationId,
		NULL AS InsurancePolicyId,
		NULL AS InsurerId,
		NULL AS InvoiceId,
		NULL AS PatientInsuranceAuthorizationId,
		ReferralId AS PatientInsuranceReferralId,
		NULL AS PatientReferralSourceId,
		Reason AS Value,
		CONVERT(bit, 0) AS IsArchived
	FROM dbo.PatientReferral pRef
	INNER JOIN dbo.PracticeVendors pv ON pv.VendorId = pRef.ReferredFromId
		AND VendorLastName <> '''' AND VendorLastName IS NOT NULL
	INNER JOIN model.PatientInsurances pi on pi.InsuredPatientId = pRef.PatientId
		AND IsDeleted = 0
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		AND ip.InsurerId = pRef.ReferredInsurer
		AND (CONVERT(DATETIME,pRef.ReferralDate) >= ip.StartDateTime 
		AND (CONVERT(DATETIME,pRef.ReferralDate) <= pi.EndDateTime OR pi.EndDateTime IS NULL))
	WHERE Reason <> ''''
		AND Reason IS NOT NULL
		AND pRef.ReferredTo <> 0
		AND (pi.InsuredPatientId = pRef.PatientId)
	
	UNION ALL
	
	----PatientReferralSource Comment
	SELECT (CONVERT(bigint, 110000000) * 16 + pd.PatientId) AS Id,
		NULL AS AdjustmentId,
		NULL AS ClinicalServiceId,
		NULL AS ContactId,
		NULL AS EncounterLensesPrescriptionId,
		NULL AS EncounterServiceId,
		NULL AS FinancialInformationId,
		NULL AS InsurancePolicyId,
		NULL AS InsurerId,
		NULL AS InvoiceId,
		NULL AS PatientInsuranceAuthorizationId,
		NULL AS PatientInsuranceReferralId,
		pd.PatientId AS PatientReferralSourceId,
		pd.ProfileComment2 AS Value,
		CONVERT(bit, 0) AS IsArchived
	FROM dbo.PatientDemographicsTable pd
	INNER JOIN dbo.PracticeCodeTable pct ON pct.Code = pd.ReferralCatagory
		AND ReferenceType = ''REFERBY''
	WHERE ProfileComment2 <> ''''
		AND ProfileComment2 IS NOT NULL
')
END
GO

IF OBJECT_ID('tempdb..#Comments') IS NOT NULL 
	DROP TABLE #Comments
GO

SELECT
FinancialInformationId
,Value
INTO #Comments
FROM model.Comments
WHERE FinancialInformationId IS NOT NULL

UPDATE fi
SET fi.Comment = com.Value
FROM model.FinancialInformations fi
JOIN #Comments com ON com.FinancialInformationId = fi.OldId

----3.Update Fk references InvoiceReceivableId

UPDATE fi 
SET fi.InvoiceReceivableId = ir.Id
FROM model.FinancialInformations fi
INNER JOIN model.InvoiceReceivables ir ON fi.InvoiceReceivableId = ir.ComputedInvoiceReceivableId

UPDATE fi
SET fi.FinancialBatchId = fb.Id
FROM model.FinancialInformations fi
INNER JOIN model.FinancialBatches fb ON fi.FinancialBatchId = fb.ComputedFinancialBatchId
GO

SELECT
*
INTO #DeletedFinancialInformations
FROM model.FinancialInformations
WHERE InvoiceReceivableId NOT IN (SELECT Id FROM model.InvoiceReceivables)

UPDATE fi
SET fi.InvoiceReceivableId = ir.Id
FROM model.FinancialInformations fi
INNER JOIN dbo.PatientReceivableServices prs ON prs.ItemId = fi.BillingServiceId
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = prs.AppointmentId
INNER JOIN model.Invoices i ON i.EncounterId = ap.AppointmentId
INNER JOIN #DeletedFinancialInformations df ON df.BillingServiceId = prs.ItemId
INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id

DELETE FROM model.FinancialInformations
WHERE FinancialBatchId NOT IN (SELECT Id FROM model.FinancialBatches)

IF EXISTS(SELECT TOP 1 Id FROM model.FinancialInformations WHERE InvoiceReceivableId NOT IN (SELECT Id FROM model.InvoiceReceivables))
BEGIN
	RAISERROR('There are FinancialInformations.InvoiceReceivableId that are not in (SELECT Id FROM model.InvoiceReceivables)... Invalid foreign keys.',16,2) WITH SETERROR
END

ALTER TABLE [model].[FinancialInformations]
ALTER COLUMN InvoiceReceivableId INT NULL

UPDATE fi
SET [LegacyInvoiceReceivableId] = dbo.GetLegacyInvoiceReceivableId(fi.InvoiceReceivableId)
FROM model.FinancialInformations fi

 ----Creating primary key on [Id] in table 'FinancialInformations'
ALTER TABLE [model].[FinancialInformations]
ADD CONSTRAINT [PK_FinancialInformations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [InvoiceReceivableId] in table 'FinancialInformations'
ALTER TABLE [model].[FinancialInformations]
ADD CONSTRAINT [FK_InvoiceReceivableFinancialInformation]
    FOREIGN KEY ([InvoiceReceivableId])
    REFERENCES [model].[InvoiceReceivables]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InvoiceReceivableFinancialInformation'
CREATE INDEX [IX_FK_InvoiceReceivableFinancialInformation]
ON [model].[FinancialInformations]
    ([InvoiceReceivableId]);
GO

-- Creating foreign key on [FinancialBatchId] in table 'FinancialInformations'
ALTER TABLE [model].[FinancialInformations]
ADD CONSTRAINT [FK_FinancialBatchFinancialInformation]
    FOREIGN KEY ([FinancialBatchId])
    REFERENCES [model].[FinancialBatches]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_FinancialBatchFinancialInformation'
CREATE INDEX [IX_FK_FinancialBatchFinancialInformation]
ON [model].[FinancialInformations]
    ([FinancialBatchId]);
GO