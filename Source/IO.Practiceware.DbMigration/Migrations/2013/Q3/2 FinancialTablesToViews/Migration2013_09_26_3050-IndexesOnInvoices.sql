﻿CREATE INDEX IX_Invoices_LegacyPatientReceivablesPatientId ON model.Invoices (LegacyPatientReceivablesPatientId)
GO
CREATE INDEX IX_Invoices_LegacyPatientReceivablesAppointmentId ON model.Invoices (LegacyPatientReceivablesAppointmentId)
GO
CREATE INDEX IX_Invoices_LegacyPatientReceivablesInvoice ON model.Invoices (LegacyPatientReceivablesInvoice)
GO