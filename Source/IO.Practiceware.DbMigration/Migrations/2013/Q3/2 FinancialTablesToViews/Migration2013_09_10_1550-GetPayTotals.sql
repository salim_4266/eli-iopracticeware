﻿IF OBJECT_ID ( 'dbo.GetPayTotals', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetPayTotals;
GO

CREATE PROCEDURE dbo.GetPayTotals
	@Invoice NVARCHAR(100)
AS
SET NOCOUNT ON;

DECLARE @AppointmentId INT
SET @AppointmentId = SUBSTRING(@Invoice, (CHARINDEX('-', @Invoice) + 1), LEN(@Invoice))

IF OBJECT_ID('tempdb..#InvoiceReceivableIds') IS NOT NULL
BEGIN
	DROP TABLE #InvoiceReceivableIds
END

SELECT Id INTO #InvoiceReceivableIds FROM dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId)

IF (@@ROWCOUNT > 0)
BEGIN
SELECT
SUM(Amount)
FROM model.Adjustments adj
INNER JOIN #InvoiceReceivableIds irs ON irs.Id = adj.InvoiceReceivableId
INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
WHERE AdjustmentTypeId = 1
GROUP BY ir.InvoiceId

END
GO