﻿-- drop the PatientReceivables table 
EXEC DropObject 'PatientReceivables'
GO

IF EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'AdjustmentTypes' and schemas.name = 'model') 
BEGIN
	EXEC dbo.DropObject 'model.AdjustmentTypes'
END
GO

EXEC( 'CREATE VIEW [model].[AdjustmentTypes]
	WITH SCHEMABINDING
	AS
	SELECT v.Id AS Id,
		v.FinancialTypeGroupId AS FinancialTypeGroupId,
		v.IsCash AS IsCash,
		v.IsDebit AS IsDebit,
		v.IsPrintOnStatement AS IsPrintOnStatement,
		v.Name AS Name,
		v.ShortName AS ShortName,
		v.IsArchived AS IsArchived
	FROM (SELECT CASE
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''P''
				THEN 1
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''X''
				THEN 2
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''8''
				THEN 3
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''R''
				THEN 4
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''U''
				THEN 5
			ELSE pct.Id + 1000
			END AS Id,
		NULL AS FinancialTypeGroupId,
			CASE 
				WHEN SUBSTRING(Code, LEN(code), 1) IN (
						''U'',
						''R'',
						''P''
						)
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsCash,
			CASE pct.AlternateCode
				WHEN ''D''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsDebit,
			CONVERT(bit, 1) AS IsPrintOnStatement,
			SUBSTRING(Code, 1, (dbo.GetMax((charindex(''-'', code) - 2),0))) AS [Name],
			LetterTranslation AS ShortName,
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pct
		WHERE pct.ReferenceType = ''PAYMENTTYPE''
			AND	pct.AlternateCode IN (''C'', ''D'')
		GROUP BY Id,
			Code,
			pct.AlternateCode,
			LetterTranslation
		) AS v
')

IF EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'Providers' and schemas.name = 'model') 
BEGIN
	EXEC dbo.DropObject 'model.Providers'
END
GO
EXEC( 'CREATE VIEW [model].[Providers]
	WITH SCHEMABINDING
	AS
	---human and facility providers where ServiceLocation is from PracticeName table, including override 
	SELECT 
		CASE 
			WHEN re.ResourceType = ''R'' ---internal facility
				THEN (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
			WHEN pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
				THEN (CONVERT(bigint, 110000000) * (110000000 * 2 + pnServLoc.PracticeId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId))
			ELSE (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
			END AS Id,
		CASE
			WHEN pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
				THEN (110000000 * 1 + re.ResourceId) 
			ELSE pnBillOrg.PracticeId
			END AS BillingOrganizationId,
		CASE re.Billable
			WHEN ''Y''
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
			END AS IsBillable,
		CASE re.GoDirect
			WHEN ''Y''
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
			END AS IsEMR,
		CASE 
			WHEN re.ResourceType = ''R''
				THEN pnBillOrg.PracticeId
			ELSE pnServLoc.PracticeId 
			END AS ServiceLocationId,
		CASE
			WHEN re.ResourceType = ''R''
				THEN NULL
			ELSE re.ResourceId 
			END AS UserId
	FROM dbo.Resources re
	INNER JOIN dbo.PracticeName AS pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
		AND pnBillOrg.PracticeType = ''P''
	INNER JOIN dbo.PracticeName AS pnServLoc ON pnServLoc.PracticeType = ''P''
	INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
	LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
			AND pa.ResourceType = ''R''
	WHERE (re.ResourceType in (''D'', ''Q'', ''Z'',''Y'')
		OR (re.ResourceType = ''R''
			AND re.ServiceCode = ''02''
			AND re.Billable = ''Y''
			AND pic.FieldValue = ''T''
			AND pnBillOrg.LocationReference <> '''')
			)
	GROUP BY
		CASE 
			WHEN re.ResourceType = ''R'' ---internal facility
				THEN (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
			WHEN pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
				THEN (CONVERT(bigint, 110000000) * (110000000 * 2 + pnServLoc.PracticeId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId))
			ELSE (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
			END, 
		CASE
			WHEN pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
				THEN (110000000 * 1 + re.ResourceId) 
			ELSE pnBillOrg.PracticeId
			END,
		CASE re.Billable
			WHEN ''Y''
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
			END ,
		CASE re.GoDirect
			WHEN ''Y''
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
			END ,
		CASE 
			WHEN re.ResourceType = ''R''
				THEN pnBillOrg.PracticeId
			ELSE pnServLoc.PracticeId 
			END ,
		CASE
			WHEN re.ResourceType = ''R''
				THEN NULL
			ELSE re.ResourceId 
			END
	
	UNION ALL
	
	----Human Providers where ServiceLocation is from Resources table including override
	SELECT
		CASE 
			WHEN pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
				THEN (CONVERT(bigint, 110000000) * (110000000 * 3 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId))
			ELSE (CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
			END AS Id,
		CASE 
			WHEN  pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
				THEN (110000000 * 1 + re.ResourceId)
			ELSE pnBillOrg.PracticeId 
			END AS BillingOrganizationId,
		CASE re.Billable
			WHEN ''Y''
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
			END AS IsBillable,
		CASE re.GoDirect
			WHEN ''Y''
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
			END AS IsEMR,
		(110000000 * 1 + reServLoc.ResourceId) AS ServiceLocationId,
		re.ResourceId AS UserId
	FROM dbo.Resources re
	INNER JOIN dbo.PracticeName AS pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		AND pnBillOrg.PracticeType = ''P''
	INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = ''R''
		AND reServLoc.ServiceCode = ''02''
	LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
			AND pa.ResourceType = ''R''
	WHERE re.ResourceType in (''D'', ''Q'', ''Z'',''Y'')
	GROUP BY
			CASE 
			WHEN pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
				THEN (CONVERT(bigint, 110000000) * (110000000 * 3 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * re.ResourceId + re.ResourceId))
			ELSE (CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId))
			END,
		CASE 
			WHEN  pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y''---override
				THEN (110000000 * 1 + re.ResourceId)
			ELSE pnBillOrg.PracticeId 
			END,
		CASE re.GoDirect
			WHEN ''Y''
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
			END ,
		(110000000 * 1 + reServLoc.ResourceId),
		re.ResourceId,
		re.Billable,
		pa.ResourceId,
		reServLoc.ResourceId,
		pnBillOrg.PracticeId
')

IF EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'BillingServices' and schemas.name = 'model') 
BEGIN
	EXEC dbo.DropObject 'model.BillingServices'
END
GO
EXEC( 'CREATE VIEW [model].[BillingServices]
	WITH SCHEMABINDING
	AS
	SELECT q.ItemId AS Id
		, q.InvoiceId
		, q.OrderingProviderId
		, q.OrdinalId
		, CONVERT(NVARCHAR, NULL) AS ProcedureCode835Description
		, q.Unit
		, q.UnitAllowableExpense
		, q.UnitCharge
		, q.EncounterServiceId
		, NULL AS OrderingExternalProviderId
	FROM (
		SELECT ItemId AS ItemId,
			inv.Id AS InvoiceId,
			CASE prs.OrderDoc
				WHEN ''T''
					THEN p.UserId
				ELSE 0
				END AS OrderingProviderId,
			CASE 
				WHEN ItemOrder > 0
					THEN ItemOrder
				ELSE 1
				END AS OrdinalId,
			prs.QuantityDecimal AS Unit,
			prs.FeeChargeDecimal AS UnitAllowableExpense,
			prs.ChargeDecimal AS UnitCharge,
			ps.CodeId AS EncounterServiceId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivableServices prs
		INNER JOIN model.Invoices inv ON inv.EncounterId  = SUBSTRING(prs.Invoice,CHARINDEX(''-'',prs.Invoice)+1,LEN(prs.Invoice))
		INNER JOIN model.Providers p ON p.Id = inv.BillingProviderId
		INNER JOIN dbo.PracticeServices ps ON ps.Code = prs.Service
		WHERE prs.[Status] = ''A''
		GROUP BY 
			prs.ItemId,
			inv.Id,
			CASE prs.OrderDoc
				WHEN ''T''
					THEN p.UserId
				ELSE 0
				END,
			CASE 
				WHEN ItemOrder > 0
					THEN ItemOrder
				ELSE 1
				END,
			prs.QuantityDecimal,
			prs.ChargeDecimal,
			prs.FeeChargeDecimal,
			CodeId
	) q')
GO

IF NOT EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'ServiceLocations_Partition1' and schemas.name = 'model') 
	BEGIN
	EXEC('CREATE VIEW model.ServiceLocations_Partition1
		WITH SCHEMABINDING
		AS
		SELECT Id AS Id
							FROM dbo.PracticeCodeTable
							WHERE SUBSTRING(Code, 1, 2) = ''11''
								AND ReferenceType = ''PLACEOFSERVICE''')
	END

IF EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'ServiceLocations' and schemas.name = 'model') 
BEGIN
	DROP VIEW [model].[ServiceLocations]
END
	EXEC('CREATE VIEW [model].[ServiceLocations]
		WITH SCHEMABINDING
		AS
		----First number of GetPairId for ServiceLocationId indicates where it comes from (0 - PracticeName, 1 - Resources)
		----If a surgery center is a provider, I''m using the PracticeName row for ServiceLocation because encounters are scheduled in the PracticeName ServiceLocation.
		SELECT v.PracticeId AS Id
			,CONVERT(BIT, 0) AS IsExternal
			,v.NAME AS [Name]
			,v.ServiceLocationCodeId AS ServiceLocationCodeId
			,v.ShortName AS ShortName
			,CONVERT(bit, 0) AS IsArchived
			,HexColor AS HexColor
		FROM (
			SELECT pn.PracticeId AS PracticeId
				,pic.FieldValue AS FieldValue
				,re.Billable AS Billable
				,re.ServiceCode AS ServiceCode
				,CASE 
					WHEN pic.FieldValue = ''T''
						AND re.Billable = ''Y''
						AND re.ServiceCode = ''02''
						AND pn.LocationReference <> ''''
						THEN pctRe.Id
					ELSE pct.Id
					END AS ServiceLocationCodeId
				,CASE LocationReference
					WHEN ''''
						THEN ''Office''
					ELSE ''Office-'' + LocationReference
					END AS ShortName
				,PracticeName AS NAME
				,pn.HexColor AS HexColor
				,COUNT_BIG(*) AS Count
			FROM dbo.PracticeName pn
			INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
			LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''PLACEOFSERVICE''
				AND SUBSTRING(pct.Code, 1, 2) = ''11''
			LEFT JOIN dbo.Resources re ON re.PracticeId = pn.PracticeId
				AND re.ResourceType = ''R''
				AND re.ServiceCode = ''02''
				AND re.Billable = ''Y''
				AND pic.FieldValue = ''T''
				AND pn.LocationReference <> ''''
			LEFT JOIN dbo.PracticeCodeTable pctRE ON pctRE.ReferenceType = ''PLACEOFSERVICE''
				AND SUBSTRING(pctRE.Code, 1, 2) = re.PlaceOfService
			WHERE pn.PracticeType = ''P''
			GROUP BY pn.PracticeId
				,pct.Id
				,LocationReference
				,pic.FieldValue
				,re.Billable
				,re.ServiceCode
				,pctRe.Id
				,PracticeName
				,pn.HexColor
			) AS v
	
	')
GO

IF NOT EXISTS(
SELECT * 
FROM sys.indexes 
WHERE Name='IX_Appointments_InvoiceNumber' AND Object_Id = Object_Id('Appointments'))
BEGIN 
	CREATE INDEX IX_Appointments_InvoiceNumber ON dbo.Appointments (InvoiceNumber)
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo.PatientReceivableServices') AND NAME ='IX_PatientReceivableServicesStatus')
BEGIN
	CREATE NONCLUSTERED INDEX IX_PatientReceivableServicesStatus
	ON dbo.PatientReceivableServices (Status)
	INCLUDE (QuantityDecimal,ChargeDecimal,FeeChargeDecimal,AppointmentId)
END
GO

IF EXISTS(
	SELECT 
	sys.objects.name
	,sys.schemas.name AS schema_name
	FROM sys.objects 
	INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
	WHERE objects.name = 'Charges' and schemas.name = 'dbo') 
BEGIN
	DROP VIEW dbo.Charges
END
GO

CREATE VIEW dbo.Charges
WITH SCHEMABINDING
AS
SELECT 
	SUM(ISNULL(prs.QuantityDecimal,0) * ISNULL(prs.ChargeDecimal,0)) AS Charge
	,SUM(ISNULL(prs.QuantityDecimal,0) * ISNULL(prs.FeeChargeDecimal,0)) AS ChargesByFee
	,inv.Id AS InvoiceId
	,COUNT_BIG(*) AS [Count]
	FROM dbo.PatientReceivableServices prs
	INNER JOIN model.Invoices inv ON inv.EncounterId  = prs.AppointmentId
	WHERE prs.[Status] = 'A'
	GROUP BY inv.Id		
GO

CREATE UNIQUE CLUSTERED INDEX IX_Charges ON dbo.Charges(InvoiceId) 
GO