﻿CREATE TABLE ViewIdentities(Name nvarchar(max), Value bigint)

GO

CREATE FUNCTION GetIdentCurrent(@name nvarchar(max))
RETURNS bigint
AS
BEGIN

	RETURN (SELECT COALESCE(IDENT_CURRENT(@name), (
		SELECT TOP 1 Value FROM ViewIdentities WHERE Name = @name
	)))
END

GO
