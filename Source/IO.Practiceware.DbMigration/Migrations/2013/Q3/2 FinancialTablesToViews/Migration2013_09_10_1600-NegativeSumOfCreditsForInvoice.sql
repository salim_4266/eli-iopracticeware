﻿IF OBJECT_ID ( 'dbo.NegativeSumOfCreditsForInvoice', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.NegativeSumOfCreditsForInvoice;
GO

CREATE PROCEDURE dbo.NegativeSumOfCreditsForInvoice
	@ReceivableId INT
AS
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#InvoiceReceivableIds') IS NOT NULL DROP TABLE #InvoiceReceivableIds

SELECT 
* 
INTO #InvoiceReceivableIds
FROM (
	SELECT
	ir.Id
	FROM model.InvoiceReceivables ir
	WHERE ir.InvoiceId IN (
		SELECT
		ir.InvoiceId
		FROM model.InvoiceReceivables ir
		WHERE ir.Id = @ReceivableId
	)
)v
IF (@@ROWCOUNT > 0)
BEGIN

CREATE UNIQUE CLUSTERED INDEX IX_#InvoiceReceivableIds_Ids ON #InvoiceReceivableIds(Id)

SELECT
-SUM(adj.Amount)
FROM model.InvoiceReceivables ir
INNER JOIN model.Adjustments adj ON ir.Id = adj.InvoiceReceivableId
INNER JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
INNER JOIN #InvoiceReceivableIds irs ON irs.Id = ir.Id
WHERE at.IsDebit = 0

END
GO