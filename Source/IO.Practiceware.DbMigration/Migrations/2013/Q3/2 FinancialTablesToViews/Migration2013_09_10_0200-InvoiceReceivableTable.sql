﻿--1.Drop View InvoiceReceivables
EXEC dbo.DropObject 'model.InvoiceReceivables'
GO

IF EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'Invoices' and schemas.name = 'model') 
BEGIN
	EXEC dbo.DropObject 'model.Invoices'
END
GO

EXEC('CREATE VIEW [model].[Invoices]
	WITH SCHEMABINDING
	AS
	----ServiceLocation used for the AttributeTo property
	
	----Appt is main or satellite office (PracticeName), AttributeTo is main or satellite office (PracticeName) [NO OVERRIDE - HANDLED IN CLAIM FILE ONLY]
	SELECT v.Id AS Id, 
		MIN(v.AttributeTo) AS AttributeToServiceLocationId,
		v.BillingProviderId,
		v.[DateTime],
		v.IsAssignmentRefused,
		v.EncounterId,
		v.HasPatientAssignedBenefits,
		v.ClinicalInvoiceProviderId as ClinicalInvoiceProviderId,
		v.ReferringExternalProviderId,
		v.InvoiceTypeId,
		CONVERT(bit, v.IsNoProviderSignatureOnFile) AS IsNoProviderSignatureOnFile,
		v.IsReleaseOfInformationNotSigned,
		v.ServiceLocationCodeId
	FROM (
		SELECT pr.AppointmentId AS Id,
			pnAttribute.PracticeId AS AttributeTo,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + pr.BillToDr)) AS BillingProviderId,
			CONVERT(datetime, InvoiceDate, 112) AS [DateTime],
			CASE 
				WHEN pd.FinancialAssignment = ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsAssignmentRefused,
			ap.AppointmentId AS EncounterId,
			CASE pd.FinancialSignature
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS HasPatientAssignedBenefits,
			(CONVERT(bigint, 110000000) * ap.ResourceId1 + ap.AppointmentId) AS ClinicalInvoiceProviderId,
			CASE 
				WHEN pvExternalProvider.VendorId IS NULL
					THEN NULL
				ELSE pr.ReferDr 
				END AS ReferringExternalProviderId,
			CASE 
				WHEN re.ResourceType IN (''Q'', ''Y'')
					THEN 3
				ELSE 1
				END AS InvoiceTypeId,
			CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
			CASE pd.FinancialSignature
				WHEN ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsReleaseOfInformationNotSigned,
			CASE 
				WHEN MAX(pctSLCode.Id) IS NOT NULL AND MAX(pctSLCode.Id) <> ''''
					THEN MAX(pctSLCode.Id)
				ELSE (SELECT MAX(Id) AS Id FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLACEOFSERVICE''
						AND SUBSTRING(Code, 1, 2) = ''11''
					GROUP BY ReferenceType, Code
					)
			END AS ServiceLocationCodeId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PatientDemographicsTable pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
		INNER JOIN dbo.Resources re ON re.ResourceId = pr.BillToDr
		INNER JOIN dbo.Resources clinicalRe ON clinicalRe.ResourceId = ap.ResourceId1
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			AND pnServLoc.PracticeId =
				CASE 
					WHEN ap.ResourceId2 = 0
						THEN (SELECT PracticeId AS PracticeId
							FROM dbo.PracticeName
							WHERE PracticeType = ''P''
							AND LocationReference = '''')
					ELSE ap.ResourceId2 - 1000
					END 
		INNER JOIN dbo.PracticeName pnAttribute ON pnAttribute.PracticeType = ''P''
			AND pnAttribute.PracticeId =
				CASE 
					WHEN pr.BillingOffice = 0
						THEN (SELECT PracticeId AS PracticeId
							FROM dbo.PracticeName
							WHERE PracticeType = ''P''
							AND LocationReference = '''')
					ELSE pr.BillingOffice - 1000
					END
		LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = prs.PlaceOfService
			AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
		LEFT JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
		LEFT JOIN dbo.PracticeVendors pvExternalProvider ON pr.ReferDr = pvExternalProvider.VendorId AND pvExternalProvider.VendorLastName <> '''' AND pvExternalProvider.VendorLastName IS NOT NULL
		WHERE ap.Comments <> ''ASC CLAIM''
			AND (
				ap.ResourceId2 = 0
				OR ap.ResourceId2 > 1000
				)
			AND (
				pr.BillingOffice = 0
				OR pr.BillingOffice > 1000
				)
		GROUP BY pr.AppointmentId,
			pnAttribute.PracticeId,
			pnServLoc.PracticeId,
			pnBillOrg.PracticeId, 
			pr.BillToDr,
			pr.InvoiceDate,
			ap.AppointmentId,
			pd.FinancialSignature,
			pd.FinancialAssignment,
			ap.ResourceId1,
			pr.ReferDr,
			re.ResourceType,
			clinicalRe.ResourceType,
			apt.ResourceId8,
			pvExternalProvider.VendorId
	
		UNION ALL
	
		----Provider is ASC, appt satellite office (PracticeName), AttributeTo is main or satellite office (PracticeName)
		SELECT pr.AppointmentId AS Id,
			pnAttribute.PracticeId AS AttributeTo,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + reASC.ResourceId)) AS BillingProviderId,
			CONVERT(datetime, pr.InvoiceDate, 112) AS [DateTime],
			CASE 
				WHEN pd.FinancialAssignment = ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsAssignmentRefused,
			apEncounter.AppointmentId AS EncounterId,
			CASE pd.FinancialSignature
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS HasPatientAssignedBenefits,
			(CONVERT(bigint, 110000000) * reASC.ResourceId + ap.AppointmentId) AS ClinicalInvoiceProviderId,
			CASE 
				WHEN pvExternalProvider.VendorId IS NULL
					THEN NULL
				ELSE pr.ReferDr 
				END AS ReferringExternalProviderId,
			2 AS InvoiceTypeId,
			CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
			CASE pd.FinancialSignature
				WHEN ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsReleaseOfInformationNotSigned,
			CASE 
				WHEN MAX(pctSLCode.Id) IS NOT NULL AND MAX(pctSLCode.Id) <> ''''
					THEN MAX(pctSLCode.Id)
				ELSE (SELECT MAX(Id) AS Id FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLACEOFSERVICE''
						AND SUBSTRING(Code, 1, 2) = ''11''
					GROUP BY ReferenceType, Code
					)
			END AS ServiceLocationCodeId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		INNER JOIN dbo.PatientDemographicsTable pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
			AND ap.Comments = ''ASC CLAIM'' 
		INNER JOIN dbo.AppointmentType apt ON ap.AppTypeId = apt.AppTypeId
			AND apt.ResourceId8 = 1
		INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
			AND apEncounter.AppTypeId = ap.AppTypeId
			AND apEncounter.AppDate = ap.AppDate
			AND apEncounter.AppTime > 0 
			AND ap.AppTime = 0
			AND apEncounter.ScheduleStatus = ap.ScheduleStatus
			AND ap.ScheduleStatus = ''D''
			AND apEncounter.ResourceId1 = ap.ResourceId1
			AND apEncounter.ResourceId2 = ap.ResourceId2
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
			AND PracticeType = ''P''
			AND LocationReference <> ''''
		INNER JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
			AND reASC.ResourceType = ''R''
			AND reASC.ServiceCode = ''02''
			AND reASC.Billable = ''Y''
			AND pic.FieldValue = ''T''
		INNER JOIN dbo.PracticeName pnAttribute ON pnAttribute.PracticeType = ''P''
			AND pnAttribute.PracticeId =
				CASE 
					WHEN pr.BillingOffice = 0
						THEN (SELECT PracticeId AS PracticeId
							FROM dbo.PracticeName
							WHERE PracticeType = ''P''
							AND LocationReference = '''')
					ELSE pr.BillingOffice - 1000
					END
		LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = reASC.PlaceOfService
			AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
		LEFT JOIN dbo.PracticeVendors pvExternalProvider ON pr.ReferDr = pvExternalProvider.VendorId AND pvExternalProvider.VendorLastName <> '''' AND pvExternalProvider.VendorLastName IS NOT NULL
		WHERE ap.ResourceId2 > 1000
			AND (
				pr.BillingOffice = 0
				OR pr.BillingOffice > 1000
				)
		GROUP BY pr.AppointmentId,
			apEncounter.AppointmentId,
			ap.AppointmentId,
			pnAttribute.PracticeId,
			pnServLoc.PracticeId,
			pr.BillToDr,
			pr.InvoiceDate,
			pd.FinancialSignature,
			pd.FinancialAssignment,
			ap.ResourceId1,
			pr.ReferDr,
			reASC.ResourceType,
			reASC.ResourceId,
			apt.ResourceId8,
			pvExternalProvider.VendorId
	
		UNION ALL
	
		----Provider is ASC, appt is satellite office (PracticeName), AttributeTo a facility (Resources)
		SELECT pr.AppointmentId AS Id,
			reAttribute.ResourceId AS AttributeTo,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + reASC.ResourceId)) AS BillingProviderId,
			CONVERT(datetime, pr.InvoiceDate, 112) AS [DateTime],
			CASE 
				WHEN pd.FinancialAssignment = ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsAssignmentRefused,
			apEncounter.AppointmentId AS EncounterId,
			CASE pd.FinancialSignature
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS HasPatientAssignedBenefits,
			(CONVERT(bigint, 110000000) * reASC.ResourceId + ap.AppointmentId) AS ClinicalInvoiceProviderId,
			CASE 
				WHEN pvExternalProvider.VendorId IS NULL
					THEN NULL
				ELSE pr.ReferDr 
				END AS ReferringExternalProviderId,
			2 AS InvoiceTypeId,
			CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
			CASE pd.FinancialSignature
				WHEN ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsReleaseOfInformationNotSigned,
			CASE 
				WHEN MAX(pctSLCode.Id) IS NOT NULL AND MAX(pctSLCode.Id) <> ''''
					THEN MAX(pctSLCode.Id)
				ELSE (SELECT MAX(Id) AS Id FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLACEOFSERVICE''
						AND SUBSTRING(Code, 1, 2) = ''11''
					GROUP BY ReferenceType, Code
					)
			END AS ServiceLocationCodeId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		INNER JOIN dbo.PatientDemographicsTable pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
			AND ap.Comments = ''ASC CLAIM'' 
		INNER JOIN dbo.AppointmentType apt ON ap.AppTypeId = apt.AppTypeId
			AND apt.ResourceId8 = 1
		INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
			AND apEncounter.AppTypeId = ap.AppTypeId
			AND apEncounter.AppDate = ap.AppDate
			AND apEncounter.AppTime > 0 
			AND ap.AppTime = 0
			AND apEncounter.ScheduleStatus = ap.ScheduleStatus
			AND ap.ScheduleStatus = ''D''
			AND apEncounter.ResourceId1 = ap.ResourceId1
			AND apEncounter.ResourceId2 = ap.ResourceId2
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
			AND PracticeType = ''P''
			AND LocationReference <> ''''
		INNER JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
			AND reASC.ResourceType = ''R''
			AND reASC.ServiceCode = ''02''
			AND reASC.Billable = ''Y''
			AND pic.FieldValue = ''T''
		INNER JOIN dbo.Resources reAttribute ON reAttribute.ResourceId = pr.BillingOffice
			AND reAttribute.ResourceType = ''R''
			AND reAttribute.ServiceCode = ''02''
			AND pr.BillingOffice between 1 and 999
		LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = reASC.PlaceOfService
			AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
		LEFT JOIN dbo.PracticeVendors pvExternalProvider ON pr.ReferDr = pvExternalProvider.VendorId AND pvExternalProvider.VendorLastName <> '''' AND pvExternalProvider.VendorLastName IS NOT NULL
		WHERE ap.ResourceId2 > 1000
			AND (
				pr.BillingOffice = 0
				OR pr.BillingOffice > 1000
				)
		GROUP BY pr.AppointmentId,
			apEncounter.AppointmentId,
			ap.AppointmentId,
			reAttribute.ResourceId,
			pnServLoc.PracticeId,
			pr.BillToDr,
			pr.InvoiceDate,
			pd.FinancialSignature,
			pd.FinancialAssignment,
			ap.ResourceId1,
			pr.ReferDr,
			reASC.ResourceType,
			reASC.ResourceId,
			apt.ResourceId8,
			pvExternalProvider.VendorId
			
		UNION ALL
	
		----Encounter is at main or satellite office (PracticeName); AttributeTo a facility (Resources) [NO OVERRIDE, OVERRIDE IS HANDLED IN CLAIM FILE ONLY]
		SELECT  pr.AppointmentId AS Id,
			(110000000 * 1 + reAttribute.ResourceId) AS AttributeTo,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS BillingProviderId,
			CONVERT(datetime, InvoiceDate, 112) AS DateTime,
			CASE 
				WHEN pd.FinancialAssignment = ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsAssignmentRefused,
			ap.AppointmentId AS EncounterId,
			CASE pd.FinancialSignature
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS HasPatientAssignedBenefits,
			(CONVERT(bigint, 110000000) * ap.ResourceId1 + ap.AppointmentId) AS ClinicalInvoiceProviderId,
			CASE 
				WHEN pr.ReferDr = 0 
					OR pr.ReferDr IS NULL 
					OR pr.ReferDr = ''''
					THEN NULL
				ELSE pr.ReferDr
				END AS ReferringExternalProviderId,
			CASE 
				WHEN re.ResourceType IN (''Q'', ''Y'')
					THEN 3
				ELSE 1
			END AS InvoiceTypeId,
			CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
			CASE pd.FinancialSignature
				WHEN ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsReleaseOfInformationNotSigned,
			CASE 
				WHEN MAX(pctSLCode.Id) IS NOT NULL
					THEN MAX(pctSLCode.Id)
				ELSE (SELECT MAX(Id) AS Id FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLACEOFSERVICE''
						AND SUBSTRING(Code, 1, 2) = ''11''
					GROUP BY ReferenceType, Code)
				END AS ServiceLocationCodeId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PatientDemographicsTable pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
		INNER JOIN dbo.Resources re ON re.ResourceId = pr.BillToDr
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
	               AND pnServLoc.PracticeId = CASE 
	                       WHEN ap.ResourceId2 = 0
	                               THEN (SELECT PracticeId AS PracticeId FROM dbo.PracticeName WHERE LocationReference = '''' AND PracticeType = ''P'')
	                       ELSE ap.ResourceId2 - 1000
	                       END
		INNER JOIN dbo.Resources reAttribute ON reAttribute.ResourceId = pr.BillingOffice
			AND reAttribute.ResourceType = ''R''
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		LEFT JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
		LEFT JOIN model.InsurancePolicies ip ON pr.InsuredId = ip.PolicyHolderPatientId
			AND pr.InsurerId = ip.InsurerId
			AND pr.InvoiceDate >= CONVERT(NVARCHAR(10),ip.StartDateTime,112)
		LEFT JOIN model.PatientInsurances pi ON pi.InsurancePolicyId = ip.Id
			AND (
				CONVERT(NVARCHAR(10),pi.EndDateTime,112) <= pr.InvoiceDate
				OR CONVERT(NVARCHAR(10),pi.EndDateTime,112) = ''''
				OR CONVERT(NVARCHAR(10),pi.EndDateTime,112) IS NULL
				)
		LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = prs.PlaceOfService
			AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
		WHERE (
				ap.ResourceId2 = 0
				OR ap.ResourceId2 > 1000
				)
			AND (pr.BillingOffice BETWEEN 1 AND 999 OR pr.BillingOffice > 2000)
		GROUP BY pr.AppointmentId,
			reAttribute.ResourceId,
			pnServLoc.PracticeId,
			pnBillOrg.PracticeId, 
			re.ResourceId,
			pr.InvoiceDate,
			ap.AppointmentId,
			pd.FinancialSignature,
			pd.FinancialAssignment,
			ap.ResourceId1,
			pr.ReferDr,
			re.ResourceType,
			apt.ResourceId8,
			re.PracticeId
	
		UNION ALL
	
		----Encounter is at an external facility (Resources); AttributeTo is facility (Resources)  [OVERRIDE IS HANDLED IN CLAIM FILE ONLY]
		SELECT  pr.AppointmentId AS Id,
			(110000000 * 1 + reAttribute.ResourceId) AS AttributeTo,
			(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS BillingProviderId,
			CONVERT(datetime, InvoiceDate, 112) AS DateTime,
			CASE 
				WHEN pd.FinancialAssignment = ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsAssignmentRefused,
			ap.AppointmentId AS EncounterId,
			CASE pd.FinancialSignature
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS HasPatientAssignedBenefits,
			(CONVERT(bigint, 110000000) * ap.ResourceId1 + ap.AppointmentId) AS ClinicalInvoiceProviderId,
			CASE 
				WHEN pr.ReferDr = 0 
					OR pr.ReferDr IS NULL 
					OR pr.ReferDr = ''''
					THEN NULL
				ELSE pr.ReferDr
				END AS ReferringExternalProviderId,
			CASE 
				WHEN re.ResourceType IN (''Q'', ''Y'')
					THEN 3
				ELSE 1
				END AS InvoiceTypeId,
			CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
			CASE pd.FinancialSignature
				WHEN ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsReleaseOfInformationNotSigned,
			CASE 
				WHEN MAX(pctSLCode.Id) IS NOT NULL
					THEN MAX(pctSLCode.Id)
				ELSE (SELECT MAX(Id)  AS Id FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLACEOFSERVICE''
						AND SUBSTRING(Code, 1, 2) = ''11''
					GROUP BY ReferenceType, Code)
				END AS ServiceLocationCodeId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PatientDemographicsTable pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
		INNER JOIN dbo.Resources re ON re.ResourceId = pr.BillToDr
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceId = ap.ResourceId2
			AND reServLoc.ResourceType = ''R''
		INNER JOIN dbo.Resources reAttribute ON reAttribute.ResourceId = pr.BillingOffice
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = prs.PlaceOfService
				AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
		LEFT JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
		LEFT JOIN model.InsurancePolicies ip ON 
			pr.InsuredId = ip.PolicyHolderPatientId
			AND pr.InsurerId = ip.InsurerId
			AND pr.InvoiceDate >= CONVERT(NVARCHAR(10),ip.StartDateTime,112)
		LEFT JOIN model.PatientInsurances pi ON 
			pi.InsurancePolicyId = ip.Id
			AND (
				pr.InvoiceDate <= CONVERT(NVARCHAR(10),pi.EndDateTime,112)
				OR CONVERT(NVARCHAR(10),pi.EndDateTime,112) = ''''
				OR CONVERT(NVARCHAR(10),pi.EndDateTime,112) IS NULL
				)
		WHERE (ap.ResourceId2 BETWEEN 1 AND 999 OR ap.ResourceId2 > 2000)
			AND (pr.BillingOffice BETWEEN 1 AND 999 OR pr.BillingOffice > 2000)
		GROUP BY pr.AppointmentId,
			reAttribute.ResourceId,
			reServLoc.ResourceId,
			pnBillOrg.PracticeId, 
			re.ResourceId,
			pr.InvoiceDate,
			ap.AppointmentId,
			pd.FinancialSignature,
			pd.FinancialAssignment,
			ap.ResourceId1,
			pr.ReferDr,
			re.ResourceType,
			apt.ResourceId8,
			reServLoc.PracticeId
	
		UNION ALL
	
		----appt at facility (Resources); AttributeTo at main or satellite office (PracticeName) [NO OVERRIDE, HANDLED IN CLAIM FILE ONLY]
		SELECT  pr.AppointmentId AS Id,
			pnAttribute.PracticeId AS AttributeTo,
			(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS BillingProviderId,
			CONVERT(datetime, InvoiceDate, 112) AS DateTime,
			CASE 
				WHEN pd.FinancialAssignment = ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsAssignmentRefused,
			ap.AppointmentId AS EncounterId,
			CASE pd.FinancialSignature
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS HasPatientAssignedBenefits,
			(CONVERT(bigint, 110000000) * ap.ResourceId1 + ap.AppointmentId) AS ClinicalInvoiceProviderId,
			CASE 
				WHEN pr.ReferDr = 0 
					OR pr.ReferDr IS NULL 
					OR pr.ReferDr = ''''
					THEN NULL
				ELSE pr.ReferDr
				END AS ReferringExternalProviderId,
			CASE 
				WHEN re.ResourceType IN (''Q'', ''Y'')
					THEN 3
				ELSE 1
				END AS InvoiceTypeId,
			CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
			CASE pd.FinancialSignature
				WHEN ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsReleaseOfInformationNotSigned,
			CASE 
				WHEN MAX(pctSLCode.Id) IS NOT NULL
					THEN MAX(pctSLCode.Id)
				ELSE (SELECT MAX(Id)  AS Id FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLACEOFSERVICE''
						AND SUBSTRING(Code, 1, 2) = ''11''
					GROUP BY ReferenceType, Code)
				END AS ServiceLocationCodeId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PatientDemographicsTable pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
		INNER JOIN dbo.Resources re ON re.ResourceId = pr.BillToDr
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceId = ap.ResourceId2
			AND reServLoc.ResourceType = ''R''
		INNER JOIN dbo.PracticeName pnAttribute ON pnAttribute.PracticeType = ''P''
			AND pnAttribute.PracticeId = CASE 
				WHEN pr.BillingOffice = 0
					THEN (SELECT PracticeId AS PracticeId
						FROM dbo.PracticeName
						WHERE PracticeType = ''P''
						AND LocationReference = '''')
				ELSE pr.BillingOffice - 1000
				END
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		LEFT JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
		LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = prs.PlaceOfService
				AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
		LEFT JOIN model.InsurancePolicies ip ON 
			pr.InsuredId = ip.PolicyHolderPatientId
			AND pr.InsurerId = ip.InsurerId
			AND pr.InvoiceDate >= CONVERT(NVARCHAR(10),ip.StartDateTime,112)
		LEFT JOIN model.PatientInsurances pi ON 
			pi.InsurancePolicyId = ip.Id
			AND (
				pr.InvoiceDate <= CONVERT(NVARCHAR(10),pi.EndDateTime,112)
				OR CONVERT(NVARCHAR(10),pi.EndDateTime,112) = ''''
				OR CONVERT(NVARCHAR(10),pi.EndDateTime,112) IS NULL
				)
		WHERE (ap.ResourceId2 BETWEEN 1 AND 999 OR ap.ResourceId2 > 2000)
			AND (
				pr.BillingOffice = 0
				OR pr.BillingOffice > 1000
				) 
		GROUP BY pr.AppointmentId,
			pnAttribute.PracticeId,
			reServLoc.ResourceId,
			pnBillOrg.PracticeId, 
			re.ResourceId,
			pr.InvoiceDate,
			ap.AppointmentId,
			pd.FinancialAssignment,
			pd.FinancialSignature,
			ap.ResourceId1,
			pr.ReferDr,
			re.ResourceType,
			pic.FieldValue,
			apt.ResourceId8,
			reServLoc.PracticeId
	
			) AS v
	GROUP BY v.Id,
		v.BillingProviderId,
		v.[DateTime],
		v.IsAssignmentRefused,
		v.EncounterId,
		v.HasPatientAssignedBenefits,
		v.ClinicalInvoiceProviderId,
		v.ReferringExternalProviderId,
		v.InvoiceTypeId,
		v.IsNoProviderSignatureOnFile,
		v.IsReleaseOfInformationNotSigned,
		v.ServiceLocationCodeId'
)
GO

--1.Create table
CREATE TABLE [model].[InvoiceReceivables] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [InvoiceId] int  NOT NULL,
	[PatientInsuranceId] bigint  NULL,
	[PayerClaimControlNumber] nvarchar(max)  NULL,
	[PatientInsuranceAuthorizationId] int  NULL,
    [PatientInsuranceReferralId] int  NULL,
	[OpenForReview] bit  NOT NULL,
	[ComputedInvoiceReceivableId] bigint NULL,
	[LegacyInsurerDesignation] NVARCHAR(1) NULL
);
GO

--2. Insert data into table
----Note: PatientInsuranceId can be null.
INSERT INTO [model].[InvoiceReceivables] ([InvoiceId], [PatientInsuranceId], [PayerClaimControlNumber], [PatientInsuranceAuthorizationId], [PatientInsuranceReferralId], [OpenForReview], [ComputedInvoiceReceivableId])
SELECT 
	[InvoiceId],
	PatientInsuranceId,	
	[PayerClaimControlNumber],
	[PatientInsuranceAuthorizationId],
    [PatientInsuranceReferralId],
	CONVERT(bit,0),
	[Id] 
FROM dbo.[InvoiceReceivablesBackup]
GO

--Found that the current InvoiceReceivables view does not include rows that have not been billed 
--The ReceivableType is equal to I or S. Need to migrate these rows along with the crossover rows  to InvoiceReceivables
--Going forward these rows will be automatically added with the trigger
DECLARE @InvoiceReceivableMax int
SET @InvoiceReceivableMax = 110000000
DECLARE @PatientInsuranceMax int
SET @PatientInsuranceMax = 110000000

SELECT v.CompositeId AS Id
	,v.InvoiceId AS InvoiceId
	,MAX(v.PatientInsuranceId) AS PatientInsuranceId
	,MAX(v.ExternalRefInfo) AS PayerClaimControlNumber
	,MAX(pCert.PreCertId) AS PatientInsuranceAuthorizationId
	,MAX(CASE WHEN pRef.ReferredInsurer <> v.FinancialInsurerId THEN NULL ELSE pRef.ReferralId END) AS PatientInsuranceReferralId
	INTO #NeverBeenBilledReceivables
FROM (
SELECT model.GetBigPairId(pr.ComputedFinancialId, ap.AppointmentId, @InvoiceReceivableMax) AS CompositeId
		---Never been billed PatientReceivables, patient does have insurnace
		,pr.AppointmentId AS InvoiceId
		,CASE pfBilled.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000			
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + CASE 
			WHEN pfBilled.PatientId = pd.PolicyPatientId
				THEN 0
			ELSE 3
			END + CONVERT(INT, pfBilled.FinancialPIndicator) AS OrdinalId
		,model.GetBigPairId(pr.PatientId, pfBilled.FinancialId, @PatientInsuranceMax) AS PatientInsuranceId
		,pr.ExternalRefInfo AS ExternalRefInfo
		,ap.ReferralId AS ReferralId
		,ap.PreCertId AS PreCertId
		,pfBilled.FinancialInsurerId AS FinancialInsurerId
		,COUNT_BIG(*) AS [Count]
	FROM dbo.PatientDemographicsTable pd
	INNER JOIN dbo.Appointments ap ON pd.PatientId = ap.PatientId
	INNER JOIN dbo.PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId
	INNER JOIN dbo.PatientFinancial pfBilled ON pfBilled.FinancialId = pr.ComputedFinancialId
	WHERE pr.ReceivableType IN ('I', 'S')
	GROUP BY model.GetBigPairId(pr.ComputedFinancialId, ap.AppointmentId, @InvoiceReceivableMax)
		,pr.AppointmentId
		,CASE pfBilled.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000			
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + CASE 
			WHEN pfBilled.PatientId = pd.PolicyPatientId
				THEN 0
			ELSE 3
			END + CONVERT(INT, pfBilled.FinancialPIndicator)
		,model.GetBigPairId(pr.PatientId, pfBilled.FinancialId, @PatientInsuranceMax)
		,pr.ExternalRefInfo
		,ap.ReferralId
		,ap.PreCertId
		,pfBilled.FinancialInsurerId
) AS v
LEFT JOIN dbo.PatientReferral pRef ON pRef.ReferralId = v.ReferralId
	AND pRef.ReferredFromId IN (SELECT VendorId AS VendorId FROM dbo.PracticeVendors)
	AND pRef.ComputedFinancialId IS NOT NULL
LEFT JOIN dbo.PatientPreCerts pCert ON pCert.PreCertId = v.PreCertId 
	AND pCert.ComputedFinancialId IS NOT NULL 
	AND COALESCE(pCert.PreCertDate, '') <> ''
LEFT JOIN dbo.InvoiceReceivablesBackup irb ON 
	(irb.InvoiceId = v.InvoiceId
	AND irb.PatientInsuranceId = v.PatientInsuranceId
	)
WHERE irb.InvoiceId IS NULL
	AND irb.PatientInsuranceId IS NULL
GROUP BY v.CompositeId
	,v.InvoiceId

INSERT INTO [model].[InvoiceReceivables] ([InvoiceId], [PatientInsuranceId], [PayerClaimControlNumber], [PatientInsuranceAuthorizationId], [PatientInsuranceReferralId], [OpenForReview], [ComputedInvoiceReceivableId])
SELECT 
[InvoiceId],
PatientInsuranceId,
[PayerClaimControlNumber],
[PatientInsuranceAuthorizationId],
[PatientInsuranceReferralId],
CONVERT(bit,0),
[Id] 
FROM #NeverBeenBilledReceivables

SELECT v.CompositeId AS Id
	,v.InvoiceId AS InvoiceId
	,MAX(v.PatientInsuranceId) AS PatientInsuranceId
	,MAX(v.ExternalRefInfo) AS PayerClaimControlNumber
	,MAX(pCert.PreCertId) AS PatientInsuranceAuthorizationId
	,MAX(CASE WHEN pRef.ReferredInsurer <> v.FinancialInsurerId THEN NULL ELSE pRef.ReferralId END) AS PatientInsuranceReferralId
	INTO #NeverBeenBilledSecondaryReceivables
FROM (
SELECT model.GetBigPairId(pr.ComputedFinancialId, ap.AppointmentId, @InvoiceReceivableMax) AS CompositeId
		---Never been billed PatientReceivables, patient does have insurnace
		,pr.AppointmentId AS InvoiceId
		,CASE pfBilled.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000			
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + CASE 
			WHEN pfBilled.PatientId = pd.PolicyPatientId
				THEN 0
			ELSE 3
			END + CONVERT(INT, pfBilled.FinancialPIndicator) AS OrdinalId
		,model.GetBigPairId(pr.PatientId, pfBilled.FinancialId, @PatientInsuranceMax) AS PatientInsuranceId
		,pr.ExternalRefInfo AS ExternalRefInfo
		,ap.ReferralId AS ReferralId
		,ap.PreCertId AS PreCertId
		,pfBilled.FinancialInsurerId AS FinancialInsurerId
		,COUNT_BIG(*) AS [Count]
	FROM dbo.PatientDemographicsTable pd
	INNER JOIN dbo.Appointments ap ON pd.PatientId = ap.PatientId
	INNER JOIN dbo.PatientReceivables pr ON pr.AppointmentId = ap.AppointmentId
	INNER JOIN dbo.PatientFinancial pfBilled ON pfBilled.FinancialId = pr.ComputedFinancialId
	INNER JOIN dbo.#NeverBeenBilledReceivables nvrBilled ON pr.AppointmentId = nvrBilled.InvoiceId
	WHERE pr.ReceivableType NOT IN ('I','S')
	GROUP BY model.GetBigPairId(pr.ComputedFinancialId, ap.AppointmentId, @InvoiceReceivableMax)
		,pr.AppointmentId
		,CASE pfBilled.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000			
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + CASE 
			WHEN pfBilled.PatientId = pd.PolicyPatientId
				THEN 0
			ELSE 3
			END + CONVERT(INT, pfBilled.FinancialPIndicator)
		,model.GetBigPairId(pr.PatientId, pfBilled.FinancialId, @PatientInsuranceMax)
		,pr.ExternalRefInfo
		,ap.ReferralId
		,ap.PreCertId
		,pfBilled.FinancialInsurerId
) AS v
LEFT JOIN dbo.PatientReferral pRef ON pRef.ReferralId = v.ReferralId
	AND pRef.ReferredFromId IN (SELECT VendorId AS VendorId FROM dbo.PracticeVendors)
	AND pRef.ComputedFinancialId IS NOT NULL
LEFT JOIN dbo.PatientPreCerts pCert ON pCert.PreCertId = v.PreCertId 
	AND pCert.ComputedFinancialId IS NOT NULL 
	AND COALESCE(pCert.PreCertDate, '') <> ''
LEFT JOIN dbo.InvoiceReceivablesBackup irb ON 
	(irb.InvoiceId = v.InvoiceId
	AND irb.PatientInsuranceId = v.PatientInsuranceId
	)
WHERE irb.InvoiceId IS NULL
	AND irb.PatientInsuranceId IS NULL
GROUP BY v.CompositeId
	,v.InvoiceId

INSERT INTO [model].[InvoiceReceivables] ([InvoiceId], [PatientInsuranceId], [PayerClaimControlNumber], [PatientInsuranceAuthorizationId], [PatientInsuranceReferralId], [OpenForReview], [ComputedInvoiceReceivableId])
SELECT 
[InvoiceId],
PatientInsuranceId,
[PayerClaimControlNumber],
[PatientInsuranceAuthorizationId],
[PatientInsuranceReferralId],
CONVERT(bit,0),
[Id] 
FROM #NeverBeenBilledSecondaryReceivables

--3. Map PatientInsurnaceId 
UPDATE ir
SET ir.PatientInsuranceId = pi.Id
FROM model.InvoiceReceivables ir
INNER JOIN model.PatientInsurances pi ON ir.PatientInsuranceId = pi.ComputedPatientInsuranceId
GO

IF NOT EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'Encounters' and schemas.name = 'model') 

EXEC('CREATE VIEW [model].[Encounters]
	WITH SCHEMABINDING
	AS
	-- Audit
	----From PracticeName table - satellite offices
	SELECT ap.AppointmentId AS Id
		,COALESCE(CASE ap.ScheduleStatus
			WHEN ''P''
				THEN 1
			WHEN ''R''
				THEN 1
			WHEN ''X''
				THEN 14
			WHEN ''Q''
				THEN 13
			WHEN ''F''
				THEN 12
			WHEN ''N''
				THEN 11
			WHEN ''S''
				THEN 10
			WHEN ''Y''
				THEN 9
			WHEN ''O''
				THEN 8
			WHEN ''D''
				THEN 7
			WHEN ''A''
				THEN CASE pa.[Status]
						WHEN ''W''
							THEN 2
						WHEN ''M''
							THEN 3
						WHEN ''E''
							THEN 4
						WHEN ''G''
							THEN 5
						WHEN ''H''
							THEN 6
						WHEN ''U''
							THEN 15
						END
			END, 14) AS EncounterStatusId
		,CASE
			WHEN apt.ResourceId8 = 1 AND reASC.ResourceId IS NOT NULL
				THEN 3
			ELSE 1 
			END AS EncounterTypeId
		, CASE ap.ApptInsType
			WHEN ''M''
				THEN 1
			WHEN ''V''
				THEN 2
			WHEN ''A''
				THEN 3
			WHEN ''W''
				THEN 4
			ELSE 1
			END AS InsuranceTypeId
		,ap.PatientId AS PatientId
		,pn.PracticeId AS ServiceLocationId
		,ap.VisitReason AS ReasonForVisit
		,EncounterStatusChangeReasonId AS EncounterStatusChangeReasonId
		,EncounterStatusChangeComment AS EncounterStatusChangeComment
		,CONVERT(int, confirmStatusPct.Id) AS ConfirmationStatusId
	FROM dbo.Appointments ap
	INNER JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
	INNER JOIN dbo.PracticeName pn ON pn.PracticeType = ''P''
		AND ap.ResourceId2 - 1000 = pn.PracticeId
	INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
	INNER JOIN dbo.PracticeCodeTable pctQU ON pctQU.Code = apt.Question
		AND pctQU.ReferenceType = ''QUESTIONSETS''
	LEFT JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
	LEFT OUTER JOIN dbo.PracticeCodeTable confirmStatusPct ON SUBSTRING(confirmStatusPct.Code, 1, 1) = ap.ConfirmStatus
		AND confirmStatusPct.ReferenceType = ''CONFIRMSTATUS''
	LEFT JOIN dbo.Resources reASC ON reASC.PracticeId = pn.PracticeId
		AND pn.LocationReference <> ''''
		AND reASC.ResourceType = ''R''
		AND reASC.ServiceCode = ''02''
		AND FieldValue = ''T'' 
	WHERE ap.AppTime >= 1 
	
	UNION ALL
	
	----At main office
	SELECT ap.AppointmentId AS Id
		,COALESCE(CASE ap.ScheduleStatus
			WHEN ''P''
				THEN 1
			WHEN ''R''
				THEN 1
			WHEN ''X''
				THEN 14
			WHEN ''Q''
				THEN 13
			WHEN ''F''
				THEN 12
			WHEN ''N''
				THEN 11
			WHEN ''S''
				THEN 10
			WHEN ''Y''
				THEN 9
			WHEN ''O''
				THEN 8
			WHEN ''D''
				THEN 7
			WHEN ''A''
				THEN CASE pa.[Status]
						WHEN ''W''
							THEN 2
						WHEN ''M''
							THEN 3
						WHEN ''E''
							THEN 4
						WHEN ''G''
							THEN 5
						WHEN ''H''
							THEN 6
						WHEN ''U''
							THEN 15
						END
			END, 14) AS EncounterStatusId
		,1 AS EncounterTypeId
		,CASE ap.ApptInsType
			WHEN ''M''
				THEN 1
			WHEN ''V''
				THEN 2
			WHEN ''A''
				THEN 3
			WHEN ''W''
				THEN 4
			ELSE 1
			END AS InsuranceTypeId
		,ap.PatientId AS PatientId
		,pn.PracticeId AS ServiceLocationId
		,ap.VisitReason AS ReasonForVisit
		,EncounterStatusChangeReasonId AS EncounterStatusChangeReasonId
		,EncounterStatusChangeComment AS EncounterStatusChangeComment
		,CONVERT(int, confirmStatusPct.Id) AS ConfirmationStatusId
	FROM dbo.Appointments ap
	INNER JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
	INNER JOIN dbo.PracticeName pn ON pn.PracticeType = ''P''
		AND pn.LocationReference = ''''
		AND ap.ResourceId2 = 0
	INNER JOIN dbo.PracticeCodeTable pctQU ON pctQU.Code = apt.Question
		AND pctQU.ReferenceType = ''QUESTIONSETS''
	LEFT OUTER JOIN dbo.PracticeCodeTable confirmStatusPct ON SUBSTRING(confirmStatusPct.Code, 1, 1) = ap.ConfirmStatus
		AND confirmStatusPct.ReferenceType = ''CONFIRMSTATUS''
	LEFT JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
	WHERE ap.AppTime >= 1
	
	UNION ALL
	
	----External Facility, not billing  (SL from Resources table)
	SELECT ap.AppointmentId AS Id
		,COALESCE(CASE ap.ScheduleStatus
			WHEN ''P''
				THEN 1
			WHEN ''R''
				THEN 1
			WHEN ''X''
				THEN 14
			WHEN ''Q''
				THEN 13
			WHEN ''F''
				THEN 12
			WHEN ''N''
				THEN 11
			WHEN ''S''
				THEN 10
			WHEN ''Y''
				THEN 9
			WHEN ''O''
				THEN 8
			WHEN ''D''
				THEN 7
			WHEN ''A''
				THEN CASE pa.[Status]
						WHEN ''W''
							THEN 2
						WHEN ''M''
							THEN 3
						WHEN ''E''
							THEN 4
						WHEN ''G''
							THEN 5
						WHEN ''H''
							THEN 6
						WHEN ''U''
							THEN 15
						END
			END, 14) AS EncounterStatusId
		,1 AS EncounterTypeId
		,CASE ap.ApptInsType
			WHEN ''M''
				THEN 1
			WHEN ''V''
				THEN 2
			WHEN ''A''
				THEN 3
			WHEN ''W''
				THEN 4
			ELSE 1
			END AS InsuranceTypeId
		,ap.PatientId AS PatientId
		,(110000000 * 1 + re.ResourceId) AS ServiceLocationId
		,ap.VisitReason AS ReasonForVisit
		,EncounterStatusChangeReasonId AS EncounterStatusChangeReasonId
		,EncounterStatusChangeComment AS EncounterStatusChangeComment
		,CONVERT(int, confirmStatusPct.Id) AS ConfirmationStatusId
	FROM dbo.Appointments ap
	INNER JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
	INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId2
		AND re.ResourceType = ''R''
		AND re.ServiceCode = ''02'' 
	INNER JOIN dbo.PracticeCodeTable pctQU ON pctQU.Code = apt.Question
		AND pctQU.ReferenceType = ''QUESTIONSETS''
	LEFT OUTER JOIN dbo.PracticeCodeTable confirmStatusPct ON SUBSTRING(confirmStatusPct.Code, 1, 1) = ap.ConfirmStatus
		AND confirmStatusPct.ReferenceType = ''CONFIRMSTATUS''
	LEFT JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
	WHERE ap.AppTime >= 1')

GO

UPDATE ir
SET OpenForReview = CONVERT(BIT,1)
FROM model.InvoiceReceivables ir
JOIN model.Invoices i ON i.Id = ir.InvoiceId
JOIN model.Encounters e ON e.Id = i.EncounterId
JOIN PatientReceivablesBackup pr ON pr.PatientId = e.PatientId AND pr.AppointmentId = e.Id
WHERE pr.ReceivableType = 'I'
AND PatientInsuranceId IS NOT NULL

--capture all the claims that are open to the patient
UPDATE ir
SET OpenForReview = CONVERT(BIT,1)
FROM model.InvoiceReceivables ir
JOIN model.Invoices i ON i.Id = ir.InvoiceId
JOIN model.Encounters e ON e.Id = i.EncounterId
JOIN PatientReceivablesBackup pr ON pr.PatientId = e.PatientId AND pr.AppointmentId = e.Id
WHERE pr.ReceivableType = 'I'
AND PatientInsuranceId IS NULL
AND InvoiceId NOT IN (SELECT InvoiceId FROM model.InvoiceReceivables WHERE PatientInsuranceId IS NOT NULL)
AND pr.OpenBalance > 0

UPDATE pi
SET pi.EndDateTime = ip.StartDateTime
FROM model.patientinsurances pi
INNER JOIN model.insurancepolicies ip ON pi.InsurancePolicyId = ip.id
WHERE pi.EndDateTime < ip.StartDateTime
	AND IsDeleted <> 1

UPDATE ir
SET LegacyInsurerDesignation = CASE WHEN z.Id IS NOT NULL THEN 'T' ELSE NULL END
FROM model.InvoiceReceivables ir
LEFT JOIN (
	SELECT 
	v.Id
	,v.InvoiceId
	FROM (
			SELECT
			ROW_NUMBER() OVER (PARTITION BY pi.InsuranceTypeId, ir.InvoiceId
				ORDER BY pi.InsuranceTypeId, pi.OrdinalId, PolicyHolderRelationshipTypeId) as rn
			,ir.InvoiceId
			,pi.Id
			FROM model.InvoiceReceivables ir
			INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
			INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
				AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= i.DateTime)
				AND pi.IsDeleted = 0
			INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
				AND ip.StartDateTime <= i.DateTime
			WHERE PatientInsuranceId IS NOT NULL	
	) v WHERE v.rn = 1
)z ON z.InvoiceId = ir.InvoiceId
	AND z.Id = ir.PatientInsuranceId