﻿CREATE TRIGGER ServiceTransactionsDelete ON dbo.ServiceTransactions
INSTEAD OF DELETE
AS
BEGIN

	DELETE bst
	FROM model.BillingServiceTransactions bst
	INNER JOIN deleted d ON d.Id = bst.Id

END