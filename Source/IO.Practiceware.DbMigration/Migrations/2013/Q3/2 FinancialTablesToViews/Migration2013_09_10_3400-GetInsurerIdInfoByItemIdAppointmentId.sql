﻿IF OBJECT_ID('dbo.GetInsurerIdInfoByItemIdAppointmentId', 'P') IS NOT NULL
	DROP PROCEDURE dbo.GetInsurerIdInfoByItemIdAppointmentId;
GO

CREATE PROCEDURE dbo.GetInsurerIdInfoByItemIdAppointmentId 
	@AppointmentId INT
	,@ItemId INT
AS
BEGIN
SET NOCOUNT ON;

SELECT Id INTO #InvoiceReceivableId FROM dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId)

;WITH bstsExistsForInvoiceId  AS (
	SELECT 
	ir.InvoiceId
	,COUNT(*) AS NumOccurances
	FROM model.BillingServiceTransactions bst
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId
	GROUP BY ir.InvoiceId
)
SELECT
ir.Id AS ReceivableId
,CASE
	WHEN ir.OpenForReview = 0
		THEN 
			CASE
				WHEN bsti.NumOccurances IS NOT NULL
					THEN 'B'
				ELSE 'S'
			END
	WHEN ir.OpenForReview = 1
		THEN 'I'
END AS ReceivableType
,i.LegacyPatientReceivablesPatientId AS PatientId
,ip.PolicyHolderPatientId AS InsuredId
,ip.InsurerId
,i.LegacyPatientReceivablesAppointmentId AS AppointmentId
,i.LegacyPatientReceivablesInvoice AS Invoice
,pins.InsurerId
,ptj.*
,bst.Id
,bst.LegacyTransactionId AS TransactionId
,bst.BillingServiceId AS ServiceId
,ptj.TransactionDate AS TransactionDate
,bst.AmountSent AS Amount
,CASE  
	WHEN (bst.BillingServiceTransactionStatusId = 1 AND ptj.TransactionStatus = 'P')
		THEN CASE 
				WHEN bst.AmountSent > 0
					THEN 'B' 
				ELSE 'P' 
			END
	WHEN (bst.BillingServiceTransactionStatusId = 3 AND ptj.TransactionStatus IN ('P','S'))
		THEN 'V'
	WHEN (bst.BillingServiceTransactionStatusId = 2 AND ptj.TransactionStatus IN ('P','S'))
		THEN 'P'
	WHEN (bst.BillingServiceTransactionStatusId = 4 AND ptj.TransactionStatus = 'S')
		THEN 'X'
	WHEN (bst.BillingServiceTransactionStatusId = 6 AND ptj.TransactionStatus = 'Z')
		THEN '1'
	WHEN (bst.BillingServiceTransactionStatusId = 6 AND ptj.TransactionStatus = 'S')
		THEN '1'
	WHEN (bst.BillingServiceTransactionStatusId = 7 AND ptj.TransactionStatus = 'Z')
		THEN '2'
	ELSE '0'
END AS TransportAction
,bst.DateTime AS ModTime
,CONVERT(UNIQUEIDENTIFIER,NULL) AS ExternalSystemMessageId
FROM model.InvoiceReceivables ir
INNER JOIN #InvoiceReceivableId irs ON irs.Id = ir.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
LEFT JOIN bstsExistsForInvoiceId bsti ON bsti.InvoiceId = i.Id
INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
INNER JOIN dbo.PracticeInsurers pins ON pins.InsurerId = ip.InsurerId
INNER JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionTypeId = irs.Id
	AND ptj.TransactionType = 'R'
	AND ptj.TransactionRef <> 'G'
	AND ptj.TransactionStatus <> 'Z'
INNER JOIN model.BillingServiceTransactions bst ON bst.InvoiceReceivableId = ir.Id
	AND bst.LegacyTransactionId = ptj.TransactionId
WHERE bst.BillingServiceId = @ItemId
ORDER BY ptj.TransactionId

END
GO