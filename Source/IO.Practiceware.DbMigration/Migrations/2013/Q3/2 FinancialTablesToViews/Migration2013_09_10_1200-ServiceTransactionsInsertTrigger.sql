﻿CREATE TRIGGER ServiceTransactionsInsert ON dbo.ServiceTransactions
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO model.BillingServiceTransactions(
	[DateTime]
	,AmountSent
	,ClaimFileNumber
	,BillingServiceTransactionStatusId
	,MethodSentId
	,BillingServiceId
	,InvoiceReceivableId
	,ClaimFileReceiverId
	,ExternalSystemMessageId
	,PatientAggregateStatementId
	,LegacyTransactionId)
	SELECT
	(DATEADD(SECOND, DATEPART(HOUR,GETDATE()) * 3600 + 
	DATEPART(MINUTE,GETDATE()) * 60 + 
    DATEPART(SECOND,GETDATE()),
	CONVERT(DATETIME,i.TransactionDate))
	                    ) AS [DateTime]
	,i.Amount AS AmountSent
	,NULL AS ClaimFileNumber
	,CASE 
		WHEN ptj.TransactionStatus = 'P'
			AND i.TransportAction IN ('B', 'P')
			THEN 1
		WHEN ptj.TransactionStatus = 'P'
			AND i.TransportAction = 'V'
			THEN 3
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'P'
			THEN 2
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'V'
			THEN 3
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'X'
			THEN 4
		WHEN ptj.TransactionStatus = 'Z'
			AND i.TransportAction = '1'
			THEN 6
		WHEN ptj.TransactionStatus = 'Z'
			AND i.TransportAction = '2'
			THEN 7
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'B'
			THEN 8
		ELSE 5
	END AS BillingServiceTransactionStatusId
	,CASE 
		WHEN (ptj.TransactionRef = 'I' AND ptj.TransactionAction = 'B')
			THEN 1
		ELSE 2 
	END AS MethodSentId
	,i.ServiceId AS BillingServiceId
	,ptj.TransactionTypeId AS InvoiceReceivableId
	,CASE 
		WHEN ptj.TransactionRef = 'I' AND ptj.TransactionAction = 'B' 
			THEN pctMethod.Id 
		ELSE NULL 
	END AS ClaimFileReceiverId
	,CONVERT(UNIQUEIDENTIFIER,i.ExternalSystemMessageId) AS ExternalSystemMessageId
	,NULL AS PatientAggregateStatementId
	,i.TransactionId AS LegacyTransactionId
	FROM inserted i 
	INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionId = i.TransactionId
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = ptj.TransactionTypeId
	LEFT JOIN model.PatientInsurances pi ON ir.PatientInsuranceId = pi.Id
	LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	LEFT JOIN dbo.PracticeInsurers pri on pri.InsurerId = ip.InsurerId
	LEFT JOIN dbo.PracticeCodeTable pctMethod ON pri.InsurerTFormat = SUBSTRING(pctMethod.Code, 1, 1) 
		AND pctMethod.ReferenceType = 'TRANSMITTYPE'
END