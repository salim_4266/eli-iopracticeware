﻿--To be run after FinancialBatches and InvoiceReceviables

EXEC dbo.DropObject 'model.Adjustments'
GO

-- Creating table 'Adjustments'
CREATE TABLE [model].[Adjustments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FinancialSourceTypeId] int  NOT NULL,
    [Amount] decimal(18,2)  NOT NULL,
    [PostedDateTime] datetime  NOT NULL,
    [AdjustmentTypeId] int  NOT NULL,
    [BillingServiceId] int  NULL,
    [InvoiceReceivableId] bigint  NULL,
    [PatientId] int  NULL,
    [InsurerId] int  NULL,
    [FinancialBatchId] int  NULL,
    [PaymentMethodId] int  NULL,
    [ClaimAdjustmentGroupCodeId] int  NULL,
    [ClaimAdjustmentReasonCodeId] int  NULL,
	[Comment] nvarchar(max) NULL,
	[OldId] int NULL,
	[IncludeCommentOnStatement] bit NOT NULL,
	[LegacyInvoiceReceivableId] INT NULL
);
GO

INSERT INTO model.Adjustments (FinancialSourceTypeId, Amount, PostedDateTime, AdjustmentTypeId,BillingServiceId, InvoiceReceivableId, PatientId, InsurerId, FinancialBatchId, PaymentMethodId, ClaimAdjustmentGroupCodeId, ClaimAdjustmentReasonCodeId, OldId, IncludeCommentOnStatement)
SELECT
	FinancialSourceTypeId,
	Amount, 
	PostedDateTime, 
	AdjustmentTypeId,
	BillingServiceId,
	InvoiceReceivableId,
	PatientId, 
	InsurerId, 
	FinancialBatchId, 
	PaymentMethodId, 
	ClaimAdjustmentGroupCodeId, 
	ClaimAdjustmentReasonCodeId,
	Id,
	CASE WHEN PaymentCommentOn = 'T' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END AS IncludeCommentOnStatement
FROM dbo.AdjustmentsBackup

IF OBJECT_ID('tempdb..#Comments') IS NOT NULL 
	DROP TABLE #Comments
GO

SELECT
AdjustmentId
,Value
INTO #Comments
FROM model.Comments
WHERE AdjustmentId IS NOT NULL

UPDATE adj
SET adj.Comment = com.Value
FROM model.Adjustments adj
JOIN #Comments com ON com.AdjustmentId = adj.OldId

-- Creating primary key on [Id] in table 'Adjustments'
ALTER TABLE [model].[Adjustments]
ADD CONSTRAINT [PK_Adjustments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

--Update FK references
UPDATE ad 
SET ad.InvoiceReceivableId = ir.Id
FROM model.Adjustments ad
INNER JOIN model.InvoiceReceivables ir ON ad.InvoiceReceivableId = ir.ComputedInvoiceReceivableId

UPDATE ad
SET ad.FinancialBatchId = fb.Id
FROM model.Adjustments ad
INNER JOIN model.FinancialBatches fb ON ad.FinancialBatchId = fb.ComputedFinancialBatchId
GO

SELECT
*
INTO #DeletedAdjustments
FROM model.Adjustments
WHERE InvoiceReceivableId NOT IN (SELECT Id FROM model.InvoiceReceivables)

UPDATE adj
SET adj.InvoiceReceivableId = ir.Id
FROM model.Adjustments adj
INNER JOIN dbo.PatientReceivableServices prs ON prs.ItemId = adj.BillingServiceId
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = prs.AppointmentId
INNER JOIN model.Invoices i ON i.EncounterId = ap.AppointmentId
INNER JOIN #DeletedAdjustments df ON df.BillingServiceId = prs.ItemId
INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id

IF EXISTS(SELECT TOP 1 Id FROM model.Adjustments WHERE InvoiceReceivableId NOT IN (SELECT Id FROM model.InvoiceReceivables))
BEGIN
	RAISERROR('There are Adjustments.InvoiceReceivableId that are not in (SELECT Id FROM model.InvoiceReceivables)... Invalid foreign keys.',16,2) WITH SETERROR
END

ALTER TABLE [model].[Adjustments]
ALTER COLUMN InvoiceReceivableId INT NULL
GO

UPDATE adj
SET [LegacyInvoiceReceivableId] = dbo.GetLegacyInvoiceReceivableId(adj.InvoiceReceivableId)
FROM model.Adjustments adj
WHERE adj.InvoiceReceivableId IS NOT NULL

UPDATE adj
SET [LegacyInvoiceReceivableId] = dbo.GetAdjustmentIdAndConvertAsReceivableId(adj.Id)
FROM model.Adjustments adj
WHERE adj.InvoiceReceivableId IS NULL

-- Creating foreign key on [InvoiceReceivableId] in table 'Adjustments'
ALTER TABLE [model].[Adjustments]
ADD CONSTRAINT [FK_InvoiceReceivableAdjustment]
    FOREIGN KEY ([InvoiceReceivableId])
    REFERENCES [model].[InvoiceReceivables]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InvoiceReceivableAdjustment'
CREATE INDEX [IX_FK_InvoiceReceivableAdjustment]
ON [model].[Adjustments]
    ([InvoiceReceivableId]);
GO

-- Creating foreign key on [FinancialBatchId] in table 'Adjustments'
ALTER TABLE [model].[Adjustments]
ADD CONSTRAINT [FK_FinancialBatchAdjustment]
    FOREIGN KEY ([FinancialBatchId])
    REFERENCES [model].[FinancialBatches]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_FinancialBatchAdjustment'
CREATE INDEX [IX_FK_FinancialBatchAdjustment]
ON [model].[Adjustments]
    ([FinancialBatchId]);
GO

--If the count of Adjustments in the PatientReceivablePaymets table are not within 1% of the model.Adjustments table
-- throw an error and abort the migration.
IF (
		(
			SELECT (
					CONVERT(DECIMAL(18,2),Count(*)) / (CONVERT(DECIMAL(18,2),(
						SELECT Count(*)
						FROM Model.Adjustments)
						)) * 100
					)
			FROM dbo.PatientReceivablePayments
			WHERE PaymentFinancialType <> ''
				AND PaymentFinancialType IS NOT NULL
				AND PaymentType <> ''
				AND PaymentType IS NOT NULL
		) < 1
	)
BEGIN
	RAISERROR ('Count of Adjustments in the PatientReceivablePaymets table are not within 1% of the model.Adjustments.', 16, 1);
END