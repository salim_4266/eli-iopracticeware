﻿IF OBJECT_ID ( 'dbo.GetInsurerDisbursementDetails', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetInsurerDisbursementDetails;
GO

CREATE PROCEDURE dbo.GetInsurerDisbursementDetails
	@PaymentCheck NVARCHAR(200)
	,@EOBDate NVARCHAR(10)
	,@PaymentBatchId INT
AS
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#LocateChecks') IS NOT NULL
BEGIN
	DROP TABLE #LocateChecks
END

SELECT 
fb.Id
,fb.FinancialSourceTypeId
,fb.PatientId
,fb.InsurerId
,fb.PaymentDateTime
,fb.ExplanationOfBenefitsDateTime
,fb.Amount
,CONVERT(NVARCHAR(MAX),fb.CheckCode) AS CheckCode
INTO #LocateChecks
FROM model.FinancialBatches fb
WHERE CheckCode = @PaymentCheck
	AND CONVERT(DATETIME,ExplanationOfBenefitsDateTime,112) = CONVERT(DATETIME,@EOBDate,112)
	AND fb.Id = @PaymentBatchId
	
IF (@@ROWCOUNT > 0)
BEGIN

SELECT * FROM (
SELECT
adj.Id AS PaymentId
,p.Id AS PatientId
,p.LastName
,p.FirstName
,pins.InsurerName
,adj.Amount AS PaymentAmount
,CONVERT(NVARCHAR,adj.PostedDateTime,112) AS PaymentDate
,CASE 
	WHEN adj.AdjustmentTypeId = 1
		THEN 'Payment'
	WHEN adj.AdjustmentTypeId = 2
		THEN 'Cont Adj'
	ELSE 'Other'
END AS PaymentType
FROM #LocateChecks lc
INNER JOIN model.Adjustments adj ON lc.Id = adj.FinancialBatchId
INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN Appointments ap ON ap.AppointmentId = i.EncounterId
INNER JOIN model.Patients p ON p.Id = ap.PatientId
INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
INNER JOIN PracticeInsurers pins ON pins.InsurerId = ip.InsurerId

UNION ALL 

SELECT
fi.Id AS PaymentId
,p.Id AS PatientId
,p.LastName
,p.FirstName
,pins.InsurerName
,fi.Amount AS PaymentAmount
,CONVERT(NVARCHAR,fi.PostedDateTime,112) AS PaymentDate
,CASE
	WHEN fi.FinancialInformationTypeId = 1
		THEN 'Deduct'
	WHEN fi.FinancialInformationTypeId = 2
		THEN 'CoIns'
	ELSE 'Other'
END AS PaymentType
FROM #LocateChecks lc
INNER JOIN model.FinancialInformations fi ON lc.Id = fi.FinancialBatchId
INNER JOIN model.InvoiceReceivables ir ON ir.Id = fi.InvoiceReceivableId
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN Appointments ap ON ap.AppointmentId = i.EncounterId
INNER JOIN model.Patients p ON p.Id = ap.PatientId
INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
INNER JOIN PracticeInsurers pins ON pins.InsurerId = ip.InsurerId
)v
ORDER BY v.LastName,v.FirstName
END
GO