﻿IF OBJECT_ID(N'dbo.GetLegacyInvoiceReceivableId', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GetLegacyInvoiceReceivableId
GO

CREATE FUNCTION dbo.GetLegacyInvoiceReceivableId (@InvoiceReceivableId INT)
RETURNS INT
AS
BEGIN
	DECLARE @ReturnInvoiceReceivableId INT
		
	SELECT TOP 1 @ReturnInvoiceReceivableId = irb.Id
	FROM model.InvoiceReceivables ira
	INNER JOIN model.InvoiceReceivables irb ON irb.InvoiceId = ira.InvoiceId
		AND ira.PatientInsuranceId IS NULL
	INNER JOIN model.Invoices i ON i.Id = ira.InvoiceId
	INNER JOIN model.PatientInsurances pi ON pi.Id = irb.PatientInsuranceId
		AND (
			pi.EndDateTime IS NULL
			OR i.DateTime <= pi.EndDateTime
			)
		AND pi.IsDeleted = 0
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		AND i.DateTime >= ip.StartDateTime
	WHERE ira.Id = @InvoiceReceivableId	
	ORDER BY pi.OrdinalId ASC
	
	IF (@ReturnInvoiceReceivableId IS NULL)	SET @ReturnInvoiceReceivableId = @InvoiceReceivableId

	RETURN @ReturnInvoiceReceivableId
END
GO

IF OBJECT_ID(N'dbo.GetPrimaryInsuranceId', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GetPrimaryInsuranceId
GO

CREATE FUNCTION dbo.GetPrimaryInsuranceId (@InvoiceId INT)
RETURNS INT
AS
BEGIN
DECLARE @ReturnPatientInsuranceId INT

SELECT TOP 1
@ReturnPatientInsuranceId = ir.PatientInsuranceId
FROM model.Invoices i
INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
INNER JOIN dbo.Appointments ap ON ap.EncounterId = i.EncounterId
	AND ap.AppointmentInsuranceTypeId = pi.InsuranceTypeId
WHERE i.Id = @InvoiceId
	AND pi.IsDeleted = 0
	AND (pi.EndDateTime IS NULL 
	OR pi.EndDateTime = '' 
	OR pi.EndDateTime >= i.DateTime)
	AND ip.StartDateTime <= i.DateTime
ORDER BY pi.OrdinalId ASC

RETURN @ReturnPatientInsuranceId
END
GO

IF OBJECT_ID(N'dbo.GetAdjustmentIdAndConvertAsReceivableId', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GetAdjustmentIdAndConvertAsReceivableId
GO 

CREATE FUNCTION dbo.GetAdjustmentIdAndConvertAsReceivableId (@AdjustmentId INT)
RETURNS INT
AS
BEGIN
	DECLARE @ReturnAdjustmentId INT
		
	SELECT
	@ReturnAdjustmentId = (1 * 1000000000 + adj.Id)
	FROM model.Adjustments adj
	WHERE adj.Id = @AdjustmentId
		AND adj.InvoiceReceivableId IS NULL
			
	RETURN @ReturnAdjustmentId
END
GO