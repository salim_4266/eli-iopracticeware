﻿
-- Change column name on invoice for readability and clarity

IF EXISTS(select * from sys.columns where Name = N'DoesProviderRefuseAssignment' and Object_ID = Object_ID(N'model.Invoices')) 
BEGIN
	-- change column names. 
	EXEC sp_rename '[model].[Invoices].[DoesProviderRefuseAssignment]' , 'IsAssignmentRefused', 'COLUMN';
END

GO
