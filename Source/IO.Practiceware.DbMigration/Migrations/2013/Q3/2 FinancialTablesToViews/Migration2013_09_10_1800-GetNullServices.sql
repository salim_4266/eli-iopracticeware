﻿IF OBJECT_ID ( 'dbo.GetNullServices', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetNullServices;
GO

CREATE PROCEDURE dbo.GetNullServices
	@Invoice NVARCHAR(100)
AS
SET NOCOUNT ON;

SELECT 
DISTINCT ptj.TransactionDate
,ptj.TransactionStatus
,ptj.TransactionRef
,ptj.transactionaction
,SUBSTRING(ptj.TransactionRemark, 6, 9) AS Amount
,st.TransportAction
,CASE ptj.TransactionRef
	WHEN 'I'
		THEN InsurerName
	WHEN 'P'
		THEN 'PATIENT'
	WHEN 'G'
		THEN 'MONTLY STMT'
	ELSE ''
END AS InsurerName
FROM PatientReceivables pr
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
INNER JOIN PatientReceivableServices prs ON prs.Invoice = pr.Invoice
LEFT OUTER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
LEFT OUTER JOIN ServiceTransactions st ON st.TransactionId = ptj.TransactionId
	AND st.ServiceId = prs.ItemId
WHERE ptj.TransactionType = 'R'
	AND ServiceId IS NULL
	AND pr.Invoice = @Invoice
	AND st.TransportAction IS NOT NULL
ORDER BY ptj.TransactionDate DESC

GO



--OLD QUERY
--select distinct
--ptj.TransactionId, ptj.TransactionDate, ptj.TransactionStatus, ptj.TransactionRef, ptj.transactionaction,
--st.Amount, st.TransportAction,
--case ptj.TransactionRef
--    when 'I' then InsurerName
--    when 'P' then 'PATIENT'
--    when 'G' then 'MONTLY STMT'
--    Else ''
--    end  as InsurerName
--from PatientReceivables pr
--inner join PracticeTransactionJournal ptj on pr.ReceivableId = ptj.TransactionTypeId
--inner join PatientReceivableServices prs on prs.Invoice = pr.Invoice
--left outer join PracticeInsurers pri on pri.InsurerId = pr.InsurerId
--inner join ServiceTransactions st on st.TransactionId = ptj.TransactionId and st.ServiceId = prs.ItemId
--where ptj.TransactionType = 'R'
--and  prs.ItemId in (296741)
--order by ptj.TransactionDate desc, ptj.TransactionId desc