﻿IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'Invoices' AND sc.Name = 'PatientSignatureSourceCodeId')
BEGIN
	-- Add Column
	ALTER TABLE model.Invoices
	ADD PatientSignatureSourceCodeId int NULL
END
GO
