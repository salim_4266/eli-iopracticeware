﻿IF OBJECT_ID ( 'dbo.GetPatientReceivableTotalPaid', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetPatientReceivableTotalPaid;
GO

CREATE PROCEDURE dbo.GetPatientReceivableTotalPaid
	@ReceivableId NVARCHAR(100)
AS
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#AdjustmentTypes') IS NOT NULL
BEGIN
	DROP TABLE #AdjustmentTypes
END

SELECT 
v.Id AS Id
,v.PaymentType
,v.IsDebit AS IsDebit
,v.ShortName AS ShortName
INTO #AdjustmentTypes
FROM (
	SELECT 
		CASE
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'P'
				THEN 1
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'X'
				THEN 2
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '8'
				THEN 3
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'R'
				THEN 4
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'U'
				THEN 5
			ELSE pct.Id + 1000
		END AS Id
		,SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS PaymentType
		,CASE pct.AlternateCode
			WHEN 'D'
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
		END AS IsDebit
		,LetterTranslation AS ShortName
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'
	GROUP BY 
	Id
	,Code
	,pct.AlternateCode
	,LetterTranslation
) AS v

SELECT 
adj.Amount AS PaymentAmount
,at.PaymentType
,CASE at.IsDebit
	WHEN 1 THEN 'D'
	ELSE 'C'
END AS PaymentFinancialType
FROM model.Adjustments adj 
INNER JOIN #AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
WHERE adj.InvoiceReceivableId = @ReceivableId
	AND at.IsDebit IN (1,0)

GO