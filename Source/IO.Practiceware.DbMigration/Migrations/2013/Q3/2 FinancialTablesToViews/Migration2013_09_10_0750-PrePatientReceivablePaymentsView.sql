﻿IF EXISTS (
SELECT
pct.Code
FROM PracticeCodeTable pct
WHERE ReferenceType = 'PAYMENTTYPE'
AND LEN(SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))) <> 1
)
BEGIN
	UPDATE pct
	SET pct.Code = REVERSE(SUBSTRING(REVERSE(pct.Code),CHARINDEX('- ',REVERSE(pct.Code)),LEN(REVERSE(pct.Code))))+' '+SUBSTRING(REVERSE(pct.Code),0,CHARINDEX('-',REVERSE(pct.Code)))
	FROM PracticeCodeTable pct
	WHERE ReferenceType = 'PAYMENTTYPE'
	AND ([IO.Practiceware.SqlClr].SearchRegexPattern(pct.Code,'\-\s')) <> '' 
	AND ([IO.Practiceware.SqlClr].SearchRegexPattern(pct.Code,'\-\s')) IS NOT NULL
	AND LEN(SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX(' - ', pct.Code)+3), 0)), LEN(pct.Code))) <> 1
END

EXEC DropObject PatientReceivablePayments
GO

ALTER TABLE model.Adjustments 
ADD LegacyPaymentId AS (
	CASE 
		WHEN InvoiceReceivableId IS NOT NULL 
			THEN (1 * 11000000 + Id)
		WHEN InvoiceReceivableId IS NULL 
			THEN (7 * 11000000 + Id)
	END
) PERSISTED
GO

CREATE INDEX IX_Adjustments_LegacyPaymentId ON model.Adjustments(LegacyPaymentId);
GO

ALTER TABLE model.FinancialInformations
ADD LegacyPaymentId AS (2 * 11000000 + Id)
PERSISTED
GO

CREATE INDEX IX_FinancialInformations_LegacyPaymentId ON model.FinancialInformations(LegacyPaymentId);
GO

IF EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'EncounterServices' and schemas.name = 'model') 
BEGIN
	EXEC dbo.DropObject 'model.EncounterServices'
END
GO

EXEC('
	CREATE VIEW [model].[EncounterServices]
	WITH SCHEMABINDING
	AS
	SELECT CodeId AS Id,
	ps.Code AS Code,
	CONVERT(decimal(18,2), 1) AS DefaultUnit,
	Description AS Description,
	pct.Id AS EncounterServiceTypeId,
	CASE WHEN EndDate = '''' THEN NULL ELSE CONVERT(datetime, EndDate, 112) END AS EndDateTime,
	CASE ConsultOn
		WHEN ''T''
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
		END AS IsConsultation,
	CONVERT(bit, 1) AS IsDoctorInvoice,
	CASE 
		WHEN SUBSTRING(ps.Code, 1, 1) = ''J''
			OR ps.CodeCategory LIKE ''%inject%''
			OR ps.CodeCategory LIKE ''%drug%''
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
		END AS IsDrug,
	CASE ps.POS
		WHEN ''98''
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
		END AS IsFacilityInvoice,
	CASE 
		WHEN len(ClaimNote) > 0
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
		END as IsUnclassified,
	CASE OrderDoc
		WHEN ''T''
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
		END AS IsOrderingProvider,
	CASE ServiceFilter
		WHEN ''F''
			THEN CONVERT(bit, 0)
		ELSE CONVERT(bit, 1)
		END AS IsZeroCharge,
	CONVERT(decimal(18,2), RelativeValueUnit) AS RelativeValueUnit,
	ps.RevenueCodeId AS RevenueCodeId,
	CASE 
		WHEN ps.CodeCategory = ''ANESTHESIA''
			THEN 1
		ELSE 2
		END AS ServiceUnitOfMeasurementId,
	CONVERT(datetime, StartDate, 112) AS StartDateTime,
	CONVERT(decimal(18,2), Fee) AS UnitFee,
	CONVERT(bit, 0) AS IsArchived,
	ps.IsCliaWaived AS IsCliaWaived
FROM dbo.PracticeServices ps
LEFT JOIN dbo.PracticeCodeTable pct ON pct.Code = ps.CodeCategory
	AND pct.ReferenceType = ''SERVICECATEGORY''
')

IF EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'PaymentMethods' and schemas.name = 'model') 
BEGIN
	EXEC dbo.DropObject 'model.PaymentMethods'
END
GO

EXEC('CREATE VIEW [model].[PaymentMethods]
	WITH SCHEMABINDING
	AS
	SELECT Id,
		Code AS [Name],
		CONVERT(bit, 0) AS IsArchived
	FROM dbo.PracticeCodeTable
	WHERE ReferenceType = ''PAYABLETYPE''
')
GO