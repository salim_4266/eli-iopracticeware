﻿IF OBJECT_ID('dbo.GetReceivableIdInsurerIdInsurerDesignationByPatientIdInvoiceDateServiceModifier', 'P') IS NOT NULL
	DROP PROCEDURE dbo.GetReceivableIdInsurerIdInsurerDesignationByPatientIdInvoiceDateServiceModifier;
GO

CREATE PROCEDURE dbo.GetReceivableIdInsurerIdInsurerDesignationByPatientIdInvoiceDateServiceModifier 
	@PatientId INT
	,@InvoiceDate NVARCHAR(8)
	,@Service NVARCHAR(MAX)
	,@Modifier NVARCHAR(MAX)
AS
BEGIN

SELECT 
ir.Id AS ReceivableId
,pins.InsurerId
,COALESCE(ir.LegacyInsurerDesignation,'F') AS InsurerDesignation
FROM model.InvoiceReceivables ir 
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
LEFT JOIN dbo.PracticeInsurers pins ON pins.InsurerId = ip.InsurerId
LEFT JOIN dbo.PatientReceivableServices prs ON prs.Invoice = i.LegacyPatientReceivablesInvoice
WHERE ir.InvoiceId IN (
	SELECT 
	i.Id 
	FROM model.Invoices i 
	WHERE i.EncounterId IN (
		SELECT 
		DISTINCT ap.EncounterId 
		FROM dbo.Appointments ap 
		WHERE ap.PatientId = @PatientId
	)
	AND CONVERT(NVARCHAR,i.DateTime,112) = @InvoiceDate
)
AND prs.[Service] = @Service
AND prs.Modifier = @Modifier
AND prs.[Status] = 'A'
END
GO