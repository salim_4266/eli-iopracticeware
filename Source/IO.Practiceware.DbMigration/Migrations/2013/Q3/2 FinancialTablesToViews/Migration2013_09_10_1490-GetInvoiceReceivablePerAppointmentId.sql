﻿IF OBJECT_ID(N'dbo.GetInvoiceReceivablePerAppointmentId', N'TF') IS NOT NULL
    DROP FUNCTION dbo.GetInvoiceReceivablePerAppointmentId;
GO

CREATE FUNCTION dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId INT)
RETURNS @InvoiceReceivables TABLE
(
Id INT PRIMARY KEY NOT NULL
)
AS
BEGIN
	DECLARE @EncounterId INT
	DECLARE @IsASCClaim BIT

	SELECT 
	@EncounterId = ap.EncounterId
	,@IsASCClaim = CASE 
		WHEN @AppointmentId <> ap.EncounterId AND ap.Comments = 'ASC CLAIM'
			THEN CONVERT(BIT, 1)
		ELSE CONVERT(BIT,0)
	END
	FROM dbo.Appointments ap 
	WHERE ap.AppointmentId = @AppointmentId

	IF(@IsASCClaim = 1)
	BEGIN
		INSERT INTO @InvoiceReceivables (Id)
		SELECT
		ir.Id
		FROM model.Invoices i 
		INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
		WHERE i.EncounterId = @EncounterId
		AND i.InvoiceTypeId = 2
	END
	ELSE
	BEGIN
		INSERT INTO @InvoiceReceivables (Id)
		SELECT
		ir.Id
		FROM model.Invoices i 
		INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
		WHERE i.EncounterId = @EncounterId
		AND i.InvoiceTypeId <> 2
	END

RETURN
END
GO