﻿-- Migration to turn InsurerAddresses, InsurerPhoneNumbers, and InsurerPhoneNumberTypes into tables
-- InsurerAddress

-- create the views if they do not exist
--InsurerAddresses
IF OBJECT_ID('model.InsurerAddresses') IS NULL
BEGIN
	EXEC dbo.sp_executesql @statement = N'
CREATE VIEW model.InsurerAddresses AS
SELECT model.GetBigPairId(15, InsurerId, 110000000) AS Id,
	pri.InsurerCity AS City,
	pri.InsurerId AS InsurerId,
	pri.Insureraddress AS Line1,
	CONVERT(NVARCHAR, NULL) AS Line2,
	CONVERT(NVARCHAR, NULL) AS Line3,
	REPLACE(pri.InsurerZip,''-'','''') AS PostalCode,
	st.Id AS StateOrProvinceId,
	3 AS InsurerAddressTypeId
FROM dbo.PracticeInsurers pri
INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pri.InsurerState
WHERE pri.InsurerAddress <> ''''
	AND pri.InsurerAddress IS NOT NULL'
END
GO

-- InsurerPhoneNumber
IF OBJECT_ID('model.InsurerPhoneNumbers', 'V') IS NOT NULL
BEGIN
	DROP VIEW model.InsurerPhoneNumbers
END

GO 
IF OBJECT_ID('model.InsurerPhoneNumbers') IS NULL
EXEC('
	CREATE VIEW model.InsurerPhoneNumbers AS
	SELECT model.GetPairId(25, InsurerId, 55000000) AS Id
		,CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE WHEN LEN(InsurerPhone) = 10
				THEN SUBSTRING(InsurerPhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE WHEN LEN(InsurerPhone) = 10
				THEN SUBSTRING(InsurerPhone, 4, 7)
			ELSE InsurerPhone
			END AS ExchangeAndSuffix
		,CONVERT(NVARCHAR, NULL) AS Extension
		,InsurerId AS InsurerId
		,CONVERT(bit, 0) AS IsInternational
		,1 AS OrdinalId
		,8 AS InsurerPhoneNumberTypeId
	FROM dbo.PracticeInsurers
	WHERE InsurerPhone <> ''''
		AND InsurerPhone IS NOT NULL

	UNION ALL

	SELECT model.GetPairId(26, InsurerId, 55000000) AS Id
		,CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE WHEN LEN(InsurerPrecPhone) = 10
				THEN SUBSTRING(InsurerPrecPhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE WHEN LEN(InsurerPrecPhone) = 10
				THEN SUBSTRING(InsurerPrecPhone, 4, 7)
			ELSE InsurerPrecPhone
			END AS ExchangeAndSuffix
		,CONVERT(NVARCHAR, NULL) AS Extension
		,InsurerId AS InsurerId
		,CONVERT(bit, 0) AS IsInternational
		,2 AS OrdinalId
		,pct.Id + 1000 AS InsurerPhoneNumberTypeId
	FROM dbo.PracticeInsurers
	LEFT OUTER JOIN dbo.PracticeCodeTable pct 
		ON pct.Code = ''Authorization''
		AND pct.ReferenceType = ''PHONETYPEINSURER''
	WHERE InsurerPrecPhone <> ''''
		AND InsurerPrecPhone IS NOT NULL

	UNION ALL

	SELECT model.GetPairId(27, InsurerId, 55000000) AS Id
		,CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE WHEN LEN(InsurerEligPhone) = 10
				THEN SUBSTRING(InsurerEligPhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE WHEN LEN(InsurerEligPhone) = 10
				THEN SUBSTRING(InsurerEligPhone, 4, 7)
			ELSE InsurerEligPhone
			END AS ExchangeAndSuffix
		,CONVERT(NVARCHAR, NULL) AS Extension
		,InsurerId AS InsurerId
		,CONVERT(bit, 0) AS IsInternational
		,3 AS OrdinalId
		,pct.Id + 1000 AS InsurerPhoneNumberTypeId
	FROM dbo.PracticeInsurers
	LEFT OUTER JOIN dbo.PracticeCodeTable pct 
		ON pct.Code = ''Eligibility''
		AND pct.ReferenceType = ''PHONETYPEINSURER''
	WHERE InsurerEligPhone <> ''''
		AND InsurerEligPhone IS NOT NULL

	UNION ALL

	SELECT model.GetPairId(28, InsurerId, 55000000) AS Id
		,CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE WHEN LEN(InsurerProvPhone) = 10
				THEN SUBSTRING(InsurerProvPhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE WHEN LEN(InsurerProvPhone) = 10
				THEN SUBSTRING(InsurerProvPhone, 4, 7)
			ELSE InsurerProvPhone
			END AS ExchangeAndSuffix
		,CONVERT(NVARCHAR, NULL) AS Extension
		,InsurerId AS InsurerId
		,CONVERT(bit, 0) AS IsInternational
		,4 AS OrdinalId
		,pct.Id + 1000 AS InsurerPhoneNumberTypeId
	FROM dbo.PracticeInsurers
	LEFT OUTER JOIN dbo.PracticeCodeTable pct 
		ON pct.Code = ''Provider''
		AND pct.ReferenceType = ''PHONETYPEINSURER''
	WHERE InsurerProvPhone <> ''''
		AND InsurerProvPhone IS NOT NULL

	UNION ALL

	SELECT model.GetPairId(29, InsurerId, 55000000) AS Id
		,CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE WHEN LEN(InsurerClaimPhone) = 10
				THEN SUBSTRING(InsurerClaimPhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE WHEN LEN(InsurerClaimPhone) = 10
				THEN SUBSTRING(InsurerClaimPhone, 4, 7)
			ELSE InsurerClaimPhone
			END AS ExchangeAndSuffix
		,CONVERT(NVARCHAR, NULL) AS Extension
		,InsurerId AS InsurerId
		,CONVERT(bit, 0) AS IsInternational
		,5 AS OrdinalId
		,pct.Id + 1000 AS InsurerPhoneNumberTypeId
	FROM dbo.PracticeInsurers
	LEFT OUTER JOIN dbo.PracticeCodeTable pct 
		ON pct.Code = ''Claims''
		AND pct.ReferenceType = ''PHONETYPEINSURER''
	WHERE InsurerClaimPhone <> ''''
		AND InsurerClaimPhone IS NOT NULL

	UNION ALL

	SELECT model.GetPairId(30, InsurerId, 55000000) AS Id
		,CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE WHEN LEN(InsurerENumber) = 10
				THEN SUBSTRING(InsurerENumber, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE WHEN LEN(InsurerENumber) = 10
				THEN SUBSTRING(InsurerENumber, 4, 7)
			ELSE InsurerENumber
			END AS ExchangeAndSuffix	
		,CONVERT(NVARCHAR, NULL) AS Extension
		,InsurerId AS InsurerId
		,CONVERT(bit, 0) AS IsInternational
		,6 AS OrdinalId
		,pct.Id + 1000 AS InsurerPhoneNumberTypeId
	FROM dbo.PracticeInsurers
	LEFT OUTER JOIN dbo.PracticeCodeTable pct 
		ON pct.Code = ''Submissions''
		AND pct.ReferenceType = ''PHONETYPEINSURER''
	WHERE InsurerENumber <> ''''
		AND InsurerENumber IS NOT NULL')
GO

-- InsurerPhoneNumberType
IF OBJECT_ID('model.InsurerPhoneNumberTypes') IS NULL
BEGIN
	EXEC dbo.sp_executesql @statement = N'
CREATE VIEW model.InsurerPhoneNumberTypes AS
SELECT CASE 
	WHEN Code = ''Primary''
		THEN 8
	ELSE pctInsurer.Id + 1000 
	END AS Id,
	AlternateCode AS Abbreviation,
	CASE 
		WHEN Code = ''Primary''
			THEN ''Main''
		ELSE Code
	END AS Name,
	CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctInsurer
WHERE pctInsurer.ReferenceType = ''PHONETYPEINSURER'''
END
GO