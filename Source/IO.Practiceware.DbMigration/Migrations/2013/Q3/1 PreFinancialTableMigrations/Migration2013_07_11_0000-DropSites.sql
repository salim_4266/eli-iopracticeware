﻿IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'Printers' AND sc.Name = 'ServiceLocationId') 
BEGIN
	ALTER TABLE [model].[Printers] ADD ServiceLocationId int NULL
END

IF EXISTS(SELECT * FROM sys.objects WHERE Name = 'Sites') 
BEGIN
	ALTER TABLE [model].[Printers] DROP CONSTRAINT [FK_PrinterSite]
	DROP INDEX [IX_FK_PrinterSite] ON [model].[Printers]
END

GO

-- Need to create ServiceLocations View to update the PatientReceivables trigger
IF EXISTS(SELECT * FROM sys.views WHERE name = 'ServiceLocations')
BEGIN
	DROP VIEW model.ServiceLocations
END
GO

EXEC ('
	CREATE VIEW [model].[ServiceLocations]
	
	AS
	----First number of GetPairId for ServiceLocationId indicates where it comes from (0 - PracticeName, 1 - Resources)
	----If a surgery center is a provider, I''m using the PracticeName row for ServiceLocation because encounters are scheduled in the PracticeName ServiceLocation.
	SELECT v.PracticeId AS Id
		,CONVERT(BIT, 0) AS IsExternal
		,v.NAME AS [Name]
		,v.ServiceLocationCodeId AS ServiceLocationCodeId
		,v.ShortName AS ShortName
		,CONVERT(bit, 0) AS IsArchived
		,HexColor AS HexColor
	FROM (
		SELECT pn.PracticeId AS PracticeId
			,pic.FieldValue AS FieldValue
			,re.Billable AS Billable
			,re.ServiceCode AS ServiceCode
			,CASE 
				WHEN pic.FieldValue = ''T''
					AND re.Billable = ''Y''
					AND re.ServiceCode = ''02''
					AND pn.LocationReference <> ''''
					THEN pctRe.Id
				ELSE pct.Id
				END AS ServiceLocationCodeId
			,CASE LocationReference
				WHEN ''''
					THEN ''Office''
				ELSE ''Office-'' + LocationReference
				END AS ShortName
			,PracticeName AS NAME
			,pn.HexColor AS HexColor
			,COUNT_BIG(*) AS Count
		FROM dbo.PracticeName pn
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''PLACEOFSERVICE''
			AND SUBSTRING(pct.Code, 1, 2) = ''11''
		LEFT JOIN dbo.Resources re ON re.PracticeId = pn.PracticeId
			AND re.ResourceType = ''R''
			AND re.ServiceCode = ''02''
			AND re.Billable = ''Y''
			AND pic.FieldValue = ''T''
			AND pn.LocationReference <> ''''
		LEFT JOIN dbo.PracticeCodeTable pctRE ON pctRE.ReferenceType = ''PLACEOFSERVICE''
			AND SUBSTRING(pctRE.Code, 1, 2) = re.PlaceOfService
		WHERE pn.PracticeType = ''P''
		GROUP BY pn.PracticeId
			,pct.Id
			,LocationReference
			,pic.FieldValue
			,re.Billable
			,re.ServiceCode
			,pctRe.Id
			,PracticeName
			,pn.HexColor
		) AS v
	
	UNION ALL
	
	----Getting ServiceLocations from Resources table (ResourceType R).  THESE ARE ALL FACILITIES.
	SELECT (110000000 * 1 + re.ResourceId) AS Id
		,CONVERT(BIT, 1) AS IsExternal
		,re.ResourceDescription AS [Name]
		,CASE 
			WHEN pct.id IS NULL
				OR pct.Id = ''''
				THEN (
						SELECT Id AS Id
						FROM dbo.PracticeCodeTable
						WHERE SUBSTRING(Code, 1, 2) = ''11''
							AND ReferenceType = ''PLACEOFSERVICE''
						)
			ELSE pct.id
			END AS ServiceLocationCodeId
		,re.ResourceName AS ShortName
		,CONVERT(bit, 0) AS IsArchived
		,HexColor AS HexColor
	FROM dbo.Resources re
	LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''PLACEOFSERVICE''
		AND SUBSTRING(pct.Code, 1, 2) = re.PlaceOfService
	WHERE re.ResourceType = ''R''
		AND re.ServiceCode = ''02''')
GO


IF EXISTS(SELECT * FROM sys.objects WHERE Name = 'Sites') IF EXISTS(SELECT * FROM sys.objects WHERE Name = 'Sites') 
BEGIN

	UPDATE p
	SET ServiceLocationId = sl.Id
	FROM model.Printers p
	INNER JOIN model.Sites s ON s.Id = p.SiteId
	INNER JOIN model.ServiceLocations sl ON sl.Name = s.Name

	DECLARE @DefaultServiceLocationIdForPrinters int
	SET @DefaultServiceLocationIdForPrinters = (SELECT TOP 1 sl.Id FROM model.ServiceLocations sl WHERE IsExternal = 0)

	UPDATE p
	SET ServiceLocationId = @DefaultServiceLocationIdForPrinters
	FROM model.Printers p
	WHERE ServiceLocationId IS NULL

	ALTER TABLE [model].[Printers] ALTER COLUMN [ServiceLocationId] int NOT NULL

	ALTER TABLE [model].[Printers] DROP COLUMN SiteId
	DROP TABLE [model].[Sites]

END

IF OBJECT_ID('model.ServiceLocations', 'V') IS NOT NULL
	DROP VIEW model.ServiceLocations


IF EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'Resources' AND sc.Name = 'SiteId') 
BEGIN
	ALTER TABLE Resources DROP COLUMN SiteId
END

IF EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'PracticeName' AND sc.Name = 'SiteId') 
BEGIN

	ALTER TABLE PracticeName DROP COLUMN SiteId
END