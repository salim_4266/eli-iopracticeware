﻿-- Making the table AuditEntries as Insert only

IF OBJECT_ID('AuditEntriesInsertOnlyTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER AuditEntriesInsertOnlyTrigger')
END

GO

CREATE TRIGGER AuditEntriesInsertOnlyTrigger ON AuditEntries
FOR UPDATE, DELETE
NOT FOR REPLICATION
AS

DECLARE @msrepl_object_id int
SET @msrepl_object_id = OBJECT_ID('[dbo].[AuditEntries]')
DECLARE @retcode int
EXEC @retcode = sp_check_for_sync_trigger @msrepl_object_id
IF @retcode = 1 RETURN
SET NOCOUNT ON

IF (dbo.GetNoCheck() = 0)	   
BEGIN
	RAISERROR( 'AuditEntries cannot be altered.', 16, 1 )
	ROLLBACK TRANSACTION
END

GO

-- Making the table AuditEntryChanges as Insert only

IF OBJECT_ID('AuditEntryChangesInsertOnlyTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER AuditEntryChangesInsertOnlyTrigger')
END

GO

CREATE TRIGGER AuditEntryChangesInsertOnlyTrigger ON AuditEntryChanges
FOR UPDATE, DELETE
NOT FOR REPLICATION
AS

DECLARE @msrepl_object_id int
SET @msrepl_object_id = OBJECT_ID('[dbo].[AuditEntryChanges]')
DECLARE @retcode int
EXEC @retcode = sp_check_for_sync_trigger @msrepl_object_id
IF @retcode = 1 RETURN

SET NOCOUNT ON
IF (dbo.GetNoCheck() = 0)	   
BEGIN
	RAISERROR( 'AuditEntryChanges cannot be altered.', 16, 1 )
	ROLLBACK TRANSACTION
END

GO





