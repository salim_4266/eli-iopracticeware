﻿IF EXISTS(SELECT * FROM sys.objects WHERE name = 'PracticeServicesDuplicateServiceTrigger')
BEGIN 
DROP TRIGGER PracticeServicesDuplicateServiceTrigger
END

IF NOT EXISTS(SELECT * FROM sys.objects WHERE Name = 'UC_Services')
BEGIN
ALTER TABLE dbo.PracticeServices ADD CONSTRAINT [UC_Services] UNIQUE (Code);
END