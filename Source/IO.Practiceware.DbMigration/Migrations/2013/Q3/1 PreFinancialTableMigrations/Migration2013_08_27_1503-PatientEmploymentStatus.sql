﻿EXEC('
IF EXISTS(SELECT * FROM sys.views WHERE name = ''PatientEmploymentStatus'')
BEGIN
	DROP VIEW model.PatientEmploymentStatus
END
')

IF OBJECT_ID(N'[model].[PatientEmploymentStatus]', 'U') IS NULL
BEGIN
	-- Create PatientEmploymentStatus table
	EXEC('
	CREATE TABLE [model].[PatientEmploymentStatus](
		[Id] int IDENTITY(1,1) NOT NULL,
		[Name] nvarchar(64) NOT NULL,
		[IsArchived] bit NOT NULL DEFAULT 0
	);
	')

	-- Creating primary key on [Id] in table 'PatientEmploymentStatus'
	EXEC('
	ALTER TABLE [model].[PatientEmploymentStatus]
	ADD CONSTRAINT [PK_PatientEmploymentStatus]
		PRIMARY KEY CLUSTERED ([Id] ASC);
	')

	-- Migrate PatientEmploymentStatus from PracticeCodeTable
	EXEC('
	SET IDENTITY_INSERT [model].[PatientEmploymentStatus] ON
	
		INSERT INTO model.PatientEmploymentStatus (Id, Name, IsArchived)
			SELECT Id,
				Code AS [Name],
				CONVERT(bit, 0) AS IsArchived
			FROM dbo.PracticeCodeTable
			WHERE ReferenceType = ''EMPLOYEESTATUS''

	SET IDENTITY_INSERT [model].[PatientEmploymentStatus] OFF
	')
END
GO
