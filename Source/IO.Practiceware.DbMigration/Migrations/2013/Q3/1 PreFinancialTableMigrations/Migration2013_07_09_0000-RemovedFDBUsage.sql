﻿-- Bug #6882.Remove usage of FDB from Stored Procedure and Views.


--AllergyView
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AllergyView]'))
DROP VIEW [dbo].[AllergyView]
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AllergyView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[AllergyView]
AS
SELECT     CompositeAllergyID, Source, ConceptID, ConceptType, Description, Status, TouchDate
FROM         fdb.tblCompositeAllergy' 


--FDBView
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[FDBView]'))
DROP VIEW [dbo].[FDBView]
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[FDBView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[FDBView]
AS
SELECT     TOP 100 PERCENT MEDID AS MED_ID, MED_ROUTED_DF_MED_ID_DESC AS MED_DESC, 
                      MED_STRENGTH + '' '' + MED_STRENGTH_UOM AS MED_STRENGTH_UOM, MED_DOSAGE_FORM_DESC AS MED_DOSAGE, MED_REF_DEA_CD AS MED_REF
FROM      fdb.tblCompositeDrug
WHERE     (Status in (''P'', ''A''))
ORDER BY MED_ID' 
GO

---- Modify usp_NewCrop_InsertNewRx

IF EXISTS (select name from sysobjects where name = 'usp_NewCrop_InsertNewRx')
DROP PROCEDURE dbo.usp_NewCrop_InsertNewRx
GO

CREATE PROCEDURE [dbo].[usp_NewCrop_InsertNewRx]
(
@PatientId int,
@DrugId int,
@PrescriptionDate nvarchar(10),
--Third
@DosageNumber int,
@DosageForm int,
@Strength nvarchar(8),
@StrengthUOM nvarchar (16),
@Route int, 
--Fourth
@DosageFrequency int,
--Fifth
@RefillNo int,
@DAW nchar(1),
@DispenseNo NVARCHAR(20),
@DispenseNumberQualifier nvarchar(2),
--Sixth
@DaysSupply int,

@ResourceId int,
@PrescriptionGuid uniqueidentifier
)

AS
BEGIN
DECLARE @DrugName as nvarchar(64)
DECLARE @Third as nvarchar(64)
DECLARE @Fourth as nvarchar(64)
DECLARE @Fifth as nvarchar(64)
DECLARE @Sixth as nvarchar(64)
DECLARE @AppointmentId as int
DECLARE @Symptom as int

CREATE TABLE #LeftJoin
(
	TempId int
)

Insert #LeftJoin Select 1

Select @DrugName = MED_ROUTED_DF_MED_ID_DESC from fdb.tblCompositeDrug where MEDID = @DrugId
Select @Third = CASE @DosageNumber
					WHEN ''
						THEN '()'
					ELSE
						'(' + isnull(pctNumber.Code, '') + ' ' + isnull(pctForm.Code, '') + ')'
					END
	From #LeftJoin  
		Left Join PracticeCodeTable pctNumber ON pctNumber.ReferenceType = 'DOSAGENUMBERTYPE'
			and pctNumber.AlternateCode = @DosageNumber
		Left Join PracticeCodeTable pctForm ON pctForm.ReferenceType = 'DOSAGEFORMTYPE'
			and pctForm.AlternateCode = @DosageForm
		
Select @Third = @Third + CASE @Strength
							WHEN ''
								THEN '()'
							ELSE
								'(' + isnull(@Strength + ' ' + @StrengthUOM, '') + ')'
							END
		
Select @Third = @Third + CASE
							WHEN @Route in (35, 34, 5)
								THEN '(' + isnull(Code, '') + ')'
							ELSE
								'()'
							END
	From #LeftJoin Left Join PracticeCodeTable ON ReferenceType = 'DOSAGEROUTETYPE'
		and AlternateCode = @Route

Select @Fourth = CASE 
					WHEN @DosageFrequency > 1
						THEN '(' + isnull(Code, '') + ')()'
					ELSE
						'()()'
					END
	From #LeftJoin LEFT JOIN PracticeCodeTable ON ReferenceType = 'DOSAGEFREQUENCYTYPE' and AlternateCode = @DosageFrequency

Select @Fourth = @Fourth + CASE @Route
							WHEN 2
								THEN '(' + isnull(Code, '') + ')()()()'
							ELSE
								'()()()()'
							END
	From #LeftJoin Left Join PracticeCodeTable ON ReferenceType = 'DOSAGEROUTETYPE'
		and AlternateCode = @Route

Select @Fifth = CASE @RefillNo
					WHEN ''
						THEN '()'
					ELSE
						'(' + cast(@RefillNo as nvarchar(5)) + ' Refills)'
					END
					+
					CASE @DAW
						WHEN 'Y' 
							THEN '(DAW)'
						ELSE
							'()'
					END
					+
					CASE @DispenseNo
						WHEN ''
							THEN '()'
						ELSE
							'(' + cast(@DispenseNo as nvarchar(5)) + ' ' + isnull(Code, '') + ')'
					END
					+ '()'
	From #LeftJoin Left Join PracticeCodeTable ON ReferenceType = 'DISPENSENUMBERQUALIFIER'
		and AlternateCode = @DispenseNumberQualifier

Select @Sixth = CASE @DaysSupply
					WHEN ''
						THEN '()()'
					ELSE	
						'()(' + CAST(@DaysSupply as nvarchar(4)) + ' DAYS SUPPLY)'
					END

Select @AppointmentId = AppointmentId 
From PracticeActivity 
Where PatientId = @PatientId
	and Status in ('H', 'G', 'D', 'U')
	and ActivityDate <= @PrescriptionDate
Order By ActivityDate asc

select @Symptom = cast(Symptom as int) + 1
From PatientClinical
Where AppointmentId = @AppointmentId
	and ClinicalType = 'A'
Order By Symptom asc

Insert PatientClinical([AppointmentId]
           ,[PatientId]
           ,[ClinicalType]
           ,[EyeContext]
           ,[Symptom]
           ,[FindingDetail]
           ,[ImageDescriptor]
           ,[ImageInstructions]
           ,[Status]
           ,[DrawFileName]
           ,[Highlights]
           ,[FollowUp]
           ,[PermanentCondition]
           ,[PostOpPeriod]
           ,[Surgery]
           ,[Activity]) 
Select @AppointmentId as AppointmentId
, @PatientId as PatientId
, 'A' as ClinicalType
, '' as EyeContext
, @Symptom as Symptom
, 'RX-8/' + @DrugName + '-2/' + @Third + '-3/' + @Fourth + '-4/' + @Fifth + '-5/' + @Sixth + '-6/P-7/' as FindingDetail
, '!' + SUBSTRING(@PrescriptionDate, 5, 2) + '/' + SUBSTRING(@PrescriptionDate, 7, 2) + '/' + SUBSTRING(@PrescriptionDate, 1, 4) as ImageDescriptor
, @PrescriptionGuid as ImageInstructions
, 'A' as Status
, cast(@DrugId as nvarchar(16)) as DrawFileName
, 'A' as Highlights
, '' as FollowUp
, '' as PermanentCondition
, 0 as PostOpPeriod
, '' as Surgery
, '' as Activity

END


--RX-8/BETAGAN EYE DROPS-2/(1 DROPS)(0.5 %)(OS)-3/(BID)()()()()()-4/(3 REFILLS)()(1 BOTTLE)()-5/(- ONGOING)(90 DAYS SUPPLY)-6/P-7/

GO


/*15***** Object:  StoredProcedure [dbo].[usp_NewCrop_UpdateRx]   ******/
SET ANSI_NULLS ON
GO

IF EXISTS (select name from sysobjects where name = 'usp_NewCrop_UpdateRx')
DROP PROCEDURE dbo.usp_NewCrop_UpdateRx
GO



CREATE procedure [dbo].[usp_NewCrop_UpdateRx]
(
@PatientId int, 
@DrugId int, 
@PrescriptionDate nvarchar(10), 
@DispenseNo int,
@DosageNumber int,
@DosageForm nvarchar(20),
@Route nvarchar(20),
@DosageFrequency nvarchar(20),
@DAW bit,
@RefillNo int,
@ResourceId int,
@ClinicalId int,
@PrescriptionGuId uniqueidentifier
)

AS
BEGIN
DECLARE @DrugName as nvarchar(32)
DECLARE @Third as nvarchar(32)
DECLARE @Fourth as nvarchar(32)
DECLARE @Fifth as nvarchar(32)
DECLARE @Sixth as nvarchar(32)
DECLARE @AppointmentId as int
DECLARE @Symptom as int

Select @DrugName = MED_NAME from fdb.tblCompositeDrug where MEDID = @DrugId
Select @Third = CASE @DosageNumber
					WHEN ''
						THEN '()()()'
					ELSE
						'(' + cast(@DosageNumber as nvarchar(8)) + ' ' + @DosageForm + ')()()'
					END
					
Select @Fourth = CASE @DosageFrequency
						WHEN ''
							THEN '()()()()()()'
						ELSE
							'(' + @DosageFrequency + ')()()()()()'
						END

Select @Fifth = CASE @RefillNo
					WHEN 0
						THEN '()'
					ELSE
						'(' + cast(@RefillNo as nvarchar(5)) + 'REFILLS)'
					END
					+
					CASE @DAW
						WHEN 1 
							THEN '(DAW)'
						ELSE
							'()'
					END
					+
					CASE @DispenseNo
						WHEN 0
							THEN '()'
						ELSE
							'(' + cast(@DispenseNo as nvarchar(5)) + ')'
					END
					+ '()'

Select @Sixth = '()()'

Select @AppointmentId = AppointmentId 
From PracticeActivity 
Where PatientId = @PatientId
	and Status in ('H', 'G', 'D', 'U')
	and ActivityDate <= @PrescriptionDate
Order By ActivityDate asc

Update PatientClinical
	Set FindingDetail = 'RX-8/' + @DrugName + '-2/' + @Third + '-3/' + @Fourth + '-4/' + @Fifth + '-5/' + @Sixth + '-6/-7/'
		, ImageDescriptor = '!' + Convert(nvarchar(10), getdate(), 101) + ImageDescriptor
		,ImageInstructions=@PrescriptionGuId
Where ClinicalId = @ClinicalId

END


GO


---------------MU CQM #55 DM with Dilated Exam----------------

DECLARE @sql nvarchar(max)
SET @sql = N'

CREATE PROCEDURE [dbo].[USP_NQM_55_DM_with_DilatedExam]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN

	DECLARE @TotParm INT;
	DECLARE @CountParm INT;
	SET @TotParm=0;
	SET @CountParm=0;

	CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
	IF @ResourceIds=''''
	BEGIN
	
		INSERT INTO #TMPResources 
		SELECT Resources.ResourceId 
		FROM Resources 
		WHERE ResourceType IN (''D'', ''Q'')
	END

	ELSE
	BEGIN
	
		DECLARE @TmpResourceIds VARCHAR(100)
		DECLARE @ResId VARCHAR(10)
	
		SELECT @TmpResourceIds =@ResourceIds
		WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
			BEGIN
				SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
				SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    
				INSERT INTO #TMPResources values(@ResId)
			END
			INSERT INTO #TMPResources values(@TmpResourceIds)
	END


		-----------CQM #55 DM Denominator
		
		----Age 18-75 by reporting end date
		SELECT pd.PatientId 
		INTO #Age
		FROM PatientDemographics pd
		WHERE DATEDIFF (yyyy, CONVERT(datetime, pd.BirthDate), CONVERT(datetime, @EndDate)) BETWEEN 18 AND 75


		----two non-acute office visits ever WITH OUR DOCTOR
		SELECT a.PatientId
		INTO #TwoNonAcuteVisits
		FROM #Age a
		INNER JOIN PatientReceivables pr ON pr.PatientId = a.PatientId
		INNER JOIN PatientReceivableServices prs ON prs.invoice = pr.invoice 
			AND prs.Service IN (''99201'', ''99202'', ''99203'', ''99204'', ''99205'', ''99211'', ''99212'', ''99213'', ''99214'', ''99215'', ''99217'', ''99218'', ''99219'', ''99220''
				, ''99241'', ''99242'', ''99243'', ''99244'', ''99245''
				, ''99341'', ''99342'', ''99343'', ''99344'', ''99345'', ''99347'', ''99348'', ''99349'', ''99350''
				, ''99384'', ''99385'', ''99386'', ''99387''
				, ''99394'', ''99395'', ''99396'', ''99397'', ''99401'', ''99402'', ''99403'', ''99404'', ''99411'', ''99412'', ''99420'', ''99429'', ''99455'', ''99456''
				, ''92002'', ''92003'', ''92004'', ''92005'', ''92006'', ''92007'', ''92008'', ''92009'', ''92010'', ''92011'', ''92012'', ''92013'', ''92014''
				, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310'', ''99315'', ''99316'', ''99318''
				, ''99324'', ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337'')
			AND prs.Status=''A''
		INNER JOIN Resources re on re.ResourceId = pr.BillToDr 
			AND re.ResourceId IN (SELECT #TMPResources.ResourceId FROM #TMPResources)
		GROUP BY a.PatientId, pr.AppointmentId
		HAVING COUNT(*) > 1

		----one acute visit ever WITH OUR DOCTOR
		SELECT a.PatientId
		INTO #OneAcuteVisit
		FROM #Age a
		INNER JOIN PatientReceivables pr ON pr.PatientId = a.PatientId
		INNER JOIN PatientReceivableServices prs ON prs.invoice = pr.invoice 
			AND prs.Service IN (''99221'', ''99222'', ''99223'', ''99231'', ''99232'', ''99233'', ''99238'', ''99239''
			, ''99251'', ''99252'', ''99253'', ''99254'', ''99255'', ''99291''
			, ''99281'', ''99282'', ''99283'', ''99284'', ''99285'')
			AND prs.Status=''A''
		INNER JOIN Resources re on re.ResourceId = pr.BillToDr 
			AND re.ResourceId IN (SELECT #TMPResources.ResourceId FROM #TMPResources)
		

		---Candidates - had visits, correct age
		SELECT *
		INTO #Visits
		FROM #TwoNonAcuteVisits

		UNION

		SELECT *
		FROM #OneAcuteVisit



		-----Diagnosis of Diabetes within two years of Reporting End Date
		SELECT v.PatientId 
		INTO #DM
		FROM #Visits v
		INNER JOIN Appointments ap2Y on ap2Y.patientid = v.patientid
			AND DATEDIFF (mm, CONVERT(datetime, @EndDate), CONVERT(datetime, ap2Y.appdate)) BETWEEN -24 AND 0
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap2Y.AppointmentId
			AND pc.ClinicalType = ''Q'' 
			AND pc.Status=''A''
			AND CASE 
				WHEN LEN(FindingDetail) > 6
					THEN SUBSTRING(FindingDetail, 1, LEN(FindingDetail)-CHARINDEX(''.'',REVERSE(findingdetail)))
				ELSE FindingDetail
				END IN (''250'', ''250.0'', ''250.00'', ''250.01'', ''250.02'', ''250.03''
				, ''250.10'', ''250.11'', ''250.12'', ''250.13''
				, ''250.20'', ''250.21'', ''250.22'', ''250.23''
				, ''250.30'', ''250.31'', ''250.32'', ''250.33''
				, ''250.4'', ''250.40'', ''250.41'', ''250.42'', ''250.43''
				, ''250.50'', ''250.51'', ''250.52'', ''250.53''
				, ''250.60'', ''250.61'', ''250.62'', ''250.63''
				, ''250.7'', ''250.70'', ''250.71'', ''250.72'', ''250.73''
				, ''250.8'', ''250.80'', ''250.81'', ''250.82'', ''250.83''
				, ''250.9'', ''250.90'', ''250.91'', ''250.92'', ''250.93''
				, ''357.2''
				, ''362.0'', ''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'', ''362.07''
				, ''366.41''
				, ''648.0'', ''648.00'', ''648.01'', ''648.02'', ''648.03'', ''648.04'')
		
		
		
		-----Taking diabetic medications within two years of Reporting End Date (NO OV REQUIREMENT; have to look at all visits for active meds)
		SELECT a.PatientId 
		INTO #Meds
		FROM #Age a
		INNER JOIN PatientClinical pc ON pc.PatientId = a.PatientId
			AND pc.ClinicalType = ''A'' 
			AND pc.Status=''A''
			AND SUBSTRING(pc.FindingDetail, 1, 3) = ''RX-''
			AND SUBSTRING(ImageDescriptor, 1, 1) IN (''!'', ''K'', ''R'', '''')
			AND SUBSTRING(pc.FindingDetail, 6, CHARINDEX(''-2/'', pc.FindingDetail)-6) IN
				(SELECT MED_ROUTED_DF_MED_ID_DESC  
					FROM fdb.tblCompositeDrug 
					WHERE 
						etc LIKE ''%Amylin Analog%'' OR
						etc LIKE ''%Biguanid%'' OR
						etc LIKE ''%diabet%'' OR
						etc LIKE ''%glucosidas%'' OR
						etc LIKE ''%insulin%'' OR
						etc LIKE ''%Meglitinid%'' OR
						etc LIKE ''%Sulfonylure%'' OR
						etc LIKE ''%Thiazolidinedi%'' 
				)
		INNER JOIN PatientReceivables pr ON pr.AppointmentId = pc.AppointmentId
		INNER JOIN Resources re on re.ResourceId = pr.BillToDr 
			AND re.ResourceId IN (SELECT #TMPResources.ResourceId FROM #TMPResources)
		
		
		---Denominator before exclusions
		SELECT *
		INTO #DenominatorBeforeExclusions
		FROM #DM

		UNION

		SELECT *
		FROM #Meds

		----exclusions - polycystic ovaries, gestational diabetes, steroid induced diabetes
		SELECT dbe.PatientId
		INTO #Exclusions
		FROM #DenominatorBeforeExclusions dbe
		INNER JOIN Appointments ap2Y on ap2Y.patientid = dbe.patientid
			AND DATEDIFF (mm, CONVERT(datetime, @EndDate), CONVERT(datetime, ap2Y.appdate)) BETWEEN -24 AND 0
		INNER JOIN PatientClinical pc ON ap2Y.AppointmentId = pc.AppointmentId
			AND pc.ClinicalType = ''Q''
			AND pc.Status = ''A''
			AND CASE 
				WHEN LEN(FindingDetail) > 6
					THEN SUBSTRING(FindingDetail, 1, LEN(FindingDetail)-CHARINDEX(''.'',REVERSE(findingdetail)))
				ELSE FindingDetail
				END IN (''256.4''
					, ''648.8'', ''648.80'', ''648.81'', ''648.82'', ''648.83'', ''648.84''
					, ''249'', ''249.0'', ''249.00'', ''249.01'', ''249.1'', ''249.10'', ''249.11''
					, ''249.2'', ''249.20'', ''249.21'', ''249.3'', ''249.30'', ''249.31''
					, ''249.4'', ''249.40'', ''249.41'', ''249.5'', ''249.50'', ''249.51''
					, ''249.6'', ''249.60'', ''249.61'', ''249.7'', ''249.70'', ''249.71''
					, ''249.8'', ''249.80'', ''249.81'', ''249.9'', ''249.90'', ''249.91''
					, ''251.8'', ''962.0'')
		GROUP BY dbe.PatientId

		--CQM #55 Denominator
		SELECT dbe.PatientId
		INTO #Denominator
		FROM #DenominatorBeforeExclusions dbe
		LEFT JOIN #Exclusions e on e.PatientId = dbe.PatientId
		WHERE e.PatientId IS NULL


		--Eye Exam had during reporting period 
		----Dilation and Findings at back of eye 
		SELECT d.PatientId
		INTO #Numerator
		FROM #Denominator d
			INNER JOIN Appointments ap on ap.PatientId = d.patientid 
				AND ap.AppDate BETWEEN @StartDate AND @EndDate
			--Dilated
			INNER JOIN PatientClinical pcDilation on pcDilation.AppointmentId = ap.AppointmentId 
				AND (
						(pcDilation.ClinicalType = ''F'' 
							AND pcDilation.Symptom IN 
								(''/DILATION''
								, ''/DILATED IOP PROVOCATIVE''
								, ''/DILATED REFRACTION''
								, ''/FA''
								, ''/FA AND FP''
								, ''/FA AND FP TECH''
								, ''/FLUORESCEIN ANGIO''
								, ''/FLUORESCEIN ANGIO TECH''
								, ''/FLUORESCEIN ANGIOGRAPHY''
								, ''/ICG ANGIOGRAPHY''
								, ''/ICG TECH''
								, ''/INFRARED''
								, ''/INFRARED TECH''
								, ''/OPHTHALMOSCOPY INITIAL''
								, ''/OPHTHALMOSCOPY SUBSEQ''
								, ''/OPHTHALMOSCOPY, DETECTION''
								, ''/OPHTHALMOSCOPY, MANAGE''
								, ''/PQRI (ARMD DILATED MACULAR EXAM)''
								, ''/PQRI (DM RETINOPATHY DOCUMENT)''
								, ''/PQRI (DM RETINOPATHY INFO)''
								, ''/PQRI (DM1 DILATED EXAM IN DM)''
								)
						)
						OR 
						(pcDilation.ClinicalType = ''Q'' 
							AND pcDilation.Symptom LIKE ''%&%'')
					)
				AND pcDilation.Status = ''A''
			--Any Back of the Eye Findings
			INNER JOIN PatientClinical pcFindings ON pcFindings.AppointmentId = pcDilation.AppointmentId
				AND pcFindings.ClinicalType = ''Q''
				AND pcFindings.ImageDescriptor IN (''M'', ''N'', ''O'', ''P'')
				AND pcFindings.Status = ''A''
			GROUP BY d.PatientId
	
			UNION

			----Imaging of back of eye is also a retinal exam
			SELECT d.PatientId
			FROM #Denominator d
			INNER JOIN Appointments ap on ap.PatientId = d.patientid 
				AND ap.AppDate BETWEEN @StartDate AND @EndDate	
			INNER JOIN PatientClinical pcImaging on pcImaging.AppointmentId = ap.AppointmentId 
				AND pcImaging.ClinicalType = ''F'' 
				AND pcImaging.Status = ''A''
				AND pcImaging.Symptom IN 
					(''/AUTOFLUORESCENCE''
					, ''/FA''
					, ''/FA AND FP''
					, ''/FLUORESCEIN ANGIO''
					, ''/FLUORESCEIN ANGIOGRAPHY''
					, ''/FUNDUS PHOTO''
					, ''/FUNDUS PHOTO (FP)''
					, ''/FUNDUS PHOTO (RETINA)''
					, ''/GDX''
					, ''/HRT''
					, ''/HRT, MACULA''
					, ''/ICG ANGIOGRAPHY''
					, ''/INFRARED''
					, ''/OCT''
					, ''/OCT, CENTRAL MACULAR THICKNESS''
					, ''/OCT H''
					, ''/OCT, MACULA''
					, ''/RED FREE''
					, ''/SCANNING LASER POLARIMETRY''
					, ''/STEREO  PHOTO'')

			UNION

			--or Low Risk -- no DR finding in year before reporting period
			SELECT d.PatientId
			FROM #Denominator d
			INNER JOIN Appointments App1Year ON App1Year.PatientId = d.PatientId
				AND DATEDIFF (yyyy, CONVERT(datetime, App1Year.AppDate), CONVERT(datetime, @StartDate)) = 1
				AND App1Year.ScheduleStatus = ''D''
			INNER JOIN PatientClinical pcLowRisk ON pcLowRisk.AppointmentId = App1Year.AppointmentId
				AND pcLowRisk.Status = ''A''
				AND pcLowRisk.ClinicalType IN (''B'', ''K'')
				AND SUBSTRING(pcLowRisk.FindingDetail, 1, 6) NOT IN (''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
		
		SELECT @TotParm=COUNT(*)  
		FROM #Denominator

		SELECT @CountParm=COUNT(*)
		FROM #Numerator

		DROP TABLE #TMPResources
		DROP TABLE #Age
		DROP TABLE #TwoNonAcuteVisits
		DROP TABLE #OneAcuteVisit
		DROP TABLE #Visits
		DROP TABLE #DM
		DROP TABLE #Meds
		DROP TABLE #Exclusions
		DROP TABLE #DenominatorBeforeExclusions
		DROP TABLE #Denominator
		DROP TABLE #Numerator

		SELECT @TotParm as Denominator,@CountParm as Numerator

END

'

IF OBJECT_ID('USP_NQM_55_DM_with_DilatedExam') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)


GO


-------Core Measure 4 - Electronic Prescribing (C4 eRx)-------------
DECLARE @sql nvarchar(max)
SET @sql = N'

CREATE PROCEDURE [dbo].[USP_CMS_GetEPrescStatus]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
		
		DECLARE @TotParm INT;
		DECLARE @CountParm INT;
		SET @TotParm=0;
		SET @CountParm=0;
		
		CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
		IF @ResourceIds=''''
		BEGIN
			INSERT INTO #TMPResources 
			SELECT Resources.ResourceId 
			FROM Resources 
			WHERE ResourceType IN (''D'',''Q'')
		END
		ELSE
			BEGIN
				DECLARE @TmpResourceIds VARCHAR(100)
				DECLARE @ResId VARCHAR(10)
				SELECT @TmpResourceIds =@ResourceIds
				WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
					BEGIN
						SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
						SELECT @TmpResourceIds=SUBSTRING(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)+1, LEN(@TmpResourceIds))    
						INSERT INTO #TMPResources values(@ResId)
					END
					INSERT INTO #TMPResources values(@TmpResourceIds)
			END


		--denominator before exclusions
		SELECT AppDate, pc.PatientId, FindingDetail, ImageDescriptor, ImageInstructions, pc.ClinicalId
		INTO #rx
		FROM Appointments ap
		INNER JOIN PatientDemographics pd ON pd.PatientId = ap.PatientId
			AND pd.LastName <> ''TEST''
		INNER JOIN Resources re ON re.ResourceId = ap.ResourceId1
			AND re.ResourceId IN (Select #TMPResources.ResourceId FROM #TMPResources)
		INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
		WHERE AppDate BETWEEN @StartDate AND @EndDate 
			AND pc.ClinicalType = ''A'' 
			AND pc.Status = ''A''
			AND pc.FindingDetail LIKE ''RX-%/P-7/%''

		--exclude changed rx''s
		---changed
		SELECT r.clinicalid
		INTO #exclusions
		FROM #rx r
		INNER JOIN Appointments apChange on apChange.PatientId = r.PatientId
			AND apChange.AppDate < r.AppDate
		INNER JOIN PatientClinical pcChange on pcChange.AppointmentId = apChange.AppointmentId
			AND pcChange.ClinicalType = ''A''
			AND SUBSTRING(pcchange.FindingDetail, 6, CHARINDEX(''-2/'', pcchange.FindingDetail)-6) = SUBSTRING(r.FindingDetail, 6, CHARINDEX(''-2/'', r.FindingDetail)-6)
			AND SUBSTRING(pcchange.Imagedescriptor, 1, 1) = ''C''
		GROUP BY r.ClinicalId

		UNION

		--otc
		SELECT  r.ClinicalId
		FROM #rx r
		INNER JOIN fdb.tblcompositedrug t on t.MED_ROUTED_DF_MED_ID_DESC =  SUBSTRING(r.FindingDetail, 6, CHARINDEX(''-2/'', FindingDetail)-6)
		WHERE MED_REF_FED_LEGEND_IND_DESC = ''OTC''
		GROUP BY r.ClinicalId

		--Denominator
		SELECT r.ClinicalId, r.ImageInstructions
		INTO #denominator
		FROM #rx r
		LEFT JOIN #exclusions e on e.Clinicalid = r.Clinicalid
		WHERE e.ClinicalId is null

		--numerator
		SELECT d.ClinicalId
		INTO #Numerator
		FROM #denominator d
		WHERE len(d.ImageInstructions) > 10

		SELECT @TotParm = COUNT(*)
		FROM #Denominator

		SELECT @CountParm = COUNT(*) 
		FROM #Numerator

		DROP TABLE #TMPResources
		DROP TABLE #RX
		DROP TABLE #Exclusions
		DROP TABLE #Denominator
		DROP TABLE #Numerator
		
		SELECT @TotParm as Denominator,@CountParm as Numerator
		
		END
' 

IF OBJECT_ID('USP_CMS_GetEPrescStatus') IS NOT NULL
	SET @sql = REPLACE(@sql, 'CREATE PROCEDURE', 'ALTER PROCEDURE')

EXEC(@sql)

GO
