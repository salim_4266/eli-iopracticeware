﻿-- Adding persisted calculated columns to improve patient query performance

ALTER TABLE dbo.PracticeCodeTable ADD SubStringTenCode AS SUBSTRING(Code, 1, 10) PERSISTED
ALTER TABLE dbo.PracticeCodeTable ADD SubStringOneCode AS SUBSTRING(Code, 1, 1) PERSISTED

GO

-- Adding default values to the following so that we don't have to perform unnecessary null checks when querying for patient phone numbers
-- The following tables already have the constraints on them
UPDATE dbo.PatientDemographicsTable
SET CellPhone = '' WHERE CellPhone IS NULL

UPDATE dbo.PatientDemographicsTable
SET EmergencyPhone = '' WHERE EmergencyPhone IS NULL

UPDATE dbo.PatientDemographicsTable
SET EmployerPhone = '' WHERE EmployerPhone IS NULL

UPDATE dbo.PatientDemographicsTable
SET EmergencyName = '' WHERE EmergencyName IS NULL

UPDATE dbo.PatientDemoAlt
SET HomePhone = '' WHERE HomePhone IS NULL

UPDATE dbo.PatientDemoAlt
SET CellPhone = '' WHERE CellPhone IS NULL

UPDATE dbo.PatientDemoAlt
SET WorkPhone = '' WHERE WorkPhone IS NULL