﻿-- Create index on AuditDateTime for AuditEntries table
IF NOT EXISTS(SELECT name FROM sysindexes where name = 'IX_AuditEntries_AuditDateTime') 
CREATE NONCLUSTERED INDEX IX_AuditEntries_AuditDateTime
ON dbo.AuditEntries (AuditDateTime)

GO

IF EXISTS(SELECT name FROM sysobjects WHERE name = 'GetIntTableFromXmlIds')
DROP FUNCTION GetIntTableFromXmlIds
GO
-- Function to turn a xml string representation of Ids into a single column table, whose int column is named 'Value'.
-- The xml looks as follows <Ids><Id>1</Id></Ids>
CREATE FUNCTION GetIntTableFromXmlIds(@String nvarchar(max)) RETURNS @Results table(Value int PRIMARY KEY CLUSTERED)
AS
BEGIN
	
	DECLARE @xml xml
	SET @xml = @String

	INSERT @Results(Value)
	SELECT DISTINCT n.Id.value('.', 'int') 
	FROM @xml.nodes('//Id') as n(Id)
	RETURN
END
GO


IF EXISTS(SELECT name FROM sysobjects WHERE name = 'GetCharTableFromXmlValues')
DROP FUNCTION GetCharTableFromXmlValues
GO

-- Function to turn a xml string representation of characters into a single column table, whose nvarchar(max) column is named 'Value'.
-- The xml looks as follows <Values><Value>asdf</Value></Values>
CREATE FUNCTION GetCharTableFromXmlValues(@String nvarchar(max)) RETURNS @Results table(Value nvarchar(max))
AS
BEGIN
	
	DECLARE @xml xml
	SET @xml = @String

	INSERT @Results(Value)
	SELECT DISTINCT n.Id.value('.', 'nvarchar(max)') 
	FROM @xml.nodes('//Value') as n(Id)
	RETURN
END

GO

IF EXISTS(SELECT name FROM sysobjects WHERE name ='GetAuditAndLogEntries')
DROP Procedure GetAuditAndLogEntries
GO

-- Function to return a single table representing an ordered list of AuditEntryChanges and LogEntries.
-- @OrderByType (0) = Date
-- @OrderByType (1) = UserId
-- @OrderByType (2) = PatientId
-- @DetailsChanged w/ LogEntries = Message
-- @DetailsChanged w/ AuditEntryChanges = ColumnName
-- @LogActionTypes w/ LogEntries = LogCategoryNames (concatonated)
-- @LogActionTypes w/ AuditEntryChanges = ChangeTypeId (AuditEntryChangeType) (string int)
CREATE PROCEDURE GetAuditAndLogEntries(@StartDate datetime, 
									   @EndDate datetime, 
									   @UserIds nvarchar(max),
									   @PatientId nvarchar(max), 
									   @DetailsChanged nvarchar(max), 
									   @LogActionTypes nvarchar(max), 
									   @AuditActionTypes nvarchar(max), 
									   @PageSize int, 
									   @PageIndex int,
									   @OrderByTypeId int)
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- Using ROWCOUNT for performance considerations See http://www.4guysfromrolla.com/webtech/042606-1.shtml
	DECLARE @maxRow bigint
	SET @maxRow = ((@PageSize * @PageIndex) + @PageSize) - 1 	
	SET ROWCOUNT @maxRow
	
	CREATE TABLE #Results(RowNumber int IDENTITY, Id uniqueidentifier, IsLogEntry bit, DateTime datetime, UserId nvarchar(max), AccessLocation nvarchar(max), AccessDevice nvarchar(max), SourceOfAccess nvarchar(max), ActionMessage nvarchar(max), PatientId bigint, DetailChanged nvarchar(max), NewValue nvarchar(max), OldValue nvarchar(max), ContentTypeId int)
	
	DECLARE @parameters nvarchar(max) 
	SET @parameters = '@StartDate datetime, 
						@EndDate datetime, 
						@UserIds nvarchar(max), 
						@PatientId nvarchar(max),
						@DetailsChanged nvarchar(max), 
						@LogActionTypes nvarchar(max), 
						@AuditActionTypes nvarchar(max), 
						@PageSize int, 
						@PageIndex int,
						@OrderByTypeId int'

	DECLARE @sql nvarchar(max)
	SET @sql =		
	'
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- Using ROWCOUNT for performance considerations See http://www.4guysfromrolla.com/webtech/042606-1.shtml
	DECLARE @maxRow bigint
	SET @maxRow = ((@PageSize * @PageIndex) + @PageSize) - 1 	
	SET ROWCOUNT @maxRow
	
	INSERT #Results(Id, IsLogEntry, DateTime, UserId, AccessLocation, AccessDevice, SourceOfAccess, ActionMessage, PatientId, DetailChanged, NewValue, OldValue, ContentTypeId)
	
	SELECT * FROM (	
		SELECT * FROM 
		(SELECT le.Id AS Id, CAST(1 AS bit) AS IsLogEntry, le.[DateTime] AS [DateTime], CAST(CONVERT(int, RIGHT(le.UserId, 12)) AS nvarchar(max)) AS UserId, le.ServerName AS AccessLocation, le.MachineName AS AccessDevice, le.ProcessName AS SourceOfAccess, 
		NULL AS ActionMessage, -- Query for action types (Concatonated LogCategoryNames) separately
		(SELECT TOP 1 CAST(lep.Value AS bigint) FROM dbo.LogEntryProperties lep WHERE lep.LogEntryId = le.Id AND lep.Name = ''PatientId'')  AS PatientId,
		le.[Message] AS DetailChanged,
		NULL AS NewValue,
		NULL AS OldValue,
		7 AS ContentTypeId -- ContentType.Text
		FROM dbo.LogEntries le) AS q
		WHERE (@StartDate IS NULL OR q.[DateTime] >= @StartDate)
		AND (@EndDate IS NULL OR q.[DateTime] <= @EndDate)
		AND (@UserIds IS NULL OR q.UserId IN (SELECT CAST(VALUE AS nvarchar(max)) FROM dbo.GetIntTableFromXmlIds(@UserIds)))
		AND (@PatientId IS NULL OR q.PatientId = @PatientId)
		AND (@DetailsChanged IS NULL OR q.DetailChanged IN (SELECT Value FROM dbo.GetCharTableFromXmlValues(@DetailsChanged)))
		AND (@LogActionTypes IS NULL OR EXISTS(SELECT lc.Name FROM dbo.LogCategories lc
											JOIN dbo.LogEntryLogCategory lelc ON lc.Id = lelc.Categories_Id
											WHERE lelc.LogEntryLogCategory_LogCategory_Id = q.Id AND lc.Name IN (SELECT Value FROM dbo.GetCharTableFromXmlValues(@LogActionTypes))))

		UNION ALL

		SELECT ae.Id AS Id, CAST(0 AS bit) AS IsLogEntry, ae.[AuditDateTime] AS [DateTime], CAST(ae.UserId AS nvarchar(max)) AS UserId, ae.ServerName AS AccessLocation, ae.HostName AS AccessDevice, ae.ObjectName AS SourceOfAccess, 				
			CONVERT(varchar(1), ae.ChangeTypeId) AS ActionMessage,
			ae.PatientId AS PatientId,
			aec.ColumnName AS DetailChanged, aec.NewValue AS NewValue, aec.OldValue AS OldValue, NULL AS ContentTypeId
		FROM dbo.AuditEntries ae JOIN dbo.AuditEntryChanges aec ON ae.Id = aec.AuditEntryId
		WHERE (@StartDate IS NULL OR ae.[AuditDateTime] >= @StartDate)
		AND (@EndDate IS NULL OR ae.[AuditDateTime] <= @EndDate)
		AND (@UserIds IS NULL OR CAST(ae.UserId AS nvarchar(max)) IN (SELECT CAST(Value AS nvarchar(max)) FROM dbo.GetIntTableFromXmlIds(@UserIds)))
		AND (@PatientId IS NULL OR CAST(ae.PatientId AS nvarchar(max)) = @PatientId)
		AND (@DetailsChanged IS NULL OR aec.ColumnName IN (SELECT Value FROM dbo.GetCharTableFromXmlValues(@DetailsChanged)))
		AND (@AuditActionTypes IS NULL OR ae.ChangeTypeId IN (SELECT Value FROM dbo.GetIntTableFromXmlIds(@AuditActionTypes)))
		) results
	ORDER BY {OrderBy}'

	SET @sql = REPLACE(@sql, '{OrderBy}',
		CASE @OrderByTypeId WHEN 0 THEN '[DateTime]' WHEN 1 THEN 'COALESCE([UserId],2147483647)' WHEN 2 THEN 'COALESCE([PatientId],2147483647)' END
	)

	EXECUTE sp_executesql @sql, @parameters, @StartDate = @StartDate, @EndDate = @EndDate, @UserIds = @UserIds, @PatientId = @PatientId, @DetailsChanged = @DetailsChanged, @LogActionTypes = @LogActionTypes, @AuditActionTypes = @AuditActionTypes, @PageSize = @PageSize, @PageIndex = @PageIndex, @OrderByTypeId = @OrderByTypeId

	SET ROWCOUNT @PageSize

	SELECT Id, IsLogEntry, DateTime, 
	UserId, AccessLocation, AccessDevice, SourceOfAccess, 
	CASE WHEN IsLogEntry = 1 THEN
		STUFF(
			(SELECT
				', ' + lc.Name
				FROM dbo.LogCategories lc
				JOIN dbo.LogEntryLogCategory lelc ON lc.Id = lelc.Categories_Id
				WHERE lelc.LogEntryLogCategory_LogCategory_Id = r.Id
				FOR XML PATH(''), TYPE
			).value('.','varchar(max)')
			,1,2, ''
		) 
		ELSE CASE ActionMessage WHEN '0' THEN 'Insert' WHEN '1' THEN 'Update' WHEN '2' THEN 'Delete' END
		END
		AS ActionMessage,
	PatientId, DetailChanged, NewValue, OldValue, ContentTypeId
	FROM #Results r
	WHERE RowNumber >= @PageIndex * @PageSize

	SET ROWCOUNT 0
END
GO
