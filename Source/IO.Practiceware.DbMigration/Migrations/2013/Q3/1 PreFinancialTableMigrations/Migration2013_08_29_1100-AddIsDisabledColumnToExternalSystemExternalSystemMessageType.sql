﻿/* Bug #7284.Add IsDisabled Column for model.ExternalSystemExternalSystemMessageTypes. */

IF NOT EXISTS (
		SELECT *
		FROM sysobjects so
		INNER JOIN syscolumns sc ON so.id = sc.id
		WHERE so.NAME = 'ExternalSystemExternalSystemMessageTypes'
			AND sc.NAME = 'IsDisabled'
		)
BEGIN
	ALTER TABLE model.ExternalSystemExternalSystemMessageTypes ADD IsDisabled Bit NOT NULL DEFAULT 0;
END