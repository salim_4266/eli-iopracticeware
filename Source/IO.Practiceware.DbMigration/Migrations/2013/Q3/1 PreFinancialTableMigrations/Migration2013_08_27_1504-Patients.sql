﻿EXEC('
IF EXISTS(SELECT * FROM sys.views WHERE name = ''Patients'')
BEGIN
	DROP VIEW model.Patients
END
')


IF OBJECT_ID(N'[model].[Patients]', 'U') IS NULL
BEGIN
	-- Create Patients table
	EXEC('
	CREATE TABLE [model].[Patients] (
		[Id] int IDENTITY(1,1) NOT NULL,
		[LastName] nvarchar(max)  NOT NULL,
		[FirstName] nvarchar(max)  NULL,
		[DateOfBirth] datetime  NULL,
		[BillingOrganizationId] int  NULL, -- TODO - Add FK when reference is table. FOREIGN KEY REFERENCES model.BillingOrganizations(Id),
		[DateOfDeath] datetime  NULL,
		[DefaultUserId] int  NULL, -- TODO - Add FK when reference is table. FOREIGN KEY REFERENCES model.Users(Id),
		[EmployerName] nvarchar(max)  NULL,
		[GenderId] int  NULL,
		[Honorific] nvarchar(max)  NULL,
		[IsClinical] bit  NOT NULL,
		[IsCollectionLetterSuppressed] bit NOT NULL DEFAULT 0,
		[IsHipaaConsentSigned] bit NULL,
		[IsStatementSuppressed] bit NOT NULL DEFAULT 0,
		[MaritalStatusId] int  NULL,
		[MiddleName] nvarchar(max)  NULL,
		[NickName] nvarchar(max)  NULL,
		[Occupation] nvarchar(max)  NULL,
		[PatientEmploymentStatusId] int  NULL FOREIGN KEY REFERENCES model.PatientEmploymentStatus(Id),
		[PatientEthnicityId] int  NULL FOREIGN KEY REFERENCES model.PatientEthnicities(Id),
		[PatientLanguageId] int  NULL FOREIGN KEY REFERENCES model.PatientLanguages(Id),
		[PatientNationalOriginId] int  NULL,
		[PatientRaceId] int  NULL FOREIGN KEY REFERENCES model.PatientRaces(Id),
		[PatientReligionId] int  NULL,
		[PatientStatusId] int  NOT NULL,
		[PaymentOfBenefitsToProviderId] int NOT NULL DEFAULT 1,
		[Prefix] nvarchar(max)  NULL,
		[PriorFirstName] nvarchar(max)  NULL,
		[PriorLastName] nvarchar(max)  NULL,
		[PriorPatientCode] nvarchar(max)  NULL,
		[Salutation] nvarchar(max)  NULL,
		[SocialSecurityNumber] nvarchar(max)  NULL,
		[Suffix] nvarchar(max)  NULL,
		[ReleaseOfInformationCodeId] int NULL,
		[ReleaseOfInformationDateTime] datetime NULL
	);
	')

	EXEC('
	-- Creating primary key on [Id] in table ''Patients''
	ALTER TABLE [model].[Patients]
	ADD CONSTRAINT [PK_Patients]
		PRIMARY KEY CLUSTERED ([Id] ASC);
	
	-- Creating non-clustered index for FOREIGN KEY ''FK_BillingOrganizationPatient''
	CREATE INDEX [IX_FK_BillingOrganizationPatient]
	ON [model].[Patients]
		([BillingOrganizationId]);
	
	-- Creating non-clustered index for FOREIGN KEY ''FK_PatientEthnicityPatient''
	CREATE INDEX [IX_FK_PatientEthnicityPatient]
	ON [model].[Patients]
		([PatientEthnicityId]);

	-- Creating non-clustered index for FOREIGN KEY ''FK_PatientRacePatient''
	CREATE INDEX [IX_FK_PatientRacePatient]
	ON [model].[Patients]
		([PatientRaceId]);
	
	-- Creating non-clustered index for FOREIGN KEY ''FK_PatientLanguagePatient''
	CREATE INDEX [IX_FK_PatientLanguagePatient]
	ON [model].[Patients]
		([PatientLanguageId]);

	-- Creating non-clustered index for FOREIGN KEY ''FK_PatientUser''
	CREATE INDEX [IX_FK_PatientUser]
	ON [model].[Patients]
		([DefaultUserId]);
	')

	-- Migrate Patients from PatientDemographics
	EXEC('
	DECLARE @currentIdentity bigint

    SET @currentIdentity = IDENT_CURRENT(''dbo.PatientDemographicsTable'')
	
	SET IDENTITY_INSERT [model].[Patients] ON
	
		INSERT INTO model.Patients (Id, 
									MaritalStatusId, 
									DateOfBirth, 
									GenderId, 
									SocialSecurityNumber, 
									Occupation, 
									PriorLastName, 
									PriorFirstName, 
									BillingOrganizationId, 
									PatientEthnicityId, 
									PatientRaceId, 
									PatientLanguageId, 
									PatientNationalOriginId, 
									PriorPatientCode, 
									PatientEmploymentStatusId,
									PatientReligionId,
									DateOfDeath,
									FirstName,
									LastName,
									MiddleName,
									Suffix,
									Prefix,
									Honorific,
									Salutation,
									NickName,
									PatientStatusId,
									IsClinical,
									DefaultUserId,
									EmployerName,
									IsHipaaConsentSigned,
									IsStatementSuppressed,
									IsCollectionLetterSuppressed,
									PaymentOfBenefitsToProviderId,
									ReleaseOfInformationCodeId,
									ReleaseOfInformationDateTime)
			SELECT
				pd.PatientId AS Id, 
				CASE pd.Marital
					WHEN ''S''
						THEN 1
					WHEN ''M''
						THEN 2
					WHEN ''D''
						THEN 3
					WHEN ''W''
						THEN 4
					ELSE NULL
					END AS MaritalStatusId, 
				CASE WHEN pd.BirthDate = '''' THEN NULL ELSE CONVERT(datetime,pd.BirthDate, 112) END AS DateOfBirth, 
				CASE pd.Gender
					WHEN ''M''
						THEN 3
					WHEN ''F''
						THEN 2
					WHEN ''U''
						THEN 1
					ELSE 4
					END AS GenderId, 
				pd.SocialSecurity AS SocialSecurityNumber,
				pd.Occupation AS Occupation, 
				CONVERT(nvarchar, NULL) AS PriorLastName,
				CONVERT(nvarchar, NULL) AS PriorFirstName,
				pnMainOffice.PracticeId AS BillingOrganizationId, 
				pctEthnicity.Id AS PatientEthnicityId,
				pctRace.Id AS PatientRaceId,
				pctLanguage.Id AS PatientLanguageId,
				NULL AS PatientNationalOriginId,
				pd.OldPatient AS PriorPatientCode,
				pctEmployment.Id AS PatientEmploymentStatusId,
				NULL AS PatientReligionId,
				CONVERT(datetime, NULL, 112) AS DateOfDeath, 
				pd.FirstName AS FirstName, 
				pd.LastName AS LastName, 
				pd.MiddleInitial AS MiddleName, 
				pd.NameReference AS Suffix, 
				pd.Salutation AS Prefix, 
				CONVERT(nvarchar, NULL) AS Honorific, 
				CONVERT(nvarchar, NULL) AS Salutation, 
				CONVERT(nvarchar, NULL) AS NickName, 
				CASE pd.[Status]
					WHEN ''C''
						THEN 2
					WHEN ''D''
						THEN 3
					WHEN ''M''
						THEN 4
					ELSE 1
					END	AS PatientStatusId,
				CASE pd.[Status]
					WHEN ''I''
						THEN CONVERT(bit, 0)
					ELSE CONVERT(bit, 1)
					END AS IsClinical,
				CASE 
					WHEN pd.SchedulePrimaryDoctor = 0
						THEN CONVERT(int, NULL)
					ELSE pd.SchedulePrimaryDoctor 
					END AS DefaultUserId,
				pd.BusinessName AS EmployerName,
				CASE 
					WHEN pd.Hipaa = ''T''
						THEN CONVERT(bit, 1)
					WHEN pd.Hipaa = ''F''
						THEN CONVERT(bit, 0)
					ELSE
						CONVERT(bit, NULL)
					END AS IsHipaaConsentSigned,
				CASE pd.SendStatements
					WHEN ''Y''
						THEN CONVERT(bit, 0)
					ELSE CONVERT(bit, 1)
					END AS IsStatementSuppressed,
				CASE pd.SendConsults
					WHEN ''Y''
						THEN CONVERT(bit, 0)
					ELSE CONVERT(bit, 1)
					END AS IsCollectionLetterSuppressed,
				1 AS PaymentOfBenefitsToProviderId,
				CASE pd.FinancialSignature
					WHEN ''Y''
						THEN 1
					ELSE
						0
					END AS ReleaseOfInformationCodeId,
				CONVERT(datetime,pd.FinancialSignatureDate, 112) AS ReleaseOfInformationDateTime
			FROM dbo.PatientDemographicsTable pd
	
			LEFT JOIN dbo.PracticeName pnMainOffice ON pnMainOffice.PracticeType = ''P'' and pnMainOffice.LocationReference = ''''
			LEFT JOIN dbo.PracticeCodeTable pctEthnicity ON pctEthnicity.ReferenceType = ''Ethnicity'' 
				AND Ethnicity = pctEthnicity.Code
			LEFT JOIN dbo.PracticeCodeTable pctLanguage ON pctLanguage.ReferenceType = ''Language''
				AND pd.[Language] = pctLanguage.SubStringTenCode
			LEFT JOIN dbo.PracticeCodeTable pctRace ON pctRace.ReferenceType = ''Race''
				AND pd.Race = pctRace.Code
			LEFT JOIN dbo.PracticeCodeTable pctEmployment ON pctEmployment.ReferenceType = ''EmployeeStatus''
				AND pd.BusinessType = pctEmployment.SubStringOneCode

	SET IDENTITY_INSERT [model].[Patients] OFF

	DBCC CHECKIDENT(''[model].[Patients]'', RESEED, @currentIdentity)
	')
END
GO
