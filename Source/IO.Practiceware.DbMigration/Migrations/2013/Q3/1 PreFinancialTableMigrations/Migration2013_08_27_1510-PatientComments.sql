﻿EXEC('
IF EXISTS(SELECT * FROM sys.views WHERE name = ''PatientComments'')
BEGIN
	DROP VIEW model.PatientComments
END
')

IF OBJECT_ID(N'[model].[PatientComments]', 'U') IS NULL
BEGIN
	-- Create PatientComments table
	EXEC('
	CREATE TABLE [model].[PatientComments] (
		[Id] int IDENTITY(1,1) NOT NULL,
		[PatientId] int  NOT NULL FOREIGN KEY REFERENCES model.Patients(Id),
		[Value] nvarchar(max)  NOT NULL,
		[PatientCommentTypeId] int  NOT NULL
	);
	')

	-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPatientComment'
	-- Creating primary key on [Id] in table 'PatientComments'
	EXEC('
	ALTER TABLE [model].[PatientComments]
	ADD CONSTRAINT [PK_PatientComments]
		PRIMARY KEY CLUSTERED ([Id] ASC);

	CREATE INDEX [IX_FK_PatientPatientComment]
	ON [model].[PatientComments]
		([PatientId]);
	')

	-- Migrate PatientComments
	EXEC('
		INSERT INTO model.PatientComments (PatientId, Value, PatientCommentTypeId) 
			
		SELECT pd.PatientId AS PatientId,
			pd.ProfileComment1 AS Value,
			2 AS PatientCommentTypeId
		FROM dbo.PatientDemographicsTable pd
		WHERE ProfileComment1 <> ''''
			AND ProfileComment1 IS NOT NULL

		UNION ALL

		SELECT pd.PatientId AS PatientId,
			 ''EmergencyName: '' + pd.EmergencyName 
				+ CHAR(13) + CHAR(10) + ''EmergencyPhone: '' + pd.EmergencyPhone AS Value,
			3 AS PatientCommentTypeId
		FROM dbo.PatientDemographicsTable pd
		WHERE pd.EmergencyName <> '''' 
	')
END
GO
