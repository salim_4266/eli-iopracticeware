﻿IF EXISTS(SELECT * FROM sys.views WHERE name = 'DoctorInsurerAssignments')
BEGIN
	DROP VIEW model.DoctorInsurerAssignments
END
GO

IF OBJECT_ID(N'[model].[DoctorInsurerAssignments]', 'U') IS NOT NULL
    DROP TABLE [model].[DoctorInsurerAssignments];
GO

IF OBJECT_ID(N'[model].[DoctorInsurerAssignments]', 'U') IS NULL
BEGIN
	-- Creating table 'DoctorInsurerAssignments'
	CREATE TABLE [model].[DoctorInsurerAssignments] (
		[Id] bigint IDENTITY(1,1) NOT NULL,
		[IsAssignmentAccepted] bit  NOT NULL,
		[InsurerId] int  NOT NULL, -- TODO Create FK when Insurers is a table
		[DoctorId] int  NOT NULL, -- TODO Create FK When Users is a table
		[SuppressPatientPayments] bit  NOT NULL
	);
END
GO

-- Creating primary key on [Id] in table 'DoctorInsurerAssignments'
ALTER TABLE [model].[DoctorInsurerAssignments]
ADD CONSTRAINT [PK_DoctorInsurerAssignments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
