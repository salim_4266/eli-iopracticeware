﻿IF OBJECT_ID(N'[model].[PatientEmailAddresses]', 'U') IS NULL
BEGIN
	-- Create PatientEmailAddresses table
	EXEC('
	CREATE TABLE [model].[PatientEmailAddresses] (
		[Id] int IDENTITY(1,1) NOT NULL,
		[Value] nvarchar(max)  NOT NULL,
		[OrdinalId] int  NOT NULL,
		[PatientId] int  NOT NULL FOREIGN KEY REFERENCES model.Patients(Id),
		[EmailAddressTypeId] int NOT NULL,
	);
	')

	-- Creating non-clustered index for FOREIGN KEY 'FK_PatientEmailAddressPatient'
	-- Creating primary key on [Id] in table 'PatientEmailAddresses'
	EXEC('
	ALTER TABLE [model].[PatientEmailAddresses]
	ADD CONSTRAINT [PK_PatientEmailAddresses]
		PRIMARY KEY CLUSTERED ([Id] ASC);

	CREATE INDEX [IX_FK_PatientEmailAddressPatient]
	ON [model].[PatientEmailAddresses]
		([PatientId]);
	')

	-- Migrate PatientEmailAddresses
	EXEC('
	DECLARE @EmailAddressMax int
	SET @EmailAddressMax = 110000000

	SET IDENTITY_INSERT [model].[PatientEmailAddresses] ON

		INSERT INTO model.PatientEmailAddresses (Id, Value, OrdinalId, PatientId, EmailAddressTypeId)
			SELECT model.GetPairId(5, PatientId, @EmailAddressMax) AS Id,
				Email AS Value,
				1 AS OrdinalId,
				PatientId AS PatientId,
				1 AS EmailAddressTypeId
			FROM dbo.PatientDemographicsTable
			WHERE Email <> ''''
				AND Email IS NOT NULL
			
	SET IDENTITY_INSERT [model].[PatientEmailAddresses] OFF
	')
END
GO
	
sp_RENAME 'model.PatientCommunicationPreferences.EmailAddressId', 'PatientEmailAddressId', 'COLUMN'
GO
