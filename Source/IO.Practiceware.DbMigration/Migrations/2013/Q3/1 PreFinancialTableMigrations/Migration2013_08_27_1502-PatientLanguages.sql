﻿EXEC ('
IF EXISTS(SELECT * FROM sys.views WHERE name = ''PatientLanguages'')
BEGIN
	DROP VIEW model.PatientLanguages
END
')

IF OBJECT_ID(N'[model].[PatientLanguages]', 'U') IS NULL
BEGIN
	-- Create PatientLanguages table
	EXEC('
	CREATE TABLE [model].[PatientLanguages](
		[Id] int IDENTITY(1,1) NOT NULL,
		[Name] nvarchar(max) NOT NULL,
		[IsArchived] bit NOT NULL DEFAULT 0,
		[OrdinalId] int NOT NULL DEFAULT 1,
		[Abbreviation] nvarchar(max) NULL
	);
	')

	-- Creating primary key on [Id] in table 'PatientLanguages'
	EXEC('
	ALTER TABLE [model].[PatientLanguages]
	ADD CONSTRAINT [PK_PatientLanguages]
		PRIMARY KEY CLUSTERED ([Id] ASC);
	')

	-- Migrate PatientLanguages from PracticeCodeTable
	EXEC('
	SET IDENTITY_INSERT [model].[PatientLanguages] ON
	
		INSERT INTO model.PatientLanguages (Id, Name, IsArchived, OrdinalId)
			SELECT Id,
				SUBSTRING(Code, 1, 10) AS [Name],
				CONVERT(bit, 0) AS IsArchived,
				1 AS OrdinalId
			FROM dbo.PracticeCodeTable
			WHERE ReferenceType = ''LANGUAGE''

	SET IDENTITY_INSERT [model].[PatientLanguages] OFF
	')
END
GO
