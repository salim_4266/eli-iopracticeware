﻿-- When an appointment is deleted then this will remove and PatientClinicalSurgeryPlan rows associated with that appointmentid
SET ANSI_NULLS ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AppointmentDeleteSurgeryPlanTrigger]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[AppointmentDeleteSurgeryPlanTrigger] ON [dbo].[Appointments] FOR UPDATE NOT FOR REPLICATION AS
BEGIN 
SET NOCOUNT ON

IF EXISTS (
	SELECT 
	* 
	FROM inserted 
	WHERE ScheduleStatus NOT IN (''R'',''D'',''A'',''P'')
)
BEGIN
DELETE
FROM PatientClinicalSurgeryPlan
WHERE SurgeryRefType = ''F'' 
	AND SurgeryValue IN (
		SELECT AppointmentId
		FROM inserted
		WHERE ScheduleStatus NOT IN (
				''R''
				,''D''
				,''A''
				,''P''
				)
		)

END
		
SET NOCOUNT OFF

END' 

GO

-- migrated 
DELETE
FROM PatientClinicalSurgeryPlan
WHERE SurgeryRefType = 'F'
	AND SurgeryValue IN (
		SELECT AppointmentId
		FROM Appointments
		WHERE ScheduleStatus NOT IN (
				'R'
				,'D'
				,'A'
				,'P'
				)
			)