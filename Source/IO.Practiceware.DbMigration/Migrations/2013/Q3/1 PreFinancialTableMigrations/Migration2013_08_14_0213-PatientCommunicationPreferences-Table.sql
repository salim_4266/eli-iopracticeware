﻿

-- Create the new PatientCommunicationPreferences table and migrate the existing information from dbo.PatientDemographics into the new table


DECLARE @schemaName NVARCHAR(70)
	SET @schemaName = 'model'

DECLARE @modelPatientCommPref NVARCHAR(70)
	SET @modelPatientCommPref ='PatientCommunicationPreferences'

-- drop all partitions that are associated to model.PatientCommunicationPreferences
EXEC dbo.DropViewsLike @schemaName,@modelPatientCommPref


-- If there is a table PatientCommunicationPreferences, then drop it
IF OBJECT_ID(N'[model].[PatientCommunicationPreferences]', 'U') IS NOT NULL
    DROP TABLE [model].[PatientCommunicationPreferences];
GO


-- Creating table 'PatientCommunicationPreferences'
CREATE TABLE [model].[PatientCommunicationPreferences] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PatientId] int  NOT NULL,
    [PatientCommunicationTypeId] int  NOT NULL,
	[CommunicationMethodTypeId] int  NOT NULL,
    [PatientAddressId] bigint  NULL,
    [IsAddressedToRecipient] bit  NULL,
    [EmailAddressId] int  NULL,
    [PatientPhoneNumberId] int  NULL,    
    [__EntityType__] nvarchar(100)  NOT NULL
);
GO


-- Need to create the model.PatientAddresses view, if it not already created

DECLARE @rollback bit
SET @rollback = 0

DECLARE @createIndexes bit
SET @createIndexes = 0


IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'model.PatientAddresses') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

	DECLARE @isPatientAddressesChanged bit 
	SET @isPatientAddressesChanged = 0

	
	DECLARE @PatientAddressesViewSql nvarchar(max) 
	SET @PatientAddressesViewSql = '
	CREATE VIEW [model].[PatientAddresses]
	
	AS
	----Patient''s first address
	SELECT  pd.PatientId AS Id,
		COALESCE(pd.City, '''') AS City,
		COALESCE(pd.Address, '''') AS Line1,
		pd.Suite AS Line2,
		CONVERT(NVARCHAR, NULL) AS Line3,
		PatientId as PatientId,
		COALESCE(REPLACE(pd.Zip,''-'',''''), '''') AS PostalCode,
		st.ID AS StateOrProvinceId,
		1 AS PatientAddressTypeId
	FROM dbo.PatientDemographicsTable pd
	INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pd.STATE
	WHERE pd.Address <> ''''
		OR pd.City <> ''''
		OR pd.State <> ''''
	
	UNION ALL
	
	----Patient''s 2nd address, PatientDemoAlt table
	SELECT (CONVERT(bigint, 110000000) * 1 + pdAlt.PatientId) AS Id,
		pdAlt.City AS City,
		pdAlt.Address AS Line1,
		pdAlt.Suite AS Line2,
		CONVERT(NVARCHAR, NULL) AS Line3,
		PatientId as PatientId,
		REPLACE(pdAlt.Zip,''-'','''') AS PostalCode,
		st.ID AS StateOrProvinceId,
		at.Id AS PatientAddressTypeId
	FROM dbo.PatientDemoAlt pdAlt
	INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pdAlt.STATE
	LEFT OUTER JOIN model.PatientAddressTypes at ON at.Name = ''Other1''
	WHERE pdAlt.AltId = 1
		AND pdalt.Address <> ''''
	
	UNION ALL
	
	----Patient''s 3rd address, PatientDemoAlt table
	SELECT (CONVERT(bigint, 110000000) * 2 + pdAlt.PatientId) AS Id,
		pdAlt.City AS City,
		pdAlt.Address AS Line1,
		pdAlt.Suite AS Line2,
		CONVERT(NVARCHAR, NULL) AS Line3,
		PatientId AS PatientId,
		REPLACE(pdAlt.Zip,''-'','''') AS PostalCode,
		st.ID AS StateOrProvinceId,
		at.Id AS PatientAddressTypeId
	FROM dbo.PatientDemoAlt pdAlt
	INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pdAlt.STATE
	LEFT OUTER JOIN model.PatientAddressTypes at ON at.Name = ''Other2''
	WHERE pdAlt.AltId = 2
		AND pdalt.Address <> ''''
	
	UNION ALL
	
	----Patient''s work address
	SELECT (CONVERT(bigint, 110000000) * 3 + pdWork.PatientId) AS Id,
		pdWork.BusinessCity AS City,
		pdWork.BusinessAddress AS Line1,
		pdWork.BusinessSuite AS Line2,
		CONVERT(NVARCHAR, NULL) AS Line3,
		pdWork.PatientId AS PatientId,
		REPLACE(pdWork.BusinessZip,''-'','''') AS PostalCode,
		st.ID AS StateOrProvinceId,
		2 AS PatientAddressTypeId
	FROM dbo.PatientDemographicsTable pdWork
	INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pdWork.BusinessState
	WHERE pdWork.BusinessAddress <> ''''
		and pdWork.BusinessAddress IS NOT NULL'
	
	EXEC [dbo].[RecreateIfDefinitionChanged] 'model.PatientAddresses', @PatientAddressesViewSql, @isChanged = @isPatientAddressesChanged OUTPUT, @error = @rollback OUTPUT
	
END

-- Migrate existing data from dbo.PatientDemographics
INSERT INTO [model].[PatientCommunicationPreferences] (PatientId, PatientCommunicationTypeId, CommunicationMethodTypeId, PatientAddressId, IsAddressedToRecipient, __EntityType__)
SELECT insertPatientId, insertType, insertMethod, insertAddressId, insertIsAddressed, insertPCPType FROM
(SELECT 
pdo.PatientId AS insertPatientId, 
3 AS insertType /* Statement */, 
3 AS insertMethod /* Letter */,
CASE pdo.BillParty
	WHEN 'N' THEN
		CASE pdo.SecondBillParty
			WHEN 'N' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
			WHEN 'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.SecondPolicyPatientId ORDER BY pad.Id)
			ELSE (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
		END
	WHEN 'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PolicyPatientId ORDER BY pad.Id)
	ELSE 
		CASE pdo.SecondBillParty
			WHEN 'N' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
			WHEN 'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.SecondPolicyPatientId ORDER BY pad.Id)
			ELSE (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
		END	
END AS insertAddressId,
0 AS insertIsAddressed, 'PatientAddressCommunicationPreference' AS insertPCPType
FROM [dbo].[PatientDemographics] pdo) as insertTable1
WHERE insertAddressId IS NOT NULL


GO

INSERT INTO [model].[PatientCommunicationPreferences] (PatientId, PatientCommunicationTypeId, CommunicationMethodTypeId, PatientAddressId, IsAddressedToRecipient, __EntityType__)
SELECT insertPatientId, insertType, insertMethod, insertAddressId, insertIsAddressed, insertPCPType FROM
(SELECT 
pdo.PatientId AS insertPatientId, 
1 AS insertType /* Recall */, 
3 AS insertMethod /* Letter */,
CASE pdo.BillParty
	WHEN 'N' THEN
		CASE pdo.SecondBillParty
			WHEN 'N' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
			WHEN 'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.SecondPolicyPatientId ORDER BY pad.Id)
			ELSE (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
		END
	WHEN 'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PolicyPatientId ORDER BY pad.Id)
	ELSE 
		CASE pdo.SecondBillParty
			WHEN 'N' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
			WHEN 'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.SecondPolicyPatientId ORDER BY pad.Id)
			ELSE (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
		END	
END AS insertAddressId,
0 AS insertIsAddressed, 'PatientAddressCommunicationPreference' AS insertPCPType
FROM [dbo].[PatientDemographics] pdo) as insertTable1
WHERE insertAddressId IS NOT NULL

-- Creating primary key on [Id] in table 'PatientCommunicationPreferences'
ALTER TABLE [model].[PatientCommunicationPreferences]
ADD CONSTRAINT [PK_PatientCommunicationPreferences]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO



-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPatientCommunicationPreference'
CREATE INDEX [IX_FK_PatientPatientCommunicationPreference]
ON [model].[PatientCommunicationPreferences]
    ([PatientId]);
GO


-- Creating non-clustered index for FOREIGN KEY 'FK_PatientAddressPatientAddressCommunicationPreference'
CREATE INDEX [IX_FK_PatientAddressPatientAddressCommunicationPreference]
ON [model].[PatientCommunicationPreferences]
    ([PatientAddressId]);
GO


-- Creating non-clustered index for FOREIGN KEY 'FK_EmailAddressPatientEmailAddressCommunicationPreference'
CREATE INDEX [IX_FK_PatientEmailAddressPatientEmailAddressCommunicationPreference]
ON [model].[PatientCommunicationPreferences]
    ([EmailAddressId]);
GO



-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPhoneNumberPatientPhoneNumberCommunicationPreference'
CREATE INDEX [IX_FK_PatientPhoneNumberPatientPhoneNumberCommunicationPreference]
ON [model].[PatientCommunicationPreferences]
    ([PatientCommunicationTypeId]);
GO
