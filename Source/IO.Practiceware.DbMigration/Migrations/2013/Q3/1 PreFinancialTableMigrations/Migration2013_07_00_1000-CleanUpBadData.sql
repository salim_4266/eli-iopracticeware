﻿--This is needed because there was a database with the word null instead of NULL
IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit

SELECT 1 AS Value INTO #NoAudit

UPDATE dbo.PatientDemographics
SET Address = ''
WHERE Address = 'NULL'

UPDATE dbo.PatientDemographics
SET City = ''
WHERE City = 'NULL'

UPDATE dbo.PatientDemographics
SET State = ''
WHERE State = 'NU'

UPDATE dbo.PatientDemographics
SET CellPhone = ''
WHERE CellPhone = 'NULL'

UPDATE dbo.PatientDemographics
SET BusinessPhone = ''
WHERE BusinessPhone= 'NULL'

DROP TABLE #NoAudit
