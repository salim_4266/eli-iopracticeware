﻿SELECT 1 AS Value INTO #NoCheck

SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] ON

-- Need to check if rows exist, because they may have been automatically previously inserted by migration Migration2013_08_20_0001-FixExternalSystemMessageTypes

IF NOT EXISTS(SELECT * FROM model.ExternalSystemMessageTypes WHERE Id = 27)
INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description], IsOutbound)
VALUES (27, 'EncounterClinicalSummaryCCDA', 'Encounter Clinical Summary C-CDA', 1)

IF NOT EXISTS(SELECT * FROM model.ExternalSystemMessageTypes WHERE Id = 28)
INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description], IsOutbound)
VALUES (28, 'PatientTransitionOfCareCCDA', 'Patient Transition of Care C-CDA', 1)

IF NOT EXISTS(SELECT * FROM model.ExternalSystemMessageTypes WHERE Id = 29)
INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description], IsOutbound)
VALUES (29, 'PatientTransitionOfCareCCDA', 'Patient Transition of Care C-CDA', 0)

SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] OFF

DROP TABLE #NoCheck