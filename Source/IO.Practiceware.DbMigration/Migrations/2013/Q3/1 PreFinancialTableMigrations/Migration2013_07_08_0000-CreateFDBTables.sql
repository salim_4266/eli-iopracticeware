﻿/* Create FDB tables*/


IF SCHEMA_ID('fdb') IS NULL EXEC('CREATE SCHEMA fdb')

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fdb].[HealthplanDetail]') AND type in (N'U'))
BEGIN
CREATE TABLE [fdb].[HealthplanDetail](
	[HealthplanDetailID] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](2) NULL,
	[Country] [nvarchar](2) NOT NULL,
	[Zip] [nvarchar](5) NULL,
	[Zip4] [nvarchar](4) NULL,
	[Phone] [nvarchar](20) NULL,
	[FormularyType] [nvarchar](10) NULL,
	[OrganizationID] [nvarchar](15) NULL,
	[FormularyID] [nvarchar](15) NULL,
	[Status] [nvarchar](2) NOT NULL,
	[TouchDate] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [fdb].[HealthplanSummary]******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fdb].[HealthplanSummary]') AND type in (N'U'))
BEGIN
CREATE TABLE [fdb].[HealthplanSummary](
	[HealthplanSummaryID] [int] NOT NULL,
	[ConsolidatedName] [nvarchar](80) NOT NULL,
	[HealthplanDetailID] [int] NULL,
	[State] [nvarchar](2) NULL,
	[Country] [nvarchar](2) NOT NULL,
	[SummaryCount] [bit] NOT NULL,
	[Source] [nvarchar](1) NOT NULL,
	[Status] [nvarchar](1) NOT NULL,
	[TouchDate] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [fdb].[RFMLINM0]******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fdb].[RFMLINM0]') AND type in (N'U'))
BEGIN
CREATE TABLE [fdb].[RFMLINM0](
	[ICD9CM] [nvarchar](10) NOT NULL,
	[ICD9CM_HCFADESC] [nvarchar](50) NULL,
	[ICD9CM_DESC100] [nvarchar](100) NULL,
	[ICD9CM_TYPE_CODE] [nvarchar](2) NOT NULL,
	[ICD9CM_SOURCE_CODE] [nvarchar](2) NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [fdb].[tblCompositeAllergy]******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fdb].[tblCompositeAllergy]') AND type in (N'U'))
BEGIN
CREATE TABLE [fdb].[tblCompositeAllergy](
	[CompositeAllergyID] [int] NOT NULL,
	[Source] [nvarchar](1) NOT NULL,
	[ConceptID] [nvarchar](10) NOT NULL,
	[ConceptType] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Status] [nvarchar](1) NOT NULL,
	[TouchDate] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [fdb].[tblCompositeDrug]******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fdb].[tblCompositeDrug]') AND type in (N'U'))
BEGIN
CREATE TABLE [fdb].[tblCompositeDrug](
	[MEDID] [int] NOT NULL,
	[GCN_SEQNO] [int] NOT NULL,
	[MED_NAME_ID] [int] NOT NULL,
	[MED_NAME] [nvarchar](30) NOT NULL,
	[MED_ROUTED_MED_ID_DESC] [nvarchar](60) NOT NULL,
	[MED_ROUTED_DF_MED_ID_DESC] [nvarchar](60) NOT NULL,
	[MED_MEDID_DESC] [nvarchar](70) NOT NULL,
	[MED_STATUS_CD] [nvarchar](1) NOT NULL,
	[MED_ROUTE_ID] [int] NOT NULL,
	[ROUTED_MED_ID] [int] NOT NULL,
	[ROUTED_DOSAGE_FORM_MED_ID] [int] NOT NULL,
	[MED_STRENGTH] [nvarchar](15) NULL,
	[MED_STRENGTH_UOM] [nvarchar](15) NULL,
	[MED_ROUTE_ABBR] [nvarchar](4) NOT NULL,
	[MED_ROUTE_DESC] [nvarchar](30) NOT NULL,
	[MED_DOSAGE_FORM_ABBR] [nvarchar](4) NOT NULL,
	[MED_DOSAGE_FORM_DESC] [nvarchar](30) NOT NULL,
	[GenericDrugName] [nvarchar](30) NULL,
	[DosageFormOverride] [nvarchar](50) NULL,
	[MED_REF_DEA_CD] [nvarchar](1) NOT NULL,
	[MED_REF_DEA_CD_DESC] [nvarchar](60) NULL,
	[MED_REF_MULTI_SOURCE_CD] [nvarchar](1) NOT NULL,
	[MED_REF_MULTI_SOURCE_CD_DESC] [nvarchar](90) NULL,
	[MED_REF_GEN_DRUG_NAME_CD] [nvarchar](1) NOT NULL,
	[MED_REF_GEN_DRUG_NAME_CD_DESC] [nvarchar](90) NULL,
	[MED_REF_FED_LEGEND_IND] [nvarchar](1) NOT NULL,
	[MED_REF_FED_LEGEND_IND_DESC] [nvarchar](60) NULL,
	[GENERIC_MEDID] [int] NULL,
	[MED_NAME_TYPE_CD] [nvarchar](1) NOT NULL,
	[GENERIC_MED_REF_GEN_DRUG_NAME_CD] [nvarchar](1) NOT NULL,
	[MED_NAME_SOURCE_CD] [nvarchar](1) NOT NULL,
	[etc] [text] NULL,
	[DrugInfo] [nvarchar](70) NULL,
	[GenericDrugNameOverride] [nvarchar](70) NULL,
	[FormularyDrugID] [int] NULL,
	[Manufacturer] [nvarchar](15) NULL,
	[Status] [nvarchar](1) NULL,
	[TouchDate] [nvarchar](8) NULL,
	[DrugTypeID] [nvarchar](1) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [fdb].[tblPharmacy]******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fdb].[tblPharmacy]') AND type in (N'U'))
BEGIN
CREATE TABLE [fdb].[tblPharmacy](
	[PharmacyGuid] [uniqueidentifier] NOT NULL,
	[Pharmacy_ID] [int] NULL,
	[Pharmacy_Status] [nvarchar](1) NULL,
	[Pharmacy_NCPDP] [nvarchar](10) NOT NULL,
	[Pharmacy_Store] [nvarchar](20) NULL,
	[Pharmacy_StoreName] [nvarchar](50) NULL,
	[Pharmacy_Address1] [nvarchar](100) NULL,
	[Pharmacy_Address2] [nvarchar](100) NULL,
	[Pharmacy_Telephone1] [nvarchar](20) NULL,
	[Pharmacy_Telephone2] [nvarchar](20) NULL,
	[Pharmacy_Fax] [nvarchar](20) NULL,
	[Pharmacy_PhoneAlt1] [nvarchar](25) NULL,
	[Pharmacy_PhoneAlt2] [nvarchar](25) NULL,
	[Pharmacy_PhoneAlt3] [nvarchar](25) NULL,
	[Pharmacy_PhoneAlt4] [nvarchar](25) NULL,
	[Pharmacy_PhoneAlt5] [nvarchar](25) NULL,
	[Pharmacy_City] [nvarchar](100) NULL,
	[Pharmacy_StateID] [int] NULL,
	[Pharmacy_Zip] [nvarchar](15) NULL,
	[Pharmacy_TouchDate] [datetime] NULL,
	[Pharmacy_StateAbbr] [nvarchar](2) NULL,
	[Pharmacy_TypeID] [int] NULL,
	[Pharmacy_DetailTypeID] [bit] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [fdb].[viewBlackBoxFDB]******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fdb].[viewBlackBoxFDB]') AND type in (N'U'))
BEGIN
CREATE TABLE [fdb].[viewBlackBoxFDB](
	[GCN_SEQNO] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [fdb].[viewWSDrug]******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fdb].[viewWSDrug]') AND type in (N'U'))
BEGIN
CREATE TABLE [fdb].[viewWSDrug](
	[dataProvider] [nvarchar](1) NULL,
	[drug] [nvarchar](70) NOT NULL,
	[DrugID] [int] NOT NULL,
	[DrugSubID1] [int] NULL,
	[DrugName] [nvarchar](30) NOT NULL,
	[DrugNameID] [int] NULL,
	[GenericName] [nvarchar](30) NULL,
	[DeaClassCode] [nvarchar](1) NULL,
	[dosage] [nvarchar](31) NOT NULL,
	[DosageForm] [nvarchar](30) NULL,
	[Route] [nvarchar](30) NULL,
	[TheraputicCategory] [text] NULL,
	[Status] [nvarchar](1) NULL,
	[TouchDate] [nvarchar](8) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__HealthplanDetail%') 
                ALTER TABLE fdb.HealthplanDetail
                ADD CONSTRAINT PK__HealthplanDetail PRIMARY KEY ([HealthplanDetailID])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__HealthplanSummar%') 
                ALTER TABLE [fdb].[HealthplanSummary]
                ADD CONSTRAINT PK__HealthplanSummary PRIMARY KEY ([HealthplanSummaryID])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__RFMLINM0%') 
                ALTER TABLE [fdb].[RFMLINM0]
                ADD CONSTRAINT PK__RFMLINM0 PRIMARY KEY ([ICD9CM])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__tblCompositeAlle%')
                ALTER TABLE [fdb].[tblCompositeAllergy]
                ADD CONSTRAINT PK__tblCompositeAllergy PRIMARY KEY ([CompositeAllergyID])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__tblCompositeDrug%') 
                ALTER TABLE [fdb].[tblCompositeDrug]
                ADD CONSTRAINT PK__tblCompositeDrug PRIMARY KEY ([MEDID])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__tblPharmacy%') 
                ALTER TABLE [fdb].[tblPharmacy]
                ADD CONSTRAINT PK__tblPharmacy PRIMARY KEY ([PharmacyGuid])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'%viewBlackBoxFDB%') 
                ALTER TABLE [fdb].[viewBlackBoxFDB]
                ADD CONSTRAINT PK__viewBlackBoxFDB PRIMARY KEY ([GCN_SEQNO])

                IF NOT EXISTS (SELECT name from sys.indexes
                    WHERE name LIKE N'PK__viewWSDrug%') 
                ALTER TABLE [fdb].[viewWSDrug]
                ADD CONSTRAINT PK__viewWSDrug PRIMARY KEY ([DrugID], [DrugName])
