﻿/* Bug #6566.Add MaximumApplyTemplateMonths Column for model.ScheduleTemplateBuliderConfigurations. */

IF NOT EXISTS (
		SELECT *
		FROM sysobjects so
		INNER JOIN syscolumns sc ON so.id = sc.id
		WHERE so.NAME = 'ScheduleTemplateBuilderConfigurations'
			AND sc.NAME = 'MaximumApplyTemplateMonths'
		)
BEGIN
	ALTER TABLE model.ScheduleTemplateBuilderConfigurations ADD MaximumApplyTemplateMonths INT NOT NULL DEFAULT(12);
END