﻿


-- Adds UserDefinedLastUpdated DateTime to the Patient table - allows user to enter the last time they 'officially' updated the patient demographics
IF NOT EXISTS(select * from sys.columns where Name = N'UserDefinedLastUpdated' and Object_ID = Object_ID(N'model.Patients')) 
BEGIN
	ALTER TABLE [model].[Patients]
	ADD UserDefinedLastUpdated DATETIME
END

GO

-- Adds PreferredServiceLocationId to the Patient table 
IF NOT EXISTS(select * from sys.columns where Name = N'PreferredServiceLocationId' and Object_ID = Object_ID(N'model.Patients')) 
BEGIN
	ALTER TABLE [model].[Patients]
	ADD PreferredServiceLocationId INT NULL
END



-- Make hipaa bool to non-nullable
IF EXISTS(select * from sys.columns where Name = N'IsHipaaConsentSigned' and Object_ID = Object_ID(N'model.Patients')) 
BEGIN

	UPDATE [model].[Patients] SET IsHipaaConsentSigned = 0 WHERE IsHipaaConsentSigned IS NULL

	ALTER TABLE [model].[Patients]
	ALTER COLUMN IsHipaaConsentSigned BIT NOT NULL	

END

GO

-- Check if default constraint exists
IF NOT EXISTS(SELECT * FROM sysobjects WHERE xtype = 'D' AND name = 'constring_default_ishipaaconsentsigned')
BEGIN
	ALTER TABLE [model].[Patients] ADD CONSTRAINT constring_default_ishipaaconsentsigned DEFAULT 0 FOR IsHipaaConsentSigned;
END

GO
