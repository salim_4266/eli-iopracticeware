﻿-- Need to update the UpdateLegacyCommunicationPreference trigger as it previously called back on the PatientDemographic, unnecessarily doing another update.
-- (This unnecessary update would create an extra ExternalSystemMessage whose contents stated that a patient was updated.
IF OBJECT_ID('[model].[UpdateLegacyCommunicationPreference]', 'TR') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER [model].[UpdateLegacyCommunicationPreference]')
END

GO
