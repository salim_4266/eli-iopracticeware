﻿/* Drops model.addresses and model.addresstypes since they
were not deleted when the addresses were split up*/

IF OBJECT_ID('model.Addresses') IS NOT NULL
	DROP VIEW model.Addresses

IF OBJECT_ID('model.AddressTypes') IS NOT NULL
	DROP TABLE model.AddressTypes