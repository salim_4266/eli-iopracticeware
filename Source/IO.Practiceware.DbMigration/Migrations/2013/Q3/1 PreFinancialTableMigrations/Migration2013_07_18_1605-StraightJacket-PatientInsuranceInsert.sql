﻿
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('[model].[OnPatientInsurancesInsert_StraightJacket]', 'TR') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER [model].[OnPatientInsurancesInsert_StraightJacket]')
END

GO

CREATE TRIGGER [model].[OnPatientInsurancesInsert_StraightJacket] ON [model].[PatientInsurances] AFTER INSERT
		AS 

		DECLARE 
		@insurancePolicyId INT,
		@patientId INT,
		@policyHolderPatientId INT,
		
		@insurerIdToInsert INT,
		@insurancePolicyIdToInsert INT,

		@allowDependants BIT,
		@insuranceTypeIdToInsert INT,
		@policyHolderRelationshipTypeIdToInsert INT,
		@ordinalIdToInsert INT,
		@endDateTimeToInsert DATETIME	

		BEGIN
			SELECT @insurancePolicyId = i.InsurancePolicyId, @patientId = i.InsuredPatientId, @policyHolderRelationshipTypeIdToInsert = i.PolicyHolderRelationshipTypeId from inserted i		

			SELECT @policyHolderPatientId = ip.PolicyHolderPatientId FROM [model].[InsurancePolicies] ip WHERE ip.Id = @insurancePolicyId;
		
			IF @patientId <> @policyHolderPatientId /* If it is NOT a self policy we need to add insurances for all policies under the Policy Holder we're linking to */
			BEGIN
				DECLARE insurancePolicyCursor CURSOR FOR
				SELECT DISTINCT ip.InsurerId, ip.Id FROM model.InsurancePolicies ip 
				WHERE ip.PolicyHolderPatientId = @policyHolderPatientId AND ip.Id <> @insurancePolicyId AND ip.Id NOT IN (SELECT pin.InsurancePolicyId FROM PatientInsurances pin WHERE pin.InsuredPatientId = @patientId and pin.IsDeleted = 0 )

				OPEN insurancePolicyCursor
				FETCH NEXT FROM insurancePolicyCursor INTO @insurerIdToInsert, @insurancePolicyIdToInsert 

				WHILE @@FETCH_STATUS = 0 /* Loop through the Policy Holder's Insurance Policies  */
				BEGIN
					SELECT @allowDependants = AllowDependents FROM model.Insurers WHERE Id = @insurerIdToInsert

					IF @allowDependants = 1 
					BEGIN
						IF EXISTS (SELECT 1 FROM model.PatientInsurances pin WHERE pin.InsurancePolicyId = @insurancePolicyIdToInsert AND pin.InsurancePolicyId <> @insurancePolicyId AND pin.IsDeleted = 0)
						BEGIN
							/* Select the patient insurance linked to this current Policy */
							SELECT TOP 1 @insuranceTypeIdToInsert = pin.InsuranceTypeId,
											@endDateTimeToInsert = pin.EndDateTime
							FROM model.PatientInsurances pin 
							WHERE pin.InsurancePolicyId = @insurancePolicyIdToInsert AND pin.InsurancePolicyId <> @insurancePolicyId AND pin.IsDeleted = 0
						
							SELECT @ordinalIdToInsert = MAX(pin.OrdinalId) + 1
							FROM model.PatientInsurances pin 
							WHERE pin.InsuredPatientId = @patientId

							INSERT INTO model.PatientInsurances (InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
							VALUES (@insuranceTypeIdToInsert, @policyHolderRelationshipTypeIdToInsert, @ordinalIdToInsert, @patientId, @insurancePolicyIdToInsert, @endDateTimeToInsert, 0)
						END
					END
					FETCH NEXT FROM insurancePolicyCursor INTO @insurerIdToInsert, @insurancePolicyIdToInsert 
				END			
				CLOSE insurancePolicyCursor;
				DEALLOCATE insurancePolicyCursor;
			END
			/* Examples Given:					
			1. I'm Joe. I add Jane's policy for Major Medical. I also get PatientInsurances for all of Jane's other policies where she is the policy holder and 
			those policies' insurers' allow dependents. (this is across ALL insurance types).
			 */

			SET NOCOUNT OFF

		END	

GO