﻿EXEC('
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N''[model].[PatientsExternalSystemMessagesTrigger]'') AND OBJECTPROPERTY(id, N''IsTrigger'') = 1)
	DROP TRIGGER [model].PatientsExternalSystemMessagesTrigger
')

EXEC('
CREATE TRIGGER model.PatientsExternalSystemMessagesTrigger
	ON  model.Patients
	FOR INSERT, UPDATE NOT FOR REPLICATION
AS 
BEGIN
SET NOCOUNT ON;

	DECLARE @now as DATETIME
	SET @now = GETDATE()

	DECLARE @insertTable TABLE(Id UNIQUEIDENTIFIER, MessageType INT, PatientId INT, [Description] NVARCHAR(255))
	DECLARE @createdBy nvarchar(max)

		INSERT INTO @insertTable 
		SELECT NEWID(), esesmt.Id, i.Id AS PatientId, ESMT.Name
		FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i
		CROSS JOIN model.ExternalSystemExternalSystemMessageTypes esesmt
		INNER JOIN model.ExternalSystemMessageTypes esmt ON esesmt.ExternalSystemMessageTypeId = esmt.Id AND esmt.Name IN (''ADT_A28_V23'') AND esmt.IsOutbound = 1

	-- Create rows for each ExternalSystemExternalSystemMessageType subscribed for ADT_A28_V23
	-- If deleted table contains rows, then this was an UPDATE
	IF EXISTS(SELECT * FROM deleted)
	BEGIN
		SET @createdBy = ''PatientUpdate''
	END
	ELSE
	BEGIN
		SET @createdBy = ''PatientInsert''
	END

	-- Create the messages
	INSERT INTO model.ExternalSystemMessages(Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId, ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId)
	-- Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId (1 = Unprocessed, 2 = Processing, 3 = Processed),
	-- ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId
	SELECT Id, [Description], '''', MessageType, 1, 0, @createdBy, @now, @now, ''''
	FROM @insertTable

	-- Add entity associated with each message for PatientId
	INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
	-- Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey
	SELECT NEWID(), it.Id, pre.Id, PatientId
	FROM
	@insertTable it INNER JOIN model.PracticeRepositoryEntities pre ON pre.Name = ''Patient''
END
')
GO
