﻿SET IDENTITY_INSERT [model].[Permissions] ON

	INSERT INTO [model].[Permissions] (Id, Name) VALUES (24, 'EditInsurancePlanInformation')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (25, 'ManageInsurancePlanPatientPayments')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (26, 'ReactivateInsurancePlans')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (27, 'CreateInsurancePlans')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (28, 'MergeInsurancePlans')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (29, 'GroupUpdateOfInsurancePlans')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (30, 'ArchiveInsurancePlans')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (31, 'DeleteInsurancePlans')

SET IDENTITY_INSERT [model].[Permissions] OFF
GO

-- Fix the default value for ViewInsurersAndPlans (#22) permission (first inserted in Migration2013_05_14_0550-Permissions)
UPDATE model.UserPermissions
SET IsDenied = CONVERT(bit,0)
WHERE PermissionId = 22 AND UserId IN (
	SELECT ResourceId
	FROM dbo.Resources
	WHERE ResourceType <> 'R' AND SUBSTRING(ResourcePermissions, 41, 1) = 1 -- Set Up Plans
)
GO

-- Provide defaults based on existing permissions
INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 41, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 24 -- 'EditInsurancePlanInformation'

UNION ALL

SELECT CONVERT(bit,1) AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 25 -- 'ManageInsurancePlanPatientPayments'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 41, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 26 -- 'ReactivateInsurancePlans'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 41, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 27 -- 'CreateInsurancePlans'

UNION ALL

SELECT CONVERT(bit,1) AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 28 -- 'MergeInsurancePlans'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 53, 1) = 1 --Allow Aggr Ins Changes
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 29 -- 'GroupUpdateOfInsurancePlans'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 41, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 30 -- 'ArchiveInsurancePlans'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 41, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 31 -- 'DeleteInsurancePlans'

GO
