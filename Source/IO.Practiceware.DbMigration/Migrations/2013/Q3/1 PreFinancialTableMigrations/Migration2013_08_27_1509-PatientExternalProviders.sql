﻿EXEC('
IF EXISTS(SELECT * FROM sys.views WHERE name = ''PatientExternalProviders'')
BEGIN
	DROP VIEW model.PatientExternalProviders
END
')

IF OBJECT_ID(N'[model].[PatientExternalProviders]', 'U') IS NULL
BEGIN
	-- Create PatientExternalProviders table
	EXEC('
	CREATE TABLE [model].[PatientExternalProviders] (
		[Id] int IDENTITY(1,1) NOT NULL,
		[ExternalProviderId] int  NOT NULL, -- TODO Create FK to Contacts table when it is no longer a view
		[IsPrimaryCarePhysician] bit  NOT NULL,
		[IsReferringPhysician] bit  NOT NULL,
		[PatientId] int  NULL FOREIGN KEY REFERENCES model.Patients(Id),
	);
	')

	EXEC('
	-- Creating primary key on [Id] in table ''PatientExternalProviders''
	ALTER TABLE [model].[PatientExternalProviders]
	ADD CONSTRAINT [PK_PatientExternalProviders]
		PRIMARY KEY CLUSTERED ([Id] ASC);

	-- Creating non-clustered index for FOREIGN KEY ''FK_PatientExternalProviderExternalProvider''
	CREATE INDEX [IX_FK_PatientExternalProviderExternalProvider]
	ON [model].[PatientExternalProviders]
		([ExternalProviderId]);

	-- Creating non-clustered index for FOREIGN KEY ''FK_PatientPatientExternalProvider''
	CREATE INDEX [IX_FK_PatientPatientExternalProvider]
	ON [model].[PatientExternalProviders]
		([PatientId]);
	')

	-- Migrate PatientExternalProviders
	EXEC('
		INSERT INTO model.PatientExternalProviders (ExternalProviderId, IsPrimaryCarePhysician, IsReferringPhysician, PatientId) 
		----ReferringPhysician
		SELECT ReferringPhysician AS ExternalProviderId,
			CONVERT(bit, 0) AS IsPrimaryCarePhysician,
			CONVERT(bit, 1) AS IsReferringPhysician,
			PatientId AS PatientId
		FROM dbo.PatientDemographicsTable pd
		INNER JOIN dbo.PracticeVendors pv ON pd.ReferringPhysician = pv.VendorId
		WHERE pd.ReferringPhysician <> ''''
			AND pv.VendorLastName <> ''''
			AND pv.VendorLastName IS NOT NULL


		UNION ALL

		----PrimaryCarePhysician
		SELECT PrimaryCarePhysician AS ExternalProviderId,
			CONVERT(bit, 1) AS IsPrimaryCarePhysician,
			CONVERT(bit, 0) AS IsReferringPhysician,
			PatientId AS PatientId
		FROM dbo.PatientDemographicsTable pd
		INNER JOIN dbo.PracticeVendors pv ON pd.PrimaryCarePhysician = pv.VendorId
		WHERE PrimaryCarePhysician <> ''''
			AND pv.VendorLastName <> ''''
			AND pv.VendorLastName IS NOT NULL	
	')
END
GO
	