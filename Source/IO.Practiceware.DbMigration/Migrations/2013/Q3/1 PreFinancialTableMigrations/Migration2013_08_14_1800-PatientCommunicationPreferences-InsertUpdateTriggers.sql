﻿

-- insert and update triggers for the PatientCommunicationPreferences table which updates the dbo.PatientDemographics table
-- to keep certain preferences in sync with the old schema so that vb6 could still access that data


SET ANSI_NULLS ON
GO

IF OBJECT_ID('[model].[OnPatientCommunicationPreferencesInsert]', 'TR') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER [model].[OnPatientCommunicationPreferencesInsert]')
END

GO

IF OBJECT_ID('[model].[UpdateLegacyCommunicationPreference]', 'TR') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER [model].[UpdateLegacyCommunicationPreference]')
END

GO

IF OBJECT_ID('[model].[OnPatientCommunicationPreferencesUpdate]', 'TR') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER [model].[OnPatientCommunicationPreferencesUpdate]')
END

GO

CREATE TRIGGER [model].UpdateLegacyCommunicationPreference ON [model].PatientCommunicationPreferences AFTER INSERT, UPDATE
		AS 

		DECLARE 		
		@patientId INT,
		@patientCommunicationTypeId INT,
		@patientAddressId INT,
		@patientIdAddressBelongsTo INT,
		@policyHolderId INT,
		@firstPolicyHolderId INT,
		@secondPolicyHolderId INT
		

			BEGIN
			
			SELECT @patientId = i.PatientId, @patientCommunicationTypeId = i.PatientCommunicationTypeId, @patientAddressId = PatientAddressId FROM inserted i	
				
			IF @patientCommunicationTypeId = 3 AND @patientAddressId IS NOT NULL /* Logic only applies to 'Statement' communication type  */
			BEGIN
				SELECT @patientIdAddressBelongsTo = pad.PatientId FROM model.PatientAddresses pad WHERE pad.Id = @patientAddressId

				UPDATE pdo SET pdo.BillParty = 
					CASE 
						WHEN pdo.PolicyPatientId = @patientIdAddressBelongsTo THEN 'Y'
						ELSE 'N'
					END,
					pdo.SecondBillParty =
						CASE
							WHEN pdo.SecondPolicyPatientId = @patientIdAddressBelongsTo THEN 'Y'
							ELSE 'N'
						END
				FROM dbo.PatientDemographics pdo WHERE pdo.PatientId=@patientId				
			END								
		END
		SET NOCOUNT OFF	
GO