﻿EXEC('
IF EXISTS(SELECT * FROM sys.views WHERE name = ''PatientReferralSourceTypes'')
BEGIN
	DROP VIEW model.PatientReferralSourceTypes
END
')

IF OBJECT_ID(N'[model].[PatientReferralSourceTypes]', 'U') IS NULL
BEGIN
	-- Create PatientReferralSourceTypes table
	EXEC('
	CREATE TABLE [model].[PatientReferralSourceTypes] (
		[Id] int IDENTITY(1,1) NOT NULL,
		[Name] nvarchar(max)  NOT NULL,
		[IsArchived] bit  NOT NULL
	);
	')

	-- Creating primary key on [Id] in table 'PatientReferralSourceTypes'
	EXEC('
	ALTER TABLE [model].[PatientReferralSourceTypes]
	ADD CONSTRAINT [PK_PatientReferralSourceTypes]
		PRIMARY KEY CLUSTERED ([Id] ASC);
	')
	-- Migrate PatientReferralSourceTypes
	EXEC('
	SET IDENTITY_INSERT [model].[PatientReferralSourceTypes] ON

		INSERT INTO model.PatientReferralSourceTypes (Id, Name, IsArchived)
			SELECT Id,
				Code AS [Name],
				CONVERT(bit, 0) AS IsArchived
			FROM dbo.PracticeCodeTable
			WHERE ReferenceType = ''REFERBY''
			
	SET IDENTITY_INSERT [model].[PatientReferralSourceTypes] OFF
	')
END
GO
	
EXEC('
IF EXISTS(SELECT * FROM sys.views WHERE name = ''PatientReferralSources'')
BEGIN
	DROP VIEW model.PatientReferralSources
END
')

IF OBJECT_ID(N'[model].[PatientReferralSources]', 'U') IS NULL
BEGIN
	-- Create PatientReferralSources table
	EXEC('
	CREATE TABLE [model].[PatientReferralSources] (
		[Id] int IDENTITY(1,1) NOT NULL PRIMARY KEY,
		[PatientId] int  NOT NULL FOREIGN KEY REFERENCES model.Patients(Id),
		[ReferralSourceTypeId] int  NOT NULL FOREIGN KEY REFERENCES model.PatientReferralSourceTypes(Id),
		[Comment] nvarchar(max) NULL
	);
	')

	EXEC('
	-- Creating non-clustered index for FOREIGN KEY ''FK_PatientReferralSourceTypePatientReferralSource''
	CREATE INDEX [IX_FK_PatientReferralSourceTypePatientReferralSource]
	ON [model].[PatientReferralSources]
		([ReferralSourceTypeId]);

	-- Creating non-clustered index for FOREIGN KEY ''FK_PatientPatientReferralSource''
	CREATE INDEX [IX_FK_PatientPatientReferralSource]
	ON [model].[PatientReferralSources]
		([PatientId]);
	')

	-- Migrate PatientReferralSources
	EXEC('
	SET IDENTITY_INSERT [model].[PatientReferralSources] ON

		INSERT INTO model.PatientReferralSources (Id, PatientId, ReferralSourceTypeId, Comment)
			SELECT 	PatientId AS Id,
			PatientId AS PatientId,
			pct.Id AS ReferralSourceTypeId,
			ProfileComment2 AS Comment
			FROM dbo.PatientDemographicsTable pd
			INNER JOIN dbo.PracticeCodeTable pct ON pct.Code = pd.ReferralCatagory
				AND ReferenceType = ''REFERBY''
			
	SET IDENTITY_INSERT [model].[PatientReferralSources] OFF
	')
END
GO
