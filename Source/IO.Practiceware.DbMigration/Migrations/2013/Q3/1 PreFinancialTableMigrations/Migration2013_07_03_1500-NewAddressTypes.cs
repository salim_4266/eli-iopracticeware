﻿using FluentMigrator;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// AddressType
    /// </summary>
    [Migration(201307031500)]
    public class Migration201307031500 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"

-- Creating table 'ContactAddressTypes'
CREATE TABLE [model].[ContactAddressTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [__EntityType__] nvarchar(100)  NOT NULL,
    [IsArchived] bit  NOT NULL,
);
GO

-- Creating primary key on [Id] in table 'ContactAddressTypes'
ALTER TABLE [model].[ContactAddressTypes]
ADD CONSTRAINT [PK_ContactAddressTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
");

            Execute.Sql(@"

-- Creating table 'PatientAddressTypes'
CREATE TABLE [model].[PatientAddressTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [__EntityType__] nvarchar(100)  NOT NULL,
    [IsArchived] bit  NOT NULL,
);
GO

-- Creating primary key on [Id] in table 'PatientAddressTypes'
ALTER TABLE [model].[PatientAddressTypes]
ADD CONSTRAINT [PK_PatientAddressTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
");

            Execute.Sql(@"

-- Creating table 'UserAddressTypes'
CREATE TABLE [model].[UserAddressTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [__EntityType__] nvarchar(100)  NOT NULL,
    [IsArchived] bit  NOT NULL,
);
GO

-- Creating primary key on [Id] in table 'UserAddressTypes'
ALTER TABLE [model].[UserAddressTypes]
ADD CONSTRAINT [PK_UserAddressTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
");

            Execute.Sql(@"

-- Creating table 'InsurerAddressTypes'
CREATE TABLE [model].[InsurerAddressTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [__EntityType__] nvarchar(100)  NOT NULL,
    [IsArchived] bit  NOT NULL,
);
GO

-- Creating primary key on [Id] in table 'InsurerAddressTypes'
ALTER TABLE [model].[InsurerAddressTypes]
ADD CONSTRAINT [PK_InsurerAddressTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
");

            Execute.Sql(@"

-- Creating table 'BillingOrganizationAddressTypes'
CREATE TABLE [model].[BillingOrganizationAddressTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [__EntityType__] nvarchar(100)  NOT NULL,
    [IsArchived] bit  NOT NULL,
);
GO

-- Creating primary key on [Id] in table 'BillingOrganizationAddressTypes'
ALTER TABLE [model].[BillingOrganizationAddressTypes]
ADD CONSTRAINT [PK_BillingOrganizationAddressTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
");

            Execute.Sql(@"

-- Creating table 'ExternalOrganizationAddressTypes'
CREATE TABLE [model].[ExternalOrganizationAddressTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [__EntityType__] nvarchar(100)  NOT NULL,
    [IsArchived] bit  NOT NULL,
);
GO

-- Creating primary key on [Id] in table 'ExternalOrganizationAddressTypes'
ALTER TABLE [model].[ExternalOrganizationAddressTypes]
ADD CONSTRAINT [PK_ExternalOrganizationAddressTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
");

            Execute.Sql(@"

-- Creating table 'ServiceLocationAddressTypes'
CREATE TABLE [model].[ServiceLocationAddressTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [__EntityType__] nvarchar(100)  NOT NULL,
    [IsArchived] bit  NOT NULL,
);
GO

-- Creating primary key on [Id] in table 'ServiceLocationAddressTypes'
ALTER TABLE [model].[ServiceLocationAddressTypes]
ADD CONSTRAINT [PK_ServiceLocationAddressTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
");

            Execute.Sql(@"
SET IDENTITY_INSERT [model].[PatientAddressTypes] ON
INSERT [model].[PatientAddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (1, 'Home', 'PatientAddressType', 0)
INSERT [model].[PatientAddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (2, 'Business', 'PatientAddressType', 0)
SET IDENTITY_INSERT [model].[PatientAddressTypes] OFF

SET IDENTITY_INSERT [model].[InsurerAddressTypes] ON
INSERT [model].[InsurerAddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (3, 'Claims', 'InsurerAddressType', 0)
INSERT [model].[InsurerAddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (4, 'Appeals', 'InsurerAddressType', 0)
SET IDENTITY_INSERT [model].[InsurerAddressTypes] OFF

SET IDENTITY_INSERT [model].[BillingOrganizationAddressTypes] ON
INSERT [model].[BillingOrganizationAddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (5, 'Physical Location','BillingOrganizationAddressType', 0)
INSERT [model].[BillingOrganizationAddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (6, 'Pay To','BillingOrganizationAddressType', 0)
SET IDENTITY_INSERT [model].[BillingOrganizationAddressTypes] OFF

SET IDENTITY_INSERT [model].[ServiceLocationAddressTypes] ON
INSERT [model].[ServiceLocationAddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (7, 'Main Office', 'ServiceLocationAddressType', 0)
SET IDENTITY_INSERT [model].[ServiceLocationAddressTypes] OFF

SET IDENTITY_INSERT [model].[ContactAddressTypes] ON
INSERT [model].[ContactAddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (8, 'Main Office', 'ContactAddressType', 0)
SET IDENTITY_INSERT [model].[ContactAddressTypes] OFF

SET IDENTITY_INSERT [model].[UserAddressTypes] ON
INSERT [model].[UserAddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (9, 'Home', 'UserAddressType', 0)
INSERT [model].[UserAddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (10, 'Work', 'UserAddressType', 0)
SET IDENTITY_INSERT [model].[UserAddressTypes] OFF

SET IDENTITY_INSERT [model].[ExternalOrganizationAddressTypes] ON
INSERT [model].[ExternalOrganizationAddressTypes]  (Id, Name, __EntityType__, [IsArchived]) VALUES (11, 'Main Office', 'ExternalOrganizationAddressType', 0)
SET IDENTITY_INSERT [model].[ExternalOrganizationAddressTypes] OFF


DBCC CHECKIDENT ('[model].[PatientAddressTypes]', 'RESEED', 1000)
DBCC CHECKIDENT ('[model].[InsurerAddressTypes]', 'RESEED', 1000)
DBCC CHECKIDENT ('[model].[ContactAddressTypes]', 'RESEED', 1000)
DBCC CHECKIDENT ('[model].[UserAddressTypes]', 'RESEED', 1000)
DBCC CHECKIDENT ('[model].[BillingOrganizationAddressTypes]', 'RESEED', 1000)
DBCC CHECKIDENT ('[model].[ServiceLocationAddressTypes]', 'RESEED', 1000)
DBCC CHECKIDENT ('[model].[ExternalOrganizationAddressTypes]', 'RESEED', 1000)

-- These can be edited or deleted by user
INSERT [model].[PatientAddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('Other1', 'PatientAddressType', 0)
INSERT [model].[PatientAddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('Other2', 'PatientAddressType', 0)
INSERT [model].[UserAddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('User1', 'UserAddressType', 0)
INSERT [model].[ContactAddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('Other1', 'ContactAddressType', 0)
INSERT [model].[ContactAddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('Other2', 'ContactAddressType', 0)
INSERT [model].[ExternalOrganizationAddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('Other1', 'ExternalOrganizationAddressType', 0)
INSERT [model].[ExternalOrganizationAddressTypes]  (Name, __EntityType__, [IsArchived]) VALUES ('Other2', 'ExternalOrganizationAddressType', 0)
");


        }

        public override void Down()
        {
        }
    }
}