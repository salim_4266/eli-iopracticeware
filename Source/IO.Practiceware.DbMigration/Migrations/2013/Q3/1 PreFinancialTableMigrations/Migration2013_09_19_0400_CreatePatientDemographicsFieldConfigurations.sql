﻿--Creation of Demographic Configurations tables.
IF OBJECT_ID(N'[model].[PatientDemographicsFieldConfigurations]', 'U') IS NOT NULL
    DROP TABLE [model].[PatientDemographicsFieldConfigurations];
GO
-- Creating table 'DemographicFieldConfigurations'
CREATE TABLE [model].[PatientDemographicsFieldConfigurations] (
    [Id] int IDENTITY(1,1) NOT NULL,    
	[IsVisibilityConfigurable] bit NOT NULL Default 0,
	[IsVisible] bit NOT NULL Default 1,
	[IsRequiredConfigurable] bit NOT NULL Default 0,
	[IsRequired] bit NOT NULL Default 0,
	[DefaultValue] nvarchar(max) NULL
);
GO

-- Creating primary key on [Id] in table 'PatientDemographicsFieldConfigurations'
ALTER TABLE [model].[PatientDemographicsFieldConfigurations]
ADD CONSTRAINT [PK_PatientDemographicsFieldConfigurations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- 1 - 'Title'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

-- 2 - 'First Name'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);

-- 3 - 'Middle Name'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 0, 0);

-- 4 -'Last Name'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);
 
-- 5 - 'Suffix'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 0);

-- 6 - 'Nick Name'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 0, 0);

-- 7 - 'Honorific'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 0, 0);

-- 8 - 'Prior First Name'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 0, 0);

-- 9 - 'Prior Last Name'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 0, 0);

--10 -  'Address Line 1'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);

--11 - 'Address Line 2'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 0);

--12 - 'Address Line 3'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 0, 0);

--13 - 'City, State, Postal Code'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);

--14 - 'Address Type'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--15 - 'Area Code & Number'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);

--16 - 'Ext.'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 0);

--17 - 'Phone Type'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 0);

--18 - 'Email Address'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 0);

--19 - 'Email Type'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 0);

--20 - 'Birth Date'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);

--21 - 'Gender'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 1);

--22 - 'Marital Status'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--23 - 'Race'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 1);

--24 - 'Ethnicity'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 1);

--25 - 'Language'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 1);

--26 - 'SSN'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--27 - 'Prior ID'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--28-  'Occupation'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--29 - 'Employment Status'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--30 - 'Employer Address Line 1'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--31 - 'Employer Address Line 2'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--32 - 'Employer City, State, Postal Code'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--33 -'Employer Phone'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--34 - 'Preferred'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 1);

--35 - 'PCP'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--36 - 'Other'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--37 - 'HIPAA Consent'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired, DefaultValue) VALUES (1, 1, 0, 0, 'True');

--38 - 'Release Medical Records'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired, DefaultValue) VALUES (1, 1, 0, 0, 'Yes');

--39 - 'Signature Date'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 1);

--40 - 'Assign Benefits to Provider'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired, DefaultValue) VALUES (0, 1, 0, 0, 'Yes');

--41 - 'Patient Tag'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 1);

--42 - 'Status'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);

--43 - 'Religion'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--44 - 'Referring Contact'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--45 - 'Referring Catery'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--46 - 'Comments'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);
