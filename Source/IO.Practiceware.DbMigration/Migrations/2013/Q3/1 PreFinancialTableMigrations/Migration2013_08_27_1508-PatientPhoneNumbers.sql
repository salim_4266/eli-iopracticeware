﻿EXEC('
IF EXISTS(SELECT * FROM sys.views WHERE name = ''PatientPhoneNumbers'')
BEGIN
	DROP VIEW model.PatientPhoneNumbers
END
')

IF OBJECT_ID(N'[model].[PatientPhoneNumbers]', 'U') IS NOT NULL
BEGIN
	DROP TABLE [model].[PatientPhoneNumbers]
END

IF OBJECT_ID(N'[model].[PatientPhoneNumbers]', 'U') IS NULL
BEGIN

	-- Creating table PatientPhoneNumbers
	CREATE TABLE [model].[PatientPhoneNumbers] (
		[Id] int IDENTITY(1,1) NOT NULL,
		[PatientId] int  NOT NULL,
		[PatientPhoneNumberTypeId] int  NOT NULL,		
		[CountryCode] nvarchar(max)  NULL,
		[AreaCode] nvarchar(max)  NULL,
		[ExchangeAndSuffix] nvarchar(max)  NOT NULL,
		[Extension] nvarchar(max)  NULL,
		[IsInternational] bit  NOT NULL,
		[OrdinalId] int  NOT NULL			
	);	
	

	-- Creating primary key on [Id] in table PatientPhoneNumbers
	ALTER TABLE [model].[PatientPhoneNumbers]
	ADD CONSTRAINT [PK_PatientPhoneNumbers]
		PRIMARY KEY CLUSTERED ([Id] ASC);

	-- Creating foreign key on [PatientId] in table PatientPhoneNumbers
	ALTER TABLE [model].[PatientPhoneNumbers]
	ADD CONSTRAINT [FK_PatientPatientPhoneNumber]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

	-- Creating non-clustered index for FOREIGN KEY FK_PatientPatientPhoneNumber
	CREATE INDEX [IX_FK_PatientPatientPhoneNumber]
	ON [model].[PatientPhoneNumbers]
		([PatientId]);

		-- Creating non-clustered index for FOREIGN KEY FK_PatientPhoneNumberTypePatientPhoneNumber
	CREATE INDEX [IX_FK_PatientPhoneNumberTypePatientPhoneNumber]
	ON [model].[PatientPhoneNumbers]
		([PatientPhoneNumberTypeId]);
	

	EXEC('
	
	-- Migrate PatientPhoneNumbers
	INSERT INTO model.PatientPhoneNumbers (
		CountryCode
		,AreaCode
		,ExchangeAndSuffix
		,Extension
		,IsInternational
		,OrdinalId
		,PatientId
		,PatientPhoneNumberTypeId
		)
	----PhoneNumber Patient Home
	SELECT CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE 
			WHEN ISNUMERIC(pd.HomePhone) = 1
				THEN CASE 
						WHEN len(pd.HomePhone) = 10
							THEN SUBSTRING(pd.HomePhone, 1, 3)
						WHEN len(pd.HomePhone) = 11
							THEN SUBSTRING(pd.HomePhone, 2, 3)
						ELSE NULL
						END
			WHEN len(pd.HomePhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.HomePhone, 1, 10)) = 1
				AND SUBSTRING(pd.HomePhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.HomePhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE 
			WHEN ISNUMERIC(pd.HomePhone) = 1
				THEN CASE 
						WHEN len(pd.HomePhone) = 7
							THEN pd.HomePhone
						WHEN len(pd.HomePhone) = 10
							THEN SUBSTRING(pd.HomePhone, 4, 7)
						WHEN len(pd.HomePhone) = 11
							THEN SUBSTRING(pd.HomePhone, 5, 7)
						ELSE pd.HomePhone
						END
			WHEN len(pd.HomePhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.HomePhone, 1, 10)) = 1
				AND SUBSTRING(pd.HomePhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.HomePhone, 1, 10)
			ELSE pd.HomePhone
			END AS ExchangeAndSuffix
		,CASE 
			WHEN len(pd.HomePhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.HomePhone, 1, 10)) = 1
				AND SUBSTRING(pd.HomePhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.HomePhone, 14, 3)
			ELSE NULL
			END AS Extension
		,CONVERT(BIT, 0) AS IsInternational
		,1 AS OrdinalId
		,PatientId AS PatientId
		,7 AS PatientPhoneNumberTypeId
	FROM dbo.PatientDemographicsTable pd
	WHERE pd.HomePhone <> ''''

	UNION ALL

	----PhoneNumber Patient Business Phone
	SELECT CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE 
			WHEN ISNUMERIC(pd.BusinessPhone) = 1
				THEN CASE 
						WHEN len(pd.BusinessPhone) = 10
							THEN SUBSTRING(pd.BusinessPhone, 1, 3)
						WHEN len(pd.BusinessPhone) = 11
							THEN SUBSTRING(pd.BusinessPhone, 2, 3)
						ELSE NULL
						END
			WHEN len(pd.BusinessPhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.BusinessPhone, 1, 10)) = 1
				AND SUBSTRING(pd.BusinessPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.BusinessPhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE 
			WHEN ISNUMERIC(pd.BusinessPhone) = 1
				THEN CASE 
						WHEN len(pd.BusinessPhone) = 7
							THEN pd.BusinessPhone
						WHEN len(pd.BusinessPhone) = 10
							THEN SUBSTRING(pd.BusinessPhone, 4, 7)
						WHEN len(pd.BusinessPhone) = 11
							THEN SUBSTRING(pd.BusinessPhone, 5, 7)
						ELSE pd.BusinessPhone
						END
			WHEN len(pd.BusinessPhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.BusinessPhone, 1, 10)) = 1
				AND SUBSTRING(pd.BusinessPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.BusinessPhone, 1, 10)
			ELSE pd.BusinessPhone
			END AS ExchangeAndSuffix
		,CASE 
			WHEN len(pd.BusinessPhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.BusinessPhone, 1, 10)) = 1
				AND SUBSTRING(pd.BusinessPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.BusinessPhone, 14, 3)
			ELSE NULL
			END AS Extension
		,CONVERT(BIT, 0) AS IsInternational
		,2 AS OrdinalId
		,PatientId AS PatientId
		,2 AS PatientPhoneNumberTypeId
	FROM dbo.PatientDemographicsTable pd
	WHERE pd.BusinessPhone <> ''''

	UNION ALL

	----PhoneNumber Patient Cell
	SELECT CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE 
			WHEN ISNUMERIC(pd.CellPhone) = 1
				THEN CASE 
						WHEN len(pd.CellPhone) = 10
							THEN SUBSTRING(pd.CellPhone, 1, 3)
						WHEN len(pd.CellPhone) = 11
							THEN SUBSTRING(pd.CellPhone, 2, 3)
						ELSE NULL
						END
			WHEN len(pd.CellPhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.CellPhone, 1, 10)) = 1
				AND SUBSTRING(pd.CellPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.CellPhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE 
			WHEN ISNUMERIC(pd.CellPhone) = 1
				THEN CASE 
						WHEN len(pd.CellPhone) = 7
							THEN pd.CellPhone
						WHEN len(pd.CellPhone) = 10
							THEN SUBSTRING(pd.CellPhone, 4, 7)
						WHEN len(pd.CellPhone) = 11
							THEN SUBSTRING(pd.CellPhone, 5, 7)
						ELSE pd.CellPhone
						END
			WHEN len(pd.CellPhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.CellPhone, 1, 10)) = 1
				AND SUBSTRING(pd.CellPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.CellPhone, 1, 10)
			ELSE pd.CellPhone
			END AS ExchangeAndSuffix
		,CASE 
			WHEN len(pd.CellPhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.CellPhone, 1, 10)) = 1
				AND SUBSTRING(pd.CellPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.CellPhone, 14, 3)
			ELSE NULL
			END AS Extension
		,CONVERT(BIT, 0) AS IsInternational
		,3 AS OrdinalId
		,PatientId AS PatientId
		,3 AS PatientPhoneNumberTypeId
	FROM dbo.PatientDemographicsTable pd
	WHERE pd.CellPhone <> ''''

	UNION ALL

	----PhoneNumber Patient Emergency
	SELECT CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE 
			WHEN ISNUMERIC(pd.EmergencyPhone) = 1
				THEN CASE 
						WHEN len(pd.EmergencyPhone) = 10
							THEN SUBSTRING(pd.EmergencyPhone, 1, 3)
						WHEN len(pd.EmergencyPhone) = 11
							THEN SUBSTRING(pd.EmergencyPhone, 2, 3)
						ELSE NULL
						END
			WHEN len(pd.EmergencyPhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.EmergencyPhone, 1, 10)) = 1
				AND SUBSTRING(pd.EmergencyPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.EmergencyPhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE 
			WHEN ISNUMERIC(pd.EmergencyPhone) = 1
				THEN CASE 
						WHEN len(pd.EmergencyPhone) = 7
							THEN pd.EmergencyPhone
						WHEN len(pd.EmergencyPhone) = 10
							THEN SUBSTRING(pd.EmergencyPhone, 4, 7)
						WHEN len(pd.EmergencyPhone) = 11
							THEN SUBSTRING(pd.EmergencyPhone, 5, 7)
						ELSE pd.EmergencyPhone
						END
			WHEN len(pd.EmergencyPhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.EmergencyPhone, 1, 10)) = 1
				AND SUBSTRING(pd.EmergencyPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.EmergencyPhone, 1, 10)
			ELSE pd.EmergencyPhone
			END AS ExchangeAndSuffix
		,CASE 
			WHEN len(pd.EmergencyPhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.EmergencyPhone, 1, 10)) = 1
				AND SUBSTRING(pd.EmergencyPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.EmergencyPhone, 14, 3)
			ELSE NULL
			END AS Extension
		,CONVERT(BIT, 0) AS IsInternational
		,4 AS OrdinalId
		,PatientId AS PatientId
		,12 AS PatientPhoneNumberTypeId
	FROM dbo.PatientDemographicsTable pd
	WHERE pd.EmergencyPhone <> ''''

	UNION ALL

	----PhoneNumber Patient Work
	SELECT CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE 
			WHEN ISNUMERIC(pd.EmployerPhone) = 1
				THEN CASE 
						WHEN len(pd.EmployerPhone) = 10
							THEN SUBSTRING(pd.EmployerPhone, 1, 3)
						WHEN len(pd.EmployerPhone) = 11
							THEN SUBSTRING(pd.EmployerPhone, 2, 3)
						ELSE NULL
						END
			WHEN len(pd.EmployerPhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.EmployerPhone, 1, 10)) = 1
				AND SUBSTRING(pd.EmployerPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.EmployerPhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE 
			WHEN ISNUMERIC(pd.EmployerPhone) = 1
				THEN CASE 
						WHEN len(pd.EmployerPhone) = 7
							THEN pd.EmployerPhone
						WHEN len(pd.EmployerPhone) = 10
							THEN SUBSTRING(pd.EmployerPhone, 4, 7)
						WHEN len(pd.EmployerPhone) = 11
							THEN SUBSTRING(pd.EmployerPhone, 5, 7)
						ELSE pd.EmployerPhone
						END
			WHEN len(pd.EmployerPhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.EmployerPhone, 1, 10)) = 1
				AND SUBSTRING(pd.EmployerPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.EmployerPhone, 1, 10)
			ELSE pd.EmployerPhone
			END AS ExchangeAndSuffix
		,CASE 
			WHEN len(pd.EmployerPhone) = 16
				AND ISNUMERIC(SUBSTRING(pd.EmployerPhone, 1, 10)) = 1
				AND SUBSTRING(pd.EmployerPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pd.EmployerPhone, 14, 3)
			ELSE NULL
			END AS Extension
		,CONVERT(BIT, 0) AS IsInternational
		,5 AS OrdinalId
		,PatientId AS PatientId
		,13 AS PatientPhoneNumberTypeId
	FROM dbo.PatientDemographicsTable pd
	WHERE pd.EmployerPhone <> ''''

	UNION ALL

	----PhoneNumber Patient - PatientDemoAlt 1 and 2 HomePhone  (x = 13 and 14)
	--*/*(no index)*/*
	SELECT CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE 
			WHEN ISNUMERIC(pdAlt.HomePhone) = 1
				THEN CASE 
						WHEN len(pdAlt.HomePhone) = 10
							THEN SUBSTRING(pdAlt.HomePhone, 1, 3)
						WHEN len(pdAlt.HomePhone) = 11
							THEN SUBSTRING(pdAlt.HomePhone, 2, 3)
						ELSE NULL
						END
			WHEN len(pdAlt.HomePhone) = 16
				AND ISNUMERIC(SUBSTRING(pdAlt.HomePhone, 1, 10)) = 1
				AND SUBSTRING(pdAlt.HomePhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pdAlt.HomePhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE 
			WHEN ISNUMERIC(pdAlt.HomePhone) = 1
				THEN CASE 
						WHEN len(pdAlt.HomePhone) = 7
							THEN pdAlt.HomePhone
						WHEN len(pdAlt.HomePhone) = 10
							THEN SUBSTRING(pdAlt.HomePhone, 4, 7)
						WHEN len(pdAlt.HomePhone) = 11
							THEN SUBSTRING(pdAlt.HomePhone, 5, 7)
						ELSE pdAlt.HomePhone
						END
			WHEN len(pdAlt.HomePhone) = 16
				AND ISNUMERIC(SUBSTRING(pdAlt.HomePhone, 1, 10)) = 1
				AND SUBSTRING(pdAlt.HomePhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pdAlt.HomePhone, 1, 10)
			ELSE pdAlt.HomePhone
			END AS ExchangeAndSuffix
		,CASE 
			WHEN len(pdAlt.HomePhone) = 16
				AND ISNUMERIC(SUBSTRING(pdAlt.HomePhone, 1, 10)) = 1
				AND SUBSTRING(pdAlt.HomePhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pdAlt.HomePhone, 14, 3)
			ELSE NULL
			END AS Extension
		,CONVERT(BIT, 0) AS IsInternational
		,7 AS OrdinalId
		,PatientId AS PatientId
		,CASE 
			WHEN Code = ''Home''
				THEN 7
			WHEN Code = ''Business''
				THEN 2
			WHEN Code = ''Cell''
				THEN 3
			WHEN Code = ''Work''
				THEN 13
			WHEN Code = ''Fax''
				THEN 14
			WHEN Code = ''Beeper''
				THEN 15
			WHEN Code = ''Night''
				THEN 16
			ELSE 17		
			END AS PatientPhoneNumberTypeId		
	FROM dbo.PatientDemoAlt pdAlt
	LEFT OUTER JOIN dbo.PracticeCodeTable pct ON (pct.Id = pdAlt.PhoneType1)
		AND pct.ReferenceType = ''PHONETYPE''
	WHERE pdAlt.HomePhone <> ''''

	UNION ALL


	----PhoneNumber Patient - PatientDemoAlt 1 and 2 .WorkPhone (x = 15 and 16)
	--*/*(no index)*/*
	SELECT CONVERT(NVARCHAR, NULL) AS CountryCode
			,CASE 
			WHEN ISNUMERIC(pdAlt.WorkPhone) = 1
				THEN CASE 
						WHEN len(pdAlt.WorkPhone) = 10
							THEN SUBSTRING(pdAlt.WorkPhone, 1, 3)
						WHEN len(pdAlt.WorkPhone) = 11
							THEN SUBSTRING(pdAlt.WorkPhone, 2, 3)
						ELSE NULL
						END
			WHEN len(pdAlt.WorkPhone) = 16
				AND ISNUMERIC(SUBSTRING(pdAlt.WorkPhone, 1, 10)) = 1
				AND SUBSTRING(pdAlt.WorkPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pdAlt.WorkPhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE 
			WHEN ISNUMERIC(pdAlt.WorkPhone) = 1
				THEN CASE 
						WHEN len(pdAlt.WorkPhone) = 7
							THEN pdAlt.WorkPhone
						WHEN len(pdAlt.WorkPhone) = 10
							THEN SUBSTRING(pdAlt.WorkPhone, 4, 7)
						WHEN len(pdAlt.WorkPhone) = 11
							THEN SUBSTRING(pdAlt.WorkPhone, 5, 7)
						ELSE pdAlt.WorkPhone
						END
			WHEN len(pdAlt.WorkPhone) = 16
				AND ISNUMERIC(SUBSTRING(pdAlt.WorkPhone, 1, 10)) = 1
				AND SUBSTRING(pdAlt.WorkPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pdAlt.WorkPhone, 1, 10)
			ELSE pdAlt.WorkPhone
			END AS ExchangeAndSuffix
		,CASE 
			WHEN len(pdAlt.WorkPhone) = 16
				AND ISNUMERIC(SUBSTRING(pdAlt.WorkPhone, 1, 10)) = 1
				AND SUBSTRING(pdAlt.WorkPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pdAlt.WorkPhone, 14, 3)
			ELSE NULL
			END AS Extension
		,CONVERT(BIT, 0) AS IsInternational
		,8 AS OrdinalId
		,PatientId AS PatientId
		,CASE 
			WHEN Code = ''Home''
				THEN 7
			WHEN Code = ''Business''
				THEN 2
			WHEN Code = ''Cell''
				THEN 3
			WHEN Code = ''Work''
				THEN 13
			WHEN Code = ''Fax''
				THEN 14
			WHEN Code = ''Beeper''
				THEN 15
			WHEN Code = ''Night''
				THEN 16
			ELSE 17		
			END AS PatientPhoneNumberTypeId		
	FROM dbo.PatientDemoAlt pdAlt
	LEFT OUTER JOIN dbo.PracticeCodeTable pct ON pct.Id = pdAlt.PhoneType2
		AND pct.ReferenceType = ''PHONETYPE''
	WHERE pdAlt.WorkPhone <> ''''

	UNION ALL

	----PhoneNumber Patient - PatientDemoAlt 1 and 2 .CellPhone (x = 17 and 18)
	SELECT CONVERT(NVARCHAR, NULL) AS CountryCode
		,CASE 
			WHEN ISNUMERIC(pdAlt.CellPhone) = 1
				THEN CASE 
						WHEN len(pdAlt.CellPhone) = 10
							THEN SUBSTRING(pdAlt.CellPhone, 1, 3)
						WHEN len(pdAlt.CellPhone) = 11
							THEN SUBSTRING(pdAlt.CellPhone, 2, 3)
						ELSE NULL
						END
			WHEN len(pdAlt.CellPhone) = 16
				AND ISNUMERIC(SUBSTRING(pdAlt.CellPhone, 1, 10)) = 1
				AND SUBSTRING(pdAlt.CellPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pdAlt.CellPhone, 1, 3)
			ELSE NULL
			END AS AreaCode
		,CASE 
			WHEN ISNUMERIC(pdAlt.CellPhone) = 1
				THEN CASE 
						WHEN len(pdAlt.CellPhone) = 7
							THEN pdAlt.CellPhone
						WHEN len(pdAlt.CellPhone) = 10
							THEN SUBSTRING(pdAlt.CellPhone, 4, 7)
						WHEN len(pdAlt.CellPhone) = 11
							THEN SUBSTRING(pdAlt.CellPhone, 5, 7)
						ELSE pdAlt.CellPhone
						END
			WHEN len(pdAlt.CellPhone) = 16
				AND ISNUMERIC(SUBSTRING(pdAlt.CellPhone, 1, 10)) = 1
				AND SUBSTRING(pdAlt.CellPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pdAlt.CellPhone, 1, 10)
			ELSE pdAlt.CellPhone
			END AS ExchangeAndSuffix
		,CASE 
			WHEN len(pdAlt.CellPhone) = 16
				AND ISNUMERIC(SUBSTRING(pdAlt.CellPhone, 1, 10)) = 1
				AND SUBSTRING(pdAlt.CellPhone, 11, 6) LIKE ''EXT[0-9][0-9][0-9]''
				THEN SUBSTRING(pdAlt.CellPhone, 14, 3)
			ELSE NULL
			END AS Extension
		,CONVERT(BIT, 0) AS IsInternational
		,9 AS OrdinalId
		,PatientId AS PatientId
		,CASE 
			WHEN Code = ''Home''
				THEN 7
			WHEN Code = ''Business''
				THEN 2
			WHEN Code = ''Cell''
				THEN 3
			WHEN Code = ''Work''
				THEN 13
			WHEN Code = ''Fax''
				THEN 14
			WHEN Code = ''Beeper''
				THEN 15
			WHEN Code = ''Night''
				THEN 16
			ELSE 17		
			END AS PatientPhoneNumberTypeId		
	FROM dbo.PatientDemoAlt pdAlt
	LEFT OUTER JOIN dbo.PracticeCodeTable pct ON pct.Id = pdAlt.PhoneType3
		AND pct.ReferenceType = ''PHONETYPE''
	WHERE pdAlt.CellPhone <> ''''
	')
END
GO
