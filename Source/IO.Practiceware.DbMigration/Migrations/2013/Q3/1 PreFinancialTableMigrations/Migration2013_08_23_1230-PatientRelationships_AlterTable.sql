﻿


IF EXISTS(select * from sys.columns where Name = N'FromContactId' and Object_ID = Object_ID(N'model.PatientRelationships')) 
BEGIN
	-- fix column names. 
	EXEC sp_rename '[model].[PatientRelationships].[FromContactId]' , 'FromPatientId', 'COLUMN';
END

GO
IF EXISTS(select * from sys.columns where Name = N'ToContactId' and Object_ID = Object_ID(N'model.PatientRelationships')) 
BEGIN
	EXEC sp_rename '[model].[PatientRelationships].[ToContactId]' , 'ToPatientId', 'COLUMN';
END

GO

IF NOT EXISTS(select * from sys.columns where Name = N'HasSignedHipaa' and Object_ID = Object_ID(N'model.PatientRelationships')) 
BEGIN
	--  Add new Hippa column
	ALTER TABLE [model].[PatientRelationships]
	ADD HasSignedHipaa BIT NOT NULL DEFAULT 0
END

GO


IF EXISTS(select * from sys.columns where Name = N'PatientRelationshipTypeId' and Object_ID = Object_ID(N'model.PatientRelationships')) 
BEGIN

	IF OBJECT_ID(N'[model].[FK_PatientRelationshipTypePatientRelationship]', 'F') IS NOT NULL
	BEGIN
		ALTER TABLE [model].[PatientRelationships] DROP CONSTRAINT [FK_PatientRelationshipTypePatientRelationship]
	END

	IF EXISTS(SELECT * FROM sysindexes WHERE name = 'IX_FK_PatientRelationshipTypePatientRelationship')
    BEGIN
       DROP INDEX model.PatientRelationships.IX_FK_PatientRelationshipTypePatientRelationship
    END

	-- Make TYPE column into string description column
	EXEC sp_rename '[model].[PatientRelationships].[PatientRelationshipTypeId]' , 'PatientRelationshipDescription', 'COLUMN';
	
	ALTER TABLE [model].[PatientRelationships]
	ALTER COLUMN PatientRelationshipDescription NVARCHAR(255) NULL
END
GO

-- drop the patient relationship types table. no longer needed.
IF OBJECT_ID(N'[model].[PatientRelationshipTypes]', 'U') IS NOT NULL
    DROP TABLE [model].[PatientRelationshipTypes];
GO


