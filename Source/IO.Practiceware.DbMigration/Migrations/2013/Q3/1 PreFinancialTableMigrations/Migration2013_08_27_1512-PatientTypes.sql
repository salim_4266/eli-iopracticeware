﻿


IF EXISTS(SELECT * FROM sys.views WHERE name = 'PatientTypes')
BEGIN
	DROP VIEW model.PatientTypes
END

IF OBJECT_ID(N'[model].[PatientTypes]', 'U') IS NOT NULL
BEGIN
	DROP TABLE model.PatientTypes
END

GO

IF OBJECT_ID(N'[model].[PatientTypes]', 'U') IS NULL
BEGIN

	-- Create PatientTypes table
	CREATE TABLE [model].[PatientTypes] (
		[Id] int IDENTITY(1,1) NOT NULL,
		[Name] nvarchar(max)  NOT NULL,
		[IsArchived] bit  NOT NULL,
		[DisplayName] nvarchar(max)  NOT NULL
	);

	-- Creating primary key on [Id] in table 'PatientTypes'
	EXEC('
	ALTER TABLE [model].[PatientTypes]
	ADD CONSTRAINT [PK_PatientTypes]
		PRIMARY KEY CLUSTERED ([Id] ASC);
	')

END

GO


IF OBJECT_ID(N'[model].[PatientTypes]', 'U') IS NOT NULL
BEGIN

	-- Migrate PatientTypes
	SET IDENTITY_INSERT [model].[PatientTypes] ON

		INSERT INTO model.PatientTypes (Id, Name, IsArchived, DisplayName)
			SELECT Id
				,RTRIM(LTRIM(SUBSTRING(Code, CHARINDEX('-', code, 0) + 1, LEN(Code)))) AS [Name]
				,CONVERT(BIT, 0) AS IsArchived
				,RTRIM(LTRIM(SUBSTRING(Code, 0, CHARINDEX('-', code, 0)))) AS [DisplayName]
			FROM dbo.PracticeCodeTable
			WHERE ReferenceType = 'PATIENTTYPE'
			
	SET IDENTITY_INSERT [model].[PatientTypes] OFF
END

GO


IF EXISTS(SELECT * FROM sys.views WHERE name = 'PatientPatientType')
BEGIN
	DROP VIEW model.PatientPatientType
END

GO

IF EXISTS(SELECT * FROM sys.views WHERE name = 'PatientTags')
BEGIN
	DROP VIEW model.PatientTags
END

GO

IF OBJECT_ID(N'[model].[FK_PatientTagPatient]', 'F') IS NOT NULL
    ALTER TABLE [model].[PatientTags] DROP CONSTRAINT [FK_PatientTagPatient];
GO
IF OBJECT_ID(N'[model].[FK_PatientTagPatientType]', 'F') IS NOT NULL
    ALTER TABLE [model].[PatientTags] DROP CONSTRAINT [FK_PatientTagPatientType];
GO

IF OBJECT_ID(N'[model].[PatientTags]', 'U') IS NOT NULL
    DROP TABLE [model].[PatientTags];
GO

IF OBJECT_ID(N'[model].[PatientPatientType]', 'U') IS NOT NULL
    DROP TABLE [model].PatientPatientType;
GO

IF OBJECT_ID(N'[model].[PatientTags]', 'U') IS NULL
BEGIN
		
	-- Creating table 'PatientTags'
	CREATE TABLE [model].[PatientTags] (
		[Id] int IDENTITY(1,1) NOT NULL,
		[PatientId] int  NOT NULL,
		[PatientTypeId] int  NOT NULL,
		[OrdinalId] int  NOT NULL
	);
	
		-- Creating primary key on [Id] in table 'PatientTags'
	ALTER TABLE [model].[PatientTags]
	ADD CONSTRAINT [PK_PatientTags]
		PRIMARY KEY CLUSTERED ([Id] ASC);
	


	-- Creating foreign key on [PatientId] in table 'PatientTags'
	ALTER TABLE [model].[PatientTags]
	ADD CONSTRAINT [FK_PatientTagPatient]
		FOREIGN KEY ([PatientId])
		REFERENCES [model].[Patients]
			([Id])
		ON DELETE NO ACTION ON UPDATE NO ACTION;

	-- Creating non-clustered index for FOREIGN KEY 'FK_PatientTagPatient'
	CREATE INDEX [IX_FK_PatientTagPatient]
	ON [model].[PatientTags]
		([PatientId]);
	

	-- Creating foreign key on [PatientTypeId] in table 'PatientTags'
	ALTER TABLE [model].[PatientTags]
	ADD CONSTRAINT [FK_PatientTagPatientType]
		FOREIGN KEY ([PatientTypeId])
		REFERENCES [model].[PatientTypes]
			([Id])
		ON DELETE NO ACTION ON UPDATE NO ACTION;

	-- Creating non-clustered index for FOREIGN KEY 'FK_PatientTagPatientType'
	CREATE INDEX [IX_FK_PatientTagPatientType]
	ON [model].[PatientTags]
		([PatientTypeId]);
	
	-- Migrate data from old dbo.PatientDemographicsTable to new model.PatientTags
	INSERT INTO model.PatientTags (PatientId, PatientTypeId, OrdinalId)
		SELECT PatientId,
			pt.Id AS PatientTypeId,
			1 AS OrdinalId
		FROM dbo.PatientDemographicsTable pd
		INNER JOIN model.PatientTypes pt ON pt.DisplayName = pd.PatType			
		WHERE pd.PatType <> ''
			AND pd.PatType IS NOT NULL

END
GO
