﻿EXEC('
IF EXISTS(SELECT * FROM sys.views WHERE name = ''PatientAddresses'')
BEGIN
	DROP VIEW model.PatientAddresses
END
')

IF OBJECT_ID(N'[model].[PatientAddresses]', 'U') IS NULL
BEGIN
	-- Create PatientAddresses table
	EXEC('
	CREATE TABLE [model].[PatientAddresses] (
		[Id] bigint IDENTITY(1,1) NOT NULL,
		[OrdinalId] int NOT NULL,
		[City] nvarchar(max)  NOT NULL,
		[Line1] nvarchar(max)  NOT NULL,
		[Line2] nvarchar(max)  NULL,
		[Line3] nvarchar(max)  NULL,
		[PatientId] int  NOT NULL FOREIGN KEY REFERENCES model.Patients(Id),
		[PostalCode] nvarchar(max)  NOT NULL,
		[StateOrProvinceId] int  NOT NULL FOREIGN KEY REFERENCES model.StateOrProvinces(Id),
		[PatientAddressTypeId] int  NOT NULL FOREIGN KEY REFERENCES model.PatientAddressTypes(Id)
	);
	')

	EXEC('
	-- Creating primary key on [Id] in table ''PatientAddresses''
	ALTER TABLE [model].[PatientAddresses]
	ADD CONSTRAINT [PK_PatientAddresses]
		PRIMARY KEY CLUSTERED ([Id] ASC);

	-- Creating non-clustered index for FOREIGN KEY ''FK_PatientAddressStateOrProvince''
	CREATE INDEX [IX_FK_PatientAddressStateOrProvince]
	ON [model].[PatientAddresses]
		([StateOrProvinceId]);

	-- Creating non-clustered index for FOREIGN KEY ''FK_PatientPatientAddress''
	CREATE INDEX [IX_FK_PatientPatientAddress]
	ON [model].[PatientAddresses]
		([PatientId]);
	
	-- Creating non-clustered index for FOREIGN KEY ''FK_PatientAddressTypePatientAddress''
	CREATE INDEX [IX_FK_PatientAddressTypePatientAddress]
	ON [model].[PatientAddresses]
		([PatientAddressTypeId]);
	')

	-- Migrate PatientAddresses
	EXEC('
		INSERT INTO model.PatientAddresses (OrdinalId, City, Line1, Line2, Line3, PatientId, PostalCode, StateOrProvinceId, PatientAddressTypeId)
			SELECT 1 AS OrdinalId,
				COALESCE(pd.City, '''') AS City,
				COALESCE(pd.[Address], '''') AS Line1,
				pd.Suite AS Line2,
				CONVERT(nvarchar, NULL) AS Line3,
				PatientId as PatientId,
				COALESCE(REPLACE(pd.Zip,''-'',''''), '''') AS PostalCode,
				st.ID AS StateOrProvinceId,
				1 AS PatientAddressTypeId
			FROM dbo.PatientDemographicsTable pd
			INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pd.[STATE]
			WHERE pd.[Address] <> ''''
				OR pd.City <> ''''
				OR pd.[State] <> ''''

			UNION ALL

			----Patient''s 2nd address, PatientDemoAlt table
			SELECT 2 AS OrdinalId,
				pdAlt.City AS City,
				pdAlt.[Address] AS Line1,
				pdAlt.Suite AS Line2,
				CONVERT(nvarchar, NULL) AS Line3,
				PatientId as PatientId,
				REPLACE(pdAlt.Zip,''-'','''') AS PostalCode,
				st.ID AS StateOrProvinceId,
				at.Id AS PatientAddressTypeId
			FROM dbo.PatientDemoAlt pdAlt
			INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pdAlt.[STATE]
			LEFT OUTER JOIN model.PatientAddressTypes at ON at.Name = ''Other1''
			WHERE pdAlt.AltId = 1
				AND pdalt.[Address] <> ''''

			UNION ALL

			----Patient''s 3rd address, PatientDemoAlt table
			SELECT 4 AS OrdinalId,
				pdAlt.City AS City,
				pdAlt.[Address] AS Line1,
				pdAlt.Suite AS Line2,
				CONVERT(nvarchar, NULL) AS Line3,
				PatientId AS PatientId,
				REPLACE(pdAlt.Zip,''-'','''') AS PostalCode,
				st.ID AS StateOrProvinceId,
				at.Id AS PatientAddressTypeId
			FROM dbo.PatientDemoAlt pdAlt
			INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pdAlt.[STATE]
			LEFT OUTER JOIN model.PatientAddressTypes at ON at.Name = ''Other2''
			WHERE pdAlt.AltId = 2
				AND pdalt.[Address] <> ''''

			UNION ALL

			----Patient''s work address
			SELECT 3 AS OrdinalId,
				pdWork.BusinessCity AS City,
				pdWork.BusinessAddress AS Line1,
				pdWork.BusinessSuite AS Line2,
				CONVERT(nvarchar, NULL) AS Line3,
				pdWork.PatientId AS PatientId,
				REPLACE(pdWork.BusinessZip,''-'','''') AS PostalCode,
				st.ID AS StateOrProvinceId,
				2 AS PatientAddressTypeId
			FROM dbo.PatientDemographicsTable pdWork
			INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pdWork.BusinessState
			WHERE pdWork.BusinessAddress <> ''''
				and pdWork.BusinessAddress IS NOT NULL
	')
END
GO
