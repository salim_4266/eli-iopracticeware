﻿
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[model].[OnPatientInsurancesUpdate_StraightJacket]', 'TR') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER [model].[OnPatientInsurancesUpdate_StraightJacket]')
END

GO

CREATE TRIGGER [model].[OnPatientInsurancesUpdate_StraightJacket] ON [model].[PatientInsurances] AFTER UPDATE
		AS 

		DECLARE 
		@insurancePolicyId INT,
		@patientId INT,
		@policyHolderPatientId INT,
		@newEndDateTime DATETIME,
		@oldEndDateTime DATETIME

		IF UPDATE(EndDateTime) /* This logic applies only if the end date is being updated */
		BEGIN
			
			SELECT @newEndDateTime = i.EndDateTime, @insurancePolicyId = i.InsurancePolicyId, @patientId = i.InsuredPatientId FROM inserted i		
			SELECT @oldEndDateTime = d.EndDateTime FROM deleted d			
			SELECT @policyHolderPatientId = ip.PolicyHolderPatientId FROM [model].[InsurancePolicies] ip WHERE ip.Id = @insurancePolicyId
		
			IF @patientId = @policyHolderPatientId 				
				UPDATE [model].[PatientInsurances] SET EndDateTime = @newEndDateTime WHERE InsurancePolicyId = @insurancePolicyId and IsDeleted = 0 AND (EndDateTime = @oldEndDateTime OR (EndDateTime IS NULL AND @oldEndDateTime IS NULL))
			ELSE
				UPDATE pin SET EndDateTime = @newEndDateTime FROM [model].[PatientInsurances] pin inner join [model].[InsurancePolicies] ip on pin.InsurancePolicyId = ip.Id
				WHERE pin.InsuredPatientId = @patientId and ip.PolicyHolderPatientId = @policyHolderPatientId and pin.IsDeleted = 0 and ip.StartDateTime <= @newEndDateTime

			/* Examples Given:

			If Self Policy
			1. I'm Joe. I am a policy holder and am ending my policy. I need to end everyone else's coverage under my policy too.
			If Linked Policy
			2. I'm Joe. I am covered under Jane's policy. I end my coverage under Jane's policies. All of Jane's policies I am covered under need to end too.

				*/
					
		END
		SET NOCOUNT OFF
	RETURN 

GO
	