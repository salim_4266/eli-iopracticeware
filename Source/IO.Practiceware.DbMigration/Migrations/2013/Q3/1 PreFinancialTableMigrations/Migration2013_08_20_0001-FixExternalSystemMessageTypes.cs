﻿using System.Data;
using FluentMigrator;
using IO.Practiceware.Model;
using Soaf;
using Soaf.Collections;
using Soaf.Data;
using System;
using System.Data.SqlClient;
using System.Linq;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// Fixes ExternalSystemMessageTypes by truncating the table and re-inserting from the enum.
    /// </summary>
    [Migration(201308200001)]
    public class Migration201308200001 : NonFluentTransactionalUpOnlyMigration
    {
        protected override void MigrateUp(IDbConnection connection, IDbTransaction transaction)
        {
            Func<ExternalSystemMessageTypeId, string> getName = e => e.ToString().Substring(0, Math.Max(e.ToString().LastIndexOf("_Inbound", StringComparison.Ordinal), e.ToString().LastIndexOf("_Outbound", StringComparison.Ordinal)) < 0 ? e.ToString().Length : Math.Max(e.ToString().LastIndexOf("_Inbound", StringComparison.Ordinal), e.ToString().LastIndexOf("_Outbound", StringComparison.Ordinal)));

            Func<ExternalSystemMessageTypeId, int> getIsOutbound = e => e.ToString().EndsWith("_Inbound") ? 0 : 1;

            var insertSql = Enums.GetValues<ExternalSystemMessageTypeId>()
                .Select(e => "INSERT INTO [model].[ExternalSystemMessageTypes](Id, Name, Description, IsOutbound) VALUES ({0}, '{1}', '{2}', {3})".FormatWith((int) e, getName(e), e.GetDescription(), getIsOutbound(e)))
                .Join(Environment.NewLine);

            var deleteSql = "DELETE m FROM [model].[ExternalSystemExternalSystemMessageTypes] m INNER JOIN [model].[ExternalSystemMessageTypes] mt ON m.ExternalSystemMessageTypeId = mt.Id WHERE NOT ({0})".FormatWith(
                Enums.GetValues<ExternalSystemMessageTypeId>()
                    .Select(e => "(mt.Name = '{0}' AND mt.IsOutbound = {1})".FormatWith(getName(e), getIsOutbound(e)))
                    .Join(" OR "));

            var updateSql = @"UPDATE m
SET m.ExternalSystemMessageTypeId = 
	CASE 
        {0}
	END
FROM [model].[ExternalSystemExternalSystemMessageTypes] m
JOIN [model].[ExternalSystemMessageTypes] mt ON m.ExternalSystemMessageTypeId = mt.Id
".FormatWith(
                Enums.GetValues<ExternalSystemMessageTypeId>().Select(e => "WHEN mt.Name = '{0}' AND mt.IsOutbound = {1} THEN {2}".FormatWith(getName(e), getIsOutbound(e), (int) e)).Join(Environment.NewLine)
                );
            var sql =
                @"               
UPDATE [model].[ExternalSystemMessageTypes] SET Name = 'PaperClaim' WHERE Name = 'PaperClaim_Outbound'
                                
ALTER TABLE [model].[ExternalSystemExternalSystemMessageTypes] DROP CONSTRAINT [FK_ExternalSystemExternalSystemMessageType_ExternalSystemMessageType]
GO

-- Update bad data in ExternalSystemExternalSystemMessageType
{0}

{1}

-- Truncate the table
TRUNCATE TABLE [model].[ExternalSystemMessageTypes]

-- Re-insert all data
SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] ON

{2}

SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] OFF

ALTER TABLE [model].[ExternalSystemExternalSystemMessageTypes]  WITH CHECK ADD  CONSTRAINT [FK_ExternalSystemExternalSystemMessageType_ExternalSystemMessageType] FOREIGN KEY([ExternalSystemMessageTypeId])
REFERENCES [model].[ExternalSystemMessageTypes] ([Id])
GO

ALTER TABLE [model].[ExternalSystemExternalSystemMessageTypes] CHECK CONSTRAINT [FK_ExternalSystemExternalSystemMessageType_ExternalSystemMessageType]
GO
".FormatWith(deleteSql, updateSql, insertSql);

            connection.RunScript(sql, false, false, () => new SqlCommand {Connection = (SqlConnection) connection, Transaction = (SqlTransaction) transaction});
        }
    }
}
