﻿--This migration creates triggers on Patients, Appointments and PracticeActivity
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[model].[PatientsExternalSystemMessagesTrigger]') AND OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [model].PatientsExternalSystemMessagesTrigger
GO

CREATE TRIGGER model.PatientsExternalSystemMessagesTrigger
	ON  model.Patients
	FOR INSERT, UPDATE NOT FOR REPLICATION
AS 
BEGIN
SET NOCOUNT ON;

	DECLARE @now as DATETIME
	SET @now = GETUTCDATE()

	DECLARE @insertTable TABLE(Id UNIQUEIDENTIFIER, MessageType INT, PatientId INT, [Description] NVARCHAR(255))
	DECLARE @createdBy nvarchar(max)

		INSERT INTO @insertTable 
		SELECT NEWID(), esesmt.Id, i.Id AS PatientId, ESMT.Name
		FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i
		CROSS JOIN model.ExternalSystemExternalSystemMessageTypes esesmt
		INNER JOIN model.ExternalSystemMessageTypes esmt ON esesmt.ExternalSystemMessageTypeId = esmt.Id AND esmt.Name IN ('ADT_A28_V23') AND esmt.IsOutbound = 1
		WHERE esesmt.IsDisabled = 0

	-- Create rows for each ExternalSystemExternalSystemMessageType subscribed for ADT_A28_V23
	-- If deleted table contains rows, then this was an UPDATE
	IF EXISTS(SELECT * FROM deleted)
	BEGIN
		SET @createdBy = 'PatientUpdate'
	END
	ELSE
	BEGIN
		SET @createdBy = 'PatientInsert'
	END

	-- Create the messages
	INSERT INTO model.ExternalSystemMessages(Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId, ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId)
	-- Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId (1 = Unprocessed, 2 = Processing, 3 = Processed),
	-- ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId
	SELECT Id, [Description], '', MessageType, 1, 0, @createdBy, @now, @now, ''
	FROM @insertTable

	-- Add entity associated with each message for PatientId
	INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
	-- Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey
	SELECT NEWID(), it.Id, pre.Id, PatientId
	FROM
	@insertTable it INNER JOIN model.PracticeRepositoryEntities pre ON pre.Name = 'Patient'
END
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PracticeActivityCheckoutExternalSystemMessagesTrigger]') AND OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[PracticeActivityCheckoutExternalSystemMessagesTrigger]
GO

CREATE TRIGGER dbo.PracticeActivityCheckoutExternalSystemMessagesTrigger 
   ON  dbo.PracticeActivity
   FOR UPDATE NOT FOR REPLICATION
AS 
BEGIN
SET NOCOUNT ON;

IF UPDATE(Status)
	BEGIN
		DECLARE @now as DATETIME
		SET @now = GETUTCDATE()

		DECLARE @insertTable TABLE(Id UNIQUEIDENTIFIER, MessageType INT, AppointmentId INT, Description NVARCHAR(255))

		-- Create rows for each ExternalSystemExternalSystemMessageType subscribed for DFT_P03_V231s, ORM_O01_V231
		INSERT INTO @insertTable 
		SELECT NEWID(), esesmt.Id, i.AppointmentId, ESMT.Name
		FROM 
		DELETED d INNER JOIN inserted i on d.ActivityId = i.ActivityId
		CROSS JOIN model.ExternalSystemExternalSystemMessageTypes esesmt WITH(NOLOCK) 
		INNER JOIN model.ExternalSystemMessageTypes esmt WITH(NOLOCK) ON esesmt.ExternalSystemMessageTypeId = esmt.Id AND esmt.Name IN ('DFT_P03_V231', 'ORM_O01_V231') AND esmt.IsOutbound = 1
		WHERE 
		i.Status = 'D' AND (d.Status = 'G' OR d.Status = 'H' OR d.Status = 'U') AND esesmt.IsDisabled = 0


		-- Create the messags
		INSERT INTO model.ExternalSystemMessages(Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId, ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId)
		SELECT Id, Description, '', MessageType, 1, 0, 'Checkout', @now, @now, ''
		FROM @insertTable

		-- Add entity associated with each message for AppointmentId
		INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
		SELECT NEWID(), it.Id, pre.Id, AppointmentId
		FROM
		@insertTable it INNER JOIN model.PracticeRepositoryEntities pre ON pre.Name = 'Appointment'
	END
END
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AppointmentExternalSystemMessagesTrigger]') AND OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].AppointmentExternalSystemMessagesTrigger
GO

CREATE TRIGGER dbo.AppointmentExternalSystemMessagesTrigger
   ON  dbo.Appointments
   FOR INSERT, UPDATE NOT FOR REPLICATION
AS 
BEGIN
SET NOCOUNT ON;

	DECLARE @now as DATETIME
	SET @now = GETUTCDATE()

	DECLARE @insertTable TABLE(Id UNIQUEIDENTIFIER, MessageType INT, AppointmentId INT, [Description] NVARCHAR(255))
	DECLARE @createdBy nvarchar(max)

	INSERT INTO @insertTable 
	SELECT NEWID(), esesmt.Id, i.AppointmentId, ESMT.Name
	FROM (SELECT [AppointmentId],[PatientId],[AppTypeId],[AppDate],[AppTime],[Duration],[ReferralId],[PreCertId],[TechApptTypeId],[ScheduleStatus],[ActivityStatus],[Comments],[ResourceId1],[ResourceId2],[ResourceId3],[ResourceId4],[ResourceId5],[ResourceId6],[ResourceId7],[ResourceId8],[ApptInsType],[ConfirmStatus],[ApptTypeCat],[SetDate],[VisitReason] FROM inserted EXCEPT SELECT [AppointmentId],[PatientId],[AppTypeId],[AppDate],[AppTime],[Duration],[ReferralId],[PreCertId],[TechApptTypeId],[ScheduleStatus],[ActivityStatus],[Comments],[ResourceId1],[ResourceId2],[ResourceId3],[ResourceId4],[ResourceId5],[ResourceId6],[ResourceId7],[ResourceId8],[ApptInsType],[ConfirmStatus],[ApptTypeCat],[SetDate],[VisitReason] FROM deleted) i
	CROSS JOIN model.ExternalSystemExternalSystemMessageTypes esesmt WITH(NOLOCK) 
	INNER JOIN model.ExternalSystemMessageTypes esmt WITH(NOLOCK) ON esesmt.ExternalSystemMessageTypeId = esmt.Id AND esmt.Name IN ('ADT_A28_V23') AND esmt.IsOutbound = 1
	WHERE esesmt.IsDisabled = 0

	-- Create rows for each ExternalSystemExternalSystemMessageType subscribed for ADT_A28_V23
	-- If deleted table contains rows, then this was an UPDATE
	IF EXISTS(SELECT * FROM deleted)
	BEGIN
		SET @createdBy = 'AppointmentUpdate'
	END
	ELSE
	BEGIN
		SET @createdBy = 'AppointmentInsert'
	END

	-- Create the messages
	INSERT INTO model.ExternalSystemMessages(Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId, ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId)
	-- Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId (1 = Unprocessed, 2 = Processing, 3 = Processed),
	-- ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId
	SELECT Id, [Description], '', MessageType, 1, 0, @createdBy, @now, @now, ''
	FROM @insertTable

	-- Add entity associated with each message for AppointmentId
	INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
	-- Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey
	SELECT NEWID(), it.Id, pre.Id, AppointmentId
	FROM
	@insertTable it INNER JOIN model.PracticeRepositoryEntities pre ON pre.Name = 'Appointment'
END
GO