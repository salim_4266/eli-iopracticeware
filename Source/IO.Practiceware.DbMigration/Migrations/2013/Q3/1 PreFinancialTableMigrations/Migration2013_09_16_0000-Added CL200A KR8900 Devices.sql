﻿-- Add Devices names to the Devices Table for IOConnected Devices.

-- KR8900
IF NOT EXISTS(Select TOP 1 * from dbo.Devices WHERE DeviceAlias='TopconKR8900')
BEGIN
	INSERT INTO dbo.Devices (Make, Model, DeviceAlias, Type, Baud, Parity, DataBits, Stopbits) VALUES('TopCon','KR-8900','TopconKR8900','Autorefractor',9600,'even',8,1)
END 

GO

--CL200A
IF NOT EXISTS(Select TOP 1 * from dbo.Devices WHERE DeviceAlias='TopconCL200A')
BEGIN
	INSERT INTO dbo.Devices (Make, Model, DeviceAlias, Type, Baud, Parity, DataBits, Stopbits) VALUES('TopCon','CL-200A','TopconCL200A','Lensometer',2400,'even',8,1)
END 

GO
