﻿EXEC('
IF EXISTS(SELECT * FROM sys.views WHERE name = ''PatientEthnicities'')
BEGIN
	DROP VIEW model.PatientEthnicities
END
')


IF OBJECT_ID(N'[model].[PatientEthnicities]', 'U') IS NULL
BEGIN
	-- Create PatientEthnicities table
	EXEC('
	CREATE TABLE [model].[PatientEthnicities](
		[Id] int IDENTITY(1,1) NOT NULL,
		[Name] nvarchar(64) NOT NULL,
		[IsArchived] bit NOT NULL DEFAULT 0
	);
	')

	-- Creating primary key on [Id] in table 'PatientEthnicities'
	EXEC('
	ALTER TABLE [model].[PatientEthnicities]
	ADD CONSTRAINT [PK_PatientEthnicities]
		PRIMARY KEY CLUSTERED ([Id] ASC);
	')

	-- Migrate PatientEthnicities from PracticeCodeTable
	EXEC('
	SET IDENTITY_INSERT [model].[PatientEthnicities] ON
	
		INSERT INTO model.PatientEthnicities(Id, Name, IsArchived)
			SELECT Id,
				Code AS [Name],
				CONVERT(bit, 0) AS IsArchived
			FROM dbo.PracticeCodeTable
			WHERE ReferenceType = ''ETHNICITY''

	SET IDENTITY_INSERT [model].[PatientEthnicities] OFF
	')
END
GO
