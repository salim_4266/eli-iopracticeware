﻿-- Making the table model.ExternalSystemMessageTypes as Read only

IF OBJECT_ID('model.ExternalSystemMessageTypesReadOnlyTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER model.ExternalSystemMessageTypesReadOnlyTrigger')
END

GO

CREATE TRIGGER model.ExternalSystemMessageTypesReadOnlyTrigger ON model.ExternalSystemMessageTypes
FOR INSERT,
UPDATE,
DELETE
AS
SET NOCOUNT ON
IF (SELECT COUNT(*) 
	FROM (SELECT Id, Name, Description, IsOutbound FROM inserted EXCEPT SELECT Id, Name, Description, IsOutbound FROM deleted) ESMT) <> 0 
	AND dbo.GetNoCheck() = 0
BEGIN
	RAISERROR( 'ExternalSystemMessageTypes table is read only.', 16, 1 )
	ROLLBACK TRANSACTION
END

