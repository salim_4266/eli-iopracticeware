﻿SET IDENTITY_INSERT [model].[Permissions] ON

	INSERT INTO [model].[Permissions] (Id, Name) VALUES (32, 'ViewPatientAppointmentSummary')

SET IDENTITY_INSERT [model].[Permissions] OFF
GO

INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 107, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 32 -- 'ViewPatientAppointmentSummary'

GO
