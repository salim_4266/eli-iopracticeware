﻿IF EXISTS( SELECT * FROM sys.columns WHERE Name = N'IsArchived' AND Object_ID = Object_ID(N'model.InsurerClaimForms'))
BEGIN
	EXEC('DELETE FROM [model].[InsurerClaimForms] WHERE IsArchived = 1')
	ALTER TABLE [model].[InsurerClaimForms] DROP COLUMN IsArchived
END
GO