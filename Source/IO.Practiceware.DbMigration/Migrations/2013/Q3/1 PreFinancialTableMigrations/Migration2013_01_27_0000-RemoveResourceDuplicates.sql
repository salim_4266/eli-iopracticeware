﻿--Remove ResourceName Duplicates
DECLARE @TotalCount INT
DECLARE @Counter INT
DECLARE @CurrentDuplicate NVARCHAR(MAX)
DECLARE @CurrentResourceId INT

SELECT ResourceName
INTO #TMPResourceName
FROM dbo.Resources
GROUP BY ResourceName
HAVING COUNT(*) > 1

SELECT @TotalCount = COUNT(#TMPResourceName.ResourceName)
FROM #TMPResourceName

WHILE (@TotalCount > 0)
BEGIN
	SELECT TOP 1 @CurrentDuplicate = #TMPResourceName.ResourceName
	FROM #TMPResourceName

	SELECT ResourceId
	INTO #TMPResId
	FROM dbo.Resources
	WHERE ResourceName IN (@CurrentDuplicate)
	ORDER BY ResourceType DESC

	SELECT @Counter = COUNT(*) - 1
	FROM #TMPResId

	WHILE (@Counter > 0)
	BEGIN
		SELECT TOP 1 @CurrentResourceId = #TMPResId.ResourceId
		FROM #TMPResId

		UPDATE dbo.Resources
		SET ResourceName = ResourceName + CAST(@counter AS VARCHAR(3))
		WHERE ResourceId = @CurrentResourceId
		AND LEN(ResourceName + CAST(@counter AS VARCHAR(3))) < 15

		DELETE
		FROM #TMPResId
		WHERE #TMPResId.ResourceId = @CurrentResourceId

		SELECT @Counter = @Counter - 1
	END

	DELETE
	FROM #TMPResourceName
	WHERE #TMPResourceName.ResourceName = @CurrentDuplicate

	SELECT @TotalCount = @TotalCount - 1

	DROP TABLE #TMPResId
END

DROP TABLE #TMPResourceName
GO

--Remove ResourcePid Duplicates
DECLARE @TotalCount INT
DECLARE @CurrentDuplicate NVARCHAR(MAX)
DECLARE @ResourcePid AS NVARCHAR(4)
DECLARE @CurrentResourceId AS INT
DECLARE @Counter AS INT

SELECT ResourcePid
INTO #TMPResourcePid
FROM dbo.Resources
WHERE ISNUMERIC(ResourcePId) = 1
GROUP BY ResourcePid
HAVING COUNT(*) > 1

SELECT @TotalCount = COUNT(#TMPResourcePid.ResourcePid)
FROM #TMPResourcePid

WHILE (@TotalCount > 0)
BEGIN
	SELECT TOP 1 @CurrentDuplicate = #TMPResourcePid.ResourcePid
	FROM #TMPResourcePid

	--Exculding lowest ResourceId.Please see Bug #15889.
	SELECT V.ResourceId As ResourceId INTO #TMPResId FROM(
	SELECT ResourceId
	FROM dbo.Resources
	WHERE ResourcePid IN (@CurrentDuplicate)
	EXCEPT
	SELECT MIN(ResourceId) 
	FROM dbo.Resources
	WHERE ResourcePId IN (@CurrentDuplicate)) AS V

	SELECT @Counter = COUNT(#TMPResId.ResourceId)
	FROM #TMPResId

	WHILE (@Counter > 0)
	BEGIN
		SELECT TOP 1 @CurrentResourceId = #TMPResId.ResourceId
		FROM #TMPResId

		SELECT @ResourcePid = (SUBSTRING(CONVERT(NVARCHAR(MAX), ABS(CHECKSUM(NewId()))), 2, 4))

		WHILE (
				(
					SELECT COUNT(ResourcePid)
					FROM dbo.Resources
					WHERE ResourcePid = @ResourcePid
					) > 0
				)
		BEGIN
			SELECT @ResourcePid = (SUBSTRING(CONVERT(NVARCHAR(MAX), ABS(CHECKSUM(NewId()))), 2, 4))
		END

		UPDATE dbo.Resources
		SET ResourcePid = @ResourcePid
		WHERE ResourceId IN (
				SELECT #TMPResId.ResourceId
				FROM #TMPResId
				WHERE ResourceId = @CurrentResourceId
				)

		DELETE #TMPResId
		WHERE #TMPResId.ResourceId = @CurrentResourceId

		SELECT @Counter = @Counter - 1
	END

	DELETE
	FROM #TMPResourcePid
	WHERE #TMPResourcePid.ResourcePid = @CurrentDuplicate

	SELECT @TotalCount = @TotalCount - 1

	DROP TABLE #TMPResId
END

DROP TABLE #TMPResourcePid
GO

--Handling Duplicates Null ResourcePid
DECLARE @CurrentDuplicate NVARCHAR(MAX)
DECLARE @ResourcePid AS NVARCHAR(4)
DECLARE @CurrentResourceId AS INT
DECLARE @Counter AS INT

SELECT ResourceId
INTO #TMPResId
FROM dbo.Resources
WHERE ResourcePid IS NULL
	AND ResourceType <> 'R'
ORDER BY ResourceType DESC

SELECT @Counter = COUNT(#TMPResId.ResourceId)
FROM #TMPResId

WHILE (@Counter > 0)
BEGIN
	SELECT TOP 1 @CurrentResourceId = #TMPResId.ResourceId
	FROM #TMPResId

	SELECT @ResourcePid = (SUBSTRING(CONVERT(NVARCHAR(MAX), ABS(CHECKSUM(NewId()))), 2, 4))

	WHILE (
			(
				SELECT COUNT(ResourcePid)
				FROM dbo.Resources
				WHERE ResourcePid = @ResourcePid
				) > 0
			)
	BEGIN
		SELECT @ResourcePid = (SUBSTRING(CONVERT(NVARCHAR(MAX), ABS(CHECKSUM(NewId()))), 2, 4))
	END

	UPDATE dbo.Resources
	SET ResourcePid = @ResourcePid
	WHERE ResourceId IN (
			SELECT #TMPResId.ResourceId
			FROM #TMPResId
			WHERE ResourceId = @CurrentResourceId
			)

	DELETE #TMPResId
	WHERE #TMPResId.ResourceId = @CurrentResourceId

	SELECT @Counter = @Counter - 1
END

DROP TABLE #TMPResId