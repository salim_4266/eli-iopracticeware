﻿-- Migration to turn InsurerAddresses, InsurerPhoneNumbers, and InsurerPhoneNumberTypes into tables
-- InsurerAddress

-- create the views if they do not exist
--InsurerAddresses
IF OBJECT_ID('model.InsurerAddresses', 'V') IS NULL
BEGIN
	EXEC dbo.sp_executesql @statement = N'
CREATE VIEW model.InsurerAddresses AS
SELECT model.GetBigPairId(15, InsurerId, 110000000) AS Id,
	pri.InsurerCity AS City,
	pri.InsurerId AS InsurerId,
	pri.Insureraddress AS Line1,
	CONVERT(NVARCHAR, NULL) AS Line2,
	CONVERT(NVARCHAR, NULL) AS Line3,
	REPLACE(pri.InsurerZip,''-'','''') AS PostalCode,
	st.Id AS StateOrProvinceId,
	3 AS InsurerAddressTypeId
FROM dbo.PracticeInsurers pri
INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pri.InsurerState
WHERE pri.InsurerAddress <> ''''
	AND pri.InsurerAddress IS NOT NULL'
END
GO

-- InsurerPhoneNumber
IF OBJECT_ID('model.InsurerPhoneNumbers', 'V') IS NULL
BEGIN
	EXEC dbo.sp_executesql @statement = 
		N'
CREATE VIEW model.InsurerPhoneNumbers AS
SELECT model.GetPairId(25, InsurerId, 55000000) AS Id
	,CONVERT(NVARCHAR, NULL) AS CountryCode
	,CONVERT(NVARCHAR, NULL) AS AreaCode
	,InsurerPhone AS ExchangeAndSuffix
	,CONVERT(NVARCHAR, NULL) AS Extension
	,InsurerId AS InsurerId
	,CONVERT(bit, 0) AS IsInternational
	,1 AS OrdinalId
	,8 AS InsurerPhoneNumberTypeId
FROM dbo.PracticeInsurers
WHERE InsurerPhone <> ''''
	AND InsurerPhone IS NOT NULL

UNION ALL

SELECT model.GetPairId(26, InsurerId, 55000000) AS Id
	,CONVERT(NVARCHAR, NULL) AS CountryCode
	,CONVERT(NVARCHAR, NULL) AS AreaCode
	,InsurerPrecPhone AS ExchangeAndSuffix
	,CONVERT(NVARCHAR, NULL) AS Extension
	,InsurerId AS InsurerId
	,CONVERT(bit, 0) AS IsInternational
	,2 AS OrdinalId
	,pct.Id + 1000 AS InsurerPhoneNumberTypeId
FROM dbo.PracticeInsurers
LEFT OUTER JOIN dbo.PracticeCodeTable pct 
	ON pct.Code = ''Authorization''
	AND pct.ReferenceType = ''PHONETYPEINSURER''
WHERE InsurerPrecPhone <> ''''
	AND InsurerPrecPhone IS NOT NULL

UNION ALL

SELECT model.GetPairId(27, InsurerId, 55000000) AS Id
	,CONVERT(NVARCHAR, NULL) AS CountryCode
	,CONVERT(NVARCHAR, NULL) AS AreaCode
	,InsurerEligPhone AS ExchangeAndSuffix
	,CONVERT(NVARCHAR, NULL) AS Extension
	,InsurerId AS InsurerId
	,CONVERT(bit, 0) AS IsInternational
	,3 AS OrdinalId
	,pct.Id + 1000 AS InsurerPhoneNumberTypeId
FROM dbo.PracticeInsurers
LEFT OUTER JOIN dbo.PracticeCodeTable pct 
	ON pct.Code = ''Eligibility''
	AND pct.ReferenceType = ''PHONETYPEINSURER''
WHERE InsurerEligPhone <> ''''
	AND InsurerEligPhone IS NOT NULL

UNION ALL

SELECT model.GetPairId(28, InsurerId, 55000000) AS Id
	,CONVERT(NVARCHAR, NULL) AS CountryCode
	,CONVERT(NVARCHAR, NULL) AS AreaCode
	,InsurerProvPhone AS ExchangeAndSuffix
	,CONVERT(NVARCHAR, NULL) AS Extension
	,InsurerId AS InsurerId
	,CONVERT(bit, 0) AS IsInternational
	,4 AS OrdinalId
	,pct.Id + 1000 AS InsurerPhoneNumberTypeId
FROM dbo.PracticeInsurers
LEFT OUTER JOIN dbo.PracticeCodeTable pct 
	ON pct.Code = ''Provider''
	AND pct.ReferenceType = ''PHONETYPEINSURER''
WHERE InsurerProvPhone <> ''''
	AND InsurerProvPhone IS NOT NULL

UNION ALL

SELECT model.GetPairId(29, InsurerId, 55000000) AS Id
	,CONVERT(NVARCHAR, NULL) AS CountryCode
	,CONVERT(NVARCHAR, NULL) AS AreaCode
	,InsurerClaimPhone AS ExchangeAndSuffix
	,CONVERT(NVARCHAR, NULL) AS Extension
	,InsurerId AS InsurerId
	,CONVERT(bit, 0) AS IsInternational
	,5 AS OrdinalId
	,pct.Id + 1000 AS InsurerPhoneNumberTypeId
FROM dbo.PracticeInsurers
LEFT OUTER JOIN dbo.PracticeCodeTable pct 
	ON pct.Code = ''Claims''
	AND pct.ReferenceType = ''PHONETYPEINSURER''
WHERE InsurerClaimPhone <> ''''
	AND InsurerClaimPhone IS NOT NULL

UNION ALL

SELECT model.GetPairId(30, InsurerId, 55000000) AS Id
	,CONVERT(NVARCHAR, NULL) AS CountryCode
	,CONVERT(NVARCHAR, NULL) AS AreaCode
	,InsurerENumber AS ExchangeAndSuffix
	,CONVERT(NVARCHAR, NULL) AS Extension
	,InsurerId AS InsurerId
	,CONVERT(bit, 0) AS IsInternational
	,6 AS OrdinalId
	,pct.Id + 1000 AS InsurerPhoneNumberTypeId
FROM dbo.PracticeInsurers
LEFT OUTER JOIN dbo.PracticeCodeTable pct 
	ON pct.Code = ''Submissions''
	AND pct.ReferenceType = ''PHONETYPEINSURER''
WHERE InsurerENumber <> ''''
	AND InsurerENumber IS NOT NULL'
END
GO

-- InsurerPhoneNumberType
IF OBJECT_ID('model.InsurerPhoneNumberTypes', 'V') IS NULL
BEGIN
	EXEC dbo.sp_executesql @statement = N'
CREATE VIEW model.InsurerPhoneNumberTypes AS
SELECT CASE 
	WHEN Code = ''Primary''
		THEN 8
	ELSE pctInsurer.Id + 1000 
	END AS Id,
	AlternateCode AS Abbreviation,
	CASE 
		WHEN Code = ''Primary''
			THEN ''Main''
		ELSE Code
	END AS Name,
	CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctInsurer
WHERE pctInsurer.ReferenceType = ''PHONETYPEINSURER'''
END
GO

-- create backup tables and insert data
-- Creating table InsurerAddressesBackup
IF OBJECT_ID('dbo.InsurerAddressesBackup', 'U') IS NULL
BEGIN
	CREATE TABLE [dbo].[InsurerAddressesBackup] (
		[InsurerId] INT NOT NULL
		,[InsurerAddressTypeId] INT NOT NULL
		,[Id] BIGINT IDENTITY(1, 1) NOT NULL
		,[Line1] NVARCHAR(max) NOT NULL
		,[Line2] NVARCHAR(max) NULL
		,[City] NVARCHAR(max) NOT NULL
		,[PostalCode] NVARCHAR(max) NOT NULL
		,[Line3] NVARCHAR(max) NULL
		,[StateOrProvinceId] INT NOT NULL
		);
END

-- insert data into backup tables
INSERT INTO dbo.InsurerAddressesBackup (
	[InsurerId]
	,[InsurerAddressTypeId]
	,[Line1]
	,[Line2]
	,[City]
	,[PostalCode]
	,[Line3]
	,[StateOrProvinceId]
	)
SELECT [InsurerId]
	,[InsurerAddressTypeId]
	,[Line1]
	,[Line2]
	,[City]
	,[PostalCode]
	,[Line3]
	,[StateOrProvinceId]
FROM model.InsurerAddresses
GO

-- Creating table InsurerPhoneNumbersBackup
IF OBJECT_ID('dbo.InsurerPhoneNumbersBackup', 'U') IS NULL
BEGIN
	CREATE TABLE [dbo].[InsurerPhoneNumbersBackup] (
		[InsurerId] INT NOT NULL
		,[InsurerPhoneNumberTypeId] INT NOT NULL
		,[Id] INT IDENTITY(1, 1) NOT NULL
		,[ExchangeAndSuffix] NVARCHAR(max) NOT NULL
		,[Extension] NVARCHAR(max) NULL
		,[IsInternational] BIT NOT NULL
		,[OrdinalId] INT NOT NULL
		,[AreaCode] NVARCHAR(max) NULL
		,[CountryCode] NVARCHAR(max) NULL
		,
		);
END

INSERT INTO dbo.InsurerPhoneNumbersBackup (
	[InsurerId]
	,[InsurerPhoneNumberTypeId]
	,[ExchangeAndSuffix]
	,[Extension]
	,[IsInternational]
	,[OrdinalId]
	,[AreaCode]
	,[CountryCode]
	)
SELECT [InsurerId]
	,[InsurerPhoneNumberTypeId]
	,[ExchangeAndSuffix]
	,[Extension]
	,[IsInternational]
	,[OrdinalId]
	,[AreaCode]
	,[CountryCode]
FROM model.InsurerPhoneNumbers
GO

-- Creating table InsurerPhoneNumberTypesBackup
IF OBJECT_ID('dbo.InsurerPhoneNumberTypesBackup', 'U') IS NULL
BEGIN
	CREATE TABLE [dbo].[InsurerPhoneNumberTypesBackup] (
		[Id] INT IDENTITY(1, 1) NOT NULL
		,[Name] NVARCHAR(max) NOT NULL
		,[Abbreviation] NVARCHAR(max) NOT NULL
		,[IsArchived] BIT NOT NULL
		);
END

SET IDENTITY_INSERT dbo.InsurerPhoneNumberTypesBackup ON
INSERT INTO dbo.InsurerPhoneNumberTypesBackup ([Id]
	,[Name]
	,[Abbreviation]
	,[IsArchived]
	)
SELECT [Id]
	,[Name]
	,[Abbreviation]
	,[IsArchived]
FROM model.InsurerPhoneNumberTypes
SET IDENTITY_INSERT dbo.InsurerPhoneNumberTypesBackup OFF
GO

-- drop model views
IF OBJECT_ID('model.InsurerAddresses', 'V') IS NOT NULL
	DROP VIEW model.InsurerAddresses
GO

IF OBJECT_ID('model.InsurerPhoneNumbers', 'V') IS NOT NULL
	DROP VIEW model.InsurerPhoneNumbers
GO

IF OBJECT_ID('model.InsurerPhoneNumberTypes', 'V') IS NOT NULL
	DROP VIEW model.InsurerPhoneNumberTypes
GO

-- create model tables
--InsurerPhoneNumbers
IF OBJECT_ID('model.InsurerPhoneNumbers', 'U') IS NULL
BEGIN
	CREATE TABLE [model].[InsurerPhoneNumbers] (
		[InsurerId] INT NOT NULL
		,[InsurerPhoneNumberTypeId] INT NOT NULL
		,[Id] INT IDENTITY(1, 1) NOT NULL
		,[ExchangeAndSuffix] NVARCHAR(max) NOT NULL
		,[Extension] NVARCHAR(max) NULL
		,[IsInternational] BIT NOT NULL
		,[OrdinalId] INT NOT NULL
		,[AreaCode] NVARCHAR(max) NULL
		,[CountryCode] NVARCHAR(max) NULL
		);
END

INSERT INTO model.InsurerPhoneNumbers (
	[InsurerId]
	,[InsurerPhoneNumberTypeId]
	,[ExchangeAndSuffix]
	,[Extension]
	,[IsInternational]
	,[OrdinalId]
	,[AreaCode]
	,[CountryCode]
	)
SELECT [InsurerId]
	,[InsurerPhoneNumberTypeId]
	,[ExchangeAndSuffix]
	,[Extension]
	,[IsInternational]
	,[OrdinalId]
	,[AreaCode]
	,[CountryCode]
FROM dbo.InsurerPhoneNumbersBackup
GO

-- InsurerAddresses
IF OBJECT_ID('model.InsurerAddresses', 'U') IS NULL
BEGIN
	CREATE TABLE [model].[InsurerAddresses] (
		[InsurerId] INT NOT NULL
		,[InsurerAddressTypeId] INT NOT NULL
		,[Id] BIGINT IDENTITY(1, 1) NOT NULL
		,[Line1] NVARCHAR(max) NOT NULL
		,[Line2] NVARCHAR(max) NULL
		,[City] NVARCHAR(max) NOT NULL
		,[PostalCode] NVARCHAR(max) NOT NULL
		,[Line3] NVARCHAR(max) NULL
		,[StateOrProvinceId] INT NOT NULL
		);
END

INSERT INTO model.InsurerAddresses (
	[InsurerId]
	,[InsurerAddressTypeId]
	,[Line1]
	,[Line2]
	,[City]
	,[PostalCode]
	,[Line3]
	,[StateOrProvinceId]
	)
SELECT [InsurerId]
	,[InsurerAddressTypeId]
	,[Line1]
	,[Line2]
	,[City]
	,[PostalCode]
	,[Line3]
	,[StateOrProvinceId]
FROM dbo.InsurerAddressesBackup
GO

-- InsurerPhoneNumberTypes
IF OBJECT_ID('model.InsurerPhoneNumberTypes', 'U') IS NULL
BEGIN
	CREATE TABLE [model].[InsurerPhoneNumberTypes] (
		[Id] INT IDENTITY(1, 1) NOT NULL
		,[Name] NVARCHAR(max) NOT NULL
		,[Abbreviation] NVARCHAR(max) NOT NULL
		,[IsArchived] BIT NOT NULL
		);
END

SET IDENTITY_INSERT model.InsurerPhoneNumberTypes ON
INSERT INTO model.InsurerPhoneNumberTypes ([Id]
	,[Name]
	,[Abbreviation]
	,[IsArchived]
	)
SELECT [Id]
	,[Name]
	,[Abbreviation]
	,[IsArchived]
FROM dbo.InsurerPhoneNumberTypesBackup
SET IDENTITY_INSERT model.InsurerPhoneNumberTypes OFF
GO


-- Add indexes and keys (from tables.sql)
IF OBJECT_ID(N'[model].[FK_InsurerAddressTypeInsurerAddress]', 'F') IS NOT NULL
    ALTER TABLE [model].[InsurerAddresses] DROP CONSTRAINT [FK_InsurerAddressTypeInsurerAddress];
GO

IF OBJECT_ID(N'[model].[FK_InsurerAddressStateOrProvince]', 'F') IS NOT NULL
    ALTER TABLE [model].[InsurerAddresses] DROP CONSTRAINT [FK_InsurerAddressStateOrProvince];
GO

IF OBJECT_ID(N'[model].[FK_InsurerPhoneNumberTypeInsurerPhoneNumber]', 'F') IS NOT NULL
    ALTER TABLE [model].[InsurerPhoneNumbers] DROP CONSTRAINT [FK_InsurerPhoneNumberTypeInsurerPhoneNumber];
GO

-- Creating primary key on [Id] in table 'InsurerAddresses'
ALTER TABLE [model].[InsurerAddresses]
ADD CONSTRAINT [PK_InsurerAddresses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerInsurerAddress'
CREATE INDEX [IX_FK_InsurerInsurerAddress]
ON [model].[InsurerAddresses]
    ([InsurerId]);
GO

-- Creating foreign key on [InsurerAddressTypeId] in table 'InsurerAddresses'
ALTER TABLE [model].[InsurerAddresses]
ADD CONSTRAINT [FK_InsurerAddressTypeInsurerAddress]
    FOREIGN KEY ([InsurerAddressTypeId])
    REFERENCES [model].[InsurerAddressTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerAddressTypeInsurerAddress'
CREATE INDEX [IX_FK_InsurerAddressTypeInsurerAddress]
ON [model].[InsurerAddresses]
    ([InsurerAddressTypeId]);
GO

-- Creating foreign key on [StateOrProvinceId] in table 'InsurerAddresses'
ALTER TABLE [model].[InsurerAddresses]
ADD CONSTRAINT [FK_InsurerAddressStateOrProvince]
    FOREIGN KEY ([StateOrProvinceId])
    REFERENCES [model].[StateOrProvinces]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerAddressStateOrProvince'
CREATE INDEX [IX_FK_InsurerAddressStateOrProvince]
ON [model].[InsurerAddresses]
    ([StateOrProvinceId]);
GO

-- Creating primary key on [Id] in table 'InsurerPhoneNumbers'
ALTER TABLE [model].[InsurerPhoneNumbers]
ADD CONSTRAINT [PK_InsurerPhoneNumbers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerInsurerPhoneNumber'
CREATE INDEX [IX_FK_InsurerInsurerPhoneNumber]
ON [model].[InsurerPhoneNumbers]
    ([InsurerId]);
GO

-- Creating primary key on [Id] in table 'InsurerPhoneNumberTypes'
ALTER TABLE [model].[InsurerPhoneNumberTypes]
ADD CONSTRAINT [PK_InsurerPhoneNumberTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerPhoneNumberTypeInsurerPhoneNumber'
CREATE INDEX [IX_FK_InsurerPhoneNumberTypeInsurerPhoneNumber]
ON [model].[InsurerPhoneNumbers]
    ([InsurerPhoneNumberTypeId]);
GO

-- Creating foreign key on [InsurerPhoneNumberTypeId] in table 'InsurerPhoneNumbers'
ALTER TABLE [model].[InsurerPhoneNumbers]
ADD CONSTRAINT [FK_InsurerPhoneNumberTypeInsurerPhoneNumber]
    FOREIGN KEY ([InsurerPhoneNumberTypeId])
    REFERENCES [model].[InsurerPhoneNumberTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;