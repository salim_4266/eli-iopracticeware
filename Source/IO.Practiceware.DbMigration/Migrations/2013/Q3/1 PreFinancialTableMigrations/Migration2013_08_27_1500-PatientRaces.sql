﻿EXEC('
IF EXISTS(SELECT * FROM sys.views WHERE name = ''PatientRaces'')
BEGIN
	DROP VIEW model.PatientRaces
END
')

IF OBJECT_ID(N'[model].[PatientRaces]', 'U') IS NULL
BEGIN
	-- Create PatientRaces table
	EXEC('
	CREATE TABLE [model].[PatientRaces](
		[Id] int IDENTITY(1,1) NOT NULL,
		[Name] nvarchar(64) NOT NULL,
		[IsArchived] bit NOT NULL DEFAULT 0
	);
	')

	-- Creating primary key on [Id] in table 'PatientRaces'
	EXEC('ALTER TABLE [model].[PatientRaces]
	ADD CONSTRAINT [PK_PatientRaces]
		PRIMARY KEY CLUSTERED ([Id] ASC);
	')
	
	-- Migrate PatientRaces from PracticeCodeTable
	EXEC('
	SET IDENTITY_INSERT [model].[PatientRaces] ON
	
		INSERT INTO model.PatientRaces (Id, Name, IsArchived)
			SELECT Id,
				Code AS [Name],
				CONVERT(bit, 0) AS IsArchived
			FROM dbo.PracticeCodeTable
			WHERE ReferenceType = ''RACE''

	SET IDENTITY_INSERT [model].[PatientRaces] OFF
	')
END
GO


