﻿IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'IsArchived' and Object_ID = Object_ID(N'PracticeInsurers')) 
BEGIN
	ALTER TABLE dbo.PracticeInsurers
	ADD IsArchived bit NOT NULL default 0
END
	
GO