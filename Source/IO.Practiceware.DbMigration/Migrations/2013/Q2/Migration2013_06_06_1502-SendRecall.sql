/****** Object:  StoredProcedure [dbo].[SendRecalls]    Script Date: 06/05/2013 16:02:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.SendRecalls') IS NOT NULL
	DROP PROCEDURE [dbo].[SendRecalls]
GO

CREATE PROCEDURE [dbo].[SendRecalls](@Date int)
AS
BEGIN

SELECT PatientId
	,LastName
	,FirstName
	,Address
	,Suite
	,City
	,State
	,Zip
	,HomePhone
	,CellPhone
	,MIN(transactiondate)as RecallDueDate
FROM patientdemographics pd
INNER JOIN practicetransactionjournal ptj ON ptj.transactiontypeid = pd.patientid
	AND transactiontype = 'l'
	AND transactionstatus = 'P'
	AND transactiondate <= @Date
	and TransactionDate >= @Date-10000
	and TransactionAction = '-'
	and ((pd.Status = 'A') OR (pd.Status = 'I'))
GROUP BY PatientId
	,LastName
	,FirstName
	,address
	,suite
	,city
	,STATE
	,zip
	,homephone
	,cellphone
ORDER BY LastName
	,FirstName
	,address
	,suite
	,city
	,STATE
	,zip
	,homephone
	,cellphone

CREATE TABLE #temp (PatientId int, TransactionDate nvarchar(10))
INSERT INTO #temp (PatientId, TransactionDate)
SELECT PatientId, MIN(TransactionDate)
		FROM patientdemographics pd
		INNER JOIN practicetransactionjournal ptj ON ptj.transactiontypeid = pd.patientid
			AND transactiontype = 'l'
			AND transactionstatus = 'P'
			AND transactiondate <= @Date
			and TransactionDate >= @Date-10000
			and TransactionAction = '-'
			and ((pd.Status = 'A') OR (pd.Status = 'I'))
			GROUP BY PatientId

UPDATE PracticeTransactionJournal
SET TransactionStatus = 'S'
	,TransactionRemark = '[' + TransactionDate + ']'
	,TransactionDate = CONVERT(nvarchar, GetDate(), 112)
WHERE transactiontype = 'L'
	AND transactionstatus = 'P'
	AND transactiondate <= @Date
	and TransactionDate >= @Date-10000
	AND transactiontypeid IN (SELECT PatientId FROM #temp)

RETURN
END