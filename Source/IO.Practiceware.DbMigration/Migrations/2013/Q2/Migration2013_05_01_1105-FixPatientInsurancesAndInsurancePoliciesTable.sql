﻿--This migration attempts to clean up insurance policies which were billed,
--possibly got payment on, but the patient no longer has the insurance. 
--This irrelevant data is caused by the calculated functions, not the migration itself.
--In this case, this fix sets an end date for those patientinsurance rows.
IF OBJECT_ID('tempdb..#SetDateTimeForIncorrectEntries') IS NOT NULL
    DROP TABLE #SetDateTimeForIncorrectEntries

;WITH 
PatientDemographicsPrimaryPolicyIdDoesNotMatchModel AS 
(
	SELECT 
	PatientId
	,PolicyPatientId
	,Relationship
	,NEWPolicyHolderPatientId
	,NEWPolicyHolderRelationship
	FROM PatientDemographics 
	WHERE PolicyPatientId<>NEWPolicyHolderPatientId 
		AND PatientId=NEWPolicyHolderPatientId
)
,PatientDemographicsSecondaryPolicyIdDoesNotMatchModel AS
(
	SELECT 
	PatientId
	,SecondPolicyPatientId
	,SecondRelationship
	,NEWSecondPolicyHolderPatientId
	,NEWSecondPolicyHolderRelationship
	FROM PatientDemographics 
	WHERE SecondPolicyPatientId<>NEWSecondPolicyHolderPatientId 
		AND PatientId=NEWSecondPolicyHolderPatientId
)
SELECT 
pi.InsuredPatientId
,SecondPolicyPatientId
,SecondRelationship
,NEWSecondPolicyHolderPatientId
,NEWSecondPolicyHolderRelationship
,PolicyPatientId
,Relationship
,NEWPolicyHolderPatientId
,NEWPolicyHolderRelationship
INTO #SetDateTimeForIncorrectEntries
FROM model.PatientInsurances PI
LEFT JOIN PatientDemographicsPrimaryPolicyIdDoesNotMatchModel pdPri ON pdPri.NEWPolicyHolderPatientId=pi.InsuredPatientId
LEFT JOIN PatientDemographicsSecondaryPolicyIdDoesNotMatchModel pdSec ON pdSec.NEWSecondPolicyHolderPatientId=pi.InsuredPatientId
WHERE pi.EndDateTime IS NULL 
	AND (pdPri.PatientId IS NOT NULL OR pdSec.PatientId IS NOT NULL)

UPDATE model.PatientInsurances
SET EndDateTime = GETDATE()
FROM model.PatientInsurances pi
INNER JOIN (
	SELECT 
	InsuredPatientId
	,NEWPolicyHolderPatientId
	FROM #SetDateTimeForIncorrectEntries
	GROUP BY InsuredPatientId
	,NEWPolicyHolderPatientId
)pdPri ON pdPri.NEWPolicyHolderPatientId=pi.InsuredPatientId
WHERE pi.EndDateTime IS NULL

UPDATE model.PatientInsurances
SET EndDateTime = GETDATE()
FROM model.PatientInsurances pi
INNER JOIN (
	SELECT 
	InsuredPatientId
	,NEWSecondPolicyHolderPatientId
	FROM #SetDateTimeForIncorrectEntries
	GROUP BY InsuredPatientId
	,NEWSecondPolicyHolderPatientId
)pdSec ON pdSec.NEWSecondPolicyHolderPatientId=pi.InsuredPatientId
WHERE pi.EndDateTime IS NULL