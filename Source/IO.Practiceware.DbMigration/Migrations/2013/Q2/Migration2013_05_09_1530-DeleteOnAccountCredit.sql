/* On Account Credit.trdx was erroneously added to model.Documents
along with its more up-to-date version On Account Credits.trdx.
This deletes the obsolete version.*/

DELETE FROM model.Documents 
WHERE Name = 'On Account Credit.trdx'
