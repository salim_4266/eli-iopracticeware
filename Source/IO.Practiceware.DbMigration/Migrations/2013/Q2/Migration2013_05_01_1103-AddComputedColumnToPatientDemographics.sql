﻿--This migration adds to the PatientDemographics table the 4 calculated columns
DECLARE @tableName NVARCHAR(250)
SET @tableName = 'PatientDemographics'

ALTER TABLE PatientDemographics ADD NEWPolicyHolderPatientId AS (ISNULL(dbo.GetPolicyHolderPatientId(PatientId),PatientId))
ALTER TABLE PatientDemographics ADD NEWSecondPolicyHolderPatientId AS (ISNULL(dbo.GetSecondPolicyHolderPatientId(PatientId),0))
ALTER TABLE PatientDemographics ADD NEWPolicyHolderRelationship AS (ISNULL(dbo.GetPolicyHolderRelationshipType(PatientId),'Y'))
ALTER TABLE PatientDemographics ADD NEWSecondPolicyHolderRelationship AS (ISNULL(dbo.GetSecondPolicyHolderRelationshipType(PatientId),''))