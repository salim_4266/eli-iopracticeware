﻿-- Default global value for AppointmentMessageHandlerSettings
-- Check-In terms: "checkin", "arrived", "kept", "started", "checked-in"
-- Check-Out terms: "checkout", "completed"
-- ApplicationSettingTypeId = 3 = AppointmentMessageHandlerSettings

INSERT [model].[ApplicationSettings] ([Xml], [Name], [MachineName], [ApplicationSettingTypeId], [UserId]) 
VALUES (N'<z:anyType z:Id="1" i:type="a:AppointmentMessageHandlerSettings" xmlns:z="http://schemas.microsoft.com/2003/10/Serialization/" xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:a="http://schemas.datacontract.org/2004/07/IO.Practiceware.Model"><a:CheckInTerms z:Id="2" z:Size="5" xmlns:b="http://schemas.microsoft.com/2003/10/Serialization/Arrays"><b:string z:Id="3">checkin</b:string><b:string z:Id="4">arrived</b:string><b:string z:Id="5">kept</b:string><b:string z:Id="6">started</b:string><b:string z:Id="7">checked-in</b:string></a:CheckInTerms><a:CheckOutTerms z:Id="8" z:Size="2" xmlns:b="http://schemas.microsoft.com/2003/10/Serialization/Arrays"><b:string z:Id="9">checkout</b:string><b:string z:Id="10">completed</b:string></a:CheckOutTerms></z:anyType>', N'AppointmentMessageHandlerSettings', NULL, 3, NULL)
GO