﻿-- Views required for migration
-- Add columns 'ClaimAdjustmentReasonCodeId' and 'ClaimAdjustmentGroupCodeId' to Adjustments View

IF NOT EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'Invoices' and schemas.name = 'model') 

BEGIN
EXEC('CREATE VIEW [model].[Invoices]
	WITH SCHEMABINDING
	AS
	----ServiceLocation used for the AttributeTo property
	
	----Appt is main or satellite office (PracticeName), AttributeTo is main or satellite office (PracticeName) [NO OVERRIDE - HANDLED IN CLAIM FILE ONLY]
	SELECT v.Id AS Id, 
		MIN(v.AttributeTo) AS AttributeToServiceLocationId,
		v.BillingProviderId,
		v.[DateTime],
		v.DoesProviderRefuseAssignment,
		v.EncounterId,
		v.HasPatientAssignedBenefits,
		v.ClinicalInvoiceProviderId as ClinicalInvoiceProviderId,
		v.ReferringExternalProviderId,
		v.InvoiceTypeId,
		CONVERT(bit, v.IsNoProviderSignatureOnFile) AS IsNoProviderSignatureOnFile,
		v.IsReleaseOfInformationNotSigned,
		v.ServiceLocationCodeId
	FROM (
		SELECT pr.AppointmentId AS Id,
			pnAttribute.PracticeId AS AttributeTo,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + pr.BillToDr)) AS BillingProviderId,
			CONVERT(datetime, InvoiceDate, 112) AS [DateTime],
			CASE 
				WHEN pd.FinancialAssignment = ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS DoesProviderRefuseAssignment,
			ap.AppointmentId AS EncounterId,
			CASE pd.FinancialSignature
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS HasPatientAssignedBenefits,
			(CONVERT(bigint, 110000000) * ap.ResourceId1 + ap.AppointmentId) AS ClinicalInvoiceProviderId,
			CASE 
				WHEN pvExternalProvider.VendorId IS NULL
					THEN NULL
				ELSE pr.ReferDr 
				END AS ReferringExternalProviderId,
			CASE 
				WHEN re.ResourceType IN (''Q'', ''Y'')
					THEN 3
				ELSE 1
				END AS InvoiceTypeId,
			CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
			CASE pd.FinancialSignature
				WHEN ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsReleaseOfInformationNotSigned,
			CASE 
				WHEN MAX(pctSLCode.Id) IS NOT NULL AND MAX(pctSLCode.Id) <> ''''
					THEN MAX(pctSLCode.Id)
				ELSE (SELECT MAX(Id) AS Id FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLACEOFSERVICE''
						AND SUBSTRING(Code, 1, 2) = ''11''
					GROUP BY ReferenceType, Code
					)
			END AS ServiceLocationCodeId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
		INNER JOIN dbo.Resources re ON re.ResourceId = pr.BillToDr
		INNER JOIN dbo.Resources clinicalRe ON clinicalRe.ResourceId = ap.ResourceId1
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			AND pnServLoc.PracticeId =
				CASE 
					WHEN ap.ResourceId2 = 0
						THEN (SELECT PracticeId AS PracticeId
							FROM dbo.PracticeName
							WHERE PracticeType = ''P''
							AND LocationReference = '''')
					ELSE ap.ResourceId2 - 1000
					END 
		INNER JOIN dbo.PracticeName pnAttribute ON pnAttribute.PracticeType = ''P''
			AND pnAttribute.PracticeId =
				CASE 
					WHEN pr.BillingOffice = 0
						THEN (SELECT PracticeId AS PracticeId
							FROM dbo.PracticeName
							WHERE PracticeType = ''P''
							AND LocationReference = '''')
					ELSE pr.BillingOffice - 1000
					END
		LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = prs.PlaceOfService
			AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
		LEFT JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
		LEFT JOIN dbo.PracticeVendors pvExternalProvider ON pr.ReferDr = pvExternalProvider.VendorId AND pvExternalProvider.VendorLastName <> '''' AND pvExternalProvider.VendorLastName IS NOT NULL
		WHERE ap.Comments <> ''ASC CLAIM''
			AND (
				ap.ResourceId2 = 0
				OR ap.ResourceId2 > 1000
				)
			AND (
				pr.BillingOffice = 0
				OR pr.BillingOffice > 1000
				)
		GROUP BY pr.AppointmentId,
			pnAttribute.PracticeId,
			pnServLoc.PracticeId,
			pnBillOrg.PracticeId, 
			pr.BillToDr,
			pr.InvoiceDate,
			ap.AppointmentId,
			pd.FinancialSignature,
			pd.FinancialAssignment,
			ap.ResourceId1,
			pr.ReferDr,
			re.ResourceType,
			clinicalRe.ResourceType,
			apt.ResourceId8,
			pvExternalProvider.VendorId
	
		UNION ALL
	
		----Provider is ASC, appt satellite office (PracticeName), AttributeTo is main or satellite office (PracticeName)
		SELECT pr.AppointmentId AS Id,
			pnAttribute.PracticeId AS AttributeTo,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + reASC.ResourceId)) AS BillingProviderId,
			CONVERT(datetime, pr.InvoiceDate, 112) AS [DateTime],
			CASE 
				WHEN pd.FinancialAssignment = ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS DoesProviderRefuseAssignment,
			apEncounter.AppointmentId AS EncounterId,
			CASE pd.FinancialSignature
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS HasPatientAssignedBenefits,
			(CONVERT(bigint, 110000000) * reASC.ResourceId + ap.AppointmentId) AS ClinicalInvoiceProviderId,
			CASE 
				WHEN pvExternalProvider.VendorId IS NULL
					THEN NULL
				ELSE pr.ReferDr 
				END AS ReferringExternalProviderId,
			2 AS InvoiceTypeId,
			CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
			CASE pd.FinancialSignature
				WHEN ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsReleaseOfInformationNotSigned,
			CASE 
				WHEN MAX(pctSLCode.Id) IS NOT NULL AND MAX(pctSLCode.Id) <> ''''
					THEN MAX(pctSLCode.Id)
				ELSE (SELECT MAX(Id) AS Id FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLACEOFSERVICE''
						AND SUBSTRING(Code, 1, 2) = ''11''
					GROUP BY ReferenceType, Code
					)
			END AS ServiceLocationCodeId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
			AND ap.Comments = ''ASC CLAIM'' 
		INNER JOIN dbo.AppointmentType apt ON ap.AppTypeId = apt.AppTypeId
			AND apt.ResourceId8 = 1
		INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
			AND apEncounter.AppTypeId = ap.AppTypeId
			AND apEncounter.AppDate = ap.AppDate
			AND apEncounter.AppTime > 0 
			AND ap.AppTime = 0
			AND apEncounter.ScheduleStatus = ap.ScheduleStatus
			AND ap.ScheduleStatus = ''D''
			AND apEncounter.ResourceId1 = ap.ResourceId1
			AND apEncounter.ResourceId2 = ap.ResourceId2
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
			AND PracticeType = ''P''
			AND LocationReference <> ''''
		INNER JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
			AND reASC.ResourceType = ''R''
			AND reASC.ServiceCode = ''02''
			AND reASC.Billable = ''Y''
			AND pic.FieldValue = ''T''
		INNER JOIN dbo.PracticeName pnAttribute ON pnAttribute.PracticeType = ''P''
			AND pnAttribute.PracticeId =
				CASE 
					WHEN pr.BillingOffice = 0
						THEN (SELECT PracticeId AS PracticeId
							FROM dbo.PracticeName
							WHERE PracticeType = ''P''
							AND LocationReference = '''')
					ELSE pr.BillingOffice - 1000
					END
		LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = reASC.PlaceOfService
			AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
		LEFT JOIN dbo.PracticeVendors pvExternalProvider ON pr.ReferDr = pvExternalProvider.VendorId AND pvExternalProvider.VendorLastName <> '''' AND pvExternalProvider.VendorLastName IS NOT NULL
		WHERE ap.ResourceId2 > 1000
			AND (
				pr.BillingOffice = 0
				OR pr.BillingOffice > 1000
				)
		GROUP BY pr.AppointmentId,
			apEncounter.AppointmentId,
			ap.AppointmentId,
			pnAttribute.PracticeId,
			pnServLoc.PracticeId,
			pr.BillToDr,
			pr.InvoiceDate,
			pd.FinancialSignature,
			pd.FinancialAssignment,
			ap.ResourceId1,
			pr.ReferDr,
			reASC.ResourceType,
			reASC.ResourceId,
			apt.ResourceId8,
			pvExternalProvider.VendorId
	
		UNION ALL
	
		----Provider is ASC, appt is satellite office (PracticeName), AttributeTo a facility (Resources)
		SELECT pr.AppointmentId AS Id,
			reAttribute.ResourceId AS AttributeTo,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnServLoc.PracticeId + reASC.ResourceId)) AS BillingProviderId,
			CONVERT(datetime, pr.InvoiceDate, 112) AS [DateTime],
			CASE 
				WHEN pd.FinancialAssignment = ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS DoesProviderRefuseAssignment,
			apEncounter.AppointmentId AS EncounterId,
			CASE pd.FinancialSignature
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS HasPatientAssignedBenefits,
			(CONVERT(bigint, 110000000) * reASC.ResourceId + ap.AppointmentId) AS ClinicalInvoiceProviderId,
			CASE 
				WHEN pvExternalProvider.VendorId IS NULL
					THEN NULL
				ELSE pr.ReferDr 
				END AS ReferringExternalProviderId,
			2 AS InvoiceTypeId,
			CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
			CASE pd.FinancialSignature
				WHEN ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsReleaseOfInformationNotSigned,
			CASE 
				WHEN MAX(pctSLCode.Id) IS NOT NULL AND MAX(pctSLCode.Id) <> ''''
					THEN MAX(pctSLCode.Id)
				ELSE (SELECT MAX(Id) AS Id FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLACEOFSERVICE''
						AND SUBSTRING(Code, 1, 2) = ''11''
					GROUP BY ReferenceType, Code
					)
			END AS ServiceLocationCodeId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
			AND ap.Comments = ''ASC CLAIM'' 
		INNER JOIN dbo.AppointmentType apt ON ap.AppTypeId = apt.AppTypeId
			AND apt.ResourceId8 = 1
		INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
			AND apEncounter.AppTypeId = ap.AppTypeId
			AND apEncounter.AppDate = ap.AppDate
			AND apEncounter.AppTime > 0 
			AND ap.AppTime = 0
			AND apEncounter.ScheduleStatus = ap.ScheduleStatus
			AND ap.ScheduleStatus = ''D''
			AND apEncounter.ResourceId1 = ap.ResourceId1
			AND apEncounter.ResourceId2 = ap.ResourceId2
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
			AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
			AND PracticeType = ''P''
			AND LocationReference <> ''''
		INNER JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
			AND reASC.ResourceType = ''R''
			AND reASC.ServiceCode = ''02''
			AND reASC.Billable = ''Y''
			AND pic.FieldValue = ''T''
		INNER JOIN dbo.Resources reAttribute ON reAttribute.ResourceId = pr.BillingOffice
			AND reAttribute.ResourceType = ''R''
			AND reAttribute.ServiceCode = ''02''
			AND pr.BillingOffice between 1 and 999
		LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = reASC.PlaceOfService
			AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
		LEFT JOIN dbo.PracticeVendors pvExternalProvider ON pr.ReferDr = pvExternalProvider.VendorId AND pvExternalProvider.VendorLastName <> '''' AND pvExternalProvider.VendorLastName IS NOT NULL
		WHERE ap.ResourceId2 > 1000
			AND (
				pr.BillingOffice = 0
				OR pr.BillingOffice > 1000
				)
		GROUP BY pr.AppointmentId,
			apEncounter.AppointmentId,
			ap.AppointmentId,
			reAttribute.ResourceId,
			pnServLoc.PracticeId,
			pr.BillToDr,
			pr.InvoiceDate,
			pd.FinancialSignature,
			pd.FinancialAssignment,
			ap.ResourceId1,
			pr.ReferDr,
			reASC.ResourceType,
			reASC.ResourceId,
			apt.ResourceId8,
			pvExternalProvider.VendorId
			
		UNION ALL
	
		----Encounter is at main or satellite office (PracticeName); AttributeTo a facility (Resources) [NO OVERRIDE, OVERRIDE IS HANDLED IN CLAIM FILE ONLY]
		SELECT  pr.AppointmentId AS Id,
			(110000000 * 1 + reAttribute.ResourceId) AS AttributeTo,
			(CONVERT(bigint, 110000000) * pnServLoc.PracticeId + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS BillingProviderId,
			CONVERT(datetime, InvoiceDate, 112) AS DateTime,
			CASE 
				WHEN pd.FinancialAssignment = ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS DoesProviderRefuseAssignment,
			ap.AppointmentId AS EncounterId,
			CASE pd.FinancialSignature
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS HasPatientAssignedBenefits,
			(CONVERT(bigint, 110000000) * ap.ResourceId1 + ap.AppointmentId) AS ClinicalInvoiceProviderId,
			CASE 
				WHEN pr.ReferDr = 0 
					OR pr.ReferDr IS NULL 
					OR pr.ReferDr = ''''
					THEN NULL
				ELSE pr.ReferDr
				END AS ReferringExternalProviderId,
			CASE 
				WHEN re.ResourceType IN (''Q'', ''Y'')
					THEN 3
				ELSE 1
			END AS InvoiceTypeId,
			CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
			CASE pd.FinancialSignature
				WHEN ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsReleaseOfInformationNotSigned,
			CASE 
				WHEN MAX(pctSLCode.Id) IS NOT NULL
					THEN MAX(pctSLCode.Id)
				ELSE (SELECT MAX(Id) AS Id FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLACEOFSERVICE''
						AND SUBSTRING(Code, 1, 2) = ''11''
					GROUP BY ReferenceType, Code)
				END AS ServiceLocationCodeId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
		INNER JOIN dbo.Resources re ON re.ResourceId = pr.BillToDr
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = ''P''
	               AND pnServLoc.PracticeId = CASE 
	                       WHEN ap.ResourceId2 = 0
	                               THEN (SELECT PracticeId AS PracticeId FROM dbo.PracticeName WHERE LocationReference = '''' AND PracticeType = ''P'')
	                       ELSE ap.ResourceId2 - 1000
	                       END
		INNER JOIN dbo.Resources reAttribute ON reAttribute.ResourceId = pr.BillingOffice
			AND reAttribute.ResourceType = ''R''
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		LEFT JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
		LEFT JOIN dbo.PatientFinancial pf ON pf.PatientId = pr.InsuredId
			AND pr.InsurerId = pf.FinancialInsurerId
			AND InvoiceDate >= FinancialStartDate
			AND (
				InvoiceDate <= FinancialEndDate
				OR FinancialEndDate = ''''
				OR FinancialEndDate IS NULL
				)
		LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = prs.PlaceOfService
			AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
		WHERE (
				ap.ResourceId2 = 0
				OR ap.ResourceId2 > 1000
				)
			AND (pr.BillingOffice BETWEEN 1 AND 999 OR pr.BillingOffice > 2000)
		GROUP BY pr.AppointmentId,
			reAttribute.ResourceId,
			pnServLoc.PracticeId,
			pnBillOrg.PracticeId, 
			re.ResourceId,
			pr.InvoiceDate,
			ap.AppointmentId,
			pd.FinancialSignature,
			pd.FinancialAssignment,
			ap.ResourceId1,
			pr.ReferDr,
			re.ResourceType,
			apt.ResourceId8,
			re.PracticeId
	
		UNION ALL
	
		----Encounter is at an external facility (Resources); AttributeTo is facility (Resources)  [OVERRIDE IS HANDLED IN CLAIM FILE ONLY]
		SELECT  pr.AppointmentId AS Id,
			(110000000 * 1 + reAttribute.ResourceId) AS AttributeTo,
			(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS BillingProviderId,
			CONVERT(datetime, InvoiceDate, 112) AS DateTime,
			CASE 
				WHEN pd.FinancialAssignment = ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS DoesProviderRefuseAssignment,
			ap.AppointmentId AS EncounterId,
			CASE pd.FinancialSignature
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS HasPatientAssignedBenefits,
			(CONVERT(bigint, 110000000) * ap.ResourceId1 + ap.AppointmentId) AS ClinicalInvoiceProviderId,
			CASE 
				WHEN pr.ReferDr = 0 
					OR pr.ReferDr IS NULL 
					OR pr.ReferDr = ''''
					THEN NULL
				ELSE pr.ReferDr
				END AS ReferringExternalProviderId,
			CASE 
				WHEN re.ResourceType IN (''Q'', ''Y'')
					THEN 3
				ELSE 1
				END AS InvoiceTypeId,
			CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
			CASE pd.FinancialSignature
				WHEN ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsReleaseOfInformationNotSigned,
			CASE 
				WHEN MAX(pctSLCode.Id) IS NOT NULL
					THEN MAX(pctSLCode.Id)
				ELSE (SELECT MAX(Id)  AS Id FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLACEOFSERVICE''
						AND SUBSTRING(Code, 1, 2) = ''11''
					GROUP BY ReferenceType, Code)
				END AS ServiceLocationCodeId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
		INNER JOIN dbo.Resources re ON re.ResourceId = pr.BillToDr
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceId = ap.ResourceId2
			AND reServLoc.ResourceType = ''R''
		INNER JOIN dbo.Resources reAttribute ON reAttribute.ResourceId = pr.BillingOffice
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = prs.PlaceOfService
				AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
		LEFT JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
		LEFT JOIN dbo.PatientFinancial pf ON pf.PatientId = pr.InsuredId
			AND pr.InsurerId = pf.FinancialInsurerId
			AND InvoiceDate >= FinancialStartDate
			AND (
				InvoiceDate <= FinancialEndDate
				OR FinancialEndDate = ''''
				OR FinancialEndDate IS NULL
				)
		WHERE (ap.ResourceId2 BETWEEN 1 AND 999 OR ap.ResourceId2 > 2000)
			AND (pr.BillingOffice BETWEEN 1 AND 999 OR pr.BillingOffice > 2000)
		GROUP BY pr.AppointmentId,
			reAttribute.ResourceId,
			reServLoc.ResourceId,
			pnBillOrg.PracticeId, 
			re.ResourceId,
			pr.InvoiceDate,
			ap.AppointmentId,
			pd.FinancialSignature,
			pd.FinancialAssignment,
			ap.ResourceId1,
			pr.ReferDr,
			re.ResourceType,
			apt.ResourceId8,
			reServLoc.PracticeId
	
		UNION ALL
	
		----appt at facility (Resources); AttributeTo at main or satellite office (PracticeName) [NO OVERRIDE, HANDLED IN CLAIM FILE ONLY]
		SELECT  pr.AppointmentId AS Id,
			pnAttribute.PracticeId AS AttributeTo,
			(CONVERT(bigint, 110000000) * (110000000 * 1 + reServLoc.ResourceId) + (CONVERT(bigint, 110000000) * pnBillOrg.PracticeId + re.ResourceId)) AS BillingProviderId,
			CONVERT(datetime, InvoiceDate, 112) AS DateTime,
			CASE 
				WHEN pd.FinancialAssignment = ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS DoesProviderRefuseAssignment,
			ap.AppointmentId AS EncounterId,
			CASE pd.FinancialSignature
				WHEN ''Y''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS HasPatientAssignedBenefits,
			(CONVERT(bigint, 110000000) * ap.ResourceId1 + ap.AppointmentId) AS ClinicalInvoiceProviderId,
			CASE 
				WHEN pr.ReferDr = 0 
					OR pr.ReferDr IS NULL 
					OR pr.ReferDr = ''''
					THEN NULL
				ELSE pr.ReferDr
				END AS ReferringExternalProviderId,
			CASE 
				WHEN re.ResourceType IN (''Q'', ''Y'')
					THEN 3
				ELSE 1
				END AS InvoiceTypeId,
			CONVERT(bit, 0) AS IsNoProviderSignatureOnFile,
			CASE pd.FinancialSignature
				WHEN ''N''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsReleaseOfInformationNotSigned,
			CASE 
				WHEN MAX(pctSLCode.Id) IS NOT NULL
					THEN MAX(pctSLCode.Id)
				ELSE (SELECT MAX(Id)  AS Id FROM dbo.PracticeCodeTable
					WHERE ReferenceType = ''PLACEOFSERVICE''
						AND SUBSTRING(Code, 1, 2) = ''11''
					GROUP BY ReferenceType, Code)
				END AS ServiceLocationCodeId,
			COUNT_BIG(*) AS Count
		FROM dbo.PatientReceivables pr
		INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pr.PatientId
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pr.AppointmentId
		INNER JOIN dbo.Resources re ON re.ResourceId = pr.BillToDr
		INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
		INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceId = ap.ResourceId2
			AND reServLoc.ResourceType = ''R''
		INNER JOIN dbo.PracticeName pnAttribute ON pnAttribute.PracticeType = ''P''
			AND pnAttribute.PracticeId = CASE 
				WHEN pr.BillingOffice = 0
					THEN (SELECT PracticeId AS PracticeId
						FROM dbo.PracticeName
						WHERE PracticeType = ''P''
						AND LocationReference = '''')
				ELSE pr.BillingOffice - 1000
				END
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = ''ASC''
		LEFT JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
		LEFT OUTER JOIN dbo.PatientReceivableServices prs ON prs.Invoice = pr.Invoice
			AND prs.Status = ''A''
		LEFT OUTER JOIN dbo.PracticeCodeTable pctSLCode ON SUBSTRING(pctSLCode.Code, 1, 2) = prs.PlaceOfService
				AND pctSLCode.ReferenceType = ''PLACEOFSERVICE''
		LEFT JOIN dbo.PatientFinancial pf ON pf.PatientId = pr.InsuredId
			AND pr.InsurerId = pf.FinancialInsurerId
			AND InvoiceDate >= FinancialStartDate
			AND (
				InvoiceDate <= FinancialEndDate
				OR FinancialEndDate = ''''
				OR FinancialEndDate IS NULL
				)
		WHERE (ap.ResourceId2 BETWEEN 1 AND 999 OR ap.ResourceId2 > 2000)
			AND (
				pr.BillingOffice = 0
				OR pr.BillingOffice > 1000
				) 
		GROUP BY pr.AppointmentId,
			pnAttribute.PracticeId,
			reServLoc.ResourceId,
			pnBillOrg.PracticeId, 
			re.ResourceId,
			pr.InvoiceDate,
			ap.AppointmentId,
			pd.FinancialAssignment,
			pd.FinancialSignature,
			ap.ResourceId1,
			pr.ReferDr,
			re.ResourceType,
			pic.FieldValue,
			apt.ResourceId8,
			reServLoc.PracticeId
	
			) AS v
	GROUP BY v.Id,
		v.BillingProviderId,
		v.[DateTime],
		v.DoesProviderRefuseAssignment,
		v.EncounterId,
		v.HasPatientAssignedBenefits,
		v.ClinicalInvoiceProviderId,
		v.ReferringExternalProviderId,
		v.InvoiceTypeId,
		v.IsNoProviderSignatureOnFile,
		v.IsReleaseOfInformationNotSigned,
		v.ServiceLocationCodeId')
	END
GO

IF EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'Adjustments' and schemas.name = 'model') 

BEGIN

EXEC ('ALTER VIEW [model].[Adjustments]
	AS
	SELECT v.Id AS Id
		, v.AdjustmentTypeId AS AdjustmentTypeId
		, v.Amount AS Amount
		, v.FinancialBatchId AS FinancialBatchId
		, InsurerId
		,CASE 
			WHEN pd.PatientId IS NULL 
				THEN NULL 
			ELSE v.PatientId 
			END AS PatientId
		,CASE 
			WHEN v.BillingServiceId  = 0
				THEN NULL
			WHEN prs.[Status] = ''X''
				THEN NULL
			ELSE 
				v.BillingServiceId
			END AS BillingServiceId
		,v.FinancialSourceTypeId AS FinancialSourceTypeId
		,v.InvoiceReceivableId AS InvoiceReceivableId
		,v.PaymentMethodId AS PaymentMethodId
		,COALESCE(v.PostedDateTime, CONVERT(datetime, ''20000101'')) AS PostedDateTime
		,CONVERT(int, v.ClaimAdjustmentReasonCodeId) AS ClaimAdjustmentReasonCodeId
		,CONVERT(int, v.ClaimAdjustmentGroupCodeId) AS ClaimAdjustmentGroupCodeId
	FROM (
		SELECT prp.PaymentId AS Id
		,CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
				WHEN  ''P''
					THEN 1
				WHEN ''X''
					THEN 2
				WHEN ''8''
					THEN 3
				WHEN ''R''
					THEN 4
				WHEN ''U''
					THEN 5
				ELSE pct.Id + 1000
		END AS AdjustmentTypeId
		,CASE
			WHEN prp.PaymentReason IS NULL 
				THEN 
					CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
						WHEN ''X'' -- AdjustmentTypeId = 2
							THEN paymentReason45.Id -- ReasonCode 45
						WHEN ''8'' -- AdjustmentTypeId = 3
							THEN paymentReason45.Id -- ReasonCode 45
						ELSE 
							CAST(NULL AS int)
					END
			WHEN prp.PaymentReason = ''''
				THEN 
					CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
						WHEN ''X'' -- AdjustmentTypeId = 2
							THEN paymentReason45.Id -- ReasonCode 45
						WHEN ''8'' -- AdjustmentTypeId = 3
							THEN paymentReason45.Id -- ReasonCode 45
						ELSE 
							CAST(NULL AS int)
					END
			ELSE
				existingPaymentReason.Id --(SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = ''PAYMENTREASON'' AND Code = prp.PaymentReason)
		 END AS ClaimAdjustmentReasonCodeId
		, CASE
			WHEN prp.ClaimAdjustmentGroupCodeId IS NULL 
				THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
					 -- These int values correspond to the ClaimAdjustmentGroupCode enum in .NET
	
						WHEN ''X'' -- AdjustmentTypeId = 2
							THEN 1 -- ContractualObligation
						WHEN ''8'' -- AdjustmentTypeId = 3
							THEN 3 -- OtherAdjustment
						WHEN ''R'' -- AdjustmentTypeId = 4
							THEN 4 -- CorrectionReversal
						WHEN ''U'' -- AdjustmentTypeId = 5
							THEN 4 -- CorrectionReversal
						ELSE 
							CAST(NULL AS int)
					 END
			ELSE
				prp.ClaimAdjustmentGroupCodeId
		 END AS ClaimAdjustmentGroupCodeId
		,prp.PaymentAmountDecimal AS Amount
		,CASE WHEN PaymentBatchCheckId IN (0, -1) THEN NULL ELSE PaymentBatchCheckId END AS FinancialBatchId
		,CASE 
				WHEN prp.PayerType = ''I'' AND PayerId > 0
					THEN prp.PayerId
				ELSE NULL
			END AS InsurerId
			,CASE 
				WHEN prp.PayerType = ''P'' 
					THEN prp.PayerId 
				WHEN prp.PayerType = ''I'' AND pr.InsurerId = 99999 
					THEN pr.PatientId
				ELSE NULL 
				END AS PatientId
			,prp.PaymentServiceItem AS BillingServiceId
			,prp.PayerId AS PayerId
		,CASE prp.PayerType
				WHEN ''I''
					THEN 1
				WHEN ''P''
					THEN 2
				WHEN ''O''
					THEN 3
				ELSE 4
				END AS FinancialSourceTypeId
		,(CONVERT(bigint, 110000000) * (CASE 
									WHEN (ComputedFinancialId IS NULL OR ComputedFinancialId = 0 OR PayerType = ''P'') AND pr.InsurerId <> 99999
									THEN 0
								ELSE ComputedFinancialId 
								END) + pr.AppointmentId) AS InvoiceReceivableId
		,pctPaymentMethod.Id AS PaymentMethodId
		,CONVERT(datetime, prp.PaymentDate, 112) AS PostedDateTime
		FROM dbo.PatientReceivablePayments prp
		INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
		INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''PAYMENTTYPE'' AND prp.PaymentType = SUBSTRING(pct.Code, LEN(pct.Code), 1)
		LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason45 ON paymentReason45.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = ''45'')
		LEFT OUTER JOIN dbo.PracticeCodeTable existingPaymentReason ON existingPaymentReason.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = prp.PaymentReason)
		LEFT JOIN dbo.PracticeCodeTable pctPaymentMethod ON pctPaymentMethod.ReferenceType = ''PAYABLETYPE'' and prp.PaymentRefType = SUBSTRING(pctPaymentMethod.Code, LEN(pctPaymentMethod.Code), 1)
		WHERE prp.PaymentFinancialType <> ''''
				AND ((prp.PayerType = ''I'' AND pr.ComputedFinancialId IS NOT NULL)
					OR
					prp.PayerType <> ''I''
					OR 
					(prp.PayerType = ''I'' AND pr.InsurerId = 99999 AND pr.ComputedFinancialId IS NULL))
		) v
	LEFT JOIN dbo.PatientReceivableServices prs ON prs.ItemId = v.BillingServiceId
	LEFT JOIN dbo.PatientDemographics pd ON pd.PatientId = v.PayerId')

END

GO


IF EXISTS (SELECT *, OBJECT_DEFINITION(object_id('model.AdjustmentTypes')) FROM sys.objects 
			WHERE type = 'V' 
				AND name = 'AdjustmentTypes' 
				AND schema_id = schema_id('model')
				AND OBJECT_DEFINITION(object_id('model.AdjustmentTypes')) NOT LIKE '%pct.AlternateCode IN (''C'', ''D'')%'
				AND OBJECT_DEFINITION(object_id('model.AdjustmentTypes')) NOT LIKE '%model.AdjustmentTypes_Internal%')
BEGIN
EXEC('ALTER VIEW [model].[AdjustmentTypes]
	WITH SCHEMABINDING
	AS
	SELECT v.Id AS Id,
		v.FinancialTypeGroupId AS FinancialTypeGroupId,
		v.IsCash AS IsCash,
		v.IsDebit AS IsDebit,
		v.IsPrintOnStatement AS IsPrintOnStatement,
		v.Name AS Name,
		v.ShortName AS ShortName,
		v.IsArchived AS IsArchived
	FROM (SELECT CASE
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''P''
				THEN 1
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''X''
				THEN 2
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''8''
				THEN 3
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''I''
				THEN 3
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''R''
				THEN 4
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''U''
				THEN 5
			ELSE pct.Id + 1000
			END AS Id,
		NULL AS FinancialTypeGroupId,
			CASE 
				WHEN SUBSTRING(Code, LEN(code), 1) IN (
						''U'',
						''R'',
						''P''
						)
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsCash,
			CASE pct.AlternateCode
				WHEN ''D''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
				END AS IsDebit,
			CONVERT(bit, 1) AS IsPrintOnStatement,
			SUBSTRING(Code, 1, (dbo.GetMax((charindex(''-'', code) - 2),0))) AS [Name],
			LetterTranslation AS ShortName,
			CONVERT(bit, 0) AS IsArchived
		FROM dbo.PracticeCodeTable pct
		WHERE pct.ReferenceType = ''PAYMENTTYPE''
			AND	pct.AlternateCode IN (''C'', ''D'')
		GROUP BY Id,
			Code,
			pct.AlternateCode,
			LetterTranslation
		) AS v')
END
GO
