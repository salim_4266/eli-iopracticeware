﻿-- In an effort to move the PatientDemographic table to a view we:
-- 1. drop all triggers associated with the table
-- 2. drop all model views
-- 3. rename PatientDemographics PatientDemographicsTable
-- 4. create a PatientDemographics view with trigger instead of insert (update, delete also)
--    (strips PolicyPatientId, Relationship, SecondPolicyPatientId, SecondRelationship)

-- 1. drop all triggers associated with the table
IF EXISTS(SELECT * FROM sysobjects WHERE name = 'AuditPatientDemographicsDeleteTrigger')
	DROP TRIGGER [AuditPatientDemographicsDeleteTrigger]
	
IF EXISTS(SELECT * FROM sysobjects WHERE name = 'AuditPatientDemographicsInsertTrigger')
	DROP TRIGGER [AuditPatientDemographicsInsertTrigger]
	
IF EXISTS(SELECT * FROM sysobjects WHERE name = 'AuditPatientDemographicsUpdateTrigger')
	DROP TRIGGER [AuditPatientDemographicsUpdateTrigger]
	
IF EXISTS(SELECT * FROM sysobjects WHERE name = 'PatientDemographicsComputedFinancialIdTrigger')
	DROP TRIGGER [PatientDemographicsComputedFinancialIdTrigger]

IF EXISTS(SELECT * FROM sysobjects WHERE name = 'PatientDemographicsExternalSystemMessagesTrigger')
	DROP TRIGGER [PatientDemographicsExternalSystemMessagesTrigger]
	
IF EXISTS(SELECT * FROM sysobjects WHERE name = 'PatientDemographicsPatientIdPatientReceivablePaymentsTrigger')
	DROP TRIGGER [PatientDemographicsPatientIdPatientReceivablePaymentsTrigger]

IF EXISTS(SELECT * FROM sysobjects WHERE name = 'InsteadOfInsertStripPolicyPatientIds')
	DROP TRIGGER [InsteadOfInsertStripPolicyPatientIds]

-- 2. drop all model views
EXEC model.DropModelViews

-- 3. rename PatientDemographics PatientDemographicsTable
EXEC sp_rename 'PatientDemographics', 'PatientDemographicsTable'

-- 4. create a PatientDemographics view with trigger instead of insert (update, delete also)
--    (strips PolicyPatientId, Relationship, SecondPolicyPatientId, SecondRelationship)
EXEC('
CREATE VIEW PatientDemographics 
WITH SCHEMABINDING, VIEW_METADATA
AS 
SELECT [PatientId]
      ,[LastName]
      ,[FirstName]
      ,[MiddleInitial]
      ,[NameReference]
      ,[SocialSecurity]
      ,[Address]
      ,[Suite]
      ,[City]
      ,[State]
      ,[Zip]
      ,[HomePhone]
      ,[CellPhone]
      ,[Email]
      ,[EmergencyName]
      ,[EmergencyPhone]
      ,[EmergencyRel]
      ,[Occupation]
      ,[BusinessName]
      ,[BusinessAddress]
      ,[BusinessSuite]
      ,[BusinessCity]
      ,[BusinessState]
      ,[BusinessZip]
      ,[BusinessPhone]
      ,[BusinessType]
      ,[Gender]
      ,[Marital]
      ,[BirthDate]
      ,[NationalOrigin]
      ,[Language]
      ,[SchedulePrimaryDoctor]
      ,[PrimaryCarePhysician]
      ,[ReferringPhysician]
      ,[ProfileComment1]
      ,[ProfileComment2]
      ,[ContactType]
      ,[BillParty]
      ,[SecondBillParty]
      ,[ReferralCatagory]
      ,[BulkMailing]
      ,[FinancialAssignment]
      ,[FinancialSignature]
      ,[FinancialSignatureDate]
      ,[Status]
      ,[Salutation]
      ,[Hipaa]
      ,[PatType]
      ,[BillingOffice]
      ,[SendStatements]
      ,[SendConsults]
      ,[OldPatient]
      ,[ReferralRequired]
      ,[MedicareSecondary]
      ,[PostOpExpireDate]
      ,[PostOpService]
      ,[EmployerPhone]
      ,[Religion]
      ,[Race]
      ,[Ethnicity]
      ,[SecondRelationshipArchived]
      ,[SecondPolicyPatientIdArchived]
      ,[RelationshipArchived]
      ,[PolicyPatientIdArchived]
      ,[PolicyPatientId]
      ,[Relationship]
      ,[SecondPolicyPatientId]
      ,[SecondRelationship]
FROM dbo.PatientDemographicsTable
')

