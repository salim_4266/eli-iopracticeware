﻿IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'IsMedicareAdvantage' and Object_ID = Object_ID(N'PracticeInsurers')) 
BEGIN
	ALTER TABLE dbo.PracticeInsurers
	ADD IsMedicareAdvantage bit NOT NULL default 0
END
	
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'Hpid' and Object_ID = Object_ID(N'PracticeInsurers')) 
BEGIN
	ALTER TABLE dbo.PracticeInsurers
	ADD Hpid nvarchar(255) NULL
END
	
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'Oeid' and Object_ID = Object_ID(N'PracticeInsurers')) 
BEGIN
	ALTER TABLE dbo.PracticeInsurers
	ADD Oeid nvarchar(255) NULL
END
	
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'Website' and Object_ID = Object_ID(N'PracticeInsurers')) 
BEGIN
	ALTER TABLE dbo.PracticeInsurers
	ADD Website nvarchar(2048) NULL
END
	
GO