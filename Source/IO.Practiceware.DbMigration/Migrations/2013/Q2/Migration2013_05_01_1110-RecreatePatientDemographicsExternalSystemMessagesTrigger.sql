﻿--This migration creates triggers on the PatientDemographicTable and PatientFinancial view and table.
--The PatientDemographic triggers were dropped to allow migration to a view.
--The PatientFinancial trigger is to be dropped at the end of the migration.

CREATE TRIGGER dbo.PatientDemographicsExternalSystemMessagesTrigger
   ON  dbo.PatientDemographicsTable
   FOR INSERT, UPDATE NOT FOR REPLICATION
AS 
BEGIN
SET NOCOUNT ON;
	 DECLARE @now as DATETIME
	 SET @now = GETUTCDATE()
	 DECLARE @insertTable TABLE(Id UNIQUEIDENTIFIER, MessageType INT, PatientId INT, [Description] NVARCHAR(255))
	 DECLARE @createdBy nvarchar(max)
		 INSERT INTO @insertTable 
		 SELECT NEWID(), esesmt.Id, i.PatientId, ESMT.Name
		 FROM (
			 SELECT [PatientId],[LastName],[FirstName],[MiddleInitial],[NameReference],[SocialSecurity],[Address],[Suite],
			 [City],[State],[Zip],[HomePhone],[CellPhone],[Email],[EmergencyName],[EmergencyPhone],[EmergencyRel],[Occupation],
			 [BusinessName],[BusinessAddress],[BusinessSuite],[BusinessCity],[BusinessState],[BusinessZip],[BusinessPhone],
			 [BusinessType],[Gender],[Marital],[BirthDate],[NationalOrigin],[Language],[SchedulePrimaryDoctor],[PrimaryCarePhysician],
			 [ReferringPhysician],[ProfileComment1],[ProfileComment2],[ContactType],[PolicyPatientId],[Relationship],[BillParty],
			 [SecondPolicyPatientId],[SecondRelationship],[SecondBillParty],[ReferralCatagory],[BulkMailing],[FinancialAssignment],
			 [FinancialSignature],[FinancialSignatureDate],[Status],[Salutation],[Hipaa],[PatType],[BillingOffice],[SendStatements],
			 [SendConsults],[OldPatient],[ReferralRequired],[MedicareSecondary],[PostOpExpireDate],[PostOpService],[EmployerPhone],
			 [Religion],[Race],[Ethnicity] 
			 FROM inserted 
		 EXCEPT 
			 SELECT [PatientId],[LastName],[FirstName],[MiddleInitial],[NameReference] ,[SocialSecurity],[Address],[Suite],
			 [City],[State],[Zip],[HomePhone],[CellPhone],[Email],[EmergencyName],[EmergencyPhone],[EmergencyRel],[Occupation],
			 [BusinessName],[BusinessAddress],[BusinessSuite],[BusinessCity],[BusinessState],[BusinessZip],[BusinessPhone],
			 [BusinessType],[Gender],[Marital],[BirthDate],[NationalOrigin],[Language],[SchedulePrimaryDoctor],[PrimaryCarePhysician]
			 ,[ReferringPhysician],[ProfileComment1],[ProfileComment2],[ContactType],[PolicyPatientId],[Relationship],[BillParty],
			 [SecondPolicyPatientId],[SecondRelationship],[SecondBillParty],[ReferralCatagory],[BulkMailing],[FinancialAssignment],
			 [FinancialSignature],[FinancialSignatureDate],[Status],[Salutation],[Hipaa],[PatType],[BillingOffice],[SendStatements],
			 [SendConsults],[OldPatient],[ReferralRequired],[MedicareSecondary],[PostOpExpireDate],[PostOpService],[EmployerPhone],
			 [Religion],[Race],[Ethnicity] 
			FROM deleted
			) i
		 CROSS JOIN model.ExternalSystemExternalSystemMessageTypes esesmt
		 INNER JOIN model.ExternalSystemMessageTypes esmt ON esesmt.ExternalSystemMessageTypeId = esmt.Id AND esmt.Name IN ('ADT_A28_V23') AND esmt.IsOutbound = 1

	 -- Create rows for each ExternalSystemExternalSystemMessageType subscribed for ADT_A28_V23
	 -- If deleted table contains rows, then this was an UPDATE
	 IF EXISTS(SELECT * FROM deleted)
	 BEGIN
		 SET @createdBy = 'PatientUpdate'
	 END
	 ELSE
	 BEGIN
		 SET @createdBy = 'PatientInsert'
	 END

	 -- Create the messages
	 INSERT INTO model.ExternalSystemMessages(Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId, ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId)

	 -- Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId (1 = Unprocessed, 2 = Processing, 3 = Processed),
	 -- ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId
	 SELECT Id, [Description], '', MessageType, 1, 0, @createdBy, @now, @now, ''
	 FROM @insertTable
	 
	 -- Add entity associated with each message for PatientId
	 INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
	 
	 -- Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey
	 SELECT NEWID(), it.Id, pre.Id, PatientId
	 FROM
	 @insertTable it INNER JOIN model.PracticeRepositoryEntities pre ON pre.Name = 'Patient'
 END
GO