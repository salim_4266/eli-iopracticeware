﻿SET IDENTITY_INSERT [model].[Permissions] ON

	INSERT INTO [model].[Permissions] (Id, Name) VALUES (16, 'AddPatientInsuranceForSelf')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (17, 'AddPatientInsuranceForOther')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (18, 'EditPatientInsuranceForSelf')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (19, 'EditPatientInsuranceForOther')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (20, 'DeletePatientInsurance')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (21, 'DeletePatientInsuranceDocument')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (22, 'ViewInsurersAndPlans')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (23, 'CreatePolicyholder')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (35, 'EditPolicyholder')

SET IDENTITY_INSERT [model].[Permissions] OFF
GO

INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)

SELECT CONVERT(bit,0) AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 16 -- 'AddPatientInsuranceForSelf'
