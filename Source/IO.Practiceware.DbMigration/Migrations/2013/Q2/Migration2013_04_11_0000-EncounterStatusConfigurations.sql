﻿-- Clearing the EncounterStatusConfigurations table, and repopulating with all values except 'rescheduled'

TRUNCATE TABLE [model].[EncounterStatusConfigurations]
GO

SET IDENTITY_INSERT [model].[EncounterStatusConfigurations] ON

INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'F5EAF7', 1)--pending
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'F27D27', 2)--questions
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'E1F227', 3)--waitingroom
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'5BF227', 4)--exam
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'279FF2', 5)--exp co
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'279FF2', 6)--checkout
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FF331689', 7)--discharged
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 8)--cx ofc
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 9)--cx pat
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 10)--cx sd
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 11)--cx ns
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 12)--cx left
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 13)--cx spec
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'FFA39B98', 14)--cx other
INSERT [model].[EncounterStatusConfigurations] (HexColor, Id) VALUES (N'B81AA7', 15)--In the checkout screen of Admin

SET IDENTITY_INSERT [model].[EncounterStatusConfigurations] OFF

GO
