﻿--this migration holds the 4 functions used for calculating the PatientDemographic table's:
--PolicyPatientId (returns an int)
--Relationship (returns nvarchar(1) ex: (Y,S,C))
--SecondPolicyPatientId (returns an int)
--SecondRelationship (returns nvarchar(1) ex: (Y,S,C))

IF EXISTS (
    SELECT 
	name 
	FROM sysobjects 
	WHERE xtype IN(N'FN', N'IF', N'TF')
    AND id = object_id(N'GetPolicyHolderPatientId') 
)
DROP FUNCTION dbo.GetPolicyHolderPatientId

EXEC (N'
CREATE FUNCTION dbo.GetPolicyHolderPatientId(@PatientId INT) 
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN 
RETURN
	 (
	 SELECT TOP 1
	 ip.PolicyHolderPatientId 
	 FROM model.PatientInsurances pi
	 INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	 INNER JOIN (
		 SELECT
		 v.InsuredPatientId
		 ,MIN(OrdinalId) AS LowestOrdinalId
		 ,v.LowestInsuranceTypeId
		 FROM model.PatientInsurances pi 
		 INNER JOIN (
			SELECT
			MIN(pi.InsuranceTypeId) AS LowestInsuranceTypeId
			,@PatientId AS InsuredPatientId
			FROM model.PatientInsurances pi
			WHERE pi.InsuredPatientId = @PatientId
				AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
				AND pi.IsDeleted <> 1
		)v ON v.InsuredPatientId = pi.InsuredPatientId
			AND v.LowestInsuranceTypeId = pi.InsuranceTypeId
		WHERE (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
			AND pi.IsDeleted <> 1
		GROUP BY v.InsuredPatientId
		,v.LowestInsuranceTypeId
	) y ON y.InsuredPatientId = pi.InsuredPatientId
	AND y.LowestInsuranceTypeId = pi.InsuranceTypeId
	AND y.LowestOrdinalId = pi.OrdinalId
	WHERE pi.InsuredPatientId = @PatientId
	AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
		AND pi.IsDeleted <> 1
	)
END
')

IF EXISTS (
    SELECT 
	name 
	FROM sysobjects 
	WHERE xtype IN(N'FN', N'IF', N'TF')
    AND id = object_id(N'GetPolicyHolderRelationshipType') 
)
DROP FUNCTION dbo.GetPolicyHolderRelationshipType

EXEC (N'
CREATE FUNCTION dbo.GetPolicyHolderRelationshipType(@PatientId INT) 
RETURNS NVARCHAR(1)
WITH SCHEMABINDING
AS
BEGIN 
RETURN
	 (
		SELECT TOP 1
		CASE pi.PolicyHolderRelationshipTypeId
			WHEN 1
				THEN ''Y''
			WHEN 2
				THEN ''S''
			WHEN 3
				THEN ''C''
			WHEN 4
				THEN ''E''
			WHEN 6
				THEN ''L''
			ELSE ''O'' 
		END AS PolicyHolderRelationshipType
		FROM model.PatientInsurances pi
		INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		INNER JOIN (
			SELECT
			v.InsuredPatientId
			,MIN(OrdinalId) AS LowestOrdinalId
			,v.LowestInsuranceTypeId
			FROM model.PatientInsurances pi 
			INNER JOIN (
			SELECT
			MIN(pi.InsuranceTypeId) AS LowestInsuranceTypeId
			,@PatientId AS InsuredPatientId
			FROM model.PatientInsurances pi
			WHERE pi.InsuredPatientId = @PatientId
				AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
				AND pi.IsDeleted <> 1
		)v ON v.InsuredPatientId = pi.InsuredPatientId
			AND v.LowestInsuranceTypeId = pi.InsuranceTypeId
		WHERE (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
			AND pi.IsDeleted <> 1
		GROUP BY v.InsuredPatientId
		,v.LowestInsuranceTypeId
		) y ON y.InsuredPatientId = pi.InsuredPatientId
		AND y.LowestInsuranceTypeId = pi.InsuranceTypeId
		AND y.LowestOrdinalId = pi.OrdinalId
		WHERE pi.InsuredPatientId = @PatientId
		AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
		AND pi.IsDeleted <> 1
	)
END
')

IF EXISTS (
    SELECT 
	name 
	FROM sysobjects 
	WHERE xtype IN(N'FN', N'IF', N'TF')
    AND id = object_id(N'GetSecondPolicyHolderPatientId') 
)
DROP FUNCTION dbo.GetSecondPolicyHolderPatientId

EXEC (N'
CREATE FUNCTION dbo.GetSecondPolicyHolderPatientId(@PatientId INT) 
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN
RETURN (
	SELECT 
	TOP 1 PolicyHolderPatientId
	FROM model.PatientInsurances pi
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	WHERE pi.InsuredPatientId = @PatientId
	AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
	AND pi.IsDeleted <> 1
	AND ip.PolicyHolderPatientId NOT IN
		(SELECT 
		 ip.PolicyHolderPatientId 
		 FROM model.PatientInsurances pi
		 INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		 INNER JOIN (
			 SELECT
			 v.InsuredPatientId
			 ,MIN(OrdinalId) AS LowestOrdinalId
			 ,v.LowestInsuranceTypeId
			 FROM model.PatientInsurances pi 
			 INNER JOIN (
				SELECT
				MIN(pi.InsuranceTypeId) AS LowestInsuranceTypeId
				,@PatientId AS InsuredPatientId
				FROM model.PatientInsurances pi
				WHERE pi.InsuredPatientId = @PatientId
					AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
					AND pi.IsDeleted <> 1
			)v ON v.InsuredPatientId = pi.InsuredPatientId
				AND v.LowestInsuranceTypeId = pi.InsuranceTypeId
			WHERE (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
				AND pi.IsDeleted <> 1
			GROUP BY v.InsuredPatientId
			,v.LowestInsuranceTypeId
		) y ON y.InsuredPatientId = pi.InsuredPatientId
		AND y.LowestInsuranceTypeId = pi.InsuranceTypeId
		AND y.LowestOrdinalId = pi.OrdinalId
		WHERE pi.InsuredPatientId = @PatientId
		AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
			AND pi.IsDeleted <> 1
	)
) 
END
')

IF EXISTS (
    SELECT 
	name 
	FROM sysobjects 
	WHERE xtype IN(N'FN', N'IF', N'TF')
    AND id = object_id(N'GetSecondPolicyHolderRelationshipType') 
)
DROP FUNCTION dbo.GetSecondPolicyHolderRelationshipType

EXEC (N'
CREATE FUNCTION dbo.GetSecondPolicyHolderRelationshipType(@PatientId INT) 
RETURNS NVARCHAR(1)
WITH SCHEMABINDING
AS
BEGIN
RETURN (
	SELECT TOP 1
	CASE pi.PolicyHolderRelationshipTypeId
		WHEN 1
			THEN ''Y''
		WHEN 2
			THEN ''S''
		WHEN 3
			THEN ''C''
		WHEN 4
			THEN ''E''
		WHEN 6
			THEN ''L''
		ELSE ''O'' 
	END AS PolicyHolderRelationshipType
	FROM model.PatientInsurances pi
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	WHERE pi.InsuredPatientId = @PatientId
		AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
		AND pi.IsDeleted <> 1
		AND PolicyHolderPatientId NOT IN
			(SELECT 
			 ip.PolicyHolderPatientId 
			 FROM model.PatientInsurances pi
			 INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			 INNER JOIN (
				 SELECT
				 v.InsuredPatientId
				 ,MIN(OrdinalId) AS LowestOrdinalId
				 ,v.LowestInsuranceTypeId
				 FROM model.PatientInsurances pi 
				 INNER JOIN (
					SELECT
					MIN(pi.InsuranceTypeId) AS LowestInsuranceTypeId
					,@PatientId AS InsuredPatientId
					FROM model.PatientInsurances pi
					WHERE pi.InsuredPatientId = @PatientId
						AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
						AND pi.IsDeleted <> 1
				)v ON v.InsuredPatientId = pi.InsuredPatientId
					AND v.LowestInsuranceTypeId = pi.InsuranceTypeId
				WHERE (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
					AND pi.IsDeleted <> 1
				GROUP BY v.InsuredPatientId
				,v.LowestInsuranceTypeId
			) y ON y.InsuredPatientId = pi.InsuredPatientId
			AND y.LowestInsuranceTypeId = pi.InsuranceTypeId
			AND y.LowestOrdinalId = pi.OrdinalId
			WHERE pi.InsuredPatientId = @PatientId
			AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= CONVERT(DATETIME,CONVERT(NVARCHAR,GETDATE(),112),112))
				AND pi.IsDeleted <> 1
	)
) 
END
')