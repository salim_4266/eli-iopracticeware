﻿/*

Increase the length of the policy number format column and transform the old format into regular expressions

*/


EXEC dbo.DropObject 'model.Insurers'

ALTER TABLE dbo.practiceinsurers
ALTER COLUMN InsurerPlanFormat NVARCHAR(256) NULL

update dbo.practiceinsurers set InsurerPlanFormat = replace(InsurerPlanFormat, 'N', '\d') where InsurerPlanFormat is not null and InsurerPlanFormat != '';
update dbo.practiceinsurers set InsurerPlanFormat = replace(InsurerPlanFormat, 'A', '[a-zA-Z]') where InsurerPlanFormat is not null  and InsurerPlanFormat != '';
update dbo.practiceinsurers set InsurerPlanFormat = replace(InsurerPlanFormat, 'E', '[\w]')  where InsurerPlanFormat is not null and InsurerPlanFormat != '';
update dbo.practiceinsurers set InsurerPlanFormat = replace(InsurerPlanFormat, 'O', '[\w]?') where InsurerPlanFormat is not null and InsurerPlanFormat != '';
update dbo.practiceinsurers set InsurerPlanFormat = '\b' + InsurerPlanFormat + '\b' where InsurerPlanFormat is not null and InsurerPlanFormat != '';


