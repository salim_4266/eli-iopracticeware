﻿--This migration is a stored procedure that finds all views that are associated with a table and drops them
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.DropViewsLike(@SchemaName NVARCHAR(70), @TheViewName NVARCHAR(70))
AS
BEGIN	
	SET NOCOUNT ON
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_SCHEMA = @SchemaName AND TABLE_NAME LIKE @TheViewName+'%') 
	BEGIN
		DECLARE @TableThatHoldsTheTableName NVARCHAR(250)
		DECLARE CursorToDropMultipleViews CURSOR FAST_FORWARD
		FOR SELECT @SchemaName+'.'+TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_SCHEMA = @SchemaName AND TABLE_NAME LIKE @TheViewName+'%'

		OPEN CursorToDropMultipleViews 
		FETCH NEXT FROM CursorToDropMultipleViews INTO @TableThatHoldsTheTableName
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC DropObject @TableThatHoldsTheTableName
			FETCH NEXT FROM CursorToDropMultipleViews INTO @TableThatHoldsTheTableName
		END
		CLOSE CursorToDropMultipleViews
		DEALLOCATE CursorToDropMultipleViews
	END
END
GO