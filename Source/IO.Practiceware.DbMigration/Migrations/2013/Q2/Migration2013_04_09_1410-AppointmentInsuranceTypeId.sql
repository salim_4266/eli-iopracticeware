﻿ALTER TABLE dbo.Appointments
ADD AppointmentInsuranceTypeId AS (CASE
	WHEN ApptInsType = 'M'
		THEN 1
	WHEN ApptInsType = 'V'
		THEN 2
	WHEN ApptInsType = 'A'
		THEN 3
	WHEN ApptInsType = 'W'
		THEN 4
	ELSE NULL
END
) PERSISTED
GO

CREATE INDEX IX_Appointments_AppointmentInsuranceTypeId ON dbo.Appointments(AppointmentInsuranceTypeId);
GO