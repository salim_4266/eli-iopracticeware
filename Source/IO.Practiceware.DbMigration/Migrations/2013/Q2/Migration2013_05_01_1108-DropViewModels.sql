﻿-- this migration is a stored procedure that drops all the model views.

IF NOT EXISTS (SELECT name FROM sys.procedures where name = 'DropModelViews' AND SCHEMA_ID = 6)
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE model.DropModelViews
AS
BEGIN

DECLARE @sql nvarchar(max)
SET @sql = (SELECT STUFF((
         SELECT ''EXEC dbo.DropObject '''''' + ''['' + SCHEMA_NAME(schema_id) + ''].['' + name +'']'' + '''''' '' FROM sys.views WHERE SCHEMA_NAME(schema_id) = ''model''
         FOR XML PATH('''')),1,0,''''))

EXEC(@sql)

END'
END