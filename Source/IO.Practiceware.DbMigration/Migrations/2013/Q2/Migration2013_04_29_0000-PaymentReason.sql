﻿IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PAYMENTREASON' AND Code = '223')
INSERT INTO PracticeCodeTable
(ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, Signature, TestOrder, FollowUp, Rank)
SELECT Top 1 ReferenceType, '223', 'Newly Mandated Adjustment Code', 'F', '', '', '', 'F', '', '', '1'
FROM PracticeCodeTable
WHERE ReferenceType = 'PAYMENTREASON'