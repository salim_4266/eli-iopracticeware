﻿--This migration is a stored procedure that takes a table name and column name and:
--1: adds to the table a column named "{ColumnName}Archived"
--2: disables all triggers on the table
--3: updates the new "{ColumnName}Archived" column with the values that are currently in "{ColumnName}"
--4: re-enables all triggers on the table
--5: drop all constraints where the name of the constraint contains the column name
--6: drop all objects which definition contains the column name from the table (uses DropObject)
--7: drop the column (uses DropColumnAndDefaultConstraints)
SET ANSI_NULLS ON
GO

IF OBJECT_ID('dbo.ArchiveAndDropColumn') IS NOT NULL
DROP PROC dbo.ArchiveAndDropColumn
GO

CREATE PROCEDURE dbo.ArchiveAndDropColumn
	@TableName NVARCHAR(MAX), 
    @ColumnName NVARCHAR(MAX)
AS
BEGIN

DECLARE @ColumnDataType NVARCHAR(100)
DECLARE @SqlStatementToAlterTableAddArchivedColumns NVARCHAR(250)
SELECT 
@ColumnDataType = 
	CASE 
		WHEN s.name = 'NVARCHAR' 
		THEN 'NVARCHAR(' + CAST(c.max_length AS VARCHAR(4)) + ')' 
		ELSE s.name 
	END             
FROM sys.tables t 
JOIN sys.columns c ON t.object_id = c.object_id 
JOIN sys.types s ON c.user_type_id = s.user_type_id 
WHERE t.name = @TableName AND c.name = @ColumnName

--1: adds to the table a column named "{ColumnName}Archived"
SET @SqlStatementToAlterTableAddArchivedColumns = 'ALTER TABLE '+ @TableName+ ' ADD ' + @ColumnName+'Archived ' + @ColumnDataType 
EXEC sp_executesql @SqlStatementToAlterTableAddArchivedColumns

DECLARE @SqlStatementToReplicateColumnsUnformatted NVARCHAR(MAX)
SET @SqlStatementToReplicateColumnsUnformatted = 
--2: disables all triggers on the table
--3: updates the new "{ColumnName}Archived" column with the values that are currently in "{ColumnName}"
'
ALTER TABLE {TableName} DISABLE TRIGGER ALL;
UPDATE {TableName}
SET {Column}Archived = {Column}
ALTER TABLE {TableName} ENABLE TRIGGER ALL;'
--4: re-enables all triggers on the table
DECLARE @SqlStatementToReplicateColumnsReplaceWithTableName NVARCHAR(MAX)
SET @SqlStatementToReplicateColumnsReplaceWithTableName = 
	REPLACE(@SqlStatementToReplicateColumnsUnformatted,'{TableName}',@TableName)
DECLARE @SqlStatementToReplicateColumnsReplaceWithColumn NVARCHAR(MAX)
SET @SqlStatementToReplicateColumnsReplaceWithColumn = 
	REPLACE(@SqlStatementToReplicateColumnsReplaceWithTableName,'{Column}',@ColumnName)
EXECUTE (@SqlStatementToReplicateColumnsReplaceWithColumn)

SET NOCOUNT ON

--5: drop all constraints where the name of the constraint contains the column name
DECLARE @SqlStatementToDropConstraints NVARCHAR(MAX)
DECLARE @TableOfConstraintsToBeDropped NVARCHAR(250)
DECLARE CursorToDropMultipleConstraints CURSOR FAST_FORWARD FOR 
SELECT   
CONSTRAINT_NAME
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
WHERE TABLE_NAME = @TableName
AND CONSTRAINT_NAME LIKE '%'+@ColumnName+'%'

OPEN CursorToDropMultipleConstraints 
FETCH NEXT FROM CursorToDropMultipleConstraints INTO @TableOfConstraintsToBeDropped
WHILE @@FETCH_STATUS <> -1
BEGIN
	SELECT 
	@SqlStatementToDropConstraints = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + CONSTRAINT_NAME 
	FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
	WHERE constraint_catalog = DB_NAME() 
	AND table_name = @TableName
	AND CONSTRAINT_NAME LIKE '%'+@ColumnName+'%'
	EXEC sp_executesql @SqlStatementToDropConstraints
	--PRINT @TableOfConstraintsToBeDropped
	FETCH NEXT FROM CursorToDropMultipleConstraints INTO @TableOfConstraintsToBeDropped
END
CLOSE CursorToDropMultipleConstraints
DEALLOCATE CursorToDropMultipleConstraints

IF OBJECT_ID('tempdb..#ObjectsToDrop') IS NOT NULL
    DROP TABLE #ObjectsToDrop

;WITH ObtainAllObjectsWhichDefinitionContainsTheColumnName AS (
SELECT 
y.schema_name+'.'+o.name AS ObjName
FROM sys.sql_dependencies d
JOIN sys.objects o ON o.object_id=d.object_id
JOIN sys.objects r ON r.object_id=d.referenced_major_id
JOIN (
	SELECT name,definition
	FROM sys.objects o
	JOIN sys.sql_modules m ON m.object_id = o.object_id
	WHERE [definition] LIKE  '%'+@ColumnName+'%'
) v ON v.Name = o.name
JOIN (
	SELECT  
	sys.objects.name
	,sys.schemas.name AS SCHEMA_NAME
	FROM sys.objects 
	INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
) y ON y.name = o.name
WHERE d.class=1 AND (o.name = @TableName OR r.name= @TableName)
)
--6: drop all objects which definition contains the column name from the table (uses DropObject)
SELECT 
DISTINCT ObjName
INTO #ObjectsToDrop
FROM ObtainAllObjectsWhichDefinitionContainsTheColumnName

DECLARE @TableOfObjectsToBeDropped NVARCHAR(250)
DECLARE CursorToDropMultipleObjects CURSOR FAST_FORWARD FOR 
SELECT * FROM #ObjectsToDrop
OPEN CursorToDropMultipleObjects 
FETCH NEXT FROM CursorToDropMultipleObjects INTO @TableOfObjectsToBeDropped
WHILE @@FETCH_STATUS <> -1
BEGIN
	PRINT @TableOfObjectsToBeDropped
    EXEC DropObject @TableOfObjectsToBeDropped	
	FETCH NEXT FROM CursorToDropMultipleObjects INTO @TableOfObjectsToBeDropped
END
CLOSE CursorToDropMultipleObjects
DEALLOCATE CursorToDropMultipleObjects

--7: drop the column (uses DropColumnAndDefaultConstraints)
EXEC dbo.DropColumnAndDefaultConstraints @TableName, @ColumnName
END

GO
