﻿--This migration is responsible for dropping all instances of the
--usage of the ComputedFinancialId, as well as recreating 2 indexes on PatientReceivables:
--IX_PatientReceivables_InsurerId
--IX_PatientReceivables_AppointmentId


IF OBJECT_ID('dbo.GetPatientReceivablesFinancialIds') IS NOT NULL
	EXEC('DROP FUNCTION dbo.GetPatientReceivablesFinancialIds')

IF OBJECT_ID('dbo.GetBulkPatientReceivablesFinancialIds') IS NOT NULL
	EXEC('DROP FUNCTION dbo.GetBulkPatientReceivablesFinancialIds')

IF OBJECT_ID('PatientFinancialPatientReceivablesFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientFinancialPatientReceivablesFinancialIdTrigger')
END

IF OBJECT_ID('PatientReceivablesFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivablesFinancialIdTrigger')
END

IF OBJECT_ID('PatientFinancialPatientReceivablesFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientFinancialPatientReceivablesFinancialIdTrigger')
END

IF OBJECT_ID('PatientReferralFinancialId') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReferralFinancialId')
END

IF OBJECT_ID('PatientReferralFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReferralFinancialIdTrigger')
END

IF OBJECT_ID('PatientFinancialPatientReferalFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientFinancialPatientReferalFinancialIdTrigger')
END

IF OBJECT_ID('PatientReferralFinancialIdTrigger') IS NOT NULL
BEGIN
	EXEC('DROP TRIGGER PatientReferralFinancialIdTrigger')
END

IF OBJECT_ID('PatientPreCertsFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientPreCertsFinancialIdTrigger')
END

IF OBJECT_ID('PatientFinancialPatientPreCertsFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientFinancialPatientPreCertsFinancialIdTrigger')
END

IF OBJECT_ID('PatientDemographicsComputedFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientDemographicsComputedFinancialIdTrigger')
END

IF EXISTS(SELECT name FROM sysindexes where name = 'IX_PatientReceivables_InsurerId')
BEGIN
	DROP INDEX PatientReceivables.IX_PatientReceivables_InsurerId
END

CREATE NONCLUSTERED INDEX IX_PatientReceivables_InsurerId
ON PatientReceivables(InsurerId) INCLUDE (ReceivableId, AppointmentId, ExternalRefInfo)      

IF EXISTS(SELECT name FROM sysindexes where name = 'IX_PatientReceivables_AppointmentId')
BEGIN
	DROP INDEX PatientReceivables.IX_PatientReceivables_AppointmentId
END

CREATE NONCLUSTERED INDEX IX_PatientReceivables_AppointmentId
ON PatientReceivables(AppointmentId) INCLUDE (ReceivableId, InsurerId, ExternalRefInfo)

IF OBJECT_ID('GetPatientReferralsFinancialIds') IS NOT NULL
BEGIN
    EXEC('DROP FUNCTION GetPatientReferralsFinancialIds')
END

IF OBJECT_ID('GetBulkPatientReferralsFinancialIds') IS NOT NULL
BEGIN
    EXEC('DROP FUNCTION GetBulkPatientReferralsFinancialIds')
END

IF OBJECT_ID('dbo.GetPatientPreCertsFinancialIds') IS NOT NULL
BEGIN
	EXEC('DROP FUNCTION dbo.GetPatientPreCertsFinancialIds')
END

IF OBJECT_ID('dbo.GetBulkPatientPreCertsFinancialIds') IS NOT NULL
BEGIN
	EXEC('DROP FUNCTION dbo.GetBulkPatientPreCertsFinancialIds')
END