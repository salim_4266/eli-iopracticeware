﻿IF OBJECT_ID(N'[model].[FK_PermissionUserPermission]', 'F') IS NOT NULL
    ALTER TABLE [model].[UserPermissions] DROP CONSTRAINT [FK_PermissionUserPermission];
GO

IF OBJECT_ID(N'[model].[FK_RolePermissionPermission]', 'F') IS NOT NULL
    ALTER TABLE [model].[RolePermissions] DROP CONSTRAINT [FK_RolePermissionPermission];
GO

IF OBJECT_ID(N'[model].[PK_RolePermissions]') IS NOT NULL
    ALTER TABLE [model].[RolePermissions] DROP CONSTRAINT [PK_RolePermissions];
GO

IF EXISTS (
		SELECT NAME
		FROM sysindexes
		WHERE NAME = 'IX_FK_RolePermissionPermission'
		)
	DROP INDEX IX_FK_RolePermissionPermission ON [model].[RolePermissions]
GO

IF EXISTS (
		SELECT NAME
		FROM sysindexes
		WHERE NAME = 'IX_FK_RolePermission_Permission'
		)
	DROP INDEX IX_FK_RolePermission_Permission ON [model].[RolePermissions]
GO

IF EXISTS (
		SELECT NAME
		FROM sysindexes
		WHERE NAME = 'IX_FK_RolePermission_Role'
		)
	DROP INDEX IX_FK_RolePermission_Role ON [model].[RolePermissions]
GO

IF EXISTS (
		SELECT NAME
		FROM sysindexes
		WHERE NAME = 'IX_FK_UserPermission_Permission'
		)
	DROP INDEX IX_FK_UserPermission_Permission ON [model].[UserPermissions]
GO

TRUNCATE TABLE [model].[RolePermissions]
GO

TRUNCATE TABLE [model].[UserPermissions]
GO

DROP TABLE [model].[RolePermissions]
GO

DROP TABLE [model].[UserPermissions]
GO

-- Creating table 'RolePermissions'
CREATE TABLE [model].[RolePermissions] 
(
	[IsDenied] bit NOT NULL,
	[RoleId] int NOT NULL,
	[PermissionId] int NOT NULL
)
GO

-- Creating table 'UserPermissions'
CREATE TABLE [model].[UserPermissions] 
(
	[IsDenied] bit,
	[UserId] int NOT NULL,
	[PermissionId] int NOT NULL
)
GO

-- Creating primary key on [RoleId], [PermissionId] in table 'RolePermissions'
ALTER TABLE [model].[RolePermissions]
ADD CONSTRAINT [PK_RolePermissions]
    PRIMARY KEY CLUSTERED ([RoleId], [PermissionId] ASC);
GO

-- Creating primary key on [PermissionId], [UserId] in table 'UserPermissions'
ALTER TABLE [model].[UserPermissions]
ADD CONSTRAINT [PK_UserPermissions]
    PRIMARY KEY CLUSTERED ([PermissionId], [UserId] ASC);
GO

-- Creating foreign key on [RoleId] in table 'RolePermissions'
ALTER TABLE [model].[RolePermissions]
ADD CONSTRAINT [FK_RolePermissionRole]
    FOREIGN KEY ([RoleId])
    REFERENCES [model].[Roles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [PermissionId] in table 'RolePermissions'
ALTER TABLE [model].[RolePermissions]
ADD CONSTRAINT [FK_RolePermissionPermission]
    FOREIGN KEY ([PermissionId])
    REFERENCES [model].[Permissions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RolePermissionPermission'
CREATE INDEX [IX_FK_RolePermissionPermission]
ON [model].[RolePermissions]
    ([PermissionId]);
GO

-- Creating foreign key on [PermissionId] in table 'UserPermissions'
ALTER TABLE [model].[UserPermissions]
ADD CONSTRAINT [FK_PermissionUserPermission]
    FOREIGN KEY ([PermissionId])
    REFERENCES [model].[Permissions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RolePermissionPermission'
CREATE INDEX [IX_FK_RolePermission_Permission]
ON [model].[RolePermissions]
    ([PermissionId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RolePermissionRole'
CREATE INDEX [IX_FK_RolePermission_Role]
ON [model].[RolePermissions]
    ([RoleId]);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserPermissionPermission'
CREATE INDEX [IX_FK_UserPermission_Permission]
ON [model].[UserPermissions]
    ([PermissionId]);
GO

EXEC dbo.DropObject 'model.PhoneNumbers'
EXEC dbo.DropObject 'model.Patient'
EXEC dbo.DropObject 'model.Patients_Partition2'

ALTER TABLE dbo.PatientDemographics
ALTER COLUMN EmergencyName nvarchar(70) NULL

GO

INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)
SELECT IsDenied, UserId, PermissionId FROM (
SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 3, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 1 -- 'ScheduleAppointment'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 11, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 2 -- 'ViewSchedule'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 37, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 3 -- 'ConfigureAppointmentCategories'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 20, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 4 -- 'InsertDeleteCategoriesInSchedule'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 36, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 5 -- 'CreateScheduleTemplates'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 52, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 6 -- 'ApplyTemplatesToDoctorsSchedule'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 3, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 7 -- 'CancelAppointment'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 3, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 8 -- 'RescheduleAppointment'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 17, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 9 -- 'CreateGlobalAndResourceScheduleComments'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 20, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 10 -- 'ForceScheduleAppointment'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 68, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 11 -- 'ChangeScheduleDoctor'

UNION ALL

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 12, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 12 -- 'SubmitTransactions'

UNION ALL

SELECT CONVERT(bit,0) AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
INNER JOIN dbo.PracticeCodeTable pct ON SUBSTRING(pct.Code, 1, 1) = re.ResourceType
	AND pct.ReferenceType = 'RESOURCETYPE'
WHERE (re.ResourceType = 'D' -- Doctor
OR re.ResourceType = 'T'-- Technician
OR pct.Code = 'Q Optical Supplier' -- Optical Supplier
OR re.EndTime7 = 'NU' -- Nurse
OR pct.Code = 'Practice Manager') -- Practice Manager
AND pe.Id = 13 -- 'CancelAppointmentInExamRoom'
) v 
EXCEPT 
SELECT IsDenied, UserId, PermissionId FROM model.UserPermissions