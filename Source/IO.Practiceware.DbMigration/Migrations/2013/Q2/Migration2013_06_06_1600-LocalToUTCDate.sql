IF EXISTS (SELECT * FROM sys.Triggers WHERE name = 'AuditExternalSystemMessagesUpdateTrigger') 
BEGIN
	DROP TRIGGER [model].[AuditExternalSystemMessagesUpdateTrigger]
END
GO

IF OBJECT_ID('dbo.LocalToUTCDate') IS NOT NULL
	DROP FUNCTION [dbo].[LocalToUTCDate]
GO

CREATE FUNCTION [dbo].[LocalToUTCDate] (
	@LocalDate DATETIME
	,@TZ INT
	)
RETURNS DATETIME
AS
BEGIN
	/* If the timzone does not already equal GMT 0 */
	IF @TZ <> 0
	BEGIN
		DECLARE @UTCDate DATETIME
		DECLARE @UTCDelta INT
		DECLARE @thisYear INT
		DECLARE @DSTDay INT
		DECLARE @NormalDay INT
		DECLARE @DSTDate DATETIME
		DECLARE @NormalDate DATETIME

		SET @thisYear = CONVERT(INT, DATEPART(yyyy, @LocalDate))

		IF (@thisYear < 2007)
			/* DST for prior 2007 */
		BEGIN
			SET @DSTDay = (2 + 6 * @thisYear - FLOOR(@thisYear / 4)) % 7 + 1
			SET @NormalDay = (31 - (FLOOR(@thisYear * 5 / 4) + 1) % 7)
			SET @DSTDate = CONVERT(DATETIME, '4/' + CONVERT(VARCHAR(2), @DSTDay) + '/' + CONVERT(VARCHAR(4), @thisYear) + ' 2:00:00.000 AM')
			SET @NormalDate = CONVERT(DATETIME, '10/' + CONVERT(VARCHAR(2), @NormalDay) + '/' + CONVERT(VARCHAR(4), @thisYear) + ' 2:00:00.000 AM')
		END
		ELSE
			/* DST for 2007 and after */
		BEGIN
			SET @DSTDay = (14 - (FLOOR(1 + @thisYear * 5 / 4)) % 7)
			SET @NormalDay = (7 - (FLOOR(1 + @thisYear * 5 / 4)) % 7)
			SET @DSTDate = CONVERT(DATETIME, '3/' + CONVERT(VARCHAR(2), @DSTDay) + '/' + CONVERT(VARCHAR(4), @thisYear) + ' 2:00:00.000 AM')
			SET @NormalDate = CONVERT(DATETIME, '11/' + CONVERT(VARCHAR(2), @NormalDay) + '/' + CONVERT(VARCHAR(4), @thisYear) + ' 2:00:00.000 AM')
		END

		/* Adjust timezone if in DST Period */
		IF (
				(@LocalDate > @DSTDate)
				AND (@LocalDate < @NormalDate)
				)
		BEGIN
			SET @UTCDelta = @TZ 
		END
		/* Adjust timezone without DST */
		ELSE
		BEGIN
			SET @UTCDelta = @TZ + 1
		END

		/* now CONVERT UTC date to local date */
		SET @UTCDate = DATEADD(hh, @UTCDelta, @LocalDate)
	END
	ELSE
		/* If the timzone already equals GMT 0 without DST */
	BEGIN
		SET @UTCDate = @LocalDate
	END

	RETURN (@UTCDate)
END
GO

IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit
GO

SELECT 1 AS Value INTO #NoAudit

UPDATE model.ExternalSystemMessages SET CreatedDateTime = dbo.LocalToUTCDate(CreatedDateTime, DATEDIFF(hh, GETDATE(), GETUTCDATE())),
										UpdatedDateTime = dbo.LocalToUTCDate(UpdatedDateTime, DATEDIFF(hh, GETDATE(), GETUTCDATE()))
GO