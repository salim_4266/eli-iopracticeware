IF OBJECT_ID(N'[model].[FK_PatientInsurancePatientInsuranceDocument]', 'F') IS NOT NULL
    ALTER TABLE [model].[PatientInsuranceDocuments] DROP CONSTRAINT [FK_PatientInsurancePatientInsuranceDocument];
GO

-- Creating table 'PatientInsuranceDocuments'
CREATE TABLE [model].[PatientInsuranceDocuments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Content] varbinary(max)  NOT NULL,
    [PatientInsuranceId] bigint  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'PatientInsuranceDocuments'
ALTER TABLE [model].[PatientInsuranceDocuments]
ADD CONSTRAINT [PK_PatientInsuranceDocuments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [PatientInsuranceId] in table 'PatientInsuranceDocuments'
ALTER TABLE [model].[PatientInsuranceDocuments]
ADD CONSTRAINT [FK_PatientInsurancePatientInsuranceDocument]
    FOREIGN KEY ([PatientInsuranceId])
    REFERENCES [model].[PatientInsurances]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientInsurancePatientInsuranceDocument'
CREATE INDEX [IX_FK_PatientInsurancePatientInsuranceDocument]
ON [model].[PatientInsuranceDocuments]
    ([PatientInsuranceId]);
GO