﻿--Removes legacy affiliation numbers if insurer not present.

DELETE pa
FROM dbo.PracticeAffiliations pa
WHERE PlanId NOT IN (SELECT InsurerId FROM dbo.PracticeInsurers)