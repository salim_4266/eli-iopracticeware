
IF NOT EXISTS (
    SELECT  schema_name
    FROM    information_schema.schemata
    WHERE   schema_name = 'fdb' ) 

    BEGIN
    EXEC sp_executesql N'CREATE SCHEMA fdb'
    END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fdb].[tblCompositeDrug]') AND type in (N'U'))
BEGIN
CREATE TABLE [fdb].[tblCompositeDrug](
	[MEDID] [int] NOT NULL,
	[GCN_SEQNO] [int] NOT NULL,
	[MED_NAME_ID] [int] NOT NULL,
	[MED_NAME] [nvarchar](30) NOT NULL,
	[MED_ROUTED_MED_ID_DESC] [nvarchar](60) NOT NULL,
	[MED_ROUTED_DF_MED_ID_DESC] [nvarchar](60) NOT NULL,
	[MED_MEDID_DESC] [nvarchar](70) NOT NULL,
	[MED_STATUS_CD] [nvarchar](1) NOT NULL,
	[MED_ROUTE_ID] [int] NOT NULL,
	[ROUTED_MED_ID] [int] NOT NULL,
	[ROUTED_DOSAGE_FORM_MED_ID] [int] NOT NULL,
	[MED_STRENGTH] [nvarchar](15) NULL,
	[MED_STRENGTH_UOM] [nvarchar](15) NULL,
	[MED_ROUTE_ABBR] [nvarchar](4) NOT NULL,
	[MED_ROUTE_DESC] [nvarchar](30) NOT NULL,
	[MED_DOSAGE_FORM_ABBR] [nvarchar](4) NOT NULL,
	[MED_DOSAGE_FORM_DESC] [nvarchar](30) NOT NULL,
	[GenericDrugName] [nvarchar](30) NULL,
	[DosageFormOverride] [nvarchar](50) NULL,
	[MED_REF_DEA_CD] [nvarchar](1) NOT NULL,
	[MED_REF_DEA_CD_DESC] [nvarchar](60) NULL,
	[MED_REF_MULTI_SOURCE_CD] [nvarchar](1) NOT NULL,
	[MED_REF_MULTI_SOURCE_CD_DESC] [nvarchar](90) NULL,
	[MED_REF_GEN_DRUG_NAME_CD] [nvarchar](1) NOT NULL,
	[MED_REF_GEN_DRUG_NAME_CD_DESC] [nvarchar](90) NULL,
	[MED_REF_FED_LEGEND_IND] [nvarchar](1) NOT NULL,
	[MED_REF_FED_LEGEND_IND_DESC] [nvarchar](60) NULL,
	[GENERIC_MEDID] [int] NULL,
	[MED_NAME_TYPE_CD] [nvarchar](1) NOT NULL,
	[GENERIC_MED_REF_GEN_DRUG_NAME_CD] [nvarchar](1) NOT NULL,
	[MED_NAME_SOURCE_CD] [nvarchar](1) NOT NULL,
	[etc] [text] NULL,
	[DrugInfo] [nvarchar](70) NULL,
	[GenericDrugNameOverride] [nvarchar](70) NULL,
	[FormularyDrugID] [int] NULL,
	[Manufacturer] [nvarchar](15) NULL,
	[Status] [nvarchar](1) NULL,
	[TouchDate] [nvarchar](8) NULL,
	[DrugTypeID] [nvarchar](1) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

GO

ALTER VIEW [dbo].[FDBView]
AS
SELECT     TOP 100 PERCENT MEDID AS MED_ID, MED_ROUTED_DF_MED_ID_DESC AS MED_DESC, 
                      MED_STRENGTH + ' ' + MED_STRENGTH_UOM AS MED_STRENGTH_UOM, MED_DOSAGE_FORM_DESC AS MED_DOSAGE, MED_REF_DEA_CD AS MED_REF
FROM         FDB.tblCompositeDrug
WHERE     (Status in ('P', 'A'))
ORDER BY MED_ID
