﻿CREATE TRIGGER PatientDemographicsPatientIdPatientReceivablePaymentsTrigger
ON PatientDemographicsTable
FOR DELETE
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (SELECT COUNT(*)
	 FROM deleted d
	 INNER JOIN PatientReceivablePayments prp WITH(NOLOCK) ON d.PatientId = prp.PayerId AND prp.PayerType = 'P'
	 ) <> 0
BEGIN
	 RAISERROR ('Patients linked to payments cannot be deleted.',16,1)
	 ROLLBACK TRANSACTION
END
GO