﻿/* Task #6690.Added two merge fields SsnIndicator and EinIndicator for CMS1500 document. */

IF EXISTS (
	SELECT * FROM model.Documents 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN
	DECLARE @oldPosition INT;
	DECLARE @NewPostion INT;

	SELECT  @oldPosition=CHARINDEX('BillingProviderTaxIdIndicator',CONVERT(varchar(max),Content)) FROM model.Documents where DisplayName='CMS-1500' AND Content IS NOT NULL

	IF(@oldPosition>0)
	BEGIN
		SELECT @oldPosition=SUBSTRING(CONVERT(varchar(max),Content),CHARINDEX('left:',CONVERT(varchar(max),Content),CHARINDEX('BillingProviderTaxIdIndicator',CONVERT(varchar(max),Content)))+5,4)
		from model.Documents 
		where DisplayName='CMS-1500' AND Content IS NOT NULL

		UPDATE model.Documents
		SET Content = CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), '<input type="text" id="BillingProviderTaxIdIndicator" value="X"', '<input type="text" id="SsnIndicator" value="@Model.SsnIndicator"')) 
		FROM model.Documents 
		WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL

		SET @NewPostion=@oldPosition + 15;

		UPDATE model.documents
		SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), '<input type="text" id="PatientId" value="@Model.PatientId"', '<input type="text" id="EinIndicator" value="@Model.EinIndicator" style="z-index: 1; left: ' + CONVERT(VARCHAR(4),@NewPostion) + 'px; top: @(row18Top)px; position: absolute; width: 15px; right: 768px; height: 14px;"/>' + CHAR(13) + char(9) + char(9)  + ' <input type="text" id="PatientId" value="@Model.PatientId"')) 
		FROM model.Documents
		WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL
	END
END
