﻿ALTER TABLE [dbo].[PatientReceivablePayments] DISABLE TRIGGER ALL
GO

IF EXISTS(SELECT * FROM sys.Views where Name = 'FinancialBatches')
BEGIN 

	WITH CTE AS (
	SELECT 
	PayerId
	,PaymentCheck
	,CONVERT(DECIMAL(18,2),SUM(PaymentAmount)) AS Total
	,prp.PayerType
	,prp.PaymentType
	,prp.PaymentDate AS PaymentDateTime
	,prp.PaymentEOBDate
	,prp.PayerId AS PayorId
	,CASE WHEN prp.PaymentBatchCheckId = 0 THEN NULL ELSE prp.PaymentBatchCheckId END AS PaymentBatchCheckId
	,v.PaymentId
	,v.PaymentDate
	,prp.PaymentId AS RealPaymentIds
	FROM dbo.PatientReceivablePayments prp
	LEFT JOIN (
		SELECT 
		PaymentId
		,PaymentDate
		,PaymentType
		FROM PatientReceivablePayments 
		WHERE PaymentId IN (SELECT Id FROM model.FinancialBatches))
	v ON v.PaymentId = prp.PaymentId
	WHERE PaymentCheck IS NOT NULL AND PaymentCheck <> ''
	AND prp.PaymentType <> '='
	GROUP BY 
	PayerId
	,PaymentCheck
	,PayerType
	,prp.PaymentType
	,prp.PaymentBatchCheckId
	,v.PaymentId
	,v.PaymentDate
	,prp.PaymentDate
	,prp.PaymentEOBDate
	,prp.PayerId
	,prp.PaymentId
	)
	SELECT 
	RealPaymentIds
	,CASE CTE.PayerType
		WHEN 'I'
			THEN 1
		WHEN 'P'
			THEN 2
		WHEN 'O'
			THEN 3
		ELSE 4
	END AS FinancialSourceTypeId 
	,CASE WHEN (CTE.PayerId IS NOT NULL AND CTE.PayerType = 'P') THEN CTE.PayerId ELSE NULL END AS PatientId
	,CASE WHEN (CTE.PayerId IS NOT NULL AND CTE.PayerType = 'I') THEN CTE.PayerId ELSE NULL END AS InsurerId
	,CASE WHEN CTE.PaymentDateTime IS NOT NULL THEN CONVERT(DATETIME,CTE.PaymentDateTime,112) ELSE NULL END AS PaymentDateTime
	,CASE WHEN CTE.PaymentEOBDate = '' THEN NULL ELSE CONVERT(DATETIME,CTE.PaymentEOBDate,112) END AS ExplanationOfBenefitsDateTime
	,CTE.Total AS Amount
	,CONVERT(NVARCHAR(MAX),CTE.PaymentCheck) AS CheckCode
	INTO #ForgottenFinancialBatches
	FROM CTE
	WHERE PaymentBatchCheckId IS NULL

	DECLARE @ExpectedIdentityInsertIdIntoPrp INT
	SET @ExpectedIdentityInsertIdIntoPrp = (SELECT MAX(PaymentId) FROM PatientReceivablePayments)

	SET IDENTITY_INSERT dbo.PatientReceivablePayments ON 

	INSERT INTO dbo.PatientReceivablePayments(PaymentId
		  ,[ReceivableId]
		  ,[PayerId]
		  ,[PayerType]
		  ,[PaymentType]
		  ,[PaymentAmount]
		  ,[PaymentDate]
		  ,[PaymentCheck]
		  ,[PaymentRefId]
		  ,[PaymentRefType]
		  ,[Status]
		  ,[PaymentService]
		  ,[PaymentCommentOn]
		  ,[PaymentFinancialType]
		  ,[PaymentServiceItem]
		  ,[PaymentAssignBy]
		  ,[PaymentReason]
		  ,[PaymentBatchCheckId]
		  ,[PaymentAppealNumber]
		  ,[PaymentEOBDate]
		  ,[PaymentAmountDecimal]
		  ,[ClaimAdjustmentGroupCodeId])
	SELECT
	ROW_NUMBER() OVER (ORDER BY ffb.Amount, ffb.CheckCode)+@ExpectedIdentityInsertIdIntoPrp AS PaymentId
	,0 AS ReceivableId
	,CONVERT(INT,COALESCE(ffb.PatientId, ffb.InsurerId, 0)) AS [PayerId]
	,CASE
		WHEN ffb.PatientId IS NOT NULL
			THEN 'P'
		WHEN ffb.InsurerId IS NOT NULL
			THEN 'I'
		ELSE 'O'
	END AS [PayerType]
	,'=' AS [PaymentType]
	,CONVERT(REAL,ffb.Amount) AS [PaymentAmount]
	,CONVERT(NVARCHAR(8),ffb.PaymentDateTime,112) AS [PaymentDate]
	,CONVERT(NVARCHAR(MAX),ffb.CheckCode) AS [PaymentCheck]
	,CONVERT(NVARCHAR(64),NULL) AS PaymentRefId
	,CONVERT(NVARCHAR(1),NULL) AS [PaymentRefType]
	,'A' AS [Status]
	,'' AS PaymentService
	,'F' AS PaymentCommentOn
	,'' AS PaymentFinancialType
	,0 AS PaymentServiceItem
	,0 AS PaymentAssignBy
	,'' AS PaymentReason
	,0 AS PaymentBatchCheckId
	,'' AS PaymentAppealNumber
	,CONVERT(NVARCHAR(8),ffb.ExplanationOfBenefitsDateTime,112) AS PaymentEOBDate
	,ffb.Amount AS PaymentAmountDecimal
	,NULL AS ClaimAdjustmentGroupCode
	FROM #ForgottenFinancialBatches ffb

	SET IDENTITY_INSERT PatientReceivablePayments OFF

	UPDATE prp
	SET prp.PaymentBatchCheckId = v.PaymentId
	FROM PatientReceivablePayments prp
	JOIN (
		SELECT
		ffb.RealPaymentIds
		,ROW_NUMBER() OVER (ORDER BY ffb.Amount, ffb.CheckCode)+@ExpectedIdentityInsertIdIntoPrp AS PaymentId
		FROM #ForgottenFinancialBatches ffb
	) v ON v.RealPaymentIds = prp.PaymentId


END

ALTER TABLE [dbo].[PatientReceivablePayments] ENABLE TRIGGER ALL
GO