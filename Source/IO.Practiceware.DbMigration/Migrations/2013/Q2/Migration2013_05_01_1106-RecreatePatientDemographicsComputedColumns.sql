﻿--This migration finalizes the patientdemographics table change.
--It calls the ArchiveAndDropColumn stored procedure to remove the existsing
--patientDemographics columns and re-adds the columns using the functions to
--determine them.
IF OBJECT_ID('dbo.[DropColumnAndDefaultConstraints]') IS NULL
BEGIN
	EXEC('
		CREATE PROCEDURE [dbo].[DropColumnAndDefaultConstraints] 
			-- Add the parameters for the stored procedure here
			@tableName nvarchar(max), 
			@columnName nvarchar(max)
		AS
		BEGIN
			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			DECLARE @ConstraintName nvarchar(200)
			SELECT @ConstraintName = Name 
			FROM SYS.DEFAULT_CONSTRAINTS 
			WHERE PARENT_OBJECT_ID = OBJECT_ID(@tableName) 
				AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns WHERE NAME = (@columnName) 
				AND object_id = OBJECT_ID(@tableName))
			IF @ConstraintName IS NOT NULL
				EXEC(''ALTER TABLE '' + @tableName + '' DROP CONSTRAINT '' + @ConstraintName)

			IF EXISTS(SELECT * FROM sys.columns WHERE Name = @columnName  
				AND Object_ID = Object_ID(@tableName))
				EXEC(''ALTER TABLE '' + @tableName + '' DROP COLUMN '' + @columnName)       
		END
	')
END
GO

DECLARE @TableName NVARCHAR(250)
SET @TableName = 'PatientDemographics'

DECLARE @ColumnName1 NVARCHAR(250)
SET @ColumnName1 = 'SecondRelationship'
EXEC dbo.ArchiveAndDropColumn @TableName,@columnName1

DECLARE @ColumnName2 NVARCHAR(250)
SET @ColumnName2 = 'SecondPolicyPatientId'
EXEC dbo.ArchiveAndDropColumn @TableName,@columnName2

DECLARE @ColumnName3 NVARCHAR(250)
SET @ColumnName3 = 'Relationship'
EXEC dbo.ArchiveAndDropColumn @TableName,@columnName3

DECLARE @ColumnName4 NVARCHAR(250)
SET @ColumnName4 = 'PolicyPatientId'
EXEC dbo.ArchiveAndDropColumn @TableName,@columnName4

ALTER TABLE PatientDemographics
DROP COLUMN NEWPolicyHolderPatientId
,NEWSecondPolicyHolderPatientId
,NEWPolicyHolderRelationship
,NEWSecondPolicyHolderRelationship

ALTER TABLE PatientDemographics ADD PolicyPatientId AS (ISNULL(dbo.GetPolicyHolderPatientId(PatientId),PatientId))
ALTER TABLE PatientDemographics ADD Relationship AS (ISNULL(dbo.GetPolicyHolderRelationshipType(PatientId),'Y'))
ALTER TABLE PatientDemographics ADD SecondPolicyPatientId AS (ISNULL(dbo.GetSecondPolicyHolderPatientId(PatientId),0))
ALTER TABLE PatientDemographics ADD SecondRelationship AS (ISNULL(dbo.GetSecondPolicyHolderRelationshipType(PatientId),''))

DROP PROC dbo.ArchiveAndDropColumn