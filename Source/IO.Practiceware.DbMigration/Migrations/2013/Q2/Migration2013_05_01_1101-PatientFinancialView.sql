﻿-- create PatientFinancial view
CREATE VIEW PatientFinancial 
WITH SCHEMABINDING, VIEW_METADATA
AS
SELECT
pi.Id AS FinancialId
,InsuredPatientId AS PatientId
,ip.InsurerId AS FinancialInsurerId
,CONVERT(NVARCHAR(32),PolicyCode) AS FinancialPerson
,CONVERT(NVARCHAR(32),GroupCode) AS FinancialGroupId
,CONVERT(REAL,Copay) AS FinancialCopay
,pi.OrdinalId AS FinancialPIndicator
,CONVERT(NVARCHAR(10),ip.StartDateTime,112) AS FinancialStartDate
,CASE
    WHEN EndDateTime IS NULL
        THEN ''
    ELSE CONVERT(NVARCHAR(10),pi.EndDateTime,112) END AS FinancialEndDate
,CASE 
	WHEN (pi.EndDateTime IS NULL OR pi.EndDateTime >= GETDATE()) AND pi.IsDeleted = 0
		THEN CONVERT(NVARCHAR(1),'C')
	ELSE CONVERT(NVARCHAR(1),'X')
END AS Status
,CASE pi.InsuranceTypeId
	WHEN '2'
		THEN CONVERT(NVARCHAR(1),'V')
	WHEN '3'
		THEN CONVERT(NVARCHAR(1),'A')			
	WHEN '4'
		THEN CONVERT(NVARCHAR(1),'W')
	ELSE CONVERT(NVARCHAR(1),'M')
END AS FinancialInsType
FROM model.PatientInsurances pi
JOIN model.InsurancePolicies ip ON ip.Id=pi.InsurancePolicyId
WHERE PolicyHolderPatientId=InsuredPatientId