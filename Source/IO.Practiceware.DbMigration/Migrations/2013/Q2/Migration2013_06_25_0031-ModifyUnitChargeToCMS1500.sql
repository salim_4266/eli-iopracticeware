﻿IF EXISTS (
	SELECT * FROM model.Documents 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN
	UPDATE model.Documents
	SET Content = CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), '<input type="text" id="ServiceCharge" value="@(service.ServiceCharge.HasValue ? service.ServiceCharge.Value.ToString("F") : "")"', '<input type="text" id="ServiceCharge" value="@(service.ServiceCharge.HasValue ? (string.Format("{0:0.00}",service.ServiceCharge.Value)) : "")"')) 
	FROM model.Documents 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL
END
