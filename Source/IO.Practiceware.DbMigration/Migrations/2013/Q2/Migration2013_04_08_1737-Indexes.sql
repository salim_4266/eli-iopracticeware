﻿IF NOT EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IX_LogEntries_DateTime') 
CREATE INDEX IX_LogEntries_DateTime ON dbo.LogEntries(DateTime) INCLUDE(Message)

IF NOT EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IX_LogEntryProperties_Name') 
CREATE INDEX IX_LogEntryProperties_Name ON dbo.LogEntryProperties(Name) INCLUDE(Value)

IF NOT EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IX_PracticeTransactionJournal_TransactionDate') 
CREATE INDEX IX_PracticeTransactionJournal_TransactionDate ON dbo.PracticeTransactionJournal(TransactionDate) INCLUDE(TransactionRemark)

IF NOT EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IX_LoginUsersAudit_Timestamp') 
CREATE INDEX IX_LoginUsersAudit_Timestamp ON dbo.LoginUsers_Audit(Timestamp) INCLUDE(ActionTaken)