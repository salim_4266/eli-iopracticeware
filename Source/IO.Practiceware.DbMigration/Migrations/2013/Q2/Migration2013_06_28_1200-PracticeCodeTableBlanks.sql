﻿/* Fixes blank codes for TransmitTypes and InsurerPTypes. */

DELETE FROM PracticeCodeTable 
WHERE (ReferenceType = 'TRANSMITTYPE' 
OR ReferenceType = 'INSURERPTYPE')
AND Code = ''

