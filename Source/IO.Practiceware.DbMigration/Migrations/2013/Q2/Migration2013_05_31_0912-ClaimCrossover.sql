﻿-- Delete ClaimCrossOvers view to replace it with table
IF OBJECT_ID('model.ClaimCrossOvers') IS NOT NULL
BEGIN
	DROP VIEW [model].[ClaimCrossOvers]
END
GO

-- Creating table 'ClaimCrossOvers'
CREATE TABLE [model].[ClaimCrossOvers] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [SendingInsurerId] int  NOT NULL,
    [ReceivingInsurerId] int  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'ClaimCrossOvers'
ALTER TABLE [model].[ClaimCrossOvers]
ADD CONSTRAINT [PK_ClaimCrossOvers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [ReceivingInsurerId] in table 'ClaimCrossOvers'
-- TODO!: do it when model.[Insurer] table is added

--ALTER TABLE [model].[ClaimCrossOvers]
--ADD CONSTRAINT [FK_ClaimCrossOverInsurer]
--    FOREIGN KEY ([ReceivingInsurerId])
--    REFERENCES [model].[Insurers]
--        ([Id])
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClaimCrossOverInsurer'
CREATE INDEX [IX_FK_ClaimCrossOverInsurer]
ON [model].[ClaimCrossOvers]
    ([ReceivingInsurerId]);
GO

-- Creating foreign key on [SendingInsurerId] in table 'ClaimCrossOvers'
-- TODO!: do it when model.[Insurer] table is added

--ALTER TABLE [model].[ClaimCrossOvers]
--ADD CONSTRAINT [FK_ClaimCrossOverInsurer1]
--    FOREIGN KEY ([SendingInsurerId])
--    REFERENCES [model].[Insurers]
--        ([Id])
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClaimCrossOverInsurer1'
CREATE INDEX [IX_FK_ClaimCrossOverInsurer1]
ON [model].[ClaimCrossOvers]
    ([SendingInsurerId]);
GO

-- Migrate data from current view to table (if nothing yet in the table)
IF NOT EXISTS (SELECT TOP 1 * FROM model.[ClaimCrossOvers])
BEGIN
	INSERT INTO model.[ClaimCrossOvers] ( ReceivingInsurerId, SendingInsurerId )
	SELECT priReceive.InsurerId AS ReceivingInsurerId,
			priSend.InsurerId AS SendingInsurerId
	FROM dbo.PracticeInsurers priSend
	INNER JOIN dbo.PracticeInsurers priReceive ON priReceive.InsurerReferenceCode NOT IN (
			'[',
			']',
			'm',
			'n'
			)
		AND priReceive.InsurerCrossOver = 'Y'
		AND priSend.InsurerReferenceCode IN (
			'[',
			']',
			'm',
			'n'
			)
		AND priSend.InsurerCrossOver = 'Y'
END
GO

-- Create function for calculated column
IF EXISTS (
    SELECT 
	name 
	FROM sysobjects 
	WHERE xtype IN(N'FN', N'IF', N'TF')
    AND id = object_id(N'GetPracticeInsurersInsurerCrossOver') 
)
DROP FUNCTION dbo.GetPracticeInsurersInsurerCrossOver

EXEC (N'
CREATE FUNCTION dbo.GetPracticeInsurersInsurerCrossOver(@InsurerId INT) 
RETURNS NVARCHAR(1)
WITH SCHEMABINDING
AS
BEGIN

DECLARE @insurerCrossOver NVARCHAR(1) 
SET @insurerCrossOver = (SELECT TOP 1 ''Y'' AS InsurerCrossOver FROM model.ClaimCrossOvers WHERE SendingInsurerId = @InsurerId OR ReceivingInsurerId = @InsurerId)

IF @insurerCrossOver IS NULL
	SET @insurerCrossOver = ''N''

RETURN @insurerCrossOver
END
')
GO

-- First, we need to remove the DEFAULT constraint set for the column
declare @table_name nvarchar(256)
declare @col_name nvarchar(256)
declare @Command  nvarchar(1000)

set @table_name = N'PracticeInsurers'
set @col_name = N'InsurerCrossOver'

select @Command = 'ALTER TABLE ' + @table_name + ' drop constraint ' + d.name
 from sys.tables t   
  join    sys.default_constraints d       
   on d.parent_object_id = t.object_id  
  join    sys.columns c      
   on c.object_id = t.object_id      
    and c.column_id = d.parent_column_id
 where t.name = @table_name
  and c.name = @col_name

--print @Command

execute (@Command)

-- Now, alter the PracticeInsurers table and replace InsurerCrossOver with calculated column
ALTER TABLE dbo.PracticeInsurers DROP COLUMN InsurerCrossOver
GO
ALTER TABLE dbo.PracticeInsurers ADD InsurerCrossOver AS (ISNULL(dbo.GetPracticeInsurersInsurerCrossOver(InsurerId),'N'))
GO