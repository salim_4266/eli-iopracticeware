﻿IF NOT EXISTS(SELECT * FROM sys.columns sc INNER JOIN sys.tables st ON sc.object_id = st.object_id WHERE sc.name = 'KeyNames' AND st.name = 'AuditEntries') 
-- Add KeyNames/KeyValues directly to AuditEntry
ALTER TABLE dbo.AuditEntries ADD KeyNames nvarchar(400) 
GO

IF NOT EXISTS(SELECT * FROM sys.columns sc INNER JOIN sys.tables st ON sc.object_id = st.object_id WHERE sc.name = 'KeyValues' AND st.name = 'AuditEntries') 
BEGIN
	ALTER TABLE dbo.AuditEntries ADD KeyValues nvarchar(400)
	CREATE INDEX IX_AuditEntries_KeyValues ON dbo.AuditEntries(KeyValues)
END
GO


-- Run outside migration as a separate transaction.
--UPDATE dbo.AuditEntries SET 
--KeyNames = STUFF(
--                   (SELECT
--                        ',' + '[' + aek2.KeyName + ']'
--                        FROM dbo.AuditEntryKeyValues aek2
--                        WHERE aek.AuditEntryId = aek2.AuditEntryId
--                        ORDER BY aek2.KeyValue
--                        FOR XML PATH(''), TYPE
--                   ).value('.','varchar(max)')
--                   ,1,1, ''
--              ),
--KeyValues = STUFF(
--                   (SELECT
--                        ',' + '[' + aek2.KeyValue + ']'
--                        FROM dbo.AuditEntryKeyValues aek2
--                        WHERE aek.AuditEntryId = aek2.AuditEntryId
--                        ORDER BY aek2.KeyValue
--                        FOR XML PATH(''), TYPE
--                   ).value('.','varchar(max)')
--                   ,1,1, ''
--              )
--FROM dbo.AuditEntries ae
--JOIN dbo.AuditEntryKeyValues aek on aek.AuditEntryId = ae.Id
--GO

---- Remove old keys table
--DROP TABLE dbo.AuditEntryKeyValues



-- Extra Indexes

IF EXISTS(SELECT * FROM sys.indexes i WHERE i.name = 'IX_AuditEntries_ObjectName')
	DROP INDEX IX_AuditEntries_ObjectName on dbo.AuditEntries

CREATE NONCLUSTERED INDEX [IX_AuditEntries_ObjectName]
ON [dbo].[AuditEntries] ([ObjectName])
INCLUDE ([Id],[ChangeTypeId],[AuditDateTime],[HostName],[ServerName],[UserId],[KeyNames])
GO


-- Add PatientId/AppointmentId columns

IF NOT EXISTS(SELECT * FROM sys.columns sc INNER JOIN sys.tables st ON sc.object_id = st.object_id WHERE sc.name = 'PatientId' AND st.name = 'AuditEntries') 
ALTER TABLE dbo.AuditEntries ADD PatientId bigint

IF NOT EXISTS(SELECT * FROM sys.columns sc INNER JOIN sys.tables st ON sc.object_id = st.object_id WHERE sc.name = 'AppointmentId' AND st.name = 'AuditEntries') 
ALTER TABLE dbo.AuditEntries ADD AppointmentId bigint

IF NOT EXISTS(SELECT * FROM sys.columns sc INNER JOIN sys.tables st ON sc.object_id = st.object_id WHERE sc.name = 'NewValue' AND st.name = 'AuditEntryChanges') 
ALTER TABLE dbo.AuditEntryChanges ADD NewValue nvarchar(max)
GO

--- RestoreAuditEntries Procedure

IF EXISTS (
		SELECT *
		FROM dbo.sysobjects
		WHERE id = OBJECT_ID(N'[RestoreAuditEntries]')
			AND OBJECTPROPERTY(id, N'IsProcedure') = 1
		)
	DROP PROCEDURE [RestoreAuditEntries]
GO

CREATE PROCEDURE RestoreAuditEntries
AS
BEGIN
	DECLARE @EntryId VARCHAR(500)
		,@TableName VARCHAR(500)
		,@ChangeTypeId INT
		,@PrimaryKeyName VARCHAR(500)
		,@PrimaryKeyValue VARCHAR(500)
		,@ColumnName VARCHAR(500)
		,@OldValue NVARCHAR(MAX)
		,@strQuery NVARCHAR(MAX)
		,@strColumnsWithValues NVARCHAR(MAX)
		,@strColumns NVARCHAR(MAX)
		,@strValues NVARCHAR(MAX)
		,@strTempQuery NVARCHAR(MAX)

	IF object_id('tempdb..#AuditEntryIdsToRestore') IS NOT NULL
	BEGIN
		DECLARE RESTORECURSOR CURSOR
		FOR
		SELECT AuditEntryId
		FROM #AuditEntryIdsToRestore

		OPEN RESTORECURSOR

		FETCH
		FROM RESTORECURSOR
		INTO @EntryId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT @EntryId = AE.Id
				,@TableName = AE.ObjectName
				,@ChangeTypeId = AE.ChangeTypeId
                ,@PrimaryKeyName = REPLACE(REPLACE(AE.KeyNames,'[',''),']','')
				,@PrimaryKeyValue = REPLACE(REPLACE(AE.KeyValues,'[',''),']','')
			FROM dbo.AuditEntries AE
			WHERE AE.Id = @EntryId

			SELECT @strQuery = ''

			-- OnUpdate re-applying the values   
			IF @ChangeTypeId = 1
			BEGIN
				DECLARE UPDATECURSOR CURSOR
				FOR
				SELECT ColumnName
					,OldValue
				FROM AuditEntryChanges
				WHERE AuditEntryId = @EntryId

				OPEN UPDATECURSOR

				SELECT @strColumnsWithValues = ''

				FETCH
				FROM UPDATECURSOR
				INTO @ColumnName
					,@OldValue

				WHILE @@FETCH_STATUS = 0
				BEGIN
					IF @strColumnsWithValues = ''
						SELECT @strColumnsWithValues = @ColumnName + ' = ''' + @OldValue + ''''
					ELSE
						SELECT @strColumnsWithValues = @strColumnsWithValues + ',' + @ColumnName + ' = ''' + @OldValue + ''''

					FETCH NEXT
					FROM UPDATECURSOR
					INTO @ColumnName
						,@OldValue
				END

				CLOSE UPDATECURSOR

				DEALLOCATE UPDATECURSOR

				IF @strColumnsWithValues <> ''
				BEGIN
					SELECT @strQuery = 'IF EXISTS (SELECT ' + @PrimaryKeyName + ' FROM ' + @TableName + ' Where ' + @PrimaryKeyName + ' = ''' + @PrimaryKeyValue + ''')'

					SELECT @strQuery = @strQuery + ' UPDATE ' + @TableName + ' SET ' + @strColumnsWithValues + ' Where ' + @PrimaryKeyName + ' = ''' + @PrimaryKeyValue + ''''

					--PRINT (@strQuery)  
					EXECUTE (@strQuery)
				END
			END

			-- OnDelete re-inserting the row  
			IF @ChangeTypeId = 2
			BEGIN
				DECLARE INSERTCURSOR CURSOR
				FOR
				SELECT ColumnName
					,OldValue
				FROM AuditEntryChanges
				WHERE AuditEntryId = @EntryId

				OPEN INSERTCURSOR

				SELECT @strColumns = ''

				SELECT @strValues = ''

				FETCH
				FROM INSERTCURSOR
				INTO @ColumnName
					,@OldValue

				WHILE @@FETCH_STATUS = 0
				BEGIN
					IF @strColumns = ''
						SELECT @strColumns = @ColumnName
					ELSE
						SELECT @strColumns = @strColumns + ',' + @ColumnName

					IF @strValues = ''
						SELECT @strValues = '''' + @OldValue + ''''
					ELSE
						SELECT @strValues = @strValues + ', ''' + @OldValue + ''''

					FETCH NEXT
					FROM INSERTCURSOR
					INTO @ColumnName
						,@OldValue
				END

				CLOSE INSERTCURSOR

				DEALLOCATE INSERTCURSOR

				IF @strColumns <> ''
					AND @strValues <> ''
				BEGIN
					SELECT @strQuery = 'IF NOT EXISTS (SELECT ' + @PrimaryKeyName + ' FROM ' + @TableName + ' Where ' + @PrimaryKeyName + ' = ''' + @PrimaryKeyValue + ''')'

					SELECT @strQuery = @strQuery + ' BEGIN SET identity_insert ' + @TableName + ' ON '

					SELECT @strQuery = @strQuery + ' INSERT INTO ' + @TableName + ' ( ' + @strColumns + ') Values( ' + @strValues + ')'

					SELECT @strQuery = @strQuery + ' SET identity_insert ' + @TableName + ' OFF END'

					--PRINT (@strQuery)  
					EXECUTE (@strQuery)
				END
			END

			FETCH NEXT
			FROM RESTORECURSOR
			INTO @EntryId
		END

		CLOSE RESTORECURSOR

		DEALLOCATE RESTORECURSOR

		
	END
END