﻿IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[OnPatientDemographicsInsert]'))
DROP TRIGGER [dbo].[OnPatientDemographicsInsert]
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[OnPatientDemographicsUpdate]'))
DROP TRIGGER [dbo].[OnPatientDemographicsUpdate]
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[OnPatientDemographicsDelete]'))
DROP TRIGGER [dbo].[OnPatientDemographicsDelete]
GO

EXEC('
CREATE TRIGGER OnPatientDemographicsInsert
ON PatientDemographics INSTEAD OF INSERT
AS
BEGIN


CREATE TABLE #insertedPatientDemographics(
	[PatientId] [int],
	[LastName] [nvarchar](35) NULL,
	[FirstName] [nvarchar](35) NULL,
	[MiddleInitial] [nvarchar](35) NULL,
	[NameReference] [nvarchar](10) NULL,
	[SocialSecurity] [nvarchar](16) NULL,
	[Address] [nvarchar](35) NULL,
	[Suite] [nvarchar](35) NULL,
	[City] [nvarchar](35) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [nvarchar](10) NULL,
	[HomePhone] [nvarchar](25) NULL,
	[CellPhone] [nvarchar](25) NULL,
	[Email] [nvarchar](80) NULL,
	[EmergencyName] [nvarchar](70) NULL,
	[EmergencyPhone] [nvarchar](16) NULL,
	[EmergencyRel] [nvarchar](1) NULL,
	[Occupation] [nvarchar](32) NULL,
	[BusinessName] [nvarchar](30) NULL,
	[BusinessAddress] [nvarchar](35) NULL,
	[BusinessSuite] [nvarchar](35) NULL,
	[BusinessCity] [nvarchar](35) NULL,
	[BusinessState] [nvarchar](2) NULL,
	[BusinessZip] [nvarchar](10) NULL,
	[BusinessPhone] [nvarchar](25) NULL,
	[BusinessType] [nvarchar](1) NULL,
	[Gender] [nvarchar](1) NULL,
	[Marital] [nvarchar](1) NULL,
	[BirthDate] [nvarchar](10) NULL,
	[NationalOrigin] [nvarchar](1) NULL,
	[Language] [nvarchar](10) NULL,
	[SchedulePrimaryDoctor] [int] NULL,
	[PrimaryCarePhysician] [int] NULL,
	[ReferringPhysician] [int] NULL,
	[ProfileComment1] [nvarchar](255) NULL,
	[ProfileComment2] [nvarchar](255) NULL,
	[ContactType] [nvarchar](1) NULL,
	[BillParty] [nvarchar](1) NULL,
	[SecondBillParty] [nvarchar](1) NULL,
	[ReferralCatagory] [nvarchar](32) NULL,
	[BulkMailing] [nvarchar](1) NULL,
	[FinancialAssignment] [nvarchar](1) NULL,
	[FinancialSignature] [nvarchar](1) NULL,
	[FinancialSignatureDate] [nvarchar](10) NULL,
	[Status] [nvarchar](1) NULL,
	[Salutation] [nvarchar](10) NULL,
	[Hipaa] [nvarchar](1) NULL,
	[PatType] [nvarchar](5) NULL,
	[BillingOffice] [int] NULL,
	[SendStatements] [nvarchar](1) NULL,
	[SendConsults] [nvarchar](1) NULL,
	[OldPatient] [nvarchar](16) NULL,
	[ReferralRequired] [nvarchar](1) NULL,
	[MedicareSecondary] [nvarchar](2) NULL,
	[PostOpExpireDate] [int] NULL,
	[PostOpService] [int] NULL,
	[EmployerPhone] [nvarchar](16) NULL,
	[Religion] [nvarchar](64) NULL,
	[Race] [nvarchar](64) NULL,
	[Ethnicity] [nvarchar](64) NULL,
	SecondRelationshipArchived nvarchar(2),
	SecondPolicyPatientIdArchived int,
	RelationshipArchived nvarchar(2),
	PolicyPatientIdArchived int,
	PolicyPatientId int,
	Relationship nvarchar(2),
	SecondPolicyPatientId int,
	SecondRelationship nvarchar(2)
	)


INSERT INTO [dbo].[PatientDemographicsTable]
           ([LastName]
           ,[FirstName]
           ,[MiddleInitial]
           ,[NameReference]
           ,[SocialSecurity]
           ,[Address]
           ,[Suite]
           ,[City]
           ,[State]
           ,[Zip]
           ,[HomePhone]
           ,[CellPhone]
           ,[Email]
           ,[EmergencyName]
           ,[EmergencyPhone]
           ,[EmergencyRel]
           ,[Occupation]
           ,[BusinessName]
           ,[BusinessAddress]
           ,[BusinessSuite]
           ,[BusinessCity]
           ,[BusinessState]
           ,[BusinessZip]
           ,[BusinessPhone]
           ,[BusinessType]
           ,[Gender]
           ,[Marital]
           ,[BirthDate]
           ,[NationalOrigin]
           ,[Language]
           ,[SchedulePrimaryDoctor]
           ,[PrimaryCarePhysician]
           ,[ReferringPhysician]
           ,[ProfileComment1]
           ,[ProfileComment2]
           ,[ContactType]
           ,[BillParty]
           ,[SecondBillParty]
           ,[ReferralCatagory]
           ,[BulkMailing]
           ,[FinancialAssignment]
           ,[FinancialSignature]
           ,[FinancialSignatureDate]
           ,[Status]
           ,[Salutation]
           ,[Hipaa]
           ,[PatType]
           ,[BillingOffice]
           ,[SendStatements]
           ,[SendConsults]
           ,[OldPatient]
           ,[ReferralRequired]
           ,[MedicareSecondary]
           ,[PostOpExpireDate]
           ,[PostOpService]
           ,[EmployerPhone]
           ,[Religion]
           ,[Race]
           ,[Ethnicity])
OUTPUT inserted.PatientId
		   ,inserted.[LastName]
           ,inserted.[FirstName]
           ,inserted.[MiddleInitial]
           ,inserted.[NameReference]
           ,inserted.[SocialSecurity]
           ,inserted.[Address]
           ,inserted.[Suite]
           ,inserted.[City]
           ,inserted.[State]
           ,inserted.[Zip]
           ,inserted.[HomePhone]
           ,inserted.[CellPhone]
           ,inserted.[Email]
           ,inserted.[EmergencyName]
           ,inserted.[EmergencyPhone]
           ,inserted.[EmergencyRel]
           ,inserted.[Occupation]
           ,inserted.[BusinessName]
           ,inserted.[BusinessAddress]
           ,inserted.[BusinessSuite]
           ,inserted.[BusinessCity]
           ,inserted.[BusinessState]
           ,inserted.[BusinessZip]
           ,inserted.[BusinessPhone]
           ,inserted.[BusinessType]
           ,inserted.[Gender]
           ,inserted.[Marital]
           ,inserted.[BirthDate]
           ,inserted.[NationalOrigin]
           ,inserted.[Language]
           ,inserted.[SchedulePrimaryDoctor]
           ,inserted.[PrimaryCarePhysician]
           ,inserted.[ReferringPhysician]
           ,inserted.[ProfileComment1]
           ,inserted.[ProfileComment2]
           ,inserted.[ContactType]
           ,inserted.[BillParty]
           ,inserted.[SecondBillParty]
           ,inserted.[ReferralCatagory]
           ,inserted.[BulkMailing]
           ,inserted.[FinancialAssignment]
           ,inserted.[FinancialSignature]
           ,inserted.[FinancialSignatureDate]
           ,inserted.[Status]
           ,inserted.[Salutation]
           ,inserted.[Hipaa]
           ,inserted.[PatType]
           ,inserted.[BillingOffice]
           ,inserted.[SendStatements]
           ,inserted.[SendConsults]
           ,inserted.[OldPatient]
           ,inserted.[ReferralRequired]
           ,inserted.[MedicareSecondary]
           ,inserted.[PostOpExpireDate]
           ,inserted.[PostOpService]
           ,inserted.[EmployerPhone]
           ,inserted.[Religion]
           ,inserted.[Race]
           ,inserted.[Ethnicity]
		   ,'''',0,'''',0
		   ,0,'''',0,''''
INTO #insertedPatientDemographics
SELECT [LastName]
           ,[FirstName]
           ,[MiddleInitial]
           ,[NameReference]
           ,[SocialSecurity]
           ,[Address]
           ,[Suite]
           ,[City]
           ,[State]
           ,[Zip]
           ,[HomePhone]
           ,[CellPhone]
           ,[Email]
           ,[EmergencyName]
           ,[EmergencyPhone]
           ,[EmergencyRel]
           ,[Occupation]
           ,[BusinessName]
           ,[BusinessAddress]
           ,[BusinessSuite]
           ,[BusinessCity]
           ,[BusinessState]
           ,[BusinessZip]
           ,[BusinessPhone]
           ,[BusinessType]
           ,[Gender]
           ,[Marital]
           ,[BirthDate]
           ,[NationalOrigin]
           ,[Language]
           ,[SchedulePrimaryDoctor]
           ,[PrimaryCarePhysician]
           ,[ReferringPhysician]
           ,[ProfileComment1]
           ,[ProfileComment2]
           ,[ContactType]
           ,[BillParty]
           ,[SecondBillParty]
           ,[ReferralCatagory]
           ,[BulkMailing]
           ,[FinancialAssignment]
           ,[FinancialSignature]
           ,[FinancialSignatureDate]
           ,[Status]
           ,[Salutation]
           ,[Hipaa]
           ,[PatType]
           ,[BillingOffice]
           ,[SendStatements]
           ,[SendConsults]
           ,[OldPatient]
           ,[ReferralRequired]
           ,[MedicareSecondary]
           ,[PostOpExpireDate]
           ,[PostOpService]
           ,[EmployerPhone]
           ,[Religion]
           ,[Race]
           ,[Ethnicity]
FROM inserted     

SELECT SCOPE_IDENTITY() AS SCOPE_ID_COLUMN

END
')

EXEC('
CREATE TRIGGER OnPatientDemographicsUpdate
ON PatientDemographics INSTEAD OF UPDATE
AS
BEGIN

SELECT * INTO #insertedPatientDemographics FROM inserted

UPDATE PatientDemographics
	SET LastName = i.LastName
      ,FirstName = i.FirstName
      ,MiddleInitial = i.MiddleInitial
      ,NameReference = i.NameReference
      ,SocialSecurity = i.SocialSecurity
      ,Address = i.Address
      ,Suite = i.Suite
      ,City = i.City
      ,State = i.State
      ,Zip = i.Zip
      ,HomePhone = i.HomePhone
      ,CellPhone = i.CellPhone
      ,Email = i.Email
      ,EmergencyName = i.EmergencyName
      ,EmergencyPhone = i.EmergencyPhone
      ,EmergencyRel = i.EmergencyRel
      ,Occupation = i.Occupation
      ,BusinessName = i.BusinessName
      ,BusinessAddress = i.BusinessAddress
      ,BusinessSuite = i.BusinessSuite
      ,BusinessCity = i.BusinessCity
      ,BusinessState = i.BusinessState
      ,BusinessZip = i.BusinessZip
      ,BusinessPhone = i.BusinessPhone
      ,BusinessType = i.BusinessType
      ,Gender = i.Gender
      ,Marital = i.Marital
      ,BirthDate = i.BirthDate
      ,NationalOrigin = i.NationalOrigin
      ,Language = i.Language
      ,SchedulePrimaryDoctor = i.SchedulePrimaryDoctor
      ,PrimaryCarePhysician = i.PrimaryCarePhysician
      ,ReferringPhysician = i.ReferringPhysician
      ,ProfileComment1 = i.ProfileComment1
      ,ProfileComment2 = i.ProfileComment2
      ,ContactType = i.ContactType
      ,BillParty = i.BillParty
      ,SecondBillParty = i.SecondBillParty
      ,ReferralCatagory = i.ReferralCatagory
      ,BulkMailing = i.BulkMailing
      ,FinancialAssignment = i.FinancialAssignment
      ,FinancialSignature = i.FinancialSignature
      ,FinancialSignatureDate = i.FinancialSignatureDate
      ,Status = i.Status
      ,Salutation = i.Salutation
      ,Hipaa = i.Hipaa
      ,PatType = i.PatType
      ,BillingOffice = i.BillingOffice
      ,SendStatements = i.SendStatements
      ,SendConsults = i.SendConsults
      ,OldPatient = i.OldPatient
      ,ReferralRequired = i.ReferralRequired
      ,MedicareSecondary = i.MedicareSecondary
      ,PostOpExpireDate = i.PostOpExpireDate
      ,PostOpService = i.PostOpService
      ,EmployerPhone = i.EmployerPhone
      ,Religion = i.Religion
      ,Race = i.Race
      ,Ethnicity = i.Ethnicity
	  FROM dbo.PatientDemographics pd
	  JOIN Inserted i ON i.PatientId = pd.PatientId

END
')

EXEC('
CREATE TRIGGER OnPatientDemographicsDelete
ON PatientDemographics INSTEAD OF DELETE
AS
BEGIN
	DELETE PatientDemographicsTable WHERE PatientId IN (SELECT PatientId FROM deleted)
END
')