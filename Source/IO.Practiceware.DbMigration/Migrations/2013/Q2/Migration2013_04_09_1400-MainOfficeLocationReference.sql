﻿/* Prevents users from adding a location reference to the main office
if the location reference is blank. */

IF OBJECT_ID('PracticeNameMainOfficeLocationReferenceTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeNameMainOfficeLocationReferenceTrigger')
END

GO

CREATE TRIGGER PracticeNameMainOfficeLocationReferenceTrigger
ON PracticeName
FOR INSERT, UPDATE
NOT FOR REPLICATION
AS
SET NOCOUNT ON

IF EXISTS (SELECT * FROM PracticeName pn
			INNER JOIN inserted i ON pn.PracticeId = i.PracticeId
			INNER JOIN deleted d ON pn.PracticeId = d.PracticeId
			WHERE pn.PracticeType = 'P' AND d.LocationReference = ''
			AND i.PracticeType = 'P' AND i.LocationReference <> '')
		AND dbo.GetNoCheck() = 0

BEGIN
	RAISERROR('The main office must have a blank location reference. ',16,1)
	ROLLBACK TRANSACTION
END
GO