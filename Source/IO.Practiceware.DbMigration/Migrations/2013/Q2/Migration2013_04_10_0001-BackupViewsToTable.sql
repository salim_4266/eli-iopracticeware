﻿------1st Migration to be run THIS MUST RUN BEFORE THE MIGRATION THAT DROPS VIEWS
--Create backup tables

IF OBJECT_ID(N'dbo.InvoiceReceivablesBackup','U') IS NULL
BEGIN
	CREATE TABLE dbo.[InvoiceReceivablesBackup] (
		[Id] bigint NOT NULL,
		[InvoiceId] int  NOT NULL,
		[OrdinalId] int  NOT NULL,
		[PatientInsuranceId] bigint  NULL,
		[PayerClaimControlNumber] nvarchar(max)  NULL,
		[PatientInsuranceAuthorizationId] int  NULL,
		[PatientInsuranceReferralId] bigint  NULL
	)
END
GO

IF OBJECT_ID(N'dbo.InvoicesBackup','U') IS NULL
BEGIN
	CREATE TABLE dbo.[InvoicesBackup] (
		[Id] int NOT NULL,
		[DateTime] datetime  NOT NULL,
		[AttributeToServiceLocationId] int  NULL,
		[EncounterId] int  NOT NULL,
		[InvoiceTypeId] int  NOT NULL,
		[ClinicalInvoiceProviderId] bigint  NOT NULL,
		[HasPatientAssignedBenefits] bit  NOT NULL,
		[IsReleaseOfInformationNotSigned] bit  NOT NULL,
		[IsNoProviderSignatureOnFile] bit  NOT NULL,
		[BillingProviderId] bigint  NOT NULL,
		[ReferringExternalProviderId] int  NULL,
		[ServiceLocationCodeId] int  NOT NULL,
		[IsAssignmentRefused] bit  NOT NULL
	)
END
GO

IF OBJECT_ID(N'dbo.InvoiceSupplementalsBackup','U') IS NULL
BEGIN
CREATE TABLE dbo.[InvoiceSupplementalsBackup] (
	[Id] int NOT NULL,
	[AccidentDateTime] datetime  NULL,
	[VisionPrescriptionDateTime] datetime  NULL,
	[DisabilityDateTime] datetime  NULL,
	[AdmissionDateTime] datetime  NULL,
	[DischargeDateTime] datetime  NULL,
	[ClaimDelayReasonId] int  NULL,
	[RelatedCause1Id] int  NULL,
	[AccidentStateOrProvinceId] int  NULL
)
END
GO

IF OBJECT_ID(N'dbo.AdjustmentsBackup','U') IS NULL
BEGIN
CREATE TABLE dbo.[AdjustmentsBackup] (
	[Id] int NOT NULL,
	[FinancialSourceTypeId] int  NOT NULL,
	[Amount] decimal(18,2)  NOT NULL,
	[PostedDateTime] datetime  NOT NULL,
	[AdjustmentTypeId] int  NOT NULL,
	[BillingServiceId] int  NULL,
	[InvoiceReceivableId] bigint  NULL,
	[PatientId] int  NULL,
	[InsurerId] int  NULL,
	[FinancialBatchId] int  NULL,
	[PaymentMethodId] int  NULL,
	[ClaimAdjustmentGroupCodeId] int  NULL,
	[ClaimAdjustmentReasonCodeId] int  NULL,
	[PaymentCommentOn] nvarchar(1) NULL
)
END
GO

IF OBJECT_ID(N'dbo.FinancialInformationsBackup','U') IS NULL
BEGIN
CREATE TABLE dbo.[FinancialInformationsBackup] (
	[Id] int NOT NULL,
	[Amount] decimal(18,2)  NOT NULL,
	[PostedDateTime] datetime  NOT NULL,
	[FinancialInformationTypeId] int  NOT NULL,
	[BillingServiceId] int  NULL,
	[InvoiceReceivableId] bigint  NULL,
	[FinancialSourceTypeId] int  NOT NULL,
	[PatientId] int  NULL,
	[InsurerId] int  NULL,
	[FinancialBatchId] int  NULL,
	[ClaimAdjustmentGroupCodeId] int  NULL,
	[ClaimAdjustmentReasonCodeId] int  NULL,
	[PaymentCommentOn] nvarchar(1) NULL
)
END
GO

IF OBJECT_ID(N'dbo.FinancialBatchesBackUp','U') IS NULL
BEGIN
CREATE TABLE [dbo].[FinancialBatchesBackUp] (
	[Id] int NOT NULL,
	[FinancialSourceTypeId] int  NOT NULL,
	[PatientId] int NULL,
	[InsurerId] int NULL,
	[PaymentDateTime] datetime NULL,
	[ExplanationOfBenefitsDateTime] datetime  NULL,
	[Amount] decimal(18,2)  NOT NULL,
	[CheckCode] nvarchar(max)  NOT NULL,
	[ComputedFinancialBatchId] int NULL
)
END
GO

IF OBJECT_ID(N'dbo.BillingServiceTransactionsBackup','U') IS NULL
BEGIN
CREATE TABLE dbo.[BillingServiceTransactionsBackup] (
	[Id] uniqueidentifier  NOT NULL,
	[DateTime] datetime  NOT NULL,
	[AmountSent] decimal(18,2)  NOT NULL,
	[ClaimFileNumber] int  NULL,
	[BillingServiceTransactionStatusId] int  NOT NULL,
	[MethodSentId] int  NOT NULL,
	[BillingServiceId] int  NOT NULL,
	[InvoiceReceivableId] bigint  NOT NULL,
	[ClaimFileReceiverId] int  NULL,
	[ExternalSystemMessageId] uniqueidentifier  NULL,
	[PatientAggregateStatementId] int  NULL,
	[TransactionId] int NULL
)
END
GO

IF OBJECT_ID(N'dbo.PatientReceivablesBackup', 'U') IS NULL
SELECT 
* 
INTO dbo.PatientReceivablesBackup
FROM PatientReceivables
GO

IF OBJECT_ID(N'dbo.PatientReceivablePaymentsBackup', 'U') IS NULL
SELECT 
* 
INTO dbo.PatientReceivablePaymentsBackup
FROM PatientReceivablePayments
GO

IF OBJECT_ID(N'dbo.ServiceTransactionsBackup', 'U') IS NULL
SELECT 
* 
INTO dbo.ServiceTransactionsBackup
FROM ServiceTransactions
GO

--Check to see if model views exist and if they do insert data into them
IF OBJECT_ID(N'model.InvoiceReceivables','V') IS NOT NULL
BEGIN
EXEC('
	INSERT INTO dbo.[InvoiceReceivablesBackup]  (Id, InvoiceId, OrdinalId, PatientInsuranceId, PayerClaimControlNumber, PatientInsuranceAuthorizationId, PatientInsuranceReferralId)
	SELECT Id, 
		InvoiceId, 
		OrdinalId, 
		PatientInsuranceId, 
		PayerClaimControlNumber, 
		PatientInsuranceAuthorizationId, 
		PatientInsuranceReferralId  
	FROM model.InvoiceReceivables
')
END
GO

IF OBJECT_ID(N'model.Invoices','V') IS NOT NULL
BEGIN
EXEC('INSERT INTO dbo.[InvoicesBackup] (Id, DateTime, AttributeToServiceLocationId, EncounterId, InvoiceTypeId, ClinicalInvoiceProviderId, HasPatientAssignedBenefits, IsReleaseOfInformationNotSigned, IsNoProviderSignatureOnFile, BillingProviderId, ReferringExternalProviderId, ServiceLocationCodeId, IsAssignmentRefused)
SELECT Id, 
	DateTime, 
	AttributeToServiceLocationId, 
	EncounterId, 
	InvoiceTypeId, 
	ClinicalInvoiceProviderId, 
	HasPatientAssignedBenefits, 
	IsReleaseOfInformationNotSigned, 
	IsNoProviderSignatureOnFile, 
	BillingProviderId, 
	ReferringExternalProviderId, 
	ServiceLocationCodeId, 
	DoesProviderRefuseAssignment
FROM model.Invoices
')
END
GO

IF OBJECT_ID(N'model.InvoiceSupplementalsBackup','V') IS NOT NULL
BEGIN
EXEC('
INSERT INTO dbo.InvoiceSupplementalsBackup (Id, AccidentDateTime, VisionPrescriptionDateTime, DisabilityDateTime, AdmissionDateTime, DischargeDateTime, ClaimDelayReasonId, RelatedCause1Id, AccidentStateOrProvinceId)
SELECT Id,
	AccidentDateTime, 
	VisionPrescriptionDateTime,
	DisabilityDateTime,
	AdmissionDateTime,
	DischargeDateTime,
	ClaimDelayReasonId,
	RelatedCause1Id,
	AccidentStateOrProvinceId
FROM model.InvoiceSupplementals
')
END
GO

-- Adjustments
IF OBJECT_ID(N'model.Adjustments','V') IS NOT NULL
BEGIN
	EXEC('
	ALTER VIEW model.Adjustments 
	AS
	SELECT v.Id AS Id
		, v.AdjustmentTypeId AS AdjustmentTypeId
		, v.Amount AS Amount
		, v.FinancialBatchId AS FinancialBatchId
		, InsurerId
		,CASE 
			WHEN pd.PatientId IS NULL 
				THEN NULL 
			ELSE v.PatientId 
			END AS PatientId
		,CASE 
			WHEN v.BillingServiceId  = 0
				THEN NULL
			WHEN prs.[Status] = ''X''
				THEN NULL
			ELSE 
				v.BillingServiceId
			END AS BillingServiceId
		,v.FinancialSourceTypeId AS FinancialSourceTypeId
		,v.InvoiceReceivableId AS InvoiceReceivableId
		,v.PaymentMethodId AS PaymentMethodId
		,COALESCE(v.PostedDateTime, CONVERT(datetime, ''20000101'')) AS PostedDateTime
		,CONVERT(int, v.ClaimAdjustmentReasonCodeId) AS ClaimAdjustmentReasonCodeId
		,CONVERT(int, v.ClaimAdjustmentGroupCodeId) AS ClaimAdjustmentGroupCodeId
	FROM (
		SELECT prp.PaymentId AS Id
		,CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
				WHEN  ''P''
					THEN 1
				WHEN ''X''
					THEN 2
				WHEN ''8''
					THEN 3
				WHEN ''R''
					THEN 4
				WHEN ''U''
					THEN 5
				ELSE pct.Id + 1000
		END AS AdjustmentTypeId
		,CASE
			WHEN prp.PaymentReason IS NULL 
				THEN 
					CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
						WHEN ''X'' -- AdjustmentTypeId = 2
							THEN paymentReason45.Id -- ReasonCode 45
						WHEN ''8'' -- AdjustmentTypeId = 3
							THEN paymentReason45.Id -- ReasonCode 45
						ELSE 
							CAST(NULL AS int)
					END
			WHEN prp.PaymentReason = ''''
				THEN 
					CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
						WHEN ''X'' -- AdjustmentTypeId = 2
							THEN paymentReason45.Id -- ReasonCode 45
						WHEN ''8'' -- AdjustmentTypeId = 3
							THEN paymentReason45.Id -- ReasonCode 45
						ELSE 
							CAST(NULL AS int)
					END
			ELSE
				existingPaymentReason.Id --(SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = ''PAYMENTREASON'' AND Code = prp.PaymentReason)
			END AS ClaimAdjustmentReasonCodeId
		, CASE
			WHEN prp.ClaimAdjustmentGroupCodeId IS NULL 
				THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
						-- These int values correspond to the ClaimAdjustmentGroupCode enum in .NET

						WHEN ''X'' -- AdjustmentTypeId = 2
							THEN 1 -- ContractualObligation
						WHEN ''8'' -- AdjustmentTypeId = 3
							THEN 3 -- OtherAdjustment
						WHEN ''R'' -- AdjustmentTypeId = 4
							THEN 4 -- CorrectionReversal
						WHEN ''U'' -- AdjustmentTypeId = 5
							THEN 4 -- CorrectionReversal
						ELSE 
							CAST(NULL AS int)
						END
			ELSE
				prp.ClaimAdjustmentGroupCodeId
			END AS ClaimAdjustmentGroupCodeId
		,prp.PaymentAmountDecimal AS Amount
		,CASE WHEN PaymentBatchCheckId IN (0, -1) THEN NULL ELSE PaymentBatchCheckId END AS FinancialBatchId
		,CASE 
				WHEN prp.PayerType = ''I'' AND PayerId > 0
					THEN prp.PayerId
				ELSE NULL
			END AS InsurerId
			,CASE 
				WHEN prp.PayerType = ''P'' 
					THEN prp.PayerId 
				WHEN prp.PayerType = ''I'' AND pr.InsurerId = 99999 
					THEN pr.PatientId
				ELSE NULL 
				END AS PatientId
			,prp.PaymentServiceItem AS BillingServiceId
			,prp.PayerId AS PayerId
		,CASE prp.PayerType
				WHEN ''I''
					THEN 1
				WHEN ''P''
					THEN 2
				WHEN ''O''
					THEN 3
				ELSE 4
				END AS FinancialSourceTypeId
		,model.GetBigPairId((CASE 
									WHEN (ComputedFinancialId IS NULL OR ComputedFinancialId = 0 OR PayerType = ''P'') AND pr.InsurerId <> 99999
									THEN 0
								ELSE ComputedFinancialId 
								END), pr.AppointmentId, 110000000) AS InvoiceReceivableId
		,pctPaymentMethod.Id AS PaymentMethodId
		,CONVERT(datetime, prp.PaymentDate, 112) AS PostedDateTime
		FROM dbo.PatientReceivablePayments prp
		INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
		INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''PAYMENTTYPE'' AND prp.PaymentType = SUBSTRING(pct.Code, LEN(pct.Code), 1)
		LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason45 ON paymentReason45.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = ''45'')
		LEFT OUTER JOIN dbo.PracticeCodeTable existingPaymentReason ON existingPaymentReason.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = prp.PaymentReason)
		LEFT JOIN dbo.PracticeCodeTable pctPaymentMethod ON pctPaymentMethod.ReferenceType = ''PAYABLETYPE'' and prp.PaymentRefType = SUBSTRING(pctPaymentMethod.Code, LEN(pctPaymentMethod.Code), 1)
		WHERE prp.PaymentFinancialType <> ''''
				AND ((prp.PayerType = ''I'' AND pr.ComputedFinancialId IS NOT NULL)
					OR
					prp.PayerType <> ''I''
					OR 
					(prp.PayerType = ''I'' AND pr.InsurerId = 99999 AND pr.ComputedFinancialId IS NULL))

		UNION ALL

		SELECT
		prp.PaymentId AS Id
		,CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
			WHEN  ''P''
				THEN 1
			WHEN ''X''
				THEN 2
			WHEN ''8''
				THEN 3
			WHEN ''R''
				THEN 4
			WHEN ''U''
				THEN 5
			ELSE pct.Id + 1000
		END AS AdjustmentTypeId
		,CASE
			WHEN prp.PaymentReason IS NULL 
				THEN 
					CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
						WHEN ''X'' -- AdjustmentTypeId = 2
							THEN paymentReason45.Id -- ReasonCode 45
						WHEN ''8'' -- AdjustmentTypeId = 3
							THEN paymentReason45.Id -- ReasonCode 45
						ELSE 
							CAST(NULL AS int)
					END
			WHEN prp.PaymentReason = ''''
				THEN 
					CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
						WHEN ''X'' -- AdjustmentTypeId = 2
							THEN paymentReason45.Id -- ReasonCode 45
						WHEN ''8'' -- AdjustmentTypeId = 3
							THEN paymentReason45.Id -- ReasonCode 45
						ELSE 
							CAST(NULL AS int)
					END
			ELSE
				existingPaymentReason.Id --(SELECT TOP 1 Id FROM dbo.PracticeCodeTable WHERE ReferenceType = ''PAYMENTREASON'' AND Code = prp.PaymentReason)
			END AS ClaimAdjustmentReasonCodeId
		, CASE
			WHEN prp.ClaimAdjustmentGroupCodeId IS NULL 
				THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code))
						-- These int values correspond to the ClaimAdjustmentGroupCode enum in .NET

						WHEN ''X'' -- AdjustmentTypeId = 2
							THEN 1 -- ContractualObligation
						WHEN ''8'' -- AdjustmentTypeId = 3
							THEN 3 -- OtherAdjustment
						WHEN ''R'' -- AdjustmentTypeId = 4
							THEN 4 -- CorrectionReversal
						WHEN ''U'' -- AdjustmentTypeId = 5
							THEN 4 -- CorrectionReversal
						ELSE 
							CAST(NULL AS int)
						END
			ELSE
				prp.ClaimAdjustmentGroupCodeId
			END AS ClaimAdjustmentGroupCodeId
		,prp.PaymentAmountDecimal AS Amount
		,CASE WHEN PaymentBatchCheckId IN (0, -1) THEN NULL ELSE PaymentBatchCheckId END AS FinancialBatchId
		,CASE 
				WHEN prp.PayerType = ''I'' AND PayerId > 0
					THEN prp.PayerId
				ELSE NULL
			END AS InsurerId
			,CASE 
				WHEN prp.PayerType = ''P'' 
					THEN prp.PayerId 
				WHEN prp.PayerType = ''I'' AND pr2.InsurerId = 99999 
					THEN pr2.PatientId
				ELSE NULL 
				END AS PatientId
			,prp.PaymentServiceItem AS BillingServiceId
			,prp.PayerId AS PayerId
		,CASE prp.PayerType
				WHEN ''I''
					THEN 1
				WHEN ''P''
					THEN 2
				WHEN ''O''
					THEN 3
				ELSE 4
				END AS FinancialSourceTypeId
		,model.GetBigPairId((CASE 
									WHEN (pr2.ComputedFinancialId IS NULL OR pr2.ComputedFinancialId = 0 OR PayerType = ''P'') AND pr2.InsurerId <> 99999
									THEN 0
								ELSE pr2.ComputedFinancialId 
								END), pr2.AppointmentId, 110000000) AS InvoiceReceivableId
		,pctPaymentMethod.Id AS PaymentMethodId
		,CONVERT(DATETIME, prp.PaymentDate, 112) AS PostedDateTime
		FROM dbo.PatientReceivables pr1
		INNER JOIN dbo.PatientReceivables pr2 ON pr1.Invoice = pr2.Invoice
			AND pr2.ComputedFinancialId IS NOT NULL
		LEFT JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr1.ReceivableId
			AND ptj.TransactionType = ''R''
		LEFT JOIN dbo.PatientReceivablePayments prp ON prp.ReceivableId = pr1.ReceivableId
			AND prp.PaymentFinancialType <> ''''
			AND prp.PayerType = ''I''
		INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''PAYMENTTYPE'' AND prp.PaymentType = SUBSTRING(pct.Code, LEN(pct.Code), 1)
		LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason45 ON paymentReason45.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = ''45'')
		LEFT OUTER JOIN dbo.PracticeCodeTable existingPaymentReason ON existingPaymentReason.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = prp.PaymentReason)
		LEFT JOIN dbo.PracticeCodeTable pctPaymentMethod ON pctPaymentMethod.ReferenceType = ''PAYABLETYPE'' and prp.PaymentRefType = SUBSTRING(pctPaymentMethod.Code, LEN(pctPaymentMethod.Code), 1)
		LEFT JOIN dbo.PatientFinancial pf1 ON pf1.PatientId = pr1.InsuredId
			AND CONVERT(DATETIME,pf1.FinancialStartDate) <= CONVERT(DATETIME,pr1.InvoiceDate)
			AND (pf1.FinancialEndDate IS NULL OR pf1.FinancialEndDate = '''' OR (CONVERT(DATETIME,pf1.FinancialEndDate) >= CONVERT(DATETIME,pr1.InvoiceDate)))
		LEFT JOIN dbo.PatientFinancial pf2 ON pf2.PatientId = pr2.InsuredId
			AND CONVERT(DATETIME,pf2.FinancialStartDate) <= CONVERT(DATETIME,pr2.InvoiceDate)
			AND (pf2.FinancialEndDate IS NULL OR pf2.FinancialEndDate = '''' OR (CONVERT(DATETIME,pf2.FinancialEndDate) >= CONVERT(DATETIME,pr2.InvoiceDate)))
		WHERE pr1.ComputedFinancialId IS NULL
			--null row has never been billed
			AND ptj.TransactionId IS NULL
			--explains the PatientId is the InsuredId
			AND pr1.PatientId = pr1.InsuredId
			--null row has no insurance
			AND pf1.FinancialId IS NULL
			--but PatientReceivables thinks it does
			AND pr1.InsurerId IS NOT NULL
			AND pr1.InsurerId <> 99999
			AND pr2.InsurerId <> 99999
			AND prp.PaymentId IS NOT NULL
			AND prp.PayerId = pf2.FinancialInsurerId
		) v
	LEFT JOIN dbo.PatientReceivableServices prs ON prs.ItemId = v.BillingServiceId
	LEFT JOIN dbo.PatientDemographics pd ON pd.PatientId = v.PayerId
	')
END
GO

IF OBJECT_ID(N'model.Adjustments','V') IS NOT NULL
BEGIN
EXEC('
INSERT INTO dbo.AdjustmentsBackup (Id,FinancialSourceTypeId,Amount, PostedDateTime, AdjustmentTypeId,BillingServiceId,InvoiceReceivableId,PatientId, InsurerId, FinancialBatchId, PaymentMethodId, ClaimAdjustmentGroupCodeId, ClaimAdjustmentReasonCodeId, PaymentCommentOn)
SELECT adj.Id,
	adj.FinancialSourceTypeId,
	adj.Amount, 
	adj.PostedDateTime, 
	adj.AdjustmentTypeId,
	adj.BillingServiceId,
	adj.InvoiceReceivableId,
	adj.PatientId, 
	adj.InsurerId, 
	adj.FinancialBatchId, 
	adj.PaymentMethodId, 
	adj.ClaimAdjustmentGroupCodeId, 
	adj.ClaimAdjustmentReasonCodeId,
	prp.PaymentCommentOn
FROM model.Adjustments adj
JOIN PatientReceivablePayments prp ON prp.PaymentId = adj.Id
')
END
GO

---FinancialInformations
IF OBJECT_ID(N'model.FinancialInformations','V') IS NOT NULL
BEGIN
EXEC('
DECLARE @InvoiceReceivableMax int
SET @InvoiceReceivableMax = 110000000

SELECT * INTO #tempFinancialInformations FROM (
	SELECT prp.PaymentId AS Id,
		prp.PaymentAmountDecimal AS Amount,
		prp.PaymentServiceItem AS BillingServiceId,
		CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code)) 
			WHEN ''!''
				THEN 1
			WHEN ''|''
				THEN 2
			WHEN ''?''
				THEN 3
			WHEN ''%''
				THEN 4
			WHEN ''D''
				THEN 5
			WHEN ''&''
				THEN 6
			WHEN '']''
				THEN 7
			WHEN ''0''
				THEN 8
			WHEN ''V''
				THEN 9
			ELSE pct.Id + 1000 
			END AS FinancialInformationTypeId,
		CASE prp.PayerType
			WHEN ''I''
				THEN 1
			WHEN ''P''
				THEN 2
			WHEN ''O''
				THEN 3
			ELSE 4
			END AS FinancialSourceTypeId,
		CASE prp.PayerType
			WHEN ''I''
				THEN Prp.PayerId
			ELSE NULL
			END AS InsurerId,
		model.GetBigPairId((CASE 
							WHEN pf.FinancialId IS NULL OR pf.FinancialId = 0
								THEN 0
							ELSE pf.FinancialId
							END), pr.AppointmentId, @InvoiceReceivableMax) AS InvoiceReceivableId,
		CASE prp.PayerType
			WHEN ''P''
				THEN prp.PayerId
			ELSE NULL
			END AS PatientId,
		CONVERT(datetime, prp.PaymentDate, 112) AS PostedDateTime,
		CASE WHEN prp.PaymentBatchCheckId IS NULL THEN NULL WHEN prp.PaymentBatchCheckId = 0 THEN NULL WHEN prp.PaymentBatchCheckId = -1 THEN NULL ELSE prp.PaymentBatchCheckId END AS FinancialBatchId,
		CASE 
			WHEN prp.PaymentReason IS NULL
				THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code)) 
					
					WHEN ''!'' -- FinancialInformationTypeId = 1
						THEN paymentReason1.Id -- ReasonCode 1
					WHEN ''|'' -- FinancialInformationTypeId = 2
						THEN paymentReason2.Id -- ReasonCode 2
					WHEN ''?'' -- FinancialInformationTypeId = 3
						THEN paymentReason3.Id -- ReasonCode 3
					ELSE 
						CAST(NULL AS int)
					END
			WHEN prp.PaymentReason = ''''
				THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code)) 
					
					WHEN ''!'' -- FinancialInformationTypeId = 1
						THEN paymentReason1.Id -- ReasonCode 1
					WHEN ''|'' -- FinancialInformationTypeId = 2
						THEN paymentReason2.Id -- ReasonCode 2
					WHEN ''?'' -- FinancialInformationTypeId = 3
						THEN paymentReason3.Id -- ReasonCode 3
					ELSE 
						CAST(NULL AS int)
					END
			ELSE
				existingPaymentReason.Id
			END AS ClaimAdjustmentReasonCodeId,
		CASE 
			WHEN prp.ClaimAdjustmentGroupCodeId IS NULL
				THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code)) 
					-- These int values correspond to the ClaimAdjustmentGroupCode enum in .NET

					WHEN ''!'' -- FinincialInformationTypeId = 1
						THEN 2 
					WHEN ''|'' -- FinincialInformationTypeId = 2
						THEN 2 
					WHEN ''?'' -- FinincialInformationTypeId = 3
						THEN 2 
					WHEN ''D'' -- FinincialInformationTypeId = 5
						THEN 3 
					WHEN '']'' -- FinincialInformationTypeId = 7
						THEN 3 
					WHEN ''0'' -- FinancialInformationTypeId = 8
						THEN 2 
					ELSE 
						CAST(NULL AS int)
					END
			ELSE
				prp.ClaimAdjustmentGroupCodeId
			END AS ClaimAdjustmentGroupCodeId
			,prp.PaymentCommentOn
	FROM dbo.PatientReceivablePayments prp
	LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason1 ON paymentReason1.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = ''1'')
	LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason2 ON paymentReason2.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = ''2'')
	LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason3 ON paymentReason3.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = ''3'')
	LEFT OUTER JOIN dbo.PracticeCodeTable existingPaymentReason ON existingPaymentReason.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = prp.PaymentReason)
	INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
	INNER JOIN dbo.PatientFinancial pf ON pf.FinancialId = pr.ComputedFinancialId
	INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''PAYMENTTYPE''
		AND prp.PaymentType = SUBSTRING(pct.Code, LEN(pct.Code), 1)
		AND (pct.AlternateCode = '''' 
			OR pct.AlternateCode IS NULL)
	INNER JOIN dbo.PatientReceivableServices prs on prs.ItemId = prp.PaymentServiceItem
		AND prs.[Status] = ''A''
	WHERE prp.PaymentFinancialType = ''''	AND prp.PayerId <> 0

	UNION ALL

	SELECT 
	prp.PaymentId AS Id
	,prp.PaymentAmountDecimal AS Amount
	,prp.PaymentServiceItem AS BillingServiceId
	,CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code)) 
		WHEN ''!''
			THEN 1
		WHEN ''|''
			THEN 2
		WHEN ''?''
			THEN 3
		WHEN ''%''
			THEN 4
		WHEN ''D''
			THEN 5
		WHEN ''&''
			THEN 6
		WHEN '']''
			THEN 7
		WHEN ''0''
			THEN 8
		WHEN ''V''
			THEN 9
		ELSE pct.Id + 1000 
	END AS FinancialInformationTypeId
	,CASE prp.PayerType
		WHEN ''I''
			THEN 1
		WHEN ''P''
			THEN 2
		WHEN ''O''
			THEN 3
		ELSE 4
	END AS FinancialSourceTypeId
	,CASE prp.PayerType
		WHEN ''I''
			THEN Prp.PayerId
		ELSE NULL
	END AS InsurerId
	,model.GetBigPairId((CASE 
		WHEN pf2.FinancialId IS NULL OR pf2.FinancialId = 0
			THEN 0
		ELSE pf2.FinancialId
		END), pr2.AppointmentId, @InvoiceReceivableMax) 
	AS InvoiceReceivableId
	,CASE prp.PayerType
		WHEN ''P''
			THEN prp.PayerId
		ELSE NULL
	END AS PatientId
	,CONVERT(DATETIME, prp.PaymentDate, 112) AS PostedDateTime
	,CASE 
		WHEN prp.PaymentBatchCheckId IS NULL 
			THEN NULL 
		WHEN prp.PaymentBatchCheckId = 0 
			THEN NULL 
		WHEN prp.PaymentBatchCheckId = -1 
			THEN NULL 
		ELSE prp.PaymentBatchCheckId 
	END AS FinancialBatchId
	,CASE 
		WHEN prp.PaymentReason IS NULL
			THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code)) 
					
				WHEN ''!'' -- FinancialInformationTypeId = 1
					THEN paymentReason1.Id -- ReasonCode 1
				WHEN ''|'' -- FinancialInformationTypeId = 2
					THEN paymentReason2.Id -- ReasonCode 2
				WHEN ''?'' -- FinancialInformationTypeId = 3
					THEN paymentReason3.Id -- ReasonCode 3
				ELSE 
					CAST(NULL AS int)
				END
		WHEN prp.PaymentReason = ''''
			THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code)) 
					
				WHEN ''!'' -- FinancialInformationTypeId = 1
					THEN paymentReason1.Id -- ReasonCode 1
				WHEN ''|'' -- FinancialInformationTypeId = 2
					THEN paymentReason2.Id -- ReasonCode 2
				WHEN ''?'' -- FinancialInformationTypeId = 3
					THEN paymentReason3.Id -- ReasonCode 3
				ELSE 
					CAST(NULL AS int)
				END
		ELSE
			existingPaymentReason.Id
	END AS ClaimAdjustmentReasonCodeId
	,CASE 
		WHEN prp.ClaimAdjustmentGroupCodeId IS NULL
			THEN CASE SUBSTRING(pct.Code, (dbo.GetMax((CHARINDEX('' - '', pct.Code)+3), 0)), LEN(pct.Code)) 
				-- These int values correspond to the ClaimAdjustmentGroupCode enum in .NET
				WHEN ''!'' -- FinincialInformationTypeId = 1
					THEN 2 
				WHEN ''|'' -- FinincialInformationTypeId = 2
					THEN 2 
				WHEN ''?'' -- FinincialInformationTypeId = 3
					THEN 2 
				WHEN ''D'' -- FinincialInformationTypeId = 5
					THEN 3 
				WHEN '']'' -- FinincialInformationTypeId = 7
					THEN 3 
				WHEN ''0'' -- FinancialInformationTypeId = 8
					THEN 2 
				ELSE 
					CAST(NULL AS int)
				END
		ELSE
			prp.ClaimAdjustmentGroupCodeId
	END AS ClaimAdjustmentGroupCodeId
	,prp.PaymentCommentOn
	FROM dbo.PatientReceivablePayments prp
	LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason1 ON paymentReason1.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = ''1'')
	LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason2 ON paymentReason2.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = ''2'')
	LEFT OUTER JOIN dbo.PracticeCodeTable paymentReason3 ON paymentReason3.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = ''3'')
	LEFT OUTER JOIN dbo.PracticeCodeTable existingPaymentReason ON existingPaymentReason.Id = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = ''PAYMENTREASON'' AND pct.Code = prp.PaymentReason)
	INNER JOIN dbo.PatientReceivables pr1 ON pr1.ReceivableId = prp.ReceivableId
		AND pr1.ComputedFinancialId IS NULL
	INNER JOIN dbo.PatientReceivables pr2 ON pr1.Invoice = pr2.Invoice
		AND pr2.ComputedFinancialId IS NOT NULL
	LEFT JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr1.ReceivableId
	LEFT JOIN dbo.PatientFinancial pf1 ON pf1.PatientId = pr1.InsuredId
		AND CONVERT(DATETIME,pf1.FinancialStartDate) <= CONVERT(DATETIME,pr1.InvoiceDate)
		AND (pf1.FinancialEndDate IS NULL OR pf1.FinancialEndDate = '''' OR (CONVERT(DATETIME,pf1.FinancialEndDate) >= CONVERT(DATETIME,pr1.InvoiceDate)))
	LEFT JOIN dbo.PatientFinancial pf2 ON pf2.PatientId = pr2.InsuredId
		AND CONVERT(DATETIME,pf2.FinancialStartDate) <= CONVERT(DATETIME,pr2.InvoiceDate)
		AND (pf2.FinancialEndDate IS NULL OR pf2.FinancialEndDate = '''' OR (CONVERT(DATETIME,pf2.FinancialEndDate) >= CONVERT(DATETIME,pr2.InvoiceDate)))
	INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = ''PAYMENTTYPE''
		AND prp.PaymentType = SUBSTRING(pct.Code, LEN(pct.Code), 1)
		AND (pct.AlternateCode = '''' 
			OR pct.AlternateCode IS NULL)
	INNER JOIN dbo.PatientReceivableServices prs on prs.ItemId = prp.PaymentServiceItem
		AND prs.[Status] = ''A''
	WHERE prp.PaymentFinancialType = ''''	
		AND prp.PayerType = ''I''
		AND prp.PayerId <> 0
		AND ptj.TransactionId IS NULL
		AND pr1.PatientId = pr1.InsuredId
		AND pf1.FinancialId IS NULL
		AND pr1.InsurerId IS NOT NULL
		AND pr1.InsurerId <> 99999
		AND pr2.InsurerId <> 99999
		AND prp.PaymentId IS NOT NULL
		AND prp.PayerId = pf2.FinancialInsurerId
)v

INSERT INTO FinancialInformationsBackup (Id, Amount, PostedDateTime, FinancialInformationTypeId, BillingServiceId,  InvoiceReceivableId,  FinancialSourceTypeId, PatientId, InsurerId, FinancialBatchId, ClaimAdjustmentGroupCodeId, ClaimAdjustmentReasonCodeId,PaymentCommentOn)
SELECT Id,
	Amount,
	PostedDateTime,
	FinancialInformationTypeId,
	BillingServiceId,
	InvoiceReceivableId,
	FinancialSourceTypeId,
	PatientId,
	InsurerId,
	FinancialBatchId,
	ClaimAdjustmentGroupCodeId,
	ClaimAdjustmentReasonCodeId,
	PaymentCommentOn
FROM #tempFinancialInformations
')

END 
GO

IF OBJECT_ID(N'model.FinancialBatches','V') IS NOT NULL
BEGIN
EXEC('
INSERT INTO dbo.[FinancialBatchesBackup] (Id, FinancialSourceTypeId, PatientId, InsurerId, PaymentDateTime, ExplanationOfBenefitsDateTime, Amount, CheckCode, ComputedFinancialBatchId)
SELECT PaymentId AS Id,
	CASE prp.PayerType
		WHEN ''I''
			THEN 1
		WHEN ''P''
			THEN 2
		WHEN ''O''
			THEN 3
		ELSE 4
		END AS FinancialSourceTypeId,
	CASE WHEN (prp.PayerId IS NOT NULL AND prp.PayerType = ''P'') THEN prp.PayerId ELSE NULL END AS PatientId,
	CASE WHEN (prp.PayerId IS NOT NULL AND prp.PayerType = ''I'') THEN prp.PayerId ELSE NULL END AS InsurerId,
	CASE WHEN prp.PaymentDate IS NOT NULL THEN prp.PaymentDate ELSE NULL END AS PaymentDateTime,
	CASE WHEN prp.PaymentEOBDate = '''' THEN NULL ELSE CONVERT(datetime, prp.PaymentEOBDate, 112) END AS ExplanationOfBenefitsDateTime,
	prp.PaymentAmountDecimal AS Amount,
	prp.PaymentCheck AS CheckCode,
	prp.PaymentId AS ComputedFinancialBatchId
FROM dbo.PatientReceivablePayments prp
WHERE prp.PaymentType = ''=''
')
END
GO

IF OBJECT_ID(N'model.BillingServiceTransactions','V') IS NOT NULL
BEGIN

EXEC('
DECLARE @InvoiceReceivableMax int
SET @InvoiceReceivableMax = 110000000

SELECT st.Id AS Id,
	CONVERT(decimal(18,2), Amount) AS AmountSent,
	ServiceId AS BillingServiceId,
	CASE 
		WHEN ptj.TransactionStatus = ''P''
			AND TransportAction IN (''B'', ''P'')
			THEN 1
		WHEN ptj.TransactionStatus = ''P''
			AND TransportAction = ''V''
			THEN 3
		WHEN ptj.TransactionStatus = ''S''
			AND TransportAction = ''P''
			THEN 2
		WHEN ptj.TransactionStatus = ''S''
			AND TransportAction = ''V''
			THEN 3
		WHEN ptj.TransactionStatus = ''S''
			AND TransportAction = ''X''
			THEN 4
		WHEN ptj.TransactionStatus = ''Z''
			AND TransportAction = ''1''
			THEN 6
		WHEN ptj.TransactionStatus = ''Z''
			AND TransportAction = ''2''
			THEN 7
		WHEN ptj.TransactionStatus = ''S''
			AND TransportAction = ''B''
			THEN 8
		ELSE 5
		END AS BillingServiceTransactionStatusId,
	NULL AS ClaimFileNumber,
	CASE 
		WHEN ptj.TransactionRef = ''I'' AND ptj.TransactionAction = ''B'' 
			THEN pctMethod.Id 
		ELSE NULL 
		END AS ClaimFileReceiverId,
	CONVERT(datetime, ptj.TransactionDate, 112) AS DateTime,
	model.GetBigPairId(CASE
		WHEN ptj.TransactionRef = ''P'' then 0
		ELSE pr.ComputedFinancialId
		END, pr.AppointmentId, @InvoiceReceivableMax)  AS InvoiceReceivableId,
	CASE 
		WHEN (ptj.TransactionRef = ''I'' AND ptj.TransactionAction = ''B'')
			THEN 1
		ELSE 2 
		END AS MethodSentId,
	st.ExternalSystemMessageId as ExternalSystemMessageId,
	NULL AS PatientAggregateStatementId
	,ptj.TransactionId
	INTO #tempBillingSerivceTransactions
FROM dbo.PracticeTransactionJournal ptj
INNER JOIN dbo.ServiceTransactions st ON st.TransactionId = ptj.TransactionId
INNER JOIN dbo.PatientReceivableServices prs on prs.ItemId = st.ServiceId
	AND prs.[Status] = ''A''
INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = ptj.TransactionTypeId	
	AND pr.ComputedFinancialId IS NOT NULL
INNER JOIN dbo.PracticeInsurers pri on pri.InsurerId = pr.InsurerId
LEFT JOIN dbo.PracticeCodeTable pctMethod ON pri.InsurerTFormat = SUBSTRING(pctMethod.Code, 1, 1) 
	AND pctMethod.ReferenceType = ''TRANSMITTYPE''
WHERE 
ptj.TransactionType = ''R''
	AND TransactionRef <> ''G''

INSERT INTO dbo.[BillingServiceTransactionsBackup] (Id, DateTime, AmountSent, ClaimFileNumber, 
	BillingServiceTransactionStatusId, MethodSentId, BillingServiceId, InvoiceReceivableId, ClaimFileReceiverId, 
	ExternalSystemMessageId, PatientAggregateStatementId, TransactionId)
SELECT Id,
	DateTime,
	AmountSent,
	ClaimFileNumber,
	BillingServiceTransactionStatusId,
	MethodSentId,
	BillingServiceId,
	InvoiceReceivableId,
	ClaimFileReceiverId,
	ExternalSystemMessageId,
	PatientAggregateStatementId,
	TransactionId
FROM  #tempBillingSerivceTransactions
')
END
GO