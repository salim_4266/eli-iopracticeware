﻿--Drop these temp table if they exist
IF OBJECT_ID('tempdb..#InsurancePolicy') IS NOT NULL 
	DROP TABLE #InsurancePolicy

IF OBJECT_ID('tempdb..#PatientInsurance') IS NOT NULL 
	DROP TABLE #PatientInsurance

DECLARE @schemaName NVARCHAR(70)
	SET @schemaName = 'model'

DECLARE @modelPItblName NVARCHAR(70)
	SET @modelPItblName ='PatientInsurances'
-- drop all partitions that are associated to model.PatientInsurances
EXEC dbo.DropViewsLike @schemaName,@modelPItblName

DECLARE @modelIPtblName NVARCHAR(70)
	SET @modelIPtblName ='InsurancePolicies'

-- drop all partitions that are associated to model.InsurancePolicies
EXEC dbo.DropViewsLike @schemaName,@modelIPtblName

-- create a temp InsurancePolicy table of data currently in the view
;WITH CreateTemporaryInsurancePolicyTable AS
(
SELECT pf.FinancialId AS Id,
	CONVERT(DECIMAL(18,2), FinancialCopay) AS Copay,
	CONVERT(DECIMAL(18,2), 0) AS Deductible,
	FinancialGroupId AS GroupCode,
	FinancialInsurerId AS InsurerId,
	CASE
		WHEN pd.MedicareSecondary = '12' AND pins.InsurerReferenceCode IN ('M', 'N', '[')
			THEN 1
		WHEN pd.MedicareSecondary = '13' AND pins.InsurerReferenceCode IN ('M', 'N', '[')
			THEN 2
		WHEN pd.MedicareSecondary = '14' AND pins.InsurerReferenceCode IN ('M', 'N', '[')
			THEN 3
		WHEN pd.MedicareSecondary = '15' AND pins.InsurerReferenceCode IN ('M', 'N', '[')
			THEN 4
		WHEN pd.MedicareSecondary = '16' AND pins.InsurerReferenceCode IN ('M', 'N', '[')
			THEN 5
		WHEN pd.MedicareSecondary = '41' AND pins.InsurerReferenceCode IN ('M', 'N', '[')
			THEN 6
		WHEN pd.MedicareSecondary = '42' AND pins.InsurerReferenceCode IN ('M', 'N', '[')
			THEN 7
		WHEN pd.MedicareSecondary = '43' AND pins.InsurerReferenceCode IN ('M', 'N', '[')
			THEN 8
		WHEN pd.MedicareSecondary = '47' AND pins.InsurerReferenceCode IN ('M', 'N', '[')
			THEN 9
		ELSE NULL 
	END AS MedicareSecondaryReasonCodeId,
	FinancialPerson AS PolicyCode,
	pf.PatientId AS PolicyHolderPatientId,
	CONVERT(DATETIME,FinancialStartDate, 112) AS StartDateTime
FROM dbo.PatientFinancial pf
INNER JOIN dbo.PatientDemographics pd ON pd.PatientId = pf.PatientId
INNER JOIN dbo.PracticeInsurers pins ON pins.InsurerId = pf.FinancialInsurerId
)
SELECT 
Id
,Copay
,Deductible
,GroupCode
,InsurerId
,MedicareSecondaryReasonCodeId
,PolicyCode
,PolicyHolderPatientId
,StartDateTime
INTO #InsurancePolicy
FROM CreateTemporaryInsurancePolicyTable

-- create a temp PatientInsurance table of data currently in the view
DECLARE @PatientInsuranceMax INT
SET @PatientInsuranceMax = 110000000

;WITH CreateTemporaryPatientInsuranceTable AS (
SELECT v.Id AS Id
	,v.InsurancePolicyId AS InsurancePolicyId
	,v.InsuranceTypeId AS InsuranceTypeId
	,v.OrdinalId AS OrdinalId
	,v.InsuredPatientId AS InsuredPatientId
	,v.PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId
	,v.EndDateTime
	,CONVERT(BIT,0) AS IsDeleted
	,v.Id AS ComputedPatientInsuranceId
FROM (
	SELECT model.GetBigPairId(pd.PatientId, pr.ComputedFinancialId, @PatientInsuranceMax) AS Id
		,pf.FinancialId AS InsurancePolicyId
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3			
			WHEN 'W'
				THEN 4
			ELSE 1
			END AS InsuranceTypeId
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000			
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + CASE 
				WHEN pf.PatientId = pd.PolicyPatientId
					THEN 0
				ELSE 3
				END + CONVERT(INT, FinancialPIndicator) AS OrdinalId
		,pd.PatientId AS InsuredPatientId
		,CASE (
				CASE 
					WHEN pd.PolicyPatientId = pf.PatientId
						THEN pd.Relationship
					ELSE pd.SecondRelationship
					END
				)
			WHEN 'Y'
				THEN 1
			WHEN 'S'
				THEN 2
			WHEN 'C'
				THEN 3
			WHEN 'E'
				THEN 4
			WHEN 'L'
				THEN 6
			WHEN 'O'
				THEN 7
			WHEN 'D'
				THEN 7
			WHEN 'P' 
				THEN 7
			ELSE 5
			END AS PolicyHolderRelationshipTypeId
		,COUNT_BIG(*) AS COUNT
		,CASE
			WHEN LEN(pf.FinancialEndDate)=8
				THEN CONVERT(DATETIME,pf.FinancialEndDate)
			ELSE NULL
		END AS EndDateTime
	FROM dbo.PatientDemographics pd
	INNER JOIN dbo.PatientReceivables pr ON pr.PatientId = pd.PatientId
	INNER JOIN dbo.PatientFinancial pf ON pf.FinancialId = pr.ComputedFinancialId
	INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
	GROUP BY model.GetBigPairId(pd.PatientId, pr.ComputedFinancialId, @PatientInsuranceMax)
		,pf.FinancialId
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3			
			WHEN 'W'
				THEN 4
			ELSE 1
			END
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000			
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + CASE 
				WHEN pf.PatientId = pd.PolicyPatientId
					THEN 0
				ELSE 3
				END + CONVERT(INT, FinancialPIndicator)
		,pd.PatientId
		,CASE (
				CASE 
					WHEN pd.PolicyPatientId = pf.PatientId
						THEN pd.Relationship
					ELSE pd.SecondRelationship
					END
				)
			WHEN 'Y'
				THEN 1
			WHEN 'S'
				THEN 2
			WHEN 'C'
				THEN 3
			WHEN 'E'
				THEN 4
			WHEN 'L'
				THEN 6
			WHEN 'O'
				THEN 7
			WHEN 'D'
				THEN 7
			WHEN 'P' 
				THEN 7
			ELSE 5
		END
		,CASE
			WHEN LEN(pf.FinancialEndDate)=8
				THEN CONVERT(DATETIME,pf.FinancialEndDate)
			ELSE NULL
		END
	) v

UNION ALL
-------all patient insurance policies where a receivable does not exist
---Policies where a receivable doesn't exist; patient is policyholder
SELECT v.Id AS Id
	,v.InsurancePolicyId AS InsurancePolicyId
	,v.InsuranceTypeId AS InsuranceTypeId
	,v.OrdinalId AS OrdinalId
	,v.InsuredPatientId AS InsuredPatientId
	,v.PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId
	,v.EndDateTime
	,CONVERT(BIT,0) AS IsDeleted
	,v.Id AS ComputedPatientInsuranceId
FROM (
	SELECT model.GetBigPairId(pd.PatientId, pf.FinancialId, @PatientInsuranceMax) AS Id
		,pf.FinancialId AS InsurancePolicyId
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3
			WHEN 'W'
				THEN 4
			ELSE 1
			END AS InsuranceTypeId
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + CASE 
				WHEN pf.PatientId = pd.PolicyPatientId
					THEN 0
				ELSE 3
				END + CONVERT(INT, FinancialPIndicator) AS OrdinalId
		,pd.PatientId AS InsuredPatientId
		,CASE (
				CASE 
					WHEN pd.PolicyPatientId = pf.PatientId
						THEN pd.Relationship
					ELSE pd.SecondRelationship
					END
				)
			WHEN 'Y'
				THEN 1
			WHEN 'S'
				THEN 2
			WHEN 'C'
				THEN 3
			WHEN 'E'
				THEN 4
			WHEN 'L'
				THEN 6
			WHEN 'O'
				THEN 7
			WHEN 'D'
				THEN 7
			WHEN 'P' 
				THEN 7
			ELSE 5  
			END AS PolicyHolderRelationshipTypeId
		,pf.FinancialId AS FinancialId
		,COUNT_BIG(*) AS COUNT
		,CASE
			WHEN LEN(pf.FinancialEndDate)=8
				THEN CONVERT(DATETIME,pf.FinancialEndDate)
			ELSE NULL
		END AS EndDateTime
	FROM dbo.PatientDemographics pd
	INNER JOIN dbo.PatientFinancial pf ON pf.PatientId = pd.PolicyPatientId
		OR pf.PatientId = pd.SecondPolicyPatientId
	INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
	WHERE 
		(pf.PatientId = pd.PolicyPatientId OR pf.PatientId = pd.SecondPolicyPatientId)
		AND pf.PatientId = pd.PatientId			
	GROUP BY model.GetBigPairId(pd.PatientId, pf.FinancialId, @PatientInsuranceMax)
		,pf.FinancialId
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3			
			WHEN 'W'
				THEN 4
			ELSE 1
			END
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000			
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + CASE 
				WHEN pf.PatientId = pd.PolicyPatientId
					THEN 0
				ELSE 3
				END + CONVERT(INT, FinancialPIndicator)
		,pd.PatientId
		,CASE (
				CASE 
					WHEN pd.PolicyPatientId = pf.PatientId
						THEN pd.Relationship
					ELSE pd.SecondRelationship
					END
				)
			WHEN 'Y'
				THEN 1
			WHEN 'S'
				THEN 2
			WHEN 'C'
				THEN 3
			WHEN 'E'
				THEN 4
			WHEN 'L'
				THEN 6
			WHEN 'O'
				THEN 7
			WHEN 'D'
				THEN 7
			WHEN 'P' 
				THEN 7
			ELSE 5
			END
		,pf.FinancialId
		,CASE
			WHEN LEN(pf.FinancialEndDate)=8
				THEN CONVERT(DATETIME,pf.FinancialEndDate)
			ELSE NULL
		END 
	) v
LEFT JOIN dbo.PatientReceivables pr ON pr.PatientId = v.InsuredPatientId
	AND pr.ComputedFinancialId = v.FinancialId
WHERE pr.ReceivableId IS NULL

UNION ALL

---all patient insurance policies where a receivable does not exist; first policyholder is not the patient
SELECT v.Id AS Id
	,v.InsurancePolicyId AS InsurancePolicyId
	,v.InsuranceTypeId AS InsuranceTypeId
	,v.OrdinalId AS OrdinalId
	,v.InsuredPatientId AS InsuredPatientId
	,v.PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId
	,v.EndDateTime
	,CONVERT(BIT,0) AS IsDeleted
	,v.Id AS ComputedPatientInsuranceId
FROM (
	SELECT model.GetBigPairId(pd.PatientId, pf.FinancialId, @PatientInsuranceMax) AS Id
		,pf.FinancialId AS InsurancePolicyId
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3
			WHEN 'W'
				THEN 4
			ELSE 1
			END AS InsuranceTypeId
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + 0 + CONVERT(INT, FinancialPIndicator) AS OrdinalId
		,pd.PatientId AS InsuredPatientId
		,CASE pd.Relationship
			WHEN 'Y'
				THEN 1
			WHEN 'S'
				THEN 2
			WHEN 'C'
				THEN 3
			WHEN 'E'
				THEN 4
			WHEN 'L'
				THEN 6
			WHEN 'O'
				THEN 7
			WHEN 'D'
				THEN 7
			WHEN 'P' 
				THEN 7
			ELSE 5  
			END AS PolicyHolderRelationshipTypeId
		,pf.FinancialId AS FinancialId
		,COUNT_BIG(*) AS COUNT
		,CASE
			WHEN LEN(pf.FinancialEndDate)=8
				THEN CONVERT(DATETIME,pf.FinancialEndDate)
			ELSE NULL
		END AS EndDateTime
	FROM dbo.PatientDemographics pd
	INNER JOIN dbo.PatientFinancial pf ON pf.PatientId = pd.PolicyPatientId
	INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
	WHERE 
		pd.PolicyPatientId <> pd.PatientId
		AND pd.PolicyPatientId = pf.PatientId
		AND pd.PolicyPatientId <> 0			
		AND pri.AllowDependents = 1				
	GROUP BY model.GetBigPairId(pd.PatientId, pf.FinancialId, @PatientInsuranceMax)
		,pf.FinancialId
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3			
			WHEN 'W'
				THEN 4
			ELSE 1
			END
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000			
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + 0 + CONVERT(INT, FinancialPIndicator)
		,pd.PatientId
		,CASE pd.Relationship
			WHEN 'Y'
				THEN 1
			WHEN 'S'
				THEN 2
			WHEN 'C'
				THEN 3
			WHEN 'E'
				THEN 4
			WHEN 'L'
				THEN 6
			WHEN 'O'
				THEN 7
			WHEN 'D'
				THEN 7
			WHEN 'P' 
				THEN 7
			ELSE 5
			END
		,pf.FinancialId
		,CASE
			WHEN LEN(pf.FinancialEndDate)=8
				THEN CONVERT(DATETIME,pf.FinancialEndDate)
			ELSE NULL
		END
	) v
LEFT JOIN dbo.PatientReceivables pr ON pr.PatientId = v.InsuredPatientId
	AND pr.ComputedFinancialId = v.FinancialId
WHERE pr.ReceivableId IS NULL

UNION ALL

---all patient insurance policies where a receivable does not exist; second policyholder is not patient
SELECT v.Id AS Id
	,v.InsurancePolicyId AS InsurancePolicyId
	,v.InsuranceTypeId AS InsuranceTypeId
	,v.OrdinalId AS OrdinalId
	,v.InsuredPatientId AS InsuredPatientId
	,v.PolicyHolderRelationshipTypeId AS PolicyHolderRelationshipTypeId
	,v.EndDateTime
	,CONVERT(BIT,0) AS IsDeleted
	,v.Id AS ComputedPatientInsuranceId
FROM (
	SELECT model.GetBigPairId(pd.PatientId, pf.FinancialId, @PatientInsuranceMax) AS Id
		,pf.FinancialId AS InsurancePolicyId
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3
			WHEN 'W'
				THEN 4
			ELSE 1
			END AS InsuranceTypeId
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + 3 + CONVERT(INT, FinancialPIndicator) AS OrdinalId
		,pd.PatientId AS InsuredPatientId
		,CASE pd.SecondRelationship
			WHEN 'Y'
				THEN 1
			WHEN 'S'
				THEN 2
			WHEN 'C'
				THEN 3
			WHEN 'E'
				THEN 4
			WHEN 'L'
				THEN 6
			WHEN 'O'
				THEN 7
			WHEN 'D'
				THEN 7
			WHEN 'P' 
				THEN 7
			ELSE 5  
			END AS PolicyHolderRelationshipTypeId
		,pf.FinancialId AS FinancialId
		,COUNT_BIG(*) AS COUNT
		,CASE
			WHEN LEN(pf.FinancialEndDate)=8
				THEN CONVERT(DATETIME,pf.FinancialEndDate)
			ELSE NULL
		END AS EndDateTime
	FROM dbo.PatientDemographics pd
	INNER JOIN dbo.PatientFinancial pf ON pf.PatientId = pd.PolicyPatientId
		OR pf.PatientId = pd.SecondPolicyPatientId
	INNER JOIN dbo.PracticeInsurers pri ON pri.InsurerId = pf.FinancialInsurerId
	WHERE pd.SecondPolicyPatientId <> pd.PatientId
		AND pd.SecondPolicyPatientId = pf.PatientId
		AND pd.SecondPolicyPatientId <> 0			
		AND pri.AllowDependents = 1				
	GROUP BY model.GetBigPairId(pd.PatientId, pf.FinancialId, @PatientInsuranceMax)
		,pf.FinancialId
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3			
			WHEN 'W'
				THEN 4
			ELSE 1
			END
		,CASE pf.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000			
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + 3 + CONVERT(INT, FinancialPIndicator)
		,pd.PatientId
		,CASE pd.SecondRelationship
			WHEN 'Y'
				THEN 1
			WHEN 'S'
				THEN 2
			WHEN 'C'
				THEN 3
			WHEN 'E'
				THEN 4
			WHEN 'L'
				THEN 6
			WHEN 'O'
				THEN 7
			WHEN 'D'
				THEN 7
			WHEN 'P' 
				THEN 7
			ELSE 5
			END
		,pf.FinancialId
		,CASE
			WHEN LEN(pf.FinancialEndDate)=8
				THEN CONVERT(DATETIME,pf.FinancialEndDate)
			ELSE NULL
		END
	) v
LEFT JOIN dbo.PatientReceivables pr ON pr.PatientId = v.InsuredPatientId
	AND pr.ComputedFinancialId = v.FinancialId
WHERE pr.ReceivableId IS NULL
)
SELECT 
ROW_NUMBER() OVER (ORDER BY Id) AS Id
,EndDateTime
,InsurancePolicyId
,InsuranceTypeId
,CASE 
	WHEN OrdinalId >= 11 AND OrdinalId <= 199
		THEN OrdinalId % 10
	WHEN OrdinalId >= 201 AND OrdinalId <= 2999
		THEN OrdinalId % 200
	WHEN OrdinalId >= 3001 AND OrdinalId <= 39999
		THEN OrdinalId % 3000
	WHEN OrdinalId >= 40001
		THEN OrdinalId % 40000
	ELSE OrdinalId
END AS OrdinalId
,InsuredPatientId
,PolicyHolderRelationshipTypeId
,IsDeleted
,ComputedPatientInsuranceId
INTO #PatientInsurance
FROM CreateTemporaryPatientInsuranceTable

-- add indexes to the temporary table
CREATE INDEX IX_InsurancePolicy_Id ON #InsurancePolicy(Id)
CREATE INDEX IX_PatientInsurance_Id ON #PatientInsurance(Id)

--if there already is a table called model.InsurancePolicies, drop it
IF (EXISTS (SELECT * 
            FROM INFORMATION_SCHEMA.TABLES 
            WHERE TABLE_SCHEMA = @schemaName 
            AND  TABLE_NAME = @modelIPtblName))
BEGIN
	EXEC DropObject 'model.InsurancePolicies';
END

CREATE TABLE [model].[InsurancePolicies] (
    [Id] INT IDENTITY(1,1) NOT NULL,
    [PolicyCode] NVARCHAR(MAX)  NOT NULL,
    [GroupCode] NVARCHAR(MAX)  NULL,
    [Copay] DECIMAL(18,0)  NULL,
    [Deductible] DECIMAL(18,0)  NULL,
    [StartDateTime] DATETIME  NOT NULL,
    [PolicyHolderPatientId] INT  NOT NULL,
    [InsurerId] INT  NOT NULL,
    [MedicareSecondaryReasonCodeId] INT  NULL
);

-- populate the newly created table with data from temp table (#InsurancePolicies)
SET IDENTITY_INSERT [model].[InsurancePolicies] ON

INSERT INTO [model].[InsurancePolicies]
(Id,PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT 
Id
,PolicyCode
,GroupCode
,Copay
,Deductible
,StartDateTime
,PolicyHolderPatientId
,InsurerId
,MedicareSecondaryReasonCodeId
FROM #InsurancePolicy

--since identity insert is used, reseed the table the the largest current Id + 1
DECLARE @MaxIdInInsurancePolicies INT 
SET @MaxIdInInsurancePolicies = (SELECT MAX(Id) + 1 FROM model.InsurancePolicies WHERE Id < 10000000) --Need this because the max can be in the range of the other server.

IF (@MaxIdInInsurancePolicies IS NULL)
BEGIN 
	SET @MaxIdInInsurancePolicies = 1
END

DBCC CHECKIDENT ('[model].[InsurancePolicies]', RESEED, @MaxIdInInsurancePolicies)

--now that table is reseeded, turn identity insert off
SET IDENTITY_INSERT model.InsurancePolicies OFF


IF (EXISTS (SELECT * 
            FROM INFORMATION_SCHEMA.TABLES 
            WHERE TABLE_SCHEMA = 'model'
            AND  TABLE_NAME = 'PatientInsurances'))
BEGIN
	EXEC DropObject 'model.PatientInsurances';
END

CREATE TABLE [model].[PatientInsurances](
    [Id] BIGINT IDENTITY(1,1) NOT NULL,
    [InsuranceTypeId] INT  NOT NULL,
    [PolicyHolderRelationshipTypeId] INT  NOT NULL,
    [OrdinalId] INT  NOT NULL,
    [InsuredPatientId] INT  NOT NULL,
    [InsurancePolicyId] INT  NOT NULL,
    [EndDateTime] DATETIME  NULL,
	[IsDeleted] BIT NOT NULL,
	[ComputedPatientInsuranceId] BIGINT NULL 
);

SET IDENTITY_INSERT [model].[PatientInsurances] ON

INSERT INTO [model].[PatientInsurances]
(Id,InsuranceTypeId,PolicyHolderRelationshipTypeId,OrdinalId,InsuredPatientId,InsurancePolicyId,EndDateTime,IsDeleted, ComputedPatientInsuranceId)
SELECT 
Id
,InsuranceTypeId
,PolicyHolderRelationshipTypeId
,OrdinalId
,InsuredPatientId
,InsurancePolicyId
,EndDateTime
,IsDeleted
,ComputedPatientInsuranceId
FROM #PatientInsurance
GO

DECLARE @MaxIdInPatientInsurance INT
SET @MaxIdInPatientInsurance = (SELECT MAX(Id) FROM #PatientInsurance)

IF (@MaxIdInPatientInsurance IS NULL)
BEGIN    
	SET @MaxIdInPatientInsurance = 1
END

DBCC CHECKIDENT ('[model].[PatientInsurances]', RESEED, @MaxIdInPatientInsurance)

SET IDENTITY_INSERT [model].[PatientInsurances] OFF

/*
Find irrelevant data (since the query above creates PatientInsurances from PatientReceivables 
billed where the insurance was then unlinked/removed the PolicyPatientId is incorrect on the 
PatientDemographics table. Turn the IsDeleted flag on and add an EndDateTime.)
*/
IF OBJECT_ID('tempdb..#UpdateTheseIdsBecauseTheyAreIrrelevant') IS NOT NULL
    DROP TABLE #UpdateTheseIdsBecauseTheyAreIrrelevant

;WITH FindAllIrrelevantPatientInsurances AS (
SELECT 
pi.InsuredPatientId
,ip.PolicyHolderPatientId
,pd.PolicyPatientId AS PDPolicyPatientId 
,pd.SecondPolicyPatientId AS PDSecondPolicyPatientId
,pd.Relationship
,pd.SecondRelationship
,pi.Id AS PatientInsuranceId
,ip.Id AS InsurancePolicyId
FROM model.PatientInsurances pi
INNER JOIN
	(SELECT 
	InsuredPatientId
	,OrdinalId
	,IsDeleted
	FROM model.PatientInsurances
	WHERE IsDeleted <> 1
	GROUP BY InsuredPatientId,OrdinalId,IsDeleted
	HAVING COUNT(*) > 1)v ON v.InsuredPatientId=pi.InsuredPatientId
JOIN model.InsurancePolicies ip ON ip.Id=pi.InsurancePolicyId
LEFT JOIN PatientDemographics pd ON pd.PatientId=pi.InsuredPatientId
WHERE pi.IsDeleted = 0
)
SELECT 
* 
INTO #UpdateTheseIdsBecauseTheyAreIrrelevant
FROM FindAllIrrelevantPatientInsurances
WHERE PolicyHolderPatientId <> PDPolicyPatientId  
AND PolicyHolderPatientId <> PDSecondPolicyPatientId
ORDER BY InsuredPatientId

UPDATE model.PatientInsurances 
SET IsDeleted = 1,
EndDateTime = GETDATE()
FROM #UpdateTheseIdsBecauseTheyAreIrrelevant irrelevant
JOIN model.PatientInsurances pi ON irrelevant.PatientInsuranceId = pi.Id

DELETE FROM model.PatientInsurances
WHERE InsurancePolicyId NOT IN (SELECT Id FROM model.InsurancePolicies)

-- add relationship properties
IF OBJECT_ID(N'[model].[FK_InsurancePolicyPatientInsurance]', 'F') IS NOT NULL
    ALTER TABLE [model].[PatientInsurances] DROP CONSTRAINT [FK_InsurancePolicyPatientInsurance];

IF OBJECT_ID(N'[model].[FK_PatientPatientInsurance]', 'F') IS NOT NULL
    ALTER TABLE [model].[PatientInsurances] DROP CONSTRAINT [FK_PatientPatientInsurance];

-- primary key clustered on patientinsurances
ALTER TABLE [model].[PatientInsurances]
ADD CONSTRAINT [PK_PatientInsurances]
    PRIMARY KEY CLUSTERED ([Id] ASC);

-- index on patientinsurances insurance policy id
CREATE INDEX [IX_FK_InsurancePolicyPatientInsurance]
ON [model].[PatientInsurances]
    ([InsurancePolicyId]);

-- index on patientinsurances insuredpatientid
CREATE INDEX [IX_FK_PatientPatientInsurance]
ON [model].[PatientInsurances]
    ([InsuredPatientId]);

-- add relationship properties
IF OBJECT_ID(N'[model].[FK_PatientInsurancePolicy]', 'F') IS NOT NULL
    ALTER TABLE [model].[InsurancePolicies] DROP CONSTRAINT [FK_PatientInsurancePolicy];

IF OBJECT_ID(N'[model].[FK_InsurerInsurancePolicy]', 'F') IS NOT NULL
    ALTER TABLE [model].[InsurancePolicies] DROP CONSTRAINT [FK_InsurerInsurancePolicy];

-- primary key clustered on insurancepolicies 
ALTER TABLE [model].[InsurancePolicies]
ADD CONSTRAINT [PK_InsurancePolicies]
    PRIMARY KEY CLUSTERED ([Id] ASC);

-- index on insurancepolicies policypatientid
CREATE INDEX [IX_FK_PatientInsurancePolicy]
ON [model].[InsurancePolicies]
    ([PolicyHolderPatientId]);

-- index on insurancepolicies insurerid
CREATE INDEX [IX_FK_InsurerInsurancePolicy]
ON [model].[InsurancePolicies]
    ([InsurerId]);

-- add foreign key to patientinsurances.InsurancePolicyId from insurancepolicies.Id
ALTER TABLE [model].[PatientInsurances]
ADD CONSTRAINT [FK_InsurancePolicyPatientInsurance]
    FOREIGN KEY ([InsurancePolicyId])
    REFERENCES [model].[InsurancePolicies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- to finalize the migration, create a backup of current patientfinancial table
IF OBJECT_ID(N'dbo.PatientFinancialBackup', 'U') IS NOT NULL
    DROP TABLE dbo.PatientFinancialBackup;

SELECT 
* 
INTO dbo.PatientFinancialBackup
FROM PatientFinancial

-- drop the PatientFinancial table 
EXEC DropObject 'PatientFinancial'

GO