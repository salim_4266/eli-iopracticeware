﻿SET IDENTITY_INSERT [model].[Permissions] ON

	INSERT INTO [model].[Permissions] (Id, Name) VALUES (33, 'AdministerPatientDemographicsFields')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (34, 'AdministerPatientTags')

SET IDENTITY_INSERT [model].[Permissions] OFF
GO

INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 38, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied, 
	   re.ResourceId AS UserId, pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 33 -- 'AdministerPatientDemographicsFields'
GO

INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)

SELECT CASE WHEN SUBSTRING(re.ResourcePermissions, 45, 1) = 1
			THEN CONVERT(bit,0)
			ELSE CONVERT(bit,1)
	   END AS IsDenied, 
	   re.ResourceId AS UserId, pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id > 0
WHERE ResourceType <> 'R'
AND pe.Id = 34 -- 'AdministerPatientTags'
GO