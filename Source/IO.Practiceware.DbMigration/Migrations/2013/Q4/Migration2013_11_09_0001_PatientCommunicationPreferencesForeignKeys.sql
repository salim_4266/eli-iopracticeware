﻿-- Drop all Indexes and FKs
IF OBJECT_ID ('AuditPatientCommunicationPreferencesInsertTrigger','TR') IS NOT NULL
	DROP TRIGGER AuditPatientCommunicationPreferencesInsertTrigger

IF OBJECT_ID ('AuditPatientCommunicationPreferencesDeleteTrigger','TR') IS NOT NULL
	DROP TRIGGER AuditPatientCommunicationPreferencesDeleteTrigger

IF OBJECT_ID ('AuditPatientCommunicationPreferencesUpdateTrigger','TR') IS NOT NULL
	DROP TRIGGER AuditPatientCommunicationPreferencesUpdateTrigger

DECLARE @sql nvarchar(max)
SET @sql = ''
SELECT @sql = @sql + 'DROP INDEX ' + i.name + ' ON model.PatientCommunicationPreferences;'
FROM sys.indexes i
WHERE i.object_id = OBJECT_ID('model.PatientCommunicationPreferences') AND i.is_primary_key <> 1
EXEC(@sql)

SET @sql = ''
SELECT @sql = @sql + 'ALTER TABLE model.PatientCommunicationPreferences DROP CONSTRAINT ' + fk.name + ';'
FROM sys.foreign_keys fk
    INNER JOIN sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
    INNER JOIN sys.columns cpa ON fkc.parent_object_id = cpa.object_id AND fkc.parent_column_id = cpa.column_id
    INNER JOIN sys.columns cref ON fkc.referenced_object_id = cref.object_id AND fkc.referenced_column_id = cref.column_id
WHERE fk.parent_object_id = OBJECT_ID('model.PatientCommunicationPreferences')
EXEC(@sql)

IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL
	EXEC('DROP TABLE #NoAudit')

SELECT 1 AS Value INTO #NoAudit

-- We need to delete and re-insert all communication preferences because PatientAddresses migration changed Primary Key values.
DELETE FROM model.PatientCommunicationPreferences

-- Migrate existing data from dbo.PatientDemographics
INSERT INTO [model].[PatientCommunicationPreferences] (PatientId, PatientCommunicationTypeId, CommunicationMethodTypeId, PatientAddressId, IsAddressedToRecipient, __EntityType__)
SELECT insertPatientId, insertType, insertMethod, insertAddressId, insertIsAddressed, insertPCPType FROM
(SELECT 
pdo.PatientId AS insertPatientId, 
3 AS insertType /* Statement */, 
3 AS insertMethod /* Letter */,
CASE pdo.BillParty
	WHEN 'N' THEN
		CASE pdo.SecondBillParty
			WHEN 'N' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
			WHEN 'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.SecondPolicyPatientId ORDER BY pad.Id)
			ELSE (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
		END
	WHEN 'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PolicyPatientId ORDER BY pad.Id)
	ELSE 
		CASE pdo.SecondBillParty
			WHEN 'N' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
			WHEN 'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.SecondPolicyPatientId ORDER BY pad.Id)
			ELSE (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
		END	
END AS insertAddressId,
0 AS insertIsAddressed, 'PatientAddressCommunicationPreference' AS insertPCPType
FROM [dbo].[PatientDemographics] pdo) as insertTable1
WHERE insertAddressId IS NOT NULL

INSERT INTO [model].[PatientCommunicationPreferences] (PatientId, PatientCommunicationTypeId, CommunicationMethodTypeId, PatientAddressId, IsAddressedToRecipient, __EntityType__)
SELECT insertPatientId, insertType, insertMethod, insertAddressId, insertIsAddressed, insertPCPType FROM
(SELECT 
pdo.PatientId AS insertPatientId, 
1 AS insertType /* Recall */, 
3 AS insertMethod /* Letter */,
CASE pdo.BillParty
	WHEN 'N' THEN
		CASE pdo.SecondBillParty
			WHEN 'N' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
			WHEN 'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.SecondPolicyPatientId ORDER BY pad.Id)
			ELSE (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
		END
	WHEN 'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PolicyPatientId ORDER BY pad.Id)
	ELSE 
		CASE pdo.SecondBillParty
			WHEN 'N' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
			WHEN 'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.SecondPolicyPatientId ORDER BY pad.Id)
			ELSE (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = pdo.PatientId ORDER BY pad.Id)
		END	
END AS insertAddressId,
0 AS insertIsAddressed, 'PatientAddressCommunicationPreference' AS insertPCPType
FROM [dbo].[PatientDemographics] pdo) as insertTable1
WHERE insertAddressId IS NOT NULL

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [PatientId] in table 'PatientCommunicationPreferences'
IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientCommunicationPreferences]
ADD CONSTRAINT [FK_PatientPatientCommunicationPreference]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPatientCommunicationPreference'
CREATE INDEX [IX_FK_PatientPatientCommunicationPreference]
ON [model].[PatientCommunicationPreferences]
    ([PatientId]);
GO

-- Creating foreign key on [PatientAddressId] in table 'PatientCommunicationPreferences'
IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientAddresses]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientCommunicationPreferences]
ADD CONSTRAINT [FK_PatientAddressPatientAddressCommunicationPreference]
    FOREIGN KEY ([PatientAddressId])
    REFERENCES [model].[PatientAddresses]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientAddressPatientAddressCommunicationPreference'
CREATE INDEX [IX_FK_PatientAddressPatientAddressCommunicationPreference]
ON [model].[PatientCommunicationPreferences]
    ([PatientAddressId]);
GO

-- Creating foreign key on [PatientPhoneNumberId] in table 'PatientCommunicationPreferences'
IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientPhoneNumbers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientCommunicationPreferences]
ADD CONSTRAINT [FK_PatientPhoneNumberCommunicationPreferencePatientPhoneNumber]
    FOREIGN KEY ([PatientPhoneNumberId])
    REFERENCES [model].[PatientPhoneNumbers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPhoneNumberCommunicationPreferencePatientPhoneNumber'
CREATE INDEX [IX_FK_PatientPhoneNumberCommunicationPreferencePatientPhoneNumber]
ON [model].[PatientCommunicationPreferences]
    ([PatientPhoneNumberId]);
GO

-- Creating foreign key on [PatientEmailAddressId] in table 'PatientCommunicationPreferences'
IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientEmailAddresses]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientCommunicationPreferences]
ADD CONSTRAINT [FK_PatientEmailAddressCommunicationPreferencePatientEmailAddress]
    FOREIGN KEY ([PatientEmailAddressId])
    REFERENCES [model].[PatientEmailAddresses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientEmailAddressCommunicationPreferencePatientEmailAddress'
CREATE INDEX [IX_FK_PatientEmailAddressCommunicationPreferencePatientEmailAddress]
ON [model].[PatientCommunicationPreferences]
    ([PatientEmailAddressId]);
GO
