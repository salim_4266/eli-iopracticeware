﻿IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PAYMENTTYPE' AND SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '8')
BEGIN
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'WriteOff - 8'
	,N'D'
	,N'F'
	,N'INSRETRACT'
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)
END


IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'PAYMENTTYPE' AND SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = ']')
-- PracticeCodeTable PaymentTypes
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Information - ]'
	,N''
	,N'F'
	,N'INSRETRACT'
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)