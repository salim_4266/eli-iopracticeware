﻿IF OBJECT_ID(N'[model].[Tags]', 'U') IS NOT NULL	
BEGIN
	-- add column to HexColor
	IF NOT EXISTS(select * from sys.columns where Name = N'HexColor' and Object_ID = Object_ID(N'model.Tags')) 
	BEGIN
		ALTER TABLE model.Tags ADD [HexColor] nvarchar(max) NOT NULL DEFAULT('FFFFFF')
	END	
END
GO