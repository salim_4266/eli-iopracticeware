﻿-- Fixing column name.  It got changed somehow and broke the model.


IF OBJECT_ID(N'[model].[PatientCommunicationPreferences]', 'U') IS NOT NULL
BEGIN
	-- rename column to back to PatientEmailAddressId
	IF EXISTS (
			SELECT *
			FROM sys.columns
			WHERE NAME = N'EmailAddressId'
				AND Object_ID = Object_ID(N'model.PatientCommunicationPreferences')
			)
	BEGIN
		EXEC sp_rename '[model].[PatientCommunicationPreferences].[EmailAddressId]'
			,'PatientEmailAddressId'
			,'COLUMN';
	END
END
GO


