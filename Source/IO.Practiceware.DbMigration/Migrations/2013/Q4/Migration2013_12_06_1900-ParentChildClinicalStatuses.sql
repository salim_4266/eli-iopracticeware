﻿-- if a clinical condition is deleted then it deletes the top row ('Diabetes-family') but leaves
-- the status of the child row ('son-diabetes') marked as Active.
-- This makes sure that child rows are given the status of their parent

SELECT MIN(pc.ClinicalId) AS ChildId
	,MIN(pc2.ClinicalId) AS ParentId
	,pc.Status AS ChildStatus
	,pc2.Status AS ParentStatus
INTO #StatusCheck
FROM dbo.PatientClinical pc
INNER JOIN dbo.PatientClinical pc2 ON  pc.PatientId = pc2.PatientId 
	AND dbo.ExtractTextValue('-', pc.FindingDetail, '=') = dbo.ExtractTextValue('(', '(' + pc2.FindingDetail, '-FAM')
	AND (pc2.Symptom = '/FAMILY MEMBER - - - - CATARACT,  RETINAL DISEASE,  GLAUCOMA,  CANCER OF THE EYE,  BLINDNESS,  OTHER' 
		OR pc2.Symptom = '/FAMILY MEMBER - - - - DIABETES,  CANCER,  BLOOD PRESSURE,  SICKLE CELL,  MIGRAINE')
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
WHERE (pc.Symptom = '?WHICH FAMILY' OR pc.Symptom = '?TYPE OF FAMILY MEDICAL HISTORY')
AND pc.ClinicalType ='H'
GROUP BY pc.FindingDetail, ap.ResourceId1,ap.AppDate,pc.Status,pc2.Status

UNION ALL

SELECT MIN(pc.ClinicalId) AS ChildId
	,MIN(pc2.ClinicalId) AS ParentId
	,pc.Status AS ChildStatus
	,pc2.Status AS ParentStatus
FROM dbo.PatientClinical pc
INNER JOIN dbo.PatientClinical pc2 ON pc.PatientId = pc2.PatientId 
	AND dbo.ExtractTextValue('-', pc.FindingDetail, '=') = dbo.ExtractTextValue('*', pc2.FindingDetail, '-FAM')
	AND (pc2.Symptom = '/FAMILY MEMBER - - - - CATARACT,  RETINAL DISEASE,  GLAUCOMA,  CANCER OF THE EYE,  BLINDNESS,  OTHER' 
		OR pc2.Symptom = '/FAMILY MEMBER - - - - DIABETES,  CANCER,  BLOOD PRESSURE,  SICKLE CELL,  MIGRAINE')
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
WHERE  (pc.Symptom = '?WHICH FAMILY' OR pc.Symptom = '?TYPE OF FAMILY MEDICAL HISTORY')
AND pc.ClinicalType = 'C'
GROUP BY pc.FindingDetail, ap.ResourceId1,ap.AppDate,pc.Status,pc2.Status

UPDATE PatientClinical SET Status = ParentStatus
FROM PatientClinical pc
INNER JOIN #StatusCheck sc
ON pc.ClinicalId = sc.ChildId
AND ChildStatus <> ParentStatus
AND ChildId > ParentId