﻿INSERT INTO [dbo].[Immunizations]
([ImmName]
,[ImmDuration]
,[ImmCPT]
,[SortNo]
,[ImmCode]
,[Abbreviation])
SELECT 
'Influenza virus vaccine' AS ImmName
,NULL AS ImMDuration
,NULL AS ImmCPT
,NULL AS SortNo
,88 AS ImmCode
,'Influenza virus' AS Abbreviation
FROM dbo.Immunizations imm

DECLARE @InsertedId INT
SELECT @InsertedId = SCOPE_IDENTITY()

DECLARE @SnomedExternalSystemId INT
SELECT @SnomedExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'CvxVaccineAdministered'

DECLARE @ClinicalProcedureId INT
SELECT @ClinicalProcedureId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'Vaccine'

DECLARE @SnomedExternalSystemIdClinicalProcedure INT
SELECT @SnomedExternalSystemIdClinicalProcedure = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @SnomedExternalSystemId
	AND Name = 'Vaccine'

DECLARE @MapToId INT
SELECT @MapToId = CVXCode FROM cvx.VaccineAdministered WHERE CVXCode = 88

INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])VALUES
(@ClinicalProcedureId,@InsertedId,@SnomedExternalSystemIdClinicalProcedure,@MapToId)
