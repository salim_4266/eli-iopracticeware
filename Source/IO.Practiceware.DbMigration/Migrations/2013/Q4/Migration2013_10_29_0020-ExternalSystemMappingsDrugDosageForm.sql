﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[DrugDosageForms]') AND type in (N'U'))
DROP TABLE [model].[DrugDosageForms]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[DrugDosageForms]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[DrugDosageForms](
	[Id] INT NOT NULL IDENTITY (1,1),
	[Name] [nvarchar](2000) NULL,
	[DisplayName] [nvarchar](2000) NULL,
	[OrdinalId] [int] NULL DEFAULT 0
)
END
GO

-- Creating primary key on [Id] in table 'DrugDosageForms'
ALTER TABLE [model].[DrugDosageForms]
ADD CONSTRAINT [PK_DrugDosageForms]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'Addl Sig', N'Addl Sig', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'application', N'application', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'capsule', N'capsule', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'drop', N'drop', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'gm', N'gram', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'lozenge', N'lozenge', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'mL', N'milliliter', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'patch', N'patch', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'pill', N'pill', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'puff', N'puff', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'squirt', N'squirt', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'suppository', N'suppository', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'tablet', N'tablet', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'troche', N'troche', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'unit', N'unit', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'syringe', N'syringe', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'package', N'package', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'mcg', N'mcg', 0)
GO
INSERT [model].[DrugDosageForms] ([Name], [DisplayName], [OrdinalId]) VALUES (N'mg', N'mg', 0)
GO

DECLARE @DrugDosageFormId INT
DECLARE @NewCropExternalSystemId INT
DECLARE @NciThesaurusExternalSystemId INT

SELECT @DrugDosageFormId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'DrugDosageForm'
SELECT @NewCropExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'NewCrop'
SELECT @NciThesaurusExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'NciThesaurus'

DECLARE @NewCropExternalSystemIdDrugDosageForm INT
DECLARE @NciThesaurusExternalSystemIdDrugDosageForm INT

SELECT @NewCropExternalSystemIdDrugDosageForm = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @NewCropExternalSystemId AND Name = 'DrugDosageForm'
SELECT @NciThesaurusExternalSystemIdDrugDosageForm = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @NciThesaurusExternalSystemId AND Name = 'DrugDosageForm'

;WITH CTE AS(
SELECT
@DrugDosageFormId AS PracticeRepositoryEntityId
,DDF.Id AS PracticeRepositoryEntityKey
,@NewCropExternalSystemIdDrugDosageForm AS ExternalSystemEntityId
,CONVERT(NVARCHAR,extDDF.Id) AS ExternalSystemEntityKey
FROM model.DrugDosageForms DDF
LEFT JOIN [newCrop].[DrugDosageForm] extDDF ON
	(DDF.DisplayName LIKE '%'+extDDF.DisplayName+'%' OR 
	DDF.Name LIKE '%'+extDDF.Name+'%')
UNION ALL
SELECT
@DrugDosageFormId AS PracticeRepositoryEntityId
,DDF.Id AS PracticeRepositoryEntityKey
,@NciThesaurusExternalSystemIdDrugDosageForm AS ExternalSystemEntityId
,extDDF.Code AS ExternalSystemEntityKey
FROM model.DrugDosageForms DDF
LEFT JOIN [nci].[DosageForms] extDDF ON
	DDF.DisplayName LIKE '%'+extDDF.SPLAcceptableTerm+'%'
)
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId,PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT PracticeRepositoryEntityId,PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey FROM (
	SELECT 
	CTE.PracticeRepositoryEntityId
	,CTE.PracticeRepositoryEntityKey
	,CTE.ExternalSystemEntityId
	,CASE 
		WHEN ExternalSystemEntityKey IS NULL AND ExternalSystemEntityId = @NciThesaurusExternalSystemIdDrugDosageForm AND DDF.DisplayName = 'Application'
			THEN 'C42966' --OINTMENT
		WHEN ExternalSystemEntityKey IS NULL AND ExternalSystemEntityId = @NciThesaurusExternalSystemIdDrugDosageForm AND DDF.DisplayName = 'Drop'
			THEN 'C60992' --SOLUTION/ DROPS
		WHEN ExternalSystemEntityKey IS NULL AND ExternalSystemEntityId = @NciThesaurusExternalSystemIdDrugDosageForm AND DDF.DisplayName = 'Puff'
			THEN 'C42944' --INHALANT 
		WHEN ExternalSystemEntityKey IS NULL AND ExternalSystemEntityId = @NciThesaurusExternalSystemIdDrugDosageForm AND DDF.DisplayName = 'Squirt'
			THEN 'C42947' --IRRIGANT
		ELSE ExternalSystemEntityKey
	END AS ExternalSystemEntityKey
	FROM CTE 
	JOIN model.DrugDosageForms DDF ON DDF.Id = CTE.PracticeRepositoryEntityKey
)v
WHERE  v.ExternalSystemEntityKey IS NOT NULL