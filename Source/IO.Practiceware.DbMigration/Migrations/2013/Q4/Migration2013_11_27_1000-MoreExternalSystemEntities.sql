﻿
IF NOT EXISTS (SELECT * FROM model.PracticeRepositoryEntities WHERE Name = 'FamilyRelationship')
BEGIN
	INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName)
	VALUES (38, 'FamilyRelationship', 'Id')
END


IF NOT EXISTS (SELECT * FROM model.ExternalSystems WHERE Name = 'ExternalLaboratory')
BEGIN
	INSERT INTO model.ExternalSystems (Name) 
	VALUES ('ExternalLaboratory') 
END

IF NOT EXISTS (SELECT * FROM  model.ExternalSystemEntities ese INNER JOIN model.ExternalSystems es ON ese.ExternalSystemId = es.Id WHERE ese.Name = 'Contact' AND es.Name = 'ExternalLaboratory')
BEGIN
	INSERT INTO model.ExternalSystemEntities (ExternalSystemId, Name) 
	SELECT Id AS ExternalSystemId, 'Contact' AS Name FROM model.ExternalSystems WHERE Name = 'ExternalLaboratory'
END

IF NOT EXISTS (SELECT * FROM  model.ExternalSystemEntities ese INNER JOIN model.ExternalSystems es ON ese.ExternalSystemId = es.Id WHERE ese.Name = 'Physician' AND es.Name = 'ExternalLaboratory')
BEGIN
	INSERT INTO model.ExternalSystemEntities (ExternalSystemId, Name) 
	SELECT Id AS ExternalSystemId, 'Physician' AS Name FROM model.ExternalSystems WHERE Name = 'ExternalLaboratory'
END

IF NOT EXISTS (SELECT * FROM  model.ExternalSystemEntities ese INNER JOIN model.ExternalSystems es ON ese.ExternalSystemId = es.Id WHERE ese.Name = 'FamilyRelationship' AND es.Name = 'SnomedCt')
BEGIN
	INSERT INTO model.ExternalSystemEntities (ExternalSystemId, Name) 
	SELECT Id AS ExternalSystemId, 'FamilyRelationship' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
END
