﻿INSERT INTO model.PracticeRepositoryEntities(Id, Name, KeyPropertyName)
SELECT 39, 'Encounter', 'Id'
UNION
SELECT 40, 'EncounterDiagnosticTestOrder', 'Id'
UNION
SELECT 41, 'EncounterLaboratoryTestOrder', 'Id'
UNION
SELECT 42, 'EncounterTreatmentGoalAndInstruction', 'Id'
UNION
SELECT 43, 'PatientAllergen', 'Id'
UNION
SELECT 44, 'PatientAllergenDetail', 'Id'
UNION
SELECT 45, 'PatientBloodPressure', 'Id'
UNION
SELECT 46, 'PatientCognitiveStatusAssessment', 'Id'
UNION
SELECT 47, 'PatientFunctionalStatusAssessment', 'Id'
UNION
SELECT 48, 'PatientHeightAndWeight', 'Id'
UNION
SELECT 49, 'PatientLaboratoryTestResult', 'Id'
UNION
SELECT 50, 'PatientMedication', 'Id'
UNION
SELECT 51, 'PatientProblemConcernLevel', 'Id'
UNION
SELECT 52, 'PatientProcedurePerformed', 'Id'
UNION
SELECT 53, 'PatientSmokingStatus', 'Id'
UNION
SELECT 54, 'PatientVaccination', 'Id'
UNION
SELECT 55, 'TreatmentGoalAndInstruction', 'Id'
UNION
SELECT 56, 'EncounterTransitionOfCareOrder', 'Id'
UNION
SELECT 57, 'EncounterProcedureOrder', 'Id'
UNION
SELECT 58, 'EncounterAdministeredMedication', 'Id'
UNION
SELECT 59, 'PatientDiagnosisDetail', 'Id'
UNION
SELECT 60, 'PatientDiagnosis', 'Id'
UNION 
SELECT 61, 'PatientDiagnosticTestPerformed', 'Id'