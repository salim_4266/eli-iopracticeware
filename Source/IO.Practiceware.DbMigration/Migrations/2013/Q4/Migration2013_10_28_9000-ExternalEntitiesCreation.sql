﻿INSERT INTO model.ExternalSystems (Name) VALUES ('Cpt4')
INSERT INTO model.ExternalSystems (Name) VALUES ('CvxVaccineAdministered')  
INSERT INTO model.ExternalSystems (Name) VALUES ('FdbDrug')
INSERT INTO model.ExternalSystems (Name) VALUES ('FdbAllergy')
INSERT INTO model.ExternalSystems (Name) VALUES ('Icd10') 
INSERT INTO model.ExternalSystems (Name) VALUES ('Icd9')
INSERT INTO model.ExternalSystems (Name) VALUES ('IOLegacy')
INSERT INTO model.ExternalSystems (Name) VALUES ('Loinc') 
INSERT INTO model.ExternalSystems (Name) VALUES ('NciThesaurus') 
INSERT INTO model.ExternalSystems (Name) VALUES ('NewCrop') 
INSERT INTO model.ExternalSystems (Name) VALUES ('RxNorm')
INSERT INTO model.ExternalSystems (Name) VALUES ('SnomedCt') 
INSERT INTO model.ExternalSystems (Name) VALUES ('UnifiedCodeForUnitsOfMeasure') 
INSERT INTO model.ExternalSystems (Name) VALUES ('UnifiedSampleIdentifier')  
INSERT INTO model.ExternalSystems (Name) VALUES ('UniqueIngredientIdentifier')  

INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (14, 'Allergen', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (15, 'AllergenReactionType', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (16, 'AllergySource', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (17, 'AxisQualifier', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (18, 'ClinicalAttribute', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (19, 'ClinicalCondition', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (20, 'ClinicalConditionStatus', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (21, 'ClinicalProcedure', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (22, 'ClinicalQualifier', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (23, 'DrugDosageAction', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (24, 'DrugDosageForm', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (25, 'DrugDosageNumber', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (26, 'DrugDispenseForm', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (27, 'LaboratoryTest', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (28, 'Medication', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (29, 'ObservableEntity', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (30, 'RelevancyQualifier', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (31, 'RouteOfAdministration', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (32, 'SmokingCondition', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (33, 'TreatmentGoal', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (34, 'UnitOfMeasurement', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (35, 'Vaccine', 'Id')
INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName) VALUES (36, 'AccuracyQualifier', 'Id')

INSERT INTO model.ExternalSystemEntities (ExternalSystemId, Name) 
SELECT Id AS ExternalSystemId, 'AccuracyQualifier' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'Allergen' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'AllergenReactionType' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'AllergySource' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'AxisQualifier' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'ClinicalAttribute' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'ClinicalCondition' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'ClinicalConditionStatus' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'ClinicalProcedure' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'ClinicalQualifier' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'ObservableEntity' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'RelevancyQualifier' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'SmokingCondition' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'
UNION ALL SELECT Id AS ExternalSystemId, 'TreatmentGoal' AS Name FROM model.ExternalSystems WHERE Name = 'SnomedCt'

UNION ALL SELECT Id AS ExternalSystemId, 'ClinicalCondition' AS Name FROM model.ExternalSystems WHERE Name = 'Cpt4'

UNION ALL SELECT Id AS ExternalSystemId, 'ClinicalCondition' AS Name FROM model.ExternalSystems WHERE Name = 'ICD10'

UNION ALL SELECT Id AS ExternalSystemId, 'ClinicalCondition' AS Name FROM model.ExternalSystems WHERE Name = 'ICD9'

UNION ALL SELECT Id AS ExternalSystemId, 'ClinicalCondition' AS Name FROM model.ExternalSystems WHERE Name = 'IoLegacy'

UNION ALL SELECT Id AS ExternalSystemId, 'DrugDosageForm' AS Name FROM model.ExternalSystems WHERE Name = 'NciThesaurus'
UNION ALL SELECT Id AS ExternalSystemId, 'RouteOfAdministration' AS Name FROM model.ExternalSystems WHERE Name = 'NciThesaurus'

UNION ALL SELECT Id AS ExternalSystemId, 'DrugDosageAction' AS Name FROM model.ExternalSystems WHERE Name = 'NewCrop'
UNION ALL SELECT Id AS ExternalSystemId, 'DrugDosageForm' AS Name FROM model.ExternalSystems WHERE Name = 'NewCrop'
UNION ALL SELECT Id AS ExternalSystemId, 'DrugDosageNumber' AS Name FROM model.ExternalSystems WHERE Name = 'NewCrop'
UNION ALL SELECT Id AS ExternalSystemId, 'RouteOfAdministration' AS Name FROM model.ExternalSystems WHERE Name = 'NewCrop'
UNION ALL SELECT Id AS ExternalSystemId, 'DrugDispenseForm' AS Name FROM model.ExternalSystems WHERE Name = 'NewCrop'

UNION ALL SELECT Id AS ExternalSystemId, 'LaboratoryTest' AS Name FROM model.ExternalSystems WHERE Name = 'Loinc'

UNION ALL SELECT Id AS ExternalSystemId, 'Allergen' AS Name FROM model.ExternalSystems WHERE Name = 'RxNorm'
UNION ALL SELECT Id AS ExternalSystemId, 'Medication' AS Name FROM model.ExternalSystems WHERE Name = 'RxNorm'

UNION ALL SELECT Id AS ExternalSystemId, 'Allergen' AS Name FROM model.ExternalSystems WHERE Name = 'FdbAllergy'
UNION ALL SELECT Id AS ExternalSystemId, 'Medication' AS Name FROM model.ExternalSystems WHERE Name = 'FdbDrug'
UNION ALL SELECT Id AS ExternalSystemId, 'Specimen' AS Name FROM model.ExternalSystems WHERE Name = 'UnifiedSampleIdentifier'
UNION ALL SELECT Id AS ExternalSystemId, 'Allergen' AS Name FROM model.ExternalSystems WHERE Name = 'UniqueIngredientIdentifier'
UNION ALL SELECT Id AS ExternalSystemId, 'UnitOfMeasurement' AS Name FROM model.ExternalSystems WHERE Name = 'UnifiedCodeForUnitsOfMeasure'
UNION ALL SELECT Id AS ExternalSystemId, 'Vaccine' AS Name FROM model.ExternalSystems WHERE Name = 'CvxVaccineAdministered'