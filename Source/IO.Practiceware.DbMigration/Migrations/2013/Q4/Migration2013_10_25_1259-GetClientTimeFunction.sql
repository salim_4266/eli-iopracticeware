﻿-- Create a function which attempts to return GETDATE() in the context of the client who is currently logged into the software.
-- We achieve this by setting the offset value in a temp table before any calls are made to the database.  This temp table data
-- can then be accessed by the this function to perform the correct conversion.



IF EXISTS (SELECT Name FROM Sys.Synonyms WHERE Name = 'UserContext')
	DROP SYNONYM model.usercontext;
GO

CREATE SYNONYM [model].[UserContext] FOR #UserContext

GO

IF EXISTS(SELECT name FROM sysobjects WHERE name = 'GetClientDateNow')
	DROP FUNCTION model.GetClientDateNow
GO

CREATE FUNCTION [model].[GetClientDateNow]()
RETURNS DATETIME
AS

BEGIN
	DECLARE @DateNow AS DATETIME
	DECLARE @ClientTimeOffset AS INT

	IF OBJECT_ID('tempdb..#UserContext') IS NOT NULL
	BEGIN
		SELECT @ClientTimeOffset = ClientTimeZoneOffset FROM model.UserContext;

		IF @ClientTimeOffset IS NOT NULL
		BEGIN
			SET @DateNow = DATEADD(minute, @ClientTimeOffset, GETUTCDATE())
			RETURN @DateNow
		END		
	END

	RETURN GETDATE();
END

GO


-- Replace calls to GETDATE() with GETUTCDATE() in ExternalSystemMessage related triggers

DECLARE commands CURSOR FOR

SELECT def FROM (
SELECT REPLACE(REPLACE(OBJECT_DEFINITION(o.object_id), 'CREATE TRIGGER', 'ALTER TRIGGER'), 'GETDATE()', 'GETUTCDATE()') AS def,
		o.object_id, o.schema_id, o.type, o.name, p.name AS Parent
FROM sys.objects o 
INNER JOIN sys.objects p ON o.parent_object_id = p.object_id
WHERE o.name in ('AppointmentExternalSystemMessagesTrigger',  'PatientDemographicsExternalSystemMessagesTrigger', 'PatientsExternalSystemMessagesTrigger', 'PracticeActivityCheckoutExternalSystemMessagesTrigger')
	AND OBJECT_DEFINITION(o.object_id) LIKE '%GETDATE()%'
) as d1

DECLARE @cmd varchar(max)
OPEN commands
FETCH NEXT FROM commands INTO @cmd
WHILE @@FETCH_STATUS=0
BEGIN
  EXEC(@cmd)
  FETCH NEXT FROM commands INTO @cmd
END
CLOSE commands
DEALLOCATE commands
GO





-- Replace calls to GETDATE() with model.GetClientDateNow() in some existing triggers

DECLARE commands CURSOR FOR

SELECT def FROM (
SELECT REPLACE(REPLACE(OBJECT_DEFINITION(o.object_id), 'CREATE TRIGGER', 'ALTER TRIGGER'), 'GETDATE()', 'model.GetClientDateNow()') AS def,
		o.object_id, o.schema_id, o.type, o.name, p.name AS Parent
FROM sys.objects o 
INNER JOIN sys.objects p ON o.parent_object_id = p.object_id
WHERE o.name in ('OnPatientReceivableInsert', 'ServiceTransactionsInsert')
	AND OBJECT_DEFINITION(o.object_id) LIKE '%GETDATE()%'
) as d1

DECLARE @cmd varchar(max)
OPEN commands
FETCH NEXT FROM commands INTO @cmd
WHILE @@FETCH_STATUS=0
BEGIN
  EXEC(@cmd)
  FETCH NEXT FROM commands INTO @cmd
END
CLOSE commands
DEALLOCATE commands
GO