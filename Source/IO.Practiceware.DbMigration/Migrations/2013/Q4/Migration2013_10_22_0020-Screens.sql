﻿IF NOT EXISTS (SELECT * FROM sys.columns sc
INNER JOIN sys.tables st ON sc.object_id = st.object_id
WHERE st.name = 'Screens' AND sc.name = 'ScreenTypeId')

BEGIN 

ALTER TABLE model.Screens
ADD ScreenTypeId INT NULL, IsDeactivated BIT NOT NULL DEFAULT 0;

END
