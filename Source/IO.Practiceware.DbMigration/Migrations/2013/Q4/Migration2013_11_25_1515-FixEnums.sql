﻿IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Name = 'EditPolicyholder' AND Id = 35)
BEGIN 
SET IDENTITY_INSERT model.Permissions ON

INSERT INTO model.Permissions (Id, Name)
VALUES (35, 'EditPolicyholder')

SET IDENTITY_INSERT model.Permissions OFF
END

UPDATE model.Permissions
SET Name = 'ApplyTemplatesToSchedules'
WHERE Name = 'ApplyTemplatesSchedule'

UPDATE model.Permissions
SET Name = 'CancelAppointment'
WHERE Name = 'ExternalContacts'

-- Allow modifications and identity inserts into ExternalSystemMessageTypes
SELECT 1 AS Value INTO #NoCheck
SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] ON

-- In an earlier version of 'Migration2013_09_30_0000' named it improperly. Updating on existing databases
UPDATE model.ExternalSystemMessageTypes
SET Name = 'EncounterClinicalSummaryCCDA'
WHERE Id = 27 AND Name = 'EncounterClinicalSummaryCDA'
UPDATE model.ExternalSystemMessageTypes
SET Name = 'PatientTransitionOfCareCCDA'
WHERE Id = 28 AND Name = 'PatientTransitionOfCareCDA'

-- There was a bug in "Migration2013_09_30_0000-CDAExternalSystemMessageTypes" migration which would check on Id=28 to insert this record
IF NOT EXISTS(SELECT * FROM model.ExternalSystemMessageTypes WHERE Id = 29)
BEGIN
	INSERT INTO model.ExternalSystemMessageTypes(Id, Name, [Description], IsOutbound)
	VALUES (29, 'PatientTransitionOfCareCCDA', 'Patient Transition of Care C-CDA', 0)
END

-- Disable
SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] OFF
DROP TABLE #NoCheck