IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[ThirdFormsLanguage]') AND type in (N'U'))
DROP TABLE [df].[ThirdFormsLanguage]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[ThirdFormsControls]') AND type in (N'U'))
DROP TABLE [df].[ThirdFormsControls]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[ThirdForms]') AND type in (N'U'))
DROP TABLE [df].[ThirdForms]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[SecondaryFormsLanguage]') AND type in (N'U'))
DROP TABLE [df].[SecondaryFormsLanguage]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[SecondaryFormsControls]') AND type in (N'U'))
DROP TABLE [df].[SecondaryFormsControls]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[SecondaryForms]') AND type in (N'U'))
DROP TABLE [df].[SecondaryForms]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[Questions]') AND type in (N'U'))
DROP TABLE [df].[Questions]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[FourthFormsLanguage]') AND type in (N'U'))
DROP TABLE [df].[FourthFormsLanguage]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[FourthFormsControls]') AND type in (N'U'))
DROP TABLE [df].[FourthFormsControls]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[FourthForms]') AND type in (N'U'))
DROP TABLE [df].[FourthForms]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[FormsLanguage]') AND type in (N'U'))
DROP TABLE [df].[FormsLanguage]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[FormsControls]') AND type in (N'U'))
DROP TABLE [df].[FormsControls]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[Forms]') AND type in (N'U'))
DROP TABLE [df].[Forms]
GO
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'df')
DROP SCHEMA [df]
GO
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'df')
EXEC sys.sp_executesql N'CREATE SCHEMA [df]'

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[Forms]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[Forms](
	[FormId] [int] NOT NULL,
	[FormName] [nvarchar](32) NULL,
	[FormInterface] [nvarchar](1) NULL,
	[BackColor] [int] NULL,
	[ForeColor] [int] NULL,
	[WindowState] [int] NULL,
	[ExitButtonY] [bit] NOT NULL,
	[ExitButton] [nvarchar](1) NULL,
	[ApplyButtonY] [bit] NOT NULL,
	[ApplyButton] [nvarchar](1) NULL,
	[DontKnowButtonY] [bit] NOT NULL,
	[DontKnowButton] [nvarchar](1) NULL,
	[HelpButtonY] [bit] NOT NULL,
	[HelpButton] [nvarchar](1) NULL,
	[NoToAllButtonY] [bit] NOT NULL,
	[NoToAllButton] [nvarchar](1) NULL,
	[StaticLabel] [nvarchar](32) NULL,
	[Question] [nvarchar](128) NULL,
	[QuestionType] [nvarchar](1) NULL,
	[FormHeight] [int] NULL,
	[FormWidth] [int] NULL,
	[EditStyleY] [bit] NOT NULL,
	[EditStyle] [nvarchar](1) NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[FormsControls]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[FormsControls](
	[ControlId] [int] NOT NULL,
	[FormId] [int] NULL,
	[ControlName] [nvarchar](64) NULL,
	[ControlType] [nvarchar](1) NULL,
	[BackColor] [int] NULL,
	[ForeColor] [int] NULL,
	[TabIndex] [int] NULL,
	[ResponseLength] [float] NULL,
	[ControlVisibleY] [bit] NOT NULL,
	[ControlVisible] [nvarchar](1) NULL,
	[ControlTemplateName] [nvarchar](64) NULL,
	[ControlFont] [nvarchar](32) NULL,
	[ControlTop] [int] NULL,
	[ControlHeight] [int] NULL,
	[ControlLeft] [int] NULL,
	[ControlWidth] [int] NULL,
	[InferenceEngineChoiceName] [nvarchar](64) NULL,
	[ControlText] [nvarchar](64) NULL,
	[DrugQuestionY] [bit] NOT NULL,
	[DrugQuestion] [nvarchar](1) NULL,
	[ControlAlternateText] [nvarchar](64) NULL,
	[ControlAlternateTrigger] [nvarchar](64) NULL,
	[ControlVisibleAfterTriggerY] [bit] NOT NULL,
	[ControlVisibleAfterTrigger] [nvarchar](1) NULL,
	[ConfirmLingo] [nvarchar](64) NULL,
	[DoctorLingo] [nvarchar](64) NULL,
	[DecimalDefault] [nvarchar](1) NULL,
	[ICDAlias1] [nvarchar](32) NULL,
	[ICDAlias2] [nvarchar](32) NULL,
	[ICDAlias3] [nvarchar](32) NULL,
	[ICDAlias4] [nvarchar](32) NULL,
	[Exclusion] [nvarchar](1) NULL,
	[LetterTranslation] [nvarchar](255) NULL,
	[OtherLtrTrans] [nvarchar](255) NULL,
	[Ros] [nvarchar](1) NULL,
	[Increment] [nvarchar](5) NULL,
	[IgnoreIt] [nvarchar](1) NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[FormsLanguage]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[FormsLanguage](
	[LanguageId] [int] NOT NULL,
	[Language] [nvarchar](32) NULL,
	[ControlId] [int] NULL,
	[ControlText] [nvarchar](64) NULL,
	[ConfirmLingo] [nvarchar](64) NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[FourthForms]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[FourthForms](
	[FourthFormId] [int] NOT NULL,
	[FormId] [int] NULL,
	[FormControlName] [nvarchar](64) NULL,
	[Question] [nvarchar](64) NULL,
	[QuestionOrder] [nvarchar](5) NULL,
	[BackColor] [int] NULL,
	[ForeColor] [int] NULL,
	[WindowState] [int] NULL,
	[ExitButtonY] [bit] NOT NULL,
	[ExitButton] [nvarchar](1) NULL,
	[ApplyButtonY] [bit] NOT NULL,
	[ApplyButton] [nvarchar](1) NULL,
	[DontKnowButtonY] [bit] NOT NULL,
	[DontKnowButton] [nvarchar](1) NULL,
	[HelpButtonY] [bit] NOT NULL,
	[HelpButton] [nvarchar](1) NULL,
	[NoToAllButtonY] [bit] NOT NULL,
	[NoToAllButton] [nvarchar](1) NULL,
	[StaticLabel] [nvarchar](32) NULL,
	[InferenceQuestion] [nvarchar](64) NULL,
	[InferenceType] [nvarchar](1) NULL,
	[FormHeight] [int] NULL,
	[FormWidth] [int] NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[FourthFormsControls]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[FourthFormsControls](
	[FourthControlId] [int] NOT NULL,
	[FourthFormId] [int] NULL,
	[ControlName] [nvarchar](64) NULL,
	[ControlType] [nvarchar](1) NULL,
	[BackColor] [int] NULL,
	[ForeColor] [int] NULL,
	[TabIndex] [int] NULL,
	[ResponseLength] [int] NULL,
	[ControlVisibleY] [bit] NOT NULL,
	[ControlVisible] [nvarchar](1) NULL,
	[ControlTemplateName] [nvarchar](64) NULL,
	[ControlFont] [nvarchar](32) NULL,
	[ControlTop] [int] NULL,
	[ControlHeight] [int] NULL,
	[ControlLeft] [int] NULL,
	[ControlWidth] [int] NULL,
	[InferenceEngineChoiceName] [nvarchar](64) NULL,
	[ControlText] [nvarchar](64) NULL,
	[DrugQuestionY] [bit] NOT NULL,
	[DrugQuestion] [nvarchar](1) NULL,
	[ControlAlternateText] [nvarchar](64) NULL,
	[ControlAlternateTrigger] [nvarchar](64) NULL,
	[ControlVisibleAfterTriggerY] [bit] NOT NULL,
	[ControlVisibleAfterTrigger] [nvarchar](1) NULL,
	[ConfirmLingo] [nvarchar](64) NULL,
	[DoctorLingo] [nvarchar](64) NULL,
	[DecimalDefault] [nvarchar](1) NULL,
	[ICDAlias1] [nvarchar](32) NULL,
	[ICDAlias2] [nvarchar](32) NULL,
	[ICDAlias3] [nvarchar](32) NULL,
	[ICDAlias4] [nvarchar](32) NULL,
	[Exclusion] [nvarchar](1) NULL,
	[LetterTranslation] [nvarchar](128) NULL,
	[OtherLtrTrans] [nvarchar](128) NULL,
	[Ros] [nvarchar](1) NULL,
	[Increment] [nvarchar](5) NULL,
	[IgnoreIt] [nvarchar](1) NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[FourthFormsLanguage]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[FourthFormsLanguage](
	[LanguageId] [int] NOT NULL,
	[Language] [nvarchar](32) NULL,
	[ControlId] [int] NULL,
	[ControlText] [nvarchar](64) NULL,
	[ConfirmLingo] [nvarchar](64) NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[Questions]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[Questions](
	[ClassId] [int] NOT NULL,
	[QuestionParty] [nvarchar](1) NULL,
	[Question] [nvarchar](128) NULL,
	[QuestionOrder] [nvarchar](5) NULL,
	[QuestionSet] [nvarchar](32) NULL,
	[ControlName] [nvarchar](64) NULL,
	[InferenceEngineQuestion] [nvarchar](64) NULL,
	[InferenceEngineQuestionType] [nvarchar](1) NULL,
	[Family] [int] NULL,
	[Timeout] [int] NULL,
	[MergeId] [int] NULL,
	[Highlight] [nvarchar](1) NULL,
	[ProcOrTest] [nvarchar](1) NULL,
	[Followup] [nvarchar](5) NULL,
	[PrintOrder] [int] NULL,
	[FormatMaskId] [int] NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[SecondaryForms]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[SecondaryForms](
	[SecondaryFormId] [int] NOT NULL,
	[FormId] [int] NULL,
	[FormControlName] [nvarchar](64) NULL,
	[Question] [nvarchar](64) NULL,
	[QuestionOrder] [nvarchar](5) NULL,
	[BackColor] [int] NULL,
	[ForeColor] [int] NULL,
	[WindowState] [int] NULL,
	[ExitButtonY] [bit] NOT NULL,
	[ExitButton] [nvarchar](1) NULL,
	[ApplyButtonY] [bit] NOT NULL,
	[ApplyButton] [nvarchar](1) NULL,
	[DontKnowButtonY] [bit] NOT NULL,
	[DontKnowButton] [nvarchar](1) NULL,
	[HelpButtonY] [bit] NOT NULL,
	[HelpButton] [nvarchar](1) NULL,
	[NoToAllButtonY] [bit] NOT NULL,
	[NoToAllButton] [nvarchar](1) NULL,
	[StaticLabel] [nvarchar](32) NULL,
	[InferenceQuestion] [nvarchar](64) NULL,
	[InferenceType] [nvarchar](1) NULL,
	[FormHeight] [int] NULL,
	[FormWidth] [int] NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[SecondaryFormsControls]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[SecondaryFormsControls](
	[SecondaryControlId] [int] NOT NULL,
	[SecondaryFormId] [int] NULL,
	[ControlName] [nvarchar](64) NULL,
	[ControlType] [nvarchar](1) NULL,
	[BackColor] [int] NULL,
	[ForeColor] [int] NULL,
	[TabIndex] [int] NULL,
	[ResponseLength] [int] NULL,
	[ControlVisibleY] [bit] NOT NULL,
	[ControlVisible] [nvarchar](1) NULL,
	[ControlTemplateName] [nvarchar](64) NULL,
	[ControlFont] [nvarchar](32) NULL,
	[ControlTop] [int] NULL,
	[ControlHeight] [int] NULL,
	[ControlLeft] [int] NULL,
	[ControlWidth] [int] NULL,
	[InferenceEngineChoiceName] [nvarchar](64) NULL,
	[ControlText] [nvarchar](64) NULL,
	[DrugQuestionY] [bit] NOT NULL,
	[DrugQuestion] [nvarchar](1) NULL,
	[ControlAlternateText] [nvarchar](64) NULL,
	[ControlAlternateTrigger] [nvarchar](64) NULL,
	[ControlVisibleAfterTriggerY] [bit] NOT NULL,
	[ControlVisibleAfterTrigger] [nvarchar](1) NULL,
	[ConfirmLingo] [nvarchar](64) NULL,
	[DoctorLingo] [nvarchar](64) NULL,
	[DecimalDefault] [nvarchar](1) NULL,
	[ICDAlias1] [nvarchar](32) NULL,
	[ICDAlias2] [nvarchar](32) NULL,
	[ICDAlias3] [nvarchar](32) NULL,
	[ICDAlias4] [nvarchar](32) NULL,
	[Exclusion] [nvarchar](1) NULL,
	[LetterTranslation] [nvarchar](128) NULL,
	[OtherLtrTrans] [nvarchar](128) NULL,
	[Ros] [nvarchar](1) NULL,
	[Increment] [nvarchar](5) NULL,
	[IgnoreIt] [nvarchar](1) NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[SecondaryFormsLanguage]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[SecondaryFormsLanguage](
	[LanguageId] [int] NOT NULL,
	[Language] [nvarchar](32) NULL,
	[ControlId] [int] NULL,
	[ControlText] [nvarchar](64) NULL,
	[ConfirmLingo] [nvarchar](64) NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[ThirdForms]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[ThirdForms](
	[ThirdFormId] [int] NOT NULL,
	[FormId] [int] NULL,
	[FormControlName] [nvarchar](64) NULL,
	[Question] [nvarchar](64) NULL,
	[QuestionOrder] [nvarchar](5) NULL,
	[BackColor] [int] NULL,
	[ForeColor] [int] NULL,
	[WindowState] [int] NULL,
	[ExitButtonY] [bit] NOT NULL,
	[ExitButton] [nvarchar](1) NULL,
	[ApplyButtonY] [bit] NOT NULL,
	[ApplyButton] [nvarchar](1) NULL,
	[DontKnowButtonY] [bit] NOT NULL,
	[DontKnowButton] [nvarchar](1) NULL,
	[HelpButtonY] [bit] NOT NULL,
	[HelpButton] [nvarchar](1) NULL,
	[NoToAllButtonY] [bit] NOT NULL,
	[NoToAllButton] [nvarchar](1) NULL,
	[StaticLabel] [nvarchar](32) NULL,
	[InferenceQuestion] [nvarchar](64) NULL,
	[InferenceType] [nvarchar](1) NULL,
	[FormHeight] [int] NULL,
	[FormWidth] [int] NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[ThirdFormsControls]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[ThirdFormsControls](
	[ThirdControlId] [int] NOT NULL,
	[ThirdFormId] [int] NULL,
	[ControlName] [nvarchar](64) NULL,
	[ControlType] [nvarchar](1) NULL,
	[BackColor] [int] NULL,
	[ForeColor] [int] NULL,
	[TabIndex] [int] NULL,
	[ResponseLength] [int] NULL,
	[ControlVisibleY] [bit] NOT NULL,
	[ControlVisible] [nvarchar](1) NULL,
	[ControlTemplateName] [nvarchar](64) NULL,
	[ControlFont] [nvarchar](32) NULL,
	[ControlTop] [int] NULL,
	[ControlHeight] [int] NULL,
	[ControlLeft] [int] NULL,
	[ControlWidth] [int] NULL,
	[InferenceEngineChoiceName] [nvarchar](64) NULL,
	[ControlText] [nvarchar](64) NULL,
	[DrugQuestionY] [bit] NOT NULL,
	[DrugQuestion] [nvarchar](1) NULL,
	[ControlAlternateText] [nvarchar](64) NULL,
	[ControlAlternateTrigger] [nvarchar](64) NULL,
	[ControlVisibleAfterTriggerY] [bit] NOT NULL,
	[ControlVisibleAfterTrigger] [nvarchar](1) NULL,
	[ConfirmLingo] [nvarchar](64) NULL,
	[DoctorLingo] [nvarchar](64) NULL,
	[DecimalDefault] [nvarchar](1) NULL,
	[ICDAlias1] [nvarchar](32) NULL,
	[ICDAlias2] [nvarchar](32) NULL,
	[ICDAlias3] [nvarchar](32) NULL,
	[ICDAlias4] [nvarchar](32) NULL,
	[Exclusion] [nvarchar](1) NULL,
	[LetterTranslation] [nvarchar](128) NULL,
	[OtherLtrTrans] [nvarchar](128) NULL,
	[Ros] [nvarchar](1) NULL,
	[Increment] [nvarchar](5) NULL,
	[IgnoreIt] [nvarchar](1) NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[df].[ThirdFormsLanguage]') AND type in (N'U'))
BEGIN
CREATE TABLE [df].[ThirdFormsLanguage](
	[LanguageId] [int] NOT NULL,
	[Language] [nvarchar](32) NULL,
	[ControlId] [int] NULL,
	[ControlText] [nvarchar](64) NULL,
	[ConfirmLingo] [nvarchar](64) NULL
)
END
GO
