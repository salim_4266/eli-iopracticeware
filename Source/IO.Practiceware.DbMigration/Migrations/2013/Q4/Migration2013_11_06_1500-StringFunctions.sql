﻿
IF OBJECT_ID('dbo.ChunkWithId', 'TF') IS NOT NULL
DROP FUNCTION dbo.ChunkWithId
GO

CREATE FUNCTION [dbo].[ChunkWithId](@text nvarchar(max), @size int, @id int)
RETURNS @result table(someValue nvarchar(max), someId INT, somekeyValue NVARCHAR(1), Position INT)
--Accepts: a string to parse, how many characters to group by, a key used to match the returned rows back to the row that it was called from.
--Returns: a table with each parsed group as a row, along with 
--Originally created to create many rows from a single PatientClinical.ImageDescriptor for PatientMedicationDetails.
BEGIN

DECLARE @currentChunk nvarchar(max)

WHILE @text <> '' AND @text IS NOT NULL
BEGIN
	SET @currentChunk = SUBSTRING(@text, 1, CASE WHEN LEN(@text) > @size THEN @size ELSE LEN(@text) END)
	INSERT @result(someValue, someId, somekeyValue, Position) VALUES(SUBSTRING(@currentChunk,2,@size), @id, SUBSTRING(@currentChunk, 1,1), LEN(@text))
	SET @text = SUBSTRING(@text, LEN(@currentChunk) + 1, LEN(@text) - LEN(@currentChunk))
END
RETURN 
END
GO

IF OBJECT_ID('dbo.ExtractBetweenDelimited', 'TF') IS NOT NULL
DROP FUNCTION dbo.ExtractBetweenDelimited
GO

CREATE FUNCTION [dbo].[ExtractBetweenDelimited] (@delimiterOne CHAR(1),@stringValue NVARCHAR(MAX),@delimiterTwo CHAR(1))
RETURNS @ExtractionSet TABLE(Id INT,Extraction NVARCHAR(4000))
--Accepts: Start and end search character, string to search.
--Returns: A table with all occurrences of the paired start and end characters.
--Created to parse groups of () from FindingDetails.
AS
BEGIN

IF (dbo.IsNullOrEmpty(@stringValue) <> 1)
BEGIN
		;WITH CTE AS (
				SELECT 1 AS N

				UNION ALL 

				SELECT N + 1 AS N
				FROM CTE
				WHERE n < LEN(@stringValue)
		)        
		INSERT INTO @ExtractionSet(Id,Extraction)
		SELECT
		ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS Id
		,Chunks
		FROM (
				SELECT 
				SUBSTRING(@stringValue,N+1,CHARINDEX(@delimiterTwo,SUBSTRING(@stringValue,N,LEN(@stringValue)))-2) AS Chunks
				,n
				FROM CTE
				WHERE SUBSTRING(@stringValue, CTE.N, 1) = @delimiterOne 
		)v WHERE v.n+1 NOT IN (SELECT 
				n
				FROM CTE
				WHERE SUBSTRING(@stringValue, CTE.N, 1) = @delimiterOne )
		OPTION (MAXRECURSION 0)
		END

		RETURN
	END
GO

IF OBJECT_ID('dbo.GetRowForRowNumber', 'FN') IS NOT NULL
DROP FUNCTION dbo.GetRowForRowNumber
GO

CREATE FUNCTION dbo.GetRowForRowNumber (
	@delimiterOne CHAR(1)
	,@stringValue NVARCHAR(MAX)
	,@delimiterTwo CHAR(1)
	,@rowNumber INT
	)
RETURNS NVARCHAR(max)
--Accepts: Start and end search character, string to search, and the number position in the desired array of results. 
--Returns: A single string value for given position.
--Created to allow for inline selection of parsed values.
--Calls the table-valued function ExtractBetweenDelimited.
AS
BEGIN
	RETURN (
			SELECT Extraction
			FROM dbo.ExtractBetweenDelimited(@delimiterOne, @stringValue, @delimiterTwo)
			WHERE Id = @rowNumber
			)
END