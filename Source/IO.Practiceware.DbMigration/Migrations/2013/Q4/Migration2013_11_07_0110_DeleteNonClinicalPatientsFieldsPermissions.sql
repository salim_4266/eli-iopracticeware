﻿-- Removing Non-Clinical Patients Fields Permissions for RequireDateOfBirthForCreatingOrEditingPolicyholder, RequireSocialSecurityForCreatingOrEditingPolicyholder
DELETE FROM model.UserPermissions Where PermissionId in (14,15)
GO

DELETE FROM model.Permissions Where Id in (14,15)
GO