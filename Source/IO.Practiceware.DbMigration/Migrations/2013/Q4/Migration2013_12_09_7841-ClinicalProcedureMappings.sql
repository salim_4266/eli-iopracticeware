﻿INSERT INTO model.ClinicalProcedures (Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated)
SELECT 'CHEST X-RAY, PA, LATERAL VIEWS', CONVERT(BIT,0), 2, CONVERT(BIT,0) UNION ALL
SELECT 'ABDOMINAL ULTRASOUND', CONVERT(BIT,0), 2, CONVERT(BIT,0) UNION ALL
SELECT 'SINUS CT', CONVERT(BIT,0), 2, CONVERT(BIT,0)

DECLARE @SnomedExternalSystemId INT
SELECT @SnomedExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'SnomedCt'

DECLARE @ClinicalProcedureId INT
SELECT @ClinicalProcedureId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalProcedure'

DECLARE @SnomedExternalSystemIdClinicalProcedure INT
SELECT @SnomedExternalSystemIdClinicalProcedure = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @SnomedExternalSystemId
	AND Name = 'ClinicalProcedure'

DELETE FROM model.ExternalSystemEntityMappings WHERE ExternalSystemEntityId = @SnomedExternalSystemIdClinicalProcedure

INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])

SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'8' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'251776000' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'64' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'410557009' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'437' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'404655007' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'225' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'416582002' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'300' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'419775003' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'211' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'12094002' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'27' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'250765005' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'212' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'412793001' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'206' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'172201009' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'104' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'110473004' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'688' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'397517002' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'218' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'76135002' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'21' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'57368009' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'617' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'251686008' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'46' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'157496001' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'32' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'252830007' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'198' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'148866006' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'79' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'11813006' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'5' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'397278000' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'233' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'148836004' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'663' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'413954006' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'35' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'25322007' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'39' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'1449002' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'628' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'20328009' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'547' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'422093005' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'84' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'252822006' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'456' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'393461006' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'250' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'64445006' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'20' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'50121007' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'65' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'141904003' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'319' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'140096001' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'186' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'103232008' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'435' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'414497003' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'6' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'252828005' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'519' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'360098007' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'367' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'155146008' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'4' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'397277005' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'33' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'397478003' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'599' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'25570002' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'18' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'313088003' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'274' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'54513001' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'204' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'75149001' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'633' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'40505001' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'159' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'84149000' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'123' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'231766008' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'3' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'424622008' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'215' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'397516006' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'197' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'32468005' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'208' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'69769008' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'312' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'51683002' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'190' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'145684005' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'436' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'405739002' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'533' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'415282007' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'24' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'141906001' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'504' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'87732007' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'95' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'392005004' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'168' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'370944007' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'106' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'370957004' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'470' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'228484007' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'389' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'251763006' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'513' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'172405003' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'207' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'148846002' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'370' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'156643008' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'201' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'148873001' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'632' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'271729008' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'678' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'311886005' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'454' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'46680005' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId AS PracticeRepositoryEntityId,'126' AS PracticeRepositoryEntityKey, @SnomedExternalSystemIdClinicalProcedure AS ExternalSystemEntityId,'19875009' AS ExternalSystemEntityKey UNION ALL
SELECT @ClinicalProcedureId,(SELECT TOP 1 Id FROM model.ClinicalProcedures WHERE Name = 'CHEST X-RAY, PA, LATERAL VIEWS'),@SnomedExternalSystemIdClinicalProcedure,168731009 UNION ALL
SELECT @ClinicalProcedureId,(SELECT TOP 1 Id FROM model.ClinicalProcedures WHERE Name = 'ABDOMINAL ULTRASOUND'),@SnomedExternalSystemIdClinicalProcedure,441987005 UNION ALL
SELECT @ClinicalProcedureId,(SELECT TOP 1 Id FROM model.ClinicalProcedures WHERE Name = 'SINUS CT'),@SnomedExternalSystemIdClinicalProcedure,241526005