﻿
IF OBJECT_ID('model.NewCropUtilizationReports') IS NULL
BEGIN
	EXEC('
    CREATE TABLE model.NewCropUtilizationReports(
		[TransactionId] nvarchar(255) NOT NULL
		,[Timestamp] datetime NOT NULL
		,[Status] nvarchar(1000)
		,[StatusTimestamp] datetime
		,[Message] nvarchar(1000)
		,[UserId] nvarchar(1000)
		,[UserType] nvarchar(1000)
		,[LicensedPrescriberId] nvarchar(1000)
		,[StartDate] datetime
		,[EndDate] datetime
		,[QuarterNumber] int
		,[QuarterYear] nvarchar(100)
		,[TimePeriodType] smallint
		,[IncludeSchema] nvarchar(1000)
		,[ReportContent] nvarchar(max)
	)
	')

	EXEC('
	ALTER TABLE model.NewCropUtilizationReports
	ADD CONSTRAINT [PK_NewCropUtilizationReports]
    PRIMARY KEY CLUSTERED ([TransactionId] ASC);
	')

END

