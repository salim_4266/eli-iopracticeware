﻿SET IDENTITY_INSERT [model].[ClinicalDataSourceTypes] ON

	INSERT INTO [model].[ClinicalDataSourceTypes] (Id, Name) VALUES (1, 'Practice Provider')
	INSERT INTO [model].[ClinicalDataSourceTypes] (Id, Name) VALUES (2, 'Patient Or Patient Representative')
	INSERT INTO [model].[ClinicalDataSourceTypes] (Id, Name) VALUES (3, 'External Provider')

SET IDENTITY_INSERT [model].[ClinicalDataSourceTypes] OFF
GO