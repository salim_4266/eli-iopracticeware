﻿-- Maps ClinicalQualifiers to snomed
-- Not that not all clinicalProcedures are mapped.
DECLARE @SnomedExternalSystemId INT
SELECT @SnomedExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'SnomedCt'

DECLARE @ClinicalQualifierId INT
SELECT @ClinicalQualifierId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalQualifier'

DECLARE @SnomedExternalSystemIdClinicalQualifier INT
SELECT @SnomedExternalSystemIdClinicalQualifier = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @SnomedExternalSystemId
	AND Name = 'ClinicalQualifier'

INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,8,@SnomedExternalSystemIdClinicalQualifier,2709139019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,9,@SnomedExternalSystemIdClinicalQualifier,646658015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,10,@SnomedExternalSystemIdClinicalQualifier,664002017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,12,@SnomedExternalSystemIdClinicalQualifier,655979017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,13,@SnomedExternalSystemIdClinicalQualifier,796215014)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,14,@SnomedExternalSystemIdClinicalQualifier,513873016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,15,@SnomedExternalSystemIdClinicalQualifier,646338012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,17,@SnomedExternalSystemIdClinicalQualifier,648143019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,18,@SnomedExternalSystemIdClinicalQualifier,2530390017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,19,@SnomedExternalSystemIdClinicalQualifier,2149568015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,22,@SnomedExternalSystemIdClinicalQualifier,1196550018)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,24,@SnomedExternalSystemIdClinicalQualifier,1196541015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,25,@SnomedExternalSystemIdClinicalQualifier,2614223010)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,26,@SnomedExternalSystemIdClinicalQualifier,1196538012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,27,@SnomedExternalSystemIdClinicalQualifier,1196545012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,28,@SnomedExternalSystemIdClinicalQualifier,1460892015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,29,@SnomedExternalSystemIdClinicalQualifier,568954017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,30,@SnomedExternalSystemIdClinicalQualifier,1196539016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,33,@SnomedExternalSystemIdClinicalQualifier,756697019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,34,@SnomedExternalSystemIdClinicalQualifier,807076019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,35,@SnomedExternalSystemIdClinicalQualifier,656006019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,41,@SnomedExternalSystemIdClinicalQualifier,820558013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,43,@SnomedExternalSystemIdClinicalQualifier,646659011)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,46,@SnomedExternalSystemIdClinicalQualifier,2749935011)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,48,@SnomedExternalSystemIdClinicalQualifier,646372016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,51,@SnomedExternalSystemIdClinicalQualifier,2749636010)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,53,@SnomedExternalSystemIdClinicalQualifier,749634019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,54,@SnomedExternalSystemIdClinicalQualifier,646658015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,56,@SnomedExternalSystemIdClinicalQualifier,2747898015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,57,@SnomedExternalSystemIdClinicalQualifier,664002017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,59,@SnomedExternalSystemIdClinicalQualifier,761871012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,61,@SnomedExternalSystemIdClinicalQualifier,2690578019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,64,@SnomedExternalSystemIdClinicalQualifier,646489013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,66,@SnomedExternalSystemIdClinicalQualifier,646463013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,67,@SnomedExternalSystemIdClinicalQualifier,2709142013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,68,@SnomedExternalSystemIdClinicalQualifier,513797019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,69,@SnomedExternalSystemIdClinicalQualifier,632936016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,72,@SnomedExternalSystemIdClinicalQualifier,2468699011)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,73,@SnomedExternalSystemIdClinicalQualifier,2709142013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,74,@SnomedExternalSystemIdClinicalQualifier,696293017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,75,@SnomedExternalSystemIdClinicalQualifier,646309014)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,76,@SnomedExternalSystemIdClinicalQualifier,646338012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,77,@SnomedExternalSystemIdClinicalQualifier,652152013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,78,@SnomedExternalSystemIdClinicalQualifier,656030017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,81,@SnomedExternalSystemIdClinicalQualifier,656081010)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,83,@SnomedExternalSystemIdClinicalQualifier,2749636010)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,85,@SnomedExternalSystemIdClinicalQualifier,646403011)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,86,@SnomedExternalSystemIdClinicalQualifier,646493019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,87,@SnomedExternalSystemIdClinicalQualifier,781194016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,88,@SnomedExternalSystemIdClinicalQualifier,829402012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,89,@SnomedExternalSystemIdClinicalQualifier,747996010)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,90,@SnomedExternalSystemIdClinicalQualifier,740446015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,91,@SnomedExternalSystemIdClinicalQualifier,800111017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,95,@SnomedExternalSystemIdClinicalQualifier,2639680013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,96,@SnomedExternalSystemIdClinicalQualifier,2719870015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,97,@SnomedExternalSystemIdClinicalQualifier,2721970019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,98,@SnomedExternalSystemIdClinicalQualifier,2748441018)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,99,@SnomedExternalSystemIdClinicalQualifier,777286015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,100,@SnomedExternalSystemIdClinicalQualifier,2529117014)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,101,@SnomedExternalSystemIdClinicalQualifier,1459604016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,102,@SnomedExternalSystemIdClinicalQualifier,619866015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,103,@SnomedExternalSystemIdClinicalQualifier,812790010)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,104,@SnomedExternalSystemIdClinicalQualifier,749781012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,105,@SnomedExternalSystemIdClinicalQualifier,696277014)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,106,@SnomedExternalSystemIdClinicalQualifier,769237011)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,109,@SnomedExternalSystemIdClinicalQualifier,2609608016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,110,@SnomedExternalSystemIdClinicalQualifier,2609692014)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,111,@SnomedExternalSystemIdClinicalQualifier,2609607014)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,112,@SnomedExternalSystemIdClinicalQualifier,2750550013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,113,@SnomedExternalSystemIdClinicalQualifier,2748986011)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,118,@SnomedExternalSystemIdClinicalQualifier,2711685014)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,120,@SnomedExternalSystemIdClinicalQualifier,756623013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,122,@SnomedExternalSystemIdClinicalQualifier,2750553010)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,125,@SnomedExternalSystemIdClinicalQualifier,2717546017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,127,@SnomedExternalSystemIdClinicalQualifier,784093013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,128,@SnomedExternalSystemIdClinicalQualifier,740470016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,129,@SnomedExternalSystemIdClinicalQualifier,2754669016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,131,@SnomedExternalSystemIdClinicalQualifier,2710085015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,134,@SnomedExternalSystemIdClinicalQualifier,646628014)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,136,@SnomedExternalSystemIdClinicalQualifier,646622010)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,138,@SnomedExternalSystemIdClinicalQualifier,829402012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,139,@SnomedExternalSystemIdClinicalQualifier,747996010)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,140,@SnomedExternalSystemIdClinicalQualifier,652832014)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,142,@SnomedExternalSystemIdClinicalQualifier,819778012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,144,@SnomedExternalSystemIdClinicalQualifier,2711685014)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,150,@SnomedExternalSystemIdClinicalQualifier,655893011)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,159,@SnomedExternalSystemIdClinicalQualifier,2717546017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,160,@SnomedExternalSystemIdClinicalQualifier,784093013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,163,@SnomedExternalSystemIdClinicalQualifier,2754669016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,165,@SnomedExternalSystemIdClinicalQualifier,646628014)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,167,@SnomedExternalSystemIdClinicalQualifier,793943015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,168,@SnomedExternalSystemIdClinicalQualifier,791890013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,169,@SnomedExternalSystemIdClinicalQualifier,800625015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,170,@SnomedExternalSystemIdClinicalQualifier,833901013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,171,@SnomedExternalSystemIdClinicalQualifier,728771012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,172,@SnomedExternalSystemIdClinicalQualifier,834114016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,173,@SnomedExternalSystemIdClinicalQualifier,1459604016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,174,@SnomedExternalSystemIdClinicalQualifier,813812016)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,175,@SnomedExternalSystemIdClinicalQualifier,766708012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,178,@SnomedExternalSystemIdClinicalQualifier,796859010)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,180,@SnomedExternalSystemIdClinicalQualifier,652140018)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,181,@SnomedExternalSystemIdClinicalQualifier,619866015)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,184,@SnomedExternalSystemIdClinicalQualifier,777871019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,185,@SnomedExternalSystemIdClinicalQualifier,829402012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,186,@SnomedExternalSystemIdClinicalQualifier,747996010)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,187,@SnomedExternalSystemIdClinicalQualifier,795769019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,188,@SnomedExternalSystemIdClinicalQualifier,2717543013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,189,@SnomedExternalSystemIdClinicalQualifier,2748708017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,190,@SnomedExternalSystemIdClinicalQualifier,832155019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,191,@SnomedExternalSystemIdClinicalQualifier,652853017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,192,@SnomedExternalSystemIdClinicalQualifier,2749935011)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,193,@SnomedExternalSystemIdClinicalQualifier,655842019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,194,@SnomedExternalSystemIdClinicalQualifier,2751708018)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,195,@SnomedExternalSystemIdClinicalQualifier,652832014)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,196,@SnomedExternalSystemIdClinicalQualifier,744947013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,197,@SnomedExternalSystemIdClinicalQualifier,646386017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,199,@SnomedExternalSystemIdClinicalQualifier,652161013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,201,@SnomedExternalSystemIdClinicalQualifier,646412013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,202,@SnomedExternalSystemIdClinicalQualifier,787305011)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,203,@SnomedExternalSystemIdClinicalQualifier,787750011)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,208,@SnomedExternalSystemIdClinicalQualifier,788142012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,210,@SnomedExternalSystemIdClinicalQualifier,761871012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,211,@SnomedExternalSystemIdClinicalQualifier,2753330012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,212,@SnomedExternalSystemIdClinicalQualifier,646558012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,217,@SnomedExternalSystemIdClinicalQualifier,2759915018)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,219,@SnomedExternalSystemIdClinicalQualifier,791890013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,220,@SnomedExternalSystemIdClinicalQualifier,833901013)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,221,@SnomedExternalSystemIdClinicalQualifier,2731674019)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,222,@SnomedExternalSystemIdClinicalQualifier,756072017)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,223,@SnomedExternalSystemIdClinicalQualifier,2717550012)
INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(@ClinicalQualifierId,224,@SnomedExternalSystemIdClinicalQualifier,2715315019)

--SELECT 
--q.Id
--,q.Name
--,dbo.GetSnomedId(q.Name) AS SnomedId
--INTO #temp
--FROM model.ClinicalQualifiers q

--DECLARE @SnomedExternalSystemId INT
--SELECT @SnomedExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'SnomedCt'

--DECLARE @ClinicalQualifierId INT
--SELECT @ClinicalQualifierId= Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalQualifier'

--DECLARE @SnomedExternalSystemIdClinicalQualifier INT
--SELECT @SnomedExternalSystemIdClinicalQualifier = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @SnomedExternalSystemId

--SELECT 
--@ClinicalQualifierId AS PracticeRepositoryEntityId
--,Id AS PracticeRepositoryEntityKey
--,@SnomedExternalSystemIdClinicalQualifier AS ExternalSystemEntityId
--,SnomedId AS ExternalSystemEntityKey
--FROM #temp
--WHERE SnomedId IS NOT NULL
--GROUP BY Id, SnomedId