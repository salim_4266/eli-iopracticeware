﻿--This is requried because there are practice vendors in the PC table that do not exist in the Practice Vendors table.
UPDATE pc
SET FindingDetail = SUBSTRING(pc.FindingDetail, 1, CHARINDEX('/', pc.FindingDetail, 30)) + '-/'
FROM dbo.PatientClinical pc
LEFT JOIN dbo.PracticeVendors pv ON pv.VendorId =  CONVERT(INT, dbo.ExtractTextValue('(', FindingDetail, ')'))
WHERE pc.ClinicalType = 'A'
AND (
	SUBSTRING(pc.FindingDetail, 1, 25) = 'REFER PATIENT TO ANOTHER'
	OR pc.FindingDetail LIKE 'SEND A CONSULTATION LETTER%'
	)
AND pc.STATUS = 'A'
AND pv.VendorId IS NULL
AND ISNUMERIC(dbo.ExtractTextValue('(', pc.FindingDetail, ')')) = 1

GO
IF  EXISTS(SELECT * FROM sys.objects WHERE name = 'PatientClinicalConsultOrder')
DROP TRIGGER PatientClinicalConsultOrder

GO

--Prevent the introduction of this bad data.
CREATE TRIGGER PatientClinicalConsultOrder
ON dbo.PracticeVendors
FOR DELETE 
NOT FOR REPLICATION
AS
BEGIN
SET NOCOUNT ON

IF EXISTS (SELECT * 
			FROM dbo.PatientClinical pc 
			INNER JOIN deleted d ON d.VendorId = CONVERT(INT, dbo.ExtractTextValue('(', FindingDetail, ')'))
			WHERE pc.ClinicalType = 'A'
				AND (
					SUBSTRING(pc.FindingDetail, 1, 25) = 'REFER PATIENT TO ANOTHER'
					OR pc.FindingDetail LIKE 'SEND A CONSULTATION LETTER%'
					)
				AND pc.STATUS = 'A'
				AND ISNUMERIC(dbo.ExtractTextValue('(', pc.FindingDetail, ')')) = 1)
	
BEGIN
	RAISERROR ('Cannot delete referring doctor because a consultation letter has been ordered for this doctor.',16,1)
	ROLLBACK TRANSACTION
END
END

GO

--Found duplicate smoking patient clinical rows.
DELETE pc1
FROM [dbo].[PatientClinical] pc1
INNER JOIN dbo.PatientClinical pc2 ON pc1.AppointmentId = pc2.AppointmentId
	AND pc1.ClinicalType = pc2.ClinicalType
	AND pc1.EyeContext = pc2.EyeContext
	AND pc1.Symptom = pc2.Symptom
	AND pc1.FindingDetail = pc2.FindingDetail
	AND pc1.ImageDescriptor = pc2.ImageDescriptor
	AND pc1.ImageInstructions = pc2.ImageInstructions
	AND pc1.Status = pc2.Status
WHERE pc2.ClinicalId > pc1.ClinicalId
	AND pc1.ClinicalType = 'F' 
	AND pc1.Symptom = '/SMOKING'
