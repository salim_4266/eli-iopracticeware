﻿--Creation of Demographic Configurations tables.
IF OBJECT_ID(N'[model].[PatientDemographicsFieldConfigurations]', 'U') IS NOT NULL
    DROP TABLE [model].[PatientDemographicsFieldConfigurations];
GO
-- Creating table 'DemographicFieldConfigurations'
CREATE TABLE [model].[PatientDemographicsFieldConfigurations] (
    [Id] int IDENTITY(1,1) NOT NULL,    
	[IsVisibilityConfigurable] bit NOT NULL Default 0,
	[IsVisible] bit NOT NULL Default 1,
	[IsRequiredConfigurable] bit NOT NULL Default 0,
	[IsRequired] bit NOT NULL Default 0,
	[DefaultValue] nvarchar(max) NULL
);
GO

-- Creating primary key on [Id] in table 'PatientDemographicsFieldConfigurations'
ALTER TABLE [model].[PatientDemographicsFieldConfigurations]
ADD CONSTRAINT [PK_PatientDemographicsFieldConfigurations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- 1 - 'Title'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 0, 0);

-- 2 - 'First Name'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);

-- 3 - 'Middle Name'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 0, 0);

-- 4 -'Last Name'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);
 
-- 5 - 'Suffix'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 0);

-- 6 - 'Nick Name'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 0, 0);

-- 7 - 'Honorific'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 0);

-- 8 - 'Prior First Name'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 0);

-- 9 - 'Prior Last Name'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 0);

--10 -  'Primary Address'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 0);

--11 - 'Primary Phone Number'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 0);

--12 - 'Primary Email Address'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 0);

--13 - 'Birth Date'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 1);

--14 - 'Gender'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 1);

--15 - 'Marital Status'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--16 - 'Race'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 0);

--17 - 'Ethnicity'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 0);

--18 - 'Language'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 0);

--19 - 'SSN'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--20 - 'Prior ID'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 1, 0);

--21-  'Employment Area'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 0, 0);

--22 - 'Preferred Physician'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);

--23 - 'PCP'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 0);

--24 - 'Other'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 0);

--25 - 'HIPAA Consent'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired, DefaultValue) VALUES (0, 1, 0, 0, 'True');

--26 - 'Release Medical Records'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired, DefaultValue) VALUES (0, 1, 0, 0, '1');

--27 - 'Signature Date'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);

--28 - 'Assign Benefits to Provider'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired, DefaultValue) VALUES (0, 1, 0, 0, '1');

--29 - 'Patient Tag'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 0);

--30 - 'Status'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);

--31 - 'Religion'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 0, 0);

--32 - 'Referring Contact & Category'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (1, 1, 0, 0);

--33 - 'Billing Organization'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 1);

--34 - 'Comments'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 0, 0);

--35 - 'Date Of Birth' - Non-Clinical Patients
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 0);

--36 - 'Social Security Number' - Non-Clinical Patients
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (0, 1, 1, 0);
