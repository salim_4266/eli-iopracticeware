﻿EXEC sp_RENAME 'model.ApplicationSettings.Xml', 'Value', 'COLUMN'

IF OBJECT_ID('model.AuditApplicationSettingsDeleteTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER model.AuditApplicationSettingsDeleteTrigger')
END

IF OBJECT_ID('model.AuditApplicationSettingsInsertTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER model.AuditApplicationSettingsInsertTrigger')
END

IF OBJECT_ID('model.AuditApplicationSettingsUpdateTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER model.AuditApplicationSettingsUpdateTrigger')
END