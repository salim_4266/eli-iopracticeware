﻿--This is required because the practice may have removed a CC from PracticeCode table. 
--Entries with the value CHIEFCOMPLAINTSARCHIVED will not be seen in the software, used so we can INNER join to PracticeCodeTable.



;WITH CTE AS (
SELECT 'CC:' + Code AS someValue
FROM dbo.PracticeCodeTable pct 
WHERE pct.ReferenceType IN('CHIEFCOMPLAINTS', 'CHIEFCOMPLAINTSARCHIVED')
)

SELECT 'CHIEFCOMPLAINTSARCHIVED'  AS Name, CONVERT(nvarchar(max), SUBSTRING(FindingDetail,4,LEN(FindingDetail))) AS Value
INTO #temp12
FROM dbo.PatientClinical pc 
LEFT JOIN CTE ON CTE.someValue = pc.FindingDetail
WHERE pc.Symptom = '/CHIEFCOMPLAINTS' AND CTE.someValue IS NULL
ORDER BY Name, Value


INSERT INTO dbo.PracticeCodeTable (ReferenceType, AlternateCode, Code)
SELECT Name, 'Archived', Value FROM #temp12 GROUP BY Name, Value
GO
