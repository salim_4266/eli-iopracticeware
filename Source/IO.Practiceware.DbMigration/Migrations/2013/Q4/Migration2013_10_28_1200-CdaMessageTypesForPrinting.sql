﻿--This was originally migration number 201312051200, but there was a migration inserting into ExternalSystems that was happening before this causing Id = 5 to be duplicated.
IF NOT EXISTS (SELECT * FROM VersionInfo WHERE Version = '201312051200')
BEGIN 
	IF NOT EXISTS (
		SELECT TOP 1 [Id] 
		FROM [model].[ExternalSystems]
		WHERE [Id] =  5 AND [Name] = 'Printer'
	)
	BEGIN
		SET IDENTITY_INSERT [model].[ExternalSystems] ON
		INSERT INTO [model].[ExternalSystems]([Id], [Name])
		VALUES (5, 'Printer')
		SET IDENTITY_INSERT [model].[ExternalSystems] OFF
	END

	IF NOT EXISTS (
		SELECT TOP 1 [Id] 
		FROM [model].[ExternalSystemExternalSystemMessageTypes]
		WHERE [ExternalSystemMessageTypeId] = 27 AND [ExternalSystemId] =  5
	)
	BEGIN
		INSERT INTO [model].[ExternalSystemExternalSystemMessageTypes]
			   ([ExternalSystemId]
			   ,[ExternalSystemMessageTypeId]
			   ,[IsDisabled])
		 VALUES
			   (5 -- Printer
			   ,27 -- EncounterClinicalSummaryCCDA_Outbound
			   ,0)
	END

	IF NOT EXISTS (
		SELECT TOP 1 [Id] 
		FROM [model].[ExternalSystemExternalSystemMessageTypes]
		WHERE [ExternalSystemMessageTypeId] = 28 AND [ExternalSystemId] =  5
	)
	BEGIN
		INSERT INTO [model].[ExternalSystemExternalSystemMessageTypes]
			   ([ExternalSystemId]
			   ,[ExternalSystemMessageTypeId]
			   ,[IsDisabled])
		 VALUES
			   (5 -- Printer
			   ,28 -- PatientTransitionOfCareCCDA_Outbound
			   ,0)
	END

END