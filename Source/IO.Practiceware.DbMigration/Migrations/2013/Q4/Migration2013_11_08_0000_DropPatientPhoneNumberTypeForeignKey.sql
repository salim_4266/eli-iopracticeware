﻿-- This has been replaced by an struct

DECLARE @sql nvarchar(max)
SET @sql = ''
SELECT @sql = @sql + 'ALTER TABLE model.PatientPhoneNumbers DROP CONSTRAINT ' + fk.name + ';'
FROM sys.foreign_keys fk
    INNER JOIN sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
    INNER JOIN sys.columns cpa ON fkc.parent_object_id = cpa.object_id AND fkc.parent_column_id = cpa.column_id
    INNER JOIN sys.columns cref ON fkc.referenced_object_id = cref.object_id AND fkc.referenced_column_id = cref.column_id
WHERE cref.object_id = OBJECT_ID('model.PatientPhoneNumberTypes') AND fk.parent_object_id = OBJECT_ID('model.PatientPhoneNumbers')
EXEC(@sql)

IF OBJECT_ID('model.PatientPhoneNumberTypes') IS NOT NULL
	EXEC('DROP TABLE model.PatientPhoneNumberTypes')