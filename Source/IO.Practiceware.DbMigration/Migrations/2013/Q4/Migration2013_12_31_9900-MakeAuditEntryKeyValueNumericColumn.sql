﻿IF EXISTS (SELECT * FROM sys.columns sc INNER JOIN sys.tables st ON sc.object_id = st.object_id WHERE sc.name = 'KeyValueNumeric' AND st.name = 'AuditEntries')
BEGIN 
	DROP INDEX IX_AuditEntries_KeyValueNumeric ON dbo.AuditEntries
	ALTER TABLE dbo.AuditEntries DROP COLUMN KeyValueNumeric
END

GO

IF EXISTS (SELECT * FROM sys.columns sc INNER JOIN sys.tables st ON sc.object_id = st.object_id WHERE sc.name = 'OldValueNumeric' AND st.name = 'AuditEntryChanges')
BEGIN 
	DROP INDEX IX_AuditEntryChanges_ColumnName ON dbo.AuditEntryChanges
	ALTER TABLE dbo.AuditEntryChanges DROP COLUMN OldValueNumeric
END

GO

IF EXISTS (SELECT * FROM sys.columns sc INNER JOIN sys.tables st ON sc.object_id = st.object_id WHERE sc.name = 'NewValueNumeric' AND st.name = 'AuditEntryChanges')
BEGIN 
	ALTER TABLE dbo.AuditEntryChanges DROP COLUMN NewValueNumeric
END

GO

-- Add KeyValueNumeric for numeric keys
ALTER TABLE dbo.AuditEntries ADD KeyValueNumeric AS 
	([dbo].[TryConvertBigint](KeyValues))

CREATE INDEX IX_AuditEntries_KeyValueNumeric ON dbo.AuditEntries(KeyValueNumeric)
GO

IF NOT EXISTS(SELECT * FROM sys.columns sc INNER JOIN sys.tables st ON sc.object_id = st.object_id WHERE sc.name = 'OldValueNumeric' AND st.name = 'AuditEntryChanges') 
-- Add OldValueNumeric for numeric values
ALTER TABLE dbo.AuditEntryChanges ADD OldValueNumeric AS 
	([dbo].[TryConvertBigint](OldValue))
GO


IF NOT EXISTS(SELECT * FROM sys.columns sc INNER JOIN sys.tables st ON sc.object_id = st.object_id WHERE sc.name = 'NewValueNumeric' AND st.name = 'AuditEntryChanges') 
-- Add NewValueNumeric for numeric values
ALTER TABLE dbo.AuditEntryChanges ADD NewValueNumeric AS 
	([dbo].[TryConvertBigint](NewValue))
GO

IF EXISTS(SELECT * FROM sys.indexes i WHERE i.name = 'IX_AuditEntryChanges_ColumnName')
	DROP INDEX IX_AuditEntryChanges_ColumnName on dbo.AuditEntryChanges

-- Index by ColumnName
CREATE INDEX IX_AuditEntryChanges_ColumnName ON dbo.AuditEntryChanges(ColumnName)
INCLUDE(OldValueNumeric, NewValueNumeric)
GO

