﻿ALTER TABLE model.ClinicalProcedures DISABLE TRIGGER ALL;
ALTER TABLE model.ClinicalProcedures NOCHECK CONSTRAINT ALL;


DELETE FROM model.ClinicalProcedures

INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISION CORRECTED', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISION UNCORRECTED', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'POTENTIAL ACUITY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'MANIFEST REFRACTION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CYCLOPLEGIC REFRACTION', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'KERATOMETRY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SCHIRMER TEAR', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ACCOMMODATION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PACHYMETRY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SPECULAR MICROSCOPY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'COLOR PLATES', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IOP', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EXOPHTHALMETER', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PUPILS SIZE/REACTIVITY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISUAL FIELD INTERPRETATION', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'A SCAN BIOMETRY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'B SCAN', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCULAR ALIGNMENT', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'GENERAL MEDICAL OBSERVATION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'GLASSES', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CONTACT LENSES', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CATARACT FUNCTIONAL DISABILITY TEST', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BLOOD SUGAR', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RETINOSCOPY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AUTOREFRACTION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BRIGHTNESS ACUITY TEST', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BLOOD PRESSURE', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AUTOKERATOMETRY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IOL', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BLOOD COUNT', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CONFRONTATION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CORNEAL TOPOGRAPHY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'MOTILITY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CUP/DISC', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DILATION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CHALAZION EXCISION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LID LESION EXCISION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LID BIOPSY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EPILATION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LACRIMAL PROBE AND IRRIGATION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PUNCTOPLASTY, BIOPSY, EXC, LAC', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CLOSURE OF PUNCTUM, PERMANENT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BOTOX', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CONJUNCTIVAL FB REMOVAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CULTURE OF CORNEA/CONJUNCTIVA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CORNEAL ABRASION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INJECTION OF FILTERING BLEB', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BIOPSY, EXCISION, CONJUNCTIVA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ANTERIOR MICROPUNCTURE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASIK POST OP', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INJECTION, SUBCONJ', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CORNEAL FB REMOVAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CLRX FIT DESCRIPTORS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'HRT', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'MONOVISION TRIAL', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCULAR DOMINANCE', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STEREOPSIS, HOUSEFLY', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STEREOPSIS, ANIMALS', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STEREOPSIS, CIRCLES', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'COLOR D, ISHIHARA', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'COLOR D, PSEUDOISOCHROMATIC', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FARNSWORTH D15', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'GENERAL IMPRESSIONS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AMSLER GRID', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'GONIOSCOPY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, YAG, CAPSULOTOMY (66821)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, IRIDOTOMY, ARGON (66761)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, MACULOP, (FOCAL 67210)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, TRABECULOPLASTY (65855)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, VIT STRANDS (67031)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PHOTOCOAG RD', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, PRP (67228)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OVERREFRACTION OVER CL', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OVERREFRACTION OVER GLASSES', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IOL MASTER', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DILATED IOP PROVOCATIVE', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FUNDUS PHOTO', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SERIAL TONOMETRY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'COVER TEST', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ANTERIOR SEGMENT PHOTO', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LAB AND MEDICAL RESULTS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'MRI RESULTS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, IRIDOPLASTY (66762)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FLUORESCEIN ANGIOGRAPHY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EPITHELIAL STRESS TEST', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DESIRED CORRECTION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASIK IOL MASTER', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASIK SCHIRMER TEST', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASIK TOPOGRAPHY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CATARACT INFO GIVEN', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASIK INFO GIVEN', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'JAEGER VISION CORRECTED', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, YAG, IRIDOTOMY (66761)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IOP, POST OP', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SCANNING LASER POLARIMETRY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'JAEGER VISION UNCORRECTED', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OPHTHALMODYNOMOMETRY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OPHTHALMOSCOPY INITIAL', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OPHTHALMOSCOPY SUBSEQ', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ICG ANGIOGRAPHY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INJECTION, INTRALESIONAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CONJUNCTIVA CHECK', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ADNEXA AND LIDS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CATARACT SURGERY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FLUORESCEIN ANGIO TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SEIDEL TEST', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCT', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EOG', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ERG', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VF (30,2, 24,2,10,2, Esterman)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REFRACT CL', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ICG TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, PDT (67221)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISUDYNE WORKUP', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, REPAIR LOCAL RD (67105)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, PROPHY RD (67145)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CRYOTHERAPY LOC RD', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CRYOPEXY PROPHYLAXIS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CRYOPEXY RETINOPATHY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AC PARACENT W DX ASP', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AC PARACENT W THERAP', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AC PARACENT W REM OF BLOOD', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PNEUMATIC RETINOPEXY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FLUID GAS EXCHANGE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PUNCTUM PROBE AND IRRIGATION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITREOUS TAP', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECT, (67036)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VF   (Superior 36)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VF   (Suprathreshold, 120)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'A SCAN TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'B SCAN TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FUNDUS PHOTO TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VF TECH (30,2, 24,2,10,2, Ester)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'HRT TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCT TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'GDX TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VF TECH (Suprathreshold, 120)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VF TECH (Superior 36)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CORNEAL EPITHELIAL REMOVAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LACRIMAL PUNCTUM CLOSURE, PERM', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PLASTIC REPAIR OF CANANLICULI', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VTTT', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, PDT, 2ND EYE (67225)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, FOCAL, CNVM (67220)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REMOVAL IOFB, MAG', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REMOVAL IOFB, NON MAG', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REPAIR CORN, SCLER LAC W PLAPSE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REPAIR CORN, SCLERAL LAC', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AC PARACENT W INJECTION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LYSIS PAS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LYSIS POST SYN', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REMOVAL AC IOL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VALVE, MOLTENO, BAERVELDT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REVIS OP WOUND', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IRIDECTOMY, PERIPHERAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CB DESTRUC, CYCLOPHOTO', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REPOSITION IOL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CAPSULOTOMY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PHACOEMULSIFICATION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LENSECTOMY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INSERT IOL SECONDARY', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EXCHANGE IOL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IMPLANT GANCICLOVIR', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECTOMY W MEM PEEL', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECT, W FOCAL LASER (67039)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECT, W PRP (67040)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SCLERAL BUCKLE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECT, REPAIR RD (67108)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SCLERAL BUCKLE, REVISION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SCLERAL BUCKLE, REMOVAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REMOVE IMPL MATERIAL, IO', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CRYO, DESTR OF RET LESION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DESTRUC LOC LES RADIATION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RETROBULB INJEC MEDICATION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RELEASE VIT OR CHOROIDAL FLUID', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INJECTION, SUB TENONS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OPTIC NERVE DECOMPRESSION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'A SCAN ONLY (76511)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VEP', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCT, OPTIC NERVE', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, TAZIDIME', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, TRIAMCINOLONE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, VANCOMYCIN', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, MACUGEN', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'HEMOGLOBIN A1C', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RED DESATURATION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BLOOD WORK', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PHYSICAL EXAM', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RADIOLOGY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ALCOHOL INJECTION, RETROBULBAR', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PACHYMETRY TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FA AND FP', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FA AND FP TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECTOMY (IN OR)', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, AVASTIN', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PTERYGIUM EXCISION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CORNEAL TRANSPLANT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASIK SURGERY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ASTIGMATIC KERATOTOMY (LRI)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'TRABECULECTOMY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REPAIR OF IRIS, CB', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECT, ANTERIOR (67010)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ORBITOTOMY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INCISE AND DRAIN, LID', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CANTHOTOMY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'TARSORRHAPHY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PTOSIS REPAIR', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ECTROPION REPAIR', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ENTROPION REPAIR', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BLEPHAROPLASTY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CANTHOPLASTY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, TPA, C3F8', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IMPLANT RETISERT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PRK', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PTK', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CLOSURE OF PUNCTUM, PLUGS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CONJUNCTIVOPLASTY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BASIC MOTILITY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VERSIONS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'W4D', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SENSORIMOTOR', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCT, OPTIC NERVE TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CORNEAL EPITH REMOVAL, CHELATION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BANDAGE CONTACT LENS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, LUCENTIS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REMOVE SKIN TAG', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EXCISION OF LESION, ADNEXA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ENUCLEATION WITH IMPLANT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REMOVAL FB, LIDS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INCISE AND DRAIN CONJ CYST', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REMOVAL FB, LACRIMAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DACRYOCYSTORHINOSTOMY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RX CONTACT LENS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CLOSURE OF WOUND, ADNEXA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EXTERNAL PHOTO', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, IRIDOTOMY (66761)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SECONDARY IOL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'TBUT', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'NEAR POINT CONVERGENCE', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ORBITAL IMPLANT REVISION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASIK ENHANCEMENT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INJECTION, RETROBULBAR', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EOM SURG, HORIZONTAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EOM SURG, VERTICAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CATARACT SURGERY, COMPLEX', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AMNIOTIC MEMBRANE TRANSPLANT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DIVISION OF SYMBLEPHARON', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'WAVESCAN', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'GLARE', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BOTOX, COSMETIC', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BOTOX, MEDICAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, AART POST, JUXTA AA', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, AART, PDT', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, IDEAA, PDT', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, IDEAA, POST JUXTA AA', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, ALIMERA, INTRAVIT FLUO', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, ALLERGAN, INTRAVIT  DEX', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, SCORE, INTRAVIT KEN', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, VERITAS, INTRAVIT KEN,MAC', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, VERITAS, PDT', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, DENALI, INTRAVIT LUCENTIS', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, DENALI, PDT LOW FLUENCE', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, DENALI, PDT', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LAMELLAR KERATECTOMY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (POAG, OPTIC NERVE EVAL)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (ARMD VITAMINS)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (ARMD DILATED MACULAR EXAM)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (CAT ASSESS VISUAL STATUS)', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (CAT DOCUMENT PRE SX)', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (CAT PRE SURG FUNDUS EVAL)', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (DM RETINOPATHY DOCUMENT)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (DM RETINOPATHY INFO)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OPHTHALMIC MUCOUS MEMBRANE TEST', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FUNDUS PHOTO (ON HEAD)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FUNDUS PHOTO (ON HEAD) TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'A AND B SCAN COMBINED', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'A AND B SCAN COMBINED TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EXCISION AND REPAIR OF LID', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LRI', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, BRAVO, INJ', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, RISE, INJ', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, VEGF TRAP, INJ', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, CRUISE, INJ', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECT, PRERET MEM PEEL (67041)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECT, ILM PEEL (67042)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECT, SRM PEEL (67043)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECT, COMPLEX RD (67113)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (DM1 DILATED EXAM IN DM)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (DM2 VF IN DM PATIENT)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (DM3 PHOTOS IN DM PATIENT)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (DM4 LOW RISK DM PATIENT)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (USE OF EMR)', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AC RESTORATION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IRIS ANGIO TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IRIS ANGIOGRAPHY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IOL MASTER TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'GDX', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, SIRIUS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BEST CORRECTED VISUAL ACUITY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PHP', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PHP TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BILL GONI PACH FF VF MRX', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCT, RNFL', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, PRP, EXTENSIVE (67228)', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BILL GONI FF VF', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BILL GONI PACH FF', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BILL GONI FF', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BILL GONI PACH', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (POAG, 15% IOP REDUCTION)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRI (POAG, >15% IOP, PLAN)', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RADIAL KERATOTOMY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'MANIFEST REFRACT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'TRABECTOME', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REMOVE LENS MATERIAL, ASPIRATION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DERMAL FILLERS, COSMETIC', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EPILATION, OTHER THAN FORCEPS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CB DESTRUC, CYCLOPHOTO, ENDO', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'HEIGHT AND WEIGHT', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, TECH', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCT, CENTRAL MACULAR THICKNESS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, DIAMOND, INTRAVIT INJ', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, EMERALD, INTRAVIT INJ', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, MIVI TRUST, INTRAVIT INJ', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, HORIZON, INTRAVIT INJ', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, PDR IST, INTRAVIT INJ', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'E PRESCRIBED', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'NO RX', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RX, NOT E PRESCRIBED', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REACH, GRASP, RELEASE', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OPTICAL MEDIA', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EX PRESS SHUNT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY VISUAL ACUITY', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REPAIR CONJ LACERATION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ANSAR PERFORMED', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BLOOD FLOW ANALYSIS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'UBM', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ANSAR REVIEWED AND SIGNED', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EXAM UNDER ANESTHESIA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, OZURDEX', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EMERGENCY VISIT (99050)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCTOPUS TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCTOPUS VF', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, HARBOR, INTRAVIT INJECT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, BRIMO, INTRAVIT INSERT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DILATED REFRACTION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, ROP (67229)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RESEARCH INJECTION', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VSP TESTING', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CORNEAL HYSTERESIS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SPEC RX', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL RX', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL TRIAL 1', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL TRIAL 2', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJECT, LUCENTIS', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJECT, AVASTIN', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJECT, TRIESENCE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, NEW FIT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, BIFOCAL NEW', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, BIFOCAL REFIT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, GAS PERM', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, OFFICE VISIT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, CLEAN AND POLISH', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, SPECIAL FIT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FOLLOW UP', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LOW VISION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'NON-VERBAL VISION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LID FISSURE MEASURMENT', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'TORTICOLLIS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISION UNCORRECTED B', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISION CORRECTED B', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'WIVC', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'WEVC', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'WEVB', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'WEVINC', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VSPI', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VSPE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CLFONLY', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CLFBA-A', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CLFBA-B', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CLFBA-C', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CLRE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CLREO', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CLINC', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CLOD', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CSOS', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CSOU', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STEREOPSIS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'COLOR SCREEN', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, TRIAMCINOLONE, PF', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SEVERING TARSORRAPHY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SCLERAL BUCKLE, RELEASE (67115)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FDT PERIMETER', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'HRT, MACULA', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'MASS LESION EXCISION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SCLERAL PATCH GRAFT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CANALOPLASTY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AV RATIO', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, EVAL, NO CHANGE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, REG DISP SPHERE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, ASTIG', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, DAILY DISP', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, MULTI', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, HARD', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, KERATOCONUS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, SPHERE TO ASTIG', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, NON BI TO MULTI', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, BI TO MULTI', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, NON DISP TO DISP', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, NON PRESBY TO PRESBY', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, ADJ SPHERE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, HARD', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, TORIC, SIMPLE, W NEW LENS', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, TORIC, COMPLEX, W NEW LENS', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCT, ANTERIOR SEGMENT', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'TORIC IOL', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL ORDER TRIAL', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'MULTIFOCAL IOL', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CRYSTALENS IOL', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, DEXAMETHASONE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, AMIKACIN', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LRI, NOT COVERED', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, TORIC', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, TORIC', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, SOFT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, GAS PERM', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, SOFT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AIR FLUID EXCHANGE', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LENSTAR', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'TAP AND INJECT', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, MULTI', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, GAS PERM', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, EVAL AT EXAM', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INFRARED', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RED FREE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AUTOFLUORESCENCE', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AUTOFLUORESENCE TECH', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INFRARED TECH', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RED FREE TECH', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, CHECK', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, TRAINING', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, SEVER ADHESIONS (65860)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ICL SURGERY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PTERYGIUM EXCISION W GRAFT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RESTOR IOL', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EMERGENCY VISIT (99058)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EMERGENCY, AFTER 10 PM (99053)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, AVASTIN, DEXAMET', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, AVASTIN, TRIAMCI', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FLUORESCEIN ANGIO', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SHAVE SMALL LESION, ADNEXA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITAL SIGNS', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FOLLOW UP NEEDED', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FOLLOW UP', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OPHTHALMOSCOPY, DETECTION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OPHTHALMOSCOPY, MANAGE', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OSMOLARITY TEST', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'XEOMIN, MEDICAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'TEAR PH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REPLACEMENT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, EVAL, RX UPDATE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, CHECK, NOT AT EXAM', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, MONOVISION ADD', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, CAT SURG PREP', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, UNUSUAL', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, SPECIALTY', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RESIDUAL PO ASTIGMATISM', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SMOKING', 1, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'MEDS RECONCILIATION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, CORNEAL REHAB', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AMNIOTIC MEMBRANE, PLACEMENT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'MULTIFOCAL VA UNCORRECTED', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, SPECIALTY', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, SHORE, INTRAVIT INJ', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, RETAIN, INTRAVIT INJ', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, VEGF TRAP, INTRAVIT INJ', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, SAKURA, INTRAVIT INJ', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INJECTION, PERIBULBAR', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PRE-OP', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'GLAUCOMA MONITOR', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LID CLOSURE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'GONIOSCOPY R', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCT, CENT MAC THICK TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CHECK RX FOR GLASSES', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, KERATOCONUS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VF   (Taped, Untaped)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SUTURE REMOVAL, LATINA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SUTURE REMOVAL, CONJ', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SUTURE REMOVAL, CORNEA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, SUTURE LYSIS (66250)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, YAG, GONIOPUNCTURE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIT, NO INSTRUCTION', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CATARACT SURGERY, CRYSTALENS', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CATARACT SURGERY, TMF', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ANGLES (VAN HERICK)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'TRICHIASIS REPAIR, SPLIT LID', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LID CLOSURE DISCUSSED', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BOTOX, STRABISMUS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PROLONGED OV (99354)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'NERVE BLOCK, OCCIPITAL', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'NERVE BLOCK, TORTICOLLIS', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REVISION OF OCULAR IMPLANT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REPAIR, SIMPLE WOUND', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PRK ENHANCEMENT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, FOSCARNET', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY INJECTION', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCT H', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, EVAL, SPHERICAL', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, EVAL, TORIC, MULTI', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, EVAL, RGP', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SUPERFICIAL KERATECTOMY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REMOVE LENS MATERIAL, INTRACAP', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCT, RNFL TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INCISE AND DRAIN ABCESS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, HOFFMAN ASTIG', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STEREO PHOTO', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'KERATOPROSTHESIS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'NEEDLE REVISION OF BLEB', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CONJUNCTIVAL ADVANCEMENT', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, EYLEA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RESEARCH LASIK', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ORBSCAN', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LENSTAR TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CORNEAL TOPOGRAPHY, TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REFRACTIVE SX CONSULT', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCT, ANTERIOR SEGMENT, TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, OASIS, INTRAVIT INJ', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RESEARCH LASER', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'TENSILON TEST, MYASTHENIA GRAVIS', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RPS ADENO DETECTOR', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REFRACTIVE LENS EXCHANGE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LUCENTIS, NO CHARGE', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LUCENTIS, RESEARCH', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LUCENTIS, SAMPLE', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OZURDEX, NO CHARGE', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OZURDEX, SAMPLE', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AVASTIN, SAMPLE', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DISCISSION OF PCO (66820)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RETINAVITES, 1 BOTTLE', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RETINAVITES, 2 BOTTLES', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RETINAVITES, 3 BOTTLES', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECT ,(67036)', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'OCT, MACULA', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FA', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EXOPHTHALMOMETER', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PRISM TEST', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DOUBLE MADDOX ROD', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITREOUS LAVAGE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'TARSORRHAPHY, TEMP, SUTURE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BSCAN', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FUNDUS PHOTO (RETINA)', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STUDY, CYPASS', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, PROPHY (67145)', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, CEFTAZIDIME', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PQRS (POAG, <15% IOP, PLAN)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VF  TECH (Taped, Untaped)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EXTERNAL PHOTO TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'POST OP DROP KIT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EYLEA RESEARCH', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, SPHERE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, GAS PERM, SPHERE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, GAS PERM, TORIC', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, GAS PERM, MULTI', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, SPHERE TO SPECIALTY', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, NO CHANGE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SURGERY BAG', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'POST OP PERIOD', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LAB RESULTS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VF   (with GHT)', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DEXA SCAN', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASIK WORKUP', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, EVAL, SUBSEQUENT', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, EVAL, INITIAL', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IRIS COLOR', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, VORICONAZOLE', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, MITOMYCIN', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'JUVEDERM', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'JUVEDERM EVAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, RIGID', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, MONO', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, FIRST FIT, MONO', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, EVAL', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LACRIMAL PROBE, ANESTHESIA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FUNDUS PHOTO (FP)', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, GAS PERM, TORIC', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, GAS PERM, MULTI', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, LUCENTIS 0.3MG', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RADIAL KERATOTOMY SURGERY', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FUSIONAL AMPLITUDE', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BASAL CELL, REMOVE, RECONSTRUCT', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER VISION CORRECTION WORKUP', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EOM', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'UBM TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'COMPUTER VERGENCE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IOLMASTER', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'TEMPORAL ARTERIAL BIOPSY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'NASAL ENDOSCOPY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISION CORRECTED C', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISION UNCORRECTED C', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DERMAL FILLERS, RESTYLANE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DERMAL FILLERS, JUVEDERM', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DERMAL FILLERS, SCULPTRA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PENTACAM, TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, ROUTINE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, SPHERICAL', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, TORIC', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, MULTIFOCAL', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, SPECIALTY 1', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, SPECIALTY 2', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'NERVE BLOCK, FACIAL', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SACCADES', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISUAL MOTOR INTEGRATION', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'KEYSTONE COLOR TESTING', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EYE HAND COORDINATION', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CONTRAST SENSITIVITY', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FUSIONAL CORRESP', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SUPPRESSION FIELDS', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SPATIAL DEPTH PERCEPTION', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VERGENCE FACILITY', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, AMPHOTERICIN B', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJECT, LUCENTIS 0.3MG', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJECT, EYLEA', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PTOSIS REPAIR, FASANELLA-SERVAT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, AVASTIN 2.5 MG', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CATARACT PLAN', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EVISCERATION OF OCULAR CONTENTS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FUSIONAL DUCTIONS', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STRABISOMETER', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SUTURE OF RECENT WOUND, EYELID', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISUAL FIELD', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ORTHOPTIC TRAINING', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'MCCANNEL SUTURE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RED MACULA FIELD', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RED MACULA FIELD TECH', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INJECTION, IMMUNIZATION', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LUCENTIS 0.3MG, SAMPLE', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LUCENTIS 0.3MG, NO CHARGE', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, JETREA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PURSUITS, SACCADES, ACCOMODATION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BASE UP AND DOWN', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'BASE IN AND OUT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CATARACT WORKUP', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'GLASSES CHECK', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'RK ENHANCEMENT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LRI ENHANCEMENT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AK ENHANCEMENT', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REFRACTIVE FREE SCREEN', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INFUSION, SOLUMEDROL', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FUNDUS PHOTO (OPTOS)', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ISTENT INSERTION', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJECT, JETREA', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ANTERIOR SEGMENT PHOTO TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EXCISION OF LESION, BENIGN', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'EXCISION OF LESION, MALIGNANT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LID EVALUATION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISION UNCORRECTED D', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISION CORRECTED D', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AMNIOTIC MEMBRANE, SUTURED', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DESTRUCTION, PROLIFERATIVE LESIONS', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'HRT, MACULA TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DEEP SCLERECTOMY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LIPIVIEW', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LIPIFLOW', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRALASE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFRACTION', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, EYELID LESION (67850)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LASER, SKIN LESION (17000)', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ORBITAL FLOOR BLOWOUT TREATMENT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ORBITAL FLOOR BLOWOUT TREATMENT, IMPLANT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'IMPLANTABLE MINI TELESCOPE', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CL, REFIT, SPHERE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'ABN', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SPECULAR MICROSCOPY TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'PURSUITS', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'STRABISOMETER EXTENDED', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VISUAL PERCEPTION', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VERHOEFF STEREOPTOR', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'SPATIAL POSTURE', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECT, ANTERIOR PARTIAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'VITRECT, ANTERIOR SUBTOTAL', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'REVISION OF AQUEOUS SHUNT', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LC4', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LC5', 0, 2, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'DESTRUCTION OF LESION, CORNEA', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FARNSWORTH TECH', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'CONDUCTIVE KERATOPLASTY', 0, 3, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'AMENDMENT REQUEST', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'LIGHT DESATURATION', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'INTRAVIT INJEC, GANCICLOVIR', 0, 3, 1)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'FUNCTIONAL STATUS', 0, 2, 0)
GO
INSERT [model].[ClinicalProcedures] ([Name], [IsSocial], [ClinicalProcedureCategoryId], [IsDeactivated]) VALUES (N'COGNITIVE STATUS', 0, 2, 0)
GO

ALTER TABLE model.ClinicalProcedures ENABLE TRIGGER ALL;
ALTER TABLE model.ClinicalProcedures CHECK CONSTRAINT ALL;


-- adds tho the ClinicalProcedures table, the missing Clinical Procedures that exist in the pre-exist PatientClinical table.
--;WITH CTE AS (
--SELECT 
--pc.Symptom AS Name
--FROM dbo.PatientClinical pc
--LEFT JOIN model.ClinicalProcedures cp ON '/' + cp.Name = pc.Symptom
--WHERE pc.ClinicalType = 'F' 
--	AND cp.Id IS NULL
--GROUP BY pc.Symptom 
--)
--INSERT INTO model.ClinicalProcedures (Name, IsSocial, ClinicalProcedureId, IsDeactivated)
--SELECT
--SUBSTRING(Name,2,LEN(Name)) AS Name
--,CONVERT(BIT,0) AS IsSocial
--,NULL AS ClinicalProcedureCategoryId
--,CONVERT(BIT,0) AS IsDeactivated
--FROM CTE

--INSERT INTO model.ClinicalProcedures (Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated)
--select 
--code 
--,CONVERT(BIT,0) AS IsSocial
--,1 AS ClinicalProcedureCategoryId
--,CONVERT(BIT,0) AS IsDeactivated
--from PracticeCodeTable 
--where ReferenceType like 'SURGERYTYPE'

--except select Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated from model.ClinicalProcedures

--INSERT INTO model.ClinicalProcedures (Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated)
--SELECT DISTINCT
--[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=SCHEDULE\sSURGERY\-3\/)(.*?)(?=(\s\(O[D|S|U]\)\-F\/\-\d\/|\-F\/\-\d\/))') AS [Procedure]
--,CONVERT(BIT,0) AS IsSocial
--,1 AS ClinicalProcedureCategoryId
--,CONVERT(BIT,0) AS IsDeactivated
--FROM dbo.PatientClinical pc
--INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
--WHERE pc.FindingDetail LIKE 'SCHEDULE SURGERY-3/%' 

--except select Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated from model.ClinicalProcedures


--INSERT INTO model.ClinicalProcedures (Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated)
--SELECT DISTINCT
--[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[O|0]\/)(.*?)(?=\-F\/\-[O|1]\/)') AS [Procedure]
--,CONVERT(BIT,0) AS IsSocial
--,1 AS ClinicalProcedureCategoryId
--,CONVERT(BIT,0) AS IsDeactivated
--FROM dbo.PatientClinical pc
--INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
--WHERE (pc.FindingDetail LIKE 'OFFICE PROCEDURES-O%' OR pc.FindingDetail LIKE 'OFFICE PROCEDURES-0%')
--except select Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated from model.ClinicalProcedures