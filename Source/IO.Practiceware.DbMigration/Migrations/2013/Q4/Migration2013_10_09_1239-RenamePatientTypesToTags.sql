﻿



-- Rename the table 'model.PatienTypes' to 'model.Tags'.  This table will be a general tag table that can be referenced by any entity.  It will have a TagType column
-- to differentiate between the categories of tags.

IF OBJECT_ID(N'[model].[FK_PatientTagPatientType]', 'F') IS NOT NULL
    ALTER TABLE [model].[PatientTags] DROP CONSTRAINT [FK_PatientTagPatientType];
GO

IF OBJECT_ID(N'[model].[FK_PatientTagTag]', 'F') IS NOT NULL
    ALTER TABLE [model].[PatientTags] DROP CONSTRAINT [FK_PatientTagTag];
GO

IF OBJECT_ID(N'[model].[Tags]', 'U') IS NOT NULL
    DROP TABLE [model].[Tags];

GO

IF OBJECT_ID(N'[model].[Tags]', 'U') IS NULL
BEGIN
		-- Creating table 'Tags'
	CREATE TABLE [model].[Tags] (
		[Id] int IDENTITY(1,1) NOT NULL,
		[Name] nvarchar(max)  NOT NULL,
		[IsArchived] bit  NOT NULL,
		[DisplayName] nvarchar(max)  NOT NULL,
		[TagTypeId] int NOT NULL DEFAULT 0
	);
END

GO

IF OBJECT_ID(N'[model].[Tags]', 'U') IS NOT NULL	
BEGIN
	-- Creating primary key on [Id] in table 'Tags'
	ALTER TABLE [model].[Tags]
	ADD CONSTRAINT [PK_Tags]
		PRIMARY KEY CLUSTERED ([Id] ASC);
	

	-- drop old index on PatientTags.PatientTypeId
	IF EXISTS(SELECT * FROM sysindexes WHERE name = 'IX_FK_PatientTagPatientType')
    BEGIN
       DROP INDEX model.PatientTags.IX_FK_PatientTagPatientType
    END
END

GO

IF OBJECT_ID(N'[model].[Tags]', 'U') IS NOT NULL	
BEGIN

	-- Migrate PatientTypes table to new Tags table
	SET IDENTITY_INSERT [model].[Tags] ON
	INSERT INTO model.Tags (Id, Name, IsArchived, DisplayName, TagTypeId)
		SELECT Id,
			Name,
			IsArchived,
			DisplayName,
			1 /*   PatientType  */
		FROM model.PatientTypes;
	SET IDENTITY_INSERT [model].[Tags] OFF

	-- grab new id's from patient tag table
	UPDATE model.PatientTags SET PatientTypeId = t.Id	
	FROM model.Tags t INNER JOIN model.PatientTypes pt on t.Name = pt.Name 
		and t.DisplayName = pt.DisplayName and t.IsArchived=pt.IsArchived
	END

GO


IF OBJECT_ID(N'[model].[Tags]', 'U') IS NOT NULL	
BEGIN
	-- rename column to TagId
	IF EXISTS(select * from sys.columns where Name = N'PatientTypeId' and Object_ID = Object_ID(N'model.PatientTags')) 
	BEGIN
		EXEC sp_rename '[model].[PatientTags].[PatientTypeId]' , 'TagId', 'COLUMN';
	END
	
END

GO

IF OBJECT_ID(N'[model].[FK_PatientTagTag]', 'F') IS NULL
BEGIN
	-- Creating foreign key on [TagId] in table 'PatientTags'
	ALTER TABLE [model].[PatientTags]
	ADD CONSTRAINT [FK_PatientTagTag]
		FOREIGN KEY ([TagId])
		REFERENCES [model].[Tags]
			([Id])
		ON DELETE NO ACTION ON UPDATE NO ACTION;
END

GO

IF NOT EXISTS(SELECT * FROM sysindexes WHERE name = 'IX_FK_PatientTagTag')
BEGIN
	-- Creating non-clustered index for FOREIGN KEY 'FK_PatientTagTag' for 'PatientTags'
	CREATE INDEX [IX_FK_PatientTagTag]
	ON [model].[PatientTags]
		([TagId]);	
END

GO

IF OBJECT_ID(N'[model].[Tags]', 'U') IS NOT NULL	
BEGIN
	-- Drop old table	
	DROP TABLE [model].PatientTypes;

END

GO