﻿--Task #8804.
--Add Rows in model.TransitionOfCareReason
SET IDENTITY_INSERT [model].[TransitionOfCareReasons] ON

IF NOT EXISTS(SELECT TOP 1 * FROM model.TransitionOfCareReasons WHERE Value='Patient changing residence') 
INSERT INTO model.TransitionOfCareReasons(Id,Value) VALUES(1,'Patient changing residence')
GO
IF NOT EXISTS(SELECT TOP 1 * FROM model.TransitionOfCareReasons WHERE Value='Patient admitted to facility') 
INSERT INTO model.TransitionOfCareReasons(Id,Value) VALUES(2,'Patient admitted to facility')
GO
IF NOT EXISTS(SELECT TOP 1 * FROM model.TransitionOfCareReasons WHERE Value='Patient referred to specialist') 
INSERT INTO model.TransitionOfCareReasons(Id,Value) VALUES(3,'Patient referred to specialist')
GO
IF NOT EXISTS(SELECT TOP 1 * FROM model.TransitionOfCareReasons WHERE Value='Patient referred back to primary care provider') 
INSERT INTO model.TransitionOfCareReasons(Id,Value) VALUES(4,'Patient referred back to primary care provider')
GO
IF NOT EXISTS(SELECT TOP 1 * FROM model.TransitionOfCareReasons WHERE Value='Patient request') 
INSERT INTO model.TransitionOfCareReasons(Id,Value) VALUES(5,'Patient request')

SET IDENTITY_INSERT [model].[TransitionOfCareReasons] OFF
GO
