﻿IF OBJECT_ID('dbo.ChopWithTally', N'FN') IS NOT NULL
    DROP FUNCTION dbo.ChopWithTally
GO

CREATE FUNCTION dbo.ChopWithTally (@StringOfCharacters NVARCHAR(MAX), @StringPattern NVARCHAR(100))
RETURNS TABLE 
AS
RETURN(
WITH tallyCTE AS 
(
	SELECT 1 AS number
	UNION ALL 
	SELECT number + 1 AS number	FROM tallyCTE WHERE number <= 99
)
SELECT
number AS 'PositionOfMatch'
FROM tallyCTE
WHERE SUBSTRING(@StringOfCharacters, tallyCTE.number, LEN(@StringPattern)) = @StringPattern
);
GO