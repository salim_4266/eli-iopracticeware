﻿--Found database that these triggers existed and failed the migration.
IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PatientReceivableServicesPracticeServiceTrigger')
BEGIN
	DROP TRIGGER PatientReceivableServicesPracticeServiceTrigger
END

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PatientReceivableServicesQuantityChargeTrigger')
BEGIN
	DROP TRIGGER PatientReceivableServicesQuantityChargeTrigger
END

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'PatientReceivableServicesUpdate')
BEGIN
	DROP TRIGGER PatientReceivableServicesUpdate
END


--Fix for 7802.
--Update rows if any with charge greater than two decimal points.

IF OBJECT_ID('model.ServiceLocationCodes','V') IS NULL
BEGIN
EXEC('
CREATE VIEW [model].[ServiceLocationCodes]
AS
SELECT Id,
	SUBSTRING(Code, 4, LEN(code)) AS [Name],
	SUBSTRING(Code, 1, 2) AS PlaceOfServiceCode
FROM dbo.PracticeCodeTable
WHERE ReferenceType = ''PLACEOFSERVICE''
')
END


UPDATE PatientReceivableServices
SET Charge = CAST(Charge AS DECIMAL(18, 2))
WHERE (
		CASE 
			WHEN CHARINDEX('.', Charge, 0) > 0
				THEN LEN(SUBSTRING(CAST(Charge AS NVARCHAR(50)), CHARINDEX('.', charge, 0) + 1, LEN(charge) - (CHARINDEX('.', Charge, 0))))
			ELSE 0
			END
		) > 2
GO

-- Drop existing CHECK constraint for Charge on dbo.PatientReceivableServices
IF EXISTS (
		SELECT *
		FROM sys.check_constraints
		WHERE object_id = OBJECT_ID(N'[dbo].[CK_PatientReceivableServices_Charge]')
			AND parent_object_id = OBJECT_ID(N'[dbo].[PatientReceivableServices]')
		)
	ALTER TABLE [dbo].[PatientReceivableServices]

DROP CONSTRAINT CK_PatientReceivableServices_Charge
GO

IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[CheckServiceFee]')
			AND type IN (
				N'FN'
				,N'IF'
				,N'TF'
				,N'FS'
				,N'FT'
				)
		)
	DROP FUNCTION [dbo].[CheckServiceFee]
GO

--Function to check Service Amount
CREATE FUNCTION dbo.CheckServiceFee (@amount VARCHAR(50))
RETURNS INT
	WITH SCHEMABINDING
AS
BEGIN
	RETURN CASE 
			WHEN CHARINDEX('.', @amount, 0) > 0
				THEN LEN(SUBSTRING(CAST(@amount AS NVARCHAR(50)), CHARINDEX('.', @amount, 0) + 1, LEN(@amount) - (CHARINDEX('.', @amount, 0))))
			ELSE 0
			END
END
GO

-- Add CHECK constraint for PatientReceivableServices
ALTER TABLE dbo.[PatientReceivableServices]
	WITH CHECK ADD CONSTRAINT CK_PatientReceivableServices_Charge CHECK (dbo.CheckServiceFee(Charge) < 3)
GO

