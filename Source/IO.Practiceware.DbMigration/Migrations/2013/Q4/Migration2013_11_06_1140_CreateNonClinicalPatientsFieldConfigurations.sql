﻿-- Non-Clinical Patients Field configurations
SET IDENTITY_INSERT [model].[PatientDemographicsFieldConfigurations] ON

-- 47 - 'Date Of Birth'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (Id, IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (47, 0, 1, 0, 0);

-- 48 - 'Social Security Number'
INSERT INTO [model].[PatientDemographicsFieldConfigurations] (Id, IsVisibilityConfigurable, IsVisible, IsRequiredConfigurable, IsRequired) VALUES (48, 0, 1, 0, 0);

SET IDENTITY_INSERT [model].[PatientDemographicsFieldConfigurations] OFF
GO