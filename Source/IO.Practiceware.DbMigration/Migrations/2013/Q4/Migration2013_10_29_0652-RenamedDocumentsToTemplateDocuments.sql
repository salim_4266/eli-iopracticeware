﻿--Fix for Bug 7289.
IF OBJECT_ID(N'[model].[Documents]', 'U') IS NOT NULL
BEGIN
	-- rename column to TemplateDocumentCategoryId
	IF EXISTS (
			SELECT *
			FROM sys.columns
			WHERE NAME = N'DocumentTypeId'
				AND Object_ID = Object_ID(N'model.Documents')
			)
	BEGIN
		EXEC sp_rename '[model].[Documents].[DocumentTypeId]'
			,'TemplateDocumentCategoryId'
			,'COLUMN';
	END
END
GO

IF OBJECT_ID(N'[model].[DocumentPrinter]', 'U') IS NOT NULL
BEGIN
	-- rename column to TemplateDocuments_Id
	IF EXISTS (
			SELECT *
			FROM sys.columns
			WHERE NAME = N'Documents_Id'
				AND Object_ID = Object_ID(N'model.DocumentPrinter')
			)
	BEGIN
		EXEC sp_rename '[model].[DocumentPrinter].[Documents_Id]'
			,'TemplateDocuments_Id'
			,'COLUMN';
	END
END
GO

IF OBJECT_ID(N'[model].[PK_Documents]', 'PK') IS NOT NULL
BEGIN
	-- Rename the primary key constraint.
	EXEC sp_rename '[model].[PK_Documents]'
		,'PK_TemplateDocuments';
END
GO

IF OBJECT_ID(N'[model].[PK_DocumentPrinter]', 'PK') IS NOT NULL
BEGIN
	-- Rename the primary key constraint.
	EXEC sp_rename '[model].[PK_DocumentPrinter]'
		,'PK_TemplateDocumentPrinter';
END
GO

IF OBJECT_ID(N'[model].[FK_DocumentPrinter_Document]', 'F') IS NOT NULL
BEGIN
	-- Rename the Foreign key constraint.
	EXEC sp_rename '[model].[FK_DocumentPrinter_Document]'
		,'FK_TemplateDocumentPrinter_TemplateDocument';
END
GO

IF OBJECT_ID(N'[model].[FK_DocumentPrinter_Printer]', 'F') IS NOT NULL
BEGIN
	-- Rename the Foreign key constraint.
	EXEC sp_rename '[model].[FK_DocumentPrinter_Printer]'
		,'FK_TemplateDocumentPrinter_Printer';
END
GO

IF EXISTS (
		SELECT *
		FROM sysindexes
		WHERE NAME = 'IX_FK_DocumentPrinter_Printer'
		)
BEGIN
	-- Rename the Index.
	EXEC sp_rename '[model].[DocumentPrinter].[IX_FK_DocumentPrinter_Printer]'
		,'IX_FK_TemplateDocumentPrinter_Printer'
		,'INDEX';
END
GO

IF OBJECT_ID(N'[model].[DocumentPrinter]', 'U') IS NOT NULL
BEGIN
	--Rename the DocumentPrinter
	EXEC sp_rename '[model].[DocumentPrinter]'
		,'TemplateDocumentPrinter';
END
GO

IF OBJECT_ID(N'[model].[Documents]', 'U') IS NOT NULL
BEGIN
	--Rename the Documents
	EXEC sp_rename '[model].[Documents]'
		,'TemplateDocuments';
END
GO
IF OBJECT_ID(N'[model].[DocumentType_Enumeration]', 'U') IS NOT NULL
BEGIN
	--Drop the DocumentType_Enumeration
	DROP TABLE [model].[DocumentType_Enumeration];
END
GO
IF OBJECT_ID(N'[model].[ClaimFormTypes]', 'U') IS NOT NULL
BEGIN
	-- rename column to TemplateDocuments_Id
	IF EXISTS (
			SELECT *
			FROM sys.columns
			WHERE NAME = N'DocumentId'
				AND Object_ID = Object_ID(N'model.ClaimFormTypes')
			)
	BEGIN
		EXEC sp_rename '[model].[ClaimFormTypes].[DocumentId]'
			,'TemplateDocumentId'
			,'COLUMN';
	END
END
GO

IF OBJECT_ID(N'[model].[FK_DocumentClaimFormType]', 'F') IS NOT NULL
BEGIN
	-- Rename the Foreign key constraint.
	EXEC sp_rename '[model].[FK_DocumentClaimFormType]'
		,'FK_TemplateDocumentClaimFormType';
END
GO

IF EXISTS (
		SELECT *
		FROM sysindexes
		WHERE NAME = 'IX_FK_DocumentClaimFormType'
		)
BEGIN
	-- Rename the Index.
	EXEC sp_rename '[model].[ClaimFormTypes].[IX_FK_DocumentClaimFormType]'
		,'IX_FK_TemplateDocumentClaimFormType'
		,'INDEX';
END
GO
IF OBJECT_ID(N'[model].[PatientRecalls]', 'U') IS NOT NULL
BEGIN
	-- rename column to TemplateDocumentId
	IF EXISTS (
			SELECT *
			FROM sys.columns
			WHERE NAME = N'DocumentId'
				AND Object_ID = Object_ID(N'model.PatientRecalls')
			)
	BEGIN
		EXEC sp_rename '[model].[PatientRecalls].[DocumentId]'
			,'TemplateDocumentId'
			,'COLUMN';
	END
END
GO
--Drop Audit Trigger's
IF OBJECT_ID(N'[model].[AuditDocumentsDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditDocumentsDeleteTrigger];
END
GO
IF OBJECT_ID(N'[model].[AuditDocumentsInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditDocumentsInsertTrigger];
END
GO
IF OBJECT_ID(N'[model].[AuditDocumentsUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditDocumentsUpdateTrigger];
END
GO
IF OBJECT_ID(N'[model].[AuditDocumentPrinterDeleteTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditDocumentPrinterDeleteTrigger];
END
GO
IF OBJECT_ID(N'[model].[AuditDocumentPrinterInsertTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditDocumentPrinterInsertTrigger];
END
GO
IF OBJECT_ID(N'[model].[AuditDocumentPrinterUpdateTrigger]', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER [model].[AuditDocumentPrinterUpdateTrigger];
END
GO

--Changed PracticeCodeTableDocumentsTrigger Trigger
DECLARE @cmd VARCHAR(max)

SELECT @cmd = def
FROM (
	SELECT REPLACE(REPLACE(REPLACE(OBJECT_DEFINITION(object_id), 'CREATE TRIGGER', 'ALTER TRIGGER'), '.Documents', '.TemplateDocuments'), 'DocumentTypeId', 'TemplateDocumentCategoryId') AS def
		,object_id
	FROM sys.objects o
	WHERE type = 'TR'
		AND SCHEMA_NAME(schema_id) = 'dbo'
		AND OBJECT_DEFINITION(object_id) LIKE '%.Documents%'
		AND NAME = 'PracticeCodeTableDocumentsTrigger'
	) AS d

EXEC (@cmd)
GO

