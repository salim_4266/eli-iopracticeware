﻿IF NOT EXISTS (SELECT * FROM model.LaboratoryTestAbnormalFlags WHERE Name = 'Abnorm')
INSERT INTO model.LaboratoryTestAbnormalFlags (Abbreviation, Name)
VALUES ('Abnorm', 'Abnormal')

IF NOT EXISTS (SELECT * FROM model.LaboratoryTestAbnormalFlags WHERE Name = 'Norm')
INSERT INTO model.LaboratoryTestAbnormalFlags (Abbreviation, Name)
VALUES ('Norm', 'Normal')