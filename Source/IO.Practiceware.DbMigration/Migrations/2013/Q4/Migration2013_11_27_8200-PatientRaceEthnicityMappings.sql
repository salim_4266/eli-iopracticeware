﻿IF NOT EXISTS (SELECT * FROM model.PracticeRepositoryEntities WHERE Name = 'PatientRace')
BEGIN
	INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName)
	VALUES (37, 'PatientRace', 'Id')
END


IF NOT EXISTS (SELECT * FROM model.ExternalSystems WHERE Name = 'CDC')
BEGIN
	INSERT INTO model.ExternalSystems (Name)
	VALUES ('CDC')
END

IF NOT EXISTS (SELECT * FROM model.ExternalSystemEntities WHERE Name = 'RaceAndEthnicityCodeSet')
BEGIN 
	INSERT INTO model.ExternalSystemEntities (ExternalSystemId, Name)
	SELECT (SELECT Id FROM model.ExternalSystems WHERE Name = 'CDC'), 'RaceAndEthnicityCodeSet'
END

--Map PatientRace to external data
--Races
IF EXISTS(SELECT Id FROM model.PatientRaces WHERE Name = 'Native Amer')
BEGIN
	INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
	SELECT (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'PatientRace'), (SELECT Id FROM model.PatientRaces WHERE Name = 'Native Amer'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'RaceAndEthnicityCodeSet'), 1
END
IF EXISTS(SELECT Id FROM model.PatientRaces WHERE Name = 'Asian')
BEGIN
	INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
	SELECT (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'PatientRace'), (SELECT Id FROM model.PatientRaces WHERE Name = 'Asian'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'RaceAndEthnicityCodeSet'), 1
END
IF EXISTS(SELECT Id FROM model.PatientRaces WHERE Name = 'Black')
BEGIN
	INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
	SELECT (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'PatientRace'), (SELECT Id FROM model.PatientRaces WHERE Name = 'Black'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'RaceAndEthnicityCodeSet'), 1
END
IF EXISTS(SELECT Id FROM model.PatientRaces WHERE Name = 'Pacific Isl')
BEGIN
	INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
	SELECT (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'PatientRace'), (SELECT Id FROM model.PatientRaces WHERE Name = 'Pacific Isl'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'RaceAndEthnicityCodeSet'), 1
END
IF EXISTS(SELECT Id FROM model.PatientRaces WHERE Name = 'White')
BEGIN
	INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
	SELECT (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'PatientRace'), (SELECT Id FROM model.PatientRaces WHERE Name = 'White'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'RaceAndEthnicityCodeSet'), 1
END
IF EXISTS(SELECT Id FROM model.PatientRaces WHERE Name = 'Other')
BEGIN
	INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
	SELECT (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'PatientRace'), (SELECT Id FROM model.PatientRaces WHERE Name = 'Other'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'RaceAndEthnicityCodeSet'), 1
END
IF EXISTS(SELECT Id FROM model.PatientEthnicities WHERE Name = 'Hisp')
BEGIN
	INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
	SELECT (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'PatientEthnicity'), (SELECT Id FROM model.PatientEthnicities WHERE Name = 'Hisp'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'RaceAndEthnicityCodeSet'), 1
END
IF EXISTS(SELECT Id FROM model.PatientEthnicities WHERE Name = 'Not Hisp')
BEGIN
	INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
	SELECT (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'PatientEthnicity'), (SELECT Id FROM model.PatientEthnicities WHERE Name = 'Not Hisp'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'RaceAndEthnicityCodeSet'), 1
END