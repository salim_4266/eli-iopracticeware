﻿--Fix for 7511.Renamed Contacts to ExternalContacts
UPDATE [model].[PracticeRepositoryEntities]
SET NAME = 'ExternalContact'
WHERE Id = 7
GO

IF (
		EXISTS (
			SELECT *
			FROM INFORMATION_SCHEMA.TABLES
			WHERE TABLE_SCHEMA = 'model'
				AND TABLE_NAME = 'ContactAddressTypes'
			)
		)
BEGIN
	EXEC DropObject 'model.ContactAddressTypes';
END
GO

-- Creating table 'ExternalContactAddressTypes'
IF NOT (
		EXISTS (
			SELECT *
			FROM INFORMATION_SCHEMA.TABLES
			WHERE TABLE_SCHEMA = 'model'
				AND TABLE_NAME = 'ExternalContactAddressTypes'
			)
		)
BEGIN
	CREATE TABLE [model].[ExternalContactAddressTypes] (
		[Id] INT IDENTITY(1, 1) NOT NULL
		,[Name] NVARCHAR(max) NOT NULL
		,[__EntityType__] NVARCHAR(100) NOT NULL
		,[IsArchived] BIT NOT NULL
		,
		);
END
GO

-- Creating primary key on [Id] in table 'ContactAddressTypes'
IF NOT EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
		WHERE TABLE_NAME = 'ExternalContactAddressTypes'
			AND TABLE_SCHEMA = 'model'
			AND CONSTRAINT_TYPE = 'PRIMARY KEY'
		)
BEGIN
	ALTER TABLE [model].[ExternalContactAddressTypes] ADD CONSTRAINT [PK_ExternalContactAddressTypes] PRIMARY KEY CLUSTERED ([Id] ASC);
END
GO


IF NOT EXISTS(SELECT * FROM model.ExternalContactAddressTypes WHERE Id=8 AND Name='Main Office')
BEGIN
SET IDENTITY_INSERT [model].[ExternalContactAddressTypes] ON
	INSERT [model].[ExternalContactAddressTypes] (
		Id
		,NAME
		,__EntityType__
		,[IsArchived]
		)
	VALUES (
		8
		,'Main Office'
		,'ExternalContactAddressType'
		,0
		)
SET IDENTITY_INSERT [model].[ExternalContactAddressTypes] OFF
END

GO

DBCC CHECKIDENT (
		'[model].[ExternalContactAddressTypes]'
		,'RESEED'
		,1000
		)
GO

INSERT [model].[ExternalContactAddressTypes] (
	NAME
	,__EntityType__
	,[IsArchived]
	)
VALUES (
	'Other1'
	,'ExternalContactAddressType'
	,0
	)

INSERT [model].[ExternalContactAddressTypes] (
	NAME
	,__EntityType__
	,[IsArchived]
	)
VALUES (
	'Other2'
	,'ExternalContactAddressType'
	,0
	)
GO

--Changed OnPatientDemographicInsertAndUpdate Trigger
DECLARE @cmd VARCHAR(max)

SELECT @cmd = def
FROM (
	SELECT REPLACE(REPLACE(OBJECT_DEFINITION(object_id), 'CREATE TRIGGER', 'ALTER TRIGGER'), 'Contacts', 'ExternalContacts') AS def
		,object_id
	FROM sys.objects o
	WHERE type = 'TR'
		AND SCHEMA_NAME(schema_id) = 'dbo'
		AND OBJECT_DEFINITION(object_id) LIKE '%.Contacts%'
		AND NAME = 'OnPatientDemographicInsertAndUpdate'
	) AS d

EXEC (@cmd)
GO

