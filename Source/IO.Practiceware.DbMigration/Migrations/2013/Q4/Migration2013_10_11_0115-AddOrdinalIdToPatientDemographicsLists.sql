﻿IF NOT EXISTS (SELECT * FROM sysobjects so
		INNER JOIN syscolumns sc ON so.id = sc.id
		WHERE so.NAME = 'PatientAddressTypes' AND sc.NAME = 'OrdinalId')
BEGIN
	ALTER TABLE model.PatientAddressTypes ADD OrdinalId int NOT NULL default 1
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects so
		INNER JOIN syscolumns sc ON so.id = sc.id
		WHERE so.NAME = 'PatientEthnicities' AND sc.NAME = 'OrdinalId')
BEGIN
	ALTER TABLE model.PatientEthnicities ADD OrdinalId int NOT NULL default 1
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects so
		INNER JOIN syscolumns sc ON so.id = sc.id
		WHERE so.NAME = 'PatientLanguages' AND sc.NAME = 'OrdinalId')
BEGIN
	ALTER TABLE model.PatientLanguages ADD OrdinalId int NOT NULL default 1
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects so
		INNER JOIN syscolumns sc ON so.id = sc.id
		WHERE so.NAME = 'PatientRaces' AND sc.NAME = 'OrdinalId')
BEGIN
	ALTER TABLE model.PatientRaces ADD OrdinalId int NOT NULL default 1
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects so
		INNER JOIN syscolumns sc ON so.id = sc.id
		WHERE so.NAME = 'PatientReferralSourceTypes' AND sc.NAME = 'OrdinalId')
BEGIN
	ALTER TABLE model.PatientReferralSourceTypes ADD OrdinalId int NOT NULL default 1
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects so
		INNER JOIN syscolumns sc ON so.id = sc.id
		WHERE so.NAME = 'PracticeVendors' AND sc.NAME = 'OrdinalId')
BEGIN
	ALTER TABLE dbo.PracticeVendors ADD OrdinalId int NOT NULL default 1
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects so
		INNER JOIN syscolumns sc ON so.id = sc.id
		WHERE so.NAME = 'Resources' AND sc.NAME = 'OrdinalId')
BEGIN
	ALTER TABLE dbo.Resources ADD OrdinalId int NOT NULL default 1
END
GO


IF EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'ExternalContacts' and schemas.name = 'model') 

BEGIN

EXEC ('ALTER VIEW [model].[ExternalContacts]
	AS
	----ExternalProvider contact
	SELECT VendorId AS Id,
		VendorName AS DisplayName,
		CASE 
			WHEN VendorFirmName <> '''' AND VendorFirmName IS NOT NULL
				THEN VendorId 
			ELSE NULL 
			END AS ExternalOrganizationId,
		VendorFirstName AS FirstName,
		VendorTitle AS Honorific,
		VendorLastName AS LastName,
		VendorMI AS MiddleName,
		VendorSalutation AS NickName,
		CONVERT(NVARCHAR, NULL) AS Prefix,
		VendorSalutation AS Salutation,
		CONVERT(NVARCHAR, NULL) AS Suffix,
		CONVERT(bit, 0) AS IsArchived,
		CASE  
			WHEN VendorExcRefDr = ''F'' OR VendorExcRefDr IS NULL OR VendorExcRefDr = ''''
				THEN CONVERT(bit, 0)
			WHEN VendorExcRefDr = ''T''
				THEN CONVERT(bit, 1)
			END AS ExcludeOnClaim,
		OrdinalId,
		''ExternalProvider'' AS __EntityType__
	FROM dbo.PracticeVendors
	WHERE VendorLastName <> ''''
		AND VendorLastName IS NOT NULL
		AND VendorType = ''D'' ')

END
GO

IF EXISTS(SELECT  sys.objects.name, sys.schemas.name AS schema_name
FROM    sys.objects 
INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id
WHERE objects.name = 'Users' and schemas.name = 'model') 

BEGIN

EXEC ('ALTER VIEW [model].[Users]
	
	AS
	SELECT re.ResourceId AS Id,
		re.ResourceDescription AS DisplayName,
		re.ResourceFirstName AS FirstName,
		re.ResourceSuffix AS Honorific,
		CASE 
			WHEN re.ResourceType IN (''Z'', ''X'', ''Y'')
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
			END AS IsArchived,
		re.IsLoggedIn AS IsLoggedIn,
		CASE 
			WHEN ResourceType NOT IN (''Q'', ''D'') 
				THEN CONVERT(bit, 0) 
			WHEN re.ResourceType IN (''D'', ''Q'')
				THEN CONVERT(bit, 1)
			END AS IsSchedulable,
		re.ResourceLastName AS LastName,
		re.ResourceMI AS MiddleName,
		re.ResourceColor as OleColor,
		ResourcePid AS PID,
		re.ResourcePrefix AS Prefix,
		CONVERT(nvarchar, NULL) AS Suffix,
		re.ResourceName AS UserName
		,OrdinalId
		,CASE 
		 WHEN ResourceType NOT IN (''D'', ''Q'', ''Z'', ''Y'')
			THEN ''User'' 
		WHEN re.ResourceType IN (''D'', ''Q'', ''Z'', ''Y'')
			THEN ''Doctor'' 
		END AS __EntityType__
	FROM dbo.Resources re
	WHERE ResourceType <> ''R'' AND ResourceId > 0')

END

