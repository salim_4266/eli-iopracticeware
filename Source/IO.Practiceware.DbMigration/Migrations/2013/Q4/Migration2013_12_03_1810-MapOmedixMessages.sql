﻿IF NOT EXISTS (SELECT * FROM model.ExternalSystems WHERE Name = 'Omedix')
INSERT INTO model.ExternalSystems (Name)
VALUES ('Omedix')

DECLARE @OmedixExternal int
SET @OmedixExternal = (SELECT Id FROM model.ExternalSystems WHERE Name = 'Omedix')

DECLARE @MessageTypePatient int
SET @MessageTypePatient = (SELECT Id from model.ExternalSystemMessageTypes WHERE Name = 'PatientTransitionOfCareCCDA' AND IsOutbound = 1)

DECLARE @MessageTypeEncounter int
SET @MessageTypeEncounter = (SELECT Id from model.ExternalSystemMessageTypes WHERE Name = 'EncounterClinicalSummaryCCDA' AND IsOutbound = 1)


IF NOT EXISTS (SELECT * FROM model.ExternalSystemExternalSystemMessageTypes WHERE ExternalSystemId = @OmedixExternal AND ExternalSystemMessageTypeId = @MessageTypePatient)
INSERT INTO model.ExternalSystemExternalSystemMessageTypes (ExternalSystemId, ExternalSystemMessageTypeId)
SELECT @OmedixExternal, @MessageTypePatient

IF NOT EXISTS (SELECT * FROM model.ExternalSystemExternalSystemMessageTypes WHERE ExternalSystemId = @OmedixExternal AND ExternalSystemMessageTypeId = @MessageTypeEncounter)
INSERT INTO model.ExternalSystemExternalSystemMessageTypes (ExternalSystemId, ExternalSystemMessageTypeId)
SELECT @OmedixExternal, @MessageTypeEncounter
