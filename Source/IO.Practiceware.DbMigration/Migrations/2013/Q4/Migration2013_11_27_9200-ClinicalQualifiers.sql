﻿INSERT INTO model.ClinicalQualifierCategories (IsSingleUse,Name,OrdinalId)
SELECT
CASE 
	WHEN SUBSTRING(ReferenceType,LEN('QUANTIFIERS')+1,LEN(ReferenceType)) = 'DESCRIPTORS'
		THEN CONVERT(BIT,0) 
	WHEN ReferenceType = 'QUANTIFIERS'
		THEN CONVERT(BIT,0) 
	ELSE CONVERT(BIT,1) 
END AS IsSingleUse
,SUBSTRING(ReferenceType,LEN('QUANTIFIERS')+1,LEN(ReferenceType)) AS Name
,1 AS OrdinalId
FROM PracticeCodeTable pct
WHERE ReferenceType LIKE '%QUANTIFIERS%' AND ReferenceType <> 'QUANTIFIERS'
GROUP BY ReferenceType

INSERT INTO model.ClinicalQualifiers (Name, ClinicalQualifierCategoryId, SimpleDescription, TechnicalDescription
	,IsDeactivated, OrdinalId)
SELECT 
Code AS Name
,q.Id AS ClinicalQualifierCategoryId
,pct.OtherLtrTrans AS SimpleDescription
,pct.LetterTranslation AS TechnicalDescription
,CONVERT(BIT,0) AS IsDeactivated
,1 AS OrdinalId
FROM PracticeCodeTable pct
INNER JOIN model.ClinicalQualifierCategories q ON 'QUANTIFIERS'+q.Name = pct.ReferenceType
WHERE pct.ReferenceType LIKE '%QUANTIFIER%' AND ReferenceType <> 'QUANTIFIERS'


IF NOT EXISTS(SELECT * FROM model.ClinicalQualifiers)
BEGIN
-- Dummy data since the above required metadata doesn't exist yet.
INSERT INTO model.ClinicalQualifiers(
[SimpleDescription]
,[TechnicalDescription] 
,[Name]
,[ClinicalQualifierCategoryId]
,[IsDeactivated]
,[OrdinalId]
)
SELECT
NULL
,NULL
,NULL
,NULL
,CONVERT(BIT,1)
,1

INSERT INTO model.ClinicalQualifiers(
[SimpleDescription]
,[TechnicalDescription] 
,[Name]
,[ClinicalQualifierCategoryId]
,[IsDeactivated]
,[OrdinalId]
)
SELECT
NULL
,NULL
,NULL
,NULL
,CONVERT(BIT,1)
,1

INSERT INTO model.ClinicalQualifiers(
[SimpleDescription]
,[TechnicalDescription] 
,[Name]
,[ClinicalQualifierCategoryId]
,[IsDeactivated]
,[OrdinalId]
)
SELECT
NULL
,NULL
,NULL
,NULL
,CONVERT(BIT,1)
,1

END