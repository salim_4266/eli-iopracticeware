﻿
SET QUOTED_IDENTIFIER OFF;
GO
IF SCHEMA_ID(N'model') IS NULL EXECUTE(N'CREATE SCHEMA [model]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[model].[FK_ReasonForVisitQuality_ReasonForVisit]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ReasonForVisitQuality]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ReasonForVisitQuality] DROP CONSTRAINT [FK_ReasonForVisitQuality_ReasonForVisit];
GO
IF OBJECT_ID(N'[model].[FK_ReasonForVisitQuality_Quality]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ReasonForVisitQuality]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ReasonForVisitQuality] DROP CONSTRAINT [FK_ReasonForVisitQuality_Quality];
GO
IF OBJECT_ID(N'[model].[FK_ReasonForVisitContext_ReasonForVisit]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ReasonForVisitContext]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ReasonForVisitContext] DROP CONSTRAINT [FK_ReasonForVisitContext_ReasonForVisit];
GO
IF OBJECT_ID(N'[model].[FK_ReasonForVisitContext_Context]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ReasonForVisitContext]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ReasonForVisitContext] DROP CONSTRAINT [FK_ReasonForVisitContext_Context];
GO
IF OBJECT_ID(N'[model].[FK_EncounterReasonForVisitReasonForVisit_EncounterReasonForVisit]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReasonForVisitReasonForVisit]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterReasonForVisitReasonForVisit] DROP CONSTRAINT [FK_EncounterReasonForVisitReasonForVisit_EncounterReasonForVisit];
GO
IF OBJECT_ID(N'[model].[FK_EncounterReasonForVisitReasonForVisit_ReasonForVisit]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReasonForVisitReasonForVisit]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterReasonForVisitReasonForVisit] DROP CONSTRAINT [FK_EncounterReasonForVisitReasonForVisit_ReasonForVisit];
GO
IF OBJECT_ID(N'[model].[FK_ModifyingFactorEncounterReasonForVisit_ModifyingFactor]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ModifyingFactorEncounterReasonForVisit]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ModifyingFactorEncounterReasonForVisit] DROP CONSTRAINT [FK_ModifyingFactorEncounterReasonForVisit_ModifyingFactor];
GO
IF OBJECT_ID(N'[model].[FK_ModifyingFactorEncounterReasonForVisit_EncounterReasonForVisit]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ModifyingFactorEncounterReasonForVisit]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ModifyingFactorEncounterReasonForVisit] DROP CONSTRAINT [FK_ModifyingFactorEncounterReasonForVisit_EncounterReasonForVisit];
GO
IF OBJECT_ID(N'[model].[FK_AssociatedSignAndSymptomEncounterReasonForVisit_AssociatedSignAndSymptom]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[AssociatedSignAndSymptomEncounterReasonForVisit]'), 'IsUserTable') = 1
    ALTER TABLE [model].[AssociatedSignAndSymptomEncounterReasonForVisit] DROP CONSTRAINT [FK_AssociatedSignAndSymptomEncounterReasonForVisit_AssociatedSignAndSymptom];
GO
IF OBJECT_ID(N'[model].[FK_AssociatedSignAndSymptomEncounterReasonForVisit_EncounterReasonForVisit]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[AssociatedSignAndSymptomEncounterReasonForVisit]'), 'IsUserTable') = 1
    ALTER TABLE [model].[AssociatedSignAndSymptomEncounterReasonForVisit] DROP CONSTRAINT [FK_AssociatedSignAndSymptomEncounterReasonForVisit_EncounterReasonForVisit];
GO
IF OBJECT_ID(N'[model].[FK_DiagnosticTestOrderTreatmentPlan_DiagnosticTestOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[DiagnosticTestOrderTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[DiagnosticTestOrderTreatmentPlan] DROP CONSTRAINT [FK_DiagnosticTestOrderTreatmentPlan_DiagnosticTestOrder];
GO
IF OBJECT_ID(N'[model].[FK_DiagnosticTestOrderTreatmentPlan_TreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[DiagnosticTestOrderTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[DiagnosticTestOrderTreatmentPlan] DROP CONSTRAINT [FK_DiagnosticTestOrderTreatmentPlan_TreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_FollowUpVisitTreatmentPlan_FollowUpVisit]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[FollowUpVisitTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[FollowUpVisitTreatmentPlan] DROP CONSTRAINT [FK_FollowUpVisitTreatmentPlan_FollowUpVisit];
GO
IF OBJECT_ID(N'[model].[FK_FollowUpVisitTreatmentPlan_TreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[FollowUpVisitTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[FollowUpVisitTreatmentPlan] DROP CONSTRAINT [FK_FollowUpVisitTreatmentPlan_TreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_ProcedureOrderTreatmentPlan_ProcedureOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ProcedureOrderTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ProcedureOrderTreatmentPlan] DROP CONSTRAINT [FK_ProcedureOrderTreatmentPlan_ProcedureOrder];
GO
IF OBJECT_ID(N'[model].[FK_ProcedureOrderTreatmentPlan_TreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ProcedureOrderTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ProcedureOrderTreatmentPlan] DROP CONSTRAINT [FK_ProcedureOrderTreatmentPlan_TreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_LaboratoryTestOrderTreatmentPlan_LaboratoryTestOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTestOrderTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[LaboratoryTestOrderTreatmentPlan] DROP CONSTRAINT [FK_LaboratoryTestOrderTreatmentPlan_LaboratoryTestOrder];
GO
IF OBJECT_ID(N'[model].[FK_LaboratoryTestOrderTreatmentPlan_TreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTestOrderTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[LaboratoryTestOrderTreatmentPlan] DROP CONSTRAINT [FK_LaboratoryTestOrderTreatmentPlan_TreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_CommunicationWithOtherProviderTreatmentPlan_CommunicationWithOtherProvider]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[CommunicationWithOtherProviderTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[CommunicationWithOtherProviderTreatmentPlan] DROP CONSTRAINT [FK_CommunicationWithOtherProviderTreatmentPlan_CommunicationWithOtherProvider];
GO
IF OBJECT_ID(N'[model].[FK_CommunicationWithOtherProviderTreatmentPlan_TreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[CommunicationWithOtherProviderTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[CommunicationWithOtherProviderTreatmentPlan] DROP CONSTRAINT [FK_CommunicationWithOtherProviderTreatmentPlan_TreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_TransitionOfCareTreatmentPlan_TransitionOfCare]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TransitionOfCareTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TransitionOfCareTreatmentPlan] DROP CONSTRAINT [FK_TransitionOfCareTreatmentPlan_TransitionOfCare];
GO
IF OBJECT_ID(N'[model].[FK_TransitionOfCareTreatmentPlan_TreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TransitionOfCareTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TransitionOfCareTreatmentPlan] DROP CONSTRAINT [FK_TransitionOfCareTreatmentPlan_TreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_OtherOrderTreatmentPlan_OtherOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[OtherOrderTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[OtherOrderTreatmentPlan] DROP CONSTRAINT [FK_OtherOrderTreatmentPlan_OtherOrder];
GO
IF OBJECT_ID(N'[model].[FK_OtherOrderTreatmentPlan_TreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[OtherOrderTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[OtherOrderTreatmentPlan] DROP CONSTRAINT [FK_OtherOrderTreatmentPlan_TreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_RefractivePrescriptionTreatmentPlan_RefractivePrescription]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[RefractivePrescriptionTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[RefractivePrescriptionTreatmentPlan] DROP CONSTRAINT [FK_RefractivePrescriptionTreatmentPlan_RefractivePrescription];
GO
IF OBJECT_ID(N'[model].[FK_RefractivePrescriptionTreatmentPlan_TreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[RefractivePrescriptionTreatmentPlan]'), 'IsUserTable') = 1
    ALTER TABLE [model].[RefractivePrescriptionTreatmentPlan] DROP CONSTRAINT [FK_RefractivePrescriptionTreatmentPlan_TreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_ProcedureOrderAppointmentType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ProcedureOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ProcedureOrders] DROP CONSTRAINT [FK_ProcedureOrderAppointmentType];
GO
IF OBJECT_ID(N'[model].[FK_MedicationMedicationFamily_Medication]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[MedicationMedicationFamily]'), 'IsUserTable') = 1
    ALTER TABLE [model].[MedicationMedicationFamily] DROP CONSTRAINT [FK_MedicationMedicationFamily_Medication];
GO
IF OBJECT_ID(N'[model].[FK_MedicationMedicationFamily_MedicationFamily]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[MedicationMedicationFamily]'), 'IsUserTable') = 1
    ALTER TABLE [model].[MedicationMedicationFamily] DROP CONSTRAINT [FK_MedicationMedicationFamily_MedicationFamily];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentPlanTreatmentGoal_TreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentPlanTreatmentGoal]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentPlanTreatmentGoal] DROP CONSTRAINT [FK_TreatmentPlanTreatmentGoal_TreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentPlanTreatmentGoal_TreatmentGoal]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentPlanTreatmentGoal]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentPlanTreatmentGoal] DROP CONSTRAINT [FK_TreatmentPlanTreatmentGoal_TreatmentGoal];
GO
IF OBJECT_ID(N'[model].[FK_ProcedureOrderProcedurePerformed_ProcedureOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ProcedureOrderProcedurePerformed]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ProcedureOrderProcedurePerformed] DROP CONSTRAINT [FK_ProcedureOrderProcedurePerformed_ProcedureOrder];
GO
IF OBJECT_ID(N'[model].[FK_ProcedureOrderProcedurePerformed_ProcedurePerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ProcedureOrderProcedurePerformed]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ProcedureOrderProcedurePerformed] DROP CONSTRAINT [FK_ProcedureOrderProcedurePerformed_ProcedurePerformed];
GO
IF OBJECT_ID(N'[model].[FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[DiagnosticTestOrderDiagnosticTestPerformed]'), 'IsUserTable') = 1
    ALTER TABLE [model].[DiagnosticTestOrderDiagnosticTestPerformed] DROP CONSTRAINT [FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestOrder];
GO
IF OBJECT_ID(N'[model].[FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestPerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[DiagnosticTestOrderDiagnosticTestPerformed]'), 'IsUserTable') = 1
    ALTER TABLE [model].[DiagnosticTestOrderDiagnosticTestPerformed] DROP CONSTRAINT [FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestPerformed];
GO
IF OBJECT_ID(N'[model].[FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTestOrderLaboratoryTestResult]'), 'IsUserTable') = 1
    ALTER TABLE [model].[LaboratoryTestOrderLaboratoryTestResult] DROP CONSTRAINT [FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestOrder];
GO
IF OBJECT_ID(N'[model].[FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestResult]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTestOrderLaboratoryTestResult]'), 'IsUserTable') = 1
    ALTER TABLE [model].[LaboratoryTestOrderLaboratoryTestResult] DROP CONSTRAINT [FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestResult];
GO
IF OBJECT_ID(N'[model].[FK_MedicationClinicalSpecialtyType_Medication]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[MedicationClinicalSpecialtyType]'), 'IsUserTable') = 1
    ALTER TABLE [model].[MedicationClinicalSpecialtyType] DROP CONSTRAINT [FK_MedicationClinicalSpecialtyType_Medication];
GO
IF OBJECT_ID(N'[model].[FK_MedicationClinicalSpecialtyType_ClinicalSpecialtyType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[MedicationClinicalSpecialtyType]'), 'IsUserTable') = 1
    ALTER TABLE [model].[MedicationClinicalSpecialtyType] DROP CONSTRAINT [FK_MedicationClinicalSpecialtyType_ClinicalSpecialtyType];
GO
IF OBJECT_ID(N'[model].[FK_EncounterMedicationOrderEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterMedicationOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterMedicationOrders] DROP CONSTRAINT [FK_EncounterMedicationOrderEncounter];
GO
IF OBJECT_ID(N'[model].[FK_EncounterLaboratoryTestOrderEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterLaboratoryTestOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT [FK_EncounterLaboratoryTestOrderEncounter];
GO
IF OBJECT_ID(N'[model].[FK_EncounterCommunicationWithOtherProviderOrderEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterCommunicationWithOtherProviderOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders] DROP CONSTRAINT [FK_EncounterCommunicationWithOtherProviderOrderEncounter];
GO
IF OBJECT_ID(N'[model].[FK_EncounterProcedureOrderEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterProcedureOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT [FK_EncounterProcedureOrderEncounter];
GO
IF OBJECT_ID(N'[model].[FK_EncounterCommunicationWithOtherProviderOrderCommunicationWithOtherProviderOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterCommunicationWithOtherProviderOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders] DROP CONSTRAINT [FK_EncounterCommunicationWithOtherProviderOrderCommunicationWithOtherProviderOrder];
GO
IF OBJECT_ID(N'[model].[FK_PatientSpecialExamPerformedEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientSpecialExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientSpecialExamPerformeds] DROP CONSTRAINT [FK_PatientSpecialExamPerformedEncounter];
GO
IF OBJECT_ID(N'[model].[FK_PatientOtherElementPerformedEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientOtherElementPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientOtherElementPerformeds] DROP CONSTRAINT [FK_PatientOtherElementPerformedEncounter];
GO
IF OBJECT_ID(N'[model].[FK_EncounterTreatmentPlanCommentEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterTreatmentPlanComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterTreatmentPlanComments] DROP CONSTRAINT [FK_EncounterTreatmentPlanCommentEncounter];
GO
IF OBJECT_ID(N'[model].[FK_EncounterLaboratoryTestOrderLaboratoryTestOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterLaboratoryTestOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT [FK_EncounterLaboratoryTestOrderLaboratoryTestOrder];
GO
IF OBJECT_ID(N'[model].[FK_EncounterLaboratoryTestOrderExternalOrganization]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterLaboratoryTestOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT [FK_EncounterLaboratoryTestOrderExternalOrganization];
GO
IF OBJECT_ID(N'[model].[FK_EncounterProcedureOrderProcedureOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterProcedureOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT [FK_EncounterProcedureOrderProcedureOrder];
GO
IF OBJECT_ID(N'[model].[FK_EncounterRefractivePrescriptionEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterRefractivePrescriptions]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterRefractivePrescriptions] DROP CONSTRAINT [FK_EncounterRefractivePrescriptionEncounter];
GO
IF OBJECT_ID(N'[model].[FK_EncounterOtherTreatmentPlanOrderEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterOtherTreatmentPlanOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterOtherTreatmentPlanOrders] DROP CONSTRAINT [FK_EncounterOtherTreatmentPlanOrderEncounter];
GO
IF OBJECT_ID(N'[model].[FK_CommunicationWithOtherProviderOrderTemplateDocument]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[CommunicationWithOtherProviderOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[CommunicationWithOtherProviderOrders] DROP CONSTRAINT [FK_CommunicationWithOtherProviderOrderTemplateDocument];
GO
IF OBJECT_ID(N'[model].[FK_MedicationOrderFavoriteMedication]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[MedicationOrderFavorites]'), 'IsUserTable') = 1
    ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT [FK_MedicationOrderFavoriteMedication];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentPlanMedicationOrderFavorite_TreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentPlanMedicationOrderFavorite]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentPlanMedicationOrderFavorite] DROP CONSTRAINT [FK_TreatmentPlanMedicationOrderFavorite_TreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentPlanMedicationOrderFavorite_MedicationOrderFavorite]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentPlanMedicationOrderFavorite]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentPlanMedicationOrderFavorite] DROP CONSTRAINT [FK_TreatmentPlanMedicationOrderFavorite_MedicationOrderFavorite];
GO
IF OBJECT_ID(N'[model].[FK_OrganStructureGroupOrganStructure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[OrganStructures]'), 'IsUserTable') = 1
    ALTER TABLE [model].[OrganStructures] DROP CONSTRAINT [FK_OrganStructureGroupOrganStructure];
GO
IF OBJECT_ID(N'[model].[FK_BodyPartClinicalSpecialtyType_BodyPart]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[BodyPartClinicalSpecialtyType]'), 'IsUserTable') = 1
    ALTER TABLE [model].[BodyPartClinicalSpecialtyType] DROP CONSTRAINT [FK_BodyPartClinicalSpecialtyType_BodyPart];
GO
IF OBJECT_ID(N'[model].[FK_BodyPartClinicalSpecialtyType_ClinicalSpecialtyType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[BodyPartClinicalSpecialtyType]'), 'IsUserTable') = 1
    ALTER TABLE [model].[BodyPartClinicalSpecialtyType] DROP CONSTRAINT [FK_BodyPartClinicalSpecialtyType_ClinicalSpecialtyType];
GO
IF OBJECT_ID(N'[model].[FK_OrganBodySystem]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[Organs]'), 'IsUserTable') = 1
    ALTER TABLE [model].[Organs] DROP CONSTRAINT [FK_OrganBodySystem];
GO
IF OBJECT_ID(N'[model].[FK_EncounterProcedureOrderTimeQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterProcedureOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT [FK_EncounterProcedureOrderTimeQualifier];
GO
IF OBJECT_ID(N'[model].[FK_EncounterProcedureOrderServiceLocation]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterProcedureOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT [FK_EncounterProcedureOrderServiceLocation];
GO
IF OBJECT_ID(N'[model].[FK_PatientMedicationProgressCommentPatientMedication]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientMedicationProgressComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientMedicationProgressComments] DROP CONSTRAINT [FK_PatientMedicationProgressCommentPatientMedication];
GO
IF OBJECT_ID(N'[model].[FK_MedicationOrderFavoriteDrugDispenseForm]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[MedicationOrderFavorites]'), 'IsUserTable') = 1
    ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT [FK_MedicationOrderFavoriteDrugDispenseForm];
GO
IF OBJECT_ID(N'[model].[FK_MedicationOrderFavoriteDrugDosageNumber]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[MedicationOrderFavorites]'), 'IsUserTable') = 1
    ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT [FK_MedicationOrderFavoriteDrugDosageNumber];
GO
IF OBJECT_ID(N'[model].[FK_MedicationOrderFavoriteFrequency]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[MedicationOrderFavorites]'), 'IsUserTable') = 1
    ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT [FK_MedicationOrderFavoriteFrequency];
GO
IF OBJECT_ID(N'[model].[FK_MedicationOrderFavoriteRouteOfAdministration]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[MedicationOrderFavorites]'), 'IsUserTable') = 1
    ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT [FK_MedicationOrderFavoriteRouteOfAdministration];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisPatient]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnoses]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnoses] DROP CONSTRAINT [FK_PatientDiagnosisPatient];
GO
IF OBJECT_ID(N'[model].[FK_EncounterClinicalDrawingEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterClinicalDrawings]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterClinicalDrawings] DROP CONSTRAINT [FK_EncounterClinicalDrawingEncounter];
GO
IF OBJECT_ID(N'[model].[FK_BodyPartOrgan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[BodyParts]'), 'IsUserTable') = 1
    ALTER TABLE [model].[BodyParts] DROP CONSTRAINT [FK_BodyPartOrgan];
GO
IF OBJECT_ID(N'[model].[FK_BodyLocationBodyPart]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[BodyLocations]'), 'IsUserTable') = 1
    ALTER TABLE [model].[BodyLocations] DROP CONSTRAINT [FK_BodyLocationBodyPart];
GO
IF OBJECT_ID(N'[model].[FK_BodyLocationOrganStructure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[BodyLocations]'), 'IsUserTable') = 1
    ALTER TABLE [model].[BodyLocations] DROP CONSTRAINT [FK_BodyLocationOrganStructure];
GO
IF OBJECT_ID(N'[model].[FK_OrganSubStructureOrganStructure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[OrganSubStructures]'), 'IsUserTable') = 1
    ALTER TABLE [model].[OrganSubStructures] DROP CONSTRAINT [FK_OrganSubStructureOrganStructure];
GO
IF OBJECT_ID(N'[model].[FK_SubsequentVisitOrderAppointmentType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[SubsequentVisitOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[SubsequentVisitOrders] DROP CONSTRAINT [FK_SubsequentVisitOrderAppointmentType];
GO
IF OBJECT_ID(N'[model].[FK_AllergenOccurrenceUser]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[AllergenOccurrences]'), 'IsUserTable') = 1
    ALTER TABLE [model].[AllergenOccurrences] DROP CONSTRAINT [FK_AllergenOccurrenceUser];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisDetailPatientDiagnosis]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetails]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT [FK_PatientDiagnosisDetailPatientDiagnosis];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisDetailUser]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetails]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT [FK_PatientDiagnosisDetailUser];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisCommentPatient]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT [FK_PatientDiagnosisCommentPatient];
GO
IF OBJECT_ID(N'[model].[FK_EncounterClinicalDrawingClinicalDrawingTemplate]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterClinicalDrawings]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterClinicalDrawings] DROP CONSTRAINT [FK_EncounterClinicalDrawingClinicalDrawingTemplate];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentPlanUser]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
    ALTER TABLE [model].[UserTreatmentPlans] DROP CONSTRAINT [FK_TreatmentPlanUser];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisCommentClinicalDataSourceType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT [FK_PatientDiagnosisCommentClinicalDataSourceType];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisCommentUser]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT [FK_PatientDiagnosisCommentUser];
GO
IF OBJECT_ID(N'[model].[FK_PatientAllergenCommentPatient]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergenComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientAllergenComments] DROP CONSTRAINT [FK_PatientAllergenCommentPatient];
GO
IF OBJECT_ID(N'[model].[FK_PatientAllergenCommentUser]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergenComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientAllergenComments] DROP CONSTRAINT [FK_PatientAllergenCommentUser];
GO
IF OBJECT_ID(N'[model].[FK_PatientAllergenCommentClinicalDataSourceType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergenComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientAllergenComments] DROP CONSTRAINT [FK_PatientAllergenCommentClinicalDataSourceType];
GO
IF OBJECT_ID(N'[model].[FK_PatientMedicalReviewPatient]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientMedicalReviews]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientMedicalReviews] DROP CONSTRAINT [FK_PatientMedicalReviewPatient];
GO
IF OBJECT_ID(N'[model].[FK_PatientMedicalReviewClinicalHistoryReviewType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientMedicalReviews]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientMedicalReviews] DROP CONSTRAINT [FK_PatientMedicalReviewClinicalHistoryReviewType];
GO
IF OBJECT_ID(N'[model].[FK_PatientMedicalReviewUser]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientMedicalReviews]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientMedicalReviews] DROP CONSTRAINT [FK_PatientMedicalReviewUser];
GO
IF OBJECT_ID(N'[model].[FK_PatientMedicalReviewClinicalHistoryReviewArea]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientMedicalReviews]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientMedicalReviews] DROP CONSTRAINT [FK_PatientMedicalReviewClinicalHistoryReviewArea];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisDetailQualifierClinicalAttribute]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetailQualifiers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisDetailQualifiers] DROP CONSTRAINT [FK_PatientDiagnosisDetailQualifierClinicalAttribute];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisDetailQualifierPatientDiagnosisDetail]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetailQualifiers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisDetailQualifiers] DROP CONSTRAINT [FK_PatientDiagnosisDetailQualifierPatientDiagnosisDetail];
GO
IF OBJECT_ID(N'[model].[FK_ClinicalHierarchyClinicalAttributeClinicalAttribute]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalHierarchyClinicalAttribute]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ClinicalHierarchyClinicalAttribute] DROP CONSTRAINT [FK_ClinicalHierarchyClinicalAttributeClinicalAttribute];
GO
IF OBJECT_ID(N'[model].[FK_LaboratoryTestResultScreen]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTestResults]'), 'IsUserTable') = 1
    ALTER TABLE [model].[LaboratoryTestResults] DROP CONSTRAINT [FK_LaboratoryTestResultScreen];
GO
IF OBJECT_ID(N'[model].[FK_LaboratoryTestResultLaboratoryTest]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTestResults]'), 'IsUserTable') = 1
    ALTER TABLE [model].[LaboratoryTestResults] DROP CONSTRAINT [FK_LaboratoryTestResultLaboratoryTest];
GO
IF OBJECT_ID(N'[model].[FK_DiagnosticTestPerformedScreen]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[DiagnosticTestPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[DiagnosticTestPerformeds] DROP CONSTRAINT [FK_DiagnosticTestPerformedScreen];
GO
IF OBJECT_ID(N'[model].[FK_ProcedurePerformedScreen]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ProcedurePerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ProcedurePerformeds] DROP CONSTRAINT [FK_ProcedurePerformedScreen];
GO
IF OBJECT_ID(N'[model].[FK_VaccinationAdministeredScreen]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[VaccinationAdministereds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[VaccinationAdministereds] DROP CONSTRAINT [FK_VaccinationAdministeredScreen];
GO
IF OBJECT_ID(N'[model].[FK_PatientAllergenDetailAllergySource_PatientAllergenDetail]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergenDetailAllergySource]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientAllergenDetailAllergySource] DROP CONSTRAINT [FK_PatientAllergenDetailAllergySource_PatientAllergenDetail];
GO
IF OBJECT_ID(N'[model].[FK_PatientAllergenDetailAllergySource_AllergySource]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergenDetailAllergySource]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientAllergenDetailAllergySource] DROP CONSTRAINT [FK_PatientAllergenDetailAllergySource_AllergySource];
GO
IF OBJECT_ID(N'[model].[FK_SpecialExamPerformedScreen]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[SpecialExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[SpecialExamPerformeds] DROP CONSTRAINT [FK_SpecialExamPerformedScreen];
GO
IF OBJECT_ID(N'[model].[FK_ProcedureOrderClinicalProcedure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ProcedureOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ProcedureOrders] DROP CONSTRAINT [FK_ProcedureOrderClinicalProcedure];
GO
IF OBJECT_ID(N'[model].[FK_VaccineAllergenReactionType_Vaccine]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[VaccineAllergenReactionType]'), 'IsUserTable') = 1
    ALTER TABLE [model].[VaccineAllergenReactionType] DROP CONSTRAINT [FK_VaccineAllergenReactionType_Vaccine];
GO
IF OBJECT_ID(N'[model].[FK_VaccineAllergenReactionType_AllergenReactionType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[VaccineAllergenReactionType]'), 'IsUserTable') = 1
    ALTER TABLE [model].[VaccineAllergenReactionType] DROP CONSTRAINT [FK_VaccineAllergenReactionType_AllergenReactionType];
GO
IF OBJECT_ID(N'[model].[FK_VaccinationAdministeredVaccine]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[VaccinationAdministereds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[VaccinationAdministereds] DROP CONSTRAINT [FK_VaccinationAdministeredVaccine];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisDetailRelevancyQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetails]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT [FK_PatientDiagnosisDetailRelevancyQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisDetailAccuracyQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetails]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT [FK_PatientDiagnosisDetailAccuracyQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisCommentAccuracyQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT [FK_PatientDiagnosisCommentAccuracyQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisCommentRelevancyQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT [FK_PatientDiagnosisCommentRelevancyQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientAllergenCommentAccuracyQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergenComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientAllergenComments] DROP CONSTRAINT [FK_PatientAllergenCommentAccuracyQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientAllergenCommentRelevancyQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergenComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientAllergenComments] DROP CONSTRAINT [FK_PatientAllergenCommentRelevancyQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisDetailQualifierClinicalQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetailQualifiers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisDetailQualifiers] DROP CONSTRAINT [FK_PatientDiagnosisDetailQualifierClinicalQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisClinicalCondition]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnoses]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnoses] DROP CONSTRAINT [FK_PatientDiagnosisClinicalCondition];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentPlanClinicalCondition_TreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentPlanClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentPlanClinicalCondition] DROP CONSTRAINT [FK_TreatmentPlanClinicalCondition_TreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentPlanClinicalCondition_ClinicalCondition]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentPlanClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentPlanClinicalCondition] DROP CONSTRAINT [FK_TreatmentPlanClinicalCondition_ClinicalCondition];
GO
IF OBJECT_ID(N'[model].[FK_EncounterLaboratoryTestOrderClinicalCondition_EncounterLaboratoryTestOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterLaboratoryTestOrderClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterLaboratoryTestOrderClinicalCondition] DROP CONSTRAINT [FK_EncounterLaboratoryTestOrderClinicalCondition_EncounterLaboratoryTestOrder];
GO
IF OBJECT_ID(N'[model].[FK_EncounterLaboratoryTestOrderClinicalCondition_ClinicalCondition]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterLaboratoryTestOrderClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterLaboratoryTestOrderClinicalCondition] DROP CONSTRAINT [FK_EncounterLaboratoryTestOrderClinicalCondition_ClinicalCondition];
GO
IF OBJECT_ID(N'[model].[FK_EncounterPatientEducationClinicalCondition_EncounterPatientEducation]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterPatientEducationClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterPatientEducationClinicalCondition] DROP CONSTRAINT [FK_EncounterPatientEducationClinicalCondition_EncounterPatientEducation];
GO
IF OBJECT_ID(N'[model].[FK_EncounterPatientEducationClinicalCondition_ClinicalCondition]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterPatientEducationClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterPatientEducationClinicalCondition] DROP CONSTRAINT [FK_EncounterPatientEducationClinicalCondition_ClinicalCondition];
GO
IF OBJECT_ID(N'[model].[FK_PatientEducationClinicalCondition_PatientEducation]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientEducationClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientEducationClinicalCondition] DROP CONSTRAINT [FK_PatientEducationClinicalCondition_PatientEducation];
GO
IF OBJECT_ID(N'[model].[FK_PatientEducationClinicalCondition_ClinicalCondition]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientEducationClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientEducationClinicalCondition] DROP CONSTRAINT [FK_PatientEducationClinicalCondition_ClinicalCondition];
GO
IF OBJECT_ID(N'[model].[FK_ProcedurePerformedClinicalCondition_ProcedurePerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ProcedurePerformedClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ProcedurePerformedClinicalCondition] DROP CONSTRAINT [FK_ProcedurePerformedClinicalCondition_ProcedurePerformed];
GO
IF OBJECT_ID(N'[model].[FK_ProcedurePerformedClinicalCondition_ClinicalCondition]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ProcedurePerformedClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ProcedurePerformedClinicalCondition] DROP CONSTRAINT [FK_ProcedurePerformedClinicalCondition_ClinicalCondition];
GO
IF OBJECT_ID(N'[model].[FK_DiagnosticTestPerformedClinicalCondition_DiagnosticTestPerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[DiagnosticTestPerformedClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[DiagnosticTestPerformedClinicalCondition] DROP CONSTRAINT [FK_DiagnosticTestPerformedClinicalCondition_DiagnosticTestPerformed];
GO
IF OBJECT_ID(N'[model].[FK_DiagnosticTestPerformedClinicalCondition_ClinicalCondition]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[DiagnosticTestPerformedClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[DiagnosticTestPerformedClinicalCondition] DROP CONSTRAINT [FK_DiagnosticTestPerformedClinicalCondition_ClinicalCondition];
GO
IF OBJECT_ID(N'[model].[FK_RouteOfAdministrationClinicalSpecialtyType_RouteOfAdministration]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[RouteOfAdministrationClinicalSpecialtyType]'), 'IsUserTable') = 1
    ALTER TABLE [model].[RouteOfAdministrationClinicalSpecialtyType] DROP CONSTRAINT [FK_RouteOfAdministrationClinicalSpecialtyType_RouteOfAdministration];
GO
IF OBJECT_ID(N'[model].[FK_RouteOfAdministrationClinicalSpecialtyType_ClinicalSpecialtyType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[RouteOfAdministrationClinicalSpecialtyType]'), 'IsUserTable') = 1
    ALTER TABLE [model].[RouteOfAdministrationClinicalSpecialtyType] DROP CONSTRAINT [FK_RouteOfAdministrationClinicalSpecialtyType_ClinicalSpecialtyType];
GO
IF OBJECT_ID(N'[model].[FK_MedicationOrderFavoriteDoctor]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[MedicationOrderFavorites]'), 'IsUserTable') = 1
    ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT [FK_MedicationOrderFavoriteDoctor];
GO
IF OBJECT_ID(N'[model].[FK_EncounterHistoryOfPresentIllnessCommentEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterHistoryOfPresentIllnessComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterHistoryOfPresentIllnessComments] DROP CONSTRAINT [FK_EncounterHistoryOfPresentIllnessCommentEncounter];
GO
IF OBJECT_ID(N'[model].[FK_ClinicalConditionOrganSubStructure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalConditions]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ClinicalConditions] DROP CONSTRAINT [FK_ClinicalConditionOrganSubStructure];
GO
IF OBJECT_ID(N'[model].[FK_VaccinationAdministeredUnitOfMeasurement_VaccinationAdministered]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[VaccinationAdministeredUnitOfMeasurement]'), 'IsUserTable') = 1
    ALTER TABLE [model].[VaccinationAdministeredUnitOfMeasurement] DROP CONSTRAINT [FK_VaccinationAdministeredUnitOfMeasurement_VaccinationAdministered];
GO
IF OBJECT_ID(N'[model].[FK_VaccinationAdministeredUnitOfMeasurement_UnitOfMeasurement]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[VaccinationAdministeredUnitOfMeasurement]'), 'IsUserTable') = 1
    ALTER TABLE [model].[VaccinationAdministeredUnitOfMeasurement] DROP CONSTRAINT [FK_VaccinationAdministeredUnitOfMeasurement_UnitOfMeasurement];
GO
IF OBJECT_ID(N'[model].[FK_HistoryOfPresentIllnessHistoryOfPresentIllnessCategory]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[HistoryOfPresentIllnesses]'), 'IsUserTable') = 1
    ALTER TABLE [model].[HistoryOfPresentIllnesses] DROP CONSTRAINT [FK_HistoryOfPresentIllnessHistoryOfPresentIllnessCategory];
GO
IF OBJECT_ID(N'[model].[FK_BodyPartBodySystem]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[BodyParts]'), 'IsUserTable') = 1
    ALTER TABLE [model].[BodyParts] DROP CONSTRAINT [FK_BodyPartBodySystem];
GO
IF OBJECT_ID(N'[model].[FK_HistoryOfPresentIllnessClinicalCondition]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[HistoryOfPresentIllnesses]'), 'IsUserTable') = 1
    ALTER TABLE [model].[HistoryOfPresentIllnesses] DROP CONSTRAINT [FK_HistoryOfPresentIllnessClinicalCondition];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentGoalClinicalCondition_TreatmentGoal]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentGoalClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentGoalClinicalCondition] DROP CONSTRAINT [FK_TreatmentGoalClinicalCondition_TreatmentGoal];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentGoalClinicalCondition_ClinicalCondition]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentGoalClinicalCondition]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentGoalClinicalCondition] DROP CONSTRAINT [FK_TreatmentGoalClinicalCondition_ClinicalCondition];
GO
IF OBJECT_ID(N'[model].[FK_EncounterHistoryOfPresentIllnessEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterHistoryOfPresentIllnesses]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterHistoryOfPresentIllnesses] DROP CONSTRAINT [FK_EncounterHistoryOfPresentIllnessEncounter];
GO
IF OBJECT_ID(N'[model].[FK_ScreenQuestion_Screen]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ScreenQuestion]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ScreenQuestion] DROP CONSTRAINT [FK_ScreenQuestion_Screen];
GO
IF OBJECT_ID(N'[model].[FK_ScreenQuestion_Question]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ScreenQuestion]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ScreenQuestion] DROP CONSTRAINT [FK_ScreenQuestion_Question];
GO
IF OBJECT_ID(N'[model].[FK_PatientLaboratoryTestResultCopiesSentExternalProvider_PatientLaboratoryTestResult]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientLaboratoryTestResultCopiesSentExternalProvider]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientLaboratoryTestResultCopiesSentExternalProvider] DROP CONSTRAINT [FK_PatientLaboratoryTestResultCopiesSentExternalProvider_PatientLaboratoryTestResult];
GO
IF OBJECT_ID(N'[model].[FK_PatientLaboratoryTestResultCopiesSentExternalProvider_ExternalProvider]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientLaboratoryTestResultCopiesSentExternalProvider]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientLaboratoryTestResultCopiesSentExternalProvider] DROP CONSTRAINT [FK_PatientLaboratoryTestResultCopiesSentExternalProvider_ExternalProvider];
GO
IF OBJECT_ID(N'[model].[FK_ClinicalQualifierClinicalQualifierCategory]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalQualifiers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ClinicalQualifiers] DROP CONSTRAINT [FK_ClinicalQualifierClinicalQualifierCategory];
GO
IF OBJECT_ID(N'[model].[FK_EncounterChiefComplaintEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterChiefComplaints]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterChiefComplaints] DROP CONSTRAINT [FK_EncounterChiefComplaintEncounter];
GO
IF OBJECT_ID(N'[model].[FK_EncounterLaboratoryTestOrderBodyPart]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterLaboratoryTestOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT [FK_EncounterLaboratoryTestOrderBodyPart];
GO
IF OBJECT_ID(N'[model].[FK_EncounterOtherTreatmentPlanOrderOtherTreatmentPlanOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterOtherTreatmentPlanOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterOtherTreatmentPlanOrders] DROP CONSTRAINT [FK_EncounterOtherTreatmentPlanOrderOtherTreatmentPlanOrder];
GO
IF OBJECT_ID(N'[model].[FK_EncounterLaboratoryTestOrderLaboratoryTestingPriority]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterLaboratoryTestOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT [FK_EncounterLaboratoryTestOrderLaboratoryTestingPriority];
GO
IF OBJECT_ID(N'[model].[FK_EncounterRefractivePrescriptionRefractivePrescription]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterRefractivePrescriptions]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterRefractivePrescriptions] DROP CONSTRAINT [FK_EncounterRefractivePrescriptionRefractivePrescription];
GO
IF OBJECT_ID(N'[model].[FK_EncounterProcedureOrderDoctor]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterProcedureOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT [FK_EncounterProcedureOrderDoctor];
GO
IF OBJECT_ID(N'[model].[FK_EncounterProcedureOrderRelativeTimeType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterProcedureOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT [FK_EncounterProcedureOrderRelativeTimeType];
GO
IF OBJECT_ID(N'[model].[FK_UserTreatmentPlanTreatmentPlanComment]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
    ALTER TABLE [model].[UserTreatmentPlans] DROP CONSTRAINT [FK_UserTreatmentPlanTreatmentPlanComment];
GO
IF OBJECT_ID(N'[model].[FK_CommunicationTransactionEncounterCommunicationWithOtherProviderOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[CommunicationTransactions]'), 'IsUserTable') = 1
    ALTER TABLE [model].[CommunicationTransactions] DROP CONSTRAINT [FK_CommunicationTransactionEncounterCommunicationWithOtherProviderOrder];
GO
IF OBJECT_ID(N'[model].[FK_CommunicationTransactionExternalProvider]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[CommunicationTransactions]'), 'IsUserTable') = 1
    ALTER TABLE [model].[CommunicationTransactions] DROP CONSTRAINT [FK_CommunicationTransactionExternalProvider];
GO
IF OBJECT_ID(N'[model].[FK_CommunicationTransactionPatient]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[CommunicationTransactions]'), 'IsUserTable') = 1
    ALTER TABLE [model].[CommunicationTransactions] DROP CONSTRAINT [FK_CommunicationTransactionPatient];
GO
IF OBJECT_ID(N'[model].[FK_EncounterMedicationOrderPatientMedicationDetail]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterMedicationOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterMedicationOrders] DROP CONSTRAINT [FK_EncounterMedicationOrderPatientMedicationDetail];
GO
IF OBJECT_ID(N'[model].[FK_ObservationTypeObservationTypeCategory]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ObservationTypes]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ObservationTypes] DROP CONSTRAINT [FK_ObservationTypeObservationTypeCategory];
GO
IF OBJECT_ID(N'[model].[FK_PatientSpecialExamPerformedClinicalProcedure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientSpecialExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientSpecialExamPerformeds] DROP CONSTRAINT [FK_PatientSpecialExamPerformedClinicalProcedure];
GO
IF OBJECT_ID(N'[model].[FK_PatientOtherElementPerformedClinicalProcedure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientOtherElementPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientOtherElementPerformeds] DROP CONSTRAINT [FK_PatientOtherElementPerformedClinicalProcedure];
GO
IF OBJECT_ID(N'[model].[FK_ChiefComplaintChiefComplaintCategory]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ChiefComplaints]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ChiefComplaints] DROP CONSTRAINT [FK_ChiefComplaintChiefComplaintCategory];
GO
IF OBJECT_ID(N'[model].[FK_EncounterChiefComplaintChiefComplaint]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterChiefComplaints]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterChiefComplaints] DROP CONSTRAINT [FK_EncounterChiefComplaintChiefComplaint];
GO
IF OBJECT_ID(N'[model].[FK_EncounterChiefComplaintCommentEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterChiefComplaintComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterChiefComplaintComments] DROP CONSTRAINT [FK_EncounterChiefComplaintCommentEncounter];
GO
IF OBJECT_ID(N'[model].[FK_EncounterReasonForVisitCommentEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReasonForVisitComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterReasonForVisitComments] DROP CONSTRAINT [FK_EncounterReasonForVisitCommentEncounter];
GO
IF OBJECT_ID(N'[model].[FK_EncounterReviewOfSystemCommentEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReviewOfSystemComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterReviewOfSystemComments] DROP CONSTRAINT [FK_EncounterReviewOfSystemCommentEncounter];
GO
IF OBJECT_ID(N'[model].[FK_ReviewOfSystemBodySystem]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[ReviewOfSystems]'), 'IsUserTable') = 1
    ALTER TABLE [model].[ReviewOfSystems] DROP CONSTRAINT [FK_ReviewOfSystemBodySystem];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisDetailBodyLocation]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetails]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT [FK_PatientDiagnosisDetailBodyLocation];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisDetailEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetails]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT [FK_PatientDiagnosisDetailEncounter];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisCommentEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT [FK_PatientDiagnosisCommentEncounter];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisCommentOrganStructure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT [FK_PatientDiagnosisCommentOrganStructure];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisCommentBodyPart]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT [FK_PatientDiagnosisCommentBodyPart];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisDetailAxisQualifierPatientDiagnosisDetail]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetailAxisQualifiers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisDetailAxisQualifiers] DROP CONSTRAINT [FK_PatientDiagnosisDetailAxisQualifierPatientDiagnosisDetail];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisDetailAxisQualifierAxisQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetailAxisQualifiers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisDetailAxisQualifiers] DROP CONSTRAINT [FK_PatientDiagnosisDetailAxisQualifierAxisQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisClinicalAttribute]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnoses]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnoses] DROP CONSTRAINT [FK_PatientDiagnosisClinicalAttribute];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisAssociatedWithPatientDiagnosis]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnoses]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnoses] DROP CONSTRAINT [FK_PatientDiagnosisAssociatedWithPatientDiagnosis];
GO
IF OBJECT_ID(N'[model].[FK_PatientFamilyDiagnosisPatient]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientFamilyHistoryEntries]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientFamilyHistoryEntries] DROP CONSTRAINT [FK_PatientFamilyDiagnosisPatient];
GO
IF OBJECT_ID(N'[model].[FK_PatientFamilyDiagnosisClinicalCondition]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientFamilyHistoryEntries]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientFamilyHistoryEntries] DROP CONSTRAINT [FK_PatientFamilyDiagnosisClinicalCondition];
GO
IF OBJECT_ID(N'[model].[FK_PatientFamilyDiagnosisClinicalDataSourceType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientFamilyHistoryEntries]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientFamilyHistoryEntries] DROP CONSTRAINT [FK_PatientFamilyDiagnosisClinicalDataSourceType];
GO
IF OBJECT_ID(N'[model].[FK_PatientFamilyHistoryEntryDetailPatientFamilyHistoryEntry]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientFamilyHistoryEntryDetails]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientFamilyHistoryEntryDetails] DROP CONSTRAINT [FK_PatientFamilyHistoryEntryDetailPatientFamilyHistoryEntry];
GO
IF OBJECT_ID(N'[model].[FK_PatientFamilyHistoryEntryDetailUser]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientFamilyHistoryEntryDetails]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientFamilyHistoryEntryDetails] DROP CONSTRAINT [FK_PatientFamilyHistoryEntryDetailUser];
GO
IF OBJECT_ID(N'[model].[FK_PatientFamilyHistoryEntryDetailAccuracyQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientFamilyHistoryEntryDetails]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientFamilyHistoryEntryDetails] DROP CONSTRAINT [FK_PatientFamilyHistoryEntryDetailAccuracyQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientFamilyHistoryEntryDetailRelevancyQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientFamilyHistoryEntryDetails]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientFamilyHistoryEntryDetails] DROP CONSTRAINT [FK_PatientFamilyHistoryEntryDetailRelevancyQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientFamilyHistoryEntryDetailClinicalQualifier_PatientFamilyHistoryEntryDetail]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientFamilyHistoryEntryDetailClinicalQualifier]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientFamilyHistoryEntryDetailClinicalQualifier] DROP CONSTRAINT [FK_PatientFamilyHistoryEntryDetailClinicalQualifier_PatientFamilyHistoryEntryDetail];
GO
IF OBJECT_ID(N'[model].[FK_PatientFamilyHistoryEntryDetailClinicalQualifier_ClinicalQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientFamilyHistoryEntryDetailClinicalQualifier]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientFamilyHistoryEntryDetailClinicalQualifier] DROP CONSTRAINT [FK_PatientFamilyHistoryEntryDetailClinicalQualifier_ClinicalQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientAllergenDetailClinicalQualifier_PatientAllergenDetail]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergenDetailClinicalQualifier]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientAllergenDetailClinicalQualifier] DROP CONSTRAINT [FK_PatientAllergenDetailClinicalQualifier_PatientAllergenDetail];
GO
IF OBJECT_ID(N'[model].[FK_PatientAllergenDetailClinicalQualifier_ClinicalQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergenDetailClinicalQualifier]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientAllergenDetailClinicalQualifier] DROP CONSTRAINT [FK_PatientAllergenDetailClinicalQualifier_ClinicalQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientBloodPressureClinicalQualifier_PatientBloodPressure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientBloodPressureClinicalQualifier]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientBloodPressureClinicalQualifier] DROP CONSTRAINT [FK_PatientBloodPressureClinicalQualifier_PatientBloodPressure];
GO
IF OBJECT_ID(N'[model].[FK_PatientBloodPressureClinicalQualifier_ClinicalQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientBloodPressureClinicalQualifier]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientBloodPressureClinicalQualifier] DROP CONSTRAINT [FK_PatientBloodPressureClinicalQualifier_ClinicalQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientHeightAndWeightClinicalQualifier_PatientHeightAndWeight]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientHeightAndWeightClinicalQualifier]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientHeightAndWeightClinicalQualifier] DROP CONSTRAINT [FK_PatientHeightAndWeightClinicalQualifier_PatientHeightAndWeight];
GO
IF OBJECT_ID(N'[model].[FK_PatientHeightAndWeightClinicalQualifier_ClinicalQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientHeightAndWeightClinicalQualifier]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientHeightAndWeightClinicalQualifier] DROP CONSTRAINT [FK_PatientHeightAndWeightClinicalQualifier_ClinicalQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientCognitiveStatusAssessmentClinicalQualifier_PatientCognitiveStatusAssessment]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientCognitiveStatusAssessmentClinicalQualifier]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientCognitiveStatusAssessmentClinicalQualifier] DROP CONSTRAINT [FK_PatientCognitiveStatusAssessmentClinicalQualifier_PatientCognitiveStatusAssessment];
GO
IF OBJECT_ID(N'[model].[FK_PatientCognitiveStatusAssessmentClinicalQualifier_ClinicalQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientCognitiveStatusAssessmentClinicalQualifier]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientCognitiveStatusAssessmentClinicalQualifier] DROP CONSTRAINT [FK_PatientCognitiveStatusAssessmentClinicalQualifier_ClinicalQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientFunctionalStatusAssessmentClinicalQualifier_PatientFunctionalStatusAssessment]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientFunctionalStatusAssessmentClinicalQualifier]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientFunctionalStatusAssessmentClinicalQualifier] DROP CONSTRAINT [FK_PatientFunctionalStatusAssessmentClinicalQualifier_PatientFunctionalStatusAssessment];
GO
IF OBJECT_ID(N'[model].[FK_PatientFunctionalStatusAssessmentClinicalQualifier_ClinicalQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientFunctionalStatusAssessmentClinicalQualifier]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientFunctionalStatusAssessmentClinicalQualifier] DROP CONSTRAINT [FK_PatientFunctionalStatusAssessmentClinicalQualifier_ClinicalQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisDetailEncounterTreatmentGoal]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetails]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT [FK_PatientDiagnosisDetailEncounterTreatmentGoal];
GO
IF OBJECT_ID(N'[model].[FK_PatientOtherElementPerformedClinicalDataSourceType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientOtherElementPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientOtherElementPerformeds] DROP CONSTRAINT [FK_PatientOtherElementPerformedClinicalDataSourceType];
GO
IF OBJECT_ID(N'[model].[FK_PatientSpecialExamPerformedClinicalDataSourceType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientSpecialExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientSpecialExamPerformeds] DROP CONSTRAINT [FK_PatientSpecialExamPerformedClinicalDataSourceType];
GO
IF OBJECT_ID(N'[model].[FK_EncounterReasonForVisitCommentBodyPart]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReasonForVisitComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterReasonForVisitComments] DROP CONSTRAINT [FK_EncounterReasonForVisitCommentBodyPart];
GO
IF OBJECT_ID(N'[model].[FK_EncounterReasonForVisitCommentUser]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReasonForVisitComments]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterReasonForVisitComments] DROP CONSTRAINT [FK_EncounterReasonForVisitCommentUser];
GO
IF OBJECT_ID(N'[model].[FK_EncounterCommunicationWithOtherProviderOrderCommunicationReviewStatus]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterCommunicationWithOtherProviderOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders] DROP CONSTRAINT [FK_EncounterCommunicationWithOtherProviderOrderCommunicationReviewStatus];
GO
IF OBJECT_ID(N'[model].[FK_EncounterProcedureOrderBodyPart1]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterProcedureOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT [FK_EncounterProcedureOrderBodyPart1];
GO
IF OBJECT_ID(N'[model].[FK_UserTreatmentPlanPatientEducation_UserTreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlanPatientEducation]'), 'IsUserTable') = 1
    ALTER TABLE [model].[UserTreatmentPlanPatientEducation] DROP CONSTRAINT [FK_UserTreatmentPlanPatientEducation_UserTreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_UserTreatmentPlanPatientEducation_PatientEducation]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlanPatientEducation]'), 'IsUserTable') = 1
    ALTER TABLE [model].[UserTreatmentPlanPatientEducation] DROP CONSTRAINT [FK_UserTreatmentPlanPatientEducation_PatientEducation];
GO
IF OBJECT_ID(N'[model].[FK_UserTreatmentPlanVaccinationOrder_UserTreatmentPlan]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlanVaccinationOrder]'), 'IsUserTable') = 1
    ALTER TABLE [model].[UserTreatmentPlanVaccinationOrder] DROP CONSTRAINT [FK_UserTreatmentPlanVaccinationOrder_UserTreatmentPlan];
GO
IF OBJECT_ID(N'[model].[FK_UserTreatmentPlanVaccinationOrder_VaccinationOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlanVaccinationOrder]'), 'IsUserTable') = 1
    ALTER TABLE [model].[UserTreatmentPlanVaccinationOrder] DROP CONSTRAINT [FK_UserTreatmentPlanVaccinationOrder_VaccinationOrder];
GO
IF OBJECT_ID(N'[model].[FK_UnitOfMeasurementUnitOfMeasurementType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[UnitsOfMeasurement]'), 'IsUserTable') = 1
    ALTER TABLE [model].[UnitsOfMeasurement] DROP CONSTRAINT [FK_UnitOfMeasurementUnitOfMeasurementType];
GO
IF OBJECT_ID(N'[model].[FK_AllergenOccurrenceTimeQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[AllergenOccurrences]'), 'IsUserTable') = 1
    ALTER TABLE [model].[AllergenOccurrences] DROP CONSTRAINT [FK_AllergenOccurrenceTimeQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientProblemConcernLevelPatientDiagnosis]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientProblemConcernLevels]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientProblemConcernLevels] DROP CONSTRAINT [FK_PatientProblemConcernLevelPatientDiagnosis];
GO
IF OBJECT_ID(N'[model].[FK_PatientProblemConcernLevelPatientAllergen]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientProblemConcernLevels]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientProblemConcernLevels] DROP CONSTRAINT [FK_PatientProblemConcernLevelPatientAllergen];
GO
IF OBJECT_ID(N'[model].[FK_EncounterLaboratoryTestOrderPatientLaboratoryTestResult]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterLaboratoryTestOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT [FK_EncounterLaboratoryTestOrderPatientLaboratoryTestResult];
GO
IF OBJECT_ID(N'[model].[FK_EncounterProcedureOrderPatientProcedurePerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterProcedureOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT [FK_EncounterProcedureOrderPatientProcedurePerformed];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentGoalAndInstructionTreatmentGoal]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentGoalAndInstructions]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentGoalAndInstructions] DROP CONSTRAINT [FK_TreatmentGoalAndInstructionTreatmentGoal];
GO
IF OBJECT_ID(N'[model].[FK_EncounterReviewOfSystemEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReviewOfSystems]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterReviewOfSystems] DROP CONSTRAINT [FK_EncounterReviewOfSystemEncounter];
GO
IF OBJECT_ID(N'[model].[FK_EncounterReviewOfSystemReviewOfSystem]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReviewOfSystems]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterReviewOfSystems] DROP CONSTRAINT [FK_EncounterReviewOfSystemReviewOfSystem];
GO
IF OBJECT_ID(N'[model].[FK_EncounterCommunicationWithOtherProviderOrderReceiverExternalProvider]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterCommunicationWithOtherProviderOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders] DROP CONSTRAINT [FK_EncounterCommunicationWithOtherProviderOrderReceiverExternalProvider];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentPlanOrderInstructionDiagnosticTestOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentPlanOrderInstructions]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentPlanOrderInstructions] DROP CONSTRAINT [FK_TreatmentPlanOrderInstructionDiagnosticTestOrder];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentPlanOrderInstructionLaboratoryTestOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentPlanOrderInstructions]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentPlanOrderInstructions] DROP CONSTRAINT [FK_TreatmentPlanOrderInstructionLaboratoryTestOrder];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentPlanOrderInstructionProcedureOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentPlanOrderInstructions]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentPlanOrderInstructions] DROP CONSTRAINT [FK_TreatmentPlanOrderInstructionProcedureOrder];
GO
IF OBJECT_ID(N'[model].[FK_TreatmentPlanOrderInstructionSubsequentVisitOrder]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentPlanOrderInstructions]'), 'IsUserTable') = 1
    ALTER TABLE [model].[TreatmentPlanOrderInstructions] DROP CONSTRAINT [FK_TreatmentPlanOrderInstructionSubsequentVisitOrder];
GO
IF OBJECT_ID(N'[model].[FK_PatientAllergenAllergenReactionTypeAllergenOccurrence]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[AllergenOccurrences]'), 'IsUserTable') = 1
    ALTER TABLE [model].[AllergenOccurrences] DROP CONSTRAINT [FK_PatientAllergenAllergenReactionTypeAllergenOccurrence];
GO
IF OBJECT_ID(N'[model].[FK_EncounterLaboratoryTestOrderAppointment]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterLaboratoryTestOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT [FK_EncounterLaboratoryTestOrderAppointment];
GO
IF OBJECT_ID(N'[model].[FK_EncounterProcedureOrderAppointment]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[EncounterProcedureOrders]'), 'IsUserTable') = 1
    ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT [FK_EncounterProcedureOrderAppointment];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosisClinicalDataSourceType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnoses]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientDiagnoses] DROP CONSTRAINT [FK_PatientDiagnosisClinicalDataSourceType];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Check if 'HistoryOfPresentIllnesses' exists...
IF OBJECT_ID(N'model.HistoryOfPresentIllnesses', 'U') IS NULL
BEGIN
-- 'HistoryOfPresentIllnesses' does not exist, creating...
	CREATE TABLE [model].[HistoryOfPresentIllnesses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [HistoryOfPresentIllnessCategoryId] int  NOT NULL,
    [SimpleDescription] nvarchar(max)  NULL,
    [TechnicalDescription] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL,
    [ClinicalConditionId] int  NOT NULL
);
END
-- 'HistoryOfPresentIllnesses' exists, validating columns.
ELSE IF OBJECT_ID(N'model.HistoryOfPresentIllnesses', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HistoryOfPresentIllnesses'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.HistoryOfPresentIllnesses'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.HistoryOfPresentIllnesses.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HistoryOfPresentIllnesses'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.HistoryOfPresentIllnesses.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HistoryOfPresentIllnesses'
		AND COLUMN_NAME = 'HistoryOfPresentIllnessCategoryId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.HistoryOfPresentIllnesses.HistoryOfPresentIllnessCategoryId. Column [HistoryOfPresentIllnessCategoryId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HistoryOfPresentIllnesses'
		AND COLUMN_NAME = 'SimpleDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.HistoryOfPresentIllnesses.SimpleDescription. Column [SimpleDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HistoryOfPresentIllnesses'
		AND COLUMN_NAME = 'TechnicalDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.HistoryOfPresentIllnesses.TechnicalDescription. Column [TechnicalDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HistoryOfPresentIllnesses'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.HistoryOfPresentIllnesses.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HistoryOfPresentIllnesses'
		AND COLUMN_NAME = 'ClinicalConditionId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.HistoryOfPresentIllnesses.ClinicalConditionId. Column [ClinicalConditionId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ReviewOfSystems' exists...
IF OBJECT_ID(N'model.ReviewOfSystems', 'U') IS NULL
BEGIN
-- 'ReviewOfSystems' does not exist, creating...
	CREATE TABLE [model].[ReviewOfSystems] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [SimpleDescription] nvarchar(max)  NULL,
    [TechnicalDescription] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL,
    [BodySystemId] int  NULL
);
END
-- 'ReviewOfSystems' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ReviewOfSystems', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ReviewOfSystems'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ReviewOfSystems'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ReviewOfSystems.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ReviewOfSystems'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ReviewOfSystems.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ReviewOfSystems'
		AND COLUMN_NAME = 'SimpleDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ReviewOfSystems.SimpleDescription. Column [SimpleDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ReviewOfSystems'
		AND COLUMN_NAME = 'TechnicalDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ReviewOfSystems.TechnicalDescription. Column [TechnicalDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ReviewOfSystems'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ReviewOfSystems.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ReviewOfSystems'
		AND COLUMN_NAME = 'BodySystemId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ReviewOfSystems.BodySystemId. Column [BodySystemId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientDiagnoses' exists...
IF OBJECT_ID(N'model.PatientDiagnoses', 'U') IS NULL
BEGIN
-- 'PatientDiagnoses' does not exist, creating...
	CREATE TABLE [model].[PatientDiagnoses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PatientId] int  NOT NULL,
    [ClinicalDataSourceTypeId] int  NOT NULL,
    [ClinicalConditionId] int  NULL,
    [LateralityId] int  NULL,
    [ClinicalAttributeId] int  NULL,
    [ToPatientDiagnosisId] int  NULL
);
END
-- 'PatientDiagnoses' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientDiagnoses', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnoses'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientDiagnoses'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnoses.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnoses'
		AND COLUMN_NAME = 'PatientId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnoses.PatientId. Column [PatientId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnoses'
		AND COLUMN_NAME = 'ClinicalDataSourceTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnoses.ClinicalDataSourceTypeId. Column [ClinicalDataSourceTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnoses'
		AND COLUMN_NAME = 'ClinicalConditionId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnoses.ClinicalConditionId. Column [ClinicalConditionId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnoses'
		AND COLUMN_NAME = 'LateralityId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnoses.LateralityId. Column [LateralityId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnoses'
		AND COLUMN_NAME = 'ClinicalAttributeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnoses.ClinicalAttributeId. Column [ClinicalAttributeId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnoses'
		AND COLUMN_NAME = 'ToPatientDiagnosisId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnoses.ToPatientDiagnosisId. Column [ToPatientDiagnosisId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'MedicationComments' exists...
IF OBJECT_ID(N'model.MedicationComments', 'U') IS NULL
BEGIN
-- 'MedicationComments' does not exist, creating...
	CREATE TABLE [model].[MedicationComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [MedicationId] int  NULL
);
END
-- 'MedicationComments' exists, validating columns.
ELSE IF OBJECT_ID(N'model.MedicationComments', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationComments'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.MedicationComments'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationComments.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationComments'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationComments.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationComments'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationComments.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationComments'
		AND COLUMN_NAME = 'MedicationId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.MedicationComments.MedicationId. Column [MedicationId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'ProcedureOrders' exists...
IF OBJECT_ID(N'model.ProcedureOrders', 'U') IS NULL
BEGIN
-- 'ProcedureOrders' does not exist, creating...
	CREATE TABLE [model].[ProcedureOrders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrdinalId] int  NOT NULL,
    [IsExcludedFromCommunications] bit  NOT NULL,
    [IsFollowUpRequired] bit  NOT NULL,
    [IsSurgeryManagementRequired] bit  NOT NULL,
    [SimpleDescription] nvarchar(max)  NULL,
    [TechnicalDescription] nvarchar(max)  NULL,
    [AppointmentTypeId] int  NULL,
    [ClinicalProcedureId] int  NOT NULL
);
END
-- 'ProcedureOrders' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ProcedureOrders', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrders'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ProcedureOrders'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedureOrders.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrders'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedureOrders.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrders'
		AND COLUMN_NAME = 'IsExcludedFromCommunications'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedureOrders.IsExcludedFromCommunications. Column [IsExcludedFromCommunications] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrders'
		AND COLUMN_NAME = 'IsFollowUpRequired'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedureOrders.IsFollowUpRequired. Column [IsFollowUpRequired] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrders'
		AND COLUMN_NAME = 'IsSurgeryManagementRequired'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedureOrders.IsSurgeryManagementRequired. Column [IsSurgeryManagementRequired] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrders'
		AND COLUMN_NAME = 'SimpleDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ProcedureOrders.SimpleDescription. Column [SimpleDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrders'
		AND COLUMN_NAME = 'TechnicalDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ProcedureOrders.TechnicalDescription. Column [TechnicalDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrders'
		AND COLUMN_NAME = 'AppointmentTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ProcedureOrders.AppointmentTypeId. Column [AppointmentTypeId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrders'
		AND COLUMN_NAME = 'ClinicalProcedureId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedureOrders.ClinicalProcedureId. Column [ClinicalProcedureId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'SubsequentVisitOrders' exists...
IF OBJECT_ID(N'model.SubsequentVisitOrders', 'U') IS NULL
BEGIN
-- 'SubsequentVisitOrders' does not exist, creating...
	CREATE TABLE [model].[SubsequentVisitOrders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrdinalId] int  NOT NULL,
    [IsExcludedFromCommunications] bit  NOT NULL,
    [IsFollowUpRequired] bit  NOT NULL,
    [SimpleDescription] nvarchar(max)  NULL,
    [TechnicalDescription] nvarchar(max)  NULL,
    [AppointmentType_Id] int  NOT NULL
);
END
-- 'SubsequentVisitOrders' exists, validating columns.
ELSE IF OBJECT_ID(N'model.SubsequentVisitOrders', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SubsequentVisitOrders'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.SubsequentVisitOrders'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SubsequentVisitOrders.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SubsequentVisitOrders'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SubsequentVisitOrders.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SubsequentVisitOrders'
		AND COLUMN_NAME = 'IsExcludedFromCommunications'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SubsequentVisitOrders.IsExcludedFromCommunications. Column [IsExcludedFromCommunications] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SubsequentVisitOrders'
		AND COLUMN_NAME = 'IsFollowUpRequired'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SubsequentVisitOrders.IsFollowUpRequired. Column [IsFollowUpRequired] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SubsequentVisitOrders'
		AND COLUMN_NAME = 'SimpleDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.SubsequentVisitOrders.SimpleDescription. Column [SimpleDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SubsequentVisitOrders'
		AND COLUMN_NAME = 'TechnicalDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.SubsequentVisitOrders.TechnicalDescription. Column [TechnicalDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SubsequentVisitOrders'
		AND COLUMN_NAME = 'AppointmentType_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SubsequentVisitOrders.AppointmentType_Id. Column [AppointmentType_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'CommunicationWithOtherProviderOrders' exists...
IF OBJECT_ID(N'model.CommunicationWithOtherProviderOrders', 'U') IS NULL
BEGIN
-- 'CommunicationWithOtherProviderOrders' does not exist, creating...
	CREATE TABLE [model].[CommunicationWithOtherProviderOrders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IsExcludedFromCommunications] bit  NOT NULL,
    [IsFollowUpRequired] bit  NOT NULL,
    [OrdinalId] int  NOT NULL,
    [SimpleDescription] nvarchar(max)  NULL,
    [TechnicalDescription] nvarchar(max)  NULL,
    [TemplateDocumentId] int  NULL,
    [ExternalProviderCommunicationTypeId] int  NOT NULL
);
END
-- 'CommunicationWithOtherProviderOrders' exists, validating columns.
ELSE IF OBJECT_ID(N'model.CommunicationWithOtherProviderOrders', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.CommunicationWithOtherProviderOrders'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationWithOtherProviderOrders.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'IsExcludedFromCommunications'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationWithOtherProviderOrders.IsExcludedFromCommunications. Column [IsExcludedFromCommunications] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'IsFollowUpRequired'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationWithOtherProviderOrders.IsFollowUpRequired. Column [IsFollowUpRequired] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationWithOtherProviderOrders.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'SimpleDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.CommunicationWithOtherProviderOrders.SimpleDescription. Column [SimpleDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'TechnicalDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.CommunicationWithOtherProviderOrders.TechnicalDescription. Column [TechnicalDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'TemplateDocumentId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.CommunicationWithOtherProviderOrders.TemplateDocumentId. Column [TemplateDocumentId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'ExternalProviderCommunicationTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationWithOtherProviderOrders.ExternalProviderCommunicationTypeId. Column [ExternalProviderCommunicationTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'TransitionOfCareOrders' exists...
IF OBJECT_ID(N'model.TransitionOfCareOrders', 'U') IS NULL
BEGIN
-- 'TransitionOfCareOrders' does not exist, creating...
	CREATE TABLE [model].[TransitionOfCareOrders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrdinalId] int  NOT NULL,
    [IsExcludedFromCommunications] bit  NOT NULL,
    [IsFollowUpRequired] bit  NOT NULL,
    [SimpleDescription] nvarchar(max)  NULL,
    [TechnicalDescription] nvarchar(max)  NULL
);
END
-- 'TransitionOfCareOrders' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TransitionOfCareOrders', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TransitionOfCareOrders'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.TransitionOfCareOrders'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TransitionOfCareOrders.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TransitionOfCareOrders'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TransitionOfCareOrders.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TransitionOfCareOrders'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TransitionOfCareOrders.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TransitionOfCareOrders'
		AND COLUMN_NAME = 'IsExcludedFromCommunications'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TransitionOfCareOrders.IsExcludedFromCommunications. Column [IsExcludedFromCommunications] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TransitionOfCareOrders'
		AND COLUMN_NAME = 'IsFollowUpRequired'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TransitionOfCareOrders.IsFollowUpRequired. Column [IsFollowUpRequired] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TransitionOfCareOrders'
		AND COLUMN_NAME = 'SimpleDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.TransitionOfCareOrders.SimpleDescription. Column [SimpleDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TransitionOfCareOrders'
		AND COLUMN_NAME = 'TechnicalDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.TransitionOfCareOrders.TechnicalDescription. Column [TechnicalDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'Qualities' exists...
IF OBJECT_ID(N'model.Qualities', 'U') IS NULL
BEGIN
-- 'Qualities' does not exist, creating...
	CREATE TABLE [model].[Qualities] (
    [Id] int IDENTITY(1,1) NOT NULL
);
END
-- 'Qualities' exists, validating columns.
ELSE IF OBJECT_ID(N'model.Qualities', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='Qualities'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.Qualities'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.Qualities.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'Timings' exists...
IF OBJECT_ID(N'model.Timings', 'U') IS NULL
BEGIN
-- 'Timings' does not exist, creating...
	CREATE TABLE [model].[Timings] (
    [Id] int IDENTITY(1,1) NOT NULL
);
END
-- 'Timings' exists, validating columns.
ELSE IF OBJECT_ID(N'model.Timings', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='Timings'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.Timings'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.Timings.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'Contexts' exists...
IF OBJECT_ID(N'model.Contexts', 'U') IS NULL
BEGIN
-- 'Contexts' does not exist, creating...
	CREATE TABLE [model].[Contexts] (
    [Id] int IDENTITY(1,1) NOT NULL
);
END
-- 'Contexts' exists, validating columns.
ELSE IF OBJECT_ID(N'model.Contexts', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='Contexts'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.Contexts'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.Contexts.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ModifyingFactors' exists...
IF OBJECT_ID(N'model.ModifyingFactors', 'U') IS NULL
BEGIN
-- 'ModifyingFactors' does not exist, creating...
	CREATE TABLE [model].[ModifyingFactors] (
    [Id] int IDENTITY(1,1) NOT NULL
);
END
-- 'ModifyingFactors' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ModifyingFactors', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ModifyingFactors'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ModifyingFactors'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ModifyingFactors.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'AssociatedSignAndSymptom' exists...
IF OBJECT_ID(N'model.AssociatedSignAndSymptom', 'U') IS NULL
BEGIN
-- 'AssociatedSignAndSymptom' does not exist, creating...
	CREATE TABLE [model].[AssociatedSignAndSymptom] (
    [Id] int IDENTITY(1,1) NOT NULL
);
END
-- 'AssociatedSignAndSymptom' exists, validating columns.
ELSE IF OBJECT_ID(N'model.AssociatedSignAndSymptom', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AssociatedSignAndSymptom'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.AssociatedSignAndSymptom'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AssociatedSignAndSymptom.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterHistoryOfPresentIllnesses' exists...
IF OBJECT_ID(N'model.EncounterHistoryOfPresentIllnesses', 'U') IS NULL
BEGIN
-- 'EncounterHistoryOfPresentIllnesses' does not exist, creating...
	CREATE TABLE [model].[EncounterHistoryOfPresentIllnesses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [IsOnProblemList] bit  NOT NULL,
    [StartDateTime] datetime  NULL,
    [EndDateTime] datetime  NULL
);
END
-- 'EncounterHistoryOfPresentIllnesses' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterHistoryOfPresentIllnesses', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterHistoryOfPresentIllnesses'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterHistoryOfPresentIllnesses'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterHistoryOfPresentIllnesses.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterHistoryOfPresentIllnesses'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterHistoryOfPresentIllnesses.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterHistoryOfPresentIllnesses'
		AND COLUMN_NAME = 'IsOnProblemList'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterHistoryOfPresentIllnesses.IsOnProblemList. Column [IsOnProblemList] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterHistoryOfPresentIllnesses'
		AND COLUMN_NAME = 'StartDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterHistoryOfPresentIllnesses.StartDateTime. Column [StartDateTime] should be datetime, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterHistoryOfPresentIllnesses'
		AND COLUMN_NAME = 'EndDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterHistoryOfPresentIllnesses.EndDateTime. Column [EndDateTime] should be datetime, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'HistoryOfPresentIllnessCategories' exists...
IF OBJECT_ID(N'model.HistoryOfPresentIllnessCategories', 'U') IS NULL
BEGIN
-- 'HistoryOfPresentIllnessCategories' does not exist, creating...
	CREATE TABLE [model].[HistoryOfPresentIllnessCategories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'HistoryOfPresentIllnessCategories' exists, validating columns.
ELSE IF OBJECT_ID(N'model.HistoryOfPresentIllnessCategories', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HistoryOfPresentIllnessCategories'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.HistoryOfPresentIllnessCategories'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.HistoryOfPresentIllnessCategories.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HistoryOfPresentIllnessCategories'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.HistoryOfPresentIllnessCategories.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'UserTreatmentPlans' exists...
IF OBJECT_ID(N'model.UserTreatmentPlans', 'U') IS NULL
BEGIN
-- 'UserTreatmentPlans' does not exist, creating...
	CREATE TABLE [model].[UserTreatmentPlans] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [UserId] int  NOT NULL,
    [TreatmentPlanCommentId] int  NULL
);
END
-- 'UserTreatmentPlans' exists, validating columns.
ELSE IF OBJECT_ID(N'model.UserTreatmentPlans', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UserTreatmentPlans'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.UserTreatmentPlans'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UserTreatmentPlans.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UserTreatmentPlans'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UserTreatmentPlans.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UserTreatmentPlans'
		AND COLUMN_NAME = 'UserId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UserTreatmentPlans.UserId. Column [UserId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UserTreatmentPlans'
		AND COLUMN_NAME = 'TreatmentPlanCommentId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.UserTreatmentPlans.TreatmentPlanCommentId. Column [TreatmentPlanCommentId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'TreatmentGoalAndInstructions' exists...
IF OBJECT_ID(N'model.TreatmentGoalAndInstructions', 'U') IS NULL
BEGIN
-- 'TreatmentGoalAndInstructions' does not exist, creating...
	CREATE TABLE [model].[TreatmentGoalAndInstructions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ShortName] nvarchar(max)  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [OrdinalId] int  NOT NULL,
    [TreatmentGoalId] int  NOT NULL
);
END
-- 'TreatmentGoalAndInstructions' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TreatmentGoalAndInstructions', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentGoalAndInstructions'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.TreatmentGoalAndInstructions'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentGoalAndInstructions.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentGoalAndInstructions'
		AND COLUMN_NAME = 'ShortName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentGoalAndInstructions.ShortName. Column [ShortName] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentGoalAndInstructions'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentGoalAndInstructions.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentGoalAndInstructions'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentGoalAndInstructions.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentGoalAndInstructions'
		AND COLUMN_NAME = 'TreatmentGoalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentGoalAndInstructions.TreatmentGoalId. Column [TreatmentGoalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'OtherTreatmentPlanOrders' exists...
IF OBJECT_ID(N'model.OtherTreatmentPlanOrders', 'U') IS NULL
BEGIN
-- 'OtherTreatmentPlanOrders' does not exist, creating...
	CREATE TABLE [model].[OtherTreatmentPlanOrders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IsExcludedFromCommunications] bit  NOT NULL,
    [IsFollowUpRequired] bit  NOT NULL,
    [SimpleDescription] nvarchar(max)  NULL,
    [TechnicalDescription] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'OtherTreatmentPlanOrders' exists, validating columns.
ELSE IF OBJECT_ID(N'model.OtherTreatmentPlanOrders', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OtherTreatmentPlanOrders'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.OtherTreatmentPlanOrders'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OtherTreatmentPlanOrders.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OtherTreatmentPlanOrders'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OtherTreatmentPlanOrders.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OtherTreatmentPlanOrders'
		AND COLUMN_NAME = 'IsExcludedFromCommunications'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OtherTreatmentPlanOrders.IsExcludedFromCommunications. Column [IsExcludedFromCommunications] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OtherTreatmentPlanOrders'
		AND COLUMN_NAME = 'IsFollowUpRequired'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OtherTreatmentPlanOrders.IsFollowUpRequired. Column [IsFollowUpRequired] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OtherTreatmentPlanOrders'
		AND COLUMN_NAME = 'SimpleDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.OtherTreatmentPlanOrders.SimpleDescription. Column [SimpleDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OtherTreatmentPlanOrders'
		AND COLUMN_NAME = 'TechnicalDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.OtherTreatmentPlanOrders.TechnicalDescription. Column [TechnicalDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OtherTreatmentPlanOrders'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OtherTreatmentPlanOrders.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'RefractivePrescriptions' exists...
IF OBJECT_ID(N'model.RefractivePrescriptions', 'U') IS NULL
BEGIN
-- 'RefractivePrescriptions' does not exist, creating...
	CREATE TABLE [model].[RefractivePrescriptions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IsExcludedFromCommunications] bit  NOT NULL,
    [IsFollowUpRequired] bit  NOT NULL,
    [SimpleDescription] nvarchar(max)  NULL,
    [TechnicalDescription] nvarchar(max)  NULL,
    [RefractivePrescriptionTypeId] int  NOT NULL
);
END
-- 'RefractivePrescriptions' exists, validating columns.
ELSE IF OBJECT_ID(N'model.RefractivePrescriptions', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RefractivePrescriptions'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.RefractivePrescriptions'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RefractivePrescriptions.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RefractivePrescriptions'
		AND COLUMN_NAME = 'IsExcludedFromCommunications'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RefractivePrescriptions.IsExcludedFromCommunications. Column [IsExcludedFromCommunications] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RefractivePrescriptions'
		AND COLUMN_NAME = 'IsFollowUpRequired'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RefractivePrescriptions.IsFollowUpRequired. Column [IsFollowUpRequired] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RefractivePrescriptions'
		AND COLUMN_NAME = 'SimpleDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.RefractivePrescriptions.SimpleDescription. Column [SimpleDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RefractivePrescriptions'
		AND COLUMN_NAME = 'TechnicalDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.RefractivePrescriptions.TechnicalDescription. Column [TechnicalDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RefractivePrescriptions'
		AND COLUMN_NAME = 'RefractivePrescriptionTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RefractivePrescriptions.RefractivePrescriptionTypeId. Column [RefractivePrescriptionTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'TreatmentPlanOrderConfigurations' exists...
IF OBJECT_ID(N'model.TreatmentPlanOrderConfigurations', 'U') IS NULL
BEGIN
-- 'TreatmentPlanOrderConfigurations' does not exist, creating...
	CREATE TABLE [model].[TreatmentPlanOrderConfigurations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrdinalId] int  NOT NULL,
    [UseProviderSignature] bit  NOT NULL,
    [IsHidden] bit  NOT NULL
);
END
-- 'TreatmentPlanOrderConfigurations' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TreatmentPlanOrderConfigurations', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanOrderConfigurations'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.TreatmentPlanOrderConfigurations'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanOrderConfigurations.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanOrderConfigurations'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanOrderConfigurations.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanOrderConfigurations'
		AND COLUMN_NAME = 'UseProviderSignature'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanOrderConfigurations.UseProviderSignature. Column [UseProviderSignature] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanOrderConfigurations'
		AND COLUMN_NAME = 'IsHidden'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanOrderConfigurations.IsHidden. Column [IsHidden] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterCommunicationWithOtherProviderOrders' exists...
IF OBJECT_ID(N'model.EncounterCommunicationWithOtherProviderOrders', 'U') IS NULL
BEGIN
-- 'EncounterCommunicationWithOtherProviderOrders' does not exist, creating...
	CREATE TABLE [model].[EncounterCommunicationWithOtherProviderOrders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IsPatientCopied] bit  NOT NULL,
    [EncounterId] int  NOT NULL,
    [CommunicationWithOtherProviderOrderId] int  NOT NULL,
    [Comment] nvarchar(max)  NULL,
    [CommunicationReviewStatusId] int  NOT NULL,
    [ReceiverExternalProviderId] int  NOT NULL
);
END
-- 'EncounterCommunicationWithOtherProviderOrders' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterCommunicationWithOtherProviderOrders', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterCommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterCommunicationWithOtherProviderOrders'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterCommunicationWithOtherProviderOrders.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterCommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'IsPatientCopied'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterCommunicationWithOtherProviderOrders.IsPatientCopied. Column [IsPatientCopied] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterCommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterCommunicationWithOtherProviderOrders.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterCommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'CommunicationWithOtherProviderOrderId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterCommunicationWithOtherProviderOrders.CommunicationWithOtherProviderOrderId. Column [CommunicationWithOtherProviderOrderId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterCommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'Comment'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterCommunicationWithOtherProviderOrders.Comment. Column [Comment] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterCommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'CommunicationReviewStatusId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterCommunicationWithOtherProviderOrders.CommunicationReviewStatusId. Column [CommunicationReviewStatusId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterCommunicationWithOtherProviderOrders'
		AND COLUMN_NAME = 'ReceiverExternalProviderId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterCommunicationWithOtherProviderOrders.ReceiverExternalProviderId. Column [ReceiverExternalProviderId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterLaboratoryTestOrders' exists...
IF OBJECT_ID(N'model.EncounterLaboratoryTestOrders', 'U') IS NULL
BEGIN
-- 'EncounterLaboratoryTestOrders' does not exist, creating...
	CREATE TABLE [model].[EncounterLaboratoryTestOrders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [Comment] nvarchar(max)  NULL,
    [LaboratoryTestOrderId] int  NOT NULL,
    [ExternalOrganizationId] int  NULL,
    [BodyPartId] int  NULL,
    [LateralityId] int  NULL,
    [LaboratoryTestingPriorityId] int  NULL,
    [PatientLaboratoryTestResultId] int  NULL,
    [AppointmentId] int  NULL
);
END
-- 'EncounterLaboratoryTestOrders' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterLaboratoryTestOrders', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterLaboratoryTestOrders'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterLaboratoryTestOrders'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterLaboratoryTestOrders.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterLaboratoryTestOrders'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterLaboratoryTestOrders.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterLaboratoryTestOrders'
		AND COLUMN_NAME = 'Comment'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterLaboratoryTestOrders.Comment. Column [Comment] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterLaboratoryTestOrders'
		AND COLUMN_NAME = 'LaboratoryTestOrderId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterLaboratoryTestOrders.LaboratoryTestOrderId. Column [LaboratoryTestOrderId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterLaboratoryTestOrders'
		AND COLUMN_NAME = 'ExternalOrganizationId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterLaboratoryTestOrders.ExternalOrganizationId. Column [ExternalOrganizationId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterLaboratoryTestOrders'
		AND COLUMN_NAME = 'BodyPartId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterLaboratoryTestOrders.BodyPartId. Column [BodyPartId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterLaboratoryTestOrders'
		AND COLUMN_NAME = 'LateralityId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterLaboratoryTestOrders.LateralityId. Column [LateralityId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterLaboratoryTestOrders'
		AND COLUMN_NAME = 'LaboratoryTestingPriorityId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterLaboratoryTestOrders.LaboratoryTestingPriorityId. Column [LaboratoryTestingPriorityId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterLaboratoryTestOrders'
		AND COLUMN_NAME = 'PatientLaboratoryTestResultId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterLaboratoryTestOrders.PatientLaboratoryTestResultId. Column [PatientLaboratoryTestResultId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterLaboratoryTestOrders'
		AND COLUMN_NAME = 'AppointmentId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterLaboratoryTestOrders.AppointmentId. Column [AppointmentId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterMedicationOrders' exists...
IF OBJECT_ID(N'model.EncounterMedicationOrders', 'U') IS NULL
BEGIN
-- 'EncounterMedicationOrders' does not exist, creating...
	CREATE TABLE [model].[EncounterMedicationOrders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PharmacistComment] nvarchar(max)  NULL,
    [EncounterId] int  NOT NULL,
    [MedicationOrderActionId] int  NOT NULL,
    [PatientMedicationDetailId] bigint  NOT NULL
);
END
-- 'EncounterMedicationOrders' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterMedicationOrders', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterMedicationOrders'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterMedicationOrders'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterMedicationOrders.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterMedicationOrders'
		AND COLUMN_NAME = 'PharmacistComment'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterMedicationOrders.PharmacistComment. Column [PharmacistComment] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterMedicationOrders'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterMedicationOrders.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterMedicationOrders'
		AND COLUMN_NAME = 'MedicationOrderActionId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterMedicationOrders.MedicationOrderActionId. Column [MedicationOrderActionId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterMedicationOrders'
		AND COLUMN_NAME = 'PatientMedicationDetailId'
		AND DATA_TYPE = 'bigint'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterMedicationOrders.PatientMedicationDetailId. Column [PatientMedicationDetailId] should be bigint, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterProcedureOrders' exists...
IF OBJECT_ID(N'model.EncounterProcedureOrders', 'U') IS NULL
BEGIN
-- 'EncounterProcedureOrders' does not exist, creating...
	CREATE TABLE [model].[EncounterProcedureOrders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [Comment] nvarchar(max)  NULL,
    [ProcedureOrderId] int  NOT NULL,
    [TimeUnit] int  NULL,
    [TimeFrameId] int  NULL,
    [TimeQualifierId] int  NULL,
    [ServiceLocationId] int  NULL,
    [DoctorId] int  NOT NULL,
    [RelativeTimeTypeId] int  NULL,
    [BodyPartId] int  NULL,
    [LateralityId] int  NULL,
    [PatientProcedurePerformedId] int  NULL,
    [AppointmentId] int  NULL
);
END
-- 'EncounterProcedureOrders' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterProcedureOrders', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterProcedureOrders'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'Comment'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.Comment. Column [Comment] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'ProcedureOrderId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.ProcedureOrderId. Column [ProcedureOrderId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'TimeUnit'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.TimeUnit. Column [TimeUnit] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'TimeFrameId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.TimeFrameId. Column [TimeFrameId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'TimeQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.TimeQualifierId. Column [TimeQualifierId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'ServiceLocationId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.ServiceLocationId. Column [ServiceLocationId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'DoctorId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.DoctorId. Column [DoctorId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'RelativeTimeTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.RelativeTimeTypeId. Column [RelativeTimeTypeId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'BodyPartId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.BodyPartId. Column [BodyPartId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'LateralityId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.LateralityId. Column [LateralityId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'PatientProcedurePerformedId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.PatientProcedurePerformedId. Column [PatientProcedurePerformedId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterProcedureOrders'
		AND COLUMN_NAME = 'AppointmentId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterProcedureOrders.AppointmentId. Column [AppointmentId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'MedicationFamilies' exists...
IF OBJECT_ID(N'model.MedicationFamilies', 'U') IS NULL
BEGIN
-- 'MedicationFamilies' does not exist, creating...
	CREATE TABLE [model].[MedicationFamilies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'MedicationFamilies' exists, validating columns.
ELSE IF OBJECT_ID(N'model.MedicationFamilies', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationFamilies'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.MedicationFamilies'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationFamilies.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationFamilies'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationFamilies.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'MedicationProgressComments' exists...
IF OBJECT_ID(N'model.MedicationProgressComments', 'U') IS NULL
BEGIN
-- 'MedicationProgressComments' does not exist, creating...
	CREATE TABLE [model].[MedicationProgressComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ShortName] nvarchar(max)  NOT NULL,
    [Value] nvarchar(max)  NOT NULL
);
END
-- 'MedicationProgressComments' exists, validating columns.
ELSE IF OBJECT_ID(N'model.MedicationProgressComments', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationProgressComments'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.MedicationProgressComments'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationProgressComments.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationProgressComments'
		AND COLUMN_NAME = 'ShortName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationProgressComments.ShortName. Column [ShortName] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationProgressComments'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationProgressComments.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'TreatmentPlanOrderInstructions' exists...
IF OBJECT_ID(N'model.TreatmentPlanOrderInstructions', 'U') IS NULL
BEGIN
-- 'TreatmentPlanOrderInstructions' does not exist, creating...
	CREATE TABLE [model].[TreatmentPlanOrderInstructions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ShortName] nvarchar(max)  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [DiagnosticTestOrderId] int  NULL,
    [LaboratoryTestOrderId] int  NULL,
    [ProcedureOrderId] int  NULL,
    [SubsequentVisitOrderId] int  NULL
);
END
-- 'TreatmentPlanOrderInstructions' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TreatmentPlanOrderInstructions', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanOrderInstructions'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.TreatmentPlanOrderInstructions'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanOrderInstructions.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanOrderInstructions'
		AND COLUMN_NAME = 'ShortName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanOrderInstructions.ShortName. Column [ShortName] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanOrderInstructions'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanOrderInstructions.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanOrderInstructions'
		AND COLUMN_NAME = 'DiagnosticTestOrderId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.TreatmentPlanOrderInstructions.DiagnosticTestOrderId. Column [DiagnosticTestOrderId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanOrderInstructions'
		AND COLUMN_NAME = 'LaboratoryTestOrderId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.TreatmentPlanOrderInstructions.LaboratoryTestOrderId. Column [LaboratoryTestOrderId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanOrderInstructions'
		AND COLUMN_NAME = 'ProcedureOrderId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.TreatmentPlanOrderInstructions.ProcedureOrderId. Column [ProcedureOrderId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanOrderInstructions'
		AND COLUMN_NAME = 'SubsequentVisitOrderId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.TreatmentPlanOrderInstructions.SubsequentVisitOrderId. Column [SubsequentVisitOrderId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'TimeQualifiers' exists...
IF OBJECT_ID(N'model.TimeQualifiers', 'U') IS NULL
BEGIN
-- 'TimeQualifiers' does not exist, creating...
	CREATE TABLE [model].[TimeQualifiers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrdinalId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'TimeQualifiers' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TimeQualifiers', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TimeQualifiers'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.TimeQualifiers'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TimeQualifiers.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TimeQualifiers'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TimeQualifiers.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TimeQualifiers'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TimeQualifiers.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TimeQualifiers'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TimeQualifiers.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'DiagnosticTestPerformeds' exists...
IF OBJECT_ID(N'model.DiagnosticTestPerformeds', 'U') IS NULL
BEGIN
-- 'DiagnosticTestPerformeds' does not exist, creating...
	CREATE TABLE [model].[DiagnosticTestPerformeds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ScreenId] int  NOT NULL
);
END
-- 'DiagnosticTestPerformeds' exists, validating columns.
ELSE IF OBJECT_ID(N'model.DiagnosticTestPerformeds', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DiagnosticTestPerformeds'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.DiagnosticTestPerformeds'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DiagnosticTestPerformeds.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DiagnosticTestPerformeds'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DiagnosticTestPerformeds.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DiagnosticTestPerformeds'
		AND COLUMN_NAME = 'ScreenId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DiagnosticTestPerformeds.ScreenId. Column [ScreenId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ProcedurePerformeds' exists...
IF OBJECT_ID(N'model.ProcedurePerformeds', 'U') IS NULL
BEGIN
-- 'ProcedurePerformeds' does not exist, creating...
	CREATE TABLE [model].[ProcedurePerformeds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ScreenId] int  NOT NULL
);
END
-- 'ProcedurePerformeds' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ProcedurePerformeds', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedurePerformeds'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ProcedurePerformeds'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedurePerformeds.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedurePerformeds'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedurePerformeds.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedurePerformeds'
		AND COLUMN_NAME = 'ScreenId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedurePerformeds.ScreenId. Column [ScreenId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'SpecialExamPerformeds' exists...
IF OBJECT_ID(N'model.SpecialExamPerformeds', 'U') IS NULL
BEGIN
-- 'SpecialExamPerformeds' does not exist, creating...
	CREATE TABLE [model].[SpecialExamPerformeds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ScreenId] int  NOT NULL
);
END
-- 'SpecialExamPerformeds' exists, validating columns.
ELSE IF OBJECT_ID(N'model.SpecialExamPerformeds', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SpecialExamPerformeds'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.SpecialExamPerformeds'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SpecialExamPerformeds.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SpecialExamPerformeds'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SpecialExamPerformeds.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SpecialExamPerformeds'
		AND COLUMN_NAME = 'ScreenId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SpecialExamPerformeds.ScreenId. Column [ScreenId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'OtherElementPerformeds' exists...
IF OBJECT_ID(N'model.OtherElementPerformeds', 'U') IS NULL
BEGIN
-- 'OtherElementPerformeds' does not exist, creating...
	CREATE TABLE [model].[OtherElementPerformeds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'OtherElementPerformeds' exists, validating columns.
ELSE IF OBJECT_ID(N'model.OtherElementPerformeds', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OtherElementPerformeds'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.OtherElementPerformeds'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OtherElementPerformeds.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OtherElementPerformeds'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OtherElementPerformeds.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryTestResults' exists...
IF OBJECT_ID(N'model.LaboratoryTestResults', 'U') IS NULL
BEGIN
-- 'LaboratoryTestResults' does not exist, creating...
	CREATE TABLE [model].[LaboratoryTestResults] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [ScreenId] int  NOT NULL,
    [LaboratoryTestId] int  NOT NULL
);
END
-- 'LaboratoryTestResults' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryTestResults', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestResults'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryTestResults'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestResults.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestResults'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.LaboratoryTestResults.Name. Column [Name] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestResults'
		AND COLUMN_NAME = 'ScreenId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestResults.ScreenId. Column [ScreenId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestResults'
		AND COLUMN_NAME = 'LaboratoryTestId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestResults.LaboratoryTestId. Column [LaboratoryTestId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientSpecialExamPerformeds' exists...
IF OBJECT_ID(N'model.PatientSpecialExamPerformeds', 'U') IS NULL
BEGIN
-- 'PatientSpecialExamPerformeds' does not exist, creating...
	CREATE TABLE [model].[PatientSpecialExamPerformeds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [ClinicalProcedureId] int  NOT NULL,
    [ClinicalDataSourceTypeId] int  NOT NULL
);
END
-- 'PatientSpecialExamPerformeds' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientSpecialExamPerformeds', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientSpecialExamPerformeds'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientSpecialExamPerformeds'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientSpecialExamPerformeds.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientSpecialExamPerformeds'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientSpecialExamPerformeds.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientSpecialExamPerformeds'
		AND COLUMN_NAME = 'ClinicalProcedureId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientSpecialExamPerformeds.ClinicalProcedureId. Column [ClinicalProcedureId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientSpecialExamPerformeds'
		AND COLUMN_NAME = 'ClinicalDataSourceTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientSpecialExamPerformeds.ClinicalDataSourceTypeId. Column [ClinicalDataSourceTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientOtherElementPerformeds' exists...
IF OBJECT_ID(N'model.PatientOtherElementPerformeds', 'U') IS NULL
BEGIN
-- 'PatientOtherElementPerformeds' does not exist, creating...
	CREATE TABLE [model].[PatientOtherElementPerformeds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [ClinicalProcedureId] int  NOT NULL,
    [ClinicalDataSourceTypeId] int  NOT NULL
);
END
-- 'PatientOtherElementPerformeds' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientOtherElementPerformeds', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientOtherElementPerformeds'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientOtherElementPerformeds'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientOtherElementPerformeds.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientOtherElementPerformeds'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientOtherElementPerformeds.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientOtherElementPerformeds'
		AND COLUMN_NAME = 'ClinicalProcedureId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientOtherElementPerformeds.ClinicalProcedureId. Column [ClinicalProcedureId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientOtherElementPerformeds'
		AND COLUMN_NAME = 'ClinicalDataSourceTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientOtherElementPerformeds.ClinicalDataSourceTypeId. Column [ClinicalDataSourceTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterTreatmentPlanComments' exists...
IF OBJECT_ID(N'model.EncounterTreatmentPlanComments', 'U') IS NULL
BEGIN
-- 'EncounterTreatmentPlanComments' does not exist, creating...
	CREATE TABLE [model].[EncounterTreatmentPlanComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [Value] nvarchar(max)  NOT NULL
);
END
-- 'EncounterTreatmentPlanComments' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterTreatmentPlanComments', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterTreatmentPlanComments'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterTreatmentPlanComments'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterTreatmentPlanComments.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterTreatmentPlanComments'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterTreatmentPlanComments.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterTreatmentPlanComments'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterTreatmentPlanComments.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'TreatmentPlanComments' exists...
IF OBJECT_ID(N'model.TreatmentPlanComments', 'U') IS NULL
BEGIN
-- 'TreatmentPlanComments' does not exist, creating...
	CREATE TABLE [model].[TreatmentPlanComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ShortName] nvarchar(max)  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'TreatmentPlanComments' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TreatmentPlanComments', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanComments'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.TreatmentPlanComments'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanComments.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanComments'
		AND COLUMN_NAME = 'ShortName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanComments.ShortName. Column [ShortName] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanComments'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanComments.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanComments'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanComments.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterRefractivePrescriptions' exists...
IF OBJECT_ID(N'model.EncounterRefractivePrescriptions', 'U') IS NULL
BEGIN
-- 'EncounterRefractivePrescriptions' does not exist, creating...
	CREATE TABLE [model].[EncounterRefractivePrescriptions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [Comment] nvarchar(max)  NULL,
    [RefractivePrescriptionId] int  NOT NULL
);
END
-- 'EncounterRefractivePrescriptions' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterRefractivePrescriptions', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterRefractivePrescriptions'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterRefractivePrescriptions'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterRefractivePrescriptions.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterRefractivePrescriptions'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterRefractivePrescriptions.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterRefractivePrescriptions'
		AND COLUMN_NAME = 'Comment'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterRefractivePrescriptions.Comment. Column [Comment] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterRefractivePrescriptions'
		AND COLUMN_NAME = 'RefractivePrescriptionId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterRefractivePrescriptions.RefractivePrescriptionId. Column [RefractivePrescriptionId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterOtherTreatmentPlanOrders' exists...
IF OBJECT_ID(N'model.EncounterOtherTreatmentPlanOrders', 'U') IS NULL
BEGIN
-- 'EncounterOtherTreatmentPlanOrders' does not exist, creating...
	CREATE TABLE [model].[EncounterOtherTreatmentPlanOrders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [OtherTreatmentPlanOrderId] int  NOT NULL
);
END
-- 'EncounterOtherTreatmentPlanOrders' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterOtherTreatmentPlanOrders', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterOtherTreatmentPlanOrders'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterOtherTreatmentPlanOrders'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterOtherTreatmentPlanOrders.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterOtherTreatmentPlanOrders'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterOtherTreatmentPlanOrders.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterOtherTreatmentPlanOrders'
		AND COLUMN_NAME = 'OtherTreatmentPlanOrderId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterOtherTreatmentPlanOrders.OtherTreatmentPlanOrderId. Column [OtherTreatmentPlanOrderId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'MedicationOrderFavorites' exists...
IF OBJECT_ID(N'model.MedicationOrderFavorites', 'U') IS NULL
BEGIN
-- 'MedicationOrderFavorites' does not exist, creating...
	CREATE TABLE [model].[MedicationOrderFavorites] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [MedicationId] int  NOT NULL,
    [DisplayName] nvarchar(max)  NULL,
    [DaysSupply] int  NULL,
    [DrugDispenseFormId] int  NULL,
    [DrugDispenseAmount] decimal(18,0)  NULL,
    [DrugDosageNumberId] int  NOT NULL,
    [TimeUnit] int  NULL,
    [TimeFrameId] int  NULL,
    [FrequencyId] int  NULL,
    [IsDispenseAsWritten] bit  NOT NULL,
    [RouteOfAdministrationId] int  NULL,
    [RefillCount] int  NULL,
    [AsPrescribedComment] nvarchar(max)  NOT NULL,
    [DoctorId] int  NULL
);
END
-- 'MedicationOrderFavorites' exists, validating columns.
ELSE IF OBJECT_ID(N'model.MedicationOrderFavorites', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.MedicationOrderFavorites'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'MedicationId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.MedicationId. Column [MedicationId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'DisplayName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.DisplayName. Column [DisplayName] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'DaysSupply'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.DaysSupply. Column [DaysSupply] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'DrugDispenseFormId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.DrugDispenseFormId. Column [DrugDispenseFormId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'DrugDispenseAmount'
		AND DATA_TYPE = 'decimal'
		AND NUMERIC_PRECISION = '18'
		AND NUMERIC_SCALE = '0'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.DrugDispenseAmount. Column [DrugDispenseAmount] should be decimal(18,0), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'DrugDosageNumberId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.DrugDosageNumberId. Column [DrugDosageNumberId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'TimeUnit'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.TimeUnit. Column [TimeUnit] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'TimeFrameId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.TimeFrameId. Column [TimeFrameId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'FrequencyId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.FrequencyId. Column [FrequencyId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'IsDispenseAsWritten'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.IsDispenseAsWritten. Column [IsDispenseAsWritten] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'RouteOfAdministrationId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.RouteOfAdministrationId. Column [RouteOfAdministrationId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'RefillCount'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.RefillCount. Column [RefillCount] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'AsPrescribedComment'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.AsPrescribedComment. Column [AsPrescribedComment] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationOrderFavorites'
		AND COLUMN_NAME = 'DoctorId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.MedicationOrderFavorites.DoctorId. Column [DoctorId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'DrugDispenseForms' exists...
IF OBJECT_ID(N'model.DrugDispenseForms', 'U') IS NULL
BEGIN
-- 'DrugDispenseForms' does not exist, creating...
	CREATE TABLE [model].[DrugDispenseForms] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'DrugDispenseForms' exists, validating columns.
ELSE IF OBJECT_ID(N'model.DrugDispenseForms', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDispenseForms'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.DrugDispenseForms'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDispenseForms.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDispenseForms'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDispenseForms.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDispenseForms'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDispenseForms.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'Frequencies' exists...
IF OBJECT_ID(N'model.Frequencies', 'U') IS NULL
BEGIN
-- 'Frequencies' does not exist, creating...
	CREATE TABLE [model].[Frequencies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DisplayName] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'Frequencies' exists, validating columns.
ELSE IF OBJECT_ID(N'model.Frequencies', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='Frequencies'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.Frequencies'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.Frequencies.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='Frequencies'
		AND COLUMN_NAME = 'DisplayName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.Frequencies.DisplayName. Column [DisplayName] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='Frequencies'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.Frequencies.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='Frequencies'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.Frequencies.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ClinicalDataSourceTypes' exists...
IF OBJECT_ID(N'model.ClinicalDataSourceTypes', 'U') IS NULL
BEGIN
-- 'ClinicalDataSourceTypes' does not exist, creating...
	CREATE TABLE [model].[ClinicalDataSourceTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'ClinicalDataSourceTypes' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ClinicalDataSourceTypes', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalDataSourceTypes'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ClinicalDataSourceTypes'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalDataSourceTypes.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalDataSourceTypes'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalDataSourceTypes.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'Organs' exists...
IF OBJECT_ID(N'model.Organs', 'U') IS NULL
BEGIN
-- 'Organs' does not exist, creating...
	CREATE TABLE [model].[Organs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [BodySystemId] int  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'Organs' exists, validating columns.
ELSE IF OBJECT_ID(N'model.Organs', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='Organs'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.Organs'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.Organs.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='Organs'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.Organs.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='Organs'
		AND COLUMN_NAME = 'BodySystemId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.Organs.BodySystemId. Column [BodySystemId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='Organs'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.Organs.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'BodyParts' exists...
IF OBJECT_ID(N'model.BodyParts', 'U') IS NULL
BEGIN
-- 'BodyParts' does not exist, creating...
	CREATE TABLE [model].[BodyParts] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrganId] int  NULL,
    [OrdinalId] int  NOT NULL,
    [BodySystemId] int  NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'BodyParts' exists, validating columns.
ELSE IF OBJECT_ID(N'model.BodyParts', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyParts'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.BodyParts'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.BodyParts.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyParts'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.BodyParts.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyParts'
		AND COLUMN_NAME = 'OrganId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.BodyParts.OrganId. Column [OrganId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyParts'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.BodyParts.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyParts'
		AND COLUMN_NAME = 'BodySystemId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.BodyParts.BodySystemId. Column [BodySystemId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyParts'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.BodyParts.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'OrganStructures' exists...
IF OBJECT_ID(N'model.OrganStructures', 'U') IS NULL
BEGIN
-- 'OrganStructures' does not exist, creating...
	CREATE TABLE [model].[OrganStructures] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrganId] int  NOT NULL,
    [OrganStructureGroupId] int  NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'OrganStructures' exists, validating columns.
ELSE IF OBJECT_ID(N'model.OrganStructures', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OrganStructures'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.OrganStructures'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OrganStructures.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OrganStructures'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OrganStructures.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OrganStructures'
		AND COLUMN_NAME = 'OrganId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OrganStructures.OrganId. Column [OrganId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OrganStructures'
		AND COLUMN_NAME = 'OrganStructureGroupId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.OrganStructures.OrganStructureGroupId. Column [OrganStructureGroupId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OrganStructures'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OrganStructures.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'OrganStructureGroups' exists...
IF OBJECT_ID(N'model.OrganStructureGroups', 'U') IS NULL
BEGIN
-- 'OrganStructureGroups' does not exist, creating...
	CREATE TABLE [model].[OrganStructureGroups] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'OrganStructureGroups' exists, validating columns.
ELSE IF OBJECT_ID(N'model.OrganStructureGroups', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OrganStructureGroups'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.OrganStructureGroups'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OrganStructureGroups.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OrganStructureGroups'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OrganStructureGroups.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'BodyLocations' exists...
IF OBJECT_ID(N'model.BodyLocations', 'U') IS NULL
BEGIN
-- 'BodyLocations' does not exist, creating...
	CREATE TABLE [model].[BodyLocations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [BodyPartId] int  NULL,
    [OrganStructureId] int  NULL,
    [OrdinalId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'BodyLocations' exists, validating columns.
ELSE IF OBJECT_ID(N'model.BodyLocations', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyLocations'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.BodyLocations'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.BodyLocations.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyLocations'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.BodyLocations.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyLocations'
		AND COLUMN_NAME = 'BodyPartId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.BodyLocations.BodyPartId. Column [BodyPartId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyLocations'
		AND COLUMN_NAME = 'OrganStructureId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.BodyLocations.OrganStructureId. Column [OrganStructureId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyLocations'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.BodyLocations.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyLocations'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.BodyLocations.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'DrugDosageActions' exists...
IF OBJECT_ID(N'model.DrugDosageActions', 'U') IS NULL
BEGIN
-- 'DrugDosageActions' does not exist, creating...
	CREATE TABLE [model].[DrugDosageActions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'DrugDosageActions' exists, validating columns.
ELSE IF OBJECT_ID(N'model.DrugDosageActions', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDosageActions'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.DrugDosageActions'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDosageActions.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDosageActions'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDosageActions.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDosageActions'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDosageActions.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'DrugDosageNumbers' exists...
IF OBJECT_ID(N'model.DrugDosageNumbers', 'U') IS NULL
BEGIN
-- 'DrugDosageNumbers' does not exist, creating...
	CREATE TABLE [model].[DrugDosageNumbers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [OrdinalId] nvarchar(max)  NOT NULL
);
END
-- 'DrugDosageNumbers' exists, validating columns.
ELSE IF OBJECT_ID(N'model.DrugDosageNumbers', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDosageNumbers'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.DrugDosageNumbers'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDosageNumbers.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDosageNumbers'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDosageNumbers.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDosageNumbers'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDosageNumbers.OrdinalId. Column [OrdinalId] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientMedicationProgressComments' exists...
IF OBJECT_ID(N'model.PatientMedicationProgressComments', 'U') IS NULL
BEGIN
-- 'PatientMedicationProgressComments' does not exist, creating...
	CREATE TABLE [model].[PatientMedicationProgressComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [PatientMedicationId] int  NOT NULL,
    [DateTime] datetime  NOT NULL
);
END
-- 'PatientMedicationProgressComments' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientMedicationProgressComments', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientMedicationProgressComments'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientMedicationProgressComments'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientMedicationProgressComments.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientMedicationProgressComments'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientMedicationProgressComments.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientMedicationProgressComments'
		AND COLUMN_NAME = 'PatientMedicationId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientMedicationProgressComments.PatientMedicationId. Column [PatientMedicationId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientMedicationProgressComments'
		AND COLUMN_NAME = 'DateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientMedicationProgressComments.DateTime. Column [DateTime] should be datetime, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ClinicalQualifierCategories' exists...
IF OBJECT_ID(N'model.ClinicalQualifierCategories', 'U') IS NULL
BEGIN
-- 'ClinicalQualifierCategories' does not exist, creating...
	CREATE TABLE [model].[ClinicalQualifierCategories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IsSingleUse] bit  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'ClinicalQualifierCategories' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ClinicalQualifierCategories', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalQualifierCategories'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ClinicalQualifierCategories'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalQualifierCategories.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalQualifierCategories'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalQualifierCategories.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalQualifierCategories'
		AND COLUMN_NAME = 'IsSingleUse'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalQualifierCategories.IsSingleUse. Column [IsSingleUse] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalQualifierCategories'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalQualifierCategories.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterClinicalDrawings' exists...
IF OBJECT_ID(N'model.EncounterClinicalDrawings', 'U') IS NULL
BEGIN
-- 'EncounterClinicalDrawings' does not exist, creating...
	CREATE TABLE [model].[EncounterClinicalDrawings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [ClinicalDrawingTemplateId] int  NULL
);
END
-- 'EncounterClinicalDrawings' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterClinicalDrawings', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterClinicalDrawings'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterClinicalDrawings'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterClinicalDrawings.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterClinicalDrawings'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterClinicalDrawings.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterClinicalDrawings'
		AND COLUMN_NAME = 'ClinicalDrawingTemplateId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterClinicalDrawings.ClinicalDrawingTemplateId. Column [ClinicalDrawingTemplateId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'ClinicalDrawingTemplates' exists...
IF OBJECT_ID(N'model.ClinicalDrawingTemplates', 'U') IS NULL
BEGIN
-- 'ClinicalDrawingTemplates' does not exist, creating...
	CREATE TABLE [model].[ClinicalDrawingTemplates] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'ClinicalDrawingTemplates' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ClinicalDrawingTemplates', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalDrawingTemplates'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ClinicalDrawingTemplates'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalDrawingTemplates.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalDrawingTemplates'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalDrawingTemplates.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'RelativeTimeTypes' exists...
IF OBJECT_ID(N'model.RelativeTimeTypes', 'U') IS NULL
BEGIN
-- 'RelativeTimeTypes' does not exist, creating...
	CREATE TABLE [model].[RelativeTimeTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'RelativeTimeTypes' exists, validating columns.
ELSE IF OBJECT_ID(N'model.RelativeTimeTypes', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RelativeTimeTypes'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.RelativeTimeTypes'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RelativeTimeTypes.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RelativeTimeTypes'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RelativeTimeTypes.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ClinicalHierarchyClinicalAttribute' exists...
IF OBJECT_ID(N'model.ClinicalHierarchyClinicalAttribute', 'U') IS NULL
BEGIN
-- 'ClinicalHierarchyClinicalAttribute' does not exist, creating...
	CREATE TABLE [model].[ClinicalHierarchyClinicalAttribute] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ClinicalHierarchyId] int  NOT NULL,
    [ClinicalAttributeId] int  NOT NULL
);
END
-- 'ClinicalHierarchyClinicalAttribute' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ClinicalHierarchyClinicalAttribute', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalHierarchyClinicalAttribute'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ClinicalHierarchyClinicalAttribute'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalHierarchyClinicalAttribute.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalHierarchyClinicalAttribute'
		AND COLUMN_NAME = 'ClinicalHierarchyId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalHierarchyClinicalAttribute.ClinicalHierarchyId. Column [ClinicalHierarchyId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalHierarchyClinicalAttribute'
		AND COLUMN_NAME = 'ClinicalAttributeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalHierarchyClinicalAttribute.ClinicalAttributeId. Column [ClinicalAttributeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'OrganSubStructures' exists...
IF OBJECT_ID(N'model.OrganSubStructures', 'U') IS NULL
BEGIN
-- 'OrganSubStructures' does not exist, creating...
	CREATE TABLE [model].[OrganSubStructures] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrganStructureId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'OrganSubStructures' exists, validating columns.
ELSE IF OBJECT_ID(N'model.OrganSubStructures', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OrganSubStructures'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.OrganSubStructures'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OrganSubStructures.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OrganSubStructures'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OrganSubStructures.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OrganSubStructures'
		AND COLUMN_NAME = 'OrganStructureId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OrganSubStructures.OrganStructureId. Column [OrganStructureId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OrganSubStructures'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OrganSubStructures.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'AllergenCategorys' exists...
IF OBJECT_ID(N'model.AllergenCategorys', 'U') IS NULL
BEGIN
-- 'AllergenCategorys' does not exist, creating...
	CREATE TABLE [model].[AllergenCategorys] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'AllergenCategorys' exists, validating columns.
ELSE IF OBJECT_ID(N'model.AllergenCategorys', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenCategorys'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.AllergenCategorys'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AllergenCategorys.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenCategorys'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AllergenCategorys.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenCategorys'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AllergenCategorys.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'AllergenOccurrences' exists...
IF OBJECT_ID(N'model.AllergenOccurrences', 'U') IS NULL
BEGIN
-- 'AllergenOccurrences' does not exist, creating...
	CREATE TABLE [model].[AllergenOccurrences] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OccurrenceDateTime] nvarchar(max)  NULL,
    [UserId] int  NOT NULL,
    [EnteredDateTime] datetime  NOT NULL,
    [Comment] nvarchar(max)  NULL,
    [TimeUnit] int  NULL,
    [TimeFrameId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL,
    [TimeQualifierId] int  NULL,
    [PatientAllergenAllergenReactionTypeId] int  NOT NULL
);
END
-- 'AllergenOccurrences' exists, validating columns.
ELSE IF OBJECT_ID(N'model.AllergenOccurrences', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenOccurrences'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.AllergenOccurrences'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AllergenOccurrences.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenOccurrences'
		AND COLUMN_NAME = 'OccurrenceDateTime'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.AllergenOccurrences.OccurrenceDateTime. Column [OccurrenceDateTime] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenOccurrences'
		AND COLUMN_NAME = 'UserId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AllergenOccurrences.UserId. Column [UserId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenOccurrences'
		AND COLUMN_NAME = 'EnteredDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AllergenOccurrences.EnteredDateTime. Column [EnteredDateTime] should be datetime, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenOccurrences'
		AND COLUMN_NAME = 'Comment'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.AllergenOccurrences.Comment. Column [Comment] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenOccurrences'
		AND COLUMN_NAME = 'TimeUnit'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.AllergenOccurrences.TimeUnit. Column [TimeUnit] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenOccurrences'
		AND COLUMN_NAME = 'TimeFrameId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AllergenOccurrences.TimeFrameId. Column [TimeFrameId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenOccurrences'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AllergenOccurrences.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenOccurrences'
		AND COLUMN_NAME = 'TimeQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.AllergenOccurrences.TimeQualifierId. Column [TimeQualifierId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergenOccurrences'
		AND COLUMN_NAME = 'PatientAllergenAllergenReactionTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AllergenOccurrences.PatientAllergenAllergenReactionTypeId. Column [PatientAllergenAllergenReactionTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientDiagnosisDetails' exists...
IF OBJECT_ID(N'model.PatientDiagnosisDetails', 'U') IS NULL
BEGIN
-- 'PatientDiagnosisDetails' does not exist, creating...
	CREATE TABLE [model].[PatientDiagnosisDetails] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PatientDiagnosisId] int  NOT NULL,
    [StartDateTime] datetime  NULL,
    [EndDateTime] datetime  NULL,
    [IsDeactivated] bit  NOT NULL,
    [EnteredDateTime] datetime  NOT NULL,
    [UserId] int  NOT NULL,
    [Comment] nvarchar(max)  NULL,
    [RelevancyQualifierId] int  NULL,
    [AccuracyQualifierId] int  NULL,
    [ClinicalConditionStatusId] int  NOT NULL,
    [BodyLocationId] int  NULL,
    [IsOnProblemList] bit  NOT NULL,
    [EncounterId] int  NULL,
    [IsEncounterDiagnosis] bit  NOT NULL,
    [EncounterTreatmentGoalAndInstructionId] int  NULL
);
END
-- 'PatientDiagnosisDetails' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientDiagnosisDetails', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientDiagnosisDetails'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'PatientDiagnosisId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.PatientDiagnosisId. Column [PatientDiagnosisId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'StartDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.StartDateTime. Column [StartDateTime] should be datetime, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'EndDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.EndDateTime. Column [EndDateTime] should be datetime, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'EnteredDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.EnteredDateTime. Column [EnteredDateTime] should be datetime, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'UserId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.UserId. Column [UserId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'Comment'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.Comment. Column [Comment] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'RelevancyQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.RelevancyQualifierId. Column [RelevancyQualifierId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'AccuracyQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.AccuracyQualifierId. Column [AccuracyQualifierId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'ClinicalConditionStatusId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.ClinicalConditionStatusId. Column [ClinicalConditionStatusId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'BodyLocationId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.BodyLocationId. Column [BodyLocationId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'IsOnProblemList'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.IsOnProblemList. Column [IsOnProblemList] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.EncounterId. Column [EncounterId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'IsEncounterDiagnosis'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.IsEncounterDiagnosis. Column [IsEncounterDiagnosis] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetails'
		AND COLUMN_NAME = 'EncounterTreatmentGoalAndInstructionId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetails.EncounterTreatmentGoalAndInstructionId. Column [EncounterTreatmentGoalAndInstructionId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientDiagnosisDetailQualifiers' exists...
IF OBJECT_ID(N'model.PatientDiagnosisDetailQualifiers', 'U') IS NULL
BEGIN
-- 'PatientDiagnosisDetailQualifiers' does not exist, creating...
	CREATE TABLE [model].[PatientDiagnosisDetailQualifiers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrdinalId] int  NOT NULL,
    [ClinicalAttributeId] int  NOT NULL,
    [PatientDiagnosisDetailId] int  NOT NULL,
    [ClinicalQualifierId] int  NOT NULL
);
END
-- 'PatientDiagnosisDetailQualifiers' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientDiagnosisDetailQualifiers', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetailQualifiers'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientDiagnosisDetailQualifiers'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetailQualifiers.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetailQualifiers'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetailQualifiers.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetailQualifiers'
		AND COLUMN_NAME = 'ClinicalAttributeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetailQualifiers.ClinicalAttributeId. Column [ClinicalAttributeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetailQualifiers'
		AND COLUMN_NAME = 'PatientDiagnosisDetailId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetailQualifiers.PatientDiagnosisDetailId. Column [PatientDiagnosisDetailId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetailQualifiers'
		AND COLUMN_NAME = 'ClinicalQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetailQualifiers.ClinicalQualifierId. Column [ClinicalQualifierId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientDiagnosisComments' exists...
IF OBJECT_ID(N'model.PatientDiagnosisComments', 'U') IS NULL
BEGIN
-- 'PatientDiagnosisComments' does not exist, creating...
	CREATE TABLE [model].[PatientDiagnosisComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PatientId] int  NOT NULL,
    [ClinicalDataSourceTypeId] int  NOT NULL,
    [UserId] int  NOT NULL,
    [EnteredDateTime] datetime  NOT NULL,
    [StartDateTime] datetime  NULL,
    [EndDateTime] datetime  NULL,
    [IsDeactivated] bit  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [AccuracyQualifierId] int  NULL,
    [RelevancyQualifierId] int  NULL,
    [ClinicalConditionStatusId] int  NOT NULL,
    [EncounterId] int  NULL,
    [LateralityId] int  NULL,
    [OrganStructureId] int  NULL,
    [BodyPartId] int  NULL
);
END
-- 'PatientDiagnosisComments' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientDiagnosisComments', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientDiagnosisComments'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'PatientId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.PatientId. Column [PatientId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'ClinicalDataSourceTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.ClinicalDataSourceTypeId. Column [ClinicalDataSourceTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'UserId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.UserId. Column [UserId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'EnteredDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.EnteredDateTime. Column [EnteredDateTime] should be datetime, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'StartDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.StartDateTime. Column [StartDateTime] should be datetime, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'EndDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.EndDateTime. Column [EndDateTime] should be datetime, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'AccuracyQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.AccuracyQualifierId. Column [AccuracyQualifierId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'RelevancyQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.RelevancyQualifierId. Column [RelevancyQualifierId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'ClinicalConditionStatusId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.ClinicalConditionStatusId. Column [ClinicalConditionStatusId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.EncounterId. Column [EncounterId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'LateralityId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.LateralityId. Column [LateralityId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'OrganStructureId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.OrganStructureId. Column [OrganStructureId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisComments'
		AND COLUMN_NAME = 'BodyPartId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientDiagnosisComments.BodyPartId. Column [BodyPartId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientAllergenComments' exists...
IF OBJECT_ID(N'model.PatientAllergenComments', 'U') IS NULL
BEGIN
-- 'PatientAllergenComments' does not exist, creating...
	CREATE TABLE [model].[PatientAllergenComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PatientId] int  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [UserId] int  NOT NULL,
    [EnteredDateTime] datetime  NOT NULL,
    [ClinicalDataSourceTypeId] int  NOT NULL,
    [StartDateTime] datetime  NULL,
    [EndDateTime] datetime  NULL,
    [IsDeactivated] bit  NOT NULL,
    [AccuracyQualifierId] int  NULL,
    [RelevancyQualifierId] int  NULL,
    [ClinicalConditionStatusId] int  NOT NULL
);
END
-- 'PatientAllergenComments' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientAllergenComments', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenComments'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientAllergenComments'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenComments.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenComments'
		AND COLUMN_NAME = 'PatientId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenComments.PatientId. Column [PatientId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenComments'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenComments.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenComments'
		AND COLUMN_NAME = 'UserId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenComments.UserId. Column [UserId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenComments'
		AND COLUMN_NAME = 'EnteredDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenComments.EnteredDateTime. Column [EnteredDateTime] should be datetime, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenComments'
		AND COLUMN_NAME = 'ClinicalDataSourceTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenComments.ClinicalDataSourceTypeId. Column [ClinicalDataSourceTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenComments'
		AND COLUMN_NAME = 'StartDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientAllergenComments.StartDateTime. Column [StartDateTime] should be datetime, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenComments'
		AND COLUMN_NAME = 'EndDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientAllergenComments.EndDateTime. Column [EndDateTime] should be datetime, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenComments'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenComments.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenComments'
		AND COLUMN_NAME = 'AccuracyQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientAllergenComments.AccuracyQualifierId. Column [AccuracyQualifierId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenComments'
		AND COLUMN_NAME = 'RelevancyQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientAllergenComments.RelevancyQualifierId. Column [RelevancyQualifierId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenComments'
		AND COLUMN_NAME = 'ClinicalConditionStatusId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenComments.ClinicalConditionStatusId. Column [ClinicalConditionStatusId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ClinicalHistoryReviewTypes' exists...
IF OBJECT_ID(N'model.ClinicalHistoryReviewTypes', 'U') IS NULL
BEGIN
-- 'ClinicalHistoryReviewTypes' does not exist, creating...
	CREATE TABLE [model].[ClinicalHistoryReviewTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'ClinicalHistoryReviewTypes' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ClinicalHistoryReviewTypes', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalHistoryReviewTypes'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ClinicalHistoryReviewTypes'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalHistoryReviewTypes.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalHistoryReviewTypes'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalHistoryReviewTypes.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientMedicalReviews' exists...
IF OBJECT_ID(N'model.PatientMedicalReviews', 'U') IS NULL
BEGIN
-- 'PatientMedicalReviews' does not exist, creating...
	CREATE TABLE [model].[PatientMedicalReviews] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PatientId] int  NOT NULL,
    [ReviewDateTime] datetime  NOT NULL,
    [ClinicalHistoryReviewTypeId] int  NOT NULL,
    [UserId] int  NOT NULL,
    [ClinicalHistoryReviewAreaId] int  NOT NULL
);
END
-- 'PatientMedicalReviews' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientMedicalReviews', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientMedicalReviews'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientMedicalReviews'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientMedicalReviews.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientMedicalReviews'
		AND COLUMN_NAME = 'PatientId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientMedicalReviews.PatientId. Column [PatientId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientMedicalReviews'
		AND COLUMN_NAME = 'ReviewDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientMedicalReviews.ReviewDateTime. Column [ReviewDateTime] should be datetime, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientMedicalReviews'
		AND COLUMN_NAME = 'ClinicalHistoryReviewTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientMedicalReviews.ClinicalHistoryReviewTypeId. Column [ClinicalHistoryReviewTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientMedicalReviews'
		AND COLUMN_NAME = 'UserId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientMedicalReviews.UserId. Column [UserId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientMedicalReviews'
		AND COLUMN_NAME = 'ClinicalHistoryReviewAreaId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientMedicalReviews.ClinicalHistoryReviewAreaId. Column [ClinicalHistoryReviewAreaId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ClinicalHistoryReviewAreas' exists...
IF OBJECT_ID(N'model.ClinicalHistoryReviewAreas', 'U') IS NULL
BEGIN
-- 'ClinicalHistoryReviewAreas' does not exist, creating...
	CREATE TABLE [model].[ClinicalHistoryReviewAreas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'ClinicalHistoryReviewAreas' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ClinicalHistoryReviewAreas', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalHistoryReviewAreas'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ClinicalHistoryReviewAreas'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalHistoryReviewAreas.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalHistoryReviewAreas'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalHistoryReviewAreas.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryComponents' exists...
IF OBJECT_ID(N'model.LaboratoryComponents', 'U') IS NULL
BEGIN
-- 'LaboratoryComponents' does not exist, creating...
	CREATE TABLE [model].[LaboratoryComponents] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratoryComponents' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryComponents', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryComponents'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryComponents'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryComponents.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryComponents'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryComponents.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryMeasuredProperties' exists...
IF OBJECT_ID(N'model.LaboratoryMeasuredProperties', 'U') IS NULL
BEGIN
-- 'LaboratoryMeasuredProperties' does not exist, creating...
	CREATE TABLE [model].[LaboratoryMeasuredProperties] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratoryMeasuredProperties' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryMeasuredProperties', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryMeasuredProperties'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryMeasuredProperties'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryMeasuredProperties.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryMeasuredProperties'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryMeasuredProperties.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryTimings' exists...
IF OBJECT_ID(N'model.LaboratoryTimings', 'U') IS NULL
BEGIN
-- 'LaboratoryTimings' does not exist, creating...
	CREATE TABLE [model].[LaboratoryTimings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratoryTimings' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryTimings', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTimings'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryTimings'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTimings.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTimings'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTimings.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'SpecimenSources' exists...
IF OBJECT_ID(N'model.SpecimenSources', 'U') IS NULL
BEGIN
-- 'SpecimenSources' does not exist, creating...
	CREATE TABLE [model].[SpecimenSources] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'SpecimenSources' exists, validating columns.
ELSE IF OBJECT_ID(N'model.SpecimenSources', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SpecimenSources'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.SpecimenSources'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SpecimenSources.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SpecimenSources'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SpecimenSources.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryScaleTypes' exists...
IF OBJECT_ID(N'model.LaboratoryScaleTypes', 'U') IS NULL
BEGIN
-- 'LaboratoryScaleTypes' does not exist, creating...
	CREATE TABLE [model].[LaboratoryScaleTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratoryScaleTypes' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryScaleTypes', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryScaleTypes'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryScaleTypes'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryScaleTypes.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryScaleTypes'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryScaleTypes.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryMethodUseds' exists...
IF OBJECT_ID(N'model.LaboratoryMethodUseds', 'U') IS NULL
BEGIN
-- 'LaboratoryMethodUseds' does not exist, creating...
	CREATE TABLE [model].[LaboratoryMethodUseds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratoryMethodUseds' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryMethodUseds', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryMethodUseds'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryMethodUseds'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryMethodUseds.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryMethodUseds'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryMethodUseds.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryNormalRanges' exists...
IF OBJECT_ID(N'model.LaboratoryNormalRanges', 'U') IS NULL
BEGIN
-- 'LaboratoryNormalRanges' does not exist, creating...
	CREATE TABLE [model].[LaboratoryNormalRanges] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratoryNormalRanges' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryNormalRanges', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryNormalRanges'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryNormalRanges'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryNormalRanges.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryNormalRanges'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryNormalRanges.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryTestingInstruments' exists...
IF OBJECT_ID(N'model.LaboratoryTestingInstruments', 'U') IS NULL
BEGIN
-- 'LaboratoryTestingInstruments' does not exist, creating...
	CREATE TABLE [model].[LaboratoryTestingInstruments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratoryTestingInstruments' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryTestingInstruments', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestingInstruments'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryTestingInstruments'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestingInstruments.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestingInstruments'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestingInstruments.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryCollectionSites' exists...
IF OBJECT_ID(N'model.LaboratoryCollectionSites', 'U') IS NULL
BEGIN
-- 'LaboratoryCollectionSites' does not exist, creating...
	CREATE TABLE [model].[LaboratoryCollectionSites] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratoryCollectionSites' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryCollectionSites', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryCollectionSites'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryCollectionSites'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryCollectionSites.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryCollectionSites'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryCollectionSites.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryTestingPriorities' exists...
IF OBJECT_ID(N'model.LaboratoryTestingPriorities', 'U') IS NULL
BEGIN
-- 'LaboratoryTestingPriorities' does not exist, creating...
	CREATE TABLE [model].[LaboratoryTestingPriorities] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratoryTestingPriorities' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryTestingPriorities', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestingPriorities'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryTestingPriorities'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestingPriorities.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestingPriorities'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestingPriorities.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryResultVerifications' exists...
IF OBJECT_ID(N'model.LaboratoryResultVerifications', 'U') IS NULL
BEGIN
-- 'LaboratoryResultVerifications' does not exist, creating...
	CREATE TABLE [model].[LaboratoryResultVerifications] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratoryResultVerifications' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryResultVerifications', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryResultVerifications'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryResultVerifications'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryResultVerifications.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryResultVerifications'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryResultVerifications.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratorySampleSizes' exists...
IF OBJECT_ID(N'model.LaboratorySampleSizes', 'U') IS NULL
BEGIN
-- 'LaboratorySampleSizes' does not exist, creating...
	CREATE TABLE [model].[LaboratorySampleSizes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratorySampleSizes' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratorySampleSizes', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratorySampleSizes'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratorySampleSizes'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratorySampleSizes.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratorySampleSizes'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratorySampleSizes.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryTestingPlaces' exists...
IF OBJECT_ID(N'model.LaboratoryTestingPlaces', 'U') IS NULL
BEGIN
-- 'LaboratoryTestingPlaces' does not exist, creating...
	CREATE TABLE [model].[LaboratoryTestingPlaces] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratoryTestingPlaces' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryTestingPlaces', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestingPlaces'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryTestingPlaces'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestingPlaces.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestingPlaces'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestingPlaces.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'VaccineManufacturers' exists...
IF OBJECT_ID(N'model.VaccineManufacturers', 'U') IS NULL
BEGIN
-- 'VaccineManufacturers' does not exist, creating...
	CREATE TABLE [model].[VaccineManufacturers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'VaccineManufacturers' exists, validating columns.
ELSE IF OBJECT_ID(N'model.VaccineManufacturers', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccineManufacturers'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.VaccineManufacturers'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccineManufacturers.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccineManufacturers'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccineManufacturers.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccineManufacturers'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccineManufacturers.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ClinicalAttributes' exists...
IF OBJECT_ID(N'model.ClinicalAttributes', 'U') IS NULL
BEGIN
-- 'ClinicalAttributes' does not exist, creating...
	CREATE TABLE [model].[ClinicalAttributes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [IsDeactivated] bit  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'ClinicalAttributes' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ClinicalAttributes', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalAttributes'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ClinicalAttributes'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalAttributes.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalAttributes'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ClinicalAttributes.Name. Column [Name] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalAttributes'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalAttributes.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalAttributes'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalAttributes.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ScreenTypes' exists...
IF OBJECT_ID(N'model.ScreenTypes', 'U') IS NULL
BEGIN
-- 'ScreenTypes' does not exist, creating...
	CREATE TABLE [model].[ScreenTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'ScreenTypes' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ScreenTypes', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ScreenTypes'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ScreenTypes'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ScreenTypes.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ScreenTypes'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ScreenTypes.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryTests' exists...
IF OBJECT_ID(N'model.LaboratoryTests', 'U') IS NULL
BEGIN
-- 'LaboratoryTests' does not exist, creating...
	CREATE TABLE [model].[LaboratoryTests] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'LaboratoryTests' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryTests', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTests'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryTests'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTests.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTests'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.LaboratoryTests.Name. Column [Name] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTests'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTests.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTests'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTests.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'VaccinationAdministereds' exists...
IF OBJECT_ID(N'model.VaccinationAdministereds', 'U') IS NULL
BEGIN
-- 'VaccinationAdministereds' does not exist, creating...
	CREATE TABLE [model].[VaccinationAdministereds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [ScreenId] int  NOT NULL,
    [VaccineId] int  NOT NULL
);
END
-- 'VaccinationAdministereds' exists, validating columns.
ELSE IF OBJECT_ID(N'model.VaccinationAdministereds', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccinationAdministereds'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.VaccinationAdministereds'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccinationAdministereds.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccinationAdministereds'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.VaccinationAdministereds.Name. Column [Name] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccinationAdministereds'
		AND COLUMN_NAME = 'ScreenId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccinationAdministereds.ScreenId. Column [ScreenId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccinationAdministereds'
		AND COLUMN_NAME = 'VaccineId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccinationAdministereds.VaccineId. Column [VaccineId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'AllergySources' exists...
IF OBJECT_ID(N'model.AllergySources', 'U') IS NULL
BEGIN
-- 'AllergySources' does not exist, creating...
	CREATE TABLE [model].[AllergySources] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'AllergySources' exists, validating columns.
ELSE IF OBJECT_ID(N'model.AllergySources', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergySources'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.AllergySources'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AllergySources.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergySources'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.AllergySources.Name. Column [Name] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergySources'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AllergySources.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AllergySources'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AllergySources.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'VaccineTypes' exists...
IF OBJECT_ID(N'model.VaccineTypes', 'U') IS NULL
BEGIN
-- 'VaccineTypes' does not exist, creating...
	CREATE TABLE [model].[VaccineTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'VaccineTypes' exists, validating columns.
ELSE IF OBJECT_ID(N'model.VaccineTypes', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccineTypes'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.VaccineTypes'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccineTypes.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccineTypes'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccineTypes.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ClinicalProcedures' exists...
IF OBJECT_ID(N'model.ClinicalProcedures', 'U') IS NULL
BEGIN
-- 'ClinicalProcedures' does not exist, creating...
	CREATE TABLE [model].[ClinicalProcedures] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [IsSocial] bit  NOT NULL,
    [ClinicalProcedureCategoryId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'ClinicalProcedures' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ClinicalProcedures', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalProcedures'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ClinicalProcedures'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalProcedures.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalProcedures'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ClinicalProcedures.Name. Column [Name] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalProcedures'
		AND COLUMN_NAME = 'IsSocial'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalProcedures.IsSocial. Column [IsSocial] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalProcedures'
		AND COLUMN_NAME = 'ClinicalProcedureCategoryId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalProcedures.ClinicalProcedureCategoryId. Column [ClinicalProcedureCategoryId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalProcedures'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalProcedures.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'RelevancyQualifiers' exists...
IF OBJECT_ID(N'model.RelevancyQualifiers', 'U') IS NULL
BEGIN
-- 'RelevancyQualifiers' does not exist, creating...
	CREATE TABLE [model].[RelevancyQualifiers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'RelevancyQualifiers' exists, validating columns.
ELSE IF OBJECT_ID(N'model.RelevancyQualifiers', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RelevancyQualifiers'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.RelevancyQualifiers'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RelevancyQualifiers.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RelevancyQualifiers'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.RelevancyQualifiers.Name. Column [Name] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RelevancyQualifiers'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RelevancyQualifiers.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RelevancyQualifiers'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RelevancyQualifiers.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'AccuracyQualifiers' exists...
IF OBJECT_ID(N'model.AccuracyQualifiers', 'U') IS NULL
BEGIN
-- 'AccuracyQualifiers' does not exist, creating...
	CREATE TABLE [model].[AccuracyQualifiers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'AccuracyQualifiers' exists, validating columns.
ELSE IF OBJECT_ID(N'model.AccuracyQualifiers', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AccuracyQualifiers'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.AccuracyQualifiers'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AccuracyQualifiers.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AccuracyQualifiers'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.AccuracyQualifiers.Name. Column [Name] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AccuracyQualifiers'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AccuracyQualifiers.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AccuracyQualifiers'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AccuracyQualifiers.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'AxisQualifiers' exists...
IF OBJECT_ID(N'model.AxisQualifiers', 'U') IS NULL
BEGIN
-- 'AxisQualifiers' does not exist, creating...
	CREATE TABLE [model].[AxisQualifiers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'AxisQualifiers' exists, validating columns.
ELSE IF OBJECT_ID(N'model.AxisQualifiers', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AxisQualifiers'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.AxisQualifiers'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AxisQualifiers.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AxisQualifiers'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.AxisQualifiers.Name. Column [Name] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AxisQualifiers'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AxisQualifiers.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AxisQualifiers'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AxisQualifiers.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ClinicalQualifiers' exists...
IF OBJECT_ID(N'model.ClinicalQualifiers', 'U') IS NULL
BEGIN
-- 'ClinicalQualifiers' does not exist, creating...
	CREATE TABLE [model].[ClinicalQualifiers] (
    [SimpleDescription] nvarchar(max)  NULL,
    [TechnicalDescription] nvarchar(max)  NULL,
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [ClinicalQualifierCategoryId] int  NULL,
    [IsDeactivated] bit  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'ClinicalQualifiers' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ClinicalQualifiers', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalQualifiers'
		AND COLUMN_NAME = 'SimpleDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ClinicalQualifiers.SimpleDescription. Column [SimpleDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalQualifiers'
		AND COLUMN_NAME = 'TechnicalDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ClinicalQualifiers.TechnicalDescription. Column [TechnicalDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalQualifiers'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ClinicalQualifiers'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalQualifiers.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalQualifiers'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ClinicalQualifiers.Name. Column [Name] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalQualifiers'
		AND COLUMN_NAME = 'ClinicalQualifierCategoryId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ClinicalQualifiers.ClinicalQualifierCategoryId. Column [ClinicalQualifierCategoryId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalQualifiers'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalQualifiers.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalQualifiers'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalQualifiers.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ClinicalConditions' exists...
IF OBJECT_ID(N'model.ClinicalConditions', 'U') IS NULL
BEGIN
-- 'ClinicalConditions' does not exist, creating...
	CREATE TABLE [model].[ClinicalConditions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [IsSocial] bit  NOT NULL,
    [OrganSubStructureId] int  NULL,
    [IsDeactivated] bit  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'ClinicalConditions' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ClinicalConditions', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalConditions'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ClinicalConditions'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalConditions.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalConditions'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ClinicalConditions.Name. Column [Name] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalConditions'
		AND COLUMN_NAME = 'IsSocial'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalConditions.IsSocial. Column [IsSocial] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalConditions'
		AND COLUMN_NAME = 'OrganSubStructureId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ClinicalConditions.OrganSubStructureId. Column [OrganSubStructureId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalConditions'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalConditions.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ClinicalConditions'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ClinicalConditions.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'SmokingConditions' exists...
IF OBJECT_ID(N'model.SmokingConditions', 'U') IS NULL
BEGIN
-- 'SmokingConditions' does not exist, creating...
	CREATE TABLE [model].[SmokingConditions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL
);
END
-- 'SmokingConditions' exists, validating columns.
ELSE IF OBJECT_ID(N'model.SmokingConditions', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SmokingConditions'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.SmokingConditions'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SmokingConditions.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SmokingConditions'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.SmokingConditions.Name. Column [Name] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SmokingConditions'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SmokingConditions.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SmokingConditions'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SmokingConditions.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'DrugDaysSupplies' exists...
IF OBJECT_ID(N'model.DrugDaysSupplies', 'U') IS NULL
BEGIN
-- 'DrugDaysSupplies' does not exist, creating...
	CREATE TABLE [model].[DrugDaysSupplies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrdinalId] nvarchar(max)  NOT NULL,
    [Value] int  NOT NULL
);
END
-- 'DrugDaysSupplies' exists, validating columns.
ELSE IF OBJECT_ID(N'model.DrugDaysSupplies', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDaysSupplies'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.DrugDaysSupplies'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDaysSupplies.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDaysSupplies'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDaysSupplies.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDaysSupplies'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDaysSupplies.OrdinalId. Column [OrdinalId] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DrugDaysSupplies'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DrugDaysSupplies.Value. Column [Value] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'VaccineDoses' exists...
IF OBJECT_ID(N'model.VaccineDoses', 'U') IS NULL
BEGIN
-- 'VaccineDoses' does not exist, creating...
	CREATE TABLE [model].[VaccineDoses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [MilliliterDose] decimal(18,0)  NOT NULL,
    [OrdinalId] nvarchar(max)  NOT NULL,
    [DisplayName] nvarchar(max)  NOT NULL,
    [OutputName] nvarchar(max)  NULL
);
END
-- 'VaccineDoses' exists, validating columns.
ELSE IF OBJECT_ID(N'model.VaccineDoses', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccineDoses'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.VaccineDoses'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccineDoses.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccineDoses'
		AND COLUMN_NAME = 'MilliliterDose'
		AND DATA_TYPE = 'decimal'
		AND NUMERIC_PRECISION = '18'
		AND NUMERIC_SCALE = '0'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccineDoses.MilliliterDose. Column [MilliliterDose] should be decimal(18,0), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccineDoses'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccineDoses.OrdinalId. Column [OrdinalId] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccineDoses'
		AND COLUMN_NAME = 'DisplayName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccineDoses.DisplayName. Column [DisplayName] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccineDoses'
		AND COLUMN_NAME = 'OutputName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.VaccineDoses.OutputName. Column [OutputName] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'InjectionSites' exists...
IF OBJECT_ID(N'model.InjectionSites', 'U') IS NULL
BEGIN
-- 'InjectionSites' does not exist, creating...
	CREATE TABLE [model].[InjectionSites] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DisplayName] nvarchar(max)  NOT NULL,
    [OutputName] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'InjectionSites' exists, validating columns.
ELSE IF OBJECT_ID(N'model.InjectionSites', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='InjectionSites'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.InjectionSites'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.InjectionSites.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='InjectionSites'
		AND COLUMN_NAME = 'DisplayName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.InjectionSites.DisplayName. Column [DisplayName] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='InjectionSites'
		AND COLUMN_NAME = 'OutputName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.InjectionSites.OutputName. Column [OutputName] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='InjectionSites'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.InjectionSites.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'HypodermicNeedles' exists...
IF OBJECT_ID(N'model.HypodermicNeedles', 'U') IS NULL
BEGIN
-- 'HypodermicNeedles' does not exist, creating...
	CREATE TABLE [model].[HypodermicNeedles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DisplayName] nvarchar(max)  NOT NULL,
    [OutputName] nvarchar(max)  NOT NULL,
    [Gauge] nvarchar(max)  NULL,
    [NeedleLength] decimal(18,0)  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'HypodermicNeedles' exists, validating columns.
ELSE IF OBJECT_ID(N'model.HypodermicNeedles', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HypodermicNeedles'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.HypodermicNeedles'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.HypodermicNeedles.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HypodermicNeedles'
		AND COLUMN_NAME = 'DisplayName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.HypodermicNeedles.DisplayName. Column [DisplayName] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HypodermicNeedles'
		AND COLUMN_NAME = 'OutputName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.HypodermicNeedles.OutputName. Column [OutputName] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HypodermicNeedles'
		AND COLUMN_NAME = 'Gauge'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.HypodermicNeedles.Gauge. Column [Gauge] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HypodermicNeedles'
		AND COLUMN_NAME = 'NeedleLength'
		AND DATA_TYPE = 'decimal'
		AND NUMERIC_PRECISION = '18'
		AND NUMERIC_SCALE = '0'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.HypodermicNeedles.NeedleLength. Column [NeedleLength] should be decimal(18,0), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='HypodermicNeedles'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.HypodermicNeedles.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterHistoryOfPresentIllnessComments' exists...
IF OBJECT_ID(N'model.EncounterHistoryOfPresentIllnessComments', 'U') IS NULL
BEGIN
-- 'EncounterHistoryOfPresentIllnessComments' does not exist, creating...
	CREATE TABLE [model].[EncounterHistoryOfPresentIllnessComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [EncounterId] int  NOT NULL,
    [IsOnProblemList] bit  NOT NULL
);
END
-- 'EncounterHistoryOfPresentIllnessComments' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterHistoryOfPresentIllnessComments', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterHistoryOfPresentIllnessComments'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterHistoryOfPresentIllnessComments'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterHistoryOfPresentIllnessComments.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterHistoryOfPresentIllnessComments'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterHistoryOfPresentIllnessComments.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterHistoryOfPresentIllnessComments'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterHistoryOfPresentIllnessComments.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterHistoryOfPresentIllnessComments'
		AND COLUMN_NAME = 'IsOnProblemList'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterHistoryOfPresentIllnessComments.IsOnProblemList. Column [IsOnProblemList] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'SpecimenConditions' exists...
IF OBJECT_ID(N'model.SpecimenConditions', 'U') IS NULL
BEGIN
-- 'SpecimenConditions' does not exist, creating...
	CREATE TABLE [model].[SpecimenConditions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'SpecimenConditions' exists, validating columns.
ELSE IF OBJECT_ID(N'model.SpecimenConditions', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SpecimenConditions'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.SpecimenConditions'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SpecimenConditions.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='SpecimenConditions'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.SpecimenConditions.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratorySpecimenRejectReasons' exists...
IF OBJECT_ID(N'model.LaboratorySpecimenRejectReasons', 'U') IS NULL
BEGIN
-- 'LaboratorySpecimenRejectReasons' does not exist, creating...
	CREATE TABLE [model].[LaboratorySpecimenRejectReasons] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'LaboratorySpecimenRejectReasons' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratorySpecimenRejectReasons', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratorySpecimenRejectReasons'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratorySpecimenRejectReasons'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratorySpecimenRejectReasons.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratorySpecimenRejectReasons'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratorySpecimenRejectReasons.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryTestAbnormalFlags' exists...
IF OBJECT_ID(N'model.LaboratoryTestAbnormalFlags', 'U') IS NULL
BEGIN
-- 'LaboratoryTestAbnormalFlags' does not exist, creating...
	CREATE TABLE [model].[LaboratoryTestAbnormalFlags] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Abbreviation] nvarchar(max)  NOT NULL
);
END
-- 'LaboratoryTestAbnormalFlags' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryTestAbnormalFlags', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestAbnormalFlags'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.LaboratoryTestAbnormalFlags'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestAbnormalFlags.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestAbnormalFlags'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestAbnormalFlags.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestAbnormalFlags'
		AND COLUMN_NAME = 'Abbreviation'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestAbnormalFlags.Abbreviation. Column [Abbreviation] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'UnitsOfMeasurement' exists...
IF OBJECT_ID(N'model.UnitsOfMeasurement', 'U') IS NULL
BEGIN
-- 'UnitsOfMeasurement' does not exist, creating...
	CREATE TABLE [model].[UnitsOfMeasurement] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Abbreviation] nvarchar(max)  NOT NULL,
    [FullName] nvarchar(max)  NOT NULL,
    [OrdinalId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL,
    [UnitOfMeasurementTypeId] int  NULL
);
END
-- 'UnitsOfMeasurement' exists, validating columns.
ELSE IF OBJECT_ID(N'model.UnitsOfMeasurement', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UnitsOfMeasurement'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.UnitsOfMeasurement'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UnitsOfMeasurement.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UnitsOfMeasurement'
		AND COLUMN_NAME = 'Abbreviation'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UnitsOfMeasurement.Abbreviation. Column [Abbreviation] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UnitsOfMeasurement'
		AND COLUMN_NAME = 'FullName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UnitsOfMeasurement.FullName. Column [FullName] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UnitsOfMeasurement'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UnitsOfMeasurement.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UnitsOfMeasurement'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UnitsOfMeasurement.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UnitsOfMeasurement'
		AND COLUMN_NAME = 'UnitOfMeasurementTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.UnitsOfMeasurement.UnitOfMeasurementTypeId. Column [UnitOfMeasurementTypeId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'TransitionOfCareReasons' exists...
IF OBJECT_ID(N'model.TransitionOfCareReasons', 'U') IS NULL
BEGIN
-- 'TransitionOfCareReasons' does not exist, creating...
	CREATE TABLE [model].[TransitionOfCareReasons] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NOT NULL
);
END
-- 'TransitionOfCareReasons' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TransitionOfCareReasons', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TransitionOfCareReasons'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.TransitionOfCareReasons'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TransitionOfCareReasons.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TransitionOfCareReasons'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TransitionOfCareReasons.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ObservationTypes' exists...
IF OBJECT_ID(N'model.ObservationTypes', 'U') IS NULL
BEGIN
-- 'ObservationTypes' does not exist, creating...
	CREATE TABLE [model].[ObservationTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ObservationTypeCategoryId] int  NOT NULL,
    [IsDeactivated] bit  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'ObservationTypes' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ObservationTypes', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ObservationTypes'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ObservationTypes'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ObservationTypes.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ObservationTypes'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ObservationTypes.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ObservationTypes'
		AND COLUMN_NAME = 'ObservationTypeCategoryId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ObservationTypes.ObservationTypeCategoryId. Column [ObservationTypeCategoryId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ObservationTypes'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ObservationTypes.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ObservationTypes'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ObservationTypes.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ChiefComplaints' exists...
IF OBJECT_ID(N'model.ChiefComplaints', 'U') IS NULL
BEGIN
-- 'ChiefComplaints' does not exist, creating...
	CREATE TABLE [model].[ChiefComplaints] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ChiefComplaintCategoryId] int  NOT NULL,
    [SimpleDescription] nvarchar(max)  NULL,
    [TechnicalDescription] nvarchar(max)  NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'ChiefComplaints' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ChiefComplaints', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ChiefComplaints'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ChiefComplaints'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ChiefComplaints.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ChiefComplaints'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ChiefComplaints.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ChiefComplaints'
		AND COLUMN_NAME = 'ChiefComplaintCategoryId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ChiefComplaints.ChiefComplaintCategoryId. Column [ChiefComplaintCategoryId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ChiefComplaints'
		AND COLUMN_NAME = 'SimpleDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ChiefComplaints.SimpleDescription. Column [SimpleDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ChiefComplaints'
		AND COLUMN_NAME = 'TechnicalDescription'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.ChiefComplaints.TechnicalDescription. Column [TechnicalDescription] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ChiefComplaints'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ChiefComplaints.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterChiefComplaints' exists...
IF OBJECT_ID(N'model.EncounterChiefComplaints', 'U') IS NULL
BEGIN
-- 'EncounterChiefComplaints' does not exist, creating...
	CREATE TABLE [model].[EncounterChiefComplaints] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [LateralityId] int  NULL,
    [ChiefComplaintId] int  NOT NULL
);
END
-- 'EncounterChiefComplaints' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterChiefComplaints', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterChiefComplaints'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterChiefComplaints'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterChiefComplaints.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterChiefComplaints'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterChiefComplaints.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterChiefComplaints'
		AND COLUMN_NAME = 'LateralityId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterChiefComplaints.LateralityId. Column [LateralityId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterChiefComplaints'
		AND COLUMN_NAME = 'ChiefComplaintId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterChiefComplaints.ChiefComplaintId. Column [ChiefComplaintId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'CommunicationTransactions' exists...
IF OBJECT_ID(N'model.CommunicationTransactions', 'U') IS NULL
BEGIN
-- 'CommunicationTransactions' does not exist, creating...
	CREATE TABLE [model].[CommunicationTransactions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterCommunicationWithOtherProviderOrderId] int  NULL,
    [ExternalProviderId] int  NULL,
    [PatientId] int  NULL,
    [DateTime] nvarchar(max)  NOT NULL,
    [CommunicationMethodTypeId] int  NOT NULL,
    [CommunicationTransmissionStatusId] int  NOT NULL
);
END
-- 'CommunicationTransactions' exists, validating columns.
ELSE IF OBJECT_ID(N'model.CommunicationTransactions', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationTransactions'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.CommunicationTransactions'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationTransactions.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationTransactions'
		AND COLUMN_NAME = 'EncounterCommunicationWithOtherProviderOrderId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.CommunicationTransactions.EncounterCommunicationWithOtherProviderOrderId. Column [EncounterCommunicationWithOtherProviderOrderId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationTransactions'
		AND COLUMN_NAME = 'ExternalProviderId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.CommunicationTransactions.ExternalProviderId. Column [ExternalProviderId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationTransactions'
		AND COLUMN_NAME = 'PatientId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.CommunicationTransactions.PatientId. Column [PatientId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationTransactions'
		AND COLUMN_NAME = 'DateTime'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationTransactions.DateTime. Column [DateTime] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationTransactions'
		AND COLUMN_NAME = 'CommunicationMethodTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationTransactions.CommunicationMethodTypeId. Column [CommunicationMethodTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationTransactions'
		AND COLUMN_NAME = 'CommunicationTransmissionStatusId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationTransactions.CommunicationTransmissionStatusId. Column [CommunicationTransmissionStatusId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ObservationTypeCategories' exists...
IF OBJECT_ID(N'model.ObservationTypeCategories', 'U') IS NULL
BEGIN
-- 'ObservationTypeCategories' does not exist, creating...
	CREATE TABLE [model].[ObservationTypeCategories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'ObservationTypeCategories' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ObservationTypeCategories', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ObservationTypeCategories'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ObservationTypeCategories'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ObservationTypeCategories.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ObservationTypeCategories'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ObservationTypeCategories.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ChiefComplaintCategories' exists...
IF OBJECT_ID(N'model.ChiefComplaintCategories', 'U') IS NULL
BEGIN
-- 'ChiefComplaintCategories' does not exist, creating...
	CREATE TABLE [model].[ChiefComplaintCategories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
END
-- 'ChiefComplaintCategories' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ChiefComplaintCategories', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ChiefComplaintCategories'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.ChiefComplaintCategories'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ChiefComplaintCategories.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ChiefComplaintCategories'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ChiefComplaintCategories.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterChiefComplaintComments' exists...
IF OBJECT_ID(N'model.EncounterChiefComplaintComments', 'U') IS NULL
BEGIN
-- 'EncounterChiefComplaintComments' does not exist, creating...
	CREATE TABLE [model].[EncounterChiefComplaintComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [Value] nvarchar(max)  NOT NULL
);
END
-- 'EncounterChiefComplaintComments' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterChiefComplaintComments', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterChiefComplaintComments'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterChiefComplaintComments'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterChiefComplaintComments.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterChiefComplaintComments'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterChiefComplaintComments.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterChiefComplaintComments'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterChiefComplaintComments.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterReasonForVisitComments' exists...
IF OBJECT_ID(N'model.EncounterReasonForVisitComments', 'U') IS NULL
BEGIN
-- 'EncounterReasonForVisitComments' does not exist, creating...
	CREATE TABLE [model].[EncounterReasonForVisitComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [LateralityId] int  NULL,
    [BodyPartId] int  NULL,
    [UserId] int  NOT NULL
);
END
-- 'EncounterReasonForVisitComments' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterReasonForVisitComments', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReasonForVisitComments'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterReasonForVisitComments'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterReasonForVisitComments.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReasonForVisitComments'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterReasonForVisitComments.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReasonForVisitComments'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterReasonForVisitComments.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReasonForVisitComments'
		AND COLUMN_NAME = 'LateralityId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterReasonForVisitComments.LateralityId. Column [LateralityId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReasonForVisitComments'
		AND COLUMN_NAME = 'BodyPartId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.EncounterReasonForVisitComments.BodyPartId. Column [BodyPartId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReasonForVisitComments'
		AND COLUMN_NAME = 'UserId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterReasonForVisitComments.UserId. Column [UserId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterReviewOfSystemComments' exists...
IF OBJECT_ID(N'model.EncounterReviewOfSystemComments', 'U') IS NULL
BEGIN
-- 'EncounterReviewOfSystemComments' does not exist, creating...
	CREATE TABLE [model].[EncounterReviewOfSystemComments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [Value] nvarchar(max)  NOT NULL
);
END
-- 'EncounterReviewOfSystemComments' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterReviewOfSystemComments', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReviewOfSystemComments'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterReviewOfSystemComments'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterReviewOfSystemComments.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReviewOfSystemComments'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterReviewOfSystemComments.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReviewOfSystemComments'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterReviewOfSystemComments.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientDiagnosisDetailAxisQualifiers' exists...
IF OBJECT_ID(N'model.PatientDiagnosisDetailAxisQualifiers', 'U') IS NULL
BEGIN
-- 'PatientDiagnosisDetailAxisQualifiers' does not exist, creating...
	CREATE TABLE [model].[PatientDiagnosisDetailAxisQualifiers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PatientDiagnosisDetailId] int  NOT NULL,
    [AxisQualifierId] int  NOT NULL
);
END
-- 'PatientDiagnosisDetailAxisQualifiers' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientDiagnosisDetailAxisQualifiers', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetailAxisQualifiers'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientDiagnosisDetailAxisQualifiers'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetailAxisQualifiers.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetailAxisQualifiers'
		AND COLUMN_NAME = 'PatientDiagnosisDetailId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetailAxisQualifiers.PatientDiagnosisDetailId. Column [PatientDiagnosisDetailId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientDiagnosisDetailAxisQualifiers'
		AND COLUMN_NAME = 'AxisQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientDiagnosisDetailAxisQualifiers.AxisQualifierId. Column [AxisQualifierId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientFamilyHistoryEntries' exists...
IF OBJECT_ID(N'model.PatientFamilyHistoryEntries', 'U') IS NULL
BEGIN
-- 'PatientFamilyHistoryEntries' does not exist, creating...
	CREATE TABLE [model].[PatientFamilyHistoryEntries] (
    [PatientId] int  NOT NULL,
    [ClinicalConditionId] int  NOT NULL,
    [ClinicalDataSourceTypeId] int  NOT NULL,
    [Id] int IDENTITY(1,1) NOT NULL
);
END
-- 'PatientFamilyHistoryEntries' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientFamilyHistoryEntries', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntries'
		AND COLUMN_NAME = 'PatientId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntries.PatientId. Column [PatientId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntries'
		AND COLUMN_NAME = 'ClinicalConditionId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntries.ClinicalConditionId. Column [ClinicalConditionId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntries'
		AND COLUMN_NAME = 'ClinicalDataSourceTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntries.ClinicalDataSourceTypeId. Column [ClinicalDataSourceTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntries'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientFamilyHistoryEntries'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntries.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientFamilyHistoryEntryDetails' exists...
IF OBJECT_ID(N'model.PatientFamilyHistoryEntryDetails', 'U') IS NULL
BEGIN
-- 'PatientFamilyHistoryEntryDetails' does not exist, creating...
	CREATE TABLE [model].[PatientFamilyHistoryEntryDetails] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PatientFamilyHistoryEntryId] int  NOT NULL,
    [StartDateTime] datetime  NULL,
    [EndDateTime] datetime  NULL,
    [UserId] int  NULL,
    [EnteredDateTime] datetime  NOT NULL,
    [IsDeactivated] bit  NOT NULL,
    [AccuracyQualifierId] int  NULL,
    [RelevancyQualifierId] int  NULL,
    [Comment] nvarchar(max)  NULL
);
END
-- 'PatientFamilyHistoryEntryDetails' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientFamilyHistoryEntryDetails', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntryDetails'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientFamilyHistoryEntryDetails'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntryDetails.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntryDetails'
		AND COLUMN_NAME = 'PatientFamilyHistoryEntryId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntryDetails.PatientFamilyHistoryEntryId. Column [PatientFamilyHistoryEntryId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntryDetails'
		AND COLUMN_NAME = 'StartDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntryDetails.StartDateTime. Column [StartDateTime] should be datetime, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntryDetails'
		AND COLUMN_NAME = 'EndDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntryDetails.EndDateTime. Column [EndDateTime] should be datetime, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntryDetails'
		AND COLUMN_NAME = 'UserId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntryDetails.UserId. Column [UserId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntryDetails'
		AND COLUMN_NAME = 'EnteredDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntryDetails.EnteredDateTime. Column [EnteredDateTime] should be datetime, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntryDetails'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntryDetails.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntryDetails'
		AND COLUMN_NAME = 'AccuracyQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntryDetails.AccuracyQualifierId. Column [AccuracyQualifierId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntryDetails'
		AND COLUMN_NAME = 'RelevancyQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntryDetails.RelevancyQualifierId. Column [RelevancyQualifierId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntryDetails'
		AND COLUMN_NAME = 'Comment'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntryDetails.Comment. Column [Comment] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'FamilyRelationships' exists...
IF OBJECT_ID(N'model.FamilyRelationships', 'U') IS NULL
BEGIN
-- 'FamilyRelationships' does not exist, creating...
	CREATE TABLE [model].[FamilyRelationships] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IsDeactivated] bit  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'FamilyRelationships' exists, validating columns.
ELSE IF OBJECT_ID(N'model.FamilyRelationships', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='FamilyRelationships'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.FamilyRelationships'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.FamilyRelationships.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='FamilyRelationships'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.FamilyRelationships.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='FamilyRelationships'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.FamilyRelationships.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='FamilyRelationships'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.FamilyRelationships.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'CommunicationCommunicationReviewStatus' exists...
IF OBJECT_ID(N'model.CommunicationCommunicationReviewStatus', 'U') IS NULL
BEGIN
-- 'CommunicationCommunicationReviewStatus' does not exist, creating...
	CREATE TABLE [model].[CommunicationCommunicationReviewStatus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrdinalId] int  NOT NULL,
    [IsDeactivatedId] bit  NOT NULL
);
END
-- 'CommunicationCommunicationReviewStatus' exists, validating columns.
ELSE IF OBJECT_ID(N'model.CommunicationCommunicationReviewStatus', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationCommunicationReviewStatus'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.CommunicationCommunicationReviewStatus'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationCommunicationReviewStatus.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationCommunicationReviewStatus'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationCommunicationReviewStatus.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationCommunicationReviewStatus'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationCommunicationReviewStatus.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationCommunicationReviewStatus'
		AND COLUMN_NAME = 'IsDeactivatedId'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationCommunicationReviewStatus.IsDeactivatedId. Column [IsDeactivatedId] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'UnitOfMeasurementTypes' exists...
IF OBJECT_ID(N'model.UnitOfMeasurementTypes', 'U') IS NULL
BEGIN
-- 'UnitOfMeasurementTypes' does not exist, creating...
	CREATE TABLE [model].[UnitOfMeasurementTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IsDeactivated] bit  NOT NULL,
    [OrdinalId] int  NOT NULL
);
END
-- 'UnitOfMeasurementTypes' exists, validating columns.
ELSE IF OBJECT_ID(N'model.UnitOfMeasurementTypes', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UnitOfMeasurementTypes'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.UnitOfMeasurementTypes'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UnitOfMeasurementTypes.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UnitOfMeasurementTypes'
		AND COLUMN_NAME = 'Name'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UnitOfMeasurementTypes.Name. Column [Name] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UnitOfMeasurementTypes'
		AND COLUMN_NAME = 'IsDeactivated'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UnitOfMeasurementTypes.IsDeactivated. Column [IsDeactivated] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UnitOfMeasurementTypes'
		AND COLUMN_NAME = 'OrdinalId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UnitOfMeasurementTypes.OrdinalId. Column [OrdinalId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientAllergenConcerns' exists...
IF OBJECT_ID(N'model.PatientAllergenConcerns', 'U') IS NULL
BEGIN
-- 'PatientAllergenConcerns' does not exist, creating...
	CREATE TABLE [model].[PatientAllergenConcerns] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ConcernLevelId] int  NOT NULL
);
END
-- 'PatientAllergenConcerns' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientAllergenConcerns', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenConcerns'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientAllergenConcerns'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenConcerns.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenConcerns'
		AND COLUMN_NAME = 'ConcernLevelId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenConcerns.ConcernLevelId. Column [ConcernLevelId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientProblemConcernLevels' exists...
IF OBJECT_ID(N'model.PatientProblemConcernLevels', 'U') IS NULL
BEGIN
-- 'PatientProblemConcernLevels' does not exist, creating...
	CREATE TABLE [model].[PatientProblemConcernLevels] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PatientDiagnosisId] int  NULL,
    [PatientAllergenId] int  NULL,
    [ConcernStartDate] datetime  NULL,
    [ConcernEndDate] datetime  NULL,
    [ConcernLevelId] int  NOT NULL,
    [EnteredDateTime] datetime  NOT NULL
);
END
-- 'PatientProblemConcernLevels' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientProblemConcernLevels', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientProblemConcernLevels'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientProblemConcernLevels'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientProblemConcernLevels.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientProblemConcernLevels'
		AND COLUMN_NAME = 'PatientDiagnosisId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientProblemConcernLevels.PatientDiagnosisId. Column [PatientDiagnosisId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientProblemConcernLevels'
		AND COLUMN_NAME = 'PatientAllergenId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientProblemConcernLevels.PatientAllergenId. Column [PatientAllergenId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientProblemConcernLevels'
		AND COLUMN_NAME = 'ConcernStartDate'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientProblemConcernLevels.ConcernStartDate. Column [ConcernStartDate] should be datetime, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientProblemConcernLevels'
		AND COLUMN_NAME = 'ConcernEndDate'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientProblemConcernLevels.ConcernEndDate. Column [ConcernEndDate] should be datetime, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientProblemConcernLevels'
		AND COLUMN_NAME = 'ConcernLevelId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientProblemConcernLevels.ConcernLevelId. Column [ConcernLevelId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientProblemConcernLevels'
		AND COLUMN_NAME = 'EnteredDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientProblemConcernLevels.EnteredDateTime. Column [EnteredDateTime] should be datetime, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'TreatmentGoals' exists...
IF OBJECT_ID(N'model.TreatmentGoals', 'U') IS NULL
BEGIN
-- 'TreatmentGoals' does not exist, creating...
	CREATE TABLE [model].[TreatmentGoals] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DisplayName] nvarchar(max)  NOT NULL,
    [Value] nvarchar(max)  NOT NULL
);
END
-- 'TreatmentGoals' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TreatmentGoals', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentGoals'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.TreatmentGoals'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentGoals.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentGoals'
		AND COLUMN_NAME = 'DisplayName'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentGoals.DisplayName. Column [DisplayName] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentGoals'
		AND COLUMN_NAME = 'Value'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentGoals.Value. Column [Value] should be nvarchar(max), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterReviewOfSystems' exists...
IF OBJECT_ID(N'model.EncounterReviewOfSystems', 'U') IS NULL
BEGIN
-- 'EncounterReviewOfSystems' does not exist, creating...
	CREATE TABLE [model].[EncounterReviewOfSystems] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [ReviewOfSystemId] int  NOT NULL
);
END
-- 'EncounterReviewOfSystems' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterReviewOfSystems', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReviewOfSystems'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.EncounterReviewOfSystems'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterReviewOfSystems.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReviewOfSystems'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterReviewOfSystems.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReviewOfSystems'
		AND COLUMN_NAME = 'ReviewOfSystemId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterReviewOfSystems.ReviewOfSystemId. Column [ReviewOfSystemId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ReasonForVisitQuality' exists...
IF OBJECT_ID(N'model.ReasonForVisitQuality', 'U') IS NULL
BEGIN
-- 'ReasonForVisitQuality' does not exist, creating...
	CREATE TABLE [model].[ReasonForVisitQuality] (
    [ReasonForVisits_Id] int  NOT NULL,
    [Qualities_Id] int  NOT NULL
);
END
-- 'ReasonForVisitQuality' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ReasonForVisitQuality', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ReasonForVisitQuality'
		AND COLUMN_NAME = 'ReasonForVisits_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ReasonForVisitQuality.ReasonForVisits_Id. Column [ReasonForVisits_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ReasonForVisitQuality'
		AND COLUMN_NAME = 'Qualities_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ReasonForVisitQuality.Qualities_Id. Column [Qualities_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ReasonForVisitContext' exists...
IF OBJECT_ID(N'model.ReasonForVisitContext', 'U') IS NULL
BEGIN
-- 'ReasonForVisitContext' does not exist, creating...
	CREATE TABLE [model].[ReasonForVisitContext] (
    [ReasonForVisits_Id] int  NOT NULL,
    [Contexts_Id] int  NOT NULL
);
END
-- 'ReasonForVisitContext' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ReasonForVisitContext', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ReasonForVisitContext'
		AND COLUMN_NAME = 'ReasonForVisits_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ReasonForVisitContext.ReasonForVisits_Id. Column [ReasonForVisits_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ReasonForVisitContext'
		AND COLUMN_NAME = 'Contexts_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ReasonForVisitContext.Contexts_Id. Column [Contexts_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterReasonForVisitReasonForVisit' exists...
IF OBJECT_ID(N'model.EncounterReasonForVisitReasonForVisit', 'U') IS NULL
BEGIN
-- 'EncounterReasonForVisitReasonForVisit' does not exist, creating...
	CREATE TABLE [model].[EncounterReasonForVisitReasonForVisit] (
    [EncounterReasonForVisits_Id] int  NOT NULL,
    [ReasonForVisits_Id] int  NOT NULL
);
END
-- 'EncounterReasonForVisitReasonForVisit' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterReasonForVisitReasonForVisit', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReasonForVisitReasonForVisit'
		AND COLUMN_NAME = 'EncounterReasonForVisits_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterReasonForVisitReasonForVisit.EncounterReasonForVisits_Id. Column [EncounterReasonForVisits_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterReasonForVisitReasonForVisit'
		AND COLUMN_NAME = 'ReasonForVisits_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterReasonForVisitReasonForVisit.ReasonForVisits_Id. Column [ReasonForVisits_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ModifyingFactorEncounterReasonForVisit' exists...
IF OBJECT_ID(N'model.ModifyingFactorEncounterReasonForVisit', 'U') IS NULL
BEGIN
-- 'ModifyingFactorEncounterReasonForVisit' does not exist, creating...
	CREATE TABLE [model].[ModifyingFactorEncounterReasonForVisit] (
    [ModifyingFactors_Id] int  NOT NULL,
    [EncounterReasonForVisits_Id] int  NOT NULL
);
END
-- 'ModifyingFactorEncounterReasonForVisit' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ModifyingFactorEncounterReasonForVisit', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ModifyingFactorEncounterReasonForVisit'
		AND COLUMN_NAME = 'ModifyingFactors_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ModifyingFactorEncounterReasonForVisit.ModifyingFactors_Id. Column [ModifyingFactors_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ModifyingFactorEncounterReasonForVisit'
		AND COLUMN_NAME = 'EncounterReasonForVisits_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ModifyingFactorEncounterReasonForVisit.EncounterReasonForVisits_Id. Column [EncounterReasonForVisits_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'AssociatedSignAndSymptomEncounterReasonForVisit' exists...
IF OBJECT_ID(N'model.AssociatedSignAndSymptomEncounterReasonForVisit', 'U') IS NULL
BEGIN
-- 'AssociatedSignAndSymptomEncounterReasonForVisit' does not exist, creating...
	CREATE TABLE [model].[AssociatedSignAndSymptomEncounterReasonForVisit] (
    [AssociatedSignAndSymptom_Id] int  NOT NULL,
    [EncounterReasonForVisits_Id] int  NOT NULL
);
END
-- 'AssociatedSignAndSymptomEncounterReasonForVisit' exists, validating columns.
ELSE IF OBJECT_ID(N'model.AssociatedSignAndSymptomEncounterReasonForVisit', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AssociatedSignAndSymptomEncounterReasonForVisit'
		AND COLUMN_NAME = 'AssociatedSignAndSymptom_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AssociatedSignAndSymptomEncounterReasonForVisit.AssociatedSignAndSymptom_Id. Column [AssociatedSignAndSymptom_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='AssociatedSignAndSymptomEncounterReasonForVisit'
		AND COLUMN_NAME = 'EncounterReasonForVisits_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.AssociatedSignAndSymptomEncounterReasonForVisit.EncounterReasonForVisits_Id. Column [EncounterReasonForVisits_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'DiagnosticTestOrderTreatmentPlan' exists...
IF OBJECT_ID(N'model.DiagnosticTestOrderTreatmentPlan', 'U') IS NULL
BEGIN
-- 'DiagnosticTestOrderTreatmentPlan' does not exist, creating...
	CREATE TABLE [model].[DiagnosticTestOrderTreatmentPlan] (
    [DiagnosticTestOrders_Id] int  NOT NULL,
    [TreatmentPlans_Id] int  NOT NULL
);
END
-- 'DiagnosticTestOrderTreatmentPlan' exists, validating columns.
ELSE IF OBJECT_ID(N'model.DiagnosticTestOrderTreatmentPlan', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DiagnosticTestOrderTreatmentPlan'
		AND COLUMN_NAME = 'DiagnosticTestOrders_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DiagnosticTestOrderTreatmentPlan.DiagnosticTestOrders_Id. Column [DiagnosticTestOrders_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DiagnosticTestOrderTreatmentPlan'
		AND COLUMN_NAME = 'TreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DiagnosticTestOrderTreatmentPlan.TreatmentPlans_Id. Column [TreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'FollowUpVisitTreatmentPlan' exists...
IF OBJECT_ID(N'model.FollowUpVisitTreatmentPlan', 'U') IS NULL
BEGIN
-- 'FollowUpVisitTreatmentPlan' does not exist, creating...
	CREATE TABLE [model].[FollowUpVisitTreatmentPlan] (
    [SubsequentVisitOrders_Id] int  NOT NULL,
    [TreatmentPlans_Id] int  NOT NULL
);
END
-- 'FollowUpVisitTreatmentPlan' exists, validating columns.
ELSE IF OBJECT_ID(N'model.FollowUpVisitTreatmentPlan', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='FollowUpVisitTreatmentPlan'
		AND COLUMN_NAME = 'SubsequentVisitOrders_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.FollowUpVisitTreatmentPlan.SubsequentVisitOrders_Id. Column [SubsequentVisitOrders_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='FollowUpVisitTreatmentPlan'
		AND COLUMN_NAME = 'TreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.FollowUpVisitTreatmentPlan.TreatmentPlans_Id. Column [TreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ProcedureOrderTreatmentPlan' exists...
IF OBJECT_ID(N'model.ProcedureOrderTreatmentPlan', 'U') IS NULL
BEGIN
-- 'ProcedureOrderTreatmentPlan' does not exist, creating...
	CREATE TABLE [model].[ProcedureOrderTreatmentPlan] (
    [ProcedureOrders_Id] int  NOT NULL,
    [TreatmentPlans_Id] int  NOT NULL
);
END
-- 'ProcedureOrderTreatmentPlan' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ProcedureOrderTreatmentPlan', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrderTreatmentPlan'
		AND COLUMN_NAME = 'ProcedureOrders_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedureOrderTreatmentPlan.ProcedureOrders_Id. Column [ProcedureOrders_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrderTreatmentPlan'
		AND COLUMN_NAME = 'TreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedureOrderTreatmentPlan.TreatmentPlans_Id. Column [TreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryTestOrderTreatmentPlan' exists...
IF OBJECT_ID(N'model.LaboratoryTestOrderTreatmentPlan', 'U') IS NULL
BEGIN
-- 'LaboratoryTestOrderTreatmentPlan' does not exist, creating...
	CREATE TABLE [model].[LaboratoryTestOrderTreatmentPlan] (
    [LaboratoryTestOrders_Id] int  NOT NULL,
    [TreatmentPlans_Id] int  NOT NULL
);
END
-- 'LaboratoryTestOrderTreatmentPlan' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryTestOrderTreatmentPlan', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestOrderTreatmentPlan'
		AND COLUMN_NAME = 'LaboratoryTestOrders_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestOrderTreatmentPlan.LaboratoryTestOrders_Id. Column [LaboratoryTestOrders_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestOrderTreatmentPlan'
		AND COLUMN_NAME = 'TreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestOrderTreatmentPlan.TreatmentPlans_Id. Column [TreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'CommunicationWithOtherProviderTreatmentPlan' exists...
IF OBJECT_ID(N'model.CommunicationWithOtherProviderTreatmentPlan', 'U') IS NULL
BEGIN
-- 'CommunicationWithOtherProviderTreatmentPlan' does not exist, creating...
	CREATE TABLE [model].[CommunicationWithOtherProviderTreatmentPlan] (
    [CommunicationWithOtherProviders_Id] int  NOT NULL,
    [TreatmentPlans_Id] int  NOT NULL
);
END
-- 'CommunicationWithOtherProviderTreatmentPlan' exists, validating columns.
ELSE IF OBJECT_ID(N'model.CommunicationWithOtherProviderTreatmentPlan', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationWithOtherProviderTreatmentPlan'
		AND COLUMN_NAME = 'CommunicationWithOtherProviders_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationWithOtherProviderTreatmentPlan.CommunicationWithOtherProviders_Id. Column [CommunicationWithOtherProviders_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='CommunicationWithOtherProviderTreatmentPlan'
		AND COLUMN_NAME = 'TreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.CommunicationWithOtherProviderTreatmentPlan.TreatmentPlans_Id. Column [TreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'TransitionOfCareTreatmentPlan' exists...
IF OBJECT_ID(N'model.TransitionOfCareTreatmentPlan', 'U') IS NULL
BEGIN
-- 'TransitionOfCareTreatmentPlan' does not exist, creating...
	CREATE TABLE [model].[TransitionOfCareTreatmentPlan] (
    [TransitionsOfCare_Id] int  NOT NULL,
    [UserTreatmentPlans_Id] int  NOT NULL
);
END
-- 'TransitionOfCareTreatmentPlan' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TransitionOfCareTreatmentPlan', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TransitionOfCareTreatmentPlan'
		AND COLUMN_NAME = 'TransitionsOfCare_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TransitionOfCareTreatmentPlan.TransitionsOfCare_Id. Column [TransitionsOfCare_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TransitionOfCareTreatmentPlan'
		AND COLUMN_NAME = 'UserTreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TransitionOfCareTreatmentPlan.UserTreatmentPlans_Id. Column [UserTreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'OtherOrderTreatmentPlan' exists...
IF OBJECT_ID(N'model.OtherOrderTreatmentPlan', 'U') IS NULL
BEGIN
-- 'OtherOrderTreatmentPlan' does not exist, creating...
	CREATE TABLE [model].[OtherOrderTreatmentPlan] (
    [OtherTreatmentPlanOrders_Id] int  NOT NULL,
    [TreatmentPlans_Id] int  NOT NULL
);
END
-- 'OtherOrderTreatmentPlan' exists, validating columns.
ELSE IF OBJECT_ID(N'model.OtherOrderTreatmentPlan', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OtherOrderTreatmentPlan'
		AND COLUMN_NAME = 'OtherTreatmentPlanOrders_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OtherOrderTreatmentPlan.OtherTreatmentPlanOrders_Id. Column [OtherTreatmentPlanOrders_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='OtherOrderTreatmentPlan'
		AND COLUMN_NAME = 'TreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.OtherOrderTreatmentPlan.TreatmentPlans_Id. Column [TreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'RefractivePrescriptionTreatmentPlan' exists...
IF OBJECT_ID(N'model.RefractivePrescriptionTreatmentPlan', 'U') IS NULL
BEGIN
-- 'RefractivePrescriptionTreatmentPlan' does not exist, creating...
	CREATE TABLE [model].[RefractivePrescriptionTreatmentPlan] (
    [RefractivePrescriptions_Id] int  NOT NULL,
    [UserTreatmentPlans_Id] int  NOT NULL
);
END
-- 'RefractivePrescriptionTreatmentPlan' exists, validating columns.
ELSE IF OBJECT_ID(N'model.RefractivePrescriptionTreatmentPlan', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RefractivePrescriptionTreatmentPlan'
		AND COLUMN_NAME = 'RefractivePrescriptions_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RefractivePrescriptionTreatmentPlan.RefractivePrescriptions_Id. Column [RefractivePrescriptions_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RefractivePrescriptionTreatmentPlan'
		AND COLUMN_NAME = 'UserTreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RefractivePrescriptionTreatmentPlan.UserTreatmentPlans_Id. Column [UserTreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'MedicationMedicationFamily' exists...
IF OBJECT_ID(N'model.MedicationMedicationFamily', 'U') IS NULL
BEGIN
-- 'MedicationMedicationFamily' does not exist, creating...
	CREATE TABLE [model].[MedicationMedicationFamily] (
    [Medications_Id] int  NOT NULL,
    [MedicationFamilies_Id] int  NOT NULL
);
END
-- 'MedicationMedicationFamily' exists, validating columns.
ELSE IF OBJECT_ID(N'model.MedicationMedicationFamily', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationMedicationFamily'
		AND COLUMN_NAME = 'Medications_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationMedicationFamily.Medications_Id. Column [Medications_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationMedicationFamily'
		AND COLUMN_NAME = 'MedicationFamilies_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationMedicationFamily.MedicationFamilies_Id. Column [MedicationFamilies_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'TreatmentPlanTreatmentGoal' exists...
IF OBJECT_ID(N'model.TreatmentPlanTreatmentGoal', 'U') IS NULL
BEGIN
-- 'TreatmentPlanTreatmentGoal' does not exist, creating...
	CREATE TABLE [model].[TreatmentPlanTreatmentGoal] (
    [UserTreatmentPlans_Id] int  NOT NULL,
    [TreatmentGoalAndInstructions_Id] int  NOT NULL
);
END
-- 'TreatmentPlanTreatmentGoal' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TreatmentPlanTreatmentGoal', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanTreatmentGoal'
		AND COLUMN_NAME = 'UserTreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanTreatmentGoal.UserTreatmentPlans_Id. Column [UserTreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanTreatmentGoal'
		AND COLUMN_NAME = 'TreatmentGoalAndInstructions_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanTreatmentGoal.TreatmentGoalAndInstructions_Id. Column [TreatmentGoalAndInstructions_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ProcedureOrderProcedurePerformed' exists...
IF OBJECT_ID(N'model.ProcedureOrderProcedurePerformed', 'U') IS NULL
BEGIN
-- 'ProcedureOrderProcedurePerformed' does not exist, creating...
	CREATE TABLE [model].[ProcedureOrderProcedurePerformed] (
    [ProcedureOrders_Id] int  NOT NULL,
    [ProcedurePerformeds_Id] int  NOT NULL
);
END
-- 'ProcedureOrderProcedurePerformed' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ProcedureOrderProcedurePerformed', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrderProcedurePerformed'
		AND COLUMN_NAME = 'ProcedureOrders_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedureOrderProcedurePerformed.ProcedureOrders_Id. Column [ProcedureOrders_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedureOrderProcedurePerformed'
		AND COLUMN_NAME = 'ProcedurePerformeds_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedureOrderProcedurePerformed.ProcedurePerformeds_Id. Column [ProcedurePerformeds_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'DiagnosticTestOrderDiagnosticTestPerformed' exists...
IF OBJECT_ID(N'model.DiagnosticTestOrderDiagnosticTestPerformed', 'U') IS NULL
BEGIN
-- 'DiagnosticTestOrderDiagnosticTestPerformed' does not exist, creating...
	CREATE TABLE [model].[DiagnosticTestOrderDiagnosticTestPerformed] (
    [DiagnosticTestOrders_Id] int  NOT NULL,
    [DiagnosticTestPerformeds_Id] int  NOT NULL
);
END
-- 'DiagnosticTestOrderDiagnosticTestPerformed' exists, validating columns.
ELSE IF OBJECT_ID(N'model.DiagnosticTestOrderDiagnosticTestPerformed', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DiagnosticTestOrderDiagnosticTestPerformed'
		AND COLUMN_NAME = 'DiagnosticTestOrders_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DiagnosticTestOrderDiagnosticTestPerformed.DiagnosticTestOrders_Id. Column [DiagnosticTestOrders_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DiagnosticTestOrderDiagnosticTestPerformed'
		AND COLUMN_NAME = 'DiagnosticTestPerformeds_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DiagnosticTestOrderDiagnosticTestPerformed.DiagnosticTestPerformeds_Id. Column [DiagnosticTestPerformeds_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'LaboratoryTestOrderLaboratoryTestResult' exists...
IF OBJECT_ID(N'model.LaboratoryTestOrderLaboratoryTestResult', 'U') IS NULL
BEGIN
-- 'LaboratoryTestOrderLaboratoryTestResult' does not exist, creating...
	CREATE TABLE [model].[LaboratoryTestOrderLaboratoryTestResult] (
    [LaboratoryTestOrders_Id] int  NOT NULL,
    [LaboratoryTestResults_Id] int  NOT NULL
);
END
-- 'LaboratoryTestOrderLaboratoryTestResult' exists, validating columns.
ELSE IF OBJECT_ID(N'model.LaboratoryTestOrderLaboratoryTestResult', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestOrderLaboratoryTestResult'
		AND COLUMN_NAME = 'LaboratoryTestOrders_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestOrderLaboratoryTestResult.LaboratoryTestOrders_Id. Column [LaboratoryTestOrders_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='LaboratoryTestOrderLaboratoryTestResult'
		AND COLUMN_NAME = 'LaboratoryTestResults_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.LaboratoryTestOrderLaboratoryTestResult.LaboratoryTestResults_Id. Column [LaboratoryTestResults_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'MedicationClinicalSpecialtyType' exists...
IF OBJECT_ID(N'model.MedicationClinicalSpecialtyType', 'U') IS NULL
BEGIN
-- 'MedicationClinicalSpecialtyType' does not exist, creating...
	CREATE TABLE [model].[MedicationClinicalSpecialtyType] (
    [Medications_Id] int  NOT NULL,
    [ClinicalSpecialtyTypes_Id] int  NOT NULL
);
END
-- 'MedicationClinicalSpecialtyType' exists, validating columns.
ELSE IF OBJECT_ID(N'model.MedicationClinicalSpecialtyType', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationClinicalSpecialtyType'
		AND COLUMN_NAME = 'Medications_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationClinicalSpecialtyType.Medications_Id. Column [Medications_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='MedicationClinicalSpecialtyType'
		AND COLUMN_NAME = 'ClinicalSpecialtyTypes_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.MedicationClinicalSpecialtyType.ClinicalSpecialtyTypes_Id. Column [ClinicalSpecialtyTypes_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'TreatmentPlanMedicationOrderFavorite' exists...
IF OBJECT_ID(N'model.TreatmentPlanMedicationOrderFavorite', 'U') IS NULL
BEGIN
-- 'TreatmentPlanMedicationOrderFavorite' does not exist, creating...
	CREATE TABLE [model].[TreatmentPlanMedicationOrderFavorite] (
    [TreatmentPlans_Id] int  NOT NULL,
    [MedicationOrderFavorites_Id] int  NOT NULL
);
END
-- 'TreatmentPlanMedicationOrderFavorite' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TreatmentPlanMedicationOrderFavorite', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanMedicationOrderFavorite'
		AND COLUMN_NAME = 'TreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanMedicationOrderFavorite.TreatmentPlans_Id. Column [TreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanMedicationOrderFavorite'
		AND COLUMN_NAME = 'MedicationOrderFavorites_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanMedicationOrderFavorite.MedicationOrderFavorites_Id. Column [MedicationOrderFavorites_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'BodyPartClinicalSpecialtyType' exists...
IF OBJECT_ID(N'model.BodyPartClinicalSpecialtyType', 'U') IS NULL
BEGIN
-- 'BodyPartClinicalSpecialtyType' does not exist, creating...
	CREATE TABLE [model].[BodyPartClinicalSpecialtyType] (
    [BodyParts_Id] int  NOT NULL,
    [ClinicalSpecialtyTypes_Id] int  NOT NULL
);
END
-- 'BodyPartClinicalSpecialtyType' exists, validating columns.
ELSE IF OBJECT_ID(N'model.BodyPartClinicalSpecialtyType', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyPartClinicalSpecialtyType'
		AND COLUMN_NAME = 'BodyParts_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.BodyPartClinicalSpecialtyType.BodyParts_Id. Column [BodyParts_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='BodyPartClinicalSpecialtyType'
		AND COLUMN_NAME = 'ClinicalSpecialtyTypes_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.BodyPartClinicalSpecialtyType.ClinicalSpecialtyTypes_Id. Column [ClinicalSpecialtyTypes_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientAllergenDetailAllergySource' exists...
IF OBJECT_ID(N'model.PatientAllergenDetailAllergySource', 'U') IS NULL
BEGIN
-- 'PatientAllergenDetailAllergySource' does not exist, creating...
	CREATE TABLE [model].[PatientAllergenDetailAllergySource] (
    [PatientAllergenDetails_Id] int  NOT NULL,
    [AllergySources_Id] int  NOT NULL
);
END
-- 'PatientAllergenDetailAllergySource' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientAllergenDetailAllergySource', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenDetailAllergySource'
		AND COLUMN_NAME = 'PatientAllergenDetails_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenDetailAllergySource.PatientAllergenDetails_Id. Column [PatientAllergenDetails_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenDetailAllergySource'
		AND COLUMN_NAME = 'AllergySources_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenDetailAllergySource.AllergySources_Id. Column [AllergySources_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'VaccineAllergenReactionType' exists...
IF OBJECT_ID(N'model.VaccineAllergenReactionType', 'U') IS NULL
BEGIN
-- 'VaccineAllergenReactionType' does not exist, creating...
	CREATE TABLE [model].[VaccineAllergenReactionType] (
    [VaccineAllergenReactionType_AllergenReactionType_Id] int  NOT NULL,
    [VaccineAllergenReactionType_Vaccine_Id] int  NOT NULL
);
END
-- 'VaccineAllergenReactionType' exists, validating columns.
ELSE IF OBJECT_ID(N'model.VaccineAllergenReactionType', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccineAllergenReactionType'
		AND COLUMN_NAME = 'VaccineAllergenReactionType_AllergenReactionType_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccineAllergenReactionType.VaccineAllergenReactionType_AllergenReactionType_Id. Column [VaccineAllergenReactionType_AllergenReactionType_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccineAllergenReactionType'
		AND COLUMN_NAME = 'VaccineAllergenReactionType_Vaccine_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccineAllergenReactionType.VaccineAllergenReactionType_Vaccine_Id. Column [VaccineAllergenReactionType_Vaccine_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'TreatmentPlanClinicalCondition' exists...
IF OBJECT_ID(N'model.TreatmentPlanClinicalCondition', 'U') IS NULL
BEGIN
-- 'TreatmentPlanClinicalCondition' does not exist, creating...
	CREATE TABLE [model].[TreatmentPlanClinicalCondition] (
    [TreatmentPlans_Id] int  NOT NULL,
    [ClinicalConditions_Id] int  NOT NULL
);
END
-- 'TreatmentPlanClinicalCondition' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TreatmentPlanClinicalCondition', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanClinicalCondition'
		AND COLUMN_NAME = 'TreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanClinicalCondition.TreatmentPlans_Id. Column [TreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentPlanClinicalCondition'
		AND COLUMN_NAME = 'ClinicalConditions_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentPlanClinicalCondition.ClinicalConditions_Id. Column [ClinicalConditions_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterLaboratoryTestOrderClinicalCondition' exists...
IF OBJECT_ID(N'model.EncounterLaboratoryTestOrderClinicalCondition', 'U') IS NULL
BEGIN
-- 'EncounterLaboratoryTestOrderClinicalCondition' does not exist, creating...
	CREATE TABLE [model].[EncounterLaboratoryTestOrderClinicalCondition] (
    [EncounterLaboratoryTestOrders_Id] int  NOT NULL,
    [ClinicalConditions_Id] int  NOT NULL
);
END
-- 'EncounterLaboratoryTestOrderClinicalCondition' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterLaboratoryTestOrderClinicalCondition', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterLaboratoryTestOrderClinicalCondition'
		AND COLUMN_NAME = 'EncounterLaboratoryTestOrders_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterLaboratoryTestOrderClinicalCondition.EncounterLaboratoryTestOrders_Id. Column [EncounterLaboratoryTestOrders_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterLaboratoryTestOrderClinicalCondition'
		AND COLUMN_NAME = 'ClinicalConditions_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterLaboratoryTestOrderClinicalCondition.ClinicalConditions_Id. Column [ClinicalConditions_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'EncounterPatientEducationClinicalCondition' exists...
IF OBJECT_ID(N'model.EncounterPatientEducationClinicalCondition', 'U') IS NULL
BEGIN
-- 'EncounterPatientEducationClinicalCondition' does not exist, creating...
	CREATE TABLE [model].[EncounterPatientEducationClinicalCondition] (
    [EncounterPatientEducations_Id] int  NOT NULL,
    [ClinicalConditions_Id] int  NOT NULL
);
END
-- 'EncounterPatientEducationClinicalCondition' exists, validating columns.
ELSE IF OBJECT_ID(N'model.EncounterPatientEducationClinicalCondition', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterPatientEducationClinicalCondition'
		AND COLUMN_NAME = 'EncounterPatientEducations_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterPatientEducationClinicalCondition.EncounterPatientEducations_Id. Column [EncounterPatientEducations_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='EncounterPatientEducationClinicalCondition'
		AND COLUMN_NAME = 'ClinicalConditions_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.EncounterPatientEducationClinicalCondition.ClinicalConditions_Id. Column [ClinicalConditions_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientEducationClinicalCondition' exists...
IF OBJECT_ID(N'model.PatientEducationClinicalCondition', 'U') IS NULL
BEGIN
-- 'PatientEducationClinicalCondition' does not exist, creating...
	CREATE TABLE [model].[PatientEducationClinicalCondition] (
    [PatientEducations_Id] int  NOT NULL,
    [ClinicalConditions_Id] int  NOT NULL
);
END
-- 'PatientEducationClinicalCondition' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientEducationClinicalCondition', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientEducationClinicalCondition'
		AND COLUMN_NAME = 'PatientEducations_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientEducationClinicalCondition.PatientEducations_Id. Column [PatientEducations_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientEducationClinicalCondition'
		AND COLUMN_NAME = 'ClinicalConditions_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientEducationClinicalCondition.ClinicalConditions_Id. Column [ClinicalConditions_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ProcedurePerformedClinicalCondition' exists...
IF OBJECT_ID(N'model.ProcedurePerformedClinicalCondition', 'U') IS NULL
BEGIN
-- 'ProcedurePerformedClinicalCondition' does not exist, creating...
	CREATE TABLE [model].[ProcedurePerformedClinicalCondition] (
    [ProcedurePerformeds_Id] int  NOT NULL,
    [ClinicalConditions_Id] int  NOT NULL
);
END
-- 'ProcedurePerformedClinicalCondition' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ProcedurePerformedClinicalCondition', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedurePerformedClinicalCondition'
		AND COLUMN_NAME = 'ProcedurePerformeds_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedurePerformedClinicalCondition.ProcedurePerformeds_Id. Column [ProcedurePerformeds_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ProcedurePerformedClinicalCondition'
		AND COLUMN_NAME = 'ClinicalConditions_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ProcedurePerformedClinicalCondition.ClinicalConditions_Id. Column [ClinicalConditions_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'DiagnosticTestPerformedClinicalCondition' exists...
IF OBJECT_ID(N'model.DiagnosticTestPerformedClinicalCondition', 'U') IS NULL
BEGIN
-- 'DiagnosticTestPerformedClinicalCondition' does not exist, creating...
	CREATE TABLE [model].[DiagnosticTestPerformedClinicalCondition] (
    [DiagnosticTestPerformeds_Id] int  NOT NULL,
    [ClinicalConditions_Id] int  NOT NULL
);
END
-- 'DiagnosticTestPerformedClinicalCondition' exists, validating columns.
ELSE IF OBJECT_ID(N'model.DiagnosticTestPerformedClinicalCondition', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DiagnosticTestPerformedClinicalCondition'
		AND COLUMN_NAME = 'DiagnosticTestPerformeds_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DiagnosticTestPerformedClinicalCondition.DiagnosticTestPerformeds_Id. Column [DiagnosticTestPerformeds_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='DiagnosticTestPerformedClinicalCondition'
		AND COLUMN_NAME = 'ClinicalConditions_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.DiagnosticTestPerformedClinicalCondition.ClinicalConditions_Id. Column [ClinicalConditions_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'RouteOfAdministrationClinicalSpecialtyType' exists...
IF OBJECT_ID(N'model.RouteOfAdministrationClinicalSpecialtyType', 'U') IS NULL
BEGIN
-- 'RouteOfAdministrationClinicalSpecialtyType' does not exist, creating...
	CREATE TABLE [model].[RouteOfAdministrationClinicalSpecialtyType] (
    [RouteOfAdministrations_Id] int  NOT NULL,
    [ClinicalSpecialtyTypes_Id] int  NOT NULL
);
END
-- 'RouteOfAdministrationClinicalSpecialtyType' exists, validating columns.
ELSE IF OBJECT_ID(N'model.RouteOfAdministrationClinicalSpecialtyType', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RouteOfAdministrationClinicalSpecialtyType'
		AND COLUMN_NAME = 'RouteOfAdministrations_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RouteOfAdministrationClinicalSpecialtyType.RouteOfAdministrations_Id. Column [RouteOfAdministrations_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='RouteOfAdministrationClinicalSpecialtyType'
		AND COLUMN_NAME = 'ClinicalSpecialtyTypes_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.RouteOfAdministrationClinicalSpecialtyType.ClinicalSpecialtyTypes_Id. Column [ClinicalSpecialtyTypes_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'VaccinationAdministeredUnitOfMeasurement' exists...
IF OBJECT_ID(N'model.VaccinationAdministeredUnitOfMeasurement', 'U') IS NULL
BEGIN
-- 'VaccinationAdministeredUnitOfMeasurement' does not exist, creating...
	CREATE TABLE [model].[VaccinationAdministeredUnitOfMeasurement] (
    [VaccinationAdministereds_Id] int  NOT NULL,
    [UnitsOfMeasurement_Id] int  NOT NULL
);
END
-- 'VaccinationAdministeredUnitOfMeasurement' exists, validating columns.
ELSE IF OBJECT_ID(N'model.VaccinationAdministeredUnitOfMeasurement', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccinationAdministeredUnitOfMeasurement'
		AND COLUMN_NAME = 'VaccinationAdministereds_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccinationAdministeredUnitOfMeasurement.VaccinationAdministereds_Id. Column [VaccinationAdministereds_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='VaccinationAdministeredUnitOfMeasurement'
		AND COLUMN_NAME = 'UnitsOfMeasurement_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.VaccinationAdministeredUnitOfMeasurement.UnitsOfMeasurement_Id. Column [UnitsOfMeasurement_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'TreatmentGoalClinicalCondition' exists...
IF OBJECT_ID(N'model.TreatmentGoalClinicalCondition', 'U') IS NULL
BEGIN
-- 'TreatmentGoalClinicalCondition' does not exist, creating...
	CREATE TABLE [model].[TreatmentGoalClinicalCondition] (
    [TreatmentGoalAndInstructions_Id] int  NOT NULL,
    [ClinicalConditions_Id] int  NOT NULL
);
END
-- 'TreatmentGoalClinicalCondition' exists, validating columns.
ELSE IF OBJECT_ID(N'model.TreatmentGoalClinicalCondition', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentGoalClinicalCondition'
		AND COLUMN_NAME = 'TreatmentGoalAndInstructions_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentGoalClinicalCondition.TreatmentGoalAndInstructions_Id. Column [TreatmentGoalAndInstructions_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='TreatmentGoalClinicalCondition'
		AND COLUMN_NAME = 'ClinicalConditions_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.TreatmentGoalClinicalCondition.ClinicalConditions_Id. Column [ClinicalConditions_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'ScreenQuestion' exists...
IF OBJECT_ID(N'model.ScreenQuestion', 'U') IS NULL
BEGIN
-- 'ScreenQuestion' does not exist, creating...
	CREATE TABLE [model].[ScreenQuestion] (
    [Screens_Id] int  NOT NULL,
    [Questions_Id] int  NOT NULL
);
END
-- 'ScreenQuestion' exists, validating columns.
ELSE IF OBJECT_ID(N'model.ScreenQuestion', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ScreenQuestion'
		AND COLUMN_NAME = 'Screens_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ScreenQuestion.Screens_Id. Column [Screens_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='ScreenQuestion'
		AND COLUMN_NAME = 'Questions_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.ScreenQuestion.Questions_Id. Column [Questions_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientLaboratoryTestResultCopiesSentExternalProvider' exists...
IF OBJECT_ID(N'model.PatientLaboratoryTestResultCopiesSentExternalProvider', 'U') IS NULL
BEGIN
-- 'PatientLaboratoryTestResultCopiesSentExternalProvider' does not exist, creating...
	CREATE TABLE [model].[PatientLaboratoryTestResultCopiesSentExternalProvider] (
    [CopiesSentPatientLaboratoryTestResults_Id] int  NOT NULL,
    [CopiesSentExternalProviders_Id] int  NOT NULL
);
END
-- 'PatientLaboratoryTestResultCopiesSentExternalProvider' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientLaboratoryTestResultCopiesSentExternalProvider', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientLaboratoryTestResultCopiesSentExternalProvider'
		AND COLUMN_NAME = 'CopiesSentPatientLaboratoryTestResults_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientLaboratoryTestResultCopiesSentExternalProvider.CopiesSentPatientLaboratoryTestResults_Id. Column [CopiesSentPatientLaboratoryTestResults_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientLaboratoryTestResultCopiesSentExternalProvider'
		AND COLUMN_NAME = 'CopiesSentExternalProviders_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientLaboratoryTestResultCopiesSentExternalProvider.CopiesSentExternalProviders_Id. Column [CopiesSentExternalProviders_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientFamilyHistoryEntryDetailClinicalQualifier' exists...
IF OBJECT_ID(N'model.PatientFamilyHistoryEntryDetailClinicalQualifier', 'U') IS NULL
BEGIN
-- 'PatientFamilyHistoryEntryDetailClinicalQualifier' does not exist, creating...
	CREATE TABLE [model].[PatientFamilyHistoryEntryDetailClinicalQualifier] (
    [PatientFamilyHistoryEntryDetails_Id] int  NOT NULL,
    [ClinicalQualifiers_Id] int  NOT NULL
);
END
-- 'PatientFamilyHistoryEntryDetailClinicalQualifier' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientFamilyHistoryEntryDetailClinicalQualifier', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntryDetailClinicalQualifier'
		AND COLUMN_NAME = 'PatientFamilyHistoryEntryDetails_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntryDetailClinicalQualifier.PatientFamilyHistoryEntryDetails_Id. Column [PatientFamilyHistoryEntryDetails_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFamilyHistoryEntryDetailClinicalQualifier'
		AND COLUMN_NAME = 'ClinicalQualifiers_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientFamilyHistoryEntryDetailClinicalQualifier.ClinicalQualifiers_Id. Column [ClinicalQualifiers_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientAllergenDetailClinicalQualifier' exists...
IF OBJECT_ID(N'model.PatientAllergenDetailClinicalQualifier', 'U') IS NULL
BEGIN
-- 'PatientAllergenDetailClinicalQualifier' does not exist, creating...
	CREATE TABLE [model].[PatientAllergenDetailClinicalQualifier] (
    [PatientAllergenDetails_Id] int  NOT NULL,
    [ClinicalQualifiers_Id] int  NOT NULL
);
END
-- 'PatientAllergenDetailClinicalQualifier' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientAllergenDetailClinicalQualifier', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenDetailClinicalQualifier'
		AND COLUMN_NAME = 'PatientAllergenDetails_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenDetailClinicalQualifier.PatientAllergenDetails_Id. Column [PatientAllergenDetails_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientAllergenDetailClinicalQualifier'
		AND COLUMN_NAME = 'ClinicalQualifiers_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientAllergenDetailClinicalQualifier.ClinicalQualifiers_Id. Column [ClinicalQualifiers_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientBloodPressureClinicalQualifier' exists...
IF OBJECT_ID(N'model.PatientBloodPressureClinicalQualifier', 'U') IS NULL
BEGIN
-- 'PatientBloodPressureClinicalQualifier' does not exist, creating...
	CREATE TABLE [model].[PatientBloodPressureClinicalQualifier] (
    [PatientBloodPressures_Id] int  NOT NULL,
    [ClinicalQualifiers_Id] int  NOT NULL
);
END
-- 'PatientBloodPressureClinicalQualifier' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientBloodPressureClinicalQualifier', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientBloodPressureClinicalQualifier'
		AND COLUMN_NAME = 'PatientBloodPressures_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientBloodPressureClinicalQualifier.PatientBloodPressures_Id. Column [PatientBloodPressures_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientBloodPressureClinicalQualifier'
		AND COLUMN_NAME = 'ClinicalQualifiers_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientBloodPressureClinicalQualifier.ClinicalQualifiers_Id. Column [ClinicalQualifiers_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientHeightAndWeightClinicalQualifier' exists...
IF OBJECT_ID(N'model.PatientHeightAndWeightClinicalQualifier', 'U') IS NULL
BEGIN
-- 'PatientHeightAndWeightClinicalQualifier' does not exist, creating...
	CREATE TABLE [model].[PatientHeightAndWeightClinicalQualifier] (
    [PatientHeightAndWeights_Id] int  NOT NULL,
    [ClinicalQualifiers_Id] int  NOT NULL
);
END
-- 'PatientHeightAndWeightClinicalQualifier' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientHeightAndWeightClinicalQualifier', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientHeightAndWeightClinicalQualifier'
		AND COLUMN_NAME = 'PatientHeightAndWeights_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientHeightAndWeightClinicalQualifier.PatientHeightAndWeights_Id. Column [PatientHeightAndWeights_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientHeightAndWeightClinicalQualifier'
		AND COLUMN_NAME = 'ClinicalQualifiers_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientHeightAndWeightClinicalQualifier.ClinicalQualifiers_Id. Column [ClinicalQualifiers_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientCognitiveStatusAssessmentClinicalQualifier' exists...
IF OBJECT_ID(N'model.PatientCognitiveStatusAssessmentClinicalQualifier', 'U') IS NULL
BEGIN
-- 'PatientCognitiveStatusAssessmentClinicalQualifier' does not exist, creating...
	CREATE TABLE [model].[PatientCognitiveStatusAssessmentClinicalQualifier] (
    [PatientCognitiveStatusAssessments_Id] int  NOT NULL,
    [ClinicalQualifiers_Id] int  NOT NULL
);
END
-- 'PatientCognitiveStatusAssessmentClinicalQualifier' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientCognitiveStatusAssessmentClinicalQualifier', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientCognitiveStatusAssessmentClinicalQualifier'
		AND COLUMN_NAME = 'PatientCognitiveStatusAssessments_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientCognitiveStatusAssessmentClinicalQualifier.PatientCognitiveStatusAssessments_Id. Column [PatientCognitiveStatusAssessments_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientCognitiveStatusAssessmentClinicalQualifier'
		AND COLUMN_NAME = 'ClinicalQualifiers_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientCognitiveStatusAssessmentClinicalQualifier.ClinicalQualifiers_Id. Column [ClinicalQualifiers_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientFunctionalStatusAssessmentClinicalQualifier' exists...
IF OBJECT_ID(N'model.PatientFunctionalStatusAssessmentClinicalQualifier', 'U') IS NULL
BEGIN
-- 'PatientFunctionalStatusAssessmentClinicalQualifier' does not exist, creating...
	CREATE TABLE [model].[PatientFunctionalStatusAssessmentClinicalQualifier] (
    [PatientFunctionalStatusAssessments_Id] int  NOT NULL,
    [ClinicalQualifiers_Id] int  NOT NULL
);
END
-- 'PatientFunctionalStatusAssessmentClinicalQualifier' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientFunctionalStatusAssessmentClinicalQualifier', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFunctionalStatusAssessmentClinicalQualifier'
		AND COLUMN_NAME = 'PatientFunctionalStatusAssessments_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientFunctionalStatusAssessmentClinicalQualifier.PatientFunctionalStatusAssessments_Id. Column [PatientFunctionalStatusAssessments_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientFunctionalStatusAssessmentClinicalQualifier'
		AND COLUMN_NAME = 'ClinicalQualifiers_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientFunctionalStatusAssessmentClinicalQualifier.ClinicalQualifiers_Id. Column [ClinicalQualifiers_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'UserTreatmentPlanPatientEducation' exists...
IF OBJECT_ID(N'model.UserTreatmentPlanPatientEducation', 'U') IS NULL
BEGIN
-- 'UserTreatmentPlanPatientEducation' does not exist, creating...
	CREATE TABLE [model].[UserTreatmentPlanPatientEducation] (
    [UserTreatmentPlans_Id] int  NOT NULL,
    [PatientEducations_Id] int  NOT NULL
);
END
-- 'UserTreatmentPlanPatientEducation' exists, validating columns.
ELSE IF OBJECT_ID(N'model.UserTreatmentPlanPatientEducation', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UserTreatmentPlanPatientEducation'
		AND COLUMN_NAME = 'UserTreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UserTreatmentPlanPatientEducation.UserTreatmentPlans_Id. Column [UserTreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UserTreatmentPlanPatientEducation'
		AND COLUMN_NAME = 'PatientEducations_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UserTreatmentPlanPatientEducation.PatientEducations_Id. Column [PatientEducations_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'UserTreatmentPlanVaccinationOrder' exists...
IF OBJECT_ID(N'model.UserTreatmentPlanVaccinationOrder', 'U') IS NULL
BEGIN
-- 'UserTreatmentPlanVaccinationOrder' does not exist, creating...
	CREATE TABLE [model].[UserTreatmentPlanVaccinationOrder] (
    [UserTreatmentPlans_Id] int  NOT NULL,
    [VaccinationOrders_Id] int  NOT NULL
);
END
-- 'UserTreatmentPlanVaccinationOrder' exists, validating columns.
ELSE IF OBJECT_ID(N'model.UserTreatmentPlanVaccinationOrder', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UserTreatmentPlanVaccinationOrder'
		AND COLUMN_NAME = 'UserTreatmentPlans_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UserTreatmentPlanVaccinationOrder.UserTreatmentPlans_Id. Column [UserTreatmentPlans_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='UserTreatmentPlanVaccinationOrder'
		AND COLUMN_NAME = 'VaccinationOrders_Id'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.UserTreatmentPlanVaccinationOrder.VaccinationOrders_Id. Column [VaccinationOrders_Id] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'HistoryOfPresentIllnesses'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.HistoryOfPresentIllnesses', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[HistoryOfPresentIllnesses] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.HistoryOfPresentIllnesses', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[HistoryOfPresentIllnesses]
ADD CONSTRAINT [PK_HistoryOfPresentIllnesses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ReviewOfSystems'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ReviewOfSystems', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ReviewOfSystems] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ReviewOfSystems', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ReviewOfSystems]
ADD CONSTRAINT [PK_ReviewOfSystems]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientDiagnoses'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientDiagnoses', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientDiagnoses] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientDiagnoses', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientDiagnoses]
ADD CONSTRAINT [PK_PatientDiagnoses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MedicationComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.MedicationComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[MedicationComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.MedicationComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[MedicationComments]
ADD CONSTRAINT [PK_MedicationComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProcedureOrders'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ProcedureOrders', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ProcedureOrders] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ProcedureOrders', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ProcedureOrders]
ADD CONSTRAINT [PK_ProcedureOrders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SubsequentVisitOrders'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.SubsequentVisitOrders', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[SubsequentVisitOrders] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.SubsequentVisitOrders', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[SubsequentVisitOrders]
ADD CONSTRAINT [PK_SubsequentVisitOrders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CommunicationWithOtherProviderOrders'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.CommunicationWithOtherProviderOrders', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[CommunicationWithOtherProviderOrders] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.CommunicationWithOtherProviderOrders', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[CommunicationWithOtherProviderOrders]
ADD CONSTRAINT [PK_CommunicationWithOtherProviderOrders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TransitionOfCareOrders'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TransitionOfCareOrders', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TransitionOfCareOrders] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TransitionOfCareOrders', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TransitionOfCareOrders]
ADD CONSTRAINT [PK_TransitionOfCareOrders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Qualities'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.Qualities', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[Qualities] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.Qualities', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[Qualities]
ADD CONSTRAINT [PK_Qualities]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Timings'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.Timings', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[Timings] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.Timings', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[Timings]
ADD CONSTRAINT [PK_Timings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Contexts'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.Contexts', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[Contexts] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.Contexts', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[Contexts]
ADD CONSTRAINT [PK_Contexts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ModifyingFactors'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ModifyingFactors', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ModifyingFactors] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ModifyingFactors', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ModifyingFactors]
ADD CONSTRAINT [PK_ModifyingFactors]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AssociatedSignAndSymptom'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.AssociatedSignAndSymptom', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[AssociatedSignAndSymptom] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.AssociatedSignAndSymptom', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[AssociatedSignAndSymptom]
ADD CONSTRAINT [PK_AssociatedSignAndSymptom]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterHistoryOfPresentIllnesses'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterHistoryOfPresentIllnesses', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterHistoryOfPresentIllnesses] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterHistoryOfPresentIllnesses', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterHistoryOfPresentIllnesses]
ADD CONSTRAINT [PK_EncounterHistoryOfPresentIllnesses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HistoryOfPresentIllnessCategories'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.HistoryOfPresentIllnessCategories', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[HistoryOfPresentIllnessCategories] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.HistoryOfPresentIllnessCategories', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[HistoryOfPresentIllnessCategories]
ADD CONSTRAINT [PK_HistoryOfPresentIllnessCategories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UserTreatmentPlans'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.UserTreatmentPlans', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[UserTreatmentPlans] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.UserTreatmentPlans', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[UserTreatmentPlans]
ADD CONSTRAINT [PK_UserTreatmentPlans]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TreatmentGoalAndInstructions'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TreatmentGoalAndInstructions', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TreatmentGoalAndInstructions] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TreatmentGoalAndInstructions', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TreatmentGoalAndInstructions]
ADD CONSTRAINT [PK_TreatmentGoalAndInstructions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OtherTreatmentPlanOrders'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.OtherTreatmentPlanOrders', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[OtherTreatmentPlanOrders] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.OtherTreatmentPlanOrders', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[OtherTreatmentPlanOrders]
ADD CONSTRAINT [PK_OtherTreatmentPlanOrders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RefractivePrescriptions'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.RefractivePrescriptions', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[RefractivePrescriptions] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.RefractivePrescriptions', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[RefractivePrescriptions]
ADD CONSTRAINT [PK_RefractivePrescriptions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TreatmentPlanOrderConfigurations'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TreatmentPlanOrderConfigurations', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TreatmentPlanOrderConfigurations] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TreatmentPlanOrderConfigurations', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TreatmentPlanOrderConfigurations]
ADD CONSTRAINT [PK_TreatmentPlanOrderConfigurations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterCommunicationWithOtherProviderOrders'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterCommunicationWithOtherProviderOrders', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterCommunicationWithOtherProviderOrders', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders]
ADD CONSTRAINT [PK_EncounterCommunicationWithOtherProviderOrders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterLaboratoryTestOrders'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterLaboratoryTestOrders', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterLaboratoryTestOrders', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterLaboratoryTestOrders]
ADD CONSTRAINT [PK_EncounterLaboratoryTestOrders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterMedicationOrders'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterMedicationOrders', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterMedicationOrders] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterMedicationOrders', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterMedicationOrders]
ADD CONSTRAINT [PK_EncounterMedicationOrders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterProcedureOrders'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterProcedureOrders', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterProcedureOrders', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterProcedureOrders]
ADD CONSTRAINT [PK_EncounterProcedureOrders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MedicationFamilies'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.MedicationFamilies', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[MedicationFamilies] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.MedicationFamilies', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[MedicationFamilies]
ADD CONSTRAINT [PK_MedicationFamilies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MedicationProgressComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.MedicationProgressComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[MedicationProgressComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.MedicationProgressComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[MedicationProgressComments]
ADD CONSTRAINT [PK_MedicationProgressComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TreatmentPlanOrderInstructions'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TreatmentPlanOrderInstructions', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TreatmentPlanOrderInstructions] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TreatmentPlanOrderInstructions', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TreatmentPlanOrderInstructions]
ADD CONSTRAINT [PK_TreatmentPlanOrderInstructions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TimeQualifiers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TimeQualifiers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TimeQualifiers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TimeQualifiers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TimeQualifiers]
ADD CONSTRAINT [PK_TimeQualifiers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DiagnosticTestPerformeds'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.DiagnosticTestPerformeds', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[DiagnosticTestPerformeds] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.DiagnosticTestPerformeds', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[DiagnosticTestPerformeds]
ADD CONSTRAINT [PK_DiagnosticTestPerformeds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProcedurePerformeds'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ProcedurePerformeds', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ProcedurePerformeds] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ProcedurePerformeds', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ProcedurePerformeds]
ADD CONSTRAINT [PK_ProcedurePerformeds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SpecialExamPerformeds'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.SpecialExamPerformeds', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[SpecialExamPerformeds] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.SpecialExamPerformeds', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[SpecialExamPerformeds]
ADD CONSTRAINT [PK_SpecialExamPerformeds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OtherElementPerformeds'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.OtherElementPerformeds', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[OtherElementPerformeds] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.OtherElementPerformeds', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[OtherElementPerformeds]
ADD CONSTRAINT [PK_OtherElementPerformeds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryTestResults'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestResults', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryTestResults] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestResults', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryTestResults]
ADD CONSTRAINT [PK_LaboratoryTestResults]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientSpecialExamPerformeds'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientSpecialExamPerformeds', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientSpecialExamPerformeds] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientSpecialExamPerformeds', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientSpecialExamPerformeds]
ADD CONSTRAINT [PK_PatientSpecialExamPerformeds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientOtherElementPerformeds'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientOtherElementPerformeds', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientOtherElementPerformeds] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientOtherElementPerformeds', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientOtherElementPerformeds]
ADD CONSTRAINT [PK_PatientOtherElementPerformeds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterTreatmentPlanComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterTreatmentPlanComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterTreatmentPlanComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterTreatmentPlanComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterTreatmentPlanComments]
ADD CONSTRAINT [PK_EncounterTreatmentPlanComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TreatmentPlanComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TreatmentPlanComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TreatmentPlanComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TreatmentPlanComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TreatmentPlanComments]
ADD CONSTRAINT [PK_TreatmentPlanComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterRefractivePrescriptions'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterRefractivePrescriptions', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterRefractivePrescriptions] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterRefractivePrescriptions', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterRefractivePrescriptions]
ADD CONSTRAINT [PK_EncounterRefractivePrescriptions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterOtherTreatmentPlanOrders'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterOtherTreatmentPlanOrders', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterOtherTreatmentPlanOrders] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterOtherTreatmentPlanOrders', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterOtherTreatmentPlanOrders]
ADD CONSTRAINT [PK_EncounterOtherTreatmentPlanOrders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MedicationOrderFavorites'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.MedicationOrderFavorites', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.MedicationOrderFavorites', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[MedicationOrderFavorites]
ADD CONSTRAINT [PK_MedicationOrderFavorites]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DrugDispenseForms'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.DrugDispenseForms', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[DrugDispenseForms] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.DrugDispenseForms', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[DrugDispenseForms]
ADD CONSTRAINT [PK_DrugDispenseForms]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Frequencies'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.Frequencies', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[Frequencies] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.Frequencies', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[Frequencies]
ADD CONSTRAINT [PK_Frequencies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClinicalDataSourceTypes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ClinicalDataSourceTypes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ClinicalDataSourceTypes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ClinicalDataSourceTypes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ClinicalDataSourceTypes]
ADD CONSTRAINT [PK_ClinicalDataSourceTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Organs'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.Organs', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[Organs] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.Organs', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[Organs]
ADD CONSTRAINT [PK_Organs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BodyParts'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.BodyParts', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[BodyParts] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.BodyParts', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[BodyParts]
ADD CONSTRAINT [PK_BodyParts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganStructures'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.OrganStructures', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[OrganStructures] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.OrganStructures', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[OrganStructures]
ADD CONSTRAINT [PK_OrganStructures]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganStructureGroups'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.OrganStructureGroups', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[OrganStructureGroups] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.OrganStructureGroups', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[OrganStructureGroups]
ADD CONSTRAINT [PK_OrganStructureGroups]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BodyLocations'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.BodyLocations', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[BodyLocations] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.BodyLocations', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[BodyLocations]
ADD CONSTRAINT [PK_BodyLocations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DrugDosageActions'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.DrugDosageActions', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[DrugDosageActions] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.DrugDosageActions', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[DrugDosageActions]
ADD CONSTRAINT [PK_DrugDosageActions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DrugDosageNumbers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.DrugDosageNumbers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[DrugDosageNumbers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.DrugDosageNumbers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[DrugDosageNumbers]
ADD CONSTRAINT [PK_DrugDosageNumbers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientMedicationProgressComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientMedicationProgressComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientMedicationProgressComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientMedicationProgressComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientMedicationProgressComments]
ADD CONSTRAINT [PK_PatientMedicationProgressComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClinicalQualifierCategories'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ClinicalQualifierCategories', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ClinicalQualifierCategories] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ClinicalQualifierCategories', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ClinicalQualifierCategories]
ADD CONSTRAINT [PK_ClinicalQualifierCategories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterClinicalDrawings'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterClinicalDrawings', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterClinicalDrawings] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterClinicalDrawings', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterClinicalDrawings]
ADD CONSTRAINT [PK_EncounterClinicalDrawings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClinicalDrawingTemplates'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ClinicalDrawingTemplates', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ClinicalDrawingTemplates] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ClinicalDrawingTemplates', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ClinicalDrawingTemplates]
ADD CONSTRAINT [PK_ClinicalDrawingTemplates]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RelativeTimeTypes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.RelativeTimeTypes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[RelativeTimeTypes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.RelativeTimeTypes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[RelativeTimeTypes]
ADD CONSTRAINT [PK_RelativeTimeTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClinicalHierarchyClinicalAttribute'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ClinicalHierarchyClinicalAttribute', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ClinicalHierarchyClinicalAttribute] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ClinicalHierarchyClinicalAttribute', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ClinicalHierarchyClinicalAttribute]
ADD CONSTRAINT [PK_ClinicalHierarchyClinicalAttribute]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganSubStructures'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.OrganSubStructures', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[OrganSubStructures] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.OrganSubStructures', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[OrganSubStructures]
ADD CONSTRAINT [PK_OrganSubStructures]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AllergenCategorys'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.AllergenCategorys', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[AllergenCategorys] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.AllergenCategorys', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[AllergenCategorys]
ADD CONSTRAINT [PK_AllergenCategorys]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AllergenOccurrences'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.AllergenOccurrences', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[AllergenOccurrences] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.AllergenOccurrences', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[AllergenOccurrences]
ADD CONSTRAINT [PK_AllergenOccurrences]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientDiagnosisDetails'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientDiagnosisDetails', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientDiagnosisDetails', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [PK_PatientDiagnosisDetails]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientDiagnosisDetailQualifiers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientDiagnosisDetailQualifiers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientDiagnosisDetailQualifiers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientDiagnosisDetailQualifiers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientDiagnosisDetailQualifiers]
ADD CONSTRAINT [PK_PatientDiagnosisDetailQualifiers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientDiagnosisComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientDiagnosisComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientDiagnosisComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientDiagnosisComments]
ADD CONSTRAINT [PK_PatientDiagnosisComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientAllergenComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientAllergenComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientAllergenComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientAllergenComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientAllergenComments]
ADD CONSTRAINT [PK_PatientAllergenComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClinicalHistoryReviewTypes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ClinicalHistoryReviewTypes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ClinicalHistoryReviewTypes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ClinicalHistoryReviewTypes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ClinicalHistoryReviewTypes]
ADD CONSTRAINT [PK_ClinicalHistoryReviewTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientMedicalReviews'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientMedicalReviews', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientMedicalReviews] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientMedicalReviews', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientMedicalReviews]
ADD CONSTRAINT [PK_PatientMedicalReviews]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClinicalHistoryReviewAreas'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ClinicalHistoryReviewAreas', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ClinicalHistoryReviewAreas] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ClinicalHistoryReviewAreas', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ClinicalHistoryReviewAreas]
ADD CONSTRAINT [PK_ClinicalHistoryReviewAreas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryComponents'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryComponents', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryComponents] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryComponents', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryComponents]
ADD CONSTRAINT [PK_LaboratoryComponents]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryMeasuredProperties'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryMeasuredProperties', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryMeasuredProperties] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryMeasuredProperties', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryMeasuredProperties]
ADD CONSTRAINT [PK_LaboratoryMeasuredProperties]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryTimings'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryTimings', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryTimings] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryTimings', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryTimings]
ADD CONSTRAINT [PK_LaboratoryTimings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SpecimenSources'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.SpecimenSources', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[SpecimenSources] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.SpecimenSources', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[SpecimenSources]
ADD CONSTRAINT [PK_SpecimenSources]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryScaleTypes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryScaleTypes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryScaleTypes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryScaleTypes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryScaleTypes]
ADD CONSTRAINT [PK_LaboratoryScaleTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryMethodUseds'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryMethodUseds', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryMethodUseds] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryMethodUseds', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryMethodUseds]
ADD CONSTRAINT [PK_LaboratoryMethodUseds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryNormalRanges'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryNormalRanges', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryNormalRanges] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryNormalRanges', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryNormalRanges]
ADD CONSTRAINT [PK_LaboratoryNormalRanges]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryTestingInstruments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestingInstruments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryTestingInstruments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestingInstruments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryTestingInstruments]
ADD CONSTRAINT [PK_LaboratoryTestingInstruments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryCollectionSites'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryCollectionSites', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryCollectionSites] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryCollectionSites', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryCollectionSites]
ADD CONSTRAINT [PK_LaboratoryCollectionSites]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryTestingPriorities'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestingPriorities', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryTestingPriorities] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestingPriorities', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryTestingPriorities]
ADD CONSTRAINT [PK_LaboratoryTestingPriorities]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryResultVerifications'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryResultVerifications', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryResultVerifications] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryResultVerifications', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryResultVerifications]
ADD CONSTRAINT [PK_LaboratoryResultVerifications]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratorySampleSizes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratorySampleSizes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratorySampleSizes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratorySampleSizes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratorySampleSizes]
ADD CONSTRAINT [PK_LaboratorySampleSizes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryTestingPlaces'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestingPlaces', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryTestingPlaces] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestingPlaces', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryTestingPlaces]
ADD CONSTRAINT [PK_LaboratoryTestingPlaces]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VaccineManufacturers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.VaccineManufacturers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[VaccineManufacturers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.VaccineManufacturers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[VaccineManufacturers]
ADD CONSTRAINT [PK_VaccineManufacturers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClinicalAttributes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ClinicalAttributes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ClinicalAttributes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ClinicalAttributes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ClinicalAttributes]
ADD CONSTRAINT [PK_ClinicalAttributes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ScreenTypes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ScreenTypes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ScreenTypes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ScreenTypes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ScreenTypes]
ADD CONSTRAINT [PK_ScreenTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryTests'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryTests', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryTests] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryTests', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryTests]
ADD CONSTRAINT [PK_LaboratoryTests]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VaccinationAdministereds'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.VaccinationAdministereds', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[VaccinationAdministereds] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.VaccinationAdministereds', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[VaccinationAdministereds]
ADD CONSTRAINT [PK_VaccinationAdministereds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AllergySources'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.AllergySources', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[AllergySources] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.AllergySources', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[AllergySources]
ADD CONSTRAINT [PK_AllergySources]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VaccineTypes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.VaccineTypes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[VaccineTypes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.VaccineTypes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[VaccineTypes]
ADD CONSTRAINT [PK_VaccineTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClinicalProcedures'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ClinicalProcedures', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ClinicalProcedures] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ClinicalProcedures', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ClinicalProcedures]
ADD CONSTRAINT [PK_ClinicalProcedures]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RelevancyQualifiers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.RelevancyQualifiers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[RelevancyQualifiers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.RelevancyQualifiers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[RelevancyQualifiers]
ADD CONSTRAINT [PK_RelevancyQualifiers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AccuracyQualifiers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.AccuracyQualifiers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[AccuracyQualifiers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.AccuracyQualifiers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[AccuracyQualifiers]
ADD CONSTRAINT [PK_AccuracyQualifiers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AxisQualifiers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.AxisQualifiers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[AxisQualifiers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.AxisQualifiers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[AxisQualifiers]
ADD CONSTRAINT [PK_AxisQualifiers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClinicalQualifiers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ClinicalQualifiers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ClinicalQualifiers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ClinicalQualifiers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ClinicalQualifiers]
ADD CONSTRAINT [PK_ClinicalQualifiers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClinicalConditions'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ClinicalConditions', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ClinicalConditions] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ClinicalConditions', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ClinicalConditions]
ADD CONSTRAINT [PK_ClinicalConditions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SmokingConditions'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.SmokingConditions', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[SmokingConditions] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.SmokingConditions', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[SmokingConditions]
ADD CONSTRAINT [PK_SmokingConditions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DrugDaysSupplies'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.DrugDaysSupplies', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[DrugDaysSupplies] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.DrugDaysSupplies', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[DrugDaysSupplies]
ADD CONSTRAINT [PK_DrugDaysSupplies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VaccineDoses'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.VaccineDoses', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[VaccineDoses] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.VaccineDoses', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[VaccineDoses]
ADD CONSTRAINT [PK_VaccineDoses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InjectionSites'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.InjectionSites', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[InjectionSites] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.InjectionSites', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[InjectionSites]
ADD CONSTRAINT [PK_InjectionSites]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HypodermicNeedles'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.HypodermicNeedles', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[HypodermicNeedles] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.HypodermicNeedles', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[HypodermicNeedles]
ADD CONSTRAINT [PK_HypodermicNeedles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterHistoryOfPresentIllnessComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterHistoryOfPresentIllnessComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterHistoryOfPresentIllnessComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterHistoryOfPresentIllnessComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterHistoryOfPresentIllnessComments]
ADD CONSTRAINT [PK_EncounterHistoryOfPresentIllnessComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SpecimenConditions'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.SpecimenConditions', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[SpecimenConditions] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.SpecimenConditions', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[SpecimenConditions]
ADD CONSTRAINT [PK_SpecimenConditions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratorySpecimenRejectReasons'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratorySpecimenRejectReasons', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratorySpecimenRejectReasons] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratorySpecimenRejectReasons', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratorySpecimenRejectReasons]
ADD CONSTRAINT [PK_LaboratorySpecimenRejectReasons]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaboratoryTestAbnormalFlags'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestAbnormalFlags', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryTestAbnormalFlags] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestAbnormalFlags', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryTestAbnormalFlags]
ADD CONSTRAINT [PK_LaboratoryTestAbnormalFlags]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UnitsOfMeasurement'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.UnitsOfMeasurement', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[UnitsOfMeasurement] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.UnitsOfMeasurement', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[UnitsOfMeasurement]
ADD CONSTRAINT [PK_UnitsOfMeasurement]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TransitionOfCareReasons'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TransitionOfCareReasons', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TransitionOfCareReasons] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TransitionOfCareReasons', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TransitionOfCareReasons]
ADD CONSTRAINT [PK_TransitionOfCareReasons]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ObservationTypes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ObservationTypes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ObservationTypes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ObservationTypes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ObservationTypes]
ADD CONSTRAINT [PK_ObservationTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ChiefComplaints'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ChiefComplaints', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ChiefComplaints] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ChiefComplaints', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ChiefComplaints]
ADD CONSTRAINT [PK_ChiefComplaints]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterChiefComplaints'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterChiefComplaints', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterChiefComplaints] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterChiefComplaints', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterChiefComplaints]
ADD CONSTRAINT [PK_EncounterChiefComplaints]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CommunicationTransactions'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.CommunicationTransactions', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[CommunicationTransactions] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.CommunicationTransactions', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[CommunicationTransactions]
ADD CONSTRAINT [PK_CommunicationTransactions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ObservationTypeCategories'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ObservationTypeCategories', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ObservationTypeCategories] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ObservationTypeCategories', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ObservationTypeCategories]
ADD CONSTRAINT [PK_ObservationTypeCategories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ChiefComplaintCategories'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ChiefComplaintCategories', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ChiefComplaintCategories] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ChiefComplaintCategories', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ChiefComplaintCategories]
ADD CONSTRAINT [PK_ChiefComplaintCategories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterChiefComplaintComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterChiefComplaintComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterChiefComplaintComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterChiefComplaintComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterChiefComplaintComments]
ADD CONSTRAINT [PK_EncounterChiefComplaintComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterReasonForVisitComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterReasonForVisitComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterReasonForVisitComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterReasonForVisitComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterReasonForVisitComments]
ADD CONSTRAINT [PK_EncounterReasonForVisitComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterReviewOfSystemComments'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterReviewOfSystemComments', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterReviewOfSystemComments] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterReviewOfSystemComments', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterReviewOfSystemComments]
ADD CONSTRAINT [PK_EncounterReviewOfSystemComments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientDiagnosisDetailAxisQualifiers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientDiagnosisDetailAxisQualifiers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientDiagnosisDetailAxisQualifiers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientDiagnosisDetailAxisQualifiers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientDiagnosisDetailAxisQualifiers]
ADD CONSTRAINT [PK_PatientDiagnosisDetailAxisQualifiers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientFamilyHistoryEntries'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientFamilyHistoryEntries', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientFamilyHistoryEntries] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientFamilyHistoryEntries', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientFamilyHistoryEntries]
ADD CONSTRAINT [PK_PatientFamilyHistoryEntries]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientFamilyHistoryEntryDetails'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientFamilyHistoryEntryDetails', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientFamilyHistoryEntryDetails] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientFamilyHistoryEntryDetails', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientFamilyHistoryEntryDetails]
ADD CONSTRAINT [PK_PatientFamilyHistoryEntryDetails]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FamilyRelationships'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.FamilyRelationships', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[FamilyRelationships] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.FamilyRelationships', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[FamilyRelationships]
ADD CONSTRAINT [PK_FamilyRelationships]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CommunicationCommunicationReviewStatus'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.CommunicationCommunicationReviewStatus', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[CommunicationCommunicationReviewStatus] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.CommunicationCommunicationReviewStatus', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[CommunicationCommunicationReviewStatus]
ADD CONSTRAINT [PK_CommunicationCommunicationReviewStatus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UnitOfMeasurementTypes'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.UnitOfMeasurementTypes', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[UnitOfMeasurementTypes] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.UnitOfMeasurementTypes', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[UnitOfMeasurementTypes]
ADD CONSTRAINT [PK_UnitOfMeasurementTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientAllergenConcerns'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientAllergenConcerns', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientAllergenConcerns] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientAllergenConcerns', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientAllergenConcerns]
ADD CONSTRAINT [PK_PatientAllergenConcerns]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientProblemConcernLevels'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientProblemConcernLevels', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientProblemConcernLevels] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientProblemConcernLevels', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientProblemConcernLevels]
ADD CONSTRAINT [PK_PatientProblemConcernLevels]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TreatmentGoals'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TreatmentGoals', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TreatmentGoals] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TreatmentGoals', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TreatmentGoals]
ADD CONSTRAINT [PK_TreatmentGoals]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EncounterReviewOfSystems'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterReviewOfSystems', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterReviewOfSystems] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterReviewOfSystems', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterReviewOfSystems]
ADD CONSTRAINT [PK_EncounterReviewOfSystems]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ReasonForVisits_Id], [Qualities_Id] in table 'ReasonForVisitQuality'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ReasonForVisitQuality', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ReasonForVisitQuality] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ReasonForVisitQuality', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ReasonForVisitQuality]
ADD CONSTRAINT [PK_ReasonForVisitQuality]
    PRIMARY KEY NONCLUSTERED ([ReasonForVisits_Id], [Qualities_Id] ASC);
GO

-- Creating primary key on [ReasonForVisits_Id], [Contexts_Id] in table 'ReasonForVisitContext'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ReasonForVisitContext', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ReasonForVisitContext] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ReasonForVisitContext', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ReasonForVisitContext]
ADD CONSTRAINT [PK_ReasonForVisitContext]
    PRIMARY KEY NONCLUSTERED ([ReasonForVisits_Id], [Contexts_Id] ASC);
GO

-- Creating primary key on [EncounterReasonForVisits_Id], [ReasonForVisits_Id] in table 'EncounterReasonForVisitReasonForVisit'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterReasonForVisitReasonForVisit', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterReasonForVisitReasonForVisit] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterReasonForVisitReasonForVisit', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterReasonForVisitReasonForVisit]
ADD CONSTRAINT [PK_EncounterReasonForVisitReasonForVisit]
    PRIMARY KEY NONCLUSTERED ([EncounterReasonForVisits_Id], [ReasonForVisits_Id] ASC);
GO

-- Creating primary key on [ModifyingFactors_Id], [EncounterReasonForVisits_Id] in table 'ModifyingFactorEncounterReasonForVisit'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ModifyingFactorEncounterReasonForVisit', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ModifyingFactorEncounterReasonForVisit] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ModifyingFactorEncounterReasonForVisit', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ModifyingFactorEncounterReasonForVisit]
ADD CONSTRAINT [PK_ModifyingFactorEncounterReasonForVisit]
    PRIMARY KEY NONCLUSTERED ([ModifyingFactors_Id], [EncounterReasonForVisits_Id] ASC);
GO

-- Creating primary key on [AssociatedSignAndSymptom_Id], [EncounterReasonForVisits_Id] in table 'AssociatedSignAndSymptomEncounterReasonForVisit'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.AssociatedSignAndSymptomEncounterReasonForVisit', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[AssociatedSignAndSymptomEncounterReasonForVisit] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.AssociatedSignAndSymptomEncounterReasonForVisit', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[AssociatedSignAndSymptomEncounterReasonForVisit]
ADD CONSTRAINT [PK_AssociatedSignAndSymptomEncounterReasonForVisit]
    PRIMARY KEY NONCLUSTERED ([AssociatedSignAndSymptom_Id], [EncounterReasonForVisits_Id] ASC);
GO

-- Creating primary key on [DiagnosticTestOrders_Id], [TreatmentPlans_Id] in table 'DiagnosticTestOrderTreatmentPlan'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.DiagnosticTestOrderTreatmentPlan', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[DiagnosticTestOrderTreatmentPlan] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.DiagnosticTestOrderTreatmentPlan', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[DiagnosticTestOrderTreatmentPlan]
ADD CONSTRAINT [PK_DiagnosticTestOrderTreatmentPlan]
    PRIMARY KEY NONCLUSTERED ([DiagnosticTestOrders_Id], [TreatmentPlans_Id] ASC);
GO

-- Creating primary key on [SubsequentVisitOrders_Id], [TreatmentPlans_Id] in table 'FollowUpVisitTreatmentPlan'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.FollowUpVisitTreatmentPlan', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[FollowUpVisitTreatmentPlan] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.FollowUpVisitTreatmentPlan', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[FollowUpVisitTreatmentPlan]
ADD CONSTRAINT [PK_FollowUpVisitTreatmentPlan]
    PRIMARY KEY NONCLUSTERED ([SubsequentVisitOrders_Id], [TreatmentPlans_Id] ASC);
GO

-- Creating primary key on [ProcedureOrders_Id], [TreatmentPlans_Id] in table 'ProcedureOrderTreatmentPlan'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ProcedureOrderTreatmentPlan', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ProcedureOrderTreatmentPlan] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ProcedureOrderTreatmentPlan', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ProcedureOrderTreatmentPlan]
ADD CONSTRAINT [PK_ProcedureOrderTreatmentPlan]
    PRIMARY KEY NONCLUSTERED ([ProcedureOrders_Id], [TreatmentPlans_Id] ASC);
GO

-- Creating primary key on [LaboratoryTestOrders_Id], [TreatmentPlans_Id] in table 'LaboratoryTestOrderTreatmentPlan'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestOrderTreatmentPlan', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryTestOrderTreatmentPlan] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestOrderTreatmentPlan', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryTestOrderTreatmentPlan]
ADD CONSTRAINT [PK_LaboratoryTestOrderTreatmentPlan]
    PRIMARY KEY NONCLUSTERED ([LaboratoryTestOrders_Id], [TreatmentPlans_Id] ASC);
GO

-- Creating primary key on [CommunicationWithOtherProviders_Id], [TreatmentPlans_Id] in table 'CommunicationWithOtherProviderTreatmentPlan'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.CommunicationWithOtherProviderTreatmentPlan', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[CommunicationWithOtherProviderTreatmentPlan] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.CommunicationWithOtherProviderTreatmentPlan', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[CommunicationWithOtherProviderTreatmentPlan]
ADD CONSTRAINT [PK_CommunicationWithOtherProviderTreatmentPlan]
    PRIMARY KEY NONCLUSTERED ([CommunicationWithOtherProviders_Id], [TreatmentPlans_Id] ASC);
GO

-- Creating primary key on [TransitionsOfCare_Id], [UserTreatmentPlans_Id] in table 'TransitionOfCareTreatmentPlan'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TransitionOfCareTreatmentPlan', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TransitionOfCareTreatmentPlan] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TransitionOfCareTreatmentPlan', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TransitionOfCareTreatmentPlan]
ADD CONSTRAINT [PK_TransitionOfCareTreatmentPlan]
    PRIMARY KEY NONCLUSTERED ([TransitionsOfCare_Id], [UserTreatmentPlans_Id] ASC);
GO

-- Creating primary key on [OtherTreatmentPlanOrders_Id], [TreatmentPlans_Id] in table 'OtherOrderTreatmentPlan'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.OtherOrderTreatmentPlan', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[OtherOrderTreatmentPlan] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.OtherOrderTreatmentPlan', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[OtherOrderTreatmentPlan]
ADD CONSTRAINT [PK_OtherOrderTreatmentPlan]
    PRIMARY KEY NONCLUSTERED ([OtherTreatmentPlanOrders_Id], [TreatmentPlans_Id] ASC);
GO

-- Creating primary key on [RefractivePrescriptions_Id], [UserTreatmentPlans_Id] in table 'RefractivePrescriptionTreatmentPlan'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.RefractivePrescriptionTreatmentPlan', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[RefractivePrescriptionTreatmentPlan] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.RefractivePrescriptionTreatmentPlan', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[RefractivePrescriptionTreatmentPlan]
ADD CONSTRAINT [PK_RefractivePrescriptionTreatmentPlan]
    PRIMARY KEY NONCLUSTERED ([RefractivePrescriptions_Id], [UserTreatmentPlans_Id] ASC);
GO

-- Creating primary key on [Medications_Id], [MedicationFamilies_Id] in table 'MedicationMedicationFamily'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.MedicationMedicationFamily', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[MedicationMedicationFamily] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.MedicationMedicationFamily', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[MedicationMedicationFamily]
ADD CONSTRAINT [PK_MedicationMedicationFamily]
    PRIMARY KEY NONCLUSTERED ([Medications_Id], [MedicationFamilies_Id] ASC);
GO

-- Creating primary key on [UserTreatmentPlans_Id], [TreatmentGoalAndInstructions_Id] in table 'TreatmentPlanTreatmentGoal'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TreatmentPlanTreatmentGoal', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TreatmentPlanTreatmentGoal] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TreatmentPlanTreatmentGoal', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TreatmentPlanTreatmentGoal]
ADD CONSTRAINT [PK_TreatmentPlanTreatmentGoal]
    PRIMARY KEY NONCLUSTERED ([UserTreatmentPlans_Id], [TreatmentGoalAndInstructions_Id] ASC);
GO

-- Creating primary key on [ProcedureOrders_Id], [ProcedurePerformeds_Id] in table 'ProcedureOrderProcedurePerformed'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ProcedureOrderProcedurePerformed', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ProcedureOrderProcedurePerformed] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ProcedureOrderProcedurePerformed', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ProcedureOrderProcedurePerformed]
ADD CONSTRAINT [PK_ProcedureOrderProcedurePerformed]
    PRIMARY KEY NONCLUSTERED ([ProcedureOrders_Id], [ProcedurePerformeds_Id] ASC);
GO

-- Creating primary key on [DiagnosticTestOrders_Id], [DiagnosticTestPerformeds_Id] in table 'DiagnosticTestOrderDiagnosticTestPerformed'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.DiagnosticTestOrderDiagnosticTestPerformed', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[DiagnosticTestOrderDiagnosticTestPerformed] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.DiagnosticTestOrderDiagnosticTestPerformed', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[DiagnosticTestOrderDiagnosticTestPerformed]
ADD CONSTRAINT [PK_DiagnosticTestOrderDiagnosticTestPerformed]
    PRIMARY KEY NONCLUSTERED ([DiagnosticTestOrders_Id], [DiagnosticTestPerformeds_Id] ASC);
GO

-- Creating primary key on [LaboratoryTestOrders_Id], [LaboratoryTestResults_Id] in table 'LaboratoryTestOrderLaboratoryTestResult'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestOrderLaboratoryTestResult', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[LaboratoryTestOrderLaboratoryTestResult] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.LaboratoryTestOrderLaboratoryTestResult', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[LaboratoryTestOrderLaboratoryTestResult]
ADD CONSTRAINT [PK_LaboratoryTestOrderLaboratoryTestResult]
    PRIMARY KEY NONCLUSTERED ([LaboratoryTestOrders_Id], [LaboratoryTestResults_Id] ASC);
GO

-- Creating primary key on [Medications_Id], [ClinicalSpecialtyTypes_Id] in table 'MedicationClinicalSpecialtyType'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.MedicationClinicalSpecialtyType', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[MedicationClinicalSpecialtyType] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.MedicationClinicalSpecialtyType', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[MedicationClinicalSpecialtyType]
ADD CONSTRAINT [PK_MedicationClinicalSpecialtyType]
    PRIMARY KEY NONCLUSTERED ([Medications_Id], [ClinicalSpecialtyTypes_Id] ASC);
GO

-- Creating primary key on [TreatmentPlans_Id], [MedicationOrderFavorites_Id] in table 'TreatmentPlanMedicationOrderFavorite'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TreatmentPlanMedicationOrderFavorite', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TreatmentPlanMedicationOrderFavorite] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TreatmentPlanMedicationOrderFavorite', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TreatmentPlanMedicationOrderFavorite]
ADD CONSTRAINT [PK_TreatmentPlanMedicationOrderFavorite]
    PRIMARY KEY NONCLUSTERED ([TreatmentPlans_Id], [MedicationOrderFavorites_Id] ASC);
GO

-- Creating primary key on [BodyParts_Id], [ClinicalSpecialtyTypes_Id] in table 'BodyPartClinicalSpecialtyType'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.BodyPartClinicalSpecialtyType', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[BodyPartClinicalSpecialtyType] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.BodyPartClinicalSpecialtyType', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[BodyPartClinicalSpecialtyType]
ADD CONSTRAINT [PK_BodyPartClinicalSpecialtyType]
    PRIMARY KEY NONCLUSTERED ([BodyParts_Id], [ClinicalSpecialtyTypes_Id] ASC);
GO

-- Creating primary key on [PatientAllergenDetails_Id], [AllergySources_Id] in table 'PatientAllergenDetailAllergySource'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientAllergenDetailAllergySource', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientAllergenDetailAllergySource] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientAllergenDetailAllergySource', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientAllergenDetailAllergySource]
ADD CONSTRAINT [PK_PatientAllergenDetailAllergySource]
    PRIMARY KEY NONCLUSTERED ([PatientAllergenDetails_Id], [AllergySources_Id] ASC);
GO

-- Creating primary key on [VaccineAllergenReactionType_AllergenReactionType_Id], [VaccineAllergenReactionType_Vaccine_Id] in table 'VaccineAllergenReactionType'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.VaccineAllergenReactionType', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[VaccineAllergenReactionType] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.VaccineAllergenReactionType', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[VaccineAllergenReactionType]
ADD CONSTRAINT [PK_VaccineAllergenReactionType]
    PRIMARY KEY NONCLUSTERED ([VaccineAllergenReactionType_AllergenReactionType_Id], [VaccineAllergenReactionType_Vaccine_Id] ASC);
GO

-- Creating primary key on [TreatmentPlans_Id], [ClinicalConditions_Id] in table 'TreatmentPlanClinicalCondition'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TreatmentPlanClinicalCondition', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TreatmentPlanClinicalCondition] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TreatmentPlanClinicalCondition', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TreatmentPlanClinicalCondition]
ADD CONSTRAINT [PK_TreatmentPlanClinicalCondition]
    PRIMARY KEY NONCLUSTERED ([TreatmentPlans_Id], [ClinicalConditions_Id] ASC);
GO

-- Creating primary key on [EncounterLaboratoryTestOrders_Id], [ClinicalConditions_Id] in table 'EncounterLaboratoryTestOrderClinicalCondition'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterLaboratoryTestOrderClinicalCondition', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterLaboratoryTestOrderClinicalCondition] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterLaboratoryTestOrderClinicalCondition', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterLaboratoryTestOrderClinicalCondition]
ADD CONSTRAINT [PK_EncounterLaboratoryTestOrderClinicalCondition]
    PRIMARY KEY NONCLUSTERED ([EncounterLaboratoryTestOrders_Id], [ClinicalConditions_Id] ASC);
GO

-- Creating primary key on [EncounterPatientEducations_Id], [ClinicalConditions_Id] in table 'EncounterPatientEducationClinicalCondition'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.EncounterPatientEducationClinicalCondition', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[EncounterPatientEducationClinicalCondition] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.EncounterPatientEducationClinicalCondition', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[EncounterPatientEducationClinicalCondition]
ADD CONSTRAINT [PK_EncounterPatientEducationClinicalCondition]
    PRIMARY KEY NONCLUSTERED ([EncounterPatientEducations_Id], [ClinicalConditions_Id] ASC);
GO

-- Creating primary key on [PatientEducations_Id], [ClinicalConditions_Id] in table 'PatientEducationClinicalCondition'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientEducationClinicalCondition', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientEducationClinicalCondition] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientEducationClinicalCondition', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientEducationClinicalCondition]
ADD CONSTRAINT [PK_PatientEducationClinicalCondition]
    PRIMARY KEY NONCLUSTERED ([PatientEducations_Id], [ClinicalConditions_Id] ASC);
GO

-- Creating primary key on [ProcedurePerformeds_Id], [ClinicalConditions_Id] in table 'ProcedurePerformedClinicalCondition'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ProcedurePerformedClinicalCondition', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ProcedurePerformedClinicalCondition] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ProcedurePerformedClinicalCondition', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ProcedurePerformedClinicalCondition]
ADD CONSTRAINT [PK_ProcedurePerformedClinicalCondition]
    PRIMARY KEY NONCLUSTERED ([ProcedurePerformeds_Id], [ClinicalConditions_Id] ASC);
GO

-- Creating primary key on [DiagnosticTestPerformeds_Id], [ClinicalConditions_Id] in table 'DiagnosticTestPerformedClinicalCondition'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.DiagnosticTestPerformedClinicalCondition', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[DiagnosticTestPerformedClinicalCondition] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.DiagnosticTestPerformedClinicalCondition', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[DiagnosticTestPerformedClinicalCondition]
ADD CONSTRAINT [PK_DiagnosticTestPerformedClinicalCondition]
    PRIMARY KEY NONCLUSTERED ([DiagnosticTestPerformeds_Id], [ClinicalConditions_Id] ASC);
GO

-- Creating primary key on [RouteOfAdministrations_Id], [ClinicalSpecialtyTypes_Id] in table 'RouteOfAdministrationClinicalSpecialtyType'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.RouteOfAdministrationClinicalSpecialtyType', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[RouteOfAdministrationClinicalSpecialtyType] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.RouteOfAdministrationClinicalSpecialtyType', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[RouteOfAdministrationClinicalSpecialtyType]
ADD CONSTRAINT [PK_RouteOfAdministrationClinicalSpecialtyType]
    PRIMARY KEY NONCLUSTERED ([RouteOfAdministrations_Id], [ClinicalSpecialtyTypes_Id] ASC);
GO

-- Creating primary key on [VaccinationAdministereds_Id], [UnitsOfMeasurement_Id] in table 'VaccinationAdministeredUnitOfMeasurement'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.VaccinationAdministeredUnitOfMeasurement', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[VaccinationAdministeredUnitOfMeasurement] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.VaccinationAdministeredUnitOfMeasurement', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[VaccinationAdministeredUnitOfMeasurement]
ADD CONSTRAINT [PK_VaccinationAdministeredUnitOfMeasurement]
    PRIMARY KEY NONCLUSTERED ([VaccinationAdministereds_Id], [UnitsOfMeasurement_Id] ASC);
GO

-- Creating primary key on [TreatmentGoalAndInstructions_Id], [ClinicalConditions_Id] in table 'TreatmentGoalClinicalCondition'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.TreatmentGoalClinicalCondition', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[TreatmentGoalClinicalCondition] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.TreatmentGoalClinicalCondition', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[TreatmentGoalClinicalCondition]
ADD CONSTRAINT [PK_TreatmentGoalClinicalCondition]
    PRIMARY KEY NONCLUSTERED ([TreatmentGoalAndInstructions_Id], [ClinicalConditions_Id] ASC);
GO

-- Creating primary key on [Screens_Id], [Questions_Id] in table 'ScreenQuestion'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.ScreenQuestion', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[ScreenQuestion] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.ScreenQuestion', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[ScreenQuestion]
ADD CONSTRAINT [PK_ScreenQuestion]
    PRIMARY KEY NONCLUSTERED ([Screens_Id], [Questions_Id] ASC);
GO

-- Creating primary key on [CopiesSentPatientLaboratoryTestResults_Id], [CopiesSentExternalProviders_Id] in table 'PatientLaboratoryTestResultCopiesSentExternalProvider'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientLaboratoryTestResultCopiesSentExternalProvider', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientLaboratoryTestResultCopiesSentExternalProvider] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientLaboratoryTestResultCopiesSentExternalProvider', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientLaboratoryTestResultCopiesSentExternalProvider]
ADD CONSTRAINT [PK_PatientLaboratoryTestResultCopiesSentExternalProvider]
    PRIMARY KEY NONCLUSTERED ([CopiesSentPatientLaboratoryTestResults_Id], [CopiesSentExternalProviders_Id] ASC);
GO

-- Creating primary key on [PatientFamilyHistoryEntryDetails_Id], [ClinicalQualifiers_Id] in table 'PatientFamilyHistoryEntryDetailClinicalQualifier'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientFamilyHistoryEntryDetailClinicalQualifier', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientFamilyHistoryEntryDetailClinicalQualifier] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientFamilyHistoryEntryDetailClinicalQualifier', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientFamilyHistoryEntryDetailClinicalQualifier]
ADD CONSTRAINT [PK_PatientFamilyHistoryEntryDetailClinicalQualifier]
    PRIMARY KEY NONCLUSTERED ([PatientFamilyHistoryEntryDetails_Id], [ClinicalQualifiers_Id] ASC);
GO

-- Creating primary key on [PatientAllergenDetails_Id], [ClinicalQualifiers_Id] in table 'PatientAllergenDetailClinicalQualifier'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientAllergenDetailClinicalQualifier', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientAllergenDetailClinicalQualifier] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientAllergenDetailClinicalQualifier', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientAllergenDetailClinicalQualifier]
ADD CONSTRAINT [PK_PatientAllergenDetailClinicalQualifier]
    PRIMARY KEY NONCLUSTERED ([PatientAllergenDetails_Id], [ClinicalQualifiers_Id] ASC);
GO

-- Creating primary key on [PatientBloodPressures_Id], [ClinicalQualifiers_Id] in table 'PatientBloodPressureClinicalQualifier'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientBloodPressureClinicalQualifier', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientBloodPressureClinicalQualifier] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientBloodPressureClinicalQualifier', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientBloodPressureClinicalQualifier]
ADD CONSTRAINT [PK_PatientBloodPressureClinicalQualifier]
    PRIMARY KEY NONCLUSTERED ([PatientBloodPressures_Id], [ClinicalQualifiers_Id] ASC);
GO

-- Creating primary key on [PatientHeightAndWeights_Id], [ClinicalQualifiers_Id] in table 'PatientHeightAndWeightClinicalQualifier'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientHeightAndWeightClinicalQualifier', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientHeightAndWeightClinicalQualifier] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientHeightAndWeightClinicalQualifier', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientHeightAndWeightClinicalQualifier]
ADD CONSTRAINT [PK_PatientHeightAndWeightClinicalQualifier]
    PRIMARY KEY NONCLUSTERED ([PatientHeightAndWeights_Id], [ClinicalQualifiers_Id] ASC);
GO

-- Creating primary key on [PatientCognitiveStatusAssessments_Id], [ClinicalQualifiers_Id] in table 'PatientCognitiveStatusAssessmentClinicalQualifier'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientCognitiveStatusAssessmentClinicalQualifier', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientCognitiveStatusAssessmentClinicalQualifier] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientCognitiveStatusAssessmentClinicalQualifier', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientCognitiveStatusAssessmentClinicalQualifier]
ADD CONSTRAINT [PK_PatientCognitiveStatusAssessmentClinicalQualifier]
    PRIMARY KEY NONCLUSTERED ([PatientCognitiveStatusAssessments_Id], [ClinicalQualifiers_Id] ASC);
GO

-- Creating primary key on [PatientFunctionalStatusAssessments_Id], [ClinicalQualifiers_Id] in table 'PatientFunctionalStatusAssessmentClinicalQualifier'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientFunctionalStatusAssessmentClinicalQualifier', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientFunctionalStatusAssessmentClinicalQualifier] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientFunctionalStatusAssessmentClinicalQualifier', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientFunctionalStatusAssessmentClinicalQualifier]
ADD CONSTRAINT [PK_PatientFunctionalStatusAssessmentClinicalQualifier]
    PRIMARY KEY NONCLUSTERED ([PatientFunctionalStatusAssessments_Id], [ClinicalQualifiers_Id] ASC);
GO

-- Creating primary key on [UserTreatmentPlans_Id], [PatientEducations_Id] in table 'UserTreatmentPlanPatientEducation'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.UserTreatmentPlanPatientEducation', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[UserTreatmentPlanPatientEducation] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.UserTreatmentPlanPatientEducation', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[UserTreatmentPlanPatientEducation]
ADD CONSTRAINT [PK_UserTreatmentPlanPatientEducation]
    PRIMARY KEY NONCLUSTERED ([UserTreatmentPlans_Id], [PatientEducations_Id] ASC);
GO

-- Creating primary key on [UserTreatmentPlans_Id], [VaccinationOrders_Id] in table 'UserTreatmentPlanVaccinationOrder'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.UserTreatmentPlanVaccinationOrder', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[UserTreatmentPlanVaccinationOrder] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.UserTreatmentPlanVaccinationOrder', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[UserTreatmentPlanVaccinationOrder]
ADD CONSTRAINT [PK_UserTreatmentPlanVaccinationOrder]
    PRIMARY KEY NONCLUSTERED ([UserTreatmentPlans_Id], [VaccinationOrders_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ReasonForVisits_Id] in table 'ReasonForVisitQuality'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ReasonForVisitQuality_ReasonForVisit'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ReasonForVisitQuality] DROP CONSTRAINT FK_ReasonForVisitQuality_ReasonForVisit

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReasonForVisits]'), 'IsUserTable') = 1
ALTER TABLE [model].[ReasonForVisitQuality]
ADD CONSTRAINT [FK_ReasonForVisitQuality_ReasonForVisit]
    FOREIGN KEY ([ReasonForVisits_Id])
    REFERENCES [model].[EncounterReasonForVisits]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Qualities_Id] in table 'ReasonForVisitQuality'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ReasonForVisitQuality_Quality'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ReasonForVisitQuality] DROP CONSTRAINT FK_ReasonForVisitQuality_Quality

IF OBJECTPROPERTY(OBJECT_ID('[model].[Qualities]'), 'IsUserTable') = 1
ALTER TABLE [model].[ReasonForVisitQuality]
ADD CONSTRAINT [FK_ReasonForVisitQuality_Quality]
    FOREIGN KEY ([Qualities_Id])
    REFERENCES [model].[Qualities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ReasonForVisitQuality_Quality'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ReasonForVisitQuality_Quality')
	DROP INDEX IX_FK_ReasonForVisitQuality_Quality ON [model].[ReasonForVisitQuality]
GO
CREATE INDEX [IX_FK_ReasonForVisitQuality_Quality]
ON [model].[ReasonForVisitQuality]
    ([Qualities_Id]);
GO

-- Creating foreign key on [ReasonForVisits_Id] in table 'ReasonForVisitContext'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ReasonForVisitContext_ReasonForVisit'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ReasonForVisitContext] DROP CONSTRAINT FK_ReasonForVisitContext_ReasonForVisit

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReasonForVisits]'), 'IsUserTable') = 1
ALTER TABLE [model].[ReasonForVisitContext]
ADD CONSTRAINT [FK_ReasonForVisitContext_ReasonForVisit]
    FOREIGN KEY ([ReasonForVisits_Id])
    REFERENCES [model].[EncounterReasonForVisits]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Contexts_Id] in table 'ReasonForVisitContext'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ReasonForVisitContext_Context'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ReasonForVisitContext] DROP CONSTRAINT FK_ReasonForVisitContext_Context

IF OBJECTPROPERTY(OBJECT_ID('[model].[Contexts]'), 'IsUserTable') = 1
ALTER TABLE [model].[ReasonForVisitContext]
ADD CONSTRAINT [FK_ReasonForVisitContext_Context]
    FOREIGN KEY ([Contexts_Id])
    REFERENCES [model].[Contexts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ReasonForVisitContext_Context'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ReasonForVisitContext_Context')
	DROP INDEX IX_FK_ReasonForVisitContext_Context ON [model].[ReasonForVisitContext]
GO
CREATE INDEX [IX_FK_ReasonForVisitContext_Context]
ON [model].[ReasonForVisitContext]
    ([Contexts_Id]);
GO

-- Creating foreign key on [EncounterReasonForVisits_Id] in table 'EncounterReasonForVisitReasonForVisit'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterReasonForVisitReasonForVisit_EncounterReasonForVisit'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterReasonForVisitReasonForVisit] DROP CONSTRAINT FK_EncounterReasonForVisitReasonForVisit_EncounterReasonForVisit

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReasonForVisits]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterReasonForVisitReasonForVisit]
ADD CONSTRAINT [FK_EncounterReasonForVisitReasonForVisit_EncounterReasonForVisit]
    FOREIGN KEY ([EncounterReasonForVisits_Id])
    REFERENCES [model].[EncounterReasonForVisits]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ReasonForVisits_Id] in table 'EncounterReasonForVisitReasonForVisit'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterReasonForVisitReasonForVisit_ReasonForVisit'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterReasonForVisitReasonForVisit] DROP CONSTRAINT FK_EncounterReasonForVisitReasonForVisit_ReasonForVisit

IF OBJECTPROPERTY(OBJECT_ID('[model].[ReasonForVisits]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterReasonForVisitReasonForVisit]
ADD CONSTRAINT [FK_EncounterReasonForVisitReasonForVisit_ReasonForVisit]
    FOREIGN KEY ([ReasonForVisits_Id])
    REFERENCES [model].[ReasonForVisits]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterReasonForVisitReasonForVisit_ReasonForVisit'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterReasonForVisitReasonForVisit_ReasonForVisit')
	DROP INDEX IX_FK_EncounterReasonForVisitReasonForVisit_ReasonForVisit ON [model].[EncounterReasonForVisitReasonForVisit]
GO
CREATE INDEX [IX_FK_EncounterReasonForVisitReasonForVisit_ReasonForVisit]
ON [model].[EncounterReasonForVisitReasonForVisit]
    ([ReasonForVisits_Id]);
GO

-- Creating foreign key on [ModifyingFactors_Id] in table 'ModifyingFactorEncounterReasonForVisit'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ModifyingFactorEncounterReasonForVisit_ModifyingFactor'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ModifyingFactorEncounterReasonForVisit] DROP CONSTRAINT FK_ModifyingFactorEncounterReasonForVisit_ModifyingFactor

IF OBJECTPROPERTY(OBJECT_ID('[model].[ModifyingFactors]'), 'IsUserTable') = 1
ALTER TABLE [model].[ModifyingFactorEncounterReasonForVisit]
ADD CONSTRAINT [FK_ModifyingFactorEncounterReasonForVisit_ModifyingFactor]
    FOREIGN KEY ([ModifyingFactors_Id])
    REFERENCES [model].[ModifyingFactors]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [EncounterReasonForVisits_Id] in table 'ModifyingFactorEncounterReasonForVisit'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ModifyingFactorEncounterReasonForVisit_EncounterReasonForVisit'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ModifyingFactorEncounterReasonForVisit] DROP CONSTRAINT FK_ModifyingFactorEncounterReasonForVisit_EncounterReasonForVisit

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReasonForVisits]'), 'IsUserTable') = 1
ALTER TABLE [model].[ModifyingFactorEncounterReasonForVisit]
ADD CONSTRAINT [FK_ModifyingFactorEncounterReasonForVisit_EncounterReasonForVisit]
    FOREIGN KEY ([EncounterReasonForVisits_Id])
    REFERENCES [model].[EncounterReasonForVisits]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ModifyingFactorEncounterReasonForVisit_EncounterReasonForVisit'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ModifyingFactorEncounterReasonForVisit_EncounterReasonForVisit')
	DROP INDEX IX_FK_ModifyingFactorEncounterReasonForVisit_EncounterReasonForVisit ON [model].[ModifyingFactorEncounterReasonForVisit]
GO
CREATE INDEX [IX_FK_ModifyingFactorEncounterReasonForVisit_EncounterReasonForVisit]
ON [model].[ModifyingFactorEncounterReasonForVisit]
    ([EncounterReasonForVisits_Id]);
GO

-- Creating foreign key on [AssociatedSignAndSymptom_Id] in table 'AssociatedSignAndSymptomEncounterReasonForVisit'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_AssociatedSignAndSymptomEncounterReasonForVisit_AssociatedSignAndSymptom'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[AssociatedSignAndSymptomEncounterReasonForVisit] DROP CONSTRAINT FK_AssociatedSignAndSymptomEncounterReasonForVisit_AssociatedSignAndSymptom

IF OBJECTPROPERTY(OBJECT_ID('[model].[AssociatedSignAndSymptom]'), 'IsUserTable') = 1
ALTER TABLE [model].[AssociatedSignAndSymptomEncounterReasonForVisit]
ADD CONSTRAINT [FK_AssociatedSignAndSymptomEncounterReasonForVisit_AssociatedSignAndSymptom]
    FOREIGN KEY ([AssociatedSignAndSymptom_Id])
    REFERENCES [model].[AssociatedSignAndSymptom]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [EncounterReasonForVisits_Id] in table 'AssociatedSignAndSymptomEncounterReasonForVisit'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_AssociatedSignAndSymptomEncounterReasonForVisit_EncounterReasonForVisit'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[AssociatedSignAndSymptomEncounterReasonForVisit] DROP CONSTRAINT FK_AssociatedSignAndSymptomEncounterReasonForVisit_EncounterReasonForVisit

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterReasonForVisits]'), 'IsUserTable') = 1
ALTER TABLE [model].[AssociatedSignAndSymptomEncounterReasonForVisit]
ADD CONSTRAINT [FK_AssociatedSignAndSymptomEncounterReasonForVisit_EncounterReasonForVisit]
    FOREIGN KEY ([EncounterReasonForVisits_Id])
    REFERENCES [model].[EncounterReasonForVisits]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AssociatedSignAndSymptomEncounterReasonForVisit_EncounterReasonForVisit'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_AssociatedSignAndSymptomEncounterReasonForVisit_EncounterReasonForVisit')
	DROP INDEX IX_FK_AssociatedSignAndSymptomEncounterReasonForVisit_EncounterReasonForVisit ON [model].[AssociatedSignAndSymptomEncounterReasonForVisit]
GO
CREATE INDEX [IX_FK_AssociatedSignAndSymptomEncounterReasonForVisit_EncounterReasonForVisit]
ON [model].[AssociatedSignAndSymptomEncounterReasonForVisit]
    ([EncounterReasonForVisits_Id]);
GO

-- Creating foreign key on [DiagnosticTestOrders_Id] in table 'DiagnosticTestOrderTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_DiagnosticTestOrderTreatmentPlan_DiagnosticTestOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[DiagnosticTestOrderTreatmentPlan] DROP CONSTRAINT FK_DiagnosticTestOrderTreatmentPlan_DiagnosticTestOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[DiagnosticTestOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[DiagnosticTestOrderTreatmentPlan]
ADD CONSTRAINT [FK_DiagnosticTestOrderTreatmentPlan_DiagnosticTestOrder]
    FOREIGN KEY ([DiagnosticTestOrders_Id])
    REFERENCES [model].[DiagnosticTestOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TreatmentPlans_Id] in table 'DiagnosticTestOrderTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_DiagnosticTestOrderTreatmentPlan_TreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[DiagnosticTestOrderTreatmentPlan] DROP CONSTRAINT FK_DiagnosticTestOrderTreatmentPlan_TreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[DiagnosticTestOrderTreatmentPlan]
ADD CONSTRAINT [FK_DiagnosticTestOrderTreatmentPlan_TreatmentPlan]
    FOREIGN KEY ([TreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DiagnosticTestOrderTreatmentPlan_TreatmentPlan'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_DiagnosticTestOrderTreatmentPlan_TreatmentPlan')
	DROP INDEX IX_FK_DiagnosticTestOrderTreatmentPlan_TreatmentPlan ON [model].[DiagnosticTestOrderTreatmentPlan]
GO
CREATE INDEX [IX_FK_DiagnosticTestOrderTreatmentPlan_TreatmentPlan]
ON [model].[DiagnosticTestOrderTreatmentPlan]
    ([TreatmentPlans_Id]);
GO

-- Creating foreign key on [SubsequentVisitOrders_Id] in table 'FollowUpVisitTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_FollowUpVisitTreatmentPlan_FollowUpVisit'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[FollowUpVisitTreatmentPlan] DROP CONSTRAINT FK_FollowUpVisitTreatmentPlan_FollowUpVisit

IF OBJECTPROPERTY(OBJECT_ID('[model].[SubsequentVisitOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[FollowUpVisitTreatmentPlan]
ADD CONSTRAINT [FK_FollowUpVisitTreatmentPlan_FollowUpVisit]
    FOREIGN KEY ([SubsequentVisitOrders_Id])
    REFERENCES [model].[SubsequentVisitOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TreatmentPlans_Id] in table 'FollowUpVisitTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_FollowUpVisitTreatmentPlan_TreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[FollowUpVisitTreatmentPlan] DROP CONSTRAINT FK_FollowUpVisitTreatmentPlan_TreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[FollowUpVisitTreatmentPlan]
ADD CONSTRAINT [FK_FollowUpVisitTreatmentPlan_TreatmentPlan]
    FOREIGN KEY ([TreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_FollowUpVisitTreatmentPlan_TreatmentPlan'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_FollowUpVisitTreatmentPlan_TreatmentPlan')
	DROP INDEX IX_FK_FollowUpVisitTreatmentPlan_TreatmentPlan ON [model].[FollowUpVisitTreatmentPlan]
GO
CREATE INDEX [IX_FK_FollowUpVisitTreatmentPlan_TreatmentPlan]
ON [model].[FollowUpVisitTreatmentPlan]
    ([TreatmentPlans_Id]);
GO

-- Creating foreign key on [ProcedureOrders_Id] in table 'ProcedureOrderTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ProcedureOrderTreatmentPlan_ProcedureOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ProcedureOrderTreatmentPlan] DROP CONSTRAINT FK_ProcedureOrderTreatmentPlan_ProcedureOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[ProcedureOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[ProcedureOrderTreatmentPlan]
ADD CONSTRAINT [FK_ProcedureOrderTreatmentPlan_ProcedureOrder]
    FOREIGN KEY ([ProcedureOrders_Id])
    REFERENCES [model].[ProcedureOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TreatmentPlans_Id] in table 'ProcedureOrderTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ProcedureOrderTreatmentPlan_TreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ProcedureOrderTreatmentPlan] DROP CONSTRAINT FK_ProcedureOrderTreatmentPlan_TreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[ProcedureOrderTreatmentPlan]
ADD CONSTRAINT [FK_ProcedureOrderTreatmentPlan_TreatmentPlan]
    FOREIGN KEY ([TreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProcedureOrderTreatmentPlan_TreatmentPlan'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ProcedureOrderTreatmentPlan_TreatmentPlan')
	DROP INDEX IX_FK_ProcedureOrderTreatmentPlan_TreatmentPlan ON [model].[ProcedureOrderTreatmentPlan]
GO
CREATE INDEX [IX_FK_ProcedureOrderTreatmentPlan_TreatmentPlan]
ON [model].[ProcedureOrderTreatmentPlan]
    ([TreatmentPlans_Id]);
GO

-- Creating foreign key on [LaboratoryTestOrders_Id] in table 'LaboratoryTestOrderTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_LaboratoryTestOrderTreatmentPlan_LaboratoryTestOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[LaboratoryTestOrderTreatmentPlan] DROP CONSTRAINT FK_LaboratoryTestOrderTreatmentPlan_LaboratoryTestOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTestOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[LaboratoryTestOrderTreatmentPlan]
ADD CONSTRAINT [FK_LaboratoryTestOrderTreatmentPlan_LaboratoryTestOrder]
    FOREIGN KEY ([LaboratoryTestOrders_Id])
    REFERENCES [model].[LaboratoryTestOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TreatmentPlans_Id] in table 'LaboratoryTestOrderTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_LaboratoryTestOrderTreatmentPlan_TreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[LaboratoryTestOrderTreatmentPlan] DROP CONSTRAINT FK_LaboratoryTestOrderTreatmentPlan_TreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[LaboratoryTestOrderTreatmentPlan]
ADD CONSTRAINT [FK_LaboratoryTestOrderTreatmentPlan_TreatmentPlan]
    FOREIGN KEY ([TreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LaboratoryTestOrderTreatmentPlan_TreatmentPlan'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_LaboratoryTestOrderTreatmentPlan_TreatmentPlan')
	DROP INDEX IX_FK_LaboratoryTestOrderTreatmentPlan_TreatmentPlan ON [model].[LaboratoryTestOrderTreatmentPlan]
GO
CREATE INDEX [IX_FK_LaboratoryTestOrderTreatmentPlan_TreatmentPlan]
ON [model].[LaboratoryTestOrderTreatmentPlan]
    ([TreatmentPlans_Id]);
GO

-- Creating foreign key on [CommunicationWithOtherProviders_Id] in table 'CommunicationWithOtherProviderTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_CommunicationWithOtherProviderTreatmentPlan_CommunicationWithOtherProvider'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[CommunicationWithOtherProviderTreatmentPlan] DROP CONSTRAINT FK_CommunicationWithOtherProviderTreatmentPlan_CommunicationWithOtherProvider

IF OBJECTPROPERTY(OBJECT_ID('[model].[CommunicationWithOtherProviderOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[CommunicationWithOtherProviderTreatmentPlan]
ADD CONSTRAINT [FK_CommunicationWithOtherProviderTreatmentPlan_CommunicationWithOtherProvider]
    FOREIGN KEY ([CommunicationWithOtherProviders_Id])
    REFERENCES [model].[CommunicationWithOtherProviderOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TreatmentPlans_Id] in table 'CommunicationWithOtherProviderTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_CommunicationWithOtherProviderTreatmentPlan_TreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[CommunicationWithOtherProviderTreatmentPlan] DROP CONSTRAINT FK_CommunicationWithOtherProviderTreatmentPlan_TreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[CommunicationWithOtherProviderTreatmentPlan]
ADD CONSTRAINT [FK_CommunicationWithOtherProviderTreatmentPlan_TreatmentPlan]
    FOREIGN KEY ([TreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CommunicationWithOtherProviderTreatmentPlan_TreatmentPlan'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_CommunicationWithOtherProviderTreatmentPlan_TreatmentPlan')
	DROP INDEX IX_FK_CommunicationWithOtherProviderTreatmentPlan_TreatmentPlan ON [model].[CommunicationWithOtherProviderTreatmentPlan]
GO
CREATE INDEX [IX_FK_CommunicationWithOtherProviderTreatmentPlan_TreatmentPlan]
ON [model].[CommunicationWithOtherProviderTreatmentPlan]
    ([TreatmentPlans_Id]);
GO

-- Creating foreign key on [TransitionsOfCare_Id] in table 'TransitionOfCareTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TransitionOfCareTreatmentPlan_TransitionOfCare'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TransitionOfCareTreatmentPlan] DROP CONSTRAINT FK_TransitionOfCareTreatmentPlan_TransitionOfCare

IF OBJECTPROPERTY(OBJECT_ID('[model].[TransitionOfCareOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[TransitionOfCareTreatmentPlan]
ADD CONSTRAINT [FK_TransitionOfCareTreatmentPlan_TransitionOfCare]
    FOREIGN KEY ([TransitionsOfCare_Id])
    REFERENCES [model].[TransitionOfCareOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [UserTreatmentPlans_Id] in table 'TransitionOfCareTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TransitionOfCareTreatmentPlan_TreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TransitionOfCareTreatmentPlan] DROP CONSTRAINT FK_TransitionOfCareTreatmentPlan_TreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[TransitionOfCareTreatmentPlan]
ADD CONSTRAINT [FK_TransitionOfCareTreatmentPlan_TreatmentPlan]
    FOREIGN KEY ([UserTreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TransitionOfCareTreatmentPlan_TreatmentPlan'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_TransitionOfCareTreatmentPlan_TreatmentPlan')
	DROP INDEX IX_FK_TransitionOfCareTreatmentPlan_TreatmentPlan ON [model].[TransitionOfCareTreatmentPlan]
GO
CREATE INDEX [IX_FK_TransitionOfCareTreatmentPlan_TreatmentPlan]
ON [model].[TransitionOfCareTreatmentPlan]
    ([UserTreatmentPlans_Id]);
GO

-- Creating foreign key on [OtherTreatmentPlanOrders_Id] in table 'OtherOrderTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_OtherOrderTreatmentPlan_OtherOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[OtherOrderTreatmentPlan] DROP CONSTRAINT FK_OtherOrderTreatmentPlan_OtherOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[OtherTreatmentPlanOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[OtherOrderTreatmentPlan]
ADD CONSTRAINT [FK_OtherOrderTreatmentPlan_OtherOrder]
    FOREIGN KEY ([OtherTreatmentPlanOrders_Id])
    REFERENCES [model].[OtherTreatmentPlanOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TreatmentPlans_Id] in table 'OtherOrderTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_OtherOrderTreatmentPlan_TreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[OtherOrderTreatmentPlan] DROP CONSTRAINT FK_OtherOrderTreatmentPlan_TreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[OtherOrderTreatmentPlan]
ADD CONSTRAINT [FK_OtherOrderTreatmentPlan_TreatmentPlan]
    FOREIGN KEY ([TreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OtherOrderTreatmentPlan_TreatmentPlan'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_OtherOrderTreatmentPlan_TreatmentPlan')
	DROP INDEX IX_FK_OtherOrderTreatmentPlan_TreatmentPlan ON [model].[OtherOrderTreatmentPlan]
GO
CREATE INDEX [IX_FK_OtherOrderTreatmentPlan_TreatmentPlan]
ON [model].[OtherOrderTreatmentPlan]
    ([TreatmentPlans_Id]);
GO

-- Creating foreign key on [RefractivePrescriptions_Id] in table 'RefractivePrescriptionTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_RefractivePrescriptionTreatmentPlan_RefractivePrescription'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[RefractivePrescriptionTreatmentPlan] DROP CONSTRAINT FK_RefractivePrescriptionTreatmentPlan_RefractivePrescription

IF OBJECTPROPERTY(OBJECT_ID('[model].[RefractivePrescriptions]'), 'IsUserTable') = 1
ALTER TABLE [model].[RefractivePrescriptionTreatmentPlan]
ADD CONSTRAINT [FK_RefractivePrescriptionTreatmentPlan_RefractivePrescription]
    FOREIGN KEY ([RefractivePrescriptions_Id])
    REFERENCES [model].[RefractivePrescriptions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [UserTreatmentPlans_Id] in table 'RefractivePrescriptionTreatmentPlan'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_RefractivePrescriptionTreatmentPlan_TreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[RefractivePrescriptionTreatmentPlan] DROP CONSTRAINT FK_RefractivePrescriptionTreatmentPlan_TreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[RefractivePrescriptionTreatmentPlan]
ADD CONSTRAINT [FK_RefractivePrescriptionTreatmentPlan_TreatmentPlan]
    FOREIGN KEY ([UserTreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RefractivePrescriptionTreatmentPlan_TreatmentPlan'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_RefractivePrescriptionTreatmentPlan_TreatmentPlan')
	DROP INDEX IX_FK_RefractivePrescriptionTreatmentPlan_TreatmentPlan ON [model].[RefractivePrescriptionTreatmentPlan]
GO
CREATE INDEX [IX_FK_RefractivePrescriptionTreatmentPlan_TreatmentPlan]
ON [model].[RefractivePrescriptionTreatmentPlan]
    ([UserTreatmentPlans_Id]);
GO

-- Creating foreign key on [AppointmentTypeId] in table 'ProcedureOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ProcedureOrderAppointmentType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ProcedureOrders] DROP CONSTRAINT FK_ProcedureOrderAppointmentType

IF OBJECTPROPERTY(OBJECT_ID('[model].[AppointmentTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[ProcedureOrders]
ADD CONSTRAINT [FK_ProcedureOrderAppointmentType]
    FOREIGN KEY ([AppointmentTypeId])
    REFERENCES [model].[AppointmentTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProcedureOrderAppointmentType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ProcedureOrderAppointmentType')
	DROP INDEX IX_FK_ProcedureOrderAppointmentType ON [model].[ProcedureOrders]
GO
CREATE INDEX [IX_FK_ProcedureOrderAppointmentType]
ON [model].[ProcedureOrders]
    ([AppointmentTypeId]);
GO

-- Creating foreign key on [Medications_Id] in table 'MedicationMedicationFamily'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_MedicationMedicationFamily_Medication'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[MedicationMedicationFamily] DROP CONSTRAINT FK_MedicationMedicationFamily_Medication

IF OBJECTPROPERTY(OBJECT_ID('[model].[Medications]'), 'IsUserTable') = 1
ALTER TABLE [model].[MedicationMedicationFamily]
ADD CONSTRAINT [FK_MedicationMedicationFamily_Medication]
    FOREIGN KEY ([Medications_Id])
    REFERENCES [model].[Medications]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [MedicationFamilies_Id] in table 'MedicationMedicationFamily'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_MedicationMedicationFamily_MedicationFamily'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[MedicationMedicationFamily] DROP CONSTRAINT FK_MedicationMedicationFamily_MedicationFamily

IF OBJECTPROPERTY(OBJECT_ID('[model].[MedicationFamilies]'), 'IsUserTable') = 1
ALTER TABLE [model].[MedicationMedicationFamily]
ADD CONSTRAINT [FK_MedicationMedicationFamily_MedicationFamily]
    FOREIGN KEY ([MedicationFamilies_Id])
    REFERENCES [model].[MedicationFamilies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_MedicationMedicationFamily_MedicationFamily'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_MedicationMedicationFamily_MedicationFamily')
	DROP INDEX IX_FK_MedicationMedicationFamily_MedicationFamily ON [model].[MedicationMedicationFamily]
GO
CREATE INDEX [IX_FK_MedicationMedicationFamily_MedicationFamily]
ON [model].[MedicationMedicationFamily]
    ([MedicationFamilies_Id]);
GO

-- Creating foreign key on [UserTreatmentPlans_Id] in table 'TreatmentPlanTreatmentGoal'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentPlanTreatmentGoal_TreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentPlanTreatmentGoal] DROP CONSTRAINT FK_TreatmentPlanTreatmentGoal_TreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentPlanTreatmentGoal]
ADD CONSTRAINT [FK_TreatmentPlanTreatmentGoal_TreatmentPlan]
    FOREIGN KEY ([UserTreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TreatmentGoalAndInstructions_Id] in table 'TreatmentPlanTreatmentGoal'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentPlanTreatmentGoal_TreatmentGoal'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentPlanTreatmentGoal] DROP CONSTRAINT FK_TreatmentPlanTreatmentGoal_TreatmentGoal

IF OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentGoalAndInstructions]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentPlanTreatmentGoal]
ADD CONSTRAINT [FK_TreatmentPlanTreatmentGoal_TreatmentGoal]
    FOREIGN KEY ([TreatmentGoalAndInstructions_Id])
    REFERENCES [model].[TreatmentGoalAndInstructions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TreatmentPlanTreatmentGoal_TreatmentGoal'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_TreatmentPlanTreatmentGoal_TreatmentGoal')
	DROP INDEX IX_FK_TreatmentPlanTreatmentGoal_TreatmentGoal ON [model].[TreatmentPlanTreatmentGoal]
GO
CREATE INDEX [IX_FK_TreatmentPlanTreatmentGoal_TreatmentGoal]
ON [model].[TreatmentPlanTreatmentGoal]
    ([TreatmentGoalAndInstructions_Id]);
GO

-- Creating foreign key on [ProcedureOrders_Id] in table 'ProcedureOrderProcedurePerformed'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ProcedureOrderProcedurePerformed_ProcedureOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ProcedureOrderProcedurePerformed] DROP CONSTRAINT FK_ProcedureOrderProcedurePerformed_ProcedureOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[ProcedureOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[ProcedureOrderProcedurePerformed]
ADD CONSTRAINT [FK_ProcedureOrderProcedurePerformed_ProcedureOrder]
    FOREIGN KEY ([ProcedureOrders_Id])
    REFERENCES [model].[ProcedureOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ProcedurePerformeds_Id] in table 'ProcedureOrderProcedurePerformed'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ProcedureOrderProcedurePerformed_ProcedurePerformed'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ProcedureOrderProcedurePerformed] DROP CONSTRAINT FK_ProcedureOrderProcedurePerformed_ProcedurePerformed

IF OBJECTPROPERTY(OBJECT_ID('[model].[ProcedurePerformeds]'), 'IsUserTable') = 1
ALTER TABLE [model].[ProcedureOrderProcedurePerformed]
ADD CONSTRAINT [FK_ProcedureOrderProcedurePerformed_ProcedurePerformed]
    FOREIGN KEY ([ProcedurePerformeds_Id])
    REFERENCES [model].[ProcedurePerformeds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProcedureOrderProcedurePerformed_ProcedurePerformed'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ProcedureOrderProcedurePerformed_ProcedurePerformed')
	DROP INDEX IX_FK_ProcedureOrderProcedurePerformed_ProcedurePerformed ON [model].[ProcedureOrderProcedurePerformed]
GO
CREATE INDEX [IX_FK_ProcedureOrderProcedurePerformed_ProcedurePerformed]
ON [model].[ProcedureOrderProcedurePerformed]
    ([ProcedurePerformeds_Id]);
GO

-- Creating foreign key on [DiagnosticTestOrders_Id] in table 'DiagnosticTestOrderDiagnosticTestPerformed'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[DiagnosticTestOrderDiagnosticTestPerformed] DROP CONSTRAINT FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[DiagnosticTestOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[DiagnosticTestOrderDiagnosticTestPerformed]
ADD CONSTRAINT [FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestOrder]
    FOREIGN KEY ([DiagnosticTestOrders_Id])
    REFERENCES [model].[DiagnosticTestOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [DiagnosticTestPerformeds_Id] in table 'DiagnosticTestOrderDiagnosticTestPerformed'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestPerformed'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[DiagnosticTestOrderDiagnosticTestPerformed] DROP CONSTRAINT FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestPerformed

IF OBJECTPROPERTY(OBJECT_ID('[model].[DiagnosticTestPerformeds]'), 'IsUserTable') = 1
ALTER TABLE [model].[DiagnosticTestOrderDiagnosticTestPerformed]
ADD CONSTRAINT [FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestPerformed]
    FOREIGN KEY ([DiagnosticTestPerformeds_Id])
    REFERENCES [model].[DiagnosticTestPerformeds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestPerformed'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestPerformed')
	DROP INDEX IX_FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestPerformed ON [model].[DiagnosticTestOrderDiagnosticTestPerformed]
GO
CREATE INDEX [IX_FK_DiagnosticTestOrderDiagnosticTestPerformed_DiagnosticTestPerformed]
ON [model].[DiagnosticTestOrderDiagnosticTestPerformed]
    ([DiagnosticTestPerformeds_Id]);
GO

-- Creating foreign key on [LaboratoryTestOrders_Id] in table 'LaboratoryTestOrderLaboratoryTestResult'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[LaboratoryTestOrderLaboratoryTestResult] DROP CONSTRAINT FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTestOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[LaboratoryTestOrderLaboratoryTestResult]
ADD CONSTRAINT [FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestOrder]
    FOREIGN KEY ([LaboratoryTestOrders_Id])
    REFERENCES [model].[LaboratoryTestOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [LaboratoryTestResults_Id] in table 'LaboratoryTestOrderLaboratoryTestResult'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestResult'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[LaboratoryTestOrderLaboratoryTestResult] DROP CONSTRAINT FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestResult

IF OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTestResults]'), 'IsUserTable') = 1
ALTER TABLE [model].[LaboratoryTestOrderLaboratoryTestResult]
ADD CONSTRAINT [FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestResult]
    FOREIGN KEY ([LaboratoryTestResults_Id])
    REFERENCES [model].[LaboratoryTestResults]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestResult'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestResult')
	DROP INDEX IX_FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestResult ON [model].[LaboratoryTestOrderLaboratoryTestResult]
GO
CREATE INDEX [IX_FK_LaboratoryTestOrderLaboratoryTestResult_LaboratoryTestResult]
ON [model].[LaboratoryTestOrderLaboratoryTestResult]
    ([LaboratoryTestResults_Id]);
GO

-- Creating foreign key on [Medications_Id] in table 'MedicationClinicalSpecialtyType'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_MedicationClinicalSpecialtyType_Medication'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[MedicationClinicalSpecialtyType] DROP CONSTRAINT FK_MedicationClinicalSpecialtyType_Medication

IF OBJECTPROPERTY(OBJECT_ID('[model].[Medications]'), 'IsUserTable') = 1
ALTER TABLE [model].[MedicationClinicalSpecialtyType]
ADD CONSTRAINT [FK_MedicationClinicalSpecialtyType_Medication]
    FOREIGN KEY ([Medications_Id])
    REFERENCES [model].[Medications]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalSpecialtyTypes_Id] in table 'MedicationClinicalSpecialtyType'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_MedicationClinicalSpecialtyType_ClinicalSpecialtyType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[MedicationClinicalSpecialtyType] DROP CONSTRAINT FK_MedicationClinicalSpecialtyType_ClinicalSpecialtyType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalSpecialtyTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[MedicationClinicalSpecialtyType]
ADD CONSTRAINT [FK_MedicationClinicalSpecialtyType_ClinicalSpecialtyType]
    FOREIGN KEY ([ClinicalSpecialtyTypes_Id])
    REFERENCES [model].[ClinicalSpecialtyTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_MedicationClinicalSpecialtyType_ClinicalSpecialtyType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_MedicationClinicalSpecialtyType_ClinicalSpecialtyType')
	DROP INDEX IX_FK_MedicationClinicalSpecialtyType_ClinicalSpecialtyType ON [model].[MedicationClinicalSpecialtyType]
GO
CREATE INDEX [IX_FK_MedicationClinicalSpecialtyType_ClinicalSpecialtyType]
ON [model].[MedicationClinicalSpecialtyType]
    ([ClinicalSpecialtyTypes_Id]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterMedicationOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterMedicationOrderEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterMedicationOrders] DROP CONSTRAINT FK_EncounterMedicationOrderEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterMedicationOrders]
ADD CONSTRAINT [FK_EncounterMedicationOrderEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterMedicationOrderEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterMedicationOrderEncounter')
	DROP INDEX IX_FK_EncounterMedicationOrderEncounter ON [model].[EncounterMedicationOrders]
GO
CREATE INDEX [IX_FK_EncounterMedicationOrderEncounter]
ON [model].[EncounterMedicationOrders]
    ([EncounterId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterLaboratoryTestOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterLaboratoryTestOrderEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT FK_EncounterLaboratoryTestOrderEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterLaboratoryTestOrders]
ADD CONSTRAINT [FK_EncounterLaboratoryTestOrderEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterLaboratoryTestOrderEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterLaboratoryTestOrderEncounter')
	DROP INDEX IX_FK_EncounterLaboratoryTestOrderEncounter ON [model].[EncounterLaboratoryTestOrders]
GO
CREATE INDEX [IX_FK_EncounterLaboratoryTestOrderEncounter]
ON [model].[EncounterLaboratoryTestOrders]
    ([EncounterId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterCommunicationWithOtherProviderOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterCommunicationWithOtherProviderOrderEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders] DROP CONSTRAINT FK_EncounterCommunicationWithOtherProviderOrderEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders]
ADD CONSTRAINT [FK_EncounterCommunicationWithOtherProviderOrderEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterCommunicationWithOtherProviderOrderEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterCommunicationWithOtherProviderOrderEncounter')
	DROP INDEX IX_FK_EncounterCommunicationWithOtherProviderOrderEncounter ON [model].[EncounterCommunicationWithOtherProviderOrders]
GO
CREATE INDEX [IX_FK_EncounterCommunicationWithOtherProviderOrderEncounter]
ON [model].[EncounterCommunicationWithOtherProviderOrders]
    ([EncounterId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterProcedureOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterProcedureOrderEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT FK_EncounterProcedureOrderEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterProcedureOrders]
ADD CONSTRAINT [FK_EncounterProcedureOrderEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterProcedureOrderEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterProcedureOrderEncounter')
	DROP INDEX IX_FK_EncounterProcedureOrderEncounter ON [model].[EncounterProcedureOrders]
GO
CREATE INDEX [IX_FK_EncounterProcedureOrderEncounter]
ON [model].[EncounterProcedureOrders]
    ([EncounterId]);
GO

-- Creating foreign key on [CommunicationWithOtherProviderOrderId] in table 'EncounterCommunicationWithOtherProviderOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterCommunicationWithOtherProviderOrderCommunicationWithOtherProviderOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders] DROP CONSTRAINT FK_EncounterCommunicationWithOtherProviderOrderCommunicationWithOtherProviderOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[CommunicationWithOtherProviderOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders]
ADD CONSTRAINT [FK_EncounterCommunicationWithOtherProviderOrderCommunicationWithOtherProviderOrder]
    FOREIGN KEY ([CommunicationWithOtherProviderOrderId])
    REFERENCES [model].[CommunicationWithOtherProviderOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterCommunicationWithOtherProviderOrderCommunicationWithOtherProviderOrder'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterCommunicationWithOtherProviderOrderCommunicationWithOtherProviderOrder')
	DROP INDEX IX_FK_EncounterCommunicationWithOtherProviderOrderCommunicationWithOtherProviderOrder ON [model].[EncounterCommunicationWithOtherProviderOrders]
GO
CREATE INDEX [IX_FK_EncounterCommunicationWithOtherProviderOrderCommunicationWithOtherProviderOrder]
ON [model].[EncounterCommunicationWithOtherProviderOrders]
    ([CommunicationWithOtherProviderOrderId]);
GO

-- Creating foreign key on [EncounterId] in table 'PatientSpecialExamPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientSpecialExamPerformedEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientSpecialExamPerformeds] DROP CONSTRAINT FK_PatientSpecialExamPerformedEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientSpecialExamPerformeds]
ADD CONSTRAINT [FK_PatientSpecialExamPerformedEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSpecialExamPerformedEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientSpecialExamPerformedEncounter')
	DROP INDEX IX_FK_PatientSpecialExamPerformedEncounter ON [model].[PatientSpecialExamPerformeds]
GO
CREATE INDEX [IX_FK_PatientSpecialExamPerformedEncounter]
ON [model].[PatientSpecialExamPerformeds]
    ([EncounterId]);
GO

-- Creating foreign key on [EncounterId] in table 'PatientOtherElementPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientOtherElementPerformedEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientOtherElementPerformeds] DROP CONSTRAINT FK_PatientOtherElementPerformedEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientOtherElementPerformeds]
ADD CONSTRAINT [FK_PatientOtherElementPerformedEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientOtherElementPerformedEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientOtherElementPerformedEncounter')
	DROP INDEX IX_FK_PatientOtherElementPerformedEncounter ON [model].[PatientOtherElementPerformeds]
GO
CREATE INDEX [IX_FK_PatientOtherElementPerformedEncounter]
ON [model].[PatientOtherElementPerformeds]
    ([EncounterId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterTreatmentPlanComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterTreatmentPlanCommentEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterTreatmentPlanComments] DROP CONSTRAINT FK_EncounterTreatmentPlanCommentEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterTreatmentPlanComments]
ADD CONSTRAINT [FK_EncounterTreatmentPlanCommentEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterTreatmentPlanCommentEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterTreatmentPlanCommentEncounter')
	DROP INDEX IX_FK_EncounterTreatmentPlanCommentEncounter ON [model].[EncounterTreatmentPlanComments]
GO
CREATE INDEX [IX_FK_EncounterTreatmentPlanCommentEncounter]
ON [model].[EncounterTreatmentPlanComments]
    ([EncounterId]);
GO

-- Creating foreign key on [LaboratoryTestOrderId] in table 'EncounterLaboratoryTestOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterLaboratoryTestOrderLaboratoryTestOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT FK_EncounterLaboratoryTestOrderLaboratoryTestOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTestOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterLaboratoryTestOrders]
ADD CONSTRAINT [FK_EncounterLaboratoryTestOrderLaboratoryTestOrder]
    FOREIGN KEY ([LaboratoryTestOrderId])
    REFERENCES [model].[LaboratoryTestOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterLaboratoryTestOrderLaboratoryTestOrder'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterLaboratoryTestOrderLaboratoryTestOrder')
	DROP INDEX IX_FK_EncounterLaboratoryTestOrderLaboratoryTestOrder ON [model].[EncounterLaboratoryTestOrders]
GO
CREATE INDEX [IX_FK_EncounterLaboratoryTestOrderLaboratoryTestOrder]
ON [model].[EncounterLaboratoryTestOrders]
    ([LaboratoryTestOrderId]);
GO

-- Creating foreign key on [ExternalOrganizationId] in table 'EncounterLaboratoryTestOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterLaboratoryTestOrderExternalOrganization'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT FK_EncounterLaboratoryTestOrderExternalOrganization

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalOrganizations]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterLaboratoryTestOrders]
ADD CONSTRAINT [FK_EncounterLaboratoryTestOrderExternalOrganization]
    FOREIGN KEY ([ExternalOrganizationId])
    REFERENCES [model].[ExternalOrganizations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterLaboratoryTestOrderExternalOrganization'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterLaboratoryTestOrderExternalOrganization')
	DROP INDEX IX_FK_EncounterLaboratoryTestOrderExternalOrganization ON [model].[EncounterLaboratoryTestOrders]
GO
CREATE INDEX [IX_FK_EncounterLaboratoryTestOrderExternalOrganization]
ON [model].[EncounterLaboratoryTestOrders]
    ([ExternalOrganizationId]);
GO

-- Creating foreign key on [ProcedureOrderId] in table 'EncounterProcedureOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterProcedureOrderProcedureOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT FK_EncounterProcedureOrderProcedureOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[ProcedureOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterProcedureOrders]
ADD CONSTRAINT [FK_EncounterProcedureOrderProcedureOrder]
    FOREIGN KEY ([ProcedureOrderId])
    REFERENCES [model].[ProcedureOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterProcedureOrderProcedureOrder'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterProcedureOrderProcedureOrder')
	DROP INDEX IX_FK_EncounterProcedureOrderProcedureOrder ON [model].[EncounterProcedureOrders]
GO
CREATE INDEX [IX_FK_EncounterProcedureOrderProcedureOrder]
ON [model].[EncounterProcedureOrders]
    ([ProcedureOrderId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterRefractivePrescriptions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterRefractivePrescriptionEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterRefractivePrescriptions] DROP CONSTRAINT FK_EncounterRefractivePrescriptionEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterRefractivePrescriptions]
ADD CONSTRAINT [FK_EncounterRefractivePrescriptionEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterRefractivePrescriptionEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterRefractivePrescriptionEncounter')
	DROP INDEX IX_FK_EncounterRefractivePrescriptionEncounter ON [model].[EncounterRefractivePrescriptions]
GO
CREATE INDEX [IX_FK_EncounterRefractivePrescriptionEncounter]
ON [model].[EncounterRefractivePrescriptions]
    ([EncounterId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterOtherTreatmentPlanOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterOtherTreatmentPlanOrderEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterOtherTreatmentPlanOrders] DROP CONSTRAINT FK_EncounterOtherTreatmentPlanOrderEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterOtherTreatmentPlanOrders]
ADD CONSTRAINT [FK_EncounterOtherTreatmentPlanOrderEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterOtherTreatmentPlanOrderEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterOtherTreatmentPlanOrderEncounter')
	DROP INDEX IX_FK_EncounterOtherTreatmentPlanOrderEncounter ON [model].[EncounterOtherTreatmentPlanOrders]
GO
CREATE INDEX [IX_FK_EncounterOtherTreatmentPlanOrderEncounter]
ON [model].[EncounterOtherTreatmentPlanOrders]
    ([EncounterId]);
GO

-- Creating foreign key on [TemplateDocumentId] in table 'CommunicationWithOtherProviderOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_CommunicationWithOtherProviderOrderTemplateDocument'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[CommunicationWithOtherProviderOrders] DROP CONSTRAINT FK_CommunicationWithOtherProviderOrderTemplateDocument

IF OBJECTPROPERTY(OBJECT_ID('[model].[TemplateDocuments]'), 'IsUserTable') = 1
ALTER TABLE [model].[CommunicationWithOtherProviderOrders]
ADD CONSTRAINT [FK_CommunicationWithOtherProviderOrderTemplateDocument]
    FOREIGN KEY ([TemplateDocumentId])
    REFERENCES [model].[TemplateDocuments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CommunicationWithOtherProviderOrderTemplateDocument'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_CommunicationWithOtherProviderOrderTemplateDocument')
	DROP INDEX IX_FK_CommunicationWithOtherProviderOrderTemplateDocument ON [model].[CommunicationWithOtherProviderOrders]
GO
CREATE INDEX [IX_FK_CommunicationWithOtherProviderOrderTemplateDocument]
ON [model].[CommunicationWithOtherProviderOrders]
    ([TemplateDocumentId]);
GO

-- Creating foreign key on [MedicationId] in table 'MedicationOrderFavorites'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_MedicationOrderFavoriteMedication'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT FK_MedicationOrderFavoriteMedication

IF OBJECTPROPERTY(OBJECT_ID('[model].[Medications]'), 'IsUserTable') = 1
ALTER TABLE [model].[MedicationOrderFavorites]
ADD CONSTRAINT [FK_MedicationOrderFavoriteMedication]
    FOREIGN KEY ([MedicationId])
    REFERENCES [model].[Medications]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_MedicationOrderFavoriteMedication'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_MedicationOrderFavoriteMedication')
	DROP INDEX IX_FK_MedicationOrderFavoriteMedication ON [model].[MedicationOrderFavorites]
GO
CREATE INDEX [IX_FK_MedicationOrderFavoriteMedication]
ON [model].[MedicationOrderFavorites]
    ([MedicationId]);
GO

-- Creating foreign key on [TreatmentPlans_Id] in table 'TreatmentPlanMedicationOrderFavorite'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentPlanMedicationOrderFavorite_TreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentPlanMedicationOrderFavorite] DROP CONSTRAINT FK_TreatmentPlanMedicationOrderFavorite_TreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentPlanMedicationOrderFavorite]
ADD CONSTRAINT [FK_TreatmentPlanMedicationOrderFavorite_TreatmentPlan]
    FOREIGN KEY ([TreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [MedicationOrderFavorites_Id] in table 'TreatmentPlanMedicationOrderFavorite'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentPlanMedicationOrderFavorite_MedicationOrderFavorite'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentPlanMedicationOrderFavorite] DROP CONSTRAINT FK_TreatmentPlanMedicationOrderFavorite_MedicationOrderFavorite

IF OBJECTPROPERTY(OBJECT_ID('[model].[MedicationOrderFavorites]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentPlanMedicationOrderFavorite]
ADD CONSTRAINT [FK_TreatmentPlanMedicationOrderFavorite_MedicationOrderFavorite]
    FOREIGN KEY ([MedicationOrderFavorites_Id])
    REFERENCES [model].[MedicationOrderFavorites]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TreatmentPlanMedicationOrderFavorite_MedicationOrderFavorite'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_TreatmentPlanMedicationOrderFavorite_MedicationOrderFavorite')
	DROP INDEX IX_FK_TreatmentPlanMedicationOrderFavorite_MedicationOrderFavorite ON [model].[TreatmentPlanMedicationOrderFavorite]
GO
CREATE INDEX [IX_FK_TreatmentPlanMedicationOrderFavorite_MedicationOrderFavorite]
ON [model].[TreatmentPlanMedicationOrderFavorite]
    ([MedicationOrderFavorites_Id]);
GO

-- Creating foreign key on [OrganStructureGroupId] in table 'OrganStructures'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_OrganStructureGroupOrganStructure'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[OrganStructures] DROP CONSTRAINT FK_OrganStructureGroupOrganStructure

IF OBJECTPROPERTY(OBJECT_ID('[model].[OrganStructureGroups]'), 'IsUserTable') = 1
ALTER TABLE [model].[OrganStructures]
ADD CONSTRAINT [FK_OrganStructureGroupOrganStructure]
    FOREIGN KEY ([OrganStructureGroupId])
    REFERENCES [model].[OrganStructureGroups]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OrganStructureGroupOrganStructure'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_OrganStructureGroupOrganStructure')
	DROP INDEX IX_FK_OrganStructureGroupOrganStructure ON [model].[OrganStructures]
GO
CREATE INDEX [IX_FK_OrganStructureGroupOrganStructure]
ON [model].[OrganStructures]
    ([OrganStructureGroupId]);
GO

-- Creating foreign key on [BodyParts_Id] in table 'BodyPartClinicalSpecialtyType'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BodyPartClinicalSpecialtyType_BodyPart'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BodyPartClinicalSpecialtyType] DROP CONSTRAINT FK_BodyPartClinicalSpecialtyType_BodyPart

IF OBJECTPROPERTY(OBJECT_ID('[model].[BodyParts]'), 'IsUserTable') = 1
ALTER TABLE [model].[BodyPartClinicalSpecialtyType]
ADD CONSTRAINT [FK_BodyPartClinicalSpecialtyType_BodyPart]
    FOREIGN KEY ([BodyParts_Id])
    REFERENCES [model].[BodyParts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalSpecialtyTypes_Id] in table 'BodyPartClinicalSpecialtyType'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BodyPartClinicalSpecialtyType_ClinicalSpecialtyType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BodyPartClinicalSpecialtyType] DROP CONSTRAINT FK_BodyPartClinicalSpecialtyType_ClinicalSpecialtyType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalSpecialtyTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[BodyPartClinicalSpecialtyType]
ADD CONSTRAINT [FK_BodyPartClinicalSpecialtyType_ClinicalSpecialtyType]
    FOREIGN KEY ([ClinicalSpecialtyTypes_Id])
    REFERENCES [model].[ClinicalSpecialtyTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BodyPartClinicalSpecialtyType_ClinicalSpecialtyType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_BodyPartClinicalSpecialtyType_ClinicalSpecialtyType')
	DROP INDEX IX_FK_BodyPartClinicalSpecialtyType_ClinicalSpecialtyType ON [model].[BodyPartClinicalSpecialtyType]
GO
CREATE INDEX [IX_FK_BodyPartClinicalSpecialtyType_ClinicalSpecialtyType]
ON [model].[BodyPartClinicalSpecialtyType]
    ([ClinicalSpecialtyTypes_Id]);
GO

-- Creating foreign key on [BodySystemId] in table 'Organs'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_OrganBodySystem'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[Organs] DROP CONSTRAINT FK_OrganBodySystem

IF OBJECTPROPERTY(OBJECT_ID('[model].[BodySystems]'), 'IsUserTable') = 1
ALTER TABLE [model].[Organs]
ADD CONSTRAINT [FK_OrganBodySystem]
    FOREIGN KEY ([BodySystemId])
    REFERENCES [model].[BodySystems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OrganBodySystem'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_OrganBodySystem')
	DROP INDEX IX_FK_OrganBodySystem ON [model].[Organs]
GO
CREATE INDEX [IX_FK_OrganBodySystem]
ON [model].[Organs]
    ([BodySystemId]);
GO

-- Creating foreign key on [TimeQualifierId] in table 'EncounterProcedureOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterProcedureOrderTimeQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT FK_EncounterProcedureOrderTimeQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[TimeQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterProcedureOrders]
ADD CONSTRAINT [FK_EncounterProcedureOrderTimeQualifier]
    FOREIGN KEY ([TimeQualifierId])
    REFERENCES [model].[TimeQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterProcedureOrderTimeQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterProcedureOrderTimeQualifier')
	DROP INDEX IX_FK_EncounterProcedureOrderTimeQualifier ON [model].[EncounterProcedureOrders]
GO
CREATE INDEX [IX_FK_EncounterProcedureOrderTimeQualifier]
ON [model].[EncounterProcedureOrders]
    ([TimeQualifierId]);
GO

-- Creating foreign key on [ServiceLocationId] in table 'EncounterProcedureOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterProcedureOrderServiceLocation'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT FK_EncounterProcedureOrderServiceLocation

IF OBJECTPROPERTY(OBJECT_ID('[model].[ServiceLocations]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterProcedureOrders]
ADD CONSTRAINT [FK_EncounterProcedureOrderServiceLocation]
    FOREIGN KEY ([ServiceLocationId])
    REFERENCES [model].[ServiceLocations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterProcedureOrderServiceLocation'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterProcedureOrderServiceLocation')
	DROP INDEX IX_FK_EncounterProcedureOrderServiceLocation ON [model].[EncounterProcedureOrders]
GO
CREATE INDEX [IX_FK_EncounterProcedureOrderServiceLocation]
ON [model].[EncounterProcedureOrders]
    ([ServiceLocationId]);
GO

-- Creating foreign key on [PatientMedicationId] in table 'PatientMedicationProgressComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientMedicationProgressCommentPatientMedication'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientMedicationProgressComments] DROP CONSTRAINT FK_PatientMedicationProgressCommentPatientMedication

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientMedications]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientMedicationProgressComments]
ADD CONSTRAINT [FK_PatientMedicationProgressCommentPatientMedication]
    FOREIGN KEY ([PatientMedicationId])
    REFERENCES [model].[PatientMedications]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientMedicationProgressCommentPatientMedication'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientMedicationProgressCommentPatientMedication')
	DROP INDEX IX_FK_PatientMedicationProgressCommentPatientMedication ON [model].[PatientMedicationProgressComments]
GO
CREATE INDEX [IX_FK_PatientMedicationProgressCommentPatientMedication]
ON [model].[PatientMedicationProgressComments]
    ([PatientMedicationId]);
GO

-- Creating foreign key on [DrugDispenseFormId] in table 'MedicationOrderFavorites'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_MedicationOrderFavoriteDrugDispenseForm'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT FK_MedicationOrderFavoriteDrugDispenseForm

IF OBJECTPROPERTY(OBJECT_ID('[model].[DrugDispenseForms]'), 'IsUserTable') = 1
ALTER TABLE [model].[MedicationOrderFavorites]
ADD CONSTRAINT [FK_MedicationOrderFavoriteDrugDispenseForm]
    FOREIGN KEY ([DrugDispenseFormId])
    REFERENCES [model].[DrugDispenseForms]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_MedicationOrderFavoriteDrugDispenseForm'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_MedicationOrderFavoriteDrugDispenseForm')
	DROP INDEX IX_FK_MedicationOrderFavoriteDrugDispenseForm ON [model].[MedicationOrderFavorites]
GO
CREATE INDEX [IX_FK_MedicationOrderFavoriteDrugDispenseForm]
ON [model].[MedicationOrderFavorites]
    ([DrugDispenseFormId]);
GO

-- Creating foreign key on [DrugDosageNumberId] in table 'MedicationOrderFavorites'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_MedicationOrderFavoriteDrugDosageNumber'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT FK_MedicationOrderFavoriteDrugDosageNumber

IF OBJECTPROPERTY(OBJECT_ID('[model].[DrugDosageNumbers]'), 'IsUserTable') = 1
ALTER TABLE [model].[MedicationOrderFavorites]
ADD CONSTRAINT [FK_MedicationOrderFavoriteDrugDosageNumber]
    FOREIGN KEY ([DrugDosageNumberId])
    REFERENCES [model].[DrugDosageNumbers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_MedicationOrderFavoriteDrugDosageNumber'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_MedicationOrderFavoriteDrugDosageNumber')
	DROP INDEX IX_FK_MedicationOrderFavoriteDrugDosageNumber ON [model].[MedicationOrderFavorites]
GO
CREATE INDEX [IX_FK_MedicationOrderFavoriteDrugDosageNumber]
ON [model].[MedicationOrderFavorites]
    ([DrugDosageNumberId]);
GO

-- Creating foreign key on [FrequencyId] in table 'MedicationOrderFavorites'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_MedicationOrderFavoriteFrequency'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT FK_MedicationOrderFavoriteFrequency

IF OBJECTPROPERTY(OBJECT_ID('[model].[Frequencies]'), 'IsUserTable') = 1
ALTER TABLE [model].[MedicationOrderFavorites]
ADD CONSTRAINT [FK_MedicationOrderFavoriteFrequency]
    FOREIGN KEY ([FrequencyId])
    REFERENCES [model].[Frequencies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_MedicationOrderFavoriteFrequency'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_MedicationOrderFavoriteFrequency')
	DROP INDEX IX_FK_MedicationOrderFavoriteFrequency ON [model].[MedicationOrderFavorites]
GO
CREATE INDEX [IX_FK_MedicationOrderFavoriteFrequency]
ON [model].[MedicationOrderFavorites]
    ([FrequencyId]);
GO

-- Creating foreign key on [RouteOfAdministrationId] in table 'MedicationOrderFavorites'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_MedicationOrderFavoriteRouteOfAdministration'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT FK_MedicationOrderFavoriteRouteOfAdministration

IF OBJECTPROPERTY(OBJECT_ID('[model].[RouteOfAdministrations]'), 'IsUserTable') = 1
ALTER TABLE [model].[MedicationOrderFavorites]
ADD CONSTRAINT [FK_MedicationOrderFavoriteRouteOfAdministration]
    FOREIGN KEY ([RouteOfAdministrationId])
    REFERENCES [model].[RouteOfAdministrations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_MedicationOrderFavoriteRouteOfAdministration'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_MedicationOrderFavoriteRouteOfAdministration')
	DROP INDEX IX_FK_MedicationOrderFavoriteRouteOfAdministration ON [model].[MedicationOrderFavorites]
GO
CREATE INDEX [IX_FK_MedicationOrderFavoriteRouteOfAdministration]
ON [model].[MedicationOrderFavorites]
    ([RouteOfAdministrationId]);
GO

-- Creating foreign key on [PatientId] in table 'PatientDiagnoses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisPatient'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnoses] DROP CONSTRAINT FK_PatientDiagnosisPatient

IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnoses]
ADD CONSTRAINT [FK_PatientDiagnosisPatient]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisPatient'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisPatient')
	DROP INDEX IX_FK_PatientDiagnosisPatient ON [model].[PatientDiagnoses]
GO
CREATE INDEX [IX_FK_PatientDiagnosisPatient]
ON [model].[PatientDiagnoses]
    ([PatientId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterClinicalDrawings'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterClinicalDrawingEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterClinicalDrawings] DROP CONSTRAINT FK_EncounterClinicalDrawingEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterClinicalDrawings]
ADD CONSTRAINT [FK_EncounterClinicalDrawingEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterClinicalDrawingEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterClinicalDrawingEncounter')
	DROP INDEX IX_FK_EncounterClinicalDrawingEncounter ON [model].[EncounterClinicalDrawings]
GO
CREATE INDEX [IX_FK_EncounterClinicalDrawingEncounter]
ON [model].[EncounterClinicalDrawings]
    ([EncounterId]);
GO

-- Creating foreign key on [OrganId] in table 'BodyParts'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BodyPartOrgan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BodyParts] DROP CONSTRAINT FK_BodyPartOrgan

IF OBJECTPROPERTY(OBJECT_ID('[model].[Organs]'), 'IsUserTable') = 1
ALTER TABLE [model].[BodyParts]
ADD CONSTRAINT [FK_BodyPartOrgan]
    FOREIGN KEY ([OrganId])
    REFERENCES [model].[Organs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BodyPartOrgan'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_BodyPartOrgan')
	DROP INDEX IX_FK_BodyPartOrgan ON [model].[BodyParts]
GO
CREATE INDEX [IX_FK_BodyPartOrgan]
ON [model].[BodyParts]
    ([OrganId]);
GO

-- Creating foreign key on [BodyPartId] in table 'BodyLocations'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BodyLocationBodyPart'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BodyLocations] DROP CONSTRAINT FK_BodyLocationBodyPart

IF OBJECTPROPERTY(OBJECT_ID('[model].[BodyParts]'), 'IsUserTable') = 1
ALTER TABLE [model].[BodyLocations]
ADD CONSTRAINT [FK_BodyLocationBodyPart]
    FOREIGN KEY ([BodyPartId])
    REFERENCES [model].[BodyParts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BodyLocationBodyPart'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_BodyLocationBodyPart')
	DROP INDEX IX_FK_BodyLocationBodyPart ON [model].[BodyLocations]
GO
CREATE INDEX [IX_FK_BodyLocationBodyPart]
ON [model].[BodyLocations]
    ([BodyPartId]);
GO

-- Creating foreign key on [OrganStructureId] in table 'BodyLocations'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BodyLocationOrganStructure'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BodyLocations] DROP CONSTRAINT FK_BodyLocationOrganStructure

IF OBJECTPROPERTY(OBJECT_ID('[model].[OrganStructures]'), 'IsUserTable') = 1
ALTER TABLE [model].[BodyLocations]
ADD CONSTRAINT [FK_BodyLocationOrganStructure]
    FOREIGN KEY ([OrganStructureId])
    REFERENCES [model].[OrganStructures]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BodyLocationOrganStructure'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_BodyLocationOrganStructure')
	DROP INDEX IX_FK_BodyLocationOrganStructure ON [model].[BodyLocations]
GO
CREATE INDEX [IX_FK_BodyLocationOrganStructure]
ON [model].[BodyLocations]
    ([OrganStructureId]);
GO

-- Creating foreign key on [OrganStructureId] in table 'OrganSubStructures'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_OrganSubStructureOrganStructure'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[OrganSubStructures] DROP CONSTRAINT FK_OrganSubStructureOrganStructure

IF OBJECTPROPERTY(OBJECT_ID('[model].[OrganStructures]'), 'IsUserTable') = 1
ALTER TABLE [model].[OrganSubStructures]
ADD CONSTRAINT [FK_OrganSubStructureOrganStructure]
    FOREIGN KEY ([OrganStructureId])
    REFERENCES [model].[OrganStructures]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OrganSubStructureOrganStructure'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_OrganSubStructureOrganStructure')
	DROP INDEX IX_FK_OrganSubStructureOrganStructure ON [model].[OrganSubStructures]
GO
CREATE INDEX [IX_FK_OrganSubStructureOrganStructure]
ON [model].[OrganSubStructures]
    ([OrganStructureId]);
GO

-- Creating foreign key on [AppointmentType_Id] in table 'SubsequentVisitOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_SubsequentVisitOrderAppointmentType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[SubsequentVisitOrders] DROP CONSTRAINT FK_SubsequentVisitOrderAppointmentType

IF OBJECTPROPERTY(OBJECT_ID('[model].[AppointmentTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[SubsequentVisitOrders]
ADD CONSTRAINT [FK_SubsequentVisitOrderAppointmentType]
    FOREIGN KEY ([AppointmentType_Id])
    REFERENCES [model].[AppointmentTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SubsequentVisitOrderAppointmentType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_SubsequentVisitOrderAppointmentType')
	DROP INDEX IX_FK_SubsequentVisitOrderAppointmentType ON [model].[SubsequentVisitOrders]
GO
CREATE INDEX [IX_FK_SubsequentVisitOrderAppointmentType]
ON [model].[SubsequentVisitOrders]
    ([AppointmentType_Id]);
GO

-- Creating foreign key on [UserId] in table 'AllergenOccurrences'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_AllergenOccurrenceUser'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[AllergenOccurrences] DROP CONSTRAINT FK_AllergenOccurrenceUser

IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[AllergenOccurrences]
ADD CONSTRAINT [FK_AllergenOccurrenceUser]
    FOREIGN KEY ([UserId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AllergenOccurrenceUser'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_AllergenOccurrenceUser')
	DROP INDEX IX_FK_AllergenOccurrenceUser ON [model].[AllergenOccurrences]
GO
CREATE INDEX [IX_FK_AllergenOccurrenceUser]
ON [model].[AllergenOccurrences]
    ([UserId]);
GO

-- Creating foreign key on [PatientDiagnosisId] in table 'PatientDiagnosisDetails'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisDetailPatientDiagnosis'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT FK_PatientDiagnosisDetailPatientDiagnosis

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnoses]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailPatientDiagnosis]
    FOREIGN KEY ([PatientDiagnosisId])
    REFERENCES [model].[PatientDiagnoses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailPatientDiagnosis'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisDetailPatientDiagnosis')
	DROP INDEX IX_FK_PatientDiagnosisDetailPatientDiagnosis ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailPatientDiagnosis]
ON [model].[PatientDiagnosisDetails]
    ([PatientDiagnosisId]);
GO

-- Creating foreign key on [UserId] in table 'PatientDiagnosisDetails'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisDetailUser'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT FK_PatientDiagnosisDetailUser

IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailUser]
    FOREIGN KEY ([UserId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailUser'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisDetailUser')
	DROP INDEX IX_FK_PatientDiagnosisDetailUser ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailUser]
ON [model].[PatientDiagnosisDetails]
    ([UserId]);
GO

-- Creating foreign key on [PatientId] in table 'PatientDiagnosisComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisCommentPatient'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT FK_PatientDiagnosisCommentPatient

IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisComments]
ADD CONSTRAINT [FK_PatientDiagnosisCommentPatient]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisCommentPatient'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisCommentPatient')
	DROP INDEX IX_FK_PatientDiagnosisCommentPatient ON [model].[PatientDiagnosisComments]
GO
CREATE INDEX [IX_FK_PatientDiagnosisCommentPatient]
ON [model].[PatientDiagnosisComments]
    ([PatientId]);
GO

-- Creating foreign key on [ClinicalDrawingTemplateId] in table 'EncounterClinicalDrawings'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterClinicalDrawingClinicalDrawingTemplate'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterClinicalDrawings] DROP CONSTRAINT FK_EncounterClinicalDrawingClinicalDrawingTemplate

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalDrawingTemplates]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterClinicalDrawings]
ADD CONSTRAINT [FK_EncounterClinicalDrawingClinicalDrawingTemplate]
    FOREIGN KEY ([ClinicalDrawingTemplateId])
    REFERENCES [model].[ClinicalDrawingTemplates]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterClinicalDrawingClinicalDrawingTemplate'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterClinicalDrawingClinicalDrawingTemplate')
	DROP INDEX IX_FK_EncounterClinicalDrawingClinicalDrawingTemplate ON [model].[EncounterClinicalDrawings]
GO
CREATE INDEX [IX_FK_EncounterClinicalDrawingClinicalDrawingTemplate]
ON [model].[EncounterClinicalDrawings]
    ([ClinicalDrawingTemplateId]);
GO

-- Creating foreign key on [UserId] in table 'UserTreatmentPlans'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentPlanUser'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[UserTreatmentPlans] DROP CONSTRAINT FK_TreatmentPlanUser

IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[UserTreatmentPlans]
ADD CONSTRAINT [FK_TreatmentPlanUser]
    FOREIGN KEY ([UserId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TreatmentPlanUser'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_TreatmentPlanUser')
	DROP INDEX IX_FK_TreatmentPlanUser ON [model].[UserTreatmentPlans]
GO
CREATE INDEX [IX_FK_TreatmentPlanUser]
ON [model].[UserTreatmentPlans]
    ([UserId]);
GO

-- Creating foreign key on [ClinicalDataSourceTypeId] in table 'PatientDiagnosisComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisCommentClinicalDataSourceType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT FK_PatientDiagnosisCommentClinicalDataSourceType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalDataSourceTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisComments]
ADD CONSTRAINT [FK_PatientDiagnosisCommentClinicalDataSourceType]
    FOREIGN KEY ([ClinicalDataSourceTypeId])
    REFERENCES [model].[ClinicalDataSourceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisCommentClinicalDataSourceType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisCommentClinicalDataSourceType')
	DROP INDEX IX_FK_PatientDiagnosisCommentClinicalDataSourceType ON [model].[PatientDiagnosisComments]
GO
CREATE INDEX [IX_FK_PatientDiagnosisCommentClinicalDataSourceType]
ON [model].[PatientDiagnosisComments]
    ([ClinicalDataSourceTypeId]);
GO

-- Creating foreign key on [UserId] in table 'PatientDiagnosisComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisCommentUser'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT FK_PatientDiagnosisCommentUser

IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisComments]
ADD CONSTRAINT [FK_PatientDiagnosisCommentUser]
    FOREIGN KEY ([UserId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisCommentUser'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisCommentUser')
	DROP INDEX IX_FK_PatientDiagnosisCommentUser ON [model].[PatientDiagnosisComments]
GO
CREATE INDEX [IX_FK_PatientDiagnosisCommentUser]
ON [model].[PatientDiagnosisComments]
    ([UserId]);
GO

-- Creating foreign key on [PatientId] in table 'PatientAllergenComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientAllergenCommentPatient'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientAllergenComments] DROP CONSTRAINT FK_PatientAllergenCommentPatient

IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientAllergenComments]
ADD CONSTRAINT [FK_PatientAllergenCommentPatient]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientAllergenCommentPatient'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientAllergenCommentPatient')
	DROP INDEX IX_FK_PatientAllergenCommentPatient ON [model].[PatientAllergenComments]
GO
CREATE INDEX [IX_FK_PatientAllergenCommentPatient]
ON [model].[PatientAllergenComments]
    ([PatientId]);
GO

-- Creating foreign key on [UserId] in table 'PatientAllergenComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientAllergenCommentUser'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientAllergenComments] DROP CONSTRAINT FK_PatientAllergenCommentUser

IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientAllergenComments]
ADD CONSTRAINT [FK_PatientAllergenCommentUser]
    FOREIGN KEY ([UserId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientAllergenCommentUser'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientAllergenCommentUser')
	DROP INDEX IX_FK_PatientAllergenCommentUser ON [model].[PatientAllergenComments]
GO
CREATE INDEX [IX_FK_PatientAllergenCommentUser]
ON [model].[PatientAllergenComments]
    ([UserId]);
GO

-- Creating foreign key on [ClinicalDataSourceTypeId] in table 'PatientAllergenComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientAllergenCommentClinicalDataSourceType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientAllergenComments] DROP CONSTRAINT FK_PatientAllergenCommentClinicalDataSourceType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalDataSourceTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientAllergenComments]
ADD CONSTRAINT [FK_PatientAllergenCommentClinicalDataSourceType]
    FOREIGN KEY ([ClinicalDataSourceTypeId])
    REFERENCES [model].[ClinicalDataSourceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientAllergenCommentClinicalDataSourceType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientAllergenCommentClinicalDataSourceType')
	DROP INDEX IX_FK_PatientAllergenCommentClinicalDataSourceType ON [model].[PatientAllergenComments]
GO
CREATE INDEX [IX_FK_PatientAllergenCommentClinicalDataSourceType]
ON [model].[PatientAllergenComments]
    ([ClinicalDataSourceTypeId]);
GO

-- Creating foreign key on [PatientId] in table 'PatientMedicalReviews'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientMedicalReviewPatient'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientMedicalReviews] DROP CONSTRAINT FK_PatientMedicalReviewPatient

IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientMedicalReviews]
ADD CONSTRAINT [FK_PatientMedicalReviewPatient]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientMedicalReviewPatient'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientMedicalReviewPatient')
	DROP INDEX IX_FK_PatientMedicalReviewPatient ON [model].[PatientMedicalReviews]
GO
CREATE INDEX [IX_FK_PatientMedicalReviewPatient]
ON [model].[PatientMedicalReviews]
    ([PatientId]);
GO

-- Creating foreign key on [ClinicalHistoryReviewTypeId] in table 'PatientMedicalReviews'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientMedicalReviewClinicalHistoryReviewType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientMedicalReviews] DROP CONSTRAINT FK_PatientMedicalReviewClinicalHistoryReviewType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalHistoryReviewTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientMedicalReviews]
ADD CONSTRAINT [FK_PatientMedicalReviewClinicalHistoryReviewType]
    FOREIGN KEY ([ClinicalHistoryReviewTypeId])
    REFERENCES [model].[ClinicalHistoryReviewTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientMedicalReviewClinicalHistoryReviewType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientMedicalReviewClinicalHistoryReviewType')
	DROP INDEX IX_FK_PatientMedicalReviewClinicalHistoryReviewType ON [model].[PatientMedicalReviews]
GO
CREATE INDEX [IX_FK_PatientMedicalReviewClinicalHistoryReviewType]
ON [model].[PatientMedicalReviews]
    ([ClinicalHistoryReviewTypeId]);
GO

-- Creating foreign key on [UserId] in table 'PatientMedicalReviews'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientMedicalReviewUser'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientMedicalReviews] DROP CONSTRAINT FK_PatientMedicalReviewUser

IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientMedicalReviews]
ADD CONSTRAINT [FK_PatientMedicalReviewUser]
    FOREIGN KEY ([UserId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientMedicalReviewUser'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientMedicalReviewUser')
	DROP INDEX IX_FK_PatientMedicalReviewUser ON [model].[PatientMedicalReviews]
GO
CREATE INDEX [IX_FK_PatientMedicalReviewUser]
ON [model].[PatientMedicalReviews]
    ([UserId]);
GO

-- Creating foreign key on [ClinicalHistoryReviewAreaId] in table 'PatientMedicalReviews'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientMedicalReviewClinicalHistoryReviewArea'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientMedicalReviews] DROP CONSTRAINT FK_PatientMedicalReviewClinicalHistoryReviewArea

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalHistoryReviewAreas]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientMedicalReviews]
ADD CONSTRAINT [FK_PatientMedicalReviewClinicalHistoryReviewArea]
    FOREIGN KEY ([ClinicalHistoryReviewAreaId])
    REFERENCES [model].[ClinicalHistoryReviewAreas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientMedicalReviewClinicalHistoryReviewArea'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientMedicalReviewClinicalHistoryReviewArea')
	DROP INDEX IX_FK_PatientMedicalReviewClinicalHistoryReviewArea ON [model].[PatientMedicalReviews]
GO
CREATE INDEX [IX_FK_PatientMedicalReviewClinicalHistoryReviewArea]
ON [model].[PatientMedicalReviews]
    ([ClinicalHistoryReviewAreaId]);
GO

-- Creating foreign key on [ClinicalAttributeId] in table 'PatientDiagnosisDetailQualifiers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisDetailQualifierClinicalAttribute'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisDetailQualifiers] DROP CONSTRAINT FK_PatientDiagnosisDetailQualifierClinicalAttribute

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalAttributes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetailQualifiers]
ADD CONSTRAINT [FK_PatientDiagnosisDetailQualifierClinicalAttribute]
    FOREIGN KEY ([ClinicalAttributeId])
    REFERENCES [model].[ClinicalAttributes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailQualifierClinicalAttribute'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisDetailQualifierClinicalAttribute')
	DROP INDEX IX_FK_PatientDiagnosisDetailQualifierClinicalAttribute ON [model].[PatientDiagnosisDetailQualifiers]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailQualifierClinicalAttribute]
ON [model].[PatientDiagnosisDetailQualifiers]
    ([ClinicalAttributeId]);
GO

-- Creating foreign key on [PatientDiagnosisDetailId] in table 'PatientDiagnosisDetailQualifiers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisDetailQualifierPatientDiagnosisDetail'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisDetailQualifiers] DROP CONSTRAINT FK_PatientDiagnosisDetailQualifierPatientDiagnosisDetail

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetails]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetailQualifiers]
ADD CONSTRAINT [FK_PatientDiagnosisDetailQualifierPatientDiagnosisDetail]
    FOREIGN KEY ([PatientDiagnosisDetailId])
    REFERENCES [model].[PatientDiagnosisDetails]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailQualifierPatientDiagnosisDetail'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisDetailQualifierPatientDiagnosisDetail')
	DROP INDEX IX_FK_PatientDiagnosisDetailQualifierPatientDiagnosisDetail ON [model].[PatientDiagnosisDetailQualifiers]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailQualifierPatientDiagnosisDetail]
ON [model].[PatientDiagnosisDetailQualifiers]
    ([PatientDiagnosisDetailId]);
GO

-- Creating foreign key on [ClinicalAttributeId] in table 'ClinicalHierarchyClinicalAttribute'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ClinicalHierarchyClinicalAttributeClinicalAttribute'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ClinicalHierarchyClinicalAttribute] DROP CONSTRAINT FK_ClinicalHierarchyClinicalAttributeClinicalAttribute

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalAttributes]'), 'IsUserTable') = 1
ALTER TABLE [model].[ClinicalHierarchyClinicalAttribute]
ADD CONSTRAINT [FK_ClinicalHierarchyClinicalAttributeClinicalAttribute]
    FOREIGN KEY ([ClinicalAttributeId])
    REFERENCES [model].[ClinicalAttributes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClinicalHierarchyClinicalAttributeClinicalAttribute'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ClinicalHierarchyClinicalAttributeClinicalAttribute')
	DROP INDEX IX_FK_ClinicalHierarchyClinicalAttributeClinicalAttribute ON [model].[ClinicalHierarchyClinicalAttribute]
GO
CREATE INDEX [IX_FK_ClinicalHierarchyClinicalAttributeClinicalAttribute]
ON [model].[ClinicalHierarchyClinicalAttribute]
    ([ClinicalAttributeId]);
GO

-- Creating foreign key on [ScreenId] in table 'LaboratoryTestResults'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_LaboratoryTestResultScreen'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[LaboratoryTestResults] DROP CONSTRAINT FK_LaboratoryTestResultScreen

IF OBJECTPROPERTY(OBJECT_ID('[model].[Screens]'), 'IsUserTable') = 1
ALTER TABLE [model].[LaboratoryTestResults]
ADD CONSTRAINT [FK_LaboratoryTestResultScreen]
    FOREIGN KEY ([ScreenId])
    REFERENCES [model].[Screens]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LaboratoryTestResultScreen'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_LaboratoryTestResultScreen')
	DROP INDEX IX_FK_LaboratoryTestResultScreen ON [model].[LaboratoryTestResults]
GO
CREATE INDEX [IX_FK_LaboratoryTestResultScreen]
ON [model].[LaboratoryTestResults]
    ([ScreenId]);
GO

-- Creating foreign key on [LaboratoryTestId] in table 'LaboratoryTestResults'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_LaboratoryTestResultLaboratoryTest'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[LaboratoryTestResults] DROP CONSTRAINT FK_LaboratoryTestResultLaboratoryTest

IF OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTests]'), 'IsUserTable') = 1
ALTER TABLE [model].[LaboratoryTestResults]
ADD CONSTRAINT [FK_LaboratoryTestResultLaboratoryTest]
    FOREIGN KEY ([LaboratoryTestId])
    REFERENCES [model].[LaboratoryTests]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LaboratoryTestResultLaboratoryTest'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_LaboratoryTestResultLaboratoryTest')
	DROP INDEX IX_FK_LaboratoryTestResultLaboratoryTest ON [model].[LaboratoryTestResults]
GO
CREATE INDEX [IX_FK_LaboratoryTestResultLaboratoryTest]
ON [model].[LaboratoryTestResults]
    ([LaboratoryTestId]);
GO

-- Creating foreign key on [ScreenId] in table 'DiagnosticTestPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_DiagnosticTestPerformedScreen'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[DiagnosticTestPerformeds] DROP CONSTRAINT FK_DiagnosticTestPerformedScreen

IF OBJECTPROPERTY(OBJECT_ID('[model].[Screens]'), 'IsUserTable') = 1
ALTER TABLE [model].[DiagnosticTestPerformeds]
ADD CONSTRAINT [FK_DiagnosticTestPerformedScreen]
    FOREIGN KEY ([ScreenId])
    REFERENCES [model].[Screens]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DiagnosticTestPerformedScreen'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_DiagnosticTestPerformedScreen')
	DROP INDEX IX_FK_DiagnosticTestPerformedScreen ON [model].[DiagnosticTestPerformeds]
GO
CREATE INDEX [IX_FK_DiagnosticTestPerformedScreen]
ON [model].[DiagnosticTestPerformeds]
    ([ScreenId]);
GO

-- Creating foreign key on [ScreenId] in table 'ProcedurePerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ProcedurePerformedScreen'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ProcedurePerformeds] DROP CONSTRAINT FK_ProcedurePerformedScreen

IF OBJECTPROPERTY(OBJECT_ID('[model].[Screens]'), 'IsUserTable') = 1
ALTER TABLE [model].[ProcedurePerformeds]
ADD CONSTRAINT [FK_ProcedurePerformedScreen]
    FOREIGN KEY ([ScreenId])
    REFERENCES [model].[Screens]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProcedurePerformedScreen'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ProcedurePerformedScreen')
	DROP INDEX IX_FK_ProcedurePerformedScreen ON [model].[ProcedurePerformeds]
GO
CREATE INDEX [IX_FK_ProcedurePerformedScreen]
ON [model].[ProcedurePerformeds]
    ([ScreenId]);
GO

-- Creating foreign key on [ScreenId] in table 'VaccinationAdministereds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_VaccinationAdministeredScreen'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[VaccinationAdministereds] DROP CONSTRAINT FK_VaccinationAdministeredScreen

IF OBJECTPROPERTY(OBJECT_ID('[model].[Screens]'), 'IsUserTable') = 1
ALTER TABLE [model].[VaccinationAdministereds]
ADD CONSTRAINT [FK_VaccinationAdministeredScreen]
    FOREIGN KEY ([ScreenId])
    REFERENCES [model].[Screens]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_VaccinationAdministeredScreen'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_VaccinationAdministeredScreen')
	DROP INDEX IX_FK_VaccinationAdministeredScreen ON [model].[VaccinationAdministereds]
GO
CREATE INDEX [IX_FK_VaccinationAdministeredScreen]
ON [model].[VaccinationAdministereds]
    ([ScreenId]);
GO

-- Creating foreign key on [PatientAllergenDetails_Id] in table 'PatientAllergenDetailAllergySource'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientAllergenDetailAllergySource_PatientAllergenDetail'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientAllergenDetailAllergySource] DROP CONSTRAINT FK_PatientAllergenDetailAllergySource_PatientAllergenDetail

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergenDetails]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientAllergenDetailAllergySource]
ADD CONSTRAINT [FK_PatientAllergenDetailAllergySource_PatientAllergenDetail]
    FOREIGN KEY ([PatientAllergenDetails_Id])
    REFERENCES [model].[PatientAllergenDetails]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [AllergySources_Id] in table 'PatientAllergenDetailAllergySource'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientAllergenDetailAllergySource_AllergySource'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientAllergenDetailAllergySource] DROP CONSTRAINT FK_PatientAllergenDetailAllergySource_AllergySource

IF OBJECTPROPERTY(OBJECT_ID('[model].[AllergySources]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientAllergenDetailAllergySource]
ADD CONSTRAINT [FK_PatientAllergenDetailAllergySource_AllergySource]
    FOREIGN KEY ([AllergySources_Id])
    REFERENCES [model].[AllergySources]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientAllergenDetailAllergySource_AllergySource'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientAllergenDetailAllergySource_AllergySource')
	DROP INDEX IX_FK_PatientAllergenDetailAllergySource_AllergySource ON [model].[PatientAllergenDetailAllergySource]
GO
CREATE INDEX [IX_FK_PatientAllergenDetailAllergySource_AllergySource]
ON [model].[PatientAllergenDetailAllergySource]
    ([AllergySources_Id]);
GO

-- Creating foreign key on [ScreenId] in table 'SpecialExamPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_SpecialExamPerformedScreen'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[SpecialExamPerformeds] DROP CONSTRAINT FK_SpecialExamPerformedScreen

IF OBJECTPROPERTY(OBJECT_ID('[model].[Screens]'), 'IsUserTable') = 1
ALTER TABLE [model].[SpecialExamPerformeds]
ADD CONSTRAINT [FK_SpecialExamPerformedScreen]
    FOREIGN KEY ([ScreenId])
    REFERENCES [model].[Screens]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SpecialExamPerformedScreen'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_SpecialExamPerformedScreen')
	DROP INDEX IX_FK_SpecialExamPerformedScreen ON [model].[SpecialExamPerformeds]
GO
CREATE INDEX [IX_FK_SpecialExamPerformedScreen]
ON [model].[SpecialExamPerformeds]
    ([ScreenId]);
GO

-- Creating foreign key on [ClinicalProcedureId] in table 'ProcedureOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ProcedureOrderClinicalProcedure'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ProcedureOrders] DROP CONSTRAINT FK_ProcedureOrderClinicalProcedure

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalProcedures]'), 'IsUserTable') = 1
ALTER TABLE [model].[ProcedureOrders]
ADD CONSTRAINT [FK_ProcedureOrderClinicalProcedure]
    FOREIGN KEY ([ClinicalProcedureId])
    REFERENCES [model].[ClinicalProcedures]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProcedureOrderClinicalProcedure'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ProcedureOrderClinicalProcedure')
	DROP INDEX IX_FK_ProcedureOrderClinicalProcedure ON [model].[ProcedureOrders]
GO
CREATE INDEX [IX_FK_ProcedureOrderClinicalProcedure]
ON [model].[ProcedureOrders]
    ([ClinicalProcedureId]);
GO

-- Creating foreign key on [VaccineAllergenReactionType_AllergenReactionType_Id] in table 'VaccineAllergenReactionType'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_VaccineAllergenReactionType_Vaccine'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[VaccineAllergenReactionType] DROP CONSTRAINT FK_VaccineAllergenReactionType_Vaccine

IF OBJECTPROPERTY(OBJECT_ID('[model].[Vaccines]'), 'IsUserTable') = 1
ALTER TABLE [model].[VaccineAllergenReactionType]
ADD CONSTRAINT [FK_VaccineAllergenReactionType_Vaccine]
    FOREIGN KEY ([VaccineAllergenReactionType_AllergenReactionType_Id])
    REFERENCES [model].[Vaccines]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [VaccineAllergenReactionType_Vaccine_Id] in table 'VaccineAllergenReactionType'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_VaccineAllergenReactionType_AllergenReactionType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[VaccineAllergenReactionType] DROP CONSTRAINT FK_VaccineAllergenReactionType_AllergenReactionType

IF OBJECTPROPERTY(OBJECT_ID('[model].[AllergenReactionTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[VaccineAllergenReactionType]
ADD CONSTRAINT [FK_VaccineAllergenReactionType_AllergenReactionType]
    FOREIGN KEY ([VaccineAllergenReactionType_Vaccine_Id])
    REFERENCES [model].[AllergenReactionTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_VaccineAllergenReactionType_AllergenReactionType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_VaccineAllergenReactionType_AllergenReactionType')
	DROP INDEX IX_FK_VaccineAllergenReactionType_AllergenReactionType ON [model].[VaccineAllergenReactionType]
GO
CREATE INDEX [IX_FK_VaccineAllergenReactionType_AllergenReactionType]
ON [model].[VaccineAllergenReactionType]
    ([VaccineAllergenReactionType_Vaccine_Id]);
GO

-- Creating foreign key on [VaccineId] in table 'VaccinationAdministereds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_VaccinationAdministeredVaccine'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[VaccinationAdministereds] DROP CONSTRAINT FK_VaccinationAdministeredVaccine

IF OBJECTPROPERTY(OBJECT_ID('[model].[Vaccines]'), 'IsUserTable') = 1
ALTER TABLE [model].[VaccinationAdministereds]
ADD CONSTRAINT [FK_VaccinationAdministeredVaccine]
    FOREIGN KEY ([VaccineId])
    REFERENCES [model].[Vaccines]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_VaccinationAdministeredVaccine'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_VaccinationAdministeredVaccine')
	DROP INDEX IX_FK_VaccinationAdministeredVaccine ON [model].[VaccinationAdministereds]
GO
CREATE INDEX [IX_FK_VaccinationAdministeredVaccine]
ON [model].[VaccinationAdministereds]
    ([VaccineId]);
GO

-- Creating foreign key on [RelevancyQualifierId] in table 'PatientDiagnosisDetails'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisDetailRelevancyQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT FK_PatientDiagnosisDetailRelevancyQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[RelevancyQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailRelevancyQualifier]
    FOREIGN KEY ([RelevancyQualifierId])
    REFERENCES [model].[RelevancyQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailRelevancyQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisDetailRelevancyQualifier')
	DROP INDEX IX_FK_PatientDiagnosisDetailRelevancyQualifier ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailRelevancyQualifier]
ON [model].[PatientDiagnosisDetails]
    ([RelevancyQualifierId]);
GO

-- Creating foreign key on [AccuracyQualifierId] in table 'PatientDiagnosisDetails'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisDetailAccuracyQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT FK_PatientDiagnosisDetailAccuracyQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[AccuracyQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailAccuracyQualifier]
    FOREIGN KEY ([AccuracyQualifierId])
    REFERENCES [model].[AccuracyQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailAccuracyQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisDetailAccuracyQualifier')
	DROP INDEX IX_FK_PatientDiagnosisDetailAccuracyQualifier ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailAccuracyQualifier]
ON [model].[PatientDiagnosisDetails]
    ([AccuracyQualifierId]);
GO

-- Creating foreign key on [AccuracyQualifierId] in table 'PatientDiagnosisComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisCommentAccuracyQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT FK_PatientDiagnosisCommentAccuracyQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[AccuracyQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisComments]
ADD CONSTRAINT [FK_PatientDiagnosisCommentAccuracyQualifier]
    FOREIGN KEY ([AccuracyQualifierId])
    REFERENCES [model].[AccuracyQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisCommentAccuracyQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisCommentAccuracyQualifier')
	DROP INDEX IX_FK_PatientDiagnosisCommentAccuracyQualifier ON [model].[PatientDiagnosisComments]
GO
CREATE INDEX [IX_FK_PatientDiagnosisCommentAccuracyQualifier]
ON [model].[PatientDiagnosisComments]
    ([AccuracyQualifierId]);
GO

-- Creating foreign key on [RelevancyQualifierId] in table 'PatientDiagnosisComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisCommentRelevancyQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT FK_PatientDiagnosisCommentRelevancyQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[RelevancyQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisComments]
ADD CONSTRAINT [FK_PatientDiagnosisCommentRelevancyQualifier]
    FOREIGN KEY ([RelevancyQualifierId])
    REFERENCES [model].[RelevancyQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisCommentRelevancyQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisCommentRelevancyQualifier')
	DROP INDEX IX_FK_PatientDiagnosisCommentRelevancyQualifier ON [model].[PatientDiagnosisComments]
GO
CREATE INDEX [IX_FK_PatientDiagnosisCommentRelevancyQualifier]
ON [model].[PatientDiagnosisComments]
    ([RelevancyQualifierId]);
GO

-- Creating foreign key on [AccuracyQualifierId] in table 'PatientAllergenComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientAllergenCommentAccuracyQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientAllergenComments] DROP CONSTRAINT FK_PatientAllergenCommentAccuracyQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[AccuracyQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientAllergenComments]
ADD CONSTRAINT [FK_PatientAllergenCommentAccuracyQualifier]
    FOREIGN KEY ([AccuracyQualifierId])
    REFERENCES [model].[AccuracyQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientAllergenCommentAccuracyQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientAllergenCommentAccuracyQualifier')
	DROP INDEX IX_FK_PatientAllergenCommentAccuracyQualifier ON [model].[PatientAllergenComments]
GO
CREATE INDEX [IX_FK_PatientAllergenCommentAccuracyQualifier]
ON [model].[PatientAllergenComments]
    ([AccuracyQualifierId]);
GO

-- Creating foreign key on [RelevancyQualifierId] in table 'PatientAllergenComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientAllergenCommentRelevancyQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientAllergenComments] DROP CONSTRAINT FK_PatientAllergenCommentRelevancyQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[RelevancyQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientAllergenComments]
ADD CONSTRAINT [FK_PatientAllergenCommentRelevancyQualifier]
    FOREIGN KEY ([RelevancyQualifierId])
    REFERENCES [model].[RelevancyQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientAllergenCommentRelevancyQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientAllergenCommentRelevancyQualifier')
	DROP INDEX IX_FK_PatientAllergenCommentRelevancyQualifier ON [model].[PatientAllergenComments]
GO
CREATE INDEX [IX_FK_PatientAllergenCommentRelevancyQualifier]
ON [model].[PatientAllergenComments]
    ([RelevancyQualifierId]);
GO

-- Creating foreign key on [ClinicalQualifierId] in table 'PatientDiagnosisDetailQualifiers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisDetailQualifierClinicalQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisDetailQualifiers] DROP CONSTRAINT FK_PatientDiagnosisDetailQualifierClinicalQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetailQualifiers]
ADD CONSTRAINT [FK_PatientDiagnosisDetailQualifierClinicalQualifier]
    FOREIGN KEY ([ClinicalQualifierId])
    REFERENCES [model].[ClinicalQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailQualifierClinicalQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisDetailQualifierClinicalQualifier')
	DROP INDEX IX_FK_PatientDiagnosisDetailQualifierClinicalQualifier ON [model].[PatientDiagnosisDetailQualifiers]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailQualifierClinicalQualifier]
ON [model].[PatientDiagnosisDetailQualifiers]
    ([ClinicalQualifierId]);
GO

-- Creating foreign key on [ClinicalConditionId] in table 'PatientDiagnoses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisClinicalCondition'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnoses] DROP CONSTRAINT FK_PatientDiagnosisClinicalCondition

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalConditions]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnoses]
ADD CONSTRAINT [FK_PatientDiagnosisClinicalCondition]
    FOREIGN KEY ([ClinicalConditionId])
    REFERENCES [model].[ClinicalConditions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisClinicalCondition'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisClinicalCondition')
	DROP INDEX IX_FK_PatientDiagnosisClinicalCondition ON [model].[PatientDiagnoses]
GO
CREATE INDEX [IX_FK_PatientDiagnosisClinicalCondition]
ON [model].[PatientDiagnoses]
    ([ClinicalConditionId]);
GO

-- Creating foreign key on [TreatmentPlans_Id] in table 'TreatmentPlanClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentPlanClinicalCondition_TreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentPlanClinicalCondition] DROP CONSTRAINT FK_TreatmentPlanClinicalCondition_TreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentPlanClinicalCondition]
ADD CONSTRAINT [FK_TreatmentPlanClinicalCondition_TreatmentPlan]
    FOREIGN KEY ([TreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalConditions_Id] in table 'TreatmentPlanClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentPlanClinicalCondition_ClinicalCondition'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentPlanClinicalCondition] DROP CONSTRAINT FK_TreatmentPlanClinicalCondition_ClinicalCondition

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalConditions]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentPlanClinicalCondition]
ADD CONSTRAINT [FK_TreatmentPlanClinicalCondition_ClinicalCondition]
    FOREIGN KEY ([ClinicalConditions_Id])
    REFERENCES [model].[ClinicalConditions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TreatmentPlanClinicalCondition_ClinicalCondition'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_TreatmentPlanClinicalCondition_ClinicalCondition')
	DROP INDEX IX_FK_TreatmentPlanClinicalCondition_ClinicalCondition ON [model].[TreatmentPlanClinicalCondition]
GO
CREATE INDEX [IX_FK_TreatmentPlanClinicalCondition_ClinicalCondition]
ON [model].[TreatmentPlanClinicalCondition]
    ([ClinicalConditions_Id]);
GO

-- Creating foreign key on [EncounterLaboratoryTestOrders_Id] in table 'EncounterLaboratoryTestOrderClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterLaboratoryTestOrderClinicalCondition_EncounterLaboratoryTestOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterLaboratoryTestOrderClinicalCondition] DROP CONSTRAINT FK_EncounterLaboratoryTestOrderClinicalCondition_EncounterLaboratoryTestOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterLaboratoryTestOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterLaboratoryTestOrderClinicalCondition]
ADD CONSTRAINT [FK_EncounterLaboratoryTestOrderClinicalCondition_EncounterLaboratoryTestOrder]
    FOREIGN KEY ([EncounterLaboratoryTestOrders_Id])
    REFERENCES [model].[EncounterLaboratoryTestOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalConditions_Id] in table 'EncounterLaboratoryTestOrderClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterLaboratoryTestOrderClinicalCondition_ClinicalCondition'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterLaboratoryTestOrderClinicalCondition] DROP CONSTRAINT FK_EncounterLaboratoryTestOrderClinicalCondition_ClinicalCondition

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalConditions]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterLaboratoryTestOrderClinicalCondition]
ADD CONSTRAINT [FK_EncounterLaboratoryTestOrderClinicalCondition_ClinicalCondition]
    FOREIGN KEY ([ClinicalConditions_Id])
    REFERENCES [model].[ClinicalConditions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterLaboratoryTestOrderClinicalCondition_ClinicalCondition'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterLaboratoryTestOrderClinicalCondition_ClinicalCondition')
	DROP INDEX IX_FK_EncounterLaboratoryTestOrderClinicalCondition_ClinicalCondition ON [model].[EncounterLaboratoryTestOrderClinicalCondition]
GO
CREATE INDEX [IX_FK_EncounterLaboratoryTestOrderClinicalCondition_ClinicalCondition]
ON [model].[EncounterLaboratoryTestOrderClinicalCondition]
    ([ClinicalConditions_Id]);
GO

-- Creating foreign key on [EncounterPatientEducations_Id] in table 'EncounterPatientEducationClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterPatientEducationClinicalCondition_EncounterPatientEducation'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterPatientEducationClinicalCondition] DROP CONSTRAINT FK_EncounterPatientEducationClinicalCondition_EncounterPatientEducation

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterPatientEducations]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterPatientEducationClinicalCondition]
ADD CONSTRAINT [FK_EncounterPatientEducationClinicalCondition_EncounterPatientEducation]
    FOREIGN KEY ([EncounterPatientEducations_Id])
    REFERENCES [model].[EncounterPatientEducations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalConditions_Id] in table 'EncounterPatientEducationClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterPatientEducationClinicalCondition_ClinicalCondition'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterPatientEducationClinicalCondition] DROP CONSTRAINT FK_EncounterPatientEducationClinicalCondition_ClinicalCondition

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalConditions]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterPatientEducationClinicalCondition]
ADD CONSTRAINT [FK_EncounterPatientEducationClinicalCondition_ClinicalCondition]
    FOREIGN KEY ([ClinicalConditions_Id])
    REFERENCES [model].[ClinicalConditions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterPatientEducationClinicalCondition_ClinicalCondition'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterPatientEducationClinicalCondition_ClinicalCondition')
	DROP INDEX IX_FK_EncounterPatientEducationClinicalCondition_ClinicalCondition ON [model].[EncounterPatientEducationClinicalCondition]
GO
CREATE INDEX [IX_FK_EncounterPatientEducationClinicalCondition_ClinicalCondition]
ON [model].[EncounterPatientEducationClinicalCondition]
    ([ClinicalConditions_Id]);
GO

-- Creating foreign key on [PatientEducations_Id] in table 'PatientEducationClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientEducationClinicalCondition_PatientEducation'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientEducationClinicalCondition] DROP CONSTRAINT FK_PatientEducationClinicalCondition_PatientEducation

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientEducations]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientEducationClinicalCondition]
ADD CONSTRAINT [FK_PatientEducationClinicalCondition_PatientEducation]
    FOREIGN KEY ([PatientEducations_Id])
    REFERENCES [model].[PatientEducations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalConditions_Id] in table 'PatientEducationClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientEducationClinicalCondition_ClinicalCondition'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientEducationClinicalCondition] DROP CONSTRAINT FK_PatientEducationClinicalCondition_ClinicalCondition

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalConditions]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientEducationClinicalCondition]
ADD CONSTRAINT [FK_PatientEducationClinicalCondition_ClinicalCondition]
    FOREIGN KEY ([ClinicalConditions_Id])
    REFERENCES [model].[ClinicalConditions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientEducationClinicalCondition_ClinicalCondition'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientEducationClinicalCondition_ClinicalCondition')
	DROP INDEX IX_FK_PatientEducationClinicalCondition_ClinicalCondition ON [model].[PatientEducationClinicalCondition]
GO
CREATE INDEX [IX_FK_PatientEducationClinicalCondition_ClinicalCondition]
ON [model].[PatientEducationClinicalCondition]
    ([ClinicalConditions_Id]);
GO

-- Creating foreign key on [ProcedurePerformeds_Id] in table 'ProcedurePerformedClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ProcedurePerformedClinicalCondition_ProcedurePerformed'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ProcedurePerformedClinicalCondition] DROP CONSTRAINT FK_ProcedurePerformedClinicalCondition_ProcedurePerformed

IF OBJECTPROPERTY(OBJECT_ID('[model].[ProcedurePerformeds]'), 'IsUserTable') = 1
ALTER TABLE [model].[ProcedurePerformedClinicalCondition]
ADD CONSTRAINT [FK_ProcedurePerformedClinicalCondition_ProcedurePerformed]
    FOREIGN KEY ([ProcedurePerformeds_Id])
    REFERENCES [model].[ProcedurePerformeds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalConditions_Id] in table 'ProcedurePerformedClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ProcedurePerformedClinicalCondition_ClinicalCondition'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ProcedurePerformedClinicalCondition] DROP CONSTRAINT FK_ProcedurePerformedClinicalCondition_ClinicalCondition

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalConditions]'), 'IsUserTable') = 1
ALTER TABLE [model].[ProcedurePerformedClinicalCondition]
ADD CONSTRAINT [FK_ProcedurePerformedClinicalCondition_ClinicalCondition]
    FOREIGN KEY ([ClinicalConditions_Id])
    REFERENCES [model].[ClinicalConditions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProcedurePerformedClinicalCondition_ClinicalCondition'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ProcedurePerformedClinicalCondition_ClinicalCondition')
	DROP INDEX IX_FK_ProcedurePerformedClinicalCondition_ClinicalCondition ON [model].[ProcedurePerformedClinicalCondition]
GO
CREATE INDEX [IX_FK_ProcedurePerformedClinicalCondition_ClinicalCondition]
ON [model].[ProcedurePerformedClinicalCondition]
    ([ClinicalConditions_Id]);
GO

-- Creating foreign key on [DiagnosticTestPerformeds_Id] in table 'DiagnosticTestPerformedClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_DiagnosticTestPerformedClinicalCondition_DiagnosticTestPerformed'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[DiagnosticTestPerformedClinicalCondition] DROP CONSTRAINT FK_DiagnosticTestPerformedClinicalCondition_DiagnosticTestPerformed

IF OBJECTPROPERTY(OBJECT_ID('[model].[DiagnosticTestPerformeds]'), 'IsUserTable') = 1
ALTER TABLE [model].[DiagnosticTestPerformedClinicalCondition]
ADD CONSTRAINT [FK_DiagnosticTestPerformedClinicalCondition_DiagnosticTestPerformed]
    FOREIGN KEY ([DiagnosticTestPerformeds_Id])
    REFERENCES [model].[DiagnosticTestPerformeds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalConditions_Id] in table 'DiagnosticTestPerformedClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_DiagnosticTestPerformedClinicalCondition_ClinicalCondition'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[DiagnosticTestPerformedClinicalCondition] DROP CONSTRAINT FK_DiagnosticTestPerformedClinicalCondition_ClinicalCondition

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalConditions]'), 'IsUserTable') = 1
ALTER TABLE [model].[DiagnosticTestPerformedClinicalCondition]
ADD CONSTRAINT [FK_DiagnosticTestPerformedClinicalCondition_ClinicalCondition]
    FOREIGN KEY ([ClinicalConditions_Id])
    REFERENCES [model].[ClinicalConditions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DiagnosticTestPerformedClinicalCondition_ClinicalCondition'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_DiagnosticTestPerformedClinicalCondition_ClinicalCondition')
	DROP INDEX IX_FK_DiagnosticTestPerformedClinicalCondition_ClinicalCondition ON [model].[DiagnosticTestPerformedClinicalCondition]
GO
CREATE INDEX [IX_FK_DiagnosticTestPerformedClinicalCondition_ClinicalCondition]
ON [model].[DiagnosticTestPerformedClinicalCondition]
    ([ClinicalConditions_Id]);
GO

-- Creating foreign key on [RouteOfAdministrations_Id] in table 'RouteOfAdministrationClinicalSpecialtyType'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_RouteOfAdministrationClinicalSpecialtyType_RouteOfAdministration'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[RouteOfAdministrationClinicalSpecialtyType] DROP CONSTRAINT FK_RouteOfAdministrationClinicalSpecialtyType_RouteOfAdministration

IF OBJECTPROPERTY(OBJECT_ID('[model].[RouteOfAdministrations]'), 'IsUserTable') = 1
ALTER TABLE [model].[RouteOfAdministrationClinicalSpecialtyType]
ADD CONSTRAINT [FK_RouteOfAdministrationClinicalSpecialtyType_RouteOfAdministration]
    FOREIGN KEY ([RouteOfAdministrations_Id])
    REFERENCES [model].[RouteOfAdministrations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalSpecialtyTypes_Id] in table 'RouteOfAdministrationClinicalSpecialtyType'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_RouteOfAdministrationClinicalSpecialtyType_ClinicalSpecialtyType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[RouteOfAdministrationClinicalSpecialtyType] DROP CONSTRAINT FK_RouteOfAdministrationClinicalSpecialtyType_ClinicalSpecialtyType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalSpecialtyTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[RouteOfAdministrationClinicalSpecialtyType]
ADD CONSTRAINT [FK_RouteOfAdministrationClinicalSpecialtyType_ClinicalSpecialtyType]
    FOREIGN KEY ([ClinicalSpecialtyTypes_Id])
    REFERENCES [model].[ClinicalSpecialtyTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RouteOfAdministrationClinicalSpecialtyType_ClinicalSpecialtyType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_RouteOfAdministrationClinicalSpecialtyType_ClinicalSpecialtyType')
	DROP INDEX IX_FK_RouteOfAdministrationClinicalSpecialtyType_ClinicalSpecialtyType ON [model].[RouteOfAdministrationClinicalSpecialtyType]
GO
CREATE INDEX [IX_FK_RouteOfAdministrationClinicalSpecialtyType_ClinicalSpecialtyType]
ON [model].[RouteOfAdministrationClinicalSpecialtyType]
    ([ClinicalSpecialtyTypes_Id]);
GO

-- Creating foreign key on [DoctorId] in table 'MedicationOrderFavorites'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_MedicationOrderFavoriteDoctor'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[MedicationOrderFavorites] DROP CONSTRAINT FK_MedicationOrderFavoriteDoctor

IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[MedicationOrderFavorites]
ADD CONSTRAINT [FK_MedicationOrderFavoriteDoctor]
    FOREIGN KEY ([DoctorId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_MedicationOrderFavoriteDoctor'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_MedicationOrderFavoriteDoctor')
	DROP INDEX IX_FK_MedicationOrderFavoriteDoctor ON [model].[MedicationOrderFavorites]
GO
CREATE INDEX [IX_FK_MedicationOrderFavoriteDoctor]
ON [model].[MedicationOrderFavorites]
    ([DoctorId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterHistoryOfPresentIllnessComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterHistoryOfPresentIllnessCommentEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterHistoryOfPresentIllnessComments] DROP CONSTRAINT FK_EncounterHistoryOfPresentIllnessCommentEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterHistoryOfPresentIllnessComments]
ADD CONSTRAINT [FK_EncounterHistoryOfPresentIllnessCommentEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterHistoryOfPresentIllnessCommentEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterHistoryOfPresentIllnessCommentEncounter')
	DROP INDEX IX_FK_EncounterHistoryOfPresentIllnessCommentEncounter ON [model].[EncounterHistoryOfPresentIllnessComments]
GO
CREATE INDEX [IX_FK_EncounterHistoryOfPresentIllnessCommentEncounter]
ON [model].[EncounterHistoryOfPresentIllnessComments]
    ([EncounterId]);
GO

-- Creating foreign key on [OrganSubStructureId] in table 'ClinicalConditions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ClinicalConditionOrganSubStructure'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ClinicalConditions] DROP CONSTRAINT FK_ClinicalConditionOrganSubStructure

IF OBJECTPROPERTY(OBJECT_ID('[model].[OrganSubStructures]'), 'IsUserTable') = 1
ALTER TABLE [model].[ClinicalConditions]
ADD CONSTRAINT [FK_ClinicalConditionOrganSubStructure]
    FOREIGN KEY ([OrganSubStructureId])
    REFERENCES [model].[OrganSubStructures]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClinicalConditionOrganSubStructure'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ClinicalConditionOrganSubStructure')
	DROP INDEX IX_FK_ClinicalConditionOrganSubStructure ON [model].[ClinicalConditions]
GO
CREATE INDEX [IX_FK_ClinicalConditionOrganSubStructure]
ON [model].[ClinicalConditions]
    ([OrganSubStructureId]);
GO

-- Creating foreign key on [VaccinationAdministereds_Id] in table 'VaccinationAdministeredUnitOfMeasurement'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_VaccinationAdministeredUnitOfMeasurement_VaccinationAdministered'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[VaccinationAdministeredUnitOfMeasurement] DROP CONSTRAINT FK_VaccinationAdministeredUnitOfMeasurement_VaccinationAdministered

IF OBJECTPROPERTY(OBJECT_ID('[model].[VaccinationAdministereds]'), 'IsUserTable') = 1
ALTER TABLE [model].[VaccinationAdministeredUnitOfMeasurement]
ADD CONSTRAINT [FK_VaccinationAdministeredUnitOfMeasurement_VaccinationAdministered]
    FOREIGN KEY ([VaccinationAdministereds_Id])
    REFERENCES [model].[VaccinationAdministereds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [UnitsOfMeasurement_Id] in table 'VaccinationAdministeredUnitOfMeasurement'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_VaccinationAdministeredUnitOfMeasurement_UnitOfMeasurement'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[VaccinationAdministeredUnitOfMeasurement] DROP CONSTRAINT FK_VaccinationAdministeredUnitOfMeasurement_UnitOfMeasurement

IF OBJECTPROPERTY(OBJECT_ID('[model].[UnitsOfMeasurement]'), 'IsUserTable') = 1
ALTER TABLE [model].[VaccinationAdministeredUnitOfMeasurement]
ADD CONSTRAINT [FK_VaccinationAdministeredUnitOfMeasurement_UnitOfMeasurement]
    FOREIGN KEY ([UnitsOfMeasurement_Id])
    REFERENCES [model].[UnitsOfMeasurement]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_VaccinationAdministeredUnitOfMeasurement_UnitOfMeasurement'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_VaccinationAdministeredUnitOfMeasurement_UnitOfMeasurement')
	DROP INDEX IX_FK_VaccinationAdministeredUnitOfMeasurement_UnitOfMeasurement ON [model].[VaccinationAdministeredUnitOfMeasurement]
GO
CREATE INDEX [IX_FK_VaccinationAdministeredUnitOfMeasurement_UnitOfMeasurement]
ON [model].[VaccinationAdministeredUnitOfMeasurement]
    ([UnitsOfMeasurement_Id]);
GO

-- Creating foreign key on [HistoryOfPresentIllnessCategoryId] in table 'HistoryOfPresentIllnesses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_HistoryOfPresentIllnessHistoryOfPresentIllnessCategory'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[HistoryOfPresentIllnesses] DROP CONSTRAINT FK_HistoryOfPresentIllnessHistoryOfPresentIllnessCategory

IF OBJECTPROPERTY(OBJECT_ID('[model].[HistoryOfPresentIllnessCategories]'), 'IsUserTable') = 1
ALTER TABLE [model].[HistoryOfPresentIllnesses]
ADD CONSTRAINT [FK_HistoryOfPresentIllnessHistoryOfPresentIllnessCategory]
    FOREIGN KEY ([HistoryOfPresentIllnessCategoryId])
    REFERENCES [model].[HistoryOfPresentIllnessCategories]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_HistoryOfPresentIllnessHistoryOfPresentIllnessCategory'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_HistoryOfPresentIllnessHistoryOfPresentIllnessCategory')
	DROP INDEX IX_FK_HistoryOfPresentIllnessHistoryOfPresentIllnessCategory ON [model].[HistoryOfPresentIllnesses]
GO
CREATE INDEX [IX_FK_HistoryOfPresentIllnessHistoryOfPresentIllnessCategory]
ON [model].[HistoryOfPresentIllnesses]
    ([HistoryOfPresentIllnessCategoryId]);
GO

-- Creating foreign key on [BodySystemId] in table 'BodyParts'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_BodyPartBodySystem'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[BodyParts] DROP CONSTRAINT FK_BodyPartBodySystem

IF OBJECTPROPERTY(OBJECT_ID('[model].[BodySystems]'), 'IsUserTable') = 1
ALTER TABLE [model].[BodyParts]
ADD CONSTRAINT [FK_BodyPartBodySystem]
    FOREIGN KEY ([BodySystemId])
    REFERENCES [model].[BodySystems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BodyPartBodySystem'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_BodyPartBodySystem')
	DROP INDEX IX_FK_BodyPartBodySystem ON [model].[BodyParts]
GO
CREATE INDEX [IX_FK_BodyPartBodySystem]
ON [model].[BodyParts]
    ([BodySystemId]);
GO

-- Creating foreign key on [ClinicalConditionId] in table 'HistoryOfPresentIllnesses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_HistoryOfPresentIllnessClinicalCondition'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[HistoryOfPresentIllnesses] DROP CONSTRAINT FK_HistoryOfPresentIllnessClinicalCondition

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalConditions]'), 'IsUserTable') = 1
ALTER TABLE [model].[HistoryOfPresentIllnesses]
ADD CONSTRAINT [FK_HistoryOfPresentIllnessClinicalCondition]
    FOREIGN KEY ([ClinicalConditionId])
    REFERENCES [model].[ClinicalConditions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_HistoryOfPresentIllnessClinicalCondition'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_HistoryOfPresentIllnessClinicalCondition')
	DROP INDEX IX_FK_HistoryOfPresentIllnessClinicalCondition ON [model].[HistoryOfPresentIllnesses]
GO
CREATE INDEX [IX_FK_HistoryOfPresentIllnessClinicalCondition]
ON [model].[HistoryOfPresentIllnesses]
    ([ClinicalConditionId]);
GO

-- Creating foreign key on [TreatmentGoalAndInstructions_Id] in table 'TreatmentGoalClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentGoalClinicalCondition_TreatmentGoal'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentGoalClinicalCondition] DROP CONSTRAINT FK_TreatmentGoalClinicalCondition_TreatmentGoal

IF OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentGoalAndInstructions]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentGoalClinicalCondition]
ADD CONSTRAINT [FK_TreatmentGoalClinicalCondition_TreatmentGoal]
    FOREIGN KEY ([TreatmentGoalAndInstructions_Id])
    REFERENCES [model].[TreatmentGoalAndInstructions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalConditions_Id] in table 'TreatmentGoalClinicalCondition'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentGoalClinicalCondition_ClinicalCondition'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentGoalClinicalCondition] DROP CONSTRAINT FK_TreatmentGoalClinicalCondition_ClinicalCondition

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalConditions]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentGoalClinicalCondition]
ADD CONSTRAINT [FK_TreatmentGoalClinicalCondition_ClinicalCondition]
    FOREIGN KEY ([ClinicalConditions_Id])
    REFERENCES [model].[ClinicalConditions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TreatmentGoalClinicalCondition_ClinicalCondition'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_TreatmentGoalClinicalCondition_ClinicalCondition')
	DROP INDEX IX_FK_TreatmentGoalClinicalCondition_ClinicalCondition ON [model].[TreatmentGoalClinicalCondition]
GO
CREATE INDEX [IX_FK_TreatmentGoalClinicalCondition_ClinicalCondition]
ON [model].[TreatmentGoalClinicalCondition]
    ([ClinicalConditions_Id]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterHistoryOfPresentIllnesses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterHistoryOfPresentIllnessEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterHistoryOfPresentIllnesses] DROP CONSTRAINT FK_EncounterHistoryOfPresentIllnessEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterHistoryOfPresentIllnesses]
ADD CONSTRAINT [FK_EncounterHistoryOfPresentIllnessEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterHistoryOfPresentIllnessEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterHistoryOfPresentIllnessEncounter')
	DROP INDEX IX_FK_EncounterHistoryOfPresentIllnessEncounter ON [model].[EncounterHistoryOfPresentIllnesses]
GO
CREATE INDEX [IX_FK_EncounterHistoryOfPresentIllnessEncounter]
ON [model].[EncounterHistoryOfPresentIllnesses]
    ([EncounterId]);
GO

-- Creating foreign key on [Screens_Id] in table 'ScreenQuestion'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ScreenQuestion_Screen'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ScreenQuestion] DROP CONSTRAINT FK_ScreenQuestion_Screen

IF OBJECTPROPERTY(OBJECT_ID('[model].[Screens]'), 'IsUserTable') = 1
ALTER TABLE [model].[ScreenQuestion]
ADD CONSTRAINT [FK_ScreenQuestion_Screen]
    FOREIGN KEY ([Screens_Id])
    REFERENCES [model].[Screens]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Questions_Id] in table 'ScreenQuestion'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ScreenQuestion_Question'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ScreenQuestion] DROP CONSTRAINT FK_ScreenQuestion_Question

IF OBJECTPROPERTY(OBJECT_ID('[model].[Questions]'), 'IsUserTable') = 1
ALTER TABLE [model].[ScreenQuestion]
ADD CONSTRAINT [FK_ScreenQuestion_Question]
    FOREIGN KEY ([Questions_Id])
    REFERENCES [model].[Questions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ScreenQuestion_Question'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ScreenQuestion_Question')
	DROP INDEX IX_FK_ScreenQuestion_Question ON [model].[ScreenQuestion]
GO
CREATE INDEX [IX_FK_ScreenQuestion_Question]
ON [model].[ScreenQuestion]
    ([Questions_Id]);
GO

-- Creating foreign key on [CopiesSentPatientLaboratoryTestResults_Id] in table 'PatientLaboratoryTestResultCopiesSentExternalProvider'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientLaboratoryTestResultCopiesSentExternalProvider_PatientLaboratoryTestResult'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientLaboratoryTestResultCopiesSentExternalProvider] DROP CONSTRAINT FK_PatientLaboratoryTestResultCopiesSentExternalProvider_PatientLaboratoryTestResult

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientLaboratoryTestResults]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientLaboratoryTestResultCopiesSentExternalProvider]
ADD CONSTRAINT [FK_PatientLaboratoryTestResultCopiesSentExternalProvider_PatientLaboratoryTestResult]
    FOREIGN KEY ([CopiesSentPatientLaboratoryTestResults_Id])
    REFERENCES [model].[PatientLaboratoryTestResults]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [CopiesSentExternalProviders_Id] in table 'PatientLaboratoryTestResultCopiesSentExternalProvider'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientLaboratoryTestResultCopiesSentExternalProvider_ExternalProvider'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientLaboratoryTestResultCopiesSentExternalProvider] DROP CONSTRAINT FK_PatientLaboratoryTestResultCopiesSentExternalProvider_ExternalProvider

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContacts]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientLaboratoryTestResultCopiesSentExternalProvider]
ADD CONSTRAINT [FK_PatientLaboratoryTestResultCopiesSentExternalProvider_ExternalProvider]
    FOREIGN KEY ([CopiesSentExternalProviders_Id])
    REFERENCES [model].[ExternalContacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientLaboratoryTestResultCopiesSentExternalProvider_ExternalProvider'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientLaboratoryTestResultCopiesSentExternalProvider_ExternalProvider')
	DROP INDEX IX_FK_PatientLaboratoryTestResultCopiesSentExternalProvider_ExternalProvider ON [model].[PatientLaboratoryTestResultCopiesSentExternalProvider]
GO
CREATE INDEX [IX_FK_PatientLaboratoryTestResultCopiesSentExternalProvider_ExternalProvider]
ON [model].[PatientLaboratoryTestResultCopiesSentExternalProvider]
    ([CopiesSentExternalProviders_Id]);
GO

-- Creating foreign key on [ClinicalQualifierCategoryId] in table 'ClinicalQualifiers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ClinicalQualifierClinicalQualifierCategory'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ClinicalQualifiers] DROP CONSTRAINT FK_ClinicalQualifierClinicalQualifierCategory

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalQualifierCategories]'), 'IsUserTable') = 1
ALTER TABLE [model].[ClinicalQualifiers]
ADD CONSTRAINT [FK_ClinicalQualifierClinicalQualifierCategory]
    FOREIGN KEY ([ClinicalQualifierCategoryId])
    REFERENCES [model].[ClinicalQualifierCategories]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClinicalQualifierClinicalQualifierCategory'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ClinicalQualifierClinicalQualifierCategory')
	DROP INDEX IX_FK_ClinicalQualifierClinicalQualifierCategory ON [model].[ClinicalQualifiers]
GO
CREATE INDEX [IX_FK_ClinicalQualifierClinicalQualifierCategory]
ON [model].[ClinicalQualifiers]
    ([ClinicalQualifierCategoryId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterChiefComplaints'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterChiefComplaintEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterChiefComplaints] DROP CONSTRAINT FK_EncounterChiefComplaintEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterChiefComplaints]
ADD CONSTRAINT [FK_EncounterChiefComplaintEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterChiefComplaintEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterChiefComplaintEncounter')
	DROP INDEX IX_FK_EncounterChiefComplaintEncounter ON [model].[EncounterChiefComplaints]
GO
CREATE INDEX [IX_FK_EncounterChiefComplaintEncounter]
ON [model].[EncounterChiefComplaints]
    ([EncounterId]);
GO

-- Creating foreign key on [BodyPartId] in table 'EncounterLaboratoryTestOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterLaboratoryTestOrderBodyPart'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT FK_EncounterLaboratoryTestOrderBodyPart

IF OBJECTPROPERTY(OBJECT_ID('[model].[BodyParts]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterLaboratoryTestOrders]
ADD CONSTRAINT [FK_EncounterLaboratoryTestOrderBodyPart]
    FOREIGN KEY ([BodyPartId])
    REFERENCES [model].[BodyParts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterLaboratoryTestOrderBodyPart'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterLaboratoryTestOrderBodyPart')
	DROP INDEX IX_FK_EncounterLaboratoryTestOrderBodyPart ON [model].[EncounterLaboratoryTestOrders]
GO
CREATE INDEX [IX_FK_EncounterLaboratoryTestOrderBodyPart]
ON [model].[EncounterLaboratoryTestOrders]
    ([BodyPartId]);
GO

-- Creating foreign key on [OtherTreatmentPlanOrderId] in table 'EncounterOtherTreatmentPlanOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterOtherTreatmentPlanOrderOtherTreatmentPlanOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterOtherTreatmentPlanOrders] DROP CONSTRAINT FK_EncounterOtherTreatmentPlanOrderOtherTreatmentPlanOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[OtherTreatmentPlanOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterOtherTreatmentPlanOrders]
ADD CONSTRAINT [FK_EncounterOtherTreatmentPlanOrderOtherTreatmentPlanOrder]
    FOREIGN KEY ([OtherTreatmentPlanOrderId])
    REFERENCES [model].[OtherTreatmentPlanOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterOtherTreatmentPlanOrderOtherTreatmentPlanOrder'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterOtherTreatmentPlanOrderOtherTreatmentPlanOrder')
	DROP INDEX IX_FK_EncounterOtherTreatmentPlanOrderOtherTreatmentPlanOrder ON [model].[EncounterOtherTreatmentPlanOrders]
GO
CREATE INDEX [IX_FK_EncounterOtherTreatmentPlanOrderOtherTreatmentPlanOrder]
ON [model].[EncounterOtherTreatmentPlanOrders]
    ([OtherTreatmentPlanOrderId]);
GO

-- Creating foreign key on [LaboratoryTestingPriorityId] in table 'EncounterLaboratoryTestOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterLaboratoryTestOrderLaboratoryTestingPriority'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT FK_EncounterLaboratoryTestOrderLaboratoryTestingPriority

IF OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTestingPriorities]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterLaboratoryTestOrders]
ADD CONSTRAINT [FK_EncounterLaboratoryTestOrderLaboratoryTestingPriority]
    FOREIGN KEY ([LaboratoryTestingPriorityId])
    REFERENCES [model].[LaboratoryTestingPriorities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterLaboratoryTestOrderLaboratoryTestingPriority'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterLaboratoryTestOrderLaboratoryTestingPriority')
	DROP INDEX IX_FK_EncounterLaboratoryTestOrderLaboratoryTestingPriority ON [model].[EncounterLaboratoryTestOrders]
GO
CREATE INDEX [IX_FK_EncounterLaboratoryTestOrderLaboratoryTestingPriority]
ON [model].[EncounterLaboratoryTestOrders]
    ([LaboratoryTestingPriorityId]);
GO

-- Creating foreign key on [RefractivePrescriptionId] in table 'EncounterRefractivePrescriptions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterRefractivePrescriptionRefractivePrescription'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterRefractivePrescriptions] DROP CONSTRAINT FK_EncounterRefractivePrescriptionRefractivePrescription

IF OBJECTPROPERTY(OBJECT_ID('[model].[RefractivePrescriptions]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterRefractivePrescriptions]
ADD CONSTRAINT [FK_EncounterRefractivePrescriptionRefractivePrescription]
    FOREIGN KEY ([RefractivePrescriptionId])
    REFERENCES [model].[RefractivePrescriptions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterRefractivePrescriptionRefractivePrescription'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterRefractivePrescriptionRefractivePrescription')
	DROP INDEX IX_FK_EncounterRefractivePrescriptionRefractivePrescription ON [model].[EncounterRefractivePrescriptions]
GO
CREATE INDEX [IX_FK_EncounterRefractivePrescriptionRefractivePrescription]
ON [model].[EncounterRefractivePrescriptions]
    ([RefractivePrescriptionId]);
GO

-- Creating foreign key on [DoctorId] in table 'EncounterProcedureOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterProcedureOrderDoctor'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT FK_EncounterProcedureOrderDoctor

IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterProcedureOrders]
ADD CONSTRAINT [FK_EncounterProcedureOrderDoctor]
    FOREIGN KEY ([DoctorId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterProcedureOrderDoctor'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterProcedureOrderDoctor')
	DROP INDEX IX_FK_EncounterProcedureOrderDoctor ON [model].[EncounterProcedureOrders]
GO
CREATE INDEX [IX_FK_EncounterProcedureOrderDoctor]
ON [model].[EncounterProcedureOrders]
    ([DoctorId]);
GO

-- Creating foreign key on [RelativeTimeTypeId] in table 'EncounterProcedureOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterProcedureOrderRelativeTimeType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT FK_EncounterProcedureOrderRelativeTimeType

IF OBJECTPROPERTY(OBJECT_ID('[model].[RelativeTimeTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterProcedureOrders]
ADD CONSTRAINT [FK_EncounterProcedureOrderRelativeTimeType]
    FOREIGN KEY ([RelativeTimeTypeId])
    REFERENCES [model].[RelativeTimeTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterProcedureOrderRelativeTimeType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterProcedureOrderRelativeTimeType')
	DROP INDEX IX_FK_EncounterProcedureOrderRelativeTimeType ON [model].[EncounterProcedureOrders]
GO
CREATE INDEX [IX_FK_EncounterProcedureOrderRelativeTimeType]
ON [model].[EncounterProcedureOrders]
    ([RelativeTimeTypeId]);
GO

-- Creating foreign key on [TreatmentPlanCommentId] in table 'UserTreatmentPlans'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_UserTreatmentPlanTreatmentPlanComment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[UserTreatmentPlans] DROP CONSTRAINT FK_UserTreatmentPlanTreatmentPlanComment

IF OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentPlanComments]'), 'IsUserTable') = 1
ALTER TABLE [model].[UserTreatmentPlans]
ADD CONSTRAINT [FK_UserTreatmentPlanTreatmentPlanComment]
    FOREIGN KEY ([TreatmentPlanCommentId])
    REFERENCES [model].[TreatmentPlanComments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UserTreatmentPlanTreatmentPlanComment'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_UserTreatmentPlanTreatmentPlanComment')
	DROP INDEX IX_FK_UserTreatmentPlanTreatmentPlanComment ON [model].[UserTreatmentPlans]
GO
CREATE INDEX [IX_FK_UserTreatmentPlanTreatmentPlanComment]
ON [model].[UserTreatmentPlans]
    ([TreatmentPlanCommentId]);
GO

-- Creating foreign key on [EncounterCommunicationWithOtherProviderOrderId] in table 'CommunicationTransactions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_CommunicationTransactionEncounterCommunicationWithOtherProviderOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[CommunicationTransactions] DROP CONSTRAINT FK_CommunicationTransactionEncounterCommunicationWithOtherProviderOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterCommunicationWithOtherProviderOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[CommunicationTransactions]
ADD CONSTRAINT [FK_CommunicationTransactionEncounterCommunicationWithOtherProviderOrder]
    FOREIGN KEY ([EncounterCommunicationWithOtherProviderOrderId])
    REFERENCES [model].[EncounterCommunicationWithOtherProviderOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CommunicationTransactionEncounterCommunicationWithOtherProviderOrder'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_CommunicationTransactionEncounterCommunicationWithOtherProviderOrder')
	DROP INDEX IX_FK_CommunicationTransactionEncounterCommunicationWithOtherProviderOrder ON [model].[CommunicationTransactions]
GO
CREATE INDEX [IX_FK_CommunicationTransactionEncounterCommunicationWithOtherProviderOrder]
ON [model].[CommunicationTransactions]
    ([EncounterCommunicationWithOtherProviderOrderId]);
GO

-- Creating foreign key on [ExternalProviderId] in table 'CommunicationTransactions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_CommunicationTransactionExternalProvider'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[CommunicationTransactions] DROP CONSTRAINT FK_CommunicationTransactionExternalProvider

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContacts]'), 'IsUserTable') = 1
ALTER TABLE [model].[CommunicationTransactions]
ADD CONSTRAINT [FK_CommunicationTransactionExternalProvider]
    FOREIGN KEY ([ExternalProviderId])
    REFERENCES [model].[ExternalContacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CommunicationTransactionExternalProvider'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_CommunicationTransactionExternalProvider')
	DROP INDEX IX_FK_CommunicationTransactionExternalProvider ON [model].[CommunicationTransactions]
GO
CREATE INDEX [IX_FK_CommunicationTransactionExternalProvider]
ON [model].[CommunicationTransactions]
    ([ExternalProviderId]);
GO

-- Creating foreign key on [PatientId] in table 'CommunicationTransactions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_CommunicationTransactionPatient'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[CommunicationTransactions] DROP CONSTRAINT FK_CommunicationTransactionPatient

IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[CommunicationTransactions]
ADD CONSTRAINT [FK_CommunicationTransactionPatient]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CommunicationTransactionPatient'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_CommunicationTransactionPatient')
	DROP INDEX IX_FK_CommunicationTransactionPatient ON [model].[CommunicationTransactions]
GO
CREATE INDEX [IX_FK_CommunicationTransactionPatient]
ON [model].[CommunicationTransactions]
    ([PatientId]);
GO

-- Creating foreign key on [PatientMedicationDetailId] in table 'EncounterMedicationOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterMedicationOrderPatientMedicationDetail'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterMedicationOrders] DROP CONSTRAINT FK_EncounterMedicationOrderPatientMedicationDetail

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientMedicationDetails]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterMedicationOrders]
ADD CONSTRAINT [FK_EncounterMedicationOrderPatientMedicationDetail]
    FOREIGN KEY ([PatientMedicationDetailId])
    REFERENCES [model].[PatientMedicationDetails]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterMedicationOrderPatientMedicationDetail'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterMedicationOrderPatientMedicationDetail')
	DROP INDEX IX_FK_EncounterMedicationOrderPatientMedicationDetail ON [model].[EncounterMedicationOrders]
GO
CREATE INDEX [IX_FK_EncounterMedicationOrderPatientMedicationDetail]
ON [model].[EncounterMedicationOrders]
    ([PatientMedicationDetailId]);
GO

-- Creating foreign key on [ObservationTypeCategoryId] in table 'ObservationTypes'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ObservationTypeObservationTypeCategory'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ObservationTypes] DROP CONSTRAINT FK_ObservationTypeObservationTypeCategory

IF OBJECTPROPERTY(OBJECT_ID('[model].[ObservationTypeCategories]'), 'IsUserTable') = 1
ALTER TABLE [model].[ObservationTypes]
ADD CONSTRAINT [FK_ObservationTypeObservationTypeCategory]
    FOREIGN KEY ([ObservationTypeCategoryId])
    REFERENCES [model].[ObservationTypeCategories]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ObservationTypeObservationTypeCategory'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ObservationTypeObservationTypeCategory')
	DROP INDEX IX_FK_ObservationTypeObservationTypeCategory ON [model].[ObservationTypes]
GO
CREATE INDEX [IX_FK_ObservationTypeObservationTypeCategory]
ON [model].[ObservationTypes]
    ([ObservationTypeCategoryId]);
GO

-- Creating foreign key on [ClinicalProcedureId] in table 'PatientSpecialExamPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientSpecialExamPerformedClinicalProcedure'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientSpecialExamPerformeds] DROP CONSTRAINT FK_PatientSpecialExamPerformedClinicalProcedure

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalProcedures]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientSpecialExamPerformeds]
ADD CONSTRAINT [FK_PatientSpecialExamPerformedClinicalProcedure]
    FOREIGN KEY ([ClinicalProcedureId])
    REFERENCES [model].[ClinicalProcedures]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSpecialExamPerformedClinicalProcedure'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientSpecialExamPerformedClinicalProcedure')
	DROP INDEX IX_FK_PatientSpecialExamPerformedClinicalProcedure ON [model].[PatientSpecialExamPerformeds]
GO
CREATE INDEX [IX_FK_PatientSpecialExamPerformedClinicalProcedure]
ON [model].[PatientSpecialExamPerformeds]
    ([ClinicalProcedureId]);
GO

-- Creating foreign key on [ClinicalProcedureId] in table 'PatientOtherElementPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientOtherElementPerformedClinicalProcedure'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientOtherElementPerformeds] DROP CONSTRAINT FK_PatientOtherElementPerformedClinicalProcedure

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalProcedures]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientOtherElementPerformeds]
ADD CONSTRAINT [FK_PatientOtherElementPerformedClinicalProcedure]
    FOREIGN KEY ([ClinicalProcedureId])
    REFERENCES [model].[ClinicalProcedures]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientOtherElementPerformedClinicalProcedure'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientOtherElementPerformedClinicalProcedure')
	DROP INDEX IX_FK_PatientOtherElementPerformedClinicalProcedure ON [model].[PatientOtherElementPerformeds]
GO
CREATE INDEX [IX_FK_PatientOtherElementPerformedClinicalProcedure]
ON [model].[PatientOtherElementPerformeds]
    ([ClinicalProcedureId]);
GO

-- Creating foreign key on [ChiefComplaintCategoryId] in table 'ChiefComplaints'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ChiefComplaintChiefComplaintCategory'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ChiefComplaints] DROP CONSTRAINT FK_ChiefComplaintChiefComplaintCategory

IF OBJECTPROPERTY(OBJECT_ID('[model].[ChiefComplaintCategories]'), 'IsUserTable') = 1
ALTER TABLE [model].[ChiefComplaints]
ADD CONSTRAINT [FK_ChiefComplaintChiefComplaintCategory]
    FOREIGN KEY ([ChiefComplaintCategoryId])
    REFERENCES [model].[ChiefComplaintCategories]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ChiefComplaintChiefComplaintCategory'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ChiefComplaintChiefComplaintCategory')
	DROP INDEX IX_FK_ChiefComplaintChiefComplaintCategory ON [model].[ChiefComplaints]
GO
CREATE INDEX [IX_FK_ChiefComplaintChiefComplaintCategory]
ON [model].[ChiefComplaints]
    ([ChiefComplaintCategoryId]);
GO

-- Creating foreign key on [ChiefComplaintId] in table 'EncounterChiefComplaints'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterChiefComplaintChiefComplaint'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterChiefComplaints] DROP CONSTRAINT FK_EncounterChiefComplaintChiefComplaint

IF OBJECTPROPERTY(OBJECT_ID('[model].[ChiefComplaints]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterChiefComplaints]
ADD CONSTRAINT [FK_EncounterChiefComplaintChiefComplaint]
    FOREIGN KEY ([ChiefComplaintId])
    REFERENCES [model].[ChiefComplaints]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterChiefComplaintChiefComplaint'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterChiefComplaintChiefComplaint')
	DROP INDEX IX_FK_EncounterChiefComplaintChiefComplaint ON [model].[EncounterChiefComplaints]
GO
CREATE INDEX [IX_FK_EncounterChiefComplaintChiefComplaint]
ON [model].[EncounterChiefComplaints]
    ([ChiefComplaintId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterChiefComplaintComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterChiefComplaintCommentEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterChiefComplaintComments] DROP CONSTRAINT FK_EncounterChiefComplaintCommentEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterChiefComplaintComments]
ADD CONSTRAINT [FK_EncounterChiefComplaintCommentEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterChiefComplaintCommentEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterChiefComplaintCommentEncounter')
	DROP INDEX IX_FK_EncounterChiefComplaintCommentEncounter ON [model].[EncounterChiefComplaintComments]
GO
CREATE INDEX [IX_FK_EncounterChiefComplaintCommentEncounter]
ON [model].[EncounterChiefComplaintComments]
    ([EncounterId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterReasonForVisitComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterReasonForVisitCommentEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterReasonForVisitComments] DROP CONSTRAINT FK_EncounterReasonForVisitCommentEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterReasonForVisitComments]
ADD CONSTRAINT [FK_EncounterReasonForVisitCommentEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterReasonForVisitCommentEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterReasonForVisitCommentEncounter')
	DROP INDEX IX_FK_EncounterReasonForVisitCommentEncounter ON [model].[EncounterReasonForVisitComments]
GO
CREATE INDEX [IX_FK_EncounterReasonForVisitCommentEncounter]
ON [model].[EncounterReasonForVisitComments]
    ([EncounterId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterReviewOfSystemComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterReviewOfSystemCommentEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterReviewOfSystemComments] DROP CONSTRAINT FK_EncounterReviewOfSystemCommentEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterReviewOfSystemComments]
ADD CONSTRAINT [FK_EncounterReviewOfSystemCommentEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterReviewOfSystemCommentEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterReviewOfSystemCommentEncounter')
	DROP INDEX IX_FK_EncounterReviewOfSystemCommentEncounter ON [model].[EncounterReviewOfSystemComments]
GO
CREATE INDEX [IX_FK_EncounterReviewOfSystemCommentEncounter]
ON [model].[EncounterReviewOfSystemComments]
    ([EncounterId]);
GO

-- Creating foreign key on [BodySystemId] in table 'ReviewOfSystems'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ReviewOfSystemBodySystem'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ReviewOfSystems] DROP CONSTRAINT FK_ReviewOfSystemBodySystem

IF OBJECTPROPERTY(OBJECT_ID('[model].[BodySystems]'), 'IsUserTable') = 1
ALTER TABLE [model].[ReviewOfSystems]
ADD CONSTRAINT [FK_ReviewOfSystemBodySystem]
    FOREIGN KEY ([BodySystemId])
    REFERENCES [model].[BodySystems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ReviewOfSystemBodySystem'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_ReviewOfSystemBodySystem')
	DROP INDEX IX_FK_ReviewOfSystemBodySystem ON [model].[ReviewOfSystems]
GO
CREATE INDEX [IX_FK_ReviewOfSystemBodySystem]
ON [model].[ReviewOfSystems]
    ([BodySystemId]);
GO

-- Creating foreign key on [BodyLocationId] in table 'PatientDiagnosisDetails'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisDetailBodyLocation'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT FK_PatientDiagnosisDetailBodyLocation

IF OBJECTPROPERTY(OBJECT_ID('[model].[BodyLocations]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailBodyLocation]
    FOREIGN KEY ([BodyLocationId])
    REFERENCES [model].[BodyLocations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailBodyLocation'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisDetailBodyLocation')
	DROP INDEX IX_FK_PatientDiagnosisDetailBodyLocation ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailBodyLocation]
ON [model].[PatientDiagnosisDetails]
    ([BodyLocationId]);
GO

-- Creating foreign key on [EncounterId] in table 'PatientDiagnosisDetails'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisDetailEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT FK_PatientDiagnosisDetailEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisDetailEncounter')
	DROP INDEX IX_FK_PatientDiagnosisDetailEncounter ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailEncounter]
ON [model].[PatientDiagnosisDetails]
    ([EncounterId]);
GO

-- Creating foreign key on [EncounterId] in table 'PatientDiagnosisComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisCommentEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT FK_PatientDiagnosisCommentEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisComments]
ADD CONSTRAINT [FK_PatientDiagnosisCommentEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisCommentEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisCommentEncounter')
	DROP INDEX IX_FK_PatientDiagnosisCommentEncounter ON [model].[PatientDiagnosisComments]
GO
CREATE INDEX [IX_FK_PatientDiagnosisCommentEncounter]
ON [model].[PatientDiagnosisComments]
    ([EncounterId]);
GO

-- Creating foreign key on [OrganStructureId] in table 'PatientDiagnosisComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisCommentOrganStructure'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT FK_PatientDiagnosisCommentOrganStructure

IF OBJECTPROPERTY(OBJECT_ID('[model].[OrganStructures]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisComments]
ADD CONSTRAINT [FK_PatientDiagnosisCommentOrganStructure]
    FOREIGN KEY ([OrganStructureId])
    REFERENCES [model].[OrganStructures]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisCommentOrganStructure'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisCommentOrganStructure')
	DROP INDEX IX_FK_PatientDiagnosisCommentOrganStructure ON [model].[PatientDiagnosisComments]
GO
CREATE INDEX [IX_FK_PatientDiagnosisCommentOrganStructure]
ON [model].[PatientDiagnosisComments]
    ([OrganStructureId]);
GO

-- Creating foreign key on [BodyPartId] in table 'PatientDiagnosisComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisCommentBodyPart'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisComments] DROP CONSTRAINT FK_PatientDiagnosisCommentBodyPart

IF OBJECTPROPERTY(OBJECT_ID('[model].[BodyParts]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisComments]
ADD CONSTRAINT [FK_PatientDiagnosisCommentBodyPart]
    FOREIGN KEY ([BodyPartId])
    REFERENCES [model].[BodyParts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisCommentBodyPart'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisCommentBodyPart')
	DROP INDEX IX_FK_PatientDiagnosisCommentBodyPart ON [model].[PatientDiagnosisComments]
GO
CREATE INDEX [IX_FK_PatientDiagnosisCommentBodyPart]
ON [model].[PatientDiagnosisComments]
    ([BodyPartId]);
GO

-- Creating foreign key on [PatientDiagnosisDetailId] in table 'PatientDiagnosisDetailAxisQualifiers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisDetailAxisQualifierPatientDiagnosisDetail'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisDetailAxisQualifiers] DROP CONSTRAINT FK_PatientDiagnosisDetailAxisQualifierPatientDiagnosisDetail

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosisDetails]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetailAxisQualifiers]
ADD CONSTRAINT [FK_PatientDiagnosisDetailAxisQualifierPatientDiagnosisDetail]
    FOREIGN KEY ([PatientDiagnosisDetailId])
    REFERENCES [model].[PatientDiagnosisDetails]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailAxisQualifierPatientDiagnosisDetail'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisDetailAxisQualifierPatientDiagnosisDetail')
	DROP INDEX IX_FK_PatientDiagnosisDetailAxisQualifierPatientDiagnosisDetail ON [model].[PatientDiagnosisDetailAxisQualifiers]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailAxisQualifierPatientDiagnosisDetail]
ON [model].[PatientDiagnosisDetailAxisQualifiers]
    ([PatientDiagnosisDetailId]);
GO

-- Creating foreign key on [AxisQualifierId] in table 'PatientDiagnosisDetailAxisQualifiers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisDetailAxisQualifierAxisQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisDetailAxisQualifiers] DROP CONSTRAINT FK_PatientDiagnosisDetailAxisQualifierAxisQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[AxisQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetailAxisQualifiers]
ADD CONSTRAINT [FK_PatientDiagnosisDetailAxisQualifierAxisQualifier]
    FOREIGN KEY ([AxisQualifierId])
    REFERENCES [model].[AxisQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailAxisQualifierAxisQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisDetailAxisQualifierAxisQualifier')
	DROP INDEX IX_FK_PatientDiagnosisDetailAxisQualifierAxisQualifier ON [model].[PatientDiagnosisDetailAxisQualifiers]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailAxisQualifierAxisQualifier]
ON [model].[PatientDiagnosisDetailAxisQualifiers]
    ([AxisQualifierId]);
GO

-- Creating foreign key on [ClinicalAttributeId] in table 'PatientDiagnoses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisClinicalAttribute'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnoses] DROP CONSTRAINT FK_PatientDiagnosisClinicalAttribute

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalAttributes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnoses]
ADD CONSTRAINT [FK_PatientDiagnosisClinicalAttribute]
    FOREIGN KEY ([ClinicalAttributeId])
    REFERENCES [model].[ClinicalAttributes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisClinicalAttribute'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisClinicalAttribute')
	DROP INDEX IX_FK_PatientDiagnosisClinicalAttribute ON [model].[PatientDiagnoses]
GO
CREATE INDEX [IX_FK_PatientDiagnosisClinicalAttribute]
ON [model].[PatientDiagnoses]
    ([ClinicalAttributeId]);
GO

-- Creating foreign key on [ToPatientDiagnosisId] in table 'PatientDiagnoses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisAssociatedWithPatientDiagnosis'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnoses] DROP CONSTRAINT FK_PatientDiagnosisAssociatedWithPatientDiagnosis

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnoses]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnoses]
ADD CONSTRAINT [FK_PatientDiagnosisAssociatedWithPatientDiagnosis]
    FOREIGN KEY ([ToPatientDiagnosisId])
    REFERENCES [model].[PatientDiagnoses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisAssociatedWithPatientDiagnosis'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisAssociatedWithPatientDiagnosis')
	DROP INDEX IX_FK_PatientDiagnosisAssociatedWithPatientDiagnosis ON [model].[PatientDiagnoses]
GO
CREATE INDEX [IX_FK_PatientDiagnosisAssociatedWithPatientDiagnosis]
ON [model].[PatientDiagnoses]
    ([ToPatientDiagnosisId]);
GO

-- Creating foreign key on [PatientId] in table 'PatientFamilyHistoryEntries'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientFamilyDiagnosisPatient'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientFamilyHistoryEntries] DROP CONSTRAINT FK_PatientFamilyDiagnosisPatient

IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientFamilyHistoryEntries]
ADD CONSTRAINT [FK_PatientFamilyDiagnosisPatient]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientFamilyDiagnosisPatient'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientFamilyDiagnosisPatient')
	DROP INDEX IX_FK_PatientFamilyDiagnosisPatient ON [model].[PatientFamilyHistoryEntries]
GO
CREATE INDEX [IX_FK_PatientFamilyDiagnosisPatient]
ON [model].[PatientFamilyHistoryEntries]
    ([PatientId]);
GO

-- Creating foreign key on [ClinicalConditionId] in table 'PatientFamilyHistoryEntries'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientFamilyDiagnosisClinicalCondition'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientFamilyHistoryEntries] DROP CONSTRAINT FK_PatientFamilyDiagnosisClinicalCondition

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalConditions]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientFamilyHistoryEntries]
ADD CONSTRAINT [FK_PatientFamilyDiagnosisClinicalCondition]
    FOREIGN KEY ([ClinicalConditionId])
    REFERENCES [model].[ClinicalConditions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientFamilyDiagnosisClinicalCondition'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientFamilyDiagnosisClinicalCondition')
	DROP INDEX IX_FK_PatientFamilyDiagnosisClinicalCondition ON [model].[PatientFamilyHistoryEntries]
GO
CREATE INDEX [IX_FK_PatientFamilyDiagnosisClinicalCondition]
ON [model].[PatientFamilyHistoryEntries]
    ([ClinicalConditionId]);
GO

-- Creating foreign key on [ClinicalDataSourceTypeId] in table 'PatientFamilyHistoryEntries'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientFamilyDiagnosisClinicalDataSourceType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientFamilyHistoryEntries] DROP CONSTRAINT FK_PatientFamilyDiagnosisClinicalDataSourceType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalDataSourceTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientFamilyHistoryEntries]
ADD CONSTRAINT [FK_PatientFamilyDiagnosisClinicalDataSourceType]
    FOREIGN KEY ([ClinicalDataSourceTypeId])
    REFERENCES [model].[ClinicalDataSourceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientFamilyDiagnosisClinicalDataSourceType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientFamilyDiagnosisClinicalDataSourceType')
	DROP INDEX IX_FK_PatientFamilyDiagnosisClinicalDataSourceType ON [model].[PatientFamilyHistoryEntries]
GO
CREATE INDEX [IX_FK_PatientFamilyDiagnosisClinicalDataSourceType]
ON [model].[PatientFamilyHistoryEntries]
    ([ClinicalDataSourceTypeId]);
GO

-- Creating foreign key on [PatientFamilyHistoryEntryId] in table 'PatientFamilyHistoryEntryDetails'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientFamilyHistoryEntryDetailPatientFamilyHistoryEntry'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientFamilyHistoryEntryDetails] DROP CONSTRAINT FK_PatientFamilyHistoryEntryDetailPatientFamilyHistoryEntry

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientFamilyHistoryEntries]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientFamilyHistoryEntryDetails]
ADD CONSTRAINT [FK_PatientFamilyHistoryEntryDetailPatientFamilyHistoryEntry]
    FOREIGN KEY ([PatientFamilyHistoryEntryId])
    REFERENCES [model].[PatientFamilyHistoryEntries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientFamilyHistoryEntryDetailPatientFamilyHistoryEntry'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientFamilyHistoryEntryDetailPatientFamilyHistoryEntry')
	DROP INDEX IX_FK_PatientFamilyHistoryEntryDetailPatientFamilyHistoryEntry ON [model].[PatientFamilyHistoryEntryDetails]
GO
CREATE INDEX [IX_FK_PatientFamilyHistoryEntryDetailPatientFamilyHistoryEntry]
ON [model].[PatientFamilyHistoryEntryDetails]
    ([PatientFamilyHistoryEntryId]);
GO

-- Creating foreign key on [UserId] in table 'PatientFamilyHistoryEntryDetails'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientFamilyHistoryEntryDetailUser'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientFamilyHistoryEntryDetails] DROP CONSTRAINT FK_PatientFamilyHistoryEntryDetailUser

IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientFamilyHistoryEntryDetails]
ADD CONSTRAINT [FK_PatientFamilyHistoryEntryDetailUser]
    FOREIGN KEY ([UserId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientFamilyHistoryEntryDetailUser'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientFamilyHistoryEntryDetailUser')
	DROP INDEX IX_FK_PatientFamilyHistoryEntryDetailUser ON [model].[PatientFamilyHistoryEntryDetails]
GO
CREATE INDEX [IX_FK_PatientFamilyHistoryEntryDetailUser]
ON [model].[PatientFamilyHistoryEntryDetails]
    ([UserId]);
GO

-- Creating foreign key on [AccuracyQualifierId] in table 'PatientFamilyHistoryEntryDetails'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientFamilyHistoryEntryDetailAccuracyQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientFamilyHistoryEntryDetails] DROP CONSTRAINT FK_PatientFamilyHistoryEntryDetailAccuracyQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[AccuracyQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientFamilyHistoryEntryDetails]
ADD CONSTRAINT [FK_PatientFamilyHistoryEntryDetailAccuracyQualifier]
    FOREIGN KEY ([AccuracyQualifierId])
    REFERENCES [model].[AccuracyQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientFamilyHistoryEntryDetailAccuracyQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientFamilyHistoryEntryDetailAccuracyQualifier')
	DROP INDEX IX_FK_PatientFamilyHistoryEntryDetailAccuracyQualifier ON [model].[PatientFamilyHistoryEntryDetails]
GO
CREATE INDEX [IX_FK_PatientFamilyHistoryEntryDetailAccuracyQualifier]
ON [model].[PatientFamilyHistoryEntryDetails]
    ([AccuracyQualifierId]);
GO

-- Creating foreign key on [RelevancyQualifierId] in table 'PatientFamilyHistoryEntryDetails'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientFamilyHistoryEntryDetailRelevancyQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientFamilyHistoryEntryDetails] DROP CONSTRAINT FK_PatientFamilyHistoryEntryDetailRelevancyQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[RelevancyQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientFamilyHistoryEntryDetails]
ADD CONSTRAINT [FK_PatientFamilyHistoryEntryDetailRelevancyQualifier]
    FOREIGN KEY ([RelevancyQualifierId])
    REFERENCES [model].[RelevancyQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientFamilyHistoryEntryDetailRelevancyQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientFamilyHistoryEntryDetailRelevancyQualifier')
	DROP INDEX IX_FK_PatientFamilyHistoryEntryDetailRelevancyQualifier ON [model].[PatientFamilyHistoryEntryDetails]
GO
CREATE INDEX [IX_FK_PatientFamilyHistoryEntryDetailRelevancyQualifier]
ON [model].[PatientFamilyHistoryEntryDetails]
    ([RelevancyQualifierId]);
GO

-- Creating foreign key on [PatientFamilyHistoryEntryDetails_Id] in table 'PatientFamilyHistoryEntryDetailClinicalQualifier'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientFamilyHistoryEntryDetailClinicalQualifier_PatientFamilyHistoryEntryDetail'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientFamilyHistoryEntryDetailClinicalQualifier] DROP CONSTRAINT FK_PatientFamilyHistoryEntryDetailClinicalQualifier_PatientFamilyHistoryEntryDetail

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientFamilyHistoryEntryDetails]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientFamilyHistoryEntryDetailClinicalQualifier]
ADD CONSTRAINT [FK_PatientFamilyHistoryEntryDetailClinicalQualifier_PatientFamilyHistoryEntryDetail]
    FOREIGN KEY ([PatientFamilyHistoryEntryDetails_Id])
    REFERENCES [model].[PatientFamilyHistoryEntryDetails]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalQualifiers_Id] in table 'PatientFamilyHistoryEntryDetailClinicalQualifier'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientFamilyHistoryEntryDetailClinicalQualifier_ClinicalQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientFamilyHistoryEntryDetailClinicalQualifier] DROP CONSTRAINT FK_PatientFamilyHistoryEntryDetailClinicalQualifier_ClinicalQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientFamilyHistoryEntryDetailClinicalQualifier]
ADD CONSTRAINT [FK_PatientFamilyHistoryEntryDetailClinicalQualifier_ClinicalQualifier]
    FOREIGN KEY ([ClinicalQualifiers_Id])
    REFERENCES [model].[ClinicalQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientFamilyHistoryEntryDetailClinicalQualifier_ClinicalQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientFamilyHistoryEntryDetailClinicalQualifier_ClinicalQualifier')
	DROP INDEX IX_FK_PatientFamilyHistoryEntryDetailClinicalQualifier_ClinicalQualifier ON [model].[PatientFamilyHistoryEntryDetailClinicalQualifier]
GO
CREATE INDEX [IX_FK_PatientFamilyHistoryEntryDetailClinicalQualifier_ClinicalQualifier]
ON [model].[PatientFamilyHistoryEntryDetailClinicalQualifier]
    ([ClinicalQualifiers_Id]);
GO

-- Creating foreign key on [PatientAllergenDetails_Id] in table 'PatientAllergenDetailClinicalQualifier'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientAllergenDetailClinicalQualifier_PatientAllergenDetail'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientAllergenDetailClinicalQualifier] DROP CONSTRAINT FK_PatientAllergenDetailClinicalQualifier_PatientAllergenDetail

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergenDetails]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientAllergenDetailClinicalQualifier]
ADD CONSTRAINT [FK_PatientAllergenDetailClinicalQualifier_PatientAllergenDetail]
    FOREIGN KEY ([PatientAllergenDetails_Id])
    REFERENCES [model].[PatientAllergenDetails]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalQualifiers_Id] in table 'PatientAllergenDetailClinicalQualifier'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientAllergenDetailClinicalQualifier_ClinicalQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientAllergenDetailClinicalQualifier] DROP CONSTRAINT FK_PatientAllergenDetailClinicalQualifier_ClinicalQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientAllergenDetailClinicalQualifier]
ADD CONSTRAINT [FK_PatientAllergenDetailClinicalQualifier_ClinicalQualifier]
    FOREIGN KEY ([ClinicalQualifiers_Id])
    REFERENCES [model].[ClinicalQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientAllergenDetailClinicalQualifier_ClinicalQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientAllergenDetailClinicalQualifier_ClinicalQualifier')
	DROP INDEX IX_FK_PatientAllergenDetailClinicalQualifier_ClinicalQualifier ON [model].[PatientAllergenDetailClinicalQualifier]
GO
CREATE INDEX [IX_FK_PatientAllergenDetailClinicalQualifier_ClinicalQualifier]
ON [model].[PatientAllergenDetailClinicalQualifier]
    ([ClinicalQualifiers_Id]);
GO

-- Creating foreign key on [PatientBloodPressures_Id] in table 'PatientBloodPressureClinicalQualifier'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientBloodPressureClinicalQualifier_PatientBloodPressure'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientBloodPressureClinicalQualifier] DROP CONSTRAINT FK_PatientBloodPressureClinicalQualifier_PatientBloodPressure

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientBloodPressures]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientBloodPressureClinicalQualifier]
ADD CONSTRAINT [FK_PatientBloodPressureClinicalQualifier_PatientBloodPressure]
    FOREIGN KEY ([PatientBloodPressures_Id])
    REFERENCES [model].[PatientBloodPressures]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalQualifiers_Id] in table 'PatientBloodPressureClinicalQualifier'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientBloodPressureClinicalQualifier_ClinicalQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientBloodPressureClinicalQualifier] DROP CONSTRAINT FK_PatientBloodPressureClinicalQualifier_ClinicalQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientBloodPressureClinicalQualifier]
ADD CONSTRAINT [FK_PatientBloodPressureClinicalQualifier_ClinicalQualifier]
    FOREIGN KEY ([ClinicalQualifiers_Id])
    REFERENCES [model].[ClinicalQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientBloodPressureClinicalQualifier_ClinicalQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientBloodPressureClinicalQualifier_ClinicalQualifier')
	DROP INDEX IX_FK_PatientBloodPressureClinicalQualifier_ClinicalQualifier ON [model].[PatientBloodPressureClinicalQualifier]
GO
CREATE INDEX [IX_FK_PatientBloodPressureClinicalQualifier_ClinicalQualifier]
ON [model].[PatientBloodPressureClinicalQualifier]
    ([ClinicalQualifiers_Id]);
GO

-- Creating foreign key on [PatientHeightAndWeights_Id] in table 'PatientHeightAndWeightClinicalQualifier'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientHeightAndWeightClinicalQualifier_PatientHeightAndWeight'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientHeightAndWeightClinicalQualifier] DROP CONSTRAINT FK_PatientHeightAndWeightClinicalQualifier_PatientHeightAndWeight

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientHeightAndWeights]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientHeightAndWeightClinicalQualifier]
ADD CONSTRAINT [FK_PatientHeightAndWeightClinicalQualifier_PatientHeightAndWeight]
    FOREIGN KEY ([PatientHeightAndWeights_Id])
    REFERENCES [model].[PatientHeightAndWeights]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalQualifiers_Id] in table 'PatientHeightAndWeightClinicalQualifier'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientHeightAndWeightClinicalQualifier_ClinicalQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientHeightAndWeightClinicalQualifier] DROP CONSTRAINT FK_PatientHeightAndWeightClinicalQualifier_ClinicalQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientHeightAndWeightClinicalQualifier]
ADD CONSTRAINT [FK_PatientHeightAndWeightClinicalQualifier_ClinicalQualifier]
    FOREIGN KEY ([ClinicalQualifiers_Id])
    REFERENCES [model].[ClinicalQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientHeightAndWeightClinicalQualifier_ClinicalQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientHeightAndWeightClinicalQualifier_ClinicalQualifier')
	DROP INDEX IX_FK_PatientHeightAndWeightClinicalQualifier_ClinicalQualifier ON [model].[PatientHeightAndWeightClinicalQualifier]
GO
CREATE INDEX [IX_FK_PatientHeightAndWeightClinicalQualifier_ClinicalQualifier]
ON [model].[PatientHeightAndWeightClinicalQualifier]
    ([ClinicalQualifiers_Id]);
GO

-- Creating foreign key on [PatientCognitiveStatusAssessments_Id] in table 'PatientCognitiveStatusAssessmentClinicalQualifier'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientCognitiveStatusAssessmentClinicalQualifier_PatientCognitiveStatusAssessment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientCognitiveStatusAssessmentClinicalQualifier] DROP CONSTRAINT FK_PatientCognitiveStatusAssessmentClinicalQualifier_PatientCognitiveStatusAssessment

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientCognitiveStatusAssessments]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientCognitiveStatusAssessmentClinicalQualifier]
ADD CONSTRAINT [FK_PatientCognitiveStatusAssessmentClinicalQualifier_PatientCognitiveStatusAssessment]
    FOREIGN KEY ([PatientCognitiveStatusAssessments_Id])
    REFERENCES [model].[PatientCognitiveStatusAssessments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalQualifiers_Id] in table 'PatientCognitiveStatusAssessmentClinicalQualifier'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientCognitiveStatusAssessmentClinicalQualifier_ClinicalQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientCognitiveStatusAssessmentClinicalQualifier] DROP CONSTRAINT FK_PatientCognitiveStatusAssessmentClinicalQualifier_ClinicalQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientCognitiveStatusAssessmentClinicalQualifier]
ADD CONSTRAINT [FK_PatientCognitiveStatusAssessmentClinicalQualifier_ClinicalQualifier]
    FOREIGN KEY ([ClinicalQualifiers_Id])
    REFERENCES [model].[ClinicalQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientCognitiveStatusAssessmentClinicalQualifier_ClinicalQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientCognitiveStatusAssessmentClinicalQualifier_ClinicalQualifier')
	DROP INDEX IX_FK_PatientCognitiveStatusAssessmentClinicalQualifier_ClinicalQualifier ON [model].[PatientCognitiveStatusAssessmentClinicalQualifier]
GO
CREATE INDEX [IX_FK_PatientCognitiveStatusAssessmentClinicalQualifier_ClinicalQualifier]
ON [model].[PatientCognitiveStatusAssessmentClinicalQualifier]
    ([ClinicalQualifiers_Id]);
GO

-- Creating foreign key on [PatientFunctionalStatusAssessments_Id] in table 'PatientFunctionalStatusAssessmentClinicalQualifier'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientFunctionalStatusAssessmentClinicalQualifier_PatientFunctionalStatusAssessment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientFunctionalStatusAssessmentClinicalQualifier] DROP CONSTRAINT FK_PatientFunctionalStatusAssessmentClinicalQualifier_PatientFunctionalStatusAssessment

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientFunctionalStatusAssessments]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientFunctionalStatusAssessmentClinicalQualifier]
ADD CONSTRAINT [FK_PatientFunctionalStatusAssessmentClinicalQualifier_PatientFunctionalStatusAssessment]
    FOREIGN KEY ([PatientFunctionalStatusAssessments_Id])
    REFERENCES [model].[PatientFunctionalStatusAssessments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClinicalQualifiers_Id] in table 'PatientFunctionalStatusAssessmentClinicalQualifier'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientFunctionalStatusAssessmentClinicalQualifier_ClinicalQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientFunctionalStatusAssessmentClinicalQualifier] DROP CONSTRAINT FK_PatientFunctionalStatusAssessmentClinicalQualifier_ClinicalQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientFunctionalStatusAssessmentClinicalQualifier]
ADD CONSTRAINT [FK_PatientFunctionalStatusAssessmentClinicalQualifier_ClinicalQualifier]
    FOREIGN KEY ([ClinicalQualifiers_Id])
    REFERENCES [model].[ClinicalQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientFunctionalStatusAssessmentClinicalQualifier_ClinicalQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientFunctionalStatusAssessmentClinicalQualifier_ClinicalQualifier')
	DROP INDEX IX_FK_PatientFunctionalStatusAssessmentClinicalQualifier_ClinicalQualifier ON [model].[PatientFunctionalStatusAssessmentClinicalQualifier]
GO
CREATE INDEX [IX_FK_PatientFunctionalStatusAssessmentClinicalQualifier_ClinicalQualifier]
ON [model].[PatientFunctionalStatusAssessmentClinicalQualifier]
    ([ClinicalQualifiers_Id]);
GO

-- Creating foreign key on [EncounterTreatmentGoalAndInstructionId] in table 'PatientDiagnosisDetails'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisDetailEncounterTreatmentGoal'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnosisDetails] DROP CONSTRAINT FK_PatientDiagnosisDetailEncounterTreatmentGoal

IF OBJECTPROPERTY(OBJECT_ID('[model].[EncounterTreatmentGoalAndInstructions]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnosisDetails]
ADD CONSTRAINT [FK_PatientDiagnosisDetailEncounterTreatmentGoal]
    FOREIGN KEY ([EncounterTreatmentGoalAndInstructionId])
    REFERENCES [model].[EncounterTreatmentGoalAndInstructions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisDetailEncounterTreatmentGoal'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisDetailEncounterTreatmentGoal')
	DROP INDEX IX_FK_PatientDiagnosisDetailEncounterTreatmentGoal ON [model].[PatientDiagnosisDetails]
GO
CREATE INDEX [IX_FK_PatientDiagnosisDetailEncounterTreatmentGoal]
ON [model].[PatientDiagnosisDetails]
    ([EncounterTreatmentGoalAndInstructionId]);
GO

-- Creating foreign key on [ClinicalDataSourceTypeId] in table 'PatientOtherElementPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientOtherElementPerformedClinicalDataSourceType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientOtherElementPerformeds] DROP CONSTRAINT FK_PatientOtherElementPerformedClinicalDataSourceType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalDataSourceTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientOtherElementPerformeds]
ADD CONSTRAINT [FK_PatientOtherElementPerformedClinicalDataSourceType]
    FOREIGN KEY ([ClinicalDataSourceTypeId])
    REFERENCES [model].[ClinicalDataSourceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientOtherElementPerformedClinicalDataSourceType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientOtherElementPerformedClinicalDataSourceType')
	DROP INDEX IX_FK_PatientOtherElementPerformedClinicalDataSourceType ON [model].[PatientOtherElementPerformeds]
GO
CREATE INDEX [IX_FK_PatientOtherElementPerformedClinicalDataSourceType]
ON [model].[PatientOtherElementPerformeds]
    ([ClinicalDataSourceTypeId]);
GO

-- Creating foreign key on [ClinicalDataSourceTypeId] in table 'PatientSpecialExamPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientSpecialExamPerformedClinicalDataSourceType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientSpecialExamPerformeds] DROP CONSTRAINT FK_PatientSpecialExamPerformedClinicalDataSourceType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalDataSourceTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientSpecialExamPerformeds]
ADD CONSTRAINT [FK_PatientSpecialExamPerformedClinicalDataSourceType]
    FOREIGN KEY ([ClinicalDataSourceTypeId])
    REFERENCES [model].[ClinicalDataSourceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSpecialExamPerformedClinicalDataSourceType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientSpecialExamPerformedClinicalDataSourceType')
	DROP INDEX IX_FK_PatientSpecialExamPerformedClinicalDataSourceType ON [model].[PatientSpecialExamPerformeds]
GO
CREATE INDEX [IX_FK_PatientSpecialExamPerformedClinicalDataSourceType]
ON [model].[PatientSpecialExamPerformeds]
    ([ClinicalDataSourceTypeId]);
GO

-- Creating foreign key on [BodyPartId] in table 'EncounterReasonForVisitComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterReasonForVisitCommentBodyPart'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterReasonForVisitComments] DROP CONSTRAINT FK_EncounterReasonForVisitCommentBodyPart

IF OBJECTPROPERTY(OBJECT_ID('[model].[BodyParts]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterReasonForVisitComments]
ADD CONSTRAINT [FK_EncounterReasonForVisitCommentBodyPart]
    FOREIGN KEY ([BodyPartId])
    REFERENCES [model].[BodyParts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterReasonForVisitCommentBodyPart'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterReasonForVisitCommentBodyPart')
	DROP INDEX IX_FK_EncounterReasonForVisitCommentBodyPart ON [model].[EncounterReasonForVisitComments]
GO
CREATE INDEX [IX_FK_EncounterReasonForVisitCommentBodyPart]
ON [model].[EncounterReasonForVisitComments]
    ([BodyPartId]);
GO

-- Creating foreign key on [UserId] in table 'EncounterReasonForVisitComments'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterReasonForVisitCommentUser'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterReasonForVisitComments] DROP CONSTRAINT FK_EncounterReasonForVisitCommentUser

IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterReasonForVisitComments]
ADD CONSTRAINT [FK_EncounterReasonForVisitCommentUser]
    FOREIGN KEY ([UserId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterReasonForVisitCommentUser'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterReasonForVisitCommentUser')
	DROP INDEX IX_FK_EncounterReasonForVisitCommentUser ON [model].[EncounterReasonForVisitComments]
GO
CREATE INDEX [IX_FK_EncounterReasonForVisitCommentUser]
ON [model].[EncounterReasonForVisitComments]
    ([UserId]);
GO

-- Creating foreign key on [CommunicationReviewStatusId] in table 'EncounterCommunicationWithOtherProviderOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterCommunicationWithOtherProviderOrderCommunicationReviewStatus'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders] DROP CONSTRAINT FK_EncounterCommunicationWithOtherProviderOrderCommunicationReviewStatus

IF OBJECTPROPERTY(OBJECT_ID('[model].[CommunicationCommunicationReviewStatus]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders]
ADD CONSTRAINT [FK_EncounterCommunicationWithOtherProviderOrderCommunicationReviewStatus]
    FOREIGN KEY ([CommunicationReviewStatusId])
    REFERENCES [model].[CommunicationCommunicationReviewStatus]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterCommunicationWithOtherProviderOrderCommunicationReviewStatus'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterCommunicationWithOtherProviderOrderCommunicationReviewStatus')
	DROP INDEX IX_FK_EncounterCommunicationWithOtherProviderOrderCommunicationReviewStatus ON [model].[EncounterCommunicationWithOtherProviderOrders]
GO
CREATE INDEX [IX_FK_EncounterCommunicationWithOtherProviderOrderCommunicationReviewStatus]
ON [model].[EncounterCommunicationWithOtherProviderOrders]
    ([CommunicationReviewStatusId]);
GO

-- Creating foreign key on [BodyPartId] in table 'EncounterProcedureOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterProcedureOrderBodyPart1'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT FK_EncounterProcedureOrderBodyPart1

IF OBJECTPROPERTY(OBJECT_ID('[model].[BodyParts]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterProcedureOrders]
ADD CONSTRAINT [FK_EncounterProcedureOrderBodyPart1]
    FOREIGN KEY ([BodyPartId])
    REFERENCES [model].[BodyParts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterProcedureOrderBodyPart1'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterProcedureOrderBodyPart1')
	DROP INDEX IX_FK_EncounterProcedureOrderBodyPart1 ON [model].[EncounterProcedureOrders]
GO
CREATE INDEX [IX_FK_EncounterProcedureOrderBodyPart1]
ON [model].[EncounterProcedureOrders]
    ([BodyPartId]);
GO

-- Creating foreign key on [UserTreatmentPlans_Id] in table 'UserTreatmentPlanPatientEducation'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_UserTreatmentPlanPatientEducation_UserTreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[UserTreatmentPlanPatientEducation] DROP CONSTRAINT FK_UserTreatmentPlanPatientEducation_UserTreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[UserTreatmentPlanPatientEducation]
ADD CONSTRAINT [FK_UserTreatmentPlanPatientEducation_UserTreatmentPlan]
    FOREIGN KEY ([UserTreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [PatientEducations_Id] in table 'UserTreatmentPlanPatientEducation'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_UserTreatmentPlanPatientEducation_PatientEducation'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[UserTreatmentPlanPatientEducation] DROP CONSTRAINT FK_UserTreatmentPlanPatientEducation_PatientEducation

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientEducations]'), 'IsUserTable') = 1
ALTER TABLE [model].[UserTreatmentPlanPatientEducation]
ADD CONSTRAINT [FK_UserTreatmentPlanPatientEducation_PatientEducation]
    FOREIGN KEY ([PatientEducations_Id])
    REFERENCES [model].[PatientEducations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UserTreatmentPlanPatientEducation_PatientEducation'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_UserTreatmentPlanPatientEducation_PatientEducation')
	DROP INDEX IX_FK_UserTreatmentPlanPatientEducation_PatientEducation ON [model].[UserTreatmentPlanPatientEducation]
GO
CREATE INDEX [IX_FK_UserTreatmentPlanPatientEducation_PatientEducation]
ON [model].[UserTreatmentPlanPatientEducation]
    ([PatientEducations_Id]);
GO

-- Creating foreign key on [UserTreatmentPlans_Id] in table 'UserTreatmentPlanVaccinationOrder'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_UserTreatmentPlanVaccinationOrder_UserTreatmentPlan'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[UserTreatmentPlanVaccinationOrder] DROP CONSTRAINT FK_UserTreatmentPlanVaccinationOrder_UserTreatmentPlan

IF OBJECTPROPERTY(OBJECT_ID('[model].[UserTreatmentPlans]'), 'IsUserTable') = 1
ALTER TABLE [model].[UserTreatmentPlanVaccinationOrder]
ADD CONSTRAINT [FK_UserTreatmentPlanVaccinationOrder_UserTreatmentPlan]
    FOREIGN KEY ([UserTreatmentPlans_Id])
    REFERENCES [model].[UserTreatmentPlans]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [VaccinationOrders_Id] in table 'UserTreatmentPlanVaccinationOrder'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_UserTreatmentPlanVaccinationOrder_VaccinationOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[UserTreatmentPlanVaccinationOrder] DROP CONSTRAINT FK_UserTreatmentPlanVaccinationOrder_VaccinationOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[VaccinationOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[UserTreatmentPlanVaccinationOrder]
ADD CONSTRAINT [FK_UserTreatmentPlanVaccinationOrder_VaccinationOrder]
    FOREIGN KEY ([VaccinationOrders_Id])
    REFERENCES [model].[VaccinationOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UserTreatmentPlanVaccinationOrder_VaccinationOrder'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_UserTreatmentPlanVaccinationOrder_VaccinationOrder')
	DROP INDEX IX_FK_UserTreatmentPlanVaccinationOrder_VaccinationOrder ON [model].[UserTreatmentPlanVaccinationOrder]
GO
CREATE INDEX [IX_FK_UserTreatmentPlanVaccinationOrder_VaccinationOrder]
ON [model].[UserTreatmentPlanVaccinationOrder]
    ([VaccinationOrders_Id]);
GO

-- Creating foreign key on [UnitOfMeasurementTypeId] in table 'UnitsOfMeasurement'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_UnitOfMeasurementUnitOfMeasurementType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[UnitsOfMeasurement] DROP CONSTRAINT FK_UnitOfMeasurementUnitOfMeasurementType

IF OBJECTPROPERTY(OBJECT_ID('[model].[UnitOfMeasurementTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[UnitsOfMeasurement]
ADD CONSTRAINT [FK_UnitOfMeasurementUnitOfMeasurementType]
    FOREIGN KEY ([UnitOfMeasurementTypeId])
    REFERENCES [model].[UnitOfMeasurementTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UnitOfMeasurementUnitOfMeasurementType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_UnitOfMeasurementUnitOfMeasurementType')
	DROP INDEX IX_FK_UnitOfMeasurementUnitOfMeasurementType ON [model].[UnitsOfMeasurement]
GO
CREATE INDEX [IX_FK_UnitOfMeasurementUnitOfMeasurementType]
ON [model].[UnitsOfMeasurement]
    ([UnitOfMeasurementTypeId]);
GO

-- Creating foreign key on [TimeQualifierId] in table 'AllergenOccurrences'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_AllergenOccurrenceTimeQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[AllergenOccurrences] DROP CONSTRAINT FK_AllergenOccurrenceTimeQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[TimeQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[AllergenOccurrences]
ADD CONSTRAINT [FK_AllergenOccurrenceTimeQualifier]
    FOREIGN KEY ([TimeQualifierId])
    REFERENCES [model].[TimeQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AllergenOccurrenceTimeQualifier'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_AllergenOccurrenceTimeQualifier')
	DROP INDEX IX_FK_AllergenOccurrenceTimeQualifier ON [model].[AllergenOccurrences]
GO
CREATE INDEX [IX_FK_AllergenOccurrenceTimeQualifier]
ON [model].[AllergenOccurrences]
    ([TimeQualifierId]);
GO

-- Creating foreign key on [PatientDiagnosisId] in table 'PatientProblemConcernLevels'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientProblemConcernLevelPatientDiagnosis'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientProblemConcernLevels] DROP CONSTRAINT FK_PatientProblemConcernLevelPatientDiagnosis

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnoses]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientProblemConcernLevels]
ADD CONSTRAINT [FK_PatientProblemConcernLevelPatientDiagnosis]
    FOREIGN KEY ([PatientDiagnosisId])
    REFERENCES [model].[PatientDiagnoses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientProblemConcernLevelPatientDiagnosis'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientProblemConcernLevelPatientDiagnosis')
	DROP INDEX IX_FK_PatientProblemConcernLevelPatientDiagnosis ON [model].[PatientProblemConcernLevels]
GO
CREATE INDEX [IX_FK_PatientProblemConcernLevelPatientDiagnosis]
ON [model].[PatientProblemConcernLevels]
    ([PatientDiagnosisId]);
GO

-- Creating foreign key on [PatientAllergenId] in table 'PatientProblemConcernLevels'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientProblemConcernLevelPatientAllergen'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientProblemConcernLevels] DROP CONSTRAINT FK_PatientProblemConcernLevelPatientAllergen

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergens]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientProblemConcernLevels]
ADD CONSTRAINT [FK_PatientProblemConcernLevelPatientAllergen]
    FOREIGN KEY ([PatientAllergenId])
    REFERENCES [model].[PatientAllergens]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientProblemConcernLevelPatientAllergen'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientProblemConcernLevelPatientAllergen')
	DROP INDEX IX_FK_PatientProblemConcernLevelPatientAllergen ON [model].[PatientProblemConcernLevels]
GO
CREATE INDEX [IX_FK_PatientProblemConcernLevelPatientAllergen]
ON [model].[PatientProblemConcernLevels]
    ([PatientAllergenId]);
GO

-- Creating foreign key on [PatientLaboratoryTestResultId] in table 'EncounterLaboratoryTestOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterLaboratoryTestOrderPatientLaboratoryTestResult'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT FK_EncounterLaboratoryTestOrderPatientLaboratoryTestResult

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientLaboratoryTestResults]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterLaboratoryTestOrders]
ADD CONSTRAINT [FK_EncounterLaboratoryTestOrderPatientLaboratoryTestResult]
    FOREIGN KEY ([PatientLaboratoryTestResultId])
    REFERENCES [model].[PatientLaboratoryTestResults]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterLaboratoryTestOrderPatientLaboratoryTestResult'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterLaboratoryTestOrderPatientLaboratoryTestResult')
	DROP INDEX IX_FK_EncounterLaboratoryTestOrderPatientLaboratoryTestResult ON [model].[EncounterLaboratoryTestOrders]
GO
CREATE INDEX [IX_FK_EncounterLaboratoryTestOrderPatientLaboratoryTestResult]
ON [model].[EncounterLaboratoryTestOrders]
    ([PatientLaboratoryTestResultId]);
GO

-- Creating foreign key on [PatientProcedurePerformedId] in table 'EncounterProcedureOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterProcedureOrderPatientProcedurePerformed'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT FK_EncounterProcedureOrderPatientProcedurePerformed

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientProceduresPerformed]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterProcedureOrders]
ADD CONSTRAINT [FK_EncounterProcedureOrderPatientProcedurePerformed]
    FOREIGN KEY ([PatientProcedurePerformedId])
    REFERENCES [model].[PatientProceduresPerformed]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterProcedureOrderPatientProcedurePerformed'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterProcedureOrderPatientProcedurePerformed')
	DROP INDEX IX_FK_EncounterProcedureOrderPatientProcedurePerformed ON [model].[EncounterProcedureOrders]
GO
CREATE INDEX [IX_FK_EncounterProcedureOrderPatientProcedurePerformed]
ON [model].[EncounterProcedureOrders]
    ([PatientProcedurePerformedId]);
GO

-- Creating foreign key on [TreatmentGoalId] in table 'TreatmentGoalAndInstructions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentGoalAndInstructionTreatmentGoal'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentGoalAndInstructions] DROP CONSTRAINT FK_TreatmentGoalAndInstructionTreatmentGoal

IF OBJECTPROPERTY(OBJECT_ID('[model].[TreatmentGoals]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentGoalAndInstructions]
ADD CONSTRAINT [FK_TreatmentGoalAndInstructionTreatmentGoal]
    FOREIGN KEY ([TreatmentGoalId])
    REFERENCES [model].[TreatmentGoals]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TreatmentGoalAndInstructionTreatmentGoal'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_TreatmentGoalAndInstructionTreatmentGoal')
	DROP INDEX IX_FK_TreatmentGoalAndInstructionTreatmentGoal ON [model].[TreatmentGoalAndInstructions]
GO
CREATE INDEX [IX_FK_TreatmentGoalAndInstructionTreatmentGoal]
ON [model].[TreatmentGoalAndInstructions]
    ([TreatmentGoalId]);
GO

-- Creating foreign key on [EncounterId] in table 'EncounterReviewOfSystems'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterReviewOfSystemEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterReviewOfSystems] DROP CONSTRAINT FK_EncounterReviewOfSystemEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterReviewOfSystems]
ADD CONSTRAINT [FK_EncounterReviewOfSystemEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterReviewOfSystemEncounter'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterReviewOfSystemEncounter')
	DROP INDEX IX_FK_EncounterReviewOfSystemEncounter ON [model].[EncounterReviewOfSystems]
GO
CREATE INDEX [IX_FK_EncounterReviewOfSystemEncounter]
ON [model].[EncounterReviewOfSystems]
    ([EncounterId]);
GO

-- Creating foreign key on [ReviewOfSystemId] in table 'EncounterReviewOfSystems'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterReviewOfSystemReviewOfSystem'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterReviewOfSystems] DROP CONSTRAINT FK_EncounterReviewOfSystemReviewOfSystem

IF OBJECTPROPERTY(OBJECT_ID('[model].[ReviewOfSystems]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterReviewOfSystems]
ADD CONSTRAINT [FK_EncounterReviewOfSystemReviewOfSystem]
    FOREIGN KEY ([ReviewOfSystemId])
    REFERENCES [model].[ReviewOfSystems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterReviewOfSystemReviewOfSystem'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterReviewOfSystemReviewOfSystem')
	DROP INDEX IX_FK_EncounterReviewOfSystemReviewOfSystem ON [model].[EncounterReviewOfSystems]
GO
CREATE INDEX [IX_FK_EncounterReviewOfSystemReviewOfSystem]
ON [model].[EncounterReviewOfSystems]
    ([ReviewOfSystemId]);
GO

-- Creating foreign key on [ReceiverExternalProviderId] in table 'EncounterCommunicationWithOtherProviderOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterCommunicationWithOtherProviderOrderReceiverExternalProvider'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders] DROP CONSTRAINT FK_EncounterCommunicationWithOtherProviderOrderReceiverExternalProvider

IF OBJECTPROPERTY(OBJECT_ID('[model].[ExternalContacts]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterCommunicationWithOtherProviderOrders]
ADD CONSTRAINT [FK_EncounterCommunicationWithOtherProviderOrderReceiverExternalProvider]
    FOREIGN KEY ([ReceiverExternalProviderId])
    REFERENCES [model].[ExternalContacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterCommunicationWithOtherProviderOrderReceiverExternalProvider'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterCommunicationWithOtherProviderOrderReceiverExternalProvider')
	DROP INDEX IX_FK_EncounterCommunicationWithOtherProviderOrderReceiverExternalProvider ON [model].[EncounterCommunicationWithOtherProviderOrders]
GO
CREATE INDEX [IX_FK_EncounterCommunicationWithOtherProviderOrderReceiverExternalProvider]
ON [model].[EncounterCommunicationWithOtherProviderOrders]
    ([ReceiverExternalProviderId]);
GO

-- Creating foreign key on [DiagnosticTestOrderId] in table 'TreatmentPlanOrderInstructions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentPlanOrderInstructionDiagnosticTestOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentPlanOrderInstructions] DROP CONSTRAINT FK_TreatmentPlanOrderInstructionDiagnosticTestOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[DiagnosticTestOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentPlanOrderInstructions]
ADD CONSTRAINT [FK_TreatmentPlanOrderInstructionDiagnosticTestOrder]
    FOREIGN KEY ([DiagnosticTestOrderId])
    REFERENCES [model].[DiagnosticTestOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TreatmentPlanOrderInstructionDiagnosticTestOrder'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_TreatmentPlanOrderInstructionDiagnosticTestOrder')
	DROP INDEX IX_FK_TreatmentPlanOrderInstructionDiagnosticTestOrder ON [model].[TreatmentPlanOrderInstructions]
GO
CREATE INDEX [IX_FK_TreatmentPlanOrderInstructionDiagnosticTestOrder]
ON [model].[TreatmentPlanOrderInstructions]
    ([DiagnosticTestOrderId]);
GO

-- Creating foreign key on [LaboratoryTestOrderId] in table 'TreatmentPlanOrderInstructions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentPlanOrderInstructionLaboratoryTestOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentPlanOrderInstructions] DROP CONSTRAINT FK_TreatmentPlanOrderInstructionLaboratoryTestOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[LaboratoryTestOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentPlanOrderInstructions]
ADD CONSTRAINT [FK_TreatmentPlanOrderInstructionLaboratoryTestOrder]
    FOREIGN KEY ([LaboratoryTestOrderId])
    REFERENCES [model].[LaboratoryTestOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TreatmentPlanOrderInstructionLaboratoryTestOrder'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_TreatmentPlanOrderInstructionLaboratoryTestOrder')
	DROP INDEX IX_FK_TreatmentPlanOrderInstructionLaboratoryTestOrder ON [model].[TreatmentPlanOrderInstructions]
GO
CREATE INDEX [IX_FK_TreatmentPlanOrderInstructionLaboratoryTestOrder]
ON [model].[TreatmentPlanOrderInstructions]
    ([LaboratoryTestOrderId]);
GO

-- Creating foreign key on [ProcedureOrderId] in table 'TreatmentPlanOrderInstructions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentPlanOrderInstructionProcedureOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentPlanOrderInstructions] DROP CONSTRAINT FK_TreatmentPlanOrderInstructionProcedureOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[ProcedureOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentPlanOrderInstructions]
ADD CONSTRAINT [FK_TreatmentPlanOrderInstructionProcedureOrder]
    FOREIGN KEY ([ProcedureOrderId])
    REFERENCES [model].[ProcedureOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TreatmentPlanOrderInstructionProcedureOrder'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_TreatmentPlanOrderInstructionProcedureOrder')
	DROP INDEX IX_FK_TreatmentPlanOrderInstructionProcedureOrder ON [model].[TreatmentPlanOrderInstructions]
GO
CREATE INDEX [IX_FK_TreatmentPlanOrderInstructionProcedureOrder]
ON [model].[TreatmentPlanOrderInstructions]
    ([ProcedureOrderId]);
GO

-- Creating foreign key on [SubsequentVisitOrderId] in table 'TreatmentPlanOrderInstructions'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_TreatmentPlanOrderInstructionSubsequentVisitOrder'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[TreatmentPlanOrderInstructions] DROP CONSTRAINT FK_TreatmentPlanOrderInstructionSubsequentVisitOrder

IF OBJECTPROPERTY(OBJECT_ID('[model].[SubsequentVisitOrders]'), 'IsUserTable') = 1
ALTER TABLE [model].[TreatmentPlanOrderInstructions]
ADD CONSTRAINT [FK_TreatmentPlanOrderInstructionSubsequentVisitOrder]
    FOREIGN KEY ([SubsequentVisitOrderId])
    REFERENCES [model].[SubsequentVisitOrders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TreatmentPlanOrderInstructionSubsequentVisitOrder'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_TreatmentPlanOrderInstructionSubsequentVisitOrder')
	DROP INDEX IX_FK_TreatmentPlanOrderInstructionSubsequentVisitOrder ON [model].[TreatmentPlanOrderInstructions]
GO
CREATE INDEX [IX_FK_TreatmentPlanOrderInstructionSubsequentVisitOrder]
ON [model].[TreatmentPlanOrderInstructions]
    ([SubsequentVisitOrderId]);
GO

-- Creating foreign key on [PatientAllergenAllergenReactionTypeId] in table 'AllergenOccurrences'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientAllergenAllergenReactionTypeAllergenOccurrence'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[AllergenOccurrences] DROP CONSTRAINT FK_PatientAllergenAllergenReactionTypeAllergenOccurrence

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientAllergenAllergenReactionTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[AllergenOccurrences]
ADD CONSTRAINT [FK_PatientAllergenAllergenReactionTypeAllergenOccurrence]
    FOREIGN KEY ([PatientAllergenAllergenReactionTypeId])
    REFERENCES [model].[PatientAllergenAllergenReactionTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientAllergenAllergenReactionTypeAllergenOccurrence'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientAllergenAllergenReactionTypeAllergenOccurrence')
	DROP INDEX IX_FK_PatientAllergenAllergenReactionTypeAllergenOccurrence ON [model].[AllergenOccurrences]
GO
CREATE INDEX [IX_FK_PatientAllergenAllergenReactionTypeAllergenOccurrence]
ON [model].[AllergenOccurrences]
    ([PatientAllergenAllergenReactionTypeId]);
GO

-- Creating foreign key on [AppointmentId] in table 'EncounterLaboratoryTestOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterLaboratoryTestOrderAppointment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterLaboratoryTestOrders] DROP CONSTRAINT FK_EncounterLaboratoryTestOrderAppointment

IF OBJECTPROPERTY(OBJECT_ID('[model].[Appointments]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterLaboratoryTestOrders]
ADD CONSTRAINT [FK_EncounterLaboratoryTestOrderAppointment]
    FOREIGN KEY ([AppointmentId])
    REFERENCES [model].[Appointments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterLaboratoryTestOrderAppointment'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterLaboratoryTestOrderAppointment')
	DROP INDEX IX_FK_EncounterLaboratoryTestOrderAppointment ON [model].[EncounterLaboratoryTestOrders]
GO
CREATE INDEX [IX_FK_EncounterLaboratoryTestOrderAppointment]
ON [model].[EncounterLaboratoryTestOrders]
    ([AppointmentId]);
GO

-- Creating foreign key on [AppointmentId] in table 'EncounterProcedureOrders'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_EncounterProcedureOrderAppointment'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[EncounterProcedureOrders] DROP CONSTRAINT FK_EncounterProcedureOrderAppointment

IF OBJECTPROPERTY(OBJECT_ID('[model].[Appointments]'), 'IsUserTable') = 1
ALTER TABLE [model].[EncounterProcedureOrders]
ADD CONSTRAINT [FK_EncounterProcedureOrderAppointment]
    FOREIGN KEY ([AppointmentId])
    REFERENCES [model].[Appointments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EncounterProcedureOrderAppointment'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_EncounterProcedureOrderAppointment')
	DROP INDEX IX_FK_EncounterProcedureOrderAppointment ON [model].[EncounterProcedureOrders]
GO
CREATE INDEX [IX_FK_EncounterProcedureOrderAppointment]
ON [model].[EncounterProcedureOrders]
    ([AppointmentId]);
GO

-- Creating foreign key on [ClinicalDataSourceTypeId] in table 'PatientDiagnoses'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosisClinicalDataSourceType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientDiagnoses] DROP CONSTRAINT FK_PatientDiagnosisClinicalDataSourceType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalDataSourceTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientDiagnoses]
ADD CONSTRAINT [FK_PatientDiagnosisClinicalDataSourceType]
    FOREIGN KEY ([ClinicalDataSourceTypeId])
    REFERENCES [model].[ClinicalDataSourceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosisClinicalDataSourceType'
IF EXISTS (select object_id from sys.indexes where name = 'IX_FK_PatientDiagnosisClinicalDataSourceType')
	DROP INDEX IX_FK_PatientDiagnosisClinicalDataSourceType ON [model].[PatientDiagnoses]
GO
CREATE INDEX [IX_FK_PatientDiagnosisClinicalDataSourceType]
ON [model].[PatientDiagnoses]
    ([ClinicalDataSourceTypeId]);
GO
