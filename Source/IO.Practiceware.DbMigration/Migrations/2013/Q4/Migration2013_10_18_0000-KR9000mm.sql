﻿-- To Add Devices names to the Devices Table for IOConnected Devices.

-- For Task 7867
IF EXISTS(Select TOP 1 * from dbo.Devices WHERE DeviceAlias='TopconKR9000')
BEGIN
	UPDATE dbo.devices SET Model='KR-9000Diopter',DeviceAlias='TopconKR9000Diopter' Where DeviceAlias='TopconKR9000' and Model='KR-9000'
END 
GO

IF NOT EXISTS(Select TOP 1 * from dbo.Devices WHERE DeviceAlias='TopconKR9000mm')
BEGIN
	INSERT INTO dbo.Devices (Make, Model, DeviceAlias, Type, Baud, Parity, DataBits, Stopbits) VALUES('Topcon','KR-9000mm','TopconKR9000mm','Autorefractor','2400','none','8', '2') 
END 
GO
