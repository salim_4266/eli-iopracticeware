﻿-- PracticeRepositoryEntities table vs C# enum were out of sync for a while
-- Id = 10 was used as (id) of BillingServiceTransaction entity, so we want to update name for this Id 
-- and move MaritalStatus to different Id (since it wasn't used from code yet, as it has been resolved via PracticeCode table)
IF (EXISTS (SELECT * FROM model.PracticeRepositoryEntities WHERE Id = 10 AND Name = 'MaritalStatus')
	AND NOT EXISTS (SELECT * FROM model.PracticeRepositoryEntities WHERE Id = 13))
BEGIN
	-- Replace MaritalStatus @ Id =
	UPDATE [model].[PracticeRepositoryEntities]
	   SET [Name] = 'BillingServiceTransaction'
	WHERE Id = 10

	-- Add MaritalStatus under Id=13, since Id=12 will be used by ClaimFilingIndicatorCode
	INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName)
	VALUES (13, 'MaritalStatus', 'Id')
	
	-- A later migration, Migration2013_10_16_1819-ExternalSystemMappings, maps Marital status with MVE's Marital Status, 
	-- so let's fix mappings if it has been run
	IF (EXISTS (SELECT * FROM Model.ExternalSystems WHERE Name = 'MVE')
		AND EXISTS (SELECT * FROM [dbo].[VersionInfo] WHERE [Version] = '201310161819'))
	BEGIN
		DECLARE @mveExternalSystemId int
		SET @mveExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'MVE')

		DECLARE @externalMaritalStatusEntityId int
		SET @externalMaritalStatusEntityId = (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'MaritalStatus' AND ExternalSystemId = @mveExternalSystemId)

		-- Cleanup any ExternalSystemEntityMappings which would point to old MaritalStatus Id
		DELETE FROM [model].[ExternalSystemEntityMappings]
		WHERE PracticeRepositoryEntityId = 10 -- Old MaritalStatus Id 
			AND ExternalSystemEntityId = @externalMaritalStatusEntityId

		-- Rebuild values for Marital Status
		-- Create mappings from our MaritalStatus values, to MVE MaritalStatus values
		IF EXISTS (SELECT AlternateCode FROM dbo.PracticeCodeTable WHERE ReferenceType = 'MARITAL' AND Code = 'Single' AND AlternateCode IS NOT NULL)
		BEGIN
			DECLARE @maritalStatusEntityId int
			SET @maritalStatusEntityId = (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'MaritalStatus')

			INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
			SELECT @maritalStatusEntityId,
					CASE Code
						WHEN 'Single' THEN 1
						WHEN 'Married' THEN 2
						WHEN 'Divorced' THEN 3
						WHEN 'Widowed' THEN 4
					END,
					@externalMaritalStatusEntityId,
					AlternateCode
			FROM dbo.PracticeCodeTable WHERE ReferenceType = 'MARITAL' AND AlternateCode IS NOT NULL
		END
	END
END
ELSE
BEGIN
	RAISERROR ('Unexpected values at Id 10 and 11 in PracticeRepositoryEntities to fix them. Please, update migration.', 16, 2)
END
GO