﻿SET IDENTITY_INSERT [model].[Permissions] ON

	INSERT INTO [model].[Permissions] (Id, Name) VALUES (37, 'CanReconcileClinical')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (38, 'CanExportClinical')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (39, 'CanEditProblemList')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (40, 'CanViewProblemList')
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (41, 'CanImportClinical')

SET IDENTITY_INSERT [model].[Permissions] OFF
GO

-- Spec doesn't mention any default settings for these permissions