﻿IF(OBJECT_ID('model.InsurancePolicies') IS NOT NULL)
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	;WITH CTE AS (
	SELECT 
	OBJECT_SCHEMA_NAME(OBJECT_ID('model.InsurancePolicies')) AS [schema]
	,o.name AS TableName
	,o.type	
	,c.Name
	,c.precision
	,c.scale
	FROM sys.Objects o
	INNER JOIN sys.columns c ON o.object_id = c.object_id
	INNER JOIN sys.systypes s ON s.xtype = c.system_type_id AND s.name = 'decimal'
	WHERE o.object_id = OBJECT_ID('model.InsurancePolicies')
		AND c.scale = 0
	)
	SELECT @sql = STUFF((
	SELECT ' '+'ALTER '+CASE WHEN CTE.type = 'U' THEN 'TABLE ' ELSE '' END+'['+CTE.[schema]+'].['+CTE.TableName+']'+
	' ALTER COLUMN ['+CTE.name+'] DECIMAL('+CONVERT(NVARCHAR(MAX),CTE.precision)+',2);'
	FROM CTE
	FOR XML PATH('')),1,1,'')

	DECLARE @PatientFinancialView NVARCHAR(MAX)

	IF (OBJECT_ID('dbo.PatientFinancial','V') IS NOT NULL AND (@sql IS NOT NULL OR @sql <> ''))
	BEGIN
		SELECT @PatientFinancialView =
		m.definition
		FROM sys.objects o
		JOIN sys.sql_modules m on m.object_id = o.object_id
		WHERE o.object_id = object_id( 'dbo.PatientFinancial')
		  and o.type = 'V'

		DROP VIEW dbo.PatientFinancial
	END

	EXECUTE sp_executesql @sql
	EXECUTE sp_executesql @PatientFinancialView
	
END