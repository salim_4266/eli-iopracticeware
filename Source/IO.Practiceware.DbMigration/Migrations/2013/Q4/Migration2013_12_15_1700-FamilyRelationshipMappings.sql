﻿DECLARE @prId INT
SET @prId = (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'FamilyRelationship')

DECLARE @externalId INT
SET @externalId = (SELECT ese.Id FROM model.ExternalSystemEntities ese
INNER JOIN model.ExternalSystems es ON ese.ExternalSystemId = es.Id
WHERE ese.Name = 'FamilyRelationship' AND es.Name = 'SnomedCt')

DELETE FROM model.ExternalSystemEntityMappings WHERE PracticeRepositoryEntityId = @prId

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 1, @externalId, '444191003' --Aunt

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 2, @externalId, '444303004' --Brother

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 3, @externalId, '444192005' --Child

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 4, @externalId, '55538000' --Cousin

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 5, @externalId, '444295003' --Father

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 6, @externalId, '34871008' --Grandfather

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 7, @externalId, '113157001' --Grandmother

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 8, @externalId, '444242001' --Grandparent

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 9, @externalId, '394862003' --Greataunt

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 10, @externalId, '394861005' --Greatuncle

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 11, @externalId, '444301002' --Mother

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 12, @externalId, '83559000' --Nephew

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 13, @externalId, '34581001' --Niece

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 15, @externalId, '444294004' --Parent

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 16, @externalId, '444302009' --Sibling

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 17, @externalId, '444304005' --Sister

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 18, @externalId, '444055008' --Uncle

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 19, @externalId, '444241008' --Son
 
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @prId, 20, @externalId, '444194006' --Daughter
