﻿DELETE FROM [model].[UnitsOfMeasurement]

DELETE FROM [model].[UnitOfMeasurementTypes]

SET IDENTITY_INSERT [model].[UnitOfMeasurementTypes] ON 

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (1, N'(unclassified)', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (2, N'acceleration', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (3, N'acidity', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (4, N'action', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (5, N'action area', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (6, N'amount of a proliferating organism', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (7, N'amount of an allergen callibrated through in-vivo testing based on the ID50EAL method of (intradermal dilution for 50mm sum of erythema diameters', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (8, N'amount of an infectious agent', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (9, N'amount of fibrinogen broken down into the measured d-dimers', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (10, N'amount of information', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (11, N'amount of substance', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (12, N'amount of substance (dissolved particles)', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (13, N'arbitrary', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (14, N'arbitrary biologic activity', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (15, N'arbitrary ELISA unit', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (16, N'area', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (17, N'biologic activity (infectivity) of an infectious agent preparation', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (18, N'biologic activity antistreptolysin O', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (19, N'biologic activity of amylase', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (20, N'biologic activity of anticardiolipin IgA', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (21, N'biologic activity of anticardiolipin IgG', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (22, N'biologic activity of anticardiolipin IgM', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (23, N'biologic activity of factor VIII inhibitor', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (24, N'biologic activity of factor Xa inhibitor (heparin)', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (25, N'biologic activity of phosphatase', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (26, N'biologic activity of tuberculin', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (27, N'brightness', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (28, N'catalytic activity', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (29, N'depth of water', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (30, N'dose equivalent', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (31, N'dry volume', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (32, N'dynamic viscosity', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (33, N'Ehrlich unit', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (34, N'electric capacitance', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (35, N'electric charge', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (36, N'electric conductance', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (37, N'electric current', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (38, N'electric permittivity', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (39, N'electric potential', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (40, N'electric potential level', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (41, N'electric resistance', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (42, N'energy', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (43, N'energy dose', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (44, N'fluid resistance', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (45, N'fluid volume', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (46, N'flux of magnetic induction', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (47, N'force', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (48, N'fraction', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (49, N'frequency', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (50, N'gauge of catheters', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (51, N'height of horses', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (52, N'homeopathic potency (Hahnemann)', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (53, N'homeopathic potency (Korsakov)', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (54, N'homeopathic potency (retired)', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (55, N'illuminance', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (56, N'inductance', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (57, N'ion dose', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (58, N'kinematic viscosity', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (59, N'length', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (60, N'level', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (61, N'linear mass density (of textile thread)', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (62, N'lineic number', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (63, N'lum. intensity density', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (64, N'luminous flux', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (65, N'magentic flux', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (66, N'magnetic field intensity', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (67, N'magnetic flux density', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (68, N'magnetic permeability', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (69, N'magnetic tension', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (70, N'mass', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (71, N'mass concentration', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (72, N'mass fraction', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (73, N'metabolic cost of physical activity', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (74, N'number', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (75, N'plane angle', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (76, N'power', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (77, N'power level', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (78, N'pressure', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (79, N'pressure level', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (80, N'procedure defined amount of a poliomyelitis d-antigen substance', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (81, N'procedure defined amount of a protein substance', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (82, N'procedure defined amount of an allergen using some reference standard', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (83, N'procedure defined amount of an antigen substance', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (84, N'procedure defined amount of the major allergen of ragweed.', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (85, N'radioactivity', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (86, N'refraction of a lens', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (87, N'refraction of a prism', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (88, N'sedimentation coefficient', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (89, N'signal transmission rate', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (90, N'slope', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (91, N'solid angle', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (92, N'temperature', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (93, N'time', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (94, N'velocity', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (95, N'view area in microscope', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (96, N'volume', 0, 0)

INSERT [model].[UnitOfMeasurementTypes] ([Id], [Name], [IsDeactivated], [OrdinalId]) VALUES (97, N'x-ray attenuation', 0, 0)

SET IDENTITY_INSERT [model].[UnitOfMeasurementTypes] OFF

SET IDENTITY_INSERT [model].[UnitsOfMeasurement] ON 


INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1, N'''', N'minute', 0, 0, 75)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2, N'''''', N'second', 0, 0, 75)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (3, N'%', N'percent', 0, 0, 48)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (4, N'%[slope]', N'percent of slope', 0, 0, 90)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (5, N'[acr_br]', N'acre', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (6, N'[acr_us]', N'acre', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (7, N'[Amb''a''1''U]', N'allergen unit for Ambrosia artemisiifolia', 0, 0, 84)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (8, N'[anti''Xa''U]', N'anti factor Xa unit', 0, 0, 24)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (9, N'[APL''U]', N'APL unit', 0, 0, 20)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (10, N'[arb''U]', N'arbitary unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (11, N'[AU]', N'allergen unit', 0, 0, 82)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (12, N'[BAU]', N'bioequivalent allergen unit', 0, 0, 7)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (13, N'[bbl_us]', N'barrel', 0, 0, 45)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (14, N'[bdsk''U]', N'Bodansky unit', 0, 0, 25)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (15, N'[beth''U]', N'Bethesda unit', 0, 0, 23)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (16, N'[bf_i]', N'board foot', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (17, N'[Btu]', N'British thermal unit', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (18, N'[Btu_39]', N'British thermal unit at 39 °F', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (19, N'[Btu_59]', N'British thermal unit at 59 °F', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (20, N'[Btu_60]', N'British thermal unit at 60 °F', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (21, N'[Btu_IT]', N'international table British thermal unit', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (22, N'[Btu_m]', N'mean British thermal unit', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (23, N'[Btu_th]', N'thermochemical British thermal unit', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (24, N'[bu_br]', N'bushel', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (25, N'[bu_us]', N'bushel', 0, 0, 31)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (26, N'[Cal]', N'nutrition label Calories', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (27, N'[car_Au]', N'carat of gold alloys', 0, 0, 72)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (28, N'[car_m]', N'metric carat', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (29, N'[CCID_50]', N'50% cell culture infectious dose', 0, 0, 17)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (30, N'[cft_i]', N'cubic foot', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (31, N'[CFU]', N'colony forming units', 0, 0, 6)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (32, N'[Ch]', N'Charrière', 0, 0, 50)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (33, N'[Ch]', N'french', 0, 0, 50)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (34, N'[ch_br]', N'Gunter''s chain', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (35, N'[ch_us]', N'Gunter''s chain', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (36, N'[ch_us]', N'Surveyor''s chain', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (37, N'[cicero]', N'cicero', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (38, N'[cicero]', N'Didot''s pica', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (39, N'[cin_i]', N'cubic inch', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (40, N'[cml_i]', N'circular mil', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (41, N'[cr_i]', N'cord', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (42, N'[crd_us]', N'cord', 0, 0, 45)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (43, N'[cup_m]', N'metric cup', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (44, N'[cup_us]', N'cup', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (45, N'[cyd_i]', N'cubic yard', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (46, N'[D''ag''U]', N'D-antigen unit', 0, 0, 80)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (47, N'[degF]', N'degree Fahrenheit', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (48, N'[degR]', N'degree Rankine', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (49, N'[den]', N'Denier', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (50, N'[didot]', N'didot', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (51, N'[didot]', N'Didot''s point', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (52, N'[diop]', N'diopter', 0, 0, 86)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (53, N'[dpt_us]', N'dry pint', 0, 0, 31)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (54, N'[dqt_us]', N'dry quart', 0, 0, 31)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (55, N'[dr_ap]', N'drachm', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (56, N'[dr_ap]', N'dram', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (57, N'[dr_av]', N'dram', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (58, N'[drp]', N'drop', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (59, N'[dye''U]', N'Dye unit', 0, 0, 19)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (60, N'[EID_50]', N'50% embryo infectious dose', 0, 0, 17)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (61, N'[ELU]', N'ELISA unit', 0, 0, 15)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (62, N'[EU]', N'Ehrlich unit', 0, 0, 33)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (63, N'[fdr_br]', N'fluid dram', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (64, N'[fdr_us]', N'fluid dram', 0, 0, 45)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (65, N'[FEU]', N'fibrinogen equivalent unit', 0, 0, 9)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (66, N'[FFU]', N'focus forming units', 0, 0, 8)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (67, N'[foz_br]', N'fluid ounce', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (68, N'[foz_m]', N'metric fluid ounce', 0, 0, 45)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (69, N'[foz_us]', N'fluid ounce', 0, 0, 45)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (70, N'[ft_br]', N'foot', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (71, N'[ft_i]', N'foot', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (72, N'[ft_us]', N'foot', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (73, N'[fth_br]', N'fathom', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (74, N'[fth_i]', N'fathom', 0, 0, 29)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (75, N'[fth_us]', N'fathom', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (76, N'[fur_us]', N'furlong', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (77, N'[gal_br]', N'gallon', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (78, N'[gal_us]', N'Queen Anne''s wine gallon', 0, 0, 45)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (79, N'[gal_wi]', N'historical winchester gallon', 0, 0, 31)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (80, N'[gil_br]', N'gill', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (81, N'[gil_us]', N'gill', 0, 0, 45)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (82, N'[GPL''U]', N'GPL unit', 0, 0, 21)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (83, N'[gr]', N'grain', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (84, N'[hd_i]', N'hand', 0, 0, 51)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (85, N'[hnsf''U]', N'Hounsfield unit', 0, 0, 97)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (86, N'[HP]', N'horsepower', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (87, N'[hp_C]', N'homeopathic potency of centesimal hahnemannian series', 0, 0, 52)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (88, N'[hp''_C]', N'homeopathic potency of centesimal series (retired)', 0, 0, 54)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (89, N'[hp_M]', N'homeopathic potency of millesimal hahnemannian series', 0, 0, 52)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (90, N'[hp''_M]', N'homeopathic potency of millesimal series (retired)', 0, 0, 54)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (91, N'[hp_Q]', N'homeopathic potency of quintamillesimal hahnemannian series', 0, 0, 52)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (92, N'[hp''_Q]', N'homeopathic potency of quintamillesimal series (retired)', 0, 0, 54)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (93, N'[hp_X]', N'homeopathic potency of decimal hahnemannian series', 0, 0, 52)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (94, N'[hp''_X]', N'homeopathic potency of decimal series (retired)', 0, 0, 54)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (95, N'[HPF]', N'high power field', 0, 0, 95)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (96, N'[in_br]', N'inch', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (97, N'[in_i]', N'inch', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (98, N'[in_i''H2O]', N'inch of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (99, N'[in_i''Hg]', N'inch of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (100, N'[in_us]', N'inch', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (101, N'[ka''U]', N'King-Armstrong unit', 0, 0, 25)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (102, N'[kn_br]', N'knot', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (103, N'[kn_i]', N'knot', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (104, N'[knk''U]', N'Kunkel unit', 0, 0, 14)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (105, N'[kp_C]', N'homeopathic potency of centesimal korsakovian series', 0, 0, 53)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (106, N'[kp_M]', N'homeopathic potency of millesimal korsakovian series', 0, 0, 53)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (107, N'[kp_Q]', N'homeopathic potency of quintamillesimal korsakovian series', 0, 0, 53)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (108, N'[kp_X]', N'homeopathic potency of decimal korsakovian series', 0, 0, 53)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (109, N'[lb_ap]', N'pound', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (110, N'[lb_av]', N'pound', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (111, N'[lb_tr]', N'pound', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (112, N'[lbf_av]', N'pound force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (113, N'[lcwt_av]', N'British hundredweight', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (114, N'[lcwt_av]', N'long hunderdweight', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (115, N'[Lf]', N'Limit of flocculation', 0, 0, 83)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (116, N'[ligne]', N'French line', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (117, N'[ligne]', N'ligne', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (118, N'[lk_br]', N'link for Gunter''s chain', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (119, N'[lk_us]', N'link for Gunter''s chain', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (120, N'[lne]', N'line', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (121, N'[LPF]', N'low power field', 0, 0, 95)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (122, N'[lton_av]', N'British ton', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (123, N'[lton_av]', N'long ton', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (124, N'[mclg''U]', N'Mac Lagan unit', 0, 0, 14)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (125, N'[mesh_i]', N'mesh', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (126, N'[MET]', N'metabolic equivalent', 0, 0, 73)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (127, N'[mi_br]', N'mile', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (128, N'[mi_i]', N'statute mile', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (129, N'[mi_us]', N'mile', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (130, N'[mil_i]', N'mil', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (131, N'[mil_us]', N'mil', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (132, N'[min_br]', N'minim', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (133, N'[min_us]', N'minim', 0, 0, 45)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (134, N'[MPL''U]', N'MPL unit', 0, 0, 22)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (135, N'[nmi_br]', N'nautical mile', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (136, N'[nmi_i]', N'nautical mile', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (137, N'[oz_ap]', N'ounce', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (138, N'[oz_av]', N'ounce', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (139, N'[oz_m]', N'metric ounce', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (140, N'[oz_tr]', N'ounce', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (141, N'[pc_br]', N'pace', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (142, N'[pca]', N'pica', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (143, N'[pca_pr]', N'Printer''s pica', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (144, N'[p''diop]', N'prism diopter', 0, 0, 87)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (145, N'[PFU]', N'plaque forming units', 0, 0, 8)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (146, N'[pH]', N'pH', 0, 0, 3)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (147, N'[pi]', N'the number pi', 0, 0, 74)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (148, N'[pied]', N'French foot', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (149, N'[pied]', N'pied', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (150, N'[pk_br]', N'peck', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (151, N'[pk_us]', N'peck', 0, 0, 31)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (152, N'[pnt]', N'point', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (153, N'[pnt_pr]', N'Printer''s point', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (154, N'[PNU]', N'protein nitrogen unit', 0, 0, 81)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (155, N'[pouce]', N'French inch', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (156, N'[pouce]', N'pouce', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (157, N'[ppb]', N'parts per billion', 0, 0, 48)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (158, N'[ppm]', N'parts per million', 0, 0, 48)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (159, N'[ppth]', N'parts per thousand', 0, 0, 48)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (160, N'[pptr]', N'parts per trillion', 0, 0, 48)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (161, N'[PRU]', N'peripheral vascular resistance unit', 0, 0, 44)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (162, N'[psi]', N'pound per sqare inch', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (163, N'[pt_br]', N'pint', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (164, N'[pt_us]', N'pint', 0, 0, 45)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (165, N'[pwt_tr]', N'pennyweight', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (166, N'[qt_br]', N'quart', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (167, N'[qt_us]', N'quart', 0, 0, 45)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (168, N'[rch_us]', N'Engineer''s chain', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (169, N'[rch_us]', N'Ramden''s chain', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (170, N'[rd_br]', N'rod', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (171, N'[rd_us]', N'rod', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (172, N'[rlk_us]', N'link for Ramden''s chain', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (173, N'[S]', N'Svedberg unit', 0, 0, 88)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (174, N'[sc_ap]', N'scruple', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (175, N'[sct]', N'section', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (176, N'[scwt_av]', N'short hundredweight', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (177, N'[scwt_av]', N'U.S. hundredweight', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (178, N'[sft_i]', N'square foot', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (179, N'[sin_i]', N'square inch', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (180, N'[smgy''U]', N'Somogyi unit', 0, 0, 19)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (181, N'[smi_us]', N'square mile', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (182, N'[smoot]', N'Smoot', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (183, N'[srd_us]', N'square rod', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (184, N'[ston_av]', N'short ton', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (185, N'[ston_av]', N'U.S. ton', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (186, N'[stone_av]', N'British stone', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (187, N'[stone_av]', N'stone', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (188, N'[syd_i]', N'square yard', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (189, N'[tbs_m]', N'metric tablespoon', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (190, N'[tbs_us]', N'tablespoon', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (191, N'[tb''U]', N'tuberculin unit', 0, 0, 26)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (192, N'[TCID_50]', N'50% tissue culture infectious dose', 0, 0, 17)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (193, N'[todd''U]', N'Todd unit', 0, 0, 18)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (194, N'[tsp_m]', N'metric teaspoon', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (195, N'[tsp_us]', N'teaspoon', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (196, N'[twp]', N'township', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (197, N'[USP''U]', N'United States Pharmacopeia unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (198, N'[wood''U]', N'Wood unit', 0, 0, 44)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (199, N'[yd_br]', N'yard', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (200, N'[yd_i]', N'yard', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (201, N'[yd_us]', N'yard', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (202, N'10*', N'the number ten for arbitrary powers', 0, 0, 74)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (203, N'10^', N'the number ten for arbitrary powers', 0, 0, 74)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (204, N'a', N'year', 0, 0, 93)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (205, N'a[c]', N'attovelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (206, N'a[e]', N'attoelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (207, N'a[eps_0]', N'attopermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (208, N'a[G]', N'atto Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (209, N'a[g]', N'attostandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (210, N'a[h]', N'atto Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (211, N'a[iU]', N'attointernational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (212, N'a[k]', N'atto Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (213, N'a[ly]', N'attolight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (214, N'a[m_e]', N'attoelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (215, N'a[m_p]', N'attoproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (216, N'a[mu_0]', N'attopermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (217, N'a_g', N'mean Gregorian year', 0, 0, 93)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (218, N'a_j', N'mean Julian year', 0, 0, 93)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (219, N'a_t', N'tropical year', 0, 0, 93)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (220, N'aA', N'atto Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (221, N'aar', N'attoare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (222, N'aB', N'attobel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (223, N'aB[10.nV]', N'attobel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (224, N'aB[kW]', N'attobel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (225, N'aB[mV]', N'attobel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (226, N'aB[SPL]', N'attobel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (227, N'aB[uV]', N'attobel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (228, N'aB[V]', N'attobel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (229, N'aB[W]', N'attobel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (230, N'abar', N'attobar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (231, N'aBd', N'attobaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (232, N'aBi', N'atto Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (233, N'abit', N'attobit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (234, N'aBq', N'atto Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (235, N'aBy', N'attobyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (236, N'acal', N'attocalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (237, N'acal_[15]', N'attocalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (238, N'acal_[20]', N'attocalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (239, N'acal_IT', N'attointernational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (240, N'acal_m', N'attomean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (241, N'acal_th', N'attothermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (242, N'aCel', N'attodegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (243, N'aCi', N'atto Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (244, N'adyn', N'attodyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (245, N'aeq', N'attoequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (246, N'aerg', N'attoerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (247, N'aeV', N'attoelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (248, N'aF', N'atto Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (249, N'aG', N'atto Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (250, N'ag%', N'attogram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (251, N'aGal', N'atto Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (252, N'aGb', N'atto Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (253, N'agf', N'attogram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (254, N'aGy', N'atto Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (255, N'aH', N'atto Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (256, N'aHz', N'atto Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (257, N'aJ', N'atto Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (258, N'akat', N'attokatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (259, N'aKy', N'atto Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (260, N'al', N'attoliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (261, N'alm', N'attolumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (262, N'aLmb', N'atto Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (263, N'alx', N'attolux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (264, N'am[H2O]', N'attometer of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (265, N'am[Hg]', N'attometer of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (266, N'amho', N'attomho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (267, N'amol', N'attomole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (268, N'aMx', N'atto Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (269, N'aN', N'atto Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (270, N'aNp', N'attoneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (271, N'Ao', N'Ångström', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (272, N'aOe', N'atto Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (273, N'aOhm', N'atto Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (274, N'aosm', N'attoosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (275, N'aP', N'atto Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (276, N'aPa', N'atto Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (277, N'apc', N'attoparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (278, N'aph', N'attophot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (279, N'aR', N'atto Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (280, N'aRAD', N'attoradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (281, N'aREM', N'attoradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (282, N'aS', N'atto Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (283, N'asb', N'attostilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (284, N'asr', N'attosteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (285, N'aSt', N'atto Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (286, N'ast', N'attostere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (287, N'aSv', N'atto Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (288, N'aT', N'atto Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (289, N'at', N'attotonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (290, N'atex', N'attotex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (291, N'atm', N'standard atmosphere', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (292, N'att', N'technical atmosphere', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (293, N'AU', N'astronomic unit', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (294, N'aU', N'atto Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (295, N'au', N'attounified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (296, N'aV', N'atto Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (297, N'aW', N'atto Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (298, N'aWb', N'atto Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (299, N'b', N'barn', 0, 0, 5)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (300, N'bit_s', N'bit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (301, N'c[c]', N'centivelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (302, N'c[e]', N'centielementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (303, N'c[eps_0]', N'centipermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (304, N'c[G]', N'centi Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (305, N'c[g]', N'centistandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (306, N'c[h]', N'centi Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (307, N'c[iU]', N'centiinternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (308, N'c[k]', N'centi Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (309, N'c[ly]', N'centilight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (310, N'c[m_e]', N'centielectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (311, N'c[m_p]', N'centiproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (312, N'c[mu_0]', N'centipermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (313, N'cA', N'centi Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (314, N'car', N'centiare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (315, N'cB', N'centibel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (316, N'cB[10.nV]', N'centibel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (317, N'cB[kW]', N'centibel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (318, N'cB[mV]', N'centibel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (319, N'cB[SPL]', N'centibel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (320, N'cB[uV]', N'centibel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (321, N'cB[V]', N'centibel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (322, N'cB[W]', N'centibel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (323, N'cbar', N'centibar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (324, N'cBd', N'centibaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (325, N'cBi', N'centi Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (326, N'cbit', N'centibit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (327, N'cBq', N'centi Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (328, N'cBy', N'centibyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (329, N'ccal', N'centicalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (330, N'ccal_[15]', N'centicalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (331, N'ccal_[20]', N'centicalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (332, N'ccal_IT', N'centiinternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (333, N'ccal_m', N'centimean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (334, N'ccal_th', N'centithermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (335, N'cCel', N'centidegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (336, N'cCi', N'centi Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (337, N'cdyn', N'centidyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (338, N'ceq', N'centiequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (339, N'cerg', N'centierg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (340, N'ceV', N'centielectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (341, N'cF', N'centi Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (342, N'cG', N'centi Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (343, N'cg%', N'centigram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (344, N'cGal', N'centi Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (345, N'cGb', N'centi Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (346, N'cgf', N'centigram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (347, N'cGy', N'centi Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (348, N'cH', N'centi Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (349, N'cHz', N'centi Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (350, N'circ', N'circle', 0, 0, 75)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (351, N'cJ', N'centi Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (352, N'ckat', N'centikatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (353, N'cKy', N'centi Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (354, N'cl', N'centiliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (355, N'clm', N'centilumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (356, N'cLmb', N'centi Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (357, N'clx', N'centilux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (358, N'cm[H2O]', N'centimeter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (359, N'cm[Hg]', N'centimeter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (360, N'cmho', N'centimho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (361, N'cmol', N'centimole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (362, N'cMx', N'centi Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (363, N'cN', N'centi Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (364, N'cNp', N'centineper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (365, N'cOe', N'centi Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (366, N'cOhm', N'centi Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (367, N'cosm', N'centiosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (368, N'cP', N'centi Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (369, N'cPa', N'centi Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (370, N'cpc', N'centiparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (371, N'cph', N'centiphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (372, N'cR', N'centi Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (373, N'cRAD', N'centiradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (374, N'cREM', N'centiradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (375, N'cS', N'centi Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (376, N'csb', N'centistilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (377, N'csr', N'centisteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (378, N'cSt', N'centi Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (379, N'cst', N'centistere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (380, N'cSv', N'centi Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (381, N'cT', N'centi Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (382, N'ct', N'centitonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (383, N'ctex', N'centitex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (384, N'cU', N'centi Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (385, N'cu', N'centiunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (386, N'cV', N'centi Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (387, N'cW', N'centi Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (388, N'cWb', N'centi Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (389, N'd', N'day', 0, 0, 93)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (390, N'd[c]', N'decivelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (391, N'd[e]', N'decielementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (392, N'd[eps_0]', N'decipermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (393, N'd[G]', N'deci Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (394, N'd[g]', N'decistandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (395, N'd[h]', N'deci Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (396, N'd[iU]', N'deciinternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (397, N'd[k]', N'deci Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (398, N'd[ly]', N'decilight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (399, N'd[m_e]', N'decielectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (400, N'd[m_p]', N'deciproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (401, N'd[mu_0]', N'decipermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (402, N'dA', N'deci Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (403, N'da[c]', N'dekavelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (404, N'da[e]', N'dekaelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (405, N'da[eps_0]', N'dekapermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (406, N'da[G]', N'deka Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (407, N'da[g]', N'dekastandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (408, N'da[h]', N'deka Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (409, N'da[iU]', N'dekainternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (410, N'da[k]', N'deka Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (411, N'da[ly]', N'dekalight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (412, N'da[m_e]', N'dekaelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (413, N'da[m_p]', N'dekaproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (414, N'da[mu_0]', N'dekapermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (415, N'daA', N'deka Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (416, N'daar', N'dekaare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (417, N'daB', N'dekabel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (418, N'daB[10.nV]', N'dekabel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (419, N'daB[kW]', N'dekabel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (420, N'daB[mV]', N'dekabel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (421, N'daB[SPL]', N'dekabel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (422, N'daB[uV]', N'dekabel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (423, N'daB[V]', N'dekabel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (424, N'daB[W]', N'dekabel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (425, N'dabar', N'dekabar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (426, N'daBd', N'dekabaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (427, N'daBi', N'deka Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (428, N'dabit', N'dekabit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (429, N'daBq', N'deka Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (430, N'daBy', N'dekabyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (431, N'dacal', N'dekacalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (432, N'dacal_[15]', N'dekacalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (433, N'dacal_[20]', N'dekacalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (434, N'dacal_IT', N'dekainternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (435, N'dacal_m', N'dekamean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (436, N'dacal_th', N'dekathermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (437, N'daCel', N'dekadegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (438, N'daCi', N'deka Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (439, N'dadyn', N'dekadyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (440, N'daeq', N'dekaequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (441, N'daerg', N'dekaerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (442, N'daeV', N'dekaelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (443, N'daF', N'deka Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (444, N'daG', N'deka Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (445, N'dag%', N'dekagram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (446, N'daGal', N'deka Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (447, N'daGb', N'deka Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (448, N'dagf', N'dekagram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (449, N'daGy', N'deka Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (450, N'daH', N'deka Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (451, N'daHz', N'deka Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (452, N'daJ', N'deka Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (453, N'dakat', N'dekakatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (454, N'daKy', N'deka Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (455, N'dal', N'dekaliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (456, N'dalm', N'dekalumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (457, N'daLmb', N'deka Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (458, N'dalx', N'dekalux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (459, N'dam[H2O]', N'dekameter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (460, N'dam[Hg]', N'dekameter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (461, N'damho', N'dekamho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (462, N'damol', N'dekamole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (463, N'daMx', N'deka Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (464, N'daN', N'deka Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (465, N'daNp', N'dekaneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (466, N'daOe', N'deka Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (467, N'daOhm', N'deka Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (468, N'daosm', N'dekaosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (469, N'daP', N'deka Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (470, N'daPa', N'deka Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (471, N'dapc', N'dekaparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (472, N'daph', N'dekaphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (473, N'dar', N'deciare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (474, N'daR', N'deka Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (475, N'daRAD', N'dekaradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (476, N'daREM', N'dekaradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (477, N'daS', N'deka Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (478, N'dasb', N'dekastilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (479, N'dasr', N'dekasteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (480, N'daSt', N'deka Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (481, N'dast', N'dekastere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (482, N'daSv', N'deka Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (483, N'daT', N'deka Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (484, N'dat', N'dekatonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (485, N'datex', N'dekatex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (486, N'daU', N'deka Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (487, N'dau', N'dekaunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (488, N'daV', N'deka Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (489, N'daW', N'deka Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (490, N'daWb', N'deka Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (491, N'dB', N'decibel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (492, N'dB[10.nV]', N'decibel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (493, N'dB[kW]', N'decibel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (494, N'dB[mV]', N'decibel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (495, N'dB[SPL]', N'decibel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (496, N'dB[uV]', N'decibel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (497, N'dB[V]', N'decibel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (498, N'dB[W]', N'decibel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (499, N'dbar', N'decibar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (500, N'dBd', N'decibaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (501, N'dBi', N'deci Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (502, N'dbit', N'decibit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (503, N'dBq', N'deci Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (504, N'dBy', N'decibyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (505, N'dcal', N'decicalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (506, N'dcal_[15]', N'decicalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (507, N'dcal_[20]', N'decicalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (508, N'dcal_IT', N'deciinternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (509, N'dcal_m', N'decimean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (510, N'dcal_th', N'decithermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (511, N'dCel', N'decidegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (512, N'dCi', N'deci Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (513, N'ddyn', N'decidyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (514, N'deg', N'degree', 0, 0, 75)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (515, N'deq', N'deciequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (516, N'derg', N'decierg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (517, N'deV', N'decielectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (518, N'dF', N'deci Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (519, N'dG', N'deci Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (520, N'dg%', N'decigram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (521, N'dGal', N'deci Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (522, N'dGb', N'deci Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (523, N'dgf', N'decigram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (524, N'dGy', N'deci Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (525, N'dH', N'deci Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (526, N'dHz', N'deci Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (527, N'dJ', N'deci Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (528, N'dkat', N'decikatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (529, N'dKy', N'deci Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (530, N'dl', N'deciliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (531, N'dlm', N'decilumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (532, N'dLmb', N'deci Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (533, N'dlx', N'decilux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (534, N'dm[H2O]', N'decimeter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (535, N'dm[Hg]', N'decimeter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (536, N'dmho', N'decimho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (537, N'dmol', N'decimole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (538, N'dMx', N'deci Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (539, N'dN', N'deci Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (540, N'dNp', N'decineper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (541, N'dOe', N'deci Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (542, N'dOhm', N'deci Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (543, N'dosm', N'deciosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (544, N'dP', N'deci Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (545, N'dPa', N'deci Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (546, N'dpc', N'deciparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (547, N'dph', N'deciphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (548, N'dR', N'deci Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (549, N'dRAD', N'deciradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (550, N'dREM', N'deciradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (551, N'dS', N'deci Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (552, N'dsb', N'decistilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (553, N'dsr', N'decisteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (554, N'dSt', N'deci Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (555, N'dst', N'decistere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (556, N'dSv', N'deci Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (557, N'dT', N'deci Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (558, N'dt', N'decitonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (559, N'dtex', N'decitex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (560, N'dU', N'deci Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (561, N'du', N'deciunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (562, N'dV', N'deci Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (563, N'dW', N'deci Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (564, N'dWb', N'deci Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (565, N'E[c]', N'exavelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (566, N'E[e]', N'exaelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (567, N'E[eps_0]', N'exapermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (568, N'E[G]', N'exa Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (569, N'E[g]', N'exastandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (570, N'E[h]', N'exa Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (571, N'E[iU]', N'exainternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (572, N'E[k]', N'exa Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (573, N'E[ly]', N'exalight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (574, N'E[m_e]', N'exaelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (575, N'E[m_p]', N'exaproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (576, N'E[mu_0]', N'exapermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (577, N'EA', N'exa Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (578, N'Ear', N'exaare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (579, N'EB', N'exabel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (580, N'EB[10.nV]', N'exabel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (581, N'EB[kW]', N'exabel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (582, N'EB[mV]', N'exabel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (583, N'EB[SPL]', N'exabel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (584, N'EB[uV]', N'exabel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (585, N'EB[V]', N'exabel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (586, N'EB[W]', N'exabel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (587, N'Ebar', N'exabar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (588, N'EBd', N'exabaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (589, N'EBi', N'exa Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (590, N'Ebit', N'exabit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (591, N'EBq', N'exa Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (592, N'EBy', N'exabyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (593, N'Ecal', N'exacalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (594, N'Ecal_[15]', N'exacalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (595, N'Ecal_[20]', N'exacalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (596, N'Ecal_IT', N'exainternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (597, N'Ecal_m', N'examean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (598, N'Ecal_th', N'exathermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (599, N'ECel', N'exadegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (600, N'ECi', N'exa Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (601, N'Edyn', N'exadyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (602, N'Eeq', N'exaequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (603, N'Eerg', N'exaerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (604, N'EeV', N'exaelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (605, N'EF', N'exa Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (606, N'EG', N'exa Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (607, N'Eg%', N'exagram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (608, N'EGal', N'exa Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (609, N'EGb', N'exa Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (610, N'Egf', N'exagram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (611, N'EGy', N'exa Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (612, N'EH', N'exa Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (613, N'EHz', N'exa Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (614, N'EJ', N'exa Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (615, N'Ekat', N'exakatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (616, N'EKy', N'exa Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (617, N'El', N'exaliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (618, N'Elm', N'exalumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (619, N'ELmb', N'exa Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (620, N'Elx', N'exalux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (621, N'Em[H2O]', N'exameter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (622, N'Em[Hg]', N'exameter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (623, N'Emho', N'examho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (624, N'Emol', N'examole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (625, N'EMx', N'exa Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (626, N'EN', N'exa Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (627, N'ENp', N'exaneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (628, N'EOe', N'exa Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (629, N'EOhm', N'exa Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (630, N'Eosm', N'exaosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (631, N'EP', N'exa Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (632, N'EPa', N'exa Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (633, N'Epc', N'exaparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (634, N'Eph', N'exaphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (635, N'ER', N'exa Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (636, N'ERAD', N'exaradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (637, N'EREM', N'exaradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (638, N'ES', N'exa Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (639, N'Esb', N'exastilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (640, N'Esr', N'exasteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (641, N'ESt', N'exa Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (642, N'Est', N'exastere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (643, N'ESv', N'exa Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (644, N'ET', N'exa Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (645, N'Et', N'exatonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (646, N'Etex', N'exatex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (647, N'EU', N'exa Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (648, N'Eu', N'exaunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (649, N'EV', N'exa Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (650, N'EW', N'exa Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (651, N'EWb', N'exa Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (652, N'f[c]', N'femtovelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (653, N'f[e]', N'femtoelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (654, N'f[eps_0]', N'femtopermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (655, N'f[G]', N'femto Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (656, N'f[g]', N'femtostandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (657, N'f[h]', N'femto Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (658, N'f[iU]', N'femtointernational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (659, N'f[k]', N'femto Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (660, N'f[ly]', N'femtolight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (661, N'f[m_e]', N'femtoelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (662, N'f[m_p]', N'femtoproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (663, N'f[mu_0]', N'femtopermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (664, N'fA', N'femto Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (665, N'far', N'femtoare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (666, N'fB', N'femtobel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (667, N'fB[10.nV]', N'femtobel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (668, N'fB[kW]', N'femtobel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (669, N'fB[mV]', N'femtobel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (670, N'fB[SPL]', N'femtobel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (671, N'fB[uV]', N'femtobel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (672, N'fB[V]', N'femtobel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (673, N'fB[W]', N'femtobel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (674, N'fbar', N'femtobar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (675, N'fBd', N'femtobaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (676, N'fBi', N'femto Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (677, N'fbit', N'femtobit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (678, N'fBq', N'femto Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (679, N'fBy', N'femtobyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (680, N'fcal', N'femtocalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (681, N'fcal_[15]', N'femtocalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (682, N'fcal_[20]', N'femtocalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (683, N'fcal_IT', N'femtointernational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (684, N'fcal_m', N'femtomean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (685, N'fcal_th', N'femtothermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (686, N'fCel', N'femtodegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (687, N'fCi', N'femto Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (688, N'fdyn', N'femtodyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (689, N'feq', N'femtoequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (690, N'ferg', N'femtoerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (691, N'feV', N'femtoelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (692, N'fF', N'femto Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (693, N'fG', N'femto Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (694, N'fg%', N'femtogram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (695, N'fGal', N'femto Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (696, N'fGb', N'femto Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (697, N'fgf', N'femtogram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (698, N'fGy', N'femto Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (699, N'fH', N'femto Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (700, N'fHz', N'femto Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (701, N'fJ', N'femto Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (702, N'fkat', N'femtokatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (703, N'fKy', N'femto Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (704, N'fl', N'femtoliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (705, N'flm', N'femtolumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (706, N'fLmb', N'femto Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (707, N'flx', N'femtolux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (708, N'fm[H2O]', N'femtometer of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (709, N'fm[Hg]', N'femtometer of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (710, N'fmho', N'femtomho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (711, N'fmol', N'femtomole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (712, N'fMx', N'femto Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (713, N'fN', N'femto Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (714, N'fNp', N'femtoneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (715, N'fOe', N'femto Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (716, N'fOhm', N'femto Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (717, N'fosm', N'femtoosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (718, N'fP', N'femto Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (719, N'fPa', N'femto Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (720, N'fpc', N'femtoparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (721, N'fph', N'femtophot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (722, N'fR', N'femto Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (723, N'fRAD', N'femtoradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (724, N'fREM', N'femtoradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (725, N'fS', N'femto Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (726, N'fsb', N'femtostilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (727, N'fsr', N'femtosteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (728, N'fSt', N'femto Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (729, N'fst', N'femtostere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (730, N'fSv', N'femto Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (731, N'fT', N'femto Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (732, N'ft', N'femtotonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (733, N'ftex', N'femtotex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (734, N'fU', N'femto Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (735, N'fu', N'femtounified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (736, N'fV', N'femto Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (737, N'fW', N'femto Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (738, N'fWb', N'femto Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (739, N'G[c]', N'gigavelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (740, N'G[e]', N'gigaelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (741, N'G[eps_0]', N'gigapermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (742, N'G[G]', N'giga Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (743, N'G[g]', N'gigastandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (744, N'G[h]', N'giga Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (745, N'G[iU]', N'gigainternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (746, N'G[k]', N'giga Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (747, N'G[ly]', N'gigalight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (748, N'G[m_e]', N'gigaelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (749, N'G[m_p]', N'gigaproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (750, N'G[mu_0]', N'gigapermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (751, N'GA', N'giga Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (752, N'Gar', N'gigaare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (753, N'GB', N'gigabel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (754, N'GB[10.nV]', N'gigabel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (755, N'GB[kW]', N'gigabel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (756, N'GB[mV]', N'gigabel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (757, N'GB[SPL]', N'gigabel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (758, N'GB[uV]', N'gigabel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (759, N'GB[V]', N'gigabel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (760, N'GB[W]', N'gigabel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (761, N'Gbar', N'gigabar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (762, N'GBd', N'gigabaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (763, N'GBi', N'giga Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (764, N'Gbit', N'gigabit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (765, N'GBq', N'giga Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (766, N'GBy', N'gigabyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (767, N'Gcal', N'gigacalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (768, N'Gcal_[15]', N'gigacalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (769, N'Gcal_[20]', N'gigacalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (770, N'Gcal_IT', N'gigainternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (771, N'Gcal_m', N'gigamean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (772, N'Gcal_th', N'gigathermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (773, N'GCel', N'gigadegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (774, N'GCi', N'giga Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (775, N'Gdyn', N'gigadyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (776, N'Geq', N'gigaequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (777, N'Gerg', N'gigaerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (778, N'GeV', N'gigaelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (779, N'GF', N'giga Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (780, N'GG', N'giga Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (781, N'Gg%', N'gigagram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (782, N'GGal', N'giga Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (783, N'GGb', N'giga Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (784, N'Ggf', N'gigagram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (785, N'GGy', N'giga Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (786, N'GH', N'giga Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (787, N'GHz', N'giga Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (788, N'Gi[c]', N'gibivelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (789, N'Gi[e]', N'gibielementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (790, N'Gi[eps_0]', N'gibipermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (791, N'Gi[G]', N'gibi Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (792, N'Gi[g]', N'gibistandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (793, N'Gi[h]', N'gibi Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (794, N'Gi[iU]', N'gibiinternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (795, N'Gi[k]', N'gibi Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (796, N'Gi[ly]', N'gibilight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (797, N'Gi[m_e]', N'gibielectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (798, N'Gi[m_p]', N'gibiproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (799, N'Gi[mu_0]', N'gibipermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (800, N'GiA', N'gibi Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (801, N'Giar', N'gibiare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (802, N'GiB', N'gibibel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (803, N'GiB[10.nV]', N'gibibel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (804, N'GiB[kW]', N'gibibel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (805, N'GiB[mV]', N'gibibel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (806, N'GiB[SPL]', N'gibibel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (807, N'GiB[uV]', N'gibibel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (808, N'GiB[V]', N'gibibel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (809, N'GiB[W]', N'gibibel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (810, N'Gibar', N'gibibar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (811, N'GiBd', N'gibibaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (812, N'GiBi', N'gibi Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (813, N'Gibit', N'gibibit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (814, N'GiBq', N'gibi Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (815, N'GiBy', N'gibibyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (816, N'Gical', N'gibicalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (817, N'Gical_[15]', N'gibicalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (818, N'Gical_[20]', N'gibicalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (819, N'Gical_IT', N'gibiinternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (820, N'Gical_m', N'gibimean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (821, N'Gical_th', N'gibithermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (822, N'GiCel', N'gibidegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (823, N'GiCi', N'gibi Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (824, N'Gidyn', N'gibidyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (825, N'Gieq', N'gibiequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (826, N'Gierg', N'gibierg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (827, N'GieV', N'gibielectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (828, N'GiF', N'gibi Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (829, N'GiG', N'gibi Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (830, N'Gig%', N'gibigram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (831, N'GiGal', N'gibi Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (832, N'GiGb', N'gibi Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (833, N'Gigf', N'gibigram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (834, N'GiGy', N'gibi Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (835, N'GiH', N'gibi Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (836, N'GiHz', N'gibi Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (837, N'GiJ', N'gibi Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (838, N'Gikat', N'gibikatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (839, N'GiKy', N'gibi Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (840, N'Gil', N'gibiliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (841, N'Gilm', N'gibilumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (842, N'GiLmb', N'gibi Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (843, N'Gilx', N'gibilux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (844, N'Gim[H2O]', N'gibimeter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (845, N'Gim[Hg]', N'gibimeter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (846, N'Gimho', N'gibimho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (847, N'Gimol', N'gibimole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (848, N'GiMx', N'gibi Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (849, N'GiN', N'gibi Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (850, N'GiNp', N'gibineper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (851, N'GiOe', N'gibi Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (852, N'GiOhm', N'gibi Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (853, N'Giosm', N'gibiosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (854, N'GiP', N'gibi Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (855, N'GiPa', N'gibi Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (856, N'Gipc', N'gibiparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (857, N'Giph', N'gibiphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (858, N'GiR', N'gibi Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (859, N'GiRAD', N'gibiradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (860, N'GiREM', N'gibiradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (861, N'GiS', N'gibi Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (862, N'Gisb', N'gibistilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (863, N'Gisr', N'gibisteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (864, N'GiSt', N'gibi Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (865, N'Gist', N'gibistere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (866, N'GiSv', N'gibi Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (867, N'GiT', N'gibi Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (868, N'Git', N'gibitonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (869, N'Gitex', N'gibitex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (870, N'GiU', N'gibi Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (871, N'Giu', N'gibiunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (872, N'GiV', N'gibi Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (873, N'GiW', N'gibi Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (874, N'GiWb', N'gibi Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (875, N'GJ', N'giga Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (876, N'Gkat', N'gigakatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (877, N'GKy', N'giga Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (878, N'Gl', N'gigaliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (879, N'Glm', N'gigalumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (880, N'GLmb', N'giga Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (881, N'Glx', N'gigalux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (882, N'Gm[H2O]', N'gigameter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (883, N'Gm[Hg]', N'gigameter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (884, N'Gmho', N'gigamho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (885, N'Gmol', N'gigamole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (886, N'GMx', N'giga Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (887, N'GN', N'giga Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (888, N'GNp', N'giganeper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (889, N'e', N'giga Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (890, N'hm', N'giga Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (891, N'gon', N'gon', 0, 0, 75)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (892, N'gon', N'grade', 0, 0, 75)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (893, N'Gosm', N'gigaosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (894, N'GP', N'giga Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (895, N'GPa', N'giga Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (896, N'Gpc', N'gigaparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (897, N'Gph', N'gigaphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (898, N'GR', N'giga Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (899, N'GRAD', N'gigaradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (900, N'GREM', N'gigaradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (901, N'GS', N'giga Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (902, N'Gsb', N'gigastilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (903, N'Gsr', N'gigasteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (904, N'GSt', N'giga Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (905, N'Gst', N'gigastere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (906, N'GSv', N'giga Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (907, N'GT', N'giga Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (908, N'Gt', N'gigatonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (909, N'Gtex', N'gigatex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (910, N'GU', N'giga Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (911, N'Gu', N'gigaunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (912, N'GV', N'giga Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (913, N'GW', N'giga Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (914, N'GWb', N'giga Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (915, N'h', N'hour', 0, 0, 93)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (916, N'h[c]', N'hectovelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (917, N'h[e]', N'hectoelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (918, N'h[eps_0]', N'hectopermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (919, N'h[G]', N'hecto Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (920, N'h[g]', N'hectostandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (921, N'h[h]', N'hecto Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (922, N'h[iU]', N'hectointernational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (923, N'h[k]', N'hecto Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (924, N'h[ly]', N'hectolight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (925, N'h[m_e]', N'hectoelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (926, N'h[m_p]', N'hectoproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (927, N'h[mu_0]', N'hectopermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (928, N'hA', N'hecto Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (929, N'har', N'hectoare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (930, N'hB', N'hectobel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (931, N'hB[10.nV]', N'hectobel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (932, N'hB[kW]', N'hectobel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (933, N'hB[mV]', N'hectobel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (934, N'hB[SPL]', N'hectobel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (935, N'hB[uV]', N'hectobel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (936, N'hB[V]', N'hectobel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (937, N'hB[W]', N'hectobel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (938, N'hbar', N'hectobar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (939, N'hBd', N'hectobaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (940, N'hBi', N'hecto Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (941, N'hbit', N'hectobit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (942, N'hBq', N'hecto Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (943, N'hBy', N'hectobyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (944, N'hcal', N'hectocalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (945, N'hcal_[15]', N'hectocalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (946, N'hcal_[20]', N'hectocalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (947, N'hcal_IT', N'hectointernational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (948, N'hcal_m', N'hectomean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (949, N'hcal_th', N'hectothermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (950, N'hCel', N'hectodegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (951, N'hCi', N'hecto Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (952, N'hdyn', N'hectodyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (953, N'heq', N'hectoequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (954, N'herg', N'hectoerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (955, N'heV', N'hectoelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (956, N'hF', N'hecto Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (957, N'hG', N'hecto Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (958, N'hg%', N'hectogram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (959, N'hGal', N'hecto Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (960, N'hGb', N'hecto Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (961, N'hgf', N'hectogram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (962, N'hGy', N'hecto Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (963, N'hH', N'hecto Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (964, N'hHz', N'hecto Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (965, N'hJ', N'hecto Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (966, N'hkat', N'hectokatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (967, N'hKy', N'hecto Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (968, N'hl', N'hectoliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (969, N'hlm', N'hectolumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (970, N'hLmb', N'hecto Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (971, N'hlx', N'hectolux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (972, N'hm[H2O]', N'hectometer of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (973, N'hm[Hg]', N'hectometer of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (974, N'hmho', N'hectomho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (975, N'hmol', N'hectomole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (976, N'hMx', N'hecto Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (977, N'hN', N'hecto Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (978, N'hNp', N'hectoneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (979, N'hOe', N'hecto Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (980, N'hOhm', N'hecto Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (981, N'hosm', N'hectoosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (982, N'hP', N'hecto Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (983, N'hPa', N'hecto Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (984, N'hpc', N'hectoparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (985, N'hph', N'hectophot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (986, N'hR', N'hecto Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (987, N'hRAD', N'hectoradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (988, N'hREM', N'hectoradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (989, N'hS', N'hecto Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (990, N'hsb', N'hectostilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (991, N'hsr', N'hectosteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (992, N'hSt', N'hecto Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (993, N'hst', N'hectostere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (994, N'hSv', N'hecto Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (995, N'hT', N'hecto Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (996, N'ht', N'hectotonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (997, N'htex', N'hectotex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (998, N'hU', N'hecto Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (999, N'hu', N'hectounified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1000, N'hV', N'hecto Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1001, N'hW', N'hecto Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1002, N'hWb', N'hecto Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1003, N'k[c]', N'kilovelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1004, N'k[e]', N'kiloelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1005, N'k[eps_0]', N'kilopermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1006, N'k[G]', N'kilo Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1007, N'k[g]', N'kilostandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1008, N'k[h]', N'kilo Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1009, N'k[iU]', N'kilointernational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1010, N'k[k]', N'kilo Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1011, N'k[ly]', N'kilolight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1012, N'k[m_e]', N'kiloelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1013, N'k[m_p]', N'kiloproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1014, N'k[mu_0]', N'kilopermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1015, N'kA', N'kilo Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1016, N'kar', N'kiloare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1017, N'kB', N'kilobel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1018, N'kB[10.nV]', N'kilobel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1019, N'kB[kW]', N'kilobel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1020, N'kB[mV]', N'kilobel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1021, N'kB[SPL]', N'kilobel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1022, N'kB[uV]', N'kilobel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1023, N'kB[V]', N'kilobel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1024, N'kB[W]', N'kilobel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1025, N'kbar', N'kilobar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1026, N'kBd', N'kilobaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1027, N'kBi', N'kilo Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1028, N'kbit', N'kilobit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1029, N'kBq', N'kilo Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1030, N'kBy', N'kilobyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1031, N'kcal', N'kilocalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1032, N'kcal_[15]', N'kilocalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1033, N'kcal_[20]', N'kilocalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1034, N'kcal_IT', N'kilointernational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1035, N'kcal_m', N'kilomean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1036, N'kcal_th', N'kilothermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1037, N'kCel', N'kilodegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1038, N'kCi', N'kilo Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1039, N'kdyn', N'kilodyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1040, N'keq', N'kiloequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1041, N'kerg', N'kiloerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1042, N'keV', N'kiloelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1043, N'kF', N'kilo Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1044, N'kG', N'kilo Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1045, N'kg%', N'kilogram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1046, N'kGal', N'kilo Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1047, N'kGb', N'kilo Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1048, N'kgf', N'kilogram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1049, N'kGy', N'kilo Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1050, N'kH', N'kilo Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1051, N'kHz', N'kilo Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1052, N'Ki[c]', N'kibivelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1053, N'Ki[e]', N'kibielementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1054, N'Ki[eps_0]', N'kibipermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1055, N'Ki[G]', N'kibi Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1056, N'Ki[g]', N'kibistandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1057, N'Ki[h]', N'kibi Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1058, N'Ki[iU]', N'kibiinternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1059, N'Ki[k]', N'kibi Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1060, N'Ki[ly]', N'kibilight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1061, N'Ki[m_e]', N'kibielectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1062, N'Ki[m_p]', N'kibiproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1063, N'Ki[mu_0]', N'kibipermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1064, N'KiA', N'kibi Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1065, N'Kiar', N'kibiare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1066, N'KiB', N'kibibel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1067, N'KiB[10.nV]', N'kibibel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1068, N'KiB[kW]', N'kibibel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1069, N'KiB[mV]', N'kibibel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1070, N'KiB[SPL]', N'kibibel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1071, N'KiB[uV]', N'kibibel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1072, N'KiB[V]', N'kibibel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1073, N'KiB[W]', N'kibibel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1074, N'Kibar', N'kibibar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1075, N'KiBd', N'kibibaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1076, N'KiBi', N'kibi Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1077, N'Kibit', N'kibibit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1078, N'KiBq', N'kibi Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1079, N'KiBy', N'kibibyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1080, N'Kical', N'kibicalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1081, N'Kical_[15]', N'kibicalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1082, N'Kical_[20]', N'kibicalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1083, N'Kical_IT', N'kibiinternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1084, N'Kical_m', N'kibimean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1085, N'Kical_th', N'kibithermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1086, N'KiCel', N'kibidegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1087, N'KiCi', N'kibi Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1088, N'Kidyn', N'kibidyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1089, N'Kieq', N'kibiequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1090, N'Kierg', N'kibierg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1091, N'KieV', N'kibielectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1092, N'KiF', N'kibi Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1093, N'KiG', N'kibi Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1094, N'Kig%', N'kibigram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1095, N'KiGal', N'kibi Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1096, N'KiGb', N'kibi Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1097, N'Kigf', N'kibigram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1098, N'KiGy', N'kibi Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1099, N'KiH', N'kibi Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1100, N'KiHz', N'kibi Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1101, N'KiJ', N'kibi Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1102, N'Kikat', N'kibikatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1103, N'KiKy', N'kibi Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1104, N'Kil', N'kibiliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1105, N'Kilm', N'kibilumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1106, N'KiLmb', N'kibi Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1107, N'Kilx', N'kibilux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1108, N'Kim[H2O]', N'kibimeter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1109, N'Kim[Hg]', N'kibimeter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1110, N'Kimho', N'kibimho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1111, N'Kimol', N'kibimole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1112, N'KiMx', N'kibi Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1113, N'KiN', N'kibi Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1114, N'KiNp', N'kibineper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1115, N'KiOe', N'kibi Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1116, N'KiOhm', N'kibi Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1117, N'Kiosm', N'kibiosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1118, N'KiP', N'kibi Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1119, N'KiPa', N'kibi Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1120, N'Kipc', N'kibiparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1121, N'Kiph', N'kibiphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1122, N'KiR', N'kibi Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1123, N'KiRAD', N'kibiradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1124, N'KiREM', N'kibiradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1125, N'KiS', N'kibi Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1126, N'Kisb', N'kibistilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1127, N'Kisr', N'kibisteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1128, N'KiSt', N'kibi Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1129, N'Kist', N'kibistere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1130, N'KiSv', N'kibi Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1131, N'KiT', N'kibi Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1132, N'Kit', N'kibitonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1133, N'Kitex', N'kibitex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1134, N'KiU', N'kibi Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1135, N'Kiu', N'kibiunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1136, N'KiV', N'kibi Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1137, N'KiW', N'kibi Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1138, N'KiWb', N'kibi Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1139, N'kJ', N'kilo Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1140, N'kkat', N'kilokatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1141, N'kKy', N'kilo Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1142, N'kl', N'kiloliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1143, N'klm', N'kilolumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1144, N'kLmb', N'kilo Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1145, N'klx', N'kilolux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1146, N'km[H2O]', N'kilometer of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1147, N'km[Hg]', N'kilometer of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1148, N'kmho', N'kilomho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1149, N'kmol', N'kilomole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1150, N'kMx', N'kilo Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1151, N'kN', N'kilo Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1152, N'kNp', N'kiloneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1153, N'kOe', N'kilo Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1154, N'kOhm', N'kilo Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1155, N'kosm', N'kiloosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1156, N'kP', N'kilo Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1157, N'kPa', N'kilo Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1158, N'kpc', N'kiloparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1159, N'kph', N'kilophot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1160, N'kR', N'kilo Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1161, N'kRAD', N'kiloradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1162, N'kREM', N'kiloradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1163, N'kS', N'kilo Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1164, N'ksb', N'kilostilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1165, N'ksr', N'kilosteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1166, N'kSt', N'kilo Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1167, N'kst', N'kilostere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1168, N'kSv', N'kilo Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1169, N'kT', N'kilo Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1170, N'kt', N'kilotonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1171, N'ktex', N'kilotex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1172, N'kU', N'kilo Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1173, N'ku', N'kilounified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1174, N'kV', N'kilo Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1175, N'kW', N'kilo Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1176, N'kWb', N'kilo Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1177, N'M[c]', N'megavelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1178, N'm[c]', N'millivelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1179, N'M[e]', N'megaelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1180, N'm[e]', N'millielementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1181, N'M[eps_0]', N'megapermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1182, N'm[eps_0]', N'millipermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1183, N'M[G]', N'mega Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1184, N'M[g]', N'megastandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1185, N'm[G]', N'milli Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1186, N'm[g]', N'millistandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1187, N'M[h]', N'mega Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1188, N'm[h]', N'milli Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1189, N'M[iU]', N'megainternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1190, N'm[iU]', N'milliinternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1191, N'M[k]', N'mega Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1192, N'm[k]', N'milli Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1193, N'M[ly]', N'megalight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1194, N'm[ly]', N'millilight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1195, N'M[m_e]', N'megaelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1196, N'm[m_e]', N'millielectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1197, N'M[m_p]', N'megaproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1198, N'm[m_p]', N'milliproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1199, N'M[mu_0]', N'megapermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1200, N'm[mu_0]', N'millipermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1201, N'MA', N'mega Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1202, N'mA', N'milli Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1203, N'Mar', N'megaare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1204, N'mar', N'milliare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1205, N'MB', N'megabel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1206, N'mB', N'millibel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1207, N'MB[10.nV]', N'megabel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1208, N'mB[10.nV]', N'millibel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1209, N'MB[kW]', N'megabel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1210, N'mB[kW]', N'millibel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1211, N'MB[mV]', N'megabel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1212, N'mB[mV]', N'millibel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1213, N'MB[SPL]', N'megabel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1214, N'mB[SPL]', N'millibel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1215, N'MB[uV]', N'megabel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1216, N'mB[uV]', N'millibel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1217, N'MB[V]', N'megabel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1218, N'mB[V]', N'millibel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1219, N'MB[W]', N'megabel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1220, N'mB[W]', N'millibel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1221, N'Mbar', N'megabar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1222, N'mbar', N'millibar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1223, N'MBd', N'megabaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1224, N'mBd', N'millibaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1225, N'MBi', N'mega Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1226, N'mBi', N'milli Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1227, N'Mbit', N'megabit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1228, N'mbit', N'millibit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1229, N'MBq', N'mega Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1230, N'mBq', N'milli Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1231, N'MBy', N'megabyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1232, N'mBy', N'millibyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1233, N'Mcal', N'megacalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1234, N'mcal', N'millicalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1235, N'Mcal_[15]', N'megacalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1236, N'mcal_[15]', N'millicalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1237, N'Mcal_[20]', N'megacalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1238, N'mcal_[20]', N'millicalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1239, N'Mcal_IT', N'megainternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1240, N'mcal_IT', N'milliinternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1241, N'Mcal_m', N'megamean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1242, N'mcal_m', N'millimean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1243, N'Mcal_th', N'megathermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1244, N'mcal_th', N'millithermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1245, N'MCel', N'megadegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1246, N'mCel', N'millidegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1247, N'MCi', N'mega Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1248, N'mCi', N'milli Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1249, N'Mdyn', N'megadyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1250, N'mdyn', N'millidyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1251, N'Meq', N'megaequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1252, N'meq', N'milliequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1253, N'Merg', N'megaerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1254, N'merg', N'millierg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1255, N'MeV', N'megaelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1256, N'meV', N'millielectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1257, N'MF', N'mega Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1258, N'mF', N'milli Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1259, N'MG', N'mega Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1260, N'mG', N'milli Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1261, N'Mg%', N'megagram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1262, N'mg%', N'milligram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1263, N'MGal', N'mega Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1264, N'mGal', N'milli Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1265, N'MGb', N'mega Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1266, N'mGb', N'milli Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1267, N'Mgf', N'megagram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1268, N'mgf', N'milligram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1269, N'MGy', N'mega Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1270, N'mGy', N'milli Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1271, N'MH', N'mega Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1272, N'mH', N'milli Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1273, N'MHz', N'mega Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1274, N'mHz', N'milli Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1275, N'Mi[c]', N'mebivelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1276, N'Mi[e]', N'mebielementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1277, N'Mi[eps_0]', N'mebipermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1278, N'Mi[G]', N'mebi Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1279, N'Mi[g]', N'mebistandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1280, N'Mi[h]', N'mebi Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1281, N'Mi[iU]', N'mebiinternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1282, N'Mi[k]', N'mebi Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1283, N'Mi[ly]', N'mebilight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1284, N'Mi[m_e]', N'mebielectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1285, N'Mi[m_p]', N'mebiproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1286, N'Mi[mu_0]', N'mebipermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1287, N'MiA', N'mebi Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1288, N'Miar', N'mebiare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1289, N'MiB', N'mebibel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1290, N'MiB[10.nV]', N'mebibel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1291, N'MiB[kW]', N'mebibel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1292, N'MiB[mV]', N'mebibel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1293, N'MiB[SPL]', N'mebibel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1294, N'MiB[uV]', N'mebibel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1295, N'MiB[V]', N'mebibel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1296, N'MiB[W]', N'mebibel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1297, N'Mibar', N'mebibar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1298, N'MiBd', N'mebibaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1299, N'MiBi', N'mebi Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1300, N'Mibit', N'mebibit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1301, N'MiBq', N'mebi Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1302, N'MiBy', N'mebibyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1303, N'Mical', N'mebicalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1304, N'Mical_[15]', N'mebicalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1305, N'Mical_[20]', N'mebicalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1306, N'Mical_IT', N'mebiinternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1307, N'Mical_m', N'mebimean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1308, N'Mical_th', N'mebithermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1309, N'MiCel', N'mebidegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1310, N'MiCi', N'mebi Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1311, N'Midyn', N'mebidyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1312, N'Mieq', N'mebiequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1313, N'Mierg', N'mebierg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1314, N'MieV', N'mebielectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1315, N'MiF', N'mebi Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1316, N'MiG', N'mebi Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1317, N'Mig%', N'mebigram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1318, N'MiGal', N'mebi Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1319, N'MiGb', N'mebi Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1320, N'Migf', N'mebigram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1321, N'MiGy', N'mebi Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1322, N'MiH', N'mebi Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1323, N'MiHz', N'mebi Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1324, N'MiJ', N'mebi Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1325, N'Mikat', N'mebikatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1326, N'MiKy', N'mebi Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1327, N'Mil', N'mebiliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1328, N'Milm', N'mebilumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1329, N'MiLmb', N'mebi Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1330, N'Milx', N'mebilux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1331, N'Mim[H2O]', N'mebimeter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1332, N'Mim[Hg]', N'mebimeter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1333, N'Mimho', N'mebimho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1334, N'Mimol', N'mebimole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1335, N'MiMx', N'mebi Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1336, N'MiN', N'mebi Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1337, N'min', N'minute', 0, 0, 93)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1338, N'MiNp', N'mebineper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1339, N'MiOe', N'mebi Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1340, N'MiOhm', N'mebi Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1341, N'Miosm', N'mebiosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1342, N'MiP', N'mebi Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1343, N'MiPa', N'mebi Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1344, N'Mipc', N'mebiparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1345, N'Miph', N'mebiphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1346, N'MiR', N'mebi Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1347, N'MiRAD', N'mebiradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1348, N'MiREM', N'mebiradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1349, N'MiS', N'mebi Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1350, N'Misb', N'mebistilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1351, N'Misr', N'mebisteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1352, N'MiSt', N'mebi Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1353, N'Mist', N'mebistere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1354, N'MiSv', N'mebi Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1355, N'MiT', N'mebi Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1356, N'Mit', N'mebitonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1357, N'Mitex', N'mebitex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1358, N'MiU', N'mebi Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1359, N'Miu', N'mebiunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1360, N'MiV', N'mebi Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1361, N'MiW', N'mebi Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1362, N'MiWb', N'mebi Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1363, N'MJ', N'mega Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1364, N'mJ', N'milli Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1365, N'Mkat', N'megakatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1366, N'mkat', N'millikatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1367, N'MKy', N'mega Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1368, N'mKy', N'milli Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1369, N'Ml', N'megaliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1370, N'ml', N'milliliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1371, N'Mlm', N'megalumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1372, N'mlm', N'millilumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1373, N'MLmb', N'mega Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1374, N'mLmb', N'milli Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1375, N'Mlx', N'megalux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1376, N'mlx', N'millilux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1377, N'Mm[H2O]', N'megameter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1378, N'mm[H2O]', N'millimeter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1379, N'Mm[Hg]', N'megameter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1380, N'mm[Hg]', N'millimeter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1381, N'Mmho', N'megamho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1382, N'mmho', N'millimho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1383, N'Mmol', N'megamole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1384, N'mmol', N'millimole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1385, N'MMx', N'mega Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1386, N'mMx', N'milli Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1387, N'MN', N'mega Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1388, N'mN', N'milli Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1389, N'MNp', N'meganeper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1390, N'mNp', N'millineper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1391, N'mo', N'month', 0, 0, 93)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1392, N'mo_g', N'mean Gregorian month', 0, 0, 93)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1393, N'mo_j', N'mean Julian month', 0, 0, 93)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1394, N'mo_s', N'synodal month', 0, 0, 93)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1395, N'MOe', N'mega Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1396, N'mOe', N'milli Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1397, N'MOhm', N'mega Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1398, N'mOhm', N'milli Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1399, N'Mosm', N'megaosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1400, N'mosm', N'milliosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1401, N'MP', N'mega Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1402, N'mP', N'milli Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1403, N'MPa', N'mega Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1404, N'mPa', N'milli Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1405, N'Mpc', N'megaparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1406, N'mpc', N'milliparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1407, N'Mph', N'megaphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1408, N'mph', N'milliphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1409, N'MR', N'mega Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1410, N'mR', N'milli Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1411, N'MRAD', N'megaradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1412, N'mRAD', N'milliradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1413, N'MREM', N'megaradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1414, N'mREM', N'milliradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1415, N'MS', N'mega Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1416, N'mS', N'milli Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1417, N'Msb', N'megastilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1418, N'msb', N'millistilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1419, N'Msr', N'megasteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1420, N'msr', N'millisteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1421, N'MSt', N'mega Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1422, N'Mst', N'megastere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1423, N'mSt', N'milli Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1424, N'mst', N'millistere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1425, N'MSv', N'mega Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1426, N'mSv', N'milli Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1427, N'MT', N'mega Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1428, N'Mt', N'megatonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1429, N'mT', N'milli Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1430, N'mt', N'millitonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1431, N'Mtex', N'megatex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1432, N'mtex', N'millitex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1433, N'MU', N'mega Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1434, N'Mu', N'megaunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1435, N'mU', N'milli Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1436, N'mu', N'milliunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1437, N'MV', N'mega Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1438, N'mV', N'milli Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1439, N'MW', N'mega Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1440, N'mW', N'milli Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1441, N'MWb', N'mega Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1442, N'mWb', N'milli Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1443, N'n[c]', N'nanovelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1444, N'n[e]', N'nanoelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1445, N'n[eps_0]', N'nanopermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1446, N'n[G]', N'nano Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1447, N'n[g]', N'nanostandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1448, N'n[h]', N'nano Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1449, N'n[iU]', N'nanointernational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1450, N'n[k]', N'nano Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1451, N'n[ly]', N'nanolight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1452, N'n[m_e]', N'nanoelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1453, N'n[m_p]', N'nanoproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1454, N'n[mu_0]', N'nanopermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1455, N'nA', N'nano Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1456, N'nar', N'nanoare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1457, N'nB', N'nanobel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1458, N'nB[10.nV]', N'nanobel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1459, N'nB[kW]', N'nanobel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1460, N'nB[mV]', N'nanobel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1461, N'nB[SPL]', N'nanobel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1462, N'nB[uV]', N'nanobel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1463, N'nB[V]', N'nanobel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1464, N'nB[W]', N'nanobel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1465, N'nbar', N'nanobar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1466, N'nBd', N'nanobaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1467, N'nBi', N'nano Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1468, N'nbit', N'nanobit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1469, N'nBq', N'nano Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1470, N'nBy', N'nanobyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1471, N'ncal', N'nanocalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1472, N'ncal_[15]', N'nanocalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1473, N'ncal_[20]', N'nanocalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1474, N'ncal_IT', N'nanointernational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1475, N'ncal_m', N'nanomean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1476, N'ncal_th', N'nanothermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1477, N'nCel', N'nanodegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1478, N'nCi', N'nano Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1479, N'ndyn', N'nanodyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1480, N'neq', N'nanoequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1481, N'nerg', N'nanoerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1482, N'neV', N'nanoelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1483, N'nF', N'nano Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1484, N'nG', N'nano Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1485, N'ng%', N'nanogram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1486, N'nGal', N'nano Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1487, N'nGb', N'nano Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1488, N'ngf', N'nanogram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1489, N'nGy', N'nano Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1490, N'nH', N'nano Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1491, N'nHz', N'nano Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1492, N'nJ', N'nano Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1493, N'nkat', N'nanokatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1494, N'nKy', N'nano Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1495, N'nl', N'nanoliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1496, N'nlm', N'nanolumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1497, N'nLmb', N'nano Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1498, N'nlx', N'nanolux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1499, N'nm[H2O]', N'nanometer of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1500, N'nm[Hg]', N'nanometer of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1501, N'nmho', N'nanomho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1502, N'nmol', N'nanomole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1503, N'nMx', N'nano Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1504, N'nN', N'nano Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1505, N'nNp', N'nanoneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1506, N'nOe', N'nano Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1507, N'nOhm', N'nano Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1508, N'nosm', N'nanoosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1509, N'nP', N'nano Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1510, N'nPa', N'nano Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1511, N'npc', N'nanoparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1512, N'nph', N'nanophot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1513, N'nR', N'nano Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1514, N'nRAD', N'nanoradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1515, N'nREM', N'nanoradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1516, N'nS', N'nano Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1517, N'nsb', N'nanostilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1518, N'nsr', N'nanosteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1519, N'nSt', N'nano Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1520, N'nst', N'nanostere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1521, N'nSv', N'nano Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1522, N'nT', N'nano Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1523, N'nt', N'nanotonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1524, N'ntex', N'nanotex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1525, N'nU', N'nano Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1526, N'nu', N'nanounified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1527, N'nV', N'nano Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1528, N'nW', N'nano Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1529, N'nWb', N'nano Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1530, N'P[c]', N'petavelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1531, N'p[c]', N'picovelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1532, N'P[e]', N'petaelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1533, N'p[e]', N'picoelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1534, N'P[eps_0]', N'petapermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1535, N'p[eps_0]', N'picopermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1536, N'P[G]', N'peta Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1537, N'P[g]', N'petastandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1538, N'p[G]', N'pico Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1539, N'p[g]', N'picostandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1540, N'P[h]', N'peta Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1541, N'p[h]', N'pico Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1542, N'P[iU]', N'petainternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1543, N'p[iU]', N'picointernational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1544, N'P[k]', N'peta Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1545, N'p[k]', N'pico Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1546, N'P[ly]', N'petalight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1547, N'p[ly]', N'picolight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1548, N'P[m_e]', N'petaelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1549, N'p[m_e]', N'picoelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1550, N'P[m_p]', N'petaproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1551, N'p[m_p]', N'picoproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1552, N'P[mu_0]', N'petapermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1553, N'p[mu_0]', N'picopermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1554, N'PA', N'peta Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1555, N'pA', N'pico Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1556, N'Par', N'petaare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1557, N'par', N'picoare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1558, N'PB', N'petabel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1559, N'pB', N'picobel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1560, N'PB[10.nV]', N'petabel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1561, N'pB[10.nV]', N'picobel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1562, N'PB[kW]', N'petabel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1563, N'pB[kW]', N'picobel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1564, N'PB[mV]', N'petabel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1565, N'pB[mV]', N'picobel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1566, N'PB[SPL]', N'petabel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1567, N'pB[SPL]', N'picobel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1568, N'PB[uV]', N'petabel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1569, N'pB[uV]', N'picobel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1570, N'PB[V]', N'petabel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1571, N'pB[V]', N'picobel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1572, N'PB[W]', N'petabel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1573, N'pB[W]', N'picobel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1574, N'Pbar', N'petabar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1575, N'pbar', N'picobar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1576, N'PBd', N'petabaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1577, N'pBd', N'picobaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1578, N'PBi', N'peta Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1579, N'pBi', N'pico Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1580, N'Pbit', N'petabit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1581, N'pbit', N'picobit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1582, N'PBq', N'peta Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1583, N'pBq', N'pico Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1584, N'PBy', N'petabyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1585, N'pBy', N'picobyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1586, N'Pcal', N'petacalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1587, N'pcal', N'picocalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1588, N'Pcal_[15]', N'petacalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1589, N'pcal_[15]', N'picocalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1590, N'Pcal_[20]', N'petacalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1591, N'pcal_[20]', N'picocalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1592, N'Pcal_IT', N'petainternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1593, N'pcal_IT', N'picointernational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1594, N'Pcal_m', N'petamean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1595, N'pcal_m', N'picomean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1596, N'Pcal_th', N'petathermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1597, N'pcal_th', N'picothermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1598, N'PCel', N'petadegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1599, N'pCel', N'picodegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1600, N'PCi', N'peta Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1601, N'pCi', N'pico Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1602, N'Pdyn', N'petadyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1603, N'pdyn', N'picodyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1604, N'Peq', N'petaequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1605, N'peq', N'picoequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1606, N'Perg', N'petaerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1607, N'perg', N'picoerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1608, N'PeV', N'petaelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1609, N'peV', N'picoelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1610, N'PF', N'peta Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1611, N'pF', N'pico Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1612, N'PG', N'peta Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1613, N'pG', N'pico Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1614, N'Pg%', N'petagram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1615, N'pg%', N'picogram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1616, N'PGal', N'peta Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1617, N'pGal', N'pico Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1618, N'PGb', N'peta Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1619, N'pGb', N'pico Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1620, N'Pgf', N'petagram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1621, N'pgf', N'picogram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1622, N'PGy', N'peta Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1623, N'pGy', N'pico Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1624, N'PH', N'peta Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1625, N'pH', N'pico Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1626, N'PHz', N'peta Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1627, N'pHz', N'pico Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1628, N'PJ', N'peta Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1629, N'pJ', N'pico Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1630, N'Pkat', N'petakatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1631, N'pkat', N'picokatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1632, N'PKy', N'peta Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1633, N'pKy', N'pico Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1634, N'Pl', N'petaliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1635, N'pl', N'picoliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1636, N'Plm', N'petalumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1637, N'plm', N'picolumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1638, N'PLmb', N'peta Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1639, N'pLmb', N'pico Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1640, N'Plx', N'petalux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1641, N'plx', N'picolux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1642, N'Pm[H2O]', N'petameter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1643, N'pm[H2O]', N'picometer of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1644, N'Pm[Hg]', N'petameter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1645, N'pm[Hg]', N'picometer of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1646, N'Pmho', N'petamho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1647, N'pmho', N'picomho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1648, N'Pmol', N'petamole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1649, N'pmol', N'picomole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1650, N'PMx', N'peta Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1651, N'pMx', N'pico Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1652, N'PN', N'peta Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1653, N'pN', N'pico Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1654, N'PNp', N'petaneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1655, N'pNp', N'piconeper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1656, N'POe', N'peta Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1657, N'pOe', N'pico Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1658, N'POhm', N'peta Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1659, N'pOhm', N'pico Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1660, N'Posm', N'petaosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1661, N'posm', N'picoosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1662, N'PP', N'peta Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1663, N'pP', N'pico Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1664, N'PPa', N'peta Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1665, N'pPa', N'pico Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1666, N'Ppc', N'petaparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1667, N'ppc', N'picoparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1668, N'Pph', N'petaphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1669, N'pph', N'picophot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1670, N'PR', N'peta Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1671, N'pR', N'pico Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1672, N'PRAD', N'petaradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1673, N'pRAD', N'picoradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1674, N'PREM', N'petaradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1675, N'pREM', N'picoradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1676, N'PS', N'peta Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1677, N'pS', N'pico Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1678, N'Psb', N'petastilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1679, N'psb', N'picostilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1680, N'Psr', N'petasteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1681, N'psr', N'picosteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1682, N'PSt', N'peta Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1683, N'Pst', N'petastere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1684, N'pSt', N'pico Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1685, N'pst', N'picostere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1686, N'PSv', N'peta Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1687, N'pSv', N'pico Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1688, N'PT', N'peta Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1689, N'Pt', N'petatonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1690, N'pT', N'pico Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1691, N'pt', N'picotonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1692, N'Ptex', N'petatex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1693, N'ptex', N'picotex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1694, N'PU', N'peta Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1695, N'Pu', N'petaunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1696, N'pU', N'pico Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1697, N'pu', N'picounified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1698, N'PV', N'peta Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1699, N'pV', N'pico Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1700, N'PW', N'peta Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1701, N'pW', N'pico Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1702, N'PWb', N'peta Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1703, N'pWb', N'pico Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1704, N'sph', N'spere', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1705, N'T[c]', N'teravelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1706, N'T[e]', N'teraelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1707, N'T[eps_0]', N'terapermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1708, N'T[G]', N'tera Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1709, N'T[g]', N'terastandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1710, N'T[h]', N'tera Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1711, N'T[iU]', N'terainternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1712, N'T[k]', N'tera Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1713, N'T[ly]', N'teralight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1714, N'T[m_e]', N'teraelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1715, N'T[m_p]', N'teraproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1716, N'T[mu_0]', N'terapermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1717, N'TA', N'tera Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1718, N'Tar', N'teraare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1719, N'TB', N'terabel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1720, N'TB[10.nV]', N'terabel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1721, N'TB[kW]', N'terabel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1722, N'TB[mV]', N'terabel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1723, N'TB[SPL]', N'terabel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1724, N'TB[uV]', N'terabel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1725, N'TB[V]', N'terabel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1726, N'TB[W]', N'terabel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1727, N'Tbar', N'terabar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1728, N'TBd', N'terabaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1729, N'TBi', N'tera Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1730, N'Tbit', N'terabit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1731, N'TBq', N'tera Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1732, N'TBy', N'terabyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1733, N'Tcal', N'teracalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1734, N'Tcal_[15]', N'teracalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1735, N'Tcal_[20]', N'teracalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1736, N'Tcal_IT', N'terainternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1737, N'Tcal_m', N'teramean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1738, N'Tcal_th', N'terathermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1739, N'TCel', N'teradegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1740, N'TCi', N'tera Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1741, N'Tdyn', N'teradyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1742, N'Teq', N'teraequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1743, N'Terg', N'teraerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1744, N'TeV', N'teraelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1745, N'TF', N'tera Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1746, N'TG', N'tera Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1747, N'Tg%', N'teragram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1748, N'TGal', N'tera Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1749, N'TGb', N'tera Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1750, N'Tgf', N'teragram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1751, N'TGy', N'tera Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1752, N'TH', N'tera Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1753, N'THz', N'tera Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1754, N'Ti[c]', N'tebivelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1755, N'Ti[e]', N'tebielementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1756, N'Ti[eps_0]', N'tebipermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1757, N'Ti[G]', N'tebi Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1758, N'Ti[g]', N'tebistandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1759, N'Ti[h]', N'tebi Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1760, N'Ti[iU]', N'tebiinternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1761, N'Ti[k]', N'tebi Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1762, N'Ti[ly]', N'tebilight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1763, N'Ti[m_e]', N'tebielectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1764, N'Ti[m_p]', N'tebiproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1765, N'Ti[mu_0]', N'tebipermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1766, N'TiA', N'tebi Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1767, N'Tiar', N'tebiare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1768, N'TiB', N'tebibel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1769, N'TiB[10.nV]', N'tebibel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1770, N'TiB[kW]', N'tebibel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1771, N'TiB[mV]', N'tebibel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1772, N'TiB[SPL]', N'tebibel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1773, N'TiB[uV]', N'tebibel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1774, N'TiB[V]', N'tebibel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1775, N'TiB[W]', N'tebibel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1776, N'Tibar', N'tebibar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1777, N'TiBd', N'tebibaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1778, N'TiBi', N'tebi Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1779, N'Tibit', N'tebibit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1780, N'TiBq', N'tebi Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1781, N'TiBy', N'tebibyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1782, N'Tical', N'tebicalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1783, N'Tical_[15]', N'tebicalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1784, N'Tical_[20]', N'tebicalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1785, N'Tical_IT', N'tebiinternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1786, N'Tical_m', N'tebimean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1787, N'Tical_th', N'tebithermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1788, N'TiCel', N'tebidegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1789, N'TiCi', N'tebi Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1790, N'Tidyn', N'tebidyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1791, N'Tieq', N'tebiequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1792, N'Tierg', N'tebierg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1793, N'TieV', N'tebielectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1794, N'TiF', N'tebi Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1795, N'TiG', N'tebi Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1796, N'Tig%', N'tebigram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1797, N'TiGal', N'tebi Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1798, N'TiGb', N'tebi Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1799, N'Tigf', N'tebigram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1800, N'TiGy', N'tebi Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1801, N'TiH', N'tebi Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1802, N'TiHz', N'tebi Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1803, N'TiJ', N'tebi Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1804, N'Tikat', N'tebikatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1805, N'TiKy', N'tebi Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1806, N'Til', N'tebiliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1807, N'Tilm', N'tebilumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1808, N'TiLmb', N'tebi Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1809, N'Tilx', N'tebilux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1810, N'Tim[H2O]', N'tebimeter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1811, N'Tim[Hg]', N'tebimeter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1812, N'Timho', N'tebimho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1813, N'Timol', N'tebimole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1814, N'TiMx', N'tebi Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1815, N'TiN', N'tebi Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1816, N'TiNp', N'tebineper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1817, N'TiOe', N'tebi Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1818, N'TiOhm', N'tebi Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1819, N'Tiosm', N'tebiosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1820, N'TiP', N'tebi Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1821, N'TiPa', N'tebi Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1822, N'Tipc', N'tebiparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1823, N'Tiph', N'tebiphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1824, N'TiR', N'tebi Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1825, N'TiRAD', N'tebiradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1826, N'TiREM', N'tebiradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1827, N'TiS', N'tebi Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1828, N'Tisb', N'tebistilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1829, N'Tisr', N'tebisteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1830, N'TiSt', N'tebi Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1831, N'Tist', N'tebistere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1832, N'TiSv', N'tebi Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1833, N'TiT', N'tebi Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1834, N'Tit', N'tebitonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1835, N'Titex', N'tebitex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1836, N'TiU', N'tebi Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1837, N'Tiu', N'tebiunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1838, N'TiV', N'tebi Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1839, N'TiW', N'tebi Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1840, N'TiWb', N'tebi Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1841, N'TJ', N'tera Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1842, N'Tkat', N'terakatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1843, N'TKy', N'tera Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1844, N'Tl', N'teraliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1845, N'Tlm', N'teralumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1846, N'TLmb', N'tera Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1847, N'Tlx', N'teralux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1848, N'Tm[H2O]', N'terameter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1849, N'Tm[Hg]', N'terameter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1850, N'Tmho', N'teramho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1851, N'Tmol', N'teramole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1852, N'TMx', N'tera Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1853, N'TN', N'tera Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1854, N'TNp', N'teraneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1855, N'TOe', N'tera Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1856, N'TOhm', N'tera Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1857, N'Tosm', N'teraosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1858, N'TP', N'tera Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1859, N'TPa', N'tera Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1860, N'Tpc', N'teraparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1861, N'Tph', N'teraphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1862, N'TR', N'tera Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1863, N'TRAD', N'teraradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1864, N'TREM', N'teraradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1865, N'TS', N'tera Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1866, N'Tsb', N'terastilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1867, N'Tsr', N'terasteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1868, N'TSt', N'tera Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1869, N'Tst', N'terastere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1870, N'TSv', N'tera Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1871, N'TT', N'tera Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1872, N'Tt', N'teratonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1873, N'Ttex', N'teratex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1874, N'TU', N'tera Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1875, N'Tu', N'teraunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1876, N'TV', N'tera Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1877, N'TW', N'tera Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1878, N'TWb', N'tera Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1879, N'wk', N'week', 0, 0, 93)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1880, N'y[c]', N'yoctovelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1881, N'Y[c]', N'yottavelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1882, N'y[e]', N'yoctoelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1883, N'Y[e]', N'yottaelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1884, N'y[eps_0]', N'yoctopermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1885, N'Y[eps_0]', N'yottapermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1886, N'y[G]', N'yocto Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1887, N'y[g]', N'yoctostandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1888, N'Y[G]', N'yotta Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1889, N'Y[g]', N'yottastandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1890, N'y[h]', N'yocto Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1891, N'Y[h]', N'yotta Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1892, N'y[iU]', N'yoctointernational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1893, N'Y[iU]', N'yottainternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1894, N'y[k]', N'yocto Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1895, N'Y[k]', N'yotta Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1896, N'y[ly]', N'yoctolight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1897, N'Y[ly]', N'yottalight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1898, N'y[m_e]', N'yoctoelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1899, N'Y[m_e]', N'yottaelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1900, N'y[m_p]', N'yoctoproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1901, N'Y[m_p]', N'yottaproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1902, N'y[mu_0]', N'yoctopermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1903, N'Y[mu_0]', N'yottapermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1904, N'yA', N'yocto Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1905, N'YA', N'yotta Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1906, N'yar', N'yoctoare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1907, N'Yar', N'yottaare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1908, N'yB', N'yoctobel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1909, N'YB', N'yottabel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1910, N'yB[10.nV]', N'yoctobel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1911, N'YB[10.nV]', N'yottabel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1912, N'yB[kW]', N'yoctobel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1913, N'YB[kW]', N'yottabel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1914, N'yB[mV]', N'yoctobel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1915, N'YB[mV]', N'yottabel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1916, N'yB[SPL]', N'yoctobel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1917, N'YB[SPL]', N'yottabel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1918, N'yB[uV]', N'yoctobel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1919, N'YB[uV]', N'yottabel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1920, N'yB[V]', N'yoctobel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1921, N'YB[V]', N'yottabel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1922, N'yB[W]', N'yoctobel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1923, N'YB[W]', N'yottabel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1924, N'ybar', N'yoctobar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1925, N'Ybar', N'yottabar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1926, N'yBd', N'yoctobaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1927, N'YBd', N'yottabaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1928, N'yBi', N'yocto Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1929, N'YBi', N'yotta Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1930, N'ybit', N'yoctobit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1931, N'Ybit', N'yottabit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1932, N'yBq', N'yocto Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1933, N'YBq', N'yotta Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1934, N'yBy', N'yoctobyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1935, N'YBy', N'yottabyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1936, N'ycal', N'yoctocalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1937, N'Ycal', N'yottacalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1938, N'ycal_[15]', N'yoctocalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1939, N'Ycal_[15]', N'yottacalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1940, N'ycal_[20]', N'yoctocalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1941, N'Ycal_[20]', N'yottacalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1942, N'ycal_IT', N'yoctointernational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1943, N'Ycal_IT', N'yottainternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1944, N'ycal_m', N'yoctomean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1945, N'Ycal_m', N'yottamean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1946, N'ycal_th', N'yoctothermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1947, N'Ycal_th', N'yottathermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1948, N'yCel', N'yoctodegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1949, N'YCel', N'yottadegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1950, N'yCi', N'yocto Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1951, N'YCi', N'yotta Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1952, N'ydyn', N'yoctodyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1953, N'Ydyn', N'yottadyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1954, N'yeq', N'yoctoequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1955, N'Yeq', N'yottaequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1956, N'yerg', N'yoctoerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1957, N'Yerg', N'yottaerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1958, N'yeV', N'yoctoelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1959, N'YeV', N'yottaelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1960, N'yF', N'yocto Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1961, N'YF', N'yotta Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1962, N'yG', N'yocto Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1963, N'YG', N'yotta Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1964, N'yg%', N'yoctogram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1965, N'Yg%', N'yottagram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1966, N'yGal', N'yocto Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1967, N'YGal', N'yotta Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1968, N'yGb', N'yocto Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1969, N'YGb', N'yotta Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1970, N'ygf', N'yoctogram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1971, N'Ygf', N'yottagram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1972, N'yGy', N'yocto Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1973, N'YGy', N'yotta Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1974, N'yH', N'yocto Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1975, N'YH', N'yotta Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1976, N'yHz', N'yocto Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1977, N'YHz', N'yotta Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1978, N'yJ', N'yocto Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1979, N'YJ', N'yotta Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1980, N'ykat', N'yoctokatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1981, N'Ykat', N'yottakatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1982, N'yKy', N'yocto Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1983, N'YKy', N'yotta Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1984, N'yl', N'yoctoliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1985, N'Yl', N'yottaliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1986, N'ylm', N'yoctolumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1987, N'Ylm', N'yottalumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1988, N'yLmb', N'yocto Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1989, N'YLmb', N'yotta Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1990, N'ylx', N'yoctolux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1991, N'Ylx', N'yottalux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1992, N'ym[H2O]', N'yoctometer of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1993, N'Ym[H2O]', N'yottameter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1994, N'ym[Hg]', N'yoctometer of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1995, N'Ym[Hg]', N'yottameter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1996, N'ymho', N'yoctomho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1997, N'Ymho', N'yottamho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1998, N'ymol', N'yoctomole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (1999, N'Ymol', N'yottamole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2000, N'yMx', N'yocto Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2001, N'YMx', N'yotta Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2002, N'yN', N'yocto Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2003, N'YN', N'yotta Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2004, N'yNp', N'yoctoneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2005, N'YNp', N'yottaneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2006, N'yOe', N'yocto Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2007, N'YOe', N'yotta Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2008, N'yOhm', N'yocto Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2009, N'YOhm', N'yotta Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2010, N'yosm', N'yoctoosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2011, N'Yosm', N'yottaosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2012, N'yP', N'yocto Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2013, N'YP', N'yotta Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2014, N'yPa', N'yocto Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2015, N'YPa', N'yotta Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2016, N'ypc', N'yoctoparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2017, N'Ypc', N'yottaparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2018, N'yph', N'yoctophot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2019, N'Yph', N'yottaphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2020, N'yR', N'yocto Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2021, N'YR', N'yotta Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2022, N'yRAD', N'yoctoradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2023, N'YRAD', N'yottaradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2024, N'yREM', N'yoctoradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2025, N'YREM', N'yottaradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2026, N'yS', N'yocto Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2027, N'YS', N'yotta Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2028, N'ysb', N'yoctostilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2029, N'Ysb', N'yottastilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2030, N'ysr', N'yoctosteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2031, N'Ysr', N'yottasteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2032, N'ySt', N'yocto Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2033, N'yst', N'yoctostere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2034, N'YSt', N'yotta Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2035, N'Yst', N'yottastere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2036, N'ySv', N'yocto Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2037, N'YSv', N'yotta Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2038, N'yT', N'yocto Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2039, N'yt', N'yoctotonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2040, N'YT', N'yotta Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2041, N'Yt', N'yottatonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2042, N'ytex', N'yoctotex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2043, N'Ytex', N'yottatex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2044, N'yU', N'yocto Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2045, N'yu', N'yoctounified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2046, N'YU', N'yotta Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2047, N'Yu', N'yottaunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2048, N'yV', N'yocto Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2049, N'YV', N'yotta Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2050, N'yW', N'yocto Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2051, N'YW', N'yotta Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2052, N'yWb', N'yocto Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2053, N'YWb', N'yotta Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2054, N'z[c]', N'zeptovelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2055, N'Z[c]', N'zettavelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2056, N'z[e]', N'zeptoelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2057, N'Z[e]', N'zettaelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2058, N'z[eps_0]', N'zeptopermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2059, N'Z[eps_0]', N'zettapermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2060, N'z[G]', N'zepto Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2061, N'z[g]', N'zeptostandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2062, N'Z[G]', N'zetta Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2063, N'Z[g]', N'zettastandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2064, N'z[h]', N'zepto Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2065, N'Z[h]', N'zetta Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2066, N'z[iU]', N'zeptointernational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2067, N'Z[iU]', N'zettainternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2068, N'z[k]', N'zepto Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2069, N'Z[k]', N'zetta Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2070, N'z[ly]', N'zeptolight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2071, N'Z[ly]', N'zettalight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2072, N'z[m_e]', N'zeptoelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2073, N'Z[m_e]', N'zettaelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2074, N'z[m_p]', N'zeptoproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2075, N'Z[m_p]', N'zettaproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2076, N'z[mu_0]', N'zeptopermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2077, N'Z[mu_0]', N'zettapermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2078, N'zA', N'zepto Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2079, N'ZA', N'zetta Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2080, N'zar', N'zeptoare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2081, N'Zar', N'zettaare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2082, N'zB', N'zeptobel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2083, N'ZB', N'zettabel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2084, N'zB[10.nV]', N'zeptobel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2085, N'ZB[10.nV]', N'zettabel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2086, N'zB[kW]', N'zeptobel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2087, N'ZB[kW]', N'zettabel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2088, N'zB[mV]', N'zeptobel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2089, N'ZB[mV]', N'zettabel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2090, N'zB[SPL]', N'zeptobel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2091, N'ZB[SPL]', N'zettabel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2092, N'zB[uV]', N'zeptobel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2093, N'ZB[uV]', N'zettabel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2094, N'zB[V]', N'zeptobel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2095, N'ZB[V]', N'zettabel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2096, N'zB[W]', N'zeptobel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2097, N'ZB[W]', N'zettabel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2098, N'zbar', N'zeptobar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2099, N'Zbar', N'zettabar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2100, N'zBd', N'zeptobaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2101, N'ZBd', N'zettabaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2102, N'zBi', N'zepto Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2103, N'ZBi', N'zetta Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2104, N'zbit', N'zeptobit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2105, N'Zbit', N'zettabit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2106, N'zBq', N'zepto Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2107, N'ZBq', N'zetta Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2108, N'zBy', N'zeptobyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2109, N'ZBy', N'zettabyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2110, N'zcal', N'zeptocalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2111, N'Zcal', N'zettacalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2112, N'zcal_[15]', N'zeptocalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2113, N'Zcal_[15]', N'zettacalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2114, N'zcal_[20]', N'zeptocalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2115, N'Zcal_[20]', N'zettacalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2116, N'zcal_IT', N'zeptointernational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2117, N'Zcal_IT', N'zettainternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2118, N'zcal_m', N'zeptomean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2119, N'Zcal_m', N'zettamean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2120, N'zcal_th', N'zeptothermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2121, N'Zcal_th', N'zettathermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2122, N'zCel', N'zeptodegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2123, N'ZCel', N'zettadegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2124, N'zCi', N'zepto Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2125, N'ZCi', N'zetta Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2126, N'zdyn', N'zeptodyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2127, N'Zdyn', N'zettadyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2128, N'zeq', N'zeptoequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2129, N'Zeq', N'zettaequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2130, N'zerg', N'zeptoerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2131, N'Zerg', N'zettaerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2132, N'zeV', N'zeptoelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2133, N'ZeV', N'zettaelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2134, N'zF', N'zepto Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2135, N'ZF', N'zetta Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2136, N'zG', N'zepto Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2137, N'ZG', N'zetta Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2138, N'zg%', N'zeptogram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2139, N'Zg%', N'zettagram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2140, N'zGal', N'zepto Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2141, N'ZGal', N'zetta Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2142, N'zGb', N'zepto Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2143, N'ZGb', N'zetta Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2144, N'zgf', N'zeptogram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2145, N'Zgf', N'zettagram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2146, N'zGy', N'zepto Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2147, N'ZGy', N'zetta Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2148, N'zH', N'zepto Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2149, N'ZH', N'zetta Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2150, N'zHz', N'zepto Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2151, N'ZHz', N'zetta Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2152, N'zJ', N'zepto Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2153, N'ZJ', N'zetta Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2154, N'zkat', N'zeptokatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2155, N'Zkat', N'zettakatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2156, N'zKy', N'zepto Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2157, N'ZKy', N'zetta Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2158, N'zl', N'zeptoliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2159, N'Zl', N'zettaliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2160, N'zlm', N'zeptolumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2161, N'Zlm', N'zettalumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2162, N'zLmb', N'zepto Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2163, N'ZLmb', N'zetta Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2164, N'zlx', N'zeptolux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2165, N'Zlx', N'zettalux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2166, N'zm[H2O]', N'zeptometer of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2167, N'Zm[H2O]', N'zettameter of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2168, N'zm[Hg]', N'zeptometer of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2169, N'Zm[Hg]', N'zettameter of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2170, N'zmho', N'zeptomho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2171, N'Zmho', N'zettamho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2172, N'zmol', N'zeptomole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2173, N'Zmol', N'zettamole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2174, N'zMx', N'zepto Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2175, N'ZMx', N'zetta Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2176, N'zN', N'zepto Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2177, N'ZN', N'zetta Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2178, N'zNp', N'zeptoneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2179, N'ZNp', N'zettaneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2180, N'zOe', N'zepto Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2181, N'ZOe', N'zetta Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2182, N'zOhm', N'zepto Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2183, N'ZOhm', N'zetta Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2184, N'zosm', N'zeptoosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2185, N'Zosm', N'zettaosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2186, N'zP', N'zepto Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2187, N'ZP', N'zetta Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2188, N'zPa', N'zepto Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2189, N'ZPa', N'zetta Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2190, N'zpc', N'zeptoparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2191, N'Zpc', N'zettaparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2192, N'zph', N'zeptophot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2193, N'Zph', N'zettaphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2194, N'zR', N'zepto Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2195, N'ZR', N'zetta Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2196, N'zRAD', N'zeptoradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2197, N'ZRAD', N'zettaradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2198, N'zREM', N'zeptoradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2199, N'ZREM', N'zettaradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2200, N'zS', N'zepto Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2201, N'ZS', N'zetta Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2202, N'zsb', N'zeptostilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2203, N'Zsb', N'zettastilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2204, N'zsr', N'zeptosteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2205, N'Zsr', N'zettasteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2206, N'zSt', N'zepto Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2207, N'zst', N'zeptostere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2208, N'ZSt', N'zetta Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2209, N'Zst', N'zettastere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2210, N'zSv', N'zepto Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2211, N'ZSv', N'zetta Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2212, N'zT', N'zepto Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2213, N'zt', N'zeptotonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2214, N'ZT', N'zetta Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2215, N'Zt', N'zettatonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2216, N'ztex', N'zeptotex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2217, N'Ztex', N'zettatex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2218, N'zU', N'zepto Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2219, N'zu', N'zeptounified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2220, N'ZU', N'zetta Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2221, N'Zu', N'zettaunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2222, N'zV', N'zepto Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2223, N'ZV', N'zetta Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2224, N'zW', N'zepto Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2225, N'ZW', N'zetta Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2226, N'zWb', N'zepto Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2227, N'ZWb', N'zetta Weber', 0, 0, 65)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2228, N'μ[c]', N'microvelocity of light', 0, 0, 94)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2229, N'μ[e]', N'microelementary charge', 0, 0, 35)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2230, N'μ[eps_0]', N'micropermittivity of vacuum', 0, 0, 38)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2231, N'μ[G]', N'micro Newtonian constant of gravitation', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2232, N'μ[g]', N'microstandard acceleration of free fall', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2233, N'μ[h]', N'micro Planck constant', 0, 0, 4)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2234, N'μ[iU]', N'microinternational unit', 0, 0, 13)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2235, N'μ[k]', N'micro Boltzmann constant', 0, 0, 1)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2236, N'μ[ly]', N'microlight-year', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2237, N'μ[m_e]', N'microelectron mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2238, N'μ[m_p]', N'microproton mass', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2239, N'μ[mu_0]', N'micropermeability of vacuum', 0, 0, 68)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2240, N'μA', N'micro Ampère', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2241, N'μar', N'microare', 0, 0, 16)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2242, N'μB', N'microbel', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2243, N'μB[10.nV]', N'microbel 10 nanovolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2244, N'μB[kW]', N'microbel kilowatt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2245, N'μB[mV]', N'microbel millivolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2246, N'μB[SPL]', N'microbel sound pressure', 0, 0, 79)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2247, N'μB[uV]', N'microbel microvolt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2248, N'μB[V]', N'microbel volt', 0, 0, 40)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2249, N'μB[W]', N'microbel watt', 0, 0, 77)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2250, N'μbar', N'microbar', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2251, N'μBd', N'microbaud', 0, 0, 89)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2252, N'μBi', N'micro Biot', 0, 0, 37)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2253, N'μbit', N'microbit', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2254, N'μBq', N'micro Becquerel', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2255, N'μBy', N'microbyte', 0, 0, 10)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2256, N'μcal', N'microcalorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2257, N'μcal_[15]', N'microcalorie at 15 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2258, N'μcal_[20]', N'microcalorie at 20 °C', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2259, N'μcal_IT', N'microinternational table calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2260, N'μcal_m', N'micromean calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2261, N'μcal_th', N'microthermochemical calorie', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2262, N'μCel', N'microdegree Celsius', 0, 0, 92)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2263, N'μCi', N'micro Curie', 0, 0, 85)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2264, N'μdyn', N'microdyne', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2265, N'μeq', N'microequivalents', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2266, N'μerg', N'microerg', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2267, N'μeV', N'microelectronvolt', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2268, N'μF', N'micro Farad', 0, 0, 34)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2269, N'μG', N'micro Gauss', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2270, N'μg%', N'microgram percent', 0, 0, 71)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2271, N'μGal', N'micro Gal', 0, 0, 2)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2272, N'μGb', N'micro Gilbert', 0, 0, 69)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2273, N'μgf', N'microgram-force', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2274, N'μGy', N'micro Gray', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2275, N'μH', N'micro Henry', 0, 0, 56)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2276, N'μHz', N'micro Hertz', 0, 0, 49)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2277, N'μJ', N'micro Joule', 0, 0, 42)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2278, N'μkat', N'microkatal', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2279, N'μKy', N'micro Kayser', 0, 0, 62)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2280, N'μl', N'microliter', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2281, N'μlm', N'microlumen', 0, 0, 64)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2282, N'μLmb', N'micro Lambert', 0, 0, 27)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2283, N'μlx', N'microlux', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2284, N'μm[H2O]', N'micrometer of water column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2285, N'μm[Hg]', N'micrometer of mercury column', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2286, N'μmho', N'micromho', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2287, N'μmol', N'micromole', 0, 0, 11)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2288, N'μMx', N'micro Maxwell', 0, 0, 46)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2289, N'μN', N'micro Newton', 0, 0, 47)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2290, N'μNp', N'microneper', 0, 0, 60)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2291, N'μOe', N'micro Oersted', 0, 0, 66)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2292, N'μOhm', N'micro Ohm', 0, 0, 41)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2293, N'μosm', N'microosmole', 0, 0, 12)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2294, N'μP', N'micro Poise', 0, 0, 32)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2295, N'μPa', N'micro Pascal', 0, 0, 78)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2296, N'μpc', N'microparsec', 0, 0, 59)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2297, N'μph', N'microphot', 0, 0, 55)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2298, N'μR', N'micro Roentgen', 0, 0, 57)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2299, N'μRAD', N'microradiation absorbed dose', 0, 0, 43)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2300, N'μREM', N'microradiation equivalent man', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2301, N'μS', N'micro Siemens', 0, 0, 36)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2302, N'μsb', N'microstilb', 0, 0, 63)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2303, N'μsr', N'microsteradian', 0, 0, 91)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2304, N'μSt', N'micro Stokes', 0, 0, 58)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2305, N'μst', N'microstere', 0, 0, 96)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2306, N'μSv', N'micro Sievert', 0, 0, 30)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2307, N'μT', N'micro Tesla', 0, 0, 67)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2308, N'μt', N'microtonne', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2309, N'μtex', N'microtex', 0, 0, 61)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2310, N'μU', N'micro Unit', 0, 0, 28)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2311, N'μu', N'microunified atomic mass unit', 0, 0, 70)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2312, N'μV', N'micro Volt', 0, 0, 39)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2313, N'μW', N'micro Watt', 0, 0, 76)

INSERT [model].[UnitsOfMeasurement] ([Id], [Abbreviation], [FullName], [OrdinalId], [IsDeactivated], [UnitOfMeasurementTypeId]) VALUES (2314, N'μWb', N'micro Weber', 0, 0, 65)

SET IDENTITY_INSERT [model].[UnitsOfMeasurement] OFF

