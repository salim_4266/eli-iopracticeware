﻿--Add column to Immunizations for model.Vaccine

IF NOT EXISTS (		
SELECT *
		FROM sys.columns sc
		INNER JOIN sys.tables st ON sc.Object_Id = st.Object_Id
		INNER JOIN sys.schemas sch ON st.schema_id = sch.schema_id
		WHERE sc.NAME = 'Abbreviation'
			AND st.NAME = 'Immunizations'
			AND sch.name = 'dbo'
		)
BEGIN
	ALTER TABLE dbo.Immunizations ADD Abbreviation NVARCHAR(max)
END

GO

--Delete old unused values
DELETE FROM dbo.Immunizations
WHERE ImmNo NOT IN (SELECT ImmNo FROM dbo.Patient_Immunizations)

--Insert new rows

INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Adenovirus, type 4 and type 7, live, oral', 0, 0,0, 143, 'Adenovirus types 4 and 7')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'anthrax vaccine', 0, 0,0, 24, 'anthrax')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Bacillus Calmette-Guerin vaccine', 0, 0,0, 19, 'BCG')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'botulinum antitoxin', 0, 0,0, 27, 'botulinum antitoxin')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'cytomegalovirus immune globulin, intravenous', 0, 0,0, 29, 'CMVIG')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'diphtheria antitoxin', 0, 0,0, 12, 'diphtheria antitoxin')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'diphtheria and tetanus toxoids, adsorbed for pedia', 0, 0,0, 28, 'DT (pediatric)')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'diphtheria, tetanus toxoids and acellular pertussi', 0, 0,0, 20, 'DTaP')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'diphtheria, tetanus toxoids and acellular pertussi', 0, 0,0, 106, 'DTaP, 5 pertussis antigens')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Diphtheria and Tetanus Toxoids and Acellular Pertu', 0, 0,0, 146, 'DTaP,IPV,Hib,HepB')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'DTaP-hepatitis B and poliovirus vaccine', 0, 0,0, 110, 'DTaP-Hep B-IPV')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'DTaP-Haemophilus influenzae type b conjugate vacci', 0, 0,0, 50, 'DTaP-Hib')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'diphtheria, tetanus toxoids and acellular pertussi', 0, 0,0, 120, 'DTaP-Hib-IPV')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Diphtheria, tetanus toxoids and acellular pertussi', 0, 0,0, 130, 'DTaP-IPV')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'hepatitis B immune globulin', 0, 0,0, 30, 'HBIG')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'hepatitis A vaccine, adult dosage', 0, 0,0, 52, 'Hep A, adult')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'hepatitis A vaccine, pediatric/adolescent dosage, ', 0, 0,0, 83, 'Hep A, ped/adol, 2 dose')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'hepatitis A and hepatitis B vaccine', 0, 0,0, 104, 'Hep A-Hep B')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'hepatitis B vaccine, dialysis patient dosage', 0, 0,0, 44, 'Hep B, dialysis')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'hepatitis B vaccine, pediatric or pediatric/adoles', 0, 0,0, 8, 'Hep B, adolescent or pediatric')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'hepatitis B vaccine, adult dosage', 0, 0,0, 43, 'Hep B, adult')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Haemophilus influenzae type b vaccine, PRP-OMP con', 0, 0,0, 49, 'Hib (PRP-OMP)')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Haemophilus influenzae type b vaccine, PRP-T conju', 0, 0,0, 48, 'Hib (PRP-T)')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Haemophilus influenzae type b conjugate and Hepati', 0, 0,0, 51, 'Hib-Hep B')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'human papilloma virus vaccine, bivalent', 0, 0,0, 118, 'HPV, bivalent')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'human papilloma virus vaccine, quadrivalent', 0, 0,0, 62, 'HPV, quadrivalent')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'immune globulin, intramuscular', 0, 0,0, 86, 'IG')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'immune globulin, intravenous', 0, 0,0, 87, 'IGIV')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'influenza, high dose seasonal, preservative-free', 0, 0,0, 135, 'Influenza, high dose seasonal')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Influenza, injectable, Madin Darby Canine Kidney, ', 0, 0,0, 153, 'Influenza, injectable, MDCK, preservative free')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'influenza, injectable, quadrivalent, contains pres', 0, 0,0, 158, 'influenza, injectable, quadrivalent')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Influenza, injectable, quadrivalent, preservative ', 0, 0,0, 150, 'influenza, injectable, quadrivalent, preservative ')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'influenza virus vaccine, live, attenuated, for int', 0, 0,0, 111, 'influenza, live, intranasal')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'influenza, live, intranasal, quadrivalent', 0, 0,0, 149, 'influenza, live, intranasal, quadrivalent')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Seasonal, trivalent, recombinant, injectable influ', 0, 0,0, 155, 'influenza, recombinant, injectable, preservative f')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Influenza, seasonal, injectable', 0, 0,0, 141, 'Influenza, seasonal, injectable')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Influenza, seasonal, injectable, preservative free', 0, 0,0, 140, 'Influenza, seasonal, injectable, preservative free')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'seasonal influenza, intradermal, preservative free', 0, 0,0, 144, 'influenza, seasonal, intradermal, preservative fre')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'poliovirus vaccine, inactivated', 0, 0,0, 10, 'IPV')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Japanese Encephalitis vaccine for intramuscular ad', 0, 0,0, 134, 'Japanese Encephalitis IM')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Japanese Encephalitis Vaccine SC', 0, 0,0, 39, 'Japanese encephalitis SC')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Meningococcal Groups C and Y and Haemophilus b Tet', 0, 0,0, 148, 'Meningococcal C/Y-HIB PRP')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'meningococcal oligosaccharide (groups A, C, Y and ', 0, 0,0, 136, 'Meningococcal MCV4O')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'meningococcal polysaccharide (groups A, C, Y and W', 0, 0,0, 114, 'meningococcal MCV4P')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'meningococcal polysaccharide vaccine (MPSV4)', 0, 0,0, 32, 'meningococcal MPSV4')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'measles, mumps and rubella virus vaccine', 0, 0,0, 3, 'MMR')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'measles, mumps, rubella, and varicella virus vacci', 0, 0,0, 94, 'MMRV')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'mumps virus vaccine', 0, 0,0, 7, 'mumps')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'plague vaccine', 0, 0,0, 23, 'plague')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'pneumococcal conjugate vaccine, 13 valent', 0, 0,0, 133, 'Pneumococcal conjugate PCV 13')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'pneumococcal conjugate vaccine, 7 valent', 0, 0,0, 100, 'pneumococcal conjugate PCV 7')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'pneumococcal polysaccharide vaccine, 23 valent', 0, 0,0, 33, 'pneumococcal polysaccharide PPV23')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'rabies vaccine, for intradermal injection', 0, 0,0, 40, 'rabies, intradermal injection')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'rabies vaccine, for intramuscular injection', 0, 0,0, 18, 'rabies, intramuscular injection')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Rho(D) Immune globulin - IM', 0, 0,0, 157, 'Rho(D) -IG IM')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'Rho(D) Immune globulin- IV or IM', 0, 0,0, 156, 'Rho(D)-IG')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'rabies immune globulin', 0, 0,0, 34, 'RIG')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'rotavirus, live, monovalent vaccine', 0, 0,0, 119, 'rotavirus, monovalent')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'rotavirus, live, pentavalent vaccine', 0, 0,0, 116, 'rotavirus, pentavalent')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'respiratory syncytial virus immune globulin, intra', 0, 0,0, 71, 'RSV-IGIV')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'respiratory syncytial virus monoclonal antibody (p', 0, 0,0, 93, 'RSV-MAb')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'respiratory syncytial virus monoclonal antibody (m', 0, 0,0, 145, 'RSV-MAb (new)')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'rubella virus vaccine', 0, 0,0, 6, 'rubella')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'tetanus and diphtheria toxoids, not adsorbed, for ', 0, 0,0, 138, 'Td (adult)')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'tetanus and diphtheria toxoids, adsorbed, preserva', 0, 0,0, 113, 'Td (adult) preservative free')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'tetanus and diphtheria toxoids, adsorbed, for adul', 0, 0,0, 9, 'Td (adult), adsorbed')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'tetanus toxoid, reduced diphtheria toxoid, and ace', 0, 0,0, 115, 'Tdap')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'tetanus toxoid, adsorbed', 0, 0,0, 35, 'tetanus toxoid, adsorbed')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'tetanus toxoid, not adsorbed', 0, 0,0, 142, 'tetanus toxoid, not adsorbed')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'tetanus immune globulin', 0, 0,0, 13, 'TIG')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'typhoid vaccine, live, oral', 0, 0,0, 25, 'typhoid, oral')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'typhoid vaccine, parenteral, other than acetone-ki', 0, 0,0, 41, 'typhoid, parenteral')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'typhoid vaccine, parenteral, acetone-killed, dried', 0, 0,0, 53, 'typhoid, parenteral, AKD (U.S. military)')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'typhoid Vi capsular polysaccharide vaccine', 0, 0,0, 101, 'typhoid, ViCPs')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'vaccinia (smallpox) vaccine', 0, 0,0, 75, 'vaccinia (smallpox)')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'vaccinia immune globulin', 0, 0,0, 79, 'vaccinia immune globulin')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'varicella virus vaccine', 0, 0,0, 21, 'varicella')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'varicella zoster immune globulin', 0, 0,0, 36, 'VZIG')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'yellow fever vaccine', 0, 0,0, 37, 'yellow fever')
INSERT INTO dbo.Immunizations ([ImmName], [ImmDuration], [ImmCPT], [SortNo], [ImmCode], [Abbreviation]) VALUES ( 'zoster vaccine, live', 0, 0,0, 121, 'zoster')
GO

--Mapping for cvx code.
IF NOT EXISTS (SELECT * FROM model.ExternalSystemEntityMappings WHERE PracticeRepositoryEntityId = 35)
BEGIN
	INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
	SELECT 35 AS PracticeRepositoryEntityId
		,imm.ImmNo AS PracticeRepositoryEntityKey
		,es.Id AS ExternalSystemEntityId
		,imm.ImmCode AS ExternalSystemEntityKey --We can use this Id because the script above is generated using the exteral data set's Id.
	FROM dbo.Immunizations imm
INNER JOIN model.ExternalSystemEntities es ON es.Name = 'Vaccine'
END