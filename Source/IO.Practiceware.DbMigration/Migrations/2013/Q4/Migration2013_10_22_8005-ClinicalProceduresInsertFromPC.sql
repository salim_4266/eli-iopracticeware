﻿ALTER TABLE model.ClinicalProcedures DISABLE TRIGGER ALL;
ALTER TABLE model.ClinicalProcedures NOCHECK CONSTRAINT ALL;


-- adds tho the ClinicalProcedures table, the missing Clinical Procedures that exist in the pre-exist PatientClinical table.
;WITH CTE AS (
SELECT 
pc.Symptom AS Name
FROM dbo.PatientClinical pc
LEFT JOIN model.ClinicalProcedures cp ON '/' + cp.Name = pc.Symptom
WHERE pc.ClinicalType = 'F' 
	AND cp.Id IS NULL
GROUP BY pc.Symptom 
)
INSERT INTO model.ClinicalProcedures (Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated)
SELECT
SUBSTRING(Name,2,LEN(Name)) AS Name
,CONVERT(BIT,0) AS IsSocial
,1 AS ClinicalProcedureCategoryId
,CONVERT(BIT,0) AS IsDeactivated
FROM CTE

INSERT INTO model.ClinicalProcedures (Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated)
select 
code 
,CONVERT(BIT,0) AS IsSocial
,1 AS ClinicalProcedureCategoryId
,CONVERT(BIT,0) AS IsDeactivated
from PracticeCodeTable 
where ReferenceType like 'SURGERYTYPE'

EXCEPT SELECT Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated from model.ClinicalProcedures

INSERT INTO model.ClinicalProcedures (Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated)
SELECT DISTINCT
[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=SCHEDULE\sSURGERY\-3\/)(.*?)(?=(\s\(O[D|S|U]\)\-F\/\-\d\/|\-F\/\-\d\/))') AS [Procedure]
,CONVERT(BIT,0) AS IsSocial
,1 AS ClinicalProcedureCategoryId
,CONVERT(BIT,0) AS IsDeactivated
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
WHERE pc.FindingDetail LIKE 'SCHEDULE SURGERY-3/%' 

EXCEPT SELECT Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated from model.ClinicalProcedures


INSERT INTO model.ClinicalProcedures (Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated)
SELECT DISTINCT
[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[O|0]\/)(.*?)(?=\-F\/\-[O|1]\/)') AS [Procedure]
,CONVERT(BIT,0) AS IsSocial
,1 AS ClinicalProcedureCategoryId
,CONVERT(BIT,0) AS IsDeactivated
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
WHERE (pc.FindingDetail LIKE 'OFFICE PROCEDURES-O%' OR pc.FindingDetail LIKE 'OFFICE PROCEDURES-0%')
EXCEPT SELECT Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated from model.ClinicalProcedures

ALTER TABLE model.ClinicalProcedures ENABLE TRIGGER ALL;
ALTER TABLE model.ClinicalProcedures CHECK CONSTRAINT ALL;