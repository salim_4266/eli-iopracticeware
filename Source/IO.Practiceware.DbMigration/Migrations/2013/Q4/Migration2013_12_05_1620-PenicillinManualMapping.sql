﻿INSERT INTO model.Allergens (IsAdverseDrugReaction,Name,OrdinalId,IsDeactivated)
SELECT 
CONVERT(BIT,1)
,'Penicillin G benzathine'
,1
,CONVERT(BIT,0)

DECLARE @PracticeRepositoryEntityKey INT
SELECT @PracticeRepositoryEntityKey = SCOPE_IDENTITY()

DECLARE @RxNormExternalSystemId INT
SELECT @RxNormExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'RxNorm'

DECLARE @AllergenId INT
SELECT @AllergenId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'Allergen'

DECLARE @RxNormAllergen INT
SELECT @RxNormAllergen = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @RxNormExternalSystemId
	AND Name = 'Allergen'

DECLARE @ExternalSystemEntityKey INT
SELECT TOP 1 
@ExternalSystemEntityKey = RXCUI 
FROM rxNorm.RXNCONSO 
WHERE [STR] = 'Penicillin G benzathine'

INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
VALUES(
	@AllergenId
	,@PracticeRepositoryEntityKey
	,@RxNormAllergen
	,@ExternalSystemEntityKey
)

--DECLARE @FdbExternalSystemId INT
--SELECT @FdbExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'FdbAllergy'

--DECLARE @FdbAllergen INT
--SELECT @FdbAllergen = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @FdbExternalSystemId
--	AND Name = 'Allergen'

--DECLARE @CompositeAllergyID INT
--SELECT @CompositeAllergyID = CompositeAllergyID FROM fdb.tblCompositeAllergy WHERE [Description] = 'Penicillin G'

--INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
--SELECT @FdbExternalSystemId, @AllergenId, @FdbAllergen, @CompositeAllergyID