﻿DELETE FROM model.PatientPhoneNumbers WHERE PatientPhoneNumberTypeId NOT IN (2, 3, 7, 12, 13, 14, 15, 16, 17)

ALTER TABLE model.PatientPhoneNumbers
ADD CONSTRAINT CK_PatientPhoneNumberPatientPhoneNumberType CHECK(PatientPhoneNumberTypeId IN (2, 3, 7, 12, 13, 14, 15, 16, 17))