﻿--Task #8419.Adding 253, 125, B10 and 105 AdustmentCodes

-- Insert for ClaimAdjustmentReasonCode 253 if it doesn't exist
IF NOT EXISTS(SELECT * FROM PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '253')
BEGIN

	INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, [Signature], TestOrder, FollowUp, [Rank], WebsiteUrl)
	  VALUES('PAYMENTREASON', 
			 '253', 
			  'Contractual Adjustment',
			  'F', 
			  '', 
			  '', 
			  '', 
			  'F', 
			  '', 
			  '',
			  1, 
			  NULL)
END

GO

-- Insert for ClaimAdjustmentReasonCode 125 if it doesn't exist
IF NOT EXISTS(SELECT * FROM PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '125')
BEGIN

	INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, [Signature], TestOrder, FollowUp, [Rank], WebsiteUrl)
	  VALUES('PAYMENTREASON', 
			 '125', 
			  'Contractual Adjustment',
			  'F', 
			  '', 
			  '', 
			  '', 
			  'F', 
			  '', 
			  '',
			  1, 
			  NULL)
END

GO

-- Insert for ClaimAdjustmentReasonCode B10 if it doesn't exist
IF NOT EXISTS(SELECT * FROM PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = 'B10')
BEGIN

	INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, [Signature], TestOrder, FollowUp, [Rank], WebsiteUrl)
	  VALUES('PAYMENTREASON', 
			 'B10', 
			  'Contractual Adjustment',
			  'F', 
			  '', 
			  '', 
			  '', 
			  'F', 
			  '', 
			  '',
			  1, 
			  NULL)
END

GO

-- Insert for ClaimAdjustmentReasonCode 105 if it doesn't exist
IF NOT EXISTS(SELECT * FROM PracticeCodeTable pct WHERE pct.ReferenceType = 'PAYMENTREASON' AND pct.Code = '105')
BEGIN

	INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, [Signature], TestOrder, FollowUp, [Rank], WebsiteUrl)
	  VALUES('PAYMENTREASON', 
			 '105', 
			  'Contractual Adjustment',
			  'F', 
			  '', 
			  '', 
			  '', 
			  'F', 
			  '', 
			  '',
			  1, 
			  NULL)
END

GO