﻿IF EXISTS (SELECT * FROM model.SmokingConditions)
BEGIN
	DELETE FROM model.SmokingConditions
END

SET IDENTITY_INSERT model.SmokingConditions ON 

INSERT INTO model.SmokingConditions
(Id, Name, OrdinalId, IsDeactivated)

SELECT 1 AS Id, 'Former' AS Name, 1 AS OrdinalId, CONVERT(bit,0) AS IsDeactivated
UNION ALL
SELECT 2, 'Current Everyday', 2, CONVERT(bit,0)
UNION ALL
SELECT 3, 'Current Some Day', 3, CONVERT(bit,0)
UNION ALL
SELECT 4, 'Heavy Smoke', 4, CONVERT(bit,0)
UNION ALL
SELECT 5, 'Light Smoke', 5, CONVERT(bit,0)
UNION ALL
SELECT 6, 'Current Status Unknown', 6, CONVERT(bit,0)
UNION ALL
SELECT 7, 'Unknown If Ever Smoked', 7, CONVERT(bit,0)
UNION ALL
SELECT 8, 'Never', 8, CONVERT(bit,0)

SET IDENTITY_INSERT model.SmokingConditions OFF

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'SmokingCondition'), (SELECT Id FROM model.SmokingConditions WHERE Name = 'Former'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'SmokingCondition'), 8517006

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'SmokingCondition'), (SELECT Id FROM model.SmokingConditions WHERE Name = 'Current Everyday'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'SmokingCondition'), 449868002

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'SmokingCondition'), (SELECT Id FROM model.SmokingConditions WHERE Name = 'Current Some Day'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'SmokingCondition'), 428041000124106

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'SmokingCondition'), (SELECT Id FROM model.SmokingConditions WHERE Name = 'Heavy Smoke'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'SmokingCondition'), 428071000124103

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'SmokingCondition'), (SELECT Id FROM model.SmokingConditions WHERE Name = 'Light Smoke'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'SmokingCondition'), 428061000124105

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'SmokingCondition'), (SELECT Id FROM model.SmokingConditions WHERE Name = 'Current Status Unknown'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'SmokingCondition'), 77176002

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'SmokingCondition'), (SELECT Id FROM model.SmokingConditions WHERE Name = 'Unknown If Ever Smoked'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'SmokingCondition'), 266927001

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'SmokingCondition'), (SELECT Id FROM model.SmokingConditions WHERE Name = 'Never'), (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'SmokingCondition'), 266919005

