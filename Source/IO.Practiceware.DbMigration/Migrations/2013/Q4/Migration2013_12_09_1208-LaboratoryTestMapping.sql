﻿-- maps LaboratoryTest to Loinc
DECLARE @LoincExternalSystemId INT
SELECT @LoincExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'Loinc'

DECLARE @PracticeRepositoryEntityId INT
SELECT @PracticeRepositoryEntityId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'LaboratoryTest'

DECLARE @LoincLaboratoryTest INT
SELECT @LoincLaboratoryTest = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @LoincExternalSystemId AND Name = 'LaboratoryTest' 

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @PracticeRepositoryEntityId, 498, @LoincLaboratoryTest, '55454-3' UNION ALL --Hemoglobin A1C
SELECT @PracticeRepositoryEntityId, 530, @LoincLaboratoryTest, '9618-0' UNION ALL --Cholesterol
SELECT @PracticeRepositoryEntityId, 545, @LoincLaboratoryTest, '53581-5' UNION ALL --Angiotensin Converting Enzyme
SELECT @PracticeRepositoryEntityId, 559, @LoincLaboratoryTest, '55878-3' UNION ALL--Homocysteine
SELECT @PracticeRepositoryEntityId, 6764, @LoincLaboratoryTest, '30313-1' UNION ALL--HGB
SELECT @PracticeRepositoryEntityId, 6765, @LoincLaboratoryTest, '33765-9' UNION ALL--WBC
SELECT @PracticeRepositoryEntityId, 6766, @LoincLaboratoryTest, '26515-7' UNION ALL--PLT
SELECT @PracticeRepositoryEntityId, 6781, @LoincLaboratoryTest, '20570-8' --HCT

--SELECT 
--'@PracticeRepositoryEntityId' AS PracticeRepositoryEntityId
--,lt.Id AS PracticeRepositoryEntityKey
--,'@LoincLaboratoryTest' AS ExternalSystemEntityId
--,l.LOINC_NUM AS ExternalSystemEntityKey
--,'SELECT @PracticeRepositoryEntityId, '+ CONVERT(NVARCHAR(MAX),lt.Id) +', @LoincLaboratoryTest, ''' + CONVERT(NVARCHAR(MAX),l.LOINC_NUM) + ''' UNION ALL --'+lt.Name AS String
--FROM model.LaboratoryTests lt
--INNER JOIN (
--	SELECT 
--	MAX(l.LOINC_NUM) AS LOINC_NUM
--	,l.COMPONENT
--	FROM loinc.loinc l
--	GROUP BY l.COMPONENT
--)l ON l.COMPONENT = lt.Name
--WHERE lt.Name <> 'Lupus Anticoagulant'