﻿SET IDENTITY_INSERT model.FamilyRelationships ON 
INSERT INTO model.FamilyRelationships (Id, Name, IsDeactivated, OrdinalId)
SELECT 1, 'Aunt', CONVERT(bit, 0), 1
UNION ALL
SELECT 2, 'Brother', CONVERT(bit, 0), 1
UNION ALL
SELECT 3, 'Child', CONVERT(bit, 0), 1
UNION ALL
SELECT 4, 'Cousin', CONVERT(bit, 0), 1
UNION ALL
SELECT 5, 'Father', CONVERT(bit, 0), 1
UNION ALL
SELECT 6, 'Grandfather', CONVERT(bit, 0), 1
UNION ALL
SELECT 7, 'Grandmother', CONVERT(bit, 0), 1
UNION ALL
SELECT 8, 'Grandparent', CONVERT(bit, 0), 1
UNION ALL
SELECT 9, 'Greataunt', CONVERT(bit, 0), 1
UNION ALL
SELECT 10, 'Greatuncle', CONVERT(bit, 0), 1
UNION ALL
SELECT 11, 'Mother', CONVERT(bit, 0), 1
UNION ALL
SELECT 12, 'Nephew', CONVERT(bit, 0), 1
UNION ALL
SELECT 13, 'Niece', CONVERT(bit, 0), 1
UNION ALL
SELECT 14, 'Other', CONVERT(bit, 0), 1
UNION ALL
SELECT 15, 'Parent', CONVERT(bit, 0), 1
UNION ALL
SELECT 16, 'Sibling', CONVERT(bit, 0), 1
UNION ALL
SELECT 17, 'Sister', CONVERT(bit, 0), 1
UNION ALL
SELECT 18, 'Uncle', CONVERT(bit, 0), 1
UNION ALL
SELECT 19, 'Son', CONVERT(bit, 0), 1
UNION ALL
SELECT 20, 'Daughter', CONVERT(bit, 0), 1


SET IDENTITY_INSERT model.FamilyRelationships OFF