﻿IF EXISTS(SELECT name FROM sysobjects WHERE name ='GetAuditAndLogEntries')
DROP PROCEDURE GetAuditAndLogEntries
GO

-- Function to return a single table representing an ordered list of AuditEntryChanges and LogEntries.
-- @OrderByType (0) = Date
-- @OrderByType (1) = UserId
-- @OrderByType (2) = PatientId
-- @DetailsChanged w/ LogEntries = Message
-- @DetailsChanged w/ AuditEntryChanges = ColumnName
-- @LogActionTypes w/ LogEntries = LogCategoryNames (concatonated)
-- @LogActionTypes w/ AuditEntryChanges = ChangeTypeId (AuditEntryChangeType) (string int)
CREATE PROCEDURE GetAuditAndLogEntries(@StartDate datetime, 
									   @EndDate datetime, 
									   @UserIds nvarchar(max),
									   @PatientId nvarchar(max), 
									   @DetailsChanged nvarchar(max), 
									   @LogActionTypes nvarchar(max),
									   @AuditActionTypes nvarchar(max), 
									   @PageSize int, 
									   @PageIndex int,
									   @OrderByTypeId int,
									   @AdditionalFilters nvarchar(max),
									   @IncludeTotalCount bit = 0,
									   @TotalCount bigint = NULL OUTPUT)
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- Using ROWCOUNT for performance considerations See http://www.4guysfromrolla.com/webtech/042606-1.shtml
	DECLARE @maxRow bigint
	SET @maxRow = ((@PageSize * @PageIndex) + @PageSize) - 1 	
	SET ROWCOUNT @maxRow
	
	CREATE TABLE #Results(RowNumber int IDENTITY, Id uniqueidentifier, IsLogEntry bit, DateTime datetime, UserId nvarchar(max), AccessLocation nvarchar(max), AccessDevice nvarchar(max), SourceOfAccess nvarchar(max), ActionMessage nvarchar(max), PatientId bigint, DetailChanged nvarchar(max), NewValue nvarchar(max), OldValue nvarchar(max), ContentTypeId int)
	
	DECLARE @parameters nvarchar(max) 
	SET @parameters = '@StartDate datetime, 
						@EndDate datetime, 
						@UserIds nvarchar(max), 
						@PatientId nvarchar(max),
						@DetailsChanged nvarchar(max), 
						@LogActionTypes nvarchar(max), 
						@AuditActionTypes nvarchar(max), 
						@PageSize int, 
						@PageIndex int,
						@OrderByTypeId int'

	DECLARE @sql nvarchar(max);
	DECLARE @LogQuery AS NVARCHAR(MAX);
	DECLARE @AuditQuery AS VARCHAR(MAX);
	DECLARE @query as VARCHAR(MAX);

	SET @sql =		
	'
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- Using ROWCOUNT for performance considerations See http://www.4guysfromrolla.com/webtech/042606-1.shtml
	DECLARE @maxRow bigint
	SET @maxRow = ((@PageSize * @PageIndex) + @PageSize) - 1 	
	SET ROWCOUNT @maxRow
	
	INSERT #Results(Id, IsLogEntry, DateTime, UserId, AccessLocation, AccessDevice, SourceOfAccess, ActionMessage, PatientId, DetailChanged, NewValue, OldValue, ContentTypeId)'
	
	
	SET @LogQuery= ' SELECT * FROM 
					(SELECT le.Id AS Id, CAST(1 AS bit) AS IsLogEntry, le.[DateTime] AS [DateTime], CAST(CONVERT(int, RIGHT(le.UserId, 12)) AS nvarchar(max)) AS UserId, le.ServerName AS AccessLocation, le.MachineName AS AccessDevice, le.ProcessName AS SourceOfAccess, 
					NULL AS ActionMessage, -- Query for action types (Concatonated LogCategoryNames) separately
					(SELECT TOP 1 CASE WHEN lep.Name=''PatientId'' THEN CAST(lep.Value AS bigint) 
					WHEN lep.Name=''AppointmentId'' THEN (SELECT Top 1 PatientId FROM dbo.Appointments WHERE AppointmentId=CAST(lep.Value As bigint)) END
					FROM dbo.LogEntryProperties lep WHERE lep.LogEntryId = le.Id AND (lep.Name = ''PatientId'' OR lep.Name=''AppointmentId''))  AS PatientId,
					le.[Message] AS DetailChanged,
					NULL AS NewValue,
					NULL AS OldValue,
					7 AS ContentTypeId -- ContentType.Text
					FROM dbo.LogEntries le) AS q
					WHERE (@StartDate IS NULL OR q.[DateTime] >= @StartDate)
					AND (@EndDate IS NULL OR q.[DateTime] <= @EndDate)
					AND (@UserIds IS NULL OR q.UserId IN (SELECT CAST(VALUE AS nvarchar(max)) FROM dbo.GetIntTableFromXmlIds(@UserIds)))
					AND (@PatientId IS NULL OR q.PatientId = @PatientId)
					AND (@DetailsChanged IS NULL OR q.DetailChanged IN (SELECT Value FROM dbo.GetCharTableFromXmlValues(@DetailsChanged)))
					AND (@LogActionTypes IS NULL OR EXISTS(SELECT lc.Name FROM dbo.LogCategories lc
														JOIN dbo.LogEntryLogCategory lelc ON lc.Id = lelc.Categories_Id
														WHERE lelc.LogEntryLogCategory_LogCategory_Id = q.Id AND lc.Name IN (SELECT Value FROM dbo.GetCharTableFromXmlValues(@LogActionTypes)))) '
		
		
	SET @AuditQuery=' SELECT ae.Id AS Id, CAST(0 AS bit) AS IsLogEntry, ae.[AuditDateTime] AS [DateTime], CAST(ae.UserId AS nvarchar(max)) AS UserId, ae.ServerName AS AccessLocation, ae.HostName AS AccessDevice, ae.ObjectName AS SourceOfAccess, 				
						CONVERT(varchar(1), ae.ChangeTypeId) AS ActionMessage,
						ae.PatientId AS PatientId,
						aec.ColumnName AS DetailChanged, aec.NewValue AS NewValue, aec.OldValue AS OldValue, NULL AS ContentTypeId
					FROM dbo.AuditEntries ae JOIN dbo.AuditEntryChanges aec ON ae.Id = aec.AuditEntryId
					WHERE (@StartDate IS NULL OR ae.[AuditDateTime] >= @StartDate)
					AND (@EndDate IS NULL OR ae.[AuditDateTime] <= @EndDate)
					AND (@UserIds IS NULL OR CAST(ae.UserId AS nvarchar(max)) IN (SELECT CAST(Value AS nvarchar(max)) FROM dbo.GetIntTableFromXmlIds(@UserIds)))
					AND (@PatientId IS NULL OR CAST(ae.PatientId AS nvarchar(max)) = @PatientId)
					AND (@DetailsChanged IS NULL OR aec.ColumnName IN (SELECT Value FROM dbo.GetCharTableFromXmlValues(@DetailsChanged)))
					AND (@AuditActionTypes IS NULL OR ae.ChangeTypeId IN (SELECT Value FROM dbo.GetIntTableFromXmlIds(@AuditActionTypes))) '


	IF(@AuditActionTypes IS NULL AND @LogActionTypes IS NOT NULL)
	BEGIN
		SET @query ='SELECT * FROM (' +@LogQuery+ ')results {@AddtionalFilters} ORDER BY {OrderBy}'
	END
	ELSE IF(@AuditActionTypes IS NOT NULL AND @LogActionTypes IS NULL)
	BEGIN
		SET @query = 'SELECT * FROM (' +@AuditQuery+ ')results {@AddtionalFilters} ORDER BY {OrderBy}'
	END
	ELSE
	BEGIN
		SET @query = 'SELECT * FROM ((' +@LogQuery + ') UNION ALL (' +@AuditQuery+ '))results {@AddtionalFilters} ORDER BY {OrderBy}'
	END

	IF COALESCE(@AdditionalFilters, '') <> ''
		SET @query = REPLACE(@query, '{@AddtionalFilters}', ' WHERE ' + LEFT(@AdditionalFilters,LEN(@AdditionalFilters)-CHARINDEX('AND',REVERSE(@AdditionalFilters))))
	ELSE
		SET @query = REPLACE(@query,'{@AddtionalFilters}','')

	SET @sql = @sql + REPLACE(@query, '{OrderBy}',
		CASE @OrderByTypeId WHEN 0 THEN '[DateTime]' WHEN 1 THEN 'COALESCE([UserId],2147483647)' WHEN 2 THEN 'COALESCE([PatientId],2147483647)' END
	) 
	
	EXECUTE sp_executesql @sql, @parameters, @StartDate = @StartDate, @EndDate = @EndDate, @UserIds = @UserIds, @PatientId = @PatientId, @DetailsChanged = @DetailsChanged, @LogActionTypes = @LogActionTypes, @AuditActionTypes = @AuditActionTypes, @PageSize = @PageSize, @PageIndex = @PageIndex, @OrderByTypeId = @OrderByTypeId
	
	SET ROWCOUNT @PageSize

	SELECT Id, IsLogEntry, DateTime, 
	UserId, AccessLocation, AccessDevice, SourceOfAccess, 
	CASE WHEN IsLogEntry = 1 THEN
		STUFF(
			(SELECT
				', ' + lc.Name
				FROM dbo.LogCategories lc
				JOIN dbo.LogEntryLogCategory lelc ON lc.Id = lelc.Categories_Id
				WHERE lelc.LogEntryLogCategory_LogCategory_Id = r.Id
				FOR XML PATH(''), TYPE
			).value('.','varchar(max)')
			,1,2, ''
		) 
		ELSE CASE ActionMessage WHEN '0' THEN 'Insert' WHEN '1' THEN 'Update' WHEN '2' THEN 'Delete' END
		END
		AS ActionMessage,
	PatientId, DetailChanged, NewValue, OldValue, ContentTypeId
	FROM #Results r
	WHERE RowNumber >= @PageIndex * @PageSize
	SET ROWCOUNT 0
	
	IF @IncludeTotalCount = 1
	BEGIN
		DECLARE @countParameters nvarchar(max) 
		SET @countParameters = @parameters + ', @TotalCount bigint OUTPUT'

		DECLARE @countQuery nvarchar(max)
		SET @countQuery = 'SELECT @TotalCount = COUNT(*) FROM (' + REPLACE(@query, 'ORDER BY {OrderBy}', '') + ') as countQuery'
		
		EXECUTE sp_executesql @countQuery, @countParameters, @StartDate = @StartDate, @EndDate = @EndDate, @UserIds = @UserIds, @PatientId = @PatientId, @DetailsChanged = @DetailsChanged, @LogActionTypes = @LogActionTypes, @AuditActionTypes = @AuditActionTypes, @PageSize = @PageSize, @PageIndex = @PageIndex, @OrderByTypeId = @OrderByTypeId, @TotalCount = @TotalCount OUTPUT
	END
END
GO
