﻿-- Recreate procedure
IF OBJECT_ID ( '[model].[CheckPatientHasPendingAlert]', 'P' ) IS NOT NULL
BEGIN
	DROP PROCEDURE [model].[CheckPatientHasPendingAlert]
END
GO

CREATE PROCEDURE [model].[CheckPatientHasPendingAlert]
(
	@PatientId INT, @AlertType INT
)
AS
BEGIN
	-- By default no pending alert
	DECLARE @ResultVar BIT
	SET @ResultVar = 0

	SELECT @ResultVar = 1
	FROM [dbo].[PatientNotes]
	WHERE PatientId = @PatientId 
		AND SUBSTRING(NoteAlertType, @AlertType, 1) = '1' -- Check corresponding bit of alert type

	SELECT @ResultVar
END

GO



