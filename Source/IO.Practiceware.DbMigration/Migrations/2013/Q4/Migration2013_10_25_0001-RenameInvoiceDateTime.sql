﻿IF OBJECT_ID(N'dbo.GetLegacyInvoiceReceivableId', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GetLegacyInvoiceReceivableId
GO
IF OBJECT_ID(N'dbo.GetPrimaryInsuranceId', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GetPrimaryInsuranceId
GO
IF OBJECT_ID(N'dbo.GetAdjustmentIdAndConvertAsReceivableId', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GetAdjustmentIdAndConvertAsReceivableId
GO 

IF OBJECT_ID(N'dbo.PatientReceivables', N'V') IS NOT NULL
	DROP VIEW dbo.PatientReceivables
GO
IF EXISTS (SELECT * FROM sys.triggers WHERE Name = 'OnPatientReceivableInsert') 
	DROP TRIGGER [dbo].[OnPatientReceivableInsert];
GO
IF EXISTS (SELECT * FROM sys.triggers WHERE Name = 'OnPatientReceivableUpdate') 
	DROP TRIGGER [dbo].[OnPatientReceivableUpdate];
GO
IF OBJECT_ID ( 'dbo.GetUnLinkedPayments', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.GetUnLinkedPayments;
GO
IF OBJECT_ID ('dbo.usp_PaymentToDeleted', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.usp_PaymentToDeleted;
GO
IF OBJECT_ID('dbo.GetReceivableIdInsurerIdInsurerDesignationByPatientIdInvoiceDateServiceModifier', 'P') IS NOT NULL
	DROP PROCEDURE dbo.GetReceivableIdInsurerIdInsurerDesignationByPatientIdInvoiceDateServiceModifier;
GO
--

IF EXISTS(
SELECT * FROM sys.columns sc
INNER JOIN sys.tables st ON sc.object_id = st.object_id
WHERE st.name = 'Invoices' AND sc.name = 'DateTime')
BEGIN 
	EXEC sp_RENAME 'model.Invoices.[DateTime]', 'LegacyDateTime', 'COLUMN';	
END
GO

IF NOT EXISTS(
SELECT * FROM sys.columns sc
INNER JOIN sys.tables st ON sc.object_id = st.object_id
WHERE st.name = 'Invoices' AND sc.name = 'LegacyDateTime')
BEGIN 
	ALTER TABLE model.Invoices ADD LegacyDateTime Datetime
END
GO



IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_Invoices_LegacyDateTime')
	DROP INDEX IX_Invoices_LegacyDateTime ON model.Invoices 
GO

CREATE INDEX IX_Invoices_LegacyDateTime ON model.Invoices (LegacyDateTime)
GO

--
CREATE FUNCTION dbo.GetLegacyInvoiceReceivableId (@InvoiceReceivableId INT)
RETURNS INT
AS
BEGIN
	DECLARE @ReturnInvoiceReceivableId INT
		
	SELECT TOP 1 @ReturnInvoiceReceivableId = irb.Id
	FROM model.InvoiceReceivables ira
	INNER JOIN model.InvoiceReceivables irb ON irb.InvoiceId = ira.InvoiceId
		AND ira.PatientInsuranceId IS NULL
	INNER JOIN model.Invoices i ON i.Id = ira.InvoiceId
	INNER JOIN model.PatientInsurances pi ON pi.Id = irb.PatientInsuranceId
		AND (
			pi.EndDateTime IS NULL
			OR i.LegacyDateTime <= pi.EndDateTime
			)
		AND pi.IsDeleted = 0
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		AND i.LegacyDateTime >= ip.StartDateTime
	WHERE ira.Id = @InvoiceReceivableId	
	ORDER BY pi.OrdinalId ASC
	
	IF (@ReturnInvoiceReceivableId IS NULL)	SET @ReturnInvoiceReceivableId = @InvoiceReceivableId

	RETURN @ReturnInvoiceReceivableId
END
GO

--
CREATE FUNCTION dbo.GetPrimaryInsuranceId (@InvoiceId INT)
RETURNS INT
AS
BEGIN
DECLARE @ReturnPatientInsuranceId INT

SELECT TOP 1
@ReturnPatientInsuranceId = ir.PatientInsuranceId
FROM model.Invoices i
INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
INNER JOIN dbo.Appointments ap ON ap.EncounterId = i.EncounterId
	AND ap.AppointmentInsuranceTypeId = pi.InsuranceTypeId
WHERE i.Id = @InvoiceId
	AND pi.IsDeleted = 0
	AND (pi.EndDateTime IS NULL 
	OR pi.EndDateTime = '' 
	OR pi.EndDateTime >= CONVERT(DATETIME,ap.Appdate))
	AND ip.StartDateTime <= CONVERT(DATETIME,ap.Appdate)
ORDER BY pi.OrdinalId ASC

RETURN @ReturnPatientInsuranceId
END
GO

--
CREATE FUNCTION dbo.GetAdjustmentIdAndConvertAsReceivableId (@AdjustmentId INT)
RETURNS INT
AS
BEGIN
	DECLARE @ReturnAdjustmentId INT
		
	SELECT
	@ReturnAdjustmentId = (1 * 1000000000 + adj.Id)
	FROM model.Adjustments adj
	WHERE adj.Id = @AdjustmentId
		AND adj.InvoiceReceivableId IS NULL
			
	RETURN @ReturnAdjustmentId
END
GO

--
CREATE VIEW [dbo].[PatientReceivables]
WITH VIEW_METADATA
AS
WITH 
adjusts AS (
	SELECT
	SUM(
		CASE 
			WHEN adjT.IsDebit = 1 
				THEN (CASE WHEN adj.Amount > 0 THEN -adj.Amount ELSE adj.Amount END) 
			ELSE adj.Amount 
		END
		) AS Adjust
	,ir.InvoiceId
	FROM model.Adjustments adj 
	INNER JOIN model.AdjustmentTypes adjT ON adjT.Id=adj.AdjustmentTypeId
	JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
	GROUP BY ir.InvoiceId
)
,lastPayDate AS (
	SELECT
	ir.InvoiceId
	,CONVERT(NVARCHAR,MAX(PostedDateTime),112) AS PostedDateTime
	FROM model.Adjustments adj 
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
	WHERE FinancialSourceTypeId = 2 --patientonly
	GROUP BY ir.InvoiceId
)
,bstAmountSent AS (
	SELECT 
	SUM(AmountSent) AS AmountSent
	,ir.InvoiceId
	FROM model.BillingServiceTransactions bst
	JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId
	WHERE BillingServiceTransactionStatusId IN (1,2,3,4,8)
	GROUP BY ir.InvoiceId
)
,bstsExistsForInvoiceId AS (
	SELECT 
	ir.InvoiceId
	,COUNT(*) AS NumOccurances
	FROM model.BillingServiceTransactions bst
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId
	GROUP BY ir.InvoiceId
)
,attributeToServiceLocations AS (
	SELECT
	Id
	,CASE
		WHEN ShortName = 'Office' AND IsExternal = 0
			THEN 0
		WHEN ShortName LIKE 'Office-%' AND IsExternal = 0
			THEN Id+1000
		WHEN IsExternal = 1
			THEN Id-110000000
		ELSE Id
	END AS PrBillingOfficeId
	FROM (
		SELECT 
		pn.PracticeId AS Id
		,CONVERT(BIT, 0) AS IsExternal
		,CASE LocationReference
			WHEN ''
				THEN 'Office'
			ELSE 'Office-' + LocationReference
		END AS ShortName
		FROM dbo.PracticeName pn
		WHERE pn.PracticeType = 'P'

		UNION ALL

		SELECT re.ResourceId+110000000 AS Id
		,CONVERT(BIT, 1) AS IsExternal
		,re.ResourceName AS ShortName
		FROM dbo.Resources re
		LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE'
			AND SUBSTRING(pct.Code, 1, 2) = re.PlaceOfService
		WHERE re.ResourceType = 'R'
			AND re.ServiceCode = '02'
	)v
)
SELECT 
ir.Id AS ReceivableId
,CASE
	WHEN charges.Charge IS NOT NULL
		THEN CASE
				WHEN adjusts.Adjust IS NOT NULL
					THEN CONVERT(REAL, charges.Charge-adjusts.Adjust)
				WHEN adjusts.Adjust IS NULL
					THEN CONVERT(REAL, charges.Charge)
			END
	ELSE 0
END AS OpenBalance
,CASE
	WHEN ir.OpenForReview = 0
		THEN 
			CASE
				WHEN bsti.NumOccurances IS NOT NULL
					THEN 'B'
				ELSE 'S'
			END
	WHEN ir.OpenForReview = 1
		THEN 'I'
END AS ReceivableType
,i.LegacyPatientReceivablesPatientId AS PatientId
,ip.PolicyHolderPatientId AS InsuredId
,ip.InsurerId
,i.LegacyPatientReceivablesAppointmentId AS AppointmentId
,i.LegacyPatientReceivablesInvoice AS Invoice
,CONVERT(NVARCHAR,i.LegacyDateTime,112) AS InvoiceDate
,COALESCE(CONVERT(REAL,CONVERT(NUMERIC(36,2),(charges.Charge))),0) AS Charge
,COALESCE(prov.UserId, (apASC.ResourceId2 -1000)) AS BillToDr
,CASE sup.RelatedCause1Id
	WHEN 1
		THEN 'A'
	WHEN 2
		THEN 'E'
	WHEN 3
		THEN 'O'
	ELSE ''
END AS AccType
,CASE
	WHEN sup.AccidentStateOrProvinceId IS NOT NULL
		THEN st.Abbreviation
	ELSE ''
END AS AccState
,CASE 
	WHEN AdmissionDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,AdmissionDateTime,101)
	ELSE ''
END AS HspAdmDate
,CASE 
	WHEN DischargeDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,DischargeDateTime,101)
	ELSE ''
END AS HspDisDate
,'' AS Disability
,'' AS RsnType
,CASE 
	WHEN DisabilityDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,DisabilityDateTime,101)
	ELSE ''
END AS RsnDate
,CASE
	WHEN AccidentDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,AccidentDateTime,101)
	ELSE ''
END AS FirstConsDate
,'' AS PrevCond
,'A' AS Status
,CASE 
	WHEN ((i.ReferringExternalProviderId IS NULL) OR (i.ReferringExternalProviderId = ''))
		THEN 0
	ELSE i.ReferringExternalProviderId
END AS ReferDr
,CASE
	WHEN charges.Charge IS NOT NULL
		THEN CASE
				WHEN adjusts.Adjust IS NOT NULL
					THEN CASE
							WHEN charges.Charge - adjusts.Adjust <> 0--WHEN charges.Charge - adjusts.Adjust > 0
								THEN CASE
										WHEN bstAmountSent.AmountSent IS NOT NULL
											THEN CONVERT(REAL,((charges.Charge - adjusts.Adjust)-bstAmountSent.AmountSent))
										WHEN bstAmountSent.AmountSent IS NULL
											THEN CONVERT(REAL,charges.Charge - adjusts.Adjust)
										END
							WHEN charges.Charge - adjusts.Adjust = 0--WHEN charges.Charge - adjusts.Adjust <= 0
								THEN 0
							END
				WHEN adjusts.Adjust IS NULL
					THEN CASE 
							WHEN bstAmountSent.AmountSent IS NOT NULL
								THEN CONVERT(REAL,charges.Charge-bstAmountSent.AmountSent)
							WHEN bstAmountSent.AmountSent IS NULL
								THEN CONVERT(REAL,charges.Charge)
							END
				END			
	ELSE 0
END AS UnallocatedBalance
,CASE 
	WHEN (ir.PayerClaimControlNumber IS NULL)
		THEN ''
	ELSE ir.PayerClaimControlNumber
END AS ExternalRefInfo
,CASE sup.ClaimDelayReasonId
	WHEN 1
		THEN '1'
	WHEN 2
		THEN '2'
	WHEN 3
		THEN '3'
	WHEN 4
		THEN '4'
	WHEN 5
		THEN '5'
	WHEN 6
		THEN '6'
	WHEN 7
		THEN '7'
	WHEN 8
		THEN '8'
	WHEN 9
		THEN '9'
	WHEN 10
		THEN '10'
	WHEN 11
		THEN '11'
	WHEN 12
		THEN '15'
	ELSE ''
END AS Over90
,asl.PrBillingOfficeId AS BillingOffice
,CASE
	WHEN i.LegacyPatientBilledDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,i.LegacyPatientBilledDateTime,112)
	ELSE ''
END AS PatientBillDate
,CASE
	WHEN lastPayDate.PostedDateTime IS NULL
		THEN ''
	ELSE lastPayDate.PostedDateTime 
END AS LastPayDate
,CASE 
	WHEN charges.ChargesByFee IS NOT NULL
		THEN CONVERT(REAL, charges.ChargesByFee) 
	ELSE '0.00'
END AS ChargesByFee
,'' AS WriteOff
,COALESCE(ir.LegacyInsurerDesignation,'F') AS InsurerDesignation
FROM model.InvoiceReceivables ir 
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.Providers prov ON prov.Id = i.BillingProviderId
INNER JOIN attributeToServiceLocations asl ON asl.Id = i.AttributeToServiceLocationId
LEFT JOIN dbo.Appointments apASC ON apASC.EncounterId = i.EncounterId 
	AND apASC.AppointmentId <> apASC.EncounterId
	AND i.InvoiceTypeId = 2
LEFT JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
LEFT JOIN dbo.Charges WITH(NOEXPAND) ON charges.InvoiceId = ir.InvoiceId
LEFT JOIN adjusts ON adjusts.InvoiceId = ir.InvoiceId
LEFT JOIN lastPayDate ON lastPayDate.InvoiceId = ir.InvoiceId
LEFT JOIN bstAmountSent ON bstAmountSent.InvoiceId = i.Id
LEFT JOIN model.InvoiceSupplementals sup ON i.Id = sup.Id
LEFT JOIN model.StateOrProvinces st ON st.Id = sup.AccidentStateOrProvinceId
LEFT JOIN bstsExistsForInvoiceId bsti ON bsti.InvoiceId = ir.InvoiceId
WHERE ir.PatientInsuranceId IS NOT NULL

UNION ALL

SELECT
ir.Id AS ReceivableId
,CASE
	WHEN charges.Charge IS NOT NULL
		THEN CASE
				WHEN adjusts.Adjust IS NOT NULL
					THEN CONVERT(REAL,charges.Charge-adjusts.Adjust)
				WHEN adjusts.Adjust IS NULL
					THEN CONVERT(REAL,charges.Charge)
			END
	ELSE 0
END AS OpenBalance
,CASE
	WHEN ir.OpenForReview = 0
		THEN 
			CASE
				WHEN bsti.NumOccurances IS NOT NULL
					THEN 'B'
				ELSE 'S'
			END
	WHEN ir.OpenForReview = 1
		THEN 'I'
END AS ReceivableType
,i.LegacyPatientReceivablesPatientId AS PatientId
,i.LegacyPatientReceivablesPatientId AS InsuredId
,0 AS InsurerId
,i.LegacyPatientReceivablesAppointmentId AS AppointmentId
,i.LegacyPatientReceivablesInvoice AS Invoice
,CONVERT(NVARCHAR,i.LegacyDateTime,112) AS InvoiceDate
,COALESCE(CONVERT(REAL,CONVERT(NUMERIC(36,2),(charges.Charge))),0) AS Charge
,COALESCE(prov.UserId, (apASC.ResourceId2 -1000)) AS BillToDr
,CASE sup.RelatedCause1Id
	WHEN 1
		THEN 'A'
	WHEN 2
		THEN 'E'
	WHEN 3
		THEN 'O'
	ELSE ''
END AS AccType
,CASE
	WHEN sup.AccidentStateOrProvinceId IS NOT NULL
		THEN st.Abbreviation
	ELSE ''
END AS AccState
,CASE 
	WHEN AdmissionDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,AdmissionDateTime,101)
	ELSE ''
END AS HspAdmDate
,CASE 
	WHEN DischargeDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,DischargeDateTime,101)
	ELSE ''
END AS HspDisDate
,'' AS Disability
,'' AS RsnType
,CASE 
	WHEN DisabilityDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,DisabilityDateTime,101)
	ELSE ''
END AS RsnDate
,CASE
	WHEN AccidentDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,AccidentDateTime,101)
	ELSE ''
END AS FirstConsDate
,'' AS PrevCond
,'A' AS Status
,CASE 
	WHEN ((i.ReferringExternalProviderId IS NULL) OR (i.ReferringExternalProviderId = ''))
		THEN 0
	ELSE i.ReferringExternalProviderId
END AS ReferDr
,CASE
	WHEN (charges.Charge IS NOT NULL)
		THEN CASE
				WHEN adjusts.Adjust IS NOT NULL
					THEN CASE
							WHEN charges.Charge - adjusts.Adjust <> 0
								THEN CASE
										WHEN bstAmountSent.AmountSent IS NOT NULL
											THEN ((charges.Charge - adjusts.Adjust)-bstAmountSent.AmountSent)
										WHEN bstAmountSent.AmountSent IS NULL
											THEN charges.Charge - adjusts.Adjust
										END
							WHEN charges.Charge - adjusts.Adjust = 0
								THEN 0
							END
				WHEN adjusts.Adjust IS NULL
					THEN CASE 
							WHEN bstAmountSent.AmountSent IS NOT NULL
								THEN CONVERT(REAL,charges.Charge-bstAmountSent.AmountSent)
							WHEN bstAmountSent.AmountSent IS NULL
								THEN CONVERT(REAL,charges.Charge)
							END
				END			
	ELSE 0
END AS UnallocatedBalance
,CASE 
	WHEN (ir.PayerClaimControlNumber IS NULL)
		THEN ''
	ELSE ir.PayerClaimControlNumber
END AS ExternalRefInfo
,CASE sup.ClaimDelayReasonId
	WHEN 1
		THEN '1'
	WHEN 2
		THEN '2'
	WHEN 3
		THEN '3'
	WHEN 4
		THEN '4'
	WHEN 5
		THEN '5'
	WHEN 6
		THEN '6'
	WHEN 7
		THEN '7'
	WHEN 8
		THEN '8'
	WHEN 9
		THEN '9'
	WHEN 10
		THEN '10'
	WHEN 11
		THEN '11'
	WHEN 12
		THEN '15'
	ELSE ''
END AS Over90
,asl.PrBillingOfficeId AS BillingOffice
,CASE
	WHEN i.LegacyPatientBilledDateTime IS NOT NULL
		THEN CONVERT(NVARCHAR,i.LegacyPatientBilledDateTime,112)
	ELSE ''
END AS PatientBillDate
,CASE
	WHEN lastPayDate.PostedDateTime IS NULL
		THEN ''
	ELSE lastPayDate.PostedDateTime 
END AS LastPayDate
,CASE 
	WHEN charges.ChargesByFee IS NOT NULL
		THEN CONVERT(REAL, charges.ChargesByFee) 
	ELSE '0.00'
END AS ChargesByFee
,'' AS WriteOff
,'T' AS InsurerDesignation
FROM model.InvoiceReceivables ir 
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
LEFT JOIN dbo.Appointments apASC ON apASC.EncounterId = i.EncounterId AND apASC.AppointmentId <> apASC.EncounterId
	AND i.InvoiceTypeId = 2
INNER JOIN attributeToServiceLocations asl ON asl.Id = i.AttributeToServiceLocationId
LEFT JOIN dbo.Charges WITH(NOEXPAND) ON charges.InvoiceId = ir.InvoiceId
LEFT JOIN adjusts ON adjusts.InvoiceId = ir.InvoiceId
LEFT JOIN lastPayDate ON lastPayDate.InvoiceId = ir.InvoiceId
LEFT JOIN bstAmountSent ON bstAmountSent.InvoiceId = i.Id
LEFT JOIN model.InvoiceSupplementals sup ON i.Id = sup.Id
LEFT JOIN model.StateOrProvinces st ON st.Id = sup.AccidentStateOrProvinceId
INNER JOIN model.Providers prov ON prov.Id = i.BillingProviderId
LEFT JOIN bstsExistsForInvoiceId bsti ON bsti.InvoiceId = ir.InvoiceId
WHERE ir.Id IN (
	SELECT Id FROM model.InvoiceReceivables
	WHERE InvoiceId IN (
		SELECT 
		InvoiceId
		FROM model.InvoiceReceivables
		GROUP BY InvoiceId HAVING COUNT(InvoiceId) = 1
	)
	AND PatientInsuranceId IS NULL
)

UNION ALL

SELECT 
(1 * 1000000000 + MIN(adj.Id)) AS ReceivableId
,CASE 
	WHEN SUM(ABS(adj.Amount) * -1) = -0.01
		THEN 0
	ELSE SUM(ABS(adj.Amount) * -1)
END AS OpenBalance
,'S' AS ReceivableType
,adj.PatientId
,0 AS InsuredId
,99999 AS InsurerId
,0 AS AppointmentId
,'' AS Invoice
,'' AS InvoiceDate
,CONVERT(REAL,0) AS Charge
,0 AS BillToDr
,'' AS AccType
,'' AS AccState
,'' AS HspAdmDate
,'' AS HspDisDate
,CONVERT(NVARCHAR,MIN(adj.Id)) AS Disability
,'' AS RsnType
,'' AS RsnDate
,'' AS FirstConsDate
,'' AS PrevCond
,'A' AS Status
,0 AS ReferDr
,0 AS UnallocatedBalance
,'' AS ExternalRefInfo
,'' AS Over90
,0 AS BillingOffice
,'' AS PatientBillDate
,'' AS LastPayDate
,0 AS ChargesByFee
,'' AS WriteOff
,'F' AS InsurerDesignation
FROM model.Adjustments adj
WHERE adj.InvoiceReceivableId IS NULL
GROUP BY adj.PatientId
GO

--
CREATE TRIGGER OnPatientReceivableInsert ON dbo.PatientReceivables
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @QualifyInsertedInvoiceReceivableAsTypeId INT

	SELECT TOP 1 @QualifyInsertedInvoiceReceivableAsTypeId =
	CASE i.InsurerId
		WHEN 0
			THEN 1 --Patient InvoiceReceivable
		WHEN 99999
			THEN 2 --Credit
		ELSE 3 --Patient + Insurance InvoiceReceivable
	END
	FROM inserted i
	
	DECLARE @ClinicalInvoiceProviderMax INT
	DECLARE @ProviderBillingOrganizationMax INT
	DECLARE @ProviderBillingOrganizationServiceLocationMax INT
	DECLARE @MainLocationId INT
	DECLARE @ServiceLocationCodeId INT
	DECLARE @InvoiceId INT
	DECLARE @ServiceLocationMax INT
	DECLARE @PatientId INT
	DECLARE @NumberOfInsuranceRowsInserted  INT
	DECLARE @InvoiceCreatedOnThisInsertTrigger INT

	IF (@QualifyInsertedInvoiceReceivableAsTypeId = 2)
	BEGIN
		IF NOT EXISTS (
		SELECT i.* FROM inserted i 
		INNER JOIN model.Adjustments adj ON adj.PatientId = i.PatientId 
		AND adj.InvoiceReceivableId IS NULL)
			BEGIN
				INSERT INTO model.Adjustments (FinancialSourceTypeId,Amount,PostedDateTime,AdjustmentTypeId
				  ,BillingServiceId,InvoiceReceivableId,PatientId,InsurerId,FinancialBatchId,PaymentMethodId
				  ,ClaimAdjustmentGroupCodeId,ClaimAdjustmentReasonCodeId,Comment,IncludeCommentOnStatement)
				  SELECT
					CASE
						WHEN i.PatientId IS NOT NULL
							THEN 2
						WHEN i.InsurerId IS NOT NULL
							THEN 1
						ELSE 4
					END AS FinancialSourceTypeId
					,-0.01 AS Amount
					,CONVERT(NVARCHAR,GETDATE(),112) AS PostedDateTime
					,1 AS AdjustmentTypeId
					,NULL AS BillingServiceId
					,NULL AS InvoiceReceivableId
					,i.PatientId 
					,NULL AS InsurerId
					,NULL AS FinancialBatchId
					,NULL AS PaymentMethodId
					,NULL AS ClaimAdjustmentReasonCodeId
					,NULL AS ClaimAdjustmentGroupCodeId
					,NULL AS PaymentRefId
					,0 AS IncludeCommentOnStatement
					FROM inserted i 

					DECLARE @InsertedCreditRowScopeIdentity INT
					IF (@@ROWCOUNT > 0) SET @InsertedCreditRowScopeIdentity = SCOPE_IDENTITY()
			END
		ELSE IF EXISTS (
			SELECT 
			i.* 
			FROM inserted i 
			INNER JOIN model.Adjustments adj ON adj.PatientId = i.PatientId 
			AND adj.InvoiceReceivableId IS NULL)
			BEGIN
				UPDATE adj
				SET adj.Amount = ABS(i.OpenBalance)
				FROM model.Adjustments adj
				INNER JOIN inserted i ON i.Disability = adj.Id
			END
	END
	ELSE
	BEGIN

	SET @InvoiceCreatedOnThisInsertTrigger = 1
	SET @PatientId = (SELECT TOP 1 PatientId FROM inserted)
	SET @ClinicalInvoiceProviderMax = 110000000
	SET @ProviderBillingOrganizationMax = 110000000
	SET @ProviderBillingOrganizationServiceLocationMax = 110000000
	SET @ServiceLocationMax = 110000000
	SET @NumberOfInsuranceRowsInserted = 0
	SET @MainLocationId = (
			SELECT TOP 1 PracticeId AS PracticeId
			FROM dbo.PracticeName
			WHERE PracticeType = 'P' AND LocationReference = ''
			)
	SET @ServiceLocationCodeId = (
			SELECT TOP 1 CASE 
					WHEN (ap.ResourceId2 = 0) OR (ap.ResourceId2 > 1000) AND (ap.Comments <> 'ASC CLAIM')
						THEN (
								SELECT TOP 1 pct.Id
								FROM PracticeName pn
								INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE' 
									AND SUBSTRING(pct.Code, 1, 2) = '11'
								)
					WHEN ap.ResourceId2 < 999
						THEN (
								SELECT TOP 1 pct.Id
								FROM inserted i
								INNER JOIN dbo.Appointments ap ON i.AppointmentId = ap.AppointmentId 
								INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId2
								INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE' 
									AND SUBSTRING(pct.Code, 1, 2) = re.PlaceOfService
								)
					WHEN ap.Comments = 'ASC CLAIM'
						THEN (
								SELECT TOP 1 pct.Id
								FROM inserted i
								INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
								INNER JOIN dbo.Appointments ap ON i.AppointmentId = ap.AppointmentId
								INNER JOIN dbo.Resources reASC ON reASC.PracticeId = ap.ResourceId2 - 1000 
									AND reASC.ResourceType = 'R' 
									AND reASC.ServiceCode = '02' 
									AND reASC.Billable = 'Y' 
									AND pic.FieldValue = 'T'
								LEFT JOIN dbo.PracticeCodeTable pct ON SUBSTRING(pct.Code, 1, 2) = reASC.PlaceOfService 
								AND pct.ReferenceType = 'PLACEOFSERVICE'
								)
					END
			FROM inserted i 
			INNER JOIN Appointments ap ON i.AppointmentId = ap.AppointmentId
			)

	IF OBJECT_ID(N'tempdb..#HoldOrdinals') IS NOT NULL
		DROP TABLE #HoldOrdinals

	CREATE TABLE #HoldOrdinals (PatientInsuranceId INT)

--step 1, does the invoice exsits for the PR you're trying to insert?
--no -- then create it, yes -- then set variable @InvoiceId = to the invoiceid
	IF EXISTS (
			SELECT Id
			FROM model.Invoices inv
			INNER JOIN inserted i ON inv.EncounterId = i.AppointmentId

			UNION ALL 
			
			SELECT inv.Id
			FROM inserted i 
			INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
				AND ap.Comments = 'ASC CLAIM'
			INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
				AND apEncounter.AppTypeId = ap.AppTypeId
				AND apEncounter.AppDate = ap.AppDate
				AND apEncounter.AppTime > 0 
				AND ap.AppTime = 0
				AND apEncounter.ScheduleStatus = ap.ScheduleStatus
				AND ap.ScheduleStatus = 'D'
				AND apEncounter.ResourceId1 = ap.ResourceId1
				AND apEncounter.ResourceId2 = ap.ResourceId2
			INNER JOIN model.Invoices inv on inv.EncounterId = apEncounter.AppointmentId AND inv.InvoiceTypeId = 2
			)
			BEGIN
				SET @InvoiceCreatedOnThisInsertTrigger = 0
				SET @InvoiceId = (
				SELECT Id
				FROM model.Invoices inv
				INNER JOIN inserted i ON inv.EncounterId = i.AppointmentId

				UNION ALL 
			
				SELECT inv.Id
				FROM inserted i 
				INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
					AND ap.Comments = 'ASC CLAIM'
				INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
					AND apEncounter.AppTypeId = ap.AppTypeId
					AND apEncounter.AppDate = ap.AppDate
					AND apEncounter.AppTime > 0 
					AND ap.AppTime = 0
					AND apEncounter.ScheduleStatus = ap.ScheduleStatus
					AND ap.ScheduleStatus = 'D'
					AND apEncounter.ResourceId1 = ap.ResourceId1
					AND apEncounter.ResourceId2 = ap.ResourceId2
				INNER JOIN model.Invoices inv on inv.EncounterId = apEncounter.AppointmentId AND inv.InvoiceTypeId = 2
				)
			END
	ELSE
	BEGIN
	--set this variable to 1, or True so that we know the invoice was inserted on this instance of the insert trigger.
		SET @InvoiceCreatedOnThisInsertTrigger = 1
		--First insert into invoices   
		DECLARE @BillToDr INT

		SET @BillToDr = (
				SELECT TOP 1 BillToDr
				FROM inserted
				)

		DECLARE @PrimaryInsurerId INT

		SET @PrimaryInsurerId = (
				SELECT TOP 1 InsurerId
				FROM inserted
				)

		DECLARE @IsAssignmentRefused BIT

		SET @IsAssignmentRefused = CONVERT(BIT, 0) --assume assignment is always accepted.

		IF EXISTS (
				SELECT d.DoctorId
				FROM model.DoctorInsurerAssignments d
				WHERE d.DoctorId = @BillToDr
					AND d.InsurerId = @PrimaryInsurerId
					AND d.IsAssignmentAccepted = 0 --indicates Assignment is not accepted, therefore @IsAssignmentRefused = 1
				)
			SET @IsAssignmentRefused = CONVERT(BIT, 1)

		INSERT INTO model.Invoices (
			LegacyDateTime
			,AttributeToServiceLocationId
			,EncounterId
			,InvoiceTypeId
			,ClinicalInvoiceProviderId
			,HasPatientAssignedBenefits
			,IsReleaseOfInformationNotSigned
			,IsNoProviderSignatureOnFile
			,BillingProviderId
			,ReferringExternalProviderId
			,ServiceLocationCodeId
			,IsAssignmentRefused
			)
		SELECT CONVERT(DATETIME, i.InvoiceDate, 112) AS [DateTime]
			,CASE 
				WHEN i.BillingOffice = 0
					THEN 1
				WHEN i.BillingOffice BETWEEN 1
						AND 999
					THEN model.GetPairId(1, reAtribTo.ResourceId, @ServiceLocationMax)
				ELSE i.BillingOffice - 1000
				END AS AttributeToServiceLocationId
			,CASE 
				WHEN ap.Comments <> 'ASC CLAIM'
					THEN i.AppointmentId
				ELSE apEncounter.AppointmentId
				END AS EncounterId
			,CASE 
				WHEN re.ResourceType IN (
						'Q'
						,'Y'
						)
					THEN 3
				WHEN ap.Comments = 'ASC CLAIM'
					THEN 2
				ELSE 1
				END AS InvoiceTypeId
			,CASE 
				WHEN ap.Comments <> 'ASC CLAIM'
					THEN model.GetBigPairId(ap.ResourceId1, i.AppointmentId, @ClinicalInvoiceProviderMax)
				ELSE model.GetBigPairId(reASC.ResourceId, apEncounter.AppointmentId, @ClinicalInvoiceProviderMax)
				END AS ClinicalInvoiceProviderId
			,CASE p.PaymentOfBenefitsToProviderId
				WHEN 1
					THEN CONVERT(BIT, 1)
				ELSE CONVERT(BIT, 0)
				END AS HasPatientAssignedBenefits
			,CASE p.ReleaseOfInformationCodeId
				WHEN 1
					THEN CONVERT(BIT, 0)
				ELSE CONVERT(BIT, 1)
				END AS IsReleaseOfInformationNotSigned
			,CONVERT(BIT, 0) AS IsNoProviderSignatureOnFile
			,model.GetBigPairId(CASE --1st
					WHEN ap.ResourceId2 = 0
						THEN @MainLocationId
					WHEN ap.ResourceId2 > 999
						THEN (ap.ResourceId2 - 1000)
					ELSE model.GetPairId(1, ap.ResourceId2, @ServiceLocationMax)
					END, model.GetBigPairId(CASE 
						WHEN ap.Comments <> 'ASC CLAIM'
							THEN re.PracticeId
						WHEN ap.Comments = 'ASC CLAIM'
							THEN (ap.ResourceId2 - 1000)
						END, --2nd number
					CASE 
						WHEN ap.Comments <> 'ASC CLAIM'
							THEN i.BillToDr
						ELSE (
								SELECT TOP 1 ResourceId
								FROM Resources
								WHERE PracticeId = (ap.ResourceId2 - 1000)
									AND ResourceType = 'R'
									AND ServiceCode = '02'
									AND Billable = 'Y'
								)
						END, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax) AS BillingProviderId
			,CASE 
				WHEN i.ReferDr = ''
					THEN NULL
				ELSE i.ReferDr
				END AS ReferringExternalProviderId
			,@ServiceLocationCodeId AS ServiceLocationCodeId
			,@IsAssignmentRefused AS IsAssignmentRefused
		FROM inserted i
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
		INNER JOIN model.Patients p ON p.Id = i.PatientId
		LEFT JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
		LEFT JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
		LEFT JOIN dbo.Resources reAtribTo ON i.BillingOffice = reAtribTo.ResourceId
		LEFT JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
			AND apEncounter.AppTypeId = ap.AppTypeId
			AND apEncounter.AppDate = ap.AppDate
			AND apEncounter.AppTime > 0
			AND ap.AppTime = 0
			AND apEncounter.ScheduleStatus = ap.ScheduleStatus
			AND ap.ScheduleStatus = 'D'
			AND apEncounter.ResourceId1 = ap.ResourceId1
			AND apEncounter.ResourceId2 = ap.ResourceId2
		LEFT JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P'
			AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
			AND PracticeType = 'P'
			AND LocationReference <> ''
		LEFT JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
			AND reASC.ResourceType = 'R'
			AND reASC.ServiceCode = '02'
			AND reASC.Billable = 'Y'
			AND pic.FieldValue = 'T'

		--Set the invoice variable to the new row that was just inserted
		SET @InvoiceId = (
				SELECT IDENT_CURRENT('model.Invoices')
				)
	END
	
	IF (@InvoiceCreatedOnThisInsertTrigger = 1 AND @PrimaryInsurerId <> 0)
	BEGIN
		DECLARE @DetermineInvoiceApptInsType NVARCHAR(1)
		SET @DetermineInvoiceApptInsType = (
		SELECT 
			CASE v.ApptInsType
				WHEN 1
					THEN 'M'
				WHEN 2
					THEN 'V'
				WHEN 3
					THEN 'A'
				WHEN 4
					THEN 'W'
			END AS ApptInsType
		FROM (
			SELECT TOP 1 
			CASE ap.ApptInsType
			WHEN 'M'
				THEN 1
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3
			WHEN 'W'
				THEN 4
			END AS ApptInsType
			FROM dbo.Appointments ap
			WHERE AppointmentId = 
			(SELECT TOP 1 EncounterId FROM model.Invoices WHERE Id = @InvoiceId)
			ORDER BY CASE ApptInsType
			WHEN 'M'
				THEN 1
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3
			WHEN 'W'
				THEN 4
			END ASC
			)v
		)

		DECLARE @DetermineInsertedPatientInsuranceInsType NVARCHAR(1)
		SET @DetermineInsertedPatientInsuranceInsType = (
		SELECT DISTINCT(
		CASE pi.InsuranceTypeId
			WHEN 1
				THEN 'M'
			WHEN 2
				THEN 'V'
			WHEN 3
				THEN 'A'
			WHEN 4
				THEN 'W'
			ELSE NULL
		END) AS InsuranceType
		FROM inserted i
		INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
			AND (pi.EndDateTime IS NULL OR pi.EndDateTime = '' OR pi.EndDateTime >= CONVERT(DATETIME,i.InvoiceDate,112))
			AND pi.IsDeleted = 0
		INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			AND ip.StartDateTime <= CONVERT(DATETIME,i.InvoiceDate,112)
			AND ip.InsurerId = i.InsurerId
			AND ip.PolicyHolderPatientId = i.InsuredId
		)
		
		IF (@DetermineInsertedPatientInsuranceInsType <> @DetermineInvoiceApptInsType)
		BEGIN
			UPDATE ap
			SET ap.ApptInsType = @DetermineInsertedPatientInsuranceInsType
			FROM dbo.Appointments ap
			WHERE ap.AppointmentId = (
				SELECT 
				ap.EncounterId
				FROM inserted i 
				INNER JOIN dbo.Appointments ap 
				ON ap.AppointmentId = i.AppointmentId)
		END
	END

		--Insert InvoiceSupplimentals table if neccessary
		IF EXISTS (
				SELECT *
				FROM inserted i
				WHERE FirstConsDate <> '' OR HspAdmDate <> '' OR Over90 <> '' OR Over90 <> '' OR RsnDate <> '' OR HspDisDate <> '' OR AccType <> ''
				)
				AND NOT EXISTS(SELECT * FROM model.InvoiceSupplementals WHERE Id = @InvoiceId)
		BEGIN
			INSERT INTO model.InvoiceSupplementals (
				Id
				,AccidentDateTime
				,AccidentStateOrProvinceId
				,AdmissionDateTime
				,ClaimDelayReasonId
				,DisabilityDateTime
				,DischargeDateTime
				,RelatedCause1Id
				,VisionPrescriptionDateTime
				)
			SELECT @invoiceId AS Id
				,CASE 
					WHEN FirstConsDate = ''
						THEN NULL
					ELSE CONVERT(DATETIME, FirstConsDate, 101)
					END AS AccidentDateTime
				,CASE st.id
					WHEN 0
						THEN NULL
					ELSE st.id
					END AS AccidentStateOrProvinceId
				,CASE 
					WHEN HspAdmDate = ''
						THEN NULL
					ELSE CONVERT(DATETIME, HspAdmDate, 101)
					END AS AdmissionDateTime
				,CASE Over90
					WHEN 1
						THEN 1
					WHEN 2
						THEN 2
					WHEN 3
						THEN 3
					WHEN 4
						THEN 4
					WHEN 5
						THEN 5
					WHEN 6
						THEN 6
					WHEN 7
						THEN 7
					WHEN 8
						THEN 8
					WHEN 9
						THEN 9
					WHEN 10
						THEN 10
					WHEN 11
						THEN 11
					WHEN 15
						THEN 12
					ELSE NULL
					END AS ClaimDelayReasonId
				,CASE 
					WHEN COALESCE(RsnDate, '') <> ''
						THEN CONVERT(DATETIME, RsnDate, 101)
					ELSE NULL
					END AS DisabilityDateTime
				,CASE 
					WHEN COALESCE(HspDisDate, '') <> ''
						THEN CONVERT(DATETIME, HspDisDate, 101)
					ELSE NULL
					END AS DischargeDateTime
				,CASE i.AccType
					WHEN 'A'
						THEN 1
					WHEN 'E'
						THEN 2
					WHEN 'O'
						THEN 3
					ELSE NULL
					END AS RelatedCause1Id
				,CONVERT(DATETIME, NULL, 101) AS VisionPrescriptionDateTime
			FROM inserted i
			LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = i.AccState
	END

	--now let's insert into the invoice receivables (the actual PR row(s))
	IF NOT EXISTS (
			SELECT *
			FROM model.InvoiceReceivables ir
			INNER JOIN model.Invoices iv ON ir.InvoiceId = @InvoiceId
			WHERE ir.PatientInsuranceId IS NULL 
			)
	BEGIN
		IF EXISTS (SELECT i.InsurerId FROM inserted i WHERE i.InsurerId = 0 AND i.ReceivableType = 'I')
		BEGIN
			INSERT INTO model.InvoiceReceivables (
			InvoiceId
			,PatientInsuranceId
			,PayerClaimControlNumber
			,PatientInsuranceAuthorizationId
			,PatientInsuranceReferralId
			,OpenForReview
			)
		SELECT @InvoiceId AS InvoiceId
			,NULL AS PatientInsuranceId
			,NULL AS PayerClaimControlNumber
			,NULL AS PatientInsuranceAuthorizationId				
			,NULL AS PatientInsuranceReferralId
			,CONVERT(BIT, 1) AS OpenForReview
		FROM inserted i
		INNER JOIN Appointments ap ON ap.AppointmentId = i.AppointmentId
		END

		ELSE BEGIN
		--Insert into InvoiceReceivables, the patient row
		INSERT INTO model.InvoiceReceivables (
			InvoiceId
			,PatientInsuranceId
			,PayerClaimControlNumber
			,PatientInsuranceAuthorizationId
			,PatientInsuranceReferralId
			,OpenForReview
			)
		SELECT @InvoiceId AS InvoiceId
			,NULL AS PatientInsuranceId
			,NULL AS PayerClaimControlNumber
			,NULL AS PatientInsuranceAuthorizationId				
			,NULL AS PatientInsuranceReferralId
			,CONVERT(BIT, 0) AS OpenForReview
		FROM inserted i
		INNER JOIN Appointments ap ON ap.AppointmentId = i.AppointmentId
		END
	END

	DECLARE @DoesExistsInvoiceReceivableForInsurer INT
	SELECT 
	@DoesExistsInvoiceReceivableForInsurer = ir.Id
	FROM model.InvoiceReceivables ir
	WHERE InvoiceId = @InvoiceId 
		AND PatientInsuranceId IN (
				SELECT
				pins.Id 
				FROM inserted i
				INNER JOIN model.PatientInsurances pins ON i.PatientId = pins.InsuredPatientId
				INNER JOIN model.InsurancePolicies ip ON ip.Id = pins.InsurancePolicyId
				WHERE ip.InsurerId = i.InsurerId
		)

	IF (@QualifyInsertedInvoiceReceivableAsTypeId = 3 AND @DoesExistsInvoiceReceivableForInsurer IS NULL)
	BEGIN
		IF (@InvoiceCreatedOnThisInsertTrigger = 1)
		--ok so this variable being one means we've created the invoice on THIS insert. (it didn't
		--already exist.)
		BEGIN
			INSERT INTO model.InvoiceReceivables (
				InvoiceId
				,PatientInsuranceId
				,PayerClaimControlNumber
				,PatientInsuranceAuthorizationId
				,PatientInsuranceReferralId
				,OpenForReview
				)
			OUTPUT INSERTED.PatientInsuranceId INTO #HoldOrdinals
			SELECT @InvoiceId AS InvoiceId	
					,pi.Id AS PatientInsuranceId
					,i.ExternalRefInfo AS PayerClaimControlNumber
					,pia.Id AS PatientInsuranceAuthorizationId
					,CASE WHEN (ap.ReferralId = '') OR (ap.ReferralId = 0)
						THEN NULL
					ELSE ap.ReferralId
					END AS PatientInsuranceReferralId
					,CONVERT(BIT, 0) AS OpenForReview
				FROM inserted i
				LEFT JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
				LEFT JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
					AND (pi.EndDateTime IS NULL OR pi.EndDateTime = '' OR CONVERT(NVARCHAR,pi.EndDateTime,112) >= i.InvoiceDate)
					AND pi.IsDeleted <> 1
				LEFT JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id AND ip.InsurerId = i.InsurerId
					AND CONVERT(NVARCHAR,ip.StartDateTime,112) <= i.InvoiceDate
				LEFT JOIN model.PatientInsuranceAuthorizations pia ON pia.EncounterId = i.AppointmentId
					AND pi.Id = pia.PatientInsuranceId
					AND pia.IsArchived = 0
					AND pia.AuthorizationDateTime >= ip.StartDateTime
			SET @NumberOfInsuranceRowsInserted = (SELECT @@ROWCOUNT)
			
			UPDATE ir
			SET ir.OpenForReview = CONVERT(BIT,1)
			FROM model.InvoiceReceivables ir
			WHERE ir.InvoiceId = @InvoiceId
			AND ir.PatientInsuranceId = dbo.GetPrimaryInsuranceId(@InvoiceId)

			UPDATE ir
			SET ir.PatientInsuranceAuthorizationId = NULL
			,ir.PatientInsuranceReferralId = NULL
			FROM model.InvoiceReceivables ir
			WHERE ir.InvoiceId = @InvoiceId
			AND ir.PatientInsuranceId <> dbo.GetPrimaryInsuranceId(@InvoiceId)

		END
		ELSE IF (@InvoiceCreatedOnThisInsertTrigger = 0)
		BEGIN
		--so if the invoice already exists, then look through invoicereceivables for the patientinsurance
		--only insert into IR, these new insurances, or what has changed.
		--ex: patient only has primary, later adds a secondary. (only insert secondary)
			INSERT INTO model.InvoiceReceivables (
			InvoiceId			
			,PatientInsuranceId
			,PayerClaimControlNumber
			,PatientInsuranceAuthorizationId
			,PatientInsuranceReferralId
			,OpenForReview
			)
			OUTPUT INSERTED.PatientInsuranceId INTO #HoldOrdinals
			SELECT
			@InvoiceId AS InvoiceId
			,pi.Id
			,i.ExternalRefInfo AS PayerClaimControlNumber
			,pia.Id AS PatientInsuranceAuthorizationId
			,CASE WHEN (ap.ReferralId = '') OR (ap.ReferralId = 0)
				THEN NULL
			ELSE ap.ReferralId
			END AS PatientInsuranceReferralId
			,CASE 
				WHEN i.InsurerDesignation = 'T'
					THEN CONVERT(BIT, 1)
				ELSE CONVERT(BIT, 0)
			END AS OpenForReview
			FROM inserted i 
			INNER JOIN dbo.Appointments ap on ap.AppointmentId = i.AppointmentId
			INNER JOIN model.Invoices iv ON iv.EncounterId = ap.EncounterId AND iv.Id = @InvoiceId
			INNER JOIN model.PatientInsurances pi on pi.InsuredPatientId = ap.PatientId
				AND (pi.EndDateTime IS NULL 
						OR pi.EndDateTime = '' 
						OR pi.EndDateTime >= iv.LegacyDateTime)
					AND pi.IsDeleted <> 1
			INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
				AND ip.InsurerId = i.InsurerId
			LEFT JOIN model.InvoiceReceivables ir on ir.InvoiceId = iv.id and pi.id = ir.PatientInsuranceId
			LEFT JOIN model.PatientInsuranceAuthorizations pia ON pia.EncounterId = i.AppointmentId
				AND pi.Id = pia.PatientInsuranceId
				AND pia.IsArchived = 0
				AND pia.AuthorizationDateTime >= ip.StartDateTime
			WHERE ir.id IS NULL

			SET @NumberOfInsuranceRowsInserted = (SELECT @@ROWCOUNT)
			--to handle when new insurances are added, and previously billed.
		END
	
		UPDATE ir
		SET LegacyInsurerDesignation = CASE WHEN z.Id IS NOT NULL THEN 'T' ELSE NULL END
		FROM model.InvoiceReceivables ir
		LEFT JOIN (
			SELECT 
			v.Id
			,v.InvoiceId
			FROM (
					SELECT
					ROW_NUMBER() OVER (PARTITION BY pi.InsuranceTypeId
						ORDER BY pi.InsuranceTypeId, pi.OrdinalId, PolicyHolderRelationshipTypeId) as rn
					,ir.InvoiceId
					,pi.Id
					FROM model.InvoiceReceivables ir
					INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
					INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
						AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= i.LegacyDateTime)
						AND pi.IsDeleted = 0
					INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
						AND ip.StartDateTime <= i.LegacyDateTime
					WHERE PatientInsuranceId IS NOT NULL
					AND ir.InvoiceId = @InvoiceId
			) v WHERE v.rn = 1
		)z ON z.InvoiceId = ir.InvoiceId
			AND z.Id = ir.PatientInsuranceId
		WHERE ir.InvoiceId = @InvoiceId
			AND ir.PatientInsuranceId IS NOT NULL
	END

	IF EXISTS(
	SELECT 
	Id 
	FROM model.BillingServiceTransactions 
	WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE InvoiceId = @InvoiceId AND PatientInsuranceId IS NOT NULL)
	) OR EXISTS (SELECT 
				* 
				FROM PracticeTransactionJournal 
				WHERE TransactionTypeId IN (SELECT Id FROM model.InvoiceReceivables WHERE InvoiceId = @InvoiceId AND PatientInsuranceId IS NOT NULL) 
					AND TransactionType ='R')
	BEGIN
		UPDATE ir
		SET ir.OpenForReview = CONVERT(BIT,0)
		FROM model.InvoiceReceivables ir
		WHERE ir.InvoiceId = @InvoiceId
		AND PatientInsuranceId IS NOT NULL
	END
	END
	IF OBJECT_ID('tempdb..#TempTableToHoldTheScopeIdentity') IS NOT NULL
	DROP TABLE #TempTableToHoldTheScopeIdentity

	CREATE TABLE #TempTableToHoldTheScopeIdentity (Id BIGINT IDENTITY(1,1) NOT NULL)
	SET IDENTITY_INSERT #TempTableToHoldTheScopeIdentity ON

	IF (@QualifyInsertedInvoiceReceivableAsTypeId = 1)
	BEGIN
		INSERT INTO #TempTableToHoldTheScopeIdentity (Id) 
		SELECT 
		ir.Id AS SCOPE_ID_COLUMN 
		FROM model.InvoiceReceivables ir
		WHERE ir.InvoiceId = @InvoiceId 
	END

	ELSE IF (@QualifyInsertedInvoiceReceivableAsTypeId = 2)
	BEGIN
		INSERT INTO #TempTableToHoldTheScopeIdentity (Id)
		SELECT (1 * 1000000000 + @InsertedCreditRowScopeIdentity) AS SCOPE_ID_COLUMN
	END
	ELSE IF (@QualifyInsertedInvoiceReceivableAsTypeId = 3)
	BEGIN
		IF (@InvoiceCreatedOnThisInsertTrigger = 1)
		BEGIN
			INSERT INTO #TempTableToHoldTheScopeIdentity (Id) 
			SELECT 
			ir.Id AS SCOPE_ID_COLUMN
			FROM #HoldOrdinals ho
			INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = @InvoiceId
				AND ir.PatientInsuranceId = ho.PatientInsuranceId
			WHERE ho.PatientInsuranceId = dbo.GetPrimaryInsuranceId(@InvoiceId)
				AND ir.PatientInsuranceId IS NOT NULL
		END
		ELSE IF (@InvoiceCreatedOnThisInsertTrigger = 0 AND @NumberOfInsuranceRowsInserted > 0)
		BEGIN
			DECLARE @InsurerIdVB INT
			SELECT @InsurerIdVB = i.InsurerId FROM inserted i
			
			INSERT INTO #TempTableToHoldTheScopeIdentity (Id) 
			SELECT 
			TOP 1 ir.Id
			FROM #HoldOrdinals ho
			INNER JOIN model.InvoiceReceivables ir ON ir.Id = ho.PatientInsuranceId
			INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
			INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			WHERE ip.InsurerId = @InsurerIdVB
				AND ir.InvoiceId = @InvoiceId
		END
		ELSE IF (@InvoiceCreatedOnThisInsertTrigger = 0 AND @NumberOfInsuranceRowsInserted = 0)
			INSERT INTO #TempTableToHoldTheScopeIdentity (Id) 
			SELECT TOP 1 ir.Id AS SCOPE_ID_COLUMN
			FROM model.InvoiceReceivables ir
			INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
			INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			WHERE ir.InvoiceId = @InvoiceId 
				AND ip.InsurerId = (SELECT i.InsurerId FROM inserted i)
	END
		
	IF NOT EXISTS(SELECT * FROM ViewIdentities WHERE Name = 'dbo.PatientReceivables')
		INSERT ViewIdentities(Name, Value) VALUES ('dbo.PatientReceivables', @@IDENTITY)
	ELSE
		UPDATE ViewIdentities SET Value = @@IDENTITY WHERE Name = 'dbo.PatientReceivables'

	SELECT @@IDENTITY AS SCOPE_ID_COLUMN

END
GO

--
CREATE TRIGGER OnPatientReceivableUpdate ON dbo.PatientReceivables 
INSTEAD OF UPDATE
AS
SET NOCOUNT ON;

IF EXISTS (
SELECT * FROM inserted
EXCEPT 
SELECT * FROM deleted
)
BEGIN

DECLARE @ProviderBillingOrganizationMax int
DECLARE @ProviderBillingOrganizationServiceLocationMax int
DECLARE @MainLocationId int
DECLARE @ServiceLocationMax int
DECLARE @ClinicalInvoiceProviderMax int

SET @ClinicalInvoiceProviderMax = 110000000
SET @ServiceLocationMax = 110000000
SET @ProviderBillingOrganizationMax = 110000000
SET @ProviderBillingOrganizationServiceLocationMax = 110000000
SET @MainLocationId = (SELECT TOP 1 PracticeId AS PracticeId FROM dbo.PracticeName WHERE PracticeType = 'P' AND LocationReference = '')

--step 1: let's get all the invoice id's we're updating.
SELECT 
ir.InvoiceId
,CASE 
	WHEN COUNT(ir.InvoiceId) = 1
		THEN 1 --only patientinsurance row
	WHEN COUNT(ir.InvoiceId) > 1
		THEN 2 -- patient has insurance
END AS InvoiceIdCounter 
INTO #InvoiceIds
FROM model.InvoiceReceivables ir 
INNER JOIN inserted i ON i.ReceivableId = ir.Id
INNER JOIN Appointments ap ON i.AppointmentId = ap.AppointmentId
GROUP BY ir.InvoiceId
,ap.ResourceId2
,ap.Comments

--make a table to handle our servicelocation codes
SELECT
ReceivableId
,AppointmentId
,CONVERT(BIT,0) AS IsExternal
,NULL AS ServiceLocationCodeId
INTO #ServiceLocationCodes
FROM inserted

--set the is external switch to on when :
UPDATE slc
SET IsExternal = 1
FROM #ServiceLocationCodes slc
JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
WHERE ResourceId2 BETWEEN 1 AND 999

UPDATE slc
SET ServiceLocationCodeId = (
	SELECT TOP 1 pct.Id
	FROM PracticeName pn
	INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE' 
		AND SUBSTRING(pct.Code, 1, 2) = '11')
FROM #ServiceLocationCodes slc
JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
WHERE IsExternal = 0
	AND ap.Comments <> 'ASC CLAIM'

UPDATE slc
SET ServiceLocationCodeId = pct.Id
FROM #ServiceLocationCodes slc
INNER JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
INNER JOIN Resources re ON re.ResourceId = ap.ResourceId2
INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE' 
		AND SUBSTRING(pct.Code, 1, 2) = re.PlaceOfService
WHERE IsExternal = 1
	AND ap.Comments <> 'ASC CLAIM'

DECLARE @pctSL11 INT
SET @pctSL11 = (SELECT 
			MAX(Id) 
			AS Id FROM dbo.PracticeCodeTable
			WHERE ReferenceType = 'PLACEOFSERVICE'
				AND SUBSTRING(Code, 1, 2) = '11'
			GROUP BY ReferenceType, Code
)

;WITH pctSLCode AS (
	SELECT
	MAX(pctSLCode.Id) AS Id
	,ap.AppointmentId
	FROM dbo.PracticeCodeTable pctSLCode 
	INNER JOIN #ServiceLocationCodes slc ON slc.AppointmentId IS NOT NULL
	INNER JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
		AND ap.Comments = 'ASC CLAIM' 
	INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
	INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P'
		AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
		AND PracticeType = 'P'
		AND LocationReference <> ''
	INNER JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
		AND reASC.ResourceType = 'R'
		AND reASC.ServiceCode = '02'
		AND reASC.Billable = 'Y'
		AND pic.FieldValue = 'T'
	WHERE SUBSTRING(pctSLCode.Code, 1, 2) = reASC.PlaceOfService
		AND pctSLCode.ReferenceType = 'PLACEOFSERVICE'
	GROUP BY ap.AppointmentId
)
UPDATE slc
SET ServiceLocationCodeId = CASE 
		WHEN pctSLCode.Id IS NOT NULL AND pctSLCode.Id <> ''
			THEN pctSLCode.Id
		ELSE @pctSL11
	END
FROM #ServiceLocationCodes slc
INNER JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
	AND ap.Comments = 'ASC CLAIM' 
LEFT OUTER JOIN pctSLCode ON pctSLCode.AppointmentId = ap.AppointmentId

IF EXISTS (
	SELECT
	*
	FROM inserted i
	WHERE i.ReceivableType = 'S'
		AND i.OpenBalance = 0
		AND i.InsuredId = 0
		AND i.InsurerId = 99999
		AND i.AppointmentId = 0
		AND i.Charge = 0
		AND i.BillToDr = 0
		AND i.BillingOffice = 0
		AND i.Status = 'A'
		AND i.ReceivableId - (1 * 1000000000) IN 
		(SELECT Id FROM model.Adjustments WHERE InvoiceReceivableId IS NULL AND Id IN 
		(SELECT ReceivableId-(1 * 1000000000) FROM inserted))
	)
BEGIN
	DECLARE @iOpenBalance DECIMAL(18,2)
	SET @iOpenBalance = (SELECT ABS(i.OpenBalance) FROM inserted i)

	IF (@iOpenBalance = 0)
	BEGIN
		UPDATE adj
		SET adj.Amount = -0.01
		FROM model.Adjustments adj
		INNER JOIN inserted i ON i.ReceivableId - (1 * 1000000000) = adj.Id
	END
	ELSE 
	BEGIN
		UPDATE adj
		SET adj.Amount = @iOpenBalance
		FROM model.Adjustments adj
		INNER JOIN inserted i ON i.ReceivableId - (1 * 1000000000) = adj.Id
	END
END

ELSE
BEGIN
--Patient does not have insurance and will not get insurance
IF EXISTS 
	(SELECT * FROM inserted i
		INNER JOIN model.InvoiceReceivables ir ON i.ReceivableId = ir.Id
		WHERE ir.PatientInsuranceId IS NULL AND i.InsurerId = 0)
BEGIN
	UPDATE ir
	SET OpenForReview = CASE 
		WHEN i.ReceivableType = 'I'
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0) END
	FROM model.InvoiceReceivables ir 
	INNER JOIN inserted i ON ir.Id = i.ReceivableId	
END

--Patient has insurance and will continue to have insurance
IF EXISTS (
		SELECT * FROM inserted i
		INNER JOIN model.InvoiceReceivables ir ON i.ReceivableId = ir.Id
		WHERE ir.PatientInsuranceId IS NOT NULL AND i.InsurerId <> 0)
BEGIN 
	UPDATE ir
	SET ir.OpenForReview = CASE 
		WHEN i.ReceivableType = 'I'
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0) 
		END
		,ir.PayerClaimControlNumber = i.ExternalRefInfo
	FROM model.InvoiceReceivables ir 
	INNER JOIN inserted i ON ir.Id = i.ReceivableId
	INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		AND ip.InsurerId = i.InsurerId 
		AND ip.PolicyHolderPatientId = i.InsuredId
END

--Patient did not have insurance and now has insurance.
IF EXISTS (
	SELECT 
	PatientId
	FROM inserted i
	JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
	WHERE Id NOT IN (
		SELECT 
		PatientInsuranceId 
		FROM model.InvoiceReceivables 
		WHERE InvoiceId = 
			(SELECT 
			InvoiceId 
			FROM model.InvoiceReceivables 
			WHERE Id = 
				(SELECT 
				ReceivableId 
				FROM inserted
				)
			)
		)
	)
BEGIN
	INSERT INTO [model].[InvoiceReceivables] (
		[InvoiceId]
		,[PatientInsuranceId]
		,[PayerClaimControlNumber]
		,[PatientInsuranceAuthorizationId]
		,[PatientInsuranceReferralId]
		,[OpenForReview]
		)
	SELECT invIds.InvoiceId
			,pi.Id AS PatientInsuranceId
			,i.ExternalRefInfo AS PayerClaimControlNumber
			,pia.Id AS PatientInsuranceAuthorizationId
			,CASE WHEN (ap.ReferralId = '') OR (ap.ReferralId = 0)
				THEN NULL
			ELSE ap.ReferralId
		    END AS PatientInsuranceReferralId
			,CASE 
				WHEN i.InsurerDesignation = 'T'
					THEN CONVERT(BIT, 1)
				ELSE CONVERT(BIT, 0)
			END AS OpenForReview
		FROM inserted i
		LEFT JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
		LEFT JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
			AND (pi.EndDateTime IS NULL OR pi.EndDateTime = '' OR CONVERT(NVARCHAR,pi.EndDateTime,112) >= i.InvoiceDate)
			AND pi.IsDeleted <> 1
		LEFT JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id AND ip.InsurerId = i.InsurerId
			AND CONVERT(NVARCHAR,ip.StartDateTime,112) <= i.InvoiceDate
		INNER JOIN model.InvoiceReceivables ir ON ir.Id = i.ReceivableId
		LEFT JOIN model.PatientInsuranceAuthorizations pia ON pia.EncounterId = i.AppointmentId
			AND pi.Id = pia.PatientInsuranceId
			AND pia.IsArchived = 0
			AND pia.AuthorizationDateTime >= ip.StartDateTime
		INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId AND InvoiceIdCounter = 1
	
	--set is open for review open false when not the minimum ordinalId, per insurance type.
	UPDATE ir
	SET ir.OpenForReview = CONVERT(BIT,0)
	,ir.PatientInsuranceAuthorizationId = NULL
	,ir.PatientInsuranceReferralId = NULL
	FROM model.InvoiceReceivables ir
	INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId AND InvoiceIdCounter = 1
	INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
	WHERE pi.OrdinalId NOT IN (
		SELECT MIN(pi.OrdinalId) 
		FROM model.InvoiceReceivables ir
		INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId 
		INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
		GROUP BY 
		ir.InvoiceId
		,pi.InsuranceTypeId)
	AND PatientInsuranceId IS NOT NULL

--Patient does not have insurance before udpate, the update is giving insurnace. Need to update all references to old receivableId
	UPDATE ptj
	SET ptj.TransactionTypeId = ir.Id
	FROM PracticeTransactionJournal ptj
	INNER JOIN inserted i ON ptj.TransactionTypeId = i.ReceivableId
	INNER JOIN model.InvoiceReceivables ir ON i.ReceivableId = ir.Id
	INNER JOIN model.Invoices inv ON ir.InvoiceId = inv.Id
	LEFT JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
	LEFT JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id AND ip.InsurerId = i.InsurerId
	LEFT JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
	WHERE TransactionType = 'R' AND TransactionRef <> 'G'
END

---invoices
	UPDATE iv
	SET ClinicalInvoiceProviderId = 
			CASE 
				WHEN ap.Comments <> 'ASC CLAIM' 
					THEN model.GetBigPairId(ap.ResourceId1, iv.EncounterId, @ClinicalInvoiceProviderMax)
				ELSE model.GetBigPairId(reASC.ResourceId, iv.EncounterId, @ClinicalInvoiceProviderMax) 
			END
		,BillingProviderId = 
			model.GetBigPairId(CASE --1st
				WHEN ap.ResourceId2 = 0
					THEN @MainLocationId
				WHEN ap.ResourceId2 > 999
					THEN (ap.ResourceId2 - 1000)
				ELSE model.GetPairId(1, ap.ResourceId2, @ServiceLocationMax)
			END 
			,model.GetBigPairId(CASE --2nd number
				WHEN ap.Comments <> 'ASC CLAIM'
					THEN re.PracticeId
				WHEN ap.Comments = 'ASC CLAIM'
					THEN (ap.ResourceId2 - 1000)
				END 
			,CASE 
			WHEN ap.Comments <> 'ASC CLAIM'
				THEN i.BillToDr
			ELSE (
					SELECT TOP 1 ResourceId
					FROM Resources
					WHERE PracticeId = (ap.ResourceId2 - 1000) AND ResourceType = 'R' AND ServiceCode = '02' AND Billable = 'Y'
				)
			END, 
			@ProviderBillingOrganizationMax), 
			@ProviderBillingOrganizationServiceLocationMax)
		,ReferringExternalProviderId = 
			CASE
				WHEN (i.ReferDr = 0 OR i.ReferDr = '')
					THEN NULL
				ELSE i.ReferDr 
			END
		,ServiceLocationCodeId = slc.ServiceLocationCodeId
		,iv.LegacyPatientBilledDateTime = 
			CASE 
				WHEN i.PatientBillDate = ''
					THEN NULL
				ELSE CONVERT(datetime, i.PatientBillDate, 112) 
			END				
		,AttributeToServiceLocationId = 
			CASE 
				WHEN i.BillingOffice = 0
					THEN 1
				WHEN i.BillingOffice BETWEEN 1 AND 999
					THEN model.GetPairId(1, reAtribTo.ResourceId, @ServiceLocationMax)
				ELSE i.BillingOffice - 1000
			END
	FROM inserted i
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = i.ReceivableId
	INNER JOIN model.Invoices iv ON iv.Id = ir.InvoiceId
	LEFT JOIN #ServiceLocationCodes slc ON slc.ReceivableId = i.ReceivableId
	INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = iv.Id
	INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
	INNER JOIN model.Patients p ON p.Id = i.PatientId
	LEFT JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
	LEFT JOIN dbo.Resources reAtribTo ON i.BillingOffice = reAtribTo.ResourceId
	LEFT JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId 
		AND apEncounter.AppTypeId = ap.AppTypeId 
		AND apEncounter.AppDate = ap.AppDate 
		AND apEncounter.AppTime > 0 
		AND ap.AppTime = 0 
		AND apEncounter.ScheduleStatus = ap.ScheduleStatus 
		AND ap.ScheduleStatus = 'D' 
		AND apEncounter.ResourceId1 = ap.ResourceId1 
		AND apEncounter.ResourceId2 = ap.ResourceId2
	LEFT JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
	LEFT JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P'
		AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
		AND PracticeType = 'P'
		AND LocationReference <> ''
	LEFT JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
		AND reASC.ResourceType = 'R'
		AND reASC.ServiceCode = '02'
		AND reASC.Billable = 'Y'
		AND pic.FieldValue = 'T'

-- invoice supplemental logic
IF EXISTS (
SELECT *
FROM inserted i
WHERE FirstConsDate <> '' OR HspAdmDate <> '' OR Over90 <> '' OR Over90 <> '' OR RsnDate <> '' OR HspDisDate <> '' OR AccType <> ''
)
AND NOT EXISTS(SELECT * FROM model.InvoiceSupplementals WHERE Id IN (SELECT InvoiceId FROM #InvoiceIds))
BEGIN
	INSERT INTO model.InvoiceSupplementals (
		Id
		,AccidentDateTime
		,AccidentStateOrProvinceId
		,AdmissionDateTime
		,ClaimDelayReasonId
		,DisabilityDateTime
		,DischargeDateTime
		,RelatedCause1Id
		,VisionPrescriptionDateTime
		)
	SELECT invIds.InvoiceId AS Id
		,CASE 
			WHEN FirstConsDate = ''
				THEN NULL
			ELSE CONVERT(DATETIME, FirstConsDate, 101)
			END AS AccidentDateTime
		,CASE st.id
			WHEN 0
				THEN NULL
			ELSE st.id
			END AS AccidentStateOrProvinceId
		,CASE 
			WHEN HspAdmDate = ''
				THEN NULL
			ELSE CONVERT(DATETIME, HspAdmDate, 101)
			END AS AdmissionDateTime
		,CASE Over90
			WHEN 1
				THEN 1
			WHEN 2
				THEN 2
			WHEN 3
				THEN 3
			WHEN 4
				THEN 4
			WHEN 5
				THEN 5
			WHEN 6
				THEN 6
			WHEN 7
				THEN 7
			WHEN 8
				THEN 8
			WHEN 9
				THEN 9
			WHEN 10
				THEN 10
			WHEN 11
				THEN 11
			WHEN 15
				THEN 12
			ELSE NULL
			END AS ClaimDelayReasonId
		,CASE 
			WHEN COALESCE(RsnDate, '') <> ''
				THEN CONVERT(DATETIME, RsnDate, 101)
			ELSE NULL
			END AS DisabilityDateTime
		,CASE 
			WHEN COALESCE(HspDisDate, '') <> ''
				THEN CONVERT(DATETIME, HspDisDate, 101)
			ELSE NULL
			END AS DischargeDateTime
		,CASE AccType
			WHEN 'A'
				THEN 1
			WHEN 'E'
				THEN 2
			WHEN 'O'
				THEN 3
			ELSE NULL
			END AS RelatedCause1Id
		,CONVERT(DATETIME, NULL, 101) AS VisionPrescriptionDateTime
	FROM inserted i
	INNER JOIN model.InvoiceReceivables ir ON i.ReceivableId = ir.Id
	INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId
	LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = i.AccState
	WHERE NOT EXISTS (SELECT * FROM model.InvoiceSupplementals WHERE Id IN (SELECT InvoiceId FROM #InvoiceIds))
END
	
ELSE IF EXISTS (
	SELECT *
	FROM inserted i
	WHERE FirstConsDate <> '' OR HspAdmDate <> '' OR Over90 <> '' OR Over90 <> '' OR RsnDate <> '' OR HspDisDate <> '' OR AccType <> '')
AND EXISTS(SELECT * FROM model.InvoiceSupplementals WHERE Id IN (SELECT InvoiceId FROM #InvoiceIds))
BEGIN
	UPDATE ivs
	SET AccidentDateTime = CASE WHEN i.FirstConsDate = '' THEN NULL ELSE CONVERT(datetime,i.FirstConsDate, 101) END
	,AccidentStateOrProvinceId = CASE 
		WHEN st.id IS NULL
			THEN NULL
		ELSE st.id
		END
	,AdmissionDateTime = CASE WHEN i.HspAdmDate = '' THEN NULL ELSE CONVERT(datetime,i.HspAdmDate, 101) END
	,ClaimDelayReasonId = CASE Over90
		WHEN 1
			THEN 1
		WHEN 2
			THEN 2
		WHEN 3
			THEN 3
		WHEN 4
			THEN 4
		WHEN 5
			THEN 5
		WHEN 6
			THEN 6
		WHEN 7
			THEN 7
		WHEN 8
			THEN 8
		WHEN 9
			THEN 9
		WHEN 10
			THEN 10
		WHEN 11
			THEN 11
		WHEN 15
			THEN 12
		ELSE NULL
		END
	,DisabilityDateTime = CASE
		WHEN COALESCE(RsnDate, '') <> ''
			THEN CONVERT(datetime,RsnDate, 101) 
		ELSE NULL END
	,DischargeDateTime = CASE 
		WHEN COALESCE(HspDisDate, '') <> ''
			THEN CONVERT(datetime, HspDisDate, 101)
		ELSE NULL END
	,RelatedCause1Id = CASE AccType
		WHEN 'A'
			THEN 1
		WHEN 'E'
			THEN 2
		WHEN 'O'
			THEN 3
		ELSE NULL
		END
	,VisionPrescriptionDateTime = CONVERT(datetime, NULL, 101)
	FROM inserted i 
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = i.ReceivableId
	INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId
	INNER JOIN model.InvoiceSupplementals ivs ON invIds.InvoiceId = ivs.Id
	LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = i.AccState
END

-- for updating the InsurerDesignation.
SELECT ReceivableId
,InsurerDesignation
INTO #NeedsLegacyInsurerDesignationUpdate
FROM inserted

EXCEPT

SELECT ReceivableId
,InsurerDesignation
FROM deleted

UPDATE ir
SET LegacyInsurerDesignation = CASE WHEN z.Id IS NOT NULL THEN 'T' ELSE NULL END
FROM model.InvoiceReceivables ir
LEFT JOIN (
	SELECT 
	v.Id
	,v.InvoiceId
	FROM (
			SELECT
			ROW_NUMBER() OVER (PARTITION BY pi.InsuranceTypeId
				ORDER BY pi.InsuranceTypeId, pi.OrdinalId, PolicyHolderRelationshipTypeId) as rn
			,ir.InvoiceId
			,pi.Id
			FROM model.InvoiceReceivables ir
			INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
			INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
				AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= i.LegacyDateTime)
				AND pi.IsDeleted = 0
			INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
				AND ip.StartDateTime <= i.LegacyDateTime
			INNER JOIN #NeedsLegacyInsurerDesignationUpdate nlidu ON nlidu.ReceivableId = ir.Id
			WHERE PatientInsuranceId IS NOT NULL
	) v WHERE v.rn = 1
)z ON z.InvoiceId = ir.InvoiceId
	AND z.Id = ir.PatientInsuranceId
INNER JOIN #NeedsLegacyInsurerDesignationUpdate nlidu ON nlidu.ReceivableId = ir.Id
WHERE ir.PatientInsuranceId IS NOT NULL

END
END
GO

--
CREATE PROCEDURE dbo.GetUnLinkedPayments
	@Invoice NVARCHAR(100)
AS
SET NOCOUNT ON;

DECLARE @AppointmentId INT
SELECT @AppointmentId = SUBSTRING(@Invoice, (CHARINDEX('-', @Invoice) + 1), LEN(@Invoice))
SELECT Id INTO #InvoiceReceivableIds FROM dbo.GetInvoiceReceivablePerAppointmentId(@AppointmentId)

DECLARE @AdjustmentIdMax INT
SET @AdjustmentIdMax = 110000000
DECLARE @FinancialIdMax INT
SET @FinancialIdMax = 110000000

IF OBJECT_ID('tempdb..#AdjustmentTypes') IS NOT NULL
BEGIN
	DROP TABLE #AdjustmentTypes
END

SELECT 
v.Id AS Id
,v.PaymentType
,v.IsDebit AS IsDebit
,v.ShortName AS ShortName
INTO #AdjustmentTypes
FROM (
	SELECT 
		CASE
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'P'
				THEN 1
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'X'
				THEN 2
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '8'
				THEN 3
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'R'
				THEN 4
			WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'U'
				THEN 5
			ELSE pct.Id + 1000
		END AS Id
		,SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS PaymentType
		,CASE pct.AlternateCode
			WHEN 'D'
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
		END AS IsDebit
		,LetterTranslation AS ShortName
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'
	GROUP BY 
	Id
	,Code
	,pct.AlternateCode
	,LetterTranslation
) AS v

IF OBJECT_ID('tempdb..#FinancialInformationTypes') IS NOT NULL
BEGIN
	DROP TABLE #FinancialInformationTypes
END

SELECT 
v.Id AS Id
,v.FinancialInformationType
,v.ShortName AS ShortName
,v.PaymentFinancialType
INTO #FinancialInformationTypes
FROM (
	SELECT 
	CASE
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '!'
			THEN 1
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '|'
			THEN 2
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '?'
			THEN 3
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '%'
			THEN 4
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'D'
			THEN 5
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '&'
			THEN 6
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = ']'
			THEN 7
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '0'
			THEN 8
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'V'
			THEN 9
		ELSE pct.Id + 1000
	END AS Id
	,SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) AS FinancialInformationType
	,AlternateCode AS PaymentFinancialType
	,LetterTranslation AS ShortName
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'		
		AND (pct.AlternateCode = '' OR pct.AlternateCode IS NULL)
	GROUP BY 
	pct.Id
	,pct.Code
	,LetterTranslation
	,AlternateCode
) AS v

SELECT
model.GetPairId(1, adj.Id, 11000000) AS PaymentId
,CASE
	WHEN adj.FinancialSourceTypeId = 2 
		THEN COALESCE(v.InvoiceReceivableId,adj.InvoiceReceivableId)
	ELSE adj.InvoiceReceivableId 
END AS ReceivableId
,CASE 
	WHEN (adj.FinancialSourceTypeId = 1)
		THEN adj.InsurerId
	WHEN (adj.FinancialSourceTypeId = 2)
		THEN adj.PatientId
	ELSE 0
END AS PayerId
,CASE adj.FinancialSourceTypeId
	WHEN 1
		THEN 'I'
	WHEN 2
		THEN 'P'
	WHEN 3
		THEN 'O'
END AS PayerType
,at.PaymentType AS PaymentType
,adj.Amount AS PaymentAmount
,CONVERT(NVARCHAR(8),adj.PostedDateTime,112) AS PaymentDate
,COALESCE(fb.CheckCode,'') AS PaymentCheck
,CASE at.IsDebit 
	WHEN 1
		THEN 'D'
	ELSE 'C'
END AS PaymentFinancialType
,COALESCE(pctRsnCode.Code,'') AS PaymentReason
,prs.Service AS PaymentService
,CASE 
	WHEN (adj.FinancialSourceTypeId = 1)
		THEN pi.InsurerName
	WHEN (adj.FinancialSourceTypeId = 2)
		THEN 'PATIENT'
	ELSE 'OFFICE'
END AS InsurerName
,CASE 
	WHEN (adj.Comment = '' OR adj.Comment IS NULL)
		THEN ''
	ELSE 'N'
END AS NoteInd
,at.ShortName AS LetterTranslation
FROM model.Adjustments adj
INNER JOIN #AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
LEFT JOIN PatientReceivableServices prs ON prs.ItemId = adj.BillingServiceId
	AND (prs.[Status] = 'X' OR prs.[Status] IS NULL)
INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
INNER JOIN #InvoiceReceivableIds irs ON irs.Id = ir.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN dbo.Appointments ap ON ap.EncounterId = i.EncounterId
LEFT JOIN (
	SELECT
	ir.Id AS InvoiceReceivableId
	,MIN(pi1.OrdinalId) AS OrdinalId
	,ir.InvoiceId
	,ips.StartDateTime
	,pi1.EndDateTime
	FROM model.PatientInsurances pi1
	INNER JOIN (
		SELECT 
		InsuredPatientId
		,MIN(pi.OrdinalId) AS LowestOrdinalId
		,InsuranceTypeId
		FROM model.PatientInsurances pi 
		INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
		WHERE (pi.EndDateTime IS NULL AND pi.IsDeleted <> 1)
		GROUP BY InsuredPatientId
		,InsuranceTypeId
	) pi2 ON pi1.InsuredPatientId = pi2.InsuredPatientId
		AND pi1.OrdinalId = pi2.LowestOrdinalId
	INNER JOIN model.InsurancePolicies ips ON ips.Id = pi1.InsurancePolicyId
	INNER JOIN model.InvoiceReceivables ir ON ir.PatientInsuranceId = pi1.Id
	WHERE pi1.IsDeleted <> 1
	GROUP BY
	ir.Id
	,ir.InvoiceId
	,ips.StartDateTime
	,pi1.EndDateTime
) v ON v.InvoiceId = ir.InvoiceId
	AND v.StartDateTime <= CONVERT(DATETIME,ap.AppDate)
	AND (v.EndDateTime IS NULL OR v.EndDateTime >= CONVERT(DATETIME,ap.AppDate))
LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
LEFT JOIN dbo.PracticeCodeTable pctRsnCode ON pctRsnCode.Id = adj.ClaimAdjustmentReasonCodeId 
	AND pctRsnCode.ReferenceType = 'PAYMENTREASON'
LEFT JOIN dbo.PracticeInsurers pi ON pi.InsurerId = adj.InsurerId
WHERE adj.BillingServiceId IS NULL

UNION ALL

SELECT
model.GetPairId(2, fi.Id, @FinancialIdMax) AS PaymentId
,CASE
	WHEN fi.FinancialSourceTypeId = 2 
		THEN COALESCE(v.InvoiceReceivableId,fi.InvoiceReceivableId)
	ELSE fi.InvoiceReceivableId 
END AS ReceivableId
,CASE 
	WHEN (fi.FinancialSourceTypeId = 1)
		THEN fi.InsurerId
	WHEN (fi.FinancialSourceTypeId = 2)
		THEN fi.PatientId
	ELSE 0
END AS PayerId
,CASE fi.FinancialSourceTypeId
	WHEN 1
		THEN 'I'
	WHEN 2
		THEN 'P'
	WHEN 3
		THEN 'O'
END AS PayerType
,fit.FinancialInformationType AS PaymentType
,fi.Amount AS PaymentAmount
,CONVERT(NVARCHAR(8),fi.PostedDateTime,112) AS PaymentDate
,COALESCE(fb.CheckCode,'') AS PaymentCheck
,COALESCE(fit.PaymentFinancialType,'') AS PaymentFinancialType
,COALESCE(pctRsnCode.Code,'') AS PaymentReason
,prs.Service AS PaymentService
,CASE 
	WHEN (fi.FinancialSourceTypeId = 1)
		THEN pi.InsurerName
	WHEN (fi.FinancialSourceTypeId = 2)
		THEN 'PATIENT'
	ELSE 'OFFICE'
END AS InsurerName
,CASE 
	WHEN (fi.Comment = '' OR fi.Comment IS NULL)
		THEN ''
	ELSE 'N'
END AS NoteInd
,fit.ShortName AS LetterTranslation
FROM model.FinancialInformations fi
INNER JOIN #FinancialInformationTypes fit ON fit.Id = fi.FinancialInformationTypeId
LEFT JOIN PatientReceivableServices prs ON prs.ItemId = fi.BillingServiceId
	AND (prs.[Status] = 'X' OR prs.[Status] IS NULL)
INNER JOIN model.InvoiceReceivables ir ON ir.Id = fi.InvoiceReceivableId
INNER JOIN #InvoiceReceivableIds irs ON irs.Id = ir.Id
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN dbo.Appointments ap ON ap.EncounterId = i.EncounterId
LEFT JOIN (
	SELECT
	ir.Id AS InvoiceReceivableId
	,MIN(pi1.OrdinalId) AS OrdinalId
	,ir.InvoiceId
	,ips.StartDateTime
	,pi1.EndDateTime
	FROM model.PatientInsurances pi1
	INNER JOIN (
		SELECT 
		InsuredPatientId
		,MIN(pi.OrdinalId) AS LowestOrdinalId
		,InsuranceTypeId
		FROM model.PatientInsurances pi 
		INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
		WHERE (pi.EndDateTime IS NULL AND pi.IsDeleted <> 1)
		GROUP BY InsuredPatientId
		,InsuranceTypeId
	) pi2 ON pi1.InsuredPatientId = pi2.InsuredPatientId
		AND pi1.OrdinalId = pi2.LowestOrdinalId
	INNER JOIN model.InsurancePolicies ips ON ips.Id = pi1.InsurancePolicyId
	INNER JOIN model.InvoiceReceivables ir ON ir.PatientInsuranceId = pi1.Id
	WHERE pi1.IsDeleted <> 1
	GROUP BY
	ir.Id
	,ir.InvoiceId
	,ips.StartDateTime
	,pi1.EndDateTime
) v ON v.InvoiceId = ir.InvoiceId
	AND v.StartDateTime <= CONVERT(DATETIME,ap.AppDate)
	AND (v.EndDateTime IS NULL OR v.EndDateTime >= CONVERT(DATETIME,ap.AppDate))
LEFT JOIN model.FinancialBatches fb ON fb.Id = fi.FinancialBatchId
LEFT JOIN dbo.PracticeCodeTable pctRsnCode ON pctRsnCode.Id = fi.ClaimAdjustmentReasonCodeId 
	AND pctRsnCode.ReferenceType = 'PAYMENTREASON'
LEFT JOIN dbo.PracticeInsurers pi ON pi.InsurerId = fi.InsurerId
WHERE fi.BillingServiceId IS NULL

GO

--
EXEC ('
CREATE PROCEDURE dbo.usp_PaymentToDeleted
(
@PatId INT
)
AS
BEGIN
	IF OBJECT_ID(''tempdb..#AdjustmentTypes'') IS NOT NULL
	BEGIN
		DROP TABLE #AdjustmentTypes
	END

	SELECT 
	v.Id AS Id
	,v.PaymentType
	,v.IsDebit AS IsDebit
	,v.ShortName AS ShortName
	INTO #AdjustmentTypes
	FROM (
		SELECT 
			CASE
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''P''
					THEN 1
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''X''
					THEN 2
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''8''
					THEN 3
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''R''
					THEN 4
				WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) = ''U''
					THEN 5
				ELSE pct.Id + 1000
			END AS Id
			,SUBSTRING(code, (dbo.GetMax((CHARINDEX('' - '', Code)+3), 0)), LEN(Code)) AS PaymentType
			,CASE pct.AlternateCode
				WHEN ''D''
					THEN CONVERT(bit, 1)
				ELSE CONVERT(bit, 0)
			END AS IsDebit
			,LetterTranslation AS ShortName
		FROM dbo.PracticeCodeTable pct
		WHERE pct.ReferenceType = ''PAYMENTTYPE''
		GROUP BY 
		Id
		,Code
		,pct.AlternateCode
		,LetterTranslation
	) AS v

	CREATE UNIQUE CLUSTERED INDEX IX_AdjustmentTypes_Id ON #AdjustmentTypes (Id)

	SELECT
	adj.Amount AS PaymentAmount
	,at.PaymentType
	,i.LegacyPatientReceivablesInvoice AS Invoice
	,CONVERT(NVARCHAR,i.LegacyDateTime,112) AS InvoiceDate
	,p.LastName
	,p.FirstName
	,adj.InvoiceReceivableId AS ReceivableId
	,model.GetPairId(1,adj.Id,11000000) AS PaymentId
	,ap.EncounterAppointmentId AS AppointmentId
	,p.Id AS PatientId
	,prs.ItemId AS PaymentServiceItem
	,CONVERT(NVARCHAR,adj.PostedDateTime,112) AS PaymentDate
	,prs.Status
	FROM model.Adjustments adj
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
	INNER JOIN #AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
	INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
	INNER JOIN dbo.Appointments ap ON ap.EncounterAppointmentId = i.EncounterId
	INNER JOIN model.Patients p ON p.Id = ap.PatientId
	INNER JOIN PatientReceivableServices prs ON prs.ItemId = adj.BillingServiceId
	WHERE adj.AdjustmentTypeId = 1
		AND prs.Status = ''X''
		AND (pd.PatientId = @PatId OR @PatId = ''0'')
END
')
GO
--
CREATE PROCEDURE dbo.GetReceivableIdInsurerIdInsurerDesignationByPatientIdInvoiceDateServiceModifier 
	@PatientId INT
	,@InvoiceDate NVARCHAR(8)
	,@Service NVARCHAR(MAX)
	,@Modifier NVARCHAR(MAX)
AS
BEGIN

SELECT 
ir.Id AS ReceivableId
,pins.InsurerId
,COALESCE(ir.LegacyInsurerDesignation,'F') AS InsurerDesignation
FROM model.InvoiceReceivables ir 
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
LEFT JOIN dbo.PracticeInsurers pins ON pins.InsurerId = ip.InsurerId
LEFT JOIN dbo.PatientReceivableServices prs ON prs.Invoice = i.LegacyPatientReceivablesInvoice
WHERE ir.InvoiceId IN (
	SELECT 
	i.Id 
	FROM model.Invoices i 
	WHERE i.EncounterId IN (
		SELECT 
		DISTINCT ap.EncounterId 
		FROM dbo.Appointments ap 
		WHERE ap.PatientId = @PatientId
	)
	AND CONVERT(NVARCHAR,i.LegacyDateTime,112) = @InvoiceDate
)
AND prs.[Service] = @Service
AND prs.Modifier = @Modifier
AND prs.[Status] = 'A'
END
GO