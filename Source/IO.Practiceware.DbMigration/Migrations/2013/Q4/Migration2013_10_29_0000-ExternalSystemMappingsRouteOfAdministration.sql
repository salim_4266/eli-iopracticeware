﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[RouteOfAdministrations]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[RouteOfAdministrations](
	Id INT NOT NULL IDENTITY (1,1) ,
	[Name] [nvarchar](2000) NULL,
	[OrdinalId] [int] NULL
)
END
GO

-- Creating primary key on [Id] in table 'RouteOfAdministrations'
ALTER TABLE [model].[RouteOfAdministrations]
ADD CONSTRAINT [PK_RouteOfAdministrations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

IF ((SELECT COUNT(*) FROM [model].[RouteOfAdministrations]) <= 0)
BEGIN
	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Apply on the skin', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Apply to eyelids', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Apply to face', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Apply to nails', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Apply to the scalp', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Apply to the teeth', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'As directed', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'By mouth', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Epidural', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'In both ears', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'In both eyes', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'In left ear', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'In left eye', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'In right ear', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'In right eye', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'In surgical eye', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'In the nostrils', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'In the rectum', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'In the urethra', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'In the vagina', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'In vitro', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Inhale', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Inject below the skin', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Inject into the penis', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Inject into the skin', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intraarterial', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intraarticular', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intramuscular injection', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intraocular', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intraperitoneal', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intrapleural', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intrathecal', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intrauterine', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intravenous', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intravesical', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'On the tongue', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Perfusion', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Rinse', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Thin layer to the face', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Under the lip', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Under the tongue', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Via feeding tube', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Instill in eyes', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intramuscular', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Subcutaneous', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intradermal', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Intranasal', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Percutaneous', 0)

	INSERT [model].[RouteOfAdministrations] ([Name], [OrdinalId]) VALUES (N'Oral', 0)

DECLARE @NciThesaurusExternalSystemIdRouteOfAdministration INT
DECLARE @NewCropExternalSystemIdRouteOfAdministration INT

DECLARE @NciThesaurusExternalSystemId INT
SELECT @NciThesaurusExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'NciThesaurus'

DECLARE @NewCropExternalSystemId INT
SELECT @NewCropExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'NewCrop'

DECLARE @RouteOfAdministrationsId INT
SELECT @RouteOfAdministrationsId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'RouteOfAdministration'

SELECT @NciThesaurusExternalSystemIdRouteOfAdministration = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @NciThesaurusExternalSystemId AND Name = 'RouteOfAdministration'
SELECT @NewCropExternalSystemIdRouteOfAdministration = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @NewCropExternalSystemId AND Name = 'RouteOfAdministration'

;WITH CTE AS (
SELECT
@RouteOfAdministrationsId AS PracticeRepositoryEntityId
,mROA.Id AS PracticeRepositoryEntityKey
,@NewCropExternalSystemIdRouteOfAdministration AS ExternalSystemEntityId
,CONVERT(NVARCHAR,(CASE WHEN nROA.NewCropId <> 1 THEN nROA.NewCropId ELSE nROA.Id END)) AS ExternalSystemEntityKey
FROM model.RouteOfAdministrations mROA
LEFT JOIN [newCrop].[RouteOfAdministrations] nROA ON
	mROA.Name LIKE '%'+nROA.Name+'%'
UNION ALL
SELECT
@RouteOfAdministrationsId AS PracticeRepositoryEntityId
,mROA.Id AS PracticeRepositoryEntityKey
,@NciThesaurusExternalSystemIdRouteOfAdministration AS ExternalSystemEntityId
,nROA.Code AS ExternalSystemEntityKey
FROM model.RouteOfAdministrations mROA
LEFT JOIN [nci].[RouteOfAdministration] nROA ON
	mROA.Name LIKE '%'+nROA.SPLAcceptableTerm+'%'
)
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId,PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT 
CTE.PracticeRepositoryEntityId
,CTE.PracticeRepositoryEntityKey
,CTE.ExternalSystemEntityId
,CASE 
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Apply on the skin'
		THEN 'C38675' --CUTANEOUS
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Apply to eyelids'
		THEN 'C38287' --OPHTHALMIC
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Apply to face'
		THEN 'C38675' --CUTANEOUS
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Apply to nails'
		THEN 'C38675' --CUTANEOUS
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Apply to the scalp'
		THEN 'C38675' --CUTANEOUS
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Apply to the teeth'
		THEN 'C38294' --PERIODONTAL
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'As directed'
		THEN 'C48623' --NOT APPLICABLE
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'By mouth'
		THEN 'C38288' --BUCCAL
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'In both ears'
		THEN 'C38192' --AURICULAR (OTIC)
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'In both eyes'
		THEN 'C38255' --INTRAOCULAR
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'In left ear'
		THEN 'C38192' --AURICULAR (OTIC)
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'In left eye'
		THEN 'C38255' --INTRAOCULAR
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'In right ear'
		THEN 'C38192' --AURICULAR (OTIC)
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'In right eye'
		THEN 'C38255' --INTRAOCULAR
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'In surgical eye'
		THEN 'C38255' --INTRAOCULAR
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'In the urethra'
		THEN 'C38271' --URETHRAL
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'In vitro'
		THEN 'C48623' --NOT APPLICABLE
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Inhale'
		THEN 'C38216' --RESPIRATORY (INHALATION)
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'In the rectum'
		THEN 'C38295' --RECTAL
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'In the vagina'
		THEN 'C38313' --VAGINAL
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Inject into the penis'
		THEN 'C38235' --INTRACORPORUS CAVERNOSUM
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Inject below the skin'
		THEN 'C38299' --SUBCUTANEOUS
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Intraarterial'
		THEN 'C38299' --SUBCUTANEOUS
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Intraarticular'
		THEN 'C38222' --INTRA-ARTERIAL
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Perfusion'
		THEN 'C48623' --NOT APPLICABLE
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Rinse'
		THEN 'C38281' --IRRIGATION
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Thin layer to the face'
		THEN 'C38676' --PERCUTANEOUS
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Under the lip'
		THEN 'C38305' --TRANSDERMAL
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Under the tongue'
		THEN 'C38193' --BUCCAL
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Via feeding tube'
		THEN 'C38246' --INTRAGASTRIC
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Instill in eyes'
		THEN 'C38255' --INTRAOCULAR
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'On the tongue'
		THEN 'C38193' --BUCCAL
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'Inject into the skin'
		THEN 'C38238' --INTRADERMAL
	WHEN ExternalSystemEntityId = @NciThesaurusExternalSystemIdRouteOfAdministration AND r.Name = 'In the nostrils'
		THEN 'C38284' --NASAL
	ELSE ExternalSystemEntityKey
END AS ExternalSystemEntityKey
FROM CTE 
JOIN model.RouteOfAdministrations r ON r.Id = CTE.PracticeRepositoryEntityKey
END

GO