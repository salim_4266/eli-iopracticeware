﻿IF OBJECT_ID('snomed.sct2_Concept_Snapshot_US1000124_20130901') IS NOT NULL
AND OBJECT_ID('snomed.Concept', 'V') IS NULL
	EXEC sp_rename 'snomed.sct2_Concept_Snapshot_US1000124_20130901', 'Concept'
	
IF OBJECT_ID('snomed.sct2_Concept_Snapshot_US1000124_20130901') IS NOT NULL
AND OBJECT_ID('snomed.Concept', 'V') IS NOT NULL
	EXEC dbo.DropObject 'snomed.sct2_Concept_Snapshot_US1000124_20130901'

IF OBJECT_ID('snomed.sct2_Description_Snapshot-en_US1000124_20130901') IS NOT NULL
AND OBJECT_ID('snomed.Description', 'V') IS NULL
	EXEC sp_rename 'snomed.sct2_Description_Snapshot-en_US1000124_20130901', 'Description'

IF OBJECT_ID('snomed.sct2_Description_Snapshot-en_US1000124_20130901') IS NOT NULL
AND OBJECT_ID('snomed.Description', 'V') IS NOT NULL
	EXEC dbo.DropObject 'snomed.sct2_Description_Snapshot-en_US1000124_20130901'

IF OBJECT_ID('snomed.sct2_Relationship_Snapshot_US1000124_20130901') IS NOT NULL
AND OBJECT_ID('snomed.Relationship', 'V') IS NULL
	EXEC sp_rename 'snomed.sct2_Relationship_Snapshot_US1000124_20130901', 'Relationship'

IF OBJECT_ID('snomed.sct2_Relationship_Snapshot_US1000124_20130901') IS NOT NULL
AND OBJECT_ID('snomed.Relationship', 'V') IS NOT NULL
	EXEC dbo.DropObject 'snomed.sct2_Relationship_Snapshot_US1000124_20130901'

IF OBJECT_ID('snomed.SNOMEDCT_CORE_SUBSET_201308') IS NOT NULL
AND OBJECT_ID('snomed.CORE', 'V') IS NULL
	EXEC sp_rename 'snomed.SNOMEDCT_CORE_SUBSET_201308', 'CORE'

IF OBJECT_ID('snomed.SNOMEDCT_CORE_SUBSET_201308') IS NOT NULL
AND OBJECT_ID('snomed.CORE', 'V') IS NOT NULL
	EXEC dbo.DropObject 'snomed.SNOMEDCT_CORE_SUBSET_201308'

IF OBJECT_ID('snomed.xder2_cciRefset_RefsetDescriptorSnapshot_US1000124_20130601') IS NOT NULL
AND OBJECT_ID('snomed.RefsetDescriptor', 'V') IS NULL
	EXEC sp_rename 'snomed.xder2_cciRefset_RefsetDescriptorSnapshot_US1000124_20130601', 'RefsetDescriptor'
	
IF OBJECT_ID('snomed.xder2_cciRefset_RefsetDescriptorSnapshot_US1000124_20130601') IS NOT NULL
AND OBJECT_ID('snomed.RefsetDescriptor', 'V') IS NOT NULL
	EXEC dbo.DropObject 'snomed.xder2_cciRefset_RefsetDescriptorSnapshot_US1000124_20130601'

IF OBJECT_ID('snomed.xder2_iisssccRefset_ComplexMapSnapshot_US1000124_20130601') IS NOT NULL
AND OBJECT_ID('snomed.ComplexMap', 'V') IS NULL
	EXEC sp_rename 'snomed.xder2_iisssccRefset_ComplexMapSnapshot_US1000124_20130601', 'ComplexMap'

IF OBJECT_ID('snomed.xder2_iisssccRefset_ComplexMapSnapshot_US1000124_20130601') IS NOT NULL
AND OBJECT_ID('snomed.ComplexMap', 'V') IS NOT NULL
	EXEC dbo.DropObject 'snomed.xder2_iisssccRefset_ComplexMapSnapshot_US1000124_20130601'

IF OBJECT_ID('snomed.xder2_ssRefset_ModuleDependencySnapshot_US1000124_20130601') IS NOT NULL 
AND OBJECT_ID('snomed.ModuleDependency', 'V') IS NULL
	EXEC sp_rename 'snomed.xder2_ssRefset_ModuleDependencySnapshot_US1000124_20130601', 'ModuleDependency'

IF OBJECT_ID('snomed.xder2_ssRefset_ModuleDependencySnapshot_US1000124_20130601') IS NOT NULL 
AND OBJECT_ID('snomed.ModuleDependency', 'V') IS NOT NULL
	EXEC dbo.DropObject 'snomed.xder2_ssRefset_ModuleDependencySnapshot_US1000124_20130601'
