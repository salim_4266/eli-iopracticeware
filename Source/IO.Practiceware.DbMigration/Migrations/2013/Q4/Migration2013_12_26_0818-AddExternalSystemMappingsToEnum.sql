﻿IF NOT EXISTS(SELECT * FROM model.ExternalSystems WHERE Id = 6 AND Name = 'Cpt4')
BEGIN

	--Fix for Bug #8816.
	DECLARE @ExternalSystemNames NVARCHAR(MAX);
	DECLARE @Counter INT;
	DECLARE @ExternalSystemName VARCHAR(MAX);
	DECLARE @Delimiter VARCHAR(1);
	DECLARE @NewExternalSystemId INT;
	DECLARE @OldNewExternalSystemId INT;

	SET @Delimiter=',';
	--Dont Change the order of externalsystemNames.Same order is created in the DB and enums are already hardcorded.
	--Add at the end if you want to create a new ExternalSystemName
	SET @ExternalSystemNames = 'Cpt4,CvxVaccineAdministered,Fdb,Icd10,Icd9,IoLegacy,Loinc,NciThesaurus,NewCrop,RxNorm,SnomedCt,UnifiedCodeForUnitsOfMeasure,UnifiedSampleIdentifier,UniqueIngredientIdentifier,CDC,Omedix,ExternalLaboratory';
	SET @Counter = 1;
	SET @NewExternalSystemId=6;

	WHILE @Counter != 0
	BEGIN
		SET @OldNewExternalSystemId=0;
		SET @Counter = CHARINDEX(@Delimiter, @ExternalSystemNames)

		IF @Counter != 0
			SET @ExternalSystemName = LEFT(@ExternalSystemNames, @Counter - 1)
		ELSE
			SET @ExternalSystemName = @ExternalSystemNames

		SELECT TOP 1 @OldNewExternalSystemId= Id FROM Model.ExternalSystems WHERE Name=@ExternalSystemName;

		IF (LEN(@ExternalSystemName) > 0 AND @OldNewExternalSystemId>0)
		BEGIN
			IF(@OldNewExternalSystemId <> @NewExternalSystemId) -- Check if there is already a row present
			BEGIN
				--Insert the new value
				SET IDENTITY_INSERT [model].[ExternalSystems] ON
				INSERT INTO [model].[ExternalSystems]([Id], [Name]) VALUES (@NewExternalSystemId,@ExternalSystemName);
				SET IDENTITY_INSERT [model].[ExternalSystems] OFF
				--Update the entries with the new ExternalSystemId 
				UPDATE model.ExternalSystemEntities SET ExternalSystemId=@NewExternalSystemId WHERE ExternalSystemId=@OldNewExternalSystemId;
				--Update ExternalSystemExternalSystemMessageTypes
				UPDATE model.ExternalSystemExternalSystemMessageTypes SET ExternalSystemId=@NewExternalSystemId  WHERE ExternalSystemId=@OldNewExternalSystemId; 
				--Delete the old ExternalSystemId value
				DELETE FROM model.ExternalSystems WHERE Id=@OldNewExternalSystemId; 
			END
		END
		ELSE
		BEGIN
			SET IDENTITY_INSERT [model].[ExternalSystems] ON
			INSERT INTO [model].[ExternalSystems]([Id], [Name]) VALUES (@NewExternalSystemId,@ExternalSystemName);
			SET IDENTITY_INSERT [model].[ExternalSystems] OFF
		END
		
		SET @ExternalSystemNames = RIGHT(@ExternalSystemNames, LEN(@ExternalSystemNames) - @Counter);

		SET @NewExternalSystemId=@NewExternalSystemId +1;
	END
END