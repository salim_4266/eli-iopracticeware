﻿INSERT INTO model.UnitsOfMeasurement(Abbreviation, FullName, OrdinalId, IsDeactivated, UnitOfMeasurementTypeId)
SELECT 'g/dl' AS abbreviation, 'gram per deciliter'AS fullname, 0 AS OrdinalId, 0 AS IsDeactivated, 96 AS UnitOfMeasurementTypeId UNION ALL
SELECT '10^3/ul' AS abbreviation, 'thousands per microliter'AS fullname, 0 AS OrdinalId, 0 AS IsDeactivated, 96 AS UnitOfMeasurementTypeId