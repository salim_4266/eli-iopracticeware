﻿--Fix for Bug #8862.

--Remove all logcategories already present.
DELETE FROM LogEntryLogCategory WHERE Categories_Id IN(Select Id from LogCategories WHERE NAME ='ExamElements Insert')
DELETE FROM LogCategories WHERE NAME ='ExamElements Insert'
