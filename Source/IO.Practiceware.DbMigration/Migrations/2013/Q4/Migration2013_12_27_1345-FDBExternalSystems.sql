﻿IF NOT EXISTS (SELECT * FROM model.ExternalSystems WHERE Name = 'FDB')
BEGIN 
--Rename FDBDrug to just FDB
	UPDATE model.ExternalSystems
	SET Name = 'FDB'
	WHERE Name = 'FdbDrug'
END

IF NOT EXISTS(SELECT * FROM model.ExternalSystemEntities WHERE ExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'FDB'))
	OR EXISTS (SELECT * FROM model.ExternalSystemEntities WHERE ExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'FDBAllergy'))

BEGIN
--Update ExternalSystemEntities set the allergy row to this new Id
	UPDATE model.ExternalSystemEntities
	SET ExternalSystemId = (SELECT Id FROM model.externalSystems WHERE Name = 'FDB')
	WHERE ExternalSystemId = (SELECT Id FROM model.externalSystems WHERE Name = 'FDBAllergy')
END

GO

--Delete FDBAllergy
DELETE model.ExternalSystems
WHERE Id = (SELECT Id FROM model.externalSystems WHERE Name = 'FDBAllergy')