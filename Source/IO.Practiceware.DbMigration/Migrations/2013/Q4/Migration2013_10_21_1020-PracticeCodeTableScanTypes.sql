﻿--Additional scan types for MU

IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'SCANTYPE' AND Code = 'PatAmReqAccepted')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code, Rank) VALUES ('SCANTYPE', 'PatAmReqAccepted', 99)


IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'SCANTYPE' AND Code = 'PrvAmReqAccepted')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code, Rank) VALUES ('SCANTYPE', 'PrvAmReqAccepted', 99)


IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'SCANTYPE' AND Code = 'PatAmReqDenied')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code, Rank) VALUES ('SCANTYPE', 'PatAmReqDenied', 99)


IF NOT EXISTS (SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'SCANTYPE' AND Code = 'PrvAmReqDenied')
INSERT INTO [PracticeCodeTable] (ReferenceType, Code, Rank) VALUES ('SCANTYPE', 'PrvAmReqDenied', 99)