﻿--PBI #8238.

--Create new Permission for ClinicalDecisions
IF NOT EXISTS(SELECT TOP 1 * FROM model.Permissions Where Name='ClinicalDecisions')
BEGIN
	SET IDENTITY_INSERT [model].[Permissions] ON
	INSERT INTO [model].[Permissions] (Id, Name) VALUES (36, 'ClinicalDecisions')
	SET IDENTITY_INSERT [model].[Permissions] OFF
END
GO

--Create UserPermissions for ClinicalDecision.Deny all by default.
IF NOT EXISTS(SELECT TOP 1 * FROM model.UserPermissions Where PermissionId=36)
BEGIN
	INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)

	SELECT 1 AS IsDenied, 
		   re.ResourceId AS UserId, pe.Id AS PermissionId
	FROM dbo.Resources re
	INNER JOIN model.Permissions pe ON pe.Id > 0
	WHERE ResourceType <> 'R'
	AND pe.Id = 36 
END
GO

--Create New FieldParams for UserGroups
IF NOT EXISTS(SELECT TOP 1 * FROM IE_FieldParams WHERE FldArea='UserGroups' AND FldName='UserGroups')
BEGIN
	INSERT INTO IE_FieldParams(FldArea,FldName,FldDesc,FldType,LookupId) VALUES('UserGroups','UserGroups','USER NAME',1,0)
END
GO
--Create New FieldParams for Medication Allergies
IF NOT EXISTS(SELECT TOP 1 * FROM IE_FieldParams WHERE FldArea='MedAllergies' AND FldName='MedAllergies')
BEGIN
	INSERT INTO IE_FieldParams(FldArea,FldName,FldDesc,FldType,LookupId) VALUES('MedAllergies','MedAllergies','Allergy Name',1,0)
END
GO
--Update FieldParams ICD to Problems
IF EXISTS(SELECT TOP 1 * FROM IE_FieldParams WHERE FldArea='ICD' AND FldName='ICD')
BEGIN
	UPDATE IE_FieldParams SET FldArea='PROBLEMS',FldName='PROBLEMS' WHERE FldArea='ICD' AND FldName='ICD'
END
GO

--Renaming Events
--update all event names to blank
UPDATE IE_EVENTS SET EventName=''
GO
--Rename events When Opening Chart,While Adding ICD to Activate,Deactivate
UPDATE IE_EVENTS SET EventName='Activate' WHERE EventId=1
GO
UPDATE IE_EVENTS SET EventName='Deactivate' WHERE EventId=3
GO