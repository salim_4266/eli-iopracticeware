﻿IF NOT EXISTS (
	SELECT TOP 1 [Id] 
	FROM [model].[ExternalSystemExternalSystemMessageTypes]
	WHERE [ExternalSystemMessageTypeId] = 29 AND [ExternalSystemId] =  1
)
BEGIN
	INSERT INTO [model].[ExternalSystemExternalSystemMessageTypes]
           ([ExternalSystemId]
           ,[ExternalSystemMessageTypeId]
           ,[IsDisabled])
     VALUES
           (1 -- IO Practiceware well-known id
           ,29 -- PatientTransitionOfCareCCDA_Inbound
           ,0)
END