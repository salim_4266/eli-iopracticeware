﻿IF NOT EXISTS (SELECT * FROM model.PracticeRepositoryEntities WHERE Name = 'ClaimFilingIndicatorCode')
BEGIN
	-- Not inserting MaritalStatus or PatientLanguage into PracticeRepositoryEntities because it already exists there
	-- Just inserting ClaimFilingIndicatorCode
	INSERT INTO model.PracticeRepositoryEntities (Id, Name, KeyPropertyName)
	VALUES (12, 'ClaimFilingIndicatorCode', 'Id')
END
GO

IF EXISTS (SELECT * FROM Model.ExternalSystems WHERE Name = 'MVE')
BEGIN
	DECLARE @mveExternalSystemId int
	SET @mveExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'MVE')
	IF NOT EXISTS (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'MaritalStatus')
	BEGIN
		INSERT INTO model.ExternalSystemEntities (ExternalSystemId, Name)
		VALUES (@mveExternalSystemId, 'MaritalStatus')
	END
	IF NOT EXISTS (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'ClaimFilingIndicatorCode')
	BEGIN
		INSERT INTO model.ExternalSystemEntities (ExternalSystemId, Name)
		VALUES (@mveExternalSystemId, 'ClaimFilingIndicatorCode')
	END
	IF NOT EXISTS (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'PatientLanguage')
	BEGIN
		INSERT INTO model.ExternalSystemEntities (ExternalSystemId, Name)
		VALUES (@mveExternalSystemId, 'PatientLanguage')
	END
END
GO

IF EXISTS (SELECT * FROM Model.ExternalSystems WHERE Name = 'MVE')
BEGIN
	DECLARE @mveExternalSystemId int
	SET @mveExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'MVE')

	-- Create mappings from our MaritalStatus values, to MVE MaritalStatus values
	IF EXISTS (SELECT AlternateCode FROM dbo.PracticeCodeTable WHERE ReferenceType = 'MARITAL' AND Code = 'Single' AND AlternateCode IS NOT NULL)
	BEGIN
		DECLARE @externalMaritalStatusEntityId int
		SET @externalMaritalStatusEntityId = (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'MaritalStatus' AND ExternalSystemId = @mveExternalSystemId)

		DECLARE @maritalStatusEntityId int
		SET @maritalStatusEntityId = (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'MaritalStatus')

		INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
		SELECT @maritalStatusEntityId,
				CASE Code
					WHEN 'Single' THEN 1
					WHEN 'Married' THEN 2
					WHEN 'Divorced' THEN 3
					WHEN 'Widowed' THEN 4
				END,
				@externalMaritalStatusEntityId,
				AlternateCode
		FROM dbo.PracticeCodeTable WHERE ReferenceType = 'MARITAL' AND AlternateCode IS NOT NULL
		AND Code IN ('Single','Married','Divorced','Widowed')
	END


	-- Create mappings from our PatientLanguage values to MVE PatientLanguage values
	IF EXISTS (SELECT AlternateCode FROM dbo.PracticeCodeTable WHERE ReferenceType = 'LANGUAGE' AND AlternateCode IS NOT NULL)
	BEGIN
		DECLARE @externalPatientLanguageEntityId int
		SET @externalPatientLanguageEntityId = (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'PatientLanguage' AND ExternalSystemId = @mveExternalSystemId)

		DECLARE @patientLanguageEntityId int
		SET @patientLanguageEntityId = (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'PatientLanguage')

		INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
		SELECT @patientLanguageEntityId,
			   Id,
			   @externalPatientLanguageEntityId,
			   AlternateCode
		FROM dbo.PracticeCodeTable WHERE ReferenceType = 'LANGUAGE' AND AlternateCode IS NOT NULL
	END


	-- Create mappings from our ClaimFilingIndicatorCode values to MVE ClaimFilingIndicatorCode values
	IF EXISTS (SELECT LetterTranslation FROM dbo.PracticeCodeTable WHERE ReferenceType = 'INSURERREF' AND LetterTranslation IS NOT NULL)
	BEGIN
		DECLARE @externalClaimFilingIndicatorCodeEntityId int
		SET @externalClaimFilingIndicatorCodeEntityId = (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'ClaimFilingIndicatorCode' AND ExternalSystemId = @mveExternalSystemId)

		DECLARE @claimFilingIndicatorCodeEntityId int
		SET @claimFilingIndicatorCodeEntityId = (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClaimFilingIndicatorCode')

		INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
		-- ClaimFilingIndicatorCode(7)=HealthMaintenanceOrganizationMedicareRisk
		SELECT @claimFilingIndicatorCodeEntityId, 7, @externalClaimFilingIndicatorCodeEntityId, 907
		UNION ALL
		-- ClaimFilingIndicatorCode(10)=BlueCrossBlueShield
		SELECT @claimFilingIndicatorCodeEntityId, 10, @externalClaimFilingIndicatorCodeEntityId, 1536
		UNION ALL
		-- ClaimFilingIndicatorCode(11)=Champus
		SELECT @claimFilingIndicatorCodeEntityId, 11, @externalClaimFilingIndicatorCodeEntityId, 908
		UNION ALL
		-- ClaimFilingIndicatorCode(12)=CommercialInsurance
		SELECT @claimFilingIndicatorCodeEntityId, 12, @externalClaimFilingIndicatorCodeEntityId, 909
		UNION ALL
		-- ClaimFilingIndicatorCode(14)=FederalEmployeesProgram
		SELECT @claimFilingIndicatorCodeEntityId, 14, @externalClaimFilingIndicatorCodeEntityId, 909
		UNION ALL
		-- ClaimFilingIndicatorCode(18)=MedicarePartB
		SELECT @claimFilingIndicatorCodeEntityId, 18, @externalClaimFilingIndicatorCodeEntityId, 903
		UNION ALL
		-- ClaimFilingIndicatorCode(19)=Medicaid
		SELECT @claimFilingIndicatorCodeEntityId, 19, @externalClaimFilingIndicatorCodeEntityId, 907
		UNION ALL
		-- ClaimFilingIndicatorCode(20)=OtherFederalProgram
		SELECT @claimFilingIndicatorCodeEntityId, 20, @externalClaimFilingIndicatorCodeEntityId, 909
		UNION ALL
		-- ClaimFilingIndicatorCode(21)=TitleV
		SELECT @claimFilingIndicatorCodeEntityId, 21, @externalClaimFilingIndicatorCodeEntityId, 909
		UNION ALL
		-- ClaimFilingIndicatorCode(22)=VeteransAffairsPlan
		SELECT @claimFilingIndicatorCodeEntityId, 22, @externalClaimFilingIndicatorCodeEntityId, 909
		UNION ALL
		-- ClaimFilingIndicatorCode(23)=WorkersCompensation
		SELECT @claimFilingIndicatorCodeEntityId, 23, @externalClaimFilingIndicatorCodeEntityId, 908
		UNION ALL
		-- ClaimFilingIndicatorCode(24)=MutuallyDefined
		SELECT @claimFilingIndicatorCodeEntityId, 24, @externalClaimFilingIndicatorCodeEntityId, 909
	END
END
GO
