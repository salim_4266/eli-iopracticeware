﻿


IF EXISTS(SELECT * FROM sys.views WHERE name = 'PatientPatientRace')
BEGIN
	DROP VIEW model.PatientPatientRace
END

GO

IF OBJECT_ID(N'[model].[FK_PatientPatientRacePatient]', 'F') IS NOT NULL
    ALTER TABLE [model].[PatientPatientRace] DROP CONSTRAINT [FK_PatientPatientRacePatient];
GO

IF OBJECT_ID(N'[model].[PatientPatientRace]', 'U') IS NOT NULL
    DROP TABLE [model].[PatientPatientRace];
GO

IF OBJECT_ID(N'[model].[PatientPatientRacePatientRace]', 'U') IS NOT NULL
    DROP TABLE [model].PatientPatientRacePatientRace;
GO

IF OBJECT_ID(N'[model].[PatientPatientRace]', 'U') IS NULL
BEGIN
		
	-- Creating table 'PatientPatientRaces'
	CREATE TABLE [model].[PatientPatientRace] (		
		[Patients_Id] int  NOT NULL,
		[PatientRaces_Id] int  NOT NULL		

	);


	ALTER TABLE [model].[PatientPatientRace]
	ADD CONSTRAINT [PK_PatientPatientRace]
		PRIMARY KEY NONCLUSTERED ([Patients_Id], [PatientRaces_Id] ASC);
	

	-- Creating foreign key on [PatientId] in table 'PatientPatientRace'
	ALTER TABLE [model].[PatientPatientRace]
	ADD CONSTRAINT [FK_PatientPatientRace_Patient]
		FOREIGN KEY ([Patients_Id])
		REFERENCES [model].[Patients]
			([Id])
		ON DELETE NO ACTION ON UPDATE NO ACTION;

	-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPatientRacePatient'
	CREATE INDEX [IX_FK_PatientPatientRace_Patient]
	ON [model].[PatientPatientRace]
		([Patients_Id]);
	

	-- Creating foreign key on [PatientRaceId] in table 'PatientPatientRace'
	ALTER TABLE [model].[PatientPatientRace]
	ADD CONSTRAINT [FK_PatientPatientRace_PatientRace]
		FOREIGN KEY ([PatientRaces_Id])
		REFERENCES [model].[PatientRaces]
			([Id])
		ON DELETE NO ACTION ON UPDATE NO ACTION;

	-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPatientRacePatientRace'
	CREATE INDEX [IX_FK_PatientPatientRacePatientRace]
	ON [model].[PatientPatientRace]
		([PatientRaces_Id]);


    insert into model.PatientPatientRace (Patients_Id, PatientRaces_Id)
	SELECT Id, PatientRaceId From model.Patients where patientraceid is not null;
END
GO
