﻿IF EXISTS(SELECT * FROM sys.views WHERE name = 'PatientDemographics')
BEGIN
	DROP VIEW dbo.PatientDemographics
END
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Appointments_PatientDemographics]'))
	ALTER TABLE [dbo].Appointments DROP CONSTRAINT [FK_Appointments_PatientDemographics]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Task_PatientDemographics]'))
	ALTER TABLE [dbo].Task DROP CONSTRAINT [FK_Task_PatientDemographics]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Appointments_Patients]'))
	ALTER TABLE [dbo].[Appointments]
	ADD CONSTRAINT [FK_Appointments_Patients]
		FOREIGN KEY ([PatientId])
		REFERENCES [model].[Patients]
			([Id])
		ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

CREATE VIEW [dbo].[PatientDemographics]	WITH VIEW_METADATA
	AS

	SELECT 
		p.Id AS PatientId,
		CASE
			WHEN DefaultUserId IS NULL
				THEN 0
			ELSE
				DefaultUserId
			END AS SchedulePrimaryDoctor,
		ISNULL(FirstName,'') AS FirstName,
		CASE
			WHEN IsClinical = 0
				THEN 'I'
			WHEN PatientStatusId = 2
				THEN 'C'
			WHEN PatientStatusId = 3
				THEN 'D'
			WHEN PatientStatusId = 4
				THEN 'M'
			ELSE
				'A'
			END AS [Status],
		ISNULL(LastName,'') AS LastName,
		ISNULL(MiddleName,'') AS MiddleInitial,
		ISNULL(Prefix,'') AS Salutation,
		ISNULL(Suffix,'') AS NameReference,
		CASE
			WHEN DateOfBirth IS NULL
				THEN CONVERT(nvarchar(64), '')
			ELSE
				DATENAME(yyyy, DateOfBirth) + 
				RIGHT('00' + CONVERT(nvarchar(2), DATEPART(mm, DateOfBirth)), 2) +
				RIGHT ('00' + CONVERT(nvarchar(2), DATEPART(dd, DateOfBirth)), 2)
			END AS BirthDate,
		CASE
			WHEN GenderId = 1
				THEN 'U'
			WHEN GenderId = 2
				THEN 'F'
			WHEN GenderId = 3
				THEN 'M'
			ELSE
				''
			END AS Gender,
		CASE
			WHEN MaritalStatusId = 1
				THEN 'S'
			WHEN MaritalStatusId = 2
				THEN 'M'
			WHEN MaritalStatusId = 3
				THEN 'D'
			WHEN MaritalStatusId = 4
				THEN 'W'
			ELSE
				''
			END AS Marital,
			ISNULL(Occupation,'') AS Occupation,
			ISNULL(PriorPatientCode,'') AS OldPatient,
			'' AS ReferralRequired,
			ISNULL(SocialSecurityNumber,'') AS SocialSecurity,
			ISNULL(EmployerName,'') AS BusinessName,
			CASE 
				WHEN IsHipaaConsentSigned = CONVERT(bit, 1)
					THEN 'T'
				WHEN IsHipaaConsentSigned = CONVERT(bit, 0)
					THEN 'F'
				ELSE
					CONVERT(nvarchar(1), NULL)
				END AS Hipaa,
			pctEthnicity.Code AS Ethnicity,
			pctLanguage.SubStringTenCode AS [Language],
			pctRace.Code AS Race,
			ISNULL(pctEmployment.SubStringOneCode,'') AS BusinessType,
			ISNULL(p.Address,'') As [Address],
			ISNULL(p.Suite,'') As Suite,
			ISNULL(p.City,'') As City,
			ISNULL(p.[State],'') AS [State],
			ISNULL(p.Zip,'') AS Zip,
			ISNULL(p.BusinessAddress,'') AS BusinessAddress,
			ISNULL(p.BusinessSuite,'') AS BusinessSuite,
			ISNULL(p.BusinessCity,'') AS BusinessCity,
			ISNULL(p.BusinessState,'') AS BusinessState,
			ISNULL(p.BusinessZip,'') AS BusinessZip,
			ISNULL(p.HomePhone,'') AS HomePhone,
			ISNULL(p.CellPhone,'') AS CellPhone,
			ISNULL(p.BusinessPhone,'') AS BusinessPhone,
			ISNULL(p.EmployerPhone,'') AS EmployerPhone,
			ISNULL((SELECT TOP 1 COALESCE(ExternalProviderId, 0) FROM model.PatientExternalProviders pep WHERE pep.PatientId = p.Id AND IsReferringPhysician = 1), '') AS ReferringPhysician,
			ISNULL((SELECT TOP 1 COALESCE(ExternalProviderId, 0) FROM model.PatientExternalProviders pep WHERE pep.PatientId = p.Id AND IsPrimaryCarePhysician = 1), '') AS PrimaryCarePhysician,
			ISNULL(p.ProfileComment1,'') AS ProfileComment1,
			ISNULL(p.ProfileComment2,'') AS ProfileComment2,
			ISNULL(p.Email,'') AS Email,
			'' AS FinancialAssignment,
			'' AS FinancialSignature,
			CONVERT(nvarchar(64),'') AS MedicareSecondary,
			CASE
				WHEN p.PreferredAddressId = (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = p.PolicyPatientId ORDER BY pad.Id)
					THEN 'Y'
				ELSE
					'N'
				END AS BillParty,
			'' AS SecondBillParty,
			CONVERT(nvarchar(32),'') AS ReferralCatagory,
			CONVERT(nvarchar(max),'') AS EmergencyName,
			CONVERT(nvarchar(max),'') AS EmergencyPhone,
			'' AS EmergencyRel,
			p.PolicyPatientId,
			p.SecondPolicyPatientId AS SecondPolicyPatientId,
			p.Relationship AS Relationship,
			p.SecondRelationship AS SecondRelationship,
			p.PatType AS PatType,
			CONVERT(int, NULL) AS BillingOffice,
			CONVERT(int, NULL) AS PostOpExpireDate,
			CONVERT(int, NULL) AS PostOpService,
			CONVERT(nvarchar(64), '') AS FinancialSignatureDate,
			'' AS Religion,
			'' AS SecondRelationshipArchived,
			CONVERT(int, NULL) AS SecondPolicyPatientIdArchived,
			'' AS RelationshipArchived,
			CONVERT(int, NULL) AS PolicyPatientIdArchived,
			'' AS BulkMailing,
			'' AS ContactType,
			'' AS NationalOrigin,
			CASE IsStatementSuppressed
				WHEN CONVERT(bit, 0)
					THEN 'Y'
				ELSE
					'N'
				END AS SendStatements,
			Case IsCollectionLetterSuppressed
				WHEN CONVERT(bit, 0)
					THEN 'Y'
				ELSE
					'N'
			END AS SendConsults
	FROM (SELECT *,
		  ISNULL(dbo.GetPolicyHolderPatientId(p1.Id), p1.Id) AS PolicyPatientId,
		  ISNULL(dbo.GetSecondPolicyHolderPatientId(p1.Id),0) AS SecondPolicyPatientId,
		  ISNULL(dbo.GetPolicyHolderRelationshipType(p1.Id),'Y') AS Relationship,
		  ISNULL(dbo.GetSecondPolicyHolderRelationshipType(p1.Id),'') AS SecondRelationship,
		  (SELECT TOP 1 ExchangeAndSuffix FROM model.PatientPhoneNumbers ppn WHERE ppn.PatientId = p1.Id AND ppn.PatientPhoneNumberTypeId = 7 AND ppn.OrdinalId = 1) As HomePhone,
		  (SELECT TOP 1 ExchangeAndSuffix FROM model.PatientPhoneNumbers ppn WHERE ppn.PatientId = p1.Id AND ppn.PatientPhoneNumberTypeId = 3 AND ppn.OrdinalId = 1) AS CellPhone,
		  (SELECT TOP 1 ExchangeAndSuffix FROM model.PatientPhoneNumbers ppn WHERE ppn.PatientId = p1.Id AND ppn.PatientPhoneNumberTypeId = 2 AND ppn.OrdinalId = 1) AS BusinessPhone,
		  (SELECT TOP 1 ExchangeAndSuffix FROM model.PatientPhoneNumbers ppn WHERE ppn.PatientId = p1.Id AND ppn.OrdinalId = 1 AND ppn.PatientPhoneNumberTypeId IN (SELECT Id FROM dbo.PracticeCodeTable pct WHERE pct.Code = 'Employer' AND ReferenceType = 'PhoneTypePatient')) AS EmployerPhone,
		  (SELECT TOP 1 Line1 FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS [Address],
		  (SELECT TOP 1 Line2 FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS Suite,
		  (SELECT TOP 1 City FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS City,
		  (SELECT TOP 1 Abbreviation FROM model.StateOrProvinces sp WHERE sp.Id = (SELECT TOP 1 StateOrProvinceId FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1)) AS [State],
		  (SELECT TOP 1 PostalCode FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 1 AND pa.OrdinalId = 1) AS Zip,
		  (SELECT TOP 1 Line1 FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1) AS BusinessAddress,
		  (SELECT TOP 1 Line2 FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1) AS BusinessSuite,
		  (SELECT TOP 1 City FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1) AS BusinessCity,
		  (SELECT TOP 1 Abbreviation FROM model.StateOrProvinces sp WHERE sp.Id = (SELECT TOP 1 StateOrProvinceId FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1)) AS BusinessState,
		  (SELECT TOP 1 PostalCode FROM model.PatientAddresses pa WHERE pa.PatientId = p1.Id AND pa.PatientAddressTypeId = 2 AND pa.OrdinalId = 1) AS BusinessZip,
		  (SELECT TOP 1 Value FROM model.PatientEmailAddresses pea WHERE pea.PatientId = p1.Id AND pea.OrdinalId = 1) AS Email,
		  (SELECT TOP 1 Value FROM model.PatientComments pc WHERE pc.PatientCommentTypeId = 2 AND pc.PatientId = p1.Id ORDER BY pc.Id DESC) AS ProfileComment1,
		  (SELECT TOP 1 Comment FROM model.PatientReferralSources rs WHERE rs.PatientId = p1.Id AND rs.Comment IS NOT NULL) AS ProfileComment2,
		  (SELECT TOP 1 COALESCE(ExternalProviderId, 0) FROM model.PatientExternalProviders pep WHERE pep.Id = p1.Id) AS ReferringPhysician,
		  (SELECT TOP 1 PatientAddressId FROM model.PatientCommunicationPreferences pcp WHERE pcp.PatientId = p1.Id AND PatientCommunicationTypeId = 3) AS PreferredAddressId,
		  (SELECT TOP 1 DisplayName FROM model.Tags pt WHERE pt.Id = (SELECT TOP 1 TagId FROM model.PatientTags ppt WHERE ppt.PatientId = p1.Id AND ppt.OrdinalId = 1)) AS PatType
		FROM model.Patients p1) p
		LEFT JOIN dbo.PracticeCodeTable pctEthnicity ON pctEthnicity.Id = PatientEthnicityId
		LEFT JOIN dbo.PracticeCodeTable pctLanguage ON pctLanguage.Id = PatientLanguageId
		LEFT JOIN dbo.PracticeCodeTable pctRace ON pctRace.Id = PatientRaceId
		LEFT JOIN dbo.PracticeCodeTable pctEmployment ON pctEmployment.Id = p.PatientEmploymentStatusId


GO


CREATE TRIGGER dbo.OnPatientDemographicDelete ON [dbo].[PatientDemographics] INSTEAD OF DELETE
	AS 
	BEGIN
	
	SET NOCOUNT ON

	DECLARE @PatientId int
	DECLARE @SchedulePrimaryDoctor int
	DECLARE @FirstName nvarchar(max)
	DECLARE @Status nvarchar(1)
	DECLARE @LastName nvarchar(max)
	DECLARE @MiddleInitial nvarchar(max)
	DECLARE @Salutation nvarchar(max)
	DECLARE @NameReference nvarchar(max)
	DECLARE @BirthDate nvarchar(90)
	DECLARE @Gender nvarchar(1)
	DECLARE @Marital nvarchar(1)
	DECLARE @Occupation nvarchar(max)
	DECLARE @OldPatient nvarchar(max)
	DECLARE @ReferralRequired nvarchar(1)
	DECLARE @SocialSecurity nvarchar(max)
	DECLARE @BusinessName nvarchar(max)
	DECLARE @Hipaa nvarchar(1)
	DECLARE @Ethnicity nvarchar(64)
	DECLARE @Language nvarchar(64)
	DECLARE @Race nvarchar(64)
	DECLARE @BusinessType nvarchar(1)
	DECLARE @Address nvarchar(max)
	DECLARE @Suite nvarchar(max)
	DECLARE @City nvarchar(max)
	DECLARE @State nvarchar(max)
	DECLARE @Zip nvarchar(max)
	DECLARE @BusinessAddress nvarchar(max)
	DECLARE @BusinessSuite nvarchar(max)
	DECLARE @BusinessCity nvarchar(max)
	DECLARE @BusinessState nvarchar(max)
	DECLARE @BusinessZip nvarchar(max)
	DECLARE @HomePhone nvarchar(max)
	DECLARE @CellPhone nvarchar(max)
	DECLARE @BusinessPhone nvarchar(max)
	DECLARE @EmployerPhone nvarchar(max)
	DECLARE @ReferringPhysician int
	DECLARE @PrimaryCarePhysician int
	DECLARE @ProfileComment1 nvarchar(max)
	DECLARE @ProfileComment2 nvarchar(max)
	DECLARE @Email nvarchar(max)
	DECLARE @EmergencyPhone nvarchar(max) 
	DECLARE @EmergencyRel nvarchar(1)
	DECLARE @FinancialAssignment nvarchar(1)
	DECLARE @FinancialSignature nvarchar(1)
	DECLARE @MedicareSecondary nvarchar(64)
	DECLARE @BillParty nvarchar(1)
	DECLARE @SecondBillParty nvarchar(1)
	DECLARE @ReferralCatagory nvarchar(max)
	DECLARE @EmergencyName nvarchar(max)
	DECLARE @PolicyPatientId int
	DECLARE @SecondPolicyPatientId int
	DECLARE @Relationship nvarchar(1)
	DECLARE @SecondRelationship nvarchar(1)
	DECLARE @PatType nvarchar(max)
	DECLARE @BillingOffice int
	DECLARE @PostOpExpireDate int
	DECLARE @PostOpService int
	DECLARE @FinancialSignatureDate nvarchar(64)
	DECLARE @Religion nvarchar(1)
	DECLARE @SecondRelationshipArchived varchar(1)
	DECLARE @SecondPolicyPatientIdArchived int
	DECLARE @RelationshipArchived nvarchar(1)
	DECLARE @PolicyPatientIdArchived int
	DECLARE @BulkMailing nvarchar(1)
	DECLARE @ContactType nvarchar(1)
	DECLARE @NationalOrigin nvarchar(1)
	DECLARE @SendStatements nvarchar(1)
	DECLARE @SendConsults nvarchar(1)
	
	DECLARE PatientDemographicsDeletedCursor CURSOR FOR
	SELECT PatientId, SchedulePrimaryDoctor, FirstName, Status, LastName, MiddleInitial, Salutation, NameReference, BirthDate, Gender, Marital, Occupation, OldPatient, ReferralRequired, SocialSecurity, BusinessName, Hipaa, Ethnicity, Language, Race, BusinessType, Address, Suite, City, State, Zip, BusinessAddress, BusinessSuite, BusinessCity, BusinessState, BusinessZip, HomePhone, CellPhone, BusinessPhone, EmployerPhone, ReferringPhysician, PrimaryCarePhysician, ProfileComment1, ProfileComment2, Email, EmergencyPhone, EmergencyRel, FinancialAssignment, FinancialSignature, MedicareSecondary, BillParty, SecondBillParty, ReferralCatagory, EmergencyName, PolicyPatientId, SecondPolicyPatientId, Relationship, SecondRelationship, PatType, BillingOffice, PostOpExpireDate, PostOpService, FinancialSignatureDate, Religion, SecondRelationshipArchived, SecondPolicyPatientIdArchived, RelationshipArchived, PolicyPatientIdArchived, BulkMailing, ContactType, NationalOrigin, SendStatements, SendConsults FROM deleted
		
	OPEN PatientDemographicsDeletedCursor
	FETCH NEXT FROM PatientDemographicsDeletedCursor INTO @PatientId, @SchedulePrimaryDoctor, @FirstName, @Status, @LastName, @MiddleInitial, @Salutation, @NameReference, @BirthDate, @Gender, @Marital, @Occupation, @OldPatient, @ReferralRequired, @SocialSecurity, @BusinessName, @Hipaa, @Ethnicity, @Language, @Race, @BusinessType, @Address, @Suite, @City, @State, @Zip, @BusinessAddress, @BusinessSuite, @BusinessCity, @BusinessState, @BusinessZip, @HomePhone, @CellPhone, @BusinessPhone, @EmployerPhone, @ReferringPhysician, @PrimaryCarePhysician, @ProfileComment1, @ProfileComment2, @Email, @EmergencyPhone, @EmergencyRel, @FinancialAssignment, @FinancialSignature, @MedicareSecondary, @BillParty, @SecondBillParty, @ReferralCatagory, @EmergencyName, @PolicyPatientId, @SecondPolicyPatientId, @Relationship, @SecondRelationship, @PatType, @BillingOffice, @PostOpExpireDate, @PostOpService, @FinancialSignatureDate, @Religion, @SecondRelationshipArchived, @SecondPolicyPatientIdArchived, @RelationshipArchived, @PolicyPatientIdArchived, @BulkMailing, @ContactType, @NationalOrigin, @SendStatements, @SendConsults
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		DELETE FROM model.PatientTags WHERE PatientId = @PatientId
		DELETE FROM model.PatientCommunicationPreferences WHERE PatientId = @PatientId
		DELETE FROM model.PatientReferralSources WHERE PatientId = @PatientId
		DELETE FROM model.PatientComments WHERE PatientId = @PatientId
		DELETE FROM model.PatientExternalProviders WHERE PatientId = @PatientId
		DELETE FROM model.PatientPhoneNumbers WHERE PatientId = @PatientId
		DELETE FROM model.PatientEmailAddresses WHERE PatientId = @PatientId	
		DELETE FROM model.PatientAddresses WHERE PatientId = @PatientId
		DELETE FROM model.Patients WHERE Id = @PatientId
		
		FETCH NEXT FROM PatientDemographicsDeletedCursor INTO @PatientId, @SchedulePrimaryDoctor, @FirstName, @Status, @LastName, @MiddleInitial, @Salutation, @NameReference, @BirthDate, @Gender, @Marital, @Occupation, @OldPatient, @ReferralRequired, @SocialSecurity, @BusinessName, @Hipaa, @Ethnicity, @Language, @Race, @BusinessType, @Address, @Suite, @City, @State, @Zip, @BusinessAddress, @BusinessSuite, @BusinessCity, @BusinessState, @BusinessZip, @HomePhone, @CellPhone, @BusinessPhone, @EmployerPhone, @ReferringPhysician, @PrimaryCarePhysician, @ProfileComment1, @ProfileComment2, @Email, @EmergencyPhone, @EmergencyRel, @FinancialAssignment, @FinancialSignature, @MedicareSecondary, @BillParty, @SecondBillParty, @ReferralCatagory, @EmergencyName, @PolicyPatientId, @SecondPolicyPatientId, @Relationship, @SecondRelationship, @PatType, @BillingOffice, @PostOpExpireDate, @PostOpService, @FinancialSignatureDate, @Religion, @SecondRelationshipArchived, @SecondPolicyPatientIdArchived, @RelationshipArchived, @PolicyPatientIdArchived, @BulkMailing, @ContactType, @NationalOrigin, @SendStatements, @SendConsults
	END
		
	CLOSE PatientDemographicsDeletedCursor
	DEALLOCATE PatientDemographicsDeletedCursor
		
	SET NOCOUNT OFF

	END

GO

CREATE TRIGGER dbo.OnPatientDemographicInsertAndUpdate ON [dbo].[PatientDemographics] INSTEAD OF INSERT, UPDATE
	AS 
	BEGIN
	
	SET NOCOUNT ON

	DECLARE @PatientId int
	DECLARE @SchedulePrimaryDoctor int
	DECLARE @FirstName nvarchar(max)
	DECLARE @Status nvarchar(1)
	DECLARE @LastName nvarchar(max)
	DECLARE @MiddleInitial nvarchar(max)
	DECLARE @Salutation nvarchar(max)
	DECLARE @NameReference nvarchar(max)
	DECLARE @BirthDate nvarchar(90)
	DECLARE @Gender nvarchar(1)
	DECLARE @Marital nvarchar(1)
	DECLARE @Occupation nvarchar(max)
	DECLARE @OldPatient nvarchar(max)
	DECLARE @ReferralRequired nvarchar(1)
	DECLARE @SocialSecurity nvarchar(max)
	DECLARE @BusinessName nvarchar(max)
	DECLARE @Hipaa nvarchar(1)
	DECLARE @Ethnicity nvarchar(64)
	DECLARE @Language nvarchar(64) 
	DECLARE @Race nvarchar(64)
	DECLARE @BusinessType nvarchar(1)
	DECLARE @Address nvarchar(max)
	DECLARE @Suite nvarchar(max)
	DECLARE @City nvarchar(max)
	DECLARE @State nvarchar(max)
	DECLARE @Zip nvarchar(max)
	DECLARE @BusinessAddress nvarchar(max)
	DECLARE @BusinessSuite nvarchar(max)
	DECLARE @BusinessCity nvarchar(max)
	DECLARE @BusinessState nvarchar(max)
	DECLARE @BusinessZip nvarchar(max)
	DECLARE @HomePhone nvarchar(max)
	DECLARE @CellPhone nvarchar(max)
	DECLARE @BusinessPhone nvarchar(max)
	DECLARE @EmployerPhone nvarchar(max)
	DECLARE @ReferringPhysician int
	DECLARE @PrimaryCarePhysician int
	DECLARE @ProfileComment1 nvarchar(max)
	DECLARE @ProfileComment2 nvarchar(max)
	DECLARE @Email nvarchar(max)
	DECLARE @EmergencyPhone nvarchar(max) 
	DECLARE @EmergencyRel nvarchar(1)
	DECLARE @FinancialAssignment nvarchar(1)
	DECLARE @FinancialSignature nvarchar(1)
	DECLARE @MedicareSecondary nvarchar(64)
	DECLARE @BillParty nvarchar(1)
	DECLARE @SecondBillParty nvarchar(1)
	DECLARE @ReferralCatagory nvarchar(max)
	DECLARE @EmergencyName nvarchar(max)
	DECLARE @PolicyPatientId int
	DECLARE @SecondPolicyPatientId int
	DECLARE @Relationship nvarchar(1)
	DECLARE @SecondRelationship nvarchar(1)
	DECLARE @PatType nvarchar(max)
	DECLARE @BillingOffice int
	DECLARE @PostOpExpireDate int
	DECLARE @PostOpService int
	DECLARE @FinancialSignatureDate nvarchar(64)
	DECLARE @Religion nvarchar(1)
	DECLARE @SecondRelationshipArchived varchar(1)
	DECLARE @SecondPolicyPatientIdArchived int
	DECLARE @RelationshipArchived nvarchar(1)
	DECLARE @PolicyPatientIdArchived int
	DECLARE @BulkMailing nvarchar(1)
	DECLARE @ContactType nvarchar(1)
	DECLARE @NationalOrigin nvarchar(1)
	DECLARE @SendStatements nvarchar(1)
	DECLARE @SendConsults nvarchar(1)
	
	DECLARE PatientDemographicsInsertedUpdatedCursor CURSOR FOR
	SELECT PatientId, SchedulePrimaryDoctor, FirstName, Status, LastName, MiddleInitial, Salutation, NameReference, BirthDate, Gender, Marital, Occupation, OldPatient, ReferralRequired, SocialSecurity, BusinessName, Hipaa, Ethnicity, Language, Race, BusinessType, Address, Suite, City, State, Zip, BusinessAddress, BusinessSuite, BusinessCity, BusinessState, BusinessZip, HomePhone, CellPhone, BusinessPhone, EmployerPhone, ReferringPhysician, PrimaryCarePhysician, ProfileComment1, ProfileComment2, Email, EmergencyPhone, EmergencyRel, FinancialAssignment, FinancialSignature, MedicareSecondary, BillParty, SecondBillParty, ReferralCatagory, EmergencyName, PolicyPatientId, SecondPolicyPatientId, Relationship, SecondRelationship, PatType, BillingOffice, PostOpExpireDate, PostOpService, FinancialSignatureDate, Religion, SecondRelationshipArchived, SecondPolicyPatientIdArchived, RelationshipArchived, PolicyPatientIdArchived, BulkMailing, ContactType, NationalOrigin, SendStatements, SendConsults FROM inserted
		
	OPEN PatientDemographicsInsertedUpdatedCursor
	FETCH NEXT FROM PatientDemographicsInsertedUpdatedCursor INTO @PatientId, @SchedulePrimaryDoctor, @FirstName, @Status, @LastName, @MiddleInitial, @Salutation, @NameReference, @BirthDate, @Gender, @Marital, @Occupation, @OldPatient, @ReferralRequired, @SocialSecurity, @BusinessName, @Hipaa, @Ethnicity, @Language, @Race, @BusinessType, @Address, @Suite, @City, @State, @Zip, @BusinessAddress, @BusinessSuite, @BusinessCity, @BusinessState, @BusinessZip, @HomePhone, @CellPhone, @BusinessPhone, @EmployerPhone, @ReferringPhysician, @PrimaryCarePhysician, @ProfileComment1, @ProfileComment2, @Email, @EmergencyPhone, @EmergencyRel, @FinancialAssignment, @FinancialSignature, @MedicareSecondary, @BillParty, @SecondBillParty, @ReferralCatagory, @EmergencyName, @PolicyPatientId, @SecondPolicyPatientId, @Relationship, @SecondRelationship, @PatType, @BillingOffice, @PostOpExpireDate, @PostOpService, @FinancialSignatureDate, @Religion, @SecondRelationshipArchived, @SecondPolicyPatientIdArchived, @RelationshipArchived, @PolicyPatientIdArchived, @BulkMailing, @ContactType, @NationalOrigin, @SendStatements, @SendConsults
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE @employmentStatusId AS int
		SET @employmentStatusId = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'EmploymentStatus' AND @BusinessType = pct.SubStringOneCode)

		DECLARE @maritalStatusId AS int
		SET @maritalStatusId = CASE @Marital
				WHEN 'S'
					THEN 1
				WHEN 'M'
					THEN 2
				WHEN 'D'
					THEN 3
				WHEN 'W'
					THEN 4
				ELSE
					NULL
				END

		DECLARE @genderId AS int
		SET @genderId = CASE @Gender
				WHEN 'U'
					THEN 1
				WHEN 'F'
					THEN 2
				WHEN 'M'
					THEN 3
				ELSE
					4
				END

		DECLARE @languageId AS int
		SET @languageId = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'Language' AND pct.SubStringTenCode = @Language)

		DECLARE @defaultUserId AS int
		SET @defaultUserId = CASE @SchedulePrimaryDoctor
			WHEN 0
				THEN NULL
			ELSE
				@SchedulePrimaryDoctor
			END

		DECLARE @isClinical AS bit
		SET @isClinical = CASE @Status
			WHEN 'I'
				THEN CONVERT(bit, 0)
			ELSE
				CONVERT(bit, 1)
			END

		DECLARE @patientStatusId AS int
		SET @patientStatusId = CASE @Status
			WHEN 'C'
				THEN 2
			WHEN 'D'
				THEN 3
			WHEN 'M'
				THEN 4
			ELSE
				1
			END

		DECLARE @isHipaaConsentSigned AS bit
		SET @isHipaaConsentSigned = CASE @Hipaa
			WHEN 'T'
				THEN CONVERT(bit, 1)
			ELSE
				CONVERT(bit, 0)			
			END

		DECLARE @isStatementSuppressed AS bit
		SET @isStatementSuppressed = CASE @SendStatements
			WHEN 'Y'
				THEN CONVERT(bit, 0)
			ELSE
				CONVERT(bit, 1)
			END

		DECLARE @isCollectionLetterSuppressed AS bit
		SET @isCollectionLetterSuppressed = CASE @SendConsults
			WHEN 'Y'
				THEN CONVERT(bit, 0)
			ELSE
				CONVERT(bit, 1)
			END

		DECLARE @patientRaceId AS int
		SET @patientRaceId = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'Race' AND pct.Code = @Race)

		DECLARE @patientEthnicityId AS int
		SET @patientEthnicityId = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.ReferenceType = 'Ethnicity' AND pct.Code = @Ethnicity)

		DECLARE @releaseOfInformationCodeId AS int
		SET @releaseOfInformationCodeId = CASE @FinancialSignature
			WHEN 'Y'
				THEN 1
			ELSE
				0
			END

		DECLARE @dateOfBirth AS datetime
		SET @dateOfBirth = CONVERT(datetime, @BirthDate, 112)

		DECLARE @releaseOfInformationDateTime AS datetime
		SET @releaseOfInformationDateTime = CONVERT(datetime, @FinancialSignatureDate, 112)

		IF (COALESCE(@PatientId, 0) = 0)
		BEGIN
			INSERT INTO model.Patients (LastName, 
											FirstName, 
											MiddleName, 
											Suffix, 
											SocialSecurityNumber, 
											Occupation, 
											EmployerName, 
											PatientEmploymentStatusId, 
											GenderId, 
											MaritalStatusId, 
											DateOfBirth,
											PatientLanguageId,
											DefaultUserId,
											ReleaseOfInformationCodeId,
											ReleaseOfInformationDateTime,
											IsClinical,
											PatientStatusId,
											Prefix,
											IsHipaaConsentSigned,
											IsStatementSuppressed,
											IsCollectionLetterSuppressed,
											PriorPatientCode,
											PatientRaceId,
											PatientEthnicityId)
				VALUES (@LastName,
						@FirstName,
						@MiddleInitial,
						@NameReference,
						@SocialSecurity,
						@Occupation,
						@BusinessName,
						@employmentStatusId,
						@genderId,
						@maritalStatusId,
						@dateOfBirth,
						@languageId,
						@defaultUserId,
						@releaseOfInformationCodeId,
						@releaseOfInformationDateTime,
						@isClinical,
						@patientStatusId,
						@Salutation,
						@isHipaaConsentSigned,
						@isStatementSuppressed,
						@isCollectionLetterSuppressed,
						@OldPatient,
						@patientRaceId,
						@patientEthnicityId)

				SET @PatientId = IDENT_CURRENT('model.Patients')
		END
		ELSE
		BEGIN
			UPDATE model.Patients
			SET 
				LastName = @LastName,
				FirstName = @FirstName,
				MiddleName = @MiddleInitial,
				Suffix = @NameReference,
				SocialSecurityNumber = @SocialSecurity,
				Occupation = @Occupation,
				EmployerName = @BusinessName,
				PatientEmploymentStatusId = @employmentStatusId,
				GenderId = @genderId,
				MaritalStatusId = @maritalStatusId,
				DateOfBirth = @dateOfBirth,
				PatientLanguageId = @languageId,
				DefaultUserId = @defaultUserId,
				IsClinical = @isClinical,
				PatientStatusId = @patientStatusId,
				Prefix = @Salutation,
				IsHipaaConsentSigned = @isHipaaConsentSigned,
				IsStatementSuppressed = @isStatementSuppressed,
				IsCollectionLetterSuppressed = @isCollectionLetterSuppressed,
				PriorPatientCode = @OldPatient,
				PatientRaceId = @patientRaceId,
				PatientEthnicityId = @patientEthnicityId
			WHERE Id = @PatientId
		END
		
		---- Update or insert Primary PatientEmailAddress
		IF (COALESCE(@Email, '') <> '')
		BEGIN
			IF EXISTS (SELECT * FROM model.PatientEmailAddresses WHERE PatientId = @PatientId AND OrdinalId = 1 AND EmailAddressTypeId = 1)
			BEGIN
				UPDATE model.PatientEmailAddresses
				SET
					Value = @Email
				WHERE PatientId = @PatientId AND OrdinalId = 1 AND EmailAddressTypeId = 1
			END
			ELSE
			BEGIN
				INSERT INTO model.PatientEmailAddresses (Value, OrdinalId, PatientId, EmailAddressTypeId)
				VALUES (@Email, 1, @PatientId, 1)
			END
		END
		ELSE
		BEGIN
			DELETE FROM model.PatientEmailAddresses
			WHERE PatientId = @PatientId AND OrdinalId = 1 AND EmailAddressTypeId = 1
		END


		DECLARE @homeAddressId AS int
		SET @homeAddressId = (SELECT TOP 1 Id FROM model.PatientAddresses pa WHERE pa.PatientId = @PatientId AND pa.PatientAddressTypeId = 1)

		---- If no home address exists, detect whether any home address values are set, and add a new home address if possible
		DECLARE @postalCode AS nvarchar(max)
		DECLARE @stateOrProvinceId AS int
		SET @stateOrProvinceId = (SELECT TOP 1 Id FROM model.StateOrProvinces st WHERE st.Abbreviation = @State)
		IF (COALESCE(@homeAddressId, 0) = 0)
		BEGIN
			IF ((COALESCE(@Address, '') <> '') OR 
				(COALESCE(@Suite, '') <> '') OR 
				(COALESCE(@City, '') <> '') OR 
				(COALESCE(@State, '') <> '') OR 
				(COALESCE(@Zip, '') <> ''))
			BEGIN
				SET @postalCode = REPLACE(@Zip,'-','')
				INSERT INTO model.PatientAddresses (Line1, Line2, City, PatientId, PostalCode, StateOrProvinceId, PatientAddressTypeId, OrdinalId)
				VALUES (@Address, @Suite, @City, @PatientId, @postalCode, @stateOrProvinceId, 1, 1)
			END
		END
		---- Otherwise, update the existing home address values
		ELSE
		BEGIN
			SET @postalCode = REPLACE(@Zip, '-', '')
			UPDATE model.PatientAddresses
			SET
				Line1 = @Address,
				Line2 = @Suite,
				City = @City,
				PostalCode = @postalCode,
				StateOrProvinceId = @stateOrProvinceId
			WHERE Id = @homeAddressId
		END

		DECLARE @businessAddressId AS int
		SET @businessAddressId = (SELECT TOP 1 Id FROM model.PatientAddresses pa WHERE pa.PatientId = @PatientId AND pa.PatientAddressTypeId = 2)

		---- If no business address exists, detect whether any business address values are set, and add a new business address if possible
		SET @postalCode = REPLACE(@BusinessZip, '-','')
		SET @stateOrProvinceId = (SELECT TOP 1 Id FROM model.StateOrProvinces st WHERE st.Abbreviation = @BusinessState)
		IF(COALESCE(@businessAddressId, 0) = 0)
		BEGIN
			IF ((COALESCE(@BusinessAddress, '') <> '') OR
				(COALESCE(@BusinessSuite, '') <> '') OR
				(COALESCE(@BusinessCity, '') <> '') OR
				(COALESCE(@BusinessState, '') <> '') OR
				(COALESCE(@BusinessZip, '') <> ''))
			BEGIN
				INSERT INTO model.PatientAddresses (Line1, Line2, City, PatientId, PostalCode, StateOrProvinceId, PatientAddressTypeId, OrdinalId)
				VALUES (@BusinessAddress, @BusinessSuite, @BusinessCity, @PatientId, @postalCode, @stateOrProvinceId, 2, 2)
			END
		END
		---- Otherwise, update the existing home address values
		ELSE
		BEGIN
			UPDATE model.PatientAddresses
			SET
				Line1 = @BusinessAddress,
				Line2 = @BusinessSuite,
				City = @BusinessCity,
				PostalCode = @postalCode,
				StateOrProvinceId = @stateOrProvinceId
			WHERE Id = @businessAddressId
		END


		---- Update or insert Primary PatientEmailAddress
		IF (COALESCE(@Email, '') <> '')
		BEGIN
			UPDATE model.PatientEmailAddresses
			SET
				Value = @Email
			WHERE PatientId = @PatientId AND OrdinalId = 1 AND EmailAddressTypeId = 1
		END
		ELSE
		BEGIN
			DELETE FROM model.PatientEmailAddresses
			WHERE PatientId = @PatientId AND OrdinalId = 1 AND EmailAddressTypeId = 1
		END

		DECLARE @homePhoneNumberId AS int
		SET @homePhoneNumberId = (SELECT TOP 1 Id FROM model.PatientPhoneNumbers pn WHERE pn.PatientId = @PatientId AND pn.PatientPhoneNumberTypeId = 7)

		---- Insert new Home PatientPhoneNumber if possible
		IF(COALESCE(@homePhoneNumberId, 0) = 0)
		BEGIN
			IF (COALESCE(@HomePhone, '') <> '')
			BEGIN
				INSERT INTO model.PatientPhoneNumbers (ExchangeAndSuffix, IsInternational, OrdinalId, PatientId, PatientPhoneNumberTypeId)
				VALUES (@HomePhone, CONVERT(bit, 0), 1, @PatientId, 7)
			END
		END
		---- Otherwise update Home PatientPhoneNumber
		ELSE
		BEGIN
			IF(COALESCE(@HomePhone, '') <> '')
			BEGIN
				UPDATE model.PatientPhoneNumbers
				SET
					ExchangeAndSuffix = @HomePhone
				WHERE PatientId = @PatientId AND PatientPhoneNumberTypeId = 7 AND OrdinalId = 1
			END
			ELSE
			BEGIN
				DELETE FROM model.PatientPhoneNumbers
				WHERE PatientId = @PatientId AND PatientPhoneNumberTypeId = 7 AND OrdinalId = 1
			END
		END

		DECLARE @cellPhoneNumberId AS int
		SET @cellPhoneNumberId = (SELECT TOP 1 Id FROM model.PatientPhoneNumbers pn WHERE pn.PatientId = @PatientId AND pn.PatientPhoneNumberTypeId = 3)

		---- Insert new Cell PatientPhoneNumber if possible
		IF (COALESCE(@cellPhoneNumberId, 0) = 0)
		BEGIN
			IF (COALESCE(@CellPhone, '') <> '')
			BEGIN
				INSERT INTO model.PatientPhoneNumbers (ExchangeAndSuffix, IsInternational, OrdinalId, PatientId, PatientPhoneNumberTypeId)
				VALUES (@CellPhone, CONVERT(bit, 0), 3, @PatientId, 3)
			END
		END
		---- Otherwise update Cell PatientPhoneNumber
		ELSE
		BEGIN
			IF (COALESCE(@CellPhone, '') <> '')
			BEGIN
				UPDATE model.PatientPhoneNumbers
				SET
					ExchangeAndSuffix = @CellPhone
				WHERE PatientId = @PatientId AND PatientPhoneNumberTypeId = 3 AND OrdinalId = 3
			END
			ELSE
			BEGIN
				DELETE FROM model.PatientPhoneNumbers
				WHERE PatientId = @PatientId AND PatientPhoneNumberTypeId = 7 AND OrdinalId = 3
			END
		END

		DECLARE @businessPhoneNumberId AS int
		SET @businessPhoneNumberId = (SELECT TOP 1 Id FROM model.PatientPhoneNumbers pn WHERE pn.PatientId = @PatientId AND pn.PatientPhoneNumberTypeId = 2)

		---- Insert new Business PatientPhoneNumber if possible
		IF(COALESCE(@businessPhoneNumberId, 0) = 0)
		BEGIN
			IF (COALESCE(@BusinessPhone, '') <> '')
			BEGIN
				INSERT INTO model.PatientPhoneNumbers (ExchangeAndSuffix, IsInternational, OrdinalId, PatientId, PatientPhoneNumberTypeId)
				VALUES (@BusinessPhone, CONVERT(bit, 0), 2, @PatientId, 2)
			END
		END
		---- Otherwise update Business PatientPhoneNumber
		ELSE
		BEGIN
			IF (COALESCE(@BusinessPhone, '') <> '')
			BEGIN
				UPDATE model.PatientPhoneNumbers
				SET
					ExchangeAndSuffix = @BusinessPhone
				WHERE PatientId = @PatientId AND PatientPhoneNumberTypeId = 2 AND OrdinalId = 2
			END
			ELSE
			BEGIN
				DELETE FROM model.PatientPhoneNumbers
				WHERE PatientId = @PatientId AND PatientPhoneNumberTypeId = 2 AND OrdinalId = 2
			END
		END

		IF EXISTS (SELECT * FROM model.ExternalContacts WHERE Id = @ReferringPhysician)
		BEGIN
			DECLARE @referringPhysicianExternalProviderId AS int
			SET @referringPhysicianExternalProviderId = (SELECT TOP 1 Id FROM model.PatientExternalProviders pep WHERE pep.Id = @PatientId)

			---- Insert new ReferringPhysician PatientExternalProvider if possible
			IF (COALESCE(@referringPhysicianExternalProviderId, 0) = 0)
			BEGIN
				SET IDENTITY_INSERT [model].[PatientExternalProviders] ON
					INSERT INTO model.PatientExternalProviders (Id, ExternalProviderId, IsPrimaryCarePhysician, IsReferringPhysician, PatientId)
					VALUES (@PatientId, @ReferringPhysician, CONVERT(bit, 0), CONVERT(bit, 1), @PatientId)
				SET IDENTITY_INSERT [model].[PatientExternalProviders] OFF
			END
			---- Otherwise update ReferringPhysician PatientExternalProvider
			ELSE
			BEGIN
				UPDATE model.PatientExternalProviders
				SET
					ExternalProviderId = @ReferringPhysician
				WHERE Id = @PatientId
			END
		END

		
		IF EXISTS (SELECT * FROM model.ExternalContacts WHERE Id = @PrimaryCarePhysician)
		BEGIN
			DECLARE @PatientExternalProviderMax int
			SET @PatientExternalProviderMax = 110000000

			DECLARE @primaryCarePhysicianExternalProviderId AS int
			SET @primaryCarePhysicianExternalProviderId = (SELECT TOP 1 Id FROM model.PatientExternalProviders pep WHERE pep.Id = model.GetPairId(1, @PatientId, @PatientExternalProviderMax))

			---- Insert new PrimaryCarePhysician PatientExternalProvider if possible
			IF (COALESCE(@primaryCarePhysicianExternalProviderId, 0) = 0)
			BEGIN
				SET IDENTITY_INSERT [model].[PatientExternalProviders] ON
					INSERT INTO model.PatientExternalProviders (Id, ExternalProviderId, IsPrimaryCarePhysician, IsReferringPhysician, PatientId)
					VALUES (model.GetPairId(1, @PatientId, @PatientExternalProviderMax), @PrimaryCarePhysician, CONVERT(bit, 1), CONVERT(bit, 0), @PatientId)
				SET IDENTITY_INSERT [model].[PatientExternalProviders] OFF
			END
			ELSE
			---- Otherwise update PrimaryCarePhysician PatientExternalProvider
			BEGIN
				UPDATE model.PatientExternalProviders
				SET
					ExternalProviderId = @PrimaryCarePhysician
				WHERE Id = model.GetPairId(1, @PatientId, @PatientExternalProviderMax)
			END
		END

		DECLARE @CommentMax int
		SET @CommentMax = 110000000

		DECLARE @patientCommentId AS int
		SET @patientCommentId = model.GetPairId(2, @PatientId, @CommentMax)

		---- Insert new PatientComment if possible
		IF NOT EXISTS (SELECT * FROM model.PatientComments WHERE Id = @patientCommentId)
		BEGIN
			IF (COALESCE(@ProfileComment1, '') <> '')
			BEGIN
				SET IDENTITY_INSERT [model].[PatientComments] ON
					INSERT INTO model.PatientComments (Id, PatientId, Value, PatientCommentTypeId)
					VALUES (@patientCommentId, @PatientId, @ProfileComment1, 2)
				SET IDENTITY_INSERT [model].[PatientComments] OFF
			END
		END
		---- Otherwise update PatientComment
		ELSE
		BEGIN
			UPDATE model.PatientComments
			SET
				Value = @ProfileComment1
			WHERE Id = @patientCommentId
		END

		---- Insert new PatientReferralSource if possible
		DECLARE @referralSourceTypeId AS int
		SET @referralSourceTypeId = (SELECT TOP 1 Id FROM dbo.PracticeCodeTable pct WHERE pct.Code = @ReferralCatagory AND pct.ReferenceType = 'REFERBY')
		IF NOT EXISTS (SELECT * FROM model.PatientReferralSources prs WHERE prs.Id = @PatientId)
		BEGIN
			IF (COALESCE(@ReferralCatagory, '') <> '' AND COALESCE(@referralSourceTypeId, 0) <> 0)
			BEGIN
				SET IDENTITY_INSERT [model].[PatientReferralSources] ON
					INSERT INTO model.PatientReferralSources (Id, PatientId, ReferralSourceTypeId, Comment)
					VALUES (@PatientId, @PatientId, @referralSourceTypeId, @ProfileComment2)
				SET IDENTITY_INSERT [model].[PatientReferralSources] OFF
			END
		END
		---- Otherwise update PatientReferralSource
		ELSE
		BEGIN
			UPDATE model.PatientReferralSources
			SET
				Comment = @ProfileComment2
			WHERE Id = @PatientId
		END


		DECLARE @communicationPreferenceAddressId AS int
		SET @communicationPreferenceAddressId = CASE @BillParty
			WHEN 
				'Y' THEN (SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = ISNULL(dbo.GetPolicyHolderPatientId(@PatientId),@PatientId) ORDER BY pad.Id)
			ELSE
				(SELECT TOP 1 pad.Id FROM model.PatientAddresses pad where pad.PatientId = @PatientId ORDER BY pad.Id)
			END

		IF (COALESCE(@communicationPreferenceAddressId, 0) <> 0)
		BEGIN
			---- Insert new PatientCommunicationPreference if possible
			IF NOT EXISTS (SELECT * FROM model.PatientCommunicationPreferences pcp  WHERE 
							pcp.PatientId = @PatientId AND 
							pcp.PatientCommunicationTypeId = 3 AND 
							pcp.CommunicationMethodTypeId = 3 AND 
							pcp.__EntityType__ = 'PatientAddressCommunicationPreference')
			BEGIN
				INSERT INTO model.PatientCommunicationPreferences (PatientId, PatientCommunicationTypeId, CommunicationMethodTypeId, IsAddressedToRecipient, PatientAddressId, __EntityType__)
				VALUES (@PatientId, 3, 3, CONVERT(bit, 1), @communicationPreferenceAddressId, 'PatientAddressCommunicationPreference')
			END
			---- Otherwise update PatientCommunicationPreference
			ELSE
			BEGIN
				IF EXISTS (SELECT * FROM model.PatientCommunicationPreferences WHERE PatientId = @PatientId AND PatientCommunicationTypeId = 3 AND CommunicationMethodTypeId = 3 AND __EntityType__ = 'PatientAddressCommunicationPreference' AND PatientAddressId <> @communicationPreferenceAddressId)
				BEGIN
					UPDATE model.PatientCommunicationPreferences
					SET
						PatientAddressId = @communicationPreferenceAddressId
					WHERE PatientId = @PatientId AND PatientCommunicationTypeId = 3 AND CommunicationMethodTypeId = 3 AND __EntityType__ = 'PatientAddressCommunicationPreference' AND PatientAddressId <> @communicationPreferenceAddressId
				END
			END
		END

		DECLARE @patientTypeId AS int
		SET @patientTypeId = (SELECT TOP 1 Id FROM model.Tags pt WHERE pt.DisplayName = @PatType)

		---- Insert new PatientTags if possible
		IF NOT EXISTS (SELECT * FROM model.PatientTags ppt WHERE ppt.PatientId = @PatientId and ppt.OrdinalId = 1)
		BEGIN
			IF (COALESCE(@patientTypeId, 0) <> 0)
			BEGIN
				INSERT INTO model.PatientTags (PatientId, TagId, OrdinalId)
				VALUES (@PatientId, @patientTypeId, 1)
			END
		END
		ELSE
		---- Otherwise update PatientTags
		BEGIN
			IF (COALESCE(@patientTypeId, 0) <> 0)
			BEGIN
				UPDATE model.PatientTags
				SET
					TagId = @patientTypeId
				WHERE PatientId = @PatientId AND OrdinalId = 1
			END
			ELSE
			BEGIN
				DELETE FROM model.PatientTags WHERE PatientId = @PatientId AND OrdinalId = 1
			END
		END

		DECLARE @employerPhoneNumberTypeId AS int
		SET @employerPhoneNumberTypeId = (SELECT TOP 1 Id + 1000 FROM dbo.PracticeCodeTable pct WHERE pct.Code = 'Employer' AND pct.ReferenceType = 'PHONETYPEPATIENT')
		DECLARE @employerPhoneNumberId AS int
		SET @employerPhoneNumberId = (SELECT TOP 1 Id FROM model.PatientPhoneNumbers pn WHERE pn.PatientId = @PatientId AND pn.PatientPhoneNumberTypeId = @employerPhoneNumberTypeId)

		---- Insert new Employer PatientPhoneNumber if possible
		IF(COALESCE(@employerPhoneNumberId, 0) = 0)
		BEGIN
			IF (COALESCE(@EmployerPhone, '') <> '')
			BEGIN
				INSERT INTO model.PatientPhoneNumbers (ExchangeAndSuffix, IsInternational, OrdinalId, PatientId, PatientPhoneNumberTypeId)
				VALUES (@EmployerPhone, CONVERT(bit, 0), 6, @PatientId, @employerPhoneNumberTypeId)
			END
		END
		---- Otherwise update Business PatientPhoneNumber
		ELSE
		BEGIN
			UPDATE model.PatientPhoneNumbers
			SET
				ExchangeAndSuffix = @EmployerPhone
			WHERE PatientId = @PatientId AND PatientPhoneNumberTypeId = @employerPhoneNumberId
		END

		UPDATE model.InsurancePolicies
		SET
			MedicareSecondaryReasonCodeId = CASE @MedicareSecondary
				WHEN '12'
					THEN 1
				WHEN '13'
					THEN 2
				WHEN '14'
					THEN 3
				WHEN '15'
					THEN 4
				WHEN '16'
					THEN 5
				WHEN '41'
					THEN 6
				WHEN '42'
					THEN 7
				WHEN '43'
					THEN 8
				WHEN '47'
					THEN 9
				ELSE 
					NULL
				END
		
		WHERE PolicyHolderPatientId = @PatientId AND EXISTS (SELECT * FROM dbo.PracticeInsurers pins WHERE pins.InsurerId = InsurerId AND pins.InsurerReferenceCode IN ('M', 'N', '['))

		FETCH NEXT FROM PatientDemographicsInsertedUpdatedCursor INTO @PatientId, @SchedulePrimaryDoctor, @FirstName, @Status, @LastName, @MiddleInitial, @Salutation, @NameReference, @BirthDate, @Gender, @Marital, @Occupation, @OldPatient, @ReferralRequired, @SocialSecurity, @BusinessName, @Hipaa, @Ethnicity, @Language, @Race, @BusinessType, @Address, @Suite, @City, @State, @Zip, @BusinessAddress, @BusinessSuite, @BusinessCity, @BusinessState, @BusinessZip, @HomePhone, @CellPhone, @BusinessPhone, @EmployerPhone, @ReferringPhysician, @PrimaryCarePhysician, @ProfileComment1, @ProfileComment2, @Email, @EmergencyPhone, @EmergencyRel, @FinancialAssignment, @FinancialSignature, @MedicareSecondary, @BillParty, @SecondBillParty, @ReferralCatagory, @EmergencyName, @PolicyPatientId, @SecondPolicyPatientId, @Relationship, @SecondRelationship, @PatType, @BillingOffice, @PostOpExpireDate, @PostOpService, @FinancialSignatureDate, @Religion, @SecondRelationshipArchived, @SecondPolicyPatientIdArchived, @RelationshipArchived, @PolicyPatientIdArchived, @BulkMailing, @ContactType, @NationalOrigin, @SendStatements, @SendConsults
	END
		
	CLOSE PatientDemographicsInsertedUpdatedCursor
	DEALLOCATE PatientDemographicsInsertedUpdatedCursor
		
	SET NOCOUNT OFF

	SELECT @PatientId AS SCOPE_ID_COLUMN

	END


GO