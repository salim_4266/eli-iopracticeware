﻿--Insert data into table
SET IDENTITY_INSERT [model].DrugDosageNumbers ON
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (1, '0.5/half', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (2, '1', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (3, '1-2', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (4, '1-3', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (5, '1.5', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (6, '2', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (7, '3', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (8, '4', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (9, '5', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (10, '10', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (11, '15', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (12, '20', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (13, '30', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (14, 'Addl Sig', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (15, '2.5', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (16, '7.5', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (17, '2-3', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (18, '6', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (19, '7', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (20, '8', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (21, '9', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (22, '11', 0)
INSERT INTO model.DrugDosageNumbers (Id, Value, OrdinalId) VALUES (23, '12', 0)
SET IDENTITY_INSERT [model].DrugDosageNumbers OFF

GO

--Map data to external dataset
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 1 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 1
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 2 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 2
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 3 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 3
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 4 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 4
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 5 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 5
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 6 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 6
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 7 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 7
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 8 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 8
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 9 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 9
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 10 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 10
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 11 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 11
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 12 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 12
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 13 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 13
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 14 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 14
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 15 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 15
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 16 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 16
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 17 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 17
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 18 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 18
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 19 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 19
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 20 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 20
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 21 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 21
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 22 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 22
INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'DrugDosageNumber'), 23 , (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'DrugDosageNumber'), 23