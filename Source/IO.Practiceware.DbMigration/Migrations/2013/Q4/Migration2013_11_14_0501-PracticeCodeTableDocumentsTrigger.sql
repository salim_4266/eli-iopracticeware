﻿--Bug #8322.Update DisplayName having Null values.
IF EXISTS (SELECT OBJECT_ID FROM sys.tables WHERE name = 'TemplateDocuments' and schema_id = 6)
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = ''

	SELECT @sql = @sql + 'DROP TRIGGER ' + s.name + '.' + tr.name + '
	'
	FROM sys.triggers tr
	INNER JOIN sys.tables ta ON tr.parent_id = ta.object_id
	INNER JOIN sys.schemas s ON s.schema_id = ta.schema_id
	 WHERE ta.Name = 'TemplateDocuments'
	 AND tr.Name LIKE '%Audit%'

	EXEC(@Sql)
END

IF EXISTS (SELECT OBJECT_ID FROM sys.tables WHERE name = 'TemplateDocumentPrinter' and schema_id = 6) 
BEGIN
	SET @sql = ''

	SELECT @sql = @sql + 'DROP TRIGGER ' + s.name + '.' + tr.name + '
	'
	FROM sys.triggers tr
	INNER JOIN sys.tables ta ON tr.parent_id = ta.object_id
	INNER JOIN sys.schemas s ON s.schema_id = ta.schema_id
	 WHERE ta.Name = 'TemplateDocumentPrinter'
	 AND tr.Name LIKE '%Audit%'

	EXEC(@Sql)
END

--Trigger to Create Documents name in Documents Table

IF OBJECT_ID('PracticeCodeTableDocumentsTrigger') IS NOT NULL
BEGIN
	EXEC ('DROP TRIGGER PracticeCodeTableDocumentsTrigger')
END
GO

CREATE TRIGGER PracticeCodeTableDocumentsTrigger ON PracticeCodeTable
FOR UPDATE
	,INSERT
	,DELETE NOT
FOR REPLICATION AS

SET NOCOUNT ON

DECLARE @DocCatgId INT;
DECLARE @DocName NVARCHAR(MAX);

SELECT @DocName = pc.Code
	,@DocCatgId = CASE 
		WHEN pc.ReferenceType = 'MISCELLANEOUSLETTERS'
			THEN 1
		WHEN pc.ReferenceType = 'CONSULTATIONLETTERS'
			THEN 2
		WHEN pc.ReferenceType = 'REFERRALLETTERS'
			THEN 3
		WHEN pc.ReferenceType = 'COLLECTIONLETTERS'
			THEN 4
		WHEN pc.ReferenceType = 'FACILITYADMISSION'
			THEN 7
		ELSE 0
		END
FROM PracticeCodeTable pc
INNER JOIN Inserted i ON i.Id = pc.Id
WHERE i.ReferenceType LIKE '%Letters'

IF EXISTS (
		SELECT *
		FROM inserted
		)
BEGIN
	IF EXISTS (
			SELECT *
			FROM deleted
			)
	BEGIN
		IF @DocCatgId <> 0
		BEGIN
			UPDATE model.TemplateDocuments
			SET TemplateDocumentCategoryId = @DocCatgId
				,NAME = @DocName + '.doc'
				,DisplayNAME = @DocName
			WHERE Id IN (
					SELECT TOP 1 Id
					FROM model.TemplateDocuments
					WHERE DisplayName IN (
							SELECT Code
							FROM Deleted
							)
					)
		END
	END
	ELSE
	BEGIN
		IF @DocCatgId <> 0
		BEGIN
			INSERT INTO model.TemplateDocuments (
				TemplateDocumentCategoryId
				,NAME
				,DisplayName 
				)
			VALUES (
				@DocCatgId
				,@DocName + '.doc'
				,@DocName
				)
		END
	END
END
ELSE
BEGIN
	DELETE model.TemplateDocuments
	WHERE Id IN (
			SELECT TOP 1 Id
			FROM model.TemplateDocuments
			WHERE DisplayName IN (
					SELECT Code
					FROM Deleted
					WHERE ReferenceType LIKE '%Letters'
					)
			)
END

GO

--Update all previously created empty Displayname Rows too
UPDATE model.TemplateDocuments SET DisplayName= CASE WHEN CHARINDEX('.',Name,0) > 0 THEN SUBSTRING(NAME,0,CHARINDEX('.',Name,0)) ELSE NAME END WHERE DisplayName='' or DisplayName is null

GO