﻿IF NOT EXISTS (		
SELECT *
		FROM sys.columns sc
		INNER JOIN sys.tables st ON sc.Object_Id = st.Object_Id
		INNER JOIN sys.schemas sch ON st.schema_id = sch.schema_id
		WHERE sc.NAME = 'ClinicalId'
			AND st.NAME = 'PatientNotes'
			AND sch.name = 'dbo'
		)
BEGIN

ALTER TABLE dbo.PatientNotes
ADD ClinicalId INT

END
GO

IF OBJECT_ID('dbo.UpdatePatientNotesWithClinicalId', 'TR') IS NOT NULL
DROP TRIGGER UpdatePatientNotesWithClinicalId
GO

CREATE TRIGGER UpdatePatientNotesWithClinicalId
ON dbo.PatientClinical
FOR INSERT, UPDATE
NOT FOR REPLICATION
--This trigger helps with the join from PatientClincial to PatientNotes.
AS
BEGIN
SET NOCOUNT ON
--Gets clinical Id for RX notes.
	IF EXISTS (SELECT i.ClinicalId 
				FROM inserted i 
				WHERE i.ClinicalType = 'A'
				AND LEN(i.FindingDetail) > 6
				AND SUBSTRING(i.FindingDetail, 1, 3) = 'RX-'
				)
	BEGIN 
		UPDATE pn
		SET pn.ClinicalId = i.ClinicalId
		FROM dbo.PatientNotes pn
		INNER JOIN inserted i ON i.AppointmentId = pn.AppointmentId AND pn.NoteSystem = SUBSTRING(i.FindingDetail, 6, CHARINDEX('-2', i.FindingDetail) - 6) -- 6 is hardcoded because it will always be 'RX-8/'
		WHERE pn.NoteType = 'R'
			AND i.ClinicalType = 'A'
			AND LEN(i.FindingDetail) > 6
			AND SUBSTRING(i.FindingDetail, 1, 3) = 'RX-'
	END

END

GO

--Used to map FDB to dbo.Drug for DosageForms.
IF NOT EXISTS (		
SELECT *
		FROM sys.columns sc
		INNER JOIN sys.tables st ON sc.Object_Id = st.Object_Id
		INNER JOIN sys.schemas sch ON st.schema_id = sch.schema_id
		WHERE sc.NAME = 'DrugDosageFormId'
			AND st.NAME = 'Drug'
			AND sch.name = 'dbo')
BEGIN
	ALTER TABLE dbo.Drug
	ADD DrugDosageFormId INT
END

GO

--Trigger to continually update.
IF OBJECT_ID('dbo.UpdateDrugDosageFormIdTrigger', 'TR') IS NOT NULL --Need this so this seciton of code doesn't run again.
BEGIN
	--Migration to map drugs to Drug Dosage Forms
	UPDATE dru
	SET DrugDosageFormId = ddf.Id
	FROM dbo.Drug dru
	INNER JOIN fdb.tblCompositeDrug tb ON tb.MED_NAME = dru.DisplayName
	INNER JOIN model.DrugDosageForms ddf ON ddf.Name = tb.MED_DOSAGE_FORM_DESC

	--Adding InternationalUnit to DrugDispenseForm
	IF NOT EXISTS(SELECT * FROM dbo.PracticeCodeTable WHERE Code = 'International Unit' AND ReferenceType = 'DISPENSENUMBERQUALIFIER')
	INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, [Signature], TestOrder, FollowUp, [Rank], WebsiteUrl) 
	VALUES ('DISPENSENUMBERQUALIFIER', 'International Unit', 'F2', 'F', '', '', '', 'F', '', '', '0' , '')


	--Backfill for PatientNotes.ClinicalId
	UPDATE pn
	SET pn.ClinicalId = pc.ClinicalId
	FROM dbo.PatientNotes pn
	INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = pn.AppointmentId AND pn.NoteSystem = SUBSTRING(pc.FindingDetail, 6, CHARINDEX('-2', pc.FindingDetail) - 6) -- 6 is hardcoded because it will always be 'RX-8/'
	WHERE pn.NoteType = 'R'
		AND pc.ClinicalType = 'A'
		AND SUBSTRING(pc.FindingDetail, 1, 3) = 'RX-'\
END