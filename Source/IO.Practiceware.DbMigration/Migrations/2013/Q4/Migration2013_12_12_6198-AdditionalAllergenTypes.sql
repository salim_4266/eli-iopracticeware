﻿INSERT INTO [model].[AllergenReactionTypes]([ClinicalQualifierId],[IsDeactivated],[Name],[OrdinalId])
VALUES(3,0,'shortnessofbreath',1)

DECLARE @InsertedId INT
SELECT @InsertedId = SCOPE_IDENTITY()

SET NOCOUNT ON;
DECLARE @SnomedExternalSystemId INT
SELECT @SnomedExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'SnomedCt'

DECLARE @PracticeRepositoryEntityId INT
SELECT @PracticeRepositoryEntityId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'AllergenReactionType'

DECLARE @ExternalSystemEntityId INT
SELECT @ExternalSystemEntityId = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @SnomedExternalSystemId
	AND Name = 'AllergenReactionType'


INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
	SELECT @PracticeRepositoryEntityId, @InsertedId AS PracticeRepositoryEntityKey, @ExternalSystemEntityId, 207057006 AS ExternalSystemEntityKey