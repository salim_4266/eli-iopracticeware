﻿SET IDENTITY_INSERT [model].[TreatmentGoals] ON 

INSERT [model].[TreatmentGoals] ([Id], [DisplayName], [Value]) VALUES (2, N'Weight loss', N'I advised the patient to lose weight.')
INSERT [model].[TreatmentGoals] ([Id], [DisplayName], [Value]) VALUES (3, N'Asthma management', N'I will manage the patient''s asthma.')
INSERT [model].[TreatmentGoals] ([Id], [DisplayName], [Value]) VALUES (4, N'Stress management surveillance', N'I will observe the patient''s  stress management.')
INSERT [model].[TreatmentGoals] ([Id], [DisplayName], [Value]) VALUES (5, N'Smoking cessasion', N'I advised the patient to stop smoking.')
INSERT [model].[TreatmentGoals] ([Id], [DisplayName], [Value]) VALUES (6, N'Well women screening', N'Preventative Health : annual pap smear, biennial screening mammogram, and annual clinical breast exam')

SET IDENTITY_INSERT [model].[TreatmentGoals] OFF

SET IDENTITY_INSERT [model].[TreatmentGoalAndInstructions] ON 

INSERT [model].[TreatmentGoalAndInstructions] ([Id], [ShortName], [Value], [OrdinalId], [TreatmentGoalId]) VALUES (1, N'Weight loss', N'I advised the patient to lose weight.', 1, 2)
INSERT [model].[TreatmentGoalAndInstructions] ([Id], [ShortName], [Value], [OrdinalId], [TreatmentGoalId]) VALUES (2, N'Asthma management', N'I will manage the patient''s asthma.', 1, 3)
INSERT [model].[TreatmentGoalAndInstructions] ([Id], [ShortName], [Value], [OrdinalId], [TreatmentGoalId]) VALUES (3, N'Stress management surveillance', N'I will observe the patient''s  stress management.', 1, 4)
INSERT [model].[TreatmentGoalAndInstructions] ([Id], [ShortName], [Value], [OrdinalId], [TreatmentGoalId]) VALUES (4, N'Smoking cessasion', N'I advised the patient to stop smoking.', 1, 5)
INSERT [model].[TreatmentGoalAndInstructions] ([Id], [ShortName], [Value], [OrdinalId], [TreatmentGoalId]) VALUES (5, N'Well women screening', N'Preventative Health : annual pap smear, biennial screening mammogram, and annual clinical breast exam', 99, 6)--I know we will never use this except for the test, but without it the Ids could be duplicated.

SET IDENTITY_INSERT [model].[TreatmentGoalAndInstructions] OFF

--These are required to pass the test, when we release we will do something different. 
INSERT INTO dbo.PracticeCodeTable (ReferenceType, Code, AlternateCode)
SELECT 'IMPRESSIONLINKDISCUSSION', 'Weight loss', '278.*'
UNION ALL
SELECT 'IMPRESSIONLINKDISCUSSION', 'Asthma management', '493.*'
UNION ALL
SELECT 'IMPRESSIONLINKDISCUSSION', 'Stress management surveillance', 'V62.89'
UNION ALL
SELECT 'IMPRESSIONLINKDISCUSSION', 'Smoking cessasion', '305.1'


DECLARE @PracticeRepositoryEntityId INT
SET @PracticeRepositoryEntityId = (SELECT Id from model.PracticeRepositoryEntities WHERE Name = 'TreatmentGoal')

DECLARE @ExternalSystemEntityId INT
SET @ExternalSystemEntityId = (SELECT esm.Id FROM model.ExternalSystems es
	INNER JOIN model.ExternalSystemEntities esm ON es.Id = esm.ExternalSystemId
	WHERE es.Name = 'SnomedCt' AND esm.Name = 'TreatmentGoal')

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @PracticeRepositoryEntityId, 2 , @ExternalSystemEntityId, 275919002

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @PracticeRepositoryEntityId, 3 , @ExternalSystemEntityId, 170641004

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @PracticeRepositoryEntityId, 4 , @ExternalSystemEntityId, 226060000

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
SELECT @PracticeRepositoryEntityId, 5 , @ExternalSystemEntityId, 225323000