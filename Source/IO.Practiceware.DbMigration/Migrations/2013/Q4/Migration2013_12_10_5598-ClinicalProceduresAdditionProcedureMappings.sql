﻿SET NOCOUNT ON;
ALTER TABLE model.ExternalSystemEntityMappings DISABLE TRIGGER ALL;

INSERT INTO model.ClinicalProcedures (Name, IsSocial, ClinicalProcedureCategoryId, IsDeactivated)
SELECT 'HGB', CONVERT(BIT,0), 2, CONVERT(BIT,0) UNION ALL
SELECT 'COLONOSCOPY', CONVERT(BIT,0), 2, CONVERT(BIT,0) UNION ALL
SELECT 'SPUTUM CULTURE', CONVERT(BIT,0), 2, CONVERT(BIT,0)

DECLARE @SnomedExternalSystemId INT
SELECT @SnomedExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'SnomedCt'

DECLARE @ClinicalProcedureId INT
SELECT @ClinicalProcedureId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClinicalProcedure'

DECLARE @SnomedExternalSystemIdClinicalProcedure INT
SELECT @SnomedExternalSystemIdClinicalProcedure = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @SnomedExternalSystemId
	AND Name = 'ClinicalProcedure'

INSERT INTO [model].[ExternalSystemEntityMappings]([PracticeRepositoryEntityId],[PracticeRepositoryEntityKey],[ExternalSystemEntityId],[ExternalSystemEntityKey])
SELECT @ClinicalProcedureId,(SELECT TOP 1 Id FROM model.ClinicalProcedures WHERE Name = 'HGB'),@SnomedExternalSystemIdClinicalProcedure,104091002 UNION ALL
SELECT @ClinicalProcedureId,(SELECT TOP 1 Id FROM model.ClinicalProcedures WHERE Name = 'COLONOSCOPY'),@SnomedExternalSystemIdClinicalProcedure,359590001 UNION ALL
SELECT @ClinicalProcedureId,(SELECT TOP 1 Id FROM model.ClinicalProcedures WHERE Name = 'SPUTUM CULTURE'),@SnomedExternalSystemIdClinicalProcedure,145234008

ALTER TABLE model.ExternalSystemEntityMappings ENABLE TRIGGER ALL;
