﻿--We will need to rerun this migration once the map to FDB is fixed (not all drugs are being mapped because some of the names have tab in our drug database and the fdb has tablet)

SELECT FDB.tblcompositeDrug.MEDID AS Id
INTO #tempDrug
FROM FDB.tblCompositeDrug
WHERE MED_ROUTED_DF_MED_ID_DESC IN (SELECT DisplayName FROM dbo.Drug)


DELETE FROM model.ExternalSystemEntityMappings
WHERE ExternalSystemEntityId = (SELECT ese.Id FROM model.ExternalSystemEntities ese INNER JOIN model.ExternalSystems es ON ese.ExternalSystemId = es.Id WHERE ese.Name = 'Medication' AND es.Name = 'FDBDrug')

INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
		SELECT (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Medication')
		,CONVERT(nvarchar(max),Id) 
		,(SELECT ese.Id FROM model.ExternalSystemEntities ese INNER JOIN model.ExternalSystems es ON ese.ExternalSystemId = es.Id WHERE ese.Name = 'Medication' AND es.Name = 'FDBDrug')
		,CONVERT(nvarchar(max),Id) 
FROM #tempDrug
