﻿SELECT 1 AS Value INTO #NoCheck

--This is due to certain databases having the bad row 30 (and also to fix previous wrong name) 
IF EXISTS (SELECT * 
           FROM model.ExternalSystemMessageTypes 
		   WHERE (Name = 'PaperClaim_Outbound' OR Name = 'ACK_V231') AND Id = 30)
BEGIN 

	UPDATE model.ExternalSystemMessageTypes 
	SET Name = 'ACK_V231_Inbound', Description = 'Acknowledgement'
	WHERE Id = 30

END

IF NOT EXISTS (SELECT * FROM model.ExternalSystemMessageTypes WHERE Name = 'ACK_V231_Inbound')
BEGIN
	SET IDENTITY_INSERT model.ExternalSystemMessageTypes ON 
	
	INSERT INTO model.ExternalSystemMessageTypes (Id, Name, Description, IsOutbound)
	VALUES (30, 'ACK_V231_Inbound', 'Acknowledgement', CONVERT(bit, 0))
	
	SET IDENTITY_INSERT model.ExternalSystemMessageTypes OFF 
END

DROP TABLE #NoCheck

