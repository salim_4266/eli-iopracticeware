﻿--This means that if the database is migrated with the account number as null, it will not be able to erx.

IF NOT EXISTS(SELECT * FROM model.ApplicationSettings WHERE Name = 'NewCropClientConfiguration') AND EXISTS (SELECT * FROM EPrescription_Credentials WHERE AccountId IS NOT NULL)
INSERT INTO model.ApplicationSettings(Name, Value, ApplicationSettingTypeId)
SELECT TOP 1 
'NewCropClientConfiguration', 
'<newCropClientConfiguration 
	update1ServiceUrl="https://secure.newcropaccounts.com/V7/webservices/Update1.asmx" 
	partnerName="IOPracticeware" 
	userName="' + UserName + '" 
	password="' + [Password] + '" 
	accountId="' + CONVERT(nvarchar, AccountId) + '" 
	siteId="' + CONVERT(nvarchar, SiteId) + '" />', 11 /* Raw */
FROM EPrescription_Credentials
