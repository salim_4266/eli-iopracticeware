﻿IF OBJECT_ID('dbo.IsNullOrEmpty', 'FN') IS NOT NULL
	DROP FUNCTION dbo.IsNullOrEmpty
GO

CREATE FUNCTION dbo.IsNullOrEmpty (@entryString NVARCHAR(MAX))
RETURNS BIT
AS 
BEGIN
	RETURN 
		CASE 
			WHEN @entryString IS NULL OR @entryString = ''
				THEN CONVERT(BIT,1)
			ELSE CONVERT(BIT,0)
		END
END
GO

IF OBJECT_ID('dbo.ExtractTextValue', 'FN') IS NOT NULL
	DROP FUNCTION dbo.ExtractTextValue
GO

CREATE FUNCTION dbo.ExtractTextValue (@startCharacter NVARCHAR(200), @textValue NVARCHAR(MAX),@endCharacter NVARCHAR(200))
--This function accepts a starting and ending search string and a string to search.
--Returns everything in-between the start and end strings (excludes start and end in returned results).
RETURNS NVARCHAR(MAX)
AS 
BEGIN
	DECLARE @startingIndex INT
	SET @startingIndex = CHARINDEX(@startCharacter, @textValue)+LEN(@startCharacter)

	DECLARE @endingIndex INT
	SET @endingIndex = CHARINDEX(@endCharacter,@textValue)-@startingIndex

	RETURN CASE WHEN @endingIndex IS NOT NULL AND @endingIndex > 0 THEN SUBSTRING(@textValue, @startingIndex, @endingIndex) ELSE NULL END

END
GO