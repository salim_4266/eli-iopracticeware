﻿using FluentMigrator;
using System;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    ///   Adds "dummy foreign keys" and PracticeCodeTable PaymentTypes and TransmitTypes.
    /// </summary>
    [Migration(201203021500)]
    public class Migration201203021500 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"
INSERT dbo.PracticeCodeTable([ReferenceType]
       ,[Code]
       ,[AlternateCode]
       ,[Exclusion]
       ,[LetterTranslation]
       ,[OtherLtrTrans]
       ,[FormatMask]
       ,[Signature]
       ,[TestOrder]
       ,[FollowUp]
       ,[Rank]
       )
SELECT DISTINCT 'PAYMENTTYPE'
, 'DoNotDelete - ' + PaymentType
,N''
,N'F'
,N'UNKNOWN'
,N''
,N''
,N'F'
,N''
,N''
,99
FROM dbo.PatientReceivablePayments prp LEFT JOIN
dbo.PracticeCodeTable pct ON prp.PaymentType = SUBSTRING(Code, LEN(pct.Code), 1) AND pct.ReferenceType = 'PAYMENTTYPE'
WHERE pct.Id IS NULL
    AND PaymentType <> '' 
    AND PaymentTYpe <> '='

INSERT dbo.PracticeCodeTable([ReferenceType]
,[Code]
,[AlternateCode]
,[Exclusion]
,[LetterTranslation]
,[OtherLtrTrans]
,[FormatMask]
,[Signature]
,[TestOrder]
,[FollowUp]
,[Rank]
)
SELECT DISTINCT 'RACE'
, pd.Race
,N''
,N'F'
,pd.Race
,N''
,N''
,N'F'
,N''
,N''
,99
FROM dbo.PatientDemographics pd LEFT JOIN
dbo.PracticeCodeTable pct ON pd.Race = pct.Code AND PCT.ReferenceType = 'RACE'
WHERE pct.Id IS NULL AND pd.Race IS NOT NULL AND pd.Race <> ''


INSERT dbo.PracticeCodeTable([ReferenceType]
,[Code]
,[AlternateCode]
,[Exclusion]
,[LetterTranslation]
,[OtherLtrTrans]
,[FormatMask]
,[Signature]
,[TestOrder]
,[FollowUp]
,[Rank]
)
SELECT DISTINCT 'LANGUAGE'
, pd.Language
,N''
,N'F'
,N''
,N''
,N''
,N'F'
,N''
,N''
,99
FROM dbo.PatientDemographics pd LEFT JOIN
dbo.PracticeCodeTable pct ON pd.Language = pct.Code AND PCT.ReferenceType = 'LANGUAGE'
WHERE pct.Id IS NULL AND pd.Language IS NOT NULL AND pd.Language <> ''


INSERT dbo.PracticeCodeTable([ReferenceType]
,[Code]
,[AlternateCode]
,[Exclusion]
,[LetterTranslation]
,[OtherLtrTrans]
,[FormatMask]
,[Signature]
,[TestOrder]
,[FollowUp]
,[Rank]
)
SELECT DISTINCT 'ETHNICITY'
, pd.Ethnicity
,N''
,N'F'
,pd.Ethnicity
,N''
,N''
,N'F'
,N''
,N''
,99
FROM dbo.PatientDemographics pd LEFT JOIN
dbo.PracticeCodeTable pct ON pd.Ethnicity = pct.Code AND PCT.ReferenceType = 'ETHNICITY'
WHERE pct.Id IS NULL AND pd.Ethnicity IS NOT NULL AND pd.Ethnicity <> ''


INSERT dbo.PracticeCodeTable([ReferenceType]
,[Code]
,[AlternateCode]
,[Exclusion]
,[LetterTranslation]
,[OtherLtrTrans]
,[FormatMask]
,[Signature]
,[TestOrder]
,[FollowUp]
,[Rank]
)
SELECT DISTINCT 'EMPLOYEESTATUS'
,pd.BusinessType
,N''
,N'F'
,N''
,N''
,N''
,N'F'
,N''
,N''
,99
FROM dbo.PatientDemographics pd LEFT JOIN
dbo.PracticeCodeTable pct ON pd.BusinessType = SUBSTRING(pct.Code, 1, 1) AND PCT.ReferenceType = 'EMPLOYEESTATUS'
WHERE pct.Id IS NULL AND pd.BusinessType IS NOT NULL AND pd.BusinessType <> ''

");
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
