﻿IF OBJECT_ID('PatientDemographicsComputedFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientDemographicsComputedFinancialIdTrigger')
END

EXEC('CREATE TRIGGER [dbo].[PatientDemographicsComputedFinancialIdTrigger]
ON [dbo].[PatientDemographics]
FOR INSERT, UPDATE
NOT FOR REPLICATION
AS
SET NOCOUNT ON;

DECLARE @changedIds table(Id int, UNIQUE CLUSTERED(Id))
INSERT @changedIds
SELECT DISTINCT c.PatientId FROM
(SELECT * FROM inserted EXCEPT SELECT * FROM deleted) c
JOIN deleted d ON c.PatientId = d.PatientId
WHERE c.PolicyPatientId <> d.PolicyPatientId OR c.SecondPolicyPatientId <> d.SecondPolicyPatientId
UNION
SELECT DISTINCT c.SecondPolicyPatientId FROM
(SELECT * FROM inserted EXCEPT SELECT * FROM deleted) c
JOIN deleted d ON c.PatientId = d.PatientId
WHERE c.PolicyPatientId <> d.PolicyPatientId OR c.SecondPolicyPatientId <> d.SecondPolicyPatientId
UNION
SELECT DISTINCT c.PolicyPatientId FROM
(SELECT * FROM inserted EXCEPT SELECT * FROM deleted) c
JOIN deleted d ON c.PatientId = d.PatientId
WHERE c.PolicyPatientId <> d.PolicyPatientId OR c.SecondPolicyPatientId <> d.SecondPolicyPatientId

IF EXISTS(SELECT * FROM @changedIds) AND dbo.GetNoRecompute() = 0
BEGIN
	SELECT 1 AS Value INTO #NoRecompute

	DECLARE @xml xml
	
	-- PatientReceivables
	SET @xml = (
		SELECT pr.ReceivableId AS Id
		FROM @changedIds i
		INNER JOIN PatientReceivables pr WITH(NOLOCK) ON i.Id = pr.PatientId
		FOR XML PATH('''')
	)

	SELECT Id, FinancialId 
	INTO #patientReceivablesFinancialIds
	FROM dbo.GetPatientReceivablesFinancialIds(@xml)

	IF EXISTS (SELECT * FROM #patientReceivablesFinancialIds)
	BEGIN
		EXEC(''
		UPDATE pr
		SET pr.ComputedFinancialId = f.FinancialId
		FROM #patientReceivablesFinancialIds f
		JOIN PatientReceivables pr WITH(NOLOCK) ON f.Id = pr.ReceivableId 	
		WHERE COALESCE(ComputedFinancialId, '''''''') <> COALESCE(f.FinancialId, '''''''')
		'')
	END

	DROP TABLE #patientReceivablesFinancialIds


	-- PatientReferral
	SET @xml = (
		SELECT pRef.ReferralId AS Id
		FROM @changedIds i
		INNER JOIN PatientReferral pRef WITH(NOLOCK) ON i.Id = pRef.PatientId
		FOR XML PATH('''')
	)

	SELECT Id, FinancialId 
	INTO #patientReferralsFinancialIds
	FROM dbo.GetPatientReferralsFinancialIds(@xml)

	IF EXISTS (SELECT * FROM #patientReferralsFinancialIds)
	BEGIN
		EXEC(''
		UPDATE pRef
		SET pRef.ComputedFinancialId = f.FinancialId
		FROM #patientReferralsFinancialIds f
		JOIN PatientReferral pRef WITH(NOLOCK) ON f.Id = pRef.ReferralId
		WHERE COALESCE(ComputedFinancialId, '''''''') <> COALESCE(f.FinancialId, '''''''')
		'')	
	END

	DROP TABLE #patientReferralsFinancialIds


	-- PatientPreCerts
	SET @xml = (
		SELECT pCert.PreCertId AS Id
		FROM @changedIds i
		INNER JOIN PatientPreCerts pCert ON i.Id = pCert.PatientId
		FOR XML PATH('''')
	)

	SELECT Id, FinancialId 
	INTO #patientPreCertsFinancialIds
	FROM dbo.GetPatientPreCertsFinancialIds(@xml)

	IF EXISTS (SELECT * FROM #patientPreCertsFinancialIds)
	BEGIN
		EXEC(''
		UPDATE pCert
		SET pCert.ComputedFinancialId = f.FinancialId
		FROM #patientPreCertsFinancialIds f
		JOIN PatientPreCerts pCert WITH(NOLOCK) ON f.Id = pCert.PreCertId
		WHERE COALESCE(ComputedFinancialId, '''''''') <> COALESCE(f.FinancialId, '''''''')
		'')
	END

	DROP TABLE #patientPreCertsFinancialIds
    
	DROP TABLE #NoRecompute
END
')