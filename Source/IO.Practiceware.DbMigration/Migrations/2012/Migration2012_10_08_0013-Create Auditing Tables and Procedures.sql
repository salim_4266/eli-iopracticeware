﻿IF OBJECT_ID('dbo.AuditEntries') IS NULL
BEGIN
	CREATE TABLE dbo.AuditEntries (
		Id uniqueidentifier NOT NULL,
		ObjectName nvarchar(max) NOT NULL,
		ChangeTypeId int NOT NULL,
		AuditDateTime datetime NOT NULL,
		HostName nvarchar(500) NOT NULL,
		UserId nvarchar(500),
		PRIMARY KEY(Id),
		CHECK (ChangeTypeId >= 0 AND ChangeTypeId <= 2)
	)
END

IF OBJECT_ID('dbo.AuditEntryKeyValues') IS NULL
BEGIN
	CREATE TABLE dbo.AuditEntryKeyValues (
		AuditEntryId uniqueidentifier NOT NULL,
		KeyName nvarchar(400) NOT NULL,
		KeyValue nvarchar(max) NOT NULL
		PRIMARY KEY(AuditEntryId, KeyName)
		FOREIGN KEY(AuditEntryId) REFERENCES dbo.AuditEntries(Id)
	)
END

IF OBJECT_ID('dbo.AuditEntryChanges') IS NULL
BEGIN
	CREATE TABLE dbo.AuditEntryChanges (
		AuditEntryId uniqueidentifier NOT NULL,
		ColumnName nvarchar(400) NOT NULL,
		OldValue nvarchar(max) NULL,
		PRIMARY KEY(AuditEntryId, ColumnName),
		FOREIGN KEY(AuditEntryId) REFERENCES dbo.AuditEntries(Id)
	)
END

GO

IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'AuditEntries' AND sc.Name = 'ServerName') 
	ALTER TABLE dbo.AuditEntries
	ADD ServerName nvarchar(500) NOT NULL DEFAULT ''
