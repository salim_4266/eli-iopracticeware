﻿--Change camera to .net camera
IF NOT EXISTS (SELECT * FROM dbo.Practiceinterfaceconfiguration WHERE FieldReference = 'LEGACYCAMERA')
INSERT INTO dbo.PracticeInterfaceConfiguration (FieldReference, FieldValue) VALUES ('LEGACYCAMERA', 'F')
GO

--Distinguish between Print in DI and Print in PI.
IF NOT EXISTS (SELECT * FROM dbo.Practiceinterfaceconfiguration WHERE FieldReference = 'PrintPI')
INSERT INTO dbo.PracticeInterfaceConfiguration (FieldReference, FieldValue) VALUES ('PrintPI', 'F')
GO