﻿--Trigger to Create Documents name in Documents Table
IF OBJECT_ID('PracticeCodeTableDocumentsTrigger') IS NOT NULL
BEGIN
	EXEC ('DROP TRIGGER PracticeCodeTableDocumentsTrigger')
END
GO

CREATE TRIGGER PracticeCodeTableDocumentsTrigger ON PracticeCodeTable
FOR UPDATE
	,INSERT
	,DELETE NOT
FOR REPLICATION AS

SET NOCOUNT ON

DECLARE @DocTypeId INT;
DECLARE @DocName NVARCHAR(MAX);

SELECT @DocName = pc.Code
	,@DocTypeId = CASE 
		WHEN pc.ReferenceType = 'MISCELLANEOUSLETTERS'
			THEN 1
		WHEN pc.ReferenceType = 'CONSULTATIONLETTERS'
			THEN 2
		WHEN pc.ReferenceType = 'REFERRALLETTERS'
			THEN 3
		WHEN pc.ReferenceType = 'COLLECTIONLETTERS'
			THEN 4
		WHEN pc.ReferenceType = 'FACILITYADMISSION'
			THEN 7
		ELSE 0
		END
FROM PracticeCodeTable pc
INNER JOIN Inserted i ON i.Id = pc.Id
WHERE i.ReferenceType LIKE '%Letters'

IF EXISTS (
		SELECT *
		FROM inserted
		)
BEGIN
	IF EXISTS (
			SELECT *
			FROM deleted
			)
	BEGIN
		IF @DocTypeId <> 0
		BEGIN
			UPDATE model.Documents
			SET DocumentTypeId = @DocTypeId
				,NAME = @DocName
			WHERE Id IN (
					SELECT TOP 1 Id
					FROM model.Documents
					WHERE NAME IN (
							SELECT Code
							FROM Deleted
							)
					)
		END
	END
	ELSE
	BEGIN
		IF @DocTypeId <> 0
		BEGIN
			INSERT INTO model.Documents (
				DocumentTypeId
				,NAME
				)
			VALUES (
				@DocTypeId
				,@DocName
				)
		END
	END
END
ELSE
BEGIN
	DELETE model.Documents
	WHERE Id IN (
			SELECT TOP 1 Id
			FROM model.Documents
			WHERE NAME IN (
					SELECT Code
					FROM Deleted
					WHERE ReferenceType LIKE '%Letters'
					)
			)
END