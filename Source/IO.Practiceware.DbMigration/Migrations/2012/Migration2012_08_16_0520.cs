﻿using FluentMigrator;
using System;

namespace IO.Practiceware.DbMigration.Migrations
{
    [Migration(201208160520)]
    public class Migration201208160520 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"
/****** Object:  StoredProcedure [dbo].[RestoreAuditEntries]    Script Date: 08/16/2012 05:04:38 ******/
SET ANSI_NULLS ON
GO


IF EXISTS (
		SELECT *
		FROM dbo.sysobjects
		WHERE id = OBJECT_ID(N'[RestoreAuditEntries]')
			AND OBJECTPROPERTY(id, N'IsProcedure') = 1
		)
	DROP PROCEDURE [RestoreAuditEntries]
GO

CREATE PROCEDURE RestoreAuditEntries
AS
BEGIN
	DECLARE @EntryId VARCHAR(500)
		,@TableName VARCHAR(500)
		,@ChangeTypeId INT
		,@PrimaryKeyName VARCHAR(500)
		,@PrimaryKeyValue VARCHAR(500)
		,@ColumnName VARCHAR(500)
		,@OldValue NVARCHAR(MAX)
		,@strQuery NVARCHAR(MAX)
		,@strColumnsWithValues NVARCHAR(MAX)
		,@strColumns NVARCHAR(MAX)
		,@strValues NVARCHAR(MAX)
		,@strTempQuery NVARCHAR(MAX)

	IF object_id('tempdb..#AuditEntryIdsToRestore') IS NOT NULL
	BEGIN
		DECLARE RESTORECURSOR CURSOR
		FOR
		SELECT AuditEntryId
		FROM tempdb..#AuditEntryIdsToRestore

		OPEN RESTORECURSOR

		FETCH
		FROM RESTORECURSOR
		INTO @EntryId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT @EntryId = AE.Id
				,@TableName = AE.ObjectName
				,@ChangeTypeId = AE.ChangeTypeId
			FROM dbo.AuditEntries AE
			WHERE AE.Id = @EntryId

			SELECT @PrimaryKeyName = AEK.KeyName
				,@PrimaryKeyValue = AEK.KeyValue
			FROM dbo.AuditEntryKeyValues AEK
			WHERE AEK.AuditEntryId = @EntryId

			SELECT @strQuery = ''

			-- OnUpdate re-applying the values   
			IF @ChangeTypeId = 1
			BEGIN
				DECLARE UPDATECURSOR CURSOR
				FOR
				SELECT ColumnName
					,OldValue
				FROM AuditEntryChanges
				WHERE AuditEntryId = @EntryId

				OPEN UPDATECURSOR

				SELECT @strColumnsWithValues = ''

				FETCH
				FROM UPDATECURSOR
				INTO @ColumnName
					,@OldValue

				WHILE @@FETCH_STATUS = 0
				BEGIN
					IF @strColumnsWithValues = ''
						SELECT @strColumnsWithValues = @ColumnName + ' = ''' + @OldValue + ''''
					ELSE
						SELECT @strColumnsWithValues = @strColumnsWithValues + ',' + @ColumnName + ' = ''' + @OldValue + ''''

					FETCH NEXT
					FROM UPDATECURSOR
					INTO @ColumnName
						,@OldValue
				END

				CLOSE UPDATECURSOR

				DEALLOCATE UPDATECURSOR

				IF @strColumnsWithValues <> ''
				BEGIN
					SELECT @strQuery = 'IF EXISTS (SELECT ' + @PrimaryKeyName + ' FROM ' + @TableName + ' Where ' + @PrimaryKeyName + ' = ''' + @PrimaryKeyValue + ''')'

					SELECT @strQuery = @strQuery + ' UPDATE ' + @TableName + ' SET ' + @strColumnsWithValues + ' Where ' + @PrimaryKeyName + ' = ''' + @PrimaryKeyValue + ''''

					--PRINT (@strQuery)  
					EXECUTE (@strQuery)
				END
			END

			-- OnDelete re-inserting the row  
			IF @ChangeTypeId = 2
			BEGIN
				DECLARE INSERTCURSOR CURSOR
				FOR
				SELECT ColumnName
					,OldValue
				FROM AuditEntryChanges
				WHERE AuditEntryId = @EntryId

				OPEN INSERTCURSOR

				SELECT @strColumns = ''

				SELECT @strValues = ''

				FETCH
				FROM INSERTCURSOR
				INTO @ColumnName
					,@OldValue

				WHILE @@FETCH_STATUS = 0
				BEGIN
					IF @strColumns = ''
						SELECT @strColumns = @ColumnName
					ELSE
						SELECT @strColumns = @strColumns + ',' + @ColumnName

					IF @strValues = ''
						SELECT @strValues = CASE WHEN @OldValue IS NULL THEN 'NULL' ELSE '''' + @OldValue + '''' END
					ELSE
						SELECT @strValues = @strValues + ', ' + CASE WHEN @OldValue IS NULL THEN 'NULL' ELSE '''' + @OldValue + '''' END

					FETCH NEXT
					FROM INSERTCURSOR
					INTO @ColumnName
						,@OldValue
				END

				CLOSE INSERTCURSOR

				DEALLOCATE INSERTCURSOR

				IF @strColumns <> ''
					AND @strValues <> ''
				BEGIN
					SELECT @strQuery = 'IF NOT EXISTS (SELECT ' + @PrimaryKeyName + ' FROM ' + @TableName + ' Where ' + @PrimaryKeyName + ' = ''' + @PrimaryKeyValue + ''')'

					SELECT @strQuery = @strQuery + ' BEGIN SET identity_insert ' + @TableName + ' ON '

					SELECT @strQuery = @strQuery + ' INSERT INTO ' + @TableName + ' ( ' + @strColumns + ') Values( ' + @strValues + ')'

					SELECT @strQuery = @strQuery + ' SET identity_insert ' + @TableName + ' OFF END'

					--PRINT (@strQuery)  
					EXECUTE (@strQuery)
				END
			END

			FETCH NEXT
			FROM RESTORECURSOR
			INTO @EntryId
		END

		CLOSE RESTORECURSOR

		DEALLOCATE RESTORECURSOR

		
	END
END

");
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
