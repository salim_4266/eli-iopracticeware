﻿IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'Appointments_PatId_AppDate_idx')
CREATE INDEX [Appointments_PatId_AppDate_idx] ON [dbo].[Appointments]([PatientId] DESC, [AppDate] DESC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientClinical_PatId_ClinType_AppId_idx')
CREATE INDEX [PatientClinical_PatId_ClinType_AppId_idx] ON [dbo].[PatientClinical]([PatientId] DESC, [ClinicalType] DESC, [AppointmentId] DESC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientClinical_AppId_ClinType_idx')
CREATE INDEX [PatientClinical_AppId_ClinType_idx] ON [dbo].[PatientClinical]([AppointmentId] ASC, [ClinicalType] ASC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientClinical_PatId_FinDet_idx')
CREATE INDEX [PatientClinical_PatId_FinDet_idx] ON [dbo].[PatientClinical]([PatientId] ASC, [FindingDetail] ASC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientClinical_PatId_AppId_Sym_idx')
CREATE INDEX [PatientClinical_PatId_AppId_Sym_idx] ON [dbo].[PatientClinical]([PatientId] ASC, [AppointmentId] ASC, [Symptom] ASC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientDemographics_LastName_FirstName_idx')
CREATE INDEX [PatientDemographics_LastName_FirstName_idx] ON [dbo].[PatientDemographics]([LastName] ASC, [FirstName] ASC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientFinancial_PatientId_Status_idx')
CREATE INDEX [PatientFinancial_PatientId_Status_idx] ON [dbo].[PatientFinancial]([PatientId] DESC, [Status] ASC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientNotes_PatId_NoteType_AppId_idx')
CREATE INDEX [PatientNotes_PatId_NoteType_AppId_idx] ON [dbo].[PatientNotes]([PatientId] DESC, [Notetype] DESC, [AppointmentId] DESC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientReceivablePayments_RecId_idx')
CREATE INDEX [PatientReceivablePayments_RecId_idx] ON [dbo].[PatientReceivablePayments]([ReceivableId] DESC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientReceivables_PatId_AppId_idx')
CREATE INDEX [PatientReceivables_PatId_AppId_idx] ON [dbo].[PatientReceivables]([PatientId] DESC, [AppointmentId] DESC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientReceivableServices_Invoice_idx')
CREATE INDEX [PatientReceivableServices_Invoice_idx] ON [dbo].[PatientReceivableServices]([Invoice] DESC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientReferral_PatId_idx')
CREATE INDEX [PatientReferral_PatId_idx] ON [dbo].[PatientReferral]([PatientId] DESC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PracticeActivity_PatId_idx')
CREATE INDEX [PracticeActivity_PatId_idx] ON [dbo].[PracticeActivity]([PatientId] DESC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PracticeAffiliations_ResId_idx')
CREATE INDEX [PracticeAffiliations_ResId_idx] ON [dbo].[PracticeAffiliations]([ResourceId] DESC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PracticeCodeTable_RefType_idx')
CREATE INDEX [PracticeCodeTable_RefType_idx] ON [dbo].[PracticeCodeTable]([ReferenceType] DESC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PracticeInsurers_InsName_idx')
CREATE INDEX [PracticeInsurers_InsName_idx] ON [dbo].[PracticeInsurers]([InsurerName] ASC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PracticeServices_Code_idx')
CREATE INDEX [PracticeServices_Code_idx] ON [dbo].[PracticeServices]([Code] ASC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PracticeTransactionJournal_TranType_TranTypeId_idx')
CREATE INDEX [PracticeTransactionJournal_TranType_TranTypeId_idx] ON [dbo].[PracticeTransactionJournal]([TransactionType] ASC, [TransactionTypeId] ASC)
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PracticeVendors_VendLast_VendFirst_idx')
CREATE INDEX [PracticeVendors_VendLast_VendFirst_idx] ON [dbo].[PracticeVendors]([VendorLastName] ASC, [VendorFirstName] ASC)
GO

