﻿-- More PatientReceivables fixes

--pr.InsuredId = 0 but received insurance payments; update InsuredId with PatientId if there's a policy
UPDATE pr
SET pr.InsuredId = pr.PatientId
FROM PatientReceivables pr
INNER JOIN PatientReceivablePayments prp ON prp.ReceivableId = pr.ReceivableId
INNER JOIN PatientFinancial pf ON pf.PatientId = pr.PatientId
	AND PayerId = pf.FinancialInsurerId
	AND InvoiceDate >= FinancialStartDate
WHERE InsuredId = 0
	AND PayerType = 'I'
