﻿--Add metadata for new table InsurerClaimForms, including ClaimFormTypes (ENUM and table)

--ClaimFormTypes
SET IDENTITY_INSERT [model].[ClaimFormTypes] ON
INSERT INTO [model].[ClaimFormTypes] (Id, Name, MethodSentId) VALUES (1, 'CMS-1500', 2)
INSERT INTO [model].[ClaimFormTypes] (Id, Name, MethodSentId) VALUES (2, 'UB-04', 2)
INSERT INTO [model].[ClaimFormTypes] (Id, Name, MethodSentId) VALUES (3, '837 Professional', 1)
INSERT INTO [model].[ClaimFormTypes] (Id, Name, MethodSentId) VALUES (4, '837 Institutional', 1)
SET IDENTITY_INSERT [model].[ClaimFormTypes] OFF

--InsurerClaimForms
--Adds UB04 and 837I to all BCBS plans
INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived) 
SELECT 2, InsurerId, 2, 0
FROM dbo.PracticeInsurers 
WHERE InsurerReferenceCode = 'P' OR InsurerName LIKE '%OXFORD%'

INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId, IsArchived) 
SELECT 4, InsurerId, 2, 0
FROM dbo.PracticeInsurers 
WHERE InsurerReferenceCode = 'P' OR InsurerName LIKE '%OXFORD%'


--Add Revenue Codes
INSERT INTO [model].[RevenueCodes]
           ([Code]
           ,[Description])
VALUES ('0490', 'Ambulatory Surgical Care')



--Assigns revenue codes to all surgery CPTs
UPDATE ps 
SET ps.RevenueCodeId = rc.Id
FROM dbo.PracticeServices ps
INNER JOIN model.RevenueCodes rc ON rc.Code = '0490'
WHERE ps.Code like '6%'