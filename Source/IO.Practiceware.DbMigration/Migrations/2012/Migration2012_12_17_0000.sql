﻿IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'Devices')
BEGIN 
	CREATE TABLE [dbo].[Devices](
		[DeviceID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
		[Make] [nvarchar](50) NULL,
		[Model] [nvarchar](50) NULL,
		[DeviceAlias] [nvarchar](50) NULL,
		[Type] [nvarchar](50) NULL,
		[Baud] [int] NULL,
		[Parity] [nvarchar](50) NULL,
		[DataBits] [int] NULL,
		[StopBits] [int] NULL
	 CONSTRAINT [PK_Devices] PRIMARY KEY CLUSTERED 
	(
		[DeviceID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

END

GO

-- To Add Devices names to the Devices Table for IOConnected Devices.

-- For Task 3405
IF NOT EXISTS(Select TOP 1 * from dbo.Devices WHERE DeviceAlias='TopconKR9000')
BEGIN
	INSERT INTO dbo.Devices (Make, Model, DeviceAlias, Type, Baud, Parity, DataBits, Stopbits) VALUES('Topcon','KR-9000','TopconKR9000','Autorefractor','2400','none','8', '2') 
END 

GO

--For Task 3515
IF NOT EXISTS(Select TOP 1 * from dbo.Devices WHERE DeviceAlias='TomeyTL2000B')
BEGIN
	INSERT INTO dbo.Devices (Make, Model, DeviceAlias, Type, Baud, Parity, DataBits, Stopbits) VALUES('TOMEY','TomeyTL-2000B','TomeyTL2000B','Lensometer','38400','none','8','1') 
END 

GO
