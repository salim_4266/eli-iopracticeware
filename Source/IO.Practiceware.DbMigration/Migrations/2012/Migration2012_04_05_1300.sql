﻿--Creation of Document and Printer table.

-- Creating table 'Documents'
CREATE TABLE [model].[Documents] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DocumentTypeId] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Printers'
CREATE TABLE [model].[Printers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [UncPath] nvarchar(max)  NOT NULL,
    [ServiceLocationId] int  NOT NULL
);
GO


-- Creating table 'DocumentPrinter'
CREATE TABLE [model].[DocumentPrinter] (
    [Documents_Id] int  NOT NULL,
    [Printers_Id] int  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'Documents'
ALTER TABLE [model].[Documents]
ADD CONSTRAINT [PK_Documents]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Printers'
ALTER TABLE [model].[Printers]
ADD CONSTRAINT [PK_Printers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO


-- Creating primary key on [Documents_Id], [Printers_Id] in table 'DocumentPrinter'
ALTER TABLE [model].[DocumentPrinter]
ADD CONSTRAINT [PK_DocumentPrinter]
    PRIMARY KEY NONCLUSTERED ([Documents_Id], [Printers_Id] ASC);
GO

-- Creating foreign key on [Documents_Id] in table 'DocumentPrinter'
ALTER TABLE [model].[DocumentPrinter]
ADD CONSTRAINT [FK_DocumentPrinter_Document]
    FOREIGN KEY ([Documents_Id])
    REFERENCES [model].[Documents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Printers_Id] in table 'DocumentPrinter'
ALTER TABLE [model].[DocumentPrinter]
ADD CONSTRAINT [FK_DocumentPrinter_Printer]
    FOREIGN KEY ([Printers_Id])
    REFERENCES [model].[Printers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DocumentPrinter_Printer'
CREATE INDEX [IX_FK_DocumentPrinter_Printer]
ON [model].[DocumentPrinter]
    ([Printers_Id]);
GO

--Creating Column FileServer 
IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'Resources' AND sc.Name = 'FileServer') 
ALTER TABLE [dbo].[Resources] ADD FileServer nvarchar(max)

GO

IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'PracticeName' AND sc.Name = 'FileServer') 
ALTER TABLE [dbo].[PracticeName] ADD FileServer nvarchar(max)

GO


