﻿

-- Creating table 'RevenueCodes'
IF NOT EXISTS(SELECT * FROM sysobjects so WHERE so.name = 'model.RevenueCodes')
BEGIN
CREATE TABLE [model].[RevenueCodes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL
);
END
GO

-- Creating primary key on [Id] in table 'RevenueCodes'
ALTER TABLE [model].[RevenueCodes]
ADD CONSTRAINT [PK_RevenueCodes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO


-- Add RevenueCodeId to dbo.PracticeServices
IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'PracticeServices' AND sc.Name = 'RevenueCodeId')
BEGIN
    ALTER TABLE dbo.PracticeServices
    ADD RevenueCodeId int
END
GO


-- Creating table 'InsurerClaimForms'
IF NOT EXISTS(SELECT * FROM sysobjects so WHERE so.name = 'model.InsurerClaimForms')
BEGIN
CREATE TABLE [model].[InsurerClaimForms] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IsArchived] bit  NOT NULL,
    [InvoiceTypeId] int  NOT NULL,
    [InsurerId] int  NOT NULL,
    [ClaimFormTypeId] int  NOT NULL
);
END
GO

-- Creating primary key on [Id] in table 'InsurerClaimForms'
ALTER TABLE [model].[InsurerClaimForms]
ADD CONSTRAINT [PK_InsurerClaimForms]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO



-- Creating table 'ClaimFormTypes'
IF NOT EXISTS(SELECT * FROM sysobjects so WHERE so.name = 'model.ClaimFormTypes')
BEGIN
CREATE TABLE [model].[ClaimFormTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [MethodSentId] int  NOT NULL
);
END
GO

ALTER TABLE [model].[ClaimFormTypes] WITH CHECK ADD CONSTRAINT CK_ClaimFormTypes_MethodSent
    CHECK (MethodSentId BETWEEN 1 AND 2)

GO

ALTER TABLE [model].InsurerClaimForms WITH CHECK ADD CONSTRAINT CK_InsurerClaimForms_InvoiceType
    CHECK (InvoiceTypeId BETWEEN 1 AND 5)

GO

-- Creating primary key on [Id] in table 'ClaimFormTypes'
ALTER TABLE [model].[ClaimFormTypes]
ADD CONSTRAINT [PK_ClaimFormTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO


-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerClaimFormInsurer'
CREATE INDEX [IX_FK_InsurerClaimFormInsurer]
ON [model].[InsurerClaimForms]
    ([InsurerId]);
GO

-- Creating foreign key on [ClaimFormTypeId] in table 'InsurerClaimForms'
ALTER TABLE [model].[InsurerClaimForms]
ADD CONSTRAINT [FK_InsurerClaimFormClaimFormType]
    FOREIGN KEY ([ClaimFormTypeId])
    REFERENCES [model].[ClaimFormTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerClaimFormClaimFormType'
CREATE INDEX [IX_FK_InsurerClaimFormClaimFormType]
ON [model].[InsurerClaimForms]
    ([ClaimFormTypeId]);
GO
