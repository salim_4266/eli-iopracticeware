﻿IF OBJECT_ID('dbo.[DropColumnAndDefaultConstraints]') IS NULL
BEGIN
	EXEC('
		CREATE PROCEDURE [dbo].[DropColumnAndDefaultConstraints] 
			-- Add the parameters for the stored procedure here
			@tableName nvarchar(max), 
			@columnName nvarchar(max)
		AS
		BEGIN
			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			DECLARE @ConstraintName nvarchar(200)
			SELECT @ConstraintName = Name 
			FROM SYS.DEFAULT_CONSTRAINTS 
			WHERE PARENT_OBJECT_ID = OBJECT_ID(@tableName) 
				AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns WHERE NAME = (@columnName) 
				AND object_id = OBJECT_ID(@tableName))
			IF @ConstraintName IS NOT NULL
				EXEC(''ALTER TABLE '' + @tableName + '' DROP CONSTRAINT '' + @ConstraintName)

			IF EXISTS(SELECT * FROM sys.columns WHERE Name = @columnName  
				AND Object_ID = Object_ID(@tableName))
				EXEC(''ALTER TABLE '' + @tableName + '' DROP COLUMN '' + @columnName)       
		END
	')
END