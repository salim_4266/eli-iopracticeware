﻿using FluentMigrator;
using System;

namespace IO.Practiceware.DbMigration.Migrations
{
    [Migration(201207190534)]
    class Migration201207190534 : Migration
    {
        public override void Up()
        {
            Execute.Sql(
                @"
ALTER TABLE [model].[Printers] DROP CONSTRAINT [UC_Printers];
GO

-- Creating unique key on [Name] in table 'Printers'
ALTER TABLE [model].[Printers] ADD CONSTRAINT [UC_Printers_Name] UNIQUE (Name);
GO

-- Creating unique key on [UncPath] in table 'Printers'
ALTER TABLE [model].[Printers] ADD CONSTRAINT [UC_Printers_UncPath] UNIQUE (UncPath);
GO
");

        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
