﻿using FluentMigrator;
using System;

namespace IO.Practiceware.DbMigration.Migrations
{
    [Migration(201208070235)]
    public class Migration201208070235 : Migration
    {
        public override void Up()
        {
            Execute.Sql(
               @"
IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'Documents' AND sc.Name = 'DisplayName') 
ALTER TABLE [model].[Documents] ADD DisplayName nvarchar(500) not null Default '';
GO

UPDATE [model].[Documents] set DisplayName = Replace(Name, '.doc', '');
GO

UPDATE [model].[Documents] set DisplayName = 'Claim Proof of Filing' where Name = 'Proof';
GO

UPDATE [model].[Documents] set DisplayName = 'Auto Posting Exceptions Report' where Name = 'Transactions';
GO
");
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
