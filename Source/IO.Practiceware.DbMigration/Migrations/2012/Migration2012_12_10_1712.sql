﻿

--Drops IsExcludedFromTaskManager Column in Resources Table if it exists

IF EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'Resources' AND sc.Name = 'IsExcludedFromTaskManager') 
BEGIN
    EXEC DropColumnAndDefaultConstraints 'dbo.Resources', 'IsExcludedFromTaskManager'
END
GO

IF OBJECT_ID(N'[model].[TaskManagerUserConfigurations]', 'U') IS NOT NULL
    DROP TABLE [model].[TaskManagerUserConfigurations];
GO

-- Creating table 'TaskManagerUserConfigurations'
CREATE TABLE [model].[TaskManagerUserConfigurations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserId] int  NOT NULL,
    [IsDisabled] bit  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'TaskManagerUserConfigurations'
ALTER TABLE [model].[TaskManagerUserConfigurations]
ADD CONSTRAINT [PK_TaskManagerUserConfigurations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO