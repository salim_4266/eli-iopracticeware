﻿using FluentMigrator;
using IO.Practiceware.Configuration;
using Soaf;
using System;
using System.IO;

namespace IO.Practiceware.DbMigration.Migrations
{
    [Migration(201207311214)]
    public class Migration201207311214 : Migration
    {
        public override void Up()
        {
            Execute.Sql(
            @"
            IF OBJECT_ID('model.EligibilityConfigurations') IS NULL 
            BEGIN 
	            CREATE TABLE model.EligibilityConfigurations ( 
		            Id uniqueidentifier NOT NULL DEFAULT NEWID(), 
		            Archive270DirectoryPath nvarchar(500) NOT NULL, 
		            Output270FilePath nvarchar(500) NOT NULL, 
		            PRIMARY KEY(Id) 
	            ) 
            END 
            GO ");

            string archive270DirectoryPath = Path.Combine(ConfigurationManager.ServerDataPath, @"PatientEligibility\270Requests\Archive\");
            string output270FilePath = Path.Combine(Environment.CurrentDirectory, @"Submit\");
            string insertIntoQuery =
            @"
            INSERT INTO model.EligibilityConfigurations (Archive270DirectoryPath, Output270FilePath) 
            VALUES ('{0}', '{1}')".FormatWith(archive270DirectoryPath, output270FilePath);

            Execute.Sql(insertIntoQuery);
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
