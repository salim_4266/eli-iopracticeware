﻿-----Fix orphan payments by adding to PatientFinancial or fixing PatientReceivables

--Insurance Payments where there's no row in PatientFinancial for PatientId, InsuredId or PolicyPatientId
INSERT INTO PatientFinancial (
	PatientId
	,FinancialInsurerId
	,FinancialPerson
	,FinancialGroupId
	,FinancialCopay
	,FinancialPIndicator
	,FinancialStartDate
	,FinancialEndDate
	,[Status]
	,FinancialInsType
	)
SELECT pr.InsuredId AS PatientId
	,pr.InsurerId AS FinancialInsurerId
	,'xxx' AS FinancialPerson
	,'' AS FinancialGroupId
	,'' AS FinancialCopay
	,1 AS FinancialPIndicator
	,MIN(InvoiceDate) AS FinancialStartDate
	,MAX(PaymentDate) AS FinancialEndDate
	,'X' AS [Status]
	,'M' AS FinancialInsType
FROM PatientReceivables pr
INNER JOIN PatientReceivablePayments prp ON prp.ReceivableId = pr.ReceivableId
INNER JOIN PatientDemographics pd ON pd.patientid = pr.PatientId
WHERE PaymentFinancialType <> ''
	AND pr.InsurerId <> 99999
	AND PaymentId NOT IN (
		SELECT prp.PaymentId AS Id
		FROM dbo.PatientReceivablePayments prp
		INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
		WHERE prp.PaymentFinancialType <> ''
			AND ((prp.PayerType = 'I' AND pr.ComputedFinancialId IS NOT NULL)
				OR
				prp.PayerType <> 'I')
		)
	AND pr.PatientId NOT IN (
		SELECT patientid
		FROM patientfinancial
		)
	AND pr.InsuredId NOT IN (
		SELECT patientid
		FROM patientfinancial
		)
	AND pd.policypatientid NOT IN (
		SELECT patientid
		FROM patientfinancial
		)
GROUP BY pr.InsuredId, pr.InsurerId

--Receivables where InsuredId = 0 but there is a policy for the pr.InsurerId + either the pr.Patient, pd.PolicyPatientId or pd.SecondPolicyPatientId
UPDATE pr 
SET pr.InsuredId = pf.PatientId
FROM  PatientReceivables pr 
INNER JOIN PatientDemographics pd ON pd.PatientId = pr.PatientId
INNER JOIN PatientFinancial pf ON pr.InsurerId = pf.FinancialInsurerId
	AND (
		pr.PatientId = pf.PatientId 
		OR pd.PolicyPatientId = pf.PatientId 
		OR pd.SecondPolicyPatientId = pf.PatientId
		)
WHERE pr.Invoice IN (
		SELECT Invoice
		FROM PatientReceivables
		WHERE InsuredId = 0
			AND InsurerId > 0
			AND InsurerId <> 99999
		)

--Add a policy for receivables with payments, but no computed financial id.
INSERT INTO PatientFinancial (
	PatientId
	,FinancialInsurerId
	,FinancialPerson
	,FinancialGroupId
	,FinancialCopay
	,FinancialPIndicator
	,FinancialStartDate
	,FinancialEndDate
	,[Status]
	,FinancialInsType
	)
SELECT pr.InsuredId AS PatientId
	,pr.Insurerid AS FinancialInsurerId
	,'xxxx' AS FinancialPerson
	,'' AS FinancialGroupId
	,'' AS FinancialCopay
	,1 AS FInancialPIndicator
	,pr.InvoiceDate AS FinancialStartDate
	,PaymentDate AS FinancialEndDate
	,'X' AS STATUS
	,'M' AS FinancialInsType
FROM PatientReceivablePayments prp
INNER JOIN patientreceivables pr ON pr.ReceivableId = prp.ReceivableId
INNER JOIN PatientDemographics pd ON pd.patientid = pr.PatientId
INNER JOIN PatientFinancial pf ON pf.patientid = pd.PolicyPatientId
LEFT JOIN patientreceivables prPH ON prPH.Patientid = pf.Patientid
	AND prPH.PatientId <> pr.PatientId
WHERE PaymentFinancialType <> ''
	AND pr.InsurerId <> 99999
	AND paymentid NOT IN (
		SELECT prp.PaymentId AS Id
		FROM dbo.PatientReceivablePayments prp
		INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
		WHERE prp.PaymentFinancialType <> ''
			AND ((prp.PayerType = 'I' AND pr.ComputedFinancialId IS NOT NULL)
				OR
				prp.PayerType <> 'I')
		)
	AND pr.PatientId NOT IN (
		SELECT patientid
		FROM patientfinancial
		)
	AND pr.InsuredId NOT IN (
		SELECT patientid
		FROM patientfinancial
		)
	AND pf.financialinsurerid = prp.PayerId
	AND prph.patientid IS NULL

--Insert more PatientFinancial rows
INSERT INTO PatientFinancial (
	PatientId
	,FinancialInsurerId
	,FinancialPerson
	,FinancialGroupId
	,FinancialCopay
	,FinancialPIndicator
	,FinancialStartDate
	,FinancialEndDate
	,[Status]
	,FinancialInsType
	)
SELECT pr.InsuredId AS PatientId
	,pr.Insurerid AS FinancialInsurerId
	,'xxxxx' AS FinancialPerson
	,'' AS FinancialGroupId
	,'' AS FinancialCopay
	,1 AS FinancialPIndicator
	,MIN(InvoiceDate) AS FinancialStartDate
	,MAX(PaymentDate) AS FinancialEndDate
	,'X' AS [Status]
	,'M' AS FinancialInsType
FROM PatientReceivables pr
INNER JOIN PatientReceivablePayments prp ON prp.ReceivableId = pr.ReceivableId
INNER JOIN PatientDemographics pd ON pd.patientid = pr.PatientId
WHERE PaymentFinancialType <> ''
	AND InsurerId <> 99999
	AND PaymentId NOT IN (
		SELECT prp.PaymentId AS Id
		FROM dbo.PatientReceivablePayments prp
		INNER JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
		WHERE prp.PaymentFinancialType <> ''
			AND ((prp.PayerType = 'I' AND pr.ComputedFinancialId IS NOT NULL)
				OR
				prp.PayerType <> 'I')
		)
GROUP BY pr.InsuredId, pr.InsurerId