﻿DECLARE @triggerNames xml
EXEC dbo.DisableEnabledTriggers 'dbo.PatientPreCerts', @names = @triggerNames OUTPUT

DECLARE @constraintNames xml
EXEC dbo.DisableEnabledCheckConstraints 'dbo.PatientPreCerts', @names = @constraintNames OUTPUT

EXEC dbo.DropIndexes 'model.Adjustments_Partition1'
EXEC dbo.DropIndexes 'model.BillingServiceBillingDiagnosis_Partition1'
EXEC dbo.DropIndexes 'model.BillingServices_Partition1'
EXEC dbo.DropIndexes 'model.BillingServiceTransactions_Internal'
EXEC dbo.DropIndexes 'model.Comments_Partition8'
EXEC dbo.DropIndexes 'model.FinancialInformations_Internal'
EXEC dbo.DropIndexes 'model.InvoiceReceivables_Partition1'
EXEC dbo.DropIndexes 'model.InvoiceReceivables_Partition2'
EXEC dbo.DropIndexes 'model.InvoiceReceivables_Partition3'
EXEC dbo.DropIndexes 'model.InvoiceReceivables_Partition5'
EXEC dbo.DropIndexes 'model.PatientInsurances_Partition1'
EXEC dbo.DropIndexes 'model.PatientInsuranceAuthorizations_Internal'

IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'PatientPreCerts' AND sc.Name = 'ComputedFinancialId')
BEGIN
    ALTER TABLE dbo.PatientPreCerts
    ADD ComputedFinancialId int
END

IF OBJECT_ID('dbo.GetPatientPreCertsFinancialIds') IS NOT NULL
	EXEC('DROP FUNCTION dbo.GetPatientPreCertsFinancialIds')

IF OBJECT_ID('dbo.GetBulkPatientPreCertsFinancialIds') IS NOT NULL
	EXEC('DROP FUNCTION dbo.GetBulkPatientPreCertsFinancialIds')
	

DECLARE @createGetPatientPreCertsFinancialIds nvarchar(max)
SET @createGetPatientPreCertsFinancialIds = '
CREATE FUNCTION dbo.GetPatientPreCertsFinancialIds(@idsXml xml)
RETURNS @financialIds table(Id int PRIMARY KEY CLUSTERED, FinancialId int)
AS
BEGIN
	DECLARE @ids table(Id int PRIMARY KEY CLUSTERED)

	INSERT @ids(Id)
	SELECT DISTINCT n.Id.value(''.'', ''int'') 
	FROM @idsXml.nodes(''//Id'') as n(Id)

	-- Looks for valid patient insurance policies
	----Patient is policyholder
	INSERT @financialIds(Id, FinancialId)
	SELECT z.Id, MAX(z.FinancialId)
	FROM (
		SELECT pCert.PreCertId AS Id, pf.FinancialId AS FinancialId
		FROM dbo.PatientPreCerts pCert WITH(NOLOCK)
		INNER JOIN dbo.PatientDemographics pd WITH(NOLOCK) ON pd.PatientId = pCert.PatientId
		INNER JOIN dbo.PatientFinancial pf WITH(NOLOCK) ON pCert.PreCertInsId = pf.FinancialInsurerId
			AND dbo.IsDate112(pf.FinancialStartDate) = 1
			AND pCert.PreCertDate >= pf.FinancialStartDate
			AND (
				pCert.PreCertDate <= pf.FinancialEndDate
				OR pf.FinancialEndDate = ''''
				OR pf.FinancialEndDate IS NULL
				)
			AND (pf.PatientId = pd.PolicyPatientId
				OR pf.PatientId = pd.SecondPolicyPatientId)
			AND	pf.PatientId = pd.PatientId
		
		UNION ALL

		----Patient is not first policyholder
		SELECT pCert.PreCertId AS Id, pf.FinancialId AS FinancialId
		FROM dbo.PatientPreCerts pCert WITH(NOLOCK)
		INNER JOIN dbo.PatientDemographics pd WITH(NOLOCK) ON pd.PatientId = pCert.PatientId
		INNER JOIN dbo.PracticeInsurers pri WITH(NOLOCK)ON pri.InsurerId = pCert.PreCertInsId
		INNER JOIN dbo.PatientFinancial pf WITH(NOLOCK) ON pCert.PreCertInsId = pf.FinancialInsurerId
			AND dbo.IsDate112(pf.FinancialStartDate) = 1
			AND pCert.PreCertDate >= pf.FinancialStartDate
			AND (
				pCert.PreCertDate <= pf.FinancialEndDate
				OR pf.FinancialEndDate = ''''
				OR pf.FinancialEndDate IS NULL
				)
			AND pd.PolicyPatientId <> pd.PatientId
			AND pd.PolicyPatientId = pf.PatientId
			AND pd.PolicyPatientId <> 0
			AND pri.AllowDependents = 1
		
		UNION ALL

		----Patient is not second policyholder
		SELECT pCert.PreCertId AS Id, pf.FinancialId AS FinancialId
		FROM dbo.PatientPreCerts pCert WITH(NOLOCK)
		INNER JOIN dbo.PatientDemographics pd WITH(NOLOCK)ON pd.PatientId = pCert.PatientId
		INNER JOIN dbo.PracticeInsurers pri WITH(NOLOCK) ON pri.InsurerId = pCert.PreCertInsId
		INNER JOIN dbo.PatientFinancial pf WITH(NOLOCK) ON pCert.PreCertInsId = pf.FinancialInsurerId
			AND dbo.IsDate112(pf.FinancialStartDate) = 1
			AND pCert.PreCertDate >= pf.FinancialStartDate
			AND (
				pCert.PreCertDate <= pf.FinancialEndDate
				OR pf.FinancialEndDate = ''''
				OR pf.FinancialEndDate IS NULL
				)
			AND pd.SecondPolicyPatientId <> pd.PatientId
			AND pd.SecondPolicyPatientId = pf.PatientId
			AND pd.SecondPolicyPatientId <> 0
			AND pri.AllowDependents = 1
	) z
	INNER JOIN @ids i ON z.Id = i.Id
	GROUP BY z.Id
		
	DELETE @ids
	WHERE Id IN (SELECT Id FROM @financialIds)
        
	-- Was billed but is no longer a valid policy
	INSERT @financialIds(Id, FinancialId)
	SELECT pCert.PreCertId, MAX(pr.ComputedFinancialId)
	FROM dbo.PatientPreCerts pCert
	INNER JOIN @ids i ON pCert.PreCertId = i.Id
	INNER JOIN dbo.Appointments ap WITH(NOLOCK) ON ap.PreCertId = pCert.PreCertId
	INNER JOIN dbo.PatientReceivables pr WITH(NOLOCK) ON pr.AppointmentId = ap.AppointmentId
	INNER JOIN dbo.PracticeTransactionJournal ptj WITH(NOLOCK) ON ptj.TransactionTypeId = pr.ReceivableId 
	INNER JOIN dbo.ServiceTransactions st WITH(NOLOCK) ON st.TransactionId = ptj.TransactionId
	WHERE ptj.TransactionType = ''R''
		AND st.TransportAction IN (''1'', ''2'')
	GROUP BY pCert.PreCertId
		
	DELETE @ids
	WHERE Id IN (SELECT Id FROM @financialIds)

	INSERT @financialIds(Id, FinancialId)
	SELECT pCert.PreCertId, MAX(pr.ComputedFinancialId)
	FROM dbo.PatientPreCerts pCert WITH(NOLOCK)
	INNER JOIN @ids i ON pCert.PreCertId = i.Id
	INNER JOIN dbo.Appointments ap WITH(NOLOCK) ON ap.PreCertId = pCert.PreCertId
	INNER JOIN dbo.PatientReceivables pr WITH(NOLOCK) ON pr.AppointmentId = ap.AppointmentId
	INNER JOIN dbo.PatientReceivablePayments prp WITH(NOLOCK) ON prp.ReceivableId = pr.ReceivableId
	GROUP BY pCert.PreCertId
		
	DELETE @ids
	WHERE Id IN (SELECT Id FROM @financialIds)
	        
	INSERT @financialIds(Id, FinancialId)
	SELECT i.Id, NULL
	FROM @ids i

	RETURN
END
'

EXEC(@createGetPatientPreCertsFinancialIds)

DECLARE @createGetBulkPatientPreCertsFinancialIds nvarchar(max)
SET @createGetPatientPreCertsFinancialIds = REPLACE(REPLACE(@createGetPatientPreCertsFinancialIds, 'INNER JOIN', 'INNER HASH JOIN'), 'GetPatientPreCertsFinancialIds', 'GetBulkPatientPreCertsFinancialIds')

EXEC (@createGetPatientPreCertsFinancialIds)

IF OBJECT_ID('PatientPreCertsFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientPreCertsFinancialIdTrigger')
END

IF OBJECT_ID('PatientFinancialPatientPreCertsFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientFinancialPatientPreCertsFinancialIdTrigger')
END

EXEC('
DECLARE @xml xml
SET @xml = (
	SELECT PreCertId AS Id
	FROM PatientPreCerts
	FOR XML PATH('''')
)

UPDATE pCert
SET pCert.ComputedFinancialId = pCertpf.FinancialId
FROM PatientPreCerts pCert
INNER JOIN 
(
	SELECT Id, FinancialId 
	FROM
	dbo.GetBulkPatientPreCertsFinancialIds(@xml)
) pCertpf
ON pCertpf.Id = pCert.PreCertId 
')

EXEC('CREATE TRIGGER [dbo].[PatientPreCertsFinancialIdTrigger]
ON [dbo].[PatientPreCerts]
FOR INSERT, UPDATE
NOT FOR REPLICATION
AS
SET NOCOUNT ON;

DECLARE @changedIds table(Id int)
INSERT @changedIds
SELECT PreCertId FROM
(SELECT * FROM inserted EXCEPT SELECT * FROM deleted) c

IF EXISTS(SELECT * FROM @changedIds) AND dbo.GetNoRecompute() = 0
BEGIN

	SELECT 1 AS Value INTO #NoRecompute

	DECLARE @xml xml
	SET @xml = (
		SELECT Id AS Id
		FROM @changedIds
		FOR XML PATH('''')
	)

	SELECT Id, FinancialId 
	INTO #financialIds
	FROM dbo.GetPatientPreCertsFinancialIds(@xml) f
	JOIN PatientPreCerts pCert ON f.Id = pCert.PreCertId
	WHERE COALESCE(ComputedFinancialId, '''') <> COALESCE(f.FinancialId, '''')

	IF EXISTS(SELECT * FROM #financialIds)	
	BEGIN
		EXEC(''UPDATE pCert
		SET pCert.ComputedFinancialId = f.FinancialId
		FROM #financialIds f
		JOIN PatientPreCerts pCert ON f.Id = pCert.PreCertId'')
	END
		
	DROP TABLE #financialIds
    
	DROP TABLE #NoRecompute

END

')

EXEC('CREATE TRIGGER [dbo].[PatientFinancialPatientPreCertsFinancialIdTrigger]
ON [dbo].[PatientFinancial]
FOR INSERT, UPDATE
NOT FOR REPLICATION
AS
SET NOCOUNT ON;

DECLARE @changedIds table(Id int, UNIQUE CLUSTERED(Id))
INSERT @changedIds
SELECT DISTINCT PatientId FROM
(SELECT * FROM inserted EXCEPT SELECT * FROM deleted) c

IF EXISTS(SELECT * FROM @changedIds) AND dbo.GetNoRecompute() = 0
BEGIN
	
	SELECT 1 AS Value INTO #NoRecompute

	DECLARE @patientIds table(Id int, UNIQUE CLUSTERED(Id))

	INSERT INTO @patientIds
	SELECT pd.PatientId
	FROM dbo.PatientDemographics pd WITH(NOLOCK)
	INNER JOIN @changedIds i
	ON i.Id = pd.PolicyPatientId
	
	UNION 

	SELECT pd.PatientId
	FROM dbo.PatientDemographics pd WITH(NOLOCK)
	INNER JOIN @changedIds i
	ON i.Id = pd.SecondPolicyPatientId

	UNION 

	SELECT pr.PatientId
	FROM dbo.PatientReceivables pr WITH(NOLOCK)
	INNER JOIN @changedIds i
	ON i.Id = pr.InsuredId

	DECLARE @xml xml
	SET @xml = (
		SELECT PreCertId AS Id
		FROM @patientIds p 
		INNER JOIN PatientPreCerts pCert WITH(NOLOCK) ON p.Id = pCert.PatientId
		FOR XML PATH('''')
	)
    
	SELECT Id, FinancialId 
	INTO #financialIds
	FROM dbo.GetPatientPreCertsFinancialIds(@xml)

	EXEC(''
	UPDATE pCert
	SET pCert.ComputedFinancialId = pCertpf.FinancialId
	FROM PatientPreCerts pCert WITH(NOLOCK) 
	INNER JOIN #financialIds pCertpf
	ON pCertpf.Id = pCert.PreCertId 
	WHERE COALESCE(pCert.ComputedFinancialId,'''''''') <> COALESCE(pCertpf.FinancialId,'''''''')
	'')
	
	DROP TABLE #financialIds
	
	DROP TABLE #NoRecompute
END

')

EXEC dbo.EnableTriggers @triggerNames
EXEC dbo.EnableCheckConstraints @constraintNames