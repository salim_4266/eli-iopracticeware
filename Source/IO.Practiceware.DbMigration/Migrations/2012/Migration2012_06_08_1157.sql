﻿-- Cleanup deprecated trigger in case it's left hanging around.
IF OBJECT_ID('dbo.LogEntriesServerNameInsertTrigger') IS NOT NULL
BEGIN
	EXEC dbo.DropObject 'dbo.LogEntriesServerNameInsertTrigger'
END


IF OBJECT_ID('dbo.LogEntries') IS NOT NULL 
BEGIN
	IF COL_LENGTH('LogEntries', 'ServerName') IS NULL
	BEGIN
		DECLARE @sql nvarchar(max)
		SET @sql = 'ALTER TABLE dbo.LogEntries
					ADD ServerName nvarchar(500) NOT NULL DEFAULT convert(nvarchar, ServerProperty(''ServerName''))'
		EXEC(@sql)
	END
	ELSE IF NOT EXISTS(SELECT * FROM sys.all_columns ac
					   INNER JOIN sys.tables t ON ac.object_id = t.object_id
					   INNER JOIN sys.default_constraints c ON ac.default_object_id = c.object_id
					   WHERE t.name='LogEntries' AND ac.name = 'ServerName')
	BEGIN
		SET @sql = 'ALTER TABLE dbo.LogEntries
					ADD CONSTRAINT defaultServerNameConstraint
					DEFAULT convert(nvarchar, ServerProperty(''ServerName'')) FOR ServerName'
		EXEC(@sql)
	END
END



IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PracticeActivityCheckoutExternalSystemMessagesTrigger]') AND OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[PracticeActivityCheckoutExternalSystemMessagesTrigger]
GO

CREATE TRIGGER dbo.PracticeActivityCheckoutExternalSystemMessagesTrigger 
   ON  dbo.PracticeActivity
   FOR UPDATE NOT FOR REPLICATION
AS 
BEGIN
SET NOCOUNT ON;

IF UPDATE(Status)
	BEGIN
		DECLARE @now as DATETIME
		SET @now = GETDATE()

		DECLARE @insertTable TABLE(Id UNIQUEIDENTIFIER, MessageType INT, AppointmentId INT, Description NVARCHAR(255))

		-- Create rows for each ExternalSystemExternalSystemMessageType subscribed for DFT_P03_V231s, ORM_O01_V231
		INSERT INTO @insertTable 
		SELECT NEWID(), esesmt.Id, i.AppointmentId, ESMT.Name
		FROM 
		DELETED d INNER JOIN inserted i on d.ActivityId = i.ActivityId
		CROSS JOIN model.ExternalSystemExternalSystemMessageTypes esesmt WITH(NOLOCK) 
		INNER JOIN model.ExternalSystemMessageTypes esmt WITH(NOLOCK) ON esesmt.ExternalSystemMessageTypeId = esmt.Id AND esmt.Name IN ('DFT_P03_V231', 'ORM_O01_V231') AND esmt.IsOutbound = 1
		WHERE 
		i.Status = 'D' AND (d.Status = 'G' OR d.Status = 'H' OR d.Status = 'U')


		-- Create the messags
		INSERT INTO model.ExternalSystemMessages(Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId, ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId)
		SELECT Id, Description, '', MessageType, 1, 0, 'Checkout', @now, @now, ''
		FROM @insertTable

		-- Add entity associated with each message for AppointmentId
		INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
		SELECT NEWID(), it.Id, pre.Id, AppointmentId
		FROM
		@insertTable it INNER JOIN model.PracticeRepositoryEntities pre ON pre.Name = 'Appointment'
	END
END
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PatientDemographicsExternalSystemMessagesTrigger]') AND OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].PatientDemographicsExternalSystemMessagesTrigger
GO

CREATE TRIGGER dbo.PatientDemographicsExternalSystemMessagesTrigger
   ON  dbo.PatientDemographics
   FOR INSERT, UPDATE NOT FOR REPLICATION
AS 
BEGIN
SET NOCOUNT ON;

	DECLARE @now as DATETIME
	SET @now = GETDATE()

	DECLARE @insertTable TABLE(Id UNIQUEIDENTIFIER, MessageType INT, PatientId INT, [Description] NVARCHAR(255))
	DECLARE @createdBy nvarchar(max)

		INSERT INTO @insertTable 
		SELECT NEWID(), esesmt.Id, i.PatientId, ESMT.Name
		FROM (SELECT [PatientId],[LastName],[FirstName],[MiddleInitial],[NameReference],[SocialSecurity],[Address],[Suite],[City],[State],[Zip],[HomePhone],[CellPhone],[Email],[EmergencyName],[EmergencyPhone],[EmergencyRel],[Occupation],[BusinessName],[BusinessAddress],[BusinessSuite],[BusinessCity],[BusinessState],[BusinessZip],[BusinessPhone],[BusinessType],[Gender],[Marital],[BirthDate],[NationalOrigin],[Language],[SchedulePrimaryDoctor],[PrimaryCarePhysician],[ReferringPhysician],[ProfileComment1],[ProfileComment2],[ContactType],[PolicyPatientId],[Relationship],[BillParty],[SecondPolicyPatientId],[SecondRelationship],[SecondBillParty],[ReferralCatagory],[BulkMailing],[FinancialAssignment],[FinancialSignature],[FinancialSignatureDate],[Status],[Salutation],[Hipaa],[PatType],[BillingOffice],[SendStatements],[SendConsults],[OldPatient],[ReferralRequired],[MedicareSecondary],[PostOpExpireDate],[PostOpService],[EmployerPhone],[Religion],[Race],[Ethnicity] FROM inserted EXCEPT SELECT [PatientId],[LastName],[FirstName],[MiddleInitial],[NameReference],[SocialSecurity],[Address],[Suite],[City],[State],[Zip],[HomePhone],[CellPhone],[Email],[EmergencyName],[EmergencyPhone],[EmergencyRel],[Occupation],[BusinessName],[BusinessAddress],[BusinessSuite],[BusinessCity],[BusinessState],[BusinessZip],[BusinessPhone],[BusinessType],[Gender],[Marital],[BirthDate],[NationalOrigin],[Language],[SchedulePrimaryDoctor],[PrimaryCarePhysician],[ReferringPhysician],[ProfileComment1],[ProfileComment2],[ContactType],[PolicyPatientId],[Relationship],[BillParty],[SecondPolicyPatientId],[SecondRelationship],[SecondBillParty],[ReferralCatagory],[BulkMailing],[FinancialAssignment],[FinancialSignature],[FinancialSignatureDate],[Status],[Salutation],[Hipaa],[PatType],[BillingOffice],[SendStatements],[SendConsults],[OldPatient],[ReferralRequired],[MedicareSecondary],[PostOpExpireDate],[PostOpService],[EmployerPhone],[Religion],[Race],[Ethnicity] FROM deleted) i
		CROSS JOIN model.ExternalSystemExternalSystemMessageTypes esesmt
		INNER JOIN model.ExternalSystemMessageTypes esmt ON esesmt.ExternalSystemMessageTypeId = esmt.Id AND esmt.Name IN ('ADT_A28_V23') AND esmt.IsOutbound = 1


	-- Create rows for each ExternalSystemExternalSystemMessageType subscribed for ADT_A28_V23
	-- If deleted table contains rows, then this was an UPDATE
	IF EXISTS(SELECT * FROM deleted)
	BEGIN
		SET @createdBy = 'PatientUpdate'
	END
	ELSE
	BEGIN
		SET @createdBy = 'PatientInsert'
	END

	-- Create the messages
	INSERT INTO model.ExternalSystemMessages(Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId, ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId)
	-- Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId (1 = Unprocessed, 2 = Processing, 3 = Processed),
	-- ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId
	SELECT Id, [Description], '', MessageType, 1, 0, @createdBy, @now, @now, ''
	FROM @insertTable

	-- Add entity associated with each message for PatientId
	INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
	-- Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey
	SELECT NEWID(), it.Id, pre.Id, PatientId
	FROM
	@insertTable it INNER JOIN model.PracticeRepositoryEntities pre ON pre.Name = 'Patient'
END
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AppointmentExternalSystemMessagesTrigger]') AND OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].AppointmentExternalSystemMessagesTrigger
GO

CREATE TRIGGER dbo.AppointmentExternalSystemMessagesTrigger
   ON  dbo.Appointments
   FOR INSERT, UPDATE NOT FOR REPLICATION
AS 
BEGIN
SET NOCOUNT ON;

	DECLARE @now as DATETIME
	SET @now = GETDATE()

	DECLARE @insertTable TABLE(Id UNIQUEIDENTIFIER, MessageType INT, AppointmentId INT, [Description] NVARCHAR(255))
	DECLARE @createdBy nvarchar(max)

	INSERT INTO @insertTable 
	SELECT NEWID(), esesmt.Id, i.AppointmentId, ESMT.Name
	FROM (SELECT [AppointmentId],[PatientId],[AppTypeId],[AppDate],[AppTime],[Duration],[ReferralId],[PreCertId],[TechApptTypeId],[ScheduleStatus],[ActivityStatus],[Comments],[ResourceId1],[ResourceId2],[ResourceId3],[ResourceId4],[ResourceId5],[ResourceId6],[ResourceId7],[ResourceId8],[ApptInsType],[ConfirmStatus],[ApptTypeCat],[SetDate],[VisitReason] FROM inserted EXCEPT SELECT [AppointmentId],[PatientId],[AppTypeId],[AppDate],[AppTime],[Duration],[ReferralId],[PreCertId],[TechApptTypeId],[ScheduleStatus],[ActivityStatus],[Comments],[ResourceId1],[ResourceId2],[ResourceId3],[ResourceId4],[ResourceId5],[ResourceId6],[ResourceId7],[ResourceId8],[ApptInsType],[ConfirmStatus],[ApptTypeCat],[SetDate],[VisitReason] FROM deleted) i
	CROSS JOIN model.ExternalSystemExternalSystemMessageTypes esesmt WITH(NOLOCK) 
	INNER JOIN model.ExternalSystemMessageTypes esmt WITH(NOLOCK) ON esesmt.ExternalSystemMessageTypeId = esmt.Id AND esmt.Name IN ('ADT_A28_V23') AND esmt.IsOutbound = 1

	-- Create rows for each ExternalSystemExternalSystemMessageType subscribed for ADT_A28_V23
	-- If deleted table contains rows, then this was an UPDATE
	IF EXISTS(SELECT * FROM deleted)
	BEGIN
		SET @createdBy = 'AppointmentUpdate'
	END
	ELSE
	BEGIN
		SET @createdBy = 'AppointmentInsert'
	END

	-- Create the messages
	INSERT INTO model.ExternalSystemMessages(Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId, ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId)
	-- Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId (1 = Unprocessed, 2 = Processing, 3 = Processed),
	-- ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId
	SELECT Id, [Description], '', MessageType, 1, 0, @createdBy, @now, @now, ''
	FROM @insertTable

	-- Add entity associated with each message for AppointmentId
	INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
	-- Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey
	SELECT NEWID(), it.Id, pre.Id, AppointmentId
	FROM
	@insertTable it INNER JOIN model.PracticeRepositoryEntities pre ON pre.Name = 'Appointment'
END
GO