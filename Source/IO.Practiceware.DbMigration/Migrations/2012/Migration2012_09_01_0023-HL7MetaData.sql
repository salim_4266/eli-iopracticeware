﻿-----New HL7 MessageTypes

IF NOT EXISTS(SELECT * FROM model.ExternalSystemMessageTypes WHERE Name = 'ADT_A28_V231')
INSERT INTO model.ExternalSystemMessageTypes
(Name, Description, IsOutbound)
SELECT 'ADT_A28_V231', 'Add Patient', 0

IF NOT EXISTS(SELECT * FROM model.ExternalSystemMessageTypes WHERE Name = 'ADT_A31_V231')
INSERT INTO model.ExternalSystemMessageTypes
(Name, Description, IsOutbound)
SELECT 'ADT_A31_V231', 'Add/Update Patient', 0

IF NOT EXISTS(SELECT * FROM model.ExternalSystemMessageTypes WHERE Name = 'ADT_A04_V231')
INSERT INTO model.ExternalSystemMessageTypes
(Name, Description, IsOutbound)
SELECT 'ADT_A04_V231', 'Add/Update Patient', 0

IF NOT EXISTS(SELECT * FROM model.ExternalSystemMessageTypes WHERE Name = 'ADT_A08_V231')
INSERT INTO model.ExternalSystemMessageTypes
(Name, Description, IsOutbound)
SELECT 'ADT_A08_V231', 'Add/Update Patient', 0

IF NOT EXISTS(SELECT * FROM model.ExternalSystemMessageTypes WHERE Name = 'SIU_S12_V231')
INSERT INTO model.ExternalSystemMessageTypes
(Name, Description, IsOutbound)
SELECT 'SIU_S12_V231', 'Add Appointment', 0