﻿--To fix 3316.Changed InvId parameter length from 16 to 32.

/****** Object:  StoredProcedure [dbo].[usp_FinancialData6]    Script Date: 11/21/2012 2:11:44 AM ******/
IF EXISTS (
		SELECT *
		FROM dbo.sysobjects
		WHERE id = OBJECT_ID(N'dbo.usp_FinancialData6')
			AND OBJECTPROPERTY(id, N'IsProcedure') = 1
		)
	DROP PROCEDURE dbo.usp_FinancialData6
GO

/****** Object:  StoredProcedure [dbo].[usp_FinancialData6]    Script Date: 11/21/2012 2:11:44 AM ******/
SET ANSI_NULLS ON
GO


IF NOT EXISTS (
		SELECT *
		FROM dbo.sysobjects
		WHERE id = OBJECT_ID(N'dbo.usp_FinancialData6')
			AND OBJECTPROPERTY(id, N'IsProcedure') = 1
		)
BEGIN
	EXEC dbo.sp_executesql @statement = 
		N'
CREATE PROCEDURE dbo.usp_FinancialData6
(
@PatId int,
@InvId nvarchar(32),
@Bal int
)
AS
SELECT distinct
pr.Invoice, 
pr.InvoiceDate, pr.AppointmentId,
pf.Referral, 
pr.BillToDr as BillDrId, 
bd.ResourceName as BillDrName,
pr.BillingOffice as SchAttId,
ap.ResourceId1 as SchDrId,
sd.ResourceName as SchDrName,
ap.ResourceId2 as SchLocId,
pr.ReferDr as RefDrId,
rf.VendorLastName as RefDrName
FROM PatientReceivables pr 
INNER JOIN PatientDemographics pd ON pd.PatientId=pr.PatientId 
INNER JOIN Appointments ap ON pr.AppointmentId=ap.AppointmentId 
LEFT JOIN PatientReferral pf ON ap.ReferralId=pf.ReferralId
LEFT JOIN PracticeInsurers pl ON pr.InsurerId=pl.InsurerId
LEFT JOIN Resources as bd ON pr.BillToDr=bd.ResourceId 
LEFT JOIN Resources as sd ON ap.ResourceId1=sd.ResourceId 
LEFT JOIN PracticeVendors as rf ON pr.ReferDr=rf.VendorId 
WHERE ((pr.OpenBalance <> @bal)  OR (@bal = -1))
AND ((pr.Invoice = @InvId) OR (@InvId = ''-1''))
AND (pr.PatientId = @PatId) 
ORDER BY pr.InvoiceDate DESC, pr.Invoice DESC

'
END
GO

