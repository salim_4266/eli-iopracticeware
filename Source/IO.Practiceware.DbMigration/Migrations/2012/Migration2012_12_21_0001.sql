﻿--Prevent users from changing an Insurer's "Cover Dependents" setting where payments from the insurer were posted against a patient who was a dependent.

IF OBJECT_ID('PracticeInsurersPreventOrphanPaymentsWithOutOfPocketChange') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeInsurersPreventOrphanPaymentsWithOutOfPocketChange')
END

GO

--Trigger when user changes insurer from "Covers Dependents" to "Does Not Cover Dependents"
CREATE TRIGGER PracticeInsurersPreventOrphanPaymentsWithOutOfPocketChange 
ON dbo.PracticeInsurers 
FOR UPDATE NOT FOR REPLICATION AS 
BEGIN
SET NOCOUNT ON
DECLARE @changedInsurerIds table(Id int)
INSERT @changedInsurerIds
	SELECT i.InsurerId
	FROM inserted i
	JOIN deleted d ON i.InsurerId = d.InsurerId
	WHERE i.OutOfPocket = 1  
	AND d.OutOfPocket = 0


--Were payments from this insurer posted where the patient was not the policyholder?
IF EXISTS (SELECT * FROM @changedInsurerIds)
	
	IF EXISTS 
		(SELECT * 
		FROM @changedInsurerIds i
		JOIN dbo.PatientReceivablePayments prp ON prp.PayerId = i.Id
			AND prp.PayerType = 'I' 
			AND prp.PaymentFinancialType <> ''
		JOIN dbo.PatientReceivables pr ON pr.ReceivableId = prp.ReceivableId
		WHERE pr.PatientId <> pr.InsuredId)
	

	BEGIN
		RAISERROR ('You cannot change the Dependents setting as payments have been posted from this insurer where the patient is a dependent.',16,1)
		ROLLBACK TRANSACTION
	END

END
