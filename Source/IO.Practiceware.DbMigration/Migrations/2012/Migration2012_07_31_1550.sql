﻿IF OBJECT_ID(N'[model].[FK_X12EligibilityPatientPatient]', 'F') IS NOT NULL
    ALTER TABLE [model].[X12EligibilityPatients] DROP CONSTRAINT [FK_X12EligibilityPatientPatient];
GO

IF OBJECT_ID(N'[model].[X12EligibilityPatients]', 'U') IS NOT NULL
    DROP TABLE [model].[X12EligibilityPatients];
GO

-- Creating table 'X12EligibilityPatients'
CREATE TABLE [model].[X12EligibilityPatients] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [SubscriberEntityIdCode] nvarchar(max)  NOT NULL,
    [SubscriberEntityTypeQualifier] nvarchar(max)  NOT NULL,
    [SubscriberLastName] nvarchar(max)  NOT NULL,
    [SubscriberFirstName] nvarchar(max)  NOT NULL,
    [SubscriberMiddleName] nvarchar(max)  NOT NULL,
    [SubscriberSuffixName] nvarchar(max)  NOT NULL,
    [SubscriberIdCodeQualifier] nvarchar(max)  NOT NULL,
    [SubscriberIdCode] nvarchar(max)  NOT NULL,
    [SubscriberReferenceIdQualifier] nvarchar(max)  NOT NULL,
    [SubscriberReferenceId] nvarchar(max)  NOT NULL,
    [SubscriberTraceReferenceIdentifier] nvarchar(max)  NOT NULL,
    [SubscriberTraceOriginatingCompanyId] nvarchar(max)  NOT NULL,
    [PatientEntityIdCode] nvarchar(max)  NOT NULL,
    [PatientTypeQualifier] nvarchar(max)  NOT NULL,
    [PatientLastName] nvarchar(max)  NOT NULL,
    [PatientFirstName] nvarchar(max)  NOT NULL,
    [PatientMiddleName] nvarchar(max)  NOT NULL,
    [PatientSuffixName] nvarchar(max)  NOT NULL,
    [PatientIdCodeQualifier] nvarchar(max)  NOT NULL,
    [PatientIdCode] nvarchar(max)  NOT NULL,
    [PatientReferenceIdQualifier] nvarchar(max)  NOT NULL,
    [PatientReferenceId] nvarchar(max)  NOT NULL,
    [PatientDateOfBirthFormatQualifier] nvarchar(max)  NOT NULL,
    [PatientDateOfBirth] nvarchar(max)  NOT NULL,
    [PatientGender] nvarchar(max)  NOT NULL,
    [PatientPolicyHolderRelationship] nvarchar(max)  NOT NULL,
    [PatientServiceTypeCode] nvarchar(max)  NOT NULL,
    [BenefitInfoCode] nvarchar(max)  NOT NULL,
    [BenefitCoverageLevelCode] nvarchar(max)  NOT NULL,
    [BenefitInsuranceTypeCode] nvarchar(max)  NOT NULL,
    [BenefitPlanCoverageDescription] nvarchar(max)  NOT NULL,
    [BenefitMonetaryAmount] nvarchar(max)  NOT NULL,
    [BenefitCoInsurancePercent] nvarchar(max)  NOT NULL,
    [BenefitQuantityQualifier] nvarchar(max)  NOT NULL,
    [BenefitQuantity] nvarchar(max)  NOT NULL,
    [InfoReceiverEntityIdCode] nvarchar(max)  NOT NULL,
    [InfoReceiverEntityTypeQualifier] nvarchar(max)  NOT NULL,
    [InfoReceiverName] nvarchar(max)  NOT NULL,
    [InfoReceiverIdCodeQualifier] nvarchar(max)  NOT NULL,
    [InfoReceiverIdCode] nvarchar(max)  NOT NULL,
    [InfoReceiverAddressLine1] nvarchar(max)  NOT NULL,
    [InfoReceiverAddressLine2] nvarchar(max)  NOT NULL,
    [InfoReceiverCity] nvarchar(max)  NOT NULL,
    [InfoReceiverState] nvarchar(max)  NOT NULL,
    [InfoReceiverZipCode] nvarchar(max)  NOT NULL,
    [InfoReceiverCountryCode] nvarchar(max)  NOT NULL,
    [InfoReceiverProviderCode] nvarchar(max)  NOT NULL,
    [InfoReceiverReferenceIdQualifier] nvarchar(max)  NOT NULL,
    [InfoReceiverReferenceId] nvarchar(max)  NOT NULL,
    [InfoSourceEntityIdCode] nvarchar(max)  NOT NULL,
    [InfoSourceEntityTypeQualifier] nvarchar(max)  NOT NULL,
    [InfoSourceName] nvarchar(max)  NOT NULL,
    [InfoSourceIdCodeQualifier] nvarchar(max)  NOT NULL,
    [InfoSourceIdCode] nvarchar(max)  NOT NULL,
    [PatientId] int  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'X12EligibilityPatients'
ALTER TABLE [model].[X12EligibilityPatients]
ADD CONSTRAINT [PK_X12EligibilityPatients]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_X12EligibilityPatientPatient'
CREATE INDEX [IX_FK_X12EligibilityPatientPatient]
ON [model].[X12EligibilityPatients]
    ([PatientId]);
GO