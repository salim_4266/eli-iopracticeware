﻿using FluentMigrator;
using System;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    ///   To correct omission of PatientReceivableServices.Status from Migration2012_03_02_1900.cs in practices where the migration has already been run.
    /// </summary>
    [Migration(201203021901)]
    public class Migration201203021901 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"

IF OBJECT_ID('PatientReceivableServicesPracticeServiceTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReceivableServicesPracticeServiceTrigger')
END

INSERT INTO [PracticeServices] (
       [Code]
       ,[CodeType]
       ,[CodeCategory]
       ,[Description]
       ,[Fee]
       ,[TOS]
       ,[POS]
       ,[StartDate]
       ,[EndDate]
       ,[RelativeValueUnit]
       ,[OrderDoc]
       ,[ConsultOn]
       ,[ClaimNote]
       ,[ServiceFilter]
       ,[NDC]
       )
SELECT DISTINCT
    Service 
    ,'P'
    ,'NEEDSTYPE'
    ,'UNKNOWN'
    ,'1000.00'
    ,''
    ,''
    ,'20000101'
    ,''
    ,''
    ,'F'
    ,'F'
    ,''
    ,''
    ,''
FROM PatientReceivableServices
WHERE [Status] = 'A'
    AND Service NOT IN (SELECT Code FROM PracticeServices)        

EXEC('CREATE TRIGGER PatientReceivableServicesPracticeServiceTrigger
ON PatientReceivableServices
FOR INSERT, UPDATE 
NOT FOR REPLICATION
AS
    SET NOCOUNT ON;
    INSERT INTO [PracticeServices] (
           [Code]
           ,[CodeType]
           ,[CodeCategory]
           ,[Description]
           ,[Fee]
           ,[TOS]
           ,[POS]
           ,[StartDate]
           ,[EndDate]
           ,[RelativeValueUnit]
           ,[OrderDoc]
           ,[ConsultOn]
           ,[ClaimNote]
           ,[ServiceFilter]
           ,[NDC]
           )
    SELECT DISTINCT
        Service 
        ,''P''
        ,''NEEDSTYPE''
        ,''UNKNOWN''
        ,''1000.00''
        ,''''
        ,''''
        ,''20000101''
        ,''''
        ,''''
        ,''F''
        ,''F''
        ,''''
        ,''''
        ,''''
    FROM (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i
    WHERE [Status] = ''A''
        AND Service NOT IN (SELECT Code FROM PracticeServices)   
')


");
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
