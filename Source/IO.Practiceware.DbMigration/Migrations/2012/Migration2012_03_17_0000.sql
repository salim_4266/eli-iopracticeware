﻿-- Creates trigger for HL7 messages

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PracticeActivityCheckoutExternalSystemMessagesTrigger]') AND OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[PracticeActivityCheckoutExternalSystemMessagesTrigger]
GO

CREATE TRIGGER dbo.PracticeActivityCheckoutExternalSystemMessagesTrigger 
   ON  dbo.PracticeActivity
   FOR UPDATE
NOT FOR REPLICATION
AS 
BEGIN
SET NOCOUNT ON;

IF UPDATE(Status)
BEGIN
	DECLARE @now as DATETIME
	SET @now = GETDATE()

	DECLARE @insertTable TABLE(Id UNIQUEIDENTIFIER, MessageType INT, AppointmentId INT, Description NVARCHAR(255))

	-- Create rows for each ExternalSystemExternalSystemMessageType subscribed for DFT_P03_V231s, ORM_O01_V231
	INSERT INTO @insertTable 
	SELECT NEWID(), esesmt.Id, i.AppointmentId, ESMT.Name
	FROM 
	DELETED d INNER JOIN inserted i on d.ActivityId = i.ActivityId
	CROSS JOIN model.ExternalSystemExternalSystemMessageTypes esesmt
	INNER JOIN model.ExternalSystemMessageTypes esmt ON esesmt.ExternalSystemMessageTypeId = esmt.Id AND esmt.Name IN ('DFT_P03_V231', 'ORM_O01_V231') AND esmt.IsOutbound = 1
	WHERE 
	i.Status = 'D' AND (d.Status = 'G' OR d.Status = 'H' OR d.Status = 'U')


	-- Create the messags
	INSERT INTO model.ExternalSystemMessages(Id, Description, Value, ExternalSystemExternalSystemMessageTypeId, ExternalSystemMessageProcessingStateId, ProcessingAttemptCount, CreatedBy, CreatedDateTime, UpdatedDateTime, CorrelationId)
	SELECT Id, Description, '', MessageType, 1, 0, 'Checkout', @now, @now, ''
	FROM @insertTable

	-- Add entity associated with each message for AppointmentId
	INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(Id, ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
	SELECT NEWID(), it.Id, pre.Id, AppointmentId
	FROM
	@insertTable it INNER JOIN model.PracticeRepositoryEntities pre ON pre.Name = 'Appointment'
END

END