﻿-- more bad data

--duplicate modifiers in same service line
UPDATE prs
SET Modifier = SUBSTRING(Modifier, 1, 2)
FROM PatientReceivableServices prs
WHERE SUBSTRING(Modifier, 1, 2) = SUBSTRING(Modifier, 3, 2)
	AND Modifier <> ''
	AND Modifier IS NOT NULL
	AND LEN(Modifier) = 4
