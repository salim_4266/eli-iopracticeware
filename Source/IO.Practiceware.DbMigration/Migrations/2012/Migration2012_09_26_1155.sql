﻿UPDATE [model].[ExternalSystemEntityMappings]
SET PracticeRepositoryEntityKey = 
	CASE WHEN (PracticeRepositoryEntityId = (
				SELECT Id
				FROM [model].[PracticeRepositoryEntities]
				WHERE [Name] = 'Appointment'
				))
			THEN CONVERT(nvarchar, model.GetBigPairId(0, (SELECT y FROM model.GetBigIdPair(PracticeRepositoryEntityKey, 20000000)), 110000000))
		WHEN (PracticeRepositoryEntityId = (
				SELECT Id
				FROM [model].[PracticeRepositoryEntities]
				WHERE [Name] = 'Patient'
				))
			THEN CONVERT(nvarchar,PracticeRepositoryEntityKey)
		WHEN (PracticeRepositoryEntityId = (
				SELECT Id
				FROM [model].[PracticeRepositoryEntities]
				WHERE [Name] = 'PatientInsurance'))
			THEN CONVERT(nvarchar, (model.GetBigPairId((SELECT PatientId FROM dbo.PatientFinancial WHERE FinancialId = (SELECT y FROM model.GetBigIdPair(PracticeRepositoryEntityKey, 20000000))), (SELECT y FROM model.GetBigIdPair(PracticeRepositoryEntityKey, 20000000)), 110000000)))
		WHEN (PracticeRepositoryEntityId = (
				SELECT Id
				FROM [model].[PracticeRepositoryEntities]
				WHERE [Name] = 'ServiceLocation'
				))
		THEN CASE  
				WHEN PracticeRepositoryEntityKey > 1000000
					THEN CONVERT(nvarchar, model.GetBigPairId(1, (SELECT y FROM model.GetBigIdPair(PracticeRepositoryEntityKey, 1000000)), 110000000))
				ELSE PracticeRepositoryEntityKey
				END
		WHEN (PracticeRepositoryEntityId = (
				SELECT Id
				FROM [model].[PracticeRepositoryEntities]
				WHERE [Name] = 'Contact'
				))
			THEN CONVERT(nvarchar, (SELECT x FROM model.GetBigIdPair(PracticeRepositoryEntityKey, 20000000)))
		ELSE
			PracticeRepositoryEntityKey
	END