﻿

EXEC sp_RENAME 'AuditEntries.UserId', 'UserIdOld', 'COLUMN'
ALTER TABLE AuditEntries ADD UserId int NULL
GO

UPDATE AuditEntries
SET UserId = UserIdOld
GO

ALTER TABLE AuditEntries DROP COLUMN UserIdOld
GO
