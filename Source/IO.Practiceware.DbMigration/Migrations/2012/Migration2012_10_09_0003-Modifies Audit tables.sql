﻿
IF EXISTS (
		SELECT NAME
		FROM sysindexes
		WHERE NAME = 'IX_AuditEntryKeyValues_KeyValueNumeric'
		)
	DROP INDEX AuditEntryKeyValues.IX_AuditEntryKeyValues_KeyValueNumeric


IF EXISTS (
		SELECT *
		FROM sys.columns
		WHERE NAME = N'KeyValueNumeric'
			AND Object_ID = Object_ID(N'AuditEntryKeyValues')
		)
	ALTER TABLE AuditEntryKeyValues DROP COLUMN KeyValueNumeric


IF EXISTS (
		SELECT NAME
		FROM sysindexes
		WHERE NAME = 'IX_AuditEntries_ObjectName'
		)
	DROP INDEX AuditEntries.IX_AuditEntries_ObjectName


EXEC sp_RENAME 'AuditEntryKeyValues.KeyValue', 'KeyValueOld', 'COLUMN'
ALTER TABLE AuditEntryKeyValues ADD KeyValue nvarchar(300) NOT NULL DEFAULT ('')
GO

UPDATE AuditEntryKeyValues
SET KeyValue = KeyValueOld
GO

EXEC DropColumnAndDefaultConstraints 'AuditEntryKeyValues', 'KeyValueOld'
GO

 


ALTER TABLE AuditEntryKeyValues ADD KeyValueNumeric bigint NULL
GO

UPDATE AuditEntryKeyValues 
SET KeyValueNumeric = CASE WHEN ISNUMERIC(KeyValue) = 1 THEN KeyValue ELSE NULL END
GO


CREATE INDEX [IX_AuditEntryKeyValues_KeyValueNumeric]
ON [dbo].[AuditEntryKeyValues]
    (KeyValueNumeric);
GO


EXEC sp_RENAME 'AuditEntries.ObjectName', 'ObjectNameOld', 'COLUMN'
GO

ALTER TABLE AuditEntries ADD ObjectName nvarchar(300) NOT NULL DEFAULT ('')
GO

UPDATE AuditEntries
SET ObjectName = ObjectNameOld

EXEC DropColumnAndDefaultConstraints 'AuditEntries', 'ObjectNameOld'




	
CREATE INDEX [IX_AuditEntries_ObjectName]
ON [dbo].[AuditEntries]
    (ObjectName);
GO


