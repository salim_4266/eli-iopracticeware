﻿IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'model.ServiceLocations'))
	EXEC('DROP VIEW model.ServiceLocations')

ALTER TABLE [dbo].[PracticeName] 
DROP COLUMN FileServer

GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'AuditPrintersUpdate')
	DROP TRIGGER model.AuditPrintersUpdate
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'AuditPrintersUpdateTrigger')
	DROP TRIGGER model.AuditPrintersUpdateTrigger
GO

-- Creating table 'Sites'
CREATE TABLE [model].[Sites] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ShortName] nvarchar(max)  NOT NULL DEFAULT CONVERT(nvarchar, SERVERPROPERTY('SERVERNAME'))
);
GO

-- Creating primary key on [Id] in table 'Sites'
ALTER TABLE [model].[Sites]
ADD CONSTRAINT [PK_Sites]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

INSERT INTO [model].[Sites](Name)
SELECT TOP 1 PracticeName
FROM [dbo].[PracticeName]
WHERE PracticeType = 'P' AND LocationReference = ''

IF EXISTS( SELECT * FROM sys.columns WHERE Name = N'ServiceLocationId'  
            and Object_ID = Object_ID(N'model.Printers'))     
	ALTER TABLE [model].[Printers] DROP COLUMN ServiceLocationId
GO

ALTER TABLE [model].[Printers] ADD SiteId int
GO

DECLARE @site int
SET @site = (SELECT TOP 1 Id FROM model.Sites)

UPDATE model.Printers 
SET SiteId = @site

ALTER TABLE [model].[Printers] ALTER COLUMN [SiteId] int NOT NULL
GO

-- Creating foreign key on [SiteId] in table 'Printers'
ALTER TABLE [model].[Printers]
ADD CONSTRAINT [FK_PrinterSite]
    FOREIGN KEY ([SiteId])
    REFERENCES [model].[Sites]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PrinterSite'
CREATE INDEX [IX_FK_PrinterSite]
ON [model].[Printers]
    ([SiteId]);
GO