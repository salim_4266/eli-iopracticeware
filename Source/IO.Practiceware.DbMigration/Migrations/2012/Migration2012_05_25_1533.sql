﻿IF OBJECT_ID('model.ExternalApplications') IS NULL
BEGIN
	CREATE TABLE model.ExternalApplications (
		Id int NOT NULL IDENTITY,
		UserId int,
		ApplicationName nvarchar(500) NOT NULL,
		UserName nvarchar(500),
		[Password] nvarchar(500),
		ApplicationPath nvarchar(max) NOT NULL,
		ArgumentFormatString nvarchar(max),
		PRIMARY KEY(Id),
	)
END
GO