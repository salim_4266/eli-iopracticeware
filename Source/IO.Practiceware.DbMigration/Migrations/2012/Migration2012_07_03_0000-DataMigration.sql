﻿--PracticeAffiliations, PracticeCodeTable, PracticeTransactionJourna, PatientReceivablePayments, PatientReceivableServices, PatientNotes, PatientReceivables, PracticeActivity

--Migration where doctor has Overrides only in PracticeAffiliations
SELECT DISTINCT ResourceId AS ResourceId
INTO #ResourceOverrideOnly
FROM dbo.PracticeAffiliations pa
--INNER JOIN PracticeInterfaceConfiguration picONLY ON picONLY.FieldReference = 'NPIONLY'
--INNER JOIN PracticeInterfaceConfiguration picALL ON picALL.FieldReference = 'NPIALL'
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pa.PlanId
WHERE ResourceType = 'R' 
	AND ResourceId IN 
		(SELECT ResourceId 
		FROM PRacticeAffiliations
		WHERE ResourceType = 'R' 
		AND Override = 'Y' OR OrgOverride = 'Y')
	AND ResourceId NOT IN 
		(SELECT ResourceId 
		FROM PRacticeAffiliations
		WHERE ResourceType = 'R' 
		AND Override = 'N' AND OrgOverride = 'N')



--ADD NON-Override Affiliation To a Plan that doesn't matter
INSERT INTO PracticeAffiliations (ResourceId, ResourceType, PlanId, Pin, Override, LocationId, AlternatePin, OrgOverride)
SELECT ResourceId, 'R', InsurerId, 'xxx', 'N', 0, '', 'N' 
FROM #ResourceOverrideOnly
INNER JOIN PracticeInsurers ON InsurerName = 'ZZZ No insurance'


-- Delete duplicate SERVICEMODS from PracticeCodeTable
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT SUBSTRING(pct1.Code, 1,2) AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1,2) = SUBSTRING(pct2.Code, 1,2)
		AND pct2.ReferenceType = 'SERVICEMODS' 
	WHERE pct1.ReferenceType like 'SERVICEMODS' 
	) v
GROUP BY v.Code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

--chop modifier to 2 characters if longer than 2 characters
UPDATE pct
SET code = replace(code, substring(code, 3, charindex(' - ', code) - 3), '')
FROM practicecodetable pct
WHERE ReferenceType LIKE 'servicemod%'
	AND len(substring(code, 1, charindex(' - ', code))) > 2




--leftover stuff from training/test patients
DELETE ptj
FROM PracticeTransactionJournal ptj
INNER JOIN patientreceivables pr1 ON pr1.ReceivableId = ptj.transactiontypeid
INNER JOIN patientreceivables pr2 ON pr1.appointmentid = pr2.appointmentid
INNER JOIN appointments ap ON ap.AppointmentId = pr1.AppointmentId
	AND pr1.ReceivableId <> pr2.ReceivableId
	AND pr1.patientid <> pr2.patientid
	AND pr1.AppointmentId <> 0
	AND ap.patientid <> pr1.PatientId
WHERE transactiontype = 'r'



--PracticeActivity test patients
DELETE pa
FROM PracticeActivity pa
INNER JOIN patientreceivables pr1 ON pr1.appointmentid = pa.AppointmentId
	AND pr1.patientid = pa.PatientId
INNER JOIN patientreceivables pr2 ON pr1.appointmentid = pr2.appointmentid
INNER JOIN appointments ap ON ap.AppointmentId = pr1.AppointmentId
	AND pr1.ReceivableId <> pr2.ReceivableId
	AND pr1.patientid <> pr2.patientid
	AND pr1.AppointmentId <> 0
	AND ap.patientid <> pr1.PatientId



--PatientNotes test patients
DELETE pn
FROM patientnotes pn
INNER JOIN patientreceivables pr1 ON pr1.appointmentid = pn.AppointmentId
	AND pr1.patientid = pn.PatientId
INNER JOIN patientreceivables pr2 ON pr1.appointmentid = pr2.appointmentid
INNER JOIN appointments ap ON ap.AppointmentId = pr1.AppointmentId
	AND pr1.ReceivableId <> pr2.ReceivableId
	AND pr1.patientid <> pr2.patientid
	AND pr1.AppointmentId <> 0
	AND ap.patientid <> pr1.PatientId



--Payments test patients
DELETE prp
FROM patientreceivablepayments prp
INNER JOIN patientreceivables pr1 ON pr1.ReceivableId = prp.ReceivableId
INNER JOIN patientreceivables pr2 ON pr1.appointmentid = pr2.appointmentid
INNER JOIN appointments ap ON ap.AppointmentId = pr1.AppointmentId
	AND pr1.ReceivableId <> pr2.ReceivableId
	AND pr1.patientid <> pr2.patientid
	AND pr1.AppointmentId <> 0
	AND ap.patientid <> pr1.PatientId


--Services test patients
DELETE prs
FROM patientreceivables pr1
INNER JOIN patientreceivables pr2 ON pr1.appointmentid = pr2.appointmentid
INNER JOIN appointments ap ON ap.AppointmentId = pr1.AppointmentId
INNER JOIN patientreceivableservices prs ON prs.invoice = pr1.Invoice
	AND pr1.ReceivableId <> pr2.ReceivableId
	AND pr1.patientid <> pr2.patientid
	AND pr1.AppointmentId <> 0
	AND ap.patientid <> pr1.PatientId


--Receivables for test patients
DELETE pr1
FROM patientreceivables pr1
INNER JOIN patientreceivables pr2 ON pr1.appointmentid = pr2.appointmentid
INNER JOIN appointments ap ON ap.AppointmentId = pr1.AppointmentId
	AND pr1.ReceivableId <> pr2.ReceivableId
	AND pr1.patientid <> pr2.patientid
	AND pr1.AppointmentId <> 0
	AND ap.patientid <> pr1.PatientId


--Add missing payment methods
INSERT INTO PracticeCodeTable (ReferenceType, Code, AlternateCode, Exclusion, LetterTranslation, OtherLtrTrans, FormatMask, Signature, TestOrder, FollowUp, [Rank])
SELECT distinct 'PAYABLETYPE', 'Payment Method - ' +  PaymentRefType, '', 'F', '', '', '', 'F', '', '', 1
FROM PatientReceivablePayments
WHERE PaymentRefType NOT IN (SELECT SUBSTRING(Code, LEN(Code), 1) FROM PracticeCodeTable pct WHERE ReferenceType = 'PAYABLETYPE')
AND PaymentRefType <> '' and PaymentRefType IS NOT NULL



--get rid of on account "payments" from Office
update pr set openbalance = openbalance + paymentamount
from patientreceivables pr
inner join patientreceivablepayments prp on pr.ReceivableId = prp.ReceivableId
where payertype = 'O'  and insurerid = 99999

delete prp
from patientreceivables pr
inner join patientreceivablepayments prp on pr.ReceivableId = prp.ReceivableId
where payertype = 'O'  and insurerid = 99999


--get rid of payments with no payment types
delete from patientreceivablepayments
where paymenttype = ''
