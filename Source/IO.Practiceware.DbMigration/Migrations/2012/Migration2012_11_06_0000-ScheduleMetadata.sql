﻿--Insert default values for Schedule Template Builder Configurations

DECLARE @ServiceLocation int
SET @ServiceLocation = (SELECT TOP 1 PracticeId FROM PracticeName WHERE LocationReference = '' AND PracticeType ='P')
INSERT INTO [model].[ScheduleTemplateBuilderConfigurations] (SpanSize, AreaStart, AreaEnd, DefaultLocationId)
VALUES ('2000-01-01 00:15:00.000', '2000-01-01 07:00:00.000', '2000-01-01 19:00:00.000', @ServiceLocation)

GO