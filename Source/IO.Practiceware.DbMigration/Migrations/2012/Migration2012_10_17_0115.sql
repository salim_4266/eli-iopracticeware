﻿-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[model].[ScheduleTemplateBuilderConfigurations]', 'U') IS NOT NULL
    DROP TABLE [model].[ScheduleTemplateBuilderConfigurations];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ScheduleTemplateBuilderConfigurations'
CREATE TABLE [model].[ScheduleTemplateBuilderConfigurations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [SpanSize] datetime  NOT NULL,
    [AreaStart] datetime  NOT NULL,
    [AreaEnd] datetime  NOT NULL,
    [DefaultLocationId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------
-- Creating primary key on [Id] in table 'ScheduleTemplateBuilderConfigurations'
ALTER TABLE [model].[ScheduleTemplateBuilderConfigurations]
ADD CONSTRAINT [PK_ScheduleTemplateBuilderConfigurations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
