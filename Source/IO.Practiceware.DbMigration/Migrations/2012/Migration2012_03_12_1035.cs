﻿using FluentMigrator;
using System;

namespace IO.Practiceware.DbMigration.Migrations
{
    /// <summary>
    /// 5010 File generation
    /// </summary>
    [Migration(201203121035)]
    public class Migration201203121035 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"

IF NOT EXISTS (SELECT * FROM dbo.PracticeInterfaceConfiguration WHERE FieldReference = '5010')
INSERT INTO dbo.PracticeInterfaceConfiguration(FieldReference, FieldValue) VALUES ('5010', 'F')
GO


");
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
