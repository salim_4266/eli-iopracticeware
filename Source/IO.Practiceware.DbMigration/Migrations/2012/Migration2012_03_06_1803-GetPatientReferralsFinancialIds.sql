﻿DECLARE @triggerNames xml
EXEC dbo.DisableEnabledTriggers 'dbo.PatientReferral', @names = @triggerNames OUTPUT

DECLARE @constraintNames xml
EXEC dbo.DisableEnabledCheckConstraints 'dbo.PatientReferral', @names = @constraintNames OUTPUT

EXEC dbo.DropIndexes 'model.Adjustments_Partition1'
EXEC dbo.DropIndexes 'model.BillingServiceBillingDiagnosis_Partition1'
EXEC dbo.DropIndexes 'model.BillingServices_Partition1'
EXEC dbo.DropIndexes 'model.BillingServiceTransactions_Internal'
EXEC dbo.DropIndexes 'model.Comments_Partition8'
EXEC dbo.DropIndexes 'model.FinancialInformations_Internal'
EXEC dbo.DropIndexes 'model.InvoiceReceivables_Partition1'
EXEC dbo.DropIndexes 'model.InvoiceReceivables_Partition2'
EXEC dbo.DropIndexes 'model.InvoiceReceivables_Partition3'
EXEC dbo.DropIndexes 'model.InvoiceReceivables_Partition5'
EXEC dbo.DropIndexes 'model.PatientInsurances_Partition1'
EXEC dbo.DropIndexes 'model.PatientInsuranceAuthorizations_Internal'

IF NOT EXISTS(SELECT * FROM sysobjects so INNER JOIN syscolumns sc ON so.id = sc.id WHERE so.name = 'PatientReferral' AND sc.Name = 'ComputedFinancialId')
BEGIN
    ALTER TABLE dbo.PatientReferral
    ADD ComputedFinancialId int
END

IF OBJECT_ID('GetPatientReferralsFinancialIds') IS NOT NULL
BEGIN
    EXEC('DROP FUNCTION GetPatientReferralsFinancialIds')
END

IF OBJECT_ID('GetBulkPatientReferralsFinancialIds') IS NOT NULL
BEGIN
    EXEC('DROP FUNCTION GetBulkPatientReferralsFinancialIds')
END

DECLARE @createGetPatientReferralsFinancialIds nvarchar(max)
SET @createGetPatientReferralsFinancialIds = '

    CREATE FUNCTION [dbo].[GetPatientReferralsFinancialIds](@idsXml xml)
    RETURNS @financialIds table(Id int PRIMARY KEY CLUSTERED, FinancialId int)
    AS
    BEGIN

	DECLARE @ids table(Id int PRIMARY KEY CLUSTERED)

	INSERT @ids(Id)
	SELECT DISTINCT n.Id.value(''.'', ''int'') 
	FROM @idsXml.nodes(''//Id'') as n(Id)

	INSERT @financialIds(Id, FinancialId)
	----Patient is policyholder
	SELECT z.Id, MAX(z.FinancialId)
	FROM (
		SELECT pRef.ReferralId as id, pf.FinancialId as FinancialId
		FROM dbo.PatientReferral pRef WITH(NOLOCK) 
		INNER JOIN dbo.PatientDemographics pd WITH(NOLOCK) ON pd.PatientId = pRef.PatientId
		INNER JOIN dbo.PatientFinancial pf WITH(NOLOCK) ON pRef.ReferredInsurer = pf.FinancialInsurerId
			AND dbo.IsDate112(pf.FinancialStartDate) = 1
			AND pRef.ReferralExpireDate >= pf.FinancialStartDate
			AND pRef.ReferralDate >= pf.FinancialStartDate
			AND (
				pRef.ReferralDate <= pf.FinancialEndDate 
				OR pf.FinancialEndDate = ''''
				OR pf.FinancialEndDate IS NULL
				)
			AND (pf.PatientId = pd.PolicyPatientId
				OR pf.PatientId = pd.SecondPolicyPatientId)
			AND pf.PatientId = pd.PatientId
		
		UNION ALL

		----patient is not first policyholder
		SELECT pRef.ReferralId as id, pf.FinancialId as FinancialId
		FROM dbo.PatientReferral pRef WITH(NOLOCK) 
		INNER JOIN dbo.PatientDemographics pd WITH(NOLOCK) ON pd.PatientId = pRef.PatientId
		INNER JOIN dbo.PracticeInsurers pri WITH(NOLOCK) ON pri.InsurerId = pRef.ReferredInsurer
		INNER JOIN dbo.PatientFinancial pf WITH(NOLOCK) ON pRef.ReferredInsurer = pf.FinancialInsurerId
			AND dbo.IsDate112(pf.FinancialStartDate) = 1
			AND pRef.ReferralExpireDate >= pf.FinancialStartDate
			AND pRef.ReferralDate >= pf.FinancialStartDate
			AND (
				pRef.ReferralDate <= pf.FinancialEndDate 
				OR pf.FinancialEndDate = ''''
				OR pf.FinancialEndDate IS NULL
				)
			AND pd.PolicyPatientId <> pd.PatientId
			AND pd.PolicyPatientId = pf.PatientId
			AND pd.PolicyPatientId <> 0
			AND pri.AllowDependents = 1
	
		UNION ALL 

		----patient is not second policyholder
		SELECT pRef.ReferralId as id, pf.FinancialId as FinancialId
		FROM dbo.PatientReferral pRef WITH(NOLOCK) 
		INNER JOIN dbo.PatientDemographics pd WITH(NOLOCK) ON pd.PatientId = pRef.PatientId
		INNER JOIN dbo.PracticeInsurers pri WITH(NOLOCK) ON pri.InsurerId = pRef.ReferredInsurer
		INNER JOIN dbo.PatientFinancial pf WITH(NOLOCK) ON pRef.ReferredInsurer = pf.FinancialInsurerId
			AND dbo.IsDate112(pf.FinancialStartDate) = 1
			AND pRef.ReferralExpireDate >= pf.FinancialStartDate
			AND pRef.ReferralDate >= pf.FinancialStartDate
			AND (
				pRef.ReferralDate <= pf.FinancialEndDate 
				OR pf.FinancialEndDate = ''''
				OR pf.FinancialEndDate IS NULL
				)
			AND  pd.SecondPolicyPatientId <> pd.PatientId
			AND pd.SecondPolicyPatientId = pf.PatientId
			AND pd.SecondPolicyPatientId <> 0
			AND pri.AllowDependents = 1
		) z 
	INNER JOIN @ids i ON z.Id = i.Id
	GROUP BY z.Id

	DELETE @ids
	WHERE Id IN (SELECT Id FROM @financialIds)
        
	-- Was billed but is no longer a valid policy
	INSERT @financialIds(Id, FinancialId)
	SELECT pRef.ReferralId, MAX(pr.ComputedFinancialId)
	FROM dbo.PatientReferral pRef WITH(NOLOCK) 
	INNER JOIN @ids i ON pRef.ReferralId = i.Id
	INNER JOIN dbo.Appointments ap WITH(NOLOCK) ON ap.ReferralId = pRef.ReferralId
	INNER JOIN dbo.PatientReceivables pr WITH(NOLOCK) ON pr.AppointmentId = ap.AppointmentId
	INNER JOIN dbo.PracticeTransactionJournal ptj WITH(NOLOCK) ON ptj.TransactionTypeId = pr.ReceivableId 
	INNER JOIN dbo.ServiceTransactions st WITH(NOLOCK) ON st.TransactionId = ptj.TransactionId
	WHERE ptj.TransactionType = ''R''
		AND st.TransportAction IN (''1'', ''2'')
	GROUP BY pRef.ReferralId

	DELETE @ids
	WHERE Id IN (SELECT Id FROM @financialIds)	

	INSERT @financialIds(Id, FinancialId)
	SELECT pRef.ReferralId, MAX(pr.ComputedFinancialId)
	FROM dbo.PatientReferral pRef WITH(NOLOCK) 
	INNER JOIN @ids i ON pRef.ReferralId = i.Id
	INNER JOIN dbo.Appointments ap WITH(NOLOCK) ON ap.ReferralId = pRef.ReferralId
	INNER JOIN dbo.PatientReceivables pr WITH(NOLOCK) ON pr.AppointmentId = ap.AppointmentId
	INNER JOIN dbo.PatientReceivablePayments prp WITH(NOLOCK) ON prp.ReceivableId = pr.ReceivableId
	GROUP BY pRef.ReferralId
		
	DELETE @ids
	WHERE Id IN (SELECT Id FROM @financialIds)	
		
	INSERT @financialIds(Id, FinancialId)
	SELECT i.Id, NULL
	FROM @ids i
		
	RETURN
END
'

EXEC(@createGetPatientReferralsFinancialIds)

DECLARE @createGetBulkPatientReferralsFinancialIds nvarchar(max)
SET @createGetBulkPatientReferralsFinancialIds = REPLACE(REPLACE(@createGetPatientReferralsFinancialIds, 'INNER JOIN', 'INNER HASH JOIN'), 'GetPatientReferralsFinancialIds', 'GetBulkPatientReferralsFinancialIds')

EXEC (@createGetBulkPatientReferralsFinancialIds)

IF OBJECT_ID('PatientReferralFinancialId') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReferralFinancialId')
END

IF OBJECT_ID('PatientReferralFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientReferralFinancialIdTrigger')
END

IF OBJECT_ID('PatientFinancialPatientReferalFinancialIdTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PatientFinancialPatientReferalFinancialIdTrigger')
END

IF OBJECT_ID('PatientReferralFinancialIdTrigger') IS NOT NULL
BEGIN
	EXEC('DROP TRIGGER PatientReferralFinancialIdTrigger')
END

EXEC('
DECLARE @xml xml
SET @xml = (
	SELECT ReferralId AS Id
	FROM PatientReferral
	FOR XML PATH('''')
)

UPDATE pRef
SET pRef.ComputedFinancialId = pRefpf.FinancialId
FROM PatientReferral pRef
INNER JOIN 
(
	SELECT Id, FinancialId 
	FROM
	dbo.GetBulkPatientReferralsFinancialIds(@xml)
) pRefpf
ON pRefpf.Id = pRef.ReferralId 
')

EXEC('CREATE TRIGGER [dbo].[PatientReferralFinancialIdTrigger]
ON [dbo].[PatientReferral]
FOR INSERT, UPDATE
NOT FOR REPLICATION
AS
SET NOCOUNT ON;

DECLARE @changedIds table(Id int)
INSERT @changedIds
SELECT ReferralId FROM
(SELECT * FROM inserted EXCEPT SELECT * FROM deleted) c

IF EXISTS(SELECT * FROM @changedIds) AND dbo.GetNoRecompute() = 0
BEGIN

	SELECT 1 AS Value INTO #NoRecompute
	
	DECLARE @xml xml
	SET @xml = (
		SELECT Id AS Id
		FROM @changedIds
		FOR XML PATH('''')
	)
	
	SELECT Id, FinancialId 
	INTO #financialIds
	FROM
	dbo.GetPatientReferralsFinancialIds(@xml) f
	JOIN PatientReferral pRef WITH(NOLOCK) ON f.Id = pRef.ReferralId
	WHERE COALESCE(ComputedFinancialId, '''') <> COALESCE(f.FinancialId, '''')

	IF EXISTS(SELECT * FROM #financialIds)	
	BEGIN
		UPDATE pRef
		SET pRef.ComputedFinancialId = f.FinancialId
		FROM #financialIds f
		JOIN PatientReferral pRef ON f.Id = pRef.ReferralId
	END	   

	DROP TABLE #financialIds	
	
	DROP TABLE #NoRecompute

END
')

EXEC('CREATE TRIGGER [dbo].[PatientFinancialPatientReferalFinancialIdTrigger]
ON [dbo].[PatientFinancial]
FOR INSERT, UPDATE
NOT FOR REPLICATION
AS
SET NOCOUNT ON;

DECLARE @changedIds table(Id int)
INSERT @changedIds
SELECT PatientId FROM
(SELECT * FROM inserted EXCEPT SELECT * FROM deleted) c

IF EXISTS(SELECT * FROM @changedIds) AND dbo.GetNoRecompute() = 0
BEGIN

	SELECT 1 AS Value INTO #NoRecompute
	
	DECLARE @patientIds table(Id int, UNIQUE CLUSTERED(Id))

	INSERT INTO @patientIds

	SELECT pd.PatientId
	FROM dbo.PatientDemographics pd WITH(NOLOCK) 
	INNER JOIN @changedIds i
	ON i.Id = pd.PolicyPatientId 

	UNION 

	SELECT pd.PatientId
	FROM dbo.PatientDemographics pd WITH(NOLOCK) 
	INNER JOIN @changedIds i
	ON i.Id = pd.SecondPolicyPatientId

	UNION 

	SELECT pr.PatientId
	FROM dbo.PatientReceivables pr WITH(NOLOCK) 
	INNER JOIN @changedIds i
	ON i.Id = pr.InsuredId
	    
	DECLARE @xml xml
	SET @xml = (
		SELECT pRef.ReferralId AS Id
		FROM @patientIds p 
		INNER JOIN PatientReferral pRef WITH(NOLOCK) ON p.Id = pRef.PatientId
		FOR XML PATH('''')
	)
    
	SELECT Id, FinancialId 
	INTO #financialIds
	FROM dbo.GetPatientReferralsFinancialIds(@xml)
	
	IF EXISTS (SELECT * FROM #financialIds)
	BEGIN
		EXEC(''
		UPDATE pRef
		SET pRef.ComputedFinancialId = f.FinancialId
		FROM #financialIds f
		JOIN PatientReferral pRef WITH(NOLOCK) ON f.Id = pRef.ReferralId 	
		WHERE COALESCE(ComputedFinancialId, '''''''') <> COALESCE(f.FinancialId, '''''''')
		'')
	END

	DROP TABLE #financialIds	
	
	DROP TABLE #NoRecompute
END 
')

EXEC dbo.EnableTriggers @triggerNames
EXEC dbo.EnableCheckConstraints @constraintNames
