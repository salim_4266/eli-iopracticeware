﻿--Place of service codes

IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code = '31-Skilled Nursing Facility')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES (N'PLACEOFSERVICE', N'31-Skilled Nursing Facility', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '32-Nursing Facility')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'32-Nursing Facility', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '33-Custodial Care Facility')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'33-Custodial Care Facility', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code = '34-Hospice' )
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'34-Hospice', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '41-Ambulance - land')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'41-Ambulance - land', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code = '42-Ambulance Air or Water' )
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'42-Ambulance Air or Water', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '50-Federal Qualified Health Ctr')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'50-Federal Qualified Health Ctr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '51-Inpatient Psychiatric Fac')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'51-Inpatient Psychiatric Fac', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '52-Psychiatric Facility')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'52-Psychiatric Facility', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '53-Community Med Hlth Ctr')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'53-Community Med Hlth Ctr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '54-Int Care Fac/Mental Retarded')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'54-Int Care Fac/Mental Retarded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '55-Res Substance Abuse Ctr')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'55-Res Substance Abuse Ctr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '56-Psychiatric Res Trmt Ctr')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'56-Psychiatric Res Trmt Ctr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '61-Comp Rehan Inpatient Fac')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'61-Comp Rehan Inpatient Fac', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code = '62-Comp Rehab Outpatient Fac' )
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'62-Comp Rehab Outpatient Fac', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code = '65-End Stage Renal Treatment Ctr' )
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'65-End Stage Renal Treatment Ctr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '71-State or Local Health Clinic')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'71-State or Local Health Clinic', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code = '72-Rural Health Clinic' )
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'72-Rural Health Clinic', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code = '81-Independent Lab' )
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'81-Independent Lab', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code = '99-Other Unlisted Facility' )
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'99-Other Unlisted Facility', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code = '11-Office')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'11-Office', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code = '12-Home' )
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'12-Home', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code = '21-Hospital Inpatient' )
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'21-Hospital Inpatient', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '22-Hospital Outpatient')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'22-Hospital Outpatient', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '23-Hospital Emergency Room')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'23-Hospital Emergency Room', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '24-Ambulatory Surgical Center')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'24-Ambulatory Surgical Center', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '25-Birthing Center')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'25-Birthing Center', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
IF NOT EXISTS(SELECT * FROM [dbo].[PracticeCodeTable] WHERE ReferenceType = 'PLACEOFSERVICE' AND Code =  '26-Military Treatment Center')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType], [Code], [AlternateCode], [Exclusion], [LetterTranslation], [OtherLtrTrans], [FormatMask], [Signature], [TestOrder], [FollowUp], [Rank]) VALUES ( N'PLACEOFSERVICE', N'26-Military Treatment Center', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)














