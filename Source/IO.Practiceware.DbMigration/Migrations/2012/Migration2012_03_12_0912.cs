﻿using FluentMigrator;
using System;

namespace IO.Practiceware.DbMigration.Migrations
{
    [Migration(201203120912)]
    public class Migration201203120912 : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"

-- Creating table 'BatchBuilderTemplates'
CREATE TABLE [model].[BatchBuilderTemplates] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TemplateXml] nvarchar(max)  NOT NULL
);
GO  

-- Creating primary key on [Id] in table 'BatchBuilderTemplates'
ALTER TABLE [model].[BatchBuilderTemplates]
ADD CONSTRAINT [PK_BatchBuilderTemplates]
PRIMARY KEY CLUSTERED ([Id] ASC);
GO
                ");
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}