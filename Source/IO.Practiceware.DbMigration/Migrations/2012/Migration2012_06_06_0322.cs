﻿using FluentMigrator;
using System;

namespace IO.Practiceware.DbMigration.Migrations
{
    [Migration(201206060322)]
    public class Migration201206060322 : Migration
    {
        public override void Up()
        {
            Execute.Sql(
                @"
ALTER TABLE [model].[Documents] ALTER COLUMN Name nvarchar(500) not null
GO

-- Creating unique key on [DocumentTypeId], [Name] in table 'Documents'
ALTER TABLE [model].[Documents] ADD CONSTRAINT [UC_Documents] UNIQUE (DocumentTypeId,Name);
GO
");
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}