﻿-- Creating Rows for PatientLanguage and PatientEthnicity in Entity Tables for Mapping.

--For PatientLanguage
IF NOT EXISTS(SELECT Id from model.PracticeRepositoryEntities WHERE  Name='PatientLanguage')
BEGIN
INSERT INTO model.PracticeRepositoryEntities(Id,Name,KeyPropertyName) VALUES(9,'PatientLanguage','Id')
END
GO

--For PatientEthnicity
IF NOT EXISTS(SELECT Id from model.PracticeRepositoryEntities WHERE  Name='PatientEthnicity')
BEGIN
INSERT INTO model.PracticeRepositoryEntities(Id,Name,KeyPropertyName) VALUES(11,'PatientEthnicity','Id')
END
GO	
	
