﻿-- --------------------------------------------------
-- Adding columns
-- --------------------------------------------------
ALTER TABLE dbo.Resources ADD HexColor nvarchar(max) NOT NULL DEFAULT('FFFFFF') 
GO
ALTER TABLE dbo.PracticeName ADD HexColor nvarchar(max) NOT NULL DEFAULT('FFFFFF') 
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[model].[FK_ScheduleTemplateBlockScheduleTemplateBlockComment]', 'F') IS NOT NULL
    ALTER TABLE [model].[ScheduleTemplateBlocks] DROP CONSTRAINT [FK_ScheduleTemplateBlockScheduleTemplateBlockComment];
GO
IF OBJECT_ID(N'[model].[FK_ScheduleBlockScheduleBlockComment]', 'F') IS NOT NULL
    ALTER TABLE [model].[ScheduleBlocks] DROP CONSTRAINT [FK_ScheduleBlockScheduleBlockComment];
GO


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[model].[ScheduleTemplateBlockComments]', 'U') IS NOT NULL
    DROP TABLE [model].[ScheduleTemplateBlockComments];
GO
IF OBJECT_ID(N'[model].[ScheduleBlockComments]', 'U') IS NOT NULL
    DROP TABLE [model].[ScheduleBlockComments];
GO