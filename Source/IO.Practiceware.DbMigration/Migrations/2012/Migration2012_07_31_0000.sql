﻿
SET IDENTITY_INSERT [model].[StateOrProvinces] ON

IF NOT EXISTS(SELECT * FROM model.StateOrProvinces WHERE Id = 74)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (74, N'AE', N'ARMED FORCES OF AFRICA, CANADA, EUROPE, MIDDLE EAST', 225)
IF NOT EXISTS(SELECT * FROM model.StateOrProvinces WHERE Id = 75)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (75, N'AA', N'ARMED FORCES OF AMERICAS', 225)
IF NOT EXISTS(SELECT * FROM model.StateOrProvinces WHERE Id = 76)
INSERT [model].[StateOrProvinces] ([Id], [Abbreviation], [Name], [CountryId]) VALUES (76, N'AP', N'ARMED FORCES OF PACIFIC', 225)

SET IDENTITY_INSERT [model].[StateOrProvinces] OFF



UPDATE [model].[StateOrProvinces]
SET Name = 'FEDERATED STATES OF MICRONESIA'
WHERE Abbreviation = 'FM'