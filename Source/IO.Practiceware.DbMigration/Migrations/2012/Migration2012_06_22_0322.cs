﻿using FluentMigrator;
using System;

namespace IO.Practiceware.DbMigration.Migrations
{
    [Migration(201206220322)]
    public class Migration201206220322 : Migration
    {
        public override void Up()
        {
           Execute.Sql(
                @"
INSERT [model].[Documents](DocumentTypeId,Name)
SELECT 1,Code + '.doc' FROM dbo.PracticeCodeTable WHERE ReferenceType='MISCELLANEOUSLETTERS'
AND Code NOT IN(SELECT  REPLACE(NAME,'.doc','') FROM [model].[Documents] WHERE  DocumentTypeId='1')

INSERT [model].[Documents](DocumentTypeId,Name)
SELECT 2,Code + '.doc' FROM dbo.PracticeCodeTable WHERE ReferenceType='CONSULTATIONLETTERS'
AND Code NOT IN(SELECT  REPLACE(NAME,'.doc','') FROM [model].[Documents] WHERE  DocumentTypeId='2')

INSERT [model].[Documents](DocumentTypeId,Name)
SELECT 3,Code + '.doc' FROM dbo.PracticeCodeTable WHERE ReferenceType='REFERRALLETTERS'
AND Code NOT IN(SELECT  REPLACE(NAME,'.doc','') from [model].[Documents] WHERE  DocumentTypeId='3')

INSERT [model].[Documents](DocumentTypeId,Name)
SELECT 4,Code + '.doc' FROM dbo.PracticeCodeTable WHERE ReferenceType='COLLECTIONLETTERS'
AND Code NOT IN(SELECT  REPLACE(NAME,'.doc','') from [model].[Documents] WHERE  DocumentTypeId='4')

INSERT [model].[Documents](DocumentTypeId,Name)
SELECT 7,Code + '.doc' FROM dbo.PracticeCodeTable WHERE ReferenceType='FACILITYADMISSION'
AND Code NOT IN(SELECT  REPLACE(NAME,'.doc','') FROM [model].[Documents] WHERE  DocumentTypeId='7')");

            Execute.Sql(@"          
IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 1 and Name = 'RecallTEnv.doc')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (1,'RecallTEnv.doc')

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 1 and Name = 'RecallTLbl.doc')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (1,'RecallTLbl.doc')

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 3 and Name = 'StandardReferralLetter.doc')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (3,'StandardReferralLetter.doc')

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 4 and Name = 'Collections')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (4,'Collections')

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 5 and Name = 'DrugRx.doc')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (5,'DrugRx.Doc')

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 5 and Name = 'CLRx.doc')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (5,'CLRx.doc')

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 5 and Name = 'EyeWearRx.doc')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (5,'EyeWearRx.doc') 

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 6 and Name = 'Hcfa1500A.doc')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (6,'Hcfa1500A.Doc')

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 6 and Name = 'Proof')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (6,'Proof')

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 6 and Name = 'Transactions')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (6,'Transactions')

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 10 and Name = 'DIPrint.doc')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (10,'DIPrint.doc')

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 11 and Name = 'Image')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (11,'Image')

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 12 and Name = 'Written Instructions')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (12,'Written Instructions')

IF NOT EXISTS (SELECT * FROM [model].[Documents] WHERE DocumentTypeId = 14 and Name = 'ASC.doc')
                    INSERT [model].[Documents] ([DocumentTypeId], [Name]) VALUES (14,'ASC.doc')

                -- 1. BatchBuilderDocuments
                -- 2. Consultationletters 
                -- 3. Referralletters 
                -- 4. Collectionletters 
                -- 5. Prescriptions 
                -- 6. FinancialDocuments 
                -- 7. SurgeryProcedureForms 
                -- 8. Reports 
                -- 9. FrontDeskDocuments 
                -- 10. ClinicalDocuments 
                -- 11. Images 
                -- 12. WrittenInstructions 
                -- 13. ExamDocuments
                -- 13. SurgeryDocuments
                ");
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}