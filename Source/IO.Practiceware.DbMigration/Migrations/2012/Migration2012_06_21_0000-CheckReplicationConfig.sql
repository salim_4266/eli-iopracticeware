﻿
--Preventing CheckIn/CheckOut in AI
IF NOT EXISTS (SELECT * FROM dbo.Practiceinterfaceconfiguration WHERE FieldReference = 'CheckReplication')
INSERT INTO dbo.PracticeInterfaceConfiguration (FieldReference, FieldValue) VALUES ('CheckReplication', 'F')
GO
