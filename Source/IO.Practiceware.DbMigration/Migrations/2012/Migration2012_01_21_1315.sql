﻿/****** Object:  Index [IX_PracticeActivity_AppointmentId]******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PracticeActivity]') AND name = N'IX_PracticeActivity_AppointmentId')
DROP INDEX [IX_PracticeActivity_AppointmentId] ON [dbo].[PracticeActivity] WITH ( ONLINE = OFF )
GO

/****** Object:  Index [IX_PracticeActivity_AppointmentId]    Script Date: 01/21/2013 12:17:02 ******/
CREATE NONCLUSTERED INDEX [IX_PracticeActivity_AppointmentId] ON [dbo].[PracticeActivity] 
(
	[AppointmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

