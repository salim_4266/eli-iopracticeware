﻿-- To Fix 3834.Corrected the QuestionId Column name for model.PatientSurgeryQuestionAnswers
IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE TABLE_NAME = 'PatientSurgeryQuestionAnswers'
			AND COLUMN_NAME = 'QustionId'
		)
BEGIN
	EXEC sp_RENAME 'model.PatientSurgeryQuestionAnswers.QustionId'
		,'QuestionId'
		,'COLUMN'
END
GO

--Create Foreign Key
IF EXISTS (
		SELECT *
		FROM sys.foreign_keys
		WHERE NAME = 'FK_PatientSurgeryQuestionAnswer_Question'
			AND parent_object_id = OBJECT_ID(N'[model].[PatientSurgeryQuestionAnswers]')
		)
	ALTER TABLE [model].[PatientSurgeryQuestionAnswers]

DROP CONSTRAINT [FK_PatientSurgeryQuestionAnswer_Question]
GO

ALTER TABLE [model].[PatientSurgeryQuestionAnswers]
	WITH CHECK ADD CONSTRAINT [FK_PatientSurgeryQuestionAnswer_Question] FOREIGN KEY ([QuestionId]) REFERENCES [model].[Questions]([Id])
GO

ALTER TABLE [model].[PatientSurgeryQuestionAnswers] CHECK CONSTRAINT [FK_PatientSurgeryQuestionAnswer_Question]
GO

--Drop and Create Index
IF EXISTS (
		SELECT NAME
		FROM sysindexes
		WHERE NAME = 'IX_FK_PatientSurgeryQuestionAnswer_Question'
		)
BEGIN
	DROP INDEX [IX_FK_PatientSurgeryQuestionAnswer_Question] ON [model].[PatientSurgeryQuestionAnswers]
END
GO

CREATE NONCLUSTERED INDEX IX_FK_PatientSurgeryQuestionAnswer_Question ON model.PatientSurgeryQuestionAnswers (QuestionId)
GO

