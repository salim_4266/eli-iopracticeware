﻿using FluentMigrator;
using System;

namespace IO.Practiceware.DbMigration.Migrations
{
    [Migration(201206260234)]
    public class Migration201206260234 : Migration
    {
        public override void Up()
        {
            Execute.Sql(
                @"
ALTER TABLE [model].[Printers] ALTER COLUMN Name nvarchar(500) not null
GO

ALTER TABLE [model].[Printers] ALTER COLUMN UncPath nvarchar(500) not null
GO

-- Creating unique key on [Name], [UncPath] in table 'Printers'
ALTER TABLE model.Printers ADD CONSTRAINT [UC_Printers] UNIQUE (Name, UncPath);
GO
");
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
