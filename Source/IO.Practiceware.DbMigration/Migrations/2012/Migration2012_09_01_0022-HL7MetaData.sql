﻿-----New HL7 MetaData

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Acknowledgement'
WHERE Name = 'ACK_V23'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Add/Update Patient'
WHERE Name = 'ADT_A04_V23'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Add/Update Patient'
WHERE Name = 'ADT_A08_V23'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Add Patient'
WHERE Name = 'ADT_A28_V23'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Add/Update Patient'
WHERE Name = 'ADT_A31_V23'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Add Appointment'
WHERE Name = 'SIU_S12_V23'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Add/Reschedule/Check-In Appointment'
WHERE Name = 'SIU_S13_V23'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Check-in/Check-out Appointment'
WHERE Name = 'SIU_S14_V23'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Cancel Appointment'
WHERE Name = 'SIU_S15_V23'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Cancel Appointment'
WHERE Name = 'SIU_S17_V23'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Cancel Appointment'
WHERE Name = 'SIU_S26_V23'


UPDATE model.ExternalSystemMessageTypes
SET Description = 'Master File Add/Update'
WHERE Name = 'MFN_M02_V23'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Charges'
WHERE Name = 'DFT_P03_V231'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Doctor Order'
WHERE Name = 'ORM_O01_V231'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Claim File'
WHERE Name = 'X12_837'

UPDATE model.ExternalSystemMessageTypes
SET Description = 'Confirmation'
WHERE Name = 'Confirmation'



DELETE FROM model.ExternalSystemMessageTypes
WHERE Name = 'MFN_ZPV_V23'