﻿IF NOT EXISTS(SELECT * FROM [model].[PracticeRepositoryEntities] WHERE [Id] = 9)
BEGIN
	INSERT [model].[PracticeRepositoryEntities] ([Id], [Name], [KeyPropertyName]) VALUES (9, N'PatientLanguage', N'Id') 
END
IF NOT EXISTS(SELECT * FROM [model].[PracticeRepositoryEntities] WHERE [Id] = 10)
BEGIN
	INSERT [model].[PracticeRepositoryEntities] ([Id], [Name], [KeyPropertyName]) VALUES (10, N'MaritalStatus', N'Id')
END
GO

IF NOT EXISTS (SELECT * FROM [model].[ExternalSystemMessageTypes] WHERE [Id] = 5 AND [Name] = 'ADT_A31_V23' AND [IsOutbound] = 0)
BEGIN
	UPDATE [model].[ExternalSystemMessageTypes] 
	SET [IsOutbound] = 0
	WHERE [Id] = 5 AND [Name] = 'ADT_A31_V23'
END

SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] ON
IF NOT EXISTS(SELECT * FROM [model].[ExternalSystemMessageTypes] WHERE [Id] = 20 AND [Name] = 'ADT_A31_V23' AND [IsOutbound] = 1)
BEGIN
	INSERT INTO [model].[ExternalSystemMessageTypes](Id, Name, [Description], IsOutbound)
	VALUES (20, 'ADT_A31_V23', '', 1)
END
SET IDENTITY_INSERT [model].[ExternalSystemMessageTypes] OFF
GO