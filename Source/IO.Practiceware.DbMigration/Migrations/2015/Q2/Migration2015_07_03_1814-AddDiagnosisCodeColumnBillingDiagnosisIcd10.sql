IF NOT EXISTS (SELECT 1 FROM sys.Columns WHERE Name = 'DiagnosisCode' AND OBJECT_ID IN (SELECT OBJECT_ID FROM sys.Tables WHERE Name = 'BillingDiagnosisIcd10'))
BEGIN
  ALTER TABLE model.BillingDiagnosisIcd10 
  ADD DiagnosisCode VARCHAR(100) NULL DEFAULT ''

END
