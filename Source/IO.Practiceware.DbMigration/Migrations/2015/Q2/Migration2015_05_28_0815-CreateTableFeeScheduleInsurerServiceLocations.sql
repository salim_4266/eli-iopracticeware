﻿-- Check if 'FeeScheduleInsurerServiceLocations' exists...
IF OBJECT_ID(N'model.FeeScheduleInsurerServiceLocations', 'U') IS NULL
BEGIN
	CREATE TABLE model.FeeScheduleInsurerServiceLocations
	(
		Id INT IDENTITY(1,1),
		FeeScheduleInsurerId INT NOT NULL FOREIGN KEY REFERENCES [model].[FeeScheduleInsurers](Id),
		ServiceLocationId INT NOT NULL,
		CONSTRAINT [PK_FeeScheduleInsurerServiceLocations] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)
	)
END
GO

-- add foreign constraints
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS  WHERE CONSTRAINT_NAME ='FK_FeeScheduleInsurerServiceLocations_FeeScheduleInsurers')
BEGIN 
	ALTER TABLE [model].[FeeScheduleInsurerServiceLocations]
	ADD CONSTRAINT FK_FeeScheduleInsurerServiceLocations_FeeScheduleInsurers
	FOREIGN KEY (FeeScheduleInsurerId)
	REFERENCES [model].[FeeScheduleInsurers](Id)
END
GO
