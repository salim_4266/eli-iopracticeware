﻿-- Check if 'FeeSchedules' exists...
IF OBJECT_ID(N'model.FeeSchedules', 'U') IS NULL
BEGIN

	CREATE TABLE model.FeeSchedules
	(
		Id INT IDENTITY(1,1),
		Name varchar(500),
		StartDate DateTime NOT NULL,
		EndDate DateTime,
		[Description] varchar(500),
		Paid decimal(18,2),
		UseAllowable Bit NOT NULL,
		Deleted bit NOT NULL,
		CONSTRAINT [PK_FeeSchedules] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)
	)
END
GO