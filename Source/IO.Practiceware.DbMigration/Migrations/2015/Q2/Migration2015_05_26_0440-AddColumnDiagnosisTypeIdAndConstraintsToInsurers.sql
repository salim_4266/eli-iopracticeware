IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'[model].[Insurers]') AND name = 'DiagnosisTypeId')
BEGIN
ALTER TABLE [model].[Insurers]
ADD DiagnosisTypeId INT NULL
END
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS  WHERE CONSTRAINT_NAME ='FK_Insurers_DiagnosisTypeId')
BEGIN 
ALTER TABLE [model].[Insurers]
ADD CONSTRAINT FK_Insurers_DiagnosisTypeId
FOREIGN KEY (DiagnosisTypeId)
REFERENCES [model].[DiagnosisTypes](Id)
END
GO