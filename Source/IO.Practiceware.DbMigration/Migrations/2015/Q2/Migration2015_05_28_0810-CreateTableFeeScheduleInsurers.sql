﻿-- Check if 'FeeScheduleInsurers' exists...
IF OBJECT_ID(N'model.FeeScheduleInsurers', 'U') IS NULL
BEGIN
	CREATE TABLE model.FeeScheduleInsurers
	(
		Id INT IDENTITY(1,1),
		FeeScheduleId INT NOT NULL FOREIGN KEY REFERENCES [model].[FeeSchedules](Id),
		InsurerId	INT NOT NULL FOREIGN KEY REFERENCES [model].[Insurers](Id),
		CONSTRAINT [PK_FeeScheduleInsurers] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)
	)
END
GO

-- add foreign constraints
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS  WHERE CONSTRAINT_NAME ='FK_FeeScheduleInsurers_FeeSchedules')
BEGIN 
	ALTER TABLE [model].[FeeScheduleInsurers]
	ADD CONSTRAINT FK_FeeScheduleInsurers_FeeSchedules
	FOREIGN KEY (FeeScheduleId)
	REFERENCES [model].[FeeSchedules](Id)
END
GO

-- add foreign constraints
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS  WHERE CONSTRAINT_NAME ='FK_FeeScheduleInsurers_Insurers')
BEGIN 
	ALTER TABLE [model].[FeeScheduleInsurers]
	ADD CONSTRAINT FK_FeeScheduleInsurers_Insurers
	FOREIGN KEY (InsurerId)
	REFERENCES [model].[Insurers](Id)
END
GO