IF NOT EXISTS (SELECT 1 FROM sys.Columns WHERE Name = 'IsIcd10' AND OBJECT_ID IN (SELECT OBJECT_ID FROM sys.Tables WHERE Name = 'BillingServiceBillingDiagnosis'))
BEGIN
  ALTER TABLE model.BillingServiceBillingDiagnosis 
  ADD IsIcd10 BIT NULL DEFAULT 0

  Exec('UPDATE model.BillingServiceBillingDiagnosis SET IsIcd10 = 0 WHERE IsIcd10 IS NULL')
END

