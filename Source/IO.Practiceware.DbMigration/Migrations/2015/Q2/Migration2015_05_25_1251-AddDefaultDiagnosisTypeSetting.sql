﻿IF NOT EXISTS(SELECT * FROM model.ApplicationSettings WHERE Name = 'DefaultDiagnosisTypeId') 
BEGIN
DECLARE @diagnosisTypeId char(1);
SET @diagnosisTypeId = (SELECT CASE WHEN GETDATE() < '10/1/2015' THEN '1' ELSE '2' END);

INSERT INTO model.ApplicationSettings(Name, Value) VALUES ('DefaultDiagnosisTypeId', @diagnosisTypeId);
END
GO
