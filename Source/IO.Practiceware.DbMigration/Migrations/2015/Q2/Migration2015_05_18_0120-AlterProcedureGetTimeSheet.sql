IF OBJECT_ID ( 'model.GetTimeSheet', 'P' ) IS NOT NULL 
	DROP PROCEDURE [model].[GetTimeSheet]
GO

CREATE PROCEDURE [model].[GetTimeSheet](@StartDate datetime, @EndDate datetime, @Users varchar(max))
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
	BEGIN

	set @StartDate = convert(varchar(8),@StartDate,112)
	set @EndDate = convert(varchar(8),@EndDate,112)

	SELECT eh.UserId, 
		u.DisplayName,
		u.UserName,
		CONVERT(VARCHAR(8),eh.StartDateTime,112) as Date,
		ISNULL(CONVERT(VARCHAR(10),eh.StartDateTime,108), '') as ClockIn,
		ISNULL(CONVERT(VARCHAR(10),eh.EndDateTime,108), '') as ClockOut,		
		ISNULL(CAST(CAST(DATEDIFF(minute,StartDateTime,EndDateTime) AS DECIMAL(6,2))/60 AS DECIMAL(6,2)), 0) AS Total
		FROM model.EmployeeHours eh
		INNER JOIN model.Users u ON u.Id = eh.UserId	
	WHERE ((@Users IS NULL AND eh.UserId IN (SELECT Id FROM model.Users))
		OR (@Users IS NOT NULL AND eh.UserId IN (SELECT CONVERT(int,nstr) FROM CharTable(@Users,','))))
		AND CONVERT(VARCHAR(8),eh.StartDateTime,112) BETWEEN @StartDate AND @EndDate	
END
RETURN
END

-- Update TimeSheet report
UPDATE model.Reports SET Name = 'Timesheet', Content =
'<?xml version="1.0" encoding="utf-8"?>
<Report DataSourceName="GetTimeSheet" Width="6.5in" Name="TimeSheet" xmlns="http://schemas.telerik.com/reporting/2012/3.5">
  <Style>
    <Font Name="Calibri" />
  </Style>
  <DataSources>
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="model.GetTimeSheet" SelectCommandType="StoredProcedure" Name="GetTimeSheet">
      <Parameters>
        <SqlDataSourceParameter DbType="DateTime" Name="@StartDate">
          <Value>
            <String>=Parameters.StartDate.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="DateTime" Name="@EndDate">
          <Value>
            <String>=Parameters.EndDate.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="AnsiString" Name="@Users">
          <Value>
            <String>=Join(",", Parameters.Users.Value)</String>
          </Value>
        </SqlDataSourceParameter>        
      </Parameters>
      <DefaultValues>
        <SqlDataSourceParameter DbType="DateTime" Name="@StartDate">
          <Value>
            <String>3/17/2014</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="DateTime" Name="@EndDate">
          <Value>
            <String>3/18/2014</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="AnsiString" Name="@Users">
          <Value>
            <String>2</String>
          </Value>
        </SqlDataSourceParameter>
      </DefaultValues>
    </SqlDataSource>
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT u.UserName, u.Id FROM model.Users u&#xD;&#xA;WHERE IsArchived = 0&#xD;&#xA;ORDER BY u.UserName" Name="GetUsers" />
  </DataSources>
  <Items>
    <DetailSection Height="0.199999968210856in" Name="detailSection1">
      <Style>
        <Font Name="Calibri" />
      </Style>
      <Items>
        <TextBox Width="0.899999956289927in" Height="0.199921131134033in" Left="4.5in" Top="0in" Value="= Fields.Total" Format="{0:N2}" Name="textBox24">
          <Style TextAlign="Right" VerticalAlign="Middle">
            <Font Name="Arial" Bold="False" />
          </Style>
          <ConditionalFormatting>
            <FormattingRule>
              <Style Visible="False" />
              <Filters>
                <Filter Expression="=Fields.Total" Operator="Equal" Value="=NULL" />
              </Filters>
            </FormattingRule>
          </ConditionalFormatting>
        </TextBox>
        <TextBox Width="1.1000000834465in" Height="0.199921131134033in" Left="3in" Top="0in" Value="= Fields.ClockOut" Name="textBox23">
          <Style VerticalAlign="Middle">
            <Font Name="Arial" />
          </Style>
        </TextBox>
        <TextBox Width="1.20000016689301in" Height="0.199921131134033in" Left="1.30000003178914in" Top="0in" Value="= Fields.ClockIn" Name="textBox22">
          <Style VerticalAlign="Middle">
            <Font Name="Arial" />
          </Style>
        </TextBox>
        <TextBox Width="0.800000131130219in" Height="0.199921131134033in" Left="0in" Top="0in" Value="=Fields.Date" Name="textBox10">
          <Style VerticalAlign="Middle">
            <Font Name="Arial" />
          </Style>
        </TextBox>
      </Items>
    </DetailSection>
    <ReportFooterSection Height="0.0805360476175944in" Name="reportFooterSection1">
      <Style Visible="False">
        <Font Name="Calibri" />
      </Style>      
    </ReportFooterSection>
    <PageHeaderSection Height="0.300000031789144in" Name="pageHeaderSection1">
      <Style>
        <Font Name="Calibri" />
      </Style>
      <Items>
        <TextBox Width="1.20000072320302in" Height="0.300000031789144in" Left="5.29999927679698in" Top="0in" Value="Page: {PageNumber}" Docking="Right" Name="textBox6">
          <Style TextAlign="Right" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="1.5in" Height="0.299999872843424in" Left="2.5in" Top="0in" Value="Timesheet" Anchoring="Left, Right" Name="textBox1">
          <Style TextAlign="Center" VerticalAlign="Middle">
            <Font Name="Arial" Size="14pt" Bold="True" />
          </Style>
        </TextBox>
        <HtmlTextBox Width="1.18110239505768in" Height="0.180417944987615in" Left="0.00003941853841146in" Top="0.00003941853841146in" Value="General" Name="ReportCategory">
          <Style Visible="False" />
        </HtmlTextBox>
      </Items>
    </PageHeaderSection>
    <ReportHeaderSection Height="1.61946392059326in" Name="reportHeaderSection1">
      <Style>
        <Font Name="Arial" Size="12pt" />
      </Style>
      <Items>
        <TextBox Width="0.906092325846354in" Height="0.168671607971191in" Left="0.0000393788019816in" Top="0.486170142889023in" Value="Start Date:" Name="textBox46">
          <Style VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="0.906171143054962in" Height="0.166588142514229in" Left="0in" Top="0.82359234491984in" Value="User(s):" Name="textBox19">
          <Style VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="2.90660262107849in" Height="0.168592765927315in" Left="0.90625in" Top="0.654920736948649in" Value="= Parameters.EndDate.Value" Format="{0:d}" Name="textBox20">
          <Style VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="0.906131744384766in" Height="0.168592765927315in" Left="0in" Top="0.654920657475789in" Value="End Date:" Name="textBox21">
          <Style VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" Bold="True" />
          </Style>
        </TextBox>
        <TextBox Width="2.90660254160563in" Height="0.168671607971191in" Left="0.90625in" Top="0.486170142889023in" Value="= Parameters.StartDate.Value" Format="{0:d}" Name="textBox45">
          <Style VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="5.59371089935303in" Height="0.166588068008423in" Left="0.90625in" Top="0.82359234491984in" Value="=IsNull(Join(&quot;, &quot;,Parameters.Users.Label),&quot;All&quot;)" Format="{0:d}" Name="textBox17">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" Bold="False" Underline="False" />
          </Style>
        </TextBox>
        <HtmlTextBox Width="1.59370867411296in" Height="0.199999809265137in" Left="4.90625190734863in" Top="0.519503355026245in" Value="Totals are calculated by the fraction of the hour, for example if the total says .05, the actual amount of time that the user was checked in for was 3 minutes because .05*60=3" Name="Documentation">
          <Style Visible="False" VerticalAlign="Middle">
            <Font Name="Arial" Size="10pt" />
          </Style>
        </HtmlTextBox>
        <TextBox Width="1.39995896816254in" Height="0.154166688521703in" Left="5.10000165303548in" Top="0in" Value="Date run:   {Now().ToString(&quot;MM/dd/yyyy&quot;)}" Name="textBox7">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="1.39995896816254in" Height="0.179358959197998in" Left="5.10000165303548in" Top="0.154284795125326in" Value="Time run:  {Now().ToString(&quot;hh:mm tt&quot;)}" Name="textBox14">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <Crosstab DataSourceName="GetTimeSheet" Width="1.99487833595412in" Height="0.439583607528085in" Left="0.00003937880198161in" Top="1.17980114618937in" Name="crosstab1" StyleName="Normal.TableNormal">
          <Body>
            <Cells>
              <TableCell RowIndex="1" ColumnIndex="0" RowSpan="1" ColumnSpan="1">
                <ReportItem>
                  <TextBox Width="0.891827919506241in" Height="0.200000254313151in" Left="0in" Top="0in" Value="=Sum(Fields.Total)" Name="textBox31" StyleName="Normal.TableBody">
                    <Style TextAlign="Right" VerticalAlign="Middle" />
                  </TextBox>
                </ReportItem>
              </TableCell>
              <TableCell RowIndex="0" ColumnIndex="0" RowSpan="1" ColumnSpan="1">
                <ReportItem>
                  <TextBox Width="0.891827908798869in" Height="0.239583344319261in" Left="0in" Top="0in" Value="Hours" Name="textBox4" StyleName="Normal.TableBody">
                    <Style TextAlign="Left" VerticalAlign="Middle">
                      <Font Bold="True" />
                    </Style>
                  </TextBox>
                </ReportItem>
              </TableCell>
            </Cells>
            <Columns>
              <Column Width="0.89182792734034in" />
            </Columns>
            <Rows>
              <Row Height="0.239583344319261in" />
              <Row Height="0.200000263208824in" />
            </Rows>
          </Body>
          <Corner />
          <Style VerticalAlign="Middle">
            <Font Name="Arial" />
          </Style>
          <RowGroups>
            <TableGroup Name="group3">
              <ReportItem>
                <TextBox Width="1.10305054605926in" Height="0.239583344319261in" Left="0in" Top="0in" Value="User" Name="textBox3" StyleName="Normal.TableGroup">
                  <Style VerticalAlign="Middle">
                    <Font Bold="True" />
                  </Style>
                </TextBox>
              </ReportItem>
            </TableGroup>
            <TableGroup Name="UserName1">
              <ReportItem>
                <TextBox Width="1.10305058532299in" Height="0.200000254313151in" Left="0in" Top="0in" Value="=Fields.UserName" Name="textBox30" StyleName="Normal.TableGroup">
                  <Style VerticalAlign="Middle" />
                </TextBox>
              </ReportItem>
              <Groupings>
                <Grouping Expression="=Fields.UserName" />
              </Groupings>
              <Sortings>
                <Sorting Expression="=Fields.UserName" Direction="Asc" />
              </Sortings>
            </TableGroup>
          </RowGroups>
          <ColumnGroups>
            <TableGroup />
          </ColumnGroups>
        </Crosstab>
      </Items>
    </ReportHeaderSection>
  </Items>
  <StyleSheet>
    <StyleRule>
      <Style>
        <Padding Left="2pt" Right="2pt" />
      </Style>
      <Selectors>
        <TypeSelector Type="TextItemBase" />
        <TypeSelector Type="HtmlTextBox" />
      </Selectors>
    </StyleRule>
    <StyleRule>
      <Style Color="Black">
        <BorderStyle Default="Solid" />
        <BorderColor Default="Black" />
        <BorderWidth Default="1px" />
        <Font Name="Tahoma" Size="9pt" />
      </Style>
      <Selectors>
        <StyleSelector Type="Table" StyleName="Normal.TableNormal" />
      </Selectors>
    </StyleRule>
    <StyleRule>
      <Style VerticalAlign="Middle">
        <BorderStyle Default="Solid" />
        <BorderColor Default="Black" />
        <BorderWidth Default="1px" />
        <Font Name="Tahoma" Size="10pt" />
      </Style>
      <Selectors>
        <DescendantSelector>
          <Selectors>
            <TypeSelector Type="Table" />
            <StyleSelector Type="ReportItem" StyleName="Normal.TableHeader" />
          </Selectors>
        </DescendantSelector>
      </Selectors>
    </StyleRule>
    <StyleRule>
      <Style>
        <BorderStyle Default="Solid" />
        <BorderColor Default="Black" />
        <BorderWidth Default="1px" />
        <Font Name="Tahoma" Size="9pt" />
      </Style>
      <Selectors>
        <DescendantSelector>
          <Selectors>
            <TypeSelector Type="Table" />
            <StyleSelector Type="ReportItem" StyleName="Normal.TableBody" />
          </Selectors>
        </DescendantSelector>
      </Selectors>
    </StyleRule>
    <StyleRule>
      <Style>
        <BorderStyle Default="Solid" />
        <BorderColor Default="Black" />
        <BorderWidth Default="1px" />
        <Font Name="Tahoma" Size="9pt" />
      </Style>
      <Selectors>
        <DescendantSelector>
          <Selectors>
            <TypeSelector Type="Table" />
            <StyleSelector Type="ReportItem" StyleName="Normal.TableGroup" />
          </Selectors>
        </DescendantSelector>
      </Selectors>
    </StyleRule>
  </StyleSheet>
  <PageSettings>
    <PageSettings PaperKind="Letter" Landscape="False">
      <Margins>
        <MarginsU Left="1in" Right="1in" Top="1in" Bottom="1in" />
      </Margins>
    </PageSettings>
  </PageSettings>
  <Sortings>
    <Sorting Expression="= Fields.Date" Direction="Asc" />
    <Sorting Expression="= Fields.ClockIn" Direction="Desc" />
  </Sortings>
  <Groups>
    <Group Name="group2">
      <GroupHeader>
        <GroupHeaderSection PrintOnEveryPage="True" PageBreak="Before" Height="0.200237433115641in" Name="groupHeaderSection2">
          <Items>
            <TextBox Width="1.20000016689301in" Height="0.19999997317791in" Left="1.30000003178914in" Top="0in" Value="Clock In" Name="textBox11">
              <Style VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" Underline="False" />
              </Style>
            </TextBox>
            <TextBox Width="0.800000131130219in" Height="0.19999997317791in" Left="0in" Top="0.00023746490478516in" Value="Date" Name="textBox9">
              <Style VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" Underline="False" />
              </Style>
            </TextBox>
            <TextBox Width="1.10000014305115in" Height="0.19999997317791in" Left="3in" Top="0in" Value="Clock Out" Name="textBox12">
              <Style VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" Underline="False" />
              </Style>
            </TextBox>
            <TextBox Width="0.899999976158142in" Height="0.19999997317791in" Left="4.49166679382324in" Top="0.00023746490478516in" Value="Total" Name="textBox13">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" Underline="False" />
              </Style>
            </TextBox>
          </Items>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.0999609629313152in" Name="groupFooterSection2">
          <Style Visible="False" />
        </GroupFooterSection>
      </GroupFooter>
    </Group>
    <Group Name="group">
      <GroupHeader>
        <GroupHeaderSection Height="0.20007864634196in" Name="groupHeaderSection">
          <Style>
            <Font Name="Calibri" />
          </Style>
          <Items>
            <TextBox Width="1.79382848739624in" Height="0.199999809265137in" Left="0.90625in" Top="0in" Value="= Fields.DisplayName" Name="textBox15">
              <Style VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" Underline="False" />
              </Style>
            </TextBox>
            <TextBox Width="0.906171143054962in" Height="0.19999997317791in" Left="0in" Top="0in" Value="User:" Name="textBox29">
              <Style VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
          </Items>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection PageBreak="After" Height="0.199999809265137in" Name="groupFooterSection">
          <Style>
            <Font Name="Calibri" />
          </Style>
          <Items>
            <TextBox Width="2.19999994834265in" Height="0.199999809265137in" Left="1.90000025431315in" Top="0in" Value="Total Hours for {UserName}:" Name="textBox2">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="0.902004897594452in" Height="0.199999809265137in" Left="4.49799505869548in" Top="0in" KeepTogether="False" Value="= Sum(Fields.Total)" Format="{0:N2}" Name="textBox8">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
          </Items>
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="=Fields.UserName" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= Fields.UserName" Direction="Asc" />
      </Sortings>
    </Group>
    <Group Name="group1">
      <GroupHeader>
        <GroupHeaderSection Height="0.0999215443929034in" Name="groupHeaderSection1">
          <Style Visible="False">
            <Font Name="Calibri" />
          </Style>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.200039227803548in" Name="groupFooterSection1">
          <Style TextAlign="Right">
            <Font Name="Calibri" />
          </Style>
          <Items>
            <TextBox Width="2.20000004768372in" Height="0.199999809265137in" Left="1.90000009536743in" Top="0in" Value="Total Hours:" Name="textBox18">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="0.891666432221731in" Height="0.199999809265137in" Left="4.5in" Top="0in" KeepTogether="False" Value="= Sum(Fields.Total)" Format="{0:N2}" Name="textBox5">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Name="Arial" Bold="False" />
              </Style>
            </TextBox>
          </Items>
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="=Fields.Date" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= Fields.Date" Direction="Asc" />
      </Sortings>
    </Group>
  </Groups>
  <ReportParameters>
    <ReportParameter Name="StartDate" Type="DateTime" Text="Start Date" Visible="True" />
    <ReportParameter Name="EndDate" Type="DateTime" Text="End Date" Visible="True" />
    <ReportParameter Name="Users" Text="Users" Visible="True" MultiValue="True" AllowNull="True">
      <AvailableValues DataSourceName="GetUsers" DisplayMember="= Fields.UserName" ValueMember="= Fields.Id" />
    </ReportParameter>    
  </ReportParameters>
</Report>' WHERE ReportTypeId=1 AND Name = 'Timesheet'