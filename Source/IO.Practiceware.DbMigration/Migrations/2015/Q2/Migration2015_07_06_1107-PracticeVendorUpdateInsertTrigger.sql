--Bug #19164

IF EXISTS (SELECT * FROM sys.triggers WHERE Name = 'PracticeVendorsInsertUpdate') 
	DROP TRIGGER [dbo].[PracticeVendorsInsertUpdate];

GO
CREATE TRIGGER [dbo].[PracticeVendorsInsertUpdate] ON [dbo].[PracticeVendors]
INSTEAD OF UPDATE,INSERT
NOT FOR REPLICATION 
AS
BEGIN
SET NOCOUNT ON;
DECLARE @VendorId INT
DECLARE @VendorName NVARCHAR(128)
DECLARE @VendorType NVARCHAR(2)
DECLARE @VendorAddress NVARCHAR(100)
DECLARE @VendorSuite NVARCHAR(70)
DECLARE @VendorCity NVARCHAR(70)
DECLARE @VendorState NVARCHAR(4)
DECLARE @VendorZip NVARCHAR(20)
DECLARE @VendorPhone NVARCHAR(32)
DECLARE @VendorFax NVARCHAR(36)
DECLARE @VendorEmail NVARCHAR(64)
DECLARE @VendorTaxId NVARCHAR(64)
DECLARE @VendorUPIN NVARCHAR(64)
DECLARE @VendorSpecialty NVARCHAR(128)
DECLARE @VendorOtherOffice1 NVARCHAR(40)
DECLARE @VendorOtherOffice2 NVARCHAR(32)
DECLARE @VendorLastName NVARCHAR(40) 
DECLARE @VendorFirstName NVARCHAR(32)
DECLARE @VendorMI NVARCHAR(2)
DECLARE @VendorTitle NVARCHAR(16)
DECLARE @VendorSalutation NVARCHAR(50)
DECLARE @VendorFirmName NVARCHAR(128)
DECLARE @VendorTaxonomy NVARCHAR(32) 
DECLARE @VendorExcRefDr NVARCHAR(2) 
DECLARE @VendorNpi NVARCHAR(20) 
DECLARE @VendorCell NVARCHAR(50)
DECLARE @VendorSpecOther NVARCHAR(128)
DECLARE @OrdinalId INT 


DECLARE PracticeVendorsUpdateCursor CURSOR FOR
SELECT
VendorId,
VendorName,
VendorType,
VendorAddress,
VendorSuite,
VendorCity,
VendorState,
VendorZip,
VendorPhone,
VendorFax,
VendorEmail,
VendorTaxId,
VendorUPIN,
VendorSpecialty,
VendorOtherOffice1,
VendorOtherOffice2,
VendorLastName,
VendorFirstName,
VendorMI,
VendorTitle,
VendorSalutation,
VendorFirmName,
VendorTaxonomy,
VendorExcRefDr,
VendorNpi,
VendorCell,
VendorSpecOther,
OrdinalId
FROM inserted

OPEN PracticeVendorsUpdateCursor FETCH NEXT FROM PracticeVendorsUpdateCursor INTO
	@VendorId,@VendorName,@VendorType,@VendorAddress,@VendorSuite,@VendorCity,@VendorState,
	@VendorZip,@VendorPhone,@VendorFax,@VendorEmail,@VendorTaxId,@VendorUPIN,@VendorSpecialty,
	@VendorOtherOffice1,@VendorOtherOffice2,@VendorLastName,@VendorFirstName,@VendorMI,
	@VendorTitle,@VendorSalutation,@VendorFirmName,@VendorTaxonomy,@VendorExcRefDr,@VendorNpi,
	@VendorCell,@VendorSpecOther,@OrdinalId
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		IF ((@VendorType = '~'))
		BEGIN

		IF(@VendorId = 0)
		BEGIN
			INSERT INTO model.ExternalContacts(FirstName,LastNameOrEntityName,DisplayName,IsArchived,IsEntity,OrdinalId,__EntityType__,ExcludeOnClaim) VALUES(@VendorFirstName,@VendorLastName,@VendorName,0,0,1,'ExternalContact',0)
			SELECT TOP 1 @VendorId = Id FROM model.ExternalContacts WHERE LastNameOrEntityName = @VendorLastName AND DisplayName = @VendorName AND __EntityType__ ='ExternalContact'
		END

		UPDATE model.ExternalContacts 
		SET  DisplayName = @VendorName,
		LastNameOrEntityName = @VendorLastName,
		FirstName = @VendorFirstName,
		MiddleName = @VendorMI,
		Suffix = @VendorTitle,
		LegacyTaxonomy = @VendorTaxonomy,
		ExcludeOnClaim = CASE WHEN @VendorExcRefDr = 'T' THEN 1 ELSE 0 END,
		LegacyNpi = @VendorNpi
		WHERE  Id = @VendorId 

		-------------------------------------------Address
		IF NOT EXISTS(SELECT TOP 1 * FROM model.ExternalContactAddresses WHERE ExternalContactId = @VendorId AND ContactAddressTypeId = 8)
		BEGIN
			IF(@VendorState <> '') AND EXISTS(SELECT TOP 1 * FROM model.StateOrProvinces WHERE Abbreviation = @VendorState)
				INSERT INTO model.ExternalContactAddresses(ExternalContactId, Line1, Line2, Line3, City, StateOrProvinceId, PostalCode, ContactAddressTypeId)
				SELECT @VendorId AS ExternalContactId,
				@VendorAddress AS Line1,
				@VendorSuite AS Line2,
				'' AS Line3,
				@VendorCity AS City,
				(SELECT TOP 1 Id FROM model.StateOrProvinces WHERE Abbreviation = @VendorState) AS StateOrProvinceId,
				@VendorZip AS PostalCode,
				8 AS ContactAddressTypeId
		END
	
		UPDATE model.ExternalContactAddresses 
		SET Line1 = @VendorAddress,
		Line2 = @VendorSuite,
		City = @VendorCity,
		PostalCode  = @VendorZip
		WHERE ExternalContactId = @VendorId AND ContactAddressTypeId = 8

		
		------------------------------------------State
		IF(@VendorState <> '') AND EXISTS(SELECT TOP 1 * FROM model.StateOrProvinces WHERE Abbreviation = @VendorState)
		BEGIN
			UPDATE model.ExternalContactAddresses
			SET StateOrProvinceId = (SELECT TOP 1 Id FROM model.StateOrProvinces WHERE Abbreviation = @VendorState)
			WHERE ExternalContactId = @VendorId AND ContactAddressTypeId = 8
		END
		IF(@VendorState = '' OR @VendorState IS NULL)
			DELETE FROM model.ExternalContactAddresses WHERE ExternalContactId = @VendorId AND ContactAddressTypeId = 8

		-----------------------------------------Phone Number 
		IF NOT EXISTS(SELECT TOP 1 * FROM model.ExternalContactPhoneNumbers WHERE ExternalContactId = @VendorId AND ExternalContactPhoneNumberTypeId = 5)
		BEGIN
			IF(@VendorPhone <> '')
				INSERT INTO model.ExternalContactPhoneNumbers(ExternalContactId, IsInternational, CountryCode, AreaCode, ExchangeAndSuffix, Extension, OrdinalId, ExternalContactPhoneNumberTypeId)
				SELECT @VendorId AS ExternalContactId,
				0 AS IsInternational,
				CASE WHEN LEN(@VendorPhone) > 1 THEN SUBSTRING(@VendorPhone,1,1) ELSE '' END AS CountryCode,
				CASE WHEN LEN(@VendorPhone) > 2 THEN SUBSTRING(@VendorPhone,1,3) ELSE '' END AS AreaCode,
				CASE WHEN LEN(@VendorPhone) > 2 THEN SUBSTRING(@VendorPhone,3,LEN(@VendorPhone)) ELSE '' END AS ExchangeAndSuffix,
				NULL AS Extension,
				1 AS OrdinalId,
				5 AS ExternalContactPhoneNumberTypeId 
			END
		ELSE
		BEGIN
			UPDATE model.ExternalContactPhoneNumbers SET AreaCode = CASE WHEN LEN(@VendorPhone) > 2 THEN SUBSTRING(@VendorPhone,1,3) ELSE '' END,
			ExchangeAndSuffix = CASE WHEN LEN(@VendorPhone) > 2 THEN SUBSTRING(@VendorPhone,3,LEN(@VendorPhone)) ELSE '' END
			WHERE ExternalContactPhoneNumberTypeId = (SELECT Id FROM model.ExternalContactPhoneNumberTypes WHERE ID = 5) AND ExternalContactId = @VendorId
		END
		IF(@VendorPhone = '' OR @VendorPhone IS NULL)
		DELETE FROM model.ExternalContactPhoneNumbers WHERE ExternalContactId = @VendorId AND ExternalContactPhoneNumberTypeId = 5 AND OrdinalId = 1

		----------------------------------------Cell Phone Number	
		IF NOT EXISTS(SELECT TOP 1 * FROM model.ExternalContactPhoneNumbers WHERE ExternalContactId = @VendorId AND ExternalContactPhoneNumberTypeId IN((SELECT TOP 1 Id FROM model.ExternalContactPhoneNumberTypes WHERE Name = 'Cell')))
		BEGIN
		IF(@VendorPhone <> '')
			INSERT INTO model.ExternalContactPhoneNumbers(ExternalContactId, IsInternational, CountryCode, AreaCode, ExchangeAndSuffix, Extension, OrdinalId, ExternalContactPhoneNumberTypeId)
			SELECT @VendorId AS ExternalContactId,
			0 AS IsInternational,
			CASE WHEN LEN(@VendorPhone) > 1 THEN SUBSTRING(@VendorPhone,1,1) ELSE '' END AS CountryCode,
			CASE WHEN LEN(@VendorPhone) > 2 THEN SUBSTRING(@VendorPhone,1,3) ELSE '' END AS AreaCode,
			CASE WHEN LEN(@VendorPhone) > 2 THEN SUBSTRING(@VendorPhone,3,LEN(@VendorPhone)) ELSE '' END AS ExchangeAndSuffix,
			NULL AS Extension,
			1 AS OrdinalId,
			(SELECT TOP 1 Id FROM model.ExternalContactPhoneNumberTypes WHERE Name = 'Cell') AS ExternalContactPhoneNumberTypeId 
		END
		ELSE
		BEGIN
			UPDATE model.ExternalContactPhoneNumbers SET AreaCode = CASE WHEN LEN(@VendorCell) > 2 THEN SUBSTRING(@VendorCell,1,3) ELSE '' END,
			ExchangeAndSuffix = CASE WHEN LEN(@VendorCell) > 2 THEN SUBSTRING(@VendorCell,3,LEN(@VendorCell)) ELSE '' END
			WHERE ExternalContactPhoneNumberTypeId = (SELECT TOP  1 Id FROM model.ExternalContactPhoneNumberTypes WHERE Name = 'Cell') AND ExternalContactId = @VendorId
		END
		--Delete Cell Phone Number
		IF(@VendorPhone = '' OR @VendorPhone IS NULL)
		DELETE FROM model.ExternalContactPhoneNumbers WHERE ExternalContactId = @VendorId AND ExternalContactPhoneNumberTypeId = (SELECT TOP  1 Id FROM model.ExternalContactPhoneNumberTypes WHERE Name = 'Cell') AND OrdinalId = 1

		----------------------------------------Email Address
		IF NOT EXISTS(SELECT TOP 1 * FROM model.ExternalContactEmailAddresses WHERE ExternalContactId = @VendorId AND EmailAddressTypeId = 1)
		BEGIN
			IF(@VendorEmail <> '')
				INSERT INTO model.ExternalContactEmailAddresses(ExternalContactId, Value, EmailAddressTypeId, OrdinalId)
				SELECT @VendorId As ExternalContactId,
				@VendorEmail AS Value,
				1 AS EmailAddressTypeId,
				0 AS OrdinalId
		END
		ELSE
		BEGIN
			UPDATE model.ExternalContactEmailAddresses SET Value = @VendorEmail WHERE ExternalContactId = @VendorId AND EmailAddressTypeId = 1 AND OrdinalId = 0
		END

		--Delete
		IF(@VendorEmail = '' OR @VendorEmail IS NULL)
		DELETE FROM model.ExternalContactEmailAddresses WHERE ExternalContactId = @VendorId AND EmailAddressTypeId = 1 AND OrdinalId = 0
		
	
			FETCH NEXT FROM PracticeVendorsUpdateCursor INTO
			@VendorId,@VendorName,@VendorType,@VendorAddress,@VendorSuite,@VendorCity,@VendorState,
			@VendorZip,@VendorPhone,@VendorFax,@VendorEmail,@VendorTaxId,@VendorUPIN,@VendorSpecialty,
			@VendorOtherOffice1,@VendorOtherOffice2,@VendorLastName,@VendorFirstName,@VendorMI,
			@VendorTitle,@VendorSalutation,@VendorFirmName,@VendorTaxonomy,@VendorExcRefDr,@VendorNpi,
			@VendorCell,@VendorSpecOther,@OrdinalId

			CLOSE PracticeVendorsUpdateCursor
			DEALLOCATE PracticeVendorsUpdateCursor
			END
	END
END
GO

--Delete
IF EXISTS (SELECT * FROM sys.triggers WHERE Name = 'PracticeVendorsDelete') 
	DROP TRIGGER [dbo].[PracticeVendorsDelete];

GO
CREATE TRIGGER [dbo].[PracticeVendorsDelete] ON [dbo].[PracticeVendors]
INSTEAD OF Delete
NOT FOR REPLICATION 
AS
BEGIN
SET NOCOUNT ON;
DECLARE @VendorId INT
DECLARE @VendorType NVARCHAR(2)

DECLARE PracticeVendorsDeleteCursor CURSOR FOR
SELECT
VendorId,
VendorType
FROM Deleted

OPEN PracticeVendorsDeleteCursor FETCH NEXT FROM PracticeVendorsDeleteCursor INTO
	@VendorId,@VendorType
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		IF ((@VendorType = '~'))
		BEGIN

		DELETE FROM model.ExternalContactAddresses WHERE ExternalContactId = @VendorId AND ContactAddressTypeId = 8
		DELETE FROM model.ExternalContactEmailAddresses WHERE ExternalContactId = @VendorId AND EmailAddressTypeId = 1 AND OrdinalId = 0
		DELETE FROM model.ExternalContactPhoneNumbers WHERE ExternalContactId = @VendorId AND ExternalContactPhoneNumberTypeId = 5 AND OrdinalId = 1
		DELETE FROM model.ExternalContactPhoneNumbers WHERE ExternalContactId = @VendorId AND ExternalContactPhoneNumberTypeId = (SELECT TOP  1 Id FROM model.ExternalContactPhoneNumberTypes WHERE Name = 'Cell') AND OrdinalId = 1
		DELETE FROM model.ExternalContactEmailAddresses WHERE ExternalContactId = @VendorId AND EmailAddressTypeId = 1 AND OrdinalId = 0
		DELETE FROM model.ExternalContacts WHERE Id = @VendorId

	
	FETCH NEXT FROM PracticeVendorsDeleteCursor INTO
	@VendorId,@VendorType
	CLOSE PracticeVendorsDeleteCursor
	DEALLOCATE PracticeVendorsDeleteCursor
		END
	END
END
GO