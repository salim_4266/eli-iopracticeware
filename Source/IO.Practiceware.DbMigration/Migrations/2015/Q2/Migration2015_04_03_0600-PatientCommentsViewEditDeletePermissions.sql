﻿IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Name = 'View Patient Comments' AND PermissionCategoryId = 12)
BEGIN

SET IDENTITY_INSERT model.Permissions ON
INSERT INTO model.Permissions (Id, Name, PermissionCategoryId) VALUES (64, 'View Patient Comments', 12)
SET IDENTITY_INSERT model.Permissions OFF
END
GO


INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)
SELECT 0 AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id = 64
WHERE ResourceType <> 'R' AND
 SUBSTRING(re.ResourcePermissions, 12, 1) = 1
 AND ResourceId > 0

EXCEPT

SELECT 0, UserId, PermissionId 
FROM model.UserPermissions
GO

IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Name = 'Edit Patient Comments' AND PermissionCategoryId = 12)
BEGIN

SET IDENTITY_INSERT model.Permissions ON
INSERT INTO model.Permissions (Id, Name, PermissionCategoryId) VALUES (65, 'Edit Patient Comments', 12)
SET IDENTITY_INSERT model.Permissions OFF
END
GO


INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)
SELECT 0 AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id = 65
WHERE ResourceType <> 'R' AND
 SUBSTRING(re.ResourcePermissions, 12, 1) = 1
 AND ResourceId > 0

EXCEPT

SELECT 0, UserId, PermissionId 
FROM model.UserPermissions
GO

IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Name = 'Delete Patient Comments' AND PermissionCategoryId = 12)
BEGIN

SET IDENTITY_INSERT model.Permissions ON
INSERT INTO model.Permissions (Id, Name, PermissionCategoryId) VALUES (66, 'Delete Patient Comments', 12)
SET IDENTITY_INSERT model.Permissions OFF
END
GO

INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)
SELECT 0 AS IsDenied,
	   re.ResourceId AS UserId,
	   pe.Id AS PermissionId
FROM dbo.Resources re
INNER JOIN model.Permissions pe ON pe.Id = 66
WHERE ResourceType <> 'R' AND
 SUBSTRING(re.ResourcePermissions, 12, 1) = 1
 AND ResourceId > 0

EXCEPT

SELECT 0, UserId, PermissionId 
FROM model.UserPermissions
GO