﻿-- Check if 'EmployeeHours' exists...
IF OBJECT_ID(N'model.EmployeeHours', 'U') IS NULL
BEGIN
CREATE TABLE [model].[EmployeeHours](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NULL,
	[UserId] [int] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	CONSTRAINT [PK_EmployeeHours] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)
)

--Inserting data in to new table
INSERT INTO model.EmployeeHours (StartDateTime, EndDateTime, UserId)
SELECT C.StartDateTime, max(C.EndDateTime), C.UserId 
FROM
(
	SELECT distinct A.UserId, A.StartDateTime, B.EndDateTime
	FROM
	(
		SELECT Date, Time, DATEADD(MI, pa.Time, CONVERT(DATETIME, pa.Date, 112))  AS StartDateTime, pa.UserId, pa.Action
		FROM PracticeAudit pa WHERE Action IN ('I') AND RecordId = 1 AND CurrentRec = '' AND PrevRec = ''
	) A

	LEFT JOIN 
	(
		SELECT Date, Time, DATEADD(MI, pa.Time, CONVERT(DATETIME, pa.Date, 112))  AS EndDateTime, pa.UserId, pa.Action, pa.RecordId
		FROM PracticeAudit pa WHERE Action IN ('O') AND RecordId = 1 AND CurrentRec = '' AND PrevRec = ''
	) B 
	ON A.Date = B.Date AND A.UserId = B.UserId AND A.Action <> B.Action AND A.Time <= B.Time 
  ) C
  GROUP BY C.UserId, C.StartDateTime
END
GO
-- Creating foreign key on [UserId] in table 'EmployeeHours'
IF OBJECTPROPERTY(OBJECT_ID('[model].[Users]'), 'IsUserTable') = 1
ALTER TABLE [model].[EmployeeHours]
ADD CONSTRAINT [FK_EmployeeHourUser]
    FOREIGN KEY ([UserId])
    REFERENCES [model].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeHourUser'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_EmployeeHourUser')
	DROP INDEX IX_FK_EmployeeHourUser ON [model].[EmployeeHours]
GO
CREATE INDEX [IX_FK_EmployeeHourUser]
ON [model].[EmployeeHours]
    ([UserId]);
GO
