﻿-- Check if 'DiagnosisTypes' exists...
IF OBJECT_ID(N'model.DiagnosisTypes', 'U') IS NULL
BEGIN
	CREATE TABLE [model].[DiagnosisTypes](
		[Id] [int] IDENTITY(1,1) NOT NULL,	
		[Name] nvarchar(max)  NOT NULL,
		CONSTRAINT [PK_DiagnosisTypes] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)
	)

--Inserting data in to new table
	IF NOT EXISTS(SELECT * FROM model.DiagnosisTypes WHERE Id = 1)
	BEGIN 
		SET IDENTITY_INSERT model.DiagnosisTypes ON

		INSERT INTO model.DiagnosisTypes (Id, Name) Values(1, 'ICD 9');
    
		SET IDENTITY_INSERT model.DiagnosisTypes OFF
	END

	IF NOT EXISTS(SELECT * FROM model.DiagnosisTypes WHERE Id = 2)
	BEGIN 
		SET IDENTITY_INSERT model.DiagnosisTypes ON

		INSERT INTO model.DiagnosisTypes (Id, Name) Values(2, 'ICD 10');
    
		SET IDENTITY_INSERT model.DiagnosisTypes OFF
	END
END
GO
