--Migration AddColumnDiagnosisTypeIdAndConstraintsToInvoices
IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'[model].[Invoices]') AND name = 'DiagnosisTypeId')
BEGIN
ALTER TABLE [model].[Invoices] ADD DiagnosisTypeId INT NULL
END
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS  WHERE CONSTRAINT_NAME ='FK_Invoices_DiagnosisTypeId')
BEGIN 
	ALTER TABLE [model].[Invoices]
	ADD CONSTRAINT FK_Invoices_DiagnosisTypeId
	FOREIGN KEY (DiagnosisTypeId)
	REFERENCES [model].[DiagnosisTypes](Id)
END

GO

--All migrated existing invoices will be having DiagnosisTypeId as 1 i.e ICD9
IF EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'[model].[Invoices]') AND name = 'DiagnosisTypeId')
BEGIN
	UPDATE [model].[Invoices] SET DiagnosisTypeId = 1 WHERE DiagnosisTypeId IS NULL
END
GO