﻿IF EXISTS(SELECT * FROM model.Permissions WHERE Name = 'Delete Standard Notes' AND PermissionCategoryId = 12)
BEGIN

UPDATE model.Permissions 
SET Name ='Delete Standard Comments' 
WHERE Name = 'Delete Standard Notes' 
AND PermissionCategoryId = 12

END
GO
