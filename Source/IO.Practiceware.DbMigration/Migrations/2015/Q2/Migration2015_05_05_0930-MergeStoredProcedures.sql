IF OBJECT_ID('model.MergeInsurers','P') IS NOT NULL
	DROP PROCEDURE [model].[MergeInsurers]
GO

CREATE PROCEDURE [model].[MergeInsurers] (@InsurerToUseId int, @InsurerToOverwriteId int)
AS
BEGIN
SET NOCOUNT ON;
-- Logic migrated from \Legacy\AdminUtilities\ToolRoutines.cls -> ApplMergePlans method

UPDATE dbo.PatientFinancialDependents SET InsurerId=@InsurerToUseId WHERE InsurerId=@InsurerToOverwriteId
UPDATE dbo.PatientReferral SET ReferredInsurer=@InsurerToUseId WHERE ReferredInsurer=@InsurerToOverwriteId
UPDATE dbo.PatientPreCerts SET PreCertInsId=@InsurerToUseId WHERE PreCertInsId=@InsurerToOverwriteId

-- Add new unique affiliations from old plan
UPDATE s
SET s.PlanId = @InsurerToUseId
FROM dbo.PracticeAffiliations s
LEFT JOIN dbo.PracticeAffiliations d ON
	s.LocationId = d.LocationId AND
	s.ResourceId = d.ResourceId AND
	s.ResourceType = d.ResourceType AND
	s.AffiliationId != d.AffiliationId AND
	d.PlanId = @InsurerToUseId
WHERE s.PlanId = @InsurerToOverwriteId AND d.AffiliationId IS NULL

-- Delete duplicate ones
DELETE FROM dbo.PracticeAffiliations WHERE PlanId=@InsurerToOverwriteId

-- Add new unique fee schedules from old plan
UPDATE s
SET s.PlanId = @InsurerToUseId
FROM dbo.PracticeFeeSchedules s
LEFT JOIN dbo.PracticeFeeSchedules d ON
	d.CPT >= s.CPT AND
	d.LocId = s.LocId AND
	d.DoctorId = s.DoctorId AND
	d.StartDate <= s.StartDate AND
	d.EndDate >= s.EndDate AND
	d.ServiceId != s.ServiceId AND
	d.PlanId = @InsurerToUseId
WHERE s.PlanId = @InsurerToOverwriteId AND d.ServiceId IS NULL

-- Delete duplicate ones
DELETE FROM dbo.PracticeFeeSchedules WHERE PlanId=@InsurerToOverwriteId

UPDATE model.InsurancePolicies SET InsurerId=@InsurerToUseId WHERE InsurerId=@InsurerToOverwriteId
UPDATE model.Adjustments SET InsurerId=@InsurerToUseId WHERE InsurerId=@InsurerToOverwriteId
UPDATE model.FinancialBatches SET InsurerId=@InsurerToUseId WHERE InsurerId=@InsurerToOverwriteId
UPDATE model.FinancialInformations SET InsurerId=@InsurerToUseId WHERE InsurerId=@InsurerToOverwriteId

-- Archive Insurer and leave comment that it has been merged
UPDATE model.Insurers 
SET IsArchived = 1, 
	Comment = 'Merged Insurer with ' + cast(@InsurerToUseId as nvarchar(max)) + '. ' + ISNULL(Comment, '') 
WHERE Id = @InsurerToOverwriteId

END
GO

IF OBJECT_ID('model.MergeProviders','P') IS NOT NULL
	DROP PROCEDURE [model].[MergeProviders]
GO

CREATE PROCEDURE [model].[MergeProviders] (@ProviderToUseId int, @ProviderToOverwriteId int)
AS
BEGIN
SET NOCOUNT ON;
-- Logic migrated from \Legacy\AdminUtilities\ToolRoutines.cls -> ApplMergeDoctors method

UPDATE dbo.PracticeAffiliations SET ResourceId=@ProviderToUseId WHERE ResourceType = 'V' AND ResourceId=@ProviderToOverwriteId

-- Update Id in Patient Clinical
UPDATE dbo.PatientClinical 
SET FindingDetail = REPLACE(FindingDetail,CONVERT(INT, dbo.ExtractTextValue('(', FindingDetail, ')')), CONVERT(nvarchar, @ProviderToUseId))
WHERE ClinicalID IN (
	SELECT pc.ClinicalId FROM dbo.PatientClinical pc 
	INNER JOIN PracticeVendors pv on 
		pv.VendorId = CASE WHEN ISNUMERIC(dbo.ExtractTextValue('(', pc.FindingDetail, ')'))=1 
			THEN CONVERT(INT, dbo.ExtractTextValue('(', pc.FindingDetail, ')')) 
			ELSE 0 END
	WHERE pc.ClinicalType='A' 
		AND pc.ClinicalType = 'A'
		AND (SUBSTRING(pc.FindingDetail, 1, 25) = 'REFER PATIENT TO ANOTHER' OR pc.FindingDetail LIKE 'SEND A CONSULTATION LETTER%')
		AND pc.STATUS = 'A'
		AND ISNUMERIC(dbo.ExtractTextValue('(', pc.FindingDetail, ')'))=1  
		AND pv.VendorId =@ProviderToOverwriteId
)

DECLARE @ProviderToUseName nvarchar(64)
SELECT @ProviderToUseName = CONVERT(NVARCHAR(64), DisplayName)
FROM model.ExternalContacts
WHERE Id = @ProviderToUseId

-- Update Name
UPDATE dbo.PatientClinical 
SET FindingDetail = REPLACE(FindingDetail,CONVERT(NVARCHAR(500), dbo.ExtractTextValue('/', CONVERT(NVARCHAR(500), dbo.ExtractTextValue('/', FindingDetail, ')')), '(')), @ProviderToUseName)
WHERE ClinicalID IN (
	SELECT pc.ClinicalId FROM dbo.PatientClinical pc
	INNER JOIN PracticeVendors pv on 
		pv.VendorId = CASE WHEN ISNUMERIC(dbo.ExtractTextValue('(', pc.FindingDetail, ')'))=1 
			THEN CONVERT(INT, dbo.ExtractTextValue('(', pc.FindingDetail, ')')) 
			ELSE 0 END
	WHERE pc.ClinicalType='A' 
		AND pc.ClinicalType = 'A'
		AND (SUBSTRING(pc.FindingDetail, 1, 25) = 'REFER PATIENT TO ANOTHER' OR pc.FindingDetail LIKE 'SEND A CONSULTATION LETTER%')
		AND pc.STATUS = 'A'
		AND ISNUMERIC(dbo.ExtractTextValue('(', pc.FindingDetail, ')'))=1
		AND pv.VendorId = @ProviderToOverwriteId
)

UPDATE model.BillingServices SET OrderingExternalProviderId=@ProviderToUseId WHERE OrderingExternalProviderId=@ProviderToOverwriteId
UPDATE model.CommunicationTransactions SET ExternalProviderId=@ProviderToUseId WHERE ExternalProviderId=@ProviderToOverwriteId
UPDATE model.EncounterCommunicationWithOtherProviderOrders SET ReceiverExternalProviderId=@ProviderToUseId WHERE ReceiverExternalProviderId=@ProviderToOverwriteId
UPDATE model.Invoices SET ReferringExternalProviderId=@ProviderToUseId WHERE ReferringExternalProviderId=@ProviderToOverwriteId
UPDATE model.PatientExternalProviders SET ExternalProviderId=@ProviderToUseId WHERE ExternalProviderId=@ProviderToOverwriteId
UPDATE model.PatientInsuranceReferrals SET ExternalProviderId=@ProviderToUseId WHERE ExternalProviderId=@ProviderToOverwriteId

-- Drop provider
DELETE FROM model.ExternalContactPhoneNumbers WHERE ExternalContactId = @ProviderToOverwriteId
DELETE FROM model.ExternalContactAddresses WHERE ExternalContactId = @ProviderToOverwriteId
DELETE FROM model.ExternalContactEmailAddresses WHERE ExternalContactId = @ProviderToOverwriteId
DELETE FROM model.ExternalContacts WHERE Id = @ProviderToOverwriteId

END
GO