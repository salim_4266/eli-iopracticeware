IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientNotes_PatId_NoteType_AppId_idx')
DROP INDEX [PatientNotes_PatId_NoteType_AppId_idx] ON [dbo].[PatientNotes]
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PatientNotes_PatId_NoteType_AppId_ClncId_idx')
CREATE INDEX [PatientNotes_PatId_NoteType_AppId_ClncId_idx] 
ON [dbo].[PatientNotes](
	[PatientId] DESC, 
	[Notetype] DESC, 
	[AppointmentId] DESC,
	[ClinicalId] DESC)
GO