﻿-- Check if 'BillingDiagnosisIcd10' exists...
IF OBJECT_ID(N'model.BillingDiagnosisIcd10', 'U') IS NULL
BEGIN
	CREATE TABLE [model].[BillingDiagnosisIcd10](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrdinalId] [int] NULL,
	[InvoiceId] [int] NOT NULL,
	[LegacyPatientClinicalIcd10Id] [int] NULL,
 CONSTRAINT [PK_BillingDiagnosisIcd10] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [model].[BillingDiagnosisIcd10]  WITH NOCHECK ADD CONSTRAINT [FK_InvoiceBillingDiagnosisIcd10] FOREIGN KEY([InvoiceId])
REFERENCES [model].[Invoices] ([Id])

ALTER TABLE [model].[BillingDiagnosisIcd10] CHECK CONSTRAINT [FK_InvoiceBillingDiagnosisIcd10]

END