﻿-- Check if 'PatientClinicalIcd10' exists...
IF OBJECT_ID(N'dbo.PatientClinicalIcd10', 'U') IS NULL
BEGIN
	CREATE TABLE [dbo].[PatientClinicalIcd10](
	[ClinicalId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AppointmentId] [int] NULL,
	[PatientId] [int] NULL,
	[ClinicalType] [nvarchar](1) NULL,
	[EyeContext] [nvarchar](2) NULL,
	[Symptom] [nvarchar](128) NULL,
	[FindingDetailIcd10] [nvarchar](255) NULL,
	[Description] [nvarchar](512) NULL,
	[ImageDescriptor] [nvarchar](512) NULL,
	[ImageInstructions] [nvarchar](512) NULL,
	[Status] [nvarchar](1) NULL,
	[Highlights] [nvarchar](1) NULL,
	[PostOpPeriod] [int] NULL,
 CONSTRAINT [PK_PatientClinicalIcd10] PRIMARY KEY CLUSTERED 
(
	[ClinicalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[PatientClinicalIcd10] ADD  DEFAULT ('') FOR [ClinicalType]

ALTER TABLE [dbo].[PatientClinicalIcd10] ADD  DEFAULT ('') FOR [EyeContext]

ALTER TABLE [dbo].[PatientClinicalIcd10] ADD  DEFAULT ('') FOR [Symptom]

ALTER TABLE [dbo].[PatientClinicalIcd10] ADD  DEFAULT ('') FOR [FindingDetailIcd10]

ALTER TABLE [dbo].[PatientClinicalIcd10] ADD  DEFAULT ('') FOR [Description]

ALTER TABLE [dbo].[PatientClinicalIcd10] ADD  DEFAULT ('') FOR [ImageDescriptor]

ALTER TABLE [dbo].[PatientClinicalIcd10] ADD  DEFAULT ('') FOR [ImageInstructions]

ALTER TABLE [dbo].[PatientClinicalIcd10] ADD  DEFAULT ('') FOR [Status]

ALTER TABLE [dbo].[PatientClinicalIcd10] ADD  DEFAULT ('') FOR [Highlights]

ALTER TABLE [dbo].[PatientClinicalIcd10] ADD  DEFAULT ((0)) FOR [PostOpPeriod]

END