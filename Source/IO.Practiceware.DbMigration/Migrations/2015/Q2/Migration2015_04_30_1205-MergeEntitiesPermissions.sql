﻿IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Id = 67)
BEGIN 
    SET IDENTITY_INSERT model.Permissions ON

    INSERT model.Permissions(Id, Name, PermissionCategoryId)
    VALUES(67, 'Merge Patients', 6)
    
    SET IDENTITY_INSERT model.Permissions OFF

	-- Copy permissions from AccessAdminUtilities permission
	INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)
	SELECT up.IsDenied,
		   up.UserId,
		   67 AS PermissionId
	FROM model.UserPermissions up
	WHERE up.IsDenied = 0 AND up.PermissionId = 63 -- AccessAdminUtilities
END

IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Id = 68)
BEGIN 
    SET IDENTITY_INSERT model.Permissions ON

    INSERT model.Permissions(Id, Name, PermissionCategoryId)
    VALUES(68, 'Merge Providers', 11)
    
    SET IDENTITY_INSERT model.Permissions OFF

	-- Copy permissions from AccessAdminUtilities permission
	INSERT INTO model.UserPermissions (IsDenied, UserId, PermissionId)
	SELECT up.IsDenied,
		   up.UserId,
		   68 AS PermissionId
	FROM model.UserPermissions up
	WHERE up.IsDenied = 0 AND up.PermissionId = 63 -- AccessAdminUtilities
END