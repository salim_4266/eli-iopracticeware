﻿IF OBJECT_ID('model.SynchronizePatientDiagnosisDetails','P') IS NOT NULL
	DROP PROCEDURE model.SynchronizePatientDiagnosisDetails
GO

CREATE PROCEDURE [model].[SynchronizePatientDiagnosisDetails] (@PatientIds nvarchar(max), @AppointmentIds nvarchar(max))
AS
BEGIN
SET NOCOUNT ON;
-- NOTE: @PatientIds, @AppointmentIds are both optional parameter.
-- BEWARE: Without specifing the above parameters, this will attempt to synchronize all ClinicalIds 
-- in the dbo.PatientClinicalPatientDiagnosisDetailInsertQueue table.

-- Disable Parameter Sniffing
DECLARE @PatientIds_Internal TABLE (Id INT)
DECLARE @SyncAll BIT
DECLARE @Xml XML
SET @Xml = @PatientIds
SET @SyncAll = 0

-- Populate patient ids
INSERT INTO @PatientIds_Internal (Id)
SELECT Id 
FROM (SELECT n.Id.value('.', 'int') As Id
	FROM @Xml.nodes('//int') as n(Id)
) Parsed
WHERE Id NOT IN (SELECT Id FROM @PatientIds_Internal)

SET @Xml = @AppointmentIds
-- Populate patient ids based on appointment ids
INSERT INTO @PatientIds_Internal (Id)
SELECT Id 
FROM (
	SELECT DISTINCT app.PatientId As Id
	FROM (SELECT n.Id.value('.', 'int') As Id
		FROM @Xml.nodes('//int') as n(Id)
	) Parsed
	JOIN dbo.Appointments app on
		app.AppointmentId = Parsed.Id) AppointmentPatients
WHERE Id NOT IN (SELECT Id FROM @PatientIds_Internal)

-- No patients to sync -> sync all
IF NOT EXISTS (SELECT * FROM @PatientIds_Internal)
BEGIN
	SET @SyncAll = 1
END

--Reconcile the queues before continuing.
DELETE FROM model.PatientDiagnosisDetails WHERE LegacyClinicalId IN (SELECT DISTINCT ClinicalId FROM dbo.PatientClinicalPatientDiagnosisDetailDeleteQueue)
DELETE FROM dbo.PatientClinicalPatientDiagnosisDetailInsertQueue WHERE ClinicalId IN (SELECT DISTINCT ClinicalId FROM dbo.PatientClinicalPatientDiagnosisDetailDeleteQueue)
DELETE FROM PatientClinicalPatientDiagnosisDetailDeleteQueue

-- Prepare a sample PatientClinical table to be used based on the queue.
DECLARE @PatientClinicalPatientDiagnosisDetailQueue TABLE (
	[ClinicalId] [int] NOT NULL,
	[AppointmentId] [int] NULL,
	[PatientId] [int] NULL,
	[ClinicalType] [nvarchar](1) NULL,
	[EyeContext] [nvarchar](2) NULL,
	[Symptom] [nvarchar](128) NULL,
	[FindingDetail] [nvarchar](255) NULL,
	[ImageDescriptor] [nvarchar](512) NULL,
	[ImageInstructions] [nvarchar](512) NULL,
	[Status] [nvarchar](1) NULL,
	[DrawFileName] [nvarchar](64) NULL,
	[Highlights] [nvarchar](1) NULL,
	[FollowUp] [nvarchar](1) NULL,
	[PermanentCondition] [nvarchar](1) NULL,
	[PostOpPeriod] [int] NULL,
	[Surgery] [nvarchar](1) NULL,
	[Activity] [nvarchar](3) NULL
)

INSERT INTO @PatientClinicalPatientDiagnosisDetailQueue (
	[ClinicalId],
	[AppointmentId],
	[PatientId],
	[ClinicalType],
	[EyeContext],
	[Symptom],
	[FindingDetail],
	[ImageDescriptor],
	[ImageInstructions],
	[Status],
	[DrawFileName],
	[Highlights],
	[FollowUp],
	[PermanentCondition],
	[PostOpPeriod],
	[Surgery],
	[Activity])
SELECT 
pc.ClinicalId,
pc.AppointmentId,
pc.PatientId,
pc.ClinicalType,
pc.EyeContext,
pc.Symptom,
pc.FindingDetail,
pc.ImageDescriptor,
pc.ImageInstructions,
pc.[Status],
pc.DrawFileName,
pc.Highlights,
pc.FollowUp,
pc.PermanentCondition,
pc.PostOpPeriod,
pc.Surgery,
pc.Activity
FROM dbo.PatientClinical pc 
JOIN dbo.PatientClinicalPatientDiagnosisDetailInsertQueue q ON pc.ClinicalId = q.ClinicalId
WHERE pc.[Status] = 'A'

	IF EXISTS (SELECT TOP 1 * FROM @PatientClinicalPatientDiagnosisDetailQueue)
	BEGIN
		-- Make sample model.ClinicalConditons view
		SELECT 
		MAX(d.PrimaryDrillId) AS ClinicalConditionId
		,d.DiagnosisNextLevel AS LegacyDiagnosisNextLevel
		,d.MedicalSystem AS LegacyMedicalSystem
		,CASE 
			WHEN d.DiagnosisRank = 113 THEN CONVERT(BIT,1)
				ELSE CONVERT(BIT,0)
		END AS IsDeactivated
		INTO #ClinicalConditions
		FROM dm.PrimaryDiagnosisTable d
		WHERE dbo.IsNullOrEmpty(d.ReviewSystemLingo) = 0
			AND dbo.IsNullOrEmpty(MedicalSystem) = 0
		GROUP BY d.DiagnosisNextLevel
		,d.MedicalSystem
		,CASE 
			WHEN d.DiagnosisRank = 113 THEN CONVERT(BIT,1)
				ELSE CONVERT(BIT,0)
		END

		UNION ALL

		SELECT 
		model.GetPairId(1,MAX(d.PrimaryDrillId),110000000) AS ClinicalConditionId
		,d.DiagnosisNextLevel AS LegacyDiagnosisNextLevel
		,NULL AS LegacyMedicalSystem
		,CASE 
			WHEN d.DiagnosisRank = 113 THEN CONVERT(BIT,1)
				ELSE CONVERT(BIT,0)
		END AS IsDeactivated
		FROM dm.PrimaryDiagnosisTable d
		WHERE dbo.IsNullOrEmpty(d.ReviewSystemLingo) = 0
		GROUP BY d.DiagnosisNextLevel
		,CASE 
			WHEN d.DiagnosisRank = 113
				THEN CONVERT(BIT,1)
			ELSE CONVERT(BIT,0)
		END

		-- Understand what ClinicalConditions a patients has
		SELECT
		pc.PatientId
		,c.LegacyDiagnosisNextLevel
		,CASE
			WHEN pc.EyeContext = 'OD' 
				THEN 1
			WHEN pc.EyeContext = 'OS' 
				THEN 2
			ELSE 3
		END AS LateralityId
		INTO #PatientDiagnoses
		FROM dbo.PatientClinical pc
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId 
		INNER JOIN #ClinicalConditions c ON c.LegacyDiagnosisNextLevel = pc.FindingDetail
			AND c.LegacyMedicalSystem = pc.ImageDescriptor
			AND dbo.IsNullOrEmpty(c.LegacyMedicalSystem) = 0
		INNER JOIN @PatientClinicalPatientDiagnosisDetailQueue i ON i.PatientId = pc.PatientId
		WHERE pc.ClinicalType = 'Q'
			AND pc.[Status] = 'A'
			AND dbo.IsNullOrEmpty(pc.ImageDescriptor) = 0
			AND (@SyncAll = 1 OR pc.PatientId IN (SELECT Id FROM @PatientIds_Internal))
		GROUP BY pc.PatientId
		,c.LegacyDiagnosisNextLevel
		,CASE
			WHEN pc.EyeContext = 'OD' 
				THEN 1
			WHEN pc.EyeContext = 'OS' 
				THEN 2
			ELSE 3
		END
		,pc.LegacyClinicalDataSourceTypeId

		--Build view based on MIN(pc.ClinicalId) to understand the foreign key to model.PatientDiagnoses
		SELECT
		MIN(pc.ClinicalId) AS PatientDiagnosisId
		,pc.PatientId
		,c.ClinicalConditionId
		,CASE
			WHEN pc.EyeContext = 'OD' 
				THEN 1
			WHEN pc.EyeContext = 'OS' 
				THEN 2
			ELSE 3
		END AS LateralityId
		INTO #PatientDiagnosisIds
		FROM dbo.PatientClinical pc
		INNER JOIN #ClinicalConditions c ON c.LegacyDiagnosisNextLevel = pc.FindingDetail
			AND c.LegacyMedicalSystem = pc.ImageDescriptor
			AND c.LegacyMedicalSystem <> ''
			AND c.LegacyMedicalSystem IS NOT NULL
			AND c.IsDeactivated = 0
		INNER JOIN @PatientClinicalPatientDiagnosisDetailQueue i ON i.PatientId = pc.PatientId
		WHERE pc.ClinicalType = 'Q'
			AND pc.[Status] = 'A'
			AND pc.ImageDescriptor <> ''
			AND pc.ImageDescriptor IS NOT NULL
			AND (@SyncAll = 1 OR pc.PatientId IN (SELECT Id FROM @PatientIds_Internal))
		GROUP BY pc.PatientId
		,c.ClinicalConditionId
		,CASE
			WHEN pc.EyeContext = 'OD' 
				THEN 1
			WHEN pc.EyeContext = 'OS' 
				THEN 2
			ELSE 3
		END
	
		UNION ALL

		SELECT 
		MIN(pc.ClinicalId) AS PatientDiagnosisId
		,pc.PatientId
		,c.ClinicalConditionId
		,CASE
			WHEN pc.EyeContext = 'OD' 
				THEN 1
			WHEN pc.EyeContext = 'OS' 
				THEN 2
			ELSE 3
		END AS LateralityId
		FROM dbo.PatientClinical pc
		INNER JOIN #ClinicalConditions c ON c.LegacyDiagnosisNextLevel = pc.FindingDetail
			AND (c.LegacyMedicalSystem = '' OR c.LegacyMedicalSystem IS NULL)
			AND c.IsDeactivated = 0
		LEFT JOIN #PatientDiagnoses pd ON pd.PatientId = pc.PatientId
			AND pd.LateralityId = CASE
				WHEN pc.EyeContext = 'OD' 
					THEN 1
				WHEN pc.EyeContext = 'OS' 
					THEN 2
				ELSE 3
			END
			AND pd.LegacyDiagnosisNextLevel = pc.FindingDetail
		INNER JOIN @PatientClinicalPatientDiagnosisDetailQueue i ON i.PatientId = pc.PatientId
		WHERE pc.ClinicalType = 'U'
			AND pc.[Status] = 'A'
			AND pd.PatientId IS NULL
			AND (pc.ImageDescriptor = '' OR pc.ImageDescriptor IS NULL)	
			AND (@SyncAll = 1 OR pc.PatientId IN (SELECT Id FROM @PatientIds_Internal))
		GROUP BY pc.PatientId
		,c.ClinicalConditionId
		,CASE
			WHEN pc.EyeContext = 'OD' 
				THEN 1
			WHEN pc.EyeContext = 'OS' 
				THEN 2
			ELSE 3
		END

		--Understand the comments to be used for modelPatientDiagnosisDetails.Comments
		SELECT pc.ClinicalId AS PatientDiagnosisDetailsId
			,SUBSTRING(pc.Symptom, CHARINDEX('[', pc.Symptom)  +1, CHARINDEX(']', pc.Symptom)-CHARINDEX('[', pc.Symptom)-1) AS Comment
		INTO #PatientDiagnosisDetailComments
		FROM dbo.PatientClinical pc
		INNER JOIN @PatientClinicalPatientDiagnosisDetailQueue i ON i.PatientId = pc.PatientId
		LEFT JOIN (
			SELECT 
			pct.Id AS Id
			,Code AS Name
			,q.Id AS ClinicalQualifierCategoryId
			,pct.OtherLtrTrans AS SimpleDescription
			,pct.LetterTranslation AS TechnicalDescription
			,CONVERT(BIT,0) AS IsDeactivated
			,1 AS OrdinalId
			FROM PracticeCodeTable pct
			INNER JOIN (
				SELECT
				MAX(pct.Id) AS Id
				,CASE 
					WHEN SUBSTRING(ReferenceType,LEN('QUANTIFIERS')+1,LEN(ReferenceType)) = 'DESCRIPTORS'
						THEN CONVERT(BIT,0) 
					WHEN ReferenceType = 'QUANTIFIERS'
						THEN CONVERT(BIT,0) 
					ELSE CONVERT(BIT,1) 
				END AS IsSingleUse
				,ReferenceType AS Name
				,1 AS OrdinalId
				FROM PracticeCodeTable pct
				WHERE ReferenceType LIKE '%QUANTIFIERS%' AND (AlternateCode <> '' OR LetterTranslation <> '' OR OtherLtrTrans <> '')
				GROUP BY ReferenceType

				UNION ALL

				SELECT
				MAX(pct.Id) AS Id
				,CONVERT(BIT,1) AS IsSingleUse
				,SUBSTRING(ReferenceType,19,LEN(ReferenceType)) AS Name
				,1 AS OrdinalId
				FROM PracticeCodeTable pct
				WHERE ReferenceType LIKE 'INSERTEDQUANTIFIERS%' 
				GROUP BY ReferenceType
				) q ON q.Name = pct.ReferenceType
			WHERE pct.ReferenceType LIKE '%QUANTIFIERS%' AND (AlternateCode <> '' OR LetterTranslation <> '' OR OtherLtrTrans <> '')
			) q ON q.Name = SUBSTRING(pc.Symptom, CHARINDEX('[', pc.Symptom)  +1, CHARINDEX(']', pc.Symptom)-CHARINDEX('[', pc.Symptom)-1)
		WHERE pc.ClinicalType IN ('Q', 'U')
			AND pc.Symptom LIKE '%]%'
			AND q.Id IS NULL
			AND (@SyncAll = 1 OR pc.PatientId IN (SELECT Id FROM @PatientIds_Internal))
		
		-- INSERT the relative rows
		INSERT INTO model.PatientDiagnosisDetails (
				[PatientDiagnosisId]
				,[StartDateTime]
				,[EndDateTime]
				,[IsDeactivated]
				,[EnteredDateTime]
				,[UserId]
				,[Comment]
				,[RelevancyQualifierId]
				,[AccuracyQualifierId]
				,[ClinicalConditionStatusId]
				,[BodyLocationId]
				,[IsOnProblemList]
				,[EncounterId]
				,[IsEncounterDiagnosis]
				,[EncounterTreatmentGoalAndInstructionId]
				,[IsBillable]
				,[LegacyClinicalId]
		)
		SELECT 
		c3.PatientDiagnosisId
		,CASE 
			WHEN ap.AppTime >=0 
				THEN DATEADD(MI, ap.AppTime, CONVERT(DATETIME, ap.AppDate, 112)) 
			ELSE CONVERT(DATETIME, ap.AppDate, 112) 
		END AS StartDateTime
		,CONVERT(DATETIME,NULL) AS EndDateTime
		,CONVERT(BIT,0) AS IsDeactivated
		,CASE 
			WHEN ap.AppTime >=0 
				THEN DATEADD(MI, ap.AppTime, CONVERT(DATETIME, ap.AppDate, 112)) 
			ELSE CONVERT(DATETIME, ap.AppDate, 112) 
		END AS EnteredDateTime
		,NULL AS UserId
		,#PatientDiagnosisDetailComments.Comment
		,NULL AS RelevancyQualifierId
		,NULL AS AccuracyQualifierId
		,1 AS ClinicalConditionStatusId
		,NULL AS BodyLocationId
		,COALESCE((SELECT TOP 1 CONVERT(BIT,1) FROM dbo.PatientClinical p WHERE p.PatientId = pc.PatientId AND p.AppointmentId = ap.AppointmentId AND p.FindingDetail = pc.FindingDetail AND ClinicalType = 'U' AND p.EyeContext = pc.EyeContext AND p.[Status] = 'A'),CONVERT(BIT,0)) AS IsOnProblemList
		,ap.AppointmentId AS EncounterId
		,COALESCE((SELECT TOP 1 CONVERT(BIT,1) FROM dbo.PatientClinical p WHERE p.PatientId = pc.PatientId AND p.AppointmentId = ap.AppointmentId AND p.FindingDetail = pc.FindingDetail AND ClinicalType = 'U' AND p.EyeContext = pc.EyeContext AND p.[Status] = 'A'),CONVERT(BIT,0)) AS IsEncounterDiagnosis
		,NULL AS EncounterTreatmentGoalAndInstructionId
		,CONVERT(BIT,0) AS IsBillable
		,pc.ClinicalId AS LegacyClinicalId
		FROM dbo.PatientClinical pc
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId 
		INNER JOIN #ClinicalConditions c1 ON c1.LegacyDiagnosisNextLevel = pc.FindingDetail
			AND c1.LegacyMedicalSystem = pc.ImageDescriptor
			AND c1.LegacyMedicalSystem <> ''
			AND c1.LegacyMedicalSystem IS NOT NULL
			AND c1.IsDeactivated = 0
		INNER JOIN #PatientDiagnosisIds c3 ON c3.ClinicalConditionId = c1.ClinicalConditionId
			AND c3.PatientId = pc.PatientId
			AND CASE
				WHEN pc.EyeContext = 'OD' 
					THEN 1
				WHEN pc.EyeContext = 'OS' 
					THEN 2
				ELSE 3
			END = c3.LateralityId
		LEFT JOIN #PatientDiagnosisDetailComments ON #PatientDiagnosisDetailComments.PatientDiagnosisDetailsId = pc.ClinicalId
		INNER JOIN @PatientClinicalPatientDiagnosisDetailQueue i ON i.PatientId = pc.PatientId
		WHERE pc.ClinicalType = 'Q'
			AND pc.[Status] = 'A'
			AND pc.ImageDescriptor <> ''
			AND pc.ImageDescriptor IS NOT NULL
			AND (@SyncAll = 1 OR pc.PatientId IN (SELECT Id FROM @PatientIds_Internal))
		GROUP BY pc.ClinicalId
		,c3.PatientDiagnosisId
		,ap.AppDate
		,ap.AppTime
		,#PatientDiagnosisDetailComments.Comment
		,ap.AppointmentId
		,pc.PatientId
		,pc.FindingDetail
		,pc.EyeContext

		UNION ALL

		SELECT 
		c3.PatientDiagnosisId
		,CASE 
			WHEN ap.AppTime >=0 
				THEN DATEADD(MI, ap.AppTime, CONVERT(DATETIME, ap.AppDate, 112)) 
			ELSE CONVERT(DATETIME, ap.AppDate, 112) 
		END AS StartDateTime
		,CONVERT(DATETIME,NULL) AS EndDateTime
		,CONVERT(BIT,0) AS IsDeactivated
		,CASE 
			WHEN ap.AppTime >=0 
				THEN DATEADD(MI, ap.AppTime, CONVERT(DATETIME, ap.AppDate, 112)) 
			ELSE CONVERT(DATETIME, ap.AppDate, 112) 
		END AS EnteredDateTime
		,NULL AS UserId
		,#PatientDiagnosisDetailComments.Comment
		,NULL AS RelevancyQualifierId
		,NULL AS AccuracyQualifierId
		,1 AS ClinicalConditionStatusId
		,NULL AS BodyLocationId
		,CONVERT(BIT,1) AS IsOnProblemList
		,ap.AppointmentId AS EncounterId
		,CONVERT(BIT,1) AS IsEncounterDiagnosis
		,NULL AS EncounterTreatmentGoalAndInstructionId
		,CONVERT(BIT,0) AS IsBillable
		,pc.ClinicalId AS LegacyClinicalId
		FROM dbo.PatientClinical pc
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId 
		INNER JOIN #ClinicalConditions c1 ON c1.LegacyDiagnosisNextLevel = pc.FindingDetail
			AND (c1.LegacyMedicalSystem = '' OR c1.LegacyMedicalSystem IS NULL)
			AND c1.IsDeactivated = 0
		INNER JOIN #PatientDiagnosisIds c3 ON c3.ClinicalConditionId = c1.ClinicalConditionId
			AND c3.PatientId = pc.PatientId
			AND c3.LateralityId = CASE
				WHEN pc.EyeContext = 'OD' 
					THEN 1
				WHEN pc.EyeContext = 'OS' 
					THEN 2
				ELSE 3
			END
		LEFT JOIN #PatientDiagnoses pd ON pd.PatientId = pc.PatientId
			AND pd.LateralityId = CASE
				WHEN pc.EyeContext = 'OD' 
					THEN 1
				WHEN pc.EyeContext = 'OS' 
					THEN 2
				ELSE 3
			END
			AND pd.LegacyDiagnosisNextLevel = pc.FindingDetail
		LEFT JOIN #PatientDiagnosisDetailComments ON #PatientDiagnosisDetailComments.PatientDiagnosisDetailsId = pc.ClinicalId
		INNER JOIN @PatientClinicalPatientDiagnosisDetailQueue i ON i.PatientId = pc.PatientId
		WHERE pc.ClinicalType = 'U'
			AND pc.[Status] = 'A'
			AND (pc.ImageDescriptor = '' OR pc.ImageDescriptor IS NULL)
			AND pd.PatientId IS NULL
			AND (@SyncAll = 1 OR pc.PatientId IN (SELECT Id FROM @PatientIds_Internal))
		GROUP BY pc.ClinicalId
		,c3.PatientDiagnosisId
		,ap.AppDate
		,ap.AppTime
		,#PatientDiagnosisDetailComments.Comment
		,ap.AppointmentId
		,pc.PatientId
		,pc.FindingDetail
		,pc.EyeContext

		EXCEPT

		SELECT 
		[PatientDiagnosisId]
		,[StartDateTime]
		,[EndDateTime]
		,[IsDeactivated]
		,[EnteredDateTime]
		,[UserId]
		,[Comment]
		,[RelevancyQualifierId]
		,[AccuracyQualifierId]
		,[ClinicalConditionStatusId]
		,[BodyLocationId]
		,[IsOnProblemList]
		,ap.[EncounterId]
		,[IsEncounterDiagnosis]
		,[EncounterTreatmentGoalAndInstructionId]
		,[IsBillable]
		,[LegacyClinicalId]
		FROM model.PatientDiagnosisDetails pdd
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pdd.EncounterId
		INNER JOIN @PatientClinicalPatientDiagnosisDetailQueue i ON i.PatientId = ap.PatientId
		WHERE (@SyncAll = 1 OR ap.PatientId IN (SELECT Id FROM @PatientIds_Internal))
		
		-- Clean up the insert queue based on what was inserted.
		DELETE FROM dbo.PatientClinicalPatientDiagnosisDetailInsertQueue WHERE ClinicalId IN (SELECT ClinicalId FROM @PatientClinicalPatientDiagnosisDetailQueue)

		DELETE FROM dbo.PatientClinicalPatientDiagnosisDetailInsertQueue WHERE ClinicalId IN (
			SELECT a.ClinicalId
			FROM dbo.PatientClinicalPatientDiagnosisDetailInsertQueue a
			JOIN dbo.PatientClinical b ON b.ClinicalId = a.ClinicalId
			JOIN dbo.Appointments ap ON ap.AppointmentId = b.AppointmentId
			WHERE b.[Status] <> 'A'
				AND (@SyncAll = 1 OR ap.PatientId IN (SELECT Id FROM @PatientIds_Internal))
		)

		DELETE FROM dbo.PatientClinicalPatientDiagnosisDetailDeleteQueue WHERE ClinicalId IN (
			SELECT d.ClinicalId
			FROM dbo.PatientClinicalPatientDiagnosisDetailDeleteQueue d
			JOIN dbo.PatientClinical b ON b.ClinicalId = d.ClinicalId
			JOIN dbo.Appointments ap ON ap.AppointmentId = b.AppointmentId
			WHERE b.[Status] <> 'A'
				AND (@SyncAll = 1 OR ap.PatientId IN (SELECT Id FROM @PatientIds_Internal))
		)
	END
END
GO