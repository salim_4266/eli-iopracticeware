IF OBJECT_ID ( 'model.MergeProviders', 'P' ) IS NOT NULL 
	DROP PROCEDURE [model].[MergeProviders]
GO


CREATE PROCEDURE [model].[MergeProviders] (@ProviderToUseId int, @ProviderToOverwriteId int)
AS
BEGIN
SET NOCOUNT ON;
-- Logic migrated from \Legacy\AdminUtilities\ToolRoutines.cls -> ApplMergeDoctors method

UPDATE dbo.PracticeAffiliations SET ResourceId=@ProviderToUseId WHERE ResourceType = 'V' AND ResourceId=@ProviderToOverwriteId

-- Update Id in Patient Clinical
UPDATE dbo.PatientClinical 
SET FindingDetail = REPLACE(FindingDetail,CONVERT(INT, dbo.ExtractTextValue('(', FindingDetail, ')')), CONVERT(nvarchar, @ProviderToUseId))
WHERE ClinicalID IN (
	SELECT pc.ClinicalId FROM dbo.PatientClinical pc 
	INNER JOIN PracticeVendors pv on 
		pv.VendorId = CASE WHEN ISNUMERIC(dbo.ExtractTextValue('(', pc.FindingDetail, ')'))=1 
			THEN CONVERT(INT, dbo.ExtractTextValue('(', pc.FindingDetail, ')')) 
			ELSE 0 END
	WHERE pc.ClinicalType='A' 
		AND pc.ClinicalType = 'A'
		AND (SUBSTRING(pc.FindingDetail, 1, 25) = 'REFER PATIENT TO ANOTHER' OR pc.FindingDetail LIKE 'SEND A CONSULTATION LETTER%')
		AND pc.STATUS = 'A'
		AND ISNUMERIC(dbo.ExtractTextValue('(', pc.FindingDetail, ')'))=1  
		AND pv.VendorId =@ProviderToOverwriteId
)

DECLARE @ProviderToUseName nvarchar(64)
SELECT @ProviderToUseName = CONVERT(NVARCHAR(64), DisplayName)
FROM model.ExternalContacts
WHERE Id = @ProviderToUseId

-- Update Name
UPDATE dbo.PatientClinical 
SET FindingDetail = REPLACE(FindingDetail,CONVERT(NVARCHAR(500), dbo.ExtractTextValue('/', CONVERT(NVARCHAR(500), dbo.ExtractTextValue('/', FindingDetail, ')')), '(')), @ProviderToUseName)
WHERE ClinicalID IN (
	SELECT pc.ClinicalId FROM dbo.PatientClinical pc
	INNER JOIN PracticeVendors pv on 
		pv.VendorId = CASE WHEN ISNUMERIC(dbo.ExtractTextValue('(', pc.FindingDetail, ')'))=1 
			THEN CONVERT(INT, dbo.ExtractTextValue('(', pc.FindingDetail, ')')) 
			ELSE 0 END
	WHERE pc.ClinicalType='A' 
		AND pc.ClinicalType = 'A'
		AND (SUBSTRING(pc.FindingDetail, 1, 25) = 'REFER PATIENT TO ANOTHER' OR pc.FindingDetail LIKE 'SEND A CONSULTATION LETTER%')
		AND pc.STATUS = 'A'
		AND ISNUMERIC(dbo.ExtractTextValue('(', pc.FindingDetail, ')'))=1
		AND pv.VendorId = @ProviderToOverwriteId
)

UPDATE model.BillingServices SET OrderingExternalProviderId=@ProviderToUseId WHERE OrderingExternalProviderId=@ProviderToOverwriteId
UPDATE model.CommunicationTransactions SET ExternalProviderId=@ProviderToUseId WHERE ExternalProviderId=@ProviderToOverwriteId
UPDATE model.EncounterCommunicationWithOtherProviderOrders SET ReceiverExternalProviderId=@ProviderToUseId WHERE ReceiverExternalProviderId=@ProviderToOverwriteId
UPDATE model.Invoices SET ReferringExternalProviderId=@ProviderToUseId WHERE ReferringExternalProviderId=@ProviderToOverwriteId
UPDATE model.PatientExternalProviders SET ExternalProviderId=@ProviderToUseId WHERE ExternalProviderId=@ProviderToOverwriteId
UPDATE model.PatientInsuranceReferrals SET ExternalProviderId=@ProviderToUseId WHERE ExternalProviderId=@ProviderToOverwriteId

-- Make provider archived
UPDATE model.ExternalContacts SET IsArchived = 1 WHERE Id=@ProviderToOverwriteId

END