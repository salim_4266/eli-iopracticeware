IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'[model].[BillingDiagnosisIcd10]') AND name = 'DiagnosisDescription')
BEGIN
ALTER TABLE [model].[BillingDiagnosisIcd10] ADD DiagnosisDescription VARCHAR(Max) NULL
END
GO

IF EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'[model].[BillingDiagnosisIcd10]') AND name = 'DiagnosisDescription')
BEGIN
  UPDATE icd10 SET icd10.DiagnosisDescription = pc.[Description]
	FROM model.BillingDiagnosisIcd10 icd10 
	INNER JOIN patientclinicalicd10 pc
	ON icd10.LegacyPatientClinicalIcd10Id = pc.ClinicalId
	AND pc.ClinicalType in ('B','K')
END
GO