IF NOT EXISTS (SELECT schema_name 
    FROM information_schema.schemata 
    WHERE schema_name = 'icd10' )
BEGIN
    EXEC sp_executesql N'CREATE SCHEMA icd10;';
END
GO

IF NOT EXISTS (SELECT * FROM sys.Tables WHERE name = 'ICD10')
BEGIN 
   CREATE TABLE icd10.ICD10
   (
      Id INT IDENTITY PRIMARY KEY,
	  DiagnosisCode NVARCHAR(50),
	  DiagnosisDescription NVARCHAR(MAX)
   )
END