﻿IF EXISTS (SELECT * FROM sys.triggers WHERE Name = 'OnPatientReceivableInsert') 
	DROP TRIGGER [dbo].[OnPatientReceivableInsert];
GO

CREATE TRIGGER [dbo].[OnPatientReceivableInsert] ON [dbo].[PatientReceivables]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @QualifyInsertedInvoiceReceivableAsTypeId INT

	SELECT TOP 1 @QualifyInsertedInvoiceReceivableAsTypeId =
	CASE i.InsurerId
		WHEN 0
			THEN 1 --Patient InvoiceReceivable
		WHEN 99999
			THEN 2 --Credit
		ELSE 3 --Patient + Insurance InvoiceReceivable
	END
	FROM inserted i
	
	DECLARE @DefaultServiceLocationCodeForVisionType INT
	SELECT TOP 1 @DefaultServiceLocationCodeForVisionType = Id FROM model.ServiceLocationCodes WHERE LOWER(Name) ='home'

	DECLARE @NewAppointmentId INT
	SELECT @NewAppointmentId= i.AppointmentId FROM INSERTED i

	DECLARE @ResourceType NVARCHAR(1)
	SELECT TOP 1 @ResourceType = re.ResourceType FROM Appointments as ap JOIN Resources  as re ON ap.ResourceId1 = re.ResourceId WHERE ap.AppointmentId = @NewAppointmentId
	
	IF(@DefaultServiceLocationCodeForVisionType = 0)
		SET @DefaultServiceLocationCodeForVisionType = NULL

	DECLARE @ClinicalInvoiceProviderMax INT
	DECLARE @ProviderBillingOrganizationMax INT
	DECLARE @ProviderBillingOrganizationServiceLocationMax INT
	DECLARE @MainLocationId INT
	DECLARE @ServiceLocationCodeId INT
	DECLARE @InvoiceId INT
	DECLARE @ServiceLocationMax INT
	DECLARE @PatientId INT
	DECLARE @NumberOfInsuranceRowsInserted  INT
	DECLARE @InvoiceCreatedOnThisInsertTrigger INT

	IF (@QualifyInsertedInvoiceReceivableAsTypeId = 2)
	BEGIN
		IF NOT EXISTS (
		SELECT i.* FROM inserted i 
		INNER JOIN model.Adjustments adj ON adj.PatientId = i.PatientId 
		AND adj.InvoiceReceivableId IS NULL)
			BEGIN
				INSERT INTO model.Adjustments (FinancialSourceTypeId,Amount,PostedDateTime,AdjustmentTypeId
				  ,BillingServiceId,InvoiceReceivableId,PatientId,InsurerId,FinancialBatchId
				  ,ClaimAdjustmentGroupCodeId,ClaimAdjustmentReasonCodeId,Comment,IncludeCommentOnStatement)
				  SELECT
					CASE
						WHEN i.PatientId IS NOT NULL
							THEN 2
						WHEN i.InsurerId IS NOT NULL
							THEN 1
						ELSE 4
					END AS FinancialSourceTypeId
					,-0.01 AS Amount
					,CONVERT(NVARCHAR,model.GetClientDateNow(),112) AS PostedDateTime
					,1 AS AdjustmentTypeId
					,NULL AS BillingServiceId
					,NULL AS InvoiceReceivableId
					,i.PatientId 
					,NULL AS InsurerId
					,NULL AS FinancialBatchId
					,NULL AS ClaimAdjustmentReasonCodeId
					,NULL AS ClaimAdjustmentGroupCodeId
					,NULL AS PaymentRefId
					,0 AS IncludeCommentOnStatement
					FROM inserted i 

					DECLARE @InsertedCreditRowScopeIdentity INT
					IF (@@ROWCOUNT > 0) SET @InsertedCreditRowScopeIdentity = SCOPE_IDENTITY()
			END
		ELSE IF EXISTS (
			SELECT 
			i.* 
			FROM inserted i 
			INNER JOIN model.Adjustments adj ON adj.PatientId = i.PatientId 
			AND adj.InvoiceReceivableId IS NULL)
			BEGIN
				UPDATE adj
				SET adj.Amount = ABS(i.OpenBalance)
				FROM model.Adjustments adj
				INNER JOIN inserted i ON i.Disability = adj.Id
			END
	END
	ELSE
	BEGIN

	SET @InvoiceCreatedOnThisInsertTrigger = 1
	SET @PatientId = (SELECT TOP 1 PatientId FROM inserted)
	SET @ClinicalInvoiceProviderMax = 110000000
	SET @ProviderBillingOrganizationMax = 110000000
	SET @ProviderBillingOrganizationServiceLocationMax = 110000000
	SET @ServiceLocationMax = 110000000
	SET @NumberOfInsuranceRowsInserted = 0
	SET @MainLocationId = (
			SELECT TOP 1 PracticeId AS PracticeId
			FROM dbo.PracticeName
			WHERE PracticeType = 'P' AND LocationReference = ''
			)
	SET @ServiceLocationCodeId = (
			SELECT TOP 1 CASE 
					WHEN (@DefaultServiceLocationCodeForVisionType IS NOT NULL AND @ResourceType IN ('Q','Y'))
						THEN
						(
						SELECT @DefaultServiceLocationCodeForVisionType
						)
					WHEN (ap.ResourceId2 = 0) OR (ap.ResourceId2 > 1000) AND (ap.Comments <> 'ASC CLAIM')
						THEN (
							SELECT TOP 1 Id FROM model.ServiceLocationCodes WHERE PlaceOfServiceCode = '11'
							)
					WHEN ap.ResourceId2 < 999
						THEN (
							SELECT TOP 1 slc.Id
							FROM inserted i
							INNER JOIN dbo.Appointments ap ON i.AppointmentId = ap.AppointmentId 
							INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId2
							INNER JOIN model.ServiceLocationCodes slc ON slc.PlaceOfServiceCode = re.PlaceOfService
							)
					WHEN ap.Comments = 'ASC CLAIM'
						THEN (
							SELECT TOP 1 slc.Id
							FROM inserted i
							INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
							INNER JOIN dbo.Appointments ap ON i.AppointmentId = ap.AppointmentId
							INNER JOIN dbo.Resources reASC ON reASC.PracticeId = ap.ResourceId2 - 1000 
								AND reASC.ResourceType = 'R' 
								AND reASC.ServiceCode = '02' 
								AND reASC.Billable = 'Y' 
								AND pic.FieldValue = 'T'
							LEFT JOIN model.ServiceLocationCodes slc ON slc.PlaceOfServiceCode = reASC.PlaceOfService 
							)
					END
			FROM inserted i 
			INNER JOIN Appointments ap ON i.AppointmentId = ap.AppointmentId
			)

	IF OBJECT_ID(N'tempdb..#HoldOrdinals') IS NOT NULL
		DROP TABLE #HoldOrdinals

	CREATE TABLE #HoldOrdinals (PatientInsuranceId INT)

--step 1, does the invoice exsits for the PR you're trying to insert?
--no -- then create it, yes -- then set variable @InvoiceId = to the invoiceid
	IF EXISTS (
			SELECT Id
			FROM model.Invoices inv
			INNER JOIN inserted i ON inv.EncounterId = i.AppointmentId

			UNION ALL 
			
			SELECT inv.Id
			FROM inserted i 
			INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
				AND ap.Comments = 'ASC CLAIM'
			INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
				AND apEncounter.AppTypeId = ap.AppTypeId
				AND apEncounter.AppDate = ap.AppDate
				AND apEncounter.AppTime > 0 
				AND ap.AppTime = 0
				AND apEncounter.ScheduleStatus = ap.ScheduleStatus
				AND ap.ScheduleStatus = 'D'
				AND apEncounter.ResourceId1 = ap.ResourceId1
				AND apEncounter.ResourceId2 = ap.ResourceId2
			INNER JOIN model.Invoices inv on inv.EncounterId = apEncounter.AppointmentId AND inv.InvoiceTypeId = 2
			)
			BEGIN
				SET @InvoiceCreatedOnThisInsertTrigger = 0
				SET @InvoiceId = (
				SELECT Id
				FROM model.Invoices inv
				INNER JOIN inserted i ON inv.EncounterId = i.AppointmentId

				UNION ALL 
			
				SELECT inv.Id
				FROM inserted i 
				INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
					AND ap.Comments = 'ASC CLAIM'
				INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
					AND apEncounter.AppTypeId = ap.AppTypeId
					AND apEncounter.AppDate = ap.AppDate
					AND apEncounter.AppTime > 0 
					AND ap.AppTime = 0
					AND apEncounter.ScheduleStatus = ap.ScheduleStatus
					AND ap.ScheduleStatus = 'D'
					AND apEncounter.ResourceId1 = ap.ResourceId1
					AND apEncounter.ResourceId2 = ap.ResourceId2
				INNER JOIN model.Invoices inv on inv.EncounterId = apEncounter.AppointmentId AND inv.InvoiceTypeId = 2
				)
			END
	ELSE
	BEGIN
	--set this variable to 1, or True so that we know the invoice was inserted on this instance of the insert trigger.
		SET @InvoiceCreatedOnThisInsertTrigger = 1
		--First insert into invoices   
		DECLARE @BillToDr INT

		SET @BillToDr = (
				SELECT TOP 1 BillToDr
				FROM inserted
				)

		DECLARE @PrimaryInsurerId INT

		SET @PrimaryInsurerId = (
				SELECT TOP 1 InsurerId
				FROM inserted
				)

		DECLARE @IsAssignmentRefused BIT

		SET @IsAssignmentRefused = CONVERT(BIT, 0) --assume assignment is always accepted.

		IF EXISTS (
				SELECT d.DoctorId
				FROM model.DoctorInsurerAssignments d
				WHERE d.DoctorId = @BillToDr
					AND d.InsurerId = @PrimaryInsurerId
					AND d.IsAssignmentAccepted = 0 --indicates Assignment is not accepted, therefore @IsAssignmentRefused = 1
				)
			SET @IsAssignmentRefused = CONVERT(BIT, 1)

		DECLARE @ServiceLocationId INT
		SET @ServiceLocationId = (
			SELECT TOP 1
			CASE 
			WHEN i.BillingOffice = 0
				THEN (SELECT TOP 1 PracticeId FROM PracticeName WHERE LocationReference = '' AND PracticeType = 'P')
			WHEN i.BillingOffice BETWEEN 1
					AND 999
				THEN model.GetPairId(1, reAtribTo.ResourceId, @ServiceLocationMax)
			ELSE i.BillingOffice - 1000
			END
			FROM inserted i
			LEFT JOIN dbo.Resources reAtribTo ON i.BillingOffice = reAtribTo.ResourceId
		)

		DECLARE @BillingProviderId INT
		IF EXISTS (SELECT TOP 1 * FROM inserted i JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId WHERE ap.Comments <> 'ASC CLAIM')
		BEGIN 
			SET @BillingProviderId = (
				SELECT DISTINCT TOP 1 p.Id
				FROM inserted i
				INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
				INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
					AND re.ResourceType IN ('D', 'Q', 'Z','Y')
				INNER JOIN dbo.PracticeName AS pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
					AND pnBillOrg.PracticeType = 'P'
				LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
					AND pa.ResourceType = 'R'
				INNER JOIN model.Providers p ON re.ResourceId = p.UserId
					AND p.BillingOrganizationId = CASE WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'
						THEN (110000000 + re.ResourceId) 
						ELSE pnBillOrg.PracticeId
					END
					AND p.IsBillable = CASE WHEN re.Billable = 'Y' THEN CONVERT(BIT,1) ELSE CONVERT(BIT, 0) END
					AND p.IsEmr = CASE WHEN re.GoDirect = 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END
			)
		END
		ELSE
		BEGIN
			SET @BillingProviderId = (
				SELECT DISTINCT TOP 1 p.Id
				FROM dbo.Appointments ap 
				LEFT JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
				LEFT JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId 
					AND apEncounter.AppTypeId = ap.AppTypeId 
					AND apEncounter.AppDate = ap.AppDate 
					AND apEncounter.AppTime > 0 
					AND ap.AppTime = 0 
					AND apEncounter.ScheduleStatus = ap.ScheduleStatus 
					AND ap.ScheduleStatus = 'D' 
					AND apEncounter.ResourceId1 = ap.ResourceId1 
					AND apEncounter.ResourceId2 = ap.ResourceId2
				LEFT JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
				LEFT JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P'
					AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
					AND PracticeType = 'P'
					AND LocationReference <> ''
				LEFT JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
					AND reASC.ResourceType = 'R'
					AND reASC.ServiceCode = '02'
					AND reASC.Billable = 'Y'
					AND pic.FieldValue = 'T'
				JOIN model.BillingOrganizations bo ON bo.Id = pnServLoc.PracticeId
				JOIN model.Providers p ON p.BillingOrganizationId = bo.Id
					AND p.UserId IS NULL
					AND p.ServiceLocationId = pnServLoc.PracticeId
				JOIN inserted i ON i.AppointmentId = ap.AppointmentId
			)
		END

		DECLARE @DiagnosisTypeId INT
		SELECT TOP 1 @DiagnosisTypeId = Id FROM model.DiagnosisTypes WHERE Name = 'ICD 9'
		
		IF((SELECT COUNT(*) FROM dbo.PatientClinicalIcd10 WHERE AppointmentID = @NewAppointmentId AND PatientId = @PatientId) > 0)
		BEGIN
			SELECT TOP 1 @DiagnosisTypeId = Id FROM model.DiagnosisTypes WHERE Name = 'ICD 10'
		END

		IF @BillingProviderId IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(MAX)
			SET @errorMessage = 'Unable to set BillingProviderId, INSERT, PatientId: ' + CONVERT(NVARCHAR(MAX),(SELECT TOP 1 PatientId FROM inserted)) + ', LastName: '+ (SELECT LastName FROM model.Patients WHERE Id = (SELECT TOP 1 PatientId FROM inserted))
			RAISERROR(@errorMessage,16,1)
		END

		INSERT INTO model.Invoices (
			LegacyDateTime
			,AttributeToServiceLocationId
			,EncounterId
			,InvoiceTypeId
			,ClinicalInvoiceProviderId
			,HasPatientAssignedBenefits
			,IsReleaseOfInformationNotSigned
			,IsNoProviderSignatureOnFile
			,BillingProviderId
			,ReferringExternalProviderId
			,ServiceLocationCodeId
			,IsAssignmentRefused
			,DiagnosisTypeId
			)
		SELECT CONVERT(DATETIME, i.InvoiceDate, 112) AS [DateTime]
			,@ServiceLocationId AS AttributeToServiceLocationId
			,CASE 
				WHEN ap.Comments <> 'ASC CLAIM'
					THEN i.AppointmentId
				ELSE apEncounter.AppointmentId
				END AS EncounterId
			,CASE 
				WHEN re.ResourceType IN (
						'Q'
						,'Y'
						)
					THEN 3
				WHEN ap.Comments = 'ASC CLAIM'
					THEN 2
				ELSE 1
				END AS InvoiceTypeId
			,CASE 
				WHEN ap.Comments <> 'ASC CLAIM'
					THEN model.GetBigPairId(ap.ResourceId1, i.AppointmentId, @ClinicalInvoiceProviderMax)
				ELSE model.GetBigPairId(reASC.ResourceId, apEncounter.AppointmentId, @ClinicalInvoiceProviderMax)
				END AS ClinicalInvoiceProviderId
			,CASE p.PaymentOfBenefitsToProviderId
				WHEN 1
					THEN CONVERT(BIT, 1)
				ELSE CONVERT(BIT, 0)
				END AS HasPatientAssignedBenefits
			,CASE p.ReleaseOfInformationCodeId
				WHEN 1
					THEN CONVERT(BIT, 0)
				ELSE CONVERT(BIT, 1)
				END AS IsReleaseOfInformationNotSigned
			,CONVERT(BIT, 0) AS IsNoProviderSignatureOnFile
			,@BillingProviderId AS BillingProviderId
			,CASE 
				WHEN i.ReferDr = ''
					THEN NULL
				ELSE i.ReferDr
				END AS ReferringExternalProviderId
			,@ServiceLocationCodeId AS ServiceLocationCodeId
			,@IsAssignmentRefused AS IsAssignmentRefused
			,@DiagnosisTypeId AS DiagnosisTypeId
		FROM inserted i
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
		INNER JOIN model.Patients p ON p.Id = i.PatientId
		LEFT JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
		LEFT JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
		LEFT JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
			AND apEncounter.AppTypeId = ap.AppTypeId
			AND apEncounter.AppDate = ap.AppDate
			AND apEncounter.AppTime > 0
			AND ap.AppTime = 0
			AND apEncounter.ScheduleStatus = ap.ScheduleStatus
			AND ap.ScheduleStatus = 'D'
			AND apEncounter.ResourceId1 = ap.ResourceId1
			AND apEncounter.ResourceId2 = ap.ResourceId2
		LEFT JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P'
			AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
			AND PracticeType = 'P'
			AND LocationReference <> ''
		LEFT JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
			AND reASC.ResourceType = 'R'
			AND reASC.ServiceCode = '02'
			AND reASC.Billable = 'Y'
			AND pic.FieldValue = 'T'

		--Set the invoice variable to the new row that was just inserted
		SET @InvoiceId = (
				SELECT IDENT_CURRENT('model.Invoices')
				)
	END
	
	IF (@InvoiceCreatedOnThisInsertTrigger = 1 AND @PrimaryInsurerId <> 0)
	BEGIN
		DECLARE @DetermineInvoiceApptInsType NVARCHAR(1)
		SET @DetermineInvoiceApptInsType = (
		SELECT 
			CASE v.ApptInsType
				WHEN 1
					THEN 'M'
				WHEN 2
					THEN 'V'
				WHEN 3
					THEN 'A'
				WHEN 4
					THEN 'W'
			END AS ApptInsType
		FROM (
			SELECT TOP 1 
			CASE ap.ApptInsType
			WHEN 'M'
				THEN 1
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3
			WHEN 'W'
				THEN 4
			END AS ApptInsType
			FROM dbo.Appointments ap
			WHERE AppointmentId = 
			(SELECT TOP 1 EncounterId FROM model.Invoices WHERE Id = @InvoiceId)
			ORDER BY CASE ApptInsType
			WHEN 'M'
				THEN 1
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3
			WHEN 'W'
				THEN 4
			END ASC
			)v
		)

		DECLARE @DetermineInsertedPatientInsuranceInsType NVARCHAR(1)
		SET @DetermineInsertedPatientInsuranceInsType = (
		SELECT 
		InsuranceType 
		FROM (
			SELECT 
			DISTINCT TOP 1
			pi.EndDateTime
			,CASE pi.InsuranceTypeId
				WHEN 1
					THEN 'M'
				WHEN 2
					THEN 'V'
				WHEN 3
					THEN 'A'
				WHEN 4
					THEN 'W'
				ELSE NULL
			END
			AS InsuranceType
			FROM inserted i
			INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
				AND (pi.EndDateTime IS NULL OR pi.EndDateTime = '' OR pi.EndDateTime >= CONVERT(DATETIME,i.InvoiceDate,112))
				AND pi.IsDeleted = 0
			INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
				AND ip.StartDateTime <= CONVERT(DATETIME,i.InvoiceDate,112)
				AND ip.InsurerId = i.InsurerId
				AND ip.PolicyHolderPatientId = i.InsuredId
			ORDER BY pi.EndDateTime ASC
		)v
		)
		
		IF (@DetermineInsertedPatientInsuranceInsType <> @DetermineInvoiceApptInsType)
		BEGIN
			UPDATE ap
			SET ap.ApptInsType = @DetermineInsertedPatientInsuranceInsType
			FROM dbo.Appointments ap
			WHERE ap.AppointmentId = (
				SELECT 
				ap.EncounterId
				FROM inserted i 
				INNER JOIN dbo.Appointments ap 
				ON ap.AppointmentId = i.AppointmentId)
		END
	END

		--Insert InvoiceSupplimentals table if neccessary
		IF EXISTS (
				SELECT *
				FROM inserted i
				WHERE FirstConsDate <> '' OR HspAdmDate <> '' OR Over90 <> '' OR Over90 <> '' OR RsnDate <> '' OR HspDisDate <> '' OR AccType <> ''
				)
				AND NOT EXISTS(SELECT * FROM model.InvoiceSupplementals WHERE Id = @InvoiceId)
		BEGIN
			INSERT INTO model.InvoiceSupplementals (
				Id
				,AccidentDateTime
				,AccidentStateOrProvinceId
				,AdmissionDateTime
				,ClaimDelayReasonId
				,DisabilityDateTime
				,DischargeDateTime
				,RelatedCause1Id
				,VisionPrescriptionDateTime
				)
			SELECT @invoiceId AS Id
				,CASE 
					WHEN FirstConsDate = ''
						THEN NULL
					ELSE CONVERT(DATETIME, FirstConsDate, 101)
					END AS AccidentDateTime
				,CASE st.id
					WHEN 0
						THEN NULL
					ELSE st.id
					END AS AccidentStateOrProvinceId
				,CASE 
					WHEN HspAdmDate = ''
						THEN NULL
					ELSE CONVERT(DATETIME, HspAdmDate, 101)
					END AS AdmissionDateTime
				,CASE Over90
					WHEN 1
						THEN 1
					WHEN 2
						THEN 2
					WHEN 3
						THEN 3
					WHEN 4
						THEN 4
					WHEN 5
						THEN 5
					WHEN 6
						THEN 6
					WHEN 7
						THEN 7
					WHEN 8
						THEN 8
					WHEN 9
						THEN 9
					WHEN 10
						THEN 10
					WHEN 11
						THEN 11
					WHEN 15
						THEN 12
					ELSE NULL
					END AS ClaimDelayReasonId
				,CASE 
					WHEN COALESCE(RsnDate, '') <> ''
						THEN CONVERT(DATETIME, RsnDate, 101)
					ELSE NULL
					END AS DisabilityDateTime
				,CASE 
					WHEN COALESCE(HspDisDate, '') <> ''
						THEN CONVERT(DATETIME, HspDisDate, 101)
					ELSE NULL
					END AS DischargeDateTime
				,CASE i.AccType
					WHEN 'A'
						THEN 1
					WHEN 'E'
						THEN 2
					WHEN 'O'
						THEN 3
					ELSE NULL
					END AS RelatedCause1Id
				,CONVERT(DATETIME, NULL, 101) AS VisionPrescriptionDateTime
			FROM inserted i
			LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = i.AccState
	END

	--now let's insert into the invoice receivables (the actual PR row(s))
	IF NOT EXISTS (
			SELECT Id
			FROM model.InvoiceReceivables ir
			WHERE ir.PatientInsuranceId IS NULL 
			AND ir.InvoiceId = @InvoiceId
			)
	BEGIN
			IF EXISTS (SELECT i.InsurerId FROM inserted i WHERE i.InsurerId = 0 AND i.ReceivableType = 'I')
			BEGIN
				INSERT INTO model.InvoiceReceivables (
				InvoiceId
				,PatientInsuranceId
				,PayerClaimControlNumber
				,PatientInsuranceAuthorizationId
				,PatientInsuranceReferralId
				,OpenForReview
				,ClaimfrequencyTypeCodeId
				)
				SELECT @InvoiceId AS InvoiceId
				,NULL AS PatientInsuranceId
				,NULL AS PayerClaimControlNumber
				,NULL AS PatientInsuranceAuthorizationId				
				,NULL AS PatientInsuranceReferralId
				,CONVERT(BIT, 1) AS OpenForReview
				,1 AS ClaimfrequencyTypeCodeId
				FROM inserted i
				INNER JOIN Appointments ap ON ap.AppointmentId = i.AppointmentId
			END
		ELSE 
			BEGIN
			--Insert into InvoiceReceivables, the patient row
			INSERT INTO model.InvoiceReceivables (
				InvoiceId
				,PatientInsuranceId
				,PayerClaimControlNumber
				,PatientInsuranceAuthorizationId
				,PatientInsuranceReferralId
				,OpenForReview
				,ClaimfrequencyTypeCodeId
				)
				SELECT @InvoiceId AS InvoiceId
				,NULL AS PatientInsuranceId
				,NULL AS PayerClaimControlNumber
				,NULL AS PatientInsuranceAuthorizationId				
				,NULL AS PatientInsuranceReferralId
				,CONVERT(BIT, 0) AS OpenForReview
				,1 AS ClaimfrequencyTypeCodeId
				FROM inserted i
				INNER JOIN Appointments ap ON ap.AppointmentId = i.AppointmentId
			END
	END

	DECLARE @DoesExistsInvoiceReceivableForInsurer INT
	SELECT 
	@DoesExistsInvoiceReceivableForInsurer = ir.Id
	FROM model.InvoiceReceivables ir
	WHERE InvoiceId = @InvoiceId 
		AND PatientInsuranceId IN (
				SELECT
				pins.Id 
				FROM inserted i
				INNER JOIN model.PatientInsurances pins ON i.PatientId = pins.InsuredPatientId
				INNER JOIN model.InsurancePolicies ip ON ip.Id = pins.InsurancePolicyId
				WHERE ip.InsurerId = i.InsurerId
				AND ip.PolicyHolderPatientId = i.InsuredId
		)
	
	IF (@QualifyInsertedInvoiceReceivableAsTypeId = 3 AND @DoesExistsInvoiceReceivableForInsurer IS NULL)
	BEGIN
		IF (@InvoiceCreatedOnThisInsertTrigger = 1)
		--ok so this variable being one means we've created the invoice on THIS insert. (it didn't
		--already exist.)
		BEGIN
			INSERT INTO model.InvoiceReceivables (
				InvoiceId
				,PatientInsuranceId
				,PayerClaimControlNumber
				,PatientInsuranceAuthorizationId
				,PatientInsuranceReferralId
				,OpenForReview
				,ClaimfrequencyTypeCodeId
				)
			OUTPUT INSERTED.PatientInsuranceId INTO #HoldOrdinals
			SELECT @InvoiceId AS InvoiceId	
					,pi.Id AS PatientInsuranceId
					,i.ExternalRefInfo AS PayerClaimControlNumber
					,pia.Id AS PatientInsuranceAuthorizationId
					,epir.PatientInsuranceReferrals_Id AS PatientInsuranceReferralId
					,CONVERT(BIT, 0) AS OpenForReview
					,1 AS ClaimfrequencyTypeCodeId
				FROM inserted i
				LEFT JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
					AND (pi.EndDateTime IS NULL OR pi.EndDateTime = '' OR CONVERT(NVARCHAR,pi.EndDateTime,112) >= i.InvoiceDate)
					AND pi.IsDeleted <> 1
				LEFT JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id AND ip.InsurerId = i.InsurerId
					AND CONVERT(NVARCHAR,ip.StartDateTime,112) <= i.InvoiceDate
			LEFT JOIN model.EncounterPatientInsuranceReferral epir ON epir.Encounters_Id = i.AppointmentId
			LEFT JOIN model.PatientInsuranceAuthorizations pia ON pia.EncounterId = i.AppointmentId
			SET @NumberOfInsuranceRowsInserted = (SELECT @@ROWCOUNT)
			
			UPDATE ir
			SET ir.OpenForReview = CONVERT(BIT,1)
			FROM model.InvoiceReceivables ir
			WHERE ir.InvoiceId = @InvoiceId
			AND ir.PatientInsuranceId = dbo.GetPrimaryInsuranceId(@InvoiceId)

			UPDATE ir
			SET ir.PatientInsuranceAuthorizationId = NULL
			,ir.PatientInsuranceReferralId = NULL
			FROM model.InvoiceReceivables ir
			WHERE ir.InvoiceId = @InvoiceId
			AND ir.PatientInsuranceId <> dbo.GetPrimaryInsuranceId(@InvoiceId)

		END
		ELSE IF (@InvoiceCreatedOnThisInsertTrigger = 0)
		BEGIN
		--so if the invoice already exists, then look through invoicereceivables for the patientinsurance
		--only insert into IR, these new insurances, or what has changed.
		--ex: patient only has primary, later adds a secondary. (only insert secondary)
			INSERT INTO model.InvoiceReceivables (
			InvoiceId			
			,PatientInsuranceId
			,PayerClaimControlNumber
			,PatientInsuranceAuthorizationId
			,PatientInsuranceReferralId
			,OpenForReview
			,ClaimfrequencyTypeCodeId
			)
			OUTPUT INSERTED.PatientInsuranceId INTO #HoldOrdinals
			SELECT
			@InvoiceId AS InvoiceId
			,pi.Id
			,i.ExternalRefInfo AS PayerClaimControlNumber
			,pia.Id AS PatientInsuranceAuthorizationId
			,epir.PatientInsuranceReferrals_Id AS PatientInsuranceReferralId
			,CASE 
				WHEN i.InsurerDesignation = 'T'
					THEN CONVERT(BIT, 1)
				ELSE CONVERT(BIT, 0)
			END AS OpenForReview
			,1 AS ClaimfrequencyTypeCodeId
			FROM inserted i 
			INNER JOIN model.Invoices iv ON iv.EncounterId = i.AppointmentId AND iv.Id = @InvoiceId
			INNER JOIN model.PatientInsurances pi on pi.InsuredPatientId = i.PatientId
				AND (pi.EndDateTime IS NULL 
						OR pi.EndDateTime = '' 
						OR pi.EndDateTime >= iv.LegacyDateTime)
					AND pi.IsDeleted <> 1
			INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
				AND ip.InsurerId = i.InsurerId
				AND ip.PolicyHolderPatientId = i.InsuredId
			LEFT JOIN model.InvoiceReceivables ir on ir.InvoiceId = iv.id and pi.id = ir.PatientInsuranceId
			LEFT JOIN model.EncounterPatientInsuranceReferral epir ON epir.Encounters_Id = i.AppointmentId
			LEFT JOIN model.PatientInsuranceAuthorizations pia ON pia.EncounterId = i.AppointmentId
			WHERE ir.id IS NULL

			SET @NumberOfInsuranceRowsInserted = (SELECT @@ROWCOUNT)
			--to handle when new insurances are added, and previously billed.

			UPDATE ir
			SET ir.OpenForReview = CONVERT(BIT,1)
			FROM model.InvoiceReceivables ir
			WHERE ir.InvoiceId = @InvoiceId
			AND ir.PatientInsuranceId = dbo.GetPrimaryInsuranceId(@InvoiceId)

			UPDATE ir
			SET ir.PatientInsuranceAuthorizationId = NULL
			,ir.PatientInsuranceReferralId = NULL
			,ir.OpenForReview = CONVERT(BIT,0)
			FROM model.InvoiceReceivables ir
			WHERE ir.InvoiceId = @InvoiceId
			AND ir.PatientInsuranceId <> dbo.GetPrimaryInsuranceId(@InvoiceId)
		END
	
		UPDATE ir
		SET LegacyInsurerDesignation = CASE WHEN z.Id IS NOT NULL THEN 'T' ELSE NULL END
		FROM model.InvoiceReceivables ir
		LEFT JOIN (
			SELECT 
			v.Id
			,v.InvoiceId
			FROM (
					SELECT
					ROW_NUMBER() OVER (PARTITION BY pi.InsuranceTypeId
						ORDER BY pi.InsuranceTypeId, pi.OrdinalId, PolicyHolderRelationshipTypeId) as rn
					,ir.InvoiceId
					,pi.Id
					FROM model.InvoiceReceivables ir
					INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
					INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
						AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= i.LegacyDateTime)
						AND pi.IsDeleted = 0
					INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
						AND ip.StartDateTime <= i.LegacyDateTime
					WHERE PatientInsuranceId IS NOT NULL
					AND ir.InvoiceId = @InvoiceId
			) v WHERE v.rn = 1
		)z ON z.InvoiceId = ir.InvoiceId
			AND z.Id = ir.PatientInsuranceId
		WHERE ir.InvoiceId = @InvoiceId
			AND ir.PatientInsuranceId IS NOT NULL
	END

	IF EXISTS(
	SELECT 
	Id 
	FROM model.BillingServiceTransactions 
	WHERE InvoiceReceivableId IN (SELECT Id FROM model.InvoiceReceivables WHERE InvoiceId = @InvoiceId AND PatientInsuranceId IS NOT NULL)
	) OR EXISTS (SELECT 
				* 
				FROM PracticeTransactionJournal 
				WHERE TransactionTypeId IN (SELECT Id FROM model.InvoiceReceivables WHERE InvoiceId = @InvoiceId AND PatientInsuranceId IS NOT NULL) 
					AND TransactionType ='R')
	BEGIN
		UPDATE ir
		SET ir.OpenForReview = CONVERT(BIT,0)
		FROM model.InvoiceReceivables ir
		WHERE ir.InvoiceId = @InvoiceId
		AND PatientInsuranceId IS NOT NULL
	END
	END

	IF OBJECT_ID('dbo.ViewIdentities','U') IS NULL
	BEGIN	
		CREATE TABLE [dbo].[ViewIdentities](
		[Name] [nvarchar](max) NULL,
		[Value] [bigint] NULL
		)
	END

	IF OBJECT_ID('tempdb..#TempTableToHoldTheScopeIdentity') IS NOT NULL
		DROP TABLE #TempTableToHoldTheScopeIdentity

	CREATE TABLE #TempTableToHoldTheScopeIdentity (Id BIGINT IDENTITY(1,1) NOT NULL)
	SET IDENTITY_INSERT #TempTableToHoldTheScopeIdentity ON

	IF (@QualifyInsertedInvoiceReceivableAsTypeId = 1)
	BEGIN
		INSERT INTO #TempTableToHoldTheScopeIdentity (Id) 
		SELECT 
		ir.Id AS SCOPE_ID_COLUMN 
		FROM model.InvoiceReceivables ir
		WHERE ir.InvoiceId = @InvoiceId 
	END

	ELSE IF (@QualifyInsertedInvoiceReceivableAsTypeId = 2)
	BEGIN
		INSERT INTO #TempTableToHoldTheScopeIdentity (Id)
		SELECT (1 * 1000000000 + @InsertedCreditRowScopeIdentity) AS SCOPE_ID_COLUMN
	END
	ELSE IF (@QualifyInsertedInvoiceReceivableAsTypeId = 3)
	BEGIN
		IF (@InvoiceCreatedOnThisInsertTrigger = 1)
		BEGIN
			INSERT INTO #TempTableToHoldTheScopeIdentity (Id) 
			SELECT 
			ir.Id AS SCOPE_ID_COLUMN
			FROM #HoldOrdinals ho
			INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = @InvoiceId
				AND ir.PatientInsuranceId = ho.PatientInsuranceId
			WHERE ho.PatientInsuranceId = dbo.GetPrimaryInsuranceId(@InvoiceId)
				AND ir.PatientInsuranceId IS NOT NULL
		END
		ELSE IF (@InvoiceCreatedOnThisInsertTrigger = 0 AND @NumberOfInsuranceRowsInserted > 0)
		BEGIN
			DECLARE @InsurerIdVB INT
			SELECT @InsurerIdVB = i.InsurerId FROM inserted i
			
			INSERT INTO #TempTableToHoldTheScopeIdentity (Id) 
			SELECT 
			TOP 1 ir.Id
			FROM #HoldOrdinals ho
			INNER JOIN model.InvoiceReceivables ir ON ir.Id = ho.PatientInsuranceId
			INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
			INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			WHERE ip.InsurerId = @InsurerIdVB
				AND ir.InvoiceId = @InvoiceId
		END
		ELSE IF (@InvoiceCreatedOnThisInsertTrigger = 0 AND @NumberOfInsuranceRowsInserted = 0)
			INSERT INTO #TempTableToHoldTheScopeIdentity (Id) 
			SELECT TOP 1 ir.Id AS SCOPE_ID_COLUMN
			FROM model.InvoiceReceivables ir
			INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
			INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			WHERE ir.InvoiceId = @InvoiceId 
				AND ip.InsurerId = (SELECT i.InsurerId FROM inserted i)
	END
		
	IF NOT EXISTS(SELECT * FROM ViewIdentities WHERE Name = 'dbo.PatientReceivables')
		INSERT ViewIdentities(Name, Value) VALUES ('dbo.PatientReceivables', @@IDENTITY)
	ELSE
		UPDATE ViewIdentities SET Value = @@IDENTITY WHERE Name = 'dbo.PatientReceivables'

	DECLARE @PatientReceivablesInsertedIdentityValue INT
	SELECT @PatientReceivablesInsertedIdentityValue = @@IDENTITY 
	
	IF OBJECT_ID('tempdb..#PatientClinicalToBillingDiagnosis') IS NOT NULL
		DROP TABLE #PatientClinicalToBillingDiagnosis

	SELECT 
	ClinicalId
    ,AppointmentId    
	,Symptom
    ,FindingDetail 
	INTO #PatientClinicalToBillingDiagnosis
	FROM dbo.PatientClinical pc
	WHERE pc.AppointmentId IN (SELECT AppointmentId FROM inserted)
		AND pc.ClinicalType IN ('B','K')
		AND pc.[Status] = 'A'
		AND pc.AppointmentId <> 0

	INSERT INTO model.BillingDiagnosis (InvoiceId, OrdinalId, ExternalSystemEntityMappingId, LegacyPatientClinicalId)
	SELECT InvoiceId, OrdinalId, MIN(ExternalSystemEntityMappingId), LegacyPatientClinicalId FROM (
	SELECT 
	inv.Id AS InvoiceId
	,CASE Symptom			
		WHEN '1'
			THEN 1
		WHEN '2'
			THEN 2
		WHEN '3'
			THEN 3
		WHEN '4'
			THEN 4
		WHEN '5'
			THEN 5
		WHEN '6'
			THEN 6
		WHEN '7'
			THEN 7
		WHEN '8'
			THEN 8
		WHEN '9'
			THEN 9
		WHEN '10'
			THEN 10
		WHEN '11'
			THEN 11
		WHEN '12'
			THEN 12
	END AS OrdinalId
	,e.Id AS ExternalSystemEntityMappingId
	,pc.ClinicalId AS LegacyPatientClinicalId
	FROM #PatientClinicalToBillingDiagnosis pc
	INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
		AND ap.ScheduleStatus <> 'A'
	INNER JOIN model.Invoices inv on inv.EncounterId = pc.AppointmentId AND inv.InvoiceTypeId <> 2
	INNER JOIN model.ExternalSystemEntityMappings e ON e.ExternalSystemEntityKey = SUBSTRING(FindingDetail, 1, CASE 
				WHEN (dbo.GetMax(CHARINDEX('.', FindingDetail, CHARINDEX('.', FindingDetail, 0) + 1) - 1, 0)) > 0
					THEN (dbo.GetMax(CHARINDEX('.', FindingDetail, CHARINDEX('.', FindingDetail, 0) + 1) - 1, 0))
				ELSE LEN(FindingDetail)
				END)
		AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE name = 'ClinicalCondition')
		AND ExternalSystemEntityId = (SELECT TOP 1 Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = (SELECT TOP 1 Id FROM model.ExternalSystems WHERE name = 'Icd9')
			AND Name = 'ClinicalCondition')
	GROUP BY ap.AppointmentId, 
	inv.Id, 
	CASE Symptom
		WHEN '1'
			THEN 1
		WHEN '2'
			THEN 2
		WHEN '3'
			THEN 3
		WHEN '4'
			THEN 4
		WHEN '5'
			THEN 5
		WHEN '6'
			THEN 6
		WHEN '7'
			THEN 7
		WHEN '8'
			THEN 8
		WHEN '9'
			THEN 9
		WHEN '10'
			THEN 10
		WHEN '11'
			THEN 11
		WHEN '12'
			THEN 12
	END
	,e.Id
	,pc.ClinicalId

	UNION ALL

	SELECT
	inv.Id AS InvoiceId
	,CASE Symptom
		WHEN '1'
			THEN 1
		WHEN '2'
			THEN 2
		WHEN '3'
			THEN 3
		WHEN '4'
			THEN 4
		WHEN '5'
			THEN 5
		WHEN '6'
			THEN 6
		WHEN '7'
			THEN 7
		WHEN '8'
			THEN 8
		WHEN '9'
			THEN 9
		WHEN '10'
			THEN 10
		WHEN '11'
			THEN 11
		WHEN '12'
			THEN 12
	END AS OrdinalId
	,e.Id AS ExternalSystemEntityMappingId
	,pc.ClinicalId AS LegacyPatientClinicalId
	FROM #PatientClinicalToBillingDiagnosis pc
	INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.Appointmentid
		AND ap.Comments = 'ASC CLAIM'
	INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
		AND apEncounter.AppTypeId = ap.AppTypeId
		AND apEncounter.AppDate = ap.AppDate
		AND apEncounter.AppTime > 0 
		AND ap.AppTime = 0
		AND apEncounter.ScheduleStatus = ap.ScheduleStatus
		AND ap.ScheduleStatus = 'D'
		AND apEncounter.ResourceId1 = ap.ResourceId1
		AND apEncounter.ResourceId2 = ap.ResourceId2
	INNER JOIN model.Invoices inv on inv.EncounterId = apEncounter.AppointmentId AND inv.InvoiceTypeId = 2
	INNER JOIN model.ExternalSystemEntityMappings e ON e.ExternalSystemEntityKey = SUBSTRING(FindingDetail, 1, CASE 
				WHEN (dbo.GetMax(CHARINDEX('.', FindingDetail, CHARINDEX('.', FindingDetail, 0) + 1) - 1, 0)) > 0
					THEN (dbo.GetMax(CHARINDEX('.', FindingDetail, CHARINDEX('.', FindingDetail, 0) + 1) - 1, 0))
				ELSE LEN(FindingDetail)
				END)
		AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE name = 'ClinicalCondition')
		AND ExternalSystemEntityId = (SELECT TOP 1 Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = (SELECT TOP 1 Id FROM model.ExternalSystems WHERE name = 'Icd9')
			AND Name = 'ClinicalCondition')
	GROUP BY apEncounter.AppointmentId, 	
	inv.Id,
	CASE Symptom
		WHEN '1'
			THEN 1
		WHEN '2'
			THEN 2
		WHEN '3'
			THEN 3
		WHEN '4'
			THEN 4
		WHEN '5'
			THEN 5
		WHEN '6'
			THEN 6
		WHEN '7'
			THEN 7
		WHEN '8'
			THEN 8
		WHEN '9'
			THEN 9
		WHEN '10'
			THEN 10
		WHEN '11'
			THEN 11
		WHEN '12'
			THEN 12
	END
	,e.Id
	,pc.ClinicalId
	)v 
	GROUP BY v.InvoiceId, v.OrdinalId, v.LegacyPatientClinicalId 
	EXCEPT SELECT InvoiceId, OrdinalId, ExternalSystemEntityMappingId, LegacyPatientClinicalId FROM model.BillingDiagnosis

	SELECT @PatientReceivablesInsertedIdentityValue AS SCOPE_ID_COLUMN
END