
IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit

SELECT 1 AS Value INTO #NoAudit

SELECT Id
     INTO #todeleteServiceModifier
	 FROM model.ServiceModifiers
	 WHERE id NOT IN (--- id's linked with duplicate code except min(Id) 
                      SELECT min(id) AS id
                      FROM model.ServiceModifiers 
                      WHERE code IN (---Identify duplicate code
                                     SELECT code
                                     FROM model.ServiceModifiers
                                     GROUP BY code
                                     HAVING count(id)>1)
                      GROUP BY code)
       AND code IN 
         (SELECT code
          FROM model.ServiceModifiers
          GROUP BY code
          HAVING count(id)>1)

--- Delete from BillingServiceModifiers
DELETE 
FROM model.BillingServiceModifiers
WHERE servicemodifierid IN
    (SELECT Id from #todeleteServiceModifier)
		 		  
--- Delete from ServiceModifiers
 
  DELETE 
  FROM model.ServiceModifiers WHERE id IN 
    (SELECT Id from #todeleteServiceModifier)


DROP TABLE #NoAudit