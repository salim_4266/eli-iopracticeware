﻿SELECT 
CONVERT(NVARCHAR,bst.[DateTime],112) AS Date
,ap.PatientId
,NEWID() AS ExternalSystemMessageId
INTO #PatientDates
FROM model.BillingServiceTransactions bst
JOIN model.InvoiceReceivables ir ON bst.InvoiceReceivableId = ir.Id
	AND ir.PatientInsuranceId IS NULL
JOIN model.Invoices i ON i.Id = ir.InvoiceId
JOIN dbo.Appointments ap ON ap.AppointmentId = i.EncounterId
WHERE bst.BillingServiceTransactionStatusId = 2
GROUP BY CONVERT(NVARCHAR,bst.[DateTime],112)
	,ap.PatientId

DECLARE @ExternalSystemExternalSystemMessageTypeId INT

SET @ExternalSystemExternalSystemMessageTypeId = (
	SELECT TOP 1 Id
	FROM model.ExternalSystemExternalSystemMessageTypes
	WHERE ExternalSystemId = (SELECT TOP 1 Id FROM model.ExternalSystems WHERE Name = 'PatientStatement')
		AND ExternalSystemMessageTypeId = (SELECT TOP 1 Id FROM model.ExternalSystemMessageTypes WHERE Name = 'PatientStatement')
)

INSERT INTO [model].[ExternalSystemMessages](
	Id
	,[Description]
	,Value
	,ExternalSystemExternalSystemMessageTypeId
	,ExternalSystemMessageProcessingStateId
	,ProcessingAttemptCount
	,CreatedBy
	,CreatedDateTime
	,UpdatedDateTime)
SELECT 
ExternalSystemMessageId
,'AggregateStatements'
,''
,@ExternalSystemExternalSystemMessageTypeId
,3
,1
,'IO.Practiceware.DbMigration'
,Date
,Date
FROM #PatientDates

DECLARE @PracticeRepositoryEntityId INT
SET @PracticeRepositoryEntityId = (
	SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'BillingServiceTransaction'
)

INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(
	ExternalSystemMessageId
	,PracticeRepositoryEntityId
	,PracticeRepositoryEntityKey)
SELECT 
ExternalSystemMessageId
,@PracticeRepositoryEntityId
,bst.Id
FROM model.BillingServiceTransactions bst 
JOIN #PatientDates patientDates  ON CONVERT(NVARCHAR(8),bst.[DateTime],112) = patientDates.[Date]
JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId
JOIN model.Invoices i ON i.Id = ir.InvoiceId
JOIN model.Encounters e ON e.Id = i.EncounterId 
	AND e.PatientId = patientDates.PatientId
WHERE bst.BillingServiceTransactionStatusId = 2 
	AND ir.PatientInsuranceId IS NULL

