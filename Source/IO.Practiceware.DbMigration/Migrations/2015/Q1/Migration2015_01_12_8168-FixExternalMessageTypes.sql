﻿IF OBJECT_ID('model.ExternalSystemMessageTypesReadOnlyTrigger') IS NOT NULL
BEGIN
	ALTER TABLE model.ExternalSystemMessageTypes DISABLE TRIGGER ExternalSystemMessageTypesReadOnlyTrigger
END
UPDATE esmt
SET Name = 
CASE 
	WHEN Name LIKE '%_Outbound'
		THEN REPLACE(Name, '_Outbound', '')
	 WHEN Name LIKE '%_Inbound'
		THEN REPLACE(Name, '_Inbound', '')
	END
	 FROM model.ExternalSystemMessageTypes esmt
WHERE Name LIKE '%_Outbound' OR Name LIKE '%_Inbound'

IF OBJECT_ID('model.ExternalSystemMessageTypesReadOnlyTrigger') IS NOT NULL
BEGIN
	ALTER TABLE model.ExternalSystemMessageTypes ENABLE TRIGGER ExternalSystemMessageTypesReadOnlyTrigger
END
