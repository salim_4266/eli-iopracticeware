IF OBJECT_ID(N'model.PatientCommunicationPreferences') IS NOT NULL
BEGIN

	 DELETE FROM model.PatientCommunicationPreferences
			WHERE  
			NOT EXISTS (Select 1 from Model.PatientAddresses Where Id= PatientAddressId)
			AND PatientAddressId IS NOT NULL

	Insert Into model.PatientCommunicationPreferences
	(
		PatientId,
		PatientCommunicationTypeId,
		CommunicationMethodTypeId,
		PatientAddressId,
		IsAddressedToRecipient,
		PatientEmailAddressId,
		PatientPhoneNumberId,
		__EntityType__,
		IsOptOut
	)
	  Select 
		p.[PatientId],
		8,
		p.[CommunicationMethodTypeId],
		p.[PatientAddressId],
		p.[IsAddressedToRecipient],
		p.[PatientEmailAddressId],
		p.[PatientPhoneNumberId],
		p.[__EntityType__],
		0
	  FROM model.PatientCommunicationPreferences p
	  WHERE PatientCommunicationTypeId = 3
	  AND NOT EXISTS (SELECT 1 FROM model.PatientCommunicationPreferences WHERE 
	  PatientCommunicationTypeId = 8 AND PatientId = p.PatientId)
END
GO