--Updating FinancialSourceTypeId of FinancialBatches
IF OBJECTPROPERTY(OBJECT_ID('[model].[FinancialBatches]'), 'IsUserTable') = 1
BEGIN
	UPDATE fb
		SET FinancialSourceTypeId = 2
		FROM model.FinancialBatches fb
		WHERE FinancialSourceTypeId = 1 AND InsurerId IS NULL
END
GO

--updating  FinancialSourceTypeId of Adjustments 
IF OBJECTPROPERTY(OBJECT_ID('[model].[Adjustments]'), 'IsUserTable') = 1
BEGIN
	UPDATE ad
		SET FinancialSourceTypeId = 2
		FROM model.Adjustments ad
		WHERE PatientId IS NOT NULL AND FinancialSourceTypeId = 1
END
GO
