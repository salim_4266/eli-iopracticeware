CREATE NONCLUSTERED INDEX [Appointments_ResourceId1] ON [dbo].[Appointments]
(
       [ResourceId1] ASC
)
INCLUDE (     
 
 [AppointmentId],
        [AppTypeId],
        [AppDate],
        [AppTime],
        [Comments],
        [EncounterId]
) 
GO
 
