﻿IF OBJECT_ID('tempdb..#NoAudit','U') IS NOT NULL
	DROP TABLE #NoAudit
GO

SELECT 1 AS Value INTO #NoAudit
GO

DECLARE @BillingServiceFirstSent TABLE (BillingServiceId INT, DateFirstSent DATETIME)

INSERT INTO @BillingServiceFirstSent (BillingServiceId, DateFirstSent)
SELECT 
bst.BillingServiceId
,MIN(bst.DateTime) AS DateFirstSent
FROM model.BillingServiceTransactions bst
JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId
	AND ir.PatientInsuranceId IS NULL
WHERE bst.BillingServiceTransactionStatusId IN (2, 6)
GROUP BY bst.BillingServiceId

UPDATE bs
SET bs.PatientBillDate = b.DateFirstSent
FROM model.BillingServices bs
JOIN @BillingServiceFirstSent b ON b.BillingServiceId = bs.Id
WHERE bs.PatientBillDate IS NULL
GO