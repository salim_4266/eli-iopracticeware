﻿IF EXISTS (
		SELECT *
		FROM sys.triggers
		WHERE object_id = OBJECT_ID(N'[model].[AuditInvoiceCommentsInsertTrigger]')
		)
BEGIN
	EXEC('DROP TRIGGER [model].AuditInvoiceCommentsInsertTrigger')
END
GO

IF EXISTS (
		SELECT *
		FROM sys.triggers
		WHERE object_id = OBJECT_ID(N'[model].[AuditInvoiceCommentsUpdateTrigger]')
		)
BEGIN
	EXEC('DROP TRIGGER [model].AuditInvoiceCommentsUpdateTrigger')
END
GO

IF EXISTS (
		SELECT *
		FROM sys.triggers
		WHERE object_id = OBJECT_ID(N'[model].[AuditInvoiceCommentsDeleteTrigger]')
		)
BEGIN
	EXEC('DROP TRIGGER [model].AuditInvoiceCommentsDeleteTrigger')
END
GO

-- add DateTime column to InvoiceComments
IF NOT EXISTS (
		SELECT object_name(object_id)
		FROM sys.columns
		WHERE NAME = 'DateTime'
			AND object_name(object_id) = 'InvoiceComments'
		)
BEGIN
	ALTER TABLE model.InvoiceComments ADD DateTime DateTime NULL
END