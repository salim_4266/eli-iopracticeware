IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'[model].[InvoiceSupplementals]') AND name = 'OrderingProviderId')
BEGIN
ALTER TABLE [model].[InvoiceSupplementals]
ADD OrderingProviderId INT NULL
END

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'[model].[InvoiceSupplementals]') AND name = 'SupervisingProviderId')
BEGIN
ALTER TABLE [model].[InvoiceSupplementals]
ADD SupervisingProviderId INT NULL
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS  WHERE CONSTRAINT_NAME ='FK_InvoiceSupplementals_SupervisingProviderId')
BEGIN 
ALTER TABLE [model].[InvoiceSupplementals]
ADD CONSTRAINT FK_InvoiceSupplementals_SupervisingProviderId
FOREIGN KEY (SupervisingProviderId)
REFERENCES [model].[Providers](Id)
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS  WHERE CONSTRAINT_NAME ='FK_InvoiceSupplementals_OrderingProviderId')
BEGIN 
ALTER TABLE [model].[InvoiceSupplementals]
ADD CONSTRAINT FK_InvoiceSupplementals_OrderingProviderId
FOREIGN KEY (OrderingProviderId)
REFERENCES [model].[ExternalContacts](Id)
END