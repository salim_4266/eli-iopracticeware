IF OBJECT_ID('model.BillingOrganizations','V') IS NULL
BEGIN
EXEC (
'CREATE VIEW [model].[BillingOrganizations]
AS
----From PracticeName table
SELECT pn.PracticeId AS Id,
CONVERT(bit,0) AS IsArchived,
CASE pn.LocationReference
	WHEN ''''
		THEN CONVERT(bit, 1)
	ELSE CONVERT(bit, 0)
	END AS IsMain,
PracticeName AS Name,
CASE LocationReference
	WHEN ''''
		THEN ''Office''
	ELSE ''Office-'' + LocationReference
	END AS ShortName,
CONVERT(NVARCHAR, NULL) AS LastName,
CONVERT(NVARCHAR, NULL) AS FirstName,
CONVERT(NVARCHAR, NULL) AS MiddleName,
CONVERT(NVARCHAR, NULL) AS Suffix,
''BillingOrganization'' AS __EntityType__
FROM dbo.PracticeName pn
WHERE pn.PracticeType = ''P''
	
UNION ALL
	
----BillingOrganization From Resources table - doctors w OrgOverride/Override only
----has duplicates because of Override/OrgOverride
SELECT (110000000 * 1 + v.ResourceId) AS Id,
v.IsArchived,
v.IsMain,
v.Name,
v.ShortName,
v.LastName,
v.FirstName,
v.MiddleName,
v.Suffix,
v.__EntityType__
FROM (SELECT re.ResourceId AS ResourceId,
	CONVERT(bit,0) AS IsArchived,
	CONVERT(bit, 0) AS IsMain,
	ResourceDescription AS [Name],
	ResourceName AS ShortName,
	ResourceLastName AS LastName,
	ResourceFirstName AS FirstName,
	ResourceMI AS MiddleName,
	ResourceSuffix AS Suffix,
	''PersonBillingOrganization'' AS __EntityType__,
	COUNT_BIG(*) AS Count
FROM dbo.Resources re
LEFT OUTER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
	AND pa.ResourceType = ''R''
WHERE re.ResourceType in (''D'', ''Q'', ''Z'', ''Y'') 
	AND (pa.[Override] = ''Y'' OR pa.OrgOverride = ''Y'')
GROUP BY re.ResourceDescription,
	re.ResourceName,
	re.ResourceLastName,
	re.ResourceFirstName,
	re.ResourceMI,
	re.ResourceSuffix,
	re.ResourceId,
	re.Billable,
	re.ResourceType
) AS v
')
END
GO

IF OBJECT_ID('model.ProviderInsurerClaimFileOverride') IS NOT NULL
	EXEC dbo.DropObject 'model.ProviderInsurerClaimFileOverride'
GO

-- 'ProviderInsurerClaimFileOverride' does not exist, creating...
CREATE TABLE [model].[ProviderInsurerClaimFileOverride] (
	[Id] int IDENTITY(1,1) NOT NULL,
    [ClaimFileOverrideProviders_Id] int NOT NULL,
    [ClaimFileOverrideInsurers_Id] int NOT NULL,
);
GO

-- Create primary key constraint on Id indentity (required for replication)
ALTER TABLE [model].[ProviderInsurerClaimFileOverride]
ADD CONSTRAINT [PK_ProviderInsurerClaimFileOverride]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [ClaimFileOverrideProviders_Id] in table 'ProviderInsurerClaimFileOverride'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_ProviderProviderInsurerClaimFileOverride'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ProviderInsurerClaimFileOverride] DROP CONSTRAINT FK_ProviderProviderInsurerClaimFileOverride

IF OBJECTPROPERTY(OBJECT_ID('[model].[ProviderInsurerClaimFileOverride]'), 'IsUserTable') = 1
ALTER TABLE [model].[ProviderInsurerClaimFileOverride]
ADD CONSTRAINT [FK_ProviderProviderInsurerClaimFileOverride]
    FOREIGN KEY ([ClaimFileOverrideProviders_Id])
    REFERENCES [model].[Providers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProviderProviderInsurerClaimFileOverride'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_ProviderProviderInsurerClaimFileOverride')
	DROP INDEX IX_FK_ProviderProviderInsurerClaimFileOverride ON [model].[ProviderInsurerClaimFileOverrides]
GO

CREATE INDEX [IX_FK_ProviderProviderInsurerClaimFileOverride]
ON [model].[ProviderInsurerClaimFileOverride]
    ([ClaimFileOverrideProviders_Id]);
GO

-- Creating foreign key on [ClaimFileOverrideInsurers_Id] in table 'ProviderInsurerClaimFileOverride'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_InsurerProviderInsurerClaimFileOverride'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[ProviderInsurerClaimFileOverride] DROP CONSTRAINT FK_InsurerProviderInsurerClaimFileOverride

IF OBJECTPROPERTY(OBJECT_ID('[model].[ProviderInsurerClaimFileOverride]'), 'IsUserTable') = 1
ALTER TABLE [model].[ProviderInsurerClaimFileOverride]
ADD CONSTRAINT [FK_InsurerProviderInsurerClaimFileOverride]
    FOREIGN KEY ([ClaimFileOverrideInsurers_Id])
    REFERENCES [model].[Insurers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsurerProviderInsurerClaimFileOverride'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_InsurerProviderInsurerClaimFileOverride')
	DROP INDEX IX_FK_InsurerProviderInsurerClaimFileOverride ON [model].[ProviderInsurerClaimFileOverride]
GO

CREATE INDEX [IX_FK_InsurerProviderInsurerClaimFileOverride]
ON [model].[ProviderInsurerClaimFileOverride]
    ([ClaimFileOverrideInsurers_Id]);
GO


INSERT INTO model.ProviderInsurerClaimFileOverride (ClaimFileOverrideProviders_Id, ClaimFileOverrideInsurers_Id)
SELECT p.Id AS ClaimFileOverrideProviders_Id
	,ins.Id AS ClaimFileOverrideInsurers_Id
FROM dbo.Resources re
JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId	
	AND pa.ResourceType = 'R'
	AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
JOIN model.Insurers ins ON ins.Id = pa.PlanId
JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.IsBillable = CASE WHEN re.Billable = 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END 
	AND p.IsEMR = CASE re.GoDirect WHEN 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'PersonBillingOrganization')
WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y') 
GROUP BY p.Id
	,ins.Id
GO


IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersInsertPracticeAffiliations' AND parent_id = OBJECT_ID('dbo.PracticeAffiliations','U'))
	DROP TRIGGER ProvidersInsertPracticeAffiliations
GO

CREATE TRIGGER dbo.ProvidersInsertPracticeAffiliations ON dbo.PracticeAffiliations
AFTER INSERT
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Override nvarchar(1)
	DECLARE @OrgOverride nvarchar(1)
	DECLARE @ResourceId int
	DECLARE @ResourceType nvarchar(1)
	DECLARE @PracticeId int

	DECLARE ProvidersInsertPracticeAffiliationsCursor CURSOR FOR
	SELECT i.[Override], i.OrgOverride, i.ResourceId, r.ResourceType, r.PracticeId
	FROM inserted i
	JOIN dbo.Resources r ON r.ResourceId = i.ResourceId 
		AND r.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND i.ResourceType = 'R'

	OPEN ProvidersInsertPracticeAffiliationsCursor FETCH NEXT FROM ProvidersInsertPracticeAffiliationsCursor INTO
	@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		IF (@ResourceId IS NOT NULL) AND 
			((SELECT TOP 1 Id FROM model.Providers WHERE UserId = @ResourceId) IS NOT NULL)
			AND NOT EXISTS(SELECT TOP 1 * FROM model.Providers 
				WHERE UserId = @ResourceId 
					AND BillingOrganizationId = (110000000 * 1 + @ResourceId)) 
			AND (@Override = 'Y' OR @OrgOverride  = 'Y')
		BEGIN
			INSERT INTO model.Providers (UserId, BillingOrganizationId, IsBillable, IsEmr)
				SELECT @ResourceId AS UserId
				,(110000000 * 1 + @ResourceId) AS BillingOrganizationId
				,CONVERT(bit, 1) AS IsBillable
				,CONVERT(bit, 0) AS IsEmr
		END

		INSERT INTO model.ProviderInsurerClaimFileOverride (ClaimFileOverrideProviders_Id, ClaimFileOverrideInsurers_Id)
		SELECT 
		p.Id AS ClaimFileOverrideProviders_Id
		,ins.Id AS ClaimFileOverrideInsurers_Id
		FROM dbo.Resources re
		JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId	
			AND pa.ResourceType = 'R'
			AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
		JOIN model.Insurers ins ON ins.Id = pa.PlanId
		JOIN model.Providers p ON p.UserId = re.ResourceId
			AND p.IsBillable = CASE WHEN re.Billable = 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END 
			AND p.IsEMR = CASE WHEN re.GoDirect = 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END
			AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'PersonBillingOrganization')
		WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y') 
			AND re.ResourceId = @ResourceId
		GROUP BY p.Id
			,ins.Id

		EXCEPT 
		
		SELECT ClaimFileOverrideProviders_Id, ClaimFileOverrideInsurers_Id FROM model.ProviderInsurerClaimFileOverride

		FETCH NEXT FROM ProvidersInsertPracticeAffiliationsCursor INTO 
		@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId
	END
	CLOSE ProvidersInsertPracticeAffiliationsCursor
	DEALLOCATE ProvidersInsertPracticeAffiliationsCursor
END

GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersUpdatePracticeAffiliations' AND parent_id = OBJECT_ID('dbo.PracticeAffiliations','U'))
	DROP TRIGGER ProvidersUpdatePracticeAffiliations
GO

CREATE TRIGGER dbo.ProvidersUpdatePracticeAffiliations ON dbo.PracticeAffiliations
AFTER UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON;

	DELETE 
	FROM model.ProviderInsurerClaimFileOverride
	WHERE ClaimFileOverrideProviders_Id IN (
		SELECT p.Id 
		FROM model.Providers p
		WHERE p.UserId IN (SELECT DISTINCT ResourceId FROM deleted)
	)
		AND ClaimFileOverrideInsurers_Id IN (
		SELECT PlanId FROM deleted
	)

	DECLARE @Override nvarchar(1)
	DECLARE @OrgOverride nvarchar(1)
	DECLARE @ResourceId int
	DECLARE @ResourceType nvarchar(1)
	DECLARE @PracticeId int
	DECLARE @PlanId int

	DECLARE ProvidersUpdatePracticeAffiliationsCursor CURSOR FOR
	SELECT i.[Override], i.OrgOverride, i.ResourceId, r.ResourceType, r.PracticeId, i.PlanId
	FROM inserted i
	JOIN dbo.Resources r ON r.ResourceId = i.ResourceId 
		AND r.ResourceType IN ('D', 'Q', 'Z','Y')
		AND i.ResourceType = 'R'

	OPEN ProvidersUpdatePracticeAffiliationsCursor FETCH NEXT FROM ProvidersUpdatePracticeAffiliationsCursor INTO
	@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId, @PlanId
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		DECLARE @DeletedPracticeId INT
		DECLARE @DeletedResourceId INT
		SET @DeletedPracticeId = (SELECT TOP 1 r.PracticeId
		FROM deleted d
		JOIN dbo.Resources r ON r.ResourceId = d.ResourceId 
			AND r.ResourceType IN ('D', 'Q', 'Z','Y')
			AND d.ResourceType = 'R')
		
		IF (@ResourceId IS NOT NULL) AND 
			((SELECT TOP 1 Id FROM model.Providers WHERE UserId = @ResourceId) IS NOT NULL)
			AND NOT EXISTS(SELECT TOP 1 * FROM model.Providers 
			WHERE UserId = @ResourceId 
					AND BillingOrganizationId = (110000000 * 1 + @ResourceId)) 
			AND (@Override = 'Y' OR @OrgOverride  = 'Y')
		BEGIN
			INSERT INTO model.Providers (UserId, BillingOrganizationId, IsBillable, IsEmr)
				SELECT @ResourceId AS UserId
				,(110000000 * 1 + @ResourceId) AS BillingOrganizationId
				,CONVERT(bit, 1) AS IsBillable
				,CONVERT(bit, 0) AS IsEmr
		END

		INSERT INTO model.ProviderInsurerClaimFileOverride (ClaimFileOverrideProviders_Id, ClaimFileOverrideInsurers_Id)
		SELECT 
		p.Id AS ClaimFileOverrideProviders_Id
		,ins.Id AS ClaimFileOverrideInsurers_Id
		FROM dbo.Resources re
		JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId	
			AND pa.ResourceType = 'R'
			AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
		JOIN model.Insurers ins ON ins.Id = pa.PlanId
		JOIN model.Providers p ON p.UserId = re.ResourceId
			AND p.IsBillable = CASE WHEN re.Billable = 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END 
			AND p.IsEMR = CASE WHEN re.GoDirect = 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END
			AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'PersonBillingOrganization')
		WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y') 
			AND re.ResourceId = @ResourceId
		GROUP BY p.Id
			,ins.Id
			
		EXCEPT 
		
		SELECT ClaimFileOverrideProviders_Id, ClaimFileOverrideInsurers_Id FROM model.ProviderInsurerClaimFileOverride

		FETCH NEXT FROM ProvidersUpdatePracticeAffiliationsCursor INTO 
		@Override, @OrgOverride, @ResourceId, @ResourceType, @PracticeId, @PlanId
	END
	CLOSE ProvidersUpdatePracticeAffiliationsCursor
	DEALLOCATE ProvidersUpdatePracticeAffiliationsCursor
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersDeletePracticeAffiliations' AND parent_id = OBJECT_ID('dbo.PracticeAffiliations','U'))
	DROP TRIGGER ProvidersDeletePracticeAffiliations
GO

CREATE TRIGGER dbo.ProvidersDeletePracticeAffiliations ON dbo.PracticeAffiliations
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	DELETE 
	FROM model.ProviderInsurerClaimFileOverride
	WHERE (ClaimFileOverrideProviders_Id IN (
		SELECT p.Id 
		FROM model.Providers p
		WHERE p.UserId IN (
		SELECT DISTINCT ResourceId FROM deleted
		)
	) 
		AND ClaimFileOverrideInsurers_Id IN (
		SELECT PlanId FROM deleted
		)
	)

END
GO