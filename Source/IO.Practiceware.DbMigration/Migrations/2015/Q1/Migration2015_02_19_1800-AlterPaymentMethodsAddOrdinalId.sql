IF NOT EXISTS (SELECT TOP 1 * FROM sys.columns WHERE object_id = OBJECT_ID('model.PaymentMethods','U') AND name = 'OrdinalId')
	BEGIN
		ALTER TABLE model.PaymentMethods ADD [OrdinalId] INT NOT NULL DEFAULT(1)
	END
GO

IF EXISTS (SELECT TOP 1 * FROM sys.columns WHERE object_id = OBJECT_ID('model.ExternalSystemEntities','U') AND name = 'PracticeRepositoryEntityId')
	BEGIN
		UPDATE pm SET pm.OrdinalId = COALESCE(pct.[rank],1)
		FROM model.PaymentMethods pm 
		INNER JOIN  dbo.PracticeCodeTable pct ON pct.Id = pm.LegacyPracticeCodeTableId
		WHERE pct.[rank] <> 0
	END
GO