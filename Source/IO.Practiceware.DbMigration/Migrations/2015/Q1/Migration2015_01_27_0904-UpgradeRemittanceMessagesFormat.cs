﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using FluentMigrator;
using IO.Practiceware.Integration.X12;
using IO.Practiceware.Model;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;

namespace IO.Practiceware.DbMigration.Migrations._2015.Q1
{
    [Migration(201501270904)]
    public class Migration201501270904UpgradeRemittanceMessagesFormat : NonFluentTransactionalUpOnlyMigration
    {
        protected override void MigrateUp(IDbConnection connection, IDbTransaction transaction)
        {
            var externalMessageTypeId = connection.Execute<int>(string.Format(
                        "SELECT TOP 1 Id FROM model.ExternalSystemExternalSystemMessageTypes WHERE ExternalSystemId = {0} AND ExternalSystemMessageTypeId = {1}",
                        (int)ExternalSystemId.Payer, (int)ExternalSystemMessageTypeId.X12_835_Inbound),
                        transaction);

            while (true)
            {
                // Get non-converted messages yet (xml values will start with '<')
                var messagesTable = connection.Execute<DataTable>(string.Format(
                    "SELECT TOP 50 * FROM model.ExternalSystemMessages WHERE ExternalSystemExternalSystemMessageTypeId = {0} AND Value NOT LIKE '<%' AND Value IS NOT NULL", externalMessageTypeId),
                    transaction);

                // No more messages to convert -> quit
                if (messagesTable.Rows.Count == 0) break;

                // Read messages info and parse them from X12
                var parser = new RemittanceMessageParser();
                var convertedMessages = messagesTable.Rows
                    .OfType<DataRow>()
                    .Select(r => new
                    {
                        Id = r["Id"].CastTo<Guid>(),
                        Error = r["Error"] == DBNull.Value ? null : r["Error"].CastTo<string>(),
                        State = r["ExternalSystemMessageProcessingStateId"].CastTo<int>().CastTo<ExternalSystemMessageProcessingStateId>(),
                        ParsedMessage = parser.Parse(r["Value"].CastTo<string>())
                    })
                    .ToList();

                // Cache insurer names resolution (only for processed)
                var payerIdentifiers = convertedMessages
                    .Where(m => m.State == ExternalSystemMessageProcessingStateId.Processed)
                    .SelectMany(m => m.ParsedMessage.Advices)
                    .SelectMany(a => a.PayerIdentifiers)
                    .Distinct().ToList();
                if (!payerIdentifiers.Any())
                {
                    payerIdentifiers.Add(string.Empty);
                }

                var insurerNamesTable = connection.Execute<DataTable>(string.Format(@"
SELECT PayerCode As PayerIdentifier, Name As InsurerName
FROM model.Insurers
WHERE PayerCode IN ({0})
UNION
SELECT DISTINCT ipc.Value As PayerIdentifier, ins.Name as InsurerName
FROM model.IncomingPayerCodes ipc
JOIN model.IncomingPayerCodeInsurer ipci on
	ipci.IncomingPayerCodes_Id = ipc.Id
JOIN model.Insurers ins on
	ins.Id = ipci.Insurers_Id
WHERE ipc.Value IN ({0})", payerIdentifiers.Select(pi => string.Format("'{0}'", pi)).Join()),
                    transaction);

                var insurerNames = insurerNamesTable.Rows.OfType<DataRow>()
                    .Select(r => new
                    {
                        PayerIdentifier = r["PayerIdentifier"].CastTo<string>(),
                        InsurerName = r["InsurerName"].CastTo<string>()
                    })
                    .ToList();

                // Assign known insurer names
                foreach (var processedAdvice in convertedMessages
                    .Where(m => m.State == ExternalSystemMessageProcessingStateId.Processed)
                    .SelectMany(m => m.ParsedMessage.Advices))
                {
                    processedAdvice.ResolvedInsurerName = insurerNames
                        .Where(i => processedAdvice.PayerIdentifiers.Contains(i.PayerIdentifier))
                        .Select(i => i.InsurerName)
                        .FirstOrDefault();
                }

                // Prepare update statements
                var updateQuery = new StringBuilder();
                var updateQueryParams = new Dictionary<string, object>();
                var updateRowsCount = 0;

                foreach (var cm in convertedMessages)
                {
                    // Restore errors for advices
                    var hasCriticalErrors = cm.ParsedMessage.CriticalErrors.Any();
                    if (!hasCriticalErrors
                        && cm.ParsedMessage.Advices.Any()
                        && cm.Error.IsNotNullOrEmpty()
                        && cm.State == ExternalSystemMessageProcessingStateId.Processed)
                    {
                        var errorsXml = XElement.Parse(cm.Error);
                        var adviceErrorsDictionary = errorsXml
                            .Elements()
                            .ToDictionary<XElement, int, IList<string>>(
                                errorElement => errorElement.FirstAttribute.Value.ToInt().GetValueOrDefault(),
                                errorElement => new List<string> { errorElement.Value });

                        for (int i = 0; i < cm.ParsedMessage.Advices.Count; i++)
                        {
                            IList<string> errors;
                            if (adviceErrorsDictionary.TryGetValue(i, out errors))
                            {
                                cm.ParsedMessage.Advices[i].Errors.Clear();
                                cm.ParsedMessage.Advices[i].Errors.AddRange(errors);
                            }
                        }
                    }

                    // Prepare Sql update
                    updateQuery.AppendLine(string.Format("UPDATE model.ExternalSystemMessages SET Value = @Value{0}, Error = @Error{0} WHERE Id = @Id{0}", ++updateRowsCount));
                    updateQueryParams.Add("@Id" + updateRowsCount, cm.Id);
                    updateQueryParams.Add("@Value" + updateRowsCount, cm.ParsedMessage.ToXml(null, false));
                    updateQueryParams.Add("@Error" + updateRowsCount, hasCriticalErrors ? cm.ParsedMessage.CriticalErrors.Join(Environment.NewLine) : (object)DBNull.Value);
                }

                // Execute update for migrated messages
                connection.Execute(updateQuery.ToString(), transaction, updateQueryParams);
            }
        }
    }
}
