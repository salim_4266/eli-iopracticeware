﻿IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	
	DECLARE @content AS NVARCHAR(MAX);
	DECLARE @old VARCHAR(max);
	DECLARE @new VARCHAR(max);
	DECLARE @StartIndex INT;
	DECLARE @EndIndex INT;
	
	SET @new =	'<input type="text" id="Line24" value="@(string.IsNullOrEmpty(service.Line24) ? "" : service.Line24.Length > 39 ? service.Line24.Substring(0,39) : service.Line24)" class="smallText" style="z-index: 1; left: 32px; top: @((beginTopRenProvTax) + (index * incrementalPixels))px; position: absolute; width: 144px; height: 14px;" />
'

	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = 'CMS-1500'
				AND Content IS NOT NULL
			)
	SET @StartIndex = CHARINDEX(LOWER('<input type="text" id="Ndc"'), LOWER(@content), 1)

	SET @EndIndex = 0

	IF @StartIndex > 0
	BEGIN
		SET @EndIndex = CHARINDEX('<input type="text" id="ServiceDate1Month"', @content, @StartIndex) - 1
		SET @old = SUBSTRING(@content, @StartIndex, @EndIndex - @StartIndex)

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = 'CMS-1500'
			AND Content IS NOT NULL
	END

END
GO
 