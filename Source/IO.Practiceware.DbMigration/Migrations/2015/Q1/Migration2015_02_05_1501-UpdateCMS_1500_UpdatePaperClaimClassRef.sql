﻿IF EXISTS (
		SELECT *
		FROM model.TemplateDocuments
		WHERE DisplayName = 'CMS-1500'
			AND Content IS NOT NULL
		)
BEGIN
	UPDATE model.TemplateDocuments
	SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), 'IO.Practiceware.Integration.PaperClaims.PaperClaimInfo', 'IO.Practiceware.Integration.PaperClaims.PaperClaim'))
	FROM model.TemplateDocuments
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL
END
GO

IF EXISTS (
		SELECT *
		FROM model.TemplateDocuments
		WHERE DisplayName = 'CMS-1500'
			AND Content IS NOT NULL
		)
BEGIN
	UPDATE model.TemplateDocuments
	SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), 'IO.Practiceware.Integration.PaperClaims.ViewModels.PaperClaimViewModel', 'IO.Practiceware.Integration.PaperClaims.PaperClaim'))
	FROM model.TemplateDocuments
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL
END
GO

