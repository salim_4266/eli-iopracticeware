﻿--Populating NoteDate into DateTime of InvoiceComments
IF OBJECTPROPERTY(OBJECT_ID('[model].[InvoiceComments]'), 'IsUserTable') = 1
BEGIN
	UPDATE ic 
		SET ic.DateTime = CONVERT(datetime, tempic.NoteDate)
		FROM  model.InvoiceComments ic
		JOIN (SELECT ic.id, MAX(pn.NoteDate) as NoteDate 
				FROM model.InvoiceComments ic
				LEFT JOIN PatientNotes pn on pn.note1 + pn.note2 + pn.note3 + pn.note4 = ic.Value 
				INNER JOIN model.Invoices inv1 ON pn.AppointmentId = inv1.EncounterId
				INNER JOIN model.Invoices inv2 ON ic.InvoiceId = inv2.Id
				GROUP BY ic.id) tempic ON tempic.Id = ic.Id
END
GO

--Populating NoteDate into DateTime of BillingServiceComments 
IF OBJECTPROPERTY(OBJECT_ID('[model].[BillingServiceComments]'), 'IsUserTable') = 1
BEGIN
	UPDATE bsc 
		SET bsc.DateTime = CONVERT(datetime, tempbsc.NoteDate)
		FROM  model.BillingServiceComments bsc
		JOIN (SELECT bsc.id, MAX(pn.NoteDate) as NoteDate
				FROM model.BillingServiceComments bsc
				LEFT JOIN PatientNotes pn on pn.note1 + pn.note2 + pn.note3 + pn.note4 = bsc.Value 
				JOIN model.Invoices i ON i.LegacyPatientReceivablesAppointmentId = pn.AppointmentId 
				JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
				JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
				GROUP BY bsc.id) as tempbsc on tempbsc.Id = bsc.Id 
END
GO
