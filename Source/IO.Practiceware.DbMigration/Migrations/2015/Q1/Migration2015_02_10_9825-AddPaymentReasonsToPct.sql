IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE Code = '237' AND ReferenceType = 'PAYMENTREASON')
BEGIN
	INSERT INTO PracticeCodeTable (ReferenceType, Code, AlternateCode)
	VALUES ('PAYMENTREASON', '237', '')
END

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE Code = '96' AND ReferenceType = 'PAYMENTREASON')
BEGIN
	INSERT INTO PracticeCodeTable (ReferenceType, Code, AlternateCode)
	VALUES ('PAYMENTREASON', '96', '')
END

