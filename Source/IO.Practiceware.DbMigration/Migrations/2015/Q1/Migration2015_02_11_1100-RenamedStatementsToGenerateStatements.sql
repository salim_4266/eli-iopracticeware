--Renamed Statements to Generate Statements.

UPDATE model.ApplicationSettings SET  Value = REPLACE(value,'i:type="d1p1:StatementsFilter"','i:type="d1p1:GenerateStatementsFilter"') WHERE Name='StatementsFilter'

UPDATE model.ApplicationSettings SET  Value = REPLACE(value,'xmlns:d1p1="http://schemas.datacontract.org/2004/07/IO.Practiceware.Presentation.ViewModels.Statements"','xmlns:d1p1="http://schemas.datacontract.org/2004/07/IO.Practiceware.Presentation.ViewModels.GenerateStatements"') WHERE Name='StatementsFilter'

UPDATE model.ApplicationSettings  SET Name='GenerateStatementsFilter' WHERE Name='StatementsFilter'