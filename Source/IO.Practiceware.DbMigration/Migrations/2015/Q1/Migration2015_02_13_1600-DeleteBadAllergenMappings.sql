﻿IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL 
	DROP TABLE #NoAudit

SELECT 1 AS Value INTO #NoAudit

DECLARE @RxNormExternalSystemId INT
SELECT @RxNormExternalSystemId = Id FROM model.ExternalSystems WHERE Name = 'RxNorm'

DECLARE @AllergenId INT
SELECT @AllergenId = Id FROM model.PracticeRepositoryEntities WHERE Name = 'Allergen'

DECLARE @RxNormAllergen INT
SELECT @RxNormAllergen = Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = @RxNormExternalSystemId
AND Name = 'Allergen'

DELETE esem
FROM model.ExternalSystemEntityMappings esem
WHERE (esem.ExternalSystemEntityId = @RxNormAllergen)
AND esem.PracticeRepositoryEntityId = @AllergenId

DROP TABLE #NoAudit
