﻿IF EXISTS (
		SELECT *
		FROM sys.triggers
		WHERE object_id = OBJECT_ID(N'[model].[AuditAlertsInsertTrigger]')
		)
BEGIN
	DROP TRIGGER [model].[AuditAlertsInsertTrigger]
END
GO

IF EXISTS (
		SELECT *
		FROM sys.triggers
		WHERE object_id = OBJECT_ID(N'[model].[AuditAlertsUpdateTrigger]')
		)
BEGIN
	DROP TRIGGER [model].[AuditAlertsUpdateTrigger]
END
GO

IF EXISTS (
		SELECT *
		FROM sys.triggers
		WHERE object_id = OBJECT_ID(N'[model].[AuditAlertsDeleteTrigger]')
		)
BEGIN
	DROP TRIGGER [model].[AuditAlertsDeleteTrigger]
END
GO

-- Remove column IsActive from Alert table
IF EXISTS (
		SELECT object_name(object_id)
		FROM sys.columns
		WHERE NAME = 'IsActive'
			AND object_name(object_id) = 'Alerts'
		)
BEGIN
	ALTER TABLE model.Alerts

	DROP COLUMN IsActive
END