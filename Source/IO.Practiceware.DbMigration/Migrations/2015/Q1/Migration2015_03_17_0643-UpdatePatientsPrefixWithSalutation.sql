IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL
  DROP  TABLE #NoAudit
GO
  SELECT 1 AS Value INTO #NoAudit
GO

  UPDATE model.Patients 
  SET Prefix= Salutation 
  WHERE ISNULL(Prefix,'') = '' AND ISNULL(Salutation,'') <> ''
GO
  UPDATE model.Patients 
  SET Salutation= NULL 
  WHERE Salutation IS NOT NULL
GO

  DROP TABLE #NoAudit
GO