﻿DECLARE @sql NVARCHAR(MAX)
SET @sql = ''
SELECT @sql = @sql +
'DROP TRIGGER [model].[' + name + '] ; ' 
FROM sys.triggers WHERE parent_id =  OBJECT_ID('model.BillingServiceTransactions', 'U')
	AND name LIKE 'Audit%'

EXEC sys.sp_executesql @sql
GO

INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities (ExternalSystemMessageId, PracticeRepositoryEntityId, PracticeRepositoryEntityKey)
SELECT 
ExternalSystemMessageId
,10 AS PracticeRepositoryEntityId
,Id AS PracticeRepositoryEntityKey
FROM model.BillingServiceTransactions
WHERE ExternalSystemMessageId IS NOT NULL
GO

DECLARE @sql NVARCHAR(MAX)
SET @sql = ''
SELECT @sql = @sql +
'DROP INDEX ' + QUOTENAME(ix.name) + ' ON [model].[BillingServiceTransactions]; '
FROM sys.index_columns ic
JOIN sys.columns c ON c.object_id = ic.object_id
	AND c.name = 'ExternalSystemMessageId'
	AND ic.column_id = c.column_id
JOIN sys.indexes ix ON ix.index_id = ic.index_id
	AND ix.object_id = ic.object_id
WHERE ic.object_id = OBJECT_ID('model.BillingServiceTransactions', 'U')

EXEC sys.sp_executesql @sql
GO

DECLARE @sql NVARCHAR(MAX)
SET @sql = ''
SELECT @sql = @sql +
'ALTER TABLE [model].[BillingServiceTransactions] DROP CONSTRAINT ' + QUOTENAME(fc.name) + '; '
FROM sys.foreign_key_columns fkc
JOIN sys.columns c ON c.object_id = fkc.parent_object_id
	AND c.name = 'ExternalSystemMessageId'
	AND fkc.parent_column_id = c.column_id
JOIN sys.foreign_keys fc ON fc.parent_object_id = fkc.parent_object_id
	AND fc.referenced_object_id = OBJECT_ID('model.ExternalSystemMessages', 'U')
WHERE fkc.parent_object_id = OBJECT_ID('model.BillingServiceTransactions', 'U')
EXEC sys.sp_executesql @sql
GO

ALTER TABLE [model].[BillingServiceTransactions] DROP COLUMN [ExternalSystemMessageId]
GO

ALTER TRIGGER [dbo].[ServiceTransactionsInsert] ON [dbo].[ServiceTransactions]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @InsertedBillingServiceTransactions TABLE (LegacyTransactionId INT, AmountSent DECIMAL(18,2))

	INSERT INTO model.BillingServiceTransactions(
	[DateTime]
	,AmountSent
	,ClaimFileNumber
	,BillingServiceTransactionStatusId
	,MethodSentId
	,BillingServiceId
	,InvoiceReceivableId
	,ClaimFileReceiverId
	,LegacyTransactionId)
	OUTPUT inserted.LegacyTransactionId, inserted.AmountSent INTO @InsertedBillingServiceTransactions(LegacyTransactionId,AmountSent)
	SELECT
	DATEADD(mi,TransactionTime,(CONVERT(DATETIME,ptj.TransactionDate))) AS [DateTime]
	,i.Amount AS AmountSent
	,NULL AS ClaimFileNumber
	,CASE 
		WHEN ptj.TransactionStatus = 'P'
			AND i.TransportAction IN ('B', 'P')
			THEN 1
		WHEN ptj.TransactionStatus = 'P'
			AND i.TransportAction = 'V'
			THEN 3
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'P'
			THEN 2
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'V'
			THEN 3
		WHEN ptj.TransactionStatus = 'S'
			AND i.TransportAction = 'X'
			THEN 4
		WHEN ptj.TransactionStatus = 'Z'
			AND i.TransportAction = '1'
			THEN 6
		WHEN ptj.TransactionStatus = 'Z'
			AND i.TransportAction = '2'
			THEN 7
		ELSE 5
	END AS BillingServiceTransactionStatusId
	,CASE 
		WHEN (ptj.TransactionRef = 'I' AND ptj.TransactionAction = 'B')
			THEN 1
		ELSE 2 
	END AS MethodSentId
	,i.ServiceId AS BillingServiceId
	,CASE 
		WHEN ptj.TransactionRef = 'P'
			THEN irPatient.Id
		ELSE ptj.TransactionTypeId
	END AS InvoiceReceivableId
	,CASE 
		WHEN ptj.TransactionRef = 'I' AND ptj.TransactionAction = 'B' 
			THEN cr.Id 
		ELSE NULL 
	END AS ClaimFileReceiverId
	,i.TransactionId AS LegacyTransactionId
	FROM inserted i 
	INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionId = i.TransactionId
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = ptj.TransactionTypeId
	INNER JOIN model.InvoiceReceivables irPatient ON irPatient.InvoiceId = ir.InvoiceId
		AND irPatient.PatientInsuranceId IS NULL
	LEFT JOIN model.PatientInsurances pi ON ir.PatientInsuranceId = pi.Id
	LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
	LEFT JOIN dbo.PracticeInsurers pri on pri.InsurerId = ip.InsurerId
	LEFT JOIN dbo.PracticeCodeTable pctMethod ON pri.InsurerTFormat = SUBSTRING(pctMethod.Code, 1, 1) 
		AND pctMethod.ReferenceType = 'TRANSMITTYPE'
	LEFT JOIN model.ClaimFileReceivers cr ON cr.LegacyPracticeCodeTableId = pctMethod.Id
END
GO

ALTER TRIGGER [dbo].[ServiceTransactionsUpdate] ON [dbo].[ServiceTransactions]
INSTEAD OF UPDATE
AS
BEGIN

IF EXISTS (
SELECT * FROM inserted
EXCEPT 
SELECT * FROM deleted
)
	BEGIN
		SET NOCOUNT ON
		UPDATE bst
		SET bst.BillingServiceId = i.ServiceId
		,bst.AmountSent = i.Amount
		,bst.BillingServiceTransactionStatusId = CASE 
			WHEN ptj.TransactionStatus = 'P'
				AND i.TransportAction IN ('B', 'P')
				THEN 1
			WHEN ptj.TransactionStatus = 'P'
				AND i.TransportAction = 'V'
				THEN 3
			WHEN ptj.TransactionStatus = 'S'
				AND i.TransportAction = 'P'
				THEN 2
			WHEN ptj.TransactionStatus = 'S'
				AND i.TransportAction = 'V'
				THEN 3
			WHEN ptj.TransactionStatus = 'S'
				AND i.TransportAction = 'X'
				THEN 4
			WHEN ptj.TransactionStatus = 'Z'
				AND i.TransportAction = '1'
				AND d.TransportAction = '0'
				THEN bst.BillingServiceTransactionStatusId
			WHEN ptj.TransactionStatus = 'Z'
				AND i.TransportAction = '1'
				THEN 6
			WHEN ptj.TransactionStatus = 'Z'
				AND i.TransportAction = '2'
				THEN 7
			ELSE 5
		END
		,bst.[DateTime] = CONVERT(DATETIME,i.ModTime,112)
		FROM model.BillingServiceTransactions bst
		JOIN inserted i ON i.Id = bst.Id
		LEFT JOIN deleted d ON d.Id = bst.Id
		JOIN PracticeTransactionJournal ptj ON i.TransactionId = ptj.TransactionId

		UPDATE ptj
		SET ptj.TransactionDate = i.TransactionDate
		FROM PracticeTransactionJournal ptj
		JOIN inserted i ON i.TransactionId = ptj.TransactionId
	END
END
GO