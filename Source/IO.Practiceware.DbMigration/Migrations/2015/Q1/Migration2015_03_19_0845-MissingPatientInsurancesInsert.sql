﻿-- Check if there are any Patient Insurances Where The PolicyHolderPatientId in the Insurance Policy matches the Insured Patient Id
-- and the PolicyHolderRelationshipId is Self
IF EXISTS (
	SELECT * FROM model.InsurancePolicies ip 
	WHERE ip.PolicyHolderPatientId NOT IN ( 
		SELECT InsuredPatientId FROM model.PatientInsurances pi WHERE pi.InsurancePolicyId = ip.Id AND PolicyHolderRelationshipTypeId = 1
	)
)
BEGIN
	-- Insert missing PatientInsurances
	INSERT INTO [model].[PatientInsurances]
			   ([InsuranceTypeId]
			   ,[PolicyHolderRelationshipTypeId]
			   ,[OrdinalId]
			   ,[InsuredPatientId]
			   ,[InsurancePolicyId]
			   ,[EndDateTime]
			   ,[IsDeleted])
		SELECT 1 AS InsuranceTypeId
			, 1 AS PolicyHolderRelationshipTypeId
			,10 AS OrdinalId
			, ip.PolicyHolderPatientId
			, ip.Id
			, ip.StartDateTime 
			, 0 AS IsDeleted
		FROM model.InsurancePolicies ip 
		WHERE ip.Id IN (	
			SELECT Id FROM model.InsurancePolicies ip 
				WHERE ip.PolicyHolderPatientId NOT IN ( 
						SELECT InsuredPatientId 
						FROM model.PatientInsurances pi 
						WHERE pi.InsurancePolicyId = ip.Id AND PolicyHolderRelationshipTypeId = 1
				)
		)
END
