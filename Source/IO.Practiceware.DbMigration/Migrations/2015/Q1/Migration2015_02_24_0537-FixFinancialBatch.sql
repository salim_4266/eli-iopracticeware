﻿-- The purpose of this migration is to set model.FinancialBatches.PatientId equal to the model.Adjustments.InvoiceReceivable.Invoice.LegacyPatientReceivablesPatientId
-- The model.Adjustments.InvoiceReceivable.Invoice.LegacyPatientReceivablesPatientId is the same as the The model.Adjustments.InvoiceReceivable.Invoice.Encounter.PatientId
IF OBJECT_ID('dbo.UpdatedPatientFinancialBatchAdjustments', 'U') IS NULL
	AND OBJECT_ID('dbo.UpdatedPatientFinancialBatchFinancialInformations', 'U') IS NULL
	AND OBJECT_ID('dbo.UpdatedPatientFinancialBatchAdjustmentsIncorrect', 'U') IS NULL
	AND OBJECT_ID('dbo.UpdatedPatientFinancialBatchFinancialInformationsIncorrect', 'U') IS NULL
BEGIN

-- Skip auditing for speed.
SELECT 1 AS Value INTO #NoAudit

-- Add temporary columns to model.FinancialBatches, to make updating the model.Adjustments easier.
ALTER TABLE model.FinancialBatches 
ADD AdjustmentId INT NULL

ALTER TABLE model.FinancialBatches 
ADD FinancialInformationId INT NULL

DECLARE @sql NVARCHAR(MAX)
SET @sql = '
-- Gather all non-credit model.Adjustments, linked to a patient FinancialBatch into dbo.UpdatedPatientFinancialBatchAdjustments
-- This is how we associate an Adjustment with a FinancialBatch and PatientId
SELECT 
a.Id
,a.PostedDateTime
,ISNULL(f.CheckCode, '''') AS CheckCode
,a.Amount
,f.PaymentMethodId
,i.LegacyPatientReceivablesPatientId AS PatientId
,f.Id AS FinancialBatchId
,a.FinancialSourceTypeId
INTO dbo.UpdatedPatientFinancialBatchAdjustments
FROM model.Adjustments a
JOIN model.FinancialBatches f ON f.Id = a.FinancialBatchId
	AND f.FinancialSourceTypeId = 2
JOIN model.InvoiceReceivables ir ON ir.Id = a.InvoiceReceivableId
JOIN model.Invoices i ON i.Id = ir.InvoiceId
	AND i.LegacyPatientReceivablesPatientId IS NOT NULL
WHERE a.PatientId IS NULL

UNION ALL

-- Gather all credit model.Adjustments (where PatientId is not null, and InvoiceReceivableId is null), linked to a patient FinancialBatch into dbo.UpdatedPatientFinancialBatchAdjustments
SELECT 
a.Id
,a.PostedDateTime
,ISNULL(f.CheckCode, '''') AS CheckCode
,a.Amount
,f.PaymentMethodId
,a.PatientId AS PatientId
,f.Id AS FinancialBatchId
,a.FinancialSourceTypeId
FROM model.Adjustments a
JOIN model.FinancialBatches f ON f.Id = a.FinancialBatchId
WHERE a.InvoiceReceivableId IS NULL
	AND a.BillingServiceId IS NULL
	AND a.PatientId IS NOT NULL
	AND a.InsurerId IS NULL

-- Gather all model.FinancialInformations, linked to a patient FinancialBatch into dbo.UpdatedPatientFinancialBatchFinancialInformations
-- This is how we associate a FinancialInformation with a FinancialBatch and PatientId
SELECT 
fi.Id
,fi.PostedDateTime
,ISNULL(f.CheckCode, '''') AS CheckCode
,fi.Amount
,f.PaymentMethodId
,i.LegacyPatientReceivablesPatientId AS PatientId
,f.Id AS FinancialBatchId
INTO dbo.UpdatedPatientFinancialBatchFinancialInformations
FROM model.FinancialInformations fi
JOIN model.FinancialBatches f ON f.Id = fi.FinancialBatchId
	AND f.FinancialSourceTypeId = 2
JOIN model.InvoiceReceivables ir ON ir.Id = fi.InvoiceReceivableId
JOIN model.Invoices i ON i.Id = ir.InvoiceId
	AND i.LegacyPatientReceivablesPatientId IS NOT NULL
WHERE fi.FinancialSourceTypeId = 2
	AND fi.PatientId IS NULL

DECLARE @CheckPaymentMethod INT
SET @CheckPaymentMethod = (SELECT TOP 1 Id FROM model.PaymentMethods WHERE Name = ''Check'')

-- Insert the FinancialBatch using the tables we created above. The PatientId of this new FinancialBatch is the LegacyPatientReceivablesPatientId from model.Invoices.
-- We will use the PaymentMethodId check, and existing checkCode. PostedDateTime will now be that of the the Adjustment row.
INSERT INTO model.FinancialBatches (FinancialSourceTypeId, PatientId, PaymentDateTime, Amount, CheckCode, PaymentMethodId, AdjustmentId)
SELECT 
FinancialSourceTypeId
,PatientId
,PostedDateTime
,Amount
,CheckCode
,ISNULL(PaymentMethodId, @CheckPaymentMethod) AS PaymentMethodId
,Id AS AdjustmentId
FROM dbo.UpdatedPatientFinancialBatchAdjustments

-- After insertting the FinancialBatches, update the Adjustment, and set it''s FinancialBatchId.
UPDATE a
SET a.FinancialBatchId = f.Id
FROM model.Adjustments a
JOIN model.FinancialBatches f ON f.AdjustmentId = a.Id

-- Repeat steps above for FinancialInformations.
INSERT INTO model.FinancialBatches (FinancialSourceTypeId, PatientId, PaymentDateTime, Amount, CheckCode, PaymentMethodId, FinancialInformationId)
SELECT 
2 AS FinancialSourceTypeId
,PatientId
,PostedDateTime
,Amount
,CheckCode
,ISNULL(PaymentMethodId, @CheckPaymentMethod) AS PaymentMethodId
,Id AS AdjustmentId
FROM dbo.UpdatedPatientFinancialBatchFinancialInformations

UPDATE fi
SET fi.FinancialBatchId = f.Id
FROM model.FinancialInformations fi
JOIN model.FinancialBatches f ON f.FinancialInformationId = fi.Id

-- Found that there were FinancialBatches where the model.FinancialBatch.FinancialSourceTypeId = 2 (patient) but the
-- model.Adjustment.FinancialSourceTypeId = 1 (insurer). In this case, correct this invalid data by creating a new batch
-- with the FinancialSourceTypeId of the batch = 1 (insurer) and the InsurerId field populated with the 
-- model.Adjustment.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId. These are stored in 
-- dbo.UpdatedPatientFinancialBatchAdjustmentsIncorrect. This also was the case for FinancialInformations. Those were
-- placed into dbo.UpdatedPatientFinancialBatchFinancialInformationsIncorrect.

SELECT 
1 AS FinancialSourceTypeId
,i.InsurerId
,PostedDateTime
,Amount
,''FixedInsurance'' AS CheckCode
,@CheckPaymentMethod AS PaymentMethodId
,a.Id AS AdjustmentId
INTO dbo.UpdatedPatientFinancialBatchAdjustmentsIncorrect
FROM model.Adjustments a
JOIN model.InvoiceReceivables ir ON ir.Id = a.InvoiceReceivableId
	AND ir.PatientInsuranceId IS NOT NULL
JOIN model.PatientInsurances p ON p.Id = ir.PatientInsuranceId
JOIN model.InsurancePolicies i ON i.Id = p.InsurancePolicyId
WHERE FinancialBatchId IN (SELECT DISTINCT FinancialBatchId FROM dbo.UpdatedPatientFinancialBatchAdjustments)

INSERT INTO model.FinancialBatches (FinancialSourceTypeId, InsurerId, PaymentDateTime, Amount, CheckCode, PaymentMethodId, AdjustmentId)
SELECT 
FinancialSourceTypeId
,InsurerId
,PostedDateTime
,Amount
,CheckCode
,ISNULL(PaymentMethodId, @CheckPaymentMethod) AS PaymentMethodId
,AdjustmentId
FROM dbo.UpdatedPatientFinancialBatchAdjustmentsIncorrect

UPDATE a
SET a.FinancialBatchId = f.Id
FROM model.Adjustments a
JOIN model.FinancialBatches f ON f.AdjustmentId = a.Id
JOIN dbo.UpdatedPatientFinancialBatchAdjustmentsIncorrect i ON i.AdjustmentId = a.Id

SELECT 
1 AS FinancialSourceTypeId
,i.InsurerId
,PostedDateTime
,Amount
,''FixedInsurance'' AS CheckCode
,@CheckPaymentMethod AS PaymentMethodId
,fi.Id AS FinancialInformationId
INTO dbo.UpdatedPatientFinancialBatchFinancialInformationsIncorrect
FROM model.FinancialInformations fi
JOIN model.InvoiceReceivables ir ON ir.Id = fi.InvoiceReceivableId
	AND ir.PatientInsuranceId IS NOT NULL
JOIN model.PatientInsurances p ON p.Id = ir.PatientInsuranceId
JOIN model.InsurancePolicies i ON i.Id = p.InsurancePolicyId
WHERE FinancialBatchId IN (SELECT DISTINCT FinancialBatchId FROM dbo.UpdatedPatientFinancialBatchAdjustments)

INSERT INTO model.FinancialBatches (FinancialSourceTypeId, InsurerId, PaymentDateTime, Amount, CheckCode, PaymentMethodId, FinancialInformationId)
SELECT 
FinancialSourceTypeId
,InsurerId
,PostedDateTime
,Amount
,CheckCode
,@CheckPaymentMethod AS PaymentMethodId
,FinancialInformationId
FROM dbo.UpdatedPatientFinancialBatchFinancialInformationsIncorrect

UPDATE fi
SET fi.FinancialBatchId = f.Id
FROM model.FinancialInformations fi
JOIN model.FinancialBatches f ON f.FinancialInformationId = fi.Id
JOIN dbo.UpdatedPatientFinancialBatchFinancialInformationsIncorrect i ON i.FinancialInformationId = fi.Id

-- The following queries can be used to debug this migration if there are issues. If any of the following queries returns
-- rows, then bad data exists and must be corrected/deleted. 
--SELECT * 
--FROM model.Adjustments 
--WHERE FinancialBatchId IN (SELECT DISTINCT FinancialBatchId FROM dbo.UpdatedPatientFinancialBatchAdjustments)

--SELECT * 
--FROM model.FinancialInformations 
--WHERE FinancialBatchId IN (SELECT DISTINCT FinancialBatchId FROM dbo.UpdatedPatientFinancialBatchAdjustments)

--SELECT * 
--FROM model.Adjustments 
--WHERE FinancialBatchId IN (SELECT DISTINCT FinancialBatchId FROM dbo.UpdatedPatientFinancialBatchFinancialInformations)

--SELECT * 
--FROM model.FinancialInformations 
--WHERE FinancialBatchId IN (SELECT DISTINCT FinancialBatchId FROM dbo.UpdatedPatientFinancialBatchFinancialInformations)

-- After we''ve updated all the Adjustments/FinancialInformations delete the old FinancialBatches.
DELETE 
FROM model.FinancialBatches 
WHERE Id IN (SELECT DISTINCT FinancialBatchId FROM dbo.UpdatedPatientFinancialBatchFinancialInformations)
	AND Id NOT IN (SELECT DISTINCT FinancialBatchId FROM model.Adjustments)
	AND Id NOT IN (SELECT DISTINCT FinancialBatchId FROM model.FinancialInformations)

DELETE 
FROM model.FinancialBatches 
WHERE Id IN (SELECT DISTINCT FinancialBatchId FROM dbo.UpdatedPatientFinancialBatchAdjustments)
	AND Id NOT IN (SELECT DISTINCT FinancialBatchId FROM model.Adjustments)
	AND Id NOT IN (SELECT DISTINCT FinancialBatchId FROM model.FinancialInformations)
'

EXEC sys.sp_executesql @sql

-- Drop columns used solely for this migration.
ALTER TABLE model.FinancialBatches 
DROP COLUMN AdjustmentId

ALTER TABLE model.FinancialBatches 
DROP COLUMN FinancialInformationId

END
GO