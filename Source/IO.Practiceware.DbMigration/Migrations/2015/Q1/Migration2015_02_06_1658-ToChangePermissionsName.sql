UPDATE model.Permissions
SET Name = 'Submit Claims' WHERE Name = 'Generate Claims and Statements'

UPDATE model.Permissions
SET Name = 'Statements' WHERE Name = 'Generate Statements'