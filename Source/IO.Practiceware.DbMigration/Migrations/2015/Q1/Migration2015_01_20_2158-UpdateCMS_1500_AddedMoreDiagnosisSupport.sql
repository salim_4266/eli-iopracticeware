﻿IF EXISTS (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = 'CMS-1500'
				AND Content IS NOT NULL
				AND CONVERT(VARCHAR(max), Content) LIKE '%<input type="text" id="DiagnosisI%'
			)
BEGIN	
	DECLARE @content AS NVARCHAR(MAX);
	DECLARE @old VARCHAR(max);
	DECLARE @new VARCHAR(max);
	DECLARE @StartIndex INT;
	DECLARE @EndIndex INT;
	
	SET @new =	'<input type="text" id="DiagnosisI" value="@Model.Diagnosis9.Replace(".","")" style="z-index: 1; left: 50px;  top: @(row15Top+40)px; position: absolute; width: 78px; right: 818px; height: 11px;" />
                <input type="text" id="DiagnosisJ" value="@Model.Diagnosis10.Replace(".","")" style="z-index: 1; left: 200px; top: @(row15Top+40)px; position: absolute; width: 78px; right: 718px; height: 11px;" />
                <input type="text" id="DiagnosisK" value="@Model.Diagnosis11.Replace(".","")" style="z-index: 1; left: 350px; top: @(row15Top+40)px; position: absolute; width: 84px; right: 618px; height: 11px;" />
                <input type="text" id="DiagnosisL" value="@Model.Diagnosis12.Replace(".","")" style="z-index: 1; left: 500px; top: @(row15Top+40)px; position: absolute; width: 84px; right: 518px; height: 11px;" />
'

	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = 'CMS-1500'
				AND Content IS NOT NULL
			)
	SET @StartIndex = CHARINDEX(LOWER('<input type="text" id="DiagnosisI"'), LOWER(@content), 1)

	SET @EndIndex = 0

	IF @StartIndex > 0
	BEGIN
		SET @EndIndex = CHARINDEX('@if (!String.IsNullOrEmpty(@Model.ResubmitCode))', @content, @StartIndex) - 1
		SET @old = SUBSTRING(@content, @StartIndex, @EndIndex - @StartIndex)

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = 'CMS-1500'
			AND Content IS NOT NULL
	END

END
GO
 