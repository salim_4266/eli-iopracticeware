UPDATE ad
SET InvoiceReceivableId = irbs.Id
FROM model.Adjustments ad
INNER JOIN model.InvoiceReceivables irad ON irad.Id = ad.InvoiceReceivableId
INNER JOIN model.BillingServices bs ON bs.Id = ad.BillingServiceId
INNER JOIN model.Invoices invbs ON invbs.Id = bs.InvoiceId
INNER JOIN model.Invoices invir ON invir.Id = irad.Invoiceid
INNER JOIN model.InvoiceReceivables irbs ON irbs.InvoiceId = invbs.Id
WHERE bs.InvoiceId <> irad.InvoiceId
	AND irad.PatientInsuranceId = irbs.PatientInsuranceid