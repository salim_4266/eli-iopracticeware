-- Query to update model.BillingServiceTransactions 'InvoiceReceivableId' which have bad data
UPDATE bst
SET bst.InvoiceReceivableId = irbs.Id
FROM model.BillingServiceTransactions bst
INNER JOIN model.InvoiceReceivables irad ON irad.Id = bst.InvoiceReceivableId
INNER JOIN model.BillingServices bs ON bs.Id = bst.BillingServiceId
INNER JOIN model.Invoices invbs ON invbs.Id = bs.InvoiceId
INNER JOIN model.Invoices invir ON invir.Id = irad.Invoiceid
INNER JOIN model.InvoiceReceivables irbs ON irbs.InvoiceId = invbs.Id
WHERE bs.InvoiceId <> irad.InvoiceId

-- Query to update model.Adjustments 'InvoiceReceivableId' which have bad data
UPDATE ad
SET ad.InvoiceReceivableId = irbs.Id
FROM model.Adjustments ad
INNER JOIN model.InvoiceReceivables irad ON irad.Id = ad.InvoiceReceivableId
INNER JOIN model.BillingServices bs ON bs.Id = ad.BillingServiceId
INNER JOIN model.Invoices invbs ON invbs.Id = bs.InvoiceId
INNER JOIN model.Invoices invir ON invir.Id = irad.Invoiceid
INNER JOIN model.InvoiceReceivables irbs ON irbs.InvoiceId = invbs.Id
WHERE bs.InvoiceId <> irad.InvoiceId	

-- Query to update model.FinancialInformations 'InvoiceReceivableId' which have bad data
UPDATE fi
SET fi.InvoiceReceivableId = irbs.Id
FROM model.FinancialInformations fi
INNER JOIN model.InvoiceReceivables irad ON irad.Id = fi.InvoiceReceivableId
INNER JOIN model.BillingServices bs ON bs.Id = fi.BillingServiceId
INNER JOIN model.Invoices invbs ON invbs.Id = bs.InvoiceId
INNER JOIN model.Invoices invir ON invir.Id = irad.Invoiceid
INNER JOIN model.InvoiceReceivables irbs ON irbs.InvoiceId = invbs.Id
WHERE bs.InvoiceId <> irad.InvoiceId