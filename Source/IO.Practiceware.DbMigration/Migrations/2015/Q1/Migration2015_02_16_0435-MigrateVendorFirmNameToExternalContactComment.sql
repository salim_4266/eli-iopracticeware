﻿IF OBJECT_ID(N'dbo.PracticeVendorsBackup') IS NOT NULL
BEGIN
	UPDATE ec SET ec.Comment = pvb.VendorFirmName
	FROM model.ExternalContacts ec
	JOIN dbo.PracticeVendorsBackup pvb ON pvb.VendorId = ec.Id
	WHERE pvb.VendorFirmName IS NOT NULL
END
GO
