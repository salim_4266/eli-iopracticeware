﻿IF EXISTS (
	SELECT * FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	
	DECLARE @content AS NVARCHAR(MAX);
	DECLARE @old VARCHAR(max);
	DECLARE @new VARCHAR(max);
	DECLARE @StartIndex INT;
	DECLARE @EndIndex INT;
	
	SET @new =	'<input type="text" id="DiagnosisLetters" value="@service.DiagnosisLetters" class="smallText" style="font-size:9px;z-index: 1; top: @(beginTop + (index * incrementalPixels))px; position: absolute; width: 85px; left: 525px; height: 14px;" />
'

	SET @Content = (
			SELECT CONVERT(VARCHAR(max), Content)
			FROM model.TemplateDocuments
			WHERE DisplayName = 'CMS-1500'
				AND Content IS NOT NULL
			)
	SET @StartIndex = CHARINDEX(LOWER('<input type="text" id="DiagnosisPointerA"'), LOWER(@content), 1)

	SET @EndIndex = 0

	IF @StartIndex > 0
	BEGIN
		SET @EndIndex = CHARINDEX('<input type="text" id="ServiceCharge"', @content, @StartIndex) - 1
		SET @old = SUBSTRING(@content, @StartIndex, @EndIndex - @StartIndex)

		UPDATE model.TemplateDocuments
		SET Content = CONVERT(VARBINARY(max), REPLACE(CONVERT(VARCHAR(max), Content), @old, @new))
		FROM model.TemplateDocuments
		WHERE DisplayName = 'CMS-1500'
			AND Content IS NOT NULL
	END

END
GO
 