﻿DECLARE @MinBilledAmount AS NVARCHAR(100)
DECLARE @BillingCycle AS NVARCHAR(100)

SELECT TOP 1 @MinBilledAmount = CAST(ISNULL(FieldValue, 0) AS NVARCHAR)
FROM dbo.PracticeInterfaceConfiguration
WHERE FieldReference = 'STATEMENTLOWERLIMIT'

SELECT TOP 1 @BillingCycle = CAST(ISNULL(FieldValue, 0) AS NVARCHAR)
FROM dbo.PracticeInterfaceConfiguration
WHERE FieldReference = 'STATEMENTRANGE'

IF @MinBilledAmount IS NULL OR @MinBilledAmount = ''
	SET @MinBilledAmount= '0'
IF @BillingCycle IS NULL OR @BillingCycle = ''
	SET @BillingCycle ='0'

--Creating Filter for Newly Queued Statements
IF NOT EXISTS (
		SELECT *
		FROM model.ApplicationSettings
		WHERE Value LIKE '%<d1p1:Name z:Id="2">Newly Queued Statement(s) (Patient Statements)</d1p1:Name>%'
			AND Name = 'StatementsFilter'
		)
BEGIN
	INSERT INTO model.applicationsettings (
		Value
		,Name
		)
	VALUES (
		'<z:anyType xmlns:z="http://schemas.microsoft.com/2003/10/Serialization/" xmlns:d1p1="http://schemas.datacontract.org/2004/07/IO.Practiceware.Presentation.ViewModels.Statements" xmlns:i="http://www.w3.org/2001/XMLSchema-instance" z:Id="1" i:type="d1p1:StatementsFilter">
			<d1p1:BillingCycleDays>' + @BillingCycle + '</d1p1:BillingCycleDays>
			<d1p1:BillingOrganizationIds xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" i:nil="true" />
			<d1p1:MinimumAmount>' + @MinBilledAmount + 
				'</d1p1:MinimumAmount>
			<d1p1:Name z:Id="2">Newly Queued Statement(s) (Patient Statements)</d1p1:Name>
			<d1p1:PatientLastNameFrom i:nil="true" />
			<d1p1:PatientLastNameTo i:nil="true" />
			<d1p1:RenderingProvideIds xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" z:Id="3" z:Size="0" />
			<d1p1:ServiceLocationIds xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" z:Id="4" z:Size="0" />
			<d1p1:StatmentCommunicationPreferenceIds xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" z:Id="5" z:Size="1">
				<d2p1:int>0</d2p1:int>
			</d1p1:StatmentCommunicationPreferenceIds>
			<d1p1:TransactionStatusIds xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" z:Id="6" z:Size="1">
				<d2p1:int>1</d2p1:int>
			</d1p1:TransactionStatusIds>
		</z:anyType>'
		,'StatementsFilter'
		)
END

--Creating Filter for Aggregate Statements
IF NOT EXISTS (
		SELECT *
		FROM model.ApplicationSettings
		WHERE Value LIKE '%<d1p1:Name z:Id="2">Current Opened Aggregate Statement(s) (Monthly Statements)</d1p1:Name>%'
			AND Name = 'StatementsFilter'
		)
BEGIN
	INSERT INTO model.applicationsettings (
		Value
		,Name
		)
	VALUES (
		'<z:anyType xmlns:z="http://schemas.microsoft.com/2003/10/Serialization/" xmlns:d1p1="http://schemas.datacontract.org/2004/07/IO.Practiceware.Presentation.ViewModels.Statements" xmlns:i="http://www.w3.org/2001/XMLSchema-instance" z:Id="1" i:type="d1p1:StatementsFilter">
			<d1p1:BillingCycleDays>' + @BillingCycle + '</d1p1:BillingCycleDays>
			<d1p1:BillingOrganizationIds xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" i:nil="true" />
			<d1p1:MinimumAmount>' + @MinBilledAmount + 
				'</d1p1:MinimumAmount>
			<d1p1:Name z:Id="2">Current Opened Aggregate Statement(s) (Monthly Statements)</d1p1:Name>
			<d1p1:PatientLastNameFrom i:nil="true" />
			<d1p1:PatientLastNameTo i:nil="true" />
			<d1p1:RenderingProvideIds xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" z:Id="3" z:Size="0" />
			<d1p1:ServiceLocationIds xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" z:Id="4" z:Size="0" />
			<d1p1:StatmentCommunicationPreferenceIds xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" z:Id="5" z:Size="1">
				<d2p1:int>0</d2p1:int>
			</d1p1:StatmentCommunicationPreferenceIds>
			<d1p1:TransactionStatusIds xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" z:Id="6" z:Size="1">
				<d2p1:int>2</d2p1:int>
			</d1p1:TransactionStatusIds>
		</z:anyType>'
		,'StatementsFilter'
		)
END