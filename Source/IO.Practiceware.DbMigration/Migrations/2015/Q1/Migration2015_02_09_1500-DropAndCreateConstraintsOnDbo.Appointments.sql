﻿IF EXISTS(SELECT OBJECT_NAME(OBJECT_ID) AS NameofConstraint
	FROM sys.objects
	WHERE SCHEMA_NAME(schema_id) = 'dbo' AND OBJECT_NAME(parent_object_id) = 'Appointments'
	AND type_desc IN ('CHECK_CONSTRAINT', 'DEFAULT_CONSTRAINT') AND OBJECT_NAME(OBJECT_ID) NOT LIKE '%MSrepl%')
BEGIN 
	DECLARE @ConstraintName NVARCHAR(200)

	DECLARE ContraintsCursor CURSOR FOR
	SELECT OBJECT_NAME(OBJECT_ID) AS NameofConstraint
		FROM sys.objects
		WHERE SCHEMA_NAME(schema_id) = 'dbo' AND OBJECT_NAME(parent_object_id) = 'Appointments'
		AND type_desc IN ('CHECK_CONSTRAINT', 'DEFAULT_CONSTRAINT') AND OBJECT_NAME(OBJECT_ID) NOT LIKE '%MSrepl%'

	OPEN ContraintsCursor FETCH NEXT FROM ContraintsCursor INTO @ConstraintName
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		EXEC('ALTER TABLE dbo.Appointments DROP CONSTRAINT ' + @ConstraintName +'')
		FETCH NEXT FROM ContraintsCursor INTO @ConstraintName
	END
	CLOSE ContraintsCursor   
	DEALLOCATE ContraintsCursor
END

ALTER TABLE [dbo].[Appointments]  
WITH NOCHECK 
ADD CONSTRAINT [CK_Appointments_AppDate_Check] 
CHECK NOT FOR REPLICATION ((isdate([AppDate])=(1) AND CONVERT([nvarchar],CONVERT([datetime],[AppDate],(112)),(112))=[AppDate] OR [dbo].[GetNoCheck]()=(1)))
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_AppDate] DEFAULT ('') FOR [AppDate]
GO

ALTER TABLE [dbo].[Appointments]  
WITH NOCHECK 
ADD CONSTRAINT [CK_Appointments_SetDate_Check] 
CHECK NOT FOR REPLICATION ((isdate([SetDate])=(1) AND CONVERT([nvarchar],CONVERT([datetime],[SetDate],(112)),(112))=[SetDate] OR ([SetDate]='' OR [SetDate] IS NULL) OR [dbo].[GetNoCheck]()=(1)))
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_SetDate] DEFAULT ('') FOR [SetDate]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_Comments] DEFAULT ('') FOR [Comments]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ActivityStatus] DEFAULT ('') FOR [ActivityStatus]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_AppTime] DEFAULT (0) FOR [AppTime]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ApptInsType] DEFAULT ('') FOR [ApptInsType]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ApptTypeCat] DEFAULT ('') FOR [ApptTypeCat]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_AppTypeId] DEFAULT (0) FOR [AppTypeId]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ConfirmStatus] DEFAULT ('') FOR [ConfirmStatus]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_Duration] DEFAULT (0) FOR [Duration]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_PatientId] DEFAULT (0) FOR [PatientId]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_PreCertId] DEFAULT (0) FOR [PreCertId]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ResourceId1] DEFAULT (0) FOR [ResourceId1]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ResourceId2]  DEFAULT (0) FOR [ResourceId2]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ResourceId3] DEFAULT (0) FOR [ResourceId3]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ResourceId4] DEFAULT (0) FOR [ResourceId4]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ResourceId5] DEFAULT (0) FOR [ResourceId5]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ResourceId6] DEFAULT (0) FOR [ResourceId6]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ResourceId7] DEFAULT (0) FOR [ResourceId7]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ResourceId8] DEFAULT (0) FOR [ResourceId8]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_ScheduleStatus] DEFAULT ('') FOR [ScheduleStatus]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_TechApptTypeId] DEFAULT (0) FOR [TechApptTypeId]
GO

ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD CONSTRAINT [CK_Appointments_VisitReason] DEFAULT ('') FOR [VisitReason]
GO
