﻿IF EXISTS(SELECT * FROM sys.Objects WHERE Name = 'ResourceResourceTypeDoctorTrigger')
BEGIN 
	ALTER TABLE [dbo].[Resources] DISABLE TRIGGER [ResourceResourceTypeDoctorTrigger]
END

UPDATE [dbo].[Resources]
SET [ResourceType] = 'A'
WHERE [ResourceName] LIKE '%-Auto'

IF EXISTS(SELECT * FROM sys.Objects WHERE Name = 'ResourceResourceTypeDoctorTrigger')
BEGIN 
	ALTER TABLE [dbo].[Resources] ENABLE TRIGGER [ResourceResourceTypeDoctorTrigger]
END
