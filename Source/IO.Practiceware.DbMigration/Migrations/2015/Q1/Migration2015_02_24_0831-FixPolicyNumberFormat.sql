--Bug #16929.
IF OBJECT_ID('tempdb..#tmpInsurers') IS NOT NULL
BEGIN
	DROP TABLE #tmpInsurers
END

--Get Back all rows which are not empty
SELECT PolicyNumberFormat
	,Id
INTO #tmpInsurers
FROM model.Insurers
WHERE PolicyNumberFormat IS NOT NULL
	OR PolicyNumberFormat <> ''

IF OBJECT_ID('tempdb..#tmpInsurersWithRegEx') IS NOT NULL
BEGIN
	DROP TABLE #tmpInsurersWithRegEx
END

--Find the regexFormat Rows
SELECT *
INTO #tmpInsurersWithRegEx
FROM #tmpInsurers
WHERE (
		PolicyNumberFormat LIKE '%a-z%'
		OR PolicyNumberFormat LIKE '%A-Z%'
		OR PolicyNumberFormat LIKE '%\d%'
		OR PolicyNumberFormat LIKE '%\w%'
		OR PolicyNumberFormat LIKE '%0-9%'
		OR PolicyNumberFormat LIKE '%\b%'
		OR PolicyNumberFormat LIKE '%$'
		OR PolicyNumberFormat LIKE '^%'
		)

--Update RegularExpression already with default sample format
--In First Pattern Exculde '{'.So that it can handled separately.For example :\b\d\d\d\d\d\d\d\d\d[a-zA-Z][\w]?\b ,	\b[a-zA-Z][\w]?\d\d\d\d\d[\w]?[\w]?[\w]?[\w]?\b
UPDATE model.Insurers
SET PolicyNumberFormat = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PolicyNumberFOrmat, '\b', ''), 'a-zA-Z', 'A'), '\d', 'N'), '\w', 'E'), '?', 'O'), '[', ''), ']', '')
WHERE Id IN (
		SELECT Id
		FROM #tmpInsurersWithRegEx
		WHERE PolicyNumberFormat NOT LIKE '%{%'
		)

--Second Pattern Inculde rows with '{' and '}'.For example :[A-Za-z]{2}[0-9]{9}$ , ^[0-9]{9}[A-Za-z]{1}[A-Za-z0-9]{0,1}$
SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PolicyNumberFormat, '^', ''), 'a-zA-z0-9', 'E'), '0-9', 'N'), 'a-zA-Z', 'A'), '{0,1}', 'O'), '[', ''), ']', ''), '$', '') AS NewValue
	,PolicyNumberFormat AS oldValue
	,Id
INTO #PatternTwo
FROM model.Insurers
WHERE PolicyNumberFOrmat <> ''
	AND PolicyNumberFormat IS NOT NULL
	AND PolicyNumberFormat LIKE '%{%'

DECLARE @PatternIndex AS INT
DECLARE @PolicyNumberFormat AS NVARCHAR(MAX)
DECLARE @CharIndex AS INT
DECLARE @Repetitions AS INT
DECLARE @RepStr AS VARCHAR(MAX)
DECLARE @Id AS INT

DECLARE ToUpdatePolicyNUmberFormatCursor CURSOR
FOR
SELECT NewValue
	,Id
FROM #PatternTwo

OPEN ToUpdatePolicyNUmberFormatCursor

FETCH NEXT
FROM ToUpdatePolicyNUmberFormatCursor
INTO @PolicyNumberFormat
	,@Id

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @PolicyNumberFormat = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@PolicyNumberFormat, '\b', ''), '\d', 'N'), '\w', 'E'), '?', 'O'), '[', ''), ']', '')

	SET @CharIndex = CHARINDEX('{', @PolicyNumberFormat);

	WHILE (@CharIndex > 0)
	BEGIN
		SELECT @Repetitions = dbo.ExtractTextValue('{', @PolicyNumberFormat, '}') --Find the repetition number
		SET @RepStr = REPLICATE(SUBSTRING(@PolicyNumberFormat, @CharIndex - 1, 1), @Repetitions) --Build the repetition string.For example : N{9} with NNNNNNNNN
		SET @PolicyNumberFormat = LEFT(@PolicyNumberFormat, @CharIndex - 2) + @RepStr + RIGHT(@PolicyNumberFormat, (LEN(@PolicyNumberFormat) - @CharIndex - 2))
		SET @CharIndex = CHARINDEX('{', @PolicyNumberFormat);
	END

	UPDATE model.Insurers
	SET PolicyNumberFormat = @PolicyNumberFormat
	WHERE ID = @Id --Update the new format

	FETCH NEXT
	FROM ToUpdatePolicyNUmberFormatCursor
	INTO @PolicyNumberFormat
		,@Id
END

CLOSE ToUpdatePolicyNUmberFormatCursor

DEALLOCATE ToUpdatePolicyNUmberFormatCursor

--Remove the regex format rows from tempTable
DELETE
FROM #tmpInsurers
WHERE Id IN (
		SELECT ID
		FROM #tmpInsurersWithRegEx
		)

--Exclude 'ANOE' Format too
DELETE
FROM #tmpInsurers
WHERE PolicyNumberFormat LIKE '%[ANEO]%'

--Make Null's the rest if any
UPDATE model.Insurers
SET PolicyNumberFormat = ''
WHERE Id IN (
		SELECT Id
		FROM #tmpInsurers
		)