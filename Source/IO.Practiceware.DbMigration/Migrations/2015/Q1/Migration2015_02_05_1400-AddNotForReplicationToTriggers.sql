﻿DECLARE commands CURSOR
FOR
SELECT def
FROM (	
	SELECT REPLACE(OBJECT_DEFINITION(t.object_id), 'CREATE TRIGGER', 'ALTER TRIGGER') AS def
		,t.object_id
		,t.NAME
	FROM sys.triggers t
	WHERE t.NAME NOT LIKE 'ON%'
		AND t.name <> 'PracticeActivityMultipleActivityRecordsTrigger'
		AND t.is_not_for_replication = 0
	) AS d

DECLARE @cmd VARCHAR(max)

OPEN commands

FETCH NEXT
FROM commands
INTO @cmd

WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE @NotForReplicationStringIndex INT

	SELECT @NotForReplicationStringIndex = CHARINDEX('NOT FOR REPLICATION', @cmd, 1)

	IF (@NotForReplicationStringIndex = 0)
	BEGIN


	-- crlfAScrlf
		IF (CHARINDEX('
AS
', @cmd) > 0)
			SELECT @cmd = STUFF(@cmd, CHARINDEX('
AS
', @cmd), LEN('
AS
'), '
NOT FOR REPLICATION 
AS
')

--crlfAS crlf
		ELSE IF (CHARINDEX('
AS 
', @cmd) > 0)
			SELECT @cmd = STUFF(@cmd, CHARINDEX('
AS 
', @cmd), LEN('
AS 
'), '
NOT FOR REPLICATION
AS
')


--AS BEGINcrlf
		ELSE IF (CHARINDEX('AS BEGIN
', @cmd) > 0)
			SELECT @cmd = STUFF(@cmd, CHARINDEX('AS BEGIN
', @cmd), LEN('AS BEGIN
'), '
NOT FOR REPLICATION 
AS
BEGIN
')


--AS crlf
		ELSE IF (CHARINDEX('AS 
', @cmd) > 0)
			SELECT @cmd = STUFF(@cmd, CHARINDEX('AS 
', @cmd), LEN('AS 
'), '
NOT FOR REPLICATION 
AS
')

	--AScrlf
		ELSE IF (CHARINDEX('AS
', @cmd) > 0)
			SELECT @cmd = STUFF(@cmd, CHARINDEX('AS
', @cmd), LEN('AS
'), '
NOT FOR REPLICATION 
AS
')


	END

	EXEC (@cmd)

	FETCH NEXT
	FROM commands
	INTO @cmd
END

CLOSE commands

DEALLOCATE commands
GO

