﻿SET NOCOUNT ON;
GO

IF OBJECT_ID('tempdb..#NoAudit','U') IS NOT NULL
	DROP TABLE #NoAudit
GO

SELECT 1 AS Value INTO #NoAudit
GO

-- For every patient, gather their first BillingService and InvoiceReceivable associated with that BillingService.
IF OBJECT_ID('tempdb..#PrimaryBillingServices','U') IS NOT NULL
	DROP TABLE #PrimaryBillingServices
GO

-- RAISERROR('Started inserting #PrimaryBillingServices', 1, 1) WITH NOWAIT
SELECT DISTINCT 
* 
INTO #PrimaryBillingServices
FROM (
	SELECT 
	(SELECT MIN(ir.Id) FROM model.InvoiceReceivables ir WHERE ir.InvoiceId = i.InvoiceId AND ir.PatientInsuranceId IS NULL) AS InvoiceReceivableId,
	(SELECT TOP 1 bs.Id FROM model.BillingServices bs WHERE bs.InvoiceId = i.InvoiceId ORDER BY bs.OrdinalId DESC) AS BillingServiceId,
	ptj.TransactionDate,
	ptj.TransactionId
	FROM dbo.PracticeTransactionJournal ptj
	JOIN (
		SELECT MIN(i.Id) InvoiceId, p.Id AS PatientId
		FROM model.Invoices i 
		JOIN model.Encounters e ON e.Id = i.EncounterId
		JOIN model.Patients p ON p.Id = e.PatientId
		GROUP BY p.Id
	) i ON ptj.TransactionRcvrId = i.PatientId
	WHERE ptj.TransactionType = 'R'
		AND ptj.TransactionRef = 'G'
	) q 
WHERE q.InvoiceReceivableId IS NOT NULL AND q.BillingServiceId IS NOT NULL
-- RAISERROR('Finished inserting #PrimaryBillingServices', 1, 1) WITH NOWAIT
GO

-- Insert BillingServiceTransactions to be used, for AggregateStatements.
-- Only 1 BST is needed.
DECLARE @BillingServiceTransactions TABLE (Id UNIQUEIDENTIFIER NOT NULL, TransactionId INT NOT NULL)
-- RAISERROR('Started inserting BillingServiceTransactions', 1, 1) WITH NOWAIT
INSERT INTO [model].[BillingServiceTransactions] (
	[DateTime]
	,AmountSent
	,BillingServiceTransactionStatusId
	,MethodSentId
	,BillingServiceId
	,InvoiceReceivableId
	,LegacyTransactionId)
OUTPUT inserted.Id, inserted.LegacyTransactionId INTO @BillingServiceTransactions(Id, TransactionId)
SELECT 
pbs.TransactionDate AS [DateTime]
,0 AS AmountSent
,5 AS BillingServiceTransactionStatusId -- ClosedNotSent
,2 AS MethodSentId -- Paper
,pbs.BillingServiceId 
,pbs.InvoiceReceivableId
,pbs.TransactionId
FROM #PrimaryBillingServices pbs
-- RAISERROR('Finished inserting BillingServiceTransactions', 1, 1) WITH NOWAIT
DECLARE @ExternalSystemExternalSystemMessageTypeId INT
SET @ExternalSystemExternalSystemMessageTypeId = (
	SELECT TOP 1 Id
	FROM model.ExternalSystemExternalSystemMessageTypes
	WHERE ExternalSystemId = (SELECT TOP 1 Id FROM model.ExternalSystems WHERE Name = 'PatientStatement')
		AND ExternalSystemMessageTypeId = (SELECT TOP 1 Id FROM model.ExternalSystemMessageTypes WHERE Name = 'PatientStatement')
)

DECLARE @ExternalSystemMessages TABLE (ExternalSystemMessageId UNIQUEIDENTIFIER NOT NULL, [TransactionId] INT NOT NULL)
-- RAISERROR('Started inserting ExternalSystemMessages', 1, 1) WITH NOWAIT

INSERT INTO [model].[ExternalSystemMessages]
	([Description]
	,Value
	,ExternalSystemExternalSystemMessageTypeId
	,ExternalSystemMessageProcessingStateId
	,ProcessingAttemptCount
	,CreatedBy
	,CreatedDateTime
	,UpdatedDateTime)
OUTPUT inserted.Id, inserted.[Description] INTO @ExternalSystemMessages (ExternalSystemMessageId, TransactionId)
SELECT 
pbs.TransactionId AS [Description]
,'' AS Value
,@ExternalSystemExternalSystemMessageTypeId AS ExternalSystemExternalSystemMessageTypeId
,3 AS ExternalSystemMessageProcessingStateId
,1 AS ProcessingAttemptCount
,'IO.Practiceware.DbMigration' AS CreatedBy
,CONVERT(DATETIME,pbs.TransactionDate) AS CreatedDateTime
,CONVERT(DATETIME,pbs.TransactionDate) AS UpdatedDateTime
FROM #PrimaryBillingServices pbs

-- RAISERROR('Finished inserting ExternalSystemMessages', 1, 1) WITH NOWAIT
DECLARE @PracticeRepositoryEntityId INT
SET @PracticeRepositoryEntityId = (
	SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE Name = 'BillingServiceTransaction'
)

-- RAISERROR('Started preparing to insert ExternalSystemMessagePracticeRepositoryEntities', 1, 1) WITH NOWAIT
ALTER TABLE model.ExternalSystemMessagePracticeRepositoryEntities DISABLE TRIGGER ALL;
ALTER TABLE model.ExternalSystemMessagePracticeRepositoryEntities NOCHECK CONSTRAINT ALL;
ALTER INDEX ALL ON model.ExternalSystemMessagePracticeRepositoryEntities
REBUILD WITH (FILLFACTOR = 80, SORT_IN_TEMPDB = ON, STATISTICS_NORECOMPUTE = ON);

-- RAISERROR('Finished preparing to insert ExternalSystemMessagePracticeRepositoryEntities', 1, 1) WITH NOWAIT

-- RAISERROR('Started inserting ExternalSystemMessagePracticeRepositoryEntities', 1, 1) WITH NOWAIT

INSERT INTO model.ExternalSystemMessagePracticeRepositoryEntities(
	ExternalSystemMessageId
	,PracticeRepositoryEntityId
	,PracticeRepositoryEntityKey)
SELECT
e.Id AS ExternalSystemMessageId
,@PracticeRepositoryEntityId AS PracticeRepositoryEntityId
,bst.Id AS PracticeRepositoryEntityKey
FROM model.ExternalSystemMessages e
JOIN #PrimaryBillingServices pbs ON pbs.TransactionId = e.[Description]
JOIN model.BillingServiceTransactions bst ON pbs.TransactionId = bst.LegacyTransactionId
WHERE bst.Id IN (SELECT Id FROM @BillingServiceTransactions)
	AND e.Id IN (SELECT ExternalSystemMessageId FROM @ExternalSystemMessages)

-- RAISERROR('Finished inserting ExternalSystemMessagePracticeRepositoryEntities', 1, 1) WITH NOWAIT

ALTER TABLE model.ExternalSystemMessagePracticeRepositoryEntities CHECK CONSTRAINT ALL;
ALTER TABLE model.ExternalSystemMessagePracticeRepositoryEntities ENABLE TRIGGER ALL;

-- RAISERROR('Started updating ExternalSystemMessages', 1, 1) WITH NOWAIT
UPDATE e
SET e.[Description] = 'AggregateStatements'
FROM model.ExternalSystemMessages e
JOIN @ExternalSystemMessages esm ON esm.ExternalSystemMessageId = e.Id
GO
-- RAISERROR('Finished updating ExternalSystemMessages', 1, 1) WITH NOWAIT

IF OBJECT_ID('tempdb..#PrimaryBillingServices','U') IS NOT NULL
	DROP TABLE #PrimaryBillingServices
GO

IF OBJECT_ID('tempdb..#NoAudit','U') IS NOT NULL
	DROP TABLE #NoAudit
GO

SET NOCOUNT OFF;
GO