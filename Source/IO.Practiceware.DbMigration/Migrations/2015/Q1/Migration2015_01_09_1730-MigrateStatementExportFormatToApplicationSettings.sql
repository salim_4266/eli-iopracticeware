﻿IF NOT EXISTS(SELECT * FROM model.ApplicationSettings WHERE Name = 'StatementExportFormat')
BEGIN
	INSERT INTO model.ApplicationSettings(Name, Value)
	SELECT 'StatementExportFormat', 
		CASE 
			WHEN picB.FieldValue = 'F' 
				THEN '1'
			WHEN picA.FieldValue = 'T' AND picC.FieldValue = 'T'
				THEN '3'
			ELSE '2'
			END AS MonthlyStatementExportFormat
	FROM dbo.PracticeInterfaceConfiguration picA
	LEFT JOIN dbo.PracticeInterfaceConfiguration picB ON picB.FieldReference = 'NEWEXPORT'
	LEFT JOIN dbo.PracticeInterfaceConfiguration picC ON picC.FieldReference = 'EXPORTAGINGON'
	WHERE picA.FieldReference = 'EXPORTRECALLDOCLOCON'
END