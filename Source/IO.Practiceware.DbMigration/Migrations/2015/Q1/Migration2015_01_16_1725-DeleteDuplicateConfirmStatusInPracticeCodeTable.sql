﻿-- This is to delete duplicate CONFIRMSTATUS in the Practice Code Table
DECLARE @duplicates table (Id int) 
INSERT INTO @duplicates SELECT v.id
FROM(SELECT pct1.Code AS Code, pct1.id AS Id
	FROM PracticeCodeTable pct1
	INNER JOIN PracticeCodeTable pct2 ON SUBSTRING(pct1.Code, 1, 1) = SUBSTRING(pct2.Code, 1, 1)
		AND pct2.ReferenceType = 'CONFIRMSTATUS' 
	WHERE pct1.ReferenceType like 'CONFIRMSTATUS' 
	) v
GROUP BY v.code, v.id
HAVING COUNT(*) >1

DELETE 
FROM PracticeCodeTable
WHERE Id = (SELECT MAX(Id) FROM @duplicates)
GO

--Prevent entry of duplicate Codes starting with the same letter
IF OBJECT_ID('PracticeCodeTableConfirmationStatusCodeTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeCodeTableConfirmationStatusCodeTrigger')
END

GO

CREATE TRIGGER PracticeCodeTableConfirmationStatusCodeTrigger
ON PracticeCodeTable
FOR UPDATE, INSERT
NOT FOR REPLICATION
AS
BEGIN
SET NOCOUNT ON
IF (
		(
			(
				SELECT COUNT(*) 
				FROM PracticeCodeTable pc WITH(NOLOCK) 
				INNER JOIN (
								SELECT * 
								FROM inserted 
								
								EXCEPT 
								
								SELECT * FROM deleted
								
				) i  ON SUBSTRING(i.Code, 1, 1) = SUBSTRING(pc.Code, 1, 1) 
								AND pc.ReferenceType = 'CONFIRMSTATUS' 
								WHERE i.ReferenceType = 'CONFIRMSTATUS' 
								GROUP BY i.Code 
								HAVING COUNT(*) > 1
			) > 1
		) 
	AND dbo.GetNoCheck() = 0
)
	
BEGIN
	RAISERROR ('Cannot insert duplicate payment types.',16,1)
	ROLLBACK TRANSACTION
END

END