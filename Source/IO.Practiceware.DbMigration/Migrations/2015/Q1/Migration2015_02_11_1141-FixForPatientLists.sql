DECLARE @RawXml AS XML
DECLARE @NodeValue AS NVARCHAR(MAX)
SELECT TOP 1 @RawXml = CAST(RepSelection AS XML)
FROM REPORTFILTERS

IF @RawXml IS NOT NULL
BEGIN
	--To remove ShowCommunication tag
	IF @Rawxml.exist('/root//SelectedItemsTB//ShowCommunication') = 1
	BEGIN
		SET @RawXml.modify('delete (/root//SelectedItemsTB//ShowCommunication)')
	END

	--To add CommunicationPreference tag
	IF @Rawxml.exist('/root//Patient//CommunicationPreference') = 0
	BEGIN
		SET @Rawxml.modify('insert <CommunicationPreference>{//patient//languageQstr//node()}</CommunicationPreference> after (//patient//languageQstr)[1]')
		SET @Rawxml.modify('insert <CommunicationPreferenceQstr>{//patient//CommunicationPreference//node()}</CommunicationPreferenceQstr> after (//patient//CommunicationPreference)[1]')
	END

	--To rename ICDList to  ProblemsList tag
	IF @Rawxml.exist('/root//encounter//IcdList') = 1
	BEGIN
		SET @Rawxml.modify('insert <ProblemsList>{//encounter//IcdList//node()}</ProblemsList> after (//encounter//CptListQstr)[1]')
		SET @Rawxml.modify('insert <ProblemsListQstr>{//encounter//IcdListQstr//node()}</ProblemsListQstr> after (//encounter//ProblemsList)[1]')
		
		SET @NodeValue = @RawXml.value('(/root//encounter//IcdList//node())[1]','VARCHAR(MAX)')
		IF @NodeValue IS NOT NULL AND @NodeValue <> ''
		BEGIN
			SET @NodeValue = REPLACE(@NodeValue,'ICD','PROBLEM')
			SET @Rawxml.modify('replace value of (/root//encounter//ProblemsList//text())[1] with sql:variable("@NodeValue")')
		END

		SET @NodeValue = @RawXml.value('(/root//encounter//IcdListQstr//node())[1]','VARCHAR(MAX)')
		IF @NodeValue IS NOT NULL AND @NodeValue <> ''
		BEGIN
			SET @NodeValue = REPLACE(@NodeValue,'ICD','PROBLEMS')
			SET @Rawxml.modify('replace value of (/root//encounter//ProblemsListQstr//text())[1] with sql:variable("@NodeValue")')
		END

		SET @RawXml.modify('delete (/root//IcdList)')
		SET @RawXml.modify('delete (/root//IcdListQstr)')
		SET @RawXml.modify('delete (/root//IcdListQstr)')
	END

	IF @Rawxml.exist('/root//SelectedItemsTB//SelectedItems') = 1
	BEGIN
		SET @NodeValue = @RawXml.value('(/root//SelectedItemsTB//SelectedItems//node())[1]','VARCHAR(MAX)')
		IF @NodeValue IS NOT NULL AND @NodeValue <> ''
		BEGIN
			SET @NodeValue = REPLACE(@NodeValue,'ICD','Problems')
			SET @Rawxml.modify('replace value of (/root//SelectedItemsTB//SelectedItems//text())[1] with sql:variable("@NodeValue")')
		END
	END
	--Consider only TOP 1 leave the rest as it is.
	UPDATE REPORTFILTERS
	SET RepSelection = CAST(@RawXml AS NVARCHAR(MAX))
	WHERE RepFilterID IN (
			SELECT TOP 1 RepFilterID
			FROM REPORTFILTERS
			)
END
