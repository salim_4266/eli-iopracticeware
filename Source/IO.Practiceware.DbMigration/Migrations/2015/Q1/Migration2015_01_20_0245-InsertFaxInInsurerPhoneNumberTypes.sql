﻿SET IDENTITY_INSERT model.InsurerPhoneNumberTypes ON

IF NOT EXISTS (SELECT Id FROM model.InsurerPhoneNumberTypes WHERE Id = 7)
	INSERT INTO model.InsurerPhoneNumberTypes (Id, Name, Abbreviation, IsArchived) VALUES (7,'Fax','',0)
ELSE 
	UPDATE model.InsurerPhoneNumberTypes SET Name = 'Fax', Abbreviation = '', IsArchived = 0 WHERE Id = 7

DELETE FROM model.InsurerPhoneNumberTypes WHERE Id > 7

SET IDENTITY_INSERT model.InsurerPhoneNumberTypes OFF

DBCC CHECKIDENT ('[model].[InsurerPhoneNumberTypes]', 'RESEED', 1000)

