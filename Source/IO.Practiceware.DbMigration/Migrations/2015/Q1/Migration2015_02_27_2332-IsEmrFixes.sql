﻿-- TFS 14786
IF EXISTS (
	SELECT * FROM sys.triggers 
	WHERE name = 'ProvidersUpdateResources'
	AND parent_id = OBJECT_ID('dbo.Resources','U'))
BEGIN
	DROP TRIGGER [ProvidersUpdateResources]
END
GO

CREATE TRIGGER [dbo].[ProvidersUpdateResources] ON [dbo].[Resources]
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ResourceId int
	DECLARE @ResourceType nvarchar(1)
	DECLARE @PracticeId int
	DECLARE @Billable nvarchar(1)
	DECLARE @GoDirect nvarchar(1)

	DECLARE ProvidersUpdateResourcesCursor CURSOR FOR
	SELECT i.ResourceId, i.ResourceType, i.PracticeId, i.Billable, i.GoDirect
	FROM inserted i
	JOIN deleted d ON d.ResourceId = i.ResourceId
		AND d.ResourceType IN ('D', 'Q', 'Z','Y', 'R')
	WHERE i.ResourceType IN ('D', 'Q', 'Z','Y', 'R')

	OPEN ProvidersUpdateResourcesCursor FETCH NEXT FROM ProvidersUpdateResourcesCursor INTO
	@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		DECLARE @DeletedPracticeId INT
		DECLARE @DeletedResourceId INT
		SET @DeletedPracticeId = (SELECT TOP 1 PracticeId FROM deleted WHERE ResourceId = @ResourceId)
		SET @DeletedResourceId = (SELECT TOP 1 ResourceId FROM deleted WHERE ResourceId = @ResourceId)

		IF ((SELECT TOP 1 PracticeId FROM inserted WHERE ResourceId = @ResourceId) <> @DeletedPracticeId)
		BEGIN
			IF NOT EXISTS (
				SELECT p.Id
				FROM model.Providers p 
				JOIN inserted i ON p.UserId = i.ResourceId
					AND p.BillingOrganizationId = i.PracticeId
				)
			BEGIN
				INSERT INTO model.Providers (IsBillable, IsEmr, BillingOrganizationId, UserId, ServiceLocationId)
				SELECT DISTINCT
				CASE @Billable WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsBillable
				,CASE @GoDirect WHEN 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS IsEMR
				,CASE WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y' THEN (110000000 * 1 + @ResourceId) ELSE pnBillOrg.PracticeId END AS BillingOrganizationId
				,@ResourceId AS UserId
				,NULL AS ServiceLocationId
				FROM inserted re
				INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
					AND pnBillOrg.PracticeType = 'P'
				LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
						AND pa.ResourceType = 'R'
				WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
					AND re.ResourceId = @ResourceId
				EXCEPT
				SELECT IsBillable, IsEmr, BillingOrganizationId, UserId, ServiceLocationId FROM model.Providers	
			END
		END

		UPDATE p
		SET p.IsBillable = CASE WHEN @Billable = 'Y' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
		,p.IsEmr = CASE WHEN @GoDirect = 'Y' THEN CONVERT(BIT,0) ELSE CONVERT(BIT,1) END
		,p.BillingOrganizationId = CASE WHEN 
			((SELECT TOP 1 [Override] FROM dbo.PracticeAffiliations WHERE ResourceId = @ResourceId AND ResourceType = 'R') = 'Y' OR
			(SELECT TOP 1 OrgOverride FROM dbo.PracticeAffiliations WHERE ResourceId = @ResourceId AND ResourceType = 'R') = 'Y')
			THEN (110000000 * 1 + @ResourceId) 
		ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @PracticeId AND PracticeType = 'P')
		END
		,p.UserId = CASE WHEN @ResourceType = 'R' THEN NULL ELSE @ResourceId END
		,p.ServiceLocationId = CASE WHEN @ResourceType = 'R' THEN (SELECT TOP 1 PracticeId FROM dbo.PracticeName pnBillOrg WHERE pnBillOrg.PracticeId = @PracticeId
				AND pnBillOrg.PracticeType = 'P') END
		FROM model.Providers p
		WHERE p.BillingOrganizationId = CASE WHEN 
				((SELECT TOP 1 [Override] FROM dbo.PracticeAffiliations WHERE ResourceId = @DeletedResourceId AND ResourceType = 'R') = 'Y' OR
				(SELECT TOP 1 OrgOverride FROM dbo.PracticeAffiliations WHERE ResourceId = @DeletedResourceId AND ResourceType = 'R') = 'Y')
				THEN (110000000 * 1 + @DeletedResourceId) 
			ELSE (SELECT TOP 1 PracticeId FROM dbo.PracticeName WHERE PracticeId = @DeletedPracticeId AND PracticeType = 'P')
			END
			AND p.UserId = CASE WHEN @ResourceType = 'R' THEN NULL ELSE @ResourceId END
			AND p.ServiceLocationId = CASE WHEN @ResourceType = 'R' THEN (SELECT TOP 1 PracticeId FROM dbo.PracticeName pnBillOrg WHERE pnBillOrg.PracticeId = @DeletedPracticeId
				AND pnBillOrg.PracticeType = 'P') END

		FETCH NEXT FROM ProvidersUpdateResourcesCursor INTO 
		@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect
	END
	CLOSE ProvidersUpdateResourcesCursor
	DEALLOCATE ProvidersUpdateResourcesCursor
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersUpdatePracticeAffiliations' AND parent_id = OBJECT_ID('dbo.PracticeAffiliations','U'))
	DROP TRIGGER ProvidersUpdatePracticeAffiliations
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'ProvidersInsertResources' AND parent_id = OBJECT_ID('dbo.Resources','U'))
	DROP TRIGGER ProvidersInsertResources
GO

CREATE TRIGGER dbo.ProvidersInsertResources ON dbo.Resources
AFTER INSERT
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @ResourceId INT
	DECLARE @ResourceType NVARCHAR(1)
	DECLARE @PracticeId INT
	DECLARE @Billable NVARCHAR(1)
	DECLARE @GoDirect NVARCHAR(1)
	DECLARE @ServiceCode NVARCHAR(4)

	DECLARE ProvidersInsertResourcesCursor CURSOR FOR
	SELECT ResourceId, ResourceType, PracticeId, Billable, GoDirect, ServiceCode
	FROM inserted WHERE ResourceType in ('D', 'Q', 'Z', 'Y', 'R')

	OPEN ProvidersInsertResourcesCursor FETCH NEXT FROM ProvidersInsertResourcesCursor INTO
	@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect, @ServiceCode
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		INSERT INTO model.Providers (
			IsBillable
			,IsEmr
			,BillingOrganizationId
			,UserId
			,ServiceLocationId
		)
		SELECT 
		* 
		FROM (
			SELECT DISTINCT
			CASE @Billable WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsBillable
			,CASE @GoDirect WHEN 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS IsEMR
			,CASE WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'
					THEN (110000000 * 1 + @ResourceId) 
				ELSE pnBillOrg.PracticeId
			END AS BillingOrganizationId
			,NULL AS UserId
			,pnBillOrg.PracticeId AS ServiceLocationId
			FROM inserted re
			INNER JOIN dbo.PracticeName pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
				AND pnBillOrg.PracticeType = 'P'
			LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
					AND pa.ResourceType = 'R'
			WHERE re.ResourceId = @ResourceId
					AND @ResourceType = 'R'
					AND @ServiceCode = '02'
					AND @Billable = 'Y'
					AND pnBillOrg.LocationReference <> ''
	
			UNION
	
			SELECT DISTINCT
			CASE @Billable WHEN 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsBillable
			,CASE @GoDirect WHEN 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS IsEMR
			,CASE WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y' THEN (110000000 * 1 + @ResourceId) ELSE pnBillOrg.PracticeId END AS BillingOrganizationId
			,@ResourceId AS UserId
			,NULL AS ServiceLocationId
			FROM inserted re
			INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
				AND pnBillOrg.PracticeType = 'P'
			LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
					AND pa.ResourceType = 'R'
			WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
				AND re.ResourceId = @ResourceId
		)v
		EXCEPT
		SELECT IsBillable, IsEmr, BillingOrganizationId, UserId, ServiceLocationId FROM model.Providers	

		FETCH NEXT FROM ProvidersInsertResourcesCursor INTO 
		@ResourceId, @ResourceType, @PracticeId, @Billable, @GoDirect, @ServiceCode
	END
	CLOSE ProvidersInsertResourcesCursor
	DEALLOCATE ProvidersInsertResourcesCursor
END
GO

UPDATE p
SET p.IsEmr = CASE WHEN p.IsEmr = 1 THEN CONVERT(BIT,0) ELSE CONVERT(BIT, 1) END
FROM model.Providers p
GO