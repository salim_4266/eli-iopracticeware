﻿DECLARE @triggerNames xml
EXEC dbo.DisableEnabledTriggers 'model.ExternalSystemMessageTypes', @names = @triggerNames OUTPUT

DECLARE @constraintNames xml
EXEC dbo.DisableEnabledCheckConstraints 'model.ExternalSystemMessageTypes', @names = @constraintNames OUTPUT

IF NOT EXISTS (SELECT Id FROM model.ExternalSystemMessageTypes WHERE Id = 37 AND Name = 'PatientStatement')
BEGIN
	SET IDENTITY_INSERT model.ExternalSystemMessageTypes ON

	INSERT model.ExternalSystemMessageTypes (Id, Name, [Description], IsOutbound)
	SELECT 
	37 AS Id
	,'PatientStatement' AS Name
	,'Patient Statement' AS [Description]
	,CONVERT(BIT,1) AS IsOutbound

	SET IDENTITY_INSERT model.ExternalSystemMessageTypes OFF
	EXEC dbo.EnableTriggers @triggerNames
	EXEC dbo.EnableCheckConstraints @constraintNames
END
ELSE IF EXISTS (SELECT Id FROM model.ExternalSystemMessageTypes WHERE Id = 37 AND Name <> 'PatientStatement')
BEGIN
	EXEC dbo.EnableTriggers @triggerNames
	EXEC dbo.EnableCheckConstraints @constraintNames
	RAISERROR('There already exists an entry with the Id 37 in model.ExternalSystemMessageTypes, where Name is not PatientStatement. INSERT fails.',16,1)
END
GO

IF NOT EXISTS (SELECT Id FROM model.ExternalSystems WHERE Name = 'PatientStatement')
BEGIN
	SET IDENTITY_INSERT model.ExternalSystems ON

	INSERT model.ExternalSystems (Id, Name)
	SELECT 
	26 AS Id, 'PatientStatement' AS Name

	SET IDENTITY_INSERT model.ExternalSystems OFF
END
GO

INSERT model.ExternalSystemExternalSystemMessageTypes (ExternalSystemId, ExternalSystemMessageTypeId, TemplateFile, IsDisabled)
SELECT 
(SELECT TOP 1 Id FROM model.ExternalSystems WHERE Name = 'PatientStatement') AS ExternalSystemId
,(SELECT TOP 1 Id FROM model.ExternalSystemMessageTypes WHERE Id = 37 AND Name = 'PatientStatement') AS ExternalSystemMessageTypeId
,NULL AS TemplateFile
,CONVERT(BIT,0) AS IsDisabled
EXCEPT SELECT ExternalSystemId, ExternalSystemMessageTypeId, TemplateFile, IsDisabled FROM model.ExternalSystemExternalSystemMessageTypes
GO