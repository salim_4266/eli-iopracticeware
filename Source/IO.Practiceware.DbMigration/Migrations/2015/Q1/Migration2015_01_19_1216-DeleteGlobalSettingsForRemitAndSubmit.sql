-- Claims directory and remittances directory must be both machine and user id scoped
delete from model.ApplicationSettings
where Name in ('ClaimsDirectory', 'RemittanceFilesPath') and (MachineName is null OR UserId is null)