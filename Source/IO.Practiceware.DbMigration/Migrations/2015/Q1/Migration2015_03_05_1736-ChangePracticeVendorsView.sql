IF OBJECT_ID(N'dbo.PracticeVendors') IS NOT NULL
BEGIN
  DROP VIEW [dbo].[PracticeVendors]
END
GO

CREATE VIEW [dbo].[PracticeVendors] AS
SELECT 
	CONVERT(INT,ec.ID) AS VendorId,
	CONVERT(NVARCHAR(64),ec.DisplayName) AS VendorName,
	CONVERT(NVARCHAR(1),
			CASE ec.__EntityType__
				WHEN 'ExternalProvider' THEN 'D'
				WHEN 'ExternalContact' THEN '~'
			END
	) AS VendorType,
	CONVERT(NVARCHAR(50),COALESCE(eca.Line1,'')) AS VendorAddress,
	CONVERT(NVARCHAR(35),COALESCE(eca.Line2,'')) AS VendorSuite,
	CONVERT(NVARCHAR(35),COALESCE(eca.City,'')) AS VendorCity,
	CONVERT(NVARCHAR(2),COALESCE(sop.Abbreviation,'')) AS VendorState,
	CONVERT(NVARCHAR(10),COALESCE(eca.PostalCode,'')) AS VendorZip,
	CONVERT(NVARCHAR(16),COALESCE(COALESCE(ecPhone.AreaCode,'') + ecPhone.ExchangeAndSuffix,'')) AS VendorPhone,
	CONVERT(NVARCHAR(18),CASE WHEN COALESCE(ecFax.AreaCode,'') <> '' 
			AND CHARINDEX('(',COALESCE(ecFax.AreaCode,'')) = 0 THEN COALESCE(COALESCE('('+ecFax.AreaCode+')','') + ecFax.ExchangeAndSuffix,'') 
			ELSE COALESCE(COALESCE(ecFax.AreaCode,'') + ecFax.ExchangeAndSuffix,'') END ) AS VendorFax,
	CONVERT(NVARCHAR(32),COALESCE(ecea.Value,'')) AS VendorEmail,
	CONVERT(NVARCHAR(32),'') AS VendorTaxId,
	CONVERT(NVARCHAR(32),'') AS VendorUPIN,
	CONVERT(NVARCHAR(64),COALESCE(cst.Name,'')) AS VendorSpecialty,
	CONVERT(NVARCHAR(20),COALESCE(other1.Line1 + ' ' + other1.Line2 + ' ' + other1.City + ' ' + otherState1.Abbreviation + ' ' + other1.PostalCode 
		+ COALESCE(' P#: ' + COALESCE(otherPhone1.AreaCode,'') + otherPhone1.ExchangeAndSuffix,'')
		+ COALESCE(' F#: ' + COALESCE(otherFax1.AreaCode,'') + otherFax1.ExchangeAndSuffix,''),'')) AS VendorOtherOffice1,
	CONVERT(NVARCHAR(16),COALESCE(other2.Line2 + ' ' + other2.Line2 + ' ' + other2.City + ' ' + otherState2.Abbreviation + ' ' + other2.PostalCode 
		+ COALESCE(' P#: ' + COALESCE(otherPhone2.AreaCode,'') + otherPhone2.ExchangeAndSuffix,'')
		+ COALESCE(' F#: ' + COALESCE(otherFax2.AreaCode,'') + otherFax2.ExchangeAndSuffix,''),'')) AS VendorOtherOffice2,
	CONVERT(NVARCHAR(20),COALESCE(ec.LastNameOrEntityName,'')) AS VendorLastName,
	CONVERT(NVARCHAR(16),COALESCE(ec.FirstName,'')) AS VendorFirstName,
	CONVERT(NVARCHAR(1),COALESCE(LEFT(ec.MiddleName,1),'')) AS VendorMI,
	CONVERT(NVARCHAR(8),COALESCE(ec.Suffix,'')) AS VendorTitle,
	CONVERT(NVARCHAR(25),ec.NickName) AS VendorSalutation,
	CONVERT(NVARCHAR(64),COALESCE(eo.DisplayName,'')) AS VendorFirmName,
	CONVERT(NVARCHAR(16),COALESCE(ec.LegacyTaxonomy,'')) AS VendorTaxonomy,
	CONVERT(NVARCHAR(1),CASE WHEN ec.ExcludeOnClaim = 1 THEN 'T' ELSE 'F' END) AS VendorExcRefDr,
	CONVERT(NVARCHAR(10),COALESCE(ec.LegacyNpi,'')) AS VendorNpi,
	CONVERT(NVARCHAR(25),COALESCE(CASE WHEN pnt.Name = 'Cell' THEN COALESCE(ecPhone.AreaCode,'') + ecPhone.ExchangeAndSuffix END,'')) AS VendorCell,
	CONVERT(NVARCHAR(64),COALESCE(cstOther.Name,'')) AS VendorSpecOther,
	CONVERT(INT,1) AS OrdinalId
FROM model.ExternalContacts ec
LEFT JOIN model.ExternalContactAddresses eca ON eca.ExternalContactId = ec.Id
	AND eca.ContactAddressTypeId = 8
LEFT JOIN model.StateOrProvinces sop ON sop.Id = eca.StateOrProvinceId
LEFT JOIN model.ExternalContactPhoneNumbers ecPhone ON ecPhone.ExternalContactId = ec.Id
	AND ecPhone.ExternalContactPhoneNumberTypeId = 5
LEFT JOIN model.ExternalContactPhoneNumbers ecFax ON ecFax.ExternalContactId = ec.Id
	AND ecFax.ExternalContactPhoneNumberTypeId = 4
LEFT JOIN model.ExternalContactEmailAddresses ecea ON ecea.ExternalContactId = ec.Id
	AND ecea.OrdinalId = 0
LEFT JOIN model.ExternalProviderClinicalSpecialtyTypes st ON st.ExternalProviderId = ec.Id
	AND st.OrdinalId = 1
LEFT JOIN model.ClinicalSpecialtyTypes cst ON cst.Id = st.ClinicalSpecialtyTypeId
LEFT JOIN model.ExternalContactAddresses other1 ON other1.ExternalContactId = ec.Id
	AND eca.ContactAddressTypeId = 1001
LEFT JOIN model.StateOrProvinces otherState1 ON otherState1.Id = other1.StateOrProvinceId
LEFT JOIN model.ExternalContactPhoneNumbers otherPhone1 ON otherPhone1.ExternalContactId = ec.Id
	AND otherPhone1.ExternalContactPhoneNumberTypeId = 5
LEFT JOIN model.ExternalContactPhoneNumbers otherFax1 ON otherFax1.ExternalContactId = ec.Id
	AND otherFax1.ExternalContactPhoneNumberTypeId = 4
LEFT JOIN model.ExternalContactAddresses other2 ON other2.ExternalContactId = ec.Id
	AND eca.ContactAddressTypeId = 1002
LEFT JOIN model.StateOrProvinces otherState2 ON otherState2.Id = other2.StateOrProvinceId
LEFT JOIN model.ExternalContactPhoneNumbers otherPhone2 ON otherPhone2.ExternalContactId = ec.Id
	AND otherPhone2.ExternalContactPhoneNumberTypeId = 5
LEFT JOIN model.ExternalContactPhoneNumbers otherFax2 ON otherFax2.ExternalContactId = ec.Id
	AND otherFax2.ExternalContactPhoneNumberTypeId = 4
LEFT JOIN model.ExternalOrganizations eo ON ec.ExternalOrganizationId = eo.Id
LEFT JOIN model.ExternalContactPhoneNumberTypes pnt ON pnt.Id = ecPhone.ExternalContactPhoneNumberTypeId
LEFT JOIN model.ExternalProviderClinicalSpecialtyTypes stOther ON stOther.ExternalProviderId = ec.Id
	AND stOther.OrdinalId > 1
LEFT JOIN model.ClinicalSpecialtyTypes cstOther ON cstOther.Id = stOther.ClinicalSpecialtyTypeId

UNION ALL 

SELECT CONVERT(INT,(1 * 1000000000 + eo.Id)) AS VendorId,
		CONVERT(NVARCHAR(64),eo.Name) AS VendorName,
		CONVERT(NVARCHAR(1),
			CASE eo.ExternalOrganizationTypeId 
				WHEN 2 THEN 'L'
				WHEN 3 THEN 'H'
				WHEN 7 THEN 'V'
				END
		) AS VendorType,
		CONVERT(NVARCHAR(50),COALESCE(eoa.Line1,'')) AS VendorAddress,
		CONVERT(NVARCHAR(35),COALESCE(eoa.Line2,'')) AS VendorSuite,
		CONVERT(NVARCHAR(35),COALESCE(eoa.City,'')) AS VendorCity,
		CONVERT(NVARCHAR(2),COALESCE(sop.Abbreviation,'')) AS VendorState,
		CONVERT(NVARCHAR(10),COALESCE(eoa.PostalCode,'')) AS VendorZip,
		CONVERT(NVARCHAR(16),COALESCE(COALESCE(ecPhone.AreaCode,'') + ecPhone.ExchangeAndSuffix,'')) AS VendorPhone,
		CONVERT(NVARCHAR(18),CASE WHEN COALESCE(ecFax.AreaCode,'') <> '' 
				AND CHARINDEX('(',COALESCE(ecFax.AreaCode,'')) = 0 THEN COALESCE(COALESCE('('+ecFax.AreaCode+')','') + ecFax.ExchangeAndSuffix,'') 
				ELSE COALESCE(COALESCE(ecFax.AreaCode,'') + ecFax.ExchangeAndSuffix,'') END ) AS VendorFax,
		CONVERT(NVARCHAR(32),COALESCE(ecea.Value,'')) AS VendorEmail,
		CONVERT(NVARCHAR(32),'') AS VendorTaxId,
		CONVERT(NVARCHAR(32),'') AS VendorUPIN,
		CONVERT(NVARCHAR(64),COALESCE(cst.Name,'')) AS VendorSpecialty,
		CONVERT(NVARCHAR(20),COALESCE(other1.Line1 + ' ' + other1.Line2 + ' ' + other1.City + ' ' + otherState1.Abbreviation + ' ' + other1.PostalCode 
			+ COALESCE(' P#: ' + COALESCE(otherPhone1.AreaCode,'') + otherPhone1.ExchangeAndSuffix,'')
			+ COALESCE(' F#: ' + COALESCE(otherFax1.AreaCode,'') + otherFax1.ExchangeAndSuffix,''),'')) AS VendorOtherOffice1,
		CONVERT(NVARCHAR(16),COALESCE(other2.Line2 + ' ' + other2.Line2 + ' ' + other2.City + ' ' + otherState2.Abbreviation + ' ' + other2.PostalCode 
			+ COALESCE(' P#: ' + COALESCE(otherPhone2.AreaCode,'') + otherPhone2.ExchangeAndSuffix,'')
			+ COALESCE(' F#: ' + COALESCE(otherFax2.AreaCode,'') + otherFax2.ExchangeAndSuffix,''),'')) AS VendorOtherOffice2,
		CONVERT(NVARCHAR(20),'') AS VendorLastName,
		CONVERT(NVARCHAR(16),'') AS VendorFirstName,
		CONVERT(NVARCHAR(1),'') AS VendorMI,
		CONVERT(NVARCHAR(8),'') AS VendorTitle,
		CONVERT(NVARCHAR(25),'') AS VendorSalutation,
		CONVERT(NVARCHAR(64),'') AS VendorFirmName,
		CONVERT(NVARCHAR(16),COALESCE(ec.LegacyTaxonomy,'')) AS VendorTaxonomy,
		CONVERT(NVARCHAR(1),CASE WHEN ec.ExcludeOnClaim = 1 THEN 'T' ELSE 'F' END) AS VendorExcRefDr,
		CONVERT(NVARCHAR(10),COALESCE(ec.LegacyNpi,'')) AS VendorNpi,
		CONVERT(NVARCHAR(25),'') AS VendorCell,
		CONVERT(NVARCHAR(64),COALESCE(cstOther.Name,'')) AS VendorSpecOther,
		CONVERT(INT,1) AS OrdinalId
FROM model.ExternalOrganizations eo
LEFT JOIN model.ExternalOrganizationAddresses eoa ON eoa.ExternalOrganizationId = eo.Id
	AND eoa.ExternalOrganizationAddressTypeId = 11
LEFT JOIN model.StateOrProvinces sop ON sop.id = eoa.StateOrProvinceId
LEFT JOIN model.ExternalOrganizationPhoneNumbers ecPhone ON ecPhone.ExternalOrganizationId = eo.Id
	AND ecPhone.ExternalOrganizationPhoneNumberTypeId = (SELECT Id FROM model.ExternalOrganizationPhoneNumberTypes WHERE Name = 'Main')
LEFT JOIN model.ExternalOrganizationPhoneNumbers ecFax ON ecFax.ExternalOrganizationId = eo.Id
	AND ecFax.ExternalOrganizationPhoneNumberTypeId = (SELECT Id FROM model.ExternalOrganizationPhoneNumberTypes WHERE Name = 'Fax')
LEFT JOIN model.ExternalOrganizationEmailAddresses ecea ON ecea.ExternalOrganizationId = eo.Id
	AND ecea.OrdinalId = 0
LEFT JOIN model.ExternalProviderClinicalSpecialtyTypes st ON st.ExternalProviderId = eo.Id
	AND st.OrdinalId = 1
LEFT JOIN model.ClinicalSpecialtyTypes cst ON cst.Id = st.ClinicalSpecialtyTypeId
LEFT JOIN model.ExternalOrganizationAddresses other1 ON other1.ExternalOrganizationId = eo.Id
	AND eoa.ExternalOrganizationAddressTypeId = 1001
LEFT JOIN model.StateOrProvinces otherState1 ON otherState1.Id = other1.StateOrProvinceId
LEFT JOIN model.ExternalOrganizationPhoneNumbers otherPhone1 ON otherPhone1.ExternalOrganizationId = eo.Id
	AND otherPhone1.ExternalOrganizationPhoneNumberTypeId = 5
LEFT JOIN model.ExternalOrganizationPhoneNumbers otherFax1 ON otherFax1.ExternalOrganizationId = eo.Id
	AND otherFax1.ExternalOrganizationPhoneNumberTypeId = 4
LEFT JOIN model.ExternalOrganizationAddresses other2 ON other2.ExternalOrganizationId = eo.Id
	AND eoa.ExternalOrganizationAddressTypeId = 1002
LEFT JOIN model.StateOrProvinces otherState2 ON otherState2.Id = other2.StateOrProvinceId
LEFT JOIN model.ExternalOrganizationPhoneNumbers otherPhone2 ON otherPhone2.ExternalOrganizationId = eo.Id
	AND otherPhone2.ExternalOrganizationPhoneNumberTypeId = 5
LEFT JOIN model.ExternalOrganizationPhoneNumbers otherFax2 ON otherFax2.ExternalOrganizationId = eo.Id
	AND otherFax2.ExternalOrganizationPhoneNumberTypeId = 4
LEFT JOIN model.ExternalContacts ec ON ec.ExternalOrganizationId = eo.Id
LEFT JOIN model.ExternalOrganizationPhoneNumberTypes pnt ON pnt.Id = ecPhone.ExternalOrganizationPhoneNumberTypeId
LEFT JOIN model.ExternalProviderClinicalSpecialtyTypes stOther ON stOther.ExternalProviderId = eo.Id
	AND stOther.OrdinalId > 1
LEFT JOIN model.ClinicalSpecialtyTypes cstOther ON cstOther.Id = stOther.ClinicalSpecialtyTypeId


GO


