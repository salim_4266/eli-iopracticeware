﻿IF OBJECT_ID ( '[model].[GetRankedPatientSearchResults]', 'P' ) IS NOT NULL
BEGIN
	DROP PROCEDURE [model].[GetRankedPatientSearchResults]
END
GO

CREATE PROCEDURE model.GetRankedPatientSearchResults(@Fields nvarchar(max), @SearchText nvarchar(max), @TextSearchMode nvarchar(max), @IsClinical bit, @IsActive bit)
AS 
BEGIN

SET @SearchText = REPLACE(@SearchText, ',', ' ')
SET @SearchText = REPLACE(@SearchText, ')', '')
SET @SearchText = REPLACE(@SearchText, '(', '')
SET @SearchText = REPLACE(@SearchText, '[', '')
SET @SearchText = REPLACE(@SearchText, ']', '')

IF(CHARINDEX('PhoneNumber', @Fields) > 0) 
BEGIN
	SET @SearchText = REPLACE(@SearchText, '-', '')
END

--Removing Duplicate stings from SearchText with delimiter ' ' and refreshing the SearchText
DECLARE @NewSearchText varchar(max)
SELECT @NewSearchText = COALESCE(@NewSearchText, ' ', '') + ' ' + A.nstr FROM (SELECT DISTINCT nstr FROM dbo.CharTable(@SearchText, CASE WHEN CHARINDEX(',', @Fields) = 0 THEN '"' ELSE ' ' END)) as A
SET @SearchText = LTRIM(RTRIM(@NewSearchText))

SELECT 
	t.*,
	CASE
		WHEN t.Field = 'LastName' THEN 1.2 -- Boost search results by last name, since they are rather unique
		WHEN t.Field = 'FirstName' THEN 0.9 -- Lower search results by first name as they can be misleading
		ELSE 1
	END AS FieldMatchValue
INTO #Terms
FROM (
	SELECT
		LTRIM(RTRIM(t.nstr)) AS Term, 
		CASE
			WHEN @TextSearchMode = 'BeginsWith' THEN LTRIM(RTRIM(t.nstr)) + '%' 
			ELSE '%' + LTRIM(RTRIM(t.nstr)) + '%' 
		END AS LikeTerm,
		t.listpos AS TermPos,
		f.nstr AS Field
	FROM dbo.CharTable(@SearchText, CASE WHEN CHARINDEX(',', @Fields) = 0 THEN '"' ELSE ' ' END) t, dbo.CharTable(@Fields,',') f
) t

CREATE UNIQUE CLUSTERED INDEX IX_Terms ON #Terms(Term, Field)

--DECLARE @IncludeAll BIT
--SET @IncludeAll = CASE WHEN @IsClinical IS NULL THEN 

DECLARE @ExactMatchWeight INT, @PartialMatchWeight INT
SET @ExactMatchWeight = 2000
SET @PartialMatchWeight = 1000

SELECT TOP 500 
	q.Rank, 
	p.*
FROM (
	SELECT pr.PatientId, 
		SUM(pr.Rank) AS Rank
	FROM (
		SELECT pmf.PatientId, 
		CASE 
			WHEN pmf.FieldValue LIKE pmf.LikeTerm THEN (2000 * LEN(pmf.Term) / LEN(pmf.FieldValue)) * pmf.FieldMatchValue  -- Rank depends on how many letters out of field value we match. Also takes field match value into consideration
			ELSE 0
		END	AS Rank,
		pmf.TermPos
		FROM (
			SELECT p.Id AS PatientId,
				CASE 
					WHEN t.Field = 'DateOfBirth' THEN CONVERT(nvarchar(max), p.DateOfBirth, 112)
					WHEN t.Field = 'LastName' THEN p.LastName
					WHEN t.Field = 'FirstName' THEN p.FirstName
					WHEN t.Field = 'PatientId' THEN CASE WHEN ISNUMERIC(t.Term) = 1 THEN CONVERT(nvarchar(max), p.Id) ELSE NULL END
					WHEN t.Field = 'PriorPatientCode' THEN CASE WHEN ISNUMERIC(t.Term) = 1 THEN CONVERT(nvarchar(max), p.PriorPatientCode) ELSE NULL END
					ELSE NULL
				END	AS FieldValue,
				t.*
			FROM model.Patients p, #Terms t

			UNION ALL

			SELECT pn.PatientId, 
				CASE
					WHEN t.Field = 'PhoneNumber' THEN COALESCE(pn.CountryCode,'') + COALESCE(pn.AreaCode,'') + REPLACE(pn.ExchangeAndSuffix, '-', '') + COALESCE(pn.Extension,'')
					ELSE NULL
				END AS FieldValue,
				t.*
			FROM model.PatientPhoneNumbers pn, #Terms t

		) pmf
		WHERE pmf.FieldValue IS NOT NULL AND pmf.FieldValue <> ''
	) pr
	WHERE pr.Rank > 0
	GROUP BY pr.PatientId
) q
JOIN model.Patients p ON 
	q.PatientId = p.Id AND (@IsClinical IS NULL OR p.IsClinical = @IsClinical)
	AND p.PatientStatusId = (CASE WHEN @IsActive = 1 THEN 1 ELSE p.PatientStatusId END)
ORDER BY q.Rank DESC, p.LastName, p.FirstName

END

GO
