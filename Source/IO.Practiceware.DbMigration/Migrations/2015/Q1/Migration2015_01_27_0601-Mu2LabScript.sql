﻿--Bug #15850.Lab Scripts for MU2.

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[dbo].[HL7_Observation]')
			AND NAME = 'SpecimenAction'
		)
BEGIN
	ALTER TABLE HL7_Observation ADD SpecimenAction VARCHAR(200) NULL
END
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[dbo].[HL7_Observation]')
			AND NAME = 'SpecimenInfo'
		)
BEGIN
	ALTER TABLE HL7_Observation ADD SpecimenInfo VARCHAR(200) NULL
END
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[dbo].[HL7_Observation]')
			AND NAME = 'ClinicalInfo'
		)
BEGIN
	ALTER TABLE HL7_Observation ADD ClinicalInfo VARCHAR(200) NULL
END
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[dbo].[HL7_Observation]')
			AND NAME = 'SpecimenCondition'
		)
BEGIN
	ALTER TABLE HL7_Observation ADD SpecimenCondition VARCHAR(200) NULL
END
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[dbo].[HL7_Observation]')
			AND NAME = 'SpecimenRejectReason'
		)
BEGIN
	ALTER TABLE HL7_Observation ADD SpecimenRejectReason VARCHAR(100) NULL
END
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[dbo].[HL7_Observation]')
			AND NAME = 'SpecimenType'
		)
BEGIN
	ALTER TABLE HL7_Observation ADD SpecimenType VARCHAR(100) NULL
END
GO

IF EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[dbo].[HL7_Observation]')
			AND NAME = 'OrderDesc'
		)
BEGIN
	ALTER TABLE HL7_Observation

	ALTER COLUMN OrderDesc VARCHAR(250) NULL
END
GO

IF EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[dbo].[HL7_Observation]')
			AND NAME = 'RequestedDate'
		)
BEGIN
	ALTER TABLE HL7_Observation

	ALTER COLUMN RequestedDate DATETIME NULL
END
GO

IF EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[dbo].[HL7_Observation_Details]')
			AND NAME = 'TestDesc'
		)
BEGIN
	ALTER TABLE HL7_Observation_Details

	ALTER COLUMN TestDesc VARCHAR(250) NULL
END
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[dbo].[HL7_LabInterface]')
			AND NAME = 'PlacerOrderNo'
		)
BEGIN
	ALTER TABLE HL7_LabInterface ADD PlacerOrderNo VARCHAR(100) NULL
END
GO

IF NOT EXISTS (
		SELECT *
		FROM sys.COLUMNS
		WHERE object_id = OBJECT_ID(N'[dbo].[HL7_LabInterface]')
			AND NAME = 'PlacerGroupNo'
		)
BEGIN
	ALTER TABLE HL7_LabInterface ADD PlacerGroupNo VARCHAR(100) NULL
END
GO

