--Add Revenue Codes if not exist
--0274 Prosthetic/orthotic devices
--0276 Intraocular lens
--0278 Other implants
--0490 Ambulatory Surgery

--Add 0274
IF NOT EXISTS 
    (   SELECT  1
        FROM    [model].[RevenueCodes] 
        WHERE   Code = '0274'         
    )
    BEGIN
        INSERT [model].[RevenueCodes] (Code, Description) 
        VALUES ('0274', 'Prosthetic/orthotic devices') 
    END;

--Add 0276
IF NOT EXISTS 
    (   SELECT  1
        FROM    [model].[RevenueCodes] 
        WHERE   Code = '0276'         
    )
    BEGIN
        INSERT [model].[RevenueCodes] (Code, Description) 
        VALUES ('0276', 'Intraocular lens') 
    END;

--Add 0278
IF NOT EXISTS 
    (   SELECT  1
        FROM    [model].[RevenueCodes] 
        WHERE   Code = '0278'         
    )
    BEGIN
        INSERT [model].[RevenueCodes] (Code, Description) 
        VALUES ('0278', 'Other implants') 
    END;

--Add 0490
IF NOT EXISTS 
    (   SELECT  1
        FROM    [model].[RevenueCodes] 
        WHERE   Code = '0490'         
    )
    BEGIN
        INSERT [model].[RevenueCodes] (Code, Description) 
        VALUES ('0490', 'Ambulatory Surgical Care') 
    END;
