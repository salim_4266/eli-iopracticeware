DELETE
FROM CMSReports
WHERE CMSRepName LIKE '2013%'

UPDATE cms SET CMSQuery = 'dbo.USP_CMS_GetEPrescStatus'
FROM CMSReports cms
WHERE CONVERT(varchar,CMSQuery) = 'model.USP_CMS_Stage2_eRx'

UPDATE cms SET CMSQuery = 'model.USP_CMS_Stage2_MedicationReconciliation'
FROM CMSReports cms
WHERE CMSRepName = 'Stage 2 � CORE 14: Medication Reconciliation'