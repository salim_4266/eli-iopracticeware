﻿IF NOT EXISTS(SELECT * FROM sys.columns 
        WHERE [name] = N'RelatedCause2Id' 
		AND [object_id] = OBJECT_ID(N'model.InvoiceSupplementals'))
			ALTER TABLE model.InvoiceSupplementals
			ADD RelatedCause2Id int null 
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
        WHERE [name] = N'RelatedCause3Id' 
		AND [object_id] = OBJECT_ID(N'model.InvoiceSupplementals'))
			ALTER TABLE model.InvoiceSupplementals
			ADD RelatedCause3Id int null 
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
        WHERE [name] = N'ClaimFrequencyTypeCodeId' 
		AND [object_id] = OBJECT_ID(N'model.InvoiceReceivables'))
			ALTER TABLE model.InvoiceReceivables
			ADD ClaimFrequencyTypeCodeId int null 
GO



