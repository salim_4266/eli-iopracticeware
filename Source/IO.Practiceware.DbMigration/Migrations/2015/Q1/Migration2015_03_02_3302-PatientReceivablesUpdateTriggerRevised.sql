﻿IF EXISTS (SELECT * FROM sys.triggers WHERE Name = 'OnPatientReceivableUpdate') 
	DROP TRIGGER [dbo].[OnPatientReceivableUpdate];
GO

CREATE TRIGGER OnPatientReceivableUpdate ON dbo.PatientReceivables 
INSTEAD OF UPDATE
AS
SET NOCOUNT ON;

IF EXISTS (
SELECT * FROM inserted
EXCEPT 
SELECT * FROM deleted
)
BEGIN

	DECLARE @ProviderBillingOrganizationMax int
	DECLARE @ProviderBillingOrganizationServiceLocationMax int
	DECLARE @MainLocationId int
	DECLARE @ServiceLocationMax int
	DECLARE @ClinicalInvoiceProviderMax int

	SET @ClinicalInvoiceProviderMax = 110000000
	SET @ServiceLocationMax = 110000000
	SET @ProviderBillingOrganizationMax = 110000000
	SET @ProviderBillingOrganizationServiceLocationMax = 110000000
	SET @MainLocationId = (SELECT TOP 1 PracticeId AS PracticeId FROM dbo.PracticeName WHERE PracticeType = 'P' AND LocationReference = '')

	--step 1: let's get all the invoice id's we're updating.
	SELECT 
	ir.InvoiceId
	,CASE 
		WHEN COUNT(ir.InvoiceId) = 1
			THEN 1 --only patientinsurance row
		WHEN COUNT(ir.InvoiceId) > 1
			THEN 2 -- patient has insurance
	END AS InvoiceIdCounter 
	INTO #InvoiceIds
	FROM model.InvoiceReceivables ir 
	INNER JOIN inserted i ON i.ReceivableId = ir.Id
	INNER JOIN Appointments ap ON i.AppointmentId = ap.AppointmentId
	GROUP BY ir.InvoiceId
	,ap.ResourceId2
	,ap.Comments

	--make a table to handle our servicelocation codes
	SELECT
	ReceivableId
	,AppointmentId
	,CONVERT(BIT,0) AS IsExternal
	,NULL AS ServiceLocationCodeId
	INTO #ServiceLocationCodes
	FROM inserted

	--set the is external switch to on when :
	UPDATE slc
	SET IsExternal = 1
	FROM #ServiceLocationCodes slc
	JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
	WHERE ResourceId2 BETWEEN 1 AND 999

	UPDATE slc
	SET ServiceLocationCodeId = (SELECT TOP 1 Id FROM model.ServiceLocationCodes WHERE PlaceOfServiceCode = '11')
	FROM #ServiceLocationCodes slc
	JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
	WHERE IsExternal = 0
		AND ap.Comments <> 'ASC CLAIM'

	UPDATE slc
	SET ServiceLocationCodeId = slcT.Id
	FROM #ServiceLocationCodes slc
	INNER JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
	INNER JOIN Resources re ON re.ResourceId = ap.ResourceId2
	INNER JOIN model.ServiceLocationCodes slcT ON slcT.PlaceOfServiceCode = re.PlaceOfService
	WHERE IsExternal = 1
		AND ap.Comments <> 'ASC CLAIM'

	DECLARE @pctSL11 INT
	SET @pctSL11 = (SELECT TOP 1 Id FROM model.ServiceLocationCodes WHERE PlaceOfServiceCode = '11')

	;WITH ServiceLocationCodesCTE AS (
		SELECT
		MAX(slcT.Id) AS Id
		,ap.AppointmentId
		FROM model.ServiceLocationCodes slcT 
		INNER JOIN #ServiceLocationCodes slc ON slc.AppointmentId IS NOT NULL
		INNER JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
			AND ap.Comments = 'ASC CLAIM' 
		INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
		INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P'
			AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
			AND PracticeType = 'P'
			AND LocationReference <> ''
		INNER JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
			AND reASC.ResourceType = 'R'
			AND reASC.ServiceCode = '02'
			AND reASC.Billable = 'Y'
			AND pic.FieldValue = 'T'
		WHERE slcT.PlaceOfServiceCode = reASC.PlaceOfService
		GROUP BY ap.AppointmentId
	)
	UPDATE slc
	SET ServiceLocationCodeId = CASE 
			WHEN ServiceLocationCodesCTE.Id IS NOT NULL AND ServiceLocationCodesCTE.Id <> ''
				THEN ServiceLocationCodesCTE.Id
			ELSE @pctSL11
		END
	FROM #ServiceLocationCodes slc
	INNER JOIN Appointments ap ON ap.AppointmentId = slc.AppointmentId
		AND ap.Comments = 'ASC CLAIM' 
	LEFT OUTER JOIN ServiceLocationCodesCTE ON ServiceLocationCodesCTE.AppointmentId = ap.AppointmentId

	IF EXISTS (
		SELECT
		*
		FROM inserted i
		WHERE i.ReceivableType = 'S'
			AND i.OpenBalance = 0
			AND i.InsuredId = 0
			AND i.InsurerId = 99999
			AND i.AppointmentId = 0
			AND i.Charge = 0
			AND i.BillToDr = 0
			AND i.BillingOffice = 0
			AND i.Status = 'A'
			AND i.ReceivableId - (1 * 1000000000) IN 
			(SELECT Id FROM model.Adjustments WHERE InvoiceReceivableId IS NULL AND Id IN 
			(SELECT ReceivableId-(1 * 1000000000) FROM inserted))
		)
	BEGIN
		DECLARE @iOpenBalance DECIMAL(18,2)
		SET @iOpenBalance = (SELECT ABS(i.OpenBalance) FROM inserted i)

		IF (@iOpenBalance = 0)
		BEGIN
			UPDATE adj
			SET adj.Amount = -0.01
			FROM model.Adjustments adj
			INNER JOIN inserted i ON i.ReceivableId - (1 * 1000000000) = adj.Id
		END
		ELSE 
		BEGIN
			UPDATE adj
			SET adj.Amount = @iOpenBalance
			FROM model.Adjustments adj
			INNER JOIN inserted i ON i.ReceivableId - (1 * 1000000000) = adj.Id
		END
	END

	ELSE
	BEGIN
	--Patient does not have insurance and will not get insurance
	IF EXISTS 
		(SELECT * FROM inserted i
			INNER JOIN model.InvoiceReceivables ir ON i.ReceivableId = ir.Id
			WHERE ir.PatientInsuranceId IS NULL AND i.InsurerId = 0)
	BEGIN
		UPDATE ir
		SET OpenForReview = CASE 
			WHEN i.ReceivableType = 'I'
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0) END
		FROM model.InvoiceReceivables ir 
		INNER JOIN inserted i ON ir.Id = i.ReceivableId	
	END

	--Patient has insurance and will continue to have insurance
	IF EXISTS (
			SELECT * FROM inserted i
			INNER JOIN model.InvoiceReceivables ir ON i.ReceivableId = ir.Id
			WHERE ir.PatientInsuranceId IS NOT NULL AND i.InsurerId <> 0)
	BEGIN 
		UPDATE ir
		SET ir.OpenForReview = CASE 
			WHEN i.ReceivableType = 'I'
				THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0) 
			END
			,ir.PayerClaimControlNumber = i.ExternalRefInfo
		FROM model.InvoiceReceivables ir 
		INNER JOIN inserted i ON ir.Id = i.ReceivableId
		INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
		INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
			AND ip.InsurerId = i.InsurerId 
			AND ip.PolicyHolderPatientId = i.InsuredId
	END

	--Patient did not have insurance and now has insurance.
	DECLARE @ReceivableId INT
	SELECT TOP 1 @ReceivableId = ReceivableId FROM inserted i

	DECLARE @InvoiceId INT
	SELECT TOP 1 @InvoiceId = ir.InvoiceId FROM model.InvoiceReceivables ir WHERE ir.Id = @ReceivableId

	IF EXISTS (
		SELECT 
		PatientId
		FROM inserted i
		JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
		WHERE Id NOT IN (
			SELECT 
			PatientInsuranceId 
			FROM model.InvoiceReceivables 
			WHERE InvoiceId IN
				(SELECT 
				InvoiceId 
				FROM model.InvoiceReceivables 
				WHERE Id IN
					(SELECT 
					ReceivableId 
					FROM inserted
					)
				)
			)
		)
	OR EXISTS (
		SELECT 
		d.ReceivableId 
		FROM deleted d
		INNER JOIN inserted i On i.ReceivableId = d.ReceivableId
			AND i.PatientId = d.PatientId
			AND i.AppointmentId = d.AppointmentId
		WHERE d.InsurerId = 0 AND i.InsurerId > 0
	)
	BEGIN
		CREATE TABLE #OutputPatientInsuranceId (PatientInsuranceId INT)
		INSERT INTO [model].[InvoiceReceivables] (
			[InvoiceId]
			,[PatientInsuranceId]
			,[PayerClaimControlNumber]
			,[PatientInsuranceAuthorizationId]
			,[PatientInsuranceReferralId]
			,[OpenForReview]
			)
		SELECT invIds.InvoiceId
				,pi.Id AS PatientInsuranceId
				,i.ExternalRefInfo AS PayerClaimControlNumber
				,pia.Id AS PatientInsuranceAuthorizationId
				,pir.Id AS PatientInsuranceReferralId
				,CASE 
					WHEN i.InsurerDesignation = 'T'
						THEN CONVERT(BIT, 1)
					ELSE CONVERT(BIT, 0)
				END AS OpenForReview
			FROM inserted i
			LEFT JOIN model.PatientInsurances pi ON pi.InsuredPatientId = i.PatientId
				AND (pi.EndDateTime IS NULL OR pi.EndDateTime = '' OR CONVERT(NVARCHAR,pi.EndDateTime,112) >= i.InvoiceDate)
				AND pi.IsDeleted <> 1
			LEFT JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id AND ip.InsurerId = i.InsurerId
				AND CONVERT(NVARCHAR,ip.StartDateTime,112) <= i.InvoiceDate
			INNER JOIN model.InvoiceReceivables ir ON ir.Id = i.ReceivableId
			LEFT JOIN model.PatientInsuranceReferrals pir ON pir.PatientInsuranceId = pi.Id And pir.PatientId = i.PatientId 
			LEFT JOIN model.PatientInsuranceAuthorizations pia ON pia.EncounterId = i.AppointmentId
				AND pi.Id = pia.PatientInsuranceId
				AND pia.IsArchived = 0
				AND pia.AuthorizationDateTime >= ip.StartDateTime
			INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId AND InvoiceIdCounter = 1

	--set is open for review open false when not the minimum ordinalId, per insurance type.
		UPDATE ir
		SET ir.OpenForReview = CONVERT(BIT,0)
		,ir.PatientInsuranceAuthorizationId = NULL
		,ir.PatientInsuranceReferralId = NULL
		FROM model.InvoiceReceivables ir
		INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId AND InvoiceIdCounter = 1
		INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
		WHERE pi.OrdinalId NOT IN (
			SELECT MIN(pi.OrdinalId) 
			FROM model.InvoiceReceivables ir
			INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId 
			INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
			GROUP BY 
			ir.InvoiceId
			,pi.InsuranceTypeId)
		AND PatientInsuranceId IS NOT NULL

		UPDATE ir
		SET ir.OpenForReview = CONVERT(BIT,0)
		FROM model.InvoiceReceivables ir
		INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId AND InvoiceIdCounter = 1
		WHERE ir.PatientInsuranceId IS NULL
		
	--Patient does not have insurance before udpate, the update is giving insurnace. Need to update all references to old receivableId
		UPDATE ptj
		SET ptj.TransactionTypeId = (
			SELECT TOP 1 ir.Id
			FROM model.InvoiceReceivables ir
			INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId 
				AND InvoiceIdCounter = 1
			INNER JOIN model.Invoices i ON i.Id = invIds.InvoiceId
			INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.EncounterId
			INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
			LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
				AND ip.InsurerId IN (SELECT InsurerId FROM inserted)
			WHERE pi.OrdinalId IN (
				SELECT MIN(pi.OrdinalId) 
				FROM model.InvoiceReceivables ir
				INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = ir.InvoiceId 
				INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
				GROUP BY 
				ir.InvoiceId
				,pi.InsuranceTypeId)
			AND PatientInsuranceId IS NOT NULL
			AND ap.AppointmentInsuranceTypeId = pi.InsuranceTypeId
		)
		FROM PracticeTransactionJournal ptj
		WHERE ptj.TransactionTypeId IN (SELECT ReceivableId FROM deleted d WHERE InsurerId = 0)
			AND TransactionType = 'R' 
			AND TransactionRef <> 'G'
	END

	---invoices
		DECLARE @BillingProviderIdForInvoiceUpdate INT
		IF EXISTS (SELECT TOP 1 * FROM inserted i JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId WHERE ap.Comments <> 'ASC CLAIM')
		BEGIN 
			SET @BillingProviderIdForInvoiceUpdate = (
				SELECT DISTINCT TOP 1 p.Id
				FROM inserted i
				INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
				INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
					AND re.ResourceType IN ('D', 'Q', 'Z','Y')
				INNER JOIN dbo.PracticeName AS pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
					AND pnBillOrg.PracticeType = 'P'
				LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
					AND pa.ResourceType = 'R'
				INNER JOIN model.Providers p ON re.ResourceId = p.UserId
					AND p.BillingOrganizationId = CASE WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'
						THEN (110000000 + re.ResourceId) 
						ELSE pnBillOrg.PracticeId
					END
					AND p.IsBillable = CASE WHEN re.Billable = 'Y' THEN CONVERT(BIT,1) ELSE CONVERT(BIT, 0) END
					AND p.IsEmr = CASE WHEN re.GoDirect = 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END
			)
		END
		ELSE
		BEGIN
			SET @BillingProviderIdForInvoiceUpdate = (
				SELECT DISTINCT TOP 1 p.Id
				FROM dbo.Appointments ap 
				LEFT JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
				LEFT JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId 
					AND apEncounter.AppTypeId = ap.AppTypeId 
					AND apEncounter.AppDate = ap.AppDate 
					AND apEncounter.AppTime > 0 
					AND ap.AppTime = 0 
					AND apEncounter.ScheduleStatus = ap.ScheduleStatus 
					AND ap.ScheduleStatus = 'D' 
					AND apEncounter.ResourceId1 = ap.ResourceId1 
					AND apEncounter.ResourceId2 = ap.ResourceId2
				LEFT JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
				LEFT JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P'
					AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
					AND PracticeType = 'P'
					AND LocationReference <> ''
				LEFT JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
					AND reASC.ResourceType = 'R'
					AND reASC.ServiceCode = '02'
					AND reASC.Billable = 'Y'
					AND pic.FieldValue = 'T'
				JOIN model.BillingOrganizations bo ON bo.Id = pnServLoc.PracticeId
				JOIN model.Providers p ON p.BillingOrganizationId = bo.Id
					AND p.UserId IS NULL
					AND p.ServiceLocationId = pnServLoc.PracticeId
				JOIN inserted i ON i.AppointmentId = ap.AppointmentId
			)
		END

		IF @BillingProviderIdForInvoiceUpdate IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(MAX)
			SET @errorMessage = 'Unable to set BillingProviderId, UPDATE, PatientId: ' + CONVERT(NVARCHAR(MAX),(SELECT TOP 1 PatientId FROM inserted)) + ', LastName: '+ (SELECT LastName FROM model.Patients WHERE Id = (SELECT TOP 1 PatientId FROM inserted))
			RAISERROR(@errorMessage,16,1)
		END

		UPDATE iv
		SET ClinicalInvoiceProviderId = 
				CASE 
					WHEN ap.Comments <> 'ASC CLAIM' 
						THEN model.GetBigPairId(ap.ResourceId1, iv.EncounterId, @ClinicalInvoiceProviderMax)
					ELSE model.GetBigPairId(reASC.ResourceId, iv.EncounterId, @ClinicalInvoiceProviderMax) 
				END
			,BillingProviderId = @BillingProviderIdForInvoiceUpdate
			,ReferringExternalProviderId = 
				CASE
					WHEN (i.ReferDr = 0 OR i.ReferDr = '')
						THEN NULL
					ELSE i.ReferDr 
				END
			,ServiceLocationCodeId = slc.ServiceLocationCodeId
			,iv.LegacyPatientBilledDateTime = 
				CASE 
					WHEN i.PatientBillDate = ''
						THEN NULL
					ELSE CONVERT(datetime, i.PatientBillDate, 112) 
				END				
			,AttributeToServiceLocationId = 
				CASE 
					WHEN i.BillingOffice = 0
						THEN (SELECT TOP 1 PracticeId FROM PracticeName WHERE LocationReference = '' AND PracticeType = 'P')
					WHEN i.BillingOffice BETWEEN 1 AND 999
						THEN model.GetPairId(1, reAtribTo.ResourceId, @ServiceLocationMax)
					ELSE i.BillingOffice - 1000
				END
			,[LegacyDateTime] = CONVERT(datetime, i.InvoiceDate, 112)
		FROM inserted i
		INNER JOIN model.InvoiceReceivables ir ON ir.Id = i.ReceivableId
		INNER JOIN model.Invoices iv ON iv.Id = ir.InvoiceId
		LEFT JOIN #ServiceLocationCodes slc ON slc.ReceivableId = i.ReceivableId
		INNER JOIN #InvoiceIds invIds ON invIds.InvoiceId = iv.Id
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.AppointmentId
		INNER JOIN model.Patients p ON p.Id = i.PatientId
		LEFT JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
		LEFT JOIN dbo.Resources reAtribTo ON i.BillingOffice = reAtribTo.ResourceId
		LEFT JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId 
			AND apEncounter.AppTypeId = ap.AppTypeId 
			AND apEncounter.AppDate = ap.AppDate 
			AND apEncounter.AppTime > 0 
			AND ap.AppTime = 0 
			AND apEncounter.ScheduleStatus = ap.ScheduleStatus 
			AND ap.ScheduleStatus = 'D' 
			AND apEncounter.ResourceId1 = ap.ResourceId1 
			AND apEncounter.ResourceId2 = ap.ResourceId2
		LEFT JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
		LEFT JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P'
			AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
			AND PracticeType = 'P'
			AND LocationReference <> ''
		LEFT JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
			AND reASC.ResourceType = 'R'
			AND reASC.ServiceCode = '02'
			AND reASC.Billable = 'Y'
			AND pic.FieldValue = 'T'

	-- invoice supplemental logic
	IF EXISTS (
	SELECT *
	FROM inserted i
	WHERE FirstConsDate <> '' OR HspAdmDate <> '' OR Over90 <> '' OR Over90 <> '' OR RsnDate <> '' OR HspDisDate <> '' OR AccType <> ''
	)
	AND NOT EXISTS(SELECT * FROM model.InvoiceSupplementals WHERE Id = @InvoiceId)
	BEGIN
		INSERT INTO model.InvoiceSupplementals (
			Id
			,AccidentDateTime
			,AccidentStateOrProvinceId
			,AdmissionDateTime
			,ClaimDelayReasonId
			,DisabilityDateTime
			,DischargeDateTime
			,RelatedCause1Id
			,VisionPrescriptionDateTime
			)
		SELECT @InvoiceId AS Id
			,CASE 
				WHEN FirstConsDate = ''
					THEN NULL
				ELSE CONVERT(DATETIME, FirstConsDate, 101)
				END AS AccidentDateTime
			,CASE st.id
				WHEN 0
					THEN NULL
				ELSE st.id
				END AS AccidentStateOrProvinceId
			,CASE 
				WHEN HspAdmDate = ''
					THEN NULL
				ELSE CONVERT(DATETIME, HspAdmDate, 101)
				END AS AdmissionDateTime
			,CASE Over90
				WHEN 1
					THEN 1
				WHEN 2
					THEN 2
				WHEN 3
					THEN 3
				WHEN 4
					THEN 4
				WHEN 5
					THEN 5
				WHEN 6
					THEN 6
				WHEN 7
					THEN 7
				WHEN 8
					THEN 8
				WHEN 9
					THEN 9
				WHEN 10
					THEN 10
				WHEN 11
					THEN 11
				WHEN 15
					THEN 12
				ELSE NULL
				END AS ClaimDelayReasonId
			,CASE 
				WHEN COALESCE(RsnDate, '') <> ''
					THEN CONVERT(DATETIME, RsnDate, 101)
				ELSE NULL
				END AS DisabilityDateTime
			,CASE 
				WHEN COALESCE(HspDisDate, '') <> ''
					THEN CONVERT(DATETIME, HspDisDate, 101)
				ELSE NULL
				END AS DischargeDateTime
			,CASE AccType
				WHEN 'A'
					THEN 1
				WHEN 'E'
					THEN 2
				WHEN 'O'
					THEN 3
				ELSE NULL
				END AS RelatedCause1Id
			,CONVERT(DATETIME, NULL, 101) AS VisionPrescriptionDateTime
		FROM inserted i
		INNER JOIN model.InvoiceReceivables ir ON i.ReceivableId = ir.Id
		LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = i.AccState
		WHERE NOT EXISTS (SELECT * FROM model.InvoiceSupplementals WHERE Id = @InvoiceId)
	END
	
	ELSE IF EXISTS (
		SELECT *
		FROM inserted i
		WHERE FirstConsDate <> '' OR HspAdmDate <> '' OR Over90 <> '' OR Over90 <> '' OR RsnDate <> '' OR HspDisDate <> '' OR AccType <> '')
	AND EXISTS(SELECT * FROM model.InvoiceSupplementals WHERE Id = @InvoiceId)
	BEGIN
		UPDATE ivs
		SET AccidentDateTime = CASE WHEN i.FirstConsDate = '' THEN NULL ELSE CONVERT(datetime,i.FirstConsDate, 101) END
		,AccidentStateOrProvinceId = CASE 
			WHEN st.id IS NULL
				THEN NULL
			ELSE st.id
			END
		,AdmissionDateTime = CASE WHEN i.HspAdmDate = '' THEN NULL ELSE CONVERT(datetime,i.HspAdmDate, 101) END
		,ClaimDelayReasonId = CASE Over90
			WHEN 1
				THEN 1
			WHEN 2
				THEN 2
			WHEN 3
				THEN 3
			WHEN 4
				THEN 4
			WHEN 5
				THEN 5
			WHEN 6
				THEN 6
			WHEN 7
				THEN 7
			WHEN 8
				THEN 8
			WHEN 9
				THEN 9
			WHEN 10
				THEN 10
			WHEN 11
				THEN 11
			WHEN 15
				THEN 12
			ELSE NULL
			END
		,DisabilityDateTime = CASE
			WHEN COALESCE(RsnDate, '') <> ''
				THEN CONVERT(datetime,RsnDate, 101) 
			ELSE NULL END
		,DischargeDateTime = CASE 
			WHEN COALESCE(HspDisDate, '') <> ''
				THEN CONVERT(datetime, HspDisDate, 101)
			ELSE NULL END
		,RelatedCause1Id = CASE AccType
			WHEN 'A'
				THEN 1
			WHEN 'E'
				THEN 2
			WHEN 'O'
				THEN 3
			ELSE NULL
			END
		,VisionPrescriptionDateTime = CONVERT(datetime, NULL, 101)
		FROM inserted i 
		INNER JOIN model.InvoiceReceivables ir ON ir.Id = i.ReceivableId
		INNER JOIN model.InvoiceSupplementals ivs ON @InvoiceId = ivs.Id
		LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = i.AccState
	END

	-- for updating the InsurerDesignation.
	SELECT ReceivableId
	,InsurerDesignation
	INTO #NeedsLegacyInsurerDesignationUpdate
	FROM inserted

	EXCEPT

	SELECT ReceivableId
	,InsurerDesignation
	FROM deleted

	UPDATE ir
	SET LegacyInsurerDesignation = CASE WHEN z.Id IS NOT NULL THEN 'T' ELSE NULL END
	FROM model.InvoiceReceivables ir
	LEFT JOIN (
		SELECT 
		v.Id
		,v.InvoiceId
		FROM (
				SELECT
				ROW_NUMBER() OVER (PARTITION BY pi.InsuranceTypeId
					ORDER BY pi.InsuranceTypeId, pi.OrdinalId, PolicyHolderRelationshipTypeId) as rn
				,ir.InvoiceId
				,pi.Id
				FROM model.InvoiceReceivables ir
				INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
				INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
					AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= i.LegacyDateTime)
					AND pi.IsDeleted = 0
				INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id
					AND ip.StartDateTime <= i.LegacyDateTime
				INNER JOIN #NeedsLegacyInsurerDesignationUpdate nlidu ON nlidu.ReceivableId = ir.Id
				WHERE PatientInsuranceId IS NOT NULL
		) v WHERE v.rn = 1
	)z ON z.InvoiceId = ir.InvoiceId
		AND z.Id = ir.PatientInsuranceId
	INNER JOIN #NeedsLegacyInsurerDesignationUpdate nlidu ON nlidu.ReceivableId = ir.Id
	WHERE ir.PatientInsuranceId IS NOT NULL
	END
END
GO