IF EXISTS (SELECT 1 FROM model.TemplateDocuments WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL)
BEGIN

	UPDATE model.TemplateDocuments	
	SET Content= CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 'Model.ReferringDoctorFullName','Model.ReferringOrOrderingOrSupervisingProviderFullName'))  
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL

	UPDATE model.TemplateDocuments	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 'Model.ReferringDoctorNpi','Model.ReferringOrOrderingOrSupervisingProviderNpi' )) 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL

END
GO

