﻿DECLARE @sql NVARCHAR(MAX)

SELECT TOP 1 @sql = 'ALTER TABLE [model].[BillingServiceTransactions]
DROP COLUMN [PatientAggregateStatementId]; ' 
FROM sys.columns 
WHERE object_id = OBJECT_ID('model.BillingServiceTransactions') 
	AND name = 'PatientAggregateStatementId'

EXEC sys.sp_executesql @sql

GO