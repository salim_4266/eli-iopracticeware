﻿DELETE FROM model.PatientTags 
WHERE Id NOT IN (
SELECT MIN(Id) FROM model.PatientTags
GROUP BY PatientId, TagId
HAVING COUNT(*) >= 1
)