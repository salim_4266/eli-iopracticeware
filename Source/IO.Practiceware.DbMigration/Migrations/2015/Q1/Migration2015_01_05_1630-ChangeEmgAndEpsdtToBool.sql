﻿IF EXISTS (SELECT * FROM sys.objects WHERE name = 'AuditBillingServicesUpdateTrigger')
BEGIN 
	DROP TRIGGER model.AuditBillingServicesUpdateTrigger
END
GO

IF EXISTS(SELECT * FROM sys.columns sc INNER JOIN sys.tables st ON sc.object_id = st.object_id WHERE sc.name = 'Epsdt' AND st.name = 'EncounterServices') 
ALTER TABLE model.EncounterServices DROP COLUMN Epsdt 

IF EXISTS(SELECT * FROM sys.columns sc INNER JOIN sys.tables st ON sc.object_id = st.object_id WHERE sc.name = 'Emg' AND st.name = 'EncounterServices') 
ALTER TABLE model.EncounterServices DROP COLUMN Emg

UPDATE model.BillingServices SET Emg = 0 WHERE Emg IS NULL
UPDATE model.BillingServices SET Epsdt = 0 WHERE Epsdt IS NULL

ALTER TABLE model.BillingServices ALTER COLUMN Epsdt BIT NOT NULL
ALTER TABLE model.BillingServices ADD CONSTRAINT DF_BillingService_IsEpsdt DEFAULT 0 FOR Epsdt
ALTER TABLE model.BillingServices ALTER COLUMN Emg BIT NOT NULL
ALTER TABLE model.BillingServices ADD CONSTRAINT DF_BillingService_IsEmg DEFAULT 0 FOR Emg

EXEC sp_rename '[model].[BillingServices].[Epsdt]','IsEpsdt','COLUMN'
EXEC sp_rename '[model].[BillingServices].[Emg]','IsEmg','COLUMN'
