﻿--Put data in temp table and drop views
IF EXISTS(SELECT * FROM sys.views WHERE name = 'IncomingPayerCodes' AND SCHEMA_NAME(Schema_Id) = 'model')
BEGIN
	DROP VIEW model.IncomingPayerCodes
END

SELECT 
	CONVERT(INT,ROW_NUMBER() OVER (ORDER BY PayerId)) AS Id
	,PayerId AS Value
INTO #IncomingPayerCodes
FROM [dbo].[PayerMapping]
WHERE PayerId IS NOT NULL
GROUP BY PayerId 

GO

IF EXISTS(SELECT * FROM sys.views WHERE name = 'IncomingPayerCodeInsurer' AND SCHEMA_NAME(Schema_Id) = 'model')
BEGIN
	DROP VIEW model.IncomingPayerCodeInsurer
END

SELECT 
i.Id AS Insurers_Id
,p.Id AS IncomingPayerCodes_Id
INTO #IncomingPayerCodeInsurer
FROM model.Insurers i
JOIN dbo.PayerMapping pm ON pm.ClrHouseId = i.PayerCode
JOIN (
	SELECT 
	CONVERT(INT,ROW_NUMBER() OVER (ORDER BY PayerId)) AS Id
	,PayerId AS Value
	FROM [dbo].[PayerMapping]
	WHERE PayerId IS NOT NULL
	GROUP BY PayerId 
) p ON p.Value = pm.PayerId
WHERE PayerCode IS NOT NULL
GROUP BY i.Id, p.id
GO

SET QUOTED_IDENTIFIER ON;
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Check if 'IncomingPayerCodes' exists...
IF OBJECT_ID(N'model.IncomingPayerCodes', 'U') IS NULL
BEGIN
-- 'IncomingPayerCodes' does not exist, creating...
	CREATE TABLE [model].[IncomingPayerCodes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NOT NULL
);
END

-- Check if 'IncomingPayerCodeInsurer' exists...
IF OBJECT_ID(N'model.IncomingPayerCodeInsurer', 'U') IS NULL
BEGIN
-- 'IncomingPayerCodeInsurer' does not exist, creating...
	CREATE TABLE [model].[IncomingPayerCodeInsurer] (
    [IncomingPayerCodes_Id] int  NOT NULL,
    [Insurers_Id] int  NOT NULL
);
END

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'IncomingPayerCodes'
IF  NOT EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.IncomingPayerCodes', 'U'))
BEGIN
ALTER TABLE [model].[IncomingPayerCodes]
ADD CONSTRAINT [PK_IncomingPayerCodes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
END
GO

-- Creating primary key on [IncomingPayerCodes_Id], [Insurers_Id] in table 'IncomingPayerCodeInsurer'
IF NOT EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.IncomingPayerCodeInsurer', 'U'))
BEGIN
ALTER TABLE [model].[IncomingPayerCodeInsurer]
ADD CONSTRAINT [PK_IncomingPayerCodeInsurer]
    PRIMARY KEY NONCLUSTERED ([IncomingPayerCodes_Id], [Insurers_Id] ASC);
END
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [IncomingPayerCodes_Id] in table 'IncomingPayerCodeInsurer'
IF NOT EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_IncomingPayerCodeInsurer_IncomingPayerCode'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
BEGIN
	IF OBJECTPROPERTY(OBJECT_ID('[model].[IncomingPayerCodes]'), 'IsUserTable') = 1
	ALTER TABLE [model].[IncomingPayerCodeInsurer]
	ADD CONSTRAINT [FK_IncomingPayerCodeInsurer_IncomingPayerCode]
		FOREIGN KEY ([IncomingPayerCodes_Id])
		REFERENCES [model].[IncomingPayerCodes]
			([Id])
		ON DELETE NO ACTION ON UPDATE NO ACTION;
END
GO

-- Creating foreign key on [Insurers_Id] in table 'IncomingPayerCodeInsurer'
IF NOT EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_IncomingPayerCodeInsurer_Insurer'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')

IF OBJECTPROPERTY(OBJECT_ID('[model].[Insurers]'), 'IsUserTable') = 1
BEGIN 
	ALTER TABLE [model].[IncomingPayerCodeInsurer]
	ADD CONSTRAINT [FK_IncomingPayerCodeInsurer_Insurer]
		FOREIGN KEY ([Insurers_Id])
		REFERENCES [model].[Insurers]
			([Id])
		ON DELETE NO ACTION ON UPDATE NO ACTION;
END

-- Creating non-clustered index for FOREIGN KEY 'FK_IncomingPayerCodeInsurer_Insurer'
IF NOT EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_IncomingPayerCodeInsurer_Insurer')

CREATE INDEX [IX_FK_IncomingPayerCodeInsurer_Insurer]
ON [model].[IncomingPayerCodeInsurer]
    ([Insurers_Id]);
GO

--Migrate the data

IF OBJECT_ID('tempdb..#IncomingPayerCodes') IS NOT NULL 
BEGIN
	SET IDENTITY_INSERT model.IncomingPayerCodes ON
	INSERT INTO model.IncomingPayerCodes (Id, Value)
	SELECT Id, Value FROM #IncomingPayerCodes
	SET IDENTITY_INSERT model.IncomingPayerCodes OFF
END

IF OBJECT_ID('tempdb..#IncomingPayerCodeInsurer') IS NOT NULL 
BEGIN 
	INSERT INTO model.IncomingPayerCodeInsurer (Insurers_Id, IncomingPayerCodes_Id)
	SELECT Insurers_Id, IncomingPayerCodes_Id FROM #IncomingPayerCodeInsurer
END