﻿IF EXISTS (
		SELECT *
		FROM model.TemplateDocuments
		WHERE DisplayName = 'CMS-1500'
			AND Content IS NOT NULL
		)
BEGIN
	UPDATE model.TemplateDocuments
	SET Content = CONVERT(VARBINARY(max), REPLACE(REPLACE(CONVERT(VARCHAR(max), Content), 'id="ResubmitCodePreQualifier" value="7"','id="ResubmitCodePreQualifier" value="@Model.ClaimFrequencyTypeCodeId"'),'id="ResubmitCodePreQualifier" value="@Model.ClaimFrequencyType"','id="ResubmitCodePreQualifier" value="@Model.ClaimFrequencyTypeCodeId"'))
	FROM model.TemplateDocuments
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL

END
GO
