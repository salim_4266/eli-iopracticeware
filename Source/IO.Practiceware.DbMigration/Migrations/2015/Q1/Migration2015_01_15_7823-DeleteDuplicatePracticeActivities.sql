--Multiple activity records
DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
		AND pa.activityid < pa2.activityid
		AND pa.patientid = pa2.patientid
		AND pa.appointmentid <> 0
		AND pa.status = 'D' AND pa2.status <> 'D')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
		AND pa.activityid > pa2.activityid
		AND pa.patientid = pa2.patientid
		AND pa.appointmentid <> 0
		AND pa.status = 'D' 
		AND pa2.status <> 'D')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
		AND pa.activityid < pa2.activityid
		AND pa.patientid = pa2.patientid
		AND pa.appointmentid <> 0
		AND pa.status = 'D' 
		AND pa2.status = 'D')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
		AND pa.activityid > pa2.activityid
		AND pa.patientid = pa2.patientid
		AND pa.appointmentid <> 0
		AND pa.status = 'H' 
		AND pa2.status = 'W')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
		AND pa.activityid < pa2.activityid
		AND pa.patientid = pa2.patientid
		AND pa.appointmentid <> 0
		AND pa.status = 'H' 
		AND pa2.status = 'W')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
		AND pa.activityid < pa2.activityid
		AND pa.patientid = pa2.patientid
		AND pa.appointmentid <> 0
		AND pa.status = 'G' 
		AND pa2.status = 'G')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid > pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'H' 
	AND pa2.status = 'E')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid < pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'H' 
	AND pa2.status = 'E')


DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid > pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'H'
	AND pa2.status = 'H')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid > pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'X'
	AND pa2.status = 'X')


DELETE FROM practiceactivity WHERE activityid IN
(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid <> pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'H' 
	AND pa2.status = 'G')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid <> pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'E' 
	AND pa2.status = 'W')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid <> pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'G'
	AND pa2.status = 'W')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid < pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'G'
	AND pa2.status = 'E')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid <> pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'W'
	AND pa2.status = 'W')

DELETE FROM practiceactivity WHERE activityid IN
	(SELECT pa2.activityid
	FROM practiceactivity pa
	INNER JOIN practiceactivity pa2 ON pa2.appointmentid = pa.appointmentid
	AND pa.activityid <> pa2.activityid
	AND pa.patientid = pa2.patientid
	AND pa.appointmentid <> 0
	AND pa.status = 'E'
	AND pa2.status = 'E')




--practice activity invalid statuses
IF OBJECT_ID('PracticeActivityMultipleActivityRecordsTrigger') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER PracticeActivityMultipleActivityRecordsTrigger')
END

GO

DECLARE @sql NVARCHAR(max)
SET @sql = 
	'CREATE TRIGGER PracticeActivityMultipleActivityRecordsTrigger
	ON PracticeActivity
	FOR INSERT
	AS
	BEGIN
		SET NOCOUNT ON
		IF EXISTS(
				SELECT * 
				FROM PracticeActivity pa WITH(NOLOCK) 
				INNER JOIN (SELECT * FROM inserted EXCEPT SELECT * FROM deleted) i ON i.AppointmentId = pa.AppointmentId
				GROUP BY pa.AppointmentId
				HAVING COUNT(*) > 1)
			AND (SELECT COUNT(*) FROM inserted) = 1
			AND dbo.GetNoCheck() = 0
	
		BEGIN
			WITH EncounterStatuses AS (
				SELECT pa.ActivityId, 
					CASE pa.[Status]
							WHEN ''W''
								THEN 1
							WHEN ''M''
								THEN 2
							WHEN ''E''
								THEN 3
							WHEN ''G''
								THEN 4
							WHEN ''H''
								THEN 5
							WHEN ''U''
								THEN 6
							WHEN ''D''
								THEN 7
							ELSE 0
						END
							AS StatusId
				FROM PracticeActivity pa
				JOIN inserted i ON i.AppointmentId = pa.AppointmentId AND pa.ActivityId <> i.ActivityId
			)

			DELETE FROM PracticeActivity WHERE ActivityId = (SELECT TOP 1 es2.ActivityId FROM EncounterStatuses es2 ORDER BY es2.StatusId)
		END
	END'

IF NOT EXISTS (SELECT name FROM sys.triggers where name = 'PracticeActivityMultipleActivityRecordsTrigger' AND type = 'TR')
BEGIN
	SET @sql = REPLACE(@sql,'ALTER TRIGGER','CREATE TRIGGER')
END

EXEC (@sql)
GO

DELETE FROM dbo.PracticeActivity WHERE AppointmentId = 0
GO

;WITH CTE AS (
SELECT 
pa.ActivityId 
,ROW_NUMBER() OVER (PARTITION BY AppointmentId ORDER BY pa.ActivityId) AS rn
FROM dbo.PracticeActivity pa
WHERE pa.AppointmentId IN (
		SELECT 
		pa.AppointmentId
		FROM dbo.PracticeActivity pa
		GROUP BY pa.AppointmentId
		HAVING COUNT(pa.AppointmentId) > 1
	)
)
DELETE FROM CTE WHERE rn > 1

GO