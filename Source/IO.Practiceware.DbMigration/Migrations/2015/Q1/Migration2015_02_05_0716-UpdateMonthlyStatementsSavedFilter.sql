﻿--Bug #16930,18117
IF EXISTS (
		SELECT *
		FROM model.ApplicationSettings
		WHERE Value LIKE '%Monthly Statements%'
			AND NAME LIKE '%StatementsFilter%' AND MachineName IS NULL AND UserId IS NULL
		)
BEGIN
	DECLARE @oldValue VARCHAR(max);
	DECLARE @newValue VARCHAR(max);
	DECLARE @StartIndex INT;
	DECLARE @value NVARCHAR(MAX);

	SET @value = (
			SELECT TOP 1 Value
			FROM model.ApplicationSettings
			WHERE Value LIKE '%Monthly Statements%'
				AND NAME LIKE '%StatementsFilter%' AND MachineName IS NULL AND UserId IS NULL
			)
	SET @newValue = '<d1p1:TransactionStatusIds xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" z:Id="15" z:Size="2">
						<d2p1:int>1</d2p1:int>
						<d2p1:int>2</d2p1:int>
					</d1p1:TransactionStatusIds>'
	SET @StartIndex = CHARINDEX(LOWER('<d1p1:TransactionStatusIds'), LOWER(@value), 1)

	IF @StartIndex > 0
	BEGIN
		DECLARE @EndIndex INT;

		SET @EndIndex = CHARINDEX('</d1p1:TransactionStatusIds>', @value, @StartIndex) + LEN('</d1p1:TransactionStatusIds>') 
		SET @oldValue = SUBSTRING(@value, @StartIndex, @EndIndex - @StartIndex)

		UPDATE model.ApplicationSettings
		SET Value = REPLACE(@value, @oldValue, @newValue) 
		WHERE Value LIKE '%Monthly Statements%'
		AND Name LIKE '%StatementsFilter%' AND MachineName IS NULL AND UserId IS NULL
	END

--Change the Name
	SET @value = (
			SELECT TOP 1 Value
			FROM model.ApplicationSettings
			WHERE Value LIKE '%<d1p1:Name z:Id="2">Current Opened Aggregate Statement(s) (Monthly Statements)</d1p1:Name>%'
				AND NAME LIKE '%StatementsFilter%' AND MachineName IS NULL AND UserId IS NULL
			)
	SET @newValue = '<d1p1:Name z:Id="2">Monthly Statements</d1p1:Name>'
	SET @oldValue = '<d1p1:Name z:Id="2">Current Opened Aggregate Statement(s) (Monthly Statements)</d1p1:Name>'

	SET @StartIndex = CHARINDEX(LOWER('<d1p1:Name z:Id="2">Current Opened Aggregate Statement(s) (Monthly Statements)</d1p1:Name>'), LOWER(@value), 1)
	IF @StartIndex > 0
	BEGIN
		UPDATE model.ApplicationSettings
		SET Value = REPLACE(@value, @oldValue, @newValue) 
		WHERE Value LIKE '%<d1p1:Name z:Id="2">Current Opened Aggregate Statement(s) (Monthly Statements)</d1p1:Name>%'
		AND Name LIKE '%StatementsFilter%' AND MachineName IS NULL AND UserId IS NULL
	END
END
GO

