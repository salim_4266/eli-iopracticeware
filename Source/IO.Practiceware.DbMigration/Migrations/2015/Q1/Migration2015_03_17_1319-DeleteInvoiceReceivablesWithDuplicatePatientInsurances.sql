﻿DECLARE @DuplicateInvoiceReceivables TABLE
(
  InvalidDuplicateInvoiceReceivableId INT, 
  ReplacementInvoiceReceivableId INT
)

INSERT INTO @DuplicateInvoiceReceivables (InvalidDuplicateInvoiceReceivableId, ReplacementInvoiceReceivableId)
SELECT InvalidIr.Id as InvalidInvoiceReceivableId, ValidIr.Id AS ValidInvoiceReceivableId
FROM
	(SELECT Id, InvoiceId, PatientInsuranceId 
	FROM model.InvoiceReceivables 
	WHERE Id NOT IN (
			SELECT MIN(Id) 
			FROM model.InvoiceReceivables
			GROUP BY InvoiceId, PatientInsuranceId 
			HAVING COUNT(*) >= 1)) AS InvalidIr
	LEFT JOIN 
	(SELECT MIN(Id) as Id, InvoiceId, PatientInsuranceId 
	FROM model.InvoiceReceivables
	GROUP BY InvoiceId, PatientInsuranceId 
	HAVING COUNT(*) >= 1) AS ValidIr 
	ON InvalidIr.InvoiceId = ValidIr.InvoiceId 
	AND InvalidIr.PatientInsuranceId = ValidIr.PatientInsuranceId

-- Update invalid InvoiceReceivableId with valid InvoiceReceivableId in model.BillingServiceTransactions
UPDATE bst SET bst.InvoiceReceivableId = dup.ReplacementInvoiceReceivableId
FROM model.BillingServiceTransactions bst 
JOIN @DuplicateInvoiceReceivables AS dup ON bst.InvoiceReceivableId = dup.InvalidDuplicateInvoiceReceivableId

-- Update invalid InvoiceReceivableId with valid InvoiceReceivableId in model.Adjustments
UPDATE adj SET adj.InvoiceReceivableId = dup.ReplacementInvoiceReceivableId
FROM model.Adjustments adj 
JOIN @DuplicateInvoiceReceivables AS dup ON adj.InvoiceReceivableId = dup.InvalidDuplicateInvoiceReceivableId

-- Update invalid InvoiceReceivableId with valid InvoiceReceivableId in model.FinancialInformations
UPDATE fi SET fi.InvoiceReceivableId = dup.ReplacementInvoiceReceivableId
FROM model.FinancialInformations fi 
JOIN @DuplicateInvoiceReceivables AS dup ON fi.InvoiceReceivableId = dup.InvalidDuplicateInvoiceReceivableId

-- Finally delete all the duplicate InvoiceReceivables having same PatientInsurances for the same invoice
DELETE FROM model.InvoiceReceivables 
WHERE Id IN (
SELECT InvalidDuplicateInvoiceReceivableId FROM @DuplicateInvoiceReceivables)