﻿IF OBJECT_ID('fdb_rxnorm_mapping.tblAllergyMappingFDB','U') IS NOT NULL
BEGIN
	DECLARE @int_system_type_id INT
	SET @int_system_type_id = (SELECT TOP 1 system_type_id FROM sys.types WHERE name = 'int')

	DELETE FROM fdb_rxnorm_mapping.tblAllergyMappingFDB WHERE MappedID IS NULL

	;WITH CTE AS (
	SELECT 
	ROW_NUMBER() OVER (PARTITION BY CompositeAllergyID, MappedId ORDER BY CompositeAllergyID, MappedId) AS Id
	,CompositeAllergyID
	,MappedId
	FROM fdb_rxnorm_mapping.tblAllergyMappingFDB
	)
	DELETE FROM CTE WHERE Id > 1
	
	DECLARE @object_id_tblAllergyMappingFDB INT
	SET @object_id_tblAllergyMappingFDB = OBJECT_ID('fdb_rxnorm_mapping.tblAllergyMappingFDB','U')

	IF NOT EXISTS (
	SELECT * 
	FROM sys.columns c
	WHERE c.object_id = @object_id_tblAllergyMappingFDB
		AND c.system_type_id = @int_system_type_id
		AND name = 'CompositeAllergyID'
		AND c.is_nullable = 0
	)
	BEGIN
		EXEC ('ALTER TABLE fdb_rxnorm_mapping.tblAllergyMappingFDB ALTER COLUMN CompositeAllergyID INT NOT NULL')
	END

	IF NOT EXISTS (
	SELECT * 
	FROM sys.columns c
	WHERE c.object_id = @object_id_tblAllergyMappingFDB
		AND c.system_type_id = @int_system_type_id
		AND name = 'MappedID'
		AND c.is_nullable = 0
	)
	BEGIN
		EXEC ('ALTER TABLE fdb_rxnorm_mapping.tblAllergyMappingFDB ALTER COLUMN MappedID INT NOT NULL')
	END

	IF OBJECTPROPERTY(@object_id_tblAllergyMappingFDB, 'TableHasPrimaryKey') = 0
	BEGIN
		EXEC ('ALTER TABLE fdb_rxnorm_mapping.tblAllergyMappingFDB ADD CONSTRAINT PK_tblAllergyMappingFDB PRIMARY KEY (CompositeAllergyID,MappedID)')
	END
END
GO

IF OBJECT_ID('fdb_rxnorm_mapping.tblDrugMappingFDB','U') IS NOT NULL
BEGIN
	DECLARE @int_system_type_id INT
	SET @int_system_type_id = (SELECT TOP 1 system_type_id FROM sys.types WHERE name = 'int')

	DECLARE @object_id_tblDrugMappingFDB INT
	SET @object_id_tblDrugMappingFDB = OBJECT_ID('fdb_rxnorm_mapping.tblDrugMappingFDB','U')

	IF NOT EXISTS (
	SELECT * 
	FROM sys.columns c
	WHERE c.object_id = @object_id_tblDrugMappingFDB
		AND c.system_type_id = @int_system_type_id
		AND name = 'DrugID'
		AND c.is_nullable = 0
	)
	BEGIN
		EXEC ('ALTER TABLE fdb_rxnorm_mapping.tblDrugMappingFDB ALTER COLUMN DrugID INT NOT NULL')
	END

	IF NOT EXISTS (
	SELECT * 
	FROM sys.columns c
	WHERE c.object_id = @object_id_tblDrugMappingFDB
		AND c.system_type_id = @int_system_type_id
		AND name = 'MappedID'
		AND c.is_nullable = 0
	)
	BEGIN
		EXEC ('ALTER TABLE fdb_rxnorm_mapping.tblDrugMappingFDB ALTER COLUMN MappedID INT NOT NULL')
	END

	IF OBJECTPROPERTY(@object_id_tblDrugMappingFDB, 'TableHasPrimaryKey') = 0
	BEGIN
		EXEC ('ALTER TABLE fdb_rxnorm_mapping.tblDrugMappingFDB ADD CONSTRAINT PK_tblDrugMappingFDB PRIMARY KEY (DrugID,MappedID)')
	END
END
GO

IF SCHEMA_ID('fdb_rxnorm_mapping') IS NULL
	EXEC ('CREATE SCHEMA [fdb_rxnorm_mapping]')
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fdb_rxnorm_mapping.tblAllergyMappingFDB') AND type in (N'U'))
BEGIN
	CREATE TABLE fdb_rxnorm_mapping.tblAllergyMappingFDB(
		CompositeAllergyID int NOT NULL,
		[Source] nvarchar(max) NULL,
		ConceptID nvarchar(max) NULL,
		ConceptType nvarchar(max) NULL,
		VocabularySource nvarchar(max) NULL,
		VocabularyTypeID nvarchar(max) NULL,
		MappedDetailTypeID nvarchar(max) NULL,
		MappedID int NOT NULL,
		MappedName nvarchar(max) NULL,
		TouchDate nvarchar(max) NULL,
		ActiveStatus nvarchar(max) NULL,
		CONSTRAINT PK_tblAllergyMappingFDB PRIMARY KEY CLUSTERED 
	(
		CompositeAllergyID ASC,
		MappedID ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fdb_rxnorm_mapping.tblDrugMappingFDB') AND type in (N'U'))
BEGIN
	CREATE TABLE fdb_rxnorm_mapping.tblDrugMappingFDB(
		DrugID int NOT NULL,
		DrugTypeID nvarchar(max) NULL,
		VocabularySource nvarchar(max) NULL,
		VocabularyTypeID int NULL,
		MappedTypeID int NULL,
		MappedDetailTypeID nvarchar(max) NULL,
		MappedID int NOT NULL,
		MappedName nvarchar(max) NULL,
		IsPrescribable int NULL,
		TouchDate nvarchar(max) NULL,
		ActiveStatus nvarchar(max) NULL,
		CONSTRAINT PK_tblDrugMappingFDB PRIMARY KEY CLUSTERED 
	(
		DrugID ASC,
		MappedID ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO