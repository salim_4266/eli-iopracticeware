IF EXISTS (SELECT * FROM sys.triggers WHERE name ='UpdatePracticeTransactionJournalTransactionStatus')
BEGIN
	DROP TRIGGER model.UpdatePracticeTransactionJournalTransactionStatus
END