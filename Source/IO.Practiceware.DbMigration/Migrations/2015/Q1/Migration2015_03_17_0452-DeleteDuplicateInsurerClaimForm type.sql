EXEC('DROP TRIGGER model.AuditInsurerClaimFormsDeleteTrigger')

DELETE icf
FROM model.InsurerClaimForms icf
JOIN (
	SELECT MIN(Id) AS IdToKeep, InvoiceTypeId, InsurerId, ClaimFormTypeId
	FROM model.InsurerClaimForms
	GROUP BY InvoiceTypeId, InsurerId, ClaimFormTypeId
	HAVING COUNT(*) > 1 
) dups ON	
	dups.InsurerId = icf.InsurerId
	AND dups.ClaimFormTypeId = icf.ClaimFormTypeId
	AND dups.InvoiceTypeId = icf.InvoiceTypeId
WHERE dups.IdToKeep <> icf.Id