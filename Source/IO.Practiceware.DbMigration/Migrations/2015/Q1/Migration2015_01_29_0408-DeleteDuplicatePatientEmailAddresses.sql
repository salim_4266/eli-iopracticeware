﻿DELETE FROM model.PatientEmailAddresses 
WHERE Id NOT IN (
SELECT MIN(Id) FROM model.PatientEmailAddresses
GROUP BY Value, PatientId, EmailAddressTypeId
HAVING COUNT(*) >= 1)