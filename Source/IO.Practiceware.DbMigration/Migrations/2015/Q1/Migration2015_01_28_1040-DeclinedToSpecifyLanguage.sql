﻿IF NOT EXISTS(SELECT * FROM model.Languages WHERE Name = 'Declined To Specify')
BEGIN
	INSERT INTO model.Languages ([IsArchived],[Name],[OrdinalId],[Abbreviation])
	VALUES(0,'Declined To Specify',1,'decline')
END