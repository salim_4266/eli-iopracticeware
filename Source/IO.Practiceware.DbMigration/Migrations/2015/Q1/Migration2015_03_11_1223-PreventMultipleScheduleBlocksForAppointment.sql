--Bug #17827.
--Prevent Multiple Appointments having same BlockId.
IF OBJECT_ID('[model].[PreventMultipleScheduleBlocksForAppointment]', 'TR') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER [model].[PreventMultipleScheduleBlocksForAppointment]')
END
GO
CREATE TRIGGER [model].[PreventMultipleScheduleBlocksForAppointment] ON [model].[ScheduleBlockAppointmentCategories]
FOR INSERT
	,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @msrepl_object_id INT

	SET @msrepl_object_id = OBJECT_ID('[model].[ScheduleBlockAppointmentCategories]')

	DECLARE @retcode INT

	EXEC @retcode = sp_check_for_sync_trigger @msrepl_object_id

	IF @retcode = 1
		RETURN

	DECLARE @AppointmentId AS INT
	DECLARE @ScheduleBlockId AS INT
	DECLARE @MaxAppointmentId AS INT
	DECLARE @AppointmentCategoryId AS INT

	SELECT @AppointmentId = AppointmentId
		,@ScheduleBlockId = ScheduleBlockId
	FROM Inserted

	IF EXISTS (
			SELECT ScheduleBlockId
			FROM model.ScheduleBlockAppointmentCategories
			WHERE ScheduleBlockId = @ScheduleBlockId
				AND AppointmentCategoryId = @AppointmentCategoryId
			GROUP BY ScheduleBlockId
			HAVING COUNT(*) > 1
			)
	BEGIN
		SELECT @MaxAppointmentId = MAX(AppointmentId)
		FROM model.ScheduleBlockAppointmentCategories
		WHERE ScheduleBlockId = @ScheduleBlockId
			AND AppointmentCategoryId = @AppointmentCategoryId

		IF (@AppointmentId > @MaxAppointmentId)
			SET @MaxAppointmentId = @AppointmentId

		UPDATE model.ScheduleBlockAppointmentCategories
		SET AppointmentId = NULL
		WHERE ScheduleBlockId = @ScheduleBlockId
			AND AppointmentId <> @MaxAppointmentId
			AND AppointmentCategoryId = @AppointmentCategoryId
	END
END