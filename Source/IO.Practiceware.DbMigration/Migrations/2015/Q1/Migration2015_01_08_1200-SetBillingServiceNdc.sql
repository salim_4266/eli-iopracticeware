﻿IF EXISTS (SELECT * FROM sys.objects WHERE name = 'AuditBillingServicesUpdateTrigger')
BEGIN 
	DROP TRIGGER model.AuditBillingServicesUpdateTrigger
END
GO

UPDATE bs
SET bs.Ndc = CONVERT(NVARCHAR(MAX),es.Ndc)
FROM  model.BillingServices bs
INNER JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
WHERE es.Ndc IS NOT NULL