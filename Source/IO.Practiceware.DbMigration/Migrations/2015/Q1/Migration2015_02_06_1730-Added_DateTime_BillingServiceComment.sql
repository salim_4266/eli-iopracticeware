﻿IF EXISTS (
		SELECT *
		FROM sys.triggers
		WHERE object_id = OBJECT_ID(N'[model].[AuditBillingServiceCommentsInsertTrigger]')
		)
BEGIN
	EXEC('DROP TRIGGER [model].AuditBillingServiceCommentsInsertTrigger')
END
GO

IF EXISTS (
		SELECT *
		FROM sys.triggers
		WHERE object_id = OBJECT_ID(N'[model].[AuditBillingServiceCommentsUpdateTrigger]')
		)
BEGIN
	EXEC('DROP TRIGGER [model].AuditBillingServiceCommentsUpdateTrigger')
END
GO

IF EXISTS (
		SELECT *
		FROM sys.triggers
		WHERE object_id = OBJECT_ID(N'[model].[AuditBillingServiceCommentsDeleteTrigger]')
		)
BEGIN
	EXEC('DROP TRIGGER [model].AuditBillingServiceCommentsDeleteTrigger')
END
GO

-- add DateTime column to BillingServiceComments
IF NOT EXISTS (
		SELECT object_name(object_id)
		FROM sys.columns
		WHERE NAME = 'DateTime'
			AND object_name(object_id) = 'BillingServiceComments'
		)
BEGIN
	ALTER TABLE model.BillingServiceComments ADD DateTime DateTime NULL
END