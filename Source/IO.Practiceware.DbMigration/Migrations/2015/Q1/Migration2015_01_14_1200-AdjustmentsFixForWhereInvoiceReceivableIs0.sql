﻿-- Find All Adjustments With InvoiceReceivableId = 0 Where the Patient Made a payment
SELECT*
INTO #AdjustmentsToUpdate
FROM model.Adjustments
WHERE InvoiceReceivableId = 0 
AND BillingServiceId IS NOT NULL
AND FinancialSourceTypeId = 2

-- Find Invoice receivables to set the adjustments InvoiceReceivableId to
SELECT atu.Id AS AdjustmentId
,ir.Id AS InvoiceReceivableId
INTO #InvoiceReceivableIds
FROM #AdjustmentsToUpdate atu
INNER JOIN model.BillingServices bs ON atu.BillingServiceId = bs.Id
INNER JOIN model.Invoices i ON bs.InvoiceId = i.Id
INNER JOIN model.InvoiceReceivables ir ON ir.PatientInsuranceId IS NULL AND i.Id = ir.InvoiceId

-- Update InvoiceReceivableIds
UPDATE model.Adjustments
SET InvoiceReceivableId = iris.InvoiceReceivableId
FROM model.Adjustments a
INNER JOIN #InvoiceReceivableIds iris ON a.Id = iris.AdjustmentId

-- Drop Temp Tables
DROP TABLE #InvoiceReceivableIds
DROP TABLE #AdjustmentsToUpdate
