﻿IF OBJECT_ID('model.DoctorInsurerAssignments') IS NOT NULL 
	AND OBJECT_ID('model.Users') IS NOT NULL 
BEGIN
	DELETE FROM model.DoctorInsurerAssignments WHERE DoctorId NOT IN (SELECT Id FROM model.Users)
END
GO