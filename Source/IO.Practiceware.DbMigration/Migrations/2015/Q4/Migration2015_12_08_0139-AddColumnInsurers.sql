
IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'[model].[Insurers]') AND name = 'IsIcd10Decimal')
BEGIN
ALTER TABLE [model].[Insurers] ADD IsIcd10Decimal BIT NOT NULL DEFAULT 0
END
GO

