IF EXISTS (SELECT 1 FROM sys.Tables WHERE name = 'CMSREports')
BEGIN
DELETE FROM CMSReports
WHERE CMSREPName IN (
'Stage 1 - CORE 1: Computerized Provider Order Entry (CPOE)',
'Stage 1 - CORE 1: Computerized Provider Order Entry (CPOE) alternate',
'Stage 1 - CORE 3: Maintain Problem List',
'Stage 1 - CORE 4: e-Prescribing (eRx)',
'Stage 1 - CORE 5: Active Medication List',
'Stage 1 - CORE 6: Medication Allergy List',
'Stage 1 - CORE 7: Record Demographics',
'Stage 1 - CORE 8: Record Vital Signs',
'Stage 1 - CORE 8: Record Vital Signs (Exclusion 1 or 3)',
'Stage 1 - CORE 8: Record Vital Signs (Exclusion 4)',
'Stage 1 - CORE 9: Record Smoking Status',
'Stage 1 - CORE 11: View, Download, Transmit Timely Access',
'Stage 1 - CORE 12: Clinical Summaries',
'Stage 1 - MENU 2: Clinical Lab-Test Results',
'Stage 1 - MENU 4: Patient Reminders',
'Stage 1 - MENU 5: Patient-specific Education Resources',
'Stage 1 - MENU 6: Perform medication reconciliation for referral patients',
'Stage 1 - MENU 7: Summary of Care',
'Stage 2 - CORE 3: Record Demographics',
'Stage 2 - CORE 4: Record Vital Signs',
'Stage 2 - CORE 4: Record Vital Signs (Exclusion 1 or 3)',
'Stage 2 - CORE 4: Record Vital Signs (Exclusion 4)',
'Stage 2 - CORE 5: Record Smoking Status',
'Stage 2 - CORE 8: Clinical Summaries',
'Stage 2 � CORE 10: Clinical Lab - Test Results',
'Stage 2 - CORE 12: Patient Reminders',
'Stage 2 - MENU 2: Electronic Notes',
'Stage 2 - MENU 3: Imaging results',
'Stage 2 - MENU 4: Family Health History')



UPDATE CMSReports SET CMSRepName = 'Objective 3: 1-CPOE - Medication Orders'
, CMSSubHeading = 'Objective 3: 1-CPOE - Medication Orders'
WHERE CMSRepCode = 'CPOE 201.1'


UPDATE CMSReports SET CMSRepName = 'Objective 3: 2-CPOE - Radiology Orders'
, CMSSubHeading = 'Objective 3: 2-CPOE - Radiology Orders'
WHERE CMSRepCode = 'CPOE 201.2'

UPDATE CMSReports SET CMSRepName = 'Objective 3: 3-CPOE - Laboratory Orders'
, CMSSubHeading = 'Objective 3: 3-CPOE - Laboratory Orders'
WHERE CMSRepCode = 'CPOE 201.3'

UPDATE CMSReports SET CMSRepName = 'Objective 4: Electronic Prescribing (eRx)'
, CMSSubHeading = 'Objective 4: Electronic Prescribing (eRx)'
WHERE CMSRepCode = 'CPOE 202'

UPDATE CMSReports SET CMSRepName = 'Objective 8: 1-Patient Electronic Access - Timely Access'
, CMSSubHeading = 'Objective 8: 1-Patient Electronic Access - Timely Access'
WHERE CMSRepCode = 'CPOE 207.1'

UPDATE CMSReports SET CMSRepName = 'Objective 8: 2-Patient Electronic Access - Patient Use'
, CMSSubHeading = 'Objective 8: 2-Patient Electronic Access - Patient Use'
WHERE CMSRepCode = 'CPOE 207.2'

UPDATE CMSReports SET CMSRepName = 'Objective 6: Patient Specific Education'
, CMSSubHeading = 'Objective 6: Patient Specific Education'
WHERE CMSRepCode = 'CPOE 213'

UPDATE CMSReports SET CMSRepName = 'Objective 7: Medication Reconciliation'
, CMSSubHeading = 'Objective 7: Medication Reconciliation'
WHERE CMSRepCode = 'CPOE 214'

UPDATE CMSReports SET CMSRepName = 'Objective 5: 1-HIE - Create Summary of Care for Each Referral'
, CMSSubHeading = 'Objective 5: 1-HIE - Create Summary of Care for Each Referral'
WHERE CMSRepCode = 'CPOE 215.1'

UPDATE CMSReports SET CMSRepName = 'Objective 5: 1-HIE - Electronically Transmit Summary of Care'
, CMSSubHeading = 'Objective 5: 1-HIE - Create Summary of Care for Each Referral'
WHERE CMSRepCode = 'CPOE 215.2'


UPDATE CMSReports SET CMSRepName = 'Objective 9: Secure Messaging'
, CMSSubHeading = 'Objective 9: Secure Messaging'
WHERE CMSRepCode = 'CPOE 217'

END