
IF EXISTS (SELECT 1 FROM sys.objects where name = 'GetPaymentsAccountHistory')
BEGIN
DROP PROCEDURE [model].[GetPaymentsAccountHistory]
END
 
GO

CREATE PROCEDURE model.GetPaymentsAccountHistory (

	 @PatientId INT, @StartDate DATETIME = NULL

	,@EndDate DATETIME = NULL

	,@PaymentStartDate DATETIME = NULL

	,@PaymentEndDate DATETIME = NULL

 )

AS

BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

   SET @StartDate = CONVERT(NVARCHAR(10),@StartDate,101)
	SET @EndDate = CONVERT(NVARCHAR(10),@EndDate,101)
	SET @PaymentStartDate = CONVERT(NVARCHAR(10),@PaymentStartDate,101)
	SET @PaymentEndDate = CONVERT(NVARCHAR(10),@PaymentEndDate,101)

	DECLARE @SumCharges INT



	SELECT e.PatientId

	,SUM(CAST(bs.Unit * bs.UnitCharge AS DECIMAL(12,2))) AS SumCharges

	INTO #SumCharges

	FROM model.invoices i

	INNER JOIN model.encounters e ON e.id = i.EncounterId

	INNER JOIN model.BillingServices bs ON bs.InvoiceId = i.id

	WHERE e.PatientId = @PatientId 

	AND (CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101)) BETWEEN COALESCE(@StartDate,CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101))) 

	AND COALESCE(@EndDate,CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101))))

	GROUP BY e.PatientId



	SELECT COALESCE(adj.PatientId, e.PatientId) AS PatientId, adj.Amount, at.IsDebit, at.IsCash, adj.PostedDateTime, fb.InsurerId, adj.FinancialSourceTypeId, adj.BillingServiceId

	INTO #Adjustments

	FROM model.Adjustments adj

	INNER JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId

	INNER JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId

	LEFT JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId

	LEFT JOIN model.Invoices i ON i.Id = ir.InvoiceId

	LEFT JOIN model.Encounters e ON e.Id = i.EncounterId

	WHERE (adj.PatientId = @PatientId OR e.PatientId = @PatientID)

		AND (CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101))

		BETWEEN COALESCE(ISNULL(@PaymentStartDate,@StartDate),CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101))) AND COALESCE(ISNULL(@PaymentEndDate,@EndDate),CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101))))



	IF(@PaymentStartDate <> '' OR @PaymentEndDate <> '')

	BEGIN

		SELECT (Unit * UnitCharge) As TotalCharge INTO #TotalCharge FROM model.BillingServices WHERE Id IN(SELECT BillingServiceId FROM #Adjustments)

		SELECT @SumCharges = SUM(TotalCharge) FROm #TotalCharge

	END



	SELECT CAST(SumCharges - SUM(PatientPayments) -	SUM(InsurerPayments) -	SUM(Adjustments) AS DECIMAL(10,2)) AS AccountBalance

		,CAST(SUM(PatientPayments) AS DECIMAL(10,2)) AS PatientPayments

		,CAST(SUM(InsurerPayments) AS DECIMAL(10,2)) AS InsurerPayments

		,CAST(SUM(Adjustments) AS DECIMAL(10,2)) AS Adjustments

		,CAST(SumCharges AS DECIMAL(10,2)) AS SumCharges FROM (

	SELECT	

		ISNULL(@SumCharges,sc.SumCharges) AS SumCharges

		,CASE 

			WHEN adj.FinancialSourceTypeId = 2

				AND IsCash = 1

				THEN COALESCE(SUM(CASE WHEN IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END),0)

			ELSE 0

			END AS PatientPayments

		,CASE 

			WHEN adj.FinancialSourceTypeId = 1

				AND isCash = 1

				THEN COALESCE(SUM(CASE WHEN IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END),0)

			ELSE 0

			END AS InsurerPayments

		,CASE 

			WHEN isCash = 0 OR (isCash = 1 AND adj.FinancialSourceTypeId NOT IN (1,2))

				THEN COALESCE(SUM(CASE WHEN IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END),0)

			ELSE 0

			END AS Adjustments

	FROM #SumCharges sc

	LEFT JOIN #Adjustments adj ON adj.PatientId = sc.PatientId

	WHERE adj.PatientId IS NULL OR 

		(

			((CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101))

				BETWEEN COALESCE(ISNULL(@PaymentStartDate,@StartDate),CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101))) AND COALESCE(ISNULL(@PaymentEndDate,@EndDate),CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101))))

			)

			OR

			((CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101))

				BETWEEN COALESCE(@StartDate,CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101))) AND COALESCE(@EndDate,CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101))))

			)

		)

	GROUP BY 

		adj.PatientId,

		adj.insurerid,

		adj.isCash,

		adj.IsDebit,

		adj.FinancialSourceTypeId,

		sc.SumCharges

		 ) AS v 

	GROUP BY SumCharges

END


