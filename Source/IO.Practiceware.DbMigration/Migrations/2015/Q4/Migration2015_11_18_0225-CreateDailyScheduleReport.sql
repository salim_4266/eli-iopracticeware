
IF NOT EXISTS(SELECT 1 FROM model.[Permissions] WHERE Name = 'Daily Pick-Up Schedule')
  BEGIN
	INSERT INTO model.[Permissions](Name, PermissionCategoryId)
	SELECT 'Daily Pick-Up Schedule', PermissionCategoryId
	FROM model.[Permissions] WHERE Name = 'Daily Schedule'
  END

GO
IF NOT EXISTS (SELECT 1 FROM model.Reports WHERE Name = 'Daily Pick-Up Schedule')
  BEGIN
	INSERT INTO model.Reports(Name, ReportCategoryId, ReportTypeId, Content, Documentation, PermissionId)
	SELECT 'Daily Pick-Up Schedule', ReportCategoryId, ReportTypeId, '<?xml version="1.0" encoding="utf-8"?>
<Report DataSourceName="GetDailyPickUpSchedule" Width="10.0000006357829in" Name="DailyPickUpSchedule" SnapToGrid="False" SnapToSnapLines="False" ShowDimensions="False" xmlns="http://schemas.telerik.com/reporting/2012/3.5">
  <Style>
    <Font Name="Arial" />
  </Style>
  <DataSources>
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="model.GetDailyPickUpSchedule" SelectCommandType="StoredProcedure" CommandTimeout="240" Name="GetDailyPickUpSchedule">
      <Parameters>
        <SqlDataSourceParameter DbType="DateTime" Name="@StartDate">
          <Value>
            <String>=Parameters.StartDate.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="DateTime" Name="@EndDate">
          <Value>
            <String>=Parameters.EndDate.Value</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="String" Name="@ServiceLocations">
          <Value>
            <String>=Join(",",Parameters.ServiceLocations.Value)</String>
          </Value>
        </SqlDataSourceParameter>
        <SqlDataSourceParameter DbType="String" Name="@Doctors">
          <Value>
            <String>=Join(",",Parameters.Providers.Value)</String>
          </Value>
        </SqlDataSourceParameter>
      </Parameters>
      <DefaultValues>
        <SqlDataSourceParameter DbType="DateTime" Name="@StartDate" />
        <SqlDataSourceParameter DbType="DateTime" Name="@EndDate" />
        <SqlDataSourceParameter DbType="String" Name="@ServiceLocations" />
        <SqlDataSourceParameter DbType="String" Name="@Doctors" />
      </DefaultValues>
    </SqlDataSource>
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT DISTINCT Id, ShortName &#xD;&#xA;FROM model.ServiceLocations&#xD;&#xA;ORDER BY ShortName ASC" CommandTimeout="600" Name="ServiceLocations" />
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT &#xD;&#xA;Id, UserName &#xD;&#xA;FROM model.Users WHERE __EntityType__ = ''Doctor''&#xD;&#xA;AND IsArchived = 0" CommandTimeout="600" Name="Doctors" />
    <SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT TOP 1 &#xD;&#xA;&#x9;a.Id&#xD;&#xA;&#x9;,bo.Name AS MainOfficeName&#xD;&#xA;&#x9;,a.Line1 AS MainOfficeLine1&#xD;&#xA;&#x9;,a.Line2 AS MainOfficeLine2&#xD;&#xA;&#x9;,a.City AS MainOfficeCity&#xD;&#xA;&#x9;,sop.Abbreviation AS MainOfficeState&#xD;&#xA;&#x9;,SUBSTRING(a.PostalCode, 0,6) AS MainOfficePostalCode&#xD;&#xA;FROM model.BillingOrganizationAddresses a&#xD;&#xA;JOIN model.StateOrProvinces sop ON a.StateOrProvinceId = sop.Id&#xD;&#xA;JOIN model.BillingOrganizations bo ON bo.Id = a.BillingOrganizationId&#xD;&#xA;WHERE bo.IsMain = 1" CommandTimeout="240" Name="GetMainOfficeAddress" />
  </DataSources>
  <Items>
    <DetailSection Height="0.395705262819926in" Name="detailSection1">
      <Style>
        <BorderStyle Bottom="Dotted" />
      </Style>
      <Items>
        <TextBox Width="0.6in" Height="0.2in" Left="0in" Top="0in" Value="{AppTime}" CanGrow="False" Name="textBox16">
          <Style VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="1.4999213218689in" Height="0.2in" Left="0.6in" Top="0in" Value="{LastName}{Suffix}, {FirstName} {MiddleName}" CanGrow="True" Name="textBox18">
          <Style VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="0.400000063578288in" Height="0.2in" Left="2.80617205301921in" Top="0in" Value="{Age}" CanGrow="False" Name="textBox19">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="0.700000063578288in" Height="0.2in" Left="3.20674228668213in" Top="0in" Value="{PatientId}" CanGrow="False" Name="textBox21">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="0.530600865681967in" Height="0.2in" Left="5.22973759969075in" Top="0in" Value="{PatientType}" CanGrow="True" Name="textBox22">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="0.637262980143229in" Height="0.2in" Left="6.82716433207194in" Top="0in" Value="{PriorId}" CanGrow="False" CanShrink="True" Name="textBox23">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="1.75613098144532in" Height="0.2in" Left="7.47291819254558in" Top="0in" Value="{Notes}" CanGrow="True" Name="textBox33">
          <Style VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="1.06666817267736in" Height="0.200000002980232in" Left="5.76041730244954in" Top="0in" Value="{PhoneNumber}" CanGrow="False" Name="textBox39">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="0.706093162298203in" Height="0.200000002980232in" Left="2.10000022252401in" Top="0in" Value="{DOB}" CanGrow="False" Name="textBox41">
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <HtmlTextBox Width="1.31651910146078in" Height="0.195784131685892in" Left="3.9094492594401in" Top="0in" Value="{Address}" Name="htmlTextBox1">
          <Style VerticalAlign="Middle">
            <Font Size="8pt" />
          </Style>
        </HtmlTextBox>
      </Items>
    </DetailSection>
    <ReportHeaderSection Height="1.09996045430502in" Name="reportHeaderSection2">
      <Style TextAlign="Center">
        <Font Name="Arial" />
      </Style>
      <Items>
        <TextBox Width="2in" Height="0.2in" Left="8.00000063578288in" Top="0in" Value="Report run on:    {Now().ToString(&quot;MM/dd/yyyy&quot;)}" Name="textBox17">
          <Style TextAlign="Right" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="2in" Height="0.2in" Left="8.00000063578288in" Top="0.199999968210856in" Value="{Now().ToString(&quot;hh:mm tt&quot;)}" Name="textBox5">
          <Style TextAlign="Right" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
        <TextBox Width="3.5999215443929in" Height="0.2in" Left="3.2000789642334in" Top="0in" Value="Daily Pick-Up Schedule" Anchoring="Top, Left, Right" Name="textBox14">
          <Style TextAlign="Center" VerticalAlign="Middle" LineStyle="Solid" LineWidth="4pt">
            <Font Name="Arial" Size="14pt" Bold="True" Underline="False" />
          </Style>
        </TextBox>
        <List DataSourceName="GetMainOfficeAddress" Width="3.2in" Height="0.6in" Left="0in" Top="0in" Name="list2">
          <Body>
            <Cells>
              <TableCell RowIndex="0" ColumnIndex="0" RowSpan="1" ColumnSpan="1">
                <ReportItem>
                  <Panel Width="3.2in" Height="0.6in" Left="0in" Top="0in" Name="panel2">
                    <Items>
                      <TextBox Width="3.2in" Height="0.2in" Left="0in" Top="0.2in" Value="{MainOfficeLine1}" Name="textBox25">
                        <Style>
                          <Font Name="Calibri" Size="10pt" Bold="True" />
                        </Style>
                      </TextBox>
                      <TextBox Width="3.2in" Height="0.2in" Left="0in" Top="0.4in" Value="{MainOfficeCity} , {MainOfficeState} {MainOfficePostalCode}" Name="textBox26">
                        <Style>
                          <Font Name="Calibri" Size="10pt" Bold="True" />
                        </Style>
                      </TextBox>
                      <TextBox Width="3.2in" Height="0.2in" Left="0in" Top="0in" Value="{MainOfficeName}" Name="textBox1">
                        <Style>
                          <Font Name="Calibri" Size="10pt" Bold="True" />
                        </Style>
                      </TextBox>
                    </Items>
                  </Panel>
                </ReportItem>
              </TableCell>
            </Cells>
            <Columns>
              <Column Width="3.2in" />
            </Columns>
            <Rows>
              <Row Height="0.6in" />
            </Rows>
          </Body>
          <Corner />
          <Style TextAlign="Left" VerticalAlign="Middle">
            <Font Name="Arial" />
          </Style>
          <RowGroups>
            <TableGroup Name="DetailGroup">
              <Groupings>
                <Grouping />
              </Groupings>
            </TableGroup>
          </RowGroups>
          <ColumnGroups>
            <TableGroup Name="ColumnGroup" />
          </ColumnGroups>
        </List>
        <TextBox Width="3.59992170333863in" Height="0.199842557311058in" Left="3.2000789642334in" Top="0.404448906580607in" Value="Start Date: {Parameters.StartDate.Value.ToString(&quot;MM/dd/yyyy&quot;)} End Date: {Parameters.EndDate.Value.ToString(&quot;MM/dd/yyyy&quot;)} " Format="{0:d}" Name="textBox145">
          <Style TextAlign="Center" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" Bold="False" />
          </Style>
        </TextBox>
        <TextBox Width="9.99984280268351in" Height="0.197837889194489in" Left="0.00003941853841146in" Top="0.6043701171875in" Value="Providers: {=IsNull(Join(&quot;,&quot;,Parameters.Providers.Label),&quot;All&quot;)}" Format="" Name="textBox142">
          <Style TextAlign="Center" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" Bold="False" />
          </Style>
        </TextBox>
        <TextBox Width="9.99996137619019in" Height="0.19999997317791in" Left="0.00003941853841316in" Top="0.802286783854167in" Value="ServiceLocations: {= IsNull(Join(&quot;,&quot;,Parameters.ServiceLocations.Label),&quot;All&quot;)}" Format="" Name="textBox141">
          <Style TextAlign="Center" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" Bold="False" />
          </Style>
        </TextBox>
      </Items>
    </ReportHeaderSection>
    <PageHeaderSection Height="0.200039386749268in" Name="pageHeaderSection1">
      <Items>
        <TextBox Width="2in" Height="0.200000002980232in" Left="8.00000063578288in" Top="0in" Value="Page {PageNumber}" Name="textBox45">
          <Style TextAlign="Right" VerticalAlign="Middle">
            <Font Name="Arial" Size="8pt" />
          </Style>
        </TextBox>
      </Items>
    </PageHeaderSection>
  </Items>
  <StyleSheet>
    <StyleRule>
      <Style>
        <Padding Left="2pt" Right="2pt" />
      </Style>
      <Selectors>
        <TypeSelector Type="TextItemBase" />
        <TypeSelector Type="HtmlTextBox" />
      </Selectors>
    </StyleRule>
  </StyleSheet>
  <PageSettings>
    <PageSettings PaperKind="Letter" Landscape="True">
      <Margins>
        <MarginsU Left="0.5in" Right="0.5in" Top="0.5in" Bottom="0.5in" />
      </Margins>
    </PageSettings>
  </PageSettings>
  <Sortings>
    <Sorting Expression="= Fields.FullDateTime" Direction="Asc" />
  </Sortings>
  <Groups>
    <Group Name="group">
      <GroupHeader>
        <GroupHeaderSection PrintOnEveryPage="True" Height="0.414534131685893in" Name="groupHeaderSection">
          <Style>
            <Font Size="8pt" />
          </Style>
          <Items>
            <TextBox Width="0.400000065565109in" Height="0.200000002980232in" Left="2.80617213249207in" Top="0in" Value="Age" Name="textBox34">
              <Style TextAlign="Left" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.52484146753947in" Height="0.200000002980232in" Left="7.43349266052246in" Top="0in" Value="Notes" Angle="0" Name="textBox15">
              <Style VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.49367129802704in" Height="0.200000002980232in" Left="0.600078761577606in" Top="0in" Value="Name" Name="textBox3">
              <Style VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="0.600000023841858in" Height="0.200000002980232in" Left="0in" Top="0in" Value="Time" Name="textBox2">
              <Style VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="0.568023324012757in" Height="0.200000002980232in" Left="5.17148208618164in" Top="0in" Value="Tags" Name="textBox12">
              <Style TextAlign="Left" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="0.699762980143229in" Height="0.200000002980232in" Left="6.73341433207194in" Top="0in" Value="Prior Id" Angle="0" CanShrink="False" Name="textBox11">
              <Style TextAlign="Left" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="0.662578165531158in" Height="0.200000002980232in" Left="3.20674228668213in" Top="0in" Value="Id" Name="textBox10">
              <Style TextAlign="Left" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="0.98333477973938in" Height="0.200000002980232in" Left="5.75000063578288in" Top="0in" Value="Phone" Name="textBox38">
              <Style TextAlign="Left" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="0.70609313249588in" Height="0.200000122189522in" Left="2.10000014305115in" Top="0in" Value="DOB" Name="textBox40">
              <Style VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="1.27485243479411in" Height="0.195784136652947in" Left="3.87819925944011in" Top="0in" Value="Address" Name="textBox6">
              <Style>
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
          </Items>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.0998818079630535in" Name="groupFooterSection">
          <Style Visible="False" />
        </GroupFooterSection>
      </GroupFooter>
    </Group>
    <Group Name="group1">
      <GroupHeader>
        <GroupHeaderSection Height="0.2in" Name="groupHeaderSection1">
          <Style>
            <BorderStyle Bottom="Solid" />
          </Style>
          <Items>
            <TextBox Width="10.0000006357829in" Height="0.2in" Left="0in" Top="0in" Value="Appointments for {AppDate}" Format="{0:d}" Name="textBox4">
              <Style TextAlign="Left" VerticalAlign="Middle">
                <Font Name="Arial" Size="11pt" Bold="True" />
              </Style>
            </TextBox>
          </Items>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.299842389424642in" Name="groupFooterSection1">
          <Items>
            <TextBox Width="0.7in" Height="0.2in" Left="2.80000019073486in" Top="0in" Value="= COUNT(AppointmentId)" Format="{0:N0}" Name="textBox24">
              <Style TextAlign="Left" VerticalAlign="Middle">
                <Font Name="Arial" />
              </Style>
            </TextBox>
            <TextBox Width="2.79992141723633in" Height="0.2in" Left="0in" Top="0in" Value="Total for {AppDate}:" Name="textBox27">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
          </Items>
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="= AppDate" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= Fulldatetime" Direction="Asc" />
      </Sortings>
    </Group>
    <Group Name="group2">
      <GroupHeader>
        <GroupHeaderSection Height="0.200078829129537in" Name="groupHeaderSection2">
          <Style>
            <Font Size="11pt" />
          </Style>
          <Items>
            <TextBox Width="2.5in" Height="0.200000002980232in" Left="0in" Top="0in" Value="= ServiceLocationShortName" Name="textBox7">
              <Style VerticalAlign="Middle">
                <Font Name="Arial" Size="11pt" Bold="True" />
              </Style>
            </TextBox>
          </Items>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.2in" Name="groupFooterSection2">
          <Items>
            <TextBox Width="2.79992141723633in" Height="0.2in" Left="0in" Top="0in" Value="Total for {ServiceLocationShortName}:" Name="textBox31">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="0.7in" Height="0.2in" Left="2.80000019073486in" Top="0in" Value="= COUNT(AppointmentId)" Format="{0:N0}" Name="textBox8">
              <Style TextAlign="Left" VerticalAlign="Middle">
                <Font Name="Arial" />
              </Style>
            </TextBox>
          </Items>
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="= ServiceLocationShortName" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= ServiceLocationShortName" Direction="Asc" />
      </Sortings>
    </Group>
    <Group Name="group4">
      <GroupHeader>
        <GroupHeaderSection Height="0.199999992052714in" Name="groupHeaderSection4">
          <Style Visible="True" />
          <Items>
            <TextBox Width="2.49999987284342in" Height="0.2in" Left="0in" Top="0in" Value="{Provider}" Name="textBox28">
              <Style Visible="True" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <HtmlTextBox Width="0.8in" Height="0.2in" Left="6.90625190734863in" Top="0in" Value="Scheduling" Name="ReportCategory">
              <Style Visible="False" VerticalAlign="Middle">
                <Font Name="Arial" />
              </Style>
            </HtmlTextBox>
          </Items>
        </GroupHeaderSection>
      </GroupHeader>
      <GroupFooter>
        <GroupFooterSection Height="0.2in" Name="groupFooterSection4">
          <Items>
            <TextBox Width="2.79992141723633in" Height="0.2in" Left="0in" Top="0in" Value="Total for {Provider}:" Angle="0" Name="textBox29">
              <Style TextAlign="Right" VerticalAlign="Middle">
                <Font Name="Arial" Bold="True" />
              </Style>
            </TextBox>
            <TextBox Width="0.7in" Height="0.2in" Left="2.80000019073486in" Top="0in" Value="= COUNT(AppointmentId)" Format="{0:N0}" Name="textBox30">
              <Style TextAlign="Left" VerticalAlign="Middle">
                <Font Name="Arial" />
              </Style>
            </TextBox>
          </Items>
        </GroupFooterSection>
      </GroupFooter>
      <Groupings>
        <Grouping Expression="= Provider" />
      </Groupings>
      <Sortings>
        <Sorting Expression="= Provider" Direction="Asc" />
      </Sortings>
    </Group>
  </Groups>
  <ReportParameters>
    <ReportParameter Name="StartDate" Type="DateTime" Text="Start Date" Visible="True" AllowBlank="False">
      <Value>
        <String></String>
      </Value>
    </ReportParameter>
    <ReportParameter Name="EndDate" Type="DateTime" Text="End Date" Visible="True" AllowBlank="False">
      <Value>
        <String>= Parameters.StartDate.Value</String>
      </Value>
    </ReportParameter>
    <ReportParameter Name="ServiceLocations" Text="Service Location" Visible="True" MultiValue="True" AllowNull="True">
      <AvailableValues DataSourceName="ServiceLocations" DisplayMember="ShortName" ValueMember="Id" />
    </ReportParameter>
    <ReportParameter Name="Providers" Text="Scheduled Provider" Visible="True" MultiValue="True" AllowNull="True">
      <AvailableValues DataSourceName="Doctors" DisplayMember="UserName" ValueMember="Id" />
    </ReportParameter>
  </ReportParameters>
</Report>', Documentation, (SELECT Id from Model.[Permissions] WHERE Name = 'Daily Pick-Up Schedule')
	FROM Model.Reports WHERE NAME = 'Daily Schedule'
  END