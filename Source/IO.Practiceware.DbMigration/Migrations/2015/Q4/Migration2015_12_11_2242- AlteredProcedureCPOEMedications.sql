IF EXISTS (SELECT 1 FROM sys.objects where name = 'USP_CMS_Stage2_CPOEMedications')
BEGIN
DROP PROCEDURE [model].[USP_CMS_Stage2_CPOEMedications]
END
GO

CREATE PROCEDURE [model].[USP_CMS_Stage2_CPOEMedications]  
(    
@StartDate DateTime,    
@EndDate DateTime,    
@ResourceIds int    
)    
AS   

BEGIN    
DECLARE @TotParm INT;    
DECLARE @CountParm INT;   
SET @TotParm=0;    
SET @CountParm=0;    

		--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
		EncounterId INT
		,EncounterDate DATETIME
		,PatientId INT
		,DOB DATETIME
		,Age INT
		)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate
		,@EndDate
		,@ResourceIds

	--Denominator
	SELECT ipp.EncounterId ,ipp.EncounterDate AS AppointmentDate,ipp.PatientId,ipp.DOB,ipp.Age--,MedicationId AS PatientMedicationID,ClinicalDataSourceTypeId
	INTO #Denominator
	FROM #InitialPatientPopulation ipp
	INNER JOIN model.EncounterMedicationOrders pm ON pm.EncounterId = ipp.EncounterId

	--numerator  
	SELECT DISTINCT d.PatientId
		,pm.Id AS PatientMedicationId
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN PatientMedications pm ON pm.PatientId = d.PatientId

SET @TotParm = (SELECT COUNT (DISTINCT PatientId) from #Denominator)
SET @CountParm = (SELECT COUNT (DISTINCT PatientId) from #Numerator)

SELECT @TotParm as Denominator,@CountParm as Numerator   

select d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.AppointmentDate--, PatientMedicationId
from #Denominator d
inner join model.patients p on p.id = d.PatientId
order by p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

select n.PatientId, p.FirstName, p.LastName, n.PatientMedicationId
from #Numerator n
inner join model.patients p on p.id = n.PatientId
order by p.LastName, p.FirstName, n.PatientId, n.PatientMedicationId

DROP TABLE #Denominator
DROP TABLE #Numerator
DROP TABLE #InitialPatientPopulation

END 
