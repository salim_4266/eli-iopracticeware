IF EXISTS (SELECT 1 FROM sys.objects where name = 'USP_CMS_GetMedicationReconStatus_Stage2')
BEGIN
DROP PROCEDURE [dbo].[USP_CMS_GetMedicationReconStatus_Stage2]
END
GO

CREATE PROCEDURE [dbo].[USP_CMS_GetMedicationReconStatus_Stage2] (

	@StartDate NVARCHAR(200)

	,@EndDate NVARCHAR(200)

	,@ResourceIds NVARCHAR(1000)

	)

AS

BEGIN

	---#15-Medication Reconclication--------------------------------------------------

	DECLARE @TotParm INT

	DECLARE @CountParm INT



	SET @TotParm = 0

	SET @CountParm = 0



	DECLARE @PatientId NVARCHAR(20)

	DECLARE @path NVARCHAR(max)



	IF @ResourceIds LIKE '%,%'

		RAISERROR (

				'Please ONLY send one doctor id at a TIME.'

				,16

				,2

				)



	DECLARE @userId NVARCHAR(20)

	DECLARE @userType NVARCHAR(20)

	DECLARE @transactionid NVARCHAR(max)

	DECLARE @medReconciliationNumerator INT



	SET @userId = 'demo'

	SET @userType = 'D'



	--Denominator

	SELECT DISTINCT ap.PatientId

	INTO #patients

	FROM Appointments ap

	WHERE AppDate BETWEEN @StartDate

			AND @EndDate

		AND ap.ResourceId1 = @ResourceIDs

		AND ScheduleStatus = 'D'

		AND AppTime > 0



	IF EXISTS (

			SELECT *

			FROM model.ApplicationSettings

			WHERE NAME = 'ServerDataPath'

			)

		SELECT @Path = Value

		FROM model.ApplicationSettings

		WHERE NAME = 'ServerDataPath'

	ELSE

		SET @Path = @@SERVERNAME + '\IOP\Pinpoint\MyScan'



	CREATE TABLE #DenominatorScans (

		PatientId INT

		,DATETIME DATETIME

		,Path NVARCHAR(MAX)

		)



	DECLARE patCursor CURSOR

	FOR

	SELECT PatientId

	FROM #patients



	OPEN patCursor



	FETCH NEXT

	FROM patCursor

	INTO @PatientId



	WHILE @@FETCH_STATUS = 0

	BEGIN

		SELECT ROW_NUMBER() OVER (

				ORDER BY path

				) AS Row

			,REVERSE(dbo.ExtractTextValue('-', REVERSE(path), '_')) AS PatientId

			,LEFT(REVERSE(dbo.ExtractTextValue('.', REVERSE(path), '-')), 8) AS DATETIME

			,path

		INTO #paths

		FROM [IO.Practiceware.SqlClr].GetFiles(@Path + CONVERT(NVARCHAR(10), @PatientId), '*.*')

		WHERE dbo.ExtractTextValue(@PatientId + '\', path, '_') IN (

				'C-CDA'

				,'CCR-'

				,'CCR'

				,'CCD'

				)



		INSERT INTO #DenominatorScans

		SELECT p.patientId

			,p.DATETIME

			,p.path

		FROM #paths p

		WHERE CONVERT(NVARCHAR(8), p.DATETIME, 112) BETWEEN @StartDate

				AND @EndDate



		DROP TABLE #paths



		FETCH NEXT

		FROM patCursor

		INTO @PatientId

	END



	CLOSE patCursor



	DEALLOCATE patCursor



	SELECT @TotParm = COUNT(*)

	FROM Appointments ap

	INNER JOIN Resources re ON re.ResourceId = ap.ResourceId1

	INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId

		AND re.ResourceId = @ResourceIds

	WHERE AppDate BETWEEN @StartDate

			AND @EndDate

		AND ScheduleStatus = 'D'

		AND AppTime > 0

		AND pc.ClinicalType = 'F'

		AND pc.Symptom = '/MEDS RECONCILIATION'

		AND pc.FindingDetail = '*TRANSITIONOFCARE-MEDREC=T14911 <TRANSITIONOFCARE-MEDREC>'

		AND pc.STATUS = 'A'



	DECLARE @TotParmScans INT



	SELECT @TotParmScans = COUNT(DISTINCT Path)

	FROM #DenominatorScans ds

	INNER JOIN model.Patients p ON ds.PatientId = p.Id

	WHERE p.DefaultUserId = @ResourceIDs



	--numerator

	--SELECT TOP 1 @transactionid = transactionid

	--FROM model.NewCropUtilizationReports

	--WHERE userid = @userId

	--	AND UserType = @userType

	--	AND LicensedPrescriberId = @ResourceIds

	--ORDER BY TIMESTAMP DESC



	--EXEC [IO.Practiceware.SqlClr].GetMeaningfulUseUtilizationReport @transactionId

	--	,@userId

	--	,@userType

	--	,NULL

	--	,NULL

	--	,NULL

	--	,NULL

	--	,NULL

	--	,NULL

	--	,NULL

	--	,NULL

	--	,NULL

	--	,NULL

	--	,@medReconciliationNumerator OUTPUT

	--	,NULL

	--	,NULL

	--	,NULL

	--	,NULL

	--	,NULL



	SELECT @TotParm = @TotParm + @TotParmScans



	--IF @CountParm > @TotParm

	--	SET @CountParm = @TotParm

	--ELSE

	--	SELECT @CountParm = medReconciliationNumerator



	SELECT @TotParm AS Denominator

		,@CountParm AS Numerator

END