IF EXISTS (
	SELECT CONVERT(VARCHAR(MAX), Content) FROM model.TemplateDocuments 
	WHERE DisplayName = 'CMS-1500'
		AND Content IS NOT NULL)
BEGIN	
	UPDATE model.TemplateDocuments	
	SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), 
	'.Replace(".","")',

	'' )) 
	FROM model.TemplateDocuments 	 
	WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL

END 
