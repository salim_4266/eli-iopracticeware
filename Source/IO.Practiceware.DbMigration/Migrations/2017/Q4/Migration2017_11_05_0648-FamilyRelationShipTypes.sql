GO
/****** Object:  Table [model].[FamilyRelationships]    Script Date: 11/29/2017 5:41:25 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[FamilyRelationships]') AND type in (N'U'))
DROP TABLE [model].[FamilyRelationships]
GO
/****** Object:  Table [model].[FamilyRelationships]    Script Date: 11/29/2017 5:41:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[FamilyRelationships]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[FamilyRelationships](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[IsDeactivated] [bit] NOT NULL,
	[OrdinalId] [int] NOT NULL,
 CONSTRAINT [PK_FamilyRelationships] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

SET QUOTED_IDENTIFIER OFF 
SET IDENTITY_INSERT [model].[FamilyRelationships] ON 

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (1,	'Aunt',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (2,	'Brother',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (3,	'Child',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (4,	'Cousin',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (5,	'Father',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (6,	'Grandfather',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (7,	'Grandmother',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (8,	'Grandparent',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (9,	'Greataunt',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (10,'Greatuncle',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (11,	'Mother',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (12,	'Nephew',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (13,	'Niece',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (14,	'Other',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (15,	'Parent',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (16,	'Sibling',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (17,	'Sister',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (18,	'Uncle',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (19,	'Son',	0,	1)

INSERT INTO [model].[FamilyRelationships]([Id],[Name],[IsDeactivated],[OrdinalId]) 
VALUES (20,	'Daughter',	0,	1)

SET IDENTITY_INSERT [model].[FamilyRelationships] OFF

SET ANSI_PADDING ON
GO
