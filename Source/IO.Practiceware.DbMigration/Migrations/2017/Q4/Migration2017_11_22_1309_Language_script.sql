
/****** Object:  Table [model].[LanguagesTmp]    Script Date: 12/13/2017 4:50:55 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[LanguagesTmp]') AND type in (N'U'))
DROP TABLE [model].[LanguagesTmp]
GO
/****** Object:  Table [model].[LanguagesTmp]    Script Date: 12/13/2017 4:50:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ansi_padding ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[LanguagesTmp]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[LanguagesTmp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[IsArchived] [bit] NOT NULL DEFAULT ((0)),
	[OrdinalId] [int] NOT NULL DEFAULT ((1)),
	[Abbreviation] [nvarchar](max) NULL,
 CONSTRAINT [PK_LanguagesTmp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ansi_padding OFF
GO

SET IDENTITY_INSERT [model].[LanguagesTmp] ON 

INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (4180, N'English', 0, 6, N'en')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (4181, N'French', 0, 7, N'fr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (4182, N'German', 0, 0, N'de')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (4183, N'Italian', 0, 3, N'it')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (4184, N'Japanese', 0, 4, N'ja')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (4185, N'Korean', 0, 5, N'ko')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (4186, N'Hindi', 0, 1, N'hi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (4187, N'Arabic', 0, 2, N'ar')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (4188, N'Urdu', 0, 8, N'ur')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (4191, N'Polish', 0, 9, N'pl')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (4192, N'Russian', 0, 10, N'ru')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (4193, N'Spanish', 0, 11, N'spa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000001, N'Afar', 0, 12, N'aa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000002, N'Abkhazian', 0, 13, N'ab')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000003, N'Afrikaans', 0, 14, N'af')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000004, N'Akan', 0, 15, N'ak')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000005, N'Albanian', 0, 16, N'sq')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000006, N'Amharic', 0, 17, N'am')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000007, N'Aragonese', 0, 18, N'an')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000008, N'Armenian', 0, 19, N'hy')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000009, N'Assamese', 0, 20, N'as')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000010, N'Avaric', 0, 21, N'av')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000011, N'Avestan', 0, 22, N'ae')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000012, N'Aymara', 0, 23, N'ay')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000013, N'Azerbaijani', 0, 24, N'az')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000014, N'Bashkir', 0, 25, N'ba')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000015, N'Bambara', 0, 26, N'bm')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000016, N'Basque', 0, 27, N'eu')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000017, N'Belarusian', 0, 28, N'be')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000018, N'Bengali', 0, 29, N'bn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000019, N'Bihari languages', 0, 30, N'bh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000020, N'Bislama', 0, 31, N'bi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000022, N'Bosnian', 0, 32, N'bs')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000023, N'Breton', 0, 33, N'br')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000024, N'Bulgarian', 0, 34, N'bg')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000025, N'Burmese', 0, 35, N'my')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000026, N'Catalan; Valencian', 0, 36, N'ca')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000028, N'Chamorro', 0, 37, N'ch')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000029, N'Chechen', 0, 38, N'ce')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000030, N'Chinese', 0, 39, N'zh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000031, N'Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic', 0, 40, N'chu')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000032, N'Chuvash', 0, 41, N'cv')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000033, N'Cornish', 0, 42, N'kw')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000034, N'Corsican', 0, 43, N'co')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000035, N'Cree', 0, 44, N'cr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000037, N'Czech', 0, 45, N'cs')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000038, N'Danish', 0, 46, N'da')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000039, N'Divehi; Dhivehi; Maldivian', 0, 47, N'dv')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000040, N'Dutch; Flemish', 0, 48, N'nl')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000041, N'Dzongkha', 0, 49, N'dz')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000042, N'Greek, Modern (1453-)', 0, 50, N'el')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000043, N'Esperanto', 0, 51, N'eo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000044, N'Estonian', 0, 52, N'et')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000046, N'Ewe', 0, 53, N'ee')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000047, N'Faroese', 0, 54, N'fo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000049, N'Fijian', 0, 55, N'fj')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000050, N'Finnish', 0, 56, N'fi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000051, N'Western Frisian', 0, 57, N'fy')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000052, N'Fulah', 0, 58, N'ff')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000054, N'Gaelic; Scottish Gaelic', 0, 59, N'gd')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000055, N'Irish', 0, 60, N'ga')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000056, N'Galician', 0, 61, N'gl')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000057, N'Manx', 0, 62, N'gv')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000059, N'Guarani', 0, 63, N'gn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000060, N'Gujarati', 0, 64, N'gu')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000061, N'Haitian; Haitian Creole', 0, 65, N'ht')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000062, N'Hausa', 0, 66, N'ha')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000063, N'Hebrew', 0, 67, N'he')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000064, N'Herero', 0, 68, N'hz')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000065, N'Hiri Motu', 0, 69, N'ho')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000066, N'Croatian', 0, 70, N'hr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000067, N'Hungarian', 0, 71, N'hu')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000069, N'Igbo', 0, 72, N'ig')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000070, N'Icelandic', 0, 73, N'is')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000071, N'Ido', 0, 74, N'io')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000072, N'Sichuan Yi; Nuosu', 0, 75, N'ii')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000073, N'Inuktitut', 0, 76, N'iu')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000074, N'Interlingue; Occidental', 0, 77, N'ie')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000075, N'Interlingua (International Auxiliary Language Association)', 0, 78, N'ina')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000076, N'Indonesian', 0, 79, N'id')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000077, N'Inupiaq', 0, 80, N'ik')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000079, N'Javanese', 0, 81, N'jv')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000080, N'Kalaallisut; Greenlandic', 0, 82, N'kl')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000081, N'Kannada', 0, 83, N'kn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000082, N'Kashmiri', 0, 84, N'ks')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000083, N'Georgian', 0, 85, N'ka')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000084, N'Kanuri', 0, 86, N'kr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000085, N'Kazakh', 0, 87, N'kk')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000086, N'Central Khmer', 0, 88, N'km')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000087, N'Kikuyu; Gikuyu', 0, 89, N'ki')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000088, N'Kinyarwanda', 0, 90, N'rw')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000089, N'Kirghiz; Kyrgyz', 0, 91, N'ky')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000090, N'Komi', 0, 92, N'kv')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000091, N'Kongo', 0, 93, N'kg')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000092, N'Kuanyama; Kwanyama', 0, 94, N'kj')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000093, N'Kurdish', 0, 95, N'ku')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000094, N'Lao', 0, 96, N'lo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000095, N'Latin', 0, 97, N'la')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000096, N'Latvian', 0, 98, N'lv')
GO
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000097, N'Limburgan; Limburger; Limburgish', 0, 99, N'lim')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000098, N'Lingala', 0, 100, N'ln')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000099, N'Lithuanian', 0, 101, N'lt')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000100, N'Luxembourgish; Letzeburgesch', 0, 102, N'lb')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000101, N'Luba-Katanga', 0, 103, N'lu')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000102, N'Ganda', 0, 104, N'lg')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000103, N'Macedonian', 0, 105, N'mk')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000104, N'Marshallese', 0, 106, N'mh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000105, N'Malayalam', 0, 107, N'ml')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000106, N'Maori', 0, 108, N'mi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000107, N'Marathi', 0, 109, N'mr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000108, N'Malay', 0, 110, N'ms')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000110, N'Malagasy', 0, 111, N'mg')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000111, N'Maltese', 0, 112, N'mt')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000112, N'Mongolian', 0, 113, N'mn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000116, N'Nauru', 0, 114, N'na')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000117, N'Navajo; Navaho', 0, 115, N'nv')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000118, N'Ndebele, South; South Ndebele', 0, 116, N'nr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000119, N'Ndebele, North; North Ndebele', 0, 117, N'nd')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000120, N'Ndonga', 0, 118, N'ng')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000121, N'Nepali', 0, 119, N'ne')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000123, N'Norwegian Nynorsk; Nynorsk, Norwegian', 0, 120, N'nno')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000124, N'Bokm�l, Norwegian; Norwegian Bokm�l', 0, 121, N'nob')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000125, N'Norwegian', 0, 122, N'no')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000126, N'Chichewa; Chewa; Nyanja', 0, 123, N'ny')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000127, N'Occitan (post 1500)', 0, 124, N'oc')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000128, N'Ojibwa', 0, 125, N'oj')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000129, N'Oriya', 0, 126, N'or')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000130, N'Oromo', 0, 127, N'om')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000131, N'Ossetian; Ossetic', 0, 128, N'os')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000132, N'Panjabi; Punjabi', 0, 129, N'pa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000133, N'Persian', 0, 130, N'fa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000134, N'Pali', 0, 131, N'pi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000135, N'Portuguese', 0, 132, N'pt')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000136, N'Pushto; Pashto', 0, 133, N'ps')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000137, N'Quechua', 0, 134, N'qu')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000138, N'Romansh', 0, 135, N'rm')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000139, N'Romanian; Moldavian; Moldovan', 0, 136, N'ro')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000141, N'Rundi', 0, 137, N'rn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000142, N'Sango', 0, 138, N'sg')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000143, N'Sanskrit', 0, 139, N'sa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000144, N'Sinhala; Sinhalese', 0, 140, N'si')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000145, N'Slovak', 0, 141, N'sk')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000147, N'Slovenian', 0, 142, N'sl')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000148, N'Northern Sami', 0, 143, N'se')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000149, N'Samoan', 0, 144, N'sm')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000150, N'Shona', 0, 145, N'sn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000151, N'Sindhi', 0, 146, N'sd')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000152, N'Somali', 0, 147, N'so')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000153, N'Sotho, Southern', 0, 148, N'st')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000154, N'Spanish; Castilian', 0, 149, N'es')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000156, N'Sardinian', 0, 150, N'sc')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000157, N'Serbian', 0, 151, N'sr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000158, N'Swati', 0, 152, N'ss')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000159, N'Sundanese', 0, 153, N'su')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000160, N'Swahili', 0, 154, N'sw')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000161, N'Swedish', 0, 155, N'sv')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000162, N'Tahitian', 0, 156, N'ty')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000163, N'Tamil', 0, 157, N'ta')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000164, N'Tatar', 0, 158, N'tt')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000165, N'Telugu', 0, 159, N'te')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000166, N'Tajik', 0, 160, N'tg')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000167, N'Tagalog', 0, 161, N'tl')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000168, N'Thai', 0, 162, N'th')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000169, N'Tibetan', 0, 163, N'bo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000170, N'Tigrinya', 0, 164, N'ti')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000171, N'Tonga (Tonga Islands)', 0, 165, N'to')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000172, N'Tswana', 0, 166, N'tn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000173, N'Tsonga', 0, 167, N'ts')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000174, N'Turkmen', 0, 168, N'tk')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000175, N'Turkish', 0, 169, N'tr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000176, N'Twi', 0, 170, N'tw')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000177, N'Uighur; Uyghur', 0, 171, N'ug')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000178, N'Ukrainian', 0, 172, N'uk')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000179, N'Uzbek', 0, 173, N'uz')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000180, N'Venda', 0, 174, N've')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000181, N'Vietnamese', 0, 175, N'vi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000182, N'Volap�k', 0, 176, N'vo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000183, N'Welsh', 0, 177, N'cy')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000184, N'Walloon', 0, 178, N'wa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000185, N'Wolof', 0, 179, N'wo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000186, N'Xhosa', 0, 180, N'xh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000187, N'Yiddish', 0, 181, N'yi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000188, N'Yoruba', 0, 182, N'yo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000189, N'Zhuang; Chuang', 0, 183, N'za')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000191, N'Zulu', 0, 184, N'zu')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000192, N'Declined To Specify', 0, 185, N'UNK')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000193, N'Achinese', 0, 186, N'ace')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000194, N'Acoli', 0, 187, N'ach')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000195, N'Adangme', 0, 188, N'ada')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000196, N'Adyghe; Adygei', 0, 189, N'ady')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000197, N'Afrihili', 0, 190, N'afh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000198, N'Afro-Asiatic languages', 0, 191, N'afa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000199, N'Ainu', 0, 192, N'ain')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000200, N'Akkadian', 0, 193, N'akk')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000201, N'Aleut', 0, 194, N'ale')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000202, N'Algonquian languages', 0, 195, N'alg')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000203, N'Altaic languages', 0, 196, N'tut')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000204, N'Angika', 0, 197, N'anp')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000205, N'Apache languages', 0, 198, N'apa')
GO
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000206, N'Arapaho', 0, 199, N'arp')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000207, N'Arawak', 0, 200, N'arw')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000208, N'Aromanian; Arumanian; Macedo-Romanian', 0, 201, N'rup')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000209, N'Artificial languages', 0, 202, N'art')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000210, N'Asturian; Bable; Leonese; Asturleonese', 0, 203, N'ast')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000211, N'Athapascan languages', 0, 204, N'ath')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000212, N'Australian languages', 0, 205, N'aus')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000213, N'Austronesian languages', 0, 206, N'map')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000214, N'Awadhi', 0, 207, N'awa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000215, N'Balinese', 0, 208, N'ban')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000216, N'Baltic languages', 0, 209, N'bat')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000217, N'Baluchi', 0, 210, N'bal')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000218, N'Bamileke languages', 0, 211, N'bai')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000219, N'Banda languages', 0, 212, N'bad')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000220, N'Bantu languages', 0, 213, N'bnt')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000221, N'Basa', 0, 214, N'bas')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000222, N'Batak languages', 0, 215, N'btk')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000223, N'Beja; Bedawiyet', 0, 216, N'bej')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000224, N'Bemba', 0, 217, N'bem')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000225, N'Berber languages', 0, 218, N'ber')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000226, N'Bhojpuri', 0, 219, N'bho')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000227, N'Bikol', 0, 220, N'bik')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000228, N'Bini; Edo', 0, 221, N'bin')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000229, N'Blin; Bilin', 0, 222, N'byn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000230, N'Blissymbols; Blissymbolics; Bliss', 0, 223, N'zbl')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000231, N'Braj', 0, 224, N'bra')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000232, N'Buginese', 0, 225, N'bug')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000233, N'Buriat', 0, 226, N'bua')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000234, N'Caddo', 0, 227, N'cad')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000235, N'Caucasian languages', 0, 228, N'cau')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000236, N'Cebuano', 0, 229, N'ceb')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000237, N'Celtic languages', 0, 230, N'cel')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000238, N'Central American Indian languages', 0, 231, N'cai')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000239, N'Chagatai', 0, 232, N'chg')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000240, N'Chamic languages', 0, 233, N'cmc')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000241, N'Cherokee', 0, 234, N'chr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000242, N'Cheyenne', 0, 235, N'chy')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000243, N'Chibcha', 0, 236, N'chb')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000244, N'Chinook jargon', 0, 237, N'chn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000245, N'Chipewyan; Dene Suline', 0, 238, N'chp')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000246, N'Choctaw', 0, 239, N'cho')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000247, N'Chuukese', 0, 240, N'chk')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000248, N'Classical Newari; Old Newari; Classical Nepal Bhasa', 0, 241, N'nwc')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000249, N'Classical Syriac', 0, 242, N'syc')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000250, N'Coptic', 0, 243, N'cop')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000251, N'Creek', 0, 244, N'mus')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000252, N'Creoles and pidgins', 0, 245, N'crp')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000253, N'Creoles and pidgins, English based', 0, 246, N'cpe')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000254, N'Creoles and pidgins, French-based', 0, 247, N'cpf')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000255, N'Creoles and pidgins, Portuguese-based', 0, 248, N'cpp')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000256, N'Crimean Tatar; Crimean Turkish', 0, 249, N'crh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000257, N'Cushitic languages', 0, 250, N'cus')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000258, N'Dakota', 0, 251, N'dak')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000259, N'Dargwa', 0, 252, N'dar')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000260, N'Delaware', 0, 253, N'del')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000261, N'Dinka', 0, 254, N'din')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000262, N'Dogri', 0, 255, N'doi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000263, N'Dogrib', 0, 256, N'dgr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000264, N'Dravidian languages', 0, 257, N'dra')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000265, N'Duala', 0, 258, N'dua')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000266, N'Dutch, Middle (ca.1050-1350)', 0, 259, N'dum')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000267, N'Dyula', 0, 260, N'dyu')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000268, N'Eastern Frisian', 0, 261, N'frs')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000269, N'Efik', 0, 262, N'efi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000270, N'Egyptian (Ancient)', 0, 263, N'egy')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000271, N'Ekajuk', 0, 264, N'eka')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000272, N'Elamite', 0, 265, N'elx')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000273, N'English, Middle (1100-1500)', 0, 266, N'enm')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000274, N'English, Old (ca.450-1100)', 0, 267, N'ang')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000275, N'Erzya', 0, 268, N'myv')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000276, N'Ewondo', 0, 269, N'ewo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000277, N'Fang', 0, 270, N'fan')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000278, N'Fanti', 0, 271, N'fat')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000279, N'Filipino; Pilipino', 0, 272, N'fil')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000280, N'Finno-Ugrian languages', 0, 273, N'fiu')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000281, N'Fon', 0, 274, N'fon')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000282, N'French, Middle (ca.1400-1600)', 0, 275, N'frm')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000283, N'French, Old (842-ca.1400)', 0, 276, N'fro')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000284, N'Friulian', 0, 277, N'fur')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000285, N'Ga', 0, 278, N'gaa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000286, N'Galibi Carib', 0, 279, N'car')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000287, N'Gayo', 0, 280, N'gay')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000288, N'Gbaya', 0, 281, N'gba')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000289, N'Geez', 0, 282, N'gez')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000290, N'German, Middle High (ca.1050-1500)', 0, 283, N'gmh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000291, N'German, Old High (ca.750-1050)', 0, 284, N'goh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000292, N'Germanic languages', 0, 285, N'gem')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000293, N'Gilbertese', 0, 286, N'gil')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000294, N'Gondi', 0, 287, N'gon')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000295, N'Gorontalo', 0, 288, N'gor')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000296, N'Gothic', 0, 289, N'got')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000297, N'Grebo', 0, 290, N'grb')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000298, N'Greek, Ancient (to 1453)', 0, 291, N'grc')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000299, N'Gwich''in', 0, 292, N'gwi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000300, N'Haida', 0, 293, N'hai')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000301, N'Hawaiian', 0, 294, N'haw')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000302, N'Hiligaynon', 0, 295, N'hil')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000303, N'Himachali languages; Western Pahari languages', 0, 296, N'him')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000304, N'Hittite', 0, 297, N'hit')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000305, N'Hmong; Mong', 0, 298, N'hmn')
GO
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000306, N'Hupa', 0, 299, N'hup')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000307, N'Iban', 0, 300, N'iba')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000308, N'Ijo languages', 0, 301, N'ijo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000309, N'Iloko', 0, 302, N'ilo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000310, N'Inari Sami', 0, 303, N'smn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000311, N'Indic languages', 0, 304, N'inc')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000312, N'Indo-European languages', 0, 305, N'ine')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000313, N'Ingush', 0, 306, N'inh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000314, N'Iranian languages', 0, 307, N'ira')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000315, N'Irish, Middle (900-1200)', 0, 308, N'mga')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000316, N'Irish, Old (to 900)', 0, 309, N'sga')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000317, N'Iroquoian languages', 0, 310, N'iro')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000318, N'Judeo-Arabic', 0, 311, N'jrb')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000319, N'Judeo-Persian', 0, 312, N'jpr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000320, N'Kabardian', 0, 313, N'kbd')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000321, N'Kabyle', 0, 314, N'kab')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000322, N'Kachin; Jingpho', 0, 315, N'kac')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000323, N'Kalmyk; Oirat', 0, 316, N'xal')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000324, N'Kamba', 0, 317, N'kam')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000325, N'Karachay-Balkar', 0, 318, N'krc')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000326, N'Kara-Kalpak', 0, 319, N'kaa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000327, N'Karelian', 0, 320, N'krl')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000328, N'Karen languages', 0, 321, N'kar')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000329, N'Kashubian', 0, 322, N'csb')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000330, N'Kawi', 0, 323, N'kaw')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000331, N'Khasi', 0, 324, N'kha')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000332, N'Khoisan languages', 0, 325, N'khi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000333, N'Khotanese; Sakan', 0, 326, N'kho')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000334, N'Kimbundu', 0, 327, N'kmb')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000335, N'Klingon; tlhIngan-Hol', 0, 328, N'tlh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000336, N'Konkani', 0, 329, N'kok')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000337, N'Kosraean', 0, 330, N'kos')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000338, N'Kpelle', 0, 331, N'kpe')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000339, N'Kru languages', 0, 332, N'kro')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000340, N'Kumyk', 0, 333, N'kum')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000341, N'Kurukh', 0, 334, N'kru')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000342, N'Kutenai', 0, 335, N'kut')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000343, N'Ladino', 0, 336, N'lad')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000344, N'Lahnda', 0, 337, N'lah')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000345, N'Lamba', 0, 338, N'lam')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000346, N'Land Dayak languages', 0, 339, N'day')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000347, N'Lezghian', 0, 340, N'lez')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000348, N'Lojban', 0, 341, N'jbo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000349, N'Low German; Low Saxon; German, Low; Saxon, Low', 0, 342, N'nds')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000350, N'Lower Sorbian', 0, 343, N'dsb')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000351, N'Lozi', 0, 344, N'loz')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000352, N'Luba-Lulua', 0, 345, N'lua')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000353, N'Luiseno', 0, 346, N'lui')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000354, N'Lule Sami', 0, 347, N'smj')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000355, N'Lunda', 0, 348, N'lun')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000356, N'Luo (Kenya and Tanzania)', 0, 349, N'luo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000357, N'Lushai', 0, 350, N'lus')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000358, N'Madurese', 0, 351, N'mad')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000359, N'Magahi', 0, 352, N'mag')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000360, N'Maithili', 0, 353, N'mai')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000361, N'Makasar', 0, 354, N'mak')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000362, N'Manchu', 0, 355, N'mnc')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000363, N'Mandar', 0, 356, N'mdr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000364, N'Mandingo', 0, 357, N'man')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000365, N'Manipuri', 0, 358, N'mni')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000366, N'Manobo languages', 0, 359, N'mno')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000367, N'Mapudungun; Mapuche', 0, 360, N'arn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000368, N'Mari', 0, 361, N'chm')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000369, N'Marwari', 0, 362, N'mwr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000370, N'Masai', 0, 363, N'mas')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000371, N'Mayan languages', 0, 364, N'myn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000372, N'Mende', 0, 365, N'men')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000373, N'Mi''kmaq; Micmac', 0, 366, N'mic')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000374, N'Minangkabau', 0, 367, N'min')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000375, N'Mirandese', 0, 368, N'mwl')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000376, N'Mohawk', 0, 369, N'moh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000377, N'Moksha', 0, 370, N'mdf')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000378, N'Mongo', 0, 371, N'lol')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000379, N'Mon-Khmer languages', 0, 372, N'mkh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000380, N'Mossi', 0, 373, N'mos')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000381, N'Multiple languages', 0, 374, N'mul')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000382, N'Munda languages', 0, 375, N'mun')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000383, N'Nahuatl languages', 0, 376, N'nah')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000384, N'Neapolitan', 0, 377, N'nap')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000385, N'Nepal Bhasa; Newari', 0, 378, N'new')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000386, N'Nias', 0, 379, N'nia')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000387, N'Niger-Kordofanian languages', 0, 380, N'nic')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000388, N'Nilo-Saharan languages', 0, 381, N'ssa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000389, N'Niuean', 0, 382, N'niu')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000390, N'N''Ko', 0, 383, N'nqo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000391, N'No linguistic content; Not applicable', 0, 384, N'zxx')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000392, N'Nogai', 0, 385, N'nog')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000393, N'Norse, Old', 0, 386, N'non')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000394, N'North American Indian languages', 0, 387, N'nai')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000395, N'Northern Frisian', 0, 388, N'frr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000396, N'Nubian languages', 0, 389, N'nub')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000397, N'Nyamwezi', 0, 390, N'nym')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000398, N'Nyankole', 0, 391, N'nyn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000399, N'Nyoro', 0, 392, N'nyo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000400, N'Nzima', 0, 393, N'nzi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000401, N'Official Aramaic (700-300 BCE); Imperial Aramaic (700-300 BCE)', 0, 394, N'arc')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000402, N'Osage', 0, 395, N'osa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000403, N'Otomian languages', 0, 396, N'oto')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000404, N'Pahlavi', 0, 397, N'pal')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000405, N'Palauan', 0, 398, N'pau')
GO
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000406, N'Pampanga; Kapampangan', 0, 399, N'pam')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000407, N'Pangasinan', 0, 400, N'pag')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000408, N'Papiamento', 0, 401, N'pap')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000409, N'Papuan languages', 0, 402, N'paa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000410, N'Pedi; Sepedi; Northern Sotho', 0, 403, N'nso')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000411, N'Persian, Old (ca.600-400 B.C.)', 0, 404, N'peo')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000412, N'Philippine languages', 0, 405, N'phi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000413, N'Phoenician', 0, 406, N'phn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000414, N'Pohnpeian', 0, 407, N'pon')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000415, N'Prakrit languages', 0, 408, N'pra')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000416, N'Proven�al, Old (to 1500);Occitan, Old (to 1500)', 0, 409, N'pro')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000417, N'Rajasthani', 0, 410, N'raj')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000418, N'Rapanui', 0, 411, N'rap')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000419, N'Rarotongan; Cook Islands Maori', 0, 412, N'rar')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000420, N'Reserved for local use', 0, 413, N'qaa-qtz')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000421, N'Romance languages', 0, 414, N'roa')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000422, N'Romany', 0, 415, N'rom')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000423, N'Salishan languages', 0, 416, N'sal')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000424, N'Samaritan Aramaic', 0, 417, N'sam')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000425, N'Sami languages', 0, 418, N'smi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000426, N'Sandawe', 0, 419, N'sad')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000427, N'Santali', 0, 420, N'sat')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000428, N'Sasak', 0, 421, N'sas')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000429, N'Scots', 0, 422, N'sco')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000430, N'Selkup', 0, 423, N'sel')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000431, N'Semitic languages', 0, 424, N'sem')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000432, N'Serer', 0, 425, N'srr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000433, N'Shan', 0, 426, N'shn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000434, N'Sicilian', 0, 427, N'scn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000435, N'Sidamo', 0, 428, N'sid')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000436, N'Sign Languages', 0, 429, N'sgn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000437, N'Siksika', 0, 430, N'bla')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000438, N'Sino-Tibetan languages', 0, 431, N'sit')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000439, N'Siouan languages', 0, 432, N'sio')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000440, N'Skolt Sami', 0, 433, N'sms')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000441, N'Slave (Athapascan)', 0, 434, N'den')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000442, N'Slavic languages', 0, 435, N'sla')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000443, N'Sogdian', 0, 436, N'sog')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000444, N'Songhai languages', 0, 437, N'son')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000445, N'Soninke', 0, 438, N'snk')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000446, N'Sorbian languages', 0, 439, N'wen')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000447, N'South American Indian languages', 0, 440, N'sai')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000448, N'Southern Altai', 0, 441, N'alt')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000449, N'Southern Sami', 0, 442, N'sma')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000450, N'Sranan Tongo', 0, 443, N'srn')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000451, N'Standard Moroccan Tamazight', 0, 444, N'zgh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000452, N'Sukuma', 0, 445, N'suk')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000453, N'Sumerian', 0, 446, N'sux')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000454, N'Susu', 0, 447, N'sus')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000455, N'Swiss German; Alemannic; Alsatian', 0, 448, N'gsw')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000456, N'Syriac', 0, 449, N'syr')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000457, N'Tai languages', 0, 450, N'tai')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000458, N'Tamashek', 0, 451, N'tmh')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000459, N'Tereno', 0, 452, N'ter')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000460, N'Tetum', 0, 453, N'tet')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000461, N'Tigre', 0, 454, N'tig')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000462, N'Timne', 0, 455, N'tem')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000463, N'Tiv', 0, 456, N'tiv')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000464, N'Tlingit', 0, 457, N'tli')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000465, N'Tok Pisin', 0, 458, N'tpi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000466, N'Tokelau', 0, 459, N'tkl')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000467, N'Tonga (Nyasa)', 0, 460, N'tog')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000468, N'Tsimshian', 0, 461, N'tsi')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000469, N'Tumbuka', 0, 462, N'tum')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000470, N'Tupi languages', 0, 463, N'tup')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000471, N'Turkish, Ottoman (1500-1928)', 0, 464, N'ota')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000472, N'Tuvalu', 0, 465, N'tvl')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000473, N'Tuvinian', 0, 466, N'tyv')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000474, N'Udmurt', 0, 467, N'udm')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000475, N'Ugaritic', 0, 468, N'uga')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000476, N'Umbundu', 0, 469, N'umb')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000477, N'Uncoded languages', 0, 470, N'mis')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000478, N'Undetermined', 0, 471, N'und')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000479, N'Upper Sorbian', 0, 472, N'hsb')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000480, N'Vai', 0, 473, N'vai')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000481, N'Votic', 0, 474, N'vot')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000482, N'Wakashan languages', 0, 475, N'wak')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000483, N'Waray', 0, 476, N'war')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000484, N'Washo', 0, 477, N'was')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000485, N'Wolaitta; Wolaytta', 0, 478, N'wal')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000486, N'Yakut', 0, 479, N'sah')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000487, N'Yao', 0, 480, N'yao')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000488, N'Yapese', 0, 481, N'yap')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000489, N'Yupik languages', 0, 482, N'ypk')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000490, N'Zande languages', 0, 483, N'znd')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000491, N'Zapotec', 0, 484, N'zap')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000492, N'Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki', 0, 485, N'zza')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000493, N'Zenaga', 0, 486, N'zen')
INSERT [model].[LanguagesTmp] ([Id], [Name], [IsArchived], [OrdinalId], [Abbreviation]) VALUES (11000494, N'Zuni', 0, 487, N'zun')
SET IDENTITY_INSERT [model].[LanguagesTmp] OFF

GO

Set ANSI_PADDING ON	
UPDATE LG  SET LG.OrdinalId=LT.OrdinalId, LG.Abbreviation=LT.Abbreviation FROM model.languages LG INNER JOIN [model].[LanguagesTmp] LT ON LT.Name=LG.Name
Set ANSI_PADDING OFF	

Drop Trigger [model].[AuditPatientLanguagesInsertTrigger]

Set ANSI_PADDING ON	
INSERT INTO  model.languages(Name,Abbreviation,IsArchived,OrdinalId)
SELECT l.Name, l.Abbreviation,l.IsArchived,l.OrdinalId FROM model.[LanguagesTmp] l 
WHERE l.name NOT in (SELECT Name FROM model.languages)
Set ANSI_PADDING OFF

GO

Set ANSI_PADDING ON
Insert into model.PatientLanguages(PatientId, LanguageId) Select Id, LanguageId From model.Patients Where LanguageId is not NULL
Set ANSI_PADDING OFF


GO
SET ANSI_NULLS ON
GO

/****** Object:  Trigger [model].[AuditPatientLanguagesInsertTrigger]    Script Date: 12/14/2017 11:13:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [model].[AuditPatientLanguagesInsertTrigger] ON [model].[PatientLanguages] AFTER INSERT NOT FOR REPLICATION AS
BEGIN
					

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID('tempdb..#UserContext') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N'SELECT @__hostName = HOST_NAME()', N'@__hostName nvarchar(500) OUTPUT', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, 'model.PatientLanguages' AS ObjectName, 0 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty('ServerName')) AS ServerName,
	'[LanguageId]' AS KeyNames,CAST('[' + CAST(LanguageId AS nvarchar(max)) + ']' AS nvarchar(400)) AS KeyValues,PatientId AS PatientId,NULL AS AppointmentId
	INTO #__AuditEntries FROM (SELECT * FROM [inserted]) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = '__NULL__' THEN NULL ELSE OldValue END, CASE WHEN NewValue = '__NULL__' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, NULL AS OldValue, NewValue AS NewValue
			FROM
		
			(SELECT CAST('[' + CAST(LanguageId AS nvarchar(max)) + ']' AS nvarchar(400)) AS KeyValues, 			 
			 (SELECT COALESCE(CAST([PatientId] AS nvarchar(max)), '__NULL__')) AS [PatientId],
			  (SELECT COALESCE(CAST(LanguageId AS nvarchar(max)), '__NULL__')) AS LanguageId
			FROM [inserted]) AS q

		
			UNPIVOT (NewValue FOR ColumnName IN ([PatientId],LanguageId )) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END
GO





IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[LanguagesTmp]') AND type in (N'U'))
DROP TABLE [model].[LanguagesTmp]
GO
SET ANSI_PADDING ON
GO


