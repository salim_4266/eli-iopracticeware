
/****** Object:  StoredProcedure [MVE].[PP_GetPatientDetailById]    Script Date: 7/17/2018 4:30:02 PM ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_GetPatientDetailById]')) 
Drop Procedure [MVE].[PP_GetPatientDetailById] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [MVE].[PP_GetPatientDetailById] @PatientId numeric(18,0) 
As
Begin
  Select Top(1) P.PatientId as ExternalId, FirstName, '' as MiddleName, LastName, '' as Suffix, '' as Prefix, FirstName + ' ' + ' ' + LastName as FullName, '' as CompanyName, '' as JobTitle, IsNull(PEA.Value,'') as EmailAddresses, IsNull(Line1,'') as Address1, Isnull(Line2, '') as  Address2, Isnull(p.City, '') as City, Isnull(p.State, '') as  State, '' as Country, Isnull(PostalCode, '') as Zip, LastName + FirstName + CAST(P.PatientId as nvarchar(max)) as UserName, LastName + '_' + REPLACE(CONVERT (CHAR(10), p.BirthDate, 101),'/','') as Password, Cast(BirthDate as Date) as Birthdate, '' as Notes, p.Gender as Sex, p.Race as Race, p.Ethnicity as Ethnicity, p.Marital as MaritalStatus,
  p.Language as LanguageName, 
  (Select Top(1) Id From model.ServiceLocations) as LocationId, 
  ISNULL(PPN.CountryCode + PPn.AreaCode + PPn.ExchangeAndSuffix, '') as PhoneNumbers 
 From dbo.PatientDemographics P with (nolock) 
 INNER JOIN model.PatientAddresses PA with (nolock) on PA.PatientId = P.PatientId    
 LEFT JOIN model.PatientEmailAddresses PEA with (nolock) on P.PatientId = PEA.PatientId 
 LEFT Join model.PatientPhoneNumbers PPN with (nolock) on P.PatientId = PPN.PatientId and PPN.OrdinalId = 1 
 Where p.PatientId = @PatientId and PA.PostalCode is not null and Pa.PostalCode != '' and PA.OrdinalId = 1 and p.BirthDate is not null
End

GO


