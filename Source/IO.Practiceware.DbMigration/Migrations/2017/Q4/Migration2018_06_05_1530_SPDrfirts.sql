
GO


ALTER     PROCEDURE [DRFIRST].[MedicationResponsePracticeLevel]
@xmlStringData varchar(max)=''

AS
SET NOCOUNT ON


IF OBJECT_ID('tempdb..#Medication') IS NOT NULL DROP TABLE #Medication 
IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  DROP TABLE #RxSegments 
IF OBJECT_ID('tempdb..#Response') IS NOT NULL  DROP TABLE #Response 

if(@xmlStringData<>'')
BEGIN
--declare @PatientID bigint=40943
--declare @appointmentId bigint=3312639
		CREATE TABLE #RxSegments
		(
			ClinicalId int,
			SegTwo int,
			SegThree int,
			SegFour int,
			SegFive int,
			SegSix int,
			SegSeven int,
			SegNine int,
			LegacyDrug bit
		)				
		
		DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)    
		SELECT   @XML= CAST(@xmlStringData AS XML)  
		  
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML  
 
		SELECT 		
		Patient_ExtID, 
		0 AS Appointmentid,		
		CASE WHEN CHARINDEX('EST',StartDate)>0 THEN  CONVERT(VARCHAR,CAST(Replace(StartDate, 'EST','' ) as Date),112)  
		WHEN CHARINDEX('EDT',StartDate)>0 THEN  CONVERT(VARCHAR,CAST(Replace(StartDate, 'EDT','' ) as Date),112) END as Appdate,
		
		ClinicalID, PrescriptionRcopiaID,NDCID,
		FirstDataBankMedID,RcopiaID,
		RxnormID, RxnormIDType,
		BrandName,GenericName,DrugSchedule,BrandType,
		[RouteDrug],[Form],Strength,
		[Action],Dose, DoseUnit, 
		[Route], Frequency, Duration, 
		Quantity, QuantityUnit, 
		Refills,
		OtherNotes,PatientNotes,	
		SUBSTRING(SigChangedDate,1,10) as SigChangedDate,
		SUBSTRING(LastModifiedDate,1,10) as LastModifiedDate,
		SUBSTRING(StartDate,1,10) as StartDate,
		SUBSTRING(StopDate,1,10) as StopDate,
		SigChangedDate as response_SigChangedDate,
		LastModifiedDate as response_LastModifiedDate,
		StartDate as response_StartDate,
		StopDate as response_StopDate,

		Deleted,
		StopReason,
		MedicationRcopiaID,

		PharmacyRcopiaID ,
		PharmacyRcopiaMasterID,
		PharmacyDeleted ,
		PharmacyName ,
		PharmacyAddress1 ,
		PharmacyState ,
		PharmacyZip,
		PharmacyPhone ,
		PharmacyFax ,
		PharmacyIs24Hour ,
		PharmacyLevel3 ,
		PharmacyElectronic ,
		PharmacyMailOrder ,
		PharmacyRequiresEligibility ,
		PharmacyRetail ,
		PharmacyLongTermCare ,
		PharmacySpecialty ,
		PharmacyCanReceiveControlledSubstance 
		INTO #Response  
		FROM OPENXML(@hDoc, 'RCExtResponse/Response/MedicationList/Medication/Sig/Drug')  
		WITH   
		(  			
		Patient_ExtID [varchar](50) '../../Patient/ExternalID',  
		ClinicalID [varchar](50) '../../ExternalID',
		PrescriptionRcopiaID [varchar](50) '../../Prescription/RcopiaID',	

		NDCID [VARCHAR](100) 'NDCID',
		FirstDataBankMedID [VARCHAR](100) 'FirstDataBankMedID',
		RcopiaID [VARCHAR](100) 'RcopiaID',	
		
		RxnormID  [VARCHAR](100)'RxnormID',
		RxnormIDType  [VARCHAR](100)'RxnormIDType',
					
		BrandName [varchar](200) 'BrandName',  
		GenericName [varchar](200) 'GenericName',  
		DrugSchedule [varchar](200) 'Schedule', 
		BrandType [VARCHAR](100) 'BrandType',		
		[RouteDrug]  [varchar](200) 'Route',
		[Form] [varchar](100) 'Form',		
		Strength  [varchar](200) 'Strength',  
		
		[Action]  [varchar](200) '../Action', 				 	 
		Dose  [varchar](200) '../Dose',
		DoseUnit  [varchar](200) '../DoseUnit',
		[Route]  [varchar](200) '../Route',   
		Frequency  [varchar](200) '../DoseTiming',  
		Duration  [varchar](200) '../Duration',  
		Quantity  [varchar](200) '../Quantity',  		
		QuantityUnit  [varchar](200) '../QuantityUnit',  		
		Refills  [varchar](200) '../Refills',  
		OtherNotes  [varchar](200) '../OtherNotes',  
		PatientNotes  [varchar](200) '../PatientNotes', 
		  		
		SigChangedDate [varchar](50) '../../SigChangedDate' ,
		LastModifiedDate [varchar](50) '../../ExternLastModifiedDatealID' ,
		StartDate [varchar](50) '../../StartDate',
		StopDate [varchar](50) '../../StopDate',
		Deleted [varchar](50) '../../Deleted' ,
		StopReason [varchar](200) '../../StopReason',
		MedicationRcopiaID VARCHAR(50)'../../RcopiaID',

		PharmacyRcopiaID [varchar](50) '../../Pharmacy/RcopiaID',
		PharmacyRcopiaMasterID [varchar](50) '../../Pharmacy/RcopiaMasterID',
		PharmacyDeleted [varchar](50) '../../Pharmacy/Deleted',
		PharmacyName [varchar](50) '../../Pharmacy/Name',
		PharmacyAddress1 [varchar](50) '../../Pharmacy/Address1',
		PharmacyState [varchar](50) '../../Pharmacy/State',
		PharmacyZip [varchar](50) '../../Pharmacy/Zip',
		PharmacyPhone [varchar](50) '../../Pharmacy/Phone',
		PharmacyFax [varchar](50) '../../Pharmacy/Fax',
		PharmacyIs24Hour [varchar](50) '../../Pharmacy/Is24Hour',
		PharmacyLevel3 [varchar](50) '../../Pharmacy/Level3',
		PharmacyElectronic [varchar](50) '../../Pharmacy/Electronic',
		PharmacyMailOrder [varchar](50) '../../Pharmacy/MailOrder',
		PharmacyRequiresEligibility [varchar](50) '../../Pharmacy/RequiresEligibility',
		PharmacyRetail [varchar](50) '../../Pharmacy/Retail',
		PharmacyLongTermCare [varchar](50) '../../Pharmacy/LongTermCare',
		PharmacySpecialty [varchar](50) '../../Pharmacy/Specialty',
		PharmacyCanReceiveControlledSubstance [varchar](50) '../../Pharmacy/CanReceiveControlledSubstance'	
		 )  
		 EXEC sp_xml_removedocument @hDoc

	
	   	INSERT INTO [DrFirst].[eRx_Medicine_Response]
      ([Patient_ExtID]   ,AppointmentId   ,[ClinicalID]      ,[NDCID]      ,[FirstDataBankMedID]      ,[RcopiaID]     
	   ,RxnormID, RxnormIDType,[BrandName]
      ,[GenericName] ,   DrugSchedule ,[BrandType]      ,[RouteDrug]      ,[Form]      ,[Strength]      ,[Action]
      ,[Dose]      ,[DoseUnit]      ,[Route]      ,[Frequency]      ,[Duration]      ,[Quantity]
      ,[QuantityUnit]      ,[Refills]      ,[OtherNotes]      ,[PatientNotes]      ,[SigChangedDate]      ,[LastModifiedDate]
      ,[StartDate]      ,[StopDate]      ,[Deleted]      ,[StopReason]      ,[MedicationRcopiaID], PrescriptionRcopiaID,
	    PharmacyRcopiaID , PharmacyRcopiaMasterID,PharmacyDeleted , PharmacyName , PharmacyAddress1 , PharmacyState , 
		PharmacyZip, PharmacyPhone , PharmacyFax ,PharmacyIs24Hour , PharmacyLevel3 , PharmacyElectronic , 
		PharmacyMailOrder , PharmacyRequiresEligibility , PharmacyRetail , PharmacyLongTermCare , PharmacySpecialty ,
		PharmacyCanReceiveControlledSubstance 	
	  )
	  SELECT  [Patient_ExtID] ,0, [ClinicalID]      ,[NDCID]      ,[FirstDataBankMedID]      ,[RcopiaID]    
	   , RxnormID, RxnormIDType  ,[BrandName]      ,[GenericName],DrugSchedule
      ,[BrandType]      ,[RouteDrug]      ,[Form]      ,[Strength]      ,[Action]      ,[Dose]      ,[DoseUnit]      ,[Route]
      ,[Frequency]      ,[Duration]      ,[Quantity]      ,[QuantityUnit]      ,[Refills]      ,[OtherNotes]      ,[PatientNotes],
	  [response_SigChangedDate]      ,[response_LastModifiedDate]      ,
	  CASE WHEN [StartDate] ='' THEN [response_StopDate] ELSE [response_StartDate] END       ,
	  [response_StopDate]  ,[Deleted]      ,[StopReason]      ,[MedicationRcopiaID], PrescriptionRcopiaID,     
	  PharmacyRcopiaID , PharmacyRcopiaMasterID,PharmacyDeleted , PharmacyName , PharmacyAddress1 , PharmacyState , 
	  PharmacyZip, PharmacyPhone , PharmacyFax ,PharmacyIs24Hour , PharmacyLevel3 , PharmacyElectronic , 
	PharmacyMailOrder , PharmacyRequiresEligibility , PharmacyRetail , PharmacyLongTermCare , PharmacySpecialty ,
	PharmacyCanReceiveControlledSubstance 
	  FROM #Response WHERE [MedicationRcopiaID] NOT IN( SELECT Tmp.[MedicationRcopiaID] FROM [DrFirst].[eRx_Medicine_Response] Tmp 
	  WHERE Tmp.[Patient_ExtID]=#Response.[Patient_ExtID] AND Tmp.Deleted=#Response.Deleted  AND Tmp.StopDate=#Response.StopDate)



		 INSERT INTO #RxSegments
		SELECT DISTINCT pc.clinicalid
		, CHARINDEX('-2', pc.FindingDetail) + 3
		, CHARINDEX(')-3', pc.FindingDetail) + 4
		, CHARINDEX(')-4', pc.FindingDetail) + 4
		, CHARINDEX(')-5', pc.FindingDetail) + 4
		, CHARINDEX(')-6', pc.FindingDetail) + 4
		, CHARINDEX(')-7', pc.FindingDetail) + 4
		, CHARINDEX('9-9', pc.FindingDetail)
		, CASE WHEN pc.FindingDetail like '%()%' THEN 0 ELSE 1 END 
		FROM PatientClinical pc
		INNER JOIN #Response rsp ON rsp.Patient_ExtID=pc.PatientId
		WHERE pc.ClinicalId in (select clinicalid as number from patientclinical where clinicaltype='A' AND FindingDetail like 'Rx%' 
		and pc.PatientId = rsp.Patient_ExtID )

		
		Update RS SET RS.Appointmentid=App.Appointmentid  FROM  #Response  RS INNER  JOIN appointments App ON RS.Patient_ExtID=App.Patientid
		WHERE RS.Appdate=App.Appdate
			

		DECLARE  @Medication TABLE
		(
		ClinicalId BIGINT,
		PatientId BIGINT,
		appointmentid BIGINT,
		AppDate Datetime,
		DoctorExternalId INT,
		ResourceName VARCHAR(200),
		LoginID INT,
		FindingDetail VARCHAR(500),
		restFD1 VARCHAR(100),
		PatientNote NVARCHAR(500),
		OtherNote NVARCHAR(500),
		Drugid VARCHAR(100),
		DrugName VARCHAR(200),
		FD  VARCHAR(500),
		FindingDuration VARCHAR(100),
		Status Varchar(100)
		)

		--select * FROM @Medication

		INSERT INTO @Medication
		Select  distinct pc.ClinicalId as ClinicalId,pc.PatientId ,A.appointmentid,


		(convert(varchar, convert(datetime, A.appdate), 101)+ ' ' 
	+ substring(A.appdate, 1, 2)+ ':' + substring(A.appdate, 3, 2)+ ':' + substring(A.appdate, 5, 2)) as AppDate,
	

		A.resourceid1 as DoctorExternalId,ResourceName,ResourcePid as LoginID,
		pc.findingdetail,
		SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)) as restFD1,
					'' as PatientNote,'' as OtherNote,
					pc.DrawfileName as DrugId,
					(CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') 
		ELSE '' END) as DrugName,
		('RX-8/'+
		(CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
			--THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') ELSE '' END)
			THEN  SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6) ELSE '' END)
		+
		'-2/(Dosage)(Strength)(EyeContext)-3/(Frequency)('+
		
		+'PRN)(PO)('+
		(CASE rx.LegacyDrug WHEN 0 THEN
			SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)) - 1)
		ELSE ''END) --as OTC,		
		+	')('+
			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-1)) +')('+
			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-1))
			+')-4/(Refills)('+
			SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-1) 
			+')(QtyUnit)('+	
			
		(CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)) - 1)
		ELSE ''	END)-- as LastTaken,
		+')-5/('+
		CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX(')', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)
	ELSE '' END 
	+')(DaySupply'

		+')'+			
		(SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)))) as FD,
		
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX(')', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)as FindingDuration,
		pc.[Status]
		--INTO #Medication
		FROM PatientClinical pc 
		INNER JOIN appointments A ON A.Appointmentid=pc.Appointmentid
		INNER JOIN Resources R ON R.ResourceId=A.resourceid1	
		INNER JOIN #RxSegments rx on pc.ClinicalId = rx.ClinicalId
		INNER JOIN #Response rsp ON rsp.Patient_ExtID=pc.PatientId --AND rsp.Appointmentid=pc.Appointmentid
		LEFT OUTER JOIN PatientNotes pn1 on pn1.AppointmentId = pc.AppointmentId 
		AND Substring(PC.FindingDetail, 6, CHARINDEX('-2',pc.findingdetail)-6) = pn1.NoteSystem 
		AND pn1.NoteType = 'R'
		LEFT OUTER JOIN PatientNotes pn2 on pn2.AppointmentId = pc.AppointmentId
		AND SUBSTRING(pc.FindingDetail, 6, CHARINDEX('-2', pc.findingdetail)-6) = pn2.NoteSystem
		AND pn2.NoteType = 'X'			
		WHERE 
		 PC.FindingDetail like 'RX%'
		 AND PC.clinicaltype='A'
		
		update M  SET M.PatientNote=PN.note1  FROM   @Medication M  INNER JOIN PatientNotes PN ON M.AppointmentId = PN.AppointmentId
		WHERE NoteType='R' AND Substring(M.FindingDetail, 6, CHARINDEX('-2',M.findingdetail)-6) = PN.NoteSystem 

		
		update M  SET M.OtherNote=PN.note1  FROM   @Medication M  INNER JOIN PatientNotes PN ON M.AppointmentId = PN.AppointmentId
		WHERE NoteType='X' AND Substring(M.FindingDetail, 6, CHARINDEX('-2',M.findingdetail)-6) = PN.NoteSystem 

		DECLARE @NewMedCount INT
		
		
		

		SELECT ROW_NUMBER() OVER(ORDER BY MedicationRcopiaID) AS ROWNUMBER,		
		Patient_ExtID , R.AppointmentId,
		ClinicalID,MedicationRcopiaID,BrandName,Form,RouteDrug,
		(CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END) AS [EyeContext],
		'99' as [Symptom],
		 RcopiaID,NDCID,
		 'RX-8/'+R.BrandName+' '+R.Form+'-2/('+Dose+' '+DoseUnit+')('+Strength+')('+
		(CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END)
		+')-3/('+Frequency+')('+
		(CASE WHEN R.Refills='PRN' THEN '' ELSE '' END)+')('+
		--(Case WHEN R.[Action]='Apply' THEN 'APPLY TO LIDS' WHEN R.[Action]='INSTILL' THEN 'INSTILL IN EYES' WHEN R.[Action]='' THEN  'PO' ELSE R.[Route] END)+')()()()-4/('+
		R.[Action] +' '+R.[Route] +')()()()-4/('+
		--(CASE WHEN R.Refills='PRN' THEN '' ELSE R.Refills+' '+'REFILLS' END)+')()('+
		(CASE  WHEN R.Refills ='' THEN '' WHEN R.Refills ='PRN' THEN 'PRN REFILLS' ELSE R.Refills+' '+'REFILLS' END)+')()('+
		(R.Quantity+' '+R.QuantityUnit)+')()-5/()('+
		(CASE WHEN R.Duration <>'' THEN R.Duration+' DAYS SUPPLY' ELSE R.Duration END)+')-6/' as Findings,	
		--(case when R.StopDate <> '' then ('D'+R.StopDate +'!'+ CONVERT(varchar(20),CAST(Ap.appdate AS  DATE),101)) ELSE ('!'+ CONVERT(varchar(20),CAST(Ap.appdate AS  DATE),101) )  end) AS ImgDescriptor,		
		'!'+ CONVERT(varchar(20),CAST(Ap.appdate AS  DATE),101) AS ImgDescriptor,		
		FirstDataBankMedID,
		(case when Deleted = 'n' then 'A' ELSE 'D'  end) AS [Status],
		PatientNotes,
		OtherNotes,
		StartDate,
		StopDate,		
		PrescriptionRcopiaID
		INTO #NewMeds
		FROM #Response R
		INNER  JOIN appointments ap ON Ap.Appointmentid=R.AppointmentId AND Ap.Patientid=Patient_ExtID
		WHERE
		 (ClinicalID =''  OR ClinicalID=0)
		-- AND 
		-- R.Deleted='n'
		AND R.MedicationRcopiaID NOT IN( SELECT RcopiyaID FROM [DrFirst].[ErxMedicationMapping] WHERE PatientID=Patient_ExtID )
		AND   ISNULL(R.PrescriptionRcopiaID,'') NOT IN( SELECT PrescriptionRcopiaID FROM [DrFirst].[ErxMedicationMapping] WHERE PatientID=R.Patient_ExtID  AND PrescriptionRcopiaID IS NOT NULL)
	
		SET @NewMedCount=@@rowcount
		
			IF(@NewMedCount>0)
		BEGIN
			DECLARE @loop INT
			SET @loop = 1

			WHILE @loop <= @NewMedCount
			BEGIN
				---SELECT  * FROM #NewMeds WHERE ROWNUMBER=@loop			
				INSERT INTO dbo.PatientClinical
				(
				[AppointmentId]
				,[PatientId]
				,[ClinicalType]
				,[EyeContext]
				,[Symptom]
				,[FindingDetail]
				,[ImageDescriptor]      
				,[Status]
				,[DrawFileName]
				,Activity)
				
				SELECT  AppointmentId, Patient_ExtID,'A',
				EyeContext,Symptom,
				CASE WHEN  PrescriptionRcopiaID IS NOT NULL THEN Findings+'P-7/' ELSE Findings END,
				ImgDescriptor, [Status], FirstDataBankMedID,CASE WHEN PrescriptionRcopiaID IS NOT NULL  THEN 'pen' ELSE '' END
				FROM #NewMeds TT 			
				WHERE ROWNUMBER=@loop

				DECLARE @ClinicalID_New BIGINT 
				SELECT @ClinicalID_New=IDENT_CURRENT('PatientClinical')
				--Select @ClinicalID_New
			
			
			INSERT INTO [DrFirst].[ErxMedicationMapping]
			([ClinicalId],[EncounterId],[PatientId],[RcopiyaID],[MedDatetime],PrescriptionRcopiaID)
			SELECT @ClinicalID_New, AppointmentId, Patient_ExtID,MedicationRcopiaID,GETDATE() ,PrescriptionRcopiaID
			FROM #NewMeds  WHERE  ROWNUMBER=@loop

			INSERT INTO [DrFirst].[PatientClinicalMeds]
			([ClinicalId] ,ExternalId,RcopiaId, RxnormId, NDC, [AppointmentId], 
			[PatientId], [FindingDetail],[status], DrugId ,[CreatedDatetime],IsUpdate)
			Select @ClinicalID_New,@ClinicalID_New,MedicationRcopiaID,'',NDCID,
			AppointmentId,Patient_ExtID,Findings,[Status],FirstDataBankMedID,Getdate() ,0 
			From #NewMeds 
			WHERE  ROWNUMBER=@loop AND @ClinicalID_New NOT IN(Select [ClinicalId] FROM [DrFirst].[PatientClinicalMeds])
			

				
			IF NOT EXISTS (SELECT 1 FROM Drug D INNER JOIN #NewMeds N ON D.DisplayName=N.BrandName+' '+ N.Form  AND D.Focus=1 and D.DrugDosageFormId is NULL WHERE ROWNUMBER=@loop )
			BEGIN
				INSERT INTO Drug (Drugcode,DisplayName,Family1,Family2,Focus,DrugDosageFormId)
				SELECT 'T',BrandName+' '+Form,'','',1,NULL FROM #NewMeds WHERE   RouteDrug='opht' AND ROWNUMBER=@loop
			END

				INSERT INTO  dbo.PatientNotes 
				(
					PatientId,
					AppointmentId,
					NoteType,
					Note1,
					NoteDate,
					NoteSystem, 
					NoteCommentOn,
					NoteAudioOn,
					NoteClaimOn,
					ClinicalId 
				)
				SELECT 
				Patient_ExtID, 
				appointmentId,
				'R',
				PatientNotes,			
				RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),

				(BrandName+' '+Form),
				'T','F','F',
				@ClinicalID_New
				FROM   #NewMeds
				WHERE  ROWNUMBER=@loop

				INSERT INTO  dbo.PatientNotes 
				(
					PatientId,
					AppointmentId,
					NoteType,
					Note1,
					NoteDate,
					NoteSystem, 
					NoteCommentOn,
					NoteAudioOn,
					NoteClaimOn,
					ClinicalId 
				)
				SELECT 
				Patient_ExtID, 
				appointmentId,
				'X',
				OtherNotes,			
				RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),

				(BrandName+' '+Form),
				'T','F','F',
				@ClinicalID_New
				FROM   #NewMeds
				WHERE  ROWNUMBER=@loop

				SET @loop = @loop + 1 								  
			END		
		END
		--ELSE
		--Print 'Not in if'

		IF OBJECT_ID('tempdb..#NewMeds') IS NOT NULL  
					DROP TABLE #NewMeds 

		--Select * from #Medication
		--Select * from #Response

		Update RS SET RS.clinicalid=Emm.Clinicalid FROM  #Response  RS INNER  JOIN [DrFirst].[ErxMedicationMapping] Emm ON RS.Patient_ExtID=Emm.Patientid
		WHERE RS.PrescriptionRcopiaId=Emm.PrescriptionRcopiaID OR Rs.MedicationRcopiaId=Emm.RcopiyaId
		
		Update Emm SET Emm.RcopiyaID=RS.MedicationRcopiaID 
		FROM  #Response  RS 
		INNER  JOIN [DrFirst].[ErxMedicationMapping] Emm ON RS.Patient_ExtID=Emm.Patientid 	AND RS.PrescriptionRcopiaID=Emm.PrescriptionRcopiaID
 
		

		UPDATE P 
		set P.FindingDetail=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TMP.FD,'EyeContext',
		 (CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END)),
		'PRN',(CASE WHEN R.Refills='PRN' THEN 'PRN' ELSE ''END)),'DaySupply',
		(CASE WHEN R.Duration <>'' THEN R.Duration+' DAYS SUPPLY' ELSE R.Duration END))
		,'Refills',CASE WHEN R.Refills='PRN' THEN '' ELSE R.Refills+' '+'REFILLS' END),'QtyUnit',R.Quantity+' '+R.QuantityUnit),'PO',
		(Case WHEN R.[Action]='Apply' THEN 'APPLY TO LIDS' WHEN R.[Action]='INSTILL' THEN 'INSTILL IN EYES' WHEN R.[Action]='' THEN  'PO' ELSE R.[Route] END)),
		'Frequency',R.Frequency),'Strength',R.Strength),'Dosage',R.Dose+' '+ R.DoseUnit) 
			
		,Activity=CASE WHEN R.PrescriptionRcopiaID IS NOT NULL  THEN 'pen' ELSE Activity END		
		FROM @Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId


		--For ChangeERXdate 
		Update P set
		--P.ImageDescriptor=CASE WHEN LEFT(P.ImageDescriptor, 1)<>'D' THEN 'D'+R.StopDate+P.ImageDescriptor ELSE P.ImageDescriptor END
		P.ImageDescriptor= CASE 
							WHEN R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) AND Left(P.ImageDescriptor, 1)='!' THEN 'D'+R.StopDate+P.ImageDescriptor 
							WHEN  R.StopDate=RIGHT(Left(P.ImageDescriptor, 11),10)  AND Left(P.ImageDescriptor, 1)='!' THEN STUFF(P.ImageDescriptor , 1, 1, 'D') ELSE P.ImageDescriptor
						  END
		FROM @Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId WHERE R.StopDate<>'' AND R.Deleted='n' AND R.Stopdate= convert(varchar(10),getdate(),101)

		Update P set
		P.ImageDescriptor=CASE 
							WHEN --R.StopDate<>Right(P.ImageDescriptor, 10) AND 
								R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) 
							THEN '!'+R.StopDate+P.ImageDescriptor 
							ELSE P.ImageDescriptor 
						END		
		,P.Status='D'
		FROM @Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId
		WHERE R.Deleted='y'
		
		----For-Patiet Note
		UPDATE  PN  SET  PN.Note1=R.PatientNotes FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID 
			AND PN.PatientId=R.Patient_ExtID AND NoteType='R'AND R.Clinicalid <>''

		--For Phrma note 
		UPDATE  PN  SET  PN.Note1=R.OtherNotes
		FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID AND PN.PatientId=R.Patient_ExtID AND  NoteType='X'AND R.Clinicalid <>''

		--INSERT INTO PatientNotes (PatientId,AppointmentId,NoteType,Note1,NoteDate,NoteSystem, NoteCommentOn,NoteAudioOn,NoteClaimOn,ClinicalId )
		--SELECT  Patient_ExtID, AappointmentId,'R',	PatientNotes,FORMAT(CAST(StartDate AS DATE),'yyyyMMdd'),(BrandName+' '+Form),'T','F','F',ClinicalID 
		--FROM  #Response R  WHERE R.ClinicalID NOT IN(Select  P.ClinicalID  FROM PatientNotes P  WHERE Patientid=@PatientID AND AppointmentId=@appointmentId)


		--INSERT INTO PatientNotes (PatientId,AppointmentId,NoteType,Note1,NoteDate,NoteSystem, NoteCommentOn,NoteAudioOn,NoteClaimOn,ClinicalId )
		--SELECT  Patient_ExtID, AppointmentId,'X',	OtherNotes,FORMAT(CAST(StartDate AS DATE),'yyyyMMdd'),(BrandName+' '+Form),'T','F','F',ClinicalID 
		--FROM  #Response R  WHERE R.ClinicalID NOT IN(Select  ClinicalID  FROM PatientNotes P  WHERE P.Patientid=Patient_ExtID AND AppointmentId=@appointmentId)

		
		UPDATE PCM  SET   PCM.ExternalID= R.ClinicalID, PCM.RcopiaId=R.RcopiaID , Isupdate=0,status=Deleted  FROM   #Response R  INNER JOIN Drfirst.[PatientClinicalMeds] PCM ON R.ClinicalID=PCM.ClinicalID
	
	
		IF OBJECT_ID('tempdb..#Medication') IS NOT NULL  
			DROP TABLE #Medication 
	
		IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  
			DROP TABLE #RxSegments 

		IF OBJECT_ID('tempdb..#Response') IS NOT NULL  
					DROP TABLE #Response 
		END		

	
SET NOCOUNT OFF




GO



ALTER   PROCEDURE [DRFIRST].[PrescriptionResponce]
@PatientID BIGINT,
@appointmentId BIGINT,
@xmlStringData varchar(max)

AS
SET NOCOUNT ON

IF OBJECT_ID('tempdb..#Medication') IS NOT NULL DROP TABLE #Medication 
IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  DROP TABLE #RxSegments 
--declare @PatientID bigint=40943
--declare @appointmentId bigint=3312639

		CREATE TABLE #RxSegments
		(
			ClinicalId int,
			SegTwo int,
			SegThree int,
			SegFour int,
			SegFive int,
			SegSix int,
			SegSeven int,
			SegNine int,
			LegacyDrug bit
		)

		INSERT INTO #RxSegments
		SELECT pc.clinicalid
		, CHARINDEX('-2', pc.FindingDetail) + 3
		, CHARINDEX('-3', pc.FindingDetail) + 3
		, CHARINDEX('-4', pc.FindingDetail) + 3
		, CHARINDEX('-5', pc.FindingDetail) + 3
		, CHARINDEX('-6', pc.FindingDetail) + 3
		, CHARINDEX('-7', pc.FindingDetail) + 3
		, CHARINDEX('9-9', pc.FindingDetail)
		, CASE WHEN pc.FindingDetail like '%()%' THEN 0 ELSE 1 END 
		FROM PatientClinical pc
		WHERE pc.ClinicalId in (select clinicalid as number from patientclinical where clinicaltype='A' AND FindingDetail like 'Rx%' 
		and pc.PatientId = @PatientID --and appointmentid=@appointmentId
		)
		and pc.PatientId = @PatientID -- and appointmentid=@appointmentId
			
	
		select  pc.ClinicalId as ClinicalId,pc.PatientId ,A.appointmentid,
		(convert(varchar, convert(datetime, A.appdate), 101)+ ' ' 
	+ substring(A.appdate, 1, 2)+ ':' + substring(A.appdate, 3, 2)+ ':' + substring(A.appdate, 5, 2)) as AppDate, 	
		A.resourceid1 as DoctorExternalId,ResourceName,ResourcePid as LoginID,
		pc.findingdetail,
		SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)) as restFD1,
		(SELECT STUFF((SELECT char(9) + Note1
					FROM patientnotes where patientid=@PatientID AND   Notetype in('R')
					FOR XML PATH('')) ,1,1,'')) AS PatientNote
			
		,(SELECT STUFF((SELECT char(9) + Note1
					FROM patientnotes where patientid=@PatientID And  notetype in('B','C','X')
					FOR XML PATH('')) ,1,1,'')) AS OtherNote,
					pc.DrawfileName as DrugId,
					(CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') 
		ELSE '' END) as DrugName,
		('RX-8/'+
		(CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') ELSE '' END)
		+
		'-2/(Dosage)(Strength)('+
				--SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3)),3)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3)),3))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3)),3))-1)
				+'EyeContext)-3/(Frequency)('+
		--(CASE rx.LegacyDrug WHEN 0 THEN 
		--SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegThree+1) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegThree+1)) - CHARINDEX('(', pc.FindingDetail, rx.SegThree+1) - 1)
		--ELSE ''END)-- as PRN,
		+'PRN)(PO)('+
		(CASE rx.LegacyDrug WHEN 0 THEN
			SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)) - 1)
		ELSE ''END) --as OTC,		
		+	')('+
			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-1)) +')('+
			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-1))
			+')-4/(Refills)('+
			SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-1) 
			+')(QtyUnit)('+	
		--(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),3)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),3))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),3))-1))+')('+
		
		(CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)) - 1)
		ELSE ''	END)-- as LastTaken,
		+')-5/(Duration)('+			
		(CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegFive+1) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegFive+1)) - CHARINDEX('(', pc.FindingDetail, rx.SegFive+1) - 1)
		ELSE ''	END)--as Supply,
		+')'+		
		(SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)))) as FD,
		'' AS FD1,
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX(')', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)as FindingDuration,
		pc.[Status]
		INTO #Medication
		FROM PatientClinical pc 
		INNER JOIN appointments A ON A.Appointmentid=pc.Appointmentid
		INNER JOIN Resources R ON R.ResourceId=A.resourceid1	
		INNER JOIN #RxSegments rx on pc.ClinicalId = rx.ClinicalId
		LEFT OUTER JOIN PatientNotes pn1 on pn1.AppointmentId = pc.AppointmentId 
		AND Substring(PC.FindingDetail, 6, CHARINDEX('-2',pc.findingdetail)-6) = pn1.NoteSystem 
		AND pn1.NoteType = 'R'
		LEFT OUTER JOIN PatientNotes pn2 on pn2.AppointmentId = pc.AppointmentId
		AND SUBSTRING(pc.FindingDetail, 6, CHARINDEX('-2', pc.findingdetail)-6) = pn2.NoteSystem
		AND pn2.NoteType = 'X'			
		WHERE pc.ClinicalId in (select clinicalid as number from patientclinical where  clinicaltype='A' AND FindingDetail like 'Rx%')
		and pc.PatientId = @PatientID

		DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)  
  
		SELECT   @XML= CAST(@xmlStringData AS XML)  
  
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML  
  
		SELECT 		
		Patient_ExtID, ClinicalID, NDCID,
		FirstDataBankMedID,RcopiaID,
		BrandName,GenericName,BrandType,
		[RouteDrug],[Form],Strength,
		[Action],Dose, DoseUnit, 
		[Route], Frequency, Duration, 
		Quantity, QuantityUnit, 
		Refills,
		OtherNotes,PatientNotes,	
		SUBSTRING(SigChangedDate,1,10) as SigChangedDate,
		SUBSTRING(LastModifiedDate,1,10) as LastModifiedDate,
		SUBSTRING(StartDate,1,10) as StartDate,
		SUBSTRING(StopDate,1,10) as StopDate,
		Deleted,
		StopReason,
		MedicationRcopiaID
		INTO #Response  
		FROM OPENXML(@hDoc, 'RCExtResponse/Response/MedicationList/Medication/Sig/Drug')  
		WITH   
		(  			
		Patient_ExtID [varchar](50) '../../Patient/ExternalID',  
		ClinicalID [varchar](50) '../../ExternalID',

		NDCID [VARCHAR](100) 'NDCID',
		FirstDataBankMedID [VARCHAR](100) 'FirstDataBankMedID',
		RcopiaID [VARCHAR](100) 'RcopiaID',				
		BrandName [varchar](200) 'BrandName',  
		GenericName [varchar](200) 'GenericName',  
		BrandType [VARCHAR](100) 'BrandType',		
		[RouteDrug]  [varchar](200) 'Route',
		[Form] [varchar](100) 'Form',		
		Strength  [varchar](200) 'Strength',  
		
		[Action]  [varchar](200) '../Action', 				 	 
		Dose  [varchar](200) '../Dose',
		DoseUnit  [varchar](200) '../DoseUnit',
		[Route]  [varchar](200) '../Route',   
		Frequency  [varchar](200) '../DoseTiming',  
		Duration  [varchar](200) '../Duration',  
		Quantity  [varchar](200) '../Quantity',  		
		QuantityUnit  [varchar](200) '../QuantityUnit',  		
		Refills  [varchar](200) '../Refills',  
		OtherNotes  [varchar](200) '../OtherNotes',  
		PatientNotes  [varchar](200) '../PatientNotes', 
		  		
		SigChangedDate [varchar](50) '../../SigChangedDate' ,
		LastModifiedDate [varchar](50) '../../ExternLastModifiedDatealID' ,
		StartDate [varchar](50) '../../StartDate',
		StopDate [varchar](50) '../../StopDate',
		Deleted [varchar](50) '../../Deleted' ,
		StopReason [varchar](200) '../../StopReason',
		MedicationRcopiaID VARCHAR(50)'../../RcopiaID'
		 )  
		 EXEC sp_xml_removedocument @hDoc 

		 --select * from #Response

			
		SELECT  ROW_NUMBER() OVER(PARTITION BY RcopiaID, MedicationRcopiaID,NDCID,Brandname,FirstDataBankMedID ORDER BY MedicationRcopiaID) AS ROWNUMBER, 
		 --'1234567'  as Patient_ExtID, '1234567' AS AppointmentId,
		@PatientID AS Patient_ExtID ,@appointmentId AS AppointmentId,
		ClinicalID,MedicationRcopiaID,BrandName,Form,
		(CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END) AS [EyeContext],
		'99' as [Symptom],
		 RcopiaID,NDCID,
		 'RX-8/'+R.BrandName+' '+R.Form+'-2/('+Dose+' '+DoseUnit+')('+Strength+')('+
		(CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END)
		+')-3/('+Frequency+')('+
		(CASE WHEN R.Refills='PRN' THEN 'PRN' ELSE '' END)+')('+
		(Case WHEN R.[Action]='Apply' THEN 'APPLY TO LIDS' WHEN R.[Action]='INSTILL' THEN 'INSTILL IN EYES' WHEN R.[Action]='' THEN  'PO' ELSE R.[Route] END)+')()()()-4/('+
		(CASE WHEN R.Refills='PRN' THEN '' ELSE R.Refills+' '+'REFILLS' END)+')()('+
		(R.Quantity+' '+R.QuantityUnit)+')()-5/('+
		(CASE WHEN R.Duration LIKE '[0-9]' THEN R.Duration+' days' ELSE R.Duration END)+')()-6/P-7/' as Findings,
		('!'+StartDate ) AS ImgDescriptor,
		FirstDataBankMedID,
		'A'	as [Status],
		PatientNotes,
		OtherNotes,
		StartDate,
		StopDate		
		INTO #NewMeds
		FROM #Response R WHERE  ClinicalID =''
		AND MedicationRcopiaID NOT IN( SELECT RcopiyaID FROM [DrFirst].[ErxMedicationMapping])



--select * from #NewMeds

		DECLARE @NewMedCount INT
		SELECT @NewMedCount= COUNT(*) FROM #NewMeds
		print @NewMedCount
--select COUNT(*) FROM #NewMeds
		DECLARE @loop INT
		SET @loop = 1

		WHILE @loop <= @NewMedCount
		BEGIN				
			INSERT INTO dbo.PatientClinical
			(
			[AppointmentId]
			,[PatientId]
			,[ClinicalType]
			,[EyeContext]
			,[Symptom]
			,[FindingDetail]
			,[ImageDescriptor]      
			,[Status]
			,[DrawFileName])
			--,[Highlights]  --,[FollowUp]      --,[PermanentCondition]      --,[PostOpPeriod]      --,[Surgery]      --,[Activity]      --,[LegacyClinicalDataSourceTypeId]
			SELECT  AppointmentId, Patient_ExtID,'A',
			EyeContext,Symptom,Findings, ImgDescriptor, [Status], FirstDataBankMedID
			FROM #NewMeds TT 			
			WHERE ROWNUMBER=@loop

			DECLARE @ClinicalID_New BIGINT 
			SELECT @ClinicalID_New=IDENT_CURRENT('PatientClinical')

			select @ClinicalID_New

			
			INSERT INTO [DrFirst].[ErxMedicationMapping]
			([ClinicalId],[EncounterId],[PatientId],[RcopiyaID],[MedDatetime])
			SELECT @ClinicalID_New, @appointmentId, @PatientID,MedicationRcopiaID,GETDATE() 
				FROM #NewMeds WHERE  ROWNUMBER=@loop

--select @ClinicalID_New		
			INSERT INTO  dbo.PatientNotes 
			(
				PatientId,
				AppointmentId,
				NoteType,
				Note1,
				NoteDate,
				NoteSystem, 
				NoteCommentOn,
				NoteAudioOn,
				NoteClaimOn,
				ClinicalId 
			)
			SELECT 
			@PatientID, 
			@appointmentId,
			'R',
			PatientNotes,			
			RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),

			(BrandName+' '+Form),
			'T','F','F',
			@ClinicalID_New
			FROM   #NewMeds
			WHERE  ROWNUMBER=@loop

			INSERT INTO  dbo.PatientNotes 
			(
				PatientId,
				AppointmentId,
				NoteType,
				Note1,
				NoteDate,
				NoteSystem, 
				NoteCommentOn,
				NoteAudioOn,
				NoteClaimOn,
				ClinicalId 
			)
			SELECT 
			@PatientID, 
			@appointmentId,
			'X',
			PatientNotes,			
			RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),

			(BrandName+' '+Form),
			'T','F','F',
			@ClinicalID_New
			FROM   #NewMeds
			WHERE  ROWNUMBER=@loop

			SET @loop = @loop + 1 
						
			  
		END

		IF OBJECT_ID('tempdb..#NewMeds') IS NOT NULL  
					DROP TABLE #NewMeds 

 
		UPDATE P 
		set P.FindingDetail=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TMP.FD,'EyeContext',
		 (CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END)),
		'PRN',(CASE WHEN R.Refills='PRN' THEN 'PRN' ELSE ''END)),'Duration',
		(CASE WHEN R.Duration LIKE '[0-9]' THEN R.Duration+' days' ELSE TMP.FindingDuration END))
		,'Refills',CASE WHEN R.Refills='PRN' THEN '' ELSE R.Refills+' '+'REFILLS' END),'QtyUnit',R.Quantity+' '+R.QuantityUnit),'PO',
		(Case WHEN R.[Action]='Apply' THEN 'APPLY TO LIDS' WHEN R.[Action]='INSTILL' THEN 'INSTILL IN EYES' WHEN R.[Action]='' THEN  'PO' ELSE R.[Route] END)),
		'Frequency',R.Frequency),'Strength',R.Strength),'Dosage',R.Dose+' '+ R.DoseUnit)  
		,P.ImageDescriptor=CASE WHEN Left(Right(P.ImageDescriptor, 11),1) = '!'  THEN REPLACE(P.ImageDescriptor,Right(P.ImageDescriptor, 10),R.StartDate) ELSE P.ImageDescriptor END
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId

		--For ChangeERXdate 
		Update P set		
		P.ImageDescriptor=CASE 
							WHEN R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) THEN 'D'+R.StopDate+P.ImageDescriptor 
							ELSE STUFF(P.ImageDescriptor , 1, 1, 'D')
						  END
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId WHERE R.StopDate<>'' AND R.Deleted='n'

		Update P set
		P.ImageDescriptor=CASE 
							WHEN --R.StopDate<>Right(P.ImageDescriptor, 10) AND 
								R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) 
							THEN '!'+R.StopDate+P.ImageDescriptor 
							ELSE P.ImageDescriptor 
						END		
		,P.Status='D'
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId
		WHERE R.Deleted='y'
		
		----For-Patiet Note
		UPDATE  PN  SET  PN.Note1=R.PatientNotes
		FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID

		--For Phrma note 
		UPDATE  PN  SET  PN.Note1=R.OtherNotes
		FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID
	


		IF OBJECT_ID('tempdb..#Medication') IS NOT NULL  
			DROP TABLE #Medication 
	
		IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  
			DROP TABLE #RxSegments 

		IF OBJECT_ID('tempdb..#Response') IS NOT NULL  
					DROP TABLE #Response 
						
SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [DRFIRST].[UpdateMedicationResponse]    Script Date: 6/5/2018 6:37:11 PM ******/

ALTER     PROCEDURE [DRFIRST].[UpdateMedicationResponse]
@PatientID BIGINT,
@appointmentId BIGINT,
@xmlStringData varchar(max)

AS
SET NOCOUNT ON

--return 

IF OBJECT_ID('tempdb..#Medication') IS NOT NULL DROP TABLE #Medication 
IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  DROP TABLE #RxSegments 
--declare @PatientID bigint=40943sal
--declare @appointmentId bigint=3312639

		CREATE TABLE #RxSegments
		(
			ClinicalId int,
			SegTwo int,
			SegThree int,
			SegFour int,
			SegFive int,
			SegSix int,
			SegSeven int,
			SegNine int,
			LegacyDrug bit
		)

		INSERT INTO #RxSegments
		SELECT pc.clinicalid
		, CHARINDEX('-2', pc.FindingDetail) + 3
		, CHARINDEX(')-3', pc.FindingDetail) + 4
		, CHARINDEX(')-4', pc.FindingDetail) + 4
		, CHARINDEX(')-5', pc.FindingDetail) + 4
		, CHARINDEX(')-6', pc.FindingDetail) + 4
		, CHARINDEX(')-7', pc.FindingDetail) + 4
		, CHARINDEX('9-9', pc.FindingDetail)
		, CASE WHEN pc.FindingDetail like '%()%' THEN 0 ELSE 1 END 
		FROM PatientClinical pc
		WHERE-- pc.ClinicalId in (select clinicalid as number from patientclinical where clinicaltype='A' AND FindingDetail like 'Rx%' 
		 pc.PatientId = @PatientID
		AND FindingDetail like 'RX%'
		 AND clinicaltype='A' 
			
	
		select  DISTINCT pc.ClinicalId as ClinicalId,pc.PatientId ,A.appointmentid,
		(convert(varchar, convert(datetime, A.appdate), 101)+ ' ' 
	+ substring(A.appdate, 1, 2)+ ':' + substring(A.appdate, 3, 2)+ ':' + substring(A.appdate, 5, 2)) as AppDate, 	
		A.resourceid1 as DoctorExternalId,ResourceName,ResourcePid as LoginID,
		pc.findingdetail,
		SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)) as restFD1,
		(SELECT STUFF((SELECT char(9) + Note1
					FROM patientnotes where patientid=@PatientID AND   Notetype in('R')
					FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'),1,1,''))  AS PatientNote
			
		,(SELECT STUFF((SELECT char(9) + Note1
					FROM patientnotes where patientid=@PatientID And  notetype in('B','C','X')
					FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'),1,1,'')) AS OtherNote,
					pc.DrawfileName as DrugId,
					(CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') 
		ELSE '' END) as DrugName,
		('RX-8/'+
		(CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
			--THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') ELSE '' END)
			THEN  SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6) ELSE '' END)
		+
		'-2/(Dosage)(Strength)(EyeContext)-3/(Frequency)('+
		
		+'PRN)(PO)('+
		(CASE rx.LegacyDrug WHEN 0 THEN
			SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)) - 1)
		ELSE ''END) --as OTC,		
		+	')('+
			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-1)) +')('+
			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-1))
			+')-4/(Refills)('+
			SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-1) 
			+')(QtyUnit)('+				
		(CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)) - 1)
		ELSE ''	END)-- as LastTaken,
		+')-5/('+
		CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX(')', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)
	ELSE '' END 
	+')('+			
		--(CASE rx.LegacyDrug WHEN 0 THEN
		--SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegFive+1) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegFive+1)) - CHARINDEX('(', pc.FindingDetail, rx.SegFive+1) - 1)
		--ELSE ''	END)--as Supply,
		'DaySupply'

		+')'+		
		(SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)))) as FD,
		'' AS FD1,
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX(')', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)as FindingDuration,
		pc.[Status]
		INTO #Medication
		FROM PatientClinical pc 
		INNER JOIN appointments A ON A.Appointmentid=pc.Appointmentid
		INNER JOIN Resources R ON R.ResourceId=A.resourceid1	
		INNER JOIN #RxSegments rx on pc.ClinicalId = rx.ClinicalId
		LEFT OUTER JOIN PatientNotes pn1 on pn1.AppointmentId = pc.AppointmentId 
		AND Substring(PC.FindingDetail, 6, CHARINDEX('-2',pc.findingdetail)-6) = pn1.NoteSystem 
		AND pn1.NoteType = 'R'
		LEFT OUTER JOIN PatientNotes pn2 on pn2.AppointmentId = pc.AppointmentId
		AND SUBSTRING(pc.FindingDetail, 6, CHARINDEX('-2', pc.findingdetail)-6) = pn2.NoteSystem
		AND pn2.NoteType = 'X'			
		WHERE --pc.ClinicalId in (select clinicalid as number from patientclinical where  clinicaltype='A' AND FindingDetail like 'Rx%')
		pc.PatientId = @PatientID
		AND PC.FindingDetail like 'RX%'
		 AND PC.clinicaltype='A'

		
		DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)  
  
		SELECT   @XML= CAST(@xmlStringData AS XML)  
  
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML  
  
		SELECT 		
		Patient_ExtID, ClinicalID, PrescriptionRcopiaID,NDCID,
		FirstDataBankMedID,RcopiaID,
		RxnormID, RxnormIDType,
		BrandName,GenericName,DrugSchedule,BrandType,
		[RouteDrug],[Form],Strength,
		[Action],Dose, DoseUnit, 
		[Route], Frequency, Duration, 
		Quantity, QuantityUnit, 
		Refills,
		OtherNotes,PatientNotes,	
		SUBSTRING(SigChangedDate,1,10) as SigChangedDate,
		SUBSTRING(LastModifiedDate,1,10) as LastModifiedDate,
		SUBSTRING(StartDate,1,10) as StartDate,
		SUBSTRING(StopDate,1,10) as StopDate,
		SigChangedDate as response_SigChangedDate,
		LastModifiedDate as response_LastModifiedDate,
		StartDate as response_StartDate,
		StopDate as response_StopDate,

		Deleted,
		StopReason,
		MedicationRcopiaID,

		PharmacyRcopiaID ,
		PharmacyRcopiaMasterID,
		PharmacyDeleted ,
		PharmacyName ,
		PharmacyAddress1 ,
		PharmacyState ,
		PharmacyZip,
		PharmacyPhone ,
		PharmacyFax ,
		PharmacyIs24Hour ,
		PharmacyLevel3 ,
		PharmacyElectronic ,
		PharmacyMailOrder ,
		PharmacyRequiresEligibility ,
		PharmacyRetail ,
		PharmacyLongTermCare ,
		PharmacySpecialty ,
		PharmacyCanReceiveControlledSubstance 
		INTO #Response  
		FROM OPENXML(@hDoc, 'RCExtResponse/Response/MedicationList/Medication/Sig/Drug')  
		WITH   
		(  			
		Patient_ExtID [varchar](50) '../../Patient/ExternalID',  
		ClinicalID [varchar](50) '../../ExternalID',
		PrescriptionRcopiaID [varchar](50) '../../Prescription/RcopiaID',	

		NDCID [VARCHAR](100) 'NDCID',
		FirstDataBankMedID [VARCHAR](100) 'FirstDataBankMedID',
		RcopiaID [VARCHAR](100) 'RcopiaID',	
		
		RxnormID  [VARCHAR](100)'RxnormID',
		RxnormIDType  [VARCHAR](100)'RxnormIDType',
					
		BrandName [varchar](200) 'BrandName',  
		GenericName [varchar](200) 'GenericName',  
		DrugSchedule [varchar](200) 'Schedule', 
		BrandType [VARCHAR](100) 'BrandType',		
		[RouteDrug]  [varchar](200) 'Route',
		[Form] [varchar](100) 'Form',		
		Strength  [varchar](200) 'Strength',  
		
		[Action]  [varchar](200) '../Action', 				 	 
		Dose  [varchar](200) '../Dose',
		DoseUnit  [varchar](200) '../DoseUnit',
		[Route]  [varchar](200) '../Route',   
		Frequency  [varchar](200) '../DoseTiming',  
		Duration  [varchar](200) '../Duration',  
		Quantity  [varchar](200) '../Quantity',  		
		QuantityUnit  [varchar](200) '../QuantityUnit',  		
		Refills  [varchar](200) '../Refills',  
		OtherNotes  [varchar](200) '../OtherNotes',  
		PatientNotes  [varchar](200) '../PatientNotes', 
		  		
		SigChangedDate [varchar](50) '../../SigChangedDate' ,
		LastModifiedDate [varchar](50) '../../ExternLastModifiedDatealID' ,
		StartDate [varchar](50) '../../StartDate',
		StopDate [varchar](50) '../../StopDate',
		Deleted [varchar](50) '../../Deleted' ,
		StopReason [varchar](200) '../../StopReason',
		MedicationRcopiaID VARCHAR(50)'../../RcopiaID',

		PharmacyRcopiaID [varchar](50) '../../Pharmacy/RcopiaID',
		PharmacyRcopiaMasterID [varchar](50) '../../Pharmacy/RcopiaMasterID',
		PharmacyDeleted [varchar](50) '../../Pharmacy/Deleted',
		PharmacyName [varchar](50) '../../Pharmacy/Name',
		PharmacyAddress1 [varchar](50) '../../Pharmacy/Address1',
		PharmacyState [varchar](50) '../../Pharmacy/State',
		PharmacyZip [varchar](50) '../../Pharmacy/Zip',
		PharmacyPhone [varchar](50) '../../Pharmacy/Phone',
		PharmacyFax [varchar](50) '../../Pharmacy/Fax',
		PharmacyIs24Hour [varchar](50) '../../Pharmacy/Is24Hour',
		PharmacyLevel3 [varchar](50) '../../Pharmacy/Level3',
		PharmacyElectronic [varchar](50) '../../Pharmacy/Electronic',
		PharmacyMailOrder [varchar](50) '../../Pharmacy/MailOrder',
		PharmacyRequiresEligibility [varchar](50) '../../Pharmacy/RequiresEligibility',
		PharmacyRetail [varchar](50) '../../Pharmacy/Retail',
		PharmacyLongTermCare [varchar](50) '../../Pharmacy/LongTermCare',
		PharmacySpecialty [varchar](50) '../../Pharmacy/Specialty',
		PharmacyCanReceiveControlledSubstance [varchar](50) '../../Pharmacy/CanReceiveControlledSubstance'	
		 )  
		 EXEC sp_xml_removedocument @hDoc 
		
		INSERT INTO [DrFirst].[eRx_Medicine_Response]
      ([Patient_ExtID]   ,AppointmentId   ,[ClinicalID]      ,[NDCID]      ,[FirstDataBankMedID]      ,[RcopiaID]     
	   ,RxnormID, RxnormIDType,[BrandName]
      ,[GenericName] ,   DrugSchedule ,[BrandType]      ,[RouteDrug]      ,[Form]      ,[Strength]      ,[Action]
      ,[Dose]      ,[DoseUnit]      ,[Route]      ,[Frequency]      ,[Duration]      ,[Quantity]
      ,[QuantityUnit]      ,[Refills]      ,[OtherNotes]      ,[PatientNotes]      ,[SigChangedDate]      ,[LastModifiedDate]
      ,[StartDate]      ,[StopDate]      ,[Deleted]      ,[StopReason]      ,[MedicationRcopiaID], PrescriptionRcopiaID,
	    PharmacyRcopiaID , PharmacyRcopiaMasterID,PharmacyDeleted , PharmacyName , PharmacyAddress1 , PharmacyState , 
		PharmacyZip, PharmacyPhone , PharmacyFax ,PharmacyIs24Hour , PharmacyLevel3 , PharmacyElectronic , 
		PharmacyMailOrder , PharmacyRequiresEligibility , PharmacyRetail , PharmacyLongTermCare , PharmacySpecialty ,
		PharmacyCanReceiveControlledSubstance 	
	  )
	  SELECT  [Patient_ExtID] ,@appointmentId, [ClinicalID]      ,[NDCID]      ,[FirstDataBankMedID]      ,[RcopiaID]    
	   , RxnormID, RxnormIDType  ,[BrandName]      ,[GenericName],DrugSchedule
      ,[BrandType]      ,[RouteDrug]      ,[Form]      ,[Strength]      ,[Action]      ,[Dose]      ,[DoseUnit]      ,[Route]
      ,[Frequency]      ,[Duration]      ,[Quantity]      ,[QuantityUnit]      ,[Refills]      ,[OtherNotes]      ,[PatientNotes],
	  [response_SigChangedDate]      ,[response_LastModifiedDate]      ,
	  CASE WHEN [StartDate] ='' THEN [response_StopDate] ELSE [response_StartDate] END       ,
	  [response_StopDate]  ,[Deleted]      ,[StopReason]      ,[MedicationRcopiaID], PrescriptionRcopiaID,     
	  PharmacyRcopiaID , PharmacyRcopiaMasterID,PharmacyDeleted , PharmacyName , PharmacyAddress1 , PharmacyState , 
	  PharmacyZip, PharmacyPhone , PharmacyFax ,PharmacyIs24Hour , PharmacyLevel3 , PharmacyElectronic , 
	PharmacyMailOrder , PharmacyRequiresEligibility , PharmacyRetail , PharmacyLongTermCare , PharmacySpecialty ,
	PharmacyCanReceiveControlledSubstance 
	  FROM #Response WHERE [MedicationRcopiaID] NOT IN( SELECT Tmp.[MedicationRcopiaID] FROM [DrFirst].[eRx_Medicine_Response] Tmp WHERE Tmp.[Patient_ExtID]=@PatientID AND Tmp.Deleted=#Response.Deleted  AND Tmp.StopDate=#Response.StopDate )
			
		SELECT  ROW_NUMBER() OVER(ORDER BY MedicationRcopiaID) AS ROWNUMBER, 
		 --'1234567'  as Patient_ExtID, '1234567' AS AppointmentId,
		@PatientID AS Patient_ExtID ,@appointmentId AS AppointmentId,
		ClinicalID,MedicationRcopiaID,BrandName,Form,RouteDrug,
		(CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END) AS [EyeContext],
		'99' as [Symptom],
		 RcopiaID,NDCID,
		 'RX-8/'+R.BrandName+' '+R.Form+'-2/('+Dose+' '+DoseUnit+')('+Strength+')('+
		(CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END)
		+')-3/('+Frequency+')('+
		(CASE WHEN R.Refills='PRN' THEN '' ELSE '' END)+')('+
		--(Case WHEN R.[Action]='Apply' THEN 'APPLY TO LIDS' WHEN R.[Action]='INSTILL' THEN 'INSTILL IN EYES' WHEN R.[Action]='' THEN  'PO' ELSE R.[Route] END)+')()()()-4/('+
		 R.[Action] +' '+R.[Route] +')()()()-4/('+
		--(CASE WHEN R.Refills IN('PRN','') THEN '' ELSE R.Refills+' '+'REFILLS' END)+')()('+
		(CASE  WHEN R.Refills ='' THEN '' WHEN R.Refills ='PRN' THEN 'PRN REFILLS' ELSE R.Refills+' '+'REFILLS' END)+')()('+
		(R.Quantity+' '+R.QuantityUnit)+')()-5/()('+
		(CASE WHEN R.Duration <>'' THEN R.Duration+' DAYS SUPPLY' ELSE R.Duration END)+')-6/' as Findings,
	
		'!'+ CONVERT(varchar(20),CAST(Ap.appdate AS  DATE),101)   AS ImgDescriptor,

		--('!'+StartDate ) AS ImgDescriptor,
		FirstDataBankMedID,
		--'A' as [Status],
		(case when Deleted = 'n' then 'A' ELSE 'D'  end) AS [Status],
		PatientNotes,
		OtherNotes,
		StartDate,
		StopDate,
		PrescriptionRcopiaID		
		INTO #NewMeds
		FROM #Response R
		INNER  JOIN appointments ap ON Ap.Appointmentid=@appointmentId AND Ap.Patientid=@PatientID
		WHERE
		 (ClinicalID =''  OR ClinicalID=0)	
			AND R.MedicationRcopiaID NOT IN( SELECT RcopiyaID FROM [DrFirst].[ErxMedicationMapping] WHERE PatientID=@PatientID)
		AND   ISNULL(R.PrescriptionRcopiaID,'') NOT IN( SELECT PrescriptionRcopiaID FROM [DrFirst].[ErxMedicationMapping] WHERE PatientID=R.Patient_ExtID AND  PrescriptionRcopiaID IS NOT NULL)
	
		


		DECLARE @NewMedCount INT
		SELECT @NewMedCount= COUNT(*) FROM #NewMeds
		print @NewMedCount
--select COUNT(*) FROM #NewMeds
		DECLARE @loop INT
		SET @loop = 1

		WHILE @loop <= @NewMedCount
		BEGIN

			---SELECT  * FROM #NewMeds WHERE ROWNUMBER=@loop
			
			INSERT INTO dbo.PatientClinical
			(
			[AppointmentId]
			,[PatientId]
			,[ClinicalType]
			,[EyeContext]
			,[Symptom]
			,[FindingDetail]
			,[ImageDescriptor]      
			,[Status]
			,[DrawFileName]			
			,Activity)
			SELECT  AppointmentId, Patient_ExtID,'A',
			EyeContext,Symptom,
			--Findings, 
			CASE WHEN  PrescriptionRcopiaID IS NOT NULL THEN Findings+'P-7/' ELSE Findings END,
			ImgDescriptor, [Status], FirstDataBankMedID, CASE WHEN PrescriptionRcopiaID IS NOT NULL  THEN 'pen' ELSE '' END
			FROM #NewMeds TT 			
			WHERE ROWNUMBER=@loop

			DECLARE @ClinicalID_New BIGINT 
			SELECT @ClinicalID_New=IDENT_CURRENT('PatientClinical')

			--select @ClinicalID_New			
			INSERT INTO [DrFirst].[ErxMedicationMapping]
			([ClinicalId],[EncounterId],[PatientId],[RcopiyaID],[MedDatetime],PrescriptionRcopiaID)
			SELECT @ClinicalID_New, @appointmentId, @PatientID,MedicationRcopiaID,GETDATE() ,PrescriptionRcopiaID
			FROM #NewMeds  WHERE  ROWNUMBER=@loop

			INSERT INTO [DrFirst].[PatientClinicalMeds]
			([ClinicalId] ,ExternalId,RcopiaId, RxnormId, NDC, [AppointmentId], 
			[PatientId], [FindingDetail],[status], DrugId ,[CreatedDatetime],IsUpdate)
			Select @ClinicalID_New,@ClinicalID_New,MedicationRcopiaID,'',NDCID,
			@appointmentId,Patient_ExtID,Findings,[Status],FirstDataBankMedID,Getdate() ,0 
			From #NewMeds 
			WHERE  ROWNUMBER=@loop AND @ClinicalID_New NOT IN(Select [ClinicalId] FROM [DrFirst].[PatientClinicalMeds])
			
 

			IF NOT EXISTS (SELECT 1 FROM Drug D INNER JOIN #NewMeds N ON D.DisplayName=N.BrandName+' '+ N.Form  AND D.Focus=1 and D.DrugDosageFormId is NULL WHERE ROWNUMBER=@loop )
			BEGIN
				INSERT INTO Drug (Drugcode,DisplayName,Family1,Family2,Focus,DrugDosageFormId)
				SELECT 'T',BrandName+' '+Form,'','',1,NULL FROM #NewMeds WHERE RouteDrug='opht' AND  ROWNUMBER=@loop
			END

--select @ClinicalID_New

			INSERT INTO  dbo.PatientNotes 
			(
				PatientId,
				AppointmentId,
				NoteType,
				Note1,
				NoteDate,
				NoteSystem, 
				NoteCommentOn,
				NoteAudioOn,
				NoteClaimOn,
				ClinicalId 
			)
			SELECT 
			@PatientID, 
			@appointmentId,
			'R',
			PatientNotes,			
			RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),

			(BrandName+' '+Form),
			'T','F','F',
			@ClinicalID_New
			FROM   #NewMeds
			WHERE  ROWNUMBER=@loop

			INSERT INTO  dbo.PatientNotes 
			(
				PatientId,
				AppointmentId,
				NoteType,
				Note1,
				NoteDate,
				NoteSystem, 
				NoteCommentOn,
				NoteAudioOn,
				NoteClaimOn,
				ClinicalId 
			)
			SELECT 
			@PatientID, 
			@appointmentId,
			'X',
			OtherNotes,			
			RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),

			(BrandName+' '+Form),
			'T','F','F',
			@ClinicalID_New
			FROM   #NewMeds
			WHERE  ROWNUMBER=@loop			

			SET @loop = @loop + 1 								  
		END

		IF OBJECT_ID('tempdb..#NewMeds') IS NOT NULL  
					DROP TABLE #NewMeds 
 
		
		Update RS SET RS.clinicalid=Emm.Clinicalid 
		FROM  #Response  RS 
		INNER  JOIN [DrFirst].[ErxMedicationMapping] Emm ON RS.Patient_ExtID=Emm.Patientid 
		AND	(RS.PrescriptionRcopiaID=Emm.PrescriptionRcopiaID OR RS.MedicationRcopiaID=Emm.RcopiyaID)

		Update Emm SET Emm.RcopiyaID=RS.MedicationRcopiaID 
		FROM  #Response  RS 
		INNER  JOIN [DrFirst].[ErxMedicationMapping] Emm ON RS.Patient_ExtID=Emm.Patientid 
		AND RS.PrescriptionRcopiaID=Emm.PrescriptionRcopiaID
		
		
		UPDATE P 
		set P.FindingDetail=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TMP.FD,'EyeContext',
		 (CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END)),
		'PRN',(CASE WHEN R.Refills='PRN' THEN '' ELSE ''END)),'DaySupply',
		(CASE WHEN R.Duration <>'' THEN R.Duration+' DAYS SUPPLY' ELSE R.Duration END))
		,'(Refills)',CASE  WHEN R.Refills ='' THEN '()' WHEN R.Refills ='PRN' THEN '(PRN REFILLS)' ELSE '('+R.Refills+' '+'REFILLS'+')' END),'QtyUnit',R.Quantity+' '+R.QuantityUnit),'(PO)',
		--(Case WHEN R.[Action]='Apply' THEN 'APPLY TO LIDS' WHEN R.[Action]='INSTILL' THEN 'INSTILL IN EYES' WHEN R.[Action]='' THEN  'PO' ELSE R.[Route] END)),
		 LTRIM(RTRIM('('+ R.[Action]+' '+R.[Route]+')' ))),
		'Frequency',R.Frequency),'Strength',R.Strength),'Dosage',R.Dose+' '+ R.DoseUnit) 				 
			,Activity=CASE WHEN R.PrescriptionRcopiaID IS NOT NULL  THEN 'pen' ELSE Activity END		
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId
		
		UPDATE P  set P.FindingDetail= Case when  P.Activity='pen' AND CHARINDEX('/P-7', P.FindingDetail)=0 THEN REPLACE(P.FindingDetail,'-6/','-6/P-7/') ELSE  P.FindingDetail END 
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId
		WHERE P.Activity='pen'

			--For ChangeERXdate 
		Update P set
		--P.ImageDescriptor=CASE WHEN LEFT(P.ImageDescriptor, 1)<>'D' THEN 'D'+R.StopDate+P.ImageDescriptor ELSE P.ImageDescriptor END
		P.ImageDescriptor= CASE 
							WHEN R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) AND Left(P.ImageDescriptor, 1)='!' AND R.Stopdate= convert(varchar(10),getdate(),101) THEN 'D'+R.StopDate+P.ImageDescriptor 
							WHEN  R.StopDate=RIGHT(Left(P.ImageDescriptor, 11),10)  AND Left(P.ImageDescriptor, 1)='!' THEN STUFF(P.ImageDescriptor , 1, 1, 'D') ELSE P.ImageDescriptor
						  END
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId WHERE R.StopDate<>'' AND R.Deleted='n' 
		--AND R.Stopdate= convert(varchar(10),getdate(),101)

		

		Update P set
		P.ImageDescriptor=CASE 
							WHEN --R.StopDate<>Right(P.ImageDescriptor, 10) AND 
								R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) 
							THEN '!'+R.StopDate+P.ImageDescriptor 
							ELSE P.ImageDescriptor 
						END		
		,P.Status='D'
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId
		WHERE R.Deleted='y'
		
		----For-Patiet Note		
		UPDATE  PN  SET  PN.Note1=R.PatientNotes FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID 
			AND PN.PatientId=R.Patient_ExtID AND NoteType='R'AND R.Clinicalid <>''

		--For Phrma note 
		UPDATE  PN  SET  PN.Note1=R.OtherNotes
		FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID AND PN.PatientId=R.Patient_ExtID AND  NoteType='X'AND R.Clinicalid <>''

		INSERT INTO PatientNotes (PatientId,AppointmentId,NoteType,Note1,NoteDate,NoteSystem, NoteCommentOn,NoteAudioOn,NoteClaimOn,ClinicalId )
		SELECT  @PatientID, @appointmentId,'R',	PatientNotes,RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),
(BrandName+' '+Form),'T','F','F',ClinicalID 
		FROM  #Response R  WHERE R.ClinicalID NOT IN(Select  P.ClinicalID  FROM PatientNotes P  WHERE Patientid=@PatientID ) AND R.Clinicalid <>''


		INSERT INTO PatientNotes (PatientId,AppointmentId,NoteType,Note1,NoteDate,NoteSystem, NoteCommentOn,NoteAudioOn,NoteClaimOn,ClinicalId )
		SELECT  @PatientID, @appointmentId,'X',	OtherNotes,RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),
(BrandName+' '+Form),'T','F','F',ClinicalID 
		FROM  #Response R  WHERE R.ClinicalID NOT IN(Select  ClinicalID  FROM PatientNotes P  WHERE P.Patientid=@PatientID) AND R.Clinicalid <>''

	
	 UPDATE PCM  SET   PCM.ExternalID= R.ClinicalID, PCM.RcopiaId=R.MedicationRcopiaID , Isupdate=0,status=Deleted  FROM   #Response R  INNER JOIN Drfirst.[PatientClinicalMeds] PCM ON R.ClinicalID=PCM.ClinicalID
		

		IF OBJECT_ID('tempdb..#Medication') IS NOT NULL  
			DROP TABLE #Medication 
	
		IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  
			DROP TABLE #RxSegments 

		IF OBJECT_ID('tempdb..#Response') IS NOT NULL  
					DROP TABLE #Response 
	
SET NOCOUNT OFF


GO
