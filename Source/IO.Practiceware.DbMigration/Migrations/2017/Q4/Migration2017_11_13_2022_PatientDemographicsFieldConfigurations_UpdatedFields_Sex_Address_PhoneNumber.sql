--Script to update entries in [model].[PatientDemographicsFieldConfigurations] for fields like Sex, Address & Phone Number
--11/22/2017
GO
set ansi_padding on
UPDATE model.PatientDemographicsFieldConfigurations
SET IsVisibilityConfigurable=0,
IsVisible=1,
IsRequiredConfigurable=0,
IsRequired=1
WHERE Id in (37,10,11,13)
GO

SET ANSI_PADDING ON
GO
