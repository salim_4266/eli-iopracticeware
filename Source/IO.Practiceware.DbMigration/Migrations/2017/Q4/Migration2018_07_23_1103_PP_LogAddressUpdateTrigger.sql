

/****** Object:  Trigger [model].[PP_LogAddressUpdateTrigger]    Script Date: 7/17/2018 4:46:56 PM ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[PP_LogAddressUpdateTrigger]'))
DROP TRIGGER [model].[PP_LogAddressUpdateTrigger]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [model].[PP_LogAddressUpdateTrigger] ON [model].[PatientAddresses] AFTER UPDATE NOT FOR REPLICATION AS
BEGIN
SET NOCOUNT ON
DECLARE @PatientId INT
DECLARE @City VARCHAR(max)
DECLARE @Line1 VARCHAR(max)
DECLARE @Line2 VARCHAR(max)
DECLARE @PostalCode VARCHAR(max)
DECLARE @StateOrProvinceId INT
	
SELECT @PatientId = INSERTED.PatientId FROM INSERTED
		
DECLARE @AddressID INT
SELECT @AddressID = i.Id FROM inserted i;

UPDATE [model].[PatientAddresses] Set PostalCode=REPLACE(PostalCode, '-', '') Where Id=@AddressID
UPDATE [model].[PatientAddresses] SET PostalCode = REPLACE(PostalCode,' ', '') WHERE Id = @AddressID

IF EXISTS (Select 1 From [MVE].[PP_ERPInputLog] with (nolock) where PatientId = @PatientId)
BEGIN
	
	    IF EXISTS (Select 1 from [MVE].[PP_ERPInputLog] with (nolock) where PatientId = @PatientId and Status ='N')
			BEGIN
				UPDATE [MVE].[PP_ERPInputLog] SET IsAddressUpdated = 1, InputLogdate=GETDATE() WHERE PatientId = @PatientId and Status ='N'
			END
		Else
		BEGIN
			INSERT INTO [MVE].[PP_ERPInputLog](Email, PatientId, InputLogDate, IsEmailUpdated, IsPhoneUpdated, IsAddressUpdated, Status, ProcessedDate, IsPatientUpdated) 
			VALUES('', @PatientId, GETDATE(), 0, 0, 1, 'N', NULL, 1)
		END
	END
END
GO










