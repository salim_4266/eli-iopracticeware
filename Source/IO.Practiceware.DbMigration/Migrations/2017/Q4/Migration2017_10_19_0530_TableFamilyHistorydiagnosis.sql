SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FamilyHistoryIcd10]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FamilyHistoryIcd10](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FHN] [varchar](50) NOT NULL,
	[ICD10] [varchar](50) NOT NULL,
	[icd10code] [varchar](100) NULL,
	[SnomedId] [bigint] NULL,
 CONSTRAINT [PK_FamilyHistoryIcd10] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[FamilyHistoryIcd10] ON 

INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (1, N'RETINAL-PROBLEMS', N'Z83.518
', N'Z83.518', 431812006)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (2, N'CATARACT', N'H26.9', N'Z83.518', 160348002)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (3, N'GLAUCOMA', N'Z83.511', N'Z83.511', 160347007)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (4, N'CANCER-OF-THE-EYE', N'C69.90', N'C69.90', 363461003)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (5, N'BLINDNESS', N'Z82.1', N'Z82.1', 275118009)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (6, N'OTHER-EYE-DISORDER', N'Z83.518', N'Z83.518', 160346003)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (7, N'CANCER', N'Z80.8', N'Z80.9', 275937001)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (8, N'DIABETES', N'Z83.3', N'Z83.3', 160303001)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (9, N'HEART-DISEASE', N'Z82.49', N'Z82.49', 266894000)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (10, N'HIGH-BLOOD-PRESSURE', N'Z82.0', N'Z82.49', 160357008)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (11, N'UNKNOWN-FHX', N'Z83.518', N'Z78.9', 407559004)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (12, N'FAMILY', N'Z84.89', N'Z78.9', 407559004)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (13, N'STROKE', N'Z82.3', N'Z82.3', 275104002)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (14, N'SICKLE-CELL', N'Z83.2', N'Z83.2', 160320002)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (15, N'MIGRAINE-HEADACHES', N'Z82.0', N'Z82.0', 160342001)
INSERT [dbo].[FamilyHistoryIcd10] ([id], [FHN], [ICD10], [icd10code], [SnomedId]) VALUES (16, N'HD', N'Z82.49', N'Z82.49', 266894000)
SET IDENTITY_INSERT [dbo].[FamilyHistoryIcd10] OFF

SET ANSI_PADDING ON
GO
