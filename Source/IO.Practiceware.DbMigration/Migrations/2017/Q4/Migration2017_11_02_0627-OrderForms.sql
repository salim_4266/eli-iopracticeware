SET QUOTED_IDENTIFIER OFF 
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (1,'2 hr GT', '2 Hour Glucose Tolerance Test', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (2,'6 hr GT', '6 Hour Glucose Tolerance Test', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (3,'Car PTCA', 'Cardiac PTCA', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (4,'Car TV', 'Cardiac Tricuspid Vavlulopasty', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (5,'CBC', 'CBC', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (6,'Cl', 'Cl', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (7,'CMP', 'Complete Metabolic Profile', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (8,'CO2', 'CO2', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (9,'CT Chest', 'CT Chest', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (10,'CT', 'Cardiovascular Testing', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (11,'Dex Who', 'Dexa Scan Whole Body', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (12,'HBsAg', 'HBsAg', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (13,'Insulin', 'Insulin', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (14,'K', 'K', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (15,'LFTs', 'Liver Function Tests', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (16,'Lipids', 'Fasting Lipid Profile', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (17,'Na', 'Na', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (18,'Pachymetry', 'Pachymetry', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (19,'Progest', 'Progesterone', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (20,'PT', 'Pulmonary Thoracentesis', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (21,'RBG', 'Random Blood Glucose', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (22,'SPE', 'Serum Protein Electrophoresis', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (23,'UreaN', 'UreaN', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (24,'VA Cat', 'VA Cataract', 'A', '')
Insert into dbo.OrderForms(OrderTestKey, SHORTCUT, DESCRIPTION1, STATUS, COMMENTS) VALUES (25,'Vit A', 'Vitamin A Level', 'A', '')



SET ANSI_PADDING ON
GO