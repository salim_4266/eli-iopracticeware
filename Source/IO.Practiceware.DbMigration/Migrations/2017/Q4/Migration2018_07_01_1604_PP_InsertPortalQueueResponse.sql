

/****** Object:  StoredProcedure [MVE].[PP_InsertPortalQueueResponse]    Script Date: 06/29/2018 13:45:32 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_InsertPortalQueueResponse]')) 
Drop Procedure [MVE].[PP_InsertPortalQueueResponse]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  



CREATE procedure [MVE].[PP_InsertPortalQueueResponse]
(
@MessageData nvarchar(max),
@ActionTypeLookupId int,
@ProcessedDate datetime,
@PatientNo varchar(50),
@IsActive bit,
@ExternalId nvarchar(500)
)
AS
BEGIN

Insert into [MVE].[PP_PortalQueueResponse](MessageData,ActionTypeLookupId,ProcessedDate,PatientNo,IsActive,ExternalId)values (@MessageData,@ActionTypeLookupId,@ProcessedDate,@PatientNo,@IsActive,@ExternalId)
delete from [PP_PortalQueueException] where ActionTypeLookupId=@ActionTypeLookupId and ExternalId=@ExternalId

END

GO

