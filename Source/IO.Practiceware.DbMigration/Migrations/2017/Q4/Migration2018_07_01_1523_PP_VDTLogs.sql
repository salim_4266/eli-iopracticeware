

/****** Object:  Table [MVE].[PP_VDTLogs]    Script Date: 06/29/2018 13:30:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_VDTLogs]') AND type in (N'U'))
BEGIN

CREATE TABLE [MVE].[PP_VDTLogs](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[CDAId] [uniqueidentifier] NOT NULL,
	[CDAType] [nvarchar](100) NULL,
	[CDAUploadedDoctorId] [nvarchar](10) NULL,
	[CDAUploadedDateUTC] [datetime] NULL,
	[CreatedDateTime] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Message] [nvarchar](max) NULL,
	[OperationKey] [nvarchar](50) NULL,
	[PatientExternalId] [nvarchar](50) NULL,
	[UserExternalId] [nvarchar](50) NULL,
	[AlreadySent] [nvarchar](10) NULL,
 CONSTRAINT [PK_PP_VDTLogs_1] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END

