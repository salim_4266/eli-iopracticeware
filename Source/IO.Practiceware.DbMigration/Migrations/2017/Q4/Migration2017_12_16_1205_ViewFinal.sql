
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_FUNCTIONALSTATUS')) 
Drop View VIEW_ACI_FUNCTIONALSTATUS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_FUNCTIONALSTATUS]
AS
	Select 
		
		CAST(fs.ID as nvarchar(20)) AS ExternalId, --R
		CASE
			WHEN fs.functionalvalue = ''ADL'' THEN ''ADL''
			WHEN fs.functionalvalue = ''Functional'' THEN ''FUNCTIONAL'' 
			WHEN fs.functionalvalue = ''Congnitive'' THEN ''CONGNITIVE''  
			ELSE ''FUNCTIONAL''
		END AS Type, --R
		CAST(fs.conceptid AS NVARCHAR(20)) AS Code, --R
		fs.Term AS Description, --R
		''2.16.840.1.113883.6.96'' AS CodeSystem, --R
		''SNOMED-CT'' AS CodeSystemName, --R
		fs.status AS Status, --R
		case when fs.Appointmentdate is null Then ''01/01/1900''
		       else (CONVERT(VARCHAR(20), CONVERT(DATE,ap.AppDate), 101)) end
		 AS DateCaptured, --R
		Case  
		when fs.status = ''ACTIVE'' then ''True''
		else ''False''
		END AS IsActive, --R
		  id  AS FUNCSTATKEY,
   		  ap.appointmentid AS CHARTRECORDKEY
	 from model.functionalstatus fs WITH(NOLOCK)
	 left join  dbo.appointments ap WITH(NOLOCK)
	on ap.appointmentid = fs.appointmentid
' 
Go

 
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_MEDICALEQUIPMENT')) 
Drop View VIEW_ACI_MEDICALEQUIPMENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_MEDICALEQUIPMENT]
AS
	select 
		'MEDICALEQUIPMENT' AS RESOURCETYPE,
		CAST(pid.ID as nvarchar(20)) AS ExternalId, --R
		id.UniqueDeviceIDentifier as UDI,
		'FDA' as AssigningAuthority,-- Rev 1.23O
		'FDA' as ScopingAuthority, -- Rev 1.23O
		'Cardiac resynchronization therapy implantable pacemaker' AS DeviceName, --R
		id.companyname AS SupplierName, --R

		CASE
		when status= 1 then 'ACTIVE'
		else 'INACTIVE'
		end AS	StatusType, --R

		CASE
		when status= 1 then 'A'
		else 'I'
		end AS Statuscode, --R
		'704708004' as Code,--R
		'2.16.840.1.113883.6.96'CodeSystem,--R
		'SNOMED-CT' as CodeSystemName,--R

		CASE
		when status= 1 then 'True'
		else 'False'
		end AS IsActive, --R
		pid.appointmentid as ChartRecordKey,
		pid.patientid
	from dbo.PatientImplantableDevices pid WITH(NOLOCK) 
	left join dbo.implantabledevices id WITH(NOLOCK)
	on pid.implantabledeviceid = id.id
	
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'MVE.ACI_PP_SecureMessageLogs')) 
Drop View MVE.ACI_PP_SecureMessageLogs 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
Create View MVE.ACI_PP_SecureMessageLogs
As
SELECT 
ROW_NUMBER() OVER (order by CreatedDateTime ASC)AS Id
,LogId
,RecipientId
,PatientId
,IsSent2Repersenative
,RepersentativeId
,MessageSubject
,MessageSentTime
,CreatedDateTime
,IsResponseSent
FROM [MVE].[PP_SecureMessagesLogs] with(nolock)
Go

 
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'ACI_VIEW_JSON4_PATIENTDEMOGRAPHICSINFORMATION_ETHNICITY')) 
Drop View ACI_VIEW_JSON4_PATIENTDEMOGRAPHICSINFORMATION_ETHNICITY 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[ACI_VIEW_JSON4_PATIENTDEMOGRAPHICSINFORMATION_ETHNICITY] 
AS
	SELECT 
		'ETHNICITY' AS RESOURCETYPE,--R
		CAST(ISNULL(ET.CODE,'UNK') AS VARCHAR(10)) AS ETHNICITYCODE, --R
		CAST(ISNULL(ET.NAME,'Declined to Specify') AS VARCHAR(50)) AS ETHNICITYDECRIPTION,
		'2.16.840.1.113883.6.238' AS ETHNICITYCODESYSTEM, --R
		'CDC - RACE AND ETHNICITY' AS ETHINITYCODESYSTEMNAME, --R
		P.ID AS ID
	FROM MODEL.PATIENTS P(NOLOCK) left join [MODEL].[PATIENTETHNICITIES] PE WITH(NOLOCK) ON PE.PatientId=P.Id
	LEFT JOIN [MODEL].[ETHNICITIES] ET WITH(NOLOCK) ON PE.ETHNICITYID =ET.ID
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PROCEDURES')) 
Drop View VIEW_ACI_PROCEDURES 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_PROCEDURES]
AS
		SELECT
		''PROCEDURE'' AS REFERENCETYPE,
		p.Id AS ExternalId,
		CASE
		WHEN p.ProcedureDetail ='''' THEN ''TRUE''
		ELSE ''FALSE''
		END  AS IsInHouseProcedure,
		R.RESOURCENAME AS PerformedBy,
		(CONVERT(VARCHAR(20), CONVERT(DATE,ap.AppDate), 101)) AS DateofProcedure,
		CASE
		WHEN p.status = ''Inactive'' THEN ''COMPLETED''
		ELSE ''ACTIVE''
		END  AS STATUS,
		''TRUE'' AS ISACTIVE,
		P.AppointmentID as ChartRecordKey,
		P.id as COLLECTEDBILLINGKEY,id
		FROM [model].[ProceduresDetail] p (NOLOCK)
		INNER JOIN DBO.APPOINTMENTS AP WITH(NOLOCK) ON P.APPOINTMENTID = AP.APPOINTMENTID
		INNER JOIN DBO.RESOURCES R WITH(NOLOCK)	ON AP.RESOURCEID1 = R.RESOURCEID
'
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'ACI_VIEW_JSON4_PATIENTDEMOGRAPHICSINFORMATION_RACE')) 
Drop View ACI_VIEW_JSON4_PATIENTDEMOGRAPHICSINFORMATION_RACE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE VIEW [dbo].[ACI_VIEW_JSON4_PATIENTDEMOGRAPHICSINFORMATION_RACE] 
AS
	SELECT
		'RACE' AS RESOURCETYPE,--R
		CAST(ISNULL(RA.Code,'UNK') AS VARCHAR(10)) AS RACECODE, --R
	    CAST(ISNULL(RA.NAME,'Declined to Specify') AS VARCHAR(50)) AS RACEDESCRIPTION, --R
		'2.16.840.1.113883.6.238' AS RACECODESYSTEM,--R
		'CDC - RACE AND ETHNICITY' AS RACECODESYSTEMNAME,--R
		P.ID AS ID
	FROM MODEL.PATIENTS P (NOLOCK) LEFT JOIN  MODEL.PATIENTRACE PR WITH(NOLOCK) ON PR.Patients_Id =P.Id 
	LEFT JOIN MODEL.RACES RA WITH(NOLOCK) ON PR.RACES_ID = RA.ID
    
	UNION

	SELECT
		'RACE' AS RESOURCETYPE,--R
		CAST(ISNULL(RA.OMBCode,'UNK') AS VARCHAR(10)) AS RACECODE, --R
	    CAST(ISNULL(RA.OMBCategory,'Declined to Specify') AS VARCHAR(50)) AS RACEDESCRIPTION, --R
		'2.16.840.1.113883.6.238' AS RACECODESYSTEM,--R
		'CDC - RACE AND ETHNICITY' AS RACECODESYSTEMNAME,--R
		P.ID AS ID
	FROM MODEL.PATIENTS P (NOLOCK) LEFT JOIN  MODEL.PATIENTRACE PR WITH(NOLOCK) ON PR.Patients_Id =P.Id 
	LEFT JOIN MODEL.RACES RA WITH(NOLOCK) ON PR.RACES_ID = RA.ID
	WHERE OMBCode <> 'UNK'
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PROCEDURES_PROCEDURECODE')) 
Drop View VIEW_ACI_PROCEDURES_PROCEDURECODE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_PROCEDURES_PROCEDURECODE]
AS
     SELECT ''PROCEDURECODE'' AS RESOURCETYPE,	 
		ConceptID AS ProcedureCodeValue,
		Term AS ProcedureCodeDescription,
		''2.16.840.1.113883.6.96'' AS ProcedureCodeSystem,
		''SNOMED-CT'' AS ProcedureCodeName,
		appointmentid as ChartRecordKey,
		patientid,
		id as COLLECTEDBILLINGKEY,
		id from [model].[ProceduresDetail](nolock)

' 

Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PROCEDURES_SPECIMEN')) 
Drop View VIEW_ACI_PROCEDURES_SPECIMEN 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_PROCEDURES_SPECIMEN]
AS
     SELECT ''SPECIMEN'' AS RESOURCETYPE,
		''NOT AVAILABLE'' AS Name,
		''NOT AVAILABLE'' AS Code,
		Appointmentid as ChartRecordKey,
		patientid,
		id as COLLECTEDBILLINGKEY
		from [model].[ProceduresDetail](nolock)
' 
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PROCEDURES_DEVICE')) 
Drop View VIEW_ACI_PROCEDURES_DEVICE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_PROCEDURES_DEVICE]
AS
     SELECT ''DEVICE'' AS RESOURCETYPE,
            ''704708004'' AS CODE, --O 
            ''Cardiac resynchronization therapy implantable pacemaker'' AS NAME, --O 
			''SNOMED-CT'' AS CodeSystemName,
			''2.16.840.1.113883.6.96'' AS CodeSystem,
		pc.appointmentid as ChartRecordKey,
		pc.patientid,
		PC.id as COLLECTEDBILLINGKEY,pc.id
		from [model].[ProceduresDetail](nolock) pc
		where pc.ConceptID = 175135009
' 
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PROCEDURES_Reason')) 
Drop View VIEW_ACI_PROCEDURES_Reason 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_PROCEDURES_Reason]
AS
	SELECT 	DISTINCT
		pd.Id,
	    ''Reason'' As ResourceType,
	    ''PATIENT'' AS Type,
		''428191000124101'' AS Code,
		''Documentation of current medications'' AS Description,
		''SNOMED CT'' AS CodeSystemName,
		''2.16.840.1.113883.6.96'' AS CodeSystem,
		pd.AppointmentId as ChartRecordKey,
		pd.PatientId,
		id as COLLECTEDBILLINGKEY
		from [model].[ProceduresDetail](nolock) pd join  dbo.PatientClinical pc on pc.AppointmentId = pd.AppointmentId
		where pd.ConceptID = 428191000124101 and pc.symptom in(''/PQRS (MEDS, NOT DOCUMENTED)'') and pc.findingdetail like ''*DONE%''
' 
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PHYSICALEXAM')) 
Drop View VIEW_ACI_PHYSICALEXAM 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_PHYSICALEXAM]
AS
     SELECT DISTINCT ''PHYSICALEXAM'' AS RESOURCETYPE,
            (SUBSTRING((CONVERT(VARCHAR(20), ap.AppDate,101)),5,2)+''/'' + SUBSTRING((CONVERT(VARCHAR(20),ap.AppDate,101)),7,2)+''/'' +SUBSTRING((CONVERT(VARCHAR(20), ap.AppDate,101)),1,4) + '' 00:00:00 AM'') AS CREATEDDATE, --R 
            ''TRUE'' AS ISACTIVE, --O 
			PC.AppointmentId as ChartRecordKey,
			PC.PatientId
     FROM patientclinical PC WITH(NOLOCK) join dbo.Appointments ap with(nolock) on ap.AppointmentId = pc.appointmentid
' 
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PHYSICALEXAM_EXAMFIELD')) 
Drop View VIEW_ACI_PHYSICALEXAM_EXAMFIELD 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_PHYSICALEXAM_EXAMFIELD]
AS
	SELECT DISTINCT
			''EYE'' AS RESOURCETYPE,
			''BOTH'' AS Laterality,
            ''BMI'' AS TYPE, --R 
            ''39156-5'' AS CODE, --R 
            ''Body mass index (BMI) [Ratio]'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''BMI'' AS Event
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*BMI%'' and clinicaltype = ''F'' 
    UNION
    SELECT DISTINCT
			''EYE'' AS RESOURCETYPE,
			''BOTH'' AS Laterality,
            ''BPSystolic'' AS TYPE, --R 
            ''8480-6'' AS CODE, --R 
            ''SYSTOLIC BLOOD PRESSURE'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''SYS BP'' AS Event
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/BLOOD P%'' AND FINDINGDETAIL LIKE ''*BLOODPRESSURE%'' and clinicaltype = ''F'' 
    UNION
    SELECT DISTINCT
			''EYE'' AS RESOURCETYPE,
			''BOTH'' AS Laterality,
            ''BPDiastolic'' AS TYPE, --R 
            ''8462-4'' AS CODE, --R 
           -- ''Body mass index (BMI) [Ratio]'' AS DESCRIPTION, --R 
			''Diastolic blood pressure'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''DIA BP'' AS Event
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/BLOOD P%'' AND FINDINGDETAIL LIKE ''*BLOODPRESSURE%'' and clinicaltype = ''F'' 
    UNION
	SELECT	DISTINCT
			''Eye'' AS RESOURCETYPE,
			''Right'' AS Laterality, --OD
            ''CuptoDiscRatio'' AS TYPE, --R 
            ''71484-0'' AS CODE, --R 
            ''Cup and Disc [Ratio]'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''C/D'' AS Event
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE SYMPTOM = ''/CUP/DISC'' and clinicaltype = ''F'' and findingdetail like ''%*OD-CUP/DISC%''
	UNION
	SELECT	DISTINCT
			''EYE'' AS RESOURCETYPE,
			''Left'' AS Laterality, --OS
            ''CuptoDiscRatio'' AS TYPE, --R 
            ''71484-0'' AS CODE, --R 
            ''Cup and Disc [Ratio]'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''C/D'' AS Event
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE SYMPTOM = ''/CUP/DISC'' and clinicaltype = ''F'' and findingdetail like ''%*OS-CUP/DISC%''
	UNION
	SELECT  DISTINCT 
			''EYE'' AS RESOURCETYPE,
			''Right'' AS Laterality, --OD
            ''OpticDiscExam'' AS TYPE, --R 
            ''71486-5'' AS CODE, --R 
            ''Optic Nerve'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''OPTIC DISC'' AS Event
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE symptom like ''/%optic nerve%'' and clinicaltype = ''F'' and findingdetail like ''%*OD-OCTON%''
	UNION
	SELECT DISTINCT
			''EYE'' AS RESOURCETYPE,
			''Left'' AS Laterality, --OS
            ''OpticDiscExam'' AS TYPE, --R 
            ''71486-5'' AS CODE, --R 
            ''Optic Nerve'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''OPTIC DISC'' AS Event
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE symptom like ''/%optic nerve%'' and clinicaltype = ''F'' and findingdetail like ''%*OS-OCTON%''
	UNION
	SELECT  DISTINCT 
			''EYE'' AS RESOURCETYPE,
			''Right'' AS Laterality, --OD
            ''MacularEdema'' AS TYPE, --R 
            ''32451-7'' AS CODE, --R 
            ''Macular Edema'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''MAC'' AS Event
     FROM DBO.PatientClinicalIcd10 WITH(NOLOCK)
     WHERE findingdetailicd10 in (''E08.3291'', ''E08.3391'', ''E08.3491'', ''E08.3521'', ''E08.3531'', ''E08.3541'', ''E08.3551'', ''E08.3591'', ''E09.3291'', ''E09.3391'', ''E09.3491'', ''E09.3521'', ''E09.3531'', ''E09.3541'', ''E09.3551'', ''E09.3591'', ''E10.3291'', ''E10.3391'', ''E10.3491'', ''E10.3521'', ''E10.3531'', ''E10.3541'', ''E10.3551'', ''E10.3591'', ''E11.3291'', ''E11.3391'', ''E11.3491'', ''E11.3521'', ''E11.3531'', ''E11.3541'', ''E11.3551'', ''E11.3591'', ''E13.3291'', ''E13.3391'', ''E13.3491'', ''E13.3521'', ''E13.3531'', ''E13.3541'', ''E13.3551'', ''E13.3591'', ''E09.3311'', ''E09.3411'', ''E13.3511'', ''E13.3211'', ''E13.3311'', ''E13.3411'', ''E09.3511'', ''E08.3411'', ''E08.3511'', ''E10.3311'', ''E10.3411'', ''E10.3511'', ''E11.3211'', ''E11.3311'', ''E11.3411'', ''E11.3511'', ''E09.3211'', ''E08.3211'', ''E08.3311'', ''E10.3211'')
	UNION
    SELECT DISTINCT
			''EYE'' AS RESOURCETYPE,
			''Left'' AS Laterality, --OS
            ''MacularEdema'' AS TYPE, --R 
            ''32451-7'' AS CODE, --R 
            ''Macular Edema'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''MAC'' AS Event
			FROM DBO.PatientClinicalIcd10 WITH(NOLOCK)
			WHERE findingdetailicd10 in (''E09.3312'',''E09.3412'',''E13.3412'',''E13.3512'',''E13.3212'',''E13.3312'',''E09.3512'',''E08.3412'',''E08.3512'',''E10.3212'',''E10.3312'',''E10.3412'',''E10.3512'',''E11.3212'',''E11.3312'',''E11.3412'',''E11.3512'',''E09.3212'',''E08.3212'',''E08.3312'',''E0
8.3292'',''E08.3392'',''E08.3492'',''E08.3522'',''E08.3532'',''E08.3542'',''E08.3552'',''E08.3592'',''E09.3292'',''E09.3392'',''E09.3492'',''E09.3522'',''E09.3532'',''E09.3542'',''E09.3552'',''E09.3592'',''E10.3292'',''E10.3392'',''E10.3492'',''E10.3522'',''E10.3532'',''E10.3542'',''E10.3552'',''E10.
3592'',''E11.3292'',''E11.3392'',''E11.3492'',''E11.3522'',''E11.3532'',''E11.3542'',''E11.3552'',''E11.3592'',''E13.3292'',''E13.3392'',''E13.3492'',''E13.3522'',''E13.3532'',''E13.3542'',''E13.3552'',''E13.3592'')

UNION


	SELECT DISTINCT
			''EYE'' AS RESOURCETYPE,
			''BOTH'' AS Laterality, --OS
            ''MacularEdema'' AS TYPE, --R 
            ''32451-7'' AS CODE, --R 
            ''Macular Edema'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''MAC'' AS Event
		    FROM DBO.PatientClinicalIcd10 WITH(NOLOCK)
			WHERE findingdetailicd10 in (''E08.3293'',''E08.3393'',''E08.3493'',''E08.3523'',''E08.3533'',''E08.3543'',''E08.3553'',''E08.3593'',''E09.3293'',''E09.3393'',''E09.3493'',''E09.3523'',''E09.3533'',''E09.3543'',''E09.3553'',''E09.3593'',''E10.3293'',''E10.3393'',''E10.3493'',''E10.3523'',''E1
0.3533'',''E10.3543'',''E10.3553'',''E10.3593'',''E11.3293'',''E11.3393'',''E11.3493'',''E11.3523'',''E11.3533'',''E11.3543'',''E11.3553'',''E11.3593'',''E13.3293'',''E13.3393'',''E13.3493'',''E13.3523'',''E13.3533'',''E13.3543'',''E13.3553'',''E13.3593'',''E08.339'',''E09.329'',''E09.339'',''E09.349
'',''E13.359'',''E13.349'',''E13.319'',''E13.329'',''E13.339'',''E09.359'',''E08.349'',''E10.329'',''E10.339'',''E10.349'',''E10.359'',''E11.319'',''E11.329'',''E11.339'',''E11.349'',''E11.359'',''E09.319'',''E08.319'',''E08.329'',''E10.319'',''E08.359'',''E08.3313'',''E09.3313'',''E09.3413'',''E13.3413''
,''E13.3513'',''E13.3213'',''E13.3313'',''E09.3513'',''E08.3413'',''E08.3513'',''E10.3213'',''E10.3313'',''E10.3413'',''E10.3513'',''E11.3213'',''E11.3313'',''E11.3413'',''E11.3513'',''E09.3213'',''E08.3213'',''E09.331'',''E09.341'',''E09.351'',''E13.351'',''E13.311'',''E13.321'',''E13.331'',''E13.341
'',''E08.341'',''E08.351'',''E10.331'',''E10.341'',''E10.351'',''E11.311'',''E11.321'',''E11.331'',''E11.341'',''E11.351'',''E09.311'',''E09.321'',''E08.311'',''E08.331'',''E10.311'',''E10.321'')

UNION

SELECT  DISTINCT 
			''EYE'' AS RESOURCETYPE,
			''Right'' AS Laterality, --OD
            ''MacularEdema'' AS TYPE, --R 
            ''32451-7'' AS CODE, --R 
            ''Macular Edema'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''OPTIC DISC'' AS Event
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE symptom like ''/%macular%'' and clinicaltype = ''F'' and findingdetail like ''%*OD-OCTCMT%''


UNION


	SELECT DISTINCT
			''EYE'' AS RESOURCETYPE,
			''Left'' AS Laterality, --OS
            ''MacularEdema'' AS TYPE, --R 
            ''32451-7'' AS CODE, --R 
            ''Macular Edema'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''MAC'' AS Event
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE symptom like ''/%macular%'' and clinicaltype = ''F'' and findingdetail like ''%*OS-OCTCMT%''




UNION

	SELECT  DISTINCT 
			''EYE'' AS RESOURCETYPE,
			''Right'' AS Laterality, --OD
            ''BCCDVA'' AS TYPE, --R 
            ''419775003'' AS CODE, --R 
            ''Visual Acuity'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.96'' AS CODESYSTEM, --R 
            ''SNOMED CT'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''VISUAL'' AS Event
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE symptom like ''/%vision correct%'' and clinicaltype = ''F'' and findingdetail like ''%*OD-VC%''


UNION

	SELECT  DISTINCT 
			''EYE'' AS RESOURCETYPE,
			''Left'' AS Laterality, --OD
            ''BCCDVA'' AS TYPE, --R 
            ''419775003'' AS CODE, --R 
            ''Visual Acuity'' AS DESCRIPTION, --R 
            ''2.16.840.1.113883.6.96'' AS CODESYSTEM, --R 
            ''SNOMED CT'' AS CODESYSTEMNAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId,
			''VISUAL'' AS Event
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE symptom like ''/%vision correct%'' and clinicaltype = ''F'' and findingdetail like ''%*OS-VC%''

'

Go
 
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PHYSICALEXAM_REASON')) 
Drop View VIEW_ACI_PHYSICALEXAM_REASON 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  


EXEC dbo.sp_executesql @statement = N'

CREATE VIEW [dbo].[VIEW_ACI_PHYSICALEXAM_REASON]
AS
     SELECT DISTINCT ''Medical'' AS Type,
	 ''183932001'' AS CODE, --R 
     ''Procedure contraindicated'' AS DESCRIPTION,
	 ''SNOMEDCT'' as CodeSystemName, 
	 ''2.16.840.1.113883.6.96'' as CodeSystem,
      AppointmentId as ChartRecordKey,
	 PatientId,
	 ''BMI'' AS EVENT
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE SYMPTOM NOT LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*MEDICAL%''
	
	UNION	
	
     SELECT DISTINCT ''Patient'' AS Type,
	 ''105480006'' AS CODE, --R 
     ''Refusal of treatment by patient'' AS DESCRIPTION,
	 ''SNOMEDCT'' as CodeSystemName, 
	 ''2.16.840.1.113883.6.96'' as CodeSystem,
      AppointmentId as ChartRecordKey,
	 PatientId,
	 ''BMI'' AS EVENT
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE SYMPTOM NOT LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*PATIENT%''
	
	UNION

     SELECT DISTINCT ''Medical'' AS Type,
	 ''183932001'' AS CODE, --R 
     ''Procedure contraindicated'' AS DESCRIPTION,
	 ''SNOMEDCT'' as CodeSystemName, 
	 ''2.16.840.1.113883.6.96'' as CodeSystem,
      AppointmentId as ChartRecordKey,
	 PatientId,
	 ''BMI'' AS EVENT
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE SYMPTOM NOT LIKE ''/PQRI (POAG, OPTIC NERVE EVAL)'' AND (FINDINGDETAIL LIKE ''*NotDoneMed-PQRIPOAGON%'' OR FINDINGDETAIL LIKE ''*NotDoneOther-PQRIPOAGON%'')
	
	UNION	
	
     SELECT DISTINCT ''Medical'' AS Type,
	 ''183932001'' AS CODE, --R 
     ''Procedure contraindicated'' AS DESCRIPTION,
	 ''SNOMEDCT'' as CodeSystemName, 
	 ''2.16.840.1.113883.6.96'' as CodeSystem,
      AppointmentId as ChartRecordKey,
	 PatientId,
	 ''BMI'' AS EVENT
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE SYMPTOM NOT LIKE ''/PQRI (ARMD DILATED MACULAR EXAM)'' AND (FINDINGDETAIL LIKE ''*NotDoneMed-PQRIAARMDED%'' OR FINDINGDETAIL LIKE ''*NotDoneOther-PQRIAARMDED%'')
	
	UNION	
	
     SELECT DISTINCT ''Patient'' AS Type,
	 ''105480006'' AS CODE, --R 
     ''Refusal of treatment by patient'' AS DESCRIPTION,
	 ''SNOMEDCT'' as CodeSystemName, 
	 ''2.16.840.1.113883.6.96'' as CodeSystem,
      AppointmentId as ChartRecordKey,
	 PatientId,
	 ''BMI'' AS EVENT
     FROM DBO.PATIENTCLINICAL WITH(NOLOCK)
     WHERE SYMPTOM NOT LIKE ''/PQRI (ARMD DILATED MACULAR EXAM)'' AND FINDINGDETAIL LIKE ''*NotDonePat-PQRIAARMDED%''

' 

Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PHYSICALEXAM_EXTERNALEYE')) 
Drop View VIEW_ACI_PHYSICALEXAM_EXTERNALEYE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_PHYSICALEXAM_EXTERNALEYE]
AS
     SELECT DISTINCT ''EXTERNALEYE'' AS RESOURCETYPE,
		  '''' as Name,
            '''' AS FINDING, --O 
            '''' AS CODE, --O 
			AppointmentId as ChartRecordKey,
			PatientId
     FROM patientclinical WITH (NOLOCK);' 
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PHYSICALEXAM_EYE')) 
Drop View VIEW_ACI_PHYSICALEXAM_EYE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_PHYSICALEXAM_EYE]
AS
     SELECT DISTINCT
			''Eye'' AS RESOURCETYPE,
		    ''Right'' AS Laterality, --R --OD
			'''' as DilationResult,
			'''' as IOP,
			'''' as Refractive,
			'''' as Keratometry,
			'''' as Pupils,
			'''' as AnteriorSegment,
			'''' as PosteriorSegment,
            AppointmentId as ChartRecordKey,
			PatientId
     FROM patientclinical WITH (NOLOCK)

	UNION

     SELECT DISTINCT
			''Eye'' AS RESOURCETYPE,
		    ''Left'' AS Laterality, --R --OD
			'''' as DilationResult,
			'''' as IOP,
			'''' as Refractive,
			'''' as Keratometry,
			'''' as Pupils,
			'''' as AnteriorSegment,
			'''' as PosteriorSegment,
            AppointmentId as ChartRecordKey,
			PatientId
     FROM patientclinical WITH (NOLOCK)

	UNION

     SELECT DISTINCT
			''Eye'' AS RESOURCETYPE,
		    ''Both'' AS Laterality, --R --OD
			'''' as DilationResult,
			'''' as IOP,
			'''' as Refractive,
			'''' as Keratometry,
			'''' as Pupils,
			'''' as AnteriorSegment,
			'''' as PosteriorSegment,
            AppointmentId as ChartRecordKey,
			PatientId
     FROM patientclinical WITH (NOLOCK)
' 
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PASTHISTORY')) 
Drop View VIEW_ACI_PASTHISTORY 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_PASTHISTORY]
AS
     SELECT TOP 1 ''PASTHISTORY'' AS RESOURCETYPE,
            '''' AS EXTERNALID, --R 
            '''' AS HISTORYTYPE, --R 
            '''' AS LATERALITY, --R 
            '''' AS ISSURGERY, --R 
            ''FALSE'' AS ISACTIVE, --R 
            AppointmentId as ChartRecordKey,
			PatientId
     FROM patientclinical (NOLOCK);
' 
Go
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PASTHISTORY_HISTORYCODE')) 
Drop View VIEW_ACI_PASTHISTORY_HISTORYCODE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW VIEW_ACI_PASTHISTORY_HISTORYCODE
AS
     SELECT TOP 1 'HISTORYCODE' AS RESOURCETYPE,
            '' AS HISTORYCODEVALUE, --R 
            '' AS HISTORYCODEDESCRIPTION, --R 
            '' AS HISTORYCODESYSTEM, --R 
            '' AS HISTORYCODENAME, --R 
			AppointmentId as ChartRecordKey,
			PatientId
     FROM patientclinical (NOLOCK);
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'View_ACI_SocialHistory')) 
Drop View View_ACI_SocialHistory 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE VIEW [dbo].[VIEW_ACI_SOCIALHISTORY]
AS
 SELECT 
		'SOCIALHISTORY' AS RESOURCETYPE,
		CAST(pc.clinicalid AS NVARCHAR(20)) AS ExternalId,
		'Smoking' AS Type,
				sc.name AS  NAME,
		ISNULL(SC.SNOMEDCode,'')  AS CODE,
		'2.16.840.1.113883.6.96' AS CODESYSTEM, --R 
		'SNOMED-CT' AS CODESYSTEMNAME, --R 
		(CONVERT(VARCHAR(20), CONVERT(DATE,ap.AppDate), 101)) + ' 12:00:00 AM' AS Date,
		'' as EndDate, --as per ccda
		CASE
		 WHEN STATUS='A' THEN 'True'
		 ELSE 'False'
	     END AS IsActive,
		pc.clinicalid,
		pc.appointmentid as CHARTRECORDKEY,
		pc.patientid From dbo.patientclinical pc WITH(NOLOCK) left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
	left join model.smokingconditions sc on replace(sc.name,' ','') like '%'+SUBSTRING(Pc.FINDINGDETAIL,CHARINDEX('<',Pc.FINDINGDETAIL)+1,CHARINDEX('>',Pc.FINDINGDETAIL)-CHARINDEX('<',Pc.FINDINGDETAIL)-1) + '%'
	where pc.Symptom Like '%Smoking' and pc.FindingDetail Like '*%'
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_RISKASSESSMENT')) 
Drop View VIEW_ACI_RISKASSESSMENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_RISKASSESSMENT]
AS
     SELECT DISTINCT ''RISKASSESSMENT'' AS RESOURCETYPE,
            CHARTRECORDKEY AS EXTERNALID, --R 
            ''Tobacco use CPHS'' AS CODEDESCRIPTION, --R 
            ''39240-7'' AS CODEVALUE, --R 
            ''2.16.840.1.113883.6.1'' AS CODESYSTEM, --R 
            ''LOINC'' AS CODESYSTEMNAME, --R 
            substring(Date,1,10) AS DATE, --R 
            '''' AS REASON, --R 
            ''TRUE''  AS ISACTIVE, --R 
			CHARTRECORDKEY,
			PatientId
     FROM [dbo].[VIEW_ACI_SOCIALHISTORY] (NOLOCK);
'
Go

/****** Object:  View [dbo].[VIEW_ACI_RISKASSESSMENT_REASON]    Script Date: 3/20/2018 1:33:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_ACI_RISKASSESSMENT_REASON]'))
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_RISKASSESSMENT_REASON]
AS
     SELECT DISTINCT ''Reason'' AS RESOURCETYPE,
			''Medical'' AS Type,
            ''183932001'' AS Code, --R 
            ''Procedure contraindicated (situation)'' AS Description, --R 
            ''SNOMED CT'' as CodeSystemName, 
			''2.16.840.1.113883.6.96'' as CodeSystem,
            PC.AppointmentId AS CHARTRECORDKEY,
			PC.PatientId,
			''SMOKING'' AS TableName,
		  PC.AppointmentId as TableKey
     FROM dbo.patientclinical PC(NOLOCK) 
	 WHERE SYMPTOM LIKE ''/SMOKING'' AND FINDINGDETAIL LIKE ''*Contraindicated%''
' 
GO
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_INTERVENTION')) 
Drop View VIEW_ACI_INTERVENTION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_INTERVENTION]
AS
     SELECT DISTINCT ''INTERVENTION'' AS RESOURCETYPE,
            PC.CLINICALID AS EXTERNALID, --R 
            ''386464006'' AS CODEVALUE, --R 
            ''Prescribed diet education'' AS  CODEDESCRIPTION, --R 
            ''SNOMED CT'' as CodeSystemName, 
			''2.16.840.1.113883.6.96'' as CodeSystem,
            SUBSTRING(Ap.AppDate,5,2)+''/''+SUBSTRING(Ap.AppDate,7,2)+''/''+SUBSTRING(Ap.AppDate,1,4) AS DATE, --R 
            ''TRUE''  AS ISACTIVE, --R 
            PC.AppointmentId AS CHARTRECORDKEY,
			PC.PatientId,
			''BMI'' AS TableName,
		  PC.AppointmentId as TableKey
     FROM dbo.patientclinical PC(NOLOCK) join dbo.appointments ap(NOLOCK) on ap.appointmentid = pc.appointmentid
	 WHERE SYMPTOM LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*Counseling%''--bmi intervention 
	UNION
	SELECT DISTINCT ''INTERVENTION'' AS RESOURCETYPE,
            PC.CLINICALID AS EXTERNALID, --R 
            ''182964004'' AS CODEVALUE, --R 
            ''Terminal care'' AS  CODEDESCRIPTION, --R 
            ''SNOMED CT'' as CodeSystemName, 
			''2.16.840.1.113883.6.96'' as CodeSystem,
            SUBSTRING(Ap.AppDate,5,2)+''/''+SUBSTRING(Ap.AppDate,7,2)+''/''+SUBSTRING(Ap.AppDate,1,4) AS DATE, --R 
            ''TRUE''  AS ISACTIVE, --R 
            PC.AppointmentId AS CHARTRECORDKEY,
			PC.PatientId,
			''BMI'' AS TableName,
		  PC.AppointmentId as TableKey
     FROM dbo.patientclinical PC(NOLOCK) join dbo.appointments ap(NOLOCK) on ap.appointmentid = pc.appointmentid
	 WHERE SYMPTOM LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*Palliative Care%''--bmi intervention 
	UNION
	SELECT DISTINCT ''INTERVENTION'' AS RESOURCETYPE,
            PC.CLINICALID AS EXTERNALID, --R 
            ''185792005'' AS CODEVALUE, --R 
            ''Stop smoking'' AS  CODEDESCRIPTION, --R 
            ''SNOMED CT'' as CodeSystemName, 
			''2.16.840.1.113883.6.96'' as CodeSystem,
            SUBSTRING(Ap.AppDate,5,2)+''/''+SUBSTRING(Ap.AppDate,7,2)+''/''+SUBSTRING(Ap.AppDate,1,4) AS DATE,
            ''TRUE''  AS ISACTIVE, --R 
            PC.AppointmentId AS CHARTRECORDKEY,
			PC.PatientId,
			''SMOKING'' AS TableName,
		  PC.AppointmentId as TableKey
     FROM dbo.patientclinical PC(NOLOCK)  join dbo.appointments ap(NOLOCK) on ap.appointmentid = pc.appointmentid
	 WHERE SYMPTOM LIKE ''/SMOKING'' AND FINDINGDETAIL LIKE ''*Cessation%''
	UNION
	SELECT DISTINCT ''INTERVENTION'' AS RESOURCETYPE,
            PC.CLINICALID AS EXTERNALID, --R 
            ''28812006'' AS CODEVALUE, --R 
            ''Procedure education'' AS  CODEDESCRIPTION, --R 
            ''SNOMED CT'' as CodeSystemName, 
			''2.16.840.1.113883.6.96'' as CodeSystem,
            SUBSTRING(Ap.AppDate,5,2)+''/''+SUBSTRING(Ap.AppDate,7,2)+''/''+SUBSTRING(Ap.AppDate,1,4) AS DATE,
            ''TRUE''  AS ISACTIVE, --R 
            PC.AppointmentId AS CHARTRECORDKEY,
			PC.PatientId,
			''BP'' AS TableName,
		  PC.AppointmentId as TableKey
     FROM dbo.patientclinical PC(NOLOCK)  join dbo.appointments ap(NOLOCK) on ap.appointmentid = pc.appointmentid
	 WHERE SYMPTOM LIKE ''/BLOOD PRESSURE'' AND FINDINGDETAIL LIKE ''*Counseling%''

' 
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_ATTRIBUTE')) 
Drop View VIEW_ACI_ATTRIBUTE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_ATTRIBUTE]
AS
     SELECT TOP 1 ''ATTRIBUTE'' AS RESOURCETYPE,
            '''' AS EXTERNALID, --R 
            '''' AS ATTRIBUTETYPE, --R 
            '''' AS ATTRIBUTESUBTYPE, --O 
            '''' AS DATE, --R 
            '''' AS ATTRIBUTENAME, --R 
            ''FALSE''  AS ISACTIVE, --R 
            AppointmentId,
			PatientId
     FROM patientclinical(NOLOCK);
' 
 Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_CARETEAM')) 
Drop View VIEW_ACI_CARETEAM 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  


EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_CARETEAM]
AS
    SELECT ''CARETEAM'' AS RESOURCETYPE,
            R.RESOURCEID AS USEREXTERNALID,
		  R.RESOURCEFIRSTNAME +'' ''+ R.RESOURCELASTNAME AS USERNAME,
		  CASE
		    WHEN R.STATUS=''A'' THEN ''TRUE''
		    ELSE ''FALSE''
		  END ISACTIVE,
		  AppointmentID as ChartRecordKey
     FROM DBO.APPOINTMENTS A WITH (NOLOCK)
	JOIN DBO.RESOURCES R WITH (NOLOCK) ON R.RESOURCEID=A.RESOURCEID1
	WHERE R.ResourceType not in (''R'',''X'',''Z'') 
UNION

    SELECT ''CARETEAM'' AS RESOURCETYPE,
            R.RESOURCEID AS USEREXTERNALID,
		  R.RESOURCEFIRSTNAME +'' '' +R.RESOURCELASTNAME AS USERNAME,
		  CASE
		    WHEN R.STATUS=''A'' THEN ''TRUE''
		    ELSE ''FALSE''
		  END ISACTIVE,
		  AppointmentID as ChartRecordKey
     FROM DBO.APPOINTMENTS A WITH (NOLOCK)
	JOIN DBO.RESOURCES R WITH (NOLOCK) ON R.RESOURCEID=A.TechApptTypeId
	WHERE R.ResourceType not in (''R'',''X'',''Z'') 
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON29_DEPMessagingData')) 
Drop View VIEW_ACI_JSON29_DEPMessagingData 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON29_DEPMessagingData]
AS
     SELECT ''DEP'' AS ResourceType,--R
            dep.AciDepKey AS ExternalId,--R
            Cast(dep.AciDepKey as Varchar(50)) AS ReferenceNumber,--R VendorId.PracticeId.FacilityId.PhysicianId.PatientId.JsontypeId.Primarykey
            ''EHR'' AS Source,--R
            dep.AciDepKey AS ExternalReferenceId, --C
		   dep.PatientKey AS PatientAccountNumber,--R
		   ap.appointmentid AS EncounterId,--C
		  --''Referral'' AS ClinicalDocumentType,--R
		   D.DATA AS ClinicalDocumentType,
(CONVERT(VARCHAR(20), dep.CreatedDate, 101)+'' ''+(CASE
                                                                  WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), dep.CreatedDate, 22), 11))) = 10
                                                                  THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20),dep.CreatedDate, 22), 11))
                                                                  ELSE LTRIM(RIGHT(CONVERT(CHAR(20),dep.CreatedDate, 22), 11))
                                                              END)) as DEPRequestDate,
            RIGHT(''0000000000''+CONVERT(VARCHAR, SUBSTRING(isnull( R.NPI, ''''), 1, 10)), 10) AS OrderingECNPI,--R

		  dep.AciDepKey
		  From ACI_DEP Dep  (NOLOCK)
		  left JOIN model.ExternalContacts ec(NOLOCK) ON ec.Id = dep.PtOutboundReferralKey
		  left JOIN PracticeTransactionJournal PTC(NOLOCK) ON cast(ec.id as nvarchar(10)) = Replace(Substring(ptc.TransactionRemark,CharIndex(''('',ptc.TransactionRemark)+ 1,LEN(ptc.TransactionRemark) ),'')'','''')		
		  left join dbo.appointments ap WITH(NOLOCK)	on ptc.transactiontypeid = ap.appointmentid  
		  left Join DROPDOWN D (NOLOCK) ON D.data =dep.ClinicalDocumentType and d.TABLENAME = ''ACI_DEP'' and D.FIELDNAME =''ClinicalDocumentType''
		  left join dbo.resources R (NOLOCK) ON R.Resourceid=dep.ProviderKey
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_IMMUNIZATION')) 
Drop View VIEW_ACI_IMMUNIZATION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_IMMUNIZATION]
AS 
     SELECT  ''IMMUNIZATION'' AS RESOURCETYPE,
            EI.Id AS EXTERNALID, --R
            SUBSTRING(cast(EI.immunization as varchar(max)),(CHARINDEX(''<Imm>'',cast(EI.immunization as varchar(max)))+5),CHARINDEX(''</Imm>'',(cast(EI.immunization as varchar(max))))-((CHARINDEX(''<Imm>'',cast(EI.immunization as varchar(max)))+5))) AS VACCINENAME, --R
           SUBSTRING(cast(EI.immunization as varchar(max)),(CHARINDEX(''<ImmCode>'',cast(EI.immunization as varchar(max)))+9),CHARINDEX(''</ImmCode>'',(cast(EI.immunization as varchar(max))))-((CHARINDEX(''<ImmCode>'',cast(EI.immunization as varchar(max)))+9))) AS VACCINECODE, --R
            ''CVX'' AS VACCINETYPE, --R
            Ei.id AS FILLERORDERNUMBER, --R	
            SUBSTRING(cast(EI.immunization as varchar(max)),(CHARINDEX(''<ImmDateGiven>'',cast(EI.immunization as varchar(max)))+14),CHARINDEX(''</ImmDateGiven>'',(cast(EI.immunization as varchar(max))))-((CHARINDEX(''<ImmDateGiven>'',cast(EI.immunization as varchar(max)))+14)))+'':00 AM'' AS DATETIMESTARTOFADMINISTRATION, --R
            Ei.id AS SERIALNUMBER, --R
            SUBSTRING(cast(EI.immunization as varchar(max)),(CHARINDEX(''<ImmLotNo>'',cast(EI.immunization as varchar(max)))+10),CHARINDEX(''</ImmLotNo>'',(cast(EI.immunization as varchar(max))))-((CHARINDEX(''<ImmLotNo>'',cast(EI.immunization as varchar(max)))+10))) AS LOTNUMBER,
            CASE
				WHEN (CHARINDEX(''<ImmRefReason/>'',cast(EI.immunization as varchar(max)))>1) THEN '''' 
			ELSE SUBSTRING(cast(EI.immunization as varchar(max)),(CHARINDEX(''<ImmRefReason>'',cast(EI.immunization as varchar(max)))+14),CHARINDEX(''</ImmRefReason>'',(cast(EI.immunization as varchar(max))))-((CHARINDEX(''<ImmRefReason>'',cast(EI.immunization as varchar(max)))+14)))
			END AS REFUSALREASON, --O
           Ap.ResourceId1 AS ORDEREDBY, --R
            Ap.ResourceId1 AS PERFORMEDBY, --O
            CASE
				WHEN (CHARINDEX(''<ImmRefReason/>'',cast(EI.immunization as varchar(max)))>1) THEN ''CP'' 
			ELSE ''RE''
			END	 AS STATUSCODE, --R
            CASE
				WHEN (CHARINDEX(''<ImmRefReason/>'',cast(EI.immunization as varchar(max)))>1) THEN ''Completed'' 
			ELSE ''Cancelled''
			END AS STATUSDESCRIPTION, --R
            ''A'' AS ACTIONCODE, --R
            ''Add'' AS ACTIONNAME, --R	
            SUBSTRING(cast(EI.immunization as varchar(max)),(CHARINDEX(''<ImmDateGiven>'',cast(EI.immunization as varchar(max)))+14),CHARINDEX(''</ImmDateGiven>'',(cast(EI.immunization as varchar(max))))-((CHARINDEX(''<ImmDateGiven>'',cast(EI.immunization as varchar(max)))+14)))+'':00 AM'' AS CREATEDDATE, --O
            Ap.ResourceId1 AS CREATEDBY, --O
            ''True'' AS ISACTIVE, --R
		  Ei.id as PATIMMKEY,
            ei.EncounterId as ChartRecordkey
			,ap.PatientId,
			ei.id
     FROM [dbo].[ENCOUNTER_IMMUNIZATION](NOLOCK) EI join dbo.Appointments Ap (nolock) on ap.AppointmentId =ei.EncounterID ;
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_IMMUNIZATION_REASON')) 
Drop View VIEW_ACI_IMMUNIZATION_REASON 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_IMMUNIZATION_REASON]
AS 
     SELECT TOP 1 ''Patient'' AS Type,
	 '''' as Code,
	 '''' as Description,
	 ''CVX'' as CodeSystemName, -- CVX/NDC
	 ''2.16.840.1.113883.6.59'' as CodeSystem,
      AppointmentId as ChartRecordkey,
			PatientId,
			0 PATIMMKEY
     FROM patientclinical(NOLOCK);
' 
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON01_FACILITYINFORMATION')) 
Drop View VIEW_ACI_JSON01_FACILITYINFORMATION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON01_FACILITYINFORMATION]
AS
	SELECT 'FACILITY' AS RESOURCETYPE, --R
            CAST(PracticeId AS VARCHAR(10)) AS REFERENCENUMBER, --R
            'EHR' ISSOURCE, --R
            PRACTICENAME AS NAME, --R
            PracticeId AS ACCOUNT, --R
			'TRUE' As STATUS,
            PracticeId
     FROM [dbo].[PracticeName] WITH (NOLOCK) WHERE PracticeType='P'
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON29_DEPMessagingData_Attachments')) 
Drop View VIEW_ACI_JSON29_DEPMessagingData_Attachments 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON29_DEPMessagingData_Attachments] AS
     SELECT '' AS ContentDisposition,--R
            '' AS ContentType,--R
            '' AS FileName,--R
            '' AS Content,--R
			D.ACIDEPKEY From dbo.ACI_DEP D  (NOLOCK)
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_IMMUNIZATION_ROUTE')) 
Drop View VIEW_ACI_IMMUNIZATION_ROUTE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_IMMUNIZATION_ROUTE]
AS 
     SELECT ''ROUTE'' AS RESOURCETYPE,
            ''Intramuscular'' AS NAME, --O
            ''C28161'' AS CODE, --O
            AppointmentId as ChartRecordkey,
			ei.id as PATIMMKEY
			,ei.id
			,ei.EncounterID As ExternalId
     FROM [dbo].[ENCOUNTER_IMMUNIZATION](NOLOCK) EI join dbo.Appointments Ap (nolock) on ap.AppointmentId =ei.EncounterID ;

' 

Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON01_FACILITYINFORMATION_ADDRESS')) 
Drop View VIEW_ACI_JSON01_FACILITYINFORMATION_ADDRESS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON01_FACILITYINFORMATION_ADDRESS]
AS
     SELECT 'ADDRESS' AS RESOURCETYPE,
             case when isnull(PRACTICEADDRESS ,'') ='' Then 'NOT AVAILABLE' else PRACTICEADDRESS end AS ADDRESSLINE1, --R 
            '' AS ADDRESSLINE2, --O 
            CASE 
		    WHEN ISNULL(PRACTICECITY,'')='' THEN 'NOT AVAILABLE'
		    ELSE PRACTICECITY
		  END AS CITY, --R 
            CASE
		    WHEN ISNULL(PRACTICESTATE,'')='' THEN 'NOT AVAILABLE'
		    ELSE PRACTICESTATE
		  END AS STATE, --R 
            CASE
		    WHEN ISNULL(PRACTICEZIP,'')='' THEN '000000'
		    ELSE PRACTICEZIP
		  END AS ZIP, --R 
            'NOT AVAILABLE' AS COUNTY, --O 
            'NA' AS COUNTYCODE, --O 
            PracticeId
     FROM [dbo].[PracticeName] WITH (NOLOCK) WHERE PracticeType='P'

Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_IMMUNIZATION_MANUFACTURER')) 
Drop View VIEW_ACI_IMMUNIZATION_MANUFACTURER 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_IMMUNIZATION_MANUFACTURER]
AS 
     SELECT TOP 1 'MANUFACTURER' AS RESOURCETYPE,
            SUBSTRING(cast(EI.immunization as varchar(max)),(CHARINDEX('<ImmManu>',cast(EI.immunization as varchar(max)))+9),CHARINDEX('</ImmManu>',(cast(EI.immunization as varchar(max))))-((CHARINDEX('<ImmManu>',cast(EI.immunization as varchar(max)))+9))) AS NAME, --O
            SUBSTRING(cast(EI.immunization as varchar(max)),(CHARINDEX('<ImmManuCode>',cast(EI.immunization as varchar(max)))+13),CHARINDEX('</ImmManuCode>',(cast(EI.immunization as varchar(max))))-((CHARINDEX('<ImmManuCode>',cast(EI.immunization as varchar(max)))+13))) AS CODE, --O
            AppointmentId as ChartRecordkey,
			ei.id as PATIMMKEY,
			ei.id
			,ei.EncounterID As ExternalId
     FROM [dbo].[ENCOUNTER_IMMUNIZATION](NOLOCK) EI join dbo.Appointments Ap (nolock) on ap.AppointmentId =ei.EncounterID ;
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON01_FACILITYINFORMATION_PHONE')) 
Drop View VIEW_ACI_JSON01_FACILITYINFORMATION_PHONE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON01_FACILITYINFORMATION_PHONE]
AS
    SELECT 'PHONE' AS RESOURCETYPE,
            'Work' AS TYPE,
            CASE
				WHEN LEN(PRACTICEPHONE) < 10
                THEN'000-000-0000'
                WHEN LEN(PRACTICEPHONE) > 10
                THEN(SUBSTRING(PRACTICEPHONE, 1, 3)+'-'+SUBSTRING(PRACTICEPHONE, 4, 3)+'-'+SUBSTRING(PRACTICEPHONE, 7, 4)+'X'+SUBSTRING(PRACTICEPHONE, 11, 4))
                WHEN LEN(PRACTICEPHONE) = 10
                THEN(SUBSTRING(PRACTICEPHONE, 1, 3)+'-'+SUBSTRING(PRACTICEPHONE, 4, 3)+'-'+SUBSTRING(PRACTICEPHONE, 7, 4))
                ELSE PRACTICEPHONE
            END AS NUMBER,
            PracticeId
     FROM [dbo].[PracticeName] WITH (NOLOCK)
     WHERE LEN(PRACTICEPHONE) > 0--O AND PracticeType='P'
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_IMMUNIZATION_DOSAGEQUANTITY')) 
Drop View VIEW_ACI_IMMUNIZATION_DOSAGEQUANTITY 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW VIEW_ACI_IMMUNIZATION_DOSAGEQUANTITY
AS 
     SELECT TOP 1 'DOSAGEQUANTITY' AS RESOURCETYPE,
            '' AS VALUE, --O
            '' AS UNITS, --O
		  AppointmentId as ChartRecordkey,
			PatientId,
			0 as PATIMMKEY
     FROM patientclinical WITH (NOLOCK);		  
	 
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON02_ELIGIBLECLINICIANINFORMATION')) 
Drop View VIEW_ACI_JSON02_ELIGIBLECLINICIANINFORMATION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE VIEW  [dbo].[VIEW_ACI_JSON02_ELIGIBLECLINICIANINFORMATION]
AS
     SELECT 'EligibleClinician' AS RESOURCETYPE, 
            CAST(ResourceId AS VARCHAR(10)) AS REFERENCENUMBER, 
            'EHR' ISSOURCE, 
            RIGHT('00000000000'+CONVERT(VARCHAR, SUBSTRING(isnull( NPI, ''), 1, 10)), 10)  AS NPI, 
            RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(ResourceTaxId, ''), 1, 9)), 9) AS TIN, 
            ResourceId AS ECACCOUNTNUMBER, 
            ResourceFirstName AS GIVENNAME, 
            ResourceLastName FAMILYNAME, 
            '' AS SECONDANDFURTHERGIVENNAMES,  
		  --CONVERT(VARCHAR(20), '01/01/1900 12:00:00 AM', 101) AS  DATEOFBIRTH,
		  '' AS  DATEOFBIRTH,
            CASE 
		   WHEN ISNULL(ResourcePrefix,'')='' THEN 'Dr' 
		   ELSE ResourcePrefix 
		  END AS PREFIX, 
            CASE
		    WHEN ISNULL(ResourceSuffix,'')='' THEN 'MD'
		    ELSE ResourceSuffix
		  END AS SUFFIX,  
            'L' AS NAMETYPECODE, 
            'Legal Name' AS NAMETYPEDESCRIPTION, 
            CASE
                WHEN status = 'A'
                THEN 'True'
                ELSE 'False'
            END AS status,
            ResourceId
     FROM dbo.Resources  P(NOLOCK)
	WHERE ResourceType='D'


UNION

 SELECT 'EligibleClinician' AS RESOURCETYPE, 
            CAST(R.ResourceId AS VARCHAR(10)) AS REFERENCENUMBER, 
            'EHR' ISSOURCE, 
            RIGHT('00000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(R.NPI, ''), 1, 10)), 10)  AS NPI, 
            RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(R.ResourceTaxId, ''), 1, 9)), 9) AS TIN, 
            R.ResourceId AS ECACCOUNTNUMBER, 
            R.ResourceFirstName AS GIVENNAME, 
            R.ResourceLastName FAMILYNAME, 
            '' AS SECONDANDFURTHERGIVENNAMES,  
		  --CONVERT(VARCHAR(20), '01/01/1900 12:00:00 AM', 101) AS  DATEOFBIRTH,
		  '' AS DATEOFBIRTH,
            CASE 
		   WHEN ISNULL(R.ResourcePrefix,'')='' THEN 'Dr' 
		   ELSE R.ResourcePrefix 
		  END AS PREFIX, 
            CASE
		    WHEN ISNULL(R.ResourceSuffix,'')='' THEN 'MD'
		    ELSE R.ResourceSuffix
		  END AS SUFFIX,  
            'L' AS NAMETYPECODE, 
            'Legal Name' AS NAMETYPEDESCRIPTION, 
			 'False' AS status,
            R.ResourceId
     FROM dbo.Resources R (NOLOCK) join  model.Users U (NOLOCK)  on U.Id=R.ResourceId and U.__EntityType__ = 'Doctor'
	WHERE R.ResourceType='Z'
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION')) 
Drop View VIEW_ACI_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[VIEW_ACI_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION]
AS
     SELECT ''User'' AS RESOURCETYPE, 
            ''EHR'' ISSOURCE, 
            CAST(RESOURCEID AS VARCHAR(10)) AS REFERENCENUMBER, --REQUIRED 
            NULL AS PRODUCTKEY, --REQUIRED -filled from SQL2JSON 
            RESOURCEID AS EXTERNALID, -- REQUIRED
			ResourceFirstName AS GIVENNAME,
            --CASE
            --    WHEN ISNULL(RESOURCEMI,'')= ''
            --    THEN isnull(RESOURCELASTNAME,'''')+'',''+isnull(RESOURCEFIRSTNAME,'''')
            --    ELSE isnull(RESOURCELASTNAME,'''')+'',''+isnull(RESOURCEFIRSTNAME,'''')+'' ''+isnull(RESOURCEMI,'''')
            --END AS GIVENNAME, --REQUIRED 
            RESOURCELASTNAME AS FAMILYNAME, --REQUIRED 
            NULL AS SecondandFurtherGivenNames, --OPTIONAL 
            --''01/01/1900'' AS DATEOFBIRTH
		  --CONVERT(VARCHAR(20), ''01/01/1900 12:00:00 AM'', 101) AS DATEOFBIRTH
		 '' '' AS DATEOFBIRTH
            ,--We are making changes in UI to collect Birthdate --REQUIRED 
            ResourceDescription AS USERNAME, --REQUIRED
		  CASE ResourceType
			  WHEN ''A'' THEN ''Administrator''
			  WHEN ''D'' THEN ''Eligible Clinician''
			  WHEN ''O'' THEN ''Administrator''
			  ELSE ''Administrator''
			 -- ELSE ''Practice-Level User''
			  /*
			  WHEN ''N'' THEN ''Nurse''
			  WHEN ''O'' THEN ''Other''
			  WHEN ''Q'' THEN ''OpticalPrescriber''
			  WHEN ''R'' THEN ''RoomOrFacility''
			  WHEN ''S'' THEN ''Staff''
			  WHEN ''T'' THEN ''Technician''
			  WHEN ''U'' THEN ''OpticalUser''
			  WHEN ''X'' THEN ''Archived''
			  WHEN ''Z'' THEN ''Archived''
			  ELSE ''Clinical''
			  */
	       end AS ROLE, --REQUIRED //as per vasu
            --''Eligible Clinician'' AS ROLE, --REQUIRED
            CASE
                WHEN STATUS = ''A''
                THEN ''TRUE''
                ELSE ''FALSE''
            END AS ACTIVE,
            RESOURCEID
     FROM dbo.Resources WITH (NOLOCK)
	where resourcetype not in (''R'',''X'',''Y'',''Z'')


UNION

     SELECT ''User'' AS RESOURCETYPE, 
            ''EHR'' ISSOURCE, 
            CAST(R.RESOURCEID AS VARCHAR(10)) AS REFERENCENUMBER, --REQUIRED 
            NULL AS PRODUCTKEY, --REQUIRED -filled from SQL2JSON 
            R.RESOURCEID AS EXTERNALID, -- REQUIRED
            --CASE
            --    WHEN ISNULL(R.RESOURCEMI,'''')= ''''
            --    THEN isnull(R.RESOURCELASTNAME,'''')+'',''+isnull(R.RESOURCEFIRSTNAME,'''')

                --ELSE isnull(R.RESOURCELASTNAME,'''')+'',''+isnull(R.RESOURCEFIRSTNAME,'''')+'' ''+isnull(R.RESOURCEMI,'''')

            R.ResourceFirstName AS GIVENNAME, --REQUIRED 
            R.RESOURCELASTNAME AS FAMILYNAME, --REQUIRED 
            NULL AS SecondandFurtherGivenNames, --OPTIONAL 
            --''01/01/1900'' AS DATEOFBIRTH
		  --CONVERT(VARCHAR(20), ''01/01/1900 12:00:00 AM'', 101) AS DATEOFBIRTH
		  '''' AS DATEOFBIRTH
            ,--We are making changes in UI to collect Birthdate --REQUIRED 
            R.ResourceFirstName AS USERNAME, --REQUIRED
		  CASE R.ResourceType
			  WHEN ''X'' THEN ''Administrator''
			  WHEN ''Z'' THEN ''Eligible Clinician''
			  ELSE ''Practice-Level User''
			  /*
			  WHEN ''N'' THEN ''Nurse''
			  WHEN ''O'' THEN ''Other''
			  WHEN ''Q'' THEN ''OpticalPrescriber''
			  WHEN ''R'' THEN ''RoomOrFacility''
			  WHEN ''S'' THEN ''Staff''
			  WHEN ''T'' THEN ''Technician''
			  WHEN ''U'' THEN ''OpticalUser''
			  WHEN ''X'' THEN ''Archived''
			  WHEN ''Z'' THEN ''Archived''
			  ELSE ''Clinical'' 
			  */
	       end AS ROLE, --REQUIRED //as per vasu
            --''Eligible Clinician'' AS ROLE, --REQUIRED
			''FALSE'' AS ACTIVE,
            R.RESOURCEID
     FROM dbo.Resources R (NOLOCK) join  model.Users U (NOLOCK)  on U.Id=R.ResourceId
	where resourcetype in (''X'',''Y'',''Z'')'
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION_ASSOCIATEDPHYSICIAN')) 
Drop View VIEW_ACI_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION_ASSOCIATEDPHYSICIAN 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'



CREATE VIEW [dbo].[VIEW_ACI_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION_ASSOCIATEDPHYSICIAN]
AS
     SELECT ''2'' AS ECACCOUNTNUMBER,
			ResourceType,
            RESOURCEID
     FROM dbo.Resources
	where resourcetype not in (''R'') 
' 
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON05_REPRESENTATIVE')) 
Drop View VIEW_ACI_JSON05_REPRESENTATIVE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_JSON05_REPRESENTATIVE]
AS

SELECT 

	''Representative'' AS RESOURCETYPE, --R
	''Representative'' AS ROLE,
	''EHR'' AS ISSOURCE, --R
	CAST(PR.PATIENTREPRESENTATIVEID AS VARCHAR(10)) AS REFERENCENUMBER, --R
	CAST(PR.PATIENTID AS NVARCHAR(20)) AS PATIENTACCOUNTNUMBER, --R
	CAST(PR.PATIENTID AS NVARCHAR(20)) AS PORTALACCOUNTNUMBER, --C
	CAST(PR.[PATIENTREPRESENTATIVEID] AS NVARCHAR(20)) AS EXTERNALID, --R
	PR.FIRSTNAME AS GIVENNAME, --R
	PR.LASTNAME AS FAMILYNAME, --R
	'''' AS SECONDANDFURTHERGIVENNAMES, --O
	PR.RELATIONSHIP AS RELATIONSHIP, --R
	SUBSTRING(PR.RELATIONSHIP, 1, 3) AS RELATIONSHIPCODE, --R
	''2.16.840.1.'' AS RELATIONSHIPCODESYSTEM, --R
	''RESPONSIBLEPARTY'' AS RELATIONSHIPCODESYSTEMNAME, --R
	CONVERT(VARCHAR(20), DATEOFBIRTH, 101) AS DATEOFBIRTH,
            CASE
                WHEN PR.STATUS = 1
                THEN ''TRUE''
                ELSE ''FALSE''
            END AS STATUS,
		  PR.PATIENTREPRESENTATIVEID
    FROM MODEL.PATIENTREPRESENTATIVES PR WITH (NOLOCK)
    WHERE STATUS>=0

' 

Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON05_REPRESENTATIVE_ADDRESS')) 
Drop View VIEW_ACI_JSON05_REPRESENTATIVE_ADDRESS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_JSON05_REPRESENTATIVE_ADDRESS]
AS
     SELECT ''ADDRESS'' AS RESOURCETYPE,
            CASE
			WHEN (ADDRESSLINE1 = '''') OR (ADDRESSLINE1 is NULL) THEN ''NOT AVAILABLE''
			ELSE ADDRESSLINE1 
			END AS ADDRESSLINE1,

            ADDRESSLINE2 AS ADDRESSLINE2,

            CASE
			WHEN (CITY = '''') OR (CITY is NULL) THEN ''NOT AVAILABLA''
			ELSE CITY
			END AS CITY,

            CASE
			WHEN (STATE = '''') OR (STATE is NULL) THEN ''NA''
			ELSE STATE
			END AS STATE,

            CASE
			WHEN (ZIP = '''') OR (ZIP is NULL) THEN ''00000''
			ELSE ZIP
			END AS ZIP,

            ISNULL(COUNTY,''NOT AVAILABLE'') AS COUNTY,
            ISNULL(COUNTYCODE,''NA'') AS COUNTYCODE,
            COUNTRY AS COUNTRY,
		  PATIENTREPRESENTATIVEID
           
     FROM MODEL.[PATIENTREPRESENTATIVES]  WITH (NOLOCK) WHERE STATUS>=0
' 
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON29_DEPMessagingData_DirectAddress')) 
Drop View VIEW_ACI_JSON29_DEPMessagingData_DirectAddress 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_JSON29_DEPMessagingData_DirectAddress]
AS
     SELECT FromAddress AS FromProviderAddress,--R
		  ''0'' AS  FromAddressPassword,
            ToAddress AS ToProviderAddress,--R
		  Case(dep.HISP_Type) When ''D'' Then ''DataMotion'' When ''U'' Then ''Updox'' Else ''Secure Exchnage'' End AS HISP_Type,--R            
          ''Outbound'' AS TransactionType,--R
		  MessageSubject as  MessageSubject,
		  Message as MessageBody,
		  AciDepKey
		  from ACI_DEP dep (NOLOCK)
		  LEFT JOIN model.ExternalContacts ec(NOLOCK) ON ec.Id = dep.PtOutboundReferralKey
		  LEFT JOIN PracticeTransactionJournal PTC(NOLOCK) ON cast(ec.id as nvarchar(10)) = Replace(Substring(ptc.TransactionRemark,CharIndex(''('',ptc.TransactionRemark)+ 1,LEN(ptc.TransactionRemark) ),'')'','''')		
		  LEFT JOIN dbo.appointments ap WITH(NOLOCK)	on ptc.transactiontypeid = ap.appointmentid  		  
		  LEFT JOIN model.Patients p (NOLOCK) on p.id = AP.PatientId
' 

Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_MEDICATIONS')) 
Drop View VIEW_ACI_MEDICATIONS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'

CREATE VIEW [dbo].[VIEW_ACI_MEDICATIONS]
AS
  SELECT DISTINCT
			''Medications'' AS RESOURCETYPE, --R
            ''EHR'' [IsSource], --R
            CAST(pc.ClinicalID AS VARCHAR(10)) AS ReferenceNumber, --R
		  pc.ClinicalID  AS ExternalID, --R
            CAST(pc.Patientid AS NVARCHAR(20)) AS PatientAccountNumber, --R
            CASE
                 WHEN pc.FindingDetail LIKE ''%RX-8/%-2/%''
                THEN Replace(Replace(SUBSTRING(pc.findingdetail, 6, CHARINDEX(''-2/'', pc.findingdetail)-6), ''&'', ''and''), ''+'', ''Plus'')
                ELSE ''''
            END AS DrugName, --R
		    ISNULL((Select top (1) RxNorm from [dbo].[FDBtoRxNorm1] where FDBId = nc.medid),''00000'')AS DRUGCODE,
            ''2.16.840.1.113883.6.88'' AS DRUGCODESYSTEM, --R
            ''RxNorm'' AS DRUGCODESYSTEMNAME, --R

            CASE dd.focus
                WHEN 0
                THEN ''SYSTEMIC''
                ELSE ''OCULAR''
            END AS MedicationType, --R

            CASE SUBSTRING(pc.findingdetail, (CHARINDEX(''-4/'', pc.findingdetail)+4), 1)
                WHEN '')''
                THEN ''NEW''
                WHEN ''0''
                THEN ''NEW''
                ELSE ''Refill''
            END AS PrescriptionType, --R


   		SUBSTRING(pc.findingdetail, CHARINDEX(''-3/'', pc.findingdetail)+4, (CHARINDEX('')'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''-3/'', pc.findingdetail), LEN(pc.findingdetail)), ''-3/'', '''')))-2))
   		 AS Directions, --R

		
            CASE SUBSTRING(pc.findingdetail, CHARINDEX(''-3/'', pc.findingdetail)+4, (CHARINDEX('')'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''-3/'', pc.findingdetail), LEN(pc.findingdetail)), ''-3/'', '''')))-2))
                WHEN ''AS PRESCRIBED''
                THEN PN.NOTE1 + PN.NOTE2 + PN.NOTE3 + PN.NOTE4
                ELSE ''''
            END AS AdditionalInstructions, --R

			nc.MED_STRENGTH +'' ''+ nc.MED_STRENGTH_UOM as Strength,

            ''Medication'' AS SUBSTITUTION, --R

            RTRIM(LTRIM((CONVERT(VARCHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 101)+'' ''+(CASE
                                                   WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))) = 10
                                                   THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))
                                                   ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))
                                               END)))) AS StartDate, --R

            RTRIM(LTRIM((CONVERT(VARCHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 101)+'' ''+(CASE
                                                   WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))) = 10
                                                   THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))
                                                   ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))
                                               END)))) AS EffectiveDate, --R

            CASE
                WHEN SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-5/'', pc.FindingDetail)+4)+1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-5/'', pc.FindingDetail)+4))-CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-5/'', pc.FindingDetail)+4)-1) = ''''
                THEN ''1 DAY'' --ACI is not accepting 0.00 so we are sending 0.01 as default for ColdStart
                ELSE SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-5/'', pc.FindingDetail)+4)+1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-5/'', pc.FindingDetail)+4))-CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-5/'', pc.FindingDetail)+4)-1)
            END AS DaysSupply, --R



            CASE
                WHEN CHARINDEX(''P-7'', pc.FindingDetail) > 0
                THEN ''TRUE''
                ELSE ''FALSE''
            END AS IsPrescribed, --R
			CASE
			WHEN CHARINDEX(''P-7'', pc.FindingDetail) = 0 THEN ''''
			ELSE
		  RTRIM(LTRIM((CONVERT(VARCHAR(20), CONVERT(DATETIME, RIGHT(pc.imagedescriptor, 10)), 101)+'' ''+(CASE
                                                   WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, RIGHT(pc.imagedescriptor, 10)), 22), 11))) = 10
                                                   THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, RIGHT(pc.imagedescriptor, 10)), 22), 11))
                                                   ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, RIGHT(pc.imagedescriptor, 10)), 22), 11))
                                               END)))) 
			END AS EprescribedDate,
            ''True'' AS ISQUERIEDFORDRUGFORMULARY, --R
            ''True'' AS ISCPOE, --R
            ''False'' AS ISHIGHRISKMEDICATION, --R
            ''False'' AS ISCONTROLSUBSTANCE, --R
            '''' AS PHARMACYNAME, --O

            		CASE
		WHEN ((select count(*) from model.PatientReconcileMedications where RxNorm in ((Select RxNorm from [dbo].[FDBtoRxNorm1] where FDBId = nc.medid)) and Appointmentid = pc.appointmentid and status = 1)>0) 
		THEN       RTRIM(LTRIM((CONVERT(VARCHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 101)+'' ''+(CASE
                                                   WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))) = 10
                                                   THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))
                                                   ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))
                                               END))))
		ELSE ''''
		END AS ReconciledDate, --C

            CASE
                WHEN SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-4/'', pc.FindingDetail)+4)+1))+1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-4/'', pc.FindingDetail)+3)+1)+1))-CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-4/'', pc.FindingDetail)+3)+1))-1) = ''''
                THEN ''1 DAY'' --ACI is not accepting 0.00 so we are sending 0.01 as default for ColdStart
                ELSE SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-4/'', pc.FindingDetail)+4)+1))+1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-4/'', pc.FindingDetail)+3)+1)+1))-CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-4/'', pc.FindingDetail)+3)+1))-1)
            END AS DosageQuantity, --R  NEED TO CHECK FOR VALUE AND UNITS


            SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-3/'', pc.FindingDetail)+4)+1))+1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-3/'', pc.FindingDetail)+3)+1)+1))-CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-3/'', pc.FindingDetail)+3)+1))-1) AS Code, --R

            CASE SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-3/'', pc.FindingDetail)+4)+1))+1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-3/'', pc.FindingDetail)+3)+1)+1))-CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-3/'', pc.FindingDetail)+3)+1))-1)
                WHEN ''INSTILL IN EYES''
                THEN ''INSTILL IN EYES''
                WHEN ''APPLY TO LIDS''
                THEN ''APPLY TO LIDS''
                WHEN ''PO''
                THEN ''BY MOUTH''
                ELSE ''''
            END AS Description, --R
            ''2.16.840.1.113883.3.26.1'' AS CODESYSTEM, --R
            ''Medication Route'' AS CODESYSTEMNAME, --R
            '''' AS DateEnd, --C
		CASE
                WHEN PC.STATUS = ''A''
                THEN ''ACTIVE''
                ELSE ''INACTIVE''
            END AS STATUS,
            CASE
                WHEN PC.STATUS = ''A''
                THEN ''TRUE''
                ELSE ''FALSE''
            END AS ACTIVE, --R
		  ''C'' as ERXSOURCE,
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.appointmentid as Schedulekey,
		 pc.ClinicalId as PATIENTMEDICATIONSKEY
  --   FROM dbo.patientclinical pc WITH (NOLOCK)
  --        JOIN dbo.appointments ap WITH (NOLOCK) ON pc.appointmentid = ap.appointmentid
  --        JOIN dbo.resources r WITH (NOLOCK) ON ap.resourceid1 = r.resourceid
  --        JOIN dbo.drug dd WITH (NOLOCK) ON dd.displayname = Replace(Replace(SUBSTRING(pc.findingdetail, 6, CHARINDEX(''-2/'', pc.findingdetail)-6), ''&'', ''and''), ''+'', ''Plus'') 
  --        left join fdb.tblcompositedrug nc WITH(NOLOCK) on Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex(''-2/'', pc.findingdetail) - 6),''&'',''and''), ''+'',''Plus'') =					nc.MED_ROUTED_DF_MED_ID_DESC and ((REPLACE(SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-2/'', pc.FindingDetail) + 4) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('',pc.FindingDetail, CHARINDEX(''-2/'', pc.FindingDetail) + 4)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-2/'', pc.FindingDetail) + 4) - 2),'' '','''') = nc.MED_STRENGTH)or (REPLACE(SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-2/'', pc.FindingDetail) + 4) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('',pc.FindingDetail, CHARINDEX(''-2/'', pc.FindingDetail) + 4)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-2/'', pc.FindingDetail) + 4) - 6),'' '','''') = nc.MED_STRENGTH))
  --       -- LEFT JOIN [fdb].[RXCUI2MEDID] rm on rm.medid = nc.medid
		--  LEFT JOIN DBO.PATIENTNOTES PN WITH (NOLOCK) ON PN.APPOINTMENTID = PC.APPOINTMENTID
  --                                                       AND PN.NOTETYPE = ''R''
  --                                                       AND Replace(Replace(SUBSTRING(pc.findingdetail, 6, CHARINDEX(''-2/'', pc.findingdetail)-6), ''&'', ''and''), ''+'', ''Plus'') = PN.NOTESYSTEM
  --   WHERE pc.clinicaltype = ''A''
  --       AND findingdetail LIKE ''RX-8%'' and pc.findingdetail not like ''%P-7%''
		--and SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-2/'', pc.FindingDetail) + 4) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-2/'', pc.FindingDetail) + 4)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX(''-2/'', pc.FindingDetail) + 4) - 1) <>''''
		--and  (((Replace(Replace(SUBSTRING(pc.findingdetail, 6, CHARINDEX(''-2/'', pc.findingdetail)-6), ''&'', ''and''), ''+'', ''Plus'')) = ''CEFTRIAXONE SOLUTION FOR INJECTION'')or((Replace(Replace(SUBSTRING(pc.findingdetail, 6, CHARINDEX(''-2/'', pc.findingdetail)-6), ''&'', ''and''), ''+'', ''Plus'')) = ''ARANESP (IN POLYSORBATE) INJECTION SYRINGE''))
		----and pc.patientid = 46896
		--and pc.ImageDescriptor not like ''C%'' and pc.status <>''D''

		From
		dbo.patientclinical pc WITH(NOLOCK) left join dbo.appointments ap WITH(NOLOCK)	on pc.appointmentid = ap.appointmentid
		join fdb.tblcompositedrug nc WITH(NOLOCK) on nc.medid = pc.drawfilename
		left join  dbo.resources r WITH(NOLOCK)	on ap.resourceid1 = r.resourceid
		left join dbo.drug dd WITH(NOLOCK) on Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex(''-2/'', pc.findingdetail) - 6),''&'',''and''), ''+'',''Plus'') =  dd.displayname
		LEFT JOIN DBO.PATIENTNOTES PN  WITH(NOLOCK) ON PC.APPOINTMENTID = PN.APPOINTMENTID AND PN.NOTETYPE= ''R'' AND Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex(''-2/'', pc.findingdetail) - 6),''&'',''and''), ''+'',''Plus'') = PN.NOTESYSTEM
		where pc.clinicaltype = ''A'' and findingdetail like ''RX-8%'' and pc.findingdetail not like ''%P-7%'' and nc.status = ''A'' and pc.ImageDescriptor not like ''C%'' and pc.Status <> ''D''




UNION

 SELECT DISTINCT
			''Medications'' AS RESOURCETYPE, --R
            ''EHR'' [IsSource], --R
            CAST(pc.ClinicalID AS VARCHAR(10)) AS ReferenceNumber, --R
		  pc.ClinicalID  AS ExternalID, --R
            CAST(pc.Patientid AS NVARCHAR(20)) AS PatientAccountNumber, --R
            ''No Known Medication'' DrugName, --R
		  --ISNULL(RM.RxCUI,''00000'')AS DRUGCODE, --R 
		  ''410942007''AS DRUGCODE, --R
            ''2.16.840.1.113883.6.96'' AS DRUGCODESYSTEM, --R
            ''SNOMED-CT'' AS DRUGCODESYSTEMNAME, --R

            '''' AS MedicationType, --R

            '''' AS PrescriptionType, --R


            '''' AS Directions, --R

		
            '''' AS AdditionalInstructions, --R

            '''' Strength, --R

            ''Medication'' AS SUBSTITUTION, --R

            RTRIM(LTRIM((CONVERT(VARCHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 101)+'' ''+(CASE
                                                   WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))) = 10
                                                   THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))
                                                   ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))
                                               END)))) AS StartDate, --R

            RTRIM(LTRIM((CONVERT(VARCHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 101)+'' ''+(CASE
                                                   WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))) = 10
                                                   THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))
                                                   ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME, LTRIM(RTRIM(RIGHT(pc.imagedescriptor, 10)))), 22), 11))
                                               END)))) AS EffectiveDate, --R

            '''' DaysSupply, --R



            ''FALSE'' AS IsPrescribed, --R
		  '''' AS EprescribedDate,
            ''True'' AS ISQUERIEDFORDRUGFORMULARY, --R
            ''True'' AS ISCPOE, --R
            ''False'' AS ISHIGHRISKMEDICATION, --R
            ''False'' AS ISCONTROLSUBSTANCE, --R
            '''' AS PHARMACYNAME, --O

            '''' AS ReconciledDate, --C

            '''' DosageQuantity, --R  NEED TO CHECK FOR VALUE AND UNITS


           '''' AS Code, --R

            '''' AS Description, --R
            ''2.16.840.1.113883.3.26.1'' AS CODESYSTEM, --R
            ''Medication Route'' AS CODESYSTEMNAME, --R
            '''' AS DateEnd, --C
		CASE
                WHEN PC.STATUS = ''A''
                THEN ''ACTIVE''
                ELSE ''INACTIVE''
            END AS STATUS,
            CASE
                WHEN PC.STATUS = ''A''
                THEN ''TRUE''
                ELSE ''FALSE''
            END AS ACTIVE, --R
		  ''C'' as ERXSOURCE,
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.appointmentid as Schedulekey,
		 pc.ClinicalId as PATIENTMEDICATIONSKEY
     FROM dbo.patientclinical pc WITH (NOLOCK)
          JOIN dbo.appointments ap WITH (NOLOCK) ON pc.appointmentid = ap.appointmentid
          JOIN dbo.resources r WITH (NOLOCK) ON ap.resourceid1 = r.resourceid
     WHERE pc.findingdetail like ''%RX%No Known Medication%'' and pc.status <>''D''
' 



 Go
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON05_REPRESENTATIVE_PHONE')) 
Drop View VIEW_ACI_JSON05_REPRESENTATIVE_PHONE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON05_REPRESENTATIVE_PHONE]
AS
SELECT 
			''PHONE'' AS RESOURCETYPE,
            ''PRIMARY HOME'' AS TYPE,
            CASE
                WHEN LEN(HOMEPHONE) > 9
                THEN(SUBSTRING(HOMEPHONE, 1, 3)+''-''+SUBSTRING(HOMEPHONE, 4, 3)+''-''+SUBSTRING(HOMEPHONE, 7, 4))
                ELSE ''000-000-0000''
            END AS NUMBER,
		  PATIENTREPRESENTATIVEID
            
     FROM [MODEL].[PATIENTREPRESENTATIVES] WITH (NOLOCK)
     WHERE LEN(HOMEPHONE) > 0--O

	 UNION
     SELECT 
			''PHONE'' AS RESOURCETYPE,
            ''WORK PLACE'' AS TYPE,
            CASE
                WHEN LEN(WORKPHONE) > 9
                THEN(SUBSTRING(WORKPHONE, 1, 3)+''-''+SUBSTRING(WORKPHONE, 4, 3)+''-''+SUBSTRING(WORKPHONE, 7, 4))
                ELSE ''000-000-0000''
            END AS NUMBER,
		  PATIENTREPRESENTATIVEID
            
     FROM [MODEL].[PATIENTREPRESENTATIVES] WITH (NOLOCK)
     WHERE LEN(WORKPHONE) > 0--O
	 AND STATUS>=0
	 UNION
SELECT 
			''PHONE'' AS RESOURCETYPE,
            ''MOBILE CONTACT'' AS TYPE,
            CASE
                WHEN LEN(CELLPHONE) > 9
                THEN(SUBSTRING(CELLPHONE, 1, 3)+''-''+SUBSTRING(CELLPHONE, 4, 3)+''-''+SUBSTRING(CELLPHONE, 7, 4))
                ELSE ''000-000-0000''
            END AS NUMBER,
		  PATIENTREPRESENTATIVEID
            
     FROM [MODEL].[PATIENTREPRESENTATIVES] WITH (NOLOCK)
     WHERE LEN(CELLPHONE) > 0--O
	AND STATUS>=0

' 

Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT')) 
Drop View VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT]

AS

	SELECT 
		'SurgicalPreOperativeStatusEvent' AS RESOURCETYPE, --R
		'EHR' AS IsSource, --R
		PC.APPOINTMENTID AS ReferenceNumber,
		PC.PATIENTID AS PatientAccountNumber, --R
		--CONVERT(VARCHAR(10), AP.APPDATE, 101) as DateofPreOpExam, --R
		(SUBSTRING((CONVERT(VARCHAR(20), AP.APPDATE,101)),5,2)+'/' + SUBSTRING((CONVERT(VARCHAR(20), AP.APPDATE,101)),7,2)+'/' +SUBSTRING((CONVERT(VARCHAR(20), AP.APPDATE,101)),1,4))		AS DateofPreOpExam,
		r.npi as PerformedPhysicianNPI, --R
		RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''), 1, 9)), 9) AS ECTIN, --R
		PC.CLINICALID AS ExternalId,
		PC.APPOINTMENTID AS EncounterId,
		CASE
			WHEN ((select count(*) from dbo.CataractLog)=1) THEN (Select top (1) AppointmentId from dbo.CataractLog where patientid = PC.PATIENTID)
		ELSE
			(Select top (1) AppointmentId from dbo.CataractLog where patientid = PC.PATIENTID AND (AP.Appdate>Appdate OR AP.Appdate=Appdate)  order by Appdate desc)
		END AS EpisodeNumber,
		ptc.transactionid AS ReferralOrderNumber,
		'TRUE' AS IsInHousePhysician,
		r.npi as ECNPI, --R
		R.RESOURCEID AS PerformedPhysicianAccountNumber,
		R.RESOURCENAME AS PerformedPhysicianName,
		PC.AppointmentId

	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN DBO.APPOINTMENTS AP WITH(NOLOCK)
	ON PC.APPOINTMENTID = AP.APPOINTMENTID

	LEFT JOIN DBO.RESOURCES R WITH(NOLOCK)
	ON AP.RESOURCEID1 = R.RESOURCEID

	LEFT JOIN dbo.PracticeTransactionJournal ptc WITH(NOLOCK)
	ON AP.APPOINTMENTID = ptc.transactiontypeid

	WHERE
	PC.FINDINGDETAIL LIKE 'schedule surgery%cataract%'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON12_CLINICALDATARECONCILIATION')) 
Drop View VIEW_ACI_JSON12_CLINICALDATARECONCILIATION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON12_CLINICALDATARECONCILIATION] AS 
SELECT TOP 1
	''Reconcilation'' AS RESOURCETYPE ,--R
	PRPI.ID AS EXTERNALID,--R
	''EHR'' AS ISSOURCE,--R
	 '''' AS REFERENCENUMBER,--R
	PRPI.PATIENTID AS PATIENTACCOUNTNUMBER,--R	
	case when  PRPI.CREATEDDATETIME is null then ''01/01/1900 01:00:00 AM'' else
	(CONVERT(VARCHAR(20), PRPI.CREATEDDATETIME, 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PRPI.CREATEDDATETIME, 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), PRPI.CREATEDDATETIME, 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PRPI.CREATEDDATETIME, 22), 11))
                                                              END)) END AS FILERECEIVEDDATE,
		CASE
		WHEN PRPI.PatientType = ''Transition of Care'' THEN ''TOC''
		WHEN PRPI.PatientType = ''Referral'' THEN ''Referral'' 
		ELSE ''NewPatient'' 
		END as ReconcileFiletype,--R	
	case when  PRPI.CREATEDDATETIME is null then ''01/01/1900 01:00:00 AM'' else
	(CONVERT(VARCHAR(20), PRPI.CREATEDDATETIME, 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PRPI.CREATEDDATETIME, 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), PRPI.CREATEDDATETIME, 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PRPI.CREATEDDATETIME, 22), 11))
                                                              END)) END AS RECONCILEDATE,
	CASE 
		WHEN ((select count(*) from [model].[PatientReconcileMedications] M1 where M1.appointmentid = (select top 1 Ap.appointmentid from dbo.appointments Ap where Ap.patientid = PRPI.patientid and ((cast(Ap.appdate as date) =( cast(PRPI.createddatetime as date)))or(cast(Ap.appdate as date) =( cast(PRPI.createddatetime as date)))) order by ap.appdate asc)))>0 THEN ''True'' 
		ELSE ''False'' 
	END AS MEDICATIONRECONCILED,--O
	
	CASE 
		WHEN ((select count(*) from [model].[PatientReconcileAllergy] A1 where A1.appointmentid = (select top 1 Ap.appointmentid from dbo.appointments Ap where Ap.patientid = PRPI.patientid and ((cast(Ap.appdate as date) =( cast(PRPI.createddatetime as date)))or(cast(Ap.appdate as date) =( cast(PRPI.createddatetime as date)))) order by ap.appdate asc)))>0 THEN ''True'' 
		ELSE ''False'' 
	END AS ALLERGIESRECONCILED,--O
	
	CASE 
		WHEN ((select count(*) from [model].[PatientReconcileProblems] P1 where P1.appointmentid = (select top 1 Ap.appointmentid from dbo.appointments Ap where Ap.patientid = PRPI.patientid and ((cast(Ap.appdate as date) =( cast(PRPI.createddatetime as date)))or(cast(Ap.appdate as date) =( cast(PRPI.createddatetime as date)))) order by ap.appdate asc)))>0 THEN ''True'' 
		ELSE ''False'' 
	END AS PROBLEMSRECONCILED,--O
	
	''True'' AS ISSOCINCORPORATED,--R
	
	ISNULL(r.npi,'''') as ECNPI, --R
	RIGHT(''0000000000''+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''''), 1, 9)), 9) AS TIN
FROM 
Model.PatientReconcilePracticeInformation PRPI WITH(NOLOCK)
LEFT JOIN Model.Patients P WITH(NOLOCK) ON PRPI.PatientId = P.Id
LEFT JOIN DBO.RESOURCES R WITH(NOLOCK) ON P.DEFAULTUSERID = R.RESOURCEID
ORDER BY PRPI.CREATEDDATETIME DESC
	
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE')) 
Drop View VIEW_ACI_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  


EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE]
AS
     SELECT ''SecureMessage'' AS RESOURCETYPE, --R
            ''EHR'' AS ISSOURCE, --R
			(select CAST(A.Id AS VARCHAR(50)) from MVE.ACI_PP_SecureMessageLogs A where CAST(A.LogId AS VARCHAR(50)) = CAST(pqr.LogId AS VARCHAR(50)) ) as ReferenceNumber,
			PatientId as PatientAccountNumber, --R
			pqr.LogId
	FROM [MVE].[PP_SecureMessagesLogs] pqr WITH(NOLOCK)
	where pqr.MessageSubject <> ''A new version of your health summary is available''
--	where actiontypelookupid = 19
' 


Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Diagnosis')) 
Drop View VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Diagnosis 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Diagnosis

AS
	SELECT 
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		LEFT(PC.FINDINGDETAIL,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',SUBSTRING(PC.FINDINGDETAIL, CHARINDEX('.',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',PC.FINDINGDETAIL)))) AS Code,
		PDT.REVIEWSYSTEMLINGO AS Description,
		'2.16.840.1.113883.6.103' AS CodeSystem,
		'ICD9CM' AS CodeSystemName
	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN DM.PRIMARYDIAGNOSISTABLE PDT WITH(NOLOCK)
	ON PC.FINDINGDETAIL = PDT.DIAGNOSISNEXTLEVEL
	WHERE PC.CLINICALTYPE IN ('B','K')
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE_MESSAGESENTBY')) 
Drop View VIEW_ACI_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE_MESSAGESENTBY 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE_MESSAGESENTBY]
AS
	SELECT
		cast(SML.LogID AS varchar(100)) as LogId,
		ISNULL(r.npi,''0000000000'') as ECNPI, --R
		ISNULL(R.RESOURCEID,''00'') AS ECAccount, --R
		RIGHT(''0000000000''+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''''), 1, 9)), 9) AS ECTIN,		
		case when  SML.createddatetime is null then ''01/01/1900 01:00:00 AM'' else
		(CONVERT(VARCHAR(20), SML.createddatetime, 101)+'' ''+(CASE
                                                                  WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), SML.createddatetime, 22), 11))) = 10
                                                                  THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), SML.createddatetime, 22), 11))
                                                                  ELSE LTRIM(RIGHT(CONVERT(CHAR(20), SML.createddatetime, 22), 11))
                                                              END)) END AS SentDate,		
		''NEW'' AS MESSAGETYPE, --R
		CASE
		WHEN SML.ISSENT2REPERSENATIVE = 0 THEN ''Patient'' 
		ELSE ''Representative'' 
		END AS SENTTO, --R

		CASE
		WHEN SML.ISSENT2REPERSENATIVE = 1 THEN CAST(SML.REPERSENTATIVEID  as VARCHAR(50))
		ELSE ''''
		END AS RepresentativeExternalId --R
	
	FROM  [MVE].[PP_SECUREMESSAGESLOGS]  SML (nolock)
		Left Join MVE.PP_PortalQueueResponse PP on (Cast(PP.ExternalId as nvarchar(50)) =  Cast(SML.RecipientId as nvarchar(50))
		OR Cast(PP.PatientNo as nvarchar(50)) =  Cast(SML.RecipientId as nvarchar(50)))
		Left join dbo.Resources R on (Cast(R.ResourceId as nvarchar(50)) = Cast(PP.PatientNo as nvarchar(50)) 
		OR Cast(R.ResourceId as nvarchar(50)) =  Cast(PP.ExternalId as nvarchar(50)))
		where PP.ActionTypeLookupId = 12
'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_ExamField')) 
Drop View VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_ExamField 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_ExamField]

AS
	SELECT DISTINCT
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		CASE 
			WHEN Q.PROCORTEST = 'T' THEN 'TEST'
			WHEN Q.PROCORTEST = 'P' THEN 'PROCEDURE'
			ELSE 'NOT DEFINED'
		END  AS Type, --R
		CASE
			WHEN Q.CONTROLNAME IS NULL THEN 'NOT DEFINED'
			ELSE  Q.ControlName
		END AS Code,
		
		CASE
			WHEN Q.CONTROLNAME IS NULL THEN 'NOT DEFINED'
			ELSE Q.QUESTION
		END AS Description, pc.symptom,
		'CPT' AS CodeSystemName,
		'2.16.840.1.113883.6.12' AS CodeSystem

	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN DF.QUESTIONS Q WITH(NOLOCK)
	ON RIGHT(PC.SYMPTOM, LEN(PC.SYMPTOM)-1) = Q.QUESTION and Q.controlname <> ''
	WHERE PC.CLINICALTYPE = 'F' and Q.PROCORTEST in ('T','P')  
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON15_CLAIMSUBMISSIONEVENT')) 
Drop View VIEW_ACI_JSON15_CLAIMSUBMISSIONEVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON15_CLAIMSUBMISSIONEVENT]
AS

	SELECT
		Inv.Id as ExternalId,
		'PMSCLAIMS' AS RESOURCETYPE, --R 
		'PMS' IsSource, --R
		Inv.Id AS ReferenceNumber, --R
		CONVERT(VARCHAR, Ap.PatientId) AS PatientAccountNumber, --R
		CONVERT(VARCHAR, Ap.EncounterID) AS EncounterID, --R
		CONVERT(VARCHAR(10), Ec.StartDateTime, 101) AS EncounterDate, --R
		Re.NPI AS ECNPI, --R
		RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(ISNULL(RE.RESOURCETAXID, ''), 1, 9)), 9) AS ECTIN ,--R
		Inv.Id AS INVOICEID
	FROM model.Invoices Inv WITH (NOLOCK) 
	JOIN dbo.Appointments Ap WITH (NOLOCK) ON AP.AppointmentID=inv.EncounterId
	LEFT JOIN model.Encounters Ec WITH (NOLOCK) ON Ec.Id = Ap.EncounterID
	LEFT JOIN dbo.Resources Re WITH (NOLOCK) ON Re.ResourceId = Ap.ResourceId1	
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Medication')) 
Drop View VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Medication 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Medication]

AS
	SELECT DISTINCT
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		ISNULL((Select top (1) RxCUI from [fdb].[RXCUI2MEDID] where medid = nc.medid),'00000')AS Code, 

		CASE WHEN pc.FindingDetail like 'RX-8/%-2/%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') 
			ELSE '' 
		END  AS Description, 
		'RxNorm' AS CodeSystem,
		'2.16.840.1.113883.6.88' AS CodeSystemName
	FROM
	dbo.patientclinical pc WITH(NOLOCK)   left join fdb.tblcompositedrug nc WITH(NOLOCK) on Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') =	nc.MED_ROUTED_DF_MED_ID_DESC and REPLACE(SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX('-2/', pc.FindingDetail) + 4) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(',pc.FindingDetail, CHARINDEX('-2/', pc.FindingDetail) + 4)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX('-2/', pc.FindingDetail) + 4) - 2),' ','') = nc.MED_STRENGTH
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON15_CLAIMSUBMISSIONEVENT_BILLINGCODE')) 
Drop View VIEW_ACI_JSON15_CLAIMSUBMISSIONEVENT_BILLINGCODE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON15_CLAIMSUBMISSIONEVENT_BILLINGCODE]
AS

	SELECT
		CONVERT(VARCHAR, Inv.Id) AS ExternalId, --R
		Ecs.Code AS BillingCodeValue, --R
		Ecs.Description AS BillingCodeDescription,  --R
		'2.16.840.1.113883.6.12' AS BillingCodeSystem, --R
		'CPT-4' AS BillingCodeName, --R
		Inv.Id AS INVOICEID
	FROM
	model.Invoices Inv WITH (NOLOCK)
	LEFT JOIN model.BillingServices Bs WITH (NOLOCK) ON Bs.InvoiceId = Inv.Id
	LEFT JOIN model.EncounterServices Ecs WITH (NOLOCK) ON Ecs.Id = Bs.EncounterServiceId

UNION

	SELECT
		CONVERT(VARCHAR, Inv.Id) AS ExternalId, --R
		Ecs.Code AS BillingCodeValue, --R
		Ecs.Description AS BillingCodeDescription,  --R
		'2.16.840.1.113883.6.12' AS BillingCodeSystem, --R
		'CPT-4' AS BillingCodeName ,--R
		Inv.Id AS INVOICEID
	FROM
	model.Invoices Inv WITH (NOLOCK)
	LEFT JOIN model.BillingServices Bs WITH (NOLOCK) ON Bs.InvoiceId = Inv.Id
	LEFT JOIN model.EncounterServices Ecs WITH (NOLOCK) ON Ecs.Id = Bs.EncounterServiceId
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure')) 
Drop View VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure

AS

	SELECT 
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		SUBSTRING(PC.FINDINGDETAIL, CHARINDEX(':',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX(':',PC.FINDINGDETAIL)+1) AS Code,
		ES.DESCRIPTION AS Description,
		'CPT' AS CodeSystemName,
		'2.16.840.1.113883.6.12' AS CodeSystem,
				BS.Id As Id
	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN MODEL.ENCOUNTERSERVICES ES WITH(NOLOCK)
	ON SUBSTRING(PC.FINDINGDETAIL, CHARINDEX(':',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX(':',PC.FINDINGDETAIL)+1) = ES.CODE
	LEFT JOIN Model.Invoices Inv
	ON PC.AppointmentId= Inv.EncounterId
	Left Join model.BillingServices BS ON
	BS.EncounterServiceId =ES.ID and Inv.Id = BS.InvoiceId
	WHERE PC.CLINICALTYPE = 'P' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON16_MEDICATIONSORDER')) 
Drop View VIEW_ACI_JSON16_MEDICATIONSORDER 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON16_MEDICATIONSORDER]
AS

	SELECT
		''MedicationOrder'' AS RESOURCETYPE, --R
		''EHR'' AS IsSource, --R
		CAST(pc.MedicationRcopiaID AS VARCHAR(20)) as ReferenceNumber, --R
		CAST(pc.Patient_ExtID AS NVARCHAR(20)) as PatientAccountNumber, --R
		pc.PrescriptionRcopiaID,
		PC.AppointmentId,
		PC.Patient_ExtID,
		eRx_Medicine_Response_Key
	FROM
	drfirst.eRx_Medicine_Response PC WITH(NOLOCK)
	WHERE  pc.PrescriptionRcopiaID  is not null and rxnormid is not null;
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Reason')) 
Drop View VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Reason 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Reason
AS
	SELECT TOP 1 
	'Reason' AS RESOURCETYPE,
'EncounterDiagnosis' AS Type,
LEFT(PC.FINDINGDETAIL,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',SUBSTRING(PC.FINDINGDETAIL, CHARINDEX('.',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',PC.FINDINGDETAIL)))) AS Code,
		Q.REVIEWSYSTEMLINGO AS Description,
	'2.16.840.1.113883.6.103' AS CodeSystem,
		'ICD9CM' AS CodeSystemName,
 PC.CLINICALID As ID,
 PC.AppointmentId AS AppointmentId,
 PC.PatientId As PatientId

	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN DM.PRIMARYDIAGNOSISTABLE Q WITH(NOLOCK)
	ON PC.FINDINGDETAIL = Q.DIAGNOSISNEXTLEVEL
	WHERE PC.CLINICALTYPE IN ('B','K') 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION')) 
Drop View VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION]
AS



       SELECT DISTINCT
		''Medication'' AS RESOURCETYPE, 	
		pc.eRx_Medicine_Response_Key as ExternalId, --R
		'''' as ECNPI, --R
		'''' as ECTIN, --R
		PC.BrandName AS DrugName, --R
		pc.RxnormID AS DRUGCODE,
		''2.16.840.1.113883.6.88'' AS DRUGCODESYSTEM, --R
        ''RxNorm'' AS DRUGCODESYSTEMNAME, --R
		CASE (select top 1 b.focus from fdb.tblCompositeDrug a join dbo.drug b on b.DisplayName=a.MED_routed_df_med_id_desc where a.medid = pc.FirstDataBankMedID)
		WHEN ''1'' THEN ''Ocular''
		ELSE ''Systemic'' end as MedicationType, --R
		--CASE SUBSTRING(pc.findingdetail,(CHARINDEX ( ''-4/'',pc.findingdetail )+4) ,1) 
		--	WHEN '')'' THEN ''NEW''
		--	WHEN ''0'' THEN ''NEW''
		--	ELSE ''Refill''
		--END 
		CASE (select count(*) from drfirst.eRx_Medicine_Response a where a.PrescriptionRcopiaID = pc.PrescriptionRcopiaID)
		WHEN ''1'' THEN ''NEW''
		ELSE ''Refill''
		END as PrescriptionType, --R
		pc.Action+'' ''+pc.Dose+'' ''+pc.DoseUnit+'' ''+pc.Route+'' ''+pc.Frequency AS Directions,--R
		pc.OtherNotes+'' ''+pc.PatientNotes AS AdditionalInstructions,--R

		pc.Strength as Strength,
		''Medication'' AS SUBSTITUTION, --R
--------------------------------------------------------------------------------------------------------

		(CASE WHEN DATEPART(HOUR, (CONVERT(VARCHAR(20), pc.StartDate, 101))) > 12 
		THEN
		(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''PM''
		ELSE
		(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''AM''

		END) AS StartDate, --R
------------------------------------------------------------------------------------------------------
		(CASE WHEN DATEPART(HOUR, (CONVERT(VARCHAR(20), pc.StartDate, 101))) > 12 
		THEN
		(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''PM''
		ELSE
		(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''AM''

		END) AS EffectiveDate, --R
------------------------------------------------------------------------------------------------------
		--(CONVERT(VARCHAR(20), pc.StartDate, 101)) +''AM'' AS EffectiveDate, --R
		
		ISNULL(pc.Duration,''1'') +'' Days'' as DaySupply,--R
		ISNULL(pc.Quantity,''1'')	as DosageQuantityValue,--R  NEED TO CHECK FOR VALUE AND UNITS
		''TRUE'' AS IsPrescribed,
--------------------------------------------------------------------------------------------------
		--CASE WHEN (PR.PrescriptionCompletionAction  =''printed,signed'')
		--THEN '''' ELSE 
		--(CASE WHEN DATEPART(HOUR, (CONVERT(VARCHAR(20), pc.StartDate, 101))) > 12 
		--THEN
		--(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''PM''
		--ELSE
		--(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''AM''
		--END)
		
		--END  AS EprescribedDate,
--------------------------------------------------------------------------------
		CASE WHEN (P.PrescriptionCompletionAction =''printed,signed'')
		THEN '''' ELSE 

		(CASE WHEN DATEPART(HOUR, (CONVERT(VARCHAR(20), pc.StartDate, 101))) > 12 
		THEN
		(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''PM''
		ELSE
		(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''AM''

		END)
		
		END 
		--CASE WHEN (P.PrescriptionCompletionAction  =''printed,signed'')
		--THEN '''' 
		--ELSE  
		--(CASE WHEN DATEPART(HOUR, (CONVERT(VARCHAR(20), pc.StartDate, 101))) > 12 
		--THEN
		--(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''PM''
		--ELSE
		--(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''AM''

		--END)
		AS EprescribedDate,
		--(CONVERT(VARCHAR(20), pc.StartDate, 101)) +''AM'' AS EprescribedDate,	
----------------------------------------------------------------------------------------------
		''True'' AS ISQUERIEDFORDRUGFORMULARY, --R
		''True'' AS  ISCPOE, --R
		''False'' AS ISHIGHRISKMEDICATION, --R
		CASE
		WHEN PC.DrugSchedule in(''2'',''3'',''4'',''1'',''5'',''6'',''7'',''8'') THEN ''TRUE''
		ELSE ''False''
		END  AS ISCONTROLSUBSTANCE, --R
		ISNULL(pc.pharmacyname,''NOT AVAILABLE'') AS PHARMACYNAME,
		CASE ISNULL(pc.StopDate,'''')
		WHEN '''' THEN ''''
		ELSE (CONVERT(VARCHAR(20), pc.StopDate, 101)) +''AM''   --C	
		END AS DateEnd, 
		--CASE ISNULL(pc.StopDate,'')
		--WHEN '' THEN ''
		----ELSE (CASE WHEN DATEPART(HOUR, (CONVERT(VARCHAR(20), pc.StopDate, 101))) > 12 
		----THEN
		----(CONVERT(VARCHAR(20), pc.StopDate, 101))  + ''PM''
		----ELSE
		----(CONVERT(VARCHAR(20), pc.StopDate, 101))  + ''AM''
		----END)   --C	
		--END AS DateEnd, --O
		--CASE ISNULL(pc.StopDate,'')
		--WHEN '' THEN ''
		--ELSE (CASE WHEN DATEPART(HOUR, (CONVERT(VARCHAR(20), pc.StopDate, 101))) > 12 
		--THEN
		--(CONVERT(VARCHAR(20), pc.StopDate, 101))  + ''PM''
		--ELSE
		--(CONVERT(VARCHAR(20), pc.StopDate, 101))  + ''AM''
		--END)   --C	
		--END AS DateEnd,  --C	
		--right(pc.imagedescriptor,10)+'' am'' 
		--AS ReconciledDate, --C----------------		
		
		CASE
			WHEN ISNULL(PC.StopDate,'''') ='''' THEN ''ACTIVE''
			ELSE ''INACTIVE''
		END AS Status, --R	
		pc.PrescriptionRcopiaID,
		PC.AppointmentId,
		PC.Patient_ExtID,
		eRx_Medicine_Response_Key

		from
		drfirst.eRx_Medicine_Response PC WITH (NOLOCK) inner join DRFIRST
		.eRx_Prescriptions_Response P on
		PC.Patient_ExtID = P.PatientID
	WHERE  pc.PrescriptionRcopiaID  is not null and rxnormid is not null

	'

Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_PHARMACYADDRESS')) 
Drop View VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_PHARMACYADDRESS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_PHARMACYADDRESS]
AS
     SELECT 
			''PharmacyAddress'' AS RESOURCETYPE,
			''physical visit address'' AS TYPE, --C
            ISNULL(PharmacyName,''NOT AVAILABLE'') AS ADDRESSLINE1, --C
            '''' AS ADDRESSLINE2, --C
            --pc.PharmacyCity
			ISNULL(pc.PharmacyState,''NOT AVAILABLE'') AS CITY, --C
            ISNULL(pc.PharmacyState,''NOT AVAILABLE'') AS STATE, --C
            ISNULL(pc.PharmacyZip,''NOT AVAILABLE'') AS ZIP, --C
            ''NA'' AS COUNTY, --C
			pc.PrescriptionRcopiaID,
		PC.AppointmentId,
		PC.Patient_ExtID,
		eRx_Medicine_Response_Key
	FROM
	drfirst.eRx_Medicine_Response PC WITH(NOLOCK)
	WHERE  pc.PrescriptionRcopiaID  is not null and rxnormid is not null;
' 

Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_PHARMACYPHONE')) 
Drop View VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_PHARMACYPHONE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_PHARMACYPHONE]
AS
     SELECT
		''PharmacyPhone'' AS RESOURCETYPE, 
		''Work'' AS TYPE, --C
        ISNULL(REPLACE(REPLACE(REPLACE(pc.PharmacyPhone,'' '',''''),''('',''''),'')'',''-''),''000-000-0000'') AS NUMBER, --C
       pc.PrescriptionRcopiaID,
		PC.AppointmentId,
		PC.Patient_ExtID,
		eRx_Medicine_Response_Key
	FROM
	drfirst.eRx_Medicine_Response PC WITH(NOLOCK)
	WHERE  pc.PrescriptionRcopiaID  is not null and rxnormid is not null;
' 

 Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_ROUTE')) 
Drop View VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_ROUTE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_ROUTE]
AS
 --    SELECT DISTINCT
	--	''Route'' AS RESOURCETYPE, 
	--	CASE SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-3/'', pc.FindingDetail) + 4)+1)) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-3/'', pc.FindingDetail) + 3)+1)+1)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-3/'', pc.FindingDetail) + 3)+1)) - 1)
	--		WHEN ''INSTILL IN EYES'' THEN ''C38287''
	--		WHEN ''APPLY TO LIDS'' THEN ''C38287''
	--		WHEN ''PO'' THEN ''C38288''
	--		WHEN ''INJECTABLE'' THEN ''C38192''
	--		ELSE ''''
	--	END AS Code, --R
	--	CASE SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-3/'', pc.FindingDetail) + 4)+1)) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-3/'', pc.FindingDetail) + 3)+1)+1)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''-3/'', pc.FindingDetail) + 3)+1)) - 1)
	--		WHEN ''INSTILL IN EYES'' THEN ''INSTILL IN EYES''
	--		WHEN ''APPLY TO LIDS'' THEN ''APPLY TO LIDS''
	--		WHEN ''PO'' THEN ''BY MOUTH''
	--		WHEN ''INJECTABLE'' THEN ''INJECTABLE''
	--		ELSE ''''
	--	END AS Description, --R
 --       ''2.16.840.1.113883.3.26.1.1'' AS CODESYSTEM, --R
 --       ''NCI Thesures'' AS CODESYSTEMNAME, --R
 --       PC.ClinicalId,
	--	PC.AppointmentId,
	--	PC.PatientId
	--from 
	--dbo.patientclinical pc WITH(NOLOCK) left join dbo.appointments ap WITH(NOLOCK)	on pc.appointmentid = ap.appointmentid
	--left join  dbo.resources r WITH(NOLOCK)	on ap.resourceid1 = r.resourceid
	--left join dbo.drug dd WITH(NOLOCK) on Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex(''-2/'', pc.findingdetail) - 6),''&'',''and''), ''+'',''Plus'') =  dd.displayname
	--left join fdb.tblcompositedrug nc WITH(NOLOCK) on Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex(''-2/'', pc.findingdetail) - 6),''&'',''and''), ''+'',''Plus'') = nc.MED_ROUTED_DF_MED_ID_DESC
	--LEFT JOIN DBO.PATIENTNOTES PN  WITH(NOLOCK) ON PC.APPOINTMENTID = PN.APPOINTMENTID AND PN.NOTETYPE= ''R'' AND Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex(''-2/'', pc.findingdetail) - 6),''&'',''and''), ''+'',''Plus'') = PN.NOTESYSTEM
	--where pc.clinicaltype = ''A'' and findingdetail like ''RX-8%''
	----AND PC.PATIENTID = 46896
	--AND PC.ImageDescriptor NOT LIKE ''C%''and pc.Status <> ''D''




	------------------------------------------------------------------------------
	  SELECT DISTINCT
		''Route'' AS RESOURCETYPE, 
		CASE PC.Action
			WHEN ''INSTILL'' THEN ''C38287''
			WHEN ''APPLY'' THEN ''C38287''
			WHEN ''Take'' THEN ''C38288''
			WHEN ''INJECT'' THEN ''C38192''
			ELSE ''''
		END AS Code, --R
		CASE PC.action
			WHEN ''INSTILL'' THEN ''INSTILL IN EYES''
			WHEN ''APPLY'' THEN ''APPLY TO LIDS''
			WHEN ''Take'' THEN ''BY MOUTH''
			WHEN ''INJECT'' THEN ''INJECTABLE''
			ELSE ''''
		END AS Description, --R
        ''2.16.840.1.113883.3.26.1.1'' AS CODESYSTEM, --R
        ''NCI Thesures'' AS CODESYSTEMNAME, --R
		pc.PrescriptionRcopiaID,
		PC.AppointmentId,
		PC.Patient_ExtID,
		eRx_Medicine_Response_Key

		from
		drfirst.eRx_Medicine_Response PC WITH(NOLOCK)
	WHERE  pc.PrescriptionRcopiaID  is not null and rxnormid is not null
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON17_SENTSUMMARYOFCAREEVENT')) 
Drop View VIEW_ACI_JSON17_SENTSUMMARYOFCAREEVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON17_SENTSUMMARYOFCAREEVENT]
AS

select 
		''SentSOCEvent'' AS RESOURCETYPE, --R
		''EHR'' AS IsSource, --R
		d.AciDepKey as ReferenceNumber, --R
		d.Patientkey as PatientAccountNumber, --R
		(CONVERT(VARCHAR(20), d.CreatedDate, 101)+'' ''+(CASE
                                                                  WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), d.CreatedDate, 22), 11))) = 10
                                                                  THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20),d.CreatedDate, 22), 11))
                                                                  ELSE LTRIM(RIGHT(CONVERT(CHAR(20),d.CreatedDate, 22), 11))
                                                              END)) AS Date,
		ISNULL(r.npi,''0000000000'') as ECNPI, --R
		RIGHT(''0000000000''+CONVERT(VARCHAR, SUBSTRING(isnull(R.RESOURCETAXID, ''''), 1, 9)), 9) AS ECTIN, --O

		CASE
			WHEN d.ClinicalDocumentType =''R'' THEN ''Referral''
			ELSE ''TOC''
		END AS SOCSentTo, --R

		d.AciDepKey as OrderNumber, --R
		d.AciDepKey


		from dbo.ACI_DEP d WITH(NOLOCK) join dbo.Resources r WITH(NOLOCK) on r.ResourceId = d.ProviderKey
' 

Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON18_PATIENTSPECIFICEDUCATIONEVENT')) 
Drop View VIEW_ACI_JSON18_PATIENTSPECIFICEDUCATIONEVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'

CREATE VIEW [dbo].[VIEW_ACI_JSON18_PATIENTSPECIFICEDUCATIONEVENT]
AS
   --  SELECT ''PatientSpecificEducationEvent'' AS RESOURCETYPE,
   --         ''EHR'' AS ISSOURCE, --R
   --         PatientId AS REFERENCENUMBER, --R
   --         PC.PATIENTID AS PATIENTACCOUNTNUMBER, --R
			--PC.APPOINTMENTID,
			--PC.CLINICALID

   --  FROM DBO.PATIENTCLINICAL PC WITH(NOLOCK)
   --  WHERE PC.FINDINGDETAIL LIKE ''WRITTEN INSTRUCTIONS%'' 


        SELECT ''PatientSpecificEducationEvent'' AS RESOURCETYPE,
            ''EHR'' AS ISSOURCE, --R
            Id AS REFERENCENUMBER, --R
            PATIENTID AS PATIENTACCOUNTNUMBER, --R
			APPOINTMENTID,
			ID

     FROM [MVE].[PP_PatientEducationMaterial] WITH(NOLOCK)

' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON18_PATIENTSPECIFICEDUCATIONEVENT_EDUCATIONMATERIAL')) 
Drop View VIEW_ACI_JSON18_PATIENTSPECIFICEDUCATIONEVENT_EDUCATIONMATERIAL 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON18_PATIENTSPECIFICEDUCATIONEVENT_EDUCATIONMATERIAL]
AS
      SELECT PC.ID AS EXTERNALID, --R
            ISNULL(r.npi,'0000000000') as ECNPI, --R
			RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''), 1, 9)), 9) AS TIN, 
			CASE 
			WHEN PC.CodeType = 'Medication' THEN 'RxNORM'
			WHEN PC.CodeType = 'Problem' and pc.ExternalId  LIKE '[a-Z]%' THEN 'ICD10CM'			
			WHEN PC.CodeType = 'Problem' and len(pc.ExternalId)<7 THEN 'ICD9CM'
			ELSE 'SNOMED'
			END AS CODETYPE, --R
			CASE
			WHEN  PC.CodeType = 'Problem' THEN pc.ExternalId
			else
			PC.ExternalId END AS CODE, --R
			(CONVERT(VARCHAR(20), pc.CreatedDateUTC, 101)+' '+(CASE
                                                                  WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), pc.CreatedDateUTC, 22), 11))) = 10
                                                                  THEN '0'+LTRIM(RIGHT(CONVERT(CHAR(20), pc.CreatedDateUTC, 22), 11))
                                                                  ELSE LTRIM(RIGHT(CONVERT(CHAR(20), pc.CreatedDateUTC, 22), 11))
                                                              END)) AS DatePrinted,
			(CONVERT(VARCHAR(20), pc.CreatedDateUTC, 101)+' '+(CASE
                                                                  WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), pc.CreatedDateUTC, 22), 11))) = 10
                                                                  THEN '0'+LTRIM(RIGHT(CONVERT(CHAR(20), pc.CreatedDateUTC, 22), 11))
                                                                  ELSE LTRIM(RIGHT(CONVERT(CHAR(20), pc.CreatedDateUTC, 22), 11))
                                                              END)) AS DateSentToPortal,
			PC.PATIENTID,
			PC.APPOINTMENTID,
			PC.ID


	FROM [MVE].[PP_PatientEducationMaterial] PC WITH(NOLOCK) LEFT JOIN DBO.RESOURCES R WITH(NOLOCK)
	ON R.RESOURCEID = PC.RESOURCEID

Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT')) 
Drop View VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
--select * from model.PatientReconcilePracticeInformation
CREATE VIEW [dbo].[VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT]
AS
SELECT

	''ReferralTOCNewPatientEvent'' AS RESOURCETYPE,--R
	''EHR'' AS ISSOURCE,--R
	PRPI.Id AS REFERENCENUMBER,--R
	PRPI.PatientId AS PATIENTACCOUNTNUMBER,--R
	--(CONVERT(VARCHAR(20), ESM.CREATEDDATETIME,101)) AS DATE	,--R
	case when  PRPI.CREATEDDATETIME is null then ''01/01/1900 01:00:00 AM'' else
	(CONVERT(VARCHAR(20), PRPI.CREATEDDATETIME, 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PRPI.CREATEDDATETIME, 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), PRPI.CREATEDDATETIME, 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PRPI.CREATEDDATETIME, 22), 11))
                                                              END)) END AS Date,
	ISNULL(r.npi,''0000000000'') as ECNPI, --R
	RIGHT(''0000000000''+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''''), 1, 9)), 9) AS ECTIN,
	PRPI.PatientId AS EXTERNALID,--R
	CASE
		WHEN PRPI.PatientType = ''Transition of Care'' THEN ''TOC''
		WHEN PRPI.PatientType = ''Referral'' THEN ''Referral'' 
		ELSE ''NewPatient'' 
		END
	 as Eventtype,--R
	''True'' AS IsSocReceived,--R
	''Not AVAILABLE'' AS DESCRIPTION,
	 PRPI.Id,
	PRPI.ExternalSystemMessagesId,
	PRPI.PatientId

FROM

-- MODEL.EXTERNALSYSTEMMESSAGES ESM  WITH(NOLOCK) LEFT JOIN Model.PatientReconcilePracticeInformation PRPI WITH(NOLOCK) ON CAST(ESM.Id AS VARCHAR(50)) = CAST(PRPI.ExternalSystemMessagesId AS VARCHAR(50))
Model.PatientReconcilePracticeInformation PRPI WITH(NOLOCK)
LEFT JOIN Model.Patients P WITH(NOLOCK) ON PRPI.PatientId = P.Id
LEFT JOIN DBO.RESOURCES R WITH(NOLOCK) ON P.DEFAULTUSERID = R.RESOURCEID
--WHERE EXTERNALSYSTEMEXTERNALSYSTEMMESSAGETYPEID = 6 
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_ATTRIBUTE')) 
Drop View VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_ATTRIBUTE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_ATTRIBUTE]
AS
	SELECT 
		''Attribute'' AS RESOURCETYPE,
		''00000'' AS ExternalId,
		''NOT AVAILABLE'' AS AttributeType,
		''NOT AVAILABLE'' AS AttributeSubType,
		''01/01/1900 12:00:00 AM'' AS Date,
		''NOT AVAILABLE'' AS AttributeName,
		PRPI.Id,
		PRPI.ExternalSystemMessagesId,
		PRPI.PatientId
	FROM
	model.PatientReconcilePracticeInformation PRPI WITH(NOLOCK)
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE')) 
Drop View VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE]
AS
	SELECT
		''OutsidePractice'' AS RESOURCETYPE,
		CASE 
		WHEN PRPI.Sender_PracticeName = '''' THEN ''NA''
		ELSE ISNULL(PRPI.Sender_PracticeName,''NA'')
		END  AS PracticeName,
		ISNULL(PRPI.Id,''00000'') AS AccountNumber,
		ISNULL(PRPI.Sender_LastName,''NA'') AS FamilyName,
		ISNULL(PRPI.Sender_FirstName,''NA'') AS GivenName,
		''NOT AVAILABLE'' AS SecondorFutherGivenNames,
		''01/01/1900''AS DateOfBirth,
		PRPI.Id,
		PRPI.ExternalSystemMessagesId,
		PRPI.PatientId
	FROM
	model.PatientReconcilePracticeInformation PRPI WITH(NOLOCK)

' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE_ADDRESS')) 
Drop View VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE_ADDRESS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE_ADDRESS]
AS
		SELECT
		''Address'' AS RESOURCETYPE,
		CASE
		WHEN PRPI.ADDRESSLINE1 = '''' THEN ''NOT AVAILAVLE''
		ELSE ISNULL(PRPI.ADDRESSLINE1,'' NOT AVAILABLE'')
		END AS AddressLine1,
		'''' AS AddressLine2,
		PRPI.CITY AS City,
		PRPI.STATE AS State,
		CASE
		WHEN PRPI.ZIP = '''' THEN ''NA''
		ELSE ISNULL(PRPI.ZIP,''00000'')
		END AS Zip,
		''NA'' AS County,
		''NA'' AS CountyCode,
		 PRPI.Id,
		PRPI.ExternalSystemMessagesId,
		PRPI.PatientId

	FROM
	model.PatientReconcilePracticeInformation PRPI WITH(NOLOCK)
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE_PHONE')) 
Drop View VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE_PHONE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE_PHONE]
AS
SELECT
		''PHONE'' AS RESOURCETYPE,
		''WORK'' AS TYPE ,
		CASE
			WHEN LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PRPI.PHONENUMBER,'' '',''''),'':'',''''),''-'',''''),''X'',''''),''('',''''),'')'',''''),''+1'',''''))>10
			THEN CAST((SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PRPI.PHONENUMBER,'' '',''''),'':'',''''),''-'',''''),''X'',''''),''('',''''),'')'',''''),''+1'',''''),1,3) + ''-'' + SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PRPI.PHONENUMBER,'' '',''''),'':'',''''),''-'',''''),''X'',''''),''('',''''),'')'',''''),''+1'',''''),4,3)+''-''+SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PRPI.PHONENUMBER,'' '',''''),'':'',''''),''-'',''''),''X'',''''),''('',''''),'')'',''''),''+1'',''''),7,4)+''X''+SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PRPI.PHONENUMBER,'' '',''''),'':'',''''),''-'',''''),''X'',''''),''('',''''),'')'',''''),''+1'',''''),11,LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PRPI.PHONENUMBER,'' '',''''),'':'',''''),''-'',''''),''X'',''''),''('',''''),'')'',''''),''+1'',''''))-10)) AS NVARCHAR(20))
			ELSE CAST((SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PRPI.PHONENUMBER,'' '',''''),'':'',''''),''-'',''''),''X'',''''),''('',''''),'')'',''''),''+1'',''''),1,3) + ''-'' + SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PRPI.PHONENUMBER,'' '',''''),'':'',''''),''-'',''''),''X'',''''),''('',''''),'')'',''''),''+1'',''''),4,3)+''-''+SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PRPI.PHONENUMBER,'' '',''''),'':'',''''),''-'',''''),''X'',''''),''('',''''),'')'',''''),''+1'',''''),7,4)) AS NVARCHAR(20))
		END AS NUMBER,
		PRPI.Id,
		PRPI.ExternalSystemMessagesId,
		PRPI.PatientId

		FROM
		model.PatientReconcilePracticeInformation PRPI WITH(NOLOCK)
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT')) 
Drop View VIEW_ACI_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT]
AS

	select DISTINCT
		
		''PHIPOSTEDEVENT'' AS RESOURCETYPE, --R
		''EHR'' [IsSource], --R
		CAST(APPOINTMENTID AS VARCHAR(15)) as ReferenceNumber,
		PatientID as PatientAccountNumber, --R
		
		(CONVERT(VARCHAR(20), GETDATE(), 101)+'' ''+(CASE
                                                                  WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), GETDATE(), 22), 11))) = 10
                                                                  THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), GETDATE(), 22), 11))
                                                                  ELSE LTRIM(RIGHT(CONVERT(CHAR(20), GETDATE(), 22), 11))
                                                              END))  AS POSTEDDATE,		
		APPOINTMENTID  
	from dbo.appointments WITH(NOLOCK)
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'MVE.ACI_PP_SecureMessageLogs')) 
Drop View MVE.ACI_PP_SecureMessageLogs 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
Create View MVE.ACI_PP_SecureMessageLogs
As
SELECT 
ROW_NUMBER() OVER (order by CreatedDateTime ASC)AS Id
,LogId
,RecipientId
,PatientId
,IsSent2Repersenative
,RepersentativeId
,MessageSubject
,MessageSentTime
,CreatedDateTime
,IsResponseSent
FROM [MVE].[PP_SecureMessagesLogs] 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON21_CARECOORDINATIONORDER')) 
Drop View VIEW_ACI_JSON21_CARECOORDINATIONORDER 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON21_CARECOORDINATIONORDER]

AS

	--select 
	--	''CARECOORDINATIONORDER'' AS RESOURCETYPE, --R
	--	''EHR''[Source], --R
	--	'''' as ReferenceNumber, --R
	--	ptc.transactionid as ExternalId, --R
	--	ptc.transactionremark as Description, --R
	--	ap.patientid as PatientAccountNumber, --R
	--	--CONVERT(VARCHAR(10), ptc.transactiondate, 101) as Date, --R
	--	(SUBSTRING((CONVERT(VARCHAR(20), ptc.transactiondate,101)),5,2)+''/'' + SUBSTRING((CONVERT(VARCHAR(20),ptc.transactiondate,101)),7,2)+''/'' +SUBSTRING((CONVERT(VARCHAR(20), ptc.transactiondate,101)),1,4) + '' 12:00:00 AM'') AS Date,
	--	''Referral''[OrderType], --R
	--	r.npi as OrderingECNPI, --R
	--	RIGHT(''0000000000''+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''''), 1, 9)), 9) AS OrderingECTIN, --O
	--	CASE
	--	WHEN ptc.TransactionStatus = ''Z'' THEN ''Inactive''
	--	ELSE  ''Active''
	--	END as Status, --R
	--	ISNULL((select top(1) a.reason from [model].[GoalInstruction] a where a.reason <> '''' and a.AppointmentId =ap.AppointmentId ),''NOT AVAILABLE'') AS ReasonforReferral,
	--	ptc.TransactionId,
	--	ec.Id
		

	--from dbo.PracticeTransactionJournal ptc WITH(NOLOCK) left join dbo.appointments ap WITH(NOLOCK)
	--on ptc.transactiontypeid = ap.appointmentid  

	--left join dbo.resources r WITH(NOLOCK)
	--on ap.resourceid1 = r.resourceid

	--left join model.externalcontacts ec WITH(NOLOCK)
	--on cast(ec.id as nvarchar(10)) = ptc.transactionrcvrid

	--where 
	--ptc.transactiontype = ''F'' --F is for Referral Letters



select 
		''CARECOORDINATIONORDER'' AS RESOURCETYPE, --R
		''EHR''[Source], --R
		'''' as ReferenceNumber, --R
		d.AciDepKey as ExternalId, --R
		CASE
			WHEN d.ClinicalDocumentType =''R'' THEN ''Referral''
			ELSE ''TOC''
		END AS Description, --R
		d.PatientKey as PatientAccountNumber, --R
		(CONVERT(VARCHAR(20), d.CreatedDate, 101)+'' ''+(CASE
                                                                  WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), d.CreatedDate, 22), 11))) = 10
                                                                  THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20),d.CreatedDate, 22), 11))
                                                                  ELSE LTRIM(RIGHT(CONVERT(CHAR(20),d.CreatedDate, 22), 11))
                                                              END)) AS Date,
		CASE
			WHEN d.ClinicalDocumentType =''R'' THEN ''Referral''
			ELSE ''TOC''
		END AS OrderType, --R
		r.npi as OrderingECNPI, --R
		RIGHT(''0000000000''+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''''), 1, 9)), 9) AS OrderingECTIN, --O

		''Active'' as Status, --R
		d.ReasonContent AS ReasonforReferral,
		d.AciDepKey

from dbo.ACI_DEP d WITH(NOLOCK) join dbo.Resources r WITH(NOLOCK) on r.ResourceId = d.ProviderKey
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN')) 
Drop View VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN]
AS
	SELECT
		d.AciDepKey,
		ec.id as AccountNumber,--R
		ec.firstname AS GivenName, --R
		ec.lastnameorentityName as FamilyName,--R
		'''' as SecondandFurtherGivenNames,--O
		'''' AS DateOfBirth, --O
		ec.Prefix AS NamePrefix, --O
		ec.Suffix AS NameSuffix --O

	from dbo.ACI_DEP d WITH(NOLOCK)  join model.externalcontacts ec WITH(NOLOCK)
	on ec.id = d.PtOutboundReferralKey
	

'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN_ADDRESS')) 
Drop View VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN_ADDRESS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN_ADDRESS]
AS
SELECT 
		d.AciDepKey,
		ec.id as AccountNumber,--R
		''ADDRESS'' AS TYPE, --O
            ECA.LINE1 AS ADDRESSLINE1, --O
            ECA.LINE2 AS ADDRESSLINE2, --O
            ECA.CITY AS CITY, --O
            SOP.NAME AS STATE, --O
            ECA.POSTALCODE AS ZIP --O
            
	
	from dbo.ACI_DEP d WITH(NOLOCK)  join model.externalcontacts ec WITH(NOLOCK) on ec.id = d.PtOutboundReferralKey
		
	LEFT JOIN [model].[ExternalContactAddresses] ECA WITH (NOLOCK) ON EC.ID = ECA.EXTERNALCONTACTID

	LEFT JOIN MODEL.[StateOrProvinces] SOP WITH (NOLOCK) ON ECA.STATEORPROVINCEID = SOP.ID
' 


Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_VITALSIGN')) 
Drop View VIEW_ACI_VITALSIGN 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'

CREATE VIEW [dbo].[VIEW_ACI_VITALSIGN]
AS

	SELECT
		''VitalSign'' AS RESOURCETYPE,
		CAST(PC.Appointmentid AS VARCHAR(20))  AS ExternalId,
		PC.appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		patientid AS PATIENTKEY
     FROM DBO.PATIENTCLINICAL PC WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/BLOOD P%'' AND FINDINGDETAIL LIKE ''*BLOODPRESSURE%''

UNION

	SELECT
		''VitalSign'' AS RESOURCETYPE,
		CAST(PC.appointmentid AS VARCHAR(20)) AS ExternalId,
		PC.appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		patientid AS PATIENTKEY
     FROM DBO.PATIENTCLINICAL PC WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/BLOOD P%'' AND FINDINGDETAIL LIKE ''*BLOODPRESSURE%''

UNION


		SELECT
		''VitalSign'' AS RESOURCETYPE,
		CAST(PC.appointmentid AS VARCHAR(20)) AS ExternalId,
		PC.Appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		patientid AS PATIENTKEY
     FROM DBO.PATIENTCLINICAL PC WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*HEIGHT%''


UNION

		SELECT
		''VitalSign'' AS RESOURCETYPE,
		CAST(PC.appointmentid AS VARCHAR(20)) AS ExternalId,
		PC.appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		patientid AS PATIENTKEY
     FROM DBO.PATIENTCLINICAL PC WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*WEIGHT%''



UNION

		SELECT
		''VitalSign'' AS RESOURCETYPE,
		CAST(PC.appointmentid AS VARCHAR(20)) AS ExternalId,
		PC.appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		patientid AS PATIENTKEY
     FROM DBO.PATIENTCLINICAL PC WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/HEART RATE'' AND FINDINGDETAIL LIKE ''*HEART RATE%''


UNION

		SELECT
		''VitalSign'' AS RESOURCETYPE,
		CAST(PC.appointmentid AS VARCHAR(20)) AS ExternalId,
		PC.appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		patientid AS PATIENTKEY
     FROM DBO.PATIENTCLINICAL PC WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/O2 % BLDC OXIMETRY'' AND FINDINGDETAIL LIKE ''*O2 % BLDC OXIMETRY%''


UNION

		SELECT
		''VitalSign'' AS RESOURCETYPE,
		CAST(PC.appointmentid AS VARCHAR(20)) AS ExternalId,
		PC.appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		patientid AS PATIENTKEY
     FROM DBO.PATIENTCLINICAL PC WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/INHALED OXYGEN CONCENTRATION'' AND FINDINGDETAIL LIKE ''*INHALED OXYGEN CONCENTRATION%''


UNION

		SELECT
		''VitalSign'' AS RESOURCETYPE,
		CAST(PC.appointmentid AS VARCHAR(20)) AS ExternalId,
		PC.appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		patientid AS PATIENTKEY
     FROM DBO.PATIENTCLINICAL PC WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/BODY TEMPERATURE'' AND FINDINGDETAIL LIKE ''*BODY TEMPERATURE%''


UNION

		SELECT
		''VitalSign'' AS RESOURCETYPE,
		CAST(PC.appointmentid AS VARCHAR(20)) AS ExternalId,
		PC.appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		patientid AS PATIENTKEY
     FROM DBO.PATIENTCLINICAL PC WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/RESPIRATORY RATE'' AND FINDINGDETAIL LIKE ''*RESPIRATORY RATE%''


UNION

		SELECT
		''VitalSign'' AS RESOURCETYPE,
		CAST(PC.appointmentid AS VARCHAR(20)) AS ExternalId,
		PC.appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		patientid AS PATIENTKEY
     FROM DBO.PATIENTCLINICAL PC WITH(NOLOCK)
     WHERE SYMPTOM LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*BMI%''
' 



Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN_PHONE')) 
Drop View VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN_PHONE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN_PHONE]
AS
		SELECT
			d.AciDepKey,
			ec.id as AccountNumber,
			''PHONE'' AS RESOURCETYPE,--O
			''WORK PLACE'' AS TYPE,--O
			CASE
			WHEN LEN(ECP.EXTENSION)>0
			THEN CAST((ECP.AREACODE + ''-'' + SUBSTRING(ECP.EXCHANGEANDSUFFIX,1,3)+''-''+SUBSTRING(ECP.EXCHANGEANDSUFFIX,4,4)+''X''+ECP.EXTENSION) AS NVARCHAR(20))
			ELSE CAST((ECP.AREACODE + ''-'' + SUBSTRING(ECP.EXCHANGEANDSUFFIX,1,3)+''-''+SUBSTRING(ECP.EXCHANGEANDSUFFIX,4,4)) AS NVARCHAR(20))
			END AS NUMBER --O

from dbo.ACI_DEP d WITH(NOLOCK)  join model.externalcontacts ec WITH(NOLOCK) on ec.id = d.PtOutboundReferralKey
		
 JOIN [model].[ExternalContactPhoneNumbers] ECP WITH (NOLOCK) ON EC.ID = ECP.EXTERNALCONTACTID

' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT')) 
Drop View VIEW_ACI_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE VIEW [dbo].[VIEW_ACI_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT]
AS
	SELECT
		'SpecialistReportReceivedEvent' AS RESOURCETYPE, --R
		'EHR' AS IsSource, --R
		PC.CLINICALID AS ReferenceNumber, --R
		PC.PATIENTID AS PatientAccountNumber, --R
		SUBSTRING(PC.FINDINGDETAIL,CHARINDEX('<',PC.FINDINGDETAIL)+1,CHARINDEX('>',PC.FINDINGDETAIL)-CHARINDEX('<',PC.FINDINGDETAIL)-1)AS DateReceived, --R
		ISNULL(r.npi,'0000000000') as ECNPI, --R
		RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''), 1, 9)), 9) AS ECTIN, --R
		PC.CLINICALID ExternalId, --R
		PTC.TRANSACTIONID AS ReferralOrderNumber,  --R
		PC.ClinicalId,
		PC.AppointmentId,
		PC.PatientId,
		R.ResourceId AS ResourceId
	FROM
	DBO.PatientClinical PC WITH(NOLOCK) LEFT JOIN DBO.APPOINTMENTS AP WITH(NOLOCK) ON PC.APPOINTMENTID = AP.APPOINTMENTID
	left join dbo.resources r WITH(NOLOCK)	on ap.resourceid1 = r.resourceid
	LEFT JOIN dbo.PracticeTransactionJournal ptc WITH(NOLOCK) ON AP.APPOINTMENTID = ptc.transactiontypeid
	WHERE SYMPTOM = '/REFERRAL RESPONSE RECEIVED' AND FINDINGDETAIL LIKE '*PERFORMED-REFERRALRESPONSERECEIVED%'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON22_SPECIALISTREPORTRECEIVEDEVENT')) 
Drop View VIEW_ACI_JSON22_SPECIALISTREPORTRECEIVEDEVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON22_SPECIALISTREPORTRECEIVEDEVENT]
AS
	SELECT
		'SpecialistReportReceivedEvent' AS RESOURCETYPE, --R
		'EHR' AS IsSource, --R
		PC.CLINICALID AS ReferenceNumber, --R
		PC.PATIENTID AS PatientAccountNumber, --R
		SUBSTRING(PC.FINDINGDETAIL,CHARINDEX('<',PC.FINDINGDETAIL)+1,CHARINDEX('>',PC.FINDINGDETAIL)-CHARINDEX('<',PC.FINDINGDETAIL)-1) AS DateReceived, --R
		ISNULL(r.npi,'0000000000') as ECNPI, --R
		RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''), 1, 9)), 9) AS ECTIN, --R
		PC.CLINICALID ExternalId, --R
		PTC.TRANSACTIONID AS ReferralOrderNumber,  --R
		PC.ClinicalId,
		PC.AppointmentId,
		PC.PatientId,
		R.ResourceId AS ResourceId
	FROM
	DBO.PatientClinical PC WITH(NOLOCK) LEFT JOIN DBO.APPOINTMENTS AP WITH(NOLOCK) ON PC.APPOINTMENTID = AP.APPOINTMENTID
	left join dbo.resources r WITH(NOLOCK)	on ap.resourceid1 = r.resourceid
	LEFT JOIN dbo.PracticeTransactionJournal ptc WITH(NOLOCK) ON AP.APPOINTMENTID = ptc.transactiontypeid
	WHERE SYMPTOM = '/REFERRAL RESPONSE RECEIVED' AND FINDINGDETAIL LIKE '*PERFORMED-REFERRALRESPONSERECEIVED%'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON12_CLINICALDATARECONCILIATION_PGHD')) 
Drop View VIEW_ACI_JSON12_CLINICALDATARECONCILIATION_PGHD 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'



CREATE VIEW [dbo].[VIEW_ACI_JSON12_CLINICALDATARECONCILIATION_PGHD] AS 
SELECT
	''Reconcilation'' AS RESOURCETYPE ,--R
	PEL.ID AS EXTERNALID,--R
	''EHR'' AS ISSOURCE,--R
	 '''' AS REFERENCENUMBER,--R
	PEL.PATIENTID AS PATIENTACCOUNTNUMBER,--R	
	case when  PEL.CREATEDDATETIME is null then ''01/01/1900 01:00:00 AM'' else
	(CONVERT(VARCHAR(20), PEL.CREATEDDATETIME, 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PEL.CREATEDDATETIME, 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), PEL.CREATEDDATETIME, 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PEL.CREATEDDATETIME, 22), 11))
                                                              END)) END AS FILERECEIVEDDATE,
	''PGHD'' as ReconcileFiletype,--R	
	case when  PEL.CREATEDDATETIME is null then ''01/01/1900 01:00:00 AM'' else
	(CONVERT(VARCHAR(20), PEL.CREATEDDATETIME, 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PEL.CREATEDDATETIME, 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), PEL.CREATEDDATETIME, 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PEL.CREATEDDATETIME, 22), 11))
                                                              END)) END AS RECONCILEDATE,
	 ''False'' AS MEDICATIONRECONCILED,--O
	''False''  AS ALLERGIESRECONCILED,--O
	''False''  AS PROBLEMSRECONCILED,--O
	''True'' AS ISSOCINCORPORATED,--R
	ISNULL(r.npi,'''') as ECNPI, --R
	RIGHT(''0000000000''+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''''), 1, 9)), 9) AS TIN,
	PEL.ID
FROM 
[MVE].[PP_PatientExternalLink] PEL WITH(NOLOCK)
LEFT JOIN DBO.Appointments AP ON AP.Appointmentid = PEL.AppointmentId
LEFT JOIN DBO.RESOURCES R WITH(NOLOCK) ON  R.RESOURCEID  =AP.ResourceID1


' 
Go



IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON25_JSON26_JSON27_EPISODENUMBER')) 
Drop View VIEW_ACI_JSON25_JSON26_JSON27_EPISODENUMBER 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 
CREATE VIEW [dbo].[VIEW_ACI_JSON25_JSON26_JSON27_EPISODENUMBER]
AS
SELECT appointmentid,patientid,  
encounterid=STUFF  
(  
     (  
       SELECT   '/' + CAST(encounterid AS VARCHAR(MAX))  
       FROM dbo.Appointments t2   
       WHERE t2.patientid = t1.patientid and t2.appdate <t1.appdate and ActivityStatus = 'D' and ScheduleStatus = 'D' and AppTypeId <>0
	   order by t2.AppDate desc
       FOR XML PATH('')  
     ),1,1,''  
)  
FROM Appointments t1 WHERE ActivityStatus = 'D' and ScheduleStatus = 'D' and AppTypeId <>0 GROUP BY  AppointmentId ,PatientId,appdate
Go

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON25_SurgeryEvent')) 
Drop View VIEW_ACI_JSON25_SurgeryEvent 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE VIEW [dbo].[VIEW_ACI_JSON25_SurgeryEvent]

AS
	
	SELECT DISTINCT
		'SurgeryEvent' AS ResourceType, --R
		'EHR' AS IsSource, --R
		PC.PATIENTID AS PatientAccountNumber, --R
		SUBSTRING(AP.APPDATE,5,2) + '/' + SUBSTRING(AP.APPDATE,7,2) + '/' + SUBSTRING(AP.APPDATE,1,4)as DateofSurgery,
		ISNULL(r.npi,'0000000000') as ECNPI, --R
		RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''), 1, 9)), 9) AS ECTIN, --R
		PC.APPOINTMENTID AS ExternalId,
		CASE
			WHEN ((select count(*) from dbo.CataractLog)=1) THEN (Select top (1) AppointmentId from dbo.CataractLog where patientid = PC.PATIENTID)
		ELSE
			(Select top (1) AppointmentId from dbo.CataractLog where patientid = PC.PATIENTID AND (AP.Appdate>Appdate OR AP.Appdate=Appdate)  order by Appdate desc)
		END AS EpisodeNumber,
		'TRUE' AS IsInHousePhysician,
		r.npi as PerformedPhysicianNPI, --R
		R.RESOURCEID AS PerformedPhysicianAccountNumber,
		R.RESOURCENAME AS PerformedPhysicianName,
		--R.ResourceType As ResourceType,
		PC.APPOINTMENTID As ReferenceNumber,
		PC.APPOINTMENTID,
		PC.PATIENTID
		--,PC.CLINICALID
	from model.servicemodifiers SM WITH(NOLOCK) JOIN model.BillingServiceModifiers BSM WITH(NOLOCK) ON BSM.ServiceModifierId =SM.Id
		JOIN model.billingservices BS WITH(NOLOCK) ON BS.Id=BSM.BillingServiceId
		JOIN model.encounterservices ES WITH(NOLOCK) ON ES.ID =BS.EncounterServiceId
		JOIN model.Invoices Inv ON Inv.Id = BS.InvoiceId
		JOIN dbo.Appointments Ap WITH(NOLOCK) ON AP.AppointmentId = Inv.EncounterId
		JOIN dbo.Resources R WITH(NOLOCK) ON R.ResourceId = Ap.ResourceId1
		JOIN dbo.PatientClinical PC WITH(NOLOCK) ON Pc.AppointmentID = Ap.AppointmentId
		JOIN dbo.VIEW_ACI_JSON25_JSON26_JSON27_EPISODENUMBER En on Ap.AppointmentId= En.AppointmentId
	WHERE SM.Code <>'78' AND ES.CODE IN ('66982','66840','66984','66983') AND AP.AppTypeId >0 And Ap.ActivityStatus = 'D' and ScheduleStatus = 'D'
		AND PC.SYMPTOM != '/CATARACT SURGERY, COMPLEX' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE')) 
Drop View VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE VIEW [dbo].[VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE]

AS

	SELECT 
	    'Procedure' AS RESOURCETYPE,
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		PC.CLINICALID,
		SUBSTRING(PC.FINDINGDETAIL, CHARINDEX(':',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX(':',PC.FINDINGDETAIL)+1) AS Code,
		ES.DESCRIPTION AS Description,
		'CPT' AS CodeSystemName,
		'2.16.840.1.113883.6.12' AS CodeSystem,
		BS.Id As Id
	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN MODEL.ENCOUNTERSERVICES ES WITH(NOLOCK)
	ON SUBSTRING(PC.FINDINGDETAIL, CHARINDEX(':',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX(':',PC.FINDINGDETAIL)+1) = ES.CODE
	LEFT JOIN Model.Invoices Inv
	ON PC.AppointmentId= Inv.EncounterId
	Left Join model.BillingServices BS ON
	BS.EncounterServiceId =ES.ID and Inv.Id = BS.InvoiceId
	WHERE PC.CLINICALTYPE = 'P' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON10_CPOELABORATORYORIMAGING')) 
Drop View VIEW_ACI_JSON10_CPOELABORATORYORIMAGING 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON10_CPOELABORATORYORIMAGING]
AS
	SELECT
		'LabRadiologyOrder' AS RESOURCETYPE,
		'EHR' AS IsSource,
		CAST(OrderID AS VARCHAR(10)) AS ReferenceNumber,
		AP.APPOINTMENTID AS EXTERNALID,
		PL.PATIENTID AS PATIENTACCOUNTNUMBER,		
		ORDERID
	FROM PATIENTLABORDERS PL
	JOIN DBO.Appointments AP WITH(NOLOCK) ON AP.AppointmentID=PL.AppID
	Where PL.LOINCNUMBER<>''
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_VITALSIGN_MEASURES')) 
Drop View VIEW_ACI_VITALSIGN_MEASURES 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_VITALSIGN_MEASURES]
AS
     SELECT ''MEASURES'' AS RESOURCETYPE,
            ''SYSTOLIC BLOOD PRESSURE'' AS NAME,
		  ''8480-6'' as NameCode, -- Rev1.1
            ''2.16.840.1.113883.6.1'' AS NameCodeSystem,
            ''LOINC'' AS NameCodeSystemName,
		   (CONVERT(VARCHAR(20), CONVERT(DATETIME,ap.AppDate), 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                           END)) AS DateTime,

            ''mm[Hg]'' AS UnitsCode,
            ''2.16.840.1.113883.1.11.12839'' AS UnitsCodeSystem,
            ''UCUM'' AS UnitsCodeSystemName,
            '''' AS Notes,
            CASE
                WHEN CHARINDEX(''*BLOODPRESSURE='', PC.FINDINGDETAIL) = 1
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''/'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN CHARINDEX(''*BLOODPRESSURELEFT='', PC.FINDINGDETAIL) = 1
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''/'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN CHARINDEX(''*BLOODPRESSURERTWRIST='', PC.FINDINGDETAIL) = 1
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''/'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN CHARINDEX(''*BLOODPRESSURELEFTWRIST='', PC.FINDINGDETAIL) = 1
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''/'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                ELSE ''''
            END AS VALUE,
            ''mm[Hg]'' AS Units,
           PC.Appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		pc.patientid AS PATIENTKEY,
		CAST(PC.CLINICALID AS VARCHAR(20)) AS VK,
		''Systolic'' as TY
		 -- SELECT pc.*
     FROM DBO.PATIENTCLINICAL PC WITH (NOLOCK)
	left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
     WHERE SYMPTOM LIKE ''/BLOOD P%''
           AND FINDINGDETAIL LIKE ''*BLOODPRESSURE%''

UNION

SELECT ''MEASURES'' AS RESOURCETYPE,
            ''DIASTOLIC BLOOD PRESSURE'' AS NAME,
		  ''8462-4'' as NameCode, -- Rev1.1
            ''2.16.840.1.113883.6.1'' AS NameCodeSystem,
            ''LOINC'' AS NameCodeSystemName,
		   (CONVERT(VARCHAR(20), CONVERT(DATETIME,ap.AppDate), 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                           END)) AS DateTime,

            ''mm[Hg]'' AS UnitsCode,
            ''2.16.840.1.113883.1.11.12839'' AS UnitsCodeSystem,
            ''UCUM'' AS UnitsCodeSystemName,
            '''' AS Notes,
            CASE
                WHEN (CHARINDEX(''*BLOODPRESSURE='', PC.FINDINGDETAIL) = 1) AND (CHARINDEX(''/'', PC.FINDINGDETAIL) > 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail), LEN(pc.findingdetail)), ''/'', '''')))-1))
                WHEN (CHARINDEX(''*BLOODPRESSURELEFT='', PC.FINDINGDETAIL) = 1)  AND (CHARINDEX(''/'', PC.FINDINGDETAIL) > 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail), LEN(pc.findingdetail)), ''/'', '''')))-1))
                WHEN (CHARINDEX(''*BLOODPRESSURERTWRIST='', PC.FINDINGDETAIL) = 1) AND (CHARINDEX(''/'', PC.FINDINGDETAIL) > 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail), LEN(pc.findingdetail)), ''/'', '''')))-1))
                WHEN (CHARINDEX(''*BLOODPRESSURELEFTWRIST='', PC.FINDINGDETAIL) = 1) AND (CHARINDEX(''/'', PC.FINDINGDETAIL) > 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail), LEN(pc.findingdetail)), ''/'', '''')))-1))
                ELSE ''''
            END AS VALUE,
            ''mm[Hg]'' AS Units,
            PC.Appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		pc.patientid AS PATIENTKEY,
		CAST(PC.CLINICALID AS VARCHAR(20))+'''' AS VK,
		''Diastolic'' as TY
		  --SELECT *
     FROM DBO.PATIENTCLINICAL PC WITH (NOLOCK)
	left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
     WHERE SYMPTOM LIKE ''/BLOOD P%''
           AND FINDINGDETAIL LIKE ''*BLOODPRESSURE%''



UNION

SELECT ''MEASURES'' AS RESOURCETYPE,
            ''BODY HEIGHT'' AS NAME,
		  ''8302-2'' as NameCode, -- Rev1.1
            ''2.16.840.1.113883.6.1'' AS NameCodeSystem,
            ''LOINC'' AS NameCodeSystemName,
		   (CONVERT(VARCHAR(20), CONVERT(DATETIME,ap.AppDate), 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                           END)) AS DateTime,

            SUBSTRING(pc.findingdetail, CHARINDEX(''-'', pc.findingdetail)+1, (CHARINDEX(''='', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''-'', pc.findingdetail), LEN(pc.findingdetail)), ''-'', '''')))-1)) AS UnitsCode,
            ''2.16.840.1.113883.1.11.12839'' AS UnitsCodeSystem,
            ''UCUM'' AS UnitsCodeSystemName,
            '''' AS Notes,
            CASE
                WHEN (CHARINDEX(''*HEIGHT-FEET'', PC.FINDINGDETAIL) = 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*HEIGHT-INCHES'', PC.FINDINGDETAIL) = 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*HEIGHT-METERS'', PC.FINDINGDETAIL) = 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*HEIGHT-CM'', PC.FINDINGDETAIL) = 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                ELSE ''''
            END AS VALUE,
            SUBSTRING(pc.findingdetail, CHARINDEX(''-'', pc.findingdetail)+1, (CHARINDEX(''='', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''-'', pc.findingdetail), LEN(pc.findingdetail)), ''-'', '''')))-1)) AS Units,
            PC.Appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		pc.patientid AS PATIENTKEY,
		CAST(PC.CLINICALID AS VARCHAR(20)) AS VK,
		''Height'' as TY
		  --SELECT *
     FROM DBO.PATIENTCLINICAL PC WITH (NOLOCK)
	left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
     WHERE SYMPTOM LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*HEIGHT%''
	 UNION

SELECT ''MEASURES'' AS RESOURCETYPE,
            ''BODY WEIGHT'' AS NAME,
		  ''29463-7'' as NameCode, -- Rev1.1
            ''2.16.840.1.113883.6.1'' AS NameCodeSystem,
            ''LOINC'' AS NameCodeSystemName,
		   (CONVERT(VARCHAR(20), CONVERT(DATETIME,ap.AppDate), 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                           END)) AS DateTime,

            --SUBSTRING(pc.findingdetail, CHARINDEX(''-'', pc.findingdetail)+1, (CHARINDEX(''='', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''-'', pc.findingdetail), LEN(pc.findingdetail)), ''-'', '''')))-1)) AS UnitsCode,
			''kg'' AS UnitsCode,
            ''2.16.840.1.113883.1.11.12839'' AS UnitsCodeSystem,
            ''UCUM'' AS UnitsCodeSystemName,
            '''' AS Notes,
            CASE
                WHEN (CHARINDEX(''*WEIGHT-POUNDS'', PC.FINDINGDETAIL) = 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*WEIGHT-OUNCES'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*WEIGHT-KGS'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*WEIGHT-GRAMS'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                ELSE ''''
            END AS VALUE,
            --SUBSTRING(pc.findingdetail, CHARINDEX(''-'', pc.findingdetail)+1, (CHARINDEX(''='', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''-'', pc.findingdetail), LEN(pc.findingdetail)), ''-'', '''')))-1)) AS Units,
			''kg'' AS Units,
            PC.Appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		pc.patientid AS PATIENTKEY,
		CAST(PC.CLINICALID AS VARCHAR(20)) AS VK,
		''Weight'' as TY
		  --SELECT *
     FROM DBO.PATIENTCLINICAL PC WITH (NOLOCK)
	left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
     WHERE SYMPTOM LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*WEIGHT%''


UNION

     SELECT ''MEASURES'' AS RESOURCETYPE,
            ''HEART RATE'' AS NAME,
		  ''8867-4'' as NameCode, -- Rev1.1
            ''2.16.840.1.113883.6.1'' AS NameCodeSystem,
            ''LOINC'' AS NameCodeSystemName,
		   (CONVERT(VARCHAR(20), CONVERT(DATETIME,ap.AppDate), 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                           END)) AS DateTime,

            ''/min'' AS UnitsCode,
            ''2.16.840.1.113883.1.11.12839'' AS UnitsCodeSystem,
            ''UCUM'' AS UnitsCodeSystemName,
            '''' AS Notes,
            CASE
                WHEN (CHARINDEX(''*HEART RATE'', PC.FINDINGDETAIL) = 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*HEART RATE'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*HEART RATE'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*HEART RATE'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                ELSE ''''
            END AS VALUE,
            ''/min'' AS Units,
           PC.Appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		pc.patientid AS PATIENTKEY,
		CAST(PC.CLINICALID AS VARCHAR(20)) AS VK,
		''Systolic'' as TY
		 -- SELECT pc.*
     FROM DBO.PATIENTCLINICAL PC WITH (NOLOCK)
	left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
       WHERE SYMPTOM LIKE ''/HEART RATE'' AND FINDINGDETAIL LIKE ''*HEART RATE%''


UNION


     SELECT ''MEASURES'' AS RESOURCETYPE,
            ''O2 % BLDC OXIMETRY'' AS NAME,
		  ''59408-5'' as NameCode, -- Rev1.1
            ''2.16.840.1.113883.6.1'' AS NameCodeSystem,
            ''LOINC'' AS NameCodeSystemName,
		   (CONVERT(VARCHAR(20), CONVERT(DATETIME,ap.AppDate), 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                           END)) AS DateTime,

            ''%'' AS UnitsCode,
            ''2.16.840.1.113883.1.11.12839'' AS UnitsCodeSystem,
            ''UCUM'' AS UnitsCodeSystemName,
            '''' AS Notes,
            CASE
                WHEN (CHARINDEX(''*O2 % BLDC OXIMETRY'', PC.FINDINGDETAIL) = 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*O2 % BLDC OXIMETRY'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*O2 % BLDC OXIMETRY'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*O2 % BLDC OXIMETRY'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                ELSE ''''
            END AS VALUE,
            ''%'' AS Units,
           PC.Appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		pc.patientid AS PATIENTKEY,
		CAST(PC.CLINICALID AS VARCHAR(20)) AS VK,
		''Systolic'' as TY
		 -- SELECT pc.*
     FROM DBO.PATIENTCLINICAL PC WITH (NOLOCK)
	left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
       WHERE SYMPTOM LIKE ''/O2 % BLDC OXIMETRY'' AND FINDINGDETAIL LIKE ''*O2 % BLDC OXIMETRY%''


UNION

 SELECT ''MEASURES'' AS RESOURCETYPE,
            ''INHALED OXYGEN CONCENTRATION'' AS NAME,
		  ''3150-0'' as NameCode, -- Rev1.1
            ''2.16.840.1.113883.6.1'' AS NameCodeSystem,
            ''LOINC'' AS NameCodeSystemName,
		   (CONVERT(VARCHAR(20), CONVERT(DATETIME,ap.AppDate), 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                           END)) AS DateTime,

            ''%'' AS UnitsCode,
            ''2.16.840.1.113883.1.11.12839'' AS UnitsCodeSystem,
            ''UCUM'' AS UnitsCodeSystemName,
            '''' AS Notes,
            CASE
                WHEN (CHARINDEX(''*INHALED OXYGEN CONCENTRATION'', PC.FINDINGDETAIL) = 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*INHALED OXYGEN CONCENTRATION'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*INHALED OXYGEN CONCENTRATION'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*INHALED OXYGEN CONCENTRATION'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                ELSE ''''
            END AS VALUE,
            ''%'' AS Units,
           PC.Appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		pc.patientid AS PATIENTKEY,
		CAST(PC.CLINICALID AS VARCHAR(20)) AS VK,
		''Systolic'' as TY
		 -- SELECT pc.*
     FROM DBO.PATIENTCLINICAL PC WITH (NOLOCK)
	left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
       WHERE SYMPTOM LIKE ''/INHALED OXYGEN CONCENTRATION'' AND FINDINGDETAIL LIKE ''*INHALED OXYGEN CONCENTRATION%''


UNION


 SELECT ''MEASURES'' AS RESOURCETYPE,
            ''BODY TEMPERATURE'' AS NAME,
		  ''8310-5'' as NameCode, -- Rev1.1
            ''2.16.840.1.113883.6.1'' AS NameCodeSystem,
            ''LOINC'' AS NameCodeSystemName,
		   (CONVERT(VARCHAR(20), CONVERT(DATETIME,ap.AppDate), 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                           END)) AS DateTime,

            ''Cel'' AS UnitsCode,
            ''2.16.840.1.113883.1.11.12839'' AS UnitsCodeSystem,
            ''UCUM'' AS UnitsCodeSystemName,
            '''' AS Notes,
            CASE
                WHEN (CHARINDEX(''*BODY TEMPERATURE'', PC.FINDINGDETAIL) = 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*BODY TEMPERATURE'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*BODY TEMPERATURE'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*BODY TEMPERATURE'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                ELSE ''''
            END AS VALUE,
            ''Cel'' AS Units,
           PC.Appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		pc.patientid AS PATIENTKEY,
		CAST(PC.CLINICALID AS VARCHAR(20)) AS VK,
		''Systolic'' as TY
		 -- SELECT pc.*
     FROM DBO.PATIENTCLINICAL PC WITH (NOLOCK)
	left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
       WHERE SYMPTOM LIKE ''/BODY TEMPERATURE'' AND FINDINGDETAIL LIKE ''*BODY TEMPERATURE%''


UNION

 SELECT ''MEASURES'' AS RESOURCETYPE,
            ''RESPIRATORY RATE'' AS NAME,
		  ''9279-1'' as NameCode, -- Rev1.1
            ''2.16.840.1.113883.6.1'' AS NameCodeSystem,
            ''LOINC'' AS NameCodeSystemName,
		   (CONVERT(VARCHAR(20), CONVERT(DATETIME,ap.AppDate), 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                           END)) AS DateTime,

            ''/min'' AS UnitsCode,
            ''2.16.840.1.113883.1.11.12839'' AS UnitsCodeSystem,
            ''UCUM'' AS UnitsCodeSystemName,
            '''' AS Notes,
            CASE
                WHEN (CHARINDEX(''*RESPIRATORY RATE'', PC.FINDINGDETAIL) = 1)
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*RESPIRATORY RATE'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*RESPIRATORY RATE'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                WHEN (CHARINDEX(''*RESPIRATORY RATE'', PC.FINDINGDETAIL) = 1) 
                THEN SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))
                ELSE ''''
            END AS VALUE,
            ''/min'' AS Units,
           PC.Appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		pc.patientid AS PATIENTKEY,
		CAST(PC.CLINICALID AS VARCHAR(20)) AS VK,
		''Systolic'' as TY
		 -- SELECT pc.*
     FROM DBO.PATIENTCLINICAL PC WITH (NOLOCK)
	left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
       WHERE SYMPTOM LIKE ''/RESPIRATORY RATE'' AND FINDINGDETAIL LIKE ''*RESPIRATORY RATE%''


UNION

SELECT ''MEASURES'' AS RESOURCETYPE,
            ''Body mass index (BMI) [Ratio]'' AS NAME,
		  ''39156-5'' as NameCode, -- Rev1.1
            ''2.16.840.1.113883.6.1'' AS NameCodeSystem,
            ''LOINC'' AS NameCodeSystemName,
		   (CONVERT(VARCHAR(20), CONVERT(DATETIME,ap.AppDate), 101)+'' ''+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))) = 10
                                                               THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                           END)) AS DateTime,

            SUBSTRING(pc.findingdetail, CHARINDEX(''-'', pc.findingdetail)+1, (CHARINDEX(''='', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''-'', pc.findingdetail), LEN(pc.findingdetail)), ''-'', '''')))-1)) AS UnitsCode,
            ''2.16.840.1.113883.1.11.12839'' AS UnitsCodeSystem,
            ''UCUM'' AS UnitsCodeSystemName,
            '''' AS Notes,
            SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail)+1, (CHARINDEX(''>'', (REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1)) AS VALUE,
            ''Kg/m2'' AS Units,
            PC.Appointmentid AS VITALSKEY,
		pc.appointmentid AS CHARTRECORDKEY,
		pc.patientid AS PATIENTKEY,
		CAST(PC.CLINICALID AS VARCHAR(20)) AS VK,
		''BMI'' as TY
		  --SELECT *
     FROM DBO.PATIENTCLINICAL PC WITH (NOLOCK)
	left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
     WHERE SYMPTOM LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*BMI%''
' 
 Go
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE_Diagnosis')) 
Drop View VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE_Diagnosis 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE_Diagnosis]

AS
	SELECT 
		'Diagnosis' AS RESOURCETYPE,
		PC.CLINICALID,
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		LEFT(PC.FINDINGDETAIL,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',SUBSTRING(PC.FINDINGDETAIL, CHARINDEX('.',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',PC.FINDINGDETAIL)))) AS Code,
		Q.REVIEWSYSTEMLINGO AS Description,
		'2.16.840.1.113883.6.103' AS CodeSystem,
		'ICD9CM' AS CodeSystemName
	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN DM.PRIMARYDIAGNOSISTABLE Q WITH(NOLOCK)
	ON PC.FINDINGDETAIL = Q.DIAGNOSISNEXTLEVEL
	WHERE PC.CLINICALTYPE IN ('B','K')


 Go
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_LABRADIOLOGYORDER_ORDERCODE')) 
Drop View VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_LABRADIOLOGYORDER_ORDERCODE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_LABRADIOLOGYORDER_ORDERCODE]
AS
    SELECT 
	    L.LOINC_NUM as Code,
        L.LONG_COMMON_NAME Description,
		'LOINC' AS CodeSystemName,
		'2.16.840.1.113883.6.1' AS CodeSystem,
        PLO.OrderID,
        PLO.AppID
	from 
    DBO.PATIENTLABORDERS PLO WITH(NOLOCK) 
	LEFT JOIN DBO.LOINC_2017 L WITH(NOLOCK) ON L.LOINC_NUM=  PLO.LOINCNUMBER 
	LEFT JOIN DBO.APPOINTMENTS AP WITH(NOLOCK) ON PLO.APPID = AP.APPOINTMENTID
	left join dbo.resources r WITH(NOLOCK) on ap.resourceid1 = r.resourceid
	Where PLO.LOINCNUMBER<>''
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_INSTRUCTIONS')) 
Drop View VIEW_ACI_INSTRUCTIONS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[VIEW_ACI_INSTRUCTIONS]
AS
     SELECT ''INSTRUCTIONS'' AS RESOURCETYPE,
            Appointmentid AS EXTERNALID, --R 
            ''NOT AVAILABLE'' AS DESCRIPTION, --R 
            ''TRUE'' AS ISACTIVE, --R 
		  CLINICALID AS INSTRUCTIONSKEY,
   		  appointmentid AS CHARTRECORDKEY,
		  patientid AS PATIENTKEY
		  --SELECT TOP 10 *
     FROM patientclinical WITH (NOLOCK)
	WHERE SYMPTOM LIKE ''/INSTRUCTION%'';
' 
 Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE_Medication')) 
Drop View VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE_Medication 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE_Medication]

AS
	SELECT 
		'Medication' AS RESOURCETYPE,
		PC.CLINICALID,
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		CASE
                WHEN NC.MEDID = '' THEN 'UNCODIFIED'
                ELSE CAST(NC.MEDID AS VARCHAR(15))
        END AS Code, 

		CASE WHEN pc.FindingDetail like 'RX-8/%-2/%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') 
			ELSE '' 
		END  AS Description, 
		'RxNorm' AS CodeSystem,
		'2.16.840.1.113883.6.88' AS CodeSystemName
	FROM
	 dbo.patientclinical pc WITH(NOLOCK) left join fdb.tblcompositedrug NC WITH(NOLOCK)
	on Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') = nc.MED_ROUTED_DF_MED_ID_DESC
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE_Reason')) 
Drop View VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE_Reason 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE_Reason]
AS
	SELECT TOP 1 
	'Reason' AS RESOURCETYPE,
'EncounterDiagnosis' AS Type,
LEFT(PC.FINDINGDETAIL,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',SUBSTRING(PC.FINDINGDETAIL, CHARINDEX('.',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',PC.FINDINGDETAIL)))) AS Code,
		Q.REVIEWSYSTEMLINGO AS Description,
	'2.16.840.1.113883.6.103' AS CodeSystem,
		'ICD9CM' AS CodeSystemName,
 PC.CLINICALID As ID,
 PC.AppointmentId AS AppointmentId,
 PC.PatientId As PatientId

	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN DM.PRIMARYDIAGNOSISTABLE Q WITH(NOLOCK)
	ON PC.FINDINGDETAIL = Q.DIAGNOSISNEXTLEVEL
	WHERE PC.CLINICALTYPE IN ('B','K') 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT')) 
Drop View VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT]
AS
	
	SELECT DISTINCT
		'SurgicalPostOperativeResultEvent' AS ResourceType, --R
		'EHR' AS IsSource, --R
		PC.PATIENTID AS PatientAccountNumber, --R
		--CONVERT(VARCHAR(10), AP.APPDATE, 101) as DateofEvent, --R
		SUBSTRING(AP.APPDATE,5,2) + '/' + SUBSTRING(AP.APPDATE,7,2) + '/' + SUBSTRING(AP.APPDATE,1,4) as DateofEvent,
		ISNULL(r.npi,'0000000000') as PerformedPhysicianNPI, --R
		RIGHT('000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''), 1, 9)), 9) AS ECTIN, --R
		PC.APPOINTMENTID AS ExternalId,
		--'67724' AS EpisodeNumber,
		 CASE
			WHEN ((select count(*) from dbo.CataractLog)=1) THEN (Select top (1) AppointmentId from dbo.CataractLog where patientid = PC.PATIENTID)
		ELSE
			(Select top (1) AppointmentId from dbo.CataractLog where patientid = PC.PATIENTID AND (AP.Appdate>Appdate OR AP.Appdate=Appdate)  order by Appdate desc)
		END AS EpisodeNumber,
		PC.APPOINTMENTID AS ReferenceNumber,
		PC.CLINICALID,
		'TRUE' AS IsInHousePhysician,
		r.npi as ECNPI, --R
		R.RESOURCEID AS PerformedPhysicianAccountNumber,
		R.RESOURCENAME AS PerformedPhysicianName,
		--R.ResourceType As ResourceType,
		'NOT AVAilable' AS ReferralOrderNumber,
		ap.AppointmentId,
		ap.patientid,
		BS.Id AS Id
	from model.billingservices BS WITH(NOLOCK) JOIN model.encounterservices ES WITH(NOLOCK) ON ES.ID =BS.EncounterServiceId
		JOIN model.Invoices Inv ON Inv.Id = BS.InvoiceId
		JOIN dbo.Appointments Ap WITH(NOLOCK) ON AP.AppointmentId = Inv.EncounterId
		JOIN dbo.Resources R WITH(NOLOCK) ON R.ResourceId = Ap.ResourceId1
		JOIN dbo.PatientClinical PC WITH(NOLOCK) ON Pc.AppointmentID = Ap.AppointmentId
		JOIN dbo.VIEW_ACI_JSON25_JSON26_JSON27_EPISODENUMBER En on Ap.AppointmentId= En.AppointmentId
	WHERE ES.CODE = '99024' AND AP.AppTypeId >0 And Ap.ActivityStatus = 'D' and ScheduleStatus = 'D'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_Procedure')) 
Drop View VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_Procedure 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_Procedure]

AS

	SELECT DISTINCT
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		SUBSTRING(PC.FINDINGDETAIL, CHARINDEX(':',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX(':',PC.FINDINGDETAIL)+1) AS Code,
		ES.DESCRIPTION AS Description,
		'CPT' AS CodeSystemName,
		'2.16.840.1.113883.6.12' AS CodeSystem,
		BS.Id As Id
	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN MODEL.ENCOUNTERSERVICES ES WITH(NOLOCK)
	ON SUBSTRING(PC.FINDINGDETAIL, CHARINDEX(':',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX(':',PC.FINDINGDETAIL)+1) = ES.CODE
	LEFT JOIN Model.Invoices Inv
	ON PC.AppointmentId= Inv.EncounterId
	Left Join model.BillingServices BS ON
	BS.EncounterServiceId =ES.ID and Inv.Id = BS.InvoiceId
	WHERE PC.CLINICALTYPE = 'P' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Diagnosis')) 
Drop View VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Diagnosis 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Diagnosis]

AS
	SELECT 
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		LEFT(PC.FINDINGDETAIL,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',SUBSTRING(PC.FINDINGDETAIL, CHARINDEX('.',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',PC.FINDINGDETAIL)))) AS Code,
		Q.REVIEWSYSTEMLINGO AS Description,
		'2.16.840.1.113883.6.103' AS CodeSystem,
		'ICD9CM' AS CodeSystemName
	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN DM.PRIMARYDIAGNOSISTABLE Q WITH(NOLOCK)
	ON PC.FINDINGDETAIL = Q.DIAGNOSISNEXTLEVEL
	WHERE PC.CLINICALTYPE IN ('B','K')
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_ExamField')) 
Drop View VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_ExamField 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE VIEW [dbo].[VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_ExamField]

AS
	SELECT DISTINCT
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		CASE 
			WHEN Q.PROCORTEST = 'T' THEN 'TEST'
			WHEN Q.PROCORTEST = 'P' THEN 'PROCEDURE'
			ELSE 'NOT DEFINED'
		END  AS Type, --R
		CASE
			WHEN Q.CONTROLNAME IS NULL THEN 'NOT DEFINED'
			ELSE  Q.ControlName
		END AS Code,
		
		CASE
			WHEN Q.CONTROLNAME IS NULL THEN 'NOT DEFINED'
			ELSE Q.QUESTION
		END AS Description,
		'CPT' AS CodeSystemName,
		'2.16.840.1.113883.6.12' AS CodeSystem

	FROM
DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN DF.QUESTIONS Q WITH(NOLOCK)
	ON RIGHT(PC.SYMPTOM, LEN(PC.SYMPTOM)-1) = Q.QUESTION and Q.controlname <> ''
	WHERE PC.CLINICALTYPE = 'F' and Q.PROCORTEST in ('T','P')  
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'ACI_IOP')) 
Drop View ACI_IOP 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW ACI_IOP
AS
select 
pc.appointmentid, pc.patientid,
case
when (select substring(a.findingdetail,1,3) from dbo.patientclinical a (NOLOCK) where a.clinicalid = (pc.clinicalid-1)) = '*OS' THEN 'OS:'+substring(pc.findingdetail,charindex('<',pc.findingdetail)+1,charindex('>',pc.findingdetail)-charindex('<',pc.findingdetail)-1)
else 'IGNORE'
end as IOP
from dbo.patientclinical pc (NOLOCK)
where pc.symptom like '%/iop%' and pc.findingdetail like '%*iop%'

UNION

select 
pc.appointmentid, pc.patientid,
case
when (select substring(a.findingdetail,1,3) from dbo.patientclinical a (NOLOCK) where a.clinicalid = (pc.clinicalid-1)) = '*OD' THEN 'OD:'+substring(pc.findingdetail,charindex('<',pc.findingdetail)+1,charindex('>',pc.findingdetail)-charindex('<',pc.findingdetail)-1)
else 'IGNORE'
end as IOP
from dbo.patientclinical pc (NOLOCK)
where pc.symptom like '%/iop%' and pc.findingdetail like '%*iop%'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMG')) 
Drop View VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMG 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMG]
AS
	SELECT
		'LabRadiologyOrder' AS RESOURCETYPE,
		'EHR' AS IsSource,
		CAST(OrderID AS VARCHAR(10)) AS ReferenceNumber,
		AP.APPOINTMENTID AS EXTERNALID,
		PL.PATIENTID AS PATIENTACCOUNTNUMBER,		
		ORDERID
	FROM PATIENTIMAGEORDERS PL
	JOIN DBO.Appointments AP WITH(NOLOCK) ON AP.AppointmentID=PL.AppointmentId
	Where PL.CPTCODE<>''
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Medication')) 
Drop View VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Medication 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE VIEW [dbo].[VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Medication]

AS
	SELECT 
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		CASE
                WHEN NC.MEDID <> ''
                THEN 'UNCODIFIED'
                ELSE CAST(NC.MEDID AS VARCHAR(15))
        END AS Code, 

		CASE WHEN pc.FindingDetail like 'RX-8/%-2/%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') 
			ELSE '' 
		END  AS Description, 
		'2.16.840.1.113883.6.88' AS CodeSystem,
		'RxNorm' AS CodeSystemName
	FROM
	 dbo.patientclinical pc WITH(NOLOCK) left join fdb.tblcompositedrug nc WITH(NOLOCK)
	on Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') = nc.MED_ROUTED_DF_MED_ID_DESC
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_LAB_ORDERCODE')) 
Drop View VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_LAB_ORDERCODE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_LAB_ORDERCODE]
AS

	SELECT
		PLO.ORDERTYPE AS OrderType,
		PLO.LOINCNUMBER AS Code,
		PLO.PATIENTID AS PATIENTACCOUNTNUMBER,
		PLO.ORDERID AS EXTERNALID,
		L.LONG_COMMON_NAME AS Description,
		'' AS CodeSystemName,
		'' AS CodeSystem,
		 (CONVERT(VARCHAR(20), PLO.OrderDate, 101)+' '+(CASE
                                                                WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PLO.OrderDate, 22), 11))) = 10
                                                                THEN '0'+LTRIM(RIGHT(CONVERT(CHAR(20), PLO.OrderDate, 22), 11))
                                                                ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PLO.OrderDate, 22), 11))
                                                            END)) AS Date,
		'TRUE' IsCPOE,
		r.npi as OrderingECNPI, --R
		RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''), 1, 9)), 9) AS OrderingECTIN,
		ORDERID		
	FROM 
	DBO.PATIENTLABORDERS PLO WITH(NOLOCK) LEFT JOIN DBO.LOINC_2017 L WITH(NOLOCK)
	ON PLO.LOINCNUMBER = L.LOINC_NUM

	LEFT JOIN DBO.APPOINTMENTS AP WITH(NOLOCK)
	ON PLO.APPID = AP.APPOINTMENTID

	left join dbo.resources r WITH(NOLOCK)
	on ap.resourceid1 = r.resourceid
	Where PLO.LOINCNUMBER<>''
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Reason')) 
Drop View VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Reason 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Reason]

AS
	SELECT TOP 1 
	'Reason' AS RESOURCETYPE,
'EncounterDiagnosis' AS Type,
LEFT(PC.FINDINGDETAIL,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',SUBSTRING(PC.FINDINGDETAIL, CHARINDEX('.',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',PC.FINDINGDETAIL)))) AS Code,
		Q.REVIEWSYSTEMLINGO AS Description,
	'2.16.840.1.113883.6.103' AS CodeSystem,
		'ICD9CM' AS CodeSystemName,
 PC.CLINICALID As ID,
 PC.AppointmentId AS AppointmentId,
 PC.PatientId As PatientId

	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN DM.PRIMARYDIAGNOSISTABLE Q WITH(NOLOCK)
	ON PC.FINDINGDETAIL = Q.DIAGNOSISNEXTLEVEL
	WHERE PC.CLINICALTYPE IN ('B','K') 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_INTERVENTION_REASON')) 
Drop View VIEW_ACI_INTERVENTION_REASON 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW VIEW_ACI_INTERVENTION_REASON
AS

     SELECT DISTINCT 'Reason' AS RESOURCETYPE,
			'Medical' AS Type,
            '183932001' AS Code, --R 
            'Procedure contraindicated (situation)' AS Description, --R 
            'SNOMED CT' as CodeSystemName, 
			'2.16.840.1.113883.6.96' as CodeSystem,
            PC.AppointmentId AS CHARTRECORDKEY,
			PC.PatientId,
			'BMI' AS TableName,
		  PC.AppointmentId as TableKey
     FROM dbo.patientclinical PC(NOLOCK) 
	 WHERE SYMPTOM LIKE '/HEIGHT AND WEIGHT' AND FINDINGDETAIL LIKE '*Medical%'

     UNION
     SELECT DISTINCT 'Reason' AS RESOURCETYPE,
			'Medical' AS Type,
            '183932001' AS Code, --R 
            'Procedure contraindicated (situation)' AS Description, --R 
            'SNOMED CT' as CodeSystemName, 
			'2.16.840.1.113883.6.96' as CodeSystem,
            PC.AppointmentId AS CHARTRECORDKEY,
			PC.PatientId,
			'SMOKING' AS TableName,
		  PC.AppointmentId as TableKey
     FROM dbo.patientclinical PC(NOLOCK) 
	 WHERE SYMPTOM LIKE '/SMOKING' AND FINDINGDETAIL LIKE '*Contraindicated%'
 

Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMG_ORDERCODE')) 
Drop View VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMG_ORDERCODE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMG_ORDERCODE]
AS
	SELECT
		'Radiology' AS OrderType,
		PIO.CPTCODE AS Code,
		PIO.PATIENTID AS PATIENTACCOUNTNUMBER,
		PIO.ORDERID AS EXTERNALID,
		ES.DESCRIPTION AS Description,
		'' AS CodeSystemName,
		'' AS CodeSystem,
		(CONVERT(VARCHAR(20), PIO.OrderDate, 101)+' '+(CASE
                                                                WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PIO.OrderDate, 22), 11))) = 10
                                                                THEN '0'+LTRIM(RIGHT(CONVERT(CHAR(20), PIO.OrderDate, 22), 11))
                                                                ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PIO.OrderDate, 22), 11))
                                                            END)) AS Date,
		'TRUE' IsCPOE,
		r.npi as OrderingECNPI, --R
		RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''), 1, 9)), 9) AS OrderingECTIN,
		ORDERID	
	FROM 
	DBO.PATIENTIMAGEORDERS PIO WITH(NOLOCK) LEFT JOIN MODEL.ENCOUNTERSERVICES ES WITH(NOLOCK)
	ON PIO.CPTCODE = ES.CODE

	LEFT JOIN DBO.APPOINTMENTS AP WITH(NOLOCK)
	ON PIO.APPOINTMENTID = AP.APPOINTMENTID

	left join dbo.resources r WITH(NOLOCK)
	on ap.resourceid1 = r.resourceid
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT')) 
Drop View VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE VIEW [dbo].[VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT]

AS
	
	SELECT DISTINCT
		'PostSurgicalComplicationEvent' AS ResourceType, --R
		'EHR' AS IsSource, --R
		PC.PATIENTID AS PatientAccountNumber, --R
		SUBSTRING(AP.APPDATE,5,2) + '/' + SUBSTRING(AP.APPDATE,7,2) + '/' + SUBSTRING(AP.APPDATE,1,4) AS DateofEvent, --R
		--case when  AP.APPDATE is null then '01/01/1900 01:00:00 AM' else
		--(CONVERT(VARCHAR(20), AP.APPDATE, 101)+' '+(CASE
  --                                                                WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), AP.APPDATE, 22), 11))) = 10
  --                                                                THEN '0'+LTRIM(RIGHT(CONVERT(CHAR(20), AP.APPDATE, 22), 11))
  --                                                                ELSE LTRIM(RIGHT(CONVERT(CHAR(20), AP.APPDATE, 22), 11))
  --                                                            END)) END AS DateofEvent,
		ISNULL(r.npi,'') as PerformedPhysicianNPI, --R
		RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''), 1, 9)), 9) AS ECTIN, --R
		PC.APPOINTMENTID AS ExternalId,
		CASE
			WHEN ((select count(*) from dbo.CataractLog)=1) THEN (Select top (1) AppointmentId from dbo.CataractLog where patientid = PC.PATIENTID)
		ELSE
			(Select top (1) AppointmentId from dbo.CataractLog where patientid = PC.PATIENTID AND (AP.Appdate>Appdate OR AP.Appdate=Appdate)  order by Appdate desc)
		END AS EpisodeNumber,
		'TRUE' AS IsInHousePhysician,
		ISNULL(r.npi,'') as ECNPI, --R
		R.RESOURCEID AS PerformedPhysicianAccountNumber,
		R.RESOURCENAME AS PerformedPhysicianName,
		--R.ResourceType As ResourceType,
		PC.APPOINTMENTID AS ReferenceNumber

	from model.servicemodifiers SM WITH(NOLOCK) JOIN model.BillingServiceModifiers BSM WITH(NOLOCK) ON BSM.ServiceModifierId =SM.Id
		JOIN model.billingservices BS WITH(NOLOCK) ON BS.Id=BSM.BillingServiceId
		JOIN model.encounterservices ES WITH(NOLOCK) ON ES.ID =BS.EncounterServiceId
		JOIN model.Invoices Inv ON Inv.Id = BS.InvoiceId
		JOIN dbo.Appointments Ap WITH(NOLOCK) ON AP.AppointmentId = Inv.EncounterId
		JOIN dbo.Resources R WITH(NOLOCK) ON R.ResourceId = Ap.ResourceId1
		JOIN dbo.PatientClinical PC WITH(NOLOCK) ON Pc.AppointmentID = Ap.AppointmentId
		JOIN dbo.VIEW_ACI_JSON25_JSON26_JSON27_EPISODENUMBER En on Ap.AppointmentId= En.AppointmentId
	WHERE SM.Code = '78' AND ES.CODE IN ('66982','66840','66984','66983') AND AP.AppTypeId >0 And Ap.ActivityStatus = 'D' and ScheduleStatus = 'D' AND PC.SYMPTOM = '/CATARACT SURGERY, COMPLEX'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMAGE_RADIOLOGYORDER')) 
Drop View VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMAGE_RADIOLOGYORDER 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMAGE_RADIOLOGYORDER]
AS
    SELECT 
	    ES.Code as Code,
        ES.Description Description,
		'CPT' AS CodeSystemName,
		'2.16.840.1.113883.6.1' AS CodeSystem,
        PIO.OrderID,
        PIO.AppointmentId
	from 
    DBO.PATIENTIMAGEORDERS PIO WITH(NOLOCK) 
	LEFT JOIN MODEL.ENCOUNTERSERVICES ES WITH(NOLOCK) ON ES.Code=  PIO.CPTCODE
	LEFT JOIN DBO.APPOINTMENTS AP WITH(NOLOCK) ON PIO.AppointmentId = AP.APPOINTMENTID
	left join dbo.resources r WITH(NOLOCK) on ap.resourceid1 = r.resourceid
	Where PIO.CptCode<>''
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure')) 
Drop View VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure]

AS

	SELECT 
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		SUBSTRING(PC.FINDINGDETAIL, CHARINDEX(':',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX(':',PC.FINDINGDETAIL)+1) AS Code,
		ES.DESCRIPTION AS Description,
		'CPT' AS CodeSystemName,
		'2.16.840.1.113883.6.12' AS CodeSystem,
		BS.Id As Id
	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN MODEL.ENCOUNTERSERVICES ES WITH(NOLOCK)
	ON SUBSTRING(PC.FINDINGDETAIL, CHARINDEX(':',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX(':',PC.FINDINGDETAIL)+1) = ES.CODE
	LEFT JOIN Model.Invoices Inv
	ON PC.AppointmentId= Inv.EncounterId
	Left Join model.BillingServices BS ON
	BS.EncounterServiceId =ES.ID and Inv.Id = BS.InvoiceId
	WHERE PC.CLINICALTYPE = 'P' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure_Diagnosis')) 
Drop View VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure_Diagnosis 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure_Diagnosis]

AS
	SELECT 
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		LEFT(PC.FINDINGDETAIL,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',SUBSTRING(PC.FINDINGDETAIL, CHARINDEX('.',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',PC.FINDINGDETAIL)))) AS Code,
		Q.REVIEWSYSTEMLINGO AS Description,
		'2.16.840.1.113883.6.103' AS CodeSystem,
		'ICD9CM' AS CodeSystemName
	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN DM.PRIMARYDIAGNOSISTABLE Q WITH(NOLOCK)
	ON PC.FINDINGDETAIL = Q.DIAGNOSISNEXTLEVEL
	WHERE PC.CLINICALTYPE IN ('B','K')
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure_Medication')) 
Drop View VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure_Medication 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure_Medication]

AS
	SELECT 
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		CASE
                WHEN NC.MEDID <> ''
                THEN 'UNCODIFIED'
                ELSE CAST(NC.MEDID AS VARCHAR(15))
        END AS Code, 

		CASE WHEN pc.FindingDetail like 'RX-8/%-2/%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') 
			ELSE '' 
		END  AS Description, 
		'2.16.840.1.113883.6.88' AS CodeSystem,
		'RxNorm' AS CodeSystemName
	FROM
   dbo.patientclinical pc WITH(NOLOCK) left join fdb.tblcompositedrug nc WITH(NOLOCK)
	on Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') = nc.MED_ROUTED_DF_MED_ID_DESC
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure_Reason')) 
Drop View VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure_Reason 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure_Reason]

AS
 
	SELECT TOP 1 
	'Reason' AS RESOURCETYPE,
'EncounterDiagnosis' AS Type,
LEFT(PC.FINDINGDETAIL,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',SUBSTRING(PC.FINDINGDETAIL, CHARINDEX('.',PC.FINDINGDETAIL)+1,LEN(PC.FINDINGDETAIL)-CHARINDEX('.',PC.FINDINGDETAIL)))) AS Code,
		Q.REVIEWSYSTEMLINGO AS Description,
	'2.16.840.1.113883.6.103' AS CodeSystem,
		'ICD9CM' AS CodeSystemName,
 PC.CLINICALID As ID,
 PC.AppointmentId AS AppointmentId,
 PC.PatientId As PatientId

	FROM
	DBO.PATIENTCLINICAL PC WITH(NOLOCK) LEFT JOIN DM.PRIMARYDIAGNOSISTABLE Q WITH(NOLOCK)
	ON PC.FINDINGDETAIL = Q.DIAGNOSISNEXTLEVEL
	WHERE PC.CLINICALTYPE IN ('B','K') 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON28_RESULTS')) 
Drop View VIEW_ACI_JSON28_RESULTS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON28_RESULTS]

AS
	SELECT
		'Result' AS RESOURCETYPE, --R
		'EHR' AS IsSource, --R
		'NOT AVAILABLE' AS LabName,
		Convert(Varchar(20),PLTR.Id) AS ReferenceNumber, --R
		Convert(Varchar(20),PLTR.Id) AS ExternalId, --R
		PLO.OrderType AS Type, --R
		PLO.LoincNumber AS Code, --R
		'2.16.840.1.113883.6.1'AS CodeSystem, --R
		'LOINC' AS CodeSystemName, --R
		PLTR.RESULT AS Value, --C
		REPLACE(REPLACE(CAST(PLTR.UNIT AS VARCHAR(50)),']','@@'),'[','##') AS Units, --C
		PLTR.MINRANGE +'-'+PLTR.MAXRANGE  AS ReferenceRange, --C
		NULL AS Interpretation, --C
		PLTR.PATIENTID AS PatientAccountNumber, --R
		SUBSTRING(AP.APPDATE,5,2) + '/' + SUBSTRING(AP.APPDATE,7,2) + '/' + SUBSTRING(AP.APPDATE,1,4)+ ' 12:00:00 AM' AS Date, --R
		ISNULL(r.npi,'0000000000') AS ECNPI, --R
		RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''), 1, 9)), 9) AS ECTIN, --R
		PLTR.Id AS Id,
		Convert (VARCHAR(MAX),LOC.LONG_COMMON_NAME) As TestName,
		CASE
			WHEN PLO.STATUS =0 THEN 'FALSE'
			ELSE 'TRUE'
		END AS ISACTIVE
	FROM
	DBO.PATIENTLABTESTRESULTS PLTR (NOLOCK) LEFT JOIN DBO.PATIENTLABORDERS PLO (NOLOCK)
	ON PLTR.LABORDERID = PLO.ORDERID and PLTR.result <> '' --and PLTR.result <> NULL
	left join [dbo].[LOINC_2017] LOC ON
    LOC.LOINC_NUM =PLO.LoincNumber

	LEFT JOIN DBO.APPOINTMENTS AP (NOLOCK)
	ON PLTR.APPOINTMENTID = AP.APPOINTMENTID

	left join dbo.resources r (NOLOCK)
	on ap.resourceid1 = r.resourceid

	where PLTR.result <> ''-- and pltr.id = 60543
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON28_RESULTS_LABADDRESS')) 
Drop View VIEW_ACI_JSON28_RESULTS_LABADDRESS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE View [dbo].[VIEW_ACI_JSON28_RESULTS_LABADDRESS]

As 

SELECT 
       'LabAddress' AS RESOURCETYPE,
		'NA' AS AddressLine1, --R
		'NA' AS AddressLine2, --o
		'NOT AVAILABLE' AS City, --R
		'NOT AVAILABLE' AS State, --R
		'NA' AS Zip, --R
		'NA' AS County, --o
		'NA' AS CountyCode,
		PLTR.AppointmentId AS APPOINTMENTID,
		PLTR.PatientId AS PATIENTID,
		PLTR.Id as ID

From  DBO.PATIENTLABTESTRESULTS PLTR join
DBO.PATIENTLABORDERS PLO ON
PLTR.LABORDERID = PLO.ORDERID
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON28_RESULTS_LABPHONE')) 
Drop View VIEW_ACI_JSON28_RESULTS_LABPHONE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE View [dbo].[VIEW_ACI_JSON28_RESULTS_LABPHONE]

As 

SELECT 
'LabPhone' AS RESOURCETYPE,
'NOT AVAILABLE' AS LabName, --R
		'WORK'  AS Type , --R
		'NOT AVAILABLE' AS Number, --R
		PL.AppointmentId AS APPOINTMENTID,
		PL.PatientId AS PATIENTID ,
		PL.ID AS ID
     From DBO.PATIENTLABTESTRESULTS  PL
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION')) 
Drop View VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION]
AS     SELECT 
	 
		''PATIENT'' AS RESOURCETYPE, --R
		''EHR'' ISSOURCE, --R
		CASE
		WHEN MP.PatientStatusId = 1 THEN ''TRUE''
		ELSE ''FALSE''
		END AS ACTIVE, --R 

		CAST(MP.ID AS NVARCHAR(20)) AS EXTERNALID,--R
		CAST(MP.ID AS VARCHAR(10)) AS REFERENCENUMBER, --R
		CAST(MP.ID AS NVARCHAR(20)) AS PATIENTACCOUNTNUMBER,--R
		MP.FIRSTNAME AS GIVENNAME,--R
		MP.MIDDLENAME AS SECONDANDFURTHERGIVENNAMES,--O
		MP.LASTNAME AS FAMILYNAME,--R
		MP.PREFIX AS NAMEPREFIX,--O
		MP.SUFFIX AS NAMESUFFIX,--O
		''L'' AS NAMETYPECODE, --R
		''LEGAL NAME'' AS NAMETYPEDESCRIPTION, --R
		ISNULL(CONVERT(VARCHAR(20), MP.DATEOFBIRTH, 101),''01/01/1900'') AS DATEOFBIRTH, --C
		''FALSE'' AS MULTIPLEBIRTHINDICATOR,  --MULTIPLE BIRTH INDICATOR MEANS THAT TWINS ARE BORN AT TIME OF DELIVERY.
		''0'' AS BIRTHORDER, --C

		CAST(ISNULL(CAST(ET.CODE AS VARCHAR(10)),''UNK'') AS VARCHAR(10)) AS ETHNICITYCODE, --R
		CAST(ISNULL(ET.NAME,''Declined to Specify'') AS VARCHAR(50)) AS ETHNICITYDECRIPTION,
		''2.16.840.1.113883.6.238'' AS ETHNICITYCODESYSTEM, --R
		''CDC - Race and Ethnicity'' AS ETHINITYCODESYSTEMNAME, --R

		CASE
		   WHEN (MP.SEXID='''') OR (MP.SEXID IS NULL) THEN ''UNK''
		   WHEN (MP.SEXID=1) THEN ''M''
		   WHEN (MP.SEXID=2) THEN ''F''
		   ELSE ''UNK''
		END AS SEXCODE, --R
		ISNULL(STE.TYPE,''UNKNOWN'') AS SEXDESCRIPTION, --R
		
		--CASE
		--   WHEN (MP.SEXID='''') OR (MP.SEXID IS NULL) THEN ''Unknown''
		--   WHEN (STE.TYPE='''') OR (STE.TYPE IS NULL) THEN ''NA''
		--   ELSE STE.TYPE 
		--END AS SEXDESCRIPTION, --R
		''ADMINSTRATIVEGENDER'' AS SEXCODESYSTEMNAME, --R
		''2.16.840.1.113883.5.1'' AS SEXCODESYTEM, --R

		ISNULL(GENE.HL7CODE,''ASKU'') AS GENDERIDENTITYCODE,--O
		ISNULL(GENE.NAME,''Choose not to disclose'') AS GENDERIDENTITYDESCRIPTION,--O
		--CASE
		--   WHEN (MP.SEXID='''') OR (MP.SEXID IS NULL) THEN ''UNK''
		--   WHEN (MP.SEXID=1) THEN ''M''
		--   WHEN (MP.SEXID=2) THEN ''F''
		--   ELSE ''UNK''
		--END AS GENDERIDENTITYCODE, --R
		--ISNULL(STE.TYPE,''UNKNOWN'') AS GENDERIDENTITYDESCRIPTION, --R

		''ADMINSTRATIVEGENDER'' AS GENDERIDENTITYCODESYSTEMNAME,--O
		''2.16.840.1.113883.5.1'' AS GENDERIDENTITYCODESYSTEM,--O


		 ISNULL(MP.SEXUALORIENTATIONID,'''') AS SEXUALORIENTATIONCODE,--O
		 ISNULL(SOE.DESCRIPTION,'''') AS SEXUALORIENTATIONDESCRIPTION,--O
		 ''PHVS_SEXUALORIENTATION_CDC'' AS SEXUALORIENTATIONCODESYSTEMNAME,--O
		 ''2.16.840.1.114222.4.11.7563'' AS SEXUALORIENTATIONCODESYSTEM,--O

		MP.MARITALSTATUSID AS MARITALSTATUSCODE,--O
		MMSE.NAME AS MARITALSTATUSDESCRIPTION,--O
		''2.16.840.1.113883.5.2'' AS MARITALSTATUSCODESYSTEM,--O
		''MARITALSTATUSCODE'' AS MARITALSTATUSCODESYSTEMNAME,--O

		MP.PATIENTRELIGIONID AS RELIGIOUSAFFILIATIONCODE,--O THIS IS NOT SAVING IN DB
		'''' AS RELIGIOUSAFFILIATIONDESC,--O THIS IS NOT SAVING IN DB
		''HL7 RELIGIOUS AFFILIATION'' AS RELIGIOUSAFFILIATIONCODESYSTEMNAME,--O
		''2.16.840.1.113883.5.1076'' AS RELIGIOUSAFFILIATIONCODESYSTEM,--O


		'''' AS MOTHERSMAIDENNAME, --C
		CASE
		WHEN MP.PatientStatusId = 3 THEN ''TRUE''
		ELSE ''FALSE''
		END AS PATIENTDEATHINDICATOR, --C
		'''' AS PATIENTDEATHDATEANDTIME, --C

		''2.16.840.1.113883.5.60'' AS LANGUAGEABILITYMODE, --R 
		''LanguageAbilityMode'' AS LANGUAGEABILITYMODENAME, --R 
		''2.16.840.1.113883.5.60'' AS LANGUAGEABILITYMODESYSTEM, --R
		''LANGUAGEABILITYMODE'' LANGUAGEABILITYMODEYSTEMNAME, --R

		MP.ID AS ID

	FROM
	MODEL.PATIENTS MP WITH(NOLOCK) 
	LEFT JOIN  [MODEL].[SEXTYPE_ENUMERATION] STE WITH(NOLOCK) ON MP.SEXID =STE.ID
	LEFT JOIN [MODEL].[GENDER_ENUMERATION] GENE WITH(NOLOCK) ON MP.GENDERID = GENE.ID
	LEFT JOIN [MODEL].[SEXUALORIENTATIONTYPE_ENUMERATION] SOE WITH(NOLOCK) ON MP.SEXUALORIENTATIONID = SOE.ID
	LEFT JOIN [MODEL].[MARITALSTATUS_ENUMERATION] MMSE WITH(NOLOCK) ON MP.MARITALSTATUSID = MMSE.ID
	LEFT JOIN [MODEL].[PATIENTETHNICITIES] PE WITH(NOLOCK) ON PE.PATIENTID=MP.ID
     LEFT JOIN [MODEL].[ETHNICITIES] ET WITH(NOLOCK) ON ET.ID=PE.ETHNICITYID 
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_ADDRESS')) 
Drop View VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_ADDRESS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_ADDRESS]
AS

     SELECT ''ADDRESS'' AS RESOURCETYPE,
			
			CASE
			WHEN PA.PatientAddressTypeId =1 THEN ''HOME''
			ELSE ''PHYSICAL VISIT ADDRESS''
			END AS Type,
			PA.LINE1 AS ADDRESSLINE1,
            PA.LINE2 AS ADDRESSLINE2,
			PA.CITY AS CITY,
			SOP.Abbreviation AS STATE,
			PA.POSTALCODE AS ZIP,
            '''' AS COUNTY,
            '''' AS COUNTYCODE,
            ''USA'' AS COUNTRY,
           P.ID AS ID 
                FROM MODEL.PATIENTS P (NOLOCK) LEFT JOIN MODEL.[PATIENTADDRESSES] PA WITH(NOLOCK) ON PA.PatientId = P.id
				LEFT JOIN [MODEL].[STATEORPROVINCES] SOP WITH(NOLOCK) ON PA.STATEORPROVINCEID =SOP.ID
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_BIRTHPLACE')) 
Drop View VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_BIRTHPLACE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_BIRTHPLACE]
AS
     SELECT 'BIRTHPLACE' AS RESOURCETYPE,
			PA.CITY AS CITY,
            SOP.Abbreviation AS STATE,
            PA.POSTALCODE AS ZIP,
            'USA' AS COUNTRY,
			PEA.VALUE AS EMAIL,
			P.ID as ID           
     FROM MODEL.PATIENTS P (NOLOCK) LEFT JOIN MODEL.[PATIENTADDRESSES] PA WITH(NOLOCK) ON PA.PatientId= P.Id
	 LEFT JOIN [MODEL].[STATEORPROVINCES] SOP WITH(NOLOCK) ON PA.STATEORPROVINCEID =SOP.ID
	 LEFT JOIN [MODEL].[PATIENTEMAILADDRESSES] PEA WITH(NOLOCK) ON PA.PATIENTID = PEA.PATIENTID
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_PHONE')) 
Drop View VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_PHONE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_PHONE]
AS

		SELECT 
			'PHONE' AS RESOURCETYPE,
            'PRIMARY HOME' AS TYPE,
            CASE
				WHEN LEN(HOMEPHONE) <11 THEN '000-000-0000'
                WHEN LEN(HOMEPHONE) >= 14
                THEN(SUBSTRING(HOMEPHONE, 1, 3)+'-'+SUBSTRING(HOMEPHONE, 4, 3)+'-'+SUBSTRING(HOMEPHONE, 8, 4)+'X'+SUBSTRING(HOMEPHONE, 11, 4))
                WHEN (LEN(HOMEPHONE)  >=11) and (LEN(HOMEPHONE) < 14)
                THEN(SUBSTRING(HOMEPHONE, 1, 3)+'-'+SUBSTRING(HOMEPHONE, 4, 3)+'-'+SUBSTRING(HOMEPHONE, 8, 4))
                ELSE '000-000-0000'
            END AS NUMBER,
           PATIENTID AS ID 
            
     FROM DBO.PATIENTDEMOGRAPHICS WITH (NOLOCK)
     WHERE LEN(HOMEPHONE) > 0--O

	 UNION
SELECT 
			'PHONE' AS RESOURCETYPE,
            'WORK PLACE' AS TYPE,
            CASE
				WHEN LEN(BUSINESSPHONE) <11 THEN '000-000-0000'
                WHEN LEN(BUSINESSPHONE) >= 14
                THEN(SUBSTRING(BUSINESSPHONE, 1, 3)+'-'+SUBSTRING(BUSINESSPHONE, 4, 3)+'-'+SUBSTRING(BUSINESSPHONE, 8, 4)+'X'+SUBSTRING(BUSINESSPHONE, 11, 4))
                WHEN (LEN(BUSINESSPHONE)  >=11) and (LEN(BUSINESSPHONE) < 14)
                THEN(SUBSTRING(BUSINESSPHONE, 1, 3)+'-'+SUBSTRING(BUSINESSPHONE, 4, 3)+'-'+SUBSTRING(BUSINESSPHONE, 8, 4))
                ELSE '000-000-0000'

            END AS NUMBER,
           PATIENTID AS ID 
            
     FROM DBO.PATIENTDEMOGRAPHICS WITH (NOLOCK)
     WHERE LEN(BUSINESSPHONE) > 0--O

	 UNION
SELECT 
			'PHONE' AS RESOURCETYPE,
            'MOBILE CONTACT' AS TYPE,
            CASE
				WHEN LEN(CELLPHONE) <11 THEN '000-000-0000'
                WHEN LEN(CELLPHONE) >= 14
                THEN(SUBSTRING(CELLPHONE, 1, 3)+'-'+SUBSTRING(CELLPHONE, 4, 3)+'-'+SUBSTRING(CELLPHONE, 8, 4)+'X'+SUBSTRING(CELLPHONE, 11, 4))
                WHEN (LEN(CELLPHONE)  >=11) and (LEN(CELLPHONE) < 14)
                THEN(SUBSTRING(CELLPHONE, 1, 3)+'-'+SUBSTRING(CELLPHONE, 4, 3)+'-'+SUBSTRING(CELLPHONE, 8, 4))
                ELSE '000-000-0000'
            END AS NUMBER,
        PATIENTID AS ID
     FROM DBO.PATIENTDEMOGRAPHICS WITH (NOLOCK)
     WHERE LEN(CELLPHONE) > 0--O
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_PREFERREDLANGUAGE')) 
Drop View VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_PREFERREDLANGUAGE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_PREFERREDLANGUAGE]
AS
     SELECT 		
		'PREFERREDLANGUAGE' AS RESOURCETYPE, --R
		CAST(ISNULL(LA.Abbreviation,'UNK')AS VARCHAR(10)) AS LANGUAGECODE, --R
		CAST(ISNULL(LA.NAME,'Declined to Specify') AS VARCHAR(50)) AS LANGUAGENAME, --R
		'2.16.840.1.114222.4.11.831' AS LANGUAGECODESYSTEM, --R
		'LANGUAGE (PHVS_LANGUAGE_ISO_639-2_ALPHA3)' AS LANGUAGECODESYSTEMNAME, --R
		P.ID AS ID
	FROM MODEL.PATIENTS P(NOLOCK) left join [MODEL].[PATIENTLANGUAGES] PL ON PL.PatientId = P.Id
	LEFT JOIN [MODEL].[LANGUAGES] LA ON PL.LANGUAGEID =LA.ID
 Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON9_PORTALACCOUNTADMINISTRATION')) 
Drop View VIEW_ACI_JSON9_PORTALACCOUNTADMINISTRATION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_JSON9_PORTALACCOUNTADMINISTRATION]
AS

SELECT 

		CAST (PQR.Patientno AS NVARCHAR(20)) as ExternalId, --R
		'EHR' [IsSource], --R
		CAST(RESPONSEID AS VARCHAR(20)) AS ReferenceNumber, --R
		CAST (PQR.Patientno AS NVARCHAR(50)) as PatientAccountNumber, --R
		'PATIENT' AS PortalType, --R
		CAST(PQR.ExternalId AS NVARCHAR(50)) AS PortalAccountNumber,
		--CAST(PQR.ExternalId AS NVARCHAR(50)) AS PortalAccountNumber, --C
		--(CONVERT(VARCHAR(20), PQR.processedDate,101))  AS ACCOUNTCREATEDDATE, --R
		CASE WHEN  PQR.processedDate is null then '01/01/1900 01:00:00 AM' else
		(CONVERT(VARCHAR(20), PQR.processedDate, 101)+' '+(CASE
                                                                  WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PQR.processedDate, 22), 11))) = 10
                                                                  THEN '0'+LTRIM(RIGHT(CONVERT(CHAR(20), PQR.processedDate, 22), 11))
                                                                  ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PQR.processedDate, 22), 11))
                                                              END)) END AS ACCOUNTCREATEDDATE,		
		CASE WHEN  PQR.processedDate is null then '01/01/1900 01:00:00 AM' else
		(CONVERT(VARCHAR(20), PQR.processedDate, 101)+' '+(CASE
                                                                  WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PQR.processedDate, 22), 11))) = 10
                                                                  THEN '0'+LTRIM(RIGHT(CONVERT(CHAR(20), PQR.processedDate, 22), 11))
                                                                  ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PQR.processedDate, 22), 11))
                                                              END)) END AS AccountInformationProvidedDate, --R
		 '12/31/2050 12:00:00 AM' AS AccountDisabledDate, --C
		 cast(pr.[PatientRepresentativeId] AS nvarchar(20)) AS RepresentativeExternalId --C

from mve.PP_PortalQueueResponse PQR WITH (NOLOCK) LEFT JOIN model.patientrepresentatives pr WITH (NOLOCK)
ON PQR.PATIENTNO = PR.PATIENTID 
where actiontypelookupid = 97
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PROCEDURES_TARGET')) 
Drop View VIEW_ACI_PROCEDURES_TARGET 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_PROCEDURES_TARGET]
AS
     SELECT 'TARGETSITE' AS RESOURCETYPE,
            '9454009' AS CODE, --O 
            'Structure of subclavian vein' AS NAME, --O 
			'SNOMED-CT' AS CodeSystemName,
			'2.16.840.1.113883.6.96' AS CodeSystem,

		pc.appointmentid as ChartRecordKey,
		pc.patientid,
		PC.id as COLLECTEDBILLINGKEY,pc.id
		from [model].[ProceduresDetail](nolock) pc
		where pc.ConceptID = 175135009
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PATIENT_PEXAM')) 
Drop View VIEW_ACI_PATIENT_PEXAM 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[VIEW_ACI_PATIENT_PEXAM]
AS
	SELECT
		''PATIENTEXAM'' AS RESOURCETYPE,
		''EHR'' AS IsSource, --R
		CAST(ap.appointmentid as Varchar(10)) AS ReferenceNumber, --R
		(CONVERT(VARCHAR(20), CONVERT(DATE,ap.AppDate), 101)) AS DATE,
		CASE
			WHEN ((select count(*) from dbo.aci_inputlog where Tablekey =ap.appointmentid and sort = 7 ) >1)THEN ''AMENDMENT''
			WHEN(((select count(*) from [model].[PatientReconcileMedications] M1 where M1.appointmentid =ap.appointmentid) >0)or((select count(*) from [model].[PatientReconcileAllergy] A1 where A1.appointmentid =ap.appointmentid) >0)or((select count(*) from [model].[PatientReconcileProblems] P1 where P1.appointmentid =ap.appointmentid) >0)) THEN ''RECONCILEDCHART''
			ELSE ''EXAM''
		END AS Type, --R
		ap.Patientid AS PatientAccountNumber, --R
		ap.appointmentid as EXTERNALID,
		ap.PatientID as PatientKey,
		ap.appointmentid as ChartRecordkey
	from
	dbo.appointments ap WITH(NOLOCK) left join dbo.practiceactivity pa WITH(NOLOCK)
	on ap.appointmentid =pa.appointmentid
	where ap.activitystatus =''D'' and ap.schedulestatus= ''D'' and pa.status =''D'' and ap.apptypeid>0
	--select top 10 * from dbo.appointments



' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_ALLERGIES')) 
Drop View VIEW_ACI_ALLERGIES 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_ALLERGIES]
AS

    SELECT ''ALLERGIES'' AS RESOURCETYPE,
		 RTRIM(LTRIM((CONVERT(VARCHAR(20), CONVERT(DATETIME,RTRIM(LTRIM(ap.AppDate))), 101)+'' ''+(CASE
                                                                WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,RTRIM(LTRIM(ap.AppDate))), 22), 11))) = 10
                                                                THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,RTRIM(LTRIM(ap.AppDate))), 22), 11))
                                                                ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,RTRIM(LTRIM(ap.AppDate))), 22), 11))
                                                            END)))) AS DATE,
		 PC.CLINICALID AS ExternalId, --R
		 CASE
		 WHEN ((Pc.FindingDetail LIKE ''%/%/%'') AND (SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))-CHARINDEX(''/'', pc.findingdetail))) = ''Shortness of Breath'') THEN ''267036007''
		 WHEN (Pc.FindingDetail LIKE ''%/%/%'') AND (SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))-CHARINDEX(''/'', pc.findingdetail)) <> ''NONE'') THEN REPLACE(REPLACE(ISNULL(CAST(ar.Snomed_ConceptId AS VARCHAR(10)),''NONE''),CHAR(13),''''),CHAR(10),'''')
			WHEN (Pc.FindingDetail LIKE ''%/%/%'') AND (SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))-CHARINDEX(''/'', pc.findingdetail)) = ''NONE'') THEN ''NONE''
			
			ELSE ''NONE''
		 END AS Reaction, --R
		 CASE
			WHEN Pc.FindingDetail LIKE ''%/%/%''
			THEN SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))-CHARINDEX(''/'', pc.findingdetail))
			ELSE ''NO THERAPEUTIC EFFECT''
		 END AS ReactionDesc, --R
		 ''2.16.840.1.113883.6.96'' AS ReactionCodeSystem, --R
		 ''SNOMED-CT'' AS ReactionCodeSystemName, --R
		 CASE
			WHEN Pc.FindingDetail LIKE ''%/%/%''
			THEN 
			(CASE SUBSTRING(SUBSTRING(pc.findingdetail, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))+2, CHARINDEX(''/'', REVERSE(pc.findingdetail))-1),1,9)
				 WHEN ''MILD'' THEN 18647004
				 WHEN ''MILD TO MODERATE'' THEN 6736007
				 WHEN ''MODERATE'' THEN 6736007
				 WHEN ''MODERATE TO SEVERE'' THEN 24484000
				 WHEN ''SEVERE'' THEN 24484000
				 ELSE ''00000000''
			 END)
			ELSE ''0000000''
		 END AS SeverityCode, --R
		 CASE
			WHEN Pc.FindingDetail LIKE ''%/%/%''
			THEN SUBSTRING(pc.findingdetail, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))+2, CHARINDEX(''/'', REVERSE(pc.findingdetail))-1)
			ELSE ''NO KNOWN SEVERITY''
		 END AS SeverityDescription, --R
		 ''2.16.840.1.113883.6.96'' AS SeverityCodeSystem, --R
		 ''SNOMED-CT'' AS SeverityCodeSystemName, --R
		 CASE
			WHEN PC.STATUS = ''A''
			THEN ''Active''
			ELSE ''In-Active''
		 END AS Status, --R
		 CASE
			WHEN PC.STATUS = ''A''
			THEN ''True''
			ELSE ''False''
		 END ISACTIVE, --R
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.patientid AS PATIENTKEY,
		 pc.clinicalid as PATIENTALLERGYKEY,
		 CAST(ISNULL(CAST((select top 1 (c.rxnorm) from fdb.tblcompositedrug b  join dbo.fdbtorxnorm1 c on c.fdbid = b.medid where med_name = (LEFT(pc.findingdetail, CHARINDEX(''='', pc.findingdetail)-1))) AS VARCHAR(20)),''00000'')AS VARCHAR(20)) AS Code

    FROM dbo.patientclinical pc
	    LEFT JOIN dbo.appointments ap ON ap.appointmentid = pc.appointmentid
		LEFT JOIN dbo.AllergyReactions ar ON ar.AllergyReaction = SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))-CHARINDEX(''/'', pc.findingdetail))
    WHERE PC.SYMPTOM LIKE ''%ALLERG%'' and pc.findingdetail not like ''%No Known Allerg%'' and pc.Status <> ''D'' and pc.findingdetail not like ''other%''
    --SELECT TOP 10 * FROM dbo.patientclinical pc


UNION


    SELECT ''ALLERGIES'' AS RESOURCETYPE,
		 RTRIM(LTRIM((CONVERT(VARCHAR(20), CONVERT(DATETIME,RTRIM(LTRIM(ap.AppDate))), 101)+'' ''+(CASE
                                                                WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,RTRIM(LTRIM(ap.AppDate))), 22), 11))) = 10
                                                                THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,RTRIM(LTRIM(ap.AppDate))), 22), 11))
                                                                ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,RTRIM(LTRIM(ap.AppDate))), 22), 11))
                                                            END)))) AS DATE,
		 PC.CLINICALID AS ExternalId, --R
		 CASE
		 WHEN (Pc.FindingDetail LIKE ''%/%/%'') AND (SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))-CHARINDEX(''/'', pc.findingdetail)) <> ''NONE'') THEN REPLACE(REPLACE(ISNULL(CAST(ar.Snomed_ConceptId AS VARCHAR(10)),''NONE''),CHAR(13),''''),CHAR(10),'''')
			WHEN (Pc.FindingDetail LIKE ''%/%/%'') AND (SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))-CHARINDEX(''/'', pc.findingdetail)) = ''NONE'') THEN ''NONE''
			
			ELSE ''00000''
		 END AS Reaction, --R
		 CASE
			WHEN Pc.FindingDetail LIKE ''%/%/%''
			THEN SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))-CHARINDEX(''/'', pc.findingdetail))
			ELSE ''NO KNOWN REACTION''
		 END AS ReactionDesc, --R
		 ''2.16.840.1.113883.6.96'' AS ReactionCodeSystem, --R
		 ''SNOMED-CT'' AS ReactionCodeSystemName, --R
		 CASE
			WHEN Pc.FindingDetail LIKE ''%/%/%''
			THEN 
			(CASE SUBSTRING(SUBSTRING(pc.findingdetail, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))+2, CHARINDEX(''/'', REVERSE(pc.findingdetail))-1),1,9)
				 WHEN ''MILD'' THEN 18647004
				 WHEN ''MILD TO MODERATE'' THEN 6736007
				 WHEN ''MODERATE'' THEN 6736007
				 WHEN ''MODERATE TO SEVERE'' THEN 24484000
				 WHEN ''SEVERE'' THEN 24484000
				 ELSE ''00000000''
			 END)
			ELSE ''0000000''
		 END AS SeverityCode, --R
		 CASE
			WHEN Pc.FindingDetail LIKE ''%/%/%''
			THEN SUBSTRING(pc.findingdetail, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))+2, CHARINDEX(''/'', REVERSE(pc.findingdetail))-1)
			ELSE ''NO KNOWN SEVERITY''
		 END AS SeverityDescription, --R
		 ''2.16.840.1.113883.6.96'' AS SeverityCodeSystem, --R
		 ''SNOMED-CT'' AS SeverityCodeSystemName, --R
		 CASE
			WHEN PC.STATUS = ''A''
			THEN ''Active''
			ELSE ''In-Active''
		 END AS Status, --R
		 CASE
			WHEN PC.STATUS = ''A''
			THEN ''True''
			ELSE ''False''
		 END ISACTIVE, --R
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.patientid AS PATIENTKEY,
		 pc.clinicalid as PATIENTALLERGYKEY,
		 ''11111'' AS CODE
    FROM dbo.patientclinical pc
	    LEFT JOIN dbo.appointments ap ON ap.appointmentid = pc.appointmentid
		LEFT JOIN dbo.AllergyReactions ar ON ar.AllergyReaction = SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))-CHARINDEX(''/'', pc.findingdetail))
    WHERE PC.SYMPTOM LIKE ''%ALLERG%'' and pc.findingdetail like ''%No Known Allerg%'' and pc.Status <> ''D''
--    --SELECT TOP 10 * FROM dbo.patientclinical pc
' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_ALLERGIES_ALLERGY')) 
Drop View VIEW_ACI_ALLERGIES_ALLERGY 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_ALLERGIES_ALLERGY]
AS
    SELECT DISTINCT ''ALLERGYCODE'' AS RESOURCETYPE,
		 CAST(ISNULL(CAST((select top 1 (c.rxnorm) from fdb.tblcompositedrug b  join dbo.fdbtorxnorm1 c on c.fdbid = b.medid where med_name = (LEFT(pc.findingdetail, CHARINDEX(''='', pc.findingdetail)-1))) AS VARCHAR(20)),''UNK'')AS VARCHAR(20)) AS Code, --R
		 LEFT(pc.findingdetail, CHARINDEX(''='', pc.findingdetail)-1) AS Description, --R
		 ''RXNORM'' CODESYSTEMNAME,
		 ''2.16.840.1.113883.6.88'' CODESYSTEM,
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.patientid AS PATIENTKEY,
		 pc.clinicalid as PATIENTALLERGYKEY
    FROM dbo.patientclinical pc
	    LEFT JOIN dbo.appointments ap ON ap.appointmentid = pc.appointmentid
	--	LEFT JOIN fdb.tblcompositedrug tc on tc.med_name = LEFT(pc.findingdetail, CHARINDEX(''='', pc.findingdetail)-1)
	--	LEFT JOIN [fdb].[RXCUI2MEDID] rm on rm.medid = tc.medid
    WHERE PC.SYMPTOM LIKE ''%ALLERG%'' and pc.findingdetail not like ''%No Known Aller%'' and pc.Status <> ''D'' and pc.findingdetail not like ''other%''
		
UNION

    SELECT ''ALLERGYCODE'' AS RESOURCETYPE,
		 ''160244002'' AS Code, --R
		 ''No Known Allergy'' AS Description, --R
		 ''SNOMED-CT'' CODESYSTEMNAME,
		 ''2.16.840.1.113883.6.96'' CODESYSTEM,
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.patientid AS PATIENTKEY,
		 pc.clinicalid as PATIENTALLERGYKEY
    FROM dbo.patientclinical pc
	    LEFT JOIN dbo.appointments ap ON ap.appointmentid = pc.appointmentid
		LEFT JOIN fdb.tblcompositedrug tc on tc.med_name = LEFT(pc.findingdetail, CHARINDEX(''='', pc.findingdetail)-1)
		LEFT JOIN [fdb].[RXCUI2MEDID] rm on rm.medid = tc.medid
    WHERE PC.SYMPTOM LIKE ''%ALLERG%'' and pc.findingdetail like ''%No Known Alle%'' and pc.Status <> ''D'';

' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_FAMILYHISTORY')) 
Drop View VIEW_ACI_FAMILYHISTORY 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_FAMILYHISTORY]
AS
     SELECT 'FAMILY HISTORY' AS RESOURCETYPE,
            CAST(fh.ID AS NVARCHAR(20)) AS ExternalId, --R
            ISNULL(fr.name,'') AS RelationshipName, --O
		  CASE 
		     WHEN RELATIONSHIP=5 THEN 66839005
		     WHEN RELATIONSHIP=11 THEN 72705000
			WHEN RELATIONSHIP=15 THEN 40683002
		     WHEN RELATIONSHIP=2 THEN 70924004
		     WHEN RELATIONSHIP=17 THEN 27733009
		     WHEN RELATIONSHIP=16 THEN 375005
			WHEN RELATIONSHIP=8 THEN 38312007
			WHEN RELATIONSHIP=19 THEN 65616008
			WHEN RELATIONSHIP=20 THEN 66089001
			WHEN RELATIONSHIP=3 THEN 67822003
		     WHEN RELATIONSHIP=18 THEN 38048003
			WHEN RELATIONSHIP=1 THEN 25211005
			WHEN RELATIONSHIP=4 THEN 55538000
			ELSE 394738000	
		  END AS RelationshipCode, --O
            '2.16.840.1.113883.1.11.19563' AS RelationshipCodeSystem, --O
            'Personal Relationship Role Type Value Set' AS RelationshipCodeSystemName, --O
            DATEDIFF(YEAR, CONVERT(DATE, P.DateofBirth, 120), CONVERT(DATE, fh.ENTEREDDATE, 120)) AS AgeAtOnSetValue, --O
            'YEARS' AS AgeAtOnSetUnit, --O
            '' AS AdministrativeGenderCode, --O
            '' AS AdministrativeGenderDescription, --O
            '2.16.840.1.113883.5.1' AS AdministrativeGenderCodeSystem, --O
            'AdminstrativeGender' AS AdministrativeGenderCodeSystemName, --O
            '' AS IsFamilyMemberAlive, --O
            CAST(fh.conceptid AS NVARCHAR(20)) AS ProblemCode, --R
            fh.Term AS ProblemDescription, --R
            '2.16.840.1.113883.6.96' AS ProblemCodeSystem, --R
            'SNOMED CT' AS ProblemCodeSystemName, --R

            CASE
                WHEN fh.status = 1
                THEN 'ACTIVE'
                ELSE 'INACTIVE'
            END AS STATUS, --R
	
            CASE
                WHEN fh.status = 1
                THEN 'True'
                ELSE 'False'
            END AS IsActive, --R
		  fh.id PatientMedicalHistory,
		  fh.PatientId PAtientKey,
		  fh.AppointmentId ChartRecordKey		  
     FROM model.familyhistory fh WITH (NOLOCK)
	left join model.patients p with (nolock) on p.id=fh.patientid
          LEFT JOIN model.familyrelationships fr WITH (NOLOCK) ON fh.relationship = fr.id;
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_APPOINTMENT')) 
Drop View VIEW_ACI_APPOINTMENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_APPOINTMENT]
AS
     SELECT  
		'ENCOUNTER' AS RESOURCETYPE,
		cast(ap.appointmentid AS VARCHAR(15)) AS EncounterId, --R
		CASE
		  WHEN AP.APPDATE IS NULL THEN '01/01/1900'
		  ELSE (CONVERT(VARCHAR(20), CONVERT(DATE,ap.AppDate), 101))
		END AS  EncounterDate, --R
		RIGHT('00000000000'+CONVERT(VARCHAR, SUBSTRING(isnull( NPI, ''), 1, 10)), 10) AS ECNPI, --R
		RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(r.ResourceTaxId, ''), 1, 9)), 9) AS ECTIN, --O
		CAST(ap.DURATION as varchar(10))+' Mins' as EncounterDuration, 
		CAST(r.PracticeID AS NVARCHAR(10)) AS FacilityAccountNumber, --R
		CASE
		  WHEN pc.symptom = '/CHIEFCOMPLAINTS' THEN pc.findingdetail 
		  ELSE ''
		END AS ChiefComplaint, --O
		ISNULL((select top(1) a.reason from [model].[GoalInstruction] a where a.reason <> '' and a.AppointmentId =ap.AppointmentId ),'') AS ReasonForReferral, --O
		ISNULL((select top(1) (Note1+Note2+Note3+Note4) from dbo.PatientNotes b where b.NoteSystem = '1' and b.NoteType = 'C' and b.AppointmentId = ap.AppointmentId),'NOT AVAILABLE') Assessment,
		AP.COMMENTS AS ReasonForVisit, --O
		 (CONVERT(VARCHAR(20), CONVERT(DATETIME,ap.AppDate), 101)+' '+(CASE
                                                                WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20),  CONVERT(DATETIME,ap.AppDate), 22), 11))) = 10
                                                                THEN '0'+LTRIM(RIGHT(CONVERT(CHAR(20),  CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                                ELSE LTRIM(RIGHT(CONVERT(CHAR(20), CONVERT(DATETIME,ap.AppDate), 22), 11))
                                                            END)) AS SIGNEDDATE,
		'True' AS IsActive, --R
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.patientid AS PATIENTKEY,
		 pc.appointmentid as Schedulekey
	from dbo.appointments ap WITH(NOLOCK) 
	left join dbo.patientclinical pc WITH(NOLOCK) on pc.appointmentid= ap.appointmentid --and pc.symptom = '/CHIEFCOMPLAINTS' 
	left join dbo.resources r WITH(NOLOCK) 	on r.resourceid = ap.resourceid1
	where ap.apptypeid>0
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_APPOINTMENT_BILLINGCODE')) 
Drop View VIEW_ACI_APPOINTMENT_BILLINGCODE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_APPOINTMENT_BILLINGCODE]
AS
    SELECT   
		inv.EncounterId AS Appointmentid,
		cast(bs.id AS nvarchar(20)) AS ExternalId, --R
		'BILLINGCODE' AS RESOURCETYPE,
		CASE
		  WHEN ES.CODE IS NULL THEN '00000'
		  ELSE cast(es.code AS nvarchar(20))
		END As BillingCodeValue, --R
		CASE
		  WHEN es.description IS NULL THEN 'NOT AVAILABLE'
		  ELSE es.description 
		END AS BillingCodeDescription, --R 
		 CASE  WHEN CAST(ES.CODE AS nvarchar(20)) = 'G0438' OR CAST(ES.CODE AS nvarchar(20))='G0439' THEN '2.16.840.1.113883.6.285' ELSE '2.16.840.1.113883.6.12' END  AS BillingCodeSystem,
		CASE  WHEN CAST(ES.CODE AS nvarchar(20))='G0438'  OR CAST(ES.CODE AS nvarchar(20))='G0439' THEN 'HCPCS' ELSE 'CPT-4' END AS BillingCodeName,
		'True' IsActive, --R
		 inv.EncounterID AS CHARTRECORDKEY,
		 inv.EncounterID as Schedulekey
	from model.invoices inv WITH(NOLOCK) 
	left join model.billingservices bs WITH(NOLOCK)	on inv.id = bs.invoiceid
	left join  model.encounterservices es WITH(NOLOCK)
	on bs.encounterserviceid = es.id
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_MEDICATIONS_DosageQuantity')) 
Drop View VIEW_ACI_MEDICATIONS_DosageQuantity 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE VIEW [dbo].[VIEW_ACI_MEDICATIONS_DosageQuantity]
AS
--R1.1
    SELECT
          'DOSAGEQUANTITY' AS RESOURCETYPE,
          CASE
		WHEN SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-4/', pc.FindingDetail) + 4)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-4/', pc.FindingDetail) + 3)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-4/', pc.FindingDetail) + 3)+1)) - 1) = ''
		THEN '1 DAY' --ACI is not accepting 0.00 so we are sending 0.01 as default for ColdStart
		ELSE SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-4/', pc.FindingDetail) + 4)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-4/', pc.FindingDetail) + 3)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-4/', pc.FindingDetail) + 3)+1)) - 1)
		END as Value,--R  NEED TO CHECK FOR VALUE AND UNITS
		CASE
		WHEN SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-4/', pc.FindingDetail) + 4)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-4/', pc.FindingDetail) + 3)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-4/', pc.FindingDetail) + 3)+1)) - 1) = ''
		THEN '1 DAY' --ACI is not accepting 0.00 so we are sending 0.01 as default for ColdStart
		ELSE SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-4/', pc.FindingDetail) + 4)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-4/', pc.FindingDetail) + 3)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-4/', pc.FindingDetail) + 3)+1)) - 1)
		END as Units,--R  NEED TO CHECK FOR VALUE AND UNITS
     	 pc.appointmentid AS CHARTRECORDKEY,
		 pc.appointmentid as Schedulekey,
		 pc.ClinicalId as PATIENTMEDICATIONSKEY
	from  dbo.appointments ap WITH(NOLOCK)
	LEFT JOIN dbo.patientclinical pc WITH(NOLOCK) on pc.appointmentid = ap.appointmentid
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%' and pc.findingdetail not like '%RX%No Known Medication%' and pc.status <>'D'


UNION

 SELECT
          'DOSAGEQUANTITY' AS RESOURCETYPE,
          '' Value,--R  NEED TO CHECK FOR VALUE AND UNITS
		'' as Units,--R  NEED TO CHECK FOR VALUE AND UNITS
     	 pc.appointmentid AS CHARTRECORDKEY,
		 pc.appointmentid as Schedulekey,
		 pc.ClinicalId as PATIENTMEDICATIONSKEY
     FROM dbo.patientclinical pc WITH (NOLOCK)
          JOIN dbo.appointments ap WITH (NOLOCK) ON pc.appointmentid = ap.appointmentid
          JOIN dbo.resources r WITH (NOLOCK) ON ap.resourceid1 = r.resourceid
     WHERE pc.findingdetail like '%RX%No Known Medication%' and pc.status <>'D'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_MEDICATIONS_Reason')) 
Drop View VIEW_ACI_MEDICATIONS_Reason 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_MEDICATIONS_Reason]
AS
--R1.1
    SELECT 'Patient' as Type,
            'NA' AS Code,
            'NOT AVAILABLE' AS Description,
		 'SNOMED CT' as CodeSystemName,
		 '2.16.840.1.113883.6.96' as CodeSystem,
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.appointmentid as Schedulekey,
		 pc.ClinicalId as PATIENTMEDICATIONSKEY
	from  dbo.appointments ap WITH(NOLOCK)
	LEFT JOIN dbo.patientclinical pc WITH(NOLOCK) on pc.appointmentid = ap.appointmentid
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%' and pc.status <>'D'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_MEDICATIONS_ROUTE')) 
Drop View VIEW_ACI_MEDICATIONS_ROUTE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_MEDICATIONS_ROUTE]
AS
     SELECT DISTINCT
	 'ROUTE' AS RESOURCETYPE,
		CASE SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-3/', pc.FindingDetail) + 4)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-3/', pc.FindingDetail) + 3)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-3/', pc.FindingDetail) + 3)+1)) - 1)
			WHEN 'INSTILL IN EYES' THEN 'C38287'
			WHEN 'APPLY TO LIDS' THEN 'C38287'
			WHEN 'PO' THEN 'C38288'
			WHEN 'INJECTABLE' THEN 'C38192'
			ELSE ''
		END AS Code, --R
		CASE SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-3/', pc.FindingDetail) + 4)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-3/', pc.FindingDetail) + 3)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-3/', pc.FindingDetail) + 3)+1)) - 1)
			WHEN 'INSTILL IN EYES' THEN 'INSTILL IN EYES'
			WHEN 'APPLY TO LIDS' THEN 'APPLY TO LIDS'
			WHEN 'PO' THEN 'BY MOUTH'
			WHEN 'INJECTABLE' THEN 'INJECTABLE'
			ELSE ''
		END AS Description, --R
        '2.16.840.1.113883.3.26.1.1' AS CODESYSTEM, --R
        'NCI Thesures' AS CODESYSTEMNAME, --R
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.appointmentid as Schedulekey,
		 pc.ClinicalId as PATIENTMEDICATIONSKEY
	from  dbo.appointments ap WITH(NOLOCK)
	LEFT JOIN dbo.patientclinical pc WITH(NOLOCK) on pc.appointmentid = ap.appointmentid
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%' and pc.findingdetail not like '%RX%No Known Medication%'
	--AND PC.PATIENTID = 46896
	AND PC.ImageDescriptor NOT LIKE 'C%' and pc.status <>'D'


UNION

  SELECT 'ROUTE' AS RESOURCETYPE,
		'UNK' AS Code, --R
		'UNKNOWN' AS Description, --R
        '2.16.840.1.113883.3.26.1' AS CODESYSTEM, --R
        'NCI Thesures' AS CODESYSTEMNAME, --R
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.appointmentid as Schedulekey,
		 pc.ClinicalId as PATIENTMEDICATIONSKEY
	from  dbo.appointments a WITH(NOLOCK)
	LEFT JOIN dbo.patientclinical pc WITH(NOLOCK) on a.appointmentid = pc.appointmentid
	WHERE pc.findingdetail like '%RX%No Known Medication%' and pc.status <>'D'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_MEDICATIONS_PHARMACYPHONE')) 
Drop View VIEW_ACI_MEDICATIONS_PHARMACYPHONE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW VIEW_ACI_MEDICATIONS_PHARMACYPHONE
AS
     SELECT 'PHARMACYPHONE' AS RESOURCETYPE,
            'WORK' AS TYPE,
            '0000000000' AS NUMBER,
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.appointmentid as Schedulekey,
		 pc.ClinicalId as PATIENTMEDICATIONSKEY
	from  dbo.appointments ap WITH(NOLOCK)
	LEFT JOIN dbo.patientclinical pc WITH(NOLOCK) on pc.appointmentid = ap.appointmentid
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%' 
 Go
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON23_Dr_Communication_Event')) 
Drop View VIEW_ACI_JSON23_Dr_Communication_Event 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE view [dbo].[VIEW_ACI_JSON23_Dr_Communication_Event] as 
select 
'DrCommunicationEvent' AS RESOURCETYPE,
'EHR' AS IsSource,
D.ACIDepKey AS ReferenceNumber,
D.PatientKey AS PatientAccountNumber,
case when  D.Dep_RequestDate is null then '01/01/1900' else
	SUBSTRING((CONVERT(VARCHAR(20), D.Dep_RequestDate, 101)+' '+(CASE
                                                               WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), D.Dep_RequestDate, 22), 11))) = 10
                                                               THEN '0'+LTRIM(RIGHT(CONVERT(CHAR(20), D.Dep_RequestDate, 22), 11))
                                                               ELSE LTRIM(RIGHT(CONVERT(CHAR(20), D.Dep_RequestDate, 22), 11))
                                                              END)),1,10) END DateSent,
D.ToAddress AS ToPhysicianName,
RIGHT('00000000000'+CONVERT(VARCHAR, SUBSTRING(isnull( NPI, ''), 1, 10)), 10) AS ECNPI,
RIGHT('0000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(ResourceTaxId, ''), 1, 9)), 9) AS  ECTIN,
D.AciDepKey AS ExternalId,
ISNULL((Select TOP(1) ConceptID From model.PatientProblemList Where PatientId = D.PATIENTkEY),'') AS Code,
ISNULL((Select TOP(1) Term From model.PatientProblemList Where PatientId = D.PATIENTkEY),'') AS Description,
'SNOMED CT' AS CodeSystemName,
'2.16.840.1.113883.6.96' AS CodeSystem,
ACIDEPKEY

from dbo.aci_dep D WITH(NOLOCK) join dbo.Resources R on R.ResourceId = D.ProviderKey
where D.PatientKey <> 0


 Go
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_MEDICATIONS_PHARMACYADDRESS')) 
Drop View VIEW_ACI_MEDICATIONS_PHARMACYADDRESS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW VIEW_ACI_MEDICATIONS_PHARMACYADDRESS
AS
     SELECT    
  
/*MEDICATIONS-BEGIN*/

     'PHARMACYADDRESS' AS RESOURCETYPE,
     'WORK' AS TYPE,
     'NOT AVAILABLE' AS ADDRESSLINE1, --R 
     '' AS ADDRESSLINE2, --O 
     'NOT AVAILABLE' AS CITY, --R 
     'NA' AS STATE, --R 
     '00000' AS ZIP, --R 
     'NA' AS COUNTY, --O 
		pc.clinicalid as PATIENTMEDICATIONSKEY,
		pc.appointmentid as CHARTRECORDKEY,
		pc.patientid as PATIENTKEY
	from  dbo.appointments ap WITH(NOLOCK)
	LEFT JOIN dbo.patientclinical pc WITH(NOLOCK) on pc.appointmentid = ap.appointmentid
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%' 
 Go
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PAYER')) 
Drop View VIEW_ACI_PAYER 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_PAYER]
AS
     SELECT 'PAYER' AS RESOURCETYPE,
            '349' AS PAYERCODE, --R 
		  P.ID as INSURANCEKEY,
          P.ID As PATIENTKEY
		  
     FROM model.patients P(NOLOCK) 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PAYER_INSURANCE')) 
Drop View VIEW_ACI_PAYER_INSURANCE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_PAYER_INSURANCE]
AS
     SELECT 'INSURANCE' AS RESOURCETYPE,
            'PRIMARY' AS Type,
            'DEFAULT TEST' AS Description,
            'PARTICIPANT' AS PARTICIPANT,
            P.Id AS Number,
            '349' AS Code,
            '01/14/2017 12:00:00 PM' AS StartDate,
            '12/31/2017 12:00:00 PM' AS ExpiryDate,
            '123-456-7890' AS PhoneNumber,
            P.ID as INSURANCEKEY,
            P.Id As PATIENTKEY
     FROM MODEL.PATIENTS P (NOLOCK)
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PAYER_INSURANCE_ADDRESS')) 
Drop View VIEW_ACI_PAYER_INSURANCE_ADDRESS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW VIEW_ACI_PAYER_INSURANCE_ADDRESS
AS
     SELECT 'ADDRESS' AS RESOURCETYPE,
            Ia.Line1 AS Addressline1,
            Ia.Line2 AS Addressline2,
            Ia.City AS City,
            SOP.Name AS State,
            Ia.PostalCode AS Zip,
            'NA' AS CountyCode,
            'Not AVAILABLE' AS County,
            'USA' AS Country,
            PIN.ID as INSURANCEKEY,
            PIN.InsuredPatientID As PATIENTKEY
     FROM [model].[InsurerAddresses] Ia(NOLOCK)
	JOIN model.patientinsurances PIn WITH (NOLOCK) ON PIN.ID=IA.ID
          LEFT JOIN Model.stateorprovinces SOP WITH (NOLOCK) ON SOP.Id = Ia.stateorprovinceId;
 Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_INFORMANTRELATED')) 
Drop View VIEW_ACI_INFORMANTRELATED 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_INFORMANTRELATED]
AS
	select
		'INFORMANTRELATED' AS RESOURCETYPE,
		pr.[PatientRepresentativeId] AS ExternalId, --R
		1 AS Active, --R
	     ISNULL(pr.relationship,'NOT AVAILABLE') AS RelationshipName, --R
		 CASE
		 WHEN pr.relationship = 'Grandparent' THEN 'GRPRN'
		 WHEN pr.relationship = 'Spouse' THEN 'SPS'
		 ELSE 'BRO'
		 END
		  AS RelationshipCode,
		--ISNULL(left(pr.relationship,3),'00000') AS RelationshipCode,
		'2.16.840.1.113883.1.11.19563' AS RelationshipCodeSystem,
		'Personal Relationship Role Type Value Set' AS RelationshipCodeSystemName,
		ISNULL(pr.firstname,'NOT AVAILABLE')  AS GivenName, --R
		ISNULL(pr.lastname,'NOT AVAILABLE')  AS FamilyName, --R
		'NOT AVAILABLE'AS SecondandFurtherGivenNames, --O
		'True' as IsActive,

		patientid as PatientKey
	from model.patientrepresentatives pr WITH(NOLOCK)
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PLANOFTREATMENT')) 
Drop View VIEW_ACI_PLANOFTREATMENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE VIEW [dbo].[VIEW_ACI_PLANOFTREATMENT]
AS
     SELECT 
	     'PLANOFTREATMENT' AS RESOURCETYPE,
		ID AS ExternalId,
		CONCEPTID AS PlanCode,
		TERM AS PlanDescription,
		'2.16.840.1.113883.6.96' AS PlanCodeSystem,
		'SNOMED-CT' AS PlanCodeSystemName,
		'' AS PriorityPreferenceCode,
		'' AS PriorityPreferenceDescription,
		'2.16.840.1.113883.6.96' AS PriorityPreferenceCodeSystem,
		'SNOMED-CT' AS PriorityPreferenceCodeSystemName,
		'MTH' AS CAREGIVERCODE,
		'MOTHER' AS CAREGIVERDESCRIPTION,
		'2.16.840.1.113883.1.11.19563' AS CareGiverCodeSystem,
		'PERSONAL RELATIONSHIP ROLE TYPE VALUE SET' AS CareGiverCodeSystemName,
		CASE
		  WHEN (HEALTH IS NULL) OR (HEALTH='') THEN 'NO HEALTH CONCERNS'
		  ELSE HEALTH 
		END AS HealthConcerns,
		CASE
		  WHEN (GOAL IS NULL) OR (GOAL='') THEN 'NONE'
		  ELSE GOAL 
		END AS Goals,
		CASE
		  WHEN (REASON IS NULL) OR (REASON='') THEN 'NONE'
		  ELSE REASON 
		END AS Instructions,
		STATUS AS Status,
		CASE
		WHEN STATUS = 'ACTIVE' THEN 'TRUE'
		ELSE 'FALSE'
		END AS IsActive,
		ISNULL((select top(1) p.FindingDetail from dbo.patientclinical p where p.findingdetail like '%schedule appointment%' and p.appointmentid = GI.AppointmentId),'')
		 as FollowupVisit, -- Rev 1.23
		ID as PLANGOALKEY,
          PatientId as PATIENTKEY,
          AppointmentId as CHARTRECORDKEY
	FROM [model].[GoalInstruction] GI WITH(NOLOCK)
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PROBLEMSLIST')) 
Drop View VIEW_ACI_PROBLEMSLIST 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'










CREATE VIEW [dbo].[VIEW_ACI_PROBLEMSLIST]
AS
     SELECT 
		''PROBLEMSLIST'' AS RESOURCETYPE,
		'''' AS TYPE, --R
		cast(ppl.id AS NVARCHAR(20)) AS ExternalId, --R
		CASE
                WHEN PPL.ONSETDATE IS NULL
                THEN(CONVERT(VARCHAR(20), PPL.ENTEREDDATE, 101)+'' ''+(CASE
                                                                         WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PPL.ENTEREDDATE, 22), 11))) = 10
                                                                         THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), PPL.ENTEREDDATE, 22), 11))
                                                                         ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PPL.ENTEREDDATE, 22), 11))
                                                                     END))
                ELSE(CONVERT(VARCHAR(20), PPL.ONSETDATE, 101)+'' ''+(CASE
                                                                           WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PPL.ONSETDATE, 22), 11))) = 10
                                                                           THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), PPL.ONSETDATE, 22), 11))
                                                                           ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PPL.ONSETDATE, 22), 11))
                                                                       END))
            END AS ONSETDATE, 
		case 
			when ppl.status = ''Resolved'' then ISNULL((CONVERT(VARCHAR(20), PPL.RESOLVEDDATE, 101)+'' ''+(CASE
                                                                         WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PPL.RESOLVEDDATE, 22), 11))) = 10
                                                                         THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), PPL.RESOLVEDDATE, 22), 11))
                                                                         ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PPL.RESOLVEDDATE, 22), 11))
                                                                     END)),''11/10/2017 05:39:51 PM'')
			else ''''
		end as ResolutionDate, --O

		CASE
		WHEN ((select count(*) from model.PatientReconcileProblems where SnomedCT = ppl.conceptID and Appointmentid = ppl.appointmentid and status = 1)>0) 
		THEN       (CONVERT(VARCHAR(20), PPL.EnteredDate, 101)+'' ''+(CASE
                                                                  WHEN LEN(LTRIM(RIGHT(CONVERT(CHAR(20), PPL.EnteredDate, 22), 11))) = 10
                                                                  THEN ''0''+LTRIM(RIGHT(CONVERT(CHAR(20), PPL.EnteredDate, 22), 11))
                                                                  ELSE LTRIM(RIGHT(CONVERT(CHAR(20), PPL.EnteredDate, 22), 11))
                                                                  END))
		ELSE ''''
		END AS ReconciledDate,--O
		CASE 
		WHEN ppl.Status = ''Resolved'' THEN ''Resolved''
		ELSE ppl.Status 
		END  AS Status ,--R

		case
			when ppl.Status = ''Active''
			then ''True''
			else ''True''
			--else ''False''
		end as IsActive,
		 PPL.ENTEREDDATE,
		 PPL.AppointmentDate,
		 ppl.OnsetDate As Onset,
            ppl.id as PATIENTPROBLEMLISTKEY,
            ppl.PatientId as  PATIENTKEY,
            ppl.AppointmentId as CHARTRECORDKEY
		from model.patientproblemlist ppl WITH(NOLOCK)
		WHERE AppointmentID IS NOT NULL and PPL.status <>''Deleted''

' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_PROBLEMSLIST_PROBLEMCODE')) 
Drop View VIEW_ACI_PROBLEMSLIST_PROBLEMCODE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[VIEW_ACI_PROBLEMSLIST_PROBLEMCODE]
AS
     SELECT 
        ''PROBLEMCODE'' AS RESOURCETYPE,
		CASE WHEN convert(varchar, ppl.OnsetDate, 101) =convert(varchar, ppl.AppointmentDate, 101) THEN ''SNOMEDCODE''
		ELSE ''SYSTEMIC''
		END AS Type,
		CAST(ppl.conceptid AS NVARCHAR(20)) AS Code, --R
		ppl.Term AS Description, --R
		''2.16.840.1.113883.6.96'' AS CodeSystem, --R
		''SNOMED CT'' AS CodeSystemName, --R
		ConceptID as PROBLEMCODEVALUE,
		Term as PROBLEMCODEDESCRIPTION,
	     ''2.16.840.1.113883.6.96'' as PROBLEMCODESYSTEM,
		''SNOMED-CT'' as PROBLEMCODENAME,
		ppl.AppointmentDate,
		 PPL.ENTEREDDATE,
		 ppl.OnsetDate,
            ppl.id as PATIENTPROBLEMLISTKEY,
            ppl.PatientId as  PATIENTKEY,
            ppl.AppointmentId as CHARTRECORDKEY
		--SELECT *
		from  dbo.appointments ap WITH(NOLOCK)
		LEFT JOIN model.patientproblemlist ppl WITH(NOLOCK) ON PPL.APPOINTMENTID=AP.APPOINTMENTID
		where PPL.status <>''Deleted''


'
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_RESULTS')) 
Drop View VIEW_ACI_RESULTS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[VIEW_ACI_RESULTS]

AS
	SELECT DISTINCT
		''RESULT'' AS REFERENCETYPE,
		PPL.ID AS ExternalId, --R
		''LAB'' AS Type, --R
		PPL.CONCEPTID AS Code, --R
		CASE
			WHEN PPL.CONCEPTID in (422497000,423059004,423364005,423862000,424703005) THEN ''VISUAL ACUITY''
			WHEN PPL.ConceptID in (238131007,414916001,248342006) THEN ''BMI''
			ELSE ''MACULAR EXAM''
		END AS TestName, --R
		''2.16.840.1.113883.6.96'' AS CodeSystem, --R
		''SNOMED CT'' AS CodeSystemName, --R
		'''' AS Value, --C
		'''' AS Units, --C
		''PROLIFERATIVE TO SEVERE''  AS ReferenceRange, --C
		NULL AS Interpretation, --C
		(CONVERT(VARCHAR(20), CONVERT(DATE,ap.AppDate), 101)) + '' 12:00:00 AM'' AS Date, --R
		''NOT AVAILABLE'' AS LabName, --R
		CASE
			WHEN PPL.STATUS = ''Active'' THEN ''True''
			ELSE ''False''
		END AS IsActive, --R
		 '''' as LabIdentifier, -- Rev 1.23
		 PPL.Id as PATIENTORDERKEY,
         PPL.AppointmentId as CHARTRECORDKEY,
         PPL.PatientId as PATIENTKEY
	FROM
	MODEL.PatientProblemList PPL WITH(NOLOCK) 
	LEFT JOIN DBO.APPOINTMENTS AP WITH(NOLOCK)ON  AP.APPOINTMENTID =PPL.AppointmentId
	WHERE PPL.ConceptID in (312903003,312904009,312905005,399876000,59276001,428341000124108,193350004,193387007,232020009,422497000,423059004,423364005,423862000,424703005,238131007,414916001,248342006)

' 
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_RESULTS_LABPHONE')) 
Drop View VIEW_ACI_RESULTS_LABPHONE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW VIEW_ACI_RESULTS_LABPHONE
AS
     SELECT 'LABPHONE' AS RESOURCETYPE,
		'WORK' AS Type , --R
		'0000000000' AS Number, --R
		 PLTR.LabOrderId as PATIENTORDERKEY,
           PLTR.AppointmentId as PATIENTKEY,
           PLTR.PatientId as CHARTRECORDKEY
     FROM
	DBO.PATIENTLABTESTRESULTS PLTR WITH(NOLOCK) 
	JOIN DBO.PATIENTLABORDERS PLO WITH(NOLOCK) ON PLTR.LABORDERID = PLO.ORDERID
	LEFT JOIN DBO.APPOINTMENTS AP WITH(NOLOCK) ON PLTR.APPOINTMENTID = AP.APPOINTMENTID
	WHERE PLTR.RESULT<>''
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_RESULTS_LABADDRESS')) 
Drop View VIEW_ACI_RESULTS_LABADDRESS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW VIEW_ACI_RESULTS_LABADDRESS
AS
     SELECT 'LABADDRESS' AS RESOURCETYPE,
		'NOT AVAILABLE' AS AddressLine1, --R
		'NOT AVAILABLE' AS AddressLine2, --o
		'NA' AS City, --R
		'NA' AS State, --R
		'00000' AS Zip, --R
		'NA' AS County, --o
		'NOT AVAILABLE' AS CountyCode, --o
		 PLTR.LabOrderId as PATIENTORDERKEY,
         PLTR.AppointmentId as PATIENTKEY,
         PLTR.PatientId as CHARTRECORDKEY
     FROM
	DBO.PATIENTLABTESTRESULTS PLTR WITH(NOLOCK) 
	JOIN DBO.PATIENTLABORDERS PLO WITH(NOLOCK) ON PLTR.LABORDERID = PLO.ORDERID
	LEFT JOIN DBO.APPOINTMENTS AP WITH(NOLOCK) ON PLTR.APPOINTMENTID = AP.APPOINTMENTID
	WHERE PLTR.RESULT<>''
Go


IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_SOCIALHISTORY')) 
Drop View VIEW_ACI_SOCIALHISTORY 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE VIEW [dbo].[VIEW_ACI_SOCIALHISTORY]
AS
 SELECT 
		'SOCIALHISTORY' AS RESOURCETYPE,
		CAST(pc.clinicalid AS NVARCHAR(20)) AS ExternalId,
		'Smoking' AS Type,
				sc.name AS  NAME,
		ISNULL(SC.SNOMEDCode,'')  AS CODE,
		'2.16.840.1.113883.6.96' AS CODESYSTEM, --R 
		'SNOMED-CT' AS CODESYSTEMNAME, --R 
		(CONVERT(VARCHAR(20), CONVERT(DATE,ap.AppDate), 101)) + ' 12:00:00 AM' AS Date,
		'' as EndDate, --as per ccda
		CASE
		 WHEN STATUS='A' THEN 'True'
		 ELSE 'False'
	     END AS IsActive,
		pc.clinicalid,
		pc.appointmentid as CHARTRECORDKEY,
		pc.patientid
	from 
	dbo.patientclinical pc WITH(NOLOCK) left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
	left join model.smokingconditions sc on replace(sc.name,' ','') like '%'+SUBSTRING(Pc.FINDINGDETAIL,CHARINDEX('<',Pc.FINDINGDETAIL)+1,CHARINDEX('>',Pc.FINDINGDETAIL)-CHARINDEX('<',Pc.FINDINGDETAIL)-1) + '%'
	where pc.Symptom Like '%Smoking' and pc.FindingDetail Like '*%'
Go