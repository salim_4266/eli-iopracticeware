

/****** Object:  Trigger [model].[PP_LogPatientsInsertTrigger]    Script Date: 7/17/2018 4:50:06 PM ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[PP_LogPatientsInsertTrigger]'))
DROP TRIGGER [model].[PP_LogPatientsInsertTrigger]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE TRIGGER [model].[PP_LogPatientsInsertTrigger] ON [model].[Patients] AFTER INSERT NOT FOR REPLICATION AS
BEGIN
				
SET NOCOUNT ON

DECLARE @PatientId NUMERIC(18,0)
SELECT @PatientId = INSERTED.Id FROM INSERTED
	
	IF NOT EXISTS (select 1 from [MVE].[PP_ERPInputLog] where PatientId = @PatientId and status='N')
	BEGIN
		INSERT INTO [MVE].[PP_ERPInputLog](Email, PatientId, InputLogDate, IsEmailUpdated, IsPhoneUpdated, IsAddressUpdated, Status, ProcessedDate, IsPatientUpdated) 
		VALUES('', @PatientId, GETDATE(), 0, 0, 0, 'N', NULL, 0)
	END
		
SET NOCOUNT OFF

END

GO


