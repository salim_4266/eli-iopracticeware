/****** Object:  Table [dbo].[DROPDOWN]    Script Date: 11/03/2017 21:16:18 ******/
Insert into dbo.DropDown(TABLENAME
,FIELDNAME
,DATA
,DESCRIPTION1
,ISMERGED
,Code
,CodeSystem
,CodeSystemOID
,SNOMEDCT
,HL7CODE
,OMBCODE
,OMBCatagory) Values ('ACI_DEP', 'HISP_Type', 'D', 'Data Motion', NULL,	NULL, NULL,	NULL, NULL,	NULL, NULL, NULL)
, ('ACI_DEP', 'ClinicalDocumentType', 'R', 'Referral', NULL,	NULL, NULL,	NULL, NULL,	NULL, NULL, NULL)
, ('ACI_DEP', 'ClinicalDocumentType', 'T', 'TOC', NULL,	NULL, NULL,	NULL, NULL,	NULL, NULL, NULL)


SET ANSI_PADDING ON
GO
