/****** Object:  StoredProcedure [dbo].[CheckExistsDiagnosis]    Script Date: 6/7/2018 8:36:41 PM ******/
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckExistsDiagnosis]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CheckExistsDiagnosis]
GO
/****** Object:  StoredProcedure [dbo].[CheckExistsDiagnosis]    Script Date: 6/7/2018 8:36:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckExistsDiagnosis]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CheckExistsDiagnosis] AS' 
END
GO


ALTER  proc [dbo].[CheckExistsDiagnosis]
@AppointmentId bigInt,
@PatientId bigInt,
@ClinicalType nvarchar(1),
@EyeContext nvarchar(2),
@Symptom nvarchar(128),
@FindingDetail Nvarchar(255),
@ImageDescriptor nvarchar(512),
@Status NVARCHAR(1)
--,@RowCount INT OUT

AS
BEGIN



declare @Flag varchar(10)=''

	IF(@Flag='D')
	BEGIN

		DELETE FROM PatientClinical WHERE PatientId = @PatientId And AppointmentId = @AppointmentId  AND Status ='z'
	--	--SELECT TOP 1  * from PatientClinical

	END	
	
	IF EXISTS (SELECT 1 FROM PatientClinical 
					WHERE PatientId = @PatientId And AppointmentId = @AppointmentId 
					And ClinicalType = @ClinicalType And EyeContext = @EyeContext And Symptom = @Symptom And 
					FindingDetail Like '%'+ @FindingDetail +'%' 
					And Status ='Z' 
					And ImageDescriptor = @ImageDescriptor)
	BEGIN

	
		SELECT * FROM PatientClinical 
					WHERE PatientId = @PatientId And AppointmentId = @AppointmentId 
					And ClinicalType = @ClinicalType And EyeContext = @EyeContext And Symptom = @Symptom And 
					FindingDetail Like '%'+ @FindingDetail +'%' 
					And Status ='Z' 
					And ImageDescriptor = @ImageDescriptor
	
 
		UPDATE PatientClinical SET Status='A' WHERE PatientId = @PatientId And AppointmentId = @AppointmentId 
		And ClinicalType = @ClinicalType And EyeContext = @EyeContext And Symptom = @Symptom And 
		FindingDetail Like '%'+ @FindingDetail +'%' And  ImageDescriptor = @ImageDescriptor 
		
		
	END 

END 



GO
