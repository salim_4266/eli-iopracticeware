--Script to Create/Insert TABLE [model].[SexType_Enumeration]

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = 'model' ) 

BEGIN
EXEC sp_executesql N'CREATE SCHEMA Model'
END

SET QUOTED_IDENTIFIER OFF 

INSERT INTO [model].[SexType_Enumeration]([Id],[Type],[Code]) 
VALUES (1,	'Male',	'M')

INSERT INTO [model].[SexType_Enumeration]([Id],[Type],[Code]) 
VALUES (2,	'Female',	'F')

INSERT INTO [model].[SexType_Enumeration]([Id],[Type],[Code])  
VALUES (3,	'Unknown',	'UNK')

SET ANSI_PADDING ON
GO
update model.patients
set sexid = 1
where id = id and genderid = 3

update model.patients
set sexid = 2
where id = id and genderid = 2

update model.patients
set sexid = 3
where id = id and genderid in (1,4)

update model.patients
set genderid = 8
where id=id and genderid in (1,4)

GO