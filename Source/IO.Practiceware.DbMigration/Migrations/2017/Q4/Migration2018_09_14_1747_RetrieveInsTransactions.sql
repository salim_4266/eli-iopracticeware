IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[dbo].[RetrieveInsTransactions]')) 
Drop Procedure [dbo].[RetrieveInsTransactions] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[RetrieveInsTransactions] (
	@SAptDate VARCHAR(8)
	,@EAptDate VARCHAR(8)
	,@LocId INT
	,@ResId INT
	,@PatId INT
	,@TStat VARCHAR(1)
	)
AS
BEGIN

SELECT DISTINCT Appointments.AppointmentId
	,Appointments.AppDate
	,Appointments.AppTime
	,Appointments.Comments
	,Appointments.ScheduleStatus
	,Appointments.ReferralId
	,AppointmentType.AppointmentType
	,AppointmentType.Question
	,Appointments.ActivityStatus
	,Appointments.Comments
	,PatientDemographics.Salutation
	,Appointments.ConfirmStatus
	,PatientDemographics.LastName
	,PatientDemographics.FirstName
	,PatientDemographics.MiddleInitial
	,PatientDemographics.NameReference
	,PatientDemographics.Gender
	,PatientDemographics.BirthDate
	,PatientDemographics.HomePhone
	,PatientDemographics.BusinessPhone
	,PatientDemographics.CellPhone
	,PatientDemographics.ReferralRequired
	,Resources.ResourceName
	,pi.OrdinalId AS FinancialPIndicator
	,i.Name AS InsurerName
	,PatientDemographics.PatientId
	,ppn.OrdinalId AS PhoneSequence --*
	,ppn.patientphonenumbertypeid as PhoneType --*
FROM model.Insurers i
RIGHT JOIN (
	(
		Resources INNER JOIN (
			PatientDemographics INNER JOIN (
				AppointmentType INNER JOIN Appointments ON AppointmentType.AppTypeId = Appointments.AppTypeId
				) ON PatientDemographics.PatientId = Appointments.PatientId
			) ON Resources.ResourceId = Appointments.ResourceId1
		) LEFT JOIN (
		model.PatientInsurances pi INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		) ON PatientDemographics.PolicyPatientId = pi.InsuredPatientId
	) ON i.Id = ip.InsurerId

	left join model.patientphonenumbers ppn --*
	on ppn.patientid = PatientDemographics.patientid --*

WHERE PPN.OrdinalId = 1 AND (
		(Appointments.ResourceId2 = @LocId)
		OR (@LocId = - 1)
		)
	AND (
		(Appointments.ResourceId1 = @ResId)
		OR (@ResId = - 1)
		)
	AND (
		(PatientDemographics.PatientId = @PatId)
		OR (@PatId < 1)
		)
	AND (
		(Appointments.ScheduleStatus = 'P')
		OR (Appointments.ScheduleStatus = 'R')
		OR (Appointments.ScheduleStatus = 'A')
		OR (Appointments.ScheduleStatus = 'D')
		OR (Appointments.ScheduleStatus = 'N')
		OR (Appointments.ScheduleStatus = 'S')
		)
	AND (
		(Appointments.AppDate <= @SAptDate)
		OR (@SAptDate = '0')
		)
	AND (
		(Appointments.AppDate >= @EAptDate)
		OR (@EAptDate = '0')
		)
ORDER BY Appointments.AppDate DESC
	,Appointments.AppTime

END
GO


