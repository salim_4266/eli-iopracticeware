
/****** Object:  Trigger [dbo].[AuditPatientClinicalUpdateTrigger]    Script Date: 6/8/2018 3:42:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER TRIGGER [dbo].[AuditPatientClinicalUpdateTrigger] ON [dbo].[PatientClinical] AFTER UPDATE NOT FOR REPLICATION AS
			  BEGIN
DECLARE @msrepl_object_id int
SET @msrepl_object_id = OBJECT_ID('[dbo].[PatientClinical]')
DECLARE @retcode int
EXEC @retcode = sp_check_for_sync_trigger @msrepl_object_id

IF @retcode = 1 RETURN
				SET NOCOUNT ON

				DECLARE @newIds table (
					AuditEntryId uniqueIdentifier,
					KeyName nvarchar(400),
					KeyValue nvarchar(400),
					PatientId INT,
					AppointmentId INT,
					PRIMARY KEY(AuditEntryId, KeyName)
				)


		Declare @status VARCHAR(10)='Z'
		SELECT @status=[Status] from  inserted

		IF (@status<>'Z')
		BEGIN
		SELECT @status=[Status] from  deleted
		IF (@status<>'Z')
		BEGIN

				INSERT INTO @newIds (AuditEntryId, KeyName, KeyValue, PatientId, AppointmentId)
					SELECT AuditEntryId , KeyName, KeyValue, PatientId, AppointmentId FROM
					(SELECT NEWID() AS AuditEntryId, (SELECT CAST(d.[ClinicalId] AS nvarchar(max))) AS [ClinicalId],PatientId AS PatientId,AppointmentId AS AppointmentId
					FROM (SELECT [Activity], [AppointmentId], [ClinicalId], [ClinicalType], [DrawFileName], [EyeContext], [FindingDetail], [FollowUp], [Highlights], [ImageDescriptor], [ImageInstructions], [PatientId], [PermanentCondition], [PostOpPeriod], [Status], [Surgery], [Symptom] FROM deleted EXCEPT SELECT [Activity], [AppointmentId], [ClinicalId], [ClinicalType], [DrawFileName], [EyeContext], [FindingDetail], [FollowUp], [Highlights], [ImageDescriptor], [ImageInstructions], [PatientId], [PermanentCondition], [PostOpPeriod], [Status], [Surgery], [Symptom] FROM inserted) d) AS q
					UNPIVOT (KeyValue FOR KeyName IN ([ClinicalId])) AS unpvt

					--SELECT [AppointmentId], [ClinicalId], [PatientId] FROM inserted
					UPdate PCM  SET IsUpdate=1, UpdatedDatetime=GetDate(), PCM.FindingDetail=Ins.FindingDetail FROM drfirst.[PatientClinicalMeds] PCM INNER JOIN inserted Ins  ON PCM.PatientId=Ins.PatientId AND PCM.AppointmentId=ins.AppointmentId AND PCM.[ClinicalId]=ins.[ClinicalId]
					--select * from [dbo].[PatientClinicalMeds]
								

				DECLARE @userIdContext nvarchar(max)				
				IF OBJECT_ID('tempdb..#UserContext') IS NOT NULL
				BEGIN
					SELECT @userIdContext = UserId FROM #UserContext
				END

				DECLARE @userId int
				IF (ISNUMERIC(RIGHT(@userIdContext, 12)) = 1)
				BEGIN
					SELECT @userId = CONVERT(int, RIGHT(@userIdContext, 12))
				END

				DECLARE @auditDate datetime
				SET @auditDate = GETUTCDATE()
				DECLARE @hostName nvarchar(500)
				BEGIN TRY
				EXEC sp_executesql N'SELECT @hostName = HOST_NAME()', N'@hostName nvarchar(500) OUTPUT', @hostName = @hostName OUTPUT
				END TRY
				BEGIN CATCH
					 SET @hostName = CURRENT_USER
				END CATCH

				INSERT INTO dbo.AuditEntries (Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName,KeyNames, KeyValues, PatientId, AppointmentId)
					SELECT DISTINCT AuditEntryId, 'dbo.PatientClinical', 1, @auditDate, @hostName, @userId, convert(nvarchar, ServerProperty('ServerName')),'[' + KeyName + ']','[' + CAST(KeyValue AS NVARCHAR(MAX)) + ']', PatientId, AppointmentId FROM @newIds
					
				INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
					SELECT newIds.AuditEntryId, DeletedColumnName, OldValue,NewValue FROM
					(SELECT InsertedColumnName, NewValue, [KeyClinicalId], [DeletedActivity] AS [Activity], [DeletedAppointmentId] AS [AppointmentId], [DeletedClinicalId] AS [ClinicalId], [DeletedClinicalType] AS [ClinicalType], [DeletedDrawFileName] AS [DrawFileName], [DeletedEyeContext] AS [EyeContext], [DeletedFindingDetail] AS [FindingDetail], [DeletedFollowUp] AS [FollowUp], [DeletedHighlights] AS [Highlights], [DeletedImageDescriptor] AS [ImageDescriptor], [DeletedImageInstructions] AS [ImageInstructions], [DeletedPatientId] AS [PatientId], [DeletedPermanentCondition] AS [PermanentCondition], [DeletedPostOpPeriod] AS [PostOpPeriod], [DeletedStatus] AS [Status], [DeletedSurgery] AS [Surgery], [DeletedSymptom] AS [Symptom] FROM
					(SELECT inserted.[ClinicalId] AS [KeyClinicalId], (SELECT CAST(inserted.[Activity] AS nvarchar(max))) AS [Activity], (SELECT CAST(inserted.[AppointmentId] AS nvarchar(max))) AS [AppointmentId], (SELECT CAST(inserted.[ClinicalId] AS nvarchar(max))) AS [ClinicalId], (SELECT CAST(inserted.[ClinicalType] AS nvarchar(max))) AS [ClinicalType], (SELECT CAST(inserted.[DrawFileName] AS nvarchar(max))) AS [DrawFileName], (SELECT CAST(inserted.[EyeContext] AS nvarchar(max))) AS [EyeContext], (SELECT CAST(inserted.[FindingDetail] AS nvarchar(max))) AS [FindingDetail], (SELECT CAST(inserted.[FollowUp] AS nvarchar(max))) AS [FollowUp], (SELECT CAST(inserted.[Highlights] AS nvarchar(max))) AS [Highlights], (SELECT CAST(inserted.[ImageDescriptor] AS nvarchar(max))) AS [ImageDescriptor], (SELECT CAST(inserted.[ImageInstructions] AS nvarchar(max))) AS [ImageInstructions], (SELECT CAST(inserted.[PatientId] AS nvarchar(max))) AS [PatientId], (SELECT CAST(inserted.[PermanentCondition] AS nvarchar(max))) AS [PermanentCondition], (SELECT CAST(inserted.[PostOpPeriod] AS nvarchar(max))) AS [PostOpPeriod], (SELECT CAST(inserted.[Status] AS nvarchar(max))) AS [Status], (SELECT CAST(inserted.[Surgery] AS nvarchar(max))) AS [Surgery], (SELECT CAST(inserted.[Symptom] AS nvarchar(max))) AS [Symptom], (SELECT CAST(deleted.[Activity] AS nvarchar(max))) AS [DeletedActivity], (SELECT CAST(deleted.[AppointmentId] AS nvarchar(max))) AS [DeletedAppointmentId], (SELECT CAST(deleted.[ClinicalId] AS nvarchar(max))) AS [DeletedClinicalId], (SELECT CAST(deleted.[ClinicalType] AS nvarchar(max))) AS [DeletedClinicalType], (SELECT CAST(deleted.[DrawFileName] AS nvarchar(max))) AS [DeletedDrawFileName], (SELECT CAST(deleted.[EyeContext] AS nvarchar(max))) AS [DeletedEyeContext], (SELECT CAST(deleted.[FindingDetail] AS nvarchar(max))) AS [DeletedFindingDetail], (SELECT CAST(deleted.[FollowUp] AS nvarchar(max))) AS [DeletedFollowUp], (SELECT CAST(deleted.[Highlights] AS nvarchar(max))) AS [DeletedHighlights], (SELECT CAST(deleted.[ImageDescriptor] AS nvarchar(max))) AS [DeletedImageDescriptor], (SELECT CAST(deleted.[ImageInstructions] AS nvarchar(max))) AS [DeletedImageInstructions], (SELECT CAST(deleted.[PatientId] AS nvarchar(max))) AS [DeletedPatientId], (SELECT CAST(deleted.[PermanentCondition] AS nvarchar(max))) AS [DeletedPermanentCondition], (SELECT CAST(deleted.[PostOpPeriod] AS nvarchar(max))) AS [DeletedPostOpPeriod], (SELECT CAST(deleted.[Status] AS nvarchar(max))) AS [DeletedStatus], (SELECT CAST(deleted.[Surgery] AS nvarchar(max))) AS [DeletedSurgery], (SELECT CAST(deleted.[Symptom] AS nvarchar(max))) AS [DeletedSymptom]
					FROM inserted
					JOIN deleted ON inserted.[ClinicalId] = deleted.[ClinicalId]) AS q
					UNPIVOT (NewValue FOR InsertedColumnName IN ([Activity], [AppointmentId], [ClinicalId], [ClinicalType], [DrawFileName], [EyeContext], [FindingDetail], [FollowUp], [Highlights], [ImageDescriptor], [ImageInstructions], [PatientId], [PermanentCondition], [PostOpPeriod], [Status], [Surgery], [Symptom])) AS unpvt) AS q2
					UNPIVOT (OldValue FOR DeletedColumnName IN ([Activity], [AppointmentId], [ClinicalId], [ClinicalType], [DrawFileName], [EyeContext], [FindingDetail], [FollowUp], [Highlights], [ImageDescriptor], [ImageInstructions], [PatientId], [PermanentCondition], [PostOpPeriod], [Status], [Surgery], [Symptom])) AS unpvt2
					JOIN @newIds newIds ON (newIds.KeyValue = unpvt2.[KeyClinicalId] AND newIds.KeyName = 'ClinicalId')
					WHERE InsertedColumnName = DeletedColumnName
					AND ((OldValue IS NULL AND NewValue IS NOT NULL) OR (NewValue IS NOT NULL AND OldValue IS NULL) OR OldValue <> NewValue)

				IF @userId IS NULL
				BEGIN
					-- The following accounts for a special case for logging in.  The UserId is null when logging in where an AuditEntryChange is recorded.  During this time only,
					-- we can use the ResourceId value as the UserId value.  Otherwise in all other cases, the UserId is null.
					SELECT @userId = KeyValue FROM @newIds newIds
					JOIN dbo.AuditEntryChanges aec ON newIds.AuditEntryId = aec.AuditEntryId AND aec.ColumnName = 'IsLoggedIn'
					WHERE newIds.KeyName = 'ResourceId'

					IF @userId IS NOT NULL
					BEGIN
						UPDATE ae
						SET UserId = @userId
						FROM dbo.AuditEntries ae
						JOIN @newIds newIds ON ae.Id = newIds.AuditEntryId AND newIds.KeyName = 'ResourceId'
						JOIN dbo.AuditEntryChanges aec ON ae.Id = aec.AuditEntryId AND aec.ColumnName = 'IsLoggedIn'
						WHERE UserId = NULL
					END
				END

		END
		END
				SET NOCOUNT OFF
			  END
			 