
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[ACI_qfn_JsonEscape]') 
    AND xtype IN (N'FN', N'IF', N'TF')
)
    DROP FUNCTION [dbo].[ACI_qfn_JsonEscape]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[ACI_qfn_JsonEscape](@value nvarchar(max) )
returns nvarchar(max)
as begin
 if (@value is null) return '""'
 if (@value=  '0') return '""'  
 set @value=replace(@value,'\','\\')
 set @value=replace(@value,'"','\"')
 return '"'+@value+'"'
end


GO
/****** Object:  UserDefinedFunction [dbo].[ACI_qfn_XmlToJson]    Script Date: 12/08/2017 12:34:12 ******/
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[ACI_qfn_XmlToJson]') 
    AND xtype IN (N'FN', N'IF', N'TF')
)
    DROP FUNCTION [dbo].[ACI_qfn_XmlToJson]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[ACI_qfn_XmlToJson](@XmlData xml)
RETURNS nvarchar(max)
AS
BEGIN
  declare @m nvarchar(max)
  SELECT @m='['+Stuff
  (
     (SELECT theline from
    (SELECT ','+' {'+Stuff
       (
              (
                    SELECT ',"'+coalesce(b.c.value('local-name(.)', 'NVARCHAR(255)'),'')+'":'+
                    dbo.[aci_qfn_JsonEscape](b.c.value('text()[1]','NVARCHAR(MAX)')) --+'"' 
                 from x.a.nodes('*') b(c)                                                                
                 for xml path(''),TYPE).value('(./text())[1]','NVARCHAR(MAX)')
               ,1,1,'')+'}'
          from @XmlData.nodes('/*') x(a)
       ) JSON(theLine)
       for xml path(''),TYPE).value('.','NVARCHAR(MAX)')
      ,1,1,'')+']'
   return @m
END



GO
/****** Object:  UserDefinedFunction [dbo].[fn_Occurences]    Script Date: 12/08/2017 12:34:12 ******/

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[fn_Occurences]') 
    AND xtype IN (N'FN', N'IF', N'TF')
)
    DROP FUNCTION [dbo].[fn_Occurences]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   FUNCTION [dbo].[fn_Occurences] 
(
    @pattern varchar(255),
    @expression varchar(max),
	@occurenceNo int
)
RETURNS int
AS
BEGIN

DECLARE @Result int = 0;

		DECLARE @String nvarchar(max) = @expression
;WITH CTE as (
SELECT CHARINDEX(@pattern,@String) as SpaceIndex
UNION ALL 
SELECT CHARINDEX(@pattern,@String,SpaceIndex+1) FROM CTE
WHERE CHARINDEX(@pattern,@String,SpaceIndex+1) > 0 
)
, CTE2 as (
SELECT 
  * 
, ROW_NUMBER() OVER (ORDER BY SpaceIndex) AS RN
FROM CTE
)
SELECT @Result=SpaceIndex 
FROM CTE2
WHERE RN = @occurenceNo

  RETURN @Result
End


GO
/****** Object:  UserDefinedFunction [dbo].[GetTableDefinition]    Script Date: 12/08/2017 12:34:12 ******/
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[GetTableDefinition]') 
    AND xtype IN (N'FN', N'IF', N'TF')
)
    DROP FUNCTION [dbo].[GetTableDefinition]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetTableDefinition](@tableName nvarchar(max), @targetDbName nvarchar(max))
RETURNS nvarchar(max)
AS
BEGIN
DECLARE @columnsDefinition nvarchar(max)
SET @columnsDefinition = '('

DECLARE @columnDefinitions table(Definition nvarchar(max))

INSERT INTO @columnDefinitions(Definition)
SELECT 	
	c.COLUMN_NAME + ' '
	+ c.DATA_TYPE 
	+ (
	CASE 
		WHEN c.CHARACTER_MAXIMUM_LENGTH IS NULL THEN ' '
		WHEN c.CHARACTER_MAXIMUM_LENGTH IS NOT NULL AND c.CHARACTER_MAXIMUM_LENGTH = -1 THEN '(max) '
		ELSE '(' + CONVERT(nvarchar(max), c.CHARACTER_MAXIMUM_LENGTH) + ') '
	END
	)
	+ 'NULL, ' AS Definition
FROM INFORMATION_SCHEMA.COLUMNS c
WHERE OBJECT_ID(QUOTENAME(c.TABLE_SCHEMA) + '.' + QUOTENAME(c.TABLE_NAME)) = OBJECT_ID(@tableName) 
ORDER BY c.ORDINAL_POSITION

SELECT @columnsDefinition = @columnsDefinition + Definition
FROM @columnDefinitions

SET @columnsDefinition = SUBSTRING(@columnsDefinition, 1, LEN(@columnsDefinition) - 1) + ')'

DECLARE @createTableSql nvarchar(max)
SELECT @createTableSql  = 'CREATE TABLE ' + COALESCE(QUOTENAME(@targetDbName), DB_NAME()) + '.' + QUOTENAME(t.TABLE_SCHEMA) + '.' + QUOTENAME(t.TABLE_NAME) + @columnsDefinition
FROM INFORMATION_SCHEMA.TABLES t
WHERE OBJECT_ID(QUOTENAME(t.TABLE_SCHEMA) + '.' + QUOTENAME(t.TABLE_NAME)) = OBJECT_ID(@tableName)

RETURN @createTableSql
END

GO
SET ANSI_PADDING ON
GO
