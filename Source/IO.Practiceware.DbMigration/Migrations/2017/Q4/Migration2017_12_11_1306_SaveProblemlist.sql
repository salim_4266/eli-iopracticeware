/****** Object:  StoredProcedure [dbo].[SaveUpdateProblem2NewPatientProblemList]    Script Date: 3/16/2018 3:45:05 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaveUpdateProblem2NewPatientProblemList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SaveUpdateProblem2NewPatientProblemList]
GO
/****** Object:  StoredProcedure [dbo].[SaveUpdateProblem2NewPatientProblemList]    Script Date: 3/16/2018 3:45:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaveUpdateProblem2NewPatientProblemList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SaveUpdateProblem2NewPatientProblemList] AS' 
END
GO
 ALTER   procedure [dbo].[SaveUpdateProblem2NewPatientProblemList](
	@AppointmentId int,
	@PatientId int,
	@ClinicalType nvarchar(1),
	@EyeContext nvarchar(2),
	@Symptom nvarchar(128),
	@FindingDetail nvarchar(255),
	@ImageDescriptor nvarchar(512),
	@ImageInstructions nvarchar(512) ,
	@Status nvarchar (10) ,
	@DrawFileName nvarchar(64),
	@Highlights  nvarchar(1),
	@FollowUp nvarchar (1),
	@PermanentCondition nvarchar(1),
	@PostOpPeriod  int,
	@Surgery nvarchar(1),
	@Activity nvarchar (3),
	@LegacyClinicalDataSourceTypeId varchar(20),
	@IsEditPV INT
	)

AS BEGIN

DECLARE  @Icd10  nvarchar(100)
DECLARE  @icd10_Desc  nvarchar(1000)
DECLARE  @ConceptID  BIGINT
DECLARE  @Term  nvarchar(1000)
DECLARE  @AppDate  nvarchar(20)
DECLARE @ResourceId Int
Declare @side VARCHAR(200)


SET NOCOUNT ON

Declare  @icd10desc as TABLE 
(
icd10 VARCHAR(50),
icd10_desc VARCHAR(500),
ConceptId  BIGINT,
Term nVARCHAR(500),
AppointmentID BIGINT,
PatientId BIGINT
)	


 if (@ClinicalType='Q')
 BEGIN

	SELECT  @ResourceId=ResourceId1, @AppDate=AppDate FROM Appointments  WHERE PatientId=@PatientId AND AppointmentId=@AppointmentId
	--SET  @FindingDetail=  (CASE WHEN  CHARINDEX('.',@findingdetail,(charindex('.',@findingdetail)+1)) >0 THEN	SUBSTRING (@findingdetail,0, CHARINDEX('.',@findingdetail,(charindex('.',@findingdetail)+1))) ELSE @findingdetail END) 

	INSERT INTO @icd10desc(icd10, icd10_desc, AppointmentID ,PatientId)
	SELECT    icd10, icd10_desc,@AppointmentId,@PatientId    from TCI_ICD_Mapping where icd9_nl =@FindingDetail  
	AND Side in(Case when @EyeContext='OD' THEN 'Right' WHEN @EyeContext='OS' THEN 'Left'  WHEN @EyeContext='OU' THEN 'Bilateral'  ELSE  'Unspecified' END ) AND Level_2<>'N/A'
	UNION 
	SELECT  icd10, icd10_desc, @AppointmentId,@PatientId from TCI_ICD_Mapping where icd9_nl =@FindingDetail  
	AND Side in(Case when @EyeContext='' THEN  'Others' END ) AND Level_2<>'N/A'
			

	IF   NOT EXISTS( SELECT 1 FROM @icd10desc)
	BEGIN
		INSERT INTO @icd10desc(icd10, icd10_desc, AppointmentID ,PatientId)
		SELECT   distinct icd10, icd10_desc,@AppointmentId,@PatientId    from TCI_ICD_Mapping where icd9_nl =@FindingDetail  
		AND Side = 'Unspecified' AND Level_2<>'N/A'	
	END

	--CREATE CLUSTERED INDEX IX_icd10des_icd10  ON @icd10desc (icd10);   
	--CREATE NONCLUSTERED INDEX IX_icd10des_ConceptID  ON @icd10desc (ConceptID);   


	--select * from @icd10desc
	IF  EXISTS( SELECT 1 FROM @icd10desc)
	BEGIN		

		DECLARE @MINConcept AS TABLE 
		(ConceptID BIGINT ,icd10 VARCHAR(100))

		INSERT INTO  @MINConcept 
		SELECT MIN(ConceptID) as ConceptID , icd10  as  ICD10  FROM  SNOMED_2016    
		WHERE icd10 IN(SELECT icd10 from @icd10desc)
		GROUP BY  ICD10	
		 
		UPDATE TmpIcd  SET TmpIcd.ConceptId= (MCP.ConceptID)
		FROM @icd10desc  TmpIcd INNER JOIN  @MINConcept  MCP ON   MCP.ICD10=TmpIcd.icd10

		DECLARE @Snomed AS TABLE 
		(ConceptID BIGINT ,icd10 nVARCHAR(100),Term VARCHAR(500))

		INSERT INTO @Snomed (ConceptID,icd10,term) 
		Select ConceptID as ConceptID , icd10  as  ICD10 , term  FROM  SNOMED_2016 
		where  icd10 in(select icd10 from @icd10desc)

		UPDATE TmpIcd  SET  TmpIcd.Term=(SN.Term)  
		FROM @icd10desc  TmpIcd INNER JOIN @Snomed SN ON SN.ICD10=TmpIcd.icd10 AND  SN.ConceptID=TmpIcd.ConceptID
		
		UPDATE @icd10desc SET ConceptId=74964007, Term='Other' WHERE ConceptId IS NULL and Term IS NULL

		UPDATE @icd10desc SET @Status=CASE WHEN @Status='A' THEN 'Active' ELSE 'Deleted' END
		--CREATE CLUSTERED INDEX IX_PPL_id  ON Model.PatientProblemList (id);   
		--CREATE NONCLUSTERED INDEX IX_PPL_Appid  ON Model.PatientProblemList (AppointmentId);   
		--CREATE NONCLUSTERED INDEX IX_PPL_Pid  ON Model.PatientProblemList (PatientId);   
		--CREATE NONCLUSTERED INDEX IX_PPL_cid  ON Model.PatientProblemList (conceptId);   



		;MERGE INTO Model.PatientProblemList AS T
			USING @icd10desc AS S
			--ON (T.AppointmentId = S.AppointmentId AND (T.PatientId =S.PatientId AND T.AppointmentId = S.AppointmentId AND T.conceptId=S.ConceptID AND T.term=S.term AND T.ICD10=S.icd10  AND T.ICD10DESCR=S.icd10_desc))
			ON (T.AppointmentId = S.AppointmentId AND (T.PatientId =S.PatientId AND T.AppointmentId = S.AppointmentId AND T.conceptId=S.ConceptID  AND T.ICD10=S.icd10   AND T.COMMENTS=@FindingDetail))
			WHEN NOT MATCHED  
			THEN INSERT
				(PatientId
				,AppointmentId
				,AppointmentDate
				,ConceptID
				,Term
				,ICD10
				,ICD10DESCR
				,COMMENTS
				,ResourceName
				,OnsetDate
				,LastModifyDate
				,STATUS
				,ResourceId
				,EnteredDate
				,ResolvedDate) 
			VALUES(@PatientId,@AppointmentId,  Convert(datetime,@AppDate, 101),ConceptID, Term, Icd10,icd10_desc,@FindingDetail,@EyeContext,
			 Convert(datetime,@AppDate, 101), Convert(datetime,@AppDate, 101), CASE WHEN @IsEditPV=0 THEN 'Active' ELSE 'Deleted' END ,@ResourceId,  Convert(datetime,@AppDate, 101),NUll);
				
	
		Declare @recourds  int =0
	
		SELECT  ResourceName   FROM Model.PatientProblemList 
		WHERE PatientId=@PatientId and AppointmentId=@AppointmentId AND  COMMENTS=@FindingDetail  AND Status='Active' GROUP BY ResourceName,COMMENTS
		SELECT @recourds=@@ROWCOUNT

		IF(@recourds>1)
		BEGIN
			Delete From  @icd10desc
			Delete From  @MINConcept
		
			INSERT INTO @icd10desc(icd10, icd10_desc, AppointmentID ,PatientId)
			SELECT   Distinct icd10, icd10_desc,@AppointmentId,@PatientId    from TCI_ICD_Mapping where icd9_nl =@FindingDetail  
			AND Side ='Bilateral' AND Level_2<>'N/A'
			SET @EyeContext='OU' 

			INSERT INTO  @MINConcept 
			SELECT MIN(ConceptID) as ConceptID , icd10  as  ICD10  FROM  SNOMED_2016    
			WHERE icd10 IN(SELECT icd10 from @icd10desc)
			GROUP BY  ICD10	

		
			IF EXISTS( SELECT 1 FROM @icd10desc) 
			BEGIN  
 
				DELETE  FROM  Model.PatientProblemList  WHERE patientid=@PatientId and appointmentid=@AppointmentId AND  COMMENTS=@FindingDetail 

				UPDATE TmpIcd  SET TmpIcd.ConceptId= (MCP.ConceptID) FROM @icd10desc  TmpIcd INNER JOIN  @MINConcept  MCP ON   MCP.ICD10=TmpIcd.icd10

				UPDATE TmpIcd  SET  TmpIcd.Term=(SN.Term)  FROM @icd10desc  TmpIcd INNER JOIN SNOMED_2016 SN ON SN.ICD10=TmpIcd.icd10 AND  SN.ConceptID=TmpIcd.ConceptID		
				UPdate @icd10desc SET ConceptId=74964007, Term='Other' WHERE ConceptId IS NULL and Term IS NULL
				UPDATE @icd10desc SET @Status=CASE WHEN @Status='A' THEN 'Active' ELSE 'Deleted' END

				INSERT INTO Model.PatientProblemList
				Select @PatientId,@AppointmentId,  Convert(datetime,@AppDate, 101),ConceptID, Term, Icd10,icd10_desc,@FindingDetail,@EyeContext,
				Convert(datetime,@AppDate, 101), Convert(datetime,@AppDate, 101), CASE WHEN @IsEditPV=0 THEN 'Active' ELSE 'Deleted' END ,@ResourceId,  Convert(datetime,@AppDate, 101),NUll FROM @icd10desc		
			END
		END
		
		IF(@IsEditPV=-1)
		BEGIN
			UPDATE model.PatientProblemList SET Status='Deleted' WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId  AND Status='Active'
		
			UPDATE model.PatientProblemList SET Status='Active' 
			WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId 
			AND Icd10   in( select FindingDetailIcd10 from [PatientClinicalIcd10] where
			AppointmentId=@AppointmentId and  PatientId=@PatientId and  ClinicalTYpe='B')

			SELECT  * INTO  #GroupIcd FROM 
			(select COMMENTS as ICD9 , MIN(icd10)as icd10, MIN(status) as Status
			from  model.PatientProblemList  where patientid=@PatientId   and appointmentid=@AppointmentId AND  Comments  IS NOT NULL  
			group by Comments, patientid,appointmentid, ResourceName)  Tmp where  Status<>'Active'
										
	--		UPdate  T  SET Status='Active' FROM  model.PatientProblemList AS T  Inner JOIN #GroupIcd G  ON G.icd10=T.icd10 AND CONVERT (float ,T.Comments)=G.ICD9
			UPdate  T  SET Status='Active' FROM  model.PatientProblemList AS T  Inner JOIN #GroupIcd G  ON G.icd10=T.icd10 AND T.Comments=G.ICD9
			WHere T.Status='Deleted' AND   patientid=@PatientId   and appointmentid=@AppointmentId
		END

	END
	END
	SET NOCOUNT OFF
End

GO
