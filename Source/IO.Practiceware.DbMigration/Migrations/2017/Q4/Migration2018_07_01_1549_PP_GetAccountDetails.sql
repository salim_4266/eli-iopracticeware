

/****** Object:  StoredProcedure [MVE].[PP_GetAccountDetails]    Script Date: 06/29/2018 13:33:19 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_GetAccountDetails]')) 
Drop Procedure [MVE].[PP_GetAccountDetails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE procedure [MVE].[PP_GetAccountDetails]
As
Begin
 Select * from [MVE].[PP_AccountDetails] Where IsActive = 0
End


GO
