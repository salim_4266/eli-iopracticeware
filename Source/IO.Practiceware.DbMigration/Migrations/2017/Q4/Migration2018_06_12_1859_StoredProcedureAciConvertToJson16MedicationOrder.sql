IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON16_MedicationsOrder')) 
Drop Procedure ACI_ConvertToJson_JSON16_MedicationsOrder 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

/****** Object:  StoredProcedure [dbo].[ACI_ConvertToJson_JSON16_MedicationsOrder]    Script Date: 06/12/2018 16:45:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE proc [dbo].[ACI_ConvertToJson_JSON16_MedicationsOrder] (@PATIENTMEDICATIONSKEY varchar(20),@aciinputlogkey int)
as 
begin
/*
ACI Version :Release 1.0
Date:05/05/2017
Developer/Author:K.V.S.S.R.KRISHNA SARMA
Details/Description: To create JSON String in ACI_JSON_MAster table
*/
/*
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/  

    DECLARE @STRINGJSON_MEDICATION VARCHAR(MAX),@STRINGJSON_MED varchar(max),@PharmacyPhone Varchar(500),@PharmacyAddress varchar(1000)
    ,@Medication Varchar(max),@Route Varchar(8000)
    set @Medication =''
    set @Route =''
    set @STRINGJSON_MEDICATION =''
    set @STRINGJSON_MED =''
    set @PharmacyPhone =''
    set @PharmacyAddress =''


  declare @aciinputlogkey1 int ,@tableKey varchar(20), @ACIJSONMasterKey int, @chartRecordKey int , @patientid int, @eRx_Medicine_Response_Key int
  set @aciinputlogkey1 =0
  set @tableKey=''
  set @ACIJSONMasterKey =0
  set @tableKey=@PATIENTMEDICATIONSKEY
  set @STRINGJSON_MEDICATION=''
  set @chartRecordKey =0

    select top 1 @chartRecordKey = AppointmentId, @patientid = Patient_ExtID,@eRx_Medicine_Response_Key=eRx_Medicine_Response_Key from drfirst.eRx_Medicine_Response WHERE  cast(PrescriptionRcopiaID as varchar(20))=@PATIENTMEDICATIONSKEY
	ORDER by eRx_Medicine_Response_Key DESC


	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status='N' and  c.TableName = 'JSON16_MEDICATIONSORDER' 
	where m.status ='N' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 =0
	begin
	   declare @ReferenceNumber varchar(50)
	   Declare @ReferenceNumberT varchar(50)
	   set @ReferenceNumberT =''
	   set @ReferenceNumber =''

	   declare @PhyId int , @FacId int ,@PATIENTKEY1 int, @np varchar(20) , @tx varchar(20)
	   set @PhyId =0
	   set @FacId =0
	   set @np = ''
	   set @tx = ''

	   select top 1 @PhyId=Ap.ResourceId1,@FacId=R.PracticeId ,@PATIENTKEY1 = Ap.PatientId  , @np = r.NPI , @tx=r.ResourceTaxId from dbo.appointments Ap (nolock)
	   join dbo.Resources R (nolock) on R.ResourceId  =Ap.ResourceId1
	   where Ap.AppointmentId =@chartRecordKey order by Ap.AppointmentId desc
		
	   set @ReferenceNumber =''
	   select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.'+CAST(@FacId as varchar(10))+'.'+CAST(@PhyId as varchar(10))+'.'+CAST(@PATIENTKEY1 as varchar(10))+'.16.' from aci_json_Defaults (NOLOCK)
        set @ReferenceNumberT =@ReferenceNumber+cast(@chartRecordKey as varchar(10))

			IF (SELECT COUNT(*) FROM VIEW_ACI_JSON16_MEDICATIONSORDER (NOLOCK) WHERE  cast(PrescriptionRcopiaID as varchar(20))=@PATIENTMEDICATIONSKEY)>0
				SELECT @STRINGJSON_MEDICATION= REPLACE(REPLACE( DBO.ACI_QFN_XMLTOJSON (
				(
				SELECT 
						ISNULL(ResourceType,'') AS ResourceType,
						ISNULL(IsSource,'') AS IsSource,
						(isnull(@ReferenceNumber,'')+ISNULL(ReferenceNumber,'')) AS ReferenceNumber,																		
						ISNULL(PatientAccountNumber,'') AS PatientAccountNumber
			 
				   FROM VIEW_ACI_JSON16_MEDICATIONSORDER (NOLOCK) WHERE  cast(PrescriptionRcopiaID as varchar(20))=@PATIENTMEDICATIONSKEY  FOR XML PATH('MEDICATIONORDER'),TYPE ) ) ,'[','') ,'}]','')     

	   if ltrim(rTrim(@STRINGJSON_MEDICATION)) <> ''
	   begin
		  DECLARE CURSORA_med SCROLL CURSOR FOR 
		    select cast(PrescriptionRcopiaID as varchar(20)) from VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION (NOLOCK) where cast(PrescriptionRcopiaID as varchar(20))=@PATIENTMEDICATIONSKEY 
		  OPEN CURSORA_med   
		  FETCH FIRST FROM CURSORA_med INTO @PATIENTMEDICATIONSKEY
		  WHILE @@FETCH_STATUS=0
		  BEGIN
			 select  @Medication= replace(--'}'
			 replace(--']'
			 replace --'['
			  (ISNULL( dbo.ACI_qfn_XmlToJson(	
					 (select 
							ISNULL(ExternalId ,'') AS ExternalId,ISNULL(@np ,'') AS ECNPI,							
							(SUBSTRING(@tx,1,3)+'-'+SUBSTRING(@tx,4,2)+'-'+SUBSTRING(@tx,6,4))  as ECTIN
							,ISNULL(DrugName ,'') AS DrugName ,ISNULL(DrugCode ,'0') AS DrugCode ,ISNULL(DrugCodeSystem ,'') AS DrugCodeSystem,
							ISNULL(DrugCodeSystemName ,'') AS DrugCodeSystemName,ISNULL(MedicationType ,'') AS MedicationType,ISNULL(PrescriptionType ,'') AS PrescriptionType,
							ISNULL(Directions ,'') AS Directions ,ISNULL(AdditionalInstructions ,'') AS AdditionalInstructions,ISNULL(Strength ,'') AS Strength,
							ISNULL(Substitution ,'') AS Substitution,ISNULL(StartDate ,'') AS StartDate,ISNULL(EffectiveDate ,'') AS EffectiveDate,
							ISNULL(DaySupply ,'') AS DaySupply,ISNULL(DosageQuantityValue ,'') AS DosageQuantityValue,ISNULL(IsPrescribed ,'') AS IsPrescribed,
							ISNULL(EprescribedDate ,'') AS EprescribedDate,ISNULL(IsQueriedForDrugFormulary ,'') AS IsQueriedForDrugFormulary,ISNULL(IsCPOE ,'') AS IsCPOE,
							ISNULL(IsHighRiskMedication ,'') AS IsHighRiskMedication,ISNULL(IsControlSubstance ,'') AS IsControlSubstance,ISNULL(PharmacyName ,'') AS PharmacyName,
							ISNULL(ReconciledDate ,'') AS ReconciledDate,ISNULL(Status ,'') AS Status,ISNULL(DateEnd ,'') AS DateEnd 

							 from VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION (NOLOCK) where eRx_Medicine_Response_Key =@eRx_Medicine_Response_Key for xml path('Medication1'),type
						 ) 
					 )
				 ,'') 
				 ,'[','')
				 ,']','')
				 ,'}','')
				 --select @Route
				 if @Medication<>''
					  select  @Route= REPLACE(
					  REPLACE(
					  ISNULL( dbo.ACI_qfn_XmlToJson(	
					 (select top 1 
							  ISNULL(Code,'') AS Code,ISNULL(Description,'') AS Description,ISNULL(CodeSystem,'') AS CodeSystem,ISNULL(CodeSystemName,'') AS CodeSystemName							
								 from VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_ROUTE (NOLOCK) where eRx_Medicine_Response_Key =@eRx_Medicine_Response_Key  for xml path('Route'),type
						 ) 
					 )
					  ,'')
					  ,'[','')
					  ,']','')

				 
					  select  @PharmacyPhone= REPLACE(
					  REPLACE(
					  ISNULL( dbo.ACI_qfn_XmlToJson(	
					 (select top 1 
							  ISNULL(Type,'') AS Type,ISNULL(Number,'') AS Number 
								 from VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_PHARMACYPHONE (NOLOCK) where eRx_Medicine_Response_Key =@eRx_Medicine_Response_Key  for xml path('phone1'),type
						 ) 
					 )
					  ,'')
					  ,'[','')
					  ,']','')

					  select  @PharmacyAddress= REPLACE(
					  REPLACE(
					  ISNULL( dbo.ACI_qfn_XmlToJson(	
					 (select top 1 
							 ISNULL(Type,'') AS Type,ISNULL(AddressLine1,'') AS AddressLine1,ISNULL(AddressLine2,'') AS AddressLine2,
											ISNULL(City,'') AS City,ISNULL(State,'') AS State,ISNULL(Zip,'') AS Zip,ISNULL(County,'') AS County							  
								 from VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_PHARMACYADDRESS (NOLOCK) where eRx_Medicine_Response_Key =@eRx_Medicine_Response_Key  for xml path('add1'),type
						 ) 
					 )
					  ,'')
					  ,'[','')
					  ,']','')

                   
			       if @PharmacyPhone <>''
				  set @PharmacyPhone =',"PharmacyPhone":'+@PharmacyPhone

			       if @PharmacyAddress <>''
				  set @PharmacyAddress =',"PharmacyAddress":'+@PharmacyAddress

				 if @Route <>''
				 begin
	    			  set @Medication =@Medication+',"Route":'+@Route+@PharmacyPhone+@PharmacyAddress+'}'
				  if @STRINGJSON_MED ='' 
				  set @STRINGJSON_MED=@Medication
				  else          
				    set @STRINGJSON_MED=@STRINGJSON_MED+','+@Medication
				   --r select @Procedures
				 end
         
		    FETCH NEXT FROM CURSORA_med INTO @PATIENTMEDICATIONSKEY
		  END
		  CLOSE CURSORA_med
		  DEALLOCATE CURSORA_med
	   end		
			
      if ltrim(rTrim(@STRINGJSON_MED)) <> ''
	  begin
	  set @STRINGJSON_MEDICATION =@STRINGJSON_MEDICATION+',"Medication": ['+@STRINGJSON_MED+']}'
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,REFERENCENUMBER,Sort) 
                 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@STRINGJSON_MEDICATION))),@ReferenceNumberT,16)  
        
		select   @STRINGJSON_MEDICATION
		
		set @ACIJSONMasterKey=IDENT_CURRENT('ACI_JSON_Master')
		update ACI_InPutLog set status ='C',statuschangedt=GETDATE() where status ='N' and TableName ='JSON16_MEDICATIONSORDER' 
		 and ( tableKey=@tableKey and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where status='N' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
  end
end



GO


