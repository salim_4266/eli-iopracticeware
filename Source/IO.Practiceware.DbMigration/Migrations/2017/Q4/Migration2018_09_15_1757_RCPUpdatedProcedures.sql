IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[dbo].[AddUpdateACI_json_defaults]')) 
Drop Procedure [dbo].[AddUpdateACI_json_defaults] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[AddUpdateACI_json_defaults] (@ACI_VendorId int,@MDO_PracticeId int, @ACI_BaseURL varchar(300), @ACI_ProductKey varchar(300), @ACI_PairKey varchar(2000), @ACI_USERNAME varchar(300), @ACI_ROLE varchar(300), @ACI_Dashboard_BaseUrl varchar(300), @ACI_Baseurl_PublishDEP varchar(300), @ACI_Baseurl_GetPublishDEPResult varchar(300), @ACI_Baseurl_GetMessageDeliveryNotification varchar(300), @ACI_Baseurl_RetriveInboundDEPData varchar(300))
AS 
BEGIN
	IF EXISTS (select 1 from aci_json_defaults) 
	  BEGIN
			UPDATE aci_json_defaults  
			SET ACI_VendorId=@ACI_VendorId, 
			MDO_PracticeId=@MDO_PracticeId,
			ACI_BaseURL=@ACI_BaseURL,
			ACI_ProductKey=@ACI_ProductKey,
			ACI_PairKey=@ACI_PairKey,
			ACI_USERNAME=@ACI_USERNAME,
			ACI_ROLE=@ACI_ROLE,
			ACI_Dashboard_BaseUrl=@ACI_Dashboard_BaseUrl
			,ACI_Baseurl_PublishDEP=@ACI_Baseurl_PublishDEP
			,ACI_Baseurl_GetPublishDEPResult=@ACI_Baseurl_GetPublishDEPResult
			,ACI_Baseurl_GetMessageDeliveryNotification=@ACI_Baseurl_GetMessageDeliveryNotification
			,ACI_Baseurl_RetriveInboundDEPData=@ACI_Baseurl_RetriveInboundDEPData
	   END 
	ELSE
	 BEGIN
		INSERT INTO [dbo].[aci_json_defaults]
           ([ACI_VendorId]
           ,[MDO_PracticeId]
           ,[ACI_INTERVAL]
           ,[RECORDS]
           ,[ACI_BaseURL]
           ,[ACI_ServiceURL]
           ,[ACI_ProductKey]
           ,[BATCHNO]
           ,[BATCHDATE]
           ,[ACI_BatchID]
           ,[ACI_PairKey]
           ,[ACI_USERNAME]
           ,[ACI_ROLE]
           ,[ACI_Dashboard_BaseUrl]
           ,[ACI_Issuer]
           ,[ACI_Audience]
           ,[ACI_Expires]
           ,[ACI_Baseurl_PublishDEP]
           ,[ACI_Baseurl_GetPublishDEPResult]
           ,[ACI_Baseurl_GetMessageDeliveryNotification]
           ,[ACI_Baseurl_RetriveInboundDEPData])
     VALUES
           (
	    @ACI_VendorId
            ,@MDO_PracticeId
            ,1
            ,1
            ,@ACI_BaseURL
            ,'v1/PublishBatchIntegration/PublishBatch?BatchID='
            ,@ACI_ProductKey
            ,15
            ,Replace(Convert(VARCHAR(10),GETDATE(),102),'.','')
            ,1000
            ,@ACI_PairKey
            ,@ACI_USERNAME
            ,@ACI_ROLE
            ,@ACI_Dashboard_BaseUrl
            ,'http://www.emrxxxxxxsample.com'
	    ,'http://localhost:54288/'
            ,10
            ,@ACI_Baseurl_PublishDEP
            ,@ACI_Baseurl_GetPublishDEPResult
            ,@ACI_Baseurl_GetMessageDeliveryNotification
            ,@ACI_Baseurl_RetriveInboundDEPData
	   )
	End 
END 

GO



