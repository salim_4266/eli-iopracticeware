IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.PatientReconcileMedications')  AND name = 'MergedDate')
BEGIN
	Alter table model.PatientReconcileMedications 
	Add MergedDate datetime
End
