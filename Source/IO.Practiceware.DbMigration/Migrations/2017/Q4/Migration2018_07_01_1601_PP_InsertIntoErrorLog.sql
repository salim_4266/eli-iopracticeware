

/****** Object:  StoredProcedure [MVE].[PP_InsertIntoErrorLog]    Script Date: 06/29/2018 13:41:22 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_InsertIntoErrorLog]')) 
Drop Procedure [MVE].[PP_InsertIntoErrorLog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  


Create Procedure [MVE].[PP_InsertIntoErrorLog]
@PageName nvarchar(100),
@MethodName nvarchar(200),
@ErrDescription nvarchar(200),
@ErrorMessage varchar(max),
@createdBy int=0
As
Begin
  Insert into [MVE].[PP_ErrorLog](PageName,MethodName,ErrDescription,ErrorMessage,createdBy) Values(@PageName, @MethodName, @ErrDescription, @ErrorMessage, @createdBy)

End

GO


