

/****** Object:  Trigger [dbo].[AuditPatientClinicalUpdateTrigger]    Script Date: 2/26/2018 6:07:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[AuditPatientClinicalUpdateTrigger] ON [dbo].[PatientClinical] AFTER UPDATE NOT FOR REPLICATION AS
			  BEGIN
DECLARE @msrepl_object_id int
SET @msrepl_object_id = OBJECT_ID('[dbo].[PatientClinical]')
DECLARE @retcode int
EXEC @retcode = sp_check_for_sync_trigger @msrepl_object_id
IF @retcode = 1 RETURN
				SET NOCOUNT ON

				DECLARE @newIds table (
					AuditEntryId uniqueIdentifier,
					KeyName nvarchar(400),
					KeyValue nvarchar(400),
					PatientId INT,
					AppointmentId INT,
					PRIMARY KEY(AuditEntryId, KeyName)
				)

				INSERT INTO @newIds (AuditEntryId, KeyName, KeyValue, PatientId, AppointmentId)
					SELECT AuditEntryId , KeyName, KeyValue, PatientId, AppointmentId FROM
					(SELECT NEWID() AS AuditEntryId, (SELECT CAST(d.[ClinicalId] AS nvarchar(max))) AS [ClinicalId],PatientId AS PatientId,AppointmentId AS AppointmentId
					FROM (SELECT [Activity], [AppointmentId], [ClinicalId], [ClinicalType], [DrawFileName], [EyeContext], [FindingDetail], [FollowUp], [Highlights], [ImageDescriptor], [ImageInstructions], [PatientId], [PermanentCondition], [PostOpPeriod], [Status], [Surgery], [Symptom] FROM deleted EXCEPT SELECT [Activity], [AppointmentId], [ClinicalId], [ClinicalType], [DrawFileName], [EyeContext], [FindingDetail], [FollowUp], [Highlights], [ImageDescriptor], [ImageInstructions], [PatientId], [PermanentCondition], [PostOpPeriod], [Status], [Surgery], [Symptom] FROM inserted) d) AS q
					UNPIVOT (KeyValue FOR KeyName IN ([ClinicalId])) AS unpvt

					--SELECT [AppointmentId], [ClinicalId], [PatientId] FROM inserted
					UPdate PCM  SET IsUpdate=1, UpdatedDatetime=GetDate(), PCM.FindingDetail=Ins.FindingDetail FROM drfirst.[PatientClinicalMeds] PCM INNER JOIN inserted Ins  ON PCM.PatientId=Ins.PatientId AND PCM.AppointmentId=ins.AppointmentId AND PCM.[ClinicalId]=ins.[ClinicalId]
					--select * from [dbo].[PatientClinicalMeds]
								

				DECLARE @userIdContext nvarchar(max)				
				IF OBJECT_ID('tempdb..#UserContext') IS NOT NULL
				BEGIN
					SELECT @userIdContext = UserId FROM #UserContext
				END

				DECLARE @userId int
				IF (ISNUMERIC(RIGHT(@userIdContext, 12)) = 1)
				BEGIN
					SELECT @userId = CONVERT(int, RIGHT(@userIdContext, 12))
				END

				DECLARE @auditDate datetime
				SET @auditDate = GETUTCDATE()
				DECLARE @hostName nvarchar(500)
				BEGIN TRY
				EXEC sp_executesql N'SELECT @hostName = HOST_NAME()', N'@hostName nvarchar(500) OUTPUT', @hostName = @hostName OUTPUT
				END TRY
				BEGIN CATCH
					 SET @hostName = CURRENT_USER
				END CATCH

				INSERT INTO dbo.AuditEntries (Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName,KeyNames, KeyValues, PatientId, AppointmentId)
					SELECT DISTINCT AuditEntryId, 'dbo.PatientClinical', 1, @auditDate, @hostName, @userId, convert(nvarchar, ServerProperty('ServerName')),'[' + KeyName + ']','[' + CAST(KeyValue AS NVARCHAR(MAX)) + ']', PatientId, AppointmentId FROM @newIds
					
				INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
					SELECT newIds.AuditEntryId, DeletedColumnName, OldValue,NewValue FROM
					(SELECT InsertedColumnName, NewValue, [KeyClinicalId], [DeletedActivity] AS [Activity], [DeletedAppointmentId] AS [AppointmentId], [DeletedClinicalId] AS [ClinicalId], [DeletedClinicalType] AS [ClinicalType], [DeletedDrawFileName] AS [DrawFileName], [DeletedEyeContext] AS [EyeContext], [DeletedFindingDetail] AS [FindingDetail], [DeletedFollowUp] AS [FollowUp], [DeletedHighlights] AS [Highlights], [DeletedImageDescriptor] AS [ImageDescriptor], [DeletedImageInstructions] AS [ImageInstructions], [DeletedPatientId] AS [PatientId], [DeletedPermanentCondition] AS [PermanentCondition], [DeletedPostOpPeriod] AS [PostOpPeriod], [DeletedStatus] AS [Status], [DeletedSurgery] AS [Surgery], [DeletedSymptom] AS [Symptom] FROM
					(SELECT inserted.[ClinicalId] AS [KeyClinicalId], (SELECT CAST(inserted.[Activity] AS nvarchar(max))) AS [Activity], (SELECT CAST(inserted.[AppointmentId] AS nvarchar(max))) AS [AppointmentId], (SELECT CAST(inserted.[ClinicalId] AS nvarchar(max))) AS [ClinicalId], (SELECT CAST(inserted.[ClinicalType] AS nvarchar(max))) AS [ClinicalType], (SELECT CAST(inserted.[DrawFileName] AS nvarchar(max))) AS [DrawFileName], (SELECT CAST(inserted.[EyeContext] AS nvarchar(max))) AS [EyeContext], (SELECT CAST(inserted.[FindingDetail] AS nvarchar(max))) AS [FindingDetail], (SELECT CAST(inserted.[FollowUp] AS nvarchar(max))) AS [FollowUp], (SELECT CAST(inserted.[Highlights] AS nvarchar(max))) AS [Highlights], (SELECT CAST(inserted.[ImageDescriptor] AS nvarchar(max))) AS [ImageDescriptor], (SELECT CAST(inserted.[ImageInstructions] AS nvarchar(max))) AS [ImageInstructions], (SELECT CAST(inserted.[PatientId] AS nvarchar(max))) AS [PatientId], (SELECT CAST(inserted.[PermanentCondition] AS nvarchar(max))) AS [PermanentCondition], (SELECT CAST(inserted.[PostOpPeriod] AS nvarchar(max))) AS [PostOpPeriod], (SELECT CAST(inserted.[Status] AS nvarchar(max))) AS [Status], (SELECT CAST(inserted.[Surgery] AS nvarchar(max))) AS [Surgery], (SELECT CAST(inserted.[Symptom] AS nvarchar(max))) AS [Symptom], (SELECT CAST(deleted.[Activity] AS nvarchar(max))) AS [DeletedActivity], (SELECT CAST(deleted.[AppointmentId] AS nvarchar(max))) AS [DeletedAppointmentId], (SELECT CAST(deleted.[ClinicalId] AS nvarchar(max))) AS [DeletedClinicalId], (SELECT CAST(deleted.[ClinicalType] AS nvarchar(max))) AS [DeletedClinicalType], (SELECT CAST(deleted.[DrawFileName] AS nvarchar(max))) AS [DeletedDrawFileName], (SELECT CAST(deleted.[EyeContext] AS nvarchar(max))) AS [DeletedEyeContext], (SELECT CAST(deleted.[FindingDetail] AS nvarchar(max))) AS [DeletedFindingDetail], (SELECT CAST(deleted.[FollowUp] AS nvarchar(max))) AS [DeletedFollowUp], (SELECT CAST(deleted.[Highlights] AS nvarchar(max))) AS [DeletedHighlights], (SELECT CAST(deleted.[ImageDescriptor] AS nvarchar(max))) AS [DeletedImageDescriptor], (SELECT CAST(deleted.[ImageInstructions] AS nvarchar(max))) AS [DeletedImageInstructions], (SELECT CAST(deleted.[PatientId] AS nvarchar(max))) AS [DeletedPatientId], (SELECT CAST(deleted.[PermanentCondition] AS nvarchar(max))) AS [DeletedPermanentCondition], (SELECT CAST(deleted.[PostOpPeriod] AS nvarchar(max))) AS [DeletedPostOpPeriod], (SELECT CAST(deleted.[Status] AS nvarchar(max))) AS [DeletedStatus], (SELECT CAST(deleted.[Surgery] AS nvarchar(max))) AS [DeletedSurgery], (SELECT CAST(deleted.[Symptom] AS nvarchar(max))) AS [DeletedSymptom]
					FROM inserted
					JOIN deleted ON inserted.[ClinicalId] = deleted.[ClinicalId]) AS q
					UNPIVOT (NewValue FOR InsertedColumnName IN ([Activity], [AppointmentId], [ClinicalId], [ClinicalType], [DrawFileName], [EyeContext], [FindingDetail], [FollowUp], [Highlights], [ImageDescriptor], [ImageInstructions], [PatientId], [PermanentCondition], [PostOpPeriod], [Status], [Surgery], [Symptom])) AS unpvt) AS q2
					UNPIVOT (OldValue FOR DeletedColumnName IN ([Activity], [AppointmentId], [ClinicalId], [ClinicalType], [DrawFileName], [EyeContext], [FindingDetail], [FollowUp], [Highlights], [ImageDescriptor], [ImageInstructions], [PatientId], [PermanentCondition], [PostOpPeriod], [Status], [Surgery], [Symptom])) AS unpvt2
					JOIN @newIds newIds ON (newIds.KeyValue = unpvt2.[KeyClinicalId] AND newIds.KeyName = 'ClinicalId')
					WHERE InsertedColumnName = DeletedColumnName
					AND ((OldValue IS NULL AND NewValue IS NOT NULL) OR (NewValue IS NOT NULL AND OldValue IS NULL) OR OldValue <> NewValue)

				IF @userId IS NULL
				BEGIN
					-- The following accounts for a special case for logging in.  The UserId is null when logging in where an AuditEntryChange is recorded.  During this time only,
					-- we can use the ResourceId value as the UserId value.  Otherwise in all other cases, the UserId is null.
					SELECT @userId = KeyValue FROM @newIds newIds
					JOIN dbo.AuditEntryChanges aec ON newIds.AuditEntryId = aec.AuditEntryId AND aec.ColumnName = 'IsLoggedIn'
					WHERE newIds.KeyName = 'ResourceId'

					IF @userId IS NOT NULL
					BEGIN
						UPDATE ae
						SET UserId = @userId
						FROM dbo.AuditEntries ae
						JOIN @newIds newIds ON ae.Id = newIds.AuditEntryId AND newIds.KeyName = 'ResourceId'
						JOIN dbo.AuditEntryChanges aec ON ae.Id = aec.AuditEntryId AND aec.ColumnName = 'IsLoggedIn'
						WHERE UserId = NULL
					END
				END

				SET NOCOUNT OFF
			  END
GO

/****** Object:  Trigger [ManageDiagnosisToProblemList]    Script Date: 3/16/2018 3:20:50 PM ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[ManageDiagnosisToProblemList]'))
DROP TRIGGER [dbo].[ManageDiagnosisToProblemList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[ManageDiagnosisToProblemList]'))
EXEC dbo.sp_executesql @statement = N'


CREATE  TRIGGER [dbo].[ManageDiagnosisToProblemList]
ON [dbo].[PatientClinicalIcd10]
FOR INSERT
NOT FOR REPLICATION
--This trigger helps with the join from PatientClincial to PatientNotes.
AS
BEGIN

SET NOCOUNT ON

	DECLARE  @AppointmentId  BIGINT
	DECLARE  @PatientId  BIGINT
	DECLARE  @Icd10  nvarchar(100)
	DECLARE  @Icd9  nvarchar(100)
	DECLARE  @Description  nvarchar(500)
	DECLARE  @Status  nvarchar(2)

	DECLARE  @ConceptID  BIGINT
	DECLARE  @Term  nvarchar(500)
	DECLARE  @AppDate  nvarchar(20)
	DECLARE @ResourceId Int

	--select * from  model.PatientProblemList  where patientid=10896


IF OBJECT_ID(''tempdb..#MINConcept'') IS NOT NULL
	DROP TABLE #MINConcept
IF OBJECT_ID(''tempdb..#GroupIcd1'') IS NOT NULL
	DROP TABLE #GroupIcd1
IF OBJECT_ID(''tempdb..#GroupIcd'') IS NOT NULL
	DROP TABLE #GroupIcd

	SELECT @AppointmentId=AppointmentId, @PatientId=PatientId, @Icd10=FindingDetailIcd10, @Description=[Description], @Status=[Status] FROM inserted WHERE ClinicalTYpe=''B''

	select * from [PatientClinicalIcd10] where
				AppointmentId=@AppointmentId and  @PatientId=PatientId and  ClinicalTYpe=''B''
				
	SELECT MIN(ConceptID) as ConceptID , MIn(icd10)  as  ICD10 INTO #MINConcept  FROM  SNOMED_2016    GROUP BY  ICD10	
	
	SET @ConceptID=0
	SELECT  TOP 1 @ConceptID=ConceptID  FROM #MINConcept WHERE ICD10=@Icd10

	SELECT  TOP 1 @Term=Term FROM SNOMED_2016 WHERE ICD10=@Icd10 AND ConceptID=@ConceptID
	
	
	--SELECT  TOP 1 @ConceptID=ConceptID, @Term=Term FROM SNOMED_2016 WHERE ICD10=@Icd10
	
	If @ConceptID=0
	BEGIN
		SET @ConceptID=74964007
		SET @Term=''Other''		
	END			

	SELECT  @AppDate= AppDate  , @ResourceId= ResourceId1 FROM  Appointments WHERE AppointmentId=@AppointmentId AND PatientId=@PatientId

	IF(@ConceptID IS NOT NULL AND @ConceptID>0)
	BEGIN
	 IF  NOT EXISTS (SELECT 1 FROM model.PatientProblemList  WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId AND ConceptID=@ConceptID AND Term=@Term AND Icd10=@Icd10 AND ICD10DESCR=@Description)
	  BEGIN
	  
		INSERT INTO model.PatientProblemList (
		PatientId,
		AppointmentId,
		AppointmentDate,
		ConceptID,
		Term,
		ICD10,
		ICD10DESCR,
		COMMENTS,
		ResourceName,
		OnsetDate,
		LastModifyDate,
		Status,
		ResourceId,
		EnteredDate,
		ResolvedDate)
		VALUES(@PatientId,@AppointmentId,  CONVERT(DATETIME,@AppDate, 101),@ConceptID, @Term, @Icd10,@Description,NUll,NULL,
		 CONVERT(DATETIME,@AppDate, 101), CONVERT(DATETIME,@AppDate, 101), CASE WHEN @Status=''A'' THEN ''Active'' ELSE ''InActive'' END ,@ResourceId,  CONVERT(DATETIME,@AppDate, 101),NUll)

		--SELECT  @icd9=comments FROM model.PatientProblemList  WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId AND ConceptID=@ConceptID AND Term=@Term AND Icd10=@Icd10 AND ICD10DESCR=@Description

		UPDATE model.PatientProblemList SET Status=''Deleted'' WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId  AND Status=''Active''
		
		UPDATE model.PatientProblemList SET Status=''Active'' 
		WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId 
		AND Icd10   in( select FindingDetailIcd10 from [PatientClinicalIcd10] where
		AppointmentId=@AppointmentId and  PatientId=@PatientId and  ClinicalTYpe=''B'')

		SELECT  * INTO  #GroupIcd FROM 
		(select MIN(comments) as ICD9 , MIN(icd10)as icd10, MIN(status) as Status
		from  model.PatientProblemList  where patientid=@PatientId   and appointmentid=@AppointmentId AND  Comments  IS NOT NULL  
		group by Comments, patientid,appointmentid,ResourceName)  Tmp where  Status<>''Active''
										
		UPdate  T  SET Status=''Active'' FROM  model.PatientProblemList AS T  Inner JOIN #GroupIcd G  ON G.icd10=T.icd10 AND T.Comments=G.ICD9
		WHere T.Status=''Deleted'' AND   patientid=@PatientId   and appointmentid=@AppointmentId

	END 
	ELSE 
	BEGIN
				UPDATE model.PatientProblemList SET Status=''Deleted'' WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId  AND Status=''Active''
			UPDATE model.PatientProblemList SET Status=''Active'' 
			WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId 
			 AND Icd10   in( select FindingDetailIcd10 from [PatientClinicalIcd10] where
				AppointmentId=@AppointmentId and  PatientId=@PatientId and  ClinicalTYpe=''B'')

				SELECT  * INTO  #GroupIcd1 FROM 
--				(select MIN(CONVERT (float ,comments)) as ICD9 , MIN(icd10)as icd10, MIN(status) as Status
				(select MIN(comments) as ICD9 , MIN(icd10)as icd10, MIN(status) as Status
				from  model.PatientProblemList  where patientid=@PatientId   and appointmentid=@AppointmentId AND  Comments  IS NOT NULL  
				group by Comments, patientid,appointmentid, ResourceName)  Tmp where  Status<>''Active''

								
				UPdate  T  SET Status=''Active'' FROM  model.PatientProblemList AS T  Inner JOIN #GroupIcd1 G  ON G.icd10=T.icd10 AND T.Comments=G.ICD9
				WHere T.Status=''Deleted'' AND   patientid=@PatientId   and appointmentid=@AppointmentId
					
		END
		ENd
END' 
GO


