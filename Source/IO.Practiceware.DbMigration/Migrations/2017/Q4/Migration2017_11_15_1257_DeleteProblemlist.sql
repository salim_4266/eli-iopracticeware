/****** Object:  StoredProcedure [dbo].[DeleteProblem2NewPatientProblemList]    Script Date: 11/29/2017 5:55:07 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteProblem2NewPatientProblemList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteProblem2NewPatientProblemList]
GO
/****** Object:  StoredProcedure [dbo].[DeleteProblem2NewPatientProblemList]    Script Date: 11/29/2017 5:55:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteProblem2NewPatientProblemList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE  procedure [dbo].[DeleteProblem2NewPatientProblemList](
	@AppointmentId int,
	@PatientId int,
	@ClinicalType nvarchar(1),
	@EyeContext nvarchar(2),
	@Symptom nvarchar(128),
	@FindingDetail nvarchar(255),
	@ImageDescriptor nvarchar(512),
	@ImageInstructions nvarchar(512) ,
	@Status nvarchar (10) ,
	@DrawFileName nvarchar(64),
	@Highlights  nvarchar(1),
	@FollowUp nvarchar (1),
	@PermanentCondition nvarchar(1),
	@PostOpPeriod  int,
	@Surgery nvarchar(1),
	@Activity nvarchar (3),
	@LegacyClinicalDataSourceTypeId varchar(20)
	)

AS BEGIN

	--Delete from dbo.patientClinical where 
 --          PatientId=@PatientId and 
 --          ClinicalType =@ClinicalType and 
 --          EyeContext=@EyeContext and 
 --          Symptom=@Symptom and 
 --          FindingDetail=@FindingDetail and 
 --          ImageDescriptor=@ImageDescriptor and 
 --          ImageInstructions=ImageInstructions and 
 --          Status=@Status and
 --          DrawFileName=@DrawFileName and 
 --          Highlights=@Highlights and 
 --          FollowUp=@FollowUp and 
 --          PermanentCondition=@PermanentCondition and 
 --          PostOpPeriod=@PostOpPeriod and 
 --          Surgery=@Surgery and 
 --          Activity=@Activity

	DECLARE  @Icd10  nvarchar(100)
	DECLARE  @icd10_Desc  nvarchar(1000)
	DECLARE  @ConceptID  BIGINT
	DECLARE  @Term  nvarchar(1000)
	DECLARE  @AppDate  nvarchar(20)
	DECLARE @ResourceId Int
	DECLARE @side VARCHAR(200)



	SELECT  @ResourceId=ResourceId1, @AppDate=AppDate FROM Appointments  WHERE PatientId=@PatientId AND AppointmentId=@AppointmentId			
	
	--SET  @FindingDetail=  (CASE WHEN  CHARINDEX(''.'',@findingdetail,(charindex(''.'',@findingdetail)+1)) >0 THEN	SUBSTRING (@findingdetail,0, CHARINDEX(''.'',@findingdetail,(charindex(''.'',@findingdetail)+1))) ELSE @findingdetail END) 
	SET @side= Case when @EyeContext=''OD'' THEN ''Right'' WHEN @EyeContext=''OS'' THEN ''Left''  WHEN @EyeContext=''OU'' THEN ''Both''  ELSE ''Other'' END 


	--SELECT   icd10, icd10_desc,@AppointmentId,@PatientId    from TCI_ICD_Mapping where icd9_nl =@FindingDetail  
	--AND Side in(Case when @EyeContext=''OD'' THEN ''Right'' WHEN @EyeContext=''OS'' THEN ''Left''  WHEN @EyeContext=''OU'' THEN ''Both''  ELSE  ''Unspecified'' END )

	--SELECT   Top 1 @Icd10=IIX_ICD10_CODE, @icd10_Desc=IIX_DESCRIPTION  from snomed_icd10 where IIX_ICD9_CODE =@FindingDetail AND   side =@side
	--SELECT   icd10, icd10_desc, @AppointmentId,@PatientId from TCI_ICD_Mapping where icd9_nl =@FindingDetail  

	Delete from model.PatientProblemList where PatientId=@PatientId and AppointmentId=@AppointmentId AND ResourceId=@ResourceId AND Comments= @FindingDetail  and status=''Active''

			
	
	--IF(@Icd10 IS  NULL)	
	--SELECT   Top 1 @Icd10=IIX_ICD10_CODE, @icd10_Desc=IIX_DESCRIPTION  from snomed_icd10 where IIX_ICD9_CODE =@FindingDetail AND   side =''Other''


	--	--SELECT  @ResourceId=ResourceId1, @AppDate=AppDate FROM Appointments  WHERE PatientId=@PatientId AND AppointmentId=@AppointmentId
	--	--SELECT   Top 1 @Icd10=icd10, @icd10_Desc=icd10_desc  from IOPWDATA_icd10 where icd9_nl =@FindingDetail
		

	--	SET @ConceptID=0
	--	SELECT  TOP 1 @ConceptID=ConceptID, @Term=Term FROM SNOMED_2016 WHERE ICD10=@Icd10

	--	If @ConceptID=0
	--	BEGIN
	--	   SET @ConceptID=74964007
	--	   SET @Term=''Other''
	--	END

	--	If @Status=''a''
	--	BEGIN
	--	   SET @Status=''Active''
	--	END

	--	delete from model.PatientProblemList where PatientId=@PatientId and
	--		ConceptID=@ConceptID and
	--		Term=@Term and 
	--		ICD10=@ICD10 and
	--		ICD10DESCR=@icd10_Desc and
	--		Status=@Status and 
	--		ResourceId=@ResourceId;		              

End' 
END
GO
SET ANSI_PADDING ON
GO
