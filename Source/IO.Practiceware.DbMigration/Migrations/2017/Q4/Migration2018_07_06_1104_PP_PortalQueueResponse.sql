

/****** Object:  Table [MVE].[PP_PortalQueueResponse]    Script Date: 06/29/2018 13:28:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_PortalQueueResponse]') AND type in (N'U'))
BEGIN

CREATE TABLE [MVE].[PP_PortalQueueResponse](
	[ResponseID] [int] IDENTITY(1,1) NOT NULL,
	[MessageData] [nvarchar](max) NOT NULL,
	[ActionTypeLookupId] [int] NOT NULL,
	[ProcessedDate] [datetime] NOT NULL,
	[PatientNo] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ExternalId] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_PortalQueueResponse] PRIMARY KEY CLUSTERED 
(
	[ResponseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PortalQueueResponse_MessageData]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueResponse] DROP CONSTRAINT [DF_PortalQueueResponse_MessageData]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PortalQueueResponse_MessageData]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueResponse] ADD  CONSTRAINT [DF_PortalQueueResponse_MessageData]  DEFAULT ('') FOR [MessageData]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PortalQueueResponse_ActionTypeLookupId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueResponse] DROP CONSTRAINT [DF_PortalQueueResponse_ActionTypeLookupId]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PortalQueueResponse_ActionTypeLookupId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueResponse] ADD  CONSTRAINT [DF_PortalQueueResponse_ActionTypeLookupId]  DEFAULT ((0)) FOR [ActionTypeLookupId]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PortalQueueResponse_ProcessedDate]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueResponse] DROP CONSTRAINT [DF_PortalQueueResponse_ProcessedDate]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PortalQueueResponse_ProcessedDate]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueResponse] ADD  CONSTRAINT [DF_PortalQueueResponse_ProcessedDate]  DEFAULT ('1-1-1900') FOR [ProcessedDate]
END
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PortalQueueResponse_PatientNo]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueResponse] DROP CONSTRAINT [DF_PortalQueueResponse_PatientNo]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PortalQueueResponse_PatientNo]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueResponse] ADD  CONSTRAINT [DF_PortalQueueResponse_PatientNo]  DEFAULT ('') FOR [PatientNo]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PortalQueueResponse_IsActive]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueResponse] DROP CONSTRAINT [DF_PortalQueueResponse_IsActive]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PortalQueueResponse_IsActive]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueResponse] ADD  CONSTRAINT [DF_PortalQueueResponse_IsActive]  DEFAULT ((1)) FOR [IsActive]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PortalQueueResponse_ExternalId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueResponse] DROP CONSTRAINT [DF_PortalQueueResponse_ExternalId]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PortalQueueResponse_ExternalId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueResponse] ADD  CONSTRAINT [DF_PortalQueueResponse_ExternalId]  DEFAULT ('') FOR [ExternalId]
END
GO



