/****** Object:  StoredProcedure [DrFirst].[UpdateMedicationResponse]    Script Date: 3/14/2018 12:17:14 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[UpdateMedicationResponse]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DrFirst].[UpdateMedicationResponse]
GO
/****** Object:  StoredProcedure [DrFirst].[UpdateAllergyResponse]    Script Date: 3/14/2018 12:17:14 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[UpdateAllergyResponse]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DrFirst].[UpdateAllergyResponse]
GO
/****** Object:  StoredProcedure [DrFirst].[PrescriptionResponce]    Script Date: 3/14/2018 12:17:14 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[PrescriptionResponce]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DrFirst].[PrescriptionResponce]
GO
/****** Object:  StoredProcedure [DrFirst].[MedicationResponsePracticeLevel]    Script Date: 3/14/2018 12:17:14 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[MedicationResponsePracticeLevel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DrFirst].[MedicationResponsePracticeLevel]
GO
/****** Object:  StoredProcedure [DrFirst].[GetPatientMedications]    Script Date: 3/14/2018 12:17:14 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[GetPatientMedications]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DrFirst].[GetPatientMedications]
GO
/****** Object:  StoredProcedure [DrFirst].[GetPatientEncounter]    Script Date: 3/14/2018 12:17:14 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[GetPatientEncounter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DrFirst].[GetPatientEncounter]
GO
/****** Object:  StoredProcedure [DrFirst].[GetPatientDiagnosis]    Script Date: 3/14/2018 12:17:14 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[GetPatientDiagnosis]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DrFirst].[GetPatientDiagnosis]
GO
/****** Object:  StoredProcedure [DrFirst].[GetPatientAllergy]    Script Date: 3/14/2018 12:17:14 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[GetPatientAllergy]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DrFirst].[GetPatientAllergy]
GO
/****** Object:  StoredProcedure [DrFirst].[GetPatient]    Script Date: 3/14/2018 12:17:14 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[GetPatient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DrFirst].[GetPatient]
GO
/****** Object:  StoredProcedure [DrFirst].[AddUpdatePrescriptionResponsePracticeLevel]    Script Date: 3/14/2018 12:17:14 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[AddUpdatePrescriptionResponsePracticeLevel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DrFirst].[AddUpdatePrescriptionResponsePracticeLevel]
GO
/****** Object:  StoredProcedure [DrFirst].[AddUpdatePrescriptionResponse]    Script Date: 3/14/2018 12:17:14 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[AddUpdatePrescriptionResponse]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DrFirst].[AddUpdatePrescriptionResponse]
GO
/****** Object:  StoredProcedure [DrFirst].[AddUpdatePrescriptionResponse]    Script Date: 3/14/2018 12:17:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[AddUpdatePrescriptionResponse]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [DrFirst].[AddUpdatePrescriptionResponse] AS' 
END
GO

Alter  Proc  [DrFirst].[AddUpdatePrescriptionResponse]
@PatientID BIGINT,
@appointmentId BIGINT,
@xmlStringData varchar(max)
AS

BEGIN
SET NOCOUNT ON


		DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)  
  
		SELECT   @XML= CAST(@xmlStringData AS XML)  
  
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML  
  
		SELECT 				
		TraceInformationResponseMessageID ,
		RequestLastUpdateDate ,
		RequestStatusAction ,
		ResponseEndTimestamp ,
		ResponseStatus ,
		ResponseLastUpdateDate,
		--Number,

		PrescriptionDeleted ,
		PrescriptionVoided ,
		PrescriptionDenied ,
		PrescriptionRcopiaID ,
		PrescriptionExternalID,
		PrescriptionNeedsReview, 
		PrescriptionCreatedDate ,
		PrescriptionCompletedDate, 
		PrescriptionSignedDate,
		PrescriptionStopDate,
		PrescriptionLastModifiedDate,
		PrescriptionLastModifiedBy ,
		PrescriptionCompletionAction,
		SendMethod,

		PatientRcopiaID,
		PatientID,
		PatientRcopiaPracticeID ,
		PatientFirstName ,
		PatientLastName ,
		ProviderRcopiaID ,
		ProviderUsername ,
		ProviderExternalID, 
		ProviderNPI ,
		ProviderFirstName,
		ProviderLastName ,
	
		PharmacyRcopiaID ,
		PharmacyDeleted ,
		PharmacyName ,
		PharmacyCity ,
		PharmacyState ,
		PharmacyZip,
		PharmacyPhone ,
		PharmacyFax ,
		PharmacyIs24Hour ,
		PharmacyLevel3 ,
		PharmacyElectronic ,
		PharmacyMailOrder ,
		PharmacyRequiresEligibility ,
		PharmacyRetail ,
		PharmacyLongTermCare ,
		PharmacySpecialty ,
		PharmacyCanReceiveControlledSubstance 	,
		DrugNDCID ,
		DrugFirstDataBankMedID ,
		DrugRcopiaID ,
		DrugRxnormID ,
		DrugRxnormIDType ,
		DrugDescription,
		DrugBrandName ,
		DrugGenericName,
		DrugSchedule ,
		DrugBrandType,
		DrugLegendStatus ,
		DrugRoute,
		DrugForm,
		DrugStrength,

		SigAction,
		SigDose,
		SigDoseUnit,
		SigRoute,
		SigFrequency,
		SigDoseOther,
		SigDuration ,
		SigQuantity ,
		SigQuantityUnit,
		SigRefills  ,
		SigSubstitutionPermitted ,
		SigOtherNotes  ,
		SigPatientNotes,
		SigComments  
				
	
		INTO #Response  
		FROM OPENXML(@hDoc, 'RCExtResponse/Response/PrescriptionList/Prescription/Sig/Drug')  
		WITH   
		( 

		TraceInformationResponseMessageID [varchar](50) '../../../../../TraceInformation/ResponseMessageID' ,
		ResponseEndTimestamp [varchar](50) '../../../../../ResponseEndTimestamp' ,

		ResponseLastUpdateDate [varchar](50) '../../../../LastUpdateDate' ,
		ResponseStatus [varchar](50) '../../../../Status' ,
		RequestLastUpdateDate [varchar](50) '../../../../../Request/Request/LastUpdateDate' ,
		RequestStatusAction [varchar](50) '../../../../../Request/Request/Status',

		
		--Drug
		DrugNDCID [VARCHAR](100) 'NDCID',
		DrugFirstDataBankMedID [VARCHAR](100) 'FirstDataBankMedID',
		DrugRcopiaID [VARCHAR](100) 'RcopiaID',		
		DrugRxnormID [VARCHAR](50)'RxnormID',
		DrugRxnormIDType [VARCHAR](50)'RxnormIDType',
		DrugDescription [VARCHAR](500)'DrugDescription',
		DrugBrandName [varchar](200) 'BrandName',  
		DrugGenericName [varchar](200) 'GenericName',  
		DrugSchedule [varchar](50) 'Schedule',
		DrugBrandType [VARCHAR](100) 'BrandType',		
		DrugLegendStatus [VARCHAR](50) 'LegendStatus',	
		DrugRoute  [varchar](200) 'Route',
		DrugForm [varchar](100) 'Form',		
		DrugStrength  [varchar](200) 'Strength',  
		--Sig
		SigAction  [varchar](200) '../Action', 				 	 
		SigDose  [varchar](200) '../Dose',
		SigDoseUnit  [varchar](200) '../DoseUnit',
		SigRoute  [varchar](200) '../Route',   
		SigFrequency  [varchar](50) '../DoseTiming',  
		SigDoseOther  [varchar](200) '../DoseOther',  
		SigDuration  [varchar](50) '../Duration',  
		SigQuantity  [varchar](50) '../Quantity',  		
		SigQuantityUnit  [varchar](50) '../QuantityUnit',  		
		SigRefills  [varchar](50) '../Refills',  
		SigSubstitutionPermitted  [varchar](10) '../SubstitutionPermitted',
		SigOtherNotes  [varchar](200) '../OtherNotes',  
		SigPatientNotes  [varchar](200) '../PatientNotes', 
		SigComments  [varchar](200) '../Comments', 
		--Prescription
		
		
		PrescriptionDeleted [varchar](50) '../../Deleted' ,
		PrescriptionVoided [varchar](50) '../../Voided' ,
		PrescriptionDenied [varchar](50) '../../Denied' ,
		PrescriptionRcopiaID [varchar](50) '../../RcopiaID' ,
		PrescriptionExternalID[varchar](50) '../../ExternalID' ,
		PrescriptionNeedsReview [varchar](50) '../../NeedsReview',		
		PrescriptionCreatedDate [varchar](50) '../../CreatedDate',
		PrescriptionCompletedDate [varchar](50) '../../CompletedDate',
		PrescriptionSignedDate [varchar](50) '../../SignedDate',		
		PrescriptionStopDate [varchar](50) '../../StopDate',		
		PrescriptionLastModifiedDate [varchar](50) '../../LastModifiedDate',
		PrescriptionLastModifiedBy [varchar](50) '../../LastModifiedBy',
		PrescriptionCompletionAction [varchar](50)'../../CompletionAction',
		SendMethod [varchar](50)'../../SendMethod',
		
	
		----Patient
		PatientRcopiaID [varchar](50) '../../Patient/RcopiaID',  
		PatientID [varchar](50) '../../Patient/ExternalID', 
		PatientRcopiaPracticeID [varchar](50) '../../Patient/RcopiaPracticeID', 
		PatientFirstName [varchar](50) '../../Patient/FirstName',
		PatientLastName [varchar](50) '../../Patient/LastName',
		--Provider
		ProviderRcopiaID [varchar](50) '../../Provider/RcopiaID',
		ProviderUsername [varchar](50) '../../Provider/Username',
		ProviderExternalID [varchar](50) '../../Provider/ExternalID',
		ProviderNPI [varchar](50) '../../Provider/NPI',
		ProviderFirstName [varchar](50) '../../Provider/FirstName',		
		ProviderLastName [varchar](50) '../../Provider/LastName',
		--Pharmacy
		PharmacyRcopiaID [varchar](50) '../../Pharmacy/RcopiaID',
		PharmacyDeleted [varchar](50) '../../Pharmacy/Deleted',
		PharmacyName [varchar](50) '../../Pharmacy/Name',
		PharmacyCity [varchar](50) '../../Pharmacy/City',
		PharmacyState [varchar](50) '../../Pharmacy/State',
		PharmacyZip [varchar](50) '../../Pharmacy/Zip',
		PharmacyPhone [varchar](50) '../../Pharmacy/Phone',
		PharmacyFax [varchar](50) '../../Pharmacy/Fax',
		PharmacyIs24Hour [varchar](50) '../../Pharmacy/Is24Hour',
		PharmacyLevel3 [varchar](50) '../../Pharmacy/Level3',
		PharmacyElectronic [varchar](50) '../../Pharmacy/Electronic',
		PharmacyMailOrder [varchar](50) '../../Pharmacy/MailOrder',
		PharmacyRequiresEligibility [varchar](50) '../../Pharmacy/RequiresEligibility',
		PharmacyRetail [varchar](50) '../../Pharmacy/Retail',
		PharmacyLongTermCare [varchar](50) '../../Pharmacy/LongTermCare',
		PharmacySpecialty [varchar](50) '../../Pharmacy/Specialty',
		PharmacyCanReceiveControlledSubstance [varchar](50) '../../Pharmacy/CanReceiveControlledSubstance'	
		)  
		EXEC sp_xml_removedocument @hDoc 
		 		   
		
		INSERT INTO [Drfirst].eRx_Prescriptions_Response (TraceInformationResponseMessageID,RequestLastUpdateDate , RequestStatusAction , ResponseEndTimestamp , ResponseStatus , 
		ResponseLastUpdateDate, PrescriptionDeleted , PrescriptionVoided , PrescriptionDenied , PrescriptionRcopiaID , PrescriptionExternalID, PrescriptionNeedsReview, 
		PrescriptionCreatedDate , PrescriptionCompletedDate, PrescriptionSignedDate, PrescriptionStopDate, PrescriptionLastModifiedDate, PrescriptionLastModifiedBy,PrescriptionCompletionAction,
		PatientRcopiaID, PatientID, PatientRcopiaPracticeID , PatientFirstName , PatientLastName , ProviderRcopiaID , ProviderUsername , ProviderExternalID, ProviderNPI ,
		ProviderFirstName, ProviderLastName , PharmacyRcopiaID , PharmacyDeleted , PharmacyName , PharmacyCity , PharmacyState , PharmacyZip, PharmacyPhone , PharmacyFax ,
		PharmacyIs24Hour , PharmacyLevel3 , PharmacyElectronic , PharmacyMailOrder , PharmacyRequiresEligibility , PharmacyRetail , PharmacyLongTermCare , PharmacySpecialty ,
		PharmacyCanReceiveControlledSubstance , DrugNDCID , DrugFirstDataBankMedID , DrugRcopiaID , DrugRxnormID , DrugRxnormIDType , DrugDescription, DrugBrandName ,
		DrugGenericName, DrugSchedule , DrugBrandType, DrugLegendStatus , DrugRoute, DrugForm, DrugStrength , SigAction, SigDose , SigDoseUnit, SigRoute , SigFrequency, 
		SigDoseOther, SigDuration , SigQuantity , SigQuantityUnit, SigRefills , SigSubstitutionPermitted , SigOtherNotes , SigPatientNotes, SigComments,SendMethod)
		select TraceInformationResponseMessageID, RequestLastUpdateDate , RequestStatusAction , ResponseEndTimestamp , ResponseStatus , ResponseLastUpdateDate, 
		PrescriptionDeleted , PrescriptionVoided , PrescriptionDenied , PrescriptionRcopiaID , @appointmentId, PrescriptionNeedsReview, PrescriptionCreatedDate , 
		PrescriptionCompletedDate, PrescriptionSignedDate, PrescriptionStopDate, PrescriptionLastModifiedDate, PrescriptionLastModifiedBy , PrescriptionCompletionAction,PatientRcopiaID, 
		PatientID, PatientRcopiaPracticeID , PatientFirstName , PatientLastName , ProviderRcopiaID , ProviderUsername , ProviderExternalID, ProviderNPI , 
		ProviderFirstName, ProviderLastName , PharmacyRcopiaID , PharmacyDeleted , PharmacyName , PharmacyCity , PharmacyState , PharmacyZip, PharmacyPhone , 
		PharmacyFax , PharmacyIs24Hour , PharmacyLevel3 , PharmacyElectronic , PharmacyMailOrder , PharmacyRequiresEligibility , PharmacyRetail , 
		PharmacyLongTermCare , PharmacySpecialty , PharmacyCanReceiveControlledSubstance , 
		DrugNDCID , DrugFirstDataBankMedID , DrugRcopiaID , DrugRxnormID , DrugRxnormIDType , DrugDescription, DrugBrandName , DrugGenericName,
		DrugSchedule , DrugBrandType, DrugLegendStatus , DrugRoute, DrugForm, DrugStrength ,
		SigAction, SigDose , SigDoseUnit, SigRoute , SigFrequency, SigDoseOther, SigDuration , SigQuantity , SigQuantityUnit, SigRefills ,
		SigSubstitutionPermitted , SigOtherNotes , SigPatientNotes, SigComments,SendMethod from #Response WHERE PrescriptionRcopiaID NOT IN(SELECT Tmp.PrescriptionRcopiaID FROM [Drfirst].eRx_Prescriptions_Response Tmp WHERE PatientID=@PatientID AND  PrescriptionExternalID=@appointmentId AND Tmp.RequestStatusAction=#Response.RequestStatusAction)
				
		IF OBJECT_ID('tempdb..#NewMeds') IS NOT NULL  
			DROP TABLE #NewMeds 	

		IF OBJECT_ID('tempdb..#Response') IS NOT NULL  
		DROP TABLE #Response 
End



GO
/****** Object:  StoredProcedure [DrFirst].[AddUpdatePrescriptionResponsePracticeLevel]    Script Date: 3/14/2018 12:17:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[AddUpdatePrescriptionResponsePracticeLevel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [DrFirst].[AddUpdatePrescriptionResponsePracticeLevel] AS' 
END
GO

--drop table #Response

Alter  Proc  [DrFirst].[AddUpdatePrescriptionResponsePracticeLevel]
@xmlStringData varchar(max)=''
AS

BEGIN
SET NOCOUNT ON

IF(@xmlStringData<>'')
BEGIN

		DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)    
		SELECT   @XML= CAST(@xmlStringData AS XML)    
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML    
		SELECT 				
		TraceInformationResponseMessageID ,
		RequestLastUpdateDate ,
		RequestStatusAction ,
		ResponseEndTimestamp ,
		ResponseStatus ,
		ResponseLastUpdateDate,
		--Number,

		PrescriptionDeleted ,
		PrescriptionVoided ,
		PrescriptionDenied ,
		PrescriptionRcopiaID ,
		PrescriptionExternalID,
		PrescriptionNeedsReview, 
		PrescriptionCreatedDate ,
		PrescriptionCompletedDate, 
		PrescriptionSignedDate,
		PrescriptionStopDate,
		PrescriptionLastModifiedDate,
		PrescriptionLastModifiedBy ,
		PrescriptionCompletionAction,

		PatientRcopiaID,
		PatientID,
		PatientRcopiaPracticeID ,
		PatientFirstName ,
		PatientLastName ,
		ProviderRcopiaID ,
		ProviderUsername ,
		ProviderExternalID, 
		ProviderNPI ,
		ProviderFirstName,
		ProviderLastName ,
	
		PharmacyRcopiaID ,
		PharmacyDeleted ,
		PharmacyName ,
		PharmacyCity ,
		PharmacyState ,
		PharmacyZip,
		PharmacyPhone ,
		PharmacyFax ,
		PharmacyIs24Hour ,
		PharmacyLevel3 ,
		PharmacyElectronic ,
		PharmacyMailOrder ,
		PharmacyRequiresEligibility ,
		PharmacyRetail ,
		PharmacyLongTermCare ,
		PharmacySpecialty ,
		PharmacyCanReceiveControlledSubstance 	,
		DrugNDCID ,
		DrugFirstDataBankMedID ,
		DrugRcopiaID ,
		DrugRxnormID ,
		DrugRxnormIDType ,
		DrugDescription,
		DrugBrandName ,
		DrugGenericName,
		DrugSchedule ,
		DrugBrandType,
		DrugLegendStatus ,
		DrugRoute,
		DrugForm,
		DrugStrength,

		SigAction,
		SigDose,
		SigDoseUnit,
		SigRoute,
		SigFrequency,
		SigDoseOther,
		SigDuration ,
		SigQuantity ,
		SigQuantityUnit,
		SigRefills  ,
		SigSubstitutionPermitted ,
		SigOtherNotes  ,
		SigPatientNotes,
		SigComments  
				
	
		INTO #Response  
		FROM OPENXML(@hDoc, 'RCExtResponse/Response/PrescriptionList/Prescription/Sig/Drug')  
		WITH   
		(  			
		
		TraceInformationResponseMessageID [varchar](50) '../../../../../TraceInformation/ResponseMessageID' ,
		ResponseEndTimestamp [varchar](50) '../../../../../ResponseEndTimestamp' ,

		ResponseLastUpdateDate [varchar](50) '../../../../LastUpdateDate' ,
		ResponseStatus [varchar](50) '../../../../Status' ,
		RequestLastUpdateDate [varchar](50) '../../../../../Request/Request/LastUpdateDate' ,
		RequestStatusAction [varchar](50) '../../../../../Request/Request/Status',
		--Number [varchar](50) '../../Number',

		--ClinicalID [varchar](50) '../../ExternalID',	
		--Drug
		DrugNDCID [VARCHAR](100) 'NDCID',
		DrugFirstDataBankMedID [VARCHAR](100) 'FirstDataBankMedID',
		DrugRcopiaID [VARCHAR](100) 'RcopiaID',		
		DrugRxnormID [VARCHAR](50)'RxnormID',
		DrugRxnormIDType [VARCHAR](50)'RxnormIDType',
		DrugDescription [VARCHAR](500)'DrugDescription',
		DrugBrandName [varchar](200) 'BrandName',  
		DrugGenericName [varchar](200) 'GenericName',  
		DrugSchedule [varchar](50) 'Schedule',
		DrugBrandType [VARCHAR](100) 'BrandType',		
		DrugLegendStatus [VARCHAR](50) 'LegendStatus',	
		DrugRoute  [varchar](200) 'Route',
		DrugForm [varchar](100) 'Form',		
		DrugStrength  [varchar](200) 'Strength',  
		--Sig
		SigAction  [varchar](200) '../Action', 				 	 
		SigDose  [varchar](200) '../Dose',
		SigDoseUnit  [varchar](200) '../DoseUnit',
		SigRoute  [varchar](200) '../Route',   
		SigFrequency  [varchar](50) '../DoseTiming',  
		SigDoseOther  [varchar](200) '../DoseOther',  
		SigDuration  [varchar](50) '../Duration',  
		SigQuantity  [varchar](50) '../Quantity',  		
		SigQuantityUnit  [varchar](50) '../QuantityUnit',  		
		SigRefills  [varchar](50) '../Refills',  
		SigSubstitutionPermitted  [varchar](10) '../SubstitutionPermitted',
		SigOtherNotes  [varchar](200) '../OtherNotes',  
		SigPatientNotes  [varchar](200) '../PatientNotes', 
		SigComments  [varchar](200) '../Comments', 
		--Prescription
		
		
		PrescriptionDeleted [varchar](50) '../../Deleted' ,
		PrescriptionVoided [varchar](50) '../../Voided' ,
		PrescriptionDenied [varchar](50) '../../Denied' ,
		PrescriptionRcopiaID [varchar](50) '../../RcopiaID' ,
		PrescriptionExternalID[varchar](50) '../../ExternalID' ,
		PrescriptionNeedsReview [varchar](50) '../../NeedsReview',		
		PrescriptionCreatedDate [varchar](50) '../../CreatedDate',
		PrescriptionCompletedDate [varchar](50) '../../CompletedDate',
		PrescriptionSignedDate [varchar](50) '../../SignedDate',		
		PrescriptionStopDate [varchar](50) '../../StopDate',		
		PrescriptionLastModifiedDate [varchar](50) '../../LastModifiedDate',
		PrescriptionLastModifiedBy [varchar](50) '../../LastModifiedBy',
		PrescriptionCompletionAction [varchar](50)'../../CompletionAction',
	
		----Patient
		PatientRcopiaID [varchar](50) '../../Patient/RcopiaID',  
		PatientID [varchar](50) '../../Patient/ExternalID', 
		PatientRcopiaPracticeID [varchar](50) '../../Patient/RcopiaPracticeID', 
		PatientFirstName [varchar](50) '../../Patient/FirstName',
		PatientLastName [varchar](50) '../../Patient/LastName',
		--Provider
		ProviderRcopiaID [varchar](50) '../../Provider/RcopiaID',
		ProviderUsername [varchar](50) '../../Provider/Username',
		ProviderExternalID [varchar](50) '../../Provider/ExternalID',
		ProviderNPI [varchar](50) '../../Provider/NPI',
		ProviderFirstName [varchar](50) '../../Provider/FirstName',		
		ProviderLastName [varchar](50) '../../Provider/LastName',
		--Pharmacy
		PharmacyRcopiaID [varchar](50) '../../Pharmacy/RcopiaID',
		PharmacyDeleted [varchar](50) '../../Pharmacy/Deleted',
		PharmacyName [varchar](50) '../../Pharmacy/Name',
		PharmacyCity [varchar](50) '../../Pharmacy/City',
		PharmacyState [varchar](50) '../../Pharmacy/State',
		PharmacyZip [varchar](50) '../../Pharmacy/Zip',
		PharmacyPhone [varchar](50) '../../Pharmacy/Phone',
		PharmacyFax [varchar](50) '../../Pharmacy/Fax',
		PharmacyIs24Hour [varchar](50) '../../Pharmacy/Is24Hour',
		PharmacyLevel3 [varchar](50) '../../Pharmacy/Level3',
		PharmacyElectronic [varchar](50) '../../Pharmacy/Electronic',
		PharmacyMailOrder [varchar](50) '../../Pharmacy/MailOrder',
		PharmacyRequiresEligibility [varchar](50) '../../Pharmacy/RequiresEligibility',
		PharmacyRetail [varchar](50) '../../Pharmacy/Retail',
		PharmacyLongTermCare [varchar](50) '../../Pharmacy/LongTermCare',
		PharmacySpecialty [varchar](50) '../../Pharmacy/Specialty',
		PharmacyCanReceiveControlledSubstance [varchar](50) '../../Pharmacy/CanReceiveControlledSubstance'	
		)  
		EXEC sp_xml_removedocument @hDoc 
		 		   
					

		INSERT INTO [Drfirst].eRx_Prescriptions_Response (TraceInformationResponseMessageID,RequestLastUpdateDate , RequestStatusAction , ResponseEndTimestamp , ResponseStatus , ResponseLastUpdateDate, PrescriptionDeleted , PrescriptionVoided , PrescriptionDenied , PrescriptionRcopiaID , PrescriptionExternalID, PrescriptionNeedsReview, PrescriptionCreatedDate , PrescriptionCompletedDate, PrescriptionSignedDate, PrescriptionStopDate, PrescriptionLastModifiedDate, PrescriptionLastModifiedBy, PrescriptionCompletionAction, PatientRcopiaID, PatientID, PatientRcopiaPracticeID , PatientFirstName , PatientLastName , ProviderRcopiaID , ProviderUsername , ProviderExternalID, ProviderNPI , ProviderFirstName, ProviderLastName , PharmacyRcopiaID , PharmacyDeleted , PharmacyName , PharmacyCity , PharmacyState , PharmacyZip, PharmacyPhone , PharmacyFax , PharmacyIs24Hour , PharmacyLevel3 , PharmacyElectronic , PharmacyMailOrder , PharmacyRequiresEligibility , PharmacyRetail , PharmacyLongTermCare , PharmacySpecialty , PharmacyCanReceiveControlledSubstance , DrugNDCID , DrugFirstDataBankMedID , DrugRcopiaID , DrugRxnormID , DrugRxnormIDType , DrugDescription, DrugBrandName , DrugGenericName, DrugSchedule , DrugBrandType, DrugLegendStatus , DrugRoute, DrugForm, DrugStrength , SigAction, SigDose , SigDoseUnit, SigRoute , SigFrequency, SigDoseOther, SigDuration , SigQuantity , SigQuantityUnit, SigRefills , SigSubstitutionPermitted , SigOtherNotes , SigPatientNotes, SigComments)
		select TraceInformationResponseMessageID, RequestLastUpdateDate , RequestStatusAction , ResponseEndTimestamp , ResponseStatus , ResponseLastUpdateDate, 
		PrescriptionDeleted , PrescriptionVoided , PrescriptionDenied , PrescriptionRcopiaID , PrescriptionExternalID, PrescriptionNeedsReview, PrescriptionCreatedDate , 
		PrescriptionCompletedDate, PrescriptionSignedDate, PrescriptionStopDate, PrescriptionLastModifiedDate, PrescriptionLastModifiedBy , PrescriptionCompletionAction,PatientRcopiaID, 
		PatientID, PatientRcopiaPracticeID , PatientFirstName , PatientLastName , ProviderRcopiaID , ProviderUsername , ProviderExternalID, ProviderNPI , 
		ProviderFirstName, ProviderLastName , PharmacyRcopiaID , PharmacyDeleted , PharmacyName , PharmacyCity , PharmacyState , PharmacyZip, PharmacyPhone , 
		PharmacyFax , PharmacyIs24Hour , PharmacyLevel3 , PharmacyElectronic , PharmacyMailOrder , PharmacyRequiresEligibility , PharmacyRetail , 
		PharmacyLongTermCare , PharmacySpecialty , PharmacyCanReceiveControlledSubstance , 
		DrugNDCID , DrugFirstDataBankMedID , DrugRcopiaID , DrugRxnormID , DrugRxnormIDType , DrugDescription, DrugBrandName , DrugGenericName,
		DrugSchedule , DrugBrandType, DrugLegendStatus , DrugRoute, DrugForm, DrugStrength ,
		SigAction, SigDose , SigDoseUnit, SigRoute , SigFrequency, SigDoseOther, SigDuration , SigQuantity , SigQuantityUnit, SigRefills ,
		SigSubstitutionPermitted , SigOtherNotes , SigPatientNotes, SigComments from #Response 
		WHERE PrescriptionRcopiaID NOT IN(SELECT PrescriptionRcopiaID FROM [Drfirst].eRx_Prescriptions_Response PR 
		WHERE  PR.RequestStatusAction=#Response.RequestStatusAction AND PR.PatientID=#Response.PatientID)

		IF OBJECT_ID('tempdb..#NewMeds') IS NOT NULL  
			DROP TABLE #NewMeds 
	
		IF OBJECT_ID('tempdb..#Response') IS NOT NULL  
					DROP TABLE #Response 

	End
END

GO
/****** Object:  StoredProcedure [DrFirst].[GetPatient]    Script Date: 3/14/2018 12:17:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[GetPatient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [DrFirst].[GetPatient] AS' 
END
GO



-- =============================================
-- Author:		<Author,,salim>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--DrFirst.GetPatient 11138
ALTER PROCEDURE [DrFirst].[GetPatient]
@PatientID BIGINT
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT  DISTINCT 
	P.[Id] AS PatientId
	,AP.[AppointmentId]
	,Ap.resourceid1 as ResourceId
	,P.[FirstName]
    ,P.[LastName]
    ,convert(varchar(200), P.[DateOfBirth],101)as [DateOfBirth]
    , CASE WHEN GE.Code='UNK' THEN 'U' ELSE  GE.Code END  as [GenderId]            
	  ,PD.[Address]     
	   ,PD.Suite  as [Address2]
      ,PD.[City]
      ,PD.[State]
      ,PD.[Zip]
       , COALESCE(NULLIF(Homephone, ''), NULLIF(BusinessPhone, ''),NULLIF(CellPhone, ''), NULLIF(EmployerPhone, '')) AS HomePhone
       
	 --INTO #TmpPatient	     
  FROM [model].[Patients] P 
  INNER JOIN [model].SexType_Enumeration GE ON GE.Id=P.SexID
  left JOIN [model].[PatientStatus_Enumeration] PE ON P.[PatientStatusId]=PE.ID
  --left JOIN [model].[ServiceLocationCodes] SLC ON SLC.Id= P.[PreferredServiceLocationId]
  LEFT JOIN  [dbo].[PatientDemographics]  PD ON PD.Patientid=P.ID
  left JOIN dbo.appointments AP ON AP.PatientId=P.Id   AND AP.ResourceId1 <>'0'

  WHERE 
 
  P.ID=@PatientID

END





GO
/****** Object:  StoredProcedure [DrFirst].[GetPatientAllergy]    Script Date: 3/14/2018 12:17:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[GetPatientAllergy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [DrFirst].[GetPatientAllergy] AS' 
END
GO

ALTER   PROCEDURE [DRFIRST].[GetPatientAllergy]
(  
@PatientID BIGINT,
@appointmentID BIGINT=0
)  
AS  
Begin


--declare @PatientID bigint=1497
--declare @appointmentId bigint=14934
   
   Select pc.PatientId, pc.AppointmentId, pc1.ClinicalId,
   CASE SUBSTRING(pc1.FindingDetail, 1, 1)  
   WHEN '*'  
    THEN CASE  
     WHEN pc1.FindingDetail like '%=%'  
      THEN SUBSTRING(pc1.FindingDetail, 2, CHARINDEX('=', pc1.FindingDetail) - 2)  
     ELSE  
      SUBSTRING(pc1.findingdetail, 2, LEN(pc1.findingdetail)-1)  
     END  
   ELSE CASE  
     WHEN pc1.FindingDetail like '%=%'  
      THEN SUBSTRING(pc1.FindingDetail, 1, CHARINDEX('=', pc1.FindingDetail) - 1)  
     ELSE  
      SUBSTRING(pc1.FindingDetail, 1, LEN(pc1.FindingDetail))  
     END  
   END 
   
   AS AllergyName,
   CASE WHEN [dbo].[fn_Occurences] ('/',pc.FindingDetail,2)>0 THEN 
     CASE WHEN 
   Substring(pc.FindingDetail,[dbo].[fn_Occurences] ('/',pc.FindingDetail,1)+1,([dbo].[fn_Occurences] ('/',pc.FindingDetail,2)-[dbo].[fn_Occurences] ('/',pc.FindingDetail,1)-1))='NONE' 
   THEN ''  
   ELSE Substring(pc.FindingDetail,[dbo].[fn_Occurences] ('/',pc.FindingDetail,1)+1,([dbo].[fn_Occurences] ('/',pc.FindingDetail,2)-[dbo].[fn_Occurences] ('/',pc.FindingDetail,1)-1))
  END
	 ELSE '' END as Reaction ,
	   CASE WHEN [dbo].[fn_Occurences] ('/',pc.FindingDetail,2)>0 THEN 
	  CASE WHEN 
   Substring(pc.FindingDetail,[dbo].[fn_Occurences] ('/',pc.FindingDetail,2)+1,([dbo].[fn_Occurences] ('/',pc.FindingDetail,2)-[dbo].[fn_Occurences] ('/',pc.FindingDetail,1)-1))='NONE' 
   THEN ''  
   ELSE Substring(pc.FindingDetail,[dbo].[fn_Occurences] ('/',pc.FindingDetail,2)+1,([dbo].[fn_Occurences] ('/',pc.FindingDetail,2)-[dbo].[fn_Occurences] ('/',pc.FindingDetail,1)-1))
  END
	
	 ELSE '' END  as Severity,

     pc.Status as AllergyStatus , pc.DrawFileName ,pc.FindingDetail 
	 INTO  #TmpAlg 
	 From dbo.PatientClinical pc with (nolock) 
   inner join PatientClinical pc1 on pc1.ClinicalId=pc.ClinicalId+1
   Where pc.ClinicalType in ('H', 'C') and pc.Symptom like '%/ANY DRUG ALLERGIES%' 
   AND pc.Symptom not in (']ALLERGY STATUS', '[ALLERGY REACTION', ']ALLERGY SEVERITY') 
   AND  CHARINDEX('> <', pc1.findingDetail)<=0
    AND pc1.Symptom ='?ALLERGY'
   AND pc.PatientId = @PatientID --and pc.AppointmentID=@appointmentID

   UNION
 
  Select pc.PatientId, pc.AppointmentId, pc.ClinicalId,
   CASE SUBSTRING(pc.FindingDetail, 1, 1)  
   WHEN '*'  
    THEN CASE  
     WHEN pc.FindingDetail like '%=%'  
      THEN SUBSTRING(pc.FindingDetail, 2, CHARINDEX('=', pc.FindingDetail) - 2)  
     ELSE  
      SUBSTRING(pc.findingdetail, 2, LEN(pc.findingdetail)-1)  
     END  
   ELSE CASE  
     WHEN pc.FindingDetail like '%=%'  
      THEN SUBSTRING(pc.FindingDetail, 1, CHARINDEX('=', pc.FindingDetail) - 1)  
     ELSE  
      SUBSTRING(pc.FindingDetail, 1, LEN(pc.FindingDetail))  
     END  
   END 
   
   AS AllergyName,
    CASE WHEN [dbo].[fn_Occurences] ('/',pc.FindingDetail,2)>0 THEN 
     CASE WHEN 
   Substring(pc.FindingDetail,[dbo].[fn_Occurences] ('/',pc.FindingDetail,1)+1,([dbo].[fn_Occurences] ('/',pc.FindingDetail,2)-[dbo].[fn_Occurences] ('/',pc.FindingDetail,1)-1))='NONE' 
   THEN ''  
   ELSE Substring(pc.FindingDetail,[dbo].[fn_Occurences] ('/',pc.FindingDetail,1)+1,([dbo].[fn_Occurences] ('/',pc.FindingDetail,2)-[dbo].[fn_Occurences] ('/',pc.FindingDetail,1)-1))
  END
	 ELSE '' END  as Reaction,

	 CASE WHEN [dbo].[fn_Occurences] ('/',pc.FindingDetail,2)>0 THEN 
	  CASE WHEN 
   Substring(pc.FindingDetail,[dbo].[fn_Occurences] ('/',pc.FindingDetail,2)+1,([dbo].[fn_Occurences] ('/',pc.FindingDetail,2)-[dbo].[fn_Occurences] ('/',pc.FindingDetail,1)-1))='NONE' 
   THEN ''  
   ELSE Substring(pc.FindingDetail,[dbo].[fn_Occurences] ('/',pc.FindingDetail,2)+1,([dbo].[fn_Occurences] ('/',pc.FindingDetail,2)-[dbo].[fn_Occurences] ('/',pc.FindingDetail,1)))
  END
	
	  ELSE '' END  as Severity,

     pc.Status as AllergyStatus , pc.DrawFileName ,pc.FindingDetail
    From dbo.PatientClinical pc with (nolock) 
	 LEFT JOIN [PatientClinical] PC1 on PC.ClinicalId+1=PC1.ClinicalId
	    where pc.ClinicalType in ('H', 'C')  and PC.Symptom like '%/ANY DRUG ALLERGIES%'
		AND pc.Symptom not in (']ALLERGY STATUS', '[ALLERGY REACTION', ']ALLERGY SEVERITY') 
	    AND  ISNULL(PC1.Symptom,'') <> '?ALLERGY'
		  AND pc.PatientId = @PatientID --and pc.AppointmentID=@appointmentId


   --end

union 
Select pc.PatientId, pc.AppointmentId, pc.ClinicalId,
 CASE SUBSTRING(pc.FindingDetail, 1, 1)  
   WHEN '*'  
    THEN CASE  
     WHEN pc.FindingDetail like '%=%'  
      THEN SUBSTRING(pc.FindingDetail, 2, CHARINDEX('=', pc.FindingDetail) - 2)  
     ELSE  
      SUBSTRING(pc.findingdetail, 2, LEN(pc.findingdetail)-1)  
     END  
   ELSE CASE  
     WHEN pc.FindingDetail like '%=%'  
      THEN SUBSTRING(pc.FindingDetail, 1, CHARINDEX('=', pc.FindingDetail) - 1)  
     ELSE  
      SUBSTRING(pc.FindingDetail, 1, LEN(pc.FindingDetail))  
     END  
   END 
   
   AS AllergyName, 
    CASE WHEN [dbo].[fn_Occurences] ('/',pc.FindingDetail,2)>0 THEN 
   CASE WHEN 
   Substring(FindingDetail,[dbo].[fn_Occurences] ('/',FindingDetail,1)+1,([dbo].[fn_Occurences] ('/',FindingDetail,2)-[dbo].[fn_Occurences] ('/',FindingDetail,1)-1))='NONE' 
   THEN ''  
   ELSE Substring(FindingDetail,[dbo].[fn_Occurences] ('/',FindingDetail,1)+1,([dbo].[fn_Occurences] ('/',FindingDetail,2)-[dbo].[fn_Occurences] ('/',FindingDetail,1)-1))
  END
	
	 ELSE '' END   as Reaction,
	  CASE WHEN [dbo].[fn_Occurences] ('/',pc.FindingDetail,2)>0 THEN 
	  CASE WHEN 
   Substring(FindingDetail,[dbo].[fn_Occurences] ('/',FindingDetail,2)+1,([dbo].[fn_Occurences] ('/',FindingDetail,2)-[dbo].[fn_Occurences] ('/',FindingDetail,1)-1))='NONE' 
   THEN ''  
   ELSE Substring(FindingDetail,[dbo].[fn_Occurences] ('/',FindingDetail,2)+1,([dbo].[fn_Occurences] ('/',FindingDetail,2)-[dbo].[fn_Occurences] ('/',FindingDetail,1)-1))
  END
	
	 ELSE '' END   as Severity,

    pc.Status as AllergyStatus , pc.DrawFileName,pc.FindingDetail
 
	 From dbo.PatientClinical pc with (nolock) 
   Where pc.ClinicalType in ('H', 'C') and pc.Symptom like '/ALLERGIES%' 
   and pc.Symptom not in (']ALLERGY STATUS', '[ALLERGY REACTION', ']ALLERGY SEVERITY')
   AND  CHARINDEX('> <', pc.findingDetail)<=0
   AND  CHARINDEX('>/NONE/', pc.FindingDetail)<=0
    and pc.PatientId = @PatientID --and pc.AppointmentID=@appointmentID
 
	;with CteAllergy(ClinicalId, PatientId,	appointmentid, AllergyName, AllergyStatus, DrawFileName,FindingDetail,  Reaction ,Severity,[Rank]) 
	as (
	Select ClinicalId, PatientId, AppointmentId, AllergyName, AllergyStatus,  DrawFileName,FindingDetail ,Reaction,Severity,
	Row_number() over(Partition by AllergyName , AllergyStatus order by ClinicalId  ASC) as [Rank] 
	from  #TmpAlg
	)

  Select  DISTINCT ISNULL(EVD_RXN_RXCUI, NULL) AS RxnormID, ISNULL(FDBCompositeAllergyId , '') as FirstDataBankMedID, PatientId, AppointmentId, 
  ClinicalId, AllergyName,  AllergyStatus ,FindingDetail,  Reaction,Severity,[Rank], ISNULL(CA.ConceptType,'') as ConceptType, ISNULL(CONVERT(VARCHAR(100),FDBCONCEPTID),'') as FDBCONCEPTID
  from CteAllergy AL 
  Left join fdb.tblCompositeAllergy CA on CA.Description =CASE WHEN CHARINDEX('-',AL.AllergyName)>0 THEN Substring(AL.AllergyName,1,Charindex('-',AL.AllergyName)-1) ELSE AL.AllergyName END 
  LEft Join dbo.AllergyRxMapping ARX ON ARX.FDBCompositeAllergyId=CA.CompositeAllergyID ORDER BY ClinicalId
			
IF OBJECT_ID('tempdb..#TmpAlg') IS NOT NULL  
DROP TABLE #TmpAlg 

END   



GO
/****** Object:  StoredProcedure [DrFirst].[GetPatientDiagnosis]    Script Date: 3/14/2018 12:17:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[GetPatientDiagnosis]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [DrFirst].[GetPatientDiagnosis] AS' 
END
GO


ALTER PROCEDURE [DrFirst].[GetPatientDiagnosis]

	@PatientID BIGINT,

	@appointmentID BIGINT

AS

BEGIN

	SET NOCOUNT ON;

	SELECT

	DISTINCT PC.ClinicalId,

	PC.PatientId,

	PC.appointmentid as EncounterId,

	A.appdate as EncounterDate,

	Case 
	When PC.FindingDetail like '%.%.%' Then 
	Substring(PC.FindingDetail,0,Len(PC.FindingDetail) - CHARINDEX('.', REVERSE(PC.FindingDetail)) + 1)
	Else PC.FindingDetail End as FindingDetail,       

	PD.ReviewSystemLingo as Description,

	PC.ImageDescriptor,

	(CASE WHEN  CHARINDEX('.',PC.findingdetail,(charindex('.',PC.findingdetail)+1)) >0 THEN

	SUBSTRING (PC.findingdetail,0, CHARINDEX('.',PC.findingdetail,(charindex('.',PC.findingdetail)+1))) ELSE PC.findingdetail END) as Code

	Into #TmpDiagnosis FROM PatientClinical PC

	INNER JOIN [dm].[PrimaryDiagnosisTable] PD 

	ON PD.diagnosisNextlevel=PC.findingdetail

	INNER JOIN dbo.Appointments  A

	ON A.appointmentid=PC.appointmentid

	WHERE PC.findingdetail LIKE '%[0-9]%' and PC.FindingDetail not in ('0001','P944')

	AND PC.PatientID=@PatientID 

	AND PC.ClinicalType = 'Q' 

	AND PC.appointmentid=@appointmentID

	;with CTEDignosis(ClinicalId, PatientId, EncounterId, EncounterDate, FindingDetail, Description, Code, ImageDescriptor, [Rank]) as 

	(Select ClinicalId, PatientId, EncounterId, EncounterDate, FindingDetail, Description, Code, ImageDescriptor,

	Row_number() over(Partition by Code order by Code) as [Rank] from  #TmpDiagnosis)

	Select ClinicalId, PatientId, EncounterId,EncounterDate, FindingDetail, Description, Code, ImageDescriptor from CTEDignosis Ds Inner join Icd9.ICD9 b on Ds.Code = b.DiagnosisCode    WHERE [Rank]=1 

	Union All

	Select ClinicalId, PatientId, EncounterId, '' as EncounterDate, FindingDetail, Description, Code, 'D' as ImageDescriptor from DrFirst.ErxDiagnosisMapping 
	Where PatientId = @PatientId and EncounterId = @appointmentID 

	IF OBJECT_ID('tempdb..#TmpDiagnosis') IS NOT NULL  

	DROP TABLE #TmpDiagnosis 

END





GO
/****** Object:  StoredProcedure [DrFirst].[GetPatientEncounter]    Script Date: 3/14/2018 12:17:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[GetPatientEncounter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [DrFirst].[GetPatientEncounter] AS' 
END
GO



-- =============================================
-- Author:		<Author,,dev-4266>
-- Create date: <Create Date,,16-May-17>
-- Description:	<Description,,TO get Appointment details>
-- =============================================
ALTER PROCEDURE [DrFirst].[GetPatientEncounter]
	@PatineiId  BIGINT,
	@Appointmentid  BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT distinct
	 A.AppointmentId,A.PatientID,AppDate,schedulestatus,activitystatus,R.ResourceName, A.Resourceid1 as ResourceID
	 ,ResourcePid as LoginId
	FROM [dbo].[Appointments] A
	INNER  JOIN dbo.Resources R ON A.resourceId1=R.resourceId
	INNER JOIN dbo.PracticeActivity PA ON PA.patientid=A.Patientid
	WHERE A.PatientID=@PatineiId AND A.Appointmentid=@Appointmentid

END







GO
/****** Object:  StoredProcedure [DrFirst].[GetPatientMedications]    Script Date: 3/14/2018 12:17:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[GetPatientMedications]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [DrFirst].[GetPatientMedications] AS' 
END
GO

Alter     PROCEDURE [DrFirst].[GetPatientMedications]
@PatientID BIGINT
AS
SET NOCOUNT ON


CREATE TABLE #RxSegments
(
	ClinicalId int,
	SegTwo int,
	SegThree int,
	SegFour int,
	SegFive int,
	SegSix int,
	SegSeven int,
	SegNine int,
	LegacyDrug bit
)

INSERT INTO #RxSegments
SELECT pc.clinicalid
	, CHARINDEX('-2', pc.FindingDetail) + 3
	, CHARINDEX(')-3', pc.FindingDetail) + 4
	, CHARINDEX(')-4', pc.FindingDetail) + 4
	, CHARINDEX(')-5', pc.FindingDetail) + 4
	, CHARINDEX(')-6', pc.FindingDetail) + 4
	, CHARINDEX(')-7', pc.FindingDetail) + 4
	, CHARINDEX('9-9', pc.FindingDetail)
	, CASE WHEN pc.FindingDetail like '%()%' THEN 0 ELSE 1 END 
	FROM PatientClinical pc
	WHERE-- pc.ClinicalId in (select clinicalid as number from patientclinical where clinicaltype='A' AND FindingDetail like 'Rx%')		
		 pc.PatientId = @PatientID
		AND FindingDetail like 'RX%'
		 AND clinicaltype='A' 
	
	Select  distinct len(pc.findingdetail)as lenF, pc.ClinicalId as ClinicalIds,pc.PatientId ,A.appointmentid,
	
	(convert(varchar, convert(datetime, A.appdate), 101)+ ' ' 
	+ substring(A.appdate, 1, 2)+ ':' + substring(A.appdate, 3, 2)+ ':' + substring(A.appdate, 5, 2)) as AppDate, 
	
	
	
	A.resourceid1 as DoctorExternalId,ResourceName,ResourcePid as LoginID,
	pc.findingdetail, SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)) as restFD1,
	(SELECT STUFF((SELECT char(9) + Note1
				FROM PatientNotes PN where PN.Patientid=@PatientID AND  PN.ClinicalId=pc.ClinicalId AND  Notetype in('R')
				FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'),1,1,'')) AS PatientNote, pc.DrawfileName as DrugId
	,CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
		THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') 
	ELSE '' END AS DrugName,
	
	
		CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegTwo + 1, CHARINDEX(')', pc.FindingDetail, rx.SegTwo) - rx.SegTwo - 1)
	ELSE ''
		END as Dosage,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegTwo+1) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegTwo+1)) - CHARINDEX('(', pc.FindingDetail, rx.SegTwo+1) - 1) 
	ELSE ''
		END as Strength,

			SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3)),3)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo

-3)),3))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3)),3))-1) as [OSODOU],


		CASE rx.LegacyDrug WHEN 0 THEN 
		SUBSTRING(pc.FindingDetail, rx.SegThree + 1, CHARINDEX(')', pc.FindingDetail, rx.SegThree) - rx.SegThree - 1)
	ELSE ''
		END as Frequency,
		CASE rx.LegacyDrug WHEN 0 THEN 
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegThree+1) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegThree+1)) - CHARINDEX('(', pc.FindingDetail, rx.SegThree+1) - 1)
	ELSE ''
		END as PRN,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree)+1)) - 1)
	ELSE ''
		END as PO,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail,

 rx.SegThree+1)+1)+1)) - 1)
	ELSE ''
		END as OTC,		
		SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.
SegThree-3)),5))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-1) as [state],
		SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.
SegThree-3)),6))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-1) as [Sample],

	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegFour + 1, CHARINDEX(')', pc.FindingDetail, rx.SegFour) - rx.SegFour - 1)
	ELSE ''
		END as Refills,

		SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-1) as [DAW],
		
SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),3)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour

-3)),3))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),3))-1) as QtyUnit,

		CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)) - 1)
	ELSE ''
		END as LastTaken,

		CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX(')', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)
	ELSE ''
		END as Duration,
			
		CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegFive+1) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegFive+1)) - CHARINDEX('(', pc.FindingDetail, rx.SegFive+1) - 1)
	ELSE ''
		END as Supply,
		CASE rx.LegacyDrug WHEN 1 THEN
		CASE WHEN pn1.Note1 IS NULL 
			THEN CASE WHEN pc.FindingDetail like 'rx-8%-2(%' THEN Substring(pc.findingdetail, CHARINDEX('-2',pc.findingdetail)+4, charindex(')', pc.findingdetail, CHARINDEX('-2',pc.findingdetail)+4)-CHARINDEX('-2',pc.findingdetail)-4)
			ELSE '' END + ' ' + CASE WHEN pc.FindingDetail like 'rx-8%-2(%(O%-3%' THEN Substring(pc.findingdetail, CHARINDEX(')-3',pc.findingdetail)-2,  2) 
			ELSE ''	END	 + ' ' + CASE WHEN pc.FindingDetail NOT like 'rx-8%-3-4%'  
			THEN SUBSTRING(pc.findingdetail ,CHARINDEX('-3(',pc.findingdetail)+4,CHARINDEX(')',pc.findingdetail, charindex('-3(',pc.findingdetail))-CHARINDEX('-3(',pc.findingdetail)-4 )
			ELSE '' END	+ ' ' + CASE WHEN pc.FindingDetail like 'rx-8%-3(%)%(%)-4%' THEN SUBSTRING(pc.findingdetail, CHARINDEX('(',pc.findingdetail, CHARINDEX('-3(',pc.findingdetail)+4)+1, charindex(')-4', pc.findingdetail)-CHARINDEX('(',pc.findingdetail, CHARINDEX('-3(',pc.findingdetail)+4)-1)
		ELSE '' END	ELSE 'AS PRESCRIBED' END 
	ELSE '' END AS sig,

	CONVERT(NVARCHAR(100),pc.DrawFileName) as FormulationID, 		
	CASE WHEN rx.SegNine > 0
			THEN 'Pending'
		ELSE '' END as PrescriptionStatus,
	replace(pn1.note1+pn1.note2+pn1.note3+pn1.note4, '&', ',') as SigNote,
	replace(pn2.note1+pn2.note2+pn2.note3+pn2.note4, '&', ',') as PharmNote,
	CASE rx.LegacyDrug WHEN 1 THEN
		CASE WHEN pc.FindingDetail like '%(PILLS-PACKAGE %'
			THEN CASE WHEN CHARINDEX(' ', pc.FindingDetail, CHARINDEX('(PILLS-PACKAGE ',pc.FindingDetail)+15) > 0
				THEN SUBSTRING(pc.FindingDetail, CHARINDEX('(PILLS-PACKAGE ', pc.FindingDetail)+15, CHARINDEX(' ', pc.FindingDetail, CHARINDEX('(PILLS-PACKAGE ',pc.FindingDetail)+15)-(CHARINDEX('(PILLS-PACKAGE ',pc.FindingDetail)+15))
				ELSE SUBSTRING(pc.FindingDetail, CHARINDEX('(PILLS-PACKAGE ', pc.FindingDetail)+15, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(PILLS-PACKAGE ',pc.FindingDetail)+15)-(CHARINDEX('(PILLS-PACKAGE ',pc.FindingDetail)+15))
				END
		ELSE '' 
		END	
	END AS dispenseNumber,
	
	CASE rx.LegacyDrug WHEN 1 THEN
		case WHEN pc.FindingDetail like '%(PRN)%' THEN '' 
			 WHEN pc.FindingDetail like 'rx-8%REFILLS%' and pc.FindingDetail not like 'rx-8%REFILLS %(%-5/%' 
				THEN SUBSTRING	(pc.findingdetail, charindex('-4/REFILLS ',pc.findingdetail)+11, charindex('-5/', pc.findingdetail)- charindex('-4/REFILLS ',pc.findingdetail)-11) 
			 WHEN pc.FindingDetail like 'rx-8%REFILLS %(%)-5/%' THEN SUBSTRING(pc.findingdetail, charindex('-4/REFILLS',pc.findingdetail)+11, CHARINDEX('(',pc.findingdetail, charindex('-4/REFILLS', pc.FindingDetail))-charindex('-4/REFILLS',pc.findingdetail)-11) 
			 ELSE '' END	
	ELSE '' END AS refillCount,
	rx.LegacyDrug as LegacyDrug

	,SUBSTRING(pc.FindingDetail,rx.SegSix,Len(Pc.FindingDetail)) as restFD
--	,Case When Left(pc.ImageDescriptor,1) = 'C' Then 'y' When Left(pc.ImageDescriptor,1) = 'P' Then 'y' When pc.Status = 'D' then 'y' else 'n' End as [Status]
	,Case When Left(pc.ImageDescriptor,1) = 'C' Then 'y' When pc.Status = 'D' then 'y' else 'n' End as [Status]
	,CASE WHEN Left(pc.ImageDescriptor, 1) = 'D' Then SUBstring(pc.ImageDescriptor,2,10)  WHEN pc.[Status]='D' THEN SUBstring(pc.ImageDescriptor,2,10)
	WHEN Left(pc.ImageDescriptor, 1) = 'P' Then SUBstring(pc.ImageDescriptor,2,10)
	 End as StopDate
	,Case WHEN Left(Right(pc.ImageDescriptor, 11),1) = '!' Then Right(pc.ImageDescriptor, 10) End as StartDate

	into #Medication
	FROM PatientClinical pc 
	INNER JOIN appointments A ON A.Appointmentid=pc.Appointmentid
	INNER JOIN Resources R ON R.ResourceId=A.resourceid1	
	INNER JOIN #RxSegments rx on pc.ClinicalId = rx.ClinicalId
	LEFT OUTER JOIN PatientNotes pn1 on pn1.AppointmentId = pc.AppointmentId AND pn1.Note1<>''
	AND Substring(PC.FindingDetail, 6, CHARINDEX('-2',pc.findingdetail)-6) = pn1.NoteSystem 
	AND pn1.NoteType = 'R'
	LEFT OUTER JOIN PatientNotes pn2 on pn2.AppointmentId = pc.AppointmentId AND pn2.Note1<>''
	AND SUBSTRING(pc.FindingDetail, 6, CHARINDEX('-2', pc.findingdetail)-6) = pn2.NoteSystem
	AND pn2.NoteType = 'X'			
	WHERE pc.ClinicalId in (select clinicalid as number from patientclinical where  clinicaltype='A' AND FindingDetail like 'Rx%')
	AND pc.PatientId = @PatientID --AND  (CHARINDEX('P-7', pc.FindingDetail)<=0 OR Activity='com')
	
	Select distinct Tmp.* ,  
	(SELECT STUFF((SELECT ', ' + Cast(NDC as nvarchar) FROM tblValidatedNDCList Where LEN(NDC)>=11 AND Cast(MEDID as nvarchar) = Tmp.DrugId FOR XML PATH ('')), 1, 1, '') FROM tblValidatedNDCList Where Cast(MEDID as nvarchar) = Tmp.DrugId GROUP BY MedId)
    as NDC,
	rxn.Rxnorm as RxnormID,
	 cd.Med_Dosage_form_Desc as Form ,cd.GenericDrugName, cd.med_name as BrandName,
	(SELECT STUFF((SELECT Char(9)+ Note1
				FROM patientnotes Pn where patientid=@PatientID And  notetype in('B','C','X') AND 
				 Pn.NoteSystem=Tmp.Drugname  
				FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'),1,1,'')) as OtherNote
	INto #TmpMeds 	from  #Medication Tmp	
	Left JOIN tblValidatedNDCList  ndc ON Tmp.DrugId=convert(varchar(100),ndc.medid)
	Left JOIN  [fdb].[tblCompositeDrug] cd on ndc.medid= cd.medid
	LEFT JOIN [dbo].[FDBtoRxNorm1] rxn ON FDBId=Tmp.DrugId
	
	
		
	;with CteAllergy(RxnormID,ClinicalIds,PatientId,	appointmentid,	AppDate,DoctorExternalId,ResourceName,	LoginID, findingdetail, restFD1, PatientNote, DrugId, DrugName, Dosage, Strength, OSODOU, Frequency, PRN, PO, OTC, state, Sample, Refills, DAW, QtyUnit, LastTaken, Duration, Supply, sig, FormulationID, PrescriptionStatus, SigNote, PharmNote, dispenseNumber, refillCount, LegacyDrug, restFD, [Status], StopDate,StartDate, OtherNote, NDC, Form ,GenericDrugName, BrandName
	,[Rank] )
	 as (
	 Select  RxnormID,ClinicalIds,PatientId,	appointmentid,	AppDate,DoctorExternalId,ResourceName,	LoginID	,findingdetail,restFD1,
	 PatientNote,DrugId,DrugName,Dosage,Strength,OSODOU,Frequency,PRN,PO,OTC,state
	,Sample,Refills,DAW,QtyUnit,LastTaken,Duration,Supply,sig,FormulationID,PrescriptionStatus,SigNote,PharmNote,dispenseNumber,refillCount,
	LegacyDrug,restFD,[Status],StopDate,StartDate,OtherNote,NDC,Form ,GenericDrugName, BrandName,  
	Row_number() over(Partition by findingdetail,DrugName,DrugId,FormulationID ,StopDate order by ClinicalIds  DESC ) as [Rank] from  #TmpMeds
	)
			Select  ISNULL(MM.RcopiyaID ,'') as RcopiyaID, ISNULL(NDC,'') as NDC, ISNULL(RxnormID,'') as RxnormID,ClinicalIds,Cat.PatientId,	Cat.appointmentid,	
		AppDate,DoctorExternalId,ResourceName,	LoginID, findingdetail, restFD1, PatientNote, DrugId, DrugName, Dosage, Strength, OSODOU, 
		 CASE WHEN Frequency = 'as directed' THEN  SigNote ELSE  ISNULL(DosageTimingDescription,Frequency) END as Frequency
		, PRN, PO, OTC, [State], Sample, REplace(Refills,'REFILLS','')AS Refills,DAW,QtyUnit,LastTaken,
		--Duration,
--		
		CASE WHEN Duration <>'' THEN SUBSTRING(Duration, 1, (CASE WHEN CHARINDEX(' ', Duration)>0 THEN CHARINDEX(' ', Duration) - 1 ELSE LEN(Duration) END )) ELSE  Duration END AS Supply,	
		CASE WHEN Supply <>'' THEN SUBSTRING(Supply, 1, (CASE WHEN CHARINDEX(' ', Supply)>0 THEN CHARINDEX(' ', Supply) - 1 ELSE LEN(Supply) END )) ELSE Supply END AS Duration,		
	

		sig,FormulationID,PrescriptionStatus,SigNote,
		PharmNote,dispenseNumber,refillCount,LegacyDrug,[Status],StopDate, StartDate, restFD,
		OtherNote,ISNULL(Form,'') as Form ,ISNULL(GenericDrugName,'')AS GenericDrugName, isnull(BrandName, DrugName) as BrandName, [Rank]
		INTO #Final from CteAllergy  Cat
		Left JOIN [DrFirst].[DrfirstDosagetimingmaster] DOS ON DOS.DosageTimingCode=Cat.Frequency
		Left JOIN [DrFirst].[ErxMedicationMapping]  MM   ON ClinicalIds=MM.ClinicalId
		WHERE [Rank]=1


	
	SELECT * FROM #Final F 
	WHERE ClinicalIds NOT IN(Select [ClinicalId] FROM [DrFirst].[PatientClinicalMeds] PC WHERE (isupdate =0 AND ExternalId IS NOT NULL AND PC.PatientID=F.Patientid))


	INSERT INTO [DrFirst].[PatientClinicalMeds] ([ClinicalId] ,ExternalId,RcopiaId, RxnormId, NDC, [AppointmentId], [PatientId], [FindingDetail],DrugId ,[CreatedDatetime],IsUpdate)
	Select ClinicalIds,NULL,RcopiyaID,RxnormID,NDC,appointmentid,PatientId,FindingDetail,DrugId,Getdate() ,0 
	From #Final WHERE ClinicalIds NOT IN(Select [ClinicalId] FROM [DrFirst].[PatientClinicalMeds])
	
	
	IF OBJECT_ID('tempdb..#TmpMeds') IS NOT NULL  
		DROP TABLE #TmpMeds 

	IF OBJECT_ID('tempdb..#Medication') IS NOT NULL  
    DROP TABLE #Medication 
	
	IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  
    DROP TABLE #RxSegments 
	
SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [DrFirst].[MedicationResponsePracticeLevel]    Script Date: 3/14/2018 12:17:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[MedicationResponsePracticeLevel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [DrFirst].[MedicationResponsePracticeLevel] AS' 
END
GO


Alter     PROCEDURE [DrFirst].[MedicationResponsePracticeLevel]
@xmlStringData varchar(max)=''

AS
SET NOCOUNT ON


IF OBJECT_ID('tempdb..#Medication') IS NOT NULL DROP TABLE #Medication 
IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  DROP TABLE #RxSegments 
IF OBJECT_ID('tempdb..#Response') IS NOT NULL  DROP TABLE #Response 

if(@xmlStringData<>'')
BEGIN
--declare @PatientID bigint=40943
--declare @appointmentId bigint=3312639
		CREATE TABLE #RxSegments
		(
			ClinicalId int,
			SegTwo int,
			SegThree int,
			SegFour int,
			SegFive int,
			SegSix int,
			SegSeven int,
			SegNine int,
			LegacyDrug bit
		)				
		
		DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)    
		SELECT   @XML= CAST(@xmlStringData AS XML)  
		  
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML  
 
		SELECT 		
		Patient_ExtID, 
		0 AS Appointmentid,		
		CASE WHEN CHARINDEX('EST',StartDate)>0 THEN  CONVERT(VARCHAR,CAST(Replace(StartDate, 'EST','' ) as Date),112)  
		WHEN CHARINDEX('EDT',StartDate)>0 THEN  CONVERT(VARCHAR,CAST(Replace(StartDate, 'EDT','' ) as Date),112) END as Appdate,
		
		ClinicalID, PrescriptionRcopiaID,NDCID,
		FirstDataBankMedID,RcopiaID,
		RxnormID, RxnormIDType,
		BrandName,GenericName,DrugSchedule,BrandType,
		[RouteDrug],[Form],Strength,
		[Action],Dose, DoseUnit, 
		[Route], Frequency, Duration, 
		Quantity, QuantityUnit, 
		Refills,
		OtherNotes,PatientNotes,	
		SUBSTRING(SigChangedDate,1,10) as SigChangedDate,
		SUBSTRING(LastModifiedDate,1,10) as LastModifiedDate,
		SUBSTRING(StartDate,1,10) as StartDate,
		SUBSTRING(StopDate,1,10) as StopDate,
		SigChangedDate as response_SigChangedDate,
		LastModifiedDate as response_LastModifiedDate,
		StartDate as response_StartDate,
		StopDate as response_StopDate,

		Deleted,
		StopReason,
		MedicationRcopiaID,

		PharmacyRcopiaID ,
		PharmacyRcopiaMasterID,
		PharmacyDeleted ,
		PharmacyName ,
		PharmacyAddress1 ,
		PharmacyState ,
		PharmacyZip,
		PharmacyPhone ,
		PharmacyFax ,
		PharmacyIs24Hour ,
		PharmacyLevel3 ,
		PharmacyElectronic ,
		PharmacyMailOrder ,
		PharmacyRequiresEligibility ,
		PharmacyRetail ,
		PharmacyLongTermCare ,
		PharmacySpecialty ,
		PharmacyCanReceiveControlledSubstance 
		INTO #Response  
		FROM OPENXML(@hDoc, 'RCExtResponse/Response/MedicationList/Medication/Sig/Drug')  
		WITH   
		(  			
		Patient_ExtID [varchar](50) '../../Patient/ExternalID',  
		ClinicalID [varchar](50) '../../ExternalID',
		PrescriptionRcopiaID [varchar](50) '../../Prescription/RcopiaID',	

		NDCID [VARCHAR](100) 'NDCID',
		FirstDataBankMedID [VARCHAR](100) 'FirstDataBankMedID',
		RcopiaID [VARCHAR](100) 'RcopiaID',	
		
		RxnormID  [VARCHAR](100)'RxnormID',
		RxnormIDType  [VARCHAR](100)'RxnormIDType',
					
		BrandName [varchar](200) 'BrandName',  
		GenericName [varchar](200) 'GenericName',  
		DrugSchedule [varchar](200) 'Schedule', 
		BrandType [VARCHAR](100) 'BrandType',		
		[RouteDrug]  [varchar](200) 'Route',
		[Form] [varchar](100) 'Form',		
		Strength  [varchar](200) 'Strength',  
		
		[Action]  [varchar](200) '../Action', 				 	 
		Dose  [varchar](200) '../Dose',
		DoseUnit  [varchar](200) '../DoseUnit',
		[Route]  [varchar](200) '../Route',   
		Frequency  [varchar](200) '../DoseTiming',  
		Duration  [varchar](200) '../Duration',  
		Quantity  [varchar](200) '../Quantity',  		
		QuantityUnit  [varchar](200) '../QuantityUnit',  		
		Refills  [varchar](200) '../Refills',  
		OtherNotes  [varchar](200) '../OtherNotes',  
		PatientNotes  [varchar](200) '../PatientNotes', 
		  		
		SigChangedDate [varchar](50) '../../SigChangedDate' ,
		LastModifiedDate [varchar](50) '../../ExternLastModifiedDatealID' ,
		StartDate [varchar](50) '../../StartDate',
		StopDate [varchar](50) '../../StopDate',
		Deleted [varchar](50) '../../Deleted' ,
		StopReason [varchar](200) '../../StopReason',
		MedicationRcopiaID VARCHAR(50)'../../RcopiaID',

		PharmacyRcopiaID [varchar](50) '../../Pharmacy/RcopiaID',
		PharmacyRcopiaMasterID [varchar](50) '../../Pharmacy/RcopiaMasterID',
		PharmacyDeleted [varchar](50) '../../Pharmacy/Deleted',
		PharmacyName [varchar](50) '../../Pharmacy/Name',
		PharmacyAddress1 [varchar](50) '../../Pharmacy/Address1',
		PharmacyState [varchar](50) '../../Pharmacy/State',
		PharmacyZip [varchar](50) '../../Pharmacy/Zip',
		PharmacyPhone [varchar](50) '../../Pharmacy/Phone',
		PharmacyFax [varchar](50) '../../Pharmacy/Fax',
		PharmacyIs24Hour [varchar](50) '../../Pharmacy/Is24Hour',
		PharmacyLevel3 [varchar](50) '../../Pharmacy/Level3',
		PharmacyElectronic [varchar](50) '../../Pharmacy/Electronic',
		PharmacyMailOrder [varchar](50) '../../Pharmacy/MailOrder',
		PharmacyRequiresEligibility [varchar](50) '../../Pharmacy/RequiresEligibility',
		PharmacyRetail [varchar](50) '../../Pharmacy/Retail',
		PharmacyLongTermCare [varchar](50) '../../Pharmacy/LongTermCare',
		PharmacySpecialty [varchar](50) '../../Pharmacy/Specialty',
		PharmacyCanReceiveControlledSubstance [varchar](50) '../../Pharmacy/CanReceiveControlledSubstance'	
		 )  
		 EXEC sp_xml_removedocument @hDoc

	
	   	INSERT INTO [DrFirst].[eRx_Medicine_Response]
      ([Patient_ExtID]   ,AppointmentId   ,[ClinicalID]      ,[NDCID]      ,[FirstDataBankMedID]      ,[RcopiaID]     
	   ,RxnormID, RxnormIDType,[BrandName]
      ,[GenericName] ,   DrugSchedule ,[BrandType]      ,[RouteDrug]      ,[Form]      ,[Strength]      ,[Action]
      ,[Dose]      ,[DoseUnit]      ,[Route]      ,[Frequency]      ,[Duration]      ,[Quantity]
      ,[QuantityUnit]      ,[Refills]      ,[OtherNotes]      ,[PatientNotes]      ,[SigChangedDate]      ,[LastModifiedDate]
      ,[StartDate]      ,[StopDate]      ,[Deleted]      ,[StopReason]      ,[MedicationRcopiaID], PrescriptionRcopiaID,
	    PharmacyRcopiaID , PharmacyRcopiaMasterID,PharmacyDeleted , PharmacyName , PharmacyAddress1 , PharmacyState , 
		PharmacyZip, PharmacyPhone , PharmacyFax ,PharmacyIs24Hour , PharmacyLevel3 , PharmacyElectronic , 
		PharmacyMailOrder , PharmacyRequiresEligibility , PharmacyRetail , PharmacyLongTermCare , PharmacySpecialty ,
		PharmacyCanReceiveControlledSubstance 	
	  )
	  SELECT  [Patient_ExtID] ,0, [ClinicalID]      ,[NDCID]      ,[FirstDataBankMedID]      ,[RcopiaID]    
	   , RxnormID, RxnormIDType  ,[BrandName]      ,[GenericName],DrugSchedule
      ,[BrandType]      ,[RouteDrug]      ,[Form]      ,[Strength]      ,[Action]      ,[Dose]      ,[DoseUnit]      ,[Route]
      ,[Frequency]      ,[Duration]      ,[Quantity]      ,[QuantityUnit]      ,[Refills]      ,[OtherNotes]      ,[PatientNotes],
	  [response_SigChangedDate]      ,[response_LastModifiedDate]      ,
	  CASE WHEN [StartDate] ='' THEN [response_StopDate] ELSE [response_StartDate] END       ,
	  [response_StopDate]  ,[Deleted]      ,[StopReason]      ,[MedicationRcopiaID], PrescriptionRcopiaID,     
	  PharmacyRcopiaID , PharmacyRcopiaMasterID,PharmacyDeleted , PharmacyName , PharmacyAddress1 , PharmacyState , 
	  PharmacyZip, PharmacyPhone , PharmacyFax ,PharmacyIs24Hour , PharmacyLevel3 , PharmacyElectronic , 
	PharmacyMailOrder , PharmacyRequiresEligibility , PharmacyRetail , PharmacyLongTermCare , PharmacySpecialty ,
	PharmacyCanReceiveControlledSubstance 
	  FROM #Response WHERE [MedicationRcopiaID] NOT IN( SELECT Tmp.[MedicationRcopiaID] FROM [DrFirst].[eRx_Medicine_Response] Tmp 
	  WHERE Tmp.[Patient_ExtID]=#Response.[Patient_ExtID] AND Tmp.Deleted=#Response.Deleted  AND Tmp.StopDate=#Response.StopDate)



		 INSERT INTO #RxSegments
		SELECT DISTINCT pc.clinicalid
		, CHARINDEX('-2', pc.FindingDetail) + 3
		, CHARINDEX(')-3', pc.FindingDetail) + 4
		, CHARINDEX(')-4', pc.FindingDetail) + 4
		, CHARINDEX(')-5', pc.FindingDetail) + 4
		, CHARINDEX(')-6', pc.FindingDetail) + 4
		, CHARINDEX(')-7', pc.FindingDetail) + 4
		, CHARINDEX('9-9', pc.FindingDetail)
		, CASE WHEN pc.FindingDetail like '%()%' THEN 0 ELSE 1 END 
		FROM PatientClinical pc
		INNER JOIN #Response rsp ON rsp.Patient_ExtID=pc.PatientId
		WHERE pc.ClinicalId in (select clinicalid as number from patientclinical where clinicaltype='A' AND FindingDetail like 'Rx%' 
		and pc.PatientId = rsp.Patient_ExtID )

		
		Update RS SET RS.Appointmentid=App.Appointmentid  FROM  #Response  RS INNER  JOIN appointments App ON RS.Patient_ExtID=App.Patientid
		WHERE RS.Appdate=App.Appdate
			

		DECLARE  @Medication TABLE
		(
		ClinicalId BIGINT,
		PatientId BIGINT,
		appointmentid BIGINT,
		AppDate Datetime,
		DoctorExternalId INT,
		ResourceName VARCHAR(200),
		LoginID INT,
		FindingDetail VARCHAR(500),
		restFD1 VARCHAR(100),
		PatientNote NVARCHAR(500),
		OtherNote NVARCHAR(500),
		Drugid VARCHAR(100),
		DrugName VARCHAR(200),
		FD  VARCHAR(500),
		FindingDuration VARCHAR(100),
		Status Varchar(100)
		)

		--select * FROM @Medication

		INSERT INTO @Medication
		Select  distinct pc.ClinicalId as ClinicalId,pc.PatientId ,A.appointmentid,


		(convert(varchar, convert(datetime, A.appdate), 101)+ ' ' 
	+ substring(A.appdate, 1, 2)+ ':' + substring(A.appdate, 3, 2)+ ':' + substring(A.appdate, 5, 2)) as AppDate,	

		A.resourceid1 as DoctorExternalId,ResourceName,ResourcePid as LoginID,
		pc.findingdetail,
		SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)) as restFD1,
					'' as PatientNote,'' as OtherNote,
					pc.DrawfileName as DrugId,
					(CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') 
		ELSE '' END) as DrugName,
		('RX-8/'+
		(CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
			--THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') ELSE '' END)
			THEN  SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6) ELSE '' END)
		+
		'-2/(Dosage)(Strength)(EyeContext)-3/(Frequency)('+
		
		+'PRN)(PO)('+
		(CASE rx.LegacyDrug WHEN 0 THEN
			SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)) - 1)
		ELSE ''END) --as OTC,		
		+	')('+
			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-1)) +')('+
			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-1))
			+')-4/(Refills)('+
			SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-1) 
			+')(QtyUnit)('+	
			
		(CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)) - 1)
		ELSE ''	END)-- as LastTaken,
		+')-5/('+
		CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX(')', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)
	ELSE '' END 
	+')(DaySupply'

		+')'+			
		(SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)))) as FD,
		
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX(')', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)as FindingDuration,
		pc.[Status]
		--INTO #Medication
		FROM PatientClinical pc 
		INNER JOIN appointments A ON A.Appointmentid=pc.Appointmentid
		INNER JOIN Resources R ON R.ResourceId=A.resourceid1	
		INNER JOIN #RxSegments rx on pc.ClinicalId = rx.ClinicalId
		INNER JOIN #Response rsp ON rsp.Patient_ExtID=pc.PatientId --AND rsp.Appointmentid=pc.Appointmentid
		LEFT OUTER JOIN PatientNotes pn1 on pn1.AppointmentId = pc.AppointmentId 
		AND Substring(PC.FindingDetail, 6, CHARINDEX('-2',pc.findingdetail)-6) = pn1.NoteSystem 
		AND pn1.NoteType = 'R'
		LEFT OUTER JOIN PatientNotes pn2 on pn2.AppointmentId = pc.AppointmentId
		AND SUBSTRING(pc.FindingDetail, 6, CHARINDEX('-2', pc.findingdetail)-6) = pn2.NoteSystem
		AND pn2.NoteType = 'X'			
		WHERE 
		 PC.FindingDetail like 'RX%'
		 AND PC.clinicaltype='A'
		
		update M  SET M.PatientNote=PN.note1  FROM   @Medication M  INNER JOIN PatientNotes PN ON M.AppointmentId = PN.AppointmentId
		WHERE NoteType='R' AND Substring(M.FindingDetail, 6, CHARINDEX('-2',M.findingdetail)-6) = PN.NoteSystem 

		
		update M  SET M.OtherNote=PN.note1  FROM   @Medication M  INNER JOIN PatientNotes PN ON M.AppointmentId = PN.AppointmentId
		WHERE NoteType='X' AND Substring(M.FindingDetail, 6, CHARINDEX('-2',M.findingdetail)-6) = PN.NoteSystem 

		DECLARE @NewMedCount INT
		
		
		

		SELECT ROW_NUMBER() OVER(ORDER BY MedicationRcopiaID) AS ROWNUMBER,		
		Patient_ExtID , R.AppointmentId,
		ClinicalID,MedicationRcopiaID,BrandName,Form,RouteDrug,
		(CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END) AS [EyeContext],
		'99' as [Symptom],
		 RcopiaID,NDCID,
		 'RX-8/'+R.BrandName+' '+R.Form+'-2/('+Dose+' '+DoseUnit+')('+Strength+')('+
		(CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END)
		+')-3/('+Frequency+')('+
		(CASE WHEN R.Refills='PRN' THEN '' ELSE '' END)+')('+
		--(Case WHEN R.[Action]='Apply' THEN 'APPLY TO LIDS' WHEN R.[Action]='INSTILL' THEN 'INSTILL IN EYES' WHEN R.[Action]='' THEN  'PO' ELSE R.[Route] END)+')()()()-4/('+
		R.[Action] +' '+R.[Route] +')()()()-4/('+
		--(CASE WHEN R.Refills='PRN' THEN '' ELSE R.Refills+' '+'REFILLS' END)+')()('+
		(CASE  WHEN R.Refills ='' THEN '' WHEN R.Refills ='PRN' THEN 'PRN REFILLS' ELSE R.Refills+' '+'REFILLS' END)+')()('+
		(R.Quantity+' '+R.QuantityUnit)+')()-5/()('+
		(CASE WHEN R.Duration <>'' THEN R.Duration+' DAYS SUPPLY' ELSE R.Duration END)+')-6/' as Findings,	
		--(case when R.StopDate <> '' then ('D'+R.StopDate +'!'+ CONVERT(varchar(20),CAST(Ap.appdate AS  DATE),101)) ELSE ('!'+ CONVERT(varchar(20),CAST(Ap.appdate AS  DATE),101) )  end) AS ImgDescriptor,		
		'!'+ CONVERT(varchar(20),CAST(Ap.appdate AS  DATE),101) AS ImgDescriptor,		
		FirstDataBankMedID,
		(case when Deleted = 'n' then 'A' ELSE 'D'  end) AS [Status],
		PatientNotes,
		OtherNotes,
		StartDate,
		StopDate,		
		PrescriptionRcopiaID
		INTO #NewMeds
		FROM #Response R
		INNER  JOIN appointments ap ON Ap.Appointmentid=R.AppointmentId AND Ap.Patientid=Patient_ExtID
		WHERE
		 (ClinicalID =''  OR ClinicalID=0)
		-- AND 
		-- R.Deleted='n'
		AND R.MedicationRcopiaID NOT IN( SELECT RcopiyaID FROM [DrFirst].[ErxMedicationMapping] WHERE PatientID=Patient_ExtID )
		AND   ISNULL(R.PrescriptionRcopiaID,'') NOT IN( SELECT PrescriptionRcopiaID FROM [DrFirst].[ErxMedicationMapping] WHERE PatientID=R.Patient_ExtID  AND PrescriptionRcopiaID IS NOT NULL)
	
		SET @NewMedCount=@@rowcount
		
			IF(@NewMedCount>0)
		BEGIN
			DECLARE @loop INT
			SET @loop = 1

			WHILE @loop <= @NewMedCount
			BEGIN
				---SELECT  * FROM #NewMeds WHERE ROWNUMBER=@loop			
				INSERT INTO dbo.PatientClinical
				(
				[AppointmentId]
				,[PatientId]
				,[ClinicalType]
				,[EyeContext]
				,[Symptom]
				,[FindingDetail]
				,[ImageDescriptor]      
				,[Status]
				,[DrawFileName]
				,Activity)
				
				SELECT  AppointmentId, Patient_ExtID,'A',
				EyeContext,Symptom,
				CASE WHEN  PrescriptionRcopiaID IS NOT NULL THEN Findings+'P-7' ELSE Findings END,
				ImgDescriptor, [Status], FirstDataBankMedID,CASE WHEN PrescriptionRcopiaID IS NOT NULL  THEN 'pen' ELSE '' END
				FROM #NewMeds TT 			
				WHERE ROWNUMBER=@loop

				DECLARE @ClinicalID_New BIGINT 
				SELECT @ClinicalID_New=IDENT_CURRENT('PatientClinical')
				--Select @ClinicalID_New
			
			
			INSERT INTO [DrFirst].[ErxMedicationMapping]
			([ClinicalId],[EncounterId],[PatientId],[RcopiyaID],[MedDatetime],PrescriptionRcopiaID)
			SELECT @ClinicalID_New, AppointmentId, Patient_ExtID,MedicationRcopiaID,GETDATE() ,PrescriptionRcopiaID
			FROM #NewMeds  WHERE  ROWNUMBER=@loop

			INSERT INTO [DrFirst].[PatientClinicalMeds]
			([ClinicalId] ,ExternalId,RcopiaId, RxnormId, NDC, [AppointmentId], 
			[PatientId], [FindingDetail],[status], DrugId ,[CreatedDatetime],IsUpdate)
			Select @ClinicalID_New,@ClinicalID_New,MedicationRcopiaID,'',NDCID,
			AppointmentId,Patient_ExtID,Findings,[Status],FirstDataBankMedID,Getdate() ,0 
			From #NewMeds 
			WHERE  ROWNUMBER=@loop AND @ClinicalID_New NOT IN(Select [ClinicalId] FROM [DrFirst].[PatientClinicalMeds])
			

				
			IF NOT EXISTS (SELECT 1 FROM Drug D INNER JOIN #NewMeds N ON D.DisplayName=N.BrandName+' '+ N.Form  AND D.Focus=1 and D.DrugDosageFormId is NULL WHERE ROWNUMBER=@loop )
			BEGIN
				INSERT INTO Drug (Drugcode,DisplayName,Family1,Family2,Focus,DrugDosageFormId)
				SELECT 'T',BrandName+' '+Form,'','',1,NULL FROM #NewMeds WHERE   RouteDrug='opht' AND ROWNUMBER=@loop
			END

				INSERT INTO  dbo.PatientNotes 
				(
					PatientId,
					AppointmentId,
					NoteType,
					Note1,
					NoteDate,
					NoteSystem, 
					NoteCommentOn,
					NoteAudioOn,
					NoteClaimOn,
					ClinicalId 
				)
				SELECT 
				Patient_ExtID, 
				appointmentId,
				'R',
				PatientNotes,			
				RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),
				(BrandName+' '+Form),
				'T','F','F',
				@ClinicalID_New
				FROM   #NewMeds
				WHERE  ROWNUMBER=@loop

				INSERT INTO  dbo.PatientNotes 
				(
					PatientId,
					AppointmentId,
					NoteType,
					Note1,
					NoteDate,
					NoteSystem, 
					NoteCommentOn,
					NoteAudioOn,
					NoteClaimOn,
					ClinicalId 
				)
				SELECT 
				Patient_ExtID, 
				appointmentId,
				'X',
				OtherNotes,			
				RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),
				(BrandName+' '+Form),
				'T','F','F',
				@ClinicalID_New
				FROM   #NewMeds
				WHERE  ROWNUMBER=@loop

				SET @loop = @loop + 1 								  
			END		
		END
		--ELSE
		--Print 'Not in if'

		IF OBJECT_ID('tempdb..#NewMeds') IS NOT NULL  
					DROP TABLE #NewMeds 

		--Select * from #Medication
		--Select * from #Response

		Update RS SET RS.clinicalid=Emm.Clinicalid FROM  #Response  RS INNER  JOIN [DrFirst].[ErxMedicationMapping] Emm ON RS.Patient_ExtID=Emm.Patientid
		WHERE RS.PrescriptionRcopiaId=Emm.PrescriptionRcopiaID OR Rs.MedicationRcopiaId=Emm.RcopiyaId
		
		Update Emm SET Emm.RcopiyaID=RS.MedicationRcopiaID 
		FROM  #Response  RS 
		INNER  JOIN [DrFirst].[ErxMedicationMapping] Emm ON RS.Patient_ExtID=Emm.Patientid 	AND RS.PrescriptionRcopiaID=Emm.PrescriptionRcopiaID
 
		

		UPDATE P 
		set P.FindingDetail=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TMP.FD,'EyeContext',
		 (CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END)),
		'PRN',(CASE WHEN R.Refills='PRN' THEN 'PRN' ELSE ''END)),'DaySupply',
		(CASE WHEN R.Duration <>'' THEN R.Duration+' DAYS SUPPLY' ELSE R.Duration END))
		,'Refills',CASE WHEN R.Refills='PRN' THEN '' ELSE R.Refills+' '+'REFILLS' END),'QtyUnit',R.Quantity+' '+R.QuantityUnit),'PO',
		(Case WHEN R.[Action]='Apply' THEN 'APPLY TO LIDS' WHEN R.[Action]='INSTILL' THEN 'INSTILL IN EYES' WHEN R.[Action]='' THEN  'PO' ELSE R.[Route] END)),
		'Frequency',R.Frequency),'Strength',R.Strength),'Dosage',R.Dose+' '+ R.DoseUnit) 
			
		,Activity=CASE WHEN R.PrescriptionRcopiaID IS NOT NULL  THEN 'pen' ELSE Activity END		
		FROM @Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId


		--For ChangeERXdate 
		Update P set
		--P.ImageDescriptor=CASE WHEN LEFT(P.ImageDescriptor, 1)<>'D' THEN 'D'+R.StopDate+P.ImageDescriptor ELSE P.ImageDescriptor END
		P.ImageDescriptor= CASE 
							WHEN R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) AND Left(P.ImageDescriptor, 1)='!' THEN 'D'+R.StopDate+P.ImageDescriptor 
							WHEN  R.StopDate=RIGHT(Left(P.ImageDescriptor, 11),10)  AND Left(P.ImageDescriptor, 1)='!' THEN STUFF(P.ImageDescriptor , 1, 1, 'D') ELSE P.ImageDescriptor
						  END
		FROM @Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId WHERE R.StopDate<>'' AND R.Deleted='n' AND R.Stopdate= convert(varchar(10),getdate(),101)

		Update P set
		P.ImageDescriptor=CASE 
							WHEN --R.StopDate<>Right(P.ImageDescriptor, 10) AND 
								R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) 
							THEN '!'+R.StopDate+P.ImageDescriptor 
							ELSE P.ImageDescriptor 
						END		
		,P.Status='D'
		FROM @Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId
		WHERE R.Deleted='y'
		
		----For-Patiet Note
		UPDATE  PN  SET  PN.Note1=R.PatientNotes FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID 
			AND PN.PatientId=R.Patient_ExtID AND NoteType='R'AND R.Clinicalid <>''

		--For Phrma note 
		UPDATE  PN  SET  PN.Note1=R.OtherNotes
		FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID AND PN.PatientId=R.Patient_ExtID AND  NoteType='X'AND R.Clinicalid <>''

		--INSERT INTO PatientNotes (PatientId,AppointmentId,NoteType,Note1,NoteDate,NoteSystem, NoteCommentOn,NoteAudioOn,NoteClaimOn,ClinicalId )
		--SELECT  Patient_ExtID, AappointmentId,'R',	PatientNotes,FORMAT(CAST(StartDate AS DATE),'yyyyMMdd'),(BrandName+' '+Form),'T','F','F',ClinicalID 
		--FROM  #Response R  WHERE R.ClinicalID NOT IN(Select  P.ClinicalID  FROM PatientNotes P  WHERE Patientid=@PatientID AND AppointmentId=@appointmentId)


		--INSERT INTO PatientNotes (PatientId,AppointmentId,NoteType,Note1,NoteDate,NoteSystem, NoteCommentOn,NoteAudioOn,NoteClaimOn,ClinicalId )
		--SELECT  Patient_ExtID, AppointmentId,'X',	OtherNotes,FORMAT(CAST(StartDate AS DATE),'yyyyMMdd'),(BrandName+' '+Form),'T','F','F',ClinicalID 
		--FROM  #Response R  WHERE R.ClinicalID NOT IN(Select  ClinicalID  FROM PatientNotes P  WHERE P.Patientid=Patient_ExtID AND AppointmentId=@appointmentId)

		
		UPDATE PCM  SET   PCM.ExternalID= R.ClinicalID, PCM.RcopiaId=R.RcopiaID , Isupdate=0,status=Deleted  FROM   #Response R  INNER JOIN Drfirst.[PatientClinicalMeds] PCM ON R.ClinicalID=PCM.ClinicalID
	
	
		IF OBJECT_ID('tempdb..#Medication') IS NOT NULL  
			DROP TABLE #Medication 
	
		IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  
			DROP TABLE #RxSegments 

		IF OBJECT_ID('tempdb..#Response') IS NOT NULL  
					DROP TABLE #Response 
		END		

	
SET NOCOUNT OFF




GO
/****** Object:  StoredProcedure [DrFirst].[PrescriptionResponce]    Script Date: 3/14/2018 12:17:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[PrescriptionResponce]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [DrFirst].[PrescriptionResponce] AS' 
END
GO


Alter   PROCEDURE [DrFirst].[PrescriptionResponce]
@PatientID BIGINT,
@appointmentId BIGINT,
@xmlStringData varchar(max)

AS
SET NOCOUNT ON

IF OBJECT_ID('tempdb..#Medication') IS NOT NULL DROP TABLE #Medication 
IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  DROP TABLE #RxSegments 
--declare @PatientID bigint=40943
--declare @appointmentId bigint=3312639

		CREATE TABLE #RxSegments
		(
			ClinicalId int,
			SegTwo int,
			SegThree int,
			SegFour int,
			SegFive int,
			SegSix int,
			SegSeven int,
			SegNine int,
			LegacyDrug bit
		)

		INSERT INTO #RxSegments
		SELECT pc.clinicalid
		, CHARINDEX('-2', pc.FindingDetail) + 3
		, CHARINDEX('-3', pc.FindingDetail) + 3
		, CHARINDEX('-4', pc.FindingDetail) + 3
		, CHARINDEX('-5', pc.FindingDetail) + 3
		, CHARINDEX('-6', pc.FindingDetail) + 3
		, CHARINDEX('-7', pc.FindingDetail) + 3
		, CHARINDEX('9-9', pc.FindingDetail)
		, CASE WHEN pc.FindingDetail like '%()%' THEN 0 ELSE 1 END 
		FROM PatientClinical pc
		WHERE pc.ClinicalId in (select clinicalid as number from patientclinical where clinicaltype='A' AND FindingDetail like 'Rx%' 
		and pc.PatientId = @PatientID --and appointmentid=@appointmentId
		)
		and pc.PatientId = @PatientID -- and appointmentid=@appointmentId
			
	
		select  pc.ClinicalId as ClinicalId,pc.PatientId ,A.appointmentid,
		(convert(varchar, convert(datetime, A.appdate), 101)+ ' ' 
	+ substring(A.appdate, 1, 2)+ ':' + substring(A.appdate, 3, 2)+ ':' + substring(A.appdate, 5, 2)) as AppDate,	
		A.resourceid1 as DoctorExternalId,ResourceName,ResourcePid as LoginID,
		pc.findingdetail,
		SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)) as restFD1,
		(SELECT STUFF((SELECT char(9) + Note1
					FROM patientnotes where patientid=@PatientID AND   Notetype in('R')
					FOR XML PATH('')) ,1,1,'')) AS PatientNote
			
		,(SELECT STUFF((SELECT char(9) + Note1
					FROM patientnotes where patientid=@PatientID And  notetype in('B','C','X')
					FOR XML PATH('')) ,1,1,'')) AS OtherNote,
					pc.DrawfileName as DrugId,
					(CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') 
		ELSE '' END) as DrugName,
		('RX-8/'+
		(CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') ELSE '' END)
		+
		'-2/(Dosage)(Strength)('+
				--SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3)),3)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3)),3))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3)),3))-1)
				+'EyeContext)-3/(Frequency)('+
		--(CASE rx.LegacyDrug WHEN 0 THEN 
		--SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegThree+1) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegThree+1)) - CHARINDEX('(', pc.FindingDetail, rx.SegThree+1) - 1)
		--ELSE ''END)-- as PRN,
		+'PRN)(PO)('+
		(CASE rx.LegacyDrug WHEN 0 THEN
			SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)) - 1)
		ELSE ''END) --as OTC,		
		+	')('+
			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-1)) +')('+
			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-1))
			+')-4/(Refills)('+
			SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-1) 
			+')(QtyUnit)('+	
		--(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),3)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),3))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),3))-1))+')('+
		
		(CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)) - 1)
		ELSE ''	END)-- as LastTaken,
		+')-5/(Duration)('+			
		(CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegFive+1) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegFive+1)) - CHARINDEX('(', pc.FindingDetail, rx.SegFive+1) - 1)
		ELSE ''	END)--as Supply,
		+')'+		
		(SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)))) as FD,
		'' AS FD1,
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX(')', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)as FindingDuration,
		pc.[Status]
		INTO #Medication
		FROM PatientClinical pc 
		INNER JOIN appointments A ON A.Appointmentid=pc.Appointmentid
		INNER JOIN Resources R ON R.ResourceId=A.resourceid1	
		INNER JOIN #RxSegments rx on pc.ClinicalId = rx.ClinicalId
		LEFT OUTER JOIN PatientNotes pn1 on pn1.AppointmentId = pc.AppointmentId 
		AND Substring(PC.FindingDetail, 6, CHARINDEX('-2',pc.findingdetail)-6) = pn1.NoteSystem 
		AND pn1.NoteType = 'R'
		LEFT OUTER JOIN PatientNotes pn2 on pn2.AppointmentId = pc.AppointmentId
		AND SUBSTRING(pc.FindingDetail, 6, CHARINDEX('-2', pc.findingdetail)-6) = pn2.NoteSystem
		AND pn2.NoteType = 'X'			
		WHERE pc.ClinicalId in (select clinicalid as number from patientclinical where  clinicaltype='A' AND FindingDetail like 'Rx%')
		and pc.PatientId = @PatientID

		DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)  
  
		SELECT   @XML= CAST(@xmlStringData AS XML)  
  
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML  
  
		SELECT 		
		Patient_ExtID, ClinicalID, NDCID,
		FirstDataBankMedID,RcopiaID,
		BrandName,GenericName,BrandType,
		[RouteDrug],[Form],Strength,
		[Action],Dose, DoseUnit, 
		[Route], Frequency, Duration, 
		Quantity, QuantityUnit, 
		Refills,
		OtherNotes,PatientNotes,	
		SUBSTRING(SigChangedDate,1,10) as SigChangedDate,
		SUBSTRING(LastModifiedDate,1,10) as LastModifiedDate,
		SUBSTRING(StartDate,1,10) as StartDate,
		SUBSTRING(StopDate,1,10) as StopDate,
		Deleted,
		StopReason,
		MedicationRcopiaID
		INTO #Response  
		FROM OPENXML(@hDoc, 'RCExtResponse/Response/MedicationList/Medication/Sig/Drug')  
		WITH   
		(  			
		Patient_ExtID [varchar](50) '../../Patient/ExternalID',  
		ClinicalID [varchar](50) '../../ExternalID',

		NDCID [VARCHAR](100) 'NDCID',
		FirstDataBankMedID [VARCHAR](100) 'FirstDataBankMedID',
		RcopiaID [VARCHAR](100) 'RcopiaID',				
		BrandName [varchar](200) 'BrandName',  
		GenericName [varchar](200) 'GenericName',  
		BrandType [VARCHAR](100) 'BrandType',		
		[RouteDrug]  [varchar](200) 'Route',
		[Form] [varchar](100) 'Form',		
		Strength  [varchar](200) 'Strength',  
		
		[Action]  [varchar](200) '../Action', 				 	 
		Dose  [varchar](200) '../Dose',
		DoseUnit  [varchar](200) '../DoseUnit',
		[Route]  [varchar](200) '../Route',   
		Frequency  [varchar](200) '../DoseTiming',  
		Duration  [varchar](200) '../Duration',  
		Quantity  [varchar](200) '../Quantity',  		
		QuantityUnit  [varchar](200) '../QuantityUnit',  		
		Refills  [varchar](200) '../Refills',  
		OtherNotes  [varchar](200) '../OtherNotes',  
		PatientNotes  [varchar](200) '../PatientNotes', 
		  		
		SigChangedDate [varchar](50) '../../SigChangedDate' ,
		LastModifiedDate [varchar](50) '../../ExternLastModifiedDatealID' ,
		StartDate [varchar](50) '../../StartDate',
		StopDate [varchar](50) '../../StopDate',
		Deleted [varchar](50) '../../Deleted' ,
		StopReason [varchar](200) '../../StopReason',
		MedicationRcopiaID VARCHAR(50)'../../RcopiaID'
		 )  
		 EXEC sp_xml_removedocument @hDoc 

		 --select * from #Response

			
		SELECT  ROW_NUMBER() OVER(PARTITION BY RcopiaID, MedicationRcopiaID,NDCID,Brandname,FirstDataBankMedID ORDER BY MedicationRcopiaID) AS ROWNUMBER, 
		 --'1234567'  as Patient_ExtID, '1234567' AS AppointmentId,
		@PatientID AS Patient_ExtID ,@appointmentId AS AppointmentId,
		ClinicalID,MedicationRcopiaID,BrandName,Form,
		(CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END) AS [EyeContext],
		'99' as [Symptom],
		 RcopiaID,NDCID,
		 'RX-8/'+R.BrandName+' '+R.Form+'-2/('+Dose+' '+DoseUnit+')('+Strength+')('+
		(CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END)
		+')-3/('+Frequency+')('+
		(CASE WHEN R.Refills='PRN' THEN 'PRN' ELSE '' END)+')('+
		(Case WHEN R.[Action]='Apply' THEN 'APPLY TO LIDS' WHEN R.[Action]='INSTILL' THEN 'INSTILL IN EYES' WHEN R.[Action]='' THEN  'PO' ELSE R.[Route] END)+')()()()-4/('+
		(CASE WHEN R.Refills='PRN' THEN '' ELSE R.Refills+' '+'REFILLS' END)+')()('+
		(R.Quantity+' '+R.QuantityUnit)+')()-5/('+
		(CASE WHEN R.Duration LIKE '[0-9]' THEN R.Duration+' days' ELSE R.Duration END)+')()-6/P-7' as Findings,
		('!'+StartDate ) AS ImgDescriptor,
		FirstDataBankMedID,
		'A'	as [Status],
		PatientNotes,
		OtherNotes,
		StartDate,
		StopDate		
		INTO #NewMeds
		FROM #Response R WHERE  ClinicalID =''
		AND MedicationRcopiaID NOT IN( SELECT RcopiyaID FROM [DrFirst].[ErxMedicationMapping])



--select * from #NewMeds

		DECLARE @NewMedCount INT
		SELECT @NewMedCount= COUNT(*) FROM #NewMeds
		print @NewMedCount
--select COUNT(*) FROM #NewMeds
		DECLARE @loop INT
		SET @loop = 1

		WHILE @loop <= @NewMedCount
		BEGIN				
			INSERT INTO dbo.PatientClinical
			(
			[AppointmentId]
			,[PatientId]
			,[ClinicalType]
			,[EyeContext]
			,[Symptom]
			,[FindingDetail]
			,[ImageDescriptor]      
			,[Status]
			,[DrawFileName])
			--,[Highlights]  --,[FollowUp]      --,[PermanentCondition]      --,[PostOpPeriod]      --,[Surgery]      --,[Activity]      --,[LegacyClinicalDataSourceTypeId]
			SELECT  AppointmentId, Patient_ExtID,'A',
			EyeContext,Symptom,Findings, ImgDescriptor, [Status], FirstDataBankMedID
			FROM #NewMeds TT 			
			WHERE ROWNUMBER=@loop

			DECLARE @ClinicalID_New BIGINT 
			SELECT @ClinicalID_New=IDENT_CURRENT('PatientClinical')

			select @ClinicalID_New

			
			INSERT INTO [DrFirst].[ErxMedicationMapping]
			([ClinicalId],[EncounterId],[PatientId],[RcopiyaID],[MedDatetime])
			SELECT @ClinicalID_New, @appointmentId, @PatientID,MedicationRcopiaID,GETDATE() 
				FROM #NewMeds WHERE  ROWNUMBER=@loop

--select @ClinicalID_New		
			INSERT INTO  dbo.PatientNotes 
			(
				PatientId,
				AppointmentId,
				NoteType,
				Note1,
				NoteDate,
				NoteSystem, 
				NoteCommentOn,
				NoteAudioOn,
				NoteClaimOn,
				ClinicalId 
			)
			SELECT 
			@PatientID, 
			@appointmentId,
			'R',
			PatientNotes,			
			RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),
			(BrandName+' '+Form),
			'T','F','F',
			@ClinicalID_New
			FROM   #NewMeds
			WHERE  ROWNUMBER=@loop

			INSERT INTO  dbo.PatientNotes 
			(
				PatientId,
				AppointmentId,
				NoteType,
				Note1,
				NoteDate,
				NoteSystem, 
				NoteCommentOn,
				NoteAudioOn,
				NoteClaimOn,
				ClinicalId 
			)
			SELECT 
			@PatientID, 
			@appointmentId,
			'X',
			PatientNotes,			
			RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),
			(BrandName+' '+Form),
			'T','F','F',
			@ClinicalID_New
			FROM   #NewMeds
			WHERE  ROWNUMBER=@loop

			SET @loop = @loop + 1 
						
			  
		END

		IF OBJECT_ID('tempdb..#NewMeds') IS NOT NULL  
					DROP TABLE #NewMeds 

 
		UPDATE P 
		set P.FindingDetail=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TMP.FD,'EyeContext',
		 (CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END)),
		'PRN',(CASE WHEN R.Refills='PRN' THEN 'PRN' ELSE ''END)),'Duration',
		(CASE WHEN R.Duration LIKE '[0-9]' THEN R.Duration+' days' ELSE TMP.FindingDuration END))
		,'Refills',CASE WHEN R.Refills='PRN' THEN '' ELSE R.Refills+' '+'REFILLS' END),'QtyUnit',R.Quantity+' '+R.QuantityUnit),'PO',
		(Case WHEN R.[Action]='Apply' THEN 'APPLY TO LIDS' WHEN R.[Action]='INSTILL' THEN 'INSTILL IN EYES' WHEN R.[Action]='' THEN  'PO' ELSE R.[Route] END)),
		'Frequency',R.Frequency),'Strength',R.Strength),'Dosage',R.Dose+' '+ R.DoseUnit)  
		,P.ImageDescriptor=CASE WHEN Left(Right(P.ImageDescriptor, 11),1) = '!'  THEN REPLACE(P.ImageDescriptor,Right(P.ImageDescriptor, 10),R.StartDate) ELSE P.ImageDescriptor END
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId

		--For ChangeERXdate 
		Update P set		
		P.ImageDescriptor=CASE 
							WHEN R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) THEN 'D'+R.StopDate+P.ImageDescriptor 
							ELSE STUFF(P.ImageDescriptor , 1, 1, 'D')
						  END
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId WHERE R.StopDate<>'' AND R.Deleted='n'

		Update P set
		P.ImageDescriptor=CASE 
							WHEN --R.StopDate<>Right(P.ImageDescriptor, 10) AND 
								R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) 
							THEN '!'+R.StopDate+P.ImageDescriptor 
							ELSE P.ImageDescriptor 
						END		
		,P.Status='D'
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId
		WHERE R.Deleted='y'
		
		----For-Patiet Note
		UPDATE  PN  SET  PN.Note1=R.PatientNotes
		FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID

		--For Phrma note 
		UPDATE  PN  SET  PN.Note1=R.OtherNotes
		FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID
	


		IF OBJECT_ID('tempdb..#Medication') IS NOT NULL  
			DROP TABLE #Medication 
	
		IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  
			DROP TABLE #RxSegments 

		IF OBJECT_ID('tempdb..#Response') IS NOT NULL  
					DROP TABLE #Response 
						
SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [DrFirst].[UpdateAllergyResponse]    Script Date: 3/14/2018 12:17:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[UpdateAllergyResponse]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [DrFirst].[UpdateAllergyResponse] AS' 
END
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [DrFirst].[UpdateAllergyResponse]
	-- Add the parameters for the stored procedure here
	@xmlStringData varchar(max)	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#XmlTable') IS NOT NULL
    DROP TABLE #XmlTable

	CREATE Table #XmlTable		
	(	xmlData Xml
	)

	select  * from #XmlTable
	
	DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)

	SELECT @XML = XMLData FROM #XmlTable

	EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML

	SELECT Clnical_ExtID, Patient_ExtID, AllergyName,Appointment_ExtID
	INTO #Response FROM OPENXML(@hDoc, 'Response/AllergyList/Allergy/Allergen')
	WITH 
	(
	Clnical_ExtID [varchar](50) '../ExternalID',
	Patient_ExtID [varchar](50) '../Patient/ExternalID',
	AllergyName [varchar](100) 'Name',
	Appointment_ExtID [varchar](50) '../EncounterList/Encounter/ExternalID'
	)

	EXEC sp_xml_removedocument @hDoc

select * from PatientClinical where patientid=116889  

select top 10  * from [dm].[Drugstable] where drugid in (1978,2697,1978,2696)



		--Update P set P.FindingDetail=replace(Replace(Replace(Replace(Replace(Replace(Replace(TMP.FD,'Duration',R.Duration),'Refills',R.Refills),'QtyUnit',R.Quantity+' '+R.QuantityUnit),'PO',R.[Route]),'Frequency',R.Frequency),'Strength',R.Strength),'Dosage',R.Dose )   from #Medication Tmp 
		--Inner join #Response R ON Tmp.ClinicalId=R.ClinicalID
		--INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId


END




GO
/****** Object:  StoredProcedure [DrFirst].[UpdateMedicationResponse]    Script Date: 3/14/2018 12:17:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[UpdateMedicationResponse]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [DrFirst].[UpdateMedicationResponse] AS' 
END
GO


Alter     PROCEDURE [DrFirst].[UpdateMedicationResponse]
@PatientID BIGINT,
@appointmentId BIGINT,
@xmlStringData varchar(max)

AS
SET NOCOUNT ON

--return 

IF OBJECT_ID('tempdb..#Medication') IS NOT NULL DROP TABLE #Medication 
IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  DROP TABLE #RxSegments 
--declare @PatientID bigint=40943sal
--declare @appointmentId bigint=3312639

		CREATE TABLE #RxSegments
		(
			ClinicalId int,
			SegTwo int,
			SegThree int,
			SegFour int,
			SegFive int,
			SegSix int,
			SegSeven int,
			SegNine int,
			LegacyDrug bit
		)

		INSERT INTO #RxSegments
		SELECT pc.clinicalid
		, CHARINDEX('-2', pc.FindingDetail) + 3
		, CHARINDEX(')-3', pc.FindingDetail) + 4
		, CHARINDEX(')-4', pc.FindingDetail) + 4
		, CHARINDEX(')-5', pc.FindingDetail) + 4
		, CHARINDEX(')-6', pc.FindingDetail) + 4
		, CHARINDEX(')-7', pc.FindingDetail) + 4
		, CHARINDEX('9-9', pc.FindingDetail)
		, CASE WHEN pc.FindingDetail like '%()%' THEN 0 ELSE 1 END 
		FROM PatientClinical pc
		WHERE-- pc.ClinicalId in (select clinicalid as number from patientclinical where clinicaltype='A' AND FindingDetail like 'Rx%' 
		 pc.PatientId = @PatientID
		AND FindingDetail like 'RX%'
		 AND clinicaltype='A' 
			
	
		select  DISTINCT pc.ClinicalId as ClinicalId,pc.PatientId ,A.appointmentid,
		(convert(varchar, convert(datetime, A.appdate), 101)+ ' ' 
	+ substring(A.appdate, 1, 2)+ ':' + substring(A.appdate, 3, 2)+ ':' + substring(A.appdate, 5, 2)) as AppDate,	
		A.resourceid1 as DoctorExternalId,ResourceName,ResourcePid as LoginID,
		pc.findingdetail,
		SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)) as restFD1,
		(SELECT STUFF((SELECT char(9) + Note1
					FROM patientnotes where patientid=@PatientID AND   Notetype in('R')
					FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'),1,1,''))  AS PatientNote
			
		,(SELECT STUFF((SELECT char(9) + Note1
					FROM patientnotes where patientid=@PatientID And  notetype in('B','C','X')
					FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'),1,1,'')) AS OtherNote,
					pc.DrawfileName as DrugId,
					(CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') 
		ELSE '' END) as DrugName,
		('RX-8/'+
		(CASE WHEN pc.FindingDetail like 'RX-8%-2%' 
			--THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6),'&','and'), '+','Plus') ELSE '' END)
			THEN  SUBSTRING(pc.findingdetail, 6, charindex('-2', pc.findingdetail) - 6) ELSE '' END)
		+
		'-2/(Dosage)(Strength)(EyeContext)-3/(Frequency)('+
		
		+'PRN)(PO)('+
		(CASE rx.LegacyDrug WHEN 0 THEN
			SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegThree+1)+1)+1)) - 1)
		ELSE ''END) --as OTC,		
		+	')('+
			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-1)) +')('+
			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-1))
			+')-4/(Refills)('+
			SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3),dbo.fn_Occurences('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2)+1,(dbo.fn_Occurences (')',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-([dbo].[fn_Occurences] ('(',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-1) 
			+')(QtyUnit)('+				
		(CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour)+1)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, rx.SegFour+1)+1)+1)) - 1)
		ELSE ''	END)-- as LastTaken,
		+')-5/('+
		CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX(')', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)
	ELSE '' END 
	+')('+			
		--(CASE rx.LegacyDrug WHEN 0 THEN
		--SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegFive+1) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, rx.SegFive+1)) - CHARINDEX('(', pc.FindingDetail, rx.SegFive+1) - 1)
		--ELSE ''	END)--as Supply,
		'DaySupply'

		+')'+		
		(SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)))) as FD,
		'' AS FD1,
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX(')', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)as FindingDuration,
		pc.[Status]
		INTO #Medication
		FROM PatientClinical pc 
		INNER JOIN appointments A ON A.Appointmentid=pc.Appointmentid
		INNER JOIN Resources R ON R.ResourceId=A.resourceid1	
		INNER JOIN #RxSegments rx on pc.ClinicalId = rx.ClinicalId
		LEFT OUTER JOIN PatientNotes pn1 on pn1.AppointmentId = pc.AppointmentId 
		AND Substring(PC.FindingDetail, 6, CHARINDEX('-2',pc.findingdetail)-6) = pn1.NoteSystem 
		AND pn1.NoteType = 'R'
		LEFT OUTER JOIN PatientNotes pn2 on pn2.AppointmentId = pc.AppointmentId
		AND SUBSTRING(pc.FindingDetail, 6, CHARINDEX('-2', pc.findingdetail)-6) = pn2.NoteSystem
		AND pn2.NoteType = 'X'			
		WHERE --pc.ClinicalId in (select clinicalid as number from patientclinical where  clinicaltype='A' AND FindingDetail like 'Rx%')
		pc.PatientId = @PatientID
		AND PC.FindingDetail like 'RX%'
		 AND PC.clinicaltype='A'

		
		DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)  
  
		SELECT   @XML= CAST(@xmlStringData AS XML)  
  
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML  
  
		SELECT 		
		Patient_ExtID, ClinicalID, PrescriptionRcopiaID,NDCID,
		FirstDataBankMedID,RcopiaID,
		RxnormID, RxnormIDType,
		BrandName,GenericName,DrugSchedule,BrandType,
		[RouteDrug],[Form],Strength,
		[Action],Dose, DoseUnit, 
		[Route], Frequency, Duration, 
		Quantity, QuantityUnit, 
		Refills,
		OtherNotes,PatientNotes,	
		SUBSTRING(SigChangedDate,1,10) as SigChangedDate,
		SUBSTRING(LastModifiedDate,1,10) as LastModifiedDate,
		SUBSTRING(StartDate,1,10) as StartDate,
		SUBSTRING(StopDate,1,10) as StopDate,
		SigChangedDate as response_SigChangedDate,
		LastModifiedDate as response_LastModifiedDate,
		StartDate as response_StartDate,
		StopDate as response_StopDate,

		Deleted,
		StopReason,
		MedicationRcopiaID,

		PharmacyRcopiaID ,
		PharmacyRcopiaMasterID,
		PharmacyDeleted ,
		PharmacyName ,
		PharmacyAddress1 ,
		PharmacyState ,
		PharmacyZip,
		PharmacyPhone ,
		PharmacyFax ,
		PharmacyIs24Hour ,
		PharmacyLevel3 ,
		PharmacyElectronic ,
		PharmacyMailOrder ,
		PharmacyRequiresEligibility ,
		PharmacyRetail ,
		PharmacyLongTermCare ,
		PharmacySpecialty ,
		PharmacyCanReceiveControlledSubstance 
		INTO #Response  
		FROM OPENXML(@hDoc, 'RCExtResponse/Response/MedicationList/Medication/Sig/Drug')  
		WITH   
		(  			
		Patient_ExtID [varchar](50) '../../Patient/ExternalID',  
		ClinicalID [varchar](50) '../../ExternalID',
		PrescriptionRcopiaID [varchar](50) '../../Prescription/RcopiaID',	

		NDCID [VARCHAR](100) 'NDCID',
		FirstDataBankMedID [VARCHAR](100) 'FirstDataBankMedID',
		RcopiaID [VARCHAR](100) 'RcopiaID',	
		
		RxnormID  [VARCHAR](100)'RxnormID',
		RxnormIDType  [VARCHAR](100)'RxnormIDType',
					
		BrandName [varchar](200) 'BrandName',  
		GenericName [varchar](200) 'GenericName',  
		DrugSchedule [varchar](200) 'Schedule', 
		BrandType [VARCHAR](100) 'BrandType',		
		[RouteDrug]  [varchar](200) 'Route',
		[Form] [varchar](100) 'Form',		
		Strength  [varchar](200) 'Strength',  
		
		[Action]  [varchar](200) '../Action', 				 	 
		Dose  [varchar](200) '../Dose',
		DoseUnit  [varchar](200) '../DoseUnit',
		[Route]  [varchar](200) '../Route',   
		Frequency  [varchar](200) '../DoseTiming',  
		Duration  [varchar](200) '../Duration',  
		Quantity  [varchar](200) '../Quantity',  		
		QuantityUnit  [varchar](200) '../QuantityUnit',  		
		Refills  [varchar](200) '../Refills',  
		OtherNotes  [varchar](200) '../OtherNotes',  
		PatientNotes  [varchar](200) '../PatientNotes', 
		  		
		SigChangedDate [varchar](50) '../../SigChangedDate' ,
		LastModifiedDate [varchar](50) '../../ExternLastModifiedDatealID' ,
		StartDate [varchar](50) '../../StartDate',
		StopDate [varchar](50) '../../StopDate',
		Deleted [varchar](50) '../../Deleted' ,
		StopReason [varchar](200) '../../StopReason',
		MedicationRcopiaID VARCHAR(50)'../../RcopiaID',

		PharmacyRcopiaID [varchar](50) '../../Pharmacy/RcopiaID',
		PharmacyRcopiaMasterID [varchar](50) '../../Pharmacy/RcopiaMasterID',
		PharmacyDeleted [varchar](50) '../../Pharmacy/Deleted',
		PharmacyName [varchar](50) '../../Pharmacy/Name',
		PharmacyAddress1 [varchar](50) '../../Pharmacy/Address1',
		PharmacyState [varchar](50) '../../Pharmacy/State',
		PharmacyZip [varchar](50) '../../Pharmacy/Zip',
		PharmacyPhone [varchar](50) '../../Pharmacy/Phone',
		PharmacyFax [varchar](50) '../../Pharmacy/Fax',
		PharmacyIs24Hour [varchar](50) '../../Pharmacy/Is24Hour',
		PharmacyLevel3 [varchar](50) '../../Pharmacy/Level3',
		PharmacyElectronic [varchar](50) '../../Pharmacy/Electronic',
		PharmacyMailOrder [varchar](50) '../../Pharmacy/MailOrder',
		PharmacyRequiresEligibility [varchar](50) '../../Pharmacy/RequiresEligibility',
		PharmacyRetail [varchar](50) '../../Pharmacy/Retail',
		PharmacyLongTermCare [varchar](50) '../../Pharmacy/LongTermCare',
		PharmacySpecialty [varchar](50) '../../Pharmacy/Specialty',
		PharmacyCanReceiveControlledSubstance [varchar](50) '../../Pharmacy/CanReceiveControlledSubstance'	
		 )  
		 EXEC sp_xml_removedocument @hDoc 
		
		INSERT INTO [DrFirst].[eRx_Medicine_Response]
      ([Patient_ExtID]   ,AppointmentId   ,[ClinicalID]      ,[NDCID]      ,[FirstDataBankMedID]      ,[RcopiaID]     
	   ,RxnormID, RxnormIDType,[BrandName]
      ,[GenericName] ,   DrugSchedule ,[BrandType]      ,[RouteDrug]      ,[Form]      ,[Strength]      ,[Action]
      ,[Dose]      ,[DoseUnit]      ,[Route]      ,[Frequency]      ,[Duration]      ,[Quantity]
      ,[QuantityUnit]      ,[Refills]      ,[OtherNotes]      ,[PatientNotes]      ,[SigChangedDate]      ,[LastModifiedDate]
      ,[StartDate]      ,[StopDate]      ,[Deleted]      ,[StopReason]      ,[MedicationRcopiaID], PrescriptionRcopiaID,
	    PharmacyRcopiaID , PharmacyRcopiaMasterID,PharmacyDeleted , PharmacyName , PharmacyAddress1 , PharmacyState , 
		PharmacyZip, PharmacyPhone , PharmacyFax ,PharmacyIs24Hour , PharmacyLevel3 , PharmacyElectronic , 
		PharmacyMailOrder , PharmacyRequiresEligibility , PharmacyRetail , PharmacyLongTermCare , PharmacySpecialty ,
		PharmacyCanReceiveControlledSubstance 	
	  )
	  SELECT  [Patient_ExtID] ,@appointmentId, [ClinicalID]      ,[NDCID]      ,[FirstDataBankMedID]      ,[RcopiaID]    
	   , RxnormID, RxnormIDType  ,[BrandName]      ,[GenericName],DrugSchedule
      ,[BrandType]      ,[RouteDrug]      ,[Form]      ,[Strength]      ,[Action]      ,[Dose]      ,[DoseUnit]      ,[Route]
      ,[Frequency]      ,[Duration]      ,[Quantity]      ,[QuantityUnit]      ,[Refills]      ,[OtherNotes]      ,[PatientNotes],
	  [response_SigChangedDate]      ,[response_LastModifiedDate]      ,
	  CASE WHEN [StartDate] ='' THEN [response_StopDate] ELSE [response_StartDate] END       ,
	  [response_StopDate]  ,[Deleted]      ,[StopReason]      ,[MedicationRcopiaID], PrescriptionRcopiaID,     
	  PharmacyRcopiaID , PharmacyRcopiaMasterID,PharmacyDeleted , PharmacyName , PharmacyAddress1 , PharmacyState , 
	  PharmacyZip, PharmacyPhone , PharmacyFax ,PharmacyIs24Hour , PharmacyLevel3 , PharmacyElectronic , 
	PharmacyMailOrder , PharmacyRequiresEligibility , PharmacyRetail , PharmacyLongTermCare , PharmacySpecialty ,
	PharmacyCanReceiveControlledSubstance 
	  FROM #Response WHERE [MedicationRcopiaID] NOT IN( SELECT Tmp.[MedicationRcopiaID] FROM [DrFirst].[eRx_Medicine_Response] Tmp WHERE Tmp.[Patient_ExtID]=@PatientID AND Tmp.Deleted=#Response.Deleted  AND Tmp.StopDate=#Response.StopDate )
			
		SELECT  ROW_NUMBER() OVER(ORDER BY MedicationRcopiaID) AS ROWNUMBER, 
		 --'1234567'  as Patient_ExtID, '1234567' AS AppointmentId,
		@PatientID AS Patient_ExtID ,@appointmentId AS AppointmentId,
		ClinicalID,MedicationRcopiaID,BrandName,Form,RouteDrug,
		(CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END) AS [EyeContext],
		'99' as [Symptom],
		 RcopiaID,NDCID,
		 'RX-8/'+R.BrandName+' '+R.Form+'-2/('+Dose+' '+DoseUnit+')('+Strength+')('+
		(CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END)
		+')-3/('+Frequency+')('+
		(CASE WHEN R.Refills='PRN' THEN '' ELSE '' END)+')('+
		--(Case WHEN R.[Action]='Apply' THEN 'APPLY TO LIDS' WHEN R.[Action]='INSTILL' THEN 'INSTILL IN EYES' WHEN R.[Action]='' THEN  'PO' ELSE R.[Route] END)+')()()()-4/('+
		 R.[Action] +' '+R.[Route] +')()()()-4/('+
		--(CASE WHEN R.Refills IN('PRN','') THEN '' ELSE R.Refills+' '+'REFILLS' END)+')()('+
		(CASE  WHEN R.Refills ='' THEN '' WHEN R.Refills ='PRN' THEN 'PRN REFILLS' ELSE R.Refills+' '+'REFILLS' END)+')()('+
		(R.Quantity+' '+R.QuantityUnit)+')()-5/()('+
		(CASE WHEN R.Duration <>'' THEN R.Duration+' DAYS SUPPLY' ELSE R.Duration END)+')-6/' as Findings,
	
		'!'+ CONVERT(varchar(20),CAST(Ap.appdate AS  DATE),101)   AS ImgDescriptor,

		--('!'+StartDate ) AS ImgDescriptor,
		FirstDataBankMedID,
		--'A' as [Status],
		(case when Deleted = 'n' then 'A' ELSE 'D'  end) AS [Status],
		PatientNotes,
		OtherNotes,
		StartDate,
		StopDate,
		PrescriptionRcopiaID		
		INTO #NewMeds
		FROM #Response R
		INNER  JOIN appointments ap ON Ap.Appointmentid=@appointmentId AND Ap.Patientid=@PatientID
		WHERE
		 (ClinicalID =''  OR ClinicalID=0)	
			AND R.MedicationRcopiaID NOT IN( SELECT RcopiyaID FROM [DrFirst].[ErxMedicationMapping] WHERE PatientID=@PatientID)
		AND   ISNULL(R.PrescriptionRcopiaID,'') NOT IN( SELECT PrescriptionRcopiaID FROM [DrFirst].[ErxMedicationMapping] WHERE PatientID=R.Patient_ExtID AND  PrescriptionRcopiaID IS NOT NULL)
	
		


		DECLARE @NewMedCount INT
		SELECT @NewMedCount= COUNT(*) FROM #NewMeds
		print @NewMedCount
--select COUNT(*) FROM #NewMeds
		DECLARE @loop INT
		SET @loop = 1

		WHILE @loop <= @NewMedCount
		BEGIN

			---SELECT  * FROM #NewMeds WHERE ROWNUMBER=@loop
			
			INSERT INTO dbo.PatientClinical
			(
			[AppointmentId]
			,[PatientId]
			,[ClinicalType]
			,[EyeContext]
			,[Symptom]
			,[FindingDetail]
			,[ImageDescriptor]      
			,[Status]
			,[DrawFileName]			
			,Activity)
			SELECT  AppointmentId, Patient_ExtID,'A',
			EyeContext,Symptom,
			--Findings, 
			CASE WHEN  PrescriptionRcopiaID IS NOT NULL THEN Findings+'P-7' ELSE Findings END,
			ImgDescriptor, [Status], FirstDataBankMedID, CASE WHEN PrescriptionRcopiaID IS NOT NULL  THEN 'pen' ELSE '' END
			FROM #NewMeds TT 			
			WHERE ROWNUMBER=@loop

			DECLARE @ClinicalID_New BIGINT 
			SELECT @ClinicalID_New=IDENT_CURRENT('PatientClinical')

			--select @ClinicalID_New			
			INSERT INTO [DrFirst].[ErxMedicationMapping]
			([ClinicalId],[EncounterId],[PatientId],[RcopiyaID],[MedDatetime],PrescriptionRcopiaID)
			SELECT @ClinicalID_New, @appointmentId, @PatientID,MedicationRcopiaID,GETDATE() ,PrescriptionRcopiaID
			FROM #NewMeds  WHERE  ROWNUMBER=@loop

			INSERT INTO [DrFirst].[PatientClinicalMeds]
			([ClinicalId] ,ExternalId,RcopiaId, RxnormId, NDC, [AppointmentId], 
			[PatientId], [FindingDetail],[status], DrugId ,[CreatedDatetime],IsUpdate)
			Select @ClinicalID_New,@ClinicalID_New,MedicationRcopiaID,'',NDCID,
			@appointmentId,Patient_ExtID,Findings,[Status],FirstDataBankMedID,Getdate() ,0 
			From #NewMeds 
			WHERE  ROWNUMBER=@loop AND @ClinicalID_New NOT IN(Select [ClinicalId] FROM [DrFirst].[PatientClinicalMeds])
			
 

			IF NOT EXISTS (SELECT 1 FROM Drug D INNER JOIN #NewMeds N ON D.DisplayName=N.BrandName+' '+ N.Form  AND D.Focus=1 and D.DrugDosageFormId is NULL WHERE ROWNUMBER=@loop )
			BEGIN
				INSERT INTO Drug (Drugcode,DisplayName,Family1,Family2,Focus,DrugDosageFormId)
				SELECT 'T',BrandName+' '+Form,'','',1,NULL FROM #NewMeds WHERE RouteDrug='opht' AND  ROWNUMBER=@loop
			END

--select @ClinicalID_New

			INSERT INTO  dbo.PatientNotes 
			(
				PatientId,
				AppointmentId,
				NoteType,
				Note1,
				NoteDate,
				NoteSystem, 
				NoteCommentOn,
				NoteAudioOn,
				NoteClaimOn,
				ClinicalId 
			)
			SELECT 
			@PatientID, 
			@appointmentId,
			'R',
			PatientNotes,			
			RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),
			(BrandName+' '+Form),
			'T','F','F',
			@ClinicalID_New
			FROM   #NewMeds
			WHERE  ROWNUMBER=@loop

			INSERT INTO  dbo.PatientNotes 
			(
				PatientId,
				AppointmentId,
				NoteType,
				Note1,
				NoteDate,
				NoteSystem, 
				NoteCommentOn,
				NoteAudioOn,
				NoteClaimOn,
				ClinicalId 
			)
			SELECT 
			@PatientID, 
			@appointmentId,
			'X',
			OtherNotes,			
			RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),
			(BrandName+' '+Form),
			'T','F','F',
			@ClinicalID_New
			FROM   #NewMeds
			WHERE  ROWNUMBER=@loop			

			SET @loop = @loop + 1 								  
		END

		IF OBJECT_ID('tempdb..#NewMeds') IS NOT NULL  
					DROP TABLE #NewMeds 
 
		
		Update RS SET RS.clinicalid=Emm.Clinicalid 
		FROM  #Response  RS 
		INNER  JOIN [DrFirst].[ErxMedicationMapping] Emm ON RS.Patient_ExtID=Emm.Patientid 
		AND	(RS.PrescriptionRcopiaID=Emm.PrescriptionRcopiaID OR RS.MedicationRcopiaID=Emm.RcopiyaID)

		Update Emm SET Emm.RcopiyaID=RS.MedicationRcopiaID 
		FROM  #Response  RS 
		INNER  JOIN [DrFirst].[ErxMedicationMapping] Emm ON RS.Patient_ExtID=Emm.Patientid 
		AND RS.PrescriptionRcopiaID=Emm.PrescriptionRcopiaID
		
		
		UPDATE P 
		set P.FindingDetail=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TMP.FD,'EyeContext',
		 (CASE WHEN R.[Route] like '%right eye' THEN 'OD' WHEN R.[Route] like '%left eye%' THEN 'OS' WHEN R.[Route] like '%both eyes' THEN 'OU' ELSE '' END)),
		'PRN',(CASE WHEN R.Refills='PRN' THEN '' ELSE ''END)),'DaySupply',
		(CASE WHEN R.Duration <>'' THEN R.Duration+' DAYS SUPPLY' ELSE R.Duration END))
		,'(Refills)',CASE  WHEN R.Refills ='' THEN '()' WHEN R.Refills ='PRN' THEN '(PRN REFILLS)' ELSE '('+R.Refills+' '+'REFILLS'+')' END),'QtyUnit',R.Quantity+' '+R.QuantityUnit),'(PO)',
		--(Case WHEN R.[Action]='Apply' THEN 'APPLY TO LIDS' WHEN R.[Action]='INSTILL' THEN 'INSTILL IN EYES' WHEN R.[Action]='' THEN  'PO' ELSE R.[Route] END)),
		 LTRIM(RTRIM('('+ R.[Action]+' '+R.[Route]+')' ))),
		'Frequency',R.Frequency),'Strength',R.Strength),'Dosage',R.Dose+' '+ R.DoseUnit) 				 
			,Activity=CASE WHEN R.PrescriptionRcopiaID IS NOT NULL  THEN 'pen' ELSE Activity END		
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId
		
		UPDATE P  set P.FindingDetail= Case when  P.Activity='pen' AND CHARINDEX('/P-7', P.FindingDetail)=0 THEN REPLACE(P.FindingDetail,'-6/','-6/P-7/') ELSE  P.FindingDetail END 
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId
		WHERE P.Activity='pen'

			--For ChangeERXdate 
		Update P set
		--P.ImageDescriptor=CASE WHEN LEFT(P.ImageDescriptor, 1)<>'D' THEN 'D'+R.StopDate+P.ImageDescriptor ELSE P.ImageDescriptor END
		P.ImageDescriptor= CASE 
							WHEN R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) AND Left(P.ImageDescriptor, 1)='!' AND R.Stopdate= convert(varchar(10),getdate(),101) THEN 'D'+R.StopDate+P.ImageDescriptor 
							WHEN  R.StopDate=RIGHT(Left(P.ImageDescriptor, 11),10)  AND Left(P.ImageDescriptor, 1)='!' THEN STUFF(P.ImageDescriptor , 1, 1, 'D') ELSE P.ImageDescriptor
						  END
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId WHERE R.StopDate<>'' AND R.Deleted='n' 
		--AND R.Stopdate= convert(varchar(10),getdate(),101)

		

		Update P set
		P.ImageDescriptor=CASE 
							WHEN --R.StopDate<>Right(P.ImageDescriptor, 10) AND 
								R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) 
							THEN '!'+R.StopDate+P.ImageDescriptor 
							ELSE P.ImageDescriptor 
						END		
		,P.Status='D'
		FROM #Medication Tmp 
		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId
		WHERE R.Deleted='y'
		
		----For-Patiet Note		
		UPDATE  PN  SET  PN.Note1=R.PatientNotes FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID 
			AND PN.PatientId=R.Patient_ExtID AND NoteType='R'AND R.Clinicalid <>''

		--For Phrma note 
		UPDATE  PN  SET  PN.Note1=R.OtherNotes
		FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID AND PN.PatientId=R.Patient_ExtID AND  NoteType='X'AND R.Clinicalid <>''

		INSERT INTO PatientNotes (PatientId,AppointmentId,NoteType,Note1,NoteDate,NoteSystem, NoteCommentOn,NoteAudioOn,NoteClaimOn,ClinicalId )
		SELECT  @PatientID, @appointmentId,'R',	PatientNotes,RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),(BrandName+' '+Form),'T','F','F',ClinicalID 
		FROM  #Response R  WHERE R.ClinicalID NOT IN(Select  P.ClinicalID  FROM PatientNotes P  WHERE Patientid=@PatientID ) AND R.Clinicalid <>''


		INSERT INTO PatientNotes (PatientId,AppointmentId,NoteType,Note1,NoteDate,NoteSystem, NoteCommentOn,NoteAudioOn,NoteClaimOn,ClinicalId )
		SELECT  @PatientID, @appointmentId,'X',	OtherNotes,RIGHT('0' + CAST(year(StartDate) AS NVARCHAR(4)), 4)+RIGHT('0' + CAST(MONTH(StartDate) AS NVARCHAR(2)), 2)+RIGHT('0' + CAST(DAY(StartDate) AS NVARCHAR(2)), 2),(BrandName+' '+Form),'T','F','F',ClinicalID 
		FROM  #Response R  WHERE R.ClinicalID NOT IN(Select  ClinicalID  FROM PatientNotes P  WHERE P.Patientid=@PatientID) AND R.Clinicalid <>''

	
	 UPDATE PCM  SET   PCM.ExternalID= R.ClinicalID, PCM.RcopiaId=R.MedicationRcopiaID , Isupdate=0,status=Deleted  FROM   #Response R  INNER JOIN Drfirst.[PatientClinicalMeds] PCM ON R.ClinicalID=PCM.ClinicalID
		

		IF OBJECT_ID('tempdb..#Medication') IS NOT NULL  
			DROP TABLE #Medication 
	
		IF OBJECT_ID('tempdb..#RxSegments') IS NOT NULL  
			DROP TABLE #RxSegments 

		IF OBJECT_ID('tempdb..#Response') IS NOT NULL  
					DROP TABLE #Response 
	
SET NOCOUNT OFF


GO
