

/****** Object:  Table [MVE].[PP_ERPInputLog]    Script Date: 7/17/2018 4:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_ERPInputLog]') AND type in (N'U'))
BEGIN

CREATE TABLE [MVE].[PP_ERPInputLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](100) NULL,
	[PatientId] [int] NULL,
	[InputLogDate] [datetime] NULL,
	[IsEmailUpdated] [bit] NULL CONSTRAINT [DF_PP_ERPInputLog_IsEmailUpdated]  DEFAULT ((0)),
	[IsPhoneUpdated] [bit] NULL CONSTRAINT [DF_PP_ERPInputLog_IsPhoneUpdated]  DEFAULT ((0)),
	[IsAddressUpdated] [bit] NULL CONSTRAINT [DF_PP_ERPInputLog_IsAddressUpdated]  DEFAULT ((0)),
	[Status] [varchar](2) NULL,
	[ProcessedDate] [datetime] NULL,
	[IsPatientUpdated] [bit] NULL CONSTRAINT [DF_PP_ERPInputLog_IsPatientUpdated]  DEFAULT ((0)),
 CONSTRAINT [PK_PP_ERPLOG] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

END

SET ANSI_PADDING OFF
GO


Insert into MVE.PP_ERPInputLog(Email, PatientId, InputLogDate, IsEmailUpdated, IsPhoneUpdated, IsAddressUpdated, Status, ProcessedDate, IsPatientUpdated)  
Select Value, PatientId, GETDATE(), 1, 1, 1, 'C', GETDATE(), 1 From Model.PatientEmailAddresses Where OrdinalId = 1

GO


