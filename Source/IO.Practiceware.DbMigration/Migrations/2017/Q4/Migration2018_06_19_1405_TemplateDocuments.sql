--ICD Decimal Point on Paper Claims (CMS-1500)
GO
DECLARE @cnt INT = 1;
WHILE @cnt <= 12
BEGIN
DECLARE @old VARCHAR(max), @new VARCHAR(max)
SET @old = 'Model.Diagnosis'+cast(@cnt as varchar(2))+'.Replace(".","")'
SET @new = 'Model.Diagnosis'+cast(@cnt as varchar(2))
 
IF EXISTS (
 SELECT * FROM model.TemplateDocuments 
 WHERE DisplayName = 'CMS-1500'
  AND ContentOriginal IS NOT NULL)
BEGIN

 UPDATE model.TemplateDocuments  
 SET ContentOriginal=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),ContentOriginal), @old, @new)) 
 FROM model.TemplateDocuments   
 WHERE DisplayName = 'CMS-1500' AND ContentOriginal IS NOT NULL

END
   SET @cnt = @cnt + 1;
END;
GO

