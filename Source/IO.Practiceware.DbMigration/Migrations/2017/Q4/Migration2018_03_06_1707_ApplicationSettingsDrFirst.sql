--if not exists(Select * From model.Applicationsettings Where Name = 'ERxStagingUploadUrl')
--Begin
--SET ANSI_PADDING ON

--  Insert into model.Applicationsettings(Value, Name, MachineName, UserId) Values('https://engine201.staging.drfirst.com/servlet/rcopia.servlet.EngineServlet', 'ERxStagingUploadUrl', NULL, 2) 
--End
--GO

--SET ANSI_PADDING ON
--GO

--if not exists(Select * From model.Applicationsettings Where Name = 'ERxStagingDownloadUrl')
--Begin
--SET ANSI_PADDING ON

--  Insert into model.Applicationsettings(Value, Name, MachineName, UserId) Values('https://update201.staging.drfirst.com/servlet/rcopia.servlet.EngineServlet', 'ERxStagingDownloadUrl', NULL, 2) 
--End
--GO

--SET ANSI_PADDING ON
--GO
--if not exists(Select * From model.Applicationsettings Where Name = 'ERxStagingSSOUrl')
--Begin
--SET ANSI_PADDING ON

--  Insert into model.Applicationsettings(Value, Name, MachineName, UserId) Values('https://web201.staging.drfirst.com/sso/portalServices', 'ERxStagingSSOUrl', NULL, 2) 
--End
--GO

--SET ANSI_PADDING ON
--GO
--if not exists(Select * From model.Applicationsettings Where Name = 'ERxStagingPortalHeader')
--Begin
--SET ANSI_PADDING ON

--  Insert into model.Applicationsettings(Value, Name, MachineName, UserId) Values('service=rcopia&action=login&rcopia_user_external_id=ext_doc_123&rcopia_patient_external_id=<patientId>&rcopia_portal_system_name=ivendor35&close_window=n&logout_url=Close&time=<time>&rcopia_practice_user_name=io98981&startup_screen=<screencontext>&limp_mode=<n>', 'ERxStagingPortalHeader', NULL, 2) 
--End
--GO

--SET ANSI_PADDING ON
--GO
--if not exists(Select * From model.Applicationsettings Where Name = 'ERxCredentials')
--Begin
--SET ANSI_PADDING ON

--  Insert into model.Applicationsettings(Value, Name, MachineName, UserId) Values('<Erx><Version>2.28</Version><Caller><VendorName>ivendor35</VendorName><VendorPassword>8ynw5pwm</VendorPassword></Caller><SystemName>**sys_name**</SystemName><RcopiaPracticeUsername>io98981</RcopiaPracticeUsername></Erx>', 'ERxCredentials', NULL, 2) 
--End
--GO

SET ANSI_PADDING ON
GO
If Exists(select * from information_schema.columns where table_name = 'ACI_INPUTLOG' 
and column_name = 'INSERTED' 
and Table_schema = 'dbo'
and column_default is NULL)
ALTER TABLE [dbo].[ACI_INPUTLOG] ADD  DEFAULT (getdate()) FOR [INSERTED]
GO

If Exists(select * from information_schema.columns where table_name = 'ACI_INPUTLOG' 
and column_name = 'STATUS' 
and Table_schema = 'dbo'
and column_default is NULL)
ALTER TABLE [dbo].[ACI_INPUTLOG] ADD  DEFAULT ('N') FOR [STATUS]
GO

