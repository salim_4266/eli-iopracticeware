
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[EthnicitiesTmp]') AND type in (N'U'))--ALTER TABLE dbo.DocExe DROP CONSTRAINT FK_Column_B; 
DROP TABLE  [model].[EthnicitiesTmp]


GO
/****** Object:  Table [model].[EthnicitiesTmp]    Script Date: 12/14/2017 2:10:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[EthnicitiesTmp]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[EthnicitiesTmp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[IsArchived] [bit] NOT NULL DEFAULT ((0)),
	[OrdinalId] [int] NOT NULL DEFAULT ((1)),
	[Code] [nvarchar](20) NULL,
	[OMBCode] [nvarchar](20) NULL,
	[OMBCategory] [nvarchar](500) NULL,
 CONSTRAINT [PK_EthnicitiesTmp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO


INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES ( N'Andalusian', 0, 1, N'2138-6', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Argentinean', 0, 1, N'2166-7', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Asturian', 0, 1, N'2139-4', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Belearic Islander', 0, 1, N'2142-8', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Bolivian', 0, 1, N'2167-5', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Canal Zone', 0, 1, N'2163-4', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Canarian', 0, 1, N'2145-1', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Castillian', 0, 1, N'2140-2', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Catalonian', 0, 1, N'2141-0', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Central American', 0, 1, N'2155-0', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Central American Indian', 0, 1, N'2162-6', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Chicano', 0, 1, N'2151-9', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Chilean', 0, 1, N'2168-3', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Colombian', 0, 1, N'2169-1', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Costa Rican', 0, 1, N'2156-8', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Criollo', 0, 1, N'2176-6', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Cuban', 0, 1, N'2182-4', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Dominican', 0, 1, N'2184-0', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Ecuadorian', 0, 1, N'2170-9', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Gallego', 0, 1, N'2143-6', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Guatemalan', 0, 1, N'2157-6', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Honduran', 0, 1, N'2158-4', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'La Raza', 0, 1, N'2152-7', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Latin American', 0, 1, N'2178-2', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Mexican', 0, 1, N'2148-5', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Mexican American', 0, 1, N'2149-3', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Mexican American Indian', 0, 1, N'2153-5', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Mexicano', 0, 1, N'2150-1', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Nicaraguan', 0, 1, N'2159-2', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Panamanian', 0, 1, N'2160-0', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Paraguayan', 0, 1, N'2171-7', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Peruvian', 0, 1, N'2172-5', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Puerto Rican', 0, 1, N'2180-8', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Salvadoran', 0, 1, N'2161-8', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'South American', 0, 1, N'2165-9', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'South American Indian', 0, 1, N'2175-8', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Spaniard', 0, 1, N'2137-8', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Spanish Basque', 0, 1, N'2146-9', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Uruguayan', 0, 1, N'2173-3', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Valencian', 0, 1, N'2144-4', N'2135-2', N'Hispanic or Latino')
INSERT [model].[EthnicitiesTmp] ([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory]) VALUES (N'Venezuelan', 0, 1, N'2174-1', N'2135-2', N'Hispanic or Latino')


IF NOT EXISTS (SELECT 1 FROM sys.columns  WHERE Name = N'Code'  AND Object_ID = Object_ID(N'[model].[Ethnicities]'))
Alter TABLE [model].[Ethnicities] add [Code][nvarchar](20) NULL

IF  NOT EXISTS  (SELECT 1 FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[model].[Ethnicities]') AND name = 'OMBCode')
Alter TABLE [model].[Ethnicities] add [OMBCode][nvarchar](20) NULL

IF  NOT EXISTS  (SELECT 1 FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[model].[Ethnicities]') AND name = 'OMBCategory')
Alter TABLE [model].[Ethnicities] add [OMBCategory][nvarchar](500) NULL

UPDATE [model].[Ethnicities]  SET [Code]='2135-2', [OMBCode]='2135-2', [OMBCategory]='Hispanic or Latino' WHERE Id=1
UPDATE [model].[Ethnicities]  SET [Code]='2186-5', [OMBCode]='', [OMBCategory]='' WHERE Id=2
UPDATE [model].[Ethnicities]  SET [Code]='UNK', [OMBCode]='UNK', [OMBCategory]='nullFlavor' WHERE Id=3

INSERT INTO [model].[Ethnicities]([Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory])
SELECT [Name], [IsArchived], [OrdinalId], [Code], [OMBCode], [OMBCategory] FROM [model].[EthnicitiesTmp] WHERE [Name] NOT IN(SELECT [Name] FROM [model].[Ethnicities])

Set ANSI_PADDING ON
Insert into model.PatientEthnicities(PatientId, EthnicityId) Select Id, ethnicityid From model.Patients Where ethnicityid is not NULL
Set ANSI_PADDING OFF

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[EthnicitiesTmp]') AND type in (N'U'))
DROP TABLE  [model].[EthnicitiesTmp]