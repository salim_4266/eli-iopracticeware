go
-------------------
IF EXISTS (SELECT * FROM   sys.objects WHERE  object_id = OBJECT_ID(N'[dbo].[getEncryptPassword]') AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' ))
    DROP FUNCTION [dbo].[getEncryptPassword]
GO 

Create Function getEncryptPassword(@strPwd nvarchar(200)) RETURNS NVARCHAR(2000)
As
BEGIN
   return Reverse(@strPwd)    
END

Go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_EmailStatus]') AND type in (N'U'))
BEGIN
	drop table [MVE].[PP_EmailStatus]
END
GO