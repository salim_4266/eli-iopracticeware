

/****** Object:  Trigger [model].[PP_LogEmailDeleteTrigger]    Script Date: 7/17/2018 4:56:11 PM ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[PP_LogEmailDeleteTrigger]'))
DROP TRIGGER [model].[PP_LogEmailDeleteTrigger]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [model].[PP_LogEmailDeleteTrigger] ON [model].[PatientEmailAddresses] AFTER DELETE NOT FOR REPLICATION AS
BEGIN
SET NOCOUNT ON
DECLARE @PatientId INT
SELECT @PatientId = DELETED.PatientId FROM DELETED
IF EXISTS(Select * FROM [MVE].[PP_ERPInputLog] with (nolock) where PatientId = @PatientId and Status = 'N')
	BEGIN
		Update MVE.PP_ERPInputLog Set Email = '', IsEmailUpdated = 1 Where PatientId = @PatientId --and Status  = 'N'
	END
ELSE
	BEGIN
	  INSERT INTO [MVE].[PP_ERPInputLog](Email, PatientId, InputLogDate, IsEmailUpdated, IsPhoneUpdated, IsAddressUpdated, Status, ProcessedDate, IsPatientUpdated) 
	  VALUES('', @PatientId, GETDATE(), 0, 1, 0, 'N', NULL, 1)
	End
SET NOCOUNT OFF
END
GO


