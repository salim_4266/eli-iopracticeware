GO
Set ANSI_PADDING ON
Update model.SmokingConditions Set SNoMedCode = '8517006', Name = 'Former Smoker' Where Id = 1
Update model.SmokingConditions Set SNoMedCode = '449868002', Name = 'Current Every Day Smoker' Where Id = 2
Update model.SmokingConditions Set SNoMedCode = '428041000124106', Name = 'Current Some Day Smoker' Where Id = 3
Update model.SmokingConditions Set SNoMedCode = '428071000124103', Name = 'HEAVYSMOKE' Where Id = 4
Update model.SmokingConditions Set SNoMedCode = '428061000124105', Name = 'LIGHTSMOKE' Where Id = 5
Update model.SmokingConditions Set SNoMedCode = '77176002', Name = 'Smoker, Current Status Unknown' Where Id = 6
Update model.SmokingConditions Set SNoMedCode = '266927001', Name = 'Unknown If Ever Smoked' Where Id = 7
Update model.SmokingConditions Set SNoMedCode = '266919005', Name = 'Never Smoker' Where Id = 8
GO
