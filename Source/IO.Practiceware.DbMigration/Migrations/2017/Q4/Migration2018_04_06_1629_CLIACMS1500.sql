DECLARE @old VARCHAR(max), @new VARCHAR(max)
SET @old = '&& Model.AuthorizationCode != string.Empty'
    
SET @new = ''


IF EXISTS (
 SELECT * FROM model.TemplateDocuments 
 WHERE DisplayName = 'CMS-1500'
  AND Content IS NOT NULL)
BEGIN 

 UPDATE model.TemplateDocuments  
 SET Content=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),Content), @old, @new)) 
 FROM model.TemplateDocuments   
 WHERE DisplayName = 'CMS-1500' AND Content IS NOT NULL 

END
ELSE
BEGIN

 UPDATE model.TemplateDocuments  
 SET ContentOriginal=CONVERT(varbinary(max),REPLACE(CONVERT(varchar(max),ContentOriginal), @old, @new)) 
 FROM model.TemplateDocuments   
 WHERE DisplayName = 'CMS-1500' AND ContentOriginal IS NOT NULL

END
