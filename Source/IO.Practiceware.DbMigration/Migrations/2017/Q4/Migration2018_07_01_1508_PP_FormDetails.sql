
/****** Object:  Table [MVE].[PP_FormDetails]    Script Date: 06/29/2018 13:26:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_FormDetails]') AND type in (N'U'))
BEGIN

CREATE TABLE [MVE].[PP_FormDetails](
	[IND] [int] IDENTITY(1,1) NOT NULL,
	[CreationDate] [datetime] NULL,
	[FormLayoutName] [nvarchar](100) NULL,
	[ID] [nvarchar](100) NOT NULL,
	[PatientExternalId] [nvarchar](50) NOT NULL,
	[PatientFullName] [nvarchar](100) NULL,
	[Status] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[PatientExternalId] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

END
GO
