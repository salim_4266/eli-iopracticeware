
/****** Object:  Trigger [DrFirst].[ACI_TR_JSON16_MedicationsOrder_UPDATE]    Script Date: 05/17/2018 18:13:46 ******/
GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[DrFirst].[ACI_TR_JSON16_MedicationsOrder_UPDATE]')) 
Drop Trigger [DrFirst].[ACI_TR_JSON16_MedicationsOrder_UPDATE] 
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [DRFIRST].[ACI_TR_JSON16_MedicationsOrder_UPDATE]  ON [DRFIRST].[eRx_Medicine_Response]
AFTER UPDATE 
NOT FOR REPLICATION 
AS
BEGIN
  DECLARE @TABLEKEY varchar(20)
  SET @TABLEKEY=''
  IF (UPDATE(Form) OR UPDATE(Strength) OR UPDATE(Action) OR UPDATE(Dose) OR UPDATE(DoseUnit) OR UPDATE(Route) OR UPDATE(Duration) OR UPDATE(Quantity) OR UPDATE(QuantityUnit) OR UPDATE(Refills) OR UPDATE(StartDate)OR UPDATE(StopDate) OR UPDATE(OtherNotes)OR UPDATE(PatientNotes))
  BEGIN
	  Declare db_INPUTLOG_cursor CURSOR FOR  
	  SELECT i.PrescriptionRcopiaID FROM INSERTED i WHERE  i.PrescriptionRcopiaID  is not null and i.rxnormid is not null	
	  OPEN db_INPUTLOG_cursor
	  FETCH NEXT FROM db_INPUTLOG_cursor INTO @TABLEKEY 
	  WHILE @@FETCH_STATUS = 0  
	  BEGIN 
	  IF len(@TABLEKEY) > 0
		 EXEC ACI_INSERT_INPUTLOG 'JSON16_MedicationsOrder',@TABLEKEY,'CLINICALID',@TABLEKEY		
		FETCH NEXT FROM db_INPUTLOG_cursor INTO  @TABLEKEY 
	  END
  CLOSE db_INPUTLOG_cursor
END
END

GO


