IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'dbo.ACI_ConvertToJson_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT')) 
Drop Trigger dbo.ACI_ConvertToJson_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc[dbo].[ACI_ConvertToJson_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT](@PATIENTCATARACTPLANKEY INT, @aciinputlogkey int)
as
begin
/*
ACI Version :Release 1.1
Date:08/21/2017
Developer/Author:K.V.S.S.R.KRISHNA SARMA
Details/Description: To create JSON String in ACI_JSON_MAster table
*/
/*
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/
    declare @StringJson_26 varchar(Max)
    declare @aciinputlogkey1 int, @tableKey INT, @ACIJSONMasterKey int,@COLLECTEDBILLINGKEY int
    set @COLLECTEDBILLINGKEY=0
    set @aciinputlogkey1 = 0
    set @tableKey = 0
    set @ACIJSONMasterKey = 0
    set @tableKey = @PATIENTCATARACTPLANKEY
    set @StringJson_26 = ''

    select top 1 @aciinputlogkey1 = m.aciinputlogkey from ACI_JSON_Master m(nolock)
    join ACI_InPutLog c (nolock) on c.aciinputlogkey = m.aciinputlogkey and c.status = 'N' and  c.TableName = 'JSON26_SURGICALPOSTOPERATIVERESULTEVENT'
    where m.status = 'N' and c.status = 'N' and  c.TableName = 'JSON26_SURGICALPOSTOPERATIVERESULTEVENT'
    and c.aciinputlogkey = @ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 = 0
	begin

		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@CHARTRECORDKEY1 INT ,@PatId INT
		 ,@Procedures varchar(8000),@ExamField varchar(8000),@Diagnosis Varchar(5000),@Medication Varchar(5000),@Reason Varchar(5000)
		set @PhyId =0
		set @FacId =0
		set @CHARTRECORDKEY1=0
		set @PatId =0

		Set @Procedures=''
		set @ExamField =''
		set @Diagnosis =''
		set @Medication=''

		--select top 1 @PhyId=c.PROVIDERKEY,@FacId=s.OFFICELOCATIONKEY ,@PatId=s.PATIENTKEY,@CHARTRECORDKEY1=pcp.CHARTRECORDKEY  from PATIENTCATARACTPLAN PCP (nolock)
		--     JOIN chartrecords c  ON c.CHARTRECORDKEY=pcp.CHARTRECORDKEY
		--	join SCHEDULE s (nolock) on s.SCHEDULEKEY  =c.SCHEDULEKEY          	
		--	where PCP.PATIENTCATARACTPLANKEY =@PATIENTCATARACTPLANKEY order by c.CHARTRECORDKEY desc

			select top 1 @PhyId=Ap.ResourceId1,@FacId=R.PracticeId ,@PatId=Ap.PatientId  from dbo.Appointments Ap (nolock) 
			join dbo.Resources R (nolock) on R.ResourceId  =Ap.ResourceId1
		where ap.appointmentid =@PATIENTCATARACTPLANKEY order by ap.appointmentid desc;

		
		set @ReferenceNumber =''
		select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.'+CAST(@FacId as varchar(10))+'.'+CAST(@PhyId as varchar(10))+'.'+CAST(@PatId as varchar(10))+'.26.' from aci_json_Defaults (NOLOCK)
--Added
		Declare @EPI varchar(50)
		Set @EPI = ''
		Select top 1 @EPI = ISNULL(cast(EpisodeNumber as varchar(10)),'') from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT
		where ExternalId = @PATIENTCATARACTPLANKEY
--Finished Adding
		Declare @ReferenceNumberT varchar(50),@LabRadiologyOrder varchar(5000),@OrderCode varchar(5000)
		set @ReferenceNumberT =''
		set @LabRadiologyOrder =''
		set @OrderCode =''
		set @ReferenceNumberT =@ReferenceNumber+cast(@EPI as varchar(10))
--Changed By Ayush 2018 Nov
			IF (SELECT COUNT(*) FROM VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT SE where SE.ExternalId =@PATIENTCATARACTPLANKEY)>0
				SELECT @StringJson_26= REPLACE(REPLACE( DBO.ACI_QFN_XMLTOJSON (
				(
				SELECT top 1	
						   ISNULL(ResourceType,'') AS ResourceType,
						   ISNULL(IsSource,'') AS IsSource,
						   (isnull(@ReferenceNumber,'')+Cast(ReferenceNumber as varchar(20) )) AS ReferenceNumber,
						   ISNULL(PatientAccountNumber,'') AS PatientAccountNumber,
						   ISNULL(DateofEvent,'') AS DateofEvent,
						   ISNULL(ECNPI,'') AS ECNPI,
						   --ISNULL(ECTIN,'') AS ECTIN,
						    (SUBSTRING(ECTIN,1,3)+'-'+SUBSTRING(ECTIN,4,2)+'-'+SUBSTRING(ECTIN,6,4)) AS ECTIN,
						   ISNULL(ExternalId,'') AS ExternalId,
						--   ISNULL(EncounterId,'') AS EncounterId,
						   ISNULL(EpisodeNumber,'') AS EpisodeNumber,
						   --ISNULL(ReferralOrderNumber,'') AS ReferralOrderNumber,
						   ISNULL(IsInHousePhysician,'') AS IsInHousePhysician,
						   ISNULL(PerformedPhysicianNPI,'') AS PerformedPhysicianNPI,
						   ISNULL(PerformedPhysicianAccountNumber,'') AS PerformedPhysicianAccountNumber,
						   ISNULL(PerformedPhysicianName,'') AS PerformedPhysicianName
				  from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT SE where  SE.ExternalId =@PATIENTCATARACTPLANKEY FOR XML PATH('Level_1'),TYPE ) ) ,'[','') ,'}]','')     
 
		   if ltrim(rTrim(@StringJson_26)) <> ''
		   begin
			  DECLARE CURSORA_26 SCROLL CURSOR FOR 
				select Id from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_Procedure SP where SP.AppointmentId = @PATIENTCATARACTPLANKEY 
			  OPEN CURSORA_26   
			  FETCH FIRST FROM CURSORA_26 INTO @COLLECTEDBILLINGKEY
			  WHILE @@FETCH_STATUS=0
			  BEGIN
	 
				  set @ExamField =''
				  set @Diagnosis =''
				  set @Medication=''
				  set @Reason =''

				 select  @Procedures= replace(--'}'
				 replace(--']'
				 replace --'['
				  (ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select top 1 
								 ISNULL(Code,'') AS Code,
								 ISNULL(Description,'') AS Description,
								 ISNULL(CodeSystemName,'') AS CodeSystemName,
								 ISNULL(CodeSystem,'') AS CodeSystem								
								 from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_Procedure SP (NOLOCK) where SP.AppointmentId = @PATIENTCATARACTPLANKEY for xml path('Level_2'),type
							 ) 
						 )
					 ,'') 
					 ,'[','')
					 ,']','')
					 ,'}','')
 
					 --select @Route
					 if @Procedures<>''
						  select  @ExamField=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Type,'') AS Type,
							  ISNULL(Code,'') AS Code,
							  ISNULL(Description,'') AS Description,
							  ISNULL(CodeSystem,'') AS CodeSystem,
							  ISNULL(CodeSystemName,'') AS CodeSystemName
									 from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_ExamField SPE (NOLOCK) where   SPE.AppointmentId = @PATIENTCATARACTPLANKEY  for xml path('Level_2'),type
							 ) 
						 )
						  ,'')
 
					 if @Procedures<>''
						  select  @Diagnosis=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Code,'') AS Code,
							 ISNULL(Description,'') AS Description,
							 ISNULL(CodeSystem,'') AS CodeSystem,
							 ISNULL(CodeSystemName,'') AS CodeSystemName
						  from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Diagnosis  SD (NOLOCK) where   SD.AppointmentId = @PATIENTCATARACTPLANKEY  for xml path('Level_2'),type
							 ) 
						 )
						  ,'')
 
					 if @Procedures<>''
						  select  @Medication=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Code,'') AS Code,
							 ISNULL(Description,'') AS Description,
							 ISNULL(CodeSystem,'') AS CodeSystem,
							 ISNULL(CodeSystemName,'') AS CodeSystemName
									 from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Medication  SM (NOLOCK) where   SM.AppointmentId = @PATIENTCATARACTPLANKEY  for xml path('Level_2'),type
							 ) 
						 )
						  ,'')

 
					 if @Procedures<>''
						  select  @Reason=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
						      ISNULL(Type,'') AS Type,
							 ISNULL(Code,'') AS Code,
							 ISNULL(Description,'') AS Description,
							 ISNULL(CodeSystem,'') AS CodeSystem,
							 ISNULL(CodeSystemName,'') AS CodeSystemName
									 from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Reason SR (NOLOCK) where   SR.AppointmentId = @PATIENTCATARACTPLANKEY  for xml path('Level_2'),type
							 ) 
						 )
						  ,'')

                      if @ExamField <>'' 
				    set @ExamField=',"ExamField":'+@ExamField
                      if @Diagnosis <>'' 
				    set @Diagnosis=',"Diagnosis":'+@Diagnosis
                      if @Medication <>'' 
				    set @Medication=',"Medication":'+@Medication
                      if @Reason <>'' 
				    set @Reason=',"Reason":'+@Reason

				  if @Procedures  <>''
				    set @Procedures =@Procedures+@ExamField+@Diagnosis+@Medication+@Reason+'}'
				  set @ExamField =''
				  set @Diagnosis =''
				  set @Medication=''
				  set @Reason =''
				--	select @Procedures
				FETCH NEXT FROM CURSORA_26 INTO @COLLECTEDBILLINGKEY
			  END
			  CLOSE CURSORA_26
			  DEALLOCATE CURSORA_26
		   end	
		   --select @Procedures
		  
			if ltrim(rTrim(@StringJson_26)) <> ''
			begin
			  set @StringJson_26 =@StringJson_26+',"Procedure": ['+@Procedures+']}'
			  
			  insert into ACI_JSON_Master(ACIInPutLogKey, tablekey, json,ReferenceNumber,Sort)
			                values(@aciinputlogkey, @tableKey, (ltrim(rTrim(@StringJson_26))),@ReferenceNumberT,26)

			set @ACIJSONMasterKey = IDENT_CURRENT('ACI_JSON_Master')
		   	update ACI_InPutLog set status = 'C', statuschangedt = GETDATE() where status = 'N' and TableName ='JSON26_SURGICALPOSTOPERATIVERESULTEVENT' 
			and(tableKey = @tableKey and tablekey in (select tablekey from ACI_JSON_Master(nolock) where status = 'N'
				and ACIJSONMasterKey = @ACIJSONMasterKey))
				--select @StringJson_26			
			end
	end
end


