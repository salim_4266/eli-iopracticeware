

/****** Object:  Trigger [model].[trgAfterInsert]    Script Date: 7/19/2018 12:11:24 PM ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[trgAfterInsert]'))
DROP TRIGGER [model].[trgAfterInsert]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [model].[trgAfterInsert] ON [model].[PatientAddresses]
FOR INSERT
NOT FOR REPLICATION 
AS

SET NOCOUNT ON
BEGIN

	DECLARE @AddressID INT

	SELECT @AddressID = i.Id FROM inserted i;

		UPDATE [model].[PatientAddresses] Set PostalCode=REPLACE(PostalCode, '-', '') Where Id=@AddressId
	
		UPDATE [model].[PatientAddresses] SET PostalCode = REPLACE(PostalCode,' ', '') WHERE Id = @AddressID
	
END
SET NOCOUNT OFF


GO


