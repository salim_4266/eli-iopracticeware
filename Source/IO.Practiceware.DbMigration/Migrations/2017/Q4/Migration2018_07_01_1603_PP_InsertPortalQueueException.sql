

/****** Object:  StoredProcedure [MVE].[PP_InsertPortalQueueException]    Script Date: 06/29/2018 13:44:58 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_InsertPortalQueueException]')) 
Drop Procedure [MVE].[PP_InsertPortalQueueException]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  



CREATE procedure [MVE].[PP_InsertPortalQueueException]
(
@MessageData nvarchar(max),
@ActionTypeLookupId int,
@ProcessedDate datetime,
@PatientNo varchar(50),
@IsActive bit,
@ExternalId nvarchar(500),
@Exception text
)
as
Begin
   Declare @RecordCount int
   Set @RecordCount=0
   Select @RecordCount=Cast(ProcessCount as int) from [MVE].[PP_PortalQueueException]  where ActionTypeLookupId=@ActionTypeLookupId and ExternalId=@ExternalId

If(@RecordCount > 0)
 Begin
   Set @RecordCount = @RecordCount + 1   
   Update [MVE].[PP_PortalQueueException] set ProcessCount=@RecordCount+1 ,Exception=@Exception  where ActionTypeLookupId=@ActionTypeLookupId and ExternalId=@ExternalId
 End
Else
 Begin
     Insert into [MVE].[PP_PortalQueueException](MessageData,ActionTypeLookupId,ProcessedDate,PatientNo,IsActive,ExternalId,Exception, ProcessCount) Values (@MessageData,  @ActionTypeLookupId, @ProcessedDate, @PatientNo, @IsActive, @ExternalId, @Exception, 1)
 End
End

GO
