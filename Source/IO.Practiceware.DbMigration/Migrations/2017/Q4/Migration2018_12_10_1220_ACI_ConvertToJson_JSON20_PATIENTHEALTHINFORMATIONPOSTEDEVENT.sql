IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'dbo.ACI_ConvertToJson_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT')) 
Drop Trigger dbo.ACI_ConvertToJson_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ACI_ConvertToJson_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT] (@CCDAXMLFileKey INT,@aciinputlogkey int)
as 
begin
/*
ACI Version :Release 1.0
Date:05/05/2017
Developer:ASHISH VIRMANI
Details/Description: To create JSON String in ACI_JSON_MAster table
SELECT * FROM mve.PP_PORTALqUEUERESPONSE
*/
/*
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/ 
  declare @StringJson varchar(8000)
  declare @aciinputlogkey1 int ,@tableKey VARCHAR(20), @ACIJSONMasterKey int
  set @aciinputlogkey1 =0
  set @tableKey=''
  set @ACIJSONMasterKey =0
  set @tableKey=@CCDAXMLFileKey
  set @StringJson=''

	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status='N'  and  c.TableName = 'JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT' 
	where m.status ='N' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 =0
	begin
		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@PatId INT--, @CHARTRECORDKEY1 int 
		set @PhyId =0
		set @FacId =0
		set @PatId =0

		--select @CHARTRECORDKEY1=ChartRecordKey from CCDAXMLFILES (NOLOCK) where CCDAXMLFILEKEY=@CCDAXMLFILEKEY 
		
			SELECT TOP 1 @PhyId = A.ResourceId1, @PatId = A.PatientId, @FacId = R.PracticeId  
			FROM dbo.Appointments A(NOLOCK) LEFT JOIN dbo.Resources R on R.ResourceId = A.ResourceId1
            WHERE A.appointmentid = @CCDAXMLFileKey AND A.ResourceId1 <> 0 AND A.APPTYPEID <> 0 
			--AND A.SCHEDULESTATUS = 'D' AND A.ACTIVITYSTATUS = 'D'  -- Changed by Ayuh -- Commented this part 
			ORDER BY A.APPDATE DESC

		  --SELECT TOP 10 * FROM dbo.appointments
		--select top 1 @PhyId=cc.PROVIDERKEY,@FacId=OFFICELOCATIONKEY ,@PatId=cc.PATIENTKEY  from 
		--CCDAXMLFILES cc (NOLOCK)
		--join patient p (NOLOCK) on p.patientkey =cc.patientkey
		--where CCDAXMLFileKey =@CCDAXMLFileKey	 
		
		set @ReferenceNumber =''
		select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.'+CAST(@FacId as varchar(10))+'.'+CAST(@PhyId as varchar(10))+'.'+CAST(@PatId as varchar(10))+'.20.' from aci_json_Defaults (NOLOCK)

		Declare @ReferenceNumberT varchar(50)
		set @ReferenceNumberT =''
		set @ReferenceNumberT =@ReferenceNumber+cast(@CCDAXMLFileKey as varchar(10))


        select @StringJson=@StringJson+ 
            REPLACE(REPLACE(
					dbo.aci_qfn_XmlToJson (
					(     
					select 
						 ISNULL(ResourceType,'') AS ResourceType,
						 ISNULL(IsSource,'') AS IsSource,
						 isnull(@ReferenceNumber,'')+ISNULL(ReferenceNumber,'') AS ReferenceNumber,
						 ISNULL(PatientAccountNumber,'') AS PatientAccountNumber,
						 ISNULL(PostedDate,'') AS PostedDate
					from VIEW_ACI_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT L where l.APPOINTMENTID =@CCDAXMLFileKey	  for xml path('pat'),type) 
            ),'[','') ,'}]','}')       

			--select @StringJson

      if ltrim(rTrim(@StringJson)) <> ''
	  begin
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,ReferenceNumber,Sort) 
                 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))),@ReferenceNumberT,20)  

		set @ACIJSONMasterKey=IDENT_CURRENT('ACI_JSON_Master')
		update ACI_InPutLog set status ='C',statuschangedt=GETDATE() where status ='N'  and  TableName = 'JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT'  
		and( tableKey=@tableKey and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where status='N' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
  end
end