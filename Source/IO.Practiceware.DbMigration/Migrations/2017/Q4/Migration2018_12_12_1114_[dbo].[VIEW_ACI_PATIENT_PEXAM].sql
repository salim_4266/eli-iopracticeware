IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'dbo.VIEW_ACI_PATIENT_PEXAM')) 
Drop Trigger dbo.VIEW_ACI_PATIENT_PEXAM
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VIEW_ACI_PATIENT_PEXAM]
AS
	SELECT
		'PATIENTEXAM' AS RESOURCETYPE,
		'EHR' AS IsSource, --R
		CAST(ap.appointmentid as Varchar(10)) AS ReferenceNumber, --R
		(CONVERT(VARCHAR(20), CONVERT(DATE,ap.AppDate), 101)) AS DATE,
		CASE
			WHEN ((select count(*) from dbo.aci_inputlog where Tablekey =ap.appointmentid and sort = 7 ) >1)THEN 'AMENDMENT'
			WHEN(((select count(*) from [model].[PatientReconcileMedications] M1 where M1.appointmentid =ap.appointmentid) >0)or((select count(*) from [model].[PatientReconcileAllergy] A1 where A1.appointmentid =ap.appointmentid) >0)or((select count(*) from [model].[PatientReconcileProblems] P1 where P1.appointmentid =ap.appointmentid) >0)) THEN 'RECONCILEDCHART'
			ELSE 'EXAM'
		END AS Type, --R
		ap.Patientid AS PatientAccountNumber, --R
		ap.appointmentid as EXTERNALID,
		ap.PatientID as PatientKey,
		ap.appointmentid as ChartRecordkey
	from
	dbo.appointments ap WITH(NOLOCK) left join dbo.practiceactivity pa WITH(NOLOCK)
	on ap.appointmentid =pa.appointmentid
	where --ap.activitystatus ='D' and ap.schedulestatus= 'D' and pa.status ='D' and 
	ap.apptypeid>0
	--select top 10 * from dbo.appointments




GO
