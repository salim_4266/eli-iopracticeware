

/****** Object:  Trigger [ManageDiagnosisToProblemList]    Script Date: 11/29/2017 7:38:57 PM ******/
IF EXISTS (SELECT * FROM sysobjects WHERE type = 'TR' AND name = '[dbo].[ManageDiagnosisToProblemList]')
    
DROP TRIGGER [dbo].[ManageDiagnosisToProblemList]
GO

/****** Object:  Trigger [dbo].[ManageDiagnosisToProblemList]    Script Date: 11/29/2017 7:38:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE  TRIGGER [dbo].[ManageDiagnosisToProblemList]
ON [dbo].[PatientClinicalIcd10]
FOR INSERT
NOT FOR REPLICATION
--This trigger helps with the join from PatientClincial to PatientNotes.
AS
BEGIN

SET NOCOUNT ON

	DECLARE  @AppointmentId  BIGINT
	DECLARE  @PatientId  BIGINT
	DECLARE  @Icd10  nvarchar(100)
	DECLARE  @Icd9  nvarchar(100)
	DECLARE  @Description  nvarchar(500)
	DECLARE  @Status  nvarchar(2)

	DECLARE  @ConceptID  BIGINT
	DECLARE  @Term  nvarchar(500)
	DECLARE  @AppDate  nvarchar(20)
	DECLARE @ResourceId Int

	--select * from  model.PatientProblemList  where patientid=10896


IF OBJECT_ID('tempdb..#MINConcept') IS NOT NULL
	DROP TABLE #MINConcept
IF OBJECT_ID('tempdb..#GroupIcd1') IS NOT NULL
	DROP TABLE #GroupIcd1
IF OBJECT_ID('tempdb..#GroupIcd') IS NOT NULL
	DROP TABLE #GroupIcd



	SELECT @AppointmentId=AppointmentId, @PatientId=PatientId, @Icd10=FindingDetailIcd10, @Description=[Description], @Status=[Status] FROM inserted WHERE ClinicalTYpe='B'


	select * from [PatientClinicalIcd10] where
				AppointmentId=@AppointmentId and  @PatientId=PatientId and  ClinicalTYpe='B'


	SELECT MIN(ConceptID) as ConceptID , MIn(icd10)  as  ICD10 INTO #MINConcept  FROM  SNOMED_2016    GROUP BY  ICD10	
	
	SET @ConceptID=0
	SELECT  TOP 1 @ConceptID=ConceptID  FROM #MINConcept WHERE ICD10=@Icd10

	SELECT  TOP 1 @Term=Term FROM SNOMED_2016 WHERE ICD10=@Icd10 AND ConceptID=@ConceptID
	
	
	--SELECT  TOP 1 @ConceptID=ConceptID, @Term=Term FROM SNOMED_2016 WHERE ICD10=@Icd10
	
	If @ConceptID=0
	BEGIN
		SET @ConceptID=74964007
		SET @Term='Other'		
	END
			

	SELECT  @AppDate= AppDate  , @ResourceId= ResourceId1 FROM  Appointments WHERE AppointmentId=@AppointmentId AND PatientId=@PatientId

	IF(@ConceptID IS NOT NULL AND @ConceptID>0)
	BEGIN
	 IF  NOT EXISTS (SELECT 1 FROM model.PatientProblemList  WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId AND ConceptID=@ConceptID AND Term=@Term AND Icd10=@Icd10 AND ICD10DESCR=@Description)
	  BEGIN
	  
		INSERT INTO model.PatientProblemList (
		PatientId,
		AppointmentId,
		AppointmentDate,
		ConceptID,
		Term,
		ICD10,
		ICD10DESCR,
		COMMENTS,
		ResourceName,
		OnsetDate,
		LastModifyDate,
		Status,
		ResourceId,
		EnteredDate,
		ResolvedDate)
		VALUES(@PatientId,@AppointmentId,  CONVERT(DATETIME,@AppDate, 101),@ConceptID, @Term, @Icd10,@Description,NUll,NULL,
		 CONVERT(DATETIME,@AppDate, 101), CONVERT(DATETIME,@AppDate, 101), CASE WHEN @Status='A' THEN 'Active' ELSE 'InActive' END ,@ResourceId,  CONVERT(DATETIME,@AppDate, 101),NUll)

		--SELECT  @icd9=comments FROM model.PatientProblemList  WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId AND ConceptID=@ConceptID AND Term=@Term AND Icd10=@Icd10 AND ICD10DESCR=@Description

		UPDATE model.PatientProblemList SET Status='Deleted' WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId  AND Status='Active'
		
		UPDATE model.PatientProblemList SET Status='Active' 
		WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId 
		AND Icd10   in( select FindingDetailIcd10 from [PatientClinicalIcd10] where
		AppointmentId=@AppointmentId and  PatientId=@PatientId and  ClinicalTYpe='B')

		SELECT  * INTO  #GroupIcd FROM 
		(select MIN(comments) as ICD9 , MIN(icd10)as icd10, MIN(status) as Status
		from  model.PatientProblemList  where patientid=@PatientId   and appointmentid=@AppointmentId AND  Comments  IS NOT NULL  
		group by Comments, patientid,appointmentid,ResourceName)  Tmp where  Status<>'Active'
										
		UPdate  T  SET Status='Active' FROM  model.PatientProblemList AS T  Inner JOIN #GroupIcd G  ON G.icd10=T.icd10 AND T.Comments=G.ICD9
		WHere T.Status='Deleted' AND   patientid=@PatientId   and appointmentid=@AppointmentId

	END 
	ELSE 
	BEGIN
		 --SELECT @PatientId  ,@AppointmentId ,@ConceptID ,@Term ,@Icd10 ,@Description,CASE WHEN @Status='A' THEN 'Active' ELSE 'InActive' END
			
			--SELECT  @icd9=comments FROM model.PatientProblemList  WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId AND ConceptID=@ConceptID AND Term=@Term AND Icd10=@Icd10 AND ICD10DESCR=@Description

			--UPdate model.PatientProblemList SET Status='InActive' WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId AND ConceptID=@ConceptID 
			--AND Term=@Term AND ICD10DESCR<>@Description  AND Icd10 <> (@Icd10) AND comments=@icd9	
			
			--update model.PatientProblemList SET Status='InActive' WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId 
			UPDATE model.PatientProblemList SET Status='Deleted' WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId  AND Status='Active'
			--WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId 
			-- AND Icd10  not in( select FindingDetailIcd10 from [PatientClinicalIcd10] where
			--	AppointmentId=@AppointmentId and  @PatientId=PatientId and  ClinicalTYpe='B')

			UPDATE model.PatientProblemList SET Status='Active' 
			WHERE PatientId=@PatientId AND  AppointmentId=@AppointmentId 
			 AND Icd10   in( select FindingDetailIcd10 from [PatientClinicalIcd10] where
				AppointmentId=@AppointmentId and  PatientId=@PatientId and  ClinicalTYpe='B')

				SELECT  * INTO  #GroupIcd1 FROM 
--				(select MIN(CONVERT (float ,comments)) as ICD9 , MIN(icd10)as icd10, MIN(status) as Status
				(select MIN(comments) as ICD9 , MIN(icd10)as icd10, MIN(status) as Status
				from  model.PatientProblemList  where patientid=@PatientId   and appointmentid=@AppointmentId AND  Comments  IS NOT NULL  
				group by Comments, patientid,appointmentid, ResourceName)  Tmp where  Status<>'Active'

								
				UPdate  T  SET Status='Active' FROM  model.PatientProblemList AS T  Inner JOIN #GroupIcd1 G  ON G.icd10=T.icd10 AND T.Comments=G.ICD9
				WHere T.Status='Deleted' AND   patientid=@PatientId   and appointmentid=@AppointmentId
				

	
		END
			--VALUES(@PatientId,@AppointmentId,  CONVERT(DATETIME,@AppDate, 101),@ConceptID, @Term, @Icd10,@icd10_Desc,null,NULL,
			-- Convert(datetime,@AppDate, 101), Convert(datetime,@AppDate, 101), CASE WHEN @Status='A' THEN 'Active' ELSE 'InActive' END ,@ResourceId,  Convert(datetime,@AppDate, 101),NUll)
	END

END
GO


SET ANSI_PADDING ON
GO
