
IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON23_DR_COMMUNICATION_EVENT')) 
Drop Procedure ACI_ConvertToJson_JSON23_DR_COMMUNICATION_EVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE PROC [dbo].[ACI_ConvertToJson_JSON23_DR_COMMUNICATION_EVENT] (@PtOutboundReferralKey INT,@ACIINPUTLOGKEY INT)
AS 
BEGIN
/*
ACI VERSION :RELEASE 1.1
DATE:07/24/2017
Developer/Author:K.V.S.S.R.KRISHNA SARMA
DETAILS/DESCRIPTION:ACI INTEGRATION RELEASE1.1 -  "JSON23– Dr Communication Event"
    ,TO CREATE JSON STRING IN ACI_JSON_MASTER TABLE
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/
/*                                                        ************************SCRIPT CASE SENSITIVE************  /


 EXEC ACI_CONVERTTOJSON_JSON23_DR_COMMUNICATION_EVENT 15,3
  SELECT * FROM ACI_JSON_MASTER ORDER BY 1 DESC
*/ 
  DECLARE @STRINGJSON VARCHAR(8000)
  DECLARE @ACIINPUTLOGKEY1 INT ,@TABLEKEY INT, @ACIJSONMASTERKEY INT
  SET @ACIINPUTLOGKEY1 =0
  SET @TABLEKEY=0
  SET @ACIJSONMASTERKEY =0
  SET @TABLEKEY=@PtOutboundReferralKey
  SET @STRINGJSON=''''

    SELECT TOP 1 @ACIINPUTLOGKEY1 = M.ACIINPUTLOGKEY
    FROM ACI_JSON_MASTER M(NOLOCK)
    JOIN ACI_INPUTLOG C(NOLOCK) ON C.ACIINPUTLOGKEY = M.ACIINPUTLOGKEY
            AND C.STATUS = ''N''
            AND C.TABLENAME = ''JSON23_DRCOMMUNICATIONEVENT''
    WHERE M.STATUS = ''N''
    and C.STATUS = ''N'' AND C.TABLENAME = ''JSON23_DRCOMMUNICATIONEVENT''
    AND C.ACIINPUTLOGKEY = @ACIINPUTLOGKEY
    ORDER BY M.ACIJSONMASTERKEY DESC;

 IF @ACIINPUTLOGKEY1 =0
 BEGIN
    DECLARE @REFERENCENUMBER VARCHAR(50),@PHYID INT , @FACID INT,@PATID INT,@CRProviderKey INT
    SET @PHYID =0
    SET @FACID =0
    SET @PATID =0
	SET @CRProviderKey =0

    SELECT TOP 1-- @PHYID = PO.ORDERINGPROVIDERKEY,
     @FACID = R.PracticeId,
     @PATID = D.PatientKey,
     @CRProviderKey =D.ProviderKey
    FROM dbo.ACI_DEP D WITH (NOLOCK)
     JOIN dbo.Resources R  WITH (NOLOCK) ON R.ResourceId = D.ProviderKey
    WHERE D.AciDepKey = @PtOutboundReferralKey;

   
    SET @REFERENCENUMBER = '''';
    if @PtOutboundReferralKey >0
    begin
        SELECT @REFERENCENUMBER = CAST(ACI_VENDORID AS VARCHAR(10))+''.''+CAST(MDO_PRACTICEID AS VARCHAR)+''.''+CAST(@FACID AS VARCHAR(10))+''.''+CAST(@CRProviderKey AS VARCHAR(10))+''.''+CAST(@PATID AS VARCHAR(10))+''.23.''
        FROM ACI_JSON_DEFAULTS(NOLOCK)
        end
        else
    begin
        SELECT @REFERENCENUMBER = CAST(ACI_VENDORID AS VARCHAR(10))+''.''+CAST(MDO_PRACTICEID AS VARCHAR(10))+''.''+CAST(@FACID AS VARCHAR(10))+''.''+CAST(@CRProviderKey AS VARCHAR(10))+''.''+CAST(@PATID AS VARCHAR(10))+''.23.''
        FROM ACI_JSON_DEFAULTS(NOLOCK)
	end
       

    DECLARE @REFERENCENUMBERT VARCHAR(50);
    SET @REFERENCENUMBERT = '''';
    SET @REFERENCENUMBERT = @REFERENCENUMBER +CAST(@PtOutboundReferralKey AS VARCHAR(20));
    
    IF
    (
    SELECT COUNT(*)
    FROM VIEW_ACI_JSON23_Dr_Communication_Event (nolock)  where ACIDEPKEY = @PtOutboundReferralKey
    ) > 0
    SELECT @StringJson = @StringJson+ISNULL(REPLACE(REPLACE(dbo.aci_qfn_XmlToJson
               (
               (
               SELECT TOP 1 ISNULL(ResourceType, '''') AS ResourceType,
                    ISNULL(IsSource, '''') AS IsSource,
                    (@ReferenceNumber+ISNULL(CAST(ReferenceNumber AS VARCHAR(10)), '''')) AS ReferenceNumber,
                    ISNULL(PatientAccountNumber, '''') AS PatientAccountNumber,
                    ISNULL(DateSent, '''') AS DateSent,
                    ISNULL(ToPhysicianName, '''') AS ToPhysicianName,
                    ISNULL(ECNPI, '''') AS ECNPI,
                    (SUBSTRING(ECTIN, 1, 3)+''-''+SUBSTRING(ECTIN, 4, 2)+''-''+SUBSTRING(ECTIN, 6, 4)) AS ECTIN,
                    ISNULL(ExternalId, '''') AS ExternalId,
                   -- ISNULL(ReferralOrderNumber, '''') AS ReferralOrderNumber,
                    ISNULL(Code, '''') AS Code,
                    ISNULL(Description, '''') AS Description,
                    ISNULL(CodeSystemName, '''') AS CodeSystemName,
                    ISNULL(CodeSystem, '''') AS CodeSystem
               FROM VIEW_ACI_JSON23_Dr_Communication_Event L (nolock) 
               WHERE L.ACIDEPKEY = @PtOutboundReferralKey  FOR XML PATH(''Communication1''), TYPE
               )
               ), ''['', ''''), ''}]'', ''}''), '''');
      IF LTRIM(RTRIM(@STRINGJSON)) <> ''''
   BEGIN
   --PRINT   @STRINGJSON
         INSERT INTO ACI_JSON_MASTER (ACIINPUTLOGKEY,TABLEKEY,JSON,REFERENCENUMBER,SORT) 
                 VALUES(@ACIINPUTLOGKEY,@TABLEKEY,(LTRIM(RTRIM(@STRINGJSON))),@REFERENCENUMBERT,23)  
    --PRINT  @STRINGJSON
  SET @ACIJSONMASTERKEY=IDENT_CURRENT(''ACI_JSON_MASTER'')
  UPDATE ACI_INPUTLOG SET STATUS =''C'',STATUSCHANGEDT=GETDATE() WHERE STATUS =''N'' AND  TABLENAME = ''JSON23_DRCOMMUNICATIONEVENT''  
  AND( TABLEKEY=@TABLEKEY AND aciinputlogkey IN(SELECT aciinputlogkey FROM ACI_JSON_MASTER (NOLOCK) WHERE STATUS=''N'' AND ACIJSONMASTERKEY=@ACIJSONMASTERKEY))
      END
  END
END


'
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON16_MedicationsOrder')) 
Drop Procedure ACI_ConvertToJson_JSON16_MedicationsOrder 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE proc [dbo].[ACI_ConvertToJson_JSON16_MedicationsOrder] (@PATIENTMEDICATIONSKEY varchar(20),@aciinputlogkey int)
as 
begin
/*
ACI Version :Release 1.0
Date:05/05/2017
Developer/Author:K.V.S.S.R.KRISHNA SARMA
Details/Description: To create JSON String in ACI_JSON_MAster table
*/
/*
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/  

    DECLARE @STRINGJSON_MEDICATION VARCHAR(MAX),@STRINGJSON_MED varchar(max),@PharmacyPhone Varchar(500),@PharmacyAddress varchar(1000)
    ,@Medication Varchar(max),@Route Varchar(8000)
    set @Medication =''''
    set @Route =''''
    set @STRINGJSON_MEDICATION =''''
    set @STRINGJSON_MED =''''
    set @PharmacyPhone =''''
    set @PharmacyAddress =''''


  declare @aciinputlogkey1 int ,@tableKey varchar(20), @ACIJSONMasterKey int, @chartRecordKey int , @patientid int, @eRx_Medicine_Response_Key int
  set @aciinputlogkey1 =0
  set @tableKey=''''
  set @ACIJSONMasterKey =0
  set @tableKey=@PATIENTMEDICATIONSKEY
  set @STRINGJSON_MEDICATION=''''
  set @chartRecordKey =0

    select top 1 @chartRecordKey = AppointmentId, @patientid = Patient_ExtID,@eRx_Medicine_Response_Key=eRx_Medicine_Response_Key from drfirst.eRx_Medicine_Response WHERE  cast(PrescriptionRcopiaID as varchar(20))=@PATIENTMEDICATIONSKEY
	ORDER by eRx_Medicine_Response_Key DESC


	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status=''N'' and  c.TableName = ''JSON16_MEDICATIONSORDER'' 
	where m.status =''N'' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 =0
	begin
	   declare @ReferenceNumber varchar(50)
	   Declare @ReferenceNumberT varchar(50)
	   set @ReferenceNumberT =''''
	   set @ReferenceNumber =''''

	   declare @PhyId int , @FacId int ,@PATIENTKEY1 int, @np varchar(20) , @tx varchar(20)
	   set @PhyId =0
	   set @FacId =0
	   set @np = ''''
	   set @tx = ''''

	   select top 1 @PhyId=Ap.ResourceId1,@FacId=R.PracticeId ,@PATIENTKEY1 = Ap.PatientId  , @np = r.NPI , @tx=r.ResourceTaxId from dbo.appointments Ap (nolock)
	   join dbo.Resources R (nolock) on R.ResourceId  =Ap.ResourceId1
	   where Ap.AppointmentId =@chartRecordKey order by Ap.AppointmentId desc
		
	   set @ReferenceNumber =''''
	   select @ReferenceNumber = aci_vendorID+''.''+MDO_PracticeId+''.''+CAST(@FacId as varchar(10))+''.''+CAST(@PhyId as varchar(10))+''.''+CAST(@PATIENTKEY1 as varchar(10))+''.16.'' from aci_json_Defaults (NOLOCK)
        set @ReferenceNumberT =@ReferenceNumber+cast(@chartRecordKey as varchar(10))

			IF (SELECT COUNT(*) FROM VIEW_ACI_JSON16_MEDICATIONSORDER (NOLOCK) WHERE  cast(PrescriptionRcopiaID as varchar(20))=@PATIENTMEDICATIONSKEY)>0
				SELECT @STRINGJSON_MEDICATION= REPLACE(REPLACE( DBO.ACI_QFN_XMLTOJSON (
				(
				SELECT 
						ISNULL(ResourceType,'''') AS ResourceType,
						ISNULL(IsSource,'''') AS IsSource,
						(isnull(@ReferenceNumber,'''')+ISNULL(ReferenceNumber,'''')) AS ReferenceNumber,																		
						ISNULL(PatientAccountNumber,'''') AS PatientAccountNumber
			 
				   FROM VIEW_ACI_JSON16_MEDICATIONSORDER (NOLOCK) WHERE  cast(PrescriptionRcopiaID as varchar(20))=@PATIENTMEDICATIONSKEY  FOR XML PATH(''MEDICATIONORDER''),TYPE ) ) ,''['','''') ,''}]'','''')     

	   if ltrim(rTrim(@STRINGJSON_MEDICATION)) <> ''''
	   begin
		  DECLARE CURSORA_med SCROLL CURSOR FOR 
		    select cast(PrescriptionRcopiaID as varchar(20)) from VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION (NOLOCK) where cast(PrescriptionRcopiaID as varchar(20))=@PATIENTMEDICATIONSKEY 
		  OPEN CURSORA_med   
		  FETCH FIRST FROM CURSORA_med INTO @PATIENTMEDICATIONSKEY
		  WHILE @@FETCH_STATUS=0
		  BEGIN
			 select  @Medication= replace(--''}''
			 replace(--'']''
			 replace --''[''
			  (ISNULL( dbo.ACI_qfn_XmlToJson(	
					 (select 
							ISNULL(ExternalId ,'''') AS ExternalId,ISNULL(@np ,'''') AS ECNPI,							
							(SUBSTRING(@tx,1,3)+''-''+SUBSTRING(@tx,4,2)+''-''+SUBSTRING(@tx,6,4))  as ECTIN
							,ISNULL(DrugName ,'''') AS DrugName ,ISNULL(DrugCode ,''0'') AS DrugCode ,ISNULL(DrugCodeSystem ,'''') AS DrugCodeSystem,
							ISNULL(DrugCodeSystemName ,'''') AS DrugCodeSystemName,ISNULL(MedicationType ,'''') AS MedicationType,ISNULL(PrescriptionType ,'''') AS PrescriptionType,
							ISNULL(Directions ,'''') AS Directions ,ISNULL(AdditionalInstructions ,'''') AS AdditionalInstructions,ISNULL(Strength ,'''') AS Strength,
							ISNULL(Substitution ,'''') AS Substitution,ISNULL(StartDate ,'''') AS StartDate,ISNULL(EffectiveDate ,'''') AS EffectiveDate,
							ISNULL(DaySupply ,'''') AS DaySupply,ISNULL(DosageQuantityValue ,'''') AS DosageQuantityValue,ISNULL(IsPrescribed ,'''') AS IsPrescribed,
							ISNULL(EprescribedDate ,'''') AS EprescribedDate,ISNULL(IsQueriedForDrugFormulary ,'''') AS IsQueriedForDrugFormulary,ISNULL(IsCPOE ,'''') AS IsCPOE,
							ISNULL(IsHighRiskMedication ,'''') AS IsHighRiskMedication,ISNULL(IsControlSubstance ,'''') AS IsControlSubstance,
							ISNULL(PharmacyName ,'''') AS PharmacyName,
							--ISNULL(ReconciledDate ,'''') AS ReconciledDate,
							ISNULL(Status ,'''') AS Status,ISNULL(DateEnd ,'''') AS DateEnd 

							 from VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION (NOLOCK) where eRx_Medicine_Response_Key =@eRx_Medicine_Response_Key for xml path(''Medication1''),type
						 ) 
					 )
				 ,'''') 
				 ,''['','''')
				 ,'']'','''')
				 ,''}'','''')
				 --select @Route
				 if @Medication<>''''
					  select  @Route= REPLACE(
					  REPLACE(
					  ISNULL( dbo.ACI_qfn_XmlToJson(	
					 (select top 1 
							  ISNULL(Code,'''') AS Code,ISNULL(Description,'''') AS Description,ISNULL(CodeSystem,'''') AS CodeSystem,ISNULL(CodeSystemName,'''') AS CodeSystemName							
								 from VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_ROUTE (NOLOCK) where eRx_Medicine_Response_Key =@eRx_Medicine_Response_Key  for xml path(''Route''),type
						 ) 
					 )
					  ,'''')
					  ,''['','''')
					  ,'']'','''')

				 
					  select  @PharmacyPhone= REPLACE(
					  REPLACE(
					  ISNULL( dbo.ACI_qfn_XmlToJson(	
					 (select top 1 
							  ISNULL(Type,'''') AS Type,ISNULL(Number,'''') AS Number 
								 from VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_PHARMACYPHONE (NOLOCK) where eRx_Medicine_Response_Key =@eRx_Medicine_Response_Key  for xml path(''phone1''),type
						 ) 
					 )
					  ,'''')
					  ,''['','''')
					  ,'']'','''')

					  select  @PharmacyAddress= REPLACE(
					  REPLACE(
					  ISNULL( dbo.ACI_qfn_XmlToJson(	
					 (select top 1 
							 ISNULL(Type,'''') AS Type,ISNULL(AddressLine1,'''') AS AddressLine1,ISNULL(AddressLine2,'''') AS AddressLine2,
											ISNULL(City,'''') AS City,ISNULL(State,'''') AS State,ISNULL(Zip,'''') AS Zip,ISNULL(County,'''') AS County							  
								 from VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION_PHARMACYADDRESS (NOLOCK) where eRx_Medicine_Response_Key =@eRx_Medicine_Response_Key  for xml path(''add1''),type
						 ) 
					 )
					  ,'''')
					  ,''['','''')
					  ,'']'','''')

                   
			       if @PharmacyPhone <>''''
				  set @PharmacyPhone ='',"PharmacyPhone":''+@PharmacyPhone

			       if @PharmacyAddress <>''''
				  set @PharmacyAddress ='',"PharmacyAddress":''+@PharmacyAddress

				 if @Route <>''''
				 begin
	    			  set @Medication =@Medication+'',"Route":''+@Route+@PharmacyPhone+@PharmacyAddress+''}''
				  if @STRINGJSON_MED ='''' 
				  set @STRINGJSON_MED=@Medication
				  else          
				    set @STRINGJSON_MED=@STRINGJSON_MED+'',''+@Medication
				   --r select @Procedures
				 end
         
		    FETCH NEXT FROM CURSORA_med INTO @PATIENTMEDICATIONSKEY
		  END
		  CLOSE CURSORA_med
		  DEALLOCATE CURSORA_med
	   end		
			
      if ltrim(rTrim(@STRINGJSON_MED)) <> ''''
	  begin
	  set @STRINGJSON_MEDICATION =@STRINGJSON_MEDICATION+'',"Medication": [''+@STRINGJSON_MED+'']}''
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,REFERENCENUMBER,Sort) 
                 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@STRINGJSON_MEDICATION))),@ReferenceNumberT,16)  
        
		select   @STRINGJSON_MEDICATION
		
		set @ACIJSONMasterKey=IDENT_CURRENT(''ACI_JSON_Master'')
		update ACI_InPutLog set status =''C'',statuschangedt=GETDATE() where status =''N'' and TableName =''JSON16_MEDICATIONSORDER'' 
		 and ( tableKey=@tableKey and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where status=''N'' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
  end
end

' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON17_SENTSUMMARYOFCAREEVENT')) 
Drop Procedure ACI_ConvertToJson_JSON17_SENTSUMMARYOFCAREEVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE PROC [dbo].[ACI_ConvertToJson_JSON17_SENTSUMMARYOFCAREEVENT] (@CCDAXMLFILEKEY VARCHAR(10),@ACIINPUTLOGKEY INT)
AS 
BEGIN
/*
ACI Version :Release 1.0
Date:28/08/2017
Developer:Ayush Mehta
Details/Description: To create JSON String in ACI_JSON_MAster table
*/
/*
EXEC ACI_CONVERTTOJSON_JSON17_SENTSUMMARYOFCAREEVENT 92219 ,292
select @CCDAXMLFILEKEY
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/
  DECLARE @STRINGJSON VARCHAR(8000)
  DECLARE @ACIINPUTLOGKEY1 INT ,@TABLEKEY INT, @ACIJSONMASTERKEY INT
  SET @ACIINPUTLOGKEY1 =0
  SET @TABLEKEY=0
  SET @ACIJSONMASTERKEY =0
  SET @TABLEKEY=cast(@CCDAXMLFILEKEY as int)
  SET @STRINGJSON=''''

	SELECT TOP 1 @ACIINPUTLOGKEY1=M.ACIINPUTLOGKEY   FROM ACI_JSON_MASTER M (NOLOCK)
	JOIN ACI_INPUTLOG C (NOLOCK) ON C.ACIINPUTLOGKEY=M.ACIINPUTLOGKEY AND C.STATUS=''N'' and  c.TableName = ''JSON17_SENTSUMMARYOFCAREEVENT'' 
	WHERE M.STATUS =''N'' AND C.ACIINPUTLOGKEY=@ACIINPUTLOGKEY ORDER BY M.ACIJSONMASTERKEY DESC

	IF @ACIINPUTLOGKEY1 =0
	BEGIN

		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@CHARTRECORDKEY1 INT ,@PatId INT
		set @PhyId =0
		set @FacId =0
		set @CHARTRECORDKEY1=0
		set @PatId =0

		--select @CHARTRECORDKEY1=ChartrecordKey from dbo.aci_dep (NOLOCK) where acidepkey =@CCDAXMLFILEKEY 

		select top 1 @PhyId=d.ProviderKey,@FacId=R.PracticeId ,@PatId=d.patientkey  
			from dbo.ACI_DEP d WITH(NOLOCK) join dbo.Resources r WITH(NOLOCK) on r.ResourceId = d.ProviderKey
			where d.AciDepKey = @TABLEKEY
		
		set @ReferenceNumber =''''
		select @ReferenceNumber = CAST(aci_vendorID as VARCHAR(10))+''.''+CAST(MDO_PracticeId as VARCHAR(20))+''.''+CAST(@FacId as varchar(10))+''.''+CAST(@PhyId as varchar(10))+''.''+CAST(@PatId as varchar(10))+''.17.'' from aci_json_Defaults (NOLOCK)

		Declare @ReferenceNumberT varchar(50)
		set @ReferenceNumberT =''''
		set @ReferenceNumberT =@ReferenceNumber+cast(@CCDAXMLFILEKEY as varchar(10))
		set @STRINGJSON=''''
	
			SELECT @STRINGJSON=@STRINGJSON+ isnull(
				REPLACE(REPLACE(
						DBO.ACI_QFN_XMLTOJSON (
						(     
						SELECT top 1
							 ISNULL(ResourceType,'''') AS ResourceType,
							 ISNULL(IsSource,'''') AS IsSource,
							 (isnull(@ReferenceNumberT,'''')) AS ReferenceNumber,
							 ISNULL(PatientAccountNumber,'''') AS PatientAccountNumber,
							 ISNULL(Date,'''') AS Date,
							 ISNULL(ECNPI,'''') AS ECNPI,
							 --ISNULL(ECTIN,'''') AS ECTIN,
							 (SUBSTRING(ECTIN,1,3)+''-''+SUBSTRING(ECTIN,4,2)+''-''+SUBSTRING(ECTIN,6,4))  as ECTIN,
							 ISNULL(SOCSentTo,'''') AS SOCSentTo,
							 ISNULL(OrderNumber,'''') AS OrderNumber
						FROM VIEW_ACI_JSON17_SENTSUMMARYOFCAREEVENT WHERE ACIDEPKEY =@TABLEKEY
						FOR XML PATH(''SENTSOCEVENT''),TYPE) 
				),''['','''') ,''}]'',''}'')  ,'''')

      IF LTRIM(RTRIM(@STRINGJSON)) <> ''''
	  BEGIN
         INSERT INTO ACI_JSON_MASTER (ACIINPUTLOGKEY,TABLEKEY,JSON,ReferenceNumber,Sort) 
                 VALUES(@ACIINPUTLOGKEY,@TABLEKEY,(LTRIM(RTRIM(@STRINGJSON))),@ReferenceNumberT,21)  

		SET @ACIJSONMASTERKEY=IDENT_CURRENT(''ACI_JSON_MASTER'')

		UPDATE ACI_INPUTLOG SET STATUS =''C'',STATUSCHANGEDT=GETDATE() WHERE STATUS =''N'' and TableName =''JSON17_SENTSUMMARYOFCAREEVENT'' 
		 AND( TABLEKEY=@TABLEKEY AND
		     TABLEKEY IN(SELECT TABLEKEY FROM ACI_JSON_MASTER (NOLOCK) WHERE STATUS=''N'' AND ACIJSONMASTERKEY=@ACIJSONMASTERKEY))
      END;
  END
END

' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON18_PATIENTSPECIFICEDUCATIONEVENT')) 
Drop Procedure ACI_ConvertToJson_JSON18_PATIENTSPECIFICEDUCATIONEVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE proc [dbo].[ACI_ConvertToJson_JSON18_PATIENTSPECIFICEDUCATIONEVENT] (@PATIENTEDUCATIONKEY INT,@aciinputlogkey int)
as 
begin
/*
ACI Version :Release 1.0
Date:05/05/2017
Developer:K.V.S.S.R.KRISHNA SARMA
Details/Description: To create JSON String in ACI_JSON_MAster table
EXEC ACI_ConvertToJson_JSON18_PATIENTSPECIFICEDUCATIONEVENT 93293,264
select * from ACI_INPUTLOG order by 1 desc

update dbo.patientclinical set appointmentid =6438, patientid =29912 where clinicalid = 8814
*/
/*
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/
  declare @StringJson varchar(8000)
  declare @aciinputlogkey1 int ,@tableKey INT, @ACIJSONMasterKey int
  set @aciinputlogkey1 =0
  set @tableKey=0
  set @ACIJSONMasterKey =0
  set @tableKey=@PATIENTEDUCATIONKEY
  set @StringJson=''''
  
	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status=''N'' and  c.TableName = ''JSON18_PATIENTSPECIFICEDUCATIONEVENT'' 
	where m.status =''N'' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 =0
	begin

		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@PatId INT
		set @PhyId =0
		set @FacId =0
		set @PatId =0
		
		select top 1 @PhyId=r.ResourceId,@FacId=r.PracticeId ,@PatId=pc.PatientId  
			FROM [MVE].[PP_PatientEducationMaterial] PC WITH(NOLOCK) LEFT JOIN DBO.RESOURCES R WITH(NOLOCK) ON R.RESOURCEID = PC.RESOURCEID
			WHERE pc.id = @PATIENTEDUCATIONKEY
		
		
		set @ReferenceNumber =''''
		select @ReferenceNumber = aci_vendorID+''.''+MDO_PracticeId+''.''+CAST(@FacId as varchar(10))+''.''+CAST(@PhyId as varchar(10))+''.''+CAST(@PatId as varchar(10))+''.18.'' from aci_json_Defaults (NOLOCK)

		Declare @ReferenceNumberT varchar(50)
		set @ReferenceNumberT =''''
		set @ReferenceNumberT =@ReferenceNumber+cast(@PATIENTEDUCATIONKEY as varchar(10))



			select @StringJson=@StringJson+ 
				isnull( REPLACE(REPLACE(
						dbo.aci_qfn_XmlToJson (
						(     
						select top 1 
							ISNULL(ResourceType,'''') AS ResourceType,
							ISNULL(IsSource,'''') AS IsSource,
							(isnull(@ReferenceNumber,'''')+CAST(ReferenceNumber AS VARCHAR(100))) AS ReferenceNumber,
							ISNULL(PatientAccountNumber,'''') AS PatientAccountNumber
						from VIEW_ACI_JSON18_PATIENTSPECIFICEDUCATIONEVENT L where ID =@PATIENTEDUCATIONKEY
						for xml path(''PatientSpecificEducationEvent1''),type) 
				),''['','''') ,''}]'','''') 
				,'''')

			if @StringJson <>'''' 	      
			select @StringJson=@StringJson+ '',"EducationMaterial":''+
				isnull( REPLACE(REPLACE(
						dbo.aci_qfn_XmlToJson (
						(     
						select 
							ISNULL(ExternalId,'''') AS ExternalId,
							ISNULL(ECNPI,'''') AS ECNPI,
							--ISNULL(TIN,'''') AS TIN,
							(SUBSTRING(TIN,1,3)+''-''+SUBSTRING(TIN,4,2)+''-''+SUBSTRING(TIN,6,4))  as TIN,
							ISNULL(CodeType,'''') AS CodeType,
							ISNULL(Code,'''') AS Code,
							ISNULL(DatePrinted,'''') AS DatePrinted,
							ISNULL(DateSentToPortal,'''') AS DateSentToPortal 
						from VIEW_ACI_JSON18_PATIENTSPECIFICEDUCATIONEVENT_EDUCATIONMATERIAL L where ID=@PATIENTEDUCATIONKEY
						for xml path(''EducationMaterial1''),type) 
				),''['',''['') ,''}]'',''}]}'')       
                  ,'''')

		  if ltrim(rTrim(@StringJson)) <> ''''
		  begin
			 insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,ReferenceNumber,Sort) 
					 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))),@ReferenceNumberT,21)  

			set @ACIJSONMasterKey=IDENT_CURRENT(''ACI_JSON_Master'')
			update ACI_InPutLog set status =''C'',statuschangedt=GETDATE() where status =''N'' and TableName =''JSON18_PATIENTSPECIFICEDUCATIONEVENT'' 
			and( tableKey=@tableKey and
				 tablekey in(select tablekey from ACI_JSON_Master (nolock) where status=''N'' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
  end
end

' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT')) 
Drop Procedure ACI_ConvertToJson_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE proc [dbo].[ACI_ConvertToJson_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT] (@CCDAXMLFileKey INT,@aciinputlogkey int)
as 
begin
/*
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/
  declare @StringJson varchar(8000)
  declare @aciinputlogkey1 int ,@tableKey INT, @ACIJSONMasterKey int ,@ProviderKey int
  set @aciinputlogkey1 =0
  set @tableKey=0
  set @ACIJSONMasterKey =0
  set @tableKey=@CCDAXMLFileKey
  set @StringJson=''''
  

	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status=''N'' and TableName = ''JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT'' 
	where m.status =''N'' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc
	
	if @aciinputlogkey1 =0
	begin


		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@CHARTRECORDKEY1 INT ,@PatId INT
		set @PhyId =0
		set @FacId =0		
		set @PatId =0

		select @CHARTRECORDKEY1=PRPI.PatientId from  Model.PatientReconcilePracticeInformation PRPI WITH(NOLOCK)
		LEFT JOIN Model.Patients P WITH(NOLOCK) ON PRPI.PatientId = P.Id
		LEFT JOIN DBO.RESOURCES R WITH(NOLOCK) ON P.DEFAULTUSERID = R.RESOURCEID
		WHERE PRPI.Id =@CCDAXMLFileKey
 

		select top 1 @PhyId=R.ResourceId,@FacId=R.PracticeId ,@PatId=P.Id  from Model.Patients P WITH(NOLOCK)
		LEFT JOIN DBO.RESOURCES R (NOLOCK) ON P.DEFAULTUSERID = R.RESOURCEID
		where P.Id =@CHARTRECORDKEY1 order by P.Id desc

		--select top 1 @PhyId=cc.PROVIDERKEY,@FacId=OFFICELOCATIONKEY ,@PatId=cc.PATIENTKEY  from 
		--CCDAXMLFILES cc (NOLOCK)
		--join patient p (NOLOCK) on p.patientkey =cc.patientkey
		--where CCDAXMLFileKey =@CCDAXMLFileKey	 
		
		set @ReferenceNumber =''''
		select @ReferenceNumber = aci_vendorID+''.''+MDO_PracticeId+''.''+CAST(@FacId as varchar(10))+''.''+CAST(@PhyId as varchar(10))+''.''+CAST(@PatId as varchar(10))+''.19.'' from aci_json_Defaults (NOLOCK)

		Declare @ReferenceNumberT varchar(50)
		set @ReferenceNumberT =''''
		set @ReferenceNumberT =@ReferenceNumber+cast(@CCDAXMLFileKey as varchar(10))


	    set @ProviderKey =0
	    --select @ProviderKey =  from CCDAXMLFiles (nolock) where  CCDAXMLFileKey =@CCDAXMLFileKey	

        select @StringJson=@StringJson+isnull( 
            REPLACE(REPLACE(
					dbo.aci_qfn_XmlToJson (
					(     
					select 
						 ISNULL(ResourceType,'''') AS ResourceType,
						 ISNULL(IsSource,'''') AS IsSource,
						 CAST(isnull(@ReferenceNumber,'''')AS VARCHAR(70))+CAST(ISNULL(ReferenceNumber,'''') AS VARCHAR(50)) AS ReferenceNumber,
						 ISNULL(PatientAccountNumber,'''') AS PatientAccountNumber,
						 ISNULL(Date,'''') AS Date,
						 ISNULL(ECNPI,'''') AS ECNPI,
						 --ISNULL(ECTIN,'''') AS ECTIN,
						 (SUBSTRING(ECTIN,1,3)+''-''+SUBSTRING(ECTIN,4,2)+''-''+SUBSTRING(ECTIN,6,4))  as ECTIN,
						 ISNULL(ExternalId,'''') AS ExternalId,
						 ISNULL(EventType,'''') AS EventType,
						 ISNULL(IsSocReceived,'''') AS IsSocReceived,
						 ISNULL(Description,'''') AS Description
					from VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT where Id =@CCDAXMLFileKey	
					for xml path(''REFERRALTOCNEWPATIENTEVENT1''),type) 
            ),''['','''') ,''}]'','''') 
			,'''')

			select @StringJson=@StringJson+'',"Attribute":''+ isnull( dbo.aci_qfn_XmlToJson (
			(
			select 
				ISNULL(ExternalId,'''') AS ExternalId,
				ISNULL(AttributeType,'''') AS AttributeType,
				ISNULL(AttributeSubType,'''') AS AttributeSubType,
				ISNULL(Date,'''') AS Date,
				ISNULL(AttributeName,'''') AS AttributeName		
			   from VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_ATTRIBUTE  where Id =@CCDAXMLFileKey	
			   for xml path(''Attribute1''),type ) )   
			   ,'''')
declare @iProvCount int
set @iProvCount=0
        if (select Count(*) from VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE) >0
		begin
		set @iProvCount =1
            select @StringJson=@StringJson+ '',"OutsidePractice":''+
           isnull( REPLACE(REPLACE(
					dbo.aci_qfn_XmlToJson (
					(     
					select
						ISNULL(PracticeName,'''') AS PracticeName,
						ISNULL(AccountNumber,'''') AS AccountNumber,
						ISNULL(FamilyName,'''') AS FamilyName,
						ISNULL(GivenName,'''') AS GivenName,
						ISNULL(SecondorFutherGivenNames,'''') AS SecondorFutherGivenNames,
						ISNULL(DateOfBirth,'''') AS DateOfBirth
					from VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE L where l.Id=@CCDAXMLFileKey 
					for xml path(''OutsidePractice1''),type) 
            ),''['','''') ,''}]'','''') 
			,'''')      


			if (select count(*) from VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE_PHONE p  where  isnull(number,'''')<>'''' )>0 
			select @StringJson=@StringJson+'',"Phone":''+ isnull( dbo.aci_qfn_XmlToJson (
			(
			select 
				 ISNULL(Type,'''') AS Type,
				 ISNULL(Number,'''') AS Number
			   from VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE_PHONE p  where p.Id=@CCDAXMLFileKey 
			   for xml path(''Phone1''),type ) )   
			   ,'''')


        if (select count(*) from VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE_ADDRESS  where ( isnull(ADDRESSLINE1,'''') <>'''' or isnull(CITY,'''') <>'''') )>0  
        select @StringJson=@StringJson+ '',"Address":''+
         isnull(   REPLACE(REPLACE(
					dbo.aci_qfn_XmlToJson (
					(     
					select
						 ISNULL(AddressLine1,'''') AS AddressLine1,
						 ISNULL(AddressLine2,'''') AS AddressLine2,
						 ISNULL(City,'''') AS City,
						 ISNULL(State,'''') AS State,
						 ISNULL(Zip,'''') AS Zip,
						 ISNULL(County,'''') AS County,
						 ISNULL(CountyCode,'''') AS CountyCode
					from VIEW_ACI_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT_OUTSIDEPRACTICE_ADDRESS L where l.Id=@CCDAXMLFileKey 
					for xml path(''Address1''),type) 
            ),''['','''') ,'']'','''')  
			,'''')    
			end

    if @iProvCount=1
	   set @StringJson=@StringJson+''}}''
    else
	   set @StringJson=@StringJson+''}''


	--select @StringJson
      if ltrim(rTrim(@StringJson)) <> ''''
	  begin
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,ReferenceNumber,Sort) 
                 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))),@ReferenceNumberT,19)  

		set @ACIJSONMasterKey=IDENT_CURRENT(''ACI_JSON_Master'')
		update ACI_InPutLog set status =''C'',statuschangedt=GETDATE() where status =''N''  and TableName = ''JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT'' 
		and( tableKey=@tableKey and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where status=''N'' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
  end
end
' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT')) 
Drop Procedure ACI_ConvertToJson_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE proc [dbo].[ACI_ConvertToJson_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT] (@CCDAXMLFileKey INT,@aciinputlogkey int)
as 
begin
/*
ACI Version :Release 1.0
Date:05/05/2017
Developer:ASHISH VIRMANI
Details/Description: To create JSON String in ACI_JSON_MAster table
SELECT * FROM mve.PP_PORTALqUEUERESPONSE
*/
/*
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/ 
  declare @StringJson varchar(8000)
  declare @aciinputlogkey1 int ,@tableKey VARCHAR(20), @ACIJSONMasterKey int
  set @aciinputlogkey1 =0
  set @tableKey=''''
  set @ACIJSONMasterKey =0
  set @tableKey=@CCDAXMLFileKey
  set @StringJson=''''

	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status=''N''  and  c.TableName = ''JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT'' 
	where m.status =''N'' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 =0
	begin
		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@PatId INT--, @CHARTRECORDKEY1 int 
		set @PhyId =0
		set @FacId =0
		set @PatId =0

		--select @CHARTRECORDKEY1=ChartRecordKey from CCDAXMLFILES (NOLOCK) where CCDAXMLFILEKEY=@CCDAXMLFILEKEY 
		
			SELECT TOP 1 @PhyId = A.ResourceId1, @PatId = A.PatientId, @FacId = R.PracticeId  
			FROM dbo.Appointments A(NOLOCK) LEFT JOIN dbo.Resources R on R.ResourceId = A.ResourceId1
            WHERE A.appointmentid = @CCDAXMLFileKey AND A.ResourceId1 <> 0 AND A.APPTYPEID <> 0 AND A.SCHEDULESTATUS = ''D'' AND A.ACTIVITYSTATUS = ''D'' ORDER BY A.APPDATE DESC

		  --SELECT TOP 10 * FROM dbo.appointments
		--select top 1 @PhyId=cc.PROVIDERKEY,@FacId=OFFICELOCATIONKEY ,@PatId=cc.PATIENTKEY  from 
		--CCDAXMLFILES cc (NOLOCK)
		--join patient p (NOLOCK) on p.patientkey =cc.patientkey
		--where CCDAXMLFileKey =@CCDAXMLFileKey	 
		
		set @ReferenceNumber =''''
		select @ReferenceNumber = aci_vendorID+''.''+MDO_PracticeId+''.''+CAST(@FacId as varchar(10))+''.''+CAST(@PhyId as varchar(10))+''.''+CAST(@PatId as varchar(10))+''.20.'' from aci_json_Defaults (NOLOCK)

		Declare @ReferenceNumberT varchar(50)
		set @ReferenceNumberT =''''
		set @ReferenceNumberT =@ReferenceNumber+cast(@CCDAXMLFileKey as varchar(10))


        select @StringJson=@StringJson+ 
            REPLACE(REPLACE(
					dbo.aci_qfn_XmlToJson (
					(     
					select 
						 ISNULL(ResourceType,'''') AS ResourceType,
						 ISNULL(IsSource,'''') AS IsSource,
						 isnull(@ReferenceNumber,'''')+ISNULL(ReferenceNumber,'''') AS ReferenceNumber,
						 ISNULL(PatientAccountNumber,'''') AS PatientAccountNumber,
						 ISNULL(PostedDate,'''') AS PostedDate
					from VIEW_ACI_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT L where l.APPOINTMENTID =@CCDAXMLFileKey	  for xml path(''pat''),type) 
            ),''['','''') ,''}]'',''}'')       

			--select @StringJson

      if ltrim(rTrim(@StringJson)) <> ''''
	  begin
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,ReferenceNumber,Sort) 
                 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))),@ReferenceNumberT,20)  

		set @ACIJSONMasterKey=IDENT_CURRENT(''ACI_JSON_Master'')
		update ACI_InPutLog set status =''C'',statuschangedt=GETDATE() where status =''N''  and  TableName = ''JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT''  
		and( tableKey=@tableKey and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where status=''N'' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
  end
end


--SELECT * FROM ACI_INPUTLOG ORDER BY 1 DESC
' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON21_CARECOORDINATIONORDER')) 
Drop Procedure ACI_ConvertToJson_JSON21_CARECOORDINATIONORDER 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE proc [dbo].[ACI_ConvertToJson_JSON21_CARECOORDINATIONORDER] (@PATIENTORDERKey INT,@aciinputlogkey int)
as 
begin
/*
ACI Version :Release 1.0
Date:05/05/2017
Developer:K.V.S.S.R.KRISHNA SARMA
Details/Description: To create JSON String in ACI_JSON_MAster table
*/
/*
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/
  declare @StringJson varchar(8000)
  declare @aciinputlogkey1 int ,@tableKey INT, @ACIJSONMasterKey int,@ProviderKey int
  set @aciinputlogkey1 =0
  set @tableKey=0
  set @ACIJSONMasterKey =0
  set @tableKey=@PATIENTORDERKey
  set @StringJson=''''

	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status=''N'' and c.TableName =''JSON21_CARECOORDINATIONORDER'' 
	where m.status =''N'' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 =0
	begin
		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@CHARTRECORDKEY1 INT ,@PatId INT,@ProvKey int
		set @PhyId =0
		set @FacId =0
		set @CHARTRECORDKEY1=0
		set @PatId =0
		set @ProvKey =0

		select @CHARTRECORDKEY1=ChartrecordKey from dbo.aci_dep (NOLOCK) where acidepkey =@Patientorderkey 

	   	set @ProviderKey =0
	    select @ProviderKey =PtOutboundReferralKey from dbo.aci_dep (NOLOCK) where acidepkey =@Patientorderkey 

		
	Select top 1  @PhyId=d.ProviderKey,@FacId=R.PracticeId ,@PatId=d.patientkey  
			from dbo.ACI_DEP d WITH(NOLOCK) join dbo.Resources r WITH(NOLOCK) on r.ResourceId = d.ProviderKey
			where d.AciDepKey = @PATIENTORDERKEY


		--select top 1 @ProvKey=c.PROVIDERKEY,@FacId=s.OFFICELOCATIONKEY ,@PatId=s.PATIENTKEY  from  c (nolock)
		--join SCHEDULE s (nolock) on s.SCHEDULEKEY  =c.SCHEDULEKEY
		--where c.CHARTRECORDKEY =@CHARTRECORDKEY1 order by c.CHARTRECORDKEY desc
		
		--if @ProviderKey =0
		--   set @PhyId=@ProvKey
  --        else set @PhyId=@ProviderKey

		set @ReferenceNumber =''''
		select @ReferenceNumber = aci_vendorID+''.''+MDO_PracticeId+''.''+CAST(@FacId as varchar(10))+''.''+CAST(@PhyId as varchar(10))+''.''+CAST(@PatId as varchar(10))+''.21.'' from aci_json_Defaults (NOLOCK)


	 Declare @ReferenceNumberT varchar(50)
	 set @ReferenceNumberT =''''
	 set @ReferenceNumberT =@ReferenceNumber+cast(@PATIENTORDERKey as varchar(10))

        select @StringJson=@StringJson+ 
            isnull(REPLACE(REPLACE(
					dbo.aci_qfn_XmlToJson (
					(     
					select 
						 ISNULL(ResourceType,'''') AS ResourceType,
						 ISNULL(@ReferenceNumberT,'''') AS ReferenceNumber,
						 ISNULL(Source,'''') AS Source,
						 ISNULL(ExternalId,'''') AS ExternalId,
						 ISNULL(Description,'''') AS Description,
						 ISNULL(PatientAccountNumber,'''') AS PatientAccountNumber,
						 ISNULL(Date,'''') AS Date,
						 ISNULL(OrderType,'''') AS OrderType,
						 ISNULL(OrderingECNPI,'''') AS OrderingECNPI,
						 --ISNULL(OrderingECTIN,'''') AS OrderingECTIN,
						 (SUBSTRING(OrderingECTIN,1,3)+''-''+SUBSTRING(OrderingECTIN,4,2)+''-''+SUBSTRING(OrderingECTIN,6,4)) as OrderingECTIN,
						 ISNULL(Status,'''') AS Status
						,ISNULL(ReasonforReferral,'''') AS ReasonforReferral
					from VIEW_ACI_JSON21_CARECOORDINATIONORDER L where l.AciDepKey = @tableKey 
					for xml path(''CareCoordinationOrder1''),type) 
            ),''['','''') ,''}]'','''')    
		  ,'''')   

        select @StringJson=@StringJson+ '',"OutsidePhysician":''+
           isnull( REPLACE(REPLACE(
					dbo.aci_qfn_XmlToJson (
					(     
					select
						 ISNULL(AccountNumber,'''') AS AccountNumber,
						 ISNULL(GivenName,'''') AS GivenName,
						 ISNULL(FamilyName,'''') AS FamilyName,
						 ISNULL(SecondandFurtherGivenNames,'''') AS SecondandFurtherGivenNames,
						 ISNULL(DateOfBirth,'''') AS DateOfBirth,
						 ISNULL(NamePrefix,'''') AS NamePrefix,
						 ISNULL(NameSuffix,'''') AS NameSuffix
					from VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN L where l.AciDepKey = @tableKey
					for xml path(''OutsidePhysician1''),type) 
            ),''['','''') ,''}]'','''')
		  ,'''')       

           if (select Count(*) from VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN_PHONE p  where isnull(p.NUMBER,'''') <> '''' and  p.AccountNumber=@PhyId ) >0
			select @StringJson=@StringJson+'',"Phone":''+  isnUll(dbo.aci_qfn_XmlToJson (
			(
			select 
				 ISNULL(Type,'''') AS Type,
				 ISNULL(Number,'''') AS Number
			   from VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN_PHONE p  where p.AciDepKey = @tableKey
			   for xml path(''Phone1''),type ) )   ,'''')



        select @StringJson=@StringJson+ '',"Address":''+
            isnull(REPLACE(REPLACE(
					dbo.aci_qfn_XmlToJson (
					(     
					select
						 ISNULL(Type,'''') AS Type,
						 ISNULL(AddressLine1,'''') AS AddressLine1,
						 ISNULL(AddressLine2,'''') AS AddressLine2,
						 ISNULL(City,'''') AS City,
						 ISNULL(State,'''') AS State,
						 ISNULL(Zip,'''') AS Zip
					from VIEW_ACI_JSON21_CARECOORDINATIONORDER_OUTSIDEPHYSICIAN_ADDRESS L where l.AciDepKey = @tableKey
					for xml path(''Address1''),type) 
            ),''['','''') ,'']'','''')    
		  ,'''')   

	set @StringJson=@StringJson+''}}''


      if ltrim(rTrim(@StringJson)) <> ''''
	  begin
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,ReferenceNumber,Sort) 
                 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))),@ReferenceNumberT,17)  

		set @ACIJSONMasterKey=IDENT_CURRENT(''ACI_JSON_Master'')
		update ACI_InPutLog set status =''C'',statuschangedt=GETDATE() where status =''N'' and TableName =''JSON21_CARECOORDINATIONORDER'' 
		and( tableKey=@tableKey and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where status=''N'' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
  end
end
' 

Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT')) 
Drop Procedure ACI_ConvertToJson_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE PROC [dbo].[ACI_ConvertToJson_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT] (@PtOutboundReferralKey INT,@ACIINPUTLOGKEY INT)
AS 
BEGIN
/*
ACI VERSION :RELEASE 1.1
DATE:08/22/2017
Developer/Author:Ayush Mehta
DETAILS/DESCRIPTION:ACI INTEGRATION RELEASE1.1 - "JSON22–SPECIALIST REPORT RECEIVED EVENT"
,TO CREATE JSON STRING IN ACI_JSON_MASTER TABLE
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/
/*                                                      ************************SCRIPT CASE SENSITIVE************  */

/*
 EXEC ACI_CONVERTTOJSON_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT 15,2
  SELECT * FROM ACI_JSON_MASTER ORDER BY 1 DESC
*/ 
  DECLARE @STRINGJSON VARCHAR(8000)
  DECLARE @ACIINPUTLOGKEY1 INT ,@TABLEKEY INT, @ACIJSONMASTERKEY INT
  SET @ACIINPUTLOGKEY1 =0
  SET @TABLEKEY=0
  SET @ACIJSONMASTERKEY =0
  SET @TABLEKEY=@PtOutboundReferralKey
  SET @STRINGJSON=''''


    SELECT TOP 1 @ACIINPUTLOGKEY1 = M.ACIINPUTLOGKEY
    FROM ACI_JSON_MASTER M(NOLOCK)
    JOIN ACI_INPUTLOG C(NOLOCK) ON C.ACIINPUTLOGKEY = M.ACIINPUTLOGKEY
            AND C.STATUS = ''N''
            AND C.TABLENAME = ''JSON22_SPECIALISTREPORTRECEIVEDEVENT''
    WHERE M.STATUS = ''N''
    and C.STATUS = ''N'' AND C.TABLENAME = ''JSON22_SPECIALISTREPORTRECEIVEDEVENT''
    AND C.ACIINPUTLOGKEY = @ACIINPUTLOGKEY
    ORDER BY M.ACIJSONMASTERKEY DESC;

 IF @ACIINPUTLOGKEY1 =0
 BEGIN
    DECLARE @REFERENCENUMBER VARCHAR(50),@PHYID INT , @FACID INT,@PATID INT,@ID INTEGER ,@OutboundReferralKey INT,@OutboundProviderKey INT,@ECNPI varchar(12),@ECTIN varchar(12)
    ,@CRProviderKey INT

    SET @PHYID =0
    SET @FACID =0
    SET @PATID =0
    SET @ID =0
    set @ECNPI =''''
    set @ECTIN=''''
    set @CRProviderKey=0
		select top 1 --@PhyId=Ap.ResourceId1,
@FacId=R.PracticeId ,@PatId=Ap.PatientId,@CRProviderKey = Ap.ResourceId1 from dbo.PatientClinical Pc (nolock)
			join dbo.Appointments Ap (nolock) on Ap.AppointmentId = pc.appointmentid
			join dbo.Resources R (nolock) on R.ResourceId  =Ap.ResourceId1
		where pc.clinicalid= @PtOutboundReferralKey order by pc.clinicalid desc;

    select @OutboundReferralKey=CLINICALID,@OutboundProviderKey=ResourceId from VIEW_ACI_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT where CLINICALID=@PtOutboundReferralKey

    SET @REFERENCENUMBER = '''';
    if @OutboundReferralKey >0
    begin
        SELECT @REFERENCENUMBER = ACI_VENDORID+''.''+MDO_PRACTICEID+''.''+CAST(@FACID AS VARCHAR(10))+''.''+CAST(@CRProviderKey AS VARCHAR(10))+''.''+CAST(@PATID AS VARCHAR(10))+''.22.''
        FROM ACI_JSON_DEFAULTS(NOLOCK)
    select @ECTIN=RIGHT(''000000000''+CONVERT(VARCHAR, SUBSTRING(isnull(resourcetaxid, ''''), 1, 9)), 9) ,
           @ECNPI=RIGHT(''0000000000''+CONVERT(VARCHAR, SUBSTRING(isnull( npi, ''''), 1, 10)), 10)
      from dbo.Resources where ResourceId =@CRProviderKey

        end
        else
    begin
        SELECT @REFERENCENUMBER = ACI_VENDORID+''.''+MDO_PRACTICEID+''.''+CAST(@FACID AS VARCHAR(10))+''.''+CAST(@OutboundProviderKey AS VARCHAR(10))+''.''+CAST(@PATID AS VARCHAR(10))+''.22.''
        FROM ACI_JSON_DEFAULTS(NOLOCK)
    select @ECTIN=RIGHT(''000000000''+CONVERT(VARCHAR, SUBSTRING(isnull(resourcetaxid, ''''), 1, 9)), 9) ,
           @ECNPI=RIGHT(''0000000000''+CONVERT(VARCHAR, SUBSTRING(isnull( npi, ''''), 1, 10)), 10)
      from dbo.Resources where ResourceId =@OutboundProviderKey
        end
       

    DECLARE @REFERENCENUMBERT VARCHAR(50);
    SET @REFERENCENUMBERT = '''';
    SET @REFERENCENUMBERT = @REFERENCENUMBER + CAST(@PtOutboundReferralKey AS VARCHAR(10));

    IF
    (
    SELECT COUNT(*)
    FROM VIEW_ACI_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT where CLINICALID = @PtOutboundReferralKey
    ) > 0
    SELECT @StringJson = @StringJson+isnull(REPLACE(REPLACE(dbo.aci_qfn_XmlToJson
               (
               (
               SELECT TOP 1 ISNULL(ResourceType, '''') AS ResourceType,
                    ISNULL(IsSource, '''') AS IsSource,
                    (@ReferenceNumber+CAST(ReferenceNumber as varchar(30))) AS ReferenceNumber,
                    ISNULL(PatientAccountNumber, '''') AS PatientAccountNumber,
                    ISNULL(DateReceived, '''') AS DateReceived,
                    ISNULL(@ECNPI, '''') AS ECNPI,
                    (SUBSTRING(@ECTIN, 1, 3)+''-''+SUBSTRING(@ECTIN, 4, 2)+''-''+SUBSTRING(@ECTIN, 6, 4)) AS ECTIN,
                    ISNULL(ExternalId, '''') AS ExternalId,
                    ISNULL(ReferralOrderNumber, '''') AS ReferralOrderNumber
               FROM VIEW_ACI_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT L
               WHERE L.ClinicalId = @PtOutboundReferralKey FOR XML PATH(''SPECIALIST''), TYPE
               )
               ), ''['', ''''), ''}]'', ''}''), '''');   

      IF LTRIM(RTRIM(@STRINGJSON)) <> ''''
   BEGIN
   --PRINT   @STRINGJSON
         INSERT INTO ACI_JSON_MASTER (ACIINPUTLOGKEY,TABLEKEY,JSON,REFERENCENUMBER,SORT) 
                 VALUES(@ACIINPUTLOGKEY,@TABLEKEY,(LTRIM(RTRIM(@STRINGJSON))),@REFERENCENUMBERT,22)  
    --PRINT  @STRINGJSON
  SET @ACIJSONMASTERKEY=IDENT_CURRENT(''ACI_JSON_MASTER'')
  UPDATE ACI_INPUTLOG SET STATUS =''C'',STATUSCHANGEDT=GETDATE() WHERE STATUS =''N'' AND  TABLENAME = ''JSON22_SPECIALISTREPORTRECEIVEDEVENT''  
  AND( TABLEKEY=@TABLEKEY AND aciinputlogkey IN(SELECT aciinputlogkey FROM ACI_JSON_MASTER (NOLOCK) WHERE STATUS=''N'' AND ACIJSONMASTERKEY=@ACIJSONMASTERKEY))
      END
  END
END


'
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT')) 
Drop Procedure ACI_ConvertToJson_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE proc[dbo].[ACI_ConvertToJson_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT](@PATIENTCATARACTPLANKEY INT, @aciinputlogkey int)
as
begin

    declare @StringJson_24 varchar(Max)
    declare @aciinputlogkey1 int, @tableKey INT, @ACIJSONMasterKey int,@COLLECTEDBILLINGKEY int
    set @COLLECTEDBILLINGKEY=0
    set @aciinputlogkey1 = 0
    set @tableKey = 0
    set @ACIJSONMasterKey = 0
    set @tableKey = @PATIENTCATARACTPLANKEY
    set @StringJson_24 = ''

    select top 1 @aciinputlogkey1 = m.aciinputlogkey from ACI_JSON_Master m(nolock)
    join ACI_InPutLog c (nolock) on c.aciinputlogkey = m.aciinputlogkey and c.status = 'N' and  c.TableName = 'JSON24_SURGICALPREOPERATIVESTATUSEVENT'
    where m.status = 'N' and c.status = 'N' and  c.TableName = 'JSON24_SURGICALPREOPERATIVESTATUSEVENT'
    and c.aciinputlogkey = @ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 = 0
	begin

		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@CHARTRECORDKEY1 INT ,@PatId INT
		 ,@Procedures varchar(8000),@ExamField varchar(8000),@Diagnosis Varchar(5000),@Medication Varchar(5000),@Reason Varchar(5000)
		set @PhyId =0
		set @FacId =0
		set @CHARTRECORDKEY1=0
		set @PatId =0

		Set @Procedures=''
		set @ExamField =''
		set @Diagnosis =''
		set @Medication=''

		select top 1 @PhyId=Ap.ResourceId1,@FacId=R.PracticeId ,@PatId=Ap.PatientId  from dbo.PatientClinical Pc (nolock)
			join dbo.Appointments Ap (nolock) on Ap.AppointmentId = pc.appointmentid
			join dbo.Resources R (nolock) on R.ResourceId  =Ap.ResourceId1
		where pc.appointmentid= @PATIENTCATARACTPLANKEY order by pc.appointmentid desc;
		
		set @ReferenceNumber =''
		select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.'+CAST(@FacId as varchar(10))+'.'+CAST(@PhyId as varchar(10))+'.'+CAST(@PatId as varchar(10))+'.24.' from aci_json_Defaults (NOLOCK)

		Declare @ReferenceNumberT varchar(50),@LabRadiologyOrder varchar(5000),@OrderCode varchar(5000)
		set @ReferenceNumberT =''
		set @LabRadiologyOrder =''
		set @OrderCode =''
		set @ReferenceNumberT =@ReferenceNumber+cast(@PATIENTCATARACTPLANKEY as varchar(10))

			IF (SELECT COUNT(*) FROM VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT where Appointmentid =@PATIENTCATARACTPLANKEY)>0
				SELECT @StringJson_24= REPLACE(REPLACE( DBO.ACI_QFN_XMLTOJSON (
				(
				SELECT top 1	
						   ISNULL(ResourceType,'') AS ResourceType,
						   ISNULL(IsSource,'') AS IsSource,
						   (isnull(@ReferenceNumber,'')+ Cast(ReferenceNumber as varchar(20) )) AS ReferenceNumber,
						   ISNULL(PatientAccountNumber,'') AS PatientAccountNumber,
						   ISNULL(DateofPreOpExam,'') AS DateofPreOpExam,
						   ISNULL(ECNPI,'') AS ECNPI,
						   (SUBSTRING(ECTIN,1,3)+'-'+SUBSTRING(ECTIN,4,2)+'-'+SUBSTRING(ECTIN,6,4)) AS ECTIN,
						   ISNULL(ExternalId,'') AS ExternalId,
						   ISNULL(EncounterId,'') AS EncounterId,
						   ISNULL(EpisodeNumber,'') AS EpisodeNumber,
						   ISNULL(ReferralOrderNumber,'') AS ReferralOrderNumber,
						   ISNULL(IsInHousePhysician,'') AS IsInHousePhysician,
						   ISNULL(PerformedPhysicianNPI,'') AS PerformedPhysicianNPI,
						   ISNULL(PerformedPhysicianAccountNumber,'') AS PerformedPhysicianAccountNumber,
						   ISNULL(PerformedPhysicianName,'') AS PerformedPhysicianName
				  from VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT where AppointmentId =@PATIENTCATARACTPLANKEY FOR XML PATH('Level_1'),TYPE ) ) ,'[','') ,'}]','')     
 
		   if ltrim(rTrim(@StringJson_24)) <> ''
		   begin
			  DECLARE CURSORA_24 SCROLL CURSOR FOR 
				select SP.Id from VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure SP where SP.AppointmentId = @PATIENTCATARACTPLANKEY 
			  OPEN CURSORA_24   
			  FETCH FIRST FROM CURSORA_24 INTO @COLLECTEDBILLINGKEY
			  WHILE @@FETCH_STATUS=0
			  BEGIN
	 
				  set @ExamField =''
				  set @Diagnosis =''
				  set @Medication=''
				  set @Reason =''

				 select  @Procedures= replace(--'}'
				 replace(--']'
				 replace --'['
				  (ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select top 1 
								 ISNULL(Code,'') AS Code,
								 ISNULL(Description,'') AS Description,
								 ISNULL(CodeSystemName,'') AS CodeSystemName,
								 ISNULL(CodeSystem,'') AS CodeSystem								
								 from VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure SP (NOLOCK) where SP.ID = @COLLECTEDBILLINGKEY for xml path('Level_2'),type
							 ) 
						 )
					 ,'') 
					 ,'[','')
					 ,']','')
					 ,'}','')
 
					 --select @Route
					 if @Procedures<>''
						  select  @ExamField=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Type,'') AS Type,
							  ISNULL(Code,'') AS Code,
							  ISNULL(Description,'') AS Description,
							  ISNULL(CodeSystem,'') AS CodeSystem,
							  ISNULL(CodeSystemName,'') AS CodeSystemName
									 from VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_ExamField (NOLOCK) where   AppointmentId = @PATIENTCATARACTPLANKEY  for xml path('Level_2'),type
							 ) 
						 )
						  ,'')
 
					 if @Procedures<>''
						  select  @Diagnosis=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Code,'') AS Code,
							 ISNULL(Description,'') AS Description,
							 ISNULL(CodeSystem,'') AS CodeSystem,
							 ISNULL(CodeSystemName,'') AS CodeSystemName
						  from VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Diagnosis (NOLOCK) where   AppointmentId = @PATIENTCATARACTPLANKEY  for xml path('Level_2'),type
							 ) 
						 )
						  ,'')
 
					 if @Procedures<>''
						  select  @Medication=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Code,'') AS Code,
							 ISNULL(Description,'') AS Description,
							 ISNULL(CodeSystem,'') AS CodeSystem,
							 ISNULL(CodeSystemName,'') AS CodeSystemName
									 from VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Medication (NOLOCK) where   AppointmentId = @PATIENTCATARACTPLANKEY  for xml path('Level_2'),type
							 ) 
						 )
						  ,'')

 
					 if @Procedures<>''
						  select  @Reason=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
						      ISNULL(Type,'') AS Type,
							 ISNULL(Code,'') AS Code,
							 ISNULL(Description,'') AS Description,
							 ISNULL(CodeSystem,'') AS CodeSystem,
							 ISNULL(CodeSystemName,'') AS CodeSystemName
									 from VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Reason (NOLOCK) where   AppointmentId = @PATIENTCATARACTPLANKEY  for xml path('Level_2'),type
							 ) 
						 )
						  ,'')

                      if @ExamField <>'' 
				    set @ExamField=',"ExamField":'+@ExamField
                      if @Diagnosis <>'' 
				    set @Diagnosis=',"Diagnosis":'+@Diagnosis
                      if @Medication <>'' 
				    set @Medication=',"Medication":'+@Medication
                      if @Reason <>'' 
				    set @Reason=',"Reason":'+@Reason

				  if @Procedures  <>''
				    set @Procedures =@Procedures+@ExamField+@Diagnosis+@Medication+@Reason+'}'
				  set @ExamField =''
				  set @Diagnosis =''
				  set @Medication=''
				  set @Reason =''
				--	select @Procedures
				FETCH NEXT FROM CURSORA_24 INTO @COLLECTEDBILLINGKEY
			  END
			  CLOSE CURSORA_24
			  DEALLOCATE CURSORA_24
		   end	
		   --select @Procedures
		  
			if ltrim(rTrim(@StringJson_24)) <> ''
			begin
			  set @StringJson_24 =@StringJson_24+',"Procedure": ['+@Procedures+']}'
			  
			  insert into ACI_JSON_Master(ACIInPutLogKey, tablekey, json,ReferenceNumber,Sort)
			                values(@aciinputlogkey, @tableKey, (ltrim(rTrim(@StringJson_24))),@ReferenceNumberT,24)

			set @ACIJSONMasterKey = IDENT_CURRENT('ACI_JSON_Master')
		   	update ACI_InPutLog set status = 'C', statuschangedt = GETDATE() where status = 'N' and TableName ='JSON24_SURGICALPREOPERATIVESTATUSEVENT' 
			and(tableKey = @tableKey and tablekey in (select tablekey from ACI_JSON_Master(nolock) where status = 'N'
				and ACIJSONMasterKey = @ACIJSONMasterKey))
				-- select @StringJson_24			
			end
	end
end
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON25_SurgeryEvent')) 
Drop Procedure ACI_ConvertToJson_JSON25_SurgeryEvent 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE proc[dbo].[ACI_ConvertToJson_JSON25_SurgeryEvent](@PATIENTSURGERYKEY INT, @aciinputlogkey int)
as
begin

    declare @StringJson_25 varchar(Max)
    declare @aciinputlogkey1 int, @tableKey INT, @ACIJSONMasterKey int,@COLLECTEDBILLINGKEY int
    set @COLLECTEDBILLINGKEY=0
    set @aciinputlogkey1 = 0
    set @tableKey = 0
    set @ACIJSONMasterKey = 0
    set @tableKey = @PATIENTSURGERYKEY
    set @StringJson_25 = ''

    select top 1 @aciinputlogkey1 = m.aciinputlogkey from ACI_JSON_Master m(nolock)
    join ACI_InPutLog c (nolock) on c.aciinputlogkey = m.aciinputlogkey and c.status = 'N' and  c.TableName = 'JSON25_SURGERYEVENT'
    where m.status = 'N' and c.status = 'N' and  c.TableName = 'JSON25_SURGERYEVENT'
    and c.aciinputlogkey = @ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 = 0
	begin

		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@CHARTRECORDKEY1 INT ,@PatId INT
		 ,@Procedures varchar(8000),@ExamField varchar(8000),@Diagnosis Varchar(5000),@Medication Varchar(5000),@Reason Varchar(5000)
		set @PhyId =0
		set @FacId =0
		set @CHARTRECORDKEY1=0
		set @PatId =0

		Set @Procedures=''
		set @ExamField =''
		set @Diagnosis =''
		set @Medication=''

		select top 1 @PhyId=Ap.ResourceId1,@FacId=R.PracticeId ,@PatId=Ap.PatientId  from dbo.PatientClinical Pc (nolock)
			join dbo.Appointments Ap (nolock) on Ap.AppointmentId = pc.appointmentid
			join dbo.Resources R (nolock) on R.ResourceId  =Ap.ResourceId1
		where pc.appointmentid= @PATIENTSURGERYKEY order by pc.appointmentid desc;
		
		set @ReferenceNumber =''
		select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.'+CAST(@FacId as varchar(10))+'.'+CAST(@PhyId as varchar(10))+'.'+CAST(@PatId as varchar(10))+'.25.' from aci_json_Defaults (NOLOCK)

		Declare @ReferenceNumberT varchar(50),@LabRadiologyOrder varchar(5000),@OrderCode varchar(5000), @ResourceType varchar(50),  @CodeSystem varchar(50)
		set @ReferenceNumberT =''
		set @LabRadiologyOrder =''
		set @OrderCode =''
		set @ReferenceNumberT =@ReferenceNumber+cast(@PATIENTSURGERYKEY as varchar(10))
		Set @ResourceType =''

			IF (SELECT COUNT(*) FROM VIEW_ACI_JSON25_SurgeryEvent SE where SE.ExternalId = @PATIENTSURGERYKEY)>0
				SELECT @StringJson_25= REPLACE(REPLACE( DBO.ACI_QFN_XMLTOJSON (
				(
				SELECT top 1	
						   ISNULL(ResourceType,'') AS ResourceType,
						   ISNULL(IsSource,'') AS IsSource,
						   (isnull(@ReferenceNumber,'')+ Cast(ReferenceNumber as varchar(20) )) AS ReferenceNumber,
						   ISNULL(PatientAccountNumber,'') AS PatientAccountNumber,
						   ISNULL(DateofSurgery,'') AS DateofSurgery,
						   ISNULL(ECNPI,'') AS ECNPI,
						   --ISNULL(ECTIN,'') AS ECTIN,
						   (SUBSTRING(ECTIN,1,3)+'-'+SUBSTRING(ECTIN,4,2)+'-'+SUBSTRING(ECTIN,6,4)) AS ECTIN,
						   ISNULL(ExternalId,'') AS ExternalId,
						--   ISNULL(EncounterId,'') AS EncounterId,
						   ISNULL(EpisodeNumber,'') AS EpisodeNumber,
					--	   ISNULL(ReferralOrderNumber,'') AS ReferralOrderNumber,
						   ISNULL(IsInHousePhysician,'') AS IsInHousePhysician,
						   ISNULL(PerformedPhysicianNPI,'') AS PerformedPhysicianNPI,
						   ISNULL(PerformedPhysicianAccountNumber,'') AS PerformedPhysicianAccountNumber,
						   ISNULL(PerformedPhysicianName,'') AS PerformedPhysicianName
				  from VIEW_ACI_JSON25_SurgeryEvent SE where SE.AppointmentId =@PATIENTSURGERYKEY FOR XML PATH('Level_1'),TYPE ) ) ,'[','') ,'}]','')     
 
		   if ltrim(rTrim(@StringJson_25)) <> ''
		   begin
			  DECLARE CURSORA_25 SCROLL CURSOR FOR 
				select SP.Id from VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE  SP where SP.AppointmentId =@PATIENTSURGERYKEY 
			  OPEN CURSORA_25   
			  FETCH FIRST FROM CURSORA_25 INTO @COLLECTEDBILLINGKEY
			  WHILE @@FETCH_STATUS=0
			  BEGIN
	 
				  set @ExamField =''
				  set @Diagnosis =''
				  set @Medication=''
				  set @Reason =''

				 select  @Procedures= replace(--'}'
				 replace(--']'
				 replace --'['
				  (ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select top 1 
								 ISNULL(Code,'') AS Code,
								 ISNULL(Description,'') AS Description,
								 ISNULL(CodeSystemName,'') AS CodeSystemName,
								 ISNULL(CodeSystem,'') AS CodeSystem								
								 from VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE  SP (NOLOCK) where SP.ID = @COLLECTEDBILLINGKEY for xml path('Level_2'),type
							 ) 
						 )
					 ,'') 
					 ,'[','')
					 ,']','')
					 ,'}','')
 
					 --select @Route
					 --if @Procedures<>''
						--  select  @ExamField=
						--  ISNULL( dbo.ACI_qfn_XmlToJson(	
						-- (select  
						--	  ISNULL(Type,'') AS Type,
						--	  ISNULL(Code,'') AS Code,
						--	  ISNULL(Description,'') AS Description,
						--	  ISNULL(CodeSystem,'') AS CodeSystem,
						--	  ISNULL(CodeSystemName,'') AS CodeSystemName
						--			 from VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_ExamField (NOLOCK) where   PATIENTCATARACTPLANKEY = @PATIENTCATARACTPLANKEY  for xml path('Level_2'),type
						--	 ) 
						-- )
						--  ,'')
 
					 if @Procedures<>''
						  select  @Diagnosis=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Code,'') AS Code,
							 ISNULL(Description,'') AS Description,
							 ISNULL(CodeSystem,'') AS CodeSystem,
							 ISNULL(CodeSystemName,'') AS CodeSystemName
						  from VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE_Diagnosis SD (NOLOCK) where   SD.AppointmentId =@PATIENTSURGERYKEY   for xml path('Level_2'),type
							 ) 
						 )
						  ,'')
 
					 if @Procedures<>''
						  select  @Medication=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Code,'') AS Code,
							 ISNULL(Description,'') AS Description,
							 ISNULL(CodeSystem,'') AS CodeSystem,
							 ISNULL(CodeSystemName,'') AS CodeSystemName
									 from VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE_Medication SM (NOLOCK) where   SM.AppointmentId = @PATIENTSURGERYKEY  for xml path('Level_21'),type
							 ) 
						 )
						  ,'')

 
					 if @Procedures<>''
						  select  @Reason=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
						      ISNULL(Type,'') AS Type,
							 ISNULL(Code,'') AS Code,
							 ISNULL(Description,'') AS Description,
							 ISNULL(CodeSystem,'') AS CodeSystem,
							 ISNULL(CodeSystemName,'') AS CodeSystemName
									 from VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE_Reason  SR (NOLOCK) where   SR.AppointmentId =@PATIENTSURGERYKEY   for xml path('Level_2'),type
							 ) 
						 )
						  ,'')

                      --if @ExamField <>'' 
				  --  set @ExamField=',"ExamField":'+@ExamField
                      if @Diagnosis <>'' 
				    set @Diagnosis=',"Diagnosis":'+@Diagnosis
                      if @Medication <>'' 
				    set @Medication=',"Medication":'+@Medication
                      if @Reason <>'' 
				    set @Reason=',"Reason":'+@Reason

				  if @Procedures  <>''
				    set @Procedures =@Procedures+@Diagnosis+@Medication+@Reason+'}'
				  set @ExamField =''
				  set @Diagnosis =''
				  set @Medication=''
				  set @Reason =''
				--	select @Procedures
				FETCH NEXT FROM CURSORA_25 INTO @COLLECTEDBILLINGKEY
			  END
			  CLOSE CURSORA_25
			  DEALLOCATE CURSORA_25
		   end	
		   --select @Procedures
		  
			if ltrim(rTrim(@StringJson_25)) <> ''
			begin
			  set @StringJson_25 =@StringJson_25+',"Procedure": ['+@Procedures+']}'
			  
			  insert into ACI_JSON_Master(ACIInPutLogKey, tablekey, json,ReferenceNumber,Sort)
			                values(@aciinputlogkey, @tableKey, (ltrim(rTrim(@StringJson_25))),@ReferenceNumberT,25)

			set @ACIJSONMasterKey = IDENT_CURRENT('ACI_JSON_Master')
		   	update ACI_InPutLog set status = 'C', statuschangedt = GETDATE() where status = 'N' and TableName ='JSON25_SURGERYEVENT' 
			and(tableKey = @tableKey and tablekey in (select tablekey from ACI_JSON_Master(nolock) where status = 'N'
				and ACIJSONMasterKey = @ACIJSONMasterKey))
				-- select @StringJson_25			
			end
	end
end
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_Get_ForDEP')) 
Drop Procedure ACI_Get_ForDEP 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE PROCEDURE [dbo].[ACI_Get_ForDEP]
(@PatientKey     INT,
 @ChartRecordKey INT
)
AS
     BEGIN
         SELECT PC.PATIENTID, (M.LASTNAME+'  '+M.FIRSTNAME) AS PatientName,
                PC.ClinicalId as ClinicalId,
                R.ResourceName,
                '' ResourceEmail,
                R.ResourceId ,
		-- l.locationName,
                R.PRACTICEID
         FROM PatientClinical PC(NOLOCK) --chartrecords CR(NOLOCK)
              JOIN Dbo.Appointments A(NOLOCK) ON A.AppointmentId = PC.AppointmentId  -- schedule s(NOLOCK) ON s.schedulekey = cr.schedulekey
              JOIN dbo.Resources R (NOLOCK) ON R.Resourceid=A.resourceid1  --provider P(NOLOCK) ON p.providerkey = cr.providerKey
              --JOIN dbo.PracticeName  L(NOLOCK)   ON l.locationkey = s.OfficeLocationKey    
			  JOIN Model.Patients M(NOLOCK) ON M.Id = PC.PATIENTID
         WHERE PC.PatientId = @PatientKey
               AND PC.ClinicalId  = @ChartRecordKey;
     END
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT')) 
Drop Procedure ACI_ConvertToJson_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE proc[dbo].[ACI_ConvertToJson_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT](@PATIENTCATARACTPLANKEY INT, @aciinputlogkey int)
as
begin
/*
ACI Version :Release 1.1
Date:08/21/2017
Developer/Author:K.V.S.S.R.KRISHNA SARMA
Details/Description: To create JSON String in ACI_JSON_MAster table
*/
/*
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/
    declare @StringJson_26 varchar(Max)
    declare @aciinputlogkey1 int, @tableKey INT, @ACIJSONMasterKey int,@COLLECTEDBILLINGKEY int
    set @COLLECTEDBILLINGKEY=0
    set @aciinputlogkey1 = 0
    set @tableKey = 0
    set @ACIJSONMasterKey = 0
    set @tableKey = @PATIENTCATARACTPLANKEY
    set @StringJson_26 = ''''

    select top 1 @aciinputlogkey1 = m.aciinputlogkey from ACI_JSON_Master m(nolock)
    join ACI_InPutLog c (nolock) on c.aciinputlogkey = m.aciinputlogkey and c.status = ''N'' and  c.TableName = ''JSON26_SURGICALPOSTOPERATIVERESULTEVENT''
    where m.status = ''N'' and c.status = ''N'' and  c.TableName = ''JSON26_SURGICALPOSTOPERATIVERESULTEVENT''
    and c.aciinputlogkey = @ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 = 0
	begin

		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@CHARTRECORDKEY1 INT ,@PatId INT
		 ,@Procedures varchar(8000),@ExamField varchar(8000),@Diagnosis Varchar(5000),@Medication Varchar(5000),@Reason Varchar(5000)
		set @PhyId =0
		set @FacId =0
		set @CHARTRECORDKEY1=0
		set @PatId =0

		Set @Procedures=''''
		set @ExamField =''''
		set @Diagnosis =''''
		set @Medication=''''

		--select top 1 @PhyId=c.PROVIDERKEY,@FacId=s.OFFICELOCATIONKEY ,@PatId=s.PATIENTKEY,@CHARTRECORDKEY1=pcp.CHARTRECORDKEY  from PATIENTCATARACTPLAN PCP (nolock)
		--     JOIN chartrecords c  ON c.CHARTRECORDKEY=pcp.CHARTRECORDKEY
		--	join SCHEDULE s (nolock) on s.SCHEDULEKEY  =c.SCHEDULEKEY          	
		--	where PCP.PATIENTCATARACTPLANKEY =@PATIENTCATARACTPLANKEY order by c.CHARTRECORDKEY desc

			select top 1 @PhyId=Ap.ResourceId1,@FacId=R.PracticeId ,@PatId=Ap.PatientId  from dbo.Appointments Ap (nolock) 
			join dbo.Resources R (nolock) on R.ResourceId  =Ap.ResourceId1
		where ap.appointmentid =@PATIENTCATARACTPLANKEY order by ap.appointmentid desc;

		
		set @ReferenceNumber =''''
		select @ReferenceNumber = aci_vendorID+''.''+MDO_PracticeId+''.''+CAST(@FacId as varchar(10))+''.''+CAST(@PhyId as varchar(10))+''.''+CAST(@PatId as varchar(10))+''.26.'' from aci_json_Defaults (NOLOCK)

		Declare @ReferenceNumberT varchar(50),@LabRadiologyOrder varchar(5000),@OrderCode varchar(5000)
		set @ReferenceNumberT =''''
		set @LabRadiologyOrder =''''
		set @OrderCode =''''
		set @ReferenceNumberT =@ReferenceNumber+cast(@PATIENTCATARACTPLANKEY as varchar(10))

			IF (SELECT COUNT(*) FROM VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT SE where SE.ExternalId =@PATIENTCATARACTPLANKEY)>0
				SELECT @StringJson_26= REPLACE(REPLACE( DBO.ACI_QFN_XMLTOJSON (
				(
				SELECT top 1	
						   ISNULL(ResourceType,'''') AS ResourceType,
						   ISNULL(IsSource,'''') AS IsSource,
						   (isnull(@ReferenceNumber,'''')+Cast(ReferenceNumber as varchar(20) )) AS ReferenceNumber,
						   ISNULL(PatientAccountNumber,'''') AS PatientAccountNumber,
						   ISNULL(DateofEvent,'''') AS DateofEvent,
						   ISNULL(ECNPI,'''') AS ECNPI,
						   --ISNULL(ECTIN,'''') AS ECTIN,
						    (SUBSTRING(ECTIN,1,3)+''-''+SUBSTRING(ECTIN,4,2)+''-''+SUBSTRING(ECTIN,6,4)) AS ECTIN,
						   ISNULL(ExternalId,'''') AS ExternalId,
						--   ISNULL(EncounterId,'''') AS EncounterId,
						   ISNULL(EpisodeNumber,'''') AS EpisodeNumber,
						   --ISNULL(ReferralOrderNumber,'''') AS ReferralOrderNumber,
						   ISNULL(IsInHousePhysician,'''') AS IsInHousePhysician,
						   ISNULL(PerformedPhysicianNPI,'''') AS PerformedPhysicianNPI,
						   ISNULL(PerformedPhysicianAccountNumber,'''') AS PerformedPhysicianAccountNumber,
						   ISNULL(PerformedPhysicianName,'''') AS PerformedPhysicianName
				  from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT SE where  SE.ExternalId =@PATIENTCATARACTPLANKEY FOR XML PATH(''Level_1''),TYPE ) ) ,''['','''') ,''}]'','''')     
 
		   if ltrim(rTrim(@StringJson_26)) <> ''''
		   begin
			  DECLARE CURSORA_26 SCROLL CURSOR FOR 
				select Id from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_Procedure SP where SP.AppointmentId = @PATIENTCATARACTPLANKEY 
			  OPEN CURSORA_26   
			  FETCH FIRST FROM CURSORA_26 INTO @COLLECTEDBILLINGKEY
			  WHILE @@FETCH_STATUS=0
			  BEGIN
	 
				  set @ExamField =''''
				  set @Diagnosis =''''
				  set @Medication=''''
				  set @Reason =''''

				 select  @Procedures= replace(--''}''
				 replace(--'']''
				 replace --''[''
				  (ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select top 1 
								 ISNULL(Code,'''') AS Code,
								 ISNULL(Description,'''') AS Description,
								 ISNULL(CodeSystemName,'''') AS CodeSystemName,
								 ISNULL(CodeSystem,'''') AS CodeSystem								
								 from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_Procedure SP (NOLOCK) where SP.AppointmentId = @PATIENTCATARACTPLANKEY for xml path(''Level_2''),type
							 ) 
						 )
					 ,'''') 
					 ,''['','''')
					 ,'']'','''')
					 ,''}'','''')
 
					 --select @Route
					 if @Procedures<>''''
						  select  @ExamField=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Type,'''') AS Type,
							  ISNULL(Code,'''') AS Code,
							  ISNULL(Description,'''') AS Description,
							  ISNULL(CodeSystem,'''') AS CodeSystem,
							  ISNULL(CodeSystemName,'''') AS CodeSystemName
									 from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_ExamField SPE (NOLOCK) where   SPE.AppointmentId = @PATIENTCATARACTPLANKEY  for xml path(''Level_2''),type
							 ) 
						 )
						  ,'''')
 
					 if @Procedures<>''''
						  select  @Diagnosis=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Code,'''') AS Code,
							 ISNULL(Description,'''') AS Description,
							 ISNULL(CodeSystem,'''') AS CodeSystem,
							 ISNULL(CodeSystemName,'''') AS CodeSystemName
						  from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Diagnosis  SD (NOLOCK) where   SD.AppointmentId = @PATIENTCATARACTPLANKEY  for xml path(''Level_2''),type
							 ) 
						 )
						  ,'''')
 
					 if @Procedures<>''''
						  select  @Medication=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Code,'''') AS Code,
							 ISNULL(Description,'''') AS Description,
							 ISNULL(CodeSystem,'''') AS CodeSystem,
							 ISNULL(CodeSystemName,'''') AS CodeSystemName
									 from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Medication  SM (NOLOCK) where   SM.AppointmentId = @PATIENTCATARACTPLANKEY  for xml path(''Level_2''),type
							 ) 
						 )
						  ,'''')

 
					 if @Procedures<>''''
						  select  @Reason=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
						      ISNULL(Type,'''') AS Type,
							 ISNULL(Code,'''') AS Code,
							 ISNULL(Description,'''') AS Description,
							 ISNULL(CodeSystem,'''') AS CodeSystem,
							 ISNULL(CodeSystemName,'''') AS CodeSystemName
									 from VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Reason SR (NOLOCK) where   SR.AppointmentId = @PATIENTCATARACTPLANKEY  for xml path(''Level_2''),type
							 ) 
						 )
						  ,'''')

                      if @ExamField <>'''' 
				    set @ExamField='',"ExamField":''+@ExamField
                      if @Diagnosis <>'''' 
				    set @Diagnosis='',"Diagnosis":''+@Diagnosis
                      if @Medication <>'''' 
				    set @Medication='',"Medication":''+@Medication
                      if @Reason <>'''' 
				    set @Reason='',"Reason":''+@Reason

				  if @Procedures  <>''''
				    set @Procedures =@Procedures+@ExamField+@Diagnosis+@Medication+@Reason+''}''
				  set @ExamField =''''
				  set @Diagnosis =''''
				  set @Medication=''''
				  set @Reason =''''
				--	select @Procedures
				FETCH NEXT FROM CURSORA_26 INTO @COLLECTEDBILLINGKEY
			  END
			  CLOSE CURSORA_26
			  DEALLOCATE CURSORA_26
		   end	
		   --select @Procedures
		  
			if ltrim(rTrim(@StringJson_26)) <> ''''
			begin
			  set @StringJson_26 =@StringJson_26+'',"Procedure": [''+@Procedures+'']}''
			  
			  insert into ACI_JSON_Master(ACIInPutLogKey, tablekey, json,ReferenceNumber,Sort)
			                values(@aciinputlogkey, @tableKey, (ltrim(rTrim(@StringJson_26))),@ReferenceNumberT,26)

			set @ACIJSONMasterKey = IDENT_CURRENT(''ACI_JSON_Master'')
		   	update ACI_InPutLog set status = ''C'', statuschangedt = GETDATE() where status = ''N'' and TableName =''JSON26_SURGICALPOSTOPERATIVERESULTEVENT'' 
			and(tableKey = @tableKey and tablekey in (select tablekey from ACI_JSON_Master(nolock) where status = ''N''
				and ACIJSONMasterKey = @ACIJSONMasterKey))
				--select @StringJson_26			
			end
	end
end


' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON27_POST_SURGICAL_COMPLICATION_EVENT')) 
Drop Procedure ACI_ConvertToJson_JSON27_POST_SURGICAL_COMPLICATION_EVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE proc[dbo].[ACI_ConvertToJson_JSON27_POST_SURGICAL_COMPLICATION_EVENT] (@PATIENTSURGERYKEY INT, @aciinputlogkey int)
as
begin

    declare @StringJson_27 varchar(Max)
    declare @aciinputlogkey1 int, @tableKey INT, @ACIJSONMasterKey int,@COLLECTEDBILLINGKEY int
    set @COLLECTEDBILLINGKEY=0
    set @aciinputlogkey1 = 0
    set @tableKey = 0
    set @ACIJSONMasterKey = 0
    set @tableKey = @PATIENTSURGERYKEY
    set @StringJson_27 = ''

    select top 1 @aciinputlogkey1 = m.aciinputlogkey from ACI_JSON_Master m(nolock)
    join ACI_InPutLog c (nolock) on c.aciinputlogkey = m.aciinputlogkey and c.status = 'N' and  c.TableName = 'JSON27_POSTSURGICALCOMPLICATIONEVENT'
    where m.status = 'N' and c.status = 'N' and  c.TableName = 'JSON27_POSTSURGICALCOMPLICATIONEVENT'
    and c.aciinputlogkey = @ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 = 0
	begin
		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@CHARTRECORDKEY1 INT ,@PatId INT
		 ,@Procedures varchar(8000),@ExamField varchar(8000),@Diagnosis Varchar(5000),@Medication Varchar(5000),@Reason Varchar(5000)
		set @PhyId =0
		set @FacId =0
		set @CHARTRECORDKEY1=0
		set @PatId =0

		Set @Procedures=''
		set @ExamField =''
		set @Diagnosis =''
		set @Medication=''

		--select top 1 @PhyId=c.PROVIDERKEY,@FacId=s.OFFICELOCATIONKEY ,@PatId=s.PATIENTKEY,@CHARTRECORDKEY1=pcp.CHARTRECORDKEY  from PATIENTSURGERY PCP (nolock)
		--     JOIN chartrecords c  ON c.CHARTRECORDKEY=pcp.CHARTRECORDKEY
		--	join SCHEDULE s (nolock) on s.SCHEDULEKEY  =c.SCHEDULEKEY          	
		--	where PCP.PATIENTSURGERYKEY =@PATIENTSURGERYKEY order by c.CHARTRECORDKEY desc

		select top 1 @PhyId=Ap.ResourceId1,@FacId=R.PracticeId ,@PatId=Ap.PatientId  from dbo.PatientClinical Pc (nolock)
			join dbo.Appointments Ap (nolock) on Ap.AppointmentId = pc.appointmentid
			join dbo.Resources R (nolock) on R.ResourceId  =Ap.ResourceId1
		where pc.appointmentid= @PATIENTSURGERYKEY order by pc.appointmentid desc;
		
		set @ReferenceNumber =''
		select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.'+CAST(@FacId as varchar(10))+'.'+CAST(@PhyId as varchar(10))+'.'+CAST(@PatId as varchar(10))+'.27.' from aci_json_Defaults (NOLOCK)

		Declare @ReferenceNumberT varchar(50),@LabRadiologyOrder varchar(5000),@OrderCode varchar(5000)
		set @ReferenceNumberT =''
		set @LabRadiologyOrder =''
		set @OrderCode =''
		set @Procedures=''
		set @ReferenceNumberT =@ReferenceNumber+cast(@PATIENTSURGERYKEY as varchar(10))
		
			IF (SELECT COUNT(*) FROM VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT SE where SE.ExternalId = @PATIENTSURGERYKEY)>0
				SELECT @StringJson_27= REPLACE(REPLACE( DBO.ACI_QFN_XMLTOJSON (
				(
				SELECT top 1	
						   ISNULL(ResourceType,'') AS ResourceType,
						   ISNULL(IsSource,'') AS IsSource,
						   (isnull(@ReferenceNumber,'')+Cast(ReferenceNumber as varchar(20) )) AS ReferenceNumber,
						   ISNULL(PatientAccountNumber,'') AS PatientAccountNumber,
						   ISNULL(DateofEvent,'') AS DateofEvent,
						   ISNULL(ECNPI,'') AS ECNPI,
						   --ISNULL(ECTIN,'') AS ECTIN,
						   (SUBSTRING(ECTIN,1,3)+'-'+SUBSTRING(ECTIN,4,2)+'-'+SUBSTRING(ECTIN,6,4)) as ECTIN,
						   ISNULL(ExternalId,'') AS ExternalId,
						--   ISNULL(EncounterId,'') AS EncounterId,
						   ISNULL(EpisodeNumber,'') AS EpisodeNumber,
					--	   ISNULL(ReferralOrderNumber,'') AS ReferralOrderNumber,
						   ISNULL(IsInHousePhysician,'') AS IsInHousePhysician,
						   ISNULL(PerformedPhysicianNPI,'') AS PerformedPhysicianNPI,
						   ISNULL(PerformedPhysicianAccountNumber,'') AS PerformedPhysicianAccountNumber,
						   ISNULL(PerformedPhysicianName,'') AS PerformedPhysicianName
				  from VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT SE where SE.ExternalId = @PATIENTSURGERYKEY FOR XML PATH('Level_1'),TYPE ) ) ,'[','') ,'}]','')     
 --print @StringJson_27
		   if ltrim(rTrim(@StringJson_27)) <> ''
		   begin
			  DECLARE CURSORA_27 SCROLL CURSOR FOR 
				select SP.ID from VIEW_ACI_JSON25_SURGERYEVENT_PROCEDURE SP where SP.AppointmentId = @PATIENTSURGERYKEY 
			  OPEN CURSORA_27   
			  FETCH FIRST FROM CURSORA_27 INTO @COLLECTEDBILLINGKEY
			  WHILE @@FETCH_STATUS=0
			  BEGIN
	 
				  set @ExamField =''
				  set @Diagnosis =''
				  set @Medication=''
				  set @Reason =''

				 select  @Procedures= replace(--'}'
				 replace(--']'
				 replace --'['
				  (ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select top 1 
								 ISNULL(Code,'') AS Code,
								 ISNULL(Description,'') AS Description,
								 ISNULL(CodeSystemName,'') AS CodeSystemName,
								 ISNULL(CodeSystem,'') AS CodeSystem								
								 from VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure SP (NOLOCK) where SP.ID = @COLLECTEDBILLINGKEY for xml path('Level_2'),type
							 ) 
						 )
					 ,'') 
					 ,'[','')
					 ,']','')
					 ,'}','')
 --print @Procedures
					 --select @Route
					 --if @Procedures<>''
						--  select  @ExamField=
						--  ISNULL( dbo.ACI_qfn_XmlToJson(	
						-- (select  
						--	  ISNULL(Type,'') AS Type,
						--	  ISNULL(Code,'') AS Code,
						--	  ISNULL(Description,'') AS Description,
						--	  ISNULL(CodeSystem,'') AS CodeSystem,
						--	  ISNULL(CodeSystemName,'') AS CodeSystemName
						--			 from VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_ExamField (NOLOCK) where   PATIENTCATARACTPLANKEY = @PATIENTCATARACTPLANKEY  for xml path('Level_2'),type
						--	 ) 
						-- )
						--  ,'')
 
					 if @Procedures<>''
						  select  @Diagnosis=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Code,'') AS Code,
							 ISNULL(Description,'') AS Description,
							 ISNULL(CodeSystem,'') AS CodeSystem,
							 ISNULL(CodeSystemName,'') AS CodeSystemName
						  from VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure_Diagnosis SD (NOLOCK) where   SD.AppointmentId = @PATIENTSURGERYKEY   for xml path('Level_2'),type
							 ) 
						 )
						  ,'')
 --print @Diagnosis
					 if @Procedures<>''
						  select  @Medication=
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  
							  ISNULL(Code,'') AS Code,
							 ISNULL(Description,'') AS Description,
							 ISNULL(CodeSystem,'') AS CodeSystem,
							 ISNULL(CodeSystemName,'') AS CodeSystemName
									 from VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure_Medication SM (NOLOCK) where   SM.AppointmentId = @PATIENTSURGERYKEY  for xml path('Level_21'),type
							 ) 
						 )
						  ,'')

					 if @Procedures<>''
						  select  @Reason=
						  replace(
						  replace(
						  ISNULL( dbo.ACI_qfn_XmlToJson(	
						 (select  top 1
						      ISNULL(Type,'') AS Type,
							 ISNULL(Code,'') AS Code,
							 ISNULL(Description,'') AS Description,
							 ISNULL(CodeSystem,'') AS CodeSystem,
							 ISNULL(CodeSystemName,'') AS CodeSystemName
									 from VIEW_ACI_JSON27_POST_SURGICAL_COMPLICATION_EVENT_Procedure_Reason SR (NOLOCK)  where   SR.AppointmentId = @PATIENTSURGERYKEY   for xml path('Level_2'),type
							 ) 
						 )
						  ,'')
						  ,'[','')
						  ,']','')

                      --if @ExamField <>'' 
				  --  set @ExamField=',"ExamField":'+@ExamField
                      if @Diagnosis <>'' 
				    set @Diagnosis=',"Diagnosis":'+@Diagnosis
                      if @Medication <>'' 
				    set @Medication=',"Medication":'+@Medication
                      if @Reason <>'' 
				    set @Reason=',"Reason":'+@Reason

				  if @Procedures  <>''
				    set @Procedures =@Procedures+@Diagnosis+@Medication+@Reason+'}'
				  set @ExamField =''
				  set @Diagnosis =''
				  set @Medication=''
				  set @Reason =''
				--	select @Procedures
				FETCH NEXT FROM CURSORA_27 INTO @COLLECTEDBILLINGKEY
			  END
			  CLOSE CURSORA_27
			  DEALLOCATE CURSORA_27
		   end	
		   --select @Procedures
		  
			if ltrim(rTrim(@StringJson_27)) <> ''
			begin
			  set @StringJson_27 =@StringJson_27+',"Procedure": ['+@Procedures+']}'
			  
			  insert into ACI_JSON_Master(ACIInPutLogKey, tablekey, json,ReferenceNumber,Sort)
			                values(@aciinputlogkey, @tableKey, (ltrim(rTrim(@StringJson_27))),@ReferenceNumberT,27)

			set @ACIJSONMasterKey = IDENT_CURRENT('ACI_JSON_Master')
		   	update ACI_InPutLog set status = 'C', statuschangedt = GETDATE() where status = 'N' and TableName ='JSON27_POSTSURGICALCOMPLICATIONEVENT' 
			and(tableKey = @tableKey and tablekey in (select tablekey from ACI_JSON_Master(nolock) where status = 'N'
				and ACIJSONMasterKey = @ACIJSONMasterKey))
				-- select @StringJson_27			
			end
	end
end
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON28_Results')) 
Drop Procedure ACI_ConvertToJson_JSON28_Results 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE proc[dbo].[ACI_ConvertToJson_JSON28_Results] (@PATIENTORDERKEY INT, @aciinputlogkey int)
as
begin

    declare @StringJson_28 varchar(Max)
    declare @aciinputlogkey1 int, @tableKey INT, @ACIJSONMasterKey int 
    
    set @aciinputlogkey1 = 0
    set @tableKey = 0
    set @ACIJSONMasterKey = 0
    set @tableKey = @PATIENTORDERKEY
    set @StringJson_28 = ''

    select top 1 @aciinputlogkey1 = m.aciinputlogkey from ACI_JSON_Master m(nolock)
    join ACI_InPutLog c (nolock) on c.aciinputlogkey = m.aciinputlogkey and c.status = 'N' and  c.TableName = 'JSON28_RESULTS'
    where m.status = 'N' and c.status = 'N' and  c.TableName = 'JSON28_RESULTS'
    and c.aciinputlogkey = @ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 = 0
	begin
		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@CHARTRECORDKEY1 INT ,@PatId INT		 
		 ,@LABADDRESS Varchar(5000),@LABPHONE varchar(2000),@LABSTATUS varchar(20)
		set @PhyId =0
		set @FacId =0
		set @CHARTRECORDKEY1=0
		set @PatId =0

		Set @LABADDRESS=''
		set @LABPHONE =''
		set @LABSTATUS =''

		Select top 1  @LABSTATUS = IsActive
		FROM [dbo].[VIEW_ACI_JSON28_RESULTS] --DBO.PATIENTLABTESTRESULTS PLTR (NOLOCK) LEFT JOIN DBO.PATIENTLABORDERS PLO (NOLOCK) ON PLTR.LABORDERID = PLO.ORDERID
		WHERE Id = @PATIENTORDERKEY
		
		Select top 1 @PhyId= ap.ResourceId1,@FacId=R.PracticeID,@PatId=ap.PatientId  from dbo.Patientlabtestresults ptlr (nolock) join dbo.appointments ap WITH (NOLOCK) ON
	ptlr.AppointmentId =ap.appointmentid join  dbo.resources R with (NOLOCK) ON R.ResourceId=ap.ResourceId1
	Where ptlr.Id  = @PATIENTORDERKEY order by ap.appdate desc   
	    	
		
		set @ReferenceNumber =''
		select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.'+CAST(@FacId as varchar(10))+'.'+CAST(@PhyId as varchar(10))+'.'+CAST(@PatId as varchar(10))+'.28.' from aci_json_Defaults (NOLOCK)

		Declare @ReferenceNumberT varchar(50),@LabRadiologyOrder varchar(5000),@OrderCode varchar(5000)
		set @ReferenceNumberT =''
		set @LabRadiologyOrder =''
		set @OrderCode =''
		set @ReferenceNumberT =@ReferenceNumber+cast(@PATIENTORDERKEY as varchar(10))




		    select  @StringJson_28= replace(--'}'
		  replace(--']'
		  replace --'['
		   (ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select top 1    ISNULL(ResourceType,'') AS ResourceType,ISNULL(IsSource,'') AS IsSource,@ReferenceNumber+ISNULL(ReferenceNumber,'0') AS ReferenceNumber,
							    ISNULL(ExternalId,'') AS ExternalId,ISNULL(Type,'') AS Type,ISNULL(Code,'') AS Code,ISNULL(CodeSystem,'') AS CodeSystem,ISNULL(TestName,'') AS TestName,
							    ISNULL(CodeSystemName,'') AS CodeSystemName,ISNULL(Value,'') AS Value,ISNULL(Units,'') AS Units,ISNULL(ReferenceRange,'') AS ReferenceRange,
							    ISNULL(Interpretation,'') AS Interpretation,isnull(PatientAccountNumber,'') as PatientAccountNumber,
							    ISNULL(Date,'') AS Date,
							    isnull(ECNPI,'') as ECNPI,
							    (SUBSTRING(ECTIN,1,3)+'-'+SUBSTRING(ECTIN,4,2)+'-'+SUBSTRING(ECTIN,6,4)) as ECTIN ,
							    ISNULL(LabName,'') AS LabName,ISNULL(ISACTIVE,'') AS ISACTIVE
					   from VIEW_ACI_JSON28_RESULTS 
					   where  Id =@PATIENTORDERKEY  for xml path('RESULT'),type	    
					  ) 
				  )
			  ,'') 
			  ,'[','')
			  ,']','')
			  ,'}','')

			 

			  select @StringJson_28
			  if @StringJson_28<>''
				   select  @LABADDRESS= REPLACE(
				   REPLACE(
				   ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select top 1
									    ISNULL(AddressLine1,'') AS AddressLine1,ISNULL(AddressLine2,'') AS AddressLine2,ISNULL(City,'') AS City,ISNULL(State,'') AS State,
									    ISNULL(Zip,'') AS Zip,ISNULL(County,'') AS County,ISNULL(CountyCode,'') AS CountyCode
							  from VIEW_ACI_JSON28_RESULTS_LABADDRESS (NOLOCK) where  Id =@PATIENTORDERKEY  for xml path('LABADDRESS'),type
					  ) 
				  )
				   ,'')
				   ,'[','')
				   ,']','')
	          
			  if @StringJson_28<>''
				   select  @LABPHONE=			
				   ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select 
						    ISNULL(Type,'') AS Type,ISNULL(Number,'') AS Number
							from VIEW_ACI_JSON28_RESULTS_LABPHONE (NOLOCK) where  Id =@PATIENTORDERKEY   for xml path('LABPHONEa'),type
					  ) 
				  )
				   ,'')        
	          
				if @LABADDRESS<>''
				  set @LABADDRESS =',"LabAddress":'+ @LABADDRESS  
				if @LABPHONE<>''
				  set @LABPHONE =',"LabPhone":'+ @LABPHONE  	         
 
			  if @StringJson_28 <>''
 
	    		   set @StringJson_28 =@StringJson_28+@LABADDRESS+@LABPHONE+',"IsActive" : "'+@LABSTATUS+'"}'
					set @StringJson_28= REPLACE(REPLACE(@StringJson_28,'@@',']'),'##','[')
		   --select @Procedures
		   select @StringJson_28
			if ltrim(rTrim(@StringJson_28)) <> ''
			begin
			  set @StringJson_28 = @StringJson_28
			  
			  insert into ACI_JSON_Master(ACIInPutLogKey, tablekey, json,ReferenceNumber,Sort)
			                values(@aciinputlogkey, @tableKey, (ltrim(rTrim(@StringJson_28))),@ReferenceNumberT,28)

			set @ACIJSONMasterKey = IDENT_CURRENT('ACI_JSON_Master')
		   	update ACI_InPutLog set status = 'C', statuschangedt = GETDATE() where status = 'N' and TableName ='JSON28_RESULTS' 
			and(tableKey = @tableKey and tablekey in (select tablekey from ACI_JSON_Master(nolock) where status = 'N'
				and ACIJSONMasterKey = @ACIJSONMasterKey))
				 --select @StringJson_28			
			end
	end
end
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON9_PORTALACCOUNTADMINISTRATION')) 
Drop Procedure ACI_ConvertToJson_JSON9_PORTALACCOUNTADMINISTRATION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE proc [dbo].[ACI_ConvertToJson_JSON9_PORTALACCOUNTADMINISTRATION] (@MUMetrics_PortalKey INT,@aciinputlogkey int)
as 
begin
 
  declare @StringJson varchar(8000)
  declare @aciinputlogkey1 int ,@tableKey INT, @ACIJSONMasterKey int
  set @aciinputlogkey1 =0
  set @tableKey=0
  set @ACIJSONMasterKey =0
  set @tableKey=@MUMetrics_PortalKey
  set @StringJson=''

	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status='N' and c.TableName ='JSON9_PORTALACCOUNTADMINISTRATION' 
	where m.status ='N' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 =0
	begin
		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@PatId INT,@ID Integer
		set @PhyId =0
		set @FacId =0
		set @PatId =0
		set @ID =0

		--select top 1 @PhyId=mp.providerID,@FacId=p.OFFICELOCATIONKEY ,@PatId=mp.patientID 
		--   from  MUMetrics_Portal MP (NOLOCK)
		--   join patient p (NOLOCK) on p.patientkey =mp.patientID where MUMetrics_PortalKey =@MUMetrics_PortalKey
		select 1;    
		SELECT TOP 1 @PhyId = A.DefaultUserId, @PatId = PRPI.PatientNo, @FacId = R.PracticeId  
			FROM MVE.PP_PortalQueueResponse PRPI WITH (NOLOCK)			
			LEFT JOIN Model.Patients A on A.Id = PRPI.PatientNo
			LEFT JOIN dbo.Resources R on R.ResourceId = A.DefaultUserId
            WHERE PRPI.ResponseId = @MUMetrics_PortalKey;

		set @ReferenceNumber =''
		select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.'+CAST(@FacId as varchar(10))+'.'+CAST(@PhyId as varchar(10))+'.'+CAST(@PatId as varchar(10))+'.9.' from aci_json_Defaults (NOLOCK)

		select top 1 @ID =ResponseID FROM MVE.PP_PortalQueueResponse (NOLOCK)

		Declare @ReferenceNumberT varchar(50)
		set @ReferenceNumberT =''
		set @ReferenceNumberT =@ReferenceNumber+cast(@ID as varchar(10))

		if (select count(*) from VIEW_ACI_JSON9_PORTALACCOUNTADMINISTRATION ) >0
			select @StringJson=@StringJson+ 
				isnull(REPLACE(REPLACE(
						dbo.aci_qfn_XmlToJson (
						(     
						select top 1
							 'PortalAccount' AS ResourceType,
							 ISNULL(ExternalId,'') AS ExternalId,
							 ISNULL(IsSource,'') AS IsSource,
							 (@ReferenceNumber+ ISNULL(ReferenceNumber,'')) AS ReferenceNumber,
							 ISNULL(@PatId,'') AS PatientAccountNumber,
							 ISNULL(PortalType,'') AS PortalType,
							 ISNULL(PortalAccountNumber,'') AS PortalAccountNumber,
							 ISNULL(AccountCreatedDate,'') AS AccountCreatedDate,							 
							 ISNULL(AccountInformationProvidedDate,'') AS AccountInformationProvidedDate,
							 ISNULL(AccountDisabledDate,'') AS AccountDisabledDate,
							 ISNULL(RepresentativeExternalId,'') as RepresentativeExternalId
						from VIEW_ACI_JSON9_PORTALACCOUNTADMINISTRATION L where L.ReferenceNumber = @MUMetrics_PortalKey
						for xml path('PortalAccount1'),type) 
				),'[','') ,'}]','}')    
				,'')   

      if ltrim(rTrim(ISNULL(@StringJson,''))) <> ''
	  begin
	  --print   @StringJson
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,ReferenceNumber,Sort) 
                 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))),@ReferenceNumberT,6)  
--print  @StringJson
		set @ACIJSONMasterKey=IDENT_CURRENT('ACI_JSON_Master')
		update ACI_InPutLog set status ='C',statuschangedt=GETDATE() where status ='N' and  TableName = 'JSON9_PORTALACCOUNTADMINISTRATION'  
		and( tableKey=@tableKey and tablekey in(select tablekey from ACI_JSON_Master (nolock) where status='N' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
  end
end
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_CREATE_JSONS')) 
Drop Procedure ACI_CREATE_JSONS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE PROCEDURE [dbo].[ACI_CREATE_JSONS] AS
BEGIN
/*------------------NOT IN USE ------------------*/
DECLARE @LISTSTR VARCHAR(8000),@FILEPATH VARCHAR(1000)
DECLARE @INITSTR VARCHAR(8000),@FILEPAT VARCHAR(8000),@PATLENGTH INT
DECLARE @ACIINPUTLOGKEY INT ,@TABLEKEY INT ,@TABLENAME VARCHAR(100) ,@JSON VARCHAR(8000),@JSONFILE VARCHAR(MAX)
       ,@FILENAME VARCHAR(100),@FILEEXISTS INT,@OLDFILENAME VARCHAR(100) , @NEWFILENAME VARCHAR(100)
	   ,@CMD VARCHAR(500),@TIMESTAMP VARCHAR(100),@ACIJSONMASTERKEY int


		/*================= BATCHNO =========================================================*/
		DECLARE @BATCHNO VARCHAR(20),@BATCHNO_PRIOR VARCHAR(9),@BATCHID INT,@BATCHDT VARCHAR(8)
		SET @BATCHNO=''
		SET @BATCHID=1
		SET @BATCHDT=CONVERT(VARCHAR, GETDATE(), 112)

		UPDATE ACI_JSON_DEFAULTS SET BATCHDATE =@BATCHDT WHERE BATCHDATE IS NULL

		UPDATE ACI_JSON_DEFAULTS SET BATCHNO=0 ,BATCHDATE = @BATCHDT WHERE BATCHDATE <> @BATCHDT

		SELECT @BATCHNO_PRIOR=(ISNULL(ACI_VendorId,'1000')+MDO_PracticeId)  FROM ACI_JSON_DEFAULTS

		--SELECT @BATCHNO_PRIOR

		SELECT @BATCHID=(ISNULL(BATCHNO,0)+1) FROM ACI_JSON_DEFAULTS WHERE BATCHDATE =@BATCHDT
		--SELECT @BATCHID
		SET @BATCHNO=@BATCHNO_PRIOR+@BATCHDT+CAST(@BATCHID AS VARCHAR(3))
		--SELECT @BATCHNO
/*================= BATCHNO =========================================================*/



  SET @JSONFILE =''
  

	DECLARE CURSORA SCROLL CURSOR FOR  
		
	 select   top (select records from aci_JSON_defaults ) ACIJSONMASTERKEY,  m.TABLEKEY ,
	   CASE WHEN CHARINDEX('"RESOURCETYPE":"PATIENTEXAM"',ISNULL(JSON,'')) >0 
	   THEN
    		ISNULL(JSON,'')+ISNULL(ENCOUNTER,'')+ISNULL(ALLERGIES,'')+ISNULL(FAMILYHISTORY,'')+
			ISNULL(FUNCTIONALSTATUS,'')+ISNULL(IMMUNIZATION,'')+ISNULL(MEDICALEQUIPMENT,'')+
			ISNULL(MEDICATION,'')+ISNULL(PAYER,'')+ISNULL(PLANOFTREATMENT,'')+ISNULL(PROBLEMSLIST,'')+
			ISNULL(PROCEDURES,'')+isnull(RESULT,'')+isnull(SOCIALHISTORY,'')+ISNULL(VITALSIGN,'')+
			ISNULL(INSTRUCTIONS,'')
			+'}'
	ELSE
    		ISNULL(JSON,'')+ISNULL(ENCOUNTER,'')+ISNULL(ALLERGIES,'')+ISNULL(FAMILYHISTORY,'')+
			ISNULL(FUNCTIONALSTATUS,'')+ISNULL(IMMUNIZATION,'')+ISNULL(MEDICALEQUIPMENT,'')+
			ISNULL(MEDICATION,'')+ISNULL(PAYER,'')+ISNULL(PLANOFTREATMENT,'')+ISNULL(PROBLEMSLIST,'')+
			ISNULL(PROCEDURES,'')+isnull(RESULT,'')+isnull(SOCIALHISTORY,'')+ISNULL(VITALSIGN,'')+
			ISNULL(INSTRUCTIONS,'')
		
	END AS JSONNEW 
	,m.ACIJSONMASTERKEY 
	  from ACI_JSON_MASTER  m (NOLOCK) 
	  JOIN ACI_INPUTLOG C (NOLOCK) ON C.ACIINPUTLOGKEY=M.ACIINPUTLOGKEY
	  WHERE ISNULL(JSON,'') <>'' -- and m.isvalidjson ='T'
	   and m.status='N'

	OPEN CURSORA   
	FETCH FIRST FROM CURSORA INTO @ACIINPUTLOGKEY,@TABLEKEY,@JSON --,@TABLENAME
	WHILE @@FETCH_STATUS=0
	BEGIN
		--SET @FILENAME =@TABLENAME+'_'+CAST(@TABLEKEY AS VARCHAR(10))+'.JSON'
	       SET @FILENAME =@BATCHNO+'.JSON'
		  SET @FILEEXISTS =0

		  SELECT @FILEEXISTS =DBO.ACI_FN_FILEEXISTS(@FILEPATH+'\'+@FILENAME)
		  IF @FILEEXISTS >0 --IF FILE EXISTS IT THEN ONLY IT WILL MOVE TO OLD FOLDER
		  BEGIN
		    DECLARE @PATHOLD VARCHAR(100)
	        SET @PATHOLD =@FILEPATH+'\OLD'  
		    EXEC MASTER.DBO.XP_CREATE_SUBDIR @PATHOLD

		    SET @CMD ='MOVE '+@FILEPATH+'\'+@FILENAME+' '+@FILEPATH+'\OLD\'
		--	PRINT @CMD
			EXEC MASTER..XP_CMDSHELL @CMD

			SET  @OLDFILENAME = @FILENAME
			SET @TIMESTAMP =REPLACE(REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR, GETDATE(), 121), '-', '_'),':','_') , '.', '_'), ' ', '_')-- CONVERT(VARCHAR, GETDATE(), 120)

			SELECT @NEWFILENAME = LEFT(@OLDFILENAME, LEN(@OLDFILENAME) - 5) + '_' + @TIMESTAMP + '.JSON'
			SET @CMD ='RENAME '+@FILEPATH+'\OLD\' + @OLDFILENAME + ' '+ @NEWFILENAME
			--PRINT @CMD
			EXEC MASTER..XP_CMDSHELL @CMD
		    
		  END
 --PRINT @JSON
         IF CAST(DATALENGTH(@JSONFILE)/ 1048576.0  AS NUMERIC(13,2)) <= 5.00
		 BEGIN
		     IF CAST(DATALENGTH(@JSONFILE+@JSON)/ 1048576.0  AS NUMERIC(13,2)) <= 5.00
			 BEGIN
			    IF @JSONFILE ='' 
		          SET @JSONFILE ='['+@JSON
				ELSE
				SET @JSONFILE =@JSONFILE+','+@JSON
			 END
		 END
	 
	--	 SET @JSONFILE =@JSONFILE+@JSON
			--SET @FILEEXISTS =0
			--SELECT @FILEEXISTS =DBO.ACI_FN_FILEEXISTS(@FILEPATH+'\'+@FILENAME)
			--IF @FILEEXISTS >0 --IF FILE EXISTS IT THEN ONLY IT WILL CHANGE THE STATUS.

		--  BEGIN
			UPDATE ACI_JSON_MASTER SET STATUS='C', STATUSCHANGEDT=GETDATE() ,BATCHNO=@BATCHNO WHERE JSON IS NOT NULL -- AND ISVALIDJSON ='T' and 
			and STATUS='N' AND ACIINPUTLOGKEY =@ACIINPUTLOGKEY AND  TABLEKEY= @TABLEKEY 
			and ACIJSONMASTERKEY =@ACIJSONMASTERKEY

	--	  END
	FETCH NEXT FROM CURSORA INTO @ACIINPUTLOGKEY,@TABLEKEY,@JSON --,@TABLENAME
	END
	CLOSE CURSORA
	DEALLOCATE CURSORA

	/* ===PROCESS END===*/

		SET @JSONFILE =@JSONFILE+']'
		--CREATE FILE DIRCTORY
		DECLARE @PATH VARCHAR(100)
		SET @PATH =@FILEPATH
		EXEC MASTER.DBO.XP_CREATE_SUBDIR @PATH
		--WRITE TEXT INTO FILE
		SET @INITSTR = 'ECHO '+@JSONFILE+' >> '+@FILEPATH+'\'+@FILENAME
		EXEC XP_CMDSHELL @INITSTR,NO_OUTPUT

		SET @FILEEXISTS =0
		SELECT @FILEEXISTS =DBO.ACI_FN_FILEEXISTS(@FILEPATH+'\'+@FILENAME)
		IF @FILEEXISTS >0 --IF FILE EXISTS IT THEN ONLY IT WILL CHANGE THE STATUS.
		BEGIN
		  IF @BATCHID >0 
		    UPDATE ACI_JSON_DEFAULTS SET BATCHNO =@BATCHID  ,BATCHDATE =@BATCHDT
		END
	/* ===PROCESS END===*/

END
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_Get_BatchID')) 
Drop Procedure ACI_Get_BatchID 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE PROCEDURE [dbo].[ACI_Get_BatchID]
AS
     BEGIN


         SELECT DISTINCT
                BatchID,
                jsonfilename
         FROM ACI_BATCH_JSON_RESPONSE(nolock)
         WHERE ACI_MessageStatus NOT IN(4, 5, 6, 7)
         OR ACI_MessageStatus IS NULL;
     END
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_Get_BatchID_Failed')) 
Drop Procedure ACI_Get_BatchID_Failed 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE PROCEDURE [dbo].[ACI_Get_BatchID_Failed]
AS
     BEGIN


         SELECT DISTINCT
                BatchID,
                jsonfilename
         FROM ACI_BATCH_JSON_RESPONSE(NOLOCK)
         WHERE ACI_MessageStatus = 4;
     END
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_GET_JSON_DEFAULTS')) 
Drop Procedure ACI_GET_JSON_DEFAULTS 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE PROCEDURE [dbo].[ACI_GET_JSON_DEFAULTS]
AS
BEGIN
    SELECT ACI_BaseURL,
        ACI_ServiceURL,
        ACI_ProductKey,
        ACI_BatchID,
        ACI_USERNAME,
        ACI_ROLE,
        ACI_PairKey,
        ACI_Dashboard_BaseUrl,
	   ACI_Issuer,
	   ACI_Audience,
	   ACI_Expires,
	   ACI_Baseurl_PublishDEP,
	   ACI_Baseurl_GetPublishDEPResult,
	   ACI_Baseurl_GetMessageDeliveryNotification,
	   ACI_Baseurl_RetriveInboundDEPData
    FROM ACI_JSON_DEFAULTS(NOLOCK);
END
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_GetStaffkey')) 
Drop Procedure ACI_GetStaffkey 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE PROCEDURE [dbo].[ACI_GetStaffkey] 
@staffkey VARCHAR(10)
AS
BEGIN
SELECT ISNULL(LOGINNAME,'')LOGINNAME FROM STAFF WHERE STAFFKEY = @staffkey
END
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_JSON29_DEP_GETReferenceNumber')) 
Drop Procedure ACI_JSON29_DEP_GETReferenceNumber 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE proc [dbo].[ACI_JSON29_DEP_GETReferenceNumber] (@AciDepKey INT

)
as 
begin

    declare @aciinputlogkey1 int, @tableKey INT, @ACIJSONMasterKey int, @chartRecordKey int
    set @chartRecordKey =0
    declare @ReferenceNumber varchar(500) 
    set @ReferenceNumber =''

    SELECT TOP 1 @CHARTRECORDKEY = CHARTRECORDKEY FROM ACI_DEP (NOLOCK) WHERE ACIDEPKEY = @ACIDEPKEY

	if @AciDepKey >0 and @CHARTRECORDKEY>0
	begin

	   declare @PhyId int , @FacId int ,@PATIENTKEY1 int
	   set @PhyId =0
	   set @FacId =0

	   Select @PhyId=D.ProviderKey,@FacId=R.PracticeId,@PATIENTKEY1=D.PatientKey 
	   From ACI_DEP D join dbo.Resources R on R.ResourceId = D.ProviderKey
	   Where AciDepKey = @AciDepKey
	   
	   
	   set @ReferenceNumber =''
	   select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.'+CAST(@FacId as varchar(10))+'.'+CAST(@PhyId as varchar(10))+'.'+CAST(@PATIENTKEY1 as varchar(10))+'.29.' from aci_json_Defaults (NOLOCK)
    end
    select @ReferenceNumber+Cast(@AciDepKey as varchar(10)) as ReferenceNumber
end
Go




IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_INSERT_GetResult')) 
Drop Procedure ACI_INSERT_GetResult 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE PROCEDURE [dbo].[ACI_INSERT_GetResult] 
(
  @BatchID int, 
  @BatchNo varchar(100),
  @COMPLETERESPONSE VARCHAR(max) ,
    
  @PR_ResponseCode VARCHAR(200) , 
  @PR_ResponseMessage  VARCHAR(200) ,   
  @Result VARCHAR(100) , 
  @R_BatchId VARCHAR(200) , 
  @R_ResponseCode  VARCHAR(200) , 
  @R_ResponseMessage  VARCHAR(500) ,   
  @ResourcesStatus Varchar(100) ,
  @RS_ResourceId varchar(100) ,
  @RS_StatusCode  varchar(100) ,
  @RS_Message varchar(200) 
) AS 
BEGIN
  
	declare @BatchNo1 varchar(100),@jsonfilename varchar(100) 
	set @BatchNo1=''
	set @jsonfilename=''
	if CHARINDEX('.json',@BatchNo) >0
	begin
	  set @BatchNo1 =replace(@BatchNo,'.json','')	 
	end
	else
	set @BatchNo1 =@BatchNo 

	if CHARINDEX('.json',@BatchNo) =0
	begin
	  set @jsonfilename = @BatchNo+'.json'
	end
	else
	  set @jsonfilename = @BatchNo

	update ACI_BATCH_JSON_RESPONSE set ACI_MessageStatus  =@R_ResponseCode where BatchID=@BatchID and jsonfilename =@jsonfilename

	update ACI_JSON_MASTER set ACI_MessageStatus  =@R_ResponseCode where  BatchNo =@BatchNo1

 --	update ACI_JSON_GetResult set status ='F' where status='T' and BatchID =@BatchID and BatchNo=@BatchNo1 

	INSERT INTO [dbo].[ACI_JSON_GetResult]
			([BatchID],[BatchNo],[COMPLETERESPONSE],[PR_ResponseCode],[PR_ResponseMessage],[Result],[R_BatchId],[R_ResponseCode],[R_ResponseMessage],[ResourcesStatus],[RS_ResourceId],[RS_StatusCode],[RS_Message],[status])
		VALUES(@BatchID,@BatchNo1,@COMPLETERESPONSE,@PR_ResponseCode,@PR_ResponseMessage,@Result,@R_BatchId,@R_ResponseCode,@R_ResponseMessage,@ResourcesStatus,@RS_ResourceId,@RS_StatusCode,@RS_Message,'T')  
END
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_INSERT_INPUTLOG')) 
Drop Procedure ACI_INSERT_INPUTLOG 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ACI_INSERT_INPUTLOG] (@TABLENAME VARCHAR(100), @TABLEKEY VARCHAR(200),@FIELDNAME VARCHAR(50), @FIELDKEY VARCHAR(100)) AS 
BEGIN

declare @Sort int

if @TABLENAME =''JSON01_FACILITYINFORMATION'' set @Sort =1 else 
if @TABLENAME =''JSON02_ELIGIBLECLINICIANINFORMATION'' set @Sort =2 else 
if @TABLENAME =''JSON03_ACIMODULEUSERACCOUNTADMINISTRATION'' set @Sort =3 else 
if @TABLENAME =''JSON04_PATIENTDEMOGRAPHICSINFORMATION'' set @Sort =4 else 
if @TABLENAME =''JSON05_REPRESENTATIVE'' set @Sort =5 else 
if @TABLENAME =''JSON9_PORTALACCOUNTADMINISTRATION'' set @Sort =6 else 
if @TABLENAME =''PATIENTEXAM'' set @Sort =7 else 
if @TABLENAME =''JSON10_CPOELABORATORYORIMAGING'' set @Sort =10 else 
if @TABLENAME =''JSON12_CLINICALDATARECONCILIATION'' set @Sort =12 else 
if @TABLENAME =''JSON12_CLINICALDATARECONCILIATION_PGHD'' set @Sort =12 else 
if @TABLENAME =''JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE'' set @Sort =13 else 
if @TABLENAME =''JSON14_PATIENTVDTEVENT'' set @Sort =14 else 
if @TABLENAME =''JSON15_CLAIMSUBMISSIONEVENT'' set @Sort =15 else 
if @TABLENAME =''JSON16_MEDICATIONSORDER'' set @Sort =16 else 
if @TABLENAME =''JSON21_CARECOORDINATIONORDER'' set @Sort =21 else 
if @TABLENAME =''JSON18_PATIENTSPECIFICEDUCATIONEVENT'' set @Sort =18 else 
if @TABLENAME =''JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT'' set @Sort =19 else 
if @TABLENAME =''JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT'' set @Sort =20 else 
if @TABLENAME =''JSON17_SENTSUMMARYOFCAREEVENT'' set @Sort =17 else 
--REV1.1
if @TABLENAME =''JSON22_SPECIALISTREPORTRECEIVEDEVENT'' set @Sort =22 else 
if @TABLENAME =''JSON23_DRCOMMUNICATIONEVENT'' set @Sort =23 else 
if @TABLENAME =''JSON24_SURGICALPREOPERATIVESTATUSEVENT'' set @Sort =24 else 
if @TABLENAME =''JSON25_SURGERYEVENT'' set @Sort =25 else 
if @TABLENAME =''JSON26_SURGICALPOSTOPERATIVERESULTEVENT'' set @Sort =26 else
if @TABLENAME =''JSON27_POSTSURGICALCOMPLICATIONEVENT'' set @Sort =27 else  
if @TABLENAME =''JSON28_RESULTS'' set @Sort =28 else  
--REV1.1
set @Sort =999

  

  IF Len(LTRIM(RTRIM(@TABLEKEY))) > 0  
    IF (SELECT COUNT(FIELDKEY) FROM ACI_INPUTLOG (NOLOCK) WHERE TABLENAME=@TABLENAME AND TABLEKEY=@TABLEKEY AND FIELDNAME=@FIELDNAME 
          AND STATUS=''N'') =0--WON''T ALLOW DUPLICATE 		  
    INSERT INTO ACI_INPUTLOG(TABLENAME,TABLEKEY,FIELDNAME,FIELDKEY,Sort)
               VALUES(@TABLENAME,@TABLEKEY,@FIELDNAME,@FIELDKEY,@Sort)
	

END


' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_INSERT_JSON_RESPONSE')) 
Drop Procedure ACI_INSERT_JSON_RESPONSE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE PROCEDURE [dbo].[ACI_INSERT_JSON_RESPONSE] @JSONFILENAME     VARCHAR(200),
                                                  @RESPONSECODE     VARCHAR(200),
                                                  @RESPONSEMESSAGE  VARCHAR(500),
                                                  @COMPLETERESPONSE VARCHAR(2000),
                                                  @BATCHID          INT
AS
     BEGIN
--SRAVAN , 06/19/2017
         INSERT INTO ACI_BATCH_JSON_RESPONSE
         (JSONFILENAME,
          RESPONSECODE,
          RESPONSEMESSAGE,
          COMPLETERESPONSE,
          BATCHID
         )
         VALUES
         (@JSONFILENAME,
          @RESPONSECODE,
          @RESPONSEMESSAGE,
          @COMPLETERESPONSE,
          @BATCHID
         );
         DECLARE @BATCHNO VARCHAR(100);
         SET @BATCHNO = '';
         SET @BATCHNO = REPLACE(UPPER(@JSONFILENAME), '.JSON', '');
         IF @BATCHNO <> ''
             BEGIN
                 UPDATE ACI_JSON_MASTER
                   SET
                       ACI_SUBMITDT = GETDATE(),
                       STATUS = 'C',
                       STATUSCHANGEDT = GETDATE()
                 WHERE BATCHNO = @BATCHNO;
                 UPDATE ACI_JSON_DEFAULTS
                   SET
                       ACI_BATCHID = ISNULL(ACI_BATCHID, 0) + 1;
         END;
     END
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_SP_JSON4_PATIENTDEMOGRAPHICSINFORMATION_RACE')) 
Drop Procedure ACI_SP_JSON4_PATIENTDEMOGRAPHICSINFORMATION_RACE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE PROCEDURE [dbo].[ACI_SP_JSON4_PATIENTDEMOGRAPHICSINFORMATION_RACE] @PATIENTKEY INT
AS
     BEGIN
         DECLARE @RACE VARCHAR(20), @SQL_RACE VARCHAR(500);
         SELECT TOP 1 @RACE = case when isnull(RACE1,'') ='' Then '6' else RACE1 end
         FROM PATIENT(NOLOCK)
         WHERE PATIENTKEY = @PATIENTKEY;
         IF RIGHT(@RACE, 1) = ','
             SET @RACE = @RACE+'0';
         --print @RACE
         SET @SQL_RACE = 'SELECT 			
			 DRACE.DATA AS RACECODE,
			 DRACE.DESCRIPTION1 AS RACEDESCRIPTION,
			 DRACE.CODE AS RACECODESYSTEMNAME,
			 DRACE.CODESYSTEM AS RACECODESYSTEM			 
	FROM  
	DROPDOWN DRACE (NOLOCK) WHERE DRACE.TABLENAME = ''PATIENTINFO'' AND DRACE.FIELDNAME = ''RACE'' AND   DATA IN ('+@RACE+')';
         EXEC (@SQL_RACE);



     END
Go

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_Check_ISVALIDJSON_WithParam')) 
Drop Procedure ACI_Check_ISVALIDJSON_WithParam 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[ACI_Check_ISVALIDJSON_WithParam] @_jsonNumber nvarchar(100), @_externalId nvarchar(100) 
As 
Begin
 	DECLARE @BATCHNO VARCHAR(20),@BATCHNO_PRIOR VARCHAR(9),@BATCHID INT,@BATCHDT VARCHAR(8)
	SET @BATCHNO=''''
	SET @BATCHID=1
	SET @BATCHDT=CONVERT(VARCHAR, GETDATE(), 112)

	UPDATE ACI_JSON_DEFAULTS SET BATCHDATE =@BATCHDT WHERE BATCHDATE IS NULL

	UPDATE ACI_JSON_DEFAULTS SET BATCHNO=0 ,BATCHDATE = @BATCHDT WHERE BATCHDATE <> @BATCHDT

	SELECT @BATCHNO_PRIOR=(ISNULL(ACI_VendorId,''1000'')+MDO_PracticeId)  FROM ACI_JSON_DEFAULTS

	SELECT @BATCHID=(ISNULL(BATCHNO,0)+1) FROM ACI_JSON_DEFAULTS WHERE BATCHDATE =@BATCHDT

	SET @BATCHNO=@BATCHNO_PRIOR+@BATCHDT+CAST(@BATCHID AS VARCHAR(20))

   IF @_jsonNumber <> '''' and  @_externalId <> ''''
	Begin
	 Select top(select isnull(records,100) from aci_JSON_defaults with (nolock)) ACIJSONMASTERKEY,
	   CASE WHEN CHARINDEX(''"RESOURCETYPE":"PATIENTEXAM"'',ISNULL(JSON,'''')) >0 
	   THEN
    		ISNULL(JSON,'''')+ISNULL(ENCOUNTER,'''')+ISNULL(InformantRelated,'''')+ISNULL(ALLERGIES,'''')+ISNULL(FAMILYHISTORY,'''')+
			ISNULL(FUNCTIONALSTATUS,'''')+ISNULL(IMMUNIZATION,'''')+ISNULL(MEDICALEQUIPMENT,'''')+
			ISNULL(MEDICATION,'''')+ISNULL(PAYER,'''')+ISNULL(PLANOFTREATMENT,'''')+ISNULL(PROBLEMSLIST,'''')+
			ISNULL(PROCEDURES,'''')+isnull(RESULT,'''')+isnull(SOCIALHISTORY,'''')+ISNULL(VITALSIGN,'''')+
			ISNULL(INSTRUCTIONS,'''')+ISNULL(CARETEAM,'''')+ISNULL(PHYSICALEXAM,'''')
			+''}''
	  ELSE
    		ISNULL(JSON,'''')+ISNULL(ENCOUNTER,'''')+ISNULL(ALLERGIES,'''')+ISNULL(FAMILYHISTORY,'''')+
			ISNULL(FUNCTIONALSTATUS,'''')+ISNULL(IMMUNIZATION,'''')+ISNULL(MEDICALEQUIPMENT,'''')+
			ISNULL(MEDICATION,'''')+ISNULL(PAYER,'''')+ISNULL(PLANOFTREATMENT,'''')+ISNULL(PROBLEMSLIST,'''')+
			ISNULL(PROCEDURES,'''')+isnull(RESULT,'''')+isnull(SOCIALHISTORY,'''')+ISNULL(VITALSIGN,'''')+
			ISNULL(INSTRUCTIONS,'''')+ISNULL(CARETEAM,'''')
		
	  END AS JSONNEW, @BATCHNO as BATCHNO from dbo.ACI_JSON_MASTER a with (NOLOCK) Inner Join dbo.ACI_INPUTLOG b on a.TABLEKEY = b.TABLEKEY and a.ACIINPUTLOGKEY = b.ACIINPUTLOGKEY		  WHERE ISNULL(a.JSON,'''') <>'''' and a.Status =''N'' and b.STATUS = ''C'' and a.ACI_SubmitDT is null and b.TABLEKEY = @_externalId and b.TABLENAME = @_jsonNumber
	  
	End
   
  	IF @BATCHID >0 
	   Begin
		  UPDATE ACI_JSON_DEFAULTS SET BATCHNO =@BATCHID  ,BATCHDATE =@BATCHDT
       End
End
' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_DEP_INBOUNDRESPONSE')) 
Drop Procedure ACI_DEP_INBOUNDRESPONSE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE PROCEDURE [dbo].[ACI_DEP_INBOUNDRESPONSE]
@ACIDEPKEYID  INT,
@staffkey   INT,
@status            varchar(25),
@audittrackingid int,
@statuscode       int,
@description        varchar(max),
@Resourcetype  Varchar(20),
@ExternalID  Varchar(20),
@ReferenceNumber  Varchar(200),
@Source  Varchar(200),
@ExternalReferenceId  Varchar(200),
@PatientAccountNumber  Varchar(20),
@EncounterId  Varchar(100),
@ClinicalDocumentType Varchar(100),
@DEPRequestDate datetime,
@OrderingECNPI  Varchar(MAX),
@AT_ContentDisposition VARCHAR(100),
@AT_ContentType VARCHAR(100),
@AT_FileName VARCHAR(MAX),
@AT_Content  VARCHAR(MAX),
@Filepath varchar(500),
@FromProviderAddress VARCHAR(1000),
@FromAddressPassword VARCHAR(100),
@ToProviderAddress  VARCHAR(1000),
@HISP_Type  VARCHAR(50),
@TransactionType VARCHAR(100),
@MessageSubject  VARCHAR(1000),
@MessageBody  VARCHAR(MAX)
--@StatusOfMSG_ControlID int OUTPUT  
AS
declare @acidepkey_id int,@TABLEKEY INT
BEGIN
Set @acidepkey_id = 0--ID FROM ACI_DEP_JSON_RESPONSE WHERE [API.NO]=3 AND Status = 'Success' and ACIDEPKEY = @ACIDEPKEYID
--IF NOT EXISTS (select ReferenceNumber,ToProviderAddress from ACI_DEP_Inbound where ReferenceNumber = @ReferenceNumber and ToProviderAddress = @ToProviderAddress)
if (select count(*) from  ACI_DEP_Inbound where  AT_FileName =@AT_FileName and ExternalId =@ExternalID) =0
BEGIN
if @status ='NULL'
   set @status =''

if @description ='NULL'
   set @description=''

if @Resourcetype='NULL'
   set @Resourcetype =''

if @ExternalID ='NULL'
   set @ExternalID='0'

if @ReferenceNumber =NULL
   set @ReferenceNumber=''

if @Source='NULL'
  set @Source=''

if @ExternalReferenceId='NULL'
  set @ExternalReferenceId=''

if @PatientAccountNumber='NULL'
   set @PatientAccountNumber=''

if @EncounterId ='NULL'
  set @EncounterId='0'

if @ClinicalDocumentType='NULL'
  set @ClinicalDocumentType=''

if @OrderingECNPI ='NULL'
  set @OrderingECNPI=''

if @AT_ContentDisposition='NULL'
  set @AT_ContentDisposition=''

if @AT_ContentType ='NULL'
  set @AT_ContentType=''

if @AT_FileName ='NULL'
  set @AT_FileName=''

if @AT_Content='NULL'
  set @AT_Content=''

if @Filepath='NULL'
  set @Filepath=''

if @FromProviderAddress ='NULL'
  set @FromProviderAddress=''

if @FromAddressPassword ='NULL'
  set @FromAddressPassword =''

if @ToProviderAddress='NULL'
   set @ToProviderAddress=''

if @HISP_Type ='NULL'
   set @HISP_Type=''

if @TransactionType ='NULL'
  set  @TransactionType=''

if @MessageSubject='NULL'
  set @MessageSubject=''

if @MessageBody='NULL'
  set @MessageBody=''

INSERT INTO ACI_DEP_Inbound(AciDepResponseID,ResourceType,ExternalId,ReferenceNumber,[Source],ExternalReferenceId,PatientAccountNumber,
EncounterId,ClinicalDocumentType,DEPRequestDate,OrderingECNPI,AT_ContentDisposition,AT_ContentType,AT_FileName,AT_Content,filepath,FromProviderAddress,
FromAddressPassword,ToProviderAddress,HISP_Type,TransactionType,MessageSubject,MessageBody,[STATUS],AUDITTRACKINGID,STATUSCODE,[DESCRIPTION],StaffKey) Values
(@acidepkey_id,          
 @Resourcetype,        
 @ExternalID,           
 @ReferenceNumber,      
 @Source,               
 @ExternalReferenceId,
 @PatientAccountNumber, 
 @EncounterId,          
 @ClinicalDocumentType,
 @DEPRequestDate,       
 @OrderingECNPI,        
 @AT_ContentDisposition,
 @AT_ContentType,       
 @AT_FileName,          
 @AT_Content,
 @Filepath,          
 @FromProviderAddress,  
 @FromAddressPassword,  
 @ToProviderAddress,    
 @HISP_Type,            
 @TransactionType,
 @MessageSubject,       
 @MessageBody,
 @status,          
 @audittrackingid,
 @statuscode,      
 @description,
 @staffkey)
 SET @TABLEKEY=IDENT_CURRENT('aci_dep_inbound')
 SELECT @TABLEKEY  AS id
 END
 ELSE
 SELECT 0 AS id 
 END 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON15_CLAIMSUBMISSIONEVENT')) 
Drop Procedure ACI_ConvertToJson_JSON15_CLAIMSUBMISSIONEVENT 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE proc [dbo].[ACI_ConvertToJson_JSON15_CLAIMSUBMISSIONEVENT] (@CLAIMKEY INT,@aciinputlogkey int)
as 
begin


  declare @StringJson varchar(8000)
  declare @aciinputlogkey1 int ,@tableKey INT, @ACIJSONMasterKey int
  set @aciinputlogkey1 =0
  set @tableKey=0
  set @ACIJSONMasterKey =0
  set @tableKey=@CLAIMKEY
  set @StringJson=''

	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status='N' and  c.TableName = 'JSON15_CLAIMSUBMISSIONEVENT' 
	where m.status ='N' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc


	if @aciinputlogkey1 =0
	begin
		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@PatId INT,@Schdkey INT
		set @PhyId =0
		set @FacId =0		
		set @PatId =0	
		set @Schdkey=0

		select top 1 @PhyId=SCHEDULE.ResourceID1,@FacId=Resources.PracticeID,@PatId=c.LegacyPatientReceivablesPatientId,@Schdkey=Schedule.appointmentid 
		from model.invoices c (nolock)
		 JOIN dbo.appointments SCHEDULE WITH (NOLOCK) ON SCHEDULE.appointmentid = C.EncounterID
		 join dbo.resources Resources with (NOLOCK) ON Resources.ResourceID=schedule.ResourceId1
		   where id =@CLAIMKEY		 
		
		set @ReferenceNumber =''
		select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.'+CAST(@FacId as varchar(10))+'.'+CAST(@PhyId as varchar(10))+'.'+CAST(@PatId as varchar(10))+'.15.' from aci_json_Defaults (NOLOCK)

		Declare @ReferenceNumberT varchar(50)
		set @ReferenceNumberT =''
		set @ReferenceNumberT =@ReferenceNumber+cast(@Schdkey as varchar(10))


        select @StringJson=@StringJson+ isnull(
            REPLACE(REPLACE(
					dbo.aci_qfn_XmlToJson (
					(     
					SELECT
						ISNULL(ResourceType,'') as ResourceType,--R
						ISNULL(IsSource,'') as IsSource,--R
						(isnull(@ReferenceNumber,'')+cast(ReferenceNumber as varchar(30))) as ReferenceNumber,--R
						ISNULL(PatientAccountNumber,'') as PatientAccountNumber,--R
						ISNULL(EncounterID,0) as EncounterID,--R
						ISNULL(EncounterDate,'') as EncounterDate,--R
						ISNULL(ECNPI,'') as ECNPI,--R
						--ISNULL(ECTIN,'') as ECTIN--R	
						(SUBSTRING(ECTIN,1,3)+'-'+SUBSTRING(ECTIN,4,2)+'-'+SUBSTRING(ECTIN,6,4))  as ECTIN					
					from VIEW_ACI_JSON15_CLAIMSUBMISSIONEVENT L where l.InvoiceID = @CLAIMKEY for xml path('PMSCLAIMS'),type) 
            ),'[','') ,'}]','')   ,'')    

		select @StringJson=@StringJson+', "BillingCode":'+ isnull( REPLACE(REPLACE(dbo.aci_qfn_XmlToJson (
		(
		select 
			ISNULL(ExternalId,'') AS ExternalId,--R
			ISNULL(BillingCodeValue,'00000') as BillingCodeValue,--R
			ISNULL(BillingCodeDescription,'NOT AVAILABLE') as BillingCodeDescription,--R
			ISNULL(BillingCodeSystem,'') AS BillingCodeSystem,--R
			ISNULL(BillingCodeName,'') as BillingCodeName--R
			
		from VIEW_ACI_JSON15_CLAIMSUBMISSIONEVENT_BILLINGCODE a where  a.InvoiceID = @CLAIMKEY for xml path('BillingCode1'),type) 
		   ),'','') ,'',''),'')
		   +'}'



      if ltrim(rTrim(@StringJson)) <> ''
	  begin
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json, ReferenceNumber,Sort) 
                 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))),@ReferenceNumberT,15)  

		set @ACIJSONMasterKey=IDENT_CURRENT('ACI_JSON_Master')
		update ACI_InPutLog set status ='C',statuschangedt=GETDATE() where status ='N' and TableName ='JSON15_CLAIMSUBMISSIONEVENT'
		 and( tableKey=@tableKey and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where status='N' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
  end
end
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION_PGHD')) 
Drop Procedure ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION_PGHD 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

--EXEC [dbo].[ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION] 67919, 7, ''
EXEC dbo.sp_executesql @statement = N'

--EXEC [dbo].[ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION] 67919, 7, ''''
CREATE PROC [dbo].[ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION_PGHD]
(@EXTMEDRECSOURCEKEY VARCHAR(100),
 @aciinputlogkey     INT,
 @Type               VARCHAR(20)
)
AS
     BEGIN
         DECLARE @StringJson VARCHAR(8000);
         DECLARE @aciinputlogkey1 INT, @tableKey VARCHAR(100), @ACIJSONMasterKey INT;
         SET @aciinputlogkey1 = 0;
         SET @tableKey = '''';
         SET @ACIJSONMasterKey = 0;
         SET @tableKey = @EXTMEDRECSOURCEKEY;
         SET @StringJson = '''';
         SELECT TOP 1 @aciinputlogkey1 = m.aciinputlogkey
         FROM ACI_JSON_Master m(nolock)
              JOIN ACI_InPutLog c(nolock) ON c.aciinputlogkey = m.aciinputlogkey
                                             AND c.status = ''N''
                                             AND c.TableName = ''JSON12_CLINICALDATARECONCILIATION_PGHD''
         WHERE m.status = ''N''
               AND c.aciinputlogkey = @ACIInPutLogKey
         ORDER BY m.ACIJSONMasterKey DESC;
         IF @aciinputlogkey1 = 0
             BEGIN
                 DECLARE @ReferenceNumber VARCHAR(50), @PhyId INT, @PatId INT, @FacId INT;
  

                         SET @PhyId = 0;
                         SET @PatId = 0;
                         SET @FacId = 0;
                         
				select top 1 @PhyId=r.ResourceId,@FacId=r.PracticeId ,@PatId=pc.PatientId  
			FROM [MVE].[PP_PatientExternalLink] PC WITH(NOLOCK) LEFT JOIN DBO.RESOURCES R WITH(NOLOCK) ON R.RESOURCEID = PC.USERID
			WHERE pc.id=@EXTMEDRECSOURCEKEY

         
                       
				SET @ReferenceNumber = '''';
                SELECT @ReferenceNumber = aci_vendorID+''.''+MDO_PracticeId+''.''+CAST(@FacId AS VARCHAR(10))+''.''+CAST(@PhyId AS VARCHAR(10))+''.''+CAST(@PatId AS VARCHAR(10))+''.12.''
                FROM aci_json_Defaults(NOLOCK); 
						                      
					     DECLARE @ReferenceNumberT VARCHAR(50);
                         SET @ReferenceNumberT = '''';
                         SET @ReferenceNumberT = @ReferenceNumber + CAST(@EXTMEDRECSOURCEKEY AS VARCHAR(10))+''_PGHD'';
                         IF
                         (
                             SELECT COUNT(*)
                             FROM VIEW_ACI_JSON12_CLINICALDATARECONCILIATION_PGHD
                         ) > 0
                             SELECT @StringJson = @StringJson+REPLACE(REPLACE(dbo.aci_qfn_XmlToJson
                                                                             (
                                                                             (
                                                                                 SELECT ''ReconcilationOrIncorporation'' AS ResourceType,
                                                                                        ISNULL(ExternalId, '''') AS ExternalId,
                                                                                        ISNULL(IsSource, '''') AS IsSource,
                                                                                        (@ReferenceNumberT +CAST(ReferenceNumber AS VARCHAR(10))) AS ReferenceNumber,
																					   -- '''' AS ReferenceNumber,
                                                                                        ISNULL(PatientAccountNumber, '''') AS PatientAccountNumber,
                                                                                        ISNULL(FileReceivedDate, '''') AS FileReceivedDate,
                                                                                        ISNULL(ReconcileFiletype, '''') AS ReconcileFiletype,
                                                                                        ISNULL(ReconcileDate, '''') AS ReconcileDate,
                                                                                        ISNULL(MedicationReconciled, '''') AS MedicationReconciled,
                                                                                        ISNULL(AllergiesReconciled, '''') AS AllergiesReconciled,
                                                                                        ISNULL(ProblemsReconciled, '''') AS ProblemsReconciled,
                                                                                        ISNULL(IsSOCIncorporated, '''') AS IsSOCIncorporated,
                                                                                        ISNULL(ECNPI, '''') AS ECNPI,
																						--ISNULL(ECTIN,'''') AS ECTIN
                                                                                        (SUBSTRING(TIN, 1, 3)+''-''+SUBSTRING(TIN, 4, 2)+''-''+SUBSTRING(TIN, 6, 4)) AS TIN
                                                                                 FROM VIEW_ACI_JSON12_CLINICALDATARECONCILIATION_PGHD L
                                                                                 WHERE L.ID = @EXTMEDRECSOURCEKEY FOR XML PATH(''RECONCILATIONORINCORPORATION''), TYPE
                                                                             )
                                                                             ), ''['', ''''), ''}]'', ''}'');
                         IF LTRIM(RTRIM(@StringJson)) <> ''''
                             BEGIN
                                 INSERT INTO ACI_JSON_Master
                                 (ACIInPutLogKey,
                                  tablekey,
                                  json,
                                  ReferenceNumber,
                                  Sort
                                 )
                                 VALUES
                                 (@aciinputlogkey,
                                  @tableKey,
                                  (LTRIM(RTRIM(@StringJson))),
                                  @ReferenceNumberT,
                                  12
                                 );
                                 SET @ACIJSONMasterKey = IDENT_CURRENT(''ACI_JSON_Master'');
								 Print ''Ayush'';
                                 UPDATE ACI_InPutLog
                                   SET
                                       status = ''C'',
                                       statuschangedt = GETDATE()
                                 WHERE status = ''N''
                                       AND TableName = ''JSON12_CLINICALDATARECONCILIATION_PGHD''
                                       AND (tableKey = @tableKey
                                            AND tablekey IN
                                           (
                                               SELECT tablekey
                                               FROM ACI_JSON_Master(nolock)
                                               WHERE status = ''N''
                                                     AND ACIJSONMasterKey = @ACIJSONMasterKey
                                           ));
                       
                 END;
	 
         END;
     END;


' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON10_CPOELABORATORYORIMAGING')) 
Drop Procedure ACI_ConvertToJson_JSON10_CPOELABORATORYORIMAGING 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE proc[dbo].[ACI_ConvertToJson_JSON10_CPOELABORATORYORIMAGING](@Patientorderkey varchar(30), @aciinputlogkey int)
as
begin

declare @StringJson_CPOE varchar(8000)
declare @aciinputlogkey1 int, @tableKey INT, @ACIJSONMasterKey int,@Patorderkey int, @OrderType varchar(10), @Patientorderkey1 varchar(20)
set @Patorderkey=0
set @aciinputlogkey1 = 0
set @tableKey = 0
set @ACIJSONMasterKey = 0
set @StringJson_CPOE = ''
set @OrderType = RIGHT(@Patientorderkey,3)
set @Patientorderkey1 = ''
set @Patientorderkey1 = @Patientorderkey
print @Patientorderkey1
set @Patientorderkey = LEFT(@Patientorderkey, LEN(@Patientorderkey)-4)
set @tableKey = @Patientorderkey


print @Patientorderkey
Print 1
select top 1 @aciinputlogkey1 = m.aciinputlogkey from ACI_JSON_Master m(nolock)
join ACI_InPutLog c (nolock) on c.aciinputlogkey = m.aciinputlogkey and c.status = 'N' and  c.TableName = 'JSON10_CPOELABORATORYORIMAGING'
where m.status = 'N'
and c.aciinputlogkey = @ACIInPutLogKey order by m.ACIJSONMasterKey desc
Print 2
Print @aciinputlogkey1
	if @aciinputlogkey1 = 0
	begin

		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int,@CHARTRECORDKEY1 INT ,@PatId INT
		set @PhyId =0
		set @FacId =0
		set @CHARTRECORDKEY1=0
		set @PatId =0
		Print 3
		IF (@OrderType = 'LAB')
		BEGIN			
			select top 1 @PhyId=s.resourceid1,@FacId=r.practiceid ,@PatId=c.patientid  from PatientLabOrders c (nolock)
			join dbo.appointments s (nolock) on s.appointmentid=c.appid
			join dbo.resources r (nolock) on r.resourceid=s.resourceid1
			where c.OrderId =@tableKey order by c.orderid desc
		END
		ELSE
		BEGIN		
			Print 4
			select top 1 @PhyId=s.resourceid1,@FacId=r.practiceid ,@PatId=c.patientid  from PatientImageOrders c (nolock)
			join dbo.appointments s (nolock) on s.appointmentid=c.AppointmentId
			join dbo.resources r (nolock) on r.resourceid=s.resourceid1
			where c.orderid =@tableKey order by c.orderid desc
						
		END
				
		set @ReferenceNumber =''
		select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.'+CAST(@FacId as varchar(10))+'.'+CAST(@PhyId as varchar(10))+'.'+CAST(@PatId as varchar(10))+'.10.' from aci_json_Defaults (NOLOCK)

		Declare @ReferenceNumberT varchar(50),@LabRadiologyOrder varchar(5000),@OrderCode varchar(5000)
		set @ReferenceNumberT =''
		set @LabRadiologyOrder =''
		set @OrderCode =''
		print @ReferenceNumber
		set @ReferenceNumberT =@ReferenceNumber+@Patientorderkey1
		print @ReferenceNumberT

		IF (@OrderType = 'LAB')
		BEGIN		
			IF (SELECT COUNT(*) FROM VIEW_ACI_JSON10_CPOELABORATORYORIMAGING where OrderID = @Patientorderkey)>0
			BEGIN
				SELECT @StringJson_CPOE= REPLACE(REPLACE( DBO.ACI_QFN_XMLTOJSON (
				(
				SELECT top 1
						ISNULL(ResourceType, '') AS ResourceType,
						ISNULL(IsSource, '') AS IsSource,
						(isnull(@ReferenceNumberT,'')) AS ReferenceNumber,
						ISNULL(PatientAccountNumber, '') AS PatientAccountNumber
				  from VIEW_ACI_JSON10_CPOELABORATORYORIMAGING where ORDERID = @Patientorderkey  FOR XML PATH('LabRadiologyOrder2'),TYPE ) ) ,'[','') ,'}]','')  
		     END
				  
			 IF ltrim(rTrim(@StringJson_CPOE)) <> ''
			   BEGIN
				  DECLARE CURSORA_CPOE SCROLL CURSOR FOR 
					SELECT top 1 OrderID from VIEW_ACI_JSON10_CPOELABORATORYORIMAGING (NOLOCK) where OrderID = @Patientorderkey 
				  OPEN CURSORA_CPOE   
				  FETCH FIRST FROM CURSORA_CPOE INTO @Patorderkey
				  WHILE @@FETCH_STATUS=0
				  BEGIN
				  set @LabRadiologyOrder=''
				  set @OrderCode =''
					 select  @LabRadiologyOrder= replace(--'}'
					 replace(--']'
					 replace --'['
					  (ISNULL( dbo.ACI_qfn_XmlToJson(	
							 (select top 1 
									ISNULL(ExternalId, '') as ExternalId,
									ISNULL(OrderType, '') as OrderType,
									ISNULL(Date, '') as Date,
									ISNULL(IsCPOE, '') IsCPOE,
									ISNULL(OrderingECNPI, '') as OrderingECNPI,								
									(SUBSTRING(OrderingECTIN,1,3)+'-'+SUBSTRING(OrderingECTIN,4,2)+'-'+SUBSTRING(OrderingECTIN,6,4)) as OrderingECTIN								 
									 from VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_LAB_ORDERCODE (NOLOCK) where OrderID = @Patorderkey for xml path('LabRadiologyOrder1'),type
								 ) 
							 )
						 ,'') 
						 ,'[','')
						 ,']','')
						 ,'}','')
						 --select @Route
						 if @LabRadiologyOrder<>''
							  select  @OrderCode= REPLACE(
							  REPLACE(
							  ISNULL( dbo.ACI_qfn_XmlToJson(	
							 (select top 1 
									 ISNULL(Code, '') as Code,
												ISNULL(Description, '') as Description,
												ISNULL(CodeSystemName, '') as CodeSystemName,
												rtrim(ltrim(ISNULL(CodeSystem, ''))) as CodeSystem
										 from VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_LABRADIOLOGYORDER_ORDERCODE (NOLOCK) where   OrderID = @Patorderkey  for xml path('OrderCode1'),type
								 ) 
							 )
							  ,'')
							  ,'[','')
							  ,']','')
						 if @OrderCode <>'' 
						   set @OrderCode =',"OrderCode": '+@OrderCode
					 
					

						 if @LabRadiologyOrder <>''
						 begin
	    					  set @LabRadiologyOrder =@LabRadiologyOrder+@OrderCode+'}'					 
						 end
	         
					FETCH NEXT FROM CURSORA_CPOE INTO @Patorderkey
				  END
				  CLOSE CURSORA_CPOE
				  DEALLOCATE CURSORA_CPOE
			   END		
				     
		END
		ELSE
		BEGIN
			IF (SELECT COUNT(*) FROM VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMG where OrderID = @Patientorderkey)>0
				SELECT @StringJson_CPOE= REPLACE(REPLACE( DBO.ACI_QFN_XMLTOJSON (
				(
				SELECT top 1
						ISNULL(ResourceType, '') AS ResourceType,
						ISNULL(IsSource, '') AS IsSource,
						(isnull(@ReferenceNumberT,'')) AS ReferenceNumber,
						ISNULL(PatientAccountNumber, '') AS PatientAccountNumber
				  from VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMG where ORDERID = @Patientorderkey  FOR XML PATH('LabRadiologyOrder2'),TYPE ) ) ,'[','') ,'}]','') 


				  IF ltrim(rTrim(@StringJson_CPOE)) <> ''
			   BEGIN
				  DECLARE CURSORA_CPOE SCROLL CURSOR FOR 
					SELECT top 1 OrderID from VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMG (NOLOCK) where OrderID = @Patientorderkey 
				  OPEN CURSORA_CPOE   
				  FETCH FIRST FROM CURSORA_CPOE INTO @Patorderkey
				  WHILE @@FETCH_STATUS=0
				  BEGIN
				  set @LabRadiologyOrder=''
				  set @OrderCode =''
					 select  @LabRadiologyOrder= replace(--'}'
					 replace(--']'
					 replace --'['
					  (ISNULL( dbo.ACI_qfn_XmlToJson(	
							 (select top 1 
									ISNULL(ExternalId, '') as ExternalId,
									ISNULL(OrderType, '') as OrderType,
									ISNULL(Date, '') as Date,
									ISNULL(IsCPOE, '') IsCPOE,
									ISNULL(OrderingECNPI, '') as OrderingECNPI,								
									(SUBSTRING(OrderingECTIN,1,3)+'-'+SUBSTRING(OrderingECTIN,4,2)+'-'+SUBSTRING(OrderingECTIN,6,4)) as OrderingECTIN								 
									 from VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMG_ORDERCODE (NOLOCK) where OrderID = @Patorderkey for xml path('LabRadiologyOrder1'),type
								 ) 
							 )
						 ,'') 
						 ,'[','')
						 ,']','')
						 ,'}','')
						 --select @Route
						 if @LabRadiologyOrder<>''
							  select  @OrderCode= REPLACE(
							  REPLACE(
							  ISNULL( dbo.ACI_qfn_XmlToJson(	
							 (select top 1 
									 ISNULL(Code, '') as Code,
												ISNULL(Description, '') as Description,
												ISNULL(CodeSystemName, '') as CodeSystemName,
												rtrim(ltrim(ISNULL(CodeSystem, ''))) as CodeSystem
										 from VIEW_ACI_JSON10_CPOELABORATORYORIMAGING_IMAGE_RADIOLOGYORDER (NOLOCK) where   OrderID = @Patorderkey  for xml path('OrderCode1'),type
								 ) 
							 )
							  ,'')
							  ,'[','')
							  ,']','')
						 if @OrderCode <>'' 
						   set @OrderCode =',"OrderCode": '+@OrderCode
					 
					

						 if @LabRadiologyOrder <>''
						 begin
	    					  set @LabRadiologyOrder =@LabRadiologyOrder+@OrderCode+'}'					 
						 end
	         
					FETCH NEXT FROM CURSORA_CPOE INTO @Patorderkey
				  END
				  CLOSE CURSORA_CPOE
				  DEALLOCATE CURSORA_CPOE
			   END	
		END				



			if ltrim(rTrim(@StringJson_CPOE)) <> ''
			begin
			  set @StringJson_CPOE =@StringJson_CPOE+',"LabRadiologyOrder": ['+@LabRadiologyOrder+']}'
			  Select @StringJson_CPOE
			  insert into ACI_JSON_Master(ACIInPutLogKey, tablekey, json,ReferenceNumber,Sort)
			                values(@aciinputlogkey, @Patientorderkey1, (ltrim(rTrim(@StringJson_CPOE))),@ReferenceNumberT,10)

			set @ACIJSONMasterKey = IDENT_CURRENT('ACI_JSON_Master')

			select * from aci_inputlog
			where status = 'N' and TableName ='JSON10_CPOELABORATORYORIMAGING' 
			and(tableKey = @Patientorderkey1 and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where isnull(JSON,'') <>'' and status='N' and ACIJSONMasterKey = @ACIJSONMasterKey))

		   	update ACI_InPutLog set status = 'C', statuschangedt = GETDATE() where status = 'N' and TableName ='JSON10_CPOELABORATORYORIMAGING' 
			and(tableKey = @Patientorderkey1 and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where isnull(JSON,'') <>'' and status='N' and ACIJSONMasterKey = @ACIJSONMasterKey))
			end


			
	end
end
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'AddUpdateACI_json_defaults')) 
Drop Procedure AddUpdateACI_json_defaults 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE PROCEDURE [dbo].[AddUpdateACI_json_defaults] (@ACI_VendorId int,@MDO_PracticeId int ,@ACI_BaseURL varchar(300),@ACI_ProductKey varchar(300),@ACI_PairKey varchar(2000),@ACI_USERNAME varchar(300),@ACI_ROLE varchar(300),@ACI_Dashboard_BaseUrl varchar(300),@ACI_Baseurl_PublishDEP varchar(300),@ACI_Baseurl_GetPublishDEPResult varchar(300),@ACI_Baseurl_GetMessageDeliveryNotification varchar(300),@ACI_Baseurl_RetriveInboundDEPData varchar(300))

AS BEGIN
	if (select count(*) from aci_json_defaults)  > 0 
		begin
			UPDATE aci_json_defaults  SET ACI_VendorId=@ACI_VendorId, MDO_PracticeId=@MDO_PracticeId,ACI_BaseURL=@ACI_BaseURL,ACI_ProductKey=@ACI_ProductKey,ACI_PairKey=@ACI_PairKey,ACI_USERNAME=@ACI_USERNAME,ACI_ROLE=@ACI_ROLE,ACI_Dashboard_BaseUrl=@ACI_Dashboard_BaseUrl,ACI_Baseurl_PublishDEP=@ACI_Baseurl_PublishDEP,ACI_Baseurl_GetPublishDEPResult=@ACI_Baseurl_GetPublishDEPResult,ACI_Baseurl_GetMessageDeliveryNotification=@ACI_Baseurl_GetMessageDeliveryNotification,ACI_Baseurl_RetriveInboundDEPData=@ACI_Baseurl_RetriveInboundDEPData--  WHERE 
		end 
	else
		begin
	--	insert into temp_aci_json_defaults() values ()
		INSERT INTO [dbo].[aci_json_defaults]
           ([ACI_VendorId]
           ,[MDO_PracticeId]
           ,[ACI_INTERVAL]
           ,[RECORDS]
           --,[SIZE]
           ,[ACI_BaseURL]
           ,[ACI_ServiceURL]
           ,[ACI_ProductKey]
           ,[BATCHNO]
           ,[BATCHDATE]
           ,[ACI_BatchID]
           ,[ACI_PairKey]
           ,[ACI_USERNAME]
           ,[ACI_ROLE]
           ,[ACI_Dashboard_BaseUrl]
           ,[ACI_Issuer]
           ,[ACI_Audience]
           ,[ACI_Expires]
           ,[ACI_Baseurl_PublishDEP]
           ,[ACI_Baseurl_GetPublishDEPResult]
           ,[ACI_Baseurl_GetMessageDeliveryNotification]
           ,[ACI_Baseurl_RetriveInboundDEPData])
     VALUES
           (@ACI_VendorId
           ,@MDO_PracticeId
           ,1
           ,1
           --,SIZE
           ,@ACI_BaseURL
           ,''--@ACI_ServiceURL
           ,@ACI_ProductKey
           ,15
           ,GETdate()
           ,0
           ,@ACI_PairKey
           ,@ACI_USERNAME
           ,@ACI_ROLE
           ,@ACI_Dashboard_BaseUrl
           ,''--@ACI_Issuer
           ,''
           ,10
           ,@ACI_Baseurl_PublishDEP
           ,@ACI_Baseurl_GetPublishDEPResult
           ,@ACI_Baseurl_GetMessageDeliveryNotification
           ,@ACI_Baseurl_RetriveInboundDEPData
		   )
		end 
END 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_INSERT_DEP_JSON_RESPONSE')) 
Drop Procedure ACI_INSERT_DEP_JSON_RESPONSE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE procedure [dbo].[ACI_INSERT_DEP_JSON_RESPONSE]
@STATUS   VARCHAR(25),
@auditTrackingID INT,
@STATUSCODE  INT,
@DESCRIPTION VARCHAR(MAX),
@ACIDEPKEY INT,
@Response varchar(MAX),
@StatusofResponse int,
@APINO INT,
@APICOMMENTS VARCHAR(100)

AS
BEGIN
IF(@StatusofResponse = 1)
   Begin
   INSERT INTO ACI_DEP_JSON_RESPONSE (ACIDEPKEY,[STATUS],AUDITTRACKINGID,STATUSCODE,[DESCRIPTION],Response,[API.NO],API_Comments) values(@ACIDEPKEY,@STATUS,@auditTrackingID,@STATUSCODE,@DESCRIPTION,@Response,@APINO,@APICOMMENTS)
   UPDATE ACI_DEP SET ACI_SubmitDT = GETDATE() WHERE AciDepKey = @ACIDEPKEY 
   END
ELSE IF (@StatusofResponse = 2)
   BEGIN
   INSERT INTO ACI_DEP_JSON_RESPONSE (ACIDEPKEY,[STATUS],AUDITTRACKINGID,STATUSCODE,[DESCRIPTION],Response,[API.NO],API_Comments) values(@ACIDEPKEY,@STATUS,@auditTrackingID,@STATUSCODE,@DESCRIPTION,@Response,@APINO,@APICOMMENTS)
   UPDATE ACI_DEP_JSON_RESPONSE SET API_Submitdate = GETDATE() WHERE AciDepKey = @ACIDEPKEY AND [API.NO] = 1
   END
ELSE IF (@StatusofResponse = 3)
   BEGIN
   INSERT INTO ACI_DEP_JSON_RESPONSE (ACIDEPKEY,[STATUS],AUDITTRACKINGID,STATUSCODE,[DESCRIPTION],Response,[API.NO],API_Comments) values(@ACIDEPKEY,@STATUS,@auditTrackingID,@STATUSCODE,@DESCRIPTION,@Response,@APINO,@APICOMMENTS)
   UPDATE ACI_DEP_JSON_RESPONSE SET API_Submitdate = GETDATE() WHERE AciDepKey = @ACIDEPKEY AND [API.NO] = 2
   END
ELSE IF (@StatusofResponse =4)
   BEGIN
   INSERT INTO ACI_DEP_JSON_RESPONSE (ACIDEPKEY,[STATUS],AUDITTRACKINGID,STATUSCODE,[DESCRIPTION],Response,[API.NO],API_Comments) values(@ACIDEPKEY,@STATUS,@auditTrackingID,@STATUSCODE,@DESCRIPTION,@Response,@APINO,@APICOMMENTS)
   UPDATE ACI_DEP_JSON_RESPONSE SET API_Submitdate = GETDATE() WHERE AciDepKey = @ACIDEPKEY AND [API.NO] = 3
   END
END
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_Check_ISVALIDJSON')) 
Drop Procedure ACI_Check_ISVALIDJSON 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[ACI_Check_ISVALIDJSON] @SortIns INT as 
Begin
 		DECLARE @BATCHNO VARCHAR(20),@BATCHNO_PRIOR VARCHAR(9),@BATCHID INT,@BATCHDT VARCHAR(8)
		SET @BATCHNO=''''
		SET @BATCHID=1
		SET @BATCHDT=CONVERT(VARCHAR, GETDATE(), 112)

		UPDATE ACI_JSON_DEFAULTS SET BATCHDATE =@BATCHDT WHERE BATCHDATE IS NULL

		UPDATE ACI_JSON_DEFAULTS SET BATCHNO=0 ,BATCHDATE = @BATCHDT WHERE BATCHDATE <> @BATCHDT

		SELECT @BATCHNO_PRIOR=(ISNULL(ACI_VendorId,''1000'')+MDO_PracticeId)  FROM ACI_JSON_DEFAULTS

		SELECT @BATCHID=(ISNULL(BATCHNO,0)+1) FROM ACI_JSON_DEFAULTS WHERE BATCHDATE =@BATCHDT

		SET @BATCHNO=@BATCHNO_PRIOR+@BATCHDT+CAST(@BATCHID AS VARCHAR(20))

   IF @SortIns >0 
	Begin
	 select   top (select isnull(records,100) from aci_JSON_defaults ) ACIJSONMASTERKEY,
	   CASE WHEN CHARINDEX(''"RESOURCETYPE":"PATIENTEXAM"'',ISNULL(JSON,'''')) >0 
	   THEN
    		ISNULL(JSON,'''')+ISNULL(ENCOUNTER,'''')+ISNULL(InformantRelated,'''')+ISNULL(ALLERGIES,'''')+ISNULL(FAMILYHISTORY,'''')+
			ISNULL(FUNCTIONALSTATUS,'''')+ISNULL(IMMUNIZATION,'''')+ISNULL(MEDICALEQUIPMENT,'''')+
			ISNULL(MEDICATION,'''')+ISNULL(PAYER,'''')+ISNULL(PLANOFTREATMENT,'''')+ISNULL(PROBLEMSLIST,'''')+
			ISNULL(PROCEDURES,'''')+isnull(RESULT,'''')+isnull(SOCIALHISTORY,'''')+ISNULL(VITALSIGN,'''')+
			ISNULL(INSTRUCTIONS,'''')+ISNULL(CARETEAM,'''')+ISNULL(PHYSICALEXAM,'''')
			+''}''
	  ELSE
    		ISNULL(JSON,'''')+ISNULL(ENCOUNTER,'''')+ISNULL(ALLERGIES,'''')+ISNULL(FAMILYHISTORY,'''')+
			ISNULL(FUNCTIONALSTATUS,'''')+ISNULL(IMMUNIZATION,'''')+ISNULL(MEDICALEQUIPMENT,'''')+
			ISNULL(MEDICATION,'''')+ISNULL(PAYER,'''')+ISNULL(PLANOFTREATMENT,'''')+ISNULL(PROBLEMSLIST,'''')+
			ISNULL(PROCEDURES,'''')+isnull(RESULT,'''')+isnull(SOCIALHISTORY,'''')+ISNULL(VITALSIGN,'''')+
			ISNULL(INSTRUCTIONS,'''')+ISNULL(CARETEAM,'''')
		
	  END AS JSONNEW, @BATCHNO as BATCHNO from ACI_JSON_MASTER (NOLOCK) WHERE ISNULL(JSON,'''') <>'''' and status =''N'' and ACI_SubmitDT is null
	  and sort =@SortIns order by Sort, ACIJSONMASTERKEY desc
	End
   Else
	Begin 
	 Select Top (select isnull(records,100) from aci_JSON_defaults ) ACIJSONMASTERKEY,
	   CASE WHEN CHARINDEX(''"RESOURCETYPE":"PATIENTEXAM"'',ISNULL(JSON,'''')) >0 
	   THEN
    		ISNULL(JSON,'''')+ISNULL(ENCOUNTER,'''')+ISNULL(ALLERGIES,'''')+ISNULL(FAMILYHISTORY,'''')+
			ISNULL(FUNCTIONALSTATUS,'''')+ISNULL(IMMUNIZATION,'''')+ISNULL(MEDICALEQUIPMENT,'''')+
			ISNULL(MEDICATION,'''')+ISNULL(PAYER,'''')+ISNULL(PLANOFTREATMENT,'''')+ISNULL(PROBLEMSLIST,'''')+
			ISNULL(PROCEDURES,'''')+isnull(RESULT,'''')+isnull(SOCIALHISTORY,'''')+ISNULL(VITALSIGN,'''')+
			ISNULL(INSTRUCTIONS,'''')+ISNULL(CARETEAM,'''')+ISNULL(InformantRelated,'''')+ISNULL(PHYSICALEXAM,'''')
			+''}''
	  ELSE
    		ISNULL(JSON,'''')+ISNULL(ENCOUNTER,'''')+ISNULL(ALLERGIES,'''')+ISNULL(FAMILYHISTORY,'''')+
			ISNULL(FUNCTIONALSTATUS,'''')+ISNULL(IMMUNIZATION,'''')+ISNULL(MEDICALEQUIPMENT,'''')+
			ISNULL(MEDICATION,'''')+ISNULL(PAYER,'''')+ISNULL(PLANOFTREATMENT,'''')+ISNULL(PROBLEMSLIST,'''')+
			ISNULL(PROCEDURES,'''')+isnull(RESULT,'''')+isnull(SOCIALHISTORY,'''')+ISNULL(VITALSIGN,'''')+
			ISNULL(INSTRUCTIONS,'''')+ISNULL(CARETEAM,'''')
	  END AS JSONNEW,@BATCHNO as BATCHNO
	  from ACI_JSON_MASTER (NOLOCK) WHERE ISNULL(JSON,'''') <>'''' and status =''N'' and ACI_SubmitDT is null
	  order by Sort,ACIJSONMASTERKEY desc
    End -- else 

  	IF @BATCHID >0 
		    UPDATE ACI_JSON_DEFAULTS SET BATCHNO =@BATCHID  ,BATCHDATE =@BATCHDT
End
' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON01_FacilityInformation')) 
Drop Procedure ACI_ConvertToJson_JSON01_FacilityInformation 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE proc [dbo].[ACI_ConvertToJson_JSON01_FacilityInformation] (@locationkey INT,@aciinputlogkey int)
as 
begin

/*                                                        *************************SCRIPT CASE SENSITIVE************  */
     
  declare @StringJson varchar(8000)
  declare @aciinputlogkey1 int ,@tableKey INT, @ACIJSONMasterKey int,@Phone Varchar(500)
  
  set @aciinputlogkey1 =0
  set @tableKey=0
  set @ACIJSONMasterKey =0
  set @tableKey=@locationkey
  set @StringJson=''
  set @Phone =''
    
	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status='N' and  c.TableName = 'JSON01_FACILITYINFORMATION'
	where m.status ='N' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 =0
	begin
	 declare @ReferenceNumber varchar(50)
	 set @ReferenceNumber =''
	 
	 select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.1.' from aci_json_Defaults (NOLOCK)

	 Declare @ReferenceNumberT varchar(50)
	 set @ReferenceNumberT =''
	 set @ReferenceNumberT =@ReferenceNumber+cast(@locationkey as varchar(10))

        select @StringJson=@StringJson+ 
            REPLACE(REPLACE(
					dbo.aci_qfn_XmlToJson (
					(     
					select ISNULL(ResourceType,'') AS ResourceType,(isnull(@ReferenceNumber,'')+ISNULL(ReferenceNumber,'')) AS ReferenceNumber,ISNULL(IsSource,'') AS IsSource,ISNULL(Name,'') AS Name,
							 ISNULL(CAST(Account AS VARCHAR(25)),'') AS Account,Status
					from VIEW_ACI_JSON01_FACILITYINFORMATION L where l.practiceid = @locationkey for xml path('location'),type) 
            ),'[','') ,'}]','')       


		--select @StringJson=@StringJson+',"Phone":'+  isnull(dbo.aci_qfn_XmlToJson (
		select @Phone=isnull(dbo.aci_qfn_XmlToJson (
		(
		select ISNULL(Type,'') AS Type,ISNULL(Number,'') AS Number
		   from VIEW_ACI_JSON01_FacilityInformation_phone p where p.practiceid=@locationkey  for xml path('phone'),type ) )   ,'')

       if @Phone <>''
	   select @StringJson=@StringJson+',"Phone":'+@Phone 

		select @StringJson=@StringJson+',"Address":'+isnull(  REPLACE(REPLACE(dbo.aci_qfn_XmlToJson (
		(
		select ISNULL(AddressLine1,'') AS AddressLine1,ISNULL(AddressLine2,'') AS AddressLine2,ISNULL(City,'') AS City,ISNULL(State,'') AS State ,ISNULL(Zip,'') AS Zip,
				  ISNULL(CAST(County AS VARCHAR(25)),'') AS County, ISNULL(CAST(CountyCode AS VARCHAR(25)),'') AS CountyCode
		   from VIEW_ACI_JSON01_FacilityInformation_Address a where  a.practiceid=@locationkey  for xml path('Address'),type) 
		   ),'[','') ,']',''),'')+'}'

      if (ltrim(rTrim(@StringJson)) <> '' and len(@StringJson) >10)
	  begin
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,ReferenceNumber,Sort) 
                 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))),@ReferenceNumberT,1)  

		set @ACIJSONMasterKey=IDENT_CURRENT('ACI_JSON_Master')
		update ACI_InPutLog set status ='C',statuschangedt=GETDATE() where status ='N'  and TableName ='JSON01_FACILITYINFORMATION' 
		and (tableKey=@tableKey and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where isnull(JSON,'') <>'' and status='N' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
  end
end
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON02_ELIGIBLECLINICIANINFORMATION')) 
Drop Procedure ACI_ConvertToJson_JSON02_ELIGIBLECLINICIANINFORMATION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE PROC [dbo].[ACI_ConvertToJson_JSON02_ELIGIBLECLINICIANINFORMATION] (@PROVIDERKEY INT,@ACIINPUTLOGKEY INT)
AS 
BEGIN
/*
ACI Version :Release 1.0
DATE:05/05/2017
Developer/Author:K.V.S.S.R.KRISHNA SARMA
DETAILS/DESCRIPTION: TO CREATE JSON STRING IN ACI_JSON_MASTER TABLE
 --THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/
/*                                                        *************************SCRIPT CASE SENSITIVE************  */

  DECLARE @STRINGJSON VARCHAR(8000)
  DECLARE @ACIINPUTLOGKEY1 INT ,@TABLEKEY INT, @ACIJSONMASTERKEY INT
  SET @ACIINPUTLOGKEY1 =0
  SET @TABLEKEY=0
  SET @ACIJSONMASTERKEY =0
  SET @TABLEKEY=@PROVIDERKEY
  SET @STRINGJSON=''''

	SELECT TOP 1 @ACIINPUTLOGKEY1=M.ACIINPUTLOGKEY   FROM ACI_JSON_MASTER M (NOLOCK)
	JOIN ACI_INPUTLOG C (NOLOCK) ON C.ACIINPUTLOGKEY=M.ACIINPUTLOGKEY AND C.STATUS=''N'' AND  C.TABLENAME = ''JSON02_ELIGIBLECLINICIANINFORMATION'' 
	WHERE M.STATUS =''N'' AND C.ACIINPUTLOGKEY=@ACIINPUTLOGKEY ORDER BY M.ACIJSONMASTERKEY DESC

	IF @ACIINPUTLOGKEY1 =0
	BEGIN
	 DECLARE @REFERENCENUMBER VARCHAR(50)
	 SET @REFERENCENUMBER =''''
	 SELECT @REFERENCENUMBER = ACI_VENDORID+''.''+MDO_PRACTICEID+''.2.'' FROM ACI_JSON_DEFAULTS (NOLOCK)

	 Declare @ReferenceNumberT varchar(50)
	 set @ReferenceNumberT =''''
	 set @ReferenceNumberT =@ReferenceNumber+cast(@providerkey as varchar(10))


		select @stringjson=@stringjson+ 
					replace(replace(
							dbo.aci_qfn_xmltojson (
							(     
							 select
									isnull(ResourceType,'''')  as ResourceType ,(isnull(@referencenumber,'''')+isnull(ReferenceNumber,''''))  as ReferenceNumber ,isnull(IsSource,'''')  as IsSource,
									isnull(NPI,'''')  as NPI ,
									(SUBSTRING(TIN,1,3)+''-''+SUBSTRING(TIN,4,2)+''-''+SUBSTRING(TIN,6,4))  as TIN
									,isnull(ECAccountNumber,'''')  as ECAccountNumber ,
									isnull(GivenName,'''')  as GivenName ,isnull(FamilyName,'''')  as FamilyName ,isnull(SecondandFurtherGivenNames,'''')  as SecondandFurtherGivenNames,
									isnull(DateOfBirth,'''')  as DateOfBirth,isnull(Prefix,'''')  as Prefix,isnull(Suffix,'''')  as Suffix ,isnull(NameTypeCode,'''')  as NameTypeCode ,
									isnull(NameTypeDescription,'''')  as NameTypeDescription,isnull(Status,'''')  as Status
								 from view_aci_json02_eligibleclinicianinformation p where p.ResourceID = @providerkey for xml path(''provider''),type
							) 
					),''['','''') ,''}]'','''')  +''}''    

      
	 if (ltrim(rTrim(@StringJson)) <> '''' and len(@StringJson) >10)
	  BEGIN
         INSERT INTO ACI_JSON_MASTER (ACIINPUTLOGKEY,TABLEKEY,JSON,ReferenceNumber,Sort) 
                 VALUES(@ACIINPUTLOGKEY,@TABLEKEY,(LTRIM(RTRIM(@STRINGJSON))),@ReferenceNumberT,2)  

		SET @ACIJSONMASTERKEY=IDENT_CURRENT(''ACI_JSON_MASTER'')
		UPDATE ACI_INPUTLOG SET STATUS =''C'',STATUSCHANGEDT=GETDATE() WHERE STATUS =''N'' AND TABLENAME =''JSON02_ELIGIBLECLINICIANINFORMATION'' 
		AND (TABLEKEY=@TABLEKEY AND
		     TABLEKEY IN(SELECT TABLEKEY FROM ACI_JSON_MASTER (NOLOCK) WHERE isnull(JSON,'''') <>'''' and STATUS=''N'' AND ACIJSONMASTERKEY=@ACIJSONMASTERKEY))
      END

   END--

END

' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON03_ACIModuleuseraccountadministration')) 
Drop Procedure ACI_ConvertToJson_JSON03_ACIModuleuseraccountadministration 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE proc [dbo].[ACI_ConvertToJson_JSON03_ACIModuleuseraccountadministration] (@StaffKey INT, @aciinputlogkey INT)
as 
begin

/*                                                        *************************SCRIPT CASE SENSITIVE************  */

  declare @StringJson varchar(8000)
  declare @aciinputlogkey1 int ,@tableKey INT, @ACIJSONMasterKey int
  set @aciinputlogkey1 =0
  set @tableKey=0
  set @ACIJSONMasterKey =0
  set @tableKey=@StaffKey
  set @StringJson=''

	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status='N' and  c.TableName = 'JSON03_ACIMODULEUSERACCOUNTADMINISTRATION' 
	where m.status ='N' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 =0
	begin
	 declare @ReferenceNumber varchar(50), @ProductKey varchar(100)
	 set @ReferenceNumber =''
	 set @ProductKey =''
	 select @ReferenceNumber = aci_vendorID+'.'+MDO_PracticeId+'.3.', @ProductKey =ACI_ProductKey from aci_json_Defaults (NOLOCK)

	 Declare @ReferenceNumberT varchar(50)
	 set @ReferenceNumberT =''
	 set @ReferenceNumberT =@ReferenceNumber+cast(@StaffKey as varchar(10))

		select @StringJson=@StringJson+ 
					REPLACE(REPLACE(
							dbo.aci_qfn_XmlToJson (
							(     
		select
				isnull(resourceType,'') as resourceType,	isnull(IsSource	,'') as IsSource,(isnull(@ReferenceNumber,'')+isnull(ReferenceNumber	,'')) as ReferenceNumber,
				isnull(@ProductKey	,'') as ProductKey,isnull(ExternalID	,'') as ExternalID,isnull(GivenName	,'') as GivenName,isnull(FamilyName	,'') as FamilyName,
				isnull(SecondandFurtherGivenNames	,'') as SecondandFurtherGivenNames,isnull(DateofBirth	,'') as DateofBirth,
				isnull(UserName	,'') as UserName,isnull(Role	,'') as Role,isnull(Active	,'') as Active
     
			 from View_ACI_JSON03_ACIModuleuseraccountadministration s where s.ResourceID = @StaffKey for xml path('USER'),type)
		
					),'[','') ,'}]','')       


		select @StringJson=@StringJson+',"AssociatedPhysician":'+  dbo.aci_qfn_XmlToJson (
		(
		select CASE
				WHEN Resourcetype in ('D','Z') THEN ECAccountNumber
				ELSE '' 
				END as ECAccountNumber 
		from View_ACI_JSON03_ACIModuleuseraccountadministration_AssociatedPhysician p where p.ResourceID =@StaffKey for xml path('AssociatedPhysician'),type) )   
					+'}'

      --if ltrim(rTrim(@StringJson)) <> ''
	 if (ltrim(rTrim(@StringJson)) <> '' and len(@StringJson) >10)
	  begin
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,ReferenceNumber,Sort) 
                 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))),@ReferenceNumberT,3)  

		 set @ACIJSONMasterKey=IDENT_CURRENT('ACI_JSON_Master')
		    update ACI_InPutLog set status ='C',statuschangedt=GETDATE() where status ='N' and TableName ='JSON03_ACIMODULEUSERACCOUNTADMINISTRATION' 
			and (tableKey=@tableKey and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where isnull(JSON,'') <>'' and status='N' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
   end--      
end
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON04_PATIENTDEMOGRAPHICSINFORMATION')) 
Drop Procedure ACI_ConvertToJson_JSON04_PATIENTDEMOGRAPHICSINFORMATION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'
CREATE proc [dbo].[ACI_ConvertToJson_JSON04_PATIENTDEMOGRAPHICSINFORMATION] (@PATIENTKEY INT,@aciinputlogkey int )
as 
begin
/*
ACI Version :Release 1.0
Date:05/05/2017
Developer/Author:K.V.S.S.R.KRISHNA SARMA
Details/Description: To create JSON String in ACI_JSON_MAster table
*/
/*
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/
  declare @StringJson varchar(8000)
  declare @aciinputlogkey1 int ,@tableKey INT, @ACIJSONMasterKey int
  set @aciinputlogkey1 =0
  set @tableKey=0
  set @ACIJSONMasterKey =0
  set @tableKey=@PATIENTKEY
  set @StringJson=''''

	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status=''N''  and  c.TableName = ''JSON04_PATIENTDEMOGRAPHICSINFORMATION'' 
	where m.status =''N'' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 =0
	begin
	 declare @ReferenceNumber varchar(50)
	 set @ReferenceNumber =''''
	 select @ReferenceNumber = aci_vendorID+''.''+MDO_PracticeId+''.4.'' from aci_json_Defaults (NOLOCK)
--ETHNICITY
  declare @Field varchar(10) 
  declare @DATA as varchar(10),@DESCRIPTION1 as varchar(10),@Code as varchar(10),@CodeSystem as varchar(10) 
  set @Field =''''
  set @DATA =''''
  set @DESCRIPTION1 =''''
  set @Code =''''
  set @CodeSystem =''''

	--SELECT @Field =isnull( case when charIndex('','',Ethnicity) >0 then substring(Ethnicity,1,(charIndex('','',Ethnicity)-1)) else Ethnicity end ,''1'')
	--  from patient (NOLOCK) where PatientKey=@PatientKey
	--  if rtrim(ltrim(@Field)) ='''' 
	--    set @Field =1
	-- --select @DATA= DATA,@DESCRIPTION1=DESCRIPTION1,@Code=Code,@CodeSystem=CodeSystem from dropdown (NOLOCK) where  ('','' + @Field +'','' LIKE ''%,'' + CONVERT(VARCHAR, data) + '',%'')
	 --and tablename =''patientinfo'' and fieldname =''ETHNICITY''
      --ETHNICITY
	 Declare @ReferenceNumberT varchar(50)
	 set @ReferenceNumberT =''''
	 set @ReferenceNumberT =@ReferenceNumber+cast(@PATIENTKEY as varchar(10))

		SELECT @STRINGJSON=@STRINGJSON+ isnull(
					REPLACE(REPLACE(
							DBO.ACI_QFN_XMLTOJSON (
							(     
							SELECT

			ISNULL(ResourceType,'''') AS ResourceType, ISNULL(ExternalId,'''') AS ExternalId, ISNULL(IsSource,'''') AS IsSource, ISNULL(Active,'''') AS Active,
			(isnull(@ReferenceNumber,'''')+ ISNULL(ReferenceNumber,'''')) AS ReferenceNumber,
			ISNULL(PatientAccountNumber,'''') AS PatientAccountNumber, ISNULL(GivenName,'''') AS GivenName, ISNULL(FamilyName,'''') AS FamilyName, 
			ISNULL(SecondandFurtherGivenNames,'''') AS SecondandFurtherGivenNames,
			ISNULL(NamePrefix,'''') AS NamePrefix, ISNULL(NameSuffix,'''') AS NameSuffix, ISNULL(DateOfBirth,'''') AS DateOfBirth, ISNULL(BirthOrder,'''') AS BirthOrder, 
			ISNULL(MultipleBirthIndicator,'''') AS MultipleBirthIndicator,
			ISNULL(SexDescription,'''') AS SexDescription, ISNULL(SexCode,'''') AS SexCode, ISNULL(SexCodeSystemName,'''') AS SexCodeSystemName, ISNULL(SexCodeSytem,'''') AS SexCodeSytem,
			ISNULL(GenderIdentityDescription,'''') AS GenderIdentityDescription,
			ISNULL(GenderIdentityCode,'''') AS GenderIdentityCode, ISNULL(GenderIdentityCodeSystemName,'''') AS GenderIdentityCodeSystemName,
			ISNULL(GenderIdentityCodeSystem,'''') AS GenderIdentityCodeSystem,
			ISNULL(SexualOrientationDescription,''Don''''t know'') AS SexualOrientationDescription, ISNULL(SexualOrientationCode,''UNK'') AS SexualOrientationIdentity,
			ISNULL(SexualOrientationCodeSystemName,'''') AS SexualOrientationCodeSystemName,
			ISNULL(SexualOrientationCodeSystem,'''') AS SexualOrientationCodeSystem, 
			CAST(ISNULL(CAST (ETHNICITYCODE AS VARCHAR(10)),''UNK'') AS VARCHAR(10)) AS EthnicityCode, 
			CAST(ISNULL(ETHNICITYDECRIPTION,''Declined to Specify'') AS VARCHAR(50)) AS EthnicityDescription,
			CAST(ISNULL(ETHINITYCODESYSTEMNAME,''RACE & ETHNICITY - CDC'') AS VARCHAR(50)) AS EthnicityCodeSystemName,
			CAST(ISNULL(ETHNICITYCODESYSTEM,''2.16.840.1.113883.6.238'') AS VARCHAR(50)) AS EthnicityCodeSystem,
			 ''G'' AS LanguageAbilityMode, ''Good'' AS LanguageAbilityModeName, 
			ISNULL(LanguageAbilityModeSystem,'''') AS LanguageAbilityModeSystem,
			ISNULL(LanguageAbilityModeystemName,'''') AS LanguageAbilityModeystemName, ISNULL(MaritalStatusCode,'''') AS MaritalStatusCode, 
			ISNULL(MaritalStatusDescription,'''') AS MaritalStatusDescription,
			ISNULL(MaritalStatusCodeSystemName,'''') AS MaritalStatusCodeSystemName, ISNULL(MaritalStatusCodeSystem,'''') AS MaritalStatusCodeSystem,
			ISNULL(ReligiousAffiliationCode,'''') AS ReligiousAffiliationCode,
			ISNULL(ReligiousAffiliationDesc,'''') AS ReligiousAffiliationDesc, ISNULL(ReligiousAffiliationCodeSystemName,'''') AS ReligiousAffiliationCodeSystemName, 
			ISNULL(ReligiousAffiliationCodeSystem,'''') AS ReligiousAffiliationCodeSystem,
			ISNULL(MothersMaidenName,'''') AS MothersMaidenName, ISNULL(NameTypeCode,'''') AS NameTypeCode, ISNULL(NameTypeDescription,'''') AS NameTypeDescription,
			ISNULL(PatientDeathIndicator,'''') AS PatientDeathIndicator,
			'''' AS PatientDeathDateandTime,'''' as Email
 

							FROM VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION P WHERE P.ID= @PATIENTKEY FOR XML PATH(''PATIENT''),TYPE) 
					),''['','''') ,''}]'','''')      ,'''')

          if (select count(*) from VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_PHONE where ID=@PATIENTKEY)>0
		  SELECT @STRINGJSON=@STRINGJSON+'',"Phone":''+  isnull( DBO.ACI_QFN_XMLTOJSON (
			 (
				SELECT ISNULL(Type,'''') AS Type,ISNULL(Number,'''') AS Number
				     FROM VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_PHONE PP WHERE PP.ID=@PATIENTKEY  FOR XML PATH(''PHONE''),TYPE ) )   ,'''')
		 --  PRINT @STRINGJSON

		SELECT @STRINGJSON=@STRINGJSON+ '',"BirthPlace":''+isnull(
					REPLACE(REPLACE(
							DBO.ACI_QFN_XMLTOJSON (
							(     
							SELECT
							ISNULL(City,''NOT AVAILABLE'') AS City,ISNULL(State,''NA'') AS State,ISNULL(Country,''USA'') AS Country
							FROM VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_BIRTHPLACE P WHERE P.ID= @PATIENTKEY FOR XML PATH(''BIRTHPLACE''),TYPE) 
					),''['','''') ,''}]'','''') ,'''')+''}''      

		SELECT @STRINGJSON=@STRINGJSON+ '',"PreferredLanguage":''+isnull(
					REPLACE(REPLACE(
							DBO.ACI_QFN_XMLTOJSON (
							(     
							SELECT
							 CAST(ISNULL(CAST(LanguageCode AS VARCHAR(10)),''UNK'')AS VARCHAR(10)) AS LanguageCode ,CAST(ISNULL(CAST(LanguageName AS VARCHAR(50)),''Declined To Specify'') AS VARCHAR(50)) AS LanguageName,CAST(ISNULL(LanguageCodeSystemName,''LANGUAGE (PHVS_LANGUAGE_ISO_639-2_ALPHA3)'')AS VARCHAR(50)) AS LanguageCodeSystemName
							  ,CAST(ISNULL(LanguageCodeSystem,''2.16.840.1.114222.4.11.831'') AS VARCHAR(50)) AS LanguageCodeSystem
								FROM VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_PREFERREDLANGUAGE P WHERE P.ID= @PATIENTKEY FOR XML PATH(''PREFERREDLANGUAGE''),TYPE) 
					),''['','''') ,''}]'','''') ,'''')+''}''      
					
       --if (select case when isnull(RACE_ID,'''') ='''' then ''0'' else RACE_ID end  from MODEL.PATIENTRACE (NOLOCK) where PATIENTS_ID = @PATIENTKEY) <>'''' --race
	   IF (SELECT COUNT(*) FROM DBO.ACI_VIEW_JSON4_PATIENTDEMOGRAPHICSINFORMATION_RACE (NOLOCK) where Id = @PATIENTKEY)>0
	   begin
			--DECLARE @TABLE AS TABLE (RACECODE VARCHAR(5),RACEDESCRIPTION VARCHAR(100),RACECODESYSTEMNAME VARCHAR(50),RACECODESYSTEM	VARCHAR(50) )
			--INSERT INTO @TABLE EXEC ACI_SP_JSON4_PATIENTDEMOGRAPHICSINFORMATION_RACE  @PATIENTKEY 

			SELECT @STRINGJSON=@STRINGJSON+'',"Race":''+isnull(  DBO.ACI_QFN_XMLTOJSON (
			(
			SELECT CAST(ISNULL(CAST(RaceCode AS VARCHAR(10)),''UNK'') AS VARCHAR(10)) AS RaceCode,CAST(ISNULL(CAST(RaceDescription AS VARCHAR(50)),''Declined to Specify'') AS VARCHAR(50)) AS RaceDescription ,CAST(ISNULL(RaceCodeSystemName,''CDC - RACE AND ETHNICITY'') AS VARCHAR(10)) AS RaceCodeSystemName,CAST(ISNULL(RaceCodeSystem ,''2.16.840.1.113883.6.238'') AS VARCHAR(50)) AS RaceCodeSystem
			FROM ACI_VIEW_JSON4_PATIENTDEMOGRAPHICSINFORMATION_RACE
			WHERE ID=@PATIENTKEY	 
			FOR XML PATH(''RACE''),TYPE ) )   ,'''')
	
       end--race
	  
		select @StringJson=@StringJson+'',"Address":''+  isnull(REPLACE(REPLACE(dbo.aci_qfn_XmlToJson (
		(
			 select ISNULL(Type,''NOT AVAILABLE'') as Type,ISNULL(AddressLine1,''NOT AVAILABLE'') AS AddressLine1,ISNULL(AddressLine2,'''') AS AddressLine2,ISNULL(City,''NOT AVAILABLE'') AS City,ISNULL(State,''NA'') AS State ,ISNULL(Zip,''00000'') AS Zip,
				  ISNULL(CAST(County AS VARCHAR(25)),'''') AS County, ISNULL(CAST(CountyCode AS VARCHAR(25)),'''') AS CountyCode,isnull(Country,'''') as Country
		   from VIEW_ACI_JSON4_PATIENTDEMOGRAPHICSINFORMATION_ADDRESS Pa where  Pa.ID = @PATIENTKEY  for xml path(''address''),type) 
		   ),''['','''') ,'']'',''''),'''')+''}''  
		    

select @StringJson

      --if ltrim(rTrim(@StringJson)) <> ''''
	 if (ltrim(rTrim(ISNULL(@StringJson,''''))) <> '''' and len(@StringJson) >10)
	  begin
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,ReferenceNumber,Sort) 
                 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))),@ReferenceNumberT,4)  

		 set @ACIJSONMasterKey=IDENT_CURRENT(''ACI_JSON_Master'')

		    update ACI_InPutLog set status =''C'',statuschangedt=GETDATE() where status =''N'' and TableName =''JSON04_PATIENTDEMOGRAPHICSINFORMATION'' 
			and( tableKey=@tableKey and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where isnull(JSON,'''') <>'''' and  status=''N'' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
  END
end
' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON05_REPRESENTATIVE')) 
Drop Procedure ACI_ConvertToJson_JSON05_REPRESENTATIVE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE proc [dbo].[ACI_ConvertToJson_JSON05_REPRESENTATIVE] (@REPRESENTATIVEKEY INT,@aciinputlogkey int)
as 
begin
/*
ACI Version :Release 1.0
Date:05/05/2017
Developer/Author:K.V.S.S.R.KRISHNA SARMA
Details/Description: To create JSON String in ACI_JSON_MAster table
*/
/*
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/
  declare @StringJson varchar(8000)
  declare @aciinputlogkey1 int ,@tableKey INT, @ACIJSONMasterKey int,@PortalAccountNumber INT
  set @aciinputlogkey1 =0
  set @tableKey=0
  set @ACIJSONMasterKey =0
  set @tableKey=@REPRESENTATIVEKEY
  set @StringJson=''''
  set @PortalAccountNumber =0

	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status=''N''  and  c.TableName = ''JSON05_REPRESENTATIVE''
	where m.status =''N'' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 =0
	begin
	 declare @ReferenceNumber varchar(50)
	 set @ReferenceNumber =''''
	 select @ReferenceNumber = aci_vendorID+''.''+MDO_PracticeId+''.5.'' from aci_json_Defaults (NOLOCK)

	 Declare @ReferenceNumberT varchar(50)
	 set @ReferenceNumberT =''''
	 set @ReferenceNumberT =@ReferenceNumber+cast(@REPRESENTATIVEKEY as varchar(10))

	 IF (SELECT COUNT(*) FROM MVE.PP_ACCOUNTDETAILS)>0
	    select top 1 @PortalAccountNumber= ID from MVE.PP_ACCOUNTDETAILS
      else
	    select @PortalAccountNumber= 1

	if (select count(*) from VIEW_ACI_JSON05_REPRESENTATIVE CP where CP.PatientRepresentativeID = @REPRESENTATIVEKEY ) >0  
        select @StringJson=@StringJson+ 
            REPLACE(REPLACE(
					dbo.aci_qfn_XmlToJson (
					(     
						SELECT 
						 ISNULL(ResourceType,'''') as ResourceType,
						 ISNULL(Role,'''') as Role,
						 ISNULL(IsSource,'''') as IsSource,
						 (isnull(@ReferenceNumber,'''')+ISNULL(ReferenceNumber,'''')) as ReferenceNumber,
						 ISNULL(PatientAccountNumber,'''') as PatientAccountNumber,
						-- @PortalAccountNumber as PortalAccountNumber,
						 ISNULL(PortalAccountNumber,'''') as PortalAccountNumber,
						 ISNULL(ExternalId,'''') as ExternalId,
						 ISNULL(GivenName,'''') as GivenName,
						 ISNULL(FamilyName,'''') as FamilyName,
						 ISNULL(SecondandFurtherGivenNames,'''') as SecondandFurtherGivenNames,
						 ISNULL(Relationship,'''') as Relationship,
						 ISNULL(RelationshipCode,'''') as RelationshipCode,
						 ISNULL(RelationshipCodeSystem,'''') as RelationshipCodeSystem,
						 ISNULL(RelationshipCodeSystemName,'''') as RelationshipCodeSystemName,
						 ISNULL(DateofBirth,'''') as DateofBirth,
						 ISNULL(STATUS,'''') as Status
						 --ISNULL(REPRESENTATIVEKEY,'''') as REPRESENTATIVEKEY
						 --,ISNULL(CHARTRECORDKEY,'''') as CHARTRECORDKEY
					from VIEW_ACI_JSON05_REPRESENTATIVE CP where CP.PatientRepresentativeID  = @REPRESENTATIVEKEY  for xml path(''REPRESENTATIVE''),type) 
            ),''['','''') ,''}]'','''')       


	if (select count(*) from VIEW_ACI_JSON05_REPRESENTATIVE_PHONE CP where CP.PatientRepresentativeID = @REPRESENTATIVEKEY  ) >0 
		select @StringJson=@StringJson+'',"Phone":''+  dbo.aci_qfn_XmlToJson (
		(
		select 
		ISNULL(Type,'''') AS Type,
		ISNULL(Number,'''') AS Number
		   from VIEW_ACI_JSON05_REPRESENTATIVE_PHONE p where P.PatientRepresentativeID = @REPRESENTATIVEKEY   for xml path(''Phone''),type ) )   

	if (select count(*) from VIEW_ACI_JSON05_REPRESENTATIVE_ADDRESS CP where CP.PatientRepresentativeID = @REPRESENTATIVEKEY  ) >0 
		select @StringJson=@StringJson+'',"Address":''+  REPLACE(REPLACE(dbo.aci_qfn_XmlToJson (
		(
		select 
		ISNULL(AddressLine1,'''') AS AddressLine1,
		ISNULL(AddressLine2,'''') AS AddressLine2,
		ISNULL(City,'''') AS City,
		ISNULL(State,'''') AS State ,
		ISNULL(Zip,'''') AS Zip,
		ISNULL(CAST(County AS VARCHAR(25)),'''') AS County, 
		ISNULL(CAST(CountyCode AS VARCHAR(25)),'''') AS CountyCode,
		ISNULL(Country,'''') AS Country
		   from VIEW_ACI_JSON05_REPRESENTATIVE_ADDRESS a where  a.PatientRepresentativeID = @REPRESENTATIVEKEY   for xml path(''Address''),type) 
		   ),''['','''') ,'']'','''')+''}''
   select @StringJson
      --if ltrim(rTrim(@StringJson)) <> ''''
	 if (ltrim(rTrim(@StringJson)) <> '''' and len(@StringJson) >10)
	  begin
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,ReferenceNumber,Sort) 
                 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))),@ReferenceNumberT,5)  

		set @ACIJSONMasterKey=IDENT_CURRENT(''ACI_JSON_Master'')
		update ACI_InPutLog set status =''C'',statuschangedt=GETDATE() where status =''N'' and TableName =''JSON05_REPRESENTATIVE'' 
		and( tableKey=@tableKey and
		     tablekey in(select tablekey from ACI_JSON_Master (nolock) where isnull(JSON,'''') <>'''' and  status=''N'' and ACIJSONMasterKey=@ACIJSONMasterKey))
      end
  end
end
' 

Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON07_PatientExam')) 
Drop Procedure ACI_ConvertToJson_JSON07_PatientExam 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'



CREATE proc [dbo].[ACI_ConvertToJson_JSON07_PatientExam] (@patientKey INT,@ChartRecordKey INT,@PROCEDUREKEY INT,@ACIInPutLogKey int)
as
BEGIN

/*
ACI Version :Release 1.0
Date:05/05/2017
Developer/Author:K.V.S.S.R.KRISHNA SARMA
Details/Description: To create JSON String in ACI_JSON_MAster table
*/
/*
  THIS SP WILL TRIGGER IN "ACI_INSERT_ACI_JSON_MASTER" 
*/
DECLARE @STRINGJSON varchar(max),@CHARTRECORDKEY1 INT,@PATIENTKEY1 INT,@PROCEDUREKEY1 INT,@Print int

set @Print=1
SET @STRINGJSON=''''
SET @CHARTRECORDKEY1 =@ChartRecordKey
--SET @PATIENTKEY1 =@patientKey
set @PROCEDUREKEY1=@PROCEDUREKEY
if @Print=0
   print ''begin''

  if @Print=1
    print 1
  declare @aciinputlogkey1 int ,@tableKey INT, @ACIJSONMasterKey int
  set @aciinputlogkey1 =0
  set @tableKey=0
  set @ACIJSONMasterKey =0
  set @tableKey=@ChartRecordKey
  set @StringJson=''''

  select top 1 @PATIENTKEY1 = PatientId from patientclinical (nolock) where AppointmentId =@CHARTRECORDKEY1

	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status=''N''  and  c.TableName = ''PATIENTEXAM''
	where m.status =''N'' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc
if @Print=1
   print 2
	if @aciinputlogkey1 =0
	begin
if @Print=1
   print 3
		declare @ReferenceNumber varchar(50),@PhyId int , @FacId int
		set @PhyId =0
		set @FacId =0

		select top 1 @PhyId=s.ResourceID1,@FacId=r.PracticeID  from patientclinical c (nolock)
		left join appointments s (nolock) on s.appointmentid  =c.appointmentid
		left join dbo.resources r (nolock)  on r.Resourceid=s.Resourceid1 and r.resourcetype=''D''
		 where c.appointmentid =@CHARTRECORDKEY1 order by c.appointmentid desc
if @Print=1
   print 4		
		 set @ReferenceNumber =''''
		 select @ReferenceNumber = aci_vendorID+''.''+MDO_PracticeId+''.''+CAST(@FacId as varchar(10))+''.''+CAST(@PhyId as varchar(10))+''.''+CAST(@PATIENTKEY1 as varchar(10))+''.7.'' from aci_json_Defaults (NOLOCK)

		 Declare @ReferenceNumberT varchar(50)
		 set @ReferenceNumberT =''''
		 set @ReferenceNumberT =@ReferenceNumber+cast(@CHARTRECORDKEY1 as varchar(10))

	   set @ACIJSONMasterKey =0
	   if (select count(*) from ACI_JSON_MASTER (nolock) where STATUS=''N'' and 
	           ISVALIDJSON is null and TABLEKEY =@tableKey and ACIINPUTLOGKEY =@aciinputlogkey)=0
	   begin
if @Print=1
   print 5
         insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,ReferenceNumber,Sort) 
                 values(@aciinputlogkey,@tableKey,@ReferenceNumberT,7)  

		 set @ACIJSONMasterKey=IDENT_CURRENT(''ACI_JSON_Master'')
	   end
if @Print=1
   print 6
/*EXAM-BEGIN ==========================================================================================================================*/
declare @T1 as varchar(20)
set @T1 = ''''

	IF (SELECT COUNT(*) FROM VIEW_ACI_PATIENT_PEXAM (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1) >0
		SELECT @STRINGJSON=@STRINGJSON+ 
		isnull(
            REPLACE(REPLACE(
					DBO.ACI_QFN_XMLTOJSON (
					(     
					SELECT top 1
							ISNULL(ResourceType,'''') AS ResourceType,
							ISNULL(IsSource,'''') AS IsSource,
							(isnull(@ReferenceNumber,'''')+ISNULL(ReferenceNumber,'''')) AS ReferenceNumber,
							--ISNULL(Date ,'''') AS Date,
							(CONVERT(VARCHAR(20), Date, 101)) AS Date,
							ISNULL(Type,'''') AS Type,
							ISNULL(PatientAccountNumber,'''') AS PatientAccountNumber,
							ISNULL(ExternalId,'''') AS ExternalId

					FROM VIEW_ACI_PATIENT_PEXAM (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''EXAM''),TYPE) 
            ),''['','''') ,''}]'','''')  
			,'''')
		  SET @STRINGJSON =REPLACE(@STRINGJSON,''NULL'',''""'')	 
          update  ACI_JSON_MASTER set json =@STRINGJSON where ACIJSONMasterKey = @ACIJSONMasterKey
if @Print=1
   print 7
/*EXAM-END ==========================================================================================================================*/
--SELECT @STRINGJSON
/*ENCOUNTER-BEGIN====================================================================================================================*/
declare @ENCOUNTER varchar(max)
set @ENCOUNTER =''''

	IF (SELECT COUNT(*) FROM VIEW_ACI_APPOINTMENT  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1)  >0
	begin
		--SELECT @STRINGJSON=@STRINGJSON+ '',"ENCOUNTER":''+
		if (SELECT count(*)						
					FROM VIEW_ACI_APPOINTMENT_BILLINGCODE (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1)>0
		SELECT @ENCOUNTER=isnull(
            REPLACE(REPLACE(
					DBO.ACI_QFN_XMLTOJSON (
					(     
					SELECT TOP 1 
						ISNULL(EncounterId,'''') AS EncounterId ,
						ISNULL(EncounterDate,'''') AS EncounterDate,
						ISNULL(ECNPI,'''') AS ECNPI,
						--ISNULL(ECTIN,'''') AS ECTIN,
						(SUBSTRING(ECTIN,1,3)+''-''+SUBSTRING(ECTIN,4,2)+''-''+SUBSTRING(ECTIN,6,4))  as ECTIN,
						ISNULL(EncounterDuration,'''') AS EncounterDuration,
						ISNULL(FacilityAccountNumber,'''') AS FacilityAccountNumber,
						ISNULL(ChiefComplaint,'''') AS ChiefComplaint,
						ISNULL(ReasonForVisit,'''') AS ReasonForVisit,
						ISNULL(ReasonForReferral,'''') AS ReasonForReferral,
						ISNULL(Assessment,'''') AS Assessment,
						ISNULL(SignedDate,'''') AS SignedDate,
						ISNULL(IsActive,'''') AS IsActive
					FROM VIEW_ACI_APPOINTMENT (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''ENCOUNTER''),TYPE) 
            ),''['','''') ,''}]'','''')  
			,'''')
   end
if @Print=1
   print 8
		IF (@ENCOUNTER)<>''''
		begin
		if (SELECT count(*)						
					FROM VIEW_ACI_APPOINTMENT_BILLINGCODE (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1)>0
           begin
			SELECT @ENCOUNTER=@ENCOUNTER+'',"BillingCode":''+ isnull( DBO.ACI_QFN_XMLTOJSON (
				( --THIS IS CHILD FOR ENCOUNTER 
					SELECT 
						ISNULL(ExternalId ,'''') AS ExternalId,
						ISNULL(BillingCodeValue ,'''') AS BillingCodeValue,
						ISNULL(BillingCodeDescription ,''Not available'') AS BillingCodeDescription,
						ISNULL(BillingCodeSystem ,'''') AS BillingCodeSystem ,
						ISNULL(BillingCodeName ,'''') AS BillingCodeName,
						ISNULL(IsActive ,'''') AS IsActive
					FROM VIEW_ACI_APPOINTMENT_BILLINGCODE (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''ENCOUNTER_BILLINGCODE''),TYPE ) ) 
					,'''')
            end

        end   
         
       IF (@ENCOUNTER)<>''''
	   begin
	    set @ENCOUNTER =@ENCOUNTER+''}'' 
	    SET @ENCOUNTER =REPLACE(@ENCOUNTER,''NULL'',''""'')	 
		SELECT @ENCOUNTER='',"Encounter":''+@ENCOUNTER
		update  ACI_JSON_MASTER set ENCOUNTER =@ENCOUNTER where ACIJSONMasterKey = @ACIJSONMasterKey
	   end
if @Print=1
   print 9	   

/*ENCOUNTER-END====================================================================================================================*/
--SELECT @STRINGJSON

/*INFORMANTRELATED-BEGIN ======================================================================================================================================*/
declare  @STRINGJSON_InformantRelated VARCHAR(MAX)
set @STRINGJSON_InformantRelated=''''
     IF (SELECT COUNT(*) FROM VIEW_ACI_INFORMANTRELATED (NOLOCK) WHERE patientkey=@PATIENTKEY1 )>0
		SELECT @STRINGJSON_InformantRelated= isnull( DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT 
				ISNULL(ExternalId,'''') AS ExternalId,
				ISNULL(RelationshipName,'''') AS RelationshipName,
				ISNULL(RelationshipCode,''00000'') AS RelationshipCode,
				ISNULL(RelationshipCodeSystem,'''') AS RelationshipCodeSystem,
				ISNULL(RelationshipCodeSystemName,'''') AS RelationshipCodeSystemName,
				ISNULL(GivenName,'''') AS GivenName,
				ISNULL(FamilyName,'''') AS FamilyName,
				ISNULL(SecondandFurtherGivenNames,'''') AS SecondandFurtherGivenNames,
				ISNULL(IsActive,'''') AS IsActive 
		    FROM VIEW_ACI_INFORMANTRELATED  (NOLOCK)  WHERE patientkey=@PATIENTKEY1 FOR XML PATH(''INFORMANTRELATED1''),TYPE ) ) ,'''')
     if @STRINGJSON_InformantRelated <>'''' 
	Begin
	    SET @STRINGJSON_InformantRelated =REPLACE(@STRINGJSON_InformantRelated,''NULL'',''""'')	 
		SELECT @STRINGJSON_InformantRelated='',"InformantRelated":''+@STRINGJSON_InformantRelated
		update  ACI_JSON_MASTER set InformantRelated =@STRINGJSON_InformantRelated where ACIJSONMasterKey = @ACIJSONMasterKey
	end
/*INFORMANTRELATED-END ======================================================================================================================================*/
/*ALLERGIES-BEGIN================================================================================================================================================*/
DECLARE @PATIENTALLERGYKEY VARCHAR(30) , @STRINGJSON_ALLERGIES VARCHAR(MAX)
,@ALLERGIES Varchar(1000),@Allergy_Code Varchar(8000)
set @ALLERGIES =''''
set @Allergy_Code =''''
set @STRINGJSON_ALLERGIES =''''
		DECLARE CURSORA_PPL SCROLL CURSOR FOR 
		  select PATIENTALLERGYKEY from VIEW_ACI_ALLERGIES_ALLERGY (NOLOCK) where  CHARTRECORDKEY = @CHARTRECORDKEY1
		OPEN CURSORA_PPL   
		FETCH FIRST FROM CURSORA_PPL INTO @PATIENTALLERGYKEY
		WHILE @@FETCH_STATUS=0
		BEGIN
			select  @ALLERGIES= replace(--''}''
			replace(--'']''
			replace --''[''
			 (ISNULL( dbo.ACI_qfn_XmlToJson(	
					(select 			
						ISNULL(Date,'''') AS  Date ,ISNULL(ExternalId , '''') AS ExternalId,	
						ISNULL(Reaction,'''') AS Reaction ,ISNULL(ReactionDesc,'''') ReactionDesc,ISNULL(ReactionCodeSystem,'''') as ReactionCodeSystem
								  ,ISNULL(ReactionCodeSystemName,'''') AS ReactionCodeSystemName ,ISNULL(SeverityCode,'''') AS SeverityCode ,ISNULL(SeverityDescription,'''') AS SeverityDescription
								  ,ISNULL(SeverityCodeSystem,'''') AS SeverityCodeSystem,ISNULL(SeverityCodeSystemName,'''') AS SeverityCodeSystemName,ISNULL(Status,'''') AS Status,ISNULL(IsActive,'''') AS IsActive								
						from VIEW_ACI_ALLERGIES 
						where PATIENTALLERGYKEY = @PATIENTALLERGYKEY for xml path(''AllergiesA''),type	    
						) 
					)
				,'''') 
				,''['','''')
				,'']'','''')
				,''}'','''')
			   -- select @ProblemsList
				if @ALLERGIES<>''''
					select  @Allergy_Code= REPLACE(
					REPLACE(
					ISNULL( dbo.ACI_qfn_XmlToJson(	
					(select 
							ISNULL(Code ,'''') AS Code,ISNULL(Description,'''') AS Description,
							CASE
							WHEN Code=''UNK'' THEN ''NonCoded''
							ELSE ISNULL(CodeSystemName,'''') 
							END AS CodeSystemName,
							CASE
							WHEN Code=''UNK'' THEN ''NonCoded''
							ELSE ISNULL(CodeSystem,'''')
							END AS CodeSystem										
								from VIEW_ACI_ALLERGIES_ALLERGY (NOLOCK) where   PATIENTALLERGYKEY = @PATIENTALLERGYKEY  for xml path(''AllergyCodeA''),type
						) 
					)
					  ,'''')
					  ,''['','''')
					  ,'']'','''')

				if @Allergy_Code <>''''
				begin
	    			set @ALLERGIES =@ALLERGIES+'',"AllergyCode":''+@Allergy_Code+''}''
				  if @STRINGJSON_ALLERGIES ='''' 
				  set @STRINGJSON_ALLERGIES=@ALLERGIES
				  else          
				  set @STRINGJSON_ALLERGIES=@STRINGJSON_ALLERGIES+'',''+@ALLERGIES
				 --r select @Procedures
				 end
		         
		  FETCH NEXT FROM CURSORA_PPL INTO @PATIENTALLERGYKEY
		END
		CLOSE CURSORA_PPL
		DEALLOCATE CURSORA_PPL
 
--select  '',"Allergies":[''+@STRINGJSON_ALLERGIES+'']''

		IF @STRINGJSON_ALLERGIES <>''''
		BEGIN
		    SET @STRINGJSON_ALLERGIES =REPLACE(@STRINGJSON_ALLERGIES,''NULL'',''""'')	 
			set @STRINGJSON_ALLERGIES='',"Allergies":[''+@STRINGJSON_ALLERGIES+'']''
			update  ACI_JSON_MASTER set ALLERGIES =@STRINGJSON_ALLERGIES where ACIJSONMasterKey = @ACIJSONMasterKey
		END
	--	SELECT @STRINGJSON=@STRINGJSON+'',"ALLERGIES":''+@SQLJson
if @Print=1
   print 10	
/*ALLERGIES-END================================================================================================================================================*/			 

/*FAMILYHISTORY BEGIN ======================================================================================================================================*/	
DECLARE @FAMILYHISTORY VARCHAR(max)
SET @FAMILYHISTORY =''''
			IF (SELECT COUNT(*) FROM  VIEW_ACI_FAMILYHISTORY (NOLOCK)   WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0--IF NO FAMILY HISTORY
					--SELECT @STRINGJSON=@STRINGJSON+'',"FAMILYHISTORY":''+ ISNULL( DBO.ACI_QFN_XMLTOJSON (
					SELECT @FAMILYHISTORY = ISNULL( DBO.ACI_QFN_XMLTOJSON (
					(
							SELECT 
							ISNULL(ExternalId ,'''') AS ExternalId,
							ISNULL(RelationshipName ,'''') AS RelationshipName,
							ISNULL(RelationshipCode ,'''') AS RelationshipCode,
							ISNULL(RelationshipCodeSystem ,'''') AS RelationshipCodeSystem,
							ISNULL(RelationshipCodeSystemName ,'''') AS RelationshipCodeSystemName,
							ISNULL(AgeAtOnSetValue ,'''') AS AgeAtOnSetValue,
							ISNULL(AgeAtOnSetUnit ,'''') AS AgeAtOnSetUnit,
							ISNULL(AdministrativeGenderCode ,'''') AS AdministrativeGenderCode,
							ISNULL(AdministrativeGenderDescription ,'''') AS AdministrativeGenderDescription,
							ISNULL(AdministrativeGenderCodeSystem ,'''') AS AdministrativeGenderCodeSystem,
							ISNULL(AdministrativeGenderCodeSystemName ,'''') AS AdministrativeGenderCodeSystemName,
							ISNULL(IsFamilyMemberAlive ,'''') AS IsFamilyMemberAlive,
							ISNULL(ProblemCode ,'''') AS ProblemCode,
							ISNULL(ProblemDescription ,'''') AS ProblemDescription,
							ISNULL(ProblemCodeSystem ,'''') AS ProblemCodeSystem,
							ISNULL(ProblemCodeSystemName ,'''') AS ProblemCodeSystemName,
							--ISNULL(ProblemCategory ,'''') AS ProblemCategory,
							ISNULL(Status ,'''') AS Status,
							ISNULL(IsActive ,'''') AS IsActive
					   FROM VIEW_ACI_FAMILYHISTORY (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1   FOR XML PATH(''FamilyHistory''),TYPE ) ),'''')   

		IF @FAMILYHISTORY <>''''
		BEGIN
		    SET @FAMILYHISTORY =REPLACE(@FAMILYHISTORY,''NULL'',''""'')	 
			SELECT @FAMILYHISTORY='',"FamilyHistory":''+@FAMILYHISTORY
		 	update  ACI_JSON_MASTER set FAMILYHISTORY =@FAMILYHISTORY where ACIJSONMasterKey = @ACIJSONMasterKey
		END
		
if @Print=1
   print 11
/*FAMILYHISTORY END======================================================================================================================================*/	
/*FUNCTIONALSTATUS-BEGIN=================================================================================================================================*/
DECLARE @FUNCTIONALSTATUS VARCHAR(max)
SET @FUNCTIONALSTATUS =''''
	IF (SELECT COUNT(*) FROM VIEW_ACI_FUNCTIONALSTATUS (NOLOCK) WHERE  CHARTRECORDKEY=@CHARTRECORDKEY1)>0
		--SELECT @STRINGJSON=@STRINGJSON+'',"FUNCTIONALSTATUS":''+  DBO.ACI_QFN_XMLTOJSON (
		SELECT @FUNCTIONALSTATUS=ISNULL( DBO.ACI_QFN_XMLTOJSON (
		(
		SELECT 
			ISNULL(ExternalId,'''') AS ExternalId,
			ISNULL(Type,'''') AS Type,
			ISNULL(Code,'''') AS Code,
			ISNULL(Description,'''') AS Description,
			ISNULL(CodeSystem,'''') AS CodeSystem,
			ISNULL(CodeSystemName,'''') AS CodeSystemName,
			ISNULL(Status,'''') AS Status,
			ISNULL(DateCaptured,'''') AS  DateCaptured,
			ISNULL(IsActive,'''') AS IsActive

		   FROM VIEW_ACI_FUNCTIONALSTATUS (NOLOCK) WHERE  CHARTRECORDKEY=@CHARTRECORDKEY1  FOR XML PATH(''FunctionalStatus''),TYPE ) ) 
		   ,'''')

		IF @FUNCTIONALSTATUS <>''''
		BEGIN
		    SET @FUNCTIONALSTATUS =REPLACE(@FUNCTIONALSTATUS,''NULL'',''""'')	 
			SELECT @FUNCTIONALSTATUS='',"FunctionalStatus":''+@FUNCTIONALSTATUS
		 	update  ACI_JSON_MASTER set FUNCTIONALSTATUS =@FUNCTIONALSTATUS where ACIJSONMasterKey = @ACIJSONMasterKey
		END
if @Print=1
   print 12	

/*FUNCTIONALSTATUS-END=================================================================================================================================*/


--IMMUNIZATION-BEGIN=================================================================================================================================*/--needtocheck
-- 08-17-2017 Rev1.1

DECLARE @PATIENTIMMKEY VARCHAR(30) 
,@STRINGJSON_IMMUNIZATION VARCHAR(MAX)
,@IMMUNIZATION Varchar(max)
,@Route Varchar(8000)
,@Manufacturer Varchar(8000)
,@DosageQuantity Varchar(8000)
,@REASON  Varchar(8000)

set @STRINGJSON_IMMUNIZATION =''''
set @IMMUNIZATION =''''
set @Route =''''
set @Manufacturer = ''''
set @DosageQuantity = ''''
set @REASON = ''''

	   DECLARE CURSORA_IMM SCROLL CURSOR FOR 
	   select PATIMMKEY from VIEW_ACI_IMMUNIZATION (NOLOCK) where  CHARTRECORDKEY=@CHARTRECORDKEY1
	   OPEN CURSORA_IMM   
	   FETCH FIRST FROM CURSORA_IMM INTO @PATIENTIMMKEY
	   WHILE @@FETCH_STATUS=0
	   BEGIN
			set @IMMUNIZATION =''''
			set @Route =''''
			set @Manufacturer = ''''
			set @DosageQuantity = ''''
			set @REASON = ''''

		  select  @IMMUNIZATION= replace(--''}''
		  replace(--'']''
		  replace --''[''
		   (ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select 
     				ISNULL(ExternalId ,'''') AS ExternalId,ISNULL(VaccineName ,'''') AS VaccineName,ISNULL(VaccineCode ,'''') AS VaccineCode,
	    			ISNULL(VaccineType ,'''') AS VaccineType,ISNULL(FillerOrderNumber ,'''') AS FillerOrderNumber,ISNULL(DateTimeStartofAdministration ,'''') AS DateTimeStartofAdministration,
		    		ISNULL(SerialNumber ,'''') AS SerialNumber,ISNULL(LotNumber ,'''') AS LotNumber,ISNULL(RefusalReason,'''') as RefusalReason,
					ISNULL(OrderedBy,'''') as OrderedBy,ISNULL(PerformedBy,'''') as PerformedBy,ISNULL(StatusCode,'''') as StatusCode,ISNULL(StatusDescription,'''') as StatusDescription,
					ISNULL(ActionCode,'''') as ActionCode, ISNULL(ActionName,'''') as ActionName, ISNULL(CreatedDate,'''') as CreatedDate,ISNULL(CreatedBy,'''') as CreatedBy,
					ISNULL(IsActive,'''') as IsActive
					from VIEW_ACI_IMMUNIZATION (NOLOCK) where PATIMMKEY =@PATIENTIMMKEY for xml path(''Immunization''),type
					  ) 
				  )
			  ,'''') 
			  ,''['','''')
			  ,'']'','''')
			  ,''}'','''')
			  --select @Route
			  if @IMMUNIZATION <>''''
				   select  @Route= REPLACE(
				   REPLACE(
				   ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select top 1 
						   ISNULL(Name,'''') AS Name,ISNULL(Code,'''') AS Code
							  from VIEW_ACI_IMMUNIZATION_ROUTE (NOLOCK) where  PATIMMKEY =@PATIENTIMMKEY  for xml path(''Route''),type
					  ) 
				  )
				   ,'''')
				   ,''['','''')
				   ,'']'','''')
				if @Route <>''''
				  set @route='',"Route": ''+@route

			  if @IMMUNIZATION <>''''
				   select  @Manufacturer= REPLACE(
				   REPLACE(
				   ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select top 1 
						   ISNULL(Name,'''') AS Name,ISNULL(Code,'''') AS Code
							  from VIEW_ACI_IMMUNIZATION_MANUFACTURER (NOLOCK) where  PATIMMKEY =@PATIENTIMMKEY  for xml path(''Manufacturer''),type
					  ) 
				  )
				   ,'''')
				   ,''['','''')
				   ,'']'','''')

				if @Manufacturer <>''''
				  set @Manufacturer='',"Manufacturer": ''+@Manufacturer

			  if @IMMUNIZATION <>''''
				   select  @DosageQuantity= REPLACE(
				   REPLACE(
				   ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select top 1 
						   ISNULL(Value,'''') AS Value,ISNULL(Units,'''') AS Units
							  from VIEW_ACI_IMMUNIZATION_DOSAGEQUANTITY (NOLOCK) where  PATIMMKEY =@PATIENTIMMKEY  for xml path(''DosageQuantity''),type
					  ) 
				  )
				   ,'''')
				   ,''['','''')
				   ,'']'','''')

				if @DosageQuantity <>''''
				  set @DosageQuantity='',"DosageQuantity": ''+@DosageQuantity

			  if @IMMUNIZATION <>''''
				   select  @Reason= REPLACE(
				   REPLACE(
				   ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select top 1 
						   ISNULL(Type,'''') AS Type,ISNULL(Code,'''') AS Code,ISNULL(Description,'''') AS Description,ISNULL(CodeSystemName,'''') AS CodeSystemName
						   ,ISNULL(CodeSystem,'''') AS CodeSystem
							  from VIEW_ACI_IMMUNIZATION_REASON (NOLOCK) where  PATIMMKEY =@PATIENTIMMKEY  for xml path(''Reason''),type
					  ) 
				  )
				   ,'''')
				   ,''['','''')
				   ,'']'','''')

				if @Reason <>''''
				  set @Reason='',"Reason": ''+@Reason

			if @IMMUNIZATION <>''''
			begin
			  --set @IMMUNIZATION =@IMMUNIZATION+@Route+@Manufacturer+@DosageQuantity+@Reason+''}'' -- As per Raghu Email, commneted Reason as there is no fields for code and desc
			  --set @IMMUNIZATION =@IMMUNIZATION+@Route+@Manufacturer+@DosageQuantity+ ''}''
			  set @IMMUNIZATION =@IMMUNIZATION+''}''
			  if @STRINGJSON_IMMUNIZATION =''''
			    set @STRINGJSON_IMMUNIZATION=@IMMUNIZATION
              else
			    set @STRINGJSON_IMMUNIZATION=@STRINGJSON_IMMUNIZATION+'',''+@IMMUNIZATION
			end
		FETCH NEXT FROM CURSORA_IMM INTO @PATIENTIMMKEY
	   END
	   CLOSE CURSORA_IMM
	   DEALLOCATE CURSORA_IMM 

 	
if @Print=1
   print 14

	IF @STRINGJSON_IMMUNIZATION <> '''' 
	BEGIN
	    SET @STRINGJSON_IMMUNIZATION =REPLACE(@STRINGJSON_IMMUNIZATION,''NULL'',''""'')	 
		SET @STRINGJSON_IMMUNIZATION = '',"Immunization":[''+@STRINGJSON_IMMUNIZATION	+'']''  

		update  ACI_JSON_MASTER set IMMUNIZATION =@STRINGJSON_IMMUNIZATION where ACIJSONMasterKey = @ACIJSONMasterKey
	END


--IMMUNIZATION-END=====================================================================================================================================*/
		    
/*MEDICALEQUIPMENT-BEGIN=================================================================================================================================*/
DECLARE @STRINGJSON_MEDEQ VARCHAR(max)
SET @STRINGJSON_MEDEQ =''''
	IF (SELECT COUNT(*) FROM VIEW_ACI_MEDICALEQUIPMENT (NOLOCK) WHERE  CHARTRECORDKEY=@CHARTRECORDKEY1)>0
	--	SELECT @STRINGJSON_MEDEQ='',"MEDICALEQUIPMENT":''+  DBO.ACI_QFN_XMLTOJSON (
		SELECT @STRINGJSON_MEDEQ= ISNULL(DBO.ACI_QFN_XMLTOJSON (
		(
		SELECT 
			ISNULL(ExternalId,'''') AS ExternalId,
			ISNULL(UDI,'''') AS UDI,
			ISNULL(AssigningAuthority,'''') AS AssigningAuthority,
			ISNULL(ScopingAuthority,'''') AS ScopingAuthority,
			ISNULL(DeviceName,'''') AS DeviceName,
			ISNULL(SupplierName,'''') AS SupplierName,
			ISNULL(StatusType,'''') AS StatusType,
			ISNULL(Statuscode,'''') AS Statuscode,
			ISNULL(Code,'''') AS Code,
			ISNULL(CodeSystem,'''') AS CodeSystem,
			ISNULL(CodeSystemName,'''') AS CodeSystemName,
			ISNULL(IsActive,'''') AS IsActive

		   FROM VIEW_ACI_MEDICALEQUIPMENT (NOLOCK) WHERE  CHARTRECORDKEY=@CHARTRECORDKEY1  FOR XML PATH(''MedicalEquipment''),TYPE ) ) 
		,'''')

	IF @STRINGJSON_MEDEQ <> '''' 
	BEGIN
	    SET @STRINGJSON_MEDEQ =REPLACE(@STRINGJSON_MEDEQ,''NULL'',''""'')	 
		SET @STRINGJSON_MEDEQ ='',"MedicalEquipment":''+@STRINGJSON_MEDEQ	  
		update  ACI_JSON_MASTER set MEDICALEQUIPMENT =@STRINGJSON_MEDEQ where ACIJSONMasterKey = @ACIJSONMasterKey
	END

if @Print=1
   print 13

--SET @STRINGJSON =@STRINGJSON+@STRINGJSON_MEDEQ
/*MEDICALEQUIPMENT-END=================================================================================================================================*/
/*MEDICATION-BEGIN===================================================================================================================================*/

DECLARE @PATIENTMEDICATIONSKEY VARCHAR(30) , @STRINGJSON_MEDICATION VARCHAR(MAX)
,@Medication Varchar(max),@Route_MED Varchar(8000), @DosageQuantity_MED Varchar(8000)
set @Medication =''''
set @Route_MED =''''
set @DosageQuantity_MED = ''''
set @STRINGJSON_MEDICATION =''''
	   DECLARE CURSORA_med SCROLL CURSOR FOR 
		select PATIENTMEDICATIONSKEY from VIEW_ACI_MEDICATIONS (NOLOCK) where ERXSOURCE is not null and CHARTRECORDKEY=@CHARTRECORDKEY1
	   OPEN CURSORA_med   
	   FETCH FIRST FROM CURSORA_med INTO @PATIENTMEDICATIONSKEY
	   WHILE @@FETCH_STATUS=0
	   BEGIN
		  select  @Medication= replace(--''}''
		  replace(--'']''
		  replace --''[''
		   (ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select TOP 1
						   ISNULL(ExternalId ,'''') AS ExternalId,ISNULL(DrugName ,'''') AS DrugName ,ISNULL(DrugCode ,''0'') AS DrugCode ,ISNULL(DrugCodeSystem ,'''') AS DrugCodeSystem,ISNULL(DrugCodeSystemName ,'''') AS DrugCodeSystemName,
						   ISNULL(MedicationType ,'''') AS MedicationType,ISNULL(PrescriptionType ,'''') AS PrescriptionType,ISNULL(Directions ,'''') AS Directions ,ISNULL(AdditionalInstructions ,'''') AS AdditionalInstructions,ISNULL(Strength ,'''') AS Strength,
						   ISNULL(Substitution ,'''') AS Substitution,ISNULL(StartDate ,'''') AS StartDate,ISNULL(EffectiveDate ,'''') AS EffectiveDate
						   ,ISNULL(DaysSupply ,'''') AS DaysSupply, -- Rev1.1
						   --ISNULL(DosageQuantityValue ,'''') AS DosageQuantityValue, -- Rev1.1
						   ISNULL(IsPrescribed ,'''') AS IsPrescribed,ISNULL(EprescribedDate ,'''') AS EprescribedDate,ISNULL(IsQueriedForDrugFormulary ,'''') AS IsQueriedForDrugFormulary,ISNULL(IsCPOE ,'''') AS IsCPOE,
						   --ISNULL(IsHighRiskMedication ,'''') AS IsHighRiskMedication, -- Rev 1.23
						   ISNULL(IsControlSubstance ,'''') AS IsControlSubstance,ISNULL(PharmacyName ,'''') AS PharmacyName,ISNULL(ReconciledDate ,'''') AS ReconciledDate,ISNULL(Status ,'''') AS Status,ISNULL(Active ,'''') AS IsActive 

						  from VIEW_ACI_MEDICATIONS (NOLOCK) where PATIENTMEDICATIONSKEY =@PATIENTMEDICATIONSKEY for xml path(''Medication1''),type
					  ) 
				  )
			  ,'''') 
			  ,''['','''')
			  ,'']'','''')
			  ,''}'','''')
			  --select @Route
             --Rev1.1 begin
			    if @DosageQuantity_MED <> ''''
			    set @DosageQuantity_MED= '',"DosageQuantity":''+@DosageQuantity_MED

                if @Medication<>''''
				   select @DosageQuantity_MED = REPLACE(
				   REPLACE(
				   ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select top 1 
						   ISNULL(Value,'''') AS Value,ISNULL(Units,'''') AS Units
							  from VIEW_ACI_MEDICATIONS_DOSAGEQUANTITY (NOLOCK) where  PATIENTMEDICATIONSKEY =@PATIENTMEDICATIONSKEY  for xml path(''DosageQuantity''),type
					  ) 
				  )
				   ,'''')
				   ,''['','''')
				   ,'']'','''')
			 --Rev1.1 end

			  if @Medication<>''''
				   select  @Route_MED= REPLACE(
				   REPLACE(
				   ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select top 1 
						   ISNULL(Code,'''') AS Code,ISNULL(Description,'''') AS Description,ISNULL(CodeSystem,'''') AS CodeSystem,ISNULL(CodeSystemName,'''') AS CodeSystemName
							  from VIEW_ACI_MEDICATIONS_ROUTE (NOLOCK) where  PATIENTMEDICATIONSKEY =@PATIENTMEDICATIONSKEY  for xml path(''Route''),type
					  ) 
				  )
				   ,'''')
				   ,''['','''')
				   ,'']'','''')

			  if @Route_MED <>''''
			  begin
	    		   set @Medication =@Medication+ '',"Route":''+@Route_MED+''}''
			   if @STRINGJSON_MEDICATION ='''' 
			      set @STRINGJSON_MEDICATION=@Medication
			   else          
				set @STRINGJSON_MEDICATION=@STRINGJSON_MEDICATION+'',''+@Medication
			    --r select @Procedures
			  end
         
		FETCH NEXT FROM CURSORA_med INTO @PATIENTMEDICATIONSKEY
	   END
	   CLOSE CURSORA_med
	   DEALLOCATE CURSORA_med
 
--select  ''"Medication":[''+@STRINGJSON_MEDICATION+'']''
 	
if @Print=1
   print 14

	IF @STRINGJSON_MEDICATION <> '''' 
	BEGIN
	    SET @STRINGJSON_MEDICATION =REPLACE(@STRINGJSON_MEDICATION,''NULL'',''""'')	 	 
		set @STRINGJSON_MEDICATION ='',"Medication":[''+@STRINGJSON_MEDICATION+'']''
		update  ACI_JSON_MASTER set MEDICATION =@STRINGJSON_MEDICATION where ACIJSONMasterKey = @ACIJSONMasterKey
	END


--SET @STRINGJSON =@STRINGJSON+@STRINGJSON_MEDICATION

/*MEDICATION-END===================================================================================================================================*/
/*PAYER-BEGIN===================================================================================================================================*/

DECLARE @INSURANCEKEY VARCHAR(30) , @STRINGJSON_PAYER VARCHAR(MAX)
,@Payer Varchar(1000),@Insurance Varchar(8000)
		set @Payer =''''
		set @Insurance =''''
		set @STRINGJSON_PAYER =''''
		DECLARE CURSORA_Ins SCROLL CURSOR FOR 
		  select INSURANCEKEY from VIEW_ACI_PAYER (NOLOCK) where  PATIENTKEY=@PATIENTKEY1
		OPEN CURSORA_Ins   
		FETCH FIRST FROM CURSORA_Ins INTO @INSURANCEKEY
		WHILE @@FETCH_STATUS=0
		BEGIN
			select  @Payer= replace(--''}''
			replace(--'']''
			replace --''[''
			 (ISNULL( dbo.ACI_qfn_XmlToJson(	
					(select 
							ISNULL(PayerCode,'''') AS PayerCode				
							from VIEW_ACI_PAYER (NOLOCK) where INSURANCEKEY =@INSURANCEKEY for xml path(''Payer1''),type
						) 
					)
				,'''') 
				,''['','''')
				,'']'','''')
				,''}'','''')
				--select @Route
				if @Payer<>''''
					select  @Insurance= REPLACE(
					REPLACE(
					ISNULL( dbo.ACI_qfn_XmlToJson(	
					(select top 1 
							 ISNULL(Type,'''') AS Type,ISNULL(Description,'''') AS Description,ISNULL(Participant,'''') AS Participant,ISNULL(Number,'''') AS Number,
							 ISNULL(Code,'''') AS Code,ISNULL(StartDate,'''') AS StartDate,ISNULL(ExpiryDate,'''') AS ExpiryDate,ISNULL(PhoneNumber,'''') AS PhoneNumber 
								from VIEW_ACI_PAYER_INSURANCE (NOLOCK) where  INSURANCEKEY =@INSURANCEKEY   for xml path(''Insurance1''),type
						) 
					)
					  ,'''')
					  ,''['','''')
					  ,'']'','''')

				if @Insurance <>''''
				begin
	    			set @Payer =@Payer+'',"Insurance":''+@Insurance+''}''
				  if @STRINGJSON_PAYER ='''' 
				  set @STRINGJSON_PAYER=@Payer
				  else          
				  set @STRINGJSON_PAYER=@STRINGJSON_PAYER+'',''+@Payer
				 --r select @Procedures
				 end
		         
		  FETCH NEXT FROM CURSORA_Ins INTO @INSURANCEKEY
		END
		CLOSE CURSORA_Ins
		DEALLOCATE CURSORA_Ins
 
--select  '',"Payer":[''+@STRINGJSON_PAYER+'']''

if @Print=1
   print 15

	IF @STRINGJSON_PAYER <> '''' 
	BEGIN
	    SET @STRINGJSON_PAYER =REPLACE(@STRINGJSON_PAYER,''NULL'',''""'')	 	 
		set @STRINGJSON_PAYER ='',"Payer":[''+@STRINGJSON_PAYER+'']''
		update  ACI_JSON_MASTER set PAYER = @STRINGJSON_PAYER where ACIJSONMasterKey = @ACIJSONMasterKey
	END


/*PAYER-END=====================================================================================================================================*/

/*PLANOFTREATMENT-BEGIN===================================================================================================================================*/
DECLARE @STRINGJSON_PLANOFTREATMENT VARCHAR(max)
SET @STRINGJSON_PLANOFTREATMENT =''''
     IF (SELECT COUNT(*) FROM VIEW_ACI_PLANOFTREATMENT (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
		--SELECT @STRINGJSON_PLANOFTREATMENT=@STRINGJSON_PLANOFTREATMENT+'',"PLANOFTREATMENT":''+  DBO.ACI_QFN_XMLTOJSON (
		SELECT @STRINGJSON_PLANOFTREATMENT=ISNULL( DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT 
				 ISNULL(ExternalId,'''') AS ExternalId,
				 ISNULL(PlanCode,'''') AS PlanCode,
				 ISNULL(PlanDescription,'''') AS PlanDescription,
				 ISNULL(PlanCodeSystem,'''') AS PlanCodeSystem,
				 ISNULL(PlanCodeSystemName,'''') AS PlanCodeSystemName,
				 ISNULL(PriorityPreferenceCode,'''') AS PriorityPreferenceCode,
				 ISNULL(PriorityPreferenceDescription,'''') AS PriorityPreferenceDescription,
				 ISNULL(PriorityPreferenceCodeSystem,'''') AS PriorityPreferenceCodeSystem,
				 ISNULL(PriorityPreferenceCodeSystemName,'''') AS PriorityPreferenceCodeSystemName,
				 ISNULL(CareGiverCode,'''') AS CareGiverCode,
				 ISNULL(CareGiverDescription,'''') AS CareGiverDescription,
				 ISNULL(CareGiverCodeSystem,'''') AS CareGiverCodeSystem,
				 ISNULL(CareGiverCodeSystemName,'''') AS CareGiverCodeSystemName,
				 ISNULL(FollowupVisit,'''') AS FollowupVisit, -- Rev 1.23
				 ISNULL(HealthConcerns,'''') AS HealthConcerns,
				 ISNULL(Goals,'''') AS Goals,
				 ISNULL(Instructions,'''') AS Instructions,
				 ISNULL(Status,'''') AS Status,
				 ISNULL(IsActive,'''') AS IsActive 
		    FROM VIEW_ACI_PLANOFTREATMENT  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''PLANOFTREATMENT''),TYPE ) ) 
			,'''')
if @Print=1
   print 16

	IF @STRINGJSON_PLANOFTREATMENT <> '''' 
	BEGIN
	    SET @STRINGJSON_PLANOFTREATMENT =REPLACE(@STRINGJSON_PLANOFTREATMENT,''NULL'',''""'')	 	 
		set @STRINGJSON_PLANOFTREATMENT ='',"PlanOfTreatment":''+@STRINGJSON_PLANOFTREATMENT
		update  ACI_JSON_MASTER set PLANOFTREATMENT = @STRINGJSON_PLANOFTREATMENT where ACIJSONMasterKey = @ACIJSONMasterKey
	END



--SET @STRINGJSON =@STRINGJSON+@STRINGJSON_PLANOFTREATMENT
/*PLANOFTREATMENT-END=====================================================================================================================================*/


/*ProblemsList-BEGIN===================================================================================================================================*/
DECLARE @PATIENTPROBLEMLISTKEY VARCHAR(30), @STRINGJSON_ProblemsList VARCHAR(MAX), @ProblemsList VARCHAR(1000), @PRoblem_Code VARCHAR(8000);
SET @ProblemsList = '''';
SET @PRoblem_Code = '''';
SET @STRINGJSON_ProblemsList = '''';
DECLARE CURSORA_PPL SCROLL CURSOR
FOR
    SELECT PATIENTPROBLEMLISTKEY
    FROM VIEW_ACI_PROBLEMSLIST(NOLOCK)
    WHERE CHARTRECORDKEY = @CHARTRECORDKEY1;
OPEN CURSORA_PPL;
FETCH FIRST FROM CURSORA_PPL INTO @PATIENTPROBLEMLISTKEY;
WHILE @@FETCH_STATUS = 0
    BEGIN
SET @ProblemsList = '''';
SET @PRoblem_Code = '''';
        SELECT @ProblemsList = replace(--''}''
               replace(--'']''
               replace --''[''
               (ISNULL(dbo.ACI_qfn_XmlToJson
                      (
                      (
                          SELECT CASE
                                     WHEN CONVERT(VARCHAR, View_Problem.Onset, 101) = CONVERT(VARCHAR, View_Problem.AppointmentDate, 101)
                                     THEN ''EncounterDiagnosis''
                                     ELSE ''PastProblem''
                                 END Type,
                                 ISNULL(ExternalId, '''') AS ExternalId,
                                 ISNULL(OnSetDate, '''') AS OnSetDate,
                                 ISNULL(ResolutionDate, '''') AS ResolutionDate,
                                 ISNULL(ReconciledDate, '''') AS ReconciledDate,
                                 ISNULL(View_Problem.Status, '''') AS Status,
                                 ISNULL(IsActive, '''') AS IsActive
                          FROM VIEW_ACI_PROBLEMSLIST View_Problem(NOLOCK)
                               JOIN dbo.appointments ap ON ap.appointmentid = View_Problem.CHARTRECORDKEY
                          WHERE View_Problem.PATIENTPROBLEMLISTKEY = @PATIENTPROBLEMLISTKEY FOR XML PATH(''ProblemsListA''), TYPE
                      )
                      ), ''''), ''['', ''''), '']'', ''''), ''}'', '''');
			   -- select @ProblemsList
        IF @ProblemsList <> ''''
            SELECT @PRoblem_Code = -- REPLACE(
				--	REPLACE(
                   ISNULL(dbo.ACI_qfn_XmlToJson
                         (
                         (
                             SELECT ISNULL(Type, '''') AS Type,
                                    ISNULL(ProblemCodeValue, '''') AS ProblemCodeValue,
                                    ISNULL(ProblemCodeDescription, '''') AS ProblemCodeDescription,
                                    ISNULL(ProblemCodeSystem, '''') AS ProblemCodeSystem,
                                    ISNULL(ProblemCodeName, '''') AS ProblemCodeName
                             FROM VIEW_ACI_PROBLEMSLIST_PROBLEMCODE(NOLOCK)
                             WHERE PATIENTPROBLEMLISTKEY = @PATIENTPROBLEMLISTKEY FOR XML PATH(''ProblemCodeA''), TYPE
                         )
                         ), '''');
					  --,''['','''')
					  --,'']'','''')

        IF @PRoblem_Code <> ''''
            BEGIN
                SET @ProblemsList = @ProblemsList+'',"ProblemCode":''+@PRoblem_Code+''}'';
                IF @STRINGJSON_ProblemsList = ''''
                    SET @STRINGJSON_ProblemsList = @ProblemsList;
                    ELSE
                SET @STRINGJSON_ProblemsList = @STRINGJSON_ProblemsList+'',''+@ProblemsList;
				 --r select @Procedures
        END;
        FETCH NEXT FROM CURSORA_PPL INTO @PATIENTPROBLEMLISTKEY;
    END;
CLOSE CURSORA_PPL;
DEALLOCATE CURSORA_PPL;
 
--select  '',"ProblemsList":[''+@STRINGJSON_ProblemsList+'']''

IF @STRINGJSON_ProblemsList <> ''''
    BEGIN
        SET @STRINGJSON_ProblemsList = REPLACE(@STRINGJSON_ProblemsList, ''NULL'', ''""'');
        SET @STRINGJSON_ProblemsList = '',"ProblemsList":[''+@STRINGJSON_ProblemsList+'']'';
        UPDATE ACI_JSON_MASTER
          SET
              PROBLEMSLIST = @STRINGJSON_ProblemsList
        WHERE ACIJSONMasterKey = @ACIJSONMasterKey;
END;
IF @Print = 1
    PRINT 17;

--SET @STRINGJSON =@STRINGJSON+@STRINGJSON_ProblemsList
 
/*ProblemsList-END=====================================================================================================================================*/


/*Procedures-BEGIN===================================================================================================================================*/
DECLARE @STRINGJSON_Procedures VARCHAR(max)
SET @STRINGJSON_Procedures =''''

		DECLARE @COLLECTEDBILLINGKEY VARCHAR(30) 
		,@Procedures Varchar(8000),@Procedure_Codes Varchar(8000),@Reason_PROC varchar(8000)
		set @Procedures =''''
		set @Procedure_Codes =''''
		set @STRINGJSON_Procedures =''''
		set @Reason_PROC  = ''''
		DECLARE CURSORProc SCROLL CURSOR FOR 
		  select COLLECTEDBILLINGKEY from VIEW_ACI_PROCEDURES (NOLOCK) where --PROCEDUREKEY is not null and
		   CHARTRECORDKEY=@CHARTRECORDKEY1
		OPEN CURSORProc   
		FETCH FIRST FROM CURSORProc INTO @COLLECTEDBILLINGKEY
		WHILE @@FETCH_STATUS=0
		BEGIN
				 set @Procedures=''''
				 set @Procedure_Codes=''''
		
			select  @Procedures= replace(--''}''
			replace(--'']''
			replace --''[''
			 (ISNULL( dbo.ACI_qfn_XmlToJson(	
					(select 
					   ISNULL(ExternalId,'''') AS ExternalId, 
					   ISNULL(Status,'''') AS Status,
					   ISNULL(IsInHouseProcedure,'''') AS IsInHouseProcedure,
						--ISNULL(Reason,'''') AS Reason, -- Rev1.1
					--	ISNULL(TargetSite,'''') AS TargetSite, -- Rev1.1
						ISNULL(PerformedBy,'''') AS PerformedBy, 
						ISNULL(DateofProcedure,'''') AS DateofProcedure, 
						ISNULL(IsActive,'''') AS IsActive
							from VIEW_ACI_PROCEDURES (NOLOCK) where --PROCEDUREKEY is not null and
							 COLLECTEDBILLINGKEY=@COLLECTEDBILLINGKEY for xml path(''Procedures1''),type
						) 
					)
				,'''') 
				,''['','''')
				,'']'','''')
				,''}'','''')
				--select @Procedures
				if @Procedures<>''''
					select  @Procedure_Codes= ISNULL( dbo.ACI_qfn_XmlToJson(	
					(select 
							 ISNULL(ProcedureCodeValue,'''') AS ProcedureCodeValue, ISNULL(ProcedureCodeDescription,'''') AS ProcedureCodeDescription, 
									ISNULL(ProcedureCodeSystem,'''') AS ProcedureCodeSystem, ISNULL(ProcedureCodeName,'''') AS ProcedureCodeName
		 				

								from VIEW_ACI_PROCEDURES_PROCEDURECODE (NOLOCK) where -- PROCEDUREKEY is not null and 
								 COLLECTEDBILLINGKEY=@COLLECTEDBILLINGKEY  for xml path(''Procedure_CODEs''),type
						) 
					)
				,'''')

				-- Rev1.1 Reason begin
				declare @Specimen1 Varchar(1000)
				set @Specimen1=''''
				if @Procedures <>''''
					select  @Specimen1=Replace( Replace ( ISNULL( dbo.ACI_qfn_XmlToJson(	
					(select top 1
							ISNULL(Name,'''') AS Name, ISNULL(Code,'''') AS Code 				
								from VIEW_ACI_PROCEDURES_SPECIMEN (NOLOCK) where COLLECTEDBILLINGKEY=@COLLECTEDBILLINGKEY  for xml path(''level12''),type
						) 
					)
			      	,''''),
				    ''['','''')
					,'']'','''')
                    If @Specimen1 <>''''
				  set @Specimen1='',"Specimen": ''+@Specimen1

		 
				declare @Target1 Varchar(1000)
				set @Target1=''''
				if @Procedures <>''''
					select  @Target1=Replace( Replace (ISNULL( dbo.ACI_qfn_XmlToJson(	
					(select top 1
							ISNULL(Name,'''') AS Description, ISNULL(Code,'''') AS Code ,ISNULL(CodeSystemName,'''') AS CodeSystemName,ISNULL(CodeSystem,'''') AS CodeSystem				
								from VIEW_ACI_PROCEDURES_TARGET (NOLOCK) where COLLECTEDBILLINGKEY=@COLLECTEDBILLINGKEY  for xml path(''level2''),type
						) 
					)
			      	,''''),
					''['','''')
					,'']'','''')
                    If @Target1 <>''''
				  set @Target1='',"TargetSite": ''+@Target1

		 
				declare @Device1 Varchar(1000)
				set @Device1=''''
				if @Procedures <>''''
					select  @Device1=Replace( Replace (ISNULL( dbo.ACI_qfn_XmlToJson(	
					(select top 1
							ISNULL(Name,'''') AS Description, ISNULL(Code,'''') AS Code ,ISNULL(CodeSystemName,'''') AS CodeSystemName,ISNULL(CodeSystem,'''') AS CodeSystem				
								from VIEW_ACI_PROCEDURES_DEVICE (NOLOCK) where COLLECTEDBILLINGKEY=@COLLECTEDBILLINGKEY  for xml path(''level2''),type
						) 
					)
			      	,''''),
					''['','''')
					,'']'','''')
                    If @Device1 <>''''
				  set @Device1='',"Device": ''+@Device1


                   if @Procedures <>''''
					   select  @Reason_PROC= REPLACE(
					   REPLACE(
					   ISNULL( dbo.ACI_qfn_XmlToJson(	
					  (select top 1 
							   ISNULL(Type,'''') AS Type,ISNULL(Code,'''') AS Code,ISNULL(Description,'''') AS Description,ISNULL(CodeSystemName,'''') AS CodeSystemName
							   ,ISNULL(CodeSystem,'''') AS CodeSystem
								  from VIEW_ACI_PROCEDURES_REASON (NOLOCK) where COLLECTEDBILLINGKEY=@COLLECTEDBILLINGKEY  for xml path(''Reason''),type
						  ) 
					  )
					   ,'''')
					   ,''['','''')
					   ,'']'','''')
                    
				if @Reason_PROC <>''''
				  set @Reason_PROC='',"Reason": ''+@Reason_PROC
				  -- Rev1.1 Reason end

				if @Procedure_Codes <>''''
				begin
				  set @Procedures =@Procedures+@Target1+@Device1+@Reason_PROC+'',"ProcedureCode":''+@Procedure_Codes+''}''
				  if @STRINGJSON_Procedures ='''' 
				  set @STRINGJSON_Procedures=@Procedures
				  else          
				  set @STRINGJSON_Procedures=@STRINGJSON_Procedures+'',''+@Procedures
				 --r select @Procedures
				 end
		         
		  FETCH NEXT FROM CURSORProc INTO @COLLECTEDBILLINGKEY
		END
		CLOSE CURSORProc
		DEALLOCATE CURSORProc
		--if @STRINGJSON_Procedures <>''''
		--select  ''"Procedures":[''+@STRINGJSON_Procedures+'']''
 

   
	IF @STRINGJSON_Procedures  <> '''' 
	BEGIN
	    SET @STRINGJSON_Procedures = '',"Procedures":[''+@STRINGJSON_Procedures+'']''
	    SET @STRINGJSON_Procedures  =REPLACE(@STRINGJSON_Procedures ,''NULL'',''""'')	 	 		
		update  ACI_JSON_MASTER set PROCEDURES = @STRINGJSON_Procedures  where ACIJSONMasterKey = @ACIJSONMasterKey
	END
if @Print=1
   print 18


--SET @STRINGJSON =@STRINGJSON+@STRINGJSON_Procedures
/*Procedures-END=====================================================================================================================================*/


/*Result-BEGIN===================================================================================================================================*/
DECLARE @PATIENTORDERKEY VARCHAR(30) , @STRINGJSON_Result VARCHAR(MAX)
,@RESULT Varchar(8000),@LABADDRESS Varchar(5000),@LABPHONE varchar(2000)
set @RESULT =''''
set @LABADDRESS =''''
set @STRINGJSON_Result =''''
set @LABPHONE=''''
	   DECLARE CURSORA_PPL SCROLL CURSOR FOR 
		select PATIENTORDERKEY from VIEW_ACI_RESULTS (NOLOCK) where  CHARTRECORDKEY=@CHARTRECORDKEY1
	   OPEN CURSORA_PPL   
	   FETCH FIRST FROM CURSORA_PPL INTO @PATIENTORDERKEY
	   WHILE @@FETCH_STATUS=0
	   BEGIN
		  select  @RESULT= replace(--''}''
		  replace(--'']''
		  replace --''[''
		   (ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select 			
							    ISNULL(ExternalId,'''') AS ExternalId,ISNULL(Type,'''') AS Type,ISNULL(TestName,'''') AS TestName,ISNULL(Code,'''') AS Code,ISNULL(CodeSystem,'''') AS CodeSystem,
							    ISNULL(CodeSystemName,'''') AS CodeSystemName,ISNULL(Value,'''') AS Value,ISNULL(Units,'''') AS Units,ISNULL(ReferenceRange,'''') AS ReferenceRange,
							    ISNULL(Interpretation,'''') AS Interpretation,ISNULL(Date,'''') AS Date,
								ISNULL(LabIdentifier,'''') AS LabIdentifier, -- Rev 1.23
								ISNULL(LabName,'''') AS LabName,ISNULL(ISACTIVE,'''') AS ISACTIVE
					   from VIEW_ACI_RESULTS 
					   where  PATIENTORDERKEY =@PATIENTORDERKEY  for xml path(''RESULT''),type	    
					  ) 
				  )
			  ,'''') 
			  ,''['','''')
			  ,'']'','''')
			  ,''}'','''')
			 -- select @ProblemsList
			  if @RESULT<>''''
				   select  @LABADDRESS= REPLACE(
				   REPLACE(
				   ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select 
									    ISNULL(AddressLine1,'''') AS AddressLine1,ISNULL(AddressLine2,'''') AS AddressLine2,ISNULL(City,'''') AS City,ISNULL(State,'''') AS State,
									    ISNULL(Zip,'''') AS Zip,ISNULL(County,'''') AS County,ISNULL(CountyCode,'''') AS CountyCode
							  from VIEW_ACI_RESULTS_LABADDRESS (NOLOCK) where  PATIENTORDERKEY =@PATIENTORDERKEY  for xml path(''LABADDRESS''),type
					  ) 
				  )
				   ,'''')
				   ,''['','''')
				   ,'']'','''')
	          
			  if @RESULT<>''''
				   select  @LABPHONE=			
				   ISNULL( dbo.ACI_qfn_XmlToJson(	
				  (select 
						    ISNULL(Type,'''') AS Type,ISNULL(Number,'''') AS Number
							from VIEW_ACI_RESULTS_LABPHONE (NOLOCK) where  PATIENTORDERKEY =@PATIENTORDERKEY   for xml path(''LABPHONEa''),type
					  ) 
				  )
				   ,'''')        
	          
				if @LABADDRESS<>''''
				  set @LABADDRESS ='',"LabAddress":''+ @LABADDRESS  
				if @LABPHONE<>''''
				  set @LABPHONE ='',"LabPhone":''+ @LABPHONE  	         
 
			  if @RESULT <>''''
			  begin
	    		   set @RESULT =@RESULT+@LABADDRESS+@LABPHONE+''}''
			   if @STRINGJSON_Result ='''' 
			   set @STRINGJSON_Result=@RESULT
			   else          
				set @STRINGJSON_Result=@STRINGJSON_Result+'',''+@RESULT
			    --r select @Procedures
			  end
         
		FETCH NEXT FROM CURSORA_PPL INTO @PATIENTORDERKEY
	   END
	   CLOSE CURSORA_PPL
	   DEALLOCATE CURSORA_PPL



	IF @STRINGJSON_Result  <> '''' 
	BEGIN
	    SET @STRINGJSON_Result  =REPLACE(@STRINGJSON_Result ,''NULL'',''""'')	 	 
		set @STRINGJSON_Result  ='',"Result":[''+@STRINGJSON_Result+'']''
		update  ACI_JSON_MASTER set RESULT = @STRINGJSON_Result  where ACIJSONMasterKey = @ACIJSONMasterKey
	END
if @Print=1
   print 19
--SET @STRINGJSON =@STRINGJSON+@STRINGJSON_Result
/*Result-END=====================================================================================================================================*/

/*SocialHistory-BEGIN===================================================================================================================================*/
DECLARE @STRINGJSON_SocialHistory VARCHAR(max)
SET @STRINGJSON_SocialHistory =''''
     IF (SELECT COUNT(*) FROM VIEW_ACI_SOCIALHISTORY (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
--		SELECT @STRINGJSON_SocialHistory=@STRINGJSON_SocialHistory+'',"SocialHistory":''+isnull(DBO.ACI_QFN_XMLTOJSON (
		SELECT @STRINGJSON_SocialHistory=isnull(DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT 
				 ISNULL(ExternalId,'''') AS ExternalId,
				 ISNULL(Type,'''') AS Type,
				 ISNULL(Name,'''') AS Name,
				 ISNULL(Code,'''') AS Code,
				 ISNULL(CodeSystem,'''') AS CodeSystem,
				 ISNULL(CodeSystemName,'''') AS CodeSystemName,
				 ISNULL(Date,'''') AS Date,
				 ISNULL(EndDate,'''')  as EndDate,
				 ISNULL(IsActive,'''') AS IsActive
			FROM VIEW_ACI_SOCIALHISTORY  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''SocialHistory''),TYPE ) ) ,'''')

	IF @STRINGJSON_SocialHistory  <> '''' 
	BEGIN
	    SET @STRINGJSON_SocialHistory  =REPLACE(@STRINGJSON_SocialHistory ,''NULL'',''""'')	 	 
		set @STRINGJSON_SocialHistory  ='',"SocialHistory":''+@STRINGJSON_SocialHistory 
		update  ACI_JSON_MASTER set SOCIALHISTORY = @STRINGJSON_SocialHistory  where ACIJSONMasterKey = @ACIJSONMasterKey
	END
if @Print=1
   print 20


--SET @STRINGJSON =@STRINGJSON+@STRINGJSON_SocialHistory
/*SocialHistory-END=====================================================================================================================================*/


/*VitalSign-BEGIN===================================================================================================================================*/
DECLARE @STRINGJSON_VitalSign VARCHAR(max),@vitalskey  int 
SET @STRINGJSON_VitalSign =''''
set @vitalskey =0
select top 1 @vitalskey= pc.appointmentid  
FROM DBO.PATIENTCLINICAL PC WITH (NOLOCK)
	left join  dbo.appointments ap WITH(NOLOCK)
	on pc.appointmentid = ap.appointmentid
     WHERE ((SYMPTOM LIKE ''/BLOOD P%''
           AND FINDINGDETAIL LIKE ''*BLOODPRESSURE%'')OR
		   (SYMPTOM LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*HEIGHT%'') OR (SYMPTOM LIKE ''/HEIGHT AND WEIGHT'' AND FINDINGDETAIL LIKE ''*WEIGHT%''))
		 AND PC.appointmentid =@CHARTRECORDKEY1
order by CLINICALID desc
IF (SELECT COUNT(*) FROM [VIEW_ACI_VITALSIGN] (NOLOCK) WHERE vitalskey=@vitalskey )>0
begin   
		SELECT @STRINGJSON_VitalSign=ISNULL( REPLACE(REPLACE(DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT top 1
                ISNULL(ExternalId,'''') AS ExternalId
			FROM VIEW_ACI_VITALSIGN  (NOLOCK)  WHERE vitalskey=@vitalskey  FOR XML PATH(''VitalSign''),TYPE ) 
			 ),''['','''') ,''}]'','''')
			 ,'''')

     
		SELECT @STRINGJSON_VitalSign=@STRINGJSON_VitalSign+'',"Measures":''+ISNULL( REPLACE(REPLACE(DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT 
				ISNULL(Name,'''') AS Name,
				ISNULL(NameCode,'''') AS NameCode, -- Rev1.1
				ISNULL(NameCodeSystem,'''') AS NameCodeSystem, -- Rev1.1
				ISNULL(NameCodeSystemName,'''') AS NameCodeSystemName, -- Rev1.1
				ISNULL(DateTime,'''') as DateTime, -- Rev 1.23
				ISNULL(Value,'''') AS Value,
				ISNULL(Units,'''') AS Units, 
				ISNULL(UnitsCode,'''') AS UnitsCode, -- Rev1.1 
				ISNULL(UnitsCodeSystem,'''') AS UnitsCodeSystem, -- Rev1.1
				ISNULL(UnitsCodeSystemName,'''') AS UnitsCodeSystemName, -- Rev1.1
				ISNULL(Notes,'''') AS Notes -- Rev1.1
				               
			FROM VIEW_ACI_VITALSIGN_MEASURES  (NOLOCK)  WHERE vitalskey=@vitalskey FOR XML PATH(''Measures''),TYPE ) 
			 ),''['',''['') ,'']'','']''),'''')
       +''}''
end
	IF @STRINGJSON_VitalSign  <> '''' 
	BEGIN
	    SET @STRINGJSON_VitalSign  =REPLACE(@STRINGJSON_VitalSign ,''NULL'',''""'')	 	 
		set @STRINGJSON_VitalSign  ='',"VitalSign":''+@STRINGJSON_VitalSign 
		update  ACI_JSON_MASTER set VITALSIGN = @STRINGJSON_VitalSign  where ACIJSONMasterKey = @ACIJSONMasterKey
	END

if @Print=1
   print 21

--SET @STRINGJSON =@STRINGJSON+@STRINGJSON_VitalSign
/*VitalSign-END=====================================================================================================================================*/

/*Instructions-BEGIN===================================================================================================================================*/
DECLARE @STRINGJSON_Instructions VARCHAR(max)
SET @STRINGJSON_Instructions =''''
     IF (SELECT COUNT(*) FROM VIEW_ACI_INSTRUCTIONS (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
		--SELECT @STRINGJSON_Instructions=@STRINGJSON_Instructions+'',"INSTRUCTIONS":''+  DBO.ACI_QFN_XMLTOJSON (
        SELECT @STRINGJSON_Instructions=ISNULL(DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT 
				ISNULL(ExternalId,'''') AS ExternalId,
				ISNULL(Description,'''') AS Description,
				ISNULL(IsActive,'''') AS IsActive
 			FROM VIEW_ACI_INSTRUCTIONS  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''Instructions''),TYPE ) ) 
			,'''')

	IF @STRINGJSON_Instructions  <> '''' 
	BEGIN
	    SET @STRINGJSON_Instructions  =REPLACE(@STRINGJSON_Instructions ,''NULL'',''""'')	 	 
		set @STRINGJSON_Instructions  ='',"Instructions":''+@STRINGJSON_Instructions 
		update  ACI_JSON_MASTER set INSTRUCTIONS = @STRINGJSON_Instructions  where ACIJSONMasterKey = @ACIJSONMasterKey
	END

if @Print=1
   print 22

--SET @STRINGJSON =@STRINGJSON+@STRINGJSON_Instructions
/*Instructions-END=====================================================================================================================================*/

/*PhysicalExam-BEGIN===================================================================================================================================*/
/*
--Dont send this section in JSon
DECLARE @STRINGJSON_PhysicalExam VARCHAR(2000)
SET @STRINGJSON_PhysicalExam =''''
     IF (SELECT COUNT(*) FROM VIEW_ACI_PHYSICALEXAM (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
		SELECT @STRINGJSON_PhysicalExam=@STRINGJSON_PhysicalExam+'',"PhysicalExam":''+ REPLACE(REPLACE(DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT                 
				ISNULL(SEQUENCEID,'''') AS SEQUENCEID,
				ISNULL(LATERALITY,'''') AS LATERALITY,
				ISNULL(CREATEDDATE,'''') AS CREATEDDATE,
				ISNULL(DILATIONRESULT,'''') AS DILATIONRESULT,
				ISNULL(LEVELOFSEVERITY,'''') AS LEVELOFSEVERITY,
				ISNULL(IOP,'''') AS IOP,
				ISNULL(REFRACTIVE,'''') AS REFRACTIVE,
				ISNULL(KERATOMETRY,'''') AS KERATOMETRY,
				ISNULL(PUPILS,'''') AS PUPILS,
				ISNULL(ANTERIORSEGMENT,'''') AS ANTERIORSEGMENT,
				ISNULL(POSTERIORSEGMENT,'''') AS POSTERIORSEGMENT,
				ISNULL(ISACTIVE,'''') AS ISACTIVE
			FROM VIEW_ACI_PHYSICALEXAM  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''PhysicalExam''),TYPE ) 
			 ),''['',''['') ,''}]'','''')
 

     IF (SELECT COUNT(*) FROM VIEW_ACI_PHYSICALEXAM_EXAMFIELD (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
		SELECT @STRINGJSON_PhysicalExam=@STRINGJSON_PhysicalExam+'',"ExamField":''+ REPLACE(REPLACE(DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT 
				 ISNULL(TYPE,'''') AS TYPE,
				 ISNULL(CODE,'''') AS CODE,r
				 ISNULL(DESCRIPTION,'''') AS DESCRIPTION,
				 ISNULL(CODESYSTEM,'''') AS CODESYSTEM,
				 ISNULL(CODESYSTEMNAME,'''') AS CODESYSTEMNAME
			FROM VIEW_ACI_PHYSICALEXAM_EXAMFIELD  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''ExamField''),TYPE ) 
			 ),''['',''['') ,'']'','']'')

     IF (SELECT COUNT(*) FROM VIEW_ACI_PHYSICALEXAM_EXTERNALEYE (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
		SELECT @STRINGJSON_PhysicalExam=@STRINGJSON_PhysicalExam+'',"ExternalEye":''+ REPLACE(REPLACE(DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT 
					ISNULL(FINDING,'''') AS FINDING,
					ISNULL(CODE,'''') AS CODE
			FROM VIEW_ACI_PHYSICALEXAM_EXTERNALEYE  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''ExternalEye''),TYPE ) 
			 ),''['',''['') ,'']'','']'')+''}]''

 
SET @STRINGJSON =@STRINGJSON+@STRINGJSON_PhysicalExam
*/
/*PhysicalExam-END=====================================================================================================================================*/

/*PhysicalExam-BEGIN===================================================================================================================================*/
--Rev1.1 PHYSICALEXAM - BEGIN


--DECLARE @STRINGJSON_PhysicalExam VARCHAR(8000),@Examkey VARCHAR(30),
--@PHYSICALEXAM Varchar(max), 
--@Eye Varchar(8000),
--@REASON_PHYEXAM varchar(8000),
--@ExamField varchar(8000),
--@ExternalEye varchar(8000)

--SET @STRINGJSON_PhysicalExam =''''
--SET @PHYSICALEXAM = ''''
--		 DECLARE CURSORA_PHYEXAM SCROLL CURSOR FOR 
--			select CHARTRECORDKEY from VIEW_ACI_PHYSICALEXAM (NOLOCK) where  CHARTRECORDKEY=@CHARTRECORDKEY1
--			OPEN CURSORA_PHYEXAM   
--			FETCH FIRST FROM CURSORA_PHYEXAM INTO @Examkey
--			WHILE @@FETCH_STATUS=0
--			BEGIN
--				set @PHYSICALEXAM =''''
-- 				set @Eye = ''''
--				set @REASON_PHYEXAM = ''''
--				set @ExamField = ''''
--				set @ExternalEye = ''''

--		  select  @PHYSICALEXAM= replace(--''}''
--		  replace(--'']''
--		  replace --''[''
--		   (ISNULL( dbo.ACI_qfn_XmlToJson(	
--				  (select 
--						ISNULL(CreatedDate,'''') as CreatedDate,
--						ISNULL(IsActive,'''') as IsActive
--					from VIEW_ACI_PHYSICALEXAM (NOLOCK) where CHARTRECORDKEY =@Examkey for xml path(''PhysicalExam''),type
--					  ) 
--				  )
--			  ,'''') 
--			  ,''['','''')
--			  ,'']'','''')
--			  ,''}'','''')

-- 			  if @PHYSICALEXAM <>''''
--				   select  @Eye= --REPLACE(
--				   --REPLACE(
--				   ISNULL( dbo.ACI_qfn_XmlToJson(	
--				  (select top 1 
--						   ISNULL(Laterality,'''') AS Laterality,
--						   ISNULL(DilationResult,'''') AS DilationResult,
--						   ISNULL(IOP,'''') AS IOP,
--						   ISNULL(Refractive,'''') AS Refractive,
--						   ISNULL(Keratometry,'''') AS Keratometry,
--						   ISNULL(Pupils,'''') AS Pupils,
--						   ISNULL(AnteriorSegment,'''') AS AnteriorSegment,
--						   ISNULL(PosteriorSegment,'''') AS PosteriorSegment
--							  from VIEW_ACI_PHYSICALEXAM_EYE (NOLOCK) where  CHARTRECORDKEY =@Examkey  for xml path(''Eye''),type
--					  )  
--				  )
--				   ,'''')
--				  -- ,''['','''')
--				  -- ,'']'','''')

--				if @Eye <>''''
--				  set @Eye='',"Eye": ''+@Eye

-- 			  if @PHYSICALEXAM <>''''
--				   select  @ExamField= --REPLACE(
--				  -- REPLACE(
--				   ISNULL( dbo.ACI_qfn_XmlToJson(	
--				  (select top 1 
--						   ISNULL(Type,'''') AS Type,ISNULL(Code,'''') AS Code,ISNULL(Description,'''') AS Description,ISNULL(CodeSystem,'''') AS CodeSystem
--						   ,ISNULL(CodeSystemName,'''') AS CodeSystemName
--							  from VIEW_ACI_PHYSICALEXAM_EXAMFIELD (NOLOCK) where  CHARTRECORDKEY =@Examkey  for xml path(''ExamField''),type
--					  )  
--				  )
--				   ,'''')
--				   --,''['','''')
--				   --,'']'','''')

--				if @ExamField <>''''
--				  set @ExamField='',"ExamField": ''+@ExamField

-- 			  if @PHYSICALEXAM <>''''
--				   select  @REASON_PHYEXAM= REPLACE(
--				   REPLACE(
--				   ISNULL( dbo.ACI_qfn_XmlToJson(	
--				  (select top 1 
--						   ISNULL(Type,'''') AS Type,ISNULL(Code,'''') AS Code,ISNULL(Description,'''') AS Description,ISNULL(CodeSystemName,'''') AS CodeSystemName
--						   ,ISNULL(CodeSystem,'''') AS CodeSystem
--							  from VIEW_ACI_PHYSICALEXAM_REASON (NOLOCK) where  CHARTRECORDKEY =@Examkey  for xml path(''Reason''),type
--					  )  
--				  )
--				   ,'''')
--				   ,''['','''')
--				   ,'']'','''')
--				if @REASON_PHYEXAM <>''''
--				  set @REASON_PHYEXAM='',"Reason": ''+@REASON_PHYEXAM



-- 			  if @PHYSICALEXAM <>''''
--				   select  @ExternalEye= --REPLACE(
--				   --REPLACE(
--				   ISNULL( dbo.ACI_qfn_XmlToJson(	
--				  (select top 1 
--						   ISNULL(Name,'''') AS Name,
--						   ISNULL(Finding,'''') AS Finding,
--						   ISNULL(Code,'''') AS Code
--							  from VIEW_ACI_PHYSICALEXAM_EXTERNALEYE (NOLOCK) where  CHARTRECORDKEY =@Examkey  for xml path(''ExternalEye''),type
--					  )  
--				  )
--				   ,'''')
--				   --,''['','''')
--				   --,'']'','''')
				   
--				if @ExternalEye <>''''
--				  set @ExternalEye='',"ExternalEye": ''+@ExternalEye

--			if @PHYSICALEXAM <>''''
--			begin
--			  set @PHYSICALEXAM =@PHYSICALEXAM + @Eye + @ExamField + @REASON_PHYEXAM +  @ExternalEye +''}''
--			  if @STRINGJSON_PhysicalExam =''''
--			    set @STRINGJSON_PhysicalExam=@PHYSICALEXAM
--              else
--			    set @STRINGJSON_PhysicalExam=@STRINGJSON_PhysicalExam+'',''+@PHYSICALEXAM
--			end
--		FETCH NEXT FROM CURSORA_PHYEXAM INTO @Examkey
--	   END
--	   CLOSE CURSORA_PHYEXAM
--	   DEALLOCATE CURSORA_PHYEXAM 

 	
--   if @Print=1
--   print 14

--	IF @STRINGJSON_PhysicalExam <> '''' 
--	BEGIN
--	    SET @STRINGJSON_PhysicalExam =REPLACE(@STRINGJSON_PhysicalExam,''NULL'',''""'')	 
--		SET @STRINGJSON_PhysicalExam = '',"PhysicalExam":[''+@STRINGJSON_PhysicalExam	+'']''  

--	--update  ACI_JSON_MASTER set PHYSICALEXAM =@STRINGJSON_PhysicalExam where ACIJSONMasterKey = @ACIJSONMasterKey
--	END
--Rev1.1 PHYSICALEXAM - END 
/*PhysicalExam-BEGIN===================================================================================================================================*/
--Latest by Raghu


 
if (select count(*) from VIEW_ACI_PHYSICALEXAM_EXAMFIELD where CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
begin
DECLARE @STRINGJSON_PhysicalExam VARCHAR(8000),@Examkey VARCHAR(30),
@PHYSICALEXAM Varchar(max), 
@Eye Varchar(8000),
@REASON_PHYEXAM varchar(8000),
@ExamField varchar(8000),
@ExternalEye varchar(8000),@Laterality varchar(25),@Type varchar(50),@PHYSICALEXAM_Eye varchar(8000), @Code varchar(50)

    SET @STRINGJSON_PhysicalExam =''''
    set @PHYSICALEXAM =''''
    set @Eye = ''''
    set @REASON_PHYEXAM = ''''
    set @ExamField = ''''
    set @ExternalEye = ''''
    set @Laterality =''''
    set @Type =''''
    set @PHYSICALEXAM_Eye =''''

    select  @PHYSICALEXAM= replace(--''}''
    replace(--'']''
    replace --''[''
	   (ISNULL( dbo.ACI_qfn_XmlToJson(	
			 (select Top 1
				    ISNULL(CreatedDate,'''') as CreatedDate,
				    ISNULL(IsActive,'''') as IsActive
				from VIEW_ACI_PHYSICALEXAM (NOLOCK) where CHARTRECORDKEY =@CHARTRECORDKEY1 for xml path(''PhysicalExam''),type
				) 
			 )
		  ,'''') 
		  ,''['','''')
		  ,'']'','''')
		  ,''}'','''')


    if @PHYSICALEXAM <>''''
    begin

	   DECLARE CURSORA_PHYEXAM SCROLL CURSOR FOR 
	   select DISTINCT CHARTRECORDKEY ,Laterality from VIEW_ACI_PHYSICALEXAM_EXAMFIELD (NOLOCK) where  CHARTRECORDKEY=@CHARTRECORDKEY1

	   OPEN CURSORA_PHYEXAM   
	   FETCH FIRST FROM CURSORA_PHYEXAM INTO @Examkey,@Laterality 
	   WHILE @@FETCH_STATUS=0
	   BEGIN

		select  @Eye= @Eye+REPLACE(
			 REPLACE(
			 ISNULL( dbo.ACI_qfn_XmlToJson(	
			 (select top 1 
					   ISNULL(Laterality,'''') AS Laterality,
					   ISNULL(DilationResult,'''') AS DilationResult,
					   ISNULL(IOP,'''') AS IOP,
					   ISNULL(Refractive,'''') AS Refractive,
					   ISNULL(Keratometry,'''') AS Keratometry,
					   ISNULL(Pupils,'''') AS Pupils,
					   ISNULL(AnteriorSegment,'''') AS AnteriorSegment,
					   ISNULL(PosteriorSegment,'''') AS PosteriorSegment
						  from VIEW_ACI_PHYSICALEXAM_EYE (NOLOCK) where  CHARTRECORDKEY =@Examkey and Laterality=@Laterality  for xml path(''Eye''),type
				)  
			 )
			 ,'''')
			  ,''['','''')
			  ,''}]'','''')

		  if @Eye <>''''
		  begin
		     set @ExamField =''''
			 DECLARE CURSORA_PE_ExamField SCROLL CURSOR FOR 
			  select   Type,Code  from VIEW_ACI_PHYSICALEXAM_EXAMFIELD (NOLOCK) where  CHARTRECORDKEY =@Examkey and Laterality =@Laterality
			 OPEN CURSORA_PE_ExamField   
			 FETCH FIRST FROM CURSORA_PE_ExamField INTO @Type,@Code
			 WHILE @@FETCH_STATUS=0
			 BEGIN  

					  select  @ExamField= @ExamField +REPLACE(
					  REPLACE(
					  ISNULL( dbo.ACI_qfn_XmlToJson(	
					 (select top 1 
							  ISNULL(Type,'''') AS Type,ISNULL(Code,'''') AS Code,ISNULL(Description,'''') AS Description,ISNULL(CodeSystem,'''') AS CodeSystem
							  ,ISNULL(CodeSystemName,'''') AS CodeSystemName
								 from VIEW_ACI_PHYSICALEXAM_EXAMFIELD (NOLOCK) where  CHARTRECORDKEY =@Examkey and Type= @Type and Code = @Code for xml path(''ExamField''),type
						 )  
					 )
					  ,'''')
					  ,''['','''')
					  ,''}]'','''')

                      if @ExamField <> ''''
				  begin
                       select  @REASON_PHYEXAM= REPLACE(
					  REPLACE(
					  ISNULL( dbo.ACI_qfn_XmlToJson(	
					 (select top 1 
							  ISNULL(Type,'''') AS Type,ISNULL(Code,'''') AS Code,ISNULL(Description,'''') AS Description,ISNULL(CodeSystemName,'''') AS CodeSystemName
							  ,ISNULL(CodeSystem,'''') AS CodeSystem
								 from VIEW_ACI_PHYSICALEXAM_REASON (NOLOCK) where  CHARTRECORDKEY =@Examkey  for xml path(''Reason''),type
						 )  
					 )
					  ,'''')
					  ,''['','''')
					  ,'']'','''')
				    if @REASON_PHYEXAM <>''''
					 set @ExamField=@ExamField+'',"Reason": ''+@REASON_PHYEXAM
					 
 					set @ExamField =@ExamField+''},''

				  end
			   FETCH NEXT FROM CURSORA_PE_ExamField INTO  @Type,@Code
			 END
			 CLOSE CURSORA_PE_ExamField
			 DEALLOCATE CURSORA_PE_ExamField
		      set @Eye	 =@Eye+ '',"ExamField":[''+substring(@ExamField,1 ,len(@ExamField)-1)+'']}''
		  end

			    if @Eye <>''''
			    begin
			      if @PHYSICALEXAM_Eye =''''
				    set @PHYSICALEXAM_Eye =@Eye
                     else
				    set @PHYSICALEXAM_Eye =@PHYSICALEXAM_Eye+'',''+@Eye
                     set @Eye ='''' 
				 set @ExamField =''''   
			    end
		  FETCH NEXT FROM CURSORA_PHYEXAM INTO @Examkey,@Laterality 
	   END
	   CLOSE CURSORA_PHYEXAM
	   DEALLOCATE CURSORA_PHYEXAM  	

    end--@PHYSICALEXAM

    IF @PHYSICALEXAM <> '''' 
    BEGIN
        SET @PHYSICALEXAM_Eye =REPLACE(@PHYSICALEXAM_Eye,''NULL'',''""'')	  
	   if @PHYSICALEXAM_Eye<>''''
	     set @PHYSICALEXAM_Eye ='', "Eye":[''+@PHYSICALEXAM_Eye+'']''

	   SET @STRINGJSON_PhysicalExam =REPLACE(@PHYSICALEXAM,''NULL'',''""'')	 
	   SET @STRINGJSON_PhysicalExam = '',"PhysicalExam":''+@PHYSICALEXAM	+@PHYSICALEXAM_Eye+''}''
	   if len(@STRINGJSON_PhysicalExam) >25
           update  ACI_JSON_MASTER set PHYSICALEXAM =@STRINGJSON_PhysicalExam where ACIJSONMasterKey = @ACIJSONMasterKey
    END
--select @STRINGJSON_PhysicalExam
end


/*PhysicalExam-END===================================================================================================================================*/

/*PastHistory-BEGIN===================================================================================================================================*/
/*
//Dont send this section in JSon
DECLARE @STRINGJSON_PastHistory VARCHAR(2000)
SET @STRINGJSON_PastHistory =''''
     IF (SELECT COUNT(*) FROM VIEW_ACI_PASTHISTORY (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
		SELECT @STRINGJSON_PastHistory=@STRINGJSON_PastHistory+'',"PastHistory":''+ REPLACE(REPLACE(DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT                 
				ISNULL(EXTERNALID,'''') AS EXTERNALID,
				ISNULL(HISTORYTYPE,'''') AS HISTORYTYPE,
				ISNULL(LATERALITY,'''') AS LATERALITY,
				ISNULL(ISSURGERY,'''') AS ISSURGERY,
				ISNULL(ISACTIVE,'''') AS ISACTIVE
			FROM VIEW_ACI_PASTHISTORY  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''PastHistory''),TYPE ) 
			 ),''['',''['') ,''}]'','''')

     IF (SELECT COUNT(*) FROM VIEW_ACI_PASTHISTORY_HISTORYCODE (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
		SELECT @STRINGJSON_PastHistory=@STRINGJSON_PastHistory+'',"HistoryCode":''+ REPLACE(REPLACE(DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT 
				ISNULL(HISTORYCODEVALUE,'''') AS HISTORYCODEVALUE,
				ISNULL(HISTORYCODEDESCRIPTION,'''') AS HISTORYCODEDESCRIPTION,
				ISNULL(HISTORYCODESYSTEM,'''') AS HISTORYCODESYSTEM,
				ISNULL(HISTORYCODENAME,'''') AS HISTORYCODENAME
			FROM VIEW_ACI_PASTHISTORY_HISTORYCODE  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''HistoryCode''),TYPE ) 
			 ),''['',''['') ,'']'','']'')+''}]''
    
 
SET @STRINGJSON =@STRINGJSON+@STRINGJSON_PastHistory
*/
/*PastHistory-END=====================================================================================================================================*/


/*RiskAssessment-BEGIN===================================================================================================================================*/
/*
Dont send this section in JSon
*/
DECLARE @STRINGJSON_RiskAssessment VARCHAR(2000)
SET @STRINGJSON_RiskAssessment =''''
     IF (SELECT COUNT(*) FROM VIEW_ACI_RISKASSESSMENT (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
		SELECT @STRINGJSON_RiskAssessment=@STRINGJSON_RiskAssessment+'',"RiskAssessment":''+  DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT 
				ISNULL(EXTERNALID,'''') AS EXTERNALID,
				ISNULL(CODEDESCRIPTION,'''') AS CODEDESCRIPTION,
				ISNULL(CODEVALUE,'''') AS CODEVALUE,
				ISNULL(CODESYSTEM,'''') AS CODESYSTEM,
				ISNULL(CODESYSTEMNAME,'''') AS CODESYSTEMNAME,
				ISNULL(DATE,'''') AS DATE,
				ISNULL(REASON,'''') AS REASON,
				ISNULL(ISACTIVE,'''') AS ISACTIVE
 			FROM VIEW_ACI_RISKASSESSMENT  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''RiskAssessment''),TYPE ) ) 
 
 
 --select ''STRINGJSON: ''+@STRINGJSON
 --select ''STRINGJSON RISKASSESSMENT1: ''+@STRINGJSON_RiskAssessment
    
SET @STRINGJSON =@STRINGJSON+@STRINGJSON_RiskAssessment

	IF (@STRINGJSON_SocialHistory  <> '''' and @STRINGJSON_RiskAssessment<>'''')
	BEGIN 	 
		set @STRINGJSON_SocialHistory  =@STRINGJSON_SocialHistory +@STRINGJSON_RiskAssessment
		update  ACI_JSON_MASTER set SOCIALHISTORY = @STRINGJSON_SocialHistory  where ACIJSONMasterKey = @ACIJSONMasterKey
	END


--select ''STRINGJSON RISKASSESSMENT2: ''+@STRINGJSON_RiskAssessment


/*RiskAssessment-END=====================================================================================================================================*/


/*Intervention-BEGIN===================================================================================================================================*/
/*
--Dont send this section in JSon*/
--DECLARE @STRINGJSON_Intervention VARCHAR(2000)
--SET @STRINGJSON_Intervention =''''
--     IF (SELECT COUNT(*) FROM VIEW_ACI_INTERVENTION (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
--		SELECT @STRINGJSON_Intervention=@STRINGJSON_Intervention+'',"Intervention":''+  DBO.ACI_QFN_XMLTOJSON (
--		(
--	   		SELECT 
--				ISNULL(EXTERNALID,'''') AS EXTERNALID,
--				ISNULL(CODEDESCRIPTION,'''') AS CODEDESCRIPTION,
--				ISNULL(CODEVALUE,'''') AS CODEVALUE,
--				ISNULL(CODESYSTEM,'''') AS CODESYSTEM,
--				ISNULL(CODESYSTEMNAME,'''') AS CODESYSTEMNAME,
--				ISNULL(DATE,'''') AS DATE,
--				ISNULL(REASON,'''') AS REASON,
--				ISNULL(ISACTIVE,'''') AS ISACTIVE
-- 			FROM VIEW_ACI_INTERVENTION  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''Intervention''),TYPE ) ) 
    
 
--SET @STRINGJSON =@STRINGJSON+@STRINGJSON_Intervention

--IF (@STRINGJSON_Intervention  <> '''')
--	BEGIN 	 
--		set @STRINGJSON_SocialHistory  =@STRINGJSON_SocialHistory +@STRINGJSON_RiskAssessment + @STRINGJSON_Intervention
--		update  ACI_JSON_MASTER set SOCIALHISTORY = @STRINGJSON_SocialHistory  where ACIJSONMasterKey = @ACIJSONMasterKey
--	END

/*Intervention-BEGIN=====================================================================================================================================*/

DECLARE @STRINGJSON_Intervention VARCHAR(MAx),@STRINGJSON_IV varchar(MAX),@TablNameIV Varchar(20),@TableKeyIV INT ,@Reason_IV Varchar(2000)
SET @STRINGJSON_Intervention =''''
Set @STRINGJSON_IV  =''''
     IF (SELECT COUNT(*) FROM VIEW_ACI_INTERVENTION (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
	Begin
		  DECLARE CURSORA_IV SCROLL CURSOR FOR 
			SELECT TableName FROM VIEW_ACI_INTERVENTION (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1
		  OPEN CURSORA_IV
		  FETCH FIRST FROM CURSORA_IV INTO @TablNameIV
		  WHILE @@FETCH_STATUS=0
		  BEGIN
		           set @STRINGJSON_IV =''''
				  SELECT @STRINGJSON_IV=
				  Replace(
				  Replace 
				  ( DBO.ACI_QFN_XMLTOJSON (
					  (
	   				  SELECT Top 1 
						  ISNULL(EXTERNALID,'''') AS ExternalId,
						  ISNULL(CODEDESCRIPTION,'''') AS CodeDescription,
						  ISNULL(CODEVALUE,'''') AS CodeValue,
						  ISNULL(CODESYSTEM,'''') AS CodeSystem,
						  ISNULL(CODESYSTEMNAME,'''') AS CodeSystemName,
						  ISNULL(DATE,'''') AS Date,
						  ISNULL(ISACTIVE,'''') AS IsActive
 					  FROM VIEW_ACI_INTERVENTION  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 and Tablename=@TablNameIV  FOR XML PATH(''TableKeyIV''),TYPE ) ) 
				   ,''['','''') 
				   ,''}]'','''')

				   if @STRINGJSON_IV <> ''''
				   Begin
				      set @Reason_IV =''''
						 select  @Reason_IV= REPLACE(
						 REPLACE(
						 ISNULL( dbo.ACI_qfn_XmlToJson(	
						(select top 1 
								 ISNULL(Type,'''') AS Type,ISNULL(Code,'''') AS Code,ISNULL(Description,'''') AS Description,ISNULL(CodeSystemName,'''') AS CodeSystemName
								 ,ISNULL(CodeSystem,'''') AS CodeSystem
									from VIEW_ACI_INTERVENTION_REASON (NOLOCK) where  CHARTRECORDKEY=@CHARTRECORDKEY1 and Tablename=@TablNameIV 
									  for xml path(''Reason_IV''),type
							) 
						)
						 ,'''')
						 ,''['','''')
						 ,'']'','''')

					   if @Reason_IV <>''''
						set @Reason_IV='',"Reason": ''+@Reason_IV
					   if @STRINGJSON_Intervention =''''
					   Begin
						  set  @STRINGJSON_Intervention =@STRINGJSON_IV+@Reason_IV +''}''
					   end
					   else
						  set  @STRINGJSON_Intervention =@STRINGJSON_Intervention+'',''+@STRINGJSON_IV+@Reason_IV +''}''
				   end
		    FETCH NEXT FROM CURSORA_IV INTO @TablNameIV,@TableKeyIV
		  END
		  CLOSE CURSORA_IV
		  DEALLOCATE CURSORA_IV
	end
	IF @STRINGJSON_Intervention  <> '''' 
	BEGIN
	    SET @STRINGJSON_Intervention  =REPLACE(@STRINGJSON_Intervention ,''NULL'',''""'')	 	 
		set @STRINGJSON_Intervention  ='',"Intervention":[''+@STRINGJSON_Intervention+'']''
		 		SELECT @ENCOUNTER=@ENCOUNTER+@STRINGJSON_Intervention
				update  ACI_JSON_MASTER set ENCOUNTER =@ENCOUNTER where ACIJSONMasterKey = @ACIJSONMasterKey
	--	update  ACI_JSON_MASTER set Intervention = @STRINGJSON_Intervention  where ACIJSONMasterKey = @ACIJSONMasterKey
	END


/*Intervention-END=====================================================================================================================================*/


/*Attribute-BEGIN===================================================================================================================================*/
/*
--Dont send this section in JSon
DECLARE @STRINGJSON_Attribute VARCHAR(2000)
SET @STRINGJSON_Attribute =''''
     IF (SELECT COUNT(*) FROM VIEW_ACI_ATTRIBUTE (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
		SELECT @STRINGJSON_Attribute=@STRINGJSON_Attribute+'',"Attribute":''+  DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT 
				 ISNULL(EXTERNALID,'''') AS EXTERNALID,
				 ISNULL(ATTRIBUTETYPE,'''') AS ATTRIBUTETYPE,
				 ISNULL(ATTRIBUTESUBTYPE,'''') AS ATTRIBUTESUBTYPE,
				 ISNULL(DATE,'''') AS DATE,
				 ISNULL(ATTRIBUTENAME,'''') AS ATTRIBUTENAME,
				 ISNULL(ISACTIVE,'''') AS ISACTIVE
 			FROM VIEW_ACI_ATTRIBUTE  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''Attribute''),TYPE ) ) 
 
 
SET @STRINGJSON =@STRINGJSON+@STRINGJSON_Attribute
*/
/*Attribute-END=====================================================================================================================================*/

/*careteam-BEGIN===================================================================================================================================*/
 
--Dont send this section in JSon
DECLARE @STRINGJSON_CareTeam VARCHAR(2000)
SET @STRINGJSON_CareTeam =''''
     IF (SELECT COUNT(*) FROM VIEW_ACI_CARETEAM (NOLOCK) WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 )>0
		SELECT @STRINGJSON_CareTeam=@STRINGJSON_CareTeam+'',"CareTeam":''+  DBO.ACI_QFN_XMLTOJSON (
		(
	   		SELECT 
				ISNULL(UserExternalId,'''') AS UserExternalId,
				ISNULL(UserName,'''') AS UserName,
				ISNULL(IsActive,'''') AS IsActive
 			FROM VIEW_ACI_CARETEAM  (NOLOCK)  WHERE CHARTRECORDKEY=@CHARTRECORDKEY1 FOR XML PATH(''CareTeam''),TYPE ) ) 
  
	IF @STRINGJSON_CareTeam  <> '''' 
	BEGIN
	    SET @STRINGJSON_CareTeam  =REPLACE(@STRINGJSON_CareTeam ,''NULL'',''""'')	 	 
		--set @STRINGJSON_Instructions  ='',"Instructions":''+@STRINGJSON_Instructions 
		update  ACI_JSON_MASTER set CARETEAM = @STRINGJSON_CareTeam  where ACIJSONMasterKey = @ACIJSONMasterKey
	END

if @Print=1
   print 23
 
/*Attribute-END=====================================================================================================================================*/
	SET @STRINGJSON =@STRINGJSON+ISNULL(@ENCOUNTER,'''')+ISNULL(@ALLERGIES,'''')+ISNULL(@FAMILYHISTORY,'''')+ISNULL(@FUNCTIONALSTATUS,'''')
								+ISNULL(@STRINGJSON_MEDEQ,'''')+ISNULL(@STRINGJSON_MEDICATION,'''')
								+ISNULL(@STRINGJSON_PAYER,'''')+ISNULL(@STRINGJSON_PLANOFTREATMENT,'''')+ISNULL(@STRINGJSON_ProblemsList,'''')
								+ISNULL(@STRINGJSON_Procedures,'''')+ISNULL(@STRINGJSON_Result,'''')+ISNULL(@STRINGJSON_SocialHistory,'''')
								+ISNULL(@STRINGJSON_VitalSign,'''')+ISNULL(@STRINGJSON_Instructions,'''')
SET @STRINGJSON =REPLACE(@STRINGJSON,''NULL'',''""'')+''}''
--select LEN(@STRINGJSON)
--SELECT @STRINGJSON
      --if ltrim(rTrim(@StringJson)) <> ''''
	 if (ltrim(rTrim(@StringJson)) <> '''' and len(@StringJson) >10)
	  begin
       --  insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json) 
         --        values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))))  

		--set @ACIJSONMasterKey=IDENT_CURRENT(''ACI_JSON_Master'')
		update ACI_InPutLog set status =''C'',statuschangedt=GETDATE() 
		where status =''N'' and TableName =''PATIENTEXAM'' 
          and aciinputlogkey=@ACIInPutLogKey 
		and aciinputlogkey in (select aciinputlogkey from ACI_JSON_Master (nolock) where isnull(JSON,'''') <>'''' 
		                      and status=''N'' and ACIJSONMasterKey=@ACIJSONMasterKey)
			if @Print=1
                print 24

      end
end
if @Print=1
   print ''END''

end
' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION')) 
Drop Procedure ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 
EXEC dbo.sp_executesql @statement = N'--EXEC [dbo].[ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION] 42571, 21, ''''
CREATE PROC [dbo].[ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION]
(@EXTMEDRECSOURCEKEY VARCHAR(100),
 @aciinputlogkey     INT,
 @Type               VARCHAR(20)
)
AS
     BEGIN
         DECLARE @StringJson VARCHAR(8000);
         DECLARE @aciinputlogkey1 INT, @tableKey VARCHAR(100), @ACIJSONMasterKey INT;
         SET @aciinputlogkey1 = 0;
         SET @tableKey = '''';
         SET @ACIJSONMasterKey = 0;
         SET @tableKey = @EXTMEDRECSOURCEKEY;
         SET @StringJson = '''';
         SELECT TOP 1 @aciinputlogkey1 = m.aciinputlogkey
         FROM ACI_JSON_Master m(nolock)
              JOIN ACI_InPutLog c(nolock) ON c.aciinputlogkey = m.aciinputlogkey
                                             AND c.status = ''N''
                                             AND c.TableName = ''JSON12_CLINICALDATARECONCILIATION''
         WHERE m.status = ''N''
               AND c.aciinputlogkey = @ACIInPutLogKey
         ORDER BY m.ACIJSONMasterKey DESC;
         IF @aciinputlogkey1 = 0
             BEGIN
                 DECLARE @ReferenceNumber VARCHAR(50), @PhyId INT, @PatId INT, @FacId INT;
                     
                         SET @PhyId = 0;
                         SET @PatId = 0;
                         SET @FacId = 0;
                         
				SELECT TOP 1 @PhyId = A.DefaultUserId, @PatId = PRPI.PatientId, @FacId = R.PracticeId  
				FROM Model.PatientReconcilePracticeInformation PRPI WITH (NOLOCK)
				LEFT JOIN Model.Patients A on A.Id = PRPI.PatientId
				LEFT JOIN dbo.Resources R on R.ResourceId = A.DefaultUserId
                WHERE PRPI.PatientId = @EXTMEDRECSOURCEKEY
				order by PRPI.CreatedDateTime desc;
         
                       
				SET @ReferenceNumber = '''';
                SELECT @ReferenceNumber = aci_vendorID+''.''+MDO_PracticeId+''.''+CAST(@FacId AS VARCHAR(10))+''.''+CAST(@PhyId AS VARCHAR(10))+''.''+CAST(@PatId AS VARCHAR(10))+''.12.''
                FROM aci_json_Defaults(NOLOCK); 
						                      
					     DECLARE @ReferenceNumberT VARCHAR(50);
                         SET @ReferenceNumberT = '''';
                         SET @ReferenceNumberT = @ReferenceNumber + CAST(@EXTMEDRECSOURCEKEY AS VARCHAR(10))+''_REFF'';
                         IF
                         (
                             SELECT COUNT(*)
                             FROM VIEW_ACI_JSON12_CLINICALDATARECONCILIATION
                         ) > 0
                             SELECT @StringJson = @StringJson+REPLACE(REPLACE(dbo.aci_qfn_XmlToJson
                                                                             (
                                                                             (
                                                                                 SELECT ''ReconcilationOrIncorporation'' AS ResourceType,
                                                                                        ISNULL(ExternalId, '''') AS ExternalId,
                                                                                        ISNULL(IsSource, '''') AS IsSource,
                                                                                        (@ReferenceNumberT +CAST(ReferenceNumber AS VARCHAR(10))) AS ReferenceNumber,
																					   -- '''' AS ReferenceNumber,
                                                                                        ISNULL(PatientAccountNumber, '''') AS PatientAccountNumber,
                                                                                        ISNULL(FileReceivedDate, '''') AS FileReceivedDate,
                                                                                        ISNULL(ReconcileFiletype, '''') AS ReconcileFiletype,
                                                                                        ISNULL(ReconcileDate, '''') AS ReconcileDate,
                                                                                        ISNULL(MedicationReconciled, '''') AS MedicationReconciled,
                                                                                        ISNULL(AllergiesReconciled, '''') AS AllergiesReconciled,
                                                                                        ISNULL(ProblemsReconciled, '''') AS ProblemsReconciled,
                                                                                        ISNULL(IsSOCIncorporated, '''') AS IsSOCIncorporated,
                                                                                        ISNULL(ECNPI, '''') AS ECNPI,
																						--ISNULL(ECTIN,'''') AS ECTIN
                                                                                        (SUBSTRING(TIN, 1, 3)+''-''+SUBSTRING(TIN, 4, 2)+''-''+SUBSTRING(TIN, 6, 4)) AS TIN
                                                                                 FROM VIEW_ACI_JSON12_CLINICALDATARECONCILIATION L
                                                                                 WHERE PATIENTACCOUNTNUMBER = @EXTMEDRECSOURCEKEY FOR XML PATH(''RECONCILATIONORINCORPORATION''), TYPE
                                                                             )
                                                                             ), ''['', ''''), ''}]'', ''}'');
                         IF LTRIM(RTRIM(@StringJson)) <> ''''
                             BEGIN
                                 INSERT INTO ACI_JSON_Master
                                 (ACIInPutLogKey,
                                  tablekey,
                                  json,
                                  ReferenceNumber,
                                  Sort
                                 )
                                 VALUES
                                 (@aciinputlogkey,
                                  @tableKey,
                                  (LTRIM(RTRIM(@StringJson))),
                                  @ReferenceNumberT,
                                  12
                                 );
                                 SET @ACIJSONMasterKey = IDENT_CURRENT(''ACI_JSON_Master'');
								 print ''Ayush''
                                 UPDATE ACI_InPutLog
                                   SET
                                       status = ''C'',
                                       statuschangedt = GETDATE()
                                 WHERE status = ''N''
                                       AND TableName = ''JSON12_CLINICALDATARECONCILIATION''
                                       AND (tableKey = @tableKey
                                            AND tablekey IN
                                           (
                                               SELECT tablekey
                                               FROM ACI_JSON_Master(nolock)
                                               WHERE status = ''N''
                                                     AND ACIJSONMasterKey = @ACIJSONMasterKey
                                           ));
                         END;
                 
	 
         END;
     END;
' 
Go


IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_ConvertToJson_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE')) 
Drop Procedure ACI_ConvertToJson_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[ACI_ConvertToJson_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE] (@MUMETRICS_PORTALKEY varchar(100),@aciinputlogkey int)
as 
begin

  declare @StringJson varchar(8000)
  declare @aciinputlogkey1 int ,@tableKey varchar(100), @ACIJSONMasterKey int,@patientkey int 
  set @aciinputlogkey1 =0
  set @tableKey=''''
  set @ACIJSONMasterKey =0
  set @tableKey=@MUMETRICS_PORTALKEY
  set @StringJson=''''

	select top 1 @aciinputlogkey1=m.aciinputlogkey   from ACI_JSON_Master m (nolock)
	join ACI_InPutLog c (nolock) on c.aciinputlogkey=m.aciinputlogkey and c.status=''N'' and  c.TableName = ''JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE'' 
	where m.status =''N'' and c.aciinputlogkey=@ACIInPutLogKey order by m.ACIJSONMasterKey desc

	if @aciinputlogkey1 =0
	begin

	  --  select top 1 @patientkey= patientkey  from CCDAXMLFILES (nolock) where CCDAXMLFileKey=@CCDAXMLFileKey

		declare @ReferenceNumber varchar(200),@PhyId varchar(20) , @FacId varchar(20), @PatId INT
		set @PhyId =''''
		set @FacId =0
		set @PatId =0

		select top 1 @PhyId=PP.PatientNo,@FacId=R.PracticeId  ,@PatId=SM.PatientID
		FROM  [MVE].[PP_SECUREMESSAGESLOGS]  SM (nolock)
		Left Join MVE.PP_PortalQueueResponse PP on (Cast(PP.PatientNo as nvarchar(50)) =  Cast(SM.RecipientId as nvarchar(50)) OR Cast(PP.ExternalId as nvarchar(50)) =  Cast(SM.RecipientId as nvarchar(50))) Left join dbo.Resources R on Cast(R.ResourceId as nvarchar(50)) = Cast(PP.PatientNo as nvarchar(50))	
		where SM.LogId=@MUMETRICS_PORTALKEY



		set @ReferenceNumber =''''
		select @ReferenceNumber = CAST(aci_vendorID AS VARCHAR(10))+''.''+CAST(MDO_PracticeId AS varchar(10))+''.''+CAST(@FacId as varchar(10))+''.''+CAST(@PhyId as varchar(50))+''.''+CAST(@PatId as varchar(10))+''.13.'' from aci_json_Defaults (NOLOCK)



		Declare @ReferenceNumberT varchar(200)
		set @ReferenceNumberT =''''
		set @ReferenceNumberT =@ReferenceNumber+cast(@MUMETRICS_PORTALKEY as varchar(100))



	   if @PatId >0
	   begin
			select @StringJson=@StringJson+isnull( 
				REPLACE(REPLACE(
						dbo.aci_qfn_XmlToJson (
						(     
						select 
							 ''SecureMessage'' AS resourceType,
							ISNULL(isSource,'''') AS isSource,
							(isnull(@ReferenceNumber,'''')+cast(ReferenceNumber as varchar(100))) AS ReferenceNumber,
							ISNULL(CAST(PatientAccountNumber AS VARCHAR(10)),'''') AS PatientAccountNumber
						from VIEW_ACI_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE L where cast(l.logid AS varchar (100)) = @MUMETRICS_PORTALKEY 
						for xml path(''SECUREMESSAGE''),type) 
				),''['','''') ,''}]'','''')  ,'''')     

			select @StringJson=@StringJson+ '',"MessageSentBy":''+
			 isnull(	REPLACE(REPLACE(
						dbo.aci_qfn_XmlToJson (
						(     
						select top 1
							 ISNULL(ECNPI,'''') AS ECNPI,
							 ISNULL(ECAccount,'''') AS ECAccount,
							-- ISNULL(ECTIN,'''') AS ECTIN,
							 (SUBSTRING(ECTIN,1,3)+''-''+SUBSTRING(ECTIN,4,2)+''-''+SUBSTRING(ECTIN,6,4)) as ECTIN ,
							 ISNULL(SentDate,'''') AS SentDate,
							 ISNULL(MessageType,'''') AS MessageType,
							 ISNULL(SentTo,'''') AS SentTo,
							 ISNULL(RepresentativeExternalId,'''') AS RepresentativeExternalId
						from VIEW_ACI_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE_MESSAGESENTBY L  where cast(l.LogID AS VARCHAR(100))=@MUMETRICS_PORTALKEY	 
						for xml path(''MessageSentBy''),type) 
				),''['','''') ,''}]'',''}}'')     
				,'''')  

		  --if ltrim(rTrim(@StringJson)) <> ''''
		  if (ltrim(rTrim(@StringJson)) <> '''' and len(@StringJson) >10)
		  begin
			 insert into ACI_JSON_Master (ACIInPutLogKey,tablekey,json,ReferenceNumber,Sort) 
					 values(@aciinputlogkey,@tableKey,(ltrim(rTrim(@StringJson))),@ReferenceNumberT,13)  

			set @ACIJSONMasterKey=IDENT_CURRENT(''ACI_JSON_Master'')
			update ACI_InPutLog set status =''C'',statuschangedt=GETDATE() where status =''N'' and TableName =''JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE''
			and  ( tableKey=@tableKey and
				 tablekey in(select tablekey from ACI_JSON_Master (nolock) where isnull(JSON,'''') <>'''' and status=''N'' and ACIJSONMasterKey=@ACIJSONMasterKey))
		  end
	  end
  end
end

' 
Go

 
IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_INSERT_ACI_JSON_MASTER')) 
Drop Procedure ACI_INSERT_ACI_JSON_MASTER 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[ACI_INSERT_ACI_JSON_MASTER] @SortIns INT AS
BEGIN

SET ARITHABORT ON
	DECLARE @TABLENAME VARCHAR(100) ,@FIELDNAME VARCHAR(50),@TABLEKEY VARCHAR(100),@FIELDKEY VARCHAR(100),@Sort INT
	 ,@CHARTRECORDKEY INT,@PROCEDUREKEY INT,@ACIINPUTLOGKEY INT ,@ACIINPUTLOGKEY1 INT , @TABLEKEY1 VARCHAR(100)


  DECLARE    @mm Varchar(2),
  @dd Varchar(2),  @yy Varchar(4),
  @hh Varchar(2),  @mi Varchar(2),
  @ss Varchar(2),  @ms Varchar(4),
  @date datetime,  @Newdt Varchar(50)
 
  SELECT @date = GETDATE()
  SELECT @mm = convert(Varchar (2),datePART(mm, @date))
  SELECT @dd=      convert(Varchar (2),datePART(dd,@date))
  SELECT @yy =  convert(Varchar (4),datePART(yyyy,@date))
  SELECT @hh =  convert(Varchar (2),datePART(hh,@date))
  SELECT @mi =  convert(Varchar (2),datePART(mi, @date))
  SELECT @ss =   convert(Varchar (2),datePART(ss,@date))
  SELECT @ms =   convert(Varchar (4),datePART(ms,@date))

  Set  @newdt = rtrim(@mm)+rtrim(@dd)+@yy+@hh+@mi+@ss+@ms
  If @SortIns > 0
   Begin
	DECLARE CURSORA SCROLL CURSOR FOR 
      SELECT top (select isnull(records,100) from aci_JSON_defaults) SS.TABLENAME,SS.TABLEKEY,sort,ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) WHERE STATUS=''N'' and Sort=@SortIns ORDER BY SS.Sort
   End  
  Else
   Begin
     DECLARE CURSORA SCROLL CURSOR FOR 
      SELECT top (select isnull(records,100) from aci_JSON_defaults) SS.TABLENAME,SS.TABLEKEY,sort,ACIINPUTLOGKEY
        FROM ACI_INPUTLOG SS (NOLOCK) WHERE STATUS=''N'' ORDER BY SS.Sort
   End

	OPEN CURSORA   
	FETCH FIRST FROM CURSORA INTO @TABLENAME,@TABLEKEY,@Sort,@ACIINPUTLOGKEY1
	WHILE @@FETCH_STATUS = 0
	BEGIN
	  If @ACIINPUTLOGKEY1 > 0 
	       Insert into ACI_JSONConversionLog_Start (ACIINPUTLOGKEY,TableKey,Sort,TempID) values(@ACIINPUTLOGKEY1,@TABLEKEY,@Sort,@newdt)
	  
	  IF (UPPER(@TABLENAME) =''PATIENTEXAM'') AND @TABLEKEY > 0 
	  BEGIN
	    Set @CHARTRECORDKEY=0
		Set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @CHARTRECORDKEY=SS.TABLEKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY       =@ACIINPUTLOGKEY1 and TABLEKEY IN (SELECT top 1 EncounterID from model.invoices inv WHERE EncounterID=@TABLEKEY)
	    If @TABLEKEY >0 and @ACIINPUTLOGKEY >0
	     Begin 
		  EXEC ACI_ConvertToJson_JSON07_PatientExam  0 ,@CHARTRECORDKEY ,0,@ACIINPUTLOGKEY
	     END
      END  
	  IF (UPPER(@TABLENAME) =''JSON01_FACILITYINFORMATION'') AND len(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0

		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''PRACTICEID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 

		If @FIELDKEY >0 and @ACIINPUTLOGKEY >0	
		BEGIN   
		   EXEC ACI_CONVERTTOJSON_JSON01_FACILITYINFORMATION @FIELDKEY,@ACIINPUTLOGKEY
        END
      END

	  IF (UPPER(@TABLENAME) =''JSON02_ELIGIBLECLINICIANINFORMATION'') AND len(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    Set @ACIINPUTLOGKEY =0

		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY FROM ACI_INPUTLOG SS (NOLOCK) WHERE STATUS =''N'' AND   [FIELDNAME] =''RESOURCEID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		
		If @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		BEGIN 
		  EXEC ACI_CONVERTTOJSON_JSON02_ELIGIBLECLINICIANINFORMATION @FIELDKEY,@ACIINPUTLOGKEY
        END
	  END

	  IF (UPPER(@TABLENAME) =''JSON03_ACIMODULEUSERACCOUNTADMINISTRATION'') AND LEN(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    Set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY FROM ACI_INPUTLOG SS (NOLOCK) WHERE STATUS =''N'' AND [FIELDNAME] =''RESOURCEID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		
		If @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		BEGIN 
		   EXEC ACI_CONVERTTOJSON_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION @FIELDKEY,@ACIINPUTLOGKEY
        END
	  END
	  IF (UPPER(@TABLENAME) =''JSON04_PATIENTDEMOGRAPHICSINFORMATION'') AND LEN(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    Set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ID'' AND TABLENAME  =''JSON04_PATIENTDEMOGRAPHICSINFORMATION''  AND TABLEKEY=@TABLEKEY and   ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 ORDER BY ACIINPUTLOGKEY DESC
     	if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON04_PATIENTDEMOGRAPHICSINFORMATION @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON05_REPRESENTATIVE'') AND len(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''PATIENTREPRESENTATIVEID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON05_REPRESENTATIVE @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON9_PORTALACCOUNTADMINISTRATION'') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0

		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''RESPONSEID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_ConvertToJson_JSON9_PORTALACCOUNTADMINISTRATION @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON10_CPOELABORATORYORIMAGING'') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=''''
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''OrderId'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if len(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_ConvertToJson_JSON10_CPOELABORATORYORIMAGING @FIELDKEY,@ACIINPUTLOGKEY
	  END
	  
	  IF (UPPER(@TABLENAME) =''JSON12_CLINICALDATARECONCILIATION'') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=''''
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ID'' AND TABLENAME = @TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY       =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		DECLARE @PI int
		SET @PI = 0
		SELECT @PI = defaultuserid from model.patients where id = @FIELDKEY
		
		if len(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0 AND @PI is not null and LTRIM(RTRIM(@PI)) <>''''
		 EXEC ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION @FIELDKEY,@ACIINPUTLOGKEY,''Ext''
	  END
	  

	  IF (UPPER(@TABLENAME) =''JSON12_CLINICALDATARECONCILIATION_PGHD'') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=''''
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ID'' AND TABLENAME = @TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		If len(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0
		   EXEC ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION_PGHD @FIELDKEY,@ACIINPUTLOGKEY,''Ext''
	   END

	  IF (UPPER(@TABLENAME) =''JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE'') AND LEn(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK)   
		WHERE STATUS =''N'' AND [FIELDNAME] =''LogID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1  
		ORDER BY ACIINPUTLOGKEY DESC
		
		if LEN(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_ConvertToJson_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON15_CLAIMSUBMISSIONEVENT'') AND len(@TABLEKEY) >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		 EXEC ACI_ConvertToJson_JSON15_CLAIMSUBMISSIONEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON16_MEDICATIONSORDER'') AND len(@TABLEKEY) >0
	  BEGIN--ACI_TR_JSON16_MEDICATIONSORDER_MEDICATION
	    SET @FIELDKEY=''''
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''CLINICALID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		
		if len(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON16_MEDICATIONSORDER @FIELDKEY,@ACIINPUTLOGKEY 
	  END

	  IF (UPPER(@TABLENAME) =''JSON17_SENTSUMMARYOFCAREEVENT'') AND @TABLEKEY >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ACIDEPKEY'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON17_SENTSUMMARYOFCAREEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON18_PATIENTSPECIFICEDUCATIONEVENT'') AND @TABLEKEY >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		 EXEC ACI_ConvertToJson_JSON18_PATIENTSPECIFICEDUCATIONEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT'') AND @TABLEKEY >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''Id'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		 EXEC ACI_CONVERTTOJSON_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT'') AND len(@TABLEKEY) >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''RESPONSEID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		 EXEC ACI_ConvertToJson_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END


	 -- IF (UPPER(@TABLENAME) =''JSON21_CARECOORDINATIONORDER'') AND @TABLEKEY >0
	 -- BEGIN
	 --   SET @FIELDKEY=0
  --       set @ACIINPUTLOGKEY =0
		--SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		--FROM ACI_INPUTLOG SS (NOLOCK) 
		--WHERE STATUS =''N'' AND [FIELDNAME] =''ACIDEPKEY'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		--ORDER BY ACIINPUTLOGKEY DESC
		
		--if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		-- EXEC ACI_CONVERTTOJSON_JSON21_CARECOORDINATIONORDER @FIELDKEY,@ACIINPUTLOGKEY
	 -- END

--REV1.1
 
	  IF (UPPER(@TABLENAME) =''JSON22_SPECIALISTREPORTRECEIVEDEVENT'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''CLINICALID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_CONVERTTOJSON_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END
	  
	  IF (UPPER(@TABLENAME) =''JSON23_DRCOMMUNICATIONEVENT'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ACIDEPKEY'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_CONVERTTOJSON_JSON23_DR_COMMUNICATION_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END



	  IF (UPPER(@TABLENAME) =''JSON24_SURGICALPREOPERATIVESTATUSEVENT'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''APPOINTMENTID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON25_SURGERYEVENT'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''APPOINTMENTID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON25_SurgeryEvent @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON26_SURGICALPOSTOPERATIVERESULTEVENT'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''APPOINTMENTID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON27_POSTSURGICALCOMPLICATIONEVENT'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''APPOINTMENTID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON27_POST_SURGICAL_COMPLICATION_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON28_RESULTS'') AND Len(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''Id'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON28_Results @FIELDKEY,@ACIINPUTLOGKEY

	  END

--REV1.1


	  if @ACIINPUTLOGKEY1 >0 	     
		insert into ACI_JSONConversionLog_END (ACIINPUTLOGKEY,TableKey,Sort,TempID) values(@ACIINPUTLOGKEY1,@TABLEKEY,@Sort,@newdt)


	  FETCH NEXT FROM CURSORA INTO @TABLENAME,@TABLEKEY,@Sort,@ACIINPUTLOGKEY1
	END
	CLOSE CURSORA
	DEALLOCATE CURSORA

 SET ARITHABORT OFF
END'

Go

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_INSERT_ACI_JSON_MASTER_WithParams')) 
Drop Procedure ACI_INSERT_ACI_JSON_MASTER_WithParams 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ACI_INSERT_ACI_JSON_MASTER_WithParams] @_jsonNumber as nvarchar(100), @_externalId INT AS
BEGIN

SET ARITHABORT ON
	DECLARE @TABLENAME VARCHAR(100) ,@FIELDNAME VARCHAR(50),@TABLEKEY VARCHAR(100),@FIELDKEY VARCHAR(100),@Sort INT
	 ,@CHARTRECORDKEY INT,@PROCEDUREKEY INT,@ACIINPUTLOGKEY INT ,@ACIINPUTLOGKEY1 INT


  DECLARE    @mm Varchar(2),
  @dd Varchar(2),  @yy Varchar(4),
  @hh Varchar(2),  @mi Varchar(2),
  @ss Varchar(2),  @ms Varchar(4),
  @date datetime,  @Newdt Varchar(50)
 
  SELECT @date = GETDATE()
  SELECT @mm = convert(Varchar (2),datePART(mm, @date))
  SELECT @dd=      convert(Varchar (2),datePART(dd,@date))
  SELECT @yy =  convert(Varchar (4),datePART(yyyy,@date))
  SELECT @hh =  convert(Varchar (2),datePART(hh,@date))
  SELECT @mi =  convert(Varchar (2),datePART(mi, @date))
  SELECT @ss =   convert(Varchar (2),datePART(ss,@date))
  SELECT @ms =   convert(Varchar (4),datePART(ms,@date))

  Set  @newdt = rtrim(@mm)+rtrim(@dd)+@yy+@hh+@mi+@ss+@ms
  If @_jsonNumber <> '''' and @_externalId <> ''''
   Begin
	DECLARE CURSORA SCROLL CURSOR FOR SELECT Top (100) SS.TABLENAME, SS.TABLEKEY, Sort, ACIINPUTLOGKEY FROM ACI_INPUTLOG SS (NOLOCK) WHERE STATUS=''N'' 
	 and TABLENAME = @_jsonNumber and TABLEKEY = @_externalId ORDER BY SS.Sort
   End  

   OPEN CURSORA FETCH FIRST FROM CURSORA INTO @TABLENAME,@TABLEKEY,@Sort,@ACIINPUTLOGKEY1
   WHILE @@FETCH_STATUS = 0
	BEGIN
	  If @ACIINPUTLOGKEY1 > 0 
	       Insert into ACI_JSONConversionLog_Start (ACIINPUTLOGKEY,TableKey,Sort,TempID) values(@ACIINPUTLOGKEY1,@TABLEKEY,@Sort,@newdt)
	  
	  IF (UPPER(@TABLENAME) =''PATIENTEXAM'') AND @TABLEKEY > 0 
	  BEGIN
	    Set @CHARTRECORDKEY=0
		Set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @CHARTRECORDKEY=SS.TABLEKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 		 and TABLEKEY IN (SELECT top 1 EncounterID from model.invoices inv WHERE EncounterID=@TABLEKEY)
	    If @TABLEKEY >0 and @ACIINPUTLOGKEY >0
	     Begin 
		  EXEC ACI_ConvertToJson_JSON07_PatientExam  0 ,@CHARTRECORDKEY ,0,@ACIINPUTLOGKEY
	     END
      END  
	  IF (UPPER(@TABLENAME) =''JSON01_FACILITYINFORMATION'') AND len(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0

		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''PRACTICEID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 

		If @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		BEGIN   
		   EXEC ACI_CONVERTTOJSON_JSON01_FACILITYINFORMATION @FIELDKEY,@ACIINPUTLOGKEY
        END
      END

	  IF (UPPER(@TABLENAME) =''JSON02_ELIGIBLECLINICIANINFORMATION'') AND len(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    Set @ACIINPUTLOGKEY =0

		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY FROM ACI_INPUTLOG SS (NOLOCK) WHERE STATUS =''N'' AND   [FIELDNAME] =''RESOURCEID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		
		If @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		BEGIN 
		  EXEC ACI_CONVERTTOJSON_JSON02_ELIGIBLECLINICIANINFORMATION @FIELDKEY,@ACIINPUTLOGKEY
        END
	  END

	  IF (UPPER(@TABLENAME) =''JSON03_ACIMODULEUSERACCOUNTADMINISTRATION'') AND LEN(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    Set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY FROM ACI_INPUTLOG SS (NOLOCK) WHERE STATUS =''N'' AND [FIELDNAME] =''RESOURCEID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		
		If @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		BEGIN 
		   EXEC ACI_CONVERTTOJSON_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION @FIELDKEY,@ACIINPUTLOGKEY
        END
	  END
	  IF (UPPER(@TABLENAME) =''JSON04_PATIENTDEMOGRAPHICSINFORMATION'') AND LEN(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    Set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ID'' AND TABLENAME  =''JSON04_PATIENTDEMOGRAPHICSINFORMATION''  AND TABLEKEY=@TABLEKEY and   ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 ORDER BY ACIINPUTLOGKEY DESC
     	if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON04_PATIENTDEMOGRAPHICSINFORMATION @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON05_REPRESENTATIVE'') AND len(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''PATIENTREPRESENTATIVEID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON05_REPRESENTATIVE @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON9_PORTALACCOUNTADMINISTRATION'') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0

		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''RESPONSEID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_ConvertToJson_JSON9_PORTALACCOUNTADMINISTRATION @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON10_CPOELABORATORYORIMAGING'') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=''''
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''OrderId'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if len(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_ConvertToJson_JSON10_CPOELABORATORYORIMAGING @FIELDKEY,@ACIINPUTLOGKEY
	  END
	  
	  IF (UPPER(@TABLENAME) =''JSON12_CLINICALDATARECONCILIATION'') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=''''
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ID'' AND TABLENAME = @TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if len(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION @FIELDKEY,@ACIINPUTLOGKEY,''Ext''
	  END
	  

	  IF (UPPER(@TABLENAME) =''JSON12_CLINICALDATARECONCILIATION_PGHD'') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=''''
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ID'' AND TABLENAME = @TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		If len(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0
		   EXEC ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION_PGHD @FIELDKEY,@ACIINPUTLOGKEY,''Ext''
	   END

	  IF (UPPER(@TABLENAME) =''JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE'') AND LEn(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK)   
		WHERE STATUS =''N'' AND [FIELDNAME] =''LogID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1  
		ORDER BY ACIINPUTLOGKEY DESC
		
		if LEN(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_ConvertToJson_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON15_CLAIMSUBMISSIONEVENT'') AND len(@TABLEKEY) >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		 EXEC ACI_ConvertToJson_JSON15_CLAIMSUBMISSIONEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON16_MEDICATIONSORDER'') AND @TABLEKEY >0
	  BEGIN--ACI_TR_JSON16_MEDICATIONSORDER_MEDICATION
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''CLINICALID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON16_MEDICATIONSORDER @FIELDKEY,@ACIINPUTLOGKEY 
	  END

	  IF (UPPER(@TABLENAME) =''JSON17_SENTSUMMARYOFCAREEVENT'') AND @TABLEKEY >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''TRANSACTIONID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON17_SENTSUMMARYOFCAREEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON18_PATIENTSPECIFICEDUCATIONEVENT'') AND @TABLEKEY >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		 EXEC ACI_ConvertToJson_JSON18_PATIENTSPECIFICEDUCATIONEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT'') AND @TABLEKEY >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''Id'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		 EXEC ACI_CONVERTTOJSON_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT'') AND len(@TABLEKEY) >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''RESPONSEID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		 EXEC ACI_ConvertToJson_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END


	  IF (UPPER(@TABLENAME) =''JSON21_CARECOORDINATIONORDER'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
         set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ACIDEPKEY'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON21_CARECOORDINATIONORDER @FIELDKEY,@ACIINPUTLOGKEY
	  END

--REV1.1
 
	  IF (UPPER(@TABLENAME) =''JSON22_SPECIALISTREPORTRECEIVEDEVENT'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''CLINICALID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_CONVERTTOJSON_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END
	  
	  IF (UPPER(@TABLENAME) =''JSON23_DRCOMMUNICATIONEVENT'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''ACIDEPKEY'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_CONVERTTOJSON_JSON23_DR_COMMUNICATION_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END



	  IF (UPPER(@TABLENAME) =''JSON24_SURGICALPREOPERATIVESTATUSEVENT'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''APPOINTMENTID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON25_SURGERYEVENT'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''APPOINTMENTID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON25_SurgeryEvent @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON26_SURGICALPOSTOPERATIVERESULTEVENT'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''APPOINTMENTID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON27_POSTSURGICALCOMPLICATIONEVENT'') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''APPOINTMENTID'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON27_POST_SURGICAL_COMPLICATION_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) =''JSON28_RESULTS'') AND Len(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS =''N'' AND [FIELDNAME] =''Id'' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON28_Results @FIELDKEY,@ACIINPUTLOGKEY

	  END

--REV1.1


	  if @ACIINPUTLOGKEY1 >0 	     
		insert into ACI_JSONConversionLog_END (ACIINPUTLOGKEY,TableKey,Sort,TempID) values(@ACIINPUTLOGKEY1,@TABLEKEY,@Sort,@newdt)


	  FETCH NEXT FROM CURSORA INTO @TABLENAME,@TABLEKEY,@Sort,@ACIINPUTLOGKEY1
	END
	CLOSE CURSORA
	DEALLOCATE CURSORA

 SET ARITHABORT OFF
END
'
Go
IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'GetFMX')) 
Drop Procedure GetFMX 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure GetFMX(@ApptId int,@PatientId int)
as begin
Select  0 as Id,Notes as History,'' as Comment  from model.FamilyHistory where patientid =  @PatientId  AND appointmentid<=@ApptId AND Notes IS not NULL
Union
select top(10) fh.Id,fr.Name+'/'+CONVERT(varchar(50),conceptid)+'/'+ICD10DESCR as History,fh.Comments as Comment from model.FamilyHistory fh inner    join [model].[FamilyRelationships] fr on fr.Id = fh.relationship  INNER JOIN  DBO.Resources RS 
ON RS.resourceid = FH.lastModofiedBy where  fh.patientid = @PatientId  and fh.status in ('1') and  appointmentid <= @ApptId Order By id ASC 
end
GO