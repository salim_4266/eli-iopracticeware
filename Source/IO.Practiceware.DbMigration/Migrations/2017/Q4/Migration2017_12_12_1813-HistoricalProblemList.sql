		SET NOCOUNT ON
		CREATE TABLE #icd10desc
		(
		icd10 VARCHAR(100),
		icd10_desc VARCHAR(500),
		ConceptId  BIGINT,
		Term VARCHAR(500)
		)
	
		SELECT PC.AppointmentId, PC.PatientId, FindingDetailIcd10, [Description], [Status], conceptId, term, Ap.ResourceId1, Ap.AppDate
		INTO  #DiagnosiIcd10 
		FROM PatientClinicalIcd10 PC
		INNER JOIN Appointments Ap ON Ap.AppointmentId=PC.AppointmentId
		LEFT JOIN SNOMED_2016 SN ON SN.ICD10=FindingDetailIcd10
		WHERE ClinicalType='B'-- AND  PC.patientid=1751

		INSERT INTO  #icd10desc (ConceptId,icd10)
		Select MIN(CONCEPTID) as CONCEPTID, MIN(FindingDetailIcd10) as icd10 from  #DiagnosiIcd10    GROUP BY FindingDetailIcd10

		Update Tmp set Tmp.icd10_desc=SN.icd10_desc  FROM TCI_ICD_Mapping SN  INNER JOIN #icd10desc Tmp ON Tmp.icd10=Sn.ICD10			

		SELECT MIN(ConceptID) as ConceptID , MIn(icd10)  as  ICD10 INTO #MINConcept  FROM  SNOMED_2016    GROUP BY  ICD10	
				 
		UPDATE TmpIcd  SET TmpIcd.ConceptId= (MCP.ConceptID)
		FROM #icd10desc  TmpIcd INNER JOIN  #MINConcept  MCP ON   MCP.ICD10=TmpIcd.icd10
				
		UPDATE TmpIcd  SET  TmpIcd.Term=(SN.Term) , TmpIcd.icd10_desc=icd10descr
		FROM #icd10desc  TmpIcd INNER JOIN SNOMED_2016 SN ON SN.ICD10=TmpIcd.icd10 AND  SN.ConceptID=TmpIcd.ConceptID
		
		UPdate #icd10desc SET ConceptId=74964007, Term='Other' WHERE ConceptId IS NULL and Term IS NULL
		
		Select  T1.AppointmentId, T1.PatientId,  CASE WHEN T1.Status='A' THEN  'Active' ELSE 'inactive'END  as Status , T1.ResourceId1, T1.AppDate,  ICD.Icd10, ISNULL(ICD.icd10_desc,		T1.Description) as icd10_desc, Icd.ConceptId, Icd.Term INTO #FinalIcd10 FROM  #DiagnosiIcd10  AS T1 INNER  JOIN #icd10desc AS ICD  ON T1.FindingDetailIcd10=ICD.icd10 
		and (T1.conceptId=ICD.conceptId OR T1.conceptId IS NULL)

		INSERT INTO model.PatientProblemList (
		PatientId,
		AppointmentId,
		AppointmentDate,
		ConceptID,
		Term,
		ICD10,
		ICD10DESCR,
		COMMENTS,
		ResourceName,
		OnsetDate,
		LastModifyDate,
		Status,
		ResourceId,
		EnteredDate,
		ResolvedDate)
		SELECT 
		PatientId,AppointmentId, Convert(datetime,AppDate, 101),conceptId,term,icd10, icd10_desc, NUll,Null,
		 Convert(datetime,AppDate, 101), Convert(datetime,AppDate, 101),
		[Status], ResourceId1, Convert(datetime,AppDate, 101),NULL
		FROM #FinalIcd10

		IF OBJECT_ID('tempdb..#DiagnosiIcd10') IS NOT NULL
		DROP TABLE #DiagnosiIcd10


		IF OBJECT_ID('tempdb..#FinalIcd10') IS NOT NULL
		DROP TABLE #FinalIcd10

		IF OBJECT_ID('tempdb..#icd10desc') IS NOT NULL
		DROP TABLE #icd10desc

		SET ANSI_PADDING ON

GO