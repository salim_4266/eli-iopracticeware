IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_MEDICATIONS_ROUTE')) 
Drop View VIEW_ACI_MEDICATIONS_ROUTE 

GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[VIEW_ACI_MEDICATIONS_ROUTE]
AS
     SELECT DISTINCT
	 'ROUTE' AS RESOURCETYPE,
		CASE SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-3/', pc.FindingDetail) + 4)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-3/', pc.FindingDetail) + 3)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-3/', pc.FindingDetail) + 3)+1)) - 1)
			WHEN 'INSTILL IN EYES' THEN 'C38287'
			WHEN 'INSTILL IN RIGHT EYE' THEN 'C38287'
			WHEN 'INSTILL IN LEFT EYE' THEN 'C38287'
			WHEN 'INSTILL IN BOTH EYE' THEN 'C38287'
			WHEN 'APPLY TO LIDS' THEN 'C38287'
			WHEN  'APPLY IN RIGHT EYE' THEN 'C38287'
			WHEN  'APPLY IN LEFT EYE' THEN 'C38287'
			WHEN  'APPLY IN BOTH EYE' THEN 'C38287'
			WHEN 'PO' THEN 'C38288'
			WHEN 'Take by mouth' THEN 'C38288'
			WHEN 'INJECTABLE' THEN 'C38192'
			WHEN ' IN RIGHT EYE' THEN 'C38192'
			WHEN ' IN LEFT EYE' THEN 'C38192'
			WHEN ' IN BOTH EYE' THEN 'C38192'
			ELSE ''
		END AS Code, --R
		CASE SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-3/', pc.FindingDetail) + 4)+1)) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-3/', pc.FindingDetail) + 3)+1)+1)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX(')', pc.FindingDetail, CHARINDEX('-3/', pc.FindingDetail) + 3)+1)) - 1)
			WHEN 'INSTILL IN EYES' THEN 'INSTILL IN EYES'
			WHEN 'INSTILL IN RIGHT EYE' THEN 'INSTILL IN EYES'
			WHEN 'INSTILL IN LEFT EYE' THEN 'INSTILL IN EYES'
			WHEN 'INSTILL IN BOTH EYE' THEN 'INSTILL IN EYES'
			WHEN 'APPLY TO LIDS' THEN 'APPLY TO LIDS'
			WHEN  'APPLY IN RIGHT EYE' THEN 'APPLY TO LIDS'
			WHEN  'APPLY IN LEFT EYE' THEN 'APPLY TO LIDS'
			WHEN  'APPLY IN BOTH EYE' THEN 'APPLY TO LIDS'
			WHEN 'Take by mouth' THEN 'BY MOUTH'
			WHEN 'PO' THEN 'BY MOUTH'
			WHEN 'INJECTABLE' THEN 'INJECTABLE'
			WHEN ' IN RIGHT EYE' THEN 'INJECTABLE'
			WHEN ' IN LEFT EYE' THEN 'INJECTABLE'
			WHEN ' IN BOTH EYE' THEN 'INJECTABLE'
			WHEN ' in both eye' THEN 'INJECTABLE'
			ELSE ''
		END AS Description, --R
        '2.16.840.1.113883.3.26.1.1' AS CODESYSTEM, --R
        'NCI Thesures' AS CODESYSTEMNAME, --R
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.appointmentid as Schedulekey,
		 pc.ClinicalId as PATIENTMEDICATIONSKEY
	from  dbo.appointments ap WITH(NOLOCK)
	LEFT JOIN dbo.patientclinical pc WITH(NOLOCK) on pc.appointmentid = ap.appointmentid
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%' and pc.findingdetail not like '%RX%No Known Medication%'
	--AND PC.PATIENTID = 46896
	AND PC.ImageDescriptor NOT LIKE 'C%' and pc.status <>'D'


UNION

  SELECT 'ROUTE' AS RESOURCETYPE,
		'UNK' AS Code, --R
		'UNKNOWN' AS Description, --R
        '2.16.840.1.113883.3.26.1' AS CODESYSTEM, --R
        'NCI Thesures' AS CODESYSTEMNAME, --R
		 pc.appointmentid AS CHARTRECORDKEY,
		 pc.appointmentid as Schedulekey,
		 pc.ClinicalId as PATIENTMEDICATIONSKEY
	from  dbo.appointments a WITH(NOLOCK)
	LEFT JOIN dbo.patientclinical pc WITH(NOLOCK) on a.appointmentid = pc.appointmentid
	WHERE pc.findingdetail like '%RX%No Known Medication%' and pc.status <>'D'
GO


