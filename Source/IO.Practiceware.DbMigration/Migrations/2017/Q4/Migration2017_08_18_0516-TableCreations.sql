/****** Object:  Table [model].[PatientEthnicities] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[PatientEthnicities]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[PatientEthnicities](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[EthnicityId] [int] NOT NULL
    CONSTRAINT [PK_PatientEthnicities] PRIMARY KEY CLUSTERED 
    (
	  [Id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    )ON [PRIMARY]
END
GO

/****** Object:  Table [model].[PatientLanguages] ******/
-----------------------------------------------------------------------------------


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[PatientLanguages]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[PatientLanguages](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
 CONSTRAINT [PK_PatientLanguages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [model].[PatientRace] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[PatientRace]') AND type in (N'U'))
Begin
CREATE TABLE [model].[PatientRace](
	[Patients_Id] [int] NOT NULL,
	[Races_Id] [int] NOT NULL
) ON [PRIMARY]
End
GO

/****** Object:  Table [dbo].[ImplantableDevices] ******/
-----------------------------------------------------------------------------------


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ImplantableDevices]') AND type in (N'U'))
Begin
CREATE TABLE [dbo].[ImplantableDevices](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[UniqueDeviceIdentifier] [nvarchar](200) NULL,
	[IssuingAgency] [nvarchar](500) NULL,
	[DeviceId] [nvarchar](500) NULL,
	[DeviceDescription] [nvarchar](2000) NULL,
	[ManufacturingDate] [datetime] NULL,
	[ExpirtaionDate] [datetime] NULL,
	[LotNumber] [nvarchar](50) NULL,
	[SerialNumber] [nvarchar](50) NULL,
	[BrandName] [nvarchar](200) NULL,
	[CompanyName] [nvarchar](200) NULL,
	[GMDNPTName] [nvarchar](200) NULL,
	[GmdnPTDefinition] [nvarchar](2000) NULL,
	[MRISafetyInformation] [nvarchar](200) NULL,
	[VersionOrModel] [nvarchar](100) NULL,
	[RequiresRubberLabelingTypeId] [nvarchar](100) NULL,
	[HCTPStatus] [bit] NULL,
	[HCTPCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_ImplantableDevices1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [model].[FamilyRelationships] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[FamilyRelationships]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[FamilyRelationships](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[IsDeactivated] [bit] NOT NULL,
	[OrdinalId] [int] NOT NULL,
 CONSTRAINT [PK_FamilyRelationships] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

/****** Object:  Table [model].[PatientReconcilePracticeInformation_PGHD] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[PatientReconcilePracticeInformation_PGHD]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[PatientReconcilePracticeInformation_PGHD](
	[Id] [numeric](18, 0) IDENTITY(2,2) NOT NULL,
	[PatientReconcilePracticeInformationId] [numeric](18, 0) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[CreatedDateTime] [datetime] NULL,
	[Status] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [model].[FamilyHistory] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF Not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[FamilyHistory]') AND type in (N'U'))
Begin
CREATE TABLE [model].[FamilyHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[AppointmentID] [int] NOT NULL,
	[EnteredDate] [datetime] NOT NULL,
	[ConceptID] [bigint] NULL,
	[Term] [varchar](500) NOT NULL,
	[ICD10] [varchar](10) NOT NULL,
	[ICD10DESCR] [varchar](255) NOT NULL,
	[Relationship] [int] NOT NULL,
	[Status] [bit] NOT NULL,
	[COMMENTS] [varchar](max) NULL,
	[LastModofiedDate] [datetime] NULL,
	[lastmodofiedby] [int] NULL,
	[OnsetDate] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_FamilyHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
End
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [model].[FunctionalStatus] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[FunctionalStatus]') AND type in (N'U'))
Begin
CREATE TABLE [model].[FunctionalStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [bigint] NULL,
	[AppointmentId] [bigint] NULL,
	[AppointmentDate] [varchar](20) NULL,
	[EnteredDate] [datetime] NULL,
	[ConceptID] [bigint] NULL,
	[Term] [varchar](500) NULL,
	[Status] [varchar](10) NULL,
	[ResourceId] [int] NULL,
	[LastModifyDate] [datetime] NULL,
	[functionalValue] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
End
GO

/****** Object:  Table [dbo].[PatientClinical_Temp] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF Not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientClinical_Temp]') AND type in (N'U'))
Begin
CREATE TABLE [dbo].[PatientClinical_Temp](
	[ClinicalId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AppointmentId] [int] NULL,
	[PatientId] [int] NULL,
	[ClinicalType] [nvarchar](1) NULL DEFAULT (''),
	[EyeContext] [nvarchar](2) NULL DEFAULT (''),
	[Symptom] [nvarchar](128) NULL DEFAULT (''),
	[FindingDetail] [nvarchar](255) NULL DEFAULT (''),
	[ImageDescriptor] [nvarchar](512) NULL DEFAULT (''),
	[ImageInstructions] [nvarchar](512) NULL DEFAULT (''),
	[Status] [nvarchar](1) NULL DEFAULT (''),
	[DrawFileName] [nvarchar](64) NULL DEFAULT (''),
	[Highlights] [nvarchar](1) NULL DEFAULT (''),
	[FollowUp] [nvarchar](1) NULL DEFAULT (''),
	[PermanentCondition] [nvarchar](1) NULL DEFAULT (''),
	[PostOpPeriod] [int] NULL DEFAULT ((0)),
	[Surgery] [nvarchar](1) NULL DEFAULT (''),
	[Activity] [nvarchar](3) NULL DEFAULT (''),
	[LegacyClinicalDataSourceTypeId] [int] NULL,
 CONSTRAINT [PK_PatientClinical_Temp] PRIMARY KEY CLUSTERED 
(
	[ClinicalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
End
GO

/****** Object:  Table [model].[GoalInstruction] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF Not  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[GoalInstruction]') AND type in (N'U'))
Begin
CREATE TABLE [model].[GoalInstruction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [bigint] NULL,
	[AppointmentId] [bigint] NULL,
	[AppointmentDate] [varchar](20) NULL,
	[EnteredDate] [datetime] NULL,
	[ConceptID] [bigint] NULL,
	[Term] [varchar](500) NULL,
	[Goal] [varchar](500) NULL,
	[Status] [varchar](10) NULL,
	[ResourceId] [int] NULL,
	[LastModifyDate] [datetime] NULL,
	[health] [varchar](500) NULL,
	[reason] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
End
GO

SET ANSI_PADDING OFF
GO


If Exists(select * from information_schema.columns where table_name = 'GoalInstruction' 
and column_name = 'EnteredDate' 
and Table_schema = 'model'
and column_default is NULL)
	BEGIN
		ALTER TABLE [model].[GoalInstruction] ADD  DEFAULT (NULL) FOR [EnteredDate]
	End
GO

If Exists(select * from information_schema.columns where table_name = 'GoalInstruction' 
and column_name = 'ResourceId' 
and Table_schema = 'model'
and column_default is NULL)
	BEGIN
		ALTER TABLE [model].[GoalInstruction] ADD  DEFAULT (NULL) FOR [ResourceId]
	End
GO

If Exists(select * from information_schema.columns where table_name = 'GoalInstruction' 
and column_name = 'health' 
and Table_schema = 'model'
and column_default is NULL)
BEGIN
		ALTER TABLE [model].[GoalInstruction] ADD  DEFAULT (NULL) FOR [health]
End
GO

If Exists(select * from information_schema.columns where table_name = 'GoalInstruction' 
and column_name = 'reason' 
and Table_schema = 'model'
and column_default is NULL)
BEGIN
		ALTER TABLE [model].[GoalInstruction] ADD  DEFAULT (NULL) FOR [reason]
End
GO

/****** Object:  Table [model].[CheckSumData] ******/
-----------------------------------------------------------------------------------


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF Not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CheckSumData]') AND type in (N'U'))
Begin
CREATE TABLE [model].[CheckSumData](
	[CheckSumId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[AuditEntryId] [uniqueidentifier] NULL,
	[ChecksumValue] [varbinary](max) NULL,
	[TamperDate] [datetime] NULL,
	[AlteredChecksum] [varbinary](8000) NULL,
 CONSTRAINT [PK_CheckSumData1] PRIMARY KEY CLUSTERED 
(
	[CheckSumId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
End
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[AuditActionName] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuditActionName]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuditActionName](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[actionID] [int] NULL,
	[ActioName] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [MVE].[PP_VDTLogs] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF Not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_VDTLogs]') AND type in (N'U'))
Begin
CREATE TABLE [MVE].[PP_VDTLogs](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[CDAId] [uniqueidentifier] NOT NULL,
	[CDAType] [nvarchar](100) NULL,
	[CDAUploadedDoctorId] [nvarchar](10) NULL,
	[CDAUploadedDateUTC] [datetime] NULL,
	[CreatedDateTime] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Message] [nvarchar](max) NULL,
	[OperationKey] [nvarchar](50) NULL,
	[PatientExternalId] [nvarchar](50) NULL,
	[UserExternalId] [nvarchar](50) NULL,
	[AlreadySent] [nvarchar](10) NULL,
 CONSTRAINT [PK_PP_VDTLogs_1] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
End
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_PatientExternalLink]') AND type in (N'U'))
DROP TABLE [MVE].[PP_PatientExternalLink]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF Not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_PatientExternalLink]') AND type in (N'U'))
Begin
CREATE TABLE [MVE].[PP_PatientExternalLink](
	[Id] [numeric](18, 0) IDENTITY(2,2) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[TypeId] [nvarchar](100) NULL,
	[ExternalLink] [nvarchar](2000) NULL,
	[Comment] [nvarchar](4000) NOT NULL,
	[DisplayName] [nvarchar](500) NULL,
	[FileName] [nvarchar](500) NULL,
	[Status] [nvarchar](100) NULL,
	[DocumentExternaId] [int] NULL,
	[UserId] [numeric](18, 0) NOT NULL,
	[UserLabel] [nvarchar](100) NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[UpdatedDateTime] [datetime] NULL,
	[IsUrlRead] [bit] NULL,
	[IsArchive] [bit] NULL,
	[IsDownloaded] [bit] NULL,
	[AppointmentId] [int] NULL,
 CONSTRAINT [PK_PP_PatientExternalLinks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
End
GO

/****** Object:  Table [model].[SmokingConditions] ******/
-----------------------------------------------------------------------------------


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF Not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[SmokingConditions]') AND type in (N'U'))
Begin
CREATE TABLE [model].[SmokingConditions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[OrdinalId] [int] NOT NULL,
	[IsDeactivated] [bit] NOT NULL,
	[SNOMEDCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_SmokingConditions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
End
GO
SET ANSI_PADDING OFF
GO
-------------
-- ACI TABLES
-------------

/****** Object:  Table [dbo].[ACI_BATCH_JSON_RESPONSE] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_BATCH_JSON_RESPONSE]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_BATCH_JSON_RESPONSE](
	[ACIJSONRESPONSEKEY] [int] IDENTITY(1,1) NOT NULL,
	[BatchID] [int] NULL,
	[JSONFILENAME] [varchar](200) NULL,
	[RESPONSECODE] [varchar](200) NULL,
	[RESPONSEMESSAGE] [varchar](500) NULL,
	[COMPLETERESPONSE] [varchar](2000) NULL,
	[CREATEDDT] [datetime] NULL,
	[ACI_Status] [varchar](2) NULL,
	[ACI_MessageStatus] [int] NULL,
	[IsValidated] [varchar](1) NULL,
 CONSTRAINT [PK_ACI_BATCH_JSON_RESPONSE] PRIMARY KEY CLUSTERED 
(
	[ACIJSONRESPONSEKEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_DEP]') AND type in (N'U'))
DROP TABLE [dbo].[ACI_DEP]
GO

/****** Object:  Table [dbo].[ACI_DEP] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_DEP]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_DEP](
	[AciDepKey] [int] IDENTITY(1,1) NOT NULL,
	[PatientKey] [int] NULL,
	[ChartrecordKey] [varchar](200) NULL,
	[PtOutboundReferralKey] [int] NULL,
	[ProviderKey] [int] NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF__ACI_DEP__Created__41CE32C8]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[FromAddress] [varchar](250) NULL,
	[ToAddress] [varchar](250) NULL,
	[Dep_RequestDate] [datetime] NULL,
	[Message] [varchar](max) NULL,
	[IsValidJSON] [varchar](1) NULL,
	[ACI_SubmitDT] [datetime] NULL,
	[ACI_MessageStatus] [nvarchar](100) NULL,
	[MessageSubject] [varchar](100) NULL,
	[HISP_Type] [varchar](50) NULL,
	[ClinicalDocumentType] [varchar](50) NULL,
	[ReasonContent] [nvarchar](500) NULL,
 CONSTRAINT [PK_ACI_DEP] PRIMARY KEY CLUSTERED 
(
	[AciDepKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ACI_DEP_Inbound]    Script Date: 2/8/2018 5:34:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_DEP_Inbound]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_DEP_Inbound](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AciDepResponseID] [int] NULL,
	[ResourceType] [varchar](20) NULL,
	[ExternalId] [varchar](20) NULL,
	[ReferenceNumber] [varchar](200) NULL,
	[Source] [varchar](200) NULL,
	[ExternalReferenceId] [varchar](200) NULL,
	[PatientAccountNumber] [varchar](20) NULL,
	[EncounterId] [varchar](100) NULL,
	[ClinicalDocumentType] [varchar](100) NULL,
	[DEPRequestDate] [datetime] NULL,
	[OrderingECNPI] [varchar](20) NULL,
	[AT_ContentDisposition] [varchar](100) NULL,
	[AT_ContentType] [varchar](100) NULL,
	[AT_FileName] [varchar](500) NULL,
	[AT_Content] [varchar](max) NULL,
	[FromProviderAddress] [varchar](1000) NULL,
	[FromAddressPassword] [varchar](100) NULL,
	[ToProviderAddress] [varchar](1000) NULL,
	[HISP_Type] [varchar](50) NULL,
	[TransactionType] [varchar](100) NULL,
	[MessageSubject] [varchar](1000) NULL,
	[MessageBody] [varchar](max) NULL,
	[filepath] [varchar](500) NULL,
	[STATUS] [varchar](25) NULL,
	[AUDITTRACKINGID] [int] NULL,
	[STATUSCODE] [int] NULL,
	[DESCRIPTION] [varchar](max) NULL,
	[ISDELETED] [int] NULL DEFAULT ((0)),
	[MessageViewedByStaffKey] [int] NULL,
	[MessageViewedDate] [datetime] NULL,
	[IsDeletedStaffKey] [int] NULL,
	[IsDeletedDate] [datetime] NULL,
	[StaffKey] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ACI_DEP_JSON_RESPONSE] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_DEP_JSON_RESPONSE]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_DEP_JSON_RESPONSE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ACIDEPKEY] [int] NULL,
	[STATUS] [varchar](25) NULL,
	[AUDITTRACKINGID] [int] NULL,
	[STATUSCODE] [int] NULL,
	[DESCRIPTION] [varchar](max) NULL,
	[Response] [varchar](max) NULL,
	[API.NO] [int] NULL,
	[API_Comments] [varchar](100) NULL,
	[API_Submitdate] [datetime] NULL,
 CONSTRAINT [PK_ACI_DEP_JSON_RESPONSE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ACI_DEP_OutBound_Attachments] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_DEP_OutBound_Attachments]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_DEP_OutBound_Attachments](
	[IdKey] [int] IDENTITY(1,1) NOT NULL,
	[ACI_DepKey] [int] NULL,
	[FileType] [varchar](200) NULL,
	[FileName1] [varchar](2000) NULL,
	[FilePath] [varchar](5000) NULL,
	[FileContent] [varchar](max) NULL,
	[status1] [varchar](1) NULL DEFAULT ('T'),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[IdKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ACI_DEP_Provider] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_DEP_Provider]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_DEP_Provider](
	[IdKey] [int] IDENTITY(1,1) NOT NULL,
	[ProviderKey] [int] NULL,
	[FromProviderAddress] [varchar](200) NULL,
	[Password1] [varchar](2000) NULL,
	[status1] [varchar](1) NULL DEFAULT ('A'),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[IdKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ACI_INPUTLOG] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_INPUTLOG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_INPUTLOG](
	[ACIINPUTLOGKEY] [int] IDENTITY(1,1) NOT NULL,
	[TABLENAME] [varchar](100) NULL,
	[TABLEKEY] [varchar](200) NULL,
	[FIELDNAME] [varchar](50) NULL,
	[FIELDKEY] [varchar](200) NULL,
	[INSERTED] [datetime] NULL,
	[STATUS] [varchar](1) NULL,
	[STATUSCHANGEDT] [datetime] NULL,
	[Sort] [int] NULL,
 CONSTRAINT [PK_ACI_INPUTLOG] PRIMARY KEY CLUSTERED 
(
	[ACIINPUTLOGKEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[aci_json_defaults] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aci_json_defaults]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aci_json_defaults](
	[ACI_VendorId] [varchar](10) NOT NULL,
	[MDO_PracticeId] [varchar](5) NULL,
	[ACI_INTERVAL] [int] NULL,
	[RECORDS] [int] NULL,
	[SIZE] [float] NULL,
	[ACI_BaseURL] [varchar](500) NULL,
	[ACI_ServiceURL] [varchar](1000) NULL,
	[ACI_ProductKey] [varchar](100) NULL,
	[BATCHNO] [int] NULL,
	[BATCHDATE] [varchar](8) NULL,
	[ACI_BatchID] [int] NULL,
	[ACI_PairKey] [varchar](3000) NULL,
	[ACI_USERNAME] [varchar](20) NULL,
	[ACI_ROLE] [varchar](20) NULL,
	[ACI_Dashboard_BaseUrl] [varchar](300) NULL,
	[ACI_Issuer] [varchar](500) NOT NULL,
	[ACI_Audience] [varchar](500) NOT NULL,
	[ACI_Expires] [int] NOT NULL,
	[ACI_Baseurl_PublishDEP] [varchar](500) NULL,
	[ACI_Baseurl_GetPublishDEPResult] [varchar](500) NULL,
	[ACI_Baseurl_GetMessageDeliveryNotification] [varchar](500) NULL,
	[ACI_Baseurl_RetriveInboundDEPData] [varchar](500) NULL,
 CONSTRAINT [PK_aci_json_defaults] PRIMARY KEY CLUSTERED 
(
	[ACI_VendorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ACI_JSON_GetResult] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_JSON_GetResult]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_JSON_GetResult](
	[ACIJSONGetResultKey] [int] IDENTITY(1,1) NOT NULL,
	[BatchID] [int] NULL,
	[BatchNo] [varchar](100) NULL,
	[COMPLETERESPONSE] [varchar](max) NULL,
	[PR_ResponseCode] [varchar](200) NULL,
	[PR_ResponseMessage] [varchar](200) NULL,
	[Result] [varchar](100) NULL,
	[R_BatchId] [varchar](200) NULL,
	[R_ResponseCode] [varchar](200) NULL,
	[R_ResponseMessage] [varchar](500) NULL,
	[ResourcesStatus] [varchar](100) NULL,
	[RS_ResourceId] [varchar](100) NULL,
	[RS_StatusCode] [varchar](100) NULL,
	[RS_Message] [varchar](200) NULL,
	[Status] [varchar](1) NULL DEFAULT ('T'),
	[CreatedDT] [datetime] NULL DEFAULT (getdate()),
	[ModifiedDT] [datetime] NULL,
	[IsValidated] [varchar](1) NULL,
 CONSTRAINT [PK_ACI_JSON_GetResult] PRIMARY KEY CLUSTERED 
(
	[ACIJSONGetResultKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ACI_JSON_MASTER] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_JSON_MASTER]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_JSON_MASTER](
	[ACIJSONMASTERKEY] [int] IDENTITY(1,1) NOT NULL,
	[ACIINPUTLOGKEY] [int] NULL,
	[TABLEKEY] [varchar](100) NULL,
	[REFERENCENUMBER] [varchar](100) NULL,
	[JSON] [varchar](max) NULL,
	[ENCOUNTER] [varchar](max) NULL,
	[ALLERGIES] [varchar](max) NULL,
	[FAMILYHISTORY] [varchar](max) NULL,
	[FUNCTIONALSTATUS] [varchar](max) NULL,
	[IMMUNIZATION] [varchar](max) NULL,
	[MEDICALEQUIPMENT] [varchar](max) NULL,
	[MEDICATION] [varchar](max) NULL,
	[PAYER] [varchar](max) NULL,
	[PLANOFTREATMENT] [varchar](max) NULL,
	[PROBLEMSLIST] [varchar](max) NULL,
	[PROCEDURES] [varchar](max) NULL,
	[RESULT] [varchar](max) NULL,
	[SOCIALHISTORY] [varchar](max) NULL,
	[VITALSIGN] [varchar](max) NULL,
	[INSTRUCTIONS] [varchar](max) NULL,
	[CARETEAM] [varchar](max) NULL,
	[INSERTED] [datetime] NULL DEFAULT (getdate()),
	[ISVALIDJSON] [varchar](1) NULL,
	[BATCHNO] [varchar](200) NULL,
	[STATUS] [varchar](1) NULL DEFAULT ('N'),
	[STATUSCHANGEDT] [datetime] NULL,
	[ACI_SubmitDT] [datetime] NULL,
	[ACI_MessageStatus] [int] NULL,
	[Sort] [int] NULL,
	[PHYSICALEXAM] [varchar](max) NULL,
	[InformantRelated] [varchar](max) NULL,
 CONSTRAINT [PK_ACI_JSON_MASTER] PRIMARY KEY CLUSTERED 
(
	[ACIJSONMASTERKEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ACI_JSONConversionLog_End] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_JSONConversionLog_End]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_JSONConversionLog_End](
	[EndKey] [int] IDENTITY(1,1) NOT NULL,
	[ACIInputLogKey] [int] NULL,
	[TableKey] [varchar](255) NULL,
	[Sort] [int] NULL,
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
	[TempID] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[EndKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ACI_JSONConversionLog_Start] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_JSONConversionLog_Start]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_JSONConversionLog_Start](
	[StartKey] [int] IDENTITY(1,1) NOT NULL,
	[ACIInputLogKey] [int] NULL,
	[TableKey] [varchar](255) NULL,
	[Sort] [int] NULL,
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
	[TempID] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[StartKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ACI_MessageStatus] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_MessageStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_MessageStatus](
	[ACIMessageStatusKey] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Code] [int] NULL,
	[IsActive] [varchar](1) NULL,
 CONSTRAINT [PK_ACI_MessageStatus] PRIMARY KEY CLUSTERED 
(
	[ACIMessageStatusKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[CataractLog]    Script Date: 12/10/2017 11:50:20 AM ******/

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CataractLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CataractLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentId] [int] NULL,
	[PatientId] [int] NULL,
	[AppDate] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

GO

/****** Object:  Table [dbo].[FDBtoRxNorm1]    Script Date: 12/10/2017 2:21:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FDBtoRxNorm1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FDBtoRxNorm1](
	[Id] [varchar](1000) NULL,
	[FDBId] [varchar](1000) NULL,
	[RxNorm] [varchar](1000) NULL
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [MVE].[PP_PortalQueueResponse]    Script Date: 12/10/2017 8:39:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_PortalQueueResponse]') AND type in (N'U'))
BEGIN
CREATE TABLE [MVE].[PP_PortalQueueResponse](
	[ResponseID] [int] IDENTITY(1,1) NOT NULL,
	[MessageData] [nvarchar](max) NOT NULL CONSTRAINT [DF_PortalQueueResponse_MessageData]  DEFAULT (''),
	[ActionTypeLookupId] [int] NOT NULL CONSTRAINT [DF_PortalQueueResponse_ActionTypeLookupId]  DEFAULT ((0)),
	[ProcessedDate] [datetime] NOT NULL CONSTRAINT [DF_PortalQueueResponse_ProcessedDate]  DEFAULT ('1-1-1900'),
	[PatientNo] [varchar](50) NOT NULL CONSTRAINT [DF_PortalQueueResponse_PatientNo]  DEFAULT (''),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PortalQueueResponse_IsActive]  DEFAULT ((1)),
	[ExternalId] [nvarchar](500) NOT NULL CONSTRAINT [DF_PortalQueueResponse_ExternalId]  DEFAULT (''),
 CONSTRAINT [PK_PortalQueueResponse] PRIMARY KEY CLUSTERED 
(
	[ResponseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ACI_MessageStatus] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[Gender_Enumeration]') AND type in (N'U'))
DROP TABLE [model].[Gender_Enumeration]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[Gender_Enumeration]') AND type in (N'U'))
BEGIN

CREATE TABLE [model].[Gender_Enumeration](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[SNOMEDCT] [nvarchar](50) NULL,
	[HL7Code] [nvarchar](50) NULL,
	[IsArchived] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

End

/****** Object:  Table [dbo].[DROPDOWN]    Script Date: 11/19/2017 5:02:56 PM ******/
--DROP TABLE [dbo].[DROPDOWN]
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].DROPDOWN') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[DROPDOWN](
	[TABLENAME] [varchar](50) NOT NULL,
	[FIELDNAME] [varchar](50) NOT NULL,
	[DATA] [varchar](10) NULL,
	[DESCRIPTION1] [varchar](255) NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[DROPDOWN] ADD [ISMERGED] [char](1) NULL
ALTER TABLE [dbo].[DROPDOWN] ADD [Code] [varchar](250) NULL
ALTER TABLE [dbo].[DROPDOWN] ADD [CodeSystem] [varchar](250) NULL
ALTER TABLE [dbo].[DROPDOWN] ADD [CodeSystemOID] [varchar](250) NULL
ALTER TABLE [dbo].[DROPDOWN] ADD [DROPDOWNKEY] [int] IDENTITY(1,1) NOT NULL
ALTER TABLE [dbo].[DROPDOWN] ADD [SNOMEDCT] [bigint] NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[DROPDOWN] ADD [HL7CODE] [varchar](255) NULL
ALTER TABLE [dbo].[DROPDOWN] ADD [OMBCODE] [varchar](20) NULL
ALTER TABLE [dbo].[DROPDOWN] ADD [OMBCatagory] [varchar](255) NULL
 CONSTRAINT [PK_DROPDOWN] PRIMARY KEY CLUSTERED 
(
	[DROPDOWNKEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]


SET ANSI_PADDING OFF
END



GO

/****** Object:  Table [dbo].[UpdoxProviders]    Script Date: 12/11/2017 2:02:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdoxProviders]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UpdoxProviders](
	[ResourceId] [numeric](18, 0) NOT NULL,
	[LoginId] [nvarchar](500) NOT NULL,
	[Password] [nvarchar](500) NOT NULL,
	[TimeZone] [nvarchar](100) NOT NULL,
	[DirectAddress] [nvarchar](500) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsProvider] [bit] NOT NULL,
	[IsAdmin] [bit] NOT NULL,
 CONSTRAINT [PK_UpdoxProviders] PRIMARY KEY CLUSTERED 
(
	[ResourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

GO

/****** Object:  Table [dbo].[UpdoxConfigurationDetails]    Script Date: 12/11/2017 2:16:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdoxConfigurationDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UpdoxConfigurationDetails](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ApplicationId] [nvarchar](500) NOT NULL,
	[ApplicationPassword] [nvarchar](1000) NOT NULL,
	[AccountId] [nvarchar](500) NOT NULL,
	[Environment] [nvarchar](500) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_UpdoxConfigurationDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [dbo].[ACI_DEP_RequestLogs]    Script Date: 1/6/2018 1:37:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACI_DEP_RequestLogs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ACI_DEP_RequestLogs](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[UserId] [numeric](18, 0) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[JSONDetails] [nvarchar](2000) NULL,
 CONSTRAINT [PK_ACI_DEP_RequestLogs_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_PatientEducationMaterial]') AND type in (N'U'))
BEGIN

CREATE TABLE [MVE].[PP_PatientEducationMaterial](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ExternalId] [numeric](18, 0) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[AppointmentId] [numeric](18, 0) NOT NULL,
	[ResourceId] [numeric](18, 0) NOT NULL,
	[DocumentName] [nvarchar](500) NOT NULL,
	[DocumentType] [nvarchar](500) NOT NULL,
	[Url] [nvarchar](500) NOT NULL,
	[CodeType] [nvarchar](100) NOT NULL,
	[CreatedDateUTC] [datetime] NOT NULL,
	[Reviews] [int] NOT NULL,
 CONSTRAINT [PK_PP_PatientEducationMaterial_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End

Go
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[eRx_Medicine_Response]') AND type in (N'U'))
BEGIN
CREATE TABLE [DrFirst].[eRx_Medicine_Response](
	[eRx_Medicine_Response_Key] [bigint] IDENTITY(1,1) NOT NULL,
	[Patient_ExtID] [bigint] NULL,
	[ClinicalID] [bigint] NULL,
	[NDCID] [varchar](max) NULL,
	[FirstDataBankMedID] [varchar](100) NULL,
	[RcopiaID] [varchar](100) NULL,
	[BrandName] [varchar](500) NULL,
	[GenericName] [varchar](500) NULL,
	[BrandType] [varchar](500) NULL,
	[RouteDrug] [varchar](100) NULL,
	[Form] [varchar](100) NULL,
	[Strength] [varchar](100) NULL,
	[Action] [varchar](100) NULL,
	[Dose] [varchar](100) NULL,
	[DoseUnit] [varchar](500) NULL,
	[Route] [varchar](500) NULL,
	[Frequency] [varchar](500) NULL,
	[Duration] [varchar](500) NULL,
	[Quantity] [varchar](500) NULL,
	[QuantityUnit] [varchar](500) NULL,
	[Refills] [varchar](500) NULL,
	[OtherNotes] [varchar](500) NULL,
	[PatientNotes] [varchar](1000) NULL,
	[SigChangedDate] [varchar](50) NULL,
	[LastModifiedDate] [varchar](50) NULL,
	[StartDate] [varchar](50) NULL,
	[StopDate] [varchar](50) NULL,
	[Deleted] [varchar](50) NULL,
	[StopReason] [varchar](50) NULL,
	[MedicationRcopiaID] [varchar](50) NULL,
	[PrescriptionRcopiaID] [varchar](100) NULL DEFAULT (NULL),
	[RxnormID] [varchar](100) NULL,
	[RxnormIDType] [varchar](100) NULL,
	 [AppointmentId] [bigint] NULL DEFAULT (NULL),
	 [PharmacyRcopiaID] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyRcopiaMasterID] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyDeleted] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyName] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyAddress1] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyState] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyZip] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyPhone] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyFax] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyIs24Hour] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyLevel3] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyElectronic] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyMailOrder] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyRequiresEligibility] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyRetail] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyLongTermCare] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacySpecialty] [varchar](100) NULL DEFAULT (NULL),
	 [PharmacyCanReceiveControlledSubstance] [varchar](100) NULL DEFAULT (NULL),
	 [DrugSchedule] [varchar](100) NULL DEFAULT (NULL)
 CONSTRAINT [PK_eRx_Medicine_Response] PRIMARY KEY CLUSTERED 
(
	[eRx_Medicine_Response_Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]) ON [PRIMARY]
End
SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[PracticeConfiguration]') AND type in (N'U'))
BEGIN
CREATE TABLE [DRFIRST].[PracticeConfiguration](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Version] [nvarchar](50) NOT NULL,
	[VendorName] [nvarchar](200) NOT NULL,
	[VendorPassword] [nvarchar](500) NOT NULL,
	[SystemName] [nvarchar](1000) NOT NULL,
	[PracticeUserName] [nvarchar](1000) NOT NULL,
	[UploadUrl] [nvarchar](2000) NOT NULL,
	[DownloadUrl] [nvarchar](2000) NOT NULL,
	[SSOUrl] [nvarchar](2000) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_DrFirst.PracticeConfiguration] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End
SET ANSI_PADDING OFF
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[UserConfiguration]') AND type in (N'U'))
BEGIN
CREATE TABLE [DRFIRST].[UserConfiguration](
	[ResourceId] [numeric](18, 0) NOT NULL,
	[ExternalId] [nvarchar](500) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_UserConfirguration] PRIMARY KEY CLUSTERED 
(
	[ResourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End
SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT 1 FROM DRFIRST.PracticeConfiguration WHERE IsActive=1)
BEGIN
Insert into DRFIRST.PracticeConfiguration(Version, VendorName, VendorPassword, SystemName, PracticeUserName, UploadUrl, DownloadUrl, SSOUrl, IsActive, CreatedDate, ModifiedDate) values 
('2.28', '', '', '', '', '', '', '', 1, GETDATE(), NULL)
END



