
/****** Object:  Trigger [model].[PP_LogPhoneUpdateTrigger]    Script Date: 7/17/2018 4:45:28 PM ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[PP_LogPhoneUpdateTrigger]'))
DROP TRIGGER [model].[PP_LogPhoneUpdateTrigger]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [model].[PP_LogPhoneUpdateTrigger] ON [model].[PatientPhoneNumbers] AFTER UPDATE NOT FOR REPLICATION AS
BEGIN
SET NOCOUNT ON
DECLARE @PatientId Numeric(18,0)
	
SELECT @PatientId = INSERTED.PatientId FROM INSERTED

IF EXISTS (select 1 from [MVE].[PP_ERPInputLog] with (nolock) where PatientId = @PatientId)
BEGIN
	IF  EXISTS (select 1 from [MVE].[PP_ERPInputLog] with (nolock) where PatientId = @PatientId and Status ='N')
		BEGIN
			UPDATE [MVE].[PP_ERPInputLog] SET 
			IsPhoneUpdated = 1,
			InputLogdate=GETDATE()  WHERE 
			PatientId = @PatientId and Status = 'N'
		END
	ELSE
		BEGIN
			INSERT INTO [MVE].[PP_ERPInputLog](Email, PatientId, InputLogDate, IsEmailUpdated, IsPhoneUpdated, IsAddressUpdated, Status, ProcessedDate, IsPatientUpdated) 
			VALUES('', @PatientId, GETDATE(), 0, 1, 0, 'N', NULL, 1)
		END
END
SET NOCOUNT OFF
END
GO


