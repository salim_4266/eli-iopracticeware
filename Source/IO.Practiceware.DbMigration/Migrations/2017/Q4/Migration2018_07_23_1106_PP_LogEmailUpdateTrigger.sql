

/****** Object:  Trigger [model].[PP_LogEmailUpdateTrigger]    Script Date: 7/17/2018 4:53:19 PM ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[PP_LogEmailUpdateTrigger]'))
DROP TRIGGER [model].[PP_LogEmailUpdateTrigger]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [model].[PP_LogEmailUpdateTrigger] ON [model].[PatientEmailAddresses] AFTER UPDATE NOT FOR REPLICATION AS
BEGIN
SET NOCOUNT ON
DECLARE @PatientId INT
DECLARE @EmailNew VARCHAR(100)
--DECLARE @EmailOld VARCHAR(100)
	
SELECT @PatientId = INSERTED.PatientId, @EmailNew = INSERTED.Value FROM INSERTED
--SELECT @EmailOld = Email from [MVE].[PP_ERPInputLog] with (nolock) where PatientId = @PatientId

If EXISTS(Select 1 from [MVE].[PP_ERPInputLog] with (nolock) where PatientId = @PatientId and STATUS='N')
 BEGIN
	
	--If((Select [Status] from [MVE].[PP_ERPInputLog] with (nolock) where Patientid= @PatientId and Email = @EmailId) = 'C')
	-- Begin
	    --Update MVE.PP_ERPInputLog Set Email = '', IsEmailUpdated = 0 Where PatientId = @PatientId and Status  = 'N'
	 --END
	--Else
	 --Begin
	 IF UPDATE(Value)
	 --IF (@EmailNew <> @EmailOld)
	 BEGIN
	    Update MVE.PP_ERPInputLog Set Email = @EmailNew, IsEmailUpdated = 1 Where PatientId = @PatientId --and Status  = 'N'
	 END
	 --End 
 END
Else
Begin
	--If NOT EXISTS(select 1 from [MVE].[PP_ERPInputLog] with (nolock) where Patientid= @PatientId and Email <> @EmailNew)
	--Begin
        INSERT INTO [MVE].[PP_ERPInputLog](Email, PatientId, InputLogDate, IsEmailUpdated, IsPhoneUpdated, IsAddressUpdated, Status, ProcessedDate, IsPatientUpdated) 
		VALUES(@EmailNew, @PatientId, GETDATE(), 1, 0, 0, 'N', NULL, 1)
	--End 
End
SET NOCOUNT OFF
END


GO


