

/****** Object:  Trigger [model].[PP_LogPatientsUpdateTrigger]    Script Date: 7/17/2018 4:51:03 PM ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[PP_LogPatientsUpdateTrigger]'))
DROP TRIGGER [model].[PP_LogPatientsUpdateTrigger]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [model].[PP_LogPatientsUpdateTrigger] ON [model].[Patients] AFTER UPDATE NOT FOR REPLICATION AS
BEGIN
				
SET NOCOUNT ON
DECLARE @PatientId INT
SELECT @PatientId = INSERTED.Id FROM INSERTED

	IF NOT EXISTS (Select 1 from [MVE].[PP_ERPInputLog] with (nolock) where PatientId = @PatientId and status='N')
	BEGIN
		INSERT INTO [MVE].[PP_ERPInputLog](Email, PatientId, InputLogDate, IsEmailUpdated, IsPhoneUpdated, IsAddressUpdated, Status, ProcessedDate, IsPatientUpdated) 
			VALUES('', @PatientId, GETDATE(), 0, 0, 0, 'N', NULL, 1)
	END
	ELSE
	BEGIN
	   UPDATE [MVE].[PP_ERPInputLog] SET IsPatientUpdated = 1, PatientId=@PatientId, InputLogdate=GETDATE()  Where PatientId = @PatientId and Status = 'N'
	END
SET NOCOUNT OFF

END


GO


