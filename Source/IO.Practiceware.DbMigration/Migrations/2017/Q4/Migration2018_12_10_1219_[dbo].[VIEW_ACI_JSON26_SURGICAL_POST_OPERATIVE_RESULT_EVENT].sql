IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'dbo.VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT')) 
Drop Trigger dbo.VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT]
AS
	
	SELECT DISTINCT
		'SurgicalPostOperativeResultEvent' AS ResourceType, --R
		'EHR' AS IsSource, --R
		PC.PATIENTID AS PatientAccountNumber, --R
		--CONVERT(VARCHAR(10), AP.APPDATE, 101) as DateofEvent, --R
		SUBSTRING(AP.APPDATE,5,2) + '/' + SUBSTRING(AP.APPDATE,7,2) + '/' + SUBSTRING(AP.APPDATE,1,4) as DateofEvent,
		ISNULL(r.npi,'0000000000') as PerformedPhysicianNPI, --R
		RIGHT('000000000'+CONVERT(VARCHAR, SUBSTRING(isnull(r.resourcetaxid, ''), 1, 9)), 9) AS ECTIN, --R
		PC.APPOINTMENTID AS ExternalId,
		--'67724' AS EpisodeNumber,
		 CASE
			WHEN ((select count(*) from dbo.CataractLog)=1) THEN (Select top (1) AppointmentId from dbo.CataractLog where patientid = PC.PATIENTID)
		ELSE
			(Select top (1) AppointmentId from dbo.CataractLog where patientid = PC.PATIENTID AND (AP.Appdate>Appdate OR AP.Appdate=Appdate)  order by Appdate desc)
		END AS EpisodeNumber,
		--PC.APPOINTMENTID AS ReferenceNumber,
		CASE
			WHEN ((select count(*) from dbo.CataractLog)=1) THEN (Select top (1) AppointmentId from dbo.CataractLog where patientid = PC.PATIENTID)
		ELSE
			(Select top (1) AppointmentId from dbo.CataractLog where patientid = PC.PATIENTID AND (AP.Appdate>Appdate OR AP.Appdate=Appdate)  order by Appdate desc)
		END ASReferenceNumber, --Change By Ayush Nov 2018
		PC.CLINICALID,
		'TRUE' AS IsInHousePhysician,
		r.npi as ECNPI, --R
		R.RESOURCEID AS PerformedPhysicianAccountNumber,
		R.RESOURCENAME AS PerformedPhysicianName,
		--R.ResourceType As ResourceType,
		'NOT AVAilable' AS ReferralOrderNumber,
		ap.AppointmentId,
		ap.patientid,
		BS.Id AS Id
	from model.billingservices BS WITH(NOLOCK) JOIN model.encounterservices ES WITH(NOLOCK) ON ES.ID =BS.EncounterServiceId
		JOIN model.Invoices Inv ON Inv.Id = BS.InvoiceId
		JOIN dbo.Appointments Ap WITH(NOLOCK) ON AP.AppointmentId = Inv.EncounterId
		JOIN dbo.Resources R WITH(NOLOCK) ON R.ResourceId = Ap.ResourceId1
		JOIN dbo.PatientClinical PC WITH(NOLOCK) ON Pc.AppointmentID = Ap.AppointmentId
		JOIN dbo.VIEW_ACI_JSON25_JSON26_JSON27_EPISODENUMBER En on Ap.AppointmentId= En.AppointmentId
	WHERE ES.CODE = '99024' AND AP.AppTypeId >0 And Ap.ActivityStatus = 'D' and ScheduleStatus = 'D'


GO
