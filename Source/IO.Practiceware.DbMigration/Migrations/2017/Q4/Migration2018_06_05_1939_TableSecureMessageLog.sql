
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_SecureMessagesLogs]') AND type in (N'U'))
BEGIN

CREATE TABLE [MVE].[PP_SecureMessagesLogs](
	[LogId] [uniqueidentifier] NOT NULL,
	[RecipientId] [nvarchar](200) NOT NULL,
	[PatientId] [nvarchar](200)  NOT NULL,
	[IsSent2Repersenative] [bit] NOT NULL,
	[RepersentativeId] [nvarchar](200) NULL,
	[MessageSubject] [nvarchar](200) NOT NULL,
	[MessageSentTime] [datetime] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[IsResponseSent] [bit] NULL,
 CONSTRAINT [PK_PP_SecureMessagesLogs] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO



