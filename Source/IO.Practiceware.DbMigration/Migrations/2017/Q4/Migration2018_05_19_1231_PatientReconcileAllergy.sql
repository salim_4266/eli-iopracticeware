IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.PatientReconcileAllergy')  AND name = 'MergedDate')
BEGIN
	Alter table model.PatientReconcileAllergy 
	Add MergedDate datetime
End

Go
IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.PatientReconcileProblems')  AND name = 'MergedDate')
BEGIN
	Alter table model.PatientReconcileProblems 
	Add MergedDate datetime
End

GO
