

/****** Object:  Trigger [model].[PP_LogEmailInsertTrigger]    Script Date: 7/17/2018 4:55:14 PM ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[PP_LogEmailInsertTrigger]'))
DROP TRIGGER [model].[PP_LogEmailInsertTrigger]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE TRIGGER [model].[PP_LogEmailInsertTrigger] ON [model].[PatientEmailAddresses] AFTER INSERT NOT FOR REPLICATION AS
BEGIN
				
SET NOCOUNT ON
DECLARE @PatientId INT
DECLARE @EmailId VARCHAR(100)

SELECT @PatientId = INSERTED.PatientId, @EmailId = inserted.Value FROM INSERTED
If EXISTS(Select 1 from [MVE].[PP_ERPInputLog] with (nolock) where PatientId = @PatientId and [STATUS]='N')
	--If (Select [STATUS] from [MVE].[PP_ERPInputLog] with (nolock) where PatientId = @PatientId and Email=@EmailId) = 'C'
	--	Begin
		   --UPDATE [MVE].[PP_ERPInputLog] SET IsEmailUpdated = 0, Email = '' Where PatientId = @PatientId and Status = 'N'
		--End
	--Else
		BEGIN
			UPDATE [MVE].[PP_ERPInputLog] SET IsEmailUpdated = 1, Email = @EmailId, InputLogdate=GETDATE() WHERE PatientId = @PatientId --and Status = 'N'
		END
Else
	Begin
		--IF NOT EXISTS(Select 1 from [MVE].[PP_ERPInputLog] with (nolock) Where PatientId = @PatientId and Email <> @EmailId) 
		--Begin
			INSERT INTO [MVE].[PP_ERPInputLog](Email, PatientId, InputLogDate, IsEmailUpdated, IsPhoneUpdated, IsAddressUpdated, Status, ProcessedDate, IsPatientUpdated) 
			VALUES(@EmailId, @PatientId, GETDATE(), 1, 0, 0, 'N', NULL, 1)
	    --End 
	End
SET NOCOUNT OFF
END


GO


