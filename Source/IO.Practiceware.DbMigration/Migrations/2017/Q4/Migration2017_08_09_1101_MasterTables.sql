SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SNOMED_2016]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SNOMED_2016](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[effectiveTime] [varchar](1000) NULL,
	[active] [varchar](1000) NULL,
	[moduleId] [varchar](1000) NULL,
	[conceptId] [bigint] NULL,
	[languageCode] [varchar](1000) NULL,
	[typeId] [varchar](1000) NULL,
	[term] [varchar](1000) NULL,
	[Icd10] [varchar](1000) NULL,
	[ICD10DESCR] [varchar](1000) NULL,
	[COMMENTS] [varchar](1000) NULL
 CONSTRAINT [PK_SNOMED_2016] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TCI_ICD_Mapping]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TCI_ICD_Mapping](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[icd9_code] [nvarchar](255) NULL,
	[icd9_nl] [nvarchar](255) NULL,
	[specialty] [nvarchar](255) NULL,
	[icd9_desc] [nvarchar](255) NULL,
	[review_system_lingo] [nvarchar](255) NULL,
	[icd10] [nvarchar](255) NULL,
	[icd10_desc] [nvarchar](255) NULL,
	[gem_ext] [nvarchar](255) NULL,
	[Side] [nvarchar](255) NULL,
	[Level_2] [nvarchar](255) NULL
 CONSTRAINT [PK_TCI_ICD_Mapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LOINC_2017]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LOINC_2017](
	[LOINC_NUM] [varchar](100) NOT NULL,
	[COMPONENT] [varchar](2000) NULL,
	[PROPERTY] [varchar](2000) NULL,
	[TIME_ASPCT] [varchar](2000) NULL,
	[SYSTEM] [varchar](2000) NULL,
	[SCALE_TYP] [varchar](2000) NULL,
	[METHOD_TYP] [varchar](2000) NULL,
	[CLASS] [varchar](2000) NULL,
	[VersionLastChanged] [varchar](2000) NULL,
	[CHNG_TYPE] [varchar](2000) NULL,
	[STATUS] [varchar](2000) NULL,
	[CONSUMER_NAME] [varchar](2000) NULL,
	[CLASSTYPE] [varchar](2000) NULL,
	[SPECIES] [varchar](2000) NULL,
	[UNITSREQUIRED] [varchar](2000) NULL,
	[SUBMITTED_UNITS] [varchar](2000) NULL,
	[SHORTNAME] [varchar](2000) NULL,
	[ORDER_OBS] [varchar](2000) NULL,
	[CDISC_COMMON_TESTS] [varchar](2000) NULL,
	[LONG_COMMON_NAME] [varchar](2000) NULL,
	[CHANGE_REASON_PUBLIC] [varchar](2000) NULL,
	[VersionFirstReleased] [varchar](2000) NULL,
 CONSTRAINT [PK_LOINC_2017] PRIMARY KEY CLUSTERED 
(
	[LOINC_NUM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [dbo].[LoincTestResults]    Script Date: 11/29/2017 5:43:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LoincTestResults]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LoincTestResults](
	[Id] [numeric](18, 0) NOT NULL,
	[ParentLoincNumber] [nvarchar](10) NOT NULL,
	[LoincNumber] [nvarchar](10) NOT NULL,
	[CompanyName] [nvarchar](500) NOT NULL,
	[IsArchieved] [bit] NULL,
	[LoincName] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_LoincTestResults] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END
GO

/****** Object:  Table [dbo].[tblValidatedNDCList]    Script Date: 11/29/2017 5:43:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblValidatedNDCList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblValidatedNDCList](
	[NDC] [numeric](18, 0) NOT NULL,
	[LBLRID] [nvarchar](max) NULL,
	[MFG] [nvarchar](max) NULL,
	[BN] [nvarchar](max) NULL,
	[LN] [nvarchar](max) NULL,
	[LN60] [nvarchar](max) NULL,
	[MEDID] [decimal](8, 0) NOT NULL,
 CONSTRAINT [PK_tblValidatedNDCList] PRIMARY KEY CLUSTERED 
(
	[NDC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END
GO

/****** Object:  Table [dbo].[AllergyRxMapping]    Script Date: 11/29/2017 5:43:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AllergyRxMapping]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AllergyRxMapping](
	[ALLERGYKEY] [int] NOT NULL,
	[DESCRIPTION1] [nvarchar](255) NULL,
	[FDBCONCEPTID] [float] NULL,
	[FDBCOMPOSITEALLERGYID] [float] NULL,
	[IMK_FDB_VOCAB_DESC] [nvarchar](255) NULL,
	[EVD_RXN_RXCUI] [float] NULL,
	[IMK_EXT_VOCAB_DESC] [nvarchar](255) NULL,
	[EVD_VOCAB_TYPE_DESC] [nvarchar](255) NULL,
 CONSTRAINT [PK_AllergyRxMapping] PRIMARY KEY CLUSTERED 
(
	[ALLERGYKEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END
GO

SET ANSI_PADDING OFF
GO

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = 'DRFIRST' ) 
BEGIN
EXEC sp_executesql N'CREATE SCHEMA DRFIRST'  
END

GO
