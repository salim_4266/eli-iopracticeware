

/****** Object:  StoredProcedure [MVE].[PP_InsertPortalQueue]    Script Date: 06/29/2018 13:42:16 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_InsertPortalQueue]')) 
Drop Procedure [MVE].[PP_InsertPortalQueue]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  


CREATE procedure [MVE].[PP_InsertPortalQueue]
(
@MessageData nvarchar(max),
@ActionTypeLookupId int,
@ProcessedDate datetime,
@PatientNo varchar(50),
@IsActive bit,
@Requestid int output
)
as
Begin
   Insert into [MVE].[PP_PortalQueue](MessageData,ActionTypeLookupId,ProcessedDate,PatientNo,IsActive)values(@MessageData,@ActionTypeLookupId,@ProcessedDate,@PatientNo,@IsActive)
   SELECT @Requestid=SCOPE_IDENTITY()
End

GO

