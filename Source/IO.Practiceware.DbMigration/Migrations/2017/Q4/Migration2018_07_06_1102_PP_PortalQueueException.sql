

/****** Object:  Table [MVE].[PP_PortalQueueException]    Script Date: 06/29/2018 13:27:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_PortalQueueException]') AND type in (N'U'))
BEGIN

CREATE TABLE [MVE].[PP_PortalQueueException](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MessageData] [nvarchar](max) NOT NULL,
	[ActionTypeLookupId] [int] NOT NULL,
	[ProcessedDate] [datetime] NOT NULL,
	[PatientNo] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ExternalId] [nvarchar](500) NOT NULL,
	[Exception] [text] NOT NULL,
	[ProcessCount] [int] NULL,
 CONSTRAINT [PK_PP_PortalQueueException] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_MessageData]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] DROP CONSTRAINT [DF_PP_PortalQueueException_MessageData]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_MessageData]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] ADD  CONSTRAINT [DF_PP_PortalQueueException_MessageData]  DEFAULT ('') FOR [MessageData]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_ActionTypeLookupId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] DROP CONSTRAINT [DF_PP_PortalQueueException_ActionTypeLookupId]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_ActionTypeLookupId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] ADD  CONSTRAINT [DF_PP_PortalQueueException_ActionTypeLookupId]  DEFAULT ((0)) FOR [ActionTypeLookupId]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_ProcessedDate]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] DROP CONSTRAINT [DF_PP_PortalQueueException_ProcessedDate]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_ProcessedDate]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] ADD  CONSTRAINT [DF_PP_PortalQueueException_ProcessedDate]  DEFAULT ('1-1-1900') FOR [ProcessedDate]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_PatientNo]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] DROP CONSTRAINT [DF_PP_PortalQueueException_PatientNo]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_PatientNo]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] ADD  CONSTRAINT [DF_PP_PortalQueueException_PatientNo]  DEFAULT ('') FOR [PatientNo]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_IsActive]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] DROP CONSTRAINT [DF_PP_PortalQueueException_IsActive]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_IsActive]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] ADD  CONSTRAINT [DF_PP_PortalQueueException_IsActive]  DEFAULT ((0)) FOR [IsActive]
END
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_ExternalId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] DROP CONSTRAINT [DF_PP_PortalQueueException_ExternalId]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_ExternalId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] ADD  CONSTRAINT [DF_PP_PortalQueueException_ExternalId]  DEFAULT ('') FOR [ExternalId]
END
GO  


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_Exception]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] DROP CONSTRAINT [DF_PP_PortalQueueException_Exception]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_PortalQueueException_Exception]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueueException] ADD  CONSTRAINT [DF_PP_PortalQueueException_Exception]  DEFAULT ('') FOR [Exception]
END
GO


If not Exists(select * from information_schema.columns where table_name = 'PP_PortalQueueException' 
and column_name = 'ProcessCount' 
and Table_schema = 'mve'
and column_default is NOT NULL)
begin
	ALTER TABLE [MVE].[PP_PortalQueueException] ADD  DEFAULT ((0)) FOR [ProcessCount]
end
GO





