
IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'ACI_INSERT_ACI_JSON_MASTER')) 
Drop Procedure ACI_INSERT_ACI_JSON_MASTER 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  


CREATE PROCEDURE [dbo].[ACI_INSERT_ACI_JSON_MASTER] @SortIns INT AS
BEGIN
SET ARITHABORT ON
	DECLARE @TABLENAME VARCHAR(100) ,@FIELDNAME VARCHAR(50),@TABLEKEY VARCHAR(100),@FIELDKEY VARCHAR(100),@Sort INT
	 ,@CHARTRECORDKEY INT,@PROCEDUREKEY INT,@ACIINPUTLOGKEY INT ,@ACIINPUTLOGKEY1 INT , @TABLEKEY1 VARCHAR(100)


  DECLARE    @mm Varchar(2),
  @dd Varchar(2),  @yy Varchar(4),
  @hh Varchar(2),  @mi Varchar(2),
  @ss Varchar(2),  @ms Varchar(4),
  @date datetime,  @Newdt Varchar(50)
 
  SELECT @date = GETDATE()
  SELECT @mm = convert(Varchar (2),datePART(mm, @date))
  SELECT @dd=      convert(Varchar (2),datePART(dd,@date))
  SELECT @yy =  convert(Varchar (4),datePART(yyyy,@date))
  SELECT @hh =  convert(Varchar (2),datePART(hh,@date))
  SELECT @mi =  convert(Varchar (2),datePART(mi, @date))
  SELECT @ss =   convert(Varchar (2),datePART(ss,@date))
  SELECT @ms =   convert(Varchar (4),datePART(ms,@date))

  Set  @newdt = rtrim(@mm)+rtrim(@dd)+@yy+@hh+@mi+@ss+@ms
  If @SortIns > 0
   Begin
	DECLARE CURSORA SCROLL CURSOR FOR 
      SELECT top (select isnull(records,100) from aci_JSON_defaults) SS.TABLENAME,SS.TABLEKEY,sort,ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) WHERE STATUS='N' and Sort=@SortIns ORDER BY SS.Sort
   End  
  Else
   Begin
     DECLARE CURSORA SCROLL CURSOR FOR 
      SELECT top (select isnull(records,100) from aci_JSON_defaults) SS.TABLENAME,SS.TABLEKEY,sort,ACIINPUTLOGKEY
        FROM ACI_INPUTLOG SS (NOLOCK) WHERE STATUS='N' ORDER BY SS.Sort
   End

	OPEN CURSORA   
	FETCH FIRST FROM CURSORA INTO @TABLENAME,@TABLEKEY,@Sort,@ACIINPUTLOGKEY1
	WHILE @@FETCH_STATUS = 0
	BEGIN
	  If @ACIINPUTLOGKEY1 > 0 
	       Insert into ACI_JSONConversionLog_Start (ACIINPUTLOGKEY,TableKey,Sort,TempID) values(@ACIINPUTLOGKEY1,@TABLEKEY,@Sort,@newdt)
	  
	  IF (UPPER(@TABLENAME) ='PATIENTEXAM') AND @TABLEKEY > 0 
	  BEGIN
	    Set @CHARTRECORDKEY=0
		Set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @CHARTRECORDKEY=SS.TABLEKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='ID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 		 and TABLEKEY IN (SELECT top 1 EncounterID from model.invoices inv WHERE EncounterID=@TABLEKEY)
	    If @TABLEKEY >0 and @ACIINPUTLOGKEY >0
	     Begin 
		  EXEC ACI_ConvertToJson_JSON07_PatientExam  0 ,@CHARTRECORDKEY ,0,@ACIINPUTLOGKEY
	     END
      END  
	  IF (UPPER(@TABLENAME) ='JSON01_FACILITYINFORMATION') AND len(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0

		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='PRACTICEID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 

		If @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		BEGIN   
		   EXEC ACI_CONVERTTOJSON_JSON01_FACILITYINFORMATION @FIELDKEY,@ACIINPUTLOGKEY
        END
      END

	  IF (UPPER(@TABLENAME) ='JSON02_ELIGIBLECLINICIANINFORMATION') AND len(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    Set @ACIINPUTLOGKEY =0

		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY FROM ACI_INPUTLOG SS (NOLOCK) WHERE STATUS ='N' AND   [FIELDNAME] ='RESOURCEID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		
		If @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		BEGIN 
		  EXEC ACI_CONVERTTOJSON_JSON02_ELIGIBLECLINICIANINFORMATION @FIELDKEY,@ACIINPUTLOGKEY
        END
	  END

	  IF (UPPER(@TABLENAME) ='JSON03_ACIMODULEUSERACCOUNTADMINISTRATION') AND LEN(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    Set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY FROM ACI_INPUTLOG SS (NOLOCK) WHERE STATUS ='N' AND [FIELDNAME] ='RESOURCEID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		
		If @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		BEGIN 
		   EXEC ACI_CONVERTTOJSON_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION @FIELDKEY,@ACIINPUTLOGKEY
        END
	  END
	  IF (UPPER(@TABLENAME) ='JSON04_PATIENTDEMOGRAPHICSINFORMATION') AND LEN(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    Set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='ID' AND TABLENAME  ='JSON04_PATIENTDEMOGRAPHICSINFORMATION'  AND TABLEKEY=@TABLEKEY and   ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 ORDER BY ACIINPUTLOGKEY DESC
     	if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON04_PATIENTDEMOGRAPHICSINFORMATION @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) ='JSON05_REPRESENTATIVE') AND len(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='PATIENTREPRESENTATIVEID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON05_REPRESENTATIVE @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) ='JSON9_PORTALACCOUNTADMINISTRATION') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0

		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='RESPONSEID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_ConvertToJson_JSON9_PORTALACCOUNTADMINISTRATION @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) ='JSON10_CPOELABORATORYORIMAGING') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=''
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='OrderId' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if len(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_ConvertToJson_JSON10_CPOELABORATORYORIMAGING @FIELDKEY,@ACIINPUTLOGKEY
	  END
	  
	  IF (UPPER(@TABLENAME) ='JSON12_CLINICALDATARECONCILIATION') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=''
	    set @ACIINPUTLOGKEY =0
		DECLARE @PI int
		SET @PI = 0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='ID' AND TABLENAME = @TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY = @ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		SELECT @PI = defaultuserid from model.patients where id = @FIELDKEY
		
		if len(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0 AND @PI is not null and LTRIM(RTRIM(@PI)) <>''
		 EXEC ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION @FIELDKEY,@ACIINPUTLOGKEY,'Ext'
	  END
	  

	  IF (UPPER(@TABLENAME) ='JSON12_CLINICALDATARECONCILIATION_PGHD') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=''
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='ID' AND TABLENAME = @TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		If len(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0
		   EXEC ACI_ConvertToJson_JSON12_CLINICALDATARECONCILIATION_PGHD @FIELDKEY,@ACIINPUTLOGKEY,'Ext'
	   END

	  IF (UPPER(@TABLENAME) ='JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE') AND LEn(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK)   
		WHERE STATUS ='N' AND [FIELDNAME] ='LogID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1  
		ORDER BY ACIINPUTLOGKEY DESC
		
		if LEN(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_ConvertToJson_JSON13_SECUREMESSAGETOPATIENTORREPRESENTATIVE @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) ='JSON15_CLAIMSUBMISSIONEVENT') AND len(@TABLEKEY) >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='ID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		 EXEC ACI_ConvertToJson_JSON15_CLAIMSUBMISSIONEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) ='JSON16_MEDICATIONSORDER') AND len(@TABLEKEY) >0
	  BEGIN--ACI_TR_JSON16_MEDICATIONSORDER_MEDICATION
	    SET @FIELDKEY=''
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='CLINICALID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		
		if len(@FIELDKEY) >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON16_MEDICATIONSORDER @FIELDKEY,@ACIINPUTLOGKEY 
	  END

	  IF (UPPER(@TABLENAME) ='JSON17_SENTSUMMARYOFCAREEVENT') AND @TABLEKEY >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='ACIDEPKEY' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		 EXEC ACI_CONVERTTOJSON_JSON17_SENTSUMMARYOFCAREEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) ='JSON18_PATIENTSPECIFICEDUCATIONEVENT') AND @TABLEKEY >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='ID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		 EXEC ACI_ConvertToJson_JSON18_PATIENTSPECIFICEDUCATIONEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) ='JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT') AND len(@TABLEKEY) >0
	  BEGIN-- 
	    SET @FIELDKEY=''
	    set @ACIINPUTLOGKEY =0
		DECLARE @PII int
		DECLARE @SII int
		SET @PII = 0
		SET @SII = 0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='ID' AND TABLENAME = @TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY = @ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC

		SELECT @SII = PatientId from model.PatientReconcilePracticeInformation PRI where PRI.Id =@TABLEKEY
		
		SELECT @PII = defaultuserid from model.patients p where p.Id = @SII
		
		if len(@TABLEKEY) >0 and @ACIINPUTLOGKEY >0 AND @PII is not null and LTRIM(RTRIM(@PII)) <>''

       EXEC ACI_CONVERTTOJSON_JSON19_REFERRALTOCOROTHERNEWPATIENTRECEIVEDEVENT @TABLEKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) ='JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT') AND len(@TABLEKEY) >0
	  BEGIN--
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='RESPONSEID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		ORDER BY ACIINPUTLOGKEY DESC
		
		if @FIELDKEY >0 and @ACIINPUTLOGKEY >0 
		 EXEC ACI_ConvertToJson_JSON20_PATIENTHEALTHINFORMATIONPOSTEDEVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END


	 -- IF (UPPER(@TABLENAME) ='JSON21_CARECOORDINATIONORDER') AND @TABLEKEY >0
	 -- BEGIN
	 --   SET @FIELDKEY=0
  --       set @ACIINPUTLOGKEY =0
		--SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		--FROM ACI_INPUTLOG SS (NOLOCK) 
		--WHERE STATUS ='N' AND [FIELDNAME] ='ACIDEPKEY' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY and ACIINPUTLOGKEY =@ACIINPUTLOGKEY1 
		--ORDER BY ACIINPUTLOGKEY DESC
		
		--if @FIELDKEY >0 and @ACIINPUTLOGKEY >0
		-- EXEC ACI_CONVERTTOJSON_JSON21_CARECOORDINATIONORDER @FIELDKEY,@ACIINPUTLOGKEY
	 -- END

--REV1.1
 
	  IF (UPPER(@TABLENAME) ='JSON22_SPECIALISTREPORTRECEIVEDEVENT') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='CLINICALID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_CONVERTTOJSON_JSON22_SPECIALIST_REPORT_RECEIVED_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END
	  
	  IF (UPPER(@TABLENAME) ='JSON23_DRCOMMUNICATIONEVENT') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='ACIDEPKEY' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_CONVERTTOJSON_JSON23_DR_COMMUNICATION_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END



	  IF (UPPER(@TABLENAME) ='JSON24_SURGICALPREOPERATIVESTATUSEVENT') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='APPOINTMENTID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) ='JSON25_SURGERYEVENT') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='APPOINTMENTID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON25_SurgeryEvent @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) ='JSON26_SURGICALPOSTOPERATIVERESULTEVENT') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='APPOINTMENTID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) ='JSON27_POSTSURGICALCOMPLICATIONEVENT') AND @TABLEKEY >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='APPOINTMENTID' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON27_POST_SURGICAL_COMPLICATION_EVENT @FIELDKEY,@ACIINPUTLOGKEY
	  END

	  IF (UPPER(@TABLENAME) ='JSON28_RESULTS') AND Len(@TABLEKEY) >0
	  BEGIN
	    SET @FIELDKEY=0
	    set @ACIINPUTLOGKEY =0
		SELECT TOP 1 @FIELDKEY=SS.FIELDKEY,@ACIINPUTLOGKEY=ACIINPUTLOGKEY
		FROM ACI_INPUTLOG SS (NOLOCK) 
		WHERE STATUS ='N' AND [FIELDNAME] ='Id' AND TABLENAME  =@TABLENAME  AND TABLEKEY=@TABLEKEY
		ORDER BY ACIINPUTLOGKEY DESC
		
		IF @FIELDKEY >0 
		 EXEC ACI_ConvertToJson_JSON28_Results @FIELDKEY,@ACIINPUTLOGKEY

	  END

--REV1.1


	  if @ACIINPUTLOGKEY1 >0 	     
		insert into ACI_JSONConversionLog_END (ACIINPUTLOGKEY,TableKey,Sort,TempID) values(@ACIINPUTLOGKEY1,@TABLEKEY,@Sort,@newdt)


	  FETCH NEXT FROM CURSORA INTO @TABLENAME,@TABLEKEY,@Sort,@ACIINPUTLOGKEY1
	END
	CLOSE CURSORA
	DEALLOCATE CURSORA

 SET ARITHABORT OFF
END


Go