go
-------------------
IF EXISTS (SELECT * FROM   sys.objects WHERE  object_id = OBJECT_ID(N'[dbo].[getDecryptPassword]') AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' ))
    DROP FUNCTION [dbo].[getDecryptPassword]
GO 

Create Function getDecryptPassword(@encryptedPwd nvarchar(200)) RETURNS NVARCHAR(2000)
As
BEGIN
   return Reverse(@encryptedPwd)    
END


Go