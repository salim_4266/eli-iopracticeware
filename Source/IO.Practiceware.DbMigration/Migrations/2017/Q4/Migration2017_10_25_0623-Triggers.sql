/****** Object:  Trigger [model].[AuditPatientlanguagesDeleteTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditPatientlanguagesDeleteTrigger]'))
DROP TRIGGER [model].[AuditPatientlanguagesDeleteTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditPatientlanguagesDeleteTrigger]'))
EXEC dbo.sp_executesql @statement = N'CREATE  TRIGGER [model].[AuditPatientlanguagesDeleteTrigger] ON [model].[PatientLanguages] AFTER DELETE NOT FOR REPLICATION AS
BEGIN
					
SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''model.[PatientLanguages]'' AS ObjectName, 2 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName,
	''[LanguageId]'' AS KeyNames,CAST(''['' + CAST(LanguageId AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues,PatientId AS PatientId	,NULL AS AppointmentId
	INTO #__AuditEntries FROM (SELECT * FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, OldValue, NULL AS NewValue
			FROM
		
			(SELECT CAST(''['' + CAST(LanguageId AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 			 
			 (SELECT COALESCE(CAST([PatientId] AS nvarchar(max)), ''__NULL__'')) AS [PatientId],
			 (SELECT COALESCE(CAST(LanguageId AS nvarchar(max)), ''__NULL__'')) AS LanguageId

			FROM deleted) AS q	
			
			UNPIVOT (OldValue FOR ColumnName IN ( [PatientId],LanguageId)) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END' 
GO

/****** Object:  Trigger [model].[AuditPatientLanguagesInsertTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditPatientLanguagesInsertTrigger]'))
DROP TRIGGER [model].[AuditPatientLanguagesInsertTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditPatientLanguagesInsertTrigger]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [model].[AuditPatientLanguagesInsertTrigger] ON [model].[PatientLanguages] AFTER INSERT NOT FOR REPLICATION AS
BEGIN
					

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''model.PatientLanguages'' AS ObjectName, 0 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName,
	''[LanguageId]'' AS KeyNames,CAST(''['' + CAST(LanguageId AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues,PatientId AS PatientId,NULL AS AppointmentId
	INTO #__AuditEntries FROM (SELECT * FROM [inserted]) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, NULL AS OldValue, NewValue AS NewValue
			FROM
		
			(SELECT CAST(''['' + CAST(LanguageId AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 			 
			 (SELECT COALESCE(CAST([PatientId] AS nvarchar(max)), ''__NULL__'')) AS [PatientId],
			  (SELECT COALESCE(CAST(LanguageId AS nvarchar(max)), ''__NULL__'')) AS LanguageId
			FROM [inserted]) AS q

		
			UNPIVOT (NewValue FOR ColumnName IN ([PatientId],LanguageId )) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END' 
GO

/****** Object:  Trigger [model].[AuditPatientLanguagesUpdateTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditPatientLanguagesUpdateTrigger]'))
DROP TRIGGER [model].[AuditPatientLanguagesUpdateTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditPatientLanguagesUpdateTrigger]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [model].[AuditPatientLanguagesUpdateTrigger] ON [model].[PatientLanguages] AFTER UPDATE NOT FOR REPLICATION AS
BEGIN

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''model.[patientlanguages]'' AS ObjectName, 1 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName,
	''[PatientId], [LanguageId]'' AS KeyNames,CAST(''['' + CAST([PatientId] AS nvarchar(max)) + '']'' + '', ['' + CAST(LanguageId AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues,NULL AS PatientId,NULL AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT [PatientId], LanguageId FROM [inserted] EXCEPT SELECT [PatientId], LanguageId FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, InsertedColumnName AS ColumnName, OldValue, NewValue 
			FROM
		
			(SELECT CAST(''['' + CAST(inserted.[PatientId] AS nvarchar(max)) + '']'' + '', ['' + CAST(inserted.LanguageId AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues,
			 (COALESCE(CAST(inserted.[PatientId] AS nvarchar(max)), ''__NULL__'')) AS [PatientId],
			  (COALESCE(CAST(inserted.LanguageId AS nvarchar(max)), ''__NULL__'')) AS LanguageId,

			  (COALESCE(CAST(deleted.[PatientId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedPatientsId],
			   (COALESCE(CAST(deleted.LanguageId AS nvarchar(max)), ''__NULL__'')) AS DeletedLanguageId

			FROM (SELECT [PatientId], LanguageId FROM [inserted] EXCEPT SELECT [PatientId], LanguageId FROM deleted) AS inserted
			JOIN deleted ON inserted.[PatientId] = deleted.[PatientId] AND inserted.LanguageId = deleted.LanguageId) AS insertedJoinedWithDeleted
		
			UNPIVOT (NewValue FOR InsertedColumnName IN ([PatientId], LanguageId)) AS unpivotNewValue
			UNPIVOT (OldValue FOR DeletedColumnName IN ([DeletedPatientsId], DeletedLanguageId)) AS unpivotOldValue

			WHERE ''Deleted''  + InsertedColumnName = DeletedColumnName
				AND ((OldValue IS NULL AND NewValue IS NOT NULL) OR (NewValue IS NOT NULL AND OldValue IS NULL) OR OldValue <> NewValue)
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END' 
GO

/****** Object:  Trigger  [model].[AuditPatientRaceDeleteTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditPatientRaceDeleteTrigger]'))
DROP TRIGGER [model].[AuditPatientRaceDeleteTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditPatientRaceDeleteTrigger]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [model].[AuditPatientRaceDeleteTrigger] ON [model].[PatientRace] AFTER DELETE NOT FOR REPLICATION AS
BEGIN
					
SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''model.PatientRace'' AS ObjectName, 2 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName,
	''[Patients_Id], [Races_Id]'' AS KeyNames,CAST(''['' + CAST([Patients_Id] AS nvarchar(max)) + '']'' + '', ['' + CAST([Races_Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues,Patients_Id AS PatientId,NULL AS AppointmentId
	INTO #__AuditEntries FROM (SELECT * FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, OldValue, NULL AS NewValue
			FROM
		
			(SELECT CAST(''['' + CAST([Patients_Id] AS nvarchar(max)) + '']'' + '', ['' + CAST([Races_Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, (SELECT COALESCE(CAST([Patients_Id] AS nvarchar(max)), ''__NULL__'')) AS [Patients_Id], (SELECT COALESCE(CAST([Races_Id] AS nvarchar(max)), ''__NULL__'')) AS [Races_Id]
			FROM deleted) AS q
		
			UNPIVOT (OldValue FOR ColumnName IN ([Patients_Id], [Races_Id])) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END' 
GO

/****** Object:  Trigger [model].[AuditPatientRaceInsertTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditPatientRaceInsertTrigger]'))
DROP TRIGGER [model].[AuditPatientRaceInsertTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditPatientRaceInsertTrigger]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [model].[AuditPatientRaceInsertTrigger] ON [model].[PatientRace] AFTER INSERT NOT FOR REPLICATION AS
BEGIN

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''model.PatientRace'' AS ObjectName, 0 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName,
	''[Patients_Id], [Races_Id]'' AS KeyNames,CAST(''['' + CAST([Patients_Id] AS nvarchar(max)) + '']'' + '', ['' + CAST([Races_Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues,Patients_Id AS PatientId,NULL AS AppointmentId
	INTO #__AuditEntries FROM (SELECT * FROM [inserted]) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, NULL AS OldValue, NewValue AS NewValue
			FROM
		
			(SELECT CAST(''['' + CAST([Patients_Id] AS nvarchar(max)) + '']'' + '', ['' + CAST([Races_Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, (SELECT COALESCE(CAST([Patients_Id] AS nvarchar(max)), ''__NULL__'')) AS [Patients_Id], (SELECT COALESCE(CAST([Races_Id] AS nvarchar(max)), ''__NULL__'')) AS [Races_Id]
			FROM [inserted]) AS q
		
			UNPIVOT (NewValue FOR ColumnName IN ([Patients_Id], [Races_Id])) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END' 
GO

/****** Object:  Trigger [model].[AuditPatientRaceUpdateTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditPatientRaceUpdateTrigger]'))
DROP TRIGGER [model].[AuditPatientRaceUpdateTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditPatientRaceUpdateTrigger]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [model].[AuditPatientRaceUpdateTrigger] ON [model].[PatientRace] AFTER UPDATE NOT FOR REPLICATION AS
BEGIN
					
SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''model.PatientRace'' AS ObjectName, 1 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName,
	''[Patients_Id], [Races_Id]'' AS KeyNames,CAST(''['' + CAST([Patients_Id] AS nvarchar(max)) + '']'' + '', ['' + CAST([Races_Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues,Patients_Id AS PatientId,NULL AS AppointmentId
	INTO #__AuditEntries FROM (SELECT [Patients_Id], [Races_Id] FROM [inserted] EXCEPT SELECT [Patients_Id], [Races_Id] FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, InsertedColumnName AS ColumnName, OldValue, NewValue 
			FROM
		
			(SELECT CAST(''['' + CAST(inserted.[Patients_Id] AS nvarchar(max)) + '']'' + '', ['' + CAST(inserted.[Races_Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, (COALESCE(CAST(inserted.[Patients_Id] AS nvarchar(max)), ''__NULL__'')) AS [Patients_Id], (COALESCE(CAST(inserted.[Races_Id] AS nvarchar(max)), ''__NULL__'')) AS [Races_Id],(COALESCE(CAST(deleted.[Patients_Id] AS nvarchar(max)), ''__NULL__'')) AS [DeletedPatients_Id], (COALESCE(CAST(deleted.[Races_Id] AS nvarchar(max)), ''__NULL__'')) AS [DeletedRaces_Id]
			FROM (SELECT [Patients_Id], [Races_Id] FROM [inserted] EXCEPT SELECT [Patients_Id], [Races_Id] FROM deleted) AS inserted
			JOIN deleted ON inserted.[Patients_Id] = deleted.[Patients_Id] AND inserted.[Races_Id] = deleted.[Races_Id]) AS insertedJoinedWithDeleted
		
			UNPIVOT (NewValue FOR InsertedColumnName IN ([Patients_Id], [Races_Id])) AS unpivotNewValue
			UNPIVOT (OldValue FOR DeletedColumnName IN ([DeletedPatients_Id], [DeletedRaces_Id])) AS unpivotOldValue

			WHERE ''Deleted''  + InsertedColumnName = DeletedColumnName
				AND ((OldValue IS NULL AND NewValue IS NOT NULL) OR (NewValue IS NOT NULL AND OldValue IS NULL) OR OldValue <> NewValue)
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END' 
GO

/****** Object:  Trigger [dbo].[AuditImplantableDevicesDeleteTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AuditImplantableDevicesDeleteTrigger]'))
DROP TRIGGER [dbo].[AuditImplantableDevicesDeleteTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AuditImplantableDevicesDeleteTrigger]'))
EXEC dbo.sp_executesql @statement = N'
create  TRIGGER [dbo].[AuditImplantableDevicesDeleteTrigger] ON [dbo].[ImplantableDevices] AFTER DELETE NOT FOR REPLICATION AS
BEGIN
					
SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN

	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''Dbo.ImplantableDevices'' AS ObjectName, 2 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName,''[id]'' AS KeyNames
	,CAST(''['' + CAST(Id AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues,NULL AS PatientId,NULL AS AppointmentId INTO #__AuditEntries 
	FROM (SELECT * FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, OldValue, NULL AS NewValue
			FROM
		
				(SELECT CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 
			(SELECT COALESCE(CAST([UniqueDeviceIdentifier] AS nvarchar(max)), ''__NULL__'')) AS [UniqueDeviceIdentifier],
			(SELECT COALESCE(CAST([IssuingAgency] AS nvarchar(max)), ''__NULL__'')) AS [IssuingAgency], 
			(SELECT COALESCE(CAST([DeviceId] AS nvarchar(max)), ''__NULL__'')) AS [DeviceId], 
			(SELECT COALESCE(CAST([DeviceDescription] AS nvarchar(max)), ''__NULL__'')) AS [DeviceDescription],
			(SELECT COALESCE(CAST([ManufacturingDate] AS nvarchar(max)), ''__NULL__'')) AS [ManufacturingDate],
			(SELECT COALESCE(CAST([ExpirtaionDate] AS nvarchar(max)), ''__NULL__'')) AS [ExpirtaionDate], 	
			(SELECT COALESCE(CAST([LotNumber] AS nvarchar(max)), ''__NULL__'')) AS [LotNumber], 
			(SELECT COALESCE(CAST([SerialNumber] AS nvarchar(max)), ''__NULL__'')) AS [SerialNumber], 		
			(SELECT COALESCE(CAST([BrandName] AS nvarchar(max)), ''__NULL__'')) AS [BrandName], 
			(SELECT COALESCE(CAST([CompanyName] AS nvarchar(max)), ''__NULL__'')) AS [CompanyName], 
			(SELECT COALESCE(CAST([GMDNPTName] AS nvarchar(max)), ''__NULL__'')) AS [GMDNPTName],	
			(SELECT COALESCE(CAST([GmdnPTDefinition] AS nvarchar(max)), ''__NULL__'')) AS [GmdnPTDefinition],
			(SELECT COALESCE(CAST([MRISafetyInformation] AS nvarchar(max)), ''__NULL__'')) AS [MRISafetyInformation], 	
			(SELECT COALESCE(CAST([VersionOrModel] AS nvarchar(max)), ''__NULL__'')) AS [VersionOrModel], 
			(SELECT COALESCE(CAST([RequiresRubberLabelingTypeId] AS nvarchar(max)), ''__NULL__'')) AS [RequiresRubberLabelingTypeId], 		
			(SELECT COALESCE(CAST([HCTPStatus] AS nvarchar(max)), ''__NULL__'')) AS [HCTPStatus], 
			(SELECT COALESCE(CAST([HCTPCode] AS nvarchar(max)), ''__NULL__'')) AS [HCTPCode]		
			FROM deleted) AS q	
			
			UNPIVOT (OldValue FOR ColumnName IN ([UniqueDeviceIdentifier], [IssuingAgency], [DeviceId],[DeviceDescription], [ManufacturingDate], [ExpirtaionDate], [LotNumber], [SerialNumber],[BrandName] , [CompanyName],[GMDNPTName],[GmdnPTDefinition],[MRISafetyInformation],[VersionOrModel],[RequiresRubberLabelingTypeId],[HCTPStatus],[HCTPCode])) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END
' 
GO

/****** Object:  Trigger [dbo].[AuditImplantableDevicesInsertTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AuditImplantableDevicesInsertTrigger]'))
DROP TRIGGER [dbo].[AuditImplantableDevicesInsertTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AuditImplantableDevicesInsertTrigger]'))
EXEC dbo.sp_executesql @statement = N'
create TRIGGER [dbo].[AuditImplantableDevicesInsertTrigger] ON [dbo].[ImplantableDevices] AFTER INSERT NOT FOR REPLICATION AS
BEGIN
					
SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''dbo.ImplantableDevices'' AS ObjectName, 0 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName,''[id]'' AS KeyNames
	,CAST(''['' + CAST(id AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues,NULL AS PatientId,NULL AS AppointmentId	INTO #__AuditEntries 
	FROM (SELECT * FROM [inserted]) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, NULL AS OldValue, NewValue AS NewValue
			FROM
		
			(SELECT CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 
			(SELECT COALESCE(CAST([UniqueDeviceIdentifier] AS nvarchar(max)), ''__NULL__'')) AS [UniqueDeviceIdentifier],
			(SELECT COALESCE(CAST([IssuingAgency] AS nvarchar(max)), ''__NULL__'')) AS [IssuingAgency], 
			(SELECT COALESCE(CAST([DeviceId] AS nvarchar(max)), ''__NULL__'')) AS [DeviceId], 
			(SELECT COALESCE(CAST([DeviceDescription] AS nvarchar(max)), ''__NULL__'')) AS [DeviceDescription],
			(SELECT COALESCE(CAST([ManufacturingDate] AS nvarchar(max)), ''__NULL__'')) AS [ManufacturingDate],
			(SELECT COALESCE(CAST([ExpirtaionDate] AS nvarchar(max)), ''__NULL__'')) AS [ExpirtaionDate], 	
			(SELECT COALESCE(CAST([LotNumber] AS nvarchar(max)), ''__NULL__'')) AS [LotNumber], 
			(SELECT COALESCE(CAST([SerialNumber] AS nvarchar(max)), ''__NULL__'')) AS [SerialNumber], 		
			(SELECT COALESCE(CAST([BrandName] AS nvarchar(max)), ''__NULL__'')) AS [BrandName], 
			(SELECT COALESCE(CAST([CompanyName] AS nvarchar(max)), ''__NULL__'')) AS [CompanyName], 
			(SELECT COALESCE(CAST([GMDNPTName] AS nvarchar(max)), ''__NULL__'')) AS [GMDNPTName],	
			(SELECT COALESCE(CAST([GmdnPTDefinition] AS nvarchar(max)), ''__NULL__'')) AS [GmdnPTDefinition],
			(SELECT COALESCE(CAST([MRISafetyInformation] AS nvarchar(max)), ''__NULL__'')) AS [MRISafetyInformation], 	
			(SELECT COALESCE(CAST([VersionOrModel] AS nvarchar(max)), ''__NULL__'')) AS [VersionOrModel], 
			(SELECT COALESCE(CAST([RequiresRubberLabelingTypeId] AS nvarchar(max)), ''__NULL__'')) AS [RequiresRubberLabelingTypeId], 		
			(SELECT COALESCE(CAST([HCTPStatus] AS nvarchar(max)), ''__NULL__'')) AS [HCTPStatus], 
			(SELECT COALESCE(CAST([HCTPCode] AS nvarchar(max)), ''__NULL__'')) AS [HCTPCode]	
			FROM [inserted]) AS q
		
			UNPIVOT (NewValue FOR ColumnName IN ([UniqueDeviceIdentifier], [IssuingAgency], [DeviceId],[DeviceDescription], [ManufacturingDate], [ExpirtaionDate], [LotNumber], [SerialNumber],[BrandName] , [CompanyName],[GMDNPTName],[GmdnPTDefinition],[MRISafetyInformation],[VersionOrModel],[RequiresRubberLabelingTypeId],[HCTPStatus],[HCTPCode])) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END

SET NOCOUNT OFF END '
GO

/****** Object:  Trigger [dbo].[AuditImplantableDevicesUpdateTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AuditImplantableDevicesUpdateTrigger]'))
DROP TRIGGER [dbo].[AuditImplantableDevicesUpdateTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AuditImplantableDevicesUpdateTrigger]'))
EXEC dbo.sp_executesql @statement = N'

CREATE TRIGGER [dbo].[AuditImplantableDevicesUpdateTrigger] ON  [dbo].[ImplantableDevices] AFTER UPDATE NOT FOR REPLICATION AS
BEGIN			

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''Dbo.ImplantableDevices'' AS ObjectName, 1 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName,''[Id]'' AS KeyNames
	,CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues,NULL AS PatientId,NULL AS AppointmentId INTO #__AuditEntries 
	FROM (SELECT ID,[UniqueDeviceIdentifier], [IssuingAgency], [DeviceId],[DeviceDescription], [ManufacturingDate], [ExpirtaionDate], [LotNumber], [SerialNumber],[BrandName] , [CompanyName],[GMDNPTName],[GmdnPTDefinition],[MRISafetyInformation],[VersionOrModel],[RequiresRubberLabelingTypeId],[HCTPStatus],[HCTPCode] FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, InsertedColumnName AS ColumnName, OldValue, NewValue 
			FROM
		
		(SELECT CAST(''['' + CAST(inserted.[Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 
			(COALESCE(CAST(inserted.[UniqueDeviceIdentifier] AS nvarchar(max)), ''__NULL__'')) AS [UniqueDeviceIdentifier],
			(COALESCE(CAST(inserted.[IssuingAgency] AS nvarchar(max)), ''__NULL__'')) AS [IssuingAgency], 
			(COALESCE(CAST(inserted.[DeviceId] AS nvarchar(max)), ''__NULL__'')) AS [DeviceId], 
			(COALESCE(CAST(inserted.[DeviceDescription] AS nvarchar(max)), ''__NULL__'')) AS [DeviceDescription],
			(COALESCE(CAST(inserted.[ManufacturingDate] AS nvarchar(max)), ''__NULL__'')) AS [ManufacturingDate],
			(COALESCE(CAST(inserted.[ExpirtaionDate] AS nvarchar(max)), ''__NULL__'')) AS [ExpirtaionDate], 	
			(COALESCE(CAST(inserted.[LotNumber] AS nvarchar(max)), ''__NULL__'')) AS [LotNumber], 
			(COALESCE(CAST(inserted.[SerialNumber] AS nvarchar(max)), ''__NULL__'')) AS [SerialNumber], 		
			(COALESCE(CAST(inserted.[BrandName] AS nvarchar(max)), ''__NULL__'')) AS [BrandName], 
			(COALESCE(CAST(inserted.[CompanyName] AS nvarchar(max)), ''__NULL__'')) AS [CompanyName], 
			(COALESCE(CAST(inserted.[GMDNPTName] AS nvarchar(max)), ''__NULL__'')) AS [GMDNPTName],
			(COALESCE(CAST(inserted.[GmdnPTDefinition] AS nvarchar(max)), ''__NULL__'')) AS [GmdnPTDefinition], 	
			(COALESCE(CAST(inserted.[MRISafetyInformation] AS nvarchar(max)), ''__NULL__'')) AS [MRISafetyInformation], 
			(COALESCE(CAST(inserted.[VersionOrModel] AS nvarchar(max)), ''__NULL__'')) AS [VersionOrModel], 		
			(COALESCE(CAST(inserted.[RequiresRubberLabelingTypeId] AS nvarchar(max)), ''__NULL__'')) AS [RequiresRubberLabelingTypeId], 
			(COALESCE(CAST(inserted.[HCTPStatus] AS nvarchar(max)), ''__NULL__'')) AS [HCTPStatus], 
			(COALESCE(CAST(inserted.[HCTPCode] AS nvarchar(max)), ''__NULL__'')) AS [HCTPCode],


			(COALESCE(CAST(deleted.[UniqueDeviceIdentifier] AS nvarchar(max)), ''__NULL__'')) AS [DeletedUniqueDeviceIdentifier],
			(COALESCE(CAST(deleted.[IssuingAgency] AS nvarchar(max)), ''__NULL__'')) AS [DeletedIssuingAgency], 
			(COALESCE(CAST(deleted.[DeviceId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedDeviceId], 
			(COALESCE(CAST(deleted.[DeviceDescription] AS nvarchar(max)), ''__NULL__'')) AS [DeletedDeviceDescription],
			(COALESCE(CAST(deleted.[ManufacturingDate] AS nvarchar(max)), ''__NULL__'')) AS [DeletedManufacturingDate],
			(COALESCE(CAST(deleted.[ExpirtaionDate] AS nvarchar(max)), ''__NULL__'')) AS [DeletedExpirtaionDate], 	
			(COALESCE(CAST(deleted.[LotNumber] AS nvarchar(max)), ''__NULL__'')) AS [DeletedLotNumber], 
			(COALESCE(CAST(deleted.[SerialNumber] AS nvarchar(max)), ''__NULL__'')) AS [DeletedSerialNumber], 		
			(COALESCE(CAST(deleted.[BrandName] AS nvarchar(max)), ''__NULL__'')) AS [DeletedBrandName], 
			(COALESCE(CAST(deleted.[CompanyName] AS nvarchar(max)), ''__NULL__'')) AS [DeletedCompanyName], 
			(COALESCE(CAST(deleted.[GMDNPTName] AS nvarchar(max)), ''__NULL__'')) AS [DeletedGMDNPTName],
			(COALESCE(CAST(deleted.[GmdnPTDefinition] AS nvarchar(max)), ''__NULL__'')) AS [DeletedGmdnPTDefinition], 	
			(COALESCE(CAST(deleted.[MRISafetyInformation] AS nvarchar(max)), ''__NULL__'')) AS [DeletedMRISafetyInformation], 
			(COALESCE(CAST(deleted.[VersionOrModel] AS nvarchar(max)), ''__NULL__'')) AS [DeletedVersionOrModel], 		
			(COALESCE(CAST(deleted.[RequiresRubberLabelingTypeId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedRequiresRubberLabelingTypeId], 
			(COALESCE(CAST(deleted.[HCTPStatus] AS nvarchar(max)), ''__NULL__'')) AS [DeletedHCTPStatus], 
			(COALESCE(CAST(deleted.[HCTPCode] AS nvarchar(max)), ''__NULL__'')) AS [DeletedHCTPCode]

			FROM (
			SELECT Id,[UniqueDeviceIdentifier], [IssuingAgency], [DeviceId],[DeviceDescription], [ManufacturingDate], [ExpirtaionDate], [LotNumber], [SerialNumber],[BrandName] , [CompanyName],[GMDNPTName],[GmdnPTDefinition],[MRISafetyInformation],[VersionOrModel],[RequiresRubberLabelingTypeId],[HCTPStatus],[HCTPCode]
			FROM [inserted]
			EXCEPT 
			SELECT Id,[UniqueDeviceIdentifier], [IssuingAgency], [DeviceId],[DeviceDescription], [ManufacturingDate], [ExpirtaionDate], [LotNumber], [SerialNumber],[BrandName] , [CompanyName],[GMDNPTName],[GmdnPTDefinition],[MRISafetyInformation],[VersionOrModel],[RequiresRubberLabelingTypeId],[HCTPStatus],[HCTPCode]
			FROM deleted) AS inserted
			JOIN deleted ON inserted.Id = deleted.Id) AS insertedJoinedWithDeleted
		
			UNPIVOT (NewValue FOR InsertedColumnName IN ([UniqueDeviceIdentifier], [IssuingAgency], [DeviceId],[DeviceDescription], [ManufacturingDate], [ExpirtaionDate], [LotNumber], [SerialNumber],[BrandName] , [CompanyName],[GMDNPTName],[GmdnPTDefinition],[MRISafetyInformation],[VersionOrModel],[RequiresRubberLabelingTypeId],[HCTPStatus],[HCTPCode])) AS unpivotNewValue
			UNPIVOT (OldValue FOR DeletedColumnName IN ([DeletedUniqueDeviceIdentifier], [DeletedIssuingAgency], [DeletedDeviceId],[DeletedDeviceDescription], [DeletedManufacturingDate], [DeletedExpirtaionDate], [DeletedLotNumber], [DeletedSerialNumber],[DeletedBrandName] , [DeletedCompanyName],[DeletedGMDNPTName],[DeletedGmdnPTDefinition],[DeletedMRISafetyInformation],[DeletedVersionOrModel],[DeletedRequiresRubberLabelingTypeId],[DeletedHCTPStatus],[DeletedHCTPCode])) AS unpivotOldValue

			WHERE ''Deleted''  + InsertedColumnName = DeletedColumnName
				AND ((OldValue IS NULL AND NewValue IS NOT NULL) OR (NewValue IS NOT NULL AND OldValue IS NULL) OR OldValue <> NewValue)
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF END'
GO

/****** Object:  Trigger [model].[AuditFamilyHistoryDeleteTrigger]  ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditFamilyHistoryDeleteTrigger]'))
DROP TRIGGER [model].[AuditFamilyHistoryDeleteTrigger] 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditFamilyHistoryDeleteTrigger]'))
EXEC dbo.sp_executesql @statement = N'

CREATE   TRIGGER [model].[AuditFamilyHistoryDeleteTrigger] ON [model].[FamilyHistory] AFTER DELETE NOT FOR REPLICATION AS
BEGIN			

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN

	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''Model.familyhistory'' AS ObjectName, 2 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName
	,''[id]'' AS KeyNames
	,CAST(''['' + CAST(Id AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues
	,PatientId AS PatientId
	,AppointmentId AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT * FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__''THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__''THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, OldValue, NULL AS NewValue
			FROM
		
				(SELECT CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 
			(SELECT COALESCE(CAST([AppointmentId] AS nvarchar(max)), ''__NULL__'')) AS [AppointmentId],
			(SELECT COALESCE(CAST([ConceptId] AS nvarchar(max)), ''__NULL__'')) AS [ConceptId], 
			(SELECT COALESCE(CAST([Term] AS nvarchar(max)), ''__NULL__'')) AS [Term], 
			(SELECT COALESCE(CAST([ICd10] AS nvarchar(max)), ''__NULL__'')) AS [ICd10],
			(SELECT COALESCE(CAST([ICD10Descr] AS nvarchar(max)), ''__NULL__'')) AS [ICD10Descr],
			(SELECT COALESCE(CAST([Comments] AS nvarchar(max)), ''__NULL__'')) AS [Comments], 	
			(SELECT COALESCE(CAST([Relationship] AS nvarchar(max)), ''__NULL__'')) AS [Relationship], 			
			(SELECT COALESCE(CAST(CASE WHEN [Status] =0 THEN ''inactive'' ELSE  ''Active'' END AS nvarchar(max)), ''__NULL__'')) AS [Status], 
			(SELECT COALESCE(CAST([PatientId] AS nvarchar(max)), ''__NULL__'')) AS [PatientId], 
			(SELECT COALESCE(CAST([lastmodofiedby] AS nvarchar(max)), ''__NULL__'')) AS [lastmodofiedby]			
			FROM deleted) AS q	
			
			UNPIVOT (OldValue FOR ColumnName IN ([AppointmentId], [ConceptId], [Term],[ICd10], [ICD10Descr], [Comments], [Relationship],[Status] , [PatientId],[lastmodofiedby])) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END'
GO

/****** Object:  Trigger [model].[AuditFamilyHistoryInsertTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditFamilyHistoryInsertTrigger]'))
DROP TRIGGER [model].[AuditFamilyHistoryInsertTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditFamilyHistoryInsertTrigger]'))
EXEC dbo.sp_executesql @statement = N'

CREATE TRIGGER [model].[AuditFamilyHistoryInsertTrigger] ON [model].[FamilyHistory] AFTER INSERT NOT FOR REPLICATION AS
BEGIN
					

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''Model.familyhistory'' AS ObjectName, 0 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName
	,''[id]'' AS KeyNames
	,CAST(''['' + CAST(id AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues
	,PatientId AS PatientId
	,AppointmentId AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT * FROM [inserted]) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, NULL AS OldValue, NewValue AS NewValue
			FROM
		
			(SELECT CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 
			(SELECT COALESCE(CAST([AppointmentId] AS nvarchar(max)), ''__NULL__'')) AS [AppointmentId],
			(SELECT COALESCE(CAST([ConceptId] AS nvarchar(max)), ''__NULL__'')) AS [ConceptId], 
			(SELECT COALESCE(CAST([Term] AS nvarchar(max)), ''__NULL__'')) AS [Term], 
			(SELECT COALESCE(CAST([ICd10] AS nvarchar(max)), ''__NULL__'')) AS [ICd10],
			(SELECT COALESCE(CAST([ICD10Descr] AS nvarchar(max)), ''__NULL__'')) AS [ICD10Descr],
			(SELECT COALESCE(CAST(Relationship AS nvarchar(max)), ''__NULL__'')) AS [RelationShip], 	
			(SELECT COALESCE(CAST([OnSetDate] AS nvarchar(max)), ''__NULL__'')) AS [OnSetDate], 			
			(SELECT COALESCE(CAST(CASE WHEN [Status] =0 THEN ''inactive'' ELSE  ''Active'' END AS nvarchar(max)), ''__NULL__'')) AS [Status], 
			(SELECT COALESCE(CAST([PatientId] AS nvarchar(max)), ''__NULL__'')) AS [PatientId], 
			(SELECT COALESCE(CAST(lastmodofiedby AS nvarchar(max)), ''__NULL__'')) AS lastmodofiedby
			FROM [inserted]) AS q
		
			UNPIVOT (NewValue FOR ColumnName IN ([AppointmentId], [ConceptId], [Term],[ICd10], [ICD10Descr], [RelationShip], [ONsetDate], [Status] , [PatientId],lastmodofiedby)) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END'
GO

/****** Object:  Trigger [model].[AuditfamilyhistoryUpdateTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditfamilyhistoryUpdateTrigger]'))
DROP TRIGGER [model].[AuditfamilyhistoryUpdateTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditfamilyhistoryUpdateTrigger]'))
EXEC dbo.sp_executesql @statement = N'

CREATE  TRIGGER [model].[AuditfamilyhistoryUpdateTrigger] ON  [model].[FamilyHistory] AFTER UPDATE NOT FOR REPLICATION AS
BEGIN
					

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''Model.familyhistory'' AS ObjectName, 1 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName
	,''[Id]'' AS KeyNames
	,CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues
	,PatientId AS PatientId
	,AppointmentId AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT ID,[AppointmentId], [ConceptId], [Term],[ICd10], [ICD10Descr], [Comments], [Relationship],[Status] , [PatientId],[lastmodofiedby] FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, InsertedColumnName AS ColumnName, OldValue, NewValue 
			FROM
		
		(SELECT CAST(''['' + CAST([inserted].[Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 
			(SELECT COALESCE(CAST([inserted].[AppointmentId] AS nvarchar(max)), ''__NULL__'')) AS [AppointmentId],
			(SELECT COALESCE(CAST([inserted].[ConceptId] AS nvarchar(max)), ''__NULL__'')) AS [ConceptId], 
			(SELECT COALESCE(CAST([inserted].[Term] AS nvarchar(max)), ''__NULL__'')) AS [Term], 
			(SELECT COALESCE(CAST([inserted].[ICd10] AS nvarchar(max)), ''__NULL__'')) AS [ICd10],
			(SELECT COALESCE(CAST([inserted].[ICD10Descr] AS nvarchar(max)), ''__NULL__'')) AS [ICD10Descr],
			(SELECT COALESCE(CAST([inserted].[Comments] AS nvarchar(max)), ''__NULL__'')) AS [Comments], 	
			(SELECT COALESCE(CAST([inserted].[Relationship] AS nvarchar(max)), ''__NULL__'')) AS [Relationship], 			
			(SELECT COALESCE(CAST( CASE WHEN [inserted].[Status]=0 THEN ''Inactive'' ELSE ''Active'' END  AS nvarchar(max)), ''__NULL__'')) AS [Status], 
			(SELECT COALESCE(CAST([inserted].[PatientId] AS nvarchar(max)), ''__NULL__'')) AS [PatientId], 
			(SELECT COALESCE(CAST([inserted].[lastmodofiedby] AS nvarchar(max)), ''__NULL__'')) AS [lastmodofiedby],		

			(SELECT COALESCE(CAST(deleted.[AppointmentId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedAppointmentId],
			(SELECT COALESCE(CAST(deleted.[ConceptId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedConceptId], 
			(SELECT COALESCE(CAST(deleted.[Term] AS nvarchar(max)), ''__NULL__'')) AS [DeletedTerm], 
			(SELECT COALESCE(CAST(deleted.[ICd10] AS nvarchar(max)),''__NULL__'')) AS [DeletedICd10],
			(SELECT COALESCE(CAST(deleted.[ICD10Descr] AS nvarchar(max)), ''__NULL__'')) AS [DeletedICD10Descr],
			(SELECT COALESCE(CAST(deleted.[Comments] AS nvarchar(max)), ''__NULL__'')) AS [DeletedComments], 	
			(SELECT COALESCE(CAST(deleted.[Relationship] AS nvarchar(max)), ''__NULL__'')) AS [DeletedRelationship], 			
			(SELECT COALESCE(CAST(CASE WHEN deleted.[Status]=0 THEN ''Inactive'' ELSE ''Active'' END AS nvarchar(max)), ''__NULL__'')) AS [DeletedStatus], 
			(SELECT COALESCE(CAST(deleted.[PatientId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedPatientId], 
			(SELECT COALESCE(CAST(deleted.[lastmodofiedby] AS nvarchar(max)),''__NULL__'')) AS [DeletedLastmodofiedby]
			--(SELECT COALESCE(CAST(deleted.ID AS nvarchar(max)), ''__NULL__'')) AS [DeletedId]
					
			FROM (
			SELECT Id,[AppointmentId], [ConceptId], [Term],[ICd10], [ICD10Descr], [Comments], [Relationship],  [Status], [PatientId],[lastmodofiedby]
			FROM [inserted]
			EXCEPT 
			SELECT Id,[AppointmentId], [ConceptId], [Term],[ICd10], [ICD10Descr], [Comments], [Relationship], [Status] , [PatientId],[lastmodofiedby]
			FROM deleted) AS inserted
			JOIN deleted ON inserted.Id = deleted.Id) AS insertedJoinedWithDeleted
		
			UNPIVOT (NewValue FOR InsertedColumnName IN ([AppointmentId], [ConceptId], [Term],[ICd10], [ICD10Descr], [Comments], [Relationship], [Status] , [PatientId],[lastmodofiedby])) AS unpivotNewValue
			UNPIVOT (OldValue FOR DeletedColumnName IN ([DeletedAppointmentId], [DeletedConceptId], [DeletedTerm], [DeletedICd10], [DeletedICD10Descr], [DeletedComments],[DeletedRelationship], [DeletedStatus],[DeletedPatientId], [DeletedLastmodofiedby])) AS unpivotOldValue

			WHERE ''Deleted''  + InsertedColumnName = DeletedColumnName
				AND ((OldValue IS NULL AND NewValue IS NOT NULL) OR (NewValue IS NOT NULL AND OldValue IS NULL) OR OldValue <> NewValue)
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END'
GO


/****** Object:  Trigger [dbo].[IE_UPDATE] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[IE_UPDATE]'))
DROP TRIGGER [dbo].[IE_UPDATE]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[IE_UPDATE]'))
EXEC dbo.sp_executesql @statement = N'

CREATE TRIGGER [dbo].[IE_UPDATE]  ON [dbo].[IE_Rules]
AFTER UPDATE AS
BEGIN
	Insert into dbo.IE_RulesHistory(RuleId, ModifyBy, ModifiedDate) Select i.RuleId, ISNULL(i.ModifyBy, i.CreateBy), isnull(i.ModifyDt,i.CreateDt) From inserted i
END'
GO

/****** Object:  Trigger [model].[AuditEthnicitiesDeleteTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditEthnicitiesDeleteTrigger]'))
DROP TRIGGER [model].[AuditEthnicitiesDeleteTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditEthnicitiesDeleteTrigger]'))
EXEC dbo.sp_executesql @statement = N'

CREATE TRIGGER [model].[AuditEthnicitiesDeleteTrigger] ON [model].[Ethnicities] AFTER DELETE NOT FOR REPLICATION AS
BEGIN
					
SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''model.Ethnicities'' AS ObjectName, 2 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName
	,''[Id]'' AS KeyNames
	,CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues
	,NULL AS PatientId
	,NULL AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT * FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, OldValue, NULL AS NewValue
			FROM
		
			(SELECT CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, (SELECT COALESCE(CAST([Id] AS nvarchar(max)), ''__NULL__'')) AS [Id], (SELECT COALESCE(CAST([IsArchived] AS nvarchar(max)), ''__NULL__'')) AS [IsArchived], (SELECT COALESCE(CAST([Name] AS nvarchar(max)), ''__NULL__'')) AS [Name], (SELECT COALESCE(CAST([OrdinalId] AS nvarchar(max)), ''__NULL__'')) AS [OrdinalId]
			FROM deleted) AS q
		
			UNPIVOT (OldValue FOR ColumnName IN ([Id], [IsArchived], [Name], [OrdinalId])) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF
END'
GO
/****** Object:  Trigger [model].[AuditEthnicitiesInsertTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditEthnicitiesInsertTrigger]'))
DROP TRIGGER [model].[AuditEthnicitiesInsertTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditEthnicitiesInsertTrigger]'))
EXEC dbo.sp_executesql @statement = N'

CREATE TRIGGER [model].[AuditEthnicitiesInsertTrigger] ON [model].[Ethnicities] AFTER INSERT NOT FOR REPLICATION AS
BEGIN
					

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''model.Ethnicities'' AS ObjectName, 0 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName
	,''[Id]'' AS KeyNames
	,CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues
	,NULL AS PatientId
	,NULL AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT * FROM [inserted]) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, NULL AS OldValue, NewValue AS NewValue
			FROM
		
			(SELECT CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, (SELECT COALESCE(CAST([Id] AS nvarchar(max)), ''__NULL__'')) AS [Id], (SELECT COALESCE(CAST([IsArchived] AS nvarchar(max)), ''__NULL__'')) AS [IsArchived], (SELECT COALESCE(CAST([Name] AS nvarchar(max)), ''__NULL__'')) AS [Name], (SELECT COALESCE(CAST([OrdinalId] AS nvarchar(max)), ''__NULL__'')) AS [OrdinalId]
			FROM [inserted]) AS q
		
			UNPIVOT (NewValue FOR ColumnName IN ([Id], [IsArchived], [Name], [OrdinalId])) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END'
GO

/****** Object:  Trigger [model].[AuditEthnicitiesUpdateTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditEthnicitiesUpdateTrigger]'))
DROP TRIGGER [model].[AuditEthnicitiesUpdateTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditEthnicitiesUpdateTrigger]'))
EXEC dbo.sp_executesql @statement = N'

CREATE TRIGGER [model].[AuditEthnicitiesUpdateTrigger] ON [model].[Ethnicities] AFTER UPDATE NOT FOR REPLICATION AS
					BEGIN
					

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''model.Ethnicities'' AS ObjectName, 1 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName
	,''[Id]'' AS KeyNames
	,CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues
	,NULL AS PatientId
	,NULL AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT [Id], [IsArchived], [Name], [OrdinalId] FROM [inserted] EXCEPT SELECT [Id], [IsArchived], [Name], [OrdinalId] FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, InsertedColumnName AS ColumnName, OldValue, NewValue 
			FROM
		
			(SELECT CAST(''['' + CAST(inserted.[Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, (COALESCE(CAST(inserted.[Id] AS nvarchar(max)), ''__NULL__'')) AS [Id], (COALESCE(CAST(inserted.[IsArchived] AS nvarchar(max)), ''__NULL__'')) AS [IsArchived], (COALESCE(CAST(inserted.[Name] AS nvarchar(max)), ''__NULL__'')) AS [Name], (COALESCE(CAST(inserted.[OrdinalId] AS nvarchar(max)), ''__NULL__'')) AS [OrdinalId],(COALESCE(CAST(deleted.[Id] AS nvarchar(max)), ''__NULL__'')) AS [DeletedId], (COALESCE(CAST(deleted.[IsArchived] AS nvarchar(max)), ''__NULL__'')) AS [DeletedIsArchived], (COALESCE(CAST(deleted.[Name] AS nvarchar(max)), ''__NULL__'')) AS [DeletedName], (COALESCE(CAST(deleted.[OrdinalId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedOrdinalId]
			FROM (SELECT [Id], [IsArchived], [Name], [OrdinalId] FROM [inserted] EXCEPT SELECT [Id], [IsArchived], [Name], [OrdinalId] FROM deleted) AS inserted
			JOIN deleted ON inserted.[Id] = deleted.[Id]) AS insertedJoinedWithDeleted
		
			UNPIVOT (NewValue FOR InsertedColumnName IN ([Id], [IsArchived], [Name], [OrdinalId])) AS unpivotNewValue
			UNPIVOT (OldValue FOR DeletedColumnName IN ([DeletedId], [DeletedIsArchived], [DeletedName], [DeletedOrdinalId])) AS unpivotOldValue

			WHERE ''Deleted''  + InsertedColumnName = DeletedColumnName
				AND ((OldValue IS NULL AND NewValue IS NOT NULL) OR (NewValue IS NOT NULL AND OldValue IS NULL) OR OldValue <> NewValue)
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END'
GO
/****** Object:  Trigger [dbo].[Cds_UserPermissions_INSERT] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Cds_UserPermissions_INSERT]'))
DROP TRIGGER [dbo].[Cds_UserPermissions_INSERT]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Cds_UserPermissions_INSERT]'))
EXEC dbo.sp_executesql @statement = N'

CREATE TRIGGER [dbo].[Cds_UserPermissions_INSERT]  ON [dbo].[Resources]
FOR INSERT AS
BEGIN
/*
ACI Version :Release 1.0
Date:09/22/2017

Details/Description:TRIGGER TO CREATE ROW IN Cds_UserPermissions TABLE

*/
  DECLARE @CDS1 INT , @CDS2 VARCHAR(10)
  SET @CDS1 = 0
  SET @CDS2 = ''''


	BEGIN
		SELECT @CDS1 = i.ResourceId,@CDS2 = i.ResourceType  FROM INSERTED i		
	END
	

	IF (@CDS1 >0)
	BEGIN
	 INSERT INTO [dbo].[Cds_UserPermissions](ResourceId,ResourceType,CDSConfiguration,Alerts,Info)  (SELECT @CDS1,@CDS2,CDSConfiguration,Alerts,Info FROM dbo.CDSRoles where ResourceType = @CDS2)
    END 

END'
GO

/****** Object:  Trigger [dbo].[AuditInsertTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AuditInsertTrigger]'))
DROP TRIGGER [dbo].[AuditInsertTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[AuditInsertTrigger]'))
EXEC dbo.sp_executesql @statement = N'

CREATE   TRIGGER [dbo].[AuditInsertTrigger] ON [dbo].[AuditEntryChanges] AFTER INSERT NOT FOR REPLICATION AS
BEGIN
					

SET NOCOUNT ON

	IF [dbo].[GetNoAudit]() = 0
	BEGIN
		
		DECLARE  @AuditEntryId  uniqueidentifier
 
		SELECT @AuditEntryId=AuditEntryId  FROM [inserted]
		EXEC [dbo].[SC_GetAUDITEVENT]  @AuditEntryId,''add''

	END	
SET NOCOUNT OFF

END'
GO

/****** Object:  Trigger [dbo].[AddDiagnosisToProblemList] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AddDiagnosisToProblemList]'))
DROP TRIGGER [dbo].[AddDiagnosisToProblemList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AddDiagnosisToProblemList]'))
EXEC dbo.sp_executesql @statement = N'


CREATE TRIGGER [dbo].[AddDiagnosisToProblemList]
ON [dbo].[PatientClinicalIcd10]
FOR INSERT
NOT FOR REPLICATION
--This trigger helps with the join from PatientClincial to PatientNotes.
AS
BEGIN

SET NOCOUNT ON

	DECLARE  @AppointmentId  BIGINT
	DECLARE  @PatinentId  BIGINT
	DECLARE  @Icd10  nvarchar(500)
	DECLARE  @Description  nvarchar(1000)
	DECLARE  @Status  nvarchar(2)

	DECLARE  @ConceptID  BIGINT
	DECLARE  @Term  nvarchar(1000)
	DECLARE  @AppDate  nvarchar(20)
	DECLARE @ResourceId Int

	SELECT @AppointmentId=AppointmentId, @PatinentId=PatientId, @Icd10=FindingDetailIcd10, @Description=[Description], @Status=[Status] FROM inserted

	SELECT  TOP 1 @ConceptID=ConceptID, @Term=Term FROM SNOMED_2016 WHERE ICD10=@Icd10
	

	SELECT  @AppDate= AppDate  , @ResourceId= ResourceId1 FROM  Appointments WHERE AppointmentId=@AppointmentId AND PatientId=@PatinentId
	IF(@ConceptID IS NOT NULL)
	BEGIN
		INSERT INTO model.PatientProblemList (
		PatientId,
		AppointmentId,
		AppointmentDate,
		ConceptID,
		Term,
		ICD10,
		ICD10DESCR,
		COMMENTS,
		ResourceName,
		OnsetDate,
		LastModifyDate,
		Status,
		ResourceId,
		EnteredDate,
		ResolvedDate)
		VALUES(@PatinentId,@AppointmentId, CAST(@AppDate as datetime),@ConceptID, @Term, @Icd10,@Description,NUll,NULL,
		CAST(@AppDate as datetime),CAST(@AppDate as datetime), CASE WHEN @Status=''A'' THEN ''Active'' ELSE ''InActive'' END ,@ResourceId, CAST(@AppDate as datetime),NUll)
	END


	--sp_help ''model.PatientProblemList''


END'
GO

/****** Object:  Trigger [dbo].[AuditEntryChangesUpdateTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AuditEntryChangesUpdateTrigger]'))
DROP TRIGGER [dbo].[AuditEntryChangesUpdateTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AuditEntryChangesUpdateTrigger]'))
EXEC dbo.sp_executesql @statement = N'


Create  TRIGGER [dbo].[AuditEntryChangesUpdateTrigger] ON [dbo].[AuditEntryChanges]
After  UPDATE
NOT FOR REPLICATION
AS
SET NOCOUNT ON
IF (dbo.GetNoCheck() = 0)	   
BEGIN


	DECLARE  @AuditEntryId  uniqueidentifier 
		SELECT @AuditEntryId=AuditEntryId  FROM [inserted]
		EXEC [dbo].[SC_GetAUDITEVENT]  @AuditEntryId,''update''

END'
GO


/****** Object:  Trigger [dbo].[IE_DELETE] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[IE_DELETE]'))
DROP TRIGGER [dbo].[IE_DELETE]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[IE_DELETE]'))
EXEC dbo.sp_executesql @statement = N'

CREATE TRIGGER [dbo].[IE_DELETE]  ON [dbo].[IE_Rules]
AFTER DELETE AS
BEGIN

  DECLARE @TABLEKEY INT 
  SET @TABLEKEY=0

  SELECT @TABLEKEY=d.RULEID FROM DELETED d   
				   
	
  IF @TABLEKEY > 0
	DELETE FROM IE_RulesPermissions WHERE RuleId = @TABLEKEY
		
END'
GO

/****** Object:  Trigger [dbo].[IE_INSERT] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[IE_INSERT]'))
DROP TRIGGER [dbo].[IE_INSERT]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[IE_INSERT]'))
EXEC dbo.sp_executesql @statement = N'

CREATE TRIGGER [dbo].[IE_INSERT]  ON [dbo].[IE_Rules]
AFTER INSERT AS
BEGIN

  DECLARE @TABLEKEY INT 
  SET @TABLEKEY=0
  SELECT @TABLEKEY=i.RULEID FROM INSERTED i    
  IF @TABLEKEY > 0
	INSERT INTO IE_RulesPermissions(RuleId,UserId,Intervention,Information,IsActive)  (Select @TABLEKEY, CU.ResourceId, CU.Alerts, CU.Info, ''1'' From  dbo.CDS_UserPermissions CU)
	Insert into dbo.IE_RulesHistory(RuleId, ModifyBy, ModifiedDate) Select i.RuleId, ISNULL(i.ModifyBy, i.CreateBy), isnull(i.ModifyDt,i.CreateDt) From inserted i

END'
GO

/****** Object:  Trigger [model].[AuditFunctionalStatusDeleteTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditFunctionalStatusDeleteTrigger]'))
DROP TRIGGER [model].[AuditFunctionalStatusDeleteTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditFunctionalStatusDeleteTrigger]'))
EXEC dbo.sp_executesql @statement = N'

CREATE  TRIGGER [model].[AuditFunctionalStatusDeleteTrigger] ON [model].[FunctionalStatus] AFTER DELETE NOT FOR REPLICATION AS
BEGIN
					

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN

	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''Model.FunctionalStatus'' AS ObjectName, 2 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName
	,''[id]'' AS KeyNames
	,CAST(''['' + CAST(Id AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues
	,PatientId AS PatientId
	,AppointmentId AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT * FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, OldValue, NULL AS NewValue
			FROM
		
				(SELECT CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 
			(SELECT COALESCE(CAST([AppointmentId] AS nvarchar(max)), ''__NULL__'')) AS [AppointmentId],
			(SELECT COALESCE(CAST([ConceptId] AS nvarchar(max)), ''__NULL__'')) AS [ConceptId], 
			(SELECT COALESCE(CAST([Term] AS nvarchar(max)), ''__NULL__'')) AS [Term],  
			(SELECT COALESCE(CAST([Appointmentdate] AS nvarchar(max)), ''__NULL__'')) AS [Appointmentdate], 		
			(SELECT COALESCE(CAST([Status] AS nvarchar(max)), ''__NULL__'')) AS [Status], 
			(SELECT COALESCE(CAST([PatientId] AS nvarchar(max)), ''__NULL__'')) AS [PatientId], 
			(SELECT COALESCE(CAST([ResourceId] AS nvarchar(max)), ''__NULL__'')) AS [ResourceId],
			(SELECT COALESCE(CAST(functionalValue AS nvarchar(max)), ''__NULL__'')) AS functionalValue

						
			FROM deleted) AS q	
			
			UNPIVOT (OldValue FOR ColumnName IN ([AppointmentId], [ConceptId], [Term], [Appointmentdate],[Status] , [PatientId],[ResourceId],functionalValue)) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END'
GO

/****** Object:  Trigger [model].[AuditFunctionalStatusInsertTrigger]  ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditFunctionalStatusInsertTrigger]'))
DROP TRIGGER [model].[AuditFunctionalStatusInsertTrigger] 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditFunctionalStatusInsertTrigger]'))
EXEC dbo.sp_executesql @statement = N'

CREATE TRIGGER [model].[AuditFunctionalStatusInsertTrigger] ON [model].[FunctionalStatus] AFTER INSERT NOT FOR REPLICATION AS
BEGIN
					

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''Model.FunctionalStatus'' AS ObjectName, 0 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName
	,''[id]'' AS KeyNames
	,CAST(''['' + CAST(id AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues
	,PatientId AS PatientId
	,AppointmentId AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT * FROM [inserted]) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, NULL AS OldValue, NewValue AS NewValue
			FROM
		
			(SELECT CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 
			(SELECT COALESCE(CAST([AppointmentId] AS nvarchar(max)), ''__NULL__'')) AS [AppointmentId],
			(SELECT COALESCE(CAST([ConceptId] AS nvarchar(max)), ''__NULL__'')) AS [ConceptId], 
			(SELECT COALESCE(CAST([Term] AS nvarchar(max)), ''__NULL__'')) AS [Term], 		
			(SELECT COALESCE(CAST([Appointmentdate] AS nvarchar(max)), ''__NULL__'')) AS [Appointmentdate], 		
			(SELECT COALESCE(CAST([Status] AS nvarchar(max)), ''__NULL__'')) AS [Status], 
			(SELECT COALESCE(CAST([PatientId] AS nvarchar(max)), ''__NULL__'')) AS [PatientId], 
			(SELECT COALESCE(CAST([ResourceId] AS nvarchar(max)), ''__NULL__'')) AS [ResourceId],
			(SELECT COALESCE(CAST(functionalValue AS nvarchar(max)), ''__NULL__'')) AS functionalValue
			FROM [inserted]) AS q
		
			UNPIVOT (NewValue FOR ColumnName IN ([AppointmentId], [ConceptId], [Term], [Appointmentdate],[Status] , [PatientId],[ResourceId],functionalValue)) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END

SET NOCOUNT OFF

END'
GO


/****** Object:  Trigger [model].[AuditFunctionalStatusUpdateTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditFunctionalStatusUpdateTrigger]'))
DROP TRIGGER [model].[AuditFunctionalStatusUpdateTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditFunctionalStatusUpdateTrigger]'))
EXEC dbo.sp_executesql @statement = N'


CREATE TRIGGER [model].[AuditFunctionalStatusUpdateTrigger] ON  [model].[FunctionalStatus] AFTER UPDATE NOT FOR REPLICATION AS
BEGIN
					

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''Model.FunctionalStatus'' AS ObjectName, 1 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName
	,''[Id]'' AS KeyNames
	,CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues
	,PatientId AS PatientId
	,AppointmentId AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT ID,[AppointmentId], [ConceptId], [Term], [Appointmentdate],[Status] , [PatientId],[ResourceId] FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, InsertedColumnName AS ColumnName, OldValue, NewValue 
			FROM
		
		(SELECT CAST(''['' + CAST(inserted.[Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 
			(COALESCE(CAST(inserted.[AppointmentId] AS nvarchar(max)), ''__NULL__'')) AS [AppointmentId],
			(COALESCE(CAST(inserted.[ConceptId] AS nvarchar(max)), ''__NULL__'')) AS [ConceptId], 
			(COALESCE(CAST(inserted.[Term] AS nvarchar(max)), ''__NULL__'')) AS [Term], 						
			(COALESCE(CAST(inserted.[Appointmentdate] AS nvarchar(max)), ''__NULL__'')) AS [Appointmentdate], 		
			(COALESCE(CAST(inserted.[Status] AS nvarchar(max)), ''__NULL__'')) AS [Status], 
			(COALESCE(CAST(inserted.[PatientId] AS nvarchar(max)), ''__NULL__'')) AS [PatientId], 
			(COALESCE(CAST(inserted.[ResourceId] AS nvarchar(max)), ''__NULL__'')) AS [ResourceId],
			(COALESCE(CAST(inserted.functionalValue AS nvarchar(max)), ''__NULL__'')) AS functionalValue,


			(COALESCE(CAST(deleted.[AppointmentId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedAppointmentId],
			(COALESCE(CAST(deleted.[ConceptId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedConceptId], 
			(COALESCE(CAST(deleted.[Term] AS nvarchar(max)), ''__NULL__'')) AS [DeletedTerm], 								
			(COALESCE(CAST(deleted.[Appointmentdate] AS nvarchar(max)), ''__NULL__'')) AS [DeletedAppointmentdate], 		
			(COALESCE(CAST(deleted.[Status] AS nvarchar(max)), ''__NULL__'')) AS [DeletedStatus], 
			(COALESCE(CAST(deleted.[PatientId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedPatientId], 
			(COALESCE(CAST(deleted.[ResourceId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedResourceId],
			(COALESCE(CAST(deleted.functionalValue AS nvarchar(max)), ''__NULL__'')) AS [DeletedfunctionalValue]

			FROM (
			SELECT Id,[AppointmentId], [ConceptId], [Term], [Appointmentdate],[Status] , [PatientId],[ResourceId],functionalValue 
			FROM [inserted]
			EXCEPT 
			SELECT Id,[AppointmentId], [ConceptId], [Term], [Appointmentdate],[Status] , [PatientId],[ResourceId] ,functionalValue
			FROM deleted) AS inserted
			JOIN deleted ON inserted.Id = deleted.Id) AS insertedJoinedWithDeleted
		
			UNPIVOT (NewValue FOR InsertedColumnName IN ([AppointmentId], [ConceptId], [Term], [Appointmentdate],[Status] , [PatientId],[ResourceId],functionalValue)) AS unpivotNewValue
			UNPIVOT (OldValue FOR DeletedColumnName IN ([DeletedAppointmentId], [DeletedConceptId], [DeletedTerm], [DeletedAppointmentdate], [DeletedStatus],[DeletedPatientId], [DeletedResourceId],[DeletedfunctionalValue])) AS unpivotOldValue

			WHERE ''Deleted''  + InsertedColumnName = DeletedColumnName
				AND ((OldValue IS NULL AND NewValue IS NOT NULL) OR (NewValue IS NOT NULL AND OldValue IS NULL) OR OldValue <> NewValue)
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END'
GO

/****** Object:  Trigger [model].[AuditGoalInstructionDeleteTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditGoalInstructionDeleteTrigger]'))
DROP TRIGGER [model].[AuditGoalInstructionDeleteTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditGoalInstructionDeleteTrigger]'))
EXEC dbo.sp_executesql @statement = N'

CREATE  TRIGGER [model].[AuditGoalInstructionDeleteTrigger] ON [model].[GoalInstruction] AFTER DELETE NOT FOR REPLICATION AS
BEGIN
					

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN

	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''Model.GoalInstruction'' AS ObjectName, 2 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName
	,''[id]'' AS KeyNames
	,CAST(''['' + CAST(Id AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues
	,PatientId AS PatientId
	,AppointmentId AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT * FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, OldValue, NULL AS NewValue
			FROM
		
				(SELECT CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 
			(SELECT COALESCE(CAST([AppointmentId] AS nvarchar(max)), ''__NULL__'')) AS [AppointmentId],
			(SELECT COALESCE(CAST([ConceptId] AS nvarchar(max)), ''__NULL__'')) AS [ConceptId], 
			(SELECT COALESCE(CAST([Term] AS nvarchar(max)), ''__NULL__'')) AS [Term],  
			(SELECT COALESCE(CAST([Goal] AS nvarchar(max)), ''__NULL__'')) AS [Goal],  
			(SELECT COALESCE(CAST([Appointmentdate] AS nvarchar(max)), ''__NULL__'')) AS [Appointmentdate], 		
			(SELECT COALESCE(CAST([Status] AS nvarchar(max)), ''__NULL__'')) AS [Status], 
			(SELECT COALESCE(CAST([PatientId] AS nvarchar(max)), ''__NULL__'')) AS [PatientId], 
			(SELECT COALESCE(CAST([ResourceId] AS nvarchar(max)), ''__NULL__'')) AS [ResourceId]	,		
			(SELECT COALESCE(CAST([ResourceId] AS nvarchar(max)), ''__NULL__'')) AS health	,		
			(SELECT COALESCE(CAST([ResourceId] AS nvarchar(max)), ''__NULL__'')) AS reason	
			FROM deleted) AS q	
			
			UNPIVOT (OldValue FOR ColumnName IN ([AppointmentId], [ConceptId], [Term],[Goal], [Appointmentdate],[Status] , [PatientId],[ResourceId],health,reason)) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END'
GO

/****** Object:  Trigger [model].[AuditGoalInstructionInsertTrigger]  ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditGoalInstructionInsertTrigger]'))
DROP TRIGGER [model].[AuditGoalInstructionInsertTrigger] 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditGoalInstructionInsertTrigger]'))
EXEC dbo.sp_executesql @statement = N'

CREATE TRIGGER [model].[AuditGoalInstructionInsertTrigger] ON [model].[GoalInstruction] AFTER INSERT NOT FOR REPLICATION AS
BEGIN
					

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''Model.GoalInstruction'' AS ObjectName, 0 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName
	,''[id]'' AS KeyNames
	,CAST(''['' + CAST(id AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues
	,PatientId AS PatientId
	,AppointmentId AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT * FROM [inserted]) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, ColumnName AS ColumnName, NULL AS OldValue, NewValue AS NewValue
			FROM
		
			(SELECT CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 
			(SELECT COALESCE(CAST([AppointmentId] AS nvarchar(max)), ''__NULL__'')) AS [AppointmentId],
			(SELECT COALESCE(CAST([ConceptId] AS nvarchar(max)), ''__NULL__'')) AS [ConceptId], 
			(SELECT COALESCE(CAST([Term] AS nvarchar(max)), ''__NULL__'')) AS [Term], 		
			(SELECT COALESCE(CAST([Goal] AS nvarchar(max)), ''__NULL__'')) AS [Goal], 		
			(SELECT COALESCE(CAST([Appointmentdate] AS nvarchar(max)), ''__NULL__'')) AS [Appointmentdate], 		
			(SELECT COALESCE(CAST([Status] AS nvarchar(max)), ''__NULL__'')) AS [Status], 
			(SELECT COALESCE(CAST([PatientId] AS nvarchar(max)), ''__NULL__'')) AS [PatientId], 
			(SELECT COALESCE(CAST([ResourceId] AS nvarchar(max)), ''__NULL__'')) AS [ResourceId],
				(SELECT COALESCE(CAST([ResourceId] AS nvarchar(max)), ''__NULL__'')) AS health	,		
			(SELECT COALESCE(CAST([ResourceId] AS nvarchar(max)), ''__NULL__'')) AS reason	
		
			FROM [inserted]) AS q
		
			UNPIVOT (NewValue FOR ColumnName IN ([AppointmentId], [ConceptId], [Term],[Goal], [Appointmentdate],[Status] , [PatientId],[ResourceId],health,reason)) AS unpivotValue
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
SET NOCOUNT OFF

END'
GO

/****** Object:  Trigger [model].[AuditGoalInstructionUpdateTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditGoalInstructionUpdateTrigger]'))
DROP TRIGGER [model].[AuditGoalInstructionUpdateTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[model].[AuditGoalInstructionUpdateTrigger]'))
EXEC dbo.sp_executesql @statement = N'


CREATE TRIGGER [model].[AuditGoalInstructionUpdateTrigger] ON  [model].[GoalInstruction] AFTER UPDATE NOT FOR REPLICATION AS
BEGIN
					

SET NOCOUNT ON

IF [dbo].[GetNoAudit]() = 0
BEGIN
	DECLARE @__userIdContext nvarchar(max)				
	IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
	BEGIN
		SELECT @__userIdContext = UserId FROM #UserContext
	END

	DECLARE @__userId int
	IF (ISNUMERIC(@__userIdContext) = 1)
	BEGIN
		SELECT @__userId = CONVERT(int, @__userIdContext)
	END

	DECLARE @__auditDate datetime
	SET @__auditDate = GETUTCDATE()
	DECLARE @__hostName nvarchar(500)
	BEGIN TRY
	EXEC sp_executesql N''SELECT @__hostName = HOST_NAME()'', N''@__hostName nvarchar(500) OUTPUT'', @__hostName = @__hostName OUTPUT
	END TRY
	BEGIN CATCH
		SET @__hostName = CURRENT_USER
	END CATCH


	SELECT NEWID() AS Id, ''Model.GoalInstruction'' AS ObjectName, 1 AS ChangeTypeId, @__auditDate AS AuditDateTime, @__hostName AS HostName, @__userId AS UserId, CONVERT(nvarchar, ServerProperty(''ServerName'')) AS ServerName
	,''[Id]'' AS KeyNames
	,CAST(''['' + CAST([Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues
	,PatientId AS PatientId
	,AppointmentId AS AppointmentId
	INTO #__AuditEntries 
	FROM (SELECT ID,[AppointmentId], [ConceptId], [Term],[Goal], [Appointmentdate],[Status] , [PatientId],[ResourceId] FROM deleted) AS changedRows

	INSERT INTO dbo.AuditEntries(Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName, KeyNames, KeyValues, PatientId, AppointmentId)
	SELECT * FROM #__AuditEntries 

	INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
		SELECT ae.Id, ColumnName, CASE WHEN OldValue = ''__NULL__'' THEN NULL ELSE OldValue END, CASE WHEN NewValue = ''__NULL__'' THEN NULL ELSE NewValue END
		FROM (
		SELECT KeyValues, InsertedColumnName AS ColumnName, OldValue, NewValue 
			FROM
		
		(SELECT CAST(''['' + CAST(inserted.[Id] AS nvarchar(max)) + '']'' AS nvarchar(400)) AS KeyValues, 
			(COALESCE(CAST(inserted.[AppointmentId] AS nvarchar(max)), ''__NULL__'')) AS [AppointmentId],
			(COALESCE(CAST(inserted.[ConceptId] AS nvarchar(max)), ''__NULL__'')) AS [ConceptId], 
			(COALESCE(CAST(inserted.[Term] AS nvarchar(max)), ''__NULL__'')) AS [Term], 			
			(COALESCE(CAST(inserted.[Goal] AS nvarchar(max)), ''__NULL__'')) AS [Goal], 				
			(COALESCE(CAST(inserted.[Appointmentdate] AS nvarchar(max)), ''__NULL__'')) AS [Appointmentdate], 		
			(COALESCE(CAST(inserted.[Status] AS nvarchar(max)), ''__NULL__'')) AS [Status], 
			(COALESCE(CAST(inserted.[PatientId] AS nvarchar(max)), ''__NULL__'')) AS [PatientId], 
			(COALESCE(CAST(inserted.[ResourceId] AS nvarchar(max)), ''__NULL__'')) AS [ResourceId],
			(COALESCE(CAST(inserted.health AS nvarchar(max)), ''__NULL__'')) AS health,
			(COALESCE(CAST(inserted.reason AS nvarchar(max)), ''__NULL__'')) AS reason,

			
			(COALESCE(CAST(deleted.[AppointmentId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedAppointmentId],
			(COALESCE(CAST(deleted.[ConceptId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedConceptId], 
			(COALESCE(CAST(deleted.[Term] AS nvarchar(max)), ''__NULL__'')) AS [DeletedTerm], 			
			(COALESCE(CAST(deleted.[Goal] AS nvarchar(max)), ''__NULL__'')) AS [DeletedGoal], 			
			(COALESCE(CAST(deleted.[Appointmentdate] AS nvarchar(max)), ''__NULL__'')) AS [DeletedAppointmentdate], 		
			(COALESCE(CAST(deleted.[Status] AS nvarchar(max)), ''__NULL__'')) AS [DeletedStatus], 
			(COALESCE(CAST(deleted.[PatientId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedPatientId], 
			(COALESCE(CAST(deleted.[ResourceId] AS nvarchar(max)), ''__NULL__'')) AS [DeletedResourceId],
			(COALESCE(CAST(deleted.health AS nvarchar(max)), ''__NULL__'')) AS [Deletedhealth], 
			(COALESCE(CAST(deleted.reason AS nvarchar(max)), ''__NULL__'')) AS [Deletedreason]

			FROM (
			SELECT Id,[AppointmentId], [ConceptId], [Term],[Goal], [Appointmentdate],[Status] , [PatientId],[ResourceId] ,health,reason
			FROM [inserted]
			EXCEPT 
			SELECT Id,[AppointmentId], [ConceptId], [Term],[Goal], [Appointmentdate],[Status] , [PatientId],[ResourceId] ,health,reason
			FROM deleted) AS inserted
			JOIN deleted ON inserted.Id = deleted.Id) AS insertedJoinedWithDeleted
		
			UNPIVOT (NewValue FOR InsertedColumnName IN ([AppointmentId], [ConceptId], [Term],[Goal], [Appointmentdate],[Status] , [PatientId],[ResourceId],health,reason)) AS unpivotNewValue
			UNPIVOT (OldValue FOR DeletedColumnName IN ([DeletedAppointmentId], [DeletedConceptId], [DeletedTerm], [DeletedGoal] ,[DeletedAppointmentdate], [DeletedStatus],[DeletedPatientId], [DeletedResourceId],[Deletedhealth],[Deletedreason])) AS unpivotOldValue

			WHERE ''Deleted''  + InsertedColumnName = DeletedColumnName
				AND ((OldValue IS NULL AND NewValue IS NOT NULL) OR (NewValue IS NOT NULL AND OldValue IS NULL) OR OldValue <> NewValue)
			
		) AS q
		JOIN #__AuditEntries ae ON q.KeyValues = ae.KeyValues		

END
		
SET NOCOUNT OFF

END'
GO

/****** Object:  Trigger [dbo].[AuditPatientClinical_tempDeleteTrigger] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AuditPatientClinical_tempDeleteTrigger]'))
DROP TRIGGER [dbo].[AuditPatientClinical_tempDeleteTrigger]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AuditPatientClinical_tempDeleteTrigger]'))
EXEC dbo.sp_executesql @statement = N'

create  TRIGGER [dbo].[AuditPatientClinical_tempDeleteTrigger] ON [dbo].PatientClinical_temp AFTER DELETE NOT FOR REPLICATION AS
				BEGIN
DECLARE @msrepl_object_id int
SET @msrepl_object_id = OBJECT_ID(''[dbo].[PatientClinical]'')
DECLARE @retcode int
EXEC @retcode = sp_check_for_sync_trigger @msrepl_object_id
IF @retcode = 1 RETURN
					SET NOCOUNT ON
					DECLARE @newIds table (
						AuditEntryId uniqueIdentifier,
						KeyValue nvarchar(400),
						KeyName nvarchar(400),
						PatientId INT,
						AppointmentId INT,
						PRIMARY KEY(AuditEntryId, KeyName)
					)

					INSERT INTO @newIds (AuditEntryId, KeyName, KeyValue, PatientId, AppointmentId)
						SELECT AuditEntryId, KeyName, KeyValue, PatientId, AppointmentId FROM
						(SELECT NEWID() AS AuditEntryId, (SELECT CAST(d.[ClinicalId] AS nvarchar(max))) AS [ClinicalId],PatientId AS PatientId,AppointmentId AS AppointmentId
						FROM deleted d) AS q
						UNPIVOT (KeyValue FOR KeyName IN ([ClinicalId])) AS unpvt

					DECLARE @userIdContext nvarchar(max)
					IF OBJECT_ID(''tempdb..#UserContext'') IS NOT NULL
					BEGIN
						SELECT @userIdContext = UserId FROM #UserContext
					END

					DECLARE @userId int
					IF (ISNUMERIC(RIGHT(@userIdContext, 12)) = 1)
					BEGIN
						SELECT @userId = CONVERT(int, right(@userIdContext, 12))
					END

					DECLARE @auditDate datetime
					SET @auditDate = GETUTCDATE()
					DECLARE @hostName nvarchar(500)
					BEGIN TRY
					EXEC sp_executesql N''SELECT @hostName = HOST_NAME()'', N''@hostName nvarchar(500) OUTPUT'', @hostName = @hostName OUTPUT
					END TRY
					BEGIN CATCH
						 SET @hostName = CURRENT_USER
					END CATCH

					INSERT INTO dbo.AuditEntries (Id, ObjectName, ChangeTypeId, AuditDateTime, HostName, UserId, ServerName,KeyNames, KeyValues, PatientId, AppointmentId)
						SELECT DISTINCT AuditEntryId, ''dbo.PatientClinical'', 2, @auditDate, @hostName, @userId, convert(nvarchar, ServerProperty(''ServerName'')) ,''['' + KeyName + '']'',''['' + CAST(KeyValue AS NVARCHAR(MAX)) + '']'', PatientId, AppointmentId FROM @newIds

					INSERT INTO dbo.AuditEntryChanges (AuditEntryId, ColumnName, OldValue, NewValue)
						SELECT newIds.AuditEntryId, DeletedColumnName, OldValue , NULL FROM
						(SELECT d.[ClinicalId] AS [KeyClinicalId],(SELECT CAST(d.Activity AS nvarchar(max))) AS [Activity], (SELECT CAST(d.[AppointmentId] AS nvarchar(max))) AS [AppointmentId], (SELECT CAST(d.[ClinicalId] AS nvarchar(max))) AS [ClinicalId], (SELECT CAST(d.[ClinicalType] AS nvarchar(max))) AS [ClinicalType], (SELECT CAST(d.[DrawFileName] AS nvarchar(max))) AS [DrawFileName], (SELECT CAST(d.[EyeContext] AS nvarchar(max))) AS [EyeContext], (SELECT CAST(d.[FindingDetail] AS nvarchar(max))) AS [FindingDetail], (SELECT CAST(d.[FollowUp] AS nvarchar(max))) AS [FollowUp], (SELECT CAST(d.[Highlights] AS nvarchar(max))) AS [Highlights], (SELECT CAST(d.[ImageDescriptor] AS nvarchar(max))) AS [ImageDescriptor], (SELECT CAST(d.[ImageInstructions] AS nvarchar(max))) AS [ImageInstructions], (SELECT CAST(d.[PatientId] AS nvarchar(max))) AS [PatientId], (SELECT CAST(d.[PermanentCondition] AS nvarchar(max))) AS [PermanentCondition], (SELECT CAST(d.[PostOpPeriod] AS nvarchar(max))) AS [PostOpPeriod], (SELECT CAST(d.[Status] AS nvarchar(max))) AS [Status], (SELECT CAST(d.[Surgery] AS nvarchar(max))) AS [Surgery], (SELECT CAST(d.[Symptom] AS nvarchar(max))) AS [Symptom]
						FROM deleted d) AS q
						UNPIVOT (OldValue FOR DeletedColumnName IN ([Activity], [AppointmentId], [ClinicalId], [ClinicalType], [DrawFileName], [EyeContext], [FindingDetail], [FollowUp], [Highlights], [ImageDescriptor], [ImageInstructions], [PatientId], [PermanentCondition], [PostOpPeriod], [Status], [Surgery], [Symptom])) AS unpvt
						INNER JOIN @newIds newIds ON (newIds.KeyValue = unpvt.[KeyClinicalId] AND newIds.KeyName = ''ClinicalId'')
					SET NOCOUNT OFF

					IF @userId IS NULL
					BEGIN
						-- The following accounts for a special case for logging in.  The UserId is null when logging in where an AuditEntryChange is recorded.  During this time only,
						-- we can use the ResourceId value as the UserId value.  Otherwise in all other cases, the UserId is null.
						SELECT @userId = KeyValue FROM @newIds newIds
						JOIN dbo.AuditEntryChanges aec ON newIds.AuditEntryId = aec.AuditEntryId AND aec.ColumnName = ''IsLoggedIn''
						WHERE newIds.KeyName = ''ResourceId''

						IF @userId IS NOT NULL
						BEGIN
							UPDATE ae
							SET UserId = @userId
							FROM dbo.AuditEntries ae
							JOIN @newIds newIds ON ae.Id = newIds.AuditEntryId AND newIds.KeyName = ''ResourceId''
							JOIN dbo.AuditEntryChanges aec ON ae.Id = aec.AuditEntryId AND aec.ColumnName = ''IsLoggedIn''
							WHERE UserId = NULL
						END
					END
END'
GO

SET ANSI_PADDING ON
GO