Insert into CDS_UserPermissions(ResourceId,
ResourceType,
CDSConfiguration,
Alerts,
Info)
Select ResourceId, ResourceType, 0, 0, 0 from dbo.Resources Where ResourceType in (Select ResourceType from cdsroles)

Insert into IE_RulesPermissions Select RuleId, ResourceId, 0, 0, 0 From IE_Rules, CDS_UserPermissions 

Update IE_Rules Set EventRaiseID = 1 

SET ANSI_PADDING ON
GO
