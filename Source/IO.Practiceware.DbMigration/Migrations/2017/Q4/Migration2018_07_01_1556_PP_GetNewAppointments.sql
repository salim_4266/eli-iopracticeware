

/****** Object:  StoredProcedure [MVE].[PP_GetNewAppointments]    Script Date: 06/29/2018 13:37:43 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_GetNewAppointments]')) 
Drop Procedure [MVE].[PP_GetNewAppointments]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  


 CREATE Procedure [MVE].[PP_GetNewAppointments] @PatientActionId int    
 As       
 Begin      
  SELECT Distinct Top(100) EN.serviceLocationId as LocationExternalId, AP.Resourceid1 as DoctorExternalId, AP.Patientid as PatientExternalId, En.EncounterStatusId as  AppointmentStatusExternalId, '' as CancelReason, EN.startDatetime as Start, EN.EndDateTime as [End], AP.AppointmentId as ExternalId FROM dbo.Appointments AP with (nolock) Inner Join  model.Encounters EN with (nolock) ON Ap.EncounterID=EN.id Inner Join MVE.PP_PortalQueueResponse PQR with (nolock) on PQR.ExternalId = AP.PatientId and  PQR.ActionTypeLookupId = 97 Where AP.Resourceid1 <> 0 and AP.AppDate > 20170331 and AP.AppointmentId NOT IN (Select distinct ExternalId from MVE.PP_PortalQueueResponse PQR1 with (nolock) Where PQR1.ExternalId = AP.AppointmentId and PQR1.ActionTypeLookupId = 80 Union Select distinct ExternalId from MVE.PP_PortalQueueResponse PQR1 with (nolock) Where PQR1.ExternalId = AP.AppointmentId and PQR1.ActionTypeLookupId = 84  Union Select distinct ExternalId from MVE.PP_PortalQueueException PQR2 with (nolock) Where PQR2.ExternalId = AP.AppointmentId and PQR2.ActionTypeLookupId = 80 
 and ProcessCount >= 5 union Select distinct ExternalId from MVE.PP_PortalQueueException PQR2 with (nolock) Where PQR2.ExternalId = AP.AppointmentId and PQR2.ActionTypeLookupId = 84 and ProcessCount >= 5)     
End

GO



