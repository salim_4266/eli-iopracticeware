

/****** Object:  Table [MVE].[PP_PortalQueue]    Script Date: 06/29/2018 13:26:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_PortalQueue]') AND type in (N'U'))
BEGIN

CREATE TABLE [MVE].[PP_PortalQueue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MessageData] [nvarchar](max) NOT NULL,
	[ActionTypeLookupId] [int] NOT NULL,
	[ProcessedDate] [datetime] NOT NULL,
	[PatientNo] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Medflow.PortalQueue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_Medflow.PortalQueue_MessageData]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueue] DROP CONSTRAINT [DF_Medflow.PortalQueue_MessageData]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_Medflow.PortalQueue_MessageData]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueue] ADD  CONSTRAINT [DF_Medflow.PortalQueue_MessageData]  DEFAULT ('') FOR [MessageData]
END
GO



IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_Medflow.PortalQueue_ActionTypeLookupId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueue] DROP CONSTRAINT [DF_Medflow.PortalQueue_ActionTypeLookupId]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_Medflow.PortalQueue_ActionTypeLookupId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueue] ADD  CONSTRAINT [DF_Medflow.PortalQueue_ActionTypeLookupId]  DEFAULT ((0)) FOR [ActionTypeLookupId]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_Medflow.PortalQueue_ProcessedDate]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueue] DROP CONSTRAINT [DF_Medflow.PortalQueue_ProcessedDate]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_Medflow.PortalQueue_ProcessedDate]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueue] ADD  CONSTRAINT [DF_Medflow.PortalQueue_ProcessedDate]  DEFAULT ('1-1-1900') FOR [ProcessedDate]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_Medflow.PortalQueue_PatientNo]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueue] DROP CONSTRAINT [DF_Medflow.PortalQueue_PatientNo]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_Medflow.PortalQueue_PatientNo]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueue] ADD  CONSTRAINT [DF_Medflow.PortalQueue_PatientNo]  DEFAULT ('') FOR [PatientNo]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_Medflow.PortalQueue_IsActive]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueue] DROP CONSTRAINT [DF_Medflow.PortalQueue_IsActive]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_Medflow.PortalQueue_IsActive]')) 
BEGIN
	ALTER TABLE [MVE].[PP_PortalQueue] ADD  CONSTRAINT [DF_Medflow.PortalQueue_IsActive]  DEFAULT ((1)) FOR [IsActive]
END
GO






