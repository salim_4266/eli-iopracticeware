
/****** Object:  View [dbo].[AuditEntry] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AuditEntry]'))
DROP VIEW [dbo].[AuditEntry]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AuditEntry]'))
EXEC dbo.sp_executesql @statement = N'

create VIEW [dbo].[AuditEntry]
AS
SELECT   top 1000      dbo.AuditEntries.Id as AuditEntryId, dbo.AuditActionName.ActioName, dbo.AuditEntries.AuditDateTime, GETDATE() as SystemTime,
DATEADD(MINUTE, 330, AuditDateTime) AS IST,DATEADD(hh, -4, AuditDateTime) AS EST,DATEADD(hh, -5, AuditDateTime) AS CST,
 dbo.AuditEntries.HostName, dbo.AuditEntries.ServerName, dbo.AuditEntries.ObjectName, R.ResourceName, 
                         dbo.AuditEntries.KeyNames, dbo.AuditEntries.KeyValues, dbo.AuditEntries.PatientId, P.FirstName+'' '' +P.LastName as PatientName, dbo.AuditEntries.AppointmentId, dbo.AuditEntries.KeyValueNumeric,
                         dbo.AuditEntryChanges.ColumnName, dbo.AuditEntryChanges.OldValue, dbo.AuditEntryChanges.NewValue, dbo.AuditEntryChanges.OldValueNumeric, dbo.AuditEntryChanges.NewValueNumeric
FROM            dbo.AuditEntries INNER JOIN
                         dbo.AuditEntryChanges ON dbo.AuditEntries.Id = dbo.AuditEntryChanges.AuditEntryId INNER JOIN
                         dbo.AuditActionName ON dbo.AuditEntries.ChangeTypeId = dbo.AuditActionName.actionID
						 Left JOIN Resources R  ON  dbo.AuditEntries.UserId=R.ResourceId
						 Left JOIN  model.Patients P ON P.id= dbo.AuditEntries.PatientId
						 Order by  dbo.AuditEntries.AuditDateTime DESC

 '
GO

/****** Object:  View [dbo].[AuditLog] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
DROP VIEW [dbo].[AuditLog]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
EXEC dbo.sp_executesql @statement = N'




--select  top 100 * FROM [dbo].[AuditLog]
create VIEW [dbo].[AuditLog]
AS
SELECT       dbo.LogEntries.Id, dbo.LogEntries.DateTime as AuditDateTime,Getdate() as SystemTime,
DATEADD(MINUTE, 330, DateTime) AS IST,DATEADD(hh, -4, DateTime) AS EST,DATEADD(hh, -5, DateTime) AS CST,
dbo.GroupFilterCategory.GroupName as ActionName,  dbo.GroupFilterCategory.LogCategoryName as ActivityName
,dbo.LogEntries.Source, 

 dbo.LogEntries.Message,
 dbo.LogEntries.MachineName, dbo.LogEntries.ThreadName, dbo.LogEntries.ProcessId, dbo.LogEntries.ProcessName,   R.ResourceName,  dbo.LogEntries.ServerName
FROM            dbo.LogEntryLogCategory INNER JOIN
                         dbo.LogEntries ON dbo.LogEntryLogCategory.LogEntryLogCategory_LogCategory_Id = dbo.LogEntries.Id INNER JOIN
                         dbo.GroupFilterCategory ON dbo.GroupFilterCategory.LogCategoryId = dbo.LogEntryLogCategory.Categories_Id
						  Left JOIN Resources R  ON  dbo.LogEntries.userid=R.ResourceId
'
GO

/****** Object:  View [dbo].[AuditLogEntries] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AuditLogEntries]'))
DROP VIEW [dbo].[AuditLogEntries]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AuditLogEntries]'))
EXEC dbo.sp_executesql @statement = N' Create View [dbo].[AuditLogEntries]
As 
SELECT
  AuditEntryId,
  ActioName,
  NULL AS ActivityName,
  NULL AS DetailChanged,
  AuditDateTime, Getdate() AS SystemTime,DATEADD(hh, -4, AuditDateTime) AS EST,DATEADD(hh, -5, AuditDateTime) AS CST,
  HostName AS AccessDevice,
  NULL ProcessId,
  NULL ProcessName,
  ResourceName,
  ServerName,
  ObjectName,
  KeyNames,
  KeyValues,
  PatientId,
  PatientName,
  AppointmentId,
  ColumnName,
  OldValue,
  NewValue
FROM [dbo].[AuditEntry]

UNION ALL

SELECT
  AuditEntryId,
  ''Copy'' as ActioName,
  NULL AS ActivityName,
  NULL AS DetailChanged,
  AuditDateTime,Getdate() AS SystemTime,DATEADD(hh, -4, AuditDateTime) AS EST,DATEADD(hh, -5, AuditDateTime) AS CST,
  HostName AS AccessDevice,
  NULL ProcessId,
  NULL ProcessName,
  ResourceName,
  ServerName,
  ObjectName,
  KeyNames,
  KeyValues,
  PatientId,
  PatientName,
  AppointmentId,
  ColumnName,
  OldValue,
  NewValue
FROM [dbo].[AuditEntry]
					WHERE
					ActioName IN (''Addition'',''Deletion'')
					AND ObjectName=''PracticeFavorites'' 

					UNIOn ALL
					
SELECT
  AuditEntryId,
  ''Changes to user previleges'' as ActioName,
  NULL AS ActivityName,
  NULL AS DetailChanged,
  AuditDateTime,Getdate() AS SystemTime,DATEADD(hh, -4, AuditDateTime) AS EST,DATEADD(hh, -5, AuditDateTime) AS CST,
  HostName AS AccessDevice,
  NULL ProcessId,
  NULL ProcessName,
  ResourceName,
  ServerName,
  ObjectName,
  KeyNames,
  KeyValues,
  PatientId,
  PatientName,
  AppointmentId,
  ColumnName,
  OldValue,
  NewValue
FROM [dbo].[AuditEntry]
					WHERE
					ActioName IN (''Addition'',''Deletion'', ''Changes'')
					AND ObjectName=''User-Privileges'' 
					Union All
SELECT
  *
FROM (SELECT
  Id AS AuditEntryId,
  ActionName,
  ActivityName,
  [Message] AS DetailChanged,
  AuditDateTime,Getdate() AS SystemTime,DATEADD(hh, -4, AuditDateTime) AS EST,DATEADD(hh, -5, AuditDateTime) AS CST,
  MachineName AS AccessDevice,
  ProcessId,
  ProcessName,
  ResourceName,
  ServerName,
  NULL ObjectName,
  NULL KeyNames,
  NULL KeyValues,

  (SELECT TOP 1
    CASE
      WHEN lep.Name = ''PatientId'' THEN CAST(lep.Value AS bigint)
      WHEN lep.Name = ''AppointmentId'' THEN (SELECT TOP 1
          PatientId
        FROM dbo.Appointments
        WHERE AppointmentId = CAST(lep.Value AS bigint))
    END
  FROM dbo.LogEntryProperties lep
  WHERE lep.LogEntryId = le.Id
  AND (lep.Name = ''PatientId''
  OR lep.Name = ''AppointmentId''))
  AS PatientId,
  '''' AS PatientName,
  '''' AppointmentId,
  '''' ColumnName,

  NULL AS OldValue,
  NULL AS NewValue

FROM [dbo].[AuditLog] le) AS q' 
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[PatientDemographics]'))
DROP VIEW [dbo].[PatientDemographics]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select top 5  * from model.patients order by 1 desc 

-- SELECT ethnicity,language,race FROM PatientDemographics WHERE PatientId=37436
CREATE VIEW [dbo].[PatientDemographics] AS
SELECT        Id AS PatientId, CASE WHEN DefaultUserId IS NULL THEN 0 ELSE DefaultUserId END AS SchedulePrimaryDoctor, REPLACE(ISNULL(FirstName, ''), '''', '') AS FirstName, 
                         CASE WHEN IsClinical = 0 THEN 'I' WHEN PatientStatusId = 2 THEN 'C' WHEN PatientStatusId = 3 THEN 'D' WHEN PatientStatusId = 4 THEN 'M' ELSE 'A' END AS Status, REPLACE(ISNULL(LastName, ''), '''', '') 
                         AS LastName, REPLACE(ISNULL(MiddleName, ''), '''', '') AS MiddleInitial, ISNULL(Prefix, '') AS Salutation, ISNULL(Suffix, '') AS NameReference, CASE WHEN DateOfBirth IS NULL THEN CONVERT(nvarchar(64), '') 
                         ELSE DATENAME(yyyy, DateOfBirth) + RIGHT('00' + CONVERT(nvarchar(2), DATEPART(mm, DateOfBirth)), 2) + RIGHT('00' + CONVERT(nvarchar(2), DATEPART(dd, DateOfBirth)), 2) END AS BirthDate, 
                         CASE WHEN GenderId = 1 THEN 'U' WHEN GenderId = 2 THEN 'F' WHEN GenderId = 3 THEN 'M' WHEN GenderId = 4 THEN 'O' ELSE '' END AS Gender, 
                         CASE WHEN MaritalStatusId = 1 THEN 'S' WHEN MaritalStatusId = 2 THEN 'M' WHEN MaritalStatusId = 3 THEN 'D' WHEN MaritalStatusId = 4 THEN 'W' ELSE '' END AS Marital, ISNULL(Occupation, '') 
                         AS Occupation, ISNULL(PriorPatientCode, '') AS OldPatient, '' AS ReferralRequired, ISNULL(SocialSecurityNumber, '') AS SocialSecurity, ISNULL(EmployerName, '') AS BusinessName, 
                         CASE WHEN IsHipaaConsentSigned = CONVERT(bit, 1) THEN 'T' WHEN IsHipaaConsentSigned = CONVERT(bit, 0) THEN 'F' ELSE CONVERT(nvarchar(1), NULL) END AS Hipaa, ISNULL(Ethnicity, 'Declined to Specify')As Ethnicity,ISNULL(Language, 'Declined to Specify')As Language, ISNULL(Race, 
                         'Declined to Specify') AS Race, ISNULL(BusinessType, '') AS BusinessType, ISNULL(Address, '') AS Address, ISNULL(Suite, '') AS Suite, ISNULL(City, '') AS City, ISNULL(State, '') AS State, ISNULL(Zip, '') AS Zip, 
                         ISNULL(BusinessAddress, '') AS BusinessAddress, ISNULL(BusinessSuite, '') AS BusinessSuite, ISNULL(BusinessCity, '') AS BusinessCity, ISNULL(BusinessState, '') AS BusinessState, ISNULL(BusinessZip, '') 
                         AS BusinessZip, ISNULL(HomePhone, '') AS HomePhone, ISNULL(CellPhone, '') AS CellPhone, ISNULL(BusinessPhone, '') AS BusinessPhone, ISNULL(EmployerPhone, '') AS EmployerPhone, ISNULL
                             ((SELECT        TOP (1) COALESCE (ExternalProviderId, 0) AS Expr1
                                 FROM            model.PatientExternalProviders AS pep
                                 WHERE        (PatientId = p.Id) AND (IsReferringPhysician = 1)), '') AS ReferringPhysician, ISNULL
                             ((SELECT        TOP (1) COALESCE (ExternalProviderId, 0) AS Expr1
                                 FROM            model.PatientExternalProviders AS pep
                                 WHERE        (PatientId = p.Id) AND (IsPrimaryCarePhysician = 1)), '') AS PrimaryCarePhysician, ISNULL(ProfileComment1, '') AS ProfileComment1, ISNULL(ProfileComment2, '') AS ProfileComment2, 
                         ISNULL(Email, '') AS Email, '' AS FinancialAssignment, '' AS FinancialSignature, CONVERT(nvarchar(64), '') AS MedicareSecondary, CASE WHEN p.PreferredAddressId =
                             (SELECT        TOP 1 pad.Id
                               FROM            model.PatientAddresses pad
                               WHERE        pad.PatientId = p.PolicyPatientId
                               ORDER BY pad.Id) THEN 'Y' ELSE 'N' END AS BillParty, '' AS SecondBillParty, CONVERT(nvarchar(32), '') AS ReferralCatagory, CONVERT(nvarchar(MAX), '') AS EmergencyName, CONVERT(nvarchar(MAX), '') 
                         AS EmergencyPhone, '' AS EmergencyRel, PolicyPatientId, SecondPolicyPatientId, Relationship, SecondRelationship, PatType, CONVERT(int, NULL) AS BillingOffice, CONVERT(int, NULL) AS PostOpExpireDate, 
                         CONVERT(int, NULL) AS PostOpService, CONVERT(nvarchar(64), '') AS FinancialSignatureDate, '' AS Religion, '' AS SecondRelationshipArchived, CONVERT(int, NULL) AS SecondPolicyPatientIdArchived, 
                         '' AS RelationshipArchived, CONVERT(int, NULL) AS PolicyPatientIdArchived, '' AS BulkMailing, '' AS ContactType, '' AS NationalOrigin, ISNULL(SendStatements, 'Y') AS SendStatements, ISNULL(SendConsults, 'Y') 
                         AS SendConsults
FROM            (SELECT        Id, LastName, FirstName, DateOfBirth, BillingOrganizationId, DateOfDeath, DefaultUserId, EmployerName, GenderId, Honorific, IsClinical, IsCollectionLetterSuppressed, IsHipaaConsentSigned, 
                                                    MaritalStatusId, MiddleName, NickName, Occupation, PatientEmploymentStatusId, EthnicityId, LanguageId, PatientNationalOriginId, PatientReligionId, PatientStatusId, 
                                                    PaymentOfBenefitsToProviderId, Prefix, PriorFirstName, PriorLastName, PriorPatientCode, Salutation, SocialSecurityNumber, Suffix, ReleaseOfInformationCodeId, ReleaseOfInformationDateTime, 
                                                    UserDefinedLastUpdated, PreferredServiceLocationId, ActivationCode, SexId, SexualOrientationId, RaceAndEthnicityId, model.GetPairId(1, Id, 110000000) AS PrimaryCarePhysicianId, 
                                                    ISNULL(dbo.GetPolicyHolderPatientId(Id), Id) AS PolicyPatientId, ISNULL(dbo.GetSecondPolicyHolderPatientId(Id), 0) AS SecondPolicyPatientId, ISNULL(dbo.GetPolicyHolderRelationshipType(Id), 'Y') 
                                                    AS Relationship, ISNULL(dbo.GetSecondPolicyHolderRelationshipType(Id), '') AS SecondRelationship,
                                                        (SELECT        TOP (1) COALESCE (AreaCode, '') + ExchangeAndSuffix AS Expr1
                                                          FROM            model.PatientPhoneNumbers AS ppn
                                                          WHERE        (PatientId = p1.Id) AND (PatientPhoneNumberTypeId = 7)
                                                          ORDER BY OrdinalId) AS HomePhone,
                                                        (SELECT        TOP (1) COALESCE (AreaCode, '') + ExchangeAndSuffix AS Expr1
                                                          FROM            model.PatientPhoneNumbers AS ppn
                                                          WHERE        (PatientId = p1.Id) AND (PatientPhoneNumberTypeId = 3)
                                                          ORDER BY OrdinalId) AS CellPhone,
                                                        (SELECT        TOP (1) COALESCE (AreaCode, '') + ExchangeAndSuffix AS Expr1
                                                          FROM            model.PatientPhoneNumbers AS ppn
                                                          WHERE        (PatientId = p1.Id) AND (PatientPhoneNumberTypeId = 2)
                                                          ORDER BY OrdinalId) AS BusinessPhone,
                                                        (SELECT        TOP (1) COALESCE (AreaCode, '') + ExchangeAndSuffix AS Expr1
                                                          FROM            model.PatientPhoneNumbers AS ppn
                                                          WHERE        (PatientId = p1.Id) AND (OrdinalId = 1) AND (PatientPhoneNumberTypeId IN
                                                                                        (SELECT        Id
                                                                                          FROM            dbo.PracticeCodeTable AS pct
                                                                                          WHERE        (Code = 'Employer') AND (ReferenceType = 'PhoneTypePatient')))) AS EmployerPhone,
                                                        (SELECT        TOP (1) Line1
                                                          FROM            model.PatientAddresses AS pa
                                                          WHERE        (PatientId = p1.Id) AND (PatientAddressTypeId = 1) AND (OrdinalId = 1)) AS Address,
                                                        (SELECT        TOP (1) Line2
                                                          FROM            model.PatientAddresses AS pa
                                                          WHERE        (PatientId = p1.Id) AND (PatientAddressTypeId = 1) AND (OrdinalId = 1)) AS Suite,
                                                        (SELECT        TOP (1) City
                                                          FROM            model.PatientAddresses AS pa
                                                          WHERE        (PatientId = p1.Id) AND (PatientAddressTypeId = 1) AND (OrdinalId = 1)) AS City,
                                                        (SELECT        TOP (1) Abbreviation
                                                          FROM            model.StateOrProvinces AS sp
                                                          WHERE        (Id =
                                                                                        (SELECT        TOP (1) StateOrProvinceId
                                                                                          FROM            model.PatientAddresses AS pa
                                                                                          WHERE        (PatientId = p1.Id) AND (PatientAddressTypeId = 1) AND (OrdinalId = 1)))) AS State,
                                                        (SELECT        TOP (1) PostalCode
                                                          FROM            model.PatientAddresses AS pa
                                                          WHERE        (PatientId = p1.Id) AND (PatientAddressTypeId = 1) AND (OrdinalId = 1)) AS Zip,
                                                        (SELECT        TOP (1) Line1
                                                          FROM            model.PatientAddresses AS pa
                                                          WHERE        (PatientId = p1.Id) AND (PatientAddressTypeId = 2) AND (OrdinalId = 1)) AS BusinessAddress,
                                                        (SELECT        TOP (1) Line2
                                                          FROM            model.PatientAddresses AS pa
                                                          WHERE        (PatientId = p1.Id) AND (PatientAddressTypeId = 2) AND (OrdinalId = 1)) AS BusinessSuite,
                                                        (SELECT        TOP (1) City
                                                          FROM            model.PatientAddresses AS pa
                                                          WHERE        (PatientId = p1.Id) AND (PatientAddressTypeId = 2) AND (OrdinalId = 1)) AS BusinessCity,
                                                        (SELECT        TOP (1) Abbreviation
                                                          FROM            model.StateOrProvinces AS sp
                                                          WHERE        (Id =
                                                                                        (SELECT        TOP (1) StateOrProvinceId
                                                                                          FROM            model.PatientAddresses AS pa
                                                                                          WHERE        (PatientId = p1.Id) AND (PatientAddressTypeId = 2) AND (OrdinalId = 1)))) AS BusinessState,
                                                        (SELECT        TOP (1) PostalCode
                                                          FROM            model.PatientAddresses AS pa
                                                          WHERE        (PatientId = p1.Id) AND (PatientAddressTypeId = 2) AND (OrdinalId = 1)) AS BusinessZip,
                                                        (SELECT        TOP (1) Value
                                                          FROM            model.PatientEmailAddresses AS pea
                                                          WHERE        (PatientId = p1.Id) AND (OrdinalId = 1)) AS Email,
                                                        (SELECT        TOP (1) Value
                                                          FROM            model.PatientComments AS pc
                                                          WHERE        (PatientCommentTypeId = 2) AND (PatientId = p1.Id)
                                                          ORDER BY Id DESC) AS ProfileComment1,
                                                        (SELECT        TOP (1) Comment
                                                          FROM            model.PatientReferralSources AS rs
                                                          WHERE        (PatientId = p1.Id) AND (Comment IS NOT NULL)) AS ProfileComment2,
                                                        (SELECT        TOP (1) COALESCE (ExternalProviderId, 0) AS Expr1
                                                          FROM            model.PatientExternalProviders AS pep
                                                          WHERE        (Id = p1.Id)) AS ReferringPhysician,
                                                        (SELECT        TOP (1) PatientAddressId
                                                          FROM            model.PatientCommunicationPreferences AS pcp
                                                          WHERE        (PatientId = p1.Id) AND (PatientCommunicationTypeId = 3)) AS PreferredAddressId,
                                                        (SELECT        TOP (1) DisplayName
                                                          FROM            model.Tags AS pt
                                                          WHERE        (Id =
                                                                                        (SELECT        TOP (1) TagId
                                                                                          FROM            model.PatientTags AS ppt
                                                                                          WHERE        (PatientId = p1.Id) AND (OrdinalId = 1)))) AS PatType,

														(SELECT        TOP (1) Name
                                                          FROM            model.Ethnicities AS pe
														  WHERE        (Id = (SELECT TOP (1) EthnicityId   
                                                          FROM            model.PatientEthnicities 
                                                          WHERE        (PatientId = p1.Id)) )) AS Ethnicity,
                                                        (SELECT        TOP (1) SUBSTRING(Name, 1, 10) AS Expr1
                                                          FROM            model.Languages AS pl
														  WHERE        (Id =(SELECT TOP (1) LanguageId   
                                                          FROM            model.PatientLanguages 
                                                          WHERE        (PatientId = p1.Id)) )) AS Language,
                                                        (SELECT        TOP (1) Name
                                                          FROM            model.Races
                                                          WHERE        (Id =
                                                                                        (SELECT        TOP (1) Races_Id
                                                                                          FROM            model.PatientRace AS ppr
                                                                                          WHERE        (Patients_Id = p1.Id)))) AS Race,
                                                        --(SELECT        TOP (1) Name
                                                        --  FROM            model.Ethnicities AS pe
                                                        --  WHERE        (Id = p1.EthnicityId)) AS Ethnicity,
                                                        --(SELECT        TOP (1) SUBSTRING(Name, 1, 10) AS Expr1
                                                        --  FROM            model.Languages AS pl
                                                        --  WHERE        (Id = p1.LanguageId)) AS Language,
                                                        --(SELECT        TOP (1) Name
                                                        --  FROM            model.Races
                                                        --  WHERE        (Id =
                                                        --                                (SELECT        TOP (1) Races_Id
                                                        --                                  FROM            model.PatientRace AS ppr
                                                        --                                  WHERE        (Patients_Id = p1.Id)))) AS Race,
                                                        (SELECT        TOP (1) SUBSTRING(Name, 1, 1) AS Expr1
                                                          FROM            model.PatientEmploymentStatus AS pes
                                                          WHERE        (Id = p1.PatientEmploymentStatusId)) AS BusinessType,
                                                        (SELECT        TOP (1) CASE IsOptOut WHEN CONVERT(bit, 0) THEN 'Y' ELSE 'N' END AS Expr1
                                                          FROM            model.PatientCommunicationPreferences AS pcp
                                                          WHERE        (PatientId = p1.Id) AND (PatientCommunicationTypeId = 8)) AS SendConsults,
                                                        (SELECT        TOP (1) CASE IsOptOut WHEN CONVERT(bit, 0) THEN 'Y' ELSE 'N' END AS Expr1
                                                          FROM            model.PatientCommunicationPreferences AS pcp
                                                          WHERE        (PatientId = p1.Id) AND (PatientCommunicationTypeId = 3)) AS SendStatements
                          FROM            model.Patients AS p1) AS p
GO

SET ANSI_PADDING ON
GO
