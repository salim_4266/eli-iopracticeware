--ERP changes for VersionInfo & PracticeOnHL7

If not exists(Select * From model.Applicationsettings Where Name in('PracticeVersion','PracticeOnHL7'))
Begin
SET ANSI_PADDING ON

Insert into model.applicationsettings (value,Name, MachineName, UserId) values ('2014','PracticeVersion', NULL, NULL)  
Insert into model.applicationsettings (value,Name, MachineName, UserId) values ('No','PracticeOnHL7', NULL, NULL)

End
GO

SET ANSI_PADDING ON
GO