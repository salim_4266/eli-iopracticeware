/****** Object: Table [dbo].[CDS_UserPermissions] ******/
-----------------------------------------------------------------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CDS_UserPermissions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CDS_UserPermissions](
	[ResourceId] [numeric](18, 0) NOT NULL,
	[ResourceType] [nvarchar](10) NOT NULL,
	[CDSConfiguration] [bit] NOT NULL,
	[Alerts] [bit] NOT NULL,
	[Info] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.CDS_UserPermission] PRIMARY KEY CLUSTERED 
(
	[ResourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientLabTestResults]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PatientLabTestResults](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[AppointmentId] [numeric](18, 0) NOT NULL,
	[LabOrderId] [numeric](18, 0) NOT NULL,
	[LabTestResultId] [numeric](18, 0) NOT NULL,
	[Result] [nvarchar](100) NOT NULL,
	[Unit] [nvarchar](100) NOT NULL,
	[MinRange] [nvarchar](100) NOT NULL,
	[MaxRange] [nvarchar](100) NOT NULL,
	[Range] [nvarchar](100) NOT NULL,
	[Comment] [nvarchar](100) NULL,
	[IsArchieved] [bit] NULL,
 CONSTRAINT [PK_PatientLabTestResults] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [model].[PatientReconcileAllergy] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF Not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[PatientReconcileAllergy]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[PatientReconcileAllergy](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[AllergyType] [nvarchar](100) NULL,
	[AllergySubstance] [nvarchar](200) NULL,
	[AllergyReaction] [nvarchar](200) NULL,
	[Status] [nvarchar](50) NULL,
	[AllergySeverity] [nvarchar](200) NULL,
	[RxNorm] [nvarchar](200) NULL,
	[ReactionDate] [nvarchar](100) NULL,
	[AlleryStatus] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
End
GO
/****** Object:  Table [model].[PatientReconcileMedications]  ******/
-----------------------------------------------------------------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF Not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[PatientReconcileMedications]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[PatientReconcileMedications](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[StartDate] [datetime] NULL,
	[Frequency] [nvarchar](500) NULL,
	[DrugName] [nvarchar](500) NULL,
	[Status] [nvarchar](50) NULL,
	[Instructions] [nvarchar](500) NULL,
	[LengthOfUse] [nvarchar](200) NULL,
	[Quantity] [nvarchar](200) NULL,
	[Refills] [nvarchar](200) NULL,
	[RxDate] [nvarchar](200) NULL,
	[RxNorm] [nvarchar](200) NULL,
	[MedicationStatus] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [dbo].[EmergencyPermission] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmergencyPermission]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmergencyPermission](
	[PermissionId] [int] IDENTITY(1,1) NOT NULL,
	[ResourceId] [int] NULL,
	[ResourcePermission] [varchar](500) NULL,
	[NewPermission] [varchar](500) NULL,
	[ModifiedDate] [datetime] NULL,
	[EnterdDate] [datetime] NULL,
	[IsAccess] [bit] NOT NULL CONSTRAINT [DF_EmergencyPermission_IsAccess]  DEFAULT 

((0)),
 CONSTRAINT [PK_EmergencyPermission] PRIMARY KEY CLUSTERED 
(
	[PermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [model].[PatientReconcileProblems]   ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[PatientReconcileProblems]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[PatientReconcileProblems](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[EffectiveDate] [datetime] NULL,
	[SnomedCT] [nvarchar](50) NULL,
	[DiagnosisCode] [nvarchar](50) NULL DEFAULT (''),
	[DiagnosisDescription] [nvarchar](2000) NULL,
	[Status] [nvarchar](50) NULL,
	[Eye] [nvarchar](500) NULL,
	[OnSetDate] [nvarchar](500) NULL,
	[ProblemStatus] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [dbo].[PatientImageOrders] ******/
-----------------------------------------------------------------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientImageOrders]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PatientImageOrders](
	[OrderId] [numeric](18, 0) IDENTITY(2,2) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[AppointmentId] [numeric](18, 0) NOT NULL,
	[CptCode] [nvarchar](10) NOT NULL,
	[OrderType] [nvarchar](10) NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModifiedBy] [numeric](18, 0) NOT NULL,
	[IsArchieved] [bit] NOT NULL,
	[OrderReason] [nvarchar](2000) NOT NULL,
	[OrderForm] [nvarchar](50) NULL,
 CONSTRAINT [PK_PatientImageOrders] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [dbo].[IE_RulesHistory] ******/
-----------------------------------------------------------------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IE_RulesHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[IE_RulesHistory](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RuleId] [numeric](18, 0) NOT NULL,
	[ModifyBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[ResourceDescription] [nvarchar](2000) NULL,
	[BiblographicsCitation] [nvarchar](2000) NULL,
 CONSTRAINT [PK_IE_RulesHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [model].[UserEmergencyPermissions]  ******/
-----------------------------------------------------------------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[UserEmergencyPermissions]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[UserEmergencyPermissions](
	[IsDenied] [bit] NULL,
	[UserId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
 CONSTRAINT [PK_UserEmergencyPermissions] PRIMARY KEY CLUSTERED 
(
	[PermissionId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO


/****** Object:  Table [MVE].[PP_SecureMessagesLogs]  ******/
-----------------------------------------------------------------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_SecureMessagesLogs]') AND type in (N'U'))
BEGIN
CREATE TABLE [MVE].[PP_SecureMessagesLogs](
	[LogId] [uniqueidentifier] NOT NULL,
	[RecipientId] [numeric](18, 0) NOT NULL,
	[PatientId] [numeric](18, 0) NULL,
	[IsSent2Repersenative] [bit] NOT NULL,
	[RepersentativeId] [numeric](18, 0) NULL,
	[MessageSubject] [nvarchar](200) NOT NULL,
	[MessageSentTime] [datetime] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[IsResponseSent] [bit] NULL,
 CONSTRAINT [PK_PP_SecureMessagesLogs] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO


/****** Object:  Table [dbo].[AllergyReactions] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AllergyReactions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AllergyReactions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AllergyReaction] [varchar](50) NULL,
	[Snomed_ConceptId] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[GroupFilterCategory] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GroupFilterCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GroupFilterCategory](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](200) NULL,
	[LogCategoryId] [int] NULL,
	[LogCategoryName] [nvarchar](200) NULL,
	[IsAudit] [bit] NULL,
 CONSTRAINT [PK_dbo.FilterCatagory] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [model].[PatientReconcilePracticeInformation] ******/
-----------------------------------------------------------------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[PatientReconcilePracticeInformation]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[PatientReconcilePracticeInformation](
	[Id] [numeric](18, 0) IDENTITY(1,2) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[ProviderName] [nvarchar](100) NULL,
	[PhoneNumber] [nvarchar](100) NULL,
	[AddressLine1] [nvarchar](200) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](500) NULL,
	[Zip] [nvarchar](50) NULL,
	[CreatedDateTime] [datetime] NULL,
	[Status] [nvarchar](50) NULL,
	[ExternalSystemMessagesId] [nvarchar](50) NULL,
	[Sender_FirstName] [varchar](25) NULL,
	[Sender_LastName] [varchar](25) NULL,
	[Sender_PracticeName] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
End
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[CDSRoles] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CDSRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CDSRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ResourceType] [varchar](10) NULL,
	[CDSConfiguration] [bit] NULL,
	[Alerts] [bit] NULL,
	[Info] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[PatientHistoryStatus] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientHistoryStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PatientHistoryStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[AppointmentId] [int] NOT NULL,
	[IsNote] [bit] NOT NULL,
	[ComplaintId] [int] NOT NULL,
	[HistoryType] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

/****** Object:  Table [dbo].[tblValidatedNDCList] ******/
-----------------------------------------------------------------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EVENTRAISE_ENUMERATION]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EVENTRAISE_ENUMERATION](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ResourcesPermissionDetail] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ResourcesPermissionDetail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ResourcesPermissionDetail](
	[id] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[ResourceIndex] [int] NULL,
 CONSTRAINT [PK_ResourcesPermissionDetail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[IE_RulesPermissions] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IE_RulesPermissions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[IE_RulesPermissions](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RuleId] [numeric](18, 0) NOT NULL,
	[UserId] [numeric](18, 0) NOT NULL,
	[Intervention] [bit] NOT NULL,
	[Information] [bit] NOT NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_IE_RulesPermissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [model].[PatientRepresentatives]   ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[PatientRepresentatives]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[PatientRepresentatives](
	[PatientRepresentativeId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[Salutation] [varchar](20) NULL,
	[FirstName] [varchar](35) NULL,
	[LastName] [varchar](35) NULL,
	[MidleName] [varchar](35) NULL,
	[Relationship] [varchar](35) NULL,
	[RelationshipCode] [varchar](6) NULL,
	[RelationshipCodeSystem] [varchar](25) NULL,
	[RelationshipCodeSystemName] [varchar](50) NULL,
	[HomePhone] [varchar](15) NULL,
	[WorkPhone] [varchar](15) NULL,
	[CellPhone] [varchar](15) NULL,
	[Addressline1] [varchar](100) NULL,
	[Addressline2] [varchar](100) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Zip] [varchar](10) NULL,
	[County] [varchar](50) NULL,
	[CountyCode] [varchar](5) NULL,
	[Country] [varchar](50) NULL,
	[DateofBirth] [datetime] NULL,
	[Status] [varchar](1) NULL,
	[IsPortalActivated] [bit] NULL,
	[Email] [varchar](400) NULL,
PRIMARY KEY CLUSTERED 
(
	[PatientRepresentativeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [model].[PatientProblemList]  ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[PatientProblemList]') AND type in (N'U'))
BEGIN
  CREATE TABLE [model].[PatientProblemList](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PatientId] [bigint] NULL,
	[AppointmentId] [bigint] NULL,
	[AppointmentDate] [datetime] NULL,
	[ConceptID] [bigint] NULL,
	[Term] [varchar](500) NULL,
	[ICD10] [varchar](100) NULL,
	[ICD10DESCR] [varchar](255) NULL,
	[COMMENTS] [varchar](55) NULL,
	[ResourceName] [varchar](100) NULL,
	[OnsetDate] [datetime] NULL,
	[LastModifyDate] [datetime] NULL,
	[Status] [varchar](10) NULL,
	[ResourceId] [int] NULL,
	[EnteredDate] [datetime] NULL,
	[ResolvedDate] [datetime] NULL,
 CONSTRAINT [PK_model.PatientProblemList] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End
GO

SET ANSI_PADDING OFF
GO

If Exists(select * from information_schema.columns where table_name = 'PatientProblemList' 
and column_name = 'ResourceId' 
and Table_schema = 'model'
and column_default is NULL)
ALTER TABLE [model].[PatientProblemList] 
ADD  CONSTRAINT [DF_model.PatientProblemList_ResourceId]  DEFAULT (NULL) FOR [ResourceId]
GO

If Exists(select * from information_schema.columns where table_name = 'PatientProblemList' 
and column_name = 'EnteredDate' 
and Table_schema = 'model'
and column_default is NULL)
ALTER TABLE [model].[PatientProblemList] 
ADD  CONSTRAINT [DF_model.PatientProblemList_EnteredDate]  DEFAULT (NULL) FOR [EnteredDate]
GO



/****** Object:  Table [model].[ProceduresDetail]  ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[ProceduresDetail]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[ProceduresDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [bigint] NULL,
	[AppointmentId] [bigint] NULL,
	[AppointmentDate] [varchar](20) NULL,
	[EnteredDate] [datetime] NULL DEFAULT (NULL),
	[ConceptID] [bigint] NULL,
	[Term] [varchar](500) NULL,
	[ProcedureDetail] [varchar](500) NULL,
	[Status] [varchar](10) NULL,
	[ResourceId] [int] NULL DEFAULT (NULL),
	[LastModifyDate] [datetime] NULL,
	[health] [varchar](500) NULL DEFAULT (NULL),
	[reason] [varchar](500) NULL DEFAULT (NULL),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientLabOrders]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PatientLabOrders](
	[OrderId] [numeric](18, 0) IDENTITY(1,2) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[AppId] [numeric](18, 0) NOT NULL,
	[LoincNumber] [nvarchar](10) NOT NULL,
	[OrderType] [nvarchar](10) NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModifiedBy] [numeric](18, 0) NOT NULL,
	[IsArchieved] [bit] NOT NULL,
	[OrderReason] [nvarchar](2000) NULL,
 CONSTRAINT [PK_PatientLabOrders] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PatientImplantableDevices]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PatientImplantableDevices](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[AppointmentId] [numeric](18, 0) NOT NULL,
	[ImplantableDeviceId] [numeric](18, 0) NOT NULL,
	[Status] [int] NOT NULL,
	[RecordDate] [datetime] NOT NULL,
	[ImplantationDate] [datetime] NULL,
	[Laternity] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.PatientImplantableDevices] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrderForms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OrderForms](
	[ORDERTESTKEY] [int] NOT NULL,
	[SHORTCUT] [nvarchar](255) NULL,
	[DESCRIPTION1] [nvarchar](255) NULL,
	[STATUS] [nvarchar](255) NULL,
	[COMMENTS] [nvarchar](255) NULL,
 CONSTRAINT [PK_OrderTestKeyId] PRIMARY KEY CLUSTERED 
(
	[ORDERTESTKEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HashcodeIntegrity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HashcodeIntegrity](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FromProvider] [bigint] NULL,
	[ToProvider] [bigint] NULL,
	[FileName] [varchar](300) NULL,
	[Hashcode] [nvarchar](max) NULL,
	[Filenamexml] [nvarchar](max) NULL,
	[DateTime] [datetime] NULL,
	[IsArchieve] [bit] NULL,
	[FileByteAry] [varbinary](max) NULL,
 CONSTRAINT [PK_HashcodeIntegrity] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[SexType_Enumeration]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[SexType_Enumeration](
	[Id] [numeric](18, 0) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_SexType_Enumeration] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [model].[SexualOrientationType_Enumeration] ******/
-----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[SexualOrientationType_Enumeration]') AND type in (N'U'))
BEGIN
CREATE TABLE [model].[SexualOrientationType_Enumeration](
	[Id] [numeric](18, 0) NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
	[SNOMEDCT] [nvarchar](50) NULL,
	[HL7Code] [nvarchar](50) NULL,
	[IsArchived] [nvarchar](50) NULL,
 CONSTRAINT [PK_model.SexualOrientationType_Enumeration] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS 

= ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[labmaster]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[labmaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ReferenceType] [nvarchar](100) NOT NULL,
	[Type] [nvarchar](10) NULL,
	[Code] [nvarchar](20) NULL
) ON [PRIMARY]
END
GO