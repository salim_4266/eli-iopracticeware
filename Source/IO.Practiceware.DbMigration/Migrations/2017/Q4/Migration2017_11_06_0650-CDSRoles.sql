SET QUOTED_IDENTIFIER OFF 
SET IDENTITY_INSERT [dbo].[CDSRoles] ON 

INSERT INTO [dbo].[CDSRoles]([Id],[ResourceType],[CDSConfiguration],[Alerts],[Info]) 
VALUES (1,'O',1,1,1)

INSERT INTO [dbo].[CDSRoles]([Id],[ResourceType],[CDSConfiguration],[Alerts],[Info]) 
VALUES (2,'D',1,1,1)

INSERT INTO [dbo].[CDSRoles]([Id],[ResourceType],[CDSConfiguration],[Alerts],[Info]) 
VALUES (3,'S',1,0,0)

INSERT INTO [dbo].[CDSRoles]([Id],[ResourceType],[CDSConfiguration],[Alerts],[Info]) 
VALUES (4,'F',1,1,0)

INSERT INTO [dbo].[CDSRoles]([Id],[ResourceType],[CDSConfiguration],[Alerts],[Info]) 
VALUES (5,'T',0,0,0)

SET IDENTITY_INSERT [dbo].[CDSRoles] OFF 

SET ANSI_PADDING ON
GO
