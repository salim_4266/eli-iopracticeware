IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'AddUpdateDrFirstConfigurationDetails')) 
Drop Procedure AddUpdateDrFirstConfigurationDetails 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

/****** Object:  StoredProcedure [dbo].[ACI_ConvertToJson_JSON16_MedicationsOrder]    Script Date: 06/12/2018 16:45:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[AddUpdateDrFirstConfigurationDetails] 
( @Erxupload VARCHAR(500) ,
 @Erxdownload VARCHAR(500) ,
 @ErxSso VARCHAR(500),
@ErxPortalheader VARCHAR(500),
@Erxcredentials VARCHAR(500))
AS BEGIN


IF (SELECT COUNT(*) FROM MODEL.ApplicationSettings Where Name in('ERxStagingPortalHeader','ERxStagingSSOUrl','ERxStagingDownloadUrl','ERxStagingUploadUrl','ERxCredentials'))  > 0
BEGIN
UPDATE MODEL.ApplicationSettings Set Value =@Erxupload Where Name  ='ERxStagingUploadUrl'
UPDATE MODEL.ApplicationSettings Set Value =@Erxdownload Where Name  ='ERxStagingDownloadUrl' 
UPDATE MODEL.ApplicationSettings Set Value =@ErxSso Where Name  ='ERxStagingSSOUrl'
UPDATE MODEL.ApplicationSettings Set Value =@ErxPortalheader Where Name  ='ERxStagingPortalHeader'
UPDATE MODEL.ApplicationSettings Set Value =@Erxcredentials Where Name  ='ErxCredentials'

End
ELSE

BEGIN
Insert Into MODEL.ApplicationSettings (Value,Name,UserId) Values (@Erxupload,'ERxStagingUploadUrl',2)
Insert Into MODEL.ApplicationSettings (Value,Name,UserId) Values (@Erxdownload,'ERxStagingDownloadUrl',2)
Insert Into MODEL.ApplicationSettings (Value,Name,UserId) Values (@ErxSso,'ERxStagingSSOUrl',2)
Insert Into MODEL.ApplicationSettings (Value,Name,UserId) Values (@ErxPortalheader,'ERxStagingPortalHeader',2)
Insert Into MODEL.ApplicationSettings (Value,Name,UserId) Values (@Erxcredentials ,'ErxCredentials',2)                              
END
END

GO


