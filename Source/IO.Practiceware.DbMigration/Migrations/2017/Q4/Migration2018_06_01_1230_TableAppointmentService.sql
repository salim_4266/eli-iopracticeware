

/****** Object:  Table [dbo].[AppointmentServiceUnit]    Script Date: 6/3/2018 4:06:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AppointmentServiceUnit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AppointmentServiceUnit](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PatientId] [numeric](18, 0) NULL,
	[AppointmentId] [numeric](18, 0) NULL,
	[Code] [nvarchar](20) NULL,
	[Unit] [int] NULL,
	[ServiceModifiers] [nvarchar](10) NULL,
 CONSTRAINT [PK_PatientProceduresServiceUnit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO


