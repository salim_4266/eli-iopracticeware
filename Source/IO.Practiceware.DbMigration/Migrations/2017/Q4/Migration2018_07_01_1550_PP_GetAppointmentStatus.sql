

/****** Object:  StoredProcedure [MVE].[PP_GetAppointmentStatus]    Script Date: 06/29/2018 13:33:53 ******/
IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_GetAppointmentStatus]')) 
Drop Procedure [MVE].[PP_GetAppointmentStatus] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  



 CREATE Procedure [MVE].[PP_GetAppointmentStatus] @ActionId int  
 As   
 Begin  
   Select Case(Name) When 'CancelOffice' Then 'Cancel' Else Name End as Name, Id as ExternalId, Case(Name) When 'CancelOffice' Then 1 Else 0 End as IsCanceled , 0 as IsRescheduled, Case(Name) When 'Pending' Then 1 Else 0 End as IsConfirmed , Case(Name) When 'Discharged' Then 1 When 'Checkout' Then 1 Else 0 End as IsCheckedOut from [model].[EncounterStatus_Enumeration] with (nolock) Where Id in (1,6,7,8) and Id NOT in (Select distinct ExternalId from MVE.PP_PortalQueueResponse PQR with (nolock) inner join MVE.PP_ActionTypeLookUps A with (nolock) on PQR.ActionTypeLookupId = A.ActionId Where PQR.ActionTypeLookupId=@ActionId  Union Select distinct ExternalId from MVE.PP_PortalQueueException PQR with (nolock) inner join MVE.PP_ActionTypeLookUps A with (nolock) on PQR.ActionTypeLookupId= A.ActionId Where PQR.ActionTypeLookupId=@ActionId and ProcessCount > 4)  
 End  

GO

