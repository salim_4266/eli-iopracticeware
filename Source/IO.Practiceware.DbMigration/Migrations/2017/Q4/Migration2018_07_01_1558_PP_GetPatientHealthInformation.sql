

/****** Object:  StoredProcedure [MVE].[PP_GetPatientHealthInformation]    Script Date: 06/29/2018 13:39:41 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_GetPatientHealthInformation]')) 
Drop Procedure [MVE].[PP_GetPatientHealthInformation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 

CREATE Procedure [MVE].[PP_GetPatientHealthInformation](@ActionId Int)
As 
Begin

   Select distinct Top(500) esm.CREATEDDATETIME, p.Id as PatientExternalId, esm.Id as ExternalId, esm.Value as CDAXml, (Select Top(1) ServiceLocationId From model.Encounters Where PatientId = P.Id order by Id Desc) as LocationExternalId, (Select Top(1) ResourceId1 From dbo.Appointments Where PatientId = P.Id order by P.Id Desc) as DoctorExternalId from model.ExternalSystemMessages esm with (nolock) INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpe with (nolock) on esm.value != '' and esmpe.ExternalSystemMessageId = esm.Id INNER JOIN model.ExternalSystemExternalSystemMessageTypes esesm with (nolock) on esesm.Id = esm.ExternalSystemExternalSystemMessageTypeId INNER JOIN model.ExternalSystemMessageTypes esmt with (nolock) on esmt.Id = esesm.ExternalSystemMessageTypeId INNER JOIN model.PracticeRepositoryEntities pe with (nolock) on pe.Id = esmpe.PracticeRepositoryEntityId INNER JOIN model.Patients p with (nolock) on p.Id = esmpe.PracticeRepositoryEntityKey INNER JOIN MVE.PP_PortalQueue pqx with (nolock) on pqx.PatientNo = p.Id and pqx.ActionTypeLookupId = 97 WHERE esm.ExternalSystemMessageProcessingStateId IN (1,3) AND esmt.Id IN(28,29) AND pe.Id IN(3) AND ((esm.Value IS NOT NULL OR RTRIM(LTRIM(esm.Value)) <> '') AND (RTRIM((LTRIM(esm.Error))) IS NULL OR RTRIM((LTRIM(esm.Error))) = '' OR RTRIM((LTRIM(esm.Error))) LIKE 'NO HANDLER%')) AND esm.ProcessingAttemptCount <= 10 AND esesm.ExternalSystemId IN(SELECT ID FROM model.ExternalSystems WHERE Name = 'Omedix') AND ExternalSystemMessageId = esm.Id and Esm.Id NOT IN (SELECT ExternalId FROM [MVE].[PP_PortalQueueResponse] Where ActionTypeLookupId = @ActionId Union SELECT ExternalId FROM [MVE].[PP_PortalQueueException] Where ActionTypeLookupId = @ActionId and ProcessCount >= 5) and Cast(esm.CREATEDDATETIME as Date) > '2018-03-31' 
ORDER BY esm.CREATEDDATETIME ASC  

End

GO

