

/****** Object:  StoredProcedure [MVE].[PP_GetCancelAppointments]    Script Date: 06/29/2018 13:34:45 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_GetCancelAppointments]')) 
Drop Procedure [MVE].[PP_GetCancelAppointments] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  


 CREATE Procedure [MVE].[PP_GetCancelAppointments] @NewAppointmentActionId int, @CancelAppointmentActionId int, @PatientActionId int
 As   
 Begin  
  SELECT Distinct Top(100) EN.serviceLocationId as LocationExternalId, AP.Resourceid1 as DoctorExternalId, AP.Patientid as PatientExternalId, 8 as  AppointmentStatusExternalId, '' as CancelReason, EN.startDatetime as Start, EN.EndDateTime as [End], En.id as ExternalId FROM dbo.Appointments AP with (nolock) Inner Join  model.Encounters EN with (nolock) ON Ap.EncounterID=EN.id Inner Join MVE.PP_PortalQueueResponse PQR with (nolock) on PQR.ExternalId = AP.PatientId and 
   PQR.ActionTypeLookupId = @PatientActionId Where AP.AppDate > 20170331 and AP.Resourceid1 <> 0 and EN.EncounterStatusId > 7 
   and En.id NOT IN (Select distinct ExternalId from MVE.PP_PortalQueueResponse PQR with (nolock) Where PQR.ActionTypeLookupId=@CancelAppointmentActionId and PQR.ExternalId = En.Id Union Select distinct ExternalId from MVE.PP_PortalQueueException PQR with (nolock) Where PQR.ActionTypeLookupId=@CancelAppointmentActionId and PQR.ExternalId = En.Id and ProcessCount >= 5)
End

GO


