ALTER VIEW [dbo].[VIEW_ACI_JSON24_SURGICAL_PRE_OPERATIVE_STATUS_EVENT_Procedure_Medication]

AS
/*	SELECT DISTINCT
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		ISNULL((Select top (1) RxCUI from [fdb].[RXCUI2MEDID] where medid = nc.medid),'00000')AS Code, 

		CASE WHEN pc.FindingDetail like 'RX-8/%-2/%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') 
			ELSE '' 
		END  AS Description, 
		'RxNorm' AS CodeSystem,
		'2.16.840.1.113883.6.88' AS CodeSystemName
	FROM
	dbo.patientclinical pc WITH(NOLOCK)   left join fdb.tblcompositedrug nc WITH(NOLOCK) on Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') =	nc.MED_ROUTED_DF_MED_ID_DESC and REPLACE(SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail, CHARINDEX('-2/', pc.FindingDetail) + 4) + 1, CHARINDEX(')', pc.FindingDetail, CHARINDEX('(',pc.FindingDetail, CHARINDEX('-2/', pc.FindingDetail) + 4)) - CHARINDEX('(', pc.FindingDetail, CHARINDEX('-2/', pc.FindingDetail) + 4) - 2),' ','') = nc.MED_STRENGTH
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%' 
*/
--Changed By Ayush - 2018 Nov
SELECT 

		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		CASE
                WHEN NC.MEDID = '' THEN 'UNCODIFIED'
                ELSE ISNULL(CAST(NC.MEDID AS VARCHAR(15)),'00000')
        END AS Code, 

		CASE WHEN pc.FindingDetail like 'RX-8/%-2/%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') 
			ELSE '' 
		END  AS Description, 
		'RxNorm' AS CodeSystem,
		'2.16.840.1.113883.6.88' AS CodeSystemName
	FROM
	 dbo.patientclinical pc WITH(NOLOCK) left join fdb.tblcompositedrug NC WITH(NOLOCK)
	on Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') = nc.MED_ROUTED_DF_MED_ID_DESC
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%'
	
GO