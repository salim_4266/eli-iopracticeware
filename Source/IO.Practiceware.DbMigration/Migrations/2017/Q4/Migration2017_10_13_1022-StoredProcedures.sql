/****** Object:  StoredProcedure [model].[USP_GetICD10_SnomedCT] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[USP_GetICD10_SnomedCT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [model].[USP_GetICD10_SnomedCT]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[USP_GetICD10_SnomedCT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE    PROCEDURE [model].[USP_GetICD10_SnomedCT]

 @IcdOrDiagnosis VARCHAR(100)='''',

 @Flag VARCHAR(10)='''',

 @Icd10Id  VARCHAR(100)=''''

AS

BEGIN

 -- SET NOCOUNT ON added to prevent extra result sets from

 -- interfering with SELECT statements.

 SET NOCOUNT ON;

    IF(@Flag=''icd10'')

 BEGIN

  IF(@IcdOrDiagnosis<>'''')

   SELECT  Distinct ICD10 , ICD10DESCR AS Diagnosis, CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as [Sataus],  COMMENTS FROM dbo.SNOMED_2016  WHERE ICD10 LIke ''%''+ @IcdOrDiagnosis+''%''

  ELSE

   SELECT  Distinct  Top 100 ICD10 , ICD10DESCR AS Diagnosis, CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as [Sataus],  COMMENTS FROM dbo.SNOMED_2016  WHERE ICD10<>''''



 END

 ELSE IF(@Flag=''diagnosis'')

 BEGIN

  IF(@IcdOrDiagnosis<>'''')

   SELECT  Distinct ICD10 , ICD10DESCR AS Diagnosis, CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as [Sataus], COMMENTS FROM dbo.SNOMED_2016  WHERE ICD10DESCR LIke +''%''+ @IcdOrDiagnosis +''%''

  ELSE

   SELECT  Distinct  Top 100 ICD10 , ICD10DESCR AS Diagnosis, CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as [Sataus], COMMENTS FROM dbo.SNOMED_2016 WHERE ICD10 <>''''

 End 

 ELSE IF(@Flag=''all'')

 BEGIN

  SELECT  Distinct  Top 100 ICD10 , ICD10DESCR AS Diagnosis , CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as [Sataus], COMMENTS FROM dbo.SNOMED_2016  WHERE ICD10<>''''

 END

 ELSE IF(@Flag=''snomedbyid'')

 BEGIN

  IF(@Icd10Id<>'''')

  BEGIN

   --SELECT ConceptID, Term  FROM dbo.SNOMED_2016 where ConceptID=@Icd10Id

   SELECT Distinct ConceptID, Term  FROM dbo.SNOMED_2016 where ConceptID Like +''%''+ @Icd10Id +''%''

--   SELECT ConceptID, Term ,ICD10 , ICD10DESCR AS Diagnosis,  CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as [Sataus], COMMENTS  FROM dbo.SNOMED_2016 where ConceptID=@snomedid

  END

 END

 ELSE IF(@Flag=''snomeddiag'')

 BEGIN

  IF(@Icd10Id<>'''')

  BEGIN

--   SELECT ConceptID, Term  FROM dbo.SNOMED_2016 where ConceptID=@snomedid
   SELECT ICD10 , ICD10DESCR AS Diagnosis,  CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as [Sataus], COMMENTS  FROM dbo.SNOMED_2016 where ICD10 IS NOT NULL AND ICD10 <> '''' and ICD10DESCR IS NOT NULL AND ICD10DESCR <> ''''  AND  ConceptID Like +''%''+ @Icd10Id +''%''
--   SELECT ICD10 , ICD10DESCR AS Diagnosis,  CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as [Sataus], COMMENTS  FROM dbo.SNOMED_2016 where  ConceptID Like +''%''+ @Icd10Id +''%''--=@Icd10Id

  END

 END

 ELSE IF(@Icd10Id<>'''')

 BEGIN

  SELECT ConceptID, Term   FROM dbo.SNOMED_2016 where icd10=@Icd10Id
  END
 END' 

END
GO

/****** Object:  StoredProcedure [model].[USP_GetSnomedDetails] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[USP_GetSnomedDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [model].[USP_GetSnomedDetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetSnomedDetails]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [model].[USP_GetSnomedDetails]
	@snomed VARCHAR(100)='''',
	@Flag VARCHAR(10)=''''	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF(@Flag=''snomed'')
	BEGIN
		SELECT  DISTINCT ConceptID as Col1, Term  as Col2  , CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as Col3 FROM dbo.SNOMED_2016  WHERE ConceptID like ''%'' + @snomed + ''%''
	END
	ELSE IF(@Flag=''snomeddesc'')
	BEGIN
		IF(@snomed='''')
			SELECT  DISTINCT Top 100 ConceptID as Col1, Term  as Col2  , CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as  Col3 FROM dbo.SNOMED_2016 
		ELSE
			SELECT  DISTINCT ConceptID as Col1, Term  as Col2  , CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as  Col3 FROM dbo.SNOMED_2016  WHERE Term LIke ''%''+ @snomed +''%''


	End 
	ELSE IF(@Flag=''all'')
	BEGIN
		SELECT    DISTINCT Top 100  ConceptID as Col1, Term  as Col2 ,  CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as Col3 FROM dbo.SNOMED_2016 
	END
		
END' 

END
GO

/****** Object:  StoredProcedure [dbo].[USP_EmergencyPermission] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_EmergencyPermission]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_EmergencyPermission]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_EmergencyPermission]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE  [dbo].[USP_EmergencyPermission]
	
	@ResourceId int=0 ,
	@ResourcePermission VARCHAR(250)='''',	
	@EnterdDate Datetime =NULL,
	@Modifieddate datetime= NULL,
	@Flag VARCHAR(10) =''''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF(@Flag=''get'')
	BEGIN    
		SELECT  R.ResourceId, R.ResourceName, R.ResourcePermissions,R.ResourcePid, EnterdDate,IsAccess
		FROM Resources R  LEFT JOIN dbo.Emergencypermission EP ON  R.ResourceId=EP.ResourceId
		WHERE ResourceType Not in(''R'',''Z'',''X'')
	END
	ELSE IF(@Flag=''save'')
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM dbo.Emergencypermission WHERE ResourceId=@ResourceId) 
		BEGIN
		DECLARE @NewPermis VARCHAR(200)
		Set @NewPermis=''11001111110100100000111110000000111001011111111111101110000001011000110000000000000000000000000000011111110111000000000000000000''
		
		 INSERT INTO dbo.Emergencypermission ([ResourceId],[ResourcePermission],[NewPermission],[EnterdDate])
         VALUES(@ResourceId,@ResourcePermission,@NewPermis,@EnterdDate)

				
		END
	END
	ELSE IF(@Flag=''set'')
	BEGIN
		IF EXISTS(SELECT 1 FROM dbo.Emergencypermission WHERE ResourceId=@ResourceId) 
		BEGIN
			 BEGIN TRY
				BEGIN TRAN
					DECLARE @NewPermission VARCHAR(200)
					Set @NewPermission=''11001111110100100000111110000000111001011111111111101110000001011000110000000000000000000000000000011111110111000000000000000000''
		
					UPDATE Resources SET  ResourcePermissions=@NewPermission  WHERE Resourceid=@ResourceId
					UPDATE dbo.Emergencypermission Set IsAccess=1 WHERE ResourceId=@ResourceId			 			
				
					IF EXISTS(SELECT 1 FROM Model.UserEmergencyPermissions WHERE UserId=@ResourceId) 
					BEGIN
						DELETE from [model].UserEmergencyPermissions WHERE UserId=@ResourceId 
					END
					INSERT INTO Model.UserEmergencyPermissions (IsDenied,userid, PermissionId)
					SELECT IsDenied,userid, PermissionId from  [model].UserPermissions WHERE UserId=@ResourceId

					DELETE from [model].UserPermissions WHERE UserId=@ResourceId

					INSERT INTO Model.UserPermissions(IsDenied,userid, PermissionId)
					SELECT 0 as IsDenied, @ResourceId, id from  [model].[Permissions] 
										
					Select 1 as val
				 COMMIT TRAN
			 END TRY 
			 BEGIN CATCH
				ROLLBACK TRAN
			 END CATCH
		END
		else
			 Select 0 as val

	END
	ELSE IF(@Flag=''remove'')
	BEGIN
		IF EXISTS(SELECT 1 FROM dbo.Emergencypermission WHERE ResourceId=@ResourceId) 
		BEGIN
			BEGIN TRY
				BEGIN TRAN

					DECLARE @permission VARCHAR(500)
					SELECT @permission=[ResourcePermission] FROM dbo.Emergencypermission WHERE ResourceId=@ResourceId
				
					UPDATE Resources SET  ResourcePermissions=@permission  WHERE Resourceid=@ResourceId
					DELETE FROM  dbo.Emergencypermission  WHERE ResourceId=@ResourceId

					DELETE from [model].UserPermissions WHERE UserId=@ResourceId

					INSERT INTO Model.UserPermissions(IsDenied,userid, PermissionId)
					SELECT IsDenied,userid, PermissionId from  [model].UserEmergencyPermissions WHERE UserId=@ResourceId

				COMMIT TRAN
			 END TRY 
			 BEGIN CATCH
				ROLLBACK TRAN
			 END CATCH

		END
	END

END' 

END
GO

/****** Object:  StoredProcedure [model].[CCDA_PatientAllergies] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_PatientAllergies]') AND type in (N'P', N'PC'))
DROP PROCEDURE [model].[CCDA_PatientAllergies]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_PatientAllergies]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE Procedure [model].[CCDA_PatientAllergies] (@PatientId nvarchar(50))
As
Begin
 Select SUBSTRING(FindingDetail,0,CHARINDEX(''='',FindingDetail)) as Col1, SUBSTRING(APPDATE,0,5) + ''-'' + SUBSTRING(APPDATE,5,2) + ''-'' + SUBSTRING(APPDATE,7,2) as Col2, 
SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail)+1, LEN(pc.findingdetail)-CHARINDEX(''/'', REVERSE(pc.findingdetail))-CHARINDEX(''/'', pc.findingdetail)) as Col3, ''Active'' as Col4 From dbo.PatientClinical PC inner join dbo.Appointments A on PC.AppointmentId = A.AppointmentId
Where A.PatientId = @PatientId and ClinicalType = ''H'' and Symptom in (''/ANY DRUG ALLERGIES'',''/ALLERGIES ALL'')
End' 

END
GO

/****** Object:  StoredProcedure [model].[CCDA_PatientMedication] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_PatientMedication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [model].[CCDA_PatientMedication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_PatientMedication]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE Procedure [model].[CCDA_PatientMedication](@PatientId nvarchar(50))
As
Select 
 CASE WHEN pc.FindingDetail like ''RX-8/%-2/%''
  THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex(''-2/'', pc.findingdetail) - 6),''&'',''and''), ''+'',''Plus'') 
  ELSE '''' 
 END AS Col1,

 Right(pc.imagedescriptor,10) AS Col2,

 CASE substring(pc.findingdetail,charindex(''-3/'',pc.findingdetail)+4,(charindex('')'',(REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''-3/'', pc.findingdetail), LEN(pc.findingdetail)), ''-3/'', '''')))-2))
  WHEN ''AS PRESCRIBED''
  THEN ''''
  ELSE   substring(pc.findingdetail,charindex(''-3/'',pc.findingdetail)+4,(charindex('')'',(REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''-3/'', pc.findingdetail), LEN(pc.findingdetail)), ''-3/'', '''')))-2))
 END AS Col3,
 ''Active'' as Col4

From dbo.PatientClinical pc where pc.clinicaltype = ''A'' and findingdetail like ''RX-8%'' and pc.Status = ''A'' and pc.PatientId = @PatientId
and PC.Status <> ''D''
' 

END
GO

/****** Object:  StoredProcedure [model].[CCDA_PatientProblem] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_PatientProblem]') AND type in (N'P', N'PC'))
DROP PROCEDURE [model].[CCDA_PatientProblem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_PatientProblem]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE Procedure [model].[CCDA_PatientProblem] (@PatientId nvarchar(50))

As

Begin

   Select Term + '', [SNOMED-CT:'' + Cast(ConceptId as nvarchar(50)) + '']'' as Col1, Convert(varchar(11),OnsetDate,101) as Col2, ''Active'' as Col3, ICD10 as Col4 from model.PatientProblemList p where p.PatientId = @PatientId and p.Status = ''Active''

 
End' 

END
GO

/****** Object:  StoredProcedure [model].[CCDA_PatientEncounters] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_PatientEncounters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [model].[CCDA_PatientEncounters]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_PatientEncounters]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE Procedure [model].[CCDA_PatientEncounters] (@PatientId nvarchar(50))

As

Begin

Select SUBSTRING(P.FindingDetail,CHARINDEX('':'',P.FindingDetail)+1 , LEN(P.FindingDetail)-CHARINDEX('':'',P.FindingDetail)) as Col2, ES.Description as Col1, SUBSTRING(A.AppDate,5,2)+''/''+SUBSTRING(A.AppDate,7,2)+''/''+SUBSTRING(A.AppDate,1,4) as Col3  From dbo.
PatientClinical P with(nolock) inner join dbo.Appointments A with(nolock) on P.AppointmentId = A.AppointmentId Left join Model.EncounterServices ES on ES.Code = SUBSTRING(P.FindingDetail,CHARINDEX('':'',P.FindingDetail)+1 , LEN(P.FindingDetail)-CHARINDEX('':
'',P.FindingDetail)) Where P.PatientId = @PatientId and P.ClinicalType = ''P''

End' 

END
GO

/****** Object:  StoredProcedure [model].[CCDA_PatientVitalSign] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_PatientVitalSign]') AND type in (N'P', N'PC'))
DROP PROCEDURE [model].[CCDA_PatientVitalSign] 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_PatientVitalSign]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE Procedure [model].[CCDA_PatientVitalSign] (@PatientId nvarchar(50))

As

Begin

 Select Top(1) 

 		CASE 

			WHEN CHARINDEX(''*BLOODPRESSURE='',PC.FINDINGDETAIL) = 1

				THEN substring(pc.findingdetail,charindex(''<'',pc.findingdetail)+1,(charindex(''/'',(REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))

			WHEN CHARINDEX(''*BLOODPRESSURELEFT='',PC.FINDINGDETAIL) = 1

				THEN substring(pc.findingdetail,charindex(''<'',pc.findingdetail)+1,(charindex(''/'',(REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))

			WHEN CHARINDEX(''*BLOODPRESSURERTWRIST='',PC.FINDINGDETAIL) = 1

				THEN substring(pc.findingdetail,charindex(''<'',pc.findingdetail)+1,(charindex(''/'',(REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))

			WHEN CHARINDEX(''*BLOODPRESSURELEFTWRIST='',PC.FINDINGDETAIL) = 1

				THEN substring(pc.findingdetail,charindex(''<'',pc.findingdetail)+1,(charindex(''/'',(REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''<'', pc.findingdetail), LEN(pc.findingdetail)), ''<'', '''')))-1))

			ELSE ''''

		END AS col1,

		CASE 

			WHEN CHARINDEX(''*BLOODPRESSURE='',PC.FINDINGDETAIL) = 1

				THEN substring(pc.findingdetail,charindex(''/'',pc.findingdetail)+1,(charindex(''>'',(REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail), LEN(pc.findingdetail)), ''/'', '''')))-1))

			WHEN CHARINDEX(''*BLOODPRESSURELEFT='',PC.FINDINGDETAIL) = 1

				THEN substring(pc.findingdetail,charindex(''/'',pc.findingdetail)+1,(charindex(''>'',(REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail), LEN(pc.findingdetail)), ''/'', '''')))-1))

			WHEN CHARINDEX(''*BLOODPRESSURERTWRIST='',PC.FINDINGDETAIL) = 1

				THEN substring(pc.findingdetail,charindex(''/'',pc.findingdetail)+1,(charindex(''>'',(REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail), LEN(pc.findingdetail)), ''/'', '''')))-1))

			WHEN CHARINDEX(''*BLOODPRESSURELEFTWRIST='',PC.FINDINGDETAIL) = 1

				THEN substring(pc.findingdetail,charindex(''/'',pc.findingdetail)+1,(charindex(''>'',(REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX(''/'', pc.findingdetail), LEN(pc.findingdetail)), ''/'', '''')))-1))

			ELSE ''''

			END AS col2

 

FROM DBO.PATIENTCLINICAL PC WITH(NOLOCK)

inner join dbo.Appointments A on PC.AppointmentId = A.AppointmentId

WHERE PC.SYMPTOM LIKE ''/BLOOD PRESSURE%'' AND PC.FINDINGDETAIL LIKE ''*BLOODPRESSURE%'' and PC.ClinicalType = ''F'' and A.PatientId = @PatientId

End' 

END
GO

/****** Object:  StoredProcedure [Model].[CCDA_PatientSmokingStatus] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Model].[CCDA_PatientSmokingStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Model].[CCDA_PatientSmokingStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Model].[CCDA_PatientSmokingStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE Procedure [Model].[CCDA_PatientSmokingStatus] (@PatientId nvarchar(50))

As

Begin

 Declare @SmokingCondition nvarchar(50)  

 Set @SmokingCondition = (Select Top(1) SUBSTRING(P.FINDINGDETAIL,CHARINDEX(''<'',P.FINDINGDETAIL)+1,CHARINDEX(''>'',P.FINDINGDETAIL)-CHARINDEX(''<'',P.FINDINGDETAIL)-1) From dbo.PatientClinical P Where ClinicalType = ''F'' and Symptom Like ''%Smoking'' and FindingDetail Like ''*%'' and PatientId = @PatientId Order by AppointmentId Desc)



 Select Top(1) ''Smoking Status'' as Col1, SC.Name + '' [SNOMED-CT: '' + ISNULL(SC.SNOMEDCode,'''') + '']'' as Col2, AppDate as Col3 From dbo.PatientClinical P inner join dbo.Appointments A on P.AppointmentId = A.AppointmentId left join model.SmokingConditions SC
 on Replace(SC.Name,'' '','''') like ''%'' + @SmokingCondition + ''%'' Where ClinicalType = ''F'' and Symptom = ''/SMOKING'' and FindingDetail Like ''*%'' and A.PatientId = @PatientId

End' 

END
GO

/****** Object:  StoredProcedure [model].[CCDA_PatientChiefComplaint] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_PatientChiefComplaint]') AND type in (N'P', N'PC'))
DROP PROCEDURE [model].[CCDA_PatientChiefComplaint]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_PatientChiefComplaint]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE Procedure [model].[CCDA_PatientChiefComplaint] (@PatientId nvarchar(50))

As

Begin

 Select Top(1) right(p.findingdetail,len(p.findingdetail)-3) as Col1 From dbo.PatientClinical P inner join dbo.Appointments A on P.AppointmentId = A.AppointmentId Where ClinicalType = ''H'' and Symptom Like ''%ChiefComplaint%'' and A.PatientId = @PatientId and
 A.ActivityStatus = ''D'' and A.ScheduleStatus = ''D'' order by A.AppDate desc

End
' 
END
GO

/****** Object:  StoredProcedure [dbo].[GetProblemDetailsBySnomedCode] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetProblemDetailsBySnomedCode]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetProblemDetailsBySnomedCode]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetProblemDetailsBySnomedCode]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

create procedure [dbo].[GetProblemDetailsBySnomedCode] (@ConceptId varchar(100))
as begin
	SELECT TOP(1) SN.Term,SN.ICD10, Replace(SN.ICD10DESCR,'''','''') as problem FROM dbo.snomed_2016 SN  WHERE SN.ConceptId=@ConceptId
END' 

END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[GetTempetdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [model].[GetTempetdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[GetTempetdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
		
 CREATE proc [model].[GetTempetdate]
 (
 @startDate datetime,
 @enddate datetime
 )
 As begin

SELECT  CS.Auditentryid,TamperDate, AE.HostName, AE.Servername, AE.OBJECTNAME, (CASE WHEN ChangeTypeId=0 THEN ''INSERTION'' 
             WHEN ChangeTypeId = 1 THEN ''CHANGES'' WHEN ChangeTypeId = 2 THEN ''DELETION''  END) as [Action]  FROM model.checksumdata CS 
            LEFT JOIN  AuditEntries AE  ON AE.Id = CS.Auditentryid  WHERE TamperDate is Not NULL   
			AND convert(date, TamperDate)  between CONVERT( DATE,@startDate) and CONVERT( DATE,@enddate)
			AND TamperDate IS NOT NULL

   
 End' 

END
GO

/****** Object:  StoredProcedure [dbo].[SC_GetAUDITEVENT] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SC_GetAUDITEVENT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SC_GetAUDITEVENT]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SC_GetAUDITEVENT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[SC_GetAUDITEVENT]  
   @AuditEntryId uniqueidentifier,
   @IsAdd VARCHAR(50)=''''
AS       
BEGIN     

	Declare @AuditEntryChangesOldVal Varchar(2000)
	Declare @AuditEntryChangesNewVal Varchar(2000)
	Declare @AuditEntryVal Varchar(2000)
	DECLARE @CHECKSUM_VALUE varbinary(8000) 

	SELECT @AuditEntryChangesOldVal= COALESCE(@AuditEntryChangesOldVal + '' '', ''_'') + ISNULL(OldValue,'''')
	FROM  AuditEntryChanges 
	WHERE AuditEntryId=@AuditEntryId

	SELECT @AuditEntryChangesNewVal= COALESCE(@AuditEntryChangesNewVal + '' '', ''_'') + ISNULL(NewValue,'''')
	FROM  AuditEntryChanges 
	WHERE AuditEntryId=@AuditEntryId

		SELECT @AuditEntryVal= CONVERT(VARCHAR(100) ,Id)+''_''+ 
		isnull(ObjectName,'''')+''_''+ 
		isnull(CAST(ChangeTypeId as varchar(50)),'''')+''_''+ 
		isnull(cast(AuditDateTime as VARCHAR(50)),'''')+''_''+
		isnull(HostName,'''')+''_''+ 
		isnull(cast(UserId as varchar(10)),'''')+''_''+ 
		isnull(ServerName,'''')+''_''+ 
		isnull(KeyNames,'''')+''_''+
		isnull(KeyValues,'''')+''_''+
		isnull(cast(PatientId as VARCHAR(50)),'''')+''_''+ 
		isnull(cast(AppointmentId as Varchar(50)) ,'''') 
		FROM AuditEntries WHERE Id=@AuditEntryId
	
	if(@AuditEntryId IS NOT NULL)
	BEGIN
	
		SELECT  @CHECKSUM_VALUE=EncryptByPassPhrase(''IOAUDITTAMPERED'', @AuditEntryVal+''_''+
											ISNULL(@AuditEntryChangesOldVal,'''') +''_''+											
											isnull(@AuditEntryChangesNewVal,''''))
			IF(@IsAdd=''add'')											
			INSERT INTO  model.CheckSumData
			(AuditEntryId,
			ChecksumValue,
			TamperDate,
			AlteredChecksum)
			VALUES(@AuditEntryId,
			@CHECKSUM_VALUE,
			NULL ,
			NULL)	
		ELSE IF(@IsAdd=''update'')	
			UPDATE model.CheckSumData 
			SET AlteredChecksum=@CHECKSUM_VALUE,TamperDate=Getutcdate() 
			WHERE AuditEntryId=@AuditEntryId
			
	END 

END' 
END
GO

--/****** Object:  StoredProcedure [DrFirst].[GetPatientProblemDiagnosis] ******/
-----------------------------------------------------------------------------------

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[USP_GetICD10_SnomedCT]') AND type in (N'P', N'PC'))
--DROP PROCEDURE [DrFirst].[GetPatientProblemDiagnosis]
--GO

--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[GetPatientProblemDiagnosis]') AND type in (N'P', N'PC'))
--BEGIN
--EXEC dbo.sp_executesql @statement = N'


----[DrFirst].[GetPatientProblemDiagnosis]43609, 73931

--CREATE  PROCEDURE [DrFirst].[GetPatientProblemDiagnosis]

--	@PatientID BIGINT,

--	@appointmentID BIGINT

--AS

--BEGIN

--	SET NOCOUNT ON;

--	SELECT 
--	Id,
--	PatientId,
--	AppointmentId,
--	AppointmentDate,
--	ConceptID,
--	Term,
--	ICD10,
--	ICD10DESCR,
--	COMMENTS,
--	ResourceName,
--	OnsetDate,
--	LastModifyDate,
--	[Status],
--	ResourceId,
--	EnteredDate,
--	ResolvedDate
--	FROM model.PatientProblemList
--	WHERE PatientId=@PatientID AND AppointmentId=@appointmentID

--	END' 

--END
--GO

--/****** Object:  StoredProcedure [DrFirst].[UpdateAllergyResponse] ******/
-------------------------------------------------------------------------------------

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[UpdateAllergyResponse]') AND type in (N'P', N'PC'))
--DROP PROCEDURE [DrFirst].[UpdateAllergyResponse]
--GO

--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[UpdateAllergyResponse]') AND type in (N'P', N'PC'))
--BEGIN
--EXEC dbo.sp_executesql @statement = N'


--CREATE PROCEDURE [DrFirst].[UpdateAllergyResponse]
--	-- Add the parameters for the stored procedure here
--	@xmlStringData varchar(max)	
	
--AS
--BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
--	SET NOCOUNT ON;

--	--IF OBJECT_ID(''tempdb..#XmlTable'') IS NOT NULL
-- --   DROP TABLE #XmlTable

--	--CREATE Table #XmlTable		
--	--(	xmlData Xml
--	--)

--	---select  * from #XmlTable
	
--	DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)

	
--	SELECT   @XML= CAST(@xmlStringData AS XML)  
	
--	EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML

--	SELECT Deleted,[Status] ,Clnical_ExtID, Patient_ExtID, AllergyName, Reaction, Appointment_ExtID,OnsetDate,LastModifiedDate
--	INTO #Response FROM OPENXML(@hDoc, ''RCExtResponse/Response/AllergyList/Allergy/Allergen'')
--	WITH 
--	(
--	Deleted [varchar](50) ''../Deleted'',
--	[Status] [varchar](50) ''../Status'',
--	Clnical_ExtID [varchar](50) ''../ExternalID'',
--	Patient_ExtID [varchar](50) ''../Patient/ExternalID'',
--	AllergyName [varchar](100) ''Name'',
--	Reaction [varchar](100) ''../Reaction'',
--	Appointment_ExtID [varchar](50) ''../EncounterList/Encounter/ExternalID'',
--	ActionTaken [varchar](50) ''../EncounterList/Encounter/ActionTaken'',
--	OnsetDate [varchar](50) ''../OnsetDate'',
--	LastModifiedDate [varchar](50) ''../LastModifiedDate''
--	)
	

--	EXEC sp_xml_removedocument @hDoc

--	select * from #Response

--END' 

--END
--GO

--/****** Object:  StoredProcedure [DrFirst].[UpdateErxDiagnosisMapping] ******/
-------------------------------------------------------------------------------------

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[UpdateErxDiagnosisMapping]') AND type in (N'P', N'PC'))
--DROP PROCEDURE [DrFirst].[UpdateErxDiagnosisMapping]
--GO

--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[UpdateErxDiagnosisMapping]') AND type in (N'P', N'PC'))
--BEGIN
--EXEC dbo.sp_executesql @statement = N'

--CREATE Procedure [DrFirst].[UpdateErxDiagnosisMapping](@PatientId numeric(18,0), @AppointmentId numeric(18,0)) 
--As 
--Begin

--SET NOCOUNT ON;

--	SELECT

--	DISTINCT PC.ClinicalId,

--	PC.PatientId,

--	PC.appointmentid as EncounterId,

--	A.appdate as EncounterDate,

--	Case 
--	When PC.FindingDetail like ''%.%.%'' Then 
--	Substring(PC.FindingDetail,0,Len(PC.FindingDetail) - CHARINDEX(''.'', REVERSE(PC.FindingDetail)) + 1)
--	Else PC.FindingDetail End as FindingDetail,       

--	PD.ReviewSystemLingo as Description,

--	PC.ImageDescriptor,

--	(CASE WHEN  CHARINDEX(''.'',PC.findingdetail,(charindex(''.'',PC.findingdetail)+1)) >0 THEN

--	SUBSTRING (PC.findingdetail,0, CHARINDEX(''.'',PC.findingdetail,(charindex(''.'',PC.findingdetail)+1))) ELSE PC.findingdetail END) as Code

--	Into #TmpDiagnosis FROM PatientClinical PC

--	INNER JOIN [dm].[PrimaryDiagnosisTable] PD 

--	ON PD.diagnosisNextlevel=PC.findingdetail

--	INNER JOIN dbo.Appointments  A

--	ON A.appointmentid=PC.appointmentid

--	WHERE PC.findingdetail LIKE ''%[0-9]%'' and PC.FindingDetail not in (''0001'',''P944'')

--	AND PC.PatientID=@PatientID 

--	AND PC.ClinicalType In(''B'',''K'',''Q'') 

--	AND PC.appointmentid=@appointmentID

--	;with CTEDignosis(ClinicalId, PatientId, EncounterId, EncounterDate, FindingDetail, Description, Code, ImageDescriptor, [Rank]) as 

--	(Select ClinicalId, PatientId, EncounterId, EncounterDate, FindingDetail, Description, Code, ImageDescriptor,

--	Row_number() over(Partition by FindingDetail order by Code) as [Rank] from  #TmpDiagnosis)
	
--	Insert into DrFirst.ErxDiagnosisMapping(ClinicalId, PatientId, EncounterId, FindingDetail, Description, Code, ImageDescriptor)
--	Select ClinicalId, PatientId, EncounterId, FindingDetail, Description, Code, ImageDescriptor from CTEDignosis Ds WHERE [Rank]=1 and Ds.ClinicalId not in (Select ClinicalId
--	from DrFirst.ErxDiagnosisMapping with (nolock) where PatientId = @PatientId and EncounterId = @AppointmentId)

--	IF OBJECT_ID(''tempdb..#TmpDiagnosis'') IS NOT NULL  

--	DROP TABLE #TmpDiagnosis 

--End' 

--END
--GO

--/****** Object:  StoredProcedure [DrFirst].[UpdateMedicationResponse] ******/
-------------------------------------------------------------------------------------

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[UpdateMedicationResponse]') AND type in (N'P', N'PC'))
--DROP PROCEDURE [DrFirst].[UpdateMedicationResponse]
--GO

--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[UpdateMedicationResponse]') AND type in (N'P', N'PC'))
--BEGIN
--EXEC dbo.sp_executesql @statement = N'

----select * from patientclinical where patientid=116996
-----select * from patientclinical where findingdetail like ''%lid%'' and clinicaltype=''A''
----[DrFirst].[GetPatientMedications]40934
----[DrFirst].[GetPatientMedications_salim]40934
----[DrFirst].[UpdateMedicationResponse_working] 116996

--CREATE   PROCEDURE [DrFirst].[UpdateMedicationResponse]
--@PatientID BIGINT,
--@appointmentId BIGINT,
--@xmlStringData varchar(max)

--AS
--SET NOCOUNT ON

--IF OBJECT_ID(''tempdb..#Medication'') IS NOT NULL DROP TABLE #Medication 
--IF OBJECT_ID(''tempdb..#RxSegments'') IS NOT NULL  DROP TABLE #RxSegments
----declare @PatientID bigint=40943
----declare @appointmentId bigint=3312639

--		CREATE TABLE #RxSegments
--		(
--			ClinicalId int,
--			SegTwo int,
--			SegThree int,
--			SegFour int,
--			SegFive int,
--			SegSix int,
--			SegSeven int,
--			SegNine int,
--			LegacyDrug bit
--		)

--		INSERT INTO #RxSegments
--		SELECT pc.clinicalid
--		, CHARINDEX(''-2'', pc.FindingDetail) + 3
--		, CHARINDEX(''-3'', pc.FindingDetail) + 3
--		, CHARINDEX(''-4'', pc.FindingDetail) + 3
--		, CHARINDEX(''-5'', pc.FindingDetail) + 3
--		, CHARINDEX(''-6'', pc.FindingDetail) + 3
--		, CHARINDEX(''-7'', pc.FindingDetail) + 3
--		, CHARINDEX(''9-9'', pc.FindingDetail)
--		, CASE WHEN pc.FindingDetail like ''%()%'' THEN 0 ELSE 1 END 
--		FROM PatientClinical pc
--		WHERE pc.ClinicalId in (select clinicalid as number from patientclinical where clinicaltype=''A'' AND FindingDetail like ''Rx%'' 
--		and pc.PatientId = @PatientID --and appointmentid=@appointmentId
--		)
--		and pc.PatientId = @PatientID -- and appointmentid=@appointmentId
			
	
--		select  pc.ClinicalId as ClinicalId,pc.PatientId ,A.appointmentid,
--		FORMAT(cast(A.appdate as datetime) , ''MM/dd/yyyy HH:mm:ss'') as AppDate,	
--		A.resourceid1 as DoctorExternalId,ResourceName,ResourcePid as LoginID,
--		pc.findingdetail,
--		SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)) as restFD1,
--		(SELECT STUFF((SELECT char(9) + Note1
--					FROM patientnotes where patientid=@PatientID AND   Notetype in(''R'')
--					FOR XML PATH('''')) ,1,1,'''')) AS PatientNote
			
--		,(SELECT STUFF((SELECT char(9) + Note1
--					FROM patientnotes where patientid=@PatientID And  notetype in(''B'',''C'',''X'')
--					FOR XML PATH('''')) ,1,1,'''')) AS OtherNote,
--					pc.DrawfileName as DrugId,
--					(CASE WHEN pc.FindingDetail like ''RX-8%-2%''
--			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex(''-2'', pc.findingdetail) - 6),''&'',''and''), ''+'',''Plus'') 
--		ELSE '''' END) as DrugName,
--		(''RX-8/''+
--		(CASE WHEN pc.FindingDetail like ''RX-8%-2%''
--			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex(''-2'', pc.findingdetail) - 6),''&'',''and''), ''+'',''Plus'') ELSE '''' END)
--		+
--		''-2/(Dosage)(Strength)(''+
--				--SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3),dbo.fn_Occurences(''('',(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3)),3)+1,(dbo.fn_Occurences ('')'',(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3)),3))-([dbo].[fn_Occurences] (''('',(SUBSTRING(pc.FindingDetail, rx.SegTwo, rx.SegThree-rx.SegTwo-3)),3))-1)
--				+''EyeContext)-3/(Frequency)(''+
--		--(CASE rx.LegacyDrug WHEN 0 THEN 
--		--SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegThree+1) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegThree+1)) - CHARINDEX(''('', pc.FindingDetail, rx.SegThree+1) - 1)
--		--ELSE ''''END)-- as PRN,
--		+''PRN)(PO)(''+
--		(CASE rx.LegacyDrug WHEN 0 THEN
--			SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree)+1)+1)) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree+1)+1)+1)+1)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree+1)+1)+1)) - 1)
--		ELSE ''''END) --as OTC,		
--		+	'')(''+
--			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences(''('',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5)+1,(dbo.fn_Occurences ('')'',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-([dbo].[fn_Occurences] (''('',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),5))-1)) +'')(''+
--			(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3),dbo.fn_Occurences(''('',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6)+1,(dbo.fn_Occurences ('')'',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-([dbo].[fn_Occurences] (''('',(SUBSTRING(pc.FindingDetail, rx.SegThree, rx.SegFour-rx.SegThree-3)),6))-1))
--			+'')-4/(Refills)(''+
--			SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3),dbo.fn_Occurences(''('',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2)+1,(dbo.fn_Occurences ('')'',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-([dbo].[fn_Occurences] (''('',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),2))-1) 
--			+'')(QtyUnit)(''+	
--		--(SUBSTRING(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3),dbo.fn_Occurences(''('',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),3)+1,(dbo.fn_Occurences ('')'',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),3))-([dbo].[fn_Occurences] (''('',(SUBSTRING(pc.FindingDetail, rx.SegFour, rx.SegFive-rx.SegFour-3)),3))-1))+'')(''+
		
--		(CASE rx.LegacyDrug WHEN 0 THEN
--		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour)+1)+1)) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour+1)+1)+1)+1)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour+1)+1)+1)) - 1)
--		ELSE ''''	END)-- as LastTaken,
--		+'')-5/(Duration)(''+			
--		(CASE rx.LegacyDrug WHEN 0 THEN
--		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegFive+1) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegFive+1)) - CHARINDEX(''('', pc.FindingDetail, rx.SegFive+1) - 1)
--		ELSE ''''	END)--as Supply,
--		+'')''+		
--		(SUBSTRING(pc.FindingDetail,rx.SegSix-3,Len(Pc.FindingDetail)))) as FD,
--		'''' AS FD1,
--		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX('')'', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)as FindingDuration,
--		pc.[Status]
--		INTO #Medication
--		FROM PatientClinical pc 
--		INNER JOIN appointments A ON A.Appointmentid=pc.Appointmentid
--		INNER JOIN Resources R ON R.ResourceId=A.resourceid1	
--		INNER JOIN #RxSegments rx on pc.ClinicalId = rx.ClinicalId
--		LEFT OUTER JOIN PatientNotes pn1 on pn1.AppointmentId = pc.AppointmentId 
--		AND Substring(PC.FindingDetail, 6, CHARINDEX(''-2'',pc.findingdetail)-6) = pn1.NoteSystem 
--		AND pn1.NoteType = ''R''
--		LEFT OUTER JOIN PatientNotes pn2 on pn2.AppointmentId = pc.AppointmentId
--		AND SUBSTRING(pc.FindingDetail, 6, CHARINDEX(''-2'', pc.findingdetail)-6) = pn2.NoteSystem
--		AND pn2.NoteType = ''X''			
--		WHERE pc.ClinicalId in (select clinicalid as number from patientclinical where  clinicaltype=''A'' AND FindingDetail like ''Rx%'')
--		and pc.PatientId = @PatientID

--		--select * from  #Medication
--		--DECLARE  @xmlStringData VARCHAR(MAX)=''<?xml version="1.0" encoding="UTF-8"?> <RCExtResponse version="1.0"> <Request> <Caller> <VendorName>ivendor35</VendorName> <VendorPassword>xxxxxxxx</VendorPassword> </Caller> <SystemName>**sys_name**</SystemName> <RcopiaPracticeUsername>io98981</RcopiaPracticeUsername> <Request> <Command>update_medication</Command> <LastUpdateDate>07/21/2017 02:25:56</LastUpdateDate> <Patient> <RcopiaID>221130388</RcopiaID> <ExternalID>40943</ExternalID> </Patient> </Request> </Request> <Response> <ServerIPAddress>update201.staging.drfirst.com:80</ServerIPAddress> <LastUpdateDate /> <MedicationList> <Medication> <Deleted>n</Deleted> <RcopiaID>22801620</RcopiaID> <ExternalID /> <Patient> <RcopiaID>221130388</RcopiaID> <ExternalID /> <FirstName>zxzxz</FirstName> <LastName>zxzxzx</LastName> </Patient> <Provider> <RcopiaID>222757172</RcopiaID> <Username>iprovider35</Username> <ExternalID /> <FirstName>IOPracticeware</FirstName> <LastName>Provider</LastName> </Provider> <Preparer> <RcopiaID>222757172</RcopiaID> <Username>iprovider35</Username> <ExternalID /> <FirstName>IOPracticeware</FirstName> <LastName>Provider</LastName> </Preparer> <DrugCodingLevel>strength</DrugCodingLevel> <Sig> <Drug> <NDCID>00065027515</NDCID> <FirstDataBankMedID>220502</FirstDataBankMedID> <RcopiaID>11317</RcopiaID> <BrandName>Azopt</BrandName> <GenericName>brinzolamide</GenericName> <BrandType>brand</BrandType> <Route>opht</Route> <Form>drops,suspension</Form> <Strength>1%</Strength> </Drug> <Action>Instill</Action> <Dose>1</Dose> <DoseUnit>drop</DoseUnit> <Route>into right eye</Route> <DoseTiming>three times a day</DoseTiming> <DoseOther /> <Duration>3</Duration> <Quantity>1</Quantity> <QuantityUnit>vial</QuantityUnit> <Refills>PRN</Refills> <SubstitutionPermitted>y</SubstitutionPermitted> <OtherNotes>topharma</OtherNotes> <PatientNotes>to patient</PatientNotes> <Comments /> </Sig> <StartDate>07/21/2017 00:00:00 EDT</StartDate> <StopDate /> <FillDate /> <StopReason /> <SigChangedDate>07/21/2017 02:29:29 EDT</SigChangedDate> <LastModifiedBy>iprovider35</LastModifiedBy> <LastModifiedDate>07/21/2017 02:29:29 EDT</LastModifiedDate> <Height /> <Weight /> <IntendedUse /> </Medication> <Medication> <Deleted>n</Deleted> <RcopiaID>22801098</RcopiaID> <ExternalID>4041737</ExternalID> <Patient> <RcopiaID>221130388</RcopiaID> <ExternalID /> <FirstName>zxzxz</FirstName> <LastName>zxzxzx</LastName> </Patient> <Provider> <RcopiaID>222757172</RcopiaID> <Username>iprovider35</Username> <ExternalID /> <FirstName>IOPracticeware</FirstName> <LastName>Provider</LastName> </Provider> <Preparer> <RcopiaID>222757142</RcopiaID> <Username>ivendor35</Username> <ExternalID /> <FirstName>IOPracticeware</FirstName> <LastName>Vendor</LastName> </Preparer> <DrugCodingLevel>strength</DrugCodingLevel> <Sig> <Drug> <NDCID>24208091055</NDCID> <FirstDataBankMedID>238273</FirstDataBankMedID> <RcopiaID>23821</RcopiaID> <BrandName>erythromycin</BrandName> <GenericName>erythromycin</GenericName> <BrandType>generic</BrandType> <Route>opht</Route> <Form>ointment</Form> <Strength>5 mg/gram (0.5 %)</Strength> </Drug> <Action>Take</Action> <Dose>10</Dose> <DoseUnit>APPLICATION</DoseUnit> <Route>by Mouth</Route> <DoseTiming>6X DAY</DoseTiming> <DoseOther /> <Duration /> <Quantity /> <QuantityUnit /> <Refills>5</Refills> <SubstitutionPermitted>y</SubstitutionPermitted> <OtherNotes /> <PatientNotes /> <Comments /> </Sig> <StartDate>07/14/2017 00:00:00 EDT</StartDate> <StopDate>07/20/2017 00:00:00 EDT</StopDate> <FillDate /> <StopReason /> <SigChangedDate>07/21/2017 02:25:56 EDT</SigChangedDate> <LastModifiedBy>ivendor35</LastModifiedBy> <LastModifiedDate>07/21/2017 02:25:56 EDT</LastModifiedDate> <Height /> <Weight /> <IntendedUse /> </Medication> <Medication> <Deleted>n</Deleted> <RcopiaID>22801127</RcopiaID> <ExternalID>4041779</ExternalID> <Patient> <RcopiaID>221130388</RcopiaID> <ExternalID /> <FirstName>zxzxz</FirstName> <LastName>zxzxzx</LastName> </Patient> <Provider> <RcopiaID>222757172</RcopiaID> <Username>iprovider35</Username> <ExternalID /> <FirstName>IOPracticeware</FirstName> <LastName>Provider</LastName> </Provider> <Preparer> <RcopiaID>222757142</RcopiaID> <Username>ivendor35</Username> <ExternalID /> <FirstName>IOPracticeware</FirstName> <LastName>Vendor</LastName> </Preparer> <DrugCodingLevel>strength</DrugCodingLevel> <Sig> <Drug> <NDCID>00065175007</NDCID> <FirstDataBankMedID>576343</FirstDataBankMedID> <RcopiaID>15619</RcopiaID> <BrandName>Ilevro</BrandName> <GenericName>nepafenac</GenericName> <BrandType>brand</BrandType> <Route>opht</Route> <Form>drops,suspension</Form> <Strength>0.3%</Strength> </Drug> <Action /> <Dose>11</Dose> <DoseUnit>DROP</DoseUnit> <Route /> <DoseTiming>QAM</DoseTiming> <DoseOther /> <Duration /> <Quantity /> <QuantityUnit /> <Refills>63</Refills> <SubstitutionPermitted>y</SubstitutionPermitted> <OtherNotes /> <PatientNotes /> <Comments /> </Sig> <StartDate>07/20/2017 00:00:00 EDT</StartDate> <StopDate /> <FillDate /> <StopReason /> <SigChangedDate>07/21/2017 02:25:56 EDT</SigChangedDate> <LastModifiedBy>ivendor35</LastModifiedBy> <LastModifiedDate>07/21/2017 02:25:56 EDT</LastModifiedDate> <Height /> <Weight /> <IntendedUse /> </Medication> <Number>3</Number> </MedicationList> <Status>ok</Status> </Response> </RCExtResponse>''
--		DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)  
  
--		SELECT   @XML= CAST(@xmlStringData AS XML)  
  
--		EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML  
  
--		SELECT 		
--		Patient_ExtID, ClinicalID, NDCID,
--		FirstDataBankMedID,RcopiaID,
--		BrandName,GenericName,BrandType,
--		[RouteDrug],[Form],Strength,
--		[Action],Dose, DoseUnit, 
--		[Route], Frequency, Duration, 
--		Quantity, QuantityUnit, 
--		Refills,
--		OtherNotes,PatientNotes,	
--		SUBSTRING(SigChangedDate,1,10) as SigChangedDate,
--		SUBSTRING(LastModifiedDate,1,10) as LastModifiedDate,
--		SUBSTRING(StartDate,1,10) as StartDate,
--		SUBSTRING(StopDate,1,10) as StopDate,
--		Deleted,
--		StopReason,
--		MedicationRcopiaID
--		INTO #Response  
--		FROM OPENXML(@hDoc, ''RCExtResponse/Response/MedicationList/Medication/Sig/Drug'')  
--		WITH   
--		(  			
--		Patient_ExtID [varchar](50) ''../../Patient/ExternalID'',  
--		ClinicalID [varchar](50) ''../../ExternalID'',

--		NDCID [VARCHAR](100) ''NDCID'',
--		FirstDataBankMedID [VARCHAR](100) ''FirstDataBankMedID'',
--		RcopiaID [VARCHAR](100) ''RcopiaID'',				
--		BrandName [varchar](200) ''BrandName'',  
--		GenericName [varchar](200) ''GenericName'',  
--		BrandType [VARCHAR](100) ''BrandType'',		
--		[RouteDrug]  [varchar](200) ''Route'',
--		[Form] [varchar](100) ''Form'',		
--		Strength  [varchar](200) ''Strength'',  
		
--		[Action]  [varchar](200) ''../Action'', 				 	 
--		Dose  [varchar](200) ''../Dose'',
--		DoseUnit  [varchar](200) ''../DoseUnit'',
--		[Route]  [varchar](200) ''../Route'',   
--		Frequency  [varchar](200) ''../DoseTiming'',  
--		Duration  [varchar](200) ''../Duration'',  
--		Quantity  [varchar](200) ''../Quantity'',  		
--		QuantityUnit  [varchar](200) ''../QuantityUnit'',  		
--		Refills  [varchar](200) ''../Refills'',  
--		OtherNotes  [varchar](200) ''../OtherNotes'',  
--		PatientNotes  [varchar](200) ''../PatientNotes'', 
		  		
--		SigChangedDate [varchar](50) ''../../SigChangedDate'' ,
--		LastModifiedDate [varchar](50) ''../../ExternLastModifiedDatealID'' ,
--		StartDate [varchar](50) ''../../StartDate'',
--		StopDate [varchar](50) ''../../StopDate'',
--		Deleted [varchar](50) ''../../Deleted'' ,
--		StopReason [varchar](200) ''../../StopReason'',
--		MedicationRcopiaID VARCHAR(50)''../../RcopiaID''
--		 )  
--		 EXEC sp_xml_removedocument @hDoc 

--		 --select * from #Response

			
--		SELECT  ROW_NUMBER() OVER(PARTITION BY RcopiaID, MedicationRcopiaID,NDCID,Brandname,FirstDataBankMedID ORDER BY MedicationRcopiaID) AS ROWNUMBER, 
--		 --''1234567''  as Patient_ExtID, ''1234567'' AS AppointmentId,
--		@PatientID AS Patient_ExtID ,@appointmentId AS AppointmentId,
--		ClinicalID,MedicationRcopiaID,BrandName,Form,
--		(CASE WHEN R.[Route] like ''%right eye'' THEN ''OD'' WHEN R.[Route] like ''%left eye%'' THEN ''OS'' WHEN R.[Route] like ''%both eyes'' THEN ''OU'' ELSE '''' END) AS [EyeContext],
--		''99'' as [Symptom],
--		 RcopiaID,NDCID,
--		 ''RX-8/''+R.BrandName+'' ''+R.Form+''-2/(''+Dose+'' ''+DoseUnit+'')(''+Strength+'')(''+
--		(CASE WHEN R.[Route] like ''%right eye'' THEN ''OD'' WHEN R.[Route] like ''%left eye%'' THEN ''OS'' WHEN R.[Route] like ''%both eyes'' THEN ''OU'' ELSE '''' END)
--		+'')-3/(''+Frequency+'')(''+
--		(CASE WHEN R.Refills=''PRN'' THEN ''PRN'' ELSE '''' END)+'')(''+
--		(Case WHEN R.[Action]=''Apply'' THEN ''APPLY TO LIDS'' WHEN R.[Action]=''INSTILL'' THEN ''INSTILL IN EYES'' WHEN R.[Action]='''' THEN  ''PO'' ELSE R.[Route] END)+'')()()()-4/(''+
--		(CASE WHEN R.Refills=''PRN'' THEN '''' ELSE R.Refills+'' ''+''REFILLS'' END)+'')()(''+
--		(R.Quantity+'' ''+R.QuantityUnit)+'')()-5/(''+
--		(CASE WHEN R.Duration LIKE ''[0-9]'' THEN R.Duration+'' days'' ELSE R.Duration END)+'')()-6/'' as Findings,
--		(''!''+StartDate ) AS ImgDescriptor,
--		FirstDataBankMedID,
--		''A''	as [Status],
--		PatientNotes,
--		OtherNotes,
--		StartDate,
--		StopDate		
--		INTO #NewMeds
--		FROM #Response R WHERE  ClinicalID =''''
--		AND MedicationRcopiaID NOT IN( SELECT RcopiyaID FROM [DrFirst].[ErxMedicationMapping])



--select * from #NewMeds

--		DECLARE @NewMedCount INT
--		SELECT @NewMedCount= COUNT(*) FROM #NewMeds
--		print @NewMedCount
----select COUNT(*) FROM #NewMeds
--		DECLARE @loop INT
--		SET @loop = 1

--		WHILE @loop <= @NewMedCount
--		BEGIN

--			---SELECT  * FROM #NewMeds WHERE ROWNUMBER=@loop
			
--			INSERT INTO dbo.PatientClinical
--			(
--			[AppointmentId]
--			,[PatientId]
--			,[ClinicalType]
--			,[EyeContext]
--			,[Symptom]
--			,[FindingDetail]
--			,[ImageDescriptor]      
--			,[Status]
--			,[DrawFileName])
--			--,[Highlights]  --,[FollowUp]      --,[PermanentCondition]      --,[PostOpPeriod]      --,[Surgery]      --,[Activity]      --,[LegacyClinicalDataSourceTypeId]
--			SELECT  AppointmentId, Patient_ExtID,''A'',
--			EyeContext,Symptom,Findings, ImgDescriptor, [Status], FirstDataBankMedID
--			FROM #NewMeds TT 			
--			WHERE ROWNUMBER=@loop

--			DECLARE @ClinicalID_New BIGINT 
--			SELECT @ClinicalID_New=IDENT_CURRENT(''PatientClinical'')

--			select @ClinicalID_New

			
--			INSERT INTO [DrFirst].[ErxMedicationMapping]
--			([ClinicalId],[EncounterId],[PatientId],[RcopiyaID],[MedDatetime])
--			SELECT @ClinicalID_New, @appointmentId, @PatientID,MedicationRcopiaID,GETUTCDATE() 
--				FROM #NewMeds
--					WHERE  ROWNUMBER=@loop

----select @ClinicalID_New

		

--			INSERT INTO  dbo.PatientNotes 
--			(
--				PatientId,
--				AppointmentId,
--				NoteType,
--				Note1,
--				NoteDate,
--				NoteSystem, 
--				NoteCommentOn,
--				NoteAudioOn,
--				NoteClaimOn,
--				ClinicalId 
--			)
--			SELECT 
--			@PatientID, 
--			@appointmentId,
--			''R'',
--			PatientNotes,			
--			FORMAT(CAST(StartDate AS DATE),''yyyyMMdd''),
--			(BrandName+'' ''+Form),
--			''T'',''F'',''F'',
--			@ClinicalID_New
--			FROM   #NewMeds
--			WHERE  ROWNUMBER=@loop

--			INSERT INTO  dbo.PatientNotes 
--			(
--				PatientId,
--				AppointmentId,
--				NoteType,
--				Note1,
--				NoteDate,
--				NoteSystem, 
--				NoteCommentOn,
--				NoteAudioOn,
--				NoteClaimOn,
--				ClinicalId 
--			)
--			SELECT 
--			@PatientID, 
--			@appointmentId,
--			''X'',
--			OtherNotes,			
--			FORMAT(CAST(StartDate AS DATE),''yyyyMMdd''),
--			(BrandName+'' ''+Form),
--			''T'',''F'',''F'',
--			@ClinicalID_New
--			FROM   #NewMeds
--			WHERE  ROWNUMBER=@loop

--			SET @loop = @loop + 1 
			
			
--		END

--		IF OBJECT_ID(''tempdb..#NewMeds'') IS NOT NULL  
--					DROP TABLE #NewMeds 

 
--		UPDATE P 
--		set P.FindingDetail=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TMP.FD,''EyeContext'',
--		 (CASE WHEN R.[Route] like ''%right eye'' THEN ''OD'' WHEN R.[Route] like ''%left eye%'' THEN ''OS'' WHEN R.[Route] like ''%both eyes'' THEN ''OU'' ELSE '''' END)),
--		''PRN'',(CASE WHEN R.Refills=''PRN'' THEN ''PRN'' ELSE ''''END)),''Duration'',
--		(CASE WHEN R.Duration LIKE ''[0-9]'' THEN R.Duration+'' days'' ELSE TMP.FindingDuration END))
--		,''Refills'',CASE WHEN R.Refills=''PRN'' THEN '''' ELSE R.Refills+'' ''+''REFILLS'' END),''QtyUnit'',R.Quantity+'' ''+R.QuantityUnit),''PO'',
--		(Case WHEN R.[Action]=''Apply'' THEN ''APPLY TO LIDS'' WHEN R.[Action]=''INSTILL'' THEN ''INSTILL IN EYES'' WHEN R.[Action]='''' THEN  ''PO'' ELSE R.[Route] END)),
--		''Frequency'',R.Frequency),''Strength'',R.Strength),''Dosage'',R.Dose+'' ''+ R.DoseUnit)  
--		,P.ImageDescriptor=CASE WHEN Left(Right(P.ImageDescriptor, 11),1) = ''!''  THEN REPLACE(P.ImageDescriptor,Right(P.ImageDescriptor, 10),R.SigChangedDate) ELSE P.ImageDescriptor END
--		FROM #Medication Tmp 
--		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
--		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId

--		--For ChangeERXdate 
--		Update P set
--		--P.ImageDescriptor=CASE WHEN LEFT(P.ImageDescriptor, 1)<>''D'' THEN ''D''+R.StopDate+P.ImageDescriptor ELSE P.ImageDescriptor END
--		P.ImageDescriptor=CASE 
--							WHEN R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) THEN ''D''+R.StopDate+P.ImageDescriptor 
--							ELSE STUFF(P.ImageDescriptor , 1, 1, ''D'')
--						  END
----		P.ImageDescriptor=CASE WHEN R.StopDate<>Right(P.ImageDescriptor, 10) AND  Left(Right(P.ImageDescriptor, 11),1) = ''!''  THEN ''D''+R.StopDate+P.ImageDescriptor ELSE P.ImageDescriptor END
--		--,P.ImageDescriptor=''D''+StopDate+P.ImageDescriptor
--		FROM #Medication Tmp 
--		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
--		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId WHERE R.StopDate<>'''' AND R.Deleted=''n''

--		Update P set
--		P.ImageDescriptor=CASE 
--							WHEN --R.StopDate<>Right(P.ImageDescriptor, 10) AND 
--								R.StopDate<>RIGHT(Left(P.ImageDescriptor, 11),10) 
--							THEN ''!''+R.StopDate+P.ImageDescriptor 
--							ELSE P.ImageDescriptor 
--						END		
--		,P.Status=''D''
--		FROM #Medication Tmp 
--		INNER JOIN #Response R ON Tmp.ClinicalId=R.ClinicalID
--		INNER JOIN dbo.PatientClinical P ON  P.ClinicalId=Tmp.ClinicalId
--		WHERE R.Deleted=''y''
		
--		----For-Patiet Note
--		UPDATE  PN  SET  PN.Note1=R.PatientNotes
--		FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID

--		--For Phrma note 
--		UPDATE  PN  SET  PN.Note1=R.OtherNotes
--		FROM  PatientNotes PN INNER JOIN #Response R ON PN.ClinicalId=R.ClinicalID
	


--		IF OBJECT_ID(''tempdb..#Medication'') IS NOT NULL  
--			DROP TABLE #Medication 
	
--		IF OBJECT_ID(''tempdb..#RxSegments'') IS NOT NULL  
--			DROP TABLE #RxSegments 

--		IF OBJECT_ID(''tempdb..#Response'') IS NOT NULL  
--					DROP TABLE #Response 

					

	
--SET NOCOUNT OFF
--' 

--END
--GO

/****** Object:  StoredProcedure [model].[CCDA_CreateNewPatient] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_CreateNewPatient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [model].[CCDA_CreateNewPatient]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[model].[CCDA_CreateNewPatient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


CREATE Procedure [model].[CCDA_CreateNewPatient](@FirstName nvarchar(500), @LastName nvarchar(500), @DateOfBirth nvarchar(500), @LanguageCode nvarchar(500), @RaceCode nvarchar(500), @EthnicityCode nvarchar(500), @GenderCode nvarchar(500), @AddressLine1 nvarchar(500), @AddressLine2 nvarchar(500), @City nvarchar(500), @State nvarchar(500), @ZipCode nvarchar(500))
As
Begin
  
  Declare @PatientId as nvarchar(25)

  Declare @GenderId as numeric(18,0)
  Set @GenderId = (Select Top(1) Id From model.SexType_Enumeration Where Code = @GenderCode)

  Insert into model.Patients(LastName, FirstName, DateOfBirth ,IsClinical, IsCollectionLetterSuppressed, IsHipaaConsentSigned, PatientStatusId, PaymentOfBenefitsToProviderId, SexId) Values (@LastName, @FirstName, @DateOfBirth, 1, 0, 1, 1, 1, @GenderId)


  SET @PatientId = (Select Cast(SCOPE_IDENTITY() as nvarchar(25)))

  If(@PatientId != '''') 
  Begin
     -- Language Code
     if(LTRIM(RTRIM((@LanguageCode))) <> '''')
	 Begin
        if(LTRIM(RTRIM((@LanguageCode))) = ''en-US'')
		Begin
		   Set @LanguageCode = ''en''
		End
	     
		Declare @LanguageId int
		Set @LanguageId = (Select Top(1) Id From model.Languages Where Abbreviation = @LanguageCode)
	   	If(@LanguageId <> '''')
		Begin
		   Insert into model.PatientLanguages(PatientId, LanguageId) Values (@PatientId, @LanguageId) 
		End 
	 End
	 
	 -- Race Code
	 if(LTRIM(RTRIM((@RaceCode))) <> '''')
	 Begin
        Declare @RaceId int
		Set @RaceId = (Select Top(1) Id From model.Races Where Code = @RaceCode)
	   	If(@RaceId <> '''')
		Begin
		   Insert into model.PatientRace(Patients_Id, Races_Id) Values (@PatientId, @RaceId) 
		End 
	 End   
	 
	 -- Ethnicities Code
	 if(LTRIM(RTRIM((@EthnicityCode))) <> '''')
	 Begin
        Declare @EthnicityCodeId int
		Set @EthnicityCodeId = (Select Top(1) Id From model.Ethnicities Where Code = @EthnicityCode)
	   	If(@EthnicityCodeId <> '''')
		Begin
		   Insert into model.PatientEthnicities(PatientId, EthnicityId) Values (@PatientId, @EthnicityCodeId) 
		End 
	 End     

	 -- Address Code
	    Declare @StateOrProvinceId int
		Set @StateOrProvinceId = (Select Top(1) Id From model.StateOrProvinces Where Abbreviation = @State)
		if(@StateOrProvinceId <> '''')
		Begin
		Insert into model.PatientAddresses(OrdinalId, City, Line1, Line2, PatientId, PostalCode, StateOrProvinceId, PatientAddressTypeId) Values (1, @City, @AddressLine1, NULL, @PatientId, @ZipCode, @StateOrProvinceId, 1)
		End
	 
	 Select @PatientId as ''PatientId''
	      
  End
End
' 

END
GO

-----------------------------------------------------------------------------------
/****** Object:  StoredProcedure [dbo].[USP_RulesEngine_GetRulesLatest] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_RulesEngine_GetRulesLatest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_RulesEngine_GetRulesLatest]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_RulesEngine_GetRulesLatest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


CREATE PROCEDURE [dbo].[USP_RulesEngine_GetRulesLatest] (@userId int)

AS              

BEGIN       

Select IE.* from IE_Rules IE

inner join IE_RulesPermissions IRP 

on IRP.ruleid=ie.ruleid and IRP.userid=@userId AND IRP.Intervention=1

LEFT OUTER  JOIN encounter_ie_overrides IER 

ON IE.RULEID=IER.RULEID 

WHERE CONVERT(NVARCHAR,IER.Comments)='''' 

OR CONVERT(NVARCHAR,IER.Comments) IS NULL

END  

' 

END
GO

/****** Object:  StoredProcedure [dbo].[USP_PloblemList] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_PloblemList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_PloblemList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_PloblemList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

Create PROCEDURE [dbo].[USP_PloblemList]
 @IcdOrDiagnosis VARCHAR(100)='''',
 @Flag VARCHAR(10)='''',
 @Icd10Id  VARCHAR(20)=''''
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

    IF(@Flag=''icd10'')
 BEGIN
  SELECT  Distinct ICD10 , ICD10DESCR AS Diagnosis, CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as [Sataus],  COMMENTS FROM dbo.SNOMED_2016  WHERE ICD10 LIke ''%''+ @IcdOrDiagnosis+''%''
 END
 ELSE IF(@Flag=''diagnosis'')
 BEGIN
  SELECT  Distinct ICD10 , ICD10DESCR AS Diagnosis, CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as [Sataus], COMMENTS FROM dbo.SNOMED_2016  WHERE ICD10DESCR LIke +''%''+ @IcdOrDiagnosis +''%''
 End 
 ELSE IF(@Flag=''all'')
 BEGIN
  SELECT  Distinct ICD10 , ICD10DESCR AS Diagnosis , CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as [Sataus], COMMENTS FROM dbo.SNOMED_2016 
 END
 ELSE IF(@Icd10Id<>'''')
 BEGIN
  SELECT ConceptID, Term  as [SNOMED CT Description] FROM dbo.SNOMED_2016 where icd10=@Icd10Id
 END
 

END
' 

END
GO

/****** Object:  StoredProcedure [NTP_ITS_AUDIT_LOG] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NTP_ITS_AUDIT_LOG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [NTP_ITS_AUDIT_LOG]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NTP_ITS_AUDIT_LOG]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

create procedure [NTP_ITS_AUDIT_LOG] (@ITSServerName VARCHAR(200), @ITSServerIP VARCHAR(20), @NTPServerName VARCHAR(200), 
@NTPServerIP VARCHAR(20), @NTPVersion VARCHAR(10), @Button CHAR(1), @FileName varchar(1000) = null)
AS BEGIN
--C-Configure  S-Start  L-Save Log, D-Stop
IF(@Button = ''C'')
BEGIN
INSERT INTO AUDITTRAILEVENT(EVENTID,EVENTACTIONCODE,EVENTDATETIME,EVENTOUTCOMEINDICATOR,EVENTOBJECTTRIGGER,
USERID,USERNAME,USERISREQUESTOR,ROLEIDCODE,COMPUTERNAME,EVENTDATETIME_UTC)
VALUES
(''Security Alert'',''E'',GETDATE(),
''ITS Server Name - '' + @ITSServerName + 
'', ITS Server IP -'' + @ITSServerIP + 
'' with '' +
'' NTP Server Name -'' + @NTPServerName +
'' , NTP Server IP - '' + @NTPServerIP +
''and NTP Version - ''+ @NTPVersion +
'' Configured Successfully'',
''MDoffice --> Admin --> NTP Configuration - Configure'',
1,''Admin'', ''Y'',1,@NTPServerName,CONVERT(VARCHAR(19), GETUTCDATE(), 120)+'' UTC'')
END

IF(@Button = ''D'')
BEGIN
INSERT INTO AUDITTRAILEVENT(EVENTID,EVENTACTIONCODE,EVENTDATETIME,EVENTOUTCOMEINDICATOR,EVENTOBJECTTRIGGER,
USERID,USERNAME,USERISREQUESTOR,ROLEIDCODE,COMPUTERNAME,EVENTDATETIME_UTC)
VALUES
(''Security Alert'',''E'',GETDATE(),
''ITS Server Name - '' + @ITSServerName + 
'', ITS Server IP -'' + @ITSServerIP + 
'' with '' +
'' NTP Server Name -'' + @NTPServerName +
'' , NTP Server IP - '' + @NTPServerIP +
'' and NTP Version - ''+ @NTPVersion +
'' Stopped Successfully'',
''MDoffice --> Admin --> NTP Configuration - Stop'',
1,''Admin'', ''Y'',1,@NTPServerName,CONVERT(VARCHAR(19), GETUTCDATE(), 120)+'' UTC'')
END

IF(@Button = ''S'')
BEGIN
INSERT INTO AUDITTRAILEVENT(EVENTID,EVENTACTIONCODE,EVENTDATETIME,EVENTOUTCOMEINDICATOR,EVENTOBJECTTRIGGER,
USERID,USERNAME,USERISREQUESTOR,ROLEIDCODE,COMPUTERNAME,EVENTDATETIME_UTC)
VALUES
(''Security Alert'',''E'',GETDATE(),
''ITS Server Name - '' + @ITSServerName + 
'', ITS Server IP -'' + @ITSServerIP + 
'' with '' +
'' NTP Server Name -'' + @NTPServerName +
'' , NTP Server IP - '' + @NTPServerIP +
'' and NTP Version - ''+ @NTPVersion +
'' Started Successfully'',
''MDoffice --> Admin --> NTP Configuration - Start'',
1,''Admin'', ''Y'',1,@NTPServerName,CONVERT(VARCHAR(19), GETUTCDATE(), 120)+'' UTC'')
END


IF(@Button = ''L'')
BEGIN
INSERT INTO AUDITTRAILEVENT(EVENTID,EVENTACTIONCODE,EVENTDATETIME,EVENTOUTCOMEINDICATOR,EVENTOBJECTTRIGGER,
USERID,USERNAME,USERISREQUESTOR,ROLEIDCODE,COMPUTERNAME,EVENTDATETIME_UTC)
VALUES
(''Security Alert'',''E'',GETDATE(),
''ITS Server Name - '' + @ITSServerName + 
'', ITS Server IP -'' + @ITSServerIP + 
'' with '' +
'' NTP Server Name -'' + @NTPServerName +
'' , NTP Server IP - '' + @NTPServerIP +
'' and NTP Version - ''+ @NTPVersion +
'' Log saved with file name - ''+ @FileName+'' Successfully'',
''MDoffice --> Admin --> NTP Configuration - Save Log'',
1,''Admin'', ''Y'',1,@NTPServerName,CONVERT(VARCHAR(19), GETUTCDATE(), 120)+'' UTC'')
END

END
CREATE TABLE [dbo].[AUDITTRAILEVENT](
	[EVENTID] [varchar](100) NULL,
	[EVENTACTIONCODE] [varchar](100) NULL,
	[EVENTDATETIME] [datetime] NULL,
	[EVENTOUTCOMEINDICATOR] [varchar](max) NULL,
	[EVENTOBJECTTRIGGER] [varchar](100) NULL,
	[USERID] [int] NULL,
	[USERNAME] [varchar](100) NULL,
	[USERISREQUESTOR] [varchar](100) NULL,
	[ROLEIDCODE] [varchar](100) NULL,
	[COMPUTERNAME] [varchar](100) NULL,
	[EVENTDATETIME_UTC] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
' 

END
GO

/****** Object:  StoredProcedure [dbo].[USP_FunctionalStatus] ******/
-----------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_FunctionalStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_FunctionalStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_FunctionalStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[USP_FunctionalStatus]
	@snomed VARCHAR(100)='''',
	@Flag VARCHAR(10)=''''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF(@Flag=''snomed'')
	BEGIN
		SELECT  DISTINCT ConceptID as Col1, Term  as Col2  , CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as Col3 FROM dbo.SNOMED_2016  WHERE ConceptID =@snomed
	END
	ELSE IF(@Flag=''snomeddesc'')
	BEGIN
		SELECT  DISTINCT ConceptID as Col1, Term  as Col2  , CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as [Sataus] FROM dbo.SNOMED_2016  WHERE Term LIke + @snomed +''%''
	End 
	ELSE IF(@Flag=''all'')
	BEGIN
		SELECT    DISTINCT Top 1000  ConceptID as Col1, Term  as Col2 ,  CASE WHEN Active=1 THEN ''Active'' ELSE ''InActive'' END as Col3 FROM dbo.SNOMED_2016 
	END
	
END' 
END
GO
SET ANSI_PADDING ON
GO
