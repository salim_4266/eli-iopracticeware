SET QUOTED_IDENTIFIER OFF 

INSERT INTO [model].[SexualOrientationType_Enumeration]([Id],[Description],[SNOMEDCT],[HL7Code],[IsArchived]) 
VALUES (1,	'Lesbian, gay or homosexual',	'38628009',	'38628009',	'0')

INSERT INTO [model].[SexualOrientationType_Enumeration]([Id],[Description],[SNOMEDCT],[HL7Code],[IsArchived]) 
VALUES (2,	'Straight or heterosexual',	'20430005',	'2043005',	'0')

INSERT INTO [model].[SexualOrientationType_Enumeration]([Id],[Description],[SNOMEDCT],[HL7Code],[IsArchived])  
VALUES (3,	'Bisexual',	'42035005',	'42035005',	'0')

INSERT INTO [model].[SexualOrientationType_Enumeration]([Id],[Description],[SNOMEDCT],[HL7Code],[IsArchived]) 
VALUES (4,	'Something else, please describe',	'NULL',	'OTH',	0)

INSERT INTO [model].[SexualOrientationType_Enumeration]([Id],[Description],[SNOMEDCT],[HL7Code],[IsArchived]) 
VALUES (5,	'Don''t know',	'NULL',	'UNK',	'0')

INSERT INTO [model].[SexualOrientationType_Enumeration]([Id],[Description],[SNOMEDCT],[HL7Code],[IsArchived])  
VALUES (6,	'Choose not to disclose',	'NULL',	'ASKU',	'0')

SET ANSI_PADDING ON
GO