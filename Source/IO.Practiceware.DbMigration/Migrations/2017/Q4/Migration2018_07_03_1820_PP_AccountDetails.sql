

/****** Object:  Table [MVE].[PP_AccountDetails]    Script Date: 06/29/2018 13:18:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_AccountDetails]') AND type in (N'U'))
BEGIN

CREATE TABLE [MVE].[PP_AccountDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountName] [varchar](50) NOT NULL,
	[AccountOwnerExternalId] [varchar](50) NOT NULL,
	[AccountOwnerFirstName] [varchar](50) NOT NULL,
	[AccountOwnerMiddleName] [varchar](50) NOT NULL,
	[AccountOwnerLastName] [varchar](50) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[AccountOwnerEmailAddress] [varchar](50) NOT NULL,
	[PartnerKey] [varchar](50) NOT NULL,
	[AccountId] [varchar](50) NOT NULL,
	[AccountNumber] [varchar](50) NOT NULL,
	[SynchronizationUserName] [varchar](50) NOT NULL,
	[SynchronizationPassword] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LinkedInUrl] [nvarchar](1000) NULL,
	[YahooUrl] [nvarchar](1000) NULL,
	[GooglePlusUrl] [nvarchar](1000) NULL,
	[FacebookUrl] [nvarchar](1000) NULL,
	[AppointmentAllowedTimeInAdvance] [timestamp] NOT NULL,
	[UsesForms] [bit] NULL,
	[OnlineAppointmentsActive] [bit] NULL,
 CONSTRAINT [PK_PP_AccountDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO



IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountName]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_AccountName]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountName]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_AccountName]  DEFAULT ('') FOR [AccountName]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountOwnerExternalId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_AccountOwnerExternalId]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountOwnerExternalId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_AccountOwnerExternalId]  DEFAULT ('') FOR [AccountOwnerExternalId]
END
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountOwnerFirstName]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_AccountOwnerFirstName]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountOwnerFirstName]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_AccountOwnerFirstName]  DEFAULT ('') FOR [AccountOwnerFirstName]
END
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountOwnerMiddleName]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_AccountOwnerMiddleName]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountOwnerMiddleName]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_AccountOwnerMiddleName]  DEFAULT ('') FOR [AccountOwnerMiddleName]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountOwnerLastName]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_AccountOwnerLastName]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountOwnerLastName]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_AccountOwnerLastName]  DEFAULT ('') FOR [AccountOwnerLastName]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_Username]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_Username]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_Username]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_Username]  DEFAULT ('') FOR [Username]
END
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_Password]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_Password]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_Password]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_Password]  DEFAULT ('') FOR [Password]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_PartnerKey]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_PartnerKey]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_PartnerKey]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_PartnerKey]  DEFAULT ('') FOR [PartnerKey]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_AccountId]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountId]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_AccountId]  DEFAULT ('') FOR [AccountId]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountNumber]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_AccountNumber]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_AccountNumber]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_AccountNumber]  DEFAULT ('') FOR [AccountNumber]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_SynchronizationUserName]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_SynchronizationUserName]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_SynchronizationUserName]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_SynchronizationUserName]  DEFAULT ('') FOR [SynchronizationUserName]
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_SynchronizationUserName]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_SynchronizationUserName]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_SynchronizationUserName]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_SynchronizationUserName]  DEFAULT ('') FOR [SynchronizationUserName]
END
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_IsActive]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] DROP CONSTRAINT [DF_PP_AccountDetails_IsActive]
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mve].[DF_PP_AccountDetails_IsActive]')) 
BEGIN
	ALTER TABLE [MVE].[PP_AccountDetails] ADD  CONSTRAINT [DF_PP_AccountDetails_IsActive]  DEFAULT ((0)) FOR [IsActive]
END
GO



If not Exists(select * from information_schema.columns where table_name = 'PP_AccountDetails' 
and column_name = 'LinkedInUrl' 
and Table_schema = 'mve'
and column_default is NOT NULL)
begin
ALTER TABLE [MVE].[PP_AccountDetails] ADD  DEFAULT ('') FOR [LinkedInUrl]
end
GO


If not Exists(select * from information_schema.columns where table_name = 'PP_AccountDetails' 
and column_name = 'GooglePlusUrl' 
and Table_schema = 'mve'
and column_default is NOT NULL)
begin
ALTER TABLE [MVE].[PP_AccountDetails] ADD  DEFAULT ('') FOR [GooglePlusUrl]
end
GO


If not Exists(select * from information_schema.columns where table_name = 'PP_AccountDetails' 
and column_name = 'YahooUrl' 
and Table_schema = 'mve'
and column_default is NOT NULL)
begin
ALTER TABLE [MVE].[PP_AccountDetails] ADD  DEFAULT ('') FOR [YahooUrl]
end
GO


If not Exists(select * from information_schema.columns where table_name = 'PP_AccountDetails' 
and column_name = 'FacebookUrl' 
and Table_schema = 'mve'
and column_default is NOT NULL)
begin
ALTER TABLE [MVE].[PP_AccountDetails] ADD  DEFAULT ('') FOR [FacebookUrl]
end
GO

If not Exists(select * from information_schema.columns where table_name = 'PP_AccountDetails' 
and column_name = 'UsesForms' 
and Table_schema = 'mve'
and column_default is NOT NULL)
begin
ALTER TABLE [MVE].[PP_AccountDetails] ADD  DEFAULT ('') FOR [UsesForms]
end
GO


If not Exists(select * from information_schema.columns where table_name = 'PP_AccountDetails' 
and column_name = 'OnlineAppointmentsActive' 
and Table_schema = 'mve'
and column_default is NOT NULL)
begin
ALTER TABLE [MVE].[PP_AccountDetails] ADD  DEFAULT ('') FOR [OnlineAppointmentsActive]
end
GO















 







