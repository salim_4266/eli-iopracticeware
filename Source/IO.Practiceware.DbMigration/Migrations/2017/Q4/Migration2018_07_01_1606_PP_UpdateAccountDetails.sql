

/****** Object:  StoredProcedure [MVE].[PP_UpdateAccountDetails]    Script Date: 06/29/2018 13:45:59 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_UpdateAccountDetails]')) 
Drop Procedure [MVE].[PP_UpdateAccountDetails] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  


Create procedure [MVE].[PP_UpdateAccountDetails]
(
@AccountId varchar(50),
@AccountNumber varchar(50),
@SynchronizationUserName varchar(50),
@SynchronizationPassword varchar(50),
@IsActive bit
)
AS
BEGIN

 UPDATE MVE.PP_AccountDetails set AccountId = @AccountId, AccountNumber = @AccountNumber, SynchronizationUserName = @SynchronizationUserName, SynchronizationPassword = @SynchronizationPassword, IsActive=@IsActive Where isactive=0

End

GO
