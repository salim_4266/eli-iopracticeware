
/***** Object:  StoredProcedure [MVE].[PP_GetLocationDetails]    Script Date: 5/28/2018 6:45:09 PM *****/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_GetLocationDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [MVE].[PP_GetLocationDetails]
GO

CREATE Procedure [MVE].[PP_GetLocationDetails] 
@ActionId int
As 
Begin 
Select ShortName as ContactName, Name, Case(IsArchived) When 0 Then 1 Else 0 End as Active, ShortName as Notes, Id as ExternalId From model.ServiceLocations with (nolock) Where Id not in  (Select distinct PatientNo from MVE.PP_PortalQueueResponse PQR inner join  MVE.PP_ActionTypeLookUps A on PQR.ActionTypeLookupId= A.ActionId Where PQR.ActionTypeLookupId=@ActionId Union Select distinct PatientNo from MVE.PP_PortalQueueException PQR inner join MVE.PP_ActionTypeLookUps A on PQR.ActionTypeLookupId= A.ActionId Where PQR.ActionTypeLookupId=@ActionId and ProcessCount >= 5)
End

