

/****** Object:  Table [MVE].[PP_ErrorLog]    Script Date: 06/29/2018 13:23:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_ErrorLog]') AND type in (N'U'))
BEGIN

CREATE TABLE [MVE].[PP_ErrorLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [nvarchar](100) NOT NULL,
	[MethodName] [nvarchar](200) NOT NULL,
	[ErrDescription] [nvarchar](200) NULL,
	[ErrorMessage] [varchar](max) NOT NULL,
	[createdBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

If not Exists(select * from information_schema.columns where table_name = 'PP_ErrorLog' 
and column_name = 'createdBy' 
and Table_schema = 'mve'
and column_default is NOT NULL)
begin
	ALTER TABLE [MVE].[PP_ErrorLog] ADD  DEFAULT ((0)) FOR [createdBy]
end
GO

If not Exists(select * from information_schema.columns where table_name = 'PP_ErrorLog' 
and column_name = 'CreatedOn' 
and Table_schema = 'mve'
and column_default is NOT NULL)
begin
	ALTER TABLE [MVE].[PP_ErrorLog] ADD  DEFAULT (getdate()) FOR [CreatedOn]
end
GO










