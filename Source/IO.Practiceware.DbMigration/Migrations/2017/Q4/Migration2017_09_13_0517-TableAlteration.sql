IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.Races')  AND name = 'Code')
BEGIN
	Alter Table model.Races
	Add Code nvarchar(50)
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.Races')  AND name = 'OMBCode')
BEGIN
	Alter Table model.Races
	Add OMBCode nvarchar(50)
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.Races')  AND name = 'OMBCategory')
BEGIN
	Alter Table model.Races
	Add OMBCategory nvarchar(500)
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'Model.Ethnicities')  AND name = 'Code')
BEGIN
	Alter Table Model.Ethnicities
	Add Code nvarchar(20)
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'Model.Ethnicities')  AND name = 'OMBCode')
BEGIN
	Alter Table Model.Ethnicities
	Add OMBCode nvarchar(20)
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'Model.Ethnicities')  AND name = 'OMBCategory')
BEGIN
	Alter Table Model.Ethnicities
	Add OMBCategory nvarchar(500)
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'Model.Patients')  AND name = 'SexId')
BEGIN
	Alter Table Model.Patients
	Add SexId int 
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'Model.Patients')  AND name = 'SexualOrientationId')
BEGIN
	Alter Table Model.Patients
	Add SexualOrientationId int
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'Model.Patients')  AND name = 'RaceAndEthnicityId')
BEGIN
	Alter Table Model.Patients
	Add RaceAndEthnicityId int
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'dbo.IE_Rules')  AND name = 'EventRaiseID')
BEGIN
	Alter Table IE_Rules
	Add EventRaiseID int
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'dbo.IE_Rules')  AND name = 'ReferenceDescription')
BEGIN
	Alter Table IE_Rules
	Add ReferenceDescription nvarchar(2000)
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'dbo.IE_Rules')  AND name = 'BibliographicsCitation')
BEGIN
	Alter Table IE_Rules
	Add BibliographicsCitation nvarchar(2000)
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.SmokingConditions')  AND name = 'SNOMEDCode')
BEGIN
	Alter Table [model].[SmokingConditions]
	Add SNOMEDCode nvarchar(50)
End
IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.patients')  AND name = 'RCopiaID')
BEGIN
	Alter table model.patients 
	Add RCopiaID nvarchar(50)
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.PatientReconcileMedications')  AND name = 'MergedDate')
BEGIN
	Alter table model.PatientReconcileMedications 
	Add MergedDate datetime
End

GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[PatientDemographics]
AS
  SELECT id                                                       AS PatientId,
         CASE
           WHEN defaultuserid IS NULL THEN 0
           ELSE defaultuserid
         END                                                      AS
            SchedulePrimaryDoctor,
         Replace(Isnull(firstname, ''), '''', '')                 AS FirstName,
         CASE
           WHEN isclinical = 0 THEN 'I'
           WHEN patientstatusid = 2 THEN 'C'
           WHEN patientstatusid = 3 THEN 'D'
           WHEN patientstatusid = 4 THEN 'M'
           ELSE 'A'
         END                                                      AS Status,
         Replace(Isnull(lastname, ''), '''', '')                  AS LastName,
         Replace(Isnull(middlename, ''), '''', '')                AS
         MiddleInitial
            ,
         Isnull(prefix, '')                                       AS
            Salutation,
         Isnull(suffix, '')                                       AS
         NameReference
            ,
         CASE
           WHEN dateofbirth IS NULL THEN CONVERT(NVARCHAR(64), '')
           ELSE Datename(yyyy, dateofbirth)
                + RIGHT('00' + CONVERT(NVARCHAR(2), Datepart(mm, dateofbirth)),
                2)
                + RIGHT('00' + CONVERT(NVARCHAR(2), Datepart(dd, dateofbirth)),
                2)
         END                                                      AS BirthDate,
         CASE
           WHEN sexid = 1 THEN 'M'
           WHEN sexid = 2 THEN 'F'
           WHEN sexid = 3 THEN 'U'
           WHEN sexid = 4 THEN 'O'
           ELSE ''
         END                                                      AS Gender,
         CASE
           WHEN maritalstatusid = 1 THEN 'S'
           WHEN maritalstatusid = 2 THEN 'M'
           WHEN maritalstatusid = 3 THEN 'D'
           WHEN maritalstatusid = 4 THEN 'W'
           ELSE ''
         END                                                      AS Marital,
         Isnull(occupation, '')                                   AS Occupation,
         Isnull(priorpatientcode, '')                             AS OldPatient,
         ''                                                       AS
            ReferralRequired,
         Isnull(socialsecuritynumber, '')                         AS
            SocialSecurity,
         Isnull(employername, '')                                 AS
         BusinessName,
         CASE
           WHEN ishipaaconsentsigned = CONVERT(BIT, 1) THEN 'T'
           WHEN ishipaaconsentsigned = CONVERT(BIT, 0) THEN 'F'
           ELSE CONVERT(NVARCHAR(1), NULL)
         END                                                      AS Hipaa,
         Isnull(ethnicity, 'Declined to Specify')                 AS Ethnicity,
         Isnull(language, 'Declined to Specify')                  AS Language,
         Isnull(race, 'Declined to Specify')                      AS Race,
         Isnull(businesstype, '')                                 AS
         BusinessType,
         Isnull(address, '')                                      AS Address,
         Isnull(suite, '')                                        AS Suite,
         Isnull(city, '')                                         AS City,
         Isnull(state, '')                                        AS State,
         Isnull(zip, '')                                          AS Zip,
         Isnull(businessaddress, '')                              AS
            BusinessAddress,
         Isnull(businesssuite, '')                                AS
         BusinessSuite
            ,
         Isnull(businesscity, '')                                 AS
            BusinessCity,
         Isnull(businessstate, '')                                AS
         BusinessState
            ,
         Isnull(businesszip, '')                                  AS
            BusinessZip,
         Isnull(homephone, '')                                    AS HomePhone,
         Isnull(cellphone, '')                                    AS CellPhone,
         Isnull(businessphone, '')                                AS
         BusinessPhone
            ,
         Isnull(employerphone, '')                                AS
            EmployerPhone,
         Isnull ((SELECT TOP (1) COALESCE (externalproviderid, 0) AS Expr1
                  FROM   model.patientexternalproviders AS pep
                  WHERE  ( patientid = p.id )
                         AND ( isreferringphysician = 1 )), '')   AS
            ReferringPhysician,
         Isnull ((SELECT TOP (1) COALESCE (externalproviderid, 0) AS Expr1
                  FROM   model.patientexternalproviders AS pep
                  WHERE  ( patientid = p.id )
                         AND ( isprimarycarephysician = 1 )), '') AS
            PrimaryCarePhysician,
         Isnull(profilecomment1, '')                              AS
            ProfileComment1,
         Isnull(profilecomment2, '')                              AS
            ProfileComment2,
         Isnull(email, '')                                        AS Email,
         ''                                                       AS
            FinancialAssignment,
         ''                                                       AS
            FinancialSignature,
         CONVERT(NVARCHAR(64), '')                                AS
            MedicareSecondary,
         CASE
           WHEN p.preferredaddressid = (SELECT TOP 1 pad.id
                                        FROM   model.patientaddresses pad
                                        WHERE  pad.patientid = p.policypatientid
                                        ORDER  BY pad.id) THEN 'Y'
           ELSE 'N'
         END                                                      AS BillParty,
         ''                                                       AS
            SecondBillParty,
         CONVERT(NVARCHAR(32), '')                                AS
            ReferralCatagory,
         CONVERT(NVARCHAR(max), '')                               AS
         EmergencyName
            ,
         CONVERT(NVARCHAR(max), '')                               AS
            EmergencyPhone,
         ''                                                       AS
         EmergencyRel,
         policypatientid,
         secondpolicypatientid,
         relationship,
         secondrelationship,
         pattype,
         CONVERT(INT, NULL)                                       AS
         BillingOffice
            ,
         CONVERT(INT, NULL)                                       AS
            PostOpExpireDate,
         CONVERT(INT, NULL)                                       AS
         PostOpService
            ,
         CONVERT(NVARCHAR(64), '')                                AS
            FinancialSignatureDate,
         ''                                                       AS Religion,
         ''                                                       AS
            SecondRelationshipArchived,
         CONVERT(INT, NULL)                                       AS
            SecondPolicyPatientIdArchived,
         ''                                                       AS
            RelationshipArchived,
         CONVERT(INT, NULL)                                       AS
            PolicyPatientIdArchived,
         ''                                                       AS BulkMailing
         ,
         ''                                                       AS
         ContactType,
         ''                                                       AS
            NationalOrigin,
         Isnull(sendstatements, 'Y')                              AS
            SendStatements,
         Isnull(sendconsults, 'Y')                                AS
         SendConsults,
		 RCopiaID
  FROM   (SELECT id,
                 lastname,
                 firstname,
                 dateofbirth,
                 billingorganizationid,
                 dateofdeath,
                 defaultuserid,
                 employername,
                 genderid,
                 honorific,
                 isclinical,
                 iscollectionlettersuppressed,
                 ishipaaconsentsigned,
                 maritalstatusid,
                 middlename,
                 nickname,
                 occupation,
                 patientemploymentstatusid,
                 ethnicityid,
                 languageid,
                 patientnationaloriginid,
                 patientreligionid,
                 patientstatusid,
                 paymentofbenefitstoproviderid,
                 prefix,
                 priorfirstname,
                 priorlastname,
                 priorpatientcode,
                 salutation,
                 socialsecuritynumber,
                 suffix,
                 releaseofinformationcodeid,
                 releaseofinformationdatetime,
                 userdefinedlastupdated,
                 preferredservicelocationid,
                 activationcode,
                 sexid,
                 sexualorientationid,
                 raceandethnicityid,
                 model.Getpairid(1, id, 110000000)                         AS
                        PrimaryCarePhysicianId,
                 Isnull(dbo.Getpolicyholderpatientid(id), id)              AS
                        PolicyPatientId,
                 Isnull(dbo.Getsecondpolicyholderpatientid(id), 0)         AS
                        SecondPolicyPatientId,
                 Isnull(dbo.Getpolicyholderrelationshiptype(id), 'Y')      AS
                        Relationship,
                 Isnull(dbo.Getsecondpolicyholderrelationshiptype(id), '') AS
                        SecondRelationship,
                 (SELECT TOP (1) COALESCE (areacode, '') + exchangeandsuffix AS
                                 Expr1
                  FROM   model.patientphonenumbers AS ppn
                  WHERE  ( patientid = p1.id )
                         AND ( patientphonenumbertypeid = 7 )
                  ORDER  BY ordinalid)                                     AS
                 HomePhone,
                 (SELECT TOP (1) COALESCE (areacode, '') + exchangeandsuffix AS
                                 Expr1
                  FROM   model.patientphonenumbers AS ppn
                  WHERE  ( patientid = p1.id )
                         AND ( patientphonenumbertypeid = 3 )
                  ORDER  BY ordinalid)                                     AS
                 CellPhone,
                 (SELECT TOP (1) COALESCE (areacode, '') + exchangeandsuffix AS
                                 Expr1
                  FROM   model.patientphonenumbers AS ppn
                  WHERE  ( patientid = p1.id )
                         AND ( patientphonenumbertypeid = 2 )
                  ORDER  BY ordinalid)                                     AS
                        BusinessPhone,
                 (SELECT TOP (1) COALESCE (areacode, '') + exchangeandsuffix AS
                                 Expr1
                  FROM   model.patientphonenumbers AS ppn
                  WHERE  ( patientid = p1.id )
                         AND ( ordinalid = 1 )
                         AND ( patientphonenumbertypeid IN
                               (SELECT id
                                FROM
                               dbo.practicecodetable AS pct
                                                            WHERE  ( code =
                               'Employer' )
                                                                   AND
                               ( referencetype =
                                 'PhoneTypePatient' )
                                                               ) ))        AS
                        EmployerPhone,
                 (SELECT TOP (1) line1
                  FROM   model.patientaddresses AS pa
                  WHERE  ( patientid = p1.id )
                         AND ( patientaddresstypeid = 1 )
                         AND ( ordinalid = 1 ))                            AS
                 Address,
                 (SELECT TOP (1) line2
                  FROM   model.patientaddresses AS pa
                  WHERE  ( patientid = p1.id )
                         AND ( patientaddresstypeid = 1 )
                         AND ( ordinalid = 1 ))                            AS
                 Suite,
                 (SELECT TOP (1) city
                  FROM   model.patientaddresses AS pa
                  WHERE  ( patientid = p1.id )
                         AND ( patientaddresstypeid = 1 )
                         AND ( ordinalid = 1 ))                            AS
                 City
                 ,
                 (SELECT TOP (1) abbreviation
                  FROM   model.stateorprovinces AS sp
                  WHERE  ( id = (SELECT TOP (1) stateorprovinceid
                                 FROM   model.patientaddresses AS pa
                                 WHERE  ( patientid = p1.id )
                                        AND ( patientaddresstypeid = 1 )
                                        AND ( ordinalid = 1 )) ))          AS
                 State,
                 (SELECT TOP (1) postalcode
                  FROM   model.patientaddresses AS pa
                  WHERE  ( patientid = p1.id )
                         AND ( patientaddresstypeid = 1 )
                         AND ( ordinalid = 1 ))                            AS
                 Zip,
                 (SELECT TOP (1) line1
                  FROM   model.patientaddresses AS pa
                  WHERE  ( patientid = p1.id )
                         AND ( patientaddresstypeid = 2 )
                         AND ( ordinalid = 1 ))                            AS
                        BusinessAddress,
                 (SELECT TOP (1) line2
                  FROM   model.patientaddresses AS pa
                  WHERE  ( patientid = p1.id )
                         AND ( patientaddresstypeid = 2 )
                         AND ( ordinalid = 1 ))                            AS
                        BusinessSuite,
                 (SELECT TOP (1) city
                  FROM   model.patientaddresses AS pa
                  WHERE  ( patientid = p1.id )
                         AND ( patientaddresstypeid = 2 )
                         AND ( ordinalid = 1 ))                            AS
                        BusinessCity,
                 (SELECT TOP (1) abbreviation
                  FROM   model.stateorprovinces AS sp
                  WHERE  ( id = (SELECT TOP (1) stateorprovinceid
                                 FROM   model.patientaddresses AS pa
                                 WHERE  ( patientid = p1.id )
                                        AND ( patientaddresstypeid = 2 )
                                        AND ( ordinalid = 1 )) ))          AS
                        BusinessState,
                 (SELECT TOP (1) postalcode
                  FROM   model.patientaddresses AS pa
                  WHERE  ( patientid = p1.id )
                         AND ( patientaddresstypeid = 2 )
                         AND ( ordinalid = 1 ))                            AS
                 BusinessZip
                        ,
                 (SELECT TOP (1) value
                  FROM   model.patientemailaddresses AS pea
                  WHERE  ( patientid = p1.id )
                         AND ( ordinalid = 1 ))                            AS
                 Email,
                 (SELECT TOP (1) value
                  FROM   model.patientcomments AS pc
                  WHERE  ( patientcommenttypeid = 2 )
                         AND ( patientid = p1.id )
                  ORDER  BY id DESC)                                       AS
                        ProfileComment1,
                 (SELECT TOP (1) comment
                  FROM   model.patientreferralsources AS rs
                  WHERE  ( patientid = p1.id )
                         AND ( comment IS NOT NULL ))                      AS
                        ProfileComment2,
                 (SELECT TOP (1) COALESCE (externalproviderid, 0) AS Expr1
                  FROM   model.patientexternalproviders AS pep
                  WHERE  ( id = p1.id ))                                   AS
                        ReferringPhysician,
                 (SELECT TOP (1) patientaddressid
                  FROM   model.patientcommunicationpreferences AS pcp
                  WHERE  ( patientid = p1.id )
                         AND ( patientcommunicationtypeid = 3 ))           AS
                        PreferredAddressId,
                 (SELECT TOP (1) displayname
                  FROM   model.tags AS pt
                  WHERE  ( id = (SELECT TOP (1) tagid
                                 FROM   model.patienttags AS ppt
                                 WHERE  ( patientid = p1.id )
                                        AND ( ordinalid = 1 )) ))          AS
                 PatType,
                 (SELECT TOP (1) NAME
                  FROM   model.ethnicities AS pe
                  WHERE  ( id = (SELECT TOP (1) ethnicityid
                                 FROM   model.patientethnicities
                                 WHERE  ( patientid = p1.id )) ))          AS
                 Ethnicity,
                 (SELECT TOP (1) Substring(NAME, 1, 10) AS Expr1
                  FROM   model.languages AS pl
                  WHERE  ( id = (SELECT TOP (1) languageid
                                 FROM   model.patientlanguages
                                 WHERE  ( patientid = p1.id )) ))          AS
                 Language,
                 (SELECT TOP (1) NAME
                  FROM   model.races
                  WHERE  ( id = (SELECT TOP (1) races_id
                                 FROM   model.patientrace AS ppr
                                 WHERE  ( patients_id = p1.id )) ))        AS
                 Race
                 ,
                 (SELECT TOP (1) Substring(NAME, 1, 1) AS Expr1
                  FROM   model.patientemploymentstatus AS pes
                  WHERE  ( id = p1.patientemploymentstatusid ))            AS
                        BusinessType,
                 (SELECT TOP (1) CASE isoptout
                                   WHEN CONVERT(BIT, 0) THEN 'Y'
                                   ELSE 'N'
                                 END AS Expr1
                  FROM   model.patientcommunicationpreferences AS pcp
                  WHERE  ( patientid = p1.id )
                         AND ( patientcommunicationtypeid = 8 ))           AS
                        SendConsults,
                 (SELECT TOP (1) CASE isoptout
                                   WHEN CONVERT(BIT, 0) THEN 'Y'
                                   ELSE 'N'
                                 END AS Expr1
                  FROM   model.patientcommunicationpreferences AS pcp
                  WHERE  ( patientid = p1.id )
                         AND ( patientcommunicationtypeid = 3 ))           AS
                        SendStatements,
						RCopiaID
          FROM   model.patients AS p1) AS p



GO


