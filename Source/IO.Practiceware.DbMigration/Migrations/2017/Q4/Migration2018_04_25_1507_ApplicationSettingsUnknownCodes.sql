--Unknown Codes default flag set to false!

If not exists(Select * From model.Applicationsettings Where Name = 'AllowUnknown')
Begin
SET ANSI_PADDING ON

Insert into model.Applicationsettings(Value, Name, MachineName, UserId) Values('0', 'AllowUnknown', NULL, NULL) 
End
GO

SET ANSI_PADDING ON
GO