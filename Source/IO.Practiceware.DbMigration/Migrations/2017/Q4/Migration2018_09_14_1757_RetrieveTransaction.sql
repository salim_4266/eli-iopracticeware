IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[dbo].[RetrieveTransactions]')) 
Drop Procedure [dbo].[RetrieveTransactions] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[RetrieveTransactions] 
(
@SAptDate varchar(8),
@EAptDate varchar(8),
@LocId Int,
@ResId Int,
@PatId Int,
@TStat varchar(1)
)
AS
SET NOCOUNT ON
SELECT DISTINCT  
	Appointments.AppointmentId, Appointments.AppDate, Appointments.AppTime, 
	Appointments.Comments, Appointments.ScheduleStatus, Appointments.ReferralId, 
	AppointmentType.AppointmentType, AppointmentType.Question, Appointments.ActivityStatus, 
	Appointments.Comments, PatientDemographics.Salutation, 	Appointments.ConfirmStatus, 
	PatientDemographics.LastName, PatientDemographics.FirstName, PatientDemographics.MiddleInitial, 
	PatientDemographics.NameReference, PatientDemographics.Gender, PatientDemographics.BirthDate, 
	PatientDemographics.HomePhone, PatientDemographics.BusinessPhone, PatientDemographics.CellPhone,
	PatientDemographics.ReferralRequired, Resources.ResourceName, PatientDemographics.PatientId
	,ppn.OrdinalId AS PhoneSequence 
	,ppn.patientphonenumbertypeid as PhoneType 
FROM Resources 
INNER JOIN (PatientDemographics 
LEFT JOIN (AppointmentType 
INNER JOIN Appointments 
ON AppointmentType.AppTypeId = Appointments.AppTypeId) 
ON PatientDemographics.PatientId = Appointments.PatientId) 
ON Resources.ResourceId = Appointments.ResourceId1
left join model.patientphonenumbers ppn
	on ppn.patientid = PatientDemographics.patientid
WHERE ((PatientDemographics.PatientId = @PatId) Or (@PatId < 1)) And
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1)) And
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) And
((Appointments.ScheduleStatus = @TStat) Or (@TStat = '0')) And
((Appointments.AppDate <= @SAptDate) Or (@SAptDate = '0')) And
((Appointments.AppDate >= @EAptDate) Or (@EAptDate = '0')) AND PPN.OrdinalId = 1
ORDER BY Appointments.AppDate DESC , Appointments.AppTime
SET NOCOUNT OFF


GO


