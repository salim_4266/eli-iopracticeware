GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION_ASSOCIATEDPHYSICIAN')) 
Drop View VIEW_ACI_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION_ASSOCIATEDPHYSICIAN 
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION_ASSOCIATEDPHYSICIAN]
AS
     SELECT  ResourceId AS ECACCOUNTNUMBER,
			ResourceType,
            RESOURCEID
     FROM dbo.Resources
	where resourcetype not in (''R'')'

GO


------------------JSON-16----------------------

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION')) 
Drop View VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION 
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[VIEW_ACI_JSON16_MEDICATIONSORDER_MEDICATION]
AS



       SELECT 
		''Medication'' AS RESOURCETYPE, 	
		pc.eRx_Medicine_Response_Key as ExternalId, --R
		'' as ECNPI, --R
		'' as ECTIN, --R
		PC.BrandName AS DrugName, --R
		pc.RxnormID AS DRUGCODE,
		''2.16.840.1.113883.6.88'' AS DRUGCODESYSTEM, --R
        ''RxNorm'' AS DRUGCODESYSTEMNAME, --R
		CASE (select top 1 b.focus from fdb.tblCompositeDrug a join dbo.drug b on b.DisplayName=a.MED_routed_df_med_id_desc where a.medid = pc.FirstDataBankMedID)
		WHEN ''1'' THEN ''Ocular''
		ELSE ''Systemic'' end as MedicationType, --R
		--CASE SUBSTRING(pc.findingdetail,(CHARINDEX ( ''-4/'',pc.findingdetail )+4) ,1) 
		--	WHEN '')'' THEN ''NEW''
		--	WHEN ''0'' THEN ''NEW''
		--	ELSE ''Refill''
		--END 
		CASE (select count(*) from drfirst.eRx_Medicine_Response a where a.PrescriptionRcopiaID = pc.PrescriptionRcopiaID)
		WHEN ''1'' THEN ''NEW''
		ELSE ''Refill''
		END as PrescriptionType, --R
		pc.Action+'' ''+pc.Dose+'' ''+pc.DoseUnit+'' ''+pc.Route+'' ''+pc.Frequency AS Directions,--R
		pc.OtherNotes+'' ''+pc.PatientNotes AS AdditionalInstructions,--R

		pc.Strength as Strength,
		''Medication'' AS SUBSTITUTION, --R
		(CASE WHEN DATEPART(HOUR, (CONVERT(VARCHAR(20), pc.StartDate, 101))) > 12 
		THEN
		(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''PM''
		ELSE
		(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''AM''

		END) AS StartDate, --R

		(CASE WHEN DATEPART(HOUR, (CONVERT(VARCHAR(20), pc.StartDate, 101))) > 12 
		THEN
		(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''PM''
		ELSE
		(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''AM''

		END) AS EffectiveDate, --R

		--(CONVERT(VARCHAR(20), pc.StartDate, 101)) +''AM'' AS EffectiveDate, --R
		
		ISNULL(pc.Duration,''1'') +'' Days'' as DaySupply,--R
		ISNULL(pc.Quantity,''1'')	as DosageQuantityValue,--R  NEED TO CHECK FOR VALUE AND UNITS
		''TRUE'' AS IsPrescribed,

		(CASE WHEN DATEPART(HOUR, (CONVERT(VARCHAR(20), pc.StartDate, 101))) > 12 
		THEN
		(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''PM''
		ELSE
		(CONVERT(VARCHAR(20), pc.StartDate, 101))  + ''AM''

		END) AS EprescribedDate,
		--(CONVERT(VARCHAR(20), pc.StartDate, 101)) +''AM'' AS EprescribedDate,	

		''True'' AS ISQUERIEDFORDRUGFORMULARY, --R
		''True'' AS  ISCPOE, --R
		''False'' AS ISHIGHRISKMEDICATION, --R
		CASE
		WHEN DrugSchedule in(''2'',''3'',''4'',''1'',''5'',''6'',''7'',''8'') THEN ''TRUE''
		ELSE ''False''
		END  AS ISCONTROLSUBSTANCE, --R
		ISNULL(pc.pharmacyname,''NOT AVAILABLE'') AS PHARMACYNAME, --O
		CASE ISNULL(pc.StopDate,'''')
		WHEN '''' THEN ''''
		ELSE (CASE WHEN DATEPART(HOUR, (CONVERT(VARCHAR(20), pc.StopDate, 101))) > 12 
		THEN
		(CONVERT(VARCHAR(20), pc.StopDate, 101))  + ''PM''
		ELSE
		(CONVERT(VARCHAR(20), pc.StopDate, 101))  + ''AM''

		END)   --C	
		END AS DateEnd,  --C	
		--right(pc.imagedescriptor,10)+'' am'' 
		''''AS ReconciledDate, --C----------------		
		
		CASE
			WHEN ISNULL(PC.StopDate,'''') ='''' THEN ''ACTIVE''
			ELSE ''INACTIVE''
		END AS Status, --R	
		pc.PrescriptionRcopiaID,
		PC.AppointmentId,
		PC.Patient_ExtID,
		eRx_Medicine_Response_Key

		from
		drfirst.eRx_Medicine_Response PC WITH(NOLOCK)
	WHERE  pc.PrescriptionRcopiaID  is not null and rxnormid is not null'

GO
