IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.FamilyHistory')  AND name = 'Notes')
BEGIN
	Alter Table [model].[FamilyHistory]
	Add Notes nvarchar(500)
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.PatientReconcileAllergy')  AND name = 'AppointmentId')
BEGIN
	Alter Table model.PatientReconcileAllergy
	Add AppointmentId int
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.PatientReconcileProblems')  AND name = 'AppointmentId')
BEGIN
	Alter Table model.PatientReconcileProblems
	Add AppointmentId int
End

IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.PatientReconcileMedications')  AND name = 'AppointmentId')
BEGIN
	Alter Table model.PatientReconcileMedications
	Add AppointmentId int
End
IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.PatientReconcileAllergy')  AND name = 'AllergyStatus')
	BEGIN
		Alter Table model.PatientReconcileAllergy
		Add AllergyStatus varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table model.PatientReconcileAllergy
		ALTER COLUMN AllergyStatus varchar(500) NULL
	END
	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.PatientReconcilePracticeInformation')  AND name = 'PatientType')
	BEGIN
		Alter Table model.PatientReconcilePracticeInformation
		Add PatientType varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table model.PatientReconcilePracticeInformation
		ALTER COLUMN PatientType varchar(500) NULL
	END
