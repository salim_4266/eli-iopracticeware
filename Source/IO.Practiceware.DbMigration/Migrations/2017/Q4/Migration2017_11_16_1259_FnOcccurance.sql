/****** Object:  UserDefinedFunction [dbo].[fn_Occurences]    Script Date: 11/29/2017 5:56:14 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_Occurences]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_Occurences]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Occurences]    Script Date: 11/29/2017 5:56:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_Occurences]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'


CREATE   FUNCTION [dbo].[fn_Occurences] 
(
    @pattern varchar(255),
    @expression varchar(max),
	@occurenceNo int
)
RETURNS int
AS
BEGIN

DECLARE @Result int = 0;

		DECLARE @String nvarchar(max) = @expression
;WITH CTE as (
SELECT CHARINDEX(@pattern,@String) as SpaceIndex
UNION ALL 
SELECT CHARINDEX(@pattern,@String,SpaceIndex+1) FROM CTE
WHERE CHARINDEX(@pattern,@String,SpaceIndex+1) > 0 
)
, CTE2 as (
SELECT 
  * 
, ROW_NUMBER() OVER (ORDER BY SpaceIndex) AS RN
FROM CTE
)
SELECT @Result=SpaceIndex 
FROM CTE2
WHERE RN = @occurenceNo

  RETURN @Result
End

' 
END

GO
SET ANSI_PADDING ON
GO
