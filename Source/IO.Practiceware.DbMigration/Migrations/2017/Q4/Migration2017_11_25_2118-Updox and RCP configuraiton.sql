--Script to Create procedure on  TABLE dbo.UpdoxConfigurationDetails,dbo.aci_json_defaults
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddUpdateUpdoxConfigurationDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddUpdateUpdoxConfigurationDetails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE PROCEDURE [dbo].[AddUpdateUpdoxConfigurationDetails] (@ID INT, @AccountId NVARCHAR(200), @Environment NVARCHAR(200), @IsActive BIT)
AS BEGIN
IF EXISTS (SELECT 1 FROM DBO.UpdoxConfigurationDetails with(nolock)) 
       BEGIN
          UPDATE DBO.UpdoxConfigurationDetails SET AccountId=@AccountId, Environment=@Environment, IsActive=@IsActive;
       END 
 ELSE
       BEGIN
          INSERT INTO DBO.UpdoxConfigurationDetails(ApplicationId,ApplicationPassword,AccountId,Environment,IsActive)
          VALUES ('', '', @AccountId, @Environment, @IsActive)
       END 
END


GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddUpdateACI_json_defaults]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddUpdateACI_json_defaults]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE PROCEDURE [dbo].[AddUpdateACI_json_defaults] (@ACI_VendorId int,@MDO_PracticeId int ,@ACI_BaseURL varchar(300),@ACI_ProductKey varchar(300),@ACI_PairKey varchar(2000),@ACI_USERNAME varchar(300),@ACI_ROLE varchar(300),@ACI_Dashboard_BaseUrl varchar(300),@ACI_Baseurl_PublishDEP varchar(300),@ACI_Baseurl_GetPublishDEPResult varchar(300),@ACI_Baseurl_GetMessageDeliveryNotification varchar(300),@ACI_Baseurl_RetriveInboundDEPData varchar(300))

AS BEGIN
	if (select count(*) from aci_json_defaults)  > 0 
		Begin
			UPDATE aci_json_defaults  SET ACI_VendorId=@ACI_VendorId, MDO_PracticeId=@MDO_PracticeId,ACI_BaseURL=@ACI_BaseURL,ACI_ProductKey=@ACI_ProductKey,ACI_PairKey=@ACI_PairKey,ACI_USERNAME=@ACI_USERNAME,ACI_ROLE=@ACI_ROLE,ACI_Dashboard_BaseUrl=@ACI_Dashboard_BaseUrl,ACI_Baseurl_PublishDEP=@ACI_Baseurl_PublishDEP,ACI_Baseurl_GetPublishDEPResult=@ACI_Baseurl_GetPublishDEPResult,ACI_Baseurl_GetMessageDeliveryNotification=@ACI_Baseurl_GetMessageDeliveryNotification,ACI_Baseurl_RetriveInboundDEPData=@ACI_Baseurl_RetriveInboundDEPData--  WHERE 
		End 
	else
		Begin
		INSERT INTO [dbo].[aci_json_defaults]
           ([ACI_VendorId]
           ,[MDO_PracticeId]
           ,[ACI_INTERVAL]
           ,[RECORDS]
           --,[SIZE]
           ,[ACI_BaseURL]
           ,[ACI_ServiceURL]
           ,[ACI_ProductKey]
           ,[BATCHNO]
           ,[BATCHDATE]
           ,[ACI_BatchID]
           ,[ACI_PairKey]
           ,[ACI_USERNAME]
           ,[ACI_ROLE]
           ,[ACI_Dashboard_BaseUrl]
           ,[ACI_Issuer]
           ,[ACI_Audience]
           ,[ACI_Expires]
           ,[ACI_Baseurl_PublishDEP]
           ,[ACI_Baseurl_GetPublishDEPResult]
           ,[ACI_Baseurl_GetMessageDeliveryNotification]
           ,[ACI_Baseurl_RetriveInboundDEPData])
     VALUES
           (@ACI_VendorId
           ,@MDO_PracticeId
           ,1
           ,100
           --,SIZE
           ,@ACI_BaseURL
           ,''--@ACI_ServiceURL
           ,@ACI_ProductKey
           ,15
           ,GETdate()
           ,0
           ,@ACI_PairKey
           ,@ACI_USERNAME
           ,@ACI_ROLE
           ,@ACI_Dashboard_BaseUrl
           ,''--@ACI_Issuer
           ,''
           ,10
           ,@ACI_Baseurl_PublishDEP
           ,@ACI_Baseurl_GetPublishDEPResult
           ,@ACI_Baseurl_GetMessageDeliveryNotification
           ,@ACI_Baseurl_RetriveInboundDEPData
		   )
		End 
END 

SET ANSI_PADDING ON
GO