IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[DrFirst].[FK_XmlRespose_XmlQueue]') AND parent_object_id = OBJECT_ID(N'[DrFirst].[XmlQueueResponse]'))
ALTER TABLE [DrFirst].[XmlQueueResponse] DROP CONSTRAINT [FK_XmlRespose_XmlQueue]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[DrFirst].[FK_XmlExcp_XmlQueue]') AND parent_object_id = OBJECT_ID(N'[DrFirst].[XmlQueueException]'))
ALTER TABLE [DrFirst].[XmlQueueException] DROP CONSTRAINT [FK_XmlExcp_XmlQueue]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DrFirst].[DF__eRx_DrFir__isArc__03AE70EF]') AND type = 'D')
BEGIN
ALTER TABLE [DrFirst].[eRx_DrFirst_Notifications] DROP CONSTRAINT [DF__eRx_DrFir__isArc__03AE70EF]
END

GO
/****** Object:  Table [DrFirst].[XmlQueueResponse]    Script Date: 2/26/2018 5:51:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[XmlQueueResponse]') AND type in (N'U'))
DROP TABLE [DrFirst].[XmlQueueResponse]
GO
/****** Object:  Table [DrFirst].[XmlQueueException]    Script Date: 2/26/2018 5:51:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[XmlQueueException]') AND type in (N'U'))
DROP TABLE [DrFirst].[XmlQueueException]
GO
/****** Object:  Table [DrFirst].[XmlQueue]    Script Date: 2/26/2018 5:51:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[XmlQueue]') AND type in (N'U'))
DROP TABLE [DrFirst].[XmlQueue]
GO
/****** Object:  Table [DrFirst].[PatientClinicalMeds]    Script Date: 2/26/2018 5:51:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[PatientClinicalMeds]') AND type in (N'U'))
DROP TABLE [DrFirst].[PatientClinicalMeds]
GO
/****** Object:  Table [DrFirst].[ErxMedicationMapping]    Script Date: 2/26/2018 5:51:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[ErxMedicationMapping]') AND type in (N'U'))
DROP TABLE [DrFirst].[ErxMedicationMapping]
GO
/****** Object:  Table [DrFirst].[ErxDiagnosisMapping]    Script Date: 2/26/2018 5:51:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[ErxDiagnosisMapping]') AND type in (N'U'))
DROP TABLE [DrFirst].[ErxDiagnosisMapping]
GO
/****** Object:  Table [DrFirst].[eRx_Prescriptions_Response]    Script Date: 2/26/2018 5:51:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[eRx_Prescriptions_Response]') AND type in (N'U'))
DROP TABLE [DrFirst].[eRx_Prescriptions_Response]
GO
/****** Object:  Table [DrFirst].[eRx_Medicine_Response]    Script Date: 2/26/2018 5:51:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[eRx_Medicine_Response]') AND type in (N'U'))
DROP TABLE [DrFirst].[eRx_Medicine_Response]
GO
/****** Object:  Table [DrFirst].[eRx_DrFirst_Notifications]    Script Date: 2/26/2018 5:51:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[eRx_DrFirst_Notifications]') AND type in (N'U'))
DROP TABLE [DrFirst].[eRx_DrFirst_Notifications]
GO
/****** Object:  Table [DrFirst].[eRx_DrFirst_Notifications]    Script Date: 2/26/2018 5:51:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[eRx_DrFirst_Notifications]') AND type in (N'U'))
BEGIN
CREATE TABLE [DrFirst].[eRx_DrFirst_Notifications](
	[eRx_DrFirst_Notifications_Key] [int] IDENTITY(1,1) NOT NULL,
	[Requested_Date] [varchar](30) NULL,
	[Notification_Type] [varchar](30) NULL,
	[Notification_Count] [int] NULL,
	[Screen] [varchar](20) NULL,
	[RCopia_Launched] [datetime] NULL,
	[isArchieved] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[eRx_DrFirst_Notifications_Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DrFirst].[eRx_Medicine_Response]    Script Date: 2/26/2018 5:51:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[eRx_Medicine_Response]') AND type in (N'U'))
BEGIN
/****** Object:  Table [DrFirst].[eRx_Medicine_Response]    Script Date: 3/14/2018 1:52:41 PM ******/
CREATE TABLE [DrFirst].[eRx_Medicine_Response](
	[eRx_Medicine_Response_Key] [bigint] IDENTITY(1,1) NOT NULL,
		[Patient_ExtID] [bigint] NULL,
[AppointmentId] [bigint] NULL,
	[ClinicalID] [bigint] NULL,
	[NDCID] [varchar](max) NULL,
	[FirstDataBankMedID] [varchar](100) NULL,
	[RcopiaID] [varchar](100) NULL,
	[BrandName] [varchar](500) NULL,
	[GenericName] [varchar](500) NULL,
	[BrandType] [varchar](500) NULL,
	[RouteDrug] [varchar](100) NULL,
	[Form] [varchar](100) NULL,
	[Strength] [varchar](100) NULL,
	[Action] [varchar](100) NULL,
	[Dose] [varchar](100) NULL,
	[DoseUnit] [varchar](500) NULL,
	[Route] [varchar](500) NULL,
	[Frequency] [varchar](500) NULL,
	[Duration] [varchar](500) NULL,
	[Quantity] [varchar](500) NULL,
	[QuantityUnit] [varchar](500) NULL,
	[Refills] [varchar](500) NULL,
	[OtherNotes] [varchar](500) NULL,
	[PatientNotes] [varchar](1000) NULL,
	[SigChangedDate] [varchar](50) NULL,
	[LastModifiedDate] [varchar](50) NULL,
	[StartDate] [varchar](50) NULL,
	[StopDate] [varchar](50) NULL,
	[Deleted] [varchar](50) NULL,
	[StopReason] [varchar](50) NULL,
	[MedicationRcopiaID] [varchar](50) NULL,
	[PrescriptionRcopiaID] [varchar](100) NULL DEFAULT (NULL),
	[RxnormID] [varchar](100) NULL,
	[RxnormIDType] [varchar](100) NULL,
	[PharmacyRcopiaID] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyRcopiaMasterID] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyDeleted] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyName] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyAddress1] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyState] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyZip] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyPhone] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyFax] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyIs24Hour] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyLevel3] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyElectronic] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyMailOrder] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyRequiresEligibility] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyRetail] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyLongTermCare] [varchar](100) NULL DEFAULT (NULL),
	[PharmacySpecialty] [varchar](100) NULL DEFAULT (NULL),
	[PharmacyCanReceiveControlledSubstance] [varchar](100) NULL DEFAULT (NULL),
	[DrugSchedule] [varchar](100) NULL DEFAULT (NULL),
CONSTRAINT [PK_eRx_Medicine_Response] PRIMARY KEY CLUSTERED 
(
	[eRx_Medicine_Response_Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY])ON [PRIMARY]
END
ELSE



BEGIN
	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'Patient_ExtID')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add Patient_ExtID bigint NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN Patient_ExtID bigint NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'AppointmentId')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add AppointmentId bigint NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN AppointmentId bigint NULL
	END


	
	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'ClinicalID')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add ClinicalID bigint NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN ClinicalID bigint NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'NDCID')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add NDCID varchar(max) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN NDCID varchar(max) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'FirstDataBankMedID')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add FirstDataBankMedID varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN FirstDataBankMedID varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'RcopiaID')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add RcopiaID varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN RcopiaID varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'BrandName')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add BrandName varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN BrandName varchar(500) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'GenericName')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add GenericName varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN GenericName varchar(500) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'BrandType')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add BrandType varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN BrandType varchar(500) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'RouteDrug')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add RouteDrug varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN RouteDrug varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'Form')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add Form varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN Form varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'Strength')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add Strength varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN Strength varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'Action')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add Action varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN Action varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'Dose')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add Dose varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN Dose varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'DoseUnit')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add DoseUnit varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN DoseUnit varchar(500) NULL
	END
	
	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'Route')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add Route varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN Route varchar(500) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'Frequency')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add Frequency varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN Frequency varchar(500) NULL
	END
	
	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'Duration')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add Duration varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN Duration varchar(500) NULL
	END
	
	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'Quantity')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add Quantity varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN Quantity varchar(500) NULL
	END
	
	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'QuantityUnit')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add QuantityUnit varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN QuantityUnit varchar(500) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'Refills')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add Refills varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN Refills varchar(500) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'OtherNotes')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add OtherNotes varchar(500) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN OtherNotes varchar(500) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PatientNotes')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PatientNotes varchar(1000) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PatientNotes varchar(1000) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'SigChangedDate')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add SigChangedDate varchar(50) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN SigChangedDate varchar(50) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'LastModifiedDate')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add LastModifiedDate varchar(50) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN LastModifiedDate varchar(50) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'StartDate')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add StartDate varchar(50) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN StartDate varchar(50) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'StopDate')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add StopDate varchar(50) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN StopDate varchar(50) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'Deleted')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add Deleted varchar(50) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN Deleted varchar(50) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'StopReason')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add StopReason varchar(50) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN StopReason varchar(50) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'MedicationRcopiaID')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add MedicationRcopiaID varchar(50) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN MedicationRcopiaID varchar(50) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PrescriptionRcopiaID')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PrescriptionRcopiaID varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PrescriptionRcopiaID varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'RxnormID')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add RxnormID varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN RxnormID varchar(100) NULL
	END
	
	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'RxnormIDType')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add RxnormIDType varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN RxnormIDType varchar(100) NULL
	END
	
	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyRcopiaID')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyRcopiaID varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyRcopiaID varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyRcopiaMasterID')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyRcopiaMasterID varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyRcopiaMasterID varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyDeleted')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyDeleted varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyDeleted varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyName')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyName varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyName varchar(100) NULL
	END
	
	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyAddress1')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyAddress1 varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyAddress1 varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyState')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyState varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyState varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyZip')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyZip varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyZip varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyPhone')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyPhone varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyPhone varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyFax')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyFax varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyFax varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyIs24Hour')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyIs24Hour varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyIs24Hour varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyLevel3')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyLevel3 varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyLevel3 varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyElectronic')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyElectronic varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyElectronic varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyMailOrder')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyMailOrder varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyMailOrder varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyRequiresEligibility')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyRequiresEligibility varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyRequiresEligibility varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyRetail')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyRetail varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyRetail varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyLongTermCare')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyLongTermCare varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyLongTermCare varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacySpecialty')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacySpecialty varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacySpecialty varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'PharmacyCanReceiveControlledSubstance')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add PharmacyCanReceiveControlledSubstance varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN PharmacyCanReceiveControlledSubstance varchar(100) NULL
	END

	IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'DrFirst.eRx_Medicine_Response')  AND name = 'DrugSchedule')
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		Add DrugSchedule varchar(100) NULL
	End
	ELSE
	BEGIN
		Alter Table DrFirst.eRx_Medicine_Response
		ALTER COLUMN DrugSchedule varchar(100) NULL
	END

END

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [DrFirst].[eRx_Prescriptions_Response]    Script Date: 2/26/2018 5:51:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[eRx_Prescriptions_Response]') AND type in (N'U'))
BEGIN
CREATE TABLE [DrFirst].[eRx_Prescriptions_Response](
	[eRx_Prescriptions_Response_Key] [bigint] IDENTITY(1,1) NOT NULL,
	[TraceInformationResponseMessageID] [varchar](30) NULL,
	[RequestLastUpdateDate] [datetime] NULL,
	[RequestStatusAction] [varchar](20) NULL,
	[ResponseEndTimestamp] [datetime] NULL,
	[ResponseStatus] [varchar](30) NULL,
	[ResponseLastUpdateDate] [datetime] NULL,
	[PrescriptionDeleted] [varchar](50) NULL,
	[PrescriptionVoided] [varchar](50) NULL,
	[PrescriptionDenied] [varchar](50) NULL,
	[PrescriptionRcopiaID] [varchar](50) NULL,
	[PrescriptionExternalID] [varchar](50) NULL,
	[PrescriptionNeedsReview] [varchar](50) NULL,
	[PrescriptionCreatedDate] [varchar](50) NULL,
	[PrescriptionCompletedDate] [varchar](50) NULL,
	[PrescriptionSignedDate] [varchar](50) NULL,
	[PrescriptionStopDate] [varchar](50) NULL,
	[PrescriptionLastModifiedDate] [varchar](50) NULL,
	[PrescriptionLastModifiedBy] [varchar](50) NULL,
	[PatientRcopiaID] [varchar](50) NULL,
	[PatientID] [varchar](50) NULL,
	[PatientRcopiaPracticeID] [varchar](50) NULL,
	[PatientFirstName] [varchar](50) NULL,
	[PatientLastName] [varchar](50) NULL,
	[ProviderRcopiaID] [varchar](50) NULL,
	[ProviderUsername] [varchar](50) NULL,
	[ProviderExternalID] [varchar](50) NULL,
	[ProviderNPI] [varchar](50) NULL,
	[ProviderFirstName] [varchar](50) NULL,
	[ProviderLastName] [varchar](50) NULL,
	[PharmacyRcopiaID] [varchar](50) NULL,
	[PharmacyDeleted] [varchar](50) NULL,
	[PharmacyName] [varchar](50) NULL,
	[PharmacyCity] [varchar](50) NULL,
	[PharmacyState] [varchar](50) NULL,
	[PharmacyZip] [varchar](50) NULL,
	[PharmacyPhone] [varchar](50) NULL,
	[PharmacyFax] [varchar](50) NULL,
	[PharmacyIs24Hour] [varchar](50) NULL,
	[PharmacyLevel3] [varchar](50) NULL,
	[PharmacyElectronic] [varchar](50) NULL,
	[PharmacyMailOrder] [varchar](50) NULL,
	[PharmacyRequiresEligibility] [varchar](50) NULL,
	[PharmacyRetail] [varchar](50) NULL,
	[PharmacyLongTermCare] [varchar](50) NULL,
	[PharmacySpecialty] [varchar](50) NULL,
	[PharmacyCanReceiveControlledSubstance] [varchar](50) NULL,
	[DrugNDCID] [varchar](100) NULL,
	[DrugFirstDataBankMedID] [varchar](100) NULL,
	[DrugRcopiaID] [varchar](100) NULL,
	[DrugRxnormID] [varchar](50) NULL,
	[DrugRxnormIDType] [varchar](50) NULL,
	[DrugDescription] [varchar](500) NULL,
	[DrugBrandName] [varchar](200) NULL,
	[DrugGenericName] [varchar](200) NULL,
	[DrugSchedule] [varchar](50) NULL,
	[DrugBrandType] [varchar](100) NULL,
	[DrugLegendStatus] [varchar](50) NULL,
	[DrugRoute] [varchar](200) NULL,
	[DrugForm] [varchar](100) NULL,
	[DrugStrength] [varchar](200) NULL,
	[SigAction] [varchar](200) NULL,
	[SigDose] [varchar](200) NULL,
	[SigDoseUnit] [varchar](200) NULL,
	[SigRoute] [varchar](200) NULL,
	[SigFrequency] [varchar](50) NULL,
	[SigDoseOther] [varchar](200) NULL,
	[SigDuration] [varchar](50) NULL,
	[SigQuantity] [varchar](50) NULL,
	[SigQuantityUnit] [varchar](50) NULL,
	[SigRefills] [varchar](50) NULL,
	[SigSubstitutionPermitted] [varchar](10) NULL,
	[SigOtherNotes] [varchar](200) NULL,
	[SigPatientNotes] [varchar](200) NULL,
	[SigComments] [varchar](500) NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [DrFirst].[eRx_Prescriptions_Response] ADD [PrescriptionCompletionAction] [varchar](50) NULL DEFAULT (NULL)
ALTER TABLE [DrFirst].[eRx_Prescriptions_Response] ADD [SendMethod] [varchar](100) NULL DEFAULT (NULL)
PRIMARY KEY CLUSTERED 
(
	[eRx_Prescriptions_Response_Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DrFirst].[ErxDiagnosisMapping]    Script Date: 2/26/2018 5:51:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[ErxDiagnosisMapping]') AND type in (N'U'))
BEGIN
CREATE TABLE [DrFirst].[ErxDiagnosisMapping](
	[RecordId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ClinicalId] [numeric](18, 0) NOT NULL,
	[EncounterId] [numeric](18, 0) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[FindingDetail] [nvarchar](500) NOT NULL,
	[Description] [nvarchar](2000) NOT NULL,
	[Code] [nvarchar](200) NOT NULL,
	[ImageDescriptor] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_DrFirst.ErxDiagnosisMapping] PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [DrFirst].[ErxMedicationMapping]    Script Date: 2/26/2018 5:51:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[ErxMedicationMapping]') AND type in (N'U'))
BEGIN
CREATE TABLE [DrFirst].[ErxMedicationMapping](
	[RecordId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ClinicalId] [numeric](18, 0) NOT NULL,
	[EncounterId] [numeric](18, 0) NOT NULL,
	[PatientId] [numeric](18, 0) NOT NULL,
	[RcopiyaID] [varchar](50) NULL,
	[MedDatetime] [datetime] NULL,
	[PrescriptionRcopiaID] [varchar](100) NULL DEFAULT (NULL),
 CONSTRAINT [PK_DrFirst.ErxMedicationMapping] PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DrFirst].[PatientClinicalMeds]    Script Date: 2/26/2018 5:51:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[PatientClinicalMeds]') AND type in (N'U'))
BEGIN
CREATE TABLE [DrFirst].[PatientClinicalMeds](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClinicalId] [bigint] NOT NULL,
	[ExternalID] [bigint] NULL DEFAULT (NULL),
	[RcopiaId] [varchar](100) NULL,
	[RxnormId] [varchar](100) NULL,
	[NDC] [varchar](max) NULL,
	[AppointmentId] [int] NULL,
	[PatientId] [int] NULL,
	[FindingDetail] [nvarchar](255) NULL DEFAULT (''),
	[Status] [nvarchar](1) NULL DEFAULT (''),
	[DrugId] [nvarchar](64) NULL DEFAULT (''),
	[CreatedDatetime] [datetime] NULL,
	[UpdatedDatetime] [datetime] NULL,
	[IsUpdate] [bit] NULL,
 CONSTRAINT [PK_PatientClinicalMeds] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DrFirst].[XmlQueue]    Script Date: 2/26/2018 5:51:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[XmlQueue]') AND type in (N'U'))
BEGIN
CREATE TABLE [DrFirst].[XmlQueue](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientId] [bigint] NULL,
	[EncounterId] [bigint] NULL,
	[XmlValue] [ntext] NULL,
	[UserId] [int] NULL,
	[StaffId] [int] NULL,
	[ActionTypeId] [int] NULL,
	[RecordDatetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [DrFirst].[XmlQueueException]    Script Date: 2/26/2018 5:51:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[XmlQueueException]') AND type in (N'U'))
BEGIN
CREATE TABLE [DrFirst].[XmlQueueException](
	[RecordId] [bigint] IDENTITY(1,1) NOT NULL,
	[XmlQueueId] [bigint] NOT NULL,
	[PatientId] [bigint] NULL,
	[EncounterId] [bigint] NULL,
	[XmlException] [ntext] NULL,
	[UserId] [int] NULL,
	[StaffId] [int] NULL,
	[RecordDateTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [DrFirst].[XmlQueueResponse]    Script Date: 2/26/2018 5:51:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[XmlQueueResponse]') AND type in (N'U'))
BEGIN
CREATE TABLE [DrFirst].[XmlQueueResponse](
	[RecordId] [bigint] IDENTITY(1,1) NOT NULL,
	[XmlQueueId] [bigint] NOT NULL,
	[PatientId] [bigint] NULL,
	[EncounterId] [bigint] NULL,
	[XmlResponse] [ntext] NULL,
	[UserId] [int] NULL,
	[StaffId] [int] NULL,
	[RecordDateTime] [datetime] NULL,
	[PatientRcopiaId] [varchar](100) NULL DEFAULT (NULL),
	[ActionTypeId] [int] NULL DEFAULT (NULL),
PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DrFirst].[DF__eRx_DrFir__isArc__03AE70EF]') AND type = 'D')
BEGIN
ALTER TABLE [DrFirst].[eRx_DrFirst_Notifications] ADD  DEFAULT ((0)) FOR [isArchieved]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[DrFirst].[FK_XmlExcp_XmlQueue]') AND parent_object_id = OBJECT_ID(N'[DrFirst].[XmlQueueException]'))
ALTER TABLE [DrFirst].[XmlQueueException]  WITH NOCHECK ADD  CONSTRAINT [FK_XmlExcp_XmlQueue] FOREIGN KEY([XmlQueueId])
REFERENCES [DrFirst].[XmlQueue] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[DrFirst].[FK_XmlExcp_XmlQueue]') AND parent_object_id = OBJECT_ID(N'[DrFirst].[XmlQueueException]'))
ALTER TABLE [DrFirst].[XmlQueueException] CHECK CONSTRAINT [FK_XmlExcp_XmlQueue]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[DrFirst].[FK_XmlRespose_XmlQueue]') AND parent_object_id = OBJECT_ID(N'[DrFirst].[XmlQueueResponse]'))
ALTER TABLE [DrFirst].[XmlQueueResponse]  WITH NOCHECK ADD  CONSTRAINT [FK_XmlRespose_XmlQueue] FOREIGN KEY([XmlQueueId])
REFERENCES [DrFirst].[XmlQueue] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[DrFirst].[FK_XmlRespose_XmlQueue]') AND parent_object_id = OBJECT_ID(N'[DrFirst].[XmlQueueResponse]'))
ALTER TABLE [DrFirst].[XmlQueueResponse] CHECK CONSTRAINT [FK_XmlRespose_XmlQueue]
GO
