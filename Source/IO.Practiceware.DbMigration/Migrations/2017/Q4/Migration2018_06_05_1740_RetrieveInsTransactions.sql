--Alter dbo.RetrieveInsTransactions to display Cell Phone Number on Review Appointments Screen.

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.RetrieveInsTransactions') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.RetrieveInsTransactions
GO

CREATE PROCEDURE dbo.RetrieveInsTransactions (
	@SAptDate VARCHAR(8)
	,@EAptDate VARCHAR(8)
	,@LocId INT
	,@ResId INT
	,@PatId INT
	,@TStat VARCHAR(1)
	)
AS
BEGIN

SELECT DISTINCT Appointments.AppointmentId
	,Appointments.AppDate
	,Appointments.AppTime
	,Appointments.Comments
	,Appointments.ScheduleStatus
	,Appointments.ReferralId
	,AppointmentType.AppointmentType
	,AppointmentType.Question
	,Appointments.ActivityStatus
	,Appointments.Comments
	,PatientDemographics.Salutation
	,Appointments.ConfirmStatus
	,PatientDemographics.LastName
	,PatientDemographics.FirstName
	,PatientDemographics.MiddleInitial
	,PatientDemographics.NameReference
	,PatientDemographics.Gender
	,PatientDemographics.BirthDate
	,PatientDemographics.HomePhone
	,PatientDemographics.BusinessPhone
	,PatientDemographics.CellPhone
	,PatientDemographics.ReferralRequired
	,Resources.ResourceName
	,pi.OrdinalId AS FinancialPIndicator
	,i.Name AS InsurerName
	,PatientDemographics.PatientId
	,ppn.OrdinalId AS PhoneSequence --*
	,ppn.patientphonenumbertypeid as PhoneType --*
FROM model.Insurers i
RIGHT JOIN (
	(
		Resources INNER JOIN (
			PatientDemographics INNER JOIN (
				AppointmentType INNER JOIN Appointments ON AppointmentType.AppTypeId = Appointments.AppTypeId
				) ON PatientDemographics.PatientId = Appointments.PatientId
			) ON Resources.ResourceId = Appointments.ResourceId1
		) LEFT JOIN (
		model.PatientInsurances pi INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		) ON PatientDemographics.PolicyPatientId = pi.InsuredPatientId
	) ON i.Id = ip.InsurerId

	inner join model.patientphonenumbers ppn --*
	on ppn.patientid = PatientDemographics.patientid --*

WHERE (
		(Appointments.ResourceId2 = @LocId)
		OR (@LocId = - 1)
		)
	AND (
		(Appointments.ResourceId1 = @ResId)
		OR (@ResId = - 1)
		)
	AND (
		(PatientDemographics.PatientId = @PatId)
		OR (@PatId < 1)
		)
	AND (
		(Appointments.ScheduleStatus = 'P')
		OR (Appointments.ScheduleStatus = 'R')
		OR (Appointments.ScheduleStatus = 'A')
		OR (Appointments.ScheduleStatus = 'D')
		OR (Appointments.ScheduleStatus = 'N')
		OR (Appointments.ScheduleStatus = 'S')
		)
	AND (
		(Appointments.AppDate <= @SAptDate)
		OR (@SAptDate = '0')
		)
	AND (
		(Appointments.AppDate >= @EAptDate)
		OR (@EAptDate = '0')
		)
ORDER BY Appointments.AppDate DESC
	,Appointments.AppTime

END
GO

--Alter dbo.RetrieveTransactions to display Cell Phone Number on Review Appointments Screen.

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.RetrieveTransactions') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.RetrieveTransactions
GO

CREATE PROCEDURE RetrieveTransactions 
(
@SAptDate varchar(8),
@EAptDate varchar(8),
@LocId Int,
@ResId Int,
@PatId Int,
@TStat varchar(1)
)
AS
BEGIN

SELECT 
	Appointments.AppointmentId, Appointments.AppDate, Appointments.AppTime, 
	Appointments.Comments, Appointments.ScheduleStatus, Appointments.ReferralId, 
	AppointmentType.AppointmentType, AppointmentType.Question, Appointments.ActivityStatus, 
	Appointments.Comments, PatientDemographics.Salutation, 	Appointments.ConfirmStatus, 
	PatientDemographics.LastName, PatientDemographics.FirstName, PatientDemographics.MiddleInitial, 
	PatientDemographics.NameReference, PatientDemographics.Gender, PatientDemographics.BirthDate, 
	PatientDemographics.HomePhone, PatientDemographics.BusinessPhone, 
	PatientDemographics.ReferralRequired, Resources.ResourceName, PatientDemographics.PatientId
	,ppn.OrdinalId AS PhoneSequence 
	,ppn.patientphonenumbertypeid as PhoneType 
FROM Resources 
INNER JOIN (PatientDemographics 
LEFT JOIN (AppointmentType 
INNER JOIN Appointments 
ON AppointmentType.AppTypeId = Appointments.AppTypeId) 
ON PatientDemographics.PatientId = Appointments.PatientId) 
ON Resources.ResourceId = Appointments.ResourceId1
inner join model.patientphonenumbers ppn
	on ppn.patientid = PatientDemographics.patientid
WHERE ((PatientDemographics.PatientId = @PatId) Or (@PatId < 1)) And
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1)) And
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) And
((Appointments.ScheduleStatus = @TStat) Or (@TStat = '0')) And
((Appointments.AppDate <= @SAptDate) Or (@SAptDate = '0')) And
((Appointments.AppDate >= @EAptDate) Or (@EAptDate = '0'))
ORDER BY Appointments.AppDate DESC , Appointments.AppTime
SET NOCOUNT OFF

END
GO