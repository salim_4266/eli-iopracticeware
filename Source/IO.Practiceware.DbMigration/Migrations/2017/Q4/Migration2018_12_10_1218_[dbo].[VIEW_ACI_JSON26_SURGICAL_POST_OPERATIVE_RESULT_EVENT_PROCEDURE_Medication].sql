IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'dbo.VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Medication')) 
Drop Trigger dbo.VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Medication
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VIEW_ACI_JSON26_SURGICAL_POST_OPERATIVE_RESULT_EVENT_PROCEDURE_Medication]

AS
	SELECT 
		PC.PATIENTID AS PatientId, --R
		PC.APPOINTMENTID AS AppointmentId, --R
		CASE
                WHEN NC.MEDID = '' THEN 'UNCODIFIED'
                ELSE ISNULL(CAST(NC.MEDID AS VARCHAR(15)),'00000')
        END AS Code, 


		CASE WHEN pc.FindingDetail like 'RX-8/%-2/%' 
			THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') 
			ELSE '' 
		END  AS Description, 
		'2.16.840.1.113883.6.88' AS CodeSystem,
		'RxNorm' AS CodeSystemName
	FROM
	 dbo.patientclinical pc WITH(NOLOCK) left join fdb.tblcompositedrug nc WITH(NOLOCK)
	on Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex('-2/', pc.findingdetail) - 6),'&','and'), '+','Plus') = nc.MED_ROUTED_DF_MED_ID_DESC
	where pc.clinicaltype = 'A' and findingdetail like 'RX-8%'
GO


