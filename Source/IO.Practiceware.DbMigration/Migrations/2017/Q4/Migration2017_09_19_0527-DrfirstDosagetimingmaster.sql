/****** Object:  Table [DrFirst].[DrfirstDosagetimingmaster]    Script Date: 3/17/2018 12:11:54 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[DrfirstDosagetimingmaster]') AND type in (N'U'))
DROP TABLE [DrFirst].[DrfirstDosagetimingmaster]
GO
/****** Object:  Table [DrFirst].[DrfirstDosagetimingmaster]    Script Date: 3/17/2018 12:11:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DrFirst].[DrfirstDosagetimingmaster]') AND type in (N'U'))
BEGIN
CREATE TABLE [DrFirst].[DrfirstDosagetimingmaster](
	[DosageID] [bigint] IDENTITY(1,1) NOT NULL,
	[DosageTimingCode] [varchar](500) NULL,
	[DosageTimingDescription] [varchar](300) NULL,
 CONSTRAINT [PK_DrfirstDosagetimingmaster] PRIMARY KEY CLUSTERED 
(
	[DosageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [DrFirst].[DrfirstDosagetimingmaster] ON 

INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (1, N'DAILY', N'daily')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (2, N'in A.M.', N'in the morning')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (3, N'BID', N'twice a day')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (4, N'TID', N'three times a day')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (5, N'QID', N'four times a day')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (6, N'Q4h', N'every 4 hours')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (7, N'Q6h', N'every 6 hours')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (8, N'Q8h', N'every 8 hours')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (9, N'Q12h', N'every 12 hours')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (10, N'EVERY OTHER DAY', N'every other day')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (11, N'Q2h WA', N'every 2 hours while awake')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (12, N'Q1wk', N'weekly')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (13, N'Q2wks', N'every 2 weeks')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (14, N'Q3wks', N'every 3 weeks')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (15, N'Add''l Sig', N'not specified')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (16, N'NIGHTLY', N'nightly')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (17, N'QHS', N'at bedtime')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (18, N'3 TIMES WEEKLY', N'3 times weekly')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (19, N'Once a month', N'once a month')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (20, N'Q72h', N'every 72 hours')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (21, N'Q4-6h', N'every 4-6 hours')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (22, N'Q1h WA', N'every hour while awake')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (23, N'Q48h', N'every 48 hours')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (24, N'2 TIMES WEEKLY', N'2 times weekly')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (25, N'Q3h', N'every 3 hours')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (26, N'Q2h', N'every 2 hours')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (27, N'once a day', N'once a day')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (28, N'single dose', N'single dose')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (29, N'every afternoon', N'every afternoon')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (30, N'every three hours while awake', N'every three hours while awake')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (31, N'as directed', N'as directed')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (32, N'QD', N'once a day')
INSERT [DrFirst].[DrfirstDosagetimingmaster] ([DosageID], [DosageTimingCode], [DosageTimingDescription]) VALUES (33, N'QAM', N'every morning')
SET IDENTITY_INSERT [DrFirst].[DrfirstDosagetimingmaster] OFF
SET ANSI_PADDING ON
GO
