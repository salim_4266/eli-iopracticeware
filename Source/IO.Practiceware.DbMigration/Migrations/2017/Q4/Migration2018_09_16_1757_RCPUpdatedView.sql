IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_ACI_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION]')) 
Drop VIEW [dbo].[VIEW_ACI_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VIEW_ACI_JSON03_ACIMODULEUSERACCOUNTADMINISTRATION]
AS
     SELECT 'User' AS RESOURCETYPE, 
            'EHR' ISSOURCE, 
            CAST(RESOURCEID AS VARCHAR(10)) AS REFERENCENUMBER, --REQUIRED 
            NULL AS PRODUCTKEY, --REQUIRED -filled from SQL2JSON 
            RESOURCEID AS EXTERNALID, -- REQUIRED
			ResourceFirstName AS GIVENNAME,
            RESOURCELASTNAME AS FAMILYNAME, --REQUIRED 
            NULL AS SecondandFurtherGivenNames, --OPTIONAL 
            ' ' AS DATEOFBIRTH,
		    ResourceFirstName + ' ' + ResourceLastName AS USERNAME, --REQUIRED
		  CASE ResourceType
			  WHEN 'A' THEN 'Administrator'
			  WHEN 'D' THEN 'Eligible Clinician'
			  WHEN 'O' THEN 'Administrator'
			  ELSE 'Administrator'
		  End AS ROLE, --REQUIRED //as per vasu
          CASE
                WHEN STATUS = 'A'
                THEN 'TRUE'
                ELSE 'FALSE'
            END AS ACTIVE,
            RESOURCEID
     FROM dbo.Resources WITH (NOLOCK)
	WHERE resourcetype not in ('R','X','Y','Z')

UNION

     SELECT 'User' AS RESOURCETYPE, 
            'EHR' ISSOURCE, 
            CAST(R.RESOURCEID AS VARCHAR(10)) AS REFERENCENUMBER, --REQUIRED 
            NULL AS PRODUCTKEY, --REQUIRED -filled from SQL2JSON 
            R.RESOURCEID AS EXTERNALID, -- REQUIRED
            R.ResourceFirstName AS GIVENNAME, --REQUIRED 
            R.RESOURCELASTNAME AS FAMILYNAME, --REQUIRED 
            NULL AS SecondandFurtherGivenNames, --OPTIONAL 
    		'' AS DATEOFBIRTH,
            ResourceFirstName + ' ' + ResourceLastName AS USERNAME, --REQUIRED
		  CASE R.ResourceType
			  WHEN 'X' THEN 'Administrator'
			  WHEN 'Z' THEN 'Eligible Clinician'
			  ELSE 'Practice-Level User'
		   end AS ROLE, --REQUIRED //as per vasu
        	'FALSE' AS ACTIVE,
            R.RESOURCEID
     FROM dbo.Resources R (NOLOCK) join  model.Users U (NOLOCK)  on U.Id=R.ResourceId Where resourcetype in ('X','Y','Z')
GO



