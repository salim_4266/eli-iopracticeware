

/****** Object:  StoredProcedure [MVE].[PP_GetLocationDetails]    Script Date: 06/29/2018 13:36:35 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_GetLocationDetails]')) 
Drop Procedure [MVE].[PP_GetLocationDetails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE Procedure [MVE].[PP_GetLocationDetails] @ActionId int
As 
Begin 

   Select ShortName as ContactName, Name, Case(IsArchived) When 0 Then 1 Else 0 End as Active, ShortName as Notes, Id as ExternalId From model.ServiceLocations with (nolock) Where Id not in  (Select distinct PatientNo from MVE.PP_PortalQueueResponse PQR inner join  MVE.PP_ActionTypeLookUps A on PQR.ActionTypeLookupId= A.ActionId Where PQR.ActionTypeLookupId=@ActionId Union Select distinct PatientNo from MVE.PP_PortalQueueException PQR inner join MVE.PP_ActionTypeLookUps A on PQR.ActionTypeLookupId= A.ActionId Where PQR.ActionTypeLookupId=@ActionId and ProcessCount >= 5)

End
  
SET ANSI_NULLS ON

GO

