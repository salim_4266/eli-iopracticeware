

/****** Object:  StoredProcedure [MVE].[PP_GetDoctorDetails]    Script Date: 06/29/2018 13:36:01 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_GetDoctorDetails]')) 
Drop Procedure [MVE].[PP_GetDoctorDetails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
  
CREATE Procedure [MVE].[PP_GetDoctorDetails] @ActionId int  
As   
 Begin  
  
   Select Id as ExternalId, LastName, FirstName, DisplayName as AliasName, Case(IsArchived) When 0 then 1 Else 0 End as Active, '1' as InHouse, (SELECT Top(1)Id From model.ServiceLocations Order by Id) as Location From model.Users   
   Where __EntityType__ = 'Doctor' and Id not in (Select distinct PatientNo from MVE.PP_PortalQueueResponse PQR inner join  MVE.PP_ActionTypeLookUps A on PQR.ActionTypeLookupId=      A.ActionId Where PQR.ActionTypeLookupId=@ActionId Union Select distinct PatientNo from MVE.PP_PortalQueueException PQR inner join MVE.PP_ActionTypeLookUps A on                       PQR.ActionTypeLookupId= A.ActionId Where PQR.ActionTypeLookupId=@ActionId and ProcessCount >= 5)  
  
 End  
GO