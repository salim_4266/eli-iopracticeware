--UserName and Password format change for Window Services

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MVE].[PP_GetPatientDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [MVE].[PP_GetPatientDetails]
GO
CREATE Procedure [MVE].[PP_GetPatientDetails] @ActionId int 
As
Begin

 Select Top(100) P.Id as ExternalId, FirstName, ISNULL(MiddleName, '') as MiddleName, LastName, ISNULL(Suffix, '') as Suffix, ISNULL(Prefix,'') Prefix, FirstName + ' ' + ISNULL(MiddleName,'') + ' ' + LastName as FullName, '' as CompanyName, '' as JobTitle, PEA.Value as EmailAddresses, IsNull(Line1,'') as Address1, Isnull(Line2, '') as  Address2, Isnull(City, '') as  City, Isnull(StateOrProvinceId, '') as  State, '' as Country, Isnull(PostalCode, '') as Zip, LastName + '' + FirstName + '' + Isnull(PostalCode, '') as Username, LastName + '_' + REPLACE(CONVERT (CHAR(10), p.DateOfBirth, 101),'/','') as Password, Cast(DateOfBirth as Date) as Birthdate, '' as Notes, ISNULL((Select top(1) Name From model.languages with (nolock) Where Id = P.LanguageId),'English') as LanguageName, ISNULL(P.PreferredServiceLocationId, (Select Top(1) Id From model.ServiceLocations)) as LocationId,   
 ISNULL(PPN.CountryCode + PPn.AreaCode + PPn.ExchangeAndSuffix,' ') as PhoneNumbers From model.Patients P with (nolock)   
 INNER JOIN model.PatientAddresses PA with (nolock) on PA.PatientId = P.Id and PA.PostalCode is not null and Pa.PostalCode != '' and PA.OrdinalId = 1 and p.DateOfBirth is not null  
 INNER JOIN model.PatientEmailAddresses PEA with (nolock) on P.Id = PEA.PatientId and p.LastName not like '%Test%' and p.FirstName not like '%Test%'   
 LEFT Join model.PatientPhoneNumbers PPN with (nolock) on P.Id = PPN.PatientId and PPN.OrdinalId = 1  
 Where Cast(P.Id as nvarchar) not in (Select distinct PatientNo
 from MVE.PP_PortalQueueResponse PQR with (nolock) inner join MVE.PP_ActionTypeLookUps A on  PQR.ActionTypeLookupId= A.ActionId where PQR.ActionTypeLookupId=97 Union Select distinct PatientNo from MVE.PP_PortalQueueException PQR with (nolock) inner join MVE.PP_ActionTypeLookUps A on  PQR.ActionTypeLookupId= A.ActionId where PQR.ActionTypeLookupId=97 and ProcessCount >= 5)  


End


