IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'model.PatientRepresentatives')  AND name = 'IsPortalActivated')
BEGIN
	Alter Table model.PatientRepresentatives
	Add IsPortalActivated [bit] NULL
END