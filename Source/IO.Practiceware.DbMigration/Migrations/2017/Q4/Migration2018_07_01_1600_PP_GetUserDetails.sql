

/****** Object:  StoredProcedure [MVE].[PP_GetUserDetails]    Script Date: 06/29/2018 13:40:37 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_GetUserDetails]')) 
Drop Procedure [MVE].[PP_GetUserDetails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  

CREATE Procedure [MVE].[PP_GetUserDetails](@ActionId int) 
As

Begin

 Select Top(100) FirstName, LastName, UserName as UserName, Cast(PID as nvarchar(4)) + Substring(Cast(NEWID() as nvarchar(50)),0,8) as Password, CASE(IsArchived) When 0 Then 1 Else 0 End as Active, '' as Notes, '' as UpDoxId, Id as ExternalId  From model.
Users with (nolock) Where IsArchived = 0 and Id not in (Select distinct PatientNo from MVE.PP_PortalQueueResponse PQR INNER JOIN MVE.PP_ActionTypeLookUps A  on PQR.ActionTypeLookupId= A.ActionId Where PQR.ActionTypeLookupId=@ActionId Union Select distinct
 PatientNo from MVE.PP_PortalQueueException PQR INNER JOIN MVE.PP_ActionTypeLookUps A  on PQR.ActionTypeLookupId= A.ActionId Where PQR.ActionTypeLookupId=@ActionId and ProcessCount >= 5)

End

GO
