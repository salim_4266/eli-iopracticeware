

/****** Object:  StoredProcedure [MVE].[PP_GetDischargeAppointments]    Script Date: 06/29/2018 13:35:32 ******/

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'[MVE].[PP_GetDischargeAppointments]')) 
Drop Procedure [MVE].[PP_GetDischargeAppointments]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  


CREATE Procedure [MVE].[PP_GetDischargeAppointments] @NewAppointmentActionId int, @DischargeAppointmentActionId int, @PatientActionId int 
As          
 Begin        
  SELECT Distinct Top(100) EN.serviceLocationId as LocationExternalId, AP.Resourceid1 as DoctorExternalId, AP.Patientid as PatientExternalId, 7 as  AppointmentStatusExternalId, '' as CancelReason, EN.startDatetime as Start, EN.EndDateTime as [End], En.id as ExternalId FROM dbo.Appointments AP with (nolock) Inner Join  model.Encounters EN with (nolock) ON Ap.EncounterID=EN.id Inner Join MVE.PP_PortalQueueResponse PQR with (nolock) on PQR.ExternalId = AP.PatientId and       
   PQR.ActionTypeLookupId = @PatientActionId Where AP.AppDate > 20170331 and AP.Resourceid1 <> 0   
   and EN.EncounterStatusId = 7 and En.id NOT IN (Select distinct ExternalId from MVE.PP_PortalQueueResponse PQR with (nolock) Where PQR.ActionTypeLookupId = 84 and PQR.ExternalId = EN.Id Union Select distinct ExternalId from MVE.PP_PortalQueueException PQR with (nolock) Where PQR.ExternalId = EN.Id and PQR.ActionTypeLookupId = 84 and ProcessCount > 4) and En.Id in (Select distinct ExternalId from MVE.PP_PortalQueueResponse PQR with (nolock) Where PQR.ActionTypeLookupId = 80 and PQR.ExternalId = AP.AppointmentId)     
End

GO



