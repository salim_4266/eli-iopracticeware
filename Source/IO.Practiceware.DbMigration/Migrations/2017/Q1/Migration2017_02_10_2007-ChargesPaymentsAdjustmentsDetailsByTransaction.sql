--- Changes done by Vikash Sharma

IF NOT Exists(SELECT 1 FROM model.Permissions WHERE Name = 'Charges, Payments, and Adjustments Detail By Transaction')
BEGIN
	INSERT INTO model.Permissions(Name,PermissionCategoryId) 
	VALUES('Charges, Payments, and Adjustments Detail By Transaction',2) 
END
GO

IF NOT Exists(SELECT 1 FROM model.reports WHERE Name = 'Charges, Payments, and Adjustments Detail By Transaction')
BEGIN
	INSERT INTO model.Reports (Name, ReportCategoryId, ReportTypeId, Content, Documentation, PermissionId) 
	(
	SELECT
	'Charges, Payments, and Adjustments Detail By Transaction',5,1,'<?xml version="1.0" encoding="utf-8"?>
	<Report DataSourceName="ChargesPaymentsAndAdjustmentsDetailByTransaction" Width="9.99791717529297in" Name="Charges, Payments, and Adjustments Detail" SnapToGrid="False" xmlns="http://schemas.telerik.com/reporting/2012/3.5">
	<DataSources>
	<SqlDataSource ConnectionString="PracticeRepository" SelectCommand="model.GetChargesPaymentsAndAdjustmentsDetailByTransaction" SelectCommandType="StoredProcedure" CommandTimeout="240" Name="ChargesPaymentsAndAdjustmentsDetailByTransaction">
		<Parameters>
		<SqlDataSourceParameter DbType="DateTime" Name="@StartDate">
			<Value>
			<String>=Parameters.StartDate.Value</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="DateTime" Name="@EndDate">
			<Value>
			<String>=Parameters.EndDate.Value</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="String" Name="@BillingOrganizations">
			<Value>
			<String>=Join(",",Parameters.BillingOrganizations.Value)</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="String" Name="@BillingProviders">
			<Value>
			<String>=Join(",",Parameters.BillingProviders.Value)</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="String" Name="@ScheduledProviders">
			<Value>
			<String>=Join(",",Parameters.ScheduledProviders.Value)</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="String" Name="@AttributeToLocations">
			<Value>
			<String>=Join(",",Parameters.AttributeToLocations.Value)</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="String" Name="@ServiceLocations">
			<Value>
			<String>=Join(",",Parameters.ServiceLocations.Value)</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="String" Name="@ServiceCategories">
			<Value>
			<String>=Join(",",Parameters.ServiceCategories.Value)</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="String" Name="@ServiceCodes">
			<Value>
			<String>=Join(",",Parameters.ServiceCodes.Value)</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowBillingOrganizationBreakdown">
			<Value>
			<String>=Parameters.ShowBillingOrganizationBreakdown.Value</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowMonthlyBreakdown">
			<Value>
			<String>=Parameters.ShowMonthlyBreakdown.Value</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowBillingProviderBreakdown">
			<Value>
			<String>=Parameters.ShowBillingProviderBreakdown.Value</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowScheduledProviderBreakdown">
			<Value>
			<String>=Parameters.ShowScheduledProviderBreakdown.Value</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowAttributeToLocationBreakdown">
			<Value>
			<String>=Parameters.ShowAttributeToLocationBreakdown.Value</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowServiceLocationBreakdown">
			<Value>
			<String>=Parameters.ShowServiceLocationBreakdown.Value</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowServiceCategoryBreakdown">
			<Value>
			<String>=Parameters.ShowServiceCategoryBreakdown.Value</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowServiceCodeBreakdown">
			<Value>
			<String>=Parameters.ShowServiceCodeBreakdown.Value</String>
			</Value>
		</SqlDataSourceParameter>
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowPatientBreakdown">
			<Value>
			<String>=Parameters.ShowPatientBreakdown.Value</String>
			</Value>
		</SqlDataSourceParameter>
		</Parameters>
		<DefaultValues>
		<SqlDataSourceParameter DbType="DateTime" Name="@StartDate" />
		<SqlDataSourceParameter DbType="DateTime" Name="@EndDate" />
		<SqlDataSourceParameter DbType="String" Name="@BillingOrganizations" />
		<SqlDataSourceParameter DbType="String" Name="@BillingProviders" />
		<SqlDataSourceParameter DbType="String" Name="@ScheduledProviders" />
		<SqlDataSourceParameter DbType="String" Name="@AttributeToLocations" />
		<SqlDataSourceParameter DbType="String" Name="@ServiceLocations" />
		<SqlDataSourceParameter DbType="String" Name="@ServiceCategories" />
		<SqlDataSourceParameter DbType="String" Name="@ServiceCodes" />
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowBillingOrganizationBreakdown" />
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowMonthlyBreakdown" />
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowBillingProviderBreakdown" />
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowScheduledProviderBreakdown" />
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowAttributeToLocationBreakdown" />
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowServiceLocationBreakdown" />
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowServiceCategoryBreakdown" />
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowServiceCodeBreakdown" />
		<SqlDataSourceParameter DbType="Boolean" Name="@ShowPatientBreakdown" />
		</DefaultValues>
	</SqlDataSource>
	<SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT Id, ShortName FROM model.BillingOrganizations&#xD;&#xA;ORDER BY ShortName" Name="BillingOrganizations" />
	<SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT Id, UserName FROM model.Users&#xD;&#xA;WHERE __EntityType__ = ''Doctor''&#xD;&#xA;ORDER BY UserName" Name="Users" />
	<SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT Id, ShortName FROM model.ServiceLocations&#xD;&#xA;ORDER BY IsExternal, ShortName" Name="ServiceLocations" />
	<SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT Id, Name FROM model.EncounterServiceTypes&#xD;&#xA;ORDER BY Name" Name="ServiceCategories" />
	<SqlDataSource ConnectionString="PracticeRepository" SelectCommand="SELECT Code, Code + '' - '' + Description AS ServiceCode FROM model.EncounterServices&#xD;&#xA;ORDER BY Code" Name="ServiceCodes" />
	<SqlDataSource ConnectionString="PracticeRepository" SelectCommand="&#xD;&#xA;SELECT TOP 1 &#xD;&#xA;&#x9;bo.Name AS MainOfficeName&#xD;&#xA;&#x9;,a.Line1 AS MainOfficeLine1&#xD;&#xA;&#x9;,a.Line2 AS MainOfficeLine2&#xD;&#xA;&#x9;,a.Line3 AS MainOfficeLine3&#xD;&#xA;&#x9;,a.City AS MainOfficeCity&#xD;&#xA;&#x9;,sop.Abbreviation AS MainOfficeState&#xD;&#xA;&#x9;,SUBSTRING(a.PostalCode, 0,6) AS MainOfficePostalCode&#xD;&#xA;&#x9;,bop.AreaCode + ''-'' +  SUBSTRING(bop.ExchangeAndSuffix,0,4) + ''-'' + SUBSTRING(bop.ExchangeAndSuffix,4,8) AS ExchangeAndSuffix&#xD;&#xA;FROM model.BillingOrganizationAddresses a&#xD;&#xA;INNER JOIN model.StateOrProvinces sop ON a.StateOrProvinceId = sop.Id&#xD;&#xA;INNER JOIN model.BillingOrganizations bo ON a.BillingOrganizationId = bo.Id&#xD;&#xA;INNER JOIN model.BillingOrganizationPhoneNumbers bop ON bop.BillingOrganizationId = bo.Id&#xD;&#xA;WHERE bo.IsMain = 1&#xD;&#xA;" Name="OfficeAddress" />
	</DataSources>
	<Items>
	<PageHeaderSection PrintOnFirstPage="False" Height="0.199999968210856in" Name="pageHeaderSection1">
		<Style>
		<Font Size="8pt" />
		</Style>
		<Items>
		<TextBox Width="1.20000044504801in" Height="0.199999968210856in" Left="8.80000114440918in" Top="0in" Value="Page {PageNumber}" Name="textBox80">
			<Style TextAlign="Right" VerticalAlign="Middle" />
		</TextBox>
		</Items>
	</PageHeaderSection>
	<DetailSection Height="0.200078805287679in" Name="detailSection1">
		<Style TextAlign="Center" VerticalAlign="Middle" />
		<Items>
		<HtmlTextBox Width="0.354166746139527in" Height="0.199999968210856in" Left="0.00007915496826172in" Top="0in" Value="This report provides a list of the charges, payments, and adjustments posted during the specified date range. Invoice date is used for charges and payment posting date is used for payments and adjustments.&amp;nbsp;" Name="Documentation">
			<Style Visible="False">
			<Font Size="8pt" />
			</Style>
		</HtmlTextBox>
		<TextBox Width="1.59455426534017in" Height="0.199961225191752in" Left="0.354324579238892in" Top="0in" Value="{PatientName}" Anchoring="Right" Name="textBox108">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.957450270652771in" Height="0.199961230158806in" Left="1.94895776112874in" Top="0in" Value="{PatientId}" Anchoring="Right" Name="textBox109">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.55200449625651in" Height="0.19999997317791in" Left="2.90648682912191in" Top="0in" Value="= Sum(Fields.Qty)" Format="{0:N1}" Anchoring="Right" Name="textBox110">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.935417652130127in" Height="0.19999997317791in" Left="3.45857016245524in" Top="0in" Value="= Sum(Fields.Charges)" Format="{0:N2}" Anchoring="Right" Name="textBox111">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="1.10000002384186in" Height="0.19999997317791in" Left="4.39607016245524in" Top="0in" Value="= Sum(Fields.InsurerPayment)" Format="{0:N2}" Anchoring="Right" Name="textBox112">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.899999916553497in" Height="0.19999997317791in" Left="5.50023651123047in" Top="0in" Value="= Sum(Fields.PatientPayment)" Format="{0:N2}" Anchoring="Right" Name="textBox113">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.8891894419988in" Height="0.19999997317791in" Left="6.40031560262044in" Top="0in" Value="= Sum(Fields.TotalPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox114">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.898231486479441in" Height="0.19999997317791in" Left="7.29359308878581in" Top="0in" Value="= Sum(Fields.NegativeAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox115">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.912420272827148in" Height="0.19999997317791in" Left="8.19190343221029in" Top="0in" Value="= Sum(Fields.PositiveAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox116">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.893985768159231in" Height="0.19999997317791in" Left="9.10432434082031in" Top="0in" Value="= Sum(Fields.TotalAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox117">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		</Items>
		<ConditionalFormatting>
		<FormattingRule>
			<Style Visible="False" />
			<Filters>
			<Filter Expression="= Parameters.ShowPatientBreakdown.Value" Operator="Equal" Value="= 0" />
			</Filters>
		</FormattingRule>
		</ConditionalFormatting>
	</DetailSection>
	<ReportFooterSection Height="0.199999968210856in" Name="reportFooterSection1">
		<Items>
		<TextBox Width="0.552083333333333in" Height="0.19999997317791in" Left="2.90208339691162in" Top="0in" Value="= Sum(Qty)" Format="{0:N1}" Name="textBox79">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.945833683013916in" Height="0.19999997317791in" Left="3.45416609446208in" Top="0in" Value="= Sum(Charges)" Format="{0:N2}" Name="textBox78">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="1.10000002384186in" Height="0.19999997317791in" Left="4.39999993642171in" Top="0in" Value="= Sum(InsurerPayment)" Format="{0:N2}" Name="textBox77">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.90000057220459in" Height="0.19999997317791in" Left="5.50000063578288in" Top="0in" Value="= Sum(PatientPayment)" Format="{0:N2}" Name="textBox76">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.900000274181366in" Height="0.19999997317791in" Left="6.40000025431315in" Top="0in" Value="= Sum(TotalPayments)" Format="{0:N2}" Name="textBox75">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.800000488758087in" Height="0.19999997317791in" Left="7.3000005086263in" Top="0in" Value="= Sum(NegativeAdjustments)" Format="{0:N2}" Name="textBox74">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.999999344348907in" Height="0.19999997317791in" Left="8.1000010172526in" Top="0in" Value="= Sum(PositiveAdjustments)" Format="{0:N2}" Name="textBox73">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="0.900000274181366in" Height="0.19999997317791in" Left="9.1000010172526in" Top="0in" Value="= Sum(TotalAdjustments)" Format="{0:N2}" Name="textBox72">
			<Style TextAlign="Right" VerticalAlign="Middle">
			<Font Size="8pt" />
			</Style>
		</TextBox>
		<TextBox Width="2.90216231346131in" Height="0.19999997317791in" Left="0in" Top="0in" Value="Sum for report:" Name="textBox71">
			<Style VerticalAlign="Middle">
			<Font Bold="True" />
			</Style>
		</TextBox>
		</Items>
	</ReportFooterSection>
	<ReportHeaderSection Height="2.68618929386139in" Name="reportHeaderSection1">
		<Style>
		<Font Size="8pt" />
		</Style>
		<Items>
		<TextBox Width="5.18526087825484in" Height="0.199960763255755in" Left="3.21466189001998in" Top="0in" Value="Charges, Payments and Adjustments Detail By Transaction" Anchoring="Top, Left, Right" Name="textBox14">
			<Style TextAlign="Center" LineStyle="Solid" LineWidth="4pt">
			<Font Name="Arial" Size="13pt" Bold="True" Underline="False" />
			</Style>
		</TextBox>
		<TextBox Width="1.59996032714844in" Height="0.399921089410782in" Left="8.40000152587891in" Top="0in" Value="Date run: {Now().ToString(&quot;MM/dd/yyyy&quot;)}&#xD;&#xA;{Now().ToString(&quot;hh:mm tt&quot;)}" Name="textBox17">
			<Style TextAlign="Right">
			<Font Name="Arial" />
			</Style>
		</TextBox>
		<HtmlTextBox Width="0.559722185134888in" Height="0.200039347012838in" Left="3.70416688919067in" Top="0.600157141685486in" Value="AccountsReceivable" Name="ReportCategory">
			<Style Visible="False" />
		</HtmlTextBox>
		<TextBox Width="1.28221246931287in" Height="0.196796053011592in" Left="0.00003941853841146in" Top="0.910069386164347in" Value="Start Date:" Format="{0:d}" Name="textBox81">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="True" />
			</Style>
		</TextBox>
		<TextBox Width="1.28221265474955in" Height="0.196527540683746in" Left="0.00003941853841146in" Top="2.09644905726115in" Value="Service Location(s):" Format="" Name="textBox82">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="True" />
			</Style>
		</TextBox>
		<TextBox Width="1.28221277395884in" Height="0.19775915145874in" Left="0.00003941853841146in" Top="1.50285657246908in" Value="Billing Provider(s):" Format="" Name="textBox83">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="True" />
			</Style>
		</TextBox>
		<TextBox Width="1.28221289316813in" Height="0.197916507720947in" Left="0.00003941853841146in" Top="1.1069446404775in" Value="End Date:" Format="{0:d}" Name="textBox84">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="True" />
			</Style>
		</TextBox>
		<TextBox Width="2.74853587150574in" Height="0.198957845568657in" Left="1.28233122825623in" Top="1.1069446404775in" Value="{Parameters.EndDate.Value.ToString(&quot;MM/dd/yyyy&quot;)}" Format="{0:d}" Name="textBox146">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="False" />
			</Style>
		</TextBox>
		<TextBox Width="2.74853571256002in" Height="0.197837829589844in" Left="1.28237056732178in" Top="0.909027576446533in" Value="{Parameters.StartDate.Value.ToString(&quot;MM/dd/yyyy&quot;)}" Format="{0:d}" Name="textBox145">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="False" />
			</Style>
		</TextBox>
		<TextBox Width="8.71975040435791in" Height="0.196717798709869in" Left="1.28237064679464in" Top="1.50389798482259in" Value="=IsNull(Join(&quot;,&quot;,Parameters.BillingProviders.Label),&quot;All&quot;)" Format="" Name="textBox142">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="False" />
			</Style>
		</TextBox>
		<TextBox Width="8.71766916910807in" Height="0.196527540683746in" Left="1.28233083089193in" Top="2.09644905726115in" Value="= IsNull(Join(&quot;,&quot;,Parameters.ServiceLocations.Label),&quot;All&quot;)" Format="" Name="textBox141">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="False" />
			</Style>
		</TextBox>
		<TextBox Width="8.71766901016235in" Height="0.197837889194489in" Left="1.28233106931051in" Top="1.30598131815592in" Value="=IsNull(Join(&quot;,&quot;, Parameters.BillingOrganizations.Label),&quot;All&quot;)" Format="" Name="textBox143">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="False" />
			</Style>
		</TextBox>
		<TextBox Width="1.28221301237742in" Height="0.197837889194489in" Left="0.00007883707682292in" Top="1.30493990580241in" Value="Billing Org(s):" Format="" Name="textBox85">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="True" />
			</Style>
		</TextBox>
		<TextBox Width="8.71766980489095in" Height="0.197759201129277in" Left="1.28237064679464in" Top="1.70069456100464in" Value="=IsNull(Join(&quot;,&quot;,Parameters.ScheduledProviders.Label),&quot;All&quot;)" Format="" Name="textBox86">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="False" />
			</Style>
		</TextBox>
		<TextBox Width="1.28221277395884in" Height="0.19775915145874in" Left="0.00007915496826172in" Top="1.70069456100464in" Value="Scheduled Provider(s):" Format="" Name="textBox87">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="True" />
			</Style>
		</TextBox>
		<TextBox Width="8.716628074646in" Height="0.196527540683746in" Left="1.28233083089193in" Top="2.29305537541707in" Value="= IsNull(Join(&quot;,&quot;,Parameters.ServiceCategories.Label),&quot;All&quot;)" Format="" Name="textBox88">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="False" />
			</Style>
		</TextBox>
		<TextBox Width="1.28221261501312in" Height="0.196527540683746in" Left="0.00003941853841146in" Top="2.29305537541707in" Value="Service Categor(y/ies):" Format="" Name="textBox89">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="True" />
			</Style>
		</TextBox>
		<TextBox Width="1.28221261501312in" Height="0.196527540683746in" Left="0.00003941853841146in" Top="2.489661693573in" Value="Service Code(s):" Format="" Name="textBox90">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="True" />
			</Style>
		</TextBox>
		<TextBox Width="8.71558666229248in" Height="0.196527540683746in" Left="1.28233083089193in" Top="2.489661693573in" Value="= IsNull(Join(&quot;,&quot;,Parameters.ServiceCodes.Value),&quot;All&quot;)" Format="" Name="textBox91">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="False" />
			</Style>
		</TextBox>
		<List DataSourceName="OfficeAddress" Width="3.2145832379659in" Height="0.800157070159912in" Left="0in" Top="0in" Name="list1">
			<Body>
			<Cells>
				<TableCell RowIndex="0" ColumnIndex="0" RowSpan="1" ColumnSpan="1">
				<ReportItem>
					<Panel Width="3.2145832379659in" Height="0.800157070159912in" Left="0in" Top="0in" Name="panel1">
					<Items>
						<TextBox Width="3.20416680971781in" Height="0.199921295046806in" Left="0in" Top="0in" Value="{MainOfficeName}" Name="textBox92">
						<Style TextAlign="Left" VerticalAlign="Middle">
							<Font Size="10pt" Bold="True" />
						</Style>
						</TextBox>
						<TextBox Width="3.20416680971781in" Height="0.19999997317791in" Left="0in" Top="0.19999997317791in" Value="{MainOfficeLine1} {MainOfficeLine2}" Name="textBox93">
						<Style TextAlign="Left" VerticalAlign="Middle">
							<Font Size="10pt" Bold="True" />
						</Style>
						</TextBox>
						<TextBox Width="3.20416680971781in" Height="0.19999997317791in" Left="0in" Top="0.400078624486923in" Value="{MainOfficeCity}, {MainOfficeState} {MainOfficePostalCode}" Name="textBox94">
						<Style TextAlign="Left" VerticalAlign="Middle">
							<Font Size="10pt" Bold="True" />
						</Style>
						</TextBox>
						<TextBox Width="3.20416680971781in" Height="0.19999997317791in" Left="0in" Top="0.600117683410645in" Value="{ExchangeAndSuffix}" Name="textBox95">
						<Style TextAlign="Left" VerticalAlign="Middle">
							<Font Size="10pt" Bold="True" />
						</Style>
						</TextBox>
					</Items>
					</Panel>
				</ReportItem>
				</TableCell>
			</Cells>
			<Columns>
				<Column Width="3.2145832379659in" />
			</Columns>
			<Rows>
				<Row Height="0.800157070159912in" />
			</Rows>
			</Body>
			<Corner />
			<RowGroups>
			<TableGroup Name="DetailGroup">
				<Groupings>
				<Grouping />
				</Groupings>
			</TableGroup>
			</RowGroups>
			<ColumnGroups>
			<TableGroup Name="ColumnGroup" />
			</ColumnGroups>
		</List>
		<TextBox Width="8.71766948699951in" Height="0.196527540683746in" Left="1.28445180257161in" Top="1.8985325495402in" Value="= IsNull(Join(&quot;,&quot;,Parameters.AttributeToLocations.Label),&quot;All&quot;)" Format="" Name="textBox96">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="False" />
			</Style>
		</TextBox>
		<TextBox Width="1.28221261501312in" Height="0.196527540683746in" Left="0in" Top="1.8985325495402in" Value="Attributed Location(s):" Format="" Name="textBox97">
			<Style TextAlign="Left" VerticalAlign="Middle">
			<Font Size="8pt" Bold="True" />
			</Style>
		</TextBox>
		</Items>
	</ReportHeaderSection>
	</Items>
	<StyleSheet>
	<StyleRule>
		<Style>
		<Padding Left="2pt" Right="2pt" />
		</Style>
		<Selectors>
		<TypeSelector Type="TextItemBase" />
		<TypeSelector Type="HtmlTextBox" />
		</Selectors>
	</StyleRule>
	</StyleSheet>
	<PageSettings>
	<PageSettings PaperKind="Letter" Landscape="True">
		<Margins>
		<MarginsU Left="0.5in" Right="0.5in" Top="0.5in" Bottom="0.5in" />
		</Margins>
	</PageSettings>
	</PageSettings>
	<Sortings>
	<Sorting Expression="= Fields.PatientName" Direction="Asc" />
	</Sortings>
	<Groups>
	<Group Name="group5">
		<GroupHeader>
		<GroupHeaderSection PrintOnEveryPage="True" Height="0.400000254313151in" Name="groupHeaderSection5">
			<Items>
			<TextBox Width="2.90216191609701in" Height="0.199999968210856in" Left="0.0000003178914388in" Top="0.200000127156576in" Value="Service" Name="textBox1">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
				<ConditionalFormatting>
				<FormattingRule>
					<Style Visible="False" />
					<Filters>
					<Filter Expression="= Parameters.ShowServiceCodeBreakdown.Value" Operator="Equal" Value="= 0" />
					</Filters>
				</FormattingRule>
				</ConditionalFormatting>
			</TextBox>
			<TextBox Width="0.541744550069173in" Height="0.199999968210856in" Left="2.90208339691162in" Top="0.200000127156576in" Value="Qty" Anchoring="Right" Name="textBox2">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="0.956093575805425in" Height="0.199999968210856in" Left="3.44390678405762in" Top="0.200000127156576in" Value="Charges" Anchoring="Right" Name="textBox3">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="1.08950364589691in" Height="0.200000127156576in" Left="4.40007932980855in" Top="0.200000127156576in" Value="Insurer Paid" Anchoring="Right" Name="textBox4">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="0.899842580159505in" Height="0.199999968210856in" Left="5.48958333333333in" Top="0.200000127156576in" Value="Patient Paid" Anchoring="Right" Name="textBox5">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="0.899921099344889in" Height="0.199999968210856in" Left="6.38958358764648in" Top="0.200000127156576in" Value="Total Paid" Anchoring="Right" Name="textBox6">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="0.900000254313151in" Height="0.400000095367432in" Left="7.28958384195964in" Top="0in" Value="Negative Adjustments" Anchoring="Right" Name="textBox7">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="0.900000274181366in" Height="0.400000095367432in" Left="8.18958409627279in" Top="0in" Value="Positive Adjustments" Anchoring="Right" Name="textBox8">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="0.912497858206431in" Height="0.400000095367432in" Left="9.08958435058594in" Top="0in" Value="Total Adjustments" Anchoring="Right" Name="textBox9">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			</Items>
		</GroupHeaderSection>
		</GroupHeader>
		<GroupFooter>
		<GroupFooterSection Height="0.0520833333333333in" Name="groupFooterSection5">
			<Style Visible="False" />
		</GroupFooterSection>
		</GroupFooter>
	</Group>
	<Group Name="group4">
		<GroupHeader>
		<GroupHeaderSection Height="0.199999968210856in" Name="groupHeaderSection4">
			<Items>
			<TextBox Width="4.40000025431315in" Height="0.19999997317791in" Left="0in" Top="0in" Value="Billing Organization: {BillingOrganization}" Name="textBox16">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowBillingOrganizationBreakdown.Value" Operator="Equal" Value="= False" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupHeaderSection>
		</GroupHeader>
		<GroupFooter>
		<GroupFooterSection Height="0.199999968210856in" Name="groupFooterSection4">
			<Items>
			<TextBox Width="2.90208347638448in" Height="0.19999997317791in" Left="0in" Top="0in" Value="Sum for {BillingOrganization}:" Name="textBox62">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="0.900000274181366in" Height="0.19999997317791in" Left="9.09999847412109in" Top="0in" Value="= Sum(TotalAdjustments)" Format="{0:N2}" Name="textBox63">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1in" Height="0.19999997317791in" Left="8.09999974568685in" Top="0in" Value="= Sum(PositiveAdjustments)" Format="{0:N2}" Name="textBox64">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.800000488758087in" Height="0.19999997317791in" Left="7.29999923706055in" Top="0in" Value="= Sum(NegativeAdjustments)" Format="{0:N2}" Name="textBox65">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.899999618530273in" Height="0.19999997317791in" Left="6.3999989827474in" Top="0in" Value="= Sum(TotalPayments)" Format="{0:N2}" Name="textBox66">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.90000057220459in" Height="0.19999997317791in" Left="5.5in" Top="0in" Value="= Sum(PatientPayment)" Format="{0:N2}" Name="textBox67">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1.10000002384186in" Height="0.19999997317791in" Left="4.39999993642171in" Top="0in" Value="= Sum(InsurerPayment)" Format="{0:N2}" Name="textBox68">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.945833683013916in" Height="0.19999997317791in" Left="3.45416609446208in" Top="0in" Value="= Sum(Charges)" Format="{0:N2}" Name="textBox69">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.552083333333333in" Height="0.19999997317791in" Left="2.90208339691162in" Top="0in" Value="= Sum(Qty)" Format="{0:N1}" Name="textBox70">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowBillingOrganizationBreakdown.Value" Operator="Equal" Value="= False" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupFooterSection>
		</GroupFooter>
		<Groupings>
		<Grouping Expression="= Fields.BillingOrganization" />
		</Groupings>
		<Sortings>
		<Sorting Expression="= Fields.BillingOrganization" Direction="Asc" />
		</Sortings>
	</Group>
	<Group Name="group8">
		<GroupHeader>
		<GroupHeaderSection Height="0.200078805287679in" Name="groupHeaderSection8">
			<Items>
			<TextBox Width="2.89583381017049in" Height="0.19999997317791in" Left="0in" Top="0in" Value="{MonthYear}" Name="textBox118">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="0.562578837076823in" Height="0.19999997317791in" Left="2.8959124883016in" Top="0in" Value="= Sum(Qty)" Format="{0:N1}" Name="textBox119">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.935496171315511in" Height="0.19999997317791in" Left="3.45849132537842in" Top="0in" Value="= Sum(Charges)" Format="{0:N2}" Name="textBox120">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1.1103378534317in" Height="0.19999997317791in" Left="4.39607016245524in" Top="0in" Value="= Sum(InsurerPayment)" Format="{0:N2}" Name="textBox121">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.90000057220459in" Height="0.19999997317791in" Left="5.51057434082031in" Top="0in" Value="= Sum(PatientPayment)" Format="{0:N2}" Name="textBox122">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.89567502339681in" Height="0.19999997317791in" Left="6.41073226928711in" Top="0in" Value="= Sum(TotalPayments)" Format="{0:N2}" Name="textBox123">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.791747391223907in" Height="0.19999997317791in" Left="7.31049410502116in" Top="0in" Value="= Sum(NegativeAdjustments)" Format="{0:N2}" Name="textBox124">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1in" Height="0.19999997317791in" Left="8.10432434082031in" Top="0in" Value="= Sum(PositiveAdjustments)" Format="{0:N2}" Name="textBox125">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.900000274181366in" Height="0.19999997317791in" Left="9.10432434082031in" Top="0in" Value="= Sum(TotalAdjustments)" Format="{0:N2}" Name="textBox126">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowMonthlyBreakdown.Value" Operator="Equal" Value="= 0" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupHeaderSection>
		</GroupHeader>
		<GroupFooter>
		<GroupFooterSection Height="0.0520833333333332in" Name="groupFooterSection8">
			<Style Visible="False" />
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowMonthlyBreakdown.Value" Operator="Equal" Value="= 0" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupFooterSection>
		</GroupFooter>
		<Groupings>
		<Grouping Expression="= Fields.MonthYear" />
		</Groupings>
		<Sortings>
		<Sorting Expression="= Fields.MonthYearInt" Direction="Asc" />
		</Sortings>
	</Group>
	<Group Name="group3">
		<GroupHeader>
		<GroupHeaderSection Height="0.199999968210856in" Name="groupHeaderSection3">
			<Items>
			<TextBox Width="4.40000025431315in" Height="0.19999997317791in" Left="0in" Top="0in" Value="Billing Provider: {BillingProvider}" Name="textBox15">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowBillingProviderBreakdown.Value" Operator="Equal" Value="= False" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupHeaderSection>
		</GroupHeader>
		<GroupFooter>
		<GroupFooterSection Height="0.199999968210856in" Name="groupFooterSection3">
			<Items>
			<TextBox Width="2.90208347638448in" Height="0.19999997317791in" Left="0in" Top="0in" Value="Sum for {BillingProvider}:" Name="textBox53">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="0.900000274181366in" Height="0.19999997317791in" Left="9.09999847412109in" Top="0in" Value="= Sum(TotalAdjustments)" Format="{0:N2}" Name="textBox54">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1.00000001986821in" Height="0.19999997317791in" Left="8.09999974568685in" Top="0in" Value="= Sum(PositiveAdjustments)" Format="{0:N2}" Name="textBox55">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.799999852975209in" Height="0.19999997317791in" Left="7.29999923706055in" Top="0in" Value="= Sum(NegativeAdjustments)" Format="{0:N2}" Name="textBox56">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.899999638398488in" Height="0.19999997317791in" Left="6.3999989827474in" Top="0in" Value="= Sum(TotalPayments)" Format="{0:N2}" Name="textBox57">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.90000057220459in" Height="0.19999997317791in" Left="5.5in" Top="0in" Value="= Sum(PatientPayment)" Format="{0:N2}" Name="textBox58">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1.10000002384186in" Height="0.19999997317791in" Left="4.39999993642171in" Top="0in" Value="= Sum(InsurerPayment)" Format="{0:N2}" Name="textBox59">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.945833683013916in" Height="0.19999997317791in" Left="3.45416609446208in" Top="0in" Value="= Sum(Charges)" Format="{0:N2}" Name="textBox60">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.552083333333333in" Height="0.19999997317791in" Left="2.90208339691162in" Top="0in" Value="= Sum(Qty)" Format="{0:N1}" Name="textBox61">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowBillingProviderBreakdown.Value" Operator="Equal" Value="= False" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupFooterSection>
		</GroupFooter>
		<Groupings>
		<Grouping Expression="= Fields.BillingProvider" />
		</Groupings>
		<Sortings>
		<Sorting Expression="= Fields.BillingProvider" Direction="Asc" />
		</Sortings>
	</Group>
	<Group Name="group2">
		<GroupHeader>
		<GroupHeaderSection Height="0.199999968210856in" Name="groupHeaderSection2">
			<Items>
			<TextBox Width="4.40000025431315in" Height="0.19999997317791in" Left="0in" Top="0in" Value="Scheduled Provider: {ScheduledProvider}" Name="textBox13">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowScheduledProviderBreakdown.Value" Operator="Equal" Value="= False" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupHeaderSection>
		</GroupHeader>
		<GroupFooter>
		<GroupFooterSection Height="0.199999968210856in" Name="groupFooterSection2">
			<Items>
			<TextBox Width="2.90208347638448in" Height="0.19999997317791in" Left="0in" Top="0in" Value="Sum for {ScheduledProvider}:" Name="textBox44">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="0.899999638398488in" Height="0.19999997317791in" Left="9.09999974568685in" Top="0in" Value="= Sum(TotalAdjustments)" Format="{0:N2}" Name="textBox45">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.999999980131785in" Height="0.19999997317791in" Left="8.09999974568685in" Top="0in" Value="= Sum(PositiveAdjustments)" Format="{0:N2}" Name="textBox46">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.800000488758087in" Height="0.19999997317791in" Left="7.29999923706055in" Top="0in" Value="= Sum(NegativeAdjustments)" Format="{0:N2}" Name="textBox47">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.899999638398488in" Height="0.19999997317791in" Left="6.3999989827474in" Top="0in" Value="= Sum(TotalPayments)" Format="{0:N2}" Name="textBox48">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.900001207987468in" Height="0.19999997317791in" Left="5.5in" Top="0in" Value="= Sum(PatientPayment)" Format="{0:N2}" Name="textBox49">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1.10000002384186in" Height="0.19999997317791in" Left="4.39999993642171in" Top="0in" Value="= Sum(InsurerPayment)" Format="{0:N2}" Name="textBox50">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.945833683013916in" Height="0.19999997317791in" Left="3.45416609446208in" Top="0in" Value="= Sum(Charges)" Format="{0:N2}" Name="textBox51">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.552083333333333in" Height="0.19999997317791in" Left="2.90208339691162in" Top="0in" Value="= Sum(Qty)" Format="{0:N1}" Name="textBox52">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowScheduledProviderBreakdown.Value" Operator="Equal" Value="= False" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupFooterSection>
		</GroupFooter>
		<Groupings>
		<Grouping Expression="= Fields.ScheduledProvider" />
		</Groupings>
		<Sortings>
		<Sorting Expression="= Fields.ScheduledProvider" Direction="Asc" />
		</Sortings>
	</Group>
	<Group Name="group6">
		<GroupHeader>
		<GroupHeaderSection Height="0.20007848739624in" Name="groupHeaderSection6">
			<Items>
			<TextBox Width="4.40000009536743in" Height="0.19999997317791in" Left="0in" Top="0in" Value="Attributed Service Location: {AttributedServiceLocation}" Name="textBox98">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowAttributeToLocationBreakdown.Value" Operator="Equal" Value="= 0" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupHeaderSection>
		</GroupHeader>
		<GroupFooter>
		<GroupFooterSection Height="0.200079441070557in" Name="groupFooterSection6">
			<Items>
			<TextBox Width="2.90208339691162in" Height="0.19999997317791in" Left="0.00015799204508464in" Top="0in" Value="Sum for {AttributedServiceLocation}:" Name="textBox99">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="0.900000274181366in" Height="0.19999997317791in" Left="9.10432434082031in" Top="0in" Value="= Sum(TotalAdjustments)" Format="{0:N2}" Name="textBox100">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1.00200333197912in" Height="0.19999997317791in" Left="8.1023203531901in" Top="0in" Value="= Sum(PositiveAdjustments)" Format="{0:N2}" Name="textBox101">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.802241411680977in" Height="0.19999997317791in" Left="7.30000008456409in" Top="0in" Value="= Sum(NegativeAdjustments)" Format="{0:N2}" Name="textBox102">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.899999426677823in" Height="0.19999997317791in" Left="6.3999989827474in" Top="0in" Value="= Sum(TotalPayments)" Format="{0:N2}" Name="textBox103">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.899999724442334in" Height="0.19999997317791in" Left="5.5in" Top="0in" Value="= Sum(PatientPayment)" Format="{0:N2}" Name="textBox104">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1.10000002384186in" Height="0.19999997317791in" Left="4.39999993642171in" Top="0in" Value="= Sum(InsurerPayment)" Format="{0:N2}" Name="textBox105">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.941429615020752in" Height="0.19999997317791in" Left="3.45849132537842in" Top="0in" Value="= Sum(Charges)" Format="{0:N2}" Name="textBox106">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.552083313465118in" Height="0.19999997317791in" Left="2.90640799204508in" Top="0in" Value="= Sum(Qty)" Format="{0:N1}" Name="textBox107">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowAttributeToLocationBreakdown.Value" Operator="Equal" Value="= 0" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupFooterSection>
		</GroupFooter>
		<Groupings>
		<Grouping Expression="= Fields.AttributedServiceLocation" />
		</Groupings>
		<Sortings>
		<Sorting Expression="= Fields.AttributedServiceLocation" Direction="Asc" />
		</Sortings>
	</Group>
	<Group Name="group1">
		<GroupHeader>
		<GroupHeaderSection Height="0.199999968210856in" Name="groupHeaderSection1">
			<Items>
			<TextBox Width="4.40000025431315in" Height="0.199999968210856in" Left="0in" Top="0in" Value="Service Location: {ServiceLocation}" Name="textBox12">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowServiceLocationBreakdown.Value" Operator="Equal" Value="= False" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupHeaderSection>
		</GroupHeader>
		<GroupFooter>
		<GroupFooterSection Height="0.199999968210856in" Name="groupFooterSection1">
			<Items>
			<TextBox Width="0.552083333333333in" Height="0.19999997317791in" Left="2.90208339691162in" Top="0in" Value="= Sum(Qty)" Format="{0:N1}" Name="textBox35">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.945833702882131in" Height="0.19999997317791in" Left="3.45416609446208in" Top="0in" Value="= Sum(Charges)" Format="{0:N2}" Name="textBox36">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1.10000002384186in" Height="0.19999997317791in" Left="4.39999993642171in" Top="0in" Value="= Sum(InsurerPayment)" Format="{0:N2}" Name="textBox37">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.90000057220459in" Height="0.19999997317791in" Left="5.5in" Top="0in" Value="= Sum(PatientPayment)" Format="{0:N2}" Name="textBox38">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.900000254313151in" Height="0.19999997317791in" Left="6.3999989827474in" Top="0in" Value="= Sum(TotalPayments)" Format="{0:N2}" Name="textBox39">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.800000488758087in" Height="0.19999997317791in" Left="7.29999923706055in" Top="0in" Value="= Sum(NegativeAdjustments)" Format="{0:N2}" Name="textBox40">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.999999364217122in" Height="0.19999997317791in" Left="8.09999974568685in" Top="0in" Value="= Sum(PositiveAdjustments)" Format="{0:N2}" Name="textBox41">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.900000274181366in" Height="0.19999997317791in" Left="9.09999847412109in" Top="0in" Value="= Sum(TotalAdjustments)" Format="{0:N2}" Name="textBox42">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="2.90208347638448in" Height="0.19999997317791in" Left="0in" Top="0in" Value="Sum for {ServiceLocation}:" Name="textBox43">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowServiceLocationBreakdown.Value" Operator="Equal" Value="= False" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupFooterSection>
		</GroupFooter>
		<Groupings>
		<Grouping Expression="= Fields.ServiceLocation" />
		</Groupings>
		<Sortings>
		<Sorting Expression="= Fields.ServiceLocation" Direction="Asc" />
		</Sortings>
	</Group>
	<Group Name="group">
		<GroupHeader>
		<GroupHeaderSection Height="0.199999968210856in" Name="groupHeaderSection">
			<Items>
			<TextBox Width="4.40000027418137in" Height="0.19999997317791in" Left="0in" Top="0in" Value="Service Category: {ServiceCategory}" Name="textBox11">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowServiceCategoryBreakdown.Value" Operator="Equal" Value="= False" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupHeaderSection>
		</GroupHeader>
		<GroupFooter>
		<GroupFooterSection Height="0.199999968210856in" Name="groupFooterSection">
			<Items>
			<TextBox Width="2.90208347638448in" Height="0.19999997317791in" Left="0in" Top="0in" Value="Sum for {ServiceCategory}:" Name="textBox26">
				<Style VerticalAlign="Middle">
				<Font Bold="True" />
				</Style>
			</TextBox>
			<TextBox Width="0.902083734671275in" Height="0.19999997317791in" Left="9.09999847412109in" Top="0in" Value="= Sum(TotalAdjustments)" Format="{0:N2}" Name="textBox27">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1in" Height="0.19999997317791in" Left="8.09999847412109in" Top="0in" Value="= Sum(PositiveAdjustments)" Format="{0:N2}" Name="textBox28">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.79999985297521in" Height="0.19999997317791in" Left="7.29999923706055in" Top="0in" Value="= Sum(NegativeAdjustments)" Format="{0:N2}" Name="textBox29">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.899999598662058in" Height="0.19999997317791in" Left="6.3999989827474in" Top="0in" Value="= Sum(TotalPayments)" Format="{0:N2}" Name="textBox30">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.899999916553497in" Height="0.19999997317791in" Left="5.5in" Top="0in" Value="= Sum(PatientPayment)" Format="{0:N2}" Name="textBox31">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1.10000002384186in" Height="0.19999997317791in" Left="4.39999993642171in" Top="0in" Value="= Sum(InsurerPayment)" Format="{0:N2}" Name="textBox32">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.94583402077357in" Height="0.19999997317791in" Left="3.45416609446208in" Top="0in" Value="= Sum(Charges)" Format="{0:N2}" Name="textBox33">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.552083333333333in" Height="0.19999997317791in" Left="2.90208339691162in" Top="0in" Value="= Sum(Qty)" Format="{0:N1}" Name="textBox34">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowServiceCategoryBreakdown.Value" Operator="Equal" Value="= False" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupFooterSection>
		</GroupFooter>
		<Groupings>
		<Grouping Expression="= Fields.ServiceCategory" />
		</Groupings>
		<Sortings>
		<Sorting Expression="= Fields.ServiceCategory" Direction="Asc" />
		</Sortings>
	</Group>
	<Group Name="group7">
		<GroupHeader>
		<GroupHeaderSection Height="0.200078805287679in" Name="groupHeaderSection7">
			<Items>
			<TextBox Width="2.91265757878621in" Height="0.19999997317791in" Left="-0.00624942779544702in" Top="0in" Value="= Fields.ServiceCode" CanGrow="False" Anchoring="Right" Name="textBox10">
				<Style VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.89355343580246in" Height="0.19999997317791in" Left="9.10432434082031in" Top="0in" Value="= Sum(Fields.TotalAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox25">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.921147028605143in" Height="0.19999997317791in" Left="8.18317667643225in" Top="0in" Value="= Sum(Fields.PositiveAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox24">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.889504730701447in" Height="0.19999997317791in" Left="7.29359308878577in" Top="0in" Value="= Sum(Fields.NegativeAdjustments)" Format="{0:N2}" Anchoring="Right" Name="textBox23">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.895755151907603in" Height="0.19999997317791in" Left="6.39390818277991in" Top="0in" Value="= Sum(Fields.TotalPayments)" Format="{0:N2}" Anchoring="Right" Name="textBox22">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.899999916553497in" Height="0.19999997317791in" Left="5.49382972717282in" Top="0in" Value="= Sum(Fields.PatientPayment)" Format="{0:N2}" Anchoring="Right" Name="textBox21">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="1.08958367506663in" Height="0.19999997317791in" Left="4.40007940928138in" Top="0in" Value="= Sum(Fields.InsurerPayment)" Format="{0:N2}" Anchoring="Right" Name="textBox20">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.947837968667348in" Height="0.19999997317791in" Left="3.45216274261471in" Top="0in" Value="= Sum(Fields.Charges)" Format="{0:N2}" Anchoring="Right" Name="textBox19">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			<TextBox Width="0.54559709628423in" Height="0.19999997317791in" Left="2.90648682912188in" Top="0in" Value="= Sum(Fields.Qty)" Format="{0:N1}" Anchoring="Right" Name="textBox18">
				<Style TextAlign="Right" VerticalAlign="Middle">
				<Font Size="8pt" />
				</Style>
			</TextBox>
			</Items>
			<ConditionalFormatting>
			<FormattingRule>
				<Style Visible="False" />
				<Filters>
				<Filter Expression="= Parameters.ShowServiceCodeBreakdown.Value" Operator="Equal" Value="= 0" />
				</Filters>
			</FormattingRule>
			</ConditionalFormatting>
		</GroupHeaderSection>
		</GroupHeader>
		<GroupFooter>
		<GroupFooterSection Height="0.0520833333333333in" Name="groupFooterSection7">
			<Style Visible="False" />
		</GroupFooterSection>
		</GroupFooter>
		<Groupings>
		<Grouping Expression="= Fields.ServiceCode" />
		</Groupings>
		<Sortings>
		<Sorting Expression="= Fields.ServiceCode" Direction="Asc" />
		</Sortings>
	</Group>
	</Groups>
	<ReportParameters>
	<ReportParameter Name="StartDate" Type="DateTime" Text="Start Date" Visible="True" />
	<ReportParameter Name="EndDate" Type="DateTime" Text="End Date" Visible="True" />
	<ReportParameter Name="BillingOrganizations" Text="Billing Organizations" Visible="True" MultiValue="True" AllowNull="True">
		<AvailableValues DataSourceName="BillingOrganizations" DisplayMember="= Fields.ShortName" ValueMember="= Fields.Id" />
	</ReportParameter>
	<ReportParameter Name="BillingProviders" Text="Billing Providers" Visible="True" MultiValue="True" AllowNull="True">
		<AvailableValues DataSourceName="Users" DisplayMember="= Fields.UserName" ValueMember="= Fields.Id" />
	</ReportParameter>
	<ReportParameter Name="ScheduledProviders" Text="Scheduled Providers" Visible="True" MultiValue="True" AllowNull="True">
		<AvailableValues DataSourceName="Users" DisplayMember="= Fields.UserName" ValueMember="= Fields.Id" />
	</ReportParameter>
	<ReportParameter Name="AttributeToLocations" Text="AttributeToLocations" Visible="True" MultiValue="True" AllowNull="True">
		<AvailableValues DataSourceName="ServiceLocations" DisplayMember="= Fields.ShortName" ValueMember="= Fields.Id" />
	</ReportParameter>
	<ReportParameter Name="ServiceLocations" Text="Service Locations" Visible="True" MultiValue="True" AllowNull="True">
		<AvailableValues DataSourceName="ServiceLocations" DisplayMember="= Fields.ShortName" ValueMember="= Fields.Id" />
	</ReportParameter>
	<ReportParameter Name="ServiceCategories" Text="Service Categories" Visible="True" MultiValue="True" AllowNull="True">
		<AvailableValues DataSourceName="ServiceCategories" DisplayMember="= Fields.Name" ValueMember="= Fields.Id" />
	</ReportParameter>
	<ReportParameter Name="ServiceCodes" Text="Service Codes" Visible="True" MultiValue="True" AllowNull="True">
		<AvailableValues DataSourceName="ServiceCodes" DisplayMember="= Fields.ServiceCode" ValueMember="= Fields.Code" />
	</ReportParameter>
	<ReportParameter Name="ShowBillingOrganizationBreakdown" Type="Boolean" Text="Show Billing Organization Breakdown" Visible="True">
		<Value>
		<String>= False</String>
		</Value>
	</ReportParameter>
	<ReportParameter Name="ShowMonthlyBreakdown" Type="Boolean" Text="Show Monthly Breakdown" Visible="True">
		<Value>
		<String>= False</String>
		</Value>
	</ReportParameter>
	<ReportParameter Name="ShowBillingProviderBreakdown" Type="Boolean" Text="Show Billing Provider Breakdown" Visible="True">
		<Value>
		<String>= False</String>
		</Value>
	</ReportParameter>
	<ReportParameter Name="ShowScheduledProviderBreakdown" Type="Boolean" Text="Show Scheduled Provider Breakdown" Visible="True">
		<Value>
		<String>= False</String>
		</Value>
	</ReportParameter>
	<ReportParameter Name="ShowAttributeToLocationBreakdown" Type="Boolean" Text="Show Attribute To Location Breakdown" Visible="True">
		<Value>
		<String>= False</String>
		</Value>
	</ReportParameter>
	<ReportParameter Name="ShowServiceLocationBreakdown" Type="Boolean" Text="Show Service Location Breakdown" Visible="True">
		<Value>
		<String>= False</String>
		</Value>
	</ReportParameter>
	<ReportParameter Name="ShowServiceCategoryBreakdown" Type="Boolean" Text="Show Service Category Breakdown" Visible="True">
		<Value>
		<String>= False</String>
		</Value>
	</ReportParameter>
	<ReportParameter Name="ShowServiceCodeBreakdown" Type="Boolean" Text="Show Service Code Breakdown" Visible="True">
		<Value>
		<String>= True</String>
		</Value>
	</ReportParameter>
	<ReportParameter Name="ShowPatientBreakdown" Type="Boolean" Text="Show Patient Breakdown" Visible="True">
		<Value>
		<String>= False</String>
		</Value>
	</ReportParameter>
	</ReportParameters>
	</Report>',
	'This report will provide you all the adjustments, charges, Payment with reference to the Transaction Date i.e. whenever the payment is posted regardless of date of service. Doing so the practice would now be able to pull the report for the specified transaction date range.'
	,Id 
	FROM model.Permissions WHERE Name='Charges, Payments, and Adjustments Detail By Transaction' 
	)
END
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('model.GetChargesPaymentsAndAdjustmentsDetailByTransaction'))
BEGIN
    DROP PROCEDURE model.GetChargesPaymentsAndAdjustmentsDetailByTransaction
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [model].[GetChargesPaymentsAndAdjustmentsDetailByTransaction](  
 @StartDate datetime  
,@EndDate datetime  
,@BillingOrganizations nvarchar(max)   
,@BillingProviders nvarchar(max)  
,@ScheduledProviders nvarchar(MAX)   
,@AttributeToLocations nvarchar(max)  
,@ServiceLocations nvarchar(MAX)   
,@ServiceCategories nvarchar(MAX)   
,@ServiceCodes nvarchar(MAX)   
,@ShowBillingOrganizationBreakdown bit  
,@ShowMonthlyBreakdown bit  
,@ShowBillingProviderBreakdown bit  
,@ShowScheduledProviderBreakdown bit  
,@ShowAttributeToLocationBreakdown bit  
,@ShowServiceLocationBreakdown bit  
,@ShowServiceCategoryBreakdown bit  
,@ShowServiceCodeBreakdown bit  
,@ShowPatientBreakdown bit  
)  

AS  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
IF (@StartDate > @EndDate)  
RAISERROR ('Invalid date range.',16,1)  
ELSE  
BEGIN  
SET @StartDate = CONVERT(datetime, CONVERT(nvarchar(10),@StartDate,101))  
SET @EndDate = CONVERT(datetime,CONVERT(nvarchar(10),DATEADD(ms,-2, DATEADD(dd,1,@EndDate)),101))  

Declare @StartDOS DateTime
Declare @EndDOS DateTime

set @StartDOS=DATEADD(Year,-7,GETDATE())
set @EndDOS=DATEADD(Year,+1,GETDATE())

CREATE TABLE #AuditUser(AdjustmentId int, AuditDateTime DateTime)

INSERT #AuditUser
SELECT distinct a.Id as AdjustmentId,CONVERT(DATETIME, CONVERT(NVARCHAR(10),ae.AuditDateTime,101)) AS AuditDateTime
FROM model.Adjustments a 
INNER JOIN AuditEntries ae ON ae.KeyValueNumeric = a.Id
WHERE ObjectName = 'model.Adjustments'
AND ae.ChangeTypeId = 0
Order BY AuditDateTime asc


-- sum of adjustments and payments  
-- need to be separate from charges because dates are based on different columns  

SELECT DISTINCT adj.Id AS AdjustmentId
,bs.Id AS BillingService  
,i.Id AS Invoice  
,CASE WHEN at.IsDebit = 0 AND at.IsCash = 0 THEN adj.Amount END AS PositiveAdjustments  
,CASE WHEN at.IsDebit = 1 AND at.IsCash = 0 THEN -adj.Amount END AS NegativeAdjustments  
,CASE WHEN adj.FinancialSourceTypeId = 1 AND at.IsCash = 1 THEN   
CASE WHEN at.IsDebit = 0 THEN adj.Amount ELSE - adj.Amount END END AS InsurerPayment  
,CASE WHEN (adj.FinancialSourceTypeId = 2 OR adj.FinancialSourceTypeId = 3) AND at.IsCash = 1 THEN   
CASE WHEN at.IsDebit = 0 THEN adj.Amount ELSE - adj.Amount END END AS PatientPayment  
,NULL AS Qty  
,NULL AS Charges  
,es.Id AS EncounterService  
,ar.AuditDateTime AS Date  
,COALESCE(adj.PatientId,e.PatientId) AS PatientId  
INTO #CPA  
FROM model.Adjustments adj  
INNER JOIN #AuditUser ar on ar.AdjustmentId=adj.Id
INNER JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId   
LEFT JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId  
LEFT JOIN model.Invoices i ON i.Id = ir.InvoiceId  
LEFT JOIN model.Encounters e ON e.Id = i.EncounterId  
LEFT JOIN model.BillingServices bs ON bs.Id = adj.BillingServiceId  
LEFT JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId  
WHERE (adj.PatientId IS NOT NULL OR adj.BillingServiceId IS NOT NULL OR adj.InvoiceReceivableId IS NOT NULL)  
AND (( ar.AuditDateTime BETWEEN @StartDate AND @EndDate)  OR (1 = ( CASE WHEN @StartDate IS NULL OR @EndDate IS NULL THEN 1 ELSE 0  END)))  

UNION ALL 


SELECT DISTINCT NULL AS AdjustmentId
,bs.Id AS BillingService
,i.Id AS Invoice
,NULL AS PositiveAdjustments
,NULL AS NegativeAdjustments
,NULL AS InsurerPayment
,NULL AS PatientPayment
,bs.Unit AS Qty
,CAST(bs.Unit * bs.UnitCharge AS DECIMAL(20,2)) AS Charges
,es.Id AS EncounterService
,CONVERT(datetime,CONVERT(nvarchar(10),e.StartDateTime,101)) AS Date
,e.PatientId AS PatientId
FROM model.BillingServices bs
INNER JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
INNER JOIN model.Invoices i ON i.Id = bs.InvoiceId
INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
INNER JOIN model.Encounters e ON e.Id = i.EncounterId
WHERE (( e.StartDateTime >= @StartDOS) AND  e.EndDateTime <= @EndDOS)  OR (1 = (CASE WHEN @StartDOS IS NULL OR @EndDOS IS NULL THEN 1 ELSE 0  END))

SELECT DISTINCT  
cpa.AdjustmentId  
,cpa.BillingService  
,cpa.Invoice  
,cpa.Date  
,bo.ShortName AS BillingOrganization  
,CAST(CAST(DATEPART(YEAR,cpa.Date) AS NVARCHAR(4)) + RIGHT('0' + CAST(DATEPART(MONTH,cpa.Date) AS NVARCHAR(2)),2) AS INT) AS MonthYearInt  
,DATENAME(MONTH,cpa.Date) + ' ' + CAST(DATEPART(YEAR,cpa.Date) AS NVARCHAR(4)) AS MonthYear  
,bp.UserName AS BillingProvider  
,COALESCE(sp.UserName,'No scheduled provider') AS ScheduledProvider  
,COALESCE(asl.ShortName,'On Account') AS AttributedServiceLocation  
,COALESCE(sl.ShortName,'On Account') AS ServiceLocation  
,COALESCE(est.Name,'Uncategorized') AS ServiceCategory  
,COALESCE(es.Code + ' - ' + es.[Description],'No billing service') AS ServiceCode  
,p.LastName + ', ' + p.FirstName AS PatientName  
,cpa.PatientId  
,COALESCE(cpa.Qty,0) AS Qty  
,COALESCE(cpa.Charges,0) AS Charges  
,COALESCE(cpa.PositiveAdjustments,0) AS PositiveAdjustments  
,COALESCE(cpa.NegativeAdjustments,0) AS NegativeAdjustments  
,COALESCE(cpa.InsurerPayment,0) AS InsurerPayment  
,COALESCE(cpa.PatientPayment,0) AS PatientPayment  
INTO #Results  
FROM #CPA cpa  
LEFT JOIN model.EncounterServices es ON cpa.EncounterService = es.Id  
LEFT JOIN model.Invoices i ON cpa.Invoice = i.Id  
LEFT JOIN model.ServiceLocations asl ON asl.Id = i.AttributeToServiceLocationId  
LEFT JOIN model.Encounters e ON i.EncounterId = e.Id  
LEFT JOIN model.Patients p ON cpa.PatientId = p.Id  
LEFT JOIN model.ServiceLocations sl ON e.ServiceLocationId = sl.Id  
LEFT JOIN model.Appointments a ON a.EncounterId = e.Id  
LEFT JOIN model.Providers pr ON pr.Id = i.BillingProviderId  
LEFT JOIN model.Users bp ON bp.Id = pr.UserId  
LEFT JOIN model.Users sp ON sp.Id = a.UserId  
LEFT JOIN model.BillingOrganizations bo ON bo.Id = p.BillingOrganizationId  
LEFT JOIN model.EncounterServiceTypes est ON es.EncounterServiceTypeId = est.Id  
WHERE (cpa.Date BETWEEN @StartDate AND @EndDate) AND  
((@BillingOrganizations IS NULL AND (bo.Id IS NULL OR bo.Id IN (SELECT Id FROM model.BillingOrganizations))) OR  
(@BillingOrganizations IS NOT NULL AND bo.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingOrganizations,',')))) AND  
((@BillingProviders IS NULL AND (bp.Id IS NULL OR bp.Id IN (SELECT Id FROM model.Users))) OR  
(@BillingProviders IS NOT NULL AND bp.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingProviders,',')))) AND  
((@ScheduledProviders IS NULL AND (sp.Id IS NULL OR sp.Id IN (SELECT Id FROM model.Users))) OR  
(@ScheduledProviders IS NOT NULL AND sp.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ScheduledProviders,',')))) AND  
((@AttributeToLocations IS NULL AND (asl.Id IS NULL OR asl.Id IN (SELECT Id FROM model.ServiceLocations))) OR  
(@AttributeToLocations IS NOT NULL AND asl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@AttributeToLocations,',')))) AND  
((@ServiceLocations IS NULL AND (sl.Id IS NULL OR sl.Id IN (SELECT Id FROM model.ServiceLocations))) OR  
(@ServiceLocations IS NOT NULL AND sl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceLocations,',')))) AND  
((@ServiceCategories IS NULL AND (est.Id IS NULL OR est.Id IN (SELECT Id FROM model.EncounterServiceTypes))) OR  
(@ServiceCategories IS NOT NULL AND est.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceCategories,',')))) AND  
((@ServiceCodes IS NULL AND (es.Code IS NULL OR es.Code IN (SELECT Code FROM model.EncounterServices))) OR  
(@ServiceCodes IS NOT NULL AND es.Code IN (SELECT CONVERT(nvarchar,nstr) FROM CharTable(@ServiceCodes,','))))   


SELECT 
CASE WHEN @ShowBillingOrganizationBreakdown = 1 THEN r.BillingOrganization ELSE NULL END AS BillingOrganization  
,CASE WHEN @ShowMonthlyBreakdown = 1 THEN r.MonthYear ELSE NULL END AS MonthYear  
,CASE WHEN @ShowMonthlyBreakdown = 1 THEN r.MonthYearInt ELSE NULL END AS MonthYearInt  
,CASE WHEN @ShowBillingProviderBreakdown = 1 THEN r.BillingProvider ELSE NULL END AS BillingProvider  
,CASE WHEN @ShowScheduledProviderBreakdown = 1 THEN r.ScheduledProvider ELSE NULL END AS ScheduledProvider  
,CASE WHEN @ShowAttributeToLocationBreakdown = 1 THEN r.AttributedServiceLocation ELSE NULL END AS AttributedServiceLocation  
,CASE WHEN @ShowServiceLocationBreakdown = 1 THEN r.ServiceLocation ELSE NULL END AS ServiceLocation  
,CASE WHEN @ShowServiceCategoryBreakdown = 1 THEN r.ServiceCategory ELSE NULL END AS ServiceCategory  
,CASE WHEN @ShowServiceCodeBreakdown = 1 THEN r.ServiceCode ELSE NULL END AS ServiceCode  
,CASE WHEN @ShowPatientBreakdown = 1 THEN r.PatientName ELSE NULL END AS PatientName  
,CASE WHEN @ShowPatientBreakdown = 1 THEN r.PatientId ELSE NULL END AS PatientId  
,BillingService  
,SUM(r.Qty) AS Qty  
,SUM(r.Charges) AS Charges  
,SUM(r.PositiveAdjustments) AS PositiveAdjustments  
,SUM(r.NegativeAdjustments) AS NegativeAdjustments  
,SUM(r.PositiveAdjustments) + SUM(r.NegativeAdjustments) AS TotalAdjustments  
,SUM(r.InsurerPayment) AS InsurerPayment  
,SUM(r.PatientPayment) AS PatientPayment  
,SUM(r.InsurerPayment) + SUM(r.PatientPayment) AS TotalPayments  
FROM #Results r  
GROUP BY  
CASE WHEN @ShowBillingOrganizationBreakdown = 1 THEN r.BillingOrganization ELSE NULL END  
,CASE WHEN @ShowMonthlyBreakdown = 1 THEN r.MonthYear ELSE NULL END  
,CASE WHEN @ShowMonthlyBreakdown = 1 THEN r.MonthYearInt ELSE NULL END   
,CASE WHEN @ShowBillingProviderBreakdown = 1 THEN r.BillingProvider ELSE NULL END  
,CASE WHEN @ShowScheduledProviderBreakdown = 1 THEN r.ScheduledProvider ELSE NULL END  
,CASE WHEN @ShowAttributeToLocationBreakdown = 1 THEN r.AttributedServiceLocation ELSE NULL END   
,CASE WHEN @ShowServiceLocationBreakdown = 1 THEN r.ServiceLocation ELSE NULL END  
,CASE WHEN @ShowServiceCategoryBreakdown = 1 THEN r.ServiceCategory ELSE NULL END  
,CASE WHEN @ShowServiceCodeBreakdown = 1 THEN r.ServiceCode ELSE NULL END  
,CASE WHEN @ShowPatientBreakdown = 1 THEN r.PatientName ELSE NULL END   
,CASE WHEN @ShowPatientBreakdown = 1 THEN r.PatientId ELSE NULL END   
,BillingService 
 
DROP TABLE #CPA  
DROP TABLE #Results
DROP TABLE #AuditUser  

END  
END 





