

/****** Object:  Trigger [model].[trgAfterInsert]    Script Date: 01/31/2017 7:54:10 PM ******/
----ZipCode

If Exists(Select 1 from sys.triggers Where name='trgAfterInsert')

BEGIN
DROP Trigger [model].[trgAfterInsert]
END

GO


CREATE TRIGGER [model].[trgAfterInsert] ON [model].[PatientAddresses]
FOR UPDATE,INSERT
NOT FOR REPLICATION 
AS

SET NOCOUNT ON
BEGIN

	DECLARE @AddressID INT

	SELECT @AddressID = i.Id FROM inserted i;

		UPDATE [model].[PatientAddresses] Set PostalCode=REPLACE(PostalCode, '-', '') Where Id=@AddressId
	
		UPDATE [model].[PatientAddresses] SET PostalCode = REPLACE(PostalCode,' ', '') WHERE Id = @AddressID
	
END
SET NOCOUNT OFF


GO


