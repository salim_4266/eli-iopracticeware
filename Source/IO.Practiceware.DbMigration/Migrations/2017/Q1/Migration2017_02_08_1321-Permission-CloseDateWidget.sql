

----02/08/2017-----------------------
----Script to add permission for Close Date widget---------


IF NOT Exists(Select 1 from model.Permissions where Name like '%Close Date Widget%')
BEGIN
SET IDENTITY_INSERT  model.Permissions  ON;

INSERT INTO model.Permissions(ID, Name, PermissionCategoryId) VALUES(69, 'Close Date Widget', 13)

SET IDENTITY_INSERT  model.Permissions  OFF;
END

