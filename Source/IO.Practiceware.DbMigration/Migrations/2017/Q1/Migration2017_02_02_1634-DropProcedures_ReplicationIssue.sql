

----Script for deletion of stored procedure------------

 

IF Exists (SELECT 1 FROM sys.procedures where name = 'GenerateMeaningfulUseUtilizationReport')
BEGIN
	DROP PROCEDURE [IO.Practiceware.SqlClr].[GenerateMeaningfulUseUtilizationReport]
END

IF Exists (SELECT 1 FROM sys.procedures where name = 'GetMeaningfulUseUtilizationReport')
BEGIN
	DROP PROCEDURE [IO.Practiceware.SqlClr].[GetMeaningfulUseUtilizationReport]
END

