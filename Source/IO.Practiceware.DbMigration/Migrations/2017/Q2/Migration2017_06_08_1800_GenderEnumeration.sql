If NOT EXISTS(Select 1 From sys.tables where Name = 'Gender_Enumeration') 
BEGIN
	CREATE TABLE [model].[Gender_Enumeration](
		[Id] [int] NOT NULL,
		[Name] [nvarchar](max) NOT NULL,
		[SNOMEDCT] [nvarchar](50) NULL,
		[HL7Code] [nvarchar](50) NULL,
		[IsArchived] [bit] NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
End

Go

If EXISTS(Select 1 From sys.tables where Name = 'Gender_Enumeration') 
Begin
 If NOT EXISTS(Select 1 From model.Gender_Enumeration)  
 Begin
	Insert into model.Gender_Enumeration (Id
	,Name
	,SNOMEDCT
	,HL7Code
	,IsArchived) 
	Values
	('2', 'Identifies as Female', '446141000124107', '446141000124107',0),
	('3', 'Identifies as Male', '446151000124109', '446151000124109',0),
	('4', 'Additional gender category or other, please specify', NULL, 'OTH',0),
	('5', 'Female-to-Male (FTM)/Transgender Male/Trans Man', '407377005', '407377005',0),
	('6', 'Male-to-Female (MTF)/Transgender Female/Trans Woman', '407376001', '407376001',0),
	('7', 'Genderqueer, neither exclusively male nor female', '446131000124102', '446131000124102',0),
	('8', 'Choose not to disclose', NULL, 'ASKU',0) 
 End 
End
