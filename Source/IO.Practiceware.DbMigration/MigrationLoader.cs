﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FluentMigrator;
using FluentMigrator.Infrastructure;
using FluentMigrator.Runner;

namespace IO.Practiceware.DbMigration
{
    internal class MigrationLoader : IMigrationInformationLoader
    {
        private readonly DefaultMigrationInformationLoader _defaultMigrationInformationLoader;

        public MigrationLoader(IMigrationConventions conventions, Assembly assembly, string @namespace, IEnumerable<string> tagsToMatch)
        {
            _defaultMigrationInformationLoader = new DefaultMigrationInformationLoader(conventions, assembly, @namespace, tagsToMatch);
            Migrations = new List<IMigrationInfo>();
        }

        public MigrationLoader(IMigrationConventions conventions, Assembly assembly, string @namespace, bool loadNestedNamespaces, IEnumerable<string> tagsToMatch)
        {
            _defaultMigrationInformationLoader = new DefaultMigrationInformationLoader(conventions, assembly, @namespace, loadNestedNamespaces, tagsToMatch);
            Migrations = new List<IMigrationInfo>();
        }

        public IList<IMigrationInfo> Migrations { get; private set; }

        public SortedList<long, IMigrationInfo> LoadMigrations()
        {
            SortedList<long, IMigrationInfo> result = _defaultMigrationInformationLoader.LoadMigrations();
            Migrations.ToList().ForEach(m => result.Add(m.Version, m));
            return result;
        }
    }
}