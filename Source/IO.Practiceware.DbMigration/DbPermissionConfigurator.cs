﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;

namespace IO.Practiceware.DbMigration
{
    internal class DbPermissionConfigurator
    {
        public static void Configure(IEnumerable<string> connectionStrings)
        {
            const string networkServiceAccount = @"NT AUTHORITY\NETWORK SERVICE";
            const string localSystemAccount = @"NT AUTHORITY\SYSTEM";
            var currentMachineAccount = string.Format(@"{0}\{1}$", Networking.GetMachineNetBiosDomain(), Environment.MachineName);

            foreach (var connectionString in connectionStrings)
            {
                if (string.IsNullOrEmpty(connectionString))
                {
                    var exception = new Exception("Could not find connection strings.");
                    throw exception;
                }

                // The following will create the login user for the NetworkServiceAccount, LocalSystemAccount, and machine name if it doesnt exist.
                // It will then add the users to the "db_owner" users group if it isn't part of it.
                const string addDbOwnerRoleFormatString =
                    @"IF NOT EXISTS (SELECT name FROM master.sys.syslogins WHERE name = '{0}') BEGIN CREATE LOGIN [{0}] FROM WINDOWS END 
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = '{0}') BEGIN CREATE USER [{0}] FOR LOGIN [{0}] END 
exec sp_addrolemember 'db_owner', '{0}' ";

                string addDbOwnerRoleNetworkServiceAccountQuery = string.Format(addDbOwnerRoleFormatString, networkServiceAccount);
                string addDbOwnerRoleLocalSystemAccountQuery = string.Format(addDbOwnerRoleFormatString, localSystemAccount);
                string addDbOwnerRoleMachineAccountQuery = string.Format(addDbOwnerRoleFormatString, currentMachineAccount);

                // The following will create the login user for the LocalSystemAccount, if it doesnt exist.
                // It will then add the user as a "sysadmin" if it's not in that role.
                const string addSysAdminRoleFormatString =
                    @"IF NOT EXISTS (SELECT name FROM master.sys.syslogins WHERE name = '{0}') BEGIN CREATE LOGIN [{0}] FROM WINDOWS END 
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = '{0}') BEGIN CREATE USER [{0}] FOR LOGIN [{0}] END 
exec sp_addsrvrolemember '{0}', 'sysadmin' ";
                string addSysAdminRoleLocalSystemAccountQuery = string.Format(addSysAdminRoleFormatString, localSystemAccount);
                string addSysAdminRoleMachineAccountQuery = string.Format(addDbOwnerRoleFormatString, currentMachineAccount);

                using (var connection = new SqlConnection(connectionString))
                {
                    if (connection.DataSource != null && connection.DataSource.Split('\\')[0].Trim().Length > 15)
                    {
                        throw new Exception("Server name must be less than 16 characters");
                    }

                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.Connection = connection;
                        try
                        {
                            connection.Open();
                        }
                        catch (Exception ex)
                        {
                            var exception = new Exception(string.Format("Invalid connection string \"{0}\"", connectionString), ex);
                            throw exception;
                        }

                        var queries = new List<string>(new[] { addSysAdminRoleLocalSystemAccountQuery, addDbOwnerRoleLocalSystemAccountQuery, addDbOwnerRoleNetworkServiceAccountQuery });
                        if (Networking.IsInDomain())
                        {
                            queries.AddRange(new[] { addDbOwnerRoleMachineAccountQuery, addSysAdminRoleMachineAccountQuery });
                        }

                        foreach (string query in queries)
                        {
                            command.CommandText = query;
                            try
                            {
                                command.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                var exception = new Exception(string.Format("Could not execute query \"{0}\"", query), ex);
                                Trace.TraceWarning(exception.ToString());
                            }
                        }
                    }
                }
            }
        }

    }
}