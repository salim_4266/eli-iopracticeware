﻿-- DiagnosticTestOrder
SELECT
pct.Id AS Id
,at.AppTypeId AS AppointmentTypeId
,COALESCE(cp1.Id,cp2.Id) AS ClinicalProcedureId
,CONVERT(BIT,0) AS IsExcludedFromCommunications
,CASE 
	WHEN pct.FollowUp = 'T'
		THEN CONVERT(BIT,1)
	ELSE CONVERT(BIT,0)
END AS IsFollowUpRequired
,pct.Code AS Name
,pct.[Rank] AS OrdinalId	
,CASE 
	WHEN dbo.IsNullOrEmpty(pct.OtherLtrTrans) = 0
		THEN pct.OtherLtrTrans
END AS SimpleDescription
,CASE 
	WHEN dbo.IsNullOrEmpty(pct.LetterTranslation) = 0
		THEN pct.LetterTranslation
END AS TechnicalDescription
FROM dbo.PracticeCodeTable pct
LEFT JOIN df.Questions q ON (q.QuestionOrder = pct.TestOrder OR q.Question = pct.Code)	
LEFT JOIN model.ClinicalProcedures cp1 ON pct.Code = cp1.Name
LEFT JOIN model.ClinicalProcedures cp2 ON q.Question = cp2.Name
LEFT JOIN dbo.AppointmentType at ON at.AppointmentType = LTRIM(RTRIM(pct.AlternateCode))
	WHERE ReferenceType LIKE '%EXTERNAL TESTS%'
		AND pct.TestOrder IS NOT NULL 
		AND pct.TestOrder <> ''
		AND q.QuestionParty IN ('T','Y')
GROUP BY pct.Id
,at.AppTypeId
,COALESCE(cp1.Id,cp2.Id)
,CASE 
	WHEN pct.FollowUp = 'T'
		THEN CONVERT(BIT,1)
	ELSE CONVERT(BIT,0)
END
,pct.Code
,pct.[Rank]
,CASE 
	WHEN dbo.IsNullOrEmpty(pct.OtherLtrTrans) = 0
		THEN pct.OtherLtrTrans
END
,CASE 
	WHEN dbo.IsNullOrEmpty(pct.LetterTranslation) = 0
		THEN pct.LetterTranslation
END

GO

-- EncounterDiagnosticTestOrder
-- novalidation
SELECT 
v.ClinicalId AS Id
,v.AppointmentId
,NULL AS BodyPartId
,CASE 
	WHEN dbo.IsNullOrEmpty([IO.Practiceware.SqlClr].[SearchRegexPattern](v.FindingDetail,'(?<=\<)(?!\^)(.*?)(?=>)')) = 0
		THEN [IO.Practiceware.SqlClr].[SearchRegexPattern](v.FindingDetail,'(?<=\<)(?!\^)(.*?)(?=>)')
END AS Comment
,v.DiagnosticTestOrderId
,ap.EncounterId
,CASE [IO.Practiceware.SqlClr].[SearchRegexPattern](v.FindingDetail,'(?<=\-1\/)(OD)(?=\s)|(?<=\-1\/)(OS)(?=\s)|(?<=\-1\/)(OU)(?=\s)')
	WHEN 'OD'
		THEN 1
	WHEN 'OS'
		THEN 2
	WHEN 'OU'
		THEN 3
	ELSE NULL
END AS LateralityId
,NULL AS PatientDiagnosticTestPerformedId
,rtt.Id AS RelativeTimeTypeId
,CASE [IO.Practiceware.SqlClr].[SearchRegexPattern](v.FindingDetail,'(?<=\-1\/\d*\s)((WEEKS)|(MONTHS)|(YEARS)|(MINUTES)|(HOURS)|(DAYS)?)')
	WHEN 'MINUTES'
		THEN 1
	WHEN 'HOURS'
		THEN 2
	WHEN 'DAYS'
		THEN 3
	WHEN 'WEEKS'
		THEN 4
	WHEN 'MONTHS'
		THEN 5
	WHEN 'YEARS'
		THEN 6
END AS TimeFrameId
,NULL AS TimeQualifierId
,CONVERT(INT,CASE WHEN dbo.IsNullOrEmpty([IO.Practiceware.SqlClr].[SearchRegexPattern](v.FindingDetail,'(?<=\-1\/)(\d*\s?)(?=(\sWEEKS)|(\sMONTHS)|(\sYEARS)|(\sMINUTES)|(\sHOURS)|(\sDAYS))')) = 0
	THEN [IO.Practiceware.SqlClr].[SearchRegexPattern](v.FindingDetail,'(?<=\-1\/)(\d*\s?)(?=(\sWEEKS)|(\sMONTHS)|(\sYEARS)|(\sMINUTES)|(\sHOURS)|(\sDAYS))')
END) AS TimeUnit
FROM
(
	SELECT
	pc.ClinicalId
	,pc.AppointmentId
	,pct.Id AS DiagnosticTestOrderId
	,SUBSTRING(
	pc.FindingDetail
	,CHARINDEX([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-[0|1|T|B]\/))(.*?)(?=\-[1|T]/)'),pc.FindingDetail)+LEN([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-[0|1|T|B]\/))(.*?)(?=\-[1|T]/)'))
	,LEN(pc.FindingDetail)-CHARINDEX([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-[0|1|T|B]\/))(.*?)(?=\-[1|T]/)'),pc.FindingDetail)
	) AS FindingDetail
	FROM dbo.PatientClinical pc
	INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType LIKE '%EXTERNAL TESTS%'
		AND pct.Code = [IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-[0|1|T|B]\/))(.*?)(?=\-[1|T]/)')
		AND pct.TestOrder IS NOT NULL  AND pct.TestOrder <> ''
	WHERE dbo.IsNullOrEmpty([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-[0|1|T|B]\/))(.*?)(?=\-[1|T]/)')) = 0
	AND SUBSTRING(
	[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-0\/\-1\/))(.*?)(?=\-1/)')
	,0
	,CHARINDEX('-',[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-0\/\-1\/))(.*?)(?=\-1/)'))
	) NOT IN (
	SELECT 
		SUBSTRING(Code,1,CHARINDEX(':',Code)-1) AS TestType
		FROM dbo.PracticeCodeTable 
		WHERE ReferenceType = 'EXTERNALTESTLINKS'
			AND SUBSTRING(Code,1,CHARINDEX(':',Code)-1) <> 'Radiology' AND CHARINDEX(':',Code) > 0
		GROUP BY SUBSTRING(Code,1,CHARINDEX(':',Code)-1)
	)
	AND pc.ClinicalType = 'A'
	AND pc.[Status] = 'A'
	AND SUBSTRING(pc.FindingDetail, 1, 13) = 'ORDER A TEST-'

	UNION ALL

	SELECT
	pc.ClinicalId
	,pc.AppointmentId
	,pct.Id AS DiagnosticTestOrderId
	,SUBSTRING(
	pc.FindingDetail
	,CHARINDEX([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-0\/\-1\/))(.*?)(?=\-1/)'),pc.FindingDetail)+LEN([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-0\/\-1\/))(.*?)(?=\-1/)'))
	,LEN(pc.FindingDetail)-CHARINDEX([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-0\/\-1\/))(.*?)(?=\-1/)'),pc.FindingDetail)
	) AS FindingDetail
	FROM dbo.PatientClinical pc 
	INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType LIKE '%EXTERNAL TESTS%'
		AND pct.Code = [IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-0\/\-1\/))(.*?)(?=\-1/)')
		AND pct.TestOrder IS NOT NULL  AND pct.TestOrder <> ''
	WHERE dbo.IsNullOrEmpty([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-0\/\-1\/))(.*?)(?=\-1/)')) = 0
	AND SUBSTRING(
	[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-0\/\-1\/))(.*?)(?=\-1/)')
	,0
	,CHARINDEX('-',[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=(^ORDER\sA\sTEST\-0\/\-1\/))(.*?)(?=\-1/)'))
	) NOT IN (
	SELECT 
		SUBSTRING(Code,1,CHARINDEX(':',Code)-1) AS TestType
		FROM dbo.PracticeCodeTable 
		WHERE ReferenceType = 'EXTERNALTESTLINKS'
			AND SUBSTRING(Code,1,CHARINDEX(':',Code)-1) <> 'Radiology' AND CHARINDEX(':',Code) > 0
		GROUP BY SUBSTRING(Code,1,CHARINDEX(':',Code)-1)
	)
	AND pc.ClinicalType = 'A'
	AND pc.[Status] = 'A'
	AND SUBSTRING(pc.FindingDetail, 1, 13) = 'ORDER A TEST-'
)v
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = v.AppointmentId
LEFT JOIN model.RelativeTimeTypes rtt ON rtt.Name = [IO.Practiceware.SqlClr].[SearchRegexPattern](v.FindingDetail,'(?<=\-1\/(OS\s|OD\s|OU\s|\-\s))(ASAP\sOUT\sOF\sOFFICE|AS\sSOON\sAS\sCONVENIENT|AT\sA\sCONVENIENT\sTIME\s|NEXT\sAVAILABLE\sDATE|NEXTVISIT|STAT|PRN|THIS\sVISIT|ONGOING|)')

GO


-- EncounterAdministeredMedication
SELECT pc.ClinicalId AS Id
	,pc.AppointmentId AS EncounterId
	,CONVERT(int, NULL) AS PatientDiagnosticTestPerformedId
	,CONVERT(int, NULL) AS PatientProcedurePerformedId
	,CONVERT(int, NULL) AS PatientVaccinationId
	,pc.ClinicalId AS PatientMedicationId
FROM dbo.PatientClinical pc 
WHERE pc.ClinicalType = 'A'
	AND pc.FindingDetail LIKE 'RX%(STAT)%'
	AND pc.Status = 'A'

GO

-- EncounterReasonForVisitComment
SELECT pn.NoteId AS Id
	,pn.AppointmentId AS EncounterId
	,pn.Note1 AS Value
	,NULL AS LateralityId
	,NULL AS BodyPartId
	,ap.ResourceId1 AS UserId
FROM dbo.PatientNotes pn
INNER JOIN dbo.Appointments ap ON pn.AppointmentId = ap.AppointmentId
WHERE pn.NoteSystem = '10'
	AND pn.NoteType = 'C'

GO

-- EncounterReasonForVisit
SELECT ClinicalId AS Id
	,CONVERT(datetime, NULL) AS StartDateTime
	,CONVERT(datetime, NULL) AS EndDateTime
	,NULL AS TimingId
	,NULL AS BodySystemId
	,pc.AppointmentId AS EncounterId
	,1 AS OrdinalId
	,pct.Id AS ReasonForVisitId
	,CASE 
		WHEN pc.EyeContext = 'OD'
			THEN 1
		WHEN pc.EyeContext = 'OS'
			THEN 2
		WHEN pc.EyeContext = 'OU'
			THEN 3
		ELSE NULL END AS LateralityId
	,ap.ResourceId1 AS UserId
	FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON pc.AppointmentId = ap.AppointmentId
INNER JOIN dbo.PracticeCodeTable pct ON pc.FindingDetail = ('CC:' + pct.Code)
WHERE pc.Symptom = '/CHIEFCOMPLAINTS'
	AND pct.ReferenceType IN ('CHIEFCOMPLAINTS', 'CHIEFCOMPLAINTSARCHIVED')
	AND pc.Status = 'A'

GO

-- EncounterPatientEducation
SELECT pc.ClinicalId AS Id
	,pc.AppointmentId AS EncounterId
	,CONVERT(nvarchar(max),NULL) AS Comment
	,pct.Id AS PatientEducationId
FROM dbo.PatientClinical pc
INNER JOIN dbo.PracticeCodeTable pct ON pct.Code = SUBSTRING(
pc.FindingDetail
,CHARINDEX([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'WRITTEN\sINSTRUCTIONS-[0|A|1]/'),pc.FindingDetail)
	+LEN([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'WRITTEN\sINSTRUCTIONS-[0|A|1]/'))
,CHARINDEX('-F/',
SUBSTRING(
pc.FindingDetail
,CHARINDEX([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'WRITTEN\sINSTRUCTIONS-[0|A|1]/'),pc.FindingDetail)
	+LEN([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'WRITTEN\sINSTRUCTIONS-[0|A|1]/'))
,LEN(pc.FindingDetail)
))-1
)
	AND pct.ReferenceType = 'PATIENTADMIN'
WHERE pc.ClinicalType = 'A'
	AND SUBSTRING(pc.FindingDetail, 1, 21) = 'WRITTEN INSTRUCTIONS-'
	AND LEN(pc.FindingDetail) > 23 --We need this clause to prevent invalid length to in substring function
	AND pc.Status = 'A'

GO

-- EncounterTransitionOfCareOrder
SELECT T.Id
	,T.EncounterId
	,T.Comment
	,ec.Id AS ExternalProviderId
	,tcre.Id AS TransitionOfCareReasonId
	,t.TransitionOfCareOrderStatusId
	,T.SentDateTime
	,T.ExternalProviderCommunicationTypeId
	,T.ResponseReceivedDateTime
	,1 AS TransitionOfCareOrderId
FROM (
	SELECT model.GetBigPairId(COALESCE(ptj.TransactionId, 1), pc.ClinicalId, 11000000) AS Id --We have to use composite key because there can be more than 1 ptj row
	,COALESCE(pcRef.AppointmentId
		,(SELECT TOP 1 pac.AppointmentId 
		FROM dbo.PatientClinical pac
		JOIN dbo.Appointments apac ON apac.AppointmentId = pac.AppointmentId
		WHERE Symptom = '/REFERRAL RESPONSE RECEIVED'
			AND pac.PatientId = pc.PatientId
			AND apac.AppDate >= ap.AppDate)
		,pc.AppointmentId) AS EncounterId
	,pc.FindingDetail
	,CASE
		--When there is a comment
		WHEN pc.ClinicalType = 'A'
			AND pc.Status = 'A'
			AND (SUBSTRING(pc.FindingDetail, 1, 25) = 'REFER PATIENT TO ANOTHER'
				OR pc.FindingDetail LIKE 'SEND A CONSULTATION LETTER%REFER%')
			AND pc.FindingDetail LIKE '%<%>%'
			AND pc.FindingDetail NOT LIKE '%<^%>%'
		THEN
			CONVERT(NVARCHAR(MAX), SUBSTRING(pc.FindingDetail, CHARINDEX('<', pc.FindingDetail)+1, CHARINDEX('>', pc.FindingDetail)-CHARINDEX('<', pc.FindingDetail)-1))
		--When there is a reason
		WHEN pc.ClinicalType = 'A'
			AND pc.Status = 'A'
			AND (SUBSTRING(pc.FindingDetail, 1, 25) = 'REFER PATIENT TO ANOTHER'
				OR pc.FindingDetail LIKE 'SEND A CONSULTATION LETTER%REFER%')
			AND pc.FindingDetail LIKE '%<^%>%'
			AND pc.FindingDetail NOT LIKE '%<%>%<^%>%'
		THEN
			CONVERT(NVARCHAR(MAX), SUBSTRING(pc.FindingDetail, CHARINDEX('<^', pc.FindingDetail)+2, CHARINDEX('>', pc.FindingDetail)-CHARINDEX('<^', pc.FindingDetail)-2))
		--When there is a comment and a reason
		WHEN pc.ClinicalType = 'A'
			AND pc.Status = 'A'
			AND (SUBSTRING(pc.FindingDetail, 1, 25) = 'REFER PATIENT TO ANOTHER'
				OR pc.FindingDetail LIKE 'SEND A CONSULTATION LETTER%REFER%')
			AND pc.FindingDetail LIKE '%<%> <^%>%'
		THEN
			CONVERT(NVARCHAR(MAX), SUBSTRING(pc.FindingDetail, CHARINDEX('<', pc.FindingDetail)+1, CHARINDEX('>', pc.FindingDetail)-CHARINDEX('<', pc.FindingDetail)-1)  + ' ' + 
			SUBSTRING(pc.FindingDetail, CHARINDEX('<^', pc.FindingDetail)+2, CHARINDEX('>', pc.FindingDetail, CHARINDEX('<^', pc.FindingDetail))-CHARINDEX('<^', pc.FindingDetail)-2))
		ELSE 
			CONVERT(NVARCHAR(MAX), NULL)
		END AS Comment
	,CASE 
		WHEN ISNUMERIC(dbo.ExtractTextValue('(', pc.FindingDetail, ')')) = 1
			THEN CONVERT(INT, dbo.ExtractTextValue('(', pc.FindingDetail, ')'))
		ELSE NULL
		END AS ExternalProviderId
	,CASE 
		WHEN (TransactionBatch = '' OR TransactionBatch IS NULL)
			THEN 5
		WHEN SUBSTRING(TransactionBatch, 1, 1) = 'N'
			THEN 1
		WHEN SUBSTRING(TransactionBatch, 1, 1) = 'D'
			THEN 2
		WHEN SUBSTRING(TransactionBatch, 1, 1) = 'S'
			THEN 3
		ELSE NULL
		END AS TransitionOfCareOrderStatusId
	,CONVERT(DATETIME, SUBSTRING(PTJ.TransactionDate, 0, 5) + '/' + SUBSTRING(PTJ.TransactionDate, 5, 2) + '/' + SUBSTRING(PTJ.TransactionDate, 7, 2)) AS SentDateTime
	,3 AS ExternalProviderCommunicationTypeId
	,CASE 
		WHEN ISDATE(dbo.ExtractTextValue('<',pcRef.FindingDetail,'>')) = 1
			THEN DATEADD(hh,23,CONVERT(DATETIME,dbo.ExtractTextValue('<',pcRef.FindingDetail,'>')))
		WHEN dbo.IsDate101(SUBSTRING(dbo.ExtractTextValue('<',pcRef.FindingDetail,'>'),0,CHARINDEX(':',dbo.ExtractTextValue('<',pcRef.FindingDetail,'>')))) = 1
			THEN DATEADD(mi,DATEPART(mi,CONVERT(DATETIME,(SUBSTRING(dbo.ExtractTextValue('<',pcRef.FindingDetail,'>'),CHARINDEX(':',dbo.ExtractTextValue('<',pcRef.FindingDetail,'>'))+1,LEN(dbo.ExtractTextValue('<',pcRef.FindingDetail,'>')))))),DATEADD(hh,DATEPART(hh,CONVERT(DATETIME,(SUBSTRING(dbo.ExtractTextValue('<',pcRef.FindingDetail,'>'),CHARINDEX(':',dbo.ExtractTextValue('<',pcRef.FindingDetail,'>'))+1,LEN(dbo.ExtractTextValue('<',pcRef.FindingDetail,'>')))))),CONVERT(DATETIME,SUBSTRING(dbo.ExtractTextValue('<',pcRef.FindingDetail,'>'),0,CHARINDEX(':',dbo.ExtractTextValue('<',pcRef.FindingDetail,'>'))))))
	END AS ResponseReceivedDateTime
	FROM dbo.PatientClinical pc
	JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
	LEFT JOIN dbo.PatientClinical pcRef ON pcRef.AppointmentId = 
			(SELECT TOP 1 pac.AppointmentId 
			FROM dbo.PatientClinical pac
			JOIN dbo.Appointments apac ON apac.AppointmentId = pac.AppointmentId
			WHERE pac.Symptom = '/REFERRAL RESPONSE RECEIVED' AND pac.PatientId = pc.PatientId AND apac.AppDate >= ap.AppDate)
			AND pcRef.ClinicalType = 'F'			
			AND SUBSTRING(pcRef.FindingDetail,0,36) = '*PERFORMED-REFERRALRESPONSERECEIVED'
			AND pcRef.[Status] = 'A'
	LEFT JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pc.AppointmentId
		AND ptj.TransactionType IN ('S','F')
	WHERE pc.ClinicalType = 'A'
		AND (SUBSTRING(pc.FindingDetail, 1, 25) = 'REFER PATIENT TO ANOTHER' OR pc.FindingDetail LIKE 'SEND A CONSULTATION LETTER%REFER%')
		AND pc.[Status] = 'A'
	) AS T
LEFT JOIN model.TransitionOfCareReasons tcre ON dbo.ExtractTextValue('<',T.FindingDetail,'>') = tcre.Value
LEFT JOIN model.ExternalContacts ec ON t.ExternalProviderId = ec.Id


GO

-- EncounterTreatmentGoalAndInstruction
SELECT pn.NoteId AS Id
,pn.Note1+pn.Note2+pn.Note3+pn.Note4 AS Value
,pn.AppointmentId AS EncounterId
,MIN(pnMaster.NoteId) AS TreatmentGoalAndInstructionId
FROM dbo.PatientNotes pn
INNER JOIN dbo.PatientNotes pnMaster ON pnMaster.Note1 + pnMaster.Note2 + pnMaster.Note3 + pnMaster.Note4 = pn.Note1+pn.Note2+pn.Note3+pn.Note4
   AND pnMaster.NoteType='C'
   AND SUBSTRING(pnmaster.NoteSystem,1,5)='IMPR-'
WHERE pn.NoteType='C'
   AND SUBSTRING(pn.NoteSystem,1,5)='IMPR-'
   AND LEN(pn.Note1)>1 
GROUP BY pn.NoteId,pn.Note1,pn.Note2,pn.Note3,pn.Note4,pn.NoteSystem,pn.AppointmentId


GO

-- LaboratoryNormalRange
SELECT
ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS Id
,obd.RefRange AS Name
FROM dbo.HL7_Observation_Details obd
GROUP BY obd.RefRange

GO

-- LaboratoryTest
SELECT 
MAX(pct.Id) AS Id
,CONVERT(BIT, 0) AS IsDeactivated
,SUBSTRING(pct.Code, CHARINDEX(':', pct.Code)+1, LEN(pct.Code)) AS Name
,0 AS OrdinalId
FROM PracticeCodeTable pct
WHERE pct.ReferenceType = 'ExternalTestLinks'
GROUP BY SUBSTRING(pct.Code, CHARINDEX(':', pct.Code)+1, LEN(pct.Code)) 

GO

-- Medication
SELECT d.DrugId + 10000000 AS Id
,d.DisplayName AS Name
,CONVERT(bit, 0) AS IsFollowUpRequired
,CONVERT(bit, 0) AS IsExcludedFromCommunications
,NULL AS DrugDosageFormId
,NULL AS DrugDosageActionId
FROM dbo.drug d
 
UNION ALL

SELECT f.MEDID AS Id
,f.MED_MEDID_DESC AS Name
	,CONVERT(bit, 0) AS IsFollowUpRequired
	,CONVERT(bit, 0) AS IsExcludedFromCommunications
	,NULL AS DrugDosageFormId
,NULL AS DrugDosageActionId
FROM fdb.tblCompositeDrug f

UNION ALL 

SELECT d.DrugId + 20000000 AS Id
,d.DisplayName AS Name
,CONVERT(bit, 0) AS IsFollowUpRequired
,CONVERT(bit, 0) AS IsExcludedFromCommunications
,NULL AS DrugDosageFormId
,NULL AS DrugDosageActionId
FROM dbo.drugArchive d

GO

-- PatientAllergen
SELECT 
MAX(Id) AS Id
,e.PatientId
,CONVERT(INT,COALESCE((SELECT CASE WHEN (SELECT p.LegacyClinicalDataSourceTypeId FROM PatientClinical p WHERE p.ClinicalId = MAX(Id)) IN (1,2,3) THEN (SELECT p.LegacyClinicalDataSourceTypeId FROM PatientClinical p WHERE p.ClinicalId = MAX(Id)) END),2)) AS ClinicalDataSourceTypeId
,e.AllergenId 
FROM (
	SELECT
	MAX(pc.ClinicalId) AS Id
	,pc.PatientId
	,MIN(ar.Id) AS AllergenId
	FROM dbo.PatientClinical pc
	INNER JOIN model.Allergens ar ON ar.Name = CASE WHEN SUBSTRING(pc.FindingDetail,1,1) = '*' THEN SUBSTRING(FindingDetail,2,CHARINDEX('=',FindingDetail) -2 ) ELSE SUBSTRING(FindingDetail,0,CHARINDEX('=',FindingDetail)) END
	WHERE Symptom LIKE '%/ALLERGIES ALL%' 
		AND pc.ClinicalType IN ('C', 'H')
		AND pc.[Status] <> 'D'
	GROUP BY pc.PatientId
	,pc.FindingDetail

	UNION ALL

	SELECT
	MAX(ClinicalId) AS Id
	,pc.PatientId 
	,MIN(ar.Id) AS AllergenId
	FROM dbo.PatientClinical pc
	INNER JOIN model.Allergens ar ON ar.Name = CASE WHEN SUBSTRING(pc.FindingDetail,1,1) = '*' THEN SUBSTRING(FindingDetail,2,CHARINDEX('=',FindingDetail) -2 ) ELSE SUBSTRING(FindingDetail,0,CHARINDEX('=',FindingDetail)) END
	WHERE Symptom LIKE '%/ALLERGIES' 
		AND pc.ClinicalType IN ('C', 'H')
		AND pc.[Status] <> 'D'	
	GROUP BY pc.PatientId
	,pc.FindingDetail

	UNION ALL

	SELECT 
	MAX(ClinicalId) AS Id
	,pc.PatientId
	,MAX(ar.Id) AS AllergenId
	FROM dbo.PatientClinical pc 
	INNER JOIN model.Allergens ar ON ar.Name = CASE WHEN SUBSTRING(pc.FindingDetail,1,1) = '*' THEN SUBSTRING(FindingDetail,2,CHARINDEX('=',FindingDetail) -2 ) ELSE SUBSTRING(FindingDetail,0,CHARINDEX('=',FindingDetail)) END
	WHERE SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail)) <> 'OTHER'
		AND pc.[Status] <> 'D'
		AND Symptom = '?ALLERGY'
	GROUP BY pc.PatientId
	,CASE 
		WHEN CHARINDEX('<',pc.FindingDetail) <> 0
			THEN SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),((CHARINDEX(' <',pc.FindingDetail))-(CHARINDEX('=T',pc.FindingDetail)+LEN('=T'))))
		ELSE SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),LEN(pc.FindingDetail))
	END

	UNION ALL

	SELECT
	pc.ClinicalId AS Id
	,pc.PatientId
	,MAX(ar.Id) AS AllergenId
	FROM dbo.PatientClinical pc
	INNER JOIN (
	SELECT 
	SecondaryControlId AS Id
	,ControlName AS Name
	,v.Id AS ParentId
	,v.Name AS ParentName
	FROM df.SecondaryFormsControls s
	INNER JOIN (
		SELECT 
		SecondaryFormId AS Id
		,FormControlName AS Name
		FROM df.SecondaryForms
		WHERE FormId IN (
			SELECT FormId
			FROM df.SecondaryForms
			WHERE FormControlName LIKE 'ANTIBIOTICS'
		)
	) v ON v.Id = s.SecondaryFormId
	WHERE SecondaryFormId IN (
		SELECT SecondaryFormId
		FROM df.SecondaryForms
		WHERE FormId IN (
				SELECT FormId
				FROM df.SecondaryForms
				WHERE FormControlName LIKE 'ANTIBIOTICS'
			)
		)
		AND ControlType = 'B'
	)z ON z.ParentName = SUBSTRING(pc.FindingDetail,0,CHARINDEX('=T',pc.FindingDetail))
	INNER JOIN model.Allergens ar ON ar.Name = SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail))+'-Other'
	LEFT JOIN (
		SELECT 
		pc.PatientId
		,z.ParentId
		FROM dbo.PatientClinical pc 
		INNER JOIN model.Allergens ar ON ar.Name = SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail))
		INNER JOIN (
			SELECT 
			SecondaryControlId AS Id
			,ControlName AS Name
			,v.Id AS ParentId
			,v.Name AS ParentName
			FROM df.SecondaryFormsControls s
			INNER JOIN (
				SELECT 
				SecondaryFormId AS Id
				,FormControlName AS Name
				FROM df.SecondaryForms
				WHERE FormId IN (
					SELECT FormId
					FROM df.SecondaryForms
					WHERE FormControlName LIKE 'ANTIBIOTICS'
				)
			) v ON v.Id = s.SecondaryFormId
			WHERE SecondaryFormId IN (
				SELECT SecondaryFormId
				FROM df.SecondaryForms
				WHERE FormId IN (
						SELECT FormId
						FROM df.SecondaryForms
						WHERE FormControlName LIKE 'ANTIBIOTICS'
					)
				)
				AND ControlType = 'B'
		)z ON z.Id = CASE 
			WHEN CHARINDEX('<',pc.FindingDetail) <> 0
				THEN SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),((CHARINDEX(' <',pc.FindingDetail))-(CHARINDEX('=T',pc.FindingDetail)+LEN('=T'))))
			ELSE SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),LEN(pc.FindingDetail))
		END
		WHERE SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail)) <> 'OTHER'
			AND Symptom = '?ALLERGY'
		GROUP BY pc.PatientId
		,z.ParentId
	)y ON y.PatientId = pc.PatientId
		AND y.ParentId = z.ParentId
	WHERE pc.Symptom = '/ANY DRUG ALLERGIES' 
		AND pc.FindingDetail NOT LIKE '%NONE%'
		AND pc.ClinicalType IN ('C', 'H')
		AND pc.[Status] <> 'D'
		AND y.ParentId IS NULL
	GROUP BY pc.PatientId
	,pc.FindingDetail
	,y.ParentId
	,pc.ClinicalId

	UNION ALL

	SELECT 
	MAX(ClinicalId) AS Id
	,pc.PatientId
	,MAX(ar.Id) AS AllergenId
	FROM dbo.PatientClinical pc 
	INNER JOIN model.Allergens ar ON ar.Name = CASE
		WHEN pc.FindingDetail LIKE '%=T2093%'
			THEN 'GLAUCOMA-Other'
		WHEN pc.FindingDetail LIKE '%=T2707%'
			THEN 'ANTIBIOTICS-Other'
		WHEN pc.FindingDetail LIKE '%=T2717%'
			THEN 'PAIN-RELIEVERS-Other'
		WHEN pc.FindingDetail LIKE '%=T2729%'
			THEN 'ANTIHISTIMINES-Other'
	END
	WHERE pc.Symptom = '?ALLERGY'
		AND SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail)) = 'OTHER'
		AND pc.[Status] <> 'D'
	GROUP BY pc.PatientId
	,pc.FindingDetail
	
		UNION ALL

	SELECT 
	MAX(ClinicalId) AS Id
	, pc.PatientId
	, 15391 AS AllergenId
	FROM PatientClinical pc
	WHERE Symptom in ('/ALLERGIES', '/ANY DRUG ALLERGIES') 
	AND FindingDetail in ('NOTSURE=T98','NOTSURE=T171','NOTSURE=T171 <NOTSURE>','NONE=T14908 <NONE>')
	GROUP BY PatientId
)e 
GROUP BY 
PatientId
,AllergenId

GO

-- PatientAllergenDetail
SELECT
CASE 
	WHEN dbo.IsNullOrEmpty(v.PatientAllergenId) = 0
		THEN model.GetPairId(1,v.PatientAllergenId,11000000) 
	ELSE CONVERT(INT,NULL) 
END AS Id
,v.PatientAllergenId 
,CONVERT(DATETIME,NULL) AS StartDateTime
,CONVERT(DATETIME,NULL) AS EndDateTime
,NULL AS AllergenCategoryId
,CONVERT(BIT,0) AS IsDeactivated
,CONVERT(DATETIME,ap.Appdate) AS EnteredDateTime
,NULL AS UserId
,CONVERT(NVARCHAR(MAX),NULL) AS Comment
,NULL AS AccuracyQualifierId
,NULL AS RelevancyQualifierId
,1 AS ClinicalConditionStatusId --"active"
FROM (
	SELECT 
	MAX(Id) AS PatientAllergenId
	,PatientId
	FROM (
		SELECT
		MAX(pc.ClinicalId) AS Id
		,pc.PatientId
		,MIN(ar.Id) AS AllergenId
		FROM dbo.PatientClinical pc
		LEFT JOIN model.Allergens ar ON ar.Name = SUBSTRING(FindingDetail,0,CHARINDEX('=',FindingDetail))
		WHERE Symptom LIKE '%/ALLERGIES ALL%' 
			AND pc.ClinicalType IN ('C', 'H')
			AND pc.[Status] <> 'D'
		GROUP BY pc.PatientId
		,pc.FindingDetail

		UNION ALL

		SELECT
		MAX(ClinicalId) AS Id
		,pc.PatientId 
		,MIN(ar.Id) AS AllergenId
		FROM dbo.PatientClinical pc
		LEFT JOIN model.Allergens ar ON ar.Name = SUBSTRING(FindingDetail,0,CHARINDEX('=',FindingDetail))
		WHERE Symptom LIKE '%/ALLERGIES' 
			AND pc.ClinicalType IN ('C', 'H')
			AND pc.[Status] <> 'D'	
		GROUP BY pc.PatientId
		,pc.FindingDetail

		UNION ALL

		SELECT 
		MAX(ClinicalId) AS Id
		,pc.PatientId
		,MAX(ar.Id) AS AllergenId
		FROM dbo.PatientClinical pc 
		LEFT JOIN model.Allergens ar ON ar.Name = SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail))
		WHERE SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail)) <> 'OTHER'
			AND pc.[Status] <> 'D'
			AND Symptom = '?ALLERGY'
		GROUP BY pc.PatientId
		,CASE 
			WHEN CHARINDEX('<',pc.FindingDetail) <> 0
				THEN SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),((CHARINDEX(' <',pc.FindingDetail))-(CHARINDEX('=T',pc.FindingDetail)+LEN('=T'))))
			ELSE SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),LEN(pc.FindingDetail))
		END

		UNION ALL

		SELECT
		pc.ClinicalId AS Id
		,pc.PatientId
		,MAX(ar.Id) AS AllergenId
		FROM dbo.PatientClinical pc
		INNER JOIN (
		SELECT 
		SecondaryControlId AS Id
		,ControlName AS Name
		,v.Id AS ParentId
		,v.Name AS ParentName
		FROM df.SecondaryFormsControls s
		INNER JOIN (
			SELECT 
			SecondaryFormId AS Id
			,FormControlName AS Name
			FROM df.SecondaryForms
			WHERE FormId IN (
				SELECT FormId
				FROM df.SecondaryForms
				WHERE FormControlName LIKE 'ANTIBIOTICS'
			)
		) v ON v.Id = s.SecondaryFormId
		WHERE SecondaryFormId IN (
			SELECT SecondaryFormId
			FROM df.SecondaryForms
			WHERE FormId IN (
					SELECT FormId
					FROM df.SecondaryForms
					WHERE FormControlName LIKE 'ANTIBIOTICS'
				)
			)
			AND ControlType = 'B'
		)z ON z.ParentName = SUBSTRING(pc.FindingDetail,0,CHARINDEX('=T',pc.FindingDetail))
		INNER JOIN model.Allergens ar ON ar.Name = SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail))+'-Other'
		LEFT JOIN (
			SELECT 
			pc.PatientId
			,z.ParentId
			FROM dbo.PatientClinical pc 
			LEFT JOIN model.Allergens ar ON ar.Name = SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail))
			INNER JOIN (
				SELECT 
				SecondaryControlId AS Id
				,ControlName AS Name
				,v.Id AS ParentId
				,v.Name AS ParentName
				FROM df.SecondaryFormsControls s
				INNER JOIN (
					SELECT 
					SecondaryFormId AS Id
					,FormControlName AS Name
					FROM df.SecondaryForms
					WHERE FormId IN (
						SELECT FormId
						FROM df.SecondaryForms
						WHERE FormControlName LIKE 'ANTIBIOTICS'
					)
				) v ON v.Id = s.SecondaryFormId
				WHERE SecondaryFormId IN (
					SELECT SecondaryFormId
					FROM df.SecondaryForms
					WHERE FormId IN (
							SELECT FormId
							FROM df.SecondaryForms
							WHERE FormControlName LIKE 'ANTIBIOTICS'
						)
					)
					AND ControlType = 'B'
			)z ON z.Id = CASE 
				WHEN CHARINDEX('<',pc.FindingDetail) <> 0
					THEN SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),((CHARINDEX(' <',pc.FindingDetail))-(CHARINDEX('=T',pc.FindingDetail)+LEN('=T'))))
				ELSE SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),LEN(pc.FindingDetail))
			END
			WHERE SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail)) <> 'OTHER'
				AND Symptom = '?ALLERGY'
			GROUP BY pc.PatientId
			,z.ParentId
		)y ON y.PatientId = pc.PatientId
			AND y.ParentId = z.ParentId
		WHERE pc.Symptom = '/ANY DRUG ALLERGIES' 
			AND pc.FindingDetail NOT LIKE '%NONE%'
			AND pc.ClinicalType IN ('C', 'H')
			AND pc.[Status] <> 'D'
			AND y.ParentId IS NULL
		GROUP BY pc.PatientId
		,pc.FindingDetail
		,y.ParentId
		,pc.ClinicalId

		UNION ALL

		SELECT 
		MAX(ClinicalId) AS Id
		,pc.PatientId
		,MAX(ar.Id) AS AllergenId
		FROM dbo.PatientClinical pc 
		LEFT JOIN model.Allergens ar ON ar.Name = CASE
			WHEN pc.FindingDetail LIKE '%=T2093%'
				THEN 'GLAUCOMA-Other'
			WHEN pc.FindingDetail LIKE '%=T2707%'
				THEN 'ANTIBIOTICS-Other'
			WHEN pc.FindingDetail LIKE '%=T2717%'
				THEN 'PAIN-RELIEVERS-Other'
			WHEN pc.FindingDetail LIKE '%=T2729%'
				THEN 'ANTIHISTIMINES-Other'
		END
		WHERE pc.Symptom = '?ALLERGY'
			AND SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail)) = 'OTHER'
			AND pc.[Status] <> 'D'
		GROUP BY pc.PatientId
		,pc.FindingDetail
	)e 
	GROUP BY 
	PatientId
	,AllergenId
)v
INNER JOIN dbo.PatientClinical pc ON pc.ClinicalId = v.PatientAllergenId
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
WHERE pc.[Status] <> 'D'
GROUP BY v.PatientAllergenId
,ap.AppDate

GO

-- PatientBloodPressure
SELECT pc.ClinicalId AS Id
	,pc.PatientId AS PatientId
	,CONVERT(DATETIME, ap.AppDate, 112) AS MeasuredDateTime
	,CONVERT(INT,[IO.Practiceware.SqlClr].SearchRegexPattern(dbo.ExtractTextValue('<',pcPressure.FindingDetail,'/'),'[0-9]*')) AS SystolicPressure
	,CONVERT(INT,[IO.Practiceware.SqlClr].SearchRegexPattern(dbo.ExtractTextValue('/',pcPressure.FindingDetail,'>'),'[0-9]*')) AS DiastolicPressure
	,
	--COMMENTED OUT UNTIL WE MAKE BODY PART TABLE
	--CASE 
	--	WHEN SUBSTRING(pcPressure.FindingDetail, 1, 20) = '*BLOODPRESSURE=T1760'
	--		THEN NULL --TODO'rt arm code'
	--	WHEN SUBSTRING(pcPressure.FindingDetail, 1, 25) = '*BLOODPRESSURELEFT=T11499'
	--		THEN NULL --TODO'left arm code'
	--	WHEN SUBSTRING(pcPressure.FindingDetail, 1, 28) = '*BLOODPRESSURERTWRIST=T17741'
	--		THEN NULL --TODO'rt wrist code'
	--	WHEN SUBSTRING(pcPressure.FindingDetail, 1, 30) = '*BLOODPRESSURELEFTWRIST=T17742'
	--		THEN NULL --TODO'left wrist code'
	--	ELSE NULL END
	NULL AS BodyPartId
	,CASE 
		WHEN pcPatReport.ClinicalId IS NOT NULL
			THEN 2
		ELSE 1
		END AS ClinicalDataSourceTypeId
	,CONVERT(NVARCHAR(MAX), pn.Note1) AS Comment
	,CONVERT(DATETIME, ap.AppDate, 112) AS EnteredDateTime
	,CONVERT(BIT,CASE 
		WHEN pc.STATUS = 'A'
			THEN 0
		ELSE 1
		END) AS IsDeactivated
	,NULL AS AccuracyQualifierId
	,NULL AS RelevancyQualifierId
	,CASE 
		WHEN SUBSTRING(pcSitting.FindingDetail, 1, 8) = '*SITTING'
			THEN 1--TODO'THE CODE FOR SITTING'
		WHEN SUBSTRING(pcStanding.FindingDetail, 1, 9) = '*STANDING'
			THEN 2--TODO 'THE CODE FOR STANDING'
		ELSE NULL
		END AS ObservationTypeId,
	re.ResourceId AS UserId,
	pc.AppointmentId AS EncounterId
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
LEFT JOIN dbo.Resources re ON re.ResourceName = SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail)+1, CHARINDEX(')', pc.FindingDetail)-CHARINDEX('(', pc.FindingDetail)-1)
--there could be three rows here; we want just the first one 
INNER JOIN dbo.PatientClinical pcPressure ON pcPressure.AppointmentId = pc.AppointmentId
	AND pcPressure.ClinicalType = 'F'
	AND pcPressure.Symptom = '/BLOOD PRESSURE'
	AND pcPressure.ClinicalId > pc.ClinicalId
	AND SUBSTRING(pcPressure.FindingDetail, 1, 14) = '*BLOODPRESSURE'
	AND pcPressure.FindingDetail LIKE '%<%/%>'
LEFT JOIN dbo.PatientClinical pcSitting ON pcSitting.AppointmentId = pc.AppointmentId
	AND pcSitting.ClinicalType = 'F'
	AND pcSitting.Symptom = '/BLOOD PRESSURE'
	AND pcSitting.ClinicalId > pc.ClinicalId
	AND pcSitting.FindingDetail = '*SITTING=T1854 <SITTING>'
LEFT JOIN dbo.PatientClinical pcStanding ON pcStanding.AppointmentId = pc.AppointmentId
	AND pcStanding.ClinicalType = 'F'
	AND pcStanding.Symptom = '/BLOOD PRESSURE'
	AND pcStanding.ClinicalId > pc.ClinicalId
	AND pcStanding.FindingDetail = '*STANDING=T1855 <STANDING>'
LEFT JOIN dbo.PatientClinical pcPatReport ON pcPatReport.AppointmentId = pc.AppointmentId
	AND pcPatReport.ClinicalType = 'F'
	AND pcPatReport.Symptom = '/BLOOD PRESSURE'
	AND pcPatReport.ClinicalId > pc.ClinicalId
	AND pcPatReport.FindingDetail = '*PATREPORT=T7351 <PATREPORT>'
LEFT JOIN dbo.PatientClinical pcMeasured ON pcMeasured.AppointmentId = pc.AppointmentId
	AND pcMeasured.ClinicalType = 'F'
	AND pcMeasured.Symptom = '/BLOOD PRESSURE'
	AND pcMeasured.ClinicalId > pc.ClinicalId
	AND pcMeasured.FindingDetail = '*MEASURED=T7353 <MEASURED>'
LEFT JOIN dbo.PatientClinical pcTimeTaken ON pcTimeTaken.AppointmentId = pc.AppointmentId
	AND pcTimeTaken.ClinicalType = 'F'
	AND pcTimeTaken.Symptom = '/BLOOD PRESSURE'
	AND pcTimeTaken.ClinicalId > pc.ClinicalId
	AND pcTimeTaken.FindingDetail LIKE '*TIME=T11500 <%:%M>'
LEFT JOIN dbo.PatientNotes pn ON pn.AppointmentId = pc.AppointmentId
	AND NoteSystem = 'T36A'
WHERE pc.ClinicalType = 'F'
	AND pc.Symptom = '/BLOOD PRESSURE'
	AND pc.STATUS = 'A'
	AND SUBSTRING(pc.FindingDetail, 1, 5) = 'TIME='
	AND SUBSTRING(pc.FindingDetail, 1, 12) <> '*TIME=T11500'

GO

-- PatientFamilyHistoryEntry

SELECT 	MIN(pc.ClinicalId) AS Id
	,pc.PatientId AS PatientId
	,CASE dbo.ExtractTextValue('(', '(' + FindingDetail, '-FAM')
		WHEN 'RETINAL-PROBLEMS'
			THEN 261
		WHEN 'CATARACT'
			THEN 860
		WHEN 'GLAUCOMA'
			THEN 2270
		WHEN 'CANCER-OF-THE-EYE'
			THEN 5355
		WHEN 'BLINDNESS'
			THEN 680
		WHEN 'OTHER-EYE-DISORDER'
			THEN 1695
		WHEN 'CANCER'
			THEN 5356
		WHEN 'DIABETES'
			THEN 1611
		WHEN 'HEART-DISEASE'
			THEN 2329
		WHEN 'HIGH-BLOOD-PRESSURE'
			THEN 1964
		WHEN 'STROKE'
			THEN 917
		WHEN 'SICKLE-CELL'
			THEN 4664
		WHEN 'MIGRAINE-HEADACHES'
			THEN 3294
		WHEN 'UNKNOWN'
			THEN 5360
		ELSE 5354
		END AS ClinicalConditionId
	,1 AS ClinicalDataSourceTypeId
FROM dbo.PatientClinical pc
WHERE (pc.Symptom = '/FAMILY MEMBER - - - - CATARACT,  RETINAL DISEASE,  GLAUCOMA,  CANCER OF THE EYE,  BLINDNESS,  OTHER' 
OR pc.Symptom = '/FAMILY MEMBER - - - - DIABETES,  CANCER,  BLOOD PRESSURE,  SICKLE CELL,  MIGRAINE')
AND pc.ClinicalType = 'H'
AND pc.Status = 'A'
GROUP BY pc.PatientId, pc.Symptom,pc.FindingDetail

UNION ALL

SELECT 	MIN(pc.ClinicalId) AS Id
	,pc.PatientId AS PatientId
	,CASE dbo.ExtractTextValue('(', '(' + FindingDetail, '=')
		WHEN '*RETINAL-PROBLEMS-FAMILY'
			THEN 261
		WHEN 'CATARACT'
			THEN 860
		WHEN 'GLAUCOMA'
			THEN 2270
		WHEN 'CANCER:EYE'
			THEN 5355
		WHEN 'BLINDNESS'
			THEN 680
		WHEN 'EYE:OTHER'
			THEN 1695
		WHEN '*CANCER-FAMILY'
			THEN 5356
		WHEN '*DIABETES-FAMILY'
			THEN 1611
		WHEN 'HIGH BLOOD PRESSURE'
			THEN CASE  dbo.ExtractTextValue('=',FindingDetail + ')',')') 
				WHEN 'T11580'THEN 2329 --for whatever reason heart disease is listed as high blood pressure :/
				WHEN 'T770' THEN 1964
			END
		WHEN 'STROKE'
			THEN 917
		WHEN '*SICKLE-CELL-FAMILY'
			THEN 4664
		WHEN 'MIGRAINE'
			THEN 3294
		WHEN 'UNKNOWN'
			THEN 5360
		END AS ClinicalConditionId
		,2 AS ClinicalDataSourceTypeId
FROM dbo.PatientClinical pc
WHERE (pc.Symptom = '/FAMILY MEMBER - - - - CATARACT,  RETINAL DISEASE,  GLAUCOMA,  CANCER OF THE EYE,  BLINDNESS,  OTHER' 
OR pc.Symptom = '/FAMILY MEMBER - - - - DIABETES,  CANCER,  BLOOD PRESSURE,  SICKLE CELL,  MIGRAINE')
AND pc.ClinicalType = 'C'
AND pc.Status = 'A'
GROUP BY pc.PatientId, pc.Symptom,pc.FindingDetail

GO

-- PatientFamilyHistoryEntryDetail

SELECT MIN(pc.ClinicalId) AS Id
	,MIN(pc2.ClinicalId) AS PatientFamilyHistoryEntryId
	,CONVERT(DATETIME, NULL) AS StartDateTime
	,CONVERT(DATETIME, NULL) AS EndDateTime
	,ap.ResourceId1 AS UserId
	,CONVERT(DATETIME, ap.AppDate) AS EnteredDateTime
	,CONVERT(BIT, CASE 
			WHEN pc.STATUS = 'A'
				THEN 0
			ELSE 1
			END) AS IsDeactivated
	,NULL AS AccuracyQualifierId
	,NULL AS RelevancyQualifierId
	,CONVERT(NVARCHAR(MAX), NULL) AS Comment
FROM dbo.PatientClinical pc
INNER JOIN dbo.PatientClinical pc2 ON pc.PatientId = pc2.PatientId
	AND pc.FindingDetail = pc2.FindingDetail
	AND pc.Symptom = pc2.Symptom
	AND (pc.Symptom = '/FAMILY MEMBER - - - - CATARACT,  RETINAL DISEASE,  GLAUCOMA,  CANCER OF THE EYE,  BLINDNESS,  OTHER'
		OR pc.Symptom = '/FAMILY MEMBER - - - - DIABETES,  CANCER,  BLOOD PRESSURE,  SICKLE CELL,  MIGRAINE')
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
WHERE pc.STATUS = 'A'
	AND pc2.STATUS = 'A'
GROUP BY pc.FindingDetail
	,pc2.FindingDetail
	,ap.ResourceId1
	,ap.AppDate
	,pc.STATUS
	,pc.PatientId



GO

-- PatientFamilyHistoryEntryDetailFamilyRelationship
SELECT * FROM (
	SELECT
	MIN(pc2.ClinicalId) AS PatientFamilyHistoryEntryDetails_Id
	,CASE 
		WHEN frHWoutS.Id IS NULL
			THEN
				CASE SUBSTRING(pc.FindingDetail,0,2)
		WHEN '*'
			THEN frC.Id
		ELSE frH.Id 
					END 
		ELSE frHWoutS.Id
		END AS FamilyRelationships_Id
FROM PatientClinical pc
INNER JOIN dbo.PatientClinical pc2 ON pc.PatientId = pc2.PatientId
	AND (dbo.ExtractTextValue('-', pc.FindingDetail, '=') = dbo.ExtractTextValue('(', '(' + pc2.FindingDetail, '-FAM')
		OR (dbo.ExtractTextValue('-', pc.FindingDetail, '=') = 'HD'
			AND dbo.ExtractTextValue('(', '(' + pc2.FindingDetail, '-FAM') = 'HEART-DISEASE')
		OR (dbo.ExtractTextValue('-', pc.FindingDetail, '=') = 'EYE-DISORDER'
			AND dbo.ExtractTextValue('(', '(' + pc2.FindingDetail, '-FAM') = 'OTHER-EYE-DISORDER')
		OR (dbo.ExtractTextValue('-', pc.FindingDetail, '=') = dbo.ExtractTextValue('*', pc2.FindingDetail, '-FAM'))
		OR (dbo.ExtractTextValue('-', pc.FindingDetail, '=') = 'HD'
			AND dbo.ExtractTextValue('*', pc2.FindingDetail, '-FAM') = 'HEART-DISEASE')
		OR (dbo.ExtractTextValue('-', pc.FindingDetail, '=') = 'EYE-DISORDER'
			AND dbo.ExtractTextValue('*', pc2.FindingDetail, '-FAM') = 'OTHER-EYE-DISORDER')
		OR (dbo.ExtractTextValue('-', pc.FindingDetail, '=') = 'UNKNOWN-FHX'
			AND dbo.ExtractTextValue('(', '(' + pc2.FindingDetail, '-FAM') = 'UNKNOWN'))
	AND (pc2.Symptom = '/FAMILY MEMBER - - - - CATARACT,  RETINAL DISEASE,  GLAUCOMA,  CANCER OF THE EYE,  BLINDNESS,  OTHER'
		OR pc2.Symptom = '/FAMILY MEMBER - - - - DIABETES,  CANCER,  BLOOD PRESSURE,  SICKLE CELL,  MIGRAINE')
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
LEFT JOIN model.FamilyRelationships frH ON frH.NAME = dbo.ExtractTextValue('(', '(' + pc.FindingDetail, '-')
LEFT JOIN model.FamilyRelationships frHWoutS ON frHWoutS.NAME = dbo.ExtractTextValue('(', '(' + pc.FindingDetail, 's-') --This is because Patient Clincal sometimes saves plurl, but enum values are singular.
LEFT JOIN model.FamilyRelationships frC ON frC.NAME = dbo.ExtractTextValue('*', pc.FindingDetail, '-')
WHERE (pc.Symptom = '?WHICH FAMILY'
		OR pc.Symptom = '?TYPE OF FAMILY MEDICAL HISTORY')
	AND pc.STATUS = 'A' AND pc2.STATUS = 'A'
GROUP BY pc.FindingDetail
	,pc2.FindingDetail
	,frH.Id
	,frC.Id
	,pc.PatientId
	,frHWoutS.Id
) v
GROUP BY PatientFamilyHistoryEntryDetails_Id,FamilyRelationships_Id

GO

-- PatientHeightAndWeight
SELECT pc.ClinicalId AS Id
	,pc.PatientId AS PatientId
	,CASE 
		WHEN ap.AppTime >=0 
			THEN DATEADD(mi, ap.AppTime, CONVERT(datetime, ap.AppDate, 112)) 
		ELSE CONVERT(datetime, ap.AppDate, 112) 
	END AS MeasuredDateTime
	,CONVERT(DECIMAL(18,2), CASE 
		--Feet and inches
		WHEN (
				pcFeet.FindingDetail IS NOT NULL
				AND pcInches.FindingDetail IS NOT NULL
				)
			THEN (CONVERT(DECIMAL(18, 2), dbo.ExtractTextValue('<', pcFeet.FindingDetail,'>')) * 12) + (CONVERT(DECIMAL(18,2), dbo.ExtractTextValue('<', pcInches.FindingDetail, '>'))) / .03937
				--Feet only
		WHEN (
				pcFeet.FindingDetail IS NOT NULL
				AND pcInches.FindingDetail IS NULL
				)
			THEN (CONVERT(DECIMAL(18, 2), dbo.ExtractTextValue('<', pcFeet.FindingDetail, '>')) * 12) / .03937
				--Inches only
		WHEN (
				pcInches.FindingDetail IS NOT NULL
				AND pcFeet.FindingDetail IS NULL
				)
			THEN (CONVERT(DECIMAL(18, 2), dbo.ExtractTextValue('<', pcInches.FindingDetail, '>'))) / .03937
				--Meters and CM
		WHEN (
				pcMeters.FindingDetail IS NOT NULL
				AND pcCM.FindingDetail IS NOT NULL
				)
			THEN (CONVERT(DECIMAL(18, 2), dbo.ExtractTextValue('<', pcMeters.FindingDetail, '>')) * 100) + (CONVERT(DECIMAL(18,2), dbo.ExtractTextValue('<', pcCM.FindingDetail, '>')) * 10)
				--Meters only
		WHEN (
				pcMeters.FindingDetail IS NOT NULL
				AND pcCM.FindingDetail IS NULL
				)
			THEN (CONVERT(DECIMAL(18, 2), dbo.ExtractTextValue('<', pcMeters.FindingDetail,'>')))  * 1000
				--CM only
		WHEN (
				pcCM.FindingDetail IS NOT NULL
				AND pcMeters.FindingDetail IS NULL
				)
			THEN (CONVERT(DECIMAL(18, 2), dbo.ExtractTextValue('<', pcCM.FindingDetail, '>'))) / 10
				--Feet, inches, Meters, CM
		END) AS HeightUnit
	,CONVERT(DECIMAL(18,2), CASE 
		--Pounds and Ounces
		WHEN (
				pcPounds.FindingDetail IS NOT NULL
				AND pcOunces.FindingDetail IS NOT NULL
				)
			THEN (CONVERT(DECIMAL(18, 2), dbo.ExtractTextValue('<',pcPounds.FindingDetail, '>')) * 16) + (CONVERT(DECIMAL(18,2), dbo.ExtractTextValue('<',pcOunces.FindingDetail, '>')) * 16) * 28.3495 
		-- Ounces only				
		WHEN (
				pcPounds.FindingDetail IS NULL
				AND pcOunces.FindingDetail IS NOT NULL
				)
			THEN (CONVERT(DECIMAL(18,2), dbo.ExtractTextValue('<',pcOunces.FindingDetail, '>'))) * 28.3495 
		--Pounds only
		WHEN (
				pcPounds.FindingDetail IS NOT NULL
				AND pcOunces.FindingDetail IS NULL
				)
			THEN (CONVERT(DECIMAL(18, 2), dbo.ExtractTextValue('<',pcPounds.FindingDetail, '>'))) * 453.592
				--Kgs and Grams
		WHEN (
				pcKgs.FindingDetail IS NOT NULL
				AND pcGrams.FindingDetail IS NOT NULL
				)
			THEN (CONVERT(DECIMAL(18, 2), dbo.ExtractTextValue('<',pcKgs.FindingDetail, '>')) * 1000) + (CONVERT(DECIMAL(18,2), dbo.ExtractTextValue('<', pcGrams.FindingDetail, '>')))
				--Kgs only
		WHEN (
				pcKgs.FindingDetail IS NOT NULL
				AND pcGrams.FindingDetail IS NULL
				)
			THEN (CONVERT(DECIMAL(18, 2), dbo.ExtractTextValue('<', pcKgs.FindingDetail, '>'))) * 1000
				--Grams only
		WHEN (
				pcKgs.FindingDetail IS NULL
				AND pcGrams.FindingDetail IS NOT NULL
				)
			THEN (CONVERT(DECIMAL(18, 2), dbo.ExtractTextValue('<', pcGrams.FindingDetail, '>')))
		END) AS WeightUnit
	,CASE 
		WHEN (
				pcPounds.FindingDetail IS NOT NULL
				OR pcFeet.FindingDetail IS NOT NULL
				OR pcPounds.FindingDetail IS NOT NULL
				OR pcOunces.FindingDetail IS NOT NULL
				)
			THEN 2
		WHEN (
				pcMeters.FindingDetail IS NOT NULL
				OR pcKgs.FindingDetail IS NOT NULL
				OR pcCM.FindingDetail IS NOT NULL
				OR pcGrams.FindingDetail IS NOT NULL
				)
			THEN 1
		END AS MeasurementSystemId
	,1 AS ClinicalDataSourceTypeId
	,CONVERT(NVARCHAR(MAX),NULL) AS Comment
	,CONVERT(DATETIME, ap.AppDate, 112) AS EnteredDateTime
	,CASE 
		WHEN pc.STATUS = 'A'
			THEN CONVERT(BIT,0)
		ELSE CONVERT(BIT,1)
		END AS IsDeactivated
	,NULL AS AccuracyQualifierId
	,NULL AS RelevancyQualifierId
	,NULL AS BodyHeightObservationTypeId
	,NULL AS BodyWeightObservationTypeId
	,re.ResourceId AS UserId
	,pc.AppointmentId AS EncounterId
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON pc.AppointmentId = ap.AppointmentId
LEFT JOIN dbo.Resources re ON re.ResourceName = SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail) + 1, CHARINDEX(')', pc.FindingDetail) - CHARINDEX('(', pc.FindingDetail) - 1)
LEFT JOIN dbo.PatientClinical pcFeet ON pc.AppointmentId = pcFeet.AppointmentId
	AND pcFeet.Symptom = '/HEIGHT AND WEIGHT'
	AND pcFeet.FindingDetail LIKE '*HEIGHT-FEET=%'
LEFT JOIN dbo.PatientClinical pcInches ON pc.AppointmentId = pcInches.AppointmentId
	AND pcInches.Symptom = '/HEIGHT AND WEIGHT'
	AND pcInches.FindingDetail LIKE '*HEIGHT-INCHES=%'
LEFT JOIN dbo.PatientClinical pcMeters ON pc.AppointmentId = pcMeters.AppointmentId
	AND pcMeters.Symptom = '/HEIGHT AND WEIGHT'
	AND pcMeters.FindingDetail LIKE '*HEIGHT-METERS=%'
LEFT JOIN dbo.PatientClinical pcCM ON pc.AppointmentId = pcCM.AppointmentId
	AND pcCM.Symptom = '/HEIGHT AND WEIGHT'
	AND pcCM.FindingDetail LIKE '*HEIGHT-CM=%'
LEFT JOIN dbo.PatientClinical pcPounds ON pc.AppointmentId = pcPounds.AppointmentId
	AND pcPounds.Symptom = '/HEIGHT AND WEIGHT'
	AND pcPounds.FindingDetail LIKE '*WEIGHT-POUNDS=%'
LEFT JOIN dbo.PatientClinical pcOunces ON pc.AppointmentId = pcOunces.AppointmentId
	AND pcOunces.Symptom = '/HEIGHT AND WEIGHT'
	AND pcOunces.FindingDetail LIKE '*WEIGHT-OUNCES=%'
LEFT JOIN dbo.PatientClinical pcKgs ON pc.AppointmentId = pcKgs.AppointmentId
	AND pcKgs.Symptom = '/HEIGHT AND WEIGHT'
	AND pcKgs.FindingDetail LIKE '*WEIGHT-KGS=%'
LEFT JOIN dbo.PatientClinical pcGrams ON pc.AppointmentId = pcGrams.AppointmentId
	AND pcGrams.Symptom = '/HEIGHT AND WEIGHT'
	AND pcGrams.FindingDetail LIKE '*WEIGHT-GRAMS=%'
WHERE pc.Symptom = '/HEIGHT AND WEIGHT'
	AND pc.FindingDetail LIKE 'TIME=%'
	AND (pcFeet.FindingDetail IS NOT NULL 
		OR pcInches.FindingDetail IS NOT NULL 
		OR pcMeters.FindingDetail IS NOT NULL
		OR pcCM.FindingDetail IS NOT NULL)
	AND (pcPounds.FindingDetail IS NOT NULL
		OR pcOunces.FindingDetail IS NOT NULL
		OR pcKgs.FindingDetail IS NOT NULL
		OR pcGrams.FindingDetail IS NOT NULL)

GO

-- PatientCognitiveStatusAssessment
SELECT
pc.ClinicalId AS Id
,NULL AS AccuracyQualifierId
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
	ELSE CONVERT(DATETIME, ap.AppDate) 
END AS AssessedDateTime
,CASE 
	WHEN dbo.IsNullOrEmpty(pcMEMORYIMPAIRMENT.ClinicalId) = 0 AND dbo.IsNullOrEmpty(pcNOIMPAIRMENT.ClinicalId) = 1
		THEN 3245 --Memory Imparirment
	ELSE 5348
END AS ClinicalConditionId
,1 AS ClinicalDataSourceTypeId
,CONVERT(NVARCHAR,NULL) AS Comment
,pc.AppointmentId AS EncounterId
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
	ELSE CONVERT(DATETIME, ap.AppDate) 
END AS EnteredDateTime
,CONVERT(BIT,0) AS IsDeactivated
,pc.PatientId
,NULL AS RelevancyQualifierId
,re.ResourceId AS UserId
FROM dbo.PatientClinical pc
LEFT JOIN dbo.PatientClinical pcNOIMPAIRMENT ON pcNOIMPAIRMENT.AppointmentId = pc.AppointmentId
	AND pcNOIMPAIRMENT.Symptom = '/COGNITIVE STATUS'
	AND pcNOIMPAIRMENT.ClinicalType = 'F'
	AND pcNOIMPAIRMENT.EyeContext = ''
	AND pcNOIMPAIRMENT.[Status] = 'A'
	AND SUBSTRING(pcNOIMPAIRMENT.FindingDetail,1,29) = '*NOIMPAIRMENT-COGNITIVESTATUS'
LEFT JOIN dbo.PatientClinical pcMEMORYIMPAIRMENT ON pcMEMORYIMPAIRMENT.AppointmentId = pc.AppointmentId
	AND pcMEMORYIMPAIRMENT.Symptom = '/COGNITIVE STATUS'
	AND pcMEMORYIMPAIRMENT.ClinicalType = 'F'
	AND pcMEMORYIMPAIRMENT.EyeContext = ''
	AND pcMEMORYIMPAIRMENT.[Status] = 'A'
	AND SUBSTRING(pcMEMORYIMPAIRMENT.FindingDetail,1,33) = '*MEMORYIMPAIRMENT-COGNITIVESTATUS'
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
INNER JOIN dbo.Resources re ON re.ResourceName = dbo.ExtractTextValue('(',pc.FindingDetail,')')
WHERE pc.Symptom = '/COGNITIVE STATUS'
	AND SUBSTRING(pc.FindingDetail,1,4) = 'TIME'
	AND pc.ClinicalType = 'F'
	AND pc.EyeContext = ''
	AND pc.[Status] = 'A'

GO

-- PatientFunctionalStatusAssessment
SELECT
pc.ClinicalId AS Id
,NULL AS AccuracyQualifierId
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
	ELSE CONVERT(DATETIME, ap.AppDate) 
END AS AssessedDateTime
,CASE 
	WHEN dbo.IsNullOrEmpty(pcWALKINGSTICK.ClinicalId) = 0 AND dbo.IsNullOrEmpty(pcNOIMPAIRMENT.ClinicalId) = 1
		THEN 5349 --Dependence on walking stick
	ELSE 5348
END AS ClinicalConditionId
,1 AS ClinicalDataSourceTypeId
,CONVERT(NVARCHAR,NULL) AS Comment
,pc.AppointmentId AS EncounterId
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
	ELSE CONVERT(DATETIME, ap.AppDate) 
END AS EnteredDateTime
,CONVERT(BIT,0) AS IsDeactivated
,pc.PatientId
,NULL AS RelevancyQualifierId
,re.ResourceId AS UserId
FROM dbo.PatientClinical pc
LEFT JOIN dbo.PatientClinical pcNOIMPAIRMENT ON pcNOIMPAIRMENT.AppointmentId = pc.AppointmentId
	AND pcNOIMPAIRMENT.Symptom = '/FUNCTIONAL STATUS'
	AND pcNOIMPAIRMENT.ClinicalType = 'F'
	AND pcNOIMPAIRMENT.EyeContext = ''
	AND pcNOIMPAIRMENT.[Status] = 'A'
	AND SUBSTRING(pcNOIMPAIRMENT.FindingDetail,1,30) = '*NOIMPAIRMENT-FUNCTIONALSTATUS'
LEFT JOIN dbo.PatientClinical pcWALKINGSTICK ON pcWALKINGSTICK.AppointmentId = pc.AppointmentId
	AND pcWALKINGSTICK.Symptom = '/FUNCTIONAL STATUS'
	AND pcWALKINGSTICK.ClinicalType = 'F'
	AND pcWALKINGSTICK.EyeContext = ''
	AND pcWALKINGSTICK.[Status] = 'A'
	AND SUBSTRING(pcWALKINGSTICK.FindingDetail,1,42) = '*DEPENDENCEONWALKINGSTICK-FUNCTIONALSTATUS'
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
INNER JOIN dbo.Resources re ON re.ResourceName = dbo.ExtractTextValue('(',pc.FindingDetail,')')
WHERE pc.Symptom = '/FUNCTIONAL STATUS'
	AND SUBSTRING(pc.FindingDetail,1,4) = 'TIME'
	AND pc.ClinicalType = 'F'
	AND pc.EyeContext = ''
	AND pc.[Status] = 'A'

GO

-- PatientLaboratoryTestResult
SELECT 
obd.OBXID AS Id
,lr.PatientId AS PatientId
,lt.Id AS LaboratoryTestId
,NULL AS ClinicalConditionId
,CASE 
	WHEN dbo.IsNullOrEmpty(obd.Result) = 0 AND ISNUMERIC(obd.Result) = 1
		THEN CONVERT(DECIMAL(18,2),obd.Result)
	ELSE CONVERT(DECIMAL(18,2),NULL)
END AS ResultUnit
,meas.Id AS UnitOfMeasurementId
,CASE 
	WHEN dbo.IsNullOrEmpty(obd.NTEComments) = 0
		THEN CONVERT(NVARCHAR(MAX),obd.NTEComments)
	ELSE CONVERT(NVARCHAR(MAX),NULL) 
END AS Comment
,NULL AS OrderingExternalProviderId
,NULL AS ExternalOrganizationId
,NULL AS LabMedicalDirectorExternalProviderId
,CONVERT(DATETIME,NULL) AS OrderedDateTime
,CONVERT(DATETIME,NULL) AS SpecimenCollectedDateTime
,CONVERT(DATETIME,NULL) AS AnalysisDateTime
,CONVERT(DATETIME,NULL) AS ObservationDateTime
,CASE 
	WHEN dbo.IsNullOrEmpty(lr.ReportDate) = 0
		THEN CONVERT(DATETIME,lr.ReportDate)
	ELSE CONVERT(DATETIME,NULL)
END AS ReportDateTime
,CONVERT(DATETIME,NULL) AS ReportReceivedDateTime
,abn.Id AS LaboratoryTestAbnormalFlagId
,NULL AS LaboratoryCollectionSiteId
,NULL AS LaboratoryComponentId
,NULL AS LaboratoryMeasuredPropertyId
,NULL AS LaboratoryMethodUsedId
,nr.Id AS LaboratoryNormalRangeId
,NULL AS LaboratoryResultVerificationId
,NULL AS LaboratorySampleSizeId
,NULL AS LaboratoryScaleTypeId
,NULL AS LaboratoryTestingInstrumentId
,NULL AS LaboratoryTestingPlaceId
,NULL AS LaboratoryTestingPriorityId
,NULL AS LaboratoryTimingId
,NULL AS SpecimenSourceId
,NULL AS SpecimenConditionId
,ob.RequestedDate AS EnteredDateTime
,CONVERT(BIT,0) AS IsDeactivated
,NULL AS AccuracyQualifierId
,NULL AS RelevancyQualifierId
,NULL AS LaboratorySpecimenRejectReasonId
,lr.ProviderId AS UserId
,1 AS ClinicalDataSourceTypeId
FROM  dbo.HL7_Observation ob
INNER JOIN dbo.HL7_Observation_Details obd ON obd.OBRID = ob.OBRID
INNER JOIN dbo.HL7_LabReports lr ON lr.ReportID = ob.ReportID
LEFT JOIN model.UnitsOfMeasurement meas ON meas.Abbreviation = obd.Units COLLATE Latin1_General_CS_AS
LEFT JOIN model.LaboratoryTests lt ON lt.Name = ob.OrderDesc COLLATE Latin1_General_CS_AI
LEFT JOIN model.LaboratoryTestAbnormalFlags abn ON abn.Name = obd.AbnormalFlag
LEFT JOIN model.LaboratoryNormalRanges nr ON nr.Name = obd.RefRange

GO

-- PatientMedication
SELECT ClinicalId AS Id
	,CASE
		WHEN dbo.IsNullOrEmpty(pc.DrawFileName) = 0 AND ISNUMERIC(pc.DrawFileName) = 1
			THEN CONVERT(INT,pc.DrawFileName)
		ELSE dru.DrugId + 10000000 
		END AS MedicationId
	,1 AS ClinicalDataSourceTypeId
	,pc.PatientId AS PatientId
FROM dbo.PatientClinical pc
--The only way to link to dbo.drug is a join on name. The drug name is in between RX-8/ and -2/. 
INNER JOIN dbo.Drug dru ON SUBSTRING(pc.FindingDetail, CHARINDEX('RX-8/', pc.FindingDetail) + 5,(CHARINDEX('-2/',pc.FindingDetail) - CHARINDEX('RX-8/', pc.FindingDetail) - 5)) = dru.DisplayName
WHERE pc.ClinicalType = 'A'
	AND SUBSTRING(pc.FindingDetail, 1, 3) = 'RX-'
	AND pc.Status = 'A'
	AND pc.PatientId > 0
	AND pc.FindingDetail LIKE '%-2/%'

UNION ALL

SELECT ClinicalId AS Id
	,CASE
		WHEN dbo.IsNullOrEmpty(pc.DrawFileName) = 0 AND ISNUMERIC(pc.DrawFileName) = 1
			THEN CONVERT(INT,pc.DrawFileName)
		ELSE dru.DrugId + 20000000 
		END AS MedicationId
	,1 AS ClinicalDataSourceTypeId
	,pc.PatientId AS PatientId
FROM dbo.PatientClinical pc
--The only way to link to dbo.drug is a join on name. The drug name is in between RX-8/ and -2/. 
INNER JOIN dbo.DrugArchive dru ON SUBSTRING(pc.FindingDetail, CHARINDEX('RX-8/', pc.FindingDetail) + 5,(CHARINDEX('-2/',pc.FindingDetail) - CHARINDEX('RX-8/', pc.FindingDetail) - 5)) = dru.DisplayName
WHERE pc.ClinicalType = 'A'
	AND SUBSTRING(pc.FindingDetail, 1, 3) = 'RX-'
	AND pc.Status = 'A'
	AND pc.PatientId > 0
	AND pc.FindingDetail LIKE '%-2/%'

GO

-- PatientMedicationDetail
WITH CTE AS (
		SELECT 
		pc.ImageDescriptor
		,pc.ClinicalId
		,CONVERT(NVARCHAR(MAX),SUBSTRING(pc.ImageDescriptor,1,1)) AS ChangeType
		,CONVERT(NVARCHAR(MAX),SUBSTRING(pc.ImageDescriptor,1,11)) AS ChangedDate
		,1 AS RowId
		FROM dbo.PatientClinical pc
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
		WHERE pc.ClinicalType = 'A'
			AND SUBSTRING(pc.FindingDetail, 1, 3) = 'RX-'
			AND pc.[Status] = 'A'
				AND pc.PatientId > 0
	
		UNION ALL
	
		SELECT 
		CTE.ImageDescriptor
		,CTE.ClinicalId
		,CONVERT(NVARCHAR(MAX),SUBSTRING(CTE.ImageDescriptor,(CTE.RowId*11)+1,1)) AS ChangeType
		,CONVERT(NVARCHAR(MAX),SUBSTRING(CTE.ImageDescriptor,(CTE.RowId*11)+1,11)) AS ChangedDate
		,CTE.RowId + 1 AS RowId
		FROM CTE
		WHERE LEN(CONVERT(NVARCHAR(MAX),SUBSTRING(CTE.ImageDescriptor,(CTE.RowId*11)+1,11)))>=11
	),
	rowVars AS (
	SELECT 
		ClinicalId
		,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'\d*\sDAYS\sSUPPLY') AS DaysSupply
		,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?:-4/\(.*?\)\(.*?\))(\(.*?\))') AS DrugDispenseAmount
		,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'-2\/\(.*?\)') AS DrugDosageNumberId
		,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'-5/\(.*?\)') AS DurationComment
		,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'-4\/(.*?)\)\(DAW\)') AS IsDispenseAsWritten
		,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'\d*?\sREFILLS') AS RefillCount
		FROM dbo.PatientClinical pc
		INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
		WHERE pc.ClinicalType = 'A'
			AND SUBSTRING(pc.FindingDetail, 1, 3) = 'RX-'
			AND pc.[Status] = 'A'
				AND pc.PatientId > 0
	)
	SELECT 
	model.GetBigPairId(CTE.clinicalId, CTE.RowId, 400000000) AS Id
	,CONVERT(INT,CASE
		WHEN dbo.IsNullOrEmpty(r.DaysSupply) = 0
			THEN SUBSTRING(r.DaysSupply,0,CHARINDEX(' ',r.DaysSupply))
	END) AS DaysSupply
	,CONVERT(DECIMAL(18,2),CASE
		WHEN (CHARINDEX('(',REVERSE(r.DrugDispenseAmount)))-CHARINDEX(')',REVERSE(r.DrugDispenseAmount)) > 0
			THEN	CASE
					WHEN dbo.IsNullOrEmpty(REVERSE(SUBSTRING(
					REVERSE(r.DrugDispenseAmount)
					,CHARINDEX(')',REVERSE(r.DrugDispenseAmount))+1
					,(CHARINDEX('(',REVERSE(r.DrugDispenseAmount)))-CHARINDEX(')',REVERSE(r.DrugDispenseAmount))-1
					))) = 0
					THEN SUBSTRING(REVERSE(SUBSTRING(
					REVERSE(r.DrugDispenseAmount)
					,CHARINDEX(')',REVERSE(r.DrugDispenseAmount))+1
					,(CHARINDEX('(',REVERSE(r.DrugDispenseAmount)))-CHARINDEX(')',REVERSE(r.DrugDispenseAmount))-1
					)),0,CHARINDEX(' ',REVERSE(SUBSTRING(
					REVERSE(r.DrugDispenseAmount)
					,CHARINDEX(')',REVERSE(r.DrugDispenseAmount))+1
					,(CHARINDEX('(',REVERSE(r.DrugDispenseAmount)))-CHARINDEX(')',REVERSE(r.DrugDispenseAmount))-1
					))))
					END
	END) AS DrugDispenseAmount
	,CONVERT(INT, NULL) AS DrugDispenseFormId
	,CONVERT(INT,CASE SUBSTRING(SUBSTRING(
		r.DrugDosageNumberId
		,CHARINDEX('(',r.DrugDosageNumberId)+1
		,(CHARINDEX(' ',r.DrugDosageNumberId))
	),1,CHARINDEX(' ',SUBSTRING(
		r.DrugDosageNumberId
		,CHARINDEX('(',r.DrugDosageNumberId)+1
		,(CHARINDEX(' ',r.DrugDosageNumberId)))
		)
	)
		WHEN '0.5'
			THEN 1
		WHEN '1'
			THEN 2
		WHEN '1.5'
			THEN 5
		WHEN '2'
			THEN 6
		WHEN '3'
			THEN 7
		WHEN '4'
			THEN 8
		WHEN '5'
			THEN 9
		WHEN '10'
			THEN 10
		WHEN '15'
			THEN 11
		WHEN '20'
			THEN 12
		WHEN '30'
			THEN 12
		WHEN '6'
			THEN 18
		WHEN '7'
			THEN 19
		WHEN '8'
			THEN 20
		WHEN '9'
			THEN 21
		WHEN '11'
			THEN 22
		WHEN '12'
			THEN 23
		ELSE NULL 
	END) AS DrugDosageNumberId
	,CONVERT(INT,CASE 
		WHEN ISNUMERIC(SUBSTRING(r.DurationComment
				,CHARINDEX('(',r.DurationComment)+1
				,1)) = 1
		THEN CASE 
			WHEN r.DurationComment LIKE '%MINUTES%'
				THEN 1
			WHEN r.DurationComment LIKE '%HOURS%'
				THEN 2
			WHEN r.DurationComment LIKE '%DAYS%'
				THEN 3
			WHEN r.DurationComment LIKE '%WEEKS%'
				THEN 4
			WHEN r.DurationComment LIKE '%MONTHS%'
				THEN 5
			WHEN r.DurationComment LIKE '%YEARS%'
				THEN 6
		END
	END) AS TimeFrameId
	,CONVERT(DECIMAL(12,2),CASE 
		WHEN ISNUMERIC(SUBSTRING(r.DurationComment
				,CHARINDEX('(',r.DurationComment)+1
				,1)) = 1
			THEN SUBSTRING([IO.Practiceware.SqlClr].[SearchRegexPattern](r.DurationComment,'\/\(\d*')
			,CHARINDEX('(',[IO.Practiceware.SqlClr].[SearchRegexPattern](r.DurationComment,'\/\(\d*'))+1
			,LEN([IO.Practiceware.SqlClr].[SearchRegexPattern](r.DurationComment,'\/\(\d*')))
			ELSE 0
	END) AS TimeUnit
	,CASE 
		WHEN dbo.IsNullOrEmpty(r.IsDispenseAsWritten) = 0
			THEN CONVERT(BIT, 1)
		ELSE CONVERT(BIT, 0) 
	END AS IsDispenseAsWritten
	,pc.ClinicalId AS PatientMedicationId
	,1 AS RouteOfAdministrationId
	,CONVERT(INT,CASE 
		WHEN dbo.IsNullOrEmpty(r.RefillCount) = 0
			THEN SUBSTRING(r.RefillCount,0,CHARINDEX(' ',r.RefillCount))
	END) AS RefillCount
	,(SELECT TOP 1 Note1 FROM dbo.PatientNotes WHERE ClinicalId = pc.ClinicalId) As AsPrescribedComment -- Left join with dbo.PatientNotes executes much longer
	,CONVERT(NVARCHAR(MAX),CASE
		WHEN SUBSTRING(
			r.DurationComment
			,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
			,CHARINDEX(')',r.DurationComment)
		) LIKE ('%MINUTES%')
			THEN NULL 
		WHEN SUBSTRING(
			r.DurationComment
			,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
			,CHARINDEX(')',r.DurationComment)
		) LIKE ('%HOURS%')
			THEN NULL 
		WHEN SUBSTRING(
			r.DurationComment
			,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
			,CHARINDEX(')',r.DurationComment)
		) LIKE ('%DAYS%')
			THEN NULL 
		WHEN SUBSTRING(
			r.DurationComment
			,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
			,CHARINDEX(')',r.DurationComment)
		) LIKE ('%WEEKS%')
			THEN NULL 
		WHEN SUBSTRING(
			r.DurationComment
			,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
			,CHARINDEX(')',r.DurationComment)
		) LIKE ('%MONTHS%')
			THEN NULL 
		WHEN SUBSTRING(
			r.DurationComment
			,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
			,CHARINDEX(')',r.DurationComment)
		) LIKE ('%YEARS%')
			THEN NULL 
		ELSE CASE 
				WHEN CHARINDEX('-5/(',r.DurationComment) >= 0 AND CHARINDEX(')',r.DurationComment) >= 5
				THEN
					CASE WHEN dbo.IsNullOrEmpty(SUBSTRING(
					r.DurationComment
					,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
					,CHARINDEX(')',r.DurationComment)-5)
					)= 0
					THEN 
					SUBSTRING(
					r.DurationComment
					,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
					,CHARINDEX(')',r.DurationComment)-5)
					END
				ELSE NULL
				END
	END) AS DurationComment
	,NULL AS FrequencyId
	,CONVERT(DATETIME, SUBSTRING(CTE.ChangedDate,2,LEN(CTE.ChangedDate))) AS MedicationEventDateTime
	,CONVERT(INT,CASE CTE.ChangeType 
		WHEN 'D'
			THEN 8
		WHEN 'K'
			THEN 2
		WHEN 'R'
			THEN 4
		WHEN 'C'
			THEN 5
		WHEN 'P'
			THEN 6
		ELSE 2
	END) AS MedicationEventTypeId
	,NULL AS ExternalProviderId
	,CASE CTE.ChangeType 
		WHEN 'D'
			THEN CONVERT(BIT,1)
		ELSE CONVERT(BIT,0)
	END AS IsDeactivated	
	FROM CTE 
	INNER JOIN dbo.PatientClinical pc ON pc.ClinicalId = CTE.ClinicalId
	INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
	INNER JOIN rowVars r ON r.ClinicalId = pc.ClinicalId
	WHERE pc.ClinicalType = 'A'
		AND SUBSTRING(pc.FindingDetail, 1, 3) = 'RX-'
		AND CTE.ChangeType <> '!'
		AND pc.[Status] = 'A'
		AND pc.PatientId > 0

	UNION ALL 
	
	SELECT 
	CONVERT(BIGINT, pc.ClinicalId) AS Id
	,CONVERT(INT,CASE
		WHEN dbo.IsNullOrEmpty(r.DaysSupply) = 0
			THEN SUBSTRING(r.DaysSupply,0,CHARINDEX(' ',r.DaysSupply))
	END) AS DaysSupply
	,CONVERT(DECIMAL(18,2),CASE
		WHEN (CHARINDEX('(',REVERSE(r.DrugDispenseAmount)))-CHARINDEX(')',REVERSE(r.DrugDispenseAmount)) > 0
			THEN CASE
				WHEN dbo.IsNullOrEmpty(REVERSE(SUBSTRING(
				REVERSE(r.DrugDispenseAmount)
				,CHARINDEX(')',REVERSE(r.DrugDispenseAmount))+1
				,(CHARINDEX('(',REVERSE(r.DrugDispenseAmount)))-CHARINDEX(')',REVERSE(r.DrugDispenseAmount))-1
				))) = 0
				THEN SUBSTRING(REVERSE(SUBSTRING(
				REVERSE(r.DrugDispenseAmount)
				,CHARINDEX(')',REVERSE(r.DrugDispenseAmount))+1
				,(CHARINDEX('(',REVERSE(r.DrugDispenseAmount)))-CHARINDEX(')',REVERSE(r.DrugDispenseAmount))-1
				)),0,CHARINDEX(' ',REVERSE(SUBSTRING(
				REVERSE(r.DrugDispenseAmount)
				,CHARINDEX(')',REVERSE(r.DrugDispenseAmount))+1
				,(CHARINDEX('(',REVERSE(r.DrugDispenseAmount)))-CHARINDEX(')',REVERSE(r.DrugDispenseAmount))-1
				))))
				END
	END) AS DrugDispenseAmount
	,CONVERT(INT, NULL) AS DrugDispenseFormId
	,CONVERT(INT,CASE SUBSTRING(SUBSTRING(
		r.DrugDosageNumberId
		,CHARINDEX('(',r.DrugDosageNumberId)+1
		,(CHARINDEX(' ',r.DrugDosageNumberId))
	),1,CHARINDEX(' ',SUBSTRING(
		r.DrugDosageNumberId
		,CHARINDEX('(',r.DrugDosageNumberId)+1
		,(CHARINDEX(' ',r.DrugDosageNumberId)))
		)
	)
		WHEN '0.5'
			THEN 1
		WHEN '1'
			THEN 2
		WHEN '1.5'
			THEN 5
		WHEN '2'
			THEN 6
		WHEN '3'
			THEN 7
		WHEN '4'
			THEN 8
		WHEN '5'
			THEN 9
		WHEN '10'
			THEN 10
		WHEN '15'
			THEN 11
		WHEN '20'
			THEN 12
		WHEN '30'
			THEN 12
		WHEN '6'
			THEN 18
		WHEN '7'
			THEN 19
		WHEN '8'
			THEN 20
		WHEN '9'
			THEN 21
		WHEN '11'
			THEN 22
		WHEN '12'
			THEN 23
		ELSE NULL 
	END) AS DrugDosageNumberId
	,CONVERT(INT,CASE 
		WHEN ISNUMERIC(SUBSTRING(r.DurationComment
				,CHARINDEX('(',r.DurationComment)+1
				,1)) = 1
		THEN CASE 
			WHEN r.DurationComment LIKE '%MINUTES%'
				THEN 1
			WHEN r.DurationComment LIKE '%HOURS%'
				THEN 2
			WHEN r.DurationComment LIKE '%DAYS%'
				THEN 3
			WHEN r.DurationComment LIKE '%WEEKS%'
				THEN 4
			WHEN r.DurationComment LIKE '%MONTHS%'
				THEN 5
			WHEN r.DurationComment LIKE '%YEARS%'
				THEN 6
		END
	END) AS TimeFrameId
	,CONVERT(DECIMAL(12,2),CASE 
		WHEN ISNUMERIC(SUBSTRING(r.DurationComment
				,CHARINDEX('(',r.DurationComment)+1
				,1)) = 1
			THEN SUBSTRING([IO.Practiceware.SqlClr].[SearchRegexPattern](r.DurationComment,'\/\(\d*')
			,CHARINDEX('(',[IO.Practiceware.SqlClr].[SearchRegexPattern](r.DurationComment,'\/\(\d*'))+1
			,LEN([IO.Practiceware.SqlClr].[SearchRegexPattern](r.DurationComment,'\/\(\d*')))
			ELSE 0
	END) AS TimeUnit
	,CONVERT(BIT,CASE 
		WHEN dbo.IsNullOrEmpty(r.IsDispenseAsWritten) = 0
			THEN CONVERT(BIT, 1)
		ELSE CONVERT(BIT, 0) 
	END) AS IsDispenseAsWritten
	,pc.ClinicalId AS PatientMedicationId
	,1 AS RouteOfAdministrationId
	,CONVERT(INT,CASE 
		WHEN dbo.IsNullOrEmpty(r.RefillCount) = 0
			THEN SUBSTRING(r.RefillCount,0,CHARINDEX(' ',r.RefillCount))
	END) AS RefillCount
	,(SELECT TOP 1 Note1 FROM dbo.PatientNotes WHERE ClinicalId = pc.ClinicalId) As AsPrescribedComment -- Left join with dbo.PatientNotes executes much longer
	,CASE
		WHEN SUBSTRING(
			r.DurationComment
			,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
			,CHARINDEX(')',r.DurationComment)
		) LIKE ('%MINUTES%')
			THEN NULL 
		WHEN SUBSTRING(
			r.DurationComment
			,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
			,CHARINDEX(')',r.DurationComment)
		) LIKE ('%HOURS%')
			THEN NULL 
		WHEN SUBSTRING(
			r.DurationComment
			,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
			,CHARINDEX(')',r.DurationComment)
		) LIKE ('%DAYS%')
			THEN NULL 
		WHEN SUBSTRING(
			r.DurationComment
			,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
			,CHARINDEX(')',r.DurationComment)
		) LIKE ('%WEEKS%')
			THEN NULL 
		WHEN SUBSTRING(
			r.DurationComment
			,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
			,CHARINDEX(')',r.DurationComment)
		) LIKE ('%MONTHS%')
			THEN NULL 
		WHEN SUBSTRING(
			r.DurationComment
			,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
			,CHARINDEX(')',r.DurationComment)
		) LIKE ('%YEARS%')
			THEN NULL 
		ELSE CASE 
				WHEN CHARINDEX('-5/(',r.DurationComment) >= 0 AND CHARINDEX(')',r.DurationComment) >= 5
				THEN
				CASE WHEN dbo.IsNullOrEmpty(SUBSTRING(
					r.DurationComment
					,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
					,CHARINDEX(')',r.DurationComment)-5)
					)= 0
					THEN 
					SUBSTRING(
					r.DurationComment
					,CHARINDEX('-5/(',r.DurationComment)+LEN('-5/(')
					,CHARINDEX(')',r.DurationComment)-5)
					END
				ELSE NULL
				END
	END AS DurationComment
	,NULL AS FrequencyId
	,CONVERT(DATETIME, ap.Appdate) AS MedicationEventDateTime
	,CASE 
		WHEN FindingDetail LIKE '%(STAT)%'
			THEN 10
		ELSE 1 
	END AS MedicationEventTypeId
	,NULL AS ExternalProviderId
	,CONVERT(BIT, 0) AS IsDeactivated
	FROM dbo.PatientClinical pc
	INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
	INNER JOIN rowVars r ON r.ClinicalId = pc.ClinicalId
	WHERE pc.ClinicalType = 'A'
		AND SUBSTRING(pc.FindingDetail, 1, 3) = 'RX-'
		AND pc.[Status] = 'A'
		AND dbo.IsNullOrEmpty(r.DrugDispenseAmount) = 0
		AND pc.PatientId > 0
GO

-- PatientProcedurePerformed
SELECT 
MAX(ClinicalId) AS Id
,1 AS ClinicalDataSourceTypeId
,cp.Id AS ClinicalProcedureId
,ap.EncounterId AS EncounterId
,pc.PatientId AS PatientId
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
	ELSE CONVERT(DATETIME, ap.AppDate) 
END AS PerformedDateTime
,NULL AS TimeQualifierId
, CONVERT(BIT,1) AS IsBillable
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
INNER JOIN model.ClinicalProcedures cp ON '/' + cp.Name = pc.Symptom
WHERE pc.ClinicalType = 'F'
	AND pc.[Status] = 'A'
	AND cp.ClinicalProcedureCategoryId = 3 --Procedure
GROUP BY cp.Id
,ap.EncounterId
,pc.PatientId
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
	ELSE CONVERT(DATETIME, ap.AppDate) 
END 

GO

-- PatientDiagnosticTestPerformed
SELECT 
MAX(ClinicalId) AS Id
,1 AS ClinicalDataSourceTypeId
,cp.Id AS ClinicalProcedureId
,ap.EncounterId AS EncounterId
,pc.PatientId AS PatientId
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
	ELSE CONVERT(DATETIME, ap.AppDate) 
END AS PerformedDateTime
,NULL AS TimeQualifierId
, CONVERT(BIT,1) AS IsBillable
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
INNER JOIN model.ClinicalProcedures cp ON '/' + cp.Name = pc.Symptom
WHERE pc.ClinicalType = 'F'
   AND pc.[Status] = 'A'
   AND cp.ClinicalProcedureCategoryId = 2 --Observation
GROUP BY cp.Id
,ap.EncounterId
,pc.PatientId
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
	ELSE CONVERT(DATETIME, ap.AppDate) 
END 

GO

-- PatientSmokingStatus
SELECT pc.ClinicalId AS Id
	,pc.AppointmentId AS EncounterId
	,CASE
		WHEN pcEveryday.ClinicalId IS NOT NULL
			THEN 2
		WHEN pcSomeDay.ClinicalId IS NOT NULL
			THEN 3
		WHEN pcHeavy.ClinicalId IS NOT NULL
			THEN 4
		WHEN pcLight.ClinicalId IS NOT NULL
			THEN 5
		WHEN pcUnknown.ClinicalId IS NOT NULL
			THEN 7
		WHEN pcNever.ClinicalId IS NOT NULL
			THEN 8
		WHEN pcFormer.ClinicalId IS NOT NULL
			THEN 1
		ELSE 6 --unknown
		END AS SmokingConditionId
	,CONVERT(nvarchar(max), NULL) AS Comment
	,CASE --We don't know what type of date time they will enter in the box. Trying 2 of the most common. Need to also add logic here for if the patient is a smoker, else should always be null. 
		WHEN dbo.TryConvertDateTime101(dbo.ExtractTextValue('<', pcStartDateTime.FindingDetail, '>')) IS NOT NULL
			THEN  dbo.TryConvertDateTime101(dbo.ExtractTextValue('<', pcStartDateTime.FindingDetail, '>'))
		WHEN dbo.TryConvertDateTime112(dbo.ExtractTextValue('<', pcStartDateTime.FindingDetail, '>')) IS NOT NULL
			THEN dbo.TryConvertDateTime112(dbo.ExtractTextValue('<', pcStartDateTime.FindingDetail, '>')) 
		ELSE CONVERT(datetime, NULL)
	END AS SmokingStartDateTime
	,CASE --We don't know what type of date time they will enter in the box. Trying 2 of the most common. Need to also add logic here for if the patient is a smoker, else should always be null. 
		WHEN dbo.TryConvertDateTime101(dbo.ExtractTextValue('<', pcEndDateTime.FindingDetail, '>')) IS NOT NULL
			THEN  dbo.TryConvertDateTime101(dbo.ExtractTextValue('<', pcEndDateTime.FindingDetail, '>'))
		WHEN dbo.TryConvertDateTime112(dbo.ExtractTextValue('<', pcEndDateTime.FindingDetail, '>')) IS NOT NULL
			THEN dbo.TryConvertDateTime112(dbo.ExtractTextValue('<', pcEndDateTime.FindingDetail, '>')) 
		ELSE CONVERT(datetime, NULL)
	END  AS SmokingEndDateTime
	,pc.PatientId AS PatientId
	,re.ResourceId AS UserId
	,NULL AS AccuracyQualifierId
	,NULL AS RelevancyQualifierId
	,CONVERT(DATETIME, ap.AppDate, 112) AS EnteredDateTime
	,CONVERT(bit, 0) AS IsDeactivated
	,1 AS ClinicalDataSourceTypeId
FROM dbo.PatientClinical pc 
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
LEFT JOIN dbo.Resources re ON re.ResourceName = 
	CASE 
		WHEN CHARINDEX('(', pc.FindingDetail) > 0 AND CHARINDEX(')', pc.FindingDetail) > 0 THEN
			SUBSTRING(pc.FindingDetail, CHARINDEX('(', pc.FindingDetail)+1, CHARINDEX(')', pc.FindingDetail)-CHARINDEX('(', pc.FindingDetail)-1)
		ELSE NULL
	END
LEFT JOIN dbo.PatientClinical pcStartDateTime ON pc.AppointmentId = pcStartDateTime.AppointmentId 
	AND pcStartDateTime.Symptom = '/SMOKING'
	AND SUBSTRING(pcStartDateTime.FindingDetail,1, 19) = '*STARTDATE-SMOKING='
LEFT JOIN dbo.PatientClinical pcEndDateTime ON pc.AppointmentId = pcEndDateTime.AppointmentId 
	AND pcEndDateTime.Symptom = '/SMOKING'
	AND SUBSTRING(pcEndDateTime.FindingDetail,1, 17) = '*ENDDATE-SMOKING='
LEFT JOIN dbo.PatientClinical pcEveryday ON pc.AppointmentId = pcEveryDay.AppointmentId 
	AND pcEveryDay.Symptom = '/SMOKING'
	AND SUBSTRING(pcEveryDay.FindingDetail,1, 17) = '*CURRENTEVERYDAY='
LEFT JOIN dbo.PatientClinical pcSomeDay ON pc.AppointmentId = pcSomeDay.AppointmentId 
	AND pcSomeDay.Symptom = '/SMOKING'
	AND SUBSTRING(pcSomeDay.FindingDetail,1, 16) = '*CURRENTSOMEDAY='
LEFT JOIN dbo.PatientClinical pcHeavy ON pc.AppointmentId = pcHeavy.AppointmentId 
	AND pcHeavy.Symptom = '/SMOKING'
	AND SUBSTRING(pcHeavy.FindingDetail,1, 12) = '*HEAVYSMOKE='
LEFT JOIN dbo.PatientClinical pcLight ON pc.AppointmentId = pcLight.AppointmentId 
	AND pcLight.Symptom = '/SMOKING'
	AND SUBSTRING(pcLight.FindingDetail,1, 12) = '*LIGHTSMOKE='
LEFT JOIN dbo.PatientClinical pcUnknown ON pc.AppointmentId = pcUnknown.AppointmentId 
	AND pcUnknown.Symptom = '/SMOKING'
	AND SUBSTRING(pcUnknown.FindingDetail,1, 21) = '*UNKNOWNIFEVERSMOKED='
LEFT JOIN dbo.PatientClinical pcNever ON pc.AppointmentId = pcNever.AppointmentId 
	AND pcNever.Symptom = '/SMOKING'
	AND SUBSTRING(pcNever.FindingDetail,1, 7) = '*NEVER='
LEFT JOIN dbo.PatientClinical pcFormer ON pc.AppointmentId = pcFormer.AppointmentId 
	AND pcFormer.Symptom = '/SMOKING'
	AND SUBSTRING(pcFormer.FindingDetail,1, 8) = '*FORMER='
WHERE pc.Symptom = '/SMOKING'
	AND pc.FindingDetail LIKE 'TIME=%'
	AND CHARINDEX('(', pc.FindingDetail) > 0

GO

-- PatientVaccination
SELECT pimm.Id AS Id
	,CONVERT(int,pimm.PatientID) AS PatientId
	,imm.ImmNo AS VaccineId
	,NULL AS RouteOfAdministrationId
	,CASE WHEN ISNUMERIC(pimm.Dosage) = 1
		THEN CONVERT(decimal(12,2), pimm.Dosage) 
		ELSE CONVERT(decimal(12,2), NULL)
	END AS VaccineDose
	,CASE 
		WHEN pimm.Lotid = ''
			THEN NULL
		ELSE CONVERT(nvarchar(max),pimm.Lotid) END AS VaccineLotNumber
	,NULL AS InjectionSiteId
	,NULL AS HypodermicNeedleId
	,pimm.DateGiven AS AdministeredDateTime
	,CASE 
		WHEN dbo.IsNullOrEmpty(pimm.Comments) = 1
			THEN NULL
		ELSE CONVERT(nvarchar(max),pimm.Comments) END AS Comment
	,NULL AS ExternalProviderId
	,CONVERT(DATETIME, ap.AppDate, 112) AS EnteredDateTime
	,CONVERT(bit, 0) AS IsDeactivated
	,NULL AS AccuracyQualifierId
	,NULL AS RelevancyQualifierId
	,NULL AS PatientAllergenId
	,NULL AS UserId
	,NULL AS UnitOfMeasurementId
	,1 AS ClinicalDataSourceTypeId
	,pimm.EncounterID AS EncounterId
	,pimm.LotExpDate AS VaccineExpirationDateTime
	,CONVERT(datetime, NULL) AS ManufacturedDateTime
	,CONVERT(datetime, NULL) AS VaccineInformationStatementEditionDateTime
	,CONVERT(datetime, NULL) AS VaccineInformationStatementProvidedDateTime
	,CONVERT(bit, 0) AS IsNotComplete
FROM dbo.Patient_Immunizations pimm
INNER JOIN dbo.Immunizations imm ON pimm.ImmID = imm.ImmNo
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pimm.EncounterID

GO

-- ReasonForVisit
SELECT pct.Id AS Id
	,rfvc.Id AS ReasonForVisitCategoryId
	,pct.Code AS Name
	,pct.OtherLtrTrans AS SimpleDescription
	,pct.LetterTranslation AS TechnicalDescription
	,pct.Rank AS OrdinalId
	,1 AS ClinicalConditionId  --This is hardcoded temp value until we make the clinical conditions table.
FROM dbo.PracticeCodeTable pct
INNER JOIN model.ReasonForVisitCategories rfvc ON pct.AlternateCode = rfvc.Name --This is to get categoryId.
WHERE pct.ReferenceType IN ('CHIEFCOMPLAINTS', 'CHIEFCOMPLAINTSARCHIVED')

GO

-- ReasonForVisitCategory
SELECT 
	CONVERT(int,ROW_NUMBER() OVER (ORDER BY (SELECT 0))) AS Id
	,v.Name AS Name
FROM (
	SELECT 	AlternateCode AS Name
FROM dbo.PracticeCodeTable pct
WHERE pct.ReferenceType IN ('CHIEFCOMPLAINTS', 'CHIEFCOMPLAINTSARCHIVED')
GROUP BY AlternateCode

	UNION ALL

	SELECT 'FromPatientNotes' AS Name
)v
GO

-- Vaccine
SELECT imm.ImmNo AS Id
	,imm.ImmName AS Name
	,NULL AS VaccineManufacturerId
	,CONVERT(nvarchar(max),NULL) AS TradeName
	,NULL AS VaccineTypeId
	,imm.Abbreviation AS Abbreviation
	,CONVERT(bit, 0) AS IsDeactivated
FROM dbo.Immunizations imm

GO

-- EncounterClinicalInstruction
SELECT pn.NoteId AS Id
	,pn.AppointmentId AS EncounterId
	,ISNULL(pn.Note1, '') + ISNULL(pn.Note2, '') + ISNULL(pn.Note3, '') + ISNULL(pn.Note4, '') AS Value
FROM dbo.PatientNotes pn
INNER JOIN dbo.Appointments ap ON pn.AppointmentId = ap. AppointmentId
WHERE pn.NoteType = 'C'
	AND pn.NoteSystem = ''

GO

-- PatientEducation
SELECT Id AS Id
	,Rank AS OrdinalId
	,CONVERT(bit, 0) AS IsExcludedFromCommunications
	,CONVERT(bit, 0) AS IsFollowUpRequired
	,CONVERT(nvarchar(max),NULL) AS SimpleDescription
	,CONVERT(nvarchar(max),NULL) AS TechnicalDescription
	,Code AS NAME
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'PATIENTADMIN'

GO

-- PatientAllergenAllergenReactionType
SELECT 
CONVERT(INT,ROW_NUMBER() OVER (ORDER BY PatientAllergenId, AllergenReactionTypeId)) AS Id
,v.PatientAllergenId AS PatientAllergenId
,v.AllergenReactionTypeId AS AllergenReactionTypeId
FROM (
	SELECT 
	MAX(pc.ClinicalId) AS PatientAllergenId
	,art.Id AS AllergenReactionTypeId
	FROM dbo.PatientClinical pc 
	LEFT JOIN model.Allergens ar ON ar.Name = SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail))
	LEFT JOIN dbo.PatientClinical pc2 ON pc2.Symptom = '[ALLERGY REACTION'
		AND pc2.AppointmentId = pc.AppointmentId
		AND pc2.[Status] <> 'D'
		AND SUBSTRING(pc2.FindingDetail,1,CHARINDEX('-',pc2.FindingDetail)-1) = SUBSTRING(pc.FindingDetail,1,CHARINDEX('=',pc.FindingDetail)-1)
	INNER JOIN (SELECT 
	MAX(Id) AS Id,
	name,
	ordinalid,
	MAX(ClinicalQualifierId) AS ClinicalQualifierId,
	IsDeactivated
	FROM model.AllergenReactionTypes
	GROUP BY name,
	ordinalid,
	IsDeactivated) art ON art.Name = dbo.ExtractTextValue('-REACTION-',pc2.FindingDetail,'=T')
	WHERE SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail)) <> 'OTHER'
		AND pc.[Status] <> 'D'
		AND pc.Symptom = '?ALLERGY'
	GROUP BY pc.PatientId
	,CASE 
		WHEN CHARINDEX('<',pc.FindingDetail) <> 0
			THEN SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),((CHARINDEX(' <',pc.FindingDetail))-(CHARINDEX('=T',pc.FindingDetail)+LEN('=T'))))
		ELSE SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),LEN(pc.FindingDetail))
	END
	,art.Id 
) v

GO

-- LaboratoryTestOrder
SELECT
lr.ReportId AS Id
,CONVERT(BIT,0) AS IsExcludedFromCommunications
,CONVERT(BIT,0) AS IsFollowUpRequired
,0 AS OrdinalId
,CONVERT(NVARCHAR(MAX),NULL) AS SimpleDescription
,CONVERT(NVARCHAR(MAX),NULL) AS TechnicalDescription
,lt.Id AS LaboratoryTestId
FROM dbo.HL7_LabReports lr
INNER JOIN dbo.HL7_Observation o ON o.ReportId = lr.ReportId
INNER JOIN model.LaboratoryTests lt ON lt.Name = o.OrderDesc

GO

-- EncounterLaboratoryTestOrder
SELECT 
lr.ReportId AS Id
,ap.AppointmentId AS EncounterId
,CONVERT(NVARCHAR(MAX),NULL) AS Comment
,lr.ReportId AS LaboratoryTestOrderId
,NULL AS ExternalOrganizationId
,NULL AS BodyPartId
,NULL AS LateralityId
,NULL AS LaboratoryTestingPriorityId
,obd.OBXID AS PatientLaboratoryTestResultId
,NULL AS AppointmentId
FROM  dbo.HL7_Observation ob
INNER JOIN dbo.HL7_Observation_Details obd ON obd.OBRID = ob.OBRID
INNER JOIN dbo.HL7_LabReports lr ON lr.ReportID = ob.ReportID
INNER JOIN (
	SELECT 
	MAX(AppointmentId) AS AppointmentId
	,PatientId
	,AppDate
	FROM dbo.Appointments ap
	GROUP BY PatientId
	,AppDate
) ap ON ap.PatientId = lr.PatientId
	AND ap.Appdate = CONVERT(NVARCHAR(8),lr.ReportDate,112)

GO

-- VaccinationOrder
SELECT 
CONVERT(INT,ROW_NUMBER() OVER (ORDER BY imm.ImmNo)) AS Id
,CONVERT(BIT,0) AS IsExcludedFromCommunications
,CONVERT(BIT,0) AS IsFollowUpRequired
,1 AS OrdinalId
,imm.Abbreviation AS SimpleDescription
,imm.Abbreviation AS TechnicalDescription
,imm.ImmName AS Name
,imm.ImmNo AS VaccineId
FROM dbo.Immunizations imm

GO

--EncounterVaccinationOrder
---This view is not needed for the MU2 test.
SELECT 
pimm.Id AS Id
,pimm.EncounterId AS AppointmentId
,ROW_NUMBER() OVER (ORDER BY imm.ImmNo) AS VaccinationOrderId
,CONVERT(NVARCHAR(MAX),NULL) AS Comment
,NULL AS TimeQualifierId
,NULL AS RelativeTimeTypeId
,NULL AS TimeFrameId
,NULL AS TimeUnit
,pimm.Id AS PatientVaccinationId
,CONVERT(DATETIME,NULL) AS VaccineInformationStatementProvidedDateTime
,CONVERT(DATETIME,NULL) AS VaccineInformationStatementEditionDateTime
,CONVERT(DATETIME,NULL) AS VaccinationDueDateTime
,e.Id AS EncounterId
FROM dbo.Patient_Immunizations pimm
INNER JOIN model.Encounters e ON e.Id = pimm.EncounterId
LEFT JOIN dbo.Immunizations imm ON imm.ImmNo = pimm.ImmID

GO

-- PatientAllergenDetailClinicalQualifier
SELECT
CASE 
	WHEN dbo.IsNullOrEmpty(v.PatientAllergenId) = 0
		THEN model.GetPairId(1,v.PatientAllergenId,11000000) 
	ELSE CONVERT(INT,NULL) 
END AS PatientAllergenDetails_Id
,q.Id AS ClinicalQualifiers_Id
FROM (
	SELECT 
	MAX(Id) AS PatientAllergenId
	,PatientId
	FROM (
		SELECT 
		MAX(ClinicalId) AS Id
		,pc.PatientId
		,MAX(ar.Id) AS AllergenId
		FROM dbo.PatientClinical pc 
		LEFT JOIN model.Allergens ar ON ar.Name = SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail))
		WHERE SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail)) <> 'OTHER'
			AND pc.[Status] = 'A'
			AND Symptom = '?ALLERGY'
		GROUP BY pc.PatientId
		,CASE 
			WHEN CHARINDEX('<',pc.FindingDetail) <> 0
				THEN SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),((CHARINDEX(' <',pc.FindingDetail))-(CHARINDEX('=T',pc.FindingDetail)+LEN('=T'))))
			ELSE SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),LEN(pc.FindingDetail))
		END

		UNION ALL

		SELECT
		pc.ClinicalId AS Id
		,pc.PatientId
		,MAX(ar.Id) AS AllergenId
		FROM dbo.PatientClinical pc
		INNER JOIN (
		SELECT 
		SecondaryControlId AS Id
		,ControlName AS Name
		,v.Id AS ParentId
		,v.Name AS ParentName
		FROM df.SecondaryFormsControls s
		INNER JOIN (
			SELECT 
			SecondaryFormId AS Id
			,FormControlName AS Name
			FROM df.SecondaryForms
			WHERE FormId IN (
				SELECT FormId
				FROM df.SecondaryForms
				WHERE FormControlName LIKE 'ANTIBIOTICS'
			)
		) v ON v.Id = s.SecondaryFormId
		WHERE SecondaryFormId IN (
			SELECT SecondaryFormId
			FROM df.SecondaryForms
			WHERE FormId IN (
					SELECT FormId
					FROM df.SecondaryForms
					WHERE FormControlName LIKE 'ANTIBIOTICS'
				)
			)
			AND ControlType = 'B'
		)z ON z.ParentName = SUBSTRING(pc.FindingDetail,0,CHARINDEX('=T',pc.FindingDetail))
		INNER JOIN model.Allergens ar ON ar.Name = SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail))+'-Other'
		LEFT JOIN (
			SELECT 
			pc.PatientId
			,z.ParentId
			FROM dbo.PatientClinical pc 
			LEFT JOIN model.Allergens ar ON ar.Name = SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail))
			INNER JOIN (
				SELECT 
				SecondaryControlId AS Id
				,ControlName AS Name
				,v.Id AS ParentId
				,v.Name AS ParentName
				FROM df.SecondaryFormsControls s
				INNER JOIN (
					SELECT 
					SecondaryFormId AS Id
					,FormControlName AS Name
					FROM df.SecondaryForms
					WHERE FormId IN (
						SELECT FormId
						FROM df.SecondaryForms
						WHERE FormControlName LIKE 'ANTIBIOTICS'
					)
				) v ON v.Id = s.SecondaryFormId
				WHERE SecondaryFormId IN (
					SELECT SecondaryFormId
					FROM df.SecondaryForms
					WHERE FormId IN (
							SELECT FormId
							FROM df.SecondaryForms
							WHERE FormControlName LIKE 'ANTIBIOTICS'
						)
					)
					AND ControlType = 'B'
			)z ON z.Id = CASE 
				WHEN CHARINDEX('<',pc.FindingDetail) <> 0
					THEN SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),((CHARINDEX(' <',pc.FindingDetail))-(CHARINDEX('=T',pc.FindingDetail)+LEN('=T'))))
				ELSE SUBSTRING(pc.FindingDetail,CHARINDEX('=T',pc.FindingDetail)+LEN('=T'),LEN(pc.FindingDetail))
			END
			WHERE SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail)) <> 'OTHER'
				AND Symptom = '?ALLERGY'
			GROUP BY pc.PatientId
			,z.ParentId
		)y ON y.PatientId = pc.PatientId
			AND y.ParentId = z.ParentId
		WHERE pc.Symptom = '/ANY DRUG ALLERGIES' 
			AND pc.FindingDetail NOT LIKE '%NONE%'
			AND pc.ClinicalType IN ('C', 'H')
			AND pc.[Status] <> 'D'
			AND y.ParentId IS NULL
		GROUP BY pc.PatientId
		,pc.FindingDetail
		,y.ParentId
		,pc.ClinicalId

		UNION ALL

		SELECT 
		MAX(ClinicalId) AS Id
		,pc.PatientId
		,MAX(ar.Id) AS AllergenId
		FROM dbo.PatientClinical pc 
		LEFT JOIN model.Allergens ar ON ar.Name = CASE
			WHEN pc.FindingDetail LIKE '%=T2093%'
				THEN 'GLAUCOMA-Other'
			WHEN pc.FindingDetail LIKE '%=T2707%'
				THEN 'ANTIBIOTICS-Other'
			WHEN pc.FindingDetail LIKE '%=T2717%'
				THEN 'PAIN-RELIEVERS-Other'
			WHEN pc.FindingDetail LIKE '%=T2729%'
				THEN 'ANTIHISTIMINES-Other'
		END
		WHERE pc.Symptom = '?ALLERGY'
			AND SUBSTRING(pc.FindingDetail,0,CHARINDEX('=',pc.FindingDetail)) = 'OTHER'
			AND pc.[Status] = 'A'
		GROUP BY pc.PatientId
		,pc.FindingDetail
	)e 
	GROUP BY 
	PatientId
	,AllergenId
)v
INNER JOIN dbo.PatientClinical pc ON pc.ClinicalId = v.PatientAllergenId
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
LEFT JOIN dbo.PatientClinical pcSeverity ON pc.AppointmentId = pcSeverity.AppointmentId
	AND pcSeverity.ClinicalType IN ('C','H')
	AND pcSeverity.Symptom = ']ALLERGY SEVERITY'
	AND pcSeverity.[Status] = 'A'
	AND SUBSTRING(pcSeverity.FindingDetail, 1, CHARINDEX('-', pcSeverity.FindingDetail) - 1) = SUBSTRING(pc.FindingDetail, 1, CHARINDEX('=', pc.FindingDetail) - 1)
INNER JOIN model.ClinicalQualifiers q ON REPLACE(q.Name,' ','') = (SUBSTRING(
	pcSeverity.FindingDetail
	,CHARINDEX(SUBSTRING(pcSeverity.FindingDetail, 1, CHARINDEX('-', pcSeverity.FindingDetail) - 1)+'-',pcSeverity.FindingDetail)+LEN(SUBSTRING(pcSeverity.FindingDetail, 1, CHARINDEX('-', pcSeverity.FindingDetail) - 1)+'-')
	,CHARINDEX('-DALG=T',pcSeverity.FindingDetail)-9
	))
WHERE pc.[Status] = 'A'
GROUP BY v.PatientAllergenId
,ap.AppDate
,q.Id

GO

-- EncounterSubsequentVisitOrder
-- novalidation
SELECT 
pc.ClinicalId AS Id
,pc.AppointmentId AS EncounterId
,CONVERT(NVARCHAR(MAX),NULL) AS Comment
,at.AppTypeId AS AppointmentTypeId
,NULL AS ServiceLocationId
,NULL AS UserId
,CASE 
	WHEN ISNUMERIC([IO.Practiceware.SqlClr].[SearchRegexPattern](FindingDetail,'\d{1,2} ')) =1
		THEN CONVERT(INT, [IO.Practiceware.SqlClr].[SearchRegexPattern](FindingDetail,'\d{1,2} '))
	ELSE NULL 
END AS TimeUnit
,CASE [IO.Practiceware.SqlClr].[SearchRegexPattern](FindingDetail,'(?<=\-1\/[\d]*?\s)(?<Unit>(DAYS|HOURS|MINUTES|WEEKS|MONTHS|YEARS))')
	WHEN 'MINUTES'
		THEN 1
	WHEN 'HOURS'
		THEN 2
	WHEN 'DAYS'
		THEN 3
	WHEN 'WEEKS'
		THEN 4
	WHEN 'MONTHS'
		THEN 5
	WHEN 'YEARS'
		THEN 6		
	ELSE NULL 
END AS TimeFrameId 
,NULL AS TimeQualifierId
,pct.Id AS ReasonForVisitId 
FROM dbo.PatientClinical pc
LEFT JOIN dbo.AppointmentType at ON at.AppointmentType = [IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=SCHEDULE APPOINTMENT\-2\/)(.*?)(?=\-)')
LEFT JOIN dbo.PracticeCodeTable pct ON pct.Code = [IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=<\^)(.*?)(?=\-5|\()')
WHERE pc.FindingDetail LIKE 'SCHEDULE APPOINTMENT%'
AND pc.[Status] = 'A'
	
GO

-- EncounterMedicationOrder
-- novalidation
SELECT pc.ClinicalId AS Id
	,CONVERT(int, NULL) AS PharmacistComment
	,pc.AppointmentId AS EncounterId
	,CASE 
		WHEN SUBSTRING(ImageDescriptor, 1, 1) = '!'
			AND LEN(ImageDescriptor) = 11
			THEN 1
		WHEN SUBSTRING(ImageDescriptor, 1, 1) = 'D'
			THEN 5
		WHEN SUBSTRING(ImageDescriptor, 1, 1) = 'C'
			THEN 3
		WHEN SUBSTRING(ImageDescriptor, 1, 1) = 'R'
			THEN 4
		ELSE 2
		END AS MedicationOrderActionId
	,CONVERT(bigint, ClinicalId) AS PatientMedicationDetailId
FROM dbo.PatientClinical pc
WHERE ClinicalType = 'A'
	AND pc.STATUS = 'A'
	AND pc.FindingDetail LIKE 'RX-%P-7%'

GO

-- ClinicalQualifierCategory
SELECT
MAX(pct.Id) AS Id
,CASE 
	WHEN SUBSTRING(ReferenceType,LEN('QUANTIFIERS')+1,LEN(ReferenceType)) = 'DESCRIPTORS'
		THEN CONVERT(BIT,0) 
	WHEN ReferenceType = 'QUANTIFIERS'
		THEN CONVERT(BIT,0) 
	ELSE CONVERT(BIT,1) 
END AS IsSingleUse
,ReferenceType AS Name
,1 AS OrdinalId
FROM PracticeCodeTable pct
WHERE ReferenceType LIKE '%QUANTIFIERS%' AND (AlternateCode <> '' OR LetterTranslation <> '' OR OtherLtrTrans <> '')
GROUP BY ReferenceType

UNION ALL

SELECT
MAX(pct.Id) AS Id
,CONVERT(BIT,1) AS IsSingleUse
,SUBSTRING(ReferenceType,19,LEN(ReferenceType)) AS Name
,1 AS OrdinalId
FROM PracticeCodeTable pct
WHERE ReferenceType LIKE 'INSERTEDQUANTIFIERS%' 
GROUP BY ReferenceType

GO

-- ClinicalQualifier
SELECT 
pct.Id AS Id
,Code AS Name
,q.Id AS ClinicalQualifierCategoryId
,pct.OtherLtrTrans AS SimpleDescription
,pct.LetterTranslation AS TechnicalDescription
,CONVERT(BIT,0) AS IsDeactivated
,1 AS OrdinalId
FROM PracticeCodeTable pct
INNER JOIN model.ClinicalQualifierCategories q ON q.Name = pct.ReferenceType
WHERE pct.ReferenceType LIKE '%QUANTIFIERS%' AND (AlternateCode <> '' OR LetterTranslation <> '' OR OtherLtrTrans <> '')

GO

-- ClinicalCondition
SELECT 
d.PrimaryDrillId AS Id
,d.ReviewSystemLingo AS Name
,CONVERT(BIT,0) AS IsSocial
,NULL AS OrganSubStructureId
,CASE 
	WHEN d.DiagnosisRank = 113 THEN CONVERT(BIT,1)
		ELSE CONVERT(BIT,0)
END AS IsDeactivated
,1 AS OrdinalId
,o.Id AS OrganStructureId
FROM dm.PrimaryDiagnosisTable d
LEFT JOIN model.OrganStructures o ON d.MedicalSystem = 
	CASE
		WHEN o.Name = 'External' THEN 'A'
		WHEN o.Name = 'Pupils' THEN 'B'
		WHEN o.Name = 'Extraocular Motility' THEN 'C'
		WHEN o.Name = 'Visual Field' THEN 'D'
		WHEN o.Name = 'Lids/Lacrimal' THEN 'E'
		WHEN o.Name = 'Conjunctiva/Sclera' THEN 'F'
		WHEN o.Name = 'Cornea' THEN 'G'
		WHEN o.Name = 'Anterior Chamber' THEN 'H'
		WHEN o.Name = 'Iris' THEN 'I'
		WHEN o.Name = 'Lens' THEN 'J'
		WHEN o.Name = 'Vitreous' THEN 'K'
		WHEN o.Name = 'Optic Nerve' THEN 'L'
		WHEN o.Name = 'Blood Vessels' THEN 'M'
		WHEN o.Name = 'Macula' THEN 'N'
		WHEN o.Name = 'Retina/Choroid' THEN 'O'
		WHEN o.Name = 'Peripheral Retina' THEN 'P'
		WHEN o.Name = 'Visual Defects' THEN 'R'
		END
WHERE dbo.IsNullOrEmpty(d.ReviewSystemLingo) = 0
	AND dbo.IsNullOrEmpty(MedicalSystem) = 0

UNION ALL

SELECT 
model.GetPairId(1,MAX(d.PrimaryDrillId),110000000) AS Id
,MAX(d.ReviewSystemLingo) AS Name
,CONVERT(BIT,0) AS IsSocial
,NULL AS OrganSubStructureId
,CASE 
	WHEN d.DiagnosisRank = 113 THEN CONVERT(BIT,1)
		ELSE CONVERT(BIT,0)
END AS IsDeactivated
,1 AS OrdinalId
,NULL AS OrganStructureId
FROM dm.PrimaryDiagnosisTable d
WHERE dbo.IsNullOrEmpty(d.ReviewSystemLingo) = 0
GROUP BY d.DiagnosisNextLevel
	,d.DiagnosisRank

GO

-- PatientDiagnosisDetailQualifier
SELECT pc.ClinicalId AS Id
,ca.Id AS ClinicalAttributeId
,1 AS OrdinalId
,pc.ClinicalId AS PatientDiagnosisDetailId
,q.Id AS ClinicalQualifierId
FROM dbo.PatientClinical pc
INNER JOIN model.ClinicalAttributes ca ON ca.Name='HAS INTERPRETATION'
INNER JOIN model.ClinicalQualifiers q ON q.Name=SUBSTRING(Symptom,CHARINDEX('[',Symptom)+1,CHARINDEX(']',Symptom)-CHARINDEX('[',Symptom)-1)
WHERE pc.ClinicalType IN ('Q','U')
AND pc.Symptom LIKE '%]%'

GO

-- PatientDiagnosis
WITH CTE1 AS (
SELECT 
MAX(d.PrimaryDrillId) AS ClinicalConditionId
,d.DiagnosisNextLevel AS LegacyDiagnosisNextLevel
,d.MedicalSystem AS LegacyMedicalSystem
,CASE 
	WHEN d.DiagnosisRank = 113 THEN CONVERT(BIT,1)
		ELSE CONVERT(BIT,0)
END AS IsDeactivated
FROM dm.PrimaryDiagnosisTable d
WHERE dbo.IsNullOrEmpty(d.ReviewSystemLingo) = 0
	AND dbo.IsNullOrEmpty(MedicalSystem) = 0
GROUP BY d.DiagnosisNextLevel
,d.MedicalSystem
,CASE 
	WHEN d.DiagnosisRank = 113 THEN CONVERT(BIT,1)
		ELSE CONVERT(BIT,0)
END

UNION ALL

SELECT 
model.GetPairId(1,MAX(d.PrimaryDrillId),110000000) AS ClinicalConditionId
,d.DiagnosisNextLevel AS LegacyDiagnosisNextLevel
,NULL AS LegacyMedicalSystem
,CASE 
	WHEN d.DiagnosisRank = 113 THEN CONVERT(BIT,1)
		ELSE CONVERT(BIT,0)
END AS IsDeactivated
FROM dm.PrimaryDiagnosisTable d
WHERE dbo.IsNullOrEmpty(d.ReviewSystemLingo) = 0
GROUP BY d.DiagnosisNextLevel
,CASE 
	WHEN d.DiagnosisRank = 113
		THEN CONVERT(BIT,1)
	ELSE CONVERT(BIT,0)
END
)
,CTE2 AS (
SELECT
pc.PatientId
,c.LegacyDiagnosisNextLevel
,CASE
	WHEN pc.EyeContext = 'OD' 
		THEN 1
	WHEN pc.EyeContext = 'OS' 
		THEN 2
	ELSE NULL
END AS LateralityId
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId 
INNER JOIN CTE1 c ON c.LegacyDiagnosisNextLevel = pc.FindingDetail
	AND c.LegacyMedicalSystem = pc.ImageDescriptor
	AND dbo.IsNullOrEmpty(c.LegacyMedicalSystem) = 0
WHERE pc.ClinicalType = 'Q'
	AND pc.[Status] = 'A'
GROUP BY pc.PatientId
,c.LegacyDiagnosisNextLevel
,pc.EyeContext
,pc.LegacyClinicalDataSourceTypeId
)
SELECT
MIN(pc.ClinicalId) AS Id
,pc.PatientId
,c.ClinicalConditionId
,CASE
	WHEN pc.EyeContext = 'OD' THEN 1
	WHEN pc.EyeContext = 'OS' THEN 2
	ELSE NULL
END AS LateralityId
,CASE 
	WHEN pc.LegacyClinicalDataSourceTypeId IS NULL 
			THEN 1 
	ELSE pc.LegacyClinicalDataSourceTypeId 
END AS ClinicalDataSourceTypeId
,NULL AS ClinicalAttributeId
,NULL AS ToPatientDiagnosisId
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId 
INNER JOIN CTE1 c ON c.LegacyDiagnosisNextLevel = pc.FindingDetail
	AND dbo.IsNullOrEmpty(c.LegacyMedicalSystem) = 0
	AND c.LegacyMedicalSystem = pc.ImageDescriptor
WHERE pc.ClinicalType = 'Q'
	AND pc.[Status] = 'A'
GROUP BY pc.PatientId
,c.ClinicalConditionId
,pc.EyeContext
,pc.LegacyClinicalDataSourceTypeId

UNION ALL

SELECT 
MIN(pc.ClinicalId) AS Id
,pc.PatientId
,c.ClinicalConditionId
,CASE
	WHEN pc.EyeContext = 'OD' 
		THEN 1
	WHEN pc.EyeContext = 'OS' 
		THEN 2
	ELSE NULL
END AS LateralityId
,CASE 
	WHEN pc.LegacyClinicalDataSourceTypeId IS NULL 
			THEN 1 
	ELSE pc.LegacyClinicalDataSourceTypeId 
END AS ClinicalDataSourceTypeId
,NULL AS ClinicalAttributeId
,NULL AS ToPatientDiagnosisId
FROM dbo.PatientClinical pc
INNER JOIN CTE1 c ON c.LegacyDiagnosisNextLevel = pc.FindingDetail
	AND dbo.IsNullOrEmpty(c.LegacyMedicalSystem) = 1
LEFT JOIN CTE2 ON CTE2.PatientId = pc.PatientId
	AND CTE2.LateralityId = CASE
		WHEN pc.EyeContext = 'OD' 
			THEN 1
		WHEN pc.EyeContext = 'OS' 
			THEN 2
		ELSE NULL
	END
	AND CTE2.LegacyDiagnosisNextLevel = pc.FindingDetail
WHERE pc.ClinicalType = 'U'
	AND pc.[Status] = 'A'
	AND CTE2.PatientId IS NULL
	AND dbo.IsNullOrEmpty(pc.ImageDescriptor) = 1
GROUP BY pc.PatientId
,c.ClinicalConditionId
,pc.EyeContext
,pc.LegacyClinicalDataSourceTypeId

GO

 -- PatientExamPerformed
SELECT 
     MAX(ClinicalId) AS Id
     ,pc.LegacyClinicalDataSourceTypeId AS ClinicalDataSourceTypeId
     ,cp.Id AS ClinicalProcedureId
     ,ap.EncounterId AS EncounterId
     ,pc.PatientId AS PatientId
     ,CASE 
		WHEN ap.AppTime >=0 
			THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
		ELSE CONVERT(DATETIME, ap.AppDate) 
	 END AS PerformedDateTime
     ,NULL AS TimeQualifierId
     , CONVERT(BIT,1) AS IsBillable
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
INNER JOIN model.ClinicalProcedures cp ON '/' + cp.Name = pc.Symptom
WHERE pc.ClinicalType = 'F'
     AND pc.[Status] = 'A'
     AND cp.ClinicalProcedureCategoryId = 4 --Exam
GROUP BY cp.Id
     ,ap.EncounterId
     ,pc.PatientId
     ,CASE 
		WHEN ap.AppTime >=0 
			THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
		ELSE CONVERT(DATETIME, ap.AppDate) 
	 END 
     ,pc.LegacyClinicalDataSourceTypeId
 
GO

-- PatientInterventionPerformed
SELECT 
     MAX(ClinicalId) AS Id
     ,pc.LegacyClinicalDataSourceTypeId AS ClinicalDataSourceTypeId
     ,cp.Id AS ClinicalProcedureId
     ,ap.EncounterId AS EncounterId
     ,pc.PatientId AS PatientId
     ,CASE 
		WHEN ap.AppTime >=0 
			THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
		ELSE CONVERT(DATETIME, ap.AppDate) 
	 END AS PerformedDateTime
     ,NULL AS TimeQualifierId
     , CONVERT(BIT,1) AS IsBillable
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
INNER JOIN model.ClinicalProcedures cp ON '/' + cp.Name = pc.Symptom
WHERE pc.ClinicalType = 'F'
     AND pc.[Status] = 'A'
     AND cp.ClinicalProcedureCategoryId = 1 --Act
GROUP BY cp.Id
     ,ap.EncounterId
     ,pc.PatientId
     ,CASE 
		WHEN ap.AppTime >=0 
			THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
		ELSE CONVERT(DATETIME, ap.AppDate) 
	 END 
	 ,pc.LegacyClinicalDataSourceTypeId 

GO

-- TreatmentGoalAndInstruction

SELECT MIN(pn.NoteId) AS Id
   , 1 AS TreatmentGoalId
   , SUBSTRING(pn.NoteSystem,CHARINDEX('-',pn.NoteSystem)+1,LEN(pn.NoteSystem))+'-'+SUBSTRING(pn.Note1,1,20) AS ShortName
   , pn.Note1+pn.Note2+pn.Note3+pn.Note4 AS Value
   , 1 AS OrdinalId
FROM dbo.PatientNotes pn
WHERE pn.NoteType='C'
   AND SUBSTRING(pn.NoteSystem,1,5)='IMPR-'
   AND LEN(pn.Note1)>1
GROUP BY pn.Note1,pn.Note2,pn.Note3,pn.Note4, pn.NoteSystem

GO

-- PatientDiagnosisDetailAxisQualifier

SELECT ROW_NUMBER() OVER (ORDER BY v.PatientDiagnosisDetailId) AS Id, * FROM (
SELECT pdd.Id AS PatientDiagnosisDetailId
, CASE
WHEN pc.Symptom LIKE '%!%'
THEN 1
WHEN pc.Symptom LIKE '%#%' 
THEN 2
WHEN pc.Symptom LIKE '%[%]%' 
THEN 3
WHEN pc.Symptom LIKE '%&%'
THEN 4
END AS AxisQualifierId
FROM model.PatientDiagnosisDetails pdd
INNER JOIN PatientClinical pc ON pc.ClinicalId = pdd.LegacyClinicalId
WHERE pc.ClinicalType IN ('Q', 'U')
AND (pc.Symptom LIKE '%!%' OR pc.Symptom LIKE '%#%' OR pc.Symptom LIKE '%&%' OR pc.Symptom LIKE '%[%]%')
) v

GO 

-- EncounterProcedureOrder
WITH ScheduleSurgery AS (
SELECT
pc.ClinicalId
,ap.EncounterId
,pc.PatientId
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=SCHEDULE\sSURGERY\-3\/)(.*?)(?=(\s\(O[D|S|U]\)\-F\/\-\d\/|\-F\/\-\d\/))') AS [Procedure]
,CASE 
	WHEN CHARINDEX('-F/-3/- ',pc.FindingDetail) > 0
		THEN [IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=\-1\/)(.*?)(?=\-7\/)')
	ELSE SUBSTRING(pc.FindingDetail,CHARINDEX('-7/',pc.FindingDetail) + 3,(LEN(pc.FindingDetail) - CHARINDEX('-7/',pc.FindingDetail)) + 3)
END AS ServiceLocationIdDoctorId
,CASE WHEN SUBSTRING([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=\-F\/\-\d\/(\-\s|))(.*?)(?=\-\d\/)')
	,1
	,1) = '-'
		THEN SUBSTRING([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=\-F\/\-\d\/(\-\s|))(.*?)(?=\-\d\/)')
		,2
		,LEN([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=\-F\/\-\d\/(\-\s|))(.*?)(?=\-\d\/)')))
	ELSE [IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=\-F\/\-\d\/(\-\s|))(.*?)(?=\-\d\/)')
END AS RelativeTimeType
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=\<)(.*?)(?=\>)') AS Comment
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=\s\()(.*?)(?=\)\-F\/)') AS EyeContext
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
WHERE pc.FindingDetail LIKE 'SCHEDULE SURGERY-3/%'
	AND SUBSTRING(pc.FindingDetail,20,12) <> 'NOT SELECTED' 
	AND pc.[Status] = 'A'
)
,TimeFrames AS (
SELECT  1 AS [Id], 'Minutes' AS [Name] UNION ALL
SELECT  2 AS [Id], 'Hours' AS [Name] UNION ALL
SELECT  3 AS [Id], 'Days' AS [Name] UNION ALL
SELECT  4 AS [Id], 'Weeks' AS [Name] UNION ALL
SELECT  5 AS [Id], 'Months' AS [Name] UNION ALL
SELECT  6 AS [Id], 'Years' AS [Name]
)
,OfficeProcedures AS (
SELECT
pc.ClinicalId
,ap.EncounterId
,pc.PatientId
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[O|0]\/)(.*?)(?=\-F\/\-[O|1]\/)') AS [Procedure]
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[0|O]\/(.)*?\-F\/\-[1|O]\/)(O[D|S|U])(?=(\s|))') AS EyeContext
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[0|O]\/(.)*?\-F\/\-[1|O]\/(O[D|S|U]\s|\-\s))(ASAP\sOUT\sOF\sOFFICE|AS\sSOON\sAS\sCONVENIENT|AT\sA\sCONVENIENT\sTIME\s|NEXT\sAVAILABLE\sDATE|NEXTVISIT|STAT|PRN|THIS\sVISIT|ONGOING|)') AS RelativeTimeType
,CONVERT(DECIMAL(12,2),CASE WHEN dbo.IsNullOrEmpty([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[0|O]\/(.)*?\-F\/\-[1|O]\/)(O[D|S|U])(?=\s)')) = 0
	THEN CASE WHEN ISNUMERIC([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[0|O]\/(.)*?\-F\/\-[1|O]\/O[D|S|U]\s)(.*?)(?=\s)')) = 1
		THEN CONVERT(DECIMAL(12,2), [IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[0|O]\/(.)*?\-F\/\-[1|O]\/O[D|S|U]\s)(.*?)(?=\s)'))
		ELSE 0
	END
	ELSE CAST(0 AS DECIMAL(12,2))
END) AS TimeUnit
,CASE WHEN dbo.IsNullOrEmpty(CASE WHEN dbo.IsNullOrEmpty([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[0|O]\/(.)*?\-F\/\-[1|O]\/)(O[D|S|U])(?=\s)')) = 0
	THEN CASE WHEN ISNUMERIC([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[0|O]\/(.)*?\-F\/\-[1|O]\/O[D|S|U]\s)(.*?)(?=\s)')) = 1
		THEN [IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[0|O]\/(.)*?\-F\/\-[1|O]\/O[D|S|U]\s)(.*?)(?=\s)')
	END
END) = 0 THEN [IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[0|O]\/(.)*?\-F\/\-[1|O]\/O[D|S|U]\s(\d*)\s)(WEEKS)|(MONTHS)|(YEARS)|(MINUTES)|(HOURS)|(DAYS)')
END AS TimeFrame
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=[^\s]<)(.*?)(?=\>)') AS Comment
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=\s<\^)(.*?)(?=\>)') AS ReasonForProcedure
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=\s<[^\^])(.*?)(?=\>)') AS Doctor
FROM dbo.PatientClinical pc 
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
WHERE (pc.FindingDetail LIKE 'OFFICE PROCEDURES-O%' OR pc.FindingDetail LIKE 'OFFICE PROCEDURES-0%')
	AND pc.[Status] = 'A'
)
,ServiceLocations AS (
SELECT 
v.PracticeId AS Id
,v.Name AS [Name]
FROM (
SELECT pn.PracticeId AS PracticeId
	,PracticeName AS NAME
FROM dbo.PracticeName pn
INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE'
	AND SUBSTRING(pct.Code, 1, 2) = '11'
LEFT JOIN dbo.Resources re ON re.PracticeId = pn.PracticeId
	AND re.ResourceType = 'R'
	AND re.ServiceCode = '02'
	AND re.Billable = 'Y'
	AND pic.FieldValue = 'T'
	AND pn.LocationReference <> ''
LEFT JOIN dbo.PracticeCodeTable pctRE ON pctRE.ReferenceType = 'PLACEOFSERVICE'
	AND SUBSTRING(pctRE.Code, 1, 2) = re.PlaceOfService
WHERE pn.PracticeType = 'P'
GROUP BY pn.PracticeId
	,pct.Id
	,LocationReference
	,pic.FieldValue
	,re.Billable
	,re.ServiceCode
	,pctRe.Id
	,PracticeName
	,pn.HexColor
) AS v
		
UNION ALL
		
SELECT (110000000 * 1 + re.ResourceId) AS Id
,re.ResourceDescription AS [Name]
FROM dbo.Resources re
LEFT JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'PLACEOFSERVICE'
AND SUBSTRING(pct.Code, 1, 2) = re.PlaceOfService
WHERE re.ResourceType = 'R'
AND re.ServiceCode = '02'
)
,Users AS (
SELECT 
re.ResourceId AS Id
,re.ResourceName AS DisplayName
FROM dbo.Resources re
WHERE ResourceType <> 'R' AND ResourceId > 0
	AND re.ResourceType IN ('D', 'Q', 'Z', 'Y')
)
,ProcedurePerformed AS (
SELECT 
MAX(ClinicalId) AS Id
,cp.Id AS ClinicalProcedureId
,pc.PatientId AS PatientId
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
INNER JOIN model.ClinicalProcedures cp ON '/' + cp.Name = pc.Symptom
WHERE pc.ClinicalType = 'F'
	AND pc.[Status] = 'A'
	AND cp.ClinicalProcedureCategoryId = 3
GROUP BY cp.Id
,pc.PatientId
)
,ProcedurePerformedWithDate AS (
SELECT 
p.Id
,p.ClinicalProcedureId
,p.PatientId
,CASE 
	WHEN ap.AppTime >=0 
		THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
	ELSE CONVERT(DATETIME, ap.AppDate) 
END AS PerformedDateTime
FROM ProcedurePerformed p
JOIN dbo.PatientClinical pc ON pc.ClinicalId = p.Id
JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
)
SELECT 
c.ClinicalId AS [Id]
,c.[EncounterId]
,CASE WHEN dbo.IsNullOrEmpty(c.Comment) = 0 THEN c.Comment END AS [Comment]
,c.ClinicalId AS [ProcedureOrderId]
,CONVERT(DECIMAL(12,2),CASE WHEN dbo.IsNullOrEmpty([IO.Practiceware.SqlClr].[SearchRegexPattern](LTRIM(c.RelativeTimeType),'(\d*?)(?=\s)')) = 0
	THEN [IO.Practiceware.SqlClr].[SearchRegexPattern](LTRIM(c.RelativeTimeType),'(\d*?)(?=\s)')
	ELSE CAST(0 AS DECIMAL(12,2))
END) AS [TimeUnit]
,CASE WHEN dbo.IsNullOrEmpty([IO.Practiceware.SqlClr].[SearchRegexPattern](LTRIM(c.RelativeTimeType),'(\d*?)(?=\s)')) = 0
	THEN CASE WHEN dbo.IsNullOrEmpty([IO.Practiceware.SqlClr].[SearchRegexPattern](c.RelativeTimeType,'(WEEKS)|(YEARS)|(MONTHS)|(MINUTES)|(HOURS)|(DAYS)')) = 0
		THEN t.Id
	END
END AS [TimeFrameId]
,NULL AS [TimeQualifierId]
,s.Id [ServiceLocationId]
,u.Id AS [DoctorId]
,rtt.Id AS [RelativeTimeTypeId]
,NULL AS [BodyPartId]
,CASE c.EyeContext
	WHEN 'OD'
		THEN 1
	WHEN 'OS'
		THEN 2
	WHEN 'OU'
		THEN 3
END AS [LateralityId]
,p.Id AS [PatientProcedurePerformedId]
,c.EncounterId AS [AppointmentId]
FROM ScheduleSurgery c
LEFT JOIN TimeFrames t ON t.Name = [IO.Practiceware.SqlClr].[SearchRegexPattern](c.RelativeTimeType,'(WEEKS)|(YEARS)|(MONTHS)|(MINUTES)|(HOURS)|(DAYS)')
LEFT JOIN ServiceLocations s ON s.Name = CASE WHEN CHARINDEX('(',c.ServiceLocationIdDoctorId) > 0 THEN [IO.Practiceware.SqlClr].[SearchRegexPattern](LTRIM(RTRIM(ServiceLocationIdDoctorId)),'(.*?)(?=\s\()') ELSE LTRIM(RTRIM(ServiceLocationIdDoctorId)) END
LEFT JOIN Users u ON u.DisplayName = [IO.Practiceware.SqlClr].[SearchRegexPattern](ServiceLocationIdDoctorId,'(?<=\()(.*?)(?=\))')
LEFT JOIN model.RelativeTimeTypes rtt ON rtt.Name = CASE WHEN CHARINDEX('<',c.RelativeTimeType) > 0 THEN [IO.Practiceware.SqlClr].[SearchRegexPattern](LTRIM(RTRIM(c.RelativeTimeType)),'(.*?)(?=\<)') ELSE LTRIM(RTRIM(c.RelativeTimeType)) END
LEFT JOIN model.ClinicalProcedures cp ON cp.Name = c.[Procedure]
	AND ClinicalProcedureCategoryId = 3
LEFT JOIN dbo.Appointments ap ON ap.EncounterId = c.EncounterId
LEFT JOIN ProcedurePerformedWithDate p ON p.PatientId = c.PatientId
	AND p.ClinicalProcedureId = cp.Id
	AND p.PerformedDateTime >= 
	CASE 
		WHEN ap.AppTime >=0 
			THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
		ELSE CONVERT(DATETIME, ap.AppDate) 
	END 
	
UNION ALL
	
SELECT
o.ClinicalId AS [Id]
,o.EncounterId AS [EncounterId]
,CASE WHEN dbo.IsNullOrEmpty(o.Comment) = 0 THEN o.Comment END AS [Comment]
,o.ClinicalId AS [ProcedureOrderId]
,CONVERT(DECIMAL(12,2),TimeUnit) AS [TimeUnit]
,t.Id AS [TimeFrameId]
,NULL AS [TimeQualifierId]
,NULL AS [ServiceLocationId]
,u.Id AS [DoctorId]
,rtt.Id AS [RelativeTimeTypeId]
,NULL AS [BodyPartId]
,CASE o.EyeContext
	WHEN 'OD'
		THEN 1
	WHEN 'OS'
		THEN 2
	WHEN 'OU'
		THEN 3
END AS [LateralityId]
,p.Id AS [PatientProcedurePerformedId]
,o.EncounterId AS [AppointmentId]
FROM OfficeProcedures o
LEFT JOIN TimeFrames t ON t.Name = o.TimeFrame
LEFT JOIN Users u ON u.DisplayName = o.Doctor
LEFT JOIN model.RelativeTimeTypes rtt ON rtt.Name = o.RelativeTimeType
LEFT JOIN model.ClinicalProcedures cp ON cp.Name = o.[Procedure]
	AND ClinicalProcedureCategoryId = 3
LEFT JOIN dbo.Appointments ap ON ap.EncounterId = o.EncounterId
LEFT JOIN ProcedurePerformedWithDate p ON p.PatientId = o.PatientId
	AND p.ClinicalProcedureId = cp.Id
	AND p.PerformedDateTime >= 
	CASE 
		WHEN ap.AppTime >=0 
			THEN DATEADD(mi, ap.AppTime, CONVERT(DATETIME, ap.AppDate)) 
		ELSE CONVERT(DATETIME, ap.AppDate) 
	END 

GO

-- ProcedureOrder
WITH ScheduleSurgery AS (
SELECT
pc.ClinicalId
,ap.EncounterId
,ap.AppTypeId AS AppointmentTypeId
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=SCHEDULE\sSURGERY\-3\/)(.*?)(?=(\s\(O[D|S|U]\)\-F\/\-\d\/|\-F\/\-\d\/))') AS [Procedure]
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=\<)(.*?)(?=\>)') AS Comment
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
WHERE pc.FindingDetail LIKE 'SCHEDULE SURGERY-3/%' 
	AND SUBSTRING(pc.FindingDetail,20,12) <> 'NOT SELECTED'
	AND pc.[Status] = 'A'
)
,OfficeProcedures AS (
SELECT
pc.ClinicalId
,ap.EncounterId
,ap.AppTypeId AS AppointmentTypeId
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[O|0]\/)(.*?)(?=\-F\/\-[O|1]\/)') AS [Procedure]
,[IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=[^\s]<)(.*?)(?=\>)') AS Comment
,CASE WHEN dbo.IsNullOrEmpty([IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=^OFFICE\sPROCEDURES\-[0|O]\/(.)*?\-F\/\-[1|O]\/(O[D|S|U]\s|\-\s))(ASAP\sOUT\sOF\sOFFICE|AS\sSOON\sAS\sCONVENIENT|AT\sA\sCONVENIENT\sTIME\s|NEXT\sAVAILABLE\sDATE|NEXTVISIT|PRN)')) = 0 THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END AS IsFollowUpRequired
FROM dbo.PatientClinical pc 
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
WHERE (pc.FindingDetail LIKE 'OFFICE PROCEDURES-O%' OR pc.FindingDetail LIKE 'OFFICE PROCEDURES-0%')
	AND pc.[Status] = 'A'
)
SELECT 
s.ClinicalId AS [Id]
,1 AS [OrdinalId]
,CONVERT(BIT,0) AS [IsExcludedFromCommunications]
,CONVERT(BIT,1) AS [IsFollowUpRequired]
,CONVERT(BIT,1) AS [IsSurgeryManagementRequired]
,CASE WHEN dbo.IsNullOrEmpty(s.Comment) = 0 THEN s.Comment END AS [SimpleDescription]
,CONVERT(NVARCHAR(MAX),NULL) AS [TechnicalDescription]
,s.AppointmentTypeId
,(SELECT TOP 1 cp.Id FROM model.ClinicalProcedures cp WHERE cp.Name = s.[Procedure] ORDER BY ClinicalProcedureCategoryId ASC) AS [ClinicalProcedureId]
FROM ScheduleSurgery s

UNION ALL

SELECT 
o.ClinicalId AS [Id]
,1 AS [OrdinalId]
,CONVERT(BIT,0) AS [IsExcludedFromCommunications]
,o.IsFollowUpRequired AS [IsFollowUpRequired]
,CONVERT(BIT,0) AS [IsSurgeryManagementRequired]
,CASE WHEN dbo.IsNullOrEmpty(o.Comment) = 0 THEN o.Comment END AS [SimpleDescription]
,CONVERT(NVARCHAR(MAX),NULL) AS [TechnicalDescription]
,o.AppointmentTypeId
,(SELECT TOP 1 cp.Id FROM model.ClinicalProcedures cp WHERE cp.Name = o.[Procedure] ORDER BY ClinicalProcedureCategoryId DESC) AS [ClinicalProcedureId]
FROM OfficeProcedures o

GO
