﻿--Must start with CREATE FUNCTION(paramaters) with the paramaters on the same line, and must end with the word go on its own line.

CREATE FUNCTION model.GetMasterFeeSchedules ()
RETURNS @temp TABLE (
	Id int,
	NAME nvarchar(255)
	)
	WITH SCHEMABINDING
AS
BEGIN
	INSERT INTO @temp
	SELECT min(pfs.ServiceId) AS Id,
		CONVERT(NVARCHAR(32),CASE WHEN i.Name <> '' AND i.Name IS NOT NULL THEN i.Name ELSE '' END) + ' ' + CONVERT(NVARCHAR(16),CASE WHEN pt.Name <> '' AND pt.Name IS NOT NULL THEN pt.Name ELSE '' END) + substring(StartDate, 1, 6) + substring(enddate, 5, 2) AS NAME
	FROM dbo.PracticeFeeSchedules pfs
	JOIN model.Insurers i ON i.Id = pfs.PlanId
	LEFT JOIN model.InsurerPlanTypes pt ON pt.Id = i.InsurerPlanTypeId
	GROUP BY i.Name,
		pfs.StartDate,
		pfs.EndDate,
		pt.Name

	RETURN
END

GO

CREATE FUNCTION model.GetScheduleEntries(@Position AS int)
RETURNS @Results TABLE ( Id int, EndDateTime datetime, EquipmentId int, RoomId int, ScheduleEntryAvailabilityTypeId int, ServiceLocationId int, StartDateTime datetime, UserId int) 
WITH SCHEMABINDING
AS 
BEGIN

INSERT INTO @Results   
SELECT model.GetPairId(@Position, pc.CalendarId, @CalendarMax) as Id, 
	CONVERT(DATETIME, CalendarDate + ' ' + SUBSTRING(CalendarEndTime, @Position * 8 + 1, 8)) AS EndDateTime,
	NULL as EquipmentId,
	CASE
		WHEN re.ResourceType = 'R'
			THEN CalendarResourceId
		ELSE NULL
		END AS RoomId,
	CASE
		WHEN RIGHT(CalendarPurpose, 2) = 'NT' 
			THEN 2
		WHEN RIGHT(CalendarPurpose, 2) IN ('TT', 'TN') 
			THEN 3
		ELSE 1
		END AS ScheduleEntryAvailibilityTypeId,
	CASE 
		WHEN pnServLoc.PracticeId IS NULL
			THEN model.GetPairId(1, reServLoc.ResourceId, @ServiceLocationMax)			
		ELSE
			model.GetPairId(0, pnServLoc.PracticeId, @ServiceLocationMax)
		END AS ServiceLocationId,
	CONVERT(DATETIME, CalendarDate + ' ' + SUBSTRING(CalendarStartTime, @Position * 8 + 1, 8)) AS StartDateTime,
	CASE
		WHEN re.ResourceType <> 'R'
			THEN CalendarResourceId 
		ELSE NULL
		END AS UserId
FROM dbo.PracticeCalendar pc
INNER JOIN dbo.Resources re ON re.ResourceId = pc.CalendarResourceId
	AND pc.CalendarResourceId <> 0
LEFT OUTER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P' 	
	AND pnServLoc.PracticeId = CASE 
		WHEN 
			CASE @Position 
				WHEN 0 THEN CalendarLoc1
				WHEN 1 THEN CalendarLoc2			
				WHEN 2 THEN CalendarLoc3			
				WHEN 3 THEN CalendarLoc4			
				WHEN 4 THEN CalendarLoc5			
				WHEN 5 THEN CalendarLoc6			
				WHEN 6 THEN CalendarLoc7			
			END = 0
			THEN (SELECT PracticeId FROM dbo.PracticeName WHERE PracticeType = 'P' AND LocationReference = '')
		WHEN 
			CASE @Position 
				WHEN 0 THEN CalendarLoc1
				WHEN 1 THEN CalendarLoc2			
				WHEN 2 THEN CalendarLoc3			
				WHEN 3 THEN CalendarLoc4			
				WHEN 4 THEN CalendarLoc5			
				WHEN 5 THEN CalendarLoc6			
				WHEN 6 THEN CalendarLoc7			
			END = -1
			THEN (SELECT PracticeId FROM dbo.PracticeName WHERE PracticeType = 'P' AND LocationReference = '')
		WHEN @Position = 0 AND CalendarLoc1 > 1000 THEN CalendarLoc1 - 1000
		WHEN @Position = 1 AND CalendarLoc2 > 1000 THEN CalendarLoc2 - 1000
		WHEN @Position = 2 AND CalendarLoc3 > 1000 THEN CalendarLoc3 - 1000
		WHEN @Position = 3 AND CalendarLoc4 > 1000 THEN CalendarLoc4 - 1000
		WHEN @Position = 4 AND CalendarLoc5 > 1000 THEN CalendarLoc5 - 1000
		WHEN @Position = 5 AND CalendarLoc6 > 1000 THEN CalendarLoc6 - 1000
		WHEN @Position = 6 AND CalendarLoc7 > 1000 THEN CalendarLoc7 - 1000
		ELSE (SELECT PracticeId FROM dbo.PracticeName WHERE PracticeType = 'P' AND LocationReference = '')
		END

LEFT OUTER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = 'R' 
		AND reServLoc.ResourceId = CASE
			WHEN @Position = 0 AND CalendarLoc1 < 1000
				THEN CalendarLoc1
			WHEN @Position = 1 AND CalendarLoc2 < 1000
				THEN CalendarLoc2
			WHEN @Position = 2 AND CalendarLoc3 < 1000
				THEN CalendarLoc3
			WHEN @Position = 3 AND CalendarLoc4 < 1000
				THEN CalendarLoc4
			WHEN @Position = 4 AND CalendarLoc5 < 1000
				THEN CalendarLoc5
			WHEN @Position = 5 AND CalendarLoc6 < 1000
				THEN CalendarLoc6
			WHEN @Position = 6 AND CalendarLoc7 < 1000
				THEN CalendarLoc7
			END
WHERE SUBSTRING(CalendarEndTime, @Position * 8 + 1, 8) > '0'
	AND (ISDATE(CalendarDate  + ' ' + SUBSTRING(CalendarEndTime, 1 * 8 + 1, 8)) = 1
		OR ISDATE(CalendarDate  + ' ' + SUBSTRING(CalendarStartTime, 1 * 8 + 1, 8)) = 1)
RETURN
END	

GO

CREATE FUNCTION model.GetAgeFromDob(@DOB AS datetime) RETURNS INT
AS

BEGIN
DECLARE @Result as INT
DECLARE @EndDate AS datetime
SET @EndDate = model.GetClientDateNow()
IF @DOB >= @EndDate
	SET @Result = 0
ELSE
	BEGIN
		IF (MONTH(@EndDate)*100)+DAY(@EndDate) >= (MONTH(@DOB)*100)+DAY(@DOB)
		SET @Result = DATEDIFF(Year,@DOB,@EndDate)
		ELSE
		SET @Result = DATEDIFF(Year,@DOB,@EndDate)-1
	END
RETURN @Result
END

GO

CREATE FUNCTION model.ToUpperCaseWords(@string NVARCHAR(MAX)) RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @theIndex INT
DECLARE @lenghtOfTheInput INT
DECLARE @theCurrentCharacter NCHAR(1)
DECLARE @isFirstLetter INT
DECLARE @theOutputString NVARCHAR(MAX)
DECLARE @charactersConsideredAWhiteSpace NVARCHAR(MAX)

SET @charactersConsideredAWhiteSpace = '[' + CHAR(13) + CHAR(10) + CHAR(9) + CHAR(160) + ' ' + ']'
SET @theIndex = 0
SET @lenghtOfTheInput = LEN(@string)
SET @isFirstLetter = 1
SET @theOutputString = ''

WHILE @theIndex <= @lenghtOfTheInput
BEGIN
SET @theCurrentCharacter = SUBSTRING(@string, @theIndex, 1)
IF @isFirstLetter = 1 
BEGIN
SET @theOutputString = @theOutputString + @theCurrentCharacter
SET @isFirstLetter = 0
END
ELSE
BEGIN
SET @theOutputString = @theOutputString + LOWER(@theCurrentCharacter)
END

IF @theCurrentCharacter LIKE @charactersConsideredAWhiteSpace SET @isFirstLetter = 1

SET @theIndex = @theIndex + 1
END

RETURN @theOutputString
END

GO
