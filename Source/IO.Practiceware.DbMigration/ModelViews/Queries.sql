﻿
-- BillingOrganizationAddress
----BillingOrganization address; 0 for PracticeName table   
SELECT model.GetBigPairId(4, pn.PracticeId, @AddressMax) AS Id,
	pn.PracticeId AS BillingOrganizationId,
	pn.PracticeCity AS City,
	pn.PracticeAddress AS Line1,
	pn.PracticeSuite AS Line2,
	CONVERT(NVARCHAR, NULL) AS Line3,
	REPLACE(pn.PracticeZip,'-','') AS PostalCode,
	st.Id AS StateOrProvinceId,
	5 AS BillingOrganizationAddressTypeId
FROM dbo.PracticeName pn
INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pn.PracticeState
INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
WHERE pn.PracticeType = 'P'
	AND pn.PracticeAddress <> ''
	AND pn.PracticeAddress IS NOT NULL
GROUP BY pn.PracticeId,
	pn.LocationReference,
	pn.PracticeCity,
	pn.PracticeAddress,
	pn.PracticeSuite,
	pn.PracticeState,
	pn.PracticeZip,
	st.Id

UNION ALL

----BillingOrganization address; 1 for Resources table (OrgOverride/Override doctors)
SELECT model.GetBigPairId(5, re.ResourceId, @AddressMax) AS Id
	,model.GetPairId(1, re.ResourceId, @BillingOrganizationMax) AS BillingOrganizationId
	,re.ResourceCity AS City
	,re.ResourceAddress AS Line1
	,re.ResourceSuite AS Line2
	,CONVERT(NVARCHAR, NULL) AS Line3
	,REPLACE(re.ResourceZip,'-','') AS PostalCode
	,st.Id AS StateOrProvinceId
	,5 AS BillingOrganizationAddressTypeId
FROM dbo.Resources re
LEFT JOIN model.StateOrProvinces st ON st.Abbreviation = re.ResourceState
INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
	AND pa.ResourceType = 'R'
WHERE re.ResourceType IN (
		'D'
		,'Q'
		,'Z'
		,'Y'
		)
	AND (
		pa.[Override] = 'Y'
		OR pa.OrgOverride = 'Y'
		)
	AND re.ResourceAddress <> ''
	AND re.ResourceAddress IS NOT NULL
GROUP BY re.ResourceId
	,re.ResourceAddress
	,re.ResourceCity
	,re.PlaceOfService
	,re.ResourceSuite
	,st.Id
	,re.ResourceZip


UNION ALL

----BillingOrganization PayTo Address
SELECT model.GetBigPairId(17, pnBO.PracticeId, @AddressMax) AS Id,
	pnBO.PracticeId AS BillingOrganizationId,
	pnPayTo.PracticeCity AS City,
	pnPayTo.PracticeAddress AS Line1,
	pnPayTo.PracticeSuite AS Line2,
	CONVERT(NVARCHAR, NULL) AS Line3,
	REPLACE(pnPayTo.PracticeZip,'-','') AS PostalCode,
	st.Id AS StateOrProvinceId,
	6 AS BillingOrganizationAddressTypeId
FROM dbo.PracticeName pnPayTo
INNER JOIN dbo.PracticeName pnBO on pnPayTo.PracticeId = pnBO.CatArray2
	AND pnBO.PracticeType = 'P'
INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pnPayTo.PracticeState
WHERE pnPayTo.PracticeType = 'P'
	AND pnPayTo.PracticeAddress <> ''
	AND pnPayTo.PracticeAddress IS NOT NULL
GROUP BY pnBO.PracticeId,
	pnPayTo.PracticeId,
	pnPayTo.PracticeCity,
	pnPayTo.PracticeAddress,
	pnPayTo.PracticeSuite,
	pnPayTo.PracticeZip,
	st.Id

GO

-- ServiceLocationAddress
----ServiceLocation address; PracticeName table
SELECT  model.GetBigPairId(6, pn.PracticeId, @AddressMax) AS Id,
	pn.PracticeCity AS City,
	pn.PracticeAddress AS Line1,
	pn.PracticeSuite AS Line2,
	CONVERT(NVARCHAR, NULL) AS Line3,
	REPLACE(pn.PracticeZip,'-','') AS PostalCode,
	pn.PracticeId AS ServiceLocationId,
	st.Id AS StateOrProvinceId,
	7 AS ServiceLocationAddressTypeId
FROM dbo.PracticeName pn
INNER JOIN model.StateOrProvinces st ON st.Abbreviation = pn.PracticeState
	AND st.CountryId = 225
WHERE pn.PracticeType = 'P'
	AND pn.PracticeAddress <> ''
	AND pn.PracticeAddress IS NOT NULL
GROUP BY pn.PracticeId,
	pn.LocationReference,
	pn.PracticeCity,
	pn.PracticeAddress,
	pn.PracticeSuite,
	pn.PracticeState,
	pn.PracticeZip,
	st.Id

UNION ALL

----ServiceLocation address; Resources table
SELECT  model.GetBigPairId(7, re.ResourceId, @AddressMax) AS Id,
	re.ResourceCity AS City,
	re.ResourceAddress AS Line1,
	re.ResourceSuite AS Line2,
	CONVERT(NVARCHAR, NULL) AS Line3,
	REPLACE(re.ResourceZip,'-','') AS PostalCode,
	model.GetPairId(1, re.ResourceId, @ServiceLocationMax) AS ServiceLocationId,
	st.Id AS StateOrProvinceId,
	7 AS ServiceLocationAddressTypeId
FROM dbo.Resources re
INNER JOIN model.StateOrProvinces st ON st.Abbreviation = re.ResourceState
	AND st.CountryId = 225
LEFT OUTER JOIN model.ServiceLocationAddressTypes at ON at.Name = 'Other1'
LEFT OUTER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
LEFT OUTER JOIN dbo.PracticeName pn on re.PracticeId = pn.PracticeId
	AND pic.FieldValue = 'T'
	AND re.Billable = 'Y'
	AND pn.LocationReference <> ''
WHERE re.ResourceType = 'R'
	AND re.ServiceCode = '02'
	AND pn.PracticeId IS NULL
	AND re.ResourceAddress <> ''
	AND re.ResourceAddress IS NOT NULL
GROUP BY re.ResourceId,
	re.ResourceAddress,
	re.ResourceCity,
	re.PlaceOfService,
	re.ResourceSuite,
	st.Id,
	re.ResourceZip

GO

-- UserAddress
----User address; 
SELECT model.GetBigPairId(8, re.ResourceId, @AddressMax) AS Id,
	re.ResourceCity AS City,
	re.ResourceAddress AS Line1,
	re.ResourceSuite AS Line2,
	CONVERT(NVARCHAR, NULL) AS Line3,
	REPLACE(re.ResourceZip,'-','') AS PostalCode,
	st.Id AS StateOrProvinceId,
	re.ResourceId AS UserId,
	at.Id AS UserAddressTypeId
FROM dbo.Resources re
INNER JOIN model.StateOrProvinces st ON st.Abbreviation = re.ResourceState
	AND st.CountryId = 225
LEFT OUTER JOIN model.UserAddressTypes at ON at.Name = 'User1'
WHERE re.ResourceType <> 'R'
	AND re.ResourceAddress <> ''
	AND re.ResourceAddress IS NOT NULL

GO

-- AdjustmentType
SELECT 
adjt.[Id]
,adjt.[FinancialTypeGroupId]
,adjt.[IsCash]
,adjt.[IsDebit]
,adjt.[IsPrintOnStatement]
,adjt.[Name]
,adjt.[ShortName]
,adjt.[IsArchived]
,adjt.[OrdinalId]
FROM [model].[AdjustmentTypes_Internal] adjt

GO

-- Appointment
-- Audit
----Scheduled with a User
SELECT AppointmentId AS Id,
	CASE ap.AppTypeId 
		WHEN 0
			THEN -1 
		ELSE ap.AppTypeId END AS AppointmentTypeId,
	CASE
		WHEN ap.AppTime = -1
			THEN CONVERT(datetime, ap.AppDate, 112)
		ELSE
			DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112)) 
	END AS [DateTime],
	ap.EncounterId AS EncounterId,
	ap.ResourceId1 AS UserId,
	NULL AS EquipmentId,
	NULL AS RoomId,
	CAST(NULL AS datetime) AS OrderDate,
	CASE ap.Comments
		WHEN 'ADD VIA HIGHLIGHTS'
			THEN NULL
		WHEN ''
			THEN NULL
		ELSE ap.Comments END AS Comment,
	CASE 
		WHEN ap.AppTime >= 0
			THEN CONVERT(bit, 1)
		ELSE
			CONVERT(bit, 0)
	END AS DisplayOnCalendar,
	'UserAppointment' AS __EntityType__
FROM dbo.Appointments ap
INNER JOIN dbo.Resources re on re.ResourceId = ap.ResourceId1
WHERE ap.ResourceId1 <> 0 AND ap.Comments NOT IN ('ADD VIA BILLING','ADD VIA OPTICAL','ADD VIA SURGERY ORDER')
GO

-- AppointmentType
--Change AppointemntCategory to NON-Nullable when screens are built!
SELECT CASE apt.AppTypeId 
		WHEN 0
			THEN -1
		ELSE apt.AppTypeId END AS Id
	,1 AS EncounterTypeId
	,apt.AppointmentType AS [Name]
	,apt.ResourceId6 AS OleColor
	,pct.Id AS PatientQuestionSetId
	,CASE 
		WHEN AppointmentType IN ('ADD VIA BILLING', 'ADD VIA SURGERY ORDER', 'ADD VIA OPTICAL', 'ASC CLAIM', 'Manual Claim', 'Deleted Appointment Type')
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0) END AS IsArchived
	,apt.AppointmentCategoryId AS AppointmentCategoryId
	,apt.OtherRank AS OrdinalId
	,CASE apt.RecallFreq
	 WHEN 0
		THEN 1
	 ELSE
		 apt.RecallFreq 
	 END AS FrequencyBetweenRecallsPerEncounter
	,CASE apt.RecallMax 
	 WHEN 0
		THEN 1
	ELSE
		apt.RecallMax 
	END AS MaximumRecallsPerEncounter
    ,CONVERT(bit, apt.ResourceId8) AS IsOrderRequired
	,CASE WHEN apt.FirstVisitIndicator = 'Y' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END AS IsFirstVisit
FROM dbo.AppointmentType apt
INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'QUESTIONSETS'
	AND pct.Code = apt.Question
INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
WHERE pic.FieldValue <> 'T'
	OR (
		pic.FieldValue = 'T'
		AND apt.ResourceId8 = 0
		)

UNION ALL

--ASC on/Surgery appt
SELECT CASE apt.AppTypeId 
		WHEN 0
			THEN -1
		ELSE apt.AppTypeId END AS Id
	,3 AS EncounterTypeId
	,apt.AppointmentType AS [Name]
	,apt.ResourceId6 AS OleColor
	,pct.Id AS PatientQuestionSetId
	,CASE 
		WHEN AppointmentType IN ('ADD VIA BILLING', 'ADD VIA SURGERY ORDER', 'ADD VIA OPTICAL', 'ASC CLAIM', 'Manual Claim', 'Deleted Appointment Type')
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0) END AS IsArchived
	,apt.AppointmentCategoryId AS AppointmentCategoryId
	,apt.OtherRank AS OrdinalId
	,CASE apt.RecallFreq
	 WHEN 0
		THEN 1
	 ELSE
		 apt.RecallFreq 
	 END AS FrequencyBetweenRecallsPerEncounter
	,CASE apt.RecallMax 
	 WHEN 0
		THEN 1
	ELSE
		apt.RecallMax 
	END AS MaximumRecallsPerEncounter
	,CONVERT(bit, apt.ResourceId8) AS IsOrderRequired
	,CASE WHEN apt.FirstVisitIndicator = 'Y' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END AS IsFirstVisit
FROM dbo.AppointmentType apt
INNER JOIN dbo.PracticeCodeTable pct ON pct.ReferenceType = 'QUESTIONSETS'
	AND pct.Code = apt.Question
INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
WHERE pic.FieldValue = 'T'
	AND apt.ResourceId8 = 1

GO

-- BillingOrganization
----From PracticeName table
SELECT pn.PracticeId AS Id,
	CONVERT(bit,0) AS IsArchived,
	CASE pn.LocationReference
		WHEN ''
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
		END AS IsMain,
	PracticeName AS Name,
	CASE LocationReference
		WHEN ''
			THEN 'Office'
		ELSE 'Office-' + LocationReference
		END AS ShortName,
	CONVERT(NVARCHAR, NULL) AS LastName,
	CONVERT(NVARCHAR, NULL) AS FirstName,
	CONVERT(NVARCHAR, NULL) AS MiddleName,
	CONVERT(NVARCHAR, NULL) AS Suffix,
	'BillingOrganization' AS __EntityType__
FROM dbo.PracticeName pn
WHERE pn.PracticeType = 'P'

UNION ALL

----BillingOrganization From Resources table - doctors w OrgOverride/Override only
----has duplicates because of Override/OrgOverride
SELECT model.GetPairId(1, v.ResourceId, @BillingOrganizationMax) AS Id,
	v.IsArchived,
	v.IsMain,
	v.Name,
	v.ShortName,
	v.LastName,
	v.FirstName,
	v.MiddleName,
	v.Suffix,
	v.__EntityType__
FROM (SELECT re.ResourceId AS ResourceId,
		CONVERT(bit,0) AS IsArchived,
		CONVERT(bit, 0) AS IsMain,
		ResourceDescription AS [Name],
		ResourceName AS ShortName,
		ResourceLastName AS LastName,
		ResourceFirstName AS FirstName,
		ResourceMI AS MiddleName,
		ResourceSuffix AS Suffix,
		'PersonBillingOrganization' AS __EntityType__,
		COUNT_BIG(*) AS Count
	FROM dbo.Resources re
	LEFT OUTER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
		AND pa.ResourceType = 'R'
	WHERE re.ResourceType in ('D', 'Q', 'Z', 'Y') 
		AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
	GROUP BY re.ResourceDescription,
		re.ResourceName,
		re.ResourceLastName,
		re.ResourceFirstName,
		re.ResourceMI,
		re.ResourceSuffix,
		re.ResourceId,
		re.Billable,
		re.ResourceType
	) AS v

GO

-- ClaimAdjustmentReasonAdjustmentType (no index)
---For Adjustments entity.  This tells us which AdjustmentType to use when we get certain reason codes in the 835 file.
SELECT pctReason.Id AS Id,
	2 AS AdjustmentTypeId,
	1 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	NULL AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctAdjType.Code, 1) = 'X'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.code = '45'

UNION ALL

SELECT model.GetBigPairId(1, pctReason.Id, @ClaimAdjustmentReasonAdjustmentTypeMax) AS Id,
	2 AS AdjustmentTypeId,
	1 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	NULL AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctAdjType.Code, 1) = 'X'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.Code = '42'

UNION ALL

SELECT model.GetBigPairId(2, pctReason.Id, @ClaimAdjustmentReasonAdjustmentTypeMax) AS Id,
	2 AS AdjustmentTypeId,
	1 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	NULL AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctAdjType.Code, 1) = 'X'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.Code = 'A2'

UNION ALL

SELECT model.GetBigPairId(3, pctReason.Id, @ClaimAdjustmentReasonAdjustmentTypeMax) AS Id,
	2 AS AdjustmentTypeId,
	1 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	NULL AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctAdjType.Code, 1) = 'X'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.Code = '59'

UNION ALL

SELECT model.GetBigPairId(4, pctReason.Id, @ClaimAdjustmentReasonAdjustmentTypeMax) AS Id,
	2 AS AdjustmentTypeId,
	1 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	NULL AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctAdjType.Code, 1) = 'X'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.Code = '94'

UNION ALL

SELECT model.GetBigPairId(5, pctReason.Id, @ClaimAdjustmentReasonAdjustmentTypeMax) AS Id,
	2 AS AdjustmentTypeId,
	1 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	NULL AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctAdjType.Code, 1) = 'X'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.Code = '172'

UNION ALL

SELECT model.GetBigPairId(6, pctReason.Id, @ClaimAdjustmentReasonAdjustmentTypeMax) AS Id,
	2 AS AdjustmentTypeId,
	1 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	NULL AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctAdjType.Code, 1) = 'X'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.Code = 'B10'

UNION ALL

SELECT model.GetBigPairId(7, pctReason.Id, @ClaimAdjustmentReasonAdjustmentTypeMax) AS Id,
	2 AS AdjustmentTypeId,
	3 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	NULL AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctAdjType.Code, 1) = 'X'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.code = '45'

UNION ALL

SELECT pctReason.Id AS Id,
	2 AS AdjustmentTypeId,
	1 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	NULL AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctAdjType.Code, 1) = 'X'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.code = '237'

UNION ALL

SELECT pctReason.Id AS Id,
	2 AS AdjustmentTypeId,
	1 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	NULL AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctAdjType ON pctAdjType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctAdjType.Code, 1) = 'X'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.code = '253'

UNION ALL

---For FinancialInformation entity
SELECT model.GetBigPairId(8, pctReason.Id, @ClaimAdjustmentReasonAdjustmentTypeMax) AS Id,
	NULL AS AdjustmentTypeId,
	2 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	CASE
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '!'
			THEN 1
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '|'
			THEN 2
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '?'
			THEN 3
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '%'
			THEN 4
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = 'D'
			THEN 5
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '&'
			THEN 6
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ']'
			THEN 7
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '0'
			THEN 8
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = 'V'
			THEN 9
		ELSE pctFinType.Id + 1000
	END AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctFinType ON pctFinType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctFinType.Code, 1) = '!'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.code = '1'

UNION ALL

SELECT model.GetBigPairId(9, pctReason.Id, @ClaimAdjustmentReasonAdjustmentTypeMax) AS Id,
	NULL AS AdjustmentTypeId,
	2 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	CASE
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '!'
			THEN 1
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '|'
			THEN 2
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '?'
			THEN 3
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '%'
			THEN 4
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = 'D'
			THEN 5
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '&'
			THEN 6
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ']'
			THEN 7
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '0'
			THEN 8
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = 'V'
			THEN 9
		ELSE pctFinType.Id + 1000
	END AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctFinType ON pctFinType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctFinType.Code, 1) = '|'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.code = '2'

UNION ALL

SELECT model.GetBigPairId(10, pctReason.Id, @ClaimAdjustmentReasonAdjustmentTypeMax) AS Id,
	NULL AS AdjustmentTypeId,
	2 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	CASE
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '!'
			THEN 1
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '|'
			THEN 2
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '?'
			THEN 3
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '%'
			THEN 4
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = 'D'
			THEN 5
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '&'
			THEN 6
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ']'
			THEN 7
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '0'
			THEN 8
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = 'V'
			THEN 9
		ELSE pctFinType.Id + 1000
	END AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctFinType ON pctFinType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctFinType.Code, 1) = '|'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.code = '3'

UNION ALL

SELECT model.GetBigPairId(11, pctReason.Id, @ClaimAdjustmentReasonAdjustmentTypeMax) AS Id,
	NULL AS AdjustmentTypeId,
	1 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	CASE
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '!'
			THEN 1
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '|'
			THEN 2
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '?'
			THEN 3
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '%'
			THEN 4
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = 'D'
			THEN 5
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '&'
			THEN 6
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ']'
			THEN 7
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '0'
			THEN 8
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = 'V'
			THEN 9
		ELSE pctFinType.Id + 1000
	END AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctFinType ON pctFinType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctFinType.Code, 1) = '|'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.code = '3'

UNION ALL

SELECT model.GetBigPairId(12, pctReason.Id, @ClaimAdjustmentReasonAdjustmentTypeMax) AS Id,
	NULL AS AdjustmentTypeId,
	3 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
CASE
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '!'
			THEN 1
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '|'
			THEN 2
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '?'
			THEN 3
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '%'
			THEN 4
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = 'D'
			THEN 5
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '&'
			THEN 6
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = ']'
			THEN 7
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = '0'
			THEN 8
		WHEN SUBSTRING(pctFinType.Code, (dbo.GetMax((CHARINDEX(' - ', pctFinType.Code)+3), 0)), LEN(pctFinType.Code)) = 'V'
			THEN 9
		ELSE pctFinType.Id + 1000
	END AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
INNER JOIN dbo.PracticeCodeTable pctFinType ON pctFinType.ReferenceType = 'PAYMENTTYPE'
	AND RIGHT(pctFinType.Code, 1) = ']'
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.code = '23'

UNION ALL

SELECT pctReason.Id AS Id,
	NULL AS AdjustmentTypeId,
	2 AS ClaimAdjustmentGroupCodeId,
	pctReason.Id AS ClaimAdjustmentReasonCodeId,
	3 AS FinancialInformationTypeId
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctReason
WHERE pctReason.ReferenceType = 'PAYMENTREASON'
	AND pctReason.code = '96'

GO

-- ClaimAdjustmentReasonCode
SELECT ID,
	Code,
	AlternateCode AS [Name],
	CONVERT(bit, 0) AS IsArchived,
	CONVERT(bit, CASE Exclusion
					WHEN 'T'
						THEN 1
					ELSE
						0
				 END) AS ExcludeFromSecondaryClaims
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'PAYMENTREASON'

GO

---- ClinicalDiagnosis (not good)
--SELECT pc.ClinicalId AS Id,
--	pc.AppointmentId AS EncounterId,
--	CONVERT(int, pc.Symptom) AS OrdinalId,
--	pc.FindingDetail AS PracticeDiagnosisId,
--	CONVERT(NVARCHAR, NULL) AS Site,
--	CONVERT(NVARCHAR, NULL) AS DiagnosisId
--FROM dbo.PatientClinical pc
--INNER JOIN dbo.appointments ap ON ap.AppointmentId = pc.AppointmentId
--WHERE pc.ClinicalType IN ('B')

--GO

---- ClinicalServiceClinicalDiagnosis (no good)
-----Blank LinkedDiag - 1st diagnosis
--SELECT ItemId AS ClinicalServiceId,
--	pc.ClinicalId AS ClinicalDiagnosisId,
--	CONVERT(int, Symptom) AS OrdinalId
--FROM dbo.PatientReceivableServices prs
--	INNER JOIN dbo.PatientReceivables pr ON pr.Invoice = prs.Invoice
--	INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = pr.AppointmentId
--		AND ClinicalType = 'B'
--		AND LinkedDiag = ''
--		AND Symptom = '1'

--UNION

-----Blank LinkedDiag - 2nd diagnosis
--SELECT ItemId AS ClinicalServiceId,
--	pc.ClinicalId AS ClinicalDiagnosisId,
--	CONVERT(int, Symptom) AS OrdinalId
--FROM dbo.PatientReceivableServices prs
--	INNER JOIN dbo.PatientReceivables pr ON pr.Invoice = prs.Invoice
--	INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = pr.AppointmentId
--		AND ClinicalType = 'B'
--		AND LinkedDiag = ''
--		AND Symptom = '2'

--UNION

-----Blank LinkedDiag - 3rd diagnosis
--SELECT ItemId AS ClinicalServiceId,
--	pc.ClinicalId AS ClinicalDiagnosisId,
--	CONVERT(int, Symptom) AS OrdinalId
--FROM dbo.PatientReceivableServices prs
--	INNER JOIN dbo.PatientReceivables pr ON pr.Invoice = prs.Invoice
--	INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = pr.AppointmentId
--		AND ClinicalType = 'B'
--		AND LinkedDiag = ''
--		AND Symptom = '3'
--UNION

-----Blank LinkedDiag - 4th diagnosis
--SELECT ItemId AS ClinicalServiceId,
--	pc.ClinicalId AS ClinicalDiagnosisId,
--	CONVERT(int, Symptom) AS OrdinalId
--FROM dbo.PatientReceivableServices prs
--	INNER JOIN dbo.PatientReceivables pr ON pr.Invoice = prs.Invoice
--	INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = pr.AppointmentId
--		AND ClinicalType = 'B'
--		AND LinkedDiag = ''
--		AND Symptom = '4'
--UNION

-----Linked diagnoses - 1st
--SELECT ItemId AS ClinicalServiceId,
--	pc.ClinicalId AS ClinicalDiagnosisId,
--	CONVERT(int, Symptom) AS OrdinalId
--FROM dbo.PatientReceivableServices prs
--	INNER JOIN dbo.PatientReceivables pr ON pr.Invoice = prs.Invoice
--	INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = pr.AppointmentId
--		AND ClinicalType = 'B'
--		AND SUBSTRING(LinkedDiag, 1, 1) = Symptom
--UNION

-----Linked diagnoses - 2nd
--SELECT ItemId AS ClinicalServiceId,
--	pc.ClinicalId AS ClinicalDiagnosisId,
--	CONVERT(int, Symptom) AS OrdinalId
--FROM dbo.PatientReceivableServices prs
--	INNER JOIN dbo.PatientReceivables pr ON pr.Invoice = prs.Invoice
--	INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = pr.AppointmentId
--		AND ClinicalType = 'B'
--		AND SUBSTRING(LinkedDiag, 2, 1) = Symptom
--		AND SUBSTRING(LinkedDiag, 2, 1) <> SUBSTRING(LinkedDiag, 1, 1) 
--UNION

-----Linked diagnoses - 3rd
--SELECT ItemId AS ClinicalServiceId,
--	pc.ClinicalId AS ClinicalDiagnosisId,
--	CONVERT(int, Symptom) AS OrdinalId
--FROM dbo.PatientReceivableServices prs
--	INNER JOIN dbo.PatientReceivables pr ON pr.Invoice = prs.Invoice
--	INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = pr.AppointmentId
--		AND ClinicalType = 'B'
--		AND SUBSTRING(LinkedDiag, 3, 1) = Symptom
--UNION

-----Linked diagnoses - 4th
--SELECT ItemId AS ClinicalServiceId,
--	pc.ClinicalId AS ClinicalDiagnosisId,
--	CONVERT(int, Symptom) AS OrdinalId
--FROM dbo.PatientReceivableServices prs
--	INNER JOIN dbo.PatientReceivables pr ON pr.Invoice = prs.Invoice
--	INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = pr.AppointmentId
--		AND ClinicalType = 'B'
--		AND SUBSTRING(LinkedDiag, 4, 1) = Symptom
--GO

-- ClinicalService
----Not using PatientClinical ClinicalType P and Z because there are no modifiers or units
SELECT
bs.Id,
i.LegacyPatientReceivablesAppointmentId AS EncounterId,
MAX(es.Id) AS EncounterServiceId,
bs.OrdinalId,
bs.Unit,
bs.UnitCharge AS UnitCharge,
bs.Id AS ItemId,
COUNT_BIG(*) AS Count
FROM model.BillingServices bs
JOIN model.Invoices i ON i.Id = bs.InvoiceId
LEFT JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.LegacyPatientReceivablesAppointmentId
WHERE i.LegacyPatientReceivablesAppointmentId > 0
	AND ap.AppointmentId = ap.EncounterId
GROUP BY i.LegacyPatientReceivablesAppointmentId,
	bs.OrdinalId,
	bs.Unit,
	bs.UnitCharge,
	bs.Id 

UNION ALL

----Second partition is for ASC.  The EncounterId is the appointment id of the surgeon's appointment.
--*/*(no index)*/*
SELECT
	bs.Id AS Id,
apEncounter.AppointmentId AS EncounterId,
MAX(es.Id) AS EncounterServiceId,
bs.OrdinalId,
bs.Unit,
bs.UnitCharge AS UnitCharge,
	bs.Id AS ItemId,
	COUNT_BIG(*) AS Count
FROM model.BillingServices bs
JOIN model.Invoices i ON i.Id = bs.InvoiceId
LEFT JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = i.LegacyPatientReceivablesAppointmentId
	AND ap.Comments = 'ASC CLAIM'
INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
	AND apEncounter.AppTypeId = ap.AppTypeId
	AND apEncounter.AppDate = ap.AppDate
	AND apEncounter.AppTime > 0 
	AND ap.AppTime = 0
	AND apEncounter.ScheduleStatus = ap.ScheduleStatus
	AND ap.ScheduleStatus = 'D'
	AND apEncounter.ResourceId1 = ap.ResourceId1
	AND apEncounter.ResourceId2 = ap.ResourceId2
WHERE i.LegacyPatientReceivablesAppointmentId > 0
GROUP BY apEncounter.AppointmentId,
bs.OrdinalId,
bs.Unit,
bs.UnitCharge,
bs.Id
GO

-- ClinicalServiceModifier (no index) (no good)
---This view will not find any modifiers that are not set up in PracticeCodeTable/ServiceModifiers
---First Modifier
SELECT CASE v.PartitionId
	WHEN 0
		THEN v.ClinicalServiceId 
	WHEN 1
		THEN model.GetPairId(1, v.ClinicalServiceId, @ClinicalServiceModifierMax)
	WHEN 2
		THEN model.GetPairId(2, v.ClinicalServiceId, @ClinicalServiceModifierMax)
	WHEN 3
		THEN model.GetPairId(3, v.ClinicalServiceId, @ClinicalServiceModifierMax)
	END AS Id,
	v.ClinicalServiceId,
	v.ServiceModifierId
FROM (
	SELECT 0 AS PartitionId,
		prs.ItemId AS ClinicalServiceId,
		pctMod1.Id AS ServiceModifierId
	FROM dbo.PatientReceivableServices prs
	INNER JOIN dbo.PracticeCodeTable pctMod1 ON SUBSTRING(pctMod1.Code, 1, 2) = SUBSTRING(modifier, 1, 2)
		AND ReferenceType = 'SERVICEMODS'
	WHERE prs.Modifier <> ''
		AND prs.[Status] = 'A'
	GROUP BY prs.ItemId,
		pctmod1.Id
		
	UNION ALL

	---Second Modifier
	SELECT 1 AS PartitionId,
		prs.ItemId AS ClinicalServiceId,
		pctMod2.Id AS ServiceModifierId
	FROM dbo.PatientReceivableServices prs
	INNER JOIN dbo.PracticeCodeTable pctMod2 ON SUBSTRING(pctMod2.Code, 1, 2) = SUBSTRING(modifier, 3, 2)
		AND ReferenceType = 'SERVICEMODS'
	WHERE prs.Modifier <> ''
		AND prs.[Status] = 'A'
	GROUP BY prs.ItemId,
		pctmod2.Id

	UNION ALL

	---Third Modifier
	SELECT 2 AS PartitionId,
		prs.ItemId AS ClinicalServiceId,
		pctMod3.Id AS ServiceModifierId
	FROM dbo.PatientReceivableServices prs
	INNER JOIN dbo.PracticeCodeTable pctMod3 ON SUBSTRING(pctMod3.Code, 1, 2) = SUBSTRING(modifier, 5, 2)
		AND ReferenceType = 'SERVICEMODS'
	WHERE prs.Modifier <> ''
		AND prs.[Status] = 'A'
	GROUP BY prs.ItemId,
		pctmod3.Id

	UNION ALL

	---Fourth Modifier
	SELECT 3 AS PartitionId,
		prs.ItemId AS ClinicalServiceId,
		pctMod4.Id AS ServiceModifierId
	FROM dbo.PatientReceivableServices prs
	INNER JOIN dbo.PracticeCodeTable pctMod4 ON SUBSTRING(pctMod4.Code, 1, 2) = SUBSTRING(modifier, 7, 2)
		AND ReferenceType = 'SERVICEMODS'
	WHERE prs.Modifier <> ''
		AND prs.[Status] = 'A'
	GROUP BY prs.ItemId,
		pctmod4.Id
	) AS v
	GROUP BY v.ClinicalServiceId,
	v.ServiceModifierId,
	v.Id

GO


-- Comment (no index)
----There are many other notes not yet included in the below query, including Patient Financial, Patient Collection, Practice, Red (CRM), Surgery and Clinical notes.
----We also need additional properties to the comments (or a restructuring) to include StartDateTime, EndDateTime, IsDeleted, IsIncludedOnClaim, IsIncludedOnStatement, IsIncludedOnRx, IsAlertOn and additional granularity such as NoteSystem, NoteCategory.
----Emergency contacts and External providers don't have notes currently in IO
----EncounterLensesPrescription - spectacles comment
SELECT model.GetBigPairId(7, ClinicalId, @CommentMax) AS Id,
	ClinicalId AS EncounterLensesPrescriptionId,
	NULL AS PatientReferralSourceId,
	SUBSTRING(ImageDescriptor, 5,(dbo.GetMax((CHARINDEX('OS:', imagedescriptor) - 5),0))) AS Value,
	CONVERT(bit, 0) AS IsArchived
FROM dbo.PatientClinical pc
WHERE ClinicalType = 'A'
	AND SUBSTRING(FindingDetail, 1, 18) = 'DISPENSE SPECTACLE'
	AND SUBSTRING(ImageDescriptor, 1, 1) <> '!'
	AND ImageDescriptor NOT IN (
		'',
		'^OD: OS:',
		'^'
		)
	AND [Status] = 'A'
	AND pc.AppointmentId <> 0

UNION ALL

----EncounterLensesPrescription - contact lens OD comment
SELECT model.GetBigPairId(8, ClinicalId, @CommentMax) AS Id,
	ClinicalId AS EncounterLensesPrescriptionId,
	NULL AS PatientReferralSourceId,
	SUBSTRING(ImageDescriptor, 5, (dbo.GetMax((CHARINDEX('OS:', ImageDescriptor) - 5),0))) AS Value,
	CONVERT(bit, 0) AS IsArchived
FROM dbo.PatientClinical pc
WHERE ClinicalType = 'A'
	AND SUBSTRING(FindingDetail, 1, 14) = 'DISPENSE CL RX'
	AND SUBSTRING(ImageDescriptor, 1, 1) <> '!'
	AND ImageDescriptor NOT IN (
		'',
		'^'
		)
	AND LEN(imagedescriptor) > 4
	AND SUBSTRING(ImageDescriptor, 1, 8) <> '^OD: OS:'
	AND SUBSTRING(ImageDescriptor, 1, 7) <> '^OD:OS:'
	AND SUBSTRING(ImageDescriptor, 1, 4) IN ('^OD:', '*OD:')
	AND ImageDescriptor like '%OS:%'
	AND [Status] = 'A'
	AND pc.AppointmentId <> 0
	
UNION ALL

----EncounterLensesPrescription - contact lens where no eye specified
SELECT model.GetBigPairId(8, ClinicalId, @CommentMax) AS Id,
	ClinicalId AS EncounterLensesPrescriptionId,
	NULL AS PatientReferralSourceId,
	ImageDescriptor AS Value,
	CONVERT(bit, 0) AS IsArchived
FROM dbo.PatientClinical pc
WHERE ClinicalType = 'A'
	AND SUBSTRING(FindingDetail, 1, 14) = 'DISPENSE CL RX'
	AND SUBSTRING(ImageDescriptor, 1, 1) <> '!'
	AND ImageDescriptor NOT IN (
		'',
		'^'
		)
	AND LEN(imagedescriptor) > 4
	AND SUBSTRING(ImageDescriptor, 1, 8) <> '^OD: OS:'
	AND SUBSTRING(ImageDescriptor, 1, 7) <> '^OD:OS:'
	AND SUBSTRING(ImageDescriptor, 1, 4) NOT IN ('^OD:', '*OD:')
	AND [Status] = 'A'
	AND pc.AppointmentId <> 0
	
UNION ALL
----EncounterLensesPrescription - contact lens OS comment
SELECT model.GetBigPairId(9, ClinicalId, @CommentMax) AS Id,
	ClinicalId AS EncounterLensesPrescriptionId,
	NULL AS PatientReferralSourceId,
	SUBSTRING(ImageDescriptor, dbo.GetMax((dbo.GetMax((CHARINDEX('OS:', ImageDescriptor) + 3),0)), LEN(imagedescriptor) - (dbo.GetMax((CHARINDEX('OS:', imagedescriptor) + 3),0))),0) AS Value,
	CONVERT(bit, 0) AS IsArchived
FROM dbo.PatientClinical pc
WHERE ClinicalType = 'A'
	AND SUBSTRING(FindingDetail, 1, 14) = 'DISPENSE CL RX'
	AND SUBSTRING(ImageDescriptor, 1, 1) <> '!'
	AND ImageDescriptor NOT IN (
		'',
		'^OD: OS:',
		'^'
		)
	AND LEN(ImageDescriptor) > 4
	AND ImageDescriptor like '%OS:%'
	AND [Status] = 'A'	
	AND pc.AppointmentId <> 0

GO

-- DoctorClinicalSpecialtyType
SELECT re.ResourceId AS Id,
	1 AS OrdinalId,
	re.ResourceId AS DoctorId,
	pct.Id AS ClinicalSpecialtyTypeId
FROM dbo.Resources re
LEFT JOIN dbo.PracticeCodeTable pct ON pct.code = re.ResourceSpecialty
	AND pct.ReferenceType = 'SPECIALTY'
WHERE pct.Id <> ''
	AND pct.Id IS NOT NULL

GO

-- ClinicalSpecialtyType
SELECT Id,
	Code AS [Name]
	,CONVERT(bit, 0) AS IsArchived
	,NULL AS BodySystemId
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'SPECIALTY'

GO

-- BodySystem
SELECT 1 AS Id ,CONVERT(NVARCHAR(MAX), 'Endocrine') AS Name ,1 AS OrdinalId ,CONVERT(BIT, 0) AS IsDeactivated 
UNION ALL
SELECT 2 AS Id ,CONVERT(NVARCHAR(MAX), 'Nervous') AS Name ,1 AS OrdinalId ,CONVERT(BIT, 0) AS IsDeactivated 
UNION ALL
SELECT 3 AS Id ,CONVERT(NVARCHAR(MAX), 'Reproductive') AS Name ,1 AS OrdinalId ,CONVERT(BIT, 0) AS IsDeactivated 
UNION ALL
SELECT 4 AS Id ,CONVERT(NVARCHAR(MAX), 'Circulatory') AS Name ,1 AS OrdinalId ,CONVERT(BIT, 0) AS IsDeactivated 

GO

-- EmailAddress
----EmailAddress BillingOrganization PracticeName table
SELECT model.GetPairId(1, pn.PracticeId, @EmailAddressMax) AS Id,
	pn.PracticeId AS BillingOrganizationId,
	NULL AS ClaimFileReceiverId,
	NULL AS InsurerId,
	0 AS OrdinalId,
	NULL AS ServiceLocationId,
	NULL AS UserId,
	pn.PracticeEmail AS Value,
	1 AS EmailAddressTypeId
FROM dbo.PracticeName pn
WHERE PracticeEmail <> ''
	AND PracticeEmail IS NOT NULL
	AND PracticeType = 'P'


UNION ALL

----EmailAddress BillingOrganization Resources table
SELECT model.GetPairId(2, re.ResourceId, @EmailAddressMax) AS Id,
	model.GetPairId(1, re.ResourceId, @BillingOrganizationMax) AS BillingOrganizationId,
	NULL AS ClaimFileReceiverId,
	NULL AS InsurerId,
	0 AS OrdinalId,
	NULL AS ServiceLocationId,
	NULL AS UserId,
	re.ResourceEmail AS Value,
	1 AS EmailAddressTypeId
FROM dbo.Resources re
WHERE re.ResourceType = 'R'
	AND re.ServiceCode = '02'
	AND re.ResourceEmail <> ''
	AND re.ResourceEmail IS NOT NULL

UNION ALL

----EmailAddress BillingOrganization Resources table - OrgOverride [to do]

----EmailAddress ClaimFileReceiverId - IO currently doesn't support this email address


----EmailAddress ServiceLocation PracticeName table
SELECT model.GetPairId(3, pn.PracticeId, @EmailAddressMax) AS Id,
	NULL AS BillingOrganizationId,
	NULL AS ClaimFileReceiverId,
	NULL AS InsurerId,
	0 AS OrdinalId,
	pn.PracticeId AS ServiceLocationId,
	NULL AS UserId,
	PracticeEmail AS Value,
	1 AS EmailAddressTypeId
FROM dbo.PracticeName pn
WHERE PracticeEmail <> ''
	AND PracticeEmail IS NOT NULL
	AND PracticeType = 'P'

UNION ALL

----EmailAddress ServiceLocation Resources table
SELECT model.GetPairId(4, re.ResourceId, @EmailAddressMax) AS Id,
	NULL AS BillingOrganizationId,
	NULL AS ClaimFileReceiverId,
	NULL AS InsurerId,
	0 AS OrdinalId,
	model.GetPairId(1, re.ResourceId, @ServiceLocationMax) AS ServiceLocationId,
	NULL AS UserId,
	ResourceEmail AS Value,
	1 AS EmailAddressTypeId
FROM dbo.Resources re
LEFT JOIN dbo.PracticeName pn ON pn.PracticeId = re.PracticeId
LEFT JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
WHERE ResourceEmail <> ''
	AND re.ResourceEmail IS NOT NULL
	AND re.ResourceType = 'R'
	AND re.ServiceCode = '02'
	AND CASE 
		WHEN pic.FieldValue = 'T'
			AND re.Billable = 'Y'
			AND pn.LocationReference <> ''
			THEN 1
		ELSE 0
		END <> 1

UNION ALL

----EmailAddress User
SELECT re.ResourceId AS Id,
	NULL AS BillingOrganizationId,
	NULL AS ClaimFileReceiverId,
	NULL AS InsurerId,
	0 AS OrdinalId,
	NULL AS ServiceLocationId,
	re.ResourceId AS UserId,
	re.ResourceEmail AS Value,
	1 AS EmailAddressTypeId
FROM dbo.Resources re
WHERE re.ResourceType <> 'R'
	AND re.ResourceEmail <> ''
	AND re.ResourceEmail IS NOT NULL

GO

-- Encounter
-- Audit
---From PracticeName table - satellite offices including internal facility encounters
SELECT ap.AppointmentId AS Id
	,COALESCE(CASE ap.ScheduleStatus
		WHEN 'P'
			THEN 1
		WHEN 'R'
			THEN 1
		WHEN 'X'
			THEN 14
		WHEN 'Q'
			THEN 13
		WHEN 'F'
			THEN 12
		WHEN 'N'
			THEN 11
		WHEN 'S'
			THEN 10
		WHEN 'Y'
			THEN 9
		WHEN 'O'
			THEN 8
		WHEN 'D'
			THEN 7
		WHEN 'A'
			THEN CASE pa.[Status]
					WHEN 'W'
						THEN 2
					WHEN 'M'
						THEN 3
					WHEN 'E'
						THEN 4
					WHEN 'G'
						THEN 5
					WHEN 'H'
						THEN 6
					WHEN 'U'
						THEN 15
					END
		END, 14) AS EncounterStatusId
	,CASE
		WHEN apt.ResourceId8 = 1 AND reASC.ResourceId IS NOT NULL
			THEN 3
		ELSE 1 
		END AS EncounterTypeId
	, CASE ap.ApptInsType
		WHEN 'M'
			THEN 1
		WHEN 'V'
			THEN 2
		WHEN 'A'
			THEN 3
		WHEN 'W'
			THEN 4
		ELSE 1
		END AS InsuranceTypeId
	,ap.PatientId AS PatientId
	,pn.PracticeId AS ServiceLocationId
	,EncounterStatusChangeReasonId AS EncounterStatusChangeReasonId
	,EncounterStatusChangeComment AS EncounterStatusChangeComment
	,CONVERT(int, confirmStatusPct.Id) AS ConfirmationStatusId
	,DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112)) AS StartDateTime 
	,DATEADD(hh, 1, DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112))) AS EndDateTime 
FROM dbo.Appointments ap
INNER JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
INNER JOIN dbo.PracticeName pn ON pn.PracticeType = 'P'
	AND ap.ResourceId2 - 1000 = pn.PracticeId
INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
INNER JOIN dbo.PracticeCodeTable pctQU ON pctQU.Code = apt.Question
	AND pctQU.ReferenceType = 'QUESTIONSETS'
LEFT JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
LEFT OUTER JOIN dbo.PracticeCodeTable confirmStatusPct ON SUBSTRING(confirmStatusPct.Code, 1, 1) = ap.ConfirmStatus
	AND confirmStatusPct.ReferenceType = 'CONFIRMSTATUS'
LEFT JOIN dbo.Resources reASC ON reASC.PracticeId = pn.PracticeId
	AND pn.LocationReference <> ''
	AND reASC.ResourceType = 'R'
	AND reASC.ServiceCode = '02'
	AND FieldValue = 'T' 
WHERE ap.AppTime >= 1 

UNION ALL

----At main office
SELECT ap.AppointmentId AS Id
	,COALESCE(CASE ap.ScheduleStatus
		WHEN 'P'
			THEN 1
		WHEN 'R'
			THEN 1
		WHEN 'X'
			THEN 14
		WHEN 'Q'
			THEN 13
		WHEN 'F'
			THEN 12
		WHEN 'N'
			THEN 11
		WHEN 'S'
			THEN 10
		WHEN 'Y'
			THEN 9
		WHEN 'O'
			THEN 8
		WHEN 'D'
			THEN 7
		WHEN 'A'
			THEN CASE pa.[Status]
					WHEN 'W'
						THEN 2
					WHEN 'M'
						THEN 3
					WHEN 'E'
						THEN 4
					WHEN 'G'
						THEN 5
					WHEN 'H'
						THEN 6
					WHEN 'U'
						THEN 15
					END
		END, 14) AS EncounterStatusId
	,1 AS EncounterTypeId
	,CASE ap.ApptInsType
		WHEN 'M'
			THEN 1
		WHEN 'V'
			THEN 2
		WHEN 'A'
			THEN 3
		WHEN 'W'
			THEN 4
		ELSE 1
		END AS InsuranceTypeId
	,ap.PatientId AS PatientId
	,pn.PracticeId AS ServiceLocationId
	,EncounterStatusChangeReasonId AS EncounterStatusChangeReasonId
	,EncounterStatusChangeComment AS EncounterStatusChangeComment
	,CONVERT(int, confirmStatusPct.Id) AS ConfirmationStatusId
	,DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112)) AS StartDateTime 
	,DATEADD(hh, 1, DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112))) AS EndDateTime 
FROM dbo.Appointments ap
INNER JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
INNER JOIN dbo.PracticeName pn ON pn.PracticeType = 'P'
	AND pn.LocationReference = ''
	AND ap.ResourceId2 = 0
INNER JOIN dbo.PracticeCodeTable pctQU ON pctQU.Code = apt.Question
	AND pctQU.ReferenceType = 'QUESTIONSETS'
LEFT OUTER JOIN dbo.PracticeCodeTable confirmStatusPct ON SUBSTRING(confirmStatusPct.Code, 1, 1) = ap.ConfirmStatus
	AND confirmStatusPct.ReferenceType = 'CONFIRMSTATUS'
LEFT JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
WHERE ap.AppTime >= 1

UNION ALL

----External Facility, not billing  (SL from Resources table)
SELECT ap.AppointmentId AS Id
	,COALESCE(CASE ap.ScheduleStatus
		WHEN 'P'
			THEN 1
		WHEN 'R'
			THEN 1
		WHEN 'X'
			THEN 14
		WHEN 'Q'
			THEN 13
		WHEN 'F'
			THEN 12
		WHEN 'N'
			THEN 11
		WHEN 'S'
			THEN 10
		WHEN 'Y'
			THEN 9
		WHEN 'O'
			THEN 8
		WHEN 'D'
			THEN 7
		WHEN 'A'
			THEN CASE pa.[Status]
					WHEN 'W'
						THEN 2
					WHEN 'M'
						THEN 3
					WHEN 'E'
						THEN 4
					WHEN 'G'
						THEN 5
					WHEN 'H'
						THEN 6
					WHEN 'U'
						THEN 15
					END
		END, 14) AS EncounterStatusId
	,1 AS EncounterTypeId
	,CASE ap.ApptInsType
		WHEN 'M'
			THEN 1
		WHEN 'V'
			THEN 2
		WHEN 'A'
			THEN 3
		WHEN 'W'
			THEN 4
		ELSE 1
		END AS InsuranceTypeId
	,ap.PatientId AS PatientId
	,model.GetPairId(1, re.ResourceId, @ServiceLocationMax) AS ServiceLocationId
	,EncounterStatusChangeReasonId AS EncounterStatusChangeReasonId
	,EncounterStatusChangeComment AS EncounterStatusChangeComment
	,CONVERT(int, confirmStatusPct.Id) AS ConfirmationStatusId
	,DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112)) AS StartDateTime 
	,DATEADD(hh, 1, DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112))) AS EndDateTime 
FROM dbo.Appointments ap
INNER JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId2
	AND re.ResourceType = 'R'
	AND re.ServiceCode = '02' 
INNER JOIN dbo.PracticeCodeTable pctQU ON pctQU.Code = apt.Question
	AND pctQU.ReferenceType = 'QUESTIONSETS'
LEFT OUTER JOIN dbo.PracticeCodeTable confirmStatusPct ON SUBSTRING(confirmStatusPct.Code, 1, 1) = ap.ConfirmStatus
	AND confirmStatusPct.ReferenceType = 'CONFIRMSTATUS'
LEFT JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
WHERE ap.AppTime >= 1

UNION ALL

----Appointments that are added, either via data conversion ("highlights"), billing or optical. Appointments with "ASC CLAIM" are excluded to avoid to encounterids for internal facility visits.  Surgery orders are included; consider removing.
SELECT ap.AppointmentId AS Id
	,7 AS EncounterStatusId
	,CASE ap.Comments 
		WHEN 'ADD VIA OPTICAL'
			THEN 4
		ELSE 1 END AS EncounterTypeId
	,CASE ap.ApptInsType
		WHEN 'M'
			THEN 1
		WHEN 'V'
			THEN 2
		WHEN 'A'
			THEN 3
		WHEN 'W'
			THEN 4
		ELSE 1
		END AS InsuranceTypeId
	,ap.PatientId AS PatientId
	,model.GetPairId(
	CASE 
		WHEN ResourceId2 = 0 OR ResourceId2 > 1000 THEN 0 
		ELSE 1 
		END, 
	CASE 
		WHEN ResourceId2 = 0 THEN pnMainOffice.PracticeId 
		WHEN ResourceId2 > 1000 THEN ResourceId2 - 1000 
		ELSE ResourceId2 
		END
	, @ServiceLocationMax) AS ServiceLocationId
	,EncounterStatusChangeReasonId AS EncounterStatusChangeReasonId
	,EncounterStatusChangeComment AS EncounterStatusChangeComment
	,NULL AS ConfirmationStatusId
	,CONVERT(datetime, ap.AppDate, 112) AS StartDateTime 
	,DATEADD(hh, 1, CONVERT(datetime, ap.AppDate, 112)) AS EndDateTime	
FROM dbo.Appointments ap
INNER JOIN dbo.PracticeCodeTable pct ON Referencetype = 'QUESTIONSETS' and Code = 'No Questions'
INNER JOIN dbo.PracticeName pnMainOffice ON 
	PracticeType = 'P' AND LocationReference = '' 
WHERE ap.AppTime < 1
	AND ap.Comments <> 'ASC CLAIM'

GO

-- EncounterLensesPrescription
----Don't have to handle internal facility encounters because IO users cannot get into the ASC appointment - it is invisible because the AppTime is 0
SELECT ClinicalId AS Id,
	pc.AppointmentId AS EncounterId,
	FindingDetail,
	ImageDescriptor
FROM dbo.PatientClinical pc
WHERE ClinicalType = 'A'
	AND (Substring(FindingDetail, 1, 8) = 'DISPENSE')
	AND [Status] = 'A'
	AND pc.AppointmentId <> 0
GO

-- EncounterStatusChangeReason
SELECT Id AS Id
	,Code AS Value
	,CASE AlternateCode	
		WHEN 'R'
			THEN 1		
		WHEN 'F'
			THEN 12
		WHEN 'N'
			THEN 11
		WHEN 'O'
			THEN 8
		WHEN 'Y'
			THEN 9
		WHEN 'Q'
			THEN 13
		WHEN 'S'
			THEN 10
		WHEN 'X'
			THEN 14
		ELSE 14
		END AS EncounterStatusId
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'APPT CANCEL REASONS'
GO

-- Equipment
SELECT AppTypeId AS Id,
	AppointmentType AS Abbreviation,
	CONVERT(bit, 1) AS IsSchedulable,
	AppointmentType AS [Name]
	,CONVERT(bit, 0) AS IsArchived
FROM dbo.AppointmentType
WHERE ResourceId7 > 0
GO


-- ExternalProviderClinicalSpecialtyType
----PracticeVendors specialty #1
SELECT CASE v.Id 
	WHEN 0
		THEN VendorId 
	WHEN 1
		THEN model.GetPairId(1, VendorId,  @ExternalProviderClinicalSpecialtyTypeMax)
	END AS Id,
	v.OrdinalId AS OrdinalId,
	VendorId AS ExternalProviderId,
	v.ClinicalSpecialtyTypeId AS ClinicalSpecialtyTypeId
FROM (SELECT
		0 AS Id,
		1 AS OrdinalId,
		VendorId AS VendorId,
		pct.id AS ClinicalSpecialtyTypeId,
		COUNT_BIG(*) AS Count
	FROM dbo.PracticeVendors pv
	LEFT JOIN dbo.PracticeCodeTable pct ON pv.VendorSpecialty = pct.Code
		AND pct.ReferenceType = 'SPECIALTY'
	WHERE pct.Id <> ''
		AND pct.Id IS NOT NULL
		AND VendorLastName <> ''
		AND VendorLastName IS NOT NULL
		AND VendorType = 'D'
	GROUP BY VendorId,
		pct.Id
		
--referring drs spec 2

	UNION ALL

----PracticeVendors specialty #2

	SELECT
		1 AS Id,	
		2 AS OrdinalId,
		VendorId AS VendorId,
		pct.id AS ClinicalSpecialtyTypeId,
		COUNT_BIG(*) AS Count
	FROM dbo.PracticeVendors pv
	LEFT JOIN dbo.PracticeCodeTable pct ON pv.VendorSpecOther = pct.Code
		AND pct.ReferenceType = 'SPECIALTY'
	WHERE pct.Id <> ''
		AND pct.Id IS NOT NULL
		AND VendorLastName <> ''
		AND VendorLastName IS NOT NULL
		AND VendorType = 'D'
	GROUP BY VendorId,
		pct.Id
	) AS v
GO

-- FeeScheduleContractAmount (no Good)
SELECT MIN(pfs.ServiceId) AS Id
	,es.Id AS EncounterServiceId
	, CONVERT(decimal(18,2), pfs.FacilityFee) AS FacilityAllowable
	,mfs.Id AS FeeScheduleContractId
	, CONVERT(decimal(18,2), pfs.AdjustmentRate) AS InsurerPaidPercent
	, CONVERT(decimal(18,2), pfs.Fee) AS InvoiceUnitFee
	, CONVERT(decimal(18,2), pfs.AdjustmentAllowed) AS OfficeAllowable
	,CASE pfs.DoctorId
		WHEN - 1
			THEN CONVERT(bigint, NULL)
		ELSE CONVERT(bigint, pfs.DoctorId)
		END AS ProviderId
	,CASE 
		WHEN (pfs.LocId BETWEEN 1 AND 999 OR pfs.LocId > 2000)
			THEN model.GetPairId(1, pfs.LocId, @ServiceLocationMax)
		WHEN pfs.LocId > 1000
			THEN (pfs.LocId - 1000)
		WHEN pfs.LocId = 0
			THEN PracticeId
		ELSE NULL
		END AS ServiceLocationId
FROM dbo.PracticeFeeSchedules pfs
INNER JOIN model.EncounterServices es ON es.Code = pfs.CPT
INNER JOIN model.Insurers ins ON ins.Id = pfs.PlanId
INNER JOIN model.GetMasterFeeSchedules() mfs ON mfs.NAME = ins.Name + SUBSTRING(pfs.StartDate, 1, 6) + SUBSTRING(pfs.EndDate, 5, 2)
INNER JOIN dbo.PracticeName pn ON pn.LocationReference = ''
	AND pn.PracticeType = 'P'
GROUP BY es.Id
	,pfs.FacilityFee
	,pfs.AdjustmentRate
	,pfs.Fee
	,pfs.AdjustmentAllowed
	,pfs.DoctorId
	,pfs.LocId
	,mfs.Id
	,pn.PracticeId

GO

-- FeeScheduleContract
SELECT MIN(pfs.ServiceId) AS Id,
	ins.Name + ' ' + CASE 
		WHEN ipt.Name IS NULL THEN ''
		ELSE ipt.Name
		END + substring(StartDate, 1, 6) + substring(enddate, 5, 2) AS [Name]
FROM dbo.PracticeFeeSchedules pfs
INNER JOIN model.Insurers ins ON ins.Id = pfs.PlanId
INNER JOIN model.InsurerPlanTypes ipt ON ins.InsurerPlanTypeId = ipt.Id
GROUP BY ins.Name,
	ipt.Name,
	pfs.StartDate,
	pfs.EndDate
GO

-- FinancialInformationType
SELECT v.Id AS Id,
	v.FinancialTypeGroupId AS FinancialTypeGroupId,
	v.IsPrintOnStatement AS IsPrintOnStatement,
	SUBSTRING(v.Code, 1, (dbo.GetMax((CHARINDEX('-', v.code) - 2),0))) AS [Name],
	v.ShortName AS ShortName,
	v.IsArchived,
	v.OrdinalId
FROM (SELECT CASE
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '!'
			THEN 1
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '|'
			THEN 2
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '?'
			THEN 3
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '%'
			THEN 4
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'D'
			THEN 5
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '&'
			THEN 6
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = ']'
			THEN 7
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = '0'
			THEN 8
		WHEN SUBSTRING(code, (dbo.GetMax((CHARINDEX(' - ', Code)+3), 0)), LEN(Code)) = 'V'
			THEN 9
		ELSE pct.Id + 1000
		END AS Id,
		NULL AS FinancialTypeGroupId,
		LetterTranslation AS ShortName,
		Code AS Code,
		pct.Rank AS OrdinalId,
		COUNT_BIG(*) AS Count,
		CONVERT(bit, 0) AS IsArchived,
		CASE pct.Exclusion WHEN 'T' THEN CONVERT(bit, 0) ELSE CONVERT(bit, 1) END AS IsPrintOnStatement
	FROM dbo.PracticeCodeTable pct
	WHERE pct.ReferenceType = 'PAYMENTTYPE'		
		AND (pct.AlternateCode = '' OR pct.AlternateCode IS NULL)
	GROUP BY pct.Id,
		LetterTranslation,
		Code,
		pct.Rank,
		pct.Exclusion
	) AS v
GO


-- IdentifierCode
SELECT  
CONVERT(INT,ROW_NUMBER() OVER (ORDER BY z.IdentifierCodeTypeId, z.BillingOrganizationId, z.ExternalProviderId, z.ProviderId, z.ServiceLocationId, z.Value)) AS Id
,* 
FROM (
---Taxonomy IdentifierCode - Index 0
----Identifier Code Taxonomy BillingOrganization from PracticeName (0)
SELECT pnBillOrg.PracticeId AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	1 AS IdentifierCodeTypeId,
	NULL AS ProviderId,
	NULL AS ServiceLocationId,
	pnBillOrg.PracticeTaxonomy AS Value
FROM dbo.PracticeName pnBillOrg
WHERE pnBillOrg.PracticeType = 'P'
	AND pnBillOrg.PracticeTaxonomy <> ''
	AND pnBillOrg.PracticeTaxonomy IS NOT NULL
	
UNION ALL

----Identifier Code Taxonomy BillingOrganization w PracticeAffiliations.OrgOverride/Override - Resources (1)
--*/*(no index)*/*
SELECT model.GetPairId(1, v.ResourceId, 110000000) AS BillingOrganizationId,
	v.ExternalProviderId AS ExternalProviderId,
	v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
	v.ProviderId AS ProviderId,
	NULL AS ServiceLocationId,
	v.Value AS Value
FROM (SELECT
		NULL AS ExternalProviderId,
		1 AS IdentifierCodeTypeId,
		NULL AS ProviderId,
		reBillOrg.Vacation AS Value, 
		reBillOrg.ResourceId AS ResourceId
	FROM dbo.Resources reBillOrg
	INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = reBillOrg.ResourceId
		AND pa.ResourceType = 'R'
		AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
	WHERE reBillOrg.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND reBillOrg.Vacation <> ''
		AND reBillOrg.Vacation IS NOT NULL
		GROUP BY reBillOrg.Vacation,
			reBillOrg.ResourceId
	) AS v
	
UNION ALL

----Identifier Code Taxonomy Provider (human) w ServiceLocation from PracticeName (Index 0)
SELECT NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	1 AS IdentifierCodeTypeId,
	p.Id AS ProviderId,
	NULL AS ServiceLocationId,
	re.Vacation AS Value
FROM dbo.Resources re
INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
INNER JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND re.Vacation <> ''
	AND re.Vacation IS NOT NULL

UNION ALL

-----Identifier Code Taxonomy Provider (facility) ServiceLocation from PracticeName (always); IdentifierCodes from Resources
SELECT NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	1 AS IdentifierCodeTypeId,
	p.Id AS ProviderId,
	NULL AS ServiceLocationId,
	reASC.Vacation AS Value
FROM dbo.Resources reASC
INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeId = reASC.PracticeId
	AND pnServLoc.PracticeType = 'P'
	AND LocationReference <> ''
LEFT JOIN model.Providers p ON p.UserId IS NULL
	AND p.ServiceLocationId = pnServLoc.PracticeId
WHERE reASC.ResourceType = 'R'
	AND reASC.ServiceCode = '02'
	AND reASC.Billable = 'Y'
	AND pic.FieldValue = 'T'
	AND reASC.Vacation <> ''
	AND reASC.Vacation IS NOT NULL

UNION ALL

------Identifier Code Taxonomy Provider w OrgOverride/Override w ServLoc from PracticeName (Index 0)
SELECT v.BillingOrganizationId AS BillingOrganizationId,
v.ExternalProviderId AS ExternalProviderId,
v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
v.ProviderId AS ProviderId,
NULL AS ServiceLocationId,
v.Value AS Value
FROM(SELECT 
	NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	1 AS IdentifierCodeTypeId,
	re.Vacation AS Value,
	re.ResourceId AS ResourceId,
	re.ResourceType AS ResourceType,
	p.Id AS ProviderId
FROM dbo.Resources re
INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
	AND pa.ResourceType = 'R'
	AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
LEFT JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.UserId IS NOT NULL
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND re.Vacation <> ''
	AND re.Vacation IS NOT NULL
GROUP BY re.Vacation,
	re.ResourceId,
	re.ResourceType,
	p.Id
) AS v

UNION ALL

---NPI IdentifierCode (Index 1)
----BillingOrganization from PracticeName table (Index 0)
SELECT pnBillOrg.PracticeId AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	2 AS IdentifierCodeTypeId,
	NULL AS ProviderId,
	NULL AS ServiceLocationId,
	pnBillOrg.PracticeNPI AS Value
FROM dbo.PracticeName pnBillOrg
WHERE pnBillOrg.PracticeType = 'P'
	AND pnBillOrg.PracticeNPI <> ''
	AND pnBillOrg.PracticeNPI IS NOT NULL

UNION ALL

----IdentifierCode NPI BillingOrganization - Resources table where PracticeAffiliation.Override or OrgOverride ONLY
SELECT model.GetPairId(1, v.ResourceId, 110000000) AS BillingOrganizationId,
	v.ExternalProviderId AS ExternalProviderId,
	v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
	v.ProviderId AS ProviderId,
	NULL AS ServiceLocationId,
	v.Value AS Value
FROM (SELECT 
		NULL AS ExternalProviderId,
		2 AS IdentifierCodeTypeId,
		NULL AS ProviderId,
		reBillOrg.NPI AS Value,
		reBillOrg.ResourceId AS ResourceId
	FROM dbo.Resources reBillOrg
	INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = reBillOrg.ResourceId
		AND pa.ResourceType = 'R'
		AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
	WHERE reBillOrg.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND reBillOrg.NPI <> ''
		AND reBillOrg.NPI IS NOT NULL
	GROUP BY reBillOrg.ResourceId,
		reBillOrg.NPI,
		reBillOrg.ResourceType
	) AS v
	
UNION ALL

---IdentifierCode NPI Provider (human providers)
SELECT NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	2 AS IdentifierCodeTypeId,
	p.Id AS ProviderId,
	NULL AS ServiceLocationId,
	re.NPI AS Value
FROM dbo.Resources re
INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
INNER JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.UserId IS NOT NULL
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND re.NPI <> ''
	AND re.NPI IS NOT NULL

UNION ALL

---IdentifierCode NPI Provider (Facility) service location from PracticeName table, identifier code from Resources 
SELECT NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	2 AS IdentifierCodeTypeId,
	p.Id AS ProviderId,
	NULL AS ServiceLocationId,
	reASC.NPI AS Value
FROM dbo.Resources reASC
INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeId = reASC.PracticeId
	AND pnServLoc.PracticeType = 'P'
INNER JOIN model.Providers p ON p.UserId IS NULL
	AND p.ServiceLocationId = pnServLoc.PracticeId
WHERE reASC.ResourceType = 'R'
	AND reASC.ServiceCode = '02'
	AND reASC.Billable = 'Y'
	AND pic.FieldValue = 'T'
	AND reASC.NPI <> ''
	AND reASC.NPI IS NOT NULL
	AND pnServLoc.LocationReference <> ''

UNION ALL

----IdentifierCode NPI Provider (Human) w OrgOverride/Override
SELECT v.BillingOrganizationId AS BillingOrganizationId,
	v.ExternalProviderId AS ExternalProviderId,
	v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
	v.ProviderId,
	NULL AS ServiceLocationId,
	v.Value AS Value
FROM(SELECT	
		NULL AS BillingOrganizationId,
		NULL AS ExternalProviderId,
		2 AS IdentifierCodeTypeId,
		re.NPI AS Value,
		re.ResourceId AS ResourceId
		,p.Id AS ProviderId
	FROM dbo.Resources re
	INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
		AND pa.ResourceType = 'R'
		AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
	INNER JOIN model.Providers p ON p.UserId = re.ResourceId
		AND p.UserId IS NOT NULL
		AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
	WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND re.NPI <> ''
		AND re.NPI IS NOT NULL
	GROUP BY re.NPI,
		re.ResourceId,
		re.ResourceType,
		p.Id
	) AS v

UNION ALL

----IdentifierCode NPI Provider (Human) w OrgOverride/Override
SELECT v.BillingOrganizationId AS BillingOrganizationId,
	v.ExternalProviderId AS ExternalProviderId,
	v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
	v.ProviderId,
	NULL AS ServiceLocationId,
	v.Value AS Value
FROM(SELECT	
		NULL AS BillingOrganizationId,
		NULL AS ExternalProviderId,
		2 AS IdentifierCodeTypeId,
		re.NPI AS Value,
		re.ResourceId AS ResourceId
		,p.Id AS ProviderId
	FROM dbo.Resources re
	INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
		AND pa.ResourceType = 'R'
		AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
	INNER JOIN model.Providers p ON p.UserId = re.ResourceId
		AND p.UserId IS NOT NULL
		AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'PersonBillingOrganization')
	WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND re.NPI <> ''
		AND re.NPI IS NOT NULL
	GROUP BY re.NPI,
		re.ResourceId,
		re.ResourceType,
		p.Id
	) AS v

UNION ALL

------Identifier Code Taxonomy Provider w OrgOverride/Override w ServLoc from PracticeName (Index 0)
SELECT v.BillingOrganizationId AS BillingOrganizationId,
v.ExternalProviderId AS ExternalProviderId,
v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
v.ProviderId AS ProviderId,
NULL AS ServiceLocationId,
v.Value AS Value
FROM(SELECT 
	NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	1 AS IdentifierCodeTypeId,
	re.Vacation AS Value,
	re.ResourceId AS ResourceId,
	re.ResourceType AS ResourceType,
	p.Id AS ProviderId
FROM dbo.Resources re
INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
	AND pa.ResourceType = 'R'
	AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
LEFT JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.UserId IS NOT NULL
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'PersonBillingOrganization')
WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND re.Vacation <> ''
	AND re.Vacation IS NOT NULL
GROUP BY re.Vacation,
	re.ResourceId,
	re.ResourceType,
	p.Id
) AS v

UNION ALL

---IdentifierCode TaxId Provider w PracticeAffiliation.OrgOverride/Override
SELECT v.BillingOrganizationId AS BillingOrganizationId,
	v.ExternalProviderId AS ExternalProviderId,
	v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
	v.ProviderId,
	NULL AS ServiceLocationId,
	v.Value AS Value	
FROM (SELECT
		NULL AS BillingOrganizationId,
		NULL AS ExternalProviderId,
		7 AS IdentifierCodeTypeId,
		re.ResourceTaxId AS Value,
		re.ResourceId AS ResourceId,
		p.Id AS ProviderId
	FROM dbo.Resources re
	INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
		AND pa.ResourceType = 'R'
		AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
	INNER JOIN model.Providers p ON p.UserId = re.ResourceId
		AND p.UserId IS NOT NULL
		AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'PersonBillingOrganization')
	WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND re.ResourceTaxId <> ''
		AND re.ResourceTaxId IS NOT NULL 
		AND re.ResourceTaxId NOT LIKE '%-%-%'
	GROUP BY re.ResourceTaxId,
		re.ResourceId
		,p.Id
	) AS v

UNION ALL

---IdentifierCode NPI ExternalProvider (Index 5)
SELECT NULL AS BillingOrganizationId,
	Id AS ExternalProviderId,
	2 AS IdentifierCodeTypeId,
	NULL AS ProviderId,
	NULL AS ServiceLocationId,
	ec.LegacyNpi AS Value
FROM model.ExternalContacts ec
WHERE __EntityType__ = 'ExternalProvider'
	AND len(ec.LegacyNpi) = 10 
	AND ISNUMERIC(ec.LegacyNpi) = 1
	AND ec.LastNameOrEntityName <> ''
	AND ec.LastNameOrEntityName IS NOT NULL

UNION ALL

---IdentifierCode Taxonomy ExternalProvider (Index 5)
SELECT NULL AS BillingOrganizationId,
	Id AS ExternalProviderId,
	1 AS IdentifierCodeTypeId,
	NULL AS ProviderId,
	NULL AS ServiceLocationId,
	ec.LegacyTaxonomy AS Value
FROM model.ExternalContacts ec
WHERE __EntityType__ = 'ExternalProvider'
	AND ec.LastNameOrEntityName <> ''
	AND ec.LastNameOrEntityName IS NOT NULL
	AND ec.LegacyTaxonomy <> ''

UNION ALL

---IdentifierCode NPI ExternalOrganization (maybe a clinic is the referral source) IdentifierCode --we don't support NPIs for external organizations in old IO
---IdentifierCode NPI ServiceLocation  from PracticeName table (Index 0)
SELECT NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	2 AS IdentifierCodeTypeId,
	NULL AS ProviderId,
	pnServLoc.PracticeId AS ServiceLocationId,
	CASE
		WHEN len(pnServLoc.PracticeNPI) = 10
			THEN pnServLoc.PracticeNPI
		ELSE pnMainLocation.PracticeNPI
		END AS Value
FROM dbo.PracticeName pnServLoc
INNER JOIN dbo.PracticeName pnMainLocation ON pnMainLocation.PracticeType = 'P'
	AND pnMainLocation.LocationReference = ''
	AND len(pnMainLocation.PracticeNPI) = 10
WHERE  pnServLoc.PracticeType = 'P'
	AND pnServLoc.PracticeNPI <> ''
	AND pnServLoc.PracticeNPI IS NOT NULL

UNION ALL

----IdentifierCode NPI ServiceLocation - Resources table 
SELECT NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	2 AS IdentifierCodeTypeId,
	NULL AS ProviderId,
	model.GetPairId(1, reServLoc.ResourceId, 110000000) AS ServiceLocationId,
	reServLoc.NPI AS Value
	FROM dbo.Resources reServLoc
INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = reServLoc.PracticeId
	AND pnBillOrg.PracticeType = 'P'
WHERE reServLoc.ResourceType = 'R'
	AND reServLoc.ServiceCode = '02'
	AND reServLoc.NPI <> ''
	AND reServLoc.NPI IS NOT NULL

UNION ALL

---IdentifierCode SSN (Index 2) - BillingOrganization - From PracticeName table (Index 0)
SELECT pnBillOrg.PracticeId AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	3 AS IdentifierCodeTypeId,
	NULL AS ProviderId,
	NULL AS ServiceLocationId,
	SUBSTRING(pnBillOrg.PracticeTaxId, 1, 3) + SUBSTRING(pnBillOrg.PracticeTaxId, 5, 2) + SUBSTRING(pnBillOrg.PracticeTaxId, 8, 4) AS Value
FROM dbo.PracticeName pnBillOrg
WHERE pnBillOrg.PracticeType = 'P'
	AND pnBillOrg.PracticeTaxId LIKE '%-%-%'

UNION ALL

----IdentifierCode SSN - BillingOrganization - From Resources (human) where PracticeAffiliation.OrgOverride/Override = T
SELECT model.GetPairId(1, reBillOrg.ResourceId, 110000000) AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	3 AS IdentifierCodeTypeId,
	NULL AS ProviderId,
	NULL AS ServiceLocationId,
	SUBSTRING(reBillOrg.ResourceTaxId, 1, 3) + SUBSTRING(reBillOrg.ResourceTaxId, 5, 2) + SUBSTRING(reBillOrg.ResourceTaxId, 8, 4) AS Value
FROM dbo.Resources reBillOrg
INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = reBillOrg.ResourceId
	AND pa.ResourceType = 'R'
	AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
WHERE reBillOrg.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND reBillOrg.ResourceTaxId LIKE '%-%-%'
GROUP BY  '2' + '_' + '1' + '_' + '10' + '_' + CONVERT(nvarchar, reBillOrg.ResourceId) 
	,model.GetPairId(1, reBillOrg.ResourceId, 110000000) 
	,SUBSTRING(reBillOrg.ResourceTaxId, 1, 3) + SUBSTRING(reBillOrg.ResourceTaxId, 5, 2) + SUBSTRING(reBillOrg.ResourceTaxId, 8, 4) 

----BillingOrganization Resources - external facilities wouldn't have an SSN
----Patient SSN saved in Patient entity

UNION ALL

----IdentifierCode SSN - Provider (Human)
SELECT NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	3 AS IdentifierCodeTypeId,
	p.Id AS ProviderId,
	NULL AS ServiceLocationId,
	SUBSTRING(re.ResourceTaxId, 1, 3) + SUBSTRING(re.ResourceTaxId, 5, 2) + SUBSTRING(re.ResourceTaxId, 8, 4) AS Value
FROM dbo.Resources re
INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
INNER JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.UserId IS NOT NULL
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND re.ResourceTaxId LIKE '%-%-%'

UNION ALL

---IdentifierCode SSN - Provider  w PracticeAffiliation.OrgOverride/Override
SELECT NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	3 AS IdentifierCodeTypeId,
	p.Id AS ProviderId,
	NULL AS ServiceLocationId,
	SUBSTRING(re.ResourceTaxId, 1, 3) + SUBSTRING(re.ResourceTaxId, 5, 2) + SUBSTRING(re.ResourceTaxId, 8, 4) AS Value
FROM dbo.Resources re
INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
	AND pa.ResourceType = 'R'
	AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
INNER JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.UserId IS NOT NULL
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND re.ResourceTaxId LIKE '%-%-%'
GROUP BY '2' + '_' + '0' + '_' + '7' + '_' + CONVERT(nvarchar, re.ResourceId) + '11' + '_' + CONVERT(nvarchar,re.ResourceId) + '13'
	,p.Id
	,SUBSTRING(re.ResourceTaxId, 1, 3) + SUBSTRING(re.ResourceTaxId, 5, 2) + SUBSTRING(re.ResourceTaxId, 8, 4) 

UNION ALL

--UPIN IdentifierCode - supposed to no longer be used; won't script unless we have to - for ProviderBillingOrganizationServiceLocation and ExternalProvider

---IdentifierCode License Provider
SELECT NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	5 AS IdentifierCodeTypeId,
	p.Id AS ProviderId,
	NULL AS ServiceLocationId,
	re.ResourceLicence AS Value
FROM dbo.Resources re
INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
INNER JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.UserId IS NOT NULL
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND re.ResourceLicence <> ''
	AND re.ResourceLicence IS NOT NULL

UNION ALL

---IdentifierCode License Provider  w PracticeAffiliation.OrgOverride/Override
SELECT v.BillingOrganizationId AS BillingOrganizationId,
	v.ExternalProviderId AS ExternalProviderId,
	v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
	v.ProviderId,
	NULL AS ServiceLocationId,
	v.Value AS Value
FROM(SELECT 
		NULL AS BillingOrganizationId,
		NULL AS ExternalProviderId,
		5 AS IdentifierCodeTypeId,
		re.ResourceLicence AS Value,
		re.ResourceId AS ResourceId,
		p.Id AS ProviderId
	FROM dbo.Resources re
	INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
		AND pa.ResourceType = 'R'
		AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
	INNER JOIN model.Providers p ON p.UserId = re.ResourceId
		AND p.UserId IS NOT NULL
		AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
	WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND re.ResourceLicence <> ''
		AND re.ResourceLicence IS NOT NULL
	GROUP BY re.ResourceLicence,
		re.ResourceId,
		re.ResourceType,
		p.Id
) AS v

UNION ALL

---IdentifierCode DEA Provider
SELECT NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	6 AS IdentifierCodeTypeId,
	p.Id AS ProviderId,
	NULL AS ServiceLocationId,
	re.ResourceDEA AS Value
FROM dbo.Resources re
INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
INNER JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.UserId IS NOT NULL
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND re.ResourceDEA <> ''
	AND re.ResourceDEA IS NOT NULL

UNION ALL

----IdentifierCode DEA Provider  w PracticeAffiliation.OrgOverride/Override
SELECT v.BillingOrganizationId AS BillingOrganizationId,
	v.ExternalProviderId AS ExternalProviderId,
	v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
	v.ProviderId,
	NULL AS ServiceLocationId,
	v.Value AS Value
FROM(SELECT 
		NULL AS BillingOrganizationId,
		NULL AS ExternalProviderId,
		6 AS IdentifierCodeTypeId,
		re.ResourceDEA AS Value,
		re.ResourceId AS ResourceId,
		p.Id AS ProviderId
	FROM dbo.Resources re
	INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
		AND pa.ResourceType = 'R'
		AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
	INNER JOIN model.Providers p ON p.UserId = re.ResourceId
		AND p.UserId IS NOT NULL
		AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
	WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND re.ResourceDEA <> ''
		AND re.ResourceDEA IS NOT NULL 
	GROUP BY re.ResourceDEA,
		re.ResourceId,
		re.ResourceType,
		p.Id
) AS v

UNION ALL

----IdentifiderCode TaxId (Index 6) BillingOrganization From PracticeName table (Index 1)
SELECT pnBillOrg.PracticeId AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	7 AS IdentifierCodeTypeId,
	NULL AS ProviderId,
	NULL AS ServiceLocationId,
	pnBillOrg.PracticeTaxId AS Value
FROM dbo.PracticeName pnBillOrg
WHERE pnBillOrg.PracticeType = 'P'
	AND pnBillOrg.PracticeTaxId <> ''
	AND pnBillOrg.PracticeTaxId IS NOT NULL
	AND pnBillOrg.PracticeTaxId NOT LIKE '%-%-%'
	
UNION ALL

----IdentifierCode TaxId BillingOrganization w OrgOverride/Override (Resources)
SELECT model.GetPairId(1, v.ResourceId, 110000000) AS BillingOrganizationId,
	v.ExternalProviderId AS ExternalProviderId,
	v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
	v.ProviderId AS ProviderId,
	NULL AS ServiceLocationId,
	v.Value AS Value
FROM(SELECT
		NULL AS ExternalProviderId,
		7 AS IdentifierCodeTypeId,
		NULL AS ProviderId,
		reBillOrg.ResourceTaxId AS Value,
		reBillOrg.ResourceId AS ResourceId
	FROM dbo.Resources reBillOrg
	INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = reBillOrg.ResourceId
		AND pa.ResourceType = 'R'
		AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
	WHERE reBillOrg.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND reBillOrg.ResourceTaxId <> ''
		AND reBillOrg.ResourceTaxId IS NOT NULL
		AND reBillOrg.ResourceTaxId NOT LIKE '%-%-%'
	GROUP BY reBillOrg.ResourceId,
		reBillOrg.ResourceTaxId		
	) AS v

UNION ALL

----IdentifierCode TaxId Provider - includes Internal Facilities as providers
SELECT NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	7 AS IdentifierCodeTypeId,
	p.Id AS ProviderId,
	NULL AS ServiceLocationId,
	re.ResourceTaxId AS Value
FROM dbo.Resources re
INNER JOIN dbo.PracticeName pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
	AND pnBillOrg.PracticeType = 'P'
INNER JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.UserId IS NOT NULL
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND re.ResourceTaxId <> ''
	AND re.ResourceTaxId IS NOT NULL
	AND re.ResourceTaxId NOT LIKE '%-%-%'

UNION ALL

---IdentifierCode TaxId Provider w PracticeAffiliation.OrgOverride/Override
SELECT v.BillingOrganizationId AS BillingOrganizationId,
	v.ExternalProviderId AS ExternalProviderId,
	v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
	v.ProviderId,
	NULL AS ServiceLocationId,
	v.Value AS Value	
FROM (SELECT
		NULL AS BillingOrganizationId,
		NULL AS ExternalProviderId,
		7 AS IdentifierCodeTypeId,
		re.ResourceTaxId AS Value,
		re.ResourceId AS ResourceId,
		p.Id AS ProviderId
	FROM dbo.Resources re
	INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
		AND pa.ResourceType = 'R'
		AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
	INNER JOIN model.Providers p ON p.UserId = re.ResourceId
		AND p.UserId IS NOT NULL
		AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
	WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND re.ResourceTaxId <> ''
		AND re.ResourceTaxId IS NOT NULL 
		AND re.ResourceTaxId NOT LIKE '%-%-%'
	GROUP BY re.ResourceTaxId,
		re.ResourceId
		,p.Id
	) AS v

 UNION ALL

----IdentifierCode EValidation (Index 7) - Provider
SELECT NULL AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	8 AS IdentifierCodeTypeId,
	p.Id AS ProviderId,
	NULL AS ServiceLocationId,
	re.StartTime7 AS Value
FROM dbo.Resources re
INNER JOIN dbo.PracticeName pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
INNER JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.UserId IS NOT NULL
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND re.StartTime7 <> ''
	AND re.StartTime7 IS NOT NULL

UNION ALL

----IdentifierCode EValidation - Provider w Override
SELECT v.BillingOrganizationId AS BillingOrganizationId,
	v.ExternalProviderId AS ExternalProviderId,
	v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
	v.ProviderId,
	NULL AS ServiceLocationId,
	v.Value AS Value
FROM (SELECT 
		NULL AS BillingOrganizationId,
		NULL AS ExternalProviderId,
		8 AS IdentifierCodeTypeId,
		re.StartTime7 AS Value,
		re.ResourceId AS ResourceId,
		p.Id AS ProviderId
	FROM dbo.Resources re
	INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
		AND pa.ResourceType = 'R'
		AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
	INNER JOIN model.Providers p ON p.UserId = re.ResourceId
		AND p.UserId IS NOT NULL
		AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
	WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND re.StartTime7 <> ''
		AND re.StartTime7 IS NOT NULL 
	GROUP BY re.StartTime7,
		re.ResourceId,
		p.Id
	) AS v

	UNION ALL

----IdentifierCode CLIA - Index 8
----Identifier Code CLIA BillingOrganization from PracticeName (0)
SELECT pnBillOrg.PracticeId AS BillingOrganizationId,
	NULL AS ExternalProviderId,
	9 AS IdentifierCodeTypeId,
	NULL AS ProviderId,
	NULL AS ServiceLocationId,
	pnBillOrg.CatArray1 AS Value
FROM dbo.PracticeName pnBillOrg
WHERE pnBillOrg.PracticeType = 'P'
	AND pnBillOrg.CatArray1 <> ''
	AND pnBillOrg.CatArray1 IS NOT NULL
	
UNION ALL

----Identifier Code CLIA BillingOrganization w PracticeAffiliations.OrgOverride/Override - Resources (1)
SELECT model.GetPairId(1, v.ResourceId, 110000000) AS BillingOrganizationId,
	v.ExternalProviderId AS ExternalProviderId,
	v.IdentifierCodeTypeId AS IdentifierCodeTypeId,
	v.ProviderId AS ProviderId,
	NULL AS ServiceLocationId,
	v.Value AS Value
FROM (SELECT
		NULL AS ExternalProviderId,
		9 AS IdentifierCodeTypeId,
		NULL AS ProviderId,
		reBillOrg.EndTime6 AS Value, 
		reBillOrg.ResourceId AS ResourceId
	FROM dbo.Resources reBillOrg
	INNER JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = reBillOrg.ResourceId
		AND pa.ResourceType = 'R'
		AND (pa.[Override] = 'Y' OR pa.OrgOverride = 'Y')
	WHERE reBillOrg.ResourceType IN ('D', 'Q', 'Z', 'Y')
		AND reBillOrg.EndTime6 <> ''
		AND reBillOrg.EndTime6 IS NOT NULL
		GROUP BY reBillOrg.EndTime6,
			reBillOrg.ResourceId
	) AS v
)z
GROUP BY z.IdentifierCodeTypeId, z.BillingOrganizationId, z.ExternalProviderId, z.ProviderId, z.ServiceLocationId, z.Value

GO

-- InsurerEncounterService
SELECT ins.Id AS InsurerId
	,es.Id AS EncounterServiceId
	, CASE 
		WHEN CHARINDEX('T', mf.Description5, 1) = 1 
			THEN CONVERT(int, SUBSTRING(mf.Description5, 2, 5))
		ELSE CONVERT(int, SUBSTRING(mf.Description5, 1, 5)) 
		END AS PostOperativePeriod
FROM dbo.ModifierRules mf
INNER JOIN model.EncounterServices es ON es.Code = mf.CPT
INNER JOIN model.Insurers ins ON CASE WHEN mf.BusinessClassId = 'AMERIH'
				THEN 1
			WHEN mf.BusinessClassId = 'BLUES'
				THEN 2
			WHEN mf.BusinessClassId = 'COMM'
				THEN 3
			WHEN mf.BusinessClassId = 'MCAIDFL'
				THEN 4
			WHEN mf.BusinessClassId = 'MCAIDNC'
				THEN 5
			WHEN mf.BusinessClassId= 'MCAIDNJ'
				THEN 6
			WHEN mf.BusinessClassId = 'MCAIDNV'
				THEN 7
			WHEN mf.BusinessClassId = 'MCAIDNY'
				THEN 8
			WHEN mf.BusinessClassId = 'MCARENJ'
				THEN 9
			WHEN mf.BusinessClassId = 'MCARENV'
				THEN 10
			WHEN mf.BusinessClassId = 'MCARENY'
				THEN 11
			WHEN mf.BusinessClassId = 'TEMPLATE'
				THEN 12
			WHEN mf.BusinessClassId IS NULL
				THEN 3
			WHEN mf.BusinessClassId = ''
				THEN 3 
			END = ins.InsurerBusinessClassId
WHERE mf.Description5 IS NOT NULL 
	AND mf.Description5 <> '' 
	AND mf.Description5 <> 'T'
GROUP BY ins.Id, es.Id, mf.Description5
GO

-- InsurerFeeScheduleContract
SELECT MIN(pfs.ServiceId) AS Id,
	CASE 
		WHEN EndDate = ''
			THEN NULL
		ELSE CONVERT(datetime, EndDate, 112)
		END AS EndDateTime,
	mfs.Id AS FeeScheduleContractId,
	PlanId AS InsurerId,
	CONVERT(datetime, StartDate, 112) AS StartDateTime
FROM model.Insurers ins
INNER JOIN dbo.PracticeFeeSchedules pfs ON ins.Id = pfs.PlanId
INNER JOIN model.GetMasterFeeSchedules() mfs ON mfs.NAME = ins.Name + SUBSTRING(startdate, 1, 6) + SUBSTRING(enddate, 3, 2)
GROUP BY EndDate,
	Ins.Id,
	StartDate,
	Ins.Name,
	mfs.Id,
	PlanId

GO

-- InsurerPlanType
SELECT Id AS Id,
	Code AS [Name]
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'PLANTYPE'
GO

-- ClinicalInvoiceProvider (no index)
-------humans with service location from PracticeName [no override, handled in claim file only]
SELECT model.GetBigPairId(re.ResourceId, ap.AppointmentId, @ClinicalInvoiceProviderMax) AS Id
	,ap.AppointmentId AS EncounterId
	,p.Id AS ProviderId
FROM dbo.Appointments ap
INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
INNER JOIN dbo.PracticeName pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
	AND pnBillOrg.PracticeType = 'P'
INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P' 
	AND (ap.ResourceId2 > 1000 OR ap.ResourceId2 = 0)
	AND pnServLoc.PracticeId = CASE 
		WHEN ap.ResourceId2 > 1000
			THEN ap.ResourceId2 - 1000
		ELSE (SELECT PracticeId AS PracticeId FROM dbo.PracticeName WHERE LocationReference = '' AND PracticeType = 'P')
		END
INNER JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.IsBillable = CASE WHEN re.Billable = 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END 
	AND p.IsEMR = CASE re.GoDirect WHEN 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END
	AND p.UserId IS NOT NULL
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
WHERE re.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND ap.Comments <> 'ASC CLAIM'
GROUP BY re.ResourceId
	,re.ResourceType
	,ap.AppointmentId
	,ap.ResourceId2
	,pnBillOrg.PracticeId
	,pnServLoc.PracticeId
	,p.Id

UNION ALL

----humans where service location is from Resources [no override; handled in claim file only]
SELECT model.GetBigPairId(re.ResourceId, ap.AppointmentId, @ClinicalInvoiceProviderMax) AS Id
	,ap.AppointmentId AS EncounterId
	,p.Id AS ProviderId
FROM dbo.Appointments ap
INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId1
INNER JOIN dbo.PracticeName AS pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
	AND pnBillOrg.PracticeType = 'P'
INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceId = ap.ResourceId2
	AND reServLoc.ResourceType = 'R'
	AND reServLoc.ServiceCode = '02'
INNER JOIN model.Providers p ON p.UserId = re.ResourceId
	AND p.IsBillable = CASE WHEN re.Billable = 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END 
	AND p.IsEMR = CASE re.GoDirect WHEN 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END
	AND p.UserId IS NOT NULL
	AND p.BillingOrganizationId IN (SELECT Id FROM model.BillingOrganizations WHERE __EntityType__ = 'BillingOrganization')
WHERE re.ResourceType in ('D', 'Q', 'Z','Y')
GROUP BY re.ResourceId,
	ap.AppointmentId,
	reServLoc.ResourceId,
	re.ResourceId,
	pnBillOrg.PracticeId,
	p.Id

UNION ALL

----Facility provider (PracticeName for billing organization and service location).  The self-join on dbo.Appointments is to find the EncounterId (which is the surgeon's appt id)
SELECT model.GetBigPairId(reASC.ResourceId, iv.EncounterId, @ClinicalInvoiceProviderMax) AS Id
	,apEncounter.AppointmentId AS EncounterId
	,prov.Id AS ProviderId
FROM dbo.Appointments ap	
INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
INNER JOIN model.Patients p ON p.Id = ap.PatientId
INNER JOIN dbo.AppointmentType apt ON ap.AppTypeId = apt.AppTypeId
	AND apt.ResourceId8 = 1
INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId
	AND apEncounter.AppTypeId = ap.AppTypeId
	AND apEncounter.AppDate = ap.AppDate
	AND apEncounter.AppTime > 0 
	AND ap.AppTime = 0
	AND apEncounter.ScheduleStatus = ap.ScheduleStatus
	AND ap.ScheduleStatus = 'D'
	AND apEncounter.ResourceId1 = ap.ResourceId1
	AND apEncounter.ResourceId2 = ap.ResourceId2
INNER JOIN dbo.PracticeName pnServLoc ON pnServLoc.PracticeType = 'P'
	AND pnServLoc.PracticeId = ap.ResourceId2 - 1000
	AND PracticeType = 'P'
	AND LocationReference <> ''
INNER JOIN dbo.Resources reASC ON reASC.PracticeId = pnServLoc.PracticeId
	AND reASC.ResourceType = 'R'
	AND reASC.ServiceCode = '02'
	AND reASC.Billable = 'Y'
	AND pic.FieldValue = 'T'
INNER JOIN model.Invoices iv ON iv.EncounterId = apEncounter.AppointmentId
LEFT JOIN model.Providers prov ON prov.IsBillable = CASE WHEN reASC.Billable = 'Y' THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END 
	AND prov.IsEMR = CASE reASC.GoDirect WHEN 'Y' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END
	AND prov.UserId IS NULL
	AND prov.ServiceLocationId = pnServLoc.PracticeId
WHERE ap.Comments = 'ASC CLAIM' 
GROUP BY iv.EncounterId
	,ap.AppointmentId
	,ap.ResourceId2
	,pnServLoc.PracticeId
	,reASC.ResourceId
	,apEncounter.AppointmentId
	,prov.Id

GO

-- PatientQuestionSet
SELECT Id AS [Id],
	Code AS [Name]
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'QUESTIONSETS'

GO

-- BillingOrganizationPhoneNumber
----PhoneNumber BillingOrganization PracticeName #1
SELECT model.GetPairId(1, pn.PracticeId, @PhoneNumberMax) AS Id
	,pn.PracticeId AS BillingOrganizationId
	,CONVERT(NVARCHAR, NULL) AS CountryCode
	,CASE len(PracticePhone)
		WHEN 10
			THEN substring(PracticePhone, 1, 3)
			ELSE NULL
		END AS AreaCode
	,CASE len(PracticePhone)
		WHEN 10
			THEN substring(PracticePhone, 4, len(PracticePhone) - 3)
		ELSE PracticePhone
		END AS ExchangeAndSuffix
	,CONVERT(NVARCHAR, NULL) AS Extension
	,CONVERT(BIT, 0) AS IsInternational
	,1 AS OrdinalId
	,9 AS BillingOrganizationPhoneNumberTypeId
FROM dbo.PracticeName pn
WHERE PracticePhone <> ''
	AND PracticePhone IS NOT NULL
	AND PracticeType = 'P'

UNION ALL

----PhoneNumber BillingOrganization PracticeName #2
SELECT model.GetPairId(2, pn.PracticeId, @PhoneNumberMax) AS Id,
	pn.PracticeId AS BillingOrganizationId,
	CONVERT(NVARCHAR, NULL) AS CountryCode,
	CASE len(PracticeOtherPhone)
		WHEN 10
			THEN substring(PracticeOtherPhone, 1, 3)
			ELSE NULL
		END AS AreaCode
	,CASE len(PracticeOtherPhone)
		WHEN 10
			THEN substring(PracticeOtherPhone, 4, len(PracticeOtherPhone) - 3)
		ELSE PracticeOtherPhone
		END AS ExchangeAndSuffix,
	CONVERT(NVARCHAR, NULL) AS Extension,
	CONVERT(bit, 0) AS IsInternational,
	2 AS OrdinalId,
	1 AS BillingOrganizationPhoneNumberTypeId
FROM dbo.PracticeName pn
WHERE PracticeOtherPhone <> ''
	AND PracticeOtherPhone IS NOT NULL
	AND PracticeType = 'P'

UNION ALL

----PhoneNumber BillingOrganization PracticeName Fax
SELECT model.GetPairId(3, pn.PracticeId, @PhoneNumberMax) AS Id,
	pn.PracticeId AS BillingOrganizationId,
	CONVERT(NVARCHAR, NULL) AS CountryCode,
	CASE len(PracticeFax)
		WHEN 10
			THEN substring(PracticeFax, 1, 3)
			ELSE NULL
		END AS AreaCode
	,CASE len(PracticeFax)
		WHEN 10
			THEN substring(PracticeFax, 4, len(PracticeFax) - 3)
		ELSE PracticeFax
		END AS ExchangeAndSuffix,
	CONVERT(NVARCHAR, NULL) AS Extension,
	CONVERT(bit, 0) AS IsInternational,
	3 AS OrdinalId,
	6 AS BillingOrganizationPhoneNumberTypeId
FROM dbo.PracticeName pn
WHERE PracticeFax <> ''
	AND PracticeFax IS NOT NULL
	AND PracticeType = 'P'

UNION ALL

----PhoneNumber BillingOrganization Resources (override/orgoverride)
SELECT model.GetPairId(4, re.ResourceId, @PhoneNumberMax) AS Id
	,model.GetPairId(1, re.ResourceId, @BillingOrganizationMax) AS BillingOrganizationId
	,CONVERT(NVARCHAR, NULL) AS CountryCode
	,CASE len(re.ResourcePhone)
		WHEN 10
			THEN substring(re.ResourcePhone, 1, 3)
			ELSE NULL
		END AS AreaCode
	,CASE len(re.ResourcePhone)
		WHEN 10
			THEN substring(re.ResourcePhone, 4, len(re.ResourcePhone) - 3)
		ELSE re.ResourcePhone
		END AS ExchangeAndSuffix
	,CONVERT(NVARCHAR, NULL) AS Extension
	,CONVERT(bit, 0) AS IsInternational
	,1 AS OrdinalId
	,9 AS BillingOrganizationPhoneNumberTypeId
FROM dbo.Resources re
LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
	AND pa.ResourceType = 'R'
WHERE re.ResourcePhone <> ''
	AND re.ResourcePhone IS NOT NULL
	AND re.ResourceType IN ('D', 'Q', 'Z', 'Y')
	AND (
		pa.[Override] = 'Y'
		OR pa.OrgOverride = 'Y'
		)
GROUP BY re.ResourceId
	,re.ResourcePhone

GO

-- ServiceLocationPhoneNumber
----PhoneNumber ServiceLocation PracticeName Phone 1
SELECT model.GetPairId(5, pnServLoc.PracticeId, @PhoneNumberMax) AS Id,
	CONVERT(NVARCHAR, NULL) AS CountryCode,
	CASE len(PracticePhone)
		WHEN 10
			THEN substring(PracticePhone, 1, 3)
			ELSE NULL
		END AS AreaCode
	,CASE len(PracticePhone)
		WHEN 10
			THEN substring(PracticePhone, 4, len(PracticePhone) - 3)
		ELSE PracticePhone
		END AS ExchangeAndSuffix,
	CONVERT(NVARCHAR, NULL) AS Extension,
	CONVERT(bit, 0) AS IsInternational,
	1 AS OrdinalId,
	pnServLoc.PracticeId AS ServiceLocationId,
	pct.Id + 1000 AS ServiceLocationPhoneNumberTypeId
FROM dbo.PracticeName pnServLoc
LEFT OUTER JOIN dbo.PracticeCodeTable pct 
	ON pct.Code = 'Primary'
	AND pct.ReferenceType = 'PHONETYPESERVICELOCATION'
WHERE PracticePhone <> ''
	AND PracticePhone IS NOT NULL
	AND PracticeType = 'P'

UNION ALL

----PhoneNumber ServiceLocation PracticeName Phone 2
SELECT model.GetPairId(6, pnServLoc.PracticeId, @PhoneNumberMax) AS Id,
	CONVERT(NVARCHAR, NULL) AS CountryCode,
	CASE len(PracticeOtherPhone)
		WHEN 10
			THEN substring(PracticeOtherPhone, 1, 3)
			ELSE NULL
		END AS AreaCode
	,CASE len(PracticeOtherPhone)
		WHEN 10
			THEN substring(PracticeOtherPhone, 4, len(PracticeOtherPhone) - 3)
		ELSE PracticeOtherPhone
		END AS ExchangeAndSuffix,
		CONVERT(NVARCHAR, NULL) AS Extension,
	CONVERT(bit, 0) AS IsInternational,
	2 AS OrdinalId,
	pnServLoc.PracticeId AS ServiceLocationId,
	pct.Id + 1000 AS ServiceLocationPhoneNumberTypeId
FROM dbo.PracticeName pnServLoc
LEFT OUTER JOIN dbo.PracticeCodeTable pct 
	ON pct.Code = 'Other'
	AND pct.ReferenceType = 'PHONETYPESERVICELOCATION'
WHERE PracticeOtherPhone <> ''
	AND PracticeOtherPhone IS NOT NULL
	AND PracticeType = 'P'

UNION ALL

----PhoneNumber ServiceLocation PracticeName Fax
SELECT model.GetPairId(7, pnServLoc.PracticeId, @PhoneNumberMax) AS Id,
	CONVERT(NVARCHAR, NULL) AS CountryCode,
	CASE len(PracticeFax)
		WHEN 10
			THEN substring(PracticeFax, 1, 3)
			ELSE NULL
		END AS AreaCode
	,CASE len(PracticeFax)
		WHEN 10
			THEN substring(PracticeFax, 4, len(PracticeFax) - 3)
		ELSE PracticeFax
		END AS ExchangeAndSuffix,
		CONVERT(NVARCHAR, NULL) AS Extension,
	CONVERT(bit, 0) AS IsInternational,
	3 AS OrdinalId,
	pnServLoc.PracticeId AS ServiceLocationId,
	pct.Id + 1000 AS ServiceLocationPhoneNumberTypeId
FROM dbo.PracticeName pnServLoc
LEFT OUTER JOIN dbo.PracticeCodeTable pct 
	ON pct.Code = 'Fax'
	AND pct.ReferenceType = 'PHONETYPESERVICELOCATION'
WHERE PracticeFax <> ''
	AND PracticeFax IS NOT NULL
	AND PracticeType = 'P'

UNION ALL

----PhoneNumber ServiceLocation Resources (external facilities) (not override which is never a service location)
SELECT model.GetPairId(8, reServLoc.ResourceId, @PhoneNumberMax) AS Id,
	CONVERT(NVARCHAR, NULL) AS CountryCode,
	CASE len(reServLoc.ResourcePhone)
		WHEN 10
			THEN substring(reServLoc.ResourcePhone, 1, 3)
			ELSE NULL
		END AS AreaCode
	,CASE len(reServLoc.ResourcePhone)
		WHEN 10
			THEN substring(reServLoc.ResourcePhone, 4, len(reServLoc.ResourcePhone) - 3)
		ELSE reServLoc.ResourcePhone
		END AS ExchangeAndSuffix,
	CONVERT(NVARCHAR, NULL) AS Extension,
	CONVERT(bit, 0) AS IsInternational,
	4 AS OrdinalId,
	model.GetPairId(1, reServLoc.ResourceId, @ServiceLocationMax) AS ServiceLocationId,
	pct.Id + 1000 AS ServiceLocationPhoneNumberTypeId
FROM dbo.Resources reServLoc
LEFT OUTER JOIN dbo.PracticeCodeTable pct 
	ON pct.Code = 'Primary'
	AND pct.ReferenceType = 'PHONETYPESERVICELOCATION'
WHERE reServLoc.ResourcePhone <> ''
	AND reServLoc.ResourcePhone IS NOT NULL
	AND reServLoc.ResourceType = 'R'
	AND reServLoc.ServiceCode = '02'
GROUP BY reServLoc.ResourceId,
	reServLoc.ResourcePhone,
	pct.Id

GO

-- UserPhoneNumber
----PhoneNumber User 
SELECT ResourceId AS Id,
	CONVERT(NVARCHAR, NULL) AS CountryCode,
	CASE len(re.ResourcePhone)
		WHEN 10
			THEN substring(re.ResourcePhone, 1, 3)
			ELSE NULL
		END AS AreaCode
	,CASE len(re.ResourcePhone)
		WHEN 10
			THEN substring(re.ResourcePhone, 4, len(re.ResourcePhone) - 3)
		ELSE re.ResourcePhone
		END AS ExchangeAndSuffix,
	CONVERT(NVARCHAR, NULL) AS Extension,
	CONVERT(bit, 0) AS IsInternational,
	1 AS OrdinalId,
	re.ResourceId AS UserId,
	10 AS UserPhoneNumberTypeId
FROM dbo.Resources re
WHERE re.ResourceType <> 'R'
	AND ResourcePhone <> ''
	AND ResourcePhone IS NOT NULL

UNION ALL

----PhoneNumber User Fax
SELECT model.GetPairId(32, ResourceId, @PhoneNumberMax) AS Id,
	CONVERT(NVARCHAR, NULL) AS CountryCode,
	CASE len(re.ResourcePhone)
		WHEN 10
			THEN substring(re.ResourcePhone, 1, 3)
			ELSE NULL
		END AS AreaCode
	,CASE len(re.ResourcePhone)
		WHEN 10
			THEN substring(re.ResourcePhone, 4, len(re.ResourcePhone) - 3)
		ELSE re.ResourcePhone
		END AS ExchangeAndSuffix,
	CONVERT(NVARCHAR, NULL) AS Extension,
	CONVERT(bit, 0) AS IsInternational,
	2 AS OrdinalId,
	re.ResourceId AS UserId,
	11 AS UserPhoneNumberTypeId
FROM dbo.Resources re
WHERE re.ResourceType <> 'R'
	AND ResourcePhone <> ''
	AND ResourcePhone IS NOT NULL

GO


-- BillingOrganizationPhoneNumberType
SELECT CASE
		WHEN Code = 'Billing'
			THEN 1
		WHEN Code = 'Primary'
			THEN 9
		WHEN Code = 'Fax'
			THEN 6
		ELSE pctBillingOrganization.Id + 1000
		END AS Id,
	AlternateCode AS Abbreviation,
	CASE 
		WHEN Code = 'Primary'
			THEN 'Main'
		ELSE Code
	END AS Name,
	CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctBillingOrganization 
WHERE pctBillingOrganization.ReferenceType = 'PHONETYPEBILLINGORGANIZATION'

GO

-- ExternalContactPhoneNumberType
SELECT CASE 
	WHEN Code = 'Primary'
		THEN 5
	WHEN Code = 'Fax'
		THEN 4
	ELSE pctContact.Id + 1000
	END AS Id,
	AlternateCode AS Abbreviation,
	CASE 
		WHEN Code = 'Primary'
			THEN 'Main'
		ELSE Code
	END AS Name,
	CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctContact
WHERE pctContact.ReferenceType = 'PHONETYPECONTACT'

GO

-- ExternalOrganizationPhoneNumberType
SELECT pctExternalOrganization.Id + 1000 AS Id,
	AlternateCode AS Abbreviation,
	CASE 
		WHEN Code = 'Primary'
			THEN 'Main'
		ELSE Code
	END AS Name,
	CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctExternalOrganization
WHERE pctExternalOrganization.ReferenceType = 'PHONETYPEEXTERNALORGANIZATION'

GO


-- ServiceLocationPhoneNumberType
SELECT pctServiceLocation.Id + 1000 AS Id,
	AlternateCode AS Abbreviation,
	CASE 
		WHEN Code = 'Primary'
			THEN 'Main'
		ELSE Code
	END AS Name,
	CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctServiceLocation
WHERE pctServiceLocation.ReferenceType = 'PHONETYPESERVICELOCATION'

GO

-- UserPhoneNumberType
SELECT CASE
	WHEN Code = 'Primary'
		THEN 10
	WHEN Code = 'Fax'
		THEN 11
	ELSE pctUser.Id + 1000 
	END AS Id,
	AlternateCode AS Abbreviation,
	CASE 
		WHEN Code = 'Primary'
			THEN 'Main'
		ELSE Code
	END AS Name,
	CONVERT(bit, 0) AS IsArchived
FROM dbo.PracticeCodeTable pctUser
WHERE pctUser.ReferenceType = 'PHONETYPEUSER'

GO


---- Diagnosis
-----This is the practice's master diagnosis table (analogous to the Drug table in the PracticeRepository).  It will include ICD9 + IO custom diagnoses (from DiagnosisMaster's PrimaryDiagnosisTable), ICD10 codes when we get them and the practice's made-up "custom" diagnoses
-----To get Description we'll need the DiagnosisMaster.  Don't need them for now.
-----To get Unspecified we'll need the DiagnosisMaster.  Don't need them for now.
------PracticeDiagnosis From PatientClinical table
--SELECT FindingDetail AS Id,
--	SUBSTRING(FindingDetail, 1, CASE 
--			WHEN (dbo.GetMax((CHARINDEX('.', FindingDetail, 5)),0)) > 0
--				THEN (dbo.GetMax((CHARINDEX('.', FindingDetail, 5) - 1),0))
--			ELSE LEN(FindingDetail)
--			END) AS BillingCode,
--	NULL AS CustomDiagnosisId,
--	CONVERT(nvarchar, NULL) AS DisplayName,
--	CONVERT(datetime, NULL, 112) AS EndDateTime,
--	FindingDetail AS Icd9DiagnosisId,
--	CONVERT(bit, 1) AS IsBillable,
--	CONVERT(bit, 1) AS IsOphthalmic,
--	CONVERT(bit, 0) AS IsUnspecified,
--	CONVERT(nvarchar, NULL) AS LetterTranslationNonTechnical,
--	CONVERT(nvarchar, NULL) AS LetterTranslationTechnical,
--	CONVERT(datetime, '1/1/2000 12:00:00 AM', 112) AS StartDateTime
--FROM dbo.PatientClinical
--WHERE ClinicalType IN (
--		'B',
--		'K'
--		)
--GROUP BY FindingDetail

--UNION ALL

------PracticeDiagnosis that are Custom diagnoses from PracticeServices table, if not present in PatientClinical table
--SELECT Code AS Id,
--	Code AS BillingCode,
--	Code AS CustomDiagnosisId,
--	CONVERT(nvarchar, NULL) AS DisplayName,
--	CONVERT(datetime, NULL, 112) AS EndDateTime,
--	NULL AS Icd9DiagnosisId,
--	CONVERT(bit, 1) AS IsBillable,
--	CONVERT(bit, 1) AS IsOphthalmic,
--	CONVERT(bit, 0) AS IsUnspecified,
--	CONVERT(nvarchar, NULL) AS LetterTranslationNonTechnical,
--	CONVERT(nvarchar, NULL) AS LetterTranslationTechnical,
--	CONVERT(datetime, '1/1/2000 12:00:00 AM', 112) AS StartDateTime
--FROM dbo.PracticeServices
--WHERE CodeType = 'D'
--	AND Code <> '' 
--	AND Code IS NOT NULL
--	AND Code NOT IN (SELECT FindingDetail AS FindingDetail FROM dbo.PatientClinical WHERE ClinicalType IN ('B', 'K'))
--GO



----Provider(no index)
-----human and facility providers where ServiceLocation is from PracticeName table, including override 
--SELECT 
--	CASE 
--		WHEN re.ResourceType = 'R' ---internal facility
--			THEN model.GetBigPairId(pnBillOrg.PracticeId, model.GetBigPairId(pnBillOrg.PracticeId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
--		WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
--			THEN model.GetBigPairId(model.GetPairId(2, pnServLoc.PracticeId, @ServiceLocationMax), model.GetBigPairId(re.ResourceId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
--		ELSE model.GetBigPairId(pnServLoc.PracticeId, model.GetBigPairId(pnBillOrg.PracticeId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
--		END AS Id,
--	CASE
--		WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
--			THEN model.GetPairId(1, re.ResourceId, @BillingOrganizationMax) 
--		ELSE pnBillOrg.PracticeId
--		END AS BillingOrganizationId,
--	CASE re.Billable
--		WHEN 'Y'
--			THEN CONVERT(bit, 1)
--		ELSE CONVERT(bit, 0)
--		END AS IsBillable,
--	CASE re.GoDirect
--		WHEN 'Y'
--			THEN CONVERT(bit, 0)
--		ELSE CONVERT(bit, 1)
--		END AS IsEMR,
--	CASE 
--		WHEN re.ResourceType = 'R'
--			THEN pnBillOrg.PracticeId
--		ELSE pnServLoc.PracticeId 
--		END AS ServiceLocationId,
--	CASE
--		WHEN re.ResourceType = 'R'
--			THEN NULL
--		ELSE re.ResourceId 
--		END AS UserId
--FROM dbo.Resources re
--INNER JOIN dbo.PracticeName AS pnBillOrg ON re.PracticeId = pnBillOrg.PracticeId
--	AND pnBillOrg.PracticeType = 'P'
--INNER JOIN dbo.PracticeName AS pnServLoc ON pnServLoc.PracticeType = 'P'
--INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
--LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
--		AND pa.ResourceType = 'R'
--WHERE (re.ResourceType in ('D', 'Q', 'Z','Y')
--	OR (re.ResourceType = 'R'
--		AND re.ServiceCode = '02'
--		AND re.Billable = 'Y'
--		AND pic.FieldValue = 'T'
--		AND pnBillOrg.LocationReference <> '')
--		)
--GROUP BY
--	CASE 
--		WHEN re.ResourceType = 'R' ---internal facility
--			THEN model.GetBigPairId(pnBillOrg.PracticeId, model.GetBigPairId(pnBillOrg.PracticeId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
--		WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
--			THEN model.GetBigPairId(model.GetPairId(2, pnServLoc.PracticeId, @ServiceLocationMax), model.GetBigPairId(re.ResourceId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
--		ELSE model.GetBigPairId(pnServLoc.PracticeId, model.GetBigPairId(pnBillOrg.PracticeId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
--		END, 
--	CASE
--		WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
--			THEN model.GetPairId(1, re.ResourceId, @BillingOrganizationMax) 
--		ELSE pnBillOrg.PracticeId
--		END,
--	CASE re.Billable
--		WHEN 'Y'
--			THEN CONVERT(bit, 1)
--		ELSE CONVERT(bit, 0)
--		END ,
--	CASE re.GoDirect
--		WHEN 'Y'
--			THEN CONVERT(bit, 0)
--		ELSE CONVERT(bit, 1)
--		END ,
--	CASE 
--		WHEN re.ResourceType = 'R'
--			THEN pnBillOrg.PracticeId
--		ELSE pnServLoc.PracticeId 
--		END ,
--	CASE
--		WHEN re.ResourceType = 'R'
--			THEN NULL
--		ELSE re.ResourceId 
--		END

--UNION ALL

------Human Providers where ServiceLocation is from Resources table including override
--SELECT
--	CASE 
--		WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
--			THEN model.GetBigPairId(model.GetPairId(3, reServLoc.ResourceId, @ServiceLocationMax), model.GetBigPairId(re.ResourceId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
--		ELSE model.GetBigPairId(model.GetPairId(1, reServLoc.ResourceId, @ServiceLocationMax), model.GetBigPairId(pnBillOrg.PracticeId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
--		END AS Id,
--	CASE 
--		WHEN  pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
--			THEN model.GetPairId(1, re.ResourceId, @BillingOrganizationMax)
--		ELSE pnBillOrg.PracticeId 
--		END AS BillingOrganizationId,
--	CASE re.Billable
--		WHEN 'Y'
--			THEN CONVERT(bit, 0)
--		ELSE CONVERT(bit, 1)
--		END AS IsBillable,
--	CASE re.GoDirect
--		WHEN 'Y'
--			THEN CONVERT(bit, 1)
--		ELSE CONVERT(bit, 0)
--		END AS IsEMR,
--	model.GetPairId(1, reServLoc.ResourceId, @ServiceLocationMax) AS ServiceLocationId,
--	re.ResourceId AS UserId
--FROM dbo.Resources re
--INNER JOIN dbo.PracticeName AS pnBillOrg ON pnBillOrg.PracticeId = re.PracticeId
--	AND pnBillOrg.PracticeType = 'P'
--INNER JOIN dbo.Resources reServLoc ON reServLoc.ResourceType = 'R'
--	AND reServLoc.ServiceCode = '02'
--LEFT JOIN dbo.PracticeAffiliations pa ON pa.ResourceId = re.ResourceId
--		AND pa.ResourceType = 'R'
--WHERE re.ResourceType in ('D', 'Q', 'Z','Y')
--GROUP BY
--		CASE 
--		WHEN pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
--			THEN model.GetBigPairId(model.GetPairId(3, reServLoc.ResourceId, @ServiceLocationMax), model.GetBigPairId(re.ResourceId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
--		ELSE model.GetBigPairId(model.GetPairId(1, reServLoc.ResourceId, @ServiceLocationMax), model.GetBigPairId(pnBillOrg.PracticeId, re.ResourceId, @ProviderBillingOrganizationMax), @ProviderBillingOrganizationServiceLocationMax)
--		END,
--	CASE 
--		WHEN  pa.[Override] = 'Y' OR pa.OrgOverride = 'Y'---override
--			THEN model.GetPairId(1, re.ResourceId, @BillingOrganizationMax)
--		ELSE pnBillOrg.PracticeId 
--		END,
--	CASE re.GoDirect
--		WHEN 'Y'
--			THEN CONVERT(bit, 0)
--		ELSE CONVERT(bit, 1)
--		END ,
--	model.GetPairId(1, reServLoc.ResourceId, @ServiceLocationMax),
--	re.ResourceId,
--	re.Billable,
--	pa.ResourceId,
--	reServLoc.ResourceId,
--	pnBillOrg.PracticeId

--GO

-- Room
SELECT re.ResourceId AS Id
	,ResourceName AS Abbreviation
	,CONVERT(bit, 0) AS IsSchedulable
	,ResourceDescription AS NAME
	,pn.PracticeId AS ServiceLocationId,
	CONVERT(bit, 0) AS IsArchived
FROM dbo.Resources re
INNER JOIN dbo.PracticeName pn ON pn.PracticeId = re.PracticeId
WHERE re.ResourceType = 'R'
	AND re.ServiceCode <> '02'

GO

-- ScheduleEntry (no index)
SELECT Id AS Id
	,EndDateTime AS EndDateTime
	,NULL AS EquipmentId
	,NULL as RoomId
	,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
	,StartDateTime AS StartDateTime
	,ServiceLocationId AS ServiceLocationId
	,UserId AS UserId
	,'UserScheduleEntry' AS __EntityType__ 
FROM model.GetScheduleEntries(0)

UNION ALL

SELECT Id AS Id
	,EndDateTime AS EndDateTime
	,NULL AS EquipmentId
	,NULL as RoomId
	,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
	,StartDateTime AS StartDateTime
	,ServiceLocationId AS ServiceLocationId
	,UserId AS UserId
	,'UserScheduleEntry' AS __EntityType__ 
FROM model.GetScheduleEntries(1)

UNION ALL

SELECT Id AS Id
	,EndDateTime AS EndDateTime
	,NULL AS EquipmentId
	,NULL as RoomId
	,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
	,StartDateTime AS StartDateTime
	,ServiceLocationId AS ServiceLocationId
	,UserId AS UserId
	,'UserScheduleEntry' AS __EntityType__ 
FROM model.GetScheduleEntries(2)

UNION ALL

SELECT Id AS Id
	,EndDateTime AS EndDateTime
	,NULL AS EquipmentId
	,NULL as RoomId
	,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
	,StartDateTime AS StartDateTime
	,ServiceLocationId AS ServiceLocationId
	,UserId AS UserId
	,'UserScheduleEntry' AS __EntityType__ 
FROM model.GetScheduleEntries(3)

UNION ALL

SELECT Id AS Id
	,EndDateTime AS EndDateTime
	,NULL AS EquipmentId
	,NULL as RoomId
	,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
	,StartDateTime AS StartDateTime
	,ServiceLocationId AS ServiceLocationId
	,UserId AS UserId
	,'UserScheduleEntry' AS __EntityType__ 
FROM model.GetScheduleEntries(4)

UNION ALL

SELECT Id AS Id
	,EndDateTime AS EndDateTime
	,NULL AS EquipmentId
	,NULL as RoomId
	,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
	,StartDateTime AS StartDateTime
	,ServiceLocationId AS ServiceLocationId
	,UserId AS UserId
	,'UserScheduleEntry' AS __EntityType__ 
FROM model.GetScheduleEntries(5)

UNION ALL

SELECT Id AS Id
	,EndDateTime AS EndDateTime
	,NULL AS EquipmentId
	,NULL as RoomId
	,ScheduleEntryAvailabilityTypeId AS ScheduleEntryAvailabilityTypeId
	,StartDateTime AS StartDateTime
	,ServiceLocationId AS ServiceLocationId
	,UserId AS UserId
	,'UserScheduleEntry' AS __EntityType__ 
FROM model.GetScheduleEntries(6)

UNION ALL

SELECT model.GetPairId(7, NoteId, @CalendarMax) AS Id
	,CASE 
		WHEN COALESCE(NoteDate, '') <> ''
			THEN CONVERT(datetime, NoteDate, 112) 
		ELSE NULL END AS EndDateTime
	,NULL AS EquipmentId
	,NULL as RoomId
	,1 AS ScheduleEntryAvailabilityTypeId
	,CASE 
		WHEN COALESCE(NoteDate, '') <> ''
			THEN CONVERT(datetime, NoteDate, 112) 
		ELSE NULL END AS StartDateTime
	,NULL AS ServiceLocationId
	,NULL AS UserId
	,'ScheduleEntry' AS __EntityType__ 
FROM dbo.PatientNotes
WHERE PatientId = -1 and AppointmentId = -1 and NoteType = '/'

GO

-- ServiceLocation
----First number of GetPairId for ServiceLocationId indicates where it comes from (0 - PracticeName, 1 - Resources)
----If a surgery center is a provider, I'm using the PracticeName row for ServiceLocation because encounters are scheduled in the PracticeName ServiceLocation.
SELECT v.PracticeId AS Id
	,CONVERT(BIT, 0) AS IsExternal
	,v.NAME AS [Name]
	,v.ServiceLocationCodeId AS ServiceLocationCodeId
	,v.ShortName AS ShortName
	,CONVERT(bit, 0) AS IsArchived
	,HexColor AS HexColor
FROM (
	SELECT pn.PracticeId AS PracticeId
		,pic.FieldValue AS FieldValue
		,re.Billable AS Billable
		,re.ServiceCode AS ServiceCode
		,CASE 
			WHEN pic.FieldValue = 'T'
				AND re.Billable = 'Y'
				AND re.ServiceCode = '02'
				AND pn.LocationReference <> ''
				THEN slcOthers.Id
			ELSE slcOffice.Id
			END AS ServiceLocationCodeId
		,CASE LocationReference
			WHEN ''
				THEN 'Office'
			ELSE 'Office-' + LocationReference
			END AS ShortName
		,PracticeName AS NAME
		,pn.HexColor AS HexColor
		,COUNT_BIG(*) AS Count
	FROM dbo.PracticeName pn
	INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
	LEFT JOIN model.ServiceLocationCodes slcOffice ON slcOffice.PlaceOfServiceCode = '11'
	LEFT JOIN dbo.Resources re ON re.PracticeId = pn.PracticeId
		AND re.ResourceType = 'R'
		AND re.ServiceCode = '02'
		AND re.Billable = 'Y'
		AND pic.FieldValue = 'T'
		AND pn.LocationReference <> ''
	LEFT JOIN model.ServiceLocationCodes slcOthers ON slcOthers.PlaceOfServiceCode = re.PlaceOfService
	WHERE pn.PracticeType = 'P'
	GROUP BY pn.PracticeId
		,LocationReference
		,pic.FieldValue
		,re.Billable
		,re.ServiceCode
		,slcOffice.Id
		,slcOthers.Id
		,PracticeName
		,pn.HexColor
	) AS v

UNION ALL

----Getting ServiceLocations from Resources table (ResourceType R).  THESE ARE ALL FACILITIES.
SELECT model.GetPairId(1, re.ResourceId, @ServiceLocationMax) AS Id
	,CONVERT(BIT, 1) AS IsExternal
	,re.ResourceDescription AS [Name]
	,CASE 
		WHEN slc.id IS NULL
			OR slc.Id = ''
			THEN (
					SELECT Id AS Id
					FROM model.ServiceLocationCodes slc
					WHERE slc.PlaceOfServiceCode = '11'
					)
		ELSE slc.id
		END AS ServiceLocationCodeId
	,re.ResourceName AS ShortName
	,CONVERT(bit, 0) AS IsArchived
	,HexColor AS HexColor
FROM dbo.Resources re
LEFT JOIN model.ServiceLocationCodes slc ON slc.PlaceOfServiceCode = re.PlaceOfService
WHERE re.ResourceType = 'R'
	AND re.ServiceCode = '02'

GO

-- User (no index)
SELECT re.ResourceId AS Id,
	re.ResourceDescription AS DisplayName,
	re.ResourceFirstName AS FirstName,
	re.ResourceSuffix AS Honorific,
	CASE 
		WHEN re.ResourceType IN ('Z', 'X', 'Y') OR re.[Status] IN ('Z')
			THEN CONVERT(bit, 1)
		ELSE CONVERT(bit, 0)
		END AS IsArchived,
	re.IsLoggedIn AS IsLoggedIn,
	CASE 
		WHEN ResourceType NOT IN ('Q', 'D') 
			THEN CONVERT(bit, 0) 
		WHEN re.ResourceType IN ('D', 'Q')
			THEN CONVERT(bit, 1)
		END AS IsSchedulable,
	re.ResourceLastName AS LastName,
	re.ResourceMI AS MiddleName,
	re.ResourceColor as OleColor,
	ResourcePid AS PID,
	re.ResourcePrefix AS Prefix,
	CONVERT(nvarchar, NULL) AS Suffix,
	re.ResourceName AS UserName
	,re.OrdinalId
	,CASE 
	 WHEN ResourceType NOT IN ('D', 'Q', 'Z', 'Y')
		THEN 'User' 
	WHEN re.ResourceType IN ('D', 'Q', 'Z', 'Y')
		THEN 'Doctor' 
	END AS __EntityType__
FROM dbo.Resources re
WHERE ResourceType <> 'R' AND ResourceId > 0

GO

-- PatientRecall
SELECT ptj.TransactionId AS Id
	 ,ptj.TransactionTypeId AS PatientId
	 ,CASE apt.AppTypeId 
			WHEN 0
		THEN -1
	ELSE apt.AppTypeId END AS AppointmentTypeId
	 ,MIN(re.ResourceId) AS UserId
	 ,NULL AS ServiceLocationId
	 ,CASE ptj.TransactionStatus
		WHEN 'P' THEN 1
		WHEN 'S' THEN 2
		WHEN 'Z' THEN 3
		END AS RecallStatusId
	,CASE
		WHEN ptj.TransactionRemark = ''  OR ptj.TransactionRemark IS NULL
			THEN CONVERT(datetime, TransactionDate, 112)
		ELSE CASE	
				WHEN LEN(ptj.TransactionRemark) = 8
					THEN CONVERT(datetime, TransactionRemark, 112)
				ELSE CONVERT(datetime, SUBSTRING(TransactionRemark, 2, 8), 112)
				END
		END AS DueDateTime
	,CASE 
		WHEN ptj.TransactionRemark = ''  OR ptj.TransactionRemark IS NULL 
			THEN NULL
		ELSE CONVERT(datetime, TransactionDate, 112)
		END AS SentOrClosedDateTime
	,CASE
		WHEN ptj.TransactionStatus = 'Z'
			THEN NULL
		WHEN SUBSTRING(ptj.TransactionRemark, 1, 1) = '[' 
			THEN 1
		ELSE 2
		END AS MethodSentId
	,NULL AS TemplateDocumentId
FROM dbo.PracticeTransactionJournal ptj
LEFT JOIN dbo.AppointmentType apt ON apt.AppointmentType = SUBSTRING(ptj.TransactionRef, 1, CHARINDEX('/', TransactionRef)-1)
LEFT JOIN dbo.Resources re ON re.ResourceName = SUBSTRING(ptj.TransactionRef, CHARINDEX('/', ptj.TransactionRef)+1, LEN(ptj.TransactionRef))
INNER JOIN model.Patients p ON p.Id = ptj.TransactionTypeId
WHERE ptj.TransactionType = 'L'
	AND CHARINDEX('/',ptj.TransactionRef) > 0
GROUP BY ptj.TransactionId
	,ptj.TransactionTypeId
	,apt.AppTypeId
	,ptj.TransactionStatus
	,ptj.TransactionRemark
	,ptj.TransactionDate

GO

-- ConfirmationStatus
SELECT
pct.Id AS Id,
CASE WHEN (LEN(pct.Code) - 4) > 0 THEN SUBSTRING(pct.Code, 1, 1) ELSE NULL END AS Code,
CASE WHEN (LEN(pct.Code) - 4) > 0 THEN SUBSTRING(pct.Code, 5, LEN(pct.Code) - 4) ELSE NULL END AS [Description]
FROM dbo.PracticeCodeTable pct
WHERE pct.ReferenceType = 'CONFIRMSTATUS'

GO

-- PatientFinancialComment
SELECT NoteId AS Id
	,PatientId AS PatientId
	,COALESCE(Note1,'') + ' ' + COALESCE(Note2,'') + ' ' + COALESCE(Note3,'') + ' ' + COALESCE(Note4,'') AS Value
	,CASE NoteCommentOn 
		WHEN 'T' 
		THEN CONVERT(BIT,1) 
		ELSE CONVERT(BIT,0) 
		END AS IsIncludedOnStatement
FROM dbo.PatientNotes
WHERE NoteType = 'B'
	AND PatientId <> 0
	AND AppointmentId = 0
GO