﻿DECLARE @AddressMax int
SET @AddressMax = 110000000
DECLARE @BillingOrganizationMax int
SET @BillingOrganizationMax = 110000000
DECLARE @BillingServiceBillingDiagnosisMax int
SET @BillingServiceBillingDiagnosisMax = 110000000
DECLARE @BillingServiceModifierMax int
SET @BillingServiceModifierMax = 110000000
DECLARE @CalendarMax int
SET @CalendarMax = 110000000
DECLARE @ClaimAdjustmentReasonAdjustmentTypeMax int
SET @ClaimAdjustmentReasonAdjustmentTypeMax = 110000000
DECLARE @ClaimCrossOverMax int
SET @ClaimCrossOverMax = 110000000
DECLARE @ClinicalInvoiceProviderMax int
SET @ClinicalInvoiceProviderMax = 110000000
DECLARE @ClinicalServiceClinicalDiagnosisMax int
SET @ClinicalServiceClinicalDiagnosisMax = 110000000
DECLARE @ClinicalServiceModifierMax int
SET @ClinicalServiceModifierMax = 110000000
DECLARE @CommentMax int
SET @CommentMax = 110000000
DECLARE @CommentAlertMax int
SET @CommentAlertMax = 110000000
DECLARE @DoctorInsurerAssignmentMax int
SET @DoctorInsurerAssignmentMax = 110000000
DECLARE @EmailAddressMax int
SET @EmailAddressMax = 110000000
DECLARE @EncounterServiceMax int
SET @EncounterServiceMax = 110000000
DECLARE @ExternalProviderClinicalSpecialtyTypeMax int
SET @ExternalProviderClinicalSpecialtyTypeMax = 110000000
DECLARE @FeeScheduleAmountMax int
SET @FeeScheduleAmountMax = 110000000
DECLARE @InvoiceReceivableMax int
SET @InvoiceReceivableMax = 110000000
DECLARE @PatientMax int
SET @PatientMax = 202000000
DECLARE @PatientExternalProviderMax int
SET @PatientExternalProviderMax = 110000000
DECLARE @PatientInsuranceMax int
SET @PatientInsuranceMax = 110000000
DECLARE @PhoneNumberMax int
SET @PhoneNumberMax = 55000000
DECLARE @ProviderMax int
SET @ProviderMax = 110000000
DECLARE @ProviderBillingOrganizationMax int
SET @ProviderBillingOrganizationMax = 110000000
DECLARE @ProviderBillingOrganizationServiceLocationMax int
SET @ProviderBillingOrganizationServiceLocationMax = 110000000
DECLARE @ReceivableBillablePartyMax int
SET @ReceivableBillablePartyMax = 110000000
DECLARE @ServiceLocationMax int
SET @ServiceLocationMax = 110000000
DECLARE @ServiceModifierMax int
SET @ServiceModifierMax = 110000000
DECLARE @EncounterTransitionOfCareOrder int
SET @EncounterTransitionOfCareOrder = 110000000
DECLARE @IdentifierCodeMax INT
SET @IdentifierCodeMax = 110000000