﻿-- EmailAddress Insert
IF @BillingOrganizationId IS NOT NULL
	SET @Id = (
			SELECT model.GetPairId(CASE x
						WHEN 0
							THEN 1
						WHEN 1
							THEN 2
						END, y, @EmailAddressMax)
			FROM model.GetIdPair(@BillingOrganizationId, @BillingOrganizationMax)
			)
     
IF @ServiceLocationId IS NOT NULL
	SET @Id = (
			SELECT model.GetPairId(CASE x
						WHEN 0
							THEN 3
						WHEN 1
							THEN 4
						END, y, @EmailAddressMax)
			FROM model.GetIdPair(@ServiceLocationId, @ServiceLocationMax)
			)

IF @UserId IS NOT NULL
	SET @Id = model.GetPairId(0, @UserId, @EmailAddressMax)
GO

-- EmailAddress Update/Insert
DECLARE @pair TABLE (
	x int,
	y int
	)
DECLARE @x int,
	@y int

SELECT @x = x,
	@y = y
FROM model.GetIdPair(@Id, @EmailAddressMax)

IF @x IN (
		0,
		2,
		4
		)
	UPDATE Resources
	SET ResourceEmail = @Value
	WHERE ResourceId = @y

IF @x IN (
		1,
		3
		)
	UPDATE PracticeName
	SET PracticeEmail = @Value
	WHERE PracticeId = @y

GO

-- User Insert
CREATE TABLE #UserIdentity(Id int)

INSERT INTO Resources (ResourceLastName)
OUTPUT inserted.ResourceId INTO #UserIdentity(Id)
VALUES ('')

SET @Id = (SELECT TOP 1 Id FROM #UserIdentity)

DROP TABLE #UserIdentity
GO

-- User Update/Insert
UPDATE Resources
SET ResourceDescription = @DisplayName,
	ResourceFirstName = @FirstName,
	ResourceSuffix = @Honorific,
	[Status] = CASE @IsArchived
		WHEN 1
			THEN 'Z'
		WHEN 0
			THEN 'A'
		END,
	IsLoggedIn = @IsLoggedIn,
	ResourceLastName = @LastName,
	ResourceMI = @MiddleName,
	ResourceColor = @OleColor,
	ResourcePid = @PID,
	ResourcePrefix = @Prefix,
	ResourceName = @UserName,
	ResourceType = CASE @__EntityType__
		WHEN 'User'
			THEN COALESCE(ResourceType, 'A')
		WHEN 'Doctor'
			THEN COALESCE(ResourceType, 'D')
		END,
	OrdinalId = @OrdinalId
WHERE ResourceId = @Id
GO
-- Encounter Insert
CREATE TABLE #EncounterIdentity(Id int)

INSERT INTO dbo.Appointments (PatientId)
OUTPUT INSERTED.AppointmentId INTO #EncounterIdentity(Id)
VALUES(@PatientId)

SET @Id = (SELECT TOP 1 Id FROM #EncounterIdentity)
DROP TABLE #EncounterIdentity

GO

-- Encounter Update/Insert
DECLARE @patientDemographicsId INT

SET @patientDemographicsId = (
		SELECT y
		FROM model.GetIdPair(@PatientId, @PatientMax)
		)

DECLARE @appointmentId INT

SET @appointmentId = @Id

DECLARE @resourceId2 INT
DECLARE @x INT
DECLARE @practiceCode NVARCHAR(255)
DECLARE @shortName NVARCHAR(255)

SELECT @resourceId2 = (
		SELECT y
		FROM model.GetIdPair(@ServiceLocationId, @ServiceLocationMax)
		)

SELECT @x = (
		SELECT x
		FROM model.GetIdPair(@ServiceLocationId, @ServiceLocationMax)
		)

SELECT @shortName = (
		SELECT ShortName
		FROM model.ServiceLocations
		WHERE Id = @ServiceLocationId
		)

DECLARE @status NVARCHAR(1)
DECLARE @count INT
DECLARE @ActivityStatusTime NVARCHAR(100)
DECLARE @AppDate NVARCHAR(20)
DECLARE @ActivityLocationId INT
DECLARE @ActivityStatusTRef INT
DECLARE @ReasonForVisit NVARCHAR(128)

SELECT @AppDate = AppDate
FROM dbo.Appointments
WHERE AppointmentId = @appointmentId

SELECT @ActivityStatusTRef = (
		SELECT DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, model.GetClientDateNow()), 0), model.GetClientDateNow())
		)

SELECT @ActivityStatusTime = (substring(convert(VARCHAR(20), model.GetClientDateNow(), 9), 13, 5) + substring(convert(VARCHAR(30), model.GetClientDateNow(), 9), 25, 2))

SELECT @ActivityStatusTime = (LTRIM(RTRIM(@ActivityStatusTime)))

IF LEN(@ActivityStatusTime) <> 7
BEGIN
	SELECT @ActivityStatusTime = ('0' + @ActivityStatusTime)
END

SELECT @count = (
		SELECT Count(AppointmentId)
		FROM dbo.PracticeActivity
		WHERE AppointmentId = @appointmentId
		)

SELECT TOP 1 @ReasonForVisit = CASE WHEN pct.Code <> '' AND pct.Code <> 'NULL' AND pct.Code IS NOT NULL THEN pct.Code END
FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId
LEFT JOIN dbo.AppointmentType at ON at.AppointmentType = [IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=SCHEDULE APPOINTMENT\-2\/)(.*?)(?=\-)')
LEFT JOIN dbo.PracticeCodeTable pct ON pct.Code = [IO.Practiceware.SqlClr].[SearchRegexPattern](pc.FindingDetail,'(?<=<\^)(.*?)(?=\-5|\()')
WHERE pc.FindingDetail LIKE 'SCHEDULE APPOINTMENT%'
	AND pc.PatientId = @PatientId
	AND ScheduleStatus = 'A'
	AND pc.[Status] = 'A'

SET @status = CASE 
		WHEN @EncounterStatusId = 2
			THEN 'W'
		WHEN @EncounterStatusId = 3
			THEN 'M'
		WHEN @EncounterStatusId = 4
			THEN 'E'
		WHEN @EncounterStatusId = 5
			THEN 'G'
		WHEN @EncounterStatusId = 6
			THEN 'H'
		WHEN @EncounterStatusId = 7
			THEN 'D'
		WHEN @EncounterStatusId = 8
			THEN 'O'
		WHEN @EncounterStatusId = 9
			THEN 'Y'
		WHEN @EncounterStatusId = 10
			THEN 'S'
		WHEN @EncounterStatusId = 11
			THEN 'N'
		WHEN @EncounterStatusId = 12
			THEN 'F'
		WHEN @EncounterStatusId = 13
			THEN 'Q'
		WHEN @EncounterStatusId = 14
			THEN 'X'
		WHEN @EncounterStatusId = 15
			THEN 'U'
		END

UPDATE dbo.Appointments
SET ScheduleStatus = CASE 
		WHEN @EncounterStatusId = 1
			THEN 'P'
		WHEN @EncounterStatusId = 14
			THEN 'X'
		WHEN @EncounterStatusId = 13
			THEN 'Q'
		WHEN @EncounterStatusId = 12
			THEN 'F'
		WHEN @EncounterStatusId = 11
			THEN 'N'
		WHEN @EncounterStatusId = 10
			THEN 'S'
		WHEN @EncounterStatusId = 9
			THEN 'Y'
		WHEN @EncounterStatusId = 8
			THEN 'O'
		WHEN @EncounterStatusId = 7
			THEN 'D'
		WHEN @status IN (
				'W'
				,'M'
				,'E'
				,'G'
				,'H'
				,'U'
				)
			THEN 'A'
		END
	,PatientId = @patientDemographicsId
	,ResourceId2 = CASE 
		WHEN @x = 0
			AND @ShortName = 'Office'
			THEN 0
		WHEN @x = 0
			AND @ShortName <> 'Office'
			THEN @resourceId2 + 1000
		WHEN @x = 1
			THEN @resourceId2
		END
	,ApptInsType = CASE CAST(@InsuranceTypeId AS NVARCHAR)
		WHEN '1'
			THEN 'M'
		WHEN '2'
			THEN 'V'
		WHEN '3'
			THEN 'A'
		WHEN '4'
			THEN 'W'
		ELSE '1'
		END
	,VisitReason = @ReasonForVisit
	,EncounterStatusChangeReasonId = @EncounterStatusChangeReasonId
	,EncounterStatusChangeComment = @EncounterStatusChangeComment
	,Appdate = CONVERT(NVARCHAR(8),@StartDateTime,112)
	,AppTime = (DATEPART(HOUR, @StartDateTime) * 60) + (DATEPART(MINUTE, @StartDateTime))
WHERE AppointmentId = @appointmentId

SELECT @ActivityLocationId = (
		SELECT TOP 1 ResourceId2
		FROM dbo.Appointments
		WHERE AppointmentId = @appointmentId
		)

IF @EncounterStatusId > 1
BEGIN
	IF (
			@count = 0
			AND (
				@EncounterStatusId < 8
				OR @EncounterStatusId > 14
				)
			)
		INSERT INTO dbo.PracticeActivity (
			AppointmentId
			,STATUS
			,Questionset
			,ActivityDate
			,PatientId
			,ActivityStatusTime
			,TechConfirmed
			,LocationId
			,ActivityStatusTRef
			)
		VALUES (
			@appointmentId
			,@status
			,@practiceCode
			,@AppDate
			,@patientDemographicsId
			,@ActivityStatusTime
			,'N'
			,@ActivityLocationId
			,@ActivityStatusTRef
			)
	ELSE
		IF @count > 0
			UPDATE dbo.PracticeActivity
			SET STATUS = @status
				,ActivityDate = @AppDate
				,PatientId = @patientDemographicsId
				,ActivityStatusTime = @ActivityStatusTime
				,TechConfirmed = 'N'
				,LocationId = @ActivityLocationId
				,ActivityStatusTRef = @ActivityStatusTRef
			WHERE AppointmentId = @appointmentId;
END


---- Set Patient.IsClinical to True
--UPDATE dbo.PatientDemographics
--SET Status = 'A'
--WHERE PatientId = @patientDemographicsId
UPDATE model.Patients
SET IsClinical = CONVERT(bit, 1)
WHERE Id = @patientDemographicsId

GO


-- Encounter Delete
DECLARE @appointmentId INT

SET @appointmentId = @Id

DELETE
FROM dbo.Appointments
WHERE AppointmentId = @appointmentId
GO

-- EncounterPatientInsuranceReferral Update/Insert
UPDATE dbo.Appointments
SET ReferralId = @PatientInsuranceReferrals_Id
WHERE AppointmentId = @Encounters_Id
GO

-- EncounterPatientInsuranceReferral Delete
UPDATE dbo.Appointments
SET ReferralId = 0
WHERE AppointmentId = @Encounters_Id
GO

-- Appointment Insert

-- Encounter will always be created already, so we don't want to insert a new dbo.Appointments row
SET @Id = @EncounterId
GO

-- Appointment Update/Insert

-- @Id is a composite key...so we need to decompose it
IF @__EntityType__ = 'EquipmentAppointment'
BEGIN
	UPDATE dbo.Appointments
	SET AppTypeId = @EquipmentId
	WHERE AppointmentId = @Id
END
ELSE
BEGIN
DECLARE @duration int
SET @duration = (SELECT TOP 1 DATEPART(n, SpanSize) FROM model.ScheduleTemplateBuilderConfigurations)

DECLARE @OldClinicalInvoiceProviderId BIGINT
DECLARE @OldUserId INT

SELECT TOP 1 @OldUserId=ResourceId1 FROM dbo.Appointments WHERE AppointmentId=@Id
SET @OldClinicalInvoiceProviderId=CONVERT(bigint, 110000000) * @OldUserId + @Id

	UPDATE dbo.Appointments
	SET AppTypeId = CASE @AppointmentTypeId 
			WHEN -1
				THEN 0
			ELSE @AppointmentTypeId END,
		AppDate = CASE 
			WHEN @DateTime IS NULL
				THEN CONVERT(VARCHAR, model.GetClientDateNow(), 112)
			ELSE CONVERT(VARCHAR, @DateTime, 112)
			END,
		AppTime = DATEPART(hh, @DateTime)* 60 + DATEPART(mi, @DateTime),
		Duration = @duration,
		ResourceId1 = @UserId,
		Comments = CASE
			WHEN @Comment IS NULL
				THEN ''
			ELSE @Comment END
		WHERE AppointmentId = @Id;

	UPDATE dbo.PracticeActivity	
	SET	ActivityDate = (SELECT TOP 1 AppDate FROM dbo.Appointments WHERE AppointmentId=@Id)
	WHERE AppointmentId = @Id;

	DECLARE @ClinicalInvoiceProviderId BIGINT
	SET @ClinicalInvoiceProviderId=CONVERT(bigint, 110000000) * @UserId + @Id
	
	UPDATE model.Invoices 
	SET ClinicalInvoiceProviderId=@ClinicalInvoiceProviderId 
	WHERE EncounterId=@Id AND ClinicalInvoiceProviderId=@OldClinicalInvoiceProviderId;
END

IF @OrderDate IS NOT NULL
BEGIN
	DECLARE @stringOrderDate AS nvarchar(10)
	SET @stringOrderDate = CONVERT(VARCHAR, @OrderDate, 112)

	DECLARE @patientId AS int
	SET @patientId = (SELECT PatientId FROM dbo.Appointments WHERE AppointmentId = @Id)
	DECLARE @serviceLocationId AS int
	SET @serviceLocationId = (SELECT TOP 1 ResourceId2 FROM dbo.Appointments WHERE AppointmentId = @Id)

	DECLARE @orderAppointmentClinicalId AS int
	SET @orderAppointmentClinicalId = (SELECT MIN(pc.ClinicalId) AS SurgeryOrderId
										FROM dbo.Appointments ap
										INNER JOIN dbo.PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
											AND pc.ClinicalType = 'A'
											AND pc.Status = 'A'
											AND substring(pc.FindingDetail, 1, 16) = 'SCHEDULE SURGERY'
										WHERE ap.PatientId = @patientId
											AND ap.AppDate = @stringOrderDate
										GROUP BY ap.PatientId, ap.AppDate)
	IF @orderAppointmentClinicalId IS NULL OR @orderAppointmentClinicalId = 0
	BEGIN
		INSERT INTO [dbo].[Appointments]
           ([PatientId]
           ,[AppTypeId]
           ,[AppDate]
           ,[AppTime]
           ,[Duration]
           ,[ReferralId]
           ,[PreCertId]
           ,[TechApptTypeId]
           ,[ScheduleStatus]
           ,[ActivityStatus]
           ,[Comments]
           ,[ResourceId1]
           ,[ResourceId2]
           ,[ResourceId3]
           ,[ResourceId4]
           ,[ResourceId5]
           ,[ResourceId6]
           ,[ResourceId7]
           ,[ResourceId8]
           ,[ApptInsType]
           ,[ConfirmStatus]
           ,[ApptTypeCat]
           ,[SetDate]
           ,[VisitReason])
		VALUES
           (@patientId
           ,0
           ,@stringOrderDate
           ,-1
           ,0
           ,0
           ,0
           ,0
           ,'D'
           ,'D'
           ,'ADD VIA SURGERY ORDER'
           ,@UserId
           ,@serviceLocationId
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,'M'
           ,''
           ,''
           ,''
           ,'')

		DECLARE @orderAppointmentId AS int
		SET @orderAppointmentId = SCOPE_IDENTITY()

		--Add the surgery order
		INSERT INTO [dbo].[PatientClinical]
           ([AppointmentId]
           ,[PatientId]
           ,[ClinicalType]
           ,[EyeContext]
           ,[Symptom]
           ,[FindingDetail]
           ,[ImageDescriptor]
           ,[ImageInstructions]
           ,[Status]
           ,[DrawFileName]
           ,[Highlights]
           ,[FollowUp]
           ,[PermanentCondition]
           ,[PostOpPeriod]
           ,[Surgery]
           ,[Activity])
		VALUES
           (@orderAppointmentId
           ,@patientId
           ,'A'
           ,''
           ,'1'
           ,'SCHEDULE SURGERY-3/NOT SELECTED-F/-1/-7/'
           ,''
           ,''
           ,'A'
           ,''
           ,''
           ,''
           ,''
           ,0
           ,'T'
           ,'')
		SET @orderAppointmentClinicalId = SCOPE_IDENTITY()
	END

	INSERT INTO [dbo].[PatientClinicalSurgeryPlan]
           ([SurgeryClnId]
           ,[SurgeryRefType]
           ,[SurgeryValue]
           ,[SurgeryStatus])
    VALUES
           (@orderAppointmentClinicalId
           ,'F'
           ,@Id -- the new surgery appointment
           ,'A')
END

GO

-- Appointment Delete
DELETE
FROM dbo.Appointments
WHERE AppointmentId = @Id
GO

-- AppointmentType Insert
CREATE TABLE #AppointmentTypeIdentity(Id int)

INSERT INTO dbo.AppointmentType (Question)
OUTPUT INSERTED.AppTypeId INTO #AppointmentTypeIdentity(Id)
VALUES ('')

SET @Id = (SELECT TOP 1 Id FROM #AppointmentTypeIdentity)
DROP TABLE #AppointmentTypeIdentity

GO

-- AppointmentType Update/Insert
UPDATE dbo.AppointmentType
SET AppointmentType = @Name,
	ResourceId6 = @OleColor,
	AppointmentCategoryId = @AppointmentCategoryId,
	Question = (
		SELECT TOP 1 code
		FROM dbo.PracticeCodeTable
		WHERE Id = @PatientQuestionSetId
			AND ReferenceType = 'QUESTIONSETS'
		),
	FirstVisitIndicator = CASE WHEN @IsFirstVisit = CONVERT(BIT,1) THEN 'Y' ELSE 'N' END
WHERE AppTypeId = @Id
GO

-- AppointmentType Delete
DELETE
FROM dbo.AppointmentType
WHERE AppTypeId = @Id
GO

-- EmailAddress Insert
IF @BillingOrganizationId IS NOT NULL
	SET @Id = (
			SELECT model.GetPairId(CASE x
						WHEN 0
							THEN 1
						WHEN 1
							THEN 2
						END, y, @EmailAddressMax)
			FROM model.GetIdPair(@BillingOrganizationId, @BillingOrganizationMax)
			)

IF @ServiceLocationId IS NOT NULL
	SET @Id = (
			SELECT model.GetPairId(CASE x
						WHEN 0
							THEN 3
						WHEN 1
							THEN 4
						END, y, @EmailAddressMax)
			FROM model.GetIdPair(@ServiceLocationId, @ServiceLocationMax)
			)

IF @UserId IS NOT NULL
	SET @Id = model.GetPairId(0, @UserId, @EmailAddressMax)
GO

-- EmailAddress Update/Insert
DECLARE @pair TABLE (
	x int,
	y int
	)
DECLARE @x int,
	@y int

SELECT @x = x,
	@y = y
FROM model.GetIdPair(@Id, @EmailAddressMax)

IF @x IN (
		0,
		2,
		4
		)
	UPDATE Resources
	SET ResourceEmail = @Value
	WHERE ResourceId = @y

IF @x IN (
		1,
		3
		)
	UPDATE PracticeName
	SET PracticeEmail = @Value
	WHERE PracticeId = @y
GO

-- User Insert
CREATE TABLE #UserIdentity(Id int)

INSERT INTO Resources (ResourceLastName)
OUTPUT INSERTED.ResourceId INTO #UserIdentity(Id)
VALUES ('')

SET @Id = (SELECT TOP 1 Id FROM #UserIdentity)
DROP TABLE #UserIdentity

GO

-- User Update/Insert
UPDATE Resources
SET ResourceDescription = @DisplayName,
	ResourceFirstName = @FirstName,
	ResourceSuffix = @Honorific,
	[Status] = CASE @IsArchived
		WHEN 0
			THEN 'A'
		WHEN 1
			THEN 'Z'
		END,
	IsLoggedIn = @IsLoggedIn,
	ResourceLastName = @LastName,
	ResourceMI = @MiddleName,
	ResourceColor = @OleColor,
	ResourcePid = @PID,
	ResourcePrefix = @Suffix,
	ResourceName = @UserName,
	ResourceType = COALESCE(ResourceType, 'D')
WHERE ResourceId = @Id
GO

-- UserRole Update/Insert
UPDATE Resources
SET ResourceType = (
		SELECT SUBSTRING(model.roles.NAME, 1, 1)
		FROM model.roles
		WHERE @roles_Id = model.roles.id
		)
WHERE @Users_Id = ResourceId
GO

-- InsurerPlanType Insert
DECLARE @checkExists int

SELECT @checkExists = COUNT(*)
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'PLANTYPE'
	AND Code = @Name

IF @checkExists = 0
BEGIN
	CREATE TABLE #InsurerPlanTypeIdentity(Id int)
	
	INSERT INTO dbo.PracticeCodeTable (ReferenceType)
	OUTPUT INSERTED.Id INTO #InsurerPlanTypeIdentity(Id)
	VALUES ('PLANTYPE')

	SET @Id = (SELECT TOP 1 Id FROM #InsurerPlanTypeIdentity)
	DROP TABLE #InsurerPlanTypeIdentity

END
ELSE
BEGIN
	SELECT @Id = Id
	FROM dbo.PracticeCodeTable
	WHERE ReferenceType = 'PLANTYPE'
		AND Code = @Name
END
GO

-- InsurerPlanType Update/Insert
UPDATE dbo.PracticeCodeTable
SET Code = @Name
WHERE ReferenceType = 'PLANTYPE'
	AND Id = @Id
GO

-- InsurerPlanType Delete
DELETE
FROM dbo.PracticeCodeTable
WHERE Id = @Id
GO

-- BillingOrganization Insert
CREATE TABLE #BillingOrganizationIdentity(Id int)

INSERT INTO dbo.PracticeName (PracticeName)
OUTPUT INSERTED.PracticeId INTO #BillingOrganizationIdentity(Id)
VALUES ('')

SET @Id = (SELECT TOP 1 Id FROM #BillingOrganizationIdentity)

DROP TABLE #BillingOrganizationIdentity
GO

-- BillingOrganization Update/Insert
UPDATE dbo.PracticeName
SET PracticeName = @Name	
	,PracticeType = 'P'
	,LocationReference = CASE @IsMain WHEN 1 THEN '' ELSE RTRIM(LTRIM(REPLACE(@ShortName, 'office-', ''))) END
WHERE PracticeId = @Id
GO

-- BillingOrganization Delete
DELETE
FROM dbo.PracticeName
WHERE PracticeId = @Id
GO


GO

-- ServiceLocation Insert
CREATE TABLE #ServiceLocationIdentity(Id int)

INSERT INTO dbo.PracticeName (PracticeName)
OUTPUT INSERTED.PracticeId INTO #ServiceLocationIdentity(Id)
VALUES ('')

SET @Id = (SELECT TOP 1 Id FROM #ServiceLocationIdentity)
SET @Id = model.GetPairId(0, @Id, @ServiceLocationMax)

DROP TABLE #ServiceLocationIdentity

GO

-- ServiceLocation Update/Insert
UPDATE dbo.PracticeName
SET PracticeName = @Name
	,LocationReference = CASE @ShortName
		WHEN 'Office'
			THEN ''
		ELSE RTRIM(LTRIM(REPLACE(@shortName, 'Office-', '')))
		END
	,PracticeType = 'P'
WHERE PracticeId = @Id
GO

-- ServiceLocation Delete
DELETE
FROM dbo.PracticeName
WHERE PracticeId = @Id
GO

-- BillingOrganizationAddress Insert
IF @BillingOrganizationId IS NOT NULL
	SET @Id = (
			SELECT model.GetBigPairId(CASE x
						WHEN 0
							THEN 4
						WHEN 1
							THEN 5
						WHEN 2
							THEN 17
						END, y, @AddressMax)
			FROM model.GetBigIdPair(@BillingOrganizationId, @BillingOrganizationMax)
			)
GO

-- ServiceLocationAddress Insert
IF @ServiceLocationId IS NOT NULL
	SET @Id = (
			SELECT model.GetPairId(CASE x
						WHEN 0
							THEN 6
						WHEN 1
							THEN 7
						END, y, @AddressMax)
			FROM model.GetIdPair(@ServiceLocationId, @ServiceLocationMax)
			)
GO

-- UserAddress Insert
IF @UserId IS NOT NULL
	SET @Id = model.GetPairId(8, @UserId, @AddressMax)
GO

-- BillingOrganizationAddress Update/Insert
-- UserAddress Update/Insert
DECLARE @pair TABLE (
	x int
	,y int
	)
DECLARE @x int
	,@y int

SELECT @x = x
	,@y = y
FROM model.GetBigIdPair(@Id, @AddressMax)

---BillingOrganization -PracticeName Table
IF @x IN (
		4
		, 17
		)
	BEGIN
		UPDATE PracticeName
		SET PracticeCity = @City
			,PracticeAddress = @Line1
			,PracticeSuite = @Line2
			,PracticeZip = @PostalCode
			,PracticeState = (
				SELECT Abbreviation
				FROM model.StateOrProvinces
				WHERE Id = @StateOrProvinceId
				)
		WHERE PracticeId = @y
	END
---BillingOrganization, User -Resources Table
IF @x IN (
		5
		,8
		)
	BEGIN
		UPDATE Resources
		SET ResourceCity = @City
			,ResourceAddress = @Line1
			,ResourceSuite = @Line2
			,ResourceZip = @PostalCode
			,ResourceState = (
				SELECT Abbreviation
				FROM model.StateOrProvinces
				WHERE Id = @StateOrProvinceId
				)
		WHERE ResourceId = @y
	END
GO

-- ServiceLocationAddress Update/Insert
DECLARE @pair TABLE (
	x int
	,y int
	)
DECLARE @x int
	,@y int

SELECT @x = x
	,@y = y
FROM model.GetBigIdPair(@Id, @AddressMax)

---BillingOrganization, ServiceLocation -PracticeName Table
IF @x = 6
	BEGIN
		UPDATE PracticeName
		SET PracticeCity = @City
			,PracticeAddress = @Line1
			,PracticeSuite = @Line2
			,PracticeZip = @PostalCode
			,PracticeState = (
				SELECT Abbreviation
				FROM model.StateOrProvinces
				WHERE Id = @StateOrProvinceId
				)
		WHERE PracticeId = @y
	END
---BillingOrganization, ServiceLocation, User -Resources Table
	IF @x = 7 AND @ServiceLocationAddressTypeId = 7
		RAISERROR('Cannot update resource table with main office', 16, 1)
	ELSE
		BEGIN
			UPDATE Resources
			SET ResourceCity = @City
				,ResourceAddress = @Line1
				,ResourceSuite = @Line2
				,ResourceZip = @PostalCode
				,ResourceState = (
					SELECT Abbreviation
					FROM model.StateOrProvinces
					WHERE Id = @StateOrProvinceId
					)
			WHERE ResourceId = @y
		END
GO

-- BillingOrganizationPhoneNumber Insert
IF @BillingOrganizationId IS NOT NULL
	SET @Id = (
			SELECT model.GetPairId(CASE x
						WHEN 0
							THEN (
									CASE @BillingOrganizationPhoneNumberTypeId
										WHEN 9
											THEN 1
										WHEN 1
											THEN 2
										WHEN 6
											THEN 3
										END
									)
						WHEN 1
							THEN 4
						END, y, @PhoneNumberMax)
			FROM model.GetIdPair(@BillingOrganizationId, @BillingOrganizationMax)
			)
GO

-- ServiceLocationPhoneNumber Insert
IF @ServiceLocationId IS NOT NULL
	SET @Id = (
			SELECT model.GetPairId(CASE x
						WHEN 0
							THEN (
									CASE @ServiceLocationPhoneNumberTypeId
										WHEN (
												SELECT Id + 1000
												FROM dbo.PracticeCodeTable
												WHERE Code = 'Primary'
													AND ReferenceType = 'PHONETYPESERVICELOCATION'
												)
											THEN 5
										WHEN (
												SELECT Id + 1000
												FROM dbo.PracticeCodeTable
												WHERE Code = 'Other'
													AND ReferenceType = 'PHONETYPESERVICELOCATION'
												)
											THEN 6
										WHEN (
												SELECT Id + 1000
												FROM dbo.PracticeCodeTable
												WHERE Code = 'Fax'
													AND ReferenceType = 'PHONETYPESERVICELOCATION'
												)
											THEN 7
										END
									)
						WHEN 1
							THEN 8
						END, y, @PhoneNumberMax)
			FROM model.GetIdPair(@ServiceLocationId, @ServiceLocationMax)
			)
GO


-- UserPhoneNumber Insert
IF @UserId IS NOT NULL
	SET @Id = model.GetPairId(0, @UserId, @PhoneNumberMax)
GO

-- BillingOrganizationPhoneNumber Update/Insert
DECLARE @x int
	,@y int

SELECT @x = x
	,@y = y
FROM model.GetIdPair(@Id, @PhoneNumberMax)

---Clear current phoneNumber column using current id (@x)
IF @x IN (
		1
		,5
		)
	UPDATE PracticeName
	SET PracticePhone = ''
	WHERE PracticeId = @y

IF @x IN (
		2
		,6
		)
	UPDATE PracticeName
	SET PracticeOtherPhone = ''
	WHERE PracticeId = @y

IF @x IN (
		3
		,7
		)
	UPDATE PracticeName
	SET PracticeFax = ''
	WHERE PracticeId = @y

IF @x IN (
		4
		,8
		,0
		)
	UPDATE Resources
	SET ResourcePhone = ''
	WHERE ResourceId = @y

---Then get new @x, @y to update the correct phonenumber column (accounts for updated PhoneNumberType)
---BillingOrganizationPhoneNumber
IF @BillingOrganizationId IS NOT NULL
	SET @Id = (
			SELECT model.GetPairId(CASE x
						WHEN 0
							THEN (
									CASE @BillingOrganizationPhoneNumberTypeId
										WHEN 9
											THEN 1
										WHEN 1
											THEN 2
										WHEN 6
											THEN 3
										END
									)
						WHEN 1
							THEN 4
						END, y, @PhoneNumberMax)
			FROM model.GetIdPair(@BillingOrganizationId, @BillingOrganizationMax)
			)

---Requery for new @x,@y to account for changed PhoneNumberTypes
SELECT @x = x
	,@y = y
FROM model.GetIdPair(@Id, @PhoneNumberMax)


IF @x IN (
		1
		,5
		)
	UPDATE PracticeName
	SET PracticePhone = CASE
			WHEN @CountryCode IS NOT NULL
				THEN @CountryCode
			ELSE ''
			END + CASE
			WHEN @AreaCode IS NOT NULL
				THEN @AreaCode
			ELSE ''
			END + CASE 
			WHEN @ExchangeAndSuffix IS NOT NULL
				THEN @ExchangeAndSuffix
			ELSE ''
			END + CASE 
			WHEN @Extension IS NOT NULL
				THEN @Extension
			ELSE ''
			END
	WHERE PracticeId = @y

IF @x IN (
		2
		,6
		)
	UPDATE PracticeName
	SET PracticeOtherPhone = CASE 
			WHEN @CountryCode IS NOT NULL
				THEN @CountryCode
			ELSE ''
			END + CASE
			WHEN @AreaCode IS NOT NULL
				THEN @AreaCode
			ELSE ''
			END + CASE 
			WHEN @ExchangeAndSuffix IS NOT NULL
				THEN @ExchangeAndSuffix
			ELSE ''
			END + CASE 
			WHEN @Extension IS NOT NULL
				THEN @Extension
			ELSE ''
			END
	WHERE PracticeId = @y

IF @x IN (
		3
		,7
		)
	UPDATE PracticeName
	SET PracticeFax = CASE
			WHEN @CountryCode IS NOT NULL
				THEN @CountryCode
			ELSE ''
			END + CASE
			WHEN @AreaCode IS NOT NULL
				THEN @AreaCode
			ELSE ''
			END + CASE 
			WHEN @ExchangeAndSuffix IS NOT NULL
				THEN @ExchangeAndSuffix
			ELSE ''
			END + CASE 
			WHEN @Extension IS NOT NULL
				THEN @Extension
			ELSE ''
			END
	WHERE PracticeId = @y

-- for users and personbillingorganizations
IF @x IN (
		4
		,8
		,0
		)
	UPDATE Resources
	SET ResourcePhone = CASE
			WHEN @CountryCode IS NOT NULL
				THEN @CountryCode
			ELSE ''
			END + CASE
			WHEN @AreaCode IS NOT NULL
				THEN @AreaCode
			ELSE ''
			END + CASE 
			WHEN @ExchangeAndSuffix IS NOT NULL
				THEN @ExchangeAndSuffix
			ELSE ''
			END + CASE 
			WHEN @Extension IS NOT NULL
				THEN @Extension
			ELSE ''
			END
	WHERE ResourceId = @y
GO

-- UserPhoneNumber Update/Insert
DECLARE @x int
	,@y int

SELECT @x = x
	,@y = y
FROM model.GetIdPair(@Id, @PhoneNumberMax)

---Clear current phoneNumber column using current id (@x)
IF @x IN (
		1
		,5
		)
	UPDATE PracticeName
	SET PracticePhone = ''
	WHERE PracticeId = @y

IF @x IN (
		2
		,6
		)
	UPDATE PracticeName
	SET PracticeOtherPhone = ''
	WHERE PracticeId = @y

IF @x IN (
		3
		,7
		)
	UPDATE PracticeName
	SET PracticeFax = ''
	WHERE PracticeId = @y

IF @x IN (
		4
		,8
		,0
		)
	UPDATE Resources
	SET ResourcePhone = ''
	WHERE ResourceId = @y

---Then get new @x, @y to update the correct phonenumber column (accounts for updated PhoneNumberType)
IF @UserId IS NOT NULL
	SET @Id = model.GetPairId(0, @UserId, @PhoneNumberMax)

---Requery for new @x,@y to account for changed PhoneNumberTypes
SELECT @x = x
	,@y = y
FROM model.GetIdPair(@Id, @PhoneNumberMax)


IF @x IN (
		1
		,5
		)
	UPDATE PracticeName
	SET PracticePhone = CASE
			WHEN @CountryCode IS NOT NULL
				THEN @CountryCode
			ELSE ''
			END + CASE
			WHEN @AreaCode IS NOT NULL
				THEN @AreaCode
			ELSE ''
			END + CASE 
			WHEN @ExchangeAndSuffix IS NOT NULL
				THEN @ExchangeAndSuffix
			ELSE ''
			END + CASE 
			WHEN @Extension IS NOT NULL
				THEN @Extension
			ELSE ''
			END
	WHERE PracticeId = @y

IF @x IN (
		2
		,6
		)
	UPDATE PracticeName
	SET PracticeOtherPhone = CASE 
			WHEN @CountryCode IS NOT NULL
				THEN @CountryCode
			ELSE ''
			END + CASE
			WHEN @AreaCode IS NOT NULL
				THEN @AreaCode
			ELSE ''
			END + CASE 
			WHEN @ExchangeAndSuffix IS NOT NULL
				THEN @ExchangeAndSuffix
			ELSE ''
			END + CASE 
			WHEN @Extension IS NOT NULL
				THEN @Extension
			ELSE ''
			END
	WHERE PracticeId = @y

IF @x IN (
		3
		,7
		)
	UPDATE PracticeName
	SET PracticeFax = CASE
			WHEN @CountryCode IS NOT NULL
				THEN @CountryCode
			ELSE ''
			END + CASE
			WHEN @AreaCode IS NOT NULL
				THEN @AreaCode
			ELSE ''
			END + CASE 
			WHEN @ExchangeAndSuffix IS NOT NULL
				THEN @ExchangeAndSuffix
			ELSE ''
			END + CASE 
			WHEN @Extension IS NOT NULL
				THEN @Extension
			ELSE ''
			END
	WHERE PracticeId = @y

-- for users and personbillingorganizations
IF @x IN (
		4
		,8
		,0
		)
	UPDATE Resources
	SET ResourcePhone = CASE
			WHEN @CountryCode IS NOT NULL
				THEN @CountryCode
			ELSE ''
			END + CASE
			WHEN @AreaCode IS NOT NULL
				THEN @AreaCode
			ELSE ''
			END + CASE 
			WHEN @ExchangeAndSuffix IS NOT NULL
				THEN @ExchangeAndSuffix
			ELSE ''
			END + CASE 
			WHEN @Extension IS NOT NULL
				THEN @Extension
			ELSE ''
			END
	WHERE ResourceId = @y
GO

-- IdentifierCode Insert
IF  (@ExternalProviderId IS NOT NULL)
BEGIN
	SELECT @Id=model.GetPairId(@IdentifierCodeTypeId,@ExternalProviderId,@IdentifierCodeMax)
END

GO

-- IdentifierCode Update/Insert
IF (@ExternalProviderId IS NOT NULL)
BEGIN
	IF (@IdentifierCodeTypeId = 1)
	BEGIN 
		UPDATE e
		SET LegacyTaxonomy = @Value
		FROM model.ExternalContacts e
		WHERE e.Id = @ExternalProviderId 
	END

	IF (@IdentifierCodeTypeId = 2)
	BEGIN 
		UPDATE e
		SET LegacyNpi = @Value
		FROM model.ExternalContacts e
		WHERE e.Id = @ExternalProviderId 
	END
END
GO

-- IdentifierCode Delete
IF (@ExternalProviderId IS NOT NULL)
BEGIN
	IF (@IdentifierCodeTypeId = 1)
	BEGIN 
		UPDATE e
		SET LegacyTaxonomy = NULL
		FROM model.ExternalContacts e
		WHERE e.Id = @ExternalProviderId 
	END

	IF (@IdentifierCodeTypeId = 2)
	BEGIN 
		UPDATE e
		SET LegacyNpi = NULL
		FROM model.ExternalContacts e
		WHERE e.Id = @ExternalProviderId 
	END
END
GO

-- PatientRecall Insert
CREATE TABLE #PatientRecallIdentity(Id int)

INSERT INTO dbo.PracticeTransactionJournal (TransactionType) 
OUTPUT INSERTED.TransactionId INTO #PatientRecallIdentity(Id)
VALUES ('')

SET @Id = (SELECT TOP 1 Id FROM #PatientRecallIdentity)
DROP TABLE #PatientRecallIdentity

GO


-- PatientRecall Update/Insert

DECLARE @AppType nvarchar(1024),
@DoctorName nvarchar(1024),
@TransactionRef nvarchar(1024)

SELECT @AppType = apt.appointmenttype FROM dbo.AppointmentType apt where apt.AppTypeId = @AppointmentTypeId;
SELECT @DoctorName = re.ResourceName FROM dbo.Resources re WHERE re.ResourceId = @UserId;
SET @TransactionRef = @AppType + '/' + @DoctorName;

UPDATE dbo.PracticeTransactionJournal SET		
			TransactionType = 'L',
			TransactionTypeId = @PatientId,		
			TransactionRef = @TransactionRef,
			-- servicelocation 
			TransactionStatus = CASE @RecallStatusId
								WHEN 1 THEN 'P'
								WHEN 2 THEN 'S'
								ELSE 'Z'
								END,
			TransactionRemark = 
								CASE @RecallStatusId
								WHEN 1 THEN ''
								WHEN 2 THEN 
									CASE @MethodSentId 
									WHEN 1 THEN '[' + CONVERT(VARCHAR, @DueDateTime, 112) + ']'					
									ELSE
									CONVERT(VARCHAR, @DueDateTime, 112)
									END
								ELSE
								CONVERT(VARCHAR, @DueDateTime, 112)
								END,
			TransactionDate = 
							  CASE @RecallStatusId
							  WHEN 1 THEN CONVERT(VARCHAR, @DueDateTime, 112)
							  ELSE
								  CASE 
								  WHEN @SentOrClosedDateTime IS NULL THEN NULL
								  ELSE
								  CONVERT(VARCHAR, @SentOrClosedDateTime, 112)
								  END	
							 END,
			TransactionAction = '-'
			WHERE TransactionId = @Id

GO

-- PatientRecall Delete
DELETE FROM dbo.PracticeTransactionJournal WHERE TransactionId = @Id

GO

-- ClinicalSpecialtyType Insert
DECLARE @checkExists int

SELECT @checkExists = COUNT(*)
FROM dbo.PracticeCodeTable
WHERE ReferenceType = 'SPECIALTY'
	AND Code = @Name

IF @checkExists = 0
BEGIN
	CREATE TABLE #ClinicalSpecialtyTypeIdentity(Id int)
	
	INSERT INTO dbo.PracticeCodeTable (ReferenceType)
	OUTPUT INSERTED.Id INTO #ClinicalSpecialtyTypeIdentity(Id)
	VALUES ('SPECIALTY')

	SET @Id = (SELECT TOP 1 Id FROM #ClinicalSpecialtyTypeIdentity)
	DROP TABLE #ClinicalSpecialtyTypeIdentity

END
ELSE
BEGIN
	SELECT @Id = Id
	FROM dbo.PracticeCodeTable
	WHERE ReferenceType = 'SPECIALTY'
		AND Code = @Name
END
GO

-- ClinicalSpecialtyType Update/Insert
UPDATE dbo.PracticeCodeTable
SET Code = @Name
WHERE ReferenceType = 'SPECIALTY'
	AND Id = @Id
GO

-- ClinicalSpecialtyType Delete
DELETE
FROM dbo.PracticeCodeTable
WHERE Id = @Id
GO

-- ExternalProviderClinicalSpecialtyType Insert
IF NOT EXISTS(SELECT TOP 1 * FROM dbo.PracticeVendors WHERE VendorId=@ExternalProviderId)
BEGIN
	CREATE TABLE #ExternalProviderClinicalSpecialtyTypeIdentity(Id int)

	INSERT INTO dbo.PracticeVendors (vendorname)
	OUTPUT INSERTED.VendorId INTO #ExternalProviderClinicalSpecialtyTypeIdentity(Id)
	VALUES ('')

	SET @Id = (SELECT TOP 1 Id FROM #ExternalProviderClinicalSpecialtyTypeIdentity)

	DROP TABLE #ExternalProviderClinicalSpecialtyTypeIdentity
END
ELSE
BEGIN
	SET @Id=@ExternalProviderId
END
GO

-- ExternalProviderClinicalSpecialtyType Update/Insert
IF (@OrdinalId=1)
BEGIN
	UPDATE dbo.PracticeVendors
	SET VendorSpecialty=(SELECT TOP 1 Code FROM dbo.PracticeCodeTable WHERE ReferenceType = 'SPECIALTY' AND Id=@ClinicalSpecialtyTypeId)
	WHERE VendorId=@Id
END
ELSE
BEGIN
	UPDATE dbo.PracticeVendors
	SET VendorSpecOther=(SELECT TOP 1 Code FROM dbo.PracticeCodeTable WHERE ReferenceType = 'SPECIALTY' AND Id=@ClinicalSpecialtyTypeId)
	WHERE VendorId=@Id
END
GO

-- ExternalProviderClinicalSpecialtyType Delete
DELETE
FROM dbo.PracticeVendors
WHERE VendorId = @Id
GO

-- PatientAllergen Insert
DECLARE @AppointmentId  INT
SET @AppointmentId = COALESCE((SELECT TOP 1 pc.AppointmentId FROM dbo.PatientClinical pc WHERE pc.PatientId = @PatientId ORDER BY pc.ClinicalId DESC)
	,(SELECT TOP 1 ap.AppointmentId FROM dbo.Appointments ap WHERE ap.PatientId = @PatientId ORDER BY ap.AppointmentId DESC))
							
IF (dbo.IsNullOrEmpty(@AppointmentId) = 1)
BEGIN
	RAISERROR ('When trying to insert the PatientAllergen, could not locate a valid AppointmentId.',16,1)
END
						
DECLARE @AllergenAllergenName NVARCHAR(MAX)
SELECT @AllergenAllergenName = Name FROM model.Allergens WHERE Id = @AllergenId
			
IF EXISTS (
	SELECT 
	SecondaryControlId AS Id
	,ControlName AS Name
	,v.Id AS ParentId
	,v.Name AS ParentName
	FROM df.SecondaryFormsControls s
	INNER JOIN (
		SELECT 
		v.ControlId  AS Id
		,FormControlName AS Name
		,SecondaryFormId
		FROM df.SecondaryForms sf
		INNER JOIN (SELECT ControlId, ControlName FROM df.FormsControls WHERE FormId = 171 AND ControlType = 'B')	
			v ON v.ControlName = sf.FormControlName
		WHERE FormId IN (
			SELECT FormId
			FROM df.SecondaryForms
			WHERE FormControlName LIKE 'ANTIBIOTICS'
		)
	) v ON v.SecondaryFormId = s.SecondaryFormId
	WHERE s.SecondaryFormId IN (
		SELECT SecondaryFormId
		FROM df.SecondaryForms
		WHERE FormId IN (
				SELECT FormId
				FROM df.SecondaryForms
				WHERE FormControlName LIKE 'ANTIBIOTICS'
		)
	)
	AND ControlType = 'B'
	AND ((ControlName LIKE '%'+@AllergenAllergenName+'%') OR (v.Name LIKE '%'+@AllergenAllergenName+'%'))
)
BEGIN
	INSERT INTO dbo.PatientClinical (AppointmentId,PatientId,ClinicalType,EyeContext,Symptom,FindingDetail,ImageDescriptor,ImageInstructions,[Status]
		,DrawFileName,Highlights,FollowUp,PermanentCondition,PostOpPeriod,Surgery,Activity,LegacyClinicalDataSourceTypeId)
	SELECT * FROM (
		SELECT 
		@AppointmentId AS AppointmentId
		,@PatientId AS PatientId
		,'H' AS ClinicalType
		,'OU' AS EyeContext
		,'/ANY DRUG ALLERGIES' AS Symptom
		,v.Name+'=T'+CONVERT(NVARCHAR(1000),v.Id)+' <'+v.Name+'>' AS FindingDetail
		,'' AS ImageDescriptor
		,'' AS ImageInstructions
		,'A' AS [Status]
		,'' AS DrawFileName
		,'' AS HighLights
		,'' AS FollowUp
		,'' AS PermanentCondition
		,0 AS PostOpPeriod
		,'' AS Surgery
		,'' AS Activity
		,@ClinicalDataSourceTypeId AS LegacyClinicalDataSourceTypeId
		FROM df.SecondaryFormsControls s
		INNER JOIN (
			SELECT 
			v.ControlId  AS Id
			,FormControlName AS Name
			,SecondaryFormId
			FROM df.SecondaryForms sf
			INNER JOIN (SELECT ControlId, ControlName FROM df.FormsControls WHERE FormId = 171 AND ControlType = 'B')	
				v ON v.ControlName = sf.FormControlName
			WHERE FormId IN (
				SELECT FormId
				FROM df.SecondaryForms
				WHERE FormControlName LIKE 'ANTIBIOTICS'
			)
		) v ON v.SecondaryFormId = s.SecondaryFormId
		WHERE s.SecondaryFormId IN (
			SELECT SecondaryFormId
			FROM df.SecondaryForms
			WHERE FormId IN (
					SELECT FormId
					FROM df.SecondaryForms
					WHERE FormControlName LIKE 'ANTIBIOTICS'
			)
		)
		AND ControlType = 'B'
		AND ((ControlName LIKE '%'+@AllergenAllergenName+'%') OR (v.Name LIKE '%'+@AllergenAllergenName+'%'))
						
		UNION ALL
						
		SELECT 
		@AppointmentId AS AppointmentId
		,@PatientId AS PatientId
		,'H' AS ClinicalType
		,'OU' AS EyeContext
		,'?ALLERGY' AS Symptom
		,ControlName+'=T'+CONVERT(NVARCHAR(1000),SecondaryControlId)+' <'+v.Name+'>' AS FindingDetail
		,'' AS ImageDescriptor
		,'' AS ImageInstructions
		,'A' AS [Status]
		,'' AS DrawFileName
		,'' AS HighLights
		,'' AS FollowUp
		,'' AS PermanentCondition
		,0 AS PostOpPeriod
		,'' AS Surgery
		,'' AS Activity
		,@ClinicalDataSourceTypeId AS LegacyClinicalDataSourceTypeId
		FROM df.SecondaryFormsControls s
		INNER JOIN (
			SELECT 
			v.ControlId  AS Id
			,FormControlName AS Name
			,SecondaryFormId
			FROM df.SecondaryForms sf
			INNER JOIN (SELECT ControlId, ControlName FROM df.FormsControls WHERE FormId = 171 AND ControlType = 'B')	
				v ON v.ControlName = sf.FormControlName
			WHERE FormId IN (
				SELECT FormId
				FROM df.SecondaryForms
				WHERE FormControlName LIKE 'ANTIBIOTICS'
			)
		) v ON v.SecondaryFormId = s.SecondaryFormId
		WHERE s.SecondaryFormId IN (
			SELECT SecondaryFormId
			FROM df.SecondaryForms
			WHERE FormId IN (
				SELECT FormId
				FROM df.SecondaryForms
				WHERE FormControlName LIKE 'ANTIBIOTICS'
			)
		)
		AND ControlType = 'B'
		AND ((ControlName LIKE '%'+@AllergenAllergenName+'%') OR (v.Name LIKE '%'+@AllergenAllergenName+'%'))
	)v
						
	IF (@@ROWCOUNT > 0) SET @Id = SCOPE_IDENTITY()
END
ELSE
BEGIN
	INSERT INTO dbo.PatientClinical (AppointmentId,PatientId,ClinicalType,EyeContext,Symptom,FindingDetail,ImageDescriptor,ImageInstructions,[Status]
		,DrawFileName,Highlights,FollowUp,PermanentCondition,PostOpPeriod,Surgery,Activity,LegacyClinicalDataSourceTypeId)
	SELECT TOP 1
	@AppointmentId AS AppointmentId
	,@PatientId AS PatientId
	,'H' AS ClinicalType
	,'OU' AS EyeContext
	,'/ALLERGIES ALL' AS Symptom
	,@AllergenAllergenName+'='+CONVERT(NVARCHAR(1000),e.ExternalSystemEntityKey)+' <'+@AllergenAllergenName+'>' AS FindingDetail
	,'' AS ImageDescriptor
	,'' AS ImageInstructions
	,'A' AS [Status]
	,e.ExternalSystemEntityKey AS DrawFileName
	,'' AS HighLights
	,'' AS FollowUp
	,'' AS PermanentCondition
	,0 AS PostOpPeriod
	,'' AS Surgery
	,'' AS Activity
	,@ClinicalDataSourceTypeId AS LegacyClinicalDataSourceTypeId
	FROM model.ExternalSystemEntityMappings e
	WHERE e.PracticeRepositoryEntityId = (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Allergen')
		AND e.PracticeRepositoryEntityKey = @AllergenId
		AND e.ExternalSystemEntityId = (
			SELECT 
			a.Id AS ExternalSystemEntityId
			FROM model.ExternalSystemEntities a
			JOIN model.ExternalSystems b ON a.ExternalSystemId = b.Id
			WHERE a.Name = 'Allergen'
				AND LOWER(b.Name) = 'rxnorm'
		)
	ORDER BY e.PracticeRepositoryEntityKey ASC
						
	IF (@@ROWCOUNT > 0) SET @Id = SCOPE_IDENTITY()
END
GO

-- PatientAllergen Update
DECLARE @NewAllergenName NVARCHAR(MAX)
SELECT @NewAllergenName= Name FROM model.Allergens WHERE Id = @AllergenId

UPDATE pc
SET PatientId = @PatientId
,pc.FindingDetail = CASE 
	WHEN pc.Symptom = '/ALLERGIES ALL'
		THEN (SELECT TOP 1
			@NewAllergenName+'='+ExternalSystemEntityKey+' <'+@NewAllergenName+'>'
			FROM model.ExternalSystemEntityMappings
			WHERE PracticeRepositoryEntityId = (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Allergen')
			AND PracticeRepositoryEntityKey = @AllergenId
			ORDER BY PracticeRepositoryEntityKey ASC)
	WHEN pc.Symptom = '/ANY DRUG ALLERGIES'
		THEN (SELECT 
			* 
			FROM (
				SELECT 
				UPPER(v.Name+'=T'+CONVERT(NVARCHAR(MAX),v.Id)+' <'+v.Name+'>') AS FindingDetail
				FROM df.SecondaryFormsControls s
				INNER JOIN (
					SELECT 
					v.ControlId  AS Id
					,FormControlName AS Name
					,SecondaryFormId
					FROM df.SecondaryForms sf
					INNER JOIN (SELECT ControlId, ControlName FROM df.FormsControls WHERE FormId = 171 AND ControlType = 'B')	
						v ON v.ControlName = sf.FormControlName
					WHERE FormId IN (
						SELECT FormId
						FROM df.SecondaryForms
						WHERE FormControlName LIKE 'ANTIBIOTICS'
					)
			) v ON v.SecondaryFormId = s.SecondaryFormId
			WHERE s.SecondaryFormId IN (
				SELECT SecondaryFormId
				FROM df.SecondaryForms
				WHERE FormId IN (
						SELECT FormId
						FROM df.SecondaryForms
						WHERE FormControlName LIKE 'ANTIBIOTICS'
				)
			)
			AND ControlType = 'B'
			AND ((ControlName LIKE '%'+@NewAllergenName+'%') OR (v.Name LIKE '%'+@NewAllergenName+'%')))n)
	WHEN pc.Symptom = '?ALLERGY'
		THEN (SELECT 
			UPPER(ControlName+'=T'+CONVERT(NVARCHAR(MAX),SecondaryControlId)+' <'+v.Name+'>') AS FindingDetail
			FROM df.SecondaryFormsControls s
			INNER JOIN (
				SELECT 
				v.ControlId  AS Id
				,FormControlName AS Name
				,SecondaryFormId
				FROM df.SecondaryForms sf
				INNER JOIN (SELECT ControlId, ControlName FROM df.FormsControls WHERE FormId = 171 AND ControlType = 'B')	
					v ON v.ControlName = sf.FormControlName
				WHERE FormId IN (
					SELECT FormId
					FROM df.SecondaryForms
					WHERE FormControlName LIKE 'ANTIBIOTICS'
				)
			) v ON v.SecondaryFormId = s.SecondaryFormId
			WHERE s.SecondaryFormId IN (
				SELECT SecondaryFormId
				FROM df.SecondaryForms
				WHERE FormId IN (
					SELECT FormId
					FROM df.SecondaryForms
					WHERE FormControlName LIKE 'ANTIBIOTICS'
				)
			)
			AND ControlType = 'B'
			AND ((ControlName LIKE '%'+@NewAllergenName+'%') OR (v.Name LIKE '%'+@NewAllergenName+'%'))	
			)
END
,pc.LegacyClinicalDataSourceTypeId = @ClinicalDataSourceTypeId
FROM dbo.PatientClinical pc
WHERE pc.ClinicalId = @AllergenId

GO

-- PatientAllergenDetail Update/Insert
DECLARE @AllergenAllergenName nvarchar(max)
SET @AllergenAllergenName = (SELECT SUBSTRING(FindingDetail, 1, CHARINDEX('=', FindingDetail) - 1 ) FROM PatientClinical WHERE ClinicalId = @PatientAllergenId)
			
DECLARE @Grouping nvarchar(max)
SET @Grouping = (SELECT SUBSTRING(FindingDetail, CHARINDEX('<', FindingDetail), CHARINDEX('>', FindingDetail)) FROM PatientClinical WHERE ClinicalId = @PatientAllergenId)
			
DECLARE @AppointmentId int
SET @AppointmentId = (SELECT AppointmentId FROM dbo.PatientClinical WHERE ClinicalId = @PatientAllergenId)
	
DECLARE @PatientId int
SET @PatientId = (SELECT PatientId FROM dbo.PatientClinical WHERE ClinicalId = @PatientAllergenId)

IF EXISTS(
	SELECT *
	FROM df.ThirdForms third
	INNER JOIN df.ThirdFormsControls tc ON third.ThirdFormId = tc.ThirdFormId
	WHERE third.Question LIKE '%Allergy Status' 
		AND tc.ControlType = 'B'
		AND tc.ControlName LIKE '%' + @AllergenAllergenName + '%' 
		AND tc.ControlName = @AllergenAllergenName + '-Active-dalg'
) AND NOT EXISTS (
	SELECT * 
	FROM dbo.PatientClinical 
	WHERE PatientId = @PatientId 
		AND Symptom = ']ALLERGY STATUS' 
		AND SUBSTRING(FindingDetail, 1, CHARINDEX('-', FindingDetail) - 1 ) = @AllergenAllergenName)

BEGIN

INSERT INTO dbo.PatientClinical (AppointmentId,PatientId,ClinicalType,EyeContext,Symptom,FindingDetail,ImageDescriptor,ImageInstructions,[Status]
	,DrawFileName,Highlights,FollowUp,PermanentCondition,PostOpPeriod,Surgery,Activity)
SELECT 
@AppointmentId AS AppointmentId
,@PatientId AS PatientId
,'H' AS ClinicalType
,'OU' AS EyeContext
,']ALLERGY STATUS' AS Symptom
,tc.controlName + '=T'+ CONVERT(NVARCHAR(1000),tc.ThirdControlId) + ' ' + @Grouping AS FindingDetail
,'' AS ImageDescriptor
,'' AS ImageInstructions
,'A' AS [Status]
,'' AS DrawFileName
,'' AS HighLights
,'' AS FollowUp
,'' AS PermanentCondition
,0 AS PostOpPeriod
,'' AS Surgery
,'' AS Activity
FROM df.ThirdForms third
INNER JOIN df.ThirdFormsControls tc ON third.ThirdFormId = tc.ThirdFormId
WHERE third.Question LIKE '%Allergy Status' AND tc.ControlType = 'B'
	AND tc.ControlName = @AllergenAllergenName + '-Active-dalg'
	
END
SET @Id = @PatientAllergenId

GO

-- PatientAllergenAllergenReactionType Update/Insert
DECLARE @AllergenAllergenName nvarchar(max)
SET @AllergenAllergenName = (SELECT SUBSTRING(FindingDetail, 1, CHARINDEX('=', FindingDetail) - 1 ) FROM PatientClinical WHERE ClinicalId = @PatientAllergenId)
			
DECLARE @AppointmentId int
SET @AppointmentId = (SELECT AppointmentId FROM dbo.PatientClinical WHERE ClinicalId = @PatientAllergenId)
	
DECLARE @PatientId int
SET @PatientId = (SELECT PatientId FROM dbo.PatientClinical WHERE ClinicalId = @PatientAllergenId)

DECLARE @Reaction nvarchar(max)
SET @Reaction = (SELECT Name FROM model.AllergenReactionTypes WHERE Id = @AllergenReactionTypeId)

DECLARE @Grouping nvarchar(max)
SET @Grouping = (SELECT SUBSTRING(FindingDetail, CHARINDEX('<', FindingDetail), CHARINDEX('>', FindingDetail)) FROM PatientClinical WHERE ClinicalId = @PatientAllergenId)
	
		
IF EXISTS (
	SELECT *
	FROM df.FourthForms ff
	INNER JOIN df.FourthFormsControls fc ON ff.FourthFormId = fc.FourthFormId
	WHERE ff.Question = 'Allergy Reaction' 
		AND fc.ControlType = 'B'
		AND fc.ControlName LIKE '%' + @AllergenAllergenName + '%' + @Reaction + '%'
		)
AND NOT EXISTS (
	SELECT * 
	FROM dbo.PatientClinical 
	WHERE PatientId = @PatientId 
		AND Symptom = '[ALLERGY REACTION' 
		AND SUBSTRING(FindingDetail, 1, CHARINDEX('-', FindingDetail) - 1 ) = @AllergenAllergenName)

BEGIN

INSERT INTO dbo.PatientClinical (AppointmentId,PatientId,ClinicalType,EyeContext,Symptom,FindingDetail,ImageDescriptor,ImageInstructions,[Status]
	,DrawFileName,Highlights,FollowUp,PermanentCondition,PostOpPeriod,Surgery,Activity)
SELECT TOP 1
@AppointmentId AS AppointmentId
,@PatientId AS PatientId
,'H' AS ClinicalType
,'OU' AS EyeContext
,'[ALLERGY REACTION' AS Symptom
,fc.ControlName + '=T'+ CONVERT(NVARCHAR(1000),fc.FourthControlId) + ' ' + @Grouping AS FindingDetail
,'' AS ImageDescriptor
,'' AS ImageInstructions
,'A' AS [Status]
,'' AS DrawFileName
,'' AS HighLights
,'' AS FollowUp
,'' AS PermanentCondition
,0 AS PostOpPeriod
,'' AS Surgery
,'' AS Activity
FROM df.FourthForms ff
INNER JOIN df.FourthFormsControls fc ON ff.FourthFormId = fc.FourthFormId
WHERE ff.Question = 'Allergy Reaction' 
	AND fc.ControlType = 'B'
	AND fc.ControlName LIKE '%' + @AllergenAllergenName + '%' + @Reaction + '%'
	AND fc.ControlName LIKE '%-active%' --Need this for now because we have no way of knowing the reaction status.

END
SET @Id = @PatientAllergenId

GO

-- ClinicalCondition Insert
--- For unit tests only.
SET @Id = (SELECT CASE WHEN MAX(PrimaryDrillID) IS NOT NULL THEN MAX(PrimaryDrillID) + 1 ELSE 1 END FROM dm.PrimaryDiagnosisTable)

INSERT INTO dm.PrimaryDiagnosisTable (
	  [PrimaryDrillID]
      ,[MedicalSystem]
      ,[Diagnosis]
      ,[DiagnosisRank]
      ,[DiagnosisDrillDownLevel]
      ,[DiagnosisNextLevel]
      ,[DiagnosisName]
      ,[ReviewSystemLingo]
      ,[Image1]
      ,[Image2]
      ,[Discipline]
      ,[Billable]
      ,[NextLevel]
      ,[ShortName]
      ,[Exclusion]
      ,[LetterTranslation]
      ,[OtherLtrTrans]
      ,[PermanentCondition]
      ,[ImpressionOn]
) 
SELECT 
@Id AS [PrimaryDrillID]
,'R' AS [MedicalSystem] -- 'Visual Defects'
,'' AS [Diagnosis]
,'' AS [DiagnosisRank]
,'' AS [DiagnosisDrillDownLevel]
,'' AS [DiagnosisNextLevel]
,'' AS [DiagnosisName]
,@Name AS [ReviewSystemLingo]
,'' AS [Image1]
,'' AS [Image2]
,'' AS [Discipline]
,'' AS [Billable]
,'' AS [NextLevel]
,'' AS [ShortName]
,'' AS [Exclusion]
,'' AS [LetterTranslation]
,'' AS [OtherLtrTrans]
,'' AS [PermanentCondition]
,'' AS [ImpressionOn]

GO

-- ClinicalCondition Update
--- For unit tests only.
UPDATE d
SET d.ReviewSystemLingo = @Name
FROM dm.PrimaryDiagnosisTable d
WHERE d.PrimaryDrillId = @Id

GO

-- ClinicalCondition Delete
--- For unit tests only.
DELETE FROM dm.PrimaryDiagnosisTable WHERE PrimaryDrillId = @Id

GO

-- ClinicalQualifierCategory Insert
--- For unit tests only.
INSERT INTO dbo.PracticeCodeTable (
    [ReferenceType]
    ,[Code]
    ,[AlternateCode]
    ,[Exclusion]
    ,[LetterTranslation]
    ,[OtherLtrTrans]
    ,[FormatMask]
    ,[Signature]
    ,[TestOrder]
    ,[FollowUp]
    ,[Rank]
    ,[WebsiteUrl]
)
SELECT 'INSERTEDQUANTIFIERS' + @Name [ReferenceType]
    ,'' AS [Code]
    ,'' AS [AlternateCode]
    ,'' AS [Exclusion]
    ,'' AS [LetterTranslation]
    ,'' AS [OtherLtrTrans]
    ,'' AS [FormatMask]
    ,'' AS [Signature]
    ,'' AS [TestOrder]
    ,'' AS [FollowUp]
    ,'' AS [Rank]
    ,'' AS [WebsiteUrl]

SET @Id = (
			SELECT
			MAX(pct.Id) AS Id
			FROM PracticeCodeTable pct
			WHERE ReferenceType LIKE 'INSERTEDQUANTIFIERS'+@Name 
			GROUP BY ReferenceType
			)

GO

-- ClinicalQualifierCategory Update
--- For unit tests only.

UPDATE PracticeCodeTable 
Set ReferenceType = ('INSERTEDQUANTIFIERS' + @Name)
WHERE Id = @Id

GO

-- ClinicalQualifier Insert
--- For unit tests only.
INSERT INTO dbo.PracticeCodeTable (
    [ReferenceType]
    ,[Code]
    ,[AlternateCode]
    ,[Exclusion]
    ,[LetterTranslation]
    ,[OtherLtrTrans]
    ,[FormatMask]
    ,[Signature]
    ,[TestOrder]
    ,[FollowUp]
    ,[Rank]
    ,[WebsiteUrl]
)
SELECT
(SELECT ReferenceType FROM PracticeCodeTable WHERE Id = @ClinicalQualifierCategoryId) AS ReferenceType
, @Name AS Code  
,'' AS AlternateCode
,'' AS Exclusion
,COALESCE(@TechnicalDescription, @Name) AS LetterTranslation
,COALESCE(@SimpleDescription, @Name) AS OtherLtrTrans
,'' AS FormatMask
,'' AS Signature
, '' AS TestOrder
, '' AS Followup
, '' AS Rank
,''AS WebsiteUrl

SET @Id =  (
			SELECT TOP 1
			pct.Id
			FROM PracticeCodeTable pct
			WHERE ReferenceType =  (SELECT ReferenceType FROM PracticeCodeTable WHERE Id = @ClinicalQualifierCategoryId)
			AND Code = @Name
			)

GO

-- ClinicalQualifier Update
--- For unit tests only.

UPDATE PracticeCodeTable 
SET ReferenceType = (SELECT ReferenceType FROM PracticeCodeTable WHERE Id = @ClinicalQualifierCategoryId),
Code = @Name
WHERE Id = @Id

GO