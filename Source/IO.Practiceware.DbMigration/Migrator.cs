﻿using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.ServiceProcess;
using System.Text;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using FluentMigrator.Runner.Processors.SqlServer;
using IO.Practiceware.DbMigration.Replication;
using IO.Practiceware.DbMigration.Properties;
using IO.Practiceware.DbSync;
using IO.Practiceware.Logging;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Reporting;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.IO;
using Soaf.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using Soaf.Reflection;
using Telerik.Reporting;
using Telerik.Reporting.XmlSerialization;
using AggregateException = System.AggregateException;
using ConfigurationManager = IO.Practiceware.Configuration.ConfigurationManager;

namespace IO.Practiceware.DbMigration
{
    /// <summary>
    ///   Responsible for initiating database migrations. Callable via API (Migrator.Run) or from the command line. Requires "commit" parameter in order to commit changes.
    /// </summary>
    public class Migrator
    {
        private static readonly DateTime DefaultLockoutPeriod = new DateTime(2014, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private static bool _allowSqlRestart;

        private static readonly string MigrationTime = DateTime.Now.ToString("yyyy.MM.dd_HH.mm.ss");

        /// <summary>
        /// States whether at least one (when updating multiple Dbs) core migration was committed
        /// </summary>
        private static bool _coreMigrationCompleted;

        private static readonly object SyncRoot = new object();

        // one log file per migration - each migration runs on its own thread
        private static readonly ConcurrentDictionary<Guid, TraceListener> ConfigurationTraceListeners = new ConcurrentDictionary<Guid, TraceListener>();
        private const string TraceListenerContextBoundKey = "__Migrator_TraceListenerKey__";

        public static bool ShowElapsedTime { get; set; }

        static Migrator()
        {
#if DEBUG
            ShowElapsedTime = true;
#endif
        }

        internal static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Running database migrator...\r\n");
                if (args.Any()) _allowSqlRestart = args.Contains("ALLOWSQLRESTART", StringComparer.OrdinalIgnoreCase);
                Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Migration(s) failed: {0}", ex);
                Environment.ExitCode = _coreMigrationCompleted
                    ? 10000 // Partially succeeded 
                    : -1; // General failure
            }
            Trace.Flush();
            Thread.Sleep(5000);
        }

        /// <summary>
        ///   Runs the specified commit.
        /// </summary>
        public static void Run(IEnumerable<DbMigrationConfiguration> configurations = null)
        {
            // Ensure configuration is loaded
            if (!ConfigurationManager.IsLoaded)
                ConfigurationManager.Load();

            // put logs folder under the directory where the config file is
            string logsDirectory = Path.Combine(Path.GetDirectoryName(ConfigurationManager.Configuration != null ? ConfigurationManager.Configuration.FilePath : string.Empty) ?? string.Empty, "~Logs");

            if (configurations == null)
            {
                configurations = ConfigurationManager.GetSectionWithRetry<DbMigrationsConfiguration>().Migrations;
            }
            
            TraceListener[] listeners = Trace.Listeners.OfType<TraceListener>().ToArray();

            try
            {
                if (!Directory.Exists(logsDirectory))
                {
                    Directory.CreateDirectory(logsDirectory);
                }

                // Clear flag before running migration
                _coreMigrationCompleted = false;

                if (configurations == null)
                {
                    configurations = ConfigurationManager.GetSectionWithRetry<DbMigrationsConfiguration>().Migrations;
                }

                Trace.Listeners.Clear();

                Trace.AutoFlush = true;

                Trace.Listeners.Add(new DelegatingTraceListener(() =>
                {
                    var traceListenerKey = ContextData.Get(TraceListenerContextBoundKey);
                    return traceListenerKey is Guid 
                        ? ConfigurationTraceListeners.GetValue((Guid) traceListenerKey) 
                        : null;
                }));

                // allow trace listeners that go to a file to listen to the migration, but not others
                Trace.Listeners.AddRange(listeners.OfType<TextWriterTraceListener>());

                var exceptions = new List<Exception>();
                configurations.AsParallel().WithDegreeOfParallelism(8).ForAll(configuration =>
                {
                    try
                    {
                        // Store configuration specific trace listener
                        var traceListenerKey = Guid.NewGuid();
                        var traceListener = new TextWriterTraceListener("{0}\\migration_{1}_{2}.log".FormatWith(logsDirectory, MigrationTime, new SqlConnectionStringBuilder(configuration.MigrationConnectionStrings[0]).InitialCatalog))
                        {
                            TraceOutputOptions = TraceOptions.DateTime | TraceOptions.ProcessId | TraceOptions.ThreadId
                        };
                        ConfigurationTraceListeners[traceListenerKey] = traceListener;

                        using (DisposableScope.Create(TraceListenerContextBoundKey,
                            k => ContextData.Set(k, traceListenerKey),
                            k => ContextData.Remove(k)))
                        {

                            Run(configuration);
                        }
                    }
                    catch (Exception ex)
                    {
                        exceptions.Add(new Exception("Error migrating {0}".FormatWith(configuration.MigrationConnectionStrings.FirstOrDefault()), ex));
                    }
                });

                if (exceptions.Count > 0)
                    throw new AggregateException(exceptions);

                File.WriteAllText("{0}\\migration_{1}.log".FormatWith(logsDirectory, MigrationTime), "Migration(s) succeeded:\r\n{0}"
                    .FormatWith(configurations.Select(c => c.MigrationConnectionStrings.FirstOrDefault()).Join(Environment.NewLine)));
            }
            catch (Exception ex)
            {
                File.WriteAllText("{0}\\migration_{1}.log".FormatWith(logsDirectory, MigrationTime), "Migration(s) failed:\r\n{0}".FormatWith(ex.ToString()));
                throw;
            }
            finally
            {
                Trace.Flush();
                Trace.Listeners.Clear();
                Trace.Listeners.AddRange(listeners);
            }
        }

        /// <summary>
        /// Runs the migrator. First runs numbers Migrations, then the Model Views script.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        private static void Run(DbMigrationConfiguration configuration)
        {
            var announcer = new TextWriterAnnouncer(s =>
            {
                Trace.TraceInformation(s);
#if DEBUG
                if (s.Contains(@": True"))
                {
                    var lines = s.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    foreach (var line in lines)
                    {
                        if (line.Contains(@": True"))
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine(configuration.GetFirstConnectionStringDisplayValue() + " - " + line);
                            Console.ResetColor();
                        }
                        else
                            Console.WriteLine(configuration.GetFirstConnectionStringDisplayValue() + " - " + line);
                    }
                }
                else
                    Console.WriteLine(configuration.GetFirstConnectionStringDisplayValue() + " - " + s);
#endif
            });

            try
            {
                announcer.ShowElapsedTime = ShowElapsedTime;

                var consoleMessage = "Migrating databases:\r\n{0}".FormatWith(configuration.GetConnectionStringsDisplayValue());

                Console.WriteLine(consoleMessage);
                announcer.Say("Migrating database...\r\n{0}".FormatWith(configuration));

                // ReSharper disable AccessToDisposedClosure
                using (new TimedScope(s => announcer.Say("Ran migration for {0} in {1}.".FormatWith(configuration.GetFirstConnectionStringDisplayValue(), s))))
                // ReSharper restore AccessToDisposedClosure
                {
                    RunMigrationComponents(configuration, announcer);
                }

                var succeededMessage = "Migration succeeded for {0}.".FormatWith(configuration.GetFirstConnectionStringDisplayValue());
                Console.WriteLine(succeededMessage);
                announcer.Say(succeededMessage);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Migration failed for {0}. Check log for details.".FormatWith(configuration.GetFirstConnectionStringDisplayValue()));
                announcer.Error("Migration Failed:\r\n{0}".FormatWith(ex));

                throw;
            }

        }

        /// <summary>
        /// Wrapper method for different migration components.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="announcer">The announcer.</param>
        private static void RunMigrationComponents(DbMigrationConfiguration configuration, IAnnouncer announcer)
        {
            SqlReplicationConfiguration replicationConfiguration;
            lock (SyncRoot)
            {
                // drop replication before doing anything, since pre-requisites may perform some prohibited action due to replicating articles
                replicationConfiguration = GetReplicationConfigurationAndDropReplication(configuration, announcer);
            }
            try
            {
                //check for sql clr enabled status before ConfigurePrerequisites
                var isSqlClrEnabledBefore = IsSqlClrEnabled(configuration);

                ConfigurePrerequisites(configuration);

                //check for sql clr enabled status again after ConfigurePrerequisites
                var isSqlClrEnabledAfter = IsSqlClrEnabled(configuration);

                //Restarting sql service after enabling clr
                if (!isSqlClrEnabledBefore && isSqlClrEnabledAfter && _allowSqlRestart)
                {
                    lock (SyncRoot)
                    {
                        RestartSqlService(configuration);
                    }
                }

                RunCoreMigration(announcer, configuration);

            }
            finally
            {
                CleanUp(configuration);
            }
            if (replicationConfiguration.DistributorPreviouslyExistedAndIsValid == false)
            {
                lock (SyncRoot)
                {
                    ConfigureReplication(configuration, replicationConfiguration);
                }
            }
        }

        private static void CleanUp(DbMigrationConfiguration configuration)
        {
            foreach (string connectionString in configuration.MigrationConnectionStrings)
            {
                new SqlConnection(connectionString).Execute(Resources.CleanUp);
            }
        }

        private static bool IsSqlClrEnabled(DbMigrationConfiguration configuration)
        {
            bool isClrEnabled = false;
            foreach (string connectionString in configuration.MigrationConnectionStrings)
            {
                var builder = new SqlConnectionStringBuilder(connectionString);

                if (builder.DataSource.Split('\\')[0].Trim().Length > 15)
                {
                    throw new Exception("Server name must be less than 16 characters.");
                }
                var databaseName = builder.InitialCatalog;
                builder.InitialCatalog = "master";
                using (var connection = new SqlConnection(builder.ConnectionString))
                {
                    EnsureDbExists(databaseName, connection);

                    connection.ChangeDatabase(databaseName);

                    if (!isClrEnabled) isClrEnabled = CheckSqlClrConfiguration(connection);
                }
            }
            return isClrEnabled;
        }

        private static bool CheckSqlClrConfiguration(SqlConnection connection)
        {
            var clrEnabledStatus = connection.Execute<bool>(
                @"SELECT value FROM sys.configurations WHERE name ='clr enabled'");

            return clrEnabledStatus;
        }

        private static void RestartSqlService(DbMigrationConfiguration configuration)
        {
            foreach (string connectionString in configuration.MigrationConnectionStrings)
            {
                if (string.IsNullOrEmpty(connectionString))
                {
                    var exception = new Exception("Connection string not present.");
                    throw exception;
                }
                var builder = new SqlConnectionStringBuilder(connectionString);

                var nameParts = builder.DataSource.Split(new[] { "\\" }, StringSplitOptions.None);
                var remoteComputerName = nameParts[0];

                if (remoteComputerName.Length > 15)
                {
                    throw new Exception("Server name must be less than 16 characters");
                }

                if (string.IsNullOrEmpty(remoteComputerName))
                {
                    var exception = new Exception("Invalid Remote Computer.");
                    throw exception;
                }

                var serviceName = nameParts.Length == 1 ? "MSSQLSERVER" : "MSSQL$" + nameParts[1];

                var sc = new ServiceController(serviceName, remoteComputerName);
                if (sc.Status == ServiceControllerStatus.Running || sc.Status == ServiceControllerStatus.Paused)
                {
                    sc.DependentServices.Where(d => d.Status == ServiceControllerStatus.Running || d.Status == ServiceControllerStatus.Paused)
                        .ForEachWithSinglePropertyChangedNotification(d =>
                            {
                                d.Stop();
                                d.WaitForCompleteOperation();
                            });
                    sc.Stop();
                }
                sc.WaitForCompleteOperation();

                while (sc.Status != ServiceControllerStatus.Stopped)
                {
                    Thread.Sleep(5000);
                    sc.WaitForStatus(ServiceControllerStatus.Stopped);
                    sc.Refresh();
                }
                // Starting the dependencies like Sql Server Agent[MSSQLSERVER]. This will start Sql Server[MSSQLSERVER] automatically
                sc.DependentServices.Where(d => d.Status == ServiceControllerStatus.Stopped || d.Status == ServiceControllerStatus.Paused)
                    .ForEachWithSinglePropertyChangedNotification(d =>
                        {
                            d.Start();
                            d.WaitForCompleteOperation();
                        });
                sc.Refresh();
                // Check the status and try to start Sql Server[MSSQLSERVER] again
                if (sc.Status == ServiceControllerStatus.Stopped || sc.Status == ServiceControllerStatus.Paused)
                {
                    sc.Start();
                    sc.WaitForCompleteOperation();
                }
                sc.Refresh();

                while (sc.Status != ServiceControllerStatus.Running)
                {
                    Thread.Sleep(5000);
                    sc.WaitForStatus(ServiceControllerStatus.Running);
                    sc.Refresh();
                }
                //After restarting the sql service waiting time to refresh all the services like for e.g create Databases, logins for the users
                if (sc.Status == ServiceControllerStatus.Running)
                {
                    // check the server ready status 
                    string dbConnectionString = connectionString;
                    bool isDataBaseReady = false;
                    RetryUtility.ExecuteWithRetry(() => isDataBaseReady = ConnectToSqlServer(dbConnectionString), 5, TimeSpan.FromMilliseconds(500),
                        // only retry for 5 times
                        ex => ex.SearchFor<SqlException>().IfNotNull(ioEx => ioEx.Message.Any()));
                    if (isDataBaseReady)
                    {
                        const string succeededMessage = "Restarted Sql service.";
                        Trace.TraceInformation(succeededMessage);
                    }
                }
                sc.Close();
            }
        }

        private static bool ConnectToSqlServer(string connectionString)
        {
            try
            {
                var builder = new SqlConnectionStringBuilder(connectionString);

                using (var connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    return true;
                }
            }
            catch (SqlException ex)
            {
                switch (ex.Number)
                {
                    case 4060: // Invalid Database 
                        return ReconnectToSqlServer(connectionString, ex);

                    case 18456: // Login Failed 
                        return ReconnectToSqlServer(connectionString, ex);
                    default:
                        throw;
                }
            }
        }

        private static bool ReconnectToSqlServer(string connectionString, SqlException ex)
        {
            Trace.TraceInformation(ex.Message);
            Thread.Sleep(5000);
            return ConnectToSqlServer(connectionString);
        }

        /// <summary>
        /// Configures SQL replication after the main component of the migration has been run.
        /// </summary>
        /// <param name="dbMigrationConfiguration">The configuration.</param>
        /// <param name="sqlReplicationConfiguration">The replication configuration.</param>
        private static void ConfigureReplication(DbMigrationConfiguration dbMigrationConfiguration, SqlReplicationConfiguration sqlReplicationConfiguration)
        {
            // check to make sure we are not in preview mode only. also check to see if replication already existed (and therefore need to re-configure)
            // or if there is more than one migration connection string (then we assume we need to setup a new replication environment). if
            // these are not the case then we don't want to configure replication. just return.
            if (dbMigrationConfiguration.MigrationConnectionStrings.Count <= 1)
            {
                return;
            }

            using (new TimedScope(v => Trace.TraceInformation("Ran replication configuration on {0} in {1}.".FormatWith(dbMigrationConfiguration.MigrationConnectionStrings.First(), v))))
            {
                InitializeObjectsToReplicate(sqlReplicationConfiguration);

                if (sqlReplicationConfiguration.ReplicationPreviouslyExisted)
                {
                    Console.Write("Reconfiguring replication for {0}...".FormatWith(dbMigrationConfiguration.GetFirstConnectionStringDisplayValue()));
                }
                else if (dbMigrationConfiguration.MigrationConnectionStrings.Count > 1)
                {
                    // replication did not previously exist but there are more than one connection 
                    // string specified in the config file, so we assume replication needs to be setup
                    Console.Write("Configuring replication for {0}...".FormatWith(dbMigrationConfiguration.GetFirstConnectionStringDisplayValue()));
                }

                sqlReplicationConfiguration.SetReplicationManager(new SqlReplicationManager());

                // did we use sql server snapshot to sync the db's ? If not then run the manual sync utility

                sqlReplicationConfiguration.SeedMetadata = sqlReplicationConfiguration.ReplicationManager.LoadSeedMetadata(sqlReplicationConfiguration);

                if (sqlReplicationConfiguration.UseDbSyncForReplication)
                {
                    DbSyncBootstrapper.Initialize();
                    new NoSnapshotWithDbSyncReplicationManager(sqlReplicationConfiguration).RunDbSyncUtilityToSynchronizeDatabases();
                }
                else
                {
                    using (var connection = new SqlConnection(sqlReplicationConfiguration.HubConnectionString))
                    {
                        connection.RunScript(Resources.AuditTablesMoveFromPrimary, false, createCommand: () => new SqlCommand { CommandTimeout = 0 });
                    }
                }

                sqlReplicationConfiguration.ConfigureReplication();

                SetupSubscriptionReinitialization(sqlReplicationConfiguration);

                Console.WriteLine("Done configuring replication for {0}.".FormatWith(dbMigrationConfiguration.GetFirstConnectionStringDisplayValue()));
            }
        }

        private static void InitializeObjectsToReplicate(SqlReplicationConfiguration sqlReplicationConfiguration)
        {
            // try to get all the objects from the main server that are not part of the 'dbo' schema and only include object types of 'U','P','V','FN','TF'
            var fromDatabaseReplicatedObjectsForNonDboSchemas = new List<string>();
            using (var hubConnection = new SqlConnection(sqlReplicationConfiguration.HubConnectionString))
            {
                const string query = "SELECT DISTINCT('['+s.name + '].[' + o.name+']') AS name FROM sys.objects o JOIN sys.schemas s ON o.schema_id = s.schema_id WHERE s.name <> 'dbo' AND type IN ('U','P','V','FN','TF') AND o.name NOT LIKE '%conflict_%'";
                var databaseNonDboSystemObjects = DbConnections.Execute<DataTable>(hubConnection, query);
                fromDatabaseReplicatedObjectsForNonDboSchemas.AddRange(from DataRow row in databaseNonDboSystemObjects.Rows select row.Get<string>("name"));
            }

            // set the default value of all objects which should be replicated by combining the list from the database and the list from the txt file.
            sqlReplicationConfiguration.ObjectsToReplicate = Resources.DefaultReplicatedObjects.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).Union(fromDatabaseReplicatedObjectsForNonDboSchemas, StringComparer.OrdinalIgnoreCase).ToList();
        }

        /// <summary>
        /// Copies over the values from the <see cref="DbMigrationConfiguration"/> to the <see cref="SqlReplicationConfiguration"/>
        /// </summary>
        /// <param name="dbMigrationConfiguration"></param>
        /// <param name="sqlReplicationArguments"></param>
        internal static void MapMigrationConfigToSqlReplicationArguments(DbMigrationConfiguration dbMigrationConfiguration, SqlReplicationConfiguration sqlReplicationArguments)
        {
            if (dbMigrationConfiguration != null && sqlReplicationArguments != null)
            {
                sqlReplicationArguments.SpokeConnectionStrings = dbMigrationConfiguration.MigrationConnectionStrings.Where(cs => !cs.Equals(sqlReplicationArguments.HubConnectionString, StringComparison.OrdinalIgnoreCase)).ToArray();
                sqlReplicationArguments.UseSnapshotForReplication = dbMigrationConfiguration.UseSnapshotForReplication;
                sqlReplicationArguments.UseDbSyncForReplication = dbMigrationConfiguration.UseDbSyncForReplication;
                sqlReplicationArguments.ExecuteDbSyncScriptAutomatically = dbMigrationConfiguration.ExecuteDbSyncScriptAutomatically;
                sqlReplicationArguments.DbSyncGeneratedScriptPath = dbMigrationConfiguration.DbSyncGeneratedScriptPath;
            }
        }

        /// <summary>
        /// Runs the core part of the DB migration.
        /// </summary>
        /// <param name="announcer">The announcer.</param>
        /// <param name="configuration">The configuration.</param>
        private static void RunCoreMigration(IAnnouncer announcer, DbMigrationConfiguration configuration)
        {
            var runnerContext = new RunnerContext(announcer)
            {
                Connection = configuration.MigrationConnectionStrings.First(),
                Timeout = 0
            };
            var processor = (SqlServerProcessor)new SqlServer2005ProcessorFactory().Create(runnerContext.Connection, announcer, new ProcessorOptions { Timeout = 0 });

            using (processor.Connection)
            using (processor.Transaction)
            {
                try
                {
                    // Read lockout period value or use default
                    var configurationLockoutPeriod = string.IsNullOrWhiteSpace(configuration.MigrationLockoutPeriod)
                        ? DefaultLockoutPeriod
                        : DateTime.ParseExact(configuration.MigrationLockoutPeriod, "u", CultureInfo.InvariantCulture);

                    var runner = new MigrationRunner(Assembly.GetExecutingAssembly(), runnerContext, processor);
                    var versionLoader = new VersionLoader(runner, Assembly.GetExecutingAssembly(), runner.Conventions, configurationLockoutPeriod);
                    runner.VersionLoader = versionLoader;
                    var migrationLoader = new MigrationLoader(runner.Conventions, runner.MigrationAssembly, runnerContext.Namespace, runnerContext.NestedNamespaces, runnerContext.Tags);
                    runner.MigrationLoader = migrationLoader;

                    migrationLoader.Migrations.Add(ConstantMigrations.VersionInfo);

                    AddSqlMigrations(runner);

                    Console.WriteLine("Updating database schema and data for {0}...".FormatWith(configuration.GetFirstConnectionStringDisplayValue()));

                    if (!configuration.ExcludeModelViews)
                    {
                        migrationLoader.Migrations.Add(ConstantMigrations.ModelViews);
                    }

                    if (!configuration.SkipAuditing)
                        migrationLoader.Migrations.Add(ConstantMigrations.Audit);

                    new[] { ConstantMigrations.MeaningfulUse, ConstantMigrations.Reports, ConstantMigrations.LegacyReports }.ForEachWithSinglePropertyChangedNotification(m => migrationLoader.Migrations.Add(m));

                    versionLoader.DeleteRowsWithDifferentChecksums();

                    // some migrations/reports depend on enum tables so create them first
                    CreateEnumTables(processor);

                    var migrationsToApply = migrationLoader.Migrations.Where(m => !runner.VersionLoader.VersionInfo.HasAppliedMigration(m.Version)).ToArray();
                    bool hasMigrationsToApply = migrationsToApply.Any();

                    using (new TimedScope(s => Trace.TraceInformation("Applied new migrations in {0}.".FormatWith(s))))
                        runner.MigrateUp(false);

                    if (hasMigrationsToApply)
                        RunScript(processor.Connection, processor.Transaction, "trigger replication configuration", Resources.AlterTriggersCheckForSyncAndReplication);

                    MigrateDocuments(processor);

                    MigrateReports(processor);

                    SynchronizePermissions(processor);

                    processor.CommitTransaction();

                    _coreMigrationCompleted = true;

                    Console.WriteLine("Done running transactional migration for {0}.".FormatWith(configuration.GetFirstConnectionStringDisplayValue()));
                }
                catch (Exception ex)
                {
                    runnerContext.Announcer.Error(new Exception("An error occurred.", ex).ToString());
                    try
                    {
                        processor.RollbackTransaction();
                    }
                    catch (Exception rollbackEx)
                    {
                        runnerContext.Announcer.Error(new Exception("Could not roll back transaction.", rollbackEx).ToString());
                    }
                    throw;
                }
            }
        }

        /// <summary>
        /// Makes sure the model.Permissions table is correct and up to date.
        /// </summary>
        /// <param name="processor">The processor.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private static void SynchronizePermissions(SqlServerProcessor processor)
        {
            var sql = @"
IF NOT EXISTS(SELECT * FROM model.Permissions WHERE Id = {0})
    BEGIN 
    SET IDENTITY_INSERT model.Permissions ON

    INSERT model.Permissions(Id, Name, PermissionCategoryId)
    VALUES({0}, @Name{1}, {2})
    
    SET IDENTITY_INSERT model.Permissions OFF
    END
ELSE
    UPDATE model.Permissions 
    SET Name = @Name{1}, 
        PermissionCategoryId = {2} 
    WHERE Id = {0}
";

            var sb = new StringBuilder();

            var parameters = new Dictionary<string, object>();

            foreach (var value in Enums.GetValues<PermissionId>())
            {
                var categoryId = value.GetAttribute<DisplayAttribute>().IfNotDefault(a => a.GroupName.IfNotDefault(g => ((int)g.ToEnum<PermissionCategory>()).ToString()));
                sb.AppendFormat(sql, (int)value, parameters.Count, categoryId.IsNullOrEmpty() ? "NULL" : categoryId);
                sb.AppendLine();

                parameters["Name" + parameters.Count] = value.GetDisplayName();
            }

            processor.Connection.Execute(sb.ToString(), processor.Transaction, parameters, false, 0);
        }

        /// <summary>
        /// Migrates all ReportingRepository and trdx reports into the database.
        /// </summary>
        /// <param name="processor"></param>
        /// <exception cref="System.NotImplementedException"></exception>
        private static void MigrateReports(SqlServerProcessor processor)
        {
            string sql = @"
IF NOT EXISTS (SELECT * FROM model.Permissions WHERE Name = @Name)
    INSERT INTO model.Permissions(Name, PermissionCategoryId) VALUES (@Name, 2 /* Reports */)

IF EXISTS(SELECT * FROM model.Reports WHERE Name = @Name)
    UPDATE model.Reports
    SET ReportTypeId = @ReportType, 
        Content = @Content,
        Documentation = @Documentation,
        ReportCategoryId = @ReportCategory
    WHERE Name = @Name
ELSE
    INSERT model.Reports(Name, ReportCategoryId, ReportTypeId, PermissionId, Content, Documentation)
	SELECT TOP 1 
        @Name,
	    @ReportCategory, 
	    @ReportType AS ReportType,
	    (SELECT TOP 1 Id FROM model.Permissions WHERE Name = @Name) AS PermissionId,
	    @Content AS Content,
	    @Documentation AS Documentation
";

            foreach (var document in GetDocumentBytes(".trdx"))
            {
                var fileNameWithNoExtension = Path.GetFileNameWithoutExtension(document.Item1);

                var serializer = new ReportXmlSerializer();
                using (var ms = new MemoryStream(document.Item2))
                using (var report = (Telerik.Reporting.Report)(serializer.Deserialize(ms)))
                {
                    var reportXml = Encoding.UTF8.GetString(document.Item2);

                    var documentation = report.Items.Recurse(i => i.Items).Where(i => i.Name == "Documentation").OfType<HtmlTextBox>().Select(t => t.Value).FirstOrDefault() ?? string.Empty;
                    var reportCategoryName = report.Items.Recurse(i => i.Items).Where(i => i.Name == "ReportCategory").OfType<HtmlTextBox>().Select(t => t.Value).FirstOrDefault() ?? string.Empty;
                    var reportCategoryId = GetReportCategoryIdFromName(reportCategoryName);

                    var parameters = new Dictionary<string, object>
                                     {
                                         {"Name", fileNameWithNoExtension},
                                         {"Content", reportXml},
                                         {"Documentation", documentation},
                                         {"ReportType", (int) ReportType.Trdx},
                                         {"ReportCategory", reportCategoryId}
                                     };

                    processor.Connection.Execute(sql, processor.Transaction, parameters, false, 0);
                }
            }

            foreach (var property in typeof(IReportingRepository)
                .GetProperties()
                .Where(p => p.PropertyType.EqualsGenericTypeFor(typeof(IQueryable<>))
                            && p.HasAttribute<DisplayAttribute>()))
            {
                var attribute = property.GetAttribute<DisplayAttribute>();
                var reportCategoryId = GetReportCategoryIdFromName(attribute.GroupName);

                var parameters = new Dictionary<string, object>
                                 {
                                     {"Name", property.GetDisplayName()},
                                     {"Content", DBNull.Value},
                                     {"Documentation", attribute.Description ?? ""},
                                     {"ReportType", (int) ReportType.ReportingRepository},
                                     {"ReportCategory", reportCategoryId}
                                 };

                processor.Connection.Execute(sql, processor.Transaction, parameters, false, 0);
            }
        }

        /// <summary>
        /// Gets the existing replication configuration and drops replication if a configuration exists.
        /// </summary>
        /// <param name="dbMigrationConfiguration">The configuration.</param>
        /// <param name="announcer">The announcer.</param>
        /// <returns>A <see cref="SqlReplicationConfiguration"/> object with the existing replication values</returns>
        private static SqlReplicationConfiguration GetReplicationConfigurationAndDropReplication(DbMigrationConfiguration dbMigrationConfiguration, IAnnouncer announcer)
        {
            var sqlReplicationConfiguration = new SqlReplicationConfiguration();

            try
            {
                // get the list of replicated objects specified in the resources text file.
                sqlReplicationConfiguration.Seeding = GetSeeding();
                sqlReplicationConfiguration.HubConnectionString = dbMigrationConfiguration.MigrationConnectionStrings.First();
                MapMigrationConfigToSqlReplicationArguments(dbMigrationConfiguration, sqlReplicationConfiguration);

                sqlReplicationConfiguration.SetReplicationManager(new SqlReplicationManager());
            }
            catch (Exception ex)
            {
                announcer.Error(ex.ToString());
            }

            
            if (sqlReplicationConfiguration.ReplicationPreviouslyExisted && dbMigrationConfiguration.MigrationConnectionStrings.Count > 1)
            {
                Console.WriteLine("Deleting existing replication configuration for {0}...".FormatWith(dbMigrationConfiguration.GetFirstConnectionStringDisplayValue()));
                sqlReplicationConfiguration.ReplicationManager.Delete(sqlReplicationConfiguration, true);
            }

            /////Remove Subscription and Articles from Existing Replication ///////

////////////            if (sqlReplicationConfiguration.ReplicationPreviouslyExisted && dbMigrationConfiguration.MigrationConnectionStrings.Count > 1)
////////////            {
////////////                Console.WriteLine("Removing articles from existing replication for {0}...".FormatWith(dbMigrationConfiguration.GetFirstConnectionStringDisplayValue()));
////////////                var builder = new SqlConnectionStringBuilder(dbMigrationConfiguration.MigrationConnectionStrings[0]);
////////////                var newDb = builder.InitialCatalog;
////////////                var theConnection = new SqlConnection(builder.ToString());
////////////                theConnection.Execute(@"Declare @publication varchar(500)
////////////                                        SET @publication = 'PracticeRepository';
////////////                                        Declare @subscriber varchar(500)
////////////                                        declare Sub_Cursor Cursor for
////////////                                        Select Name from sys.servers where is_subscriber = 1
////////////                                        Open Sub_Cursor
////////////                                        fetch next from Sub_Cursor into @subscriber
////////////                                        while @@FETCH_STATUS=0
////////////                                        begin
////////////                                            EXEC sp_dropsubscription 
////////////                                            @publication = @publication, 
////////////                                            @article = N'all',
////////////                                            @subscriber = @subscriber;
////////////                                            fetch next from Sub_Cursor into @subscriber
////////////                                        End
////////////                                        close Sub_Cursor
////////////                                        deallocate Sub_Cursor");

////////////                theConnection.Execute(@"declare @TableName varchar(500)
////////////                                        declare @Type varchar(50)
////////////                                        declare @publication as sysname
////////////
////////////                                        Declare Rep_Cursor Cursor for
////////////                                        SELECT 
////////////                                          msa.article AS ArticleName,
////////////                                          obj.Type
////////////                                        FROM distribution.dbo.MSarticles msa
////////////                                        Inner JOIN distribution.dbo.MSpublications msp ON msa.publication_id = msp.publication_id
////////////                                        Inner Join sys.objects obj on obj.name = msa.source_object 
////////////                                        where msp.publication='PracticeRepository' and (name like '%AuditEnt%' or name like '%Enumeration%' or type <> 'U')
////////////                                        open Rep_Cursor
////////////                                        fetch next from Rep_Cursor into @TableName,@Type
////////////                                        while @@FETCH_STATUS=0
////////////                                        Begin
////////////                                          EXEC sp_droparticle 
////////////                                          @publication = 'PracticeRepository', 
////////////                                          @article = @TableName,
////////////                                          @force_invalidate_snapshot = 1;
////////////                                          Print @TableName +' Successfully remomoved from Publication: PracticeRepository'
////////////                                          fetch next from Rep_Cursor into @TableName,@Type
////////////
////////////                                        End
////////////                                        Close Rep_Cursor
////////////                                        Deallocate Rep_Cursor");
////////////                Console.WriteLine("Articles removed from existing replication for {0}...".FormatWith(dbMigrationConfiguration.GetFirstConnectionStringDisplayValue()));

////////////            }
            
            // map again since values were overwritten
            MapMigrationConfigToSqlReplicationArguments(dbMigrationConfiguration, sqlReplicationConfiguration);

            InitializeObjectsToReplicate(sqlReplicationConfiguration);

            if (sqlReplicationConfiguration.UseSnapshotForReplication && sqlReplicationConfiguration.SpokeConnectionStrings != null)
            {
                // must be done prior to configure pre-requisites - that's why it's not directly in SqlReplicationManager
                foreach (var cs in sqlReplicationConfiguration.SpokeConnectionStrings)
                {
                    DropAndRecreateDb(cs);
                }
            }

            return sqlReplicationConfiguration;
        }

        private static void DropAndRecreateDb(string cs)
        {
            var builder = new SqlConnectionStringBuilder(cs);
            var newDb = builder.InitialCatalog;
            builder.InitialCatalog = "master";
            using (var connection = new SqlConnection(builder.ToString()))
            {
                connection.Execute(@"
                    IF EXISTS(SELECT * FROM sys.databases WHERE name = '{0}' OR '[' + name + ']' = '{0}') 
                        EXEC('
                                ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
                                DROP DATABASE [{0}]
                        ') 
                    CREATE DATABASE [{0}]
                    ".FormatWith(newDb));

            }

        }

        /// <summary>
        /// Performs any prerequisites steps required before running the actual migrations.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        private static void ConfigurePrerequisites(DbMigrationConfiguration configuration)
        {
            lock (SyncRoot)
            {
                MsDtcConfigurator.Configure(configuration.MigrationConnectionStrings);

                DbPermissionConfigurator.Configure(configuration.MigrationConnectionStrings);
            }

            foreach (string connectionString in configuration.MigrationConnectionStrings)
            {
                var builder = new SqlConnectionStringBuilder(connectionString);

                if (builder.DataSource.Split('\\')[0].Trim().Length > 15)
                {
                    throw new Exception("Server name must be less than 16 characters.");
                }

                var databaseName = builder.InitialCatalog;
                builder.InitialCatalog = "master";
                using (var connection = new SqlConnection(builder.ConnectionString))
                {
                    EnsureDbExists(databaseName, connection);

                    connection.ChangeDatabase(databaseName);

                    // create any schema pre-requisites for the migration
                    connection.RunScript(Resources.CreateSchemaPrerequisites, false);

                    try
                    {
                        lock (SyncRoot)
                        {
                            // configure server/db level options
                            connection.RunScript(Resources.Configure, false);
                        }
                    }
                    catch (Exception ex)
                    {
                        // may not be supported because of SQL security...we consider this an "acceptable" error
                        Trace.TraceWarning(ex.ToString());
                    }

                    Trace.TraceInformation("Running AuditTablesMoveFromPrimary.");
                    connection.RunScript(Resources.AuditTablesMoveFromPrimary, false, createCommand: () => new SqlCommand { CommandTimeout = 0 });

                    lock (SyncRoot)
                    {
                        Trace.TraceInformation("Configuring ShrinkLogRebuildIndexes.");
                        connection.RunScript(Resources.ShrinkLogRebuildIndexes, false, createCommand: () => new SqlCommand { CommandTimeout = 0 });
                    }

                    AddLinkedExternalData(connectionString);

                    connection.ChangeDatabase(databaseName);
                    InstallSqlClrAssemblies(connection);
                }
            }
        }

        /// <summary>
        /// Installs all SQL CLR assemblies on the connection specified.
        /// </summary>
        /// <param name="connection"></param>
        private static void InstallSqlClrAssemblies(SqlConnection connection)
        {
            foreach (var asm in new[] { typeof(SqlClr.Common).Assembly }.WhereNotDefault())
            {
                SqlClrInstaller.Install(asm, connection);
            }
        }

        /// <summary>
        /// Ensures the db exists. Creates it if it does not.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="connection">The connection.</param>
        private static void EnsureDbExists(string databaseName, SqlConnection connection)
        {
            var dbExists = DbConnections.Execute<bool>(connection, @"SELECT CASE WHEN EXISTS(SELECT * FROM sys.databases WHERE name = '{0}' OR '[' + name + ']' = '{0}') THEN CAST(1 AS bit) ELSE CAST(0 as bit) END".FormatWith(databaseName));

            if (!dbExists)
            {
                DbConnections.Execute(connection, @"CREATE DATABASE [{0}]".FormatWith(databaseName));
            }
        }

        /// <summary>
        /// Adds a linked server and wrapper views to the cloud based external data server.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <exception cref="System.ApplicationException">An error occurred trying to add linked external data for connection {0}..FormatWith(connection.ConnectionString)</exception>
        private static void AddLinkedExternalData(string connectionString)
        {
            try
            {
                using (var externalDataConnection = new SqlConnection(ConfigurationManager.Configuration.ConnectionStrings.ConnectionStrings["ExternalData"].EnsureNotDefault("ExternalData connection string is not present.").ConnectionString))
                {
                    lock (SyncRoot)
                    {
                        var addExternalDataLinkedServer = Resources.AddExternalDataLinkedServer;

                        // Provide connection parameters to external data server
                        var externalDataConnectionStringBuilder = new SqlConnectionStringBuilder(externalDataConnection.ConnectionString);
                        addExternalDataLinkedServer = addExternalDataLinkedServer.Replace("{DataSource}", externalDataConnectionStringBuilder.DataSource);
                        addExternalDataLinkedServer = addExternalDataLinkedServer.Replace("{Catalog}", externalDataConnectionStringBuilder.InitialCatalog);
                        addExternalDataLinkedServer = addExternalDataLinkedServer.Replace("{UserID}", externalDataConnectionStringBuilder.UserID);
                        addExternalDataLinkedServer = addExternalDataLinkedServer.Replace("{Password}", externalDataConnectionStringBuilder.Password);

                        // add linked server
                        new SqlConnection(connectionString).RunScriptAndDispose(addExternalDataLinkedServer, false);
                    }

                    // get the sql to create all the schemas and views from the external data DB
                    var objectCreationSql = DbConnections.Execute<string>(externalDataConnection, Resources.ExternalDataObjectCreationSql);

                    var parts = objectCreationSql
                        .Split(new[] { Environment.NewLine + "GO" + Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                    using (var c = new SqlConnection(connectionString))
                    {
                        RetryUtility.ExecuteWithRetry(() =>
                        {
                            c.Close();
                            c.Open();
                            // includes schema creation statements
                            DbConnections.Execute(c, parts[0]);
                        }, 10, TimeSpan.FromSeconds(10));
                    }
                    parts.Skip(1).AsParallel().ForAll(s =>
                                             {
                                                 using (var c = new SqlConnection(connectionString))
                                                 {
                                                     RetryUtility.ExecuteWithRetry(() =>
                                                     {
                                                         c.Close();
                                                         c.Open();
                                                         DbConnections.Execute(c, s);
                                                     }, 10, TimeSpan.FromSeconds(10));
                                                 }
                                             });

                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("An error occurred trying to add linked external data for connection {0}.".FormatWith(connectionString), ex);
            }
        }

        private static void MigrateDocuments(SqlServerProcessor processor)
        {
            const string rowExists = @"SELECT 1 from model.TemplateDocuments WHERE Name = @Name";
            const string updateDocuments = @"
UPDATE model.TemplateDocuments 
SET ContentOriginal = @ContentOriginal,
    ContentTypeId=@ContentTypeId, 
    IsEditable=@IsEditable 
WHERE Name= @Name";
            const string insertDocuments = @"
INSERT INTO model.TemplateDocuments (TemplateDocumentCategoryId, Name, DisplayName, ContentOriginal, ContentTypeId, IsEditable)
VALUES (@TemplateDocumentCategoryId, @Name, @DisplayName, @ContentOriginal, @ContentTypeId, @IsEditable)";

            foreach (var document in GetDocumentBytes())
            {
                var ext = Path.GetExtension(document.Item1);
                if (ext == null)
                    continue;
                var fileNameWithNoExtension = Path.GetFileNameWithoutExtension(document.Item1);

                // try updating first

                var parameters = new Dictionary<string, object> { { "ContentOriginal", document.Item2 }, { "Name", document.Item1 }, { "DisplayName", fileNameWithNoExtension }, { "TemplateDocumentCategoryId", (int)TemplateDocumentCategory.Unknown } };
                if (ext.IsNotNullOrEmpty() && ext.Equals(".cshtml", StringComparison.OrdinalIgnoreCase))
                {
                    parameters.Add("ContentTypeId", (int)ContentType.RazorTemplate);
                    parameters.Add("IsEditable", 1);
                }
                else if (ext.IsNotNullOrEmpty() && ext.Equals(".doc", StringComparison.OrdinalIgnoreCase))
                {
                    parameters.Add("ContentTypeId", (int)ContentType.WordDocument);
                    parameters.Add("IsEditable", 0);
                }
                else if (ext.IsNotNullOrEmpty() && ext.Equals(".txt", StringComparison.OrdinalIgnoreCase))
                {
                    parameters.Add("ContentTypeId", (int)ContentType.TextFile);
                    parameters.Add("IsEditable", 1);
                }
                else if (ext.IsNotNullOrEmpty() && ext.Equals(".trdx", StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                    // handled externally
                }
                else
                {
                    parameters.Add("ContentTypeId", (int)ContentType.Unknown);
                    parameters.Add("IsEditable", 0);
                }

                var doesRowExist = processor.Connection.Execute<int>(rowExists, processor.Transaction, parameters, false, 0);
                if (doesRowExist == 1)
                {
                    processor.Connection.Execute(updateDocuments, processor.Transaction, parameters, false, 0);
                }
                else
                {
                    // try to insert
                    processor.Connection.Execute(insertDocuments, processor.Transaction, parameters, false, 0);
                }
            }

            Trace.TraceInformation("Migrated documents.");
        }

        private static IEnumerable<Tuple<string, byte[]>> GetDocumentBytes(string extension = null)
        {
            const string documentPath = "IO.Practiceware.DbMigration.Documents.";

            var list = new List<Tuple<string, byte[]>>();

            var assembly = Assembly.GetExecutingAssembly();

            foreach (var name in assembly.GetManifestResourceNames().Where(r => r.StartsWith(documentPath)))
            {
                if (name.Contains("readme.txt"))
                    continue;

                if (extension != null && !string.Equals(extension, Path.GetExtension(name), StringComparison.OrdinalIgnoreCase))
                    continue;

                using (var stream = assembly.GetManifestResourceStream(name))
                {
                    list.Add(new Tuple<string, byte[]>(name.Replace(documentPath, string.Empty), stream.ToArray()));
                }
            }

            return list;
        }

        private static Seeding GetSeeding()
        {
            var serializer = Serialization.CreateCachedXmlSerializer(typeof(Seeding));
            using (var reader = new StringReader(Resources.Seeding))
            {
                return (Seeding)serializer.Deserialize(reader);
            }
        }

        private static void AddSqlMigrations(MigrationRunner runner)
        {
            string migrationsNamespace = "{0}.Migrations.".FormatWith(runner.MigrationAssembly.GetName().Name);
            Func<string, int> getStringStartIndexOfDate = s =>
                {
                    const string migrationName = "Migration";
                    int skipLength = migrationsNamespace.Length;
                    int fileNameIndex = skipLength + new string(s.Skip(skipLength).ToArray()).IndexOf(migrationName);
                    return fileNameIndex + migrationName.Length;  // StartIndexOfDate
                };

            foreach (var sqlMigration in runner.MigrationAssembly.GetManifestResourceNames()
                .Where(r => r.StartsWith(migrationsNamespace) && r.EndsWith(".sql"))
                .Select(r => new
                                 {
                                     Key = new string(r.Substring(getStringStartIndexOfDate(r)).Where(char.IsDigit).Take(12).ToArray()),
                                     Resource = r
                                 }))
            {
                long key;
                if (long.TryParse(sqlMigration.Key, out key))
                {
                    if (sqlMigration.Key.Length != 12)
                        throw new InvalidOperationException("SQL Migrations should have a 12 digit key.");

                    var migration = new SqlMigration(key.ToString(), runner.MigrationAssembly.GetManifestResourceStream(sqlMigration.Resource).GetString(), key);
                    ((MigrationLoader)runner.MigrationLoader).Migrations.Add(migration);
                }
            }
        }

        private static void CreateEnumTables(SqlServerProcessor processor)
        {
            var connection = processor.Connection;
            var transaction = processor.Transaction;

            const string modelTableNamesQuery = @"
SELECT o.name
FROM sys.objects o
WHERE o.schema_id = SCHEMA_ID('model') 
    AND o.Name LIKE '%[_]Enumeration' 
    AND OBJECTPROPERTY(o.object_id, 'IsUserTable') = 1 
ORDER BY o.name";

            var modelTableNames = connection.Execute<DataTable>(modelTableNamesQuery, transaction, null, false, 9).AsEnumerable().Select(r => (string)r[0]).ToArray();

            var sql = new StringBuilder();

            foreach (var t in typeof(IPracticeRepository).Assembly
                .GetTypes()
                .Where(t => t.IsEnum
                    && (!t.Name.EndsWith("Id")
                        || t.Assembly.GetType("{0}.{1}".FormatWith(t.Namespace, t.Name.Substring(0, t.Name.Length - 2))) == null)
                    && Enum.GetUnderlyingType(t) == typeof(int)))
            {
                var enumType = t;

                if (enumType.Name.ToLower() != "gender")
                {
                    var tableName = "{0}_Enumeration".FormatWith(enumType.Name);

                    // find int enum types in Model assembly where name doesn't end with id or if it does, there isn't a corresponding entity name (we don't want to include enums for 'well known values' 
                    // that also allow user defined entries
                    if (modelTableNames.Contains(tableName, StringComparer.OrdinalIgnoreCase))
                    {
                        // delete previous values
                        sql.AppendLine("DROP TABLE [model].[{0}]".FormatWith(tableName));
                        sql.AppendLine("GO");
                    }
                    // create new table
                    sql.AppendLine("CREATE TABLE [model].[{0}](Id int NOT NULL PRIMARY KEY, Name nvarchar(max) NOT NULL)".FormatWith(tableName));
                    sql.AppendLine("GO");

                    var insertSql = Enum.GetValues(enumType).Cast<int>().Select(i => new { Id = i, Name = Enum.GetName(enumType, i) })
                        .Select(e => "INSERT INTO [model].[{0}](Id, Name) VALUES ({1}, '{2}')".FormatWith(tableName, e.Id, e.Name))
                        .Join(Environment.NewLine);

                    sql.AppendLine(insertSql);
                    sql.AppendLine("GO");

                    sql.AppendLine("ALTER TABLE [model].[{0}] WITH NOCHECK ADD CONSTRAINT [{0}_ReadOnlyEnumeration] CHECK( 1 = 0 )".FormatWith(tableName));
                }
            }

            connection.RunScript(sql.ToString(), createCommand: () => new SqlCommand { CommandTimeout = 0, Transaction = (SqlTransaction)transaction }, useSingleCommand: true);

            Trace.TraceInformation("Added enumeration tables.");
        }

        /// <summary>
        /// Validates and retrieves report category Id for specified name if it exists in <see cref="ReportCategory"/> enum
        /// </summary>
        /// <param name="reportCategoryName"></param>
        /// <returns></returns>
        private static int GetReportCategoryIdFromName(string reportCategoryName)
        {
            var reportCategoryId = (int)ReportCategory.General;
            if (!string.IsNullOrEmpty(reportCategoryName))
            {
                reportCategoryId = (int)reportCategoryName.ToEnumOrDefault(ReportCategory.General);
            }
            return reportCategoryId;
        }

        /// <summary>
        /// Runs a collection of scripts.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="transaction">The transaction.</param>
        /// <param name="name">The name.</param>
        /// <param name="script">The script.</param>
        /// <exception cref="System.Exception">Script for {0} failed..FormatWith(name)</exception>
        private static void RunScript(IDbConnection connection, IDbTransaction transaction, string name, string script)
        {
            try
            {
                using (new TimedScope(s => Trace.TraceInformation("Script(s) for {0} ran in {1}.".FormatWith(name, s))))
                {
                    if (transaction == null)
                        connection.RunScript(script, false);
                    else
                        connection.RunScript(script, transaction, false, 0, true);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Script for {0} failed.".FormatWith(name), ex);
            }
        }

        private static void SetupSubscriptionReinitialization(SqlReplicationConfiguration sqlReplicationConfiguration)
        {
            using (var connection = new SqlConnection(sqlReplicationConfiguration.HubConnectionString))
            {
                Trace.TraceInformation("Adding subscription reinitialization logging...");
                connection.Open();
                connection.Execute(@"IF NOT EXISTS (SELECT * FROM sys.databases WHERE NAME = 'ReplicationAuditDb')
	CREATE DATABASE ReplicationAuditDb");
                connection.ChangeDatabase("ReplicationAuditDb");
                connection.Execute(@"IF OBJECT_ID('task_audit', 'U') IS NULL
BEGIN 
CREATE TABLE task_audit (
	[SPID] [smallint] NULL
	,[ApplicationName] [nvarchar](128) NULL
	,[auditdatetime] [datetime] NULL
	,[hostname] [nvarchar](255) NULL
	,[inputbuffer] [nvarchar](4000) NULL
	,[queued_reinit] BIT
	,[type] [varchar](2) NULL
	,[user] NVARCHAR(200) NULL
	)
END");
                connection.ChangeDatabase(new SqlConnectionStringBuilder(sqlReplicationConfiguration.HubConnectionString).InitialCatalog);
                connection.Execute(@"IF EXISTS (SELECT * FROM sys.triggers WHERE NAME = 'trig_syssubscriptions')
	DROP TRIGGER trig_syssubscriptions");
                connection.Execute(@"CREATE TRIGGER trig_syssubscriptions ON dbo.syssubscriptions
FOR UPDATE
AS
BEGIN
	DECLARE @command NVARCHAR(255)
	-- Create table variable to receive output of dbcc inputbuffer
	DECLARE @InputBuffer TABLE (
		[eventtype] NVARCHAR(30)
		,[parameters] INT
		,[eventinfo] NVARCHAR(4000)
		)

	INSERT INTO @InputBuffer
	EXEC ('
DBCC INPUTBUFFER(@@spid) WITH NO_INFOMSGS
')

	INSERT INTO [ReplicationAuditDb].dbo.[task_audit]
	SELECT @@SPID
		,APP_NAME()
		,GETDATE()
		,HOST_NAME()
		,(
			SELECT eventinfo
			FROM @InputBuffer
			) AS command
		,queued_reinit
		,'U'
		,SUSER_NAME()
	FROM inserted
END");
            }
        }
    }
}
