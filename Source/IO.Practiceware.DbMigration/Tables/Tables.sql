﻿


-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- --------------------------------------------------

-- BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON;
GO
IF SCHEMA_ID(N'model') IS NULL EXECUTE(N'CREATE SCHEMA [model]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[model].[FK_PatientSpecialExamPerformedEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT [FK_PatientSpecialExamPerformedEncounter];
GO
IF OBJECT_ID(N'[model].[FK_PatientOtherElementPerformedEncounter]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientInterventionPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT [FK_PatientOtherElementPerformedEncounter];
GO
IF OBJECT_ID(N'[model].[FK_PatientSpecialExamPerformedClinicalProcedure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT [FK_PatientSpecialExamPerformedClinicalProcedure];
GO
IF OBJECT_ID(N'[model].[FK_PatientOtherElementPerformedClinicalProcedure]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientInterventionPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT [FK_PatientOtherElementPerformedClinicalProcedure];
GO
IF OBJECT_ID(N'[model].[FK_PatientOtherElementPerformedClinicalDataSourceType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientInterventionPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT [FK_PatientOtherElementPerformedClinicalDataSourceType];
GO
IF OBJECT_ID(N'[model].[FK_PatientSpecialExamPerformedClinicalDataSourceType]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT [FK_PatientSpecialExamPerformedClinicalDataSourceType];
GO
IF OBJECT_ID(N'[model].[FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT [FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed];
GO
IF OBJECT_ID(N'[model].[FK_PatientInterventionPerformedPatient]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientInterventionPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT [FK_PatientInterventionPerformedPatient];
GO
IF OBJECT_ID(N'[model].[FK_PatientExamPerformedPatient]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT [FK_PatientExamPerformedPatient];
GO
IF OBJECT_ID(N'[model].[FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT [FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed];
GO
IF OBJECT_ID(N'[model].[FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT [FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed];
GO
IF OBJECT_ID(N'[model].[FK_PatientExamPerformedQuestionAnswerPatientExamPerformed]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT [FK_PatientExamPerformedQuestionAnswerPatientExamPerformed];
GO
IF OBJECT_ID(N'[model].[FK_QuestionAnswerQuestion]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswers]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT [FK_QuestionAnswerQuestion];
GO
IF OBJECT_ID(N'[model].[FK_QuestionAnswerValueQuestionAnswer]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswerValues]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswerValues] DROP CONSTRAINT [FK_QuestionAnswerValueQuestionAnswer];
GO
IF OBJECT_ID(N'[model].[FK_LinkedQuestionAnswerValuePracticeRepositoryEntity]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswerValues]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswerValues] DROP CONSTRAINT [FK_LinkedQuestionAnswerValuePracticeRepositoryEntity];
GO
IF OBJECT_ID(N'[model].[FK_UnitQuestionAnswerValueUnitOfMeasurement]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswerValues]'), 'IsUserTable') = 1
    ALTER TABLE [model].[QuestionAnswerValues] DROP CONSTRAINT [FK_UnitQuestionAnswerValueUnitOfMeasurement];
GO
IF OBJECT_ID(N'[model].[FK_PatientInterventionPerformedTimeQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientInterventionPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT [FK_PatientInterventionPerformedTimeQualifier];
GO
IF OBJECT_ID(N'[model].[FK_PatientExamPerformedTimeQualifier]', 'F') IS NOT NULL AND OBJECTPROPERTY(OBJECT_ID('[model].[PatientExamPerformeds]'), 'IsUserTable') = 1
    ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT [FK_PatientExamPerformedTimeQualifier];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Check if 'PatientExamPerformeds' exists...
IF OBJECT_ID(N'model.PatientExamPerformeds', 'U') IS NULL
BEGIN
-- 'PatientExamPerformeds' does not exist, creating...
	CREATE TABLE [model].[PatientExamPerformeds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [ClinicalProcedureId] int  NOT NULL,
    [ClinicalDataSourceTypeId] int  NOT NULL,
    [IsBillable] bit  NOT NULL,
    [PatientId] int  NOT NULL,
    [PerformedDateTime] datetime  NOT NULL,
    [TimeQualifierId] int  NULL
);
END
-- 'PatientExamPerformeds' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientExamPerformeds', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientExamPerformeds'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientExamPerformeds'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientExamPerformeds.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientExamPerformeds'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientExamPerformeds.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientExamPerformeds'
		AND COLUMN_NAME = 'ClinicalProcedureId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientExamPerformeds.ClinicalProcedureId. Column [ClinicalProcedureId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientExamPerformeds'
		AND COLUMN_NAME = 'ClinicalDataSourceTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientExamPerformeds.ClinicalDataSourceTypeId. Column [ClinicalDataSourceTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientExamPerformeds'
		AND COLUMN_NAME = 'IsBillable'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientExamPerformeds.IsBillable. Column [IsBillable] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientExamPerformeds'
		AND COLUMN_NAME = 'PatientId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientExamPerformeds.PatientId. Column [PatientId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientExamPerformeds'
		AND COLUMN_NAME = 'PerformedDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientExamPerformeds.PerformedDateTime. Column [PerformedDateTime] should be datetime, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientExamPerformeds'
		AND COLUMN_NAME = 'TimeQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientExamPerformeds.TimeQualifierId. Column [TimeQualifierId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

END

-- Check if 'PatientInterventionPerformeds' exists...
IF OBJECT_ID(N'model.PatientInterventionPerformeds', 'U') IS NULL
BEGIN
-- 'PatientInterventionPerformeds' does not exist, creating...
	CREATE TABLE [model].[PatientInterventionPerformeds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EncounterId] int  NOT NULL,
    [ClinicalProcedureId] int  NOT NULL,
    [ClinicalDataSourceTypeId] int  NOT NULL,
    [PatientId] int  NOT NULL,
    [PerformedDateTime] datetime  NOT NULL,
    [TimeQualifierId] int  NULL,
    [IsBillable] bit  NOT NULL
);
END
-- 'PatientInterventionPerformeds' exists, validating columns.
ELSE IF OBJECT_ID(N'model.PatientInterventionPerformeds', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientInterventionPerformeds'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.PatientInterventionPerformeds'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientInterventionPerformeds.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientInterventionPerformeds'
		AND COLUMN_NAME = 'EncounterId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientInterventionPerformeds.EncounterId. Column [EncounterId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientInterventionPerformeds'
		AND COLUMN_NAME = 'ClinicalProcedureId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientInterventionPerformeds.ClinicalProcedureId. Column [ClinicalProcedureId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientInterventionPerformeds'
		AND COLUMN_NAME = 'ClinicalDataSourceTypeId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientInterventionPerformeds.ClinicalDataSourceTypeId. Column [ClinicalDataSourceTypeId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientInterventionPerformeds'
		AND COLUMN_NAME = 'PatientId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientInterventionPerformeds.PatientId. Column [PatientId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientInterventionPerformeds'
		AND COLUMN_NAME = 'PerformedDateTime'
		AND DATA_TYPE = 'datetime'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientInterventionPerformeds.PerformedDateTime. Column [PerformedDateTime] should be datetime, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientInterventionPerformeds'
		AND COLUMN_NAME = 'TimeQualifierId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.PatientInterventionPerformeds.TimeQualifierId. Column [TimeQualifierId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='PatientInterventionPerformeds'
		AND COLUMN_NAME = 'IsBillable'
		AND DATA_TYPE = 'bit'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.PatientInterventionPerformeds.IsBillable. Column [IsBillable] should be bit, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'QuestionAnswers' exists...
IF OBJECT_ID(N'model.QuestionAnswers', 'U') IS NULL
BEGIN
-- 'QuestionAnswers' does not exist, creating...
	CREATE TABLE [model].[QuestionAnswers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [QuestionId] int  NOT NULL,
    [LateralityId] int  NULL,
    [PatientDiagnosticTestPerformedId] int  NULL,
    [PatientInterventionPerformedId] int  NULL,
    [PatientProcedurePerformedId] int  NULL,
    [PatientExamPerformedId] int  NULL,
    [__EntityType__] nvarchar(100)  NOT NULL
);
END
-- 'QuestionAnswers' exists, validating columns.
ELSE IF OBJECT_ID(N'model.QuestionAnswers', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswers'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.QuestionAnswers'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.QuestionAnswers.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswers'
		AND COLUMN_NAME = 'QuestionId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.QuestionAnswers.QuestionId. Column [QuestionId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswers'
		AND COLUMN_NAME = 'LateralityId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.QuestionAnswers.LateralityId. Column [LateralityId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswers'
		AND COLUMN_NAME = 'PatientDiagnosticTestPerformedId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.QuestionAnswers.PatientDiagnosticTestPerformedId. Column [PatientDiagnosticTestPerformedId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswers'
		AND COLUMN_NAME = 'PatientInterventionPerformedId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.QuestionAnswers.PatientInterventionPerformedId. Column [PatientInterventionPerformedId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswers'
		AND COLUMN_NAME = 'PatientProcedurePerformedId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.QuestionAnswers.PatientProcedurePerformedId. Column [PatientProcedurePerformedId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswers'
		AND COLUMN_NAME = 'PatientExamPerformedId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.QuestionAnswers.PatientExamPerformedId. Column [PatientExamPerformedId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswers'
		AND COLUMN_NAME = '__EntityType__'
		AND DATA_TYPE = 'nvarchar'
				AND CHARACTER_MAXIMUM_LENGTH = '100'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.QuestionAnswers.__EntityType__. Column [__EntityType__] should be nvarchar(100), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- Check if 'QuestionAnswerValues' exists...
IF OBJECT_ID(N'model.QuestionAnswerValues', 'U') IS NULL
BEGIN
-- 'QuestionAnswerValues' does not exist, creating...
	CREATE TABLE [model].[QuestionAnswerValues] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [QuestionAnswerId] int  NOT NULL,
    [PracticeRepositoryEntityId] int  NULL,
    [PracticeRepositoryEntityKey] nvarchar(max)  NULL,
    [Unit] decimal(18,0)  NULL,
    [UnitOfMeasurementId] int  NULL,
    [Text] nvarchar(max)  NULL,
    [__EntityType__] nvarchar(100)  NOT NULL
);
END
-- 'QuestionAnswerValues' exists, validating columns.
ELSE IF OBJECT_ID(N'model.QuestionAnswerValues', 'U') IS NOT NULL
BEGIN
IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswerValues'
		AND COLUMN_NAME = 'Id'
		AND COLUMNPROPERTY(OBJECT_ID(N'model.QuestionAnswerValues'),COLUMN_NAME,'IsIdentity') = 1
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.QuestionAnswerValues.Id. Column [Id] should be int, and have the following properties:IDENTITY(1,1) NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswerValues'
		AND COLUMN_NAME = 'QuestionAnswerId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.QuestionAnswerValues.QuestionAnswerId. Column [QuestionAnswerId] should be int, and have the following properties: NOT NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswerValues'
		AND COLUMN_NAME = 'PracticeRepositoryEntityId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.QuestionAnswerValues.PracticeRepositoryEntityId. Column [PracticeRepositoryEntityId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswerValues'
		AND COLUMN_NAME = 'PracticeRepositoryEntityKey'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.QuestionAnswerValues.PracticeRepositoryEntityKey. Column [PracticeRepositoryEntityKey] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswerValues'
		AND COLUMN_NAME = 'Unit'
		AND DATA_TYPE = 'decimal'
		AND NUMERIC_PRECISION = '18'
		AND NUMERIC_SCALE = '0'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.QuestionAnswerValues.Unit. Column [Unit] should be decimal(18,0), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswerValues'
		AND COLUMN_NAME = 'UnitOfMeasurementId'
		AND DATA_TYPE = 'int'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.QuestionAnswerValues.UnitOfMeasurementId. Column [UnitOfMeasurementId] should be int, and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswerValues'
		AND COLUMN_NAME = 'Text'
		AND DATA_TYPE = 'nvarchar'
		AND CHARACTER_MAXIMUM_LENGTH = '-1'
		AND IS_NULLABLE = 'YES'
) 
RAISERROR ('Error found, with model.QuestionAnswerValues.Text. Column [Text] should be nvarchar(max), and have the following properties: NULL', 16, 2) WITH SETERROR

IF NOT EXISTS (
	SELECT 
	COLUMN_NAME
	FROM information_schema.COLUMNS 
	WHERE TABLE_SCHEMA = 'model'
		AND TABLE_NAME ='QuestionAnswerValues'
		AND COLUMN_NAME = '__EntityType__'
		AND DATA_TYPE = 'nvarchar'
				AND CHARACTER_MAXIMUM_LENGTH = '100'
		AND IS_NULLABLE = 'NO'
) 
RAISERROR ('Error found, with model.QuestionAnswerValues.__EntityType__. Column [__EntityType__] should be nvarchar(100), and have the following properties: NOT NULL', 16, 2) WITH SETERROR

END

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'PatientExamPerformeds'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientExamPerformeds', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientExamPerformeds', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientExamPerformeds]
ADD CONSTRAINT [PK_PatientExamPerformeds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PatientInterventionPerformeds'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.PatientInterventionPerformeds', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.PatientInterventionPerformeds', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[PatientInterventionPerformeds]
ADD CONSTRAINT [PK_PatientInterventionPerformeds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'QuestionAnswers'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.QuestionAnswers', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.QuestionAnswers', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[QuestionAnswers]
ADD CONSTRAINT [PK_QuestionAnswers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'QuestionAnswerValues'
IF EXISTS (SELECT * FROM sys.key_constraints WHERE [type] = 'PK' AND parent_object_id = OBJECT_ID(N'model.QuestionAnswerValues', 'U'))
BEGIN
DECLARE @sql NVARCHAR(MAX)
SELECT 
@sql = 'ALTER TABLE [model].[QuestionAnswerValues] DROP CONSTRAINT '+name+';'
FROM sys.key_constraints
WHERE [type] = 'PK'
AND parent_object_id = OBJECT_ID(N'model.QuestionAnswerValues', 'U')
EXEC(@sql)
END
GO
ALTER TABLE [model].[QuestionAnswerValues]
ADD CONSTRAINT [PK_QuestionAnswerValues]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [EncounterId] in table 'PatientExamPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientSpecialExamPerformedEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT FK_PatientSpecialExamPerformedEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientExamPerformeds]
ADD CONSTRAINT [FK_PatientSpecialExamPerformedEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSpecialExamPerformedEncounter'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientSpecialExamPerformedEncounter')
	DROP INDEX IX_FK_PatientSpecialExamPerformedEncounter ON [model].[PatientExamPerformeds]
GO
CREATE INDEX [IX_FK_PatientSpecialExamPerformedEncounter]
ON [model].[PatientExamPerformeds]
    ([EncounterId]);
GO

-- Creating foreign key on [EncounterId] in table 'PatientInterventionPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientOtherElementPerformedEncounter'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT FK_PatientOtherElementPerformedEncounter

IF OBJECTPROPERTY(OBJECT_ID('[model].[Encounters]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInterventionPerformeds]
ADD CONSTRAINT [FK_PatientOtherElementPerformedEncounter]
    FOREIGN KEY ([EncounterId])
    REFERENCES [model].[Encounters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientOtherElementPerformedEncounter'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientOtherElementPerformedEncounter')
	DROP INDEX IX_FK_PatientOtherElementPerformedEncounter ON [model].[PatientInterventionPerformeds]
GO
CREATE INDEX [IX_FK_PatientOtherElementPerformedEncounter]
ON [model].[PatientInterventionPerformeds]
    ([EncounterId]);
GO

-- Creating foreign key on [ClinicalProcedureId] in table 'PatientExamPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientSpecialExamPerformedClinicalProcedure'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT FK_PatientSpecialExamPerformedClinicalProcedure

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalProcedures]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientExamPerformeds]
ADD CONSTRAINT [FK_PatientSpecialExamPerformedClinicalProcedure]
    FOREIGN KEY ([ClinicalProcedureId])
    REFERENCES [model].[ClinicalProcedures]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSpecialExamPerformedClinicalProcedure'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientSpecialExamPerformedClinicalProcedure')
	DROP INDEX IX_FK_PatientSpecialExamPerformedClinicalProcedure ON [model].[PatientExamPerformeds]
GO
CREATE INDEX [IX_FK_PatientSpecialExamPerformedClinicalProcedure]
ON [model].[PatientExamPerformeds]
    ([ClinicalProcedureId]);
GO

-- Creating foreign key on [ClinicalProcedureId] in table 'PatientInterventionPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientOtherElementPerformedClinicalProcedure'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT FK_PatientOtherElementPerformedClinicalProcedure

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalProcedures]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInterventionPerformeds]
ADD CONSTRAINT [FK_PatientOtherElementPerformedClinicalProcedure]
    FOREIGN KEY ([ClinicalProcedureId])
    REFERENCES [model].[ClinicalProcedures]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientOtherElementPerformedClinicalProcedure'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientOtherElementPerformedClinicalProcedure')
	DROP INDEX IX_FK_PatientOtherElementPerformedClinicalProcedure ON [model].[PatientInterventionPerformeds]
GO
CREATE INDEX [IX_FK_PatientOtherElementPerformedClinicalProcedure]
ON [model].[PatientInterventionPerformeds]
    ([ClinicalProcedureId]);
GO

-- Creating foreign key on [ClinicalDataSourceTypeId] in table 'PatientInterventionPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientOtherElementPerformedClinicalDataSourceType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT FK_PatientOtherElementPerformedClinicalDataSourceType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalDataSourceTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInterventionPerformeds]
ADD CONSTRAINT [FK_PatientOtherElementPerformedClinicalDataSourceType]
    FOREIGN KEY ([ClinicalDataSourceTypeId])
    REFERENCES [model].[ClinicalDataSourceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientOtherElementPerformedClinicalDataSourceType'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientOtherElementPerformedClinicalDataSourceType')
	DROP INDEX IX_FK_PatientOtherElementPerformedClinicalDataSourceType ON [model].[PatientInterventionPerformeds]
GO
CREATE INDEX [IX_FK_PatientOtherElementPerformedClinicalDataSourceType]
ON [model].[PatientInterventionPerformeds]
    ([ClinicalDataSourceTypeId]);
GO

-- Creating foreign key on [ClinicalDataSourceTypeId] in table 'PatientExamPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientSpecialExamPerformedClinicalDataSourceType'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT FK_PatientSpecialExamPerformedClinicalDataSourceType

IF OBJECTPROPERTY(OBJECT_ID('[model].[ClinicalDataSourceTypes]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientExamPerformeds]
ADD CONSTRAINT [FK_PatientSpecialExamPerformedClinicalDataSourceType]
    FOREIGN KEY ([ClinicalDataSourceTypeId])
    REFERENCES [model].[ClinicalDataSourceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientSpecialExamPerformedClinicalDataSourceType'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientSpecialExamPerformedClinicalDataSourceType')
	DROP INDEX IX_FK_PatientSpecialExamPerformedClinicalDataSourceType ON [model].[PatientExamPerformeds]
GO
CREATE INDEX [IX_FK_PatientSpecialExamPerformedClinicalDataSourceType]
ON [model].[PatientExamPerformeds]
    ([ClinicalDataSourceTypeId]);
GO

-- Creating foreign key on [PatientDiagnosticTestPerformedId] in table 'QuestionAnswers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientDiagnosticTestPerformeds]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswers]
ADD CONSTRAINT [FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed]
    FOREIGN KEY ([PatientDiagnosticTestPerformedId])
    REFERENCES [model].[PatientDiagnosticTestPerformeds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed')
	DROP INDEX IX_FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed ON [model].[QuestionAnswers]
GO
CREATE INDEX [IX_FK_PatientDiagnosticTestPerformedQuestionAnswerPatientDiagnosticTestPerformed]
ON [model].[QuestionAnswers]
    ([PatientDiagnosticTestPerformedId]);
GO

-- Creating foreign key on [PatientId] in table 'PatientInterventionPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientInterventionPerformedPatient'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT FK_PatientInterventionPerformedPatient

IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInterventionPerformeds]
ADD CONSTRAINT [FK_PatientInterventionPerformedPatient]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientInterventionPerformedPatient'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientInterventionPerformedPatient')
	DROP INDEX IX_FK_PatientInterventionPerformedPatient ON [model].[PatientInterventionPerformeds]
GO
CREATE INDEX [IX_FK_PatientInterventionPerformedPatient]
ON [model].[PatientInterventionPerformeds]
    ([PatientId]);
GO

-- Creating foreign key on [PatientId] in table 'PatientExamPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientExamPerformedPatient'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT FK_PatientExamPerformedPatient

IF OBJECTPROPERTY(OBJECT_ID('[model].[Patients]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientExamPerformeds]
ADD CONSTRAINT [FK_PatientExamPerformedPatient]
    FOREIGN KEY ([PatientId])
    REFERENCES [model].[Patients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientExamPerformedPatient'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientExamPerformedPatient')
	DROP INDEX IX_FK_PatientExamPerformedPatient ON [model].[PatientExamPerformeds]
GO
CREATE INDEX [IX_FK_PatientExamPerformedPatient]
ON [model].[PatientExamPerformeds]
    ([PatientId]);
GO

-- Creating foreign key on [PatientInterventionPerformedId] in table 'QuestionAnswers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientInterventionPerformeds]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswers]
ADD CONSTRAINT [FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed]
    FOREIGN KEY ([PatientInterventionPerformedId])
    REFERENCES [model].[PatientInterventionPerformeds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed')
	DROP INDEX IX_FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed ON [model].[QuestionAnswers]
GO
CREATE INDEX [IX_FK_PatientInterventionPerformedQuestionAnswerPatientInterventionPerformed]
ON [model].[QuestionAnswers]
    ([PatientInterventionPerformedId]);
GO

-- Creating foreign key on [PatientProcedurePerformedId] in table 'QuestionAnswers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientProcedurePerformeds]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswers]
ADD CONSTRAINT [FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed]
    FOREIGN KEY ([PatientProcedurePerformedId])
    REFERENCES [model].[PatientProcedurePerformeds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed')
	DROP INDEX IX_FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed ON [model].[QuestionAnswers]
GO
CREATE INDEX [IX_FK_PatientProcedurePerformedQuestionAnswerPatientProcedurePerformed]
ON [model].[QuestionAnswers]
    ([PatientProcedurePerformedId]);
GO

-- Creating foreign key on [PatientExamPerformedId] in table 'QuestionAnswers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientExamPerformedQuestionAnswerPatientExamPerformed'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT FK_PatientExamPerformedQuestionAnswerPatientExamPerformed

IF OBJECTPROPERTY(OBJECT_ID('[model].[PatientExamPerformeds]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswers]
ADD CONSTRAINT [FK_PatientExamPerformedQuestionAnswerPatientExamPerformed]
    FOREIGN KEY ([PatientExamPerformedId])
    REFERENCES [model].[PatientExamPerformeds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientExamPerformedQuestionAnswerPatientExamPerformed'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientExamPerformedQuestionAnswerPatientExamPerformed')
	DROP INDEX IX_FK_PatientExamPerformedQuestionAnswerPatientExamPerformed ON [model].[QuestionAnswers]
GO
CREATE INDEX [IX_FK_PatientExamPerformedQuestionAnswerPatientExamPerformed]
ON [model].[QuestionAnswers]
    ([PatientExamPerformedId]);
GO

-- Creating foreign key on [QuestionId] in table 'QuestionAnswers'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_QuestionAnswerQuestion'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[QuestionAnswers] DROP CONSTRAINT FK_QuestionAnswerQuestion

IF OBJECTPROPERTY(OBJECT_ID('[model].[Questions]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswers]
ADD CONSTRAINT [FK_QuestionAnswerQuestion]
    FOREIGN KEY ([QuestionId])
    REFERENCES [model].[Questions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionAnswerQuestion'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_QuestionAnswerQuestion')
	DROP INDEX IX_FK_QuestionAnswerQuestion ON [model].[QuestionAnswers]
GO
CREATE INDEX [IX_FK_QuestionAnswerQuestion]
ON [model].[QuestionAnswers]
    ([QuestionId]);
GO

-- Creating foreign key on [QuestionAnswerId] in table 'QuestionAnswerValues'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_QuestionAnswerValueQuestionAnswer'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[QuestionAnswerValues] DROP CONSTRAINT FK_QuestionAnswerValueQuestionAnswer

IF OBJECTPROPERTY(OBJECT_ID('[model].[QuestionAnswers]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswerValues]
ADD CONSTRAINT [FK_QuestionAnswerValueQuestionAnswer]
    FOREIGN KEY ([QuestionAnswerId])
    REFERENCES [model].[QuestionAnswers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionAnswerValueQuestionAnswer'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_QuestionAnswerValueQuestionAnswer')
	DROP INDEX IX_FK_QuestionAnswerValueQuestionAnswer ON [model].[QuestionAnswerValues]
GO
CREATE INDEX [IX_FK_QuestionAnswerValueQuestionAnswer]
ON [model].[QuestionAnswerValues]
    ([QuestionAnswerId]);
GO

-- Creating foreign key on [PracticeRepositoryEntityId] in table 'QuestionAnswerValues'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_LinkedQuestionAnswerValuePracticeRepositoryEntity'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[QuestionAnswerValues] DROP CONSTRAINT FK_LinkedQuestionAnswerValuePracticeRepositoryEntity

IF OBJECTPROPERTY(OBJECT_ID('[model].[PracticeRepositoryEntities]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswerValues]
ADD CONSTRAINT [FK_LinkedQuestionAnswerValuePracticeRepositoryEntity]
    FOREIGN KEY ([PracticeRepositoryEntityId])
    REFERENCES [model].[PracticeRepositoryEntities]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkedQuestionAnswerValuePracticeRepositoryEntity'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_LinkedQuestionAnswerValuePracticeRepositoryEntity')
	DROP INDEX IX_FK_LinkedQuestionAnswerValuePracticeRepositoryEntity ON [model].[QuestionAnswerValues]
GO
CREATE INDEX [IX_FK_LinkedQuestionAnswerValuePracticeRepositoryEntity]
ON [model].[QuestionAnswerValues]
    ([PracticeRepositoryEntityId]);
GO

-- Creating foreign key on [UnitOfMeasurementId] in table 'QuestionAnswerValues'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_UnitQuestionAnswerValueUnitOfMeasurement'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[QuestionAnswerValues] DROP CONSTRAINT FK_UnitQuestionAnswerValueUnitOfMeasurement

IF OBJECTPROPERTY(OBJECT_ID('[model].[UnitsOfMeasurement]'), 'IsUserTable') = 1
ALTER TABLE [model].[QuestionAnswerValues]
ADD CONSTRAINT [FK_UnitQuestionAnswerValueUnitOfMeasurement]
    FOREIGN KEY ([UnitOfMeasurementId])
    REFERENCES [model].[UnitsOfMeasurement]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UnitQuestionAnswerValueUnitOfMeasurement'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_UnitQuestionAnswerValueUnitOfMeasurement')
	DROP INDEX IX_FK_UnitQuestionAnswerValueUnitOfMeasurement ON [model].[QuestionAnswerValues]
GO
CREATE INDEX [IX_FK_UnitQuestionAnswerValueUnitOfMeasurement]
ON [model].[QuestionAnswerValues]
    ([UnitOfMeasurementId]);
GO

-- Creating foreign key on [TimeQualifierId] in table 'PatientInterventionPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientInterventionPerformedTimeQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientInterventionPerformeds] DROP CONSTRAINT FK_PatientInterventionPerformedTimeQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[TimeQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientInterventionPerformeds]
ADD CONSTRAINT [FK_PatientInterventionPerformedTimeQualifier]
    FOREIGN KEY ([TimeQualifierId])
    REFERENCES [model].[TimeQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientInterventionPerformedTimeQualifier'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientInterventionPerformedTimeQualifier')
	DROP INDEX IX_FK_PatientInterventionPerformedTimeQualifier ON [model].[PatientInterventionPerformeds]
GO
CREATE INDEX [IX_FK_PatientInterventionPerformedTimeQualifier]
ON [model].[PatientInterventionPerformeds]
    ([TimeQualifierId]);
GO

-- Creating foreign key on [TimeQualifierId] in table 'PatientExamPerformeds'
IF EXISTS (SELECT object_id FROM sys.Objects WHERE Name = 'FK_PatientExamPerformedTimeQualifier'
	AND type_desc = 'FOREIGN_KEY_CONSTRAINT')
	ALTER TABLE [model].[PatientExamPerformeds] DROP CONSTRAINT FK_PatientExamPerformedTimeQualifier

IF OBJECTPROPERTY(OBJECT_ID('[model].[TimeQualifiers]'), 'IsUserTable') = 1
ALTER TABLE [model].[PatientExamPerformeds]
ADD CONSTRAINT [FK_PatientExamPerformedTimeQualifier]
    FOREIGN KEY ([TimeQualifierId])
    REFERENCES [model].[TimeQualifiers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientExamPerformedTimeQualifier'
IF EXISTS (SELECT object_id FROM sys.indexes WHERE name = 'IX_FK_PatientExamPerformedTimeQualifier')
	DROP INDEX IX_FK_PatientExamPerformedTimeQualifier ON [model].[PatientExamPerformeds]
GO
CREATE INDEX [IX_FK_PatientExamPerformedTimeQualifier]
ON [model].[PatientExamPerformeds]
    ([TimeQualifierId]);
GO


-- COMMIT TRANSACTION
-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------