﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Soaf;
using Soaf.Collections;
using ConfigurationManager = IO.Practiceware.Configuration.ConfigurationManager;

namespace IO.Practiceware.DbMigration
{
    [XmlRoot("dbMigrations")]
    public class DbMigrationsConfiguration
    {
        [XmlElement("dbMigration")]
        public List<DbMigrationConfiguration> Migrations { get; set; }
    }

    /// <summary>
    ///   Information about a migration to perform.
    /// </summary>
    public class DbMigrationConfiguration
    {
        private ObservableCollection<string> _migrationConnectionStrings;
        private bool _isUpdatingMigrationConnectionStrings;
        private string _dbSyncGeneratedScriptPath;

        public DbMigrationConfiguration()
        {
            UseSnapshotForReplication = false;
            UseDbSyncForReplication = true;
            ExecuteDbSyncScriptAutomatically = true;
            DbSyncGeneratedScriptPath = Path.Combine(Path.GetTempPath(), "DbSyncScripts");
        }

        /// <summary>
        ///   Gets or sets the connection string for the database to migrate. If not a fully qualified connection string, will check other named connection strings in the config for a matching name.
        /// </summary>
        /// <value> The connection string. </value>
        [XmlArray("migrationConnectionStrings")]
        [XmlArrayItem("add")]
        public ObservableCollection<string> MigrationConnectionStrings
        {
            get
            {
                if (_migrationConnectionStrings == null)
                {
                    _migrationConnectionStrings = new ObservableCollection<string>();
                    _migrationConnectionStrings.CollectionChanged += OnMigrationConnectionStringsCollectionChanged;
                }
                return _migrationConnectionStrings;
            }
        }

        private void OnMigrationConnectionStringsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (_isUpdatingMigrationConnectionStrings) return;

            _isUpdatingMigrationConnectionStrings = true;
            try
            {
                int i = 0;
                foreach (string cs in MigrationConnectionStrings.ToArray())
                {
                    var connectionString = Regex.Replace(cs, "MultipleActiveResultSets=True", string.Empty, RegexOptions.IgnoreCase);
                    connectionString = ConfigurationManager.Configuration.ConnectionStrings.ConnectionStrings.OfType<ConnectionStringSettings>()
                                                           .Where(css => css.Name.Equals(connectionString, StringComparison.OrdinalIgnoreCase)).Select(css => css.ConnectionString).FirstOrDefault() ?? connectionString;

                    _migrationConnectionStrings[i] = connectionString;
                    i++;
                }
            }
            finally
            {
                _isUpdatingMigrationConnectionStrings = false;
            }
        }

        /// <summary>
        ///   Gets or sets a value indicating whether to [exclude model views] when migrating.
        /// </summary>
        /// <value> <c>true</c> if [exclude model views]; otherwise, <c>false</c> . </value>
        [XmlAttribute("excludeModelViews")]
        public bool ExcludeModelViews { get; set; }

        /// <summary>
        ///   Gets or sets a value indicating whether to skip auditing after running the migration.
        /// </summary>
        [XmlAttribute("skipAuditing")]
        public bool SkipAuditing { get; set; }

        /// <summary>
        /// Gets or sets the migration lockout datetime. Migrations are not re-run even when checksums do not match earlier than this date
        /// Value should be in Universal sortable format: 2009-06-15 20:45:30Z
        /// </summary>
        /// <value>
        /// The migration lockout period.
        /// </value>
        [XmlAttribute("migrationLockoutPeriod")]
        public string MigrationLockoutPeriod { get; set; }

        /// <summary>
        /// Gets or sets a value indicating to the migrator whether or not to use the Snapshot feature to setup replication. Default false;
        /// </summary>
        [XmlAttribute("useSnapshotForReplication")]
        public bool UseSnapshotForReplication { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not to use the DbSync utility to setup replication. Default true.
        /// </summary>
        /// <remarks>
        /// If <see cref="UseSnapshotForReplication"/> is false and <see cref="UseDbSyncForReplication"/> is true,
        /// the migrator will run the DbSync utility to perform replication.  The DbSync utility performs a 
        /// manually comarison against the main hub server and it's replicated servers.  It generates
        /// a script which can then be ran automatically or saved to a local directory for manual execution.
        /// </remarks>
        [XmlAttribute("useDbSyncForReplication")]
        public bool UseDbSyncForReplication { get; set; }

        /// <summary>
        /// Gets or sets a value to indicate whether or not the DbSync utility's generated scripts get executed automatically. Default true
        /// </summary>
        /// <remarks>
        /// If <see cref="UseDbSyncForReplication"/> is true and <see cref="ExecuteDbSyncScriptAutomatically"/> is set to true, when the migrator tries to 
        /// setup replication using the DbSync utility, it will execute the generated scripts automatically rather than saving them
        /// to a directory for future manual execution.
        /// </remarks>
        [XmlAttribute("executeDbSyncScriptAutomatically")]
        public bool ExecuteDbSyncScriptAutomatically { get; set; }

        /// <summary>
        /// Gets or sets a value representing a path to use when the DbSync utility creates and saves
        /// the synchronizer scripts. Default current directory.
        /// </summary>
        /// <remarks>
        /// If <see cref="UseDbSyncForReplication"/> is true and a valid path is provided here,
        /// the DbSync utility will save the generated scripts in this location. If no path is
        /// provided, the scripts will be saved in the same directory as the executing process.
        /// </remarks>
        [XmlAttribute("dbSyncGeneratedScriptPath")]
        public string DbSyncGeneratedScriptPath
        {
            get { return _dbSyncGeneratedScriptPath; }
            set { _dbSyncGeneratedScriptPath = Environment.ExpandEnvironmentVariables(value); }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("ConnectionString: {0}".FormatWith(MigrationConnectionStrings.Join()));
            sb.AppendLine("ExcludeModelViews: {0}".FormatWith(ExcludeModelViews));
            sb.AppendLine("SkipAuditing: {0}".FormatWith(SkipAuditing));
            sb.AppendLine("UseSnapshotForReplication: {0}".FormatWith(UseSnapshotForReplication));
            sb.AppendLine("UseDbSyncForReplication: {0}".FormatWith(UseDbSyncForReplication));
            sb.AppendLine("ExecuteDbSyncScriptAutomatically: {0}".FormatWith(ExecuteDbSyncScriptAutomatically));
            sb.AppendLine("DbSyncGeneratedScriptPath: {0}".FormatWith(DbSyncGeneratedScriptPath));

            return sb.ToString();
        }

        public string GetConnectionStringsDisplayValue()
        {
            var sb = new StringBuilder();
            foreach (var s in MigrationConnectionStrings)
            {
                try
                {
                    var connectionStringBuilder = new SqlConnectionStringBuilder(s);
                    sb.AppendLine("{0} on {1}".FormatWith(connectionStringBuilder.InitialCatalog, connectionStringBuilder.DataSource));
                }
                catch (Exception)
                {
                    sb.AppendLine(s);
                }
            }
            return sb.ToString();
        }

        public string GetFirstConnectionStringDisplayValue()
        {
            var connectionStringBuilder = new SqlConnectionStringBuilder(MigrationConnectionStrings[0]);
            return "{0} on {1}".FormatWith(connectionStringBuilder.InitialCatalog, connectionStringBuilder.DataSource);
        }
    }
}
