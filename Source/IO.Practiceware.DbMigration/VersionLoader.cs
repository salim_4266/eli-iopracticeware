﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using FluentMigrator;
using FluentMigrator.Model;
using FluentMigrator.Runner;
using Soaf;
using Soaf.Collections;

namespace IO.Practiceware.DbMigration
{
    /// <summary>
    ///     A custom VersionLoader that uses the default VersionLoader, but tracks checksums.
    /// </summary>
    public class VersionLoader : FluentMigrator.Runner.VersionLoader
    {
        private readonly string _productVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        private static readonly Dictionary<string, byte[]> ChecksumCache = new Dictionary<string, byte[]>();

        /// <summary>
        /// Specify date after which migrations never get re-executed even if checksums differ
        /// </summary>
        public static DateTime? MigrationsLockoutPeriod { get; private set; }

        public VersionLoader(IMigrationRunner runner, Assembly assembly, IMigrationConventions conventions, DateTime? lockoutPeriod)
            : base(runner, assembly, conventions)
        {
            MigrationsLockoutPeriod = lockoutPeriod;
        }

        public void DeleteRowsWithDifferentChecksums()
        {
            var table = Runner.Processor.ReadTableData(VersionTableMetaData.SchemaName, VersionTableMetaData.TableName).Tables[0];

            if (table.Columns["Checksum"] == null)
            {
                // db has never been migrated to add checksum column, but does have a VersionInfo table already
                return;
            }

            foreach (var dataRow in table.AsEnumerable())
            {
                var version = dataRow["Version"].IfNotNull(o => (long)o);

                // this process is only supported for constant migrations (Views.sql, etc).
                if (version < ConstantMigrations.BaseVersion) continue;

                var migration = GetMigrationByVersion(version);
                if (migration == null) continue;

                var dbChecksum = dataRow["Checksum"].As<byte[]>();
                var formattedDbChecksum = Checksum.Format(dbChecksum);
                var migrationChecksum = Checksum.Format(GetChecksum(version));

                if (formattedDbChecksum != null && migrationChecksum != null
                    && formattedDbChecksum != migrationChecksum // For SQL 2012 (byte[] is returned as byte[])
                    && migrationChecksum != Encoding.UTF8.GetString(dbChecksum) // SQL Server 2005 returns byte[] as hex string converted to byte[]
                    && TryConvertVersionToDate(version) > (MigrationsLockoutPeriod ?? DateTime.MinValue)) // Lockout migrations we never want to re-run
                {
                    Trace.TraceInformation("Migration \"{0}\" has changed. Removing from VersionInfo and will re-run.".FormatWith(migration.As<SqlMigration>().IfNotNull(m => m.ToString(), version.ToString)));

                    var toDelete = new List<long> { version };

                    if (version == ConstantMigrations.ModelViews.Version)
                    {
                        toDelete.Add(ConstantMigrations.Audit.Version);
                    }

                    toDelete.ForEach(v => Runner.Processor.Execute("DELETE FROM dbo.VersionInfo WHERE Version = {0}".FormatWith(v)));
                }
            }
            LoadVersionInfo();
        }

        protected override InsertionDataDefinition CreateVersionInfoInsertionData(long version, string description)
        {
            InsertionDataDefinition result = base.CreateVersionInfoInsertionData(version, description);

            var checksum = GetChecksum(version);

            if (checksum != null)
            {
                result.Add(new KeyValuePair<string, object>("Checksum", new Checksum(checksum)));
            }

            result.Add(new KeyValuePair<string, object>("ProductVersion", _productVersion));

            return result;
        }

        /// <summary>
        ///     Gets a unique checksum for the migration content if possible.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns></returns>
        private byte[] GetChecksum(long version)
        {
            var m = GetMigrationByVersion(version);

            var sqlMigration = m as SqlMigration;
            if (sqlMigration != null)
            {
                return GetSqlMigrationChecksum(sqlMigration);
            }

            // note, tried using GetMethodBody to get IL as byte array for C# migrations
            // but results turn out not to be consistent across builds

            return null;
        }

        private IMigration GetMigrationByVersion(long version)
        {
            var migration = ((MigrationRunner)Runner).MigrationLoader.LoadMigrations().Where(m => m.Key == version).Select(m => m.Value).FirstOrDefault();

            if (migration != null)
            {
                return migration.Migration;
            }
            return null;
        }

        private static byte[] GetSqlMigrationChecksum(SqlMigration migration)
        {
            return ChecksumCache.GetValue(migration.Sql, () => migration.Sql.ComputeMd5Hash());
        }

        private static DateTime TryConvertVersionToDate(long version)
        {
            DateTime result;
            if (!DateTime.TryParseExact(version.ToString(CultureInfo.InvariantCulture), "yyyyMMddHHmm",
                CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal, out result))
            {
                // If version does not represent date and time -> use now
                result = DateTime.UtcNow;
            }
            return result;
        }

        /// <summary>
        /// Required so FluentMigrator doesn't enquote the checksum.
        /// </summary>
        private class Checksum
        {
            private readonly byte[] _value;

            public Checksum(byte[] value)
            {
                _value = value;
            }

            public override string ToString()
            {
                return "CONVERT(varbinary(max), '{0}', 2)".FormatWith(Format(_value));
            }

            public static string Format(byte[] value)
            {
                return value == null
                    ? null
                    : BitConverter.ToString(value).Replace("-", string.Empty);
            }
        }
    }
}