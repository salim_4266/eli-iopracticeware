﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.ServiceProcess;
using System.Threading;
using Soaf;

namespace IO.Practiceware.DbMigration
{
    internal class MsDtcConfigurator
    {
        public static void Configure(IEnumerable<string> connectionStrings)
        {
            foreach (string connectionString in connectionStrings)
            {
                if (string.IsNullOrEmpty(connectionString))
                {
                    var exception = new Exception("Connection string not present.");
                    throw exception;
                }
                var builder = new SqlConnectionStringBuilder(connectionString);

                var remoteComputerName = builder.DataSource.Split(new[] { "\\" }, StringSplitOptions.None).First();

                if (remoteComputerName.Length > 15)
                {
                    throw new Exception("Server name must be less than 16 characters");
                }

                if (string.IsNullOrEmpty(remoteComputerName))
                {
                    var exception = new Exception("Invalid Remote Computer.");
                    throw exception;
                }
                string uncFilePath = "\\\\" + remoteComputerName + "\\C$\\MSDTC_Security.reg";
                try
                {
                    File.WriteAllText(uncFilePath, MigrationResources.MSDTC_Security);

                    var wmiScope = new ManagementScope(String.Format("\\\\{0}\\root\\cimv2", remoteComputerName));
                    var wmiProcess = new ManagementClass(wmiScope, new ManagementPath("WIN32_PROCESS"), new ObjectGetOptions());

                    ManagementBaseObject inParams = wmiProcess.GetMethodParameters("Create");
                    string arguments = "regedit.exe " + "/s \"" + uncFilePath + "\"";
                    //Fill in input parameter values
                    inParams["CommandLine"] = arguments;

                    //Execute the method
                    ManagementBaseObject outParams = wmiProcess.InvokeMethod("Create", inParams, null);
                    if (outParams != null && (uint) outParams["returnValue"] != 0)
                    {
                        throw new Exception("Error while starting process " + arguments + " - creation returned an exit code of " + outParams["returnValue"] + ". It was launched on " + remoteComputerName);
                    }
                    var sc = new ServiceController("Distributed Transaction Coordinator", remoteComputerName);
                    if (sc.Status == ServiceControllerStatus.Running || sc.Status == ServiceControllerStatus.Paused) sc.Stop();
                    sc.WaitForCompleteOperation();
                    sc.Start();
                    sc.WaitForCompleteOperation();

                    uint processId = 0;
                    if (outParams != null) processId = (uint) outParams["processId"];

                    var checkProcess = new SelectQuery("Select * from Win32_Process Where ProcessId = " + processId);
                    using (var processSearcher = new ManagementObjectSearcher(wmiScope, checkProcess))
                    {
                        using (processSearcher.Get())
                        {
                        }
                    }
                    var eventQuery = new WqlEventQuery("Win32_ProcessStopTrace");
                    int exitCode = 0;
                    bool eventArrived = false;
                    var resetEvent = new ManualResetEvent(false);
                    using (var eventWatcher = new ManagementEventWatcher(wmiScope, eventQuery))
                    {
                        eventWatcher.EventArrived += (sender, e) =>
                        {
                            if ((uint) e.NewEvent.Properties["ProcessId"].Value == processId)
                            {
                                Console.WriteLine("Process: {0}, Stopped with Code: {1}", (int) (uint) e.NewEvent.Properties["ProcessId"].Value, (int) (uint) e.NewEvent.Properties["ExitStatus"].Value);
                                exitCode = (int) (uint) e.NewEvent.Properties["ExitStatus"].Value;
                                eventArrived = true;
                                resetEvent.Set();
                            }
                        };
                        eventWatcher.Start();
                        if (!resetEvent.WaitOne(100, false))
                        {
                            eventWatcher.Stop();
                            eventArrived = false;
                        }
                        else
                            eventWatcher.Stop();
                    }
                    if (!eventArrived)
                    {
                        var selectQuery = new SelectQuery("Select * from Win32_Process Where ProcessId = " + processId);
                        using (var searcher = new ManagementObjectSearcher(wmiScope, selectQuery))
                        {
                            foreach (ManagementObject queryObject in searcher.Get())
                            {
                                queryObject.InvokeMethod("Terminate", null);
                                queryObject.Dispose();
                                Trace.TraceError(new Exception("Process " + arguments + " timed out and was killed on " + remoteComputerName).ToString());
                            }
                        }
                    }
                    else
                    {
                        if (exitCode != 0)
                            Trace.TraceError(new Exception("Process " + arguments + " exited with exit code " + exitCode + " on " + remoteComputerName).ToString());
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceWarning(new Exception("Error configuring MSDTC for {0}.".FormatWith(remoteComputerName), ex).ToString());
                }
                finally
                {
                    try
                    {
                        File.Delete(uncFilePath);
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceWarning(new Exception("Error cleaning up MSDTC for {0}.".FormatWith(remoteComputerName), ex).ToString()); 
                    }
                }
            }
        }
    }
}