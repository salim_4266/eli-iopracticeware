using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Soaf.Data;

namespace IO.Practiceware.DbMigration.Replication
{
    internal static class SqlReplicationUtilities
    {
        public static ReplicationPublisher ReplaceServer(this ReplicationPublisher publisher, ReplicationServerDetail replicationServerDetail)
        {
            publisher.ConnectionString = publisher.ConnectionString.ReplaceServer(replicationServerDetail.HubServerName);
            publisher.Server = replicationServerDetail.HubServerName;
            return publisher;
        }


        #region Hub-Spoke-Setup

        public static ReplicationServerDetail GetReplicationServerDetailInfo(string distributorConnectionString)
        {
            try
            {
                using (var distributorConnection = new SqlConnection(distributorConnectionString))
                {
                    distributorConnection.Open();
                    var hubServer = (QueryDistAgentMatchDb(distributorConnection) ?? QueryDistAgentVirtual(distributorConnection)) ?? QuerySnapshotAgent(distributorConnection);
                    if (String.IsNullOrEmpty(hubServer))
                        return null;
                    var hubSpokeServer = new ReplicationServerDetail();
                    hubSpokeServer.HubServerName = hubServer;

                    hubSpokeServer.SpokeServerNames = QuerySubscriberInfo(distributorConnection, hubSpokeServer.HubServerName);
                    return hubSpokeServer;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceWarning(ex.ToString());
                return null;
            }
        }

        private static string QueryDistAgentMatchDb(SqlConnection distributorConnection)
        {
            const string query = "SELECT id, name, publisher_db, publication FROM msdistribution_agents WHERE publisher_db = subscriber_db GROUP BY id, name, publisher_db, publication";
            var agentInfo = distributorConnection.Execute<DataTable>(query);
            return ParseForReplicationServerDetailInfo(agentInfo);
        }

        private static string QueryDistAgentVirtual(SqlConnection distributorConnection)
        {
            const string query = "SELECT TOP 1 id, name, publisher_db, publication FROM msdistribution_agents WHERE subscriber_db = 'virtual'";
            var agentInfo = distributorConnection.Execute<DataTable>(query);
            return ParseForReplicationServerDetailInfo(agentInfo);
        }

        private static string QuerySnapshotAgent(SqlConnection distributorConnection)
        {
            const string query = "SELECT TOP 1 id, name, publisher_db, publication FROM mssnapshot_agents ORDER BY id";
            var agentInfo = distributorConnection.Execute<DataTable>(query);
            return ParseForReplicationServerDetailInfo(agentInfo);
        }

        private static string ParseForReplicationServerDetailInfo(DataTable agentInfo)
        {
            if (agentInfo == null || agentInfo.Rows.Count == 0)
                return null;

            var servers = new Dictionary<string, int>();
            foreach (DataRow row in agentInfo.Rows)
            {
                var name = row.GetStringValue("name"); // e.g (a)IOP-DT117\HUB-AdventureWorks-PUBIT-IOP-DT117\SPOKEONE-3 (b)IOP-DT117\HUB-AdventureWorks-PUBIT--3 (c)IOP-DT117\HUB-AdventureWorks-PUBIT-3
                var database = row.GetStringValue("publisher_db"); // e.g AdventureWorks

                if (database.Length > 21)
                    database = database.Substring(0, 21);

                var endOfHubName = name.IndexOf("-" + database, StringComparison.Ordinal);
                if (endOfHubName == -1)
                    continue;
                var hubName = name.Substring(0, endOfHubName);

                if (servers.ContainsKey(hubName))
                    servers[hubName] = servers[hubName] + 1;
                else
                    servers.Add(hubName, 1);
            }
            if (servers.Count == 0)
                return null;

            var max = servers.Max(s => s.Value);
            return servers.First(s => s.Value == max).Key;
        }

        private static List<string> QuerySubscriberInfo(SqlConnection distributorConnection, string hubServer)
        {
            var query = String.Format("SELECT DISTINCT subscriber FROM mssubscriber_info WHERE publisher = '{0}' AND subscriber NOT IN ('{0}','{1}')", hubServer, distributorConnection.DataSource);
            var info = distributorConnection.Execute<DataTable>(query);
            return ParseForSpokeInfo(info);
        }

        private static List<string> ParseForSpokeInfo(DataTable info)
        {
            if (info == null || info.Rows.Count == 0)
                return null;
            var servers = info.Rows.Cast<DataRow>().Select(row => row.GetStringValue("subscriber")).ToList();
            return servers.Count == 0 ? null : servers;
        }

        #endregion
    }
}