using System.Collections.Generic;

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    /// Contains the hub and spoke server information from the distributor perspective 
    /// </summary>
    public class ReplicationServerDetail
    {
        /// <summary>
        /// The name of the hub server for replication
        /// </summary>
        public string HubServerName { get; set; }

        /// <summary>
        /// The names of the servers that are subscribers to the hub
        /// </summary>
        public List<string> SpokeServerNames { get; set; }
    }
}