using System;

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    /// Information about a replication error.
    /// </summary>
    public class ReplicationException : Exception
    {
        public ReplicationException(string message, Exception innerException = null)
            : base(message, innerException)
        {
        }
    }
}