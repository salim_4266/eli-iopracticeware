namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    /// Contains the distributor information
    /// </summary>
    public class ReplicationDistributor
    {
        public string ConnectionString { get; set; } // Connection string to hub server and distribution database (internally determined)
        public bool IsInstalled { get; set; }
        public string Server { get; set; }
        public bool IsDbInstalled { get; set; }
        public bool IsPublisher { get; set; }
        public bool HasRemotePublisher { get; set; }
        public ReplicationServerDetail ReplicationServerDetailSetup { get; set; }
    }
}