using System;

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    /// Contains an article information and the underlining database system object metadata
    /// </summary>
    public class ReplicationArticle : IEquatable<ReplicationArticle>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SourceOwner { get; set; }
        public string SourceObject { get; set; }
        public string DestinationObject { get; set; }
        public DbSysObj DatabaseSystemObject { get; set; }

        #region IEquatable<ReplicationArticle> Members

        public bool Equals(ReplicationArticle other)
        {
            return other != null && Name.Equals(other.Name, StringComparison.OrdinalIgnoreCase);
        }

        #endregion

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override bool Equals(Object obj)
        {
            var other = obj as ReplicationArticle;
            return other != null && Equals(other);
        }
    }
}