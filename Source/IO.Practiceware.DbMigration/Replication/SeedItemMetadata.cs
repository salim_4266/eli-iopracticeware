namespace IO.Practiceware.DbMigration.Replication
{
    public class SeedItemMetadata
    {
        public string Name { get; set; }
        public long Value { get; set; }
    }
}