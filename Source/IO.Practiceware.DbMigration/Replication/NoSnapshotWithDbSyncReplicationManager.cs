﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using IO.Practiceware.DbSync;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Collections;
using Soaf.Data;
using Soaf.Logging;

namespace IO.Practiceware.DbMigration.Replication
{
    public class NoSnapshotWithDbSyncReplicationManager
    {
        private readonly SqlReplicationConfiguration _sqlReplicationConfiguration;

        private readonly Func<IDbCommand> _createCommand;

        private static readonly IEnumerable<string> ObjectsToExclude = new []
        {
            "[dbo].[AuditEntries]",
            "[dbo].[AuditEntryChanges]",
            "[dbo].[LogEntries]",
            "[dbo].[TryConvertBigint]",
            "[dbo].[LogEntryLogCategory]",
            "[dbo].[LogEntryProperties]"
        };

        public NoSnapshotWithDbSyncReplicationManager(SqlReplicationConfiguration sqlReplicationConfiguration)
        {
            _sqlReplicationConfiguration = sqlReplicationConfiguration;
            _createCommand = () =>
                new SqlCommand
                {
                    Parameters = { new SqlParameter("objectsToInclude", _sqlReplicationConfiguration.ObjectsToReplicate
                        .Except(ObjectsToExclude)
                        .Join(",")) },
                    CommandTimeout = 3600
                };
        }

        /// <summary>
        /// Runs the DbSync to compare database sets and then generates a script which will synchronize them.
        /// </summary>
        public void RunDbSyncUtilityToSynchronizeDatabases()
        {
            CleanupSpokes(_sqlReplicationConfiguration.SpokeConnectionStrings);

            //  snapshot was not ran against subscriber spokes, so run dbSync                                                                    
            var hub = new SqlConnectionStringBuilder(_sqlReplicationConfiguration.HubConnectionString);
            var spokes = _sqlReplicationConfiguration.SpokeConnectionStrings.Select(s => new SqlConnectionStringBuilder(s)).ToArray();


            /* This MUST be done on the hub so that new tables that didn't previously have msrepl column get it BEFORE doing a schema
         * sync. Otherwise:
         * 1. No msrepl on table x
         * 2. Schema, data, non-table sync, still no column on table x
         * 3. Configure replication - adds msrepl to hub table x
         * 4. Spoke still doesn't have msrepl on table x
         *
         */
            using (new TimedScope(s => Trace.TraceInformation("Configured msrepl_tran_version on hub in {0}.".FormatWith(s))))
                new SqlConnection(hub.ToString()).RunScriptAndDispose(ReplicationResources.Configure_msrepl_tran_version, false, _createCommand);

            spokes.AsParallel().WithDegreeOfParallelism(10).ForAll(s =>
            {
                var spoke = s;

                SynchronizeTableSchema(_sqlReplicationConfiguration, hub, spoke);

                SynchronizeTableData(_sqlReplicationConfiguration, hub, spoke);

                SynchronizeNonTableObjects(_sqlReplicationConfiguration, hub, spoke);
            });
        }

        private void CleanupSpokes(IEnumerable<string> spokeConnectionStrings)
        {
            foreach (var cs in spokeConnectionStrings)
            {
                var spoke = cs;
                using (new TimedScope(v => Trace.TraceInformation("Cleaned up spoke {0} for replication in {1}.".FormatWith(spoke, v))))
                {
                    try
                    {
                        CleanupSpoke(spoke);
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceWarning(ex.ToString());
                    }
                }
            }
        }

        private void SynchronizeNonTableObjects(SqlReplicationConfiguration sqlReplicationConfiguration, SqlConnectionStringBuilder hub, SqlConnectionStringBuilder spoke)
        {
            var dbSynchronizer = new Synchronizer(new DbSyncParameters().GetNonTableObjectsSyncOnly());

            Trace.TraceInformation("Running DbSync to synchronize non-table objects...");
            dbSynchronizer.DbSyncParameters.Server1Name = hub.DataSource;
            dbSynchronizer.DbSyncParameters.Server2Name = spoke.DataSource;
            dbSynchronizer.DbSyncParameters.Db1Name = hub.InitialCatalog;
            dbSynchronizer.DbSyncParameters.Db2Name = spoke.InitialCatalog;
            dbSynchronizer.DbSyncParameters.DbSyncGeneratedScriptPath = Path.Combine(sqlReplicationConfiguration.DbSyncGeneratedScriptPath, "NonTableObjects");
            dbSynchronizer.DbSyncParameters.ExecuteDbSyncScriptAutomatically = sqlReplicationConfiguration.ExecuteDbSyncScriptAutomatically;
            dbSynchronizer.DbSyncParameters.TablesToInclude = sqlReplicationConfiguration.ObjectsToReplicate.ToList();
            using (new TimedScope(v => Trace.TraceInformation("Synchronized spoke {0} non-table objects in {1}.".FormatWith(spoke, v))))
                dbSynchronizer.Run();

            using (new TimedScope(v => Trace.TraceInformation("Set identity columns not for replication on spoke {0} in {1}.".FormatWith(spoke, v))))
                new SqlConnection(spoke.ToString()).RunScriptAndDispose(ReplicationResources.SetIdentityColumnsNotForReplication, false, _createCommand);
        }

        private static void SynchronizeTableData(SqlReplicationConfiguration sqlReplicationConfiguration, SqlConnectionStringBuilder hub, SqlConnectionStringBuilder spoke)
        {
            var dbSynchronizer = new Synchronizer(new DbSyncParameters().GetTableDataSyncOnly());

            Trace.TraceInformation("Running DbSync to synchronize table data...");
            dbSynchronizer.DbSyncParameters.Server1Name = hub.DataSource;
            dbSynchronizer.DbSyncParameters.Server2Name = spoke.DataSource;
            dbSynchronizer.DbSyncParameters.Db1Name = hub.InitialCatalog;
            dbSynchronizer.DbSyncParameters.Db2Name = spoke.InitialCatalog;
            dbSynchronizer.DbSyncParameters.DbSyncGeneratedScriptPath = Path.Combine(sqlReplicationConfiguration.DbSyncGeneratedScriptPath, "Data");
            FileSystem.TryDeleteDirectory(dbSynchronizer.DbSyncParameters.DbSyncGeneratedScriptPath);
            dbSynchronizer.DbSyncParameters.ExecuteDbSyncScriptAutomatically = false;
            dbSynchronizer.DbSyncParameters.Database1DataAlwaysWins = true;
            dbSynchronizer.DbSyncParameters.IgnoreMsreplComparison = true;
            dbSynchronizer.DbSyncParameters.TablesToInclude = sqlReplicationConfiguration.ObjectsToReplicate.ToList();

            using (new TimedScope(v => Trace.TraceInformation("Compared spoke {0} data for synchronization in {1}.".FormatWith(spoke, v))))
                dbSynchronizer.Run();

            if (Directory.Exists(dbSynchronizer.DbSyncParameters.DbSyncGeneratedScriptPath) && Directory.GetFiles(dbSynchronizer.DbSyncParameters.DbSyncGeneratedScriptPath, "*.sql", SearchOption.AllDirectories).Any()
                && sqlReplicationConfiguration.ExecuteDbSyncScriptAutomatically)
            {
                using (new TimedScope(v => Trace.TraceInformation("Synchronized spoke {0} data in {1}.".FormatWith(spoke, v))))
                    RunSqlCmdEx(spoke.ConnectionString, dbSynchronizer.DbSyncParameters.DbSyncGeneratedScriptPath, true);
            }
        }

        private void SynchronizeTableSchema(SqlReplicationConfiguration sqlReplicationConfiguration, SqlConnectionStringBuilder hub, SqlConnectionStringBuilder spoke)
        {
            /* This MUST be done on the spoke so that column order matches on the hub and spoke and tables aren't dropped and recreated
             * unnecessarily during schema sync. This could cause problems in terms of dependencies.
             * 1. msrepl exists on table x on hub with column order foo, bar, msrepl
             * 2. Table sync runs and order on the spoke is msrepl (but foo and bar don't exist yet).
             * 3. We need to drop msrepl to avoid doing a table rebuild.
             *
             */
            using (new TimedScope(v => Trace.TraceInformation("Dropped msrepl_tran_version on spoke {0} in {1}.".FormatWith(spoke, v))))
                new SqlConnection(spoke.ToString()).RunScriptAndDispose(ReplicationResources.Drop_msrepl_tran_version, false, _createCommand);

            using (new TimedScope(v => Trace.TraceInformation("Prepared spoke {0} for replication in {1}.".FormatWith(spoke, v))))
                new SqlConnection(spoke.ToString()).RunScriptAndDispose(ReplicationResources.PrepareSpokeSchemaForReplication, false, _createCommand);

            var dbSynchronizer = new Synchronizer(new DbSyncParameters().GetTableSchemaSyncOnly());

            Trace.TraceInformation("Running DbSync to synchronize table schema...");
            dbSynchronizer.DbSyncParameters.Server1Name = hub.DataSource;
            dbSynchronizer.DbSyncParameters.Server2Name = spoke.DataSource;
            dbSynchronizer.DbSyncParameters.Db1Name = hub.InitialCatalog;
            dbSynchronizer.DbSyncParameters.Db2Name = spoke.InitialCatalog;
            dbSynchronizer.DbSyncParameters.DbSyncGeneratedScriptPath = Path.Combine(sqlReplicationConfiguration.DbSyncGeneratedScriptPath, "Tables");
            FileSystem.TryDeleteDirectory(dbSynchronizer.DbSyncParameters.DbSyncGeneratedScriptPath);
            dbSynchronizer.DbSyncParameters.ExecuteDbSyncScriptAutomatically = false;
            dbSynchronizer.DbSyncParameters.TablesToInclude = sqlReplicationConfiguration.ObjectsToReplicate.ToList();

            using (new TimedScope(v => Trace.TraceInformation("Compared spoke {0} table schema in {1}.".FormatWith(spoke, v))))
                dbSynchronizer.Run();

            if (Directory.Exists(dbSynchronizer.DbSyncParameters.DbSyncGeneratedScriptPath) && Directory.GetFiles(dbSynchronizer.DbSyncParameters.DbSyncGeneratedScriptPath, "*.sql", SearchOption.AllDirectories).Any()
                && sqlReplicationConfiguration.ExecuteDbSyncScriptAutomatically)
            {
                foreach (var f in Directory.GetFiles(dbSynchronizer.DbSyncParameters.DbSyncGeneratedScriptPath))
                {
                    // remote unnecessary parts to improve script speed

                    var newText = Regex.Replace(File.ReadAllText(f), "CREATE (UNIQUE )?NONCLUSTERED INDEX(?<Name>.*?)[\n\r]+", e =>
                        string.Format("PRINT 'Skipping nonclustered index creation for{0}'" + Environment.NewLine, e.Groups["Name"].Value));

                    newText = Regex.Replace(newText, @"ALTER TABLE(?<Name>.*?)ADD\[msrepl_tran_version\] \[uniqueidentifier\] NOT NULL CONSTRAINT .*DEFAULT \(newid\(\)\)", e =>
                        @"PRINT 'Skipping msrepl_tran_version'");

                    File.WriteAllText(f, newText);
                }
                using (new TimedScope(v => Trace.TraceInformation("Synchronized spoke {0} table schema in {1}.".FormatWith(spoke, v))))
                    RunSqlCmdEx(spoke.ConnectionString, dbSynchronizer.DbSyncParameters.DbSyncGeneratedScriptPath, true);
            }

            using (new TimedScope(v => Trace.TraceInformation("Configured msrepl_tran_version on spoke {0} in {1}.".FormatWith(spoke, v))))
                new SqlConnection(spoke.ToString()).RunScriptAndDispose(ReplicationResources.Configure_msrepl_tran_version, false, _createCommand);

        }

        private static void RunSqlCmdEx(string connectionString, string scriptPath, bool deploy)
        {
            File.WriteAllBytes("SqlCmdEx.exe", ReplicationResources.SqlCmdEx);

            var arguments = string.Format(@"""{0}"" ""{1}"" ""{2}""", connectionString, scriptPath, deploy ? "deploy" : String.Empty);

            var exitCode = Processes.RunProcessAndWaitForExit("SqlCmdEx", arguments, s => Trace.TraceInformation(s), s => Trace.TraceInformation(s));

            if (exitCode != 0)
                throw new Exception("Error running SqlCmdEx:\r\n{0}\r\nCheck log file for additional information.");

        }


        /// <summary>
        /// Drops obsolete destination tables
        /// </summary>
        /// <param name="spoke">The spoke.</param>
        private static void CleanupSpoke(string spoke)
        {
            using (var c = new SqlConnection(spoke))
            {
                c.RunScript(ReplicationResources.CleanupSpokes);
            }
        }

    }
}