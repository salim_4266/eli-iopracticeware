using System;
using System.Collections.Generic;

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    ///   Manages Sql Replication activities.
    /// </summary>
    public interface ISqlReplicationManager
    {
        /// <summary>
        ///   Gets the SQL replication arguments from a hub.
        /// </summary>
        /// <param name="hubConnectionString"> The connection string to hub. </param>
        /// <returns>A Tuple of ReplicatedObject names and Spoke connection strings</returns>
        Tuple<IEnumerable<string>, IEnumerable<string>> GetReplicatedObjectsAndSpokeConnectionStringsFromHubServer(string hubConnectionString);

        /// <summary>
        ///   Deletes replication publications and subscriptions based on the specified arguments.
        /// </summary>
        /// <param name="arguments"> The arguments. </param>
        /// <param name="forceDeleteIfReplicating"> Drop replication regardless of state (default = false) </param>
        void Delete(SqlReplicationConfiguration arguments, bool forceDeleteIfReplicating = false);

        /// <summary>
        ///   Creates and configures replication (including distributor, publications and subscriptions) using the specified arguments. Will create distribution database and agents if necessary. If publications/subscriptions already exist, this method will reconfigure so replication configuration matches ObjectsToReplicate specified in the arguments. If publications/subscriptions exist for only some of the ReplicationPartners, this method will add/remove publishers/subscribers so replication configuration matches the ReplicationPartners specified in the arguments.
        /// </summary>
        /// <param name="arguments"> The arguments. </param>
        /// <param name="seeding"> The seeding configuration </param>
        void Configure(SqlReplicationConfiguration arguments, Seeding seeding = null);

        /// <summary>
        /// Loads the seed metadata.
        /// </summary>
        /// <param name="sqlReplicationConfiguration">The SQL replication configuration.</param>
        /// <param name="seeding">The seeding.</param>
        /// <param name="forceFromDb">if set to <c>true</c> [force from database].</param>
        /// <returns></returns>
        IEnumerable<SeedMetadata> LoadSeedMetadata(SqlReplicationConfiguration sqlReplicationConfiguration, Seeding seeding = null, bool forceFromDb = false);       
    }
}