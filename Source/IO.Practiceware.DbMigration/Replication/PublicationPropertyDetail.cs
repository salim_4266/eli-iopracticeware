using System;
using System.Collections.Generic;
using System.Linq;
using Soaf;

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    /// A state object which represents the configurable attributes of a Sql Server publication. 
    /// </summary>
    public class PublicationPropertyDetail
    {
        /// <summary>
        /// Initializes a new instance of <see cref="PublicationPropertyDetail"/>
        /// and sets the <paramref name="publicationName"/>
        /// </summary>
        /// <param name="publicationName">The name of the publication. It is required and must be unique</param>
        public PublicationPropertyDetail(string publicationName)
        {
            if (publicationName.IsNullOrEmpty())
                throw new ArgumentException("The publication name is required", "publicationName");

            PublicationName = publicationName;
        }

        #region Sql Server Publication Properties

        /// <summary>
        /// he name of the publication to create. must be unique with the db
        /// </summary>
        public string PublicationName { get; private set; }

        /// <summary>
        /// the synchronization mode. native, character, or concurrent (does not lock tables)
        /// </summary>
        public string SynchronizationMethod { get; set; }

        /// <summary>
        /// the type of replication frequency. continuous, or snapshot
        /// </summary>
        public string ReplicationFrequency { get; set; }

        /// <summary>
        /// Specifies if publication data is available. active or inactive (default) => Publication data is not available for 
        /// Subscribers when the publication is first created (they can subscribe, but the subscriptions 
        /// are not processed).
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Specifies if there is a stand-alone Distribution Agent for this publication.  
        /// If true, there is a stand-alone Distribution Agent for this publication. If false, the 
        /// publication uses a shared Distribution Agent, and each Publisher database/Subscriber database
        /// pair has a single, shared Agent        
        /// </summary>
        public bool CreateIndependentAgent { get; set; }

        /// <summary>
        /// Specifies if the synchronization files for the publication are created each time the Snapshot Agent runs.
        ///  If true, the synchronization files are created or re-created each time the Snapshot Agent runs
        ///  Subscribers are able to get the synchronization files immediately if the Snapshot Agent has completed before the subscription is created
        /// </summary>
        public bool ImmediateSynchronization { get; set; }

        /// <summary>
        /// Allow the publication to push changes to the subscribers
        /// </summary>
        public bool AllowPush { get; set; }

        /// <summary>
        /// Allow the subscribers to pull changes from a publication
        /// </summary>
        public bool AllowPull { get; set; }

        /// <summary>
        /// Specifies if anonymous subscriptions can be created for the given publication
        ///  If false, changes at the Subscriber are not queued. */                      
        /// </summary>
        public bool AllowAnonymous { get; set; }

        /// <summary>
        /// Enables or disables queuing of changes at the Subscriber until they can be applied at the Publisher
        /// </summary>
        public bool AllowQueuedTransactions { get; set; }

        /// <summary>
        /// Specifies if immediate-updating subscriptions are allowed on the publication. Default is False
        /// </summary>
        public bool AllowSynchronizedTransactions { get; set; }

        /// <summary>
        /// Specifies if the synchronizing stored procedure for updating subscriptions is generated at the Publisher
        /// The user supplied value for autogen_sync_procs will be overridden depending on the values specified for allow_queued_tran and allow_sync_tran        
        /// </summary>
        public bool AutoGenerateSynchronizationProcedure { get; set; }

        /// <summary>
        /// the retention period in hours for subscription activity. If 0, well-known subscriptions 
        /// to the publication will never expire and be removed by the Expired Subscription Cleanup Agent
        /// </summary>
        public int RetentionHours { get; set; }

        /// <summary>
        /// Specifies that the publication allows data transformations 
        /// </summary>
        public bool AllowDts { get; set; }

        /// <summary>
        /// Specifies the conflict resolution policy followed when the queued updating subscriber option is used
        /// Possible Values = 'pub wins', 'sub reinit' (reinitialize the subscription), 'sub wins' (subscriber wins conflict) 
        /// </summary>
        public string ConflictPolicy { get; set; }

        /// <summary>
        /// Whether or not the conflict records are stored on the publisher
        /// </summary>
        public bool ShouldCentralizeConflicts { get; set; }

        /// <summary>
        /// Which type of queue is used. Possible Values = 'sql' (sql server queue), NULL (defaults to sql)
        /// </summary>
        public string QueueType { get; set; }

        /// <summary>
        /// Indicates if Subscribers can initialize a subscription to this publication from a backup rather than an initial snapshot
        /// See Link: http://technet.microsoft.com/en-us/library/ms151705.aspx
        /// To avoid missing subscriber data, when using sp_addpublication with @allow_initialize_from_backup = N'true', always use @immediate_sync = N'true'
        /// </summary>
        public bool AllowInitializationFromBackup { get; set; }

        /// <summary>
        /// the name of an existing agent job. This parameter is only specified when the Log Reader Agent will use an existing job instead of a new one being created
        /// </summary>
        public string LogReaderJobName { get; set; }

        /// <summary>
        /// Specifies if snapshot files are stored in the default folder. You can also save the snapshot 
        /// files to an FTP site, for retrieval by the Subscriber at a later time. Note that this
        /// parameter can be true and still have a location in the @alt_snapshot_folder parameter. 
        /// This combination specifies that the snapshot files will be stored in both the default 
        /// and alternate locations        
        /// </summary>
        public bool StoreSnapshotInDefaultFolder { get; set; }

        /// <summary>
        /// the location of the alternate folder for the snapshot
        /// </summary>
        public string AlternateSnapshotFolderPath { get; set; }

        /// <summary>
        /// Distribution Agent will run the pre-snapshot script before running any of
        /// the replicated object scripts when applying a snapshot at a Subscriber 
        /// </summary>
        public string PresnapshotScriptPath { get; set; }

        /// <summary>
        /// Distribution Agent will run the post-snapshot script after all the other 
        /// replicated object scripts and data have been applied during an initial 
        /// synchronization         
        /// </summary>
        public string PostSnapshotScriptPath { get; set; }

        /// <summary>
        /// Indicates if schema replication is supported for the publication. default of true for SQL Server Publishers 
        /// and false for non-SQL Server Publishers. If true, data definition language (DDL) statements executed 
        /// at the publisher are replicated, and if false, DDL statements are not replicated    
        /// </summary>
        public bool ReplicateDdlStatements { get; set; }

        /// <summary>
        /// Indicates whether to use compression for the snapshot        
        /// </summary>
        public bool CompressSnapshot { get; set; }

        #endregion

        #region Default Properties

        /// <summary>
        /// The setup parameters used when creating a publication on the publishing server. 
        /// Used to specify the type, behavior, and frequency of the publication when
        /// the 'sp_addpublication' stored procedure         
        /// 
        /// If multiple publications exist that publish the same database object, only publications with a replicate_ddl value of 1 will replicate ALTER TABLE, ALTER VIEW, ALTER PROCEDURE, ALTER FUNCTION, and ALTER TRIGGER DDL statements. However, an ALTER TABLE DROP COLUMN DDL statement will be replicated by all publications that are publishing the dropped column.
        /// With DDL replication enabled (replicate_ddl = 1) for a publication, in order to make non-replicating DDL changes to the publication, sp_changepublication must first be executed to set replicate_ddl to 0. After the non-replicating DDL statements have been issued, sp_changepublication can be run again to turn DDL replication back on. 
        /// </summary>
        private static readonly IDictionary<string, object> DefaultPublicationProperties = new Dictionary<string, object>
                                                                                            {
                                                                                                {"publication", ""},             /* the name of the publication to create. must be unique with the db */
                                                                                                {"sync_method", "concurrent"},   /* the synchronization mode. native, character, or concurrent (does not lock tables) */
                                                                                                {"repl_freq", "continuous"},     /* the type of replication frequency. continuous, or snapshot. */

                                                                                                {"status", "active"},            /* Specifies if publication data is available. active or inactive (default) => Publication data is not available for 
                                                                                                                                * Subscribers when the publication is first created (they can subscribe, but the subscriptions 
                                                                                                                                * are not processed).*/

                                                                                                {"independent_agent", "true"},   /* Specifies if there is a stand-alone Distribution Agent for this publication.  
                                                                                                                                  * If true, there is a stand-alone Distribution Agent for this publication. If false, the 
                                                                                                                                  * publication uses a shared Distribution Agent, and each Publisher database/Subscriber database 
                                                                                                                                  * pair has a single, shared Agent.*/

                                                                                                {"immediate_sync", "true"},      /* Specifies if the synchronization files for the publication are created each time the Snapshot Agent runs.
                                                                                                                                  *  If true, the synchronization files are created or re-created each time the Snapshot Agent runs
                                                                                                                                  *  Subscribers are able to get the synchronization files immediately if the Snapshot Agent has completed before the subscription is created
                                                                                                                                  * 
                                                                                                                                  * independent_agent must be true for immediate_synchronization to be true. If false, the synchronization files are created only if there are new subscriptions.*/

                                                                                                {"enabled_for_internet", "false"}, /*  determines if file transfer protocol (FTP) can be used to transfer the snapshot files to a subscriber. 
                                                                                                                                    * If true, the synchronization files for the publication are put into the C:\Program Files\Microsoft SQL Server\MSSQL\MSSQL.x\Repldata\Ftp directory. The user must create the Ftp directory*/
                                            
                                                                                                {"allow_push", "true"},          /* */
                                                                                                {"allow_pull", "true"},          /* */
                                                                                                {"allow_anonymous", "true"},     /* Specifies if anonymous subscriptions can be created for the given publication 
                                                                                                                                 *  If false, changes at the Subscriber are not queued. */                                            
                                            
                                            
                                            
                                                                                                {"allow_queued_tran", "true"},   /* Enables or disables queuing of changes at the Subscriber until they can be applied at the Publisher */
                                                                                                {"allow_sync_tran", "true"},     /* Specifies if immediate-updating subscriptions are allowed on the publication. Default is False */
                                                                                                {"autogen_sync_procs", "true"},  /* Specifies if the synchronizing stored procedure for updating subscriptions is generated at the Publisher
                                                                                                                                * The user supplied value for autogen_sync_procs will be overridden depending on the values specified for allow_queued_tran and allow_sync_tran */
                                            
                                                                                                {"retention", 0},                /* the retention period in hours for subscription activity. If 0, well-known subscriptions 
                                                                                                                                    * to the publication will never expire and be removed by the Expired Subscription Cleanup Agent */

                                                                                                {"allow_dts", "false"},          /* Specifies that the publication allows data transformations */

                                                                                                {"conflict_policy", "pub wins"}, /* Specifies the conflict resolution policy followed when the queued updating subscriber option is used
                                                                                                                                        * Possible Values = 'pub wins', 'sub reinit' (reinitialize the subscription), 'sub wins' (subscriber wins conflict) */

                                                                                                {"centralized_conflicts", "true"}, /* Whether or not the conflict records are stored on the publisher */

                                                                                                {"queue_type", "sql"},             /* Which type of queue is used. Possible Values = 'sql' (sql server queue), NULL (defaults to sql) */

                                                                                                {"replicate_ddl", 1},              /* Indicates if schema replication is supported for the publication. default of 1 for SQL Server Publishers 
                                                                                                                                    * and 0 for non-SQL Server Publishers. 1 indicates that data definition language (DDL) statements executed 
                                                                                                                                    * at the publisher are replicated, and 0 indicates that DDL statements are not replicated  */

                                                                                                {"allow_initialize_from_backup", "false"}, /* Indicates if Subscribers can initialize a subscription to this publication from a backup rather than an initial snapshot
                                                                                                                                            * See Link: http://technet.microsoft.com/en-us/library/ms151705.aspx
                                                                                                                                            * To avoid missing subscriber data, when using sp_addpublication with @allow_initialize_from_backup = N'true', always use @immediate_sync = N'true'*/

                                                                                                {"logreader_job_name", "" },  /* the name of an existing agent job. This parameter is only specified when the Log Reader Agent will use an existing job instead of a new one being created*/

                                                                                                {"snapshot_in_defaultfolder", "false"},    /* Specifies if snapshot files are stored in the default folder. You can also save the snapshot 
                                                                                                                                            * files to an FTP site, for retrieval by the Subscriber at a later time. Note that this
                                                                                                                                            * parameter can be true and still have a location in the @alt_snapshot_folder parameter. 
                                                                                                                                            * This combination specifies that the snapshot files will be stored in both the default 
                                                                                                                                            * and alternate locations
                                                                                                                                            * */

                                                                                                {"alt_snapshot_folder", ""},     /* the location of the alternate folder for the snapshot. */

                                                                                                {"pre_snapshot_script", "" },    /* Distribution Agent will run the pre-snapshot script before running any of 
                                                                                                                                  * the replicated object scripts when applying a snapshot at a Subscriber */

                                                                                                {"post_snapshot_script", "" },   /* Distribution Agent will run the post-snapshot script after all the other 
                                                                                                                                  * replicated object scripts and data have been applied during an initial 
                                                                                                                                  * synchronization */
                                            
                                                                                                {"compress_snapshot", "false"},   /* Specifies that the snapshot that is written to the @alt_snapshot_folder location is 
                                                                                                                                    * to be compressed into the Microsoft CAB format. Snapshot files larger than 2 GB cannot be compressed */

                                                                                                {"ftp_address", "false"},
                                                                                                {"ftp_port", "false"},
                                                                                                {"ftp_subdirectory", "false"},
                                                                                                {"ftp_login", "false"},
                                                                                                {"ftp_password", "false"}                                                                                             
                                                                                            };




        #endregion

        #region State Transformation

        /// <summary>
        /// Returns a dictionary of key value pairs representing the state of the object in the format which sql server expects when executing the 
        /// stored procedure to create publications.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> ToSqlStoredProcedureParameters()
        {
            return new Dictionary<string, object>
                   {
                       {"publication", PublicationName},
                       {"sync_method", SynchronizationMethod},
                       {"repl_freq", ReplicationFrequency},
                       {"status", Status},
                       {"independent_agent", CreateIndependentAgent.ToString().ToLower()},
                       {"immediate_sync", ImmediateSynchronization.ToString().ToLower()},
                       {"allow_push", AllowPush.ToString().ToLower()},
                       {"allow_pull", AllowPull.ToString().ToLower()},
                       {"allow_anonymous", AllowAnonymous.ToString().ToLower()},
                       {"allow_queued_tran", AllowQueuedTransactions.ToString().ToLower()},
                       {"allow_sync_tran", AllowSynchronizedTransactions.ToString().ToLower()},
                       {"autogen_sync_procs", AutoGenerateSynchronizationProcedure.ToString().ToLower()},
                       {"retention", RetentionHours},
                       {"allow_dts", AllowDts.ToString().ToLower()},
                       {"conflict_policy", ConflictPolicy},
                       {"centralized_conflicts", ShouldCentralizeConflicts.ToString().ToLower()},
                       {"queue_type", QueueType},
                       {"replicate_ddl", ReplicateDdlStatements ? 1 : 0},
                       {"allow_initialize_from_backup", AllowInitializationFromBackup.ToString().ToLower()},
                       {"logreader_job_name", LogReaderJobName},
                       {"snapshot_in_defaultfolder", StoreSnapshotInDefaultFolder.ToString().ToLower()},
                       {"alt_snapshot_folder", AlternateSnapshotFolderPath},
                       {"pre_snapshot_script", PresnapshotScriptPath},
                       {"post_snapshot_script", PostSnapshotScriptPath},          
                       {"compress_snapshot", CompressSnapshot.ToString().ToLower()},
                   };
        }

        /// <summary>
        /// Sets the internal state of the object based on the parameter dictionary.
        /// </summary>
        private void SetStateFromParameters(IDictionary<string, object> sqlParameters)
        {
            var unsupportedProperty = sqlParameters.FirstOrDefault(p => !DefaultPublicationProperties.Keys.Contains(p.Key)).IfNotDefault(p => p.Key);
            if (unsupportedProperty != null)
            {
                throw new ArgumentException("The {0} property specified is not a supported sql publication property".FormatWith(unsupportedProperty), "sqlParameters");
            }

            if (sqlParameters.ContainsKey("sync_method"))
                SynchronizationMethod = sqlParameters["sync_method"].ToString();
            if (sqlParameters.ContainsKey("repl_freq"))
                ReplicationFrequency = sqlParameters["repl_freq"].ToString();
            if (sqlParameters.ContainsKey("status"))
                Status = sqlParameters["status"].ToString();
            if (sqlParameters.ContainsKey("independent_agent"))
                CreateIndependentAgent = sqlParameters["independent_agent"].ToBool();
            if (sqlParameters.ContainsKey("immediate_sync"))
                ImmediateSynchronization = sqlParameters["immediate_sync"].ToBool();
            if (sqlParameters.ContainsKey("allow_push"))
                AllowPush = sqlParameters["allow_push"].ToBool();
            if (sqlParameters.ContainsKey("allow_pull"))
                AllowPull = sqlParameters["allow_pull"].ToBool();
            if (sqlParameters.ContainsKey("allow_anonymous"))
                AllowAnonymous = sqlParameters["allow_anonymous"].ToBool();
            if (sqlParameters.ContainsKey("allow_queued_tran"))
                AllowQueuedTransactions = sqlParameters["allow_queued_tran"].ToBool();
            if (sqlParameters.ContainsKey("allow_sync_tran"))
                AllowSynchronizedTransactions = sqlParameters["allow_sync_tran"].ToBool();
            if (sqlParameters.ContainsKey("autogen_sync_procs"))
                AutoGenerateSynchronizationProcedure = sqlParameters["autogen_sync_procs"].ToBool();
            if (sqlParameters.ContainsKey("retention"))
                RetentionHours = sqlParameters["retention"].CastTo<int>();
            if (sqlParameters.ContainsKey("allow_dts"))
                AllowDts = sqlParameters["allow_dts"].ToBool();
            if (sqlParameters.ContainsKey("conflict_policy"))
                ConflictPolicy = sqlParameters["conflict_policy"].ToString();
            if (sqlParameters.ContainsKey("centralized_conflicts"))
                ShouldCentralizeConflicts = sqlParameters["centralized_conflicts"].ToBool();
            if (sqlParameters.ContainsKey("queue_type"))
                QueueType = sqlParameters["queue_type"].ToString();
            if (sqlParameters.ContainsKey("replicate_ddl"))
                ReplicateDdlStatements = sqlParameters["replicate_ddl"].ToBool();
            if (sqlParameters.ContainsKey("allow_initialize_from_backup"))
                AllowInitializationFromBackup = sqlParameters["allow_initialize_from_backup"].ToBool();
            if (sqlParameters.ContainsKey("logreader_job_name"))
                LogReaderJobName = sqlParameters["logreader_job_name"].ToString();
            if (sqlParameters.ContainsKey("snapshot_in_defaultfolder"))
                StoreSnapshotInDefaultFolder = sqlParameters["snapshot_in_defaultfolder"].ToBool();
            if (sqlParameters.ContainsKey("alt_snapshot_folder"))
                AlternateSnapshotFolderPath = sqlParameters["alt_snapshot_folder"].ToString();
            if (sqlParameters.ContainsKey("pre_snapshot_script"))
                PresnapshotScriptPath = sqlParameters["pre_snapshot_script"].ToString();
            if (sqlParameters.ContainsKey("post_snapshot_script"))
                PostSnapshotScriptPath = sqlParameters["post_snapshot_script"].ToString();
            if (sqlParameters.ContainsKey("compress_snapshot") && string.Equals("false", sqlParameters["post_snapshot_script"] as string, StringComparison.OrdinalIgnoreCase))
                CompressSnapshot = true;
        }

        #endregion

        #region Factory Methods

        /// <summary>        
        /// Create a <see cref="PublicationPropertyDetail"/> state object with default configuration
        /// set to 'Continuous' replication.
        /// </summary>
        /// <param name="publicationName">The name of the publication. Required. Must be unique</param>        
        /// <returns></returns>
        public static PublicationPropertyDetail CreateContinuousConfiguration(string publicationName)
        {
            var detail = new PublicationPropertyDetail(publicationName);
            detail.SetStateFromParameters(DefaultPublicationProperties);

            return detail;
        }

        /// <summary>        
        /// Create a <see cref="PublicationPropertyDetail"/> state object with default configuration
        /// set to 'Continuous' replication.
        /// </summary>
        /// <param name="publicationName">The name of the publication. Required. Must be unique</param>      
        /// <param name="useSnapshotForInitialReplicationRollout">Specify True to configure replication to use the immediate initial snapshot (created on first run) against all the subscribers. False otherwise.</param>  
        /// <returns></returns>
        public static PublicationPropertyDetail CreateContinuousConfiguration(string publicationName, bool useSnapshotForInitialReplicationRollout)
        {
            var detail = new PublicationPropertyDetail(publicationName);
            detail.SetStateFromParameters(DefaultPublicationProperties);

            if (useSnapshotForInitialReplicationRollout)
            {
                detail.ImmediateSynchronization = true;
                detail.AllowAnonymous = true;
            }
            else
            {
                detail.ImmediateSynchronization = false;
                detail.AllowAnonymous = false;
            }

            return detail;
        }

        #endregion
    }

    internal static class PublicationPropertyExtensions
    {
        public static bool ToBool(this object objValue)
        {
            if (objValue == null)
                return false;

            var boolString = objValue.ToString();

            if (boolString.Equals("true", StringComparison.OrdinalIgnoreCase) || boolString.Equals("1", StringComparison.OrdinalIgnoreCase))
                return true;

            if (boolString.Equals("false", StringComparison.OrdinalIgnoreCase) || boolString.Equals("0", StringComparison.OrdinalIgnoreCase))
                return false;

            throw new InvalidCastException();
        }
    }

}