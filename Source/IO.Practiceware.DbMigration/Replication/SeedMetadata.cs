using System.Collections.Generic;

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    /// Wrapping the seeding metadata to be applied at database
    /// </summary>
    public class SeedMetadata
    {
        public Seeding ReferrerSeeding { get; set; }
        public string ConnectionString { get; set; } // Connection to server and database (external datapoint)
        public string Server { get; set; }
        public string Database { get; set; }
        public int ServerIndex { get; set; }
        public List<SeedItemMetadata> SeedList { get; set; }
    }
}