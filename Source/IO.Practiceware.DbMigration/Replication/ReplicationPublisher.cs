using System.Collections.Generic;

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    /// Contains the publisher information including the list of replicated articles and subscribers
    /// </summary>
    public class ReplicationPublisher
    {
        public string ConnectionString { get; set; } // Connection string to hub server and database (internally determined)
        public string Name { get; set; }
        public string Server { get; set; }
        public string Database { get; set; }
        public List<ReplicationArticle> ArticleList { get; set; }
        public List<ReplicationSubscriber> SubscriberList { get; set; }
    }
}