using System.Xml.Serialization;

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    /// Provides a per object override of the default step range value
    /// </summary>
    public class SeededObject
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("StepRange")]
        public long StepRange { get; set; }
    }
}