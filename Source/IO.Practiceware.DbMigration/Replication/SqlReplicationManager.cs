﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using IO.Practiceware.DbMigration.Replication;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Logging;

[assembly: Component(typeof(SqlReplicationManager), typeof(ISqlReplicationManager))]

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    ///   Default implementation of an ISqlReplicationManager,
    /// </summary>
    public class SqlReplicationManager : ISqlReplicationManager
    {
        private readonly string _now = DateTime.Now.ToString("yyyy.MM.dd_HH.mm.ss");

        #region Constant Values

        // Query to get system database object information or list of system database objects information
        private const string DatabaseSystemObjectQuery =
                                                        @"SELECT o.name objectname, o.object_id, o.parent_object_id, o.type, o.type_desc, o.create_date, o.modify_date, 
                                                        o.is_ms_shipped, o.is_published, o.is_schema_published, s.name schemaname, s.schema_id, 
                                                        OBJECTPROPERTY(o.object_id, 'IsIndexed') IsIndexed, 
                                                        OBJECTPROPERTY(o.object_id, 'TableHasIdentity') AS TableHasIdentity,
                                                        OBJECTPROPERTY(o.object_id, 'IsSchemaBound') IsSchemaBound, 
                                                        CASE WHEN OBJECTPROPERTY(o.object_id, 'TableHasIdentity') = 1 THEN IDENT_CURRENT((s.name + '.' +  o.name)) ELSE NULL END AS CurrentSeedValue,
                                                        (SELECT TOP 1 c.name FROM sys.columns c WHERE is_identity = 1 AND object_id = o.object_id) AS IdentityColumnName,
                                                        OBJECT_DEFINITION(o.object_id) AS Definition
                                                        FROM sys.objects(NOLOCK) o INNER JOIN sys.schemas(NOLOCK) s ON o.schema_id = s.schema_id 
                                                        WHERE o.object_id ";

        private const int MaxDistributionDatabaseRetentionPeriod = 672;

        #endregion

        #region ISqlReplicationManager Members

        /// <summary>
        /// Get the replication metadata from a hub data source. Returns a tuple representing a collection 
        /// of replicated objects (item1) and a collection of spoke server connection strings (item2)
        /// </summary>
        /// <param name="hubConnectionString">The hub connection string</param>        
        /// <returns>A tuple representing a collection of replicated objects (item1) and a collection of spoke server connection strings (item2)</returns>
        public Tuple<IEnumerable<string>, IEnumerable<string>> GetReplicatedObjectsAndSpokeConnectionStringsFromHubServer(string hubConnectionString)
        {
            var hubServerMasterDatabaseConnectionString = hubConnectionString.ToMasterDatabaseConnection().OpenAndReturn();

            try
            {
                var publisherServer = hubConnectionString.GetServer();
                var publisherDatabase = hubConnectionString.GetDatabase();

                // Get current replication world
                ReplicationDistributor distributor;
                ReplicationPublisher publisher;

                if (!GetReplicationInfo(publisherServer, publisherDatabase, hubServerMasterDatabaseConnectionString, out distributor, out publisher))
                    return distributor == null ? null : Tuple.Create(Enumerable.Empty<string>(), Enumerable.Empty<string>());

                if (publisher != null)
                {
                    // Add articles
                    List<string> replicatedObjects = null;
                    if (publisher.ArticleList != null)
                        replicatedObjects = publisher.ArticleList.Select(a => a.Name).ToList();

                    // Add partners
                    List<string> spokeConnectionStrings = null;
                    if (publisher.SubscriberList != null)
                        spokeConnectionStrings = publisher.SubscriberList.Select(s => s.ConnectionString).ToList();

                    return new Tuple<IEnumerable<string>, IEnumerable<string>>(replicatedObjects, spokeConnectionStrings);
                }
            }
            finally
            {
                if (hubServerMasterDatabaseConnectionString != null)
                    hubServerMasterDatabaseConnectionString.Dispose();
            }

            return null;
        }

        /// <summary>
        /// Remove replication from a set of servers as defined by arguments
        /// </summary>
        /// <param name="arguments">The set of servers and articles to remove</param>
        /// <param name="forceDeleteIfReplicating">Drop replication regardless of state (default = false)</param>
        public void Delete(SqlReplicationConfiguration arguments, bool forceDeleteIfReplicating = false)
        {
            if (!IsValidDeleteArguments(arguments))
                return;
            var connHubServerMaster = arguments.HubConnectionString.ToMasterDatabaseConnection().OpenAndReturn();

            try
            {
                var publisherServer = arguments.HubConnectionString.GetServer();
                string publisherDatabase = arguments.HubConnectionString.GetDatabase();

                // Get current replication world
                ReplicationDistributor distributor;
                ReplicationPublisher publisher;
                GetReplicationInfo(publisherServer, publisherDatabase, connHubServerMaster, out distributor, out publisher);

                if (distributor == null) return;

                if (publisher != null)
                {
                    if (!forceDeleteIfReplicating && !IsCompletedAllReplication(distributor, publisher))
                    {
                        Trace.TraceWarning("Cannot complete operation. There are replication jobs running");
                        return;
                    }

                    // Augment with actual replicate items and note current state
                    var existingSubscribers = AugmentSysObjReplicationSubscribers(publisher.ArticleList, publisher.SubscriberList);

                    // Delete replication partners
                    if (existingSubscribers != null)
                    {
                        RemoveReplicationPartners(existingSubscribers.Select(s => s.ConnectionString).ToArray(), connHubServerMaster, publisher);
                    }


                    try
                    {
                        RemovePublishingInfo(connHubServerMaster, publisher);
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceWarning("Could not removing publishing info. {0}".FormatWith(ex.ToString()));
                        RemoveDistributorInfo(connHubServerMaster);
                        return;
                    }
                }

                if (!IsValidDistributor(distributor)) RemoveDistributorInfo(connHubServerMaster);
            }
            finally
            {
                if (connHubServerMaster != null)
                    connHubServerMaster.Dispose();
            }
        }

        /// <summary>
        /// Adds replication to a set of servers, either a new setup or modifying an existing, seeding optional
        /// </summary>
        /// <param name="sqlReplicationConfiguration">The set of servers and articles to remove</param>
        /// <param name="seeding">The default step range value to use for seeding and any overrides per object</param>
        /// <remarks>
        /// Checks to see if there is a valid distributor setup. If not, it will create a new replication environment 
        /// otherwise it will re-configure an existing one (setting up publication and subscriptions and creating an initial snapshot)
        /// </remarks>
        public void Configure(SqlReplicationConfiguration sqlReplicationConfiguration, Seeding seeding = null)
        {
            if (!IsValidConfigureArguments(sqlReplicationConfiguration))
                throw new ArgumentException("SqlReplicationArguments Is Invalid");

            if (!IsValidSeeding(seeding))
                throw new ArgumentException("Seeding Is Invalid");

            var seedMetadata = (sqlReplicationConfiguration.SeedMetadata ?? LoadSeedMetadata(sqlReplicationConfiguration, seeding)).ToList();

            var connHubServerMaster = sqlReplicationConfiguration.HubConnectionString.ToMasterDatabaseConnection().OpenAndReturn();

            try
            {
                var publisherServer = sqlReplicationConfiguration.HubConnectionString.GetServer();
                var publisherDatabase = sqlReplicationConfiguration.HubConnectionString.GetDatabase();

                if (Networking.IsInDomain())
                {
                    var hubMachineAccount = string.Format(@"{0}\{1}$", Networking.GetMachineNetBiosDomain(), publisherServer.Split('\\')[0]);
                    ConfigureDatabasePermissions(hubMachineAccount, sqlReplicationConfiguration.HubConnectionString);
                    foreach (var spoke in sqlReplicationConfiguration.SpokeConnectionStrings)
                    {
                        ConfigureDatabasePermissions(hubMachineAccount, spoke);
                    }
                }

                var distributor = GetDistributorInfo(connHubServerMaster, publisherServer);
                sqlReplicationConfiguration.DistributorPreviouslyExistedAndIsValid = IsValidDistributor(distributor);
                if (!sqlReplicationConfiguration.DistributorPreviouslyExistedAndIsValid)
                {
                    ConfigureNew(sqlReplicationConfiguration, connHubServerMaster, seedMetadata, publisherServer, publisherDatabase);
                }
                else
                {
                    ConfigureExisting(connHubServerMaster, distributor, publisherDatabase, publisherServer, seedMetadata, sqlReplicationConfiguration);
                }

                CleanupSeedMetadata();
            }
            finally
            {
                if (connHubServerMaster != null)
                {
                    connHubServerMaster.Dispose();
                }
            }
        }


        public IEnumerable<SeedMetadata> LoadSeedMetadata(SqlReplicationConfiguration sqlReplicationConfiguration, Seeding seeding, bool forceFromDb = false)
        {
            seeding = seeding ?? sqlReplicationConfiguration.Seeding;

            var seedMetadata = new List<SeedMetadata>();

            if (!forceFromDb)
            {
                seedMetadata = LoadSeedMetadataFile() ?? new List<SeedMetadata>();
            }

            AddSeedMetadataFromDb(sqlReplicationConfiguration, seeding, seedMetadata);
            SaveSeedMetadataFile(seedMetadata);

            return seedMetadata;
        }

        #endregion

        #region Helpers - Get (General)

        /// <summary>
        /// Configures the database permissions on the target connection string so that NETWORK SERVICE, SYSTEM, and the publisher server have db owner permissions..
        /// </summary>
        /// <param name="machineAccount">The machine account.</param>
        /// <param name="connectionString">The connection string.</param>
        private static void ConfigureDatabasePermissions(string machineAccount, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                const string networkServiceAccount = @"NT AUTHORITY\NETWORK SERVICE";
                const string localSystemAccount = @"NT AUTHORITY\SYSTEM";

                const string sqlQueryRegisterAccountFormatString =
                    @"IF NOT EXISTS (SELECT name FROM master.sys.syslogins WHERE name = '{0}') BEGIN CREATE LOGIN [{0}] FROM WINDOWS END 
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = '{0}') BEGIN CREATE USER [{0}] FOR LOGIN [{0}] END 
exec sp_addrolemember 'db_owner', '{0}' ";

                var queries = new List<string>();

                // The following will create the login user for the NetworkServiceAccount if it doesnt exist, and add it to to the "db_owner" users group if it isn't part of it.
                queries.Add(string.Format(sqlQueryRegisterAccountFormatString, networkServiceAccount));

                // The following will create the login user for the LocalSystemAccount if it doesnt exist, and add it to to the "db_owner" users group if it isn't part of it.
                queries.Add(string.Format(sqlQueryRegisterAccountFormatString, localSystemAccount));

                // The following will create the login user for the domainname\machinename$ if it doesnt exist, and add it to to the "db_owner" users group if it isn't part of it.
                if (machineAccount != null)
                    queries.Add(string.Format(sqlQueryRegisterAccountFormatString, machineAccount));

                using (var command = connection.CreateCommand())
                {
                    command.Connection = connection;

                    foreach (var query in queries)
                    {
                        command.CommandText = query;
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceError(ex.ToString());
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get the replication metadata
        /// </summary>
        /// <param name="publisherServer">Hub server passed by client</param>
        /// <param name="publisherDatabase">Hub database passed by client</param>
        /// <param name="publishingMaster">Connection to hub master database</param>
        /// <param name="distributor">Out parameter for ReplicationDistributor</param>
        /// <param name="publisher">Out parameter for ReplicationPublisher</param>
        /// <param name="augmentSysObj">Flag to indicate if underlining system objects should be included</param>
        private bool GetReplicationInfo(string publisherServer, string publisherDatabase, SqlConnection publishingMaster, out ReplicationDistributor distributor, out ReplicationPublisher publisher, bool augmentSysObj = false)
        {
            publisher = null;
            distributor = GetDistributorInfo(publishingMaster, publisherServer);

            // Ensure that distributor is configured correctly
            if (distributor == null || !distributor.IsInstalled || !distributor.IsDbInstalled) { Trace.TraceWarning("No Distributor"); return false; }
            if (!distributor.IsPublisher && !distributor.HasRemotePublisher) { Trace.TraceWarning("Distribution Not Configured"); return false; }
            if (String.IsNullOrEmpty(distributor.Server) || String.IsNullOrEmpty(distributor.ConnectionString)) { Trace.TraceWarning("Distributor Connection String Missing"); return false; }

            // Get hub and spoke servers
            distributor.ReplicationServerDetailSetup = SqlReplicationUtilities.GetReplicationServerDetailInfo(distributor.ConnectionString);
            if (distributor.ReplicationServerDetailSetup == null) { Trace.TraceWarning("Missing publisher (HubSpokeServerSetup)"); return false; }

            publisher = GetPublishingInfo(distributor.ConnectionString, publisherDatabase);
            if (publisher == null) { Trace.TraceWarning("Missing publisher"); return false; }

            publisher = publisher.ReplaceServer(distributor.ReplicationServerDetailSetup);
            if (!publisher.Server.Equals(publisherServer, StringComparison.OrdinalIgnoreCase)) { Trace.TraceWarning("Mismatch publisher server"); return false; }
            if (!publisher.Database.Equals(publisherDatabase, StringComparison.OrdinalIgnoreCase)) { Trace.TraceWarning("Mismatch publisher database"); return false; }

            publisher.ArticleList = GetArticleInfo(distributor.ConnectionString, publisher.Database);
            publisher.SubscriberList = GetSubscriberInfo(distributor.ConnectionString, publisher.ConnectionString);

            if (augmentSysObj)
            {
                publisher.ArticleList = AugmentSysObjReplicationArticles(publishingMaster, publisher.Database, publisher.ArticleList);
                publisher.SubscriberList = AugmentSysObjReplicationSubscribers(publisher.ArticleList, publisher.SubscriberList);
            }
            return true;
        }

        /// <summary>
        /// Add the underling system objects for a list of articles
        /// </summary>
        /// <param name="conn">Connection to master database</param>
        /// <param name="publisherDatabase">Name of database containing the articles</param>
        /// <param name="articleList">List of articles that system objects should be added for</param>
        /// <returns>List of ReplicationArticle</returns>
        private List<ReplicationArticle> AugmentSysObjReplicationArticles(SqlConnection conn, string publisherDatabase, List<ReplicationArticle> articleList)
        {
            if (articleList == null)
                return null;
            conn.ChangeDatabase(publisherDatabase);

            var dbSysObjs = GetDatabaseSystemObjects(conn, articleList.Select(a => a.Name).ToArray()).ToDictionary(i => i.Name, StringComparer.OrdinalIgnoreCase);

            foreach (var article in articleList)
            {
                article.DatabaseSystemObject = dbSysObjs.GetValue(article.Name);
            }

            return articleList;
        }

        /// <summary>
        /// Add the underling system objects for a list of subscribers
        /// </summary>
        /// <param name="articleList">List of articles that system objects should be added for</param>
        /// <param name="subscriberList">List of subscribers that system objects should be added for</param>
        /// <returns>List of ReplicationSubscriber</returns>
        private List<ReplicationSubscriber> AugmentSysObjReplicationSubscribers(List<ReplicationArticle> articleList, List<ReplicationSubscriber> subscriberList)
        {
            if (articleList == null || subscriberList == null)
                return subscriberList;
            foreach (var subscriber in subscriberList)
            {
                using (var conn = new SqlConnection(subscriber.ConnectionString))
                {
                    conn.Open();
                    subscriber.DatabaseSystemObjects = GetDatabaseSystemObjects(conn, articleList.Select(a => a.Name).ToList());
                }
            }
            return subscriberList;
        }

        #endregion

        #region Helpers - Delete


        /// <summary>
        /// Process list of partners that need to be removed from replication
        /// </summary>
        /// <param name="partnersConnStrings">List of servers to be removed</param>
        /// <param name="publishingMaster">Connection for hub</param>
        /// <param name="publisher">Replication publisher</param>
        /// <returns>List of ReplicationSubscriber that were removed</returns>
        private void RemoveReplicationPartners(IEnumerable<string> partnersConnStrings, SqlConnection publishingMaster, ReplicationPublisher publisher)
        {
            var deletedSubscribers = new List<ReplicationSubscriber>();
            foreach (var connString in partnersConnStrings)
            {
                var partnerServer = connString.GetServer();
                var partnerDatabase = connString.GetDatabase();

                if (partnerServer == null || partnerDatabase == null)
                    throw new ArgumentException("Replication Partner Connection String Is Invalid");

                var subscriber = publisher.SubscriberList.First(s => s.Server.Equals(partnerServer, StringComparison.OrdinalIgnoreCase) && s.Database.Equals(partnerDatabase, StringComparison.OrdinalIgnoreCase));
                RemoveSubscriberInfo(publishingMaster, publisher, subscriber);
                deletedSubscribers.Add(subscriber);
            }
        }

        #endregion

        #region Helpers - Configure

        private void ConfigureExisting(SqlConnection connHubServerMaster, ReplicationDistributor distributor, string publisherDatabase, string publisherServer,
                                        IEnumerable<SeedMetadata> seedMetadata, SqlReplicationConfiguration sqlReplicationConfiguration)
        {
            // Add replication as needed
            var publisher = GetPublishingInfo(distributor.ConnectionString, publisherDatabase);
            if (publisher == null)
            {
                CreatePublishingInfo(connHubServerMaster, publisherDatabase, seedMetadata, sqlReplicationConfiguration.UseSnapshotForReplication);
            }

            // Get current replication world, get underlining system objects also
            if (!GetReplicationInfo(publisherServer, publisherDatabase, connHubServerMaster, out distributor, out publisher, true))
            {
                throw new ApplicationException("Cannot complete operation. Check log file");
            }

            // Add additional replication as needed
            List<string> replicatedObjects = sqlReplicationConfiguration.ObjectsToReplicate.Distinct().ToList();

            IEnumerable<string> fixupScripts;

            var addedObjects = ConfigureReplicatedObjects(replicatedObjects, connHubServerMaster, publisher, sqlReplicationConfiguration.UseSnapshotForReplication, out fixupScripts);
            if (addedObjects != null)
            {
                Trace.TraceInformation("Added {0} objects for replication.".FormatWith(addedObjects.Count.ToString()));
            }

            // Get updated replication world, get underlining system objects also
            if (!GetReplicationInfo(publisherServer, publisherDatabase, connHubServerMaster, out distributor, out publisher, true))
            {
                throw new ApplicationException("Cannot complete operation. Check log file");
            }

            if (sqlReplicationConfiguration.UseSnapshotForReplication)
            {
                // create snapshot first so that subscribers have access to it

                var addedSubscribers = sqlReplicationConfiguration.SpokeConnectionStrings == null ? null : ConfigureReplicationPartners(sqlReplicationConfiguration.SpokeConnectionStrings, connHubServerMaster, distributor, publisher, true);
                if (addedSubscribers != null)
                    Trace.TraceInformation("Added {0} subscribers for replication.".FormatWith(addedSubscribers.Count.ToString()));

                // Create initial snapshot
                StartPublicationSnapshotAgentJob(connHubServerMaster, publisherDatabase);

                if (!HoldUntilSnapshotGenerated(distributor, publisher.Database))
                    throw new ApplicationException("Snapshot generation failed");

                if (fixupScripts != null)
                {
                    connHubServerMaster.ChangeDatabase(publisher.Database);
                    fixupScripts.ToList().ForEach(s => connHubServerMaster.Execute(s));
                }
            }
            else
            {
                // Create initial snapshot first before the subscribers are added, this way we don't use the snapshot against them.

                StartPublicationSnapshotAgentJob(connHubServerMaster, publisherDatabase);

                if (!HoldUntilSnapshotGenerated(distributor, publisher.Database))
                    throw new ApplicationException("Snapshot generation failed");

                if (fixupScripts != null)
                {
                    connHubServerMaster.ChangeDatabase(publisher.Database);
                    fixupScripts.ToList().ForEach(s => connHubServerMaster.Execute(s));
                }

                var addedSubscribers = sqlReplicationConfiguration.SpokeConnectionStrings == null ? null : ConfigureReplicationPartners(sqlReplicationConfiguration.SpokeConnectionStrings, connHubServerMaster, distributor, publisher, false);
                if (addedSubscribers != null)
                    Trace.TraceInformation("Added {0} subscribers for replication.".FormatWith(addedSubscribers.Count.ToString()));

                ApplySeeding(sqlReplicationConfiguration, seedMetadata.ToList());
            }
        }

        /// <summary>
        /// Create a new replication setup
        /// </summary>
        /// <param name="sqlReplicationConfiguration">The set of servers and articles to create</param>
        /// <param name="publishingMaster">Connection to hub master database</param>
        /// <param name="seedMetadata">The default step range value to use for seeding and any overrides per object</param>
        /// <param name="publisherServer">Server for the hub</param>
        /// <param name="publisherDatabase">Database for the hub</param>
        private void ConfigureNew(SqlReplicationConfiguration sqlReplicationConfiguration, SqlConnection publishingMaster, List<SeedMetadata> seedMetadata, string publisherServer, string publisherDatabase)
        {
            CreateDistributorInfo(publishingMaster);
            CreatePublishingInfo(publishingMaster, publisherDatabase, seedMetadata, sqlReplicationConfiguration.UseSnapshotForReplication);

            var replicatedObjects = sqlReplicationConfiguration.ObjectsToReplicate.Distinct().ToList();

            IEnumerable<string> fixupScripts;

            CreateArticlesFromList(publishingMaster, publisherDatabase, replicatedObjects, sqlReplicationConfiguration.UseSnapshotForReplication, out fixupScripts);

            if (sqlReplicationConfiguration.UseSnapshotForReplication)
            {
                // create snapshot first so that subscribers have access to it

                // for each 'spoke' connection, create the subscription information at both the publisher and subscriber servers.
                if (sqlReplicationConfiguration.SpokeConnectionStrings != null)
                {
                    sqlReplicationConfiguration.SpokeConnectionStrings.ToList().ForEach(c => CreateSubscriptionInfoForPublisherAndSubscriber(publisherServer, publishingMaster, publisherServer, publisherDatabase, c, true));
                }

                StartPublicationSnapshotAgentJob(publishingMaster, publisherDatabase);

                // Allow snapshot time to generate
                ReplicationDistributor distributor;
                ReplicationPublisher publisher;
                if (!GetReplicationInfo(publisherServer, publisherDatabase, publishingMaster, out distributor, out publisher))
                    throw new ApplicationException("Cannot complete operation. Check log file");

                if (!HoldUntilSnapshotGenerated(distributor, publisher.Database))
                    throw new ApplicationException("Snapshot generation failed");

                if (fixupScripts != null)
                {
                    publishingMaster.ChangeDatabase(publisher.Database);
                    fixupScripts.ToList().ForEach(s => publishingMaster.Execute(s));
                }
            }
            else
            {
                // Create initial snapshot first before the subscribers are added, this way we don't use the snapshot against them.

                StartPublicationSnapshotAgentJob(publishingMaster, publisherDatabase);

                // Allow snapshot time to generate
                ReplicationDistributor distributor;
                ReplicationPublisher publisher;
                if (!GetReplicationInfo(publisherServer, publisherDatabase, publishingMaster, out distributor, out publisher))
                    throw new ApplicationException("Cannot complete operation. Check log file");

                if (!HoldUntilSnapshotGenerated(distributor, publisher.Database))
                    throw new ApplicationException("Snapshot generation failed");

                if (fixupScripts != null)
                {
                    publishingMaster.ChangeDatabase(publisher.Database);
                    fixupScripts.ToList().ForEach(s => publishingMaster.Execute(s));
                }

                // for each 'spoke' connection, create the subscription information at both the publisher and subscriber servers.
                if (sqlReplicationConfiguration.SpokeConnectionStrings != null)
                {
                    sqlReplicationConfiguration.SpokeConnectionStrings.ToList().ForEach(c => CreateSubscriptionInfoForPublisherAndSubscriber(publisherServer, publishingMaster, publisherServer, publisherDatabase, c, false));
                }

                ApplySeeding(sqlReplicationConfiguration, seedMetadata);
            }
        }

        /// <summary>
        /// Applies the seeding for the hub and all spokes.
        /// </summary>
        /// <param name="sqlReplicationConfiguration">The SQL replication configuration.</param>
        /// <param name="seedMetadata">The seed metadata.</param>
        private static void ApplySeeding(SqlReplicationConfiguration sqlReplicationConfiguration, List<SeedMetadata> seedMetadata)
        {
            var seedingScript = CreateSeedingScript(seedMetadata);
            using (var connection = new SqlConnection(sqlReplicationConfiguration.HubConnectionString))
            {
                connection.Execute(seedingScript);
            }
            Trace.TraceInformation("Applied seeding script on {0}.".FormatWith(sqlReplicationConfiguration.HubConnectionString));

            if (sqlReplicationConfiguration.SpokeConnectionStrings != null)
            {
                sqlReplicationConfiguration.SpokeConnectionStrings.ToList().ForEach(cs =>
                {
                    using (var connection = new SqlConnection(cs))
                    {
                        connection.Execute(seedingScript);
                    }
                    Trace.TraceInformation("Applied seeding script on {0}.".FormatWith(cs));
                });
            }
        }

        /// <summary>
        /// Add objects to an existing replication setup
        /// </summary>
        /// <param name="objects">The objects.</param>
        /// <param name="publishingMaster">The publishing master.</param>
        /// <param name="publisher">The publisher.</param>
        /// <param name="useSnapshotForReplication">if set to <c>true</c> [use snapshot for replication].</param>
        /// <param name="fixupScripts">The fixup scripts.</param>
        /// <returns></returns>
        private List<string> ConfigureReplicatedObjects(IEnumerable<string> objects, SqlConnection publishingMaster, ReplicationPublisher publisher, bool useSnapshotForReplication, out IEnumerable<string> fixupScripts)
        {
            var objectsToAdd = objects.Where(obj => publisher.ArticleList == null || !publisher.ArticleList.Any(a => a.Name.Equals(obj, StringComparison.OrdinalIgnoreCase))).ToList();
            CreateArticlesFromList(publishingMaster, publisher.Database, objectsToAdd, useSnapshotForReplication, out fixupScripts);
            return objectsToAdd;
        }

        /// <summary>
        /// Add partners to an existing replication setup
        /// </summary>
        /// <param name="partnersConnStrings">List of servers to be added</param>
        /// <param name="publishingMaster">Connection to hub</param>
        /// <param name="distributor">Replication distributor</param>
        /// <param name="publisher">Replication publisher</param>
        /// <param name="automaticSyncType"></param>
        /// <returns>
        /// List of ReplicationSubscriber that were added
        /// </returns>
        /// <exception cref="System.ArgumentException">Replication Partner Connection String Is Invalid</exception>
        private List<ReplicationSubscriber> ConfigureReplicationPartners(IEnumerable<string> partnersConnStrings, SqlConnection publishingMaster, ReplicationDistributor distributor, ReplicationPublisher publisher, bool automaticSyncType)
        {
            var addedSubscribers = new List<ReplicationSubscriber>();
            foreach (var connString in partnersConnStrings)
            {
                var partnerServer = connString.GetServer();
                var partnerDatabase = connString.GetDatabase();
                if (partnerServer == null || partnerDatabase == null)
                    throw new ArgumentException("Replication Partner Connection String Is Invalid");

                if (publisher.SubscriberList != null && publisher.SubscriberList.Any(s => s.Server.Equals(partnerServer, StringComparison.OrdinalIgnoreCase) && s.Database.Equals(partnerDatabase, StringComparison.OrdinalIgnoreCase)))
                    continue;
                CreateSubscriptionInfoForPublisherAndSubscriber(distributor.Server, publishingMaster, publisher.Server, publisher.Database, connString, automaticSyncType);
                addedSubscribers.Add(new ReplicationSubscriber { ConnectionString = connString, Server = partnerServer, Database = partnerDatabase });
            }
            return addedSubscribers;
        }

        #endregion

        #region Seeding

        /// <summary>
        /// Get the seeding configuration to be used after replication set up
        /// </summary>
        /// <param name="arguments">The set of servers and articles to remove</param>
        /// <param name="seeding">The default step range value to use for seeding and any overrides per object</param>
        /// <param name="seedMetadatas"></param>
        /// <returns>Seeding configuration</returns>
        private void AddSeedMetadataFromDb(SqlReplicationConfiguration arguments, Seeding seeding, List<SeedMetadata> seedMetadatas)
        {
            var server = arguments.HubConnectionString.GetServer();
            var database = arguments.HubConnectionString.GetDatabase();

            if (server == null || database == null)
                throw new ArgumentException("Hub Connection String Is Invalid");


            if (IsDbAccessible(arguments.HubConnectionString)) server = GetInstanceQualifiedServerName(arguments.HubConnectionString);

            var hubSeedMetadata =
                seedMetadatas.FirstOrDefault(m => m.Server.Equals(server, StringComparison.OrdinalIgnoreCase) && m.Database.Equals(database, StringComparison.OrdinalIgnoreCase));

            if (hubSeedMetadata == null)
            {
                hubSeedMetadata = new SeedMetadata { ConnectionString = arguments.HubConnectionString, Server = server, Database = database, ServerIndex = 0 };
                seedMetadatas.Add(hubSeedMetadata);
            }

            AddSpokeSeedMetadata(arguments.SpokeConnectionStrings, seedMetadatas);

            List<string> replicatedObjects = arguments.ObjectsToReplicate.Distinct().ToList();

            foreach (var metadata in seedMetadatas)
            {
                metadata.ReferrerSeeding = seeding;
                if (IsDbAccessible(metadata.ConnectionString)) ComputeSeedMetadata(metadata, replicatedObjects, seeding);
            }

        }

        /// <summary>
        /// Checks if the DB exists. Tries to open the connection string and fails silently.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        private static bool IsDbAccessible(string connectionString)
        {
            try
            {
                using (var c = new SqlConnection(connectionString)) c.Open();
                return true;
            }
            catch (Exception ex)
            {
                Trace.TraceWarning(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// Process the list of spoke connection strings and compute the server index
        /// </summary>
        /// <param name="spokeConnectionStrings">List of spoke connection strings</param>
        /// <param name="seedMetadatas"></param>
        /// <returns>List of seeding configuration</returns>
        private static void AddSpokeSeedMetadata(IEnumerable<string> spokeConnectionStrings, List<SeedMetadata> seedMetadatas)
        {
            foreach (var connString in spokeConnectionStrings)
            {
                var server = connString.GetServer();
                var database = connString.GetDatabase();

                if (server == null || database == null)
                    throw new ArgumentException("Replication Partner Connection String Is Invalid");

                if (IsDbAccessible(connString)) server = GetInstanceQualifiedServerName(connString);

                var seedMetadata = seedMetadatas.FirstOrDefault(m => m.Server.Equals(server, StringComparison.OrdinalIgnoreCase) && m.Database.Equals(database, StringComparison.OrdinalIgnoreCase));
                if (seedMetadata == null)
                {
                    seedMetadata = new SeedMetadata { ConnectionString = connString, Server = server, Database = database, ServerIndex = seedMetadatas.Select(i => i.ServerIndex).Max() + 1 };
                    seedMetadatas.Add(seedMetadata);
                }
            }
        }

        /// <summary>
        /// Gets the instance qualified server name.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns></returns>
        private static string GetInstanceQualifiedServerName(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                var combinedName = connection.Execute<string>(@"SELECT @@SERVERNAME + '|' + @@SERVICENAME");
                var nameParts = combinedName.Split('|');
                
                // For named instances, server name includes service name
                if (nameParts[0].EndsWith(nameParts[1]))
                {
                    return nameParts[0];
                }

                return string.Join(@"\", nameParts);
            }
        }

        /// <summary>
        /// Compute the seed value for each object that is being replicated
        /// </summary>
        /// <param name="seedMetadata">Seeding configuration with empty list</param>
        /// <param name="objects">Objects to be replicated</param>
        /// <param name="seeding">The default step range value to use for seeding and any overrides per object</param>
        /// <returns>Seeding configuration with list set</returns>
        private void ComputeSeedMetadata(SeedMetadata seedMetadata, IEnumerable<string> objects, Seeding seeding)
        {
            if (seedMetadata.SeedList == null) seedMetadata.SeedList = new List<SeedItemMetadata>();

            using (var conn = new SqlConnection(seedMetadata.ConnectionString))
            {
                conn.Open();

                var dbSysObjs = GetDatabaseSystemObjects(conn, objects).ToDictionary(i => i.Name, StringComparer.OrdinalIgnoreCase);

                foreach (var obj in objects)
                {
                    if (seedMetadata.SeedList.Any(i => i.Name.Equals(obj, StringComparison.OrdinalIgnoreCase)))
                    {
                        // already exists
                        continue;
                    }

                    var dbSysObj = dbSysObjs.GetValue(obj);
                    if (dbSysObj == null)
                    {
                        var seedItemMetadata = new SeedItemMetadata { Name = obj, Value = GetDefaultSeed(seeding, obj, seedMetadata.ServerIndex) };
                        if (seeding != null)
                            seedMetadata.SeedList.Add(seedItemMetadata);
                    }
                    else
                    {
                        var seedItemMetadata = new SeedItemMetadata
                        {
                            Name = obj,
                            Value = dbSysObj.CurrentSeedValue < 1 ? GetDefaultSeed(seeding, obj, seedMetadata.ServerIndex) : dbSysObj.CurrentSeedValue,
                        };
                        seedMetadata.SeedList.Add(seedItemMetadata);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the default seed for the server index specified. Returns a multiple of a constant value.
        /// </summary>
        /// <param name="seeding">The default step range value to use for seeding and any overrides per object</param>
        /// <param name="objectName">Name of object</param>
        /// <param name="serverIndex">Index of the server</param>
        /// <returns>Floor value of identity</returns>
        private static long GetDefaultSeed(Seeding seeding, string objectName, int serverIndex)
        {
            var seededObject = seeding.SeedingOverrides == null ? null : seeding.SeedingOverrides.FirstOrDefault(so => so.Name.Equals(objectName, StringComparison.OrdinalIgnoreCase));
            if (seededObject != null)
                return serverIndex * seededObject.StepRange + 1;
            return serverIndex * seeding.ServerStepRange + 1;
        }

        #endregion

        #region Distributor

        private ReplicationDistributor GetDistributorInfo(SqlConnection conn, string publisherServer)
        {
            try
            {
                conn.ChangeDatabase("master");
                var distributorInfo = DbConnections.Execute<DataTable>(conn, "sp_get_distributor");
                var distributor = ParseDistributorInfo(conn, distributorInfo);
                if (distributor == null || !distributor.IsInstalled)
                    return null;
                // check to see if distributor is remote
                if (!distributor.Server.Equals(publisherServer, StringComparison.OrdinalIgnoreCase))
                {
                    Trace.TraceInformation("Distributor is remote: {0}", distributor.ConnectionString);
                    using (var distributorConnection = new SqlConnection(distributor.ConnectionString))
                    {
                        distributorConnection.Open();
                        distributorInfo = DbConnections.Execute<DataTable>(distributorConnection, "sp_get_distributor");
                        distributor = ParseDistributorInfo(distributorConnection, distributorInfo);
                    }
                }

                return distributor;
            }
            catch (Exception ex)
            {
                // distributor may not be accessible - we don't want to fail entire migration - just don't set up replication
                Trace.TraceWarning(ex.ToString());
                return null;
            }
        }

        private ReplicationDistributor ParseDistributorInfo(SqlConnection conn, DataTable distributorInfo)
        {
            if (distributorInfo == null || distributorInfo.Rows.Count == 0)
                return null;

            return new ReplicationDistributor
            {
                ConnectionString = conn.ConnectionString.ReplaceServer(distributorInfo.GetStringValue("distribution server")).ReplaceDatabase("distribution"),
                IsInstalled = distributorInfo.GetBoolValue("installed"),
                Server = distributorInfo.GetStringValue("distribution server"),
                IsDbInstalled = distributorInfo.GetBoolValue("distribution db installed"),
                IsPublisher = distributorInfo.GetBoolValue("is distribution publisher"),
                HasRemotePublisher = distributorInfo.GetBoolValue("has remote distribution publisher")
            };
        }

        [DebuggerNonUserCode]
        private void RemoveDistributorInfo(SqlConnection conn)
        {
            // ReSharper disable EmptyGeneralCatchClause
            try
            {
                conn.ChangeDatabase("master");

                //sp_dropdistpublisher
                var dropDistributionPublisherParameters = new Dictionary<string, object> { { "publisher", conn.DataSource }, { "no_checks", 1 } };
                conn.Execute("sp_dropdistpublisher", dropDistributionPublisherParameters, false, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
            }
            catch (Exception ex)
            {
                Trace.TraceWarning(ex.ToString());
            }

            try
            {
                var dropDistributionDbParameters = new Dictionary<string, object> { { "database", "distribution" } };
                conn.Execute("sp_dropdistributiondb", dropDistributionDbParameters, false, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
            }
            catch (Exception ex)
            {
                Trace.TraceWarning(ex.ToString());
            }

            try
            {
                conn.Execute(@"ALTER DATABASE distribution SET SINGLE_USER WITH ROLLBACK IMMEDIATE; ALTER DATABASE distribution SET MULTI_USER WITH ROLLBACK IMMEDIATE");
                var dropDistributorParameters = new Dictionary<string, object> { { "no_checks", 1 } };
                conn.Execute("sp_dropdistributor", dropDistributorParameters, false, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
            }
            catch (Exception ex)
            {
                Trace.TraceWarning(ex.ToString());
            }

            // ReSharper restore EmptyGeneralCatchClause
        }

        private void CreateDistributorInfo(SqlConnection conn)
        {
            var distributorParams = new Dictionary<string, object> { { "distributor", conn.DataSource } };
            var distributorDbParams = new Dictionary<string, object> { { "database", "distribution" } };
            var distPublisherParams = new Dictionary<string, object> { { "publisher", conn.DataSource }, { "distribution_db", "distribution" } };
            conn.ChangeDatabase("master");
            DbConnections.Execute(conn, "sp_adddistributor", distributorParams, false, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
            DbConnections.Execute(conn, "sp_adddistributiondb", distributorDbParams, false, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
            DbConnections.Execute(conn, "sp_adddistpublisher", distPublisherParams, false, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
        }

        #endregion

        #region Publisher

        private ReplicationPublisher GetPublishingInfo(string distributorConnectionString, string publisherDatabase)
        {
            using (var distributorConnection = new SqlConnection(distributorConnectionString))
            {
                distributorConnection.Open();
                var query = string.Format("SELECT publisher_db, publication FROM MSpublications WHERE publisher_db = '{0}' GROUP BY publisher_db, publication", publisherDatabase);
                var publisherInfo = DbConnections.Execute<DataTable>(distributorConnection, query);
                return ParsePublishingInfo(distributorConnection, publisherInfo);
            }
        }

        private ReplicationPublisher ParsePublishingInfo(SqlConnection conn, DataTable publisherInfo)
        {
            if (publisherInfo == null || publisherInfo.Rows.Count == 0)
                return null;
            return new ReplicationPublisher
            {
                ConnectionString = conn.ConnectionString.ReplaceDatabase(publisherInfo.GetStringValue("publisher_db")),
                Name = publisherInfo.GetStringValue("publication"),
                Server = conn.ConnectionString.GetServer(),
                Database = publisherInfo.GetStringValue("publisher_db")
            };
        }

        private void RemovePublishingInfo(SqlConnection publishingMaster, ReplicationPublisher publisher)
        {
            try
            {
                publishingMaster.ChangeDatabase(publisher.Database);
                publishingMaster.Execute("sp_replflush", null, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

                try
                {
                    publishingMaster.ChangeDatabase(publisher.Database);
                    var dropPublicationParams = new Dictionary<string, object> { { "publication", publisher.Name } };
                    DbConnections.Execute(publishingMaster, "sp_droppublication", dropPublicationParams, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
                }
                catch (Exception ex)
                {
                    // expected error - drop works anyway
                    if (!ex.ToString().Contains("Only one Log Reader Agent or log-related procedure (sp_repldone, sp_replcmds, and sp_replshowcmds) can connect to a database at a time"))
                    {
                        throw;
                    }
                }

                publishingMaster.Execute("sp_replflush", null, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

                publishingMaster.ChangeDatabase("master");
                var replicationDbOptionParams = new Dictionary<string, object> { { "dbname", publisher.Database }, { "optname", "publish" }, { "value", "false" } };
                DbConnections.Execute(publishingMaster, "sp_replicationdboption", replicationDbOptionParams, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
            }
            finally
            {
                publishingMaster.ChangeDatabase("master");
            }
        }

        /// <summary>
        /// 1. Initializes replication on the specified publisher server and database.
        /// 2. Creates a pre-snapshot sql script to create a backup of the database before the snapshot agent runs against a subscriber
        /// 3. Creates a post-snapshot sql script to Alter the 'seeding' of the database. This gets ran after  after all the other replicated object scripts and data have been applied during an initial synchronization
        /// 4. Creates a publication on the publication server and database.sp
        /// 5. Creates a publication snapshot agent for single execution
        /// </summary>
        /// <param name="publishingMaster"></param>
        /// <param name="publisherDatabase"></param>
        /// <param name="seedMetadata"></param>
        /// <param name="useInitializedSnapshot">Set to true to have a snapshot agent created on the publication at the pub server. It will be ran only ONCE, for initialization purposes, against the subscriptions</param>
        private static void CreatePublishingInfo(SqlConnection publishingMaster, string publisherDatabase, IEnumerable<SeedMetadata> seedMetadata, bool useInitializedSnapshot)
        {
            publishingMaster.ChangeDatabase(publisherDatabase);

            var dbInfo = DbConnections.Execute<DataTable>(publishingMaster, "SELECT TOP 1 physical_name FROM sys.master_files");
            if (dbInfo == null || dbInfo.Rows.Count == 0)
                throw new ApplicationException();

            var sqlFilePath = dbInfo.GetStringValue("physical_name");
            var sqlDirectory = Path.GetDirectoryName(sqlFilePath);

            var accessibleSqlDirectory = sqlDirectory;
            if (publishingMaster.DataSource != null)
            {
                // check the datasource.  if the source server is not the same as the current machine this process is executing on, then set the correct sql directory path format.
                var serverName = publishingMaster.DataSource.Split('\\').First().Split('.').First();
                var machineName = Environment.MachineName.Split('.').First();
                if (!serverName.Equals("localhost", StringComparison.OrdinalIgnoreCase) && !serverName.Equals(machineName, StringComparison.OrdinalIgnoreCase))
                {
                    accessibleSqlDirectory = @"\\{0}\{1}".FormatWith(serverName, sqlDirectory.ReplaceFirst(":", "$"));
                }
            }

            // gets executed before snapshot runs against subscriber
            var preSnapshotScriptPath = Path.Combine(accessibleSqlDirectory ?? string.Empty, "PreSnapshotScript.sql");
            File.WriteAllText(preSnapshotScriptPath, string.Empty);

            // gets executed after snapshot runs against all subscribers
            var postSnapshotScriptPath = Path.Combine(accessibleSqlDirectory ?? string.Empty, "Seeding_{0}.sql".FormatWith(DateTime.Now.ToString("yyyy.MM.dd_HH.mm.ss")));
            File.WriteAllText(postSnapshotScriptPath, CreateSeedingScript(seedMetadata));

            // Sets a replication database option for the specified database. This stored procedure is executed at the Publisher or Subscriber on any database.
            var replicationDbOptionParams = new Dictionary<string, object> { { "dbname", publisherDatabase }, { "optname", "publish" }, { "value", "true" } };
            publishingMaster.Execute("sp_replicationdboption", replicationDbOptionParams, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

            // Changes the properties of the distribution database. This stored procedure is executed at the Distributor on any database.
            // Here we are changing the Max distribution retention period (history retention in days).
            publishingMaster.Execute("sp_changedistributiondb", new Dictionary<string, object> { { "database", "distribution" }, { "property", "max_distretention" }, { "value", MaxDistributionDatabaseRetentionPeriod } }, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

            // check if there is a queue reader agent installed at the publisher master.  if so add one at the publisher db sys level.
            var queueReaderAgentParams = new Dictionary<string, object> { { "frompublisher", "1" } };
            var queueReaderAgentInfo = DbConnections.Execute<DataTable>(publishingMaster, "sp_helpqreader_agent", queueReaderAgentParams, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
            if (queueReaderAgentInfo == null || queueReaderAgentInfo.Rows.Count == 0)
                publishingMaster.Execute(publisherDatabase + ".sys.sp_addqreader_agent", queueReaderAgentParams, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

            // same as above queue reader.
            var logReaderAgentInfo = DbConnections.Execute<DataTable>(publishingMaster, "sp_helplogreader_agent", null, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
            if (logReaderAgentInfo == null || logReaderAgentInfo.Rows.Count == 0)
                publishingMaster.Execute(publisherDatabase + ".sys.sp_addlogreader_agent", null, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

            // parameters to specify when creating a new publication.
            var publicationDetails = PublicationPropertyDetail.CreateContinuousConfiguration(publisherDatabase, useInitializedSnapshot);
            publicationDetails.PresnapshotScriptPath = preSnapshotScriptPath;
            publicationDetails.PostSnapshotScriptPath = postSnapshotScriptPath;
            publicationDetails.AlternateSnapshotFolderPath = sqlDirectory;
            publicationDetails.CompressSnapshot = IsDatabaseSizeUnder20GB(publishingMaster, publisherDatabase) && !IsAnyDatabaseTableOver2GB(publishingMaster, publisherDatabase);

            // Creates a snapshot or transactional publication
            publishingMaster.Execute("sp_addpublication", publicationDetails.ToSqlStoredProcedureParameters(), false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });


            // Here we set the values for a new snapshot agent. in this case we are only creating an agent for a single execution.
            var publicationSnapshotParams = new Dictionary<string, object>
                                                {
                                                    { "publication", publisherDatabase }, 
                                                    { "frequency_type", 1 },               /*   frequency with which the Snapshot Agent is executed. here we specify 'Once' */
                                                    { "frequency_interval", 1 },           /*   value to apply to the frequency set by frequency_type. here we specify a value of 1, which means frequency_interval is unused*/
                                                    { "publisher_security_mode", 1 /* Windows Authentication */ }
                                                };

            // Creates the Snapshot Agent for the specified publication. This stored procedure is executed at the Publisher on the publication database
            publishingMaster.Execute("sp_addpublication_snapshot", publicationSnapshotParams, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

        }

        /// <summary>
        /// Determines whether the database size under is 20GB or less
        /// </summary>
        /// <param name="publishingMaster">The publishing master.</param>
        /// <param name="publisherDatabase">The publisher database.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        private static bool IsDatabaseSizeUnder20GB(SqlConnection publishingMaster, string publisherDatabase)
        {
            var originalDb = publishingMaster.Database;
            publishingMaster.ChangeDatabase(publisherDatabase);

            var size = publishingMaster.Execute<int>(@"
SELECT SUM(UsedSpaceKB) FROM (
SELECT 
    t.NAME AS TableName,
    s.Name AS SchemaName,
    p.rows AS RowCounts,
    SUM(a.total_pages) * 8 AS TotalSpaceKB, 
    SUM(a.used_pages) * 8 AS UsedSpaceKB, 
    (SUM(a.total_pages) - SUM(a.used_pages)) * 8 AS UnusedSpaceKB
FROM 
    sys.tables t
INNER JOIN      
    sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN 
    sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN 
    sys.allocation_units a ON p.partition_id = a.container_id
LEFT OUTER JOIN 
    sys.schemas s ON t.schema_id = s.schema_id
WHERE 
    t.NAME NOT LIKE 'dt%' 
    AND t.is_ms_shipped = 0
    AND i.OBJECT_ID > 255 
GROUP BY 
    t.Name, s.Name, p.Rows
) q
");

            publishingMaster.ChangeDatabase(originalDb);

            return size < 20000000;
        }

        private static bool IsAnyDatabaseTableOver2GB(SqlConnection publishingMaster, string publisherDatabase)
        {
            var originalDb = publishingMaster.Database;
            publishingMaster.ChangeDatabase(publisherDatabase);

            var anyTableOver2GB = publishingMaster.Execute<int>(@"
WITH CTE AS (
SELECT 
s.Name AS SchemaName
,t.NAME AS TableName
,((SUM(a.total_pages) * 8) / 1000000) AS TotalSpaceGB
FROM sys.tables t
INNER JOIN sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id
LEFT OUTER JOIN sys.schemas s ON t.schema_id = s.schema_id
WHERE t.NAME NOT LIKE 'dt%' 
	AND t.is_ms_shipped = 0
GROUP BY t.Name, s.Name
)
SELECT TOP 1 1 FROM CTE WHERE TotalSpaceGB >= 2");

            publishingMaster.ChangeDatabase(originalDb);

            return anyTableOver2GB == 1;
        }

        /// <summary>
        /// Creates a seeding script to reseed all servers.
        /// </summary>
        /// <param name="seedMetadata">The seed metadata.</param>
        /// <returns></returns>
        private static string CreateSeedingScript(IEnumerable<SeedMetadata> seedMetadata)
        {
            const string instanceNameQuery = @"@@SERVERNAME + '\' + @@SERVICENAME";
            var sb = new StringBuilder();

            sb.Append(@"
DECLARE @identityColumnName nvarchar(max)
DECLARE @sql nvarchar(max)
");

            foreach (var s in seedMetadata)
            {
                if (s.SeedList.Count == 0) continue;

                sb.AppendLine("IF ({0}) = '{1}' AND '{2}' = DB_NAME()".FormatWith(instanceNameQuery, s.Server, s.Database));
                sb.AppendLine("BEGIN");
                foreach (var i in s.SeedList)
                {
                    var item = i;

                    sb.AppendLine(
                        @"
    IF OBJECTPROPERTY(OBJECT_ID('{0}'), 'IsTable') = 1 AND OBJECTPROPERTY(OBJECT_ID('{0}'), 'TableHasIdentity') = 1
    BEGIN

        SET @identityColumnName = (SELECT TOP 1 c.name FROM sys.columns c WHERE is_identity = 1 AND object_id = OBJECT_ID('{0}'))

        SET @sql =  '
        DECLARE @identityValue bigint
        SET @identityValue = {1}

        WHILE EXISTS(SELECT * FROM {0} WHERE [' + @identityColumnName + '] >= @identityValue AND [' + @identityColumnName + ']  < @identityValue + 5000)
	        SET @identityValue = @identityValue + 1

        DBCC CHECKIDENT(''{0}'', ''RESEED'', @identityValue)
        '

        EXEC sp_executesql @statement = @sql
    END".FormatWith(item.Name, item.Value + 1));
                }

                sb.AppendLine("END");
            }

            Trace.TraceInformation("Created seeding script for {0}.".FormatWith(seedMetadata.Select(m => m.ConnectionString + " ({0} articles)".FormatWith(m.SeedList.Count)).Join()));

            return sb.ToString();
        }

        #endregion

        #region Article

        private List<ReplicationArticle> GetArticleInfo(string distributorConnectionString, string publisherDatabase)
        {
            using (var distributorConnection = new SqlConnection(distributorConnectionString))
            {
                distributorConnection.Open();
                var query = String.Format("SELECT article_id, '[' + source_owner + '].[' + source_object + ']' AS name, source_owner, source_object, destination_object FROM MSArticles WHERE publisher_db = '{0}' GROUP BY article_id, article, source_owner, source_object, destination_object", publisherDatabase);
                var articleInfo = DbConnections.Execute<DataTable>(distributorConnection, query);
                return ParseArticleInfo(articleInfo);
            }
        }

        private List<ReplicationArticle> ParseArticleInfo(DataTable articleInfo)
        {
            if (articleInfo == null || articleInfo.Rows.Count == 0)
                return null;
            return (from DataRow row in articleInfo.Rows
                    select new ReplicationArticle
                    {
                        Id = row.GetInt32Value("article_id"),
                        Name = row.GetStringValue("name"),
                        SourceOwner = row.GetStringValue("source_owner"),
                        SourceObject = row.GetStringValue("source_object"),
                        DestinationObject = row.GetStringValue("destination_object")
                    }).ToList();
        }

        /// <summary>
        /// Creates replication articles in the db for the publisher based on the specified objectNames.  Each name represents an existing object in the database.
        /// There is special logic for 'View' type objects and each view's script definition will be addded to the 
        /// <see cref="fixupScripts" /> collection.
        /// </summary>
        /// <param name="publishingMaster">The publishing master.</param>
        /// <param name="publisherDatabase">The publisher database.</param>
        /// <param name="articleNames">A collection of db object names to create as an article.  Most of them should be existing.  Some new ones may not be.</param>
        /// <param name="useSnapshotForReplication">if set to <c>true</c> [use snapshot for replication].</param>
        /// <param name="fixupScripts">The fixup scripts.</param>
        private void CreateArticlesFromList(SqlConnection publishingMaster, string publisherDatabase, IEnumerable<string> articleNames, bool useSnapshotForReplication, out IEnumerable<string> fixupScripts)
        {
            publishingMaster.ChangeDatabase(publisherDatabase);

            articleNames = OrderByDependency(publishingMaster, articleNames);

            var fixupScriptsList = new List<string>();

            var dbSysObjs = GetDatabaseSystemObjects(publishingMaster, articleNames).ToDictionary(i => i.Name, StringComparer.OrdinalIgnoreCase);

            // remove noexpand
            foreach (var article in articleNames.ToArray())
            {
                var dbSysObj = dbSysObjs.GetValue(article);

                if (dbSysObj == null) { Trace.TraceWarning(article + " was not found in database " + publisherDatabase); continue; }

                // check if this object is a view
                if (new[] { "V", "P", "TF", "FN" }.Contains(dbSysObj.Type) && useSnapshotForReplication)
                {
                    var noexpand = @"WITH\s*\(NOEXPAND\)";
                    var text = dbSysObj.Definition;
                    if (!String.IsNullOrEmpty(text) && Regex.IsMatch(text, noexpand))
                    {
                        string type;
                        if (dbSysObj.Type == "V") type = "VIEW";
                        else if (dbSysObj.Type == "P") type = "PROCEDURE";
                        else type = "FUNCTION";

                        // instead of creating the view, use the 'Alter' keyword since this object already exists (it's definition body may have changed only)
                        var alterView = Regex.Replace(text, @"CREATE\s*{0}".FormatWith(type), "ALTER {0}".FormatWith(type));

                        fixupScriptsList.Add(alterView);
                        publishingMaster.Execute(Regex.Replace(alterView, noexpand, string.Empty, RegexOptions.IgnoreCase));
                    }
                }


                CreateArticleInfo(publishingMaster, publisherDatabase, dbSysObj);
            }

            fixupScripts = fixupScriptsList;
        }

        private static IEnumerable<string> OrderByDependency(SqlConnection connection, IEnumerable<string> objectNames)
        {
            var orderedNames = connection.Execute<DataTable>("SELECT ObjectName FROM [dbo].[GetObjectsInDependencyOrder](NULL)").AsEnumerable().Select(r => r["ObjectName"] as string).ToList();

            var result = objectNames.OrderBy(orderedNames.IndexOf).ToArray();

            return result;
        }

        /// <summary>
        /// Create a replication article based on the dbSysObj information.
        /// </summary>
        /// <param name="publishingMaster"></param>
        /// <param name="publisherDatabase"></param>
        /// <param name="dbSysObj"></param>
        private static void CreateArticleInfo(SqlConnection publishingMaster, string publisherDatabase, DbSysObj dbSysObj)
        {
            publishingMaster.ChangeDatabase(publisherDatabase);
            var parameters = new Dictionary<string, object>
            {
                {"force_invalidate_snapshot", 1}, {"publication", publisherDatabase}, {"article", dbSysObj.Name.Replace("[", string.Empty).Replace("]", string.Empty)},
                {"source_owner", dbSysObj.SchemaName}, {"source_object", dbSysObj.ObjectName},
                {"destination_owner", dbSysObj.SchemaName}, {"destination_table", dbSysObj.ObjectName}
            };

            using (var command = new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = 3600 })
            {
                /*
                TR	SQL_TRIGGER
                D 	DEFAULT_CONSTRAINT
                PK	PRIMARY_KEY_CONSTRAINT
                F 	FOREIGN_KEY_CONSTRAINT
                U 	USER_TABLE
                P 	SQL_STORED_PROCEDURE
                V 	VIEW
                FN	SQL_SCALAR_FUNCTION
                TF	SQL_TABLE_VALUED_FUNCTION
                */
                // http://social.msdn.microsoft.com/Forums/is/sqlreplication/thread/3b65827a-1886-4b43-a6ed-a8c1b7d66e8c
                // http://stackoverflow.com/questions/8467072/sql-server-varbinary-bigint-with-bitconverter-toint64-values-are-different
                switch (dbSysObj.Type)
                {
                    case "U": // USER_TABLE 
                        parameters.Add("type", "logbased");
                        parameters.Add("identityrangemanagementoption", "manual");
                        var schemaOptionUserTable = BitConverter.GetBytes(0x0000000008635FDF); // original: 0x0000000008035CDF
                        if (BitConverter.IsLittleEndian)
                            Array.Reverse(schemaOptionUserTable);
                        command.Parameters.Add(new SqlParameter("schema_option", schemaOptionUserTable) { SqlDbType = SqlDbType.VarBinary });
                        break;
                    case "P": // SQL_STORED_PROCEDURE
                        parameters.Add("type", "proc schema only");
                        break;
                    case "V": // VIEW
                        parameters.Add("type", (dbSysObj.IsIndexed || dbSysObj.IsSchemaBound) ? "indexed view schema only" : "view schema only");
                        var schemaOptionView = BitConverter.GetBytes(0x0000000008000151);
                        if (BitConverter.IsLittleEndian)
                            Array.Reverse(schemaOptionView);
                        command.Parameters.Add(new SqlParameter("schema_option", schemaOptionView) { SqlDbType = SqlDbType.VarBinary });
                        break;
                    case "FN": // SQL_SCALAR_FUNCTION
                    case "TF": // SQL_TABLE_VALUED_FUNCTION
                        parameters.Add("type", "func schema only");
                        break;
                    default:
                        Trace.TraceWarning("Unhandled DatabaseSystemObject : {0} : {1}", dbSysObj.Type, dbSysObj.TypeDesc);
                        break;
                }

                publishingMaster.Execute("sp_addarticle", parameters, false, () => command);

                Trace.TraceInformation("Added article {0} for replication.", dbSysObj.Name);
            }
        }

        #endregion

        #region Subscription

        private List<ReplicationSubscriber> GetSubscriberInfo(string distributorConnectionString, string publisherConnectionString)
        {
            using (var distributorConnection = new SqlConnection(distributorConnectionString))
            {
                distributorConnection.Open();
                try
                {
                    var subscriberInfo = DbConnections.Execute<DataTable>(distributorConnection, @"
SELECT DISTINCT ss.data_source AS SubscriberDataSource, subscriptions.subscriber_db AS SubscriberDatabase
FROM MSsubscriptions subscriptions JOIN sys.servers ss ON ss.server_id = subscriptions.subscriber_id
WHERE publisher_db = '{0}'
".FormatWith(publisherConnectionString.GetDatabase()));
                    return subscriberInfo.AsEnumerable().Select(row =>
                        new ReplicationSubscriber
                        {
                            ConnectionString = publisherConnectionString.ReplaceServer(row.GetStringValue("SubscriberDataSource")).ReplaceDatabase(row.GetStringValue("SubscriberDatabase")),
                            Server = row.GetStringValue("SubscriberDataSource"),
                            Database = row.GetStringValue("SubscriberDatabase")
                        }
                        ).ToList();
                }
                catch (Exception ex)
                {
                    Trace.TraceWarning(ex.ToString());
                    return new List<ReplicationSubscriber>();
                }
            }
        }

        private void RemoveSubscriberInfo(SqlConnection publishingMaster, ReplicationPublisher publisher, ReplicationSubscriber subscriber)
        {
            var conn = new SqlConnection(subscriber.ConnectionString);
            try
            {
                conn.Open(); // Get subscriber connection at this point if problems then unnecessary work is avoided

                try
                {
                    // Remove at the publisher database
                    var publisherParams = new Dictionary<string, object> { { "publication", publisher.Name }, { "article", "all" }, { "subscriber", subscriber.Server }, { "destination_db", subscriber.Database } };
                    publishingMaster.ChangeDatabase(publisher.Database);
                    publishingMaster.Execute("sp_dropsubscription", publisherParams, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

                }
                catch (Exception ex)
                {
                    Trace.TraceWarning(new ApplicationException("Could not drop subscription for {0}.", ex).ToString(), subscriber.ConnectionString);
                }

                // Remove at the subscriber database
                var subscriberParams = new Dictionary<string, object> { { "publisher", publisher.Server }, { "publisher_db", publisher.Database }, { "publication", publisher.Name } };
                DbConnections.Execute(conn, "sp_subscription_cleanup", subscriberParams, false, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
                DbConnections.Execute(conn, "sp_removedbreplication", null, false, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
            }
            catch (Exception ex)
            {
                Trace.TraceWarning(new ApplicationException("Cloud not clean up subscription for {0}.", ex).ToString(), subscriber.ConnectionString);
            }
            finally
            {
                conn.Dispose();
            }
        }

        private void CreateSubscriptionInfoForPublisherAndSubscriber(string distributorServer, SqlConnection publishingMaster, string publisherServer, string publisherDatabase, string subscriberConnString, bool automaticSyncType)
        {
            var subscriberServer = subscriberConnString.GetServer();
            var subscriberDatabase = subscriberConnString.GetDatabase();

            if (subscriberServer == null || subscriberDatabase == null)
                throw new ArgumentException("Subscriber Connection String Is Invalid");
            var subscriberConnection = new SqlConnection(subscriberConnString);
            try
            {
                subscriberConnection.Open(); // Get subscriber connection at this point if problems then unnecessary work is avoided

                // Adds the actual subscription and sets it's options. It gets added at the publisher on the publication database (distributor). 
                var publisherSubscriptionParams = new Dictionary<string, object>
                {
                    {"publication", publisherDatabase}, {"article", "all"}, {"subscriber", subscriberServer},
                    {"destination_db", subscriberDatabase}, {"subscription_type", "Push"}, {"sync_type", automaticSyncType ? "automatic" : "replication support only"}, {"update_mode", "queued failover"}
                };
                publishingMaster.ChangeDatabase(publisherDatabase);
                publishingMaster.Execute("sp_addsubscription", publisherSubscriptionParams, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

                // Adds a 'push' subscription agent which is really just a scheduled job (used only with transactional subscriptions/publications). 
                // It gets added at the publisher on the publication database (distributor). 
                var publisherPushSubscriptionAgentParams = new Dictionary<string, object> { { "publication", publisherDatabase }, { "subscriber", subscriberServer }, { "subscriber_db", subscriberDatabase }, { "subscriber_security_mode", 1 } };
                publishingMaster.Execute("sp_addpushsubscription_agent", publisherPushSubscriptionAgentParams, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

                if (!automaticSyncType) HoldUntilSyncTriggersCreated(subscriberConnection, publishingMaster);

                LinkPublication(distributorServer, publisherServer, publisherDatabase, subscriberConnection);
            }
            finally
            {
                subscriberConnection.Dispose();
            }
        }

        /// <summary>
        /// Holds the until synchronize triggers created. sp_addsubscription will add mssync triggers asyncrhonously. link_publication will deadlock or error
        /// until this async process is complete. This method waits until addsubscription has added all necessary triggers.
        /// </summary>
        /// <param name="subscriberConnection">The spoke.</param>
        /// <param name="publishingMaster">The publishing master.</param>
        private void HoldUntilSyncTriggersCreated(SqlConnection subscriberConnection, SqlConnection publishingMaster)
        {
            using (new TimedScope(s => Trace.TraceInformation("All synchronization triggers have been created on the spoke {0} in {1}.", subscriberConnection.ConnectionString, s)))
            {
                int tableCount = publishingMaster.Execute<int>(@"SELECT COUNT(*) FROM sysarticles a");

                int checkCount = 0;

                var query = @"SELECT COUNT(*)
FROM sys.triggers tr JOIN sys.trigger_events te
ON te.object_id = tr.object_id
JOIN sys.tables t ON t.object_id = tr.parent_id
WHERE tr.name LIKE 'trg_MSsync_%'";
                while (checkCount < 50)
                {
                    Thread.Sleep(15000);

                    var syncTriggers = subscriberConnection.Execute<int>(query);

                    if ((syncTriggers / 3.0) >= tableCount) return;

                    checkCount++;
                }

                throw new ApplicationException("Timed out waiting for sync triggers to be created.");
            }
        }

        private static void LinkPublication(string distributorServer, string publisherServer, string publisherDatabase, SqlConnection subscriberConnection)
        {
            // Add at the subscriber database
            var subscriberParams = new Dictionary<string, object>
            {
                {"publisher", publisherServer}, {"publisher_db", publisherDatabase},
                {"publication", publisherDatabase}, {"distributor", distributorServer}, {"security_mode", 1}
            };
            try
            {
                // Tells the subscriber which publisher to look out for and specifies security information
                // Gets executed at the Subscriber on the subscription database
                DbConnections.Execute(subscriberConnection, "sp_link_publication", subscriberParams, false, () => new SqlCommand { Connection = subscriberConnection, CommandType = CommandType.StoredProcedure, CommandTimeout = subscriberConnection.ConnectionTimeout });
            }
            catch (Exception ex)
            {
                var sqlException = ex.SearchFor<SqlException>();
                if (sqlException == null || sqlException.Number != 18456)
                    throw; // Link is successful however MSSQLServer still complains (Login failed for user 'NT AUTHORITY\ANONYMOUS LOGON'.)
            }
        }

        #endregion

        #region Publication Snapshot

        /// <summary>
        /// Used to start the Snapshot Agent job that generates the snapshot for a publication.
        /// This is executed at the Publisher on the publication database..
        /// </summary>
        /// <param name="publishingMaster">Connection to hub master database</param>
        /// <param name="publisherDatabase">Database for the hub</param>
        private static void StartPublicationSnapshotAgentJob(SqlConnection publishingMaster, string publisherDatabase)
        {
            var publicationSnapshotParams = new Dictionary<string, object> { { "publication", publisherDatabase } };
            publishingMaster.ChangeDatabase(publisherDatabase);
            publishingMaster.Execute("sp_startpublication_snapshot", publicationSnapshotParams, false, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
        }

        /// <summary>
        /// Loop and wait for snapshot to be generated
        /// </summary>
        /// <param name="distributor">Replication Distributor</param>
        /// <param name="publisherDatabase">Database for the hub</param>
        /// <returns>True once the snapshot is completed</returns>
        private bool HoldUntilSnapshotGenerated(ReplicationDistributor distributor, string publisherDatabase)
        {
            using (var distributorConnection = new SqlConnection(distributor.ConnectionString))
            {
                int runstatus;
                do
                {
                    Thread.Sleep(15000); // Pause for 15 seconds
                    runstatus = GetSnapshotAgentStatus(distributorConnection, publisherDatabase);
                } while (runstatus <= 1 || runstatus == 3);

                if (runstatus == 2)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Get the current status of the agent as it generates the snapshot
        /// </summary>
        /// <param name="distributorConnection">Connection to distributor</param>
        /// <param name="publisherDatabase">Database for the hub</param>
        /// <returns>0 = Unknown, 1 = Start, 2 = Succeed, 3 = In progress, 4 = Idle, 5 = Retry, 6 = Fail</returns>
        private static int GetSnapshotAgentStatus(SqlConnection distributorConnection, string publisherDatabase)
        {
            var parameters = new Dictionary<string, object> { { "type", 1 } };
            var dt = DbConnections.Execute<DataTable>(distributorConnection, "sp_MSenum_replication_agents", parameters, false, () => new SqlCommand
            {
                Connection = distributorConnection,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = distributorConnection.ConnectionTimeout
            });

            var status = dt.AsEnumerable().Where(r => r.Get<string>("publisher_db").Equals(publisherDatabase, StringComparison.OrdinalIgnoreCase)).Select(r => r.Get<int>("status")).Select(i => new int?(i)).FirstOrDefault();

            return status ?? -1;
        }

        #endregion

        #region Monitoring

        /// <summary>
        /// Indicate if replication is completed or not
        /// </summary>
        /// <param name="distributor">Replication Distributor</param>
        /// <param name="publisher">Replication Publisher</param>
        /// <returns>If replication is complete</returns>
        private bool IsCompletedAllReplication(ReplicationDistributor distributor, ReplicationPublisher publisher)
        {
            using (var distributorConnection = new SqlConnection(distributor.ConnectionString))
            {
                // Check that snapshot is generated
                if (GetSnapshotAgentStatus(distributorConnection, publisher.Database) == 3)
                    return true;

                // Ensure that replication is completed on subscribers
                if (publisher.SubscriberList == null)
                    return true;
                if (publisher.SubscriberList.Any(s => GetPendingCommandCount(distributorConnection, publisher, s) != 0))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Get the pending commands that need to be executed for the subscriber to be replicated (usually each will take 1 sec to complete)
        /// </summary>
        /// <param name="distributorConnection">Connection to distributor</param>
        /// <param name="publisher">Replication Publisher</param>
        /// <param name="subscriber">Replication Subscriber</param>
        /// <returns>The pending commands count</returns>
        private int GetPendingCommandCount(SqlConnection distributorConnection, ReplicationPublisher publisher, ReplicationSubscriber subscriber)
        {
            var parameters = new Dictionary<string, object>
                                 {
                                     {"publisher", publisher.Server}, {"publisher_db", publisher.Database}, {"publication", publisher.Name},
                                     {"subscriber", subscriber.Server}, {"subscriber_db", subscriber.Database}, {"subscription_type", 0}
                                 };
            var commandInfo = DbConnections.Execute<DataTable>(distributorConnection, "sp_replmonitorsubscriptionpendingcmds", parameters, false, () => new SqlCommand { Connection = distributorConnection, CommandType = CommandType.StoredProcedure, CommandTimeout = distributorConnection.ConnectionTimeout });
            if (commandInfo == null || commandInfo.Rows.Count == 0)
                throw new ApplicationException("Could Not Get Monitoring Information");
            return commandInfo.GetInt32Value("pendingcmdcount");
        }

        #endregion

        #region DatabaseSystemObject

        private List<DbSysObj> GetDatabaseSystemObjects(SqlConnection conn, IEnumerable<string> objectNameList)
        {
            if (objectNameList.IsNullOrEmpty()) return new List<DbSysObj>();

            var list = objectNameList.Select(o => "OBJECT_ID('{0}')".FormatWith(o)).Join(",");
            var query = String.Format(DatabaseSystemObjectQuery + "IN ({0})", list);
            var table = DbConnections.Execute<DataTable>(conn, query);
            if (table == null)
            {
                return null;
            }
            
            return table.Rows.Count == 0 ? new List<DbSysObj>() : (from DataRow row in table.Rows select ParseDatabaseSystemObjectInfo(row)).ToList();
        }

        private static DbSysObj ParseDatabaseSystemObjectInfo(DataRow row)
        {
            if (row == null)
                return null;
            return new DbSysObj
            {
                Name = "[{0}].[{1}]".FormatWith(row.GetStringValue("schemaname"), row.GetStringValue("objectname")),
                ObjectName = row.GetStringValue("objectname"),
                ObjectId = row.GetStringValue("object_id"),
                ParentObjectId = row.GetInt32Value("parent_object_id"),
                Type = row.GetStringValue("type").Trim().ToUpper(),
                TypeDesc = row.GetStringValue("type_desc"),
                CreateDate = row.GetDateTimeValue("create_date"),
                ModifyDate = row.GetDateTimeValue("modify_date"),
                IsMsShipped = row.GetBoolValue("is_ms_shipped"),
                IsPublished = row.GetBoolValue("is_published"),
                IsSchemaPublished = row.GetBoolValue("is_schema_published"),
                SchemaName = row.GetStringValue("schemaname"),
                SchemaId = row.GetInt32Value("schema_id"),
                IsIndexed = row.GetBoolValue("IsIndexed"),
                TableHasIdentity = row.GetBoolValue("TableHasIdentity"),
                IsSchemaBound = row.GetBoolValue("IsSchemaBound"),
                CurrentSeedValue = row.GetInt32Value("CurrentSeedValue"),
                IdentityColumnName = row.GetStringValue("IdentityColumnName"),
                Definition = row.GetStringValue("Definition")
            };
        }

        #endregion

        #region Utilities

        private List<SeedMetadata> LoadSeedMetadataFile()
        {
            var filePath = GetSeedMetadataFilePath();
            if (!String.IsNullOrEmpty(filePath) && File.Exists(filePath))
            {
                var result = FromXml<List<SeedMetadata>>(Soaf.IO.IO.ReadAllText(filePath));

                return result;
            }
            return null;
        }

        private void SaveSeedMetadataFile(List<SeedMetadata> seedMetadata)
        {
            CleanupSeedMetadata();
            var filePath = GetSeedMetadataFilePath();
            if (String.IsNullOrEmpty(filePath))
                return;
            var xml = ToXml(seedMetadata);
            File.WriteAllText(filePath, xml);
        }

        private void CleanupSeedMetadata()
        {
            var filePath = GetSeedMetadataFilePath();
            var newPath = Path.Combine(Path.GetDirectoryName(filePath) ?? string.Empty, string.Format("SeedMetadata_{0}.xml", _now));
            if (!String.IsNullOrEmpty(filePath) && File.Exists(filePath))
            {
                if (File.Exists(newPath)) File.Delete(newPath);
                File.Move(filePath, newPath);
            }
        }

        private string GetSeedMetadataFilePath()
        {
            var fullPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (String.IsNullOrEmpty(fullPath))
                return null;
            var filePath = Path.Combine(fullPath, "SeedMetadata.xml");
            return filePath;
        }

        private static string ToXml(object value)
        {
            var serializer = Serialization.CreateCachedXmlSerializer(value.GetType());
            using (var ms = new MemoryStream())
            {
                serializer.Serialize(ms, value);
                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        private static T FromXml<T>(string s)
        {
            var serializer = Serialization.CreateCachedXmlSerializer(typeof(T));
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(s)))
            {
                return (T)serializer.Deserialize(ms);
            }
        }

        #endregion

        #region Validators

        private bool IsValidDeleteArguments(SqlReplicationConfiguration arguments)
        {
            if (arguments == null)
                return false;
            if (String.IsNullOrWhiteSpace(arguments.HubConnectionString))
                return false;

            return true;
        }

        private static bool IsValidConfigureArguments(SqlReplicationConfiguration arguments)
        {
            if (arguments == null)
                return false;

            if (String.IsNullOrWhiteSpace(arguments.HubConnectionString))
                return false;

            // MUST have DefaultReplicatedObjects or ReplicatedObjects or ReplicationPartnerConnectionStrings (or two or all)
            return (arguments.ObjectsToReplicate.IsNotNullOrEmpty())
                   && (arguments.SpokeConnectionStrings.IsNotNullOrEmpty());
        }

        private bool IsValidSeeding(Seeding seeding)
        {
            if (seeding == null)
                return true;
            if (seeding.ServerStepRange == 0)
                return false;
            if (seeding.SeedingOverrides != null && seeding.SeedingOverrides.Any(so => String.IsNullOrWhiteSpace(so.Name) || so.StepRange == 0))
                return false;
            return true;
        }

        private bool IsValidDistributor(ReplicationDistributor distributor)
        {
            return (distributor != null && distributor.IsInstalled && distributor.IsDbInstalled && (distributor.IsPublisher || distributor.HasRemotePublisher) && !String.IsNullOrEmpty(distributor.Server) && !String.IsNullOrEmpty(distributor.ConnectionString));
        }

        #endregion
    }
}