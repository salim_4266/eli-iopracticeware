﻿DECLARE @objects table(Name nvarchar(max), object_id int)

INSERT INTO @objects (Name)
SELECT 'dbo.PatientReceivablePayments' AS TableName
UNION ALL
SELECT 'dbo.PatientReceivables'
UNION ALL
SELECT 'dbo.ServiceTransactions'
UNION ALL
SELECT 'dbo.PatientFinancial'


DECLARE @sql nvarchar(max)
SET @sql = ''

SELECT @sql = @sql + '
EXEC dbo.DropObject ''' + Name + '''
' 
FROM @objects WHERE OBJECT_ID(Name, 'U') IS NOT NULL

EXEC sp_executesql @statement = @sql

GO

-- The below breaks the RedGate syncer
IF EXISTS (SELECT * FROM sys.indexes where Name = '[PK_PatientRace]')
BEGIN
	ALTER TABLE [model].[PatientRace] DROP CONSTRAINT [[PK_PatientRace]]]
	
	ALTER TABLE [model].[PatientRace] ADD  CONSTRAINT PK_PatientRace PRIMARY KEY NONCLUSTERED 
	(
		[Patients_Id] ASC,
		[Races_Id] ASC
	)
END
