-- BEGIN TRAN

IF OBJECT_ID(N'CreateTableFromCommaSeperatedNvarchar', N'TF') IS NOT NULL
    EXEC('DROP FUNCTION CreateTableFromCommaSeperatedNvarchar')

EXEC('
CREATE FUNCTION [dbo].CreateTableFromCommaSeperatedNvarchar(@input AS NVARCHAR(MAX))
RETURNS
      @Result TABLE([Value] NVARCHAR(MAX))
AS
BEGIN
      DECLARE @segment NVARCHAR(MAX)
      DECLARE @index INT
      IF(@input IS NOT NULL)
      BEGIN
            SET @index = CHARINDEX('','',@input)
            WHILE @index > 0
            BEGIN
                  SET @segment = SUBSTRING(@input,1,@index-1)
                  SET @input = SUBSTRING(@input,@index+1,LEN(@input)-@index)
                  INSERT INTO @Result VALUES (@segment)
                  SET @index = CHARINDEX('','',@input)
            END
            SET @segment = @input
            INSERT INTO @Result VALUES (@segment)
      END
      RETURN
END
')

--DECLARE @objectsToInclude nvarchar(max) = '[dbo].[usp_DI_ExamNoteContent],[dbo].[USP_Surveillance_GetPatientDetails],[dbo].[LogEntryProperties],[dbo].[usp_DI_ExamPatInfo],[dbo].[USP_Surveillance_GetPatientDiagAndCheifCompaint],[dbo].[IE_Events],[dbo].[DropIndexes],[dbo].[usp_DI_ExamPrevDiags],[dbo].[usp_DI_PrevImprPlan],[dbo].[usp_PaymentToDeleted],[dbo].[PracticeCalendar],[dbo].[usp_DI_ProcsBilled],[dbo].[NewPendingPayClaims],[dbo].[PatientDemographics],[dbo].[GetPolicyHolderPatientId],[dbo].[usp_DI_ProcsPerformed],[dbo].[usp_UnassignPayments],[dbo].[Patient_Immunizations],[dbo].[GetPolicyHolderRelationshipType],[dbo].[CharTable],[dbo].[GetSecondPolicyHolderPatientId],[dbo].[VersionInfo],[dbo].[usp_EPgetRenewalRequest],[dbo].[GetServicesByServicePatientIdServiceDate],[dbo].[IsDate112],[dbo].[GetSecondPolicyHolderRelationshipType],[dbo].[IE_Rules],[dbo].[usp_EPInsUpd_PatEPrescription_RenewalRequest],[dbo].[HasAscClaim],[dbo].[usp_EPrescription_Patient_RenewalRequest],[dbo].[PracticeCodeTable],[dbo].[usp_EPUpdateRenewalRequest],[dbo].[Patient_MedicationsXMl],[dbo].[IsResourcePIDUnique],[dbo].[AuditEntries],[dbo].[Encounter_IE_Overrides],[dbo].[usp_FinancialDataDetails],[dbo].[usp_FinancialDataDetails6],[dbo].[PracticeFavorites],[dbo].[USP_Get_Pat_Immunizations],[dbo].[usp_GetEPAccountInfo],[dbo].[PatientClinical],[dbo].[RestoreAuditEntries],[dbo].[usp_GetEPCredentials],[dbo].[usp_GetEPLocationInfo],[dbo].[usp_GetEPPatientInfo],[dbo].[GetInsurerIdInfoByItemIdAppointmentId],[dbo].[DisableEnabledCheckConstraints],[dbo].[usp_GetEPPrescriberInfo],[dbo].[PracticeFeeSchedules],[dbo].[GetNullServices],[dbo].[DisableEnabledTriggers],[dbo].[usp_GetEPStaffInfo],[dbo].[EnableCheckConstraints],[dbo].[PatientClinicalSurgeryPlan],[dbo].[usp_GetMeds],[dbo].[EnableTriggers],[dbo].[AuditEntryChanges],[dbo].[usp_GetPatientAllergyDet],[dbo].[GetMax],[dbo].[GetPracticeInsurersInsurerCrossOver],[dbo].[EPrescription_Credentials],[dbo].[usp_GetPatientDet],[dbo].[SendRecalls],[dbo].[usp_GetSigMapData],[dbo].[PracticeMessages],[dbo].[LocalToUTCDate],[dbo].[USP_Insert_Immunizations],[dbo].[usp_InsertPatientMedXMl],[dbo].[PatientDemoAlt],[dbo].[usp_InsertRenewalReqXML],[dbo].[PracticeInterfaceConfiguration],[dbo].[USp_Lab_GetPatientIds],[dbo].[EventCodes],[dbo].[USP_Lab_GetProviderIds],[dbo].[USP_Lab_GetScheduledProvId],[dbo].[PracticeName],[dbo].[usp_ListLocs],[dbo].[usp_ListStaff],[dbo].[PracticeInsurers],[dbo].[usp_ListStatus],[dbo].[usp_MatchPtnDetails],[dbo].[HL7_LabInterface],[dbo].[IntTable],[dbo].[PracticePayables],[dbo].[IsDate101],[dbo].[USP_NQM_55_DM_with_DilatedExam],[dbo].[RecreateIfDefinitionChanged],[dbo].[USP_NQM_86_POAG],[dbo].[TryConvertDateTime112],[dbo].[USP_NQM_88_GetDiabetic_Retinopathy_MacularEdema],[dbo].[TryConvertDateTime101],[dbo].[USP_NQM_89_DR_Communication_Physician],[dbo].[TryConvertUniqueIdentifier],[dbo].[HL7_LabReports],[dbo].[GetNoAudit],[dbo].[usp_PagedItems],[dbo].[InjectAuditSql],[dbo].[USP_PatientDemographics],[dbo].[PracticeReports],[dbo].[CreateAuditTrigger],[dbo].[GetNoCheck],[dbo].[GetAuditSql],[dbo].[USP_PatientImmunizations],[dbo].[GetNoRecompute],[dbo].[PatientFinancialDependents],[dbo].[usp_PendingSurgery],[dbo].[HL7_Observation],[dbo].[usp_PendingSurgeryAged],[dbo].[collectionprocsPostDate],[dbo].[USP_PQRI_Diabetes_GetMellitus_Dilated_Eye],[dbo].[PracticeServices],[dbo].[collections],[dbo].[USP_PQRI_GetAMD_AREDS_Counseling],[dbo].[collectionsPostDate],[dbo].[USP_PQRI_GetAMD_Dilated_Macular_Examination],[dbo].[PatientNotes],[dbo].[USP_PQRI_GetCataracts_Complications],[dbo].[Appointments],[dbo].[InsurerAging],[dbo].[USP_PQRI_GetCataracts_Visual_Acuity],[dbo].[InsurerAgingPostDate],[dbo].[HL7_Observation_Details],[dbo].[USP_PQRI_GetDiabetic_Retinopathy_Communication_Physician],[dbo].[PatAging],[dbo].[USP_PQRI_GetDiabetic_Retinopathy_MacularEdema],[dbo].[PracticeTransactionJournal],[dbo].[PatAgingPostDate],[dbo].[USP_PQRI_GetHIT_eRX],[dbo].[PrintMonthlyStatementsBulk],[dbo].[AllergyView],[dbo].[USP_PQRI_GetHIT_USE_EHR],[dbo].[production],[dbo].[PatientPreCerts],[dbo].[FDBView],[dbo].[USP_PQRI_GetPOAGDetails],[dbo].[productionPostDate],[dbo].[Charges],[dbo].[usp_NewCrop_InsertNewRx],[dbo].[USP_PQRI_GetPOAG_ReducePressure],[dbo].[PtTransJrnlToPtFinancial],[dbo].[usp_NewCrop_UpdateRx],[dbo].[IE_FieldParams],[dbo].[PtTransJrnlToPtFinancialPostDate],[dbo].[PatientReceivablePayments],[dbo].[USP_RulesEngine_GetFieldParams],[dbo].[PracticeVendors],[dbo].[qryDxBreakdown],[dbo].[USP_RulesEngine_GetRulePhrases],[dbo].[qryMerge],[dbo].[USP_RulesEngine_GetRules],[dbo].[qryPrimIns],[dbo].[usp_Spectacle_Rx],[dbo].[qryProctype],[dbo].[GetIdentCurrent],[dbo].[GetLegacyInvoiceReceivableId],[dbo].[TotalAR],[dbo].[GetPrimaryInsuranceId],[dbo].[IE_RulesPhrases],[dbo].[usp_SurgeryTransactions],[dbo].[CheckOut],[dbo].[GetAdjustmentIdAndConvertAsReceivableId],[dbo].[REPORTFILTERS],[dbo].[ClaimsList],[dbo].[usp_UpdateImgDesc],[dbo].[usp_UpdateImgInstructions],[dbo].[DailyPayments],[dbo].[ServiceTransactions_Internal],[dbo].[usp_UpdatePatientImgDet],[dbo].[DI_ApptDrList],[dbo].[ServiceTransactions],[dbo].[GetUnLinkedPayments],[dbo].[DI_ImpressionsList],[dbo].[Immunizations],[dbo].[DI_LinkNotesList],[dbo].[GetReceivableIdInsurerIdInsurerDesignationByPatientIdInvoiceDateServiceModifier],[dbo].[FinancialSummary],[dbo].[AppointmentType],[dbo].[FinancialSummaryMedical],[dbo].[USP_CMS_GetMedicationReconStatus_Stage2],[dbo].[FinancialSummaryOptical],[dbo].[PatientReceivableServices],[dbo].[USP_CMS_HL7LabOrdersStats],[dbo].[InsuranceAgingDetail],[dbo].[GetInvoiceReceivablePerAppointmentId],[dbo].[usp_FinancialData],[dbo].[USP_CMS_Drug_Interactions_Enabled],[dbo].[MonthlyPatientStatements],[dbo].[LoginUsers],[dbo].[USP_CMS_GetElectronicCopyStatus],[dbo].[GetLinkedBills],[dbo].[USP_CMS_GetMedicationReconStatus_Stage1],[dbo].[tblGrowthChartDetail],[dbo].[GetPayTotals],[dbo].[GetIntTableFromXmlIds],[dbo].[PatientAgingDetail],[dbo].[NegativeSumOfCreditsForInvoice],[dbo].[PatientDocumentList],[dbo].[SumOfDebitsForInvoice],[dbo].[Resources],[dbo].[GetCharTableFromXmlValues],[dbo].[PatientLocator],[dbo].[GetInsurerDisbursementDetails],[dbo].[PatientStatements],[dbo].[GetPatientDisbursementDetails],[dbo].[LoginUsers_Audit],[dbo].[PrecertsList],[dbo].[IsNullOrEmpty],[dbo].[tblGrowthChartMaster],[dbo].[ProcCharges],[dbo].[GetUnLinkedBills],[dbo].[ExtractTextValue],[dbo].[ProcChargesIns],[dbo].[AnyPositiveServices],[dbo].[ChunkWithId],[dbo].[ProcPayments],[dbo].[PatientReferral],[dbo].[ExtractBetweenDelimited],[dbo].[ProcPaymentsIns],[dbo].[GetRowForRowNumber],[dbo].[ProcPaymentsmonth],[dbo].[Lookups],[dbo].[ProcTypeColl],[dbo].[ApplGetServicePaid],[dbo].[GetAuditAndLogEntries],[dbo].[tblMsg],[dbo].[RetrieveClnTransactions],[dbo].[ApplGetServiceBalance],[dbo].[AppointmentTrack],[dbo].[RetrieveCollectionsList],[dbo].[GetFindingDetail],[dbo].[RetrieveInsTransactions],[dbo].[GetUnattachedBills],[dbo].[PayerMapping],[dbo].[RetrieveTransactions],[dbo].[UpdateRcv],[dbo].[RetrieveTransactionType1],[dbo].[GetLinkedPayments],[dbo].[LookupValues],[dbo].[RetrieveTransactionType2],[dbo].[InvoiceTransactionRemark],[dbo].[User_Patients_Restrict],[dbo].[RetrieveTransactionType3],[dbo].[ApplIsServicesChargeZero],[dbo].[CLInventory],[dbo].[RetrieveTransactionTypeR1],[dbo].[GetUnattachedPayments],[dbo].[RetrieveTranscriptions],[dbo].[GetPatientReceivableTotalPaid],[dbo].[PCInventory],[dbo].[SurgerySchedule],[dbo].[CreditListByPatient],[dbo].[VerifyPrpRecord],[dbo].[MedicaidLocationCodes],[dbo].[usp_BillingNotes],[dbo].[GetBillingOfficeForReceivable],[dbo].[Valuesets],[dbo].[USP_BMI_GetPatientDetails],[dbo].[CLOrders],[dbo].[USP_BMI_GROWTHCHARTDETAILS],[dbo].[USP_CCD_GetPatientDetails],[dbo].[Pharmacies],[dbo].[USP_CCD_GetPatientMedsDetails],[dbo].[ClaimsListReverse],[dbo].[USP_CCD_GetPatientProblemDetails],[dbo].[Messaging],[dbo].[USP_CCD_GetPatientProcedureDetails],[dbo].[ZIPCodes],[dbo].[CMSReports],[dbo].[USP_CMS_GetActiveMedicationAllergyList],[dbo].[CreateTableFromCommaSeperatedNvarchar],[dbo].[USP_CMS_GetActiveMedicationList],[dbo].[PhysicianSpi],[dbo].[USP_CMS_GetClinicalSummaryStatus],[dbo].[DropObject],[dbo].[Messaging2],[dbo].[USP_CMS_GetEPrescStatus],[dbo].[LogCategories],[dbo].[USP_CMS_GetMedicationList],[dbo].[CMSReports_Params],[dbo].[usp_FinancialData6],[dbo].[USP_CMS_GetMedicationReconStatus],[dbo].[USP_CMS_GetPatEducationMaterial],[dbo].[PracticeActivity],[dbo].[NewPendingClaims],[dbo].[USP_CMS_GetPatientEAccess],[dbo].[USP_CMS_GetPatVitalSigns],[dbo].[ModifierRules],[dbo].[USP_CMS_GetProblemList],[dbo].[LogEntries],[dbo].[USP_CMS_GetRecordedDemographics],[dbo].[USP_CMS_GetReminderStatus],[dbo].[PracticeAffiliations],[dbo].[USP_CMS_PatientReferralStatus],[dbo].[USP_CMS_SmokeStatus],[dbo].[Patient_EPrescription_RenewalRequest],[dbo].[USP_Del_Immunizations],[dbo].[LogEntryLogCategory],[dbo].[usp_DI_ActivityList],[dbo].[Drug],[dbo].[usp_DI_DocumentImages],[dbo].[DropViewsLike],[dbo].[usp_DI_ExamContent],[dbo].[PracticeAudit],[dbo].[PatientFinancial],[dbo].[usp_DI_ExamHighlightImprs],[dbo].[usp_DI_ExamHighlights],[dbo].[Patient_Images],[dbo].[Chunk],[dbo].[GetPatientReferralsFinancialIds],[dbo].[GetPatientPreCertsFinancialIds],[dbo].[GetPatientReceivablesFinancialIds],[dbo].[Devices],[dbo].[GetBulkPatientPreCertsFinancialIds],[dbo].[GetBulkPatientReferralsFinancialIds],[cdc].[RaceAndEthnicityCodeSet],[cvx].[VaccineAdministered],[df].[Forms],[df].[FormsControls],[df].[FormsLanguage],[df].[FourthForms],[df].[FourthFormsControls],[df].[FourthFormsLanguage],[df].[Questions],[df].[SecondaryForms],[df].[SecondaryFormsControls],[df].[SecondaryFormsLanguage],[df].[ThirdForms],[df].[ThirdFormsControls],[df].[ThirdFormsLanguage],[fdb].[HealthplanDetail],[fdb].[HealthplanSummary],[fdb].[RFMLINM0],[fdb].[tblCompositeAllergy],[fdb].[tblCompositeDrug],[fdb].[tblPharmacy],[fdb].[viewBlackBoxFDB],[fdb].[viewWSDrug],[loinc].[loinc],[loinc].[map_to],[loinc].[source_organization],[model].[AccuracyQualifiers],[model].[Adjustments],[model].[AdjustmentTypes],[model].[AdjustmentTypes_Internal],[model].[AllergenCategorys],[model].[AllergenOccurrences],[model].[AllergenReactionTypes],[model].[Allergens],[model].[AllergySources],[model].[ApplicationSettings],[model].[ApplicationSettingType_Enumeration],[model].[AppointmentCategories],[model].[Appointments],[model].[AppointmentTypes],[model].[AssociatedSignAndSymptom],[model].[AssociatedSignAndSymptomEncounterReasonForVisit],[model].[AuditEntryChangeType_Enumeration],[model].[AxisQualifiers],[model].[BatchBuilderTemplates],[model].[BillingDiagnosis],[model].[BillingOrganizationAddresses],[model].[BillingOrganizationAddressTypes],[model].[BillingOrganizationPhoneNumbers],[model].[BillingOrganizationPhoneNumberTypes],[model].[BillingOrganizations],[model].[BillingServiceBillingDiagnosis],[model].[BillingServiceComments],[model].[BillingServiceModifiers],[model].[BillingServices],[model].[BillingServiceTransactions],[model].[BillingServiceTransactionStatus_Enumeration],[model].[BodyLocations],[model].[BodyPartClinicalSpecialtyType],[model].[BodyParts],[model].[BodySystems],[model].[CalendarComments],[model].[CheckPatientHasPendingAlert],[model].[ChiefComplaintCategories],[model].[ChiefComplaints],[model].[Choices],[model].[ClaimAdjustmentGroupCode_Enumeration],[model].[ClaimAdjustmentReasonAdjustmentTypes],[model].[ClaimAdjustmentReasonCodes],[model].[ClaimCrossOvers],[model].[ClaimDelayReason_Enumeration],[model].[ClaimFileReceivers],[model].[ClaimFilingIndicatorCode_Enumeration],[model].[ClaimFormTypes],[model].[ClinicalAttributes],[model].[ClinicalConditions],[model].[ClinicalConditionStatus_Enumeration],[model].[ClinicalDataSourceTypes],[model].[ClinicalDrawingTemplates],[model].[ClinicalHierarchy_Enumeration],[model].[ClinicalHierarchyClinicalAttribute],[model].[ClinicalHistoryReviewAreas],[model].[ClinicalHistoryReviewTypes],[model].[ClinicalInvoiceProviders],[model].[ClinicalProcedureCategory_Enumeration],[model].[ClinicalProcedures],[model].[ClinicalQualifierCategories],[model].[ClinicalQualifiers],[model].[ClinicalServiceModifiers],[model].[ClinicalServices],[model].[ClinicalSpecialtyTypes],[model].[Comments],[model].[CommunicationCommunicationReviewStatus],[model].[CommunicationMethodType_Enumeration],[model].[CommunicationReviewStatusId_Enumeration],[model].[CommunicationTransactions],[model].[CommunicationTransmissionStatus_Enumeration],[model].[CommunicationWithOtherProviderOrders],[model].[CommunicationWithOtherProviderTreatmentPlan],[model].[ConcernLevel_Enumeration],[model].[ConfirmationStatus],[model].[ContentType_Enumeration],[model].[Contexts],[model].[ControlType_Enumeration],[model].[Countries],[model].[CountryNumberCode_Enumeration],[model].[CreatePatientReceipt],[model].[DiagnosticTestOrderDiagnosticTestPerformed],[model].[DiagnosticTestOrders],[model].[DiagnosticTestOrderTreatmentPlan],[model].[DiagnosticTestPerformedClinicalCondition],[model].[DiagnosticTestPerformeds],[model].[DoctorClinicalSpecialtyTypes],[model].[DoctorInsurerAssignments],[model].[DropModelViews],[model].[DrugDaysSupplies],[model].[DrugDispenseForms],[model].[DrugDosageActions],[model].[DrugDosageForms],[model].[DrugDosageNumbers],[model].[DrugUnitOfMeasurement_Enumeration],[model].[EligibilityConfigurations],[model].[EmailAddresses],[model].[EmailAddressType_Enumeration],[model].[EncounterAdministeredMedications],[model].[EncounterChiefComplaintComments],[model].[EncounterChiefComplaints],[model].[EncounterClinicalDrawings],[model].[EncounterClinicalInstructions],[model].[EncounterCommunicationWithOtherProviderOrderCopiedExternalProvider],[model].[EncounterCommunicationWithOtherProviderOrders],[model].[EncounterDiagnosticTestOrders],[model].[EncounterHistoryOfPresentIllnessComments],[model].[EncounterHistoryOfPresentIllnesses],[model].[EncounterLaboratoryTestOrderClinicalCondition],[model].[EncounterLaboratoryTestOrders],[model].[EncounterLensesPrescriptions],[model].[EncounterMedicationOrders],[model].[EncounterOtherTreatmentPlanOrders],[model].[EncounterPatientEducationClinicalCondition],[model].[EncounterPatientEducations],[model].[EncounterPatientInsuranceReferral],[model].[EncounterProcedureOrders],[model].[EncounterReasonForVisitComments],[model].[EncounterReasonForVisitReasonForVisit],[model].[EncounterReasonForVisits],[model].[EncounterRefractivePrescriptions],[model].[EncounterReviewOfSystemComments],[model].[EncounterReviewOfSystems],[model].[Encounters],[model].[EncounterServiceDrugs],[model].[EncounterServices],[model].[EncounterServiceTypes],[model].[EncounterStatus_Enumeration],[model].[EncounterStatusChangeReasons],[model].[EncounterStatusConfigurations],[model].[EncounterSubsequentVisitOrders],[model].[EncounterTransitionOfCareOrders],[model].[EncounterTreatmentGoalAndInstructions],[model].[EncounterTreatmentPlanComments],[model].[EncounterTypeInvoiceTypes],[model].[EncounterTypes],[model].[EncounterVaccinationOrders],[model].[Equipments],[model].[Ethnicities],[model].[ExamPerformeds],[model].[ExternalApplications],[model].[ExternalContactAddresses],[model].[ExternalContactAddressTypes],[model].[ExternalContactPhoneNumbers],[model].[ExternalContactPhoneNumberTypes],[model].[ExternalContacts],[model].[ExternalOrganizationAddresses],[model].[ExternalOrganizationAddressTypes],[model].[ExternalOrganizationPhoneNumbers],[model].[ExternalOrganizationPhoneNumberTypes],[model].[ExternalOrganizations],[model].[ExternalOrganizationTypes],[model].[ExternalProviderClinicalSpecialtyTypes],[model].[ExternalProviderCommunicationType_Enumeration],[model].[ExternalSystemEntities],[model].[ExternalSystemEntityMappings],[model].[ExternalSystemExternalSystemMessageTypes],[model].[ExternalSystemMessagePracticeRepositoryEntities],[model].[ExternalSystemMessageProcessingStates],[model].[ExternalSystemMessages],[model].[ExternalSystemMessageTypes],[model].[ExternalSystems],[model].[FamilyRelationships],[model].[FeeScheduleContractAmounts],[model].[FeeScheduleContracts],[model].[FinancialBatches],[model].[FinancialInformations],[model].[FinancialInformationTypes],[model].[FinancialSourceType_Enumeration],[model].[FinancialTypeGroups],[model].[FollowUpVisitTreatmentPlan],[model].[Frequencies],[model].[Gender_Enumeration],[model].[GetAccountHistoryDetail],[model].[GetAdjustmentAnalysis],[model].[GetAgeFromDob],[model].[GetAgingReport],[model].[GetBigIdPair],[model].[GetBigPairId],[model].[GetChargesPaymentsAndAdjustmentsDetail],[model].[GetChargesPaymentsAndAdjustmentsWithPaymentMethodAndOpenBalance],[model].[GetClientDateNow],[model].[GetCreditBalance],[model].[GetDailyAdjustments],[model].[GetDailyCharges],[model].[GetDailyDeposit],[model].[GetDailyPatientPayments],[model].[GetDailyPayments],[model].[GetDailySchedule],[model].[GetGlassesReceipt],[model].[GetIdPair],[model].[GetMainOfficeAddress],[model].[GetMasterFeeSchedules],[model].[GetOnAccountPayments],[model].[GetOrphanedAppointments],[model].[GetPairId],[model].[GetPatientBalancesGroupedByInterval],[model].[GetPatientBillHold],[model].[GetPatientInsurers],[model].[GetPatientStatement],[model].[GetPatientsWithoutFutureAppointmentsOrRecalls],[model].[GetPaymentAnalysis],[model].[GetPaymentsAccountHistory],[model].[GetPaymentsByPrimaryInsurers],[model].[GetProcedureAnalysis],[model].[GetRankedPatientSearchResults],[model].[GetReferringDoctorDetail],[model].[GetScheduleEntries],[model].[GetTimeSheet],[model].[GetUnbilledAmount],[model].[GetViewColumnType],[model].[Groups],[model].[GroupUsers],[model].[HistoryOfPresentIllnessCategories],[model].[HistoryOfPresentIllnesses],[model].[Honorific_Enumeration],[model].[HypodermicNeedles],[model].[IdentifierCodes],[model].[IdentifierCodeType_Enumeration],[model].[InjectionSites],[model].[InsurancePolicies],[model].[InsuranceType_Enumeration],[model].[InsurerAddresses],[model].[InsurerAddressTypes],[model].[InsurerBusinessClasses],[model].[InsurerClaimForms],[model].[InsurerEncounterServices],[model].[InsurerFeeScheduleContracts],[model].[InsurerIdentifiers],[model].[InsurerPayType_Enumeration],[model].[InsurerPhoneNumbers],[model].[InsurerPhoneNumberTypes],[model].[InsurerPlanTypes],[model].[Insurers],[model].[InterventionPerformeds],[model].[InvoiceComments],[model].[InvoiceReceivables],[model].[Invoices],[model].[InvoiceSupplementals],[model].[InvoiceType_Enumeration],[model].[IsAppointmentCancelledOrNotUnique],[model].[LaboratoryCollectionSites],[model].[LaboratoryComponents],[model].[LaboratoryMeasuredProperties],[model].[LaboratoryMethodUseds],[model].[LaboratoryNormalRanges],[model].[LaboratoryResultVerifications],[model].[LaboratorySampleSizes],[model].[LaboratoryScaleTypes],[model].[LaboratorySpecimenRejectReasons],[model].[LaboratoryTestAbnormalFlags],[model].[LaboratoryTestingInstruments],[model].[LaboratoryTestingPlaces],[model].[LaboratoryTestingPriorities],[model].[LaboratoryTestOrderLaboratoryTestResult],[model].[LaboratoryTestOrders],[model].[LaboratoryTestOrderTreatmentPlan],[model].[LaboratoryTestResults],[model].[LaboratoryTests],[model].[LaboratoryTimings],[model].[Languages],[model].[Laterality_Enumeration],[model].[MaritalStatus_Enumeration],[model].[MeasurementSystem_Enumeration],[model].[MedicareSecondaryReasonCode_Enumeration],[model].[MedicationClinicalSpecialtyType],[model].[MedicationComments],[model].[MedicationEventType_Enumeration],[model].[MedicationFamilies],[model].[MedicationMedicationFamily],[model].[MedicationOrderAction_Enumeration],[model].[MedicationOrderFavorites],[model].[MedicationProgressComments],[model].[Medications],[model].[MethodSent_Enumeration],[model].[ModifyingFactorEncounterReasonForVisit],[model].[ModifyingFactors],[model].[NewCropUtilizationReports],[model].[NotFoundBehavior_Enumeration],[model].[ObservationTypeCategories],[model].[ObservationTypes],[model].[Organs],[model].[OrganStructureGroups],[model].[OrganStructures],[model].[OrganSubStructures],[model].[OtherOrderTreatmentPlan],[model].[OtherTreatmentPlanOrders],[model].[PatientAddresses],[model].[PatientAddressTypes],[model].[PatientAggregateStatements],[model].[PatientAllergenAllergenReactionTypes],[model].[PatientAllergenComments],[model].[PatientAllergenConcerns],[model].[PatientAllergenDetailAllergySource],[model].[PatientAllergenDetailClinicalQualifier],[model].[PatientAllergenDetails],[model].[PatientAllergens],[model].[PatientBloodPressureClinicalQualifier],[model].[PatientBloodPressures],[model].[PatientClinicalStatus_Enumeration],[model].[PatientClinicalType_Enumeration],[model].[PatientCognitiveStatusAssessmentClinicalQualifier],[model].[PatientCognitiveStatusAssessments],[model].[PatientComments],[model].[PatientCommentType_Enumeration],[model].[PatientCommunicationPreferences],[model].[PatientCommunicationType_Enumeration],[model].[PatientDemographicsFieldConfigurations],[model].[PatientDemographicsList_Enumeration],[model].[PatientDiagnoses],[model].[PatientDiagnosisComments],[model].[PatientDiagnosisDetailAxisQualifiers],[model].[PatientDiagnosisDetailQualifiers],[model].[PatientDiagnosisDetails],[model].[PatientDiagnosticTestPerformeds],[model].[PatientEducationClinicalCondition],[model].[PatientEducations],[model].[PatientEmailAddresses],[model].[PatientEmploymentStatus],[model].[PatientExamPerformeds],[model].[PatientExternalProviders],[model].[PatientFamilyHistoryEntries],[model].[PatientFamilyHistoryEntryDetailClinicalQualifier],[model].[PatientFamilyHistoryEntryDetailFamilyRelationship],[model].[PatientFamilyHistoryEntryDetails],[model].[PatientFinancialComments],[model].[PatientFunctionalStatusAssessmentClinicalQualifier],[model].[PatientFunctionalStatusAssessments],[model].[PatientHeightAndWeightClinicalQualifier],[model].[PatientHeightAndWeights],[model].[PatientInsuranceAuthorizations],[model].[PatientInsuranceDocuments],[model].[PatientInsuranceReferrals],[model].[PatientInsurances],[model].[PatientInterventionPerformeds],[model].[PatientLaboratoryTestResultCopiesSentExternalProvider],[model].[PatientLaboratoryTestResults],[model].[PatientMedicalReviews],[model].[PatientMedicationDetails],[model].[PatientMedicationProgressComments],[model].[PatientMedications],[model].[PatientPhoneNumbers],[model].[PatientProblemConcernLevels],[model].[PatientProcedurePerformeds],[model].[PatientProceduresPerformed],[model].[PatientQuestionSets],[model].[PatientRace],[model].[PatientRecalls],[model].[PatientReferralSources],[model].[PatientReferralSourceTypes],[model].[PatientRelationships],[model].[PatientRelationshipTypeId_Enumeration],[model].[Patients],[model].[PatientSignatureSourceCode_Enumeration],[model].[PatientSmokingStatus],[model].[PatientStatus_Enumeration],[model].[PatientSurgeries],[model].[PatientSurgeryQuestionAnswers],[model].[PatientSurgeryQuestionChoices],[model].[PatientTags],[model].[PatientVaccinations],[model].[PayerMappings],[model].[PaymentMethods],[model].[PaymentOfBenefitsToProvider_Enumeration],[model].[PaymentRequestForms],[model].[PermissionCategory_Enumeration],[model].[Permissions],[model].[PersonNameSuffix_Enumeration],[model].[PolicyHolderRelationshipType_Enumeration],[model].[PracticeCodeTableReferenceType_Enumeration],[model].[PracticeNameType_Enumeration],[model].[PracticeRepositoryEntities],[model].[PracticeVendorType_Enumeration],[model].[Printers],[model].[ProcedureOrderProcedurePerformed],[model].[ProcedureOrders],[model].[ProcedureOrderTreatmentPlan],[model].[ProcedurePerformedClinicalCondition],[model].[ProcedurePerformeds],[model].[PropertyConfigurationKind_Enumeration],[model].[ProviderInsurerClaimFileOverride],[model].[Providers],[model].[Qualities],[model].[QuestionAnswers],[model].[QuestionAnswerValues],[model].[Questions],[model].[Races],[model].[RaiseErrorIfHasDuplicateKey],[model].[RaiseErrorIfHasNulls],[model].[RaiseErrorIfInvalidColumnType],[model].[RaiseErrorIfInvalidForeignKey],[model].[ReasonForVisitCategories],[model].[ReasonForVisitContext],[model].[ReasonForVisitQuality],[model].[ReasonForVisits],[model].[RecallStatus_Enumeration],[model].[RefractivePrescriptions],[model].[RefractivePrescriptionTreatmentPlan],[model].[RefractivePrescriptionType_Enumeration],[model].[RelatedCause_Enumeration],[model].[RelativeTimeTypes],[model].[ReleaseOfInformationCode_Enumeration],[model].[RelevancyQualifiers],[model].[ReportCategory_Enumeration],[model].[Reports],[model].[ReportType_Enumeration],[model].[ResourceServiceCode_Enumeration],[model].[ResourceType_Enumeration],[model].[RevenueCodes],[model].[ReviewOfSystems],[model].[RolePermissions],[model].[Roles],[model].[Rooms],[model].[RouteOfAdministrationClinicalSpecialtyType],[model].[RouteOfAdministrations],[model].[ScheduleBlockAppointmentCategories],[model].[ScheduleBlocks],[model].[ScheduleTemplateBlockAppointmentCategories],[model].[ScheduleTemplateBlocks],[model].[ScheduleTemplateBuilderConfigurations],[model].[ScheduleTemplates],[model].[ScreenQuestion],[model].[Screens],[model].[ScreenTypes],[model].[ServiceLocationAddresses],[model].[ServiceLocationAddressTypes],[model].[ServiceLocationCodes],[model].[ServiceLocationPhoneNumbers],[model].[ServiceLocationPhoneNumberTypes],[model].[ServiceLocations],[model].[ServiceLocations_Partition1],[model].[ServiceModifiers],[model].[ServiceUnitOfMeasurement_Enumeration],[model].[SmokingConditions],[model].[SpecimenConditions],[model].[SpecimenSources],[model].[StateOrProvinces],[model].[SubsequentVisitOrders],[model].[SurgeryTemplates],[model].[SurgeryTemplateScreenQuestions],[model].[SurgeryTypes],[model].[Tags],[model].[TagType_Enumeration],[model].[TaskActivities],[model].[TaskActivityTypes],[model].[TaskActivityUsers],[model].[TaskManagerUserConfigurations],[model].[Tasks],[model].[TemplateDocumentCategory_Enumeration],[model].[TemplateDocumentPrinter],[model].[TemplateDocuments],[model].[TimeFrame_Enumeration],[model].[TimeQualifiers],[model].[Timings],[model].[ToUpperCaseWords],[model].[TransitionOfCareOrders],[model].[TransitionOfCareOrderStatusId_Enumeration],[model].[TransitionOfCareReasons],[model].[TransitionOfCareTreatmentPlan],[model].[TreatmentGoalAndInstructions],[model].[TreatmentGoalClinicalCondition],[model].[TreatmentGoals],[model].[TreatmentPlanClinicalCondition],[model].[TreatmentPlanComments],[model].[TreatmentPlanMedicationOrderFavorite],[model].[TreatmentPlanOrderConfigurations],[model].[TreatmentPlanOrderInstructions],[model].[TreatmentPlanTreatmentGoal],[model].[UnitOfMeasurementTypes],[model].[UnitsOfMeasurement],[model].[UserAddresses],[model].[UserAddressTypes],[model].[UserPermissions],[model].[UserPhoneNumbers],[model].[UserPhoneNumberTypes],[model].[UserRole],[model].[Users],[model].[UserSurgeryTemplatePreferences],[model].[UserTreatmentPlanPatientEducation],[model].[UserTreatmentPlans],[model].[UserTreatmentPlanVaccinationOrder],[model].[USP_CMS_NewCrop],[model].[USP_CMS_Stage1_ClinicalSummary],[model].[USP_CMS_Stage1_CPOEMedications],[model].[USP_CMS_Stage1_Demographics],[model].[USP_CMS_Stage1_eRx],[model].[USP_CMS_Stage1_MedicationAllergyList],[model].[USP_CMS_Stage1_MedicationList],[model].[USP_CMS_Stage1_PatientEducation],[model].[USP_CMS_Stage1_PatientReminders],[model].[USP_CMS_Stage1_ProblemList],[model].[USP_CMS_Stage1_SmokingStatus],[model].[USP_CMS_Stage1_SummaryOfCare],[model].[USP_CMS_Stage1_VitalSigns2013],[model].[USP_CMS_Stage2_ClinicalSummary],[model].[USP_CMS_Stage2_CPOELaboratory],[model].[USP_CMS_Stage2_CPOEMedications],[model].[USP_CMS_Stage2_CPOERadiology],[model].[USP_CMS_Stage2_ElectronicNotes],[model].[USP_CMS_Stage2_eRx],[model].[USP_CMS_Stage2_FamilyHistory],[model].[USP_CMS_Stage2_Imaging],[model].[USP_CMS_Stage2_PatientReminders],[model].[USP_CMS_Stage2_SecureMessage],[model].[USP_CMS_Stage2_SummaryOfCareB],[model].[USP_CMS_Stage2_ViewDownloadTransmitPatientUse],[model].[USP_CMS_Stage2_ViewDownloadTransmitTimelyAccess],[model].[VaccinationAdministereds],[model].[VaccinationAdministeredUnitOfMeasurement],[model].[VaccinationOrders],[model].[VaccineAllergenReactionType],[model].[VaccineDoses],[model].[VaccineManufacturers],[model].[Vaccines],[model].[VaccineTypes],[model].[X12EligibilityPatients],[nci].[DosageForms],[nci].[RouteOfAdministration],[newCrop].[DrugDosageForm],[newCrop].[RouteOfAdministrations],[qdm].[MappingSets],[qdm].[ValueSets],[rxNorm].[RXNATOMARCHIVE],[rxNorm].[RXNCONSO],[rxNorm].[RXNCONSOOCD],[rxNorm].[rxncui],[rxNorm].[RXNCUICHANGES],[rxNorm].[RXNDOC],[rxNorm].[RXNREL],[rxNorm].[RXNSAB],[rxNorm].[RXNSAT],[rxNorm].[RXNSATOCD],[rxNorm].[RXNSTY],[rxNorm].[RXNSTYOCD],[snomed].[ComplexMap],[snomed].[Concept],[snomed].[CORE],[snomed].[Description],[snomed].[ICD9CM_SNOMED_MAP_1TO1],[snomed].[ICD9CM_SNOMED_MAP_1TOM],[snomed].[ModuleDependency],[snomed].[RefsetDescriptor],[snomed].[Relationship]'

DECLARE @objects table(Name nvarchar(max), object_id int)

INSERT INTO @objects(Name, object_id)
SELECT Value, OBJECT_ID(Value) FROM dbo.CreateTableFromCommaSeperatedNvarchar(@objectsToInclude) o

DECLARE @sql nvarchar(max)
SET @sql = ''

PRINT 'DROPPING VIEWS'
WHILE EXISTS (SELECT 
DISTINCT 'DROP VIEW ' + QUOTENAME(SCHEMA_NAME(schema_id)) + '.' + QUOTENAME(v.name) + ';' AS Value
FROM sys.views v
)
BEGIN
	SET @sql = NULL
	SELECT TOP 1 @sql = 'DROP VIEW ' + QUOTENAME(SCHEMA_NAME(schema_id)) + '.' + QUOTENAME(v.name) + ';'
	FROM sys.views v
	ORDER BY LEN(v.name) ASC
	PRINT @sql
	EXEC (@sql)
END

PRINT 'DROPPING TRIGGERS ON SPECIFIED TABLES'
WHILE EXISTS (SELECT 'DROP TRIGGER ' + QUOTENAME(OBJECT_SCHEMA_NAME(o.object_id)) + '.' + QUOTENAME(OBJECT_NAME(o.object_id))+'; ' AS Value
FROM sys.triggers ta
JOIN @objects o ON ta.parent_id = o.object_id
WHERE is_ms_shipped = 0)
BEGIN
	SET @sql = NULL
	SELECT TOP 1 @sql = 'DROP TRIGGER ' + QUOTENAME(OBJECT_SCHEMA_NAME(ta.object_id)) + '.' + QUOTENAME(OBJECT_NAME(ta.object_id))+'; '
	FROM sys.triggers ta
	JOIN @objects o ON ta.parent_id = o.object_id
	WHERE is_ms_shipped = 0
	PRINT @sql
	EXEC (@sql)
END

PRINT 'DROPPING CK CONSTRAINTS ON SPECIFIED TABLES'
WHILE EXISTS (SELECT ('ALTER TABLE [' + TABLE_SCHEMA + '].[' + TABLE_NAME
+ '] DROP CONSTRAINT [' + CONSTRAINT_NAME + '];') AS Value
FROM information_schema.table_constraints tc
WHERE CONSTRAINT_TYPE = 'CHECK')
BEGIN
	SET @sql = NULL
	SELECT TOP 1 @sql = ('ALTER TABLE [' + tc.TABLE_SCHEMA + '].[' + tc.TABLE_NAME
	+ '] DROP CONSTRAINT [' + tc.CONSTRAINT_NAME + '];')
	FROM information_schema.table_constraints tc
	WHERE tc.CONSTRAINT_TYPE = 'CHECK'
	PRINT @sql
	EXEC (@sql)
END

PRINT 'DROPPING FK CONSTRAINTS ON SPECIFIED TABLES'
WHILE EXISTS (SELECT N'ALTER TABLE ' 
  + QUOTENAME(OBJECT_SCHEMA_NAME([parent_object_id]))
  + '.' + QUOTENAME(OBJECT_NAME([parent_object_id])) 
  + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + ';' AS Value
FROM sys.foreign_keys fk
WHERE OBJECTPROPERTY([parent_object_id], 'IsMsShipped') = 0
)
BEGIN
	SET @sql = NULL
	SELECT TOP 1 @sql = N'ALTER TABLE ' 
	  + QUOTENAME(OBJECT_SCHEMA_NAME([parent_object_id]))
	  + '.' + QUOTENAME(OBJECT_NAME([parent_object_id])) 
	  + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + ';'
	FROM sys.foreign_keys fk
	WHERE OBJECTPROPERTY([parent_object_id], 'IsMsShipped') = 0
	PRINT @sql
	EXEC (@sql)
END

PRINT 'DROPPING UQ CONSTRAINTS ON SPECIFIED TABLES'
WHILE EXISTS (SELECT N'ALTER TABLE ' 
  + QUOTENAME(OBJECT_SCHEMA_NAME([parent_object_id]))
  + '.' + QUOTENAME(OBJECT_NAME([parent_object_id])) 
  + ' DROP CONSTRAINT ' + QUOTENAME(kc.name) + ';' AS Value
  ,type
FROM sys.key_constraints kc
WHERE OBJECTPROPERTY([parent_object_id], 'IsMsShipped') = 0
	AND type = 'UQ'
)
BEGIN
	SET @sql = NULL
	SELECT TOP 1 @sql = N'ALTER TABLE ' 
	  + QUOTENAME(OBJECT_SCHEMA_NAME([parent_object_id]))
	  + '.' + QUOTENAME(OBJECT_NAME([parent_object_id])) 
	  + ' DROP CONSTRAINT ' + QUOTENAME(kc.name) + ';'
	FROM sys.key_constraints kc
	WHERE OBJECTPROPERTY([parent_object_id], 'IsMsShipped') = 0
		AND type = 'UQ'
	PRINT @sql
	EXEC (@sql)
END

PRINT 'DROPPING INDEXES ON SPECIFIED TABLES'
WHILE EXISTS (SELECT 
	N'DROP INDEX ' + QUOTENAME(OBJECT_SCHEMA_NAME(o.[object_id])) + '.' + QUOTENAME(OBJECT_NAME(o.[object_id])) + '.' + QUOTENAME(i.NAME) + ';' 
	FROM sys.indexes i
	LEFT JOIN sys.index_columns ic on ic.object_id = i.object_id
		and ic.index_column_id = i.index_id
	LEFT JOIN sys.objects o on o.object_id = i.object_id
	JOIN @objects o2 ON o2.object_id = o.object_id
	WHERE o.is_ms_shipped <> 1
		AND i.is_primary_key <> 1
		AND i.name IS NOT NULL
)
BEGIN
	SET @sql = NULL
	SELECT TOP 1
	@sql = N'DROP INDEX ' + QUOTENAME(OBJECT_SCHEMA_NAME(o.[object_id])) + '.' + QUOTENAME(OBJECT_NAME(o.[object_id])) + '.' + QUOTENAME(i.NAME) + ';' 
	FROM sys.indexes i
	LEFT JOIN sys.index_columns ic on ic.object_id = i.object_id
		and ic.index_column_id = i.index_id
	LEFT JOIN sys.objects o on o.object_id = i.object_id
	JOIN @objects o2 ON o2.object_id = o.object_id
	WHERE o.is_ms_shipped <> 1
		AND i.is_primary_key <> 1
		AND i.name IS NOT NULL
	PRINT @sql
	EXEC (@sql)
END


-- This is the last step performed because there may be objects (columns/check constraints/etc.) that are dependent on a schemabound function.
-- EX: Function: IsResourcePIDUnique.

PRINT 'REMOVING SCHEMABINDING ON SPECIFIED OBJECTS'
SET @sql = NULL

DECLARE @dropDependenciesSql nvarchar(max), @addDependenciesSql nvarchar(max)

;WITH CTE AS (
SELECT 'EXEC dbo.DropSchemabinding ' + CONVERT(nvarchar(max), o.object_id) AS Value
FROM sys.objects o
JOIN @objects o2 ON o2.object_id = o.object_id
WHERE OBJECTPROPERTY(o.object_id, 'IsSchemaBound') = 1
)
SELECT DISTINCT @sql =
	LTRIM(STUFF((SELECT '; ' + CTE.Value
		FROM CTE
		FOR XML PATH(''), type
			).value('.', 'nvarchar(max)')
			, 1, 1, ' '))
	FROM CTE

EXEC sp_executesql @statement = @sql

--ROLLBACK