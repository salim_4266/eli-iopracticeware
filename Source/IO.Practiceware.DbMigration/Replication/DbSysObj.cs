using System;

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    /// Wrapping the underlining metadata of a database system item 
    /// </summary>
    public class DbSysObj
    {
        public string Name { get; set; }
        public string ObjectName { get; set; }
        public string ObjectId { get; set; }
        public int ParentObjectId { get; set; }
        public string Type { get; set; }
        public string TypeDesc { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool IsMsShipped { get; set; }
        public bool IsPublished { get; set; }
        public bool IsSchemaPublished { get; set; }
        public string SchemaName { get; set; }
        public int SchemaId { get; set; }
        public bool IsIndexed { get; set; }
        public bool TableHasIdentity { get; set; }
        public bool IsSchemaBound { get; set; }
        public int CurrentSeedValue { get; set; }
        public string IdentityColumnName { get; set; }
        public string Definition { get; set; }
    }
}