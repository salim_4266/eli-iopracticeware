using System.Collections.Generic;

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    /// Provides the default step range value to use for seeding and any overrides per object
    /// </summary>
    public class Seeding
    {
        public long ServerStepRange { get; set; }
        public List<SeededObject> SeedingOverrides { get; set; }
    }
}