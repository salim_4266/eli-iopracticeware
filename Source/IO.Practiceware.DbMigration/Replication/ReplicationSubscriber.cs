using System;
using System.Collections.Generic;

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    /// Contains a subscriber information and the underlining metadata of the database system objects that are defined for replication
    /// </summary>
    public class ReplicationSubscriber : IEquatable<ReplicationSubscriber>
    {
        public string ConnectionString { get; set; } // Connection string to spoke server server and database (internally determined)
        public string Server { get; set; }
        public string Database { get; set; }
        public List<DbSysObj> DatabaseSystemObjects { get; set; }

        #region IEquatable<ReplicationSubscriber> Members

        public bool Equals(ReplicationSubscriber other)
        {
            return other != null && (Server.Equals(other.Server, StringComparison.OrdinalIgnoreCase) && Database.Equals(other.Database, StringComparison.OrdinalIgnoreCase));
        }

        #endregion

        public override int GetHashCode()
        {
            return Server.GetHashCode() ^ Database.GetHashCode();
        }

        public override bool Equals(Object obj)
        {
            var other = obj as ReplicationSubscriber;
            return other != null && Equals(other);
        }
    }
}