using System;
using System.Data.SqlClient;

namespace IO.Practiceware.DbMigration.Replication
{
    internal static class SqlConnectionExtensions
    {
        public static string GetDatabase(this string connectionString)
        {
            return new SqlConnectionStringBuilder(connectionString).InitialCatalog;
        }

        public static string GetServer(this string connectionString)
        {
            return new SqlConnectionStringBuilder(connectionString).DataSource;
        }

        public static string ReplaceServer(this string connectionString, string newServer)
        {
            var b = new SqlConnectionStringBuilder(connectionString);
            b.DataSource = newServer;
            return b.ToString();
        }

        public static string ReplaceDatabase(this string connectionString, string newDatabase)
        {
            var b = new SqlConnectionStringBuilder(connectionString);
            b.InitialCatalog = newDatabase;
            return b.ToString();
        }

        public static SqlConnection ToSqlConnection(this string connectionString)
        {
            if (String.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentException("Connection String Is Invalid");

            return new SqlConnection(connectionString);
        }

        public static SqlConnection ReplaceServer(this SqlConnection sqlConnection, string newServer)
        {
            sqlConnection.ConnectionString = sqlConnection.ConnectionString.ReplaceServer(newServer);
            return sqlConnection;
        }

        public static SqlConnection ReplaceDatabase(this SqlConnection sqlConnection, string newDatabase)
        {
            sqlConnection.ConnectionString = sqlConnection.ConnectionString.ReplaceDatabase(newDatabase);
            return sqlConnection;
        }

        public static SqlConnectionStringBuilder ToSqlConnectionStringBuilder(this string connectionString)
        {
            return new SqlConnectionStringBuilder(connectionString);
        }

        public static SqlConnection OpenAndReturn(this SqlConnection sqlConnection)
        {
            sqlConnection.Open();
            return sqlConnection;
        }

        /// <summary>
        /// Returns a connection to the master database given the hub connecton string
        /// </summary>
        /// <param name="connectionString">Hub connection string</param>
        /// <returns>Connection to master</returns>
        public static SqlConnection ToMasterDatabaseConnection(this string connectionString)
        {
            return connectionString.ReplaceDatabase("master").ToSqlConnection();
        }
    }
}