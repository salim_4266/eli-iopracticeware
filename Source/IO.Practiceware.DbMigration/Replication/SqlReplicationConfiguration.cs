using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.DbMigration.Replication
{
    /// <summary>
    ///   Information about how to configure sql replication.
    /// </summary>
    public class SqlReplicationConfiguration
    {
        private IEnumerable<string> _objectsToReplicate;

        /// <summary>
        /// Sets the
        /// <see cref="ISqlReplicationManager" /> replication manager object and if possible
        /// queries the
        /// <see cref="HubConnectionString" /> hub database for existing replicated objects and
        /// existing spoke server (subscribers) connection strings. If successful, will set the
        /// <see cref="SpokeConnectionStrings" /> object.
        /// </summary>
        /// <param name="sqlReplicationManager">The SQL replication manager.</param>
        /// <remarks>
        /// If there was an existing replication setup, the <see cref="ReplicationPreviouslyExisted" /> is set to true.
        /// </remarks>
        public void SetReplicationManager(ISqlReplicationManager sqlReplicationManager)
        {
            if (ReplicationManager == null && sqlReplicationManager != null)
            {
                var replicationValues = sqlReplicationManager.GetReplicatedObjectsAndSpokeConnectionStringsFromHubServer(HubConnectionString);
                if (replicationValues != null)
                {
                    ExistingReplicatedObjects = replicationValues.Item1;
                    SpokeConnectionStrings = replicationValues.Item2;
                    SeedMetadata = sqlReplicationManager.LoadSeedMetadata(this);
                }

                ReplicationPreviouslyExisted = replicationValues != null;
                ReplicationManager = sqlReplicationManager;
            }
        }

        /// <summary>
        /// <seealso cref="ISqlReplicationManager"/>
        /// </summary>
        public ISqlReplicationManager ReplicationManager { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not replication was already existing.
        /// </summary>
        public bool ReplicationPreviouslyExisted { get; private set; }

        /// <summary>
        /// Gets or sets a value that indicates whether or not a distributor in a replication environment was already setup and is valid
        /// </summary>
        public bool DistributorPreviouslyExistedAndIsValid { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="Seeding"/> value
        /// </summary>
        public Seeding Seeding { get; set; }

        /// <summary>
        ///   Connection strings for each of the spoke in replication.
        /// </summary>
        /// <value> The spoke connection strings. </value>
        public IEnumerable<string> SpokeConnectionStrings { get; set; }

        /// <summary>
        ///   Gets or sets the hub (main server) database connection string.
        /// </summary>
        /// <value> The hub connection string. </value>
        public string HubConnectionString { get; set; }

        /// <summary>
        /// Determines whether or not to use snapshots for replication
        /// </summary>
        public bool UseSnapshotForReplication { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not to use the DbSync utility to setup replication.
        /// </summary>
        /// <remarks>
        /// If <see cref="UseSnapshotForReplication"/> is false and <see cref="UseDbSyncForReplication"/> is true,
        /// the migrator will run the DbSync utility to perform replication.  The DbSync utility performs a 
        /// manually comarison against the main hub server and it's replicated servers.  It generates
        /// a script which can then be ran automatically or saved to a local directory for manual execution.
        /// </remarks>        
        public bool UseDbSyncForReplication { get; set; }

        /// <summary>
        /// Gets or sets a value to indicate whether or not the DbSync utility's generated scripts get executed automatically
        /// </summary>
        /// <remarks>
        /// If <see cref="UseDbSyncForReplication"/> is true and <see cref="ExecuteDbSyncScriptAutomatically"/> is set to true, when the migrator tries to 
        /// setup replication using the DbSync utility, it will execute the generated scripts automatically rather than saving them
        /// to a directory for future manual execution.
        /// </remarks>        
        public bool ExecuteDbSyncScriptAutomatically { get; set; }

        /// <summary>
        /// Gets or sets a value representing a path to use when the DbSync utility creates and saves
        /// the synchronizer scripts.
        /// </summary>
        /// <remarks>
        /// If <see cref="UseDbSyncForReplication"/> is true and a valid path is provided here,
        /// the DbSync utility will save the generated scripts in this location. If no path is
        /// provided, the scripts will be saved in the same directory as the executing process.
        /// </remarks>        
        public string DbSyncGeneratedScriptPath { get; set; }

        /// <summary>
        ///   Gets or sets the existing replicated objects.
        /// </summary>
        /// <value> The replicated objects. </value>
        public IEnumerable<string> ExistingReplicatedObjects { get; private set; }

        /// <summary>
        /// Gets the list of objects replicated by default.
        /// </summary>
        public IEnumerable<string> DefaultReplicatedObjects { get; set; }

        /// <summary>
        /// Gets the objects for replication. A combination of existing replicated objects plus default ones (unless overriden).
        /// </summary>
        /// <value>
        /// The objects for replication.
        /// </value>
        public IEnumerable<string> ObjectsToReplicate
        {
            get { return _objectsToReplicate ?? (ExistingReplicatedObjects ?? new string[0]).Union((DefaultReplicatedObjects ?? new string[0])).ToArray(); }
            set { _objectsToReplicate = value; }
        }

        /// <summary>
        /// Gets or sets the seed metadata.
        /// </summary>
        /// <value>
        /// The seed metadata.
        /// </value>
        public IEnumerable<SeedMetadata> SeedMetadata { get; set; }

        /// <summary>
        /// Adds replication to a set of servers, either a new setup or modifying an existing, seeding optional
        /// </summary>
        /// <remarks>
        /// Checks to see if there is a valid distributor setup. If not, it will create a new replication environment 
        /// otherwise it will re-configure an existing one (setting up publication and subscriptions and creating an initial snapshot)
        /// </remarks>
        public void ConfigureReplication()
        {
            ReplicationManager.Configure(this);
        }
    }
}