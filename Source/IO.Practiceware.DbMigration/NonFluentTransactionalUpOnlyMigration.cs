﻿using System;
using System.Data;
using FluentMigrator;
using FluentMigrator.Infrastructure;
using Soaf;
using Soaf.Reflection;

namespace IO.Practiceware.DbMigration
{
    public abstract class NonFluentTransactionalUpOnlyMigration : Migration, IMigrationInfo
    {
        private readonly MigrationAttribute _migrationAttribute;

        /// <summary>
        /// Initializes a new instance of the <see cref="NonFluentTransactionalUpOnlyMigration"/> class.
        /// </summary>
        protected NonFluentTransactionalUpOnlyMigration()
        {
            _migrationAttribute = GetType().GetAttribute<MigrationAttribute>(false, false)
                .EnsureNotDefault("Default constructor can only be used with [Migration] on derived class");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NonFluentTransactionalUpOnlyMigration"/> class.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <param name="description">The description.</param>
        /// <param name="transactionBehavior">The transaction behavior.</param>
        protected NonFluentTransactionalUpOnlyMigration(long version, string description = null, TransactionBehavior transactionBehavior = TransactionBehavior.Default)
        {
            _migrationAttribute = new MigrationAttribute(version, transactionBehavior, description);
        }

        /// <summary>
        /// Performs up migration using given connection and transaction
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="transaction">The transaction.</param>
        protected abstract void MigrateUp(IDbConnection connection, IDbTransaction transaction);

        public override void Up()
        {
            Execute.WithConnection((connection, transaction) =>
            {
                // In case automatic transaction handling is disabled
                var manualTransaction = false;
                if (transaction == null)
                {
                    transaction = connection.BeginTransaction();
                    manualTransaction = true;
                }

                try
                {
                    MigrateUp(connection, transaction);

                    if (manualTransaction)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception)
                {
                    if (manualTransaction)
                    {
                        transaction.Rollback();
                    }
                    throw;
                }
                finally
                {
                    if (manualTransaction)
                    {
                        transaction.Dispose();
                    }
                }
 
            });
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }

        public virtual object Trait(string name)
        {
            return null;
        }

        public virtual bool HasTrait(string name)
        {
            return false;
        }

        public virtual string GetName()
        {
            return GetType().Name;
        }

        public long Version { get { return _migrationAttribute.Version; } }
        public string Description { get { return _migrationAttribute.Description; } }
        public TransactionBehavior TransactionBehavior { get { return _migrationAttribute.TransactionBehavior; } }
        public IMigration Migration { get { return this; } }
    }
}