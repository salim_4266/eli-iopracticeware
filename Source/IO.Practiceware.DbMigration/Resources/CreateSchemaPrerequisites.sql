﻿--Add data type Time to sql 2005

IF NOT EXISTS (SELECT * FROM sys.types WHERE name = 'time')
BEGIN
	
	EXEC('CREATE TYPE time FROM dateTime')

	EXEC('CREATE RULE TimeOnlyRule AS DATEDIFF(dd,0,@DateTime) = 0')
		
	EXEC sp_bindrule 'TimeOnlyRule', 'time'
END

GO

IF NOT EXISTS (SELECT * FROM sys.types WHERE name = 'date')
BEGIN

	EXEC('CREATE TYPE date FROM dateTime')
	EXEC('CREATE RULE DateOnlyRule AS DATEADD(dd,DATEDIFF(dd,0,@DateTime),0) = @DateTime')

	EXEC sp_bindrule 'DateOnlyRule', 'date'

END

--This is required because the dbo.date is used by this function in one version. 
IF EXISTS (SELECT * FROM sys.objects so
			INNER JOIN sys.schemas sc ON so.schema_id = sc.schema_id
			WHERE so.name = 'GetAgeFromDoB' AND sc.name = 'model'
			AND OBJECT_DEFINITION(so.object_id) LIKE '%AS DATE)%'
			)
BEGIN
	DROP FUNCTION model.GetAgeFromDoB
END

IF EXISTS (
	SELECT * FROM sys.types st
	INNER JOIN sys.schemas sc ON st.schema_id = sc.schema_id
	WHERE st.name = 'time' AND sc.name = 'sys')
	AND  EXISTS (
	SELECT * FROM sys.types st
	INNER JOIN sys.schemas sc ON st.schema_id = sc.schema_id
	WHERE st.name = 'time' AND sc.name = 'dbo')
BEGIN
	DROP TYPE dbo.time
	DROP RULE TimeOnlyRule
END

IF EXISTS (
	SELECT * FROM sys.types st
	INNER JOIN sys.schemas sc ON st.schema_id = sc.schema_id
	WHERE st.name = 'date' AND sc.name = 'sys')
	AND  EXISTS (
	SELECT * FROM sys.types st
	INNER JOIN sys.schemas sc ON st.schema_id = sc.schema_id
	WHERE st.name = 'date' AND sc.name = 'dbo')
BEGIN
	--This is required because the dbo.date is used by this function in one version. 
	IF EXISTS (SELECT * FROM sys.objects so
				INNER JOIN sys.schemas sc ON so.schema_id = sc.schema_id
				WHERE so.name = 'GetAgeFromDoB' AND sc.name = 'model'
				)
	BEGIN
		DROP FUNCTION model.GetAgeFromDoB
	END

	DROP TYPE dbo.date
	DROP RULE DateOnlyRule
END

IF SCHEMA_ID('model') IS NULL EXEC('CREATE SCHEMA model')

IF NOT EXISTS (SELECT Name FROM Sys.Synonyms WHERE Name = 'NoCheck')
CREATE SYNONYM [dbo].[NoCheck] FOR #NoCheck

GO

IF NOT EXISTS (SELECT Name FROM Sys.Synonyms WHERE Name = 'NoRecompute')
CREATE SYNONYM [dbo].[NoRecompute] FOR #NoRecompute

GO

IF NOT EXISTS (SELECT Name FROM Sys.Synonyms WHERE Name = 'NoAudit')
CREATE SYNONYM [dbo].[NoAudit] FOR #NoAudit

GO

IF EXISTS (SELECT Name FROM Sys.Synonyms WHERE Name = 'UserContext')
	DROP SYNONYM model.usercontext;
GO

CREATE SYNONYM [model].[UserContext] FOR #UserContext

GO
IF OBJECT_ID('[dbo].[GetObjectsInDependencyOrder]') IS NOT NULL
	DROP FUNCTION [dbo].GetObjectsInDependencyOrder

GO


CREATE FUNCTION [dbo].[GetObjectsInDependencyOrder](@objectIdFilter int)
RETURNS @objects table(ObjectId int, ObjectName nvarchar(max))
AS
BEGIN	
-- Does everything to provide a script to safely drop and recreate schemabinding (taking dependencies into account)

DECLARE @dependentObjects table(DependentObjectId INT, DependentObjectName nvarchar(max), ReferencedObjectId int, ReferencedObjectName nvarchar(max), RowNumber int)

;WITH DependentObjectCTE (DependentObjectId, DependentObjectName, ReferencedObjectId, ReferencedObjectName)
		AS
		(
		SELECT DISTINCT
			sd.object_id,
			CASE WHEN OBJECTPROPERTY(sd.object_id, 'IsConstraint') = 0 THEN '[' + ds.name + '].[' + do.name + ']' ELSE ds.name END,
			ReferencedObjectId = sd.referenced_major_id,
			ReferencedObject = CASE WHEN OBJECTPROPERTY(sd.referenced_major_id, 'IsConstraint') = 0 THEN '[' + rs.name + '].[' + ro.name + ']' END
		FROM    
			sys.sql_dependencies sd
			JOIN sys.objects ro ON sd.referenced_major_id = ro.object_id
			JOIN sys.schemas rs ON ro.schema_id = rs.schema_id
			JOIN sys.objects do ON sd.object_id = do.object_id
			JOIN sys.schemas ds ON do.schema_id = ds.schema_id
		WHERE @objectIdFilter IS NULL OR sd.referenced_major_id = @objectIdFilter
		UNION ALL
		SELECT
			sd.object_id,
			CASE WHEN OBJECTPROPERTY(sd.object_id, 'IsConstraint') = 0 THEN '[' + ds.name + '].[' + do.name + ']' ELSE do.name END,
			sd.referenced_major_id,
			ReferencedObject = CASE WHEN OBJECTPROPERTY(sd.referenced_major_id, 'IsConstraint') = 0 THEN '[' + rs.name + '].[' + ro.name + ']' END
		FROM    
			sys.sql_dependencies sd
			JOIN sys.objects ro ON sd.referenced_major_id = ro.object_id
			JOIN sys.schemas rs ON ro.schema_id = rs.schema_id
			JOIN sys.objects do ON sd.object_id = do.object_id
			JOIN sys.schemas ds ON do.schema_id = ds.schema_id	
			JOIN DependentObjectCTE doCTE ON sd.referenced_major_id = doCTE.DependentObjectId       
		WHERE
			sd.referenced_major_id <> sd.object_id     
			AND ((OBJECTPROPERTY(sd.object_id, 'IsSchemaBound') = 1 AND OBJECT_DEFINITION(sd.object_id) IS NOT NULL) OR OBJECTPROPERTY(sd.object_id, 'IsUserTable') = 1)
		)
INSERT INTO @dependentObjects 
SELECT * FROM (
SELECT *, ROW_NUMBER() OVER (ORDER BY DependentObjectId) AS RowNumber FROM DependentObjectCTE
WHERE ReferencedObjectId <> DependentObjectId
) q
ORDER BY q.RowNumber

DECLARE @endOfChain bit 
DECLARE @currentObjectId int
DECLARE @currentCount int
DECLARE @initialCount int
SELECT @initialCount = COUNT(*) FROM @dependentObjects

WHILE EXISTS(SELECT * FROM @dependentObjects)
BEGIN
	SELECT @currentObjectId = (SELECT TOP 1 DependentObjectId FROM @dependentObjects ORDER BY RowNumber)

	SELECT @currentCount = COUNT(*) FROM @dependentObjects

	--PRINT CAST(@currentCount AS nvarchar(max)) + ' objects remaining'

	SELECT @endOfChain = CASE WHEN EXISTS(SELECT * FROM @dependentObjects do WHERE do.ReferencedObjectId = @currentObjectId)
	THEN 0 
	ELSE 1 END
	
	IF @endOfChain = 1
	BEGIN
		--PRINT 'End of chain at ' + OBJECT_NAME(@currentObjectId)

		INSERT INTO @objects
		SELECT TOP 1 DependentObjectId, '[' + OBJECT_SCHEMA_NAME(DependentObjectId) + '].[' + OBJECT_NAME(DependentObjectId) + ']' FROM @dependentObjects ORDER BY RowNumber
	
		DELETE FROM @dependentObjects WHERE DependentObjectId = @currentObjectId
	END
	ELSE
	BEGIN
		--PRINT 'Pushing ' + OBJECT_NAME(@currentObjectId) + ' to end of queue'
		UPDATE @dependentObjects 
		SET RowNumber = (SELECT MAX(RowNumber) + 1 FROM @dependentObjects)
		WHERE DependentObjectId = @currentObjectId
	END

END

RETURN 
END

GO

IF OBJECT_ID('[dbo].[GetDropAndAddSchemabindingSql]') IS NOT NULL
	DROP PROCEDURE [dbo].[GetDropAndAddSchemabindingSql]

GO


IF OBJECT_ID('[dbo].[GetDropAndAddSchemaboundDependenciesSql]') IS NOT NULL
	DROP PROCEDURE [dbo].[GetDropAndAddSchemaboundDependenciesSql]

GO

CREATE PROCEDURE [dbo].[GetDropAndAddSchemaboundDependenciesSql](@objectIdFilter int, @dropSql nvarchar(max) OUTPUT, @addSql nvarchar(max) OUTPUT)
AS
BEGIN	
-- Does everything to provide a script to safely drop and recreate schemabinding (taking dependencies into account)
SET NOCOUNT ON 

--DECLARE @objectIdFilter int

DECLARE @dropSchemabindingSql nvarchar(max)
SET @dropSchemabindingSql = ''

DECLARE @dropConstraintsSql nvarchar(max)
SET @dropConstraintsSql = ''

DECLARE @dropComputedColumnsSql nvarchar(max)
SET @dropComputedColumnsSql = ''

DECLARE @addSchemabindingSql nvarchar(max)
SET @addSchemabindingSql = ''

DECLARE @addConstraintsSql nvarchar(max)
SET @addConstraintsSql = ''

DECLARE @addComputedColumnsSql nvarchar(max)
SET @addComputedColumnsSql = ''

IF OBJECT_ID('tempdb..#Objects') IS NOT NULL
	DROP TABLE #Objects

CREATE TABLE #Objects(ObjectId int, ObjectName nvarchar(max))

INSERT INTO #Objects(ObjectId, ObjectName)
SELECT ObjectId, ObjectName FROM
[dbo].[GetObjectsInDependencyOrder](@objectIdFilter)

SELECT @dropConstraintsSql = @dropConstraintsSql + 'EXEC(''ALTER TABLE [' + OBJECT_SCHEMA_NAME(ck.parent_object_id) + '].[' + OBJECT_NAME(ck.parent_object_id) + '] DROP CONSTRAINT ' + OBJECT_NAME(o.ObjectId) + ''');'
, @addConstraintsSql = @addConstraintsSql + 'EXEC(''ALTER TABLE [' + OBJECT_SCHEMA_NAME(ck.parent_object_id) + '].[' + OBJECT_NAME(ck.parent_object_id) + '] ADD CONSTRAINT ' + OBJECT_NAME(o.ObjectId) + ' CHECK ' + REPLACE(OBJECT_DEFINITION(o.ObjectId),'''', '''''') + ''');'
FROM #Objects o
JOIN sys.check_constraints ck ON ck.object_id = o.ObjectId
WHERE OBJECT_DEFINITION(o.ObjectId) IS NOT NULL

SELECT @dropComputedColumnsSql = @dropComputedColumnsSql + 'EXEC(''ALTER TABLE [' + OBJECT_SCHEMA_NAME(o.ObjectId) + '].[' + OBJECT_NAME(o.ObjectId) + '] DROP COLUMN [' + cc.name + ']'');'
, @addComputedColumnsSql = @addComputedColumnsSql + 'EXEC(''ALTER TABLE [' + OBJECT_SCHEMA_NAME(o.ObjectId) + '].[' + OBJECT_NAME(o.ObjectId) + '] ADD [' + cc.name + '] AS ' + REPLACE(cc.definition,'''', '''''') + 
(CASE WHEN cc.is_persisted = 1 THEN 'PERSISTED' ELSE '' END) + ''');'
FROM #Objects o
JOIN sys.computed_columns cc ON o.ObjectId = cc.object_id

SELECT
 @dropSchemabindingSql = @dropSchemabindingSql +  
'EXEC(''' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(OBJECT_DEFINITION(d.ObjectId), '''', ''''''), 'WITH SCHEMABINDING,', 'WITH'), 'WITH SCHEMABINDING', ''), ',WITH SCHEMABINDING', ''), 'CREATE ', 'ALTER ') + ''');'
,
@addSchemabindingSql = @addSchemabindingSql + 
'EXEC(''' + REPLACE(REPLACE(OBJECT_DEFINITION(d.ObjectId), '''', ''''''), 'CREATE ', 'ALTER ') + ''');'
	FROM #Objects d
WHERE OBJECTPROPERTY(d.ObjectId, 'IsSchemaBound') = 1 AND OBJECT_DEFINITION(d.ObjectId) IS NOT NULL

SET @dropSql = @dropConstraintsSql + @dropComputedColumnsSql + @dropSchemabindingSql

SET @addSql = @addSchemabindingSql + @addComputedColumnsSql + @addConstraintsSql

END
--PRINT ('Dropping check constraints')
--EXEC sp_executesql @statement = @dropConstraintsSql
--PRINT ('Dropped check constraints')

--PRINT ('Dropping computed columns')
--EXEC sp_executesql @statement = @dropComputedColumnsSql
--PRINT ('Dropped computed columns')


--PRINT ('Dropping SCHEMABINDING')
--EXEC sp_executesql @statement = @dropSchemabindingSql
--PRINT ('Dropped SCHEMABINDING')

--PRINT ('Adding SCHEMABINDING')
--EXEC sp_executesql @statement = @addSchemabindingSql
--PRINT ('Added SCHEMABINDING')

--PRINT ('Adding computed columns')
--EXEC sp_executesql @statement = @addComputedColumnsSql
--PRINT ('Added computed columns')

--PRINT ('Adding check constraints')
--EXEC sp_executesql @statement = @addConstraintsSql
--PRINT ('Added check constraints')

GO



IF OBJECT_ID('[dbo].[DropSchemabinding]') IS NOT NULL
	DROP PROCEDURE [dbo].[DropSchemabinding]

GO

CREATE PROCEDURE [dbo].[DropSchemabinding](@objectId int)
AS
BEGIN	
	DECLARE @dropSql nvarchar(max) 
	DECLARE @addSql nvarchar(max) 

	EXEC dbo.GetDropAndAddSchemaboundDependenciesSql @objectidfilter = @objectid, @dropSql = @dropSql OUTPUT, @addSql = @addSql OUTPUT

	EXEC sp_executesql @statement = @dropSql
END

GO

IF OBJECT_ID('[dbo].[TryConvertBigint]') IS NULL
EXEC('
	CREATE FUNCTION [dbo].[TryConvertBigint] (@value nvarchar(20))
	RETURNS BIGINT
		WITH SCHEMABINDING
	AS
	BEGIN
		IF @value IS NULL RETURN NULL

		DECLARE @strippedValue nvarchar(20)
		SET @strippedValue = REPLACE(REPLACE(@value, ''['', ''''),'']'','''')
	
		RETURN CASE WHEN LEN(@strippedValue) BETWEEN 1 AND 19 AND CONVERT(varchar(19), @strippedValue) NOT LIKE ''%[^0-9]%'' AND RIGHT(REPLICATE(''0'', 19) + @strippedValue, 19) < ''9223372036854775807'' THEN CONVERT(bigint, CONVERT(varchar(19), @strippedValue)) ELSE NULL END
	END')

	IF SCHEMA_ID('qrda') IS NULL EXEC('CREATE SCHEMA [qrda]')