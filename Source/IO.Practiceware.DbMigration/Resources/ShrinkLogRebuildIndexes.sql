USE [msdb]
GO

/****** Object:  Job [ShrinkLogRebuildIndexes]    Script Date: 8/14/2014 12:47:25 PM ******/
IF EXISTS (SELECT Name FROM msdb.dbo.sysjobs WHERE Name = 'ShrinkLogRebuildIndexes')
	EXEC msdb.dbo.sp_delete_job @job_name=N'ShrinkLogRebuildIndexes', @delete_unused_schedule=1
	
/****** Object:  Job [ShrinkLogRebuildIndexes]    Script Date: 8/14/2014 12:47:25 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 8/14/2014 12:47:25 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'ShrinkLogRebuildIndexes', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Job to perform two tasks - Shrink the log file and Rebuild the Indexes', 
		@category_name=N'[Uncategorized (Local)]', 
		@job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
/****** Object:  Step [ShrinkLog]    Script Date: 8/14/2014 12:47:26 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'ShrinkLog', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'CREATE TABLE #databaseLogs (Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY, databasename nvarchar(max), logFile nvarchar(max))

INSERT INTO #databaseLogs (databasename, logFile)

(SELECT dt.name, mf.name
FROM sys.databases dt
INNER JOIN sys.master_files mf ON mf.database_id = dt.database_id
WHERE dt.name LIKE ''PracticeRepository%''
AND type_desc = ''LOG''
AND dt.name NOT LIKE ''MVE%'')

DECLARE @Id int
DECLARE @name varchar(max)
DECLARE @logfile varchar(max)

WHILE (SELECT COUNT(*) FROM #databaseLogs) > 0
BEGIN 
	SET @Id = (SELECT TOP 1 Id FROM #databaseLogs ORDER BY Id ASC)
	SET @name = (SELECT TOP 1 databasename FROM #databaseLogs WHERE Id = @Id)
	SET @logfile = (SELECT TOP 1 logFile FROM #databaseLogs WHERE Id = @Id)
	
	BEGIN
		EXEC(''ALTER DATABASE '' + @name + ''  SET RECOVERY SIMPLE'')
	END
	BEGIN
		EXEC(''USE '' + @name +  '' DBCC SHRINKFILE (['' + @logfile +''] , 1)'')
	END
	IF @name NOT LIKE ''%Archive%''
	BEGIN
		EXEC(''ALTER DATABASE '' + @name + '' SET RECOVERY FULL'')
	END
	
	DELETE #databaseLogs WHERE Id = @Id
END

DROP table #databaseLogs', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
/****** Object:  Step [RebuildIndexes]    Script Date: 8/14/2014 12:47:26 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'RebuildIndexes', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'SET QUOTED_IDENTIFIER ON;
DECLARE @Database VARCHAR(255)   
DECLARE @Table VARCHAR(255)  
DECLARE @cmd NVARCHAR(500)  
DECLARE @fillfactor INT 

SET @fillfactor = 90 

DECLARE DatabaseCursor CURSOR FOR  
SELECT name FROM master.dbo.sysdatabases   
WHERE name LIKE ''PracticeRepository%''   
ORDER BY 1  

OPEN DatabaseCursor  

FETCH NEXT FROM DatabaseCursor INTO @Database  
WHILE @@FETCH_STATUS = 0  
BEGIN  

   SET @cmd = ''DECLARE TableCursor CURSOR FOR SELECT ''''['''' + table_catalog + ''''].['''' + table_schema + ''''].['''' + 
  table_name + '''']'''' as tableName FROM ['' + @Database + ''].INFORMATION_SCHEMA.TABLES 
  WHERE table_type = ''''BASE TABLE''''''   

   -- create table cursor  
   EXEC (@cmd)  
   OPEN TableCursor   

   FETCH NEXT FROM TableCursor INTO @Table   
   WHILE @@FETCH_STATUS = 0   
   BEGIN   

       IF (@@MICROSOFTVERSION / POWER(2, 24) >= 9)
       BEGIN
           -- SQL 2005 or higher command 
           SET @cmd = ''ALTER INDEX ALL ON '' + @Table + '' REBUILD WITH (FILLFACTOR = '' + CONVERT(VARCHAR(3),@fillfactor) + '')'' 
           EXEC (@cmd) 
       END
       ELSE
       BEGIN
          -- SQL 2000 command 
          DBCC DBREINDEX(@Table,'' '',@fillfactor)  
       END

       FETCH NEXT FROM TableCursor INTO @Table   
   END   

   CLOSE TableCursor   
   DEALLOCATE TableCursor  

   FETCH NEXT FROM DatabaseCursor INTO @Database  
END  
CLOSE DatabaseCursor   
DEALLOCATE DatabaseCursor ', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Sunday3AM', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20130311, 
		@active_end_date=99991231, 
		@active_start_time=30000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
COMMIT TRANSACTION
GO


