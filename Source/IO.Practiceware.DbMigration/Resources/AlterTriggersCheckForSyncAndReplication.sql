DECLARE commands CURSOR FOR

SELECT * FROM (

SELECT STUFF(def, CHARINDEX('BEGIN', def), LEN('BEGIN'), 'BEGIN
DECLARE @msrepl_object_id int
SET @msrepl_object_id = OBJECT_ID(''[' + d.SchemaName + '].[' + d.Parent + ']'')
DECLARE @retcode int
EXEC @retcode = sp_check_for_sync_trigger @msrepl_object_id
IF @retcode = 1 RETURN') AS AlterDefinition
FROM 
(
SELECT REPLACE(
		OBJECT_DEFINITION(t.object_id), 'CREATE TRIGGER', 'ALTER TRIGGER'
		) AS def, t.object_id, OBJECT_SCHEMA_NAME(t.object_id) AS SchemaName, t.name, OBJECT_NAME( t.parent_id) AS Parent

		
FROM sys.triggers t 
WHERE t.type = 'TR' AND OBJECT_DEFINITION(t.object_id) NOT LIKE '%sp_check_for_sync_trigger%' AND t.name NOT LIKE '%MSsync%' AND is_not_for_replication = 0 AND t.name <> 'PracticeActivityMultipleActivityRecordsTrigger'
) AS d
) d2
WHERE d2.AlterDefinition IS NOT NULL

DECLARE @cmd varchar(max)

OPEN commands
FETCH NEXT FROM commands INTO @cmd
WHILE @@FETCH_STATUS=0
BEGIN
	DECLARE @NotForReplicationStringIndex int
	SELECT @NotForReplicationStringIndex = CHARINDEX('NOT FOR REPLICATION', @cmd, 1)

	IF(@NotForReplicationStringIndex = 0)
	BEGIN


	-- crlfAScrlf
		IF (CHARINDEX('
AS
', @cmd) > 0)
			SELECT @cmd = STUFF(@cmd, CHARINDEX('
AS
', @cmd), LEN('
AS
'), '
NOT FOR REPLICATION 
AS
')

--crlfAS crlf
		ELSE IF (CHARINDEX('
AS 
', @cmd) > 0)
			SELECT @cmd = STUFF(@cmd, CHARINDEX('
AS 
', @cmd), LEN('
AS 
'), '
NOT FOR REPLICATION
AS
')


--AS BEGINcrlf
		ELSE IF (CHARINDEX('AS BEGIN
', @cmd) > 0)
			SELECT @cmd = STUFF(@cmd, CHARINDEX('AS BEGIN
', @cmd), LEN('AS BEGIN
'), '
NOT FOR REPLICATION 
AS
BEGIN
')


--AS crlf
		ELSE IF (CHARINDEX('AS 
', @cmd) > 0)
			SELECT @cmd = STUFF(@cmd, CHARINDEX('AS 
', @cmd), LEN('AS 
'), '
NOT FOR REPLICATION 
AS
')

	--AScrlf
		ELSE IF (CHARINDEX('AS
', @cmd) > 0)
			SELECT @cmd = STUFF(@cmd, CHARINDEX('AS
', @cmd), LEN('AS
'), '
NOT FOR REPLICATION 
AS
')


	END

	EXEC(@cmd)
	FETCH NEXT FROM commands INTO @cmd
END

CLOSE commands
DEALLOCATE commands

GO