IF  EXISTS (SELECT srv.name FROM master.sys.servers srv WHERE srv.server_id != 0 AND srv.name = N'IOPUBLICDATA')
BEGIN
	EXEC master.dbo.sp_dropserver @server=N'IOPUBLICDATA', @droplogins='droplogins'
END
GO

EXEC master.dbo.sp_addlinkedserver @server = N'IOPUBLICDATA', @srvproduct=N'sql_server', @provider=N'SQLNCLI', @datasrc=N'{DataSource}', @catalog=N'{Catalog}'
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'IOPUBLICDATA',@useself=N'False',@locallogin=NULL,@rmtuser=N'{UserID}',@rmtpassword='{Password}'

GO

EXEC master.dbo.sp_serveroption @server=N'IOPUBLICDATA', @optname=N'collation compatible', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'IOPUBLICDATA', @optname=N'data access', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'IOPUBLICDATA', @optname=N'dist', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'IOPUBLICDATA', @optname=N'pub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'IOPUBLICDATA', @optname=N'rpc', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'IOPUBLICDATA', @optname=N'rpc out', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'IOPUBLICDATA', @optname=N'sub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'IOPUBLICDATA', @optname=N'connect timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'IOPUBLICDATA', @optname=N'collation name', @optvalue=null
GO

EXEC master.dbo.sp_serveroption @server=N'IOPUBLICDATA', @optname=N'lazy schema validation', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'IOPUBLICDATA', @optname=N'query timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'IOPUBLICDATA', @optname=N'use remote collation', @optvalue=N'true'
GO