﻿DECLARE @dbName nvarchar(100)
SET @dbName = DB_NAME()

EXEC('ALTER DATABASE [' + @dbName + '] SET RECOVERY FULL')