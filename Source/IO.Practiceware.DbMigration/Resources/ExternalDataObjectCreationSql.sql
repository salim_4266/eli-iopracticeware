SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT 
      table_name =  o.name
    , [columns] = STUFF((
        SELECT ', ' + '[' + c.name +']'
        FROM sys.columns c WITH (NOWAIT)
        WHERE c.[object_id] = o.[object_id]
        FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '')
INTO #listOfTables
FROM (
    SELECT 
          o.[object_id]
        , o.name
        , o.[schema_id]
    FROM sys.objects o WITH (NOWAIT)
    WHERE o.[type] = 'U'
        AND o.is_ms_shipped = 0
) o
JOIN sys.schemas s WITH (NOWAIT) ON o.[schema_id] = s.[schema_id] 
ORDER BY 
      s.name
    , o.name

DECLARE @createSchemas NVARCHAR(max)

SET @createSchemas = ''

SELECT @createSchemas = @createSchemas + 'IF SCHEMA_ID(''' + s + ''') IS NULL EXEC(''CREATE SCHEMA ' + s + '''); 
'
FROM (
	SELECT DISTINCT SCHEMA_NAME(schema_id) AS s
	FROM sys.tables
	WHERE type = 'U'
	) q

DECLARE @createViews NVARCHAR(max)

SET @createViews = ''

SELECT @createViews = @createViews + 'IF OBJECT_ID(''[' + SCHEMA_NAME(schema_id) + '].[' + t.NAME + ']''' + ', ''V'') IS NOT NULL ' +' 
BEGIN 
DROP VIEW [' + SCHEMA_NAME(schema_id) + '].[' + t.NAME + ']' + '
END;
IF OBJECT_ID(''[' + SCHEMA_NAME(schema_id) + '].[' + t.NAME + ']''' + ', ''U'') IS NOT NULL ' +' 
BEGIN 
DROP TABLE [' + SCHEMA_NAME(schema_id) + '].[' + t.NAME + ']' + '
END;
IF OBJECT_ID(''[' + SCHEMA_NAME(schema_id) + '].[' + t.NAME + ']''' + ', ''V'') IS NULL ' +'  
BEGIN
EXEC(''CREATE VIEW [' + SCHEMA_NAME(schema_id) + '].[' + t.NAME + '] AS SELECT  ' + columns + ' FROM [IOPUBLICDATA].[ExternalData].[' + SCHEMA_NAME(schema_id) + '].[' + t.NAME + ']'')
END;
GO
'
FROM sys.tables t
INNER JOIN #listOfTables lt ON lt.Table_Name = t.name
WHERE type = 'U'

SELECT @createSchemas + '
' + @createViews 

