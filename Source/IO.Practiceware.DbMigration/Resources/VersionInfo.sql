IF NOT EXISTS(SELECT * FROM sys.columns WHERE name = 'ProductVersion' AND object_id = OBJECT_ID('dbo.VersionInfo'))
	EXEC('ALTER TABLE dbo.VersionInfo ADD ProductVersion varchar(32) NULL')

IF NOT EXISTS(SELECT * FROM sys.columns WHERE name = 'Checksum' AND object_id = OBJECT_ID('dbo.VersionInfo'))
	EXEC('ALTER TABLE dbo.VersionInfo ADD [Checksum] varbinary(max) NULL')

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.VersionInfo') AND name = N'UC_Version')
	EXEC('DROP INDEX [UC_Version] ON dbo.VersionInfo')

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.VersionInfo') AND name = N'PK_VersionInfo')
	EXEC('ALTER TABLE dbo.VersionInfo ADD CONSTRAINT [PK_VersionInfo] PRIMARY KEY CLUSTERED (Version)')