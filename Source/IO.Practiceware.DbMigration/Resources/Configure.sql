﻿DECLARE @dbName nvarchar(100)
SET @dbName = DB_NAME()

DECLARE @owner nvarchar(255)
SET @owner = 'sa'

BEGIN TRY
EXEC('ALTER AUTHORIZATION ON DATABASE::[' + @dbName + '] TO [' + @owner + ']')
END TRY
BEGIN CATCH
RAISERROR('Error occured trying to ALTER AUTHORIZATION ON DATABASE',10,1)
END CATCH
GO

EXEC SP_CONFIGURE 'clr enabled', 1
GO
RECONFIGURE WITH OVERRIDE
GO

DECLARE @dbName nvarchar(100)
SET @dbName = DB_NAME()

EXEC('ALTER DATABASE [' + @dbName + '] SET TRUSTWORTHY ON')

EXEC('ALTER DATABASE [' + @dbName + '] SET ARITHABORT ON')

EXEC('ALTER DATABASE [' + @dbName + '] SET RECOVERY SIMPLE')

DECLARE @logFileName nvarchar(max)
SELECT @logFileName = name FROM sys.database_files f WHERE f.type_desc = 'LOG'

EXEC('DBCC SHRINKFILE(''' + @logFileName + ''', 1)')

GO

sp_configure 'show advanced options', 1;
GO

RECONFIGURE WITH OVERRIDE;
GO

sp_configure 'max degree of parallelism', 8;
GO

RECONFIGURE WITH OVERRIDE;
GO

sp_configure 'max text repl size', 2147483647;
GO

RECONFIGURE WITH OVERRIDE;
GO

EXEC master..sp_configure 'xp_cmdshell', 1
RECONFIGURE WITH OVERRIDE;
GO

BEGIN TRY
DECLARE @dbName nvarchar(100)
SET @dbName = DB_NAME()
	EXEC sp_dbcmptlevel @dbname=@dbName, @new_cmptlevel=90
END TRY
BEGIN CATCH
END CATCH
GO

DECLARE @dbName nvarchar(max)
	,@sql nvarchar(max)

BEGIN TRY
	SET @dbName = DB_NAME()

	IF NOT EXISTS (
			SELECT *
			FROM master.sys.objects
			WHERE name = 'SetTrustWorthyOn'
			)
	BEGIN
		SET @sql = N'CREATE PROCEDURE SetTrustWorthyOn
            AS
            BEGIN
                ALTER DATABASE ' + QUOTENAME(@dbName) + ' SET TRUSTWORTHY ON
            END;'

		EXECUTE master..sp_executesql @sql

		EXECUTE master.sys.sp_procoption 'SetTrustWorthyOn'
			,'startup'
			,'ON'
	END
END TRY

BEGIN CATCH
END CATCH
GO

