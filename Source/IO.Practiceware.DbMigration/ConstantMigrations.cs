﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using IO.Practiceware.DbMigration.Properties;
using Soaf;
using Soaf.Collections;
using Soaf.IO;

namespace IO.Practiceware.DbMigration
{
    /// <summary>
    /// Well known migration values for scripts that must be run every time (if the content of that script changes).
    /// </summary>
    internal static class ConstantMigrations
    {
        public const long BaseVersion = long.MaxValue - 10000;

        public static readonly SqlMigration VersionInfo = new SqlMigration("VersionInfo", Resources.VersionInfo, -1);
        public static readonly SqlMigration ModelViews = new SqlMigration("Model Views", Resources.ModelViews, BaseVersion + 1);
        public static readonly SqlMigration Audit = new SqlMigration("Audit", Resources.Audit, BaseVersion + 2);
        public static readonly SqlMigration Reports = new SqlMigration("Reports", GetEmbeddedResources("Reports").Select(i => i.Item2).Join("\r\nGO\r\n"), BaseVersion + 3);
        public static readonly SqlMigration LegacyReports = new SqlMigration("Legacy Reports", GetEmbeddedResources("Legacy Reports").Select(i => i.Item2).Join("\r\nGO\r\n"), BaseVersion + 4);
        public static readonly SqlMigration MeaningfulUse = new SqlMigration("Meaningful Use", GetEmbeddedResources("MeaningfulUse").Select(i => i.Item2).Join("\r\nGO\r\n"), BaseVersion + 5);

        private static IEnumerable<Tuple<string, string>> GetEmbeddedResources(string ns)
        {
            var assembly = Assembly.GetExecutingAssembly();

            string @namespace = "{0}.{1}.".FormatWith(assembly.GetName().Name, ns);
            var results = new List<Tuple<string, string>>();

            foreach (var name in assembly.GetManifestResourceNames()
                                         .Where(r => r.StartsWith(@namespace)))
            {
                using (var stream = assembly.GetManifestResourceStream(name))
                {
                    results.Add(Tuple.Create(name, stream.GetString()));
                }
            }
            return results.ToArray();
        }

    }
}