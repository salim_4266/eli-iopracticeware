﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Transactions;
using Microsoft.SqlServer.Server;
using Soaf;
using Soaf.Collections;
using Soaf.Data;
using Soaf.Reflection;

namespace IO.Practiceware.DbMigration
{
    /// <summary>
    /// </summary>
    internal class SqlClrInstaller
    {
        /// <summary>
        ///     The type mappings from SqlTypes to native SQL server types.
        /// </summary>
        private static readonly IDictionary<Type, string> SqlTypeMappings = new Dictionary<Type, string>
                                                                            {
                                                                                {typeof (SqlInt64), "bigint"},
                                                                                {typeof (SqlBinary), "varbinary(max)"},
                                                                                {typeof (SqlBoolean), "bit"},
                                                                                {typeof (SqlDateTime), "datetime"},
                                                                                {typeof (DateTimeOffset), "DATETIMEOFFSET"},
                                                                                {typeof (SqlDecimal), "decimal"},
                                                                                {typeof (SqlDouble), "float"},
                                                                                {typeof (SqlInt32), "int"},
                                                                                {typeof (SqlMoney), "money"},
                                                                                {typeof (SqlString), "nvarchar(max)"},
                                                                                {typeof (SqlChars), "nvarchar(1)"},
                                                                                {typeof (SqlSingle), "real"},
                                                                                {typeof (SqlInt16), "smallint"},
                                                                                {typeof (TimeSpan), "time"},
                                                                                {typeof (SqlByte), "tinyint"},
                                                                                {typeof (SqlGuid), "uniqueidentifier"},
                                                                                {typeof (SqlXml), "xml"}
                                                                            };

        /// <summary>
        ///     Installs an assembly and all SqlFunctions and SqlProcedures defined within it.
        /// </summary>
        /// <param name="asm">The asm.</param>
        /// <param name="connection">The connection.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public static void Install(Assembly asm, SqlConnection connection)
        {
            CreateDropAssemblyProcedure(connection);

            using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { Timeout = TimeSpan.FromMinutes(60) }))
            {
                // dropping dependencies could destroy the DB....use a transaction

                Dictionary<MethodInfo, string> reAddDependenciesScript = new Dictionary<MethodInfo, string>();

                foreach (MethodInfo method in asm.GetTypes().SelectMany(t => t.GetMethods(BindingFlags.Public | BindingFlags.Static)))
                {
                    reAddDependenciesScript[method] = DropDependencies(connection, method);
                }

                DeployAssembly(asm, connection);

                foreach (MethodInfo method in asm.GetTypes().SelectMany(t => t.GetMethods(BindingFlags.Public | BindingFlags.Static)))
                {
                    var sqlFunction = method.GetAttribute<SqlFunctionAttribute>();
                    if (sqlFunction != null)
                    {
                        string createOrAlterFunctionSql = GetCreateOrAlterFunctionSql(method, sqlFunction);
                        try
                        {
                            if (createOrAlterFunctionSql != null) connection.Execute(createOrAlterFunctionSql);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(string.Format("Could not create function from sql\r\n{0}", createOrAlterFunctionSql), ex);
                        }
                        continue;
                    }

                    var sqlProcedure = method.GetAttribute<SqlProcedureAttribute>();
                    if (sqlProcedure != null)
                    {
                        string createOrAlterProcedureSql = GetCreateOrAlterProcedureSql(method, sqlProcedure);
                        try
                        {
                            if (createOrAlterProcedureSql != null) connection.Execute(createOrAlterProcedureSql);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(string.Format("Could not create procedure from sql\r\n{0}", createOrAlterProcedureSql), ex);
                        }
                    }

                    var reAddSql = reAddDependenciesScript[method];
                    if (reAddSql != null)
                    {
                        connection.Execute(reAddSql);
                    }
                }
                ts.Complete();
            }
        }

        /// <summary>
        /// Drops the dependencies of a SQL CLR function.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="method">The method.</param>
        /// <returns></returns>
        private static string DropDependencies(SqlConnection connection, MethodInfo method)
        {
            var sqlName = "[{0}].[{1}]".FormatWith(method.DeclaringType.EnsureNotDefault().Assembly.GetName().Name, method.Name);

            var objectId = connection.Execute<int>("SELECT OBJECT_ID('{0}')".FormatWith(sqlName));

            if (objectId == 0) return null;

            using (var command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "[GetDropAndAddSchemaboundDependenciesSql]";

                var objectIdFilter = command.CreateParameter();
                objectIdFilter.ParameterName = "objectIdFilter";
                objectIdFilter.Value = objectId;
                command.Parameters.Add(objectIdFilter);

                var dropSql = command.CreateParameter();
                dropSql.ParameterName = "dropSql";
                dropSql.DbType = DbType.String;
                dropSql.Size = int.MaxValue;
                dropSql.Direction = ParameterDirection.Output;
                command.Parameters.Add(dropSql);

                var addSql = command.CreateParameter();
                addSql.ParameterName = "addSql";
                addSql.DbType = DbType.String;
                addSql.Size = int.MaxValue;
                addSql.Direction = ParameterDirection.Output;
                command.Parameters.Add(addSql);

                command.ExecuteScalar();

                if (!string.IsNullOrEmpty(dropSql.Value as string))
                {
                    connection.Execute(dropSql.Value as string);
                }

                if (!string.IsNullOrEmpty(addSql.Value as string))
                {
                    return addSql.Value as string;
                }
            }

            return null;
        }

        /// <summary>
        /// Creates the drop assembly procedure.
        /// </summary>
        /// <param name="connection">The connection.</param>
        private static void CreateDropAssemblyProcedure(SqlConnection connection)
        {

            const string createDropAssemblySql = @"
IF OBJECT_ID('DropAssembly') IS NOT NULL
    DROP PROCEDURE DropAssembly

GO

CREATE PROCEDURE dbo.DropAssembly(@name varchar(max))
AS
BEGIN
IF EXISTS(SELECT * FROM sys.assemblies WHERE name = @name) 
BEGIN
    DECLARE @sql varchar(max)

    -- Drop dependent functions/procedures
    SET @sql = ''
    SELECT @sql = @sql + 'DROP ' + 
	    (CASE WHEN o.type = 'PC' THEN 'PROCEDURE' ELSE 'FUNCTION' END)
    + ' [' + SCHEMA_NAME(o.schema_id) + '].[' + o.name + '];'
    FROM sys.assemblies a
    JOIN sys.assembly_modules am ON a.assembly_id = am.assembly_id
    JOIN sys.objects o ON am.object_id = o.object_id
    WHERE a.name = @name
    AND o.type IN ('PC', 'FT', 'FS')

    EXEC(@sql)

    -- Drop dependent assemblies
    SET @sql = ''
    SELECT @sql = @sql + 'EXEC dbo.DropAssembly ''' + ara.name + ''''
    FROM sys.assemblies a
    JOIN sys.assembly_references ar ON a.assembly_id = ar.referenced_assembly_id
    JOIN sys.assemblies ara ON ar.assembly_id = ara.assembly_id
    WHERE a.name = @name

    EXEC(@sql)

    SET @sql = 'DROP ASSEMBLY [' + @name + ']'
    
    EXEC(@sql)
END
END
";

            connection.RunScript(createDropAssemblySql);
        }

        /// <summary>
        /// Deploys the assembly and all dependent assemblies to the specified connections DB data directory.
        /// </summary>
        /// <param name="asm">The asm.</param>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        private static void DeployAssembly(Assembly asm, SqlConnection connection)
        {
            var dbDirectory = Path.Combine(GetDbDataDirectory(connection, true), connection.Database.Trim());
            // deploy all assemblies to DB path
            var deployedAssemblies = DeployAssembly(asm, dbDirectory).ToArray();

            // Check if the user hasd permissions to write to the DbDataDirectories
            HasRequiredPermissionOnDirectory(dbDirectory);
            // convert back to non-UNC path
            deployedAssemblies = deployedAssemblies
                .Select(Path.GetFileName).WhereNotDefault()
                .Select(a => Path.Combine(GetDbDataDirectory(connection, false), connection.Database.Trim(), a))
                .ToArray();

            var dropAssembliesSql = deployedAssemblies.Reverse().Select(path => "EXEC dbo.DropAssembly '{0}'".FormatWith(Path.GetFileNameWithoutExtension(path)))
                .Join(Environment.NewLine);

            const string createAssemblySqlFormat = @"
CREATE ASSEMBLY [{0}] FROM '{1}' WITH PERMISSION_SET = UNSAFE";

            var createAssembliesSql = deployedAssemblies.Select(path => createAssemblySqlFormat.FormatWith(Path.GetFileNameWithoutExtension(path), path)).Join(Environment.NewLine);

            var sql = new[] { dropAssembliesSql, createAssembliesSql }.Join(Environment.NewLine);

            TryCreateAssemblies(connection, sql);

            string createAssemblySchemaSql = @"
IF SCHEMA_ID('{0}') IS NULL EXEC('CREATE SCHEMA [{0}]')
".FormatWith(asm.GetName().Name);

            connection.Execute(createAssemblySchemaSql);
        }

        /// <summary>
        /// Tries to create the assembly.
        /// Suppresses errors due to duplicate assemblies. See http://connect.microsoft.com/SQLServer/feedback/details/444044/alter-assembly-should-not-fail-when-the-mvid-is-the-same.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="createAlterAssemblySql">The create alter assembly SQL.</param>
        [DebuggerNonUserCode]
        private static void TryCreateAssemblies(SqlConnection connection, string createAlterAssemblySql)
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = 180;
                    command.CommandText = (createAlterAssemblySql);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                if (!ex.Message.StartsWith("ALTER ASSEMBLY failed because the source assembly is, according to MVID, identical to an assembly that is already registered under the name "))
                    throw;
            }
        }

        /// <summary>
        /// Deploys the assembly and its dependencies to the specified path. Returns a collection of deployed assemblies (including reference assembly and XmlSerializers assemblies).
        /// </summary>
        /// <param name="asm">The asm.</param>
        /// <param name="path">The path.</param>
        /// <param name="excludedPaths">The excluded paths (assemblies not to deploy).</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        private static IEnumerable<string> DeployAssembly(Assembly asm, string path, HashSet<string> excludedPaths = null)
        {
            if (excludedPaths == null) excludedPaths = new HashSet<string>();

            var result = new Stack<string>();

            if (!Directory.Exists(path)) Directory.CreateDirectory(path);

            var targetPath = Path.Combine(path, Path.GetFileName(asm.Location));

            if (File.Exists(asm.Location) && !asm.GlobalAssemblyCache && excludedPaths.Add(asm.Location))
            {

                var xmlSerializersDllPath = GetXmlSerializersDllPath(asm.Location);
                if (File.Exists(xmlSerializersDllPath))
                {
                    var xmlSerializersTargetPath = GetXmlSerializersDllPath(targetPath);
                    File.Copy(xmlSerializersDllPath, xmlSerializersTargetPath, true);
                    result.Push(xmlSerializersTargetPath);
                }

                File.Copy(asm.Location, targetPath, true);
                result.Push(targetPath);

                foreach (var referencedAssembly in asm.GetReferencedAssemblies())
                {
                    foreach (var deployedPath in DeployAssembly(Assembly.Load(referencedAssembly), path, excludedPaths))
                    {
                        result.Push(deployedPath);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the expected XML serializers DLL path for the specified DLL path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private static string GetXmlSerializersDllPath(string path)
        {
            return Path.Combine(Path.GetDirectoryName(path) ?? "", Path.GetFileNameWithoutExtension(path) + ".XmlSerializers.dll");
        }

        /// <summary>
        /// Gets the directory path of the connection's database's first file, as a unc path if requested.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="asUnc">if set to <c>true</c> [as unc].</param>
        /// <returns></returns>
        private static string GetDbDataDirectory(SqlConnection connection, bool asUnc)
        {
            var directory = Path.GetDirectoryName(connection.Execute<string>("SELECT TOP 1 physical_name FROM sys.master_files WHERE database_id = DB_ID()"));

            return asUnc ? @"\\{0}\{1}".FormatWith(connection.DataSource.EnsureNotDefault().Split('\\')[0], directory).ReplaceFirst(":", "$") : directory;
        }

        /// <summary>
        ///     Returns a string of SQL that will create or alter a SQL function for the specified method.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="sqlFunction">The SQL function.</param>
        /// <returns></returns>
        private static string GetCreateOrAlterFunctionSql(MethodInfo method, SqlFunctionAttribute sqlFunction)
        {
            string assemblyName = method.DeclaringType.EnsureNotDefault().Assembly.GetName().Name;

            const string functionSqlTemplate = "FUNCTION [{0}].[{1}]({2}) RETURNS {3} AS EXTERNAL NAME [{0}].[{4}].[{5}]";

            string createOrAlterFunctionSqlTemplate = "IF OBJECT_ID('[{0}].[{1}]') IS NULL EXEC('CREATE " + functionSqlTemplate + "')" + Environment.NewLine + "ELSE EXEC('ALTER " + functionSqlTemplate + "')";

            string parametersSql = GetParametersSql(method);
            if (parametersSql == null) return null;

            string returnTypeSql = GetReturnTypeSql(method, sqlFunction);
            if (returnTypeSql == null) return null;

            string createOrAlterFunctionSql = createOrAlterFunctionSqlTemplate.FormatWith(
                assemblyName,
                sqlFunction.IfNotNull(p => p.Name) ?? method.Name,
                parametersSql,
                returnTypeSql,
                method.DeclaringType.EnsureNotDefault().FullName,
                method.Name);

            return createOrAlterFunctionSql;
        }

        /// <summary>
        ///     Returns a string of SQL that will create or alter a SQL procedure for the specified method.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="sqlProcedure">The SQL procedure.</param>
        /// <returns></returns>
        private static string GetCreateOrAlterProcedureSql(MethodInfo method, SqlProcedureAttribute sqlProcedure)
        {
            string assemblyName = method.DeclaringType.EnsureNotDefault().Assembly.GetName().Name;

            const string procedureTemplateSql = "PROCEDURE [{0}].[{1}]({2}) AS EXTERNAL NAME [{0}].[{3}].[{4}]";

            string createOrAlterProcedureSqlTemplate = "IF OBJECT_ID('[{0}].[{1}]') IS NULL EXEC('CREATE " + procedureTemplateSql + "')" + Environment.NewLine + "ELSE EXEC('ALTER " + procedureTemplateSql + "')";

            string parametersSql = GetParametersSql(method);
            if (parametersSql == null) return null;

            string createOrAlterProcedureSql = createOrAlterProcedureSqlTemplate.FormatWith(
                assemblyName,
                sqlProcedure.IfNotNull(p => p.Name) ?? method.Name,
                parametersSql,
                method.DeclaringType.EnsureNotDefault().FullName,
                method.Name);

            return createOrAlterProcedureSql;
        }

        /// <summary>
        ///     Gets SQL representing definitions for the method parameters.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <returns></returns>
        private static string GetParametersSql(MethodInfo method)
        {
            var parameters = method.GetParameters().Select(p => new { p.Name, ClrType = p.ParameterType, SqlType = SqlTypeMappings.GetValue(p.ParameterType.GetElementType() ?? p.ParameterType), p.IsOut, p.IsOptional, DefaultValue = p.IsOptional ? p.DefaultValue : null }).ToArray();

            // invalid/missing SQL type mapping
            if (parameters.Any(p => p.SqlType == null))
            {
                throw new InvalidOperationException("Method {0} is annoatated for SQL CLR use, but has invalid parameter types for {1}.".FormatWith(FormatMethodName(method), parameters.Where(p => p.SqlType == null).Select(p => p.Name).Join()));
            }

            Func<Type, object, string> formatDefaultValue = (t, v) =>
                                                            {
                                                                if (v == null || v == Missing.Value) return "NULL";
                                                                if (new[] { typeof(string), typeof(DateTime) }.Contains(t.GetElementType() ?? t)) return string.Format("'{0}'", v);
                                                                return v.ToString();
                                                            };

            string parametersSql = parameters.Select(p => "@{0} {1}{2}{3}".FormatWith(p.Name, p.SqlType, p.IsOptional ? " = " + formatDefaultValue(p.ClrType, p.DefaultValue) : string.Empty, p.IsOut ? " OUTPUT" : string.Empty)).Join();

            return parametersSql;
        }

        /// <summary>
        ///     Gets sql representing the return type of a SQL CLR function. Supports primitive or table valued return types.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="sqlFunction">The SQL function.</param>
        /// <returns></returns>
        private static string GetReturnTypeSql(MethodInfo method, SqlFunctionAttribute sqlFunction)
        {
            if (method.ReturnType == typeof(IEnumerable) && sqlFunction != null && sqlFunction.FillRowMethodName != null && method.DeclaringType != null)
            {
                MethodInfo fillMethod = method.DeclaringType.GetMethod(sqlFunction.FillRowMethodName);

                if (fillMethod == null)
                {
                    throw new InvalidOperationException("Fill method {0} for {1} could not be found.".FormatWith(sqlFunction.FillRowMethodName, FormatMethodName(method)));
                }

                var fillOutParameters = fillMethod.GetParameters().Skip(1).Select(p => new { ParameterInfo = p, SqlType = SqlTypeMappings.GetValue(p.ParameterType.GetElementType()) }).ToArray();

                if (!fillOutParameters.All(p => p.ParameterInfo.IsOut))
                {
                    throw new InvalidOperationException("Fill method {0} for {1} should have all 'out' parameters except for the first parameter.".FormatWith(sqlFunction.FillRowMethodName, FormatMethodName(method)));
                }

                if (fillOutParameters.Any(p => p.SqlType == null))
                {
                    throw new InvalidOperationException("Fill method {0} for {1} has invalid parameter types for {1}.".FormatWith(fillMethod.Name, FormatMethodName(method)));
                }

                if (!fillOutParameters.Any()) throw new InvalidOperationException("Table values function fill method {0} does not return any columns.".FormatWith(sqlFunction.FillRowMethodName));

                return "TABLE({0})".FormatWith(fillOutParameters.Select(p => "[" + p.ParameterInfo.Name + "] " + p.SqlType).Join());
            }

            string sql = SqlTypeMappings.GetValue(method.ReturnType);
            if (sql == null)
            {
                throw new InvalidOperationException("Method {0} is annoatated for SQL CLR use, but has invalid return type {1}.".FormatWith(FormatMethodName(method), method.ReturnType.FullName));
            }
            return sql;
        }

        private static string FormatMethodName(MethodInfo method)
        {
            return new[] { method.DeclaringType.IfNotNull(t => t.FullName), method.Name }.WhereNotDefault().Join(".");
        }

        /// <summary>
        /// Used to check if the user has write or delete privelidges in the folder specified
        /// </summary>
        /// <param name="currentValue"></param>
        /// <returns></returns>
        private static void HasRequiredPermissionOnDirectory(string currentValue)
        {
            try
            {
                if (Directory.Exists(currentValue))
                {
                    Directory.GetAccessControl(currentValue);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}