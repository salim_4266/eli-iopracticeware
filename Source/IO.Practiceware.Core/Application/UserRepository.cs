using IO.Practiceware.Configuration;
using IO.Practiceware.Model;
using Soaf.ComponentModel;
using Soaf.Domain;
using Soaf.Security;
using System.Linq;
using Role = Soaf.Security.Role;
using User = IO.Practiceware.Model.User;
using UserRepository = IO.Practiceware.Application.UserRepository;

[assembly: Component(typeof(UserRepository), typeof(IUserRepository), Priority = 1)]
[assembly: Component(typeof(IO.Practiceware.Application.MembershipProvider), typeof(System.Web.Security.MembershipProvider), Priority = 1)]

namespace IO.Practiceware.Application
{
    public class UserRepository : IUserRepository
    {
        private readonly IRepository<User> _userRepository;
        readonly IRepository<BillingOrganization> _organizationRepository;
        private readonly IRepository<ApplicationSetting> _applicationSettingRepository;

        public UserRepository(IRepository<User> userRepository,
            IRepository<BillingOrganization> organizationRepository, IRepository<ApplicationSetting> applicationSettingRepository)
        {
            _userRepository = userRepository;
            _organizationRepository = organizationRepository;
            _applicationSettingRepository = applicationSettingRepository;
        }

        #region IUserRepository Members

        public IQueryable<Soaf.Security.User> Users
        {
            get
            {
                string token = ConfigurationManager.AuthenticationToken ?? string.Empty;

                IQueryable<Soaf.Security.User> users =
                    from u in _userRepository.Query()
                    select new Soaf.Security.User
                               {
                                   Id = u.Id.ConvertToString(),
                                   UserName = (u.Pid ?? string.Empty) + "@" + token,
                                   Password = u.Pid,
                                   IsApproved = true,
                                   IsOnline = u.IsLoggedIn,
                                   RolesSource = (from p in u.UserPermissions
                                                  where !p.IsDenied
                                                  select new Role { Name = p.Permission.Name, Tag = p }),

                                   // Uncomment when we start supporting RolePermissions 
                                   // .Concat((
                                   //from rp in u.Roles.SelectMany(r => r.RolePermissions)
                                   //where !rp.IsDenied
                                   //select new Role { Name = rp.Permission.Name }))
                                   //,
                                   Tag = new UserDetails
                                       {
                                           Id = u.Id,
                                           UserName = u.UserName,
                                           DisplayName = u.DisplayName,
                                           FirstName = u.FirstName,
                                           LastName = u.LastName,
                                           Suffix = u.Suffix,
                                           Email = u.EmailAddresses.FirstOrDefault().Value,
                                           IsProvider = u.Providers.Any(),
                                           BillingOrganizationName = _organizationRepository
                                                .Query()
                                                .Where(o => o.IsMain)
                                                .Select(o => o.Name)
                                                .FirstOrDefault(),
                                           ClientName = _applicationSettingRepository
                                                .Query()
                                                .Where(a => a.Name == "ClientName")
                                                .Select(a => a.Value)
                                                .FirstOrDefault(),
                                           ClientId = _applicationSettingRepository
                                               .Query()
                                               .Where(a => a.Name == "ClientId")
                                               .Select(a => a.Value)
                                               .FirstOrDefault(),
                                           IsArchived = u.IsArchived
                                       }
                               };

                return users;
            }
        }


        public void Save(Soaf.Security.User value)
        {
            User user = _userRepository.Query().FirstOrDefault(u => u.Id.ConvertToString() == value.Id) ?? new User();

            user.Pid = value.Password;
            user.IsLoggedIn = value.IsOnline;
            _userRepository.Save(user);
        }

        public IQueryable<Role> Roles
        {
            get { return null; }
        }

        public void Save(Role value)
        {
        }

        public void Delete(Role value)
        {
        }

        public void Delete(Soaf.Security.User value)
        {
            User user =
                _userRepository.Query().FirstOrDefault(u => u.Id.ConvertToString() == value.Id);

            if (user == null) return;
            user.UserName = value.UserName;

            _userRepository.Delete(user);
        }

        public Role CreateRole()
        {
            var role = new Role();
            return role;
        }

        public Soaf.Security.User CreateUser()
        {
            var user = new Soaf.Security.User();
            return user;
        }

        #endregion
    }

    public class MembershipProvider : UserRepositoryMembershipProvider
    {
        public MembershipProvider(IUserRepository repository)
            : base(repository)
        {
        }

        public override bool ValidateUser(string userName, string password)
        {

            var oldPrincipal = PrincipalContext.Current.Principal;
            if (userName.Contains("@") && !userName.EndsWith("@"))
            {
                PrincipalContext.Current.Principal = new GenericPrincipal(new GenericIdentity(userName));

                var serverConfiguration = ConfigurationManager.GetSectionWithRetry<ApplicationServerConfiguration>();
                if (serverConfiguration == null || serverConfiguration.Clients.All(c => c.AuthenticationToken != ConfigurationManager.AuthenticationToken))
                {
                    // invalid authentication token
                    return false;
                }
            }
            try
            {
                return base.ValidateUser(userName, password);
            }
            finally
            {
                PrincipalContext.Current.Principal = oldPrincipal;
            }
        }
    }
}