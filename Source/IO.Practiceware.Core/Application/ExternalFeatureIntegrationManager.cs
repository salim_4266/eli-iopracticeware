﻿using System;
using IO.Practiceware.Application;
using Soaf.ComponentModel;

[assembly: Component(typeof(ExternalFeatureIntegrationManager), typeof(IExternalFeatureIntegrationManager), AddAllServices = false)]

namespace IO.Practiceware.Application
{
    public interface IExternalFeatureIntegrationManager
    {
        /// <summary>
        /// Request feature execution by it's key
        /// </summary>
        /// <param name="featureKey">The feature key.</param>
        /// <param name="argument">The argument.</param>
        void ExecuteFeature(string featureKey, object argument = null);
    }

    [Singleton]
    public class ExternalFeatureIntegrationManager : IExternalFeatureIntegrationManager
    {
        /// <summary>
        ///   The event is used to callback into VB6 or other functionality
        /// </summary>
        public event EventHandler<FeatureExectionRequestEventArgs> ExternalFeatureExectionRequested;

        /// <summary>
        /// Request feature execution by it's key
        /// </summary>
        /// <param name="featureKey">The feature key.</param>
        /// <param name="argument">The argument.</param>
        public void ExecuteFeature(string featureKey, object argument = null)
        {
            // Forward to even subscribers
            OnExternalFeatureExectionRequested(new FeatureExectionRequestEventArgs
            {
                FeatureKey = featureKey,
                Argument = argument
            });
        }

        protected virtual void OnExternalFeatureExectionRequested(FeatureExectionRequestEventArgs e)
        {
            EventHandler<FeatureExectionRequestEventArgs> handler = ExternalFeatureExectionRequested;
            if (handler != null) handler(this, e);
        }
    }

    /// <summary>
    ///   Contain information about the feature requested for execution
    /// </summary>
    public class FeatureExectionRequestEventArgs : EventArgs
    {
        public string FeatureKey { get; set; }
        public object Argument { get; set; }
    }
}
