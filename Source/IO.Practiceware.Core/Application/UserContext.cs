using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Exceptions;
using IO.Practiceware.Model;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Logging;
using Soaf.Security;
using System;
using System.Linq;
using System.Reflection;
using LogCategory = IO.Practiceware.Logging.LogCategory;
using System.Net;

[assembly: Component(typeof(UserContext))]

namespace IO.Practiceware.Application
{
    // for XAML instantiation
    public class UserContextProxy
    {
        public UserContext Current
        {
            get { return UserContext.Current; }
        }
    }

    /// <summary>
    ///   Contextual information about the current user logged in.
    /// </summary>
    [Singleton]
    public class UserContext : INotifyPropertyChanged
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserRepository _userRepository;
        private readonly IPracticeRepository _practiceRepository;
        private static Lazy<UserContext> _current;
        private readonly Lazy<ILogger> _logger;
        private readonly SerializedTaskQueue _taskQueue = new SerializedTaskQueue();
        
        public UserContext(ILogManager logManager, IAuthenticationService authenticationService, IUserRepository userRepository, IPracticeRepository practiceRepository)
        {
            _authenticationService = authenticationService;
            _userRepository = userRepository;
            _practiceRepository = practiceRepository;
            _logger = Lazy.For(logManager.GetCurrentTypeLogger);
        }

        public static UserContext Current
        {
            get { return _current.IfNotNull(c => c.Value); }
        }

        /// <summary>
        /// Gets all service locations available.
        /// </summary>
        /// <value>
        /// The service location List.
        /// </value>
        public IEnumerable<Tuple<int, string>> ServiceLocations
        {
            get { return _serviceLocations; }
            private set
            {
                var isChanged = !Equals(_serviceLocations, value);
                _serviceLocations = value;
                if (isChanged)
                {
                    PropertyChanged.Fire(this, () => ServiceLocations);
                }
            }
        }

        /// <summary>
        /// Gets or sets the service location.
        /// </summary>
        /// <value>
        /// The service location.
        /// </value>
        public Tuple<int, string> ServiceLocation
        {
            get { return _serviceLocation; }
            set
            {
                var isChanged = !Equals(_serviceLocation, value);
                _serviceLocation = value;
                if (isChanged)
                {
                    PropertyChanged.Fire(this, () => ServiceLocation);
                    _taskQueue.Enqueue(() =>
                    {
                        var serviceLocationSetting = ServiceLocationSetting;

                        if (serviceLocationSetting == null)
                        {
                            serviceLocationSetting = new ApplicationSettingContainer<string>
                            {
                                MachineName = ApplicationContext.Current.ComputerName,
                                UserId = Current.UserDetails.Id,
                                Name = ApplicationSetting.ServiceLocation,
                            };
                        }
                        serviceLocationSetting.Value = value.Item2;

                        try
                        {
                            ApplicationSettings.Cached.SetSetting(serviceLocationSetting);
                        }
                        catch (Exception ex)
                        {
                            // maybe two users save at the same time - violates unique contraint
                            Trace.TraceWarning(ex.ToString());
                        }
                    });
                }
            }
        }

        private static ApplicationSettingContainer<string> ServiceLocationSetting
        {
            get { return ApplicationSettings.Cached.GetSetting<string>(ApplicationSetting.ServiceLocation, ApplicationContext.Current.ComputerName, Current.UserDetails.Id); }
        }

        private Tuple<int, string> _serviceLocation;
        private IEnumerable<Tuple<int, string>> _serviceLocations;

        public UserDetails UserDetails
        {
            get { return (PrincipalContext.Current.Principal as UserPrincipal).IfNotNull(u => u.Identity.User.Tag as UserDetails); }
        }

        internal static void Initialize(Func<UserContext> createUserContext)
        {
            _current = Lazy.For(createUserContext);
        }

        public int Login(string pid)
        {
            var userName = "{0}@{1}".FormatWith(pid, ConfigurationManager.AuthenticationToken);
            return Login(userName, pid);
        }

        private string APILogin()
        {
            string response = "False";
            string AuthToken = string.Empty;
            AuthToken = _practiceRepository.ApplicationSettings.Where(x => x.Name == "PracticeAuthToken").Select(u => u.Value).FirstOrDefault();
            if (AuthToken == null)
            {
                response = "Invalid Token";
                return response;
            }
            #region API Validation
            //login validation ...
            string hostname = Dns.GetHostName();
            string ipAddress = Dns.GetHostEntry(hostname).AddressList[1].ToString();
            WebClient client = new WebClient();
            client.Headers.Add("Content-Type:application/text");
            client.Headers.Add("Accept:application/text");
            var result = client.DownloadString("https://idc.iopracticeware.com/API/PracticeManagement/ValidatePractice?AuthToken=" + AuthToken + "&IPAddress=" + ipAddress + "&MachineName=" + hostname + "&UserName=" + ApplicationContext.Current.WindowsUserName);
            // Code Modification - Ankush Jain (02/11/2017)
            if (result.ToLower().Contains("true"))
            {
                response = "True";
            }
            //response = (string)Newtonsoft.Json.JsonConvert.DeserializeObject(result);
            #endregion
            return response;
        }
        public int Login(string userName, string password)
        {
            bool UserStatus = _authenticationService.Login(userName, password, null, true);
            string APIStatus = "";
            if (UserStatus)
                APIStatus = APILogin();
            else
                return -1;
            if (UserStatus && APIStatus == "True")
            {
                // Configure temporary credentials to allow service calls over the wire before UserPrincipal is set
                PrincipalContext.Current.Principal = new GenericPrincipal(new GenericIdentity(userName, true));

                if (ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled && !ConfigurationManager.DisableVerifyClientServerAssemblyVersions)
                {
                    ValidateAssemblyVersionsBetweenClientAndServer();
                }

                SetPrincipal(userName);

                if (UserDetails.IsArchived)
                {
                    _logger.Value.Log("User with Id {0} is archived. An archived user can not log in to the applicaton.".FormatWith(UserDetails.EnsureNotDefault("UserDetails not found").Id), categories: new[] { LogCategory.LogIn });
                    Logout();
                    throw new InactiveUserLoginException("Your user account has been deactivated. Please contact your practice administrator.");
                }

                _logger.Value.Log("User with Id {0} logged in.".FormatWith(UserDetails.EnsureNotDefault("UserDetails not found").Id), categories: new[] { LogCategory.LogIn });

                // set the timezone 
                PrincipalContext.Current.IfNotNull(p => p.Principal.As<UserPrincipal>().IfNotNull(up => up.Identity.User.TimeZoneId = DateTimes.ClientTimeZoneIdContext));

                ServiceLocations = _practiceRepository.ServiceLocations.ToArray().Select(x => Tuple.Create(x.Id, x.ShortName)).ToList();

                var serviceLocationSetting = ServiceLocationSetting;
                if (serviceLocationSetting != null)
                {
                    _serviceLocation = ServiceLocations.FirstOrDefault(sl => sl.Item2 == serviceLocationSetting.Value);
                    PropertyChanged.Fire(this, () => ServiceLocation);
                }

                if (ServiceLocation == null)
                {
                    // default to first item
                    ServiceLocation = ServiceLocations.FirstOrDefault();
                }

                // IO user information has been loaded along with User record
                return UserDetails.Id;
            }
            else
            {
                if (APIStatus == "False")
                {
                    return -2;
                }
                else if (APIStatus == "Invalid Token")
                {
                    return -3;
                }
                else if (APIStatus == "Error")
                {
                    return -4;
                }
                else
                {
                    return -1;
                }
            }
        }

        private void ValidateAssemblyVersionsBetweenClientAndServer()
        {
            var coreAssembly = Assembly.GetAssembly(typeof(UserContext));
            var coreAssemblyFullName = coreAssembly.FullName;
            var isValid = _authenticationService.ValidateAssemblyNameAndVersion(coreAssemblyFullName);

            if (!isValid)
            {
                throw new ClientServerAssemblyMismatchException("Please stand by and your software will update momentarily.");
            }
        }

        public void RefreshUser()
        {
            if (UserDetails != null)
            {
                SetPrincipal(UserDetails.Id);
            }
        }

        public void Logout()
        {
            if (UserDetails != null)
            {
                _logger.Value.Log("User with Id {0} logged out.".FormatWith(UserDetails.Id), categories: new[] { LogCategory.LogOut });
                _authenticationService.Logout();
                PrincipalContext.Current.IfNotNull(p => p.Principal = null);
            }
            ServiceLocations = null;
            _serviceLocation = null;
            PropertyChanged.Fire(this, () => ServiceLocation);
        }

        private void SetPrincipal(string userName)
        {
            PrincipalContext.Current.IfNotNull(p => p.Principal = new UserPrincipal(new UserIdentity(_userRepository.Users.Single(u => u.UserName == userName), true, string.Empty)));
            PropertyChanged.Fire(this, () => UserDetails);
        }

        private void SetPrincipal(int userId)
        {
            PrincipalContext.Current.IfNotNull(p => p.Principal = new UserPrincipal(new UserIdentity(_userRepository.Users.Single(u => u.Id == userId.ToString()), true, string.Empty)));
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}