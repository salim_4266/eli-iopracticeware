﻿namespace IO.Practiceware.Application
{
    public class UserDetails
    {
        /// <summary>
        /// User Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>
        /// The display name.
        /// </value>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the Last Name
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the Suffix
        /// </summary>
        /// <value>
        /// The Suffix.
        /// </value>
        public string Suffix { get; set; }

        /// <summary>
        /// Gets or sets the Email
        /// </summary>
        /// <value>
        /// The Email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets whether the user is a provider
        /// </summary>
        /// <value>
        /// Whether the user is a provider
        /// </value>
        public bool IsProvider { get; set; }

        /// <summary>
        /// Gets or sets the name of the business organization.
        /// </summary>
        /// <value>
        /// The name of the business organization.
        /// </value>
        public string BillingOrganizationName { get; set; }

        /// <summary>
        /// Gets or sets the name of the client.
        /// </summary>
        /// <value>
        /// The name of the client.
        /// </value>
        public string ClientName { get; set; }

        /// <summary>
        /// Gets or sets the client oid namespace branch name (for use creating OIDs).
        /// </summary>
        /// <value>
        /// The client oid namespace.
        /// </value>
        public string ClientId { get; set; }

        /// <summary>
        /// Gets or sets whether the user is archived
        /// </summary>
        /// <value>
        /// Whether the user is archived or not
        /// </value>
        public bool IsArchived { get; set; }
    }
}
