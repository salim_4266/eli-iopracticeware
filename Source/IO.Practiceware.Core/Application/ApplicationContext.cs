using System.Runtime.Serialization;
using Cassia;
using System.Web.Hosting;
using IO.Practiceware.Configuration;
using System;
using Soaf.ComponentModel;

namespace IO.Practiceware.Application
{
    /// <summary>
    ///   Contextual information about the current application instance.
    /// </summary>
    [DataContract]
    public class ApplicationContext
    {
        public static ApplicationContext Current
        {
            get { return ContextData.Get("__ApplicationContext__", () => new ApplicationContext()); }
        }

        [DataMember]
        public int? PatientId { get; set; }

        [DataMember]
        public int? AppointmentId { get; set; }

        public string ComputerName 
        {
            get
            {
                try
                {
                    // client's computer name
                    if (ConfigurationManager.ClientApplicationConfiguration.RdsWithSharedAccountMode)
                    {
                        try
                        {
                            var clientName = new TerminalServicesManager().CurrentSession.ClientName;
                            if (!string.IsNullOrWhiteSpace(clientName))
                            {
                                return string.Format("cn_{0}", clientName);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                    return Environment.MachineName;
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public string WindowsUserName
        {
            get
            {
                try
                {
                    return Environment.UserName;
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public string Version
        {
            get { return ConfigurationManager.AssemblyVersion.ToString(); }
        }

        /// <summary>
        /// Unique key identifying the client
        /// </summary>
        public string ClientKey
        {
            get { return ConfigurationManager.AuthenticationToken ?? "DirectNoKey"; }
        }

        /// <summary>
        /// Gets the name of the client.
        /// </summary>
        /// <value>
        /// The name of the client.
        /// </value>
        public string ClientName
        {
            get
            {
                var userDetails = UserContext.Current != null ? UserContext.Current.UserDetails : null;
                var clientName = userDetails != null ? userDetails.ClientName : null;

                // Client name might be not set in database. Try to use client configuration in such case
                if (string.IsNullOrEmpty(clientName))
                {
                    var clientConfiguration = ConfigurationManager.ClientConfiguration ?? ConfigurationManager.DefaultClientConfiguration;
                    clientName = clientConfiguration != null
                        ? clientConfiguration.Name
                        : "Default";
                }

                return clientName;
            }
        }

        /// <summary>
        /// Returns whether current execution occurs on server's end
        /// </summary>
        public bool IsServerSide
        {
            get { return HostingEnvironment.IsHosted; }
        }

        /// <summary>
        /// Gets the name of the current environment
        /// </summary>
        /// <value>
        /// The name of the current environment.
        /// </value>
        public string EnvironmentName
        {
            get
            {
                var environmentName = ConfigurationManager.CustomAppSettings["ApplicationServerEnvironmentName"];
                return string.IsNullOrEmpty(environmentName) ? "Deployed" : environmentName;
            }
        }
    }
}