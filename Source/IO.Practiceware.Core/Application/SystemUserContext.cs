using System;
using System.Diagnostics;
using System.Security.Principal;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.Security;

namespace IO.Practiceware.Application
{
    /// <summary>
    /// Executes as a system user until disposed.
    /// </summary>
    public class SystemUserContext : IDisposable
    {
        private readonly IPrincipal _oldPrincipal;

        public SystemUserContext(string authenticationToken = null)
        {
            _oldPrincipal = PrincipalContext.Current.IfNotNull(p => p.Principal);

            const string systemUserName = "system";

            var userDetails = new UserDetails
            {
                Id = 0,
                UserName = systemUserName,
                DisplayName = systemUserName,
                FirstName = string.Empty,
                BillingOrganizationName = string.Empty
            };

            var user = new User
            {
                Id = string.Empty,
                UserName = systemUserName + "@" + authenticationToken,
                Password = string.Empty,
                IsApproved = true,
                IsOnline = true,
                RolesSource = new Role[0],
                Tag = userDetails
            };

            // set principal to a faux system account.
            PrincipalContext.Current
                .IfNotNull(p => p.Principal = new UserPrincipal(new UserIdentity(user, true, string.Empty)));

            if (!string.IsNullOrEmpty(authenticationToken))
            {
                try
                {
                    // initialize client name and id now that the user is set (and we can access the correct db)
                    userDetails.ClientName = ApplicationSettings.Cached.GetSetting<string>("ClientName").IfNotNull(s => s.Value);
                    userDetails.ClientId = ApplicationSettings.Cached.GetSetting<string>("ClientId").IfNotNull(s => s.Value);
                }
                catch (Exception ex)
                {
                    Trace.TraceError("Failed to read ClientName and ClientId for token {0}. Error: {1}", authenticationToken, ex);
                }
            }
        }

        public void Dispose()
        {
            PrincipalContext.Current.IfNotNull(p => p.Principal = _oldPrincipal);
        }
    }
}