﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Transactions;
using IO.Practiceware.Application;
using IO.Practiceware.Data;
using IO.Practiceware.Integration.Cda;
using IO.Practiceware.Services.Cda;
using IO.Practiceware.Services.ExternalSystems;
using Soaf;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Presentation;

[assembly: Component(typeof(CdaService), typeof(ICdaService))]

namespace IO.Practiceware.Services.Cda
{
    [DataContract]
    public class LoincModel
    {
        [DataMember]
        public string Component { get; set; }

        [DataMember]
        public string LoincNum { get; set; }
    }

    [DataContract]
    public class SnomedDescriptionModel
    {
        [DataMember]
        public string Term { get; set; }

        [DataMember]
        public string ConceptId { get; set; }
    }

    [DataContract]
    public class CvxAdministeredModel
    {
        [DataMember]
        public string FullVaccineName { get; set; }

        [DataMember]
        public double CvxCode { get; set; }
    }

    [DataContract]
    public class RxNormModel
    {
        [DataMember]
        public string Str { get; set; }

        [DataMember]
        public string RxCui { get; set; }
    }

    [DataContract]
    public class CdcRaceAndEthnicityModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Name { get; set; }
    }

    [DataContract]
    public class CdcRaceModel : CdcRaceAndEthnicityModel
    {

    }

    [DataContract]
    public class CdcEthnicityModel : CdcRaceAndEthnicityModel
    {
    }

    [DataContract]
    public class QualityDataModelValueSetEntryModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string CodeSystemOid { get; set; }

        [DataMember]
        public string ValueSetOid { get; set; }

        public QualityDataModelValueSet ValueSet { get; set; }

        public CodeSystem CodeSystem
        {
            get { return Enums.GetValues<CodeSystem>().FirstOrDefault(v => v.Oid() == CodeSystemOid); }
        }
    }

    /// <summary>
    /// Service that queries external clinical databases using external entity keys.
    /// </summary>
#if DEBUG
    [BackgroundThreadOnly]
#endif
    public interface ICdaService
    {
        /// <summary>
        /// Queries the loinc external clinical database using the provided loinc ids.
        /// </summary>
        /// <param name="loincIds">Ids that are derived from external entity keys which map to loinc.LOINC_NUM.</param>
        /// <returns></returns>
        IEnumerable<LoincModel> GetLoincModels(IEnumerable<string> loincIds);

        /// <summary>
        /// Queries the snomed external clinical database using the provided snomed ids.
        /// </summary>
        /// <param name="snomedDescriptionIds">Ids that are derived from external entity keys which map to Snomed.ConceptId.</param>
        /// <returns></returns>
        IEnumerable<SnomedDescriptionModel> GetSnomedDescriptionModels(IEnumerable<string> snomedDescriptionIds);

        /// <summary>
        /// Queries the CVX external clinical database using the provided CVX ids.
        /// </summary>
        /// <param name="cvxAdministeredIds">Ids that are derived from external entity keys which map to CVX.CVXCode.</param>
        /// <returns></returns>
        IEnumerable<CvxAdministeredModel> GetCvxAdministeredModels(IEnumerable<double> cvxAdministeredIds);

        /// <summary>
        /// Queries the RxNorm external clinical database using the provided RxNorm ids.
        /// </summary>
        /// <param name="rxNormIds">Ids that are derived from external entity keys which map to RxNorm.RXCUI.</param>
        /// <returns></returns>
        IEnumerable<RxNormModel> GetRxNormModels(IEnumerable<string> rxNormIds);

        /// <summary>
        /// Queries the Cdc external clinical database using the provided cdc race ids.
        /// </summary>
        /// <param name="cdcRaceIds"></param>
        /// <returns></returns>
        IEnumerable<CdcRaceModel> GetCdcRaceModels(IEnumerable<string> cdcRaceIds);

        /// <summary>
        /// Queries the Cdc external clinical database using the provided cdc ethnicity ids.
        /// </summary>
        /// <param name="cdcEthnicityIds"></param>
        /// <returns></returns>
        IEnumerable<CdcEthnicityModel> GetCdcEthnicityModels(IEnumerable<string> cdcEthnicityIds);

        /// <summary>
        /// Queries models from qrda schema of external data.
        /// </summary>
        /// <returns></returns>
        IEnumerable<QualityDataModelValueSetEntryModel> GetQualityDataModelValueSetModels(IEnumerable<QualityDataModelValueSet> valueSets);
    }

    public class CdaService : ICdaService
    {
        private static readonly CacheHoldInterval ModelsCacheLifetime = CacheHoldInterval.Custom(TimeSpan.FromMinutes(20));

        public IEnumerable<LoincModel> GetLoincModels(IEnumerable<string> loincIds)
        {
            return GetModels<LoincModel>(loincIds, missingIds =>
            {
                var stringIds = missingIds.Select(i => "'{0}'".FormatWith(i)).Join();
                var query = @"SELECT LOINC_NUM AS LoincNum, COMPONENT AS Component FROM loinc.loinc WHERE LOINC_NUM IN ({0})".FormatWith(stringIds);
                return query;
            }, model => model.LoincNum);
        }

        public IEnumerable<SnomedDescriptionModel> GetSnomedDescriptionModels(IEnumerable<string> snomedDescriptionIds)
        {
            return GetModels<SnomedDescriptionModel>(snomedDescriptionIds, missingIds =>
            {
                var stringIds = missingIds.Select(i => "'{0}'".FormatWith(i)).Join();
                var query = @"SELECT 
term AS Term, conceptId AS ConceptId 
FROM [snomed].[Description] WHERE ConceptId IN ({0})
ORDER BY CASE WHEN typeId = '900000000000003001' THEN 1
ELSE 2
END ASC
,LEN(term) ASC".FormatWith(stringIds);
                return query;
            }, model => model.ConceptId);
        }

        public IEnumerable<CvxAdministeredModel> GetCvxAdministeredModels(IEnumerable<double> cvxAdministeredIds)
        {
            return GetModels<CvxAdministeredModel>(cvxAdministeredIds.Select(i => i.ToString()), missingIds =>
            {
                var query = @"SELECT CVXCode AS CvxCode, FullVaccineName AS FullVaccineName FROM cvx.VaccineAdministered WHERE CVXCode IN ({0})".FormatWith(missingIds.Join());
                return query;
            }, model => model.CvxCode.ToString());
        }

        public IEnumerable<RxNormModel> GetRxNormModels(IEnumerable<string> rxNormIds)
        {
            return GetModels<RxNormModel>(rxNormIds, missingIds =>
            {
                var stringIds = missingIds.Select(i => "'{0}'".FormatWith(i)).Join();
                var query = @"SELECT 
RXCUI AS RxCui
,[STR] AS Str
FROM 
(
	SELECT 
	RXCUI,[STR],[SAB]
	FROM rxNorm.RXNCONSO WHERE RXCUI IN ({0})
)v
ORDER BY CASE v.SAB 
	WHEN 'RXNORM'
		THEN 1
	WHEN 'SNOMEDCT_US'
		THEN 2
	WHEN 'MTHSPL'
		THEN 3
	ELSE 4
END ASC

".FormatWith(stringIds);

                return query;
            }, model => model.RxCui);
        }

        public IEnumerable<CdcRaceModel> GetCdcRaceModels(IEnumerable<string> cdcRaceIds)
        {
            return GetModels<CdcRaceModel>(cdcRaceIds, missingIds =>
            {
                var stringIds = missingIds.Select(i => "'{0}'".FormatWith(i)).Join();
                var query = @"SELECT Id, Code, Name FROM cdc.RaceAndEthnicityCodeSet WHERE Id IN ({0})".FormatWith(stringIds);
                return query;
            }, model => model.Id.ToString());
        }

        public IEnumerable<CdcEthnicityModel> GetCdcEthnicityModels(IEnumerable<string> cdcEthnicityIds)
        {
            return GetModels<CdcEthnicityModel>(cdcEthnicityIds, missingIds =>
            {
                var stringIds = missingIds.Select(i => "'{0}'".FormatWith(i)).Join();
                var query = @"SELECT Id, Code, Name FROM cdc.RaceAndEthnicityCodeSet WHERE Id IN ({0})".FormatWith(stringIds);
                return query;
            }, model => model.Id.ToString());
        }

        public IEnumerable<QualityDataModelValueSetEntryModel> GetQualityDataModelValueSetModels(IEnumerable<QualityDataModelValueSet> valueSets)
        {
            return GetModels<QualityDataModelValueSetEntryModel>(valueSets.Select(v => v.Oid()), missingIds =>
            {
                var stringIds = missingIds.Select(i => "'{0}'".FormatWith(i)).Join();
                var query = @"SELECT CONVERT(INT, Id) AS Id, Code, Description, [Code System OID] AS CodeSystemOid, [Value Set OID] AS ValueSetOid FROM qdm.MappingSets WHERE [Value Set OID] IN ({0})".FormatWith(stringIds);
                return query;
            }, model => model.ValueSetOid);
        }

        private static IEnumerable<T> RunQueryAndMapToModel<T>(string query)
        {
            // Cannot use the current ambient transaction when querying external data sources, so we suppress the current TransactionScope
            using (new TransactionScope(TransactionScopeOption.Suppress))
            using (IDbConnection connection = DbConnectionFactory.PracticeRepository)
            {
                return connection.Execute<DataTable>(query).AsEnumerable()
                        .Select(i => i.MapTo<T>()).ToList();
            }
        }

        private IEnumerable<TModel> GetModels<TModel>(IEnumerable<string> ids, Func<IEnumerable<string>, string> getLoadModelsQuery, Func<TModel, string> getIdFromModel)
        {
            var tenantKey = ContextData.Keys.Contains(ExternalSystemServices.CustomImplicitCachingClientKeyContextDataKey)
                ? (string)ContextData.Get(ExternalSystemServices.CustomImplicitCachingClientKeyContextDataKey)
                : ApplicationContext.Current.ClientKey;

            var modelTypeName = typeof(TModel).FullName;
            var modelsStorage = !string.IsNullOrEmpty(tenantKey)
                ? this.ExecuteCached(i => i.GetModelsStorage<TModel>(tenantKey, modelTypeName), ModelsCacheLifetime)
                : null;

            var result = new List<TModel>();
            var missingKeysInStorage = new List<string>();

            // Evaluate missing keys
            if (modelsStorage != null)
            {
                foreach (var entityKey in ids)
                {
                    if (modelsStorage.ContainsKey(entityKey))
                    {
                        result.Add(modelsStorage[entityKey]);
                    }
                    else
                    {
                        missingKeysInStorage.Add(entityKey);
                    }
                }
            }
            else
            {
                missingKeysInStorage.AddRange(ids);
            }

            // Fetch missing
            if (missingKeysInStorage.Any())
            {
                var query = getLoadModelsQuery(missingKeysInStorage);
                var loadedModels = RunQueryAndMapToModel<TModel>(query);
                foreach (var loadedModel in loadedModels)
                {
                    result.Add(loadedModel);

                    if (modelsStorage != null)
                    {
                        modelsStorage.AddOrUpdate(getIdFromModel(loadedModel), loadedModel, (s, model) => loadedModel);    
                    }
                }
            }

            return result;
        }

// ReSharper disable UnusedParameter.Local
        private ConcurrentDictionary<string, TModel> GetModelsStorage<TModel>(string tenantKey, string modelTypeName)
// ReSharper restore UnusedParameter.Local
        {
            return new ConcurrentDictionary<string, TModel>();
        }
    }
}
