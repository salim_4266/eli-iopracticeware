﻿using Soaf.Presentation;
using System.Collections.Generic;

namespace IO.Practiceware.Services.Icd
{
	/// <summary>
	/// Service that queries external clinical databases using external entity keys.
	/// </summary>
	[BackgroundThreadOnly]
	public interface IIcd9To10Service
	{
		/// <summary>
		/// Gets the icd10s for the provided icd9 codes.
		/// </summary>
		/// <param name="icd9Ids">Ids that are icd9 codes</param>
		/// <returns></returns>
		IEnumerable<Icd9To10Model> GetIcd9To10Models(IEnumerable<IcdModel> icd9Ids);

        IEnumerable<Icd9To10Model> GetBulkICD9to10Models(IEnumerable<IcdModel> icd9Ids);

		IEnumerable<IcdModel> GetIcd10Codes(string searchText);

        IEnumerable<IcdModel> GetIcd10Codes(string searchText, string CallingFrom);

        IEnumerable<IcdModel> GetIcd9Codes();

        IEnumerable<IcdModel> GetIcd10Mappings(string icd9Code);
	}
}
