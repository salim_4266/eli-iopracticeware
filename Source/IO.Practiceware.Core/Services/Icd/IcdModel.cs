﻿using System.Runtime.Serialization;

namespace IO.Practiceware.Services.Icd
{
	[DataContract]
	public class IcdModel
	{
		[DataMember]
		public int Id { get; set; }
		[DataMember]
		public int Type { get; set; }
		[DataMember]
		public string Code { get; set; }
		[DataMember]
		public string Description { get; set; }
		[DataMember]
		public string Lingo { get; set; }
		[DataMember]
		public string key { get; set; }
		[DataMember]
		public string EyeContext { get; set; }
		[DataMember]
		public string EyeLidLocation { get; set; }
		[DataMember]
		public string Side { get; set; }
	}
}
