﻿using System;
using System.Collections.Generic;

namespace IO.Practiceware.Services.Icd
{
    internal class MedflowIcd10CodeResponse
    {
        public List<Icd10CodeModel> results {get; set;}
    }

    internal class Icd10CodeModel
    {
        public string diagnosisCode { get; set; }

        public string diagnosisDescription { get; set; }
    }
}
