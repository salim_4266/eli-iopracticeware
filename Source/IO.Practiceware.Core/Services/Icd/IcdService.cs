﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using IO.Practiceware.Services.Icd;
using Newtonsoft.Json;
using Soaf.ComponentModel;
using System.Data;
using IO.Practiceware.Data;
using Soaf.Data;
using System.Data.SqlClient;
using IO.Practiceware.Model;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Headers;

[assembly: Component(typeof(Icd9To10Service), typeof(IIcd9To10Service))]
[assembly: Component(typeof(Icd10Service), typeof(IIcd10Service))]

namespace IO.Practiceware.Services.Icd
{
    public class Icd9To10Service : IIcd9To10Service
    {

        #region Constant properties

        private const string Icd10ApiUserName = "ecliop";

        private const string Icd10ApiPassword = "uhhNrVq9gUn9k0pMly5IfvbrTWOpbTL9G5CBsM0I5bzsrLdDnahnwBveSoE=";

        private const string Icd10BaseApiUrl = "https://eclicdapi.eyecareleaders.com/ECLICD/api/ICD/";

        #endregion

        #region Helpers

        private string GetToken()
        {
            MedflowLoginModel requestLogin = new MedflowLoginModel();
            requestLogin.UserName = Icd10ApiUserName;
            requestLogin.Password = Icd10ApiPassword;
            string jsonbody = JsonConvert.SerializeObject(requestLogin);
            string token = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Icd10BaseApiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                    var content = new StringContent(jsonbody, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = client.PostAsync(Icd10BaseApiUrl + "Authenticate", content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var responseString = response.Content.ReadAsStringAsync().Result;
                        token = JsonConvert.DeserializeObject<string>(responseString.ToString());
                        if (token != null)
                        {
                            return token.Replace("Token:", "");
                        }
                    }
                }
            }
            catch( Exception ex)
            {
                return ex.Message;
            }
            return token;
        }

        private bool Icd10CodesExist()
        {
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                bool retStatus = dbConnection.Execute<bool>("IF EXISTS( SELECT 1 FROM icd10.ICD10)  SELECT 1  ELSE  SELECT 0");
                return retStatus;
            }
        }

        private bool Icd9CodesExist()
        {
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                bool retStatus = dbConnection.Execute<bool>("IF EXISTS(select  1 from model.ClinicalConditions c inner join model.ExternalSystemEntityMappings e on c.id= cast(e.practiceRepositoryEntityKey AS int) inner join model.externalsystementities em on em.id = e.externalsystementityid where  em.ExternalSystemid = '" + (int)ExternalSystemId.Icd9 + "'  and e.PracticeRepositoryEntityId = " + (int)PracticeRepositoryEntityId.ClinicalCondition + ") SELECT 1 Else SELECT 0");
                return retStatus;
            }
        }

        private void InsertIcd10Codes(List<IcdModel> icdCodeList)
        {
            using (var connection = new SqlConnection(DbConnectionFactory.PracticeRepository.ConnectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                using (var bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                {

                    bulkCopy.BatchSize = 100;
                    bulkCopy.DestinationTableName = "icd10.ICD10";

                    // Set up the column mappings by name.
                    SqlBulkCopyColumnMapping mapCode = new SqlBulkCopyColumnMapping("Code", "DiagnosisCode");
                    bulkCopy.ColumnMappings.Add(mapCode);

                    SqlBulkCopyColumnMapping mapDescription = new SqlBulkCopyColumnMapping("Description", "DiagnosisDescription");
                    bulkCopy.ColumnMappings.Add(mapDescription);

                    try
                    {
                        bulkCopy.WriteToServer(icdCodeList.AsDataTable());
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        connection.Close();
                    }
                }
                transaction.Commit();
            }
        }

        private List<IcdModel> GetIcd10CodesFromDb(string searchText)
        {
            if (Icd10CodesExist())
            {
                DataTable icdCodeTable;
                var icd10Codes = new List<IcdModel>();
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    if (!string.IsNullOrEmpty(searchText))
                    {
                        icdCodeTable = dbConnection.Execute<DataTable>("SELECT * FROM icd10.ICD10 with (nolock) where DiagnosisCode  like '%" + searchText + "%'");
                    }
                    else
                    {
                        icdCodeTable = dbConnection.Execute<DataTable>("SELECT  * FROM icd10.ICD10 with (nolock)");
                    }
                }

                if (icdCodeTable != null && icdCodeTable.Rows.Count > 0)
                {
                    foreach (DataRow item in icdCodeTable.Rows)
                    {
                        icd10Codes.Add(new IcdModel
                        {
                            Code = item["DiagnosisCode"] as string,
                            Description = item["DiagnosisDescription"] as string
                        });
                    }
                }
                return icd10Codes;
            }
            else
            {
                return LoadIcd10CodesFromAPI();
            }
        }

        private List<IcdModel> LoadIcd10CodesFromAPI()
        {
            var icd10Codes = new List<IcdModel>();
            // for API need to use these credentials
            string apiToken = GetToken();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Icd10BaseApiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + apiToken);
                    HttpResponseMessage response = client.PostAsync(Icd10BaseApiUrl + "GetAllICD10ListIOP", null).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var responseItem = response.Content.ReadAsAsync<MedflowIcd10CodeResponse>().Result;
                        
                        if(responseItem != null && responseItem.results.Count > 0)
                        {
                            foreach(var item in responseItem.results)
                            {
                                icd10Codes.Add(new IcdModel()
                                {
                                    Code = item.diagnosisCode,
                                    Description = item.diagnosisDescription
                                });
                            }
                            InsertIcd10Codes(icd10Codes);
                        }
                    }
                }
            }
            catch
            {

            }
            var icd10CodesCollection = new ObservableCollection<IcdModel>(icd10Codes);
            return icd10CodesCollection.Distinct().ToList();
        }

        private List<IcdModel> GetIcd9CodesFromDb()
        {
            DataTable icdCodeTable;
            List<IcdModel> icdCodes = new List<IcdModel>();
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                icdCodeTable = dbConnection.Execute<DataTable>("select  e.id,e.ExternalSystemEntityKey,e.PracticeRepositoryEntityKey,c.name from model.ClinicalConditions c inner join model.ExternalSystemEntityMappings e on c.id= cast(e.practiceRepositoryEntityKey AS int) inner join model.externalsystementities em on em.id = e.externalsystementityid where  em.ExternalSystemid = '" + (int)ExternalSystemId.Icd9 + "'  and e.PracticeRepositoryEntityId = " + (int)PracticeRepositoryEntityId.ClinicalCondition + " group by e.id,e.ExternalSystemEntityKey,e.PracticeRepositoryEntityKey,c.name order by e.ExternalSystemEntityKey");
            }
            if (icdCodeTable != null && icdCodeTable.Rows.Count > 0)
            {
                foreach (DataRow item in icdCodeTable.Rows)
                {
                    icdCodes.Add(new IcdModel
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Code = Convert.ToString(item["ExternalSystemEntityKey"]),
                        Description = Convert.ToString(item["name"]),
                        key = Convert.ToString(item["PracticeRepositoryEntityKey"])
                    });
                }

            }
            icdCodeTable = null;
            return icdCodes;
        }

        private IEnumerable<Icd9To10Model> GetBulkICD10Mappings(IEnumerable<IcdModel> ICD9codes, NameValueCollection values)
        {
            // for API need to use these credentials
            string apiToken = GetToken();
            var requestModel = new MedflowApiRequestModel();
            var responseModel = new List<Icd9To10Model>();
            foreach (var icd9 in ICD9codes)
            {
                requestModel.Icd10Mappings.Add(new Medflow_Icd9Model() { Icd9 = icd9.Code, EyeType = icd9.EyeContext, Description = icd9.Code });
            }
            string jsonbody = JsonConvert.SerializeObject(requestModel);
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Icd10BaseApiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + apiToken);
                    var content = new StringContent(jsonbody, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(Icd10BaseApiUrl + "GetICD9ListIOP", content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var responseItem = response.Content.ReadAsAsync<MedflowApiResponseModel>().Result;
                        foreach (var item in responseItem.results)
                        {
                            var selectedIcd = ICD9codes.Where(x => x.Code == item.icd9 && x.EyeContext == item.eyeType).SingleOrDefault();
                            var tempModel = new List<IcdModel>();
                            var model = new Icd9To10Model()
                            {
                                Icd9 = new IcdModel()
                                {
                                    Code = selectedIcd.Code,
                                    Description = selectedIcd.Description,
                                    EyeContext = selectedIcd.EyeContext,
                                    EyeLidLocation = selectedIcd.EyeLidLocation,
                                    Id = selectedIcd.Id,
                                    key = selectedIcd.key,
                                    Lingo = selectedIcd.Lingo,
                                    Side = selectedIcd.EyeContext.ToLower() == "od" ? "Right" : selectedIcd.EyeContext.ToLower() == "os" ? "Left" : "Bilateral"
                                }
                            };
                            foreach (var icd10 in item.icd10Mappings)
                            {
                                tempModel.Add(new IcdModel()
                                {
                                    Code = icd10.icd10,
                                    Description = icd10.icd10desc,
                                    EyeContext = "",
                                    EyeLidLocation = "",
                                    Id = int.Parse(icd10.id),
                                    key = selectedIcd.key,
                                    Lingo = selectedIcd.Lingo,
                                    Side = selectedIcd.EyeContext.ToLower() == "od" ? "right" : selectedIcd.EyeContext.ToLower() == "os" ? "left" : "both"

                                });
                            }
                            model.Icd10Mappings = tempModel;
                            responseModel.Add(model);
                        }
                    }
                }
            }
            catch
            {

            }
            return responseModel;
        }

        #endregion

        public IEnumerable<Icd9To10Model> GetIcd9To10Models(IEnumerable<IcdModel> icd9Ids)
        {
            var icd9To10Mappings = new ObservableCollection<Icd9To10Model>();
            foreach (IcdModel icd9 in icd9Ids)
            {
                var icd9To10Mapping = new Icd9To10Model();
                icd9To10Mapping.Icd9 = icd9;
                var code = icd9.Code;
                int i = code.IndexOf('.') + 1;
                if (i > 0)
                {
                    int j = code.Substring(i, code.Length - i).IndexOf('.');
                    if (j > 0)
                    {
                        code = code.Substring(0, i + j);
                    }
                }
                icd9To10Mapping.Icd10Mappings = GetIcd10Mappings(code);
                icd9To10Mappings.Add(icd9To10Mapping);
            }
            return icd9To10Mappings.ToList();
        }

        public IEnumerable<Icd9To10Model> GetBulkICD9to10Models(IEnumerable<IcdModel> icd9Codes)
        {
            var icd9To10Mappings = new ObservableCollection<Icd9To10Model>();
            int countParam = 0;

            List<IcdModel> bulkIcd9Codes = new List<IcdModel>();

            //If multiple EyeLids create New code as we Process in Bulk.
            string eyeLid = string.Empty;
            foreach (IcdModel icd9 in icd9Codes)
            {
                eyeLid = string.Empty;
                string[] lids = icd9.EyeLidLocation.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (lids != null && lids.Length > 0)
                {
                    for (int i = 0; i < lids.Length; i += 1)
                    {
                        if (!string.IsNullOrEmpty(lids[i]))  // parameter - lid_loc;
                        {
                            if (lids[i].ToLower().Contains("lower"))
                            {
                                eyeLid = "lower";
                            }
                            else if (lids[i].ToLower().Contains("upper"))
                            {
                                eyeLid = "upper";
                            }
                            if (!string.IsNullOrEmpty(eyeLid) && i > 0)
                            {
                                var icd9CodeNew = new IcdModel()
                                {
                                    Code = icd9.Code,
                                    Description = icd9.Description,
                                    EyeContext = icd9.EyeContext,
                                    Id = icd9.Id,
                                    key = icd9.key,
                                    Side = icd9.Side,
                                    Type = icd9.Type
                                };
                                icd9CodeNew.EyeLidLocation = eyeLid;
                                bulkIcd9Codes.Add(icd9CodeNew);
                            }
                            else
                            {
                                icd9.EyeLidLocation = eyeLid;
                                bulkIcd9Codes.Add(icd9);
                            }
                        }
                    }
                }
                else
                {
                    bulkIcd9Codes.Add(icd9);
                }
            }

            int TotalDiagnosisCount = bulkIcd9Codes.Count;
            int MaxElementsAllowed = 15;

            int iterationCount = TotalDiagnosisCount / MaxElementsAllowed;
            if (TotalDiagnosisCount % MaxElementsAllowed == 0)
            {
                iterationCount = iterationCount - 1;
            }
            int currentDiagCount = 0;

            List<Icd9To10Model> allIcd9To10Codes = new List<Icd9To10Model>();

            int currentTotalDiagCount = 0;

            for (int chunkCount = 0; chunkCount <= iterationCount; chunkCount++)
            {
                if (TotalDiagnosisCount >= (chunkCount + 1) * MaxElementsAllowed)
                {
                    currentTotalDiagCount = ((chunkCount + 1) * MaxElementsAllowed) - 1;
                }
                else
                {
                    currentTotalDiagCount = TotalDiagnosisCount - 1;
                }

                List<IcdModel> bulkIcd9Codes_15 = new List<IcdModel>();
                var values = new NameValueCollection();
                while (currentDiagCount <= currentTotalDiagCount)
                {
                    bulkIcd9Codes_15.Add(bulkIcd9Codes[currentDiagCount]);
                    DataTable icdCodeTable;
                    string description = string.Empty;
                    string lingo = string.Empty;
                    using (var dbConnection = DbConnectionFactory.PracticeRepository)
                    {
                        icdCodeTable = dbConnection.Execute<DataTable>("SELECT TOP 1 DiagnosisName, ReviewSystemLingo FROM [dm].[PrimaryDiagnosisTable] WHERE DiagnosisNextLevel = '" + bulkIcd9Codes[currentDiagCount].Code + "'");
                    }

                    if (icdCodeTable != null && icdCodeTable.Rows.Count > 0)
                    {
                        foreach (DataRow item in icdCodeTable.Rows)
                        {
                            description = item["DiagnosisName"].ToString();
                            lingo = item["ReviewSystemLingo"].ToString();
                        }
                    }

                    bulkIcd9Codes[currentDiagCount].Lingo = lingo;
                    bulkIcd9Codes[currentDiagCount].Description = description;

                    if (bulkIcd9Codes[currentDiagCount].EyeContext.ToLower() == "od")
                    {
                        values["search[" + countParam.ToString() + "][side]"] = "Right";
                    }
                    else if (bulkIcd9Codes[currentDiagCount].EyeContext.ToLower() == "os")
                    {
                        values["search[" + countParam.ToString() + "][side]"] = "Left";
                    }
                    else if (bulkIcd9Codes[currentDiagCount].EyeContext.ToLower() == "ou")
                    {
                        values["search[" + countParam.ToString() + "][side]"] = "Bilateral";
                    }

                    values["search[" + countParam.ToString() + "][lid_loc]"] = bulkIcd9Codes[currentDiagCount].EyeLidLocation; // parameter - lid_loc;
                    values["search[" + countParam.ToString() + "][icd9_nl]"] = bulkIcd9Codes[currentDiagCount].Code; // parameter - icd9_nl;
                    values["search[" + countParam.ToString() + "][icd9_desc]"] = description; // parameter - icd9_desc;
                    values["search[" + countParam.ToString() + "][ReviewSystemLingo]"] = lingo; // parameter - ReviewSystemLingo;

                    countParam += 1;
                    currentDiagCount = currentDiagCount + 1;
                }
                var icd10Mappings = GetBulkICD10Mappings(bulkIcd9Codes_15, values);
                allIcd9To10Codes.AddRange(icd10Mappings);
            }
            return allIcd9To10Codes.ToList();
        }

        public IEnumerable<IcdModel> GetIcd10Mappings(string icd9Code)
        {
            var client = new WebClient();
            client.BaseAddress = Icd10BaseApiUrl;
            client.Headers[HttpRequestHeader.Authorization] = "Bearer " + GetToken();
            var result = client.DownloadString(Icd10BaseApiUrl + "");
            XmlDocument xmlDoc = JsonConvert.DeserializeXmlNode(result, "ICD9to10");
            XDocument xDoc = XmlParserHelper.GetXDocument(xmlDoc.InnerXml);
            var gemElements = new List<XElement>();
            var extendedElements = new List<XElement>();
            if (XmlParserHelper.FindElements(xDoc.Root, "gem") != null)
                gemElements = XmlParserHelper.FindElements(xDoc.Root, "gem").ToList();
            if (XmlParserHelper.FindElements(xDoc.Root, "extended") != null)
                extendedElements = XmlParserHelper.FindElements(xDoc.Root, "extended").ToList();
            IEnumerable<XElement> foundElements = gemElements.Concat(extendedElements);
            var icd10Mappings = new ObservableCollection<IcdModel>();
            foreach (var element in foundElements)
            {
                var icd10 = new IcdModel();
                var xCodeElement = element.Element("code_name");
                if (xCodeElement != null) icd10.Code = xCodeElement.Value;
                var xDescElement = element.Element("code_description");
                if (xDescElement != null) icd10.Description = xDescElement.Value;
                icd10Mappings.Add(icd10);
            }
            return icd10Mappings.ToList();
        }

        public IEnumerable<IcdModel> GetIcd9Codes()
        {
            var icd9Codes = new List<IcdModel>();

            if (Icd9CodesExist())
            {
                icd9Codes = GetIcd9CodesFromDb();
            }

            var icd9CodesCollection = new ObservableCollection<IcdModel>(icd9Codes);
            icd9Codes = null;
            return icd9CodesCollection.Distinct().ToList();
        }

        public IEnumerable<IcdModel> GetIcd10Codes(string searchText)
        {
            var icd10Codes = new List<IcdModel>();
            icd10Codes = GetIcd10CodesFromDb(searchText);
            return icd10Codes;
        }

        public IEnumerable<IcdModel> GetIcd10Codes(string searchText, string CallingFrom)
        {
            var icd10Codes = new List<IcdModel>();
            if (CallingFrom == "ExamBilling")
            {
                icd10Codes = GetIcd10CodesFromDb("");
            }
            else
            {
                icd10Codes = GetIcd10CodesFromDb(searchText);
            }
            return icd10Codes.Distinct().ToList();
        }
    }
}
