﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IO.Practiceware.Services.Icd
{
	[DataContract]
	public class Icd9To10Model
	{
		[DataMember]
		public IcdModel Icd9 { get; set; }
		[DataMember]
		public IEnumerable<IcdModel> Icd10Mappings { get; set; }
	}
}
