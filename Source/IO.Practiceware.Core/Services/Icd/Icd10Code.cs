﻿
namespace IO.Practiceware.Services.Icd
{
	public class Icd10Code
	{
		public string code { get; set; }
		public string descriptor { get; set; }
	}
}
