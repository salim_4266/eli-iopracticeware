﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Services.Icd
{
	public interface IIcd10Service
	{
		string GetIcd10DiagnosisCodesQuery(int invoiceId);
		int GetIcd10DiagnosisCodeCount(int invoiceId);
	}
}
