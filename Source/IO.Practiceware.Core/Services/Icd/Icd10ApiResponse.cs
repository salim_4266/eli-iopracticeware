﻿using System.Collections.Generic;

namespace IO.Practiceware.Services.Icd
{
	public class Icd10ApiResponse
	{
		public string status { get; set; }

		public List<Icd10Code> data { get; set; }
	}
}
