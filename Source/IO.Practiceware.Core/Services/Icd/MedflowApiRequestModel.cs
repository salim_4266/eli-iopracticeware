﻿using System.Collections.Generic;

namespace IO.Practiceware.Services.Icd
{
	public class MedflowApiRequestModel
	{
		public MedflowApiRequestModel()
		{
			this.Icd10Mappings = new List<Medflow_Icd9Model>();
		}
		public List<Medflow_Icd9Model> Icd10Mappings { get; set; } 
	}

	public class Medflow_Icd9Model
	{
		public string Icd9 { get; set; }

        public string EyeType { get; set; }

        public string Description { get; set; }
	}
}
