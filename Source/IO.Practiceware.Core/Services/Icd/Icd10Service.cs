﻿using IO.Practiceware.Data;
using Soaf.Data;
using System.Data;

namespace IO.Practiceware.Services.Icd
{
	public class Icd10Service : IIcd10Service
	{
		public string GetIcd10DiagnosisCodesQuery(int invoiceId)
		{
			string icd10DiagQuery = "Select Distinct diag.Id, DiagnosisCode, diag.DiagnosisDescription AS DiagnosisDescription, diag.OrdinalId,Case When link.BillingDiagnosisId Is Not Null Then 1 ELSE 0 End As IsLinked From " +
									 "model.BillingDiagnosisIcd10(NOLOCK) diag Left Join model.BillingServiceBillingDiagnosis(NOLOCK) link " +
									 "ON diag.Id = link.BillingDiagnosisId And link.IsIcd10 = 1 " +
									 "Where InvoiceId = " + invoiceId + " And DiagnosisCode Is Not Null Order by diag.OrdinalId";
			return icd10DiagQuery;
		}

		public int GetIcd10DiagnosisCodeCount(int invoiceId)
		{
			string icd10DiagQuery = GetIcd10DiagnosisCodesQuery(invoiceId);

			using (var dbConnection = DbConnectionFactory.PracticeRepository)
			{
				var diagTable = dbConnection.Execute<DataTable>(icd10DiagQuery);
				return diagTable != null ? diagTable.Rows.Count : 0;
			}
		}
	}
}
