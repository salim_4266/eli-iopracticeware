﻿using System.Collections.Generic;

namespace IO.Practiceware.Services.Icd
{
	public class MedflowApiResponseModel
	{
		public IEnumerable<Medflow_Icd9To10Model> results { get; set; }
	}

	public class Medflow_Icd9To10Model
	{
		public string icd9 { get; set; }

        public string eyeType { get; set; }

        public string description { get; set; }

		public IEnumerable<Medflow_Icd10Model> icd10Mappings { get; set; }
	}

	public class Medflow_Icd10Model
	{
		public string id { get; set; }

		public string icd9code { get; set; }

		public string icd9nl { get; set; }

		public string icd10 { get; set; }

		public string icd10desc { get; set; }
	}
}
