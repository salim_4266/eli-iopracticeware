﻿
namespace IO.Practiceware.Services.Icd
{
	public class MedflowLoginModel
	{
		public string UserName { get; set; }

		public string Password { get; set; }
	}
}
