﻿using System.Collections;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Services.EntityLink;
using IO.Practiceware.Services.ExternalSystems;
using Soaf;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using Soaf.Reflection;

[assembly: Component(typeof(ExternalSystemService), typeof(IExternalSystemService))]

namespace IO.Practiceware.Services.ExternalSystems
{
    /// <summary>
    /// </summary>
    public interface IExternalSystemService
    {
        /// <summary>
        /// Gets all ExternalSystemEntityMappings for the given PracticeRepositoryEntity.
        /// </summary>
        /// <param name="practiceRepositoryEntityName"></param>
        /// <param name="externalSystemName"></param>
        /// <param name="externalSystemEntityName"></param>
        /// <returns></returns>
        IEnumerable<ExternalSystemEntityMapping> GetAllMappings(string practiceRepositoryEntityName, string externalSystemName = null, string externalSystemEntityName = null);

        /// <summary>
        ///   Gets the mappings for the practice repository entity for the external system information specified (or for all).
        /// </summary>
        /// <param name="entities"> The entity. </param>
        /// <param name="externalSystemName"> Name of the external system. </param>
        /// <param name="externalSystemEntityName"> Name of the external system entity. </param>
        /// <param name="externalSystemEntityKey"> The external system entity key. </param>
        /// <returns> </returns>
        IEnumerable<ExternalSystemEntityMapping> GetMappings(IEnumerable entities, string externalSystemName = null, string externalSystemEntityName = null, string externalSystemEntityKey = null);

        /// <summary>
        /// Gets the mappings for the practice repository entity for the external system information specified (or for all).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="practiceRepositoryEntityKeys">The practice repository entity keys.</param>
        /// <param name="externalSystemName">Name of the external system.</param>
        /// <param name="externalSystemEntityName">Name of the external system entity.</param>
        /// <param name="externalSystemEntityKey">The external system entity key.</param>
        /// <returns></returns>
        IEnumerable<ExternalSystemEntityMapping> GetMappings<T>(IEnumerable<string> practiceRepositoryEntityKeys, string externalSystemName = null, string externalSystemEntityName = null, string externalSystemEntityKey = null);

        /// <summary>
        /// Gets the mappings for the practice repository entity for the external system information specified (or for all).
        /// </summary>
        /// <param name="practiceRepositoryEntityId">The practice repository entity id.</param>
        /// <param name="practiceRepositoryEntityKeys">The practice repository entity keys.</param>
        /// <param name="externalSystemName">Name of the external system.</param>
        /// <param name="externalSystemEntityName">Name of the external system entity.</param>
        /// <param name="externalSystemEntityKey">The external system entity key.</param>
        /// <returns></returns>
        IEnumerable<ExternalSystemEntityMapping> GetMappings(int practiceRepositoryEntityId, IEnumerable<string> practiceRepositoryEntityKeys, string externalSystemName = null, string externalSystemEntityName = null, string externalSystemEntityKey = null);

        /// <summary>
        ///   Gets the mapping for the external system information. Throws an exception if specified and not found, otherwise creates new.
        /// </summary>
        /// <param name="entityType"> Type of the entity. </param>
        /// <param name="externalSystemName"> Name of the external system. </param>
        /// <param name="practiceRepositoryEntityId"> PracticeRepositoryEntityId </param>
        /// <param name="externalSystemEntityKey"> The external system entity key. </param>
        /// <param name="exceptionOnNotFound"> if set to <c>true</c> [exception on not found]. </param>
        /// <returns> </returns>
        ExternalSystemEntityMapping GetMapping(Type entityType, string externalSystemName, PracticeRepositoryEntityId practiceRepositoryEntityId, string externalSystemEntityKey, bool exceptionOnNotFound = false);

        /// <summary>
        ///   Gets the mapped entity for the specified mapping from the practice repository or the model repository.
        /// </summary>
        /// <param name="mapping"> The mapping. </param>
        /// <returns> </returns>
        object GetMappedEntity(ExternalSystemEntityMapping mapping);

        /// <summary>
        ///   Ensures the external system has is registerd for the specified message type. Registers it if not registered already.
        /// </summary>
        /// <param name="externalSystemName"> Name of the external system. </param>
        /// <param name="externalSystemMessageType"> Name of the external system message type. </param>
        /// <returns> </returns>
        ExternalSystemExternalSystemMessageType EnsureExternalSystemMessageType(string externalSystemName, ExternalSystemMessageTypeId externalSystemMessageType);

        /// <summary>
        ///   Ensures the external system entity exists. Creates it if it does not.
        /// </summary>
        /// <param name="externalSystemName"> Name of the external system. </param>
        /// <param name="externalSystemEntityName"> Name of the external system entity. </param>
        ExternalSystemEntity EnsureExternalSystemEntity(string externalSystemName, string externalSystemEntityName);

        /// <summary>
        ///   Ensures the external system entity exists. Creates it if it does not.
        /// </summary>
        /// <param name="externalSystemName"> Name of the external system. </param>
        /// <param name="externalSystemEntityName"> Name of the external system entity. </param>
        /// <param name="practiceRepositoryEntityId"> PracticeRepositoryEntityId. </param>
        ExternalSystemEntity EnsureExternalSystemEntity(string externalSystemName, string externalSystemEntityName, PracticeRepositoryEntityId practiceRepositoryEntityId);

        /// <summary>
        ///   Creates a message with the specified parameters. Does not save it.
        /// </summary>
        /// <param name="practiceRepositoryEntityKeys"> The practice repository entity keys. </param>
        /// <param name="externalSystemName"> Name of the external system. </param>
        /// <param name="externalSystemMessageType"> Type of the external system message. </param>
        /// <param name="templateFile"> The template file. </param>
        /// <returns> </returns>
        ExternalSystemMessage CreateMessage(IDictionary<PracticeRepositoryEntityId, IEnumerable<object>> practiceRepositoryEntityKeys, string externalSystemName, ExternalSystemMessageTypeId externalSystemMessageType, string templateFile);

        /// <summary>
        ///   Gets the ExternalSystemMessagePracticeRepositoryEntity with the specified ExternalMessageId and PracticeRepositoryEntityId.
        /// </summary>
        /// <param name="externalMessageId"> The identifier of the ExternalMessage associated with the returned ExternalSystemMessagePracticeRepositoryEntity. </param>
        /// <param name="entityId"> The identifier of the type of PracticeRepositoryEntity we wish to return. </param>
        /// <returns> Returns the ExternalSystemMessagePracticeRepositoryEntity if one exists. </returns>
        ExternalSystemMessagePracticeRepositoryEntity GetExternalSystemMessagePracticeRepositoryEntity(Guid externalMessageId, PracticeRepositoryEntityId entityId);
    }

    internal class ExternalSystemService : IExternalSystemService
    {
        private static readonly CacheHoldInterval MappingsCacheLifetime = CacheHoldInterval.Custom(TimeSpan.FromMinutes(20));

        private readonly IPracticeRepository _practiceRepository;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        private readonly IEntityLinkService _entityLinkService;

        public ExternalSystemService(IPracticeRepository practiceRepository, IUnitOfWorkProvider unitOfWorkProvider, IEntityLinkService entityLinkService)
        {
            _practiceRepository = practiceRepository;
            _unitOfWorkProvider = unitOfWorkProvider;
            _entityLinkService = entityLinkService;
        }

        #region IExternalSystemService Members

        public virtual IEnumerable<ExternalSystemEntityMapping> GetAllMappings(string practiceRepositoryEntityName, string externalSystemName = null, string externalSystemEntityName = null)
        {
            var whereClause = new Expression<Func<ExternalSystemEntityMapping, bool>>[]
                {
                    i => i.PracticeRepositoryEntity.Name == practiceRepositoryEntityName,
                    i => externalSystemName.IsNullOrEmpty() || i.ExternalSystemEntity.ExternalSystem.Name == externalSystemName,
                    i => externalSystemEntityName.IsNullOrEmpty() || i.ExternalSystemEntity.Name == externalSystemEntityName
                }.Aggregate((current, next) => current.And(next));

            var result = _practiceRepository.ExternalSystemEntityMappings
                .Where(whereClause)
                .ToArray();
            return result;
        }

        public virtual IEnumerable<ExternalSystemEntityMapping> GetMappings(IEnumerable entities, string externalSystemName = null, string externalSystemEntityName = null, string externalSystemEntityKey = null)
        {
            entities = entities.OfType<object>().ToArray();

            if (!entities.OfType<object>().WhereNotDefault().Any())
            {
                return new ExternalSystemEntityMapping[0];
            }

            return entities.OfType<object>().GroupBy(e => e.GetType()).Select(g =>
                                                             {
                                                                 Type entityType = g.Key;
                                                                 while (entityType.BaseType != null && entityType.BaseType != typeof(object) && !entityType.BaseType.Is<ValueType>())
                                                                 {
                                                                     entityType = entityType.BaseType;
                                                                 }
                                                                 if (entityType == null || entityType.Namespace != "IO.Practiceware.Model")
                                                                 {
                                                                     throw new InvalidOperationException("Entity type {0} is not valid for mapping.".FormatWith(entityType.Namespace));
                                                                 }
                                                                 PracticeRepositoryEntityId practiceRepositoryEntity = Enums.GetValues<PracticeRepositoryEntityId>().Single(i => i.ToString() == entityType.Name);

                                                                 IEnumerable<string> keys;
                                                                 if (entityType.Is<Enum>())
                                                                 {
                                                                     keys = g.Select(i => ((int)i).ToString()).ToArray();
                                                                 }
                                                                 else
                                                                 {
                                                                     // must keys not supported
                                                                     var getKey = entityType.GetProperties().Single(p => p.HasAttribute<KeyAttribute>()).GetGetter();
                                                                     keys = g.Select(getKey).Select(i => i.ToString()).ToArray();
                                                                 }

                                                                 return GetMappings((int)practiceRepositoryEntity, keys, externalSystemName, externalSystemEntityName, externalSystemEntityKey);
                                                             })

                .SelectMany(g => g).ToArray();
        }

        public virtual IEnumerable<ExternalSystemEntityMapping> GetMappings<T>(IEnumerable<string> practiceRepositoryEntityKeys, string externalSystemName = null, string externalSystemEntityName = null, string externalSystemEntityKey = null)
        {
            var entityName = typeof(T).Name;

            if (typeof(T).Namespace != "IO.Practiceware.Model") throw new InvalidOperationException("Type {0} is not a valid type for mapping.".FormatWith(entityName));

            var tenantKey = ContextData.Keys.Contains(ExternalSystemServices.CustomImplicitCachingClientKeyContextDataKey)
                ? (string)ContextData.Get(ExternalSystemServices.CustomImplicitCachingClientKeyContextDataKey)
                : ApplicationContext.Current.ClientKey;

            var entityId = this.ExecuteCached(i => i.GetEntityId(tenantKey, entityName), CacheHoldInterval.Long);

            return GetMappings(entityId, practiceRepositoryEntityKeys, externalSystemName, externalSystemEntityName, externalSystemEntityKey);
        }

        public virtual IEnumerable<ExternalSystemEntityMapping> GetMappings(int practiceRepositoryEntityId, IEnumerable<string> practiceRepositoryEntityKeys, string externalSystemName = null, string externalSystemEntityName = null, string externalSystemEntityKey = null)
        {
            var tenantKey = ContextData.Keys.Contains(ExternalSystemServices.CustomImplicitCachingClientKeyContextDataKey) 
                ? (string)ContextData.Get(ExternalSystemServices.CustomImplicitCachingClientKeyContextDataKey)
                : ApplicationContext.Current.ClientKey;

            var mappingsStorage = !string.IsNullOrEmpty(externalSystemName) && !string.IsNullOrEmpty(tenantKey)
                ? this.ExecuteCached(i => i.GetMappingsStorage(tenantKey, practiceRepositoryEntityId, externalSystemName), MappingsCacheLifetime)
                // Don't use caching if external system name wasn't provided
                : null;

            var mappingsBag = new ConcurrentBag<ExternalSystemEntityMapping>();
            var missingKeysInStorage = new List<string>();

            // Evaluate missing keys
            if (mappingsStorage != null)
            {
                foreach (var entityKey in practiceRepositoryEntityKeys)
                {
                    if (mappingsStorage.ContainsKey(entityKey))
                    {
                        mappingsBag.Add(mappingsStorage[entityKey]);
                    }
                    else
                    {
                        missingKeysInStorage.Add(entityKey);
                    }
                }
            }
            else
            {
                missingKeysInStorage.AddRange(practiceRepositoryEntityKeys);
            }


            // Batch to avoid EF throwing "An expression services limit has been reached. Please look for potentially complex expressions in your query, and try to simplify them" exception
            missingKeysInStorage.Batch(5000).ForAllInParallel(batch =>
            {
                ExternalSystemEntityMapping[] batchMappings =
                    (from mapping in
                         _practiceRepository.ExternalSystemEntityMappings.Include(i => i.ExternalSystemEntity.ExternalSystem)
                     where
                         (mapping.ExternalSystemEntity.ExternalSystem.Name == externalSystemName || externalSystemName == null) &&
                         mapping.PracticeRepositoryEntityId == practiceRepositoryEntityId && batch.Contains(mapping.PracticeRepositoryEntityKey)
                     select mapping).ToArray();

                foreach (var entityMapping in batchMappings)
                {
                    mappingsBag.Add(entityMapping);

                    // Store resolution result
                    if (mappingsStorage != null)
                    {
                        mappingsStorage.AddOrUpdate(entityMapping.PracticeRepositoryEntityKey, entityMapping, (s, mapping) => entityMapping);
                    }
                }
                batchMappings.ForEach(mappingsBag.Add);
            });

            // Apply additional filtering
            var mappings = mappingsBag
                .Where(m => 
                    (externalSystemEntityName == null || m.ExternalSystemEntity.Name == externalSystemEntityName) 
                    && (externalSystemEntityKey == null || m.ExternalSystemEntityKey == externalSystemEntityKey))
                .ToArray();

            return mappings;
        }

        public virtual ExternalSystemEntityMapping GetMapping(Type entityType, string externalSystemName, PracticeRepositoryEntityId practiceRepositoryEntityId, string externalSystemEntityKey, bool exceptionOnNotFound)
        {
            if (entityType == null || (entityType != typeof(object) && entityType.Namespace != "IO.Practiceware.Model")) throw new InvalidOperationException("Type {0} is not a valid type for mapping.".FormatWith(entityType != null ? entityType.Name : "null"));

            if (externalSystemName.IsNullOrWhiteSpace()) throw new ArgumentNullException("externalSystemName");
            if (externalSystemEntityKey.IsNullOrWhiteSpace()) throw new ArgumentNullException("externalSystemEntityKey");

            using (_unitOfWorkProvider.GetUnitOfWorkScope())
            {
                ExternalSystemEntityMapping mapping = _practiceRepository.ExternalSystemEntityMappings
                    .Include(i => i.ExternalSystemEntity.ExternalSystem)
                    .Include(i => i.PracticeRepositoryEntity).FirstOrDefault(m => m.PracticeRepositoryEntityId ==(int) practiceRepositoryEntityId && m.ExternalSystemEntity.ExternalSystem.Name == externalSystemName.ToLower() && m.ExternalSystemEntityKey == externalSystemEntityKey);

                if (mapping != null) return mapping;

                if (exceptionOnNotFound)
                {
                    throw new InvalidOperationException("Mapping not found for system {0}, practice repository entity {1}, with key {2}.".FormatWith(externalSystemName, practiceRepositoryEntityId.ToString(), externalSystemEntityKey));
                }

                int externalSystemEntityId = _practiceRepository.ExternalSystemEntities.Where(e => e.ExternalSystem.Name.ToLower() == externalSystemName.ToLower() && e.PracticeRepositoryEntityId ==(int) practiceRepositoryEntityId).Select(e => e.Id).FirstOrDefault().EnsureNotDefault("Could not find ExternalSystemEntity {0} for {1}.".FormatWith(practiceRepositoryEntityId, externalSystemName));
                mapping = new ExternalSystemEntityMapping { ExternalSystemEntityKey = externalSystemEntityKey, ExternalSystemEntityId = externalSystemEntityId, PracticeRepositoryEntityId = (int) practiceRepositoryEntityId };

                return mapping;
            }
        }

        public virtual object GetMappedEntity(ExternalSystemEntityMapping mapping)
        {
            return _entityLinkService.GetMappedEntity(mapping);
        }

        public virtual ExternalSystemExternalSystemMessageType EnsureExternalSystemMessageType(string externalSystemName, ExternalSystemMessageTypeId externalSystemMessageType)
        {
            ExternalSystemExternalSystemMessageType externalSystemExternalSystemMessageType;

            ExternalSystem externalSystem = _practiceRepository.ExternalSystems.Include(i => i.ExternalSystemExternalSystemMessageTypes).FirstOrDefault(i => i.Name.ToLower() == externalSystemName.ToLower());
            if (externalSystem == null)
            {
                externalSystem = new ExternalSystem { Name = externalSystemName };

                externalSystemExternalSystemMessageType = new ExternalSystemExternalSystemMessageType { ExternalSystemMessageTypeId = (int)externalSystemMessageType };
                externalSystem.ExternalSystemExternalSystemMessageTypes.Add(externalSystemExternalSystemMessageType);
            }
            else
            {
                externalSystemExternalSystemMessageType = externalSystem.ExternalSystemExternalSystemMessageTypes.FirstOrDefault(i => i.ExternalSystemMessageTypeId == (int)externalSystemMessageType);
                if (externalSystemExternalSystemMessageType == null)
                {
                    externalSystemExternalSystemMessageType = new ExternalSystemExternalSystemMessageType { ExternalSystemMessageTypeId = (int)externalSystemMessageType };
                    externalSystem.ExternalSystemExternalSystemMessageTypes.Add(externalSystemExternalSystemMessageType);
                }
                else if (externalSystemExternalSystemMessageType.IsDisabled)
                {
                    externalSystemExternalSystemMessageType.IsDisabled = false;
                }
            }
            _practiceRepository.Save(externalSystem);

            return externalSystemExternalSystemMessageType;
        }

        public ExternalSystemEntity EnsureExternalSystemEntity(string externalSystemName, string externalSystemEntityName, PracticeRepositoryEntityId practiceRepositoryEntityId)
        {
            ExternalSystemEntity externalSystemEntity = null;

            ExternalSystem externalSystem = _practiceRepository.ExternalSystems.Include(i => i.ExternalSystemEntities).FirstOrDefault(i => i.Name == externalSystemName) ?? new ExternalSystem { Name = externalSystemName };

            if (externalSystemEntityName != null && externalSystem.ExternalSystemEntities.All(i => i.Name != externalSystemEntityName))
            {
                externalSystemEntity = new ExternalSystemEntity { Name = externalSystemEntityName, PracticeRepositoryEntityId = (int) practiceRepositoryEntityId };
                externalSystem.ExternalSystemEntities.Add(externalSystemEntity);
            }

            _practiceRepository.Save(externalSystem);

            return externalSystemEntity;
        }

        public ExternalSystemEntity EnsureExternalSystemEntity(string externalSystemName, string externalSystemEntityName)
        {
            ExternalSystemEntity externalSystemEntity = null;

            ExternalSystem externalSystem = _practiceRepository.ExternalSystems.Include(i => i.ExternalSystemEntities).FirstOrDefault(i => i.Name == externalSystemName) ?? new ExternalSystem { Name = externalSystemName };

            if (externalSystemEntityName != null && externalSystem.ExternalSystemEntities.All(i => i.Name != externalSystemEntityName))
            {
                externalSystemEntity = new ExternalSystemEntity { Name = externalSystemEntityName };
                externalSystem.ExternalSystemEntities.Add(externalSystemEntity);
            }

            _practiceRepository.Save(externalSystem);

            return externalSystemEntity;
        }

        public ExternalSystemMessage CreateMessage(IDictionary<PracticeRepositoryEntityId, IEnumerable<object>> practiceRepositoryEntityKeys, string externalSystemName, ExternalSystemMessageTypeId externalSystemMessageType, string templateFile = null)
        {
            return new ExternalSystemMessage
                {
                    ExternalSystemMessagePracticeRepositoryEntities =
                        practiceRepositoryEntityKeys.SelectMany(i => i.Value.Select(j =>
                                                                                    new ExternalSystemMessagePracticeRepositoryEntity
                                                                                        {
                                                                                            PracticeRepositoryEntityId = (int)i.Key,
                                                                                            PracticeRepositoryEntityKey = j.ToString()
                                                                                        })).ToList(),
                    ExternalSystemExternalSystemMessageTypeId = GetExternalSystemExternalSystemMessageType(externalSystemName, externalSystemMessageType, templateFile),
                };
        }

        public ExternalSystemMessagePracticeRepositoryEntity GetExternalSystemMessagePracticeRepositoryEntity(Guid externalMessageId, PracticeRepositoryEntityId entityId)
        {
            return _practiceRepository.ExternalSystemMessagePracticeRepositoryEntities.FirstOrDefault(e => e.ExternalSystemMessageId == externalMessageId &&
                                                                                                           e.PracticeRepositoryEntityId == (int)entityId);
        }

        #endregion

        private int GetExternalSystemExternalSystemMessageType(string externalSystemName, ExternalSystemMessageTypeId externalSystemMessageType, string templateFile = null)
        {
            if (templateFile == null)
            {
                return _practiceRepository.ExternalSystemExternalSystemMessageTypes.Where(i =>
                                                                                          i.ExternalSystem.Name.ToLower() == externalSystemName
                                                                                          && i.ExternalSystemMessageTypeId == (int)externalSystemMessageType
                                                                                          && i.TemplateFile == null).Select(i => i.Id).FirstOrDefault();
            }
            return _practiceRepository.ExternalSystemExternalSystemMessageTypes.Where(i =>
                                                                                      i.ExternalSystem.Name.ToLower() == externalSystemName
                                                                                      && i.ExternalSystemMessageTypeId == (int)externalSystemMessageType
                                                                                      && i.TemplateFile.ToLower() == templateFile.ToLower()).Select(i => i.Id).FirstOrDefault();
        }

        /// <summary>
        /// Returns empty dictionary of mapping resolutions. Use with caching
        /// </summary>
        /// <param name="tenantKey">Client key (to properly support multi-tenancy).</param>
        /// <param name="practiceRepositoryEntityId">The practice repository entity identifier.</param>
        /// <param name="externalSystemName">Name of the external system.</param>
        /// <returns></returns>
// ReSharper disable UnusedParameter.Local
        private ConcurrentDictionary<string, ExternalSystemEntityMapping> GetMappingsStorage(string tenantKey, int practiceRepositoryEntityId, string externalSystemName)
// ReSharper enable UnusedParameter.Local
        {
            return new ConcurrentDictionary<string, ExternalSystemEntityMapping>();
        }

        /// <summary>
        /// Gets PracticeRepositoryEntityId by entity name.
        /// </summary>
        /// <param name="tenantKey">Client key (to properly support multi-tenancy).</param>
        /// <param name="entityName">Name of the entity.</param>
        /// <returns></returns>
// ReSharper disable once UnusedParameter.Local
        private int GetEntityId(string tenantKey, string entityName)
        {
            int entityId = _practiceRepository.PracticeRepositoryEntities.Where(i => i.Name == entityName).Select(i => i.Id).FirstOrDefault();
            entityId.EnsureNotDefault(new InvalidOperationException("Could not find entity {0}.".FormatWith(entityName)));
            return entityId;
        }
    }

    /// <summary>
    /// </summary>
    public static class ExternalSystemServices
    {
        /// <summary>
        /// Provides custom implicit caching key of external entity resolution services: external system service and cda service
        /// </summary>
        public const string CustomImplicitCachingClientKeyContextDataKey = "__ExternalSystemService_CustomImplicitCachingClientKey__";

        /// <summary>
        ///   Gets the mapping.
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="externalSystemService"> The external system service. </param>
        /// <param name="externalSystemName"> Name of the external system. </param>
        /// <param name="practiceRepositoryEntityId"> PracticeRepositoryEntityId </param>
        /// <param name="externalSystemEntityKey"> The external system entity key. </param>
        /// <param name="exceptionOnNotFound"> if set to <c>true</c> [exception on not found]. </param>
        /// <returns> </returns>
        public static ExternalSystemEntityMapping GetMapping<T>(this IExternalSystemService externalSystemService, string externalSystemName, PracticeRepositoryEntityId practiceRepositoryEntityId, string externalSystemEntityKey, bool exceptionOnNotFound = false)
        {
            return externalSystemService.GetMapping(typeof(T), externalSystemName, practiceRepositoryEntityId, externalSystemEntityKey, exceptionOnNotFound);
        }

        /// <summary>
        ///   Gets the mapping for the external system information. Throws an exception if specified and not found, otherwise creates new.
        /// </summary>
        /// <param name="externalSystemService"> The external system repository. </param>
        /// <param name="externalSystemName"> Name of the external system. </param>
        /// <param name="practiceRepositoryEntityId"> PracticeRepositoryEntityId. </param>
        /// <param name="externalSystemEntityKey"> The external system entity key. </param>
        /// <param name="exceptionOnNotFound"> if set to <c>true</c> [exception on not found]. </param>
        /// <returns> </returns>
        public static ExternalSystemEntityMapping GetMapping(this IExternalSystemService externalSystemService, string externalSystemName, PracticeRepositoryEntityId practiceRepositoryEntityId, string externalSystemEntityKey, bool exceptionOnNotFound = false)
        {
            return externalSystemService.GetMapping<object>(externalSystemName, practiceRepositoryEntityId, externalSystemEntityKey, exceptionOnNotFound);
        }


        /// <summary>
        /// Gets the mappings for the practice repository entity for the external system information specified (or for all).
        /// </summary>
        /// <param name="externalSystemService">The external system service.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="externalSystemName">Name of the external system.</param>
        /// <param name="externalSystemEntityName">Name of the external system entity.</param>
        /// <param name="externalSystemEntityKey">The external system entity key.</param>
        /// <returns></returns>
        public static IEnumerable<ExternalSystemEntityMapping> GetMappings(this IExternalSystemService externalSystemService, object entity, string externalSystemName = null, string externalSystemEntityName = null, string externalSystemEntityKey = null)
        {
            return externalSystemService.GetMappings(new[] { entity }, externalSystemName, externalSystemEntityName, externalSystemEntityKey);
        }

        /// <summary>
        /// Gets the mappings for the practice repository entity for the external system information specified (or for all).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="externalSystemService">The external system service.</param>
        /// <param name="practiceRepositoryEntityKey">The practice repository entity key.</param>
        /// <param name="externalSystemName">Name of the external system.</param>
        /// <param name="externalSystemEntityName">Name of the external system entity.</param>
        /// <param name="externalSystemEntityKey">The external system entity key.</param>
        /// <returns></returns>
        public static IEnumerable<ExternalSystemEntityMapping> GetMappings<T>(this IExternalSystemService externalSystemService, string practiceRepositoryEntityKey, string externalSystemName = null, string externalSystemEntityName = null, string externalSystemEntityKey = null)
        {
            return externalSystemService.GetMappings<T>(new[] { practiceRepositoryEntityKey }, externalSystemName, externalSystemEntityName, externalSystemEntityKey);
        }

        /// <summary>
        /// Gets the mappings for the practice repository entity for the external system information specified (or for all).
        /// </summary>
        /// <param name="externalSystemService">The external system service.</param>
        /// <param name="practiceRepositoryEntityId">The practice repository entity id.</param>
        /// <param name="practiceRepositoryEntityKey">The practice repository entity key.</param>
        /// <param name="externalSystemName">Name of the external system.</param>
        /// <param name="externalSystemEntityName">Name of the external system entity.</param>
        /// <param name="externalSystemEntityKey">The external system entity key.</param>
        /// <returns></returns>
        public static IEnumerable<ExternalSystemEntityMapping> GetMappings(this IExternalSystemService externalSystemService, int practiceRepositoryEntityId, string practiceRepositoryEntityKey, string externalSystemName = null, string externalSystemEntityName = null, string externalSystemEntityKey = null)
        {
            return externalSystemService.GetMappings(practiceRepositoryEntityId, new[] { practiceRepositoryEntityKey }, externalSystemName, externalSystemEntityName, externalSystemEntityKey);
        }

    }
}