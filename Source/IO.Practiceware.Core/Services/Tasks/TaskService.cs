﻿using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Tasks;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;

[assembly: Component(typeof(TaskService), typeof(ITaskService))]

namespace IO.Practiceware.Services.Tasks
{
    /// <summary>
    ///   Operations for interacting with tasks.
    /// </summary>
    public interface ITaskService
    {
        void DeactivateTaskAlarm(TaskActivity taskActivity);
        void CompleteTask(TaskActivity taskActivity);
        TaskActivity AssignTask(AssignTaskArguments arguments);
    }

    internal class TaskService : ITaskService
    {
        private readonly IPracticeRepository _practiceRepository;

        public TaskService(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
        }

        #region ITaskService Members

        public void DeactivateTaskAlarm(TaskActivity taskActivity)
        {
            var taskActivityUser = taskActivity.TaskActivityUsers.First(tau => tau.AssignedToUserId == UserContext.Current.UserDetails.Id);
            taskActivityUser.AlertActive = false;
            taskActivityUser.AlertDeactivatedDateTime = DateTime.Now.ToClientTime();
        }

        public void CompleteTask(TaskActivity taskActivity)
        {
            // complete the task activity
            CompleteTaskActivityUsers(!taskActivity.AssignedIndividually ?
                                                                             taskActivity.TaskActivityUsers :
                                                                                                                taskActivity.TaskActivityUsers.Where(tar => tar.AssignedToUserId == UserContext.Current.UserDetails.Id));
        }

        public TaskActivity AssignTask(AssignTaskArguments arguments)
        {
            if ((arguments.Task == null))
            {
                // creating a new task, so create a new TaskEntity
                arguments.Task = new Task();
            }
            if ((arguments.Task.PatientId == null))
            {
                // Don't allow overwrite of an existing patient associated with the task
                arguments.Task.PatientId = arguments.PatientId;
            }

            // update the task title
            arguments.Task.Title = arguments.Title;

            var userIds = new List<int>();
            if ((!arguments.AssignToUserIds.IsNullOrEmpty()))
            {
                // add individual resources
                userIds.AddRange(arguments.AssignToUserIds);
            }
            if ((!arguments.AssignToGroupIds.IsNullOrEmpty()))
            {
                // add group resources
                var users = _practiceRepository.Users.Where(r => !r.IsArchived && r.GroupUsers.Any(gr => arguments.AssignToGroupIds.Contains(gr.GroupId))).ToArray();
                userIds.AddRange(users.Select(i => i.Id));
            }

            // don't dupe resource IDs
            userIds = userIds.Distinct().ToList();

            // create a new task activity
            var newTaskActivity = new TaskActivity
                                      {
                                          Task = arguments.Task,
                                          Title = arguments.Title,
                                          TaskActivityTypeId = arguments.TaskActivityTypeId,
                                          Description = arguments.Description,
                                          AssignedIndividually = arguments.AssignIndivudally,
                                          CreatedByUserId = arguments.AssignedBy,
                                          CreatedDateTime = DateTime.Now.ToClientTime()
                                      };

            foreach (var resourceId in userIds)
            {
                // add a new task activity resource 
                var newTaskActivityUser = new TaskActivityUser
                                              {
                                                  AssignedToUserId = resourceId,
                                                  Active = true,
                                                  AlertActive = arguments.Alert
                                              };
                newTaskActivity.TaskActivityUsers.Add(newTaskActivityUser);
            }

            return newTaskActivity;
        }

        #endregion

        private static void CompleteTaskActivityUsers(IEnumerable<TaskActivityUser> taskActivityResources)
        {
            var completedDateTime = DateTime.Now.ToClientTime();

            foreach (var taskActivityResource in taskActivityResources)
            {
                taskActivityResource.Active = false;
                taskActivityResource.CompletedDateTime = completedDateTime;

                if ((taskActivityResource.AlertActive))
                {
                    taskActivityResource.AlertActive = false;
                    taskActivityResource.AlertDeactivatedDateTime = completedDateTime;
                }
            }
        }
    }

    public class AssignTaskArguments
    {
        public bool Alert;
        public bool AssignIndivudally;
        public IEnumerable<int> AssignToGroupIds;
        public IEnumerable<int> AssignToUserIds;
    public int AssignedBy = UserContext.Current.UserDetails.Id;
        public string Description;
        public int? PatientId;
        public Task Task;
        public int TaskActivityTypeId;
        public string Title;
    }
}