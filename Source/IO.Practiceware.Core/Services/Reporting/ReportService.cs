﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security;
using System.Text;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Reporting;
using IO.Practiceware.Services.Reporting;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Reflection;
using Soaf.Security;
using Telerik.Reporting;
using Telerik.Reporting.XmlSerialization;

[assembly: Component(typeof (ReportService), typeof (IReportService))]

namespace IO.Practiceware.Services.Reporting
{
    /// <summary>
    ///     A model for a report.
    /// </summary>
    [DataContract]
    public class Report
    {
        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        [DataMember]
        public virtual string Name { get; set; }

        /// <summary>
        ///     Gets or sets the group name.
        /// </summary>
        /// <value>
        ///     The group name.
        /// </value>
        [DataMember]
        public virtual string Category { get; set; }

        /// <summary>
        ///     Gets or sets the documentation.
        /// </summary>
        /// <value>
        ///     The documentation.
        /// </value>
        [DataMember]
        public virtual string Documentation { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the current user [is permissioned] to view the report.
        /// </summary>
        /// <value>
        ///     <c>true</c> if [is permissioned]; otherwise, <c>false</c>.
        /// </value>
        public bool IsPermissioned { get; set; }
    }

    [DataContract]
    public class ReportGroupViewModel
    {
        [DataMember]
        [Dependency]
        public virtual IEnumerable<Report> Reports { get; set; }

        [DataMember]
        public virtual string GroupName { get; set; }
    }

    [DataContract]
    public abstract class ReportContent
    {
    }

    public class TelerikReportContent : ReportContent
    {
        [DataMember]
        public virtual byte[] Content { get; set; }
    }

    [DataContract]
    public class QueryableReportContent : ReportContent
    {
        [DataMember]
        public virtual string RepositoryType { get; set; }

        [DataMember]
        public virtual string QueryableMemberName { get; set; }
    }

    public interface IReportService
    {
        /// <summary>
        ///     Gets all reports in the repository.
        /// </summary>
        /// <returns> </returns>
        IEnumerable<Report> GetReports();

        /// <summary>
        /// Gets the report for the given report name
        /// </summary>
        /// <param name="reportName"></param>
        /// <returns></returns>
        Report GetReport(string reportName);

        /// <summary>
        ///     Gets binary content of given report name.
        /// </summary>
        /// <param name="reportName"> The Report Name. </param>
        /// <returns> </returns>
        ReportContent GetReportContent(string reportName);
    }

    internal class ReportService : IReportService
    {
        private readonly IPracticeRepository _practiceRepository;

        public ReportService(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
        }

        /// <summary>
        ///     Gets all reports in the repository.
        /// </summary>
        /// <returns> </returns>
        public IEnumerable<Report> GetReports()
        {
            string[] permissions = PrincipalContext.Current.Principal.Identity
                .CastTo<UserIdentity>().IfNotNull(ui => ui.User.Roles.Select(r => r.Name)).ToArray();

            var reports = _practiceRepository.Reports.OrderBy(r=>r.ReportCategory).Include(r => r.Permission).ToList();
            var results = new List<Report>();
            foreach (var report in reports)
            {
                var result = new Report();
                result.Name = report.Name;
                result.Documentation = report.Documentation;
                result.Category = report.ReportCategory.GetDisplayName();
                result.IsPermissioned = report.Permission == null || permissions.Contains(report.Permission.Name);
                results.Add(result);
            }

            return results;
        }

        /// <summary>
        /// Gets report for the given report name
        /// </summary>
        /// <param name="reportName"></param>
        /// <returns></returns>
        public Report GetReport(string reportName)
        {
            Model.Report report = _practiceRepository.Reports.Include(r => r.Permission).FirstOrDefault(r => r.Name == reportName);
            if (report == null) return null;

            string[] permissions = PrincipalContext.Current.Principal.Identity
                .CastTo<UserIdentity>().IfNotNull(ui => ui.User.Roles.Select(r => r.Name)).ToArray();

            var result = new Report();
            result.Name = report.Name;
            result.Documentation = report.Documentation;
            result.Category = report.ReportCategory.GetDisplayName();
            result.IsPermissioned = report.Permission == null || permissions.Contains(report.Permission.Name);
            return result;
        }

        /// <summary>
        ///     Gets binary content of given report name.
        /// </summary>
        /// <param name="reportName">The Report Name.</param>
        /// <returns></returns>
        public ReportContent GetReportContent(string reportName)
        {
            Model.Report report = _practiceRepository.Reports.Include(r => r.Permission).FirstOrDefault(r => r.Name == reportName);

            if (report == null) throw new NotSupportedException("Report {0} not found.".FormatWith(reportName));
            string[] permissions = PrincipalContext.Current.Principal.Identity
                .CastTo<UserIdentity>().IfNotNull(ui => ui.User.Roles.Select(r => r.Name)).ToArray();

            if (report.Permission != null && !permissions.Contains(report.Permission.Name))
                throw new SecurityException("Not permissioned for report {0}.".FormatWith(reportName));

            switch (report.ReportType)
        {
                case ReportType.Trdx:
                    return new TelerikReportContent {Content = FillReport(report.Content)};
                case ReportType.ReportingRepository:
                    return new QueryableReportContent
                           {
                                QueryableMemberName = typeof (IReportingRepository).GetProperties()
                                   .FirstOrDefault(p => p.GetDisplayName() == reportName)
                                   .EnsureNotDefault("Report {0} not found.".FormatWith(reportName)).Name,
                                RepositoryType = typeof (IReportingRepository).FullName
                           };
                                                                    }

            throw new NotSupportedException("Report {0} not found.".FormatWith(reportName));
        }

        private byte[] FillReport(string content)
        {
            var serializer = new ReportXmlSerializer();
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(content)))
            using (var report = (Telerik.Reporting.Report) (serializer.Deserialize(ms)))
            {
                // replace the default command timeout of 30 seconds on the report data source itself and all sub-objects' data sources
                report.Items.Recurse(i => i.Items).OfType<DataItem>().Select(i => i.DataSource).OfType<SqlDataSource>()
                    .Concat(new[] {report.DataSource as SqlDataSource}).WhereNotDefault().Distinct()
                    .Where(ds => ds.CommandTimeout < 240)
                    .ForEachWithSinglePropertyChangedNotification(ds => ds.CommandTimeout = 240);

                SubReport[] subReports = report.Items.Recurse(i => i.Items).OfType<SubReport>().ToArray();
                foreach (SubReport subReport in subReports)
                {
                    subReport.ReportSource = ConvertToInstanceReportSource(subReport.ReportSource) ?? subReport.ReportSource;
                }

                using (var msOut = new MemoryStream())
                {
                    serializer.Serialize(msOut, report);
                    return msOut.ToArray();
                }
            }
        }

        private InstanceReportSource ConvertToInstanceReportSource(ReportSource reportSource)
        {
            var uriReportSource = reportSource as UriReportSource;
            if (uriReportSource == null) return null;

            byte[] content = GetReportContent(Path.GetFileNameWithoutExtension(uriReportSource.Uri)).CastTo<TelerikReportContent>().Content;

            if (content != null)
            {
                using (var subReportMs = new MemoryStream(content))
                {
                    var instanceReportSource = new InstanceReportSource {ReportDocument = (Telerik.Reporting.Report) new ReportXmlSerializer().Deserialize(subReportMs)};
                    instanceReportSource.Parameters.AddRange(uriReportSource.Parameters);
                    return instanceReportSource;
                }
            }
            return null;
        }
    }
    
}