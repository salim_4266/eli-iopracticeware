﻿using System;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using IO.Practiceware.Configuration;
using IO.Practiceware.Services.FileService;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Web.Client;
using System.IO;
using File = System.IO.File;

[assembly: Component(typeof(FileService), typeof(IFileService))]

namespace IO.Practiceware.Services.FileService
{
    /// <summary>
    /// Service methods that provide basic, generic access to a remote or local file system. All methods accept relative, not absolute paths. The IFileService is responsible for determining
    /// the root and absolute path to use.
    /// </summary>
    [RemoteService]
    public interface IFileService
    {
        /// <summary>
        /// Checks if the absolute version of the specified file path exists.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        bool FileExists(string path);

        /// <summary>
        /// Checks if the absolute version of the specified directory path exists.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        bool DirectoryExists(string path);

        /// <summary>
        /// Copies the specified source file (converted to absolute path) to the specified destination file (converted to absolute path).
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The destination path.</param>
        void Copy(string sourcePath, string destinationPath);

        /// <summary>
        /// Moves the specified source file (converted to absolute path) to the specified destination file (converted to absolute path).
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The destination path.</param>
        void Move(string sourcePath, string destinationPath);

        /// <summary>
        /// Deletes the specified file (converted to absolute path).
        /// </summary>
        /// <param name="path">The path.</param>
        void Delete(string path);

        /// <summary>
        /// Writes the specified data to the specified path (converted to absolute path).
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        void Write(WriteFileArguments arguments);

        /// <summary>
        /// Reads the specified path.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <returns></returns>
        ReadFileResult Read(ReadFileArguments arguments);

        /// <summary>
        /// Creates a directory at the given file path (converted to absolute path).
        /// </summary>
        /// <param name="path">The path.</param>
        void CreateDirectory(string path);

        /// <summary>
        /// Deletes directory at given path
        /// </summary>
        /// <param name="directoryPath"></param>
        void DeleteDirectory(string directoryPath);

        /// <summary>
        /// Gets the files.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="allDirectories">if set to <c>true</c> [all directories].</param>
        /// <returns></returns>
        string[] GetFiles(string path, bool allDirectories);

        /// <summary>
        /// Gets the directories.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="allDirectories">if set to <c>true</c> [all directories].</param>
        /// <returns></returns>
        string[] GetDirectories(string path, bool allDirectories);

        /// <summary>
        /// Retrieves file information records
        /// </summary>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        FileInfoRecord[] GetFileInfos(string[] filePaths);

        // All methods should be called with relative paths. 
        // The implementer should define a method to convert these relative paths to absolute paths using Path.Combine(ConfigurationManager.ServerDataPath, path).
        // private string ToAbsolutePath(string path);
    }

    public class FileService : IFileService
    {
        private static readonly CultureInfo InstalledUICulture = CultureInfo.InstalledUICulture;

        public bool FileExists(string path)
        {
            var absolutePath = ToAbsolutePath(path);
            return File.Exists(absolutePath);
        }

        public bool DirectoryExists(string path)
        {
            var absolutePath = ToAbsolutePath(path);
            return Directory.Exists(absolutePath);
        }

        public void Copy(string sourcePath, string destinationPath)
        {
            var sourceAbsolutePath = ToAbsolutePath(sourcePath);
            var destinationAbsolutePath = ToAbsolutePath(destinationPath);
            FileSystem.TryCopyFile(sourceAbsolutePath, destinationAbsolutePath);
        }

        public void Move(string sourcePath, string destinationPath)
        {
            var sourceAbsolutePath = ToAbsolutePath(sourcePath);
            var destinationAbsolutePath = ToAbsolutePath(destinationPath);
            FileSystem.TryMoveFile(sourceAbsolutePath, destinationAbsolutePath);
        }

        public void Delete(string path)
        {
            var absolutePath = ToAbsolutePath(path);
            FileSystem.TryDeleteFile(absolutePath, true);
        }

        public void Write(WriteFileArguments arguments)
        {
            using (arguments)
            {
                var absolutePath = ToAbsolutePath(arguments.Path);
                FileSystem.TryWriteFile(absolutePath, arguments.Data);
            }
        }

        public ReadFileResult Read(ReadFileArguments arguments)
        {
            var absolutePath = ToAbsolutePath(arguments.Path);
            var fileServiceContents = FileSystem.TryReadAsStream(absolutePath, true);

            // Init result
            var result = new ReadFileResult
            {
                UseLocalCopy = false,
                Exists = fileServiceContents != null,
                Data = new MemoryStream()
            };

            // File didn't exist -> return
            if (fileServiceContents == null) return result;

            // Hash matches?
            if (arguments.LocalCopySha1 != null && fileServiceContents
                .ComputeSha1Hash()
                .SequenceEqual(arguments.LocalCopySha1))
            {
                // Dispose stream
                fileServiceContents.Close();

                // Local copy can be used, since contents matches
                result.UseLocalCopy = true;
            }
            else
            {
                // Reset stream position back to beginning
                fileServiceContents.Seek(0, SeekOrigin.Begin);

                // Return file
                result.Data = fileServiceContents;
            }

            return result;
        }

        public void CreateDirectory(string path)
        {
            var absolutePath = ToAbsolutePath(path);
            Directory.CreateDirectory(absolutePath);
        }

        public void DeleteDirectory(string directoryPath)
        {
            var absolutePath = ToAbsolutePath(directoryPath);
            FileSystem.DeleteDirectory(absolutePath);
        }

        public string[] GetFiles(string path, bool allDirectories)
        {
            var absolutePath = ToAbsolutePath(path);
            var result = FileSystem.ListFiles(absolutePath, 
                allDirectories ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = ToRelativePath(result[i]);
            }
            return result;
        }

        public string[] GetDirectories(string path, bool allDirectories)
        {
            var absolutePath = ToAbsolutePath(path);
            var result = FileSystem.ListDirectories(absolutePath,
                allDirectories ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = ToRelativePath(result[i]);
            }
            return result;
        }

        public FileInfoRecord[] GetFileInfos(string[] filePaths)
        {
            var result = filePaths
                .Select(path =>
                {
                    var absolutePath = ToAbsolutePath(path);
                    var fileInfo = FileSystem.GetFileInfo(absolutePath);
                    fileInfo.FullName = ToRelativePath(fileInfo.FullName);
                    return fileInfo;
                })
                .ToArray();
            return result;
        }

        private static string ToAbsolutePath(string path)
        {
            // Nothing provided -> use server data path as current folder
            if (path.IsNullOrEmpty()) return ConfigurationManager.ServerDataPath;

            // Relative path -> use server data path
            var absolutePath = path.StartsWith(@"\", true, InstalledUICulture) && !path.StartsWith(@"\\", true, InstalledUICulture)
                ? Path.Combine(ConfigurationManager.ServerDataPath, path.TrimStart(@"\"))
                : path;

            // Convert short paths to full paths
            return FileSystem.GetFullPath(absolutePath);
        }

        /// <summary>
        /// Constructs relative file service path from absolute path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string ToRelativePath(string path)
        {
            var result = path;
            var serverDataPath = ConfigurationManager.ServerDataPath;
            if (path.StartsWith(serverDataPath, true, InstalledUICulture))
            {
                result = path.Remove(0, serverDataPath.Length);
            }
            return result;
        }
    }

    [MessageContract]
    public class WriteFileArguments : IDisposable
    {
        [MessageHeader(MustUnderstand = true)]
        public string Path { get; set; }

        [MessageBodyMember(Order = 1)]
        public Stream Data { get; set; }

        public void Dispose()
        {
            if (Data != null)
            {
                Data.Close();
                Data = null;
            }
        }
    }

    [MessageContract]
    public class ReadFileArguments
    {
        [MessageBodyMember(Order = 1)]
        public string Path { get; set; }

        [MessageBodyMember(Order = 2)]
        public byte[] LocalCopySha1 { get; set; }
    }

    /// <summary>
    /// Read operation result
    /// </summary>
    [MessageContract]
    public class ReadFileResult : IDisposable
    {
        /// <summary>
        /// <c>True</c> if server file hash matches local copy provided hash
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public bool UseLocalCopy { get; set; }

        /// <summary>
        /// <c>True</c> if file exists on server
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public bool Exists { get; set; }

        /// <summary>
        /// File contents or <c>null</c>, if UseLocalCopy = True
        /// </summary>
        [MessageBodyMember(Order = 1)]
        public Stream Data { get; set; }

        public void Dispose()
        {
            if (Data != null)
            {
                Data.Close();
                Data = null;
            }
        }
    }
}
