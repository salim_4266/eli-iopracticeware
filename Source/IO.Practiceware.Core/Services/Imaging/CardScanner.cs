﻿using IO.Practiceware.Storage;
using snNetMedicSdkCOM;
using snNetScanW;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Windows;
using System.Windows.Input;

namespace IO.Practiceware.Services.Imaging
{
    /// <summary>
    ///   A resource for interacting with a card scanner.
    /// </summary>
    public class CardScanner : Freezable
    {
        private class ExternalOcrApiResources
        {
            private static readonly Lazy<ExternalOcrApiResources> GetInstance = new Lazy<ExternalOcrApiResources>(() => new ExternalOcrApiResources(), true);

            private ExternalOcrApiResources()
            {
                try
                {
                    SLibEx = new snNetScanWex.SLibExClass();
                    ImageEx = new snNetScanWex.CImageClass();
                    Activation = new snNetScanWex.CActivateSwClass();
                    IdData = new IdDataClass();
                    Image = new CImageClass();
                    SLib = new SLibExClass();
                    Med = new Med();

                    var activation = Activation.IsActivated();

                    if (activation > 0)
                    {
                        IsTwainActivated = true;
                    }
                }
                catch (Exception ex)
                {
                    var errorMessage = ErrorMessageFormatString.FormatWith("Error initializing card scanner. Please verify the card scanner is installed correctly. /nDetails: {0}".FormatWith(ex));
                    Trace.TraceWarning(errorMessage);
                }
            }


            public void Init()
            {
                if (SLib != null && Image != null && IdData != null && Med != null)
                {
                    const string license = "WCR2UFWR8CYEU8BC";

                    var result = SLib.InitLibrary(license);

                    if ((result < 0 && result != -13 && result != -4) || (result == -4 && !IsTwainActivated))
                    {
                        var errorMessage = ErrorMessageFormatString.FormatWith(GetScannerClassErrorMessage(result));
                        Trace.TraceInformation(errorMessage);
                        return;
                    }

                    if (result != -4)
                    {
                        HasCssnScanner = true;
                    }

                    Image.InitLibrary(license);
                    IdData.InitLibrary(license);
                    Med.InitSdk(license);
                }
            }

            public static ExternalOcrApiResources Instance
            {
                get { return GetInstance.Value; }
            }

            public snNetScanWex.SLibExClass SLibEx { get; private set; }
            public snNetScanWex.CImageClass ImageEx { get; private set; }
            private snNetScanWex.CActivateSwClass Activation { get; set; }
            public IdDataClass IdData { get; private set; }
            public CImageClass Image { get; private set; }
            public SLibExClass SLib { get; private set; }
            public Med Med { get; private set; }

            public bool IsTwainActivated { get; private set; }

            public bool HasCssnScanner { get; private set; }

            public bool CanScan()
            {
                if (SLibEx != null)
                {
                    UnInit();

                    Init();
                }

                return HasCssnScanner || IsTwainActivated;
            }

            public void UnInit()
            {
                if (SLibEx != null)
                {
                    SLibEx.UnInit();
                }
            }
        }

        private const string ErrorMessageFormatString = "OCR Scanner encountered the following error {0}";

        public static readonly DependencyProperty ProcessedDataProperty = DependencyProperty.Register("ProcessedData", typeof(ICollection<CardScannerDataModel>), typeof(CardScanner));

        public static readonly DependencyProperty ScannedImageDataProperty = DependencyProperty.Register("ScannedImageData", typeof(byte[]), typeof(CardScanner));

        public static readonly DependencyProperty ScannerNameProperty = DependencyProperty.Register("ScannerName", typeof(string), typeof(CardScanner), new PropertyMetadata(null));

        public static readonly DependencyProperty CardTypeProperty = DependencyProperty.Register("CardType", typeof(CardType), typeof(CardScanner));

        public static readonly DependencyProperty InteractionContextProperty = DependencyProperty.Register("InteractionContext", typeof(IInteractionContext), typeof(CardScanner));

        public static readonly DependencyProperty CanScanProperty = DependencyProperty.Register("CanScan", typeof(bool), typeof(CardScanner), new PropertyMetadata(false));

        private static readonly IDictionary<string, short> ScannerModels = new Dictionary<string, short>
        {
            {"SnapShell IDR", 13},
            {"ScanShell 600", 1},
            {"ScanShell 800R", 2},
            {"ScanShell 800NR", 3},
            {"ScanShell 1000", 4},
            {"ScanShell 2000R", 5},
            {"ScanShell2000 NR", 6},
            {"ScanShell 800E", 7},
            {"ScanShell 800 EN", 8},
            {"ScanShell 3000", 9},
            {"Fujistu fi60", 10},
            {"Twain", 11},
            {"Bancor", 12},
            {"ScanShell 800DX", 14},
            {"ScanShell 800DXN", 15},
            {"SnapShell FDA", 16},
            {"SnapShell WMD", 17},
            {"SnapShell TWN", 18},
            {"SnapShell Pass.", 19},
            {"RTE 8000", 20},
            {"Twain N", 21},
            {"Magtek STX", 22},
            {"SnapShell Clb.", 23},
            {"ScanShell IP", 24},
            {"ScanShell 1000N", 25},
            {"ScanShell 3000DN", 26},
            {"IC Scan", 27},
            {"AT 9000", 28},
            {"ScanShell 3100", 29},
            {"ScanShell 3100N", 30},
            {"SnapShell R2", 33},
            {"Unknown", 0}
        };

        private readonly ExternalOcrApiResources _apiResources;

        public CardScanner()
        {
            _apiResources = ExternalOcrApiResources.Instance;

            CanScan = _apiResources.CanScan();
            if (CanScan)
            {
                ScannerName = ScannerModels.Where(m => m.Value == _apiResources.SLib.ScannerType).Select(m => m.Key).FirstOrDefault() ?? ScannerModels.OrderBy(m => m.Value).Select(m => m.Key).FirstOrDefault();
                Calibrate = Command.Create(() => _apiResources.SLibEx.CalibrateScannerEx());
                RotateImage = Command.Create(() => _apiResources.Image.RotateImage(string.Empty, 1, 0, string.Empty));

                Scan = Command.Create(ExecuteScan, () => CanScan).Async(() => InteractionContext, true);
                ProcessScan = Command.Create(ExecuteProcessScan).Async(() => InteractionContext, true);
            }
        }

        /// <summary>
        /// Gets a value that indicates whether the card scanner is initialized.
        /// </summary>
        public bool CanScan
        {
            get { return (bool)GetValue(CanScanProperty); }
            set { SetValue(CanScanProperty, value); }
        }

        public IEnumerable<string> ScannerNames
        {
            get
            {
                var apiResources = ExternalOcrApiResources.Instance;
                var scanningService = new ScanningService(null);
                var scannerNames = new List<string>();

                if (apiResources.HasCssnScanner)
                {
                    scannerNames.Add("SnapShell IDR");
                    scannerNames.Add("SnapShell R2");
                }
                if (apiResources.IsTwainActivated)
                {
                    scannerNames.AddRange(scanningService.GetTwainScannerNames());
                }

                return scannerNames;
            }
        }

        /// <summary>
        ///   Gets or sets the processed data parsed once the Process command has been invoked.
        /// </summary>
        /// <value> The processed data. </value>
        public ICollection<CardScannerDataModel> ProcessedData
        {
            get { return (ICollection<CardScannerDataModel>)GetValue(ProcessedDataProperty); }
            set { SetValue(ProcessedDataProperty, value); }
        }

        /// <summary>
        ///   Gets or sets the type of card being scanned.
        /// </summary>
        /// <value> The scanned image. </value>
        public CardType CardType
        {
            get { return (CardType)GetValue(CardTypeProperty); }
            set { SetValue(CardTypeProperty, value); }
        }

        /// <summary>
        ///   Gets or sets the scanned image. Set after Scan has been invoked.
        /// </summary>
        /// <value> The scanned image. </value>
        public byte[] ScannedImageData
        {
            get { return (byte[])GetValue(ScannedImageDataProperty); }
            set { SetValue(ScannedImageDataProperty, value); }
        }

        /// <summary>
        /// Gets or sets the interaction context.
        /// </summary>
        /// <value>
        /// The interaction context.
        /// </value>
        public IInteractionContext InteractionContext
        {
            get { return (IInteractionContext)GetValue(InteractionContextProperty); }
            set { SetValue(InteractionContextProperty, value); }
        }

        /// <summary>
        ///   Scans an image using the card scanner.
        /// </summary>
        /// <value> The scan. </value>
        public static ICommand Scan { get; private set; }

        /// <summary>
        ///   Calibrates the selected scanner.
        /// </summary>
        /// <value> The calibrate. </value>
        public static ICommand Calibrate { get; private set; }

        /// <summary>
        ///   Processed data found in the current ScannedImage.
        /// </summary>
        /// <value> The process. </value>
        public static ICommand ProcessScan { get; private set; }

        /// <summary>
        ///   Rotates the current ScannedImage.
        /// </summary>
        /// <value> The process. </value>
        public static ICommand RotateImage { get; private set; }

        /// <summary>
        ///   Gets or sets the name of the scanner to use.
        /// </summary>
        /// <value> The name of the scanner. </value>
        [DispatcherThread]
        public virtual string ScannerName
        {
            get { return (string)GetValue(ScannerNameProperty); }
            set { SetValue(ScannerNameProperty, value); }
        }

        public void ExecuteScan()
        {
            Dispatcher.Invoke(() =>
            {
                SetScannerName();

                if (_apiResources.SLib.IsScannerValid != 1)
                {
                    var errorMessage = ErrorMessageFormatString.FormatWith("Scanner {0} is not valid.".FormatWith(ScannerName));
                    Trace.TraceWarning(errorMessage);
                    return;
                }

                if (_apiResources.SLib.IsNeedCalibration == 1)
                {
                    _apiResources.SLibEx.CalibrateScannerEx();
                    Dispatcher.Invoke(() => InteractionManager.Current.Alert("Please load card into the tray to scan"));
                }

                _apiResources.SLib.Resolution = 400;
                _apiResources.SLib.ScanHeight = 360;
                _apiResources.SLib.ScanWidth = 220;
                _apiResources.SLibEx.SetCenteredImage = 1;

                int result = _apiResources.SLib.ScanToFile(string.Empty);
                if (result < 0)
                {
                    var errorMessage = ErrorMessageFormatString.FormatWith(GetScannerClassErrorMessage(result));
                    Trace.TraceWarning(errorMessage);
                    return;
                }
                string imageBuffer;
                _apiResources.ImageEx.GetImageBufferData("JPG", out imageBuffer);

                byte[] imageData = imageBuffer.Select(c => (byte)c).ToArray();
                Dispatcher.BeginInvoke(() => ScannedImageData = imageData).Wait();
            });
        }

        private void SetScannerName()
        {
            ExternalOcrApiResources.Instance.UnInit();
            short scannerModelId;
            if (ScannerName != null && ScannerModels.TryGetValue(ScannerName, out scannerModelId))
            {
                ExternalOcrApiResources.Instance.SLibEx.UseFixedModel = scannerModelId;
            }
            else if (ScannerName != null)
            {
                ExternalOcrApiResources.Instance.SLibEx.UseFixedModel = ScannerModels["Twain"];
                ExternalOcrApiResources.Instance.SLibEx.SetTwainScanner(ScannerName);
            }
            else if (ScannerName == null)
            {
                ExternalOcrApiResources.Instance.SLibEx.UseFixedModel = 0;
            }

            ExternalOcrApiResources.Instance.Init();
        }

        private static string GetScannerClassErrorMessage(int value)
        {
            string errorValue = new ResourceManager(typeof(CardScanner).Namespace + ".SlibErrors", Assembly.GetExecutingAssembly()).GetString(value.ToString());
            if (string.IsNullOrEmpty(errorValue))
            {
                return (value.ToString());
            }
            return errorValue;
        }

        private static string GetIdDataClassErrorMessage(int value)
        {
            string errorValue = new ResourceManager(typeof(CardScanner).Namespace + ".IdDataErrors", Assembly.GetExecutingAssembly()).GetString(value.ToString());
            if (string.IsNullOrEmpty(errorValue))
            {
                return (value.ToString());
            }
            return errorValue;
        }

        public void ExecuteProcessScan()
        {
            CardType cardType = default(CardType);
            Dispatcher.Invoke(() => cardType = CardType);
            switch (cardType)
            {
                case CardType.Identification:
                    ProcessIdCard();
                    break;
                case CardType.Insurance:
                    ProcessInsuranceCard();
                    break;
            }
        }

        private void ProcessInsuranceCard()
        {
            var med = ExternalOcrApiResources.Instance.Med;
            int result = med.ProcessMedical(string.Empty, string.Empty, 0);
            if (result < 0)
            {
                var errorMessage = ErrorMessageFormatString.FormatWith("Error {0} occurred processing the insurance card data.".FormatWith(result));
                Trace.TraceWarning(errorMessage);
                return;
            }

            var newProcessedData = new List<CardScannerDataModel>();

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Prefix, med.propNamePrefix));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.FirstName, med.propNameFirst));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.LastName, med.propNameLast));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.MiddleName, med.propNameMiddle));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Suffix, med.propNameSuffix));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.DateOfBirth, med.propDateOfBirth));

            if (med.propAddressTotalItems > 0)
            {
                newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.AddressLine1, med.propAddressStreet[0]));
                newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.City, med.propAddressCity[0]));
                newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.State, med.propAddressState[0]));
                newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PostalCode, med.propAddressZip[0]));
            }

            if (med.propEmailTotalItems > 0)
            {
                newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Email, med.propEmailValue[0]));
            }

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.ContractCode, med.propContractCode));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopayEmergencyRoom, med.propCopayER));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopayOfficeVisit, med.propCopayOV));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopaySpecialist, med.propCopaySP));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopayUrgentCare, med.propCopayUC));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Coverage, med.propCoverage));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Deductible, med.propDeductible));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.EffectiveDate, med.propEffectiveDate));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Employer, med.propEmployer));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.ExpirationDate, med.propExpireDate));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.GroupName, med.propGroupName));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.GroupNumber, med.propGroupNumber));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.IssuerNumber, med.propIssuerNumber));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.MemberId, med.propMemberID));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PayerId, med.propPayerID));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PlanAdministrator, med.propPlanAdmin));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PlanType, med.propPlanType));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PlanProvider, med.propPlanProvider));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxBin, med.propRxBin));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxGroup, med.propRxGroup));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxId, med.propRxId));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxPcn, med.propRxPCN));

            Dispatcher.BeginInvoke(() =>
            {
                ProcessedData = new ExtendedObservableCollection<CardScannerDataModel>(newProcessedData);
            });
        }

        private void ProcessIdCard()
        {
            var idData = _apiResources.IdData;
            int stateId = idData.AutoDetectState(string.Empty);
            if (stateId < 0)
            {
                Dispatcher.BeginInvoke(() => InteractionManager.Current.Alert(GetIdDataClassErrorMessage(stateId)));
                return;
            }

            // Extract data using state ID template
            int result = idData.ProcState(string.Empty, stateId);
            if (result < 0)
            {
                Dispatcher.BeginInvoke(() => InteractionManager.Current.Alert(GetIdDataClassErrorMessage(stateId)));
                return;
            }
            // Set data in properties
            idData.RefreshData();

            var photoPath = FileManager.Instance.GetTempPathName();
            var photoResult = idData.GetFaceImage(string.Empty, photoPath, stateId);

            var newProcessedData = new List<CardScannerDataModel>();
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.FirstName, idData.NameFirst));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.LastName, idData.NameLast));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.MiddleName, idData.NameMiddle));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Suffix, idData.NameSuffix));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Sex, idData.Sex));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.SocialSecurityNumber, idData.SocialSecurity));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.DateOfBirth, idData.DateOfBirth));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.AddressLine1, idData.Address));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.AddressLine2, idData.Address2));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.City, idData.City));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.State, idData.State));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PostalCode, idData.Zip));

            if (photoResult >= 0)
            {
                newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Photo, FileManager.Instance.ReadContents(photoPath)));
            }

            FileManager.Instance.Delete(photoPath);

            Dispatcher.BeginInvoke(() =>
            {
                if (ProcessedData == null) ProcessedData = new ObservableCollection<CardScannerDataModel>();
                ProcessedData.Clear();
                newProcessedData.ForEach(ProcessedData.Add);
            });
        }

        protected override Freezable CreateInstanceCore()
        {
            return this;
        }
    }
}