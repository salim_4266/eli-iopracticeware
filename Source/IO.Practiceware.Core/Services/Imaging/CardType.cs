﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Services.Imaging
{
    /// <summary>
    /// Types of cards that can be scanned.
    /// </summary>
    public enum CardType
    {
        [Display(Name = "ID Card")] Identification,
        [Display(Name = "Insurance Card")] Insurance,
    }
}