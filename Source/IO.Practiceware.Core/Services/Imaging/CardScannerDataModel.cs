﻿//using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Services.Imaging
{
    /// <summary>
    /// Represents a key/value pair of data parsed from the card scanner serice
    /// </summary>
    public class CardScannerDataModel : KeyValueModel<CardScannerDataField, object>
    {
        /// <summary>
        ///   Creates the specified view model
        /// </summary>
        /// <param name="key"> The key. </param>
        /// <param name="value"> The value. </param>
        /// <returns> </returns>
        public static CardScannerDataModel Create(CardScannerDataField key, object value)
        {
            return new CardScannerDataModel {Key = key, Value = value};
        }
    }
}
