﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Services.Imaging
{
    /// <summary>
    ///   Data fields that can be scanned using a card scanner.
    /// </summary>
    public enum CardScannerDataField
    {
        [Display(Name = "Prefix")] Prefix,
        [Display(Name = "First Name")] FirstName,
        [Display(Name = "Last Name")] LastName,
        [Display(Name = "Middle Name")] MiddleName,
        [Display(Name = "Suffix")] Suffix,
        [Display(Name = "Date of Birth")] DateOfBirth,
        [Display(Name = "Sex")] Sex,
        [Display(Name = "SSN")] SocialSecurityNumber,
        [Display(Name = "Address Line 1")] AddressLine1,
        [Display(Name = "Address Line 2")] AddressLine2,
        [Display(Name = "City")] City,
        [Display(Name = "State")] State,
        [Display(Name = "Postal Code")] PostalCode,
        [Display(Name = "Email")] Email,
        [Display(Name = "Telephone")] Telephone,
        [Display(Name = "Photo")] Photo,
        [Display(Name = "Contract Code")] ContractCode,
        [Display(Name = "Copay ER")] CopayEmergencyRoom,
        [Display(Name = "Copay Office Visit")] CopayOfficeVisit,
        [Display(Name = "Copay Specialist")] CopaySpecialist,
        [Display(Name = "Copay Urgent Care")] CopayUrgentCare,
        [Display(Name = "Coverage")] Coverage,
        [Display(Name = "Deductable")] Deductible,
        [Display(Name = "Effective Date")] EffectiveDate,
        [Display(Name = "Employer")] Employer,
        [Display(Name = "Expiration Date")] ExpirationDate,
        [Display(Name = "Group Name")] GroupName,
        [Display(Name = "Group Number")] GroupNumber,
        [Display(Name = "Issuer Number")] IssuerNumber,
        [Display(Name = "Member ID")] MemberId,
        [Display(Name = "Payer ID")] PayerId,
        [Display(Name = "Plan Admin")] PlanAdministrator,
        [Display(Name = "Plan Type")] PlanType,
        [Display(Name = "Plan Provider")] PlanProvider,
        [Display(Name = "Rx BIN")] RxBin,
        [Display(Name = "Rx Group")] RxGroup,
        [Display(Name = "Rx ID")] RxId,
        [Display(Name = "Rx PCN")] RxPcn
    }

    public static class CardScannerDataFieldExtensions
    {
        public static bool IsAddressField(this CardScannerDataField field)
        {
            return field == CardScannerDataField.AddressLine1 || field == CardScannerDataField.AddressLine2 || field == CardScannerDataField.City
                   || field == CardScannerDataField.State || field == CardScannerDataField.PostalCode;
        }
    }
}