﻿using IO.Practiceware.Configuration;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Imaging;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

[assembly: Component(typeof(PatientImageImporterService), typeof(IPatientImageImporterService))]

namespace IO.Practiceware.Services.Imaging
{
    public class PatientImageImportEventArgs
    {
        public PatientDocument Document { get; set; }

        public bool Ignore { get; set; }

        public bool Cancel { get; set; }
    }

    public class PatientImageImportErrorEventArgs
    {
        public string Message { get; set; }

        public PatientDocument Document { get; set; }

        public bool Cancel { get; set; }
    }

    /// <summary>
    ///   Imports patient images into a file store.
    /// </summary>
    public interface IPatientImageImporterService
    {
        /// <summary>
        ///   Imports all images from the specified reader into the specified destination directory.
        /// </summary>
        /// <param name="source"> The reader. </param>
        /// <param name="destinationDirectory"> The destination path. </param>
        /// <param name="moveFiles"> if set to <c>true</c> [move files]. If false, will copy. </param>
        /// <param name="convertToPdf"> if set to <c>true</c> [convert to PDF]. </param>
        /// <param name="onImport"> The on import action. </param>
        /// <param name="onError"> The on error. </param>
        void Import(IEnumerable<PatientDocument> source, bool moveFiles, bool convertToPdf, string destinationDirectory = null, Action<PatientImageImportEventArgs> onImport = null, Action<PatientImageImportErrorEventArgs> onError = null);
    }

    internal class PatientImageImporterService : IPatientImageImporterService
    {
        private readonly IDocumentGenerationService _documentGenerationService;

        public PatientImageImporterService(IDocumentGenerationService documentGenerationService)
        {
            _documentGenerationService = documentGenerationService;
        }

        #region IPatientImageImporterService Members

        public void Import(IEnumerable<PatientDocument> source, bool moveFiles, bool convertToPdf, string destinationDirectory = null, Action<PatientImageImportEventArgs> onImport = null, Action<PatientImageImportErrorEventArgs> onError = null)
        {
            if (destinationDirectory == null) destinationDirectory = DefaultDestinationDirectory;

            foreach (PatientDocument document in source)
            {
                document.Pages = document.Pages.ToList().AsReadOnly();

                bool isValid = document.Pages.Count > 0 && document.Pages.OfType<PatientDocumentFilePage>().Select(f => f.FilePath).All(FileManager.Instance.FileExists);

                if (!isValid)
                {
                    var errorArgs = new PatientImageImportErrorEventArgs { Document = document, Message = "File not found for {0}: {1}".FormatWith(document, document.Pages.OfType<PatientDocumentFilePage>().Select(f => f.FilePath).Join(",")) };
                    if (onError != null) onError(errorArgs);
                    if (errorArgs.Cancel) break;
                    continue;
                }

                var args = new PatientImageImportEventArgs { Document = document };

                try
                {
                    if (onImport != null) onImport(args);

                    if (args.Cancel) break;
                    if (args.Ignore) continue;

                    isValid = document.Pages.Count > 0 && document.Pages.OfType<PatientDocumentFilePage>().Select(f => f.FilePath).All(FileManager.Instance.FileExists);

                    if (!isValid) continue;

                    if (convertToPdf && !document.Pages.OfType<PatientDocumentFilePage>().Any(p => ".pdf".Equals(Path.GetExtension(p.FilePath), StringComparison.OrdinalIgnoreCase)))
                    {
                        ConvertToPdf(document, destinationDirectory, moveFiles);
                    }
                    else
                    {
                        MoveOrCopyFiles(document, destinationDirectory, moveFiles);
                    }
                }
                catch (Exception ex)
                {
                    if (onError != null) onError(new PatientImageImportErrorEventArgs { Document = args.Document, Message = ex.Message });
                    Trace.TraceError(ex.ToString());
                }
            }
        }

        #endregion

        private static string DefaultDestinationDirectory
        {
            get { return Path.Combine(ConfigurationManager.ServerDataPath, "MyScan"); }
        }

        private static void MoveOrCopyFiles(PatientDocument document, string destinationDirectory, bool moveFiles)
        {
            string destinationPath = GetPath(document, document.FileExtension);
            if (destinationDirectory.IsNotNullOrEmpty()) destinationPath = Path.Combine(destinationDirectory, destinationPath);

            foreach (PatientDocumentPage p in document.Pages)
            {
                FileInfoRecord sourceFile = null;

                if (p is PatientDocumentFilePage)
                {
                    sourceFile = FileManager.Instance.GetFileInfo(((PatientDocumentFilePage)p).FilePath);
                }

                var sourceFileSize = sourceFile != null && sourceFile.Exists
                    ? sourceFile.Size 
                    : p.As<PatientDocumentBinaryPage>().IfNotNull(bp => bp.Data.Length);

                var destinationFile = GetAvailableFilePath(destinationPath, document.FileExtension, sourceFileSize);

                if (sourceFile != null)
                {
                    if (moveFiles)
                    {
                        FileManager.Instance.Move(sourceFile.FullName, destinationFile.FullName);
                    }
                    else if (!destinationFile.Exists 
                        || sourceFile.Size != destinationFile.Size 
                        || sourceFile.LastWriteTimeUtc != destinationFile.LastWriteTimeUtc)
                    {
                        FileManager.Instance.Copy(sourceFile.FullName, destinationFile.FullName);
                    }
                }
                else if (p is PatientDocumentBinaryPage)
                {
                    FileManager.Instance.CommitContents(destinationFile.FullName, 
                        ((PatientDocumentBinaryPage)p).Data);
                }
            }
        }

        private void ConvertToPdf(PatientDocument document, string destinationDirectory, bool delete)
        {
            try
            {
                byte[] pdf = _documentGenerationService.ConvertImagesToPdf(PageSize.Letter.Size(), document.Pages.Select(p => p is PatientDocumentFilePage ? FileManager.Instance.ReadContents(((PatientDocumentFilePage)p).FilePath) : p.As<PatientDocumentBinaryPage>().IfNotNull(bp => bp.Data)).WhereNotDefault().ToArray());
                string destinationPath = GetPath(document, ".pdf");
                if (destinationDirectory.IsNotNullOrEmpty()) destinationPath = Path.Combine(destinationDirectory, destinationPath);
                destinationPath = GetAvailableFilePath(destinationPath, ".pdf", pdf.LongLength).FullName;

                var fileDestinationDirectory = Path.GetDirectoryName(destinationPath);
                if (fileDestinationDirectory != null && !FileManager.Instance.DirectoryExists(fileDestinationDirectory)) FileManager.Instance.CreateDirectory(fileDestinationDirectory);

                FileManager.Instance.CommitContents(destinationPath, pdf);
                if (delete) document.Pages.OfType<PatientDocumentFilePage>().ForEachWithSinglePropertyChangedNotification(fp => FileManager.Instance.Delete(fp.FilePath));
            }
            catch
            {
                MoveOrCopyFiles(document, destinationDirectory, delete);
            }
        }

        private static FileInfoRecord GetAvailableFilePath(string destinationPath, string fileExtension, long size)
        {
            int i = 2;
            var destinationFile = FileManager.Instance.GetFileInfo(destinationPath);
            while (destinationFile.Exists && size != destinationFile.Size)
            {
                var nextPath = @"{0}\{1}-{2}{3}".FormatWith(
                    Path.GetDirectoryName(destinationPath),
                    Path.GetFileNameWithoutExtension(destinationPath),
                    i, fileExtension);
                destinationFile = FileManager.Instance.GetFileInfo(nextPath);
                i++;
            }
            return destinationFile;
        }

        private static string GetPath(PatientDocument document, string preferredExtension = null)
        {
            return @"{0}\{1}__{0}-{2}{3}".FormatWith(document.PatientId, ReplaceInvalidCharacters(document.FileType), document.DateTime.ToString("yyyyMMdd"), ReplaceInvalidCharacters(preferredExtension ?? Path.GetExtension(document.Pages.OfType<PatientDocumentFilePage>().Select(p => p.FilePath).FirstOrDefault() ?? string.Empty)));
        }

        private static string ReplaceInvalidCharacters(string value)
        {
            Path.GetInvalidFileNameChars().Concat(Path.GetInvalidPathChars()).Concat(new[] { '/', '\\' }).ForEachWithSinglePropertyChangedNotification(c => value = value.Replace(c, '_'));
            return value;
        }
    }
}