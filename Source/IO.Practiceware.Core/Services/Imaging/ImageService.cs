﻿using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Imaging;
using IO.Practiceware.Storage;
using Soaf.ComponentModel;
using System.IO;

[assembly: Component(typeof(ImageService), typeof(IImageService))]

namespace IO.Practiceware.Services.Imaging
{
    /// <summary>
    ///     Services for interacting with images.
    /// </summary>
    public interface IImageService
    {
        /// <summary>
        ///     Gets the patient photo.
        /// </summary>
        /// <param name="patientId"> The patient id. </param>
        /// <returns> </returns>
        byte[] GetPatientPhoto(int patientId);

        /// <summary>
        ///     Saves the patient photo.
        /// </summary>
        /// <param name="patientId"> The patient id. </param>
        /// <param name="data"> The data. </param>
        void SavePatientPhoto(int patientId, byte[] data);
    }

    public class ImageService : IImageService
    {
        private const string PatientPhotoPrefix = "Photo_";

        #region IImageService Members

        public byte[] GetPatientPhoto(int patientId)
        {
            string fileName = GetPatientPhotoFileName(patientId);
            byte[] content = FileManager.Instance.ReadContents(Directories.PatientImages + fileName, true);
            return content;
        }

        public void SavePatientPhoto(int patientId, byte[] data)
        {
            if (data != null)
            {
                FileManager.Instance.CommitContents(Path.Combine(Directories.PatientImages, GetPatientPhotoFileName(patientId)), data);
            }
            else
            {
                FileManager.Instance.Delete(Path.Combine(Directories.PatientImages, GetPatientPhotoFileName(patientId)));
            }
        }

        #endregion

        private static string GetPatientPhotoFileName(int patientId)
        {
            return (PatientPhotoPrefix + patientId + ".jpg").ToLower();
        }
    }
}