﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using CommonWin32;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Imaging;
using NTwain;
using NTwain.Data;
using Soaf;
using Soaf.Caching;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Runtime.InteropServices;
using System.Xml.Serialization;
using WIA;
using DeviceEvent = NTwain.Data.DeviceEvent;

[assembly: Component(typeof(ScanningService), typeof(IScanningService))]

namespace IO.Practiceware.Services.Imaging
{
    public enum ColorSetting
    {
        [Description("Color")]
        Color = 1,
        [Description("Grayscale")]
        Grayscale = 2,
        [Description("Black And White")]
        BlackAndWhite = 4
    }

    public enum DpiSetting
    {
        [Display(Name = "Low")]
        Low = 75,

        [Display(Name = "Medium")]
        Medium = 150,

        [Display(Name = "High")]
        High = 300
    }

    public enum ScanReturnStatus
    {
        [Description("Scan Successful.")]
        Success,
        [Description("Nothing Scanned.")]
        NothingScanned,
        [Description("Failed to convert to PDF.")]
        FailedToConvertToPdf,
        [Description("Scan Failed.")]
        Failed,
        [Description("Scan Cancelled.")]
        Cancelled
    }

    public interface IScanningService
    {
        Tuple<ScanReturnStatus, byte[]> ScanToPdf(ScanSettings scanSettings = null);
        Tuple<ScanReturnStatus, IEnumerable<byte[]>> ScanToJpg(ScanSettings scanSettings = null);

        Tuple<ScanReturnStatus, IEnumerable<byte[]>> Scan(ScanSettings scanSettings = null);

        IEnumerable<String> GetTwainScannerNames();

        bool CanScanUsingTwain();

        bool CanScanUsingWia();

        bool CanScanUsingOcr();

        bool CanScan();

    }

    [SupportsCaching]
    public class ScanningService : IScanningService
    {
        private readonly IDocumentGenerationService _documentGenerationService;

        private readonly Lazy<bool> _canScanUsingTwain = Lazy.For(() =>
        {
            var session = CreateTwainSession();

            var twainSources = session.GetSources();
            var dataSource = twainSources.FirstOrDefault();

            var scannerExists = (dataSource != null);

            session.Close();

            return scannerExists;
        });

        private readonly Lazy<bool> _canScanUsingWia = Lazy.For(() =>
        {
            var scanner = GetUnmanagedScanner(null);
            ReleaseComObject(scanner);
            return scanner != null;
        });

        private readonly Lazy<bool> _canScanUsingOcr = Lazy.For(() =>
        {
            var cardScanner = new CardScanner();
            return cardScanner.CanScan;
        });

        public ScanningService(IDocumentGenerationService documentGenerationService)
        {
            _documentGenerationService = documentGenerationService;
        }
        #region IScanningService Members

        public virtual bool CanScanUsingTwain()
        {
            return _canScanUsingTwain.Value;
        }

        public virtual bool CanScanUsingWia()
        {
            return _canScanUsingWia.Value;
        }

        public bool CanScanUsingOcr()
        {
            return _canScanUsingOcr.Value;
        }

        public bool CanScan()
        {
            return _canScanUsingTwain.Value || _canScanUsingOcr.Value || _canScanUsingWia.Value;
        }

        public Tuple<ScanReturnStatus, byte[]> ScanToPdf(ScanSettings scanSettings = null)
        {
            if (scanSettings == null)
            {
                scanSettings = new ScanSettings();
            }
            var result = Scan(scanSettings);
            var status = result.Item1;
            var images = result.Item2;
            var binaryImages = images.Select(i => i).ToArray();

            var pdfByteArray = _documentGenerationService.ConvertImagesToPdf(scanSettings.PageSize.Size(), binaryImages).ToArray();
            return new Tuple<ScanReturnStatus, byte[]>(status, pdfByteArray);
        }

        public Tuple<ScanReturnStatus, IEnumerable<byte[]>> ScanToJpg(ScanSettings scanSettings = null)
        {
            if (scanSettings == null)
            {
                scanSettings = new ScanSettings();
            }
            var result = Scan(scanSettings);
            var status = result.Item1;
            var images = result.Item2;
            var binaryImages = images.Select(i => i);

            return new Tuple<ScanReturnStatus, IEnumerable<byte[]>>(status, binaryImages);
        }

        #endregion

        private static void ReleaseComObject(object instance)
        {
            try
            {
                if (instance != null && Marshal.IsComObject(instance))
                {
                    Marshal.ReleaseComObject(instance);
                }
            }
            catch (Exception ex)
            {
                var exception = new Exception("Unable to release com object {0}".FormatWith(instance), ex);
                Trace.TraceError(exception.ToString());
            }
        }

        private static Device GetUnmanagedScanner(string scannerName)
        {
            var deviceManager = new DeviceManager();
            Device scanner = null;

            try
            {
                Trace.TraceInformation(deviceManager.DeviceInfos.Count + " devices found.");
                if (scannerName != null)
                {
                    foreach (DeviceInfo info in deviceManager.DeviceInfos)
                    {
                        if (info.Properties["Name"].get_Value().ToString().StartsWith(scannerName))
                        {
                            scanner = info.Connect();
                            break;
                        }
                    }
                }
                if (scanner == null && deviceManager.DeviceInfos.Count > 0)
                {
                    Trace.TraceInformation("Trying to use first WIA device.");
                    scanner = deviceManager.DeviceInfos[1].Connect();
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                ReleaseComObject(scanner);
                ReleaseComObject(deviceManager);
                scanner = null;
                deviceManager = null;
            }

            if (deviceManager != null)
            {
                ReleaseComObject(deviceManager);
            }

            return scanner;
        }

        private static ImageProcess GetUnmanagedImageProcess(ScanSettings scanSettings)
        {
            var imageProcess = new ImageProcess();
            try
            {
                imageProcess.Filters.Add(imageProcess.FilterInfos["Convert"].FilterID);
                if (scanSettings.ColorSetting == ColorSetting.Color)
                {
                    imageProcess.Filters[1].Properties["FormatID"].set_Value(FormatID.wiaFormatJPEG);
                    imageProcess.Filters[1].Properties["Quality"].set_Value(imageProcess.Filters[1].Properties["Quality"].SubTypeMax);
                }
                else
                {
                    imageProcess.Filters[1].Properties["FormatID"].set_Value(FormatID.wiaFormatPNG);
                }
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not initialize imageProcess", ex);
                Trace.TraceError(exception.ToString());
                return null;
            }
            return imageProcess;
        }

        private static bool InitializeScanner(Device scanner, ScanSettings scanSettings)
        {
            if (scanner == null)
            {
                return false;
            }

            try
            {
                if (scanSettings.DuplexMode)
                {
                    scanner.Properties["Document Handling Select"].set_Value(5);
                }
                Item item = scanner.Items[1];
                item.Properties["6146"].set_Value(scanSettings.ColorSetting);
                item.Properties["6147"].set_Value(scanSettings.Dpi);
                item.Properties["6148"].set_Value(scanSettings.Dpi);
                item.Properties["6149"].set_Value(0);
                item.Properties["6150"].set_Value(0);
                double width = scanSettings.PageSize.Size().Width;
                double height = scanSettings.PageSize.Size().Height;
                try
                {
                    item.Properties["6151"].set_Value(width * (int)scanSettings.Dpi);
                }
                catch
                {
                    item.Properties["6151"].set_Value(item.Properties["6151"].SubTypeMax);
                }
                try
                {
                    item.Properties["6152"].set_Value(height * (int)scanSettings.Dpi);
                }
                catch
                {
                    item.Properties["6152"].set_Value(item.Properties["6152"].SubTypeMax);
                }
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not initialize Scanner", ex);
                Trace.TraceError(exception.ToString());
                return false;
            }
            return true;
        }

        public Tuple<ScanReturnStatus, IEnumerable<byte[]>> Scan(ScanSettings scanSettings)
        {
            if (CanScanUsingTwain() && (scanSettings == null || scanSettings.Mode == null || scanSettings.Mode == ScanMode.Twain))
            {
                var result = ScanUsingTwain(scanSettings);
                return new Tuple<ScanReturnStatus, IEnumerable<byte[]>>(result.Item1, result.Item2);
            }
            if (CanScanUsingWia() && (scanSettings == null || scanSettings.Mode == null || scanSettings.Mode == ScanMode.Wia))
            {
                var result = ScanUsingWia(scanSettings);
                return Tuple.Create(result.Item1, result.Item2.Select(i => i).ToArray().AsEnumerable());
            }
            if (CanScanUsingOcr() && (scanSettings == null || scanSettings.Mode == null || scanSettings.Mode == ScanMode.Ocr))
            {
                var listOfPics = new List<byte[]>();
                var image = ScanUsingOcr(scanSettings);
                if (image != null)
                {
                    listOfPics.Add(image);
                    return Tuple.Create(ScanReturnStatus.Success, listOfPics.AsEnumerable());
                }
            }
            return Tuple.Create(ScanReturnStatus.NothingScanned, Enumerable.Empty<byte[]>());
        }

        private static byte[] ScanUsingOcr(ScanSettings settings)
        {
            var cardScanner = new CardScanner();
            if (settings.SelectedScanner != null)
            {
                cardScanner.ScannerName = settings.SelectedScanner;
            }
            cardScanner.ExecuteScan();
            return cardScanner.ScannedImageData;
        }

        private static Tuple<ScanReturnStatus, IEnumerable<byte[]>> ScanUsingWia(ScanSettings scanSettings)
        {
            var scanner = GetUnmanagedScanner(scanSettings.SelectedScanner);

            Trace.TraceInformation("Instantiating CommonDialog.");
            var commonDialog = new CommonDialog();
            var scannedFiles = new List<byte[]>();
            var imageProcess = GetUnmanagedImageProcess(scanSettings);

            #region helper action - cleanupUnmanagedResourcesExcludeImages

            Action cleanupUnmanagedResourcesExcludeImages = () =>
            {
                ReleaseComObject(scanner);
                ReleaseComObject(imageProcess);
            };

            #endregion

            #region helper action - cleanupAllUnmangedResources

            Action cleanupAllUnmangedResources = () =>
            {
                cleanupUnmanagedResourcesExcludeImages();
                foreach (var imageFile in scannedFiles)
                {
                    ReleaseComObject(imageFile);
                }
            };

            #endregion

            if (imageProcess != null && InitializeScanner(scanner, scanSettings))
            {
                bool morePages = true;
                while (morePages)
                {
                    try
                    {
                        var imageFile = (ImageFile)commonDialog.ShowTransfer(scanner.Items[1], FormatID.wiaFormatJPEG, true);
                        imageFile = imageProcess.Apply(imageFile);
                        byte[] buffer = (byte[])imageFile.FileData.get_BinaryData();
                        using (var ms = new MemoryStream(buffer))
                        using (var image = Image.FromStream(ms))
                        {
                            buffer = image.ToByteArray(ImageFormat.Jpeg, scanSettings.Quality);
                        }
                        scannedFiles.Add(buffer);
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.EndsWith("0x80210003"))
                        {
                            Trace.TraceInformation("Scanned all pages.");
                            morePages = false;
                        }
                        else if (ex.Message.EndsWith("0x80210064"))
                        {
                            Trace.TraceInformation("Scanning cancelled.");
                            cleanupAllUnmangedResources();
                            return Tuple.Create(ScanReturnStatus.Cancelled, new List<byte[]>().AsEnumerable());
                        }
                        else
                        {
                            Trace.TraceError(ex.ToString());
                            cleanupAllUnmangedResources();
                            return Tuple.Create(ScanReturnStatus.Failed, new List<byte[]>().AsEnumerable());
                        }
                    }
                }
            }
            if (scannedFiles.Any())
            {
                cleanupUnmanagedResourcesExcludeImages();
                return Tuple.Create(ScanReturnStatus.Success, scannedFiles.AsEnumerable());
            }

            cleanupAllUnmangedResources();
            return Tuple.Create(ScanReturnStatus.NothingScanned, new List<byte[]>().AsEnumerable());
        }

        private static Tuple<ScanReturnStatus, IEnumerable<byte[]>> ScanUsingTwain(ScanSettings settings)
        {
            TwainSession session = null;
            TwainSource dataSource = null;

            try
            {
                var scannedImages = new List<byte[]>();

                var wait = new ManualResetEvent(false);

                session = CreateTwainSession();

                var sources = session.GetSources();

                dataSource = (settings.SelectedScanner == null) ? null : sources.First(n => n.Name.Equals(settings.SelectedScanner));

                if (dataSource == null)
                    return Tuple.Create(ScanReturnStatus.Failed, new List<byte[]>().AsEnumerable());

                if (dataSource.ProductFamily == "Twain Data Source On WIA" && dataSource.Name.StartsWith("WIA-"))
                {
                    // clone so changes aren't passed back to the caller
                    var wiaSettings = settings.Clone();
                    // default back to WIA scanning so full feature set is supported
                    wiaSettings.SelectedScanner = settings.SelectedScanner.Substring(4); // remove WIA- from the name
                    return ScanUsingWia(wiaSettings);
                }

                dataSource.Open();

                if (settings.DuplexMode && (dataSource.CapGetValues(CapabilityId.CapDuplex)) != null)
                    dataSource.CapSetDuplex(true);

                switch (settings.ColorSetting)
                {
                    case ColorSetting.BlackAndWhite:
                        dataSource.CapSetPixelType(PixelType.BlackWhite);
                        break;
                    case ColorSetting.Grayscale:
                        dataSource.CapSetPixelType(PixelType.Gray);
                        break;
                    case ColorSetting.Color:
                        dataSource.CapSetPixelType(PixelType.RGB);
                        break;
                }

                switch (settings.PageSize)
                {
                    case PageSize.Letter:
                        dataSource.CapSetSupportedSize(SupportedSize.USLetter);
                        break;
                    case PageSize.Legal:
                        dataSource.CapSetSupportedSize(SupportedSize.USLegal);
                        break;
                    case PageSize.Card:
                        dataSource.CapSetSupportedSize(SupportedSize.BusinessCard);
                        break;
                }

                var dpi = dataSource.CapGetDPIs().FirstOrDefault(d => (int)d.Whole == (int)settings.Dpi);
                if (dpi != default(TWFix32))
                {
                    dataSource.CapSetDPI(dpi);
                }

                EventHandler<DataTransferredEventArgs> onDataTransferred = (sender, e) =>
                {
                    if (e.NativeData == IntPtr.Zero) return;
                    using (var img = e.NativeData.GetDrawingBitmap())
                    {
                        if (settings.PageSize == PageSize.Card && img.Height > img.Width)
                        {
                            // need to rotate

                            if (scannedImages.Count % 2 == 1 && settings.DuplexMode)
                            {
                                img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                            }
                            else
                            {
                                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                            }
                        }

                        scannedImages.Add(img.ToByteArray(ImageFormat.Jpeg, settings.Quality));
                    }
                };

                session.DataTransferred += onDataTransferred;

                EventHandler<TransferErrorEventArgs> onTransferError = (sender, e) =>
                {
                    Trace.TraceWarning(new Exception("Scanning transfer error: {0}.".FormatWith(e.ReturnCode), e.Exception).ToString());
                    wait.Set();
                };

                session.TransferError += onTransferError;

                EventHandler<DeviceEventArgs> onDeviceEvent = (sender, e) =>
                {                    
                    if (new[] {DeviceEvent.LampFailure, DeviceEvent.DeviceOffline, DeviceEvent.DeviceRemoved, DeviceEvent.PaperJam}.Contains(e.DeviceEvent.Event))
                    {
                        Trace.TraceWarning(new Exception("Scanning device error: {0}.".FormatWith(e.DeviceEvent)).ToString());
                        wait.Set();
                    }
                };

                session.DeviceEvent += onDeviceEvent;

                var lastState = session.State;

                EventHandler onStateChanged = delegate
                {
                    if (lastState < session.State)
                    {
                        lastState = session.State;

                        return;
                    }

                    lastState = session.State;

                    if (session.State == 4)
                    {
                        session.CurrentSource.Close();
                    }
                    if (session.State == 3)
                    {
                        wait.Set();
                    }
                };

                session.StateChanged += onStateChanged;

                var transferResult = dataSource.StartTransfer(SourceEnableMode.NoUI, true, IntPtr.Zero);

                if (transferResult != ReturnCode.Success)
                {
                    Trace.TraceWarning("Could not perform TWAIN scan with transfer status {0}.", transferResult);
                }

                wait.WaitOne();

                session.StateChanged -= onStateChanged;
                session.DataTransferred -= onDataTransferred;
                session.TransferError -= onTransferError;
                session.DeviceEvent -= onDeviceEvent;

                Tuple<ScanReturnStatus, IEnumerable<byte[]>> result;

                if (scannedImages.Count == 0)
                    transferResult = ReturnCode.Cancel;

                switch (transferResult)
                {
                    case ReturnCode.Success:
                        result = Tuple.Create(ScanReturnStatus.Success, scannedImages.AsEnumerable());
                        break;
                    case ReturnCode.Cancel:
                        result = Tuple.Create(ScanReturnStatus.Cancelled, scannedImages.AsEnumerable());
                        break;
                    default:
                        result = Tuple.Create(ScanReturnStatus.Failed, scannedImages.AsEnumerable());
                        break;
                }

                return result;
            }
            finally
            {
                if (dataSource != null && session.State > 3)
                {
                    dataSource.Close();
                }
                if (session != null)
                {
                    session.Close();
                }
            }

        }

        public IEnumerable<String> GetTwainScannerNames()
        {
            var session = CreateTwainSession();
            var sources = session.GetSources();
            var result = sources.Select(source => source.Name).ToList();
            session.Close();
            return result;
        }

        private static TwainSession CreateTwainSession()
        {
            return Task.Factory.StartNew(() =>
            {
                var twIdentity = TWIdentity.CreateFromAssembly(DataGroups.Image, Assembly.GetExecutingAssembly());
                var session = new TwainSession(twIdentity);
                session.Open();

                return session;
            }).Result;
        }
    }



    [XmlRoot]
    public class ScanSettings
    {
        public String SelectedScanner;
        public ColorSetting ColorSetting = ColorSetting.Color;
        public DpiSetting Dpi = DpiSetting.Medium;
        public long Quality = 90;
        public bool DuplexMode;
        public PageSize PageSize = PageSize.Letter;
        public ScanMode? Mode;

        public ScanSettings Clone()
        {
            return new ScanSettings
            {
                SelectedScanner = SelectedScanner,
                ColorSetting = ColorSetting,
                Dpi = Dpi,
                DuplexMode = DuplexMode,
                PageSize = PageSize,
                Mode = Mode
            };
        }
    }

    public enum ScanMode
    {
        Wia,
        Ocr,
        Twain
    }
}