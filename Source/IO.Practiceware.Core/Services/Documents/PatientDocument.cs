﻿using Soaf;
using Soaf.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace IO.Practiceware.Services.Documents
{
    /// <summary>
    ///   Information about a document corresponding to a patient.
    /// </summary>
    public class PatientDocument
    {
        public PatientDocument()
        {
            Pages = new List<PatientDocumentPage>();
        }

        public int PatientId { get; set; }
        public string FileType { get; set; }
        public DateTime DateTime { get; set; }
        public IList<PatientDocumentPage> Pages { get; set; }
        public string FileExtension { get; set; }
    }

    /// <summary>
    /// Represents a page in a patient document.
    /// </summary>
    public abstract class PatientDocumentPage
    {

    }

    /// <summary>
    /// A patient document page that corresponds to binary data.
    /// </summary>
    public class PatientDocumentBinaryPage : PatientDocumentPage
    {
        public PatientDocumentBinaryPage() { }

        public PatientDocumentBinaryPage(byte[] data)
        {
            Data = data;
        }

        public byte[] Data { get; set; }
    }

    /// <summary>
    /// A patient document page that corresponds to a physical file at a path.
    /// </summary>
    public class PatientDocumentFilePage : PatientDocumentPage
    {
        public PatientDocumentFilePage()
        {
        }

        public PatientDocumentFilePage(string filePath)
        {
            FilePath = filePath;
        }

        public string FilePath { get; set; }

        public override string ToString()
        {
            return FilePath;
        }
    }


    /// <summary>
    ///   A patient document sourced from a flat file.
    /// </summary>
    public class FlatFilePatientDocument : PatientDocument
    {
        public FlatFilePatientDocument()
        {
            Lines = new List<string>();
        }

        /// <summary>
        ///   Gets or sets the line data corresponding to this patient file.
        /// </summary>
        /// <value> The line. </value>
        public List<string> Lines { get; set; }

        public override string ToString()
        {
            return Lines.Join(Environment.NewLine);
        }
    }

    public class FlatFilePatientDocumentSourceErrorEventArgs : EventArgs
    {
        public FlatFilePatientDocumentSourceErrorEventArgs(string message)
        {
            Message = message;
        }

        public bool Cancel { get; set; }
        public string Message { get; private set; }
    }

    /// <summary>
    ///   Reads patient files from a flat file. Matches lines using regular expressions.
    /// </summary>
    public class FlatFilePatientDocumentSource : IEnumerable<PatientDocument>, IDisposable
    {
        private readonly string _dateFormat;
        private readonly Regex _lineMatcher;
        private readonly IDictionary<string, int> _patientMap;
        private readonly StreamReader _reader;
        private readonly string _sourceRootPath;

        private FlatFilePatientDocument _current;
        private bool _hasEnumerated;

        /// <summary>
        ///   Initializes a new instance of the <see cref="FlatFilePatientDocumentSource" /> class.
        /// </summary>
        /// <param name="inputFilePath"> The input file path. </param>
        /// <param name="sourceRootPath"> The source root path. </param>
        /// <param name="lineMatcher"> The line matcher. </param>
        /// <param name="patientMap"> The patient map. </param>
        /// <param name="dateFormat"> The date format. </param>
        public FlatFilePatientDocumentSource(string inputFilePath, string sourceRootPath, Regex lineMatcher, IDictionary<string, int> patientMap, string dateFormat = null)
        {
            _sourceRootPath = sourceRootPath;
            _lineMatcher = lineMatcher;
            _patientMap = patientMap;
            _dateFormat = dateFormat;
            _reader = new StreamReader(new FileStream(inputFilePath, FileMode.Open, FileAccess.Read));

            if (new[] { "PatientId", "FileType", "Date", "Path" }.Intersect(lineMatcher.GetGroupNames()).Count() != 4) throw new InvalidOperationException("Regular Expression does not have valid group names PatientId, FileType, and Date");
        }

        #region IDisposable Members

        public void Dispose()
        {
            _reader.Dispose();
        }

        #endregion

        #region IEnumerable<PatientDocument> Members

        public IEnumerator<PatientDocument> GetEnumerator()
        {
            if (_hasEnumerated) throw new InvalidOperationException("Can only enumerate files once per instance.");

            _hasEnumerated = true;

            while (true)
            {
                DateTime now = DateTime.Now.ToClientTime();

                string line = _reader.ReadLine();

                if (line == null)
                {
                    if (_current != null) yield return _current;
                    _current = null;
                    break;
                }

                Match match = _lineMatcher.Match(line);
                if (!match.Success)
                {
                    if (_current != null) yield return _current;
                    _current = null;
                    if (OnError("Could not process because the document line did not match the regular expression specified: {0}.".FormatWith(line))) break;
                    continue;
                }

                string inputPatientId = match.Groups["PatientId"].Value;

                if (inputPatientId.IsNullOrEmpty() && _current == null)
                {
                    if (OnError("Could not process because no PatientId was found: {0}.".FormatWith(line))) break;
                    continue;
                }

                int internalPatientId = 0;
                if (inputPatientId.IsNotNullOrEmpty() && !_patientMap.TryGetValue(inputPatientId, out internalPatientId))
                {
                    if (_current != null) yield return _current;
                    _current = null;
                    if (OnError("Could not process because the patient id is not mapped: {0}.".FormatWith(line))) break;
                    continue;
                }

                string fileType = match.Groups["FileType"].Value;

                string fileDate = match.Groups["Date"].Value;

                DateTime fileDateResolved = now;
                if (fileDate.IsNotNullOrEmpty() && !TryParseFileDate(fileDate, out fileDateResolved))
                {
                    if (_current != null) yield return _current;
                    _current = null;
                    if (OnError("Could not process because date was not in a valid format: {0}.".FormatWith(line))) break;
                    continue;
                }

                string path = match.Groups["Path"].Value;
                if (path.IsNullOrEmpty())
                {
                    if (_current != null) yield return _current;
                    _current = null;
                    if (OnError("Could not process because no path was found: {0}.".FormatWith(line))) break;
                    continue;
                }

                if (!_sourceRootPath.IsNullOrEmpty())
                {
                    path = Path.Combine(_sourceRootPath, path);
                }

                string fileExtension = match.Groups["FileExtension"].IfNotNull(g => g.Value);
                if (string.IsNullOrWhiteSpace(fileExtension)) fileExtension = Path.GetExtension(path).TrimStart(".");
                if (!fileExtension.StartsWith(".")) fileExtension = "." + fileExtension;

                if (_current != null && fileExtension != "." && fileExtension != _current.FileExtension)
                {
                    yield return _current;
                    _current = null;
                }

                if (_current != null && internalPatientId != 0 && internalPatientId != _current.PatientId)
                {
                    yield return _current;
                    _current = null;
                }
                if (_current != null && fileDateResolved != now && fileDateResolved != _current.DateTime)
                {
                    yield return _current;
                    _current = null;
                }
                if (_current != null && fileType.IsNotNullOrEmpty() && fileType != _current.FileType)
                {
                    yield return _current;
                    _current = null;
                }

                if (_current == null && internalPatientId == 0)
                {
                    if (OnError("No mapped PatientId was found: {0}.".FormatWith(line))) break;
                    continue;
                }


                if (_current == null) _current = new FlatFilePatientDocument { DateTime = fileDateResolved, PatientId = internalPatientId, FileType = fileType.IsNullOrEmpty() ? "OTHER" : fileType, FileExtension = fileExtension };

                _current.Lines.Add(line);
                _current.Pages.Add(new PatientDocumentFilePage(path));
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        /// <summary>
        ///   Occurs when [error].
        /// </summary>
        public event EventHandler<FlatFilePatientDocumentSourceErrorEventArgs> Error;

        private bool OnError(string error)
        {
            var args = new FlatFilePatientDocumentSourceErrorEventArgs(error);
            Error.Fire(this, args);
            return args.Cancel;
        }

        private bool TryParseFileDate(string fileDate, out DateTime dateTime)
        {
            if (_dateFormat.IsNotNullOrEmpty())
            {
                return DateTime.TryParseExact(fileDate, _dateFormat, null, DateTimeStyles.None, out dateTime);
            }
            if (DateTime.TryParseExact(fileDate, "yyyyMMdd", null, DateTimeStyles.None, out dateTime))
            {
                return true;
            }
            if (DateTime.TryParse(fileDate, out dateTime))
            {
                return true;
            }

            return false;
        }
    }
}