﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using Aspose.Words;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Documents.RazorEngine;
using IO.Practiceware.Storage;
using RazorEngine;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using AggregateException = System.AggregateException;

[assembly: Component(typeof(DocumentGenerationService), typeof(IDocumentGenerationService))]

namespace IO.Practiceware.Services.Documents
{
    public interface IDocumentGenerationService
    {
        /// <summary>
        ///   Converts the document to the specified format.
        /// </summary>
        /// <param name="documentData"> The document data. </param>
        /// <param name="format"> The format. </param>
        /// <returns> </returns>
        byte[] ConvertDocument(byte[] documentData, string format);

        /// <summary>
        ///   Performs the mail merge.
        /// </summary>
        /// <param name="templateFilePath"> The template file path. </param>
        /// <param name="mergeData"> The merge data. </param>
        /// <param name="mergeToFilePath"> The merge to file path. </param>
        /// <param name="signatureFilePath"> The signature file path. </param>
        /// <param name="replaceCharacters"> if set to <c>true</c> [replace characters]. </param>
        /// <param name="replaceCurrency"> if set to <c>true</c> [replace currency]. </param>
        void PerformMailMerge(string templateFilePath, string mergeData, string mergeToFilePath, string signatureFilePath = null, bool replaceCharacters = true, bool replaceCurrency = true);

        // For PDF Utility
        void PerformMailMerge(string templateFilePath, string mergeData, string patientId, string appointmentId, string defaultPDFDirectory, string signatureFilePath = "", bool replaceCharacters = true, bool replaceCurrency = true);
        /// <summary>
        ///   Performs the mail merge.
        /// </summary>
        /// <param name="templateFile"> The template file. </param>
        /// <param name="mergeData"> The merge data. </param>
        /// <param name="signatureFilePath"> The signature file path. </param>
        /// <param name="replaceCharacters"> if set to <c>true</c> [replace characters]. </param>
        /// <param name="replaceCurrency"> if set to <c>true</c> [replace currency]. </param>
        /// <returns> </returns>
        byte[] PerformMailMerge(byte[] templateFile, object mergeData, string signatureFilePath = null, bool replaceCharacters = true, bool replaceCurrency = true);

        /// <summary>
        /// Compiles the razor template, and then runs it against the <see cref="viewModel"/>
        /// </summary>
        /// <param name="templateContent">The razor template</param>
        /// <param name="viewModel">The view model which contains the data the razor engine will use to create the merge with the template</param>
        /// <param name="removeImgTags">Should image tags be removed. Default is false</param>
        /// <returns></returns>
        string CompileRazorAndRun(string templateContent, object viewModel, bool removeImgTags = false);

        /// <summary>
        /// Compiles the razor template, and then runs it against the <see cref="viewModel"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="templateContent"></param>
        /// <param name="viewModel"></param>
        /// <param name="removeImgTags"></param>
        /// <returns></returns>
        string CompileRazorAndRun<T>(string templateContent, T viewModel, bool removeImgTags = false);

        /// <summary>
        /// Compiles the razor template, and then runs it against each viewModel data in the list of <see cref="viewModels"/>.  Will return a new merged file for every model.
        /// </summary>
        /// <param name="templateContent">The razor template</param>
        /// <param name="viewModels">The view models which contains the data the razor engine will use to create the merge with the template</param>
        /// <param name="removeImgTags">Should image tags be removed. Default is false</param>		
        /// <returns></returns>
        IEnumerable<string> CompileRazorAndRun<T>(string templateContent, IEnumerable<T> viewModels, bool removeImgTags = true);


        /// <summary>
        ///   Converts the images to a PDF.
        /// </summary>
        /// <param name="pageSize"> Size of the page. </param>
        /// <param name="images"> The images. </param>
        /// <returns> </returns>
        byte[] ConvertImagesToPdf(Size pageSize, params byte[][] images);

        /// <summary>
        /// Combines the PDFs into a single document.
        /// </summary>
        /// <param name="documents">The documents.</param>
        /// <returns></returns>
        byte[] CombinePdfs(IEnumerable<byte[]> documents);

        /// <summary>
        /// Combines CSV files.
        /// </summary>
        /// <param name="hasHeader">if set to <c>true</c> [has header].</param>
        /// <param name="contents">The contents.</param>
        /// <returns></returns>
        string CombineCsvs(bool hasHeader, params string[] contents);
    }

    internal class DocumentGenerationService : IDocumentGenerationService
    {
        static DocumentGenerationService()
        {
            var license = new License();
            license.SetLicense((Assembly.GetExecutingAssembly().GetManifestResourceStream("IO.Practiceware.Services.Documents.Aspose.Words.lic")));
        }

        #region IDocumentGenerationService Members

        /// <summary>
        ///   Converts the document to the specified format.
        /// </summary>
        /// <param name="documentData"> The document data. </param>
        /// <param name="format"> The format. </param>
        /// <returns> </returns>
        public byte[] ConvertDocument(byte[] documentData, string format)
        {
            var saveFormat = format.ToEnum<SaveFormat>();
            using (var inputStream = new MemoryStream(documentData))
            {
                var document = new Aspose.Words.Document(inputStream);

                using (var resultStream = new MemoryStream())
                {
                    document.Save(resultStream, saveFormat);

                    return resultStream.ToArray();
                }
            }
        }

        public void PerformMailMerge(string templateFilePath, string mergeData, string mergeToFilePath, string signatureFilePath, bool replaceCharacters, bool replaceCurrency)
        {
            if (mergeToFilePath.IsNullOrWhiteSpace())
            {
                Trace.TraceError(new ArgumentException("mergeToFilePath is not a valid file path.").ToString());
                return;
            }
            if (templateFilePath.IsNullOrWhiteSpace() || !FileManager.Instance.FileExists(templateFilePath))
            {
                Trace.TraceError(new ArgumentException("templateFilePath is not a valid file path.").ToString());
                return;
            }

            try
            {
                byte[] result = PerformMailMerge(FileManager.Instance.ReadContents(templateFilePath), mergeData, signatureFilePath, replaceCharacters, replaceCurrency);
                FileManager.Instance.CommitContents(mergeToFilePath, result);
            }
            catch
            {
                var log = "Performing mail merge...\r\nTemplate: {0}\r\nMerge Data: {1}\r\nOutput: {2}\r\nSignature: {3}\r\nReplace Characters: {4}\r\nReplace Currency: {5}"
                    .FormatWith(templateFilePath, mergeData, mergeToFilePath, signatureFilePath, replaceCharacters, replaceCurrency);

                FileManager.Instance.CommitContents(FileManager.Instance.GetTempPathName() + ".merge.log", log);
                throw;
            }
        }

        // For PDF Utility
        public void PerformMailMerge(string templateFilePath, string mergeData, string patientId, string appointmentId, string defaultPDFDirectory, string signatureFilePath, bool replaceCharacters, bool replaceCurrency)
        {
            if (templateFilePath.IsNullOrWhiteSpace() || !FileManager.Instance.FileExists(templateFilePath))
            {
                Trace.TraceError(new ArgumentException("templateFilePath is not a valid file path.").ToString());
                return;
            }
            try
            {

                string fileDirectory = defaultPDFDirectory + @"\" + patientId.Replace(" ", "");
                string filePath4Doc = fileDirectory + @"\" + appointmentId.Replace(" ", "") + ".doc";
                string filePath4Pdf = fileDirectory + @"\" + appointmentId.Replace(" ", "") + ".pdf";
                if (!Directory.Exists(fileDirectory))
                {
                    Directory.CreateDirectory(fileDirectory);
                }
                byte[] result = PerformMailMerge(FileManager.Instance.ReadContents(templateFilePath), mergeData, signatureFilePath, replaceCharacters, replaceCurrency);
                FileManager.Instance.CommitContents(filePath4Doc, result);
                using (var inputStream = new MemoryStream(result))
                {
                    var document = new Aspose.Words.Document(inputStream);

                    using (var resultStream = new MemoryStream())
                    {
                        document.Save(filePath4Pdf, SaveFormat.Pdf);
                    }
                }
            }
            catch
            {
                var log = "Performing mail merge...\r\nTemplate: {0}\r\nMerge Data: {1}\r\nOutput: {2}\r\nSignature: {3}\r\nReplace Characters: {4}\r\nReplace Currency: {5}"
                    .FormatWith(templateFilePath, mergeData, defaultPDFDirectory, signatureFilePath, replaceCharacters, replaceCurrency);

                FileManager.Instance.CommitContents(FileManager.Instance.GetTempPathName() + ".merge.log", log);
                throw;
            }
        }

        public byte[] PerformMailMerge(byte[] templateFile, object mergeData, string signatureFilePath, bool replaceCharacters, bool replaceCurrency)
        {
            using (var inStream = new MemoryStream(templateFile))
            {
                using (var outStream = new MemoryStream())
                {
                    new Aspose.Words.Document(inStream).PerformMailMerge(mergeData, signatureFilePath, replaceCharacters, replaceCurrency).Save(outStream, SaveFormat.Doc);
                    return outStream.ToArray();
                }
            }
        }

        public string CompileRazorAndRun(string templateContent, object viewModel, bool removeImgTags = false)
        {
            return CompileRazorAndRun<object>(templateContent, viewModel, removeImgTags);
        }

        public string CompileRazorAndRun<T>(string templateContent, T viewModel, bool removeImgTags = false)
        {
            return CompileRazorAndRun<T>(templateContent, new[] { viewModel }, removeImgTags).FirstOrDefault();
        }

        public IEnumerable<string> CompileRazorAndRun<T>(string templateContent, IEnumerable<T> viewModels, bool removeImgTags = true)
        {
            if (templateContent == null) throw new ArgumentNullException("templateContent");

            if (templateContent == string.Empty) return viewModels != null ? viewModels.Select(vm => string.Empty) : new[] { string.Empty };

            if (viewModels.IsNullOrEmpty()) return new[] { templateContent };

            if (removeImgTags)
            {
                // remove all img tag
                templateContent = Regex.Replace(templateContent, @"(<img\/?[^>]+>)", @"", RegexOptions.IgnoreCase);
            }

            var type = typeof(T);
            var cacheNameKey = "cachedRazorTemplate" + type.FullName;

            try
            {
                if (viewModels.Count() > 1)
                {
                    // compiles and caches the template based on the cache key name.  Internally, if the template content has changed, then 
                    // the cache is invalidated for that key name and re-cached.
                    Razor.Compile<T>(templateContent, cacheNameKey);
                }
                else
                {
                    return new List<string> { Razor.Parse(templateContent, viewModels.First(), cacheNameKey) };
                }
            }
            catch (CustomTemplateCompilationException)
            {
                throw;
            }
            catch (CustomTemplateParsingException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not compile template {0}.\r\n{1}".FormatWith(cacheNameKey, templateContent), ex);
            }

            var exceptions = new List<Exception>();
            var results = viewModels.Select(x =>
                                     {
                                         var viewModel = x;
                                         try
                                         {
                                             return Razor.Run(cacheNameKey, viewModel);
                                         }
                                         catch (CustomTemplateParsingException cex)
                                         {
                                             exceptions.Add(cex);
                                             return null;
                                         }
                                         catch (Exception ex)
                                         {
                                             exceptions.Add(new ApplicationException("Error merging the template. Model".FormatWith(Equals(viewModel, default(T)) ? "null" : viewModel.ToXml()), ex));
                                             return null;
                                         }
                                     }).ToList();

            if (exceptions.Any())
            {
                throw new AggregateException(exceptions);
            }

            return results;
        }

        public byte[] CombinePdfs(IEnumerable<byte[]> documents)
        {
            byte[] completedDocument;

            using (var stream = new MemoryStream())
            using (var document = new iTextSharp.text.Document())
            using (var pdfCopy = new PdfCopy(document, stream))
            {
                document.Open();

                foreach (var byteArray in documents)
                {
                    using (var reader = new PdfReader(byteArray))
                    {
                        for (var i = 0; i < reader.NumberOfPages; i++)
                        {
                            pdfCopy.AddPage(pdfCopy.GetImportedPage(reader, ++i));
                        }
                    }
                }

                pdfCopy.Close();
                document.Close();
                completedDocument = stream.ToArray();
            }
            return completedDocument;
        }

     

        public string CombineCsvs(bool hasHeader, params string[] contents)
        {
            var lines = new List<string>();

            foreach (var content in contents)
            {
                foreach (var line in content.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                    .Skip(lines.Count > 0 && hasHeader ? 1 : 0))
                {
                    lines.Add(line);
                }
            }
            return lines.Join(Environment.NewLine);
        }

        public byte[] ConvertImagesToPdf(Size pageSize, params byte[][] images)
        {
            try
            {
                float width = Convert.ToSingle(72 * pageSize.Width);
                float height = Convert.ToSingle(72 * pageSize.Height);
                var iTextSharpPageSize = new Rectangle(width, height);
                var document = new iTextSharp.text.Document(iTextSharpPageSize);
                iTextSharp.text.Document.Compress = true;

                using (var outputStream = new MemoryStream())
                {
                    PdfWriter writer = PdfWriter.GetInstance(document, outputStream);
                    writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_5);
                    writer.CompressionLevel = PdfStream.BEST_COMPRESSION;

                    document.Open();
                    PdfContentByte content = writer.DirectContent;
                    foreach (byte[] imageData in images)
                    {
                        Image image = Image.GetInstance(imageData);
                        image.ScaleAbsolute(iTextSharpPageSize.Width, iTextSharpPageSize.Height);
                        image.SetAbsolutePosition(0, 0);
                        content.AddImage(image);
                        document.NewPage();
                    }
                    if (images.Count() != 0)
                        document.Close();

                    return outputStream.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Failed to convert images to pdf.", ex);
            }
        }

        #endregion
    }

    public enum PageSize
    {
        [Display(Name = "Letter")]
        Letter,

        [Display(Name = "Legal")]
        Legal,

        [Display(Name = "Card")]
        Card
    }

    public static class PageSizeExtensions
    {
        public static Size Size(this PageSize pageSize)
        {
            switch (pageSize)
            {
                case PageSize.Letter:
                    return new Size(8.5, 11);
                case PageSize.Legal:
                    return new Size(8.5, 14);
                case PageSize.Card:
                    return new Size(3.5, 2);
                default:
                    throw new Exception("No size defined for {0}".FormatWith(pageSize));
            }
        }
    }
}