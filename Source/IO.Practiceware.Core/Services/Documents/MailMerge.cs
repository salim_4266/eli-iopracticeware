﻿using Aspose.Words;
using Aspose.Words.Fields;
using Aspose.Words.Reporting;
using Aspose.Words.Tables;
using IO.Practiceware.Configuration;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.Presentation;
using Soaf.Reflection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace IO.Practiceware.Services.Documents
{
    internal static class AsposeDocuments
    {
        public class FieldRemover : DocumentVisitor
        {
            private static readonly FieldType[] ExcludedFieldTypes = new[]
                                                                         {
                                                                             FieldType.FieldNone,
                                                                             FieldType.FieldPage, // remove once bug mentioned here is fixed: http://www.aspose.com/community/forums/permalink/349379/349379/showthread.aspx
                                                                             FieldType.FieldAutoNum,
                                                                             FieldType.FieldAutoNumLegal,
                                                                             FieldType.FieldAutoNumOutline
                                                                         };

            public FieldRemover(FieldType? targetFieldType)
            {
                _targetFieldType = targetFieldType;
            }

            public override VisitorAction VisitFieldStart(FieldStart fieldStart)
            {
                // We must keep track of the starts and ends of fields incase of any nested fields.
                if (IsMatchingFieldType(fieldStart))
                {
                    _fieldStartToSeparatorDepth++;
                    _fieldDepth++;
                    fieldStart.Remove();
                }
                else
                {
                    // This removes the field start if it's inside a field that is being converted.
                    CheckDepthAndRemoveNode(fieldStart);
                }

                return VisitorAction.Continue;
            }

            public override VisitorAction VisitFieldSeparator(FieldSeparator fieldSeparator)
            {
                // When visiting a field separator we should decrease the depth level.
                if (IsMatchingFieldType(fieldSeparator))
                {
                    _fieldStartToSeparatorDepth--;
                    fieldSeparator.Remove();
                }
                else
                {
                    // This removes the field separator if it's inside a field that is being converted.
                    CheckDepthAndRemoveNode(fieldSeparator);
                }

                return VisitorAction.Continue;
            }

            public override VisitorAction VisitFieldEnd(FieldEnd fieldEnd)
            {
                if (IsMatchingFieldType(fieldEnd))
                {
                    fieldEnd.Remove();
                    _fieldDepth--;
                    _fieldStartToSeparatorDepth = _fieldDepth;
                }
                else
                    CheckDepthAndRemoveNode(fieldEnd); // This removes the field end if it's inside a field that is being converted.

                return VisitorAction.Continue;
            }

            public override VisitorAction VisitRun(Run run)
            {
                // Remove the run if it is between the FieldStart and FieldSeparator of the field being converted.
                CheckDepthAndRemoveNode(run);

                return VisitorAction.Continue;
            }

            public override VisitorAction VisitParagraphEnd(Paragraph paragraph)
            {
                if (_fieldStartToSeparatorDepth > 0)
                {
                    // The field code that is being converted continues onto another paragraph. We 
                    // need to copy the remaining content from this paragraph onto the next paragraph.
                    Node nextParagraph = paragraph.NextSibling;

                    // Skip ahead to the next available paragraph.
                    while (nextParagraph != null && nextParagraph.NodeType != NodeType.Paragraph)
                        nextParagraph = nextParagraph.NextSibling;

                    // Copy all of the nodes over. Keep a list of these nodes so we know not to remove them.
                    while (paragraph.HasChildNodes && nextParagraph != null)
                    {
                        _nodesToSkip.Add(paragraph.LastChild);
                        ((Paragraph)nextParagraph).PrependChild(paragraph.LastChild);
                    }

                    paragraph.Remove();
                }

                return VisitorAction.Continue;
            }

            public override VisitorAction VisitTableStart(Table table)
            {
                CheckDepthAndRemoveNode(table);

                return VisitorAction.Continue;
            }

            /// <summary>
            /// Checks whether the node is inside a field or should be skipped and then removes it if necessary.
            /// </summary>
            private void CheckDepthAndRemoveNode(Node node)
            {
                if (_fieldStartToSeparatorDepth > 0 && !_nodesToSkip.Contains(node))
                    node.Remove();
            }

            private bool IsMatchingFieldType(FieldChar fieldNode)
            {
                return (_targetFieldType == null && !ExcludedFieldTypes.Contains(fieldNode.FieldType)) || fieldNode.FieldType.Equals(_targetFieldType);
            }

            private int _fieldStartToSeparatorDepth;
            private int _fieldDepth;
            private readonly ArrayList _nodesToSkip = new ArrayList();
            private readonly FieldType? _targetFieldType;
        }

        /// <summary>
        /// Converts any fields of the specified type found in the descendants of the node into static text.
        /// </summary>
        /// <param name="compositeNode">The node in which all descendants of the specified FieldType will be converted to static text.</param>
        /// <param name="targetFieldType">The FieldType of the field to convert to static text.</param>
        public static void ConvertFieldsToStaticText(this CompositeNode compositeNode, FieldType? targetFieldType = null)
        {
            var remover = new FieldRemover(targetFieldType);
            compositeNode.Accept(remover);

        }

        public static void InsertSignature(this Aspose.Words.Document document, string signaturefile)
        {
            document.Range.Replace(new Regex(@"-\?-"), new SignatureReplacer(signaturefile), true);
        }

        /// <summary>
        /// Replaces autonum fields with static text.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="dataSource">The data source.</param>
        public static void ReplaceAutoNumFields(this Aspose.Words.Document document, object dataSource)
        {
            var mailMergeDataSource = new MailMergeDataSource(dataSource);

            int count = 1;
            while (mailMergeDataSource.MoveNext())
            {
                count++;
            }

            Action<FieldType> insertAutoNumFields = fieldType =>
                                                        {
                                                            FieldStart[] starts = document.GetChildNodes(NodeType.FieldStart, true).OfType<FieldStart>().Where(start => start.FieldType == fieldType).ToArray();

                                                            int i = 1;
                                                            foreach (var start in starts)
                                                            {
                                                                var fieldNodes = start.GetFieldNodes();
                                                                start.ParentNode.InsertAfter(new Run(document, (i++).ToString()), start);
                                                                fieldNodes.ForEachWithSinglePropertyChangedNotification(n => n.Remove());
                                                                if (i == count) break;
                                                            }
                                                        };

            new[] { FieldType.FieldAutoNum, FieldType.FieldAutoNumLegal, FieldType.FieldAutoNumOutline }.ForEachWithSinglePropertyChangedNotification(insertAutoNumFields);
        }

        private const string PreserveEmptySpaces = "PreserveEmptySpaces";
        public static Aspose.Words.Document PerformMailMerge(this Aspose.Words.Document document, object dataSource, string signatureFilePath = null, bool replaceCharacters = true, bool replaceCurrency = true)
        {
            document.FieldOptions.FieldUpdateCultureSource = FieldUpdateCultureSource.FieldCode;

            document.FieldOptions.UserPromptRespondent = new FieldUserPrompt();

            document.MailMerge.UseNonMergeFields = true;

            document.MailMerge.CleanupOptions = MailMergeCleanupOptions.RemoveContainingFields;
            if (document.Range.Bookmarks.OfType<Bookmark>().All(b => b.Name != PreserveEmptySpaces))
            {
                document.MailMerge.CleanupOptions |= MailMergeCleanupOptions.RemoveEmptyParagraphs | MailMergeCleanupOptions.RemoveUnusedRegions;
            }

            document.MailMerge.FieldMergingCallback = new MergeFieldHandler();

            var mailMergeDataSource = new MailMergeDataSource(dataSource);

            var text = document.GetText();

            document.MailMerge.ExecuteWithRegions(mailMergeDataSource);

            // document did not have regions, so execute again (if the document had regions, executing this again will completely wipe out the document, so we must do this check)
            if (text == document.GetText())
            {
                document.MailMerge.Execute(mailMergeDataSource);
            }

            document.MailMerge.DeleteFields();

            document.ConvertFieldsToStaticText(FieldType.FieldRef);

            document.ReplaceAutoNumFields(dataSource);

            document.ReplaceCharactersAndCurrency(replaceCharacters, replaceCurrency);

            var validSignatureFilePath = OperationCache.ExecuteCached(() => FixSignatureFilePath(signatureFilePath));
            if (validSignatureFilePath.IsNotNullOrEmpty())
            {
                document.InsertSignature(validSignatureFilePath);
            }

            document.ConvertFieldsToStaticText();

            return document;
        }

        public static void ReplaceCharactersAndCurrency(this Aspose.Words.Document document, bool replaceCharacters, bool replaceCurrency)
        {
            if (replaceCharacters)
            {
                document.Range.Replace(new Regex(@"(-&-|~|-\+-|\^)"), new SymbolReplacer(), true);
            }

            if (replaceCurrency)
            {
                document.Range.Replace(new Regex("-$-"), new SymbolReplacer(), true);
            }
        }

        public static IEnumerable<Node> GetFieldNodes(this FieldStart start)
        {
            int depth = 0;

            var nodes = new List<Node>();
            nodes.Add(start);

            Node node = start.NextPreOrder(start.Document);

            while (depth != 0 || !node.Is<FieldEnd>())
            {
                if (node is FieldStart) depth++;
                else if (node is FieldEnd) depth--;
                nodes.Add(node);
                node = node.NextPreOrder(node.Document);
            }

            // fieldend
            nodes.Add(node);

            return nodes;
        }

        /// <summary>
        /// Attempts to fix signature file path when running in Cloud Storage mode
        /// </summary>
        /// <param name="signatureFilePath"></param>
        /// <returns></returns>
        private static string FixSignatureFilePath(string signatureFilePath)
        {
            // Can't fix empty value
            if (string.IsNullOrEmpty(signatureFilePath)) return signatureFilePath;

            var fixedSignaturePath = signatureFilePath;

            // Identify relative signature file paths
            var pathRoot = Path.GetPathRoot(fixedSignaturePath);
            if (string.IsNullOrEmpty(pathRoot))
            {
                fixedSignaturePath = Path.Combine(ConfigurationManager.ServerDataPath, fixedSignaturePath);
            }

            // If path exists -> return
            if (FileManager.Instance.FileExists(fixedSignaturePath))
            {
                return fixedSignaturePath;
            }

            const string pinpointPathPart = @"Pinpoint\";
            var pinpointIndex = fixedSignaturePath.IndexOf(pinpointPathPart, StringComparison.OrdinalIgnoreCase);
            if (pinpointIndex >= 0)
            {
                fixedSignaturePath = fixedSignaturePath.Remove(0, pinpointIndex + pinpointPathPart.Length);
                fixedSignaturePath = Path.Combine(ConfigurationManager.ServerDataPath, fixedSignaturePath);

                // Verify it exists
                fixedSignaturePath = FileManager.Instance.FileExists(fixedSignaturePath)
                    ? fixedSignaturePath
                    : null;
            }
            else
            {
                // Path doesn't exist and we couldn't fix it
                fixedSignaturePath = null;
            }

            // Log error fixing signature path
            if (string.IsNullOrEmpty(fixedSignaturePath))
            {
                Trace.TraceWarning("Could not locate specified signature file. Path: {0}", signatureFilePath);
            }

            return fixedSignaturePath;
        }
    }

    internal class FieldUserPrompt : IFieldUserPromptRespondent
    {
        public string Respond(string promptText, string defaultResponse)
        {
            string answer = InteractionManager.Current.Prompt(promptText).Input;
            return answer.IsNotNullOrEmpty() ? answer : string.Empty;
        }
    }

    internal class MergeFieldHandler : IFieldMergingCallback
    {
        #region IFieldMergingCallback Members

        public void FieldMerging(FieldMergingArgs e)
        {
            if (e.FieldValue != null)
            {
                FieldStart fieldStart = e.Field.Start;
                FieldEnd fieldEnd = e.Field.End;
                Node currentNode = fieldStart;

                // aspose doesn't handled caps format correctly, so we need to do it manually...
                while (!ReferenceEquals(currentNode, fieldEnd) & currentNode != null)
                {
                    var nodeTextWithoutSpaces = currentNode.GetText().ToLower().Replace(" ", string.Empty);
                    var fieldValue = e.FieldValue.ToString();
                    if (nodeTextWithoutSpaces.Contains("\\*caps") && fieldValue.Trim() != string.Empty)
                    {
                        e.Text = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(fieldValue.ToLower());
                    }
                    currentNode = currentNode.NextPreOrder(currentNode.Document);
                }
            }
        }

        public void ImageFieldMerging(ImageFieldMergingArgs args)
        {
        }

        #endregion
    }

    /// <summary>
    ///   Supports mail merges against an object or list data source.
    /// </summary>
    internal class MailMergeDataSource : IMailMergeDataSource
    {
        private readonly object[] _collection;

        private readonly string _tableName;
        private int _index = -1;

        private readonly object _parent;

        public MailMergeDataSource(object source, object parent = null, string name = null)
        {
            _parent = parent;

            if (source is string)
            {
                source = ParseDataTable((string)source);
            }
            if (source is DataTable)
            {
                source = ConvertDataTable((DataTable)source);
            }

            _collection = !source.Is<IEnumerable>() ? new[] { source } : ((IEnumerable)source).OfType<object>().ToArray();
            _tableName = name ?? "Root";
        }

        #region IMailMergeDataSource Members

        public bool MoveNext()
        {
            _index += 1;
            return _index < _collection.Length;
        }

        [DebuggerNonUserCode]
        public bool GetValue(string fieldName, out object fieldValue)
        {
            try
            {
                object instance;

                if (fieldName.StartsWith("Parent.") || fieldName == "Parent")
                {
                    instance = _parent;
                    fieldName = fieldName.ReplaceFirst("Parent.", "").ReplaceFirst("Parent", "");
                }
                else if (fieldName.StartsWith("Item.") || fieldName == "Item")
                {
                    instance = _collection[_index];
                    fieldName = fieldName.ReplaceFirst("Item.", "").ReplaceFirst("Item", "");
                }
                else
                {
                    instance = _collection[_index];
                }

                fieldValue = Evaluator.Current.Evaluate(fieldName, instance);
                return true;
            }
            catch (Exception)
            {
                fieldValue = null;
                return false;
            }
        }

        [DebuggerNonUserCode]
        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            if (tableName == "Parent" || tableName == "Item")
            {
                return null;
            }
            try
            {
                object child = Evaluator.Current.Evaluate(tableName, _collection[_index]);
                return new MailMergeDataSource(child, _collection[_index], tableName);
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Child data source not found: " + tableName + ". " + exception.Message);
                return null;
            }
        }

        public string TableName
        {
            get { return _tableName; }
        }

        #endregion

        private static DataTable ParseDataTable(string content)
        {
            using (var reader = new StringReader(content))
            {
                var dataTable = new DataTable();
                string[] fields = null;
                int i = 1;
                string line;
                do
                {
                    line = reader.ReadLine();
                    if (line != null)
                    {
                        if (i == 1)
                        {
                            fields = line.Trim().Split(new[] { "\t" }, StringSplitOptions.None);
                            if (fields.Length > 0)
                            {
                                for (int index = 0; index <= fields.Length - 1; index++)
                                {
                                    try
                                    {
                                        dataTable.Columns.Add("M_" + fields[index].Replace(":", string.Empty).Replace("-", string.Empty).Trim(), typeof(string));
                                    }
                                    catch (Exception exception)
                                    {
                                        Trace.TraceWarning("Input has duplicate field: " + fields[index] + ".");
                                        Trace.TraceError(exception.Message);
                                    }
                                }
                            }
                        }
                        else
                        {
                            var valuesList = new List<string>();
                            string[] values = line.Trim().Split(new[] { "\t" }, StringSplitOptions.None);
                            if (values.Length > 0)
                            {
                                if (fields != null && values.Length <= fields.Length)
                                {
                                    IEnumerator valuesEnumerator = values.GetEnumerator();
                                    while (valuesList.Count <= fields.Length & valuesList.Count < values.Length)
                                    {
                                        string valueToAdd = null;
                                        if (valuesEnumerator.MoveNext())
                                        {
                                            valueToAdd = valuesEnumerator.Current.ToString().Trim();
                                        }
                                        valuesList.Add(valueToAdd);
                                    }
                                }
                                dataTable.Rows.Add(valuesList.ToArray());
                            }
                        }
                    }

                    i += 1;
                } while (line != null);

                return dataTable;
            }
        }

        private static object ConvertDataTable(DataTable dataTable)
        {
            var collection = new List<dynamic>();
            foreach (DataRow row in dataTable.Rows)
            {
                IDictionary<string, object> item = new DataSourceExpandoObject();
                collection.Add(item);
                foreach (DataColumn column in dataTable.Columns)
                {
                    item[column.ColumnName] = row[column];
                }
            }
            return collection;
        }


        #region Nested type: ExpandoObject

        internal class DataSourceExpandoObject : DynamicObject, IDictionary<string, object>
        {
            private readonly IDictionary<string, object> _dictionary = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);

            #region IDictionary<string,object> Members

            public void Add(KeyValuePair<string, object> item)
            {
                _dictionary.Add(item);
            }

            public void Clear()
            {
                _dictionary.Clear();
            }

            public bool Contains(KeyValuePair<string, object> item)
            {
                return _dictionary.Contains(item);
            }

            public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
            {
                _dictionary.CopyTo(array, arrayIndex);
            }

            public bool Remove(KeyValuePair<string, object> item)
            {
                return _dictionary.Remove(item);
            }

            public int Count
            {
                get { return _dictionary.Keys.Count; }
            }

            public bool IsReadOnly
            {
                get { return _dictionary.IsReadOnly; }
            }

            public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
            {
                return _dictionary.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public bool ContainsKey(string key)
            {
                return _dictionary.ContainsKey(key);
            }

            public void Add(string key, object value)
            {
                _dictionary.Add(key, value);
            }

            public bool Remove(string key)
            {
                return _dictionary.Remove(key);
            }

            public bool TryGetValue(string key, out object value)
            {
                return _dictionary.TryGetValue(key, out value);
            }

            public object this[string key]
            {
                get { return _dictionary[key]; }
                set { _dictionary[key] = value; }
            }

            public ICollection<string> Keys
            {
                get { return _dictionary.Keys; }
            }

            public ICollection<object> Values
            {
                get { return _dictionary.Values; }
            }

            #endregion

            public override bool TryGetMember(GetMemberBinder binder, out object result)
            {
                if (_dictionary.ContainsKey(binder.Name))
                {
                    result = _dictionary[binder.Name];
                    return true;
                }
                return base.TryGetMember(binder, out result);
            }

            public override bool TrySetMember(SetMemberBinder binder, object value)
            {
                if (!_dictionary.ContainsKey(binder.Name))
                    _dictionary.Add(binder.Name, value);
                else
                    _dictionary[binder.Name] = value;
                return true;
            }

            public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
            {
                if (_dictionary.ContainsKey(binder.Name) && _dictionary[binder.Name] is Delegate)
                {
                    var del = (Delegate)_dictionary[binder.Name];
                    result = del.DynamicInvoke(args);
                    return true;
                }
                return base.TryInvokeMember(binder, args, out result);
            }

            public override bool TryDeleteMember(DeleteMemberBinder binder)
            {
                if (_dictionary.ContainsKey(binder.Name))
                {
                    _dictionary.Remove(binder.Name);
                    return true;
                }
                return base.TryDeleteMember(binder);
            }
        }

        #endregion
    }

    internal class SymbolReplacer : IReplacingCallback
    {
        #region IReplacingCallback Members

        public ReplaceAction Replacing(ReplacingArgs e)
        {
            if (e.Match.Value == "-&-")
            {
                e.Replacement = ControlChar.LineBreak;
            }
            else if (e.Match.Value == "-$-")
            {
                e.Replacement = "0";
            }
            else if (e.Match.Value == "-+-")
            {
                e.Replacement = "      ";
            }
            else if (new[] { "~", "^" }.Contains(e.Match.Value))
            {
                e.Replacement = " ";
            }
            else if (e.Match.Value.Trim().StartsWith("REF"))
            {
                e.Replacement = "";
            }
            return ReplaceAction.Replace;
        }

        #endregion
    }

    internal class SignatureReplacer : IReplacingCallback
    {
        private readonly Aspose.Words.Document _signatureDocument;

        internal SignatureReplacer(string signatureDocumentPath)
        {
            using (var ms = new MemoryStream(FileManager.Instance.ReadContents(signatureDocumentPath)))
                _signatureDocument = new Aspose.Words.Document(ms);
        }

        #region IReplacingCallback Members

        ReplaceAction IReplacingCallback.Replacing(ReplacingArgs e)
        {
            return OnReplacing(e);
        }

        #endregion

        private ReplaceAction OnReplacing(ReplacingArgs e)
        {
            InsertSignature(e.MatchNode, _signatureDocument);
            e.Replacement = string.Empty;
            return ReplaceAction.Replace;
        }

        private static void InsertSignature(Node insertAfter, Aspose.Words.Document signatureDocument)
        {
            var importer = new NodeImporter(signatureDocument, insertAfter.Document, ImportFormatMode.KeepSourceFormatting);
            foreach (Section srcSection in signatureDocument.Sections)
            {
                foreach (Node srcNode in srcSection.Body.GetChildNodes(NodeType.Any, true).OfType<Node>().Where(n => n.NodeType != NodeType.Paragraph))
                {
                    Node newNode = importer.ImportNode(srcNode, true);
                    insertAfter.ParentNode.InsertAfter(newNode, insertAfter);
                    insertAfter = newNode;
                }
            }
        }
    }
}
