﻿using IO.Practiceware.Model;
using IO.Practiceware.Services.Documents;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Encounter = IO.Practiceware.Model.Encounter;

[assembly: Component(typeof(EncounterDocumentMap), typeof(IMap<Encounter, IO.Practiceware.Services.Documents.Encounter>))]

namespace IO.Practiceware.Services.Documents
{
    /// <summary>
    ///     A model that corresponds to the merge fields for an encounter
    /// </summary>
    [DataContract]
    public class Encounter
    {
        [DataMember]
        public int EncounterId { get; set; }

        [DataMember]
        public string PatientLastName { get; set; }

        [DataMember]
        public string PatientFirstName { get; set; }

        [DataMember]
        public DateTime? PatientDateOfBirth { get; set; }

        [DataMember]
        public DateTime? AppointmentDateTime { get; set; }

        [DataMember]
        public string DoctorName { get; set; }

        public PatientSurgery PatientSurgery { get; set; }
    }

    /// <summary>
    ///     A model that corresponds to the merge fields for an ASC Surgery
    /// </summary>
    [DataContract]
    public class PatientSurgery
    {
        [DataMember]
        public string SurgeryType;

        public virtual QuestionAnswer[] Answers { get; set; }

        //dictionary of Question name paired with Answer
        public virtual IDictionary<string, QuestionAnswer> AnswersByQuestion
        {
            get { return Answers == null ? null : Answers.ToDictionary(i => i.Question); }
        }
    }

    /// <summary>
    ///     A model that corresponds to the merge fields for an ASC Surgery Question Answers
    /// </summary>
    [DataContract]
    public class QuestionAnswer
    {
        [DataMember]
        public string Question { get; set; }

        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public string Note { get; set; }

        public virtual QuestionChoice[] Choices { get; set; }
    }

    /// <summary>
    ///     A model that corresponds to the merge fields for an ASC Surgery Question Choices
    /// </summary>
    [DataContract]
    public class QuestionChoice
    {
        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public string Note { get; set; }
    }

    /// <summary>
    ///     Maps an Encounter to an EncounterDocument.
    /// </summary>
    internal class EncounterDocumentMap : IMap<Model.Encounter, Encounter>
    {
        private readonly IMapMember<Model.Encounter, Encounter>[] _members;

        public EncounterDocumentMap()
        {
            _members = this
                .CreateMembers(source =>
                               new Encounter
                                   {
                                       EncounterId = source.Id,
                                       PatientLastName = source.Patient.LastName,
                                       PatientFirstName = source.Patient.FirstName,
                                       PatientDateOfBirth = source.Patient.DateOfBirth,
                                       AppointmentDateTime = source.Appointments.Select(a => a.DateTime).FirstOrDefault(),
                                       DoctorName =
                                           source.Appointments.OfType<UserAppointment>().Select(a => a.User.DisplayName).FirstOrDefault(),
                                       PatientSurgery = source.PatientSurgery == null ? null : new PatientSurgery
                                           {
                                               SurgeryType = source.PatientSurgery.SurgeryType.Name,
                                               Answers = (
                                                             source.PatientSurgery.PatientSurgeryQuestionAnswers.ToArray()
                                                                   .Select(qa => new QuestionAnswer { Question = qa.Question.Label, Value = qa.Value, Note = qa.Note })
                                                                   .Union(source.PatientSurgery.PatientSurgeryQuestionChoices.ToArray()
                                                                                .Select(qc => new QuestionAnswer { Question = qc.Choice.Question.Label, Value = qc.Choice.Name, Note = qc.Note }))
                                                         )
                                   .GroupBy(an => an.Question).Select(an => new QuestionAnswer { Question = an.Key, Choices = an.ToArray().Select(ans => new QuestionChoice { Value = ans.Value, Note = ans.Note }).ToArray() }).ToArray()
                                           }
                                   })
                .ToArray();
        }

        #region IMap<Encounter,EncounterDocument> Members

        public IEnumerable<IMapMember<Model.Encounter, Encounter>> Members
        {
            get { return _members; }
        }

        #endregion
    }
}