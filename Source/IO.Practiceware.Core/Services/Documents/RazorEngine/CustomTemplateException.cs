﻿using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Services.Documents.RazorEngine
{
	public class CustomTemplateException : Exception
	{
		/// <summary>
		/// Gets the source code that wasn't compiled.		
		/// </summary>
		public string SourceCode { get; protected set; }

		/// <summary>
		/// Gets the source template that wasn't compiled.		
		/// </summary>
		public string Template { get; protected set; }

		/// <summary>
		/// Gets the model that this template was ran against
		/// </summary>
		public object Model { get; protected set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="CustomTemplateException"/> class.
		/// </summary>
		/// <param name="message">The message that describes the error.</param>
		internal CustomTemplateException(string message)
			: base(message)
		{
		}


		/// <summary>
		/// Initializes a new instance of the <see cref="CustomTemplateException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="innerException">The ex.</param>
		internal CustomTemplateException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		/// <summary>
		/// Initialises a new instance of <see cref="CustomTemplateCompilationException" />
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="template">The source template that wasn't compiled.</param>
		internal CustomTemplateException(string message, string template)
			: this(message, null, template)
		{
		}

		/// <summary>
		/// Initialises a new instance of <see cref="CustomTemplateCompilationException" />
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="sourceCode">The source code that wasn't compiled.</param>
		/// <param name="template">The source template that wasn't compiled.</param>
		internal CustomTemplateException(string message, string sourceCode, string template)
			: this(message, sourceCode, template, null)
		{
		}

		/// <summary>
		/// Initialises a new instance of <see cref="CustomTemplateCompilationException" />
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="sourceCode">The source code that wasn't compiled.</param>
		/// <param name="template">The source template that wasn't compiled.</param>
		/// <param name="model">The model.</param>
		internal CustomTemplateException(string message, string sourceCode, string template, object model)
			: this(message, sourceCode, template, model, null)
		{
		}

		/// <summary>
		/// Initialises a new instance of <see cref="CustomTemplateCompilationException" />
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="sourceCode">The source code that wasn't compiled.</param>
		/// <param name="template">The source template that wasn't compiled.</param>
		/// <param name="model">The model.</param>
		/// <param name="innerException">The inner exception.</param>
		internal CustomTemplateException(string message, string sourceCode, string template, object model, Exception innerException)
			: base(message, innerException)
		{
			SourceCode = sourceCode;
			Template = template;
			Model = model;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CustomTemplateException"/> class.
		/// </summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
		protected CustomTemplateException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

	}
}
