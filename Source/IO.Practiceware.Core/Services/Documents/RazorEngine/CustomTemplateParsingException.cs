﻿using System;
using System.Runtime.Serialization;
using Soaf.ComponentModel;

namespace IO.Practiceware.Services.Documents.RazorEngine
{
	/// <summary>
	/// Defines an exception that occurs during parsing of a template. Includes accurate line numbers
	/// </summary>
	[Serializable]
	public class CustomTemplateParsingException : CustomTemplateException
	{				
		/// <summary>
		/// Gets the column the parsing error occured
		/// </summary>
		/// <value>The column.</value>
		public int Column { get; private set; }

		/// <summary>
		/// Gets the line the parsing error occured.    
		/// </summary>
		public int Line { get; private set; }	

		/// <summary>
		/// Initializes a new instance of the <see cref="CustomTemplateParsingException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="column">The column.</param>
		/// <param name="line">The line.</param>		
		internal CustomTemplateParsingException(string message, int column, int line)
			: this(message, column, line, null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CustomTemplateParsingException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="column">The column.</param>
		/// <param name="line">The line.</param>
		/// <param name="template"></param>
		internal CustomTemplateParsingException(string message, int column, int line, string template)
			: this(message, column, line, null, template)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CustomTemplateParsingException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="column">The column.</param>
		/// <param name="line">The line.</param>
		/// <param name="sourceCode"></param>
		/// <param name="template"></param>
		internal CustomTemplateParsingException(string message, int column, int line, string sourceCode, string template)
			: this(message, column, line, sourceCode, template, null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CustomTemplateParsingException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="column">The column.</param>
		/// <param name="line">The line.</param>
		/// <param name="sourceCode"></param>
		/// <param name="template"></param>
		/// <param name="model"></param>
		internal CustomTemplateParsingException(string message, int column, int line, string sourceCode, string template, object model)
			: this(message, column, line, sourceCode, template, model, null)
		{	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CustomTemplateParsingException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="column">The column.</param>
		/// <param name="line">The line.</param>
		/// <param name="sourceCode"></param>
		/// <param name="template"></param>
		/// <param name="model"></param>
		/// <param name="innerException"></param>
		internal CustomTemplateParsingException(string message, int column, int line, string sourceCode, string template, object model, Exception innerException)
			: base(message, sourceCode, template, model, innerException)
		{
			Column = column;
			Line = line;
		}


		/// <summary>
		/// Gets the object data for serialisation.		
		/// </summary>
		/// <param name="info">The serialisation info.</param><param name="context">The streaming context.</param>
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("Column", Column);
			info.AddValue("Line", Line);
			info.AddValue("SourceCode", (SourceCode ?? string.Empty));
			info.AddValue("Template", (Template ?? string.Empty));
			info.AddValue("Model", (Model ?? Model.ToXml()));
		}
	
		/// <summary>
		/// Initialises a new instance of <see cref="T:RazorEngine.Templating.TemplateCompilationException"/> from serialised data.
		/// 
		/// </summary>
		/// <param name="info">The serialisation info.</param><param name="context">The streaming context.</param>
		protected CustomTemplateParsingException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			Column = info.GetInt32("Column");
			Line = info.GetInt32("Line");			
			SourceCode = info.GetString("SourceCode");
			Template = info.GetString("Template");
		}
	}
}
