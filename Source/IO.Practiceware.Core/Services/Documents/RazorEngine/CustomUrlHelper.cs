﻿using System;

namespace IO.Practiceware.Services.Documents.RazorEngine
{
    public class CustomUrlHelper
    {
        public string Encode(string url)
        {
            return Uri.EscapeUriString(url);
        }
    }
}