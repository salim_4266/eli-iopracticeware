﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using RazorEngine.Text;

namespace IO.Practiceware.Services.Documents.RazorEngine
{
	/// <summary>
	/// A custom TemplateService that includes accurate line numbers when running. TODO: Add implementations that wrap _defaultTemplateService.
	/// </summary>
	public class CustomTemplateService : MarshalByRefObject, ITemplateService
	{
		private readonly TemplateService _defaultTemplateService = new TemplateService();

		/// <summary>
		/// Initialises a new instance of <see cref="CustomTemplateService"/>
		/// </summary>
		/// <param name="config">The template service configuration.</param>
		public CustomTemplateService(ITemplateServiceConfiguration config)
		{
			_defaultTemplateService = new TemplateService(config);
		}

		public void Dispose()
		{
			_defaultTemplateService.Dispose();
		}

		public void AddNamespace(string ns)
		{
			_defaultTemplateService.AddNamespace(ns);
		}

		public void Compile(string razorTemplate, Type modelType, string cacheName)
		{
			try
			{
				_defaultTemplateService.Compile(razorTemplate, modelType, cacheName);
			}
			catch (TemplateCompilationException ex)
			{
				throw new CustomTemplateCompilationException(new CompilerErrorCollection(ex.Errors.ToArray()), ex.SourceCode, ex.Template, null, ex);
			}
		}

		public ITemplate CreateTemplate(string razorTemplate, Type templateType, object model)
		{
			return _defaultTemplateService.CreateTemplate(razorTemplate, templateType, model);
		}

		public IEnumerable<ITemplate> CreateTemplates(IEnumerable<string> razorTemplates, IEnumerable<Type> templateTypes, IEnumerable<object> models, bool parallel = false)
		{
			return _defaultTemplateService.CreateTemplates(razorTemplates, templateTypes, models, parallel);
		}

		public Type CreateTemplateType(string razorTemplate, Type modelType)
		{
			return _defaultTemplateService.CreateTemplateType(razorTemplate, modelType);
		}

		public IEnumerable<Type> CreateTemplateTypes(IEnumerable<string> razorTemplates, IEnumerable<Type> modelTypes, bool parallel = false)
		{
			return _defaultTemplateService.CreateTemplateTypes(razorTemplates, modelTypes, parallel);
		}

		public ITemplate GetTemplate(string razorTemplate, object model, string cacheName)
		{
			return _defaultTemplateService.GetTemplate(razorTemplate, model, cacheName);
		}

		public IEnumerable<ITemplate> GetTemplates(IEnumerable<string> razorTemplates, IEnumerable<object> models, IEnumerable<string> cacheNames, bool parallel = false)
		{
			return _defaultTemplateService.GetTemplates(razorTemplates, models, cacheNames, parallel);
		}

		public bool HasTemplate(string cacheName)
		{
			return _defaultTemplateService.HasTemplate(cacheName);
		}

		public bool RemoveTemplate(string cacheName)
		{
			return _defaultTemplateService.RemoveTemplate(cacheName);
		}

		public string Parse(string razorTemplate, object model, DynamicViewBag viewBag, string cacheName)
		{
			try
			{
				return _defaultTemplateService.Parse(razorTemplate, model, viewBag, cacheName);
			}
			catch (TemplateParsingException ex)
			{
				throw new CustomTemplateParsingException(ex.Message, ex.Column, ex.Line, null, razorTemplate, model);
			}
		}

		public string Parse<T>(string razorTemplate, object model, DynamicViewBag viewBag, string cacheName)
		{
			try
			{
				return _defaultTemplateService.Parse(razorTemplate, model, viewBag, cacheName);
			}
			catch (TemplateParsingException ex)
			{
				throw new CustomTemplateParsingException(ex.Message, ex.Column, ex.Line, null, razorTemplate, model);
			}
		}

		public IEnumerable<string> ParseMany(IEnumerable<string> razorTemplates, IEnumerable<object> models, IEnumerable<DynamicViewBag> viewBags, IEnumerable<string> cacheNames, bool parallel)
		{
			try
			{
				return _defaultTemplateService.ParseMany(razorTemplates, models, viewBags, cacheNames, parallel);
			}
			catch (TemplateParsingException ex)
			{
				throw new CustomTemplateParsingException(ex.Message, ex.Column, ex.Line);
			}
		}

		public ITemplate Resolve(string cacheName, object model)
		{
			try
			{
				return _defaultTemplateService.Resolve(cacheName, model);
			}
			catch (TemplateParsingException ex)
			{
				throw new CustomTemplateParsingException(ex.Message, ex.Column, ex.Line);
			}
		}

		public string Run(string cacheName, object model, DynamicViewBag viewBag)
		{
			return _defaultTemplateService.Run(cacheName, model, viewBag);
		}

		public string Run(ITemplate template, DynamicViewBag viewBag)
		{
			return _defaultTemplateService.Run(template, viewBag);
		}

		public IEncodedStringFactory EncodedStringFactory { get; private set; }

	}
}
