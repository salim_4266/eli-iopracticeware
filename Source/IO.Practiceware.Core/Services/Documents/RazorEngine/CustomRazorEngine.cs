﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Documents.RazorEngine;
using RazorEngine;
using RazorEngine.Configuration;
using Soaf.ComponentModel;
using Language = RazorEngine.Language;

[assembly: Component(typeof(CustomRazorEngine), Initialize = true)]

namespace IO.Practiceware.Services.Documents.RazorEngine
{
    /// <summary>
    ///     Registers customizations for RazorEngine.
    /// </summary>
    public class CustomRazorEngine
    {
        private static IPracticeRepository _practiceRepository;

        static CustomRazorEngine()
        {
            var configBuilder = new Action<IConfigurationBuilder>(config =>
                                                                  {
                                                                      config.CompileUsing(new CompilerServiceFactory()).IncludeNamespaces("IO.Practiceware").WithCodeLanguage(Language.CSharp);
                                                                      config.WithBaseTemplateType(typeof(CustomTemplateBase<>));
                                                                      config.ResolveUsing(Resolve);
                                                                  });
            var razorTemplateServiceConfiguration = new FluentTemplateServiceConfiguration(configBuilder);
            var templateService = new CustomTemplateService(razorTemplateServiceConfiguration);
            Razor.SetTemplateService(templateService);
        }

        public CustomRazorEngine(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
        }

        /// <summary>
        /// Resolves paths using template documents in the practice repository
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static string Resolve(string arg)
        {
            if (_practiceRepository == null) return null;

            // embedded template documents had / and \ replaced with . due to embedded resource naming rules
            var namesToCheck = new[] {
                arg.ToLower(), 
                arg.Replace("/", ".").Replace("\\", ".").ToLower()};

            // call to Html.Partial might not include .cshtml extension
            if (!arg.ToLower().EndsWith(".cshtml"))
            {
                namesToCheck = namesToCheck.Concat(namesToCheck.Select(n => n + ".cshtml")).ToArray();
            }

            var document = _practiceRepository.TemplateDocuments.Where(d => d.ContentType == ContentType.RazorTemplate)
                .FirstOrDefault(d => namesToCheck.Contains(d.Name.ToLower()));

            if (document == null) return null;

            return document.GetContent() as string;
        }
    }
}