﻿using RazorEngine.Templating;

namespace IO.Practiceware.Services.Documents.RazorEngine
{
    public class CustomTemplateBase<T> : TemplateBase<T>
    {
        private readonly CustomHtmlHelper _htmlHelper = new CustomHtmlHelper();
        private readonly CustomUrlHelper _urlHelper = new CustomUrlHelper();

        public CustomUrlHelper Url
        {
            get { return _urlHelper; }
        }

        public CustomHtmlHelper Html
        {
            get { return _htmlHelper; }
        }
    }
}