﻿using RazorEngine;
using RazorEngine.Compilation;

namespace IO.Practiceware.Services.Documents.RazorEngine
{
	/// <summary>
	/// A custom compiler service factory that uses our internal CompilerService.
	/// </summary>
	public class CompilerServiceFactory : ICompilerServiceFactory
	{
		private readonly DefaultCompilerServiceFactory _defaultCompilerServiceFactory = new DefaultCompilerServiceFactory();

		public ICompilerService CreateCompilerService(Language language)
		{
			if (language == Language.CSharp) return new CompilerService();
			return _defaultCompilerServiceFactory.CreateCompilerService(language);
		}
	}
}
