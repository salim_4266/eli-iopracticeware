﻿using RazorEngine;
using RazorEngine.Templating;
using RazorEngine.Text;

namespace IO.Practiceware.Services.Documents.RazorEngine
{
    public class CustomHtmlHelper
    {
        public IEncodedString Partial(string viewName)
        {
            ITemplate template = Razor.Resolve(viewName);

            var ec = new ExecuteContext();

            var result = new RawString(template.Run(ec));

            return result;
        }

        public IEncodedString Partial(string viewName, object model)
        {
            if (model != null)
            {
                ITemplate template = Razor.Resolve(viewName, model);

                var ec = new ExecuteContext();

                var result = new RawString(template.Run(ec));

                return result;
            }
            return null;
        }
    }
}