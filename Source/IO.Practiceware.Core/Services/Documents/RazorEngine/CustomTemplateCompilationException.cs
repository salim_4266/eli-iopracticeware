﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;

namespace IO.Practiceware.Services.Documents.RazorEngine
{
	[Serializable]
	public class CustomTemplateCompilationException : CustomTemplateException
	{
		/// <summary>
		/// Gets the set of compiler errors.		
		/// </summary>
		public ReadOnlyCollection<CompilerError> Errors { get; private set; }

		private readonly string _message;
		public override string Message
		{
			get
			{
				return _message;
			}
		}

		/// <summary>
		/// Initialises a new instance of <see cref="CustomTemplateCompilationException"/>		
		/// </summary>
		/// <param name="errors">The set of compiler errors.</param>
		/// <param name="sourceCode">The source code that wasn't compiled.</param>
		/// <param name="template">The source template that wasn't compiled.</param>
		internal CustomTemplateCompilationException(CompilerErrorCollection errors, string sourceCode, string template)
			: this(errors, sourceCode, template, null)
		{
		}


		/// <summary>
		/// Initialises a new instance of <see cref="CustomTemplateCompilationException"/>		
		/// </summary>
		/// <param name="errors">The set of compiler errors.</param>
		/// <param name="sourceCode">The source code that wasn't compiled.</param>
		/// <param name="template">The source template that wasn't compiled.</param>
		/// <param name="model"></param>
		internal CustomTemplateCompilationException(CompilerErrorCollection errors, string sourceCode, string template, object model)
			: this(errors, sourceCode, template, model, null)
		{
		}

		/// <summary>
		/// Initialises a new instance of <see cref="CustomTemplateCompilationException"/>		
		/// </summary>
		/// <param name="errors">The set of compiler errors.</param>
		/// <param name="sourceCode">The source code that wasn't compiled.</param>
		/// <param name="template">The source template that wasn't compiled.</param>			
		/// <param name="model"></param>
		/// <param name="innerException"></param>
		internal CustomTemplateCompilationException(CompilerErrorCollection errors, string sourceCode, string template, object model, Exception innerException)
			: base("Unable to compile the template.", sourceCode, template, model, innerException)
		{
			Errors = new ReadOnlyCollection<CompilerError>(errors.Cast<CompilerError>().ToList());
			_message = ConstructMessage();
		}

		/// <summary>
		/// Gets the object data for serialisation.		
		/// </summary>
		/// <param name="info">The serialisation info.</param>
		/// <param name="context">The streaming context.</param>
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);

			info.AddValue("Count", Errors.Count);
			for (int index = 0; index < Errors.Count; ++index)
			{
				info.AddValue("Errors[" + index + "]", Errors[index]);
			}
			info.AddValue("SourceCode", (SourceCode ?? string.Empty));
			info.AddValue("Template", (Template ?? string.Empty));
			info.AddValue("Model", (Model ?? Model.ToXml()));
		}

		/// <summary>
		/// Initialises a new instance of <see cref="T:RazorEngine.Templating.TemplateCompilationException"/> from serialised data.
		/// 
		/// </summary>
		/// <param name="info">The serialisation info.</param><param name="context">The streaming context.</param>
		protected CustomTemplateCompilationException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			var int32 = info.GetInt32("Count");
			var list = new List<CompilerError>();
			var type = typeof(CompilerError);
			for (var index = 0; index < int32; ++index)
				list.Add((CompilerError)info.GetValue("Errors[" + index + "]", type));

			Errors = new ReadOnlyCollection<CompilerError>(list);
			SourceCode = info.GetString("SourceCode");
			Template = info.GetString("Template");
		}

		private string ConstructMessage()
		{
			var length = Template != null ? Template.Length : 1024;
			var exceptionMessage = new StringBuilder(length);
			exceptionMessage.AppendLine("\t Message: {0}".FormatWith(base.Message));

			if (Errors != null)
			{
				exceptionMessage.AppendLine();
				exceptionMessage.AppendLine("\t Errors:");
				exceptionMessage.AppendLine(Errors.Select(error => string.Format("Error Number {0}, Occured on Line Number {1}, Column Number {2}, {3}",
											error.ErrorNumber, error.Line, error.Column, error.ErrorText))
											.Join(Environment.NewLine, true)
											);
			}

			if (Template != null)
			{
				exceptionMessage.AppendLine();
				exceptionMessage.AppendLine("\t Template: ");
				exceptionMessage.AppendLine(Template);
			}

			if (Model != null)
			{
				exceptionMessage.AppendLine();
				exceptionMessage.AppendLine("\t Model: ");
				exceptionMessage.AppendLine(Model.ToXml());
			}

			return exceptionMessage.ToString();
		}

		public override string ToString()
		{
			return ConstructMessage();
		}

	}
}
