﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Razor;
using System.Web.Razor.Generator;
using Microsoft.CSharp;
using RazorEngine.Compilation;
using RazorEngine.Compilation.CSharp;
using RazorEngine.Templating;
using RazorEngineHost = RazorEngine.Compilation.RazorEngineHost;

namespace IO.Practiceware.Services.Documents.RazorEngine
{
	/// <summary>
	///     A custom CSharpDirectCompilerService that includes line numbers for compilation exceptions.
	/// </summary>
	public class CompilerService : CSharpDirectCompilerService
	{
		private readonly CodeDomProvider _codeDomProvider = new CSharpCodeProvider();

		private bool _disposed;

		/// <summary>
		///     Compiles the type defined in the specified type context.
		/// </summary>
		/// <param name="context">The type context which defines the type to compile.</param>
		/// <returns>
		///     The compiled type.
		/// </returns>
		public override Tuple<Type, Assembly> CompileType(TypeContext context)
		{
			if (context == null)
				throw new ArgumentNullException("context");						

			var tuple = Compile(context);
			var compilerResults = tuple.Item1;

			if (compilerResults.Errors != null && compilerResults.Errors.Count > 0)
			{
				throw new CustomTemplateCompilationException(compilerResults.Errors, tuple.Item2, context.TemplateContent);
			}

			return Tuple.Create(compilerResults.CompiledAssembly.GetType("CompiledRazorTemplates.Dynamic." + context.ClassName), compilerResults.CompiledAssembly);
		}		

		protected override void Dispose(bool disposing)
		{
			_disposed = true;
			base.Dispose(disposing);
		}		

		/// <summary>
		///     Creates the compile results for the specified <see cref="T:RazorEngine.Compilation.TypeContext" />.
		/// </summary>
		/// <param name="context">The type context.</param>
		/// <returns>
		///     The compiler results.
		/// </returns>
		private Tuple<CompilerResults, string> Compile(TypeContext context)
		{
			if (_disposed)
				throw new ObjectDisposedException(GetType().Name);

			var codeCompileUnit = GetCodeCompileUnit(context.ClassName, context.TemplateContent, context.Namespaces, context.TemplateType, context.ModelType); 

			var compileParameters = new CompilerParameters
									{
										GenerateInMemory = true,
										GenerateExecutable = false,										
										IncludeDebugInformation = true,
										CompilerOptions = "/target:library /optimize",										
										TreatWarningsAsErrors = false
									};

			var source = CompilerServicesUtility.GetLoadedAssemblies()
				.Where(a => !a.IsDynamic && File.Exists(a.Location))
				.GroupBy(a => a.GetName().Name)
				.Select(grp => grp.First(y => y.GetName().Version == grp.Max(x => x.GetName().Version)))
				.Select(a => a.Location)
				.Concat(IncludeAssemblies() ?? Enumerable.Empty<string>())
				.Where(a => !string.IsNullOrWhiteSpace(a))
				.Distinct(StringComparer.OrdinalIgnoreCase);

			compileParameters.ReferencedAssemblies.AddRange(source.ToArray());

			var str = (string)null;

			if (Debug)
			{
				var sb = new StringBuilder();
				using (var stringWriter = new StringWriter(sb, CultureInfo.InvariantCulture))
				{
					_codeDomProvider.GenerateCodeFromCompileUnit(codeCompileUnit, stringWriter, new CodeGeneratorOptions());
					str = sb.ToString();
				}
			}

			return Tuple.Create(_codeDomProvider.CompileAssemblyFromDom(compileParameters, codeCompileUnit), str);
		}


		/// <summary>
		///		Creates a <see cref="T:RazorEngine.Compilation.RazorEngineHost"/> used for class generation.		
		/// </summary>
		/// <param name="templateType">The template base type.</param><param name="modelType">The model type.</param><param name="className">The class name.</param>
		/// <returns>
		///		An instance of <see cref="T:RazorEngine.Compilation.RazorEngineHost"/>.
		/// </returns>
		private RazorEngineHost CreateHost(Type templateType, Type modelType, string className)
		{
			var razorEngineHost = new RazorEngineHost(CodeLanguage, MarkupParserFactory);
			razorEngineHost.DefaultBaseTemplateType = templateType;
			razorEngineHost.DefaultModelType = modelType;
			razorEngineHost.DefaultBaseClass = BuildTypeName(templateType);
			razorEngineHost.DefaultClassName = className;
			razorEngineHost.DefaultNamespace = "CompiledRazorTemplates.Dynamic";			
			razorEngineHost.GeneratedClassContext = new GeneratedClassContext("Execute", "Write", "WriteLiteral", "WriteTo", "WriteLiteralTo", "RazorEngine.Templating.TemplateWriter", "DefineSection")
			{
				ResolveUrlMethodName = "ResolveUrl"
			};
			return razorEngineHost;
		}

		/// <summary>
		///		Gets any required namespace imports.
		/// </summary>
		/// <param name="templateType">The template type.</param><param name="otherNamespaces">The requested set of namespace imports.</param>
		/// <returns>
		///		A set of namespace imports.
		/// </returns>
		private static IEnumerable<string> GetNamespaces(Type templateType, IEnumerable<string> otherNamespaces)
		{
			return templateType.GetCustomAttributes(typeof(RequireNamespacesAttribute), true).Cast<RequireNamespacesAttribute>().SelectMany(a => a.Namespaces).Concat(otherNamespaces).Distinct();
		}

		/// <summary>
		///		Gets the code compile unit used to compile a type.		
		/// </summary>
		/// <param name="className">The class name.</param><param name="template">The template to compile.</param><param name="namespaceImports">The set of namespace imports.</param><param name="templateType">The template type.</param><param name="modelType">The model type.</param>
		/// <returns>
		///		A <see cref="T:System.CodeDom.CodeCompileUnit"/> used to compile a type.
		/// </returns>
		public new CodeCompileUnit GetCodeCompileUnit(string className, string template, ISet<string> namespaceImports, Type templateType, Type modelType)
		{
			if (string.IsNullOrEmpty(className))
				throw new ArgumentException("Class name is required.");
			if (string.IsNullOrEmpty(template))
				throw new ArgumentException("Template is required.");

			namespaceImports = namespaceImports ?? new HashSet<string>();
			templateType = templateType ?? (modelType == (Type)null ? typeof(TemplateBase) : typeof(TemplateBase<>));
			var host = CreateHost(templateType, modelType, className);

			foreach (var str in GetNamespaces(templateType, namespaceImports))
				host.NamespaceImports.Add(str);

			// NOTE: This is the important line change that made the generated code produce the mapped line numbers from the original template file (the .cshtml file)
			var generatorResult = GetGeneratorResult(host, template, "__ioDynamicFile" );

			var codeType = generatorResult.GeneratedCode.Namespaces[0].Types[0];
			if (modelType != null && CompilerServicesUtility.IsAnonymousType(modelType))
				codeType.CustomAttributes.Add(new CodeAttributeDeclaration(new CodeTypeReference(typeof(HasDynamicModelAttribute))));

			GenerateConstructors(CompilerServicesUtility.GetConstructors(templateType), codeType);
			Inspect(generatorResult.GeneratedCode);
			return generatorResult.GeneratedCode;
		}

		/// <summary>
		///		Generates any required contructors for the specified type.
		/// </summary>
		/// <param name="constructors">The set of constructors.</param><param name="codeType">The code type declaration.</param>
		private static void GenerateConstructors(IEnumerable<ConstructorInfo> constructors, CodeTypeDeclaration codeType)
		{
			if (constructors == null || !constructors.Any())
				return;

			foreach (var codeConstructor in codeType.Members.OfType<CodeConstructor>().ToArray())
				codeType.Members.Remove(codeConstructor);

			foreach (var constructorInfo in constructors)
			{
				var codeConstructor = new CodeConstructor();
				codeConstructor.Attributes = MemberAttributes.Public;
				foreach (var parameterInfo in constructorInfo.GetParameters())
				{
					codeConstructor.Parameters.Add(new CodeParameterDeclarationExpression(parameterInfo.ParameterType, parameterInfo.Name));
					codeConstructor.BaseConstructorArgs.Add(new CodeSnippetExpression(parameterInfo.Name));
				}
				codeType.Members.Add(codeConstructor);
			}
		}

		/// <summary>
		/// Gets the generator result.
		/// 
		/// </summary>
		/// <param name="host">The razor engine host.</param><param name="template">The template.</param>
		/// <param name="fileName"></param>
		/// <returns>
		/// The generator result.
		/// </returns>
		private static GeneratorResults GetGeneratorResult(RazorEngineHost host, string template, string fileName)
		{
			using (var stringReader = new StringReader(template))
				return new RazorTemplateEngine(host).GenerateCode(stringReader, host.DefaultClassName, host.DefaultNamespace, fileName);
		}

	}
}