﻿using IO.Practiceware.Model;
using IO.Practiceware.Services.Documents;
using Soaf.ComponentModel;
using System.Linq;

[assembly: Component(typeof (PrintingService), typeof (IPrintingService))]

namespace IO.Practiceware.Services.Documents
{
    public interface IPrintingService
    {
        string GetPrinterUncPath(string documentName, string serviceLocationShortName);
    }

    internal class PrintingService : IPrintingService
    {
        private readonly IPracticeRepository _practiceRepository;

        public PrintingService(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
        }

        #region IPrintingService Members

        public string GetPrinterUncPath(string documentName, string serviceLocationShortName)
        {
            return _practiceRepository.Printers
                .Where(p => p.TemplateDocuments.Any(d => d.Name == documentName) && p.ServiceLocation.ShortName == serviceLocationShortName)
                .Select(p => p.UncPath).FirstOrDefault();
        }

        #endregion
    }
}