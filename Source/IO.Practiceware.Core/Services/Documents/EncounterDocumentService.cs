﻿using IO.Practiceware.Model;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Validation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;

[assembly: Component(typeof(EncounterDocumentService), typeof(IEncounterDocumentService))]

namespace IO.Practiceware.Services.Documents
{
    public enum EncounterDocumentType
    {
        [Display(Name = "Exam Documents")]
        ExamDocuments = 1,

        [Display(Name = "Surgery Documents")]
        SurgeryDocuments = 2,
    }

    public interface IEncounterDocumentService
    {
        /// <summary>
        ///     Creates documents for an encounter
        /// </summary>
        /// <param name="encounterId"></param>
        /// <param name="encounterDocumentTypes"></param>
        /// <returns></returns>
        [NotNull]
        IEnumerable<BinaryDocument> CreateDocuments(int encounterId, EncounterDocumentType[] encounterDocumentTypes = null);
    }

    internal class EncounterDocumentService : IEncounterDocumentService
    {
        private readonly IDocumentGenerationService _documentGenerationService;
        private readonly IMapper<Model.Encounter, Encounter> _encounterDocumentMapper;
        private readonly IPracticeRepository _practiceRepository;

        public EncounterDocumentService(IPracticeRepository practiceRepository, IMapper<Model.Encounter, Encounter> encounterDocumentMapper, IDocumentGenerationService documentGenerationService)
        {
            _practiceRepository = practiceRepository;
            _encounterDocumentMapper = encounterDocumentMapper;
            _documentGenerationService = documentGenerationService;
        }

        #region IEncounterDocumentService Members

        public IEnumerable<BinaryDocument> CreateDocuments(int encounterId, EncounterDocumentType[] encounterDocumentTypes = null)
        {
            var documents = new List<BinaryDocument>();
            Model.Encounter encounter = GetEncounter(encounterId);
            if (encounter != null)
            {
                // Maps Model Encounter data into Documents Encounter
                Encounter viewModel = _encounterDocumentMapper.Map(encounter);

                if (encounterDocumentTypes == null) encounterDocumentTypes = Enums.GetValues<EncounterDocumentType>();

                if (viewModel.PatientSurgery == null) encounterDocumentTypes = encounterDocumentTypes.Except(new[] { EncounterDocumentType.SurgeryDocuments }).ToArray();

                IEnumerable<int> ids =
                    encounterDocumentTypes.Select(
                        i => (int) i.ToString().ToEnum<TemplateDocumentCategory>());

                IEnumerable<TemplateDocument> modelDocumentTemplates =
                    _practiceRepository.TemplateDocuments.Where(d => ids.Contains(d.TemplateDocumentCategoryId)).OrderBy(d => d.Name).ToList();

                // dictionary of template name paired with merged document bytes
                foreach (TemplateDocument modelDocumentTemplate in modelDocumentTemplates)
                {
                    string templateName = modelDocumentTemplate.Name;
                    byte[] templateFile = FileManager.Instance.ReadContents(Path.Combine(Directories.Templates, templateName), true);
                    if (templateFile != null) documents.Add(new BinaryDocument { Name = templateName, Content = _documentGenerationService.PerformMailMerge(templateFile, viewModel, null, false, false) });
                }
            }
            return documents;
        }

        #endregion

        private Model.Encounter GetEncounter(int encounterId)
        {
            Model.Encounter encounter = _practiceRepository.Appointments.OfType<UserAppointment>().Select(ua => ua.Encounter).WithId(encounterId);
            if (encounter != null)
            {
                _practiceRepository.AsQueryableFactory().Load(encounter,
                                                              e => e.Appointments.OfType<UserAppointment>().Select(ua => ua.User),
                                                              e => e.Patient,
                                                              e => e.PatientSurgery,
                                                              e => e.PatientSurgery.SurgeryType,
                                                              e => e.PatientSurgery.PatientSurgeryQuestionAnswers,
                                                              e => e.PatientSurgery.PatientSurgeryQuestionChoices,
                                                              e => e.PatientSurgery.PatientSurgeryQuestionAnswers.Select(a => a.Question),
                                                              e => e.PatientSurgery.PatientSurgeryQuestionChoices.Select(c => c.Choice),
                                                              e => e.PatientSurgery.PatientSurgeryQuestionChoices.Select(c => c.Choice.Question)
                    );
            }
            return encounter;
        }
    }
}