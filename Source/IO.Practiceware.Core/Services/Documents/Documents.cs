﻿using IO.Practiceware.Configuration;
using Soaf;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

namespace IO.Practiceware.Services.Documents
{
    /// <summary>
    ///     Well known application directories.
    /// </summary>
    public static class Directories
    {
        private const string TemplatesPath = "Templates\\";

        private const string PatientImagesPath = "MyScan\\";

        private const string ReportsPath = "Reports\\";

        /// <summary>
        /// Gets the Templates directory path. Includes trailing slash for concatenation convenience.
        /// </summary>
        /// <value>
        /// The templates.
        /// </value>
        public static string Templates
        {
            get { return Path.Combine(ConfigurationManager.ServerDataPath, TemplatesPath); }
        }

        /// <summary>
        /// Gets the Patient Images directory path. Includes trailing slash for concatenation convenience.
        /// </summary>
        /// <value>
        /// The patient images.
        /// </value>
        public static string PatientImages
        {
            get { return Path.Combine(ConfigurationManager.ServerDataPath, PatientImagesPath); }
        }

        /// <summary>
        /// Gets the Reports directory path. Includes trailing slash for concatenation convenience.
        /// </summary>
        /// <value>
        /// The reports.
        /// </value>
        public static string Reports
        {
            get { return Path.Combine(ConfigurationManager.ServerDataPath, ReportsPath); }
        }

    }

    /// <summary>
    ///     A document with binary content.
    /// </summary>
    [DataContract]
    public class BinaryDocument : Document
    {
        public byte[] Content { get; set; }
    }

    /// <summary>
    /// A document composed of a list of html pages.
    /// </summary>
    [DataContract]
    public class HtmlDocument : Document
    {
        public HtmlDocument()
        {
            Content = new List<string>();
        }

        [DataMember]
        public List<string> Content
        {
            get { return Data != null ? Data.CastTo<List<string>>() : null; }
            set { Data = value; }
        }
    }

    /// <summary>
    /// A base document class that represents some document with data.
    /// </summary>
    [DataContract]
    public abstract class Document
    {
        [DataMember]
        public string Name { get; set; }


        // Cannot put DataMember Attribute on Data because it is of type object
        protected object Data { get; set; }
    }
}