﻿using IO.Practiceware.Services.Documents;
using IO.Practiceware.Storage;
using Soaf;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Soaf.IO;
using File = System.IO.File;

namespace IO.Practiceware.Services.Utilities
{
    public class WkHtmlToPdfWrapperService
    {
        private const string HtmlToPdfExePath = @"wkhtmltopdf.exe";

        public static byte[] GeneratePdf(IEnumerable<string> html)
        {
            return GeneratePdf(html, 0, 0, 0, 0);
        }
        
        public static BinaryDocument GeneratePdf(IEnumerable<string> html, bool returnDocument = true)
        {
            if (returnDocument) return new BinaryDocument { Content = GeneratePdf(html) };
            return null;
        }
        
        /// <summary>
        /// Produces a single pdf from a list of html pages
        /// </summary>
        /// <param name="html"></param>
        /// <param name="leftMargin"></param>
        /// <param name="topMargin"></param>
        /// <param name="rightMargin"></param>
        /// <param name="bottomMargin"></param>
        /// <returns></returns>
        public static byte[] GeneratePdf(IEnumerable<string> html, int leftMargin, int topMargin, int rightMargin, int bottomMargin)
        {
            CheckApplicationExists();

            var htmlPages = html.ToList();
            if (!htmlPages.Any()) return null;
            
            var filePaths = new List<string>();

            foreach (var h in htmlPages)
            {
                var randomFilePath = FileManager.Instance.GetTempPathName() + ".html"; // Temp file which ends with .html
                FileManager.Instance.CommitContents(randomFilePath, h);
                filePaths.Add(randomFilePath);
            }

            byte[] content;
            var success = GeneratePdf(filePaths, out content, leftMargin, topMargin, rightMargin, bottomMargin);

            foreach (var filePath in filePaths)
            {
                FileManager.Instance.Delete(filePath);
            }

            if (!success)
            {
                return null;
            }

            return content;
        }

        private const string ApplicationPath = "IO.Practiceware.ExternalApplications.HtmlToPdfConverter.";
        private static void CheckApplicationExists()
        {
            var assembly = Assembly.GetExecutingAssembly();

            foreach (var name in assembly.GetManifestResourceNames().Where(r => r.StartsWith(ApplicationPath)))
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), name.Replace(ApplicationPath, string.Empty));
                // We are writing to application folder, so use FileSystem directly
                if (!File.Exists(path)) // check to see if file exists, if it doesn't then write the file to disk
                {
                    using (var stream = assembly.GetManifestResourceStream(name))
                    {
                        FileSystem.TryWriteFile(path, stream.ToArray());
                    }
                }
            }
        }

        private static bool GeneratePdf(IEnumerable<string> filePaths, out byte[] pdf, int? leftMargin = null, int? topMargin = null, int? rightMargin = null, int? bottomMargin = null)
        {
            pdf = new byte[] { };
            Process p = null;
            var psi = new ProcessStartInfo();

            try
            {
                psi.FileName = Path.Combine(Directory.GetCurrentDirectory(), HtmlToPdfExePath);
                psi.WorkingDirectory = Path.GetDirectoryName(psi.FileName);

                // run the conversion utility
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;
                psi.RedirectStandardInput = true;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardError = true;

                var arguments = "-q -n --disable-smart-shrinking";

                if (leftMargin.HasValue)
                {
                    arguments += " -L " + leftMargin;
                }

                if (topMargin.HasValue)
                {
                    arguments += " -T " + topMargin;
                }

                if (rightMargin.HasValue)
                {
                    arguments += " -R " + rightMargin;
                }

                if (bottomMargin.HasValue)
                {
                    arguments += " -B " + bottomMargin;
                }


                arguments += " -s Letter ";

                // note: that we tell wkhtmltopdf to be quiet and not run scripts
                psi.Arguments = arguments;

                foreach (var path in filePaths)
                {
                    var locallyCachedPath = FileManager.Instance.Read(path);
                    psi.Arguments += " \"" + locallyCachedPath + "\"";
                }

                psi.Arguments += " -";

                p = Process.Start(psi);


                pdf = GetBytes(p.StandardOutput.BaseStream);
                p.StandardOutput.Close();
                p.WaitForExit(15000);
                int returnCode = p.ExitCode;

                return returnCode == 0;
            }
            catch
            {
                return false;

            }
            finally
            {
                if (p != null)
                {
                    p.Dispose();
                }
            }
        }

        private static byte[] GetBytes(Stream input)
        {
            byte[] buffer = new byte[65536];
            byte[] file = new byte[65536];
            using (var ms = new MemoryStream())
            {
                while (true)
                {
                    int read = input.Read(buffer, 0, buffer.Length);

                    if (read <= 0)
                    {
                        break;
                    }
                    ms.Write(buffer, 0, read);
                }
                file = ms.ToArray();
            }

            return file;
        }
    }
}
