﻿using IO.Practiceware.Model;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IO.Practiceware.Services.PatientSearch
{
    [DataContract]
    public class PatientSearchResult
    {
        [DataMember]
        public virtual string PriorId { get; set; }

        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Prefix { get; set; }

        [DataMember]
        public virtual string FirstName { get; set; }

        [DataMember]
        public virtual string MiddleName { get; set; }

        [DataMember]
        public virtual string LastName { get; set; }

        [DataMember]
        public virtual string Suffix { get; set; }

        [DataMember]
        public virtual string DisplayName { get; set; }

        [DataMember]
        public virtual DateTime? DateOfBirth { get; set; }

        [DataMember]
        public virtual List<string> ListOfTypeName { get; set; }

        [DataMember]
        public virtual string PrimaryAddress { get; set; }          

        [DataMember]
        public virtual string HomePhone { get; set; }

        [DataMember]
        public virtual string CellPhone { get; set; }

        [DataMember]
        public virtual string WorkPhone { get; set; }

        [DataMember]
        public virtual DateTime? NextAppointmentDate { get; set; }

        [DataMember]
        public virtual DateTime? LastAppointmentDate { get; set; }

        [DataMember]
        public virtual string PracticeDoctor { get; set; }

        [DataMember]
        public virtual string ReferringDoctor { get; set; }

        [DataMember]
        public virtual string PrimaryCareDoctor { get; set; }

        [DataMember]
        public virtual Gender Gender { get; set; }

        [DataMember]
        public virtual string EmailAddress { get; set; }

        [DataMember]
        public virtual bool IsClinical { get; set; }       
    }
}
