﻿using System.Globalization;
using IO.Practiceware.Model;
using IO.Practiceware.Services.PatientSearch;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Transactions;

[assembly: Component(typeof(PatientSearchService), typeof(IPatientSearchService))]
[assembly: Component(typeof(PatientSearchResultMap), typeof(IMap<Patient, PatientSearchResult>))]

namespace IO.Practiceware.Services.PatientSearch
{
    [DataContract]
    public enum TextSearchMode
    {
        [EnumMember]
        [Display(Name = "Begins With")]
        [Description("Use this option to search for results that begin with the text entered")]
        BeginsWith,

        [EnumMember]
        [Display(Name = "Contains")]
        [Description("Use this option to search for results that contain the text entered anywhere within the chosen details")]
        Contains
    }

    [Flags]
    [DataContract]
    public enum SearchFields
    {
        [EnumMember]
        None = 0x0,

        [EnumMember]
        [Display(Name = "First Name")]
        FirstName = 0x1,

        [EnumMember]
        [Display(Name = "Last Name")]
        LastName = 0x10,

        [EnumMember]
        [Display(Name = "ID #")]
        PatientId = 0x100,

        [EnumMember]
        [Display(Name = "Prior ID #")]
        PriorPatientCode = 0x1000,

        [EnumMember]
        [Display(Name = "Phone #")]
        PhoneNumber = 0x10000,

        [EnumMember]
        [Display(Name = "Birth Date")]
        DateOfBirth = 0x100000,

        [EnumMember]
        [Display(Name = "Non Clinical Only")]
        [Description("Set this flag to include only non-clinical patients in the search")]
        IsNonClinicalOnly = 0x1000000,

        [EnumMember]
        [Display(Name = "Active Only")]
        [Description("Set this flag to include only active patients in the search")]
        IsActiveOnly = 0x10000000,
    }

    [DataContract]
    [Flags]
    public enum IncludedResultFields
    {
        PatientPhoneNumbers = 0x1,
        PatientAddresses = 0x10,
        EmailAddresses = 0x100,
        Appointments = 0x1000,
        DefaultUser = 0x10000,
        ExternalProviders = 0x100000,
        PatientTags = 0x1000000,
        All = PatientPhoneNumbers | PatientAddresses | EmailAddresses | Appointments | DefaultUser | ExternalProviders | PatientTags
    }

    public interface IPatientSearchService
    {
        /// <summary>
        /// Performs a patient search returning fully populated results
        /// </summary>
        /// <param name="textSearchMode"></param>
        /// <param name="searchFields"></param>
        /// <param name="searchText"></param>
        /// <param name="includedResultFields"> </param>
        /// <returns></returns>
        ObservableCollection<PatientSearchResult> Search(TextSearchMode textSearchMode, SearchFields searchFields, string searchText, IncludedResultFields includedResultFields = IncludedResultFields.All);

        IEnumerable<PatientSearchResult> GetPatients(IEnumerable<int> patientIds);
    }

    [SupportsTransactionScopes]
    public class PatientSearchService : IPatientSearchService
    {
        private readonly IPracticeRepository _practiceRepository;
        private readonly IMapper<Patient, PatientSearchResult> _patientMapper;

        public PatientSearchService(IPracticeRepository practiceRepository, IMapper<Patient, PatientSearchResult> patientMapper)
        {
            _practiceRepository = practiceRepository;
            _patientMapper = patientMapper;
        }

        [TransactionScope(TransactionScopeOption.Suppress, IsolationLevel.ReadUncommitted)]
        public virtual ObservableCollection<PatientSearchResult> Search(TextSearchMode textSearchMode, SearchFields searchFields, string searchText, IncludedResultFields includedResultFields)
        {

            var searchFieldsParameter = Enums.GetValues<SearchFields>().Where(v => v != SearchFields.None && v != SearchFields.IsActiveOnly && v != SearchFields.IsNonClinicalOnly && searchFields.HasFlag(v)).Join(",");

            var searchTermsParameter = PatientSearchExtensions.GetSearchTerms(searchText, searchFields.HasFlag(SearchFields.DateOfBirth)).Join(" ");

            var isNonClinicalOnly = searchFields.HasFlag(SearchFields.IsNonClinicalOnly);

            var isActiveOnly = searchFields.HasFlag(SearchFields.IsActiveOnly);

            var patients = _practiceRepository.GetRankedPatientSearchResults(searchFieldsParameter, searchTermsParameter, textSearchMode.ToString(), isNonClinicalOnly ? false : new bool?(), isActiveOnly).ToArray();

            _practiceRepository.AsQueryableFactory().Load(patients, includedResultFields.GetPropertiesToLoad().ToArray());

            return patients.Select(_patientMapper.Map).ToExtendedObservableCollection();
        }

        public IEnumerable<PatientSearchResult> GetPatients(IEnumerable<int> patientIds)
        {
            var patients = _practiceRepository.Patients.Where(p => patientIds.Contains(p.Id)).ToList();
            return patients.Select(_patientMapper.Map).ToExtendedObservableCollection();
        }
    }

    public static class PatientSearchExtensions
    {
        public static IEnumerable<Expression<Func<Patient, object>>> GetPropertiesToLoad(this IncludedResultFields includedResultFields)
        {
            var propertiesToLoad = new List<Expression<Func<Patient, object>>>();
            if (includedResultFields.HasFlag(IncludedResultFields.Appointments))
            {
                propertiesToLoad.Add(p => p.Encounters.Select(e => e.Appointments));
            }
            if (includedResultFields.HasFlag(IncludedResultFields.DefaultUser))
            {
                propertiesToLoad.Add(p => p.DefaultUser);
            }
            if (includedResultFields.HasFlag(IncludedResultFields.EmailAddresses))
            {
                propertiesToLoad.Add(p => p.PatientEmailAddresses);
            }
            if (includedResultFields.HasFlag(IncludedResultFields.ExternalProviders))
            {
                propertiesToLoad.Add(p => p.ExternalProviders.Select(ep => ep.ExternalProvider));
            }
            if (includedResultFields.HasFlag(IncludedResultFields.PatientAddresses))
            {
                propertiesToLoad.Add(p => p.PatientAddresses.Select(pa => pa.StateOrProvince));
            }
            if (includedResultFields.HasFlag(IncludedResultFields.PatientPhoneNumbers))
            {
                propertiesToLoad.Add(p => p.PatientPhoneNumbers);
            }
            if (includedResultFields.HasFlag(IncludedResultFields.PatientTags))
            {
                propertiesToLoad.Add(p => p.PatientTags.Select(pt => pt.Tag));
            }

            return propertiesToLoad;
        }

        public static IEnumerable<string> GetSearchTerms(string searchText, bool formatDateFields = false)
        {
            #region characters to split and remove

            var charactersToSplit = new[]
                {
                    ',',
                    ' '
                };

            #endregion

            foreach (var splitText in searchText.Split(charactersToSplit).Where(s => s.IsNotNullOrEmpty()))
            {
                DateTime dateTime;
                if (formatDateFields && DateTime.TryParseExact(splitText.Trim(), DateTimes.DateFormats, CultureInfo.CurrentUICulture, DateTimeStyles.AssumeLocal, out dateTime))
                {
                    // format as a yyyyMMdd so SQL can handle it
                    yield return dateTime.ToString("yyyyMMdd");
                }
                else
                {
                    yield return splitText;
                }
            }
        }
    }

    public class PatientSearchResultMap : IMap<Patient, PatientSearchResult>
    {
        public PatientSearchResultMap()
        {
            #region helper func getAddressString

            Func<PatientAddress, string> getAddressString = address =>
            {
                if (address == null) return null;

                var mainLine = new[] { address.Line1, address.Line2, address.Line3 }.Join(" ", true);
                var cityState = String.Format("{0}, {1}", address.City, address.StateOrProvince.Abbreviation);
                return new[] { mainLine, cityState, address.PostalCode }.Join(Environment.NewLine);
            };

            #endregion

            Members = this.CreateMembers(i => new PatientSearchResult
            {
                CellPhone = i.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Cell, NotFoundBehavior.None).IfNotDefault(p => p.ToString()),
                DateOfBirth = i.DateOfBirth,
                DisplayName = i.DisplayName,
                EmailAddress = i.PatientEmailAddresses.FirstOrDefault().IfNotDefault(j => j.Value),
                FirstName = i.FirstName,
                Gender = i.Gender.HasValue ? i.Gender.Value : Gender.Decline,
                HomePhone = i.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home, NotFoundBehavior.None).IfNotDefault(p => p.ToString()),
                Id = i.Id,
                LastAppointmentDate = i.LastAppointmentDateTime,
                LastName = i.LastName,
                ListOfTypeName = i.PatientTags.Select(t => t.Tag.DisplayName).ToList(),
                MiddleName = i.MiddleName,
                NextAppointmentDate = i.NextAppointmentDateTime,
                PracticeDoctor = (i.DefaultUser != null) ? i.DefaultUser.UserName : null,
                Prefix = i.Prefix,
                PrimaryAddress = getAddressString(i.PatientAddresses.WithAddressType(PatientAddressTypeId.Home)),
                PrimaryCareDoctor = (i.ExternalProviders.Any(p => p.IsPrimaryCarePhysician))
                                        ? i.ExternalProviders.First(p => p.IsPrimaryCarePhysician).ExternalProvider.DisplayName
                                        : null,
                PriorId = i.PriorPatientCode,
                ReferringDoctor = (i.ExternalProviders.Any(p => p.IsReferringPhysician))
                                    ? i.ExternalProviders.First(p => p.IsReferringPhysician).ExternalProvider.DisplayName
                                    : null,
                Suffix = i.Suffix,
                WorkPhone = i.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business, NotFoundBehavior.None).IfNotDefault(p => p.ToString()),
                IsClinical = i.IsClinical
            }).ToArray();

        }

        public IEnumerable<IMapMember<Patient, PatientSearchResult>> Members { get; private set; }
    }
}
