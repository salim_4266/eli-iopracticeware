﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using IO.Practiceware.Configuration;
using IO.Practiceware.Services.Data;
using Soaf;
using Soaf.Caching;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Web.Client;

[assembly: Component(typeof(ExtendedAdoService), typeof(IAdoService), Priority = 10)]

namespace IO.Practiceware.Services.Data
{
    public interface IExtendedAdoService : IAdoService
    {
    }

    [Serializable]
    [DataContract]
    public class QueryingOptimizationParameters
    {
        public QueryingOptimizationParameters()
        {
            OptimizeViews = true;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [optimize views]. Default is true.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [optimize views]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool OptimizeViews { get; set; }

        [DataMember]
        public bool ReuseCachedViewData { get; set; }

        [DataMember]
        public TimeSpan? ViewCacheTimeout { get; set; }

        [DataMember]
        public bool FastViewDataCopy { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to run all queries with read uncommitted.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [read uncommitted]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool ReadUncommitted { get; set; }
    }


    [SupportsTransactionScopes]
    [RemoteService]
    public class ExtendedAdoService : IExtendedAdoService
    {
        private readonly IAdoService _implementation;

        /// <summary>
        /// The enable view querying optimizations context data key. Set <see cref="QueryingOptimizationParameters"/> as value
        /// </summary>
        public const string EnableQueryingOptimizationsContextDataKey = "__ExtendedAdoService_EnableOptimizations__";

        public ExtendedAdoService(AdoService implementation)
        {
            _implementation = implementation;
        }

        public IEnumerable<AdoServiceCommandExecutionResult> ExecuteCommands(IEnumerable<AdoServiceCommandExecutionArgument> commands)
        {
            using (var scope = GetOptimizationScope())
            {
                var commandsEnumerated = commands.ToArray();
                foreach (var command in commandsEnumerated)
                {
                    if (!IsConnectionToClientDatabase(command.ConnectionString, command.Database))
                    {
                        command.ConnectionString = ConfigurationManager.PracticeRepositoryConnectionString;
                        command.Database = null; // Use Initial Catalog from connection string
                    }
                }

                if (scope.ReadUncommitted)
                {
                    return ExecuteCommandsInternal(commandsEnumerated);
                }
                return _implementation.ExecuteCommands(commandsEnumerated);
            }
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        protected virtual IEnumerable<AdoServiceCommandExecutionResult> ExecuteCommandsInternal(AdoServiceCommandExecutionArgument[] commandsEnumerated)
        {
            return _implementation.ExecuteCommands(commandsEnumerated);
        }

        public void ExecuteBulkCopy(AdoServiceBulkCopyExecutionArgument argument)
        {
            if (!IsConnectionToClientDatabase(argument.ConnectionString, argument.Database))
            {
                argument.ConnectionString = ConfigurationManager.PracticeRepositoryConnectionString;
                argument.Database = null; // Use Initial Catalog from connection string
            }

            _implementation.ExecuteBulkCopy(argument);
        }

        public DataSet GetSchema(string connectionString, string collectionName, string[] restrictionValues)
        {
            if (!IsConnectionToClientDatabase(connectionString, null))
            {
                return _implementation.GetSchema(ConfigurationManager.PracticeRepositoryConnectionString, collectionName, restrictionValues);
            }

            return _implementation.GetSchema(connectionString, collectionName, restrictionValues);
        }

        public string ResolveConnectionString(string connectionString)
        {
            if (!IsConnectionToClientDatabase(connectionString, null))
            {
                return _implementation.ResolveConnectionString(ConfigurationManager.PracticeRepositoryConnectionString);
            }

            return _implementation.ResolveConnectionString(connectionString);
        }

        public AdoServiceCommandExecutionArgument DeriveCommandParameters(AdoServiceCommandExecutionArgument command)
        {
            if (!IsConnectionToClientDatabase(command.ConnectionString, command.Database))
            {
                command.ConnectionString = ConfigurationManager.PracticeRepositoryConnectionString;
                command.Database = null; // Use Initial Catalog from connection string
            }

            return _implementation.DeriveCommandParameters(command);
        }

        private static QueryOptimizationScope GetOptimizationScope()
        {
            if (!ContextData.Keys.Contains(EnableQueryingOptimizationsContextDataKey)) return new QueryOptimizationScope(null, false);

            var parameters = ContextData.Get<QueryingOptimizationParameters>(EnableQueryingOptimizationsContextDataKey, () => null);
            var result = parameters == null
                ? new QueryOptimizationScope(new DbViewQueryingOptimizationScope(true), false) // Default shared scope
                : new QueryOptimizationScope(parameters.OptimizeViews ? new DbViewQueryingOptimizationScope(parameters.ReuseCachedViewData, parameters.ViewCacheTimeout, parameters.FastViewDataCopy) : null, parameters.ReadUncommitted);
            return result;
        }

        private static bool IsConnectionToClientDatabase(string connectionString, string database)
        {
            // Skip check when it's not a WCF service call (locally clients can connect to any database)
            if (ConfigurationManager.ClientConfiguration == null || OperationContext.Current == null) return true;

            return OperationCache.ExecuteCached(() => CheckClientConnectionString(ConfigurationManager.PracticeRepositoryConnectionString, connectionString, database));
        }

        private static bool CheckClientConnectionString(string clientConnectionString, string passedConnectionString, string passedDatabase)
        {
            var clientConnectionStringParsed = DbConnections.TryGetConnectionStringBuilder<SqlConnectionStringBuilder>(clientConnectionString);
            var passedConnectionStringParsed = DbConnections.TryGetConnectionStringBuilder<SqlConnectionStringBuilder>(passedConnectionString)
                ?? new SqlConnectionStringBuilder();

            // Point parsed connection to correct database
            if (!string.IsNullOrEmpty(passedDatabase))
            {
                passedConnectionStringParsed.InitialCatalog = passedDatabase;
            }


            // Client might be just sending a default connection string key which is absolutely ok
            if (string.Equals(passedConnectionString, ConfigurationManager.DefaultPracticeRepositoryConnectionStringKey, StringComparison.OrdinalIgnoreCase)
                && string.IsNullOrEmpty(passedDatabase)) // Db must not be specified
            {
                return true;
            }

            // Verify data source and initial catalog match
            if (string.Equals(passedConnectionStringParsed.DataSource, clientConnectionStringParsed.DataSource, StringComparison.OrdinalIgnoreCase)
                && string.Equals(passedConnectionStringParsed.InitialCatalog, clientConnectionStringParsed.InitialCatalog, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            // Warn
            Trace.TraceWarning("Client attempted to connect to wrong server or database. Passed values: {0}, {1}. Stored values: {2}, {3}"
                .FormatWith(passedConnectionStringParsed.DataSource, passedConnectionStringParsed.InitialCatalog,
                    clientConnectionStringParsed.DataSource, clientConnectionStringParsed.InitialCatalog));

            return false;
        }

        private class QueryOptimizationScope : IDisposable
        {
            private readonly DbViewQueryingOptimizationScope _dbViewQueryingOptimizationScope;

            public QueryOptimizationScope(DbViewQueryingOptimizationScope dbViewQueryingOptimizationScope, bool readUncommitted)
            {
                _dbViewQueryingOptimizationScope = dbViewQueryingOptimizationScope;
                ReadUncommitted = readUncommitted;
            }

            public bool ReadUncommitted { get; private set; }
          
            public void Dispose()
            {
                if (_dbViewQueryingOptimizationScope != null)
                {
                    _dbViewQueryingOptimizationScope.Dispose();
                }
            }
        }
    }
}
