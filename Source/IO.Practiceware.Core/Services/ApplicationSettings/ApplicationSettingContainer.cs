﻿using System.Runtime.Serialization;

namespace IO.Practiceware.Services.ApplicationSettings
{
    [DataContract]
    public class ApplicationSettingContainer<TValue>
    {
        [DataMember]
        public int? Id { get; set; }

        /// <summary>
        /// User Id, if settings are specific to user. <c>null</c> if non-user specific
        /// </summary>
        [DataMember]
        public int? UserId { get; set; }

        /// <summary>
        /// Machine name, if settings are specific to machine or user machine. <c>null</c> if non-machine specific
        /// </summary>
        [DataMember]
        public string MachineName { get; set; }

        /// <summary>
        /// Name of this specific setting
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Setting value
        /// </summary>
        [DataMember]
        public TValue Value { get; set; }

        public ApplicationSettingScope Scope
        {
            get
            {
                if (UserId.HasValue && !string.IsNullOrEmpty(MachineName))
                {
                    return ApplicationSettingScope.UserMachine;
                }
                if (UserId.HasValue)
                {
                    return ApplicationSettingScope.User;
                }
                if (!string.IsNullOrEmpty(MachineName))
                {
                    return ApplicationSettingScope.Machine;
                }
                return ApplicationSettingScope.Global;
            }
        }
    }

    public enum ApplicationSettingScope
    {
        Global = 1,
        Machine = 2,
        User = 3,
        UserMachine = 4
    }
}