﻿using System.Collections.Generic;
using Soaf.Caching;
using Soaf.Threading;

namespace IO.Practiceware.Services.ApplicationSettings
{
    [SupportsCaching]
    public class CachingApplicationSettingsServiceProxy : IApplicationSettingsService
    {
        private readonly IApplicationSettingsService _settingsService;

        public CachingApplicationSettingsServiceProxy(IApplicationSettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        [Cache(HoldInterval = CommonCacheHoldInterval.Short, PerInstance = true)]
        public virtual ICollection<ApplicationSettingContainer<string>> GetSettings(string nameFilter, string machineName = null, int? userId = null)
        {
            // Retrieve settings synchronously via background thread
            var retrievalTask = System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(
                () => _settingsService.GetSettings(nameFilter, machineName, userId));
            retrievalTask.Wait();
            return retrievalTask.Result;
        }

        public virtual ICollection<int> SetSettings(ICollection<ApplicationSettingContainer<string>> settings)
        {
            this.ClearInstanceCache();
            return _settingsService.SetSettings(settings);
        }

        public virtual void DeleteSettings(ICollection<int> settingIds)
        {
            this.ClearInstanceCache();
            _settingsService.DeleteSettings(settingIds);
        }
    }
}