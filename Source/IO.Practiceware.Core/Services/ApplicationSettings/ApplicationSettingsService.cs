﻿using System;
using System.Linq.Expressions;
using IO.Practiceware.Model;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using Soaf.Data;
using Soaf.Web.Client;

[assembly: Component(typeof(ApplicationSettingsService), typeof(IApplicationSettingsService))]

namespace IO.Practiceware.Services.ApplicationSettings
{
    [RemoteService]
    [SupportsTransactionScopes]
    [SupportsUnitOfWork]
    public interface IApplicationSettingsService
    {
        /// <summary>
        /// Retrieves filtered settings
        /// </summary>
        /// <param name="nameFilter">The name filter.</param>
        /// <param name="machineName">Name of the machine.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        ICollection<ApplicationSettingContainer<string>> GetSettings(string nameFilter, string machineName = null, int? userId = null);

        /// <summary>
        /// Sets settings
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        ICollection<int> SetSettings(ICollection<ApplicationSettingContainer<string>> settings);

        /// <summary>
        /// Deletes all settings with specified ids
        /// </summary>
        /// <param name="settingIds"></param>
        void DeleteSettings(ICollection<int> settingIds);
    }

    /// <summary>
    /// Application settings service
    /// </summary>
    public class ApplicationSettingsService : IApplicationSettingsService
    {
        readonly IPracticeRepository _practiceRepository;
        IMapper<ApplicationSetting, ApplicationSettingContainer<string>> _settingsMapper;

        public ApplicationSettingsService(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
            InitMappers();
        }

        public virtual ICollection<ApplicationSettingContainer<string>> GetSettings(string nameFilter, string machineName = null, int? userId = null)
        {
            Expression<Func<ApplicationSetting, bool>> filterExpression = s =>
                (nameFilter == null || s.Name == nameFilter);


            // Global settings
            var query = _practiceRepository.ApplicationSettings
                .Where(filterExpression)
                .Where(s => s.UserId == null
                            && s.MachineName == null);

            // Machine level settings
            if (!string.IsNullOrEmpty(machineName))
            {
                query = query.Union(
                    _practiceRepository.ApplicationSettings
                    .Where(filterExpression)
                    .Where(s => s.UserId == null
                        && s.MachineName == machineName)
                );
            }

            // User level settings
            if (userId != null)
            {
                query = query.Union(
                    _practiceRepository.ApplicationSettings
                    .Where(filterExpression)
                    .Where(s => s.UserId == userId.GetValueOrDefault(-1)
                        && s.MachineName == null)
                );
            }


            // User & machine level settings
            if (userId != null && !string.IsNullOrEmpty(machineName))
            {
                query = query.Union(
                    _practiceRepository.ApplicationSettings
                    .Where(filterExpression)
                    .Where(s => s.UserId == userId.GetValueOrDefault(-1)
                        && s.MachineName == machineName)
                );
            }

            var result = query
                .ToArray()
                .Select(_settingsMapper.Map)
                .OrderBy(i => i.Name)
                .ThenByDescending(i => i.Scope)
                .ToList();

            return result;
        }

        public virtual ICollection<int> SetSettings(ICollection<ApplicationSettingContainer<string>> settings)
        {
            // Preload existing settings
            var existingIds = settings.Where(s => s.Id != null).Select(s => s.Id.GetValueOrDefault()).ToArray();
            var existingSettings = _practiceRepository.ApplicationSettings
                .Where(s => existingIds.Contains(s.Id))
                .ToDictionary(s => s.Id, s => s);

            // Update
            var newSettings = new List<ApplicationSetting>();
            foreach (var setting in settings)
            {
                ApplicationSetting model;
                if (setting.Id != null && existingSettings.ContainsKey(setting.Id.Value))
                {
                    model = existingSettings[setting.Id.Value];
                }
                else
                {
                    model = new ApplicationSetting();
                    newSettings.Add(model);
                }

                model.UserId = setting.UserId;
                model.MachineName = setting.MachineName;
                model.Name = setting.Name;
                model.Value = setting.Value;
            }

            // Save
            _practiceRepository.Save(newSettings.Union(existingSettings.Values));

            // Return Ids of new settings
            return newSettings.Select(s => s.Id).ToArray();
        }

        public virtual void DeleteSettings(ICollection<int> settingIds)
        {
            // Load them
            var settings = _practiceRepository.ApplicationSettings
                .Where(s => settingIds.Contains(s.Id))
                .ToList();

            // Delete them
            settings.ForEach(_practiceRepository.Delete);
        }

        void InitMappers()
        {
            _settingsMapper = Mapper.Factory.CreateMapper<ApplicationSetting, ApplicationSettingContainer<string>>(
                setting => new ApplicationSettingContainer<string>
                           {
                               Id = setting.Id,
                               MachineName = setting.MachineName,
                               Name = setting.Name,
                               UserId = setting.UserId,
                               Value = setting.Value
                           });
        }
    }
}
