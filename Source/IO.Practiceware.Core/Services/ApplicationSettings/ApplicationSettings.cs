﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf.ComponentModel;

[assembly: Component(typeof(ApplicationSettings.Initializer), Initialize = true)]

namespace IO.Practiceware.Services.ApplicationSettings
{
    public static class ApplicationSettings
    {
        private static ConcurrentDictionary<string, IApplicationSettingsService> _applicationSettingsTenants;
        private static Func<CachingApplicationSettingsServiceProxy> _cachingSettingServiceConstruct;

        public class Initializer
        {
            public Initializer(Func<CachingApplicationSettingsServiceProxy> cachingSettingServiceConstruct)
            {
                _cachingSettingServiceConstruct = cachingSettingServiceConstruct;
                _applicationSettingsTenants = new ConcurrentDictionary<string, IApplicationSettingsService>();
            }
        }

        /// <summary>
        /// Returns a default <see cref="IApplicationSettingsService"/> which caches retrieval of settings
        /// </summary>
        public static IApplicationSettingsService Cached
        {
            get { return _applicationSettingsTenants.GetOrAdd(ApplicationContext.Current.ClientKey, k => _cachingSettingServiceConstruct()); }
        }

        /// <summary>
        /// Gets single setting of the type for current user and machine
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="service">The service.</param>
        /// <param name="nameFilter">The name filter.</param>
        /// <returns></returns>
        /// <remarks>
        /// The most specific setting based on input criteria is returned
        /// </remarks>
        public static ApplicationSettingContainer<T> GetSetting<T>(this IApplicationSettingsService service, string nameFilter)
        {
            return GetSettings<T>(service, nameFilter).OrderByDescending(i => i.Scope).FirstOrDefault();
        }

        /// <summary>
        /// Gets the or create setting.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="service">The service.</param>
        /// <param name="nameFilter">The name filter.</param>
        /// <param name="machineName">Name of the machine.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public static ApplicationSettingContainer<T> GetOrCreateSetting<T>(this IApplicationSettingsService service, string nameFilter, string machineName = null, int? userId = null)
        {
            return GetSettings<T>(service, nameFilter, machineName, userId).OrderByDescending(i => i.Scope).FirstOrDefault()
                ?? new ApplicationSettingContainer<T>
                {
                    Name = nameFilter,
                    MachineName = machineName,
                    UserId = userId
                };
        }

        /// <summary>
        /// Gets single setting of the type with specific user and machine filtering
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="service">The service.</param>
        /// <param name="nameFilter">The name filter.</param>
        /// <param name="machineName">Name of the machine.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        /// <remarks>
        /// The most specific setting based on input criteria is returned
        /// </remarks>
        public static ApplicationSettingContainer<T> GetSetting<T>(this IApplicationSettingsService service, string nameFilter, string machineName, int? userId)
        {
            return GetSettings<T>(service, nameFilter, machineName, userId).OrderByDescending(i => i.Scope).FirstOrDefault();
        }

        /// <summary>
        /// Gets the settings of specified type for current user and machine
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="service">The service.</param>
        /// <param name="nameFilter">The name filter.</param>
        /// <returns></returns>
        public static ICollection<ApplicationSettingContainer<T>> GetSettings<T>(this IApplicationSettingsService service, string nameFilter)
        {
            // Evaluate current user machine and id
            var machineName = ApplicationContext.Current.ComputerName;
            var userId = UserContext.Current != null && UserContext.Current.UserDetails != null 
                ? UserContext.Current.UserDetails.Id 
                : (int?)null;

            return GetSettings<T>(service, nameFilter, machineName, userId);
        }

        /// <summary>
        /// Gets the settings with specific user and machine filtering
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="service">The service.</param>
        /// <param name="nameFilter">Additionally filter settings by Name.</param>
        /// <param name="machineName">Name of the machine.</param>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public static ICollection<ApplicationSettingContainer<T>> GetSettings<T>(this IApplicationSettingsService service, string nameFilter, string machineName, int? userId)
        {
            ICollection<ApplicationSettingContainer<T>> result;
            var retrievedSettings = service.GetSettings(nameFilter, machineName, userId);

            // Deserialize depending on requested type
            if (typeof(T) == typeof(string))
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                result = (ICollection<ApplicationSettingContainer<T>>)retrievedSettings;
            }
            else if (typeof(T) == typeof(int))
            {
                result = retrievedSettings
                    .Select(s => ConvertSettingValue(s, int.Parse))
                    // ReSharper disable once SuspiciousTypeConversion.Global
                    .OfType<ApplicationSettingContainer<T>>()
                    .ToList();
            }
            else if (typeof(T) == typeof(bool))
            {
                result = retrievedSettings
                    .Select(s => ConvertSettingValue(s, bool.Parse))
                    // ReSharper disable once SuspiciousTypeConversion.Global
                    .OfType<ApplicationSettingContainer<T>>()
                    .ToList();
            }
            else
            {
                // Deserialize from Xml
                result = retrievedSettings.Select(s => ConvertSettingValue(s, xmlValue => xmlValue.FromXml<T>())).ToList();
            }

            return result;
        }

        /// <summary>
        /// Sets the setting and returns it's id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="service"></param>
        /// <param name="setting"></param>
        /// <returns></returns>
        public static void SetSetting<T>(this IApplicationSettingsService service, ApplicationSettingContainer<T> setting)
        {
            var newIds = SetSettings(service, new[] { setting });
            // If it got a new id -> set it
            if (newIds.Count > 0)
            {
                setting.Id = newIds.Single();
            }
        }

        /// <summary>
        /// Sets the settings.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="service">The service.</param>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        public static ICollection<int> SetSettings<T>(this IApplicationSettingsService service, ICollection<ApplicationSettingContainer<T>> settings)
        {
            var serializedSettings = settings.Select(s => ConvertSettingValue(s, value => value is string ? (string)(object)value : value.ToXml())).ToList();
            var result = service.SetSettings(serializedSettings);
            return result;
        }

        /// <summary>
        /// Converts settings from one type to another
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="s"></param>
        /// <param name="convertValue"></param>
        /// <returns></returns>
        public static ApplicationSettingContainer<TDestination> ConvertSettingValue<TSource, TDestination>(
            this ApplicationSettingContainer<TSource> s, Func<TSource, TDestination> convertValue)
        {
            return new ApplicationSettingContainer<TDestination>
                   {
                       Id = s.Id,
                       UserId = s.UserId,
                       MachineName = s.MachineName,
                       Value = convertValue(s.Value),
                       Name = s.Name
                   };
        }
    }
}