﻿using IO.Practiceware.Model;
using IO.Practiceware.Services.Geocode;
using Newtonsoft.Json;
using RestSharp;
using Soaf;
using Soaf.Caching;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

[assembly: Component(typeof(GeocodingService), typeof(IGeocodingService))]

namespace IO.Practiceware.Services.Geocode
{
    [SupportsDataErrorInfo]
    public class GeocodeInformation
    {
        [Required(ErrorMessage = "City not found.")]
        public string City { get; set; }

        [Required(ErrorMessage = "State not found.")]
        public int? StateId { get; set; }

        [Required(ErrorMessage = "Country not found.")]
        public int? CountryId { get; set; }

        public string ZipCode { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }

    /// <summary>
    /// Service to get Geocode information for places within the U.S.
    /// </summary>
#if DEBUG
    [BackgroundThreadOnly]
#endif
    [SupportsCaching]
    public interface IGeocodingService
    {
        /// <summary>
        /// Gets the geocode information.
        /// </summary>
        /// <param name="zipCode">The zip code.  If null, will use current zip code based on IP address geolocation.</param>
        /// <returns></returns>
        GeocodeInformation GetGeocodeInformation(string zipCode = null);
    }

    [DebuggerNonUserCode]
    public class GeocodingService : IGeocodingService
    {
        private readonly IPracticeRepository _practiceRepository;
        private readonly Func<GeocodeInformation> _getNewGeocodeInformation;
        private const int MaxQueryRetryCount = 12;

        public GeocodingService(IPracticeRepository practiceRepository, Func<GeocodeInformation> getNewGeocodeInformation)
        {
            _practiceRepository = practiceRepository;
            _getNewGeocodeInformation = getNewGeocodeInformation;
        }

        [Cache(HoldInterval = CommonCacheHoldInterval.XLong)]
        public virtual GeocodeInformation GetGeocodeInformation(string zipCode = null)
        {

            if (zipCode == null) zipCode = GetCurrentZipCodeWithRetry();

            // Still no Zip code? -> return
            if (string.IsNullOrEmpty(zipCode)) return null;

            const string httpAddressFormat = "http://maps.googleapis.com/maps/api/geocode/json?address={0}&sensor=true";
            var httpAddress = httpAddressFormat.FormatWith(zipCode);

            try
            {
                return RetryUtility.ExecuteWithRetry(() => GetGeocodeInformation(zipCode, httpAddress), MaxQueryRetryCount);
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not get geocode information from {0}".FormatWith(httpAddress), ex);
                throw exception;
            }
        }

        private GeocodeInformation GetGeocodeInformation(string zipCode, string url)
        {
            var response = new RestClient().Execute<GeocodeResponse>(new RestRequest(url));
            var geocodeResponse = response.Data;
            List<AddressComponent> addressComponents = null;
            Geometry geometry = null;
            if (geocodeResponse !=null)
            {
                 addressComponents = geocodeResponse.Results.Select(r => r.AddressComponents).FirstOrDefault();
            }
            var geocodeInfo = _getNewGeocodeInformation();
            geocodeInfo.ZipCode = zipCode;

            if (addressComponents != null)
            {
                var city = addressComponents.GetCity().IfNotDefault(ac => ac.LongName);

                geocodeInfo.City = city;

                var stateComponent = addressComponents.FirstOrDefault(ac => ac.IsState());

                if (stateComponent != null)
                {
                    var state = _practiceRepository
                        .StateOrProvinces
                        .Include(s => s.Country)
                        .FirstOrDefault(s => s.Abbreviation.Equals(stateComponent.ShortName, StringComparison.InvariantCultureIgnoreCase)
                                                || s.Name.Equals(stateComponent.LongName, StringComparison.InvariantCultureIgnoreCase));

                    if (state != null)
                    {
                        geocodeInfo.StateId = state.Id;
                        geocodeInfo.CountryId = state.Country.Id;
                    }
                }
            }
            if (geocodeResponse != null)
            {
                 geometry = geocodeResponse.Results.Select(r => r.Geometry).FirstOrDefault();
            }

            if (geometry != null)
            {
                geocodeInfo.Latitude = geometry.Location.Latitude;
                geocodeInfo.Longitude = geometry.Location.Longitude;
            }
            return geocodeInfo;
        }

        const string HttpAddressFormatForZipCodeApi = "http://freegeoip.net/json/{0}";

        [Cache(HoldInterval = CommonCacheHoldInterval.XLong)]
        public virtual string GetCurrentZipCodeWithRetry()
        {
            try
            {
                return RetryUtility.ExecuteWithRetry(GetCurrentZipCode, MaxQueryRetryCount);
            }
            catch (Exception ex)
            {
                var httpAddressForZipCodeApi = HttpAddressFormatForZipCodeApi.FormatWith(GetCurrentIpAddressWithRetry());
                var exception = new Exception("Could not get current zip code from {0}".FormatWith(httpAddressForZipCodeApi), ex);
                throw exception;
            }
        }

        private string GetCurrentZipCode()
        {
            var httpAddressForZipCodeApi = HttpAddressFormatForZipCodeApi.FormatWith(GetCurrentIpAddressWithRetry());
            var response = new RestClient().Execute(new RestRequest(httpAddressForZipCodeApi));
            dynamic geolocationResponse = JsonConvert.DeserializeObject(response.Content);
            return geolocationResponse.zipcode;
        }

        const string CheckIpAddressApi = "http://checkip.dyndns.org/";

        [Cache(HoldInterval = CommonCacheHoldInterval.XLong)]
        public virtual string GetCurrentIpAddressWithRetry()
        {
            try
            {
                return RetryUtility.ExecuteWithRetry(GetCurrentIpAddress, MaxQueryRetryCount);
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not determine current ip address from {0}".FormatWith(CheckIpAddressApi), ex);
                throw exception;
            }
        }

        private static string GetCurrentIpAddress()
        {
            string externalIp = (new WebClient()).DownloadString(CheckIpAddressApi);
            externalIp = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                .Matches(externalIp)[0].ToString();
            return externalIp;
        }
    }

    #region Classes that are produced when deserializing the JSON objects returned by the google maps api query to the GeocodingService

    [DataContract(Namespace = ".")]
    public class GeocodeResponse
    {
        [DataMember(Name = "results")]
        public List<Result> Results { get; set; }
    }

    [DataContract(Namespace = ".")]
    public class Result
    {
        [DataMember(Name = "types")]
        public List<string> Types { get; set; }

        [DataMember(Name = "address_components")]
        public List<AddressComponent> AddressComponents { get; set; }

        [DataMember(Name = "formatted_address")]
        public string FormattedAddress { get; set; }

        [DataMember(Name = "geometry")]
        public Geometry Geometry { get; set; }
    }

    [DataContract(Namespace = ".")]
    public class AddressComponent
    {
        [DataMember(Name = "long_name")]
        public string LongName { get; set; }

        [DataMember(Name = "short_name")]
        public string ShortName { get; set; }

        [DataMember(Name = "types")]
        public List<string> Types { get; set; }
    }

    [DataContract(Namespace = ".")]
    public class Geometry
    {
        [DataMember(Name = "location")]
        public Location Location { get; set; }
    }

    [DataContract(Namespace = ".")]
    public class Location
    {
        [DataMember(Name = "lat")]
        public double Latitude { get; set; }

        [DataMember(Name = "lng")]
        public double Longitude { get; set; }
    }

    #endregion

    /// <summary>
    /// Extension class that assists in parsing the deserialized objects returned by the google maps api.
    /// </summary>
    public static class GeocodeResponseExtensions
    {
        public static AddressComponent GetCity(this IEnumerable<AddressComponent> source)
        {
            return
                    source.FirstOrDefault(s => s.Types.Any(t => t.Equals("political", StringComparison.InvariantCultureIgnoreCase)) && s.Types.Any(t => t.Equals("locality", StringComparison.InvariantCultureIgnoreCase)))
                        ??
                            source.FirstOrDefault(s => s.Types.Any(t => t.Equals("political", StringComparison.InvariantCultureIgnoreCase)) && s.Types.Any(t => t.Equals("neighborhood", StringComparison.InvariantCultureIgnoreCase)))
                                           ??
                                               source.FirstOrDefault(s => s.Types.Any(t => t.Equals("political", StringComparison.InvariantCultureIgnoreCase)) && s.Types.Any(t => t.Equals("sublocality", StringComparison.InvariantCultureIgnoreCase)));

        }

        public static bool IsState(this AddressComponent addressComponent)
        {
            return addressComponent.Types.Any(t => t.Equals("administrative_area_level_1", StringComparison.InvariantCultureIgnoreCase))
                && addressComponent.Types.Any(t => t.Equals("political", StringComparison.InvariantCultureIgnoreCase));
        }

        public static bool IsCountry(this AddressComponent addressComponent)
        {
            return addressComponent.Types.Any(t => t.Equals("country", StringComparison.InvariantCultureIgnoreCase))
                && addressComponent.Types.Any(t => t.Equals("political", StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
