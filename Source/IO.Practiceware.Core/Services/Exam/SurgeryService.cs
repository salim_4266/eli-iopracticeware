﻿using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Exam;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;

[assembly: Component(typeof (SurgeryService), typeof (ISurgeryService))]

namespace IO.Practiceware.Services.Exam
{
    public interface ISurgeryService
    {
        /// <summary>
        ///   Gets the patient surgery.
        /// </summary>
        /// <param name="arguments"> The arguments. </param>
        /// <returns> </returns>
        PatientSurgery SaveAndGetPatientSurgery(PatientSurgeryArguments arguments);
    }

    /// <summary>
    ///   Surgery Manager used to make PatientSurgery
    /// </summary>
    public class SurgeryService : ISurgeryService
    {
        private readonly IPracticeRepository _practiceRepository;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;

        public SurgeryService(IPracticeRepository practiceRepository, IUnitOfWorkProvider unitOfWorkProvider)
        {
            _practiceRepository = practiceRepository;
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        #region ISurgeryService Members

        /// <summary>
        ///   Gets the patient surgery.
        /// </summary>
        /// <param name="arguments"> The arguments. </param>
        /// <returns> </returns>
        public PatientSurgery SaveAndGetPatientSurgery(PatientSurgeryArguments arguments)
        {
            using (IUnitOfWork work = _unitOfWorkProvider.Create())
            {
            if ((arguments.PatientSurgery == null))
            {
                // creating a new task, so create a new TaskEntity
                arguments.PatientSurgery = new PatientSurgery();
                arguments.PatientSurgery.SurgeryType = arguments.SurgeryType;
                arguments.PatientSurgery.SurgeryTypeId = arguments.SurgeryTypeId;
                arguments.PatientSurgery.Encounter = arguments.Encounter;
                arguments.PatientSurgery.SurgeonUserId = arguments.SurgeonUserId;
            }
            if ((arguments.PatientSurgery.Id != 0))
            {
                // Don't allow overwrite of an existing patient associated with the task
                arguments.PatientSurgery.SurgeryType = arguments.SurgeryType;
                arguments.PatientSurgery.SurgeryTypeId = arguments.SurgeryTypeId;
            }
            PatientSurgery patientSurgery = arguments.PatientSurgery;

            if (arguments.SurgeryQuestionChoices != null)
            {
                if (arguments.SurgeryQuestionChoices.Any())
                {
                    foreach (QuestionChoiceArguments qChoice in arguments.SurgeryQuestionChoices)
                    {
                            patientSurgery = GetUpdatedPatientSurgery(arguments, qChoice, patientSurgery);
                                    }
                                }
                            }
            if (arguments.HealthAssessmentQuestionChoices != null)
            {
                if (arguments.HealthAssessmentQuestionChoices.Any())
                {
                    foreach (QuestionChoiceArguments qChoice in arguments.HealthAssessmentQuestionChoices)
                    {
                            patientSurgery = GetUpdatedPatientSurgery(arguments, qChoice, patientSurgery);
                    }
                }
            }
            if (arguments.WorkUpQuestionChoices != null)
            {
                if (arguments.WorkUpQuestionChoices.Any())
                {
                    foreach (QuestionChoiceArguments qChoice in arguments.WorkUpQuestionChoices)
                    {
                            patientSurgery = GetUpdatedPatientSurgery(arguments, qChoice, patientSurgery);
                        }
                    }
                }
            if (arguments.OpRoomQuestionChoices != null)
            {
                if (arguments.OpRoomQuestionChoices.Any())
                {
                    foreach (QuestionChoiceArguments qChoice in arguments.OpRoomQuestionChoices)
                    {
                            patientSurgery = GetUpdatedPatientSurgery(arguments, qChoice, patientSurgery);
                                    }
                                    }
                                }
            if (arguments.PostOpQuestionChoices != null)
            {
                if (arguments.PostOpQuestionChoices.Any())
                {
                    foreach (QuestionChoiceArguments qChoice in arguments.PostOpQuestionChoices)
                    {
                            patientSurgery = GetUpdatedPatientSurgery(arguments, qChoice, patientSurgery);
                    }
                }
            }
            if (arguments.SurgeryQuestionAnswers != null)
            {
                if (arguments.SurgeryQuestionAnswers.Any())
                {
                    foreach (QuestionAnswerArguments qAnswer in arguments.SurgeryQuestionAnswers)
                    {
                            patientSurgery = GetUpdatedPatientSurgery(arguments, qAnswer, patientSurgery);
                                    }
                                }
                            }
            if (arguments.HeightWeightQuestionAnswers != null)
            {
                if (arguments.HeightWeightQuestionAnswers.Any())
                {
                    foreach (QuestionAnswerArguments qAnswer in arguments.HeightWeightQuestionAnswers)
                    {
                            patientSurgery = GetUpdatedPatientSurgery(arguments, qAnswer, patientSurgery);
                                        }
                                    }
                                }
            if (arguments.WorkUpQuestionAnswers != null)
            {
                if (arguments.WorkUpQuestionAnswers.Any())
                {
                    foreach (QuestionAnswerArguments qAnswer in arguments.WorkUpQuestionAnswers)
                    {
                            patientSurgery = GetUpdatedPatientSurgery(arguments, qAnswer, patientSurgery);
                        }
                    }
                }
                if (arguments.OpRoomQuestionAnswers != null)
                        {
                    if (arguments.OpRoomQuestionAnswers.Any())
                            {
                        foreach (QuestionAnswerArguments qAnswer in arguments.OpRoomQuestionAnswers)
                                {
                            patientSurgery = GetUpdatedPatientSurgery(arguments, qAnswer, patientSurgery);
                                        }
                                    }
                                }
                if (arguments.PostOpQuestionAnswers != null)
                            {
                    if (arguments.PostOpQuestionAnswers.Any())
                    {
                        foreach (QuestionAnswerArguments qAnswer in arguments.PostOpQuestionAnswers)
                        {
                           patientSurgery = GetUpdatedPatientSurgery(arguments, qAnswer, patientSurgery);
                            }
                        }
                    }
                _practiceRepository.Save(patientSurgery);
                work.AcceptChanges();
                return patientSurgery;
                }
            }

        private PatientSurgery GetUpdatedPatientSurgery(PatientSurgeryArguments arguments, QuestionAnswerArguments qAnswer, PatientSurgery patientSurgery)
            {
                        QuestionAnswerArguments answer = qAnswer;

            patientSurgery = answer.PatientSurgeryQuestionAnswerId != 0 ? UpdateOrDeleteSurgeryQuestionAnswer(arguments, answer, patientSurgery) : AddNewSurgeryQuestionAnswer(arguments, answer, patientSurgery);
            return patientSurgery;
        }

        private PatientSurgery GetUpdatedPatientSurgery(PatientSurgeryArguments arguments, QuestionChoiceArguments qChoice, PatientSurgery patientSurgery)
                        {
            QuestionChoiceArguments choice = qChoice;
            patientSurgery = choice.PatientSurgeryQuestionChoiceId != 0 ? UpdateOrDeleteSurgeryQuestionChoice(arguments, choice, patientSurgery) : AddNewSurgeryQuestionChoice(arguments, choice, patientSurgery);
            return patientSurgery;
        }

        private PatientSurgery UpdateOrDeleteSurgeryQuestionChoice(PatientSurgeryArguments arguments, QuestionChoiceArguments choice, PatientSurgery patientSurgery)
                            {
            // update the choice of Question
            foreach (PatientSurgeryQuestionChoice pChoice in patientSurgery.PatientSurgeryQuestionChoices)
                                {
                if (pChoice.Id == choice.PatientSurgeryQuestionChoiceId)
                                    {
                    if (choice.IsDeleted)
                                        {
                        patientSurgery.PatientSurgeryQuestionChoices.Remove(pChoice);
                        _practiceRepository.Delete(pChoice);
                        break;
                            }
                    if (!choice.IsDeleted & (pChoice.ChoiceId != choice.ChoiceId | pChoice.Note.Trim() != choice.Note.Trim()))
                            {
                        pChoice.PatientSurgeryId = arguments.PatientSurgery.Id;
                        pChoice.ChoiceId = choice.ChoiceId;
                        pChoice.Note = choice.Note;
                        pChoice.ModifiedByUserId = UserContext.Current.UserDetails.Id;
                        pChoice.ModifiedDateTime = DateTime.Now.ToClientTime();
                        }
                    }
                }
            return patientSurgery;
            }

        private static PatientSurgery AddNewSurgeryQuestionChoice(PatientSurgeryArguments arguments, QuestionChoiceArguments choice, PatientSurgery patientSurgery)
                        {
            // insert the choice of Question
            var newSurgeryQuestionChoice = new PatientSurgeryQuestionChoice();
            newSurgeryQuestionChoice.PatientSurgeryId = arguments.PatientSurgery.Id;
            newSurgeryQuestionChoice.ChoiceId = choice.ChoiceId;
            newSurgeryQuestionChoice.Note = choice.Note;
            newSurgeryQuestionChoice.ModifiedByUserId = UserContext.Current.UserDetails.Id;
            newSurgeryQuestionChoice.ModifiedDateTime = DateTime.Now.ToClientTime();
            patientSurgery.PatientSurgeryQuestionChoices.Add(newSurgeryQuestionChoice);
            return patientSurgery;
        }

        #endregion

        private PatientSurgery UpdateOrDeleteSurgeryQuestionAnswer(PatientSurgeryArguments arguments, QuestionAnswerArguments answer, PatientSurgery patientSurgery)
                            {
                                foreach (PatientSurgeryQuestionAnswer pAnswer in patientSurgery.PatientSurgeryQuestionAnswers)
                                {
                                    if (pAnswer.Id == answer.PatientSurgeryQuestionAnswerId & pAnswer.QuestionId == answer.QuestionId)
                                    {
                    // Delete the answer if it is null or empty
                    if (answer.IsDeleted)
                    {
                        patientSurgery.PatientSurgeryQuestionAnswers.Remove(pAnswer);
                        _practiceRepository.Delete(pAnswer);
                        break;
                    }
                    // update the Answer of Question
                    if (!answer.IsDeleted & !string.IsNullOrEmpty(answer.Value.Trim()) & (pAnswer.Value.Trim() != answer.Value.Trim() | pAnswer.Note.Trim() != answer.Note.Trim()))
                                        {
                                            pAnswer.PatientSurgeryId = arguments.PatientSurgery.Id;
                                            pAnswer.QuestionId = answer.QuestionId;
                                            pAnswer.Value = answer.Value;
                                            pAnswer.Note = answer.Note;
                                            pAnswer.ModifiedByUserId = UserContext.Current.UserDetails.Id;
                                            pAnswer.ModifiedDateTime = DateTime.Now.ToClientTime();
                                        }
                                    }
                                }
            return patientSurgery;
                            }

        private static PatientSurgery AddNewSurgeryQuestionAnswer(PatientSurgeryArguments arguments, QuestionAnswerArguments answer, PatientSurgery patientSurgery)
        {
            if (!string.IsNullOrEmpty(answer.Value.Trim()))
                            {
                                // insert the Answer of Question
                                var newSurgeryQuestionAnswer = new PatientSurgeryQuestionAnswer();
                                newSurgeryQuestionAnswer.PatientSurgeryId = arguments.PatientSurgery.Id;
                                newSurgeryQuestionAnswer.QuestionId = answer.QuestionId;
                                newSurgeryQuestionAnswer.Value = answer.Value;
                                newSurgeryQuestionAnswer.Note = answer.Note;
                                newSurgeryQuestionAnswer.ModifiedByUserId = UserContext.Current.UserDetails.Id;
                                newSurgeryQuestionAnswer.ModifiedDateTime = DateTime.Now.ToClientTime();
                                patientSurgery.PatientSurgeryQuestionAnswers.Add(newSurgeryQuestionAnswer);
                            }
            return patientSurgery;
            }
    }

    public class PatientSurgeryArguments
    {
        public Encounter Encounter;
        public int EncounterId;
        public IEnumerable<QuestionChoiceArguments> HealthAssessmentQuestionChoices;
        public IEnumerable<QuestionAnswerArguments> HeightWeightQuestionAnswers;
        public int ModifiedBy;
        public IEnumerable<QuestionAnswerArguments> OpRoomQuestionAnswers;
        public IEnumerable<QuestionChoiceArguments> OpRoomQuestionChoices;

        /// <summary>
        ///   Class PatientSurgeryArguments
        /// </summary>
        public PatientSurgery PatientSurgery;

        public IEnumerable<QuestionAnswerArguments> PostOpQuestionAnswers;
        public IEnumerable<QuestionChoiceArguments> PostOpQuestionChoices;
        public int SurgeonUserId;
        public IEnumerable<QuestionAnswerArguments> SurgeryQuestionAnswers;
        public IEnumerable<QuestionChoiceArguments> SurgeryQuestionChoices;
        public SurgeryType SurgeryType;
        public int SurgeryTypeId;
        public IEnumerable<QuestionAnswerArguments> WorkUpQuestionAnswers;
        public IEnumerable<QuestionChoiceArguments> WorkUpQuestionChoices;
    }

    public class QuestionAnswerArguments
    {
        public bool IsDeleted;
        public int ModifiedByUserId;
        public DateTime ModifiedDateTime;
        public string Note;
        public int PatientSurgeryQuestionAnswerId;
        public int QuestionId;
        public string Value;
    }

    public class QuestionChoiceArguments
    {
        public int ChoiceId;
        public bool IsDeleted;
        public int ModifiedByUserId;
        public DateTime ModifiedDateTime;
        public string Note;
        public int PatientSurgeryQuestionChoiceId;
        public int QuestionId;
    }
}