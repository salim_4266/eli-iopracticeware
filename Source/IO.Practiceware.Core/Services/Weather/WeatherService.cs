﻿using IO.Practiceware.Application;
using IO.Practiceware.Services.Geocode;
using IO.Practiceware.Services.Weather;
using Newtonsoft.Json;
using RestSharp;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

[assembly: Component(typeof(WeatherService), typeof(IWeatherService))]

namespace IO.Practiceware.Services.Weather
{
    public enum DegreeType
    {
        [Display(Name = "°C")]
        Celcius,
        [Display(Name = "°F")]
        Fahrenheit
    }

    public enum WeatherType
    {
        ClearDay,
        ClearNight,
        PartlyCloudyDay,
        PartlyCloudyNight,
        CloudyDay,
        CloudyNight,
        Rain,
        Snow,
        Thunderstorm
    }

    public class WeatherInformation
    {
        public double Degrees { get; set; }
        public DegreeType DegreeType { get; set; }
        public byte[] WeatherIcon { get; set; }
        public WeatherType WeatherType { get; set; }

        public override string ToString()
        {
            return "{0}{1}".FormatWith(Math.Truncate(Degrees), DegreeType.GetDisplayName());
        }
    }

#if DEBUG
    [BackgroundThreadOnly]
#endif
    public interface IWeatherService
    {
        /// <summary>
        /// Gets the weather information.
        /// </summary>
        /// <param name="degreeType">Type of the degree.</param>
        /// <param name="zipCode">The zipCode.  If null, will use current location.</param>
        /// <returns></returns>
        WeatherInformation GetWeatherInformation(DegreeType degreeType = DegreeType.Fahrenheit, string zipCode = null);
    }

    [DebuggerNonUserCode]
    public class WeatherService : IWeatherService
    {
        private readonly IGeocodingService _geocodingService;
        private const int MaxQueryRetryCount = 5;

        public WeatherService(IGeocodingService geocodingService)
        {
            _geocodingService = geocodingService;
        }

        public WeatherInformation GetWeatherInformation(DegreeType degreeType = DegreeType.Fahrenheit, string zipCode = null)
        {
            try
            {

                if (UserContext.Current.UserDetails == null) return null;

                // Try get geocode information
                var geocodeInfo = _geocodingService.GetGeocodeInformation(zipCode);
                if (geocodeInfo == null) return null;

                var weatherInfoRequest = new WeatherInformationRequest(degreeType, geocodeInfo);

                WeatherInformation weatherInfo = RetryUtility.ExecuteWithRetry(weatherInfoRequest.GetWeatherInformationInternal, MaxQueryRetryCount);
                weatherInfo.DegreeType = degreeType;
                return weatherInfo;
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not get weather information", ex);
                throw exception;
            }
        }

        [DebuggerNonUserCode]
        private class WeatherInformationRequest
        {
            const string HttpAddressFormat = "http://api.openweathermap.org/data/2.5/weather?lat={0}&lon={1}&units={2}";

            private readonly DegreeType _degreeType;
            private readonly GeocodeInformation _geocodeInformation;

            private string Units
            {
                get { return _degreeType == DegreeType.Celcius ? "metric" : "imperial"; }
            }

            private string HttpAddressForWeatherInformation
            {
                get { return HttpAddressFormat.FormatWith(_geocodeInformation.Latitude, _geocodeInformation.Longitude, Units); }
            }

            public WeatherInformationRequest(DegreeType degreeType, GeocodeInformation geocodeInformation)
            {
                _degreeType = degreeType;
                _geocodeInformation = geocodeInformation;
            }

            public WeatherInformation GetWeatherInformationInternal()
            {
                #region helper func

                Func<string, WeatherType> parseIconToWeatherType = iconStringVal =>
                {
                    switch (iconStringVal)
                    {
                        case "01d":
                            return WeatherType.ClearDay;
                        case "01n":
                            return WeatherType.ClearNight;
                        case "03d":
                        case "04d":
                        case "50d":
                            return WeatherType.CloudyDay;
                        case "03n":
                        case "04n":
                        case "50n":
                            return WeatherType.CloudyNight;
                        case "02d":
                            return WeatherType.PartlyCloudyDay;
                        case "02n":
                            return WeatherType.PartlyCloudyNight;
                        case "09d":
                        case "09n":
                        case "10d":
                        case "10n":
                            return WeatherType.Rain;
                        case "13d":
                        case "13n":
                            return WeatherType.Snow;
                        case "11d":
                        case "11n":
                            return WeatherType.Thunderstorm;
                    }
                    throw new Exception("Could not parse icon value, {0}".FormatWith(iconStringVal));
                };

                #endregion

                try
                {
                    var response = new RestClient().Execute(new RestRequest(HttpAddressForWeatherInformation));
                    dynamic weatherResponse = JsonConvert.DeserializeObject(response.Content);

                    if (weatherResponse == null) return null;

                    var info = new WeatherInformation();
                    info.Degrees = weatherResponse.main.temp;

                    string weatherString = weatherResponse.weather.ToString();
                    var iconValue = weatherString.Substring(weatherString.IndexOf("icon") + 8, 3);
                    info.WeatherType = parseIconToWeatherType(iconValue);
                    return info;
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch
                // ReSharper restore EmptyGeneralCatchClause
                {

                }

                return null;
            }
        }
    }
}