﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using IO.Practiceware.Model;
using IO.Practiceware.Services.EntityLink;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Linq;

[assembly: Component(typeof(EntityLinkService), typeof(IEntityLinkService))]

namespace IO.Practiceware.Services.EntityLink
{
    /// <summary>
    ///     A service for retrieving information regarding questions and answers.
    /// </summary>
    public interface IEntityLinkService
    {
        /// <summary>
        ///     Gets the mapped entity for a linked entity (an ILinkedEntity).
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        object GetMappedEntity(IEntityLink value);
    }

    internal class EntityLinkService : IEntityLinkService
    {
        private readonly IPracticeRepository _practiceRepository;

        public EntityLinkService(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
        }

        public object GetMappedEntity(IEntityLink value)
        {
            if (value == null) return null;

            if (value.PracticeRepositoryEntityId == 0 || value.PracticeRepositoryEntityKey == "0" || string.IsNullOrEmpty(value.PracticeRepositoryEntityKey))
            {
                return null;
            }
            PracticeRepositoryEntity practiceRepositoryEntity = _practiceRepository.PracticeRepositoryEntities.WithId(value.PracticeRepositoryEntityId);

            PropertyInfo queryableProperty = _practiceRepository.GetType().GetProperties().FirstOrDefault(i => i.PropertyType.EqualsGenericTypeFor(typeof(IQueryable<>)) && i.PropertyType.GetGenericArguments()[0].Name == practiceRepositoryEntity.Name);

            if (queryableProperty != null)
            {
                var queryable = queryableProperty.GetValue(_practiceRepository, null).CastTo<IQueryable>();

                queryable = queryable.Where("{0} = {1}".FormatWith(queryable.GetType().GetGenericArguments()[0].GetProperty(practiceRepositoryEntity.KeyPropertyName).Name, value.PracticeRepositoryEntityKey));

                return queryable.As<IEnumerable>().OfType<object>().FirstOrDefault();
            }
            // try enum
            Type type = typeof(IPracticeRepository).Assembly.GetType("IO.Practiceware.Model." + (PracticeRepositoryEntityId)value.PracticeRepositoryEntityId);
            int intValue;
            if (type == null || !type.IsEnum || !int.TryParse(value.PracticeRepositoryEntityKey, out intValue))
            {
                throw new InvalidOperationException("Could not find entity for mapping with PracticeRepositoryEntityKey {0} and PracticeRepositoryEntityId {1}.".FormatWith(value.PracticeRepositoryEntityKey, value.PracticeRepositoryEntityId));
            }
            return Enums.GetValues(type).FirstOrDefault(v => (int)v == intValue).EnsureNotDefault("Could not find a matching enum value of {0} in {1}.".FormatWith(intValue, type.Name));
        }
    }
}