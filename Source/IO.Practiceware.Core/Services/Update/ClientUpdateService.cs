﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Services.Update;
using Soaf;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Web.Client;

[assembly: Component(typeof(ClientUpdateService), typeof(IClientUpdateService))]

namespace IO.Practiceware.Services.Update
{
    [RemoteService]
    public interface IClientUpdateService
    {
        /// <summary>
        /// Returns older manifest delivering updates only to application's shared data
        /// </summary>
        /// <returns></returns>
        ClientUpdateManifest GetUpdateManifest(UpdateApiVersion apiVersion = UpdateApiVersion.V1);

        /// <summary>
        /// Download update part contents found in manifest
        /// </summary>
        /// <param name="updatePartPath"></param>
        /// <param name="apiVersion"></param>
        /// <returns></returns>
        Stream DownloadUpdatePart(string updatePartPath, UpdateApiVersion apiVersion = UpdateApiVersion.V1); 
    }

    public class ClientUpdateService : IClientUpdateService
    {
        public const string NewVersionFolder = "NewVersion";
        public const string IOClientUpdatesFolder = @"SoftwareUpdates\Client";

        private const string ClientDataDirectory = "Pinpoint";
        private readonly IUpdateContentSearch _updateContentSearch;

        public ClientUpdateService(IUpdateContentSearch updateContentSearch)
        {
            _updateContentSearch = updateContentSearch;
        }

        public ClientUpdateManifest GetUpdateManifest(UpdateApiVersion apiVersion = UpdateApiVersion.V1)
        {
            ValidateConnectedClient();

            string v1SourcePath = Path.Combine(ConfigurationManager.ServerDataPath, NewVersionFolder);
            if (apiVersion == UpdateApiVersion.V1)
            {
                return GetUpdateManifestFromLocation(v1SourcePath);
            }

            // Attempt to serve a complete update from new location
            var v2SourcePath = Path.Combine(ConfigurationManager.ServerDataPath, IOClientUpdatesFolder);
            if (Directory.Exists(v2SourcePath))
            {
                return GetUpdateManifestFromLocation(v2SourcePath);
            }

            // For compatibility attempt to serve update from old update location and fix file paths
            var updateManifest = GetUpdateManifestFromLocation(v1SourcePath);
            updateManifest.AvailableUpdateFiles
                .IfNotNull(auf => auf
                    .ForEachWithSinglePropertyChangedNotification(f => f.FullName = Path.Combine(ClientDataDirectory, f.FullName))
                );
            return updateManifest;
        }

        public Stream DownloadUpdatePart(string updatePartPath, UpdateApiVersion apiVersion = UpdateApiVersion.V1)
        {
            ValidateConnectedClient();

            string v1SourcePath = Path.Combine(ConfigurationManager.ServerDataPath, NewVersionFolder);
            if (apiVersion == UpdateApiVersion.V1)
            {
                return GetUpdatePartStream(updatePartPath, v1SourcePath);
            }

            // Attempt to serve a complete update from new location
            var v2SourcePath = Path.Combine(ConfigurationManager.ServerDataPath, IOClientUpdatesFolder);
            if (Directory.Exists(v2SourcePath))
            {
                return GetUpdatePartStream(updatePartPath, v2SourcePath);
            }

            // For compatibility attempt to serve update from old update location and fix file paths
            if (!updatePartPath.StartsWith(ClientDataDirectory, StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException(string.Format("Download of {0} is not allowed", updatePartPath));
            }
            var fixedUpdatePartPath = updatePartPath.Remove(0, ClientDataDirectory.Length + 1);
            return GetUpdatePartStream(fixedUpdatePartPath, v1SourcePath);
        }

        /// <summary>
        /// Validates that connected client properly authenticated and is valid.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Client not recognized. Please, check connection settings</exception>
        private static void ValidateConnectedClient()
        {
            if (ApplicationContext.Current.IsServerSide && ConfigurationManager.ClientConfiguration == null)
            {
                throw new InvalidOperationException("Client not recognized. Please check connection settings");
            }
        }

        private Stream GetUpdatePartStream(string relativeUpdatePartPath, string sourcePath)
        {
            var sourcePathDirectory = new DirectoryInfo(sourcePath);
            if (!sourcePathDirectory.Exists)
            {
                throw new InvalidOperationException("Update part NewVersion not found");
            }

            // Locate update part in new version folder
            var updatePart = _updateContentSearch
                .ExecuteCached(cs => cs.LocateUpdatePart(relativeUpdatePartPath, sourcePathDirectory), 
                CacheHoldInterval.Short);

            if (updatePart == null)
            {
                throw new InvalidOperationException(string.Format("Update part {0} not found", relativeUpdatePartPath));
            }

            // Stream file directly from new version folder
            var result = new FileStream(updatePart.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);
            return result;
        }

        private ClientUpdateManifest GetUpdateManifestFromLocation(string sourcePath)
        {
            var result = new ClientUpdateManifest
            {
                ServerVersion = ApplicationContext.Current.Version
            };

            var newVersionDirectory = new DirectoryInfo(sourcePath);
            if (newVersionDirectory.Exists)
            {
                result.AvailableUpdateFiles = _updateContentSearch
                    .ExecuteCached(cs => cs.ListUpdateContent(newVersionDirectory),
                        CacheHoldInterval.Short)
                    .ToArray();
            }

            return result;
        }
    }

    [DataContract]
    public enum UpdateApiVersion
    {
        [EnumMember]
        V1,

        [EnumMember]
        V2
    }

    [DataContract]
    public class ClientUpdateManifest
    {
        [DataMember]
        public string ServerVersion { get; set; }

        [DataMember]
        public ICollection<FileInfoRecord> AvailableUpdateFiles { get; set; }
    }
}
