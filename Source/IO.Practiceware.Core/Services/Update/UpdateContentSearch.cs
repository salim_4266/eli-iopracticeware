﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
#if Updater
using IO.Practiceware.ClientUpdater;
using IO.Practiceware.ClientUpdater.ClientUpdateService;
#else
using IO.Practiceware.Services.Update;
using IO.Practiceware.Storage;
using Soaf.ComponentModel;
[assembly: Component(typeof(UpdateContentSearch), typeof(IUpdateContentSearch))]
#endif

namespace IO.Practiceware.Services.Update
{
    /// <summary>
    /// Allows to search new version folder for update content
    /// </summary>
    public interface IUpdateContentSearch
    {
        /// <summary>
        /// Lists files in update folder
        /// </summary>
        /// <param name="newVersionDirectory">New version folder</param>
        /// <returns><see cref="FileInfoRecord"/> with relative paths within <paramref name="newVersionDirectory"/></returns>
        ICollection<FileInfoRecord> ListUpdateContent(DirectoryInfo newVersionDirectory);

        /// <summary>
        /// Locates update part in new version folder.
        /// </summary>
        /// <param name="updatePartPath">The update part path.</param>
        /// <param name="newVersionDirectory">The new version directory.</param>
        /// <returns><see cref="FileInfoRecord"/> with full path to update</returns>
        FileInfoRecord LocateUpdatePart(string updatePartPath, DirectoryInfo newVersionDirectory);
    }

    public class UpdateContentSearch : IUpdateContentSearch
    {
        private static readonly string[] UpdateExtensions =
        {
            ".mdb", 
            ".bmp", 
            ".jpg", 
            ".txt", 
            ".exe", 
            ".log", 
            ".dll", 
            ".tlb", 
            ".msi"
        };

        public ICollection<FileInfoRecord> ListUpdateContent(DirectoryInfo newVersionDirectory)
        {
            var updateContent = ListUpdateContentRecursive(newVersionDirectory);

            // Return relative paths
            var result = updateContent.ToArray();
            foreach (var record in result)
            {
                record.FullName = MakeRelative(record.FullName, newVersionDirectory.FullName);
            }

            return result;
        }

        public FileInfoRecord LocateUpdatePart(string updatePartPath, DirectoryInfo newVersionDirectory)
        {
            var fullPartPath = Path.GetFullPath(Path.Combine(newVersionDirectory.FullName, updatePartPath));
            if (!fullPartPath.StartsWith(newVersionDirectory.FullName, StringComparison.OrdinalIgnoreCase))
            {
                // Looks like full path is outside new version folder -> skipping
                return null;
            }

            // Do we include this file in update?
            var partInfo = new FileInfo(fullPartPath);
            if (!partInfo.Exists || IsExcluded(partInfo) || !IsIncluded(partInfo))
            {
                return null;
            }

            // Do we include every parent folder of this part?
            var parentFolder = partInfo.Directory;
            do
            {
                // Got to new version folder? No need to check anymore 
                if (parentFolder != null
                    && (parentFolder.FullName.Length < newVersionDirectory.FullName.Length 
                    || string.Equals(parentFolder.FullName, newVersionDirectory.FullName, StringComparison.OrdinalIgnoreCase)))
                {
                    break;
                }

                // Parent folder not included? -> skip
                if (IsExcluded(parentFolder) || !IsIncluded(parentFolder))
                {
                    return null;
                }
            
// ReSharper disable once PossibleNullReferenceException
            } while ((parentFolder = parentFolder.Parent) != null);

            // Return part
            return FileSystem.GetFileInfo(fullPartPath);
        }

        private static IEnumerable<FileInfoRecord> ListUpdateContentRecursive(DirectoryInfo newVersionDirectory)
        {
            var updateContent = newVersionDirectory.EnumerateFileSystemInfos()
                .Where(fsi => !IsExcluded(fsi) && IsIncluded(fsi))
                .SelectMany(fsi => fsi is DirectoryInfo
                    ? ListUpdateContentRecursive((DirectoryInfo)fsi)
                    : fsi is FileInfo
                    ? new[] { FileSystem.GetFileInfo((FileInfo)fsi) }
                    : new FileInfoRecord[0]);

            return updateContent;
        }

        private static bool IsExcluded(FileSystemInfo fsi)
        {
            // Exclude non-update folders and files (+server msi)
            return fsi.Name.StartsWith("_") 
                || fsi.Name.StartsWith("~") 
                || IsServerMsi(fsi)
                || (fsi.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden;  // Exclude hidden content
        }

        private static bool IsIncluded(FileSystemInfo fsi)
        {
            return (fsi is DirectoryInfo) // Include folders
                   || IsSupportedExtension(fsi.Extension) // Include files with only supported extension
                   || IsUpdateCleanupBatch(fsi);
        }

        private static bool IsUpdateCleanupBatch(FileSystemInfo fsi)
        {
            // Include update cleanup batch file (it has "unsupported" extension)
            return string.Equals(fsi.Name, "UpdateCleanUp.bat", StringComparison.OrdinalIgnoreCase);
        }

        private static bool IsServerMsi(FileSystemInfo fsi)
        {
            return fsi.Name.ToLowerInvariant().Contains("server") && string.Equals(fsi.Extension, ".msi", StringComparison.OrdinalIgnoreCase);
        }

        private static bool IsSupportedExtension(string extension)
        {
            var isSupported = UpdateExtensions.Any(e => string.Equals(e, extension, StringComparison.OrdinalIgnoreCase));
            return isSupported;
        }

        private static string MakeRelative(string path, string basePath)
        {
            var relative = path.Remove(0, basePath.Length + 1);
            return relative;
        }
    }
}
