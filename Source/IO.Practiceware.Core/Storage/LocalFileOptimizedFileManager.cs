﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using IO.Practiceware.Services.FileService;
using Soaf;
using Soaf.ComponentModel;

namespace IO.Practiceware.Storage
{
    /// <summary>
    /// Vb6 was written in mind to operate over files saved locally on the machine, so it performs a lot of small reads, writes and exists checks.
    /// This class serves as performance optimization layer over <see cref="FileManager"/> to make most frequent operations execute faster.
    /// </summary>
    [Singleton]
    public sealed class LocalFileOptimizedFileManager : FileManager
    {
        private readonly object _syncObj = new object();
        private bool _isInitialized;
        private Dictionary<string, CachedFileInfo> _localFilesMap;

        public LocalFileOptimizedFileManager(IFileManagerConfiguration configuration, IFileManagerEngine engine) 
            : base(configuration, engine)
        {
        }

        public override void ClearCache()
        {
            base.ClearCache();

            lock (_syncObj)
            {
                _localFilesMap = null;
                _isInitialized = false;
            }
        }

        protected override void EnqueueOperation(IFileServiceOperation operation)
        {
            if (!IsLocalDataPathOperation(operation))
            {
                base.EnqueueOperation(operation);
                return;
            }

            CheckInitialized();
            
            // Try handle operation (all methods are aware that one of operation paths is local file path)
            bool handled;
            if (operation.GetType() == typeof(WriteOperation))
            {
                handled = TryHandleOperation(operation as WriteOperation);
            }
            else if (operation.GetType() == typeof(CopyOperation))
            {
                handled = TryHandleOperation(operation as CopyOperation);
            }
            else if (operation.GetType() == typeof(MoveOperation))
            {
                handled = TryHandleOperation(operation as MoveOperation);
            }
            else if (operation.GetType() == typeof(DeleteOperation))
            {
                handled = TryHandleOperation(operation as DeleteOperation);
            }
            else if (operation.GetType() == typeof(DeleteDirectoryOperation))
            {
                handled = TryHandleOperation(operation as DeleteDirectoryOperation);
            }
            else if (operation.GetType() == typeof (CreateDirectoryOperation))
            {
                handled = TryHandleOperation(operation as CreateDirectoryOperation);
            }
            else
            {
                throw new ArgumentException("When FileManager optimization for VB6 is used, all operation types must be handled. Attempted operation: " + operation);
            }

            // Still requires handling?
            if (!handled)
            {
                base.EnqueueOperation(operation);
            }
        }

        protected override TOperation ExecuteOperation<TOperation>(TOperation operation)
        {
            if (!IsLocalDataPathOperation(operation)
                || operation is ListDirectoriesOperation)
            {
                return base.ExecuteOperation(operation);
            }

            CheckInitialized();

            // Try handle operation (all methods are aware that one of operation paths is local file path)
            bool handled;
            if (operation.GetType() == typeof (ListFilesOperation))
            {
                handled = TryHandleOperation(operation as ListFilesOperation);
            }
            else if (operation.GetType() == typeof (FileExistsOperation))
            {
                handled = TryHandleOperation(operation as FileExistsOperation);
            }
            else if (operation.GetType() == typeof (ReadOperation))
            {
                handled = TryHandleOperation(operation as ReadOperation);
            }
            else if (operation.GetType() == typeof (DirectoryExistsOperation))
            {
                handled = TryHandleOperation(operation as DirectoryExistsOperation);
            }
            else if (operation.GetType() == typeof(GetFileInfosOperation))
            {
                handled = TryHandleOperation(operation as GetFileInfosOperation);
            }
            else
            {
                throw new ArgumentException("When FileManager optimization for VB6 is used, all operation types must be handled. Attempted operation: " + operation);
            }

            return handled ? operation : base.ExecuteOperation(operation);
        }

        #region Deferred execution operations (mostly to keep cached file listing in sync)

        private bool TryHandleOperation(DeleteDirectoryOperation operation)
        {
            // Remove all files in that path recursively
            lock (_syncObj)
            {
                // Support scenario when file is deleted and then directory is created with the same name
                if (_localFilesMap.ContainsKey(operation.DirectoryPath))
                {
                    _localFilesMap.Remove(operation.DirectoryPath);
                }

                // Locate all files in that path
                var cacheRecordsToRemove = _localFilesMap.Keys
                    .Where(p => p.StartsWith(operation.DirectoryPath, true, FileManagerConfiguration.OsCulture))
                    .ToArray();

                // Reflect removal in cache
                foreach (var s in cacheRecordsToRemove)
                {
                    var cacheInfo = _localFilesMap[s];
                    lock (cacheInfo)
                    {
                        cacheInfo.Reset(false);
            }
                }
            }
            return false;
        }

        private bool TryHandleOperation(CreateDirectoryOperation operation)
        {
            lock (_syncObj)
            {
                // Support scenario when file is deleted and then directory is created with the same name
                if (_localFilesMap.ContainsKey(operation.FilePath))
                {
                _localFilesMap.Remove(operation.FilePath);
            }
            }

            return false;
        }

        private bool TryHandleOperation(DeleteOperation operation)
        {
            // Remove deleted file from cache
            EnsureCachedFileInfo(operation.FilePath, false);
            return false;
        }

        private bool TryHandleOperation(MoveOperation operation)
        {
            ReflectCopyOfFileInCache(operation.SourcePath, operation.DestinationPath);

            // If source is local path and it doesn't match destination path -> delete it from cache
            if (IsLocalDataPath(operation.SourcePath)
                && !string.Equals(operation.SourcePath, operation.DestinationPath, StringComparison.OrdinalIgnoreCase))
            {
                EnsureCachedFileInfo(operation.SourcePath, false);
                }

            return false;
        }

        private bool TryHandleOperation(CopyOperation operation)
        {
            ReflectCopyOfFileInCache(operation.SourcePath, operation.DestinationPath);
            return false;
        }

        private bool TryHandleOperation(WriteOperation operation)
        {
            var cacheInfo = EnsureCachedFileInfo(operation.DestinationPath);

            // Enqueue optimized operation from here and return that requested operation was handled
            base.EnqueueOperation(new OptimizedWriteOperation(operation, cacheInfo));

            return true;
        }

        /// <summary>
        /// Helper method for Copy and Move operations handling (to apply these changes to cache)
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="destinationPath"></param>
        private void ReflectCopyOfFileInCache(string sourcePath, string destinationPath)
        {
            if (IsLocalDataPath(destinationPath))
            {
                if (IsLocalDataPath(sourcePath))
                {
                    lock (_syncObj)
                    {
                        CachedFileInfo sourceInfo;
                        if (_localFilesMap.TryGetValue(sourcePath, out sourceInfo))
                        {
                            lock (sourceInfo)
                            {
                                // If source doesn't exists -> operation result is unknown, so skip
                                if (sourceInfo.Exists)
                                {
                                    var destinationInfo = EnsureCachedFileInfo(destinationPath);
                                    lock (destinationInfo)
                                    {
                                        // Copy file cache information from source path
                                        destinationInfo.Sha1Hash = sourceInfo.Sha1Hash;
                                    }
                                }
                            }

                        }
                    }
                }
                else
                {
                    var cacheInfo = EnsureCachedFileInfo(destinationPath);
                    lock (cacheInfo)
                    {
                        // Copy destination exists, but we know nothing about it, so reset.
                        cacheInfo.Reset();
                    }
                }
            }
        }

        #endregion

        #region Immediate execution operations handling (mostly to bypass call to the server)

        private bool TryHandleOperation(ReadOperation operation)
        {
            var cacheInfo = EnsureCachedFileInfo(operation.FileServicePath);
            base.ExecuteOperation(new OptimizedReadOperation(operation, cacheInfo));

            // Handled
            return true;
        }

        private bool TryHandleOperation(FileExistsOperation operation)
        {
            lock (_syncObj)
            {
                CachedFileInfo cacheInfo;
                if (_localFilesMap.TryGetValue(operation.FilePath, out cacheInfo))
                {
                    lock (cacheInfo)
                    {
                        // Operation handled from cache
                        operation.Result = cacheInfo.Exists;
                    }
                    return true;
                }
            }

            // Execute operation and record it's result
            base.ExecuteOperation(operation);
            EnsureCachedFileInfo(operation.FilePath, operation.Result);

            return true;
        }

        private bool TryHandleOperation(DirectoryExistsOperation operation)
        {
            lock (_syncObj)
            {
                // If there is a file in that folder -> it exists for sure
                if (_localFilesMap.Any(k =>
                {
                    lock (k.Value)
                    {
                        var fileInDirectoryExists = k.Value.Exists
                            && k.Key.StartsWith(operation.FilePath, true, FileManagerConfiguration.OsCulture);
                        return fileInDirectoryExists;
                    }
                }))
                {
                    operation.Result = true;
                    return true;
                }
            }

            // Operation requires server roundtrip
            return false;
        }

        private bool TryHandleOperation(ListFilesOperation operation)
        {
            base.ExecuteOperation(operation);

            // Put all files operation listed into cache
            lock (_syncObj)
            {
                foreach (var localFilePath in operation.Result)
                {
                    EnsureCachedFileInfo(localFilePath);
                }
            }

            return true;
        }

        private bool TryHandleOperation(GetFileInfosOperation operation)
        {
            base.ExecuteOperation(operation);

            // Put all files operation listed into cache
            lock (_syncObj)
            {
                foreach (var fileInfo in operation.Result)
                {
                    EnsureCachedFileInfo(fileInfo.FullName, fileInfo.Exists);
                }
            }

            return true;
        }

        #endregion

        private CachedFileInfo EnsureCachedFileInfo(string path, bool exists = true)
        {
            CachedFileInfo cacheInfo;
            lock (_syncObj)
            {
                if (!_localFilesMap.TryGetValue(path, out cacheInfo))
                {
                    cacheInfo = new CachedFileInfo(exists);
                    _localFilesMap[path] = cacheInfo;
                }
                else
                {
                    lock (cacheInfo)
                    {
                        if (cacheInfo.Exists != exists)
                        {
                            cacheInfo.Reset(exists);
                        }
                    }
                }
            }
            return cacheInfo;
        }

        private bool IsLocalDataPath(string path)
        {
            return path.StartsWith(Configuration.FileServiceApplicationDataPath, true, FileManagerConfiguration.OsCulture);
        }

        private bool IsLocalDataPathOperation(IFileServiceOperation operation)
        {
            return operation.OperationPaths.Any(op => op.StartsWith(Configuration.FileServiceApplicationDataPath, true, FileManagerConfiguration.OsCulture));
        }

        private void CheckInitialized()
        {
            lock (_syncObj)
            {
                // Already initialized or request is not to local files
                if (_isInitialized) return;

                _localFilesMap = new Dictionary<string, CachedFileInfo>(FileManagerConfiguration.OsCultureStringComparer);
                _isInitialized = true;
            }
        }

        private class CachedFileInfo
        {
            private bool _exists;

            public CachedFileInfo(bool exists = true)
        {
                _exists = exists;
            }

            public byte[] Sha1Hash { get; set; }

            public bool Exists
            {
                get { return _exists; }
            }

            public void Reset(bool exists = true)
            {
                Sha1Hash = null;
                _exists = exists;
            }
        }

        private class OptimizedReadOperation : FileServiceOperationBase
        {
            private readonly ReadOperation _originalOperation;
            private readonly CachedFileInfo _cacheInfo;

            /// <summary>
            /// Initializes a new instance of the <see cref="OptimizedReadOperation"/> class.
            /// </summary>
            /// <param name="originalOperation">The original operation.</param>
            /// <param name="cacheInfo">The cache information.</param>
            public OptimizedReadOperation(ReadOperation originalOperation, CachedFileInfo cacheInfo)
            {
                _originalOperation = originalOperation;
                _cacheInfo = cacheInfo;
            }

            public override IEnumerable<string> OperationPaths
            {
                get { return _originalOperation.OperationPaths; }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                // Local copy exists and we know it's contents? -> no need to read from server
                lock (_cacheInfo)
                {
                    if (_cacheInfo.Sha1Hash != null && File.Exists(_originalOperation.LocalCopyFilePath))
                    {
                        TraceSource.TraceEvent(TraceEventType.Verbose, 0, "Optimized operation. Not forcing read from server, since file exists locally: {0}".FormatWith(_originalOperation.FileServicePath));
                        return;
                    }
                }

                // Execute original operation
                _originalOperation.Execute(fileService);

                lock (_cacheInfo)
                {
                    // File downloaded? If not -> mark that it doesn't exist
                    if (!File.Exists(_originalOperation.LocalCopyFilePath))
                    {
                        _cacheInfo.Reset(false);
                    }
                    else
                    {
                        // Save Sha1 hash of downloaded file
                        _cacheInfo.Sha1Hash = FileSystem.GetSha1Checksum(_originalOperation.LocalCopyFilePath);
                    }
                }
            }

            public override bool IsModifying
            {
                get { return _originalOperation.IsModifying; }
            }
        }

        private class OptimizedWriteOperation : FileServiceOperationBase
        {
            private readonly WriteOperation _originalOperation;
            private readonly CachedFileInfo _cacheInfo;

            /// <summary>
            /// Initializes a new instance of the <see cref="OptimizedReadOperation"/> class.
            /// </summary>
            /// <param name="originalOperation">The original operation.</param>
            /// <param name="cacheInfo">The cache information.</param>
            public OptimizedWriteOperation(WriteOperation originalOperation, CachedFileInfo cacheInfo)
            {
                _originalOperation = originalOperation;
                _cacheInfo = cacheInfo;
            }

            public override IEnumerable<string> OperationPaths
            {
                get { return _originalOperation.OperationPaths; }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                byte[] localCopySha1;
                lock (_cacheInfo)
                {
                    localCopySha1 = FileSystem.GetSha1Checksum(_originalOperation.LocalSourcePath);

                    // Sha1 hash matches last known server hash? -> no need to push update
                    if (_cacheInfo.Sha1Hash != null && _cacheInfo.Sha1Hash.SequenceEqual(localCopySha1))
                    {
                        TraceSource.TraceEvent(TraceEventType.Verbose, 0, "Skipping write. SHA1 hash matches for: {0}".FormatWith(_originalOperation.DestinationPath));
                        return;
                    }
                }

                // Execute write
                _originalOperation.Execute(fileService);

                lock (_cacheInfo)
                {
                    // Record new sha1 
                    _cacheInfo.Sha1Hash = localCopySha1;
                }
            }

            public override bool IsModifying
            {
                get { return _originalOperation.IsModifying; }
            }
        }
    }
}
