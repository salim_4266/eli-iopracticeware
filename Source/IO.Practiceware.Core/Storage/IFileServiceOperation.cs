using System;
using System.Collections.Generic;
using System.Threading;
using IO.Practiceware.Services.FileService;
using Soaf.Collections;

namespace IO.Practiceware.Storage
{
    /// <summary>
    /// An operation to be called on the IFileService that is recorded in an async writes queue. 
    /// Any paths in properties/fields in implementing classes should be stored after calling ToFileServiceRelativePath.
    /// </summary>
    public interface IFileServiceOperation
    {
        /// <summary>
        /// The path of the file we are operating on
        /// </summary>
        IEnumerable<string> OperationPaths { get; }

        /// <summary>
        /// Executes operation through provided file service
        /// </summary>
        /// <param name="fileService"></param>
        void Execute(IFileService fileService);

        /// <summary>
        /// Returns whether operations modifies contents of file system in any way
        /// </summary>
        bool IsModifying { get; }

        /// <summary>
        /// Gets a value indicating whether this operation has executed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has executed; otherwise, <c>false</c>.
        /// </value>
        bool HasExecuted { get; }

        /// <summary>
        /// Waits until operation has completed execution.
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        bool WaitUntilComplete(TimeSpan timeout);
    }

    /// <summary>
    /// Base class for file service operations which tracks whether operation has already executed
    /// </summary>
    public abstract class FileServiceOperationBase : IFileServiceOperation
    {
        private readonly ManualResetEventSlim _executedEvent = new ManualResetEventSlim(false);

        /// <summary>
        /// The path of the file we are operating on
        /// </summary>
        public abstract IEnumerable<string> OperationPaths { get; }

        /// <summary>
        /// Returns whether operations modifies contents of file system in any way
        /// </summary>
        public abstract bool IsModifying { get; }

        /// <summary>
        /// Executes the operation.
        /// </summary>
        /// <param name="fileService">The file service.</param>
        public abstract void ExecuteOperation(IFileService fileService);

        /// <summary>
        /// Executes operation through provided file service
        /// </summary>
        /// <param name="fileService"></param>
        public void Execute(IFileService fileService)
        {
            if (HasExecuted)
            {
                throw new InvalidOperationException("Operation can only execute once");
            }

            try
            {
                ExecuteOperation(fileService);
            }
            finally
            {
                HasExecuted = true;
                _executedEvent.Set();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this operation has executed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has executed; otherwise, <c>false</c>.
        /// </value>
        public bool HasExecuted { get; private set; }

        /// <summary>
        /// Waits until operation has completed execution.
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        public bool WaitUntilComplete(TimeSpan timeout)
        {
            return _executedEvent.Wait(timeout);
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", GetType().Name, OperationPaths.Join());
        }
    }
}