﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;

namespace IO.Practiceware.Storage
{
    /// <summary>
    ///     Helper methods for the file system.
    /// </summary>
    public static class FileSystem
    {
        /// <summary>
        ///     Returns all the source file contents as byte arrays.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static IEnumerable<byte[]> Contents(this IEnumerable<FileInfo> source)
        {
            return source.Select(f => f.FullName).Select(p => Soaf.IO.IO.ReadAllBytes(p));
        }

        /// <summary>
        ///     Returns the file contents as a byte array.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        public static byte[] Contents(this FileInfo file)
        {
            return TryReadAllBytes(file.FullName);
        }

        /// <summary>
        /// Parses path expression and splits it into path and expression parts
        /// </summary>
        /// <param name="pathWithSearchPattern">The path expression (could be a simple path or path with "wildcard" search pattern).</param>
        /// <param name="path">File or directory path</param>
        /// <param name="searchPattern">Expression specified in path. Defaults to "*"</param>
        public static void ParsePathWithSearchPattern(string pathWithSearchPattern, out string path, out string searchPattern)
        {
            path = pathWithSearchPattern;
            searchPattern = "*";

            // Path expression doesn't contain wildcards? -> return
            if (!(path.Contains('*') || path.Contains('?'))) return;

            var lastPathSeparator = Math.Max(path.LastIndexOf('\\'), path.LastIndexOf('/'));
            if (lastPathSeparator < path.Length - 1)
            {
                searchPattern = path.Substring(lastPathSeparator + 1, path.Length - lastPathSeparator - 1);
                path = path.Substring(0, lastPathSeparator);
            }
        }

        /// <summary>
        /// Lists files in a path using specified path expression.
        /// </summary>
        /// <param name="pathExpression">The path expression (could be a simple path or path with "wildcard" search pattern).</param>
        /// <param name="searchOption">The search option.</param>
        /// <returns></returns>
        public static string[] ListFiles(string pathExpression, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            return ListContents(pathExpression, false, searchOption);
        }

        /// <summary>
        /// Lists directories in a path using specified path expression.
        /// </summary>
        /// <param name="pathExpression">The path expression (could be a simple path or path with "wildcard" search pattern).</param>
        /// <param name="searchOption">The search option.</param>
        /// <returns></returns>
        public static string[] ListDirectories(string pathExpression, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            return ListContents(pathExpression, true, searchOption);
        }

        /// <summary>
        /// Lists path contents using specified path expression.
        /// </summary>
        /// <param name="pathExpression">The path expression (could be a simple path or path with "wildcard" search pattern).</param>
        /// <param name="directoriesInsteadOfFiles">if set to <c>true</c>, will list directories instead of files.</param>
        /// <param name="searchOption">The search option.</param>
        /// <returns></returns>
        private static string[] ListContents(string pathExpression, bool directoriesInsteadOfFiles, SearchOption searchOption)
        {
            // Break down path expression into path and search pattern (if specified)
            string searchPattern;
            string path;
            ParsePathWithSearchPattern(pathExpression, out path, out searchPattern);

            // If specified path doesn't exist as directory -> try treating last part in it as file
            // Info: What we are trying to accomplish here is support following types of path expressions
            //      1. ListContents("C:\Pinpoint\DirectoryInterface\Locked.txt") -> should list the file if directory with such name doesn't exist
            //      2. ListContents("C:\Pinpoint\DirectoryInterface") -> lists all files in a directory
            //      3. ListContents("C:\Pinpoint\DirectoryInterface\") -> lists all files in a directory
            if (!Directory.Exists(path) && File.Exists(path))
            {
                var filePath = path;
                path = Path.GetDirectoryName(filePath);
                searchPattern = Path.GetFileName(filePath);
            }

            // Still not a path to directory? -> there is nothing to return then
// ReSharper disable AssignNullToNotNullAttribute
            if (!Directory.Exists(path))
            {
                return new string[0];
            }

            // List
            var result = directoriesInsteadOfFiles
                ? Directory.GetDirectories(path, searchPattern, searchOption)
                : Directory.GetFiles(path, searchPattern, searchOption);
            return result;
// ReSharper restore AssignNullToNotNullAttribute
        }

        /// <summary>
        ///     Ensures that all file path parts exist.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="throwExceptionWhenInvalid">If the path is incorrectly formatted, will throw an exception if true.</param>
        /// <returns>Returns a flag indicating whether the path is valid and contains the corresponding directories (and file).</returns>
        public static bool EnsureFilePath(string path, bool throwExceptionWhenInvalid = false)
        {
            bool isValidPath = IsValidFilePath(path, throwExceptionWhenInvalid);
            if (isValidPath)
            {
                path = Path.GetFullPath(path);
                string directoryPath = Path.GetDirectoryName(path);
                if (directoryPath == null)
                {
                    if (throwExceptionWhenInvalid)
                    {
                        throw new Exception("Could not derive directory path from {0}".FormatWith(path));
                    }
                    return false;
                }

                // If directory doesn't exist -> create it
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
            }
            return true;
        }

        /// <summary>
        ///     Ensures that the directory exists at the given path.
        /// </summary>
        /// <param name="directory"></param>
        public static void EnsureDirectory(this DirectoryInfo directory)
        {
            EnsureDirectory(directory.FullName);
        }

        /// <summary>
        ///     Ensures that the directory exists at the given path.
        /// </summary>
        /// <param name="directoryPath"></param>
        public static void EnsureDirectory(string directoryPath)
        {
            directoryPath = Path.GetFullPath(directoryPath);
            // If directory doesn't exist -> create it
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
        }

        [DebuggerNonUserCode]
        public static bool IsValidFilePath(string path, bool throwExceptionWhenInvalid = false)
        {
            // Check if path contains any invalid path characters
            char[] invalidChars = Path.GetInvalidPathChars();
            bool containsInvalidChars = path.Any(invalidChars.Contains);
            if (containsInvalidChars)
            {
                if (throwExceptionWhenInvalid)
                {
                    throw new Exception("{0} contains an Invalid Path Character {1}".FormatWith(path, invalidChars));
                }
                return false;
            }

            // Also try leveraging FileInfo Constructor which checks for file path validity.
            try
            {
                // ReSharper disable UnusedVariable
                new FileInfo(path).EnsureNotDefault();
                // ReSharper restore UnusedVariable
            }
            catch (Exception ex)
            {
                if (throwExceptionWhenInvalid)
                {
                    var exception = new Exception("Provided path value of {0} is invalid.".FormatWith(path), ex);
                    throw exception;
                }
                return false;
            }

            return true;
        }

        public static bool IsNestedSubDirectory(this DirectoryInfo subDirectory, string directory)
        {
            while (subDirectory != null && !subDirectory.IsSubDirectory(directory))
            {
                subDirectory = subDirectory.Parent;
            }

            return subDirectory.IsSubDirectory(directory);
        }

        public static bool IsNestedSubDirectory(this DirectoryInfo subDirectory, DirectoryInfo directory)
        {
            return IsNestedSubDirectory(subDirectory, directory.FullName);
        }

        public static bool IsNestedSubDirectory(string subDirectory, string directory)
        {
            return IsNestedSubDirectory(new DirectoryInfo(subDirectory), directory);
        }

        public static bool IsSubDirectory(this DirectoryInfo subDirectory, string directory)
        {
            return subDirectory != null && subDirectory.Parent != null && subDirectory.Parent.FullName == directory;
        }

        public static bool IsSubDirectory(this DirectoryInfo subDirectory, DirectoryInfo directory)
        {
            return subDirectory.IsSubDirectory(directory.FullName);
        }

        public static bool IsSubDirectory(string subDirectory, string directory)
        {
            return new DirectoryInfo(subDirectory).IsSubDirectory(directory);
        }

        /// <summary>
        ///     Gets the MD5 Hash of the byte array contents for the given file path.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="throwException">If enabled, will throw an exception on errors.</param>
        /// <returns>The 16 byte MD5 hash.</returns>
        public static byte[] GetMd5Checksum(string filePath, bool throwException = false)
        {
            if (!File.Exists(filePath)) return null;

            try
            {
                using (var fileStream = TryReadAsStream(filePath))
                {
                    return fileStream.ComputeMd5Hash();
                }
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not open file or compute MD5 hash for {0}".FormatWith(filePath), ex);
                if (throwException) throw exception;

                Trace.TraceWarning(exception.ToString());
                return null;
            }
        }

        /// <summary>
        ///     Gets the SHA1 Hash of the byte array contents for the given file path.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="throwException">If enabled, will throw an exception on errors.</param>
        /// <returns>SHA1 hash.</returns>
        public static byte[] GetSha1Checksum(string filePath, bool throwException = false)
        {
            if (!File.Exists(filePath)) return null;

            try
            {
                using (var fileStream = TryReadAsStream(filePath))
                {
                    return fileStream.ComputeSha1Hash();
                }
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not open file or compute SHA1 hash for {0}".FormatWith(filePath), ex);
                if (throwException) throw exception;

                Trace.TraceWarning(exception.ToString());
                return null;
            }
        }

        /// <summary>
        /// Deletes the directory. Deletes directories and files recursively. Handles readonly files. Deletes one at a time
        /// because of known limitation in Directory.DeleteDirectory(path, true). See: [insert reference].
        /// Calls the deletionScope resolver before each file and directory to be deleted. Disposes the IDisposable upon
        /// completion of deleting that file/directory.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="silent">if set to <c>true</c> runs in silent mode and doesn't raise exceptions.</param>
        /// <exception cref="System.ApplicationException">Could not delete directory that contains the following content</exception>
        public static void DeleteDirectory(string directory, bool silent = false)
        {
            var directoryInfo = new DirectoryInfo(directory);
            if (!directoryInfo.Exists) return;

            var files = new FileInfo[0];
            try
            {
                files = directoryInfo.GetFiles();
            }
            catch (Exception ex)
            {
                if (!silent) throw;
                Trace.TraceWarning("Silent clean of directory partially failed. Error: {0}", ex);
            }

            var dirs = new DirectoryInfo[0];
            try
            {
                dirs = directoryInfo.GetDirectories();
            }
            catch (Exception ex)
            {
                if (!silent) throw;
                Trace.TraceWarning("Silent clean of directory partially failed. Error: {0}", ex);
            }


            // Delete all files in a directory
            foreach (var file in files)
            {
                try
                {
                    file.Attributes = FileAttributes.Normal;
                    TryDeleteFile(file.FullName);
                }
                catch (Exception ex)
                {
                    if (!silent) throw;
                    Trace.TraceWarning("Silent clean of directory partially failed. Error: {0}", ex);
                }
            }

            // Delete all subdirectories recursively
            foreach (var dir in dirs)
            {
                DeleteDirectory(dir.FullName, silent);
            }

            // Can't delete non-empty folders, so check beforehand
            var currentDirectoryContent = directoryInfo.EnumerateFileSystemInfos().ToArray();
            if (currentDirectoryContent.Any())
            {
                if (!silent)
                {
                    throw new ApplicationException("Could not delete directory {0} that contains the following content: {1}."
                        .FormatWith(directoryInfo.FullName, currentDirectoryContent.Select(s => s.FullName).Join()));
                }
            }
            else
            {
                try
                {
                    // Try delete folder
                    directoryInfo.Attributes.Remove(FileAttributes.ReadOnly);
                    RetryUtility.ExecuteWithRetry(() => directoryInfo.Delete(false), 5, TimeSpan.FromMilliseconds(20));
                }
                catch (Exception ex)
                {
                    if (!silent) throw;
                    Trace.TraceWarning("Silent clean of directory partially failed. Error: {0}", ex);
                }
            }
        }

        /// <summary>
        ///     Deletes the directory by calling FileSystem.DeleteDirectory and retries up to 5 times if unsuccessful.
        /// </summary>
        /// <remarks>
        ///     Windows does not release it's file locks immediately, so we sometimes encounter an exception that the file is in
        ///     use.
        ///     Retrying is a workaround to this low level issue.
        /// </remarks>
        /// <param name="directory">The directory.</param>
        [DebuggerNonUserCode]
        public static void TryDeleteDirectory(string directory)
        {
            RetryUtility.ExecuteWithRetry(() => DeleteDirectory(directory), 5, TimeSpan.FromMilliseconds(20));
        }

        /// <summary>
        ///     Reads the contents of a file into a byte array using the given path and retries up to 5 times if unsuccessful.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="nullIfNotExists">
        ///     if set to <c>true</c> returns null if the file doesn't exist instead of throwing an
        ///     error.
        /// </param>
        /// <returns></returns>
        /// <remarks>
        ///     Windows does not release it's file locks immediately, so we sometimes encounter an exception that the file is in
        ///     use.
        ///     Retrying is a workaround to this low level issue.
        /// </remarks>
        [DebuggerNonUserCode]
        public static byte[] TryReadAllBytes(string path, bool nullIfNotExists = false)
        {
            return RetryUtility.ExecuteWithRetry(() =>
                                                 {
                                                     if (!nullIfNotExists || File.Exists(path))
                                                         return File.ReadAllBytes(path);
                                                     return null;
                                                 }, 5, TimeSpan.FromMilliseconds(20));
        }

        [DebuggerNonUserCode]
        public static string TryReadAllText(string path, bool nullIfNotExists = false)
        {
            return RetryUtility.ExecuteWithRetry(() =>
            {
                if (!nullIfNotExists || File.Exists(path))
                    return Soaf.IO.IO.ReadAllText(path);
                return null;
            }, 5, TimeSpan.FromMilliseconds(20));
        }

        [DebuggerNonUserCode]
        public static FileStream TryReadAsStream(string path, bool nullIfNotExists = false)
        {
            return RetryUtility.ExecuteWithRetry(() =>
            {
                if (!nullIfNotExists || File.Exists(path))
                    return new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                return null;
            }, 5, TimeSpan.FromMilliseconds(20));
        }

        /// <summary>
        ///     Creates or overwrites a file at the given path, writes the specified data to it, and retries up to 5 times if
        ///     unsuccessful.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="data">The data.</param>
        /// <param name="ensureDirectory">if set to <c>true</c> ensures destination directory existence.</param>
        /// <remarks>
        ///     Windows does not release it's file locks immediately, so we sometimes encounter an exception that the file is in
        ///     use.
        ///     Retrying is a workaround to this low level issue.
        /// </remarks>
        [DebuggerNonUserCode]
        public static void TryWriteFile(string path, byte[] data, bool ensureDirectory = true)
        {
            if (ensureDirectory) EnsureFilePath(path);
            RetryUtility.ExecuteWithRetry(() => File.WriteAllBytes(path, data), 5, TimeSpan.FromMilliseconds(20));
        }

        /// <summary>
        ///     Creates or overwrites a file at the given path, writes the specified data to it, and retries up to 5 times if
        ///     unsuccessful.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="data">The data.</param>
        /// <param name="ensureDirectory">if set to <c>true</c> ensures destination directory existence.</param>
        /// <remarks>
        ///     Windows does not release it's file locks immediately, so we sometimes encounter an exception that the file is in
        ///     use.
        ///     Retrying is a workaround to this low level issue.
        /// </remarks>
        [DebuggerNonUserCode]
        public static void TryWriteFile(string path, string data, bool ensureDirectory = true)
        {
            if (ensureDirectory) EnsureFilePath(path);
            RetryUtility.ExecuteWithRetry(() => File.WriteAllText(path, data), 5, TimeSpan.FromMilliseconds(20));
        }

        public static void TryWriteFile(string path, Stream data, bool ensureDirectory = true)
        {
            if (ensureDirectory) EnsureFilePath(path);
            RetryUtility.ExecuteWithRetry(() =>
            {
                using (var targetFile = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    // Seek to start
                    if (data.CanSeek) data.Seek(0, SeekOrigin.Begin);

                    // Copy
                    data.CopyTo(targetFile);
                }
            }, 5, TimeSpan.FromMilliseconds(20));
        }

        /// <summary>
        ///     Copies an existing file to a new file and retries up to 5 times if unsuccessful.
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The destination path.</param>
        /// <param name="overwrite">if set to <c>true</c> overwrites file at destination.</param>
        /// <param name="ensureDirectory">if set to <c>true</c> ensures destination directory existence.</param>
        /// <param name="inheritAccessControl">Use inherited access control in destination</param>
        /// <remarks>
        ///     Windows does not release it's file locks immediately, so we sometimes encounter an exception that the file is in
        ///     use.
        ///     Retrying is a workaround to this low level issue.
        /// </remarks>
        [DebuggerNonUserCode]
        public static void TryCopyFile(string sourcePath, string destinationPath, bool overwrite = true, bool ensureDirectory = true, bool inheritAccessControl = true)
        {
            if (ensureDirectory) EnsureFilePath(destinationPath);

            RetryUtility.ExecuteWithRetry(() => File.Copy(sourcePath, destinationPath, overwrite), 5, TimeSpan.FromMilliseconds(20));

            // Now reset ACLs with retries
            if (inheritAccessControl)
            {
                ResetFilePermissions(destinationPath, true);
            }
        }

        /// <summary>
        /// Moves an existing file to a new location and retries up to 5 times if unsuccessful.
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The destination path.</param>
        /// <param name="overwrite">if set to <c>true</c> [overwrite].</param>
        /// <param name="ensureDirectory">if set to <c>true</c> ensures destination directory existence.</param>
        /// <param name="inheritAccessControl">if set to <c>true</c> [inherit access control].</param>
        /// <remarks>
        /// Windows does not release it's file locks immediately, so we sometimes encounter an exception that the file is in
        /// use.
        /// Retrying is a workaround to this low level issue.
        /// </remarks>
        [DebuggerNonUserCode]
        public static void TryMoveFile(string sourcePath, string destinationPath, bool overwrite = true, bool ensureDirectory = true, bool inheritAccessControl = true)
        {
            if (ensureDirectory) EnsureFilePath(destinationPath);

            // First move file with retries
            RetryUtility.ExecuteWithRetry(() =>
            {
                if (overwrite) TryDeleteFile(destinationPath, true);
                File.Move(sourcePath, destinationPath);
            }, 5, TimeSpan.FromMilliseconds(20));

            // Now reset ACLs with retries
            if (inheritAccessControl)
            {
                ResetFilePermissions(destinationPath, true);
            }
        }

        /// <summary>
        /// Moves an existing directory to a new location and retries up to 5 times if unsuccessful.
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The destination path.</param>
        /// <param name="deleteDestination">if set to <c>true</c> [overwrite].</param>
        /// <param name="ensureDirectory">if set to <c>true</c> [ensure directory].</param>
        [DebuggerNonUserCode]
        public static void TryMoveDirectory(string sourcePath, string destinationPath, bool deleteDestination = true, bool ensureDirectory = true)
        {
            if (ensureDirectory) EnsureFilePath(destinationPath);

            // Try move directory with retries
            RetryUtility.ExecuteWithRetry(() =>
            {
                if (deleteDestination) TryDeleteDirectory(destinationPath);
                Directory.Move(sourcePath, destinationPath);
            }, 5, TimeSpan.FromMilliseconds(20));
        }

        /// <summary>
        /// Reset file permissions back to default inherited ones
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="ignoreUnathorizedErrors"></param>
        [DebuggerNonUserCode]
        public static void ResetFilePermissions(string filePath, bool ignoreUnathorizedErrors = false)
        {
            RetryUtility.ExecuteWithRetry(() =>
            {
                try
                {
                    var fs = File.GetAccessControl(filePath);
                    fs.SetAccessRuleProtection(false, false);
                    File.SetAccessControl(filePath, fs);
                }
                    // Ignore unauthorized exceptions
                catch (UnauthorizedAccessException)
                {
                    if (!ignoreUnathorizedErrors) throw;
                }
            }, 5, TimeSpan.FromMilliseconds(20));
        }

        /// <summary>
        ///     Deletes the specified file and retries up to 5 times if unsuccessful.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="checkExists">if set to <c>true</c> checks if the file exists before deleting.</param>
        /// <remarks>
        ///     Windows does not release it's file locks immediately, so we sometimes encounter an exception that the file is in
        ///     use.
        ///     Retrying is a workaround to this low level issue.
        /// </remarks>
        [DebuggerNonUserCode]
        public static void TryDeleteFile(string path, bool checkExists = false)
        {
            RetryUtility.ExecuteWithRetry(() => { if (!checkExists || File.Exists(path)) File.Delete(path); }, 5, TimeSpan.FromMilliseconds(20));
        }

        /// <summary>
        /// Returns file information
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static FileInfoRecord GetFileInfo(string filePath)
        {
            var fileInfo = new FileInfo(filePath);
            return GetFileInfo(fileInfo);
        }

        /// <summary>
        /// Returns file information
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public static FileInfoRecord GetFileInfo(FileInfo fileInfo)
        {
            var infoRecord = new FileInfoRecord
            {
                FullName = fileInfo.FullName,
                Exists = fileInfo.Exists
            };

            // Fill in other part of information if it exists
            if (infoRecord.Exists)
            {
                infoRecord.Size = fileInfo.Length;
                infoRecord.CreationTimeUtc = fileInfo.CreationTimeUtc;
                infoRecord.LastWriteTimeUtc = fileInfo.LastWriteTimeUtc;
                infoRecord.LastAccessTimeUtc = fileInfo.LastAccessTimeUtc;
            }

            return infoRecord;
        }

        /// <summary>
        /// Converts path to a full path. Unlike Path.GetFullName -> will not throw exception on path expressions
        /// </summary>
        /// <param name="path">Path or path expression (with wildcards)</param>
        /// <returns></returns>
        public static string GetFullPath(string path)
        {
            // Check if path represents at least something
            if (string.IsNullOrEmpty(path)) return path;

            // Check if file name contains any special characters
            var fileName = Path.GetFileName(path);
            if (string.IsNullOrEmpty(fileName) // No file name
                || !Path.GetInvalidFileNameChars().Any(fileName.Contains)) // No invalid characters
            {
                // Path is safe to convert to full path
                return Path.GetFullPath(path);
            }

            // Retrieve directory name
            var directoryName = Path.GetDirectoryName(path);
            if (string.IsNullOrEmpty(directoryName)) return path;

            // Convert directory to full path and combine back with file name
            directoryName = Path.GetFullPath(directoryName);
            var resultPath = Path.Combine(directoryName, fileName);

            // Ensure trailing backslash is preserved
            if (path.EndsWith(@"\") && !resultPath.EndsWith(@"\"))
            {
                resultPath += @"\";
            }
            return resultPath;
        }

        /// <summary>
        /// Searches the file in folder and it's parents.
        /// </summary>
        /// <param name="folderToCheck">The folder to check.</param>
        /// <param name="fileToFind">The file to find.</param>
        /// <param name="maxLevelsToGoUp">The maximum levels to go up.</param>
        /// <returns></returns>
        public static string SearchFileInFolderAndUp(string folderToCheck, string fileToFind, int maxLevelsToGoUp = int.MaxValue)
        {
            var currentDirectory = new DirectoryInfo(folderToCheck);

            string result = null;
            int currentLevel = -1;
            while (currentDirectory != null
                && currentDirectory.Exists
                && ++currentLevel <= maxLevelsToGoUp)
            {
                var testFileLocation = Path.Combine(currentDirectory.FullName, fileToFind);
                if (File.Exists(testFileLocation))
                {
                    result = testFileLocation;
                    break;
                }

                currentDirectory = currentDirectory.Parent;
            }

            return result;
        }

        /// <summary>
        /// Gets path to IOP temp location
        /// </summary>
        /// <returns></returns>
        public static string GetTempPath()
        {
            // Use dedicated Temp folder for IOP files
            var iopTemp = Path.Combine(Path.GetTempPath(), "IOP\\Temp");
            EnsureDirectory(iopTemp);
            return iopTemp;
        }

        /// <summary>
        /// Gets the name of the temporary file. Does not use file system API - combines temp path with a guid.
        /// </summary>
        /// <returns></returns>
        public static string GetTempPathName()
        {
            var iopTemp = GetTempPath();

            // Generate unique file name
            var tempFilePath = Path.Combine(iopTemp, Guid.NewGuid().ToString("N") + ".tmp");
            return tempFilePath;
        }

        public static readonly char[] InvalidPathCharacters = Path.GetInvalidFileNameChars().Union(Path.GetInvalidPathChars()).ToArray();

        /// <summary>
        /// Removes all invalid path characters from specified name
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string ToSafePathName(this string name)
        {
            var result = name.RemoveAll(InvalidPathCharacters);
            return result;
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern int GetShortPathName([MarshalAs(UnmanagedType.LPTStr)] string path, [MarshalAs(UnmanagedType.LPTStr)] StringBuilder shortPath, int shortPathLength);
    }
}