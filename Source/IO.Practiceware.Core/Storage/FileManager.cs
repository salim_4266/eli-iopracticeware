﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using IO.Practiceware.Configuration;
using IO.Practiceware.Services.FileService;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.ComponentModel;

[assembly: Component(typeof(FileManager), typeof(IFileManager))]
[assembly: Component(typeof(FileManager.Initializer), Initialize = true)]

namespace IO.Practiceware.Storage
{
    /// <summary>
    ///     These are more generic methods in the Interop dll which VB6 will call via the IVB6FileManager to interact  with files and the file system. 
    /// Implementation should use caching and async writes queue for performance.
    /// </summary>
    public interface IFileManager
    {
        /// <summary>
        /// Downloads the file to a redirected version of the specified path
        /// </summary>
        /// <remarks>Note: it must return actual file path (redirected or not), since VB6 will be calling Open</remarks>
        /// <param name="path">The path.</param>
        string Read(string path);

        /// <summary>
        /// Commits the specified file to the IFileService
        /// </summary>
        /// <param name="path">The path.</param>
        void Commit(string path);

        /// <summary>
        /// Copies the specified source path to the specified destination path
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The destination path.</param>
        void Copy(string sourcePath, string destinationPath);

        /// <summary>
        /// Copies the specified source path to the specified destination path
        /// </summary>
        /// <param name="path">The path.</param>
        void Delete(string path);

        /// <summary>
        /// Moves the specified source path to the destination path
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The destination path.</param>
        void Move(string sourcePath, string destinationPath);

        /// <summary>
        /// Checks if the specified file exists
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        bool FileExists(string path);

        /// <summary>
        /// Checks if the specified directory exists
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        bool DirectoryExists(string path);

        /// <summary>
        /// Creates a directory at the specified path
        /// </summary>
        /// <param name="path">The path.</param>
        void CreateDirectory(string path);

        /// <summary>
        /// Deletes directory with all files. Recursively.
        /// </summary>
        /// <param name="path"></param>
        void DeleteDirectory(string path);

        /// <summary>
        /// Gets all files matching the search pattern.  Note the path contains the search pattern.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="fileNamesOnly">if set to <c>true</c> returns file names only, not the full path.</param>
        /// <returns></returns>
        string[] ListFiles(string path, bool fileNamesOnly = false);

        /// <summary>
        /// Gets all directories matching the search pattern.  Note the path contains the search pattern.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="directoryNamesOnly">if set to <c>true</c> directory names only, not the full path.</param>
        /// <returns></returns>
        string[] ListDirectories(string path, bool directoryNamesOnly = false);

        /// <summary>
        /// Clears all locally cached files and directories at the redirected root.
        /// </summary>
        void ClearCache();

        /// <summary>
        /// Retrieves file information for files at specified path
        /// </summary>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        FileInfoRecord[] GetFileInfos(params string[] filePaths);

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        IFileManagerConfiguration Configuration { get; }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <value>
        /// The engine.
        /// </value>
        IFileManagerEngine Engine { get; }
    }

    /// <summary>
    /// 
    /// </summary>
    [Singleton]
    public class FileManager : IFileManager
    {
        public const string TraceSourceName = "FileManager";
        public static readonly TraceSource TraceSource = new TraceSource(TraceSourceName);

        public class Initializer
        {
            public Initializer(Func<IFileManager> fileManagerConstruct)
            {
                SetSharedInstance(fileManagerConstruct());
            }

            /// <summary>
            /// Changes value of FileManager.Instance
            /// </summary>
            /// <param name="fileManager">The file manager.</param>
            public static void SetSharedInstance(IFileManager fileManager)
            {
                Instance = fileManager;
            }
        }

        /// <summary>
        /// Current file manager
        /// </summary>
        public static IFileManager Instance { get; private set; }

        /// <summary>
        /// Configuration
        /// </summary>
        public IFileManagerConfiguration Configuration { get; private set; }

        /// <summary>
        /// Engine
        /// </summary>
        public IFileManagerEngine Engine { get; private set; }

        public FileManager(IFileManagerConfiguration configuration, IFileManagerEngine engine)
        {
            Engine = engine;
            Configuration = configuration;
        }

        public string Read(string path)
        {
            try
            {
                TraceSource.TraceInformation("FileManager.Read({0})".FormatWith(path));

                var redirectedPath = Configuration.ToRedirectedPath(path);
                if (!Configuration.IsRedirectedPath(redirectedPath))
                {
                    return redirectedPath;
                }

                var pendingOperation = new ReadOperation
                {
                    FileServicePath = Configuration.ToFileServiceRelativePath(redirectedPath),
                    LocalCopyFilePath = redirectedPath
                };

                ExecuteOperation(pendingOperation);
                return redirectedPath;
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not read file from {0}.".FormatWith(path), ex);
                TraceSource.TraceEvent(TraceEventType.Error, 0, exception.ToString());
                throw exception;
            }
        }

        public void Commit(string path)
        {
            try
            {
                TraceSource.TraceInformation("FileManager.Commit({0}).".FormatWith(path));

                var redirectedPath = Configuration.ToRedirectedPath(path);
                if (!Configuration.IsRedirectedPath(redirectedPath))
                {
                    return;
                }

                EnqueueOperation(new WriteOperation(redirectedPath, Configuration.ToFileServiceRelativePath(redirectedPath)));
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not commit file at {0}".FormatWith(path), ex);
                TraceSource.TraceEvent(TraceEventType.Error, 0, exception.ToString());
                throw exception;
            }
        }

        public void Delete(string path)
        {
            try
            {
                TraceSource.TraceInformation("FileManager.Delete({0}).".FormatWith(path));

                var redirectedPath = Configuration.ToRedirectedPath(path);
                var isRedirectedPath = Configuration.IsRedirectedPath(redirectedPath);

                Engine.WaitForIncompleteOperations(redirectedPath);
                FileSystem.TryDeleteFile(redirectedPath, isRedirectedPath);

                if (Configuration.IsRedirectedPath(redirectedPath))
                {
                    EnqueueOperation(new DeleteOperation
                    {
                        FilePath = Configuration.ToFileServiceRelativePath(redirectedPath)
                    });
                }
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not delete file at {0}.".FormatWith(path), ex);
                TraceSource.TraceEvent(TraceEventType.Error, 0, exception.ToString());
                throw exception;
            }
        }

        public void Copy(string sourcePath, string destinationPath)
        {
            try
            {
                TraceSource.TraceInformation("FileManager.Copy({0}, {1}).".FormatWith(sourcePath, destinationPath));

                var redirectedSourcePath = Configuration.ToRedirectedPath(sourcePath);
                var redirectedDestinationPath = Configuration.ToRedirectedPath(destinationPath);
                bool isDirectSourcePath = !Configuration.IsRedirectedPath(redirectedSourcePath);
                bool isDirectDestinationPath = !Configuration.IsRedirectedPath(redirectedDestinationPath);

                // Ensure latest local copy exists when copying from redirected to local direct path
                if (isDirectDestinationPath && !isDirectSourcePath)
                {
                    TraceSource.TraceEvent(TraceEventType.Verbose, 0,
                        "Reading file into {0} from server for Copy operation", redirectedSourcePath);

                    // Download file from server first
                    var pendingOperation = new ReadOperation
                    {
                        FileServicePath = Configuration.ToFileServiceRelativePath(redirectedSourcePath),
                        LocalCopyFilePath = redirectedSourcePath
                    };
                    ExecuteOperation(pendingOperation);
                }

                Engine.WaitForIncompleteOperations(redirectedSourcePath, redirectedDestinationPath);
                if (isDirectSourcePath || File.Exists(redirectedSourcePath))
                {
                    FileSystem.TryCopyFile(redirectedSourcePath, redirectedDestinationPath);
                }

                // Copying outside of redirection directories -> can't reflect server side
                if (isDirectDestinationPath)
                {
                    return;
                }

                if (isDirectSourcePath)
                {
                    // This is a special case, where a non-redirected file is being moved into a redirected path.
                    // To accommodate this, we will perform a write to the file service, instead of performing a move.

                    // Using destination path, since we already copied file there
                    EnqueueOperation(new WriteOperation(redirectedDestinationPath,
                        Configuration.ToFileServiceRelativePath(redirectedDestinationPath)));
                }
                else
                {
                    EnqueueOperation(new CopyOperation
                    {
                        DestinationPath = Configuration.ToFileServiceRelativePath(redirectedDestinationPath),
                        SourcePath = Configuration.ToFileServiceRelativePath(redirectedSourcePath)
                    });
                }
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not copy file from {0} to {1}.".FormatWith(sourcePath, destinationPath), ex);
                TraceSource.TraceEvent(TraceEventType.Error, 0, exception.ToString());
                throw exception;
            }
        }

        public void Move(string sourcePath, string destinationPath)
        {
            try
            {
                TraceSource.TraceInformation("FileManager.Move({0}, {1}).".FormatWith(sourcePath, destinationPath));

                var redirectedSourcePath = Configuration.ToRedirectedPath(sourcePath);
                var redirectedDestinationPath = Configuration.ToRedirectedPath(destinationPath);
                bool isDirectSourcePath = !Configuration.IsRedirectedPath(redirectedSourcePath);
                bool isDirectDestinationPath = !Configuration.IsRedirectedPath(redirectedDestinationPath);

                // Ensure latest local copy exists when moving from redirected to local direct path
                if (isDirectDestinationPath && !isDirectSourcePath)
                {
                    TraceSource.TraceEvent(TraceEventType.Verbose, 0,
                        "Reading file into {0} from server for Move operation", redirectedSourcePath);

                    // Download file from server first
                    var pendingOperation = new ReadOperation
                    {
                        FileServicePath = Configuration.ToFileServiceRelativePath(redirectedSourcePath),
                        LocalCopyFilePath = redirectedSourcePath
                    };
                    ExecuteOperation(pendingOperation);
                }

                Engine.WaitForIncompleteOperations(redirectedSourcePath, redirectedDestinationPath);
                if (isDirectSourcePath || File.Exists(redirectedSourcePath))
                {
                    // Move to new location
                    FileSystem.TryMoveFile(redirectedSourcePath, redirectedDestinationPath);
                }

                if (isDirectDestinationPath)
                {
                    // If source is redirected -> delete it on server
                    if (!isDirectSourcePath)
                    {
                        TraceSource.TraceEvent(TraceEventType.Verbose, 0,
                            "Deleting moved file {0} from server", redirectedSourcePath);

                        EnqueueOperation(new DeleteOperation
                        {
                            FilePath = Configuration.ToFileServiceRelativePath(redirectedSourcePath)
                        });
                    }
                    return;
                }

                if (isDirectSourcePath)
                {
                    // This is a special case, where a non-redirected file is being moved into a redirected path.
                    // To accommodate this, we will perform a write to the file service, instead of performing a move.

                    // Using destination path, since we already moved file there
                    EnqueueOperation(new WriteOperation(redirectedDestinationPath,
                        Configuration.ToFileServiceRelativePath(redirectedDestinationPath)));
                }
                else
                {
                    EnqueueOperation(new MoveOperation
                    {
                        DestinationPath = Configuration.ToFileServiceRelativePath(redirectedDestinationPath),
                        SourcePath = Configuration.ToFileServiceRelativePath(redirectedSourcePath)
                    });
                }
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not move file from {0} to {1}.".FormatWith(sourcePath, destinationPath), ex);
                TraceSource.TraceEvent(TraceEventType.Error, 0, exception.ToString());
                throw exception;
            }
        }

        public bool FileExists(string path)
        {
            TraceSource.TraceInformation("FileManager.FileExists({0}).".FormatWith(path));

            var redirectedPath = Configuration.ToRedirectedPath(path);
            bool result;
            if (!Configuration.IsRedirectedPath(redirectedPath))
            {
                result = File.Exists(redirectedPath);
            }
            else
            {
                var pendingOperation = new FileExistsOperation
                {
                    FilePath = Configuration.ToFileServiceRelativePath(redirectedPath)
                };

                ExecuteOperation(pendingOperation);
                result = pendingOperation.Result;
            }

            TraceSource.TraceInformation("FileManager.FileExists({0}) = {1}.".FormatWith(path, result));
            return result;
        }

        public bool DirectoryExists(string path)
        {
            TraceSource.TraceInformation("FileManager.DirectoryExists({0}).".FormatWith(path));

            var redirectedPath = Configuration.ToRedirectedPath(path);
            bool result;
            if (!Configuration.IsRedirectedPath(redirectedPath))
            {
                result = Directory.Exists(redirectedPath);
            }
            else
            {
                var pendingOperation = new DirectoryExistsOperation
                {
                    FilePath = Configuration.ToFileServiceRelativePath(redirectedPath)
                };

                ExecuteOperation(pendingOperation);
                result = pendingOperation.Result;

                // If directory exists on the server -> ensure all parts of path exist locally as well
                if (result)
                {
                    FileSystem.EnsureDirectory(redirectedPath);
                }
            }

            TraceSource.TraceInformation("FileManager.Exists({0}) = {1}.".FormatWith(path, result));
            return result;
        }

        public void CreateDirectory(string path)
        {
            TraceSource.TraceInformation("FileManager.CreateDirectory({0}).".FormatWith(path));

            // Create locally first
            var redirectedPath = Configuration.ToRedirectedPath(path);
            Engine.WaitForIncompleteOperations(redirectedPath);
            FileSystem.EnsureDirectory(redirectedPath);

            if (Configuration.IsRedirectedPath(redirectedPath))
            {
                EnqueueOperation(new CreateDirectoryOperation
                {
                    FilePath = Configuration.ToFileServiceRelativePath(redirectedPath)
                });
            }
        }

        public void DeleteDirectory(string path)
        {
            TraceSource.TraceInformation("FileManager.DeleteDirectory({0}).".FormatWith(path));

            var redirectedPath = Configuration.ToRedirectedPath(path);

            Engine.WaitForIncompleteOperations(redirectedPath);
            FileSystem.DeleteDirectory(redirectedPath);
            if (!Configuration.IsRedirectedPath(redirectedPath)) return;

            // Delete excluded from redirection files in the requested path
            var exclusionsInTarget = Configuration.ExcludedPaths
                .Where(p => p.Path.StartsWith(path, true, FileManagerConfiguration.OsCulture))
                .ToList();
            foreach (var excludedPath in exclusionsInTarget)
            {
                if (excludedPath.IsDirectory)
                {
                    FileSystem.TryDeleteDirectory(excludedPath.Path);
                }
                else
                {
                    FileSystem.TryDeleteFile(excludedPath.Path, true);
                }
            }

            // Delete server-side
            EnqueueOperation(new DeleteDirectoryOperation
            {
                DirectoryPath = Configuration.ToFileServiceRelativePath(redirectedPath),
            });
        }

        public string[] ListFiles(string path, bool fileNameOnly = false)
        {
            TraceSource.TraceInformation("FileManager.ListFiles({0},{1}).".FormatWith(path, fileNameOnly));

            var redirectedPath = Configuration.ToRedirectedPath(path);

            string[] result;
            if (!Configuration.IsRedirectedPath(redirectedPath))
            {
                result = FileSystem.ListFiles(redirectedPath);
            }
            else
            {
                var pendingOperation = new ListFilesOperation
                {
                    Path = Configuration.ToFileServiceRelativePath(redirectedPath)
                };

                ExecuteOperation(pendingOperation);
                result = pendingOperation.Result;

                // Convert back to local path
                var modifiedResult = result.Select(Configuration.FromFileServiceToLocalPath).ToList();

                // Mixin excluded files in target path if they exist and match search pattern
                string operationPath;
                string searchPattern;
                FileSystem.ParsePathWithSearchPattern(path, out operationPath, out searchPattern);
                var exclusionsInTarget = Configuration.ExcludedPaths
                    .Where(p => !p.IsDirectory // files only
                        && FileManagerConfiguration.OsCultureStringComparer.Compare(p.ParentDirectory, operationPath) == 0)
                    .ToDictionary(p => p.Path, p => p.IsDirectory);
                if (exclusionsInTarget.Any())
                {
                    var localFiles = FileSystem.ListFiles(path);
                    modifiedResult.AddRange(localFiles.Where(exclusionsInTarget.ContainsKey));
                }

                result = modifiedResult.ToArray();
            }

            if (fileNameOnly) result = result.Select(Path.GetFileName).ToArray();

            TraceSource.TraceInformation("FileManager.ListFiles({0}) = Found {1} files.".FormatWith(path, result.Length));

            return result;
        }

        public string[] ListDirectories(string path, bool directoryNamesOnly = false)
        {
            TraceSource.TraceInformation("FileManager.ListDirectories({0},{1}).".FormatWith(path, directoryNamesOnly));

            var redirectedPath = Configuration.ToRedirectedPath(path);

            string[] result;
            if (!Configuration.IsRedirectedPath(redirectedPath))
            {
                result = FileSystem.ListDirectories(redirectedPath);
            }
            else
            {
                var pendingOperation = new ListDirectoriesOperation
                {
                    Path = Configuration.ToFileServiceRelativePath(redirectedPath)
                };

                ExecuteOperation(pendingOperation);
                result = pendingOperation.Result;

                // Convert back to local path
                var modifiedResult = result.Select(Configuration.FromFileServiceToLocalPath).ToList();

                // Mixin excluded folders in target path if they exist and match search pattern
                string operationPath;
                string searchPattern;
                FileSystem.ParsePathWithSearchPattern(path, out operationPath, out searchPattern);
                var exclusionsInTarget = Configuration.ExcludedPaths
                    .Where(p => p.IsDirectory // directories only
                        && FileManagerConfiguration.OsCultureStringComparer.Compare(p.ParentDirectory, operationPath) == 0)
                    .ToDictionary(p => p.Path, p => p.IsDirectory);
                if (exclusionsInTarget.Any())
                {
                    var localDirectories = FileSystem.ListDirectories(path);
                    modifiedResult.AddRange(localDirectories.Where(exclusionsInTarget.ContainsKey));
                }

                result = modifiedResult.ToArray();
            }

            if (directoryNamesOnly) result = result.Select(d => new DirectoryInfo(d).Name).ToArray();

            TraceSource.TraceInformation("FileManager.ListDirectories({0}) = Found {1} directories.".FormatWith(path, result.Length));

            return result;
        }

        public virtual void ClearCache()
        {
            try
            {
                TraceSource.TraceInformation("FileManager.Clear {0}.".FormatWith(Configuration.RedirectedRootPath));

                if (ConfigurationManager.ClientApplicationConfiguration.TreatLocaFilesAsTransient)
                {
                    // Wipe local files server-side
                    EnqueueOperation(new DeleteDirectoryOperation
                    {
                        DirectoryPath = @"\" + Configuration.LocalFilesRelativePath
                    });
                }

                // Hold of until pending operations are complete
                Engine.WaitForIncompleteOperations();

                // Cleanup (do not throw exceptions and clean as much as possible)
                FileSystem.DeleteDirectory(Configuration.RedirectedRootPath, true);
            }
            catch (Exception ex)
            {
                var exception = new Exception("Could not clear {0}.".FormatWith(Configuration.RedirectedRootPath), ex);
                TraceSource.TraceEvent(TraceEventType.Error, 0, exception.ToString());
                throw exception;
            }
        }

        public FileInfoRecord[] GetFileInfos(params string[] filePaths)
        {
            TraceSource.TraceInformation("FileManager.GetFileInfos({0}).".FormatWith(filePaths.Join()));

            // Sort file paths into redirected and non-redirected
            var redirectedFilePaths = filePaths
                .Select(Configuration.ToRedirectedPath)
                .ToList();
            var nonRedirectedFilePaths = redirectedFilePaths
                .Where(fp => !Configuration.IsRedirectedPath(fp))
                .ToArray();
            redirectedFilePaths.RemoveAll(nonRedirectedFilePaths);

            // Fetch information for local files
            var result = new List<FileInfoRecord>();
            foreach (var nonRedirectedFilePath in nonRedirectedFilePaths)
            {
                var fileTimestampRecord = FileSystem.GetFileInfo(nonRedirectedFilePath);
                result.Add(fileTimestampRecord);
            }

            // Fetch information for redirected files
            if (redirectedFilePaths.Any())
            {
                var pendingOperation = new GetFileInfosOperation
                {
                    FilePaths = redirectedFilePaths.Select(Configuration.ToFileServiceRelativePath).ToArray()
                };

                ExecuteOperation(pendingOperation);

                // Convert back to local path 
                foreach (var fileInfo in pendingOperation.Result)
                {
                    fileInfo.FullName = Configuration.FromFileServiceToLocalPath(fileInfo.FullName);
                }

                result.AddRange(pendingOperation.Result);
            }

            TraceSource.TraceInformation("FileManager.GetFileInfos Done");

            return result.ToArray();
        }

        /// <summary>
        /// Enqueues operation execution
        /// </summary>
        /// <param name="operation">The operation.</param>
        protected virtual void EnqueueOperation(IFileServiceOperation operation)
        {
            Engine.EnqueueOperation(operation);
        }

        /// <summary>
        /// Executes operation and returns once complete.
        /// </summary>
        /// <param name="operation">The operation.</param>
        protected virtual TOperation ExecuteOperation<TOperation>(TOperation operation)
            where TOperation : IFileServiceOperation
        {
            Engine.EnqueueOperation(operation);
            Engine.WaitForIncompleteOperations(operation);
            return operation;
        }

        #region FileServiceOperations

        protected class CopyOperation : FileServiceOperationBase
        {
            public string SourcePath { get; set; }
            public string DestinationPath { get; set; }

            public override IEnumerable<string> OperationPaths
            {
                get
                {
                    yield return SourcePath;
                    yield return DestinationPath;
                }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                RetryUtility.ExecuteWithRetry(() => fileService.Copy(SourcePath, DestinationPath), 5, TimeSpan.FromMilliseconds(500));
            }

            public override bool IsModifying 
            {
                get { return true; }
            }
        }

        /// <summary>
        /// An operation to move the specified source path to the specified destination path using IFileService.Copy.
        /// </summary>
        protected class MoveOperation : FileServiceOperationBase
        {
            public string SourcePath { get; set; }
            public string DestinationPath { get; set; }

            public override IEnumerable<string> OperationPaths
            {
                get
                {
                    yield return SourcePath;
                    yield return DestinationPath;
                }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                RetryUtility.ExecuteWithRetry(() => fileService.Move(SourcePath, DestinationPath), 5, TimeSpan.FromMilliseconds(500));
            }

            public override bool IsModifying
            {
                get { return true; }
            }
        }

        /// <summary>
        /// An operation to write the specified file data to the ServerRelativePath using IFileService.Delete.
        /// </summary>
        protected class DeleteOperation : FileServiceOperationBase
        {
            public string FilePath { get; set; }

            public override IEnumerable<string> OperationPaths
            {
                get { yield return FilePath; }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                RetryUtility.ExecuteWithRetry(() => fileService.Delete(FilePath), 5, TimeSpan.FromMilliseconds(500));
            }

            public override bool IsModifying
            {
                get { return true; }
            }
        }

        protected class CreateDirectoryOperation : FileServiceOperationBase
        {
            public string FilePath { get; set; }

            public override IEnumerable<string> OperationPaths
            {
                get { yield return FilePath; }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                RetryUtility.ExecuteWithRetry(() => fileService.CreateDirectory(FilePath), 5, TimeSpan.FromMilliseconds(500));
            }

            public override bool IsModifying
            {
                get { return true; }
            }
        }

        protected class DeleteDirectoryOperation : FileServiceOperationBase
        {
            public string DirectoryPath { get; set; }

            public override IEnumerable<string> OperationPaths
            {
                get { yield return DirectoryPath; }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                RetryUtility.ExecuteWithRetry(() => fileService.DeleteDirectory(DirectoryPath), 5, TimeSpan.FromMilliseconds(500));
            }

            public override bool IsModifying
            {
                get { return true; }
            }
        }

        protected class FileExistsOperation : FileServiceOperationBase
        {
            public string FilePath { get; set; }

            /// <summary>
            /// Operation result
            /// </summary>
            public bool Result { get; set; }

            public override IEnumerable<string> OperationPaths
            {
                get { yield return FilePath; }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                Result = RetryUtility.ExecuteWithRetry(() => fileService.FileExists(FilePath), 5);
            }

            public override bool IsModifying
            {
                get { return false; }
            }
        }

        protected class DirectoryExistsOperation : FileServiceOperationBase
        {
            public string FilePath { get; set; }

            /// <summary>
            /// Operation result
            /// </summary>
            public bool Result { get; set; }

            public override IEnumerable<string> OperationPaths
            {
                get { yield return FilePath; }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                Result = RetryUtility.ExecuteWithRetry(() => fileService.DirectoryExists(FilePath), 5);
            }

            public override bool IsModifying
            {
                get { return false; }
            }
        }

        protected class WriteOperation : FileServiceOperationBase
        {
            public string DestinationPath { get; private set; }
            public string LocalSourcePath { get; private set; }

            public WriteOperation(string localSourcePath, string destinationPath)
            {
                DestinationPath = destinationPath;
                LocalSourcePath = localSourcePath;
            }

            public override IEnumerable<string> OperationPaths
            {
                get
                {
                    yield return DestinationPath;
                }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                var fileStream = RetryUtility.ExecuteWithRetry(
                    () => new FileStream(LocalSourcePath, FileMode.Open, FileAccess.Read, FileShare.Read), 
                    5, TimeSpan.FromMilliseconds(500));

                using (var arguments = new WriteFileArguments
                {
                    Path = DestinationPath,
                    Data = fileStream
                })
                {
                    RetryUtility.ExecuteWithRetry(() => fileService.Write(arguments), 
                        5, TimeSpan.FromMilliseconds(500));
                }
            }

            public override bool IsModifying
            {
                get { return true; }
            }
        }

        protected class ReadOperation : FileServiceOperationBase
        {
            public string FileServicePath { get; set; }

            // Not in OperationPaths, since it's a same FileService path
            public string LocalCopyFilePath { get; set; }

            public override IEnumerable<string> OperationPaths
            {
                get
                {
                    yield return FileServicePath;
                }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                // Get checksum of local copy
                var localCopySha1 = FileSystem.GetSha1Checksum(LocalCopyFilePath);

                // Try read it from file service
                var arguments = new ReadFileArguments
                {
                    LocalCopySha1 = localCopySha1,
                    Path = FileServicePath
                };

                using (var reply = RetryUtility.ExecuteWithRetry(() => fileService.Read(arguments), 5))
                {
                    // Can use local copy -> quit
                    if (reply.UseLocalCopy) return;

                    if (reply.Exists)
                    {
                        FileSystem.TryWriteFile(LocalCopyFilePath, reply.Data);
                    }
                    else
                    {
                        // Ensure all path parts exist, since most likely we will be writing to it next
                        FileSystem.EnsureFilePath(LocalCopyFilePath);

                        // Ensure then it doesn't exist locally as well now
                        FileSystem.TryDeleteFile(LocalCopyFilePath, true);
                    }
                }
            }

            public override bool IsModifying
            {
                get { return false; }
            }
        }

        protected class ListFilesOperation : FileServiceOperationBase
        {
            private readonly RefreshableCachedValue<string> _operationPath = new RefreshableCachedValue<string>(CacheHoldInterval.Brief);

            public string Path { get; set; }

            public bool AllDirectories { get; set; }

            /// <summary>
            /// Operation result
            /// </summary>
            public string[] Result { get; set; }

            public override IEnumerable<string> OperationPaths
            {
                get
                {
                    yield return _operationPath.GetOrRefreshValue(() =>
                    {
                        string operationPath;
                        string searchPattern;
                        FileSystem.ParsePathWithSearchPattern(Path, out operationPath, out searchPattern);
                        return operationPath;
                    }, Path);
                }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                Result = RetryUtility.ExecuteWithRetry(() => fileService.GetFiles(Path, AllDirectories), 5);
            }

            public override bool IsModifying
            {
                get { return false; }
            }
        }

        protected class ListDirectoriesOperation : FileServiceOperationBase
        {
            private readonly RefreshableCachedValue<string> _operationPath = new RefreshableCachedValue<string>(CacheHoldInterval.Brief);

            public string Path { get; set; }

            public bool AllDirectories { get; set; }

            /// <summary>
            /// Operation result
            /// </summary>
            public string[] Result { get; set; }

            public override IEnumerable<string> OperationPaths
            {
                get
                {
                    yield return _operationPath.GetOrRefreshValue(() =>
                    {
                        string operationPath;
                        string searchPattern;
                        FileSystem.ParsePathWithSearchPattern(Path, out operationPath, out searchPattern);
                        return operationPath;
                    }, Path);
                }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                Result = RetryUtility.ExecuteWithRetry(() => fileService.GetDirectories(Path, AllDirectories), 5);
            }

            public override bool IsModifying
            {
                get { return false; }
            }
        }

        protected class GetFileInfosOperation : FileServiceOperationBase
        {
            public string[] FilePaths { get; set; }

            /// <summary>
            /// Operation result
            /// </summary>
            public FileInfoRecord[] Result { get; set; }

            public override IEnumerable<string> OperationPaths
            {
                get { return FilePaths; }
            }

            public override void ExecuteOperation(IFileService fileService)
            {
                Result = RetryUtility.ExecuteWithRetry(() => fileService.GetFileInfos(FilePaths), 5);
            }

            public override bool IsModifying
            {
                get { return false; }
            }
        }

        #endregion
    }
}
