﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace IO.Practiceware
{
    /// <summary>
    /// File timestamps
    /// </summary>
    [Serializable]
    [DataContract]
    public class FileInfoRecord
    {
        /// <summary>
        /// File's full name
        /// </summary>
        [DataMember]
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether file exists.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [exists]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool Exists { get; set; }

        [DataMember]
        public long Size { get; set; }

        [DataMember]
        public DateTime CreationTimeUtc { get; set; }

        [DataMember]
        public DateTime LastWriteTimeUtc { get; set; }

        [DataMember]
        public DateTime LastAccessTimeUtc { get; set; }

        /// <summary>
        /// The name of the file with extension
        /// </summary>
        public string Name 
        {
            get { return Path.GetFileName(FullName); }
        }

        /// <summary>
        /// The extension
        /// </summary>
        public string Extension
        {
            get { return Path.GetExtension(FullName); }
        }
    }
}