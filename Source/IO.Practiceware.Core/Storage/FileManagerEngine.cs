﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IO.Practiceware.Services.FileService;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Threading;

[assembly: Component(typeof(FileManagerEngine), typeof(IFileManagerEngine))]

namespace IO.Practiceware.Storage
{
    public interface IFileManagerEngine
    {
        /// <summary>
        /// Enqueues operation execution
        /// </summary>
        /// <param name="operation"></param>
        void EnqueueOperation(IFileServiceOperation operation);

        /// <summary>
        /// Awaits completion of all pending operations.
        /// </summary>
        void WaitForIncompleteOperations();

        /// <summary>
        /// Awaits completion of pending operations executed on specified paths
        /// </summary>
        /// <param name="paths">Operation paths</param>
        void WaitForIncompleteOperations(params string[] paths);

        /// <summary>
        /// Awaits completion of specified operations
        /// </summary>
        /// <param name="operationsOfInterest">The operations of interest.</param>
        void WaitForIncompleteOperations(params IFileServiceOperation[] operationsOfInterest);
    }

    /// <summary>
    /// File manager engine. Responsible for operations execution and issuing locks
    /// </summary>
    [Singleton]
    public class FileManagerEngine : IFileManagerEngine
    {
        private readonly IFileManagerConfiguration _configuration;
        private readonly IFileService _fileService;

        private readonly ReaderWriterLockSlim _operationsQueueAccessLock = new ReaderWriterLockSlim();
        private readonly LinkedList<IFileServiceOperation> _pendingOperationsQueue = new LinkedList<IFileServiceOperation>();
        private readonly LimitedConcurrencyLevelTaskScheduler _priorityTaskScheduler = new LimitedConcurrencyLevelTaskScheduler(4);
        private readonly LimitedConcurrencyLevelTaskScheduler _generalTaskScheduler = new LimitedConcurrencyLevelTaskScheduler(4);
        private readonly CultureInfo _culture = CultureInfo.InstalledUICulture;
        private static readonly TimeSpan WaitUntilOperationCompleteTimeout = TimeSpan.FromMinutes(1);

        public FileManagerEngine(IFileManagerConfiguration configuration, IFileService fileService)
        {
            _configuration = configuration;
            _fileService = fileService;
        }

        public void EnqueueOperation(IFileServiceOperation operation)
        {
            FileManager.TraceSource.TraceInformation("Enqueued operation {0}".FormatWith(operation));

            using (new DisposableScope<ReaderWriterLockSlim>(_operationsQueueAccessLock, l => l.EnterWriteLock(), l => l.ExitWriteLock()))
            {
                // Add to queue
                _pendingOperationsQueue.AddLast(operation);
            }

            // Process non-modifying operations with higher priority (since most of the time they are blocking)
            var targetPool = !operation.IsModifying
                ? _priorityTaskScheduler
                : _generalTaskScheduler;

            Task.Factory.StartNew(o => ProcessFileServiceOperation((IFileServiceOperation)o),
                operation, CancellationToken.None, TaskCreationOptions.PreferFairness, targetPool);
        }

        public void WaitForIncompleteOperations()
        {
            IFileServiceOperation[] pendingOperationsUpUntilNow;
            using (new DisposableScope<ReaderWriterLockSlim>(_operationsQueueAccessLock, l => l.EnterReadLock(), l => l.ExitReadLock()))
            {
                pendingOperationsUpUntilNow = _pendingOperationsQueue.ToArray();
            }

            WaitForIncompleteOperations(pendingOperationsUpUntilNow);
        }

        public void WaitForIncompleteOperations(params string[] paths)
        {
            // No need to wait if non-redirected paths provided
            var redirectedPaths = paths.Select(_configuration.ToRedirectedPath).ToArray();
            if (!redirectedPaths.Any(p => _configuration.IsRedirectedPath(p)))
            {
                return;
            }

            // Get list of pending operations executed on specified paths
            var fileServicePaths = redirectedPaths.Select(_configuration.ToFileServiceRelativePath).ToArray();
            var operationDependencies = GetOperationDependencies(new DependenciesCheckOperation(fileServicePaths));
            
            if (operationDependencies.Any())
            {
                WaitForIncompleteOperations(operationDependencies);
            }
        }

        public void WaitForIncompleteOperations(params IFileServiceOperation[] operationsOfInterest)
        {
            FileManager.TraceSource.TraceInformation("Waiting for {0} incomplete operations. Thread: {1}",
                operationsOfInterest.Length, Thread.CurrentThread.ManagedThreadId);

            bool success = true;
            var waitStart = DateTime.Now;
            var timeout = WaitUntilOperationCompleteTimeout;
            foreach (var operation in operationsOfInterest)
            {
                // Ensure we still have time to wait
                if (timeout <= TimeSpan.Zero)
                {
                    success = false;
                    break;
                }

                success = operation.WaitUntilComplete(timeout);
                if (!success) break;

                // Recalculate new wait
                timeout -= (DateTime.Now - waitStart);
            }

            if (success) return;

            var builder = new StringBuilder();
            using (new DisposableScope<ReaderWriterLockSlim>(_operationsQueueAccessLock, l => l.EnterReadLock(), l => l.ExitReadLock()))
            {
                _pendingOperationsQueue
                    .ToList()
                    .ForEach(i => builder.AppendLine(Environment.NewLine + i.ToString()));
            }

            throw new Exception("WaitForIncompleteOperations timed out waiting for {0}.".FormatWith(builder.ToString()));
        }

        private void ProcessFileServiceOperation(IFileServiceOperation operation)
        {
            var start = DateTime.Now;
            try
            {
                var operationDependencies = GetOperationDependencies(operation);

                // We can only continue once all dependencies completed their execution
                if (operationDependencies.Any())
                {
                    WaitForIncompleteOperations(operationDependencies);
                }

                operation.Execute(_fileService);
            }
            catch (Exception ex)
            {
                string message = "Failed to complete operation: {0}. Thread: {1}"
                    .FormatWith(operation, Thread.CurrentThread.ManagedThreadId);
                FileManager.TraceSource.TraceEvent(TraceEventType.Error, 0, "{0} Error: {1}. Thread: {2}"
                    .FormatWith(message, ex, Thread.CurrentThread.ManagedThreadId));

#if DEBUG // note, this will kill the application, so only throw in DEBUG mode.
                throw new InvalidOperationException(message, ex);
#endif
            }
            finally
            {
                // Remove from queue
                using (new DisposableScope<ReaderWriterLockSlim>(_operationsQueueAccessLock, l => l.EnterWriteLock(), l => l.ExitWriteLock()))
                {
                    _pendingOperationsQueue.Remove(operation);
                }
                FileManager.TraceSource.TraceInformation("Executed operation {0} in {1}ms. Thread: {2}"
                    .FormatWith(operation, (DateTime.Now - start).TotalMilliseconds, Thread.CurrentThread.ManagedThreadId));
            }

        }

        private IFileServiceOperation[] GetOperationDependencies(IFileServiceOperation operation)
        {
            var operationDependencies = new List<IFileServiceOperation>();
            using (new DisposableScope<ReaderWriterLockSlim>(_operationsQueueAccessLock, l => l.EnterReadLock(), l => l.ExitReadLock()))
            {
                // Find operation in the queue prior to provided one
                var queueItem = _pendingOperationsQueue.Find(operation);
                queueItem = queueItem == null 
                    ? _pendingOperationsQueue.Last 
                    : queueItem.Previous;
                
                // Check dependencies in reverse order
                while (queueItem != null)
                {
                    if (IsOperationDependentOn(operation, queueItem.Value))
                    {
                        operationDependencies.Add(queueItem.Value);
                    }

                    queueItem = queueItem.Previous;
                }
            }
            return operationDependencies.ToArray();
        }

        private bool IsOperationDependentOn(IFileServiceOperation target, IFileServiceOperation dependency)
        {
            // Can't depend on executed operations
            if (target.HasExecuted || dependency.HasExecuted) return false;

            // Non-modifying operations can't depend on each other
            if (!target.IsModifying && !dependency.IsModifying) return false;

            // Operations over similar operation paths -> depend
            foreach (var targetOperationPath in target.OperationPaths)
            {
                foreach (string dependencyOperationPath in dependency.OperationPaths)
                {
                    if (dependencyOperationPath.StartsWith(targetOperationPath, true, _culture)
                        || targetOperationPath.StartsWith(dependencyOperationPath, true, _culture))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        protected class DependenciesCheckOperation : IFileServiceOperation
        {
            private readonly string[] _operationPaths;

            public DependenciesCheckOperation(params string[] operationPaths)
            {
                _operationPaths = operationPaths;
            }

            public IEnumerable<string> OperationPaths 
            {
                get { return _operationPaths; }
            }
            
            public void Execute(IFileService fileService)
            {
                throw new InvalidOperationException("Dependencies check operation is not expected to be ever executed");
            }

            public bool IsModifying 
            {
                // Always considered modifying
                get { return true; }
            }

            public bool HasExecuted 
            {
                get { return false; }
            }

            public bool WaitUntilComplete(TimeSpan timeout)
            {
                throw new InvalidOperationException("Dependencies check operation is not expected to be ever executed");
            }
        }
    }
}
