﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Caching;
using Soaf.ComponentModel;

[assembly: Component(typeof(FileManagerConfiguration), typeof(IFileManagerConfiguration))]

namespace IO.Practiceware.Storage
{
    public interface IFileManagerConfiguration
    {
        /// <summary>
        /// Gets a value indicating whether redirection is enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if redirection is enabled; otherwise, <c>false</c>.
        /// </value>
        bool IsRedirectionEnabled { get; set; }

        /// <summary>
        /// Gets or sets the root path of the redirected file system. For example, may be %TEMP%\IOPracticeware\FileSystem.
        /// </summary>
        /// <value>
        /// The redirected root path.
        /// </value>
        string RedirectedRootPath { get; set; }

        /// <summary>
        /// Path to main application directory
        /// </summary>
        string ApplicationDataPath { get; set; }

        /// <summary>
        /// Gets or sets the local files relative path to the RedirectedRootPath. Usually "LocalFiles", so the full path might be %TEMP%\IOPracticeware\FileSystem\LocalFiles\{Environment.MachineName}.
        /// </summary>
        /// <value>
        /// The local files relative path.
        /// </value>
        string LocalFilesRelativePath { get; }

        /// <summary>
        /// Gets or sets the server files relative path to the RedirectedRootPath. Usually "ServerFiles", so the full path might be %TEMP%\IOPracticeware\FileSystem\ServerFiles.
        /// </summary>
        /// <value>
        /// The server files relative path.
        /// </value>
        string ServerFilesRelativePath { get; }

        /// <summary>
        /// Relative path user application files stored on the server
        /// </summary>
        string FileServiceApplicationDataPath { get; }

        /// <summary>
        /// Gets the excluded paths.
        /// </summary>
        /// <value>
        /// The excluded paths.
        /// </value>
        IEnumerable<FileManagerConfiguration.ExcludedPath> ExcludedPaths { get; }

        /// <summary>
        /// Converts path to file service relative path.
        /// </summary>
        /// <param name="path">Final path (redirected, direct or relative).</param>
        /// <returns></returns>
        string ToFileServiceRelativePath(string path);

        /// <summary>
        /// Converts from file service relative path back to local path (direct)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        string FromFileServiceToLocalPath(string path);

        /// <summary>
        /// Converts specified path to local redirected path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        string ToRedirectedPath(string path);

        /// <summary>
        /// Determines whether specified path is in redirected format.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        bool IsRedirectedPath(string path);

        /// <summary>
        /// Adds path to redirection exclusions list
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="isDirectory">if set to <c>true</c> specified path is directory. Otherwise - file</param>
        void ExcludePath(string path, bool isDirectory = false);
    }

    [Singleton]
    public class FileManagerConfiguration : IFileManagerConfiguration
    {
        public static readonly CultureInfo OsCulture = CultureInfo.InstalledUICulture;
        public static readonly StringComparer OsCultureStringComparer = StringComparer.Create(OsCulture, true);

        private const string DefaultServerFilesRelativePath = "ServerFiles";
        private Lazy<ConcurrentDictionary<ExcludedPath, object>> _excludedPaths;
        private RefreshableCachedValue<string> _redirectedRootPath;
        private RefreshableCachedValue<string> _redirectedRootShortPath;
        private Lazy<string> _localFilesRelativePath;
        private Lazy<string> _fileServiceApplicationDataPath;
        
        private bool? _customIsRedirectionEnabled;
        private string _customRedirectedRootPath;
        private string _customApplicationDataPath;

        public FileManagerConfiguration()
        {
            Reinit();
        }

        public void Reinit()
        {
            if (_redirectedRootPath != null) _redirectedRootPath.Dispose();
            _redirectedRootPath = new RefreshableCachedValue<string>(() =>
            {
                // Calculate current redirected root. Scope by:
                // 1. ClientName to avoid collisions when multiple clients are processed (like Monitoring Service or Test)
                //   Note: ClientKey can't be used, since it may contain invalid characters for directory
                // 2. ProcessId to avoid collisions on RDP servers
                var result = _customRedirectedRootPath ?? string.Format(@"%TEMP%\IOP\FileSystem\{0}\{1}", 
                    ApplicationContext.Current.ClientName, Process.GetCurrentProcess().Id);

                // Convert it to full and expanded path
                result = Environment.ExpandEnvironmentVariables(result);
                result = Path.GetFullPath(result);

                // Use it
                return result;
            }, CacheHoldInterval.XLong);

            if (_redirectedRootShortPath != null) _redirectedRootShortPath.Dispose();
            _redirectedRootShortPath = new RefreshableCachedValue<string>(() =>
            {
                var result = RedirectedRootPath;

                // directory must exist for call to GetShortPathName to work.
                FileSystem.EnsureDirectory(result);

                // Find out the short path
                var sb = new StringBuilder(result.Length);
                FileSystem.GetShortPathName(result, sb, sb.Capacity);
                result = sb.ToString().Trim();

                return result;
            }, CacheHoldInterval.XLong);

            _localFilesRelativePath = new Lazy<string>(() =>
            {
                var localFiles = Path.Combine("LocalFiles", "__" + Environment.MachineName);

                // Scope local files per connected client's computer name
                if (ConfigurationManager.ClientApplicationConfiguration.RdsWithSharedAccountMode)
                {
                    var clientName = Environment.GetEnvironmentVariable("clientname");
                    if (!string.IsNullOrWhiteSpace(clientName))
                    {
                        localFiles = Path.Combine(localFiles, string.Format("cn_{0}", clientName));
                    }
                }

                // Create temporary subfolder per process when local files are treated transient
                if (ConfigurationManager.ClientApplicationConfiguration.TreatLocaFilesAsTransient)
                {
                    localFiles = Path.Combine(localFiles,
                        string.Format("tp_{0}", Process.GetCurrentProcess().Id));
                }
                return localFiles;
            });

            _excludedPaths = new Lazy<ConcurrentDictionary<ExcludedPath, object>>(() =>
            {
                var result = new[]
                    {
                        new ExcludedPath (Path.Combine(ApplicationDataPath, "Images"), true),
                        new ExcludedPath (Path.Combine(ApplicationDataPath, "Submit"), true),
                        new ExcludedPath (Path.Combine(ApplicationDataPath, "DiagnosisMaster.mdb"), false),
                        new ExcludedPath (Path.Combine(ApplicationDataPath, "DynamicForms.mdb"), false),
                        new ExcludedPath (Path.Combine(ApplicationDataPath, "DynamicFormsPage.mdb"), false),
                        new ExcludedPath (Path.Combine(ApplicationDataPath, "IO Practiceware Server Setup.msi"), false),
                        new ExcludedPath (Path.Combine(ApplicationDataPath, "Zones.txt"), false),
                        new ExcludedPath (Path.Combine(ApplicationDataPath, "Zones02.txt"), false),
                        new ExcludedPath (Path.Combine(ApplicationDataPath, "Zones08.txt"), false)
                    }
                    .Select(p => new KeyValuePair<ExcludedPath, object>(p, null))
                    .ToArray();

                return new ConcurrentDictionary<ExcludedPath,object>(result);
            });

            _fileServiceApplicationDataPath = new Lazy<string>(() => ToFileServiceRelativePath(ApplicationDataPath));
        }

        public bool IsRedirectionEnabled
        {
            get
            {
                return _customIsRedirectionEnabled != null 
                    ? _customIsRedirectionEnabled.Value 
                    // Enabled client side only when user is authenticated
                    : !ApplicationContext.Current.IsServerSide && (UserContext.Current != null && UserContext.Current.UserDetails != null);
            }
            set { _customIsRedirectionEnabled = value; }
        }

        public string ApplicationDataPath
        {
            get
            {
                // Allow overriding this directory. For example for tests
                return _customApplicationDataPath ?? ConfigurationManager.ApplicationDataPath;
            }
            set
            {
                _customApplicationDataPath = value;
                Reinit();
            }
        }

        public string RedirectedRootPath
        {
            get
            {
                return _redirectedRootPath.GetOrRefreshValue(new object[]
                {
                    ApplicationContext.Current.ClientKey,
                    _customRedirectedRootPath
                });
            }
            set
            {
                _customRedirectedRootPath = value;
                Reinit();
            }
        }

        public string RedirectedRootShortPath
        {
            get
            {
                return _redirectedRootShortPath.GetOrRefreshValue(new object[]
                {
                    ApplicationContext.Current.ClientKey,
                    _customRedirectedRootPath
                });
            }
        }

        public string LocalFilesRelativePath
        {
            get { return _localFilesRelativePath.Value; }
        }

        public string ServerFilesRelativePath
        {
            get { return DefaultServerFilesRelativePath; }
        }

        public IEnumerable<ExcludedPath> ExcludedPaths
        {
            get { return _excludedPaths.Value.Keys; }
        }

        /// <summary>
        /// Relative path user application files stored on the server
        /// </summary>
        public string FileServiceApplicationDataPath
        {
            get { return _fileServiceApplicationDataPath.Value; }
        }

        /// <summary>
        /// Converts specified path to local redirected path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string ToRedirectedPath(string path)
        {
            if (path.IsNullOrEmpty())
            {
                return path;
            }

            // Fix relative paths and short paths
            var result = FileSystem.GetFullPath(path);

            // Read server data path once
            var serverDataPath = ConfigurationManager.ServerDataPath;

            // Check if path should be modified to redirected version
            if (IsExcludedPath(result, serverDataPath) || IsRedirectedPath(result))
            {
                return result;
            }

            // Create redirected path
            result = result.StartsWith(serverDataPath, true, OsCulture)
                ? Path.Combine(RedirectedRootPath, ServerFilesRelativePath, result.Substring(serverDataPath.Length).TrimStart(@"\"))
                : Path.Combine(RedirectedRootPath, LocalFilesRelativePath, result.Replace(":", string.Empty).TrimStart(@"\"));

            return result;
        }

        /// <summary>
        /// Converts path to file service relative path.
        /// </summary>
        /// <param name="path">Final path (redirected, direct or relative).</param>
        /// <returns></returns>
        public string ToFileServiceRelativePath(string path)
        {
            // If its already relative path -> return it
            if (path.StartsWith(@"\", true, OsCulture) && !path.StartsWith(@"\\", true, OsCulture))
            {
                return path;
            }

            // Only redirected paths can be accessed through their relative paths on file service
            var redirectedPath = ToRedirectedPath(path);
            if (!IsRedirectedPath(redirectedPath))
            {
                return redirectedPath;
            }

            // Remove redirected path to come up with relative file path on server
            var localRedirectedServerFilesPath = Path.Combine(RedirectedRootPath, ServerFilesRelativePath);
            int trimStartIndex = redirectedPath.StartsWith(localRedirectedServerFilesPath, true, OsCulture)
                ? localRedirectedServerFilesPath.Length
                : RedirectedRootPath.Length;

            var result = redirectedPath.Substring(trimStartIndex);
            return result;
        }

        /// <summary>
        /// Converts from file service relative path back to local path (direct)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string FromFileServiceToLocalPath(string path)
        {
            var result = path;

            // If it's a server path for local files -> convert back to direct local path
            var localFilesOnServerPath = Path.Combine(@"\", LocalFilesRelativePath);
            if (path.StartsWith(localFilesOnServerPath, true, OsCulture))
            {
                result = path.Substring(localFilesOnServerPath.Length).TrimStart('\\');
                // Restore drive letter reference
                result = result.ReplaceFirst(@"\", @":\");
            }
            else if (path.StartsWith(@"\", true, OsCulture)
                && !path.StartsWith(@"\\", true, OsCulture))
            {
                result = Path.Combine(ConfigurationManager.ServerDataPath, path.TrimStart('\\'));
            }

            return result;
        }

        /// <summary>
        /// Determines whether specified path represents a redirected path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public bool IsRedirectedPath(string path)
        {
            var result = path.StartsWith(RedirectedRootPath, true, OsCulture)
                || path.StartsWith(RedirectedRootShortPath, true, OsCulture);
            return result;
        }

        /// <summary>
        /// Adds path to redirection exclusions list
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="isDirectory">if set to <c>true</c> specified path is directory. Otherwise - file</param>
        public void ExcludePath(string path, bool isDirectory = false)
        {
            var pathToExclude = new ExcludedPath(path, isDirectory);
            if (_excludedPaths.Value.ContainsKey(pathToExclude)) return;

            _excludedPaths.Value.TryAdd(pathToExclude, null);
        }

        /// <summary>
        /// Determines whether path is excluded.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="serverDataPath"></param>
        /// <returns>
        ///   <c>true</c> if path is excluded; otherwise, <c>false</c>.
        /// </returns>
        private bool IsExcludedPath(string path, string serverDataPath)
        {
            // Must be included and not excluded
            var isPathExcluded = !IsRedirectionEnabled // When redirection is not enabled -> skip all
                || (!path.StartsWith(ApplicationDataPath, true, OsCulture)
                    && !path.StartsWith(serverDataPath, true, OsCulture));
            if (!isPathExcluded)
            {
                isPathExcluded = ExcludedPaths.Any(p => path.StartsWith(p.Path, true, OsCulture));
            }

            return isPathExcluded;
        }

        public class ExcludedPath
        {
            public ExcludedPath(string path, bool isDirectory)
            {
                Path = path;
                IsDirectory = isDirectory;
                ParentDirectory = System.IO.Path.GetDirectoryName(path);
            }

            /// <summary>
            /// Gets the full path to excluded item.
            /// </summary>
            public string Path { get; private set; }

            /// <summary>
            /// Gets a value indicating whether path is directory.
            /// </summary>
            public bool IsDirectory { get; private set; }

            /// <summary>
            /// Gets the parent directory path of excluded item
            /// </summary>
            public string ParentDirectory { get; private set; }

            protected bool Equals(ExcludedPath other)
            {
                return string.Equals(Path, other.Path) && IsDirectory.Equals(other.IsDirectory);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != GetType()) return false;
                return Equals((ExcludedPath) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return ((Path != null ? Path.GetHashCode() : 0)*397) ^ IsDirectory.GetHashCode();
                }
            }
        }
    }
}
