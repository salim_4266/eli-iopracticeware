﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using IO.Practiceware.Services.ApplicationSettings;

namespace IO.Practiceware.Storage
{
    /// <summary>
    /// Helper methods for IFileManager.
    /// </summary>
    public static class FileManagerExtensions
    {
        /// <summary>
        /// Reads the file path using the FileManager and returns its contents as a byte array.
        /// </summary>
        /// <param name="fileManager">The file manager.</param>
        /// <param name="path">The path.</param>
        /// <param name="returnNullIfNotExists">if set to <c>true</c> [return null if not exists].</param>
        /// <returns></returns>
        public static byte[] ReadContents(this IFileManager fileManager, string path, bool returnNullIfNotExists = false)
        {
            var result = fileManager.ReadAs(path, localPath => FileSystem.TryReadAllBytes(localPath, returnNullIfNotExists));
            return result;
        }

        /// <summary>
        /// Reads the file path using the FileManager and returns its contents as a string.
        /// </summary>
        /// <param name="fileManager">The file manager.</param>
        /// <param name="path">The path.</param>
        /// <param name="returnNullIfNotExists">if set to <c>true</c> [return null if not exists].</param>
        /// <returns></returns>
        public static string ReadContentsAsText(this IFileManager fileManager, string path, bool returnNullIfNotExists = false)
        {
            var result = fileManager.ReadAs(path, localPath => FileSystem.TryReadAllText(localPath, returnNullIfNotExists));
            return result;
        }

        /// <summary>
        /// Reads file into custom format via callback.
        /// </summary>
        /// <param name="fileManager">The file manager.</param>
        /// <param name="path">The path.</param>
        /// <param name="rawRead">The raw read (use passed parameter for file location).</param>
        public static TResult ReadAs<TResult>(this IFileManager fileManager, string path, Func<string, TResult> rawRead)
        {
            var localPath = fileManager.Read(path);
            return rawRead(localPath);
        }

        /// <summary>
        /// Compresses the file specified using different techniques depending on the file type.
        /// </summary>
        /// <param name="fileManager">The file manager.</param>
        /// <param name="sourcePath">The source path.</param>
        /// <returns></returns>
        public static bool Compress(this IFileManager fileManager, string sourcePath)
        {
            var extension = Path.GetExtension(sourcePath) ?? string.Empty;
            var compressionLevelSetting = ApplicationSettings.Cached.GetSetting<string>("FileCompressionLevel");
            
            int compressionLevelValue;
            if (compressionLevelSetting != null && int.TryParse(compressionLevelSetting.Value, out compressionLevelValue))
            {
                var redirectedSourcePath = fileManager.Read(sourcePath);
                var tempPath = FileSystem.GetTempPathName();

                bool compressedSuccessfully;
                switch (extension.ToLower())
                {
                    case ".pdf":
                        compressedSuccessfully = Images.TryCompressPdfImages(redirectedSourcePath, tempPath, compressionLevelValue);
                        break;
                    case ".jpg":
                    case ".jpeg":
                        compressedSuccessfully = Images.TryCompressJpeg(redirectedSourcePath, tempPath, compressionLevelValue);
                        break;
                    default:
                        // couldn't compress
                        compressedSuccessfully = false;
                        break;
                }

                var tempFileInfo = new FileInfo(tempPath);
                var redirectedSourceFileInfo = new FileInfo(redirectedSourcePath);
                if (compressedSuccessfully && File.Exists(tempPath) && tempFileInfo.Length > redirectedSourceFileInfo.Length)
                {
                    Trace.TraceWarning("Skipping compression because compressed file {0} ({1} bytes) is larger than original file {2} ({3} bytes).", redirectedSourcePath, redirectedSourceFileInfo.Length, tempPath, tempFileInfo.Length);
                    FileSystem.TryDeleteFile(tempPath, true);
                    return false;
                }
                if (!compressedSuccessfully)
                {
                    Trace.TraceWarning("Failed to compress file {0}.", redirectedSourcePath);
                    FileSystem.TryDeleteFile(tempPath, true);
                    return false;
                }

                FileSystem.TryCopyFile(tempPath, redirectedSourcePath);
                fileManager.Commit(redirectedSourcePath);

                FileManager.TraceSource.TraceInformation("Successfully compressed file {0} from {1} bytes to {2} bytes.", redirectedSourcePath, redirectedSourceFileInfo.Length, tempFileInfo.Length);

                return true;
            }

            FileManager.TraceSource.TraceInformation("Not compressing file {0} because compression is not enabled or the file type is not supported.", sourcePath);

            return false;
        }

        /// <summary>
        /// Perform direct write to file and then commit changes.
        /// </summary>
        /// <param name="fileManager">The file manager.</param>
        /// <param name="path">The path.</param>
        /// <param name="rawWrite">The raw write (use passed parameter for file location).</param>
        public static void CommitWrite(this IFileManager fileManager, string path, Action<string> rawWrite)
        {
            var savePath = fileManager.Configuration.ToRedirectedPath(path);
            fileManager.Engine.WaitForIncompleteOperations(savePath);
            FileSystem.EnsureFilePath(savePath);
            rawWrite(savePath);
            fileManager.Commit(path);
        }

        /// <summary>
        /// Writes the contents to the specified file path and commits the operations.
        /// </summary>
        /// <param name="fileManager">The file manager.</param>
        /// <param name="path">The path.</param>
        /// <param name="data">The data.</param>
        public static void CommitContents(this IFileManager fileManager, string path, byte[] data)
        {
            fileManager.CommitWrite(path, localPath => FileSystem.TryWriteFile(localPath, data, false));
        }


        /// <summary>
        /// Writes the contents to the specified file path and commits the operations.
        /// </summary>
        /// <param name="fileManager">The file manager.</param>
        /// <param name="path">The path.</param>
        /// <param name="data">The data.</param>
        public static void CommitContents(this IFileManager fileManager, string path, string data)
        {
            fileManager.CommitWrite(path, localPath => FileSystem.TryWriteFile(localPath, data, false));
        }

        /// <summary>
        /// Retrieves and returns file information
        /// </summary>
        /// <param name="fileManager"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static FileInfoRecord GetFileInfo(this IFileManager fileManager, string path)
        {
            return fileManager.GetFileInfos(path).Single();
        }

        /// <summary>
        /// Gets the temporary path.
        /// </summary>
        /// <param name="fileManager">The file manager.</param>
        /// <returns></returns>
        public static string GetTempPath(this IFileManager fileManager)
        {
            return FileSystem.GetTempPath();
        }

        /// <summary>
        /// Gets the name of the temporary file. Does not use file system API - combines temp path with a guid.
        /// </summary>
        /// <returns></returns>
        public static string GetTempPathName(this IFileManager fileManager)
        {
            return FileSystem.GetTempPathName();
        }

        /// <summary>
        /// Deletes the file if it exists.
        /// </summary>
        /// <param name="fileManager">The file manager.</param>
        /// <param name="filePath">The path.</param>
        public static void DeleteIfExists(this IFileManager fileManager, string filePath)
        {
            var exists = fileManager.FileExists(filePath);
            if (exists) fileManager.Delete(filePath);
        }

        /// <summary>
        /// Excludes file from redirection. If file existed prior to excluding it -> it will be moved to it's expected location.
        /// </summary>
        /// <param name="fileManager">The file manager.</param>
        /// <param name="filePath">The file path.</param>
        public static void ExcludeFileFromRedirection(this IFileManager fileManager, string filePath)
        {
            // Skip if already excluded
            if (fileManager.Configuration.ExcludedPaths.Contains(new FileManagerConfiguration.ExcludedPath(filePath, false)))
            {
                return;
            }

            string localTempPath = null;

            if (fileManager.FileExists(filePath))
            {
                localTempPath = fileManager.GetTempPathName();
                // Move outside of redirected location
                fileManager.Move(filePath, localTempPath);
            }

            FileSystem.EnsureFilePath(filePath);
            fileManager.Configuration.ExcludePath(filePath);

            if (!string.IsNullOrEmpty(localTempPath))
            {
                // Move at new non-redirected location
                fileManager.Move(localTempPath, filePath);
            }
        }
    }
}