﻿using Soaf.Data;
using System;

namespace IO.Practiceware.Data
{
    /// <summary>
    /// Class to disable auditing.
    /// It re-enables auditing when exiting the scope.
    /// </summary>
    public class SuppressAuditingScope : IDisposable
    {
        private const string DisableAuditingQuery =
@"IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL
DROP TABLE #NoAudit
SELECT 1 AS Value INTO #NoAudit 
";

//        private const string EnableAuditingQuery =
//@"IF OBJECT_ID('tempdb..#NoAudit') IS NOT NULL
//DROP TABLE #NoAudit";

        public SuppressAuditingScope()
        {
            AdoServiceProviderFactory.Instance.CreateCommandFunc = () => new AdoServiceCommand {CommandText = DisableAuditingQuery};
        }

        public void Dispose()
        {
            AdoServiceProviderFactory.Instance.CreateCommandFunc = null;
        }
    }
}
