﻿
using IO.Practiceware.Data;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

[assembly: Component(typeof(SqlReplicationManager), typeof(ISqlReplicationManager))]

namespace IO.Practiceware.Data
{
    /// <summary>
    /// Provides a per object override of the default step range value
    /// </summary>
    public class SeededObject
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("StepRange")]
        public long StepRange { get; set; }
    }

    /// <summary>
    /// Provides the default step range value to use for seeding and any overrides per object
    /// </summary>
    public class Seeding
    {
        public long ServerStepRange { get; set; }
        public List<SeededObject> SeedingOverrides { get; set; }
    }

    /// <summary>
    ///   Information about how to configure sql replication.
    /// </summary>
    public class SqlReplicationArguments
    {
        /// <summary>
        ///   Connection strings for each of the spoke in replication.
        /// </summary>
        /// <value> The spoke connection strings. </value>
        public IEnumerable<string> SpokeConnectionStrings { get; set; }

        /// <summary>
        ///   Gets or sets the hub db connection string.
        /// </summary>
        /// <value> The hub connection string. </value>
        public string HubConnectionString { get; set; }

        private IEnumerable<string> _replicatedObjects;

        /// <summary>
        ///   Gets or sets the replicated objects.
        /// </summary>
        /// <value> The replicated objects. </value>
        public IEnumerable<string> ReplicatedObjects
        {
            get { return _replicatedObjects; }
            set
            {
                if (value != null) value = ScrubReplicatedObjectArgument(value);
                _replicatedObjects = value;
            }
        }

        private static IEnumerable<string> _defaultReplicatedObjects;

        /// <summary>
        /// Gets the list of objects replicated by default.
        /// </summary>
        public static IEnumerable<string> DefaultReplicatedObjects
        {
            get { return _defaultReplicatedObjects; }
            set
            {
                if (value != null) value = ScrubReplicatedObjectArgument(value);
                _defaultReplicatedObjects = value;
            }
        }

        /// <summary>
        /// Scrub the list of replicated objects to remove dbo scheme
        /// </summary>
        /// <param name="replicatedObjects">List of replicated objects</param>
        /// <returns>List of replicated objects with dbo scheme removed</returns>
        private static IEnumerable<string> ScrubReplicatedObjectArgument(IEnumerable<string> replicatedObjects)
        {
            if (replicatedObjects == null) return null;
            var objects = new List<string>();
            foreach (var obj in replicatedObjects)
            {
                var temp = obj.Replace("[", null).Replace("]", null);
                if (temp.Contains("."))
                {
                    var items = temp.Split(new[] { '.' });
                    if (items[0].Equals("dbo", StringComparison.OrdinalIgnoreCase)) temp = items[1];
                }
                objects.Add(temp);
            }
            return objects;
        }
    }

    /// <summary>
    ///   Manages Sql Replication activities.
    /// </summary>
    public interface ISqlReplicationManager
    {
        /// <summary>
        ///   Gets the SQL replication arguments from a hub.
        /// </summary>
        /// <param name="hubConnectionString"> The connection string to hub. </param>
        /// <returns> SqlReplicationArguments </returns>
        SqlReplicationArguments GetArgumentsFromHub(string hubConnectionString);

        /// <summary>
        ///   Deletes replication publications and subscriptions based on the specified arguments.
        /// </summary>
        /// <param name="arguments"> The arguments. </param>
        /// <param name="forceDeleteIfReplicating"> Drop replication regardless of state (default = false) </param>
        void Delete(SqlReplicationArguments arguments, bool forceDeleteIfReplicating = false);

        /// <summary>
        ///   Creates and configures replication (including distributor, publications and subscriptions) using the specified arguments. Will create distribution database and agents if necessary. If publications/subscriptions already exist, this method will reconfigure so replication configuration matches ReplicatedObjects specified in the arguments. If publications/subscriptions exist for only some of the ReplicationPartners, this method will add/remove publishers/subscribers so replication configuration matches the ReplicationPartners specified in the arguments.
        /// </summary>
        /// <param name="arguments"> The arguments. </param>
        /// <param name="seeding"> The seeding configuration </param>
        void Configure(SqlReplicationArguments arguments, Seeding seeding = null);
    }

    /// <summary>
    ///   Default implementation of an ISqlReplicationManager,
    /// </summary>
    public class SqlReplicationManager : ISqlReplicationManager
    {
        private static readonly Func<Exception, bool> ShouldRetryOnException = e => e is SqlException && new[] { "Timeout expired.", "An existing connection was forcibly closed by the remote host." }.Any(m => e.ToString().Contains(m)); // Unplug RJ-45 cable

        private const int NumberOfRetries = 5;
        private static readonly TimeSpan RetryInterval = TimeSpan.FromSeconds(15);

        // Query to get system database object information or list of system database objects information
        private const string DatabaseSystemObjectQuery =
@"SELECT o.name objectname, o.object_id, o.parent_object_id, o.type, o.type_desc, o.create_date, o.modify_date, 
o.is_ms_shipped, o.is_published, o.is_schema_published, s.name schemaname, s.schema_id, 
OBJECTPROPERTY(o.object_id, 'IsIndexed') IsIndexed, 
OBJECTPROPERTY(o.object_id, 'TableHasIdentity') AS TableHasIdentity,
OBJECTPROPERTY(o.object_id, 'IsSchemaBound') IsSchemaBound, 
CASE WHEN OBJECTPROPERTY(o.object_id, 'TableHasIdentity') = 1 THEN IDENT_CURRENT((s.name + '.' +  o.name)) ELSE NULL END AS CurrentSeedValue 
FROM sys.objects(NOLOCK) o INNER JOIN sys.schemas(NOLOCK) s ON o.schema_id = s.schema_id 
WHERE o.object_id ";

        // Query to get information on the snapshot id
        private const string SnapShotAgentIdQuery = @"SELECT * FROM mssnapshot_agents(NOLOCK) WHERE name LIKE '{0}%' AND publisher_db = '{1}'";

        // Query to get information on the snapshot history
        private const string SnapShotAgentHistoryQuery = @"SELECT * FROM mssnapshot_history(NOLOCK) WHERE agent_id = {0} AND start_time = (SELECT max(start_time) FROM mssnapshot_history(NOLOCK) WHERE agent_id = {0})";

        #region ISqlReplicationManager Members

        private static TResult ExecuteWithRetry<TResult>(IDbConnection connection, string sql, Dictionary<string, object> parameters = null, Func<IDbCommand> createCommand = null)
        {
            try
            {
                return new Func<TResult>(() => connection.Execute<TResult>(sql, parameters, false, createCommand)).ExecuteWithRetry(NumberOfRetries, RetryInterval, ShouldRetryOnException);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not execute\r\n{0}\r\non {1}".FormatWith(sql, connection.ConnectionString), ex);
            }
        }

        private static void ExecuteWithRetry(IDbConnection connection, string sql, Dictionary<string, object> parameters = null, Func<IDbCommand> createCommand = null)
        {
            try
            {
                new Action(() => connection.Execute(sql, parameters, createCommand: createCommand)).ExecuteWithRetry(NumberOfRetries, RetryInterval, ShouldRetryOnException);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not execute\r\n{0}\r\non {1}".FormatWith(sql, connection.ConnectionString), ex);
            }
        }

        private string GetDatabaseSystemObjectText(SqlConnection conn, string objectName)
        {
            var query = String.Format("sp_helptext '{0}'", objectName);
            var textInfo = ExecuteWithRetry<DataTable>(conn, query);
            if (textInfo == null || textInfo.Rows.Count == 0) return null;
            return textInfo.Rows.Cast<DataRow>().Aggregate<DataRow, string>(null, (current, row) => current + row.GetStringValue("Text"));
        }

        /// <summary>
        /// Configures the database permissions on the target connection string so that NETWORK SERVICE, SYSTEM, and the publisher server have db owner permissions..
        /// </summary>
        /// <param name="machineAccount">The machine account.</param>
        /// <param name="connectionString">The connection string.</param>
        private static void ConfigureDatabasePermissions(string machineAccount, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                const string networkServiceAccount = @"NT AUTHORITY\NETWORK SERVICE";
                const string localSystemAccount = @"NT AUTHORITY\SYSTEM";

                const string sqlQueryRegisterAccountFormatString =
                    @"IF NOT EXISTS (SELECT name FROM master.sys.syslogins WHERE name = '{0}') BEGIN CREATE LOGIN [{0}] FROM WINDOWS END 
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = '{0}') BEGIN CREATE USER [{0}] FOR LOGIN [{0}] END 
exec sp_addrolemember 'db_owner', '{0}' ";

                var queries = new List<string>();

                // The following will create the login user for the NetworkServiceAccount if it doesnt exist, and add it to to the "db_owner" users group if it isn't part of it.
                queries.Add(string.Format(sqlQueryRegisterAccountFormatString, networkServiceAccount));

                // The following will create the login user for the LocalSystemAccount if it doesnt exist, and add it to to the "db_owner" users group if it isn't part of it.
                queries.Add(string.Format(sqlQueryRegisterAccountFormatString, localSystemAccount));

                // The following will create the login user for the domainname\machinename$ if it doesnt exist, and add it to to the "db_owner" users group if it isn't part of it.
                if (machineAccount != null) queries.Add(string.Format(sqlQueryRegisterAccountFormatString, machineAccount));

                using (SqlCommand command = connection.CreateCommand())
                {
                    command.Connection = connection;

                    foreach (string query in queries)
                    {
                        command.CommandText = query;
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceError(ex.ToString());
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get the replication metadata from a hub
        /// </summary>
        /// <param name="hubConnectionString">The hub connection string</param>
        /// <returns>null or a valid args</returns>
        public SqlReplicationArguments GetArgumentsFromHub(string hubConnectionString)
        {
            var args = new SqlReplicationArguments();
            SqlConnection connHubServerMaster = OpenDbServerMaster(hubConnectionString);

            try
            {
                string publisherServer;
                string publisherDatabase;
                ConnStringExtractServerDatabase(hubConnectionString, out publisherServer, out publisherDatabase);

                // Get current replication world
                ReplicationDistributor distributor;
                ReplicationPublisher publisher;
                if (!GetReplicationInfo(publisherServer, publisherDatabase, connHubServerMaster, out distributor, out publisher)) return null;

                // Ensure correct conn string
                args.HubConnectionString = hubConnectionString;

                // Add articles
                if (publisher != null && publisher.ArticleList != null) args.ReplicatedObjects = publisher.ArticleList.Select(a => a.Name).ToList();

                // Add partners
                if (publisher != null && publisher.SubscriberList != null) args.SpokeConnectionStrings = publisher.SubscriberList.Select(s => s.ConnectionString).ToList();
            }
            finally
            {
                if (connHubServerMaster != null) connHubServerMaster.Dispose();
            }
            return args;
        }

        /// <summary>
        /// Remove replication from a set of servers has defined by arguments
        /// </summary>
        /// <param name="arguments">The set of servers and articles to remove</param>
        /// <param name="forceDeleteIfReplicating">Drop replication regardless of state (default = false)</param>
        public void Delete(SqlReplicationArguments arguments, bool forceDeleteIfReplicating = false)
        {
            if (!IsValidDeleteArguments(arguments)) return;
            SqlConnection connHubServerMaster = OpenDbServerMaster(arguments.HubConnectionString);

            try
            {
                string publisherServer;
                string publisherDatabase;
                ConnStringExtractServerDatabase(arguments.HubConnectionString, out publisherServer, out publisherDatabase);

                // Get current replication world
                ReplicationDistributor distributor;
                ReplicationPublisher publisher;
                if (!GetReplicationInfo(publisherServer, publisherDatabase, connHubServerMaster, out distributor, out publisher)) throw new ApplicationException("Cannot complete operation. Check log file");

                try
                {
                    if (!forceDeleteIfReplicating && !IsCompletedAllReplication(distributor, publisher)) throw new ApplicationException("Cannot complete operation. There are replication jobs running");
                }
                catch (ReplicationException ex)
                {
                    // continue to delete on replication error (may need to delete an errored setup).
                    Trace.TraceError(ex.ToString());
                }

                // Confirm if delete can continue
                ConfirmValidRequestedDelete(arguments, publisher);

                // Augment with actual replicate items and note current state
                List<ReplicationSubscriber> existingSubscribers = AugmentSysObjReplicationSubscribers(publisher.ArticleList, publisher.SubscriberList);

                // Delete replication partners
                if (arguments.SpokeConnectionStrings != null)
                {
                    IEnumerable<ReplicationSubscriber> deletedSubscribers = DeleteReplicationPartners(arguments.SpokeConnectionStrings, connHubServerMaster, publisher);
                    if (existingSubscribers != null) existingSubscribers = existingSubscribers.Except(deletedSubscribers).ToList();
                }

                // Replication world changed so update
                publisher.SubscriberList = existingSubscribers;

                // Delete replicated objects
                if (arguments.ReplicatedObjects != null)
                {
                    IEnumerable<string> deletedObjects = DeleteReplicatedObjects(arguments.ReplicatedObjects, connHubServerMaster, publisher);
                    if (existingSubscribers != null) existingSubscribers.ForEach(s => s.DatabaseSystemObjects.RemoveAll(o => deletedObjects.Contains(o.Name, StringComparer.OrdinalIgnoreCase)));
                }

                // Look at replication world again
                if (!GetReplicationInfo(publisherServer, publisherDatabase, connHubServerMaster, out distributor, out publisher)) throw new ApplicationException("Cannot complete operation. Check log file");

                // If there are no subscribers and no articles then remove publisher

                if (existingSubscribers != null)
                {
                    existingSubscribers.ForEach(s => RemoveSubscriberInfo(connHubServerMaster, publisher, s));
                }

                RemovePublisherInfo(connHubServerMaster, publisher);

                TryRemoveDistributorInfo(connHubServerMaster);
            }
            finally
            {
                if (connHubServerMaster != null) connHubServerMaster.Dispose();
            }
        }

        /// <summary>
        /// Add replication to a set of servers, either a new setup or modify an existing, seeding optional
        /// </summary>
        /// <param name="arguments">The set of servers and articles to remove</param>
        /// <param name="seeding">The default step range value to use for seeding and any overrides per object</param>
        public void Configure(SqlReplicationArguments arguments, Seeding seeding = null)
        {
            if (!IsValidConfigureArguments(arguments)) throw new ArgumentException("SqlReplicationArguments Is Invalid");
            if (!IsValidSeeding(seeding)) throw new ArgumentException("Seeding Is Invalid");

            SqlConnection connHubServerMaster = OpenDbServerMaster(arguments.HubConnectionString);
            List<SeedMetadata> seedMetadata = LoadSeedMetadataFile();

            if (seedMetadata == null)
            {
                seedMetadata = GetSeedMetadataFromDb(arguments, seeding);

                SaveSeedMetadataFile(seedMetadata);
            }

            try
            {
                string publisherServer;
                string publisherDatabase;
                ConnStringExtractServerDatabase(arguments.HubConnectionString, out publisherServer, out publisherDatabase);

                if (Networking.IsInDomain())
                {
                    var hubMachineAccount = string.Format(@"{0}\{1}$", Networking.GetMachineNetBiosDomain(), publisherServer.Split('\\')[0]);
                    ConfigureDatabasePermissions(hubMachineAccount, arguments.HubConnectionString);
                    foreach (var spoke in arguments.SpokeConnectionStrings) ConfigureDatabasePermissions(hubMachineAccount, spoke);
                }

                ReplicationDistributor distributor = GetDistributorInfo(connHubServerMaster, publisherServer);
                if (!IsValidDistributor(distributor))
                {
                    ConfigureNew(arguments, connHubServerMaster, seedMetadata, publisherServer, publisherDatabase);
                }
                else
                {
                    // Add replication as needed
                    ReplicationPublisher publisher = GetPublisherInfo(distributor.ConnectionString, publisherDatabase);
                    if (publisher == null) CreatePublisherInfo(connHubServerMaster, publisherDatabase, seedMetadata);

                    // Get current replication world, get underlining system objects also
                    if (!GetReplicationInfo(publisherServer, publisherDatabase, connHubServerMaster, out distributor, out publisher, true)) throw new ApplicationException("Cannot complete operation. Check log file");

                    // Add additional replication as needed
                    List<string> replicatedObjects = null;
                    if (SqlReplicationArguments.DefaultReplicatedObjects != null && arguments.ReplicatedObjects != null)
                    {
                        replicatedObjects = SqlReplicationArguments.DefaultReplicatedObjects.Union(arguments.ReplicatedObjects, StringComparer.OrdinalIgnoreCase).ToList();
                    }
                    else if (SqlReplicationArguments.DefaultReplicatedObjects != null)
                    {
                        replicatedObjects = SqlReplicationArguments.DefaultReplicatedObjects.ToList();
                    }
                    else if (arguments.ReplicatedObjects != null)
                    {
                        replicatedObjects = arguments.ReplicatedObjects.ToList();
                    }

                    IEnumerable<string> fixupScripts = null;

                    List<string> addedObjects = replicatedObjects == null ? null : ConfigureReplicatedObjects(replicatedObjects, connHubServerMaster, publisher, out fixupScripts);
                    List<ReplicationSubscriber> addedSubscribers = arguments.SpokeConnectionStrings == null ? null : ConfigureReplicationPartners(arguments.SpokeConnectionStrings, connHubServerMaster, distributor, publisher);

                    if (addedObjects != null) Trace.TraceInformation("Added {0} objects for replication.".FormatWith(addedObjects.Count.ToString()));
                    if (addedSubscribers != null) Trace.TraceInformation("Added {0} subscribers for replication.".FormatWith(addedSubscribers.Count.ToString()));

                    // Get updated replication world, get underlining system objects also
                    if (!GetReplicationInfo(publisherServer, publisherDatabase, connHubServerMaster, out distributor, out publisher, true)) throw new ApplicationException("Cannot complete operation. Check log file");

                    // Create initial snapshot
                    StartSnapshotAgentJob(connHubServerMaster, publisherDatabase);

                    if (!HoldUntilSnapshotGenerated(distributor, publisher.Database)) throw new ApplicationException("Snapshot generation failed");

                    if (fixupScripts != null)
                    {
                        connHubServerMaster.ChangeDatabase(publisher.Database);
                        fixupScripts.ToList().ForEach(s => ExecuteWithRetry(connHubServerMaster, s));
                    }
                }

                CleanupSeedMetadata();
            }
            finally
            {
                if (connHubServerMaster != null) connHubServerMaster.Dispose();
            }
        }

        #endregion

        #region Helpers - Get (General)

        /// <summary>
        /// Returns a connection to the master database given the hub connecton string
        /// </summary>
        /// <param name="connString">Hub connection string</param>
        /// <returns>Connection to master</returns>
        private SqlConnection OpenDbServerMaster(string connString)
        {
            if (String.IsNullOrWhiteSpace(connString)) throw new ArgumentException("Connection String Is Invalid");

            string db = ConnStringExtractDatabase(connString);
            if (String.IsNullOrEmpty(db) || db.Equals("distribution", StringComparison.OrdinalIgnoreCase)) throw new ArgumentException("Please Pass The Hub. Passing The Distribution DB Is Redundant");

            var masterConnString = ConnStringSubstituteDatabase(connString, "master");
            var conn = new SqlConnection(masterConnString);
            conn.Open();
            return conn;
        }

        /// <summary>
        /// Get the replication metadata
        /// </summary>
        /// <param name="publisherServer">Hub server passed by client</param>
        /// <param name="publisherDatabase">Hub database passed by client</param>
        /// <param name="publishingMaster">Connection to hub master database</param>
        /// <param name="distributor">Out parameter for ReplicationDistributor</param>
        /// <param name="publisher">Out parameter for ReplicationPublisher</param>
        /// <param name="augmentSysObj">Flag to indicate if underlining system objects should be included</param>
        private bool GetReplicationInfo(string publisherServer, string publisherDatabase, SqlConnection publishingMaster, out ReplicationDistributor distributor, out ReplicationPublisher publisher, bool augmentSysObj = false)
        {
            publisher = null;
            distributor = GetDistributorInfo(publishingMaster, publisherServer);

            // Ensure that distributor is configured correctly
            if (distributor == null || !distributor.IsInstalled || !distributor.IsDbInstalled) { Trace.TraceError("No Distributor"); return false; }
            if (!distributor.IsPublisher && !distributor.HasRemotePublisher) { Trace.TraceError("Distribution Not Configured"); return false; }
            if (String.IsNullOrEmpty(distributor.Server) || String.IsNullOrEmpty(distributor.ConnectionString)) { Trace.TraceError("Distributor Connection String Missing"); return false; }

            // Get hub and spoke servers
            distributor.HubSpokeServerSetup = GetHubSpokeInfo(distributor.ConnectionString);
            if (distributor.HubSpokeServerSetup == null) { Trace.TraceError("Missing publisher (HubSpokeServerSetup)"); return false; }

            publisher = GetPublisherInfo(distributor.ConnectionString, publisherDatabase);
            if (publisher == null) { Trace.TraceError("Missing publisher"); return false; }

            publisher = AugmentHubSpokeReplicationPublisher(distributor.HubSpokeServerSetup, publisher);
            if (!publisher.Server.Equals(publisherServer, StringComparison.OrdinalIgnoreCase)) { Trace.TraceError("Mismatch publisher server"); return false; }
            if (!publisher.Database.Equals(publisherDatabase, StringComparison.OrdinalIgnoreCase)) { Trace.TraceError("Mismatch publisher database"); return false; }

            publisher.ArticleList = GetArticleInfo(distributor.ConnectionString, publisher.Database);
            publisher.SubscriberList = GetSubscriberInfo(publisher.ConnectionString);

            if (augmentSysObj)
            {
                publisher.ArticleList = AugmentSysObjReplicationArticles(publishingMaster, publisher.Database, publisher.ArticleList);
                publisher.SubscriberList = AugmentSysObjReplicationSubscribers(publisher.ArticleList, publisher.SubscriberList);
            }
            return true;
        }

        private ReplicationPublisher AugmentHubSpokeReplicationPublisher(HubSpokeServer hubSpokeServer, ReplicationPublisher publisher)
        {
            publisher.ConnectionString = ConnStringSubstituteServer(publisher.ConnectionString, hubSpokeServer.Hub);
            publisher.Server = hubSpokeServer.Hub;
            return publisher;
        }

        /// <summary>
        /// Add the underling system objects for a list of articles
        /// </summary>
        /// <param name="conn">Connection to master database</param>
        /// <param name="publisherDatabase">Name of database containing the articles</param>
        /// <param name="articleList">List of articles that system objects should be added for</param>
        /// <returns>List of ReplicationArticle</returns>
        private List<ReplicationArticle> AugmentSysObjReplicationArticles(SqlConnection conn, string publisherDatabase, List<ReplicationArticle> articleList)
        {
            if (articleList == null) return null;
            conn.ChangeDatabase(publisherDatabase);
            foreach (var article in articleList) article.DatabaseSystemObject = GetDatabaseSystemObject(conn, article.Name);
            return articleList;
        }

        /// <summary>
        /// Add the underling system objects for a list of subscribers
        /// </summary>
        /// <param name="articleList">List of articles that system objects should be added for</param>
        /// <param name="subscriberList">List of subscribers that system objects should be added for</param>
        /// <returns>List of ReplicationSubscriber</returns>
        private List<ReplicationSubscriber> AugmentSysObjReplicationSubscribers(List<ReplicationArticle> articleList, List<ReplicationSubscriber> subscriberList)
        {
            if (articleList == null || subscriberList == null) return subscriberList;
            foreach (var subscriber in subscriberList)
            {
                using (var conn = new SqlConnection(subscriber.ConnectionString))
                {
                    conn.Open();
                    subscriber.DatabaseSystemObjects = GetDatabaseSystemObjects(conn, articleList.Select(a => a.Name).ToList());
                }
            }
            return subscriberList;
        }

        #endregion

        #region Hub-Spoke-Setup

        private HubSpokeServer GetHubSpokeInfo(string distributorConnectionString)
        {
            using (var distributorConnection = new SqlConnection(distributorConnectionString))
            {
                distributorConnection.Open();
                var hubServer = (QueryDistAgentMatchDb(distributorConnection) ?? QueryDistAgentVirtual(distributorConnection)) ?? QuerySnapshotAgent(distributorConnection);
                if (String.IsNullOrEmpty(hubServer)) return null;
                var hubSpokeServer = new HubSpokeServer();
                hubSpokeServer.Hub = hubServer;
                hubSpokeServer.Spoke = QuerySubscriberInfo(distributorConnection, hubSpokeServer.Hub);
                return hubSpokeServer;
            }
        }

        private string QueryDistAgentMatchDb(SqlConnection distributorConnection)
        {
            const string query = "SELECT id, name, publisher_db, publication FROM msdistribution_agents WHERE publisher_db = subscriber_db GROUP BY id, name, publisher_db, publication";
            var agentInfo = ExecuteWithRetry<DataTable>(distributorConnection, query);
            return ParseForHubInfo(agentInfo);
        }

        private string QueryDistAgentVirtual(SqlConnection distributorConnection)
        {
            const string query = "SELECT TOP 1 id, name, publisher_db, publication FROM msdistribution_agents WHERE subscriber_db = 'virtual'";
            var agentInfo = ExecuteWithRetry<DataTable>(distributorConnection, query);
            return ParseForHubInfo(agentInfo);
        }

        private string QuerySnapshotAgent(SqlConnection distributorConnection)
        {
            const string query = "SELECT TOP 1 id, name, publisher_db, publication FROM mssnapshot_agents ORDER BY id";
            var agentInfo = ExecuteWithRetry<DataTable>(distributorConnection, query);
            return ParseForHubInfo(agentInfo);
        }

        private string ParseForHubInfo(DataTable agentInfo)
        {
            if (agentInfo == null || agentInfo.Rows.Count == 0) return null;

            var servers = new Dictionary<string, int>();
            foreach (DataRow row in agentInfo.Rows)
            {
                var name = row.GetStringValue("name"); // e.g (a)IOP-DT117\HUB-AdventureWorks-PUBIT-IOP-DT117\SPOKEONE-3 (b)IOP-DT117\HUB-AdventureWorks-PUBIT--3 (c)IOP-DT117\HUB-AdventureWorks-PUBIT-3
                var database = row.GetStringValue("publisher_db"); // e.g AdventureWorks

                if (database.Length > 21) database = database.Substring(0, 21);

                var endOfHubName = name.IndexOf("-" + database, StringComparison.Ordinal);
                if (endOfHubName == -1) continue;
                var hubName = name.Substring(0, endOfHubName);

                if (servers.ContainsKey(hubName))
                    servers[hubName] = servers[hubName] + 1;
                else
                    servers.Add(hubName, 1);
            }
            if (servers.Count == 0) return null;

            var max = servers.Max(s => s.Value);
            return servers.First(s => s.Value == max).Key;
        }

        private List<string> QuerySubscriberInfo(SqlConnection distributorConnection, string hubServer)
        {
            var query = String.Format("SELECT DISTINCT subscriber FROM mssubscriber_info WHERE publisher = '{0}' AND subscriber NOT IN ('{0}','{1}')", hubServer, distributorConnection.DataSource);
            var info = ExecuteWithRetry<DataTable>(distributorConnection, query);
            return ParseForSpokeInfo(info);
        }

        private List<string> ParseForSpokeInfo(DataTable info)
        {
            if (info == null || info.Rows.Count == 0) return null;
            var servers = info.Rows.Cast<DataRow>().Select(row => row.GetStringValue("subscriber")).ToList();
            return servers.Count == 0 ? null : servers;
        }

        #endregion

        #region Helpers - Delete

        /// <summary>
        /// Process list of partners that need to be removed from replication
        /// </summary>
        /// <param name="partnersConnStrings">List of servers to be removed</param>
        /// <param name="publishingMaster">Connection for hub</param>
        /// <param name="publisher">Replication publisher</param>
        /// <returns>List of ReplicationSubscriber that were removed</returns>
        private IEnumerable<ReplicationSubscriber> DeleteReplicationPartners(IEnumerable<string> partnersConnStrings, SqlConnection publishingMaster, ReplicationPublisher publisher)
        {
            var deletedSubscribers = new List<ReplicationSubscriber>();
            foreach (var connString in partnersConnStrings)
            {
                string partnerServer;
                string partnerDatabase;
                ConnStringExtractServerDatabase(connString, out partnerServer, out partnerDatabase);
                if (partnerServer == null || partnerDatabase == null) throw new ArgumentException("Replication Partner Connection String Is Invalid");

                ReplicationSubscriber subscriber = publisher.SubscriberList.First(s => s.Server.Equals(partnerServer, StringComparison.OrdinalIgnoreCase) && s.Database.Equals(partnerDatabase, StringComparison.OrdinalIgnoreCase));
                RemoveSubscriberInfo(publishingMaster, publisher, subscriber);
                deletedSubscribers.Add(subscriber);
            }
            return deletedSubscribers;
        }

        /// <summary>
        /// Process list of objects that need to be removed from replication
        /// </summary>
        /// <param name="objects">List of objects to be removed</param>
        /// <param name="publishingMaster">Connection to hub</param>
        /// <param name="publisher">Replication publisher</param>
        /// <returns>List of replicated object that were removed</returns>
        private IEnumerable<string> DeleteReplicatedObjects(IEnumerable<string> objects, SqlConnection publishingMaster, ReplicationPublisher publisher)
        {
            if (publisher.SubscriberList != null) publisher.SubscriberList.ForEach(s => RemoveSubscriberInfo(publishingMaster, publisher, s));
            var removing = objects.ToList();
            removing.ForEach(o => RemoveArticleInfo(publishingMaster, publisher, o));
            return removing;
        }

        #endregion

        #region Helpers - Configure

        /// <summary>
        /// Create a new replication setup
        /// </summary>
        /// <param name="arguments">The set of servers and articles to create</param>
        /// <param name="publishingMaster">Connection to hub master database</param>
        /// <param name="seedMetadata">The default step range value to use for seeding and any overrides per object</param>
        /// <param name="publisherServer">Server for the hub</param>
        /// <param name="publisherDatabase">Database for the hub</param>
        private void ConfigureNew(SqlReplicationArguments arguments, SqlConnection publishingMaster, List<SeedMetadata> seedMetadata, string publisherServer, string publisherDatabase)
        {
            TryRemoveDistributorInfo(publishingMaster);
            CreateDistributorInfo(publishingMaster);
            CreatePublisherInfo(publishingMaster, publisherDatabase, seedMetadata);
            List<string> replicatedObjects = null;
            if (SqlReplicationArguments.DefaultReplicatedObjects != null && arguments.ReplicatedObjects != null)
            {
                replicatedObjects = SqlReplicationArguments.DefaultReplicatedObjects.Union(arguments.ReplicatedObjects, StringComparer.OrdinalIgnoreCase).ToList();
            }
            else if (SqlReplicationArguments.DefaultReplicatedObjects != null)
            {
                replicatedObjects = SqlReplicationArguments.DefaultReplicatedObjects.ToList();
            }
            else if (arguments.ReplicatedObjects != null)
            {
                replicatedObjects = arguments.ReplicatedObjects.ToList();
            }

            IEnumerable<string> fixupScripts = null;
            if (replicatedObjects != null) CreateArticles(publishingMaster, publisherDatabase, replicatedObjects, out fixupScripts);
            if (arguments.SpokeConnectionStrings != null) arguments.SpokeConnectionStrings.ToList().ForEach(c => CreateSubscriberInfo(publisherServer, publishingMaster, publisherServer, publisherDatabase, c));

            // Create initial snapshot
            // todo - comment out and see if databases are still replicating even though they're out of sync.
            StartSnapshotAgentJob(publishingMaster, publisherDatabase);

            // Allow snapshot time to generate
            ReplicationDistributor distributor;
            ReplicationPublisher publisher;
            if (!GetReplicationInfo(publisherServer, publisherDatabase, publishingMaster, out distributor, out publisher)) throw new ApplicationException("Cannot complete operation. Check log file");

            if (!HoldUntilSnapshotGenerated(distributor, publisher.Database)) throw new ApplicationException("Snapshot generation failed");

            if (fixupScripts != null)
            {
                publishingMaster.ChangeDatabase(publisher.Database);
                fixupScripts.ToList().ForEach(s => ExecuteWithRetry(publishingMaster, s));
            }
        }

        /// <summary>
        /// Add objects to an existing replication setup
        /// </summary>
        /// <param name="objects">The objects.</param>
        /// <param name="publishingMaster">The publishing master.</param>
        /// <param name="publisher">The publisher.</param>
        /// <param name="fixupScripts">The fixup scripts.</param>
        /// <returns></returns>
        private List<string> ConfigureReplicatedObjects(IEnumerable<string> objects, SqlConnection publishingMaster, ReplicationPublisher publisher, out IEnumerable<string> fixupScripts)
        {
            List<string> objectsToAdd = objects.Where(obj => publisher.ArticleList == null || !publisher.ArticleList.Any(a => a.Name.Equals(obj, StringComparison.OrdinalIgnoreCase))).ToList();
            CreateArticles(publishingMaster, publisher.Database, objectsToAdd, out fixupScripts);
            return objectsToAdd;
        }

        /// <summary>
        /// Add partners to an existing replication setup
        /// </summary>
        /// <param name="partnersConnStrings">List of servers to be added</param>
        /// <param name="publishingMaster">Connection to hub</param>
        /// <param name="distributor">Replication distributor</param>
        /// <param name="publisher">Replication publisher</param>
        /// <returns>List of ReplicationSubscriber that were added</returns>
        private List<ReplicationSubscriber> ConfigureReplicationPartners(IEnumerable<string> partnersConnStrings, SqlConnection publishingMaster, ReplicationDistributor distributor, ReplicationPublisher publisher)
        {
            var addedSubscribers = new List<ReplicationSubscriber>();
            foreach (var connString in partnersConnStrings)
            {
                string partnerServer;
                string partnerDatabase;
                ConnStringExtractServerDatabase(connString, out partnerServer, out partnerDatabase);
                if (partnerServer == null || partnerDatabase == null) throw new ArgumentException("Replication Partner Connection String Is Invalid");

                if (publisher.SubscriberList != null && publisher.SubscriberList.Any(s => s.Server.Equals(partnerServer, StringComparison.OrdinalIgnoreCase) && s.Database.Equals(partnerDatabase, StringComparison.OrdinalIgnoreCase))) continue;
                CreateSubscriberInfo(distributor.Server, publishingMaster, publisher.Server, publisher.Database, connString);
                addedSubscribers.Add(new ReplicationSubscriber { ConnectionString = connString, Server = partnerServer, Database = partnerDatabase });
            }
            return addedSubscribers;
        }

        #endregion

        #region Seeding

        /// <summary>
        /// Get the seeding configuration to be used after replication set up
        /// </summary>
        /// <param name="arguments">The set of servers and articles to remove</param>
        /// <param name="seeding">The default step range value to use for seeding and any overrides per object</param>
        /// <returns>Seeding configuration</returns>
        private List<SeedMetadata> GetSeedMetadataFromDb(SqlReplicationArguments arguments, Seeding seeding)
        {
            string server;
            string database;
            ConnStringExtractServerDatabase(arguments.HubConnectionString, out server, out database);
            if (server == null || database == null) throw new ArgumentException("Hub Connection String Is Invalid");

            var initialSeedMetadata = new List<SeedMetadata> { new SeedMetadata { ConnectionString = arguments.HubConnectionString, Server = server, Database = database, ServerIndex = 0 } };
            List<SeedMetadata> spokesSeedMetadata = GetSpokeSeedMetadata(arguments.SpokeConnectionStrings);
            if (spokesSeedMetadata != null) initialSeedMetadata.AddRange(spokesSeedMetadata);

            List<string> replicatedObjects = null;
            if (SqlReplicationArguments.DefaultReplicatedObjects != null && arguments.ReplicatedObjects != null)
            {
                replicatedObjects = SqlReplicationArguments.DefaultReplicatedObjects.Union(arguments.ReplicatedObjects, StringComparer.OrdinalIgnoreCase).ToList();
            }
            else if (SqlReplicationArguments.DefaultReplicatedObjects != null)
            {
                replicatedObjects = SqlReplicationArguments.DefaultReplicatedObjects.ToList();
            }
            else if (arguments.ReplicatedObjects != null)
            {
                replicatedObjects = arguments.ReplicatedObjects.ToList();
            }

            var finSeedMetadata = new List<SeedMetadata>();
            foreach (var metadata in initialSeedMetadata)
            {
                metadata.ReferrerSeeding = seeding;
                finSeedMetadata.Add(ComputeSeedMetadata(metadata, replicatedObjects, seeding));
            }
            return finSeedMetadata;
        }

        /// <summary>
        /// Process the list of spoke connection strings and compute the server index
        /// </summary>
        /// <param name="spokeConnectionStrings">List of spoke connection strings</param>
        /// <returns>List of seeding configuration</returns>
        private List<SeedMetadata> GetSpokeSeedMetadata(IEnumerable<string> spokeConnectionStrings)
        {
            if (spokeConnectionStrings == null) return null;
            var metadata = new List<SeedMetadata>();
            var index = 1;
            foreach (var connString in spokeConnectionStrings)
            {
                string server;
                string database;
                ConnStringExtractServerDatabase(connString, out server, out database);
                if (server == null || database == null) throw new ArgumentException("Replication Partner Connection String Is Invalid");
                var seedMetadata = new SeedMetadata { ConnectionString = connString, Server = server, Database = database, ServerIndex = index };
                metadata.Add(seedMetadata);
                index++;
            }
            return metadata;
        }

        /// <summary>
        /// Compute the seed value for each object that is being replicated
        /// </summary>
        /// <param name="seedMetadata">Seeding configuration with empty list</param>
        /// <param name="objects">Objects to be replicated</param>
        /// <param name="seeding">The default step range value to use for seeding and any overrides per object</param>
        /// <returns>Seeding configuration with list set</returns>
        private SeedMetadata ComputeSeedMetadata(SeedMetadata seedMetadata, IEnumerable<string> objects, Seeding seeding)
        {
            seedMetadata.SeedList = new List<SeedItemMetadata>();
            if (objects == null) return seedMetadata;

            using (var conn = new SqlConnection(seedMetadata.ConnectionString))
            {
                conn.Open();
                foreach (var obj in objects)
                {
                    DbSysObj dbSysObj = GetDatabaseSystemObject(conn, obj);
                    if (dbSysObj == null)
                    {
                        var seedItemMetadata = new SeedItemMetadata { Name = obj, Value = GetDefaultSeed(seeding, obj, seedMetadata.ServerIndex) };
                        if (seeding != null) seedMetadata.SeedList.Add(seedItemMetadata);
                        continue;
                    }
                    if (dbSysObj.Type == "U" && dbSysObj.IsIndexed && dbSysObj.TableHasIdentity)
                    {
                        var seedItemMetadata = new SeedItemMetadata { Name = obj, Value = dbSysObj.CurrentSeedValue };
                        seedMetadata.SeedList.Add(seedItemMetadata);
                    }
                }
            }
            return seedMetadata;
        }

        /// <summary>
        /// Gets the default seed for the server index specified. Returns a multiple of a constant value.
        /// </summary>
        /// <param name="seeding">The default step range value to use for seeding and any overrides per object</param>
        /// <param name="objectName">Name of object</param>
        /// <param name="serverIndex">Index of the server</param>
        /// <returns>Floor value of identity</returns>
        private long GetDefaultSeed(Seeding seeding, string objectName, int serverIndex)
        {
            SeededObject seededObject = seeding.SeedingOverrides == null ? null : seeding.SeedingOverrides.FirstOrDefault(so => so.Name.Equals(objectName, StringComparison.OrdinalIgnoreCase));
            if (seededObject != null) return serverIndex * seededObject.StepRange + 1;
            return serverIndex * seeding.ServerStepRange + 1;
        }

        #endregion

        #region Distributor

        private ReplicationDistributor GetDistributorInfo(SqlConnection conn, string publisherServer)
        {
            conn.ChangeDatabase("master");
            var distributorInfo = ExecuteWithRetry<DataTable>(conn, "sp_get_distributor");
            var distributor = ParseDistributorInfo(conn, distributorInfo);
            if (distributor == null || !distributor.IsInstalled) return null;
            // check to see if distributor is remote
            if (!distributor.Server.Equals(publisherServer, StringComparison.OrdinalIgnoreCase))
            {
                Trace.TraceInformation("Distributor is remote: {0}", distributor.ConnectionString);
                using (var distributorConnection = new SqlConnection(distributor.ConnectionString))
                {
                    distributorConnection.Open();
                    distributorInfo = ExecuteWithRetry<DataTable>(distributorConnection, "sp_get_distributor");
                    distributor = ParseDistributorInfo(distributorConnection, distributorInfo);
                }
            }
            return distributor;
        }

        private ReplicationDistributor ParseDistributorInfo(SqlConnection conn, DataTable distributorInfo)
        {
            if (distributorInfo == null || distributorInfo.Rows.Count == 0) return null;
            return new ReplicationDistributor
            {
                ConnectionString = ConnStringSubstituteServerDatabase(conn.ConnectionString, distributorInfo.GetStringValue("distribution server"), "distribution"),
                IsInstalled = distributorInfo.GetBoolValue("installed"),
                Server = distributorInfo.GetStringValue("distribution server"),
                IsDbInstalled = distributorInfo.GetBoolValue("distribution db installed"),
                IsPublisher = distributorInfo.GetBoolValue("is distribution publisher"),
                HasRemotePublisher = distributorInfo.GetBoolValue("has remote distribution publisher")
            };
        }

        [DebuggerNonUserCode]
        private void TryRemoveDistributorInfo(SqlConnection conn)
        {
            try
            {
                conn.ChangeDatabase("master");

                //sp_dropdistpublisher
                var dropDistributionPublisherParameters = new Dictionary<string, object> { { "publisher", conn.DataSource }, { "no_checks", 1 } };
                conn.Execute("sp_dropdistpublisher", dropDistributionPublisherParameters, false, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch 
            {
                // expected to fail in some environments
            }
            try
            {
                var dropDistributionDbParameters = new Dictionary<string, object> { { "database", "distribution" } };
                conn.Execute("sp_dropdistributiondb", dropDistributionDbParameters, false, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch 
            {
                // expected to fail in some environments
            }
            try
            {
                var dropDistributorParameters = new Dictionary<string, object> { { "no_checks", 1 } };
                conn.Execute("sp_dropdistributor", dropDistributorParameters, false, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch 
            {
                // expected to fail in some environments
            }
        }

        private void CreateDistributorInfo(SqlConnection conn)
        {
            var distributorParams = new Dictionary<string, object> { { "distributor", conn.DataSource } };
            var distributorDbParams = new Dictionary<string, object> { { "database", "distribution" } };
            var distPublisherParams = new Dictionary<string, object> { { "publisher", conn.DataSource }, { "distribution_db", "distribution" } };
            conn.ChangeDatabase("master");
            ExecuteWithRetry(conn, "sp_adddistributor", distributorParams, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
            ExecuteWithRetry(conn, "sp_adddistributiondb", distributorDbParams, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
            ExecuteWithRetry(conn, "sp_adddistpublisher", distPublisherParams, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
        }

        #endregion

        #region Publisher

        private ReplicationPublisher GetPublisherInfo(string distributorConnectionString, string publisherDatabase)
        {
            using (var distributorConnection = new SqlConnection(distributorConnectionString))
            {
                distributorConnection.Open();
                string query = string.Format("SELECT publisher_db, publication FROM MSpublications WHERE publisher_db = '{0}' GROUP BY publisher_db, publication", publisherDatabase);
                var publisherInfo = ExecuteWithRetry<DataTable>(distributorConnection, query);
                return ParsePublisherInfo(distributorConnection, publisherInfo);
            }
        }

        private ReplicationPublisher ParsePublisherInfo(SqlConnection conn, DataTable publisherInfo)
        {
            if (publisherInfo == null || publisherInfo.Rows.Count == 0) return null;
            return new ReplicationPublisher
            {
                ConnectionString = ConnStringSubstituteDatabase(conn.ConnectionString, publisherInfo.GetStringValue("publisher_db")),
                Name = publisherInfo.GetStringValue("publication"),
                Server = ConnStringExtractServer(conn.ConnectionString),
                Database = publisherInfo.GetStringValue("publisher_db")
            };
        }

        private void RemovePublisherInfo(SqlConnection publishingMaster, ReplicationPublisher publisher)
        {
            var newConn = new SqlConnection(publishingMaster.ConnectionString);
            try
            {
                publishingMaster.ChangeDatabase(publisher.Database);
                ExecuteWithRetry(publishingMaster, "sp_replflush", null, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
              
                newConn.Open();
                newConn.ChangeDatabase(publisher.Database);
                var dropPublicationParams = new Dictionary<string, object> { { "publication", publisher.Name } };
                ExecuteWithRetry(newConn, "sp_droppublication", dropPublicationParams, () => new SqlCommand { Connection = newConn, CommandType = CommandType.StoredProcedure, CommandTimeout = newConn.ConnectionTimeout });

                newConn.ChangeDatabase("master");
                var replicationDbOptionParams = new Dictionary<string, object> { { "dbname", publisher.Database }, { "optname", "publish" }, { "value", "false" } };
                ExecuteWithRetry(newConn, "sp_replicationdboption", replicationDbOptionParams, () => new SqlCommand { Connection = newConn, CommandType = CommandType.StoredProcedure, CommandTimeout = newConn.ConnectionTimeout });
            }
            catch (SqlException sqlEx)
            {
                if (sqlEx.Number != 18752) throw; // Drop is successful however MSSQLServer still complains (Only one Log Reader agent or log-related procedure can connect to a database at a time.)
            }
            finally
            {
                newConn.Dispose();
            }
        }

        private void CreatePublisherInfo(SqlConnection publishingMaster, string publisherDatabase, List<SeedMetadata> seedMetadata)
        {
            publishingMaster.ChangeDatabase(publisherDatabase);

            var dbInfo = ExecuteWithRetry<DataTable>(publishingMaster, "SELECT TOP 1 physical_name FROM sys.master_files");
            if (dbInfo == null || dbInfo.Rows.Count == 0) throw new ApplicationException();
            var sqlFilePath = dbInfo.GetStringValue("physical_name");
            var sqlDirectory = Path.GetDirectoryName(sqlFilePath);

            var replicationDbOptionParams = new Dictionary<string, object> { { "dbname", publisherDatabase }, { "optname", "publish" }, { "value", "true" } };
            var queueReaderAgentParams = new Dictionary<string, object> { { "frompublisher", "1" } };

            var accessibleSqlDirectory = sqlDirectory;
            if (publishingMaster.DataSource != null)
            {
                var serverName = publishingMaster.DataSource.Split('\\').First();
                if (!new[] { "localhost", Environment.MachineName.Split('.').First().ToLower() }.Contains(serverName.Split('.').First().ToLower()))
                {
                    accessibleSqlDirectory = @"\\{0}\{1}".FormatWith(serverName, sqlDirectory.ReplaceFirst(":", "$"));
                }
            }
            string postSnapshotScriptPath = Path.Combine(accessibleSqlDirectory ?? string.Empty, "Seeding_{0}.sql".FormatWith(DateTime.Now.ToString("yyyy.MM.dd_HH.mm.ss")));
            File.WriteAllText(postSnapshotScriptPath, CreateSeedingScript(seedMetadata));

            string preSnapshotScriptPath = Path.Combine(accessibleSqlDirectory ?? string.Empty, "PreSnapshotScript.sql");
            File.WriteAllText(preSnapshotScriptPath, "DECLARE @sql varchar(4000) = 'osql -E -' + CONVERT(varchar, SERVERPROPERTY('ServerName'))" + Environment.NewLine +
                                                     @" + ' -q ""BACKUP DATABASE {0} TO DISK = ''{0}'' ""' ".FormatWith(publisherDatabase) + Environment.NewLine +
                                                     "EXEC master.dbo.xp_cmdshell @sql");

            var publicationParams = new Dictionary<string, object>
                                        {
                                            {"publication", publisherDatabase}, {"sync_method", "concurrent"}, {"retention", 0}, {"allow_push", "true"}, {"allow_pull", "true"},
                                            {"allow_anonymous", "true"}, {"repl_freq", "continuous"}, {"status", "active"}, {"independent_agent", "true"}, {"immediate_sync", "true"},
                                            {"allow_sync_tran", "true"}, {"autogen_sync_procs", "true"}, {"allow_queued_tran", "true"}, {"allow_dts", "false"}, {"conflict_policy", "pub wins"},
                                            {"centralized_conflicts", "true"}, {"queue_type", "sql"}, {"replicate_ddl", 1}, {"allow_initialize_from_backup", "false"},
                                            {"snapshot_in_defaultfolder", "false"}, {"alt_snapshot_folder", sqlDirectory}, {"post_snapshot_script", postSnapshotScriptPath }, {"pre_snapshot_script", preSnapshotScriptPath }
                                        };
            var publicationSnapshotParams = new Dictionary<string, object> { { "publication", publisherDatabase }, { "frequency_type", 1 }, { "frequency_interval", 1 }, { "publisher_security_mode", 1 } };

            ExecuteWithRetry(publishingMaster, "sp_replicationdboption", replicationDbOptionParams, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

            ExecuteWithRetry(publishingMaster, "sp_changedistributiondb", new Dictionary<string, object> { { "database", "distribution" }, { "property", "max_distretention" }, { "value", 672 } }, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

            var queueReaderAgentInfo = ExecuteWithRetry<DataTable>(publishingMaster, "sp_helpqreader_agent", queueReaderAgentParams, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
            if (queueReaderAgentInfo == null || queueReaderAgentInfo.Rows.Count == 0) ExecuteWithRetry(publishingMaster, publisherDatabase + ".sys.sp_addqreader_agent", queueReaderAgentParams, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

            var logReaderAgentInfo = ExecuteWithRetry<DataTable>(publishingMaster, "sp_helplogreader_agent", null, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
            if (logReaderAgentInfo == null || logReaderAgentInfo.Rows.Count == 0) ExecuteWithRetry(publishingMaster, publisherDatabase + ".sys.sp_addlogreader_agent", null, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

            ExecuteWithRetry(publishingMaster, "sp_addpublication", publicationParams, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
            ExecuteWithRetry(publishingMaster, "sp_addpublication_snapshot", publicationSnapshotParams, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
        }

        /// <summary>
        /// Creates a seeding script to reseed all servers.
        /// </summary>
        /// <param name="seedMetadata">The seed metadata.</param>
        /// <returns></returns>
        private static string CreateSeedingScript(List<SeedMetadata> seedMetadata)
        {
            const string instanceNameQuery = @"@@SERVERNAME + '\' + @@SERVICENAME";
            var sb = new StringBuilder();

            var identityColumnNames = new Dictionary<string, string>();

            foreach (var s in seedMetadata)
            {
                using (var connection = new SqlConnection(s.ConnectionString))
                {
                    string instanceName = connection.Execute<string>("SELECT {0}".FormatWith(instanceNameQuery));
                    sb.AppendLine("IF ({0}) = '{1}'".FormatWith(instanceNameQuery, instanceName));
                    sb.AppendLine("BEGIN");
                    foreach (var i in s.SeedList)
                    {
                        var item = i;
                        Func<string, string> stripInvalidCharacters = x => x.Replace("[", "").Replace("]", "").Replace(".", "").Replace("-", "");

                        var seedVariableName = "@" + stripInvalidCharacters(instanceName.Replace("\\", ""))
                            + "_" + stripInvalidCharacters(item.Name);

                        string identityColumnName = identityColumnNames.GetValue(item.Name, () => connection.Execute<string>("SELECT TOP 1 c.name FROM sys.columns c WHERE is_identity = 1 AND object_id = OBJECT_ID('{0}')".FormatWith(item.Name)));

                        if (!string.IsNullOrEmpty(identityColumnName))
                        {
                            sb.AppendLine(
                                @"
    IF OBJECTPROPERTY(OBJECT_ID('{0}'), 'IsTable') = 1 AND OBJECTPROPERTY(OBJECT_ID('{0}'), 'TableHasIdentity') = 1
    BEGIN
        DECLARE {1} bigint
        SET {1} = {2}
        WHILE EXISTS(SELECT * FROM {0} WHERE {3} >= {1} AND {3} < {1} + 5000)
            SET {1} = {1} + 1
    
        DBCC CHECKIDENT('{0}', 'RESEED', {1})
    END".FormatWith(item.Name, seedVariableName, item.Value + 1, identityColumnName));
                        }
                    }

                    sb.AppendLine("END");
                }
            }

            return sb.ToString();
        }

        #endregion

        #region Article

        private List<ReplicationArticle> GetArticleInfo(string distributorConnectionString, string publisherDatabase)
        {
            using (var distributorConnection = new SqlConnection(distributorConnectionString))
            {
                distributorConnection.Open();
                var query = String.Format("SELECT article_id, article, source_owner, source_object, destination_object FROM MSArticles WHERE publisher_db = '{0}' GROUP BY article_id, article, source_owner, source_object, destination_object", publisherDatabase);
                var articleInfo = ExecuteWithRetry<DataTable>(distributorConnection, query);
                return ParseArticleInfo(articleInfo);
            }
        }

        private List<ReplicationArticle> ParseArticleInfo(DataTable articleInfo)
        {
            if (articleInfo == null || articleInfo.Rows.Count == 0) return null;
            return (from DataRow row in articleInfo.Rows
                    select new ReplicationArticle
                    {
                        Id = row.GetInt32Value("article_id"),
                        Name = row.GetStringValue("article"),
                        SourceOwner = row.GetStringValue("source_owner"),
                        SourceObject = row.GetStringValue("source_object"),
                        DestinationObject = row.GetStringValue("destination_object")
                    }).ToList();
        }

        private void RemoveArticleInfo(SqlConnection publishingMaster, ReplicationPublisher publisher, string article)
        {
            var parameters = new Dictionary<string, object> { { "publication", publisher.Name }, { "article", article }, { "force_invalidate_snapshot", 1 } };
            publishingMaster.ChangeDatabase(publisher.Database);
            ExecuteWithRetry(publishingMaster, "sp_droparticle", parameters, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
        }

        private void CreateArticles(SqlConnection publishingMaster, string publisherDatabase, List<string> articles, out IEnumerable<string> fixupScripts)
        {
            var fixupScriptsList = new List<string>();

            publishingMaster.ChangeDatabase(publisherDatabase);
            foreach (var article in articles)
            {
                DbSysObj dbSysObj = GetDatabaseSystemObject(publishingMaster, article);

                if (dbSysObj == null) { Trace.TraceWarning(article + " was not found in database " + publisherDatabase); continue; }

                if (dbSysObj.Type == "V")
                {
                    var text = GetDatabaseSystemObjectText(publishingMaster, dbSysObj.Name);
                    if (!String.IsNullOrEmpty(text) && text.Contains("WITH(NOEXPAND)"))
                    {
                        var alterView = text.Replace("CREATE VIEW", "ALTER VIEW");
                        fixupScriptsList.Add(alterView);
                        ExecuteWithRetry(publishingMaster, alterView.Replace("WITH(NOEXPAND)", string.Empty));
                    }
                }

                CreateArticleInfo(publishingMaster, publisherDatabase, dbSysObj);
            }

            fixupScripts = fixupScriptsList;
        }

        private void CreateArticleInfo(SqlConnection publishingMaster, string publisherDatabase, DbSysObj dbSysObj)
        {
            publishingMaster.ChangeDatabase(publisherDatabase);
            var parameters = new Dictionary<string, object>
                                 {
                                     {"force_invalidate_snapshot", 1}, {"publication", publisherDatabase}, {"article", dbSysObj.Name},
                                     {"source_owner", dbSysObj.SchemaName}, {"source_object", dbSysObj.ObjectName},
                                     {"destination_owner", dbSysObj.SchemaName}, {"destination_table", dbSysObj.ObjectName}
                                 };

            using (var command = new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout })
            {
                /*
                TR	SQL_TRIGGER
                D 	DEFAULT_CONSTRAINT
                PK	PRIMARY_KEY_CONSTRAINT
                F 	FOREIGN_KEY_CONSTRAINT
                U 	USER_TABLE
                P 	SQL_STORED_PROCEDURE
                V 	VIEW
                FN	SQL_SCALAR_FUNCTION
                TF	SQL_TABLE_VALUED_FUNCTION
                */
                // http://social.msdn.microsoft.com/Forums/is/sqlreplication/thread/3b65827a-1886-4b43-a6ed-a8c1b7d66e8c
                // http://stackoverflow.com/questions/8467072/sql-server-varbinary-bigint-with-bitconverter-toint64-values-are-different
                switch (dbSysObj.Type)
                {
                    case "U": // USER_TABLE 
                        parameters.Add("type", "logbased");
                        parameters.Add("identityrangemanagementoption", "manual");
                        var schemaOptionUserTable = BitConverter.GetBytes(0x0000000008635FDF); // original: 0x0000000008035CDF
                        if (BitConverter.IsLittleEndian) Array.Reverse(schemaOptionUserTable);
                        command.Parameters.Add(new SqlParameter("schema_option", schemaOptionUserTable) { SqlDbType = SqlDbType.VarBinary });
                        break;
                    case "P": // SQL_STORED_PROCEDURE
                        parameters.Add("type", "proc schema only");
                        break;
                    case "V": // VIEW
                        parameters.Add("type", (dbSysObj.IsIndexed || dbSysObj.IsSchemaBound) ? "indexed view schema only" : "view schema only");
                        var schemaOptionView = BitConverter.GetBytes(0x0000000008000151);
                        if (BitConverter.IsLittleEndian) Array.Reverse(schemaOptionView);
                        command.Parameters.Add(new SqlParameter("schema_option", schemaOptionView) { SqlDbType = SqlDbType.VarBinary });
                        break;
                    case "FN": // SQL_SCALAR_FUNCTION
                    case "TF": // SQL_TABLE_VALUED_FUNCTION
                        parameters.Add("type", "func schema only");
                        break;
                    default:
                        Trace.TraceWarning("Unhandled DatabaseSystemObject : {0} : {1}", dbSysObj.Type, dbSysObj.TypeDesc);
                        break;
                }
                ExecuteWithRetry(publishingMaster, "sp_addarticle", parameters, () => command);
            }
        }

        #endregion

        #region Subscriber

        private List<ReplicationSubscriber> GetSubscriberInfo(string publisherConnectionString)
        {
            using (var publisherConnection = new SqlConnection(publisherConnectionString))
            {
                publisherConnection.Open();
                var subscriberInfo = ExecuteWithRetry<DataTable>(publisherConnection, "sp_helpsubscription");
                return ParseSubscriberInfo(publisherConnection, subscriberInfo);
            }
        }

        private List<ReplicationSubscriber> ParseSubscriberInfo(SqlConnection conn, DataTable subscriberInfo)
        {
            if (subscriberInfo == null || subscriberInfo.Rows.Count == 0) return null;
            var view = new DataView(subscriberInfo);
            var distinct = view.ToTable(true, "subscriber", "destination database");
            return (from DataRow row in distinct.Rows
                    select new ReplicationSubscriber
                    {
                        ConnectionString = ConnStringSubstituteServerDatabase(conn.ConnectionString, row.GetStringValue("subscriber"), row.GetStringValue("destination database")),
                        Server = row.GetStringValue("subscriber"),
                        Database = row.GetStringValue("destination database")
                    }).ToList();
        }

        private void RemoveSubscriberInfo(SqlConnection publishingMaster, ReplicationPublisher publisher, ReplicationSubscriber subscriber)
        {
            var conn = new SqlConnection(subscriber.ConnectionString);
            try
            {
                conn.Open(); // Get subscriber connection at this point if problems then unnecessary work is avoided

                // Remove at the publisher database
                var publisherParams = new Dictionary<string, object> { { "publication", publisher.Name }, { "article", "all" }, { "subscriber", subscriber.Server }, { "destination_db", subscriber.Database } };
                publishingMaster.ChangeDatabase(publisher.Database);
                ExecuteWithRetry(publishingMaster, "sp_dropsubscription", publisherParams, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

                // Remove at the subscriber database
                var subscriberParams = new Dictionary<string, object> { { "publisher", publisher.Server }, { "publisher_db", publisher.Database }, { "publication", publisher.Name } };
                ExecuteWithRetry(conn, "sp_subscription_cleanup", subscriberParams, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
                ExecuteWithRetry(conn, "sp_removedbreplication", null, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
            }
            finally
            {
                conn.Dispose();
            }
        }

        private void CreateSubscriberInfo(string distributorServer, SqlConnection publishingMaster, string publisherServer, string publisherDatabase, string subscriberConnString)
        {
            string subscriberServer;
            string subscriberDatabase;
            ConnStringExtractServerDatabase(subscriberConnString, out subscriberServer, out subscriberDatabase);
            if (subscriberServer == null || subscriberDatabase == null) throw new ArgumentException("Subscriber Connection String Is Invalid");
            var conn = new SqlConnection(subscriberConnString);
            try
            {
                conn.Open(); // Get subscriber connection at this point if problems then unnecessary work is avoided

                // Add at the publisher database (distributor)
                var publisherSubscriptionParams = new Dictionary<string, object>
                                                      {
                                                          {"publication", publisherDatabase}, {"article", "all"}, {"subscriber", subscriberServer},
                                                          {"destination_db", subscriberDatabase}, {"subscription_type", "Push"}, {"sync_type", "automatic"}, {"update_mode", "queued failover"}
                                                      };
                var publisherPushSubscriptionAgentParams = new Dictionary<string, object> { { "publication", publisherDatabase }, { "subscriber", subscriberServer }, { "subscriber_db", subscriberDatabase }, { "subscriber_security_mode", 1 } };
                publishingMaster.ChangeDatabase(publisherDatabase);
                ExecuteWithRetry(publishingMaster, "sp_addsubscription", publisherSubscriptionParams, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
                ExecuteWithRetry(publishingMaster, "sp_addpushsubscription_agent", publisherPushSubscriptionAgentParams, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });

                // Add at the subscriber database
                var subscriberParams = new Dictionary<string, object>
                                           {
                                               {"publisher", publisherServer}, {"publisher_db", publisherDatabase},
                                               {"publication", publisherDatabase}, {"distributor", distributorServer}, {"security_mode", 1}
                                           };
                try
                {
                    ExecuteWithRetry(conn, "sp_link_publication", subscriberParams, () => new SqlCommand { Connection = conn, CommandType = CommandType.StoredProcedure, CommandTimeout = conn.ConnectionTimeout });
                }
                catch (Exception ex)
                {
                    var sqlException = ex as SqlException;
                    if (sqlException == null) sqlException = ex.InnerException as SqlException;
                    if (sqlException == null || sqlException.Number != 18456) throw; // Link is successful however MSSQLServer still complains (Login failed for user 'NT AUTHORITY\ANONYMOUS LOGON'.)
                }
            }
            finally
            {
                conn.Dispose();
            }
        }

        #endregion

        #region Snapshot

        /// <summary>
        /// Used to start the Snapshot Agent job that generates the snapshot for a publication
        /// </summary>
        /// <param name="publishingMaster">Connection to hub master database</param>
        /// <param name="publisherDatabase">Database for the hub</param>
        private void StartSnapshotAgentJob(SqlConnection publishingMaster, string publisherDatabase)
        {
            var publicationSnapshotParams = new Dictionary<string, object> { { "publication", publisherDatabase } };
            publishingMaster.ChangeDatabase(publisherDatabase);
            ExecuteWithRetry(publishingMaster, "sp_startpublication_snapshot", publicationSnapshotParams, () => new SqlCommand { Connection = publishingMaster, CommandType = CommandType.StoredProcedure, CommandTimeout = publishingMaster.ConnectionTimeout });
        }

        /// <summary>
        /// Loop and wait for snapshot to be generated
        /// </summary>
        /// <param name="distributor">Replication Distributor</param>
        /// <param name="publisherDatabase">Database for the hub</param>
        /// <returns>True once the snapshot is completed</returns>
        private bool HoldUntilSnapshotGenerated(ReplicationDistributor distributor, string publisherDatabase)
        {
            using (var distributorConnection = new SqlConnection(distributor.ConnectionString))
            {
                int runstatus;
                do
                {
                    Thread.Sleep(15000); // Pause for 15 seconds
                    runstatus = GetSnapshotAgentStatus(distributorConnection, distributor, publisherDatabase);
                } while (runstatus == 1 || runstatus == 3);

                if (IsSnapshotGenerated(distributorConnection, distributor, publisherDatabase)) return true;
            }
            return false;
        }

        /// <summary>
        /// Get the current status of the agent as it generates the snapshot
        /// </summary>
        /// <param name="distributorConnection">Connection to distributor</param>
        /// <param name="distributor">Replication Distributor</param>
        /// <param name="publisherDatabase">Database for the hub</param>
        /// <returns>0 = Unknown, 1 = Start, 2 = Succeed, 3 = In progress, 4 = Idle, 5 = Retry, 6 = Fail</returns>
        private int GetSnapshotAgentStatus(SqlConnection distributorConnection, ReplicationDistributor distributor, string publisherDatabase)
        {
            var queryId = String.Format(SnapShotAgentIdQuery, distributor.HubSpokeServerSetup.Hub, publisherDatabase);
            var infoId = ExecuteWithRetry<DataTable>(distributorConnection, queryId);
            if (infoId == null || infoId.Rows.Count == 0) return 0;
            var id = infoId.GetInt32Value("id");

            var queryHistory = String.Format(SnapShotAgentHistoryQuery + " AND timestamp = (SELECT max(timestamp) FROM mssnapshot_history WHERE agent_id = {0})", id);
            var infoHistory = ExecuteWithRetry<DataTable>(distributorConnection, queryHistory);
            if (infoHistory == null || infoHistory.Rows.Count == 0) return 0;
            return infoHistory.GetInt32Value("runstatus");
        }

        /// <summary>
        /// Indicates if the snapshot has been generated
        /// </summary>
        /// <param name="distributorConnection">Connection to distributor</param>
        /// <param name="distributor">Replication Distributor</param>
        /// <param name="publisherDatabase">Database for the hub</param>
        /// <returns>True if snapshot has been generated otherwise false</returns>
        private bool IsSnapshotGenerated(SqlConnection distributorConnection, ReplicationDistributor distributor, string publisherDatabase)
        {
            var queryId = String.Format(SnapShotAgentIdQuery, distributor.HubSpokeServerSetup.Hub, publisherDatabase);
            var infoId = ExecuteWithRetry<DataTable>(distributorConnection, queryId);
            if (infoId == null || infoId.Rows.Count == 0) return false;
            var id = infoId.GetInt32Value("id");

            var queryHistory = String.Format(SnapShotAgentHistoryQuery + " AND comments like '%100[%]] A snapshot of % article(s) was generated%'", id);
            var infoHistory = ExecuteWithRetry<DataTable>(distributorConnection, queryHistory);
            return infoHistory != null && infoHistory.Rows.Count != 0;
        }

        #endregion

        #region Monitoring

        /// <summary>
        /// Indicate if replication is completed or not
        /// </summary>
        /// <param name="distributor">Replication Distributor</param>
        /// <param name="publisher">Replication Publisher</param>
        /// <returns>If replication is complete</returns>
        private bool IsCompletedAllReplication(ReplicationDistributor distributor, ReplicationPublisher publisher)
        {
            using (var distributorConnection = new SqlConnection(distributor.ConnectionString))
            {
                // Check that snapshot is generated
                if (!IsSnapshotGenerated(distributorConnection, distributor, publisher.Database)) return true;

                // Ensure that replication is completed on subscribers
                if (publisher.SubscriberList == null) return true;
                if (publisher.SubscriberList.Any(s => GetPendingCommandCount(distributorConnection, publisher, s) != 0))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Get the pending commands that need to be executed for the subscriber to be replicated (usually each will take 1 sec to complete)
        /// </summary>
        /// <param name="distributorConnection">Connection to distributor</param>
        /// <param name="publisher">Replication Publisher</param>
        /// <param name="subscriber">Replication Subscriber</param>
        /// <returns>The pending commands count</returns>
        private int GetPendingCommandCount(SqlConnection distributorConnection, ReplicationPublisher publisher, ReplicationSubscriber subscriber)
        {
            var parameters = new Dictionary<string, object>
                                 {
                                     {"publisher", publisher.Server}, {"publisher_db", publisher.Database}, {"publication", publisher.Name},
                                     {"subscriber", subscriber.Server}, {"subscriber_db", subscriber.Database}, {"subscription_type", 0}
                                 };
            var commandInfo = ExecuteWithRetry<DataTable>(distributorConnection, "sp_replmonitorsubscriptionpendingcmds", parameters, () => new SqlCommand { Connection = distributorConnection, CommandType = CommandType.StoredProcedure, CommandTimeout = distributorConnection.ConnectionTimeout });
            if (commandInfo == null || commandInfo.Rows.Count == 0) throw new ApplicationException("Could Not Get Monitoring Information");
            return commandInfo.GetInt32Value("pendingcmdcount");
        }

        #endregion

        #region DatabaseSystemObject

        private List<DbSysObj> GetDatabaseSystemObjects(SqlConnection conn, IEnumerable<string> objectNameList)
        {
            var list = objectNameList.Select(o => "OBJECT_ID('{0}')".FormatWith(o)).Join(",");
            var query = String.Format(DatabaseSystemObjectQuery + "IN ({0})", list);
            var table = ExecuteWithRetry<DataTable>(conn, query);
            if (table == null || table.Rows.Count == 0) return null;
            return (from DataRow row in table.Rows select ParseDatabaseSystemObjectInfo(row)).ToList();
        }

        private DbSysObj GetDatabaseSystemObject(SqlConnection conn, string objectName)
        {
            var query = String.Format(DatabaseSystemObjectQuery + "= OBJECT_ID(N'{0}')", objectName);
            var objectInfo = ExecuteWithRetry<DataTable>(conn, query);
            if (objectInfo == null || objectInfo.Rows.Count == 0) return null;
            return ParseDatabaseSystemObjectInfo(objectInfo.Rows[0]);
        }

        private static DbSysObj ParseDatabaseSystemObjectInfo(DataRow row)
        {
            if (row == null) return null;
            return new DbSysObj
            {
                Name = row.GetStringValue("schemaname").Equals("dbo", StringComparison.OrdinalIgnoreCase) ? row.GetStringValue("objectname") : row.GetStringValue("schemaname") + "." + row.GetStringValue("objectname"),
                ObjectName = row.GetStringValue("objectname"),
                ObjectId = row.GetStringValue("object_id"),
                ParentObjectId = row.GetInt32Value("parent_object_id"),
                Type = row.GetStringValue("type").Trim().ToUpper(),
                TypeDesc = row.GetStringValue("type_desc"),
                CreateDate = row.GetDateTimeValue("create_date"),
                ModifyDate = row.GetDateTimeValue("modify_date"),
                IsMsShipped = row.GetBoolValue("is_ms_shipped"),
                IsPublished = row.GetBoolValue("is_published"),
                IsSchemaPublished = row.GetBoolValue("is_schema_published"),
                SchemaName = row.GetStringValue("schemaname"),
                SchemaId = row.GetInt32Value("schema_id"),
                IsIndexed = row.GetBoolValue("IsIndexed"),
                TableHasIdentity = row.GetBoolValue("TableHasIdentity"),
                IsSchemaBound = row.GetBoolValue("IsSchemaBound"),
                CurrentSeedValue = row.GetInt32Value("CurrentSeedValue")
            };
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Given a connection string substitute a new server and database
        /// </summary>
        /// <param name="connString">Source connection string</param>
        /// <param name="server">New server string</param>
        /// <param name="database">New database string</param>
        /// <returns>New connection string</returns>
        private string ConnStringSubstituteServerDatabase(string connString, string server, string database)
        {
            var newConnString = ConnStringSubstituteServer(connString, server);
            if (!String.IsNullOrEmpty(newConnString)) newConnString = ConnStringSubstituteDatabase(newConnString, database);
            return newConnString;
        }

        /// <summary>
        /// Given a connection string substitute a new server
        /// </summary>
        /// <param name="connString">Source connection string</param>
        /// <param name="server">New server string</param>
        /// <returns>New connection string</returns>
        private static string ConnStringSubstituteServer(string connString, string server)
        {
            var builder = new SqlConnectionStringBuilder(connString) { DataSource = server };
            return builder.ToString();
        }

        /// <summary>
        /// Given a connection string substitute a new database
        /// </summary>
        /// <param name="connString">Source connection string</param>
        /// <param name="database">New database string</param>
        /// <returns>New connection string</returns>
        private string ConnStringSubstituteDatabase(string connString, string database)
        {
            var builder = new SqlConnectionStringBuilder(connString) { InitialCatalog = database };
            return builder.ToString();
        }

        /// <summary>
        /// Given a connection string extract the server and database
        /// </summary>
        /// <param name="connString">Source connection string</param>
        /// <param name="server">Server taken from connection string</param>
        /// <param name="database">Database taken from connection string</param>
        private void ConnStringExtractServerDatabase(string connString, out string server, out string database)
        {
            server = ConnStringExtractServer(connString);
            database = ConnStringExtractDatabase(connString);
        }

        /// <summary>
        /// Given a connectin string extract the server
        /// </summary>
        /// <param name="connString">Source connection string</param>
        /// <returns>Server taken from connection string</returns>
        private static string ConnStringExtractServer(string connString)
        {
            var builder = new SqlConnectionStringBuilder(connString);
            return builder.DataSource;
        }

        /// <summary>
        /// Given a connectin string extract the database
        /// </summary>
        /// <param name="connString">Source connection string</param>
        /// <returns>Database taken from connection string</returns>
        private static string ConnStringExtractDatabase(string connString)
        {
            var builder = new SqlConnectionStringBuilder(connString);
            return builder.InitialCatalog;
        }

        private List<SeedMetadata> LoadSeedMetadataFile()
        {
            var filePath = GetSeedMetadataFilePath();
            if (!String.IsNullOrEmpty(filePath) && File.Exists(filePath))
            {
                return FromXml<List<SeedMetadata>>(Soaf.IO.IO.ReadAllText(filePath));
            }
            return null;
        }

        private void SaveSeedMetadataFile(List<SeedMetadata> seedMetadata)
        {
            CleanupSeedMetadata();
            var filePath = GetSeedMetadataFilePath();
            if (String.IsNullOrEmpty(filePath)) return;
            var xml = ToXml(seedMetadata);
            File.WriteAllText(filePath, xml);
        }

        private void CleanupSeedMetadata()
        {
            var filePath = GetSeedMetadataFilePath();
            var newPath = Path.Combine(Path.GetDirectoryName(filePath) ?? string.Empty, string.Format("SeedMetadata_{0}.xml", DateTime.Now.ToString("yyyy.MM.dd_HH.mm.ss")));
            if (!String.IsNullOrEmpty(filePath) && File.Exists(filePath)) File.Move(filePath, newPath);
        }

        private string GetSeedMetadataFilePath()
        {
            var fullPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (String.IsNullOrEmpty(fullPath)) return null;
            var filePath = Path.Combine(fullPath, "SeedMetadata.xml");
            return filePath;
        }

        private static string ToXml(object value)
        {
            var serializer = new XmlSerializer(value.GetType());
            using (var ms = new MemoryStream())
            {
                serializer.Serialize(ms, value);
                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        private static T FromXml<T>(string s)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(s)))
            {
                return (T)serializer.Deserialize(ms);
            }
        }

        #endregion

        #region Validators

        private bool IsValidDeleteArguments(SqlReplicationArguments arguments)
        {
            if (arguments == null) return false;
            if (String.IsNullOrWhiteSpace(arguments.HubConnectionString)) return false;
            // MUST have ReplicatedObjects or ReplicationPartnerConnectionStrings (or both)
            return (arguments.ReplicatedObjects != null && arguments.ReplicatedObjects.Any()) || (arguments.SpokeConnectionStrings != null && arguments.SpokeConnectionStrings.Any());
        }

        private void ConfirmValidRequestedDelete(SqlReplicationArguments arguments, ReplicationPublisher publisher)
        {
            if (publisher == null) throw new ApplicationException("Nothing To Delete In Replication. No Publisher Found");
            if (arguments.ReplicatedObjects != null && publisher.ArticleList == null) throw new ApplicationException("Nothing To Delete In Replication. No Objects Found");
            if (arguments.SpokeConnectionStrings != null && publisher.SubscriberList == null) throw new ApplicationException("Nothing To Delete In Replication. No Partners Found");

            // If there are ReplicatedObjects to be deleted then make sure they are part of the existing replication world articles
            if (arguments.ReplicatedObjects != null && !AreReplicationItems(arguments.ReplicatedObjects, publisher.ArticleList.Select(a => a.Name))) throw new ApplicationException("ReplicatedObjects Are Not Part Of Replication");

            // If there are ReplicationPartnerConnectionStrings to be deleted then make sure they are part of the existing replication world subscribers
            if (arguments.SpokeConnectionStrings != null && !AreReplicationPartnersItems(arguments.SpokeConnectionStrings, publisher.SubscriberList)) throw new ApplicationException("ReplicationPartnerConnectionStrings Are Not Part Of Replication");
        }

        private bool AreReplicationItems(IEnumerable<string> items, IEnumerable<string> replicatedItems)
        {
            return items.All(i => replicatedItems.Contains(i, StringComparer.OrdinalIgnoreCase));
        }

        private bool AreReplicationPartnersItems(IEnumerable<string> partnersConnStrings, List<ReplicationSubscriber> subscriberList)
        {
            foreach (var connString in partnersConnStrings)
            {
                string partnerServer;
                string partnerDatabase;
                ConnStringExtractServerDatabase(connString, out partnerServer, out partnerDatabase);
                if (partnerServer == null || partnerDatabase == null) throw new ArgumentException("Replication Partner Connection String Is Invalid");

                ReplicationSubscriber subscriber = subscriberList.FirstOrDefault(s => s.Server.Equals(partnerServer, StringComparison.OrdinalIgnoreCase) && s.Database.Equals(partnerDatabase, StringComparison.OrdinalIgnoreCase));
                if (subscriber == null) return false;
            }
            return true;
        }

        private bool IsValidConfigureArguments(SqlReplicationArguments arguments)
        {
            if (arguments == null) return false;
            if (String.IsNullOrWhiteSpace(arguments.HubConnectionString)) return false;
            // MUST have DefaultReplicatedObjects or ReplicatedObjects or ReplicationPartnerConnectionStrings (or two or all)
            return (SqlReplicationArguments.DefaultReplicatedObjects != null && SqlReplicationArguments.DefaultReplicatedObjects.Any())
                   || (arguments.ReplicatedObjects != null && arguments.ReplicatedObjects.Any())
                   || (arguments.SpokeConnectionStrings != null && arguments.SpokeConnectionStrings.Any());
        }

        private bool IsValidSeeding(Seeding seeding)
        {
            if (seeding == null) return true;
            if (seeding.ServerStepRange == 0) return false;
            if (seeding.SeedingOverrides != null && seeding.SeedingOverrides.Any(so => String.IsNullOrWhiteSpace(so.Name) || so.StepRange == 0)) return false;
            return true;
        }

        private bool IsValidDistributor(ReplicationDistributor distributor)
        {
            return (distributor != null && distributor.IsInstalled && distributor.IsDbInstalled && (distributor.IsPublisher || distributor.HasRemotePublisher) && !String.IsNullOrEmpty(distributor.Server) && !String.IsNullOrEmpty(distributor.ConnectionString));
        }

        #endregion

        #region Nested type: DbSysObj

        /// <summary>
        /// Wrapping the underlining metadata of a database system item 
        /// </summary>
        private class DbSysObj
        {
            public string Name { get; set; }
            public string ObjectName { get; set; }
            public string ObjectId { get; set; }
            public int ParentObjectId { get; set; }
            public string Type { get; set; }
            public string TypeDesc { get; set; }
            public DateTime CreateDate { get; set; }
            public DateTime ModifyDate { get; set; }
            public bool IsMsShipped { get; set; }
            public bool IsPublished { get; set; }
            public bool IsSchemaPublished { get; set; }
            public string SchemaName { get; set; }
            public int SchemaId { get; set; }
            public bool IsIndexed { get; set; }
            public bool TableHasIdentity { get; set; }
            public bool IsSchemaBound { get; set; }
            public int CurrentSeedValue { get; set; }
        }

        #endregion

        #region Nested type: SeedMetadata

        /// <summary>
        /// Wrapping the seeding metadata to be applied at database
        /// </summary>
        public class SeedMetadata
        {
            public Seeding ReferrerSeeding { get; set; }
            public string ConnectionString { get; set; } // Connection to server and database (external datapoint)
            public string Server { get; set; }
            public string Database { get; set; }
            public int ServerIndex { get; set; }
            public List<SeedItemMetadata> SeedList { get; set; }
        }

        public class SeedItemMetadata
        {
            public string Name { get; set; }
            public long Value { get; set; }
        }

        #endregion

        #region Nested type: HubSpokeServer

        /// <summary>
        /// Contains the hub and spoke server information from the distributor perspective 
        /// </summary>
        private class HubSpokeServer
        {
            public string Hub { get; set; }
            public List<string> Spoke { get; set; }
        }

        #endregion

        #region Nested type: ReplicationDistributor

        /// <summary>
        /// Contains the distributor information
        /// </summary>
        private class ReplicationDistributor
        {
            public string ConnectionString { get; set; } // Connection string to hub server and distribution database (internally determined)
            public bool IsInstalled { get; set; }
            public string Server { get; set; }
            public bool IsDbInstalled { get; set; }
            public bool IsPublisher { get; set; }
            public bool HasRemotePublisher { get; set; }
            public HubSpokeServer HubSpokeServerSetup { get; set; }
        }

        #endregion

        #region Nested type: ReplicationPublisher

        /// <summary>
        /// Contains the publisher information including the list of replicated articles and subscribers
        /// </summary>
        private class ReplicationPublisher
        {
            public string ConnectionString { get; set; } // Connection string to hub server and database (internally determined)
            public string Name { get; set; }
            public string Server { get; set; }
            public string Database { get; set; }
            public List<ReplicationArticle> ArticleList { get; set; }
            public List<ReplicationSubscriber> SubscriberList { get; set; }
        }

        #endregion

        #region Nested type: ReplicationArticle

        /// <summary>
        /// Contains an article information and the underlining database system object metadata
        /// </summary>
        private class ReplicationArticle : IEquatable<ReplicationArticle>
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string SourceOwner { get; set; }
            public string SourceObject { get; set; }
            public string DestinationObject { get; set; }
            public DbSysObj DatabaseSystemObject { get; set; }

            #region IEquatable<ReplicationArticle> Members

            public bool Equals(ReplicationArticle other)
            {
                return other != null && Name.Equals(other.Name, StringComparison.OrdinalIgnoreCase);
            }

            #endregion

            public override int GetHashCode()
            {
                return Name.GetHashCode();
            }

            public override bool Equals(Object obj)
            {
                var other = obj as ReplicationArticle;
                return other != null && Equals(other);
            }
        }

        #endregion

        #region Nested type: ReplicationSubscriber

        /// <summary>
        /// Contains a subscriber information and the underlining metadata of the database system objects that are defined for replication
        /// </summary>
        private class ReplicationSubscriber : IEquatable<ReplicationSubscriber>
        {
            public string ConnectionString { get; set; } // Connection string to spoke server server and database (internally determined)
            public string Server { get; set; }
            public string Database { get; set; }
            public List<DbSysObj> DatabaseSystemObjects { get; set; }

            #region IEquatable<ReplicationSubscriber> Members

            public bool Equals(ReplicationSubscriber other)
            {
                return other != null && (Server.Equals(other.Server, StringComparison.OrdinalIgnoreCase) && Database.Equals(other.Database, StringComparison.OrdinalIgnoreCase));
            }

            #endregion

            public override int GetHashCode()
            {
                return Server.GetHashCode() ^ Database.GetHashCode();
            }

            public override bool Equals(Object obj)
            {
                var other = obj as ReplicationSubscriber;
                return other != null && Equals(other);
            }
        }

        #endregion
    }

    /// <summary>
    /// Information about a replication error.
    /// </summary>
    internal class ReplicationException : Exception
    {
        public ReplicationException(string message, Exception innerException = null)
            : base(message, innerException)
        {
        }
    }

}