﻿using IO.Practiceware.Configuration;
using IO.Practiceware.Data;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Data;
using Soaf.EntityFramework;
using System;
using System.Data;

[assembly: Component(typeof(DbConnectionFactory.ConnectionStringObjectContextFactory), typeof(ConnectionStringObjectContextFactory), Priority = 100)]
[assembly: Component(typeof(DbConnectionFactory.ConnectionStringRepository), typeof(IConnectionStringRepository), Priority = 100)]

namespace IO.Practiceware.Data
{
    /// <summary>
    /// Provides instances of DbConnections
    /// </summary>
    public static class DbConnectionFactory
    {
        public static readonly TimeSpan MaximumTimeout = TimeSpan.FromSeconds(0);

        /// <summary>
        /// Gets the practice repository db connection.
        /// </summary>
        public static IDbConnection PracticeRepository
        {
            get { return new AdoServiceConnection { ConnectionString = ConfigurationManager.PracticeRepositoryConnectionString }; }
        }

        /// <summary>
        /// Ensures Entity Framework always uses the current PracticeRepositoryConnectionString.
        /// </summary>
        public class ConnectionStringObjectContextFactory : Soaf.EntityFramework.ConnectionStringObjectContextFactory
        {
            public override string ProviderConnectionString
            {
                get
                {
                    if (ConnectionStringName == ConfigurationManager.DefaultPracticeRepositoryConnectionStringKey)
                    {
                        return ConfigurationManager.PracticeRepositoryConnectionString;
                    }
                    return base.ProviderConnectionString;
                }
            }
        }

        /// <summary>
        /// Ensures all services always use the current PracticeRepositoryConnectionString.
        /// </summary>
        public class ConnectionStringRepository : ConfigurationContainerConnectionStringRepository
        {
            public ConnectionStringRepository(IConfigurationContainer configurationContainer)
                : base(configurationContainer)
            {
            }

            public override ConnectionStringConfiguration this[string name]
            {
                get
                {

                    if (name.Equals(ConfigurationManager.DefaultPracticeRepositoryConnectionStringKey, StringComparison.OrdinalIgnoreCase))
                    {
                        name = ConfigurationManager.PracticeRepositoryConnectionStringKey;
                    }
                    return base[name];
                }
                set
                {
                    base[name] = value;
                }
            }
        }
    }
}
