﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Soaf.ComponentModel;

namespace IO.Practiceware.Validation
{

    /// <summary>
    /// Will determine if the attributed property is valid or not by first checking a user-defined dependant property for true, then the actual value.
    /// e.g. if the attributed property is empty (which would normally fail validation) but the property passed in is false, then validation does not proceed,
    /// or rather a default of validation.success is returned.
    /// </summary>
    public class RequiredIfAttribute : RequiredWithDisplayNameAttribute, INotifyPropertyChangedAttribute
    {
        public IDictionary<string, bool> PropertyNames { get; private set; }

        public LogicalOperator LogicalOperator { get; set; }

        /// <summary>
        /// Initializes the RequiredIf attribute with the name of the property to evaluate to determine if validation should proceed.
        /// </summary>
        /// <param name="propertyName">A boolean property which determines whether or not to apply validation against the attributed property.</param>
        public RequiredIfAttribute(string propertyName)
            : this(LogicalOperator.All, new[] { propertyName })
        {}

        /// <summary>
        /// Initializes the RequiredIf attribute with a specified operator ('All', 'Any') and the names of the properties to evaluate to determine 
        /// if validation should proceed.
        /// </summary>
        /// <param name="logicalOperator">Determines which conditional operator to apply. e.g. if LogicalOperator.All is supplied, then all properties must be true to proceed with validation.</param>
        /// <param name="propertyNames">A list of property names (representing boolean values) which determine whether or not to apply validation.</param>
        public RequiredIfAttribute(LogicalOperator logicalOperator, params string[] propertyNames)
        {
            PropertyNames = new Dictionary<string, bool>();
            foreach (var propertyName in propertyNames)
            {
                PropertyNames.Add(GetParsedPropertyName(propertyName));
            }
            LogicalOperator = logicalOperator;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var isPropertyTrue = new Func<KeyValuePair<string, bool>, bool>(prop =>
                                  {
                                      var property = validationContext.ObjectInstance
                                          .GetType()
                                          .GetProperty(prop.Key, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                                      if (property == null)
                                      {
                                          throw new ArgumentException("Property does not exist.", prop.Key);
                                      }

                                      var propertyValue = property.GetValue(validationContext.ObjectInstance, null);
                                      if (!(propertyValue is bool))
                                      {
                                          throw new ArgumentException("Property cannot be evaluated to a boolean logical value.", prop.Key);
                                      }

                                      var boolPropertyValue = (bool)propertyValue;
                                      return boolPropertyValue ^ prop.Value;
                                  })
            ;

            // based on the logical operator, check all or any property for truthfulness to determine if we should validate
            if ((LogicalOperator == LogicalOperator.All && PropertyNames.All(isPropertyTrue))
                || (LogicalOperator == LogicalOperator.Any && PropertyNames.Any(isPropertyTrue)))
            {

                return base.IsValid(value, validationContext);
            }

            return ValidationResult.Success;
        }

        private static KeyValuePair<string, bool> GetParsedPropertyName(string propertyName)
        {
            var actualPropertyName = propertyName.Trim();
            var negateProperty = false;
            if (actualPropertyName.First() == '!')
            {
                negateProperty = true;
                actualPropertyName = actualPropertyName.Substring(1);
            }

            return new KeyValuePair<string, bool>(actualPropertyName, negateProperty);
        }

        #region INotifyPropertyChangedAttribute
        public string[] GetDependencies()
        {
            return PropertyNames.Keys.ToArray();
        }
        #endregion
    }
}

