﻿using Soaf;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using Soaf.ComponentModel;

namespace IO.Practiceware.Validation
{
    public class RegexMatchAttribute : RegularExpressionAttribute
    {
        private readonly string _regex;

        public RegexMatchAttribute(string regex) : base(regex)
        {
            _regex = regex;
        }

        public RegexMatchAttribute(string regex, string errorMessage)
            : base(regex)
        {
            _regex = regex;
            ErrorMessage = errorMessage;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var errorMessage = this.GetErrorMessageOrDefault("{0} does not match Regex {1}".FormatWith(value, _regex));
            if (SuccessOnNullOrEmptyInput)
            {
                if (base.IsValid(value)) return ValidationResult.Success;

                return new ValidationResult(errorMessage,new[] {validationContext.MemberName});
            }

            if (value == null)
            {
                return new ValidationResult(errorMessage, new[] {validationContext.MemberName});
            }
            return Regex.IsMatch(value.ToString(), _regex) ? ValidationResult.Success : new ValidationResult(errorMessage, new[] {validationContext.MemberName});
        }

        public bool SuccessOnNullOrEmptyInput
        {
            get;
            set;
        }
    }
}
