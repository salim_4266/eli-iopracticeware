﻿using Soaf.Collections;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Validation
{
    public class NotEmptyAttribute : ValidationAttribute
    {
        public NotEmptyAttribute() : base("Value missing")
        {
            
        }
        public override bool IsValid(object value)
        {
            return ((IEnumerable) value).IsNotNullOrEmpty();
        }
    }
}
