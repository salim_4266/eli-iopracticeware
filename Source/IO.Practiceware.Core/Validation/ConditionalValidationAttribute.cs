﻿using IO.Practiceware.Validation;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Reflection;
using System;
using System.ComponentModel.DataAnnotations;

[assembly: Component(typeof(ConditionalAttribute), Initialize = true)]

namespace IO.Practiceware.Validation
{
    /// <summary>
    /// Validates using the given ValidationType only if the 'Condition' passes.  Otherwise, returns ValidationResult.Success.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple=true)]
    public class ConditionalAttribute : ValidationAttribute
    {
        private readonly object _typeId = new object();
        private static IEvaluator _evaluator;

        /// <summary>
        /// Gets or sets the condition expression to be evaluated
        /// </summary>
        public string ConditionExpression { get; set; }

        /// <summary>
        /// Gets or sets the parameters for the given ValidationType.
        /// </summary>
        public object[] ValidationParameters { get; set; }

        /// <summary>
        /// Gets or sets the type of ValidationAttribute.
        /// </summary>
        public Type ValidationType { get; set; }

        /// <summary>
        /// Gets the TypeId.  This is needed to uniquely identify multiple instances of the same attribute.
        /// </summary>
        public override object TypeId
        {
            get
            {
                return _typeId;
            }
        }

        /// <summary>
        /// Gets or sets the ValidationAttribute.
        /// </summary>
        /// <remarks>
        /// Dynamically created by the given ValidationType and ValidationParameters
        /// </remarks>
        private ValidationAttribute ValidationAttribute { get; set; }

        public ConditionalAttribute() { }

        public ConditionalAttribute(IEvaluator evaluator) : this()
        {
            _evaluator = evaluator;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (ValidationAttribute == null) { ValidationAttribute = (ValidationAttribute)Activator.CreateInstance(ValidationType, ValidationParameters); }

            object conditionResult = _evaluator.Evaluate(ConditionExpression, validationContext.ObjectInstance);
            if (conditionResult is bool && (bool)conditionResult)
            {
                var result = ValidationAttribute.GetValidationResult(value, validationContext);
                if (result != null)
                {
                    return ErrorMessage.IsNullOrEmpty() ? result : new ValidationResult(ErrorMessage, result.MemberNames);
                }
            }

            return ValidationResult.Success;
        }
    }
}
