﻿using System;
using System.ComponentModel.DataAnnotations;
using Soaf.ComponentModel;

namespace IO.Practiceware.Validation
{
    public class EqualToGreaterThanAttribute : ValidationAttribute
    {
        public string PropertyName { get; set; }

        public EqualToGreaterThanAttribute(string propertyName)
        {
            PropertyName = propertyName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var property = validationContext.ObjectInstance.GetType().GetProperty(PropertyName);
            if (property == null)
            {
                throw new ArgumentException("Property {0} does not exist", PropertyName);
            }

            var propertyValue = property.GetValue(validationContext.ObjectInstance, null);
            if (propertyValue != null && !(propertyValue is DateTime) && !(propertyValue is TimeSpan))
            {
                throw new ArgumentException("Property {0} is not Date-based or Time-based", PropertyName);
            }

            if (value != null && propertyValue != null && propertyValue is DateTime)
            {
                var valueDateTime = (DateTime) value;
                var propertyValueDateTime = (DateTime) propertyValue;
                if (DateTime.Compare(propertyValueDateTime, valueDateTime) > 0)
                {
                    var errorMessage = this.GetErrorMessageOrDefault("Value cannot be greater");
                    return new ValidationResult(errorMessage, new[] {validationContext.MemberName});
                }
            }

            if (value != null && propertyValue != null && propertyValue is TimeSpan)
            {
                var valueDateTime = (TimeSpan) value;
                var propertyValueDateTime = (TimeSpan) propertyValue;
                if (TimeSpan.Compare(propertyValueDateTime, valueDateTime) > 0)
                {
                    var errorMessage = this.GetErrorMessageOrDefault("Value cannot be greater");
                    return new ValidationResult(errorMessage, new[] {validationContext.MemberName});
                }
            }

            return ValidationResult.Success;
        }
    }
}
