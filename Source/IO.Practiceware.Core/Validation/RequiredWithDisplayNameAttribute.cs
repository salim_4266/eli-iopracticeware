﻿using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Soaf;
using Soaf.Collections;
using Soaf.Reflection;

namespace IO.Practiceware.Validation
{
    /// <summary>
    ///     A RequiredAttribute that pretty-formats the member name.
    /// </summary>
    public class RequiredWithDisplayNameAttribute : RequiredAttribute
    {
        private MemberInfo _memberInfo;
        private string _displayName;

        /// <summary>
        /// Gets or sets a value indicating whether to allow default values for value types. For example, if this is false, 0 will be an invalid value for an int.
        /// </summary>
        /// <value>
        /// <c>true</c> if [allow default values for value types]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowDefaultValuesForValueTypes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether empty collections are considered valid. The default is true.
        /// </summary>
        public bool AllowEmptyCollections { get; set; }

        public RequiredWithDisplayNameAttribute()
        {
            AllowEmptyCollections = true;
        }

        public RequiredWithDisplayNameAttribute(bool allowEmptyCollections, string errorMessage)
        {
            AllowEmptyCollections = allowEmptyCollections;
            ErrorMessage = errorMessage;
        }

        public override string FormatErrorMessage(string name)
        {
            if (_displayName == null)
            {
                if (_memberInfo != null &&
                    (_memberInfo.HasAttribute<DescriptionAttribute>() || _memberInfo.HasAttribute<DisplayAttribute>()))
                {
                    _displayName = Strings.FormatAsDisplayName(_memberInfo.GetDisplayName(), false);
                }
                else
                {
                    _displayName = Strings.FormatAsDisplayName(name);
                }
            }

            return base.FormatErrorMessage(_displayName);
        }

        public override bool IsValid(object value)
        {
            if (!AllowDefaultValuesForValueTypes && value != null &&
                // is default value for value type
                value.GetType().IsValueType && Activator.CreateInstance(value.GetType()).Equals(value))
            {
                return false;
            }

            // If we are NOT allowing an EMPTY collection to be a VALID value, then we fail validation
            if (!AllowEmptyCollections)
            {
                var collectionValue = value as IEnumerable;

                if (collectionValue != null && collectionValue.ToInferredElementTypeArray().Length <= 0)
                {
                    return false;
                }
            }
            return base.IsValid(value);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var isValidationEnabled = validationContext.ObjectInstance.As<IIsValidationEnabled>().IfNotDefault(v => v.IsValidationEnabled, true);

            if (!isValidationEnabled) return ValidationResult.Success;

            if (_memberInfo == null)
                _memberInfo = validationContext.ObjectType.GetMember(validationContext.MemberName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).FirstOrDefault();

            return base.IsValid(value, validationContext);
        }
    }
}