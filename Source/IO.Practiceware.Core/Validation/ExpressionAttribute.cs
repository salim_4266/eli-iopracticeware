﻿using IO.Practiceware.Validation;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Reflection;
using System;
using System.ComponentModel.DataAnnotations;

[assembly: Component(typeof (ExpressionAttribute), Initialize = true)]

namespace IO.Practiceware.Validation
{
    public class ExpressionAttribute : ValidationAttribute
    {
        private bool _hasRegisteredExpressionTypes;
        private static IEvaluator _evaluator;

        public ExpressionAttribute(IEvaluator evaluator)
        {
            _evaluator = evaluator;
        }

        public ExpressionAttribute(string exression)
        {
            Expression = exression;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (Expression == null) throw new InvalidOperationException();
            RegisterExpressionTypes();

            object result = _evaluator.Evaluate(Expression, validationContext.ObjectInstance);

            if (result is bool && !((bool) result))
            {
                var errorMessage = GetErrorMessage(validationContext.ObjectInstance);
                return new ValidationResult(errorMessage, new[] {validationContext.MemberName});
            }

            return ValidationResult.Success;
        }

        private void RegisterExpressionTypes()
        {
            if (!_hasRegisteredExpressionTypes && RegisteredExpressionTypes != null)
            {
                RegisteredExpressionTypes.ForEachWithSinglePropertyChangedNotification(t => _evaluator.RegisterType(t));
                _hasRegisteredExpressionTypes = true;
            }
        }

        private string GetErrorMessage(object targetInstance)
        {
            if (ErrorMessage != null)
            {
                return ErrorMessage;
            }
            if (ErrorMessageExpression != null)
            {
                object result = _evaluator.Evaluate(ErrorMessageExpression, targetInstance);
                var errorMessage = result as string;
                if (errorMessage == null) throw new Exception("Could not evaluate ErrorMessageExpression, {0}".FormatWith(ErrorMessageExpression));

                return errorMessage;
            }

            var defaultErrorMessage = "{0} returned false.".FormatWith(Expression);
            return defaultErrorMessage;
        }

        public string Expression { get; private set; }
        public string ErrorMessageExpression { get; set; }
        public Type[] RegisteredExpressionTypes { get; set; }
    }
}