﻿namespace IO.Practiceware.Validation
{
    /// <summary>
    /// Interface to enable/disable a Validation
    /// </summary>
    public interface IIsValidationEnabled
    {
        bool IsValidationEnabled { get; }
    }
}
