﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using IO.Practiceware.Model;
using Soaf;
using Soaf.Reflection;

namespace IO.Practiceware.Validation
{
    /// <summary>
    /// A custom RequiredIf attribute that looks for an IsPropertyRequired boolean property on the applied instance and 
    /// is enabled or disabled based on its value. 
    /// </summary>
    public class RequiredIfIsRequiredAttribute : RequiredWithDisplayNameAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // If a property has IsPropertyNameRequired property then 
            // IsPropertyNameRequired property takes high priority for validation than IHasPropertyConfigurations
            // even though when we have property configuration setup for the same property
            var property = validationContext.ObjectType.GetProperty("Is{0}Required".FormatWith(validationContext.MemberName), BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            if (property != null)
            {
                // has IsPropertyNameRequired property
                var isRequired = property.GetGetMethod().GetInvoker()(validationContext.ObjectInstance);
                if (isRequired != null && (bool)isRequired)
                {
                    return base.IsValid(value, validationContext);
                }
                return ValidationResult.Success;
            }

            var hasPropertyConfigurations = validationContext.ObjectInstance as IHasPropertyConfigurations;
            if (hasPropertyConfigurations != null)
            {
                // find the configuration for this property
                var propertyConfiguration = hasPropertyConfigurations.PropertyConfigurations.IfNotNull(s => s.FirstOrDefault(c => c.PropertyName == validationContext.MemberName));

                if (propertyConfiguration != null && propertyConfiguration.IsRequired)
                {
                    // validate based on corresponding property configuration if it exists
                    return base.IsValid(value, validationContext);
                }
            }
            
            return ValidationResult.Success;
        }
    }
}
