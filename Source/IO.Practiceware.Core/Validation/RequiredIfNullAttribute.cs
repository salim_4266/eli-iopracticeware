﻿using Soaf;
using Soaf.Collections;
using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Soaf.ComponentModel;

namespace IO.Practiceware.Validation
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RequiredIfNotNullAttribute : RequiredIfNullAttributeBase
    {
        public RequiredIfNotNullAttribute(string propertyToCompare) 
            : this(new [] {propertyToCompare})
        {}

        public RequiredIfNotNullAttribute(params string[] propertiesToCompare)
            : base(propertiesToCompare)
        {}

        protected override bool CheckValue(object compareValue, PropertyInfo sourceProperty, ValidationContext validationContext)
        {
            if (compareValue == null) return true;

            if (!sourceProperty.PropertyType.CanHaveNullValue())
            {
                throw new ArgumentException("{0} will always be required because Property {1} is not a nullable type."
                    .FormatWith(validationContext.MemberName, sourceProperty.Name));
            }
            return false;
        }

        protected override string DefaultErrorMessage
        {
            get { return "{0} is required because {1} is set"; }
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class RequiredIfNullAttribute : RequiredIfNullAttributeBase
    {
        public RequiredIfNullAttribute(string propertyToCompare) 
            : this(new[] { propertyToCompare }) 
        { }

        public RequiredIfNullAttribute(params string[] propertiesToCompare)
            :base(propertiesToCompare)
        {}

        protected override bool CheckValue(object compareValue, PropertyInfo sourceProperty, ValidationContext validationContext)
        {
            if (compareValue == null) return false;

            if (!sourceProperty.DeclaringType.IsNullableType())
            {
                throw new ArgumentException("{0} will never be required because Property {1} is not a nullable type."
                    .FormatWith(validationContext.MemberName, sourceProperty.Name));
            }
            return true;
        }

        protected override string DefaultErrorMessage
        {
            get { return "{0} is required because {1} is not set"; }
        }
    }

    public abstract class RequiredIfNullAttributeBase : ValidationAttribute, INotifyPropertyChangedAttribute
    {
        protected RequiredIfNullAttributeBase(params string[] propertiesToCompare)
        {
            AllowEmptyStrings = true;
            PropertiesToCompare = propertiesToCompare;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                if (!(value is string) || AllowEmptyStrings || !string.IsNullOrEmpty((string)value))
                {
                    return ValidationResult.Success;
                }
            }

            foreach (var propertyToCompare in PropertiesToCompare)
            {
                var propertyInfo = validationContext.ObjectInstance.GetType().GetProperty(propertyToCompare);
                if (propertyInfo == null)
                {
                    throw new ArgumentException("Property {0} does not exist", propertyToCompare);
                }
                var compareValue = propertyInfo.GetValue(validationContext.ObjectInstance, null);
                if (CheckValue(compareValue, propertyInfo, validationContext))
                {
                    return ValidationResult.Success;
                }
            }

            var errorMessage = this.GetErrorMessageOrDefault(DefaultErrorMessage
                .FormatWith(validationContext.MemberName, PropertiesToCompare.Join()));
            return new ValidationResult(errorMessage, new[] {validationContext.MemberName});
        }

        protected abstract bool CheckValue(object compareValue, PropertyInfo sourceProperty, ValidationContext validationContext);

        private string[] PropertiesToCompare { get; set; }

        protected abstract string DefaultErrorMessage { get; }

        public bool AllowEmptyStrings { get; set; }

        #region INotifyPropertyChangedAttribute
        public string[] GetDependencies()
        {
            return PropertiesToCompare;
        }
        #endregion
    }
}
