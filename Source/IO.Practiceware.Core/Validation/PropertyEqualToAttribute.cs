﻿using Soaf;
using System;
using System.ComponentModel.DataAnnotations;
using Soaf.ComponentModel;

namespace IO.Practiceware.Validation
{
    /// <summary>
    /// Validates whether the given property is equal to the 'PropertyValue'.
    /// </summary>
    public class PropertyEqualToAttribute : ValidationAttribute
    {
        public string PropertyName { get; set; }
        public object PropertyValue { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var property = validationContext.ObjectInstance.GetType().GetProperty(PropertyName);
            if (property == null)
            {
                throw new ArgumentException("Property {0} does not exist", PropertyName);
            }

            var propertyValue = property.GetValue(validationContext.ObjectInstance, null);
            var errorMessage = this.GetErrorMessageOrDefault("Property {0} with value {1} does not equal {2}".FormatWith(PropertyName, propertyValue, PropertyValue));

            if (propertyValue == null)
            {
                return PropertyValue == null ? ValidationResult.Success : new ValidationResult(errorMessage, new[] { validationContext.MemberName });
            }

            return propertyValue.Equals(PropertyValue) ? ValidationResult.Success : new ValidationResult(errorMessage, new[] { validationContext.MemberName });
        }
    }
}
