﻿using Soaf;
using System;
using System.ComponentModel.DataAnnotations;
using Soaf.ComponentModel;

namespace IO.Practiceware.Validation
{
    [AttributeUsage(AttributeTargets.Property)]
    public class EqualToAttribute : ValidationAttribute
    {
        private readonly object _equalToValue;

        public EqualToAttribute(object equalToValue)
        {
            _equalToValue = equalToValue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string errorMessage;
            if (value == null)
            {
                if (_equalToValue == null)
                {
                    return ValidationResult.Success;
                }

                errorMessage = this.GetErrorMessageOrDefault("{0} and {1} are not equal".FormatWith("null", _equalToValue));
                return new ValidationResult(errorMessage, new[] { validationContext.MemberName });
            }
            if(value.Equals(_equalToValue))
            {
                return ValidationResult.Success;
            }

            errorMessage = this.GetErrorMessageOrDefault("{0} and {1} are not equal".FormatWith(value, _equalToValue));
            return new ValidationResult(errorMessage, new[] { validationContext.MemberName });
        }
    }
}
