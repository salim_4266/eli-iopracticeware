using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Logging;
using Soaf;
using Soaf.Collections;
using Soaf.Configuration;
using Soaf.Logging;
using Soaf.Reflection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Soaf.Web.Client;

namespace IO.Practiceware
{
    public class Bootstrapper
    {
        internal static IEnumerable<Assembly> BootstrappedAssemblies = new[]
            {
                typeof(Bootstrapper).Assembly, 
                TryLoadAssembly("IO.Practiceware.Model"), 
                TryLoadAssembly("IO.Practiceware.Presentation"), 
                TryLoadAssembly("IO.Practiceware.Integration.MVE"),
                TryLoadAssembly("IO.Practiceware.Monitoring.Service")
            }
            .WhereNotDefault()
            .Distinct()
            .ToArray();

        private static readonly object SyncRoot = new object();

        public static bool HasRun { get; private set; }

        private static IServiceProvider _serviceProvider;

        static Bootstrapper()
        {
            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;
            ForceExplicitReferences();
        }

        /// <summary>
        /// .NET will not copy local assemblies that don't have explicit references. Make explicit references.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        private static void ForceExplicitReferences()
        {
            typeof (System.Web.Razor.RazorEngineHost).EnsureNotDefault();
        }

        /// <summary>
        /// This is necessary for client installs, where the assemblies are in the GAC and .NET requires a fully qualified name (including version and public key token) in order to
        /// find them. Our config files only use partial names for configuration section types.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            var partialName = args.Name.Split(',').FirstOrDefault() ?? string.Empty;
            if (partialName.StartsWith("IO.Practiceware", StringComparison.OrdinalIgnoreCase))
            {
                var asm = BootstrappedAssemblies.FirstOrDefault(a => a.FullName.Split(',').First().Equals(partialName, StringComparison.OrdinalIgnoreCase));
                return asm;
            }
            if (partialName.Equals("Soaf", StringComparison.OrdinalIgnoreCase))
            {
                return typeof(Soaf.Bootstrapper).Assembly;
            }
            return null;
        }

        [DebuggerNonUserCode]
        internal static Assembly TryLoadAssembly(string name)
        {
            try
            {
                Assembly assembly = Assembly.Load("{0}, {1}".FormatWith(name, typeof(Bootstrapper).Assembly.FullName.Split(',').Skip(1).Join(",")));
                return assembly;
            }
            catch
            {
                return null;
            }
        }

        public static T Run<T>(string configurationPath = null)
        {
            lock (SyncRoot)
            {
                if (!HasRun)
                {
                    TryEnableMultiCoreJit(configurationPath ?? AppDomain.CurrentDomain.BaseDirectory);

                    string time = null;
                    try
                    {
                        using (new TimedScope(s => time = s))
                        {
                            if (!ConfigurationManager.IsLoaded) ConfigurationManager.Load(configurationPath);

                            _serviceProvider = Soaf.Bootstrapper.Run<IServiceProvider>(new AssemblyRepository(BootstrappedAssemblies), new StringXmlConfiguration(string.Empty));

							ServiceModel.Initialize();
                            ConfigurationManager.Initialize();
                            UserContext.Initialize(_serviceProvider.GetService<Func<UserContext>>());

                            Evaluator.Current.RegisterType(typeof(UserContext));
                            Evaluator.Current.RegisterType(typeof(ConfigurationManager));

                            HasRun = true;

                            return (T)_serviceProvider.GetService(typeof(T));
                        }
                    }
                    catch (Exception ex)
                    {
                        if (!Trace.Listeners.OfType<LogEntryEventLogTraceListener>().Any()) Trace.Listeners.Add(LoggingConfigurationManager.EventLogTraceListener);
                        Trace.TraceError("Error initializating application: {0}".FormatWith(ex.ToString()));
                        throw;
                    }
                    finally
                    {
                        Debug.WriteLine("Ran IO Practiceware Bootstraper in {0}.".FormatWith(time));
                    }
                }
            }
            return (T)_serviceProvider.GetService(typeof(T));
        }

        /// <summary>
        /// Tries to enable multi core JIT using reflection (since we don't specifically target .NET 4.5 yet).
        /// http://blogs.msdn.com/b/dotnet/archive/2012/10/18/an-easy-solution-for-improving-app-launch-performance.aspx
        /// </summary>
        /// <param name="profileRoot">The profile root.</param>
        private static void TryEnableMultiCoreJit(string profileRoot)
        {
            var mscorlib = typeof(string).Assembly;
            var profileOptimization = mscorlib.GetType("System.Runtime.ProfileOptimization");
            if (profileOptimization != null)
            {
                var setProfileRoot = profileOptimization.GetMethod("SetProfileRoot", BindingFlags.Public | BindingFlags.Static);
                var startProfile = profileOptimization.GetMethod("StartProfile", BindingFlags.Public | BindingFlags.Static);
                if (setProfileRoot != null && startProfile != null)
                {
                    setProfileRoot.Invoke(null, new[] { profileRoot });
                    startProfile.Invoke(null, new[] { "Startup.Profile" });
                }
            }
        }
    }
}