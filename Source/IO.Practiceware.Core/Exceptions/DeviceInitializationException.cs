﻿using System;

namespace IO.Practiceware.Exceptions
{
    [Serializable]
    public class DeviceInitializationException : Exception
    {
        public DeviceInitializationException(string message) : base(message)
        {
            
        }

        public DeviceInitializationException(string message, Exception innerException) : base(message, innerException)
        {
            
        }

        public DeviceInitializationException(Exception innerException): base(null, innerException){}
    }
}
