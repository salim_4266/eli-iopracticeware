﻿using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Exceptions
{
    [Serializable]
    public class GenericTypeParameterException : ArgumentException
    {
        public GenericTypeParameterException()
        {
        }

        public GenericTypeParameterException(string message)
            : base(message)
        {
        }

        public GenericTypeParameterException(string message, string paramName)
            : base(message, paramName)
        {
        }

        public GenericTypeParameterException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected GenericTypeParameterException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

}
