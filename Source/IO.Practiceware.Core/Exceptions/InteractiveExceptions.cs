﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using IO.Practiceware.Application;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Exceptions
{
    /// <summary>
    ///     Represents an exception whose message should be directly displayed to the user.
    /// </summary>
    [Serializable]
    public class InteractiveException : ApplicationException
    {
        public InteractiveException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public InteractiveException(string message)
            : base(message)
        {
        }

        public override string ToString()
        {
            var innerException = InnerException != null ? InnerException.ToString() : string.Empty;
            if (innerException.Contains("ConnectionString"))
            {
                int strtindCS = innerException.IndexOf("ConnectionString");
                int strtindCT = innerException.IndexOf("ConnectionTimeout=\"1200\"");
                string subException = innerException.Substring(strtindCS, (strtindCT + 25) - strtindCS);

                if (innerException.Contains(subException))
                {
                     innerException = innerException.Replace(subException, "");
                }
            }
            return string.Join(Environment.NewLine, Message, string.Empty, innerException);
        }
    }

    public static class InteractiveExceptions
    {
        /// <summary>
        /// Displays the exception along with contextual information regarding application state.
        /// </summary>
        /// <param name="interactionManager">The interaction manager.</param>
        /// <param name="exception">The exception.</param>
        public static void DisplayDetailedException(this IInteractionManager interactionManager, Exception exception)
        {
            interactionManager.DisplayException(GetExceptionInteractionArguments(exception));
        }

        private static ExceptionInteractionArguments GetExceptionInteractionArguments(Exception ex)
        {
            // Construct initial result
            var result = new ExceptionInteractionArguments { Exception = ex };
            if (ex == null) return result;

            // Set message displayed directly to user
            var interactiveException = ex.SearchFor<InteractiveException>();
            if (interactiveException != null)
            {
                result.Message = interactiveException.Message;
            }

            // Collect contextual information and construct new interactive exception details which will be shown
            result.Exception = new InteractiveException(GetDetailedMessage(ex), ex);

            return result;
        }

        private static string GetDetailedMessage(Exception exception)
        {
            var result = new StringBuilder();

            var applicationContext = ApplicationContext.Current;
            var userContext = UserContext.Current;

            // 1. Client name
            var clientName = applicationContext.ClientName;
            if (clientName.IsNotNullOrEmpty())
            {
                if (userContext != null && userContext.ServiceLocation != null)
                {
                    clientName = string.Format("{0} ({1})", clientName, userContext.ServiceLocation.Item2);
                }

                result.AppendFormat("Client name: {0}\n", clientName);
            }

            // 2. Version
            result.AppendFormat("Version: {0}\n", applicationContext.Version);

            // 3. Date
            result.AppendFormat("Date: {0}\n", DateTimeOffset.Now);

            // 4. User Id
            if (userContext != null && userContext.UserDetails != null)
            {
                result.AppendFormat("User Id: {0} ({1})\n", userContext.UserDetails.Id, userContext.UserDetails.DisplayName);
            }

            // 5. Computer name
            var computerName = applicationContext.ComputerName;
            result.AppendFormat("Machine and User: {0} | {1}\n", Environment.MachineName != computerName 
                ? string.Join(@"\\", Environment.MachineName, computerName) 
                : computerName, applicationContext.WindowsUserName);

            // 6. Process name
            result.AppendFormat("Process: {0}\n", Process.GetCurrentProcess().ProcessName);

            // 7. PatientId and AppointmentId
            if (applicationContext.PatientId != null || applicationContext.AppointmentId != null)
            {
                var contextIdsToOutput = new List<string>();
                if (applicationContext.PatientId != null)
                {
                    contextIdsToOutput.Add(string.Format("Patient Id: {0}", applicationContext.PatientId));
                }
                if (applicationContext.AppointmentId != null)
                {
                    contextIdsToOutput.Add(string.Format("Appointment Id: {0}", applicationContext.AppointmentId));
                }

                result.AppendFormat("{0}\n", contextIdsToOutput.Join());
            }

            // 8. All errors

            IEnumerable<Exception> enumval = exception.GetExceptions();
            string newexcptn = "";
            foreach (var exc in enumval)
            {
                if (exc.Message.Contains("ConnectionString"))
                {
                    int strtindCS = exc.Message.IndexOf("ConnectionString");
                    int strtindCT = exc.Message.IndexOf("ConnectionTimeout=\"1200\"");
                    string subException= exc.Message.Substring(strtindCS, (strtindCT + 25) - strtindCS);
                    
                    if(exc.Message.Contains(subException))
                    {
                        string ex=exc.Message.Replace(subException, "");
                        newexcptn = newexcptn + "" + ex;
                    }
                }
                else
                {
                    newexcptn = newexcptn + "" + exc.Message;
                }
            }
            //newexcptn = newexcptn.TrimStart(";");

            result.AppendFormat("Errors: {0}", newexcptn);
            
            return result.ToString();

            //result.AppendFormat("Errors: {0}", exception.GetExceptions().Select(e => e.Message).Join(excludeEmptyItems: true));
            //return result.ToString();
        }
    }
}