﻿using System;

namespace IO.Practiceware.Exceptions
{
    [Serializable]
    public class ClientServerAssemblyMismatchException : InteractiveException
    {
        public ClientServerAssemblyMismatchException(string message) : base(message)
        {
            
        }

        public ClientServerAssemblyMismatchException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
