﻿using System;

namespace IO.Practiceware.Exceptions
{
    [Serializable]
    public class LoadArgumentException : ArgumentException
    {
        public LoadArgumentException()
        {
        }

        public LoadArgumentException(string message)
            : base(message)
        {
        }

        public LoadArgumentException(string message, string paramName)
            : base(message, paramName)
        {

        }

        public LoadArgumentException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public LoadArgumentException(string message, string paramName, Exception inner)
            : base(message, paramName, inner)
        {

        }
    }
}
