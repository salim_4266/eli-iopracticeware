﻿using System;

namespace IO.Practiceware.Exceptions
{
    [Serializable]
    public class InactiveUserLoginException : InteractiveException
    {
        public InactiveUserLoginException(string message) : base(message)
        {
            
        }

        public InactiveUserLoginException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
