namespace IO.Practiceware.Logging
{
    /// <summary>
    /// This type contains the names of well known log categories.
    /// </summary>
    public class LogCategory
    {
        public const string Usage = "Usage";

        public const string LogIn = "Log In";

        public const string LogOut = "Log Out";

        public const string AppExit = "Application Exit";

        public const string AppOpen = "Application Open";

        public const string PatientLettersView = "View PatientLetter";

        public const string PatientLetterInsert = "Create PatientLetter";

        public const string PatDel = "Patient Info Delete";

        public const string RoomInsert = "Room Insert";

        public const string RoomEdit = "Room Edit";

        public const string StaffInsert = "User Insert";

        public const string StaffEdit = "User Edit";

        public const string PatInsert = "Add a Patient";

        public const string PatEdit = "Patient Info Edit";

        public const string BillGen = "Bill Generation";

        public const string MedicationInsert = "Medication Insert";

        public const string EncounterClose = "Encounter Close";

        public const string EncounterOpen = "Encounter Open";

        public const string MedicationRenew = "Medication Renew";

        public const string PrnDocument = "Print Document";

        public const string ViewDocument = "View Document";

        public const string ApptCreate = "Appointment Create";

        public const string OrderSet = "Order Entry";

        public const string PatientNotes = "Patient Notes";

        public const string PatientHistory = "Patient History";

        public const string EncounterSave = "Encounter Save";

        public const string PrnMedication = "Medication Print";

        public const string MedicationDiscontinue = "Medication Discontinue";

        public const string MedicationDel = "Medication Delete";

        public const string MedicationContinue = "Medication Continue";

        public const string PatientLogIn = "Patient LogIn";

        public const string PatientLogOut = "Patient LogOut";

        public const string Tools = "Tools";

        public const string ClinicalImport = "Clinical Import";

        public const string MachineConfig = "Machine Configuration";

        public const string MediCare = "Medicare FeeUtility";

        public const string RxUtility = "RxUtility";

        public const string Utilities = "Utilities";

        public const string Exception = "Exception";

        public const string AuditLog = "Audit Log";

        public const string Copy = "BringFindingsForwardCopy";

        public const string Query = "Query";

        //used only for ClinicalDecisions dll
        public const string ClinicalDecisions = "Clinical Decisions";
    }
}