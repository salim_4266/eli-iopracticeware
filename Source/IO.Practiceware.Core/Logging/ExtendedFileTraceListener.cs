using System;
using System.Diagnostics;
using System.IO;

namespace IO.Practiceware.Logging
{
    public class ExtendedFileTraceListener : TextWriterTraceListener
    {
        public ExtendedFileTraceListener()
        {
        }

        public ExtendedFileTraceListener(Stream stream) : base(stream)
        {
        }

        public ExtendedFileTraceListener(Stream stream, string name) : base(stream, name)
        {
        }

        public ExtendedFileTraceListener(TextWriter writer) : base(writer)
        {
        }

        public ExtendedFileTraceListener(TextWriter writer, string name) : base(writer, name)
        {
        }

        public ExtendedFileTraceListener(string fileName, string name) : base(fileName, name)
        {
        }

        public ExtendedFileTraceListener(string fileName) : base(fileName)
        {
        }

        public override void WriteLine(string message)
        {
            Writer.WriteLine("{0}\t{1}", DateTime.Now.ToString("HH:mm:ss.ffff"), message);
        }
    }
}