﻿using System;
using System.Diagnostics;
using Soaf;

namespace IO.Practiceware.Logging
{
    /// <summary>
    ///     Delegates tracing to another listener.
    /// </summary>
    public class DelegatingTraceListener : TraceListener
    {
        private readonly Func<TraceListener> _getTraceListener;

        public DelegatingTraceListener(Func<TraceListener> getTraceListener)
        {
            _getTraceListener = getTraceListener;
        }

        public override bool IsThreadSafe
        {
            get { return _getTraceListener().IfNotNull(l => l.IsThreadSafe); }
        }

        public override string Name
        {
            get { return _getTraceListener().IfNotNull(l => l.Name); }
            set { _getTraceListener().IfNotNull(l => l.Name = value); }
        }

        public override void Write(string message)
        {
            _getTraceListener().IfNotNull(l => l.Write(message));
        }

        public override void WriteLine(string message)
        {
            _getTraceListener().IfNotNull(l => l.WriteLine(message));
        }

        public override void Flush()
        {
            _getTraceListener().IfNotNull(l => l.Flush());
        }

        public override void Close()
        {
            _getTraceListener().IfNotNull(l => l.Close());
        }

        protected override void Dispose(bool disposing)
        {
            _getTraceListener().IfNotNull(l => l.Dispose());
        }

        public override void Fail(string message)
        {
            _getTraceListener().IfNotNull(l => l.Fail(message));
        }

        public override void Fail(string message, string detailMessage)
        {
            _getTraceListener().IfNotNull(l => l.Fail(message, detailMessage));
        }

        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
        {
            _getTraceListener().IfNotNull(l => l.TraceData(eventCache, source, eventType, id, data));
        }

        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, params object[] data)
        {
            _getTraceListener().IfNotNull(l => l.TraceData(eventCache, source, eventType, id, data));
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id)
        {
            _getTraceListener().IfNotNull(l => l.TraceEvent(eventCache, source, eventType, id));
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
        {
            _getTraceListener().IfNotNull(l => l.TraceEvent(eventCache, source, eventType, id, format, args));
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message)
        {
            _getTraceListener().IfNotNull(l => l.TraceEvent(eventCache, source, eventType, id, message));
        }

        public override void TraceTransfer(TraceEventCache eventCache, string source, int id, string message, Guid relatedActivityId)
        {
            _getTraceListener().IfNotNull(l => l.TraceTransfer(eventCache, source, id, message, relatedActivityId));
        }

        public override void Write(object o)
        {
            _getTraceListener().IfNotNull(l => l.Write(o));
        }

        public override void Write(object o, string category)
        {
            _getTraceListener().IfNotNull(l => l.Write(o, category));
        }

        public override void Write(string message, string category)
        {
            _getTraceListener().IfNotNull(l => l.Write(message, category));
        }

        public override void WriteLine(object o)
        {
            _getTraceListener().IfNotNull(l => l.WriteLine(o));
        }

        public override void WriteLine(object o, string category)
        {
            _getTraceListener().IfNotNull(l => l.WriteLine(o, category));
        }

        public override void WriteLine(string message, string category)
        {
            _getTraceListener().IfNotNull(l => l.WriteLine(message, category));
        }

    }
}