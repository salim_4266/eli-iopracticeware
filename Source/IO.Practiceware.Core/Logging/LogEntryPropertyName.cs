using IO.Practiceware.Model.Auditing;

namespace IO.Practiceware.Logging
{
    /// <summary>
    /// Well known Log Entry Property names.
    /// </summary>
    public class LogEntryPropertyName
    {
        public static readonly LogEntryPropertyName PatientId = new LogEntryPropertyName("PatientId");
        public static readonly LogEntryPropertyName AppointmentId = new LogEntryPropertyName("AppointmentId");
        public static readonly LogEntryPropertyName Version = new LogEntryPropertyName("Version");
        public static readonly LogEntryPropertyName ClientName = new LogEntryPropertyName("Client Name");
        public static readonly LogEntryPropertyName SourceCodeOrigin = new LogEntryPropertyName("Source Code Origin");

        public string Name { get; private set; }

        private LogEntryPropertyName(string name)
        {
            Name = name;
        }

        public LogEntryProperty Value(object value)
        {
            return new LogEntryProperty { Name = Name, Value = string.Format("{0}", value) };
        }

        public static implicit operator string(LogEntryPropertyName name)
        {
            return name.Name;
        }
    }
}