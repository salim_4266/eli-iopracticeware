using System.ComponentModel;
using Cassia;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Storage;
using NLog;
using NLog.Config;
using NLog.Targets;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using ApplicationContext = IO.Practiceware.Application.ApplicationContext;
using IO.Practiceware.Logging;

[assembly: Component(typeof(LoggingDatabaseConfiguration), typeof(ILoggingDatabaseConfiguration), Priority = 1000)]

namespace IO.Practiceware.Logging
{
    internal class LoggingConfigurationManager
    {

        /// <summary>
        ///   The amount of time that must pass before taking another screenshot.
        /// </summary>
        private const int MaxScreenshotFrequencySeconds = 8;

        private const string ErrorCapturingScreenshotMessage = "Error capturing screenshot.";
        private const string ClientConnectionContextKey = "__Logging_ActiveClient_ConnectionString___";
        private static readonly Lazy<EventLogTraceListener> EventLogTraceListenerInternal = Lazy.For(CreateEventLogTraceListener);
        private static DateTime _lastScreenshot;

        internal static EventLogTraceListener EventLogTraceListener
        {
            get { return EventLogTraceListenerInternal.Value; }
        }

        private static EventLogTraceListener CreateEventLogTraceListener()
        {
            string logName = EventLog.LogNameFromSourceName("IO Practiceware", ".");
            if (logName != "IO Practiceware" && logName != string.Empty)
            {
                EventLog.DeleteEventSource("IO Practiceware", ".");
            }

            if (!EventLog.SourceExists("IO Practiceware"))
            {
                try
                {
                    EventLog.CreateEventSource("IO Practiceware", "IO Practiceware");
                }
                catch (Exception ex)
                {
                    var newEx = new Exception
                        ("An error occurred creating the IO Practiceware Event Log.".FormatWith(ex));
                    EventLog.WriteEntry("Application Error", newEx.ToString());
                    throw newEx;
                }
            }
            // Can't use log entry trace listener yet, so add a normal one for startup error logging
            var eventLogTraceListener = new EventLogTraceListener("IO Practiceware") { Name = "EventLogTraceListener", EventLog = new EventLog("IO Practiceware") { Source = "IO Practiceware" } };
            return eventLogTraceListener;
        }

        /// <summary>
        ///   Tries to capture and save a screenshot of the error if applicable.
        /// </summary>
        /// <param name="logEntry"> The log entry. </param>
        private static void TryCaptureAndSaveScreenshot(LogEntry logEntry)
        {
            try
            {
                var screenShotDate = DateTime.Now.ToClientTime();

                if (ShouldTakeScreenshot(logEntry)
                    // Ensure we are not creating screenshots too often
                    && (screenShotDate - _lastScreenshot).TotalSeconds >= MaxScreenshotFrequencySeconds)
                {
                    _lastScreenshot = screenShotDate;
                    string directoryPath = Path.Combine(ConfigurationManager.ServerDataPath, "Error Screenshots");
                    string filePath = Path.Combine(directoryPath, screenShotDate.ToString("yyyy.MM.dd HH.mm.ss") + " {0}.jpg".FormatWith(Environment.MachineName));

                    ScreenCapture.CaptureAndSaveScreen(filePath);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    Trace.TraceError(new Exception(ErrorCapturingScreenshotMessage, ex).ToString());
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch
                // ReSharper restore EmptyGeneralCatchClause
                {
                }
            }
        }

        /// <summary>
        ///   Determines whether [is log entry valid for screenshot] [the specified log entry]. Only includes Warning+ entries.
        /// </summary>
        /// <param name="logEntry"> The log entry. </param>
        /// <returns> <c>true</c> if [is log entry valid for screenshot] [the specified log entry]; otherwise, <c>false</c> . </returns>
        private static bool ShouldTakeScreenshot(LogEntry logEntry)
        {
            // Check that we are interested in taking the screenshot
            var shouldTake = logEntry.Severity > Severity.Warning
                && Environment.UserInteractive
                && !logEntry.Message.Contains(ErrorCapturingScreenshotMessage);

            // Check that configuration says it is enabled
            if (shouldTake)
            {
                shouldTake = ConfigurationManager.IsLoaded &&
                        "true".Equals(ConfigurationManager.CustomAppSettings["EnableErrorScreenshots"], StringComparison.OrdinalIgnoreCase);
            }

            // Check that we have a place to store them
            if (shouldTake)
            {
                shouldTake = logEntry.Source == FileManager.TraceSourceName
                    // We can't use FileManager if it's an error coming from FileManager
                    ? !string.IsNullOrEmpty(ConfigurationManager.ServerDataPath)
                    : FileManager.Instance.DirectoryExists(ConfigurationManager.ServerDataPath);
            }
            return shouldTake;
        }

        public static void Initialize()
        {
            Trace.Listeners.Add(new LogEntryEventLogTraceListener(Lazy.For(() => EventLogTraceListener)) { Name = "EventLogTraceListener" });

            var loggingDatabaseTraceListener = new LoggingDatabaseTraceListener(l =>
            {
                var connectionString = l.ExtendedProperties
                    .OfType<Tuple<string, string>>()
                    .Where(t => t.Item1 == ClientConnectionContextKey)
                    .Select(t => t.Item2)
                    .FirstOrDefault() ?? ConfigurationManager.PracticeRepositoryConnectionString;
                return connectionString;
            });
            loggingDatabaseTraceListener.Filter = new IsLoggedInTraceFilter(loggingDatabaseTraceListener.Filter);
            Trace.Listeners.Add(loggingDatabaseTraceListener);

            InitializeLogentriesLogging();

            Trace.Listeners.OfType<LogEntryTraceListener>().ToList().ForEach(l => l.Filter = new LogEntryPropertiesTraceFilter(l.Filter));

            // Make sure it's first so it gets to add the properties before any other listeners get it.
            Trace.Listeners.Insert(0, new LogEntryTraceListener((le, filter) => TryCaptureAndSaveScreenshot(le)));
            Trace.Listeners.OfType<TraceListener>().ToList().ForEach(i => FileManager.TraceSource.Listeners.Add(i));
            if (FileManager.TraceSource.Switch == null || FileManager.TraceSource.Switch.Level == SourceLevels.Off)
                // only explicitly set the error level switch if it currently has a .NET provided default value (of Off)
                FileManager.TraceSource.Switch = new SourceSwitch("FileManagerTraceSwitch") { Level = SourceLevels.Error };
        }

        private static void InitializeLogentriesLogging()
        {
            ConfigurationItemFactory.Default.Targets.RegisterDefinition("Logentries", typeof(LogentriesTarget));
            var nLogTraceListener = new LogentriesTraceListener();
            Trace.Listeners.Add(nLogTraceListener);
        }

        /// <summary>
        /// Should only log to this listener if the user is logged in.
        /// </summary>
        private class IsLoggedInTraceFilter : TraceFilter
        {
            private readonly TraceFilter _innerFilter;

            public IsLoggedInTraceFilter(TraceFilter innerFilter)
            {
                _innerFilter = innerFilter;
            }

            public override bool ShouldTrace(TraceEventCache cache, string source, TraceEventType eventType, int id, string formatOrMessage, object[] args, object data1, object[] data)
            {
                return (!LoggingConfigurationExtensions.DisableLoggingDatabaseTracing) && (IsLoggedIn || IsDirectDatabaseConnection) && (_innerFilter == null || _innerFilter.ShouldTrace(cache, source, eventType, id, formatOrMessage, args, data1, data));
            }

            private static bool IsLoggedIn
            {
                get { return UserContext.Current != null && UserContext.Current.UserDetails != null; }
            }

            private static bool IsDirectDatabaseConnection
            {
                get { return DbConnections.TryGetConnectionStringBuilder<SqlConnectionStringBuilder>(ConfigurationManager.PracticeRepositoryConnectionString) != null; }
            }
        }

        /// <summary>
        /// Adds LogEntryProperties.
        /// </summary>
        private class LogEntryPropertiesTraceFilter : TraceFilter
        {
            private readonly TraceFilter _innerFilter;
            private readonly Lazy<string> _connectedClientName;

            public LogEntryPropertiesTraceFilter(TraceFilter innerFilter)
            {
                _innerFilter = innerFilter;
                _connectedClientName = new Lazy<string>(() =>
                {
                    try
                    {
                        return new TerminalServicesManager().CurrentSession.ClientName;
            }
                    catch (Win32Exception)
                    {
                        return null;
                    }
                });
            }

            public override bool ShouldTrace(TraceEventCache cache, string source, TraceEventType eventType, int id, string formatOrMessage, object[] args, object data1, object[] data)
            {
                if (_innerFilter == null || _innerFilter.ShouldTrace(cache, source, eventType, id, formatOrMessage, args, data1, data))
                {
                    var logEntry = data1 as LogEntry;
                    if (logEntry != null)
                    {
                        // lock since this method may fire on multiple listeners on different threads
                        lock (logEntry)
                        {
                            try
                            {
                            AddLogEntryProperties(logEntry);
                        }
// ReSharper disable once EmptyGeneralCatchClause
                            catch (Exception)
                            {}
                    }
                    }

                    return true;
                }
                return false;
            }

            /// <summary>
            ///   Adds the log entry properties based on the ApplicationContext
            /// </summary>
            private void AddLogEntryProperties(LogEntry logEntry)
            {
                // Temporarily store connection string for database trace listener
                logEntry.ExtendedProperties.Add(new Tuple<string, string>(ClientConnectionContextKey, ConfigurationManager.PracticeRepositoryConnectionString));

                var applicationContext = ApplicationContext.Current;
                if (applicationContext.AppointmentId.HasValue)
                {
                        LogEntryProperty propertyExists =
                            logEntry.Properties.FirstOrDefault(i => i.Name == LogEntryPropertyName.AppointmentId &&
                                                                i.Value == applicationContext.AppointmentId.Value.ToString());

                        if (propertyExists == null)
                        {
                            logEntry.Properties.Add(new LogEntryProperty
                                                        {
                                                            Name = LogEntryPropertyName.AppointmentId,
                            Value = applicationContext.AppointmentId.Value.ToString()
                                                        });
                        }
                    }
                
                if (applicationContext.PatientId.HasValue)
                    {
                        LogEntryProperty propertyExists =
                        logEntry.Properties.FirstOrDefault(i => i.Value == applicationContext.PatientId.Value.ToString() &&
                                                                    i.Name == LogEntryPropertyName.PatientId);

                        if (propertyExists == null)
                        {
                            logEntry.Properties.Add(new LogEntryProperty
                                                        {
                                                            Name = LogEntryPropertyName.PatientId,
                            Value = applicationContext.PatientId.Value.ToString()
                                                        });
                        }
                    }

                var sourceCodeOrigin = logEntry.Properties.FirstOrDefault(u => u.Name == LogEntryPropertyName.SourceCodeOrigin);
                if (sourceCodeOrigin == null)
                {
                    logEntry.Properties.Add(
                        new LogEntryProperty
                        {
                            Name = LogEntryPropertyName.SourceCodeOrigin,
                            Value = applicationContext.IsServerSide ? "Server" : "Client"
                        });
                }

                var versionPropertyExists = logEntry.Properties.FirstOrDefault(u => u.Name == LogEntryPropertyName.Version);
                if (versionPropertyExists == null)
                {
                    logEntry.Properties.Add(
                        new LogEntryProperty
                        {
                            Name = LogEntryPropertyName.Version,
                            Value = applicationContext.Version
                        });
                }

                var userContext = UserContext.Current;

                var clientName = applicationContext.ClientName;
                if (clientName.IsNotNullOrEmpty())
                {
                    if (userContext != null && userContext.ServiceLocation != null)
                    {
                        clientName = string.Format("{0} ({1})", clientName, userContext.ServiceLocation.Item2);
                    }

                    var clientNamePropertyExists = logEntry.Properties.FirstOrDefault(u => u.Name == LogEntryPropertyName.ClientName && u.Value == clientName);
                    if (clientNamePropertyExists == null)
                    {
                        logEntry.Properties.Add(
                            new LogEntryProperty
                            {
                                Name = LogEntryPropertyName.ClientName,
                                Value = clientName
                            });
                    }
                }

                if (userContext != null && userContext.UserDetails != null)
                    {
                    logEntry.UserId = userContext.UserDetails.Id.ToString();
                }

                // override the ip address in the LogEntry if we are in the context of a web server (cloud service)
                var clientIp = GetUserHostAddressSafe();
                logEntry.IpAddress = clientIp ?? logEntry.IpAddress;

                // Update machine name for RDS
                if (!string.IsNullOrWhiteSpace(_connectedClientName.Value) && !logEntry.MachineName.Contains("\\"))
                {
                    logEntry.MachineName = logEntry.MachineName + "\\" + _connectedClientName.Value;
                }
            }

            [DebuggerNonUserCode]
            private static string GetUserHostAddressSafe()
            {
                try
                {
                    if (HostingEnvironment.IsHosted
                        && HttpContext.Current != null
                        && !HttpContext.Current.Request.IsLocal)
                    {
                        return HttpContext.Current.Request.UserHostAddress;
                    }
                }
                // ReSharper disable once EmptyGeneralCatchClause
                catch
                { }

                return null;
            }
        }
    }

    public static class LoggingConfigurationExtensions
    {
        internal static bool DisableLoggingDatabaseTracing { get; set; }

        /// <summary>
        /// Wrapper around TraceError for the System.Diagnostics.Trace class
        /// </summary>
        /// <param name="message">The message to trace</param>
        /// <param name="disableLoggingRepositoryTracing">Set to true to disable the LoggingRepositoryTraceListener, which disables any trace logging to the Database</param>
        public static void TraceError(string message, bool disableLoggingRepositoryTracing)
        {
            if (disableLoggingRepositoryTracing)
            {
                DisableLoggingDatabaseTracing = true;
                Trace.TraceError(message);
                DisableLoggingDatabaseTracing = false;
            }
            else
            {
                Trace.TraceError(message);
            }
        }
    }

    /// <summary>
    /// A trace listener that supports sending events to the logentries.com data hosting provider
    /// </summary>
    public class LogentriesTraceListener : SavingTraceListener
    {
        public LogentriesTraceListener()
            : base(SaveLogEntry)
        {

        }

        private static readonly ICustomFormatter LeFormatter = new LogentriesFormatter();

        private static void SaveLogEntry(LogEntry logEntry)
        {
            // write to nlog 
            var logger = LogManager.GetLogger("Logentries");
            if (logEntry.Severity == Severity.Error)
            {
                logger.Error(LeFormatter.Format(null, logEntry, null));
            }
            else if (logEntry.Severity == Severity.Information)
            {
                logger.Info(LeFormatter.Format(null, logEntry, null));
            }
            else if (logEntry.Severity == Severity.Warning)
            {
                logger.Warn(LeFormatter.Format(null, logEntry, null));
            }
        }

        public override string ToString()
        {
            return string.Format(new LogentriesFormatter(), "{0}", this);
        }

        /// <summary>
        ///   Provides a default mechanism for producing text output of a LogEntries LogEntry.
        /// </summary>
        private class LogentriesFormatter : ICustomFormatter, IFormatProvider
        {
            #region ICustomFormatter Members

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                var logEntry = arg as LogEntry;
                if (logEntry == null) throw new InvalidOperationException("Argument should be a log entry.");

                var builder = new StringBuilder();

                if (logEntry.ActivityId.HasValue)
                {
                    builder.AppendFormat("Activity Id = {0} | ", logEntry.ActivityId);
                }
                if (logEntry.RelatedActivityId.HasValue)
                {
                    builder.AppendFormat("Related Activity Id = {0} | ", logEntry.RelatedActivityId);
                }
                if (logEntry.Categories.IsNotNullOrEmpty())
                {
                    builder.AppendFormat("Categories = {0} | ", string.Join(", ", logEntry.Categories.Select(i => i.Name).ToArray()));
                }
                if (logEntry.EventId.HasValue && logEntry.EventId.Value > 0)
                {
                    builder.AppendFormat("Event Id = {0} | ", logEntry.EventId);
                }
                if (logEntry.MachineName.IsNotNullOrEmpty())
                {
                    builder.AppendFormat("Machine Name = {0} | ", logEntry.MachineName);
                }
                if (logEntry.IpAddress.IsNotNullOrEmpty())
                {
                    builder.AppendFormat("Ip Address = {0} | ", logEntry.IpAddress);
                }
                if (logEntry.ProcessId.HasValue && logEntry.ProcessId.Value > 0)
                {
                    builder.AppendFormat("Process Id = {0} | ", logEntry.ProcessId);
                }
                if (logEntry.ProcessName.IsNotNullOrEmpty())
                {
                    builder.AppendFormat("Process Name = {0} | ", logEntry.ProcessName);
                }
                if (logEntry.Source.IsNotNullOrEmpty())
                {
                    builder.AppendFormat("Source = {0} | ", logEntry.Source);
                }
                if (logEntry.ThreadName.IsNotNullOrEmpty())
                {
                    builder.AppendFormat("Thread = {0} | ", logEntry.ThreadName);
                }

                if (logEntry.Properties.IsNotNullOrEmpty())
                {
                    foreach (var property in logEntry.Properties.Where(property => property.Name.IsNotNullOrEmpty() && property.Value.IsNotNullOrEmpty()))
                    {
                        builder.AppendFormat("{0} = {1} | ", property.Name, property.Value);
                    }
                }

                builder.AppendFormat("Message = {0}", logEntry.Message);

                return format == null ? builder.ToString() : string.Format(format, builder);
            }

            #endregion

            #region IFormatProvider Members

            public object GetFormat(Type formatType)
            {
                return formatType == typeof(ICustomFormatter) ? this : null;
            }

            #endregion
        }
    }

    internal class LoggingDatabaseConfiguration : ILoggingDatabaseConfiguration
    {
        private readonly string[] _categoryFilters =
        {
            LogCategory.Usage,
            LogCategory.LogIn,
            LogCategory.LogOut,
            LogCategory.AppExit,
            LogCategory.AppOpen,
            LogCategory.PatientLettersView,
            LogCategory.PatientLetterInsert,
            LogCategory.PatDel,
            LogCategory.RoomInsert,
            LogCategory.RoomEdit,
            LogCategory.StaffInsert,
            LogCategory.StaffEdit,
            LogCategory.PatInsert,
            LogCategory.PatEdit,
            LogCategory.BillGen,
            LogCategory.MedicationInsert,
            LogCategory.EncounterClose,
            LogCategory.EncounterOpen,
            LogCategory.MedicationRenew,
            LogCategory.PrnDocument,
            LogCategory.ViewDocument,
            LogCategory.ApptCreate,
            LogCategory.OrderSet,
            LogCategory.PatientNotes,
            LogCategory.PatientHistory,
            LogCategory.EncounterSave,
            LogCategory.PrnMedication,
            LogCategory.MedicationDiscontinue,
            LogCategory.MedicationDel,
            LogCategory.MedicationContinue,
            LogCategory.PatientLogIn,
            LogCategory.PatientLogOut,
            LogCategory.Tools,
            LogCategory.ClinicalImport,
            LogCategory.MachineConfig,
            LogCategory.MediCare,
            LogCategory.RxUtility,
            LogCategory.Utilities,
            LogCategory.Exception,
            LogCategory.AuditLog,
            LogCategory.Copy,
            LogCategory.Query,
            LogCategory.ClinicalDecisions
        };

        #region ILoggingRepositoryConfiguration Members

        public IEnumerable<string> CategoryFilters
        {
            get { return _categoryFilters; }
        }

        #endregion
    }
}