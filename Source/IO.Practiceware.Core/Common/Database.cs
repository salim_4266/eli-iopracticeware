﻿using Soaf.Data;
using System;
using System.Data;

namespace IO.Practiceware
{
    public static class Database
    {
        public static int GetInt32Value(this DataTable table, string columnName)
        {
            if (table == null) throw new ArgumentNullException("table");
            if (String.IsNullOrWhiteSpace(columnName)) throw new ArgumentNullException("columnName");
            return table.Rows[0].GetInt32Value(columnName);
        }

        public static int GetInt32Value(this DataRow row, string columnName)
        {
            if (row == null) throw new ArgumentNullException("row");
            if (String.IsNullOrWhiteSpace(columnName)) throw new ArgumentNullException("columnName");
            return row[columnName] == DBNull.Value ? 0 : Convert.ToInt32(row[columnName]);
        }

        public static bool GetBoolValue(this DataTable table, string columnName)
        {
            if (table == null) throw new ArgumentNullException("table");
            if (String.IsNullOrWhiteSpace(columnName)) throw new ArgumentNullException("columnName");
            return table.Rows[0].GetBoolValue(columnName);
        }

        public static bool GetBoolValue(this DataRow row, string columnName)
        {
            if (row == null) throw new ArgumentNullException("row");
            if (String.IsNullOrWhiteSpace(columnName)) throw new ArgumentNullException("columnName");
            return row[columnName] != DBNull.Value && Convert.ToBoolean(row[columnName]);
        }

        public static DateTime GetDateTimeValue(this DataTable table, string columnName)
        {
            if (table == null) throw new ArgumentNullException("table");
            if (String.IsNullOrWhiteSpace(columnName)) throw new ArgumentNullException("columnName");
            return table.Rows[0].GetDateTimeValue(columnName);
        }

        public static DateTime GetDateTimeValue(this DataRow row, string columnName)
        {
            if (row == null) throw new ArgumentNullException("row");
            if (String.IsNullOrWhiteSpace(columnName)) throw new ArgumentNullException("columnName");
            return row[columnName] == DBNull.Value ? new DateTime() : Convert.ToDateTime(row[columnName]);
        }

        public static string GetStringValue(this DataTable table, string columnName)
        {
            if (table == null) throw new ArgumentNullException("table");
            if (String.IsNullOrWhiteSpace(columnName)) throw new ArgumentNullException("columnName");
            return table.Rows[0].GetStringValue(columnName);
        }

        public static string GetStringValue(this DataRow row, string columnName)
        {
            if (row == null) throw new ArgumentNullException("row");
            if (String.IsNullOrWhiteSpace(columnName)) throw new ArgumentNullException("columnName");
            return row[columnName] == DBNull.Value ? String.Empty : row[columnName].ToString();
        }

        /// <summary>
        /// Gets value of column with name columnName if table contains it.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row">The row.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="returnEmptyStrings">if set to <c>true</c> then an empty string is returned if the field specified is missing and T is string.</param>
        /// <returns></returns>
        public static T GetValueOrDefault<T>(this DataRow row, string columnName, bool returnEmptyStrings=true)
        {
            if (row.Table.Columns.Contains(columnName))
                return (row[columnName] as string == string.Empty && typeof(T) != typeof(string)) ? default(T) : row.Get<T>(columnName);


            if (returnEmptyStrings && typeof(T) == typeof(string))
                return (T)(object)string.Empty;

            return default(T);

        }
    }
}