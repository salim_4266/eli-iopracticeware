﻿using Soaf.Collections;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace IO.Practiceware
{
    /// <summary>
    /// Provides "better" method matching than the default binder.
    /// </summary>
    public class CustomBinder : Binder
    {
        private readonly Binder _defaultBinder;

        public CustomBinder(Binder defaultBinder = null)
        {
            _defaultBinder = defaultBinder;
        }

        public override MethodBase BindToMethod(BindingFlags bindingAttr, MethodBase[] match, ref object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] names, out object state)
        {
            var originalArgs = args;
            var result =
                TryBindToMethod(_defaultBinder, bindingAttr, match, ref args, modifiers, culture, names, out state) ??
                match.OrderByDescending(m => m.GetParameters()
                                                    .Where((item, index) => originalArgs.Length > index && originalArgs[index] != null && item.ParameterType == originalArgs[index].GetType()).Count()).FirstOrDefault();

            if (result == null) return null;

            var parameters = result.GetParameters();

            args = new object[parameters.Length];

            for (int i = 0; i < args.Length; i++)
            {
                var parameter = parameters[i];

                var namedIndex = names == null ? -1 : names.IndexOf(parameter.Name);

                if (namedIndex >= 0 && originalArgs.Length > namedIndex) args[i] = originalArgs[namedIndex];
                else if (originalArgs.Length > i) args[i] = originalArgs[i];
                else args[i] = Missing.Value;
            }

            return result;
        }

        [DebuggerNonUserCode]
        private MethodBase TryBindToMethod(Binder binder, BindingFlags bindingAttr, MethodBase[] match, ref object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] names, out object state)
        {
            state = null;
            try
            {
                return binder.BindToMethod(bindingAttr, match, ref args, modifiers, culture, names, out state);
            }
            catch (MissingMethodException)
            {
            }
            return null;
        }

        public override FieldInfo BindToField(BindingFlags bindingAttr, FieldInfo[] match, object value, CultureInfo culture)
        {
            return TryBindToField(_defaultBinder, bindingAttr, match, value, culture) ?? match.FirstOrDefault();
        }

        [DebuggerNonUserCode]
        private FieldInfo TryBindToField(Binder binder, BindingFlags bindingAttr, FieldInfo[] match, object value, CultureInfo culture)
        {
            if (binder == null) return null;
            try
            {
                return binder.BindToField(bindingAttr, match, value, culture);
            }
            catch (MissingMethodException)
            {
            }
            return null;
        }

        public override MethodBase SelectMethod(BindingFlags bindingAttr, MethodBase[] match, Type[] types, ParameterModifier[] modifiers)
        {
            MethodBase result = null;
            if (_defaultBinder != null)
            {
                result = _defaultBinder.SelectMethod(bindingAttr, match, types, modifiers);
            }

            if (result == null)
            {
                result = match.OrderByDescending(m => m.GetParameters()
                                                          .Where((item, index) => types.Length > index && types[index] != null && item.ParameterType == types[index]).Count()).FirstOrDefault();
            }

            return result;
        }

        public override PropertyInfo SelectProperty(BindingFlags bindingAttr, PropertyInfo[] match, Type returnType, Type[] indexes, ParameterModifier[] modifiers)
        {
            PropertyInfo result = null;
            if (_defaultBinder != null)
            {
                result = _defaultBinder.SelectProperty(bindingAttr, match, returnType, indexes, modifiers);
            }
            if (result == null)
            {
                return match.OrderByDescending(m => m.GetIndexParameters()
                                                        .Where((item, index) => indexes.Length > index && indexes[index] != null && item.ParameterType == indexes[index]).Count()).FirstOrDefault();
            }
            return result;
        }

        public override object ChangeType(object value, Type type, CultureInfo culture)
        {
            if (_defaultBinder != null) return _defaultBinder.ChangeType(value, type, culture);
            return Convert.ChangeType(value, type);
        }

        public override void ReorderArgumentArray(ref object[] args, object state)
        {
            if (_defaultBinder != null) _defaultBinder.ReorderArgumentArray(ref args, state);
        }
    }
}
