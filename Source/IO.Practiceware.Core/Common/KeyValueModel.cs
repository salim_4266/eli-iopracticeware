﻿using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware
{
    /// <summary>
    ///   A mutable keyvalue pair for storing key/value pairs. Use sparingly. Many situations would be better served by a dedicated class.
    /// </summary>
    /// <typeparam name="TKey"> The type of the key. </typeparam>
    /// <typeparam name="TValue"> The type of the value. </typeparam>
    [DataContract]
    public class KeyValueModel<TKey, TValue> : IViewModel
    {
        public KeyValueModel()
        {
        }

        public KeyValueModel(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }

        [DataMember]
        public TKey Key { get; set; }

        [DataMember]
        public TValue Value { get; set; }
    }

    [DataContract]
    public class KeyValueModel : KeyValueModel<object, object>
    {
        /// <summary>
        ///   Creates the specified key.
        /// </summary>
        /// <typeparam name="TKey"> The type of the key. </typeparam>
        /// <typeparam name="TValue"> The type of the value. </typeparam>
        /// <param name="key"> The key. </param>
        /// <param name="value"> The value. </param>
        /// <returns> </returns>
        public static KeyValueModel<TKey, TValue> Create<TKey, TValue>(TKey key, TValue value)
        {
            return new KeyValueModel<TKey, TValue>(key, value);
        }
    }
}