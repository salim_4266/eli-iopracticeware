﻿using System.Text.RegularExpressions;
using Soaf;
using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Linq;
using System.Printing;
using System.Threading;
using System.Threading.Tasks;

namespace IO.Practiceware
{
    public static class Printers
    {
        private static readonly IDictionary<string, string> ResolvedNetworkPrinterNames = new Dictionary<string, string>().Synchronized();

        /// <summary>
        ///   Information about a printer.
        /// </summary>
        public class Printer
        {
            public string Computer { get; set; }
            public string ShareName { get; set; }
            public string Location { get; set; }

            public string UncPath
            {
                get { return @"\\{0}\{1}".FormatWith(Computer, ShareName); }
            }
        }

        /// <summary>
        ///   Finds network printers across all network computers.
        /// </summary>
        /// <param name="onNetworkPrinterFound"> The shared printer found. </param>
        /// <param name="onSearchComplete"> The search complete. </param>
        public static void FindNetworkPrinters(Action<Printer> onNetworkPrinterFound, Action onSearchComplete = null)
        {
            if (onSearchComplete == null) onSearchComplete = delegate { };

            int isComplete = 0;

            // ReSharper disable AccessToModifiedClosure
            Action<Printer> onNetworkPrinterFoundInternal = p => { if (Interlocked.Exchange(ref isComplete, 0) == 0) onNetworkPrinterFound(p); };
            // ReSharper restore AccessToModifiedClosure

            Task.Factory
                .StartNew(() => Networking.GetNetworkComputers())
                .ContinueWith(result =>
                              result.Result.ToList().AsParallel().WithDegreeOfParallelism(10).ForAll(
                                  computer => Task.Factory.StartNew(() => FindComputerNetworkPrinters(computer, onNetworkPrinterFoundInternal)).Wait(15000)))
                                  .ContinueWith(t =>
                                                    {
                                                        Interlocked.Exchange(ref isComplete, 1);
                                                        onSearchComplete();
                                                    });
        }

        /// <summary>
        /// Finds the computer's network printers.
        /// </summary>
        /// <param name="computer">The computer.</param>
        /// <param name="onNetworkPrinterFound">The network printer found.</param>
        [DebuggerNonUserCode]
        public static void FindComputerNetworkPrinters(string computer, Action<Printer> onNetworkPrinterFound)
        {
            try
            {
                using (var server = new PrintServer(@"\\" + computer))
                {
                    server.GetPrintQueues().Where(q => q.IsShared).Select(ToNetworkPrinter).ForEachWithSinglePropertyChangedNotification(onNetworkPrinterFound);
                }
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }
        }

        /// <summary>
        ///   Converts a ManagementObject to a network printer object.
        /// </summary>
        /// <param name="printQueue"> The print queue. </param>
        /// <returns> </returns>
        private static Printer ToNetworkPrinter(PrintQueue printQueue)
        {
            var printer = new Printer
                              {
                                  Computer = printQueue.HostingPrintServer.Name.TrimStart('\\'),
                                  Location = printQueue.Location,
                                  ShareName = printQueue.ShareName
                              };
            return printer;
        }


        /// <summary>
        /// Tries to resolve the printer name to a locally accessible printer. Handles and logs errors.
        /// </summary>
        /// <param name="printerName">Name of the printer.</param>
        /// <returns></returns>
        public static string TryResolvePrinterName(string printerName)
        {
            try
            {
                return ResolvePrinterName(printerName);
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
            return null;
        }

        /// <summary>
        /// Tries to resolve the printer name to a locally accessible printer.
        /// </summary>
        /// <param name="printerName">Name of the printer.</param>
        /// <returns></returns>
        public static string ResolvePrinterName(string printerName)
        {
            Trace.TraceInformation("Trying to resolve printer name {0}.", printerName);

            if (printerName.IsNullOrWhiteSpace())
            {
                return null;
            }

            // RDP redirected printers will have a "(redirected #)" suffix. We should ignore this suffix.
            var redirectedPrinterNameReplacer = new Regex(@"\s\(redirected \d+\)$");

            var installedPrinter = PrinterSettings.InstalledPrinters.OfType<string>().FirstOrDefault(i =>
                printerName.Equals(i, StringComparison.OrdinalIgnoreCase)
                || printerName.Equals(redirectedPrinterNameReplacer.Replace(i, string.Empty), StringComparison.OrdinalIgnoreCase));
            if (installedPrinter.IsNotNullOrEmpty())
            {
                return installedPrinter;
            }

            string[] printerNameParts = printerName.Split('\\').Where(p => p != String.Empty).ToArray();
            if (printerNameParts.Length == 2)
            {
                string serverName = printerNameParts[0];
                string shareName = printerNameParts[1];

                string result;
                if (!ResolvedNetworkPrinterNames.TryGetValue(printerName, out result))
                {
                    result = ResolveNetworkPrinterName(shareName, serverName);

                    if (result != null) ResolvedNetworkPrinterNames[printerName] = result;
                }

                return result;
            }

            return null;
        }

        private static string ResolveNetworkPrinterName(string shareName, string serverName)
        {
            using (var localServer = new LocalPrintServer())
            {
                if (Environment.MachineName.Equals(serverName, StringComparison.OrdinalIgnoreCase))
                {
                    // find local printer with identical share name
                    return FindPrintQueueWithNameOrSharedName(shareName, localServer.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local }).ToArray()).IfNotNull(q => q.FullName);
                }

                PrintQueue connectedPrintQueue = FindPrintQueueWithNameOrSharedName(shareName, GetConnectedPrintQueuesToServer(serverName, localServer));

                if (connectedPrintQueue != null) return connectedPrintQueue.FullName;

                using (var remoteServer = new PrintServer(@"\\" + serverName))
                {
                    PrintQueue printQueueToConnect = FindPrintQueueWithNameOrSharedName(shareName, remoteServer.GetPrintQueues().ToArray());

                    if (printQueueToConnect != null && localServer.ConnectToPrintQueue(printQueueToConnect))
                    {
                        return printQueueToConnect.FullName;
                    }
                }
            }
            return null;
        }

        private static PrintQueue[] GetConnectedPrintQueuesToServer(string serverName, PrintServer server)
        {
            return server.
                GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Connections }).Where(q =>
                    serverName.Equals(q.FullName.Split(new[] { @"\" }, StringSplitOptions.RemoveEmptyEntries).First(), StringComparison.OrdinalIgnoreCase)).ToArray();
        }

        private static PrintQueue FindPrintQueueWithNameOrSharedName(string name, PrintQueue[] source)
        {
            return
                source.FirstOrDefault(q => q.ShareName.Equals(name, StringComparison.OrdinalIgnoreCase))
                ??
                source.FirstOrDefault(q => q.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
        }
    }
}