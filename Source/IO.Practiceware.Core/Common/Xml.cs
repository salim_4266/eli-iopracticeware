﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using System.Text;
using Soaf;
using Soaf.Collections;

namespace IO.Practiceware
{
    /// <summary>
    /// Provides static helper functions to transform xml data into various forms (xhtml, pdf, etc.)
    /// </summary>
    public static class XmlTransform
    {

        /// <summary>
        /// Transforms the <see cref="xml" /> string content using the <see cref="xsl" /> stylesheet content
        /// </summary>
        /// <param name="xml">The xml content to transform</param>
        /// <param name="xsl">The xsl stylesheet content used to perform the transformation</param>
        /// <param name="omitXmlDeclaration">Optional. Set to true if you want to omit the beginning 'xml' declaration from the output document. Default is false</param>
        /// <returns>
        /// A <see cref="StringBuilder" /> containing the transformed Xml content
        /// </returns>
        public static StringBuilder TransformXml(string xml, string xsl, bool omitXmlDeclaration = false)
        {

            return InnerTransformXml(xml, xsl, omitXmlDeclaration);
        }

        private static StringBuilder InnerTransformXml(string xml, string xsl, bool omitXmlDeclaration)
        {
            // create a transformer
            var xslTransformer = CreateTransformer(xsl);

            // string builder used by the transformer to write the output html
            var htmlBuilder = new StringBuilder(512);

            var writerSettings = new XmlWriterSettings { ConformanceLevel = ConformanceLevel.Auto, OmitXmlDeclaration = omitXmlDeclaration };

            using (var htmlWriter = XmlWriter.Create(htmlBuilder, writerSettings))
            using (var sr = new StringReader(xml))
            using (var xmlReader = XmlReader.Create(sr, new XmlReaderSettings { DtdProcessing = DtdProcessing.Parse }))
            {
                // transform the xml
                xslTransformer.Transform(xmlReader, htmlWriter);
            }

            return htmlBuilder;
        }

        /// <summary>
        /// Creates and returns an <see cref="XslCompiledTransform"/> object which uses the <paramref name="xsl"/> stylesheet content for transforming Cda files
        /// </summary>
        /// <param name="xsl">String representation of an Xml Stylesheet</param>
        /// <returns>An <see cref="XslCompiledTransform"/> that can transform xml files</returns>
        private static XslCompiledTransform CreateTransformer(string xsl)
        {
#if DEBUG
            // create a transformer
            var xslTransformer = new XslCompiledTransform(true);
#elif !DEBUG
            var xslTransformer = new XslCompiledTransform(false);
#endif

            // load the stylesheet
            using (var sr = new StringReader(xsl))
            using (var xmlReader = XmlReader.Create(sr))
            {
                xslTransformer.Load(xmlReader, new XsltSettings(true, false), new XmlUrlResolver());
            }

            return xslTransformer;
        }
    }

    /// <summary>
    /// Helper class for searching and filtering an Xml document
    /// </summary>
    public static class XmlParserHelper
    {
        public static bool IsValidXml(string xml)
        {
            try
            {
                GetXDocument(xml);
            }
            catch (XmlException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Recursively searches (Breadth First) through the elements in the <see cref="xmlDocument"/> to look for the <see cref="elementName"/>
        /// </summary>
        /// <param name="xmlDocument"></param>
        /// <param name="elementName"></param>
        /// <returns></returns>
        public static IEnumerable<XElement> FindElements(XElement xmlDocument, string elementName)
        {
            if (xmlDocument.Name.LocalName.Equals(elementName, StringComparison.OrdinalIgnoreCase)) return new[] {xmlDocument};

            var elements = xmlDocument.Elements().ToList();

            var foundElements = elements.Where(element => element.Name.LocalName.Equals(elementName, StringComparison.OrdinalIgnoreCase)).ToList();

            if (foundElements.IsNotNullOrEmpty()) return foundElements;

            foreach (var element in elements)
            {
                return FindElements(element, elementName);
            }

            return null;
        }

        /// <summary>
        /// Recursively searches through the elements in the <see cref="xmlDocument"/> to look for the <see cref="elementName"/>
        /// </summary>
        /// <param name="xmlDocument"></param>
        /// <param name="elementName"></param>
        /// <returns></returns>
        public static bool DoesElementExist(string xmlDocument, string elementName)
        {
            var doc = GetXDocument(xmlDocument);

            return FindElements(doc.Root, elementName).IsNotNullOrEmpty();
        }

        /// <summary>
        /// Recursively searches through the elements in the <see cref="xmlDocument"/> to look for the <see cref="elementName"/> and see if it has a 
        /// matching attribute with <see cref="attributeName"/>
        /// </summary>
        /// <param name="xmlDocument">An xml document in string form</param>
        /// <param name="elementName">The name of the element to locate</param>
        /// <param name="attributeName">The name of the attribute to locate within the element</param>
        /// <param name="attributeValue">Optional. If supplied, will try to find the attribute with the <see cref="attributeValue"/> value.  Note: value is case-sensitive</param>
        /// <returns></returns>
        public static bool DoesElementAndAttributeExist(string xmlDocument, string elementName, string attributeName, string attributeValue = null)
        {
            var doc = GetXDocument(xmlDocument);
            var elements = FindElements(doc.Root, elementName).ToList();

            // check each found element
            foreach (var element in elements)
            {
                if (element.Attributes().Any(attribute => attribute.Name.LocalName.Equals(attributeName, StringComparison.OrdinalIgnoreCase) 
                    && (attributeValue == null || attribute.Value.Equals(attributeValue))))
                {
                    return true;
                }
            }              

            return false;
        }

        public static XDocument GetXDocument(string xmlDocument)
        {
            var doc = XDocument.Parse(xmlDocument, LoadOptions.None);

            return doc;
        }

    }
}
