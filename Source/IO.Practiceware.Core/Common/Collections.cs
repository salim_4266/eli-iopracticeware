﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware
{    

    /// <summary>
    /// 
    /// </summary>
    public static class Collections
    {
        /// <summary>
        /// Enumerates exclusive pairs of combinations (without repetition). 
        /// 
        /// e.g.  for a set of integers {3, 4, 5, 6}, will enumerate the following
        /// pairs: {3,4}, {3,5}, {3,6}, {4,5}, {4,6}, {5,6}
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>        
        public static IEnumerable<Tuple<T,T>> Combinations<T>(this IEnumerable<T> source)
        {        
            var counter1 = 0;
            var counter2 = 0;
            foreach (var item1 in source)
            {
                foreach (var item2 in source)
                {
                    if (counter1 < counter2)
                    {
                        yield return new Tuple<T, T>(item1, item2);                        
                    }
                    counter2++;
                }
                counter1++;
                counter2 = 0;
            }
        }  


        /// <summary>
        /// Enumerates exclusive pairs of permutations (without repetition). 
        /// 
        /// Permutation definition: for a set of integers {3, 4, 5}, will enumerate the following
        /// pairs: {3,4}, {3,5}, {4,3}, {4,5}, {5,3}, {5,4}
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>        
        public static IEnumerable<Tuple<T, T>> Permutations<T>(this IEnumerable<T> source)
        {
            var counter1 = 0;
            var counter2 = 0;
            foreach (var item1 in source)
            {
                foreach (var item2 in source)
                {
                    if (counter1 != counter2)
                    {
                        yield return new Tuple<T, T>(item1, item2);
                    }
                    counter2++;
                }
                counter1++;
                counter2 = 0;
            }                                 
        }

        /// <summary>
        /// Returns a new list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IList<T> RemoveWhere<T>(this IList<T> source, Func<T, bool> predicate)
        {
            if (source == null) return null;

            var itemsToRemove = source.Where(predicate).ToList();
            foreach (var item in itemsToRemove)
            {
                source.Remove(item);
            }
            return source;
        }        
    }
}
