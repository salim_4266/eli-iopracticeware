﻿using System.Runtime.Serialization;

namespace IO.Practiceware
{
    /// <summary>
    /// Wraps a enum / or any value type in a class.  This is a stand in for nullable types because our current implementation of protobuf and wcf 
    /// does not handle nullable enums very well.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract]
    public class NullableEnumWrapper<T> where T : struct
    {
        public NullableEnumWrapper()
        {
            
        }

        public NullableEnumWrapper(T value)
        {
            Value = value;
        }

        [DataMember]
        public T Value { get; set; }
    }
}
