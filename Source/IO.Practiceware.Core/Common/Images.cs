﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Encoder = System.Drawing.Imaging.Encoder;

namespace IO.Practiceware
{
    /// <summary>
    /// General helper class that provides utilities for Images
    /// </summary>
    public static class Images
    {
        /// <summary>
        /// Takes a Bitmap image and converts it to a <see cref="BitmapSource"/> which can be used natively by Wpf.
        /// </summary>
        /// <param name="image"></param>
        /// <returns>A <see cref="BitmapSource"/> that represents the current screen. BitmapSource objects are used by Wpf</returns>
        public static BitmapSource ToBitmapSource(this Bitmap image)
        {
            var hBitmap = image.GetHbitmap();

            try
            {
                return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    hBitmap,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());
            }
            catch (Win32Exception)
            {
                return null;
            }
            finally
            {
                GdiImports.DeleteObject(hBitmap);
            }

        }

        /// <summary>
        /// Retrieves the <see cref="ImageCodecInfo"/> information given the specified format
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public static ImageCodecInfo GetEncoder(this ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        /// <summary>
        /// Encodes the image as the specified format, using the specified color conversion and quality level.
        /// </summary>
        /// <param name="image">The image</param>
        /// <param name="imageFormat">The image format.</param>
        /// <param name="qualityLevel">The quality.</param>
        /// <param name="colorConversion">The color conversion.</param>
        /// <returns></returns>
        public static byte[] ToByteArray(this Image image, ImageFormat imageFormat, long? qualityLevel = null, ColorConversion colorConversion = ColorConversion.None)
        {
            using (var ms = new MemoryStream())
            {
                image.Save(ms, imageFormat, qualityLevel, colorConversion);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Encodes the image as the specified format, using the specified color conversion and quality level.
        /// </summary>
        /// <param name="image">The image</param>
        /// <param name="destination">The destination.</param>
        /// <param name="imageFormat">The image format.</param>
        /// <param name="qualityLevel">The quality.</param>
        /// <param name="colorConversion">The color conversion.</param>
        public static void Save(this Image image, Stream destination, ImageFormat imageFormat, long? qualityLevel = null, ColorConversion colorConversion = ColorConversion.None)
        {
            var codec = imageFormat.GetEncoder();

            var original = image;

            try
            {

                if (image == null)
                {
                    throw new InvalidOperationException();
                }

                var encoderParameters = new List<EncoderParameter>();
                if (qualityLevel != null)
                {
                    encoderParameters.SetQuality(qualityLevel.Value);
                }

                if (colorConversion != ColorConversion.None)
                {
                    switch (colorConversion)
                    {
                        case ColorConversion.Grayscale:
                            encoderParameters.Add(new EncoderParameter(Encoder.ColorDepth, 8L));
                            image = image.ToGrayscale();
                            break;
                        case ColorConversion.BlackAndWhite:
                            encoderParameters.Add(new EncoderParameter(Encoder.ColorDepth, 1L));
                            image = image.ToBlackAndWhite();
                            break;
                    }
                }

                if (encoderParameters.Count > 0)
                {
                    image.Save(destination, codec, encoderParameters.ToEncoderParametersObject());
                }
                else
                {
                    image.Save(destination, imageFormat);
                }
            }
            finally
            {
                if (!ReferenceEquals(image, original) && image != null) image.Dispose();
            }
        }

        /// <summary>
        /// Returns a new black and white version of the image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns></returns>
        public static Bitmap ToBlackAndWhite(this Image image)
        {
            var bitmap = new Bitmap(image.Width, image.Height, image.PixelFormat);
            using (Graphics gr = Graphics.FromImage(bitmap))
            {
                var grayMatrix = new[]
                    {
                        new[] {0.299f, 0.299f, 0.299f, 0, 0},
                        new[] {0.587f, 0.587f, 0.587f, 0, 0},
                        new[] {0.114f, 0.114f, 0.114f, 0, 0},
                        new[] {0f, 0, 0, 1, 0},
                        new[] {0f, 0, 0, 0, 1}
                    };

                var ia = new ImageAttributes();
                ia.SetColorMatrix(new ColorMatrix(grayMatrix));
                ia.SetThreshold((float)0.8); // Change this threshold as needed
                var rc = new Rectangle(0, 0, image.Width, image.Height);
                gr.DrawImage(image, rc, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, ia);
            }
            return bitmap;
        }

        /// <summary>
        /// Returns a new grayscale version of the image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns></returns>
        public static Bitmap ToGrayscale(this Image image)
        {
            var bitmap = new Bitmap(image.Width, image.Height, image.PixelFormat);
            using (Graphics gr = Graphics.FromImage(bitmap))
            {
                var grayMatrix = new[]
                    {
                        new[] {.3f, .3f, .3f, 0, 0},
                        new[] {.59f, .59f, .59f, 0, 0},
                        new[] {.11f, .11f, .11f, 0, 0},
                        new[] {0f, 0, 0, 1, 0},
                        new[] {0f, 0, 0, 0, 1}
                    };

                var ia = new ImageAttributes();
                ia.SetColorMatrix(new ColorMatrix(grayMatrix));
                ia.SetThreshold((float)0.8); // Change this threshold as needed
                var rc = new Rectangle(0, 0, image.Width, image.Height);
                gr.DrawImage(image, rc, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, ia);
            }
            return bitmap;
        }

        /// <summary>
        /// Compresses jpegs
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The destination path.</param>
        /// <param name="qualityLevel">The compression level.</param>
        /// <param name="colorConversion">The color conversion.</param>
        /// <returns></returns>
        public static bool TryCompressJpeg(string sourcePath, string destinationPath, int qualityLevel, ColorConversion colorConversion = ColorConversion.None)
        {
            try
            {
                using (var fs = new FileStream(sourcePath, FileMode.Open, FileAccess.ReadWrite))
                using (var image = Image.FromStream(fs))
                using (var fsOut = new FileStream(destinationPath, FileMode.Create))
                {
                    image.Save(fsOut, ImageFormat.Jpeg, qualityLevel, colorConversion);
                }
                return true;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// Compresses all JPEGs in a PDF
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The destination path.</param>
        /// <param name="qualityLevel">The quality level.</param>
        /// <param name="?">The ?.</param>
        /// <param name="colorConversion">The color conversion.</param>
        /// <returns></returns>
        public static bool TryCompressPdfImages(string sourcePath, string destinationPath, int? qualityLevel = null, ColorConversion colorConversion = ColorConversion.None)
        {
            using (var source = new FileStream(sourcePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var destination = new FileStream(destinationPath, FileMode.Create))
            {
                return TryCompressPdfImages(source, destination, qualityLevel, colorConversion);
            }
        }

        /// <summary>
        /// Compresses all JPEGs in a PDF
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <param name="qualityLevel">The quality level.</param>
        /// <param name="colorConversion">The color conversion.</param>
        /// <returns></returns>
        public static bool TryCompressPdfImages(Stream source, Stream destination, int? qualityLevel = null, ColorConversion colorConversion = ColorConversion.None)
        {
            using (var reader = new PdfReader(source))
            {
                using (var stamper = new PdfStamper(reader, destination, PdfWriter.VERSION_1_5))
                {
                    if (!TryCompressPdfImages(reader, qualityLevel, colorConversion))
                    {
                        return false;
                    }
                    stamper.SetFullCompression();
                }
                return true;
            }
        }


        /// <summary>
        /// Tries to compress JPEGs from PdfReader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="qualityLevel">The quality level.</param>
        /// <param name="colorConversion">The color conversion.</param>
        /// <returns></returns>
        private static bool TryCompressPdfImages(PdfReader reader, int? qualityLevel = null, ColorConversion colorConversion = ColorConversion.None)
        {
            try
            {
                int n = reader.NumberOfPages;
                for (int i = 1; i <= n; i++)
                {
                    try
                    {

                        PdfDictionary resources = reader.GetPageN(i).GetAsDict(PdfName.RESOURCES);
                        PdfDictionary xobjects = resources.GetAsDict(PdfName.XOBJECT);

                        if (xobjects == null) continue;

                        foreach (var imgRef in xobjects.Keys)
                        {
                            var stream = (PRStream)xobjects.GetAsStream(imgRef);

                            if (!Equals(stream.Get(PdfName.SUBTYPE), PdfName.IMAGE)) continue;

                            var pdfImageObject = new PdfImageObject(stream);

                            using (var drawingImage = pdfImageObject.GetDrawingImage())
                            using (var drawingImageMemoryStream = new MemoryStream())
                            using (var pdfImageMemoryStream = new MemoryStream())
                            {
                                if (drawingImage == null) continue;

                                var imageFormat = colorConversion == ColorConversion.BlackAndWhite ? ImageFormat.Png : ImageFormat.Jpeg;

                                drawingImage.Save(drawingImageMemoryStream, imageFormat, qualityLevel, colorConversion);

                                var compressedPdfImage = new PdfImage(iTextSharp.text.Image.GetInstance(drawingImageMemoryStream.ToArray()), string.Empty, null);

                                compressedPdfImage.WriteContent(pdfImageMemoryStream);

                                var oldStreamValues = stream.Keys.ToDictionary(k => k, stream.Get);

                                stream.Clear();

                                stream.SetData(pdfImageMemoryStream.ToArray(), false);

                                if (imageFormat.Guid == ImageFormat.Jpeg.Guid && drawingImage.RawFormat.Guid == ImageFormat.Jpeg.Guid && colorConversion == ColorConversion.None)
                                {
                                    // use old values since we're not changing format at all
                                    oldStreamValues.ToList().ForEach(p => stream.Put(p.Key, p.Value));
                                }
                                else
                                {
                                    // we're changing the image format
                                    foreach (PdfName name in compressedPdfImage.Keys)
                                    {
                                        stream.Put(name, compressedPdfImage.Get(name));
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError(ex.ToString());
                    }
                    finally
                    {
                        // may or may not help      
                        reader.RemoveUnusedObjects();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                return false;
            }
        }


        #region Private Helper Methods

        /// <summary>
        /// Adds a <see cref="Encoder.Quality"/> parameter to the list of parameters with the specified quality Level
        /// </summary>
        /// <param name="encoderParameters"></param>
        /// <param name="qualityLevel">Specifies the level quality for an image. The valid range of values are 0-100.  Zero will give you the lowest quality image and 100 the highest</param>
        /// <returns></returns>
        public static List<EncoderParameter> SetQuality(this List<EncoderParameter> encoderParameters, long qualityLevel)
        {
            return encoderParameters.SetEncoderParameter(Encoder.Quality, qualityLevel);
        }

        /// <summary>
        /// Converts the list of EncoderParameter objects to an EncoderParameters object
        /// </summary>
        /// <param name="encoderParameters"></param>
        /// <returns></returns>
        public static EncoderParameters ToEncoderParametersObject(this List<EncoderParameter> encoderParameters)
        {
            var p = new EncoderParameters(encoderParameters.Count);
            p.Param = encoderParameters.ToArray();
            return p;
        }

        /// <summary>
        /// General purpose set method that adds a new EncoderParameter with the specified Encoder and either a long value, or an EncoderValue enum value
        /// </summary>
        /// <param name="encoderParameters"></param>
        /// <param name="encoder"></param>
        /// <param name="value"></param>
        /// <param name="encoderValue"></param>
        /// <returns></returns>
        private static List<EncoderParameter> SetEncoderParameter(this List<EncoderParameter> encoderParameters, Encoder encoder, long? value = null, EncoderValue? encoderValue = null)
        {
            if (encoderParameters == null)
            {
                encoderParameters = new List<EncoderParameter>();
            }

            EncoderParameter encoderParameter;
            if (value.HasValue) encoderParameter = new EncoderParameter(encoder, value.Value);
            else if (encoderValue.HasValue) encoderParameter = new EncoderParameter(encoder, (long)encoderValue.Value);
            else return encoderParameters;

            encoderParameters.Add(encoderParameter);
            return encoderParameters;
        }

        #endregion

    }

    public enum ColorConversion
    {
        None,
        BlackAndWhite,
        Grayscale
    }

}
