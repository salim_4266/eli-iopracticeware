﻿namespace IO.Practiceware
{
    public enum LogicalOperator
    {
        All = 0,
        Any,
        LessThan,
        GreaterThan,
        EqualTo,
        NotEqualTo,
        GreaterThanOrEqualTo,
        LessThanOrEqualTo
    }
}
