using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security;

namespace IO.Practiceware
{
    public static class Networking
    {
        #region Dll Imports

        [DllImport("netapi32.dll", CharSet = CharSet.Auto)]
        private static extern int NetWkstaGetInfo(string server,
                                                  int level,
                                                  out IntPtr info);

        [DllImport("Netapi32", CharSet = CharSet.Auto, SetLastError = true), SuppressUnmanagedCodeSecurity]
        public static extern int NetServerEnum(
            string serverNane,
            int dwLevel,
            ref IntPtr pBuf,
            int dwPrefMaxLen,
            out int dwEntriesRead,
            out int dwTotalEntries,
            int dwServerType,
            string domain,
            out int dwResumeHandle
            );

        [DllImport("Netapi32", SetLastError = true), SuppressUnmanagedCodeSecurity]
        public static extern int NetApiBufferFree(IntPtr pBuf);

        [StructLayout(LayoutKind.Sequential)]
        public struct ServerInfo100
        {
            internal int sv100_platform_id;
            [MarshalAs(UnmanagedType.LPWStr)] internal string sv100_name;
        }

        #endregion

        /// <summary>
        ///   Uses the DllImport : NetServerEnum with all its required parameters (see http://msdn.microsoft.com/library/default.asp?url=/library/en-us/netmgmt/netmgmt/netserverenum.asp for full details or method signature) to retrieve a list of domain SV_TYPE_WORKSTATION and SV_TYPE_SERVER PC's
        /// </summary>
        /// <returns> Arraylist that represents all the SV_TYPE_WORKSTATION and SV_TYPE_SERVER PC's in the Domain </returns>
        public static string[] GetNetworkComputers()
        {
            var networkComputers = new List<string>();
            const int maxPreferredLength = -1;
            const int svTypeWorkstation = 1;
            const int svTypeServer = 2;
            IntPtr buffer = IntPtr.Zero;
            int sizeofInfo = Marshal.SizeOf(typeof (ServerInfo100));

            try
            {
                //call the DllImport : NetServerEnum with all its required parameters
                //see http://msdn.microsoft.com/library/default.asp?url=/library/en-us/netmgmt/netmgmt/netserverenum.asp
                //for full details of method signature
                int entriesRead;
                int resHandle;
                int totalEntries;
                int ret = NetServerEnum(null, 100, ref buffer, maxPreferredLength,
                                        out entriesRead,
                                        out totalEntries, svTypeWorkstation | svTypeServer, null, out
                                                                                                      resHandle);
                //if the returned with a NERR_Success (C++ term), =0 for C#
                if (ret == 0)
                {
                    //loop through all SV_TYPE_WORKSTATION and SV_TYPE_SERVER PC's
                    for (int i = 0; i < totalEntries; i++)
                    {
                        //get pointer to, Pointer to the buffer that received the data from
                        //the call to NetServerEnum. Must ensure to use correct size of 
                        //STRUCTURE to ensure correct location in memory is pointed to
                        var tmpBuffer = new IntPtr((int) buffer + (i*sizeofInfo));
                        //Have now got a pointer to the list of SV_TYPE_WORKSTATION and 
                        //SV_TYPE_SERVER PC's, which is unmanaged memory
                        //Needs to Marshal data from an unmanaged block of memory to a 
                        //managed object, again using STRUCTURE to ensure the correct data
                        //is marshalled 
                        var svrInfo = (ServerInfo100)
                                      Marshal.PtrToStructure(tmpBuffer, typeof (ServerInfo100));

                        //add the PC names to the ArrayList
                        networkComputers.Add(svrInfo.sv100_name);
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(
                    new Exception(@"Problem with acessing network computers in NetworkBrowser.", ex).ToString());
                return new string[0];
            }
            finally
            {
                //The NetApiBufferFree function frees 
                //the memory that the NetApiBufferAllocate function allocates
                NetApiBufferFree(buffer);
            }
            //return entries found
            return networkComputers.ToArray();
        }

        public static string GetMachineNetBiosDomain()
        {
            IntPtr pBuffer;

            int retval = NetWkstaGetInfo(null, 100, out pBuffer);
            if (retval != 0)
                throw new Win32Exception(retval);

            var info = (WKSTA_INFO_100)Marshal.PtrToStructure(pBuffer, typeof(WKSTA_INFO_100));
            string domainName = info.wki100_langroup;
            NetApiBufferFree(pBuffer);
            return domainName;
        }

        public static bool IsInDomain()
        {
            // ReSharper disable RedundantAssignment
            Win32.NetJoinStatus status = Win32.NetJoinStatus.NetSetupUnknownStatus;
            // ReSharper restore RedundantAssignment
            IntPtr pDomain;
            int result = Win32.NetGetJoinInformation(null, out pDomain, out status);
            if (pDomain != IntPtr.Zero)
            {
                NetApiBufferFree(pDomain);
            }
            if (result == Win32.ErrorSuccess)
            {
                return status == Win32.NetJoinStatus.NetSetupDomainName;
            }
            throw new Exception("Domain Info Get Failed");
        }

        [DebuggerNonUserCode]
        public static bool IsLocalhost(string hostName)
        {
            if (string.IsNullOrEmpty(hostName))
                return false;

            try
            {
                IPAddress[] hostIPs = Dns.GetHostAddresses(hostName);
                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
                return hostIPs.Any(i => IPAddress.IsLoopback(i) || localIPs.Contains(i));
            }
            catch
            {
                return false;
            }
        }

        #region Nested type: WKSTA_INFO_100

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        // ReSharper disable InconsistentNaming
        private class WKSTA_INFO_100
        // ReSharper restore InconsistentNaming
        {
#pragma warning disable 169
#pragma warning disable 649
            public int wki100_platform_id;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string wki100_computername;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string wki100_langroup;
            public int wki100_ver_major;
            public int wki100_ver_minor;
#pragma warning restore 169
#pragma warning restore 649
        }

        #endregion

        internal class Win32
        {
            public const int ErrorSuccess = 0;

            [DllImport("Netapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            public static extern int NetGetJoinInformation(string server, out IntPtr domain, out NetJoinStatus status);

            public enum NetJoinStatus
            {
                NetSetupUnknownStatus = 0,
                NetSetupUnjoined,
                NetSetupWorkgroupName,
                NetSetupDomainName
            }

        }
    }
}