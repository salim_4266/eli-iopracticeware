using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace IO.Practiceware
{
    public static class Arithmetic
    {
        public static double ConvertFeetToMeter(this double feet)
        {
            return (feet * 0.3048);
        }

        public static double ConvertInchToCm(this double inch)
        {
            return (inch * 2.54);
        }

        public static double ConvertMeterToFeet(this double meter)
        {
            return (meter * 3.281);
        }

        public static double ConvertCmToInch(this double cm)
        {
            return (cm * 0.3937);
        }

        public static double ConvertPoundToKg(this double pound)
        {
            return (pound * 0.453592);
        }

        public static double ConvertOunceToGram(this double ounce)
        {
            return (ounce * 28.35);
        }

        public static double ConvertKgToPound(this double kg)
        {
            return (kg * 2.205);
        }

        public static double ConvertGramToOunce(this double gram)
        {
            return (gram * 0.03527);
        }

        public static double ConvertMeterToCm(this double meter)
        {
            return (meter * 100);
        }

        public static double ConvertCmToMeter(this double cm)
        {
            return (cm / 100);
        }

        public static double ConvertFeetToInch(this double feet)
        {
            return (feet * 12);
        }

        public static double ConvertInchToFeet(this double inch)
        {
            return (inch / 12);
        }

        public static double ConvertKgsToGrams(this double kgs)
        {
            return (kgs * 1000);
        }

        public static double ConvertGramsToKgs(this double grams)
        {
            return (grams / 1000);
        }

        public static double ConvertPoundToOunces(this double pound)
        {
            return (pound * 16);
        }

        public static double ConvertOuncesToPound(this double ounces)
        {
            return (ounces / 16);
        }

        public static void ConvertFeetInchToMeterCm(double feet, double inch, out double meter, out double cm)
        {
            meter = feet.ConvertFeetToMeter();
            cm = meter.ConvertMeterToCm() + inch.ConvertInchToCm();
            meter = cm.ConvertCmToMeter();
            cm = meter - Math.Floor(meter);
            meter = meter - cm;
            cm = cm.ConvertMeterToCm();
        }

        public static void ConvertMeterCmToFeetInch(double meter, double cm, out double feet, out double inch)
        {
            feet = meter.ConvertMeterToFeet();
            inch = Math.Round(feet.ConvertFeetToInch() + cm.ConvertCmToInch());
            feet = inch.ConvertInchToFeet();
            inch = feet - Math.Floor(feet);
            feet = feet - inch;
            inch = Math.Round(inch.ConvertFeetToInch());
        }

        public static void ConvertLbsOuncesToKgsGrams(double lbs, double ounces, out double kgs, out double grams)
        {
            kgs = lbs.ConvertPoundToKg();
            grams = kgs.ConvertKgsToGrams() + ounces.ConvertOunceToGram();
            kgs = grams.ConvertGramsToKgs();
            grams = kgs - Math.Floor(kgs);
            kgs = kgs - grams;
            grams = Math.Round(grams.ConvertKgsToGrams());
        }

        public static void ConvertKgsGramsToLbsOunces(double kgs, double grams, out double lbs, out double ounces)
        {
            lbs = kgs.ConvertKgToPound();
            ounces = Math.Round((lbs.ConvertPoundToOunces()) + grams.ConvertGramToOunce());
            lbs = ounces.ConvertOuncesToPound();
            ounces = lbs - Math.Floor(lbs);
            lbs = lbs - ounces;
            ounces = Math.Round(ounces.ConvertPoundToOunces());
        }

        public static double GetBodyMassIndex(this double kgs, double meters)
        {
            return (double) GetBodyMassIndex((decimal) kgs, (decimal) meters);
        }

        public static decimal GetBodyMassIndex(this decimal kgs, decimal meters)
        {
            if (meters > 0)
            {
                return kgs / (meters * meters);
            }
            return 0;
        }

        public static bool IsWholeNumber(this decimal value)
        {
            return decimal.Round(value, 0).Equals(value);
        }

        public static bool IsWholeNumber(this double value)
        {
            return Math.Round(value, 0).Equals(value);
        }

        public static byte[] ComputeSha1Hash(this byte[] value)
        {
            using (var md5 = SHA1.Create())
            using (var readStream = new MemoryStream(value))
            {
                return md5.ComputeHash(readStream);
            }
        }

        public static byte[] ComputeSha1Hash(this Stream source)
        {
            using (var md5 = SHA1.Create())
                return md5.ComputeHash(source);
        }

        public static byte[] ComputeSha1Hash(this string source)
        {
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(source)))
                return ms.ComputeSha1Hash();
        }

        public static byte[] ComputeMd5Hash(this byte[] value)
        {
            using (var md5 = MD5.Create())
            using (var readStream = new MemoryStream(value))
            {
                return md5.ComputeHash(readStream);
            }
        }

        public static byte[] ComputeMd5Hash(this Stream source)
        {
            using (var md5 = MD5.Create())
                return md5.ComputeHash(source);
        }

        public static byte[] ComputeMd5Hash(this string source)
        {
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(source)))
                return ms.ComputeMd5Hash();
        }
    }
}