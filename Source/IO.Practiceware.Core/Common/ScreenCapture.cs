﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows;
using IO.Practiceware.Storage;

namespace IO.Practiceware
{
    /// <summary>
    ///   Provides various utilities to capture a screenshot of the current window
    /// </summary>
    public static class ScreenCapture
    {
        /// <summary>
        ///   Creates a Bitmap and uses the System.Drawing.Graphics library (Gdi+) to capture the current screen.  This may be more stable 
        ///   but less performant as it uses the Gdi+ library which is a wrapper around Gdi32.  
        ///   May also cause issues with current logged in Window user permissions, since the Graphics
        ///   library performs a Permissions check.
        /// </summary>
        /// <returns> A <see cref="Bitmap" /> that represents the current screen. </returns>
        public static Bitmap CaptureScreen()
        {
            var screenWidth = (int) SystemParameters.PrimaryScreenWidth;
            var screenHeight = (int) SystemParameters.PrimaryScreenHeight;

            return CaptureScreenArea(0, 0, screenWidth, screenHeight);
        }

        /// <summary>
        /// Captures a portion of screen area using Gdi
        /// </summary>
        /// <param name="startX">The start x.</param>
        /// <param name="startY">The start y.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns></returns>
        public static Bitmap CaptureScreenArea(double startX, double startY, double width, double height)
        {
            // Convert to ints
            var intWidth = Convert.ToInt32(width);
            var intHeight = Convert.ToInt32(height);
            var sourceX = Convert.ToInt32(startX);
            var sourceY = Convert.ToInt32(startY);

            return CaptureScreenArea(sourceX, sourceY, intWidth, intHeight);
        }

        /// <summary>
        /// Captures a portion of screen area using Gdi
        /// </summary>
        /// <param name="startX">The start x.</param>
        /// <param name="startY">The start y.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns></returns>
        public static Bitmap CaptureScreenArea(int startX, int startY, int width, int height)
        {
            // Bitmap in right size
            var screenshot = new Bitmap(width, height, PixelFormat.Format32bppArgb);

            using (var g = Graphics.FromImage(screenshot))
            {
                // snip wanted area
                g.CopyFromScreen(startX, startY, 0, 0, screenshot.Size);
            }

            return screenshot;
        }

        /// <summary>
        ///   Creates a Bitmap and uses Win32 libraries to capture the current screen.  May be less stable 
        ///   but much more performant.  May also bypass Window user permissions issues.
        /// </summary>
        /// <returns> </returns>
        public static Bitmap CaptureScreenWin32()
        {
            return CaptureScreenWin32(0, 0, Win32Imports.GetSystemMetrics(Win32Imports.SM_CXSCREEN), 
                Win32Imports.GetSystemMetrics(Win32Imports.SM_CYSCREEN));
        }

        /// <summary>
        ///   Creates a Bitmap and uses Win32 libraries to capture the current screen.  May be less stable 
        ///   but much more performant.  May also bypass Window user permissions issues.
        /// </summary>
        /// <returns> </returns>
        public static Bitmap CaptureScreenWin32(int startX, int startY, int width, int height)
        {
            SIZE size;
            IntPtr hDc = Win32Imports.GetDC(Win32Imports.GetDesktopWindow());
            IntPtr hMemDc = GdiImports.CreateCompatibleDC(hDc);

            size.Cx = width;
            size.Cy = height;

            IntPtr hBitmap = GdiImports.CreateCompatibleBitmap(hDc, size.Cx, size.Cy);

            if (hBitmap != IntPtr.Zero)
            {
                IntPtr hOld = GdiImports.SelectObject(hMemDc, hBitmap);

                GdiImports.BitBlt(hMemDc, 0, 0, size.Cx, size.Cy, hDc, startX, startY, GdiImports.SRCCOPY);

                GdiImports.SelectObject(hMemDc, hOld);
                GdiImports.DeleteDC(hMemDc);
                Win32Imports.ReleaseDC(Win32Imports.GetDesktopWindow(), hDc);
                Bitmap bmp = Image.FromHbitmap(hBitmap);
                GdiImports.DeleteObject(hBitmap);
                //GC.Collect();  // Not sure we really want to do this
                return bmp;
            }

            return null;
        }

        /// <summary>
        ///   Captures an image of the cursor on the desktop
        /// </summary>
        /// <param name="x"> </param>
        /// <param name="y"> </param>
        /// <returns> </returns>
        public static Bitmap CaptureCursor(ref int x, ref int y)
        {
            var ci = new Win32Imports.CURSORINFO();
            ci.cbSize = Marshal.SizeOf(ci);

            if (Win32Imports.GetCursorInfo(out ci))
            {
                if (ci.flags == Win32Imports.CURSOR_SHOWING)
                {
                    IntPtr hicon = Win32Imports.CopyIcon(ci.hCursor);
                    Win32Imports.ICONINFO icInfo;
                    if (Win32Imports.GetIconInfo(hicon, out icInfo))
                    {
                        x = ci.ptScreenPos.x - icInfo.xHotspot;
                        y = ci.ptScreenPos.y - icInfo.yHotspot;

                        Icon ic = Icon.FromHandle(hicon);
                        Bitmap bmp = ic.ToBitmap();
                        return bmp;
                    }
                }
            }

            return null;
        }

        /// <summary>
        ///   Captures the desktop image including the cursor in the position it held at the time of the capture. Uses a combination
        ///   of Win32 library functions (for Desktop Image and mouse capture) and Gdi32 functions (for drawing)
        /// </summary>
        /// <returns> </returns>
        public static Bitmap CaptureScreenWithCursorWin32(int startX, int startY, int width, int height)
        {
            int cursorX = 0;
            int cursorY = 0;

            Bitmap desktopBmp = CaptureScreenWin32(startX, startY, width, height);
            using (Bitmap cursorBmp = CaptureCursor(ref cursorX, ref cursorY))
            {
                if (desktopBmp != null)
                {
                    if (cursorBmp != null)
                    {
                        var r = new Rectangle(cursorX, cursorY, cursorBmp.Width, cursorBmp.Height);
                        Graphics g = Graphics.FromImage(desktopBmp);
                        g.DrawImage(cursorBmp, r);
                        g.Flush();

                        return desktopBmp;
                    }
                    return desktopBmp;
                }
            }

            return null;
        }

        /// <summary>
        /// Captures and saves screen shot to specified file location
        /// </summary>
        /// <param name="imageFilePath"></param>
        public static void CaptureAndSaveScreen(string imageFilePath)
        {
            var bmpImage = CaptureScreen();
            FileManager.Instance.CommitWrite(imageFilePath, bmpImage.Save);
        }

        /// <summary>
        /// Attempts to captures and saves screen shot to specified file location
        /// </summary>
        public static void TryCaptureAndSaveScreen(string imageFilePath)
        {
            try
            {
                CaptureAndSaveScreen(imageFilePath);
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }
    }

}