﻿using System.Text;
using Soaf;
using Soaf.Reflection;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace IO.Practiceware
{
    public static class Strings
    {
        /// <summary>
        /// BOM. In .NET it is represented with a single character (#FEFF)
        /// </summary>
        private static readonly char ByteOrderMarkUtf8 = Encoding.UTF8.GetChars(Encoding.UTF8.GetPreamble()).Single();

        public const string SocialSecurityNumberRegex = @"(\d{3}\-\d{2}\-\d{4})|(\d{9})|(___-__-____)";

        /// <summary>
        /// Truncates the string properties of an object based on the maximum string length of the field as specified in the StringLengthAttribute of the property.
        /// </summary>
        /// <param name="instance">The instance of the object.</param>
        /// <returns></returns>
        public static object TruncateStringProperties(this object instance)
        {
            foreach (var property in instance.GetType()
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Select(a => new { Property = a, Attribute = a.GetAttribute<StringLengthAttribute>() })
                .Where(a => a.Property.PropertyType == typeof(string) && a.Attribute != null && a.Property.CanWrite && a.Property.CanRead))
            {
                var currentValue = property.Property.GetValue(instance, null) as String;

                if (currentValue != null && currentValue.Length > property.Attribute.MaximumLength)
                {
                    currentValue = currentValue.Truncate(property.Attribute.MaximumLength);
                    property.Property.SetValue(instance, currentValue, null);
                }
            }

            return instance;
        }

        /// <summary>
        /// Trims the string properties of an object.  Uses reflection to get and set the properties.
        /// </summary>
        /// <param name="instance">The object to modify</param>
        /// <returns>The same object with it's string properties trimmed.</returns>
        public static object TrimStringProperties(this object instance)
        {
            foreach (var property in instance.GetType()
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(a => a.PropertyType == typeof(string) && a.CanRead && a.CanWrite))
            {
                var currentValue = property.GetValue(instance, null) as string;
                if (currentValue != null)
                {
                    currentValue = currentValue.Trim();
                    property.SetValue(instance, currentValue, null);
                }
            }

            return instance;
        }

        /// <summary>
        /// Modifies the string properties of an object so they are changed to all uppper case letters. Uses reflection to get and set the properties.
        /// </summary>
        /// <param name="instance">The object to modify</param>
        /// <returns></returns>
        public static object ToUpperStringProperties(this object instance)
        {
            foreach (var property in instance.GetType()
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(a => a.PropertyType == typeof(string) && a.CanRead && a.CanWrite))
            {
                var currentValue = property.GetValue(instance, null) as string;
                if (currentValue != null)
                {
                    currentValue = currentValue.ToUpper();
                    property.SetValue(instance, currentValue, null);
                }

            }

            return instance;
        }

        public static readonly Func<char, bool> IsAlphanumeric = character => Char.IsNumber(character) || Char.IsLetter(character);
        public static readonly Func<char, bool> IsNotAlphanumeric = character => !IsAlphanumeric(character);

        /// <summary>
        /// Removes characters from the string based on a specified predicate.
        /// </summary>
        /// <param name="s">The string to modify</param>
        /// <param name="predicate">The predicate which determines whether a character should be removed</param>
        /// <returns>A new string without the characters specified in <see cref="predicate"/></returns>
        public static string Remove(this string s, Func<char, bool> predicate)
        {
            return new string(s.Where(character => !predicate(character)).ToArray());
        }

        /// <summary>
        /// Returns a new string containing only alpha and numeric characters of the original string
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ToAlphanumeric(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }

            string newString = string.Empty;

            foreach (char c in s)
            {
                if (char.IsNumber(c) || char.IsLetter(c))
                {
                    newString += c;
                }
            }

            return newString;
        }

        public static string FormatAsCurrency(this decimal amount)
        {
            var result = amount.ToString("#.##");
            if (result.IsNullOrEmpty()) result = "0";
            return result;
        }

        public static string FormatAsCurrency(this double amount)
        {
            var result = amount.ToString("#.##");
            if (result.IsNullOrEmpty()) result = "0";
            return result;
        }

        public static string TrimDecimalIfWholeNumber(this decimal @decimal)
        {
            if ((@decimal % 1) == 0 || @decimal == 0)
            {
                return @decimal.ToString("F0");
            }
            return @decimal.ToString("F2");
        }

        /// <summary>
        /// Pretty-Formats the name provided for display. Removes tailing "Id". Splits on capital letters.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="removeId">Indicates whether to remove tailing Id? default is true</param>
        /// <returns></returns>
        public static string FormatAsDisplayName(string name, bool removeId = true)
        {
            if (name == null) return null;
            if (name.EndsWith("Id") && removeId) name = name.Substring(0, name.Length - 2);

            var output = new StringBuilder();

            foreach (var c in name)
            {
                if (char.IsUpper(c) && output.Length > 0 && output.ToString().Last() != ' ')
                {
                    output.Append(" ");
                }
                output.Append(c);
            }

            return output.ToString();
        }

        /// <summary>
        /// Removes all Environment.Newline 's from this string and returns the new string.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string RemoveNewlines(this string s)
        {
            return s.RemoveAll(Environment.NewLine).RemoveAll("\n");
        }

        /// <summary>
        /// Removes all tab [\t] characters's from this string and returns the new string.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string RemoveTabs(this string s)
        {
            return s.RemoveAll("\t");
        }

        /// <summary>
        /// Removes all tab [\t] characters's from this string and returns the new string.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string RemoveSpaces(this string s)
        {
            return s.RemoveAll(" ");
        }

        /// <summary>
        /// Removes all tab [\t] characters's from this string and returns the new string.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string RemoveAllWhiteSpaces(this string s)
        {
            return s != null ? new String(s.Where(c => !char.IsWhiteSpace(c)).ToArray()) : null;
        }

        /// <summary>
        /// Replaces all tab [\t] characters's in this string with <see cref="numberOfSpaces"/> spaces and returns the new string.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="numberOfSpaces">The number of spaces to replace each tab with.  Default is 4.</param>
        /// <returns></returns>
        public static string ReplaceTabsWithSpaces(this string s, int numberOfSpaces = 4)
        {
            var spacesString = new String(Enumerable.Repeat(Char.Parse(" "), numberOfSpaces).ToArray());
            return s != null ? s.Replace("\t", spacesString) : null;
        }

        /// <summary>
        /// Removes all occurrences of <see cref="stringSequenceToRemove"/> from this string and returns a new string.
        /// </summary>
        /// <param name="s">The original string</param>
        /// <param name="stringSequenceToRemove">The sequence of characters to remove</param>
        /// <returns>A new string that contains the contents of the original string, minus the stringSequence that was removed</returns>
        public static string RemoveAll(this string s, string stringSequenceToRemove)
        {
            return s != null ? s.Replace(stringSequenceToRemove, string.Empty) : null;
        }

        /// <summary>
        /// Removes all occurrences of <see cref="charactersToRemove"/> from this string and returns a new string.
        /// </summary>
        /// <param name="s">The original string</param>
        /// <param name="charactersToRemove">Characters which should be removed from string</param>
        /// <returns>A new string that contains the contents of the original string, minus the stringSequence that was removed</returns>
        public static string RemoveAll(this string s, char[] charactersToRemove)
        {
            return s != null ? string.Join(string.Empty, s.Split(charactersToRemove)) : null;
        }

        public static DateTime? ToDateTime(this string value)
        {
            DateTime result;
            return DateTime.TryParse(value, out result) ? result : new DateTime?();
        }

        /// <summary>
        /// Removes UTF's Byte-Order-Mark everywhere in the string
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string CleanFromBom(this string s)
        {
            return s.RemoveAll(new [] {ByteOrderMarkUtf8});
        }
    }
}