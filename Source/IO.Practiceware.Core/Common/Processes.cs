﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace IO.Practiceware
{
    public static class Processes
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// Ensures the current process is not already running and kills the current process if it is already running. Optionally only restricts uniqueness to the current sessions.
        /// Sets focus to the existing process if one is already running.
        /// </summary>
        /// <param name="perSessionId">if set to <c>true</c> [per session identifier].</param>
        public static void EnsureSingleInstance(bool perSessionId = true)
        {
            var currentProcess = Process.GetCurrentProcess();

            foreach (var process in Process.GetProcessesByName(currentProcess.ProcessName))
            {
                if (process.Id != currentProcess.Id &&
                    (!perSessionId || process.SessionId == currentProcess.SessionId))
                {
                    SetForegroundWindow(process.MainWindowHandle);
                    Environment.Exit(0);
                }
            }
        }

        /// <summary>
        /// Runs process and allows to perform other tasks once it finishes
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static Task RunProcessAsync(string fileName)
        {
            // there is no non-generic TaskCompletionSource
            var tcs = new TaskCompletionSource<bool>();

            var process = new Process
            {
                StartInfo = { FileName = fileName },
                EnableRaisingEvents = true
            };

            process.Exited += (sender, args) =>
            {
                tcs.SetResult(true);
                process.Dispose();
            };

            process.Start();

            return tcs.Task;
        }


        /// <summary>
        /// Runs the process and wait for exit.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="arguments">The arguments.</param>
        /// <param name="onStandardOutput">The on standard output.</param>
        /// <param name="onErrorOutput">The on error output.</param>
        /// <param name="timeout">The timeout.</param>
        /// <returns></returns>
        public static int RunProcessAndWaitForExit(string fileName, string arguments, Action<string> onStandardOutput, Action<string> onErrorOutput, int timeout = Timeout.Infinite)
        {
            using (Process process = new Process())
            {
                process.StartInfo.FileName = fileName;
                process.StartInfo.Arguments = arguments;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
                using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
                {
                    process.OutputDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                        {
                            outputWaitHandle.Set();
                        }
                        else
                        {
                            onStandardOutput(e.Data);
                        }
                    };
                    process.ErrorDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                        {
                            errorWaitHandle.Set();
                        }
                        else
                        {
                            onErrorOutput(e.Data);
                        }
                    };

                    Stopwatch sw = new Stopwatch();
                    sw.Start();

                    try
                    {
                        process.Start();

                        process.BeginOutputReadLine();
                        process.BeginErrorReadLine();

                        int checkWait = 2000;

                        while (true)
                        {
                            if (timeout > 0 && sw.ElapsedMilliseconds > timeout)
                            {
                                return -1;
                            }

                            if (process.WaitForExit(checkWait) &&
                                outputWaitHandle.WaitOne() &&
                                errorWaitHandle.WaitOne())
                            {
                                // Process completed. Check process.ExitCode here.
                                return process.ExitCode;
                            }
                        }
                    }
                    finally
                    {
                        sw.Stop();
                        Trace.TraceInformation("Process {0} {1} ran in {2}ms with exit code {3}.", 
                            fileName, arguments, sw.ElapsedMilliseconds, 
                            process.HasExited ? process.ExitCode.ToString() : "N/A (because it timed out waiting to finish).");
                    }
                }
            }
        }
    }
}
