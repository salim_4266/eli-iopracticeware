﻿using System;
using System.ServiceProcess;

namespace IO.Practiceware
{
    public static class ServiceControllers
    {
        /// <summary>
        ///   Waits for the service to reach it's operation of running, paused, or stopped.
        ///   By default, it will wait indefinitely.
        /// </summary>
        /// <param name="serviceController"> </param>
        /// <param name="timespan"> </param>
        public static void WaitForCompleteOperation(this ServiceController serviceController, TimeSpan timespan = default(TimeSpan))
        {
            serviceController.Refresh();
            ServiceControllerStatus statusToWaitFor;
            if (serviceController.Status == ServiceControllerStatus.StartPending || serviceController.Status == ServiceControllerStatus.ContinuePending)
            {
                statusToWaitFor = ServiceControllerStatus.Running;
            }
            else if (serviceController.Status == ServiceControllerStatus.PausePending)
            {
                statusToWaitFor = ServiceControllerStatus.Paused;
            }
            else if (serviceController.Status == ServiceControllerStatus.StopPending)
            {
                statusToWaitFor = ServiceControllerStatus.Stopped;
            }
            else
            {
                serviceController.Refresh();
                return;
            }

            if (timespan != default(TimeSpan))
            {
                serviceController.WaitForStatus(statusToWaitFor, timespan);
            }
            else
            {
                serviceController.WaitForStatus(statusToWaitFor);
            }
            serviceController.Refresh();
        }
    }
}