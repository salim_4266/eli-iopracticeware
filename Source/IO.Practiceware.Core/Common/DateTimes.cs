﻿using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Soaf;
using Soaf.ComponentModel;
using System;
using System.Globalization;
using System.Web.Hosting;

// ReSharper disable CheckNamespace
namespace IO.Practiceware
// ReSharper restore CheckNamespace
{
    public static class DateTimes
    {
        private static readonly DateTime EpochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static readonly string[] DateFormats =
            // special formats
            new[] { "MMddyyyy", "MMddyy", "yyyyMMdd", "MMdd", "yyyyMM" }.Concat(
            CultureInfo.CurrentUICulture.DateTimeFormat.GetAllDateTimePatterns())

            .ToArray();

        private static readonly GregorianCalendar Calendar = new GregorianCalendar();

        public static int GetWeekOfMonth(this DateTime time)
        {
            var first = new DateTime(time.Year, time.Month, 1);
            return time.GetWeekOfYear() - first.GetWeekOfYear() + 1;
        }

        public static int GetWeekOfYear(this DateTime time)
        {
            return Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
        }

        public static DateTime RoundToNearest(this DateTime dt, TimeSpan d)
        {
            int f = 0;
            double m = (double)(dt.Ticks % d.Ticks) / d.Ticks;
            if (m >= 0.5)
                f = 1;
            return new DateTime(((dt.Ticks / d.Ticks) + f) * d.Ticks);
        }

        /// <summary>
        /// Returns the amount of years that have passed starting from this date to the reference date. e.g. (to calculate a person's age)
        /// If this date is greater than or equal to the reference date, then a value of zero will be returned.
        /// </summary>
        /// <param name="beginDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static int GetDifferenceInYears(this DateTime beginDate, DateTime endDate)
        {
            if (beginDate.Date >= endDate.Date) return 0;

            var yearDifference = endDate.Year - beginDate.Year;
            if (endDate < beginDate.AddYears(yearDifference)) yearDifference--;

            return yearDifference;
        }

        /// <summary>
        /// Determines whether or not the first date range overlaps with the second date range
        /// </summary>
        /// <param name="startDate1"></param>
        /// <param name="endDate1"></param>
        /// <param name="startDate2"></param>
        /// <param name="endDate2"></param>
        /// <returns></returns>
        public static bool IsOverlapping(DateTime startDate1, DateTime endDate1, DateTime startDate2, DateTime endDate2)
        {
            return (startDate1 <= endDate2) && (endDate1 >= startDate2);
        }

        /// <summary>
        /// Determines whether or not the first date range overlaps with the second date range. Allows for null enddates which is considered never or infinity
        /// </summary>
        /// <param name="startDate1"></param>
        /// <param name="endDate1"></param>
        /// <param name="startDate2"></param>
        /// <param name="endDate2"></param>
        /// <returns></returns>
        public static bool IsOverlapping(DateTime startDate1, DateTime? endDate1, DateTime startDate2, DateTime? endDate2)
        {
            return (!endDate2.HasValue || startDate1 <= endDate2.Value) && (!endDate1.HasValue || endDate1.Value >= startDate2);
        }

        /// <summary>
        /// Compares the <see cref="testDate"/> against the startDate and endDate range and return true if it is between those dates.
        /// </summary>
        /// <param name="startDate">The start of the range to compare against. A null value is consider zero</param>
        /// <param name="endDate">The end of the range to compare against.  A null value is considered infinity.</param>
        /// <param name="testDate">The date value to test.  If this value is null, method returns false.</param>
        /// <returns></returns>
        public static bool IsBetween(this DateTime testDate, DateTime startDate, DateTime? endDate)
        {
            return testDate >= startDate && (endDate == null || testDate <= endDate);
        }

        /// <summary>
        /// Compares the Date component of this dateTime object to today's date.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns>Returns true if the Date is less than today's date.</returns>
        public static bool IsBeforeToday(this DateTime dt)
        {
            return dt.Date <= DateTime.Now.ToClientTime().Date;
        }

        /// <summary>
        /// Compares the Date component of this dateTime object to today's date.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns>Returns true if the Date is less than today's date. If the nullable dateTime is null, returns false</returns>
        public static bool IsBeforeToday(this DateTime? dt)
        {
            return dt.HasValue && dt.Value.IsBeforeToday();
        }

        /// <summary>
        /// Returns the local DateTime.Now.ToClientTime() for the current client making requests to the server. Takes into account the client's time zone.        
        /// E.g  If a client in pacific time at 5 PM makes a request to get all appointments from the database that are earlier than
        /// right now, the server must convert the local time (8 PM eastern time) to the client's local time before making the comparison
        /// against appointment times.
        /// </summary>
        /// <returns></returns>
        public static DateTime GetClientNow()
        {
            return DateTime.Now.ToClientTime();
        }

        /// <summary>
        /// Converts the dateTime to the local time for the current client making requests to the server. Takes into account the client's time zone.                
        /// </summary>
        /// <returns></returns>
        public static DateTime ToClientTime(this DateTime dt)
        {
            var clientTimeZoneId = ClientTimeZoneIdContext;

            if (clientTimeZoneId.IsNullOrEmpty() || ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled)
                return dt;

            if (dt.Kind == DateTimeKind.Unspecified) dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            var clientTime = TimeZoneInfo.ConvertTime(dt, TimeZoneInfo.FindSystemTimeZoneById(clientTimeZoneId));

            return clientTime;
        }

        /// <summary>
        /// Converts DateTime to Unix epoch
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int ToUnixEpoch(this DateTime dt)
        {
            var seconds = (dt - EpochStart).TotalSeconds;
            return Convert.ToInt32(seconds);
        }

        const string ClientTimeZoneContextKey = "__ClientTimeZoneContext__";

        public static string ClientTimeZoneIdContext
        {
            get { return ContextData.Get(ClientTimeZoneContextKey).IfNotNull(d => (string)d); }
            private set { ContextData.Set(ClientTimeZoneContextKey, value); }
        }

        /// <summary>
        /// Sets the time zone for custom ToClientTime method.  This time zone information is stored in the Wcf Context so that the 
        /// server can access the time zone information.
        /// </summary>
        /// <param name="timeZoneId"></param>
        public static void SetClientTimeZone(string timeZoneId)
        {
            ClientTimeZoneIdContext = timeZoneId;

            Model.DateTimeConversionBootstrapper.BootstrapToClientTimeFunction(ToClientTime);
        }
    }
}