﻿using IO.Practiceware.Application;
using IO.Practiceware.Integration.HL7;
using IO.Practiceware.Integration.HL7.Connections;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Integration.Omedix;
using IO.Practiceware.Model;
using IO.Practiceware.Services.PatientSearch;
using RestSharp;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Text.RegularExpressions;

[assembly: Component(typeof(OmedixIntegrationService), typeof(IOmedixIntegrationService))]

namespace IO.Practiceware.Integration.Omedix
{
    [DataContract]
    public class AppointmentTypeModel
    {
        [DataMember]
        public int AppointmentTypeId { get; set; }

        [DataMember]
        public string AppointmentTypeName { get; set; }
    }

    [DataContract]
    public class PatientSearchModel
    {
        [DataMember]
        public virtual string FirstName { get; set; }

        [DataMember]
        public virtual string MiddleName { get; set; }

        [DataMember]
        public virtual string LastName { get; set; }

        [DataMember]
        public virtual string DateOfBirth { get; set; }

        [DataMember]
        public virtual string CellPhone { get; set; }
    }

    [DataContract]
    public class PatientAppointmentsModel
    {
        [DataMember]
        public virtual DateTime? FromDate { get; set; }

        [DataMember]
        public virtual DateTime? ToDate { get; set; }

        [DataMember]
        public List<PatientAppointmentListModel> PatientAppointmentList { get; set; }
    }

    [DataContract]
    public class PatientAppointmentListModel
    {
        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual DateTime? AppointmentDate { get; set; }

        [DataMember]
        public virtual int ResourceId { get; set; }

        [DataMember]
        public virtual string ResourceName { get; set; }

        [DataMember]
        public virtual int ServiceLocationId { get; set; }

        [DataMember]
        public virtual string ServiceLocationName { get; set; }

        [DataMember]
        public virtual string AppointmentType { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }
    }

    [DataContract]
    public class LocationsModel
    {
        [DataMember]
        public int LocationId { get; set; }

        [DataMember]
        public string ShortName { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Address { get; set; }
    }

    [DataContract]
    public class PhysicianModel
    {
        [DataMember]
        public int PhysicianId { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public string Prefix { get; set; }

        [DataMember]
        public string Suffix { get; set; }
    }

    [DataContract]
    public class SecureMessagingInfo
    {
        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public Guid Guid { get; set; }
    }

    #region Patient Appointments

    [DataContract]
    public class AppointmentSearchModel
    {
        [DataMember]
        public string StartDate { get; set; }

        [DataMember]
        public string EndDate { get; set; }

        [DataMember]
        public int CurrentPage { get; set; }

        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public int TotalPages { get; set; }

        [DataMember]
        public int IsLastRecord { get; set; }

        [DataMember]
        public int Page { get; set; }

        [DataMember]
        public string PatientId { get; set; }
    }

    [DataContract]
    public class AppointmentModel
    {
        [DataMember]
        public virtual string LocationName { get; set; }

        [DataMember]
        public virtual int LocationId { get; set; }

        [DataMember]
        public virtual string PhysicianName { get; set; }

        [DataMember]
        public virtual int? PhysicianId { get; set; }

        [DataMember]
        public virtual DateTime? PatientDateofBirth { get; set; }

        [DataMember]
        public virtual string PatientFirstName { get; set; }

        [DataMember]
        public virtual string PatientLastName { get; set; }

        [DataMember]
        public virtual string PatientAddress { get; set; }

        [DataMember]
        public virtual int PatientId { get; set; }

        [DataMember]
        public virtual string PatientGender { get; set; }

        [DataMember]
        public virtual string PatientContactNo { get; set; }

        [DataMember]
        public virtual int AppointmentId { get; set; }

        [DataMember]
        public virtual DateTime AppointmentDate { get; set; }

        [DataMember]
        public virtual EncounterStatus AppointmentStatus { get; set; }

        [DataMember]
        public virtual string AppointmentComments { get; set; }

    }

    #endregion

    [DataContract]
    public class MessageModel
    {
        [DataMember]
        public virtual string LocationName { get; set; }

        [DataMember]
        public virtual int LocationId { get; set; }

        [DataMember]
        public virtual string PhysicianName { get; set; }
    }

    public class MessageIntegrationModel
    {
        [DataMember]
        public virtual string Message { get; set; }
    }

    [DataContract]
    public class GuidResponseData
    {

        [DataMember(Name = "ResponseCode")]
        public virtual string ResponseCode { get; set; }

        [DataMember(Name = "status")]
        public virtual string Status { get; set; }

        [DataMember(Name = "ResponseCodeDetails")]
        public virtual string ResponseCodeDetails { get; set; }

        // ReSharper disable InconsistentNaming
        [DataMember(Name = "GUID")]
        public virtual Guid GUID { get; set; }
        // ReSharper restore InconsistentNaming

    }

    [ServiceContract]
    public interface IOmedixIntegrationService
    {
        [OperationContract]
        List<PatientSearchResult> SearchPatient(PatientSearchModel patients);

        [OperationContract]
        List<AppointmentTypeModel> GetAppointmentTypes();

        [OperationContract]
        List<LocationsModel> GetLocations();

        [OperationContract]
        List<PhysicianModel> GetPhysicians();

        #region Patient Appointments

        [OperationContract]
        IEnumerable<AppointmentModel> SearchAppointments(AppointmentSearchModel appointment);

        #endregion

        [OperationContract]
        String ReceiveHL7Message(MessageIntegrationModel message);

        [OperationContract]
        SecureMessagingInfo GetSecureMessagingInfo();

        [OperationContract]
        Guid GetSingleSignOnGuid();

        #region [ Numerator Functions ]

        /// <summary>
        /// Provides numerator summary for patient education.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria. </param>
        /// <returns>NumeratorPatientEducationSummaryModel : contains numerator count with list of provider</returns>
        [OperationContract]
        NumeratorPatientEducationSummaryModel GetNumeratorSummaryForPatientEducation(NumeratorSearchModel param);

        /// <summary>
        /// Provides numerator details for patient education.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria. </param>
        /// <returns>List of patient which using patient education </returns>
        [OperationContract]
        List<NumeratorPatientEducationDetailsModel> GetNumeratorDetailsForPatientEducation(NumeratorSearchModel param);

        /// <summary>
        /// Provides numerator summary for secure message.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria. </param>
        /// <returns>NumeratorSecureMessageSummaryModel : contains numerator count with list of provider</returns>
        [OperationContract]
        NumeratorSecureMessageSummaryModel GetNumeratorSummaryForSecureMessage(NumeratorSearchModel param);

        /// <summary>
        /// Provides numerator details for secure message.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria. </param>
        /// <returns>List of patient which using secure message</returns>
        [OperationContract]
        List<NumeratorSecureMessageDetailsModel> GetNumeratorDetailsForSecureMessage(NumeratorSearchModel param);

        /// <summary>
        /// Provides numerator count for health Info VDT.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria. </param>
        /// <returns>Numerator count for VDT</returns>
        [OperationContract]
        NumeratorVdtSummaryModel GetNumeratorSummaryForHealthInfoViewDownloadTransmit(NumeratorSearchModel param);

        /// <summary>
        /// Provides summary for health Info VDT.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria. </param>
        /// <returns>List of patient summary</returns>
        [OperationContract]
        List<NumeratorVdtDetailsModel> GetNumeratorDetailsForHealthInfoViewDownloadTransmit(NumeratorSearchModel param);

        #endregion [ Numerator Functions ]

    }

    /// <summary>
    /// This class contains implementation of methods declared in service interface i.e. IOmedixIntegrationService.
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class OmedixIntegrationService : IOmedixIntegrationService
    {
        private readonly IPatientSearchService _patientService;
        private readonly IPracticeRepository _practiceRepository;
        private readonly IMessageAuditor _auditor;
        private readonly WebServiceAuthenticator _webServiceAuthenticator;

        private const string DateFormat = @"yyyy-MM-dd";
        private const string DateTimeFormat = @"yyyy-MM-dd HH:mm:ss";

        private const string PatientEducationSummary = @"PatientEducationSummary";
        private const string PatientEducationDetails = @"PatientEducationDetails";
        private const string SecureMessageSummary = @"SecureMessageSummary";
        private const string SecureMessageDetails = @"SecureMessageDetails";
        private const string VdtSummary = @"VDTSummary";
        private const string VdtDetails = @"VDTDetails";

        /// <summary>
        /// Initiates the instance of class OmedixIntegrationService.
        /// </summary>
        /// <param name="practiceRepository"></param>
        /// <param name="patientService"></param>
        /// <param name="auditor"></param>
        /// <param name="webServiceAuthenticator"></param>
        public OmedixIntegrationService(IPracticeRepository practiceRepository, 
            IPatientSearchService patientService, 
            IMessageAuditor auditor,
            WebServiceAuthenticator webServiceAuthenticator)
        {
            _practiceRepository = practiceRepository;
            _patientService = patientService;
            _auditor = auditor;
            _webServiceAuthenticator = webServiceAuthenticator;
        }

        public String ReceiveHL7Message(MessageIntegrationModel messageModel)
        {
            try
            {
                if (string.IsNullOrEmpty(messageModel.Message))
                {
                    throw new Exception("Message can not be null.");
                }
                return ProcessFileMessage(messageModel.Message);
            }
            catch (Exception ex)
            {
                Trace.TraceError("Message can not be inserted. InnerException: {0} Message: {1}".FormatWith(ex.InnerException.Message, ex.Message));
                throw;
            }
        }

        private String ProcessFileMessage(string text)
        {
            var message = new HL7Message();
            message.Load(text);
            _auditor.Audit(message, false, ProcessingState.Unprocessed);
            if (message.RequestsAcknowledgement)
            {
                IMessage acknowledgement = message.GetAcknowledgement();
                return acknowledgement.ToString();
            }
            return String.Empty;
        }

        public List<PatientSearchResult> SearchPatient(PatientSearchModel patients)
        {
            var searchField = SearchFields.None;

            if (patients == null) return new List<PatientSearchResult>();

            var searchTerms = new List<string>();

            if (patients.FirstName != null)
            {
                searchField |= SearchFields.FirstName;
                searchTerms.Add(patients.FirstName);
            }
            if (patients.LastName != null)
            {
                searchField |= SearchFields.LastName;
                searchTerms.Add(patients.LastName);
            }
            if (patients.CellPhone != null)
            {
                if (ValidatePhoneNumbers(ScrubTextPhoneNumber(patients.CellPhone)))
                {
                    searchField |= SearchFields.PhoneNumber;
                    searchTerms.Add(patients.CellPhone);
                }
                else
                    throw new Exception("Invalid phone number received.");
            }
            if (patients.DateOfBirth != null)
            {
                if (ValidateDate(patients.DateOfBirth))
                {
                    searchField |= SearchFields.DateOfBirth;
                    searchTerms.Add(patients.DateOfBirth);
                }
                else
                    throw new Exception("Invalid date of birth received.");
            }
            if (searchField == SearchFields.None) return new List<PatientSearchResult>();
            return _patientService.Search(TextSearchMode.BeginsWith, searchField, searchTerms.Join()).ToList();
        }

        public List<AppointmentTypeModel> GetAppointmentTypes()
        {
            return (from x in _practiceRepository.AppointmentTypes
                    where x.Id >= 0 && x.IsArchived == false
                    orderby x.Id
                    select new AppointmentTypeModel
                    {
                        AppointmentTypeId = x.Id,
                        AppointmentTypeName = x.Name
                    }).ToList();
        }

        public List<LocationsModel> GetLocations()
        {
            return (from x in _practiceRepository.ServiceLocations.AsEnumerable()
                    where x.IsExternal == false && x.IsArchived == false
                    orderby x.Id
                    select new LocationsModel
                    {
                        LocationId = x.Id,
                        Name = x.Name,
                        ShortName = x.ShortName,
                        Address = ExtractFormatAddress(x.ServiceLocationAddresses.WithAddressType(ServiceLocationAddressTypeId.MainOffice))
                    }).ToList();
        }

        public List<PhysicianModel> GetPhysicians()
        {
            return (from x in _practiceRepository.Users.OfType<Doctor>()
                    where x.IsArchived == false
                    orderby x.Id
                    select new PhysicianModel
                    {
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        MiddleName = x.MiddleName,
                        FullName = x.DisplayName,
                        Prefix = x.Prefix,
                        Suffix = x.Suffix,
                        PhysicianId = x.Id
                    }).ToList();
        }

        private static string ExtractFormatAddress(ServiceLocationAddress address)
        {
            if (address == null) return null;
            string mainLine = String.Format("{0} {1} {2}", address.Line1, address.Line2, address.Line3);
            string cityState = String.Format("{0}, {1}", address.City, address.StateOrProvince.Abbreviation);
            return mainLine + Environment.NewLine + cityState + Environment.NewLine + address.PostalCode;
        }

        private static bool ValidatePhoneNumbers(string phoneNumber)
        {
            return Regex.IsMatch(phoneNumber, @"^[0-9]{10,12}$");
        }

        private static bool ValidateDate(string date)
        {
            return Regex.IsMatch(date, @"^[0-1]?[0-9](/|-)[0-3]?[0-9](/|-)[1-2][0-9][0-9][0-9]$");
        }

        private static string ScrubTextPhoneNumber(string text)
        {
            const string pattern = @"[\s()-]+";
            var rgx = new Regex(pattern);
            return rgx.Replace(text, "");
        }

        public static bool ValidateDate(string from, string to)
        {
            // ReSharper disable RedundantAssignment
            bool isValid = false;
            // ReSharper restore RedundantAssignment
            if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
            {
                switch (DateTime.Compare(Convert.ToDateTime(@from), Convert.ToDateTime(to)))
                {
                    case 1:
                        // ReSharper disable RedundantAssignment
                        isValid = false;
                        // ReSharper restore RedundantAssignment
                        break;
                    default:
                        isValid = true;
                        break;
                }
            }
            else
                isValid = true;
            return isValid;
        }

        #region Patient Appointments

        public IEnumerable<AppointmentModel> SearchAppointments(AppointmentSearchModel appointments)
        {
            var searchparams = new Dictionary<AppointmentSearchField, string>();
            if (appointments.StartDate != null)
            {
                if (ValidateDate(appointments.StartDate))
                    searchparams.Add(AppointmentSearchField.StartDate, appointments.StartDate);
                else
                    throw new Exception("Invalid start date received.");
            }
            else
                throw new Exception("Start date can not be null.");
            if (appointments.EndDate != null)
            {
                if (ValidateDate(appointments.EndDate))
                    searchparams.Add(AppointmentSearchField.EndDate, appointments.EndDate);
                else
                    throw new Exception("Invalid end date received.");
            }
            else
                throw new Exception("End date can not be null.");

            if (!ValidateDate(appointments.StartDate, appointments.EndDate))
                throw new Exception("Start date must be less than end date.");

            if (appointments.PatientId != null)
            {
                searchparams.Add(AppointmentSearchField.PatientId, appointments.PatientId);
            }
            if (!searchparams.Any()) return new List<AppointmentModel>();

            var paging = AppointmentPaging.False;
            if (appointments.Page > 1)
                paging = AppointmentPaging.True;
            IEnumerable<AppointmentModel> collection = SearchAppointment(appointments, searchparams, paging);
            return collection.ToList();
        }

        private IEnumerable<AppointmentModel> SearchAppointment(AppointmentSearchModel appointment, IDictionary<AppointmentSearchField, string> searchParams, AppointmentPaging paging)
        {
            IQueryable<UserAppointment> fromDatabasePatients = _practiceRepository.Appointments.OfType<UserAppointment>();
            // ReSharper disable JoinDeclarationAndInitializer
            Expression<Func<UserAppointment, bool>> whereCondition;
            // ReSharper restore JoinDeclarationAndInitializer
            whereCondition = ConstructWhereCondition(fromDatabasePatients, searchParams);
            if (whereCondition == null) return new List<AppointmentModel>();
            List<UserAppointment> fromDatabaseResults;
            switch (paging)
            {
                case AppointmentPaging.False:
                    fromDatabaseResults = fromDatabasePatients.Where(whereCondition).OrderBy(p => p.DateTime).ThenBy(p => p.DateTime).ToList();
                    appointment.TotalPages = fromDatabaseResults.Count();
                    break;
                case AppointmentPaging.True:
                    fromDatabaseResults = fromDatabasePatients.Where(whereCondition).OrderBy(p => p.DateTime).ThenBy(p => p.DateTime).ThenBy(p => p.Encounter.Patient.FirstName).Skip((appointment.Page - 1) * appointment.PageSize).Take(appointment.PageSize + 1).ToList();
                    break;
                default:
                    fromDatabaseResults = fromDatabasePatients.Where(whereCondition).OrderBy(p => p.DateTime).ThenBy(p => p.DateTime).ToList();
                    break;
            }
            // Load dependent data
            _practiceRepository.AsQueryableFactory().Load(fromDatabaseResults,
                                                          r => r.Encounter,
                                                          r => r.Encounter.ServiceLocation,
                                                          r => r.User,
                                                          r => r.Encounter.Patient
                );
            IEnumerable<UserAppointment> sortedResults = fromDatabaseResults;

            // Build up view model data and return
            var results = new List<AppointmentModel>();
            foreach (UserAppointment r in sortedResults)
            {
                var result = new AppointmentModel();
                result.AppointmentId = r.Id;
                result.AppointmentDate = r.DateTime;
                result.LocationId = r.Encounter.ServiceLocation.Id;
                result.LocationName = r.Encounter.ServiceLocation.Name;
                result.AppointmentComments = r.Comment;
                result.PatientFirstName = r.Encounter.Patient.FirstName;
                result.PatientLastName = r.Encounter.Patient.LastName;
                result.PatientDateofBirth = r.Encounter.Patient.DateOfBirth;
                result.PatientContactNo = (r.Encounter.Patient.CellPhoneNumber != null) ? r.Encounter.Patient.CellPhoneNumber.ToString() : string.Empty;
                result.PhysicianName = r.User.UserName;
                result.PhysicianId = r.User.Id;
                result.PatientId = r.Encounter.PatientId;
                result.AppointmentStatus = r.Encounter.EncounterStatus;
                results.Add(result);
            }

            appointment.CurrentPage = appointment.Page;
            if (results.Count() <= appointment.PageSize)
                appointment.IsLastRecord = 2;

            if (appointment.IsLastRecord != 2)
            {
                if (results.Count() <= appointment.PageSize)
                    appointment.IsLastRecord = 1;
                else
                    appointment.IsLastRecord = 0;
            }
            // ReSharper disable AssignNullToNotNullAttribute
            return results.Take(200);
        }

        private static Expression<Func<UserAppointment, bool>> ConstructWhereCondition(IQueryable<UserAppointment> appointment, IEnumerable<KeyValuePair<AppointmentSearchField, string>> searchParams)
        {
            var wherePredicates = new List<Expression<Func<UserAppointment, bool>>>();

            foreach (KeyValuePair<AppointmentSearchField, String> entry in searchParams)
            {
                var terms = new List<string>();
                var fields = new List<AppointmentSearchField>();
                terms.Add(entry.Value);
                fields.Add(entry.Key);
                wherePredicates.AddRange(QueryContains(appointment, terms, fields));
            }
            Expression<Func<UserAppointment, bool>> whereCondition = wherePredicates.FirstOrDefault();
            if (whereCondition == null) return null;
            if (wherePredicates.Count > 1)
            {
                whereCondition = wherePredicates.Skip(1).Aggregate(whereCondition, (current, predicate) => current.And(predicate));
            }
            return whereCondition;
        }

        #region QueryContains

        private static IEnumerable<Expression<Func<UserAppointment, bool>>> QueryContains(IQueryable<UserAppointment> appointment, IEnumerable<string> searchTerms, IEnumerable<AppointmentSearchField> searchFields)
        {
            var predicates = new List<Expression<Func<UserAppointment, bool>>>();
            if (searchFields.Contains(AppointmentSearchField.StartDate)) predicates.AddRange(QueryMatchFromDate(appointment, searchTerms));
            if (searchFields.Contains(AppointmentSearchField.EndDate)) predicates.AddRange(QueryMatchToDate(appointment, searchTerms));
            if (searchFields.Contains(AppointmentSearchField.PatientId)) predicates.AddRange(QueryMatchPatientId(appointment, searchTerms));
            return predicates;
        }

        private static IEnumerable<Expression<Func<UserAppointment, bool>>> QueryMatchFromDate(IQueryable<UserAppointment> appointment, IEnumerable<string> searchTerms)
        {
            var queries = new List<Expression<Func<UserAppointment, bool>>>();
            foreach (string term in searchTerms)
            {
                string t = term;
                DateTime fromdate;
                if (DateTime.TryParse(t, out fromdate))
                {
                    TimeSpan time = new TimeSpan(0, 0, 0);
                    fromdate = fromdate.Add(time);
                    Expression<Func<UserAppointment, bool>> queryActualDateTime = appointment.CreatePredicate(p => p.DateTime >= fromdate);
                    queries.Add(queryActualDateTime);
                }
            }
            return queries;
        }

        private static IEnumerable<Expression<Func<UserAppointment, bool>>> QueryMatchToDate(IQueryable<UserAppointment> appointment, IEnumerable<string> searchTerms)
        {
            var queries = new List<Expression<Func<UserAppointment, bool>>>();
            foreach (string term in searchTerms)
            {
                string t = term;
                DateTime todate;
                if (DateTime.TryParse(t, out todate))
                {
                    TimeSpan time = new TimeSpan(23, 59, 0);
                    todate = todate.Add(time);
                    Expression<Func<UserAppointment, bool>> queryActualDateTime = appointment.CreatePredicate(p => p.DateTime <= todate);
                    queries.Add(queryActualDateTime);
                }
            }
            return queries;
        }

        private static IEnumerable<Expression<Func<UserAppointment, bool>>> QueryMatchPatientId(IQueryable<UserAppointment> appointment, IEnumerable<string> searchTerms)
        {
            var queries = new List<Expression<Func<UserAppointment, bool>>>();
            foreach (string term in searchTerms)
            {
                string t = term;
                int patientid;
                if (int.TryParse(t, out patientid))
                {
                    if (patientid > 0)
                    {
                        Expression<Func<UserAppointment, bool>> queryActualDateTime = appointment.CreatePredicate(p => p.Encounter.PatientId == patientid);
                        queries.Add(queryActualDateTime);
                    }
                }
            }
            return queries;
        }

        #endregion

        #endregion

        #region [ Numerator Functions ]
        /// <summary>
        /// Provides numerator summary for patient education.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria. </param>
        /// <returns>NumeratorPatientEducationSummaryModel : contains numerator count with list of provider</returns>
        public NumeratorPatientEducationSummaryModel GetNumeratorSummaryForPatientEducation(NumeratorSearchModel param)
        {
            var numeratorResponse = new NumeratorPatientEducationSummaryModel();

            ValidateInputForNumerator(param);

            WebServiceMessagingOutboundEndpointConfiguration endpoint =
                MessagingConfiguration.Current.WebServiceMessageConnectorConfiguration.OutboundEndpoints.For(param.ReceivingApplication, PatientEducationSummary);

            if (endpoint == null || endpoint.MessageType.IsNullOrWhiteSpace())
            {
                Trace.TraceError("Could not find remote endpoint for message type '{0}' for {1}.".FormatWith(PatientEducationSummary, param.ReceivingApplication));
                throw new ArgumentNullException("param", "Could not find remote endpoint for message type {0} for {1}.".FormatWith(PatientEducationSummary, param.ReceivingApplication));
            }

            string authenticationToken = string.Empty;
            authenticationToken = AuthenticationForNumerator(param.ReceivingApplication, endpoint, authenticationToken);

            StringBuilder url = GenerateUrlForNumeratorFunction(param, endpoint, authenticationToken);
            url = url.Replace(" ", "");
            var request = GenerateWebRequestForNumerator(url, endpoint);
            if (request == null) throw new ArgumentNullException("param");

            using (var response = request.GetResponse())
            {
                var dataStream = response.GetResponseStream();
                var jsonSerializer = new DataContractJsonSerializer(typeof(NumeratorPatientEducationSummaryModel));
                if (dataStream != null)
                    numeratorResponse = (NumeratorPatientEducationSummaryModel)jsonSerializer.ReadObject(dataStream);
            }

            return numeratorResponse;
        }

        /// <summary>
        /// Provides numerator details for patient education.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria. </param>
        /// <returns>List of patient which using patient education </returns>
        public List<NumeratorPatientEducationDetailsModel> GetNumeratorDetailsForPatientEducation(NumeratorSearchModel param)
        {
            var numeratorDetailsResponseList = new List<NumeratorPatientEducationDetailsModel>();

            ValidateInputForNumerator(param);

            WebServiceMessagingOutboundEndpointConfiguration endpoint =
                MessagingConfiguration.Current.WebServiceMessageConnectorConfiguration.OutboundEndpoints.For(param.ReceivingApplication, PatientEducationDetails);

            if (endpoint == null || endpoint.MessageType.IsNullOrWhiteSpace())
            {
                Trace.TraceError("Could not find remote endpoint for message type '{0}' for {1}.".FormatWith(PatientEducationDetails, param.ReceivingApplication));
                throw new ArgumentNullException("param", "Could not find remote endpoint for message type {0} for {1}.".FormatWith(PatientEducationDetails, param.ReceivingApplication));
            }

            string authenticationToken = string.Empty;
            authenticationToken = AuthenticationForNumerator(param.ReceivingApplication, endpoint, authenticationToken);

            StringBuilder url = GenerateUrlForNumeratorFunction(param, endpoint, authenticationToken);

            HttpWebRequest request = GenerateWebRequestForNumerator(url, endpoint);

            using (var response = request.GetResponse())
            {
                using (var dataStream = response.GetResponseStream())
                {
                    using (var ms = new MemoryStream())
                    {
                        List<string> patientDataArrayList = GetRecordLinesFromStream(dataStream, ms);

                        for (int i = 1; i < patientDataArrayList.Count; i++)
                        {
                            string[] patientDataArray = patientDataArrayList[i].Split(',');
                            var patientData = new NumeratorPatientEducationDetailsModel();
                            /*
                            patientData.ProviderRemoteId = patientDataArray[0];
                            patientData.ProviderLastName = patientDataArray[1];
                            patientData.ProviderFirstName = patientDataArray[2];
                            patientData.PatientRemoteId = patientDataArray[3];
                            patientData.PatientLastName = patientDataArray[4];
                            patientData.PatientFirstName = patientDataArray[5];

                            DateTime tempDate;
                            if (DateTime.TryParseExact(patientDataArray[6], DateFormat, null, System.Globalization.DateTimeStyles.None, out tempDate))
                                patientData.PatientDob = tempDate;
                            else
                                patientData.PatientDob = null;

                            patientData.PatientGender = patientDataArray[7];
                            patientData.PatientEmailId = patientDataArray[8];
                            patientData.EmailSubject = patientDataArray[9];

                            if (DateTime.TryParseExact(patientDataArray[10], DateTimeFormat, null, System.Globalization.DateTimeStyles.None, out tempDate))
                                patientData.DateTimeOfAction = tempDate;
                            else
                                patientData.DateTimeOfAction = null;

                            */
                            patientData.ProviderRemoteId = patientDataArray[0]; //ProviderLastName,
                            patientData.ProviderLastName = patientDataArray[0];
                            patientData.ProviderFirstName = patientDataArray[1];
                            patientData.PatientRemoteId = patientDataArray[2];
                            patientData.PatientLastName = patientDataArray[3];
                            patientData.PatientFirstName = patientDataArray[4];

                            DateTime tempDate;
                            if (DateTime.TryParseExact(patientDataArray[5], DateFormat, null, System.Globalization.DateTimeStyles.None, out tempDate))
                                patientData.PatientDob = tempDate;
                            else
                                patientData.PatientDob = null;

                            patientData.PatientGender = patientDataArray[6];
                            patientData.PatientEmailId = patientDataArray[7];
                            patientData.EmailSubject = patientDataArray[8];

                            if (DateTime.TryParseExact(patientDataArray[9], DateTimeFormat, null, System.Globalization.DateTimeStyles.None, out tempDate))
                                patientData.DateTimeOfAction = tempDate;
                            else
                                patientData.DateTimeOfAction = null;
                            numeratorDetailsResponseList.Add(patientData);
                        }
                    }
                }
            }

            return numeratorDetailsResponseList;
        }

        /// <summary>
        /// Provides numerator summary for secure message.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria. </param>
        /// <returns>NumeratorSecureMessageSummaryModel : contains numerator count with list of provider</returns>
        public NumeratorSecureMessageSummaryModel GetNumeratorSummaryForSecureMessage(NumeratorSearchModel param)
        {
            var numeratorResponse = new NumeratorSecureMessageSummaryModel();

            ValidateInputForNumerator(param);

            WebServiceMessagingOutboundEndpointConfiguration endpoint =
                MessagingConfiguration.Current.WebServiceMessageConnectorConfiguration.OutboundEndpoints.For(param.ReceivingApplication, SecureMessageSummary);

            if (endpoint == null || endpoint.MessageType.IsNullOrWhiteSpace())
            {
                Trace.TraceError("Could not find remote endpoint for message type '{0}' for {1}.".FormatWith(SecureMessageSummary, param.ReceivingApplication));
                throw new ArgumentNullException("param", "Could not find remote endpoint for message type {0} for {1}.".FormatWith(SecureMessageSummary, param.ReceivingApplication));
            }

            string authenticationToken = string.Empty;
            authenticationToken = AuthenticationForNumerator(param.ReceivingApplication, endpoint, authenticationToken);

            StringBuilder url = GenerateUrlForNumeratorFunction(param, endpoint, authenticationToken);

            HttpWebRequest request = GenerateWebRequestForNumerator(url, endpoint);

            using (var response = request.GetResponse())
            {
                var dataStream = response.GetResponseStream();
                var jsonSerializer = new DataContractJsonSerializer(typeof(NumeratorSecureMessageSummaryModel));
                if (dataStream != null)
                    numeratorResponse = (NumeratorSecureMessageSummaryModel)jsonSerializer.ReadObject(dataStream);
            }

            return numeratorResponse;
        }

        /// <summary>
        /// Provides numerator details for secure message.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria. </param>
        /// <returns>List of patient which using secure message</returns>
        public List<NumeratorSecureMessageDetailsModel> GetNumeratorDetailsForSecureMessage(NumeratorSearchModel param)
        {
            var numeratorDetailsResponseList = new List<NumeratorSecureMessageDetailsModel>();

            ValidateInputForNumerator(param);
            var endpoint =
                MessagingConfiguration.Current.WebServiceMessageConnectorConfiguration.OutboundEndpoints.For(param.ReceivingApplication, SecureMessageDetails);

            if (endpoint == null || endpoint.MessageType.IsNullOrWhiteSpace())
            {
                Trace.TraceError("Could not find remote endpoint for message type '{0}' for {1}.".FormatWith(SecureMessageDetails, param.ReceivingApplication));
                throw new ArgumentNullException("param", "Could not find remote endpoint for message type {0} for {1}.".FormatWith(SecureMessageDetails, param.ReceivingApplication));
            }

            string authenticationToken = string.Empty;
            authenticationToken = AuthenticationForNumerator(param.ReceivingApplication, endpoint, authenticationToken);

            StringBuilder url = GenerateUrlForNumeratorFunction(param, endpoint, authenticationToken);

            HttpWebRequest request = GenerateWebRequestForNumerator(url, endpoint);
            if (request == null) throw new ArgumentNullException("param");

            using (var response = request.GetResponse())
            {
                using (var dataStream = response.GetResponseStream())
                {
                    using (var ms = new MemoryStream())
                    {
                        List<string> patientDataArrayList = GetRecordLinesFromStream(dataStream, ms);

                        for (int i = 1; i < patientDataArrayList.Count; i++)
                        {
                            string[] patientDataArray = patientDataArrayList[i].Split(',');
                            var patientData = new NumeratorSecureMessageDetailsModel();
                            patientData.ProviderRemoteId = patientDataArray[0];
                            patientData.ProviderLastName = patientDataArray[1];
                            patientData.ProviderFirstName = patientDataArray[2];
                            patientData.PatientRemoteId = patientDataArray[3];
                            patientData.PatientLastName = patientDataArray[4];
                            patientData.PatientFirstName = patientDataArray[5];

                            DateTime tempDate;
                            patientData.PatientDob = DateTime.TryParseExact(patientDataArray[6], DateFormat, null, System.Globalization.DateTimeStyles.None, out tempDate) ? (DateTime?)tempDate : null;

                            patientData.PatientGender = patientDataArray[7];

                            if (DateTime.TryParseExact(patientDataArray[8], DateTimeFormat, null, System.Globalization.DateTimeStyles.None, out tempDate))
                                patientData.DateTimeOfAction = tempDate;
                            else
                                patientData.DateTimeOfAction = null;
                            numeratorDetailsResponseList.Add(patientData);
                        }
                    }
                }
            }

            return numeratorDetailsResponseList;
        }

        /// <summary>
        /// Provides numerator count for "View, Download, Transmit" of patient health Info.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria. </param>
        /// <returns>Numerator count for VDT</returns>
        public NumeratorVdtSummaryModel GetNumeratorSummaryForHealthInfoViewDownloadTransmit(NumeratorSearchModel param)
        {
            var numeratorResponse = new NumeratorVdtSummaryModel();

            ValidateInputForNumerator(param);
            var endpoint =
                MessagingConfiguration.Current.WebServiceMessageConnectorConfiguration.OutboundEndpoints.For(param.ReceivingApplication, VdtSummary);

            if (endpoint == null || endpoint.MessageType.IsNullOrWhiteSpace())
            {
                Trace.TraceError("Could not find remote endpoint for message type '{0}' for {1}.".FormatWith(VdtSummary, param.ReceivingApplication));
                throw new ArgumentNullException("param", "Could not find remote endpoint for message type {0} for {1}.".FormatWith(VdtSummary, param.ReceivingApplication));
            }

            string authenticationToken = string.Empty;
            authenticationToken = AuthenticationForNumerator(param.ReceivingApplication, endpoint, authenticationToken);

            StringBuilder url = GenerateUrlForNumeratorFunction(param, endpoint, authenticationToken);
            url = url.Replace(" ", "");
            HttpWebRequest request = GenerateWebRequestForNumerator(url, endpoint);
            if (request == null) throw new ArgumentNullException("param");

            using (var response = request.GetResponse())
            {
                var dataStream = response.GetResponseStream();
                var jsonSerializer = new DataContractJsonSerializer(typeof(NumeratorVdtSummaryModel));
                if (dataStream != null)
                    numeratorResponse = (NumeratorVdtSummaryModel)jsonSerializer.ReadObject(dataStream);
            }

            return numeratorResponse;
        }

        /// <summary>
        /// Provides details breakdown for "View, Download, Transmit" of patient health Info.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria. </param>
        /// <returns>List of patient summary</returns>
        public List<NumeratorVdtDetailsModel> GetNumeratorDetailsForHealthInfoViewDownloadTransmit(NumeratorSearchModel param)
        {
            var numeratorDetailsResponseList = new List<NumeratorVdtDetailsModel>();

            ValidateInputForNumerator(param);
            WebServiceMessagingOutboundEndpointConfiguration endpoint =
                MessagingConfiguration.Current.WebServiceMessageConnectorConfiguration.OutboundEndpoints.For(param.ReceivingApplication, VdtDetails);

            if (endpoint == null || endpoint.MessageType.IsNullOrWhiteSpace())
            {
                Trace.TraceError("Could not find remote endpoint for message type '{0}' for {1}.".FormatWith(VdtDetails, param.ReceivingApplication));
                throw new ArgumentNullException("param", "Could not find remote endpoint for message type {0} for {1}.".FormatWith(VdtDetails, param.ReceivingApplication));
            }

            string authenticationToken = string.Empty;
            authenticationToken = AuthenticationForNumerator(param.ReceivingApplication, endpoint, authenticationToken);

            StringBuilder url = GenerateUrlForNumeratorFunction(param, endpoint, authenticationToken);
            url = url.Replace(" ", "");
            HttpWebRequest request = GenerateWebRequestForNumerator(url, endpoint);

            using (var response = request.GetResponse())
            {
                using (var dataStream = response.GetResponseStream())
                {
                    using (var ms = new MemoryStream())
                    {
                        List<string> patientDataArrayList = GetRecordLinesFromStream(dataStream, ms);

                        for (int i = 1; i < patientDataArrayList.Count; i++)
                        {
                            string[] patientDataArray = patientDataArrayList[i].Split(',');
                            var patientData = new NumeratorVdtDetailsModel();
                            patientData.PatientRemoteId = patientDataArray[0];
                            patientData.PatientLastName = patientDataArray[1];
                            patientData.PatientFirstName = patientDataArray[2];

                            DateTime tempDate;
                            if (DateTime.TryParseExact(patientDataArray[3], DateFormat, null, System.Globalization.DateTimeStyles.None, out tempDate))
                                patientData.PatientDob = tempDate;
                            else
                                patientData.PatientDob = null;

                            patientData.PatientGender = patientDataArray[4];


                            if (DateTime.TryParseExact(patientDataArray[5], DateTimeFormat, null, System.Globalization.DateTimeStyles.None, out tempDate))
                                patientData.DateTimeOfAction = tempDate;
                            else
                                patientData.DateTimeOfAction = null;
                            numeratorDetailsResponseList.Add(patientData);
                        }
                    }
                }
            }

            return numeratorDetailsResponseList;
        }

        /// <summary>
        /// Returns lines from strame data of .csv file.
        /// </summary>
        /// <param name="dataStream">Contains .csv file data in stream format</param>
        /// <param name="ms">Memory stream object to read data from DataStream </param>
        /// <returns>List of records fetch from .csv file data.</returns>
        private static List<string> GetRecordLinesFromStream(Stream dataStream, MemoryStream ms)
        {
            dataStream.CopyTo(ms);

            if (ms.Length == 0)
                return new List<string>();

            ms.Position = 0;

            var patientDataArrayList = new List<string>();

            using (var streamReader = new StreamReader(ms))
            using (var stringReader = new StringReader(streamReader.ReadToEnd()))
            {
                while (true)
                {
                    string aLine = stringReader.ReadLine();
                    if (aLine != null)
                    {
                        patientDataArrayList.Add(aLine);
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return patientDataArrayList;
        }

        /// <summary>
        /// Generate HTTP web request.
        /// </summary>
        /// <param name="url">URL for web request</param>
        /// <param name="endpoint">Web service messaging outbound endpoint</param>
        /// <returns>HTTP web request</returns>
        private static HttpWebRequest GenerateWebRequestForNumerator(StringBuilder url, WebServiceMessagingOutboundEndpointConfiguration endpoint)
        {
            var request = (HttpWebRequest)WebRequest.Create(url.ToString());
            ServicePointManager.Expect100Continue = false;
            request.Method = endpoint.MethodType;
            request.ContentType = endpoint.ContentType;
            request.UserAgent = WebServiceAuthenticator.GenericUserAgent;
            return request;
        }
        /// <summary>
        /// Authentication for omedix services.
        /// </summary>
        /// <param name="receivingApplication">Receiving application</param>
        /// <param name="endpoint">Web service messaging outbound endpoint</param>
        /// <param name="authenticationToken">Web service messaging outbound endpoint</param>
        /// <returns>Authentication token</returns>
        private string AuthenticationForNumerator(string receivingApplication, WebServiceMessagingOutboundEndpointConfiguration endpoint, string authenticationToken)
        {
            if (endpoint.AuthenticationRequired)
            {
                authenticationToken = _webServiceAuthenticator.GenerateNewAuthenticationToken(receivingApplication);
                if (string.IsNullOrWhiteSpace(authenticationToken))
                {
                    Trace.TraceError("Could not generate authentication token for {0}.".FormatWith(receivingApplication));
                    throw new Exception("Authentication failed.");
                }
            }
            return authenticationToken;
        }
        /// <summary>
        /// validation of input parameter for numerator search criteria.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria.</param>
        private static void ValidateInputForNumerator(NumeratorSearchModel param)
        {
            if (string.IsNullOrWhiteSpace(param.ReceivingApplication))
                throw new Exception("Invalid Receiving Application");

            if (param.FromDate == null || !ValidateDate(param.FromDate.ToString("MM-dd-yyyy")))
                throw new Exception("Invalid From date received.");

            if (param.ToDate == null || !ValidateDate(param.ToDate.ToString("MM-dd-yyyy")))
                throw new Exception("Invalid To date received.");

            if (!ValidateDate(param.FromDate.ToString(), param.ToDate.ToString()))
                throw new Exception("Invalid From and To date received.");
        }
        /// <summary>
        /// Genarate valid Url from web service messaging outbound endpoint, search parameter, authentication token for services to get numerator details.
        /// </summary>
        /// <param name="param">NumeratorSearchModel: Input parameter for search criteria.</param>
        /// <param name="endpoint">Web service messaging outbound endpoint</param>
        /// <param name="authenticationToken">Authentication token</param>
        /// <returns></returns>
        private static StringBuilder GenerateUrlForNumeratorFunction(NumeratorSearchModel param, WebServiceMessagingOutboundEndpointConfiguration endpoint, string authenticationToken)
        {
            var url = new StringBuilder(string.Format("{0}{1}", endpoint.Value, endpoint.MethodName));
            url.Replace("{Token}", authenticationToken);
            url.Replace("{From}", param.FromDate.ToString("MM-dd-yyyy"));
            url.Replace("{To}", param.ToDate.ToString("MM-dd-yyyy"));
            return url.Replace("{ProviderId}", param.PhysicianId ?? string.Empty);
        }

        #endregion [ Numerator Functions ]

        public SecureMessagingInfo GetSecureMessagingInfo()
        {
            var guid = GetSingleSignOnGuid();
            //Access the endpoint for sending the secure message
            WebServiceMessagingOutboundEndpointConfiguration endpoint =
                MessagingConfiguration.Current.WebServiceMessageConnectorConfiguration.OutboundEndpoints.For("OMEDIX", "ViewSecureMessages");

            //put together the url for sending the secure message
            var url = new StringBuilder(endpoint.Value).Append(endpoint.MethodName).ToString();

            return new SecureMessagingInfo { Url = url, Guid = guid };
        }

        public Guid GetSingleSignOnGuid()
        {
            // if the user is valid and returned a token     

            // Get the authentication token
            string authenticationToken = _webServiceAuthenticator.GetAuthenticationToken("OMEDIX");

            //Access the endpoint for getting the GUID
            WebServiceMessagingOutboundEndpointConfiguration endpoint =
                MessagingConfiguration.Current.WebServiceMessageConnectorConfiguration.OutboundEndpoints.For("OMEDIX", "SingleSignOnGuid");

            //put together the url for accessing the GUID
            var url = new StringBuilder(endpoint.Value).Append(endpoint.MethodName).Replace("{token}", authenticationToken).ToString();

            var request = new RestRequest(url);
            // pass the data request through to the address specified
            request.Method = Method.POST;
            request.RequestFormat = DataFormat.Json;
            request.AddBody(new
            {
                firstName = UserContext.Current.UserDetails.FirstName,
                lastName = UserContext.Current.UserDetails.LastName,
                suffix = UserContext.Current.UserDetails.Suffix,
                email = UserContext.Current.UserDetails.Email,
                remoteId = UserContext.Current.UserDetails.Id,
                isProvider = UserContext.Current.UserDetails.IsProvider
            });

            var client = new RestClient();
            client.UserAgent = WebServiceAuthenticator.GenericUserAgent;
            // get the response from the server
            var response = client.Execute<GuidResponseData>(request);

            if (response.Data != null)
            {
                if (response.Data.GUID != default(Guid))
                    return response.Data.GUID;

                throw new Exception("GetSingleSignOnGuid {0} returned: {1} {2}".FormatWith(endpoint.Value, response.Data.ResponseCode, response.Data.ResponseCodeDetails));
            }
            throw new Exception("Nothing returned from GetSingleSignOnGuid {0}".FormatWith(endpoint.Value));
        }
    }


    #region Patient Appointments

    [DataContract]
    public enum AppointmentSearchField
    {
        [EnumMember]
        StartDate,
        [EnumMember]
        EndDate,
        [EnumMember]
        PatientId
    }

    [DataContract]
    public enum AppointmentPaging
    {
        [EnumMember]
        True,
        [EnumMember]
        False
    }
    #endregion

    #region Numerator Data
    /// <summary>
    /// Represents the input parameter such as from date, to date , physician id, receiving application 
    /// for fetching numerator data related to PatientEducation, Secure message and VDT from Omedix.
    /// </summary>
    [DataContract]
    public class NumeratorSearchModel
    {
        /// <summary>
        /// From date
        /// </summary>
        [DataMember]
        public virtual DateTime FromDate { get; set; }
        /// <summary>
        /// To date 
        /// </summary>
        [DataMember]
        public virtual DateTime ToDate { get; set; }
        /// <summary>
        /// Physician ID
        /// </summary>
        [DataMember]
        public virtual string PhysicianId { get; set; }
        /// <summary>
        /// Receiving application
        /// </summary>
        [DataMember]
        public virtual string ReceivingApplication { get; set; }
    }

    /// <summary>
    /// Represents provider information such as provider id,last name, first name, count. 
    /// </summary>
    [DataContract]
    public class ProviderInfo
    {
        /// <summary>
        /// Remote ID of provider 
        /// </summary>
        [DataMember(Name = "RemoteProviderID")]
        public string RemoteProviderId { get; set; }
        /// <summary>
        /// Provider last name 
        /// </summary>
        [DataMember(Name = "ProviderLastName")]
        public string ProviderLastName { get; set; }
        /// <summary>
        /// Provider first name
        /// </summary>
        [DataMember(Name = "ProviderFirstName")]
        public string ProviderFirstName { get; set; }
        /// <summary>
        /// Provider count
        /// </summary>
        [DataMember(Name = "ProviderCount")]
        public int ProviderCount { get; set; }

    }

    /// <summary>
    /// This class represents provider wise numerator value for patient education.
    /// </summary>
    [DataContract]
    public class NumeratorPatientEducationSummaryModel
    {
        /// <summary>
        /// Result code for action.
        /// </summary>
        [DataMember(Name = "ResponseCode")]
        public int ResponseCode { get; set; }
        /// <summary>
        /// Status for action.
        /// </summary>
        [DataMember(Name = "status")]
        public string Status { get; set; }
        /// <summary>
        /// Numerator value for patient education.
        /// </summary>
        [DataMember(Name = "totalCount")]
        public int TotalCount { get; set; }
        /// <summary>
        /// List of providers for patient education summary.
        /// </summary>
        [DataMember(Name = "providers")]
        public List<ProviderInfo> Providers { get; set; }
    }

    /// <summary>
    /// This class represents provider wise numerator value for secure message.
    /// </summary>
    [DataContract]
    public class NumeratorSecureMessageSummaryModel
    {
        /// <summary>
        /// Result code for action.
        /// </summary>
        [DataMember]
        public int ResponseCode { get; set; }
        /// <summary>
        /// Status for action.
        /// </summary>
        [DataMember(Name = "status")]
        public string Status { get; set; }
        /// <summary>
        /// Numerator value for secure message summary.
        /// </summary>
        [DataMember(Name = "totalCount")]
        public int TotalCount { get; set; }
        /// <summary>
        /// List of providers for secure message summary.
        /// </summary>
        [DataMember(Name = "providers")]
        public List<ProviderInfo> Providers { get; set; }
    }

    /// <summary>
    /// This class represents report wise numerator value for “View, Download, Transmit” (abbr. VDT) of Health info.
    /// </summary>
    [DataContract]
    public class NumeratorVdtSummaryModel
    {
        /// <summary>
        /// Result code for action.
        /// </summary>
        [DataMember]
        public int ResponseCode { get; set; }
        /// <summary>
        /// Status for action.
        /// </summary>
        [DataMember(Name = "status")]
        public string Status { get; set; }
        /// <summary>
        /// Numerator value for VDT summary.
        /// </summary>
        [DataMember(Name = "totalCount")]
        public int TotalCount { get; set; }
        /// <summary>
        /// List of report for VDT summary.
        /// </summary>
        [DataMember(Name = "report")]
        public List<VdtReport> Report { get; set; }
    }

    /// <summary>
    /// Represents patients education details.
    /// </summary>
    [DataContract]
    public class NumeratorPatientEducationDetailsModel
    {
        /// <summary>
        /// Remote ID of provider 
        /// </summary>
        [DataMember]
        public string ProviderRemoteId { get; set; }
        /// <summary>
        /// Provider last name
        /// </summary>
        [DataMember]
        public string ProviderLastName { get; set; }
        /// <summary>
        /// Provider first name
        /// </summary>
        [DataMember]
        public string ProviderFirstName { get; set; }
        /// <summary>
        /// Remote ID of patient
        /// </summary>
        [DataMember]
        public string PatientRemoteId { get; set; }
        /// <summary>
        /// Patient last name
        /// </summary>
        [DataMember]
        public string PatientLastName { get; set; }
        /// <summary>
        /// Patient first name
        /// </summary>
        [DataMember]
        public string PatientFirstName { get; set; }
        /// <summary>
        /// Patient date of birth
        /// </summary>
        [DataMember]
        public DateTime? PatientDob { get; set; }
        /// <summary>
        /// Patient gender
        /// </summary>
        [DataMember]
        public string PatientGender { get; set; }
        /// <summary>
        /// Patient email ID
        /// </summary>
        [DataMember]
        public string PatientEmailId { get; set; }
        /// <summary>
        /// Email subject which was sent to patient from provider for patient education.
        /// </summary>
        [DataMember]
        public string EmailSubject { get; set; }
        /// <summary>
        /// Sent email time.
        /// </summary>
        [DataMember]
        public DateTime? DateTimeOfAction { get; set; }
    }

    /// <summary>
    /// Represents secure message details.
    /// </summary>
    [DataContract]
    public class NumeratorSecureMessageDetailsModel
    {
        /// <summary>
        /// Remote ID of provider 
        /// </summary>
        [DataMember]
        public string ProviderRemoteId { get; set; }
        /// <summary>
        /// Provider last name
        /// </summary>
        [DataMember]
        public string ProviderLastName { get; set; }
        /// <summary>
        /// Provider first name
        /// </summary>
        [DataMember]
        public string ProviderFirstName { get; set; }
        /// <summary>
        /// Remote ID of patient 
        /// </summary>
        [DataMember]
        public string PatientRemoteId { get; set; }
        /// <summary>
        /// Patient last name
        /// </summary>
        [DataMember]
        public string PatientLastName { get; set; }
        /// <summary>
        /// Patient first name
        /// </summary>
        [DataMember]
        public string PatientFirstName { get; set; }
        /// <summary>
        /// Patient date of birth
        /// </summary>
        [DataMember]
        public DateTime? PatientDob { get; set; }
        /// <summary>
        /// Patient gender
        /// </summary>
        [DataMember]
        public string PatientGender { get; set; }
        /// <summary>
        /// Date time of action
        /// </summary>
        [DataMember]
        public DateTime? DateTimeOfAction { get; set; }

    }

    /// <summary>
    /// Information regarding view ,download and transmit details corresponding to patient.
    /// </summary>
    [DataContract]
    public class NumeratorVdtDetailsModel
    {
        /// <summary>
        /// Remote ID of provider 
        /// </summary>
        [DataMember]
        public string PatientRemoteId { get; set; }
        /// <summary>
        /// Patient last name
        /// </summary>
        [DataMember]
        public string PatientLastName { get; set; }
        /// <summary>
        /// Patient first name
        /// </summary>
        [DataMember]
        public string PatientFirstName { get; set; }
        /// <summary>
        /// Patient date of birth
        /// </summary>
        [DataMember]
        public DateTime? PatientDob { get; set; }
        /// <summary>
        /// Patient gender
        /// </summary>
        [DataMember]
        public string PatientGender { get; set; }
        /// <summary>
        /// Date time of action
        /// </summary>
        [DataMember]
        public DateTime? DateTimeOfAction { get; set; }
    }

    /// <summary>
    /// Report information regarding view ,download and transmit details corresponding to patient.
    /// </summary>
    [DataContract(Name = "VdtReport")]
    [KnownType(typeof(NumeratorPatientEducationSummaryModel))]
    public class VdtReport
    {
        private const string DateFormat = @"yyyy-MM-dd";
        private const string DateTimeFormat = @"yyyy-MM-dd HH:mm:ss";

        /// <summary>
        /// Patient date of birth
        /// </summary>
        [DataMember(Name = "PatientDOB")]
        private string PatientDob { get; set; }
        /// <summary>
        /// Date time of action
        /// </summary>
        [DataMember(Name = "DateTimeOfAction")]
        private string DateTimeOfAction { get; set; }
        /// <summary>
        /// Remote ID of patient
        /// </summary>
        [DataMember(Name = "PatientRemoteID")]
        public virtual string PatientRemoteId { get; set; }
        /// <summary>
        /// Patient first name
        /// </summary>
        [DataMember(Name = "PatientFirstName")]
        public virtual string PatientFirstName { get; set; }
        /// <summary>
        /// Patient last name
        /// </summary>
        [DataMember(Name = "PatientLastName")]
        public virtual string PatientLastName { get; set; }
        /// <summary>
        /// Patient date of birth
        /// </summary>
        [DataMember]
        public virtual DateTime? Dob
        {
            get
            {
                DateTime tempDate;
                return (DateTime.TryParseExact(PatientDob, DateFormat, null, System.Globalization.DateTimeStyles.None, out tempDate))
                    ? tempDate : (DateTime?)null;
            }
            set
            {
                PatientDob = (value != null) ? value.ToString(DateFormat) : null;
            }
        }
        /// <summary>
        /// Patient gender
        /// </summary>
        [DataMember(Name = "PatientGender")]
        public virtual string PatientGender { get; set; }
        /// <summary>
        /// Date time of action
        /// </summary>
        [DataMember]
        public virtual DateTime? ActionDateTime
        {
            get
            {
                DateTime tempDate;
                return (DateTime.TryParseExact(DateTimeOfAction, DateTimeFormat, null, System.Globalization.DateTimeStyles.None, out tempDate))
                    ? tempDate : (DateTime?)null;
            }
            set
            {
                DateTimeOfAction = (value != null) ? value.ToString(DateFormat) : null;
            }
        }
    }
    #endregion
}