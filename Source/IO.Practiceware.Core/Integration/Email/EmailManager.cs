using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace IO.Practiceware.Integration.Email
{
    public class EmailManager
    {
        public void Send(MailMessage mailMessage, bool isTextMessage)
        {
            SmtpConfiguration smtpMailSection = isTextMessage ? MessagingConfiguration.Current.TextMessageConfiguration :
                                                                MessagingConfiguration.Current.EmailConfiguration;
            var smtpClient = new SmtpClient(smtpMailSection.Network != null ? smtpMailSection.Network.Host : string.Empty);
            smtpClient.DeliveryMethod = smtpMailSection.DeliveryMethod;

            if (smtpMailSection.From != null) { mailMessage.From = new MailAddress(smtpMailSection.From); }

            if (smtpMailSection.Network != null)
            {
                smtpClient.Host = smtpMailSection.Network.Host;
                smtpClient.Port = smtpMailSection.Network.Port == 0 ? 25 : smtpMailSection.Network.Port;
                smtpClient.EnableSsl = smtpMailSection.Network.EnableSsl;
                if (!string.IsNullOrEmpty(smtpMailSection.Network.UserName) && !string.IsNullOrEmpty(smtpMailSection.Network.Password))
                {
                    smtpClient.Credentials = new NetworkCredential(smtpMailSection.Network.UserName, smtpMailSection.Network.Password);
                }
                smtpClient.UseDefaultCredentials = smtpMailSection.Network.DefaultCredentials;
            }
            if (smtpMailSection.DeliveryMethod == SmtpDeliveryMethod.SpecifiedPickupDirectory)
            {
                smtpClient.PickupDirectoryLocation = Path.GetFullPath(Environment.ExpandEnvironmentVariables(smtpMailSection.SpecifiedPickupDirectory.PickupDirectoryLocation));
            }

            smtpClient.Timeout = 20000;
            smtpClient.Send(mailMessage);
        }

        public void SendWebEmail(Patient patient, string content)
        {
            var emailMailMessage = new MailMessage
            {
                Body = content,
                Subject = "Email Test",
                IsBodyHtml = true,
                Priority = MailPriority.Normal
            };
            List<string> patientEmailAddress = patient.PatientEmailAddresses.Select(i => i.Value).ToList();
            if (patientEmailAddress.Count == 0) { throw new Exception("No Email Address found for patient."); }
            patientEmailAddress.ForEach(item => emailMailMessage.To.Add(item));
            Send(emailMailMessage, false);
        }

        public void SendTextMessage(Patient patient, string content)
        {
            var textMsgMailMessage = new MailMessage
            {
                Body = content,
                Subject = "Text Message Test",
                IsBodyHtml = true,
                Priority = MailPriority.Normal
            };
            var recipient = string.Format("{0}@Sms.{1}", patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Cell, NotFoundBehavior.Exception),
                                                        MessagingConfiguration.Current.TextMessageConfiguration.Network.Host);
            textMsgMailMessage.To.Add(recipient);
            if (textMsgMailMessage.To.Count == 0)
            {
                throw new InvalidOperationException("No cell phone numbers were found for patient.");
            }
            Send(textMsgMailMessage, true);
        }
    }
}
