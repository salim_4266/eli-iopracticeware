using System;
using IO.Practiceware.Integration.Email.Connections;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using Soaf;
using Soaf.ComponentModel;
using System.Net.Mail;

[assembly: Component(typeof(TextMessageConnector.SendMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.Email.Connections
{
	public class TextMessageConnector
	{
	    public class SendMessageSubscriber
	    {
            public SendMessageSubscriber(IMessenger messenger, Func<TextMessageConnector> connector)
	        {
                // ReSharper disable RedundantArgumentName
                messenger.Subscribe<SendMessageRequest>(handler: (sender, token, message) => connector().SendMessage(message),
                                                        filter: m => IsSupported(m.Message));
                // ReSharper restore RedundantArgumentName
	        }
	    }

        private void SendMessage(SendMessageRequest message)
        {
            message.Handlers.Add(typeof(TextMessageConnector));
            SendMessage(message.Message);
        }

		private static bool IsSupported(IMessage message)
		{
            return (message is EmailMessage && 
                    message.Destination == ExternalSystemId.TextMessage.ToString());
		}

		protected void SendMessage(IMessage message)
		{
			var emailMessage = (EmailMessage)message;
			var mailManager = new EmailManager();
			MailMessage mailMessage = emailMessage.Message;
			mailManager.Send(mailMessage, true);
		}
	}
}
