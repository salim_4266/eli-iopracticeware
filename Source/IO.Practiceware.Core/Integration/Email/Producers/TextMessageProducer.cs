using IO.Practiceware.Integration.Email.Producers;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;

[assembly: Component(typeof(TextMessageProducer.ProduceMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.Email.Producers
{
    public class TextMessageProducer
    {
        public class ProduceMessageSubscriber
        {
            public ProduceMessageSubscriber(IMessenger messenger, Func<TextMessageProducer> producer)
            {
                // ReSharper disable RedundantArgumentName            
                messenger.Subscribe<ProduceMessageRequest>(
                    handler: (sender, token, message) =>
                    {
                        IMessage m = producer().ProduceMessage(message.ExternalSystemMessage);
                        message.ProducedMessage = m ?? message.ProducedMessage;
                    },
                    filter: message => IsSupported(message.ExternalSystemMessage));
                // ReSharper restore RedundantArgumentName
            }
        }

        private readonly Lazy<IPracticeRepository> _practiceRepository;
        private readonly IDocumentGenerationService _documentGenerationService;

        public TextMessageProducer(Func<IPracticeRepository> practiceRepository, 
            IDocumentGenerationService documentGenerationService)
        {
            _practiceRepository = Lazy.For(practiceRepository);
            _documentGenerationService = documentGenerationService;
        }

        private EmailMessage CreateTextMessage(ExternalSystemMessage externalSystemMessage, int? patientId, int? appointmentId)
        {
            Patient patient;
            object dataSource;

            if (appointmentId.HasValue)
            {
                UserAppointment appointment = _practiceRepository.Value.Appointments.OfType<UserAppointment>().Include(i => i.Encounter.Patient.PatientPhoneNumbers).
                                                                                                         Include(i => i.User).
                                                                                                         Include(i => i.Encounter.PatientSurgery).
                                                                                                         Include(i => i.Encounter.ServiceLocation).FirstOrDefault(a => a.Id == appointmentId.Value);
                if (appointment == null) { throw new NullReferenceException("Appointment not found."); }
                patient = appointment.Encounter.Patient;
                dataSource = appointment;
            }
            else if (patientId.HasValue)
            {
                patient = _practiceRepository.Value.Patients.Include(i => i.PatientPhoneNumbers).FirstOrDefault(i => i.Id == patientId.Value);
                dataSource = patient;
            }
            else
            {
                throw new InvalidOperationException("Either an appointment or patient id must be specified.");
            }
            string fileName = externalSystemMessage.ExternalSystemExternalSystemMessageType.TemplateFile.Trim();
            if (fileName == string.Empty) { throw new ArgumentException("File Name is Empty."); }
            if (patient == null) { throw new ArgumentException("Patient not found."); }

            byte[] mergedContent = _documentGenerationService.PerformMailMerge(FileManager.Instance.ReadContents(Path.Combine(Directories.Templates, fileName)), dataSource);
            var mailMessage = new MailMessage
            {
                Subject = "Email Appointment Reminder for system Text Message",
                Body = Encoding.UTF8.GetString(_documentGenerationService.ConvertDocument(mergedContent, "Html")),
                Priority = MailPriority.Normal,
                IsBodyHtml = true
            };
            var recipient = string.Format("{0}@Sms.{1}", patient.PatientPhoneNumbers.First(i => i.PatientPhoneNumberType == PatientPhoneNumberType.Cell),
                                                            MessagingConfiguration.Current.TextMessageConfiguration.Network.Host);
            mailMessage.To.Add(recipient);
            return new EmailMessage(mailMessage, externalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType.Name, externalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystem.Name);

        }

        public IMessage ProduceMessage(ExternalSystemMessage externalSystemMessage)
        {
            int? appointmentId = (from e in externalSystemMessage.ExternalSystemMessagePracticeRepositoryEntities
                                  where e.PracticeRepositoryEntityId == 1
                                  select e.PracticeRepositoryEntityKey).FirstOrDefault<string>().ToInt();
            int? patientId = (from e in externalSystemMessage.ExternalSystemMessagePracticeRepositoryEntities
                              where e.PracticeRepositoryEntityId == 3
                              select e.PracticeRepositoryEntityKey).FirstOrDefault<string>().ToInt();
            return CreateTextMessage(externalSystemMessage, patientId, appointmentId);
        }

        private static bool IsSupported(ExternalSystemMessage message)
        {
            return (message.ExternalSystemExternalSystemMessageType.ExternalSystemId == (int)ExternalSystemId.TextMessage &&
                    message.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType.IsOutbound);
        }
    }
}
