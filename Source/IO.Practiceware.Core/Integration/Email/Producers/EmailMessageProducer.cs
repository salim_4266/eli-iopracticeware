using IO.Practiceware.Integration.Email.Producers;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;

[assembly: Component(typeof(EmailMessageProducer.ProduceMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.Email.Producers
{
    public class EmailMessageProducer
    {
        public class ProduceMessageSubscriber
        {
            public ProduceMessageSubscriber(IMessenger messenger, Func<EmailMessageProducer> producer)
            {
                // ReSharper disable RedundantArgumentName
                messenger.Subscribe<ProduceMessageRequest>(
                    handler: (sender, token, message) =>
                    {
                        IMessage m = producer().ProduceMessage(message.ExternalSystemMessage);
                        message.ProducedMessage = m ?? message.ProducedMessage;
                    },
                    filter: message => IsSupported(message.ExternalSystemMessage));
                // ReSharper restore RedundantArgumentName
            }
        }

        private readonly Lazy<IPracticeRepository> _practiceRepository;
        private readonly IDocumentGenerationService _documentGenerationService;

        public EmailMessageProducer(Func<IPracticeRepository> practiceRepository, 
            IDocumentGenerationService documentGenerationService)
        {
            _practiceRepository = Lazy.For(practiceRepository);
            _documentGenerationService = documentGenerationService;
        }

        private EmailMessage CreateEmailMessage(ExternalSystemMessage externalSystemMessage, int? patientId, int? appointmentId)
        {
            Patient patient;
            object datasource;
            if (appointmentId.HasValue)
            {
                var appointment = _practiceRepository.Value.Appointments.OfType<UserAppointment>().Include(i => i.Encounter.Patient.PatientEmailAddresses).
                                                                                             Include(i => i.User).
                                                                                             Include(i => i.Encounter.PatientSurgery).
                                                                                             Include(i => i.Encounter.ServiceLocation).FirstOrDefault(a => a.Id == appointmentId.Value);

                if (appointment == null) { throw new NullReferenceException("Appointment not found."); }
                patient = appointment.Encounter.Patient;
                datasource = appointment;
            }
            else if (patientId.HasValue)
            {
                patient = _practiceRepository.Value.Patients.Include(i => i.PatientEmailAddresses).FirstOrDefault(i => i.Id == patientId.Value);
                datasource = patient;
            }
            else
            {
                throw new InvalidOperationException("Either an appointment or patient id must be specified.");
            }

            if (patient == null) { throw new ArgumentException("Patient not found."); }

            string fileName = externalSystemMessage.ExternalSystemExternalSystemMessageType.TemplateFile.Trim();
            if (fileName == string.Empty) { throw new ArgumentException("File Name is Empty."); }

            List<string> patientEmailAddress = patient.PatientEmailAddresses.Select(i => i.Value).ToList();
            if (patientEmailAddress.Count == 0) { throw new Exception("No Email Address found for patient."); }

            byte[] mergedContent = _documentGenerationService.PerformMailMerge(FileManager.Instance.ReadContents(Path.Combine(Directories.Templates, fileName)), datasource);
            var mailMessage = new MailMessage
            {
                Subject = "Email Appointment Reminder for system Email",
                Body = Encoding.UTF8.GetString(_documentGenerationService.ConvertDocument(mergedContent, "Html")),
                Priority = MailPriority.Normal,
                IsBodyHtml = true
            };
            patientEmailAddress.ForEach(item => mailMessage.To.Add(item));
            return new EmailMessage(mailMessage, externalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType.Name, externalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystem.Name);
        }

        public IMessage ProduceMessage(ExternalSystemMessage externalSystemMessage)
        {
            int? appointmentId = externalSystemMessage.ExternalSystemMessagePracticeRepositoryEntities.Where(e => e.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.Appointment).Select(e => e.PracticeRepositoryEntityKey).FirstOrDefault().ToInt();
            int? patientId = externalSystemMessage.ExternalSystemMessagePracticeRepositoryEntities.Where(e => e.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.Patient).Select(e => e.PracticeRepositoryEntityKey).FirstOrDefault().ToInt();
            return CreateEmailMessage(externalSystemMessage, patientId, appointmentId);
        }

        private static bool IsSupported(ExternalSystemMessage message)
        {
            return (message.ExternalSystemExternalSystemMessageType.ExternalSystemId == (int)ExternalSystemId.Email &&
                    message.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType.IsOutbound);
        }
    }
}
