using IO.Practiceware.Integration.Messaging;
using System;
using System.Net.Mail;
namespace IO.Practiceware.Integration.Email
{
	public class EmailMessage : IMessage
	{
		private MailMessage _mailMessage;
		private readonly string _messageType;
		private readonly string _destination;
		private readonly string _id;
		
        public object AcknowledgementForId
		{
			get { return null; }
		}

		public object Id
		{
			get { return _id; }
		}

		public MailMessage Message
		{
			get { return _mailMessage; }
		}

		public string MessageType
		{
			get { return _messageType; }
		}

		public string CreatedBy
		{
			get { return _destination; }
		}

		public string Destination
		{
			get { return _destination; }
		}

		public bool RequestsAcknowledgement
		{
			get { return false; }
		}

		public string Source
		{
			get { return "IO PRACTICEWARE"; }
		}

		public bool ShouldProcess
		{
			get { return true; }
		}

		public EmailMessage(MailMessage mailMessage, string messageType, string destination)
		{
			_mailMessage = mailMessage;
			_messageType = messageType;
			_destination = destination;
			_id = Guid.NewGuid().ToString();
		}
		public IMessage GetAcknowledgement()
		{
			return null;
		}

		public void Load(string value)
		{
			_mailMessage = new MailMessage();
		}

		public override string ToString()
		{
			return _mailMessage.Body;
		}
	}
}
