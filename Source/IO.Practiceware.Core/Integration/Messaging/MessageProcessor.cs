﻿using System.Collections.Generic;
using System.Linq.Expressions;
using IO.Practiceware.Configuration;
using IO.Practiceware.Model;
using Soaf;
using Soaf.Collections;
using Soaf.Data;
using Soaf.Linq;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Transactions;

namespace IO.Practiceware.Integration.Messaging
{
    /// <summary>
    ///   A request to produce a message to send.
    /// </summary>
    public class ProduceMessageRequest
    {
        public IMessage ProducedMessage { get; set; }
        public ExternalSystemMessage ExternalSystemMessage { get; set; }
    }

    /// <summary>
    ///   A request to handle an inbound message.
    /// </summary>
    public class HandleMessageRequest
    {
        public IMessage Message { get; set; }
    }

    /// <summary>
    ///   Sends out queued messages to external systems.
    /// </summary>
    [SupportsTransactionScopes]
    public class MessageProcessor
    {
        protected const int MaxAttemptCount = 10;

        private readonly IMessenger _messenger;
        private readonly IPracticeRepository _practiceRepository;
        private static readonly IDictionary<string, object> SyncRoots = new Dictionary<string, object>().Synchronized();

        private static readonly int[] UnsupportedMessageTypeIds = new[] { ExternalSystemMessageTypeId.X12_835_Inbound }.Select(i => (int)i).ToArray();

        public MessageProcessor(IPracticeRepository practiceRepository, IMessenger messenger)
        {
            Transaction = Transaction.Current;
            _practiceRepository = practiceRepository;
            _messenger = messenger;
        }

        internal Transaction Transaction { get; set; }

        /// <summary>
        /// Loops through all unprocessed messages, attempts to produce the content if not already populated, finds the appropriate connector, and attempts to send them.
        /// </summary>
        /// <param name="shouldContinueProcessing">The should continue processing.</param>
        public void ProcessUnprocessedMessages(Func<bool> shouldContinueProcessing = null)
        {
            ProcessUnprocessedMessages(null, shouldContinueProcessing);
        }

        /// <summary>
        /// Loops through filtered list of unprocessed messages, attempts to produce the content if not already populated, finds the appropriate connector, and attempts to send them.
        /// </summary>
        /// <param name="customFilter">The custom filter.</param>
        /// <param name="shouldContinueProcessing">The should continue processing.</param>
        public void ProcessUnprocessedMessages(Expression<Func<ExternalSystemMessage, bool>> customFilter, Func<bool> shouldContinueProcessing = null)
        {
            ProcessMessages(customFilter, shouldContinueProcessing, ExternalSystemMessageProcessingStateId.Unprocessed);
        }

        /// <summary>
        ///   Loops through filtered list of messages, attempts to produce the content if not already populated, finds the appropriate connector, and attempts to send them.
        /// </summary>
        public void ProcessMessages(Expression<Func<ExternalSystemMessage, bool>> customFilter, Func<bool> shouldContinueProcessing = null, params ExternalSystemMessageProcessingStateId[] statesFilter)
        {
            ProcessMessages((practiceRepository, processMessageAction) =>
            {
                // Create base query
                var messagesQuery = practiceRepository.ExternalSystemMessages
                    .Where(i => i.ProcessingAttemptCount < MaxAttemptCount
                        && !i.ExternalSystemExternalSystemMessageType.IsDisabled
                        && !UnsupportedMessageTypeIds.Contains(i.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId));

                // Filter by state
                if (statesFilter.Length > 0)
                {
                    var stateIds = statesFilter.Select(s => (int)s).ToArray();
                    messagesQuery = messagesQuery
                        .Where(i => stateIds.Contains(i.ExternalSystemMessageProcessingStateId));
                }

                // Apply custom filter
                if (customFilter != null)
                {
                    messagesQuery = messagesQuery
                        .Where(customFilter);
                }

                // Fetch messages matching all filters
                var messageIdsToProcess = messagesQuery
                    .Select(em => new
                    {
                        em.Id,
                        em.ExternalSystemExternalSystemMessageType.ExternalSystemId,
                        em.UpdatedDateTime
                    })
                    .ToArray();

                if (messageIdsToProcess.Any())
                {
                    Trace.TraceInformation("Processing {0} messages.", messageIdsToProcess.Count());

                    messageIdsToProcess
                        .GroupBy(m => m.ExternalSystemId)
                        .ForAllInParallel(group =>
                        {
                            foreach (var message in group.OrderBy(m => m.UpdatedDateTime))
                            {
                                if (shouldContinueProcessing == null || shouldContinueProcessing())
                                {
                                    var loadedMessage = practiceRepository.ExternalSystemMessages
                                        .Include(i => i.ExternalSystemExternalSystemMessageType.ExternalSystem,
                                            i => i.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType,
                                            i => i.ExternalSystemMessagePracticeRepositoryEntities.Select(j => j.PracticeRepositoryEntity))
                                        .WithId(message.Id);
                                    processMessageAction(loadedMessage);
                                }
                                else
                                {
                                    // Stop attempts to process more messages and end
                                    break;
                                }
                            }
                        });
                }
            });
        }

        /// <summary>
        /// Barebone message processing entry point. Defers loading and filtering of messages to caller
        /// </summary>
        /// <param name="loadAndPassToProcessing">Action gives access to repository and provides function implementation to process ready message.</param>
        protected void ProcessMessages(Action<IPracticeRepository, Action<ExternalSystemMessage>> loadAndPassToProcessing)
        {
            lock (SyncRoots.GetValue(ConfigurationManager.PracticeRepositoryConnectionString, () => new object()))
            {
                TransactionScope ts = null;
                try
                {
                    if (Transaction != null)
                    {
                        ts = new TransactionScope(Transaction.DependentClone(DependentCloneOption.RollbackIfNotComplete), TransactionManager.MaximumTimeout);
                    }

                    // Callback to load messages and then process them once requested
                    loadAndPassToProcessing(_practiceRepository, m =>
                    {
                        if (m.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType.IsOutbound)
                        {
                            TryProcessOutboundMessage(m);
                        }
                        else
                        {
                            TryProcessInboundMessage(m);
                        }
                    });

                    if (ts != null)
                    {
                        ts.Complete();
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError(new ApplicationException("Unhandled error occurred processing messages.", ex).ToString());
                }
                finally
                {
                    if (ts != null)
                    {
                        ts.Dispose();
                    }
                }
            }
        }

        [TransactionScope(TransactionScopeOption.Suppress, IsolationLevel.ReadUncommitted)]
        protected virtual void TryProcessInboundMessage(ExternalSystemMessage externalSystemMessage)
        {
            int originalProcessingAttemptCount = externalSystemMessage.ProcessingAttemptCount;

            try
            {
                var request = new ProduceMessageRequest { ExternalSystemMessage = externalSystemMessage };

                Exception produceMessageException = null;
                try
                {
                    _messenger.Publish(request);
                }
                catch (Exception ex)
                {
                    produceMessageException = new Exception("An error occurred producing the message {0}.".FormatWith(externalSystemMessage.Id), ex);
                    Trace.TraceError(produceMessageException.ToString());
                    externalSystemMessage.Error = produceMessageException.ToString();
                }

                if (request.ProducedMessage != null && request.ProducedMessage.ToString() != externalSystemMessage.Value && request.ProducedMessage.Id.ToString() != externalSystemMessage.CorrelationId)
                {
                    externalSystemMessage.Value = request.ProducedMessage.ToString();
                    externalSystemMessage.CorrelationId = request.ProducedMessage.Id.ToString();
                }

                if (externalSystemMessage.ExternalSystemMessageProcessingStateId == (int)ExternalSystemMessageProcessingStateId.Processed || produceMessageException != null)
                {
                    Trace.TraceInformation("Message {0} was excluded and marked as processed because it contains no content or was otherwise marked as processed.".FormatWith(externalSystemMessage.Id));
                }
                else
                {
                    UpdateMessageStateAndSave(externalSystemMessage, ProcessingState.Processing);
                    _messenger.Publish(new HandleMessageRequest { Message = request.ProducedMessage });
                }

                UpdateMessageStateAndSave(externalSystemMessage, ProcessingState.Processed);
            }
            catch (Exception ex)
            {
                if (originalProcessingAttemptCount == externalSystemMessage.ProcessingAttemptCount)
                {
                    externalSystemMessage.ProcessingAttemptCount += 1;
                }
                externalSystemMessage.Error = ex.ToString();
                UpdateMessageStateAndSave(externalSystemMessage, ProcessingState.Unprocessed);

                Trace.TraceError(new Exception("An error occurred processing the message {0}.".FormatWith(externalSystemMessage.Id), ex).ToString());
            }
        }

        [TransactionScope(TransactionScopeOption.Suppress, IsolationLevel.ReadUncommitted)]
        protected virtual void TryProcessOutboundMessage(ExternalSystemMessage externalSystemMessage)
        {
            int originalProcessingAttemptCount = externalSystemMessage.ProcessingAttemptCount;

            try
            {
                var request = new ProduceMessageRequest { ExternalSystemMessage = externalSystemMessage };

                Exception produceMessageException = null;
                try
                {
                    _messenger.Publish(request);
                }
                catch (Exception ex)
                {
                    produceMessageException = new Exception("An error occurred producing the message {0}.".FormatWith(externalSystemMessage.Id), ex);
                    Trace.TraceError(produceMessageException.ToString());
                    externalSystemMessage.Error = produceMessageException.ToString();
                }

                if (request.ProducedMessage != null && request.ProducedMessage.ToString() != externalSystemMessage.Value && request.ProducedMessage.Id.ToString() != externalSystemMessage.CorrelationId)
                {
                    externalSystemMessage.Value = request.ProducedMessage.ToString();
                    externalSystemMessage.CorrelationId = request.ProducedMessage.Id.ToString();
                }

                if (request.ProducedMessage == null || string.IsNullOrEmpty(externalSystemMessage.Value) || externalSystemMessage.ExternalSystemMessageProcessingStateId == (int)ExternalSystemMessageProcessingStateId.Processed || produceMessageException != null)
                {
                    Trace.TraceInformation("Message {0} was excluded and marked as processed because it contains no content or was otherwise marked as processed.".FormatWith(externalSystemMessage.Id));

                    if (produceMessageException != null) externalSystemMessage.Value = produceMessageException.ToString();
                }
                else
                {
                    UpdateMessageStateAndSave(externalSystemMessage, ProcessingState.Processing);

                    var sendMessageRequest = new SendMessageRequest { Message = request.ProducedMessage };
                    _messenger.Publish(sendMessageRequest);

                    if (!sendMessageRequest.Handlers.Any())
                    {
                        Trace.TraceWarning("Message {0} had no handlers that supported sending the message.".FormatWith(sendMessageRequest.Message.Id));
                        externalSystemMessage.Error = "No handlers found that supported sending the message";
                    }
                    else
                    {
                        Debug.WriteLine("Message {0} was sent by handlers {1}.".FormatWith(sendMessageRequest.Message.Id, sendMessageRequest.Handlers.Select(h => h.Name).Join()));
                    }
                }

                UpdateMessageStateAndSave(externalSystemMessage, ProcessingState.Processed);
            }
            catch (Exception ex)
            {
                if (originalProcessingAttemptCount == externalSystemMessage.ProcessingAttemptCount)
                {
                    externalSystemMessage.ProcessingAttemptCount += 1;
                }
                externalSystemMessage.Error = ex.ToString();
                UpdateMessageStateAndSave(externalSystemMessage, ProcessingState.Unprocessed);

                Trace.TraceError(new Exception("An error occurred processing the message {0}.".FormatWith(externalSystemMessage.Id), ex).ToString());
            }
        }

        protected virtual void UpdateMessageStateAndSave(ExternalSystemMessage externalSystemMessage, ProcessingState state)
        {
            externalSystemMessage.UpdatedDateTime = DateTime.UtcNow;
            externalSystemMessage.ExternalSystemMessageProcessingStateId = (int)state.TranslateEnum<ExternalSystemMessageProcessingStateId>();

            if (state == ProcessingState.Processing
                || (state == ProcessingState.Processed && externalSystemMessage.ProcessingAttemptCount == 0))
            {
                externalSystemMessage.ProcessingAttemptCount += 1;
            }

            _practiceRepository.Save(externalSystemMessage);
        }

        public bool WaitForIdle(TimeSpan timeout = default(TimeSpan))
        {
            if (timeout == default(TimeSpan)) timeout = TimeSpan.FromMilliseconds(-1);
            var syncRoot = SyncRoots.GetValue(ConfigurationManager.PracticeRepositoryConnectionString, () => new object());
            if (Monitor.TryEnter(syncRoot, timeout))
            {
                Monitor.Exit(syncRoot);

                // try again in case any locks were queued up since we started waiting the first time
                if (Monitor.TryEnter(syncRoot, timeout))
                {
                    Monitor.Exit(syncRoot);

                    return true;
                }
            }
            return false;
        }
    }
}