﻿using System.Collections.Concurrent;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using Soaf;
using Soaf.ComponentModel;
using System;
using System.Diagnostics;
using System.Linq;
using System.Transactions;

[assembly: Component(typeof(MessageAuditor), typeof(IMessageAuditor))]

namespace IO.Practiceware.Integration.Messaging
{
    /// <summary>
    ///   Audits messages.
    /// </summary>
    internal class MessageAuditor : IMessageAuditor
    {
        private readonly MessageProcessor _processor;

        private readonly IPracticeRepository _practiceRepository;
        private readonly ConcurrentDictionary<System.Threading.Tasks.Task, IMessage> _processingTasks = new ConcurrentDictionary<System.Threading.Tasks.Task, IMessage>();

        public MessageAuditor(IPracticeRepository practiceRepository, MessageProcessor processor)
        {
            _practiceRepository = practiceRepository;
            _processor = processor;
        }

        internal Transaction Transaction { get; set; }

        #region IMessageAuditor Members

        public void Audit(IMessage message, bool isOutbound, ProcessingState state)
        {
            TransactionScope ts = null;
            var transaction = Transaction;
            if (transaction != null)
            {
                ts = new TransactionScope(transaction.DependentClone(DependentCloneOption.RollbackIfNotComplete), TransactionManager.MaximumTimeout);
            }

            try
            {
                DateTime now = DateTime.UtcNow;

                var guidId = message.Id is Guid ? (Guid)message.Id : Guid.Empty;
                ExternalSystemMessage externalSystemMessage = _practiceRepository.ExternalSystemMessages.FirstOrDefault(m => (m.CorrelationId == message.Id.ToString() || m.Id == guidId)
                    && m.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType.Name == message.MessageType
                    && m.ExternalSystemMessageProcessingStateId != (int)ExternalSystemMessageProcessingStateId.Processed
                    && m.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType.IsOutbound == isOutbound);

                bool isNew = externalSystemMessage == null;

                if (isNew)
                {
                    externalSystemMessage = new ExternalSystemMessage();

                    externalSystemMessage.CorrelationId = message.Id.ToString();

                    ExternalSystemExternalSystemMessageType externalSystemMessageType = _practiceRepository.ExternalSystemExternalSystemMessageTypes.FirstOrDefault(i => (i.ExternalSystem.Name == message.Source || i.ExternalSystem.Name == message.Destination) && i.ExternalSystemMessageType.Name == message.MessageType && i.ExternalSystemMessageType.IsOutbound == isOutbound);
                    if (externalSystemMessageType == null)
                    {
                        throw new InvalidOperationException("Could not find a message type for {0} associated with external system {1} or {2}.".FormatWith(message.MessageType, message.Source, message.Destination));
                    }

                    externalSystemMessage.ExternalSystemExternalSystemMessageTypeId = externalSystemMessageType.Id;

                    externalSystemMessage.Description = message.MessageType;

                    externalSystemMessage.CreatedDateTime = now;

                    externalSystemMessage.CreatedBy = message.CreatedBy;
                }

                externalSystemMessage.Value = message.ToString();
                externalSystemMessage.UpdatedDateTime = now;
                externalSystemMessage.ExternalSystemMessageProcessingStateId = (int)state.TranslateEnum<ExternalSystemMessageProcessingStateId>();

                if (state == ProcessingState.Processing)
                {
                    externalSystemMessage.ProcessingAttemptCount += 1;
                }

                _practiceRepository.Save(externalSystemMessage);

                if (ts != null)
                {
                    ts.Complete();
                }

                if (isNew && state == ProcessingState.Unprocessed)
                {
                    // Auditing a new message? -> force it's immediate processing
                    _processingTasks
                        .TryAdd(System.Threading.Tasks.Task.Factory.StartNew(
                            () => _processor.ProcessUnprocessedMessages(em => em.Id == externalSystemMessage.Id))
                        .ContinueWith(t =>
                        {
                            IMessage value;
                            _processingTasks.TryRemove(t, out value);
                        }), message);
                }
            }
            catch (Exception ex)
            {
                var exception = new Exception("A failure occurred auditing message {0}, with content {1}.".FormatWith(message.Id, message.ToString()), ex);
                Trace.TraceError(exception.ToString());

                throw exception;
            }
            finally
            {
                if (ts != null)
                {
                    ts.Dispose();
                }
            }
        }

        #endregion

        public void WaitForIdle()
        {
            _processingTasks.Keys.ToList().ForEach(t => t.Wait());
        }
    }
}