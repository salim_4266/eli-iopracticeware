﻿namespace IO.Practiceware.Integration.Messaging
{
    /// <summary>
    ///   Defines a type of message processed by Integration services.
    /// </summary>
    public interface IMessage
    {
        /// <summary>
        ///   Gets the id.
        /// </summary>
        object Id { get; }

        /// <summary>
        ///   Gets the id this message is an acknowledgement for.
        /// </summary>
        object AcknowledgementForId { get; }

        /// <summary>
        ///   Gets a value indicating whether this message requests an acknowlegement.
        /// </summary>
        /// <value> <c>true</c> if [requests acknowledgement]; otherwise, <c>false</c> . </value>
        bool RequestsAcknowledgement { get; }

        /// <summary>
        ///   Gets the sending application of this message.
        /// </summary>
        string Source { get; }

        /// <summary>
        ///   Gets the sending application.
        /// </summary>
        string Destination { get; }

        /// <summary>
        ///   Gets a value indicating whether the processing of this message should be committed.
        /// </summary>
        /// <value> <c>true</c> if [should process]; otherwise, <c>false</c> . </value>
        bool ShouldProcess { get; }

        /// <summary>
        ///   Gets the name of the type of the message.
        /// </summary>
        /// <value> The type of the message. </value>
        string MessageType { get; }

        /// <summary>
        ///   Gets the name of the object that created this message.
        /// </summary>
        string CreatedBy { get; }

        /// <summary>
        ///   Returns the textual representation of this message.
        /// </summary>
        /// <returns> A <see cref="System.String" /> that represents this instance. </returns>
        string ToString();

        /// <summary>
        ///   Loads the specified value into this message object.
        /// </summary>
        /// <param name="value"> The value. </param>
        void Load(string value);

        /// <summary>
        ///   Gets an acknowledgement for this message
        /// </summary>
        /// <returns> </returns>
        IMessage GetAcknowledgement();
    }
}