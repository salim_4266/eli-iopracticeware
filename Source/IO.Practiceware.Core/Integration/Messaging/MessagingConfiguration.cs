﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Xml.Serialization;
using IO.Practiceware.Model;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.Configuration;

namespace IO.Practiceware.Integration.Messaging
{
    /// <summary>
    ///     Helper methods.
    /// </summary>
    public static class MessagingConfigurations
    {
        /// <summary>
        ///     Finds the endpoint for the specified name and message type.
        /// </summary>
        /// <typeparam name="TEndpoint">The type of the endpoint.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="name">The name.</param>
        /// <param name="messageType">Type of the message.</param>
        /// <returns></returns>
        public static TEndpoint For<TEndpoint>(this IEnumerable<TEndpoint> source, string name, string messageType = null) where TEndpoint : MessagingOutboundEndpointConfiguration
        {
            foreach (TEndpoint item in source.OrderBy(i => string.IsNullOrEmpty(i.MessageType) ? 1 : 0).ThenBy(i => string.IsNullOrEmpty(i.Name) ? 1 : 0))
            {
                if ((string.IsNullOrEmpty(item.MessageType) || item.MessageType.Equals(messageType, StringComparison.OrdinalIgnoreCase))
                    &&
                    (string.IsNullOrEmpty(item.Name) || item.Name.Equals(name, StringComparison.OrdinalIgnoreCase)))
                {
                    return item;
                }
            }
            return null;
        }
    }

    /// <summary>
    ///     Configuration for handlers and connectors for the Integration Service.
    /// </summary>
    [XmlRoot("messagingConfiguration")]
    public class MessagingConfiguration
    {
        public MessagingConfiguration()
        {
            FileMessageConnectorConfiguration = new MessageConnectorConfiguration<FileMessagingInboundEndpointConfiguration, FileMessagingOutboundEndpointConfiguration>();
            SocketsMessageConnectorConfiguration = new MessageConnectorConfiguration<MessagingEndpointConfiguration, SocketsMessagingOutboundEndpointConfiguration>();
            HL7MessagingConfiguration = new HL7MessagingConfiguration();
            EmailConfiguration = new SmtpConfiguration();
            TextMessageConfiguration = new SmtpConfiguration();
            WebServiceMessageConnectorConfiguration = new WebServiceMeesageConnectorConfiguration<WebServiceMessagingOutboundEndpointConfiguration>();
        }

        [XmlElement("fileMessageConnectorConfiguration")]
        public MessageConnectorConfiguration<FileMessagingInboundEndpointConfiguration, FileMessagingOutboundEndpointConfiguration> FileMessageConnectorConfiguration { get; set; }

        [XmlElement("socketsMessageConnectorConfiguration")]
        public MessageConnectorConfiguration<MessagingEndpointConfiguration, SocketsMessagingOutboundEndpointConfiguration> SocketsMessageConnectorConfiguration { get; set; }

        [XmlElement("hl7MessagingConfiguration")]
        public HL7MessagingConfiguration HL7MessagingConfiguration { get; set; }

        [XmlElement("emailConfiguration")]
        public SmtpConfiguration EmailConfiguration { get; set; }

        [XmlElement("textMessageConfiguration")]
        public SmtpConfiguration TextMessageConfiguration { get; set; }

        [XmlElement("webMessageConnectorConfiguration")]
        public WebServiceMeesageConnectorConfiguration<WebServiceMessagingOutboundEndpointConfiguration> WebServiceMessageConnectorConfiguration { get; set; }

        public static MessagingConfiguration Current
        {
            get
            {
                var settings = ConfigurationSection<MessagingConfiguration>.Current
                               ?? ApplicationSettings.Cached.GetSetting<MessagingConfiguration>(ApplicationSetting.MessagingConfiguration).IfNotNull(s => s.Value)
                               ?? new MessagingConfiguration();
                return settings;
            }
        }
    }

    /// <summary>
    ///     A type containing information for a connector.
    /// </summary>
    public class MessageConnectorConfiguration<TInboundEndpoint, TOutboundEndpoint>
        where TInboundEndpoint : MessagingEndpointConfiguration
        where TOutboundEndpoint : MessagingOutboundEndpointConfiguration
    {
        public MessageConnectorConfiguration()
        {
            InboundEndpoints = new List<TInboundEndpoint>();
            OutboundEndpoints = new List<TOutboundEndpoint>();
        }

        [XmlArray("inboundEndpoints")]
        [XmlArrayItem("endpoint")]
        public List<TInboundEndpoint> InboundEndpoints { get; set; }

        [XmlArray("outboundEndpoints")]
        [XmlArrayItem("endpoint")]
        public List<TOutboundEndpoint> OutboundEndpoints { get; set; }
    }

    public class WebServiceMeesageConnectorConfiguration<TOutboundEndpoint>
        where TOutboundEndpoint : MessagingOutboundEndpointConfiguration
    {
        public WebServiceMeesageConnectorConfiguration()
        {
            OutboundEndpoints = new List<TOutboundEndpoint>();
        }

        [XmlArray("outboundEndpoints")]
        [XmlArrayItem("endpoint")]
        public List<TOutboundEndpoint> OutboundEndpoints { get; set; }
    }

    public class FileMessagingOutboundEndpointConfiguration : MessagingOutboundEndpointConfiguration
    {
        [XmlAttribute("value")]
        public override string Value
        {
            get { return base.Value; }
            set
            {
                if (value != null)
                {
                    value = Environment.ExpandEnvironmentVariables(value);

                    if (Value != null && !Path.IsPathRooted(value))
                    {
                        value = Path.Combine(Value, value);
                    }
                    base.Value = value;
                }
            }
        }
    }

    public class FileMessagingInboundEndpointConfiguration : MessagingEndpointConfiguration
    {
        private string _failedPath;
        private string _processedPath;

        [XmlAttribute("value")]
        public override string Value
        {
            get { return base.Value; }
            set
            {
                if (value != null)
                {
                    value = Environment.ExpandEnvironmentVariables(value);

                    if (Value != null && !Path.IsPathRooted(value))
                    {
                        value = Path.Combine(Value, value);
                    }
                    base.Value = value;
                }
            }
        }

        /// <summary>
        ///     Gets or sets the path where processed files are moved to.
        /// </summary>
        /// <value>
        ///     The processed path.
        /// </value>
        [XmlAttribute("processedPath")]
        public string ProcessedPath
        {
            get { return string.IsNullOrEmpty(_processedPath) ? Path.Combine(Value ?? string.Empty, "Processed") : _processedPath; }
            set
            {
                if (value != null)
                {
                    value = Environment.ExpandEnvironmentVariables(value);

                    if (Value != null && !Path.IsPathRooted(value))
                    {
                        value = Path.Combine(Value, value);
                    }
                }
                _processedPath = value;
            }
        }

        /// <summary>
        ///     Gets or sets the path where failed files are moved to.
        /// </summary>
        /// <value>
        ///     The failed path.
        /// </value>
        [XmlAttribute("failedPath")]
        public string FailedPath
        {
            get { return string.IsNullOrEmpty(_failedPath) ? Path.Combine(Value ?? string.Empty, "Failed") : _failedPath; }
            set
            {
                if (value != null)
                {
                    value = Environment.ExpandEnvironmentVariables(value);

                    if (Value != null && !Path.IsPathRooted(value))
                    {
                        value = Path.Combine(Value, value);
                    }
                }
                _failedPath = value;
            }
        }
    }

    public class SocketsMessagingOutboundEndpointConfiguration : MessagingOutboundEndpointConfiguration
    {
        [XmlAttribute("requestAcknowledgements")]
        public bool RequestAcknowledgements { get; set; }
    }

    public class MessagingEndpointConfiguration
    {
        /// <summary>
        ///     Gets the name of the endpoint (the external system name).
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        ///     Gets the value indicating the destination of the endpoint.
        /// </summary>
        [XmlAttribute("value")]
        public virtual string Value { get; set; }
    }

    public class MessagingOutboundEndpointConfiguration : MessagingEndpointConfiguration
    {
        /// <summary>
        ///     Gets or sets the type of the message this endpoint corresponds to.
        /// </summary>
        /// <value>
        ///     The type of the message.
        /// </value>
        [XmlAttribute("messageType")]
        public string MessageType { get; set; }
    }


    public class WebServiceMessagingOutboundEndpointConfiguration : MessagingOutboundEndpointConfiguration
    {
        /// <summary>
        ///     Gets or sets the type of the message this endpoint corresponds to.
        /// </summary>
        /// <value>
        ///     The type of the message.
        /// </value>
        [XmlAttribute("methodType")]
        public string MethodType { get; set; }

        /// <summary>
        ///     Gets or sets the type of the message this endpoint corresponds to.
        /// </summary>
        /// <value>
        ///     The type of the message.
        /// </value>
        [XmlAttribute("contentType")]
        public string ContentType { get; set; }

        [XmlAttribute("methodName")]
        public string MethodName { get; set; }

        [XmlAttribute("requestAcknowledgements")]
        public bool RequestAcknowledgements { get; set; }

        [XmlAttribute("authenticationRequired")]
        public bool AuthenticationRequired { get; set; }

        [XmlAttribute("userName")]
        public string UserName { get; set; }

        [XmlAttribute("password")]
        public string Password { get; set; }
    }

    public class HL7MessagingConfiguration
    {
        /// <summary>
        ///     Gets or sets a value indicating whether [process all Outbound messages]. If true, will also process non-production level messages.
        /// </summary>
        /// <value>
        ///     <c>true</c> if [process all Outbound messages]; otherwise, <c>false</c> .
        /// </value>
        [XmlAttribute("processAllInboundMessages")]
        public bool ProcessAllInboundMessages { get; set; }

        /// <summary>
        ///     Gets or sets the message processing id to use for outbound messages
        /// </summary>
        /// <value> The message processing id. </value>
        [XmlAttribute("outboundMessagesProcessingId")]
        public string OutboundMessagesProcessingId { get; set; }
    }

    public class SmtpConfiguration
    {
        /// <summary>
        ///     Gets or sets the SMTP delivery method. The default delivery method is
        ///     <see
        ///         cref="F:System.Net.Mail.SmtpDeliveryMethod.Network" />
        ///     .
        /// </summary>
        /// <returns> A string that represents the SMTP delivery method. </returns>
        [XmlAttribute("deliveryMethod")]
        public SmtpDeliveryMethod DeliveryMethod { get; set; }

        /// <summary>
        ///     Gets or sets the default value that indicates who the email message is from.
        /// </summary>
        /// <returns> A string that represents the default value indicating who a mail message is from. </returns>
        [XmlAttribute("from")]
        public string From { get; set; }


        /// <summary>
        ///     Gets a <see cref="T:System.Net.Configuration.SmtpNetworkElement" /> .
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.Net.Configuration.SmtpNetworkElement" /> object.
        /// </returns>
        [XmlElement("network")]
        public SmtpNetworkConfiguration Network { get; set; }

        /// <summary>
        ///     Gets the pickup directory that will be used by the SMPT client.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.Net.Configuration.SmtpSpecifiedPickupDirectoryElement" /> object that specifies the pickup directory folder.
        /// </returns>
        [XmlElement("specifiedPickupDirectory")]
        public SmtpSpecifiedPickupDirectoryConfiguration SpecifiedPickupDirectory { get; set; }
    }

    /// <summary>
    ///     Represents an SMTP pickup directory configuration element.
    /// </summary>
    public class SmtpSpecifiedPickupDirectoryConfiguration
    {
        private string _pickupDirectoryLocation;

        /// <summary>
        ///     Gets or sets the folder where applications save mail messages to be processed by the SMTP server.
        /// </summary>
        /// <returns> A string that specifies the pickup directory for e-mail messages. </returns>
        [XmlAttribute("pickupDirectoryLocation")]
        public string PickupDirectoryLocation
        {
            get { return _pickupDirectoryLocation; }
            set { _pickupDirectoryLocation = value != null ? Environment.ExpandEnvironmentVariables(value) : null; }
        }
    }

    /// <summary>
    ///     Represents the network element in the SMTP configuration file. This class cannot be inherited.
    /// </summary>
    public class SmtpNetworkConfiguration
    {
        private int _port = 25;

        /// <summary>
        ///     Determines whether or not default user credentials are used to access an SMTP server. The default value is false.
        /// </summary>
        /// <returns> true indicates that default user credentials will be used to access the SMTP server; otherwise, false. </returns>
        [XmlAttribute("defaultCredentials")]
        public bool DefaultCredentials { get; set; }

        /// <summary>
        ///     Gets or sets the name of the SMTP server.
        /// </summary>
        /// <returns> A string that represents the name of the SMTP server to connect to. </returns>
        [XmlAttribute("host")]
        public string Host { get; set; }

        /// <summary>
        ///     Gets or sets the Service Provider Name (SPN) to use for authentication when using extended protection to connect to an SMTP mail server.
        /// </summary>
        /// <returns> A string that represents the SPN to use for authentication when using extended protection to connect to an SMTP mail server. </returns>
        [XmlAttribute("targetName")]
        public string TargetName { get; set; }

        /// <summary>
        ///     Gets or sets the client domain name used in the initial SMTP protocol request to connect to an SMTP mail server.
        /// </summary>
        /// <returns> A string that represents the client domain name used in the initial SMTP protocol request to connect to an SMTP mail server. </returns>
        [XmlAttribute("clientDomain")]
        public string ClientDomain { get; set; }

        /// <summary>
        ///     Gets or sets the user password to use to connect to an SMTP mail server.
        /// </summary>
        /// <returns> A string that represents the password to use to connect to an SMTP mail server. </returns>
        [XmlAttribute("password")]
        public string Password { get; set; }

        /// <summary>
        ///     Gets or sets the port that SMTP clients use to connect to an SMTP mail server. The default value is 25.
        /// </summary>
        /// <returns> A string that represents the port to connect to an SMTP mail server. </returns>
        [XmlAttribute("port")]
        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }


        /// <summary>
        ///     Gets or sets the user name to connect to an SMTP mail server.
        /// </summary>
        /// <returns> A string that represents the user name to connect to an SMTP mail server. </returns>
        [XmlAttribute("userName")]
        public string UserName { get; set; }

        /// <summary>
        ///     Gets or sets whether SSL is used to access an SMTP mail server. The default value is false.
        /// </summary>
        /// <returns> true indicates that SSL will be used to access the SMTP mail server; otherwise, false. </returns>
        [XmlAttribute("enableSsl")]
        public bool EnableSsl { get; set; }
    }
}