﻿namespace IO.Practiceware.Integration.Messaging
{
    /// <summary>
    ///   Audits message transactions.
    /// </summary>
    public interface IMessageAuditor
    {
        /// <summary>
        ///   Audits the specified message.
        /// </summary>
        /// <param name="message"> The message. </param>
        /// <param name="isOutbound"> if set to <c>true</c> [is outbound]. </param>
        /// <param name="state"> The state. </param>
        void Audit(IMessage message, bool isOutbound, ProcessingState state);
    }

    public enum ProcessingState
    {
        Unprocessed,
        Processing,
        Processed
    }
}