﻿using System;
using System.Collections.Generic;

namespace IO.Practiceware.Integration.Messaging
{
    public class SendMessageRequest
    {
        public SendMessageRequest()
        {
            Handlers = new List<Type>();
        }

        public IMessage Message { get; set; }

        /// <summary>
        /// Gets the handlers. Handlers should add their type to this collection for tracing purposes.
        /// </summary>
        /// <value>
        /// The handlers.
        /// </value>
        public IList<Type> Handlers { get; private set; }
    }
}