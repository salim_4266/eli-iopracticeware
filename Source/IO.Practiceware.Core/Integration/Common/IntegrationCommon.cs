﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using Soaf.Data;

namespace IO.Practiceware.Integration.Common
{
    public static class IntegrationCommon
    {
        public static Invoice ReplaceInvoiceWithIcd10(Invoice invoice)
        {
            if (invoice == null) return null;

            using (IDbConnection connection = DbConnectionFactory.PracticeRepository)
            {
                var query = " SELECT * FROM Model.BillingDiagnosisIcd10 WHERE InvoiceId IN (" + invoice.Id + ") AND (DiagnosisCode <> '' OR DiagnosisCode IS NOT NULL) ";
                var billingDiagnosesIcd10 = connection.Execute<DataTable>(query).AsEnumerable().ToList();
                var billingDiagnoses = new List<BillingDiagnosis>();
                var billingServices = invoice.BillingServices;

                foreach (var billingService in billingServices)
                {
                    billingService.BillingServiceBillingDiagnoses.Clear();
                }

                foreach (var billingDiagnosisIcd10 in billingDiagnosesIcd10)
                {
                    var dr = billingDiagnosisIcd10;
                    var billingServiceBillingDiagnoses = new List<BillingServiceBillingDiagnosis>();

                    var billingDiagnosis = new BillingDiagnosis();
                    billingDiagnosis.Id = (int)dr["Id"];
                    billingDiagnosis.OrdinalId = (int)dr["OrdinalId"];
                    billingDiagnosis.InvoiceId = (int)dr["InvoiceId"];
                    billingDiagnosis.ExternalSystemEntityMapping = new ExternalSystemEntityMapping { Id = 0, ExternalSystemEntityKey = (string)dr["DiagnosisCode"] };

                    query = " SELECT bsbd.BillingServiceId,bsbd.BillingDiagnosisId,bsbd.OrdinalId,bsbd.IsIcd10 FROM model.BillingDiagnosisIcd10 diag10  " +
                            " INNER JOIN model.BillingServiceBillingDiagnosis bsbd ON bsbd.BillingDiagnosisId = diag10.Id " +
                            " WHERE diag10.InvoiceId IN (" + invoice.Id + ") AND bsbd.BillingDiagnosisId = " + dr["Id"] + " " +
                            " AND (diag10.DiagnosisCode <> '' OR diag10.DiagnosisCode IS NOT NULL ) AND bsbd.IsIcd10 = 1 ";

                    var existingbillingServiceBillingDiagnoses = connection.Execute<DataTable>(query).AsEnumerable().ToList();

                    foreach (var existingbillingServiceBillingDiagnosis in existingbillingServiceBillingDiagnoses)
                    {
                        var drBillingServiceBillingDiagnosis = existingbillingServiceBillingDiagnosis;
                        var billingServiceBillingDiagnosis = new BillingServiceBillingDiagnosis();
                        billingServiceBillingDiagnosis.BillingDiagnosisId = (int)drBillingServiceBillingDiagnosis["BillingDiagnosisId"];
                        billingServiceBillingDiagnosis.BillingServiceId = (int)drBillingServiceBillingDiagnosis["BillingServiceId"];
                        billingServiceBillingDiagnosis.OrdinalId = (int)drBillingServiceBillingDiagnosis["OrdinalId"];
                        billingServiceBillingDiagnosis.IsIcd10 = true;
                        billingServiceBillingDiagnosis.BillingDiagnosis = billingDiagnosis;
                        billingServiceBillingDiagnoses.Add(billingServiceBillingDiagnosis);
                    }

                    billingDiagnosis.BillingServiceBillingDiagnoses = billingServiceBillingDiagnoses;
                    billingDiagnoses.Add(billingDiagnosis);

                    foreach (var billingService in billingServices)
                    {
                        var correctedbillingServiceBillingDiagnoses = billingServiceBillingDiagnoses.FirstOrDefault(i => i.BillingServiceId == billingService.Id);
                        if (correctedbillingServiceBillingDiagnoses != null) billingService.BillingServiceBillingDiagnoses.Add(correctedbillingServiceBillingDiagnoses);
                    }
                }

                invoice.BillingDiagnoses = billingDiagnoses;
                invoice.BillingServices = billingServices;
            }
            return invoice;
        }
    }
}
