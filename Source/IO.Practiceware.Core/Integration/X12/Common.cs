﻿using IO.Practiceware.Model;
using System.Collections.Generic;

namespace IO.Practiceware.Integration.X12
{
    internal static class Common
    {
        public static readonly Dictionary<ClaimAdjustmentGroupCode, string> ClaimAdjustmentGroupCodeMappings = new Dictionary<ClaimAdjustmentGroupCode, string>
            {
                {ClaimAdjustmentGroupCode.ContractualObligation, "CO"},
                {ClaimAdjustmentGroupCode.CorrectionReversal, "CR"},
                {ClaimAdjustmentGroupCode.OtherAdjustment, "OA"},
                {ClaimAdjustmentGroupCode.PatientResponsibility, "PR"},
                {ClaimAdjustmentGroupCode.PayerInitiatedReduction, "PI"}
            };

        public static readonly Dictionary<PolicyHolderRelationshipType, string> PolicyHolderRelationshipTypeMappings = new Dictionary<PolicyHolderRelationshipType, string>
            {
                { PolicyHolderRelationshipType.Self, "18" },
                { PolicyHolderRelationshipType.Spouse, "01" },
                { PolicyHolderRelationshipType.Child, "19" },
                { PolicyHolderRelationshipType.Employee, "20" },
                { PolicyHolderRelationshipType.Unknown, "21" },
                { PolicyHolderRelationshipType.LifePartner, "53" },
                { PolicyHolderRelationshipType.OtherRelationship, "G8" },
                { PolicyHolderRelationshipType.OrganDonor, "39" },
                { PolicyHolderRelationshipType.CadaverDonor, "40" }
            };

        public static readonly Dictionary<MedicareSecondaryReasonCode, string> MedicareSecondaryReasonCodeMappings = new Dictionary<MedicareSecondaryReasonCode, string>
            { 
                { MedicareSecondaryReasonCode.WorkingAged, "12" },
                { MedicareSecondaryReasonCode.EndStageRenalDisease, "13" },
                { MedicareSecondaryReasonCode.NoFault, "14" },
                { MedicareSecondaryReasonCode.WorkersCompensation, "15" },
                { MedicareSecondaryReasonCode.PublicHealthService, "16" },
                { MedicareSecondaryReasonCode.BlackLung, "41" },
                { MedicareSecondaryReasonCode.VeteransAdministration, "42" },
                { MedicareSecondaryReasonCode.LargeGroupHealthPlan, "43" },
                { MedicareSecondaryReasonCode.OtherLiabilityInsurance, "47" }
            };

        public static readonly Dictionary<int, string> PayerResponsibilitySequenceMap = new Dictionary<int, string>
            {
                { 0, "P" },
                { 1, "S" },
                { 2, "T" },
                { 3, "A" },
                { 4, "B" },
                { 5, "C" },
                { 6, "D" },
                { 7, "E" },
                { 8, "F" },
                { 9, "G" },
                { 99, "U" }
            };

        public static readonly Dictionary<ClaimFilingIndicatorCode, string> ClaimFilingIndicatorCodeMappings = new Dictionary<ClaimFilingIndicatorCode, string>
            {
                { ClaimFilingIndicatorCode.CentralCertification, "10" },
                { ClaimFilingIndicatorCode.OtherNonFederalPrograms, "11" },
                { ClaimFilingIndicatorCode.PreferredProviderOrganization, "12" },
                { ClaimFilingIndicatorCode.PointOfService, "13" },
                { ClaimFilingIndicatorCode.ExclusiveProviderOrganization, "14" },
                { ClaimFilingIndicatorCode.IdemnityInsurance, "15" },
                { ClaimFilingIndicatorCode.HealthMaintenanceOrganizationMedicareRisk, "16" },
                { ClaimFilingIndicatorCode.DentalMaintenanceOrganization, "17" },
                { ClaimFilingIndicatorCode.AutomobileMedical, "AM" },
                { ClaimFilingIndicatorCode.BlueCrossBlueShield, "BL" },
                { ClaimFilingIndicatorCode.Champus, "CH" },
                { ClaimFilingIndicatorCode.CommercialInsurance, "CI" },
                { ClaimFilingIndicatorCode.Disability, "DS" },
                { ClaimFilingIndicatorCode.FederalEmployeesProgram, "FI" },
                { ClaimFilingIndicatorCode.HealthMaintenanceOrganization, "HM" },
                { ClaimFilingIndicatorCode.LiabilityMedical, "LM" },
                { ClaimFilingIndicatorCode.MedicarePartA, "MA" },
                { ClaimFilingIndicatorCode.MedicarePartB, "MB" },
                { ClaimFilingIndicatorCode.Medicaid, "MC" },
                { ClaimFilingIndicatorCode.OtherFederalProgram, "OF" },
                { ClaimFilingIndicatorCode.TitleV, "TV" },
                { ClaimFilingIndicatorCode.VeteransAffairsPlan, "VA" },
                { ClaimFilingIndicatorCode.WorkersCompensation, "WC" },
                { ClaimFilingIndicatorCode.MutuallyDefined, "ZZ" }
            };

        public static readonly Dictionary<DrugUnitOfMeasurement, string> DrugUnitOfMeasurementMappings = new Dictionary<DrugUnitOfMeasurement, string>
            {
                { DrugUnitOfMeasurement.InternationalUnit, "F2" },
                { DrugUnitOfMeasurement.Gram, "GR" },
                { DrugUnitOfMeasurement.Milligram, "ME" },
                { DrugUnitOfMeasurement.Milliliter, "ML" },
                { DrugUnitOfMeasurement.Unit, "UN" }
            };

        public static readonly Dictionary<ServiceUnitOfMeasurement, string> ServiceUnitOfMeasurementMappings = new Dictionary<ServiceUnitOfMeasurement, string>
            {
                { ServiceUnitOfMeasurement.Minute, "MJ" },
                { ServiceUnitOfMeasurement.Unit, "UN" }
            };

        public static readonly Dictionary<RelatedCause, string> RelatedCauseMappings = new Dictionary<RelatedCause, string>
            {
                { RelatedCause.AutoAccident, "AA" },
                { RelatedCause.Employment, "EM" },
                { RelatedCause.OtherAccident, "OA" }
            };
    }
}