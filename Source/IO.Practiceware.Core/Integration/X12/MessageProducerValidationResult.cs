﻿using IO.Practiceware.Model;

namespace IO.Practiceware.Integration.X12
{
    public class MessageProducerValidationResult
    {
        private readonly string _errorMessage;
        private readonly Invoice _invoice;

        public MessageProducerValidationResult(string errorMessage)
        {
            _errorMessage = errorMessage;
            _invoice = null;
        }

        public MessageProducerValidationResult(string errorMessage, Invoice invoice)
        {
            _errorMessage = errorMessage;
            _invoice = invoice;
        }

        public string ErrorMessage
        {
            get
            {
                if (_invoice != null)
                {
                    return _errorMessage + " PatientId: " + _invoice.Encounter.PatientId + ", Date of Service: " + _invoice.Encounter.StartDateTime.ToShortDateString();
                }
                return _errorMessage;
            } 
        }

        public Invoice Invoice 
        {
            get { return _invoice; }
        }
    }
}
