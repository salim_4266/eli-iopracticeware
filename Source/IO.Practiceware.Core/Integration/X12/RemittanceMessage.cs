using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace IO.Practiceware.Integration.X12
{
    /// <summary>
    /// Parsed remittance message representation
    /// </summary>
    /// <remarks>
    /// Important: classes should be highly optimized for minimum footprint when storing as Xml
    /// </remarks>
    [XmlRoot("rm")]
    public class RemittanceMessage
    {
        private List<RemittanceAdvice> _advices;
        private List<string> _criticalErrors;

        public RemittanceMessage()
        {
            _advices = new List<RemittanceAdvice>();
            _criticalErrors = new List<string>();
        }

        [XmlArray("advs")]
        [XmlArrayItem("adv")]
        public List<RemittanceAdvice> Advices
        {
            get { return _advices; }
        }

        /// <summary>
        /// Gets the list of critical errors loading the message.
        /// </summary>
        [XmlArray("ces")]
        [XmlArrayItem("ce")]
        public List<string> CriticalErrors
        {
            get { return _criticalErrors; }
        }

        [XmlText]
        public string SourceX12Message { get; set; }

        public class RemittanceAdvice
        {
            private readonly List<RemittanceAdviceClaimPayment> _claimPayments;
            private readonly List<string> _errors;

            public RemittanceAdvice()
            {
                _claimPayments = new List<RemittanceAdviceClaimPayment>();
                Informations = new List<string>();
                _errors = new List<string>();
            }

            [XmlArray("cps")]
            [XmlArrayItem("cp")]
            public List<RemittanceAdviceClaimPayment> ClaimPayments
            {
                get { return _claimPayments; }
            }

            [XmlArray("infs")]
            [XmlArrayItem("inf")]
            public List<string> Informations { get; set; }

            [XmlArray("ers")]
            [XmlArrayItem("er")]
            public List<string> Errors
            {
                get { return _errors; }
            }

            [XmlAttribute("ta")]
            public decimal TotalAmount { get; set; }

            [XmlAttribute("c")]
            public string Code { get; set; }

            [XmlAttribute("pd")]
            public DateTime PostingDate { get; set; }

            [XmlArray("pids")]
            [XmlArrayItem("pid")]
            public List<string> PayerIdentifiers { get; set; }

            /// <summary>
            /// Resolved insurer name by payer identifiers
            /// </summary>
            [XmlAttribute("rin")]
            public string ResolvedInsurerName { get; set; }
        }

        public class RemittanceAdviceClaimPayment
        {
            private readonly List<RemittanceAdviceServicePayment> _servicePayments;
            private readonly List<string> _errors;

            public RemittanceAdviceClaimPayment()
            {
                _servicePayments = new List<RemittanceAdviceServicePayment>();
                _errors = new List<string>();
            }

            [XmlArray("sps")]
            [XmlArrayItem("sp")]
            public List<RemittanceAdviceServicePayment> ServicePayments
            {
                get { return _servicePayments; }
            }

            [XmlElement("pid")] // Element, since Nullable is complex type and can't appear as attribute
            public int? PatientId { get; set; }

            [XmlAttribute("csc")]
            public string ClaimStatusCode { get; set; }

            /// <summary>
            /// Gets the resolved claim status code.
            /// </summary>
            /// <value>
            /// The resolved claim status code.
            /// </value>
            public KnownClaimStatusCode ResolvedClaimStatusCode {
                get
                {
                    int code;
                    var statusCode = KnownClaimStatusCode.Unknown;
                    if (int.TryParse(ClaimStatusCode, out code))
                    {
                        statusCode = (KnownClaimStatusCode)code;
                        if (!Enum.IsDefined(typeof(KnownClaimStatusCode), statusCode))
                        {
                            statusCode = KnownClaimStatusCode.Unknown;
                        }
                    }

                    return statusCode;
                }
            }

            [XmlAttribute("icq")]
            public string IdentificationCodeQualifier { get; set; }

            [XmlAttribute("ic")]
            public string IdentificationCode { get; set; }

            [XmlArray("ers")]
            [XmlArrayItem("er")]
            public List<string> Errors
            {
                get { return _errors; }
            }
        }

        public class RemittanceAdviceServicePayment
        {
            private readonly List<string> _errors;
            private readonly List<RemittanceAdviceServiceAdjustment> _serviceAdjustments;

            public RemittanceAdviceServicePayment()
            {
                _serviceAdjustments = new List<RemittanceAdviceServiceAdjustment>();
                ProcedureModifiers = new List<string>();
                SecondaryProcedureModifiers = new List<string>();
                _errors = new List<string>();
            }

            [XmlArray("ers")]
            [XmlArrayItem("er")]
            public List<string> Errors
            {
                get { return _errors; }
            }

            [XmlAttribute("sd")]
            public DateTime ServiceDate { get; set; }

            [XmlElement("bsid")] // Element, since Nullable is complex type and can't appear as attribute
            public int? BillingServiceId { get; set; }

            /// <summary>
            /// Identifier from SVC06
            /// </summary>
            /// <value>
            /// The procedure identifier.
            /// </value>
            [XmlAttribute("pi")]
            public string ProcedureIdentifier { get; set; }

            /// <summary>
            /// Identifier from SVC01
            /// </summary>
            /// <value>
            /// The secondary procedure identifier.
            /// </value>
            [XmlAttribute("spi")]
            public string SecondaryProcedureIdentifier { get; set; }

            /// <summary>
            /// Modifiers from SVC06
            /// </summary>
            /// <value>
            /// The procedure modifiers.
            /// </value>
            [XmlArray("pms")]
            [XmlArrayItem("pm")]
            public List<string> ProcedureModifiers { get; set; }

            /// <summary>
            /// Modifiers from SVC01
            /// </summary>
            /// <value>
            /// The secondary procedure modifiers.
            /// </value>
            [XmlArray("spms")]
            [XmlArrayItem("spm")]
            public List<string> SecondaryProcedureModifiers { get; set; }

            [XmlArray("sas")]
            [XmlArrayItem("sa")]
            public List<RemittanceAdviceServiceAdjustment> ServiceAdjustments
            {
                get { return _serviceAdjustments; }
            }
        }

        public class RemittanceAdviceServiceAdjustment
        {
            private readonly List<string> _errors;

            public RemittanceAdviceServiceAdjustment()
            {
                _errors = new List<string>();
            }

            [XmlArray("ers")]
            [XmlArrayItem("er")]
            public List<string> Errors
            {
                get { return _errors; }
            }

            [XmlAttribute("gc")]
            public string GroupCode { get; set; }

            [XmlAttribute("rc")]
            public string ReasonCode { get; set; }

            [XmlAttribute("am")]
            public decimal Amount { get; set; }

            [XmlAttribute("q")]
            public decimal Quantity { get; set; }

            [XmlAttribute("ispp")]
            public bool IsPrimaryPayment { get; set; }
        }

        public enum KnownClaimStatusCode
        {
            Unknown = 0,
            ProcessedPrimary = 1,
            ProcessedSecondary = 2,
            ProcessedTertiary = 3,
            Denied = 4,
            ProcessedPrimaryAutoCrossed = 19,
            ProcessedSecondaryAutoCrossed = 20,
            ProcessedTertiaryAutoCrossed = 21,
            ReversalOfPreviousPayment = 22,
            NotProcessedForwarded = 23,
            PredeterminationPricingOnly = 25
        }
    }
}