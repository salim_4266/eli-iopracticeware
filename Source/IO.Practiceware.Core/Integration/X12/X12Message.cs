﻿using IO.Practiceware.Integration.Messaging;
using OopFactory.X12.Parsing.Model;
using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace IO.Practiceware.Integration.X12
{
    public class X12Message : IMessage
    {
        private readonly string _createdBy;
        private readonly List<Interchange> _interchanges;

        public X12Message()
        {
            _interchanges = new List<Interchange>();
            Type declaringType = new StackTrace().GetFrame(1).GetMethod().DeclaringType;
            if (declaringType != null) _createdBy = declaringType.Name;
        }

        public X12Message(Interchange interchange)
        {
            _interchanges = new List<Interchange>();
            Type declaringType = new StackTrace().GetFrame(1).GetMethod().DeclaringType;
            if (declaringType != null) _createdBy = declaringType.Name;
            _interchanges.Add(interchange);
        }

        public X12Message(IEnumerable<Interchange> interchanges)
        {
            _interchanges = new List<Interchange>();
            _interchanges.AddRange(interchanges);
        }

        /// <summary>
        /// Gets a non-null array of Interchanges.
        /// </summary>
        public Interchange[] Messages
        {
            get { return _interchanges.ToArray(); }
        }

        #region IMessage Members

        public object Id
        {
            get
            {
                return _interchanges.Any() ? _interchanges.First().FunctionGroups.First().Transactions.Select(i => i.ControlNumber).FirstOrDefault() : null;
            }
        }

        public object AcknowledgementForId
        {
            get { return null; }
        }

        public string CreatedBy
        {
            get { return _createdBy; }
        }

        public string Source
        {
            get { return Constants.LocalApplicationId; }
        }

        public string Destination
        {
            get { return string.Empty; }
        }

        public bool ShouldProcess
        {
            get { return true; }
        }

        public string MessageType
        {
            get { return _interchanges.Any() ? _interchanges.First().FunctionGroups.First().Transactions.Select(i => i.IdentifierCode).FirstOrDefault() : null; }
        }

        public override string ToString()
        {
            return _interchanges.Select(i => i.SerializeToX12(false).Replace(Environment.NewLine, string.Empty) ).Join(Environment.NewLine);
        }

        public void Load(string value)
        {
            _interchanges.AddRange(X12Parser.Parse(value));
        }

        public IMessage GetAcknowledgement()
        {
            return null;
        }

        public bool RequestsAcknowledgement
        {
            get { return false; }
        }

        #endregion
    }
}