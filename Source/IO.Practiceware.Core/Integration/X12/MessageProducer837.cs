﻿using System.Collections.ObjectModel;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Integration.X12;
using IO.Practiceware.Model;
using OopFactory.X12.Parsing.Model;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;

[assembly: Component(typeof(MessageProducer837.ProduceMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.X12
{
    /// <summary>
    ///   Produces X12 837 messages.
    /// </summary>
    [SupportsTransactionScopes]
    public class MessageProducer837
    {
        public class ProduceMessageSubscriber
        {
            public ProduceMessageSubscriber(IMessenger messenger, Func<MessageProducer837> messageProducer)
            {
                messenger.Subscribe<ProduceMessageRequest>(
                    (s, t, m) => m.ProducedMessage = messageProducer().ProduceMessage(m.ExternalSystemMessage) ?? m.ProducedMessage,
                    m => m.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.X12_837_Outbound);
            }
        }

        private readonly IPracticeRepository _practiceRepository;
        private readonly bool _removeNonQueuedBillingServiceTransactions;
        private readonly MessageProducer837I _messageProducer837I;
        private readonly MessageProducer837P _messageProducer837P;
        private readonly MessageProducerUtilities _messageProducerUtilities;

        public MessageProducer837(
                IPracticeRepository practiceRepository,
                MessageProducerUtilities messageProducerUtilities,
                Func<MessageProducerUtilities, MessageProducer837I> getMessageProducer837I,
                Func<MessageProducerUtilities, MessageProducer837P> getMessageProducer837P)
        {
            // TODO: why we need this unused switch?
            _removeNonQueuedBillingServiceTransactions = true;

            _practiceRepository = practiceRepository;

            // Ensure same instance of message producer utilities is used
            _messageProducerUtilities = messageProducerUtilities;
            _messageProducer837I = getMessageProducer837I(_messageProducerUtilities);
            _messageProducer837P = getMessageProducer837P(_messageProducerUtilities);
        }

        public IMessage ProduceMessage(ExternalSystemMessage externalSystemMessage)
        {
            Guid[] billingServiceTransactionIds = externalSystemMessage.ExternalSystemMessagePracticeRepositoryEntities.Where(e => e.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.BillingServiceTransaction).Select(i => new Guid(i.PracticeRepositoryEntityKey)).ToArray();

            return ProduceMessage(billingServiceTransactionIds);
        }

        public IMessage ProduceMessage(IEnumerable<string> billingServiceTransactionIds)
        {
            return ProduceMessage(billingServiceTransactionIds.Select(i => new Guid(i)));
        }

        public IMessage ProduceMessage(IEnumerable<Guid> billingServiceTransactionIds, int? claimFormTypeId = null)
        {
            using (new TimedScope(s => Debug.WriteLine("Generated 837 message in {0}".FormatWith(s))))
            {
                var billingServiceTransactions = LoadAllInvoicesForBillingServiceTransactions(billingServiceTransactionIds);

                BillingServiceTransaction[] electronicServiceTransactions = billingServiceTransactions.RemoveAll(billingServiceTransactions.
                                                                                                                 Where(bst => bst.MethodSent == MethodSent.Paper).
                                                                                                                 ForEachWithSinglePropertyChangedNotification(bst => { throw new ValidationException("Cannot produce message for a paper billing service transaction."); })).ToArray();

                Interchange interchange;
                var interchanges = new List<Interchange>();


                if (claimFormTypeId != null)
                {
                    if (((ClaimFormTypeId)claimFormTypeId) == ClaimFormTypeId.Professional837)
                    {
                        if (_messageProducer837P.TryProduceInterchange(electronicServiceTransactions, out interchange))
                        {
                            interchanges.Add(interchange);
                        }
                    }
                    else if (((ClaimFormTypeId)claimFormTypeId) == ClaimFormTypeId.Institutional837)
                    {
                        if (_messageProducer837I.TryProduceInterchange(electronicServiceTransactions, out interchange))
                        {
                            interchanges.Add(interchange);
                        }
                    }
                }
                else
                {

                    if (_messageProducer837I.TryProduceInterchange(electronicServiceTransactions, out interchange))
                    {
                        interchanges.Add(interchange);
                    }
                    if (_messageProducer837P.TryProduceInterchange(electronicServiceTransactions, out interchange))
                    {
                        interchanges.Add(interchange);
                    }
                }

                _messageProducerUtilities.DateTime = null;

                return interchanges.Any() ? new X12Message(interchanges.ToArray()) : null;
            }
        }

        /// <summary>
        /// Produce message along with validation results for invalid BillingServiceTransactionIds(for excluded invoices) 
        /// </summary>
        /// <param name="billingServiceTransactionIds"></param>
        /// <param name="validationResults"></param>
        /// <param name="excludedTransactionIds"></param>
        /// <param name="claimFormTypeId"></param>
        /// <returns></returns>
        public IMessage ProduceMessage(IEnumerable<Guid> billingServiceTransactionIds, out IList<string> validationResults, out IEnumerable<Guid> excludedTransactionIds, int? claimFormTypeId = null)
        {
            _messageProducerUtilities.ValidationResults = new Collection<MessageProducerValidationResult>();
            validationResults = new List<string>();
            excludedTransactionIds = new List<Guid>();
            using (new TimedScope(s => Debug.WriteLine("Generated 837 message in {0}".FormatWith(s))))
            {
                var billingServiceTransactions = LoadAllInvoicesForBillingServiceTransactions(billingServiceTransactionIds);

                BillingServiceTransaction[] electronicServiceTransactions = billingServiceTransactions.RemoveAll(billingServiceTransactions.
                                                                                                                 Where(bst => bst.MethodSent == MethodSent.Paper).
                                                                                                                 ForEachWithSinglePropertyChangedNotification(bst => { throw new ValidationException("Cannot produce message for a paper billing service transaction."); })).ToArray();

                Interchange interchange;
                var interchanges = new List<Interchange>();


                if (claimFormTypeId != null)
                {
                    if (((ClaimFormTypeId)claimFormTypeId) == ClaimFormTypeId.Professional837)
                    {
                        if (_messageProducer837P.TryProduceInterchange(electronicServiceTransactions, out interchange))
                        {
                            interchanges.Add(interchange);
                        }
                    }
                    else if (((ClaimFormTypeId)claimFormTypeId) == ClaimFormTypeId.Institutional837)
                    {
                        if (_messageProducer837I.TryProduceInterchange(electronicServiceTransactions, out interchange))
                        {
                            interchanges.Add(interchange);
                        }
                    }
                }
                else
                {

                    if (_messageProducer837I.TryProduceInterchange(electronicServiceTransactions, out interchange))
                    {
                        interchanges.Add(interchange);
                    }
                    if (_messageProducer837P.TryProduceInterchange(electronicServiceTransactions, out interchange))
                    {
                        interchanges.Add(interchange);
                    }
                }

                _messageProducerUtilities.DateTime = null;
                if (_messageProducerUtilities.ValidationResults.Any())
                {
                    validationResults = _messageProducerUtilities.ValidationResults.Select(v => v.ErrorMessage).ToList();
                    var invoiceIds = _messageProducerUtilities.ValidationResults.Where(v => v.Invoice != null).Select(v => v.Invoice.Id).ToArray();
                    if (invoiceIds.Any())
                    {
                        excludedTransactionIds = electronicServiceTransactions.Where(est => invoiceIds.Contains(est.InvoiceReceivable.InvoiceId)).Select(est => est.Id).ToList();
                        var electronicServiceTransactionsExceptInvalidInvoices = electronicServiceTransactions.Where(est => !invoiceIds.Contains(est.InvoiceReceivable.InvoiceId)).Select(est => est.Id).ToList();
                        if (electronicServiceTransactionsExceptInvalidInvoices.Any())
                            return ProduceMessage(electronicServiceTransactionsExceptInvalidInvoices, claimFormTypeId);
                    }
                }
                return interchanges.Any() ? new X12Message(interchanges.ToArray()) : null;
            }
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        protected virtual BillingServiceTransaction[] LoadAllInvoicesForBillingServiceTransactions(IEnumerable<Guid> billingServiceTransactionIds)
        {
            int?[] invoiceIds = _practiceRepository.BillingServiceTransactions.Where(i => billingServiceTransactionIds.Contains(i.Id)).
                    Select(bst => bst.BillingService.InvoiceId).Distinct().ToArray();

            Invoice[] invoices = _practiceRepository.Invoices.Where(i => invoiceIds.Contains(i.Id)).ToArray();

            invoices.LoadForClaimFileGeneration(_practiceRepository);

            BillingServiceTransaction[] billingServiceTransactions = invoices.SelectMany(i => i.InvoiceReceivables).
                                                                                             SelectMany(ir => ir.BillingServiceTransactions).
                                                                                             Where(bst => billingServiceTransactionIds.Contains(bst.Id)
                                                                                             && (!_removeNonQueuedBillingServiceTransactions || bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued)).Distinct().ToArray();


            return billingServiceTransactions;
        }
    }
}
