﻿using OopFactory.X12.Parsing.Model;
using System.Collections.Generic;

namespace IO.Practiceware.Integration.X12
{
    /// <summary>
    ///   Helpers for parsing x12 data.
    /// </summary>
    public class X12Parser
    {
        public static List<Interchange> Parse(string x12)
        {
            var parser = new OopFactory.X12.Parsing.X12Parser();
            List<Interchange> interchanges = parser.ParseMultiple(x12);
            return interchanges;
        }
    }
}