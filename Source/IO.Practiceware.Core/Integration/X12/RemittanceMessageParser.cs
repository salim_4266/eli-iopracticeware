﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using OopFactory.X12.Parsing.Model;
using Soaf;

namespace IO.Practiceware.Integration.X12
{
    public class RemittanceMessageParser
    {
        private static readonly CultureInfo USCulture = CultureInfo.GetCultureInfo("en-US");
        private static readonly string[] ServicePaymentIdentifierSeparator = { ":", "^" };

        /// <summary>
        /// Parses the specified X12 message as remittance message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public RemittanceMessage Parse(string message)
        {
            var result = new RemittanceMessage();
            result.SourceX12Message = message;

            // Load X12 message
            List<Interchange> interchanges;
            try
            {
                interchanges = X12Parser.Parse(message);
            }
            catch (Exception ex)
            {
                var wrapperException = new ArgumentException("Could not parse remittance file.", ex);
                result.CriticalErrors.AddRange(wrapperException.GetExceptions().Select(e => e.Message));
                return result;
            }
            
            // Parse advices from transactions
            foreach (var interchange in GetAdviceTransactions(interchanges))
            {
                var advice = ParseRemittanceAdvice(interchange.Transaction);
                result.Advices.Add(advice);
            }

            return result;
        }

        private static RemittanceMessage.RemittanceAdvice ParseRemittanceAdvice(Transaction transaction)
        {
            var result = new RemittanceMessage.RemittanceAdvice();

            var bprSegment = transaction.Segments.FirstOrDefault(s => s.SegmentId == "BPR");
            if (bprSegment == null)
            {
                result.Errors.Add(@"BPR segment not found.");
            }

            var trnSegment = transaction.Segments.FirstOrDefault(s => s.SegmentId == "TRN");
            if (trnSegment == null)
            {
                result.Errors.Add(@"TRN segment not found.");
            }

            var n1Segment = transaction.Segments.FirstOrDefault(s => s.SegmentId == "N1");
            if (n1Segment == null)
            {
                result.Errors.Add(@"N1 segment not found.");
            }

            var remittanceDate = new DateTime();
            if (bprSegment != null && !DateTime.TryParseExact(bprSegment.GetElement(16), "yyyyMMdd", null, DateTimeStyles.None, out remittanceDate))
            {
                result.Errors.Add(@"Unable to obtain BPR16 (Remittance Advice Date).");
            }

            Decimal totalAmount = 0;
            if (bprSegment != null && !Decimal.TryParse(bprSegment.GetElement(2), NumberStyles.Currency, USCulture.NumberFormat, out totalAmount))
            {
                result.Errors.Add(@"Unable to parse BPR02 (The total amount of the remittance).");
            }

            var remittanceCode = string.Empty;
            if (trnSegment != null) remittanceCode = trnSegment.GetElement(2);

            if (remittanceCode == string.Empty)
            {
                result.Errors.Add(@"Unable to obtain TRN02 (Remittance Code (ex: Check #).");
            }

            var payerIdentifiers = GetRefPayerIdentifier(transaction).ToList();
            if (n1Segment != null && !n1Segment.GetElement(4).IsNullOrEmpty()) 
                payerIdentifiers.Add(n1Segment.GetElement(4));
            if (trnSegment != null && !trnSegment.GetElement(3).IsNullOrEmpty()) 
                payerIdentifiers.Add(trnSegment.GetElement(3));
            if (bprSegment != null && !bprSegment.GetElement(10).IsNullOrEmpty()) 
                payerIdentifiers.Add(bprSegment.GetElement(10));

            result.TotalAmount = totalAmount;
            result.Code = remittanceCode;
            result.PostingDate = remittanceDate;
            result.PayerIdentifiers = payerIdentifiers;

            result.ClaimPayments.AddRange(transaction.Loops.Where(l => l.SegmentId == "LX").SelectMany(loop2000 => loop2000.Loops).Select(ParseClaimPayment));

            // Add information about provider level adjustments
            var plbSegment = transaction.Segments.FirstOrDefault(s => s.SegmentId == "PLB");
            if (plbSegment != null)
            {
                var plbDate = plbSegment.GetElement(2);
                var plbCount = (plbSegment.ElementCount - 2)/2;
                for (int i = 0; i < plbCount; i++)
                {
                    var identifierIndex = 3 + 2*i;
                    var identifier = plbSegment.GetElement(identifierIndex);
                    var amountIndex = 4 + 2*i;
                    var amount = plbSegment.GetElement(amountIndex);

                    result.Informations.Add(string.Format("PLB02 {0} PLB{1:d2} {2} PLB{3:d2} {4}", 
                        plbDate, identifierIndex, identifier, amountIndex, amount));
                }
            }

            return result;
        }

        private static RemittanceMessage.RemittanceAdviceClaimPayment ParseClaimPayment(Loop loop2100)
        {
            var result = new RemittanceMessage.RemittanceAdviceClaimPayment();

            int patientId;

            var possiblePatientId = loop2100.GetElement(1);

            if (possiblePatientId.Count(x => x == '-') == 1)
            {
                possiblePatientId = possiblePatientId.Split('-').Last();
            }

            if (Int32.TryParse(possiblePatientId, out patientId)) result.PatientId = patientId;
            else
            {
                result.Errors.Add(@"Invalid PatientId in loop 2100, segment CLP, element 1. Segment: {0}".FormatWith(loop2100.SegmentString));
            }

            var claimStatusCode = loop2100.GetElement(2);
            if (!string.IsNullOrEmpty(claimStatusCode))
            {
                result.ClaimStatusCode = claimStatusCode;
            }

            switch (result.ResolvedClaimStatusCode)
            {
                case RemittanceMessage.KnownClaimStatusCode.ProcessedPrimaryAutoCrossed:
                case RemittanceMessage.KnownClaimStatusCode.ProcessedSecondaryAutoCrossed:
                case RemittanceMessage.KnownClaimStatusCode.ProcessedTertiaryAutoCrossed:
                case RemittanceMessage.KnownClaimStatusCode.NotProcessedForwarded:
                    {
                        var nm1Tt = loop2100.Segments.Where(s => s.SegmentId == "NM1" && s.GetElement(1) == "TT").Select(s => s).FirstOrDefault();
                        if (nm1Tt != null)
                        {
                            var identificationCodeQualifier = nm1Tt.GetElement(8);
                            if (!string.IsNullOrEmpty(identificationCodeQualifier))
                            {
                                result.IdentificationCodeQualifier = identificationCodeQualifier;
                            }

                            var identificationCode = nm1Tt.GetElement(9);
                            if (!string.IsNullOrEmpty(identificationCodeQualifier))
                            {
                                result.IdentificationCode = identificationCode;
                            }
                        }
                    }
                    break;
            }

            result.ServicePayments.AddRange(loop2100.Loops.Select(ParseServicePayment));

            if (!result.ServicePayments.Any())
            {
                result.Errors.Add(@"Patient {0}'s remittance advice contains no service payments. ".FormatWith(patientId));
            }

            return result;
        }

        private static RemittanceMessage.RemittanceAdviceServicePayment ParseServicePayment(Loop loop2110)
        {
            var result = new RemittanceMessage.RemittanceAdviceServicePayment();
            List<string> errors;
            result.ServiceDate = GetServiceDate(loop2110, out errors);
            result.Errors.AddRange(errors);

            var ref6R = loop2110.Segments.Where(s => s.SegmentId == "REF" && s.GetElement(1) == "6R")
                .Select(s => s.GetElement(2))
                .FirstOrDefault();

            if (!string.IsNullOrEmpty(ref6R))
            {
                result.BillingServiceId = ref6R.ToInt();
            }

            // SVC06
            var serviceCodeAndServiceCodeModifiersFromElement6 = loop2110.GetElement(6);
            if (!string.IsNullOrEmpty(serviceCodeAndServiceCodeModifiersFromElement6))
            {
                var element6Parts = serviceCodeAndServiceCodeModifiersFromElement6.Split(ServicePaymentIdentifierSeparator, StringSplitOptions.None);
                if (element6Parts.FirstOrDefault() == "HC" && element6Parts.Length > 1)
                {
                    result.ProcedureIdentifier = element6Parts[1];
                    if (element6Parts.Length > 2)
                    {
                        result.ProcedureModifiers.AddRange(element6Parts.Skip(2));
                    }
                }
            }

            // SVC01
            var serviceCodeAndServiceCodeModifiersFromElement1 = loop2110.GetElement(1);
            if (!string.IsNullOrEmpty(serviceCodeAndServiceCodeModifiersFromElement1))
            {
                var element1Parts = serviceCodeAndServiceCodeModifiersFromElement1.Split(ServicePaymentIdentifierSeparator, StringSplitOptions.None);
                if (element1Parts.FirstOrDefault() == "HC" && element1Parts.Length > 1)
                {
                    result.SecondaryProcedureIdentifier = element1Parts[1];
                    if (element1Parts.Length > 2)
                    {
                        result.SecondaryProcedureModifiers.AddRange(element1Parts.Skip(2));
                    }
                }
            }

            if (string.IsNullOrEmpty(result.ProcedureIdentifier)
                && string.IsNullOrEmpty(result.SecondaryProcedureIdentifier))
            {
                result.Errors.Add(@"Unable to determine service code (CPT) in service payment information. (SVC06 & SVC01). Segment: {0}".FormatWith(loop2110.SegmentString));
            }

            decimal adjustmentAmount;
            if (Decimal.TryParse(loop2110.GetElement(3), NumberStyles.Currency, USCulture.NumberFormat, out adjustmentAmount)
                && adjustmentAmount != 0)
            {
                result.ServiceAdjustments.Add(new RemittanceMessage.RemittanceAdviceServiceAdjustment
                {
                    Amount = adjustmentAmount,
                    IsPrimaryPayment = true
                });
            }
            
            result.ServiceAdjustments.AddRange(loop2110.Segments.Where(s => s.SegmentId == "CAS").SelectMany(ParseServiceAdjustment).ToArray());

            return result;
        }

        private static RemittanceMessage.RemittanceAdviceServiceAdjustment[] ParseServiceAdjustment(Segment casSegment)
        {
            var groupCode = casSegment.GetElement(1);
            var adjustments = Enumerable.Range(2, casSegment.ElementCount - 1)
                .Where(i => (i - 2) % 3 == 0)
                .Select(i => ParseServiceAdjustment(groupCode, casSegment.GetElement(i), casSegment.GetElement(i + 1), casSegment.GetElement(i + 2)))
                .ToArray();
            return adjustments;
        }

        private static RemittanceMessage.RemittanceAdviceServiceAdjustment ParseServiceAdjustment(string groupCode, string reasonCode, string amount, string quantity)
        {
            var remittanceAdviceServiceAdjustment = new RemittanceMessage.RemittanceAdviceServiceAdjustment();

            if (!new[] {"CO", "OA", "PR", "PI", "CR"}.Contains(groupCode))
            {
                remittanceAdviceServiceAdjustment.Errors.Add("{0} is not a valid group code. Loop 2110, Segment CAS, Element 1.".FormatWith(groupCode));
            }
            
            decimal serviceAdjustmentAmount;
            if (!decimal.TryParse(amount, NumberStyles.Currency, USCulture.NumberFormat, out serviceAdjustmentAmount))
            {
                remittanceAdviceServiceAdjustment.Errors.Add("Can't understand claim adjustment amount {0} (group code {1}, reason code {2}. Loop 2110, Segment CAS)".FormatWith(amount, groupCode, reasonCode));
            }

            int serviceAdjustmentQuanitity;
            if (!int.TryParse(quantity, out serviceAdjustmentQuanitity))
            {
                serviceAdjustmentQuanitity = 1;
            }

            remittanceAdviceServiceAdjustment.GroupCode = groupCode;
            remittanceAdviceServiceAdjustment.ReasonCode = reasonCode;
            remittanceAdviceServiceAdjustment.Amount = serviceAdjustmentAmount;
            remittanceAdviceServiceAdjustment.Quantity = serviceAdjustmentQuanitity;

            return remittanceAdviceServiceAdjustment;
        }

        private static DateTime GetServiceDate(Loop loop2110, out List<string> errors)
        {
            errors = new List<string>();

            var loop2100 = loop2110.Parent;

            var serviceDate = new DateTime();

            var dtm232Date = loop2100.Segments.Where(s => s.SegmentId == "DTM" && s.GetElement(1) == "232").Select(s => s.GetElement(2)).FirstOrDefault();
            var dtm233Date = loop2100.Segments.Where(s => s.SegmentId == "DTM" && s.GetElement(1) == "233").Select(s => s.GetElement(2)).FirstOrDefault();
            if (dtm232Date == dtm233Date && dtm232Date != null && DateTime.TryParseExact(dtm232Date, "yyyyMMdd", null, DateTimeStyles.None, out serviceDate))
            {
                return serviceDate;
            }
            var dtm472Date = loop2110.Segments.Where(s => s.SegmentId == "DTM" && s.GetElement(1) == "472").Select(s => s.GetElement(2)).FirstOrDefault();
            if (dtm472Date != null && DateTime.TryParseExact(dtm472Date, "yyyyMMdd", null, DateTimeStyles.None, out serviceDate))
            {
                return serviceDate;
            }
            var dtm150Date = loop2110.Segments.Where(s => s.SegmentId == "DTM" && s.GetElement(1) == "150").Select(s => s.GetElement(2)).FirstOrDefault();
            var dtm151Date = loop2110.Segments.Where(s => s.SegmentId == "DTM" && s.GetElement(1) == "151").Select(s => s.GetElement(2)).FirstOrDefault();
            if (dtm150Date == dtm151Date && dtm150Date != null && DateTime.TryParseExact(dtm150Date, "yyyyMMdd", null, DateTimeStyles.None, out serviceDate))
            {
                return serviceDate;
            }
            errors.Add(@"Unable to parse service date. Looked at loop 2100, segment DTM, element 232 & 233, then loop 2110, segment DTM, element 472, then segment DTM, element 150 & 152. CLP is {0}".FormatWith(loop2100.SegmentString));
            return serviceDate;
        }

        private static IEnumerable<string> GetRefPayerIdentifier(Transaction transaction)
        {
            var refPayerIdentifier = transaction.Loops
                .Where(l => l.SegmentId == "N1" && l.Specification.LoopId == "1000A")
                .SelectMany(l => l.Segments)
                .Where(s => s.SegmentId == "REF")
                .OrderBy(r => GetRefSegmentPriority(r.GetElement(1)))
                .Select(s => s.GetElement(2));
            return refPayerIdentifier;
        }

        private static int GetRefSegmentPriority(string ref01)
        {
            switch (ref01)
            {
                case "NF":
                    return 1;
                case "2U":
                    return 2;
                case "HI":
                    return 3;
                case "EO":
                    return 4;
                default:
                    return 5;
            }
        }

        /// <summary>
        /// Filters and returns only advice transactions from a list of interchanges.
        /// </summary>
        /// <param name="interchanges">The interchanges.</param>
        /// <returns></returns>
        private static IEnumerable<Transaction> GetAdviceTransactions(IEnumerable<Interchange> interchanges)
        {
            var transactions = interchanges
                .SelectMany(interchange => interchange.FunctionGroups)
                .Where(functionGroup => !functionGroup.VersionIdentifierCode.StartsWith(@"004010X") 
                    || !functionGroup.VersionIdentifierCode.StartsWith(@"005010X"))
                .SelectMany(group => group.Transactions
                    .Where(transaction => transaction != null && transaction.IdentifierCode == "835"))
                .ToArray();

            return transactions;
        }
    }
}
