﻿
using System.Collections.ObjectModel;
using IO.Practiceware.Model;
using OopFactory.X12.Parsing.Model;
using OopFactory.X12.Parsing.Model.Typed;
using Soaf;
using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Gender = OopFactory.X12.Parsing.Model.Typed.Gender;

namespace IO.Practiceware.Integration.X12
{
    public class MessageProducerUtilities
    {
        public ICollection<MessageProducerValidationResult> ValidationResults;
        public MessageProducerUtilities()
        {
            ReferenceIdentifier = Guid.NewGuid().ToString("N").Truncate(9);
            ValidationResults = new Collection<MessageProducerValidationResult>();
        }

        internal string ReferenceIdentifier { get; set; }
        internal DateTime? DateTime { get; set; }

        /// <summary>
        /// Configures the 'ISA', or Interchange Control Header, for the given Interchange.
        /// </summary>
        /// <param name="message">The message to configure.</param>
        /// <param name="messageTime">The time to set for the message.</param>
        /// <param name="claimFileReceiver">The claim file receiver.</param>
        public void ConfigureIsa(Interchange message, DateTime messageTime, ClaimFileReceiver claimFileReceiver)
        {
            message.InterchangeSenderIdQualifier = claimFileReceiver.InterchangeSenderCodeQualifier;
            message.InterchangeSenderId = claimFileReceiver.InterchangeSenderCode;
            message.InterchangeReceiverIdQualifier = claimFileReceiver.InterchangeReceiverCodeQualifier;
            message.InterchangeReceiverId = claimFileReceiver.InterchangeReceiverCode;
            message.SetElement(11, "^");
            message.SetElement(12, "00501");
            string headerDate = messageTime.ToString("MMddmmssf").Truncate(9);
            message.SetElement(13, headerDate);
            message.SetElement(15, claimFileReceiver.InterchangeUsageIndicator);
            Segment messageTrailer = message.TrailerSegments.First();
            messageTrailer.SetElement(2, headerDate);
        }

        /// <summary>
        /// Configure the 'BHT', or Beginning of Hierarchical Transaction, for the given Transaction.
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="messageTime"></param>
        /// <param name="referenceIdentifier"></param>
        public void ConfigureBht(Transaction transaction, DateTime messageTime, string referenceIdentifier)
        {
            // "BHT" BEGINNING OF HIERARCHICAL TRANSACTION  
            Segment bht = transaction.AddSegment("BHT");
            bht.SetElement(1, "0019");
            bht.SetElement(2, "00");
            bht.SetElement(3, referenceIdentifier);
            bht.SetElement(4, messageTime.ToString("yyyyMMdd"));
            bht.SetElement(5, messageTime.ToString("hhmm"));
            bht.SetElement(6, "CH");
        }

        /// <summary>
        /// Configures the NM1 and PER segment for Loop 1000A, which stands for Submitter Name and Submitter EDI Contact Information respectively.
        /// Loop 1000A represents the submitter name.
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="primaryOrganization"></param>
        /// <param name="claimFileReceiver"></param>
        public TypedLoopNM1 Add1000ALoop(Transaction transaction, BillingOrganization primaryOrganization, ClaimFileReceiver claimFileReceiver)
        {
            // "NM1" SUBMITTER NAME - loop 1000A
            TypedLoopNM1 nm1 = transaction.AddLoop(new TypedLoopNM1("41"));
            nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.NonPersonEntity;
            nm1.NM103_NameLastOrOrganizationName = primaryOrganization.Name;
            nm1.NM108_IdCodeQualifier = "46";
            if (string.IsNullOrEmpty(claimFileReceiver.Loop1000ASubmitterCode) || claimFileReceiver.Loop1000ASubmitterCode.Length < 2 || claimFileReceiver.Loop1000ASubmitterCode.Length > 80)
            {
                ValidationResults.Add(new MessageProducerValidationResult("Invalid SubmitterCode for the ClaimFileReceiver {0}.".FormatWith(primaryOrganization.Name)));
                return nm1;
            }
            nm1.NM109_IdCode = claimFileReceiver.Loop1000ASubmitterCode;

            return nm1;
        }

        /// <summary>
        /// Configures the PER Segment, or Billing Provider Contact Information, for the given 1000A Loop.
        /// </summary>
        /// <param name="nm11000A"></param>
        /// <param name="primaryOrganization"></param>
        public void Configure1000A(TypedLoopNM1 nm11000A, BillingOrganization primaryOrganization)
        {
            var phone = (primaryOrganization.BillingOrganizationPhoneNumbers.WithPhoneNumberType(BillingOrganizationPhoneNumberTypeId.Billing) ?? primaryOrganization.BillingOrganizationPhoneNumbers.WithPhoneNumberType(BillingOrganizationPhoneNumberTypeId.Main));
            if (phone == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("Could not find Billing or Primary PhoneNumber for the BillingOrganization {0}.".FormatWith(primaryOrganization.Name)));
                return;
            }
            TypedSegmentPER per = nm11000A.AddSegment(new TypedSegmentPER());
            per.PER01_ContactFunctionCode = "IC";
            per.PER02_Name = "BILLING MANAGER";
            per.PER03_CommunicationNumberQualifier = CommunicationNumberQualifer.Telephone;
            per.PER04_CommunicationNumber = phone.ToString().ToNumeric();
        }

        /// <summary>
        /// Adds a 1000B loop for the given Transaction, that is represented by the returned NM1 segment.
        /// The 1000B loop represents the Receiver Name.
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="claimFileReceiver"></param>
        public void Add1000BLoop(Transaction transaction, ClaimFileReceiver claimFileReceiver)
        {
            // "NM1" RECEIVER NAME - loop 1000B
            TypedLoopNM1 nm1 = transaction.AddLoop(new TypedLoopNM1("40"));
            nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.NonPersonEntity;
            nm1.NM103_NameLastOrOrganizationName = claimFileReceiver.Name;
            nm1.NM108_IdCodeQualifier = "46";
            if (string.IsNullOrEmpty(claimFileReceiver.Loop1000BRecipientCode) || claimFileReceiver.Loop1000BRecipientCode.Length < 2 || claimFileReceiver.Loop1000BRecipientCode.Length > 80)
            {
                ValidationResults.Add(new MessageProducerValidationResult("Invalid RecipientCode for the ClaimFileReceiver {0}.".FormatWith(claimFileReceiver.Name)));
                return;
            }
            nm1.NM109_IdCode = claimFileReceiver.Loop1000BRecipientCode;
        }

        /// <summary>
        /// Configures the PRV segment for loop 2000A.
        /// </summary>
        /// <param name="hl2000A"></param>
        /// <param name="invoiceReceivable"> </param>
        /// <returns></returns>
        public void Configure2000A(HierarchicalLoop hl2000A, InvoiceReceivable invoiceReceivable)
        {
            if ((invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer == null) ||
                invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFilingIndicatorCode != ClaimFilingIndicatorCode.Medicaid) return;

            // "PRV" BILLING PROVIDER SPECIALTY INFORMATION - loop 2000A
            TypedSegmentPRV prv = hl2000A.AddSegment(new TypedSegmentPRV());
            prv.PRV01_ProviderCode = "BI";
            prv.PRV02_ReferenceIdQualifier = "PXC";
            var taxonomyIdentifierCode = invoiceReceivable.Invoice.BillingProvider.GetClaimFileOverrideProviderOrCurrent(invoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId).BillingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy);
            if (taxonomyIdentifierCode == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("Could not find a Taxonomy IdentifierCode for the BillingOrganization {0}.".FormatWith(invoiceReceivable.Invoice.BillingProvider.BillingOrganization.Name), invoiceReceivable.Invoice));
                return;
            }
            prv.PRV03_ProviderTaxonomyCode = taxonomyIdentifierCode;
        }

        /// <summary>
        /// Adds a 2000A loop that represents the Billing Provider to the Transaction.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public HierarchicalLoop Add2000ALoop(Transaction transaction)
        {
            // "HL" BILLING PROVIDER HIERARCHICAL LEVEL - loop 2000A
            HierarchicalLoop hl2000A = transaction.AddHLoop((transaction.HLoops.Count() + 1).ToString(), "20", true);
            hl2000A.SetElement(2, "");
            return hl2000A;
        }

        /// <summary>
        /// Adds a new 2010AA loop that represents the Billing Provider Name, from the given 2010A loop.  The new loop is represented by the returned NM1 segment.
        /// Configures NM101, NM108 and NM109.
        /// </summary>
        /// <param name="hl2000A"></param>
        /// <param name="billingOrganization"></param>
        /// <param name="invoiceReceivable"></param>
        public TypedLoopNM1 Add2010AaLoop(HierarchicalLoop hl2000A, BillingOrganization billingOrganization, InvoiceReceivable invoiceReceivable)
        {
            // "NM1" Billing Provider Name - loop 2010AA
            TypedLoopNM1 nm1 = hl2000A.AddLoop(new TypedLoopNM1("85"));
            nm1.NM108_IdCodeQualifier = "XX";

            string npi = billingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
            if (npi == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The BillingOrganization {0}, has no NPI.".FormatWith(billingOrganization.Name), invoiceReceivable.Invoice));
                return nm1;
            }
            if (npi.Length < 2 || npi.Length > 80)
            {
                ValidationResults.Add(new MessageProducerValidationResult("Invalid NPI for the BillingOrganization {0}.".FormatWith(billingOrganization.Name), invoiceReceivable.Invoice));
                return nm1;
            }
            nm1.NM109_IdCode = npi;

            return nm1;
        }

        /// <summary>
        /// Adds a new 2010AB loop, represented by the returned NM1 segment, to the given 2000A loop.
        /// The 2010AB loop represents the Pay-To-Address name.
        /// </summary>
        /// <param name="hl2000A"></param>
        /// <returns></returns>
        public TypedLoopNM1 Add2010AbLoop(HierarchicalLoop hl2000A)
        {
            // "NM1" PAY-TO ADDRESS NAME - loop 2010AB
            TypedLoopNM1 nm1 = hl2000A.AddLoop(new TypedLoopNM1("87"));
            nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.NonPersonEntity;
            return nm1;
        }

        /// <summary>
        /// Given an NM1 Segment for a 2010AA loop, this initializes and returns the N3 and N4 segments for the NM1.
        /// </summary>
        /// <param name="nm1"></param>
        /// <param name="billingOrganization"></param>
        /// <param name="invoiceReceivable"></param>
        public void Configure2010Aa(TypedLoopNM1 nm1, BillingOrganization billingOrganization, InvoiceReceivable invoiceReceivable)
        {
            BillingOrganizationAddress address = billingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PhysicalLocation);
            if (address == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The BillingOrganization {0}, has no PhysicalLocation Address.".FormatWith(billingOrganization.Name), invoiceReceivable.Invoice));
                return;
            }

            // "N3" BILLING PROVIDER ADDRESS - loop 2010AA
            TypedSegmentN3 n3 = nm1.AddSegment(new TypedSegmentN3());
            if (address.Line1.IsNullOrEmpty())
            {
                ValidationResults.Add(new MessageProducerValidationResult("The address of BillingOrganization {0} cannot be empty.".FormatWith(billingOrganization.Name), invoiceReceivable.Invoice));
                return;
            }

            n3.N301_AddressInformation = address.Line1 ?? string.Empty;
            if (!string.IsNullOrEmpty(address.Line2))
            {
                n3.N302_AddressInformation = address.Line2.Truncate(55);
            }

            // "N4" BILLING PROVIDER CITY/STATE/ZIP CODE - loop 2010AA
            TypedSegmentN4 n4 = nm1.AddSegment(new TypedSegmentN4());
            n4.N401_CityName = address.City;

            if (address.StateOrProvince.IsInUnitedStates || address.StateOrProvince.IsInCanada)
            {
                n4.N402_StateOrProvinceCode = address.StateOrProvince.Abbreviation;
            }
            else
            {
                n4.N407_CountrySubdivisionCode = address.StateOrProvince.Abbreviation;
            }

            if (address.IsValidPostalCode())
            {
                n4.N403_PostalCode = address.PostalCode;
            }
            else
            {
                ValidationResults.Add(new MessageProducerValidationResult("The BillingOrganization {0}, has an invalid zip code.".FormatWith(billingOrganization.Name), invoiceReceivable.Invoice));
                return;
            }
            if (!address.StateOrProvince.IsInUnitedStates)
            {
                n4.N404_CountryCode = address.StateOrProvince.Country.Abbreviation2Letters;
            }

            // "REF" BILLING PROVIDER TAX IDENTIFICATION - loop 2010AA
            string referenceId = billingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Ssn);

            TypedSegmentREF @ref = nm1.AddSegment(new TypedSegmentREF());
            if ((referenceId != null))
            {
                @ref.REF01_ReferenceIdQualifier = "SY";
                @ref.REF02_ReferenceId = referenceId;
            }
            else
            {
                referenceId = billingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.TaxId);
                if (referenceId == null)
                {
                    ValidationResults.Add(new MessageProducerValidationResult("The BillingOrganization {0}, has no SSN or Tax Id.".FormatWith(billingOrganization.Name), invoiceReceivable.Invoice));
                    return;
                }
                @ref.REF01_ReferenceIdQualifier = "EI";
                @ref.REF02_ReferenceId = referenceId;
            }
        }

        /// <summary>
        /// Configures the N3 and N4 Segments for the given 2010AB Loop.
        /// </summary>
        /// <param name="nm1"></param>
        /// <param name="address"></param>
        /// <param name="invoiceReceivable"></param>
        public void Configure2010Ab(TypedLoopNM1 nm1, BillingOrganizationAddress address, InvoiceReceivable invoiceReceivable)
        {
            // "N3" PAY-TO PROVIDER ADDRESS - loop 2010AB
            TypedSegmentN3 n3 = nm1.AddSegment(new TypedSegmentN3());

            if (address.Line1.IsNullOrEmpty())
            {
                ValidationResults.Add(new MessageProducerValidationResult("The address of BillingOrganization cannot be empty.", invoiceReceivable.Invoice));
                return;
            }

            n3.N301_AddressInformation = address.Line1;
            n3.N302_AddressInformation = address.Line2.Truncate(55) ?? string.Empty;

            // "N4" PAY-TO PROVIDER CITY/STATE/ZIP CODE - loop 2010AB
            TypedSegmentN4 n4 = nm1.AddSegment(new TypedSegmentN4());
            n4.N401_CityName = address.City;
            if (address.StateOrProvince.IsInUnitedStates | address.StateOrProvince.IsInCanada)
            {
                n4.N402_StateOrProvinceCode = address.StateOrProvince.Abbreviation;
            }
            else
            {
                n4.N407_CountrySubdivisionCode = address.StateOrProvince.Abbreviation;
            }
            if (address.IsValidPostalCode())
            {
                n4.N403_PostalCode = address.PostalCode;
            }
            else
            {
                ValidationResults.Add(new MessageProducerValidationResult("The BillingOrganization {0}, has an invalid zip code.".FormatWith(address.BillingOrganization.Name), invoiceReceivable.Invoice));
                return;
            }
            if (!address.StateOrProvince.IsInUnitedStates)
            {
                n4.N404_CountryCode = "US";
            }
        }

        /// <summary>
        /// Adds a 2000C loop that represents the Patient to the Transaction.
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="hl2000B"></param>
        /// <returns></returns>
        public HierarchicalLoop Add2000CLoop(Transaction transaction, HierarchicalLoop hl2000B)
        {
            // "HL" PATIENT HIERARCHICAL LEVEL - loop 2000C
            HierarchicalLoop hl2000C = transaction.AddHLoop((transaction.HLoops.Count() + 1).ToString(), "23", true);
            hl2000C.SetElement(2, hl2000B.Id);
            hl2000C.SetElement(4, "0");
            return hl2000C;
        }

        /// <summary>
        /// Adds a 2010CA loop, represented by the returned NM1 segment, to the 2000C loop.
        /// THe 2010CA loop represents the Patient Name.
        /// </summary>
        /// <param name="hl2000C"></param>
        /// <param name="patient"></param>
        /// <returns></returns>
        public TypedLoopNM1 Add2010CaLoop(HierarchicalLoop hl2000C, Patient patient)
        {
            // "NM1" PATIENT NAME - loop 2010CA
            TypedLoopNM1 nm1 = hl2000C.AddLoop(new TypedLoopNM1("QC"));
            nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.Person;
            nm1.NM103_NameLastOrOrganizationName = patient.LastName;
            nm1.NM104_NameFirst = patient.FirstName.Truncate(35);
            nm1.NM105_NameMiddle = patient.MiddleName ?? string.Empty;
            nm1.NM107_NameSuffix = patient.Suffix ?? string.Empty;

            return nm1;
        }

        /// <summary>
        /// Configures the N3, N4 and DMG Segments for the given 2010CA loop.
        /// </summary>
        /// <param name="nm1"></param>
        /// <param name="invoiceReceivable"></param>
        public void Configure2010Ca(TypedLoopNM1 nm1, InvoiceReceivable invoiceReceivable)
        {
            Patient patient = invoiceReceivable.Invoice.Encounter.Patient;
            PatientAddress address = patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home);
            if (address == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The Patient {0}, has no Home Address.".FormatWith(patient.GetFormattedName()), invoiceReceivable.Invoice));
                return;
            }
            // "N3" PATIENT ADDRESS - loop 2010CA
            TypedSegmentN3 n3 = nm1.AddSegment(new TypedSegmentN3());
            if (address.Line1.IsNullOrEmpty())
            {
                ValidationResults.Add(new MessageProducerValidationResult("The address of patient {0} cannot be empty.".FormatWith(patient.GetFormattedName()), invoiceReceivable.Invoice));
                return;
            }
            n3.N301_AddressInformation = address.Line1;
            if (!string.IsNullOrEmpty(address.Line2))
            {
                n3.N302_AddressInformation = address.Line2.Truncate(55);
            }

            // "N4" PATIENT CITY/STATE/ZIP CODE - loop 2010CA
            TypedSegmentN4 n4 = nm1.AddSegment(new TypedSegmentN4());
            n4.N401_CityName = address.City;
            if (address.StateOrProvince.IsInUnitedStates || address.StateOrProvince.IsInCanada)
            {
                n4.N402_StateOrProvinceCode = address.StateOrProvince.Abbreviation;
            }
            else
            {
                n4.N407_CountrySubdivisionCode = address.StateOrProvince.Abbreviation;
            }
            if (address.IsValidPostalCode())
            {
                n4.N403_PostalCode = address.PostalCode;
            }
            else
            {
                ValidationResults.Add(new MessageProducerValidationResult("The patient {0}, has an invalid zip code.".FormatWith(patient.GetFormattedName()), invoiceReceivable.Invoice));
                return;
            }

            if (!address.StateOrProvince.IsInUnitedStates)
            {
                n4.N404_CountryCode = address.StateOrProvince.Country.Abbreviation2Letters;
            }

            // "DMG" PATIENT DEMOGRAPHIC INFORMATION - loop 2010CA
            TypedSegmentDMG dmg = nm1.AddSegment(new TypedSegmentDMG());
            dmg.DMG01_DateTimePeriodFormatQualifier = "D8";
            if (invoiceReceivable.Invoice.Encounter.Patient.DateOfBirth != null) dmg.DMG02_DateOfBirth = invoiceReceivable.Invoice.Encounter.Patient.DateOfBirth.Value;
            if (invoiceReceivable.Invoice.Encounter.Patient.Gender.HasValue)
            {
                switch (invoiceReceivable.Invoice.Encounter.Patient.Gender.Value)
                {
                    case Model.Gender.Female:
                        dmg.DMG03_Gender = Gender.Female;
                        break;
                    case Model.Gender.Male:
                        dmg.DMG03_Gender = Gender.Male;
                        break;
                    case Model.Gender.Other:
                        dmg.DMG03_Gender = Gender.Unknown;
                        break;
                }
            }
            else
            {
                dmg.DMG03_Gender = Gender.Unknown;
            }
        }

        /// <summary>
        /// Configures the PAT segment for the given 2000C loop.
        /// </summary>
        /// <param name="hl2000C"> </param>
        /// <param name="invoiceReceivable"></param>
        public void Configure2000C(HierarchicalLoop hl2000C, InvoiceReceivable invoiceReceivable)
        {
            // "PAT" PATIENT INFORMATION - loop 2000C
            TypedSegmentPAT pat2000C = hl2000C.AddSegment(new TypedSegmentPAT());

            pat2000C.PAT01_IndividualRelationshipCode = Common.PolicyHolderRelationshipTypeMappings.GetValue(invoiceReceivable.PatientInsurance.PolicyholderRelationshipType) ?? Common.PolicyHolderRelationshipTypeMappings[PolicyHolderRelationshipType.Unknown];

            pat2000C.PAT06_DateOfDeath = invoiceReceivable.Invoice.Encounter.Patient.DateOfDeath;
            if (invoiceReceivable.Invoice.Encounter.Patient.DateOfDeath.HasValue)
            {
                pat2000C.PAT05_DateTimePeriodFormatQualifier = "D8";
            }
            else
            {
                pat2000C.PAT05_DateTimePeriodFormatQualifier = "";
                pat2000C.PAT06_DateOfDeath = null;
            }
        }

        /// <summary>
        /// Adds a 2000B loop that represents the subscriber to the Transaction.
        /// </summary>
        /// <remarks>If the subscriber is also the patient, no 2000C loop will be added.</remarks>
        /// <param name="transaction"></param>
        /// <param name="hl2000A"></param>
        /// <param name="invoiceReceivable"></param>
        /// <returns></returns>
        public HierarchicalLoop Add2000BLoop(Transaction transaction, HierarchicalLoop hl2000A, InvoiceReceivable invoiceReceivable)
        {
            // "HL" SUBSCRIBER HIERARCHICAL LEVEL - loop 2000B
            HierarchicalLoop hl2000B = transaction.AddHLoop((transaction.HLoops.Count() + 1).ToString(), "22", true);
            hl2000B.SetElement(2, hl2000A.Id);
            hl2000B.SetElement(4, invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient.Id == invoiceReceivable.PatientInsurance.InsuredPatientId ? "0" : "1");
            return hl2000B;
        }

        /// <summary>
        /// Adds a 2000BA loop that represents the Subscriber Name to the given 2000B loop.
        /// </summary>
        /// <param name="hl2000B"></param>
        /// <param name="insurancePolicy"></param>
        /// <param name="invoiceReceivable"></param>
        /// <returns></returns>
        public TypedLoopNM1 Add2000BaLoop(HierarchicalLoop hl2000B, InsurancePolicy insurancePolicy, InvoiceReceivable invoiceReceivable)
        {
            // "NM1" SUBSCRIBER NAME - loop 2010BA
            TypedLoopNM1 nm1 = hl2000B.AddLoop(new TypedLoopNM1("IL"));
            nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.Person;
            nm1.NM103_NameLastOrOrganizationName = insurancePolicy.PolicyholderPatient.LastName;
            nm1.NM104_NameFirst = insurancePolicy.PolicyholderPatient.FirstName.Truncate(35);

            nm1.NM105_NameMiddle = insurancePolicy.PolicyholderPatient.MiddleName.Truncate(25) ?? string.Empty;
            nm1.NM107_NameSuffix = insurancePolicy.PolicyholderPatient.Suffix ?? string.Empty;
            nm1.NM108_IdCodeQualifier = "MI";
            Regex policyCodeExpression = new Regex(@"^[a-zA-Z0-9]*$");
            if (string.IsNullOrEmpty(insurancePolicy.PolicyCode) || insurancePolicy.PolicyCode.Length < 2 || insurancePolicy.PolicyCode.Length > 80 || !policyCodeExpression.IsMatch(insurancePolicy.PolicyCode))
            {
                ValidationResults.Add(new MessageProducerValidationResult("Invalid PolicyCode for insurer, {0}.".FormatWith(insurancePolicy.PolicyholderPatient.DisplayName), invoiceReceivable.Invoice));
                return nm1;
            }

            nm1.NM109_IdCode = insurancePolicy.PolicyCode;

            return nm1;
        }

        /// <summary>
        /// Configures the N3, N4 and DMG Segments for the given 2000BA Loop.
        /// </summary>
        /// <param name="nm1"></param>
        /// <param name="invoiceReceivable"></param>
        public void Configure2000Ba(TypedLoopNM1 nm1, InvoiceReceivable invoiceReceivable)
        {
            Patient patient = invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient;
            PatientAddress address = patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home);
            if (address == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The Patient {0}, has no Home Address.".FormatWith(patient.GetFormattedName()), invoiceReceivable.Invoice));
                return;
            }

            if (invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient.Id == invoiceReceivable.PatientInsurance.InsuredPatientId)
            {
                // "N3" SUBSCRIBER ADDRESS - loop 2010BA
                TypedSegmentN3 n32010Ba = nm1.AddSegment(new TypedSegmentN3());
                if (address.Line1.IsNullOrEmpty())
                {
                    ValidationResults.Add(new MessageProducerValidationResult("The address of patient {0} cannot be empty.".FormatWith(patient.GetFormattedName()), invoiceReceivable.Invoice));
                    return;
                }
                n32010Ba.N301_AddressInformation = address.Line1;
                n32010Ba.N302_AddressInformation = address.Line2.Truncate(55) ?? string.Empty;

                // "N4" SUBSCRIBER CITY/STATE/ZIP CODE - loop 2010BA
                TypedSegmentN4 n42010Ba = nm1.AddSegment(new TypedSegmentN4());
                n42010Ba.N401_CityName = address.City;

                if (address.StateOrProvince.IsInUnitedStates | address.StateOrProvince.IsInCanada)
                {
                    n42010Ba.N402_StateOrProvinceCode = address.StateOrProvince.Abbreviation;
                }
                else
                {
                    n42010Ba.N407_CountrySubdivisionCode = address.StateOrProvince.Abbreviation;
                }

                if (address.IsValidPostalCode())
                {
                    n42010Ba.N403_PostalCode = address.PostalCode;
                }
                else
                {
                    ValidationResults.Add(new MessageProducerValidationResult("The patient {0}, has an invalid zip code.".FormatWith(patient.GetFormattedName()), invoiceReceivable.Invoice));
                    return;
                }

                if (!address.StateOrProvince.IsInUnitedStates)
                {
                    n42010Ba.N404_CountryCode = address.StateOrProvince.Country.Abbreviation2Letters;
                }
                // "DMG" SUBSCRIBER DEMOGRAPHIC INFORMATION - loop 2010BA
                TypedSegmentDMG dmg2010Ba = nm1.AddSegment(new TypedSegmentDMG());
                dmg2010Ba.DMG01_DateTimePeriodFormatQualifier = "D8";
                dmg2010Ba.DMG02_DateOfBirth = invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient.DateOfBirth;
                if (invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient.Gender.HasValue)
                {
                    switch (invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient.Gender.Value)
                    {
                        case Model.Gender.Female:
                            dmg2010Ba.DMG03_Gender = Gender.Female;
                            break;
                        case Model.Gender.Male:
                            dmg2010Ba.DMG03_Gender = Gender.Male;
                            break;
                        case Model.Gender.Decline:
                        case Model.Gender.Other:
                            dmg2010Ba.DMG03_Gender = Gender.Unknown;
                            break;
                    }
                }
                else
                {
                    dmg2010Ba.DMG03_Gender = Gender.Unknown;
                }
            }
        }

        /// <summary>
        /// Adds a 2010BB loop, represented by the returned NM1 segment, to the 2000B loop.
        /// The 2010BB loop represents the Payer Name.
        /// </summary>
        /// <param name="hl2000B"></param>
        /// <param name="insurer"></param>
        /// <param name="invoiceReceivable"></param>
        /// <returns></returns>
        public TypedLoopNM1 Add2010BbLoop(HierarchicalLoop hl2000B, Insurer insurer, InvoiceReceivable invoiceReceivable)
        {
            // "NM1" PAYER NAME - loop 2010BB
            TypedLoopNM1 nm1 = hl2000B.AddLoop(new TypedLoopNM1("PR"));
            nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.NonPersonEntity;
            nm1.NM103_NameLastOrOrganizationName = insurer.Name;
            nm1.NM108_IdCodeQualifier = "PI";

            if (string.IsNullOrEmpty(insurer.PayerCode) || insurer.PayerCode.Length < 2 || insurer.PayerCode.Length > 80)
            {
                ValidationResults.Add(new MessageProducerValidationResult("Invalid PayerCode for the insurer, {0}".FormatWith(insurer.Name), invoiceReceivable.Invoice));
                return nm1;
            }
            nm1.NM109_IdCode = insurer.PayerCode;

            return nm1;
        }

        /// <summary>
        /// Configures the N3 and N4 Segments for the given 2000BB Loop.
        /// </summary>
        /// <param name="nm1"></param>
        /// <param name="address"></param>
        public void Configure2010Bb(TypedLoopNM1 nm1, InsurerAddress address)
        {
            // "N3" PAYER ADDRESS - loop 2010BB
            TypedSegmentN3 n32010Bb = nm1.AddSegment(new TypedSegmentN3());
            n32010Bb.N301_AddressInformation = address.Line1;
            n32010Bb.N302_AddressInformation = address.Line2.Truncate(55) ?? string.Empty;

            // "N4" PAYER CITY/STATE/ZIP CODE - loop 2010BB
            TypedSegmentN4 n42010Bb = nm1.AddSegment(new TypedSegmentN4());
            n42010Bb.N401_CityName = address.City;
            if (address.StateOrProvince.IsInUnitedStates | address.StateOrProvince.IsInCanada)
            {
                n42010Bb.N402_StateOrProvinceCode = address.StateOrProvince.Abbreviation;
            }
            else
            {
                n42010Bb.N407_CountrySubdivisionCode = address.StateOrProvince.Abbreviation;
            }

            n42010Bb.N403_PostalCode = address.PostalCode;

            if (!address.StateOrProvince.IsInUnitedStates)
            {
                n42010Bb.N404_CountryCode = address.StateOrProvince.Country.Abbreviation2Letters;
            }
        }

        /// <summary>
        /// Configures the SBR segment of the given 2000B loop.
        /// </summary>
        /// <param name="hl2000B"></param>
        /// <param name="invoiceReceivable"></param>
        public void Configure2000B(HierarchicalLoop hl2000B, InvoiceReceivable invoiceReceivable)
        {
            // Get the index of this invoice receivable's patient insurance among all active policies for this invoice
            int indexOfCurrentPatientInsurance = invoiceReceivable.PatientInsurance.GetIndex(invoiceReceivable.Invoice.InvoiceReceivables.Where(i => i.PatientInsuranceId.HasValue.Equals(true)).Select(i => i.PatientInsurance), invoiceReceivable.Invoice.Encounter.StartDateTime);

            // "SBR" SUBSCRIBER INFORMATION - loop 2000B
            TypedSegmentSBR sbr2000B = hl2000B.AddSegment(new TypedSegmentSBR());
            sbr2000B.SBR01_PayerResponsibilitySequenceNumberCode = Common.PayerResponsibilitySequenceMap.ContainsKey(indexOfCurrentPatientInsurance) ? Common.PayerResponsibilitySequenceMap[indexOfCurrentPatientInsurance] : Common.PayerResponsibilitySequenceMap[1];

            if (invoiceReceivable.PatientInsurance.PolicyholderRelationshipType == PolicyHolderRelationshipType.Self)
            {
                sbr2000B.SBR02_IndividualRelationshipCode = Common.PolicyHolderRelationshipTypeMappings[invoiceReceivable.PatientInsurance.PolicyholderRelationshipType];
            }
            sbr2000B.SBR03_PolicyOrGroupNumber = invoiceReceivable.PatientInsurance.InsurancePolicy.GroupCode ?? string.Empty;
            if (sbr2000B.SBR03_PolicyOrGroupNumber.IsNullOrEmpty())
            {
                sbr2000B.SBR04_GroupName = invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.PlanName ?? string.Empty;
            }

            PatientInsurance primaryPatientInsurance = invoiceReceivable.GetPrimaryPatientInsurance();
            if (primaryPatientInsurance == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The Patient {0}, has an invalid InvoiceReceivable containing no Primary Patient Insurance.".FormatWith(invoiceReceivable.Invoice.Encounter.Patient.GetFormattedName()), invoiceReceivable.Invoice));
                return;
            }
            if (invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.MedicarePartB && primaryPatientInsurance.Id != invoiceReceivable.PatientInsuranceId)
            {
                if (invoiceReceivable.PatientInsurance.InsurancePolicy.MedicareSecondaryReasonCode.HasValue)
                {
                    sbr2000B.SBR05_InsuranceTypeCode = Common.MedicareSecondaryReasonCodeMappings[invoiceReceivable.PatientInsurance.InsurancePolicy.MedicareSecondaryReasonCode.Value];
                }
                else if (invoiceReceivable.Invoice.Encounter.Patient.DateOfBirth != null && (System.DateTime.Now.ToClientTime().Year - invoiceReceivable.Invoice.Encounter.Patient.DateOfBirth.Value.Year) >= 65)
                {
                    sbr2000B.SBR05_InsuranceTypeCode = Common.MedicareSecondaryReasonCodeMappings[MedicareSecondaryReasonCode.WorkingAged];
                }
            }
            sbr2000B.SBR09_ClaimFilingIndicatorCode = Common.ClaimFilingIndicatorCodeMappings[invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFilingIndicatorCode];
        }

        /// <summary>
        /// Configures the DTP segments for the given 2300 loop.
        /// </summary>
        /// <param name="clm2300"></param>
        /// <param name="invoiceReceivable"></param>
        public void Configure2300Dtp(TypedLoopCLM clm2300, InvoiceReceivable invoiceReceivable)
        {
            #region Helper DTP actions - AccidentDate, HearingAndVisionPrescriptionDate, DisabilityDate, AdmissionDate
            Action<DateTime> configureDtpAccidentDate = dateOfAccident =>
            {
                // "DTP" DATE - ACCIDENT - loop 2300
                Segment dtp52300 = clm2300.AddSegment("DTP");
                dtp52300.SetElement(1, "439");
                dtp52300.SetElement(2, "D8");
                dtp52300.SetElement(3, dateOfAccident.ToString("yyyyMMdd"));
            };

            Action<DateTime> configureDtpHearingAndVisionPrescriptionDate = dateOfHearingAndVisionPrescription =>
            {
                // "DTP" DATE - HEARING AND VISION PRESCRIPTION  DATE - loop 2300
                Segment dtp82300 = clm2300.AddSegment("DTP");
                dtp82300.SetElement(1, "471");
                dtp82300.SetElement(2, "D8");
                dtp82300.SetElement(3, dateOfHearingAndVisionPrescription.ToString("yyyyMMdd"));
            };

            Action<DateTime> configureDtpDisabilityDate = dateOfDisability =>
            {
                // "DTP" DATE - DISABILITY DATES - loop 2300
                Segment dtp92300 = clm2300.AddSegment("DTP");
                dtp92300.SetElement(1, "360");
                // 360 361
                dtp92300.SetElement(2, "D8");
                dtp92300.SetElement(3, dateOfDisability.ToString("yyyyMMdd"));
            };

            Action<DateTime> configureDtpAdmissionDate = dateOfAdmission =>
            {
                // "DTP" DATE - ADMISSION - loop 2300
                Segment dtp122300 = clm2300.AddSegment("DTP");
                dtp122300.SetElement(1, "435");
                dtp122300.SetElement(2, "D8");
                dtp122300.SetElement(3, dateOfAdmission.ToString("yyyyMMdd"));
            };

            Action<DateTime> configureDtpDischargeDate = dateOfDischarge =>
            {
                // "DTP" DATE - DISCHARGE - loop 2300
                Segment dtp132300 = clm2300.AddSegment("DTP");
                dtp132300.SetElement(1, "096");
                dtp132300.SetElement(2, "D8");
                dtp132300.SetElement(3, dateOfDischarge.ToString("yyyyMMdd"));
            };
            #endregion

            InvoiceSupplemental invoiceSupplemental = invoiceReceivable.Invoice.InvoiceSupplemental;
            if (invoiceSupplemental != null)
            {
                if (invoiceSupplemental.AccidentDateTime.HasValue)
                {
                    configureDtpAccidentDate(invoiceSupplemental.AccidentDateTime.Value);
                }
                if (invoiceSupplemental.VisionPrescriptionDateTime.HasValue)
                {
                    configureDtpHearingAndVisionPrescriptionDate(invoiceSupplemental.VisionPrescriptionDateTime.Value);
                }
                if (invoiceSupplemental.DisabilityDateTime.HasValue)
                {
                    configureDtpDisabilityDate(invoiceSupplemental.DisabilityDateTime.Value);
                }
                if (invoiceSupplemental.AdmissionDateTime.HasValue)
                {
                    configureDtpAdmissionDate(invoiceSupplemental.AdmissionDateTime.Value);
                }
                if (invoiceSupplemental.DischargeDateTime.HasValue)
                {
                    configureDtpDischargeDate(invoiceSupplemental.DischargeDateTime.Value);
                }
            }
        }


        /// <summary>
        /// Configures the REF segments for the given 2300 loop.
        /// </summary>
        /// <param name="clm2300"></param>
        /// <param name="invoiceReceivable"></param>
        public void Configure2300Ref(TypedLoopCLM clm2300, InvoiceReceivable invoiceReceivable)
        {
            #region Helper REF actions - REFERRAL NUMBER, PRIOR AUTHORIZATION, PAYER CLAIM CONTROL NUMBER
            Action configureReferralNumber = () =>
            {
                // "REF" REFERRAL NUMBER - loop 2300
                TypedSegmentREF ref42300 = clm2300.AddSegment(new TypedSegmentREF());
                ref42300.REF01_ReferenceIdQualifier = "9F";
                ref42300.REF02_ReferenceId = invoiceReceivable.PatientInsuranceReferral.ReferralCode;
            };

            Action configurePriorAuthorization = () =>
            {
                // "REF" PRIOR AUTHORIZATION - loop 2300
                TypedSegmentREF ref52300 = clm2300.AddSegment(new TypedSegmentREF());
                ref52300.REF01_ReferenceIdQualifier = "G1";
                ref52300.REF02_ReferenceId = invoiceReceivable.PatientInsuranceAuthorization.AuthorizationCode;
            };

            Action configurePayerClaimControlNumber = () =>
            {
                if (!string.IsNullOrEmpty(invoiceReceivable.PayerClaimControlNumber))
                {
                    // "REF" PAYER CLAIM CONTROL NUMBER - loop 2300
                    TypedSegmentREF ref62300 = clm2300.AddSegment(new TypedSegmentREF());
                    ref62300.REF01_ReferenceIdQualifier = "F8";
                    ref62300.REF02_ReferenceId = invoiceReceivable.PayerClaimControlNumber;
                }
            };
            #endregion

            if ((invoiceReceivable.PatientInsuranceReferral != null && !invoiceReceivable.PatientInsuranceReferral.ExternalProvider.ExcludeOnClaim))
            {
                configureReferralNumber();
            }

            if ((invoiceReceivable.PatientInsuranceAuthorization != null))
            {
                configurePriorAuthorization();
            }

            if (!string.IsNullOrEmpty(invoiceReceivable.PayerClaimControlNumber))
            {
                configurePayerClaimControlNumber();
            }
        }

        /// <summary>
        /// Configures the NTE segments for the given 2300 loop.
        /// </summary>
        /// <param name="clm2300"></param>
        /// <param name="invoiceReceivable"></param>
        public void Configure2300Nte(TypedLoopCLM clm2300, InvoiceReceivable invoiceReceivable)
        {
            // "NTE" LINE NOTE - loop 2300
            if (!string.IsNullOrWhiteSpace(invoiceReceivable.Invoice.ClaimComment))
            {
                Segment nte2300 = clm2300.AddSegment("NTE");
                nte2300.SetElement(1, "ADD");
                nte2300.SetElement(2, StripInvalidCharacters(invoiceReceivable.Invoice.ClaimComment));
            }
        }

        /// <summary>
        /// Adds a 2300 Loop and all its necessary CLM segments except for CLM05-1, CLM05-2, CLM05-3, CLM06.
        /// The 2300 Loop represents the Claim Information.
        /// </summary>
        /// <param name="parentLoop">The parent Loop of the new 2300 Loop.  This may be Loop 2000B, or 2000C if it exists.</param>
        /// <param name="invoiceReceivable"></param>
        /// <param name="billingServices"></param>
        /// <returns></returns>
        public TypedLoopCLM Add2300Loop(HierarchicalLoopContainer parentLoop, InvoiceReceivable invoiceReceivable, BillingService[] billingServices)
        {

            // "CLM" CLAIM INFORMATION - loop 2300
            TypedLoopCLM clm2300 = parentLoop.AddLoop(new TypedLoopCLM());
            clm2300.CLM01_PatientControlNumber = invoiceReceivable.Invoice.Encounter.Patient.Id.ToString();
            decimal claimChargeAmount = (Math.Round(billingServices.SumCharges(), billingServices.SumCharges().IsWholeNumber() ? 0 : 2));
            if (claimChargeAmount == 0)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The claim amount for patient {0} cannot be zero.".FormatWith(invoiceReceivable.Invoice.Encounter.Patient.DisplayName), invoiceReceivable.Invoice));
                return clm2300;
            }
            clm2300.CLM02_TotalClaimChargeAmount = claimChargeAmount;
            clm2300.CLM05._3_ClaimFrequencyTypeCode = string.IsNullOrEmpty(invoiceReceivable.PayerClaimControlNumber) ? "1" : invoiceReceivable.ClaimFrequencyTypeCodeId.ToStringIfNotNull("1");
            clm2300.CLM07_ProviderAcceptAssignmentCode = invoiceReceivable.Invoice.IsAssignmentRefused ? "C" : "A";
            clm2300.CLM08_BenefitsAssignmentCerficationIndicator = invoiceReceivable.Invoice.HasPatientAssignedBenefits ? "Y" : "N";
            clm2300.CLM09_ReleaseOfInformationCode = invoiceReceivable.Invoice.IsReleaseOfInformationNotSigned ? "I" : "Y";

            if (invoiceReceivable.Invoice.InvoiceSupplemental == null) return clm2300;

            clm2300.CLM11._1_RelatedCausesCode = invoiceReceivable.Invoice.InvoiceSupplemental.HasAutoAccidentRelatedCause
                ? Common.RelatedCauseMappings[RelatedCause.AutoAccident]
                    : invoiceReceivable.Invoice.InvoiceSupplemental.HasEmploymentRelatedCause
                    ? Common.RelatedCauseMappings[RelatedCause.Employment]
                        : invoiceReceivable.Invoice.InvoiceSupplemental.HasOtherAccidentRelatedCause
                        ? Common.RelatedCauseMappings[RelatedCause.OtherAccident]
                            : null;
            clm2300.CLM11._2_RelatedCausesCode = invoiceReceivable.Invoice.InvoiceSupplemental.HasEmploymentRelatedCause
                && clm2300.CLM11._1_RelatedCausesCode != Common.RelatedCauseMappings[RelatedCause.Employment]
                ? Common.RelatedCauseMappings[RelatedCause.Employment]
                    : invoiceReceivable.Invoice.InvoiceSupplemental.HasOtherAccidentRelatedCause
                    && clm2300.CLM11._1_RelatedCausesCode != Common.RelatedCauseMappings[RelatedCause.OtherAccident]
                    ? Common.RelatedCauseMappings[RelatedCause.OtherAccident]
                        : null;
            clm2300.CLM11._3_RelatedCausesCode = invoiceReceivable.Invoice.InvoiceSupplemental.HasOtherAccidentRelatedCause
                && clm2300.CLM11._1_RelatedCausesCode != Common.RelatedCauseMappings[RelatedCause.OtherAccident]
                && clm2300.CLM11._2_RelatedCausesCode != Common.RelatedCauseMappings[RelatedCause.OtherAccident]
                ? Common.RelatedCauseMappings[RelatedCause.OtherAccident]
                : null;

            if (invoiceReceivable.Invoice.InvoiceSupplemental.HasAutoAccidentRelatedCause)
            {
                if (invoiceReceivable.Invoice.InvoiceSupplemental.AccidentStateOrProvinceId == null)
                {
                    ValidationResults.Add(new MessageProducerValidationResult("Patient {0} must have a state specified for accident.".FormatWith(invoiceReceivable.Invoice.Encounter.Patient.DisplayName), invoiceReceivable.Invoice));
                    return clm2300;
                }

                clm2300.CLM11._4_StateOrProvidenceCode = invoiceReceivable.Invoice.InvoiceSupplemental.AccidentStateOrProvince.Abbreviation;
            }

            if (invoiceReceivable.Invoice.InvoiceSupplemental.ClaimDelayReason.HasValue && invoiceReceivable.Invoice.InvoiceSupplemental.ClaimDelayReason.Value > 0)
            {
                clm2300.CLM20_DelayReasonCode = Convert.ToString((int)invoiceReceivable.Invoice.InvoiceSupplemental.ClaimDelayReason.Value);
            }

            return clm2300;
        }

        /// <summary>
        /// Tries to add a 2410 loop that represents the Drug Identification to the given 2400 loop.
        /// </summary>
        /// <remarks>The encounter service, encounter service drug, and encounter service drug NDC values must exist to successfully add a 2410 loop.</remarks>
        /// <param name="l2400"></param>
        /// <param name="billingService"></param>
        /// <param name="l2410"></param>
        /// <returns></returns>
        public bool TryAdd2410Loop(Loop l2400, BillingService billingService, out Loop l2410)
        {
            if (billingService != null && !string.IsNullOrEmpty(billingService.Ndc))
            {
                // "LIN" DRUG IDENTIFICATION - loop 2410
                l2410 = l2400.AddLoop("LIN");
                l2410.SetElement(2, "N4");
                l2410.SetElement(3, billingService.Ndc);
                return true;
            }
            l2410 = null;
            return false;
        }

        /// <summary>
        /// Tries to add a 2430 loop that represents the "Line Adjudication Information" to the given 2400 loop.
        /// </summary>
        /// <remarks>The patient insurance for the invoice receivable must NOT be the patient's primary insurance,
        /// or the primary insurance invoice receivable for billing service must exist, to successfully add a 2430 loop.</remarks>
        /// <param name="l2400"></param>
        /// <param name="billingServiceTransaction"></param>
        /// <param name="svd2430"></param>
        /// <returns></returns>
        public bool TryAdd2430Loop(Loop l2400, BillingServiceTransaction billingServiceTransaction, out Loop svd2430)
        {
            BillingService billingService = billingServiceTransaction.BillingService;

            // From the patient insurances for the invoice, get the lowest ordinal that has the same insurance type and is active as the current invoice receivable's patient insurance
            PatientInsurance primaryInsurance = billingServiceTransaction.InvoiceReceivable.GetPrimaryPatientInsurance();
            if (primaryInsurance == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The Patient {0} has an invalid InvoiceReceivable containing no Primary Patient Insurance.".FormatWith(billingServiceTransaction.InvoiceReceivable.Invoice.Encounter.Patient.GetFormattedName()), billingServiceTransaction.InvoiceReceivable.Invoice));
                svd2430 = null;
                return false;
            }

            // Find invoice receivable that has the primary insurance and contains the current billing service
            InvoiceReceivable primaryInsuranceInvoiceReceivableForBillingService = GetPrimaryInsuranceInvoiceReceivableForBillingService(billingServiceTransaction);

            bool isPrimary = Convert.ToBoolean(primaryInsurance.Id == billingServiceTransaction.InvoiceReceivable.PatientInsuranceId);
            if (isPrimary || primaryInsuranceInvoiceReceivableForBillingService == null)
            {
                svd2430 = null;
                return false;
            }

            // "SVD" LINE ADJUDICATION INFORMATION - loop 2430
            //Find adjustments for each individual BillingService
            string svd243002 = ((double)primaryInsuranceInvoiceReceivableForBillingService.Adjustments.Where(j => j.AdjustmentTypeId == (int)AdjustmentTypeId.Payment && Convert.ToBoolean(j.BillingServiceId == billingServiceTransaction.BillingServiceId)).CalculateTotalAmount()).FormatAsCurrency();
            svd2430 = l2400.AddLoop("SVD");
            svd2430.SetElement(1, primaryInsurance.InsurancePolicy.Insurer.PayerCode ?? string.Empty);
            svd2430.SetElement(2, svd243002);

            Segment sv12400 = l2400.Segments.FirstOrDefault(s => s.SegmentId == "SV1");
            if (sv12400 != null)
            {
                svd2430.SetElement(3,
                    !string.IsNullOrEmpty(billingService.ProcedureCode835Description)
                        ? new[] { sv12400.GetElement(1) }.Concat(new[] { billingService.ProcedureCode835Description }).Join(":")
                        : sv12400.GetElement(1));
            }
            else
            {
                //If sv1 is null go look for the Sv2 which mean it must be a Inst. Claim
                Segment sv22400 = l2400.Segments.First(s => s.SegmentId == "SV2");
                svd2430.SetElement(3, !string.IsNullOrEmpty(billingService.ProcedureCode835Description) ? new[] { sv22400.GetElement(2) }.Concat(new[] { billingService.ProcedureCode835Description }).Join(":") : sv22400.GetElement(2));
                RevenueCode revenueCode = billingService.IfNotNull(x => x.FacilityEncounterService).IfNotNull(x => x.RevenueCode);
                if (revenueCode == null)
                {
                    ValidationResults.Add(new MessageProducerValidationResult("The Encounter service {0}, does not have a revenue code.".FormatWith(billingService.EncounterService.Description), billingServiceTransaction.InvoiceReceivable.Invoice));
                    return false;
                }
                svd2430.SetElement(4, revenueCode.Code);
            }

            // ReSharper disable PossibleInvalidOperationException
            // Suppressing possible null ref exception for billingService.Unit to keep the original logic intact.
            svd2430.SetElement(5, Convert.ToDouble(billingService.Unit.Value).FormatAsCurrency());
            // ReSharper restore PossibleInvalidOperationException

            return true;
        }

        /// <summary>
        /// Configures the CAS and DTP (573) segments.
        /// </summary>
        /// <param name="svd2430"></param>
        /// <param name="l2400"> </param>
        /// <param name="billingServiceTransaction"></param>
        public void Configure2430(Loop svd2430, Loop l2400, BillingServiceTransaction billingServiceTransaction)
        {
            InvoiceReceivable primaryInsuranceInvoiceReceivableForBillingService = GetPrimaryInsuranceInvoiceReceivableForBillingService(billingServiceTransaction);

            Action<int, ClaimAdjustmentGroupCode, Adjustment[], FinancialInformation[]>
                configureCasSegment = (casIndex, groupCode, adjustmentsByGroupCode, financialInformationsByGroupCode) =>
                {
                    var cas2430PatientResponsibility = svd2430.AddSegment("CAS");
                    cas2430PatientResponsibility.SetElement(casIndex++, Common.ClaimAdjustmentGroupCodeMappings[groupCode]);

                    var reasonCodes = new List<ClaimAdjustmentReasonCode>();
                    reasonCodes.AddRange(adjustmentsByGroupCode.Select(a => a.ClaimAdjustmentReasonCode).Distinct());
                    reasonCodes.AddRange(financialInformationsByGroupCode.Select(a => a.ClaimAdjustmentReasonCode).Distinct());

                    foreach (var reasonCode in reasonCodes.Distinct().Where(rc => !rc.ExcludeFromSecondaryClaims).OrderBy(rc => rc.Code))
                    {
                        var localReasonCode = reasonCode;
                        var adjustmentsByGroupAndReasonCode = adjustmentsByGroupCode.Where(a => a.ClaimAdjustmentReasonCode == localReasonCode).ToArray();
                        var financialInformationsByGroupAndReasonCode = financialInformationsByGroupCode.Where(fi => fi.ClaimAdjustmentReasonCode == localReasonCode).ToArray();
                        var adjustmentsSum = adjustmentsByGroupAndReasonCode.CalculateTotalAmount();

                        // Financial Infos are always positive
                        var financialInformationsSum = financialInformationsByGroupAndReasonCode.Sum(fi => fi.Amount);
                        var totalSum = Strings.FormatAsCurrency(Convert.ToDouble(adjustmentsSum + financialInformationsSum));
                        int quantity = adjustmentsByGroupAndReasonCode.Count() + financialInformationsByGroupAndReasonCode.Count();

                        cas2430PatientResponsibility.SetElement(casIndex++, reasonCode.Code);
                        cas2430PatientResponsibility.SetElement(casIndex++, totalSum);
                        cas2430PatientResponsibility.SetElement(casIndex++, quantity.ToString());
                    }
                };
            // Adding specific ordering due to the unit tests outputting groupcodes in a different order
            foreach (ClaimAdjustmentGroupCode groupCode in Enums.GetValues<ClaimAdjustmentGroupCode>().OrderByDescending(i => i.GetDisplayName()))
            {
                int casIndex = 1;
                var localGroupCode = groupCode;
                var adjustmentsByGroupCode = primaryInsuranceInvoiceReceivableForBillingService.Adjustments.Where(a => a.ClaimAdjustmentGroupCode == localGroupCode && a.ClaimAdjustmentReasonCode != null && a.BillingServiceId == billingServiceTransaction.BillingServiceId).ToArray();
                var financialInformationsByGroupCode = primaryInsuranceInvoiceReceivableForBillingService.FinancialInformations.Where(fi => fi.ClaimAdjustmentGroupCode == localGroupCode && fi.ClaimAdjustmentReasonCode != null && fi.BillingServiceId == billingServiceTransaction.BillingServiceId).ToArray();

                if (adjustmentsByGroupCode.Any() || financialInformationsByGroupCode.Any())
                {
                    configureCasSegment(casIndex, groupCode, adjustmentsByGroupCode, financialInformationsByGroupCode);
                }
            }

            var adjustmentsWithoutGroupCode = primaryInsuranceInvoiceReceivableForBillingService.Adjustments.Where(a => a.ClaimAdjustmentReasonCode != null && a.ClaimAdjustmentGroupCode == null && a.BillingServiceId == billingServiceTransaction.BillingServiceId).ToArray();
            var financialInformationsWithoutGroupCode = primaryInsuranceInvoiceReceivableForBillingService.FinancialInformations.Where(fi => fi.ClaimAdjustmentReasonCode != null && fi.ClaimAdjustmentGroupCode == null && fi.BillingServiceId == billingServiceTransaction.BillingServiceId).ToArray();

            if (adjustmentsWithoutGroupCode.Any() || financialInformationsWithoutGroupCode.Any())
            {
                configureCasSegment(1, ClaimAdjustmentGroupCode.OtherAdjustment, adjustmentsWithoutGroupCode, financialInformationsWithoutGroupCode);
            }


            #region helper func getFinancialBatchPaymentDateOrPostedDate

            Func<DateTime?> getFinancialBatchPaymentDateOrPostedDate = () =>
            {
                var financialBatch = primaryInsuranceInvoiceReceivableForBillingService.Adjustments.Select(a => a.FinancialBatch).WhereNotDefault().FirstOrDefault(fb => fb.ExplanationOfBenefitsDateTime.HasValue)
                                     ?? primaryInsuranceInvoiceReceivableForBillingService.FinancialInformations.Select(fi => fi.FinancialBatch).WhereNotDefault().FirstOrDefault(fb => fb.ExplanationOfBenefitsDateTime.HasValue);
                if (financialBatch != null)
                {
                    return financialBatch.ExplanationOfBenefitsDateTime;
                }

                var postedDate = primaryInsuranceInvoiceReceivableForBillingService.Adjustments.Select(a => a.PostedDateTime).FirstOrDefault();
                if (postedDate.Equals(default(DateTime)))
                {
                    postedDate = primaryInsuranceInvoiceReceivableForBillingService.FinancialInformations.Select(a => a.PostedDateTime).FirstOrDefault();
                }
                return postedDate;
            };

            #endregion

            // "DTP" LINE CHECK OR REMITTANCE DATE - loop 2430
            // TODO Possibly update this so that DTP-573 is added to loop 2430 instead of loop 2400
            Segment dtp2430 = l2400.AddSegment("DTP");
            dtp2430.SetElement(1, "573");
            dtp2430.SetElement(2, "D8");
            dtp2430.SetElement(3, getFinancialBatchPaymentDateOrPostedDate().ToString("yyyyMMdd"));
        }

        /// <summary>
        /// Configures the CTP segment for the given 2410 loop.
        /// </summary>
        /// <param name="l2410"></param>
        /// <param name="billingService"></param>
        public void Configure2410(Loop l2410, BillingService billingService)
        {
            // "CTP" DRUG PRICING - loop 2410
            Segment ctp2410 = l2410.AddSegment("CTP");
            if (billingService.Unit != null) ctp2410.SetElement(4, Strings.FormatAsCurrency(Convert.ToDouble(billingService.Unit.Value)));
            ctp2410.SetElement(5, Common.DrugUnitOfMeasurementMappings[(DrugUnitOfMeasurement)billingService.EncounterService.DrugUnitOfMeasurementId]);
        }

        /// <summary>
        /// Adds and configures the SV1 segment for the given 2400 loop.
        /// </summary>
        /// <param name="l2400"></param>
        /// <param name="billingService"></param>
        /// <param name="billingServiceTransaction"></param>
        /// <returns></returns>
        public void Configure2400Sv1(Loop l2400, BillingService billingService, BillingServiceTransaction billingServiceTransaction)
        {
            // "SV1" PROFESSIONAL SERVICE - loop 2400
            Segment sv12400 = l2400.AddSegment("SV1");

            // SV101-1
            var sv101 = new StringBuilder("HC");

            // SV101-2
            bool hasSv1012 = (billingService.EncounterService != null && !(billingService.EncounterService == null && string.IsNullOrEmpty(billingService.EncounterService.Code)));
            if (hasSv1012)
            {
                sv101.AppendFormat(":{0}", billingService.EncounterService.Code.Truncate(5));
            }
            if (!hasSv1012)
            {
                hasSv1012 = (billingService.FacilityEncounterService != null && !(billingService.FacilityEncounterService == null && string.IsNullOrEmpty(billingService.FacilityEncounterService.Code)));
                if (hasSv1012)
                {
                    sv101.AppendFormat(":{0}", billingService.FacilityEncounterService.Code.Truncate(5));
                }
            }
            // SV101-3 through SV101-6 (Procedure Modifiers)
            var procedureModifiers = billingService.BillingServiceModifiers.OrderBy(i => i.OrdinalId).Select(i => i.ServiceModifier.Code).Take(4).ToList();
            if (procedureModifiers.Any())
            {
                if (!hasSv1012)
                {
                    sv101.Append(":");
                    hasSv1012 = true;
                }
                procedureModifiers.ForEach(modifier => sv101.AppendFormat(":{0}", modifier));
            }

            // SV101-7
            var hasSv1017 = (billingService.EncounterService != null && (billingService.UnclassifiedServiceDescription != null));
            if (hasSv1017)
            {
                if (!hasSv1012)
                {
                    sv101.Append(":");
                }
                int numberOfBlankProcedureModifiers = 4 - procedureModifiers.Count;
                while (numberOfBlankProcedureModifiers > 0)
                {
                    numberOfBlankProcedureModifiers--;
                    sv101.Append(":");
                }

                sv101.AppendFormat(":{0}", StripInvalidCharacters(billingService.UnclassifiedServiceDescription));
            }

            sv12400.SetElement(1, sv101.ToString());

            // SV102, SV103, SV104
            if (billingService.UnitCharge != null && billingService.Unit != null)
            {
                string monetaryAmount = Strings.FormatAsCurrency(Convert.ToDouble(billingService.UnitCharge.Value * billingService.Unit.Value));
                sv12400.SetElement(2, monetaryAmount);
                sv12400.SetElement(3, "UN");
                sv12400.SetElement(4, Convert.ToDouble(billingService.Unit.Value).ToString().TrimStart());
            }

            // SV107
            IEnumerable<string> billingDiagnosisOrdinalIds = billingService.BillingServiceBillingDiagnoses.OrderBy(i => i.OrdinalId).Select(i => i.BillingDiagnosis.OrdinalId.ToString());
            string billingDiagnosisOrdinalIdsElement = billingDiagnosisOrdinalIds.Take(4).Join(":");
            sv12400.SetElement(7, billingDiagnosisOrdinalIdsElement);

            // SV109
            // EMG LINE NOTE
            if (billingService.IsEmg)
            {
                // Include Emg
                sv12400.SetElement(9, "Y");
            }

            // SV111
            if (billingServiceTransaction.BillingService.IsEpsdt
                    && billingServiceTransaction.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.Medicaid)
            {
                // Include Epsdt
                sv12400.SetElement(11, "Y");
            }
        }

        /// <summary>
        /// Adds and configures the REF segment for the given 2400 loop.
        /// </summary>
        /// <param name="l2400"></param>
        /// <param name="billingService"></param>
        public void Configure2400Ref(Loop l2400, BillingService billingService)
        {
            //Ref Elements
            Segment ref2400 = l2400.AddSegment("REF");
            ref2400.SetElement(1, "6R");
            ref2400.SetElement(2, billingService.Id.ToString());
        }

        /// <summary>
        /// Adds and configures the NTE segment for the given 2400 loop if the encounter service is unclassified and the billing service contains non-empty comments.
        /// </summary>
        /// <param name="l2400"></param>
        /// <param name="billingService"></param>
        public void Configure2400Nte(Loop l2400, BillingService billingService)
        {
            // "NTE" LINE NOTE - loop 2400
            if (billingService.EncounterService != null && !string.IsNullOrWhiteSpace(billingService.ClaimComment))
            {
                //Include drug part
                Segment nte2400 = l2400.AddSegment("NTE");
                nte2400.SetElement(1, "ADD");
                nte2400.SetElement(2, StripInvalidCharacters(billingService.ClaimComment));
            }
        }

        /// <summary>
        ///  checks if we can add ordering provider to loop or not
        /// </summary>
        /// <param name="inv"></param>
        /// <param name="bServ"></param>
        /// <returns></returns>
        public bool CanAdd2420ELoopForOrderingProvider(InvoiceReceivable inv, BillingService bServ)
        {
            bool valid = false;
            if ((inv.Invoice != null && inv.Invoice.InvoiceSupplemental != null && inv.Invoice.InvoiceSupplemental.OrderingProvider != null)
                && (bServ.EncounterService != null && bServ.EncounterService.IsOrderingProviderRequiredOnClaim))
            {
                if ((bServ.EncounterService != null && bServ.EncounterService.IsReferringDoctorRequiredOnClaim) &&
                    (inv.Invoice.ReferringExternalProvider == null || (inv.Invoice.ReferringExternalProvider.Id != inv.Invoice.InvoiceSupplemental.OrderingProviderId)))
                {
                    valid = true;
                }
                else if (inv.Invoice.InvoiceType == InvoiceType.Vision)
                {
                    valid = true;
                }
            }
            return valid;
        }

        /// <summary>
        /// Adds a 2420E loop that represents the Ordering Provider Name to the given 2400 loop.
        /// </summary>
        /// <param name="l2400"></param>
        /// <param name="orderingProvider"></param>
        /// <param name="invoiceReceivable"></param>
        public void Add2420ELoop(Loop l2400, ExternalProvider orderingProvider, InvoiceReceivable invoiceReceivable)
        {
            // "NM1" ORDERING PROVIDER NAME - loop 2420E
            TypedLoopNM1 nm12420E = l2400.AddLoop(new TypedLoopNM1("DK"));
            nm12420E.NM102_EntityTypeQualifier = EntityTypeQualifier.Person;
            nm12420E.NM103_NameLastOrOrganizationName = orderingProvider.LastNameOrEntityName;
            nm12420E.NM104_NameFirst = orderingProvider.FirstName.Truncate(35);
            nm12420E.NM105_NameMiddle = orderingProvider.MiddleName ?? string.Empty;
            nm12420E.NM107_NameSuffix = orderingProvider.Suffix ?? string.Empty;
            nm12420E.NM108_IdCodeQualifier = "XX";

            string npi = orderingProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
            if (npi == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The Ordering Provider {0}, has no NPI.".FormatWith(orderingProvider.GetFormattedName()), invoiceReceivable.Invoice));
                return;
            }
            if (npi.Length < 2 || npi.Length > 80)
            {
                ValidationResults.Add(new MessageProducerValidationResult("Invalid NPI for the Ordering Provider {0}.".FormatWith(orderingProvider.GetFormattedName()), invoiceReceivable.Invoice));
                return;
            }
            nm12420E.NM109_IdCode = npi;
        }

        /// <summary>
        /// Adds a 2320 loop that represents "Other Subscriber Information" to the given 2300 Claim loop.
        /// </summary>
        /// <param name="clm2300"></param>
        /// <param name="invoiceReceivable"></param>
        /// <param name="otherInvoiceReceivable"></param>
        /// <returns>Returns a TypedLoopSBR that represents the new 2320 loop.</returns>
        public TypedLoopSBR Add2320Loop(TypedLoopCLM clm2300, InvoiceReceivable invoiceReceivable, InvoiceReceivable otherInvoiceReceivable)
        {
            int indexOfCurrentPatientInsurance = GetIndexOfCurrentPatientInsurance(invoiceReceivable, otherInvoiceReceivable);

            // "SBR" OTHER SUBSCRIBER INFORMATION - loop 2320
            TypedLoopSBR sbr2320 = clm2300.AddLoop(new TypedLoopSBR());
            sbr2320.SBR01_PayerResponsibilitySequenceNumberCode = Common.PayerResponsibilitySequenceMap.ContainsKey(indexOfCurrentPatientInsurance) ? Common.PayerResponsibilitySequenceMap[indexOfCurrentPatientInsurance] : Common.PayerResponsibilitySequenceMap[99];

            sbr2320.SBR02_IndividualRelationshipCode = Common.PolicyHolderRelationshipTypeMappings[otherInvoiceReceivable.PatientInsurance.PolicyholderRelationshipType];

            sbr2320.SBR03_PolicyOrGroupNumber = otherInvoiceReceivable.PatientInsurance.InsurancePolicy.GroupCode.IfNull(() => string.Empty);
            if (sbr2320.SBR03_PolicyOrGroupNumber == string.Empty)
            {
                sbr2320.SBR04_GroupName = otherInvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.PlanName.IfNull(() => string.Empty);
            }


            if (otherInvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.MedicarePartB && invoiceReceivable.GetPrimaryPatientInsurance().Id != otherInvoiceReceivable.PatientInsuranceId)
            {
                if (otherInvoiceReceivable.PatientInsurance.InsurancePolicy.MedicareSecondaryReasonCode.HasValue)
                {
                    sbr2320.SBR05_InsuranceTypeCode = Common.MedicareSecondaryReasonCodeMappings[otherInvoiceReceivable.PatientInsurance.InsurancePolicy.MedicareSecondaryReasonCode.Value];
                }
                else if (invoiceReceivable.Invoice.Encounter.Patient.DateOfBirth != null && (System.DateTime.Now.ToClientTime().Year - invoiceReceivable.Invoice.Encounter.Patient.DateOfBirth.Value.Year) >= 65)
                {
                    sbr2320.SBR05_InsuranceTypeCode = Common.MedicareSecondaryReasonCodeMappings[MedicareSecondaryReasonCode.WorkingAged];
                }
            }

            if (Common.ClaimFilingIndicatorCodeMappings.ContainsKey(otherInvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFilingIndicatorCode)) sbr2320.SBR09_ClaimFilingIndicatorCode = Common.ClaimFilingIndicatorCodeMappings[otherInvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFilingIndicatorCode];

            return sbr2320;
        }

        /// <summary>
        /// Configures the AMT segment for the given 2320 loop.
        /// </summary>
        /// <param name="sbr2320"></param>
        /// <param name="invoiceReceivable"></param>
        /// <param name="otherInvoiceReceivable"></param>
        public void Configure2320Amt(TypedLoopSBR sbr2320, InvoiceReceivable invoiceReceivable, InvoiceReceivable otherInvoiceReceivable)
        {
            int indexOfCurrentPatientInsurance = GetIndexOfCurrentPatientInsurance(invoiceReceivable, otherInvoiceReceivable);

            // "AMT" COB PAYER PAID AMOUNT - loop 2320
            if (Common.PayerResponsibilitySequenceMap.ContainsKey(indexOfCurrentPatientInsurance))
            {
                if (Common.PayerResponsibilitySequenceMap[indexOfCurrentPatientInsurance] == "P")
                {
                    Segment amt12320 = sbr2320.AddSegment("AMT");
                    amt12320.SetElement(1, "D");

                    var queuedBillingServiceIds = invoiceReceivable.BillingServiceTransactions
                                                                   .Where(t => t.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued)
                                                                   .Select(transaction => transaction.BillingServiceId);

                    amt12320.SetElement(2, otherInvoiceReceivable.Adjustments
                                                                 .Where(adjustment => adjustment.AdjustmentTypeId == (int)AdjustmentTypeId.Payment && queuedBillingServiceIds.Contains(adjustment.BillingServiceId ?? 0)).CalculateTotalAmount().ToString());
                    // only sum the Adjustments where the main InvoiceReceivable's BillingServiceTransactions BillingServices contains the Adjustments BillingService
                }
            }

        }

        /// <summary>
        /// Adds a new 2330A loop that represents the "Other Subscriber Name" to the 2320 loop.
        /// </summary>
        /// <param name="sbr2320"></param>
        /// <param name="invoiceReceivable"></param>
        /// <returns></returns>
        public TypedLoopNM1 Add2330ALoop(TypedLoopSBR sbr2320, InvoiceReceivable invoiceReceivable)
        {
            // "NM1" OTHER SUBSCRIBER NAME - loop 2330A
            TypedLoopNM1 nm1 = sbr2320.AddLoop(new TypedLoopNM1("IL"));
            nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.Person;
            nm1.NM103_NameLastOrOrganizationName = invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient.LastName;
            nm1.NM104_NameFirst = invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient.FirstName.Truncate(35);
            nm1.NM105_NameMiddle = invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient.MiddleName ?? string.Empty;
            nm1.NM107_NameSuffix = invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient.Suffix ?? string.Empty;
            nm1.NM108_IdCodeQualifier = "MI";
            Regex policyCodeExpression = new Regex(@"^[a-zA-Z0-9]*$");
            if (string.IsNullOrEmpty(invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyCode) || invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyCode.Length < 2 || invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyCode.Length > 80 || !policyCodeExpression.IsMatch(invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyCode))
            {
                ValidationResults.Add(new MessageProducerValidationResult("Invalid PolicyCode for the PolicyholderPatient {0}.".FormatWith(invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient.DisplayName), invoiceReceivable.Invoice));
                return nm1;
            }
            nm1.NM109_IdCode = invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyCode;

            return nm1;
        }

        /// <summary>
        /// Configures the N3 and N4 segments for the given 2330A loop.
        /// </summary>
        /// <param name="nm12330A"></param>
        /// <param name="invoiceReceivable"></param>
        public void Configure2330A(TypedLoopNM1 nm12330A, InvoiceReceivable invoiceReceivable)
        {
            Patient policyHolder = invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient;
            PatientAddress address = policyHolder.PatientAddresses.WithAddressType(PatientAddressTypeId.Home);
            if (address == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult(string.Format("The policy holder {0} does not have a valid address.", policyHolder.GetFormattedName()), invoiceReceivable.Invoice));
                return;
            }

            // "N3" OTHER SUBSCRIBER ADDRESS - loop 2330A
            TypedSegmentN3 n32330A = nm12330A.AddSegment(new TypedSegmentN3());
            if (address.Line1.IsNullOrEmpty())
            {
                ValidationResults.Add(new MessageProducerValidationResult("The address of patient {0} cannot be empty.".FormatWith(policyHolder.GetFormattedName()), invoiceReceivable.Invoice));
                return;
            }
            n32330A.N301_AddressInformation = address.Line1;
            n32330A.N302_AddressInformation = address.Line2.Truncate(55) ?? string.Empty;

            // "N4" OTHER SUBSCRIBER CITY/STATE/ZIP CODE - loop 2330A
            TypedSegmentN4 n42330A = nm12330A.AddSegment(new TypedSegmentN4());
            n42330A.N401_CityName = address.City;
            if (address.StateOrProvince.IsInUnitedStates | address.StateOrProvince.IsInCanada)
            {
                n42330A.N402_StateOrProvinceCode = address.StateOrProvince.Abbreviation;
            }
            else
            {
                n42330A.N407_CountrySubdivisionCode = address.StateOrProvince.Abbreviation;
            }

            if (address.IsValidPostalCode())
            {
                n42330A.N403_PostalCode = address.PostalCode;
            }
            else
            {
                ValidationResults.Add(new MessageProducerValidationResult("The patient {0}, has an invalid zip code.".FormatWith(policyHolder.GetFormattedName()), invoiceReceivable.Invoice));
                return;
            }

            if (!address.StateOrProvince.IsInUnitedStates)
            {
                n42330A.N404_CountryCode = address.StateOrProvince.Country.Abbreviation2Letters;
            }
        }

        /// <summary>
        /// Adds a new 2330B loop that represents the "Other Payer Name" for the given 2320 loop.
        /// </summary>
        /// <param name="sbr2320"></param>
        /// <param name="invoiceReceivable"></param>
        /// <returns></returns>
        public TypedLoopNM1 Add2330BLoop(TypedLoopSBR sbr2320, InvoiceReceivable invoiceReceivable)
        {
            // "NM1" OTHER PAYER NAME - loop 2330B
            TypedLoopNM1 nm1 = sbr2320.AddLoop(new TypedLoopNM1("PR"));
            nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.NonPersonEntity;
            nm1.NM103_NameLastOrOrganizationName = invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.Name;
            nm1.NM108_IdCodeQualifier = "PI";
            string payerCode = invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.PayerCode;
            nm1.NM109_IdCode = payerCode.IsNotNullOrEmpty() ? payerCode : "99999";

            return nm1;
        }

        /// <summary>
        /// Configures the N3, N4 and REF segments for the given 2330B loop.
        /// </summary>
        /// <param name="nm12330B"></param>
        /// <param name="invoiceReceivable"></param>
        public void Configure2330B(TypedLoopNM1 nm12330B, InvoiceReceivable invoiceReceivable)
        {
            Insurer insurer = invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer;
            InsurerAddress address = insurer.InsurerAddresses.WithAddressType(InsurerAddressTypeId.Claims);
            if (address == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult(string.Format("The insurer {0} does not have a valid address.", insurer.Name), invoiceReceivable.Invoice));
                return;
            }

            if (address.Line1.IsNullOrEmpty())
            {
                ValidationResults.Add(new MessageProducerValidationResult("The address of insurer {0} cannot be empty.".FormatWith(insurer.Name), invoiceReceivable.Invoice));
                return;
            }

            // "N3" OTHER PAYER ADDRESS - loop 2330B
            TypedSegmentN3 n32330B = nm12330B.AddSegment(new TypedSegmentN3());
            n32330B.N301_AddressInformation = address.Line1 ?? string.Empty;
            if (!string.IsNullOrEmpty(address.Line2))
            {
                n32330B.N302_AddressInformation = address.Line2.Truncate(55);
            }

            // "N4" OTHER PAYER CITY/STATE/ZIP CODE - loop 2330B
            TypedSegmentN4 n42330B = nm12330B.AddSegment(new TypedSegmentN4());
            n42330B.N401_CityName = address.City;
            if (address.StateOrProvince.IsInUnitedStates | address.StateOrProvince.IsInCanada)
            {
                n42330B.N402_StateOrProvinceCode = address.StateOrProvince.Abbreviation;
            }
            else
            {
                n42330B.N407_CountrySubdivisionCode = address.StateOrProvince.Abbreviation;
            }
            if (address.IsValidPostalCode())
            {
                n42330B.N403_PostalCode = address.PostalCode;
            }
            else
            {
                ValidationResults.Add(new MessageProducerValidationResult("The Insurer {0}, has an invalid zip code.".FormatWith(invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.Name), invoiceReceivable.Invoice));
                return;
            }

            if (!address.StateOrProvince.IsInUnitedStates)
            {
                n42330B.N404_CountryCode = address.StateOrProvince.Country.Abbreviation2Letters;
            }

            if ((invoiceReceivable.PatientInsuranceAuthorization != null))
            {
                // "REF" OTHER PAYER PRIOR AUTHORIZATION NUMBER - loop 2330B
                TypedSegmentREF ref22330B = nm12330B.AddSegment(new TypedSegmentREF());
                ref22330B.REF01_ReferenceIdQualifier = "G1";
                ref22330B.REF02_ReferenceId = invoiceReceivable.PatientInsuranceAuthorization.AuthorizationCode;
            }

            if ((invoiceReceivable.PatientInsuranceReferral != null) && !invoiceReceivable.PatientInsuranceReferral.ExternalProvider.ExcludeOnClaim)
            {
                // "REF" OTHER PAYER REFERRAL NUMBER - loop 2330B
                TypedSegmentREF ref32330B = nm12330B.AddSegment(new TypedSegmentREF());
                ref32330B.REF01_ReferenceIdQualifier = "9F";
                ref32330B.REF02_ReferenceId = invoiceReceivable.PatientInsuranceReferral.ReferralCode;
            }
        }

        /// <summary>
        /// Configures the PRV segment for the 2310B loop.
        /// </summary>
        /// <param name="nm1"></param>
        /// <param name="invoiceReceivable"></param>
        public void Configure2310B(TypedLoopNM1 nm1, InvoiceReceivable invoiceReceivable)
        {
            // "PRV" RENDERING PROVIDER SPECIALTY INFORMATION - loop 2310B
            TypedSegmentPRV prv = nm1.AddSegment(new TypedSegmentPRV());
            prv.PRV01_ProviderCode = "PE";
            prv.PRV02_ReferenceIdQualifier = "PXC";
            Provider billingProvider = invoiceReceivable.Invoice.BillingProvider;
            var taxonomyCode = billingProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy);
            if (taxonomyCode == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The BillingProvider {0}, has no Taxonomy code.".FormatWith(billingProvider.GetNameDescription()), invoiceReceivable.Invoice));
                return;
            }
            prv.PRV03_ProviderTaxonomyCode = taxonomyCode;
        }

        public bool CanAdd2310BLoop(InvoiceReceivable invoiceReceivable)
        {
            BillingOrganization billingOrganization = invoiceReceivable.Invoice.BillingProvider.GetClaimFileOverrideProviderOrCurrent(invoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId).BillingOrganization;
            // This value is the same as 2010AA.NM109, or BillingProvider ID.
            string billingProviderId = billingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
            if (billingProviderId == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("BillingOrganization {0}, has no NPI.".FormatWith(billingOrganization.Name), invoiceReceivable.Invoice));
                return false;
            }
            string renderingProviderId = invoiceReceivable.Invoice.BillingProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
            if (renderingProviderId == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The BillingProvider {0}, has no NPI.".FormatWith(invoiceReceivable.Invoice.BillingProvider.GetNameDescription()), invoiceReceivable.Invoice));
                return false;
            }

            // Is rendering provider different then billing provider (from 2010AA)?  If so, then add rendering provider info by adding loop 2310B.
            return (billingProviderId != renderingProviderId);
        }

        public bool CanAdd2310CLoop(InvoiceReceivable invoiceReceivable)
        {
            // Is service facility location name different then billing provider address (from 2010AA)?  If so, then add service facility location name.
            BillingOrganization billingOrganization = invoiceReceivable.Invoice.BillingProvider.GetClaimFileOverrideProviderOrCurrent(invoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId).BillingOrganization;
            BillingOrganizationAddress billingAddress = billingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PhysicalLocation);
            if (billingAddress == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The BillingOrganization {0}, has no PhysicalLocation Address.".FormatWith(billingOrganization.Name), invoiceReceivable.Invoice));
                return false;
            }
            ServiceLocationAddress serviceLocationAddress = invoiceReceivable.Invoice.Encounter.ServiceLocation.ServiceLocationAddresses.WithAddressType(ServiceLocationAddressTypeId.MainOffice);
            if (serviceLocationAddress == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The ServiceLocation {0}, has no MainOffice Address.".FormatWith(invoiceReceivable.Invoice.Encounter.ServiceLocation.Name), invoiceReceivable.Invoice));
                return false;
            }

            string billingProviderId = invoiceReceivable.Invoice.BillingProvider.BillingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
            if (billingProviderId == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The BillingOrganization {0}, has no NPI.".FormatWith(invoiceReceivable.Invoice.BillingProvider.BillingOrganization.Name), invoiceReceivable.Invoice));
                return false;
            }
            string serviceLocationId = invoiceReceivable.Invoice.Encounter.ServiceLocation.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
            if (serviceLocationId == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The ServiceLocation {0}, has no NPI.".FormatWith(invoiceReceivable.Invoice.Encounter.ServiceLocation.Name), invoiceReceivable.Invoice));
                return false;
            }
            return !(billingAddress.Line1 == serviceLocationAddress.Line1 &&
                    (billingAddress.Line2 == serviceLocationAddress.Line2 | billingAddress.Line2 == null) &&
                     billingAddress.City == serviceLocationAddress.City &&
                     billingAddress.StateOrProvince.Abbreviation == serviceLocationAddress.StateOrProvince.Abbreviation &&
                     billingAddress.PostalCode == serviceLocationAddress.PostalCode &&
                     billingProviderId == serviceLocationId);
        }

        public bool CanAdd2310DLoop(Provider supervisingProvider)
        {
            return (supervisingProvider != null) ? true : false;
        }

        public void Add2310DLoop(TypedLoopCLM l2310D, Provider supervisingProvider, InvoiceReceivable invoiceReceivable)
        {
            string lastOrOrganizationName = supervisingProvider.IfNotNull(x => (x.User != null) ? x.User.LastName : x.ServiceLocation.Name);
            string firstName = supervisingProvider.IfNotNull(x => x.User.IfNotNull(y => y.FirstName)) ?? string.Empty;
            string middleName = supervisingProvider.IfNotNull(x => x.User.IfNotNull(y => y.MiddleName)) ?? string.Empty;
            string suffix = supervisingProvider.IfNotNull(x => x.User.IfNotNull(y => y.Suffix)) ?? string.Empty;

            // "NM1" ORDERING PROVIDER NAME - loop 2310D
            TypedLoopNM1 nm12310D = l2310D.AddLoop(new TypedLoopNM1("DQ"));
            nm12310D.NM102_EntityTypeQualifier = EntityTypeQualifier.Person;
            nm12310D.NM103_NameLastOrOrganizationName = lastOrOrganizationName;
            nm12310D.NM104_NameFirst = firstName;
            nm12310D.NM105_NameMiddle = middleName;
            nm12310D.NM107_NameSuffix = suffix;
            nm12310D.NM108_IdCodeQualifier = "XX";

            string npi = supervisingProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
            if (npi == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The Supervising Provider {0}, has no NPI.".FormatWith(lastOrOrganizationName), invoiceReceivable.Invoice));
                return;
            }
            if (npi.Length < 2 || npi.Length > 80)
            {
                ValidationResults.Add(new MessageProducerValidationResult("Invalid NPI for the Supervising Provider {0}.".FormatWith(lastOrOrganizationName), invoiceReceivable.Invoice));
                return;
            }
            nm12310D.NM109_IdCode = npi;
        }

        /// <summary>
        /// Utility function used by two methods in this instance.
        /// </summary>
        /// <param name="billingServiceTransaction"></param>
        /// <returns></returns>
        private InvoiceReceivable GetPrimaryInsuranceInvoiceReceivableForBillingService(BillingServiceTransaction billingServiceTransaction)
        {
            BillingService billingService = billingServiceTransaction.BillingService;

            // From the patient insurances for the invoice, get the lowest ordinal that has the same insurance type and is active as the current invoice receivable's patient insurance
            PatientInsurance primaryInsurance = billingServiceTransaction.InvoiceReceivable.GetPrimaryPatientInsurance();
            if (primaryInsurance == null)
            {
                ValidationResults.Add(new MessageProducerValidationResult("The Patient {0}, has an invalid InvoiceReceivable containing no Primary Patient Insurance.".FormatWith(billingServiceTransaction.InvoiceReceivable.Invoice.Encounter.Patient.GetFormattedName()), billingServiceTransaction.InvoiceReceivable.Invoice));
                return null;
            }

            // Find invoice receivable that has the primary insurance and contains the current billing service
            return billingServiceTransaction.InvoiceReceivable.Invoice.InvoiceReceivables.FirstOrDefault(i => i.PatientInsuranceId.HasValue && Convert.ToBoolean(i.PatientInsuranceId == primaryInsurance.Id) && i.BillingServiceTransactions.Any(bst => ReferenceEquals(bst.BillingService, billingService)));
        }

        private int GetIndexOfCurrentPatientInsurance(InvoiceReceivable invoiceReceivable, InvoiceReceivable otherInvoiceReceivable)
        {
            // Get the index of this invoice receivable's patient insurance among all active policies for this invoice
            return otherInvoiceReceivable.PatientInsurance.GetIndex(invoiceReceivable.Invoice.InvoiceReceivables.Where(i => i.PatientInsuranceId.HasValue.Equals(true)).Select(i => i.PatientInsurance), invoiceReceivable.Invoice.Encounter.StartDateTime);
        }

        public decimal? GetAmountPaid(InvoiceReceivable invoiceReceivable, int[] billingServiceIdFilters = null)
        {
            bool suppressPatientPayments = invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.DoctorInsurerAssignments.Where(dia => Convert.ToInt32(invoiceReceivable.Invoice.BillingProvider.UserId) == dia.DoctorId).Select(dia => dia.SuppressPatientPayments).FirstOrDefault();
            if (!suppressPatientPayments)
            {
                var adjustments = invoiceReceivable.Invoice.InvoiceReceivables.SelectMany(i => i.Adjustments).ToList();
                if (billingServiceIdFilters != null)
                {
                    adjustments = adjustments.Where(a => a.BillingServiceId.HasValue && billingServiceIdFilters.Contains(a.BillingServiceId.Value)).ToList();
                }

                decimal totalAmount = adjustments.Where(adjustment => adjustment.FinancialSourceType == FinancialSourceType.Patient && adjustment.AdjustmentTypeId == (int)AdjustmentTypeId.Payment).CalculateTotalAmount();
                if (totalAmount > 0) { return totalAmount; }
            }
            return null;
        }

        public decimal? GetEstimatedAmountDue(BillingServiceTransaction[] billingServiceTransactions)
        {
            decimal totalAmount = 0;

            foreach (BillingServiceTransaction billingServiceTransaction in billingServiceTransactions)
            {
                // From the patient insurances for the invoice, get the lowest ordinal that has the same insurance type and is active as the current invoice receivable's patient insurance
                PatientInsurance primaryInsurance = billingServiceTransaction.InvoiceReceivable.GetPrimaryPatientInsurance();
                if (primaryInsurance == null)
                {
                    ValidationResults.Add(new MessageProducerValidationResult("The Patient {0} has an invalid InvoiceReceivable containing no Primary Patient Insurance.".FormatWith(billingServiceTransaction.InvoiceReceivable.Invoice.Encounter.Patient.GetFormattedName()), billingServiceTransaction.InvoiceReceivable.Invoice));
                    return new decimal?();
                }

                // Find invoice receivable that has the primary insurance and contains the current billing service
                InvoiceReceivable primaryInsuranceInvoiceReceivableForBillingService = GetPrimaryInsuranceInvoiceReceivableForBillingService(billingServiceTransaction);

                bool isPrimary = Convert.ToBoolean(primaryInsurance.Id == billingServiceTransaction.InvoiceReceivable.PatientInsuranceId);
                if (!isPrimary && primaryInsuranceInvoiceReceivableForBillingService != null)
                {
                    BillingServiceTransaction bst = billingServiceTransaction;

                    decimal financialDeductibleSum = primaryInsuranceInvoiceReceivableForBillingService.FinancialInformations.Where(j => j.FinancialInformationTypeId == (int)FinancialInformationTypeId.Deductible && Convert.ToBoolean(j.BillingServiceId == bst.BillingServiceId)).Sum(j => j.Amount);

                    decimal financialCoinsuranceSum = primaryInsuranceInvoiceReceivableForBillingService.FinancialInformations.Where(j => j.FinancialInformationTypeId == (int)FinancialInformationTypeId.Coinsurance && Convert.ToBoolean(j.BillingServiceId == bst.BillingServiceId)).Sum(j => j.Amount);

                    totalAmount += financialDeductibleSum + financialCoinsuranceSum;
                }
            }
            return totalAmount > 0 ? totalAmount : new decimal?();
        }

        /// <summary>
        /// Remove invalid chars
        /// </summary>
        /// <param name="actualcontent"></param>
        /// <returns></returns>
        public string StripInvalidCharacters(string actualcontent)
        {
            return string.IsNullOrEmpty(actualcontent) ? string.Empty : actualcontent.Replace("*", string.Empty);
        }


        /// <summary>
        /// Validates the NM104 names i.e. its length should be less than or equal to 35.
        /// </summary>
        /// <param name="invoiceReceivable">The invoice receivable.</param>
        /// <param name="billingServices">The billing services.</param>
        /// <param name="producerType837P">if set to <c>true</c> [producer type837 p].</param>
        public void ValidateNm104Names(InvoiceReceivable invoiceReceivable, BillingService[] billingServices, bool producerType837P)
        {
            ExternalProvider orderingProvider = billingServices.Where(b => b.OrderingExternalProvider != null).Select(p => p.OrderingExternalProvider).FirstOrDefault();
            var user = invoiceReceivable.Invoice.Encounter.Appointments.OfType<UserAppointment>().Select(ua => ua.User).FirstOrDefault();
            var namesToValidate = new[]
            {
                   new { EntityName = typeof(Patient).Name,  Name = invoiceReceivable.Invoice.Encounter.Patient.IfNotNull(p=>p.FirstName), Id= invoiceReceivable.Invoice.Encounter.Patient.IfNotNull(p=>p.Id)}
                  ,new { EntityName = typeof(Patient).Name,  Name = invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient.IfNotNull(p=>p.FirstName),  Id= invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatient.IfNotNull(p=>p.Id)}                                              
                  ,new { EntityName = typeof(ExternalProvider).Name, Name = orderingProvider.IfNotNull(p=>p.FirstName), Id =  orderingProvider.IfNotNull(p=>p.Id)}
                  ,new { EntityName = typeof(ExternalProvider).Name, Name = invoiceReceivable.Invoice.ReferringExternalProvider.IfNotNull(p=>p.FirstName), Id =  invoiceReceivable.Invoice.ReferringExternalProvider.IfNotNull(p=>p.Id)}
                  ,new { EntityName = typeof(ExternalProvider).Name, Name = invoiceReceivable.PatientInsuranceReferral.IfNotNull(p=>p.ExternalProvider.FirstName), Id = invoiceReceivable.PatientInsuranceReferral.IfNotNull(p=>p.ExternalProvider.Id)}
                  ,new { EntityName = typeof(User).Name, Name = user.IfNotNull(p=>p.FirstName), Id = user.IfNotNull(p=>p.Id)}
            }.ToList();

            if (producerType837P)
            {
                Provider renderingOrderingProvider = billingServices.Where(i => (i.IfNotNull(x => x.EncounterService).IfNotNull(x => x.IsOrderingProviderRequiredOnClaim) && i.EncounterService.IsOrderingProviderRequiredOnClaim) && i.Invoice.BillingProvider.User != null).Select(i => i.Invoice.BillingProvider).FirstOrDefault();
                BillingOrganization billingOrganization = invoiceReceivable.Invoice.BillingProvider.GetClaimFileOverrideProviderOrCurrent(invoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId).BillingOrganization;
                var personBillingOrganization = (PersonBillingOrganization)null;
                if (billingOrganization is PersonBillingOrganization)
                    personBillingOrganization = billingOrganization.CastTo<PersonBillingOrganization>();

                namesToValidate.Add(new { EntityName = typeof(BillingOrganization).Name, Name = personBillingOrganization.IfNotNull(p => p.FirstName), Id = personBillingOrganization.IfNotNull(p => p.Id) });
                namesToValidate.Add(new { EntityName = typeof(User).Name, Name = renderingOrderingProvider.IfNotNull(p => p.User.FirstName), Id = renderingOrderingProvider.IfNotNull(p => p.User.Id) });
                namesToValidate.Add(new { EntityName = typeof(User).Name, Name = invoiceReceivable.Invoice.BillingProvider.User.IfNotNull(p => p.FirstName), Id = invoiceReceivable.Invoice.BillingProvider.User.IfNotNull(p => p.Id) });
            }

            foreach (var nameToValidate in namesToValidate.Where(n => n.Name != null && n.Name.Length > 35).GroupBy(i => new { i.EntityName, i.Id }))
            {
                ValidationResults.Add(new MessageProducerValidationResult(string.Format("The {0} {1} has a name that is too long.", nameToValidate.Key.EntityName, nameToValidate.First().Name)));
            }
        }

        public void ValidateN302Addresses(InvoiceReceivable invoiceReceivable, bool producerType837P)
        {
            var addressesToValidate = new[]
            {
                   new { EntityName = typeof(BillingOrganization).Name,  Address = invoiceReceivable.Invoice.BillingProvider.GetClaimFileOverrideProviderOrCurrent(invoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId).BillingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PhysicalLocation).IfNotNull(p=>p.Line2), Id= invoiceReceivable.Invoice.BillingProvider.GetClaimFileOverrideProviderOrCurrent(invoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId).BillingOrganization.IfNotNull(p=>p.Id)}
                  ,new { EntityName = typeof(BillingOrganization).Name,  Address = invoiceReceivable.Invoice.BillingProvider.BillingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PayTo).IfNotNull(p=>p.Line2),  Id= invoiceReceivable.Invoice.BillingProvider.BillingOrganization.IfNotNull(p=>p.Id)}                                              
                  ,new { EntityName = typeof(Patient).Name, Address = invoiceReceivable.Invoice.Encounter.Patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home).IfNotNull(p=>p.Line2), Id =  invoiceReceivable.Invoice.Encounter.Patient.IfNotNull(p=>p.Id)}                 
                  ,new { EntityName = typeof(Insurer).Name, Address = invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.InsurerAddresses.WithAddressType(InsurerAddressTypeId.Claims).IfNotNull(p=>p.Line2), Id = invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.IfNotNull(p=>p.Id)}
            }.ToList();

            if (producerType837P)
            {
                addressesToValidate.Add(new { EntityName = typeof(ServiceLocation).Name, Address = invoiceReceivable.Invoice.Encounter.ServiceLocation.ServiceLocationAddresses.WithAddressType(ServiceLocationAddressTypeId.MainOffice).IfNotNull(p => p.Line2), Id = invoiceReceivable.Invoice.Encounter.ServiceLocation.IfNotNull(p => p.Id) });
            }

            foreach (var addressToValidate in addressesToValidate.Where(n => n.Address != null && n.Address.Length > 55).GroupBy(i => new { i.EntityName, i.Id }))
            {
                ValidationResults.Add(new MessageProducerValidationResult(string.Format("The {0} {1} has a address that is too long.", addressToValidate.Key.EntityName, addressToValidate.First().Id)));
            }
        }
    }
}
