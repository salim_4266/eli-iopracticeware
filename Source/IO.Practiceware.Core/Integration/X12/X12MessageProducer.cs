﻿using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Integration.X12;
using OopFactory.X12.Parsing.Model;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using System.Collections.Generic;

[assembly: Component(typeof (X12MessageProducer.ProduceMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.X12
{
    public class X12MessageProducer
    {
        public class ProduceMessageSubscriber
        {
            public ProduceMessageSubscriber(IMessenger messenger)
            {
                messenger.Subscribe<ProduceMessageRequest>(
                    (s, t, m) => ProduceMessage(m), IsSupported);
            }
        }

        public static bool IsSupported(ProduceMessageRequest m)
        {
            // Check there is a source message
            var isSupported = m.ExternalSystemMessage.Value.IsNotNullOrEmpty() && m.ProducedMessage == null;
            
            if (isSupported)
            {
                // Attempt to load it as X12 message
                try
                {
                    isSupported = LoadMessage(m.ExternalSystemMessage.Value) != null;
                }
                catch
                {
                    isSupported = false;
                }
            }
            return isSupported;
        }

        /// <summary>
        /// Produces the message.
        /// </summary>
        /// <param name="request"> The request. </param>
        public static void ProduceMessage(ProduceMessageRequest request)
        {
            request.ProducedMessage = LoadMessage(request.ExternalSystemMessage.Value);
        }

        private static X12Message LoadMessage(string storedMessage)
        {
            List<Interchange> interchanges = new OopFactory.X12.Parsing.X12Parser().ParseMultiple(storedMessage);
            if (interchanges == null) return null;

            var message = new X12Message(interchanges);
            return message;
        }
    }
}