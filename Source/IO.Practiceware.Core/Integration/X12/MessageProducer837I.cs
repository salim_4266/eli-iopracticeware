﻿using IO.Practiceware.Integration.Common;
using IO.Practiceware.Model;
using OopFactory.X12.Parsing.Model;
using OopFactory.X12.Parsing.Model.Typed;
using Soaf;
using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.X12
{
    /// <summary>
    ///     Produces X12 837I messages.
    /// </summary>
    public class MessageProducer837I
    {
        private readonly MessageProducerUtilities _messageProducerUtilities;
        private readonly IPracticeRepository _practiceRepository;

        public MessageProducer837I(IPracticeRepository practiceRepository,
            MessageProducerUtilities messageProducerUtilities)
        {
            _practiceRepository = practiceRepository;
            _messageProducerUtilities = messageProducerUtilities;
        }

        private string ReferenceIdentifier
        {
            get { return _messageProducerUtilities.ReferenceIdentifier; }
        }

        private DateTime DateTime
        {
            get
            {
                if (_messageProducerUtilities.DateTime == null) _messageProducerUtilities.DateTime = DateTime.Now.ToClientTime();
                return _messageProducerUtilities.DateTime.Value;
            }
        }

        public bool TryProduceInterchange(IEnumerable<BillingServiceTransaction> electronicServiceTransactions, out Interchange message)
        {
            BillingServiceTransaction[] billingServiceTransactions = electronicServiceTransactions.Where(est => _practiceRepository.InsurerClaimForms.
                                                                                                        Any(icf => icf.InsurerId == est.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId &&
                                                                                                                   icf.InvoiceType == est.InvoiceReceivable.Invoice.InvoiceType &&
                                                                                                                                               icf.ClaimFormTypeId == (int)ClaimFormTypeId.Institutional837)).ToArray();
            if (billingServiceTransactions.IsNullOrEmpty())
            {
                message = null;
                return false;
            }

            ClaimFileReceiver claimFileReceiver = billingServiceTransactions.ToList().First().ClaimFileReceiver;
            BillingOrganization primaryOrganization = billingServiceTransactions.ToList().First().InvoiceReceivable.Invoice.BillingProvider.BillingOrganization;


            // This is the message object
            message = new Interchange(DateTime, 1, true);

            // Add and initialize loop 1000
            Transaction transaction = AddAndConfigureLoop1000(message, claimFileReceiver, primaryOrganization);

            if (transaction == null) return false;

            foreach (IGrouping<InvoiceReceivable, BillingServiceTransaction> grouping in billingServiceTransactions.GroupBy(i => i.InvoiceReceivable).OrderBy(i => i.Key.Id))
            {
                // Add and initialize loop 2000
                InvoiceReceivable invoiceReceivable = grouping.Key;
                BillingService[] billingServices = grouping.Where(i => i.BillingService.InvoiceId != null && i.BillingService.InvoiceId.Value == invoiceReceivable.InvoiceId).Select(i => i.BillingService).ToArray();
                _messageProducerUtilities.ValidateNm104Names(invoiceReceivable, billingServices, false);
                _messageProducerUtilities.ValidateN302Addresses(invoiceReceivable, false);

                HierarchicalLoop patientLoop = AddAndConfigureLoop2000(transaction, invoiceReceivable);

                if (patientLoop == null) return false;

                // Add and initialize loop 2300               
                TypedLoopCLM clm2300 = AddAndConfigureLoop2300(patientLoop, invoiceReceivable, billingServices, grouping.ToArray());

                // Add and initialize loop 2400
                AddAndConfigureLoop2400(clm2300, invoiceReceivable, grouping.ToArray());
            }

            // Create the output X12 message string
            return true;
        }

        /// <summary>
        ///     Using the given messasge, initialize its header segments and return a Transaction segment that may be filled with claims.
        /// </summary>
        /// <param name="message">The message to initialize.</param>
        /// <param name="claimFileReceiver">The claim file receiver.</param>
        /// <param name="primaryOrganization">The primary billing organization.</param>
        /// <returns>Returns a Transaction segment that may be filled with claims.</returns>
        private Transaction AddAndConfigureLoop1000(Interchange message, ClaimFileReceiver claimFileReceiver, BillingOrganization primaryOrganization)
        {
            // Allow MessageProducerUtilities to generate ISA, GS, BHT, Loop 1000A, Loop 1000B
            // "ISA" - INTERCHANGE CONTROL HEADER
            // "GS" - FUNCTIONAL GROUP HEADER
            // "ST" TRANSACTION SET HEADER
            // "BHT" BEGINNING OF HIERARCHICAL TRANSACTION  
            // "NM1" SUBMITTER NAME - loop 1000A
            // "NM1" RECEIVER NAME - loop 1000B

            _messageProducerUtilities.ConfigureIsa(message, DateTime, claimFileReceiver);
            FunctionGroup gs = message.AddFunctionGroup("HC", DateTime, 999999, "005010X223A2");

            if (string.IsNullOrEmpty(claimFileReceiver.GSApplicationReceiverCode))
            {
                _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult(string.Format("The ClaimFileReceiver {0} has no GS Application Receiver Code.", claimFileReceiver.Name)));
                return null;
            }
            if (string.IsNullOrEmpty(claimFileReceiver.GSApplicationSenderCode))
            {
                _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult(string.Format("The ClaimFileReceiver {0} has no GS Application Sender Code.", claimFileReceiver.Name)));
                return null;
            }

            gs.ApplicationSendersCode = claimFileReceiver.GSApplicationSenderCode;
            gs.ApplicationReceiversCode = claimFileReceiver.GSApplicationReceiverCode;
            string headerDate = DateTime.ToString("MMddmmssf").TrimStart('0').TrimEnd('0').Truncate(9);
            gs.SetElement(6, headerDate);
            Segment groupTrailerSegment = gs.TrailerSegments.First();
            groupTrailerSegment.SetElement(2, headerDate);

            // Initialize our own ST.
            Transaction transaction = gs.AddTransaction("837", ReferenceIdentifier);
            transaction.SetElement(3, "005010X223A2");

            _messageProducerUtilities.ConfigureBht(transaction, DateTime, ReferenceIdentifier);
            TypedLoopNM1 nm1 = _messageProducerUtilities.Add1000ALoop(transaction, primaryOrganization, claimFileReceiver);
            _messageProducerUtilities.Configure1000A(nm1, primaryOrganization);
            _messageProducerUtilities.Add1000BLoop(transaction, claimFileReceiver);

            return transaction;
        }

        /// <summary>
        ///     Adds and configures a complete 2000 loop that represents the Billing Provider.
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="invoiceReceivable"></param>
        /// <returns>Returns the patient loop.  This may be loop 2000B if the subscriber is also the patient.  Otherwise returns the 2000C loop.</returns>
        private HierarchicalLoop AddAndConfigureLoop2000(Transaction transaction, InvoiceReceivable invoiceReceivable)
        {
            // Add and configure loop 2000A
            HierarchicalLoop hl2000A = _messageProducerUtilities.Add2000ALoop(transaction);
            _messageProducerUtilities.Configure2000A(hl2000A, invoiceReceivable);

            BillingOrganization billingOrganization = invoiceReceivable.Invoice.BillingProvider.GetClaimFileOverrideProviderOrCurrent(invoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId).BillingOrganization;

            // Add and configure loop 2010AA
            TypedLoopNM1 nm12010Aa = _messageProducerUtilities.Add2010AaLoop(hl2000A, billingOrganization, invoiceReceivable);
            nm12010Aa.NM102_EntityTypeQualifier = EntityTypeQualifier.NonPersonEntity;
            nm12010Aa.NM103_NameLastOrOrganizationName = billingOrganization.Name;
            _messageProducerUtilities.Configure2010Aa(nm12010Aa, billingOrganization, invoiceReceivable);

            // Add and configure loop 2010AB
            BillingOrganizationAddress billingAddress = invoiceReceivable.Invoice.BillingProvider.BillingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PayTo);
            if (billingAddress != null)
            {
                TypedLoopNM1 nm12010Ab = _messageProducerUtilities.Add2010AbLoop(hl2000A);
                _messageProducerUtilities.Configure2010Ab(nm12010Ab, billingAddress, invoiceReceivable);
            }

            // Add and configure loop 2000B
            HierarchicalLoop hl2000B = _messageProducerUtilities.Add2000BLoop(transaction, hl2000A, invoiceReceivable);
            _messageProducerUtilities.Configure2000B(hl2000B, invoiceReceivable);

            // Add and configure loop 2010BA                         
            TypedLoopNM1 nm12010Ba = _messageProducerUtilities.Add2000BaLoop(hl2000B, invoiceReceivable.PatientInsurance.InsurancePolicy, invoiceReceivable);
            _messageProducerUtilities.Configure2000Ba(nm12010Ba, invoiceReceivable);

            // Add loop 2010BB
            InsurerAddress insurerAddress = invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.InsurerAddresses.WithAddressType(InsurerAddressTypeId.Claims);
            if (insurerAddress == null || string.IsNullOrEmpty(insurerAddress.Line1))
            {
                _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Insurer {0}, has no or blank Claims Address.".FormatWith(invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.Name), invoiceReceivable.Invoice));
                return null;
            }

            if (insurerAddress.PostalCode.IsNullOrEmpty() || !(insurerAddress.IsValidPostalCode()))
            {
                _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Insurer {0}, has an invalid zip code.".FormatWith(invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.Name), invoiceReceivable.Invoice));
                return null;
            }

            TypedLoopNM1 nm12010Bb = _messageProducerUtilities.Add2010BbLoop(hl2000B, invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer, invoiceReceivable);
            _messageProducerUtilities.Configure2010Bb(nm12010Bb, insurerAddress);

            bool shouldAddHl2000C = invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatientId != invoiceReceivable.PatientInsurance.InsuredPatientId;
            if (shouldAddHl2000C)
            {
                // Add and configure loop 2000C
                HierarchicalLoop hl2000C = _messageProducerUtilities.Add2000CLoop(transaction, hl2000B);
                _messageProducerUtilities.Configure2000C(hl2000C, invoiceReceivable);

                // Add and configure loop 2010CA                            
                TypedLoopNM1 nm12010Ca = _messageProducerUtilities.Add2010CaLoop(hl2000C, invoiceReceivable.Invoice.Encounter.Patient);
                _messageProducerUtilities.Configure2010Ca(nm12010Ca, invoiceReceivable);

                return hl2000C;
            }
            return hl2000B;
        }

        private TypedLoopCLM AddAndConfigureLoop2300(HierarchicalLoopContainer hl, InvoiceReceivable invoiceReceivable, BillingService[] billingServices, BillingServiceTransaction[] billingServiceTransactions)
        {
            #region Helper 2300 actions, configure2300HI, configure2300DTP (434 or STATEMENT DATES), REF Auto Accident Date, configure2300AMT

            Action<TypedLoopCLM> configure2300Hi = claim =>
            {
                // "HI" HEALTH CARE DIAGNOSIS CODE - loop 2300
                Segment hi12300;
                string value;
                bool isIcd10 = false;
                if (invoiceReceivable.Invoice.DiagnosisTypeId != null && invoiceReceivable.Invoice.DiagnosisTypeId == (int) DiagnosisTypeId.Icd10)
                {
                    isIcd10 = true;
                    IntegrationCommon.ReplaceInvoiceWithIcd10(invoiceReceivable.Invoice);
                }

                // HI - PRINCIPAL DIAGNOSIS (First diagnosis)
                BillingDiagnosis principalDiagnosis = invoiceReceivable.Invoice.BillingDiagnoses.OrderBy(i => i.OrdinalId).FirstOrDefault();
                if (principalDiagnosis != null)
                {
                    value = isIcd10 ? "ABK:" : "BK:";
                    value += principalDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntityKey.Replace(".", string.Empty);
                    hi12300 = claim.AddSegment("HI");
                    hi12300.SetElement(1, value);
                }

                // HI - PATIENT'S REASON FOR VISIT (Max of 3)
                int elementIndex = 1;
                hi12300 = null;
                foreach (BillingDiagnosis billingDiagnosis in invoiceReceivable.Invoice.BillingDiagnoses.Where(i => !string.IsNullOrEmpty(i.ExternalSystemEntityMapping.ExternalSystemEntityKey)).OrderBy(i => i.OrdinalId).Take(3))
                {
                    value = "PR:" + billingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntityKey.Replace(".", string.Empty);
                    if (hi12300 == null)
                    {
                        hi12300 = claim.AddSegment("HI");
                    }
                    hi12300.SetElement(elementIndex, value);
                    elementIndex += 1;
                }

                // HI - OTHER DIAGNOSIS INFORMATION (Takes remaining reasons not processed in HI - PATIENT'S REASON FOR VISIT)
                elementIndex = 1;
                hi12300 = null;
                value = isIcd10 ? "ABF": "BF";
                foreach (BillingDiagnosis billingDiagnosis in invoiceReceivable.Invoice.BillingDiagnoses.Where(i => !string.IsNullOrEmpty(i.ExternalSystemEntityMapping.ExternalSystemEntityKey)).OrderBy(i => i.OrdinalId).Skip(3))
                {
                    value += ":" + billingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntityKey.Replace(".", string.Empty);
                    if (hi12300 == null)
                    {
                        hi12300 = claim.AddSegment("HI");
                    }
                    hi12300.SetElement(elementIndex, value);
                    elementIndex += 1;
                }
            };

            Action<TypedLoopCLM, DateTime> configure2300Dtp = (claim, invoiceDateTime) =>
            {
                // "DTP" - STATEMENT DATES - loop 2300
                Segment dtp52300 = claim.AddSegment("DTP");
                dtp52300.SetElement(1, "434");
                dtp52300.SetElement(2, "RD8");
                string dateRange = string.Format("{0}-{0}", invoiceDateTime.ToString("yyyyMMdd"));
                dtp52300.SetElement(3, dateRange);
            };

            Action<TypedLoopCLM> configure2300Ref = claim =>
            {
                if (invoiceReceivable.Invoice.InvoiceSupplemental == null)
                {
                    return;
                }

                // "REF" Auto Accident State - loop 2300
                TypedSegmentREF ref2300 = claim.AddSegment(new TypedSegmentREF());
                ref2300.REF01_ReferenceIdQualifier = "LU";
                ref2300.REF02_ReferenceId = invoiceReceivable.Invoice.InvoiceSupplemental.AccidentStateOrProvince.Abbreviation;
            };

            Action<TypedLoopCLM, double> configure2300Amt = (claim, amt) =>
            {
                // "AMT" PATIENT AMOUNT PAID - loop 2300
                Segment amt2300 = claim.AddSegment("AMT");
                amt2300.SetElement(1, "F3");
                amt2300.SetElement(2, Strings.FormatAsCurrency(amt));
            };

            Action<TypedLoopCLM> configure2300Cl1 = claim =>
            {
                // "CL1" CONTRACT INFORMATION
                Segment cl12300 = claim.AddSegment("CL1");
                cl12300.SetElement(1, "3");
                cl12300.SetElement(3, "01");
            };

            #endregion

            #region Helper 2310A Function Add2310ALoop (Represents Attending Provider Name)

            Func<TypedLoopCLM, TypedLoopNM1> add2310ALoop = claim =>
            {
                User attendingProvider = invoiceReceivable.Invoice.Encounter.Appointments.OfType<UserAppointment>().Select(ua => ua.User).First();
                Provider provider = attendingProvider.Providers.FirstOrDefault();
                if (provider == null)
                {
                    return null;
                }

                TypedLoopNM1 nm1 = claim.AddLoop(new TypedLoopNM1("71"));
                nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.Person;
                nm1.NM103_NameLastOrOrganizationName = attendingProvider.LastName;
                nm1.NM104_NameFirst = attendingProvider.FirstName.Truncate(35);
                nm1.NM105_NameMiddle = attendingProvider.MiddleName ?? string.Empty;
                nm1.NM107_NameSuffix = attendingProvider.Suffix ?? string.Empty;
                nm1.NM108_IdCodeQualifier = "XX";

                string npi = provider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
                if (npi == null)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Provider {0}, has no NPI.".FormatWith(provider.GetNameDescription()), invoiceReceivable.Invoice));
                    return nm1;
                }

                if (npi.Length < 2 || npi.Length > 80)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Provider {0}, has an invalid NPI.".FormatWith(provider.GetNameDescription()), invoiceReceivable.Invoice));
                    return nm1;
                }
                nm1.NM109_IdCode = npi;

                return nm1;
            };

            Action<TypedLoopNM1> configure2310APrv = nm1 =>
            {
                User attendingProvider = invoiceReceivable.Invoice.Encounter.Appointments.OfType<UserAppointment>().Select(ua => ua.User).First();
                Provider provider = attendingProvider.Providers.FirstOrDefault();
                if (provider != null)
                {
                    TypedSegmentPRV prv = nm1.AddSegment(new TypedSegmentPRV());
                    prv.PRV01_ProviderCode = "AT";
                    prv.PRV02_ReferenceIdQualifier = "PXC";
                    var taxonomyCode = provider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy);
                    if (taxonomyCode == null)
                    {
                        _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Provider {0}, has no Taxonomy code.".FormatWith(provider.GetNameDescription()), invoiceReceivable.Invoice));
                        return;
                    }
                    prv.PRV03_ProviderTaxonomyCode = taxonomyCode;
                }
            };

            #endregion

            #region Helper 2310B Function, Add2310BLoop (Represents the Operating Physician)

            Action<TypedLoopCLM> add2310BLoop = claim =>
            {
                User operatingPhysician = invoiceReceivable.Invoice.Encounter.Appointments.OfType<UserAppointment>().Select(ua => ua.User).First();
                Provider provider = operatingPhysician.Providers.FirstOrDefault();
                if (provider == null)
                {
                    return;
                }

                // "NM1" OPERATING PROVIDER NAME - loop 2310B
                TypedLoopNM1 nm1 = claim.AddLoop(new TypedLoopNM1("72"));
                nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.Person;
                nm1.NM103_NameLastOrOrganizationName = operatingPhysician.LastName;
                nm1.NM104_NameFirst = operatingPhysician.FirstName.Truncate(35);
                nm1.NM105_NameMiddle = operatingPhysician.MiddleName ?? string.Empty;
                nm1.NM107_NameSuffix = operatingPhysician.Suffix ?? string.Empty;
                nm1.NM108_IdCodeQualifier = "XX";

                string npi = provider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
                if (npi == null)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Provider {0}, has no NPI.".FormatWith(provider.GetNameDescription()), invoiceReceivable.Invoice));
                    return;
                }

                if (npi.Length < 2 || npi.Length > 80)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Provider {0}, has an invalid NPI.".FormatWith(provider.GetNameDescription()), invoiceReceivable.Invoice));
                    return;
                }
                nm1.NM109_IdCode = npi;
            };

            #endregion

            #region Helper 2310F Function, Add2310FLoop (Represents Referring Provider Name)

            Action<TypedLoopCLM, ExternalProvider> add2310FLoop = (claim, referringProvider) =>
            {
                TypedLoopNM1 nm1 = claim.AddLoop(new TypedLoopNM1("DN"));
                nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.Person;
                nm1.NM103_NameLastOrOrganizationName = referringProvider.LastNameOrEntityName;
                nm1.NM104_NameFirst = referringProvider.FirstName.Truncate(35);
                nm1.NM105_NameMiddle = referringProvider.MiddleName ?? string.Empty;
                nm1.NM107_NameSuffix = referringProvider.Suffix ?? string.Empty;
                nm1.NM108_IdCodeQualifier = "XX";

                string npi = referringProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
                if (npi == null)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The ExternalProvider {0}, has no NPI.".FormatWith(referringProvider.GetFormattedName()), invoiceReceivable.Invoice));
                    return;
                }
                if (npi.Length < 2 || npi.Length > 80)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("Invalid NPI for the ExternalProvider {0}.".FormatWith(referringProvider.GetFormattedName()), invoiceReceivable.Invoice));
                    return;
                }
                nm1.NM109_IdCode = npi;
            };

            #endregion

            // Add and configure loop 2300.
            TypedLoopCLM clm2300 = _messageProducerUtilities.Add2300Loop(hl, invoiceReceivable, billingServices);
            clm2300.CLM05._1_FacilityCodeValue = "83";
            clm2300.CLM05._2_FacilityCodeQualifier = "A";

            _messageProducerUtilities.Configure2300Dtp(clm2300, invoiceReceivable);
            configure2300Dtp(clm2300, invoiceReceivable.Invoice.Encounter.StartDateTime);
            configure2300Cl1(clm2300);

            int indexOfCurrentPatientInsurance = invoiceReceivable.PatientInsurance.GetIndex(invoiceReceivable.Invoice.InvoiceReceivables.Where(i => i.PatientInsuranceId.HasValue.Equals(true)).Select(i => i.PatientInsurance), invoiceReceivable.Invoice.Encounter.StartDateTime);
            if (Common.PayerResponsibilitySequenceMap[indexOfCurrentPatientInsurance] != "P")
            {
                decimal? amountToPay = _messageProducerUtilities.GetEstimatedAmountDue(billingServiceTransactions);
                if (amountToPay != null) configure2300Amt(clm2300, Convert.ToDouble(amountToPay));
            }
            _messageProducerUtilities.Configure2300Ref(clm2300, invoiceReceivable);

            configure2300Ref(clm2300);
            _messageProducerUtilities.Configure2300Nte(clm2300, invoiceReceivable);
            configure2300Hi(clm2300);

            // Add loop 2310A
            TypedLoopNM1 nm12310A = add2310ALoop(clm2300);
            if (nm12310A != null)
            {
                configure2310APrv(nm12310A);
            }

            // Add loop 2310B that represents the Rendering Provider Name.
            add2310BLoop(clm2300);

            // Add and configure loop 2310F
            User user = invoiceReceivable.Invoice.Encounter.Appointments.OfType<UserAppointment>().Select(ua => ua.User).First();
            if (user.Providers.FirstOrDefault() != null)
            {
                string providerId = user.Providers.First().IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
                if (providerId == null)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Provider {0}, has no NPI".FormatWith(user.Providers.First().GetNameDescription()), invoiceReceivable.Invoice));
                    return clm2300;
                }

                if (invoiceReceivable.PatientInsuranceReferralId.HasValue && invoiceReceivable.PatientInsuranceReferralId > 0 && invoiceReceivable.PatientInsuranceReferral == null)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult(string.Format("Invalid Patient Insurance Referral attached to InvoiceReceivable. The PatientInsuranceReferralId {0} doesn't exists.", invoiceReceivable.PatientInsuranceReferralId), invoiceReceivable.Invoice));
                }
                bool hasExternalProvider = false;
                if (invoiceReceivable.PatientInsuranceReferral != null)
                    hasExternalProvider = !invoiceReceivable.PatientInsuranceReferral.ExternalProvider.ExcludeOnClaim;

                if (invoiceReceivable.Invoice.ReferringExternalProviderId.HasValue && invoiceReceivable.Invoice.ReferringExternalProviderId > 0 && invoiceReceivable.Invoice.ReferringExternalProvider == null)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult(string.Format("Invalid Referring External Provider attached to Invoice. The ReferringExternalProviderId {0} doesn't exists.", invoiceReceivable.Invoice.ReferringExternalProviderId), invoiceReceivable.Invoice));
                }
                bool hasReferringProvider = false;
                if (invoiceReceivable.Invoice.ReferringExternalProvider != null)
                    hasReferringProvider = !invoiceReceivable.Invoice.ReferringExternalProvider.ExcludeOnClaim;

                ExternalProvider externalProvider = (hasExternalProvider) ? invoiceReceivable.PatientInsuranceReferral.ExternalProvider :
                                                    (hasReferringProvider) ? invoiceReceivable.Invoice.ReferringExternalProvider : null;
                if (externalProvider != null)
                {
                    string externalProviderId = externalProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
                    if (externalProviderId == null)
                    {
                        _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The ExternalProvider {0}, has no NPI.".FormatWith(externalProvider.GetFormattedName()), invoiceReceivable.Invoice));
                        return clm2300;
                    }

                    if (externalProviderId != providerId)
                    {
                        add2310FLoop(clm2300, externalProvider);
                    }
                }
            }

            // Get other invoice receivables on this invoice with active/matching patient insurances
            IOrderedEnumerable<InvoiceReceivable> otherInvoiceReceivables = invoiceReceivable.Invoice.InvoiceReceivables.Where(i => i.PatientInsurance != null && i.PatientInsurance.OrdinalId != invoiceReceivable.PatientInsurance.OrdinalId && i.PatientInsurance != null && i.PatientInsurance.InsuranceType == invoiceReceivable.PatientInsurance.InsuranceType && i.PatientInsurance.IsActiveForDateTime(i.Invoice.Encounter.StartDateTime)).OrderBy(i => i.PatientInsurance.OrdinalId);

            // Add each of those other invoice receivables to the claim
            foreach (InvoiceReceivable otherInvoiceReceivable in otherInvoiceReceivables)
            {
                // Add and configure loop 2320
                // Include main invoice receivable so we can figure out the adjustments with billing services that are already included on the main invoice receivable
                TypedLoopSBR sbr2320 = _messageProducerUtilities.Add2320Loop(clm2300, invoiceReceivable, otherInvoiceReceivable);
                _messageProducerUtilities.Configure2320Amt(sbr2320, invoiceReceivable, otherInvoiceReceivable);

                #region Configure loop 2320 - "OI" OTHER INSURANCE COVERAGE INFORMATION

                Segment oi2320 = sbr2320.AddSegment("OI");
                oi2320.SetElement(3, otherInvoiceReceivable.Invoice.HasPatientAssignedBenefits ? "Y" : "N");
                oi2320.SetElement(6, otherInvoiceReceivable.Invoice.IsReleaseOfInformationNotSigned ? "I" : "Y");

                #endregion

                // Add and configure loop 2330A
                TypedLoopNM1 nm12330A = _messageProducerUtilities.Add2330ALoop(sbr2320, otherInvoiceReceivable);
                _messageProducerUtilities.Configure2330A(nm12330A, otherInvoiceReceivable);

                // Add and configure loop 2330B
                TypedLoopNM1 nm12330B = _messageProducerUtilities.Add2330BLoop(sbr2320, otherInvoiceReceivable);
                _messageProducerUtilities.Configure2330B(nm12330B, otherInvoiceReceivable);
            }

            return clm2300;
        }

        private void AddAndConfigureLoop2400(TypedLoopCLM clm2300, InvoiceReceivable invoiceReceivable, BillingServiceTransaction[] billingServiceTransactions)
        {
            BillingService[] billingServices = billingServiceTransactions.Where(i => i.BillingService.InvoiceId != null && i.BillingService.InvoiceId.Value == invoiceReceivable.InvoiceId).Select(i => i.BillingService).ToArray();
            ExternalProvider orderingProvider = billingServices.Where(b => b.OrderingExternalProvider != null).Select(p => p.OrderingExternalProvider).FirstOrDefault();
            int s = 0;
            foreach (BillingServiceTransaction billingServiceTransaction in billingServiceTransactions.OrderBy(i => i.BillingService.OrdinalId))
            {
                BillingService billingService = billingServiceTransaction.BillingService;

                #region Helper Action - configure2400DTP

                Action<Loop> configure2400Dtp = loop2400 =>
                {
                    // "DTP" DATE - SERVICE DATE - loop 2400
                    Segment dtp12400 = loop2400.AddSegment("DTP");
                    dtp12400.SetElement(1, "472");
                    dtp12400.SetElement(2, "D8");
                    string dtp3 = billingService.Invoice.Encounter.StartDateTime.ToString("yyyyMMdd");
                    dtp12400.SetElement(3, dtp3);
                };

                #endregion

                #region Helper Action - configure2400SV2

                Action<Loop> configure2400Sv2 = loop2400 =>
                {
                    // "SV2" INSTITUTIONAL SERVICE LINE - loop 2400
                    Segment sv22400 = loop2400.AddSegment("SV2");

                    // SV201
                    var revenueCode = billingService.FacilityEncounterService != null ? billingService.FacilityEncounterService.RevenueCode : null;
                    if (revenueCode == null)
                    {
                        _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Encounter service {0}, does not have a revenue code.".FormatWith(billingService.IfNotNull(x => x.FacilityEncounterService).IfNotNull(x => x.Description) ?? String.Empty), invoiceReceivable.Invoice));
                        return;
                    }
                    sv22400.SetElement(1, revenueCode.Code);

                    // SV202-1
                    var sv202 = new StringBuilder("HC");

                    // SV202-2
                    bool hasSv2022 = (billingService.FacilityEncounterService != null && !(billingService.FacilityEncounterService == null && string.IsNullOrEmpty(billingService.FacilityEncounterService.Code)));
                    if (hasSv2022)
                    {
                        sv202.AppendFormat(":{0}", billingService.FacilityEncounterService.Code.Truncate(5));
                    }

                    // SV202-3 through SV202-6 (Procedure Modifiers)
                    List<string> procedureModifiers = billingService.BillingServiceModifiers.OrderBy(i => i.OrdinalId).Select(i => i.ServiceModifier.Code).Take(4).ToList();
                    if (procedureModifiers.Any())
                    {
                        if (!hasSv2022)
                        {
                            sv202.Append(":");
                            hasSv2022 = true;
                        }
                        procedureModifiers.ForEach(modifier => sv202.AppendFormat(":{0}", modifier));
                    }

                    // SV202-7
                    if (!string.IsNullOrEmpty(billingService.UnclassifiedServiceDescription))
                    {
                        if (!hasSv2022)
                        {
                            sv202.Append(":");
                        }
                        int numberOfBlankProcedureModifiers = 4 - procedureModifiers.Count;
                        while (numberOfBlankProcedureModifiers > 0)
                        {
                            numberOfBlankProcedureModifiers--;
                            sv202.Append(":");
                        }

                        sv202.AppendFormat(":{0}", _messageProducerUtilities.StripInvalidCharacters(billingService.UnclassifiedServiceDescription));
                    }

                    sv22400.SetElement(2, sv202.ToString());

                    // SV203, SV204, SV205
                    if (billingService.UnitCharge != null && billingService.Unit != null)
                    {
                        sv22400.SetElement(3, Strings.FormatAsCurrency(Convert.ToDouble(billingService.UnitCharge.Value * billingService.Unit.Value)));
                        sv22400.SetElement(4, "UN");
                        sv22400.SetElement(5, Strings.FormatAsCurrency(Convert.ToDouble(billingService.Unit.Value)));
                    }
                };

                #endregion

                // Add loop 2400
                Loop lx2400 = clm2300.AddLoop("LX");
                lx2400.SetElement(1, (++s).ToString());

                // Configure loop 2400
                configure2400Sv2(lx2400);
                configure2400Dtp(lx2400);
                _messageProducerUtilities.Configure2400Ref(lx2400, billingService);

                // Add and configure loop 2410
                Loop lin2410;
                if (_messageProducerUtilities.TryAdd2410Loop(lx2400, billingService, out lin2410))
                {
                    _messageProducerUtilities.Configure2410(lin2410, billingService);
                }

                // Add loop 2420E
                if ((orderingProvider != null))
                {
                    _messageProducerUtilities.Add2420ELoop(lx2400, orderingProvider, invoiceReceivable);
                }

                // Add and configure loop 2430
                Loop svd2430;
                if (_messageProducerUtilities.TryAdd2430Loop(lx2400, billingServiceTransaction, out svd2430))
                {
                    _messageProducerUtilities.Configure2430(svd2430, lx2400, billingServiceTransaction);
                }
            }
        }
    }
}
