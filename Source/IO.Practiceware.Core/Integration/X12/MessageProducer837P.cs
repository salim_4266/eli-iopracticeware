﻿
using IO.Practiceware.Integration.Common;
using IO.Practiceware.Model;
using OopFactory.X12.Parsing.Model;
using OopFactory.X12.Parsing.Model.Typed;
using Soaf;
using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Integration.X12
{
    /// <summary>
    ///     Produces X12 837P messages.
    /// </summary>
    public class MessageProducer837P
    {
        private readonly MessageProducerUtilities _messageProducerUtilities;
        private readonly IPracticeRepository _practiceRepository;

        public MessageProducer837P(IPracticeRepository practiceRepository,
            MessageProducerUtilities messageProducerUtilities)
        {
            _practiceRepository = practiceRepository;
            _messageProducerUtilities = messageProducerUtilities;
        }

        private string ReferenceIdentifier
        {
            get { return _messageProducerUtilities.ReferenceIdentifier; }
        }

        private DateTime DateTime
        {
            get
            {
                if (!_messageProducerUtilities.DateTime.HasValue) _messageProducerUtilities.DateTime = DateTime.Now.ToClientTime();
                return _messageProducerUtilities.DateTime.Value;
            }
        }

        public bool TryProduceInterchange(BillingServiceTransaction[] electronicServiceTransactions, out Interchange message)
        {
            // Get all billing service transactions where the claim form type id = 3 or when there is no claim form.
            List<BillingServiceTransaction> billingServiceTransactionsProfessional = electronicServiceTransactions.Where(est => _practiceRepository.InsurerClaimForms.
                                                                                                                    Any(icf => icf.InsurerId == est.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId &&
                                                                                                                               icf.InvoiceType == est.InvoiceReceivable.Invoice.InvoiceType &&
                                                                                                                                                               icf.ClaimFormTypeId == (int)ClaimFormTypeId.Professional837)).ToList();

            billingServiceTransactionsProfessional.AddRange(electronicServiceTransactions.Where(est => _practiceRepository.InsurerClaimForms.
                                                                                                           Where(icf => icf.InsurerId == est.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId &&
                                                                                                                        icf.InvoiceType == est.InvoiceReceivable.Invoice.InvoiceType).
                                                                                                           IsNullOrEmpty()));

            if (billingServiceTransactionsProfessional.IsNullOrEmpty())
            {
                message = null;
                return false;
            }

            BillingServiceTransaction[] medicalBillingServiceTransactions = billingServiceTransactionsProfessional.Where(bst => bst.InvoiceReceivable.Invoice.InvoiceType == InvoiceType.Professional ||
                                                                                                                                bst.InvoiceReceivable.Invoice.InvoiceType == InvoiceType.Vision).ToArray();
            BillingServiceTransaction[] facilityBillingServiceTransactions = billingServiceTransactionsProfessional.Where(bst => bst.InvoiceReceivable.Invoice.InvoiceType == InvoiceType.Facility).ToArray();

            // This is the message object
            message = new Interchange(DateTime, 1, true);

            #region Initialize 837 Message, Loop 1000

            ClaimFileReceiver claimFileReceiver;
            BillingOrganization primaryOrganization;

            if (medicalBillingServiceTransactions.Any())
            {
                claimFileReceiver = medicalBillingServiceTransactions.ToList().First().ClaimFileReceiver;
                primaryOrganization = medicalBillingServiceTransactions.ToList().First().InvoiceReceivable.Invoice.BillingProvider.BillingOrganization;
            }
            else
            {
                claimFileReceiver = facilityBillingServiceTransactions.ToList().First().ClaimFileReceiver;
                primaryOrganization = facilityBillingServiceTransactions.ToList().First().InvoiceReceivable.Invoice.BillingProvider.BillingOrganization;
            }
            // Add and initialize loop 1000
            Transaction transaction = AddAndConfigureLoop1000(message, claimFileReceiver, primaryOrganization);

            if (transaction == null) return false;

            #endregion

            #region Initialize Loop 2000, 2300, and 2400 for 837P

            if (facilityBillingServiceTransactions.Any())
            {
                foreach (IGrouping<InvoiceReceivable, BillingServiceTransaction> grouping in facilityBillingServiceTransactions.GroupBy(i => i.InvoiceReceivable).OrderBy(i => i.Key.Id))
                {
                    // Add and initialize loop 2000
                    InvoiceReceivable invoiceReceivable = grouping.Key;
                    BillingService[] billingServices = grouping.Where(i => i.BillingService.InvoiceId != null && i.BillingService.InvoiceId.Value == invoiceReceivable.InvoiceId).Select(i => i.BillingService).ToArray();
                    _messageProducerUtilities.ValidateNm104Names(invoiceReceivable, billingServices, true);
                    _messageProducerUtilities.ValidateN302Addresses(invoiceReceivable, true);
                    HierarchicalLoop patientLoop = AddAndConfigureLoop2000(transaction, invoiceReceivable);

                    if (patientLoop == null) return false;

                    // Add and initialize loop 2300                    
                    TypedLoopCLM clm2300 = AddAndConfigureLoop2300(patientLoop, invoiceReceivable, billingServices, false);

                    // Add and initialize loop 2400
                    AddAndConfigureLoop2400(clm2300, invoiceReceivable, grouping.ToArray());
                }
            }

            #endregion

            #region Initialize Loop 2000, 2300, and 2400 for 837

            if (medicalBillingServiceTransactions.Any())
            {
                foreach (IGrouping<InvoiceReceivable, BillingServiceTransaction> grouping in medicalBillingServiceTransactions.GroupBy(i => i.InvoiceReceivable).OrderBy(i => i.Key.Id))
                {
                    // Add and initialize loop 2000
                    InvoiceReceivable invoiceReceivable = grouping.Key;
                    BillingService[] billingServices = grouping.Where(i => i.BillingService.InvoiceId != null && i.BillingService.InvoiceId.Value == invoiceReceivable.InvoiceId).Select(i => i.BillingService).ToArray();
                    _messageProducerUtilities.ValidateNm104Names(invoiceReceivable, billingServices, true);
                    _messageProducerUtilities.ValidateN302Addresses(invoiceReceivable, true);
                    HierarchicalLoop patientLoop = AddAndConfigureLoop2000(transaction, invoiceReceivable);

                    if (patientLoop == null) return false;

                    // Add and initialize loop 2300                   
                    TypedLoopCLM clm2300 = AddAndConfigureLoop2300(patientLoop, invoiceReceivable, billingServices, true);

                    // Add and initialize loop 2400
                    AddAndConfigureLoop2400(clm2300, invoiceReceivable, grouping.ToArray());
                }
            }

            #endregion

            return true;
        }

        /// <summary>
        ///     Using the given messasge, initialize its header segments and return a Transaction segment that may be filled with claims.
        /// </summary>
        /// <param name="message">The message to initialize.</param>
        /// <param name="claimFileReceiver">The claim file receiver.</param>
        /// <param name="primaryOrganization">The primary billing organization.</param>
        /// <returns>Returns a Transaction segment that may be filled with claims.</returns>
        private Transaction AddAndConfigureLoop1000(Interchange message, ClaimFileReceiver claimFileReceiver, BillingOrganization primaryOrganization)
        {
            // Allow MessageProducerUtilities to generate ISA, GS, BHT, Loop 1000A, Loop 1000B
            // "ISA" - INTERCHANGE CONTROL HEADER
            // "GS" - FUNCTIONAL GROUP HEADER
            // "ST" TRANSACTION SET HEADER
            // "BHT" BEGINNING OF HIERARCHICAL TRANSACTION  
            // "NM1" SUBMITTER NAME - loop 1000A
            // "NM1" RECEIVER NAME - loop 1000B

            _messageProducerUtilities.ConfigureIsa(message, DateTime, claimFileReceiver);
            FunctionGroup gs = message.AddFunctionGroup("HC", DateTime, 999999, "005010X222A1");

            if (string.IsNullOrEmpty(claimFileReceiver.GSApplicationReceiverCode))
            {
                _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult(string.Format("The ClaimFileReceiver {0} has no GS Application Receiver Code.", claimFileReceiver.Name)));
                return null;
            }
            if (string.IsNullOrEmpty(claimFileReceiver.GSApplicationSenderCode))
            {
                _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult(string.Format("The ClaimFileReceiver {0} has no GS Application Sender Code.", claimFileReceiver.Name)));
                return null;
            }

            gs.ApplicationSendersCode = claimFileReceiver.GSApplicationSenderCode;
            gs.ApplicationReceiversCode = claimFileReceiver.GSApplicationReceiverCode;
            string headerDate = DateTime.ToString("MMddmmssf").TrimStart('0').TrimEnd('0').Truncate(9);
            gs.SetElement(6, headerDate);
            Segment groupTrailerSegment = gs.TrailerSegments.First();
            groupTrailerSegment.SetElement(2, headerDate);

            // Initialize our own ST.
            Transaction transaction = gs.AddTransaction("837", ReferenceIdentifier);
            transaction.SetElement(3, "005010X222A1");

            _messageProducerUtilities.ConfigureBht(transaction, DateTime, ReferenceIdentifier);
            TypedLoopNM1 nm1 = _messageProducerUtilities.Add1000ALoop(transaction, primaryOrganization, claimFileReceiver);
            _messageProducerUtilities.Configure1000A(nm1, primaryOrganization);
            _messageProducerUtilities.Add1000BLoop(transaction, claimFileReceiver);

            return transaction;
        }

        /// <summary>
        ///     Adds and configures a complete 2000 loop that represents the Billing Provider.
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="invoiceReceivable"></param>
        /// <returns>Returns the patient loop.  This may be loop 2000B if the subscriber is also the patient.  Otherwise returns the 2000C loop.</returns>
        private HierarchicalLoop AddAndConfigureLoop2000(Transaction transaction, InvoiceReceivable invoiceReceivable)
        {
            // Add and configure loop 2000A
            HierarchicalLoop hl2000A = _messageProducerUtilities.Add2000ALoop(transaction);
            _messageProducerUtilities.Configure2000A(hl2000A, invoiceReceivable);

            BillingOrganization billingOrganization = invoiceReceivable.Invoice.BillingProvider.GetClaimFileOverrideProviderOrCurrent(invoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId).BillingOrganization;

            // Add and configure loop 2010AA
            TypedLoopNM1 nm12010Aa = _messageProducerUtilities.Add2010AaLoop(hl2000A, billingOrganization, invoiceReceivable);
            if (billingOrganization is PersonBillingOrganization)
            {
                var personBillingOrganization = billingOrganization.CastTo<PersonBillingOrganization>();
                nm12010Aa.NM102_EntityTypeQualifier = EntityTypeQualifier.Person;
                nm12010Aa.NM103_NameLastOrOrganizationName = personBillingOrganization.LastName;
                nm12010Aa.NM104_NameFirst = personBillingOrganization.FirstName.Truncate(35);
                nm12010Aa.NM105_NameMiddle = personBillingOrganization.MiddleName ?? string.Empty;
                nm12010Aa.NM107_NameSuffix = personBillingOrganization.Suffix ?? string.Empty;
            }
            else
            {
                nm12010Aa.NM102_EntityTypeQualifier = EntityTypeQualifier.NonPersonEntity;
                nm12010Aa.NM103_NameLastOrOrganizationName = billingOrganization.Name;
            }
            _messageProducerUtilities.Configure2010Aa(nm12010Aa, billingOrganization, invoiceReceivable);

            // Add and configure loop 2010AB
            BillingOrganizationAddress billingAddress = invoiceReceivable.Invoice.BillingProvider.BillingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PayTo);
            if (billingAddress != null)
            {
                TypedLoopNM1 nm12010Ab = _messageProducerUtilities.Add2010AbLoop(hl2000A);
                _messageProducerUtilities.Configure2010Ab(nm12010Ab, billingAddress, invoiceReceivable);
            }

            // Add and configure loop 2000B
            HierarchicalLoop hl2000B = _messageProducerUtilities.Add2000BLoop(transaction, hl2000A, invoiceReceivable);
            _messageProducerUtilities.Configure2000B(hl2000B, invoiceReceivable);

            // Add and configure loop 2010BA            
            TypedLoopNM1 nm12010Ba = _messageProducerUtilities.Add2000BaLoop(hl2000B, invoiceReceivable.PatientInsurance.InsurancePolicy, invoiceReceivable);
            _messageProducerUtilities.Configure2000Ba(nm12010Ba, invoiceReceivable);

            // Add loop 2010BB
            InsurerAddress insurerAddress = invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.InsurerAddresses.WithAddressType(InsurerAddressTypeId.Claims);
            if (insurerAddress == null || string.IsNullOrEmpty(insurerAddress.Line1))
            {
                _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Insurer {0}, has no or blank Claims Address.".FormatWith(invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.Name), invoiceReceivable.Invoice));
                return null;
            }

            if (insurerAddress.PostalCode.IsNullOrEmpty() || !(insurerAddress.IsValidPostalCode()))
            {
                _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Insurer {0}, has an invalid zip code.".FormatWith(invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.Name), invoiceReceivable.Invoice));
                return null;
            }

            TypedLoopNM1 nm12010Bb = _messageProducerUtilities.Add2010BbLoop(hl2000B, invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer, invoiceReceivable);
            _messageProducerUtilities.Configure2010Bb(nm12010Bb, insurerAddress);

            bool shouldAddHl2000C = invoiceReceivable.PatientInsurance.InsurancePolicy.PolicyholderPatientId != invoiceReceivable.PatientInsurance.InsuredPatientId;
            if (shouldAddHl2000C)
            {
                // Add and configure loop 2000C
                HierarchicalLoop hl2000C = _messageProducerUtilities.Add2000CLoop(transaction, hl2000B);
                _messageProducerUtilities.Configure2000C(hl2000C, invoiceReceivable);

                // Add and configure loop 2010CA                
                TypedLoopNM1 nm12010Ca = _messageProducerUtilities.Add2010CaLoop(hl2000C, invoiceReceivable.Invoice.Encounter.Patient);
                _messageProducerUtilities.Configure2010Ca(nm12010Ca, invoiceReceivable);

                return hl2000C;
            }
            return hl2000B;
        }

        /// <summary>
        /// </summary>
        /// <param name="hl"></param>
        /// <param name="invoiceReceivable"></param>
        /// <param name="billingServices"></param>
        /// <param name="isProducingForMedicalClaim">Flag that indicates whether we are producing for a medical claim or a facility claim.</param>
        /// <returns></returns>
        private TypedLoopCLM AddAndConfigureLoop2300(HierarchicalLoopContainer hl, InvoiceReceivable invoiceReceivable, BillingService[] billingServices, bool isProducingForMedicalClaim)
        {
            #region Helper HI action, Configure 'HI' segment for the given 2300 loop

            Action<TypedLoopCLM> configure2300Hi = claim =>
            {
                // "HI" HEALTH CARE DIAGNOSIS CODE - loop 2300
                int elementIndex = 1;
                bool isIcd10 = false;
                if (invoiceReceivable.Invoice.DiagnosisTypeId != null && invoiceReceivable.Invoice.DiagnosisTypeId == (int)DiagnosisTypeId.Icd10)
                {
                    isIcd10 = true;
                    IntegrationCommon.ReplaceInvoiceWithIcd10(invoiceReceivable.Invoice);
                }

                Segment hi12300 = null;
                foreach (BillingDiagnosis billingDiagnosis in invoiceReceivable.Invoice.BillingDiagnoses.OrderBy(i => i.OrdinalId).Take(12))
                {
                    if (billingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntityKey.IsNullOrEmpty()) continue;

                    string value = elementIndex == 1 ? (isIcd10 ? "ABK" : "BK") : (isIcd10 ? "ABF" : "BF");

                    value += ":" + billingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntityKey.Replace(".", string.Empty);
                    if (hi12300 == null)
                    {
                        hi12300 = claim.AddSegment("HI");
                    }
                    hi12300.SetElement(elementIndex, value);
                    elementIndex += 1;
                }
            };

            #endregion

            #region Helper AMT action, Configure 'AMT' segment for the given 2300 loop

            Action<TypedLoopCLM, double> configure2300Amt = (claim, amt) =>
            {
                // "AMT" PATIENT AMOUNT PAID - loop 2300
                Segment amt2300 = claim.AddSegment("AMT");
                amt2300.SetElement(1, "F5");
                amt2300.SetElement(2, amt.FormatAsCurrency());
            };

            #endregion

            #region Helper 2310B Function, Add2310BLoop

            Func<TypedLoopCLM, TypedLoopNM1> add2310BLoop = claim =>
                                                                {
                                                                    // "NM1" RENDERING PROVIDER NAME - loop 2310B
                                                                    TypedLoopNM1 nm1 = claim.AddLoop(new TypedLoopNM1("82"));

                                                                    nm1.NM102_EntityTypeQualifier = invoiceReceivable.Invoice.BillingProvider.User != null ? EntityTypeQualifier.Person : EntityTypeQualifier.NonPersonEntity;

                                                                    if (invoiceReceivable.Invoice.BillingProvider.User != null)
                                                                    {
                                                                        nm1.NM103_NameLastOrOrganizationName = invoiceReceivable.Invoice.BillingProvider.User.LastName;
                                                                        nm1.NM104_NameFirst = invoiceReceivable.Invoice.BillingProvider.User.FirstName.Truncate(35);
                                                                        nm1.NM105_NameMiddle = invoiceReceivable.Invoice.BillingProvider.User.MiddleName ?? string.Empty;
                                                                        nm1.NM107_NameSuffix = invoiceReceivable.Invoice.BillingProvider.User.Suffix ?? string.Empty;
                                                                    }
                                                                    else if (invoiceReceivable.Invoice.BillingProvider.ServiceLocation != null)
                                                                    {
                                                                        nm1.NM103_NameLastOrOrganizationName = invoiceReceivable.Invoice.BillingProvider.ServiceLocation.Name;
                                                                    }
                                                                    else
                                                                    {
                                                                        _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Billing Provider for the invoice does not have a user or a service location.", invoiceReceivable.Invoice));
                                                                        return nm1;
                                                                    }

                                                                    nm1.NM108_IdCodeQualifier = "XX";

                                                                    string npi = invoiceReceivable.Invoice.BillingProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
                                                                    if (npi == null)
                                                                    {
                                                                        _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The BillingProvider {0}, has no NPI.".FormatWith(invoiceReceivable.Invoice.BillingProvider.GetNameDescription()), invoiceReceivable.Invoice));
                                                                        return nm1;
                                                                    }

                                                                    if (npi.Length < 2 || npi.Length > 80)
                                                                    {
                                                                        _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The BillingProvider {0}, has an invalid NPI.".FormatWith(invoiceReceivable.Invoice.BillingProvider.GetNameDescription()), invoiceReceivable.Invoice));
                                                                        return nm1;
                                                                    }
                                                                    nm1.NM109_IdCode = npi;

                                                                    return nm1;
                                                                };

            #endregion

            // Add and configure loop 2300.
            TypedLoopCLM clm2300 = _messageProducerUtilities.Add2300Loop(hl, invoiceReceivable, billingServices);
            clm2300.CLM05._1_FacilityCodeValue = invoiceReceivable.Invoice.ServiceLocationCode.PlaceOfServiceCode;
            clm2300.CLM05._2_FacilityCodeQualifier = "B";
            clm2300.CLM06_ProviderOrSupplierSignatureIndicator = true;

            _messageProducerUtilities.Configure2300Dtp(clm2300, invoiceReceivable);

            decimal? amountPaid = _messageProducerUtilities.GetAmountPaid(invoiceReceivable, billingServices.Select(b => b.Id).ToArray());
            if (amountPaid != null) configure2300Amt(clm2300, Convert.ToDouble(amountPaid));

            _messageProducerUtilities.Configure2300Ref(clm2300, invoiceReceivable);

            //Configure Clia Certification Number in REF Segment before HI Segment
            Configure2300RefClia(clm2300, invoiceReceivable);
            _messageProducerUtilities.Configure2300Nte(clm2300, invoiceReceivable);
            configure2300Hi(clm2300);

            ExternalProvider orderingProvider = invoiceReceivable.IfNotNull(x => x.Invoice.IfNotNull(y => y.InvoiceSupplemental.IfNotNull(z => z.OrderingProvider)));
            Provider renderingOrderingProvider = billingServices.Where(i => (i.IfNotNull(x => x.EncounterService).IfNotNull(x => x.IsOrderingProviderRequiredOnClaim) && i.EncounterService.IsOrderingProviderRequiredOnClaim) && i.Invoice.BillingProvider.User != null).Select(i => i.Invoice.BillingProvider).FirstOrDefault();

            // Add and configure loop 2310A for a medical claim or a facility claim
            if (!isProducingForMedicalClaim)
            {
                Add2310ALoopFacilityClaim(clm2300, invoiceReceivable);
            }
            else
            {
                if (invoiceReceivable.PatientInsuranceReferralId.HasValue && invoiceReceivable.PatientInsuranceReferralId > 0 && invoiceReceivable.PatientInsuranceReferral == null)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult(string.Format("Invalid Patient Insurance Referral attached to InvoiceReceivable. The PatientInsuranceReferralId {0} doesn't exists.", invoiceReceivable.PatientInsuranceReferralId), invoiceReceivable.Invoice));
                }
                bool hasExternalProvider = false;
                if (invoiceReceivable.PatientInsuranceReferral != null)
                    hasExternalProvider = !invoiceReceivable.PatientInsuranceReferral.ExternalProvider.ExcludeOnClaim;

                if (invoiceReceivable.Invoice.ReferringExternalProviderId.HasValue && invoiceReceivable.Invoice.ReferringExternalProviderId > 0 && invoiceReceivable.Invoice.ReferringExternalProvider == null)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult(string.Format("Invalid Referring External Provider attached to Invoice. The ReferringExternalProviderId {0} doesn't exists.", invoiceReceivable.Invoice.ReferringExternalProviderId), invoiceReceivable.Invoice));
                }
                bool hasReferringProvider = false;
                if (invoiceReceivable.Invoice.ReferringExternalProvider != null)
                    hasReferringProvider = !invoiceReceivable.Invoice.ReferringExternalProvider.ExcludeOnClaim;

                if (hasExternalProvider)
                {
                    Add2310ALoopMedicalClaim(clm2300, invoiceReceivable.PatientInsuranceReferral.ExternalProvider, invoiceReceivable);
                }
                else if (hasReferringProvider)
                {
                    Add2310ALoopMedicalClaim(clm2300, invoiceReceivable.Invoice.ReferringExternalProvider, invoiceReceivable);
                }
                else if (orderingProvider != null)
                {
                    Add2310ALoopMedicalClaim(clm2300, orderingProvider, invoiceReceivable);
                }

                else if (renderingOrderingProvider != null && invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.IsMedicare)
                {
                    Add2310ALoopMedicalClaim(clm2300, renderingOrderingProvider, invoiceReceivable);
                }
            }

            // Add and configure loop 2310B that represents the Rendering Provider Name.
            if (_messageProducerUtilities.CanAdd2310BLoop(invoiceReceivable))
            {
                TypedLoopNM1 nm12310B = add2310BLoop(clm2300);
                _messageProducerUtilities.Configure2310B(nm12310B, invoiceReceivable);
            }

            // Add and configure loop 2310C that represents the Service Facility Location Name.
            if (_messageProducerUtilities.CanAdd2310CLoop(invoiceReceivable))
            {
                ServiceLocationAddress address = invoiceReceivable.Invoice.Encounter.ServiceLocation.ServiceLocationAddresses.WithAddressType(ServiceLocationAddressTypeId.MainOffice);
                if (address == null)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The ServiceLocation {0}, has no MainOffice Address.".FormatWith(invoiceReceivable.Invoice.BillingProvider.ServiceLocation.Name), invoiceReceivable.Invoice));
                    return clm2300;
                }

                #region Helper Functions - Add 2310C Loop, Configure 2310C Loop

                Func<TypedLoopNM1> add2310CLoop = () =>
                {
                    // "NM1" SERVICE FACILITY LOCATION - loop 2310C
                    TypedLoopNM1 nm1 = clm2300.AddLoop(new TypedLoopNM1("77"));
                    nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.NonPersonEntity;
                    nm1.NM103_NameLastOrOrganizationName = invoiceReceivable.Invoice.Encounter.ServiceLocation.Name;
                    nm1.NM108_IdCodeQualifier = "XX";
                    var code = invoiceReceivable.Invoice.Encounter.ServiceLocation.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
                    if (code == null)
                    {
                        _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The ServiceLocation {0}, has no NPI.".FormatWith(invoiceReceivable.Invoice.BillingProvider.ServiceLocation.Name), invoiceReceivable.Invoice));
                        return nm1;
                    }
                    nm1.NM109_IdCode = code;
                    return nm1;
                };

                Action<TypedLoopNM1> configure2310C = nm1 =>
                {
                    // "N3" SERVICE FACILITY LOCATION ADDRESS - loop 2310C
                    TypedSegmentN3 n3 = nm1.AddSegment(new TypedSegmentN3());
                    n3.N301_AddressInformation = address.Line1;
                    n3.N302_AddressInformation = address.Line2.Truncate(55);

                    // "N4" SERVICE FACILITY LOCATION CITY/STATE/ZIP CODE - loop 2310C
                    TypedSegmentN4 n4 = nm1.AddSegment(new TypedSegmentN4());
                    n4.N401_CityName = address.City;
                    if (address.StateOrProvince.IsInUnitedStates || address.StateOrProvince.IsInCanada)
                    {
                        n4.N402_StateOrProvinceCode = address.StateOrProvince.Abbreviation;
                    }
                    else
                    {
                        n4.N407_CountrySubdivisionCode = address.StateOrProvince.Abbreviation;
                    }

                    if (address.IsValidPostalCode())
                    {
                        n4.N403_PostalCode = address.PostalCode;
                    }
                    else
                    {
                        _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The ServiceLocation {0}, has an invalid zip code.".FormatWith(invoiceReceivable.Invoice.BillingProvider.ServiceLocation.Name), invoiceReceivable.Invoice));
                    }

                    if (!address.StateOrProvince.IsInUnitedStates)
                    {
                        n4.N404_CountryCode = address.StateOrProvince.Country.Abbreviation2Letters;
                    }
                };

                #endregion

                // Add and configure loop 2310C
                TypedLoopNM1 nm12310C = add2310CLoop();
                configure2310C(nm12310C);
            }

            Provider supervisingProvider = invoiceReceivable.IfNotNull(x => x.Invoice.IfNotNull(y => y.InvoiceSupplemental.IfNotNull(z => z.SupervisingProvider)));

            // Add and configure loop 2310D that represents the Supervising Provider Name.
            if (_messageProducerUtilities.CanAdd2310DLoop(supervisingProvider))
            {
                _messageProducerUtilities.Add2310DLoop(clm2300, supervisingProvider, invoiceReceivable);
            }

            // Get other invoice receivables on this invoice with active/matching patient insurances
            IOrderedEnumerable<InvoiceReceivable> otherInvoiceReceivables = invoiceReceivable.Invoice.InvoiceReceivables.Where(i => i.PatientInsurance != null && i.PatientInsurance.OrdinalId != invoiceReceivable.PatientInsurance.OrdinalId && i.PatientInsurance.InsuranceType == invoiceReceivable.PatientInsurance.InsuranceType && i.PatientInsurance.IsActiveForDateTime(i.Invoice.Encounter.StartDateTime)).OrderBy(i => i.PatientInsurance.OrdinalId);

            // Add each of those other invoice receivables to the claim
            foreach (InvoiceReceivable otherInvoiceReceivable in otherInvoiceReceivables)
            {
                // Add and configure loop 2320
                // Include main invoice receivable so we can figure out the adjustments with billing services that are already included on the main invoice receivable
                TypedLoopSBR sbr2320 = _messageProducerUtilities.Add2320Loop(clm2300, invoiceReceivable, otherInvoiceReceivable);
                _messageProducerUtilities.Configure2320Amt(sbr2320, invoiceReceivable, otherInvoiceReceivable);

                #region Configure loop 2320 - "OI" OTHER INSURANCE COVERAGE INFORMATION

                Segment oi2320 = sbr2320.AddSegment("OI");
                oi2320.SetElement(3, otherInvoiceReceivable.Invoice.HasPatientAssignedBenefits ? "Y" : "N");
                oi2320.SetElement(4, "");
                oi2320.SetElement(5, "");
                oi2320.SetElement(6, otherInvoiceReceivable.Invoice.IsReleaseOfInformationNotSigned ? "I" : "Y");

                #endregion

                // Add and configure loop 2330A
                TypedLoopNM1 nm12330A = _messageProducerUtilities.Add2330ALoop(sbr2320, otherInvoiceReceivable);
                _messageProducerUtilities.Configure2330A(nm12330A, otherInvoiceReceivable);

                // Add and configure loop 2330B
                TypedLoopNM1 nm12330B = _messageProducerUtilities.Add2330BLoop(sbr2320, otherInvoiceReceivable);
                _messageProducerUtilities.Configure2330B(nm12330B, otherInvoiceReceivable);
            }

            return clm2300;
        }

        private void Configure2300RefClia(TypedLoopCLM clm2300, InvoiceReceivable invoiceReceivable)
        {
            BillingOrganization billingOrganization = invoiceReceivable.Invoice.BillingProvider.GetClaimFileOverrideProviderOrCurrent(invoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId).BillingOrganization;

            #region Helper REF actions - CLIA CERTIFICATE NUMBER
            Action configureCliaCertificateNumber = () =>
            {
                // "REF" REFERRAL NUMBER - loop 2300
                TypedSegmentREF ref42300 = clm2300.AddSegment(new TypedSegmentREF());
                ref42300.REF01_ReferenceIdQualifier = "X4";
                ref42300.REF02_ReferenceId = billingOrganization.IdentifierCodes.Where(i => i.IdentifierCodeType == IdentifierCodeType.CliaCertNumber).Select(i => i.Value).First();
            };
            #endregion

            //Condition 1
            var identifierCode = billingOrganization.IdentifierCodes.FirstOrDefault(i => i.IdentifierCodeType == IdentifierCodeType.CliaCertNumber);
            if (identifierCode != null)
            {
                //Condition 2
                BillingService[] billingServices = invoiceReceivable.BillingServiceTransactions.Select(i => i.BillingService).ToArray();
                billingServices = billingServices.RemoveAll(billingServices.Where(bs => bs == null).ForEachWithSinglePropertyChangedNotification(bs => _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult(string.Format("Invalid billing services attached to invoice."), invoiceReceivable.Invoice))).ToArray())
                                    .Where(i => ((i.IfNotNull(x => x.EncounterService).IfNotNull(x => x.IsCliaCertificateRequiredOnClaim) && i.EncounterService.IsCliaCertificateRequiredOnClaim) || (i.IfNotNull(x => x.FacilityEncounterService).IfNotNull(x => x.IsCliaCertificateRequiredOnClaim) && i.FacilityEncounterService.IsCliaCertificateRequiredOnClaim)) && i.BillingServiceTransactions.Any(bst => bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued)).ToArray();
                if (billingServices.Any())
                {
                    //Condition 3
                    //var billingserviceTransaction = invoiceReceivable.BillingServiceTransactions.FirstOrDefault(b => b.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.Medicaid || b.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.MedicarePartB);
                    var billingserviceTransaction = invoiceReceivable.BillingServiceTransactions.FirstOrDefault(b => b.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.IsCLIAIncludedOnClaims); //task 22202
                    if (billingserviceTransaction != null) configureCliaCertificateNumber();
                }
            }
        }

        private void AddAndConfigureLoop2400(TypedLoopCLM clm2300, InvoiceReceivable invoiceReceivable, BillingServiceTransaction[] billingServiceTransactions)
        {
            ExternalProvider orderingProvider = invoiceReceivable.IfNotNull(x => x.Invoice.IfNotNull(y => y.InvoiceSupplemental.IfNotNull(z => z.OrderingProvider)));
            int s = 0;
            bool dmeClaim = false;
            foreach (BillingServiceTransaction billingServiceTransaction in billingServiceTransactions.OrderBy(i => i.BillingService.OrdinalId))
            {
                #region Helper Action - configure2400DTP

                Action<Loop, DateTime> configure2400Dtp = (loop2400, serviceDateTime) =>
                                                              {
                                                                  // "DTP" DATE - SERVICE DATE - loop 2400
                                                                  Segment dtp12400 = loop2400.AddSegment("DTP");
                                                                  dtp12400.SetElement(1, "472");
                                                                  dtp12400.SetElement(2, "D8");
                                                                  string dtp3 = serviceDateTime.ToString("yyyyMMdd");
                                                                  dtp12400.SetElement(3, dtp3);
                                                              };

                #endregion

                // Add loop 2400
                Loop lx2400 = clm2300.AddLoop("LX");
                lx2400.SetElement(1, (++s).ToString());

                BillingService billingService = billingServiceTransaction.BillingService;

                // Configure loop 2400
                _messageProducerUtilities.Configure2400Sv1(lx2400, billingService, billingServiceTransaction);
                configure2400Dtp(lx2400, billingService.Invoice.Encounter.StartDateTime);
                _messageProducerUtilities.Configure2400Ref(lx2400, billingService);
                _messageProducerUtilities.Configure2400Nte(lx2400, billingService);

                // Add and configure loop 2410
                Loop lin2410;
                if (_messageProducerUtilities.TryAdd2410Loop(lx2400, billingService, out lin2410))
                {
                    _messageProducerUtilities.Configure2410(lin2410, billingService);
                }

                // Add loop 2420E
                if (billingServiceTransaction.InvoiceReceivable.Invoice.InvoiceType == InvoiceType.Vision)
                {
                    IEnumerable<Insurer> insurers = billingServiceTransaction.InvoiceReceivable.Invoice.Encounter.Patient.PatientInsurances.Where(pi =>
                                                                                                                                                  pi.InsurancePolicy != null && pi.InsurancePolicy.Insurer != null).Select(cl => cl.InsurancePolicy.Insurer);
                    int isValid = insurers.Where(i => i.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.MedicarePartB).Select(i => i.Id).Count();

                    dmeClaim = isValid > 0;
                }

                if (dmeClaim)
                {
                    if (billingServiceTransaction.InvoiceReceivable.Invoice.ReferringExternalProvider == null)
                    {
                        _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult(string.Format("Patient {0} doesn't have Referring Provider.", billingServiceTransaction.InvoiceReceivable.Invoice.Encounter.Patient.GetFormattedName()), invoiceReceivable.Invoice));
                        return;
                    }

                    _messageProducerUtilities.Add2420ELoop(lx2400, billingServiceTransaction.InvoiceReceivable.Invoice.ReferringExternalProvider, invoiceReceivable);
                    clm2300.CLM05._1_FacilityCodeValue = "12";
                }

                // Add loop 2420E for ordering provider
                if (_messageProducerUtilities.CanAdd2420ELoopForOrderingProvider(billingServiceTransaction.InvoiceReceivable, billingService))
                {
                    _messageProducerUtilities.Add2420ELoop(lx2400, orderingProvider, invoiceReceivable);
                }

                // Add and configure loop 2430
                Loop svd2430;
                if (_messageProducerUtilities.TryAdd2430Loop(lx2400, billingServiceTransaction, out svd2430))
                {
                    _messageProducerUtilities.Configure2430(svd2430, lx2400, billingServiceTransaction);
                }
            }
        }

        /// <summary>
        ///     Adds a 2310A loop that represents the Referring Provider Name for the given 2300 claim loop.
        ///     This is used for medical claims, as opposed to facility claims.
        /// </summary>
        /// <param name="clm2300"></param>
        /// <param name="externalProvider"></param>
        /// <param name="invoiceReceivable"></param>
        private void Add2310ALoopMedicalClaim(TypedLoopCLM clm2300, ExternalProvider externalProvider, InvoiceReceivable invoiceReceivable)
        {
            TypedLoopNM1 nm12310A = clm2300.AddLoop(new TypedLoopNM1("DN"));
            nm12310A.NM102_EntityTypeQualifier = EntityTypeQualifier.Person;
            nm12310A.NM103_NameLastOrOrganizationName = externalProvider.LastNameOrEntityName;
            nm12310A.NM104_NameFirst = externalProvider.FirstName.Truncate(35);
            nm12310A.NM105_NameMiddle = externalProvider.MiddleName ?? string.Empty;
            nm12310A.NM107_NameSuffix = externalProvider.Suffix ?? string.Empty;
            nm12310A.NM108_IdCodeQualifier = "XX";

            string npi = externalProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
            if (npi == null)
            {
                _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Referring doctor {0}, has no NPI.".FormatWith(externalProvider.GetFormattedName()), invoiceReceivable.Invoice));
                return;
            }

            if (npi.Length < 2 || npi.Length > 80)
            {
                _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The ExternalProvider {0}, has an invalid NPI.".FormatWith(externalProvider.GetFormattedName()), invoiceReceivable.Invoice));
                return;
            }
            nm12310A.NM109_IdCode = npi;
        }

        /// <summary>
        ///     Adds a 2310A loop that represents the Referring Provider Name for the given 2300 claim loop.
        ///     This is used for medical claims, as opposed to facility claims.
        /// </summary>
        /// <param name="clm2300"></param>
        /// <param name="provider"></param>
        /// <param name="invoiceReceivable"></param>
        private void Add2310ALoopMedicalClaim(TypedLoopCLM clm2300, Provider provider, InvoiceReceivable invoiceReceivable)
        {
            TypedLoopNM1 nm12310A = clm2300.AddLoop(new TypedLoopNM1("DN"));
            nm12310A.NM102_EntityTypeQualifier = EntityTypeQualifier.Person;
            nm12310A.NM103_NameLastOrOrganizationName = provider.User.LastName;
            nm12310A.NM104_NameFirst = provider.User.FirstName.Truncate(35);
            nm12310A.NM105_NameMiddle = provider.User.MiddleName ?? string.Empty;
            nm12310A.NM107_NameSuffix = provider.User.Suffix ?? string.Empty;
            nm12310A.NM108_IdCodeQualifier = "XX";

            string npi = provider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
            if (npi == null)
            {
                _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Provider {0}, has no NPI.".FormatWith(provider.GetNameDescription()), invoiceReceivable.Invoice));
                return;
            }
            if (npi.Length < 2 || npi.Length > 80)
            {
                _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Provider {0}, has an invalid NPI.".FormatWith(provider.GetNameDescription()), invoiceReceivable.Invoice));
                return;
            }
            nm12310A.NM109_IdCode = npi;
        }

        /// <summary>
        ///     Adds a 2310A loop that represents the Referring Provider Name for the given 2300 claim loop.
        ///     This is used for facility claims, as opposed to medical claims.
        /// </summary>
        /// <param name="clm2300"></param>
        /// <param name="invoiceReceivable"></param>
        private void Add2310ALoopFacilityClaim(TypedLoopCLM clm2300, InvoiceReceivable invoiceReceivable)
        {
            TypedLoopNM1 nm1 = clm2300.AddLoop(new TypedLoopNM1("DN"));
            nm1.NM102_EntityTypeQualifier = EntityTypeQualifier.Person;

            User user = invoiceReceivable.Invoice.Encounter.Appointments.OfType<UserAppointment>().Select(ua => ua.User).First();

            nm1.NM103_NameLastOrOrganizationName = user.LastName;
            nm1.NM104_NameFirst = user.FirstName.Truncate(35);
            nm1.NM105_NameMiddle = user.MiddleName ?? string.Empty;
            nm1.NM107_NameSuffix = user.Suffix ?? string.Empty;

            Provider provider = user.Providers.FirstOrDefault();
            if (provider != null)
            {
                string idCode = provider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
                if (idCode == null)
                {
                    _messageProducerUtilities.ValidationResults.Add(new MessageProducerValidationResult("The Provider {0}, has no NPI".FormatWith(provider.GetNameDescription()), invoiceReceivable.Invoice));
                    return;
                }
                if (!idCode.IsNotNullOrEmpty()) return;

                nm1.NM108_IdCodeQualifier = "XX";
                nm1.NM109_IdCode = idCode;
            }
        }
    }
}