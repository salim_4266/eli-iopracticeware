using System;
using System.ComponentModel;

namespace IO.Practiceware.Integration.HL7
{
	public class Constants
	{
        public static string MVEExternalSystemName = "MVE";

		public static string[] HL7Versions = new []
		{
			"2.5",
			"2.4",
			"2.3.1",
			"2.3",
			"2.2"
		};

		public static Type[] HL7MSHSegmentTypeVersions = new[]
		{
			typeof(NHapi.Model.V25.Segment.MSH),
			typeof(NHapi.Model.V24.Segment.MSH),
			typeof(NHapi.Model.V231.Segment.MSH),
			typeof(NHapi.Model.V23.Segment.MSH),
			typeof(NHapi.Model.V22.Segment.MSH)
		};

		public const string DefaultHL7Version = "2.3.1";
		public const string MessageStartDelimiter = "\v";
		public const string MessageEndDelimiter = "\u001c\r";

        public enum ProcessingId
        {
            [Description("P")]
            Production,
            [Description("T")]
            Training,
            [Description("D")]
            Debugging
        }
	}
}
