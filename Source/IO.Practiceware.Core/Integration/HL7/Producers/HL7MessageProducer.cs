using IO.Practiceware.Integration.HL7.Producers;
using IO.Practiceware.Integration.Messaging;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;

[assembly: Component(typeof(HL7MessageProducer.ProduceMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.HL7.Producers
{
    public class HL7MessageProducer
    {
        public class ProduceMessageSubscriber
        {
            public ProduceMessageSubscriber(IMessenger messenger)
            {
                messenger.Subscribe<ProduceMessageRequest>(
                    (s, t, m) => ProduceMessage(m), IsSupported);
            }
        }

        public static bool IsSupported(ProduceMessageRequest m)
        {
            // Check there is a source non-outbound message
            var isSupported = !m.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageType.IsOutbound 
                && m.ExternalSystemMessage.Value.IsNotNullOrEmpty() 
                && m.ProducedMessage == null;

            if (isSupported)
            {
                // Attempt to load it as HL7 message
                try
                {
                    isSupported = LoadMessage(m.ExternalSystemMessage.Value) != null;
                }
                catch
                {
                    isSupported = false;
                }
            }
            return isSupported;
        }

        public static void ProduceMessage(ProduceMessageRequest request)
        {
            request.ProducedMessage = LoadMessage(request.ExternalSystemMessage.Value);
        }

        private static HL7Message LoadMessage(string storedMessage)
        {
            var message = new HL7Message();
            message.Load(storedMessage);

            return message.Message != null ? message : null;
        }
    }
}
