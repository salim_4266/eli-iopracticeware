using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Legacy;
using IO.Practiceware.Services.ExternalSystems;
using Soaf;
using System;
using System.Linq;
using V231MSH = NHapi.Model.V231.Segment.MSH;
using V231PID = NHapi.Model.V231.Segment.PID;
using V23MSH = NHapi.Model.V23.Segment.MSH;
using V23PID = NHapi.Model.V23.Segment.PID;

namespace IO.Practiceware.Integration.HL7.Producers
{
    public class MessageProducerUtilities
    {
        private readonly Lazy<IExternalSystemService> _externalSystemService;
        private readonly Lazy<ILegacyPracticeRepository> _legacyPracticeRepository;

        public MessageProducerUtilities(Func<ILegacyPracticeRepository> legacyPracticeRepository, Func<IExternalSystemService> externalSystemService)
        {
            _externalSystemService = Lazy.For(externalSystemService);
            _legacyPracticeRepository = Lazy.For(legacyPracticeRepository);
        }

        #region V231
        public void ConfigurePidSegment(V231PID segment, Patient patient)
        {
            segment.PatientID.ID.Value = patient.Id.ToString();
            segment.GetPatientIdentifierList(0).ID.Value = patient.Id.ToString();
            segment.SetIDPID.Value = "1";
            segment.GetPatientName(0).FamilyLastName.FamilyName.Value = patient.LastName;
            segment.GetPatientName(0).GivenName.Value = patient.FirstName;
            segment.GetPatientName(0).MiddleInitialOrName.Value = patient.MiddleName;
            if (patient.DateOfBirth.HasValue)
            {
                segment.DateTimeOfBirth.TimeOfAnEvent.Set(patient.DateOfBirth.Value, "yyyyMMdd");
            }
            if (patient.Gender.HasValue)
            {
                segment.Sex.Value = patient.Gender.ToString().Substring(0, 1);
            }

            if (patient.MaritalStatus.HasValue)
            {
                var mapping = _externalSystemService.Value.GetMappings<MaritalStatus>(((int)patient.MaritalStatus.Value).ToString(), Constants.MVEExternalSystemName).FirstOrDefault(i => i.ExternalSystemEntity.Name == "MaritalStatus");
                if (mapping != null)
                {
                    segment.MaritalStatus.Identifier.Value = mapping.ExternalSystemEntityKey;
                }
            }

            string language = null;
            if (patient.LanguageId.HasValue)
            {
                var mapping = _externalSystemService.Value.GetMappings<Language>(patient.LanguageId.Value.ToString(), Constants.MVEExternalSystemName).FirstOrDefault(i => i.ExternalSystemEntity.Name == "Language");
                if (mapping != null)
                {
                    language = mapping.ExternalSystemEntityKey;
                }
            }

            var homeAddress = patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home);
            if (homeAddress != null)
            {
                segment.PrimaryLanguage.Identifier.Value = language;
                var address = segment.GetPatientAddress(0);
                address.StreetAddress.Value = homeAddress.Line1;
                address.OtherDesignation.Value = homeAddress.Line2;
                address.City.Value = homeAddress.City;
                address.StateOrProvince.Value = homeAddress.StateOrProvince.Abbreviation;
                address.ZipOrPostalCode.Value = homeAddress.PostalCode;
            }

            var homePhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home);
            if (homePhone != null)
            {
                segment.GetPhoneNumberHome(0).Get9999999X99999CAnyText.Value = homePhone.ToString().ToNumeric();
            }

            var personalEmailAddress = patient.PatientEmailAddresses.FirstOrDefault(e => e.EmailAddressTypeId == (int)EmailAddressType.Personal);
            if (personalEmailAddress != null)
            {
                segment.GetPhoneNumberHome(0).EmailAddress.Value = personalEmailAddress.Value;
            }

            var businessPhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business);
            if (businessPhone != null)
            {
                segment.GetPhoneNumberBusiness(0).Get9999999X99999CAnyText.Value = businessPhone.ToString().ToNumeric();
            }

            var businessEmailAddress = patient.PatientEmailAddresses.FirstOrDefault(e => e.EmailAddressTypeId == (int)EmailAddressType.Business);
            if (businessEmailAddress != null)
            {
                segment.GetPhoneNumberBusiness(0).EmailAddress.Value = businessEmailAddress.Value;
            }

            if (!patient.SocialSecurityNumber.IsNullOrEmpty())
            {
                segment.SSNNumberPatient.Value = patient.SocialSecurityNumber;
            }
        }

        public void ConfigureMshSegment(V231MSH msh, DateTime timeOfEvent, string externalSystemName, string externalMessageId)
        {
            msh.SendingApplication.NamespaceID.Value = Messaging.Constants.LocalApplicationId;
            msh.ProcessingID.ProcessingID.Value = MessagingConfiguration.Current.HL7MessagingConfiguration.OutboundMessagesProcessingId;
            int? facilityId = _legacyPracticeRepository.Value.EPrescription_Credentials.Select(i => i.AccountID).FirstOrDefault();
            msh.SendingFacility.NamespaceID.Value = (facilityId != null) ? facilityId.ToString() : String.Empty;
            msh.ReceivingApplication.NamespaceID.Value = externalSystemName;
            msh.DateTimeOfMessage.TimeOfAnEvent.Set(timeOfEvent, "yyyyMMddHHmmss");
            msh.MessageControlID.Value = externalMessageId;
        }
        #endregion

        #region V23
        public void ConfigurePidSegment(V23PID segment, Patient patient)
        {
            segment.PatientIDExternalID.ID.Value = patient.Id.ToString();
            segment.GetPatientIDInternalID(0).ID.Value = patient.Id.ToString();
            segment.SetIDPatientID.Value = "1";
            segment.PatientName.FamilyName.Value = patient.LastName;
            segment.PatientName.GivenName.Value = patient.FirstName;
            segment.PatientName.MiddleInitialOrName.Value = patient.MiddleName;
            if (patient.DateOfBirth.HasValue)
            {
                segment.DateOfBirth.TimeOfAnEvent.Set(patient.DateOfBirth.Value, "yyyyMMdd");
            }
            if (patient.Gender.HasValue)
            {
                segment.Sex.Value = patient.Gender.ToString().Substring(0, 1);
            }

            if (patient.MaritalStatus.HasValue)
            {
                var mapping = _externalSystemService.Value.GetMappings<MaritalStatus>(((int)patient.MaritalStatus.Value).ToString(), Constants.MVEExternalSystemName).FirstOrDefault(i => i.ExternalSystemEntity.Name == "MaritalStatus");
                if (mapping != null)
                {
                    segment.GetMaritalStatus(0).Value = mapping.ExternalSystemEntityKey;
                }
            }

            string language = null;
            if (patient.LanguageId.HasValue)
            {
                var mapping = _externalSystemService.Value.GetMappings<Language>(patient.LanguageId.Value.ToString(), Constants.MVEExternalSystemName).FirstOrDefault(i => i.ExternalSystemEntity.Name == "Language");
                if (mapping != null)
                {
                    language = mapping.ExternalSystemEntityKey;
                }
            }

            if (patient.PatientAddresses.Any())
            {
                var homeAddress = patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home);
                if (homeAddress != null)
                {
                    segment.PrimaryLanguage.Identifier.Value = language;
                    var address = segment.GetPatientAddress(0);
                    address.StreetAddress.Value = homeAddress.Line1;
                    address.OtherDesignation.Value = homeAddress.Line2;
                    address.City.Value = homeAddress.City;
                    address.StateOrProvince.Value = homeAddress.StateOrProvince.Abbreviation;
                    address.ZipOrPostalCode.Value = homeAddress.PostalCode;
                }
            }

            if (patient.PatientPhoneNumbers.Any())
            {
                var homePhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home);
                if (homePhone != null)
                {
                    segment.GetPhoneNumberHome(0).Value = homePhone.ToString();
                }
                var businessPhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business);
                if (businessPhone != null)
                {
                    segment.GetPhoneNumberBusiness(0).Value = businessPhone.ToString();
                }
            }
        }

        public void ConfigureMshSegment(V23MSH msh, DateTime timeOfEvent, string externalSystemName, string externalMessageId)
        {
            msh.SendingApplication.NamespaceID.Value = Messaging.Constants.LocalApplicationId;
            msh.ProcessingID.ProcessingID.Value = MessagingConfiguration.Current.HL7MessagingConfiguration.OutboundMessagesProcessingId;
            int? facilityId = _legacyPracticeRepository.Value.EPrescription_Credentials.Select(i => i.AccountID).FirstOrDefault();
            msh.SendingFacility.NamespaceID.Value = (facilityId != null) ? facilityId.ToString() : String.Empty;
            msh.ReceivingApplication.NamespaceID.Value = externalSystemName;
            msh.DateTimeOfMessage.TimeOfAnEvent.Set(timeOfEvent, "yyyyMMddHHmmss");
            msh.MessageControlID.Value = externalMessageId;
        }
        #endregion
    }
}
