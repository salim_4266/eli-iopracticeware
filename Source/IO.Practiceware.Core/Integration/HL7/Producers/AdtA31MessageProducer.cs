﻿using IO.Practiceware.Integration.HL7.Producers;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using NHapi.Model.V23.Message;
using NHapi.Model.V23.Segment;
using Soaf;
using Soaf.ComponentModel;
using System;
using System.Diagnostics;
using System.Linq;

[assembly: Component(typeof(AdtA31MessageProducer.ProduceMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.HL7.Producers
{
    class AdtA31MessageProducer
    {
        public class ProduceMessageSubscriber
        {
            public ProduceMessageSubscriber(IMessenger messenger, Func<AdtA31MessageProducer> producer)
            {
                messenger.Subscribe((IMessenger s, object t, ProduceMessageRequest m) => producer().ProduceMessage(m),
                                                m => m.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.ADT_A31_V23_Outbound);
            }
        }

        private readonly Lazy<IPracticeRepository> _practiceRepository;
        private readonly Lazy<MessageProducerUtilities> _messageProducerUtilities;

        public AdtA31MessageProducer(Func<IPracticeRepository> practiceRepository, Func<MessageProducerUtilities> messageProducerUtilities)
        {
            _practiceRepository = Lazy.For(practiceRepository);
            _messageProducerUtilities = Lazy.For(messageProducerUtilities);
        }

        public void ProduceMessage(ProduceMessageRequest request)
        {
            try
            {
                var externalMessage = request.ExternalSystemMessage;
                if (externalMessage.CreatedBy == "PatientUpdate")
                {
                    int patientId = externalMessage.ExternalSystemMessagePracticeRepositoryEntities.First(e => e.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.Patient).PracticeRepositoryEntityKey.ToInt().GetValueOrDefault();
                    var patient = _practiceRepository.Value.Patients.FirstOrDefault(p => p.Id == patientId);
                    string externalSystemName = externalMessage.ExternalSystemExternalSystemMessageType.ExternalSystem.Name;
                    var internalMessage = CreateAdtA31(patient, externalSystemName, externalMessage.Id.ToString());
                    var message = new HL7Message(internalMessage);
                    request.ProducedMessage = message;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(new Exception("Could not produce message.", ex).ToString());
            }
        }

        public ADT_A31 CreateAdtA31(Patient patient, string externalSystemName, string externalMessageId)
        {
            DateTime timeOfEvent = DateTime.UtcNow;
            var message = new ADT_A31();
            _messageProducerUtilities.Value.ConfigureMshSegment(message.MSH, timeOfEvent, externalSystemName, externalMessageId);
            _messageProducerUtilities.Value.ConfigurePidSegment(message.PID, patient);
            ConfigurePv1Segment(message.PV1);
            ConfigureEvnSegment(message.EVN, timeOfEvent);
            return message;
        }

        private static void ConfigureEvnSegment(EVN evn, DateTime timeOfEvent)
        {
            evn.EventTypeCode.Value = "A31";
            evn.RecordedDateTime.TimeOfAnEvent.Set(timeOfEvent, "yyyyMMddHHmmss");
        }

        private static void ConfigurePv1Segment(PV1 pv1)
        {
            pv1.SetIDPatientVisit.Value = 1.ToString();
            // Using static 'U' (possibly stands for unknown) to match Zeiss' syntax since we do not have a Patient Class value.
            // (Patient class value does not have industry-wide definition.  It is subject to site-specific variations.)
            // (It stands for General type of patient, e.g., Inpatient, Outpatient, Emergency)
            pv1.PatientClass.Value = "U";
        }
    }
}
