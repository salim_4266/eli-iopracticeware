using IO.Practiceware.Integration.HL7.Producers;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Legacy;
using IO.Practiceware.Services.ExternalSystems;
using NHapi.Base.Model;
using NHapi.Model.V231.Message;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using IOIMessage = IO.Practiceware.Integration.Messaging.IMessage;
using IO.Practiceware.Data;
using System.Data;
using Soaf.Data;
//using System.Data.Entity;

[assembly: Component(typeof(DftP03MessageProducer.ProduceMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.HL7.Producers
{
    public class DftP03MessageProducer
    {
        public class ProduceMessageSubscriber
        {
            public ProduceMessageSubscriber(IMessenger messenger, Func<DftP03MessageProducer> producer)
            {
                messenger.Subscribe((IMessenger s, object t, ProduceMessageRequest m) =>
                {
                    IOIMessage message = producer().ProduceMessage(m.ExternalSystemMessage);
                    m.ProducedMessage = (message ?? m.ProducedMessage);
                },
                                    m => m.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.DFT_P03_V231_Outbound);
            }
        }

        private readonly Lazy<ILegacyPracticeRepository> _legacyPracticeRepository;
        private readonly Lazy<IPracticeRepository> _practiceRepository;
        private readonly Lazy<IExternalSystemService> _externalSystemService;
        private readonly Lazy<MessageProducerUtilities> _messageProducerUtilities;
        private DataTable patientClinicalsIcd10;

        public DftP03MessageProducer(Func<ILegacyPracticeRepository> legacyPracticeRepository, 
            Func<IPracticeRepository> practiceRepository, 
            Func<IExternalSystemService> externalSystemService,
            Func<MessageProducerUtilities> messageProducerUtilities)
        {
            _legacyPracticeRepository = Lazy.For(legacyPracticeRepository);
            _practiceRepository = Lazy.For(practiceRepository);
            _externalSystemService = Lazy.For(externalSystemService);
            _messageProducerUtilities = Lazy.For(messageProducerUtilities);
        }

        public DFT_P03 CreateDftP03(int appointmentId, string externalSystemName, string externalMessageId)
        {
            var message = new DFT_P03();
            var now = DateTime.UtcNow;

            var appointment = _practiceRepository.Value.Appointments.OfType<UserAppointment>().
                Include(i => i.Encounter.Patient).
                Include(i => i.Encounter.ClinicalInvoiceProviders.Select(ep => ep.Provider.User)).
                Include(i => i.Encounter.ServiceLocation).
                Include(i => i.Encounter.ClinicalServices.Select(cs => cs.EncounterService)).
                FirstOrDefault(a => a.Id == appointmentId);
            if (appointment == null)
            {
                throw new ArgumentException("Appointment not found.");
            }
            ExternalSystemEntityMapping appointmentMapping = _externalSystemService.Value.GetMappings(appointment, externalSystemName).FirstOrDefault();
            if (appointmentMapping == null)
            {
                Trace.TraceWarning(String.Format("Appointment with Id {0} is not mapped, so a DFTP03 message cannot be created for {1}.", appointment.Id, externalSystemName));
                return null;
            }

            ExternalSystem externalSystem = appointmentMapping.ExternalSystemEntity.ExternalSystem;
            if (externalSystem == null)
            {
                throw new ArgumentException("External system not found.");
            }

            _messageProducerUtilities.Value.ConfigureMshSegment(message.MSH, now, externalSystem.Name, externalMessageId);
            message.EVN.EventTypeCode.Value = "P03";
            message.EVN.RecordedDateTime.TimeOfAnEvent.Set(now, "yyyyMMddHHmmss");

            var patientMapping = _externalSystemService.Value.GetMappings(appointment.Encounter.Patient, externalSystem.Name).FirstOrDefault();
            if (patientMapping == null)
            {
                Trace.TraceWarning("Patient mapping for Patient Id {0} not found for {1}.".FormatWith(appointment.Encounter.Patient.Id, externalSystem.Name));
                return null;
            }

            var userId = appointment.Encounter.ClinicalInvoiceProviders.Select(i => i.Provider.UserId.ToString()).FirstOrDefault();
            if (userId == null)
            {
                Trace.TraceWarning(String.Format("No clinical invoice provider found for appointment {0}.", appointment.Id));
                return null;
            }

            var userMapping = _externalSystemService.Value.GetMappings<User>(userId, externalSystemName).FirstOrDefault();
            if (userMapping == null)
            {
                Trace.TraceWarning(String.Format("User with Id {0} is not mapped for system {1}.", appointment.Encounter.ClinicalInvoiceProviders.First().Provider.User.Id, externalSystem.Name));
                return null;
            }

            var user = _practiceRepository.Value.Users.FirstOrDefault(i => i.Id == userMapping.PracticeRepositoryEntityKey.ToInt().Value).
                                                 EnsureNotDefault("Could not find a user for the specified provider {0}.".FormatWith(userMapping.PracticeRepositoryEntityKey));

            var visitMapping = _externalSystemService.Value.GetMappings<Encounter>(appointment.EncounterId.ToString(), externalSystemName).FirstOrDefault();

            var notes = _legacyPracticeRepository.Value.PatientNotes.Where(pn => pn.AppointmentId.HasValue &&
                                                                           pn.AppointmentId.Value == appointment.Id &&
                                                                           pn.PatientId.HasValue &&
                                                                           pn.PatientId.Value == appointment.Encounter.Patient.Id &&
                                                                           pn.NoteType == "B" &&
                                                                           pn.NoteClaimOn == "T").ToArray();

            var locationMapping = _externalSystemService.Value.GetMappings(appointment.Encounter.ServiceLocation, externalSystem.Name).FirstOrDefault();

            var patientClinicals = _legacyPracticeRepository.Value.PatientClinicals.
                Where(pc => pc.AppointmentId.Value == appointment.Id &&
                            pc.PatientId.Value == appointment.Encounter.Patient.Id &&
                            pc.Status == PatientClinicalStatus.Active.GetDescription() &&
                            new[] { PatientClinicalType.Billing.GetDescription(), PatientClinicalType.Diagnosis.GetDescription() }.Contains(pc.ClinicalType))
                .OrderBy(pc => pc.Symptom).ToArray();


            string icd10DiagQuery = "select FindingDetailIcd10,Symptom from dbo.patientClinicalIcd10 where AppointmentId = " + appointment.Id + " and patientId = " + appointment.Encounter.Patient.Id + " and status = '" + PatientClinicalStatus.Active.GetDescription() + "' and clinicalType in('"+PatientClinicalType.Billing.GetDescription()+"','"+PatientClinicalType.Diagnosis.GetDescription()+"' ) order by symptom";
             
             using ( var dbConnection = DbConnectionFactory.PracticeRepository)
             {
                 patientClinicalsIcd10 =  dbConnection.Execute<DataTable>(icd10DiagQuery);
             }
            
            var receivables = _legacyPracticeRepository.Value.PatientReceivables.Where(pr => pr.AppointmentId.Value == appointment.Id && pr.PatientId.Value == appointment.Encounter.Patient.Id);
            var receivable = receivables.FirstOrDefault();

            PatientReceivableService[] receivableServices = { };
            if (receivable != null)
            {
                receivableServices = _legacyPracticeRepository.Value.PatientReceivableServices.Where(prs => prs.Invoice == receivable.Invoice).ToArray();
            }

            _messageProducerUtilities.Value.ConfigurePidSegment(message.PID, appointment.Encounter.Patient);
            message.PID.PatientID.ID.Value = patientMapping.ExternalSystemEntityKey;
            message.PID.GetPatientIdentifierList(0).ID.Value = patientMapping.ExternalSystemEntityKey;

            message.PV1.SetIDPV1.Value = 1.ToString();
            message.PV1.GetAttendingDoctor(0).IDNumber.Value = userMapping.ExternalSystemEntityKey;
            message.PV1.GetAttendingDoctor(0).FamilyLastName.FamilyName.Value = user.LastName;
            if (visitMapping != null)
            {
                message.PV1.VisitNumber.ID.Value = visitMapping.ExternalSystemEntityKey;
                message.PV1.AlternateVisitID.ID.Value = visitMapping.ExternalSystemEntityKey;
            }

            int ft1Repetition = 0;
            foreach (var receivableService in receivableServices)
            {
                var financial = message.GetFINANCIAL(ft1Repetition);

                var ft1 = financial.FT1;
                ft1.SetIDFT1.Value = (ft1Repetition + 1).ToString();
                ft1.TransactionID.Value = receivableService.ItemId.ToString();
                ft1.TransactionDate.TimeOfAnEvent.SetShortDate(appointment.DateTime);
                ft1.TransactionType.Value = "CG";
                ft1.TransactionQuantity.Value = receivableService.Quantity.GetValueOrDefault().ToString().Substring(0, Math.Min(4, receivableService.Quantity.GetValueOrDefault().ToString().Length));

                if (locationMapping != null)
                {
                    ft1.AssignedPatientLocation.PointOfCare.Value = locationMapping.ExternalSystemEntityKey;
                }

                var diagnosisCodes = new List<String>();
                var icd = _practiceRepository.Value.BillingServiceBillingDiagnosis.FirstOrDefault(i => i.BillingServiceId == receivableService.ItemId);
                if (!icd.IsIcd10)
                {
                    foreach (var patientClinical in patientClinicals)
                    {
                        if (receivableService.LinkedDiag.IsNullOrEmpty() || receivableService.LinkedDiag.Contains(patientClinical.Symptom))
                        {
                            var diagnosisCodeParts = patientClinical.FindingDetail.Split('.');
                            var diagnosisCodesValue = diagnosisCodeParts[0];
                            if (diagnosisCodeParts.Length > 1)
                            {
                                diagnosisCodesValue += "." + diagnosisCodeParts[1];
                            }
                            diagnosisCodes.Add(diagnosisCodesValue);
                        }
                    }
                }
                if (icd.IsIcd10)
                {
                     foreach (DataRow  patientClinicalIcd10 in patientClinicalsIcd10.Rows)
                     {
                         if (receivableService.LinkedDiag.IsNullOrEmpty() || receivableService.LinkedDiag.Contains(patientClinicalIcd10["Symptom"].ToString()))
                         {
                             var diagnosisCodeParts = patientClinicalIcd10["FindingDetailIcd10"].ToString();
                             diagnosisCodes.Add(diagnosisCodeParts);
                         }
                     }
                }
       
                for (int i = 0; i <= Math.Min(3, diagnosisCodes.Count - 1); i++)
                {
                    ft1.GetDiagnosisCodeFT1(i).Identifier.Value = diagnosisCodes[i];
                }

                ft1.GetPerformedByCode(0).IDNumber.Value = userMapping.ExternalSystemEntityKey;
                ft1.GetPerformedByCode(0).FamilyLastName.FamilyName.Value = user.LastName;

                ft1.ProcedureCode.Identifier.Value = receivableService.Service.Substring(0, Math.Min(5, receivableService.Service.Length));
                ft1.TransactionCode.Identifier.Value = ft1.ProcedureCode.Identifier.Value;
                var modifierIndex = 0;
                if (receivableService.Service.Length > 5)
                {
                    ft1.GetProcedureCodeModifier(modifierIndex).Identifier.Value = receivableService.Service.Substring(5);
                    modifierIndex += 1;
                }
                var modifiersText = receivableService.Modifier;
                while (modifiersText.Length >= 2)
                {
                    var modifier = modifiersText.Substring(0, 2);
                    ft1.GetProcedureCodeModifier(modifierIndex).Identifier.Value = modifier;
                    modifierIndex += 1;
                    modifiersText = modifiersText.Substring(2);
                }

                var service = receivableService.Service;
                var note = notes.FirstOrDefault(n => n.NoteSystem.EndsWith("C{0}".FormatWith(service)));
                if (note != null)
                {
                    var zcc = financial.GetStructure(financial.addNonstandardSegment("ZCC")).CastTo<GenericSegment>();
                    zcc.SetFieldPrimitive(1, 1.ToString());
                    zcc.SetFieldPrimitive(3, note.Note1);
                }

                var encounterService =
                    appointment.Encounter.ClinicalServices.Where(i => i.EncounterService.Code == service).Select(
                        i => i.EncounterService).FirstOrDefault();
                if (encounterService == null)
                {
                    Trace.TraceWarning(String.Format("No Encounter Service found for appointment {0}, service {1}. Encounter contains encounter services: {2}.", appointment.Id, service, appointment.Encounter.ClinicalServices.Select(cs => cs.EncounterServiceId).Join(",")));
                    return null;
                }

                var financialProcedure = financial.GetFINANCIAL_PROCEDURE();
                var pr1 = financialProcedure.PR1;
                pr1.SetIDPR1.Value = (ft1Repetition + 1).ToString();
                pr1.ProcedureCode.Identifier.Value = encounterService.Code;
                pr1.ProcedureCode.Text.Value = encounterService.Description;
                pr1.ProcedureCode.NameOfCodingSystem.Value = "C4";

                ft1Repetition += 1;
            }

            return message;
        }

        public IOIMessage ProduceMessage(ExternalSystemMessage externalSystemMessage)
        {
            int appointmentId = externalSystemMessage.ExternalSystemMessagePracticeRepositoryEntities.First(e => e.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.Appointment).PracticeRepositoryEntityKey.ToInt().GetValueOrDefault();
            DFT_P03 message = CreateDftP03(appointmentId, externalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystem.Name, externalSystemMessage.Id.ToString("N"));
            return message == null ? null : new HL7Message(message);
        }
    }
}
