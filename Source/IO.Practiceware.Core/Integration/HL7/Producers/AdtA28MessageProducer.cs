﻿using IO.Practiceware.Integration.HL7.Producers;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Services.ExternalSystems;
using NHapi.Model.V23.Message;
using NHapi.Model.V23.Segment;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Linq;

[assembly: Component(typeof(AdtA28MessageProducer.ProduceMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.HL7.Producers
{
    public class AdtA28MessageProducer
    {
        public class ProduceMessageSubscriber
        {
            public ProduceMessageSubscriber(IMessenger messenger, Func<AdtA28MessageProducer> producer)
            {
                messenger.Subscribe((IMessenger s, object t, ProduceMessageRequest m) => producer().ProduceMessage(m),
                                                m => m.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.ADT_A28_V23_Outbound);
            }
        }

        private readonly Lazy<IPracticeRepository> _practiceRepository;
        private readonly Lazy<MessageProducerUtilities> _messageProducerUtilities;
        private readonly Lazy<IExternalSystemService> _externalSystemService;

        public AdtA28MessageProducer(
            Func<IPracticeRepository> practiceRepository, 
            Func<MessageProducerUtilities> messageProducerUtilities, 
            Func<IExternalSystemService> externalSystemService)
        {
            _practiceRepository = Lazy.For(practiceRepository);
            _messageProducerUtilities = Lazy.For(messageProducerUtilities);
            _externalSystemService = Lazy.For(externalSystemService);
        }

        public void ProduceMessage(ProduceMessageRequest request)
        {
            Patient patient = null;

            var externalMessage = request.ExternalSystemMessage;
            var esmpre = _externalSystemService.Value.GetExternalSystemMessagePracticeRepositoryEntity(externalMessage.Id, PracticeRepositoryEntityId.Patient);
            if (esmpre != null)
            {
                int patientId = esmpre.PracticeRepositoryEntityKey.ToInt().GetValueOrDefault();
                patient = _practiceRepository.Value.Patients.FirstOrDefault(p => p.Id == patientId).EnsureNotDefault("Could not find patient {0}.".FormatWith(patientId));
            }
            else
            {
                esmpre = _externalSystemService.Value.GetExternalSystemMessagePracticeRepositoryEntity(externalMessage.Id, PracticeRepositoryEntityId.Appointment);
                if (esmpre != null)
                {
                    int appointmentId = esmpre.PracticeRepositoryEntityKey.ToInt().GetValueOrDefault();
                    patient = _practiceRepository.Value.Appointments.Include(i => i.Encounter.Patient).FirstOrDefault(a => a.Id == appointmentId)
                        .EnsureNotDefault("Could not find appointment {0}.".FormatWith(appointmentId))
                        .Encounter.Patient;
                }
            }

            string externalSystemName = externalMessage.ExternalSystemExternalSystemMessageType.ExternalSystem.Name;
            var internalMessage = CreateAdtA28(patient, externalSystemName, externalMessage.Id.ToString("N"));
            var message = new HL7Message(internalMessage);
            request.ProducedMessage = message;

        }

        public ADT_A28 CreateAdtA28(Patient patient, string externalSystemName, string externalMessageId)
        {
            DateTime timeOfEvent = DateTime.UtcNow;
            var message = new ADT_A28();

            _messageProducerUtilities.Value.ConfigureMshSegment(message.MSH, timeOfEvent, externalSystemName, externalMessageId);
            _messageProducerUtilities.Value.ConfigurePidSegment(message.PID, patient);
            ConfigureEvnSegment(message.EVN, timeOfEvent);
            ConfigurePv1Segment(message.PV1);

            return message;
        }

        private static void ConfigureEvnSegment(EVN evn, DateTime timeOfEvent)
        {
            evn.EventTypeCode.Value = "A28";
            evn.RecordedDateTime.TimeOfAnEvent.Set(timeOfEvent, "yyyyMMddHHmmss");
        }

        private static void ConfigurePv1Segment(PV1 pv1)
        {
            pv1.SetIDPatientVisit.Value = 1.ToString();
            // Using static 'U' (possibly stands for unknown) to match Zeiss' syntax since we do not have a Patient Class value.
            // (Patient class value does not have industry-wide definition.  It is subject to site-specific variations.)
            // (It stands for General type of patient, e.g., Inpatient, Outpatient, Emergency)
            pv1.PatientClass.Value = "U";
        }
    }
}
