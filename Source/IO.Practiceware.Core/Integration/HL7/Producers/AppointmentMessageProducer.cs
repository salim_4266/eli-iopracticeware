﻿using System;
using System.Linq;
using IO.Practiceware.Integration.HL7.Producers;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using NHapi.Model.V23.Message;
using NHapi.Model.V23.Segment;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Linq;
using NHapiIMessage = NHapi.Base.Model.IMessage;

[assembly: Component(typeof(AppointmentMessageProducer.ProduceMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.HL7.Producers
{
    public class AppointmentMessageProducer
    {
        public class ProduceMessageSubscriber
        {
            public ProduceMessageSubscriber(IMessenger messenger, Func<AppointmentMessageProducer> producer)
            {
                messenger.Subscribe((IMessenger s, object t, ProduceMessageRequest m) => producer().ProduceMessage(m), m => m.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.SIU_S12_V23_Outbound
                                                                                                                 || m.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.SIU_S14_V23_Outbound
                                                                                                                 || m.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.SIU_S15_V23_Outbound);
            }
        }

        private readonly Lazy<IPracticeRepository> _practiceRepository;
        private readonly Lazy<MessageProducerUtilities> _messageProducerUtilities;

        public AppointmentMessageProducer(Func<IPracticeRepository> practiceRepository, 
            Func<MessageProducerUtilities> messageProducerUtilities)
        {
            _practiceRepository = Lazy.For(practiceRepository);
            _messageProducerUtilities = Lazy.For(messageProducerUtilities);
        }

        public void ProduceMessage(ProduceMessageRequest request)
        {
            var externalMessage = request.ExternalSystemMessage;

            int appointmentId = externalMessage.ExternalSystemMessagePracticeRepositoryEntities.First(e => e.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.Appointment).PracticeRepositoryEntityKey.ToInt().GetValueOrDefault();

            var appointment = _practiceRepository.Value.Appointments.OfType<UserAppointment>().Include(i => i.AppointmentType).Include(i => i.Encounter.Patient).Include(i => i.Encounter.Patient.Language)
                .Include(i => i.Encounter.Patient.PatientAddresses).Include(i => i.Encounter.Patient.PatientAddresses.Select(j => j.StateOrProvince))
                .Include(i => i.Encounter.Patient.PatientPhoneNumbers).Include(i => i.Encounter.Patient.ExternalProviders)
                .Include(i => i.Encounter.Patient.ExternalProviders.Select(j => j.ExternalProvider)).Include(i => i.Encounter.ServiceLocation)
                .Include(i => i.User).Include(i => i.Encounter.EncounterReasonForVisits).FirstOrDefault(a => a.Id == appointmentId);

            if (appointment == null)
            {
                throw new ArgumentException("Appointment not found.");
            }

            string externalSystemName = externalMessage.ExternalSystemExternalSystemMessageType.ExternalSystem.Name;
            ExternalSystemExternalSystemMessageType externalSystemMessageType = externalMessage.ExternalSystemExternalSystemMessageType;

            var siumessage = CreateSiuMessage(appointment, externalSystemName, externalSystemMessageType.ExternalSystemMessageTypeId, externalMessage.Id.ToString("N"));
            var message = new HL7Message(siumessage);
            request.ProducedMessage = message;
        }

        public NHapiIMessage CreateSiuMessage(UserAppointment appointment, string externalSystemName, int messageTypeId, string externalSystemMessageId)
        {
            if (messageTypeId == (int)ExternalSystemMessageTypeId.SIU_S12_V23_Outbound)
            {
                return CreateMessage(new SIU_S12(), appointment, DateTime.UtcNow, externalSystemName, externalSystemMessageId);
            }
            if (messageTypeId == (int)ExternalSystemMessageTypeId.SIU_S14_V23_Outbound)
            {
                return CreateMessage(new SIU_S14(), appointment, DateTime.UtcNow, externalSystemName, externalSystemMessageId);
            }
            if (messageTypeId == (int)ExternalSystemMessageTypeId.SIU_S15_V23_Outbound)
            {
                return CreateMessage(new SIU_S15(), appointment, DateTime.UtcNow, externalSystemName, externalSystemMessageId);
            }
            return null;
        }

       
        private SIU_S12 CreateMessage(SIU_S12 message, UserAppointment appointment, DateTime timeOfEvent, string externalSystemName, string externalSystemMessageId)
        {
            //Create Header
            _messageProducerUtilities.Value.ConfigureMshSegment(message.MSH, timeOfEvent, externalSystemName, externalSystemMessageId);

            CreateMessageComponents(message.SCH, message.GetPATIENT().PID, message.GetPATIENT().PV1, message.GetRESOURCES().GetLOCATION_RESOURCE().AIL, message.GetRESOURCES().RGS, appointment);

            message.SCH.FillerStatusCode.Identifier.Value = "Booked"; //SCH-25

            return message;
        }

        private SIU_S14 CreateMessage(SIU_S14 message, UserAppointment appointment, DateTime timeOfEvent, string externalSystemName, string externalSystemMessageId)
        {
            //Create Header
            _messageProducerUtilities.Value.ConfigureMshSegment(message.MSH, timeOfEvent, externalSystemName, externalSystemMessageId);

            CreateMessageComponents(message.SCH, message.GetPATIENT().PID, message.GetPATIENT().PV1, message.GetRESOURCES().GetLOCATION_RESOURCE().AIL, message.GetRESOURCES().RGS, appointment);

            //SCH-25
            message.SCH.FillerStatusCode.Identifier.Value = appointment.Encounter.EncounterStatus == EncounterStatus.Checkout ? "CheckOut" : "CheckIn";

            return message;
        }

        private SIU_S15 CreateMessage(SIU_S15 message, UserAppointment appointment, DateTime timeOfEvent, string externalSystemName, string externalSystemMessageId)
        {
            //Create Header
            _messageProducerUtilities.Value.ConfigureMshSegment(message.MSH, timeOfEvent, externalSystemName, externalSystemMessageId);

            CreateMessageComponents(message.SCH, message.GetPATIENT().PID, message.GetPATIENT().PV1, message.GetRESOURCES().GetLOCATION_RESOURCE().AIL,message.GetRESOURCES().RGS, appointment);

            message.SCH.FillerStatusCode.Identifier.Value = "Cancel"; //SCH-25

            return message;
        }

        private void CreateMessageComponents(SCH sch, PID pid, PV1 pv1, AIL ail,RGS rgs, UserAppointment appointment)
        {
            Patient patient = appointment.Encounter.Patient;

            //Sch segment
            sch.PlacerAppointmentID.EntityIdentifier.Value = appointment.Id.ToString(); //SCH-1
            sch.FillerAppointmentID.EntityIdentifier.Value = appointment.Id.ToString(); //SCH-2

            var encounterReasonForVisit = appointment.Encounter.EncounterReasonForVisits.FirstOrDefault();
            if (encounterReasonForVisit != null) sch.EventReason.Identifier.Value = encounterReasonForVisit.ReasonForVisit.Name; //SCH-6

            sch.AppointmentType.Identifier.Value = appointment.AppointmentType.Name;//SCH-8
            sch.GetAppointmentTimingQuantity(0).StartDateTime.TimeOfAnEvent.SetLongDate(appointment.DateTime); //SCH-11

            //Create Pid segment.
            _messageProducerUtilities.Value.ConfigurePidSegment(pid, patient);

            //PV1 segment 
            pv1.SetIDPatientVisit.Value = "1"; //PV1-1
            if (appointment.Encounter.Patient.BillingOrganization != null)
            {
                pv1.AssignedPatientLocation.Facility.NamespaceID.Value = appointment.Encounter.Patient.BillingOrganization.Id.ToString(); //PV1-3
            }

            //Attending Doctor PV1-7
            var attendingDoctor = appointment.User;
            if (attendingDoctor != null)
            {

                pv1.AttendingDoctor.IDNumber.Value = attendingDoctor.Id.ToString();  //PV1-7-0
                pv1.AttendingDoctor.FamilyName.Value = attendingDoctor.LastName; //PV1-7-1
                pv1.AttendingDoctor.GivenName.Value = attendingDoctor.FirstName; //PV1-7-2
                pv1.AttendingDoctor.MiddleInitialOrName.Value = attendingDoctor.MiddleName;  //PV1-7-3
                pv1.AttendingDoctor.SuffixEgJRorIII.Value = attendingDoctor.Suffix; //PV1-7-4
                pv1.AttendingDoctor.PrefixEgDR.Value = attendingDoctor.Prefix;//PV1-7-5
                pv1.AttendingDoctor.DegreeEgMD.Value = attendingDoctor.Honorific; //PV-7-6
            }

            //Referring Doctor PV1-8
            var externalProvider = patient.ExternalProviders.FirstOrDefault(i => i.IsReferringPhysician);
            if (externalProvider != null && externalProvider.ExternalProvider != null)
            {
                var referringProvider = externalProvider.ExternalProvider;
                pv1.ReferringDoctor.IDNumber.Value = referringProvider.Id.ToString(); //PV1-8-0
                pv1.ReferringDoctor.FamilyName.Value = referringProvider.LastNameOrEntityName;  //PV1-8-1
                pv1.ReferringDoctor.GivenName.Value = referringProvider.FirstName;  //PV1-8-2
                pv1.ReferringDoctor.MiddleInitialOrName.Value = referringProvider.MiddleName;   //PV1-8-3
                pv1.ReferringDoctor.SuffixEgJRorIII.Value = referringProvider.Suffix; //PV1-8-4
                pv1.ReferringDoctor.PrefixEgDR.Value = referringProvider.Prefix;    //PV1-8-5
                pv1.ReferringDoctor.DegreeEgMD.Value = referringProvider.Honorific; //PV1-8-6
            }
            if ((int)appointment.Encounter.EncounterStatus >= (int)EncounterStatus.Questions)
            {
                pv1.VisitNumber.ID.Value = appointment.Id.ToString(); //PV1-19
            }

            //AIL
            if (appointment.Encounter.ServiceLocation != null)
            {
                ail.SetIDAIL.Value = "1";
                ail.LocationResourceID.PointOfCare.Value = appointment.Encounter.ServiceLocation.Id.ToString(); //AIL-3
                ail.LocationResourceID.Bed.Value = appointment.Encounter.ServiceLocation.ShortName; //AIL-3-2
            }
            //RGS
            rgs.SetIDRGS.Value = "1"; //RGS-1
        }

    }
}
