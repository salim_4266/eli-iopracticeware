using IO.Practiceware.Integration.HL7.Producers;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Legacy;
using IO.Practiceware.Services.ExternalSystems;
using NHapi.Base.Model;
using NHapi.Model.V231.Message;
using NHapi.Model.V231.Segment;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Diagnostics;
using System.Linq;
using IMessage = IO.Practiceware.Integration.Messaging.IMessage;

[assembly: Component(typeof(OrmO01MessageProducer.ProduceMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.HL7.Producers
{
    public class OrmO01MessageProducer
    {
        public class ProduceMessageSubscriber
        {
            public ProduceMessageSubscriber(IMessenger messenger, Func<OrmO01MessageProducer> producer)
            {
                messenger.Subscribe<ProduceMessageRequest>(
                    (s, t, m) => m.ProducedMessage = producer().ProduceMessage(m.ExternalSystemMessage) ?? m.ProducedMessage,
                    m => m.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.ORM_O01_V231_Outbound);
            }
        }

        private readonly Lazy<ILegacyPracticeRepository> _legacyPracticeRepository;
        private readonly Lazy<IPracticeRepository> _practiceRepository;
        private readonly MessageProducerUtilities _messageProducerUtilities;
        private readonly Lazy<IExternalSystemService> _externalSystemService;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;

        public OrmO01MessageProducer(Func<ILegacyPracticeRepository> legacyPracticeRepository, 
            Func<IPracticeRepository> practiceRepository, 
            MessageProducerUtilities messageProducerUtilities, 
            IUnitOfWorkProvider unitOfWorkProvider, 
            Func<IExternalSystemService> externalSystemService)
        {
            _legacyPracticeRepository = Lazy.For(legacyPracticeRepository);
            _practiceRepository = Lazy.For(practiceRepository);
            _externalSystemService = Lazy.For(externalSystemService);
            _messageProducerUtilities = messageProducerUtilities;
            _unitOfWorkProvider = unitOfWorkProvider;

        }

        public ORM_O01 CreateMessage(int appointmentId, string externalSystemName, string externalMessageId)
        {
            using (_unitOfWorkProvider.Create())
            {
                var now = DateTime.UtcNow;

                var message = new ORM_O01();

                var appointment = _practiceRepository.Value.Appointments.Include(i => i.Encounter, i => i.Encounter.ClinicalInvoiceProviders, i => i.Encounter.ClinicalInvoiceProviders.Select(p => p.Provider.User)).Include(i => i.Encounter.Patient, i => i.Encounter.ServiceLocation).FirstOrDefault(a => a.Id == appointmentId);
                if ((appointment == null)) { throw new ArgumentException("Appointment not found."); }
                message.MSH.SendingFacility.UniversalID.Value = appointment.Encounter.ClinicalInvoiceProviders.First().Provider.BillingOrganizationId.ToString();
                
                _messageProducerUtilities.ConfigureMshSegment(message.MSH, now, externalSystemName, externalMessageId);

                var patient = _practiceRepository.Value.Patients.Include(p => p.PatientPhoneNumbers, p => p.PatientEmailAddresses, p => p.PatientAddresses.Select(pa => pa.StateOrProvince)).FirstOrDefault(pd => pd.Id == appointment.Encounter.PatientId);
                if ((patient == null)) { throw (new ArgumentException(string.Format("Patient with Id {0} not found.", appointment.Encounter.PatientId))); }

                _messageProducerUtilities.ConfigurePidSegment(message.PATIENT.PID, patient);

                AddVisionPlans(message, appointment);

                ConfigureOrc(appointment, message.GetORDER(0));

                AddPrescriptions(message, appointment);

                return message;
            }
        }

        private void AddPrescriptions(ORM_O01 message, Model.Appointment appointment)
        {
            var prescriptions = _practiceRepository.Value.EncounterLensesPrescriptions.Where(i => i.Encounter.Id == appointment.Encounter.Id).ToArray();

            var prescriptionIndex = 0;
            foreach (var prescription in prescriptions)
            {
                var order = message.GetORDER(prescriptionIndex);
                ConfigureOrc(appointment, order);
                order.ORDER_DETAIL.RQD.RequisitionLineNumber.Value = (prescriptionIndex + 1).ToString();
                ConfigureRqd(order.ORDER_DETAIL.RQD, prescription);

                if (prescription.LensesPrescription is SpectaclePrescription)
                {
                    ConfigurePrescription(order.ORDER_DETAIL, (SpectaclePrescription)prescription.LensesPrescription);
                }
                else if (prescription.LensesPrescription is ContactLensesPrescription)
                {
                    ConfigurePrescription(order.ORDER_DETAIL, (ContactLensesPrescription)prescription.LensesPrescription);
                }

                prescriptionIndex += 1;
            }
        }

        private static void ConfigurePrescription(NHapi.Model.V231.Group.ORM_O01_ORDER_DETAIL order, SpectaclePrescription prescription)
        {
            order.addNonstandardSegment("ZOD");
            var odSegment = (GenericSegment)order.GetStructure("ZOD");
            odSegment.SetFieldPrimitive(1, "1");

            ConfigurePrescription(odSegment, prescription.ODPrescription);

            order.addNonstandardSegment("ZOS");
            var osSegment = (GenericSegment)order.GetStructure("ZOS");
            osSegment.SetFieldPrimitive(1, "1");
            ConfigurePrescription(osSegment, prescription.OSPrescription);

            if (!string.IsNullOrWhiteSpace(prescription.Comment))
            {
                order.GetNTE(0).SetIDNTE.Value = "1";
                order.GetNTE(0).GetComment(0).Value = prescription.Comment;
            }
        }

        private static void ConfigurePrescription(GenericSegment segment, SpectacleEyePrescription prescription)
        {
            segment.SetFieldPrimitive(2, prescription.Sphere);
            segment.SetFieldPrimitive(3, prescription.Cylinder);
            segment.SetFieldPrimitive(4, prescription.Axis);
            segment.SetFieldPrimitive(5, prescription.Reading);
            segment.SetFieldPrimitive(6, prescription.Intermediate);
            segment.SetFieldPrimitive(7, prescription.PrismOfAngle1);
            segment.SetFieldPrimitive(8, prescription.PrismOfAngle2);
            segment.SetFieldPrimitive(9, prescription.VertexDistance);
        }

        private void ConfigurePrescription(NHapi.Model.V231.Group.ORM_O01_ORDER_DETAIL order, ContactLensesPrescription prescription)
        {
            order.addNonstandardSegment("ZOD");
            var odSegment = (GenericSegment)order.GetStructure("ZOD");
            odSegment.SetFieldPrimitive(1, "1");
            ConfigurePrescription(odSegment, prescription.ODPrescription);

            order.addNonstandardSegment("ZOS");
            var osSegment = (GenericSegment)order.GetStructure("ZOS");
            osSegment.SetFieldPrimitive(1, "1");
            ConfigurePrescription(osSegment, prescription.OSPrescription);
        }

        private void ConfigurePrescription(GenericSegment segment, ContactLensEyePrescription prescription)
        {
            var dominant = string.Empty;
            if (prescription.Dominant.HasValue)
            {
                dominant = prescription.Dominant.Value ? "D" : "N";
            }

            var inventory = _legacyPracticeRepository.Value.CLInventories.FirstOrDefault(i => i.InventoryId == prescription.InventoryId);
            if (inventory != null)
            {
                var contactLensName = inventory.Name;
                segment.SetFieldPrimitive(10, contactLensName);
            }

            segment.SetFieldPrimitive(2, prescription.Sphere);
            segment.SetFieldPrimitive(3, prescription.Cylinder);
            segment.SetFieldPrimitive(4, prescription.Axis);
            segment.SetFieldPrimitive(5, prescription.Reading);
            segment.SetFieldPrimitive(6, prescription.BaseCurve);
            segment.SetFieldPrimitive(7, prescription.Diameter);
            segment.SetFieldPrimitive(8, prescription.PeripheralCurve);
            segment.SetFieldPrimitive(9, dominant);
            segment.SetFieldPrimitive(11, prescription.Comment);
        }

        private void ConfigureRqd(RQD rqd, EncounterLensesPrescription prescription)
        {
            rqd.ItemCodeInternal.Identifier.Value = prescription.Id.ToString();
            if (prescription.LensesPrescription is SpectaclePrescription)
            {
                rqd.ItemCodeInternal.Text.Value = PatientClinical.SpectaclePrescriptionIndicator;
                rqd.ItemCodeInternal.AlternateText.Value = ((SpectaclePrescription)prescription.LensesPrescription).GlassesType;
            }
            else
            {
                rqd.ItemCodeInternal.Text.Value = PatientClinical.ContactLensesPrescriptionIndicator;
                var inventory = _legacyPracticeRepository.Value.CLInventories.FirstOrDefault(i => i.InventoryId == ((ContactLensesPrescription)prescription.LensesPrescription).ODPrescription.InventoryId);
                if (inventory != null)
                {
                    string material = inventory.Material == "SOFT" ? "SOFT" : "HARD";
                    rqd.ItemCodeInternal.AlternateText.Value = material;
                }
            }
        }

        private void AddVisionPlans(ORM_O01 message, Model.Appointment appointment)
        {
            var visionPlans = _practiceRepository.Value.PatientInsurances.Include(i => i.InsurancePolicy, i => i.InsurancePolicy.PolicyholderPatient.PatientAddresses).Where(i => i.InsuredPatientId == appointment.Encounter.PatientId & i.InsuranceType == InsuranceType.Vision).ToArray();
            // Order by self held insurances first, then by ordinal Id
            visionPlans = visionPlans.OrderBy(i => i.InsuredPatientId == i.InsurancePolicy.PolicyholderPatientId ? 0 : 1).ThenBy(i => i.OrdinalId).ToArray();

            foreach (var visionPlan in visionPlans)
            {
                AddVisionPlan(message, appointment, visionPlan);
            }
        }

        private static void ConfigureOrc(Model.Appointment appointment, NHapi.Model.V231.Group.ORM_O01_ORDER order)
        {
            var provider = appointment.Encounter.ClinicalInvoiceProviders.First().Provider;

            order.ORC.OrderControl.Value = "NW";
            order.ORC.PlacerOrderNumber.EntityIdentifier.Value = appointment.Id.ToString();
            order.ORC.DateTimeOfTransaction.TimeOfAnEvent.Value = appointment.DateTime.ToString("yyyyMMdd");
            order.ORC.GetOrderingProvider(0).IDNumber.Value = provider.User.Id.ToString();
            order.ORC.GetOrderingProvider(0).FamilyLastName.FamilyName.Value = provider.User.LastName;
            order.ORC.GetOrderingProvider(0).GivenName.Value = provider.User.FirstName;
            order.ORC.GetOrderingProvider(0).MiddleInitialOrName.Value = provider.User.MiddleName;
            order.ORC.GetOrderingProvider(0).SuffixEgJRorIII.Value = provider.User.Honorific;

            order.ORC.EntererSLocation.Facility.NamespaceID.Value = appointment.Encounter.ServiceLocationId.ToString();
            order.ORC.EntererSLocation.PointOfCare.Value = appointment.Encounter.ServiceLocation.Name;
        }

        private void AddVisionPlan(ORM_O01 message, Model.Appointment appointment, PatientInsurance patientInsurance)
        {
            var policyInsurer = _practiceRepository.Value.Insurers.Include(i => i.InsurerPhoneNumbers, i => i.InsurerAddresses.Select(ia => ia.StateOrProvince), i=> i.InsurerPlanType).First(i => i.Id == patientInsurance.InsurancePolicy.InsurerId);
            var insuranceRepitition = message.PATIENT.GetINSURANCE(message.PATIENT.INSURANCERepetitionsUsed);
            insuranceRepitition.IN1.SetIDIN1.Value = message.PATIENT.INSURANCERepetitionsUsed.ToString();

            insuranceRepitition.IN1.InsurancePlanID.Identifier.Value = patientInsurance.Id.ToString();
            insuranceRepitition.IN1.GetInsuranceCompanyID(0).ID.Value = patientInsurance.InsurancePolicy.InsurerId.ToString();

            insuranceRepitition.IN1.GetInsuranceCompanyName(0).OrganizationName.Value = policyInsurer.Name;
            insuranceRepitition.IN1.GetInsuranceCompanyName(0).OrganizationNameTypeCode.Value = policyInsurer.PlanName;

            var address = policyInsurer.InsurerAddresses.WithAddressType(InsurerAddressTypeId.Claims);
            if (address != null)
            {
                insuranceRepitition.IN1.GetInsuranceCompanyAddress(0).StreetAddress.Value = address.Line1;
                insuranceRepitition.IN1.GetInsuranceCompanyAddress(0).City.Value = address.City;
                insuranceRepitition.IN1.GetInsuranceCompanyAddress(0).StateOrProvince.Value = address.StateOrProvince.Abbreviation;
                insuranceRepitition.IN1.GetInsuranceCompanyAddress(0).ZipOrPostalCode.Value = address.PostalCode;
            }

            var phone = policyInsurer.InsurerPhoneNumbers.WithPhoneNumberType(InsurerPhoneNumberTypeId.Main);
            if (phone != null)
            {
                insuranceRepitition.IN1.GetInsuranceCoPhoneNumber(0).Get9999999X99999CAnyText.Value = phone.ToString().ToNumeric();
            }

            insuranceRepitition.IN1.GroupNumber.Value = patientInsurance.InsurancePolicy.GroupCode;

            insuranceRepitition.IN1.GetGroupName(0).OrganizationName.Value = policyInsurer.GroupName;

            insuranceRepitition.IN1.PlanEffectiveDate.Value = patientInsurance.InsurancePolicy.StartDateTime.ToString("yyyyMMdd");

            if (patientInsurance.EndDateTime.HasValue)
            {
                insuranceRepitition.IN1.PlanExpirationDate.Value = patientInsurance.EndDateTime.Value.ToString("yyyyMMdd");
            }

            string authorizationNumber = null;
            string authorizationComment = null;

            var referral = appointment.Encounter.PatientInsuranceReferrals.FirstOrDefault(i => i.PatientInsuranceId == patientInsurance.Id);
            if (referral != null)
            {
                authorizationNumber = referral.ReferralCode;
                authorizationComment = referral.Comment;
            }
            else if (appointment.Encounter.PatientInsuranceAuthorizations.Count > 0)
            {
                var authorization = appointment.Encounter.PatientInsuranceAuthorizations.First();
                authorizationNumber = authorization.AuthorizationCode;
                authorizationComment = authorization.Comment;
            }

            insuranceRepitition.IN1.AuthorizationInformation.AuthorizationNumber.Value = authorizationNumber;
            insuranceRepitition.IN1.AuthorizationInformation.Source.Value = authorizationComment;

            var claimFilingIndicatorCode = patientInsurance.InsurancePolicy.Insurer.ClaimFilingIndicatorCode;
            var mapping = _externalSystemService.Value.GetMappings<ClaimFilingIndicatorCode>(((int)claimFilingIndicatorCode).ToString(), Constants.MVEExternalSystemName).FirstOrDefault(i => i.ExternalSystemEntity.Name == "ClaimFilingIndicatorCode");
            if(mapping != null)
            {
                insuranceRepitition.IN1.PlanType.Value = mapping.ExternalSystemEntityKey;
            }

            insuranceRepitition.IN1.GetNameOfInsured(0).FamilyLastName.FamilyName.Value = patientInsurance.InsurancePolicy.PolicyholderPatient.LastName;
            insuranceRepitition.IN1.GetNameOfInsured(0).GivenName.Value = patientInsurance.InsurancePolicy.PolicyholderPatient.FirstName;
            insuranceRepitition.IN1.GetNameOfInsured(0).MiddleInitialOrName.Value = patientInsurance.InsurancePolicy.PolicyholderPatient.MiddleName;

            string relationship;
            var relationshipMap = Enum.GetValues(typeof(PolicyHolderRelationshipType))
               .Cast<PolicyHolderRelationshipType>()
               .ToDictionary(t => (int)t, t => t.ToString());
            relationshipMap.TryGetValue(Convert.ToInt32(patientInsurance.PolicyholderRelationshipType), out relationship);
            if (relationship != null)
            {
                relationship = relationship == "Self" ? "Y" : relationship.Substring(0, 1);
                insuranceRepitition.IN1.InsuredSRelationshipToPatient.Identifier.Value = relationship;
            }

            if (patientInsurance.InsurancePolicy.PolicyholderPatient.DateOfBirth.HasValue)
            {
                insuranceRepitition.IN1.InsuredSDateOfBirth.TimeOfAnEvent.Value = patientInsurance.InsurancePolicy.PolicyholderPatient.DateOfBirth.Value.ToString("yyyyMMdd");
            }

            var policyHolderAddress = patientInsurance.InsurancePolicy.PolicyholderPatient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home);

            if (policyHolderAddress == null) Trace.TraceWarning("No address for policyholder {0}.", patientInsurance.InsurancePolicy.PolicyholderPatient.DisplayName);

            if (policyHolderAddress != null)
            {
            insuranceRepitition.IN1.GetInsuredSAddress(0).StreetAddress.Value = policyHolderAddress.Line1;
            insuranceRepitition.IN1.GetInsuredSAddress(0).OtherDesignation.Value = policyHolderAddress.Line2;
            insuranceRepitition.IN1.GetInsuredSAddress(0).City.Value = policyHolderAddress.City;
            insuranceRepitition.IN1.GetInsuredSAddress(0).StateOrProvince.Value = policyHolderAddress.StateOrProvince.Abbreviation;
            insuranceRepitition.IN1.GetInsuredSAddress(0).ZipOrPostalCode.Value = policyHolderAddress.PostalCode;
            }

            if (patientInsurance.InsurancePolicy.PolicyholderPatient.Gender != null)
            {
// ReSharper disable once PossibleInvalidOperationException
                insuranceRepitition.IN1.InsuredSSex.Value = patientInsurance.InsurancePolicy.PolicyholderPatient.Gender.Value.ToString().Substring(0, 1);
            }

            insuranceRepitition.IN1.PolicyNumber.Value = patientInsurance.InsurancePolicy.PolicyCode;
        }

        public IMessage ProduceMessage(ExternalSystemMessage externalSystemMessage)
        {
            var appointmentId = externalSystemMessage.ExternalSystemMessagePracticeRepositoryEntities.Where(e => e.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.Appointment).Select(i => i.PracticeRepositoryEntityKey.ToInt().GetValueOrDefault()).FirstOrDefault();
            if ((appointmentId == 0))
            {
                return null;
            }

            var prescriptions = _practiceRepository.Value.EncounterLensesPrescriptions.Where(i => i.Encounter.Appointments.Any(a => a.Id == appointmentId)).ToArray();

            if (prescriptions.Length == 0)
            {
                Trace.TraceInformation("ORM Message will not be created because there are no prescriptions for AppointmentId {0}".FormatWith(appointmentId));
                return null;
            }

            var message = CreateMessage(appointmentId, externalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystem.Name, externalSystemMessage.Id.ToString("N"));
            return new HL7Message(message);
        }

    }
}
