using IO.Practiceware.Integration.Messaging;
using NHapi.Model.V23.Message;
using Soaf;
using System;
using System.Diagnostics;
using IOIMessage = IO.Practiceware.Integration.Messaging.IMessage;
using NHapiIMessage = NHapi.Base.Model.IMessage;
using V23MSH = NHapi.Model.V23.Segment.MSH;

namespace IO.Practiceware.Integration.HL7
{
    public class HL7Message : IOIMessage
	{
        private NHapiIMessage _message;
		private string _messageText;
        private string _messageType;
		private readonly string _createdBy;

        public NHapiIMessage Message
		{
			get { return _message; }
		}

		public object Id
		{
			get { return _message.GetMessageControlId(); }
		}

		public object AcknowledgementForId
		{
			get
			{
				var ack = _message as ACK;
                return (ack != null) ? ack.MSA.MessageControlID.Value : null;
			}
		}

		public string CreatedBy
		{
			get { return _createdBy; }
		}

		public string Source
		{
			get { return _message.GetSendingApplication(); }
		}

		public string Destination
		{
			get { return _message.GetReceivingApplication(); }
		}

		public bool ShouldProcess
		{
			get { return _message.ShouldProcess(); }
		}

		public string MessageType
		{
			get
			{
				return _messageType ?? "{0}_V{1}".FormatWith(Message.GetType().Name,
				                             Message.Version.Replace(".", string.Empty));
			}
		    set
		    {
                _messageType = value;
		    }
		}

		public bool RequestsAcknowledgement
		{
			get { return _message.RequestsAcknowledgement(); }
		}

		public HL7Message()
		{
		    var declaringType = new StackTrace().GetFrame(1).GetMethod().DeclaringType;
		    if (declaringType != null) _createdBy = declaringType.Name;
		}

        public HL7Message(string message)
        {
            var declaringType = new StackTrace().GetFrame(1).GetMethod().DeclaringType;
            if (declaringType != null) _createdBy = declaringType.Name;
            Load(message);
        }

        public HL7Message(NHapiIMessage message)
        {
            var declaringType = new StackTrace().GetFrame(1).GetMethod().DeclaringType;
            if (declaringType != null) _createdBy = declaringType.Name;
            _message = message;
        }

        public override string ToString()
		{
			return _messageText ?? HL7Messages.GetPipeParser().Encode(Message).Trim();
		}
		
		public void Load(string value)
		{
			if (value.StartsWith(Constants.MessageStartDelimiter) && value.EndsWith(Constants.MessageEndDelimiter))
			{
				value = value.TrimStart(Constants.MessageStartDelimiter).TrimEnd(Constants.MessageEndDelimiter);
			}
			_messageText = value;
			_message = HL7Messages.GetPipeParser().Parse(value);
		}

        public IOIMessage GetAcknowledgement()
		{
            if (Message.RequestsAcknowledgement() && !Message.IsAcknowledgement())
			{
				var msh = Message.GetMsh<V23MSH>(HL7Messages.Version.V23);
			    var ack = new ACK();
				ack.MSH.MessageType.MessageType.Value = "ACK";
			    ack.MSH.MessageControlID.Value = Guid.NewGuid().ToString("N");
				ack.MSH.SendingApplication.NamespaceID.Value = "IO PRACTICEWARE";
				ack.MSH.ReceivingApplication.NamespaceID.Value = msh.SendingApplication.NamespaceID.Value;
				ack.MSH.DateTimeOfMessage.TimeOfAnEvent.SetLongDateWithSecond(DateTime.UtcNow);
                ack.MSH.ProcessingID.ProcessingID.Value = MessagingConfiguration.Current.HL7MessagingConfiguration.OutboundMessagesProcessingId;
				ack.MSA.AcknowledgementCode.Value = "AA";
				ack.MSA.MessageControlID.Value = msh.MessageControlID.Value;
				return new HL7Message(ack);
			}
            return null;
		}
	}
}
