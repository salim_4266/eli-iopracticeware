using IO.Practiceware.Integration.Messaging;
using NHapi.Base.Model;
using NHapi.Base.Parser;
using NHapi.Model.V23.Datatype;
using Soaf;
using Soaf.Collections;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using ID = NHapi.Model.V21.Datatype.ID;
using NHapiIMessage = NHapi.Base.Model.IMessage;
using V21MSH = NHapi.Model.V21.Segment.MSH;
using V22MSH = NHapi.Model.V22.Segment.MSH;
using V231MSH = NHapi.Model.V231.Segment.MSH;
using V23MSH = NHapi.Model.V23.Segment.MSH;
using V24MSH = NHapi.Model.V24.Segment.MSH;
using V25MSH = NHapi.Model.V25.Segment.MSH;

namespace IO.Practiceware.Integration.HL7
{
    public static class HL7Messages
    {
        #region Version enum

        public enum Version
        {
            [Description("2.1")]
            V21,
            [Description("2.2")]
            V22,
            [Description("2.3")]
            V23,
            [Description("2.3.1")]
            V231,
            [Description("2.4")]
            V24,
            [Description("2.5")]
            V25
        }

        #endregion

        public static bool IsPopulated(this AD address)
        {
            return new[] { address.StreetAddress.Value, address.City.Value, address.ZipOrPostalCode.Value, address.StateOrProvince.Value }.All(i => i.IsNotNullOrEmpty());
        }

        public static PipeParser GetPipeParser()
        {
            var parser = new PipeParser(new ModelClassFactory());
            var validationContext = new NHapi.Base.validation.impl.DefaultValidation();
            var limitationRules = validationContext.PrimitiveRuleBindings;
            validationContext.PrimitiveRuleBindings.Remove(limitationRules[4]);
            parser.ValidationContext = validationContext;
            return parser;

        }

        public static string GetFieldPrimitive(this GenericSegment segment, int fieldNumber)
        {
            return ((IPrimitive)((Varies)segment.GetField(fieldNumber, 0)).Data).Value;
        }

        public static void SetFieldPrimitive(this GenericSegment segment, int fieldNumber, string value)
        {
            ((IPrimitive)((Varies)segment.GetField(fieldNumber, 0)).Data).Value = value;
        }

        public static string GetReceivingApplication(this NHapiIMessage message)
        {
            IStructure msh = message.GetStructure("MSH");
            if (msh is V21MSH)
            {
                return ((V21MSH)msh).RECEIVINGAPPLICATION.Value;
            }
            if (msh is V22MSH)
            {
                return ((V22MSH)msh).ReceivingApplication.Value;
            }
            if (msh is V23MSH)
            {
                return ((V23MSH)msh).ReceivingApplication.NamespaceID.Value;
            }
            if (msh is V231MSH)
            {
                return ((V231MSH)msh).ReceivingApplication.NamespaceID.Value;
            }
            if (msh is V24MSH)
            {
                return ((V24MSH)msh).ReceivingApplication.NamespaceID.Value;
            }
            if (msh is V25MSH)
            {
                return ((V25MSH)msh).ReceivingApplication.NamespaceID.Value;
            }

            return null;
        }

        public static string GetSendingApplication(this NHapiIMessage message)
        {
            IStructure msh = message.GetStructure("MSH");
            if (msh is V21MSH)
            {
                return ((V21MSH)msh).SENDINGAPPLICATION.Value;
            }
            if (msh is V22MSH)
            {
                return ((V22MSH)msh).SendingApplication.Value;
            }
            if (msh is V23MSH)
            {
                return ((V23MSH)msh).SendingApplication.NamespaceID.Value;
            }
            if (msh is V231MSH)
            {
                return ((V231MSH)msh).SendingApplication.NamespaceID.Value;
            }
            if (msh is V24MSH)
            {
                return ((V24MSH)msh).SendingApplication.NamespaceID.Value;
            }
            if (msh is V25MSH)
            {
                return ((V25MSH)msh).SendingApplication.NamespaceID.Value;
            }

            return null;
        }

        public static string GetMessageType(this NHapiIMessage message)
        {
            IStructure msh = message.GetStructure("MSH");
            if (msh is V21MSH)
            {
                return ((V21MSH)msh).MESSAGETYPE.Value;
            }
            if (msh is V22MSH)
            {
                return ((V22MSH)msh).MessageType.MessageType.Value;
            }
            if (msh is V231MSH)
            {
                return ((V231MSH)msh).MessageType.MessageType.Value;
            }
            if (msh is V23MSH)
            {
                return ((V23MSH)msh).MessageType.MessageType.Value;
            }
            if (msh is V24MSH)
            {
                return ((V24MSH)msh).MessageType.MessageType.Value;
            }
            if (msh is V25MSH)
            {
                return ((V25MSH)msh).MessageType.MessageCode.Value;
            }

            return null;
        }

        public static string GetTriggerEvent(this NHapiIMessage message)
        {
            IStructure msh = message.GetStructure("MSH");
            if (msh is V21MSH)
            {
                return ((V21MSH)msh).MESSAGETYPE.Value;
            }
            if (msh is V22MSH)
            {
                return ((V22MSH)msh).MessageType.TriggerEvent.Value;
            }
            if (msh is V231MSH)
            {
                return ((V231MSH)msh).MessageType.TriggerEvent.Value;
            }
            if (msh is V23MSH)
            {
                return ((V23MSH)msh).MessageType.TriggerEvent.Value;
            }
            if (msh is V24MSH)
            {
                return ((V24MSH)msh).MessageType.TriggerEvent.Value;
            }
            if (msh is V25MSH)
            {
                return ((V25MSH)msh).MessageType.TriggerEvent.Value;
            }

            return null;
        }

        public static void SetAcknowledgementType(this NHapiIMessage message, string value)
        {
            IStructure msh = message.GetStructure("MSH");
            if (msh is V22MSH)
            {
                ((V22MSH)msh).AcceptAcknowledgementType.Value = value;
            }
            else if (msh is V231MSH)
            {
                ((V231MSH)msh).AcceptAcknowledgmentType.Value = value;
            }
            else if (msh is V23MSH)
            {
                ((V23MSH)msh).AcceptAcknowledgementType.Value = value;
            }
            else if (msh is V24MSH)
            {
                ((V24MSH)msh).AcceptAcknowledgmentType.Value = value;
            }
            else if (msh is V25MSH)
            {
                ((V25MSH)msh).AcceptAcknowledgmentType.Value = value;
            }
        }

        public static void RequestAcknowledgement(this NHapiIMessage message)
        {
            message.SetAcknowledgementType("AL");
        }

        public static ISegment GetSegment(this NHapiIMessage message, string name)
        {
            return message.GetStructure(name).CastTo<ISegment>();
        }

        public static string GetPrimitiveFieldValue(this ISegment segment, int index)
        {
            try
            {
                return segment.GetField(index)[0].CastTo<Varies>().Data.CastTo<GenericPrimitive>().Value;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string[] GetCompositePrimitiveFieldValues(this ISegment segment, int index)
        {
            try
            {
                var composite = segment.GetField(index)[0].CastTo<Varies>().Data.CastTo<GenericComposite>();
                return composite.Components.Select(component => component.CastTo<Varies>().Data.CastTo<GenericPrimitive>().Value).ToArray();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static NHapiIMessage Reparse(this NHapiIMessage message, Version version)
        {
            PipeParser parser = GetPipeParser();
            string messageString = parser.Encode(message).Trim();
            return parser.Parse(messageString, version.GetAttribute<DescriptionAttribute>().Description);
        }

        public static TMessage Reparse<TMessage>(this NHapiIMessage message) where TMessage : NHapiIMessage, new()
        {
            PipeParser parser = GetPipeParser();
            string messageString = parser.Encode(message).Trim();
            return parser.Parse(messageString, new TMessage().Version).CastTo<TMessage>();
        }

        public static bool ShouldProcess(this NHapiIMessage message)
        {
            return (message.IsAcknowledgement() ||
                    message.GetMsh<V23MSH>(Version.V23).ProcessingID.ProcessingID.Value == Constants.ProcessingId.Production.GetAttribute<DescriptionAttribute>().Description ||
                    MessagingConfiguration.Current.HL7MessagingConfiguration.ProcessAllInboundMessages);
        }

        public static bool ShouldProcessMessage(this IStructure value)
        {
            return value.Message.ShouldProcess();
        }

        public static TMsh GetMsh<TMsh>(this NHapiIMessage message, Version version) where TMsh : ISegment
        {
            var msh = (ISegment)message.GetStructure("MSH");
            if (message.Version != version.GetAttribute<DescriptionAttribute>().Description)
            {
                var encodingChars = new EncodingCharacters('|', null);
                PipeParser parser = GetPipeParser();
                string segmentString = PipeParser.encode(msh, encodingChars).Trim();
                msh = ParserBase.MakeControlMSH(version.GetAttribute<DescriptionAttribute>().Description, new ModelClassFactory());
                parser.parse(msh, segmentString, encodingChars);
            }
            return msh.CastTo<TMsh>();
        }

        public static TStructure GetStructure<TStructure>(this IGroup group, string name) where TStructure : IStructure
        {
            return group.GetStructure(name).CastTo<TStructure>();
        }

        public static bool IsAcknowledgement(this NHapiIMessage message)
        {
            var msh = message.GetMsh<V23MSH>(Version.V23);
            return (msh.MessageType.MessageType.Value == "ACK");
        }

        public static bool RequestsAcknowledgement(this NHapiIMessage message)
        {
            var msh = message.GetMsh<V23MSH>(Version.V23);
            return (msh.AcceptAcknowledgementType.Value == "AL" || msh.ApplicationAcknowledgementType.Value == "AL");
        }

        public static string GetMessageControlId(this NHapiIMessage message)
        {
            var msh = message.GetMsh<V23MSH>(Version.V23);
            return msh.MessageControlID.Value;
        }

        public static void SetMessageControlId(this NHapiIMessage message, string value)
        {
            value = value.Truncate(20);
            IStructure msh = message.GetStructure("MSH");
            if (msh is V22MSH)
            {
                ((V22MSH)msh).MessageControlID.Value = value;
            }
            else if (msh is V231MSH)
            {
                ((V231MSH)msh).MessageControlID.Value = value;
            }
            else if (msh is V23MSH)
            {
                ((V23MSH)msh).MessageControlID.Value = value;
            }
            else if (msh is V24MSH)
            {
                ((V24MSH)msh).MessageControlID.Value = value;
            }
            else if (msh is V25MSH)
            {
                ((V25MSH)msh).MessageControlID.Value = value;
            }
        }

        #region Nested type: ModelClassFactory

        /// <summary>
        ///   Helper model class factory that uses reflection over well known assembly instances (not by name, since that doesn't work when GAC'd).
        /// </summary>
        private class ModelClassFactory : IModelClassFactory
        {
            private static readonly Assembly[] Assemblies =
            {
                typeof (ID).Assembly,
                typeof (NHapi.Model.V22.Datatype.ID).Assembly,
                typeof (NHapi.Model.V23.Datatype.ID).Assembly,
                typeof (NHapi.Model.V231.Datatype.ID).Assembly,
                typeof (NHapi.Model.V24.Datatype.ID).Assembly,
                typeof (NHapi.Model.V25.Datatype.ID).Assembly
            };

            public Type GetGroupClass(string theName, string theVersion)
            {
                return Assemblies.Where(a => GetAssemblyVersion(a) == GetVersionNamespace(theVersion)).Select(a => a.GetType("NHapi.Model.{0}.Group.{1}".FormatWith(GetVersionNamespace(theVersion), theName))).FirstOrDefault();
            }

            public Type GetMessageClass(string theName, string theVersion, bool isExplicit)
            {

                var result = Assemblies.Where(a => GetAssemblyVersion(a) == GetVersionNamespace(theVersion)).Select(a => a.GetType("NHapi.Model.{0}.Message.{1}".FormatWith(GetVersionNamespace(theVersion), theName))).FirstOrDefault();
                if (result == null && theVersion == "2.3.1" && !isExplicit)
                {
                    result = GetMessageClass(theName, "2.3", false);
                }
                return result;
            }

            public Type GetSegmentClass(string theName, string theVersion)
            {
                return Assemblies.Where(a => GetAssemblyVersion(a) == GetVersionNamespace(theVersion)).Select(a => a.GetType("NHapi.Model.{0}.Segment.{1}".FormatWith(GetVersionNamespace(theVersion), theName))).FirstOrDefault();
            }

            public Type GetTypeClass(string theName, string theVersion)
            {
                return Assemblies.Where(a => GetAssemblyVersion(a) == GetVersionNamespace(theVersion)).Select(a => a.GetType("NHapi.Model.{0}.Datatype.{1}".FormatWith(GetVersionNamespace(theVersion), theName))).FirstOrDefault();
            }

            private static string GetAssemblyVersion(Assembly assembly)
            {
                return assembly.GetName().Name.Split('.').Last();
            }

            private static string GetVersionNamespace(string theVersion)
            {
                return "V" + theVersion.Replace(".", string.Empty);
            }
        }

        #endregion
    }
}