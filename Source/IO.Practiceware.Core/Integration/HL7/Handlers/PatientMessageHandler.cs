using IO.Practiceware.Integration.HL7.Handlers;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Services.ExternalSystems;
using NHapi.Base.Model;
using NHapi.Model.V23.Datatype;
using NHapi.Model.V23.Message;
using NHapi.Model.V23.Segment;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using IOIMessage = IO.Practiceware.Integration.Messaging.IMessage;
using NHapiIMessage = NHapi.Base.Model.IMessage;

[assembly: Component(typeof(PatientMessageHandler.HandleMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.HL7.Handlers
{
    [SupportsUnitOfWork]
    public class PatientMessageHandler
    {
        public class HandleMessageSubscriber
        {
            public HandleMessageSubscriber(IMessenger messenger, Func<PatientMessageHandler> handler)
            {
                messenger.Subscribe((IMessenger s, object t, HandleMessageRequest m) => handler().HandleMessage((HL7Message)m.Message),
                                    m => handler().HandlesMessage(m.Message));
            }
        }

        private readonly Lazy<IExternalSystemService> _externalSystemService;
        private readonly Lazy<IPracticeRepository> _practiceRepository;

        public PatientMessageHandler(Func<IPracticeRepository> practiceRepository,
            Func<IExternalSystemService> externalSystemService)
        {
            _practiceRepository = Lazy.For(practiceRepository);
            _externalSystemService = Lazy.For(externalSystemService);
        }

        [UnitOfWork(AcceptChanges = true, AutoAcceptChanges = true)]
        protected virtual void HandleMessage(NHapiIMessage message)
        {
            if (message is ADT_A28)
            {
                HandleMessage((ADT_A28)message);
            }
            else if (message is ADT_A31)
            {
                HandleMessage((ADT_A31)message);
            }
            else if (message is ADT_A04)
            {
                HandleMessage((ADT_A04)message);
            }
            else if (message is ADT_A08)
            {
                HandleMessage((ADT_A08)message);
            }
            else if (new[] { typeof(NHapi.Model.V231.Message.ADT_A28), typeof(NHapi.Model.V231.Message.ADT_A31), typeof(NHapi.Model.V231.Message.ADT_A04), typeof(NHapi.Model.V231.Message.ADT_A08) }.Any(t => message.Is(t)))
            {
                HandleMessage(message.Reparse(HL7Messages.Version.V23));
            }
        }

        private void HandleMessage(ADT_A28 message)
        {
            HandleMessageComponents(message.MSH, message.PID, message.GetINSURANCE().IN1, false);
        }

        private void HandleMessage(ADT_A31 message)
        {
            HandleMessageComponents(message.MSH, message.PID, message.GetINSURANCE().IN1, false);
        }

        private void HandleMessage(ADT_A04 message)
        {
            HandleMessageComponents(message.MSH, message.PID, message.GetINSURANCE().IN1, false);
        }

        private void HandleMessage(ADT_A08 message)
        {
            HandleMessageComponents(message.MSH, message.PID, message.GetINSURANCE().IN1, false);
        }

        private void HandleMessageComponents(MSH msh, PID pid, IN1 in1, bool updateOnly)
        {
            string externalPatientId = pid.PatientIDExternalID.ID.Value;
            if (externalPatientId.IsNullOrEmpty())
            {
                externalPatientId = pid.GetPatientIDInternalID(0).ID.Value;
            }
            ExternalSystemEntityMapping patientMapping = _externalSystemService.Value.GetMapping<Patient>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.Patient, externalPatientId, updateOnly);
            Patient patient = (patientMapping.Id == 0) ? new Patient() :
                                                                           _practiceRepository.Value.Patients.Include(p => p.PatientAddresses).
                                                                               Include(p => p.PatientPhoneNumbers, p => p.Ethnicity, p => p.PatientEmailAddresses, p => p.Races).
                                                                               First(p => p.Id == Int32.Parse(patientMapping.PracticeRepositoryEntityKey));
            ExternalSystemEntityMapping insurerMapping = null;
            Insurer insurer = null;
            if (in1.InsuranceCompanyID.ID.Value != null)
            {
                insurerMapping = _externalSystemService.Value.GetMapping<Insurer>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.Insurer, in1.InsuranceCompanyID.ID.Value);
                insurer = (insurerMapping.Id == 0) ? new Insurer() :
                                                                       _practiceRepository.Value.Insurers.Include(i => i.InsurerAddresses).
                                                                           Include(i => i.InsurerPhoneNumbers).
                                                                           FirstOrDefault(i => i.Id == Int32.Parse(insurerMapping.PracticeRepositoryEntityKey));
            }
            HandlePatient(patient, pid, msh);

            _practiceRepository.Value.Save(patient);
            patientMapping.PracticeRepositoryEntityKey = patient.Id.ToString();
            _practiceRepository.Value.Save(patientMapping);

            if (insurerMapping == null || insurer == null) return;
            HandleInsurer(insurer, in1);
            _practiceRepository.Value.Save(insurer);
            insurerMapping.PracticeRepositoryEntityKey = insurer.Id.ToString();
            _practiceRepository.Value.Save(insurerMapping);
            InsurancePolicy insurance = HandlePatientInsurance(patient, insurer);
            _practiceRepository.Value.Save(insurance);
        }

        private void HandlePatient(Patient patient, PID pid, MSH msh)
        {
            #region Helper function: handleAddress - Returns true if PID Address segment contains a valid address.  Otherwise false.

            Action<XAD> handleAddress = pidAddressSegment =>
                                            {
                                                bool isAddressValid = true;
                                                if (pidAddressSegment.StreetAddress.Value.IsNullOrEmpty())
                                                {
                                                    Trace.TraceWarning("PID does not contain a valid street address.");
                                                    isAddressValid = false;
                                                }
                                                if (pidAddressSegment.ZipOrPostalCode.Value.IsNullOrEmpty() ||
                                                    pidAddressSegment.City.Value.IsNullOrEmpty() || pidAddressSegment.StateOrProvince.Value.IsNullOrEmpty())
                                                {
                                                    Trace.TraceWarning("PID does not contain a valid zipcode or City/State.");
                                                    isAddressValid = false;
                                                }
                                                if (!isAddressValid)
                                                {
                                                    return;
                                                }

                                                PatientAddress homeAddress = patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home, NotFoundBehavior.Add);
                                                homeAddress.Line1 = pidAddressSegment.StreetAddress.Value.Truncate(35);
                                                homeAddress.Line2 = pidAddressSegment.OtherDesignation.Value.Truncate(35);
                                                homeAddress.City = pidAddressSegment.City.Value.Truncate(35);
                                                homeAddress.StateOrProvince = _practiceRepository.Value.StateOrProvinces.FirstOrDefault(s => s.Abbreviation == pidAddressSegment.StateOrProvince.Value.Truncate(2)).
                                                    EnsureNotDefault("Could not find state {0}.".FormatWith(pidAddressSegment.StateOrProvince.Value));
                                                homeAddress.PostalCode = pidAddressSegment.ZipOrPostalCode.Value.Truncate(10);
                                            };

            #endregion

            patient.LastName = pid.PatientName.FamilyName.Value.Truncate(35);
            patient.FirstName = pid.PatientName.GivenName.Value.Truncate(35);
            string middleInitial = pid.PatientName.MiddleInitialOrName.Value;
            middleInitial = middleInitial != null ? middleInitial.FirstOrDefault().ToString(CultureInfo.InvariantCulture) : String.Empty;

            patient.MiddleName = middleInitial.Truncate(1);
            patient.DateOfBirth = (String.IsNullOrEmpty(pid.DateOfBirth.TimeOfAnEvent.Value)) ? new DateTime?() :
                                                                                                                    pid.DateOfBirth.TimeOfAnEvent.GetAsDate();

            if (pid.Sex.Value.IsNotNullOrEmpty())
            {
                var patientGenderMapping = _externalSystemService.Value.GetMapping<Gender>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.Gender, pid.Sex.Value.Truncate(1));
                if (patientGenderMapping != null && patientGenderMapping.Id > 0)
                {
                    var patientGender = (Gender)_externalSystemService.Value.GetMappedEntity(patientGenderMapping);
                    patient.Gender = patientGender;
                }
            }

            if (pid.GetMaritalStatus(0).Value.IsNotNullOrEmpty())
            {
                var patientMaritalStatusMapping = _externalSystemService.Value.GetMapping<MaritalStatus>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.MaritalStatus, pid.GetMaritalStatus(0).Value.Truncate(1));
                if (patientMaritalStatusMapping != null && patientMaritalStatusMapping.Id > 0)
                {
                    var patientMaritalStatus = (MaritalStatus)_externalSystemService.Value.GetMappedEntity(patientMaritalStatusMapping);
                    patient.MaritalStatus = patientMaritalStatus;
                }
            }
            patient.SocialSecurityNumber = pid.SSNNumberPatient.Value.Truncate(16);

            handleAddress(pid.GetPatientAddress(0));

            if (pid.PhoneNumberHomeRepetitionsUsed > 3)
            {
                string emailAddress = pid.GetPhoneNumberHome(3).Value;
                if (emailAddress.IsNotNullOrEmpty())
                {
                    var patientEmailAddress = patient.PatientEmailAddresses.FirstOrDefault(e => e.EmailAddressTypeId == (int) EmailAddressType.Personal);
                    if (patientEmailAddress == null) patient.PatientEmailAddresses.Add(new PatientEmailAddress { Value = emailAddress, EmailAddressTypeId = (int) EmailAddressType.Personal });
                    else patientEmailAddress.Value = emailAddress;
                }
            }
            //As per specs Personal EmailAddress is the 3rd component number starting at 0. But, 
            //As per NHapi Personal EmailAddress is the 2nd extra component number starting at 0 after getting Home Phone number from PID-13 segment
            //the format of PID-13 is like for example: |(HomePhone)^^^EmailAdress|
            if (pid.GetPhoneNumberHome(0).ExtraComponents != null && pid.GetPhoneNumberHome(0).ExtraComponents.numComponents() > 2)
            {
                string emailAddress = pid.GetPhoneNumberHome(0).ExtraComponents.getComponent(2).Data.ToString();
                if (emailAddress.IsNotNullOrEmpty())
                {
                    var patientEmailAddress = patient.PatientEmailAddresses.FirstOrDefault(e => e.EmailAddressTypeId == (int) EmailAddressType.Personal);
                    if (patientEmailAddress == null) patient.PatientEmailAddresses.Add(new PatientEmailAddress { Value = emailAddress, EmailAddressTypeId = (int) EmailAddressType.Personal });
                    else patientEmailAddress.Value = emailAddress;
                }
            }
            string homePhone = pid.GetPhoneNumberHome(0).Value.Truncate(25);
            if (homePhone.IsNotNullOrEmpty() && PhoneNumberParserExtensions.IsValidPhoneNumber(homePhone))
            {
                PatientPhoneNumber patientHomePhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home, NotFoundBehavior.Add);
                patientHomePhone.SetValuesFromString(homePhone);
            }
            string businessPhone = pid.GetPhoneNumberBusiness(0).Value.Truncate(25);
            if (businessPhone.IsNotNullOrEmpty() && PhoneNumberParserExtensions.IsValidPhoneNumber(businessPhone))
            {
                PatientPhoneNumber patientBusinessPhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business, NotFoundBehavior.Add);
                patientBusinessPhone.SetValuesFromString(businessPhone);
            }

            if (pid.PhoneNumberBusinessRepetitionsUsed > 3)
            {
                string emailAddress = pid.GetPhoneNumberBusiness(3).Value;
                if (emailAddress.IsNotNullOrEmpty())
                {
                    var patientEmailAddress = patient.PatientEmailAddresses.FirstOrDefault(e => e.EmailAddressTypeId == (int)EmailAddressType.Business);
                    if (patientEmailAddress == null) patient.PatientEmailAddresses.Add(new PatientEmailAddress { Value = emailAddress, EmailAddressTypeId = (int)EmailAddressType.Business });
                    else patientEmailAddress.Value = emailAddress;
                }
            }
            //As per specs Business EmailAddress is the 3rd component number starting at 0. But, 
            //As per NHapi Business EmailAddress is the 2nd extra component number starting at 0 after getting Home Phone number from PID-14 segment
            //the format of PID-14 is like for example: |(BusinessPhone)^^^EmailAdress|
            if (pid.GetPhoneNumberBusiness(0).ExtraComponents != null && pid.GetPhoneNumberBusiness(0).ExtraComponents.numComponents() > 2)
            {
                string emailAddress = pid.GetPhoneNumberBusiness(0).ExtraComponents.getComponent(2).Data.ToString();
                if (emailAddress.IsNotNullOrEmpty())
                {
                    var patientEmailAddress = patient.PatientEmailAddresses.FirstOrDefault(e => e.EmailAddressTypeId == (int)EmailAddressType.Business);
                    if (patientEmailAddress == null) patient.PatientEmailAddresses.Add(new PatientEmailAddress { Value = emailAddress, EmailAddressTypeId = (int)EmailAddressType.Business });
                    else patientEmailAddress.Value = emailAddress;
                }
            }
            Varies raceComponent = pid.Race.ExtraComponents.getComponent(0);
            if (raceComponent != null && raceComponent.Data is GenericPrimitive)
            {
                string raceValue = pid.Race.Value.IsNullOrEmpty() ? ((GenericPrimitive)pid.Race.ExtraComponents.getComponent(0).Data).Value : pid.Race.Value;
                if (raceValue.IsNotNullOrEmpty())
                {
                    var raceMapping = _externalSystemService.Value.GetMapping<Race>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.Race, raceValue);
                    var race = (Race)_externalSystemService.Value.GetMappedEntity(raceMapping) ?? _practiceRepository.Value.Races.WithName(raceValue);
                    if (race != null && patient.Races.All(r => r.Id != race.Id))
                    {
                        patient.Races.Add(race);
                    }
                }
            }

            string language = pid.PrimaryLanguage.Text.Value.Truncate(10);
            ExternalSystemEntityMapping patientLanguageMapping = null;
            Language patientLanguage = null;

            if (!string.IsNullOrEmpty(pid.PrimaryLanguage.Identifier.Value))
            {
                patientLanguageMapping = _externalSystemService.Value.GetMapping<Language>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.Language, pid.PrimaryLanguage.Identifier.Value);
                patientLanguage = (Language)_externalSystemService.Value.GetMappedEntity(patientLanguageMapping);
            }
            if (!string.IsNullOrEmpty(language) && (patientLanguageMapping == null || patientLanguageMapping.Id == 0))
            {
                patientLanguageMapping = _externalSystemService.Value.GetMapping<Language>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.Language, language);

                patientLanguage = (Language)_externalSystemService.Value.GetMappedEntity(patientLanguageMapping) ??
                                  (_practiceRepository.Value.Languages.WithName(language));
            }
            patient.Language = patientLanguage;

            ExternalSystemEntityMapping patientEthnicityMapping = null;
            Ethnicity patientEthnicity = null;
            Varies ethnicityComponent = pid.EthnicGroup.ExtraComponents.getComponent(0);
            IS ethnicityIdentifierComponent = pid.EthnicGroup;

            if (ethnicityIdentifierComponent.Value != null)
            {
                string ethnicityIdentifier = ethnicityIdentifierComponent.Value;
                patientEthnicityMapping = _externalSystemService.Value.GetMapping<Ethnicity>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.Ethnicity, ethnicityIdentifier);
                patientEthnicity = (Ethnicity)_externalSystemService.Value.GetMappedEntity(patientEthnicityMapping);
            }
            if (ethnicityComponent != null && ethnicityComponent.Data is GenericPrimitive)
            {
                string ethnicity = ((GenericPrimitive)ethnicityComponent.Data).Value;
                if (ethnicity != null && (patientEthnicityMapping == null || patientEthnicityMapping.Id == 0))
                {
                    patientEthnicityMapping = _externalSystemService.Value.GetMapping<Ethnicity>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.Ethnicity, ethnicity);

                    patientEthnicity = (Ethnicity)_externalSystemService.Value.GetMappedEntity(patientEthnicityMapping) ??
                                       (_practiceRepository.Value.Ethnicities.WithName(ethnicity));
                }
            }

            if (patientEthnicity != null) patient.Ethnicity = patientEthnicity;

            patient.PriorPatientCode = pid.PatientIDExternalID.ID.Value.Truncate(16);
        }

        private void HandleInsurer(Insurer insurer, IN1 in1)
        {
            insurer.Name = in1.InsuranceCompanyName.OrganizationName.Value.Truncate(32);

            bool isInsuranceCompanyAddressValid = new[] { in1.InsuranceCompanyAddress.StreetAddress.Value, in1.InsuranceCompanyAddress.City.Value, in1.InsuranceCompanyAddress.ZipOrPostalCode.Value, in1.InsuranceCompanyAddress.StateOrProvince.Value }.All(i => i.IsNotNullOrEmpty());
            if (isInsuranceCompanyAddressValid)
            {
                InsurerAddress address = insurer.InsurerAddresses.WithAddressType(InsurerAddressTypeId.Claims, NotFoundBehavior.Add);
                address.Line1 = String.Format("{0} {1}", in1.InsuranceCompanyAddress.StreetAddress.Value, in1.InsuranceCompanyAddress.OtherDesignation.Value).Truncate(50);
                address.City = in1.InsuranceCompanyAddress.City.Value;
                address.StateOrProvince = _practiceRepository.Value.StateOrProvinces.SingleOrDefault(s => s.Abbreviation == in1.InsuranceCompanyAddress.StateOrProvince.Value.Truncate(2)).
                    EnsureNotDefault("Could not find state {0}.".FormatWith(in1.InsuranceCompanyAddress.StateOrProvince.Value));
                address.PostalCode = in1.InsuranceCompanyAddress.ZipOrPostalCode.Value;
            }

            string phone = in1.GetInsuranceCoPhoneNumber(0).Get9999999X99999CAnyText.Value;
            if (phone.IsNotNullOrEmpty() && PhoneNumberParserExtensions.IsValidPhoneNumber(phone))
            {
                InsurerPhoneNumber insurerPhone = insurer.InsurerPhoneNumbers.WithPhoneNumberType(InsurerPhoneNumberTypeId.Main, NotFoundBehavior.Add);
                insurerPhone.SetValuesFromString(phone);
            }
        }

        private InsurancePolicy HandlePatientInsurance(Patient patient, Insurer insurer)
        {
            InsurancePolicy insurance = (_practiceRepository.Value.InsurancePolicies.Include(p => p.PatientInsurances).
                                            FirstOrDefault(i => i.PolicyholderPatientId == patient.Id))
                                        ?? new InsurancePolicy();
            insurance.StartDateTime = DateTime.Now.ToClientTime().AddMonths(-1);
            insurance.PolicyCode = "12345";
            insurance.Insurer = insurer;

            if (insurance.Id == 0)
            {
                insurance.PolicyholderPatient = patient;
                insurance.PatientInsurances.Add(new PatientInsurance
                                                    {
                                                        InsuredPatient = patient,
                                                        InsuranceType = InsuranceType.MajorMedical,
                                                        OrdinalId = 0,
                                                        PolicyholderRelationshipType = PolicyHolderRelationshipType.Self
                                                    });
            }
            return insurance;
        }

        protected bool HandlesMessage(NHapiIMessage message)
        {
            return (message is ADT_A28 ||
                    message is ADT_A31 ||
                    message is ADT_A04 ||
                    message is ADT_A08 ||
                    message is NHapi.Model.V231.Message.ADT_A28 ||
                    message is NHapi.Model.V231.Message.ADT_A31 ||
                    message is NHapi.Model.V231.Message.ADT_A04 ||
                    message is NHapi.Model.V231.Message.ADT_A08);
        }

        protected void HandleMessage(HL7Message message)
        {
            HandleMessage(message.Message);
        }

        protected bool HandlesMessage(HL7Message message)
        {
            return HandlesMessage(message.Message);
        }

        protected bool HandlesMessage(IOIMessage message)
        {
            return message is HL7Message && HandlesMessage((HL7Message)message);
        }
    }
}