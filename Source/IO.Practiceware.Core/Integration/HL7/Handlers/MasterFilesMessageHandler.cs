using IO.Practiceware.Integration.HL7.Handlers;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Services.ExternalSystems;
using NHapi.Model.V23.Datatype;
using NHapi.Model.V23.Message;
using NHapi.Model.V23.Segment;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Diagnostics;
using System.Linq;
using IOIMessage = IO.Practiceware.Integration.Messaging.IMessage;
using NHapiIMessage = NHapi.Base.Model.IMessage;

[assembly: Component(typeof(MasterFilesMessageHandler.HandleMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.HL7.Handlers
{
    [SupportsUnitOfWork]
    public class MasterFilesMessageHandler
    {
        public class HandleMessageSubscriber
        {
            public HandleMessageSubscriber(IMessenger messenger, Func<MasterFilesMessageHandler> handler)
            {
                messenger.Subscribe((IMessenger s, object t, HandleMessageRequest m) => handler().HandleMessage((HL7Message)m.Message),
                                    m => handler().HandlesMessage(m.Message));
            }
        }

        private readonly Lazy<IExternalSystemService> _externalSystemService;
        private readonly Lazy<IPracticeRepository> _practiceRepository;

        public MasterFilesMessageHandler(Func<IPracticeRepository> practiceRepository, 
            Func<IExternalSystemService> externalSystemService)
        {
            _practiceRepository = Lazy.For(practiceRepository);
            _externalSystemService = Lazy.For(externalSystemService);
        }

        [UnitOfWork(AcceptChanges = true, AutoAcceptChanges = true)]
        protected virtual void HandleMessage(NHapiIMessage message)
        {
            var mfn = (MFN_M02)message;
            MSH msh = mfn.MSH;
            MFE mfe = mfn.GetMF_STAFF(0).MFE;
            STF stf = mfn.GetMF_STAFF(0).STF;
            PRA pra = mfn.GetMF_STAFF(0).PRA;
            // Right now we only support referring providers in MFN_M02 messages (not internal providers)...so ignore messages for staff types other than R or RD
            if (stf.GetStaffType(0).Value == "R" || stf.GetStaffType(0).Value == "RD")
            {
                HandleMessageComponents(msh, mfe, stf, pra);
            }
        }

        private void HandleMessageComponents(MSH msh, MFE mfe, STF stf, PRA pra)
        {
            var referringDoctorId = stf.STFPrimaryKeyValue.Identifier.Value;
            if (referringDoctorId.IsNullOrEmpty())
            {
                referringDoctorId = stf.GetStaffIDCode().Select(i => i.Identifier.Value).FirstOrDefault();
            }
            bool updateOnly = (mfe.RecordLevelEventCode.Value != "MAD") && (mfe.RecordLevelEventCode.Value != "MUP");

            ExternalSystemEntityMapping mapping = _externalSystemService.Value.GetMapping<ExternalContact>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.ExternalContact, referringDoctorId, updateOnly);
            ExternalProvider externalProvider = _practiceRepository.Value.ExternalContacts.OfType<ExternalProvider>().
                                                    Include(p => p.IdentifierCodes, p => p.ExternalContactAddresses, p => p.ExternalContactPhoneNumbers, p => p.ExternalProviderClinicalSpecialtyTypes.Select(cst => cst.ClinicalSpecialtyType)).
                                                    FirstOrDefault(i => i.Id == mapping.PracticeRepositoryEntityKey.ToInt().GetValueOrDefault())
                                                ?? new ExternalProvider();

            if (mfe.RecordLevelEventCode.Value == "MDL" && !externalProvider.FirstName.StartsWith("DELETED - "))
            {
                externalProvider.FirstName = ("DELETED - " + externalProvider.FirstName);
            }
            else
            {
                var displayName = stf.StaffName.GivenName.Value;
                if (stf.StaffName.MiddleInitialOrName.Value != null)
                    displayName = displayName + " " + stf.StaffName.MiddleInitialOrName.Value;
                displayName = displayName + " " + stf.StaffName.FamilyName.Value;
                if (stf.StaffName.SuffixEgJRorIII.Value != null)
                    displayName = displayName + " " + stf.StaffName.SuffixEgJRorIII.Value;
                if (stf.StaffName.DegreeEgMD.Value != null)
                    displayName = displayName + ", " + stf.StaffName.DegreeEgMD.Value;

                externalProvider.LastNameOrEntityName = stf.StaffName.FamilyName.Value;
                externalProvider.FirstName = stf.StaffName.GivenName.Value;
                externalProvider.MiddleName = stf.StaffName.MiddleInitialOrName.Value.Truncate(1);
                externalProvider.Honorific = stf.StaffName.DegreeEgMD.Value.Truncate(8);
                externalProvider.DisplayName = displayName;
                AD address = stf.GetOfficeHomeAddress(0);

                if (address.IsPopulated())
                {
                    ExternalContactAddress externalProviderAddress = externalProvider.ExternalContactAddresses.FirstOrDefault() ?? new ExternalContactAddress();
                    externalProvider.ExternalContactAddresses.Add(externalProviderAddress);
                    externalProviderAddress.ExternalContactAddressType = _practiceRepository.Value.ExternalContactAddressTypes.FirstOrDefault();
                    externalProviderAddress.Line1 = address.StreetAddress.Value;
                    if (!string.IsNullOrEmpty(address.OtherDesignation.Value))
                        externalProviderAddress.Line2 = address.OtherDesignation.Value.Truncate(10);
                    externalProviderAddress.City = address.City.Value;
                    externalProviderAddress.PostalCode = address.ZipOrPostalCode.Value;
                    externalProviderAddress.StateOrProvinceId = _practiceRepository.Value.StateOrProvinces.FirstOrDefault(s => s.Abbreviation.ToLower() == (address.StateOrProvince.Value ?? string.Empty).ToLower() ||
                                                                                                                               s.Name.ToLower() == (address.StateOrProvince.Value ?? string.Empty).ToLower()).
                        EnsureNotDefault("State/Province {0} cannot be found.".
                                             FormatWith(address.StateOrProvince.Value)).Id;
                }
                else
                {
                    if (address.StreetAddress.Value.IsNullOrEmpty())
                    {
                        Trace.TraceWarning("STF does not contain a valid street address.");
                    }
                    if (address.ZipOrPostalCode.Value.IsNullOrEmpty() || address.City.Value.IsNullOrEmpty() || address.StateOrProvince.Value.IsNullOrEmpty())
                    {
                        Trace.TraceWarning("STF does not contain a valid zip code or City/State.");
                    }
                }
            }

            if (stf.PhoneRepetitionsUsed == 0 || (stf.GetPhone(0).Value == null && stf.GetPhone(1).Value == null)) Trace.TraceWarning("STF doesnot contain a valid Phone Number");
            else
            {
                if (stf.GetPhone(0).Value != null)
                {
                    var phoneString = stf.GetPhone(0).Value;
                    if (PhoneNumberParserExtensions.IsValidPhoneNumber(phoneString))
                    {
                        externalProvider.ExternalContactPhoneNumbers.WithPhoneNumberType(ExternalContactPhoneNumberTypeId.Main, NotFoundBehavior.Add).SetValuesFromString(phoneString);
                    }
                    else
                    {
                        Trace.TraceWarning("STF does not contain a valid Primary Phone Number.");
                    }
                }
                else
                {
                    Trace.TraceWarning("STF does not contain a valid Primary Phone Number.");
                }

                if (stf.GetPhone(1).Value != null)
                {
                    var phoneString = stf.GetPhone(1).Value;
                    if (PhoneNumberParserExtensions.IsValidPhoneNumber(phoneString))
                    {
                        externalProvider.ExternalContactPhoneNumbers.WithPhoneNumberType(ExternalContactPhoneNumberTypeId.Fax, NotFoundBehavior.Add).SetValuesFromString(phoneString);
                    }
                    else
                    {
                        Trace.TraceWarning("STF does not contain a valid Fax Number.");
                    }
                }
                else
                {
                    Trace.TraceWarning("STF does not contain a valid Fax Number.");
                }
            }

            if (pra.SpecialtyRepetitionsUsed > 0)
            {
                ClinicalSpecialtyType specialtyType = _practiceRepository.Value.ClinicalSpecialtyTypes.FirstOrDefault(i => i.Name.ToLower() == pra.GetSpecialty(0).SpecialtyName.Value) ??
                                                      new ClinicalSpecialtyType { Name = pra.GetSpecialty(0).SpecialtyName.Value, ExternalProviderClinicalSpecialtyTypes = externalProvider.ExternalProviderClinicalSpecialtyTypes };

                var externalProviderClinicalSpecialtyType = externalProvider.ExternalProviderClinicalSpecialtyTypes.FirstOrDefault() ?? new ExternalProviderClinicalSpecialtyType { ExternalProvider = externalProvider };

                externalProviderClinicalSpecialtyType.ClinicalSpecialtyType = specialtyType;
                externalProviderClinicalSpecialtyType.OrdinalId = externalProvider.ExternalProviderClinicalSpecialtyTypes.Count;
            }

            string npi = pra.GetPractitionerIDNumbers().Where(i => i.TypeOfIDNumber.Value == "NPI").Select(i => i.IDNumber.Value).FirstOrDefault();
            if (npi.IsNotNullOrEmpty())
            {
                IdentifierCode code = externalProvider.IdentifierCodes.FirstOrDefault(c => c.IdentifierCodeType == IdentifierCodeType.Npi);
                if (code == null)
                {
                    var identifierCode = new IdentifierCode();
                    identifierCode.IdentifierCodeType = IdentifierCodeType.Npi;
                    code = identifierCode;
                    externalProvider.IdentifierCodes.Add(code);
                }
                code.ExternalProvider = externalProvider;
                code.Value = npi;
            }
            _practiceRepository.Value.Save(externalProvider);
            mapping.PracticeRepositoryEntityKey = externalProvider.Id.ToString();
            _practiceRepository.Value.Save(mapping);
        }

        protected bool HandlesMessage(NHapiIMessage message)
        {
            return message is MFN_M02 || message is NHapi.Model.V231.Message.MFN_M02;
        }

        protected void HandleMessage(HL7Message message)
        {
            if (message.Message is NHapi.Model.V231.Message.MFN_M02)
            {
                message = new HL7Message(message.ToString().ReplaceFirst("2.3.1", "2.3")) { MessageType = message.MessageType };
            }

            HandleMessage(message.Message);
        }

        protected bool HandlesMessage(HL7Message message)
        {
            return HandlesMessage(message.Message);
        }

        protected bool HandlesMessage(IOIMessage message)
        {
            return message is HL7Message && HandlesMessage((HL7Message)message);
        }
    }
}